﻿# ======================== Проверяем что корректно загружен контекст исполнения =====================================================================

if ($OutputPath -eq $null){	write-host "$($MyInvocation.MyCommand.Definition): ERROR 0: Не найдена обязательная переменная [OutputPath]";	return -1;}
if ($TargetDir -eq $null){	write-host "$($MyInvocation.MyCommand.Definition): ERROR 0: Не найдена обязательная переменная [TargetDir]"; return -1; }
if ($BuildProjectDirectory -eq $null){	write-host "$($MyInvocation.MyCommand.Definition): ERROR 0: Не найдена обязательная переменная [BuildProjectDirectory]"; return -1; }
if ($Content -eq $null){	write-host "$($MyInvocation.MyCommand.Definition): ERROR 0: Не найдена обязательная переменная [Content]"; return -1; }

#==========================================================================================================================

$MainBinDir = "$($TargetDir)..\..\Bin\"

#========================= формирование конфигурируемого пакета билда (BuildRawConfig) =======================================================

ApplyContentTemplates -targetDirectory "$($TargetDir)BuildRawConfig\" -sourceDirectory $MainBinDir -templateDirectory "$($BuildProjectDirectory)\BuildRawConfig\" -contentDirectory $BuildProjectDirectory -contentIn $Content -configuration $Configuration -templateMarker "CopyFiles.lst"

WriteToHost  "Выполнение PostBuild.FLSCommon\CopyFiles.ps1"  -newLine
#========================= формирование исполняемого пакета (NDS2) =================================================================================================

# здесь операции копирования из бинарной части + конфигурационной - CopyFiles.lst
ApplyContentTemplates -targetDirectory "$($TargetDir)NDS2\" -sourceDirectory $MainBinDir -templateDirectory "$($BuildProjectDirectory)\Build\" -contentDirectory $BuildProjectDirectory -contentIn $Content -configuration $Configuration -templateMarker "CopyFiles.lst"
