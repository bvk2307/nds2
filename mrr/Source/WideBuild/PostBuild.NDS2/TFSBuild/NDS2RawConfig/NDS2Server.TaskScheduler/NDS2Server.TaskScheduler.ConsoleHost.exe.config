<?xml version="1.0" encoding="utf-8"?>
<configuration>
  <configSections>
    <section name="hosting" type="CommonComponents.Hosting.Common.Configuration.HostingSection, CommonComponents.Hosting.Common"/>
    <section name="environment" type="CommonComponents.CompositeEnvironment.Configuration.EnvironmentSection, CommonComponents.CorLib.Bundle"/>
  </configSections>

  <!-- секция конфигурирования физического хостинга, определяет имя и роль хостинга -->
  <hosting systemName="INIAS"
           role="logicalcatalog://DmCatalog/StaticMetadata/Configuration/NDS2Server.TaskScheduler.Role.config">

    <!-- может содержать только одно опредление логического хоста (нулевой) который запускается при старте физического хостинга -->
    <host name="DemoServer.PhysicalHost"
         assembly="CommonComponents.CorLib.Bundle"
         type="CommonComponents.GenericHost.ApplicationHost"
         roleEnviAddress="logicalcatalog://InfraCatalog/Infra/Environments/ServerLoaderEnvi">

      <strategyGroup>
        <chains>
          <add name="Create-Destroy">
            <strategies>
              <add name="CreateServiceCollectionStrategy"
                   instanceType="CommonComponents.CompositeEnvironment.CreateServiceCollectionStrategy, CommonComponents.CorLib.Bundle"/>
              <add name="CreateLoggingServiceStrategy"
                   instanceType="CommonComponents.Instrumentation.CreateLoggingServiceStrategy, CommonComponents.CorLib.Bundle"/>
              <add name="ManagementStrategy"
                   instanceType="CommonComponents.Management.ManagementStrategy, CommonComponents.CorLib.Bundle"/>
              <add name="CreateApplicationHostStrategy"
                   instanceType="CommonComponents.GenericHost.Strategies.CreateApplicationHostStrategy, CommonComponents.CorLib.Bundle"/>
              <add name="RegistryCatalogServicesStrategy"
                   instanceType="CommonComponents.Catalog.RegistryCatalogServicesStrategy, CommonComponents.CorLib.Bundle"/>
            </strategies>
          </add >
          <add name="Initialize-Deinitialize">
            <strategies>
              <add name="InitializeChildHostLoaderEnviStrategy"
                   instanceType="CommonComponents.GenericHost.Strategies.InitializeChildHostLoaderEnviStrategy, CommonComponents.CorLib.Bundle"/>

            </strategies>
          </add>
          <add name="Activate-Deactivate">
            <strategies/>
          </add>
        </chains>
        <events>
          <add name="ApplicationHost.BuildUp" direction="Forward" chainName="Create-Destroy"/>
          <add name="ApplicationHost.TearDown" direction="Reverse" chainName="Create-Destroy"/>
          <add name="ApplicationHost.Initialize" direction="Forward" chainName="Initialize-Deinitialize"/>
          <add name="ApplicationHost.Deinitialize" direction="Reverse" chainName="Initialize-Deinitialize"/>
          <add name="ApplicationHost.Activate" direction="Forward" chainName="Activate-Deactivate"/>
          <add name="ApplicationHost.Deactivate" direction="Reverse" chainName="Activate-Deactivate"/>
        </events>
      </strategyGroup>
    </host>
  </hosting>

  <!-- конфигурация находящаяся ниже используется нулевым логическим хостом -->
  <environment name="DemoServer.PhysicalHost.Envi">
    <catalogs>
      <!-- Провайдер Сервис -->
      <add name="ProviderService"
           providerType="CommonComponents.CompositeEnvironment.Engine.Composition.TypeCatalogProvider, CommonComponents.CorLib.Bundle"
           types="CommonComponents.CompositeEnvironment.Services.ComposableProviderService, CommonComponents.CorLib.Bundle"/>
      <!-- Сервис точного времени -->
      <add name="TimeService"
           providerType="CommonComponents.CompositeEnvironment.Engine.Composition.TypeCatalogProvider, CommonComponents.CorLib.Bundle"
           types="CommonComponents.TimeService.SntpTimeService, CommonComponents.CorLib.Bundle"/>

      <!-- Сервис обработки ошибок. -->
      <add name="ExceptionHandlingService"
           providerType="CommonComponents.CompositeEnvironment.Engine.Composition.TypeCatalogProvider, CommonComponents.CorLib.Bundle"
           types="CommonComponents.ExceptionHandling.ExceptionHandlingService, CommonComponents.CorLib.Bundle" />

      <!-- сервис создания сред -->
      <add name="EnvironmentService"
           providerType="CommonComponents.CompositeEnvironment.MiniCatalogProvider, CommonComponents.CorLib.Bundle"
           miniCatalogs="CommonComponents.CorLib.Bundle:EnvironmentService"/>
      <!-- конфигурация -->
      <add name="Configuration"
           providerType="CommonComponents.CompositeEnvironment.ConfigurationCatalogProvider, CommonComponents.CorLib.Bundle"/>
    </catalogs>
  </environment>

  <appSettings>
    <add key="DevDynamicCatalogs" value="DmCatalogTemplate;DmCatalogTemplate.Dev;DmCatalogTemplate.Test" />

    <!--Инструментирование, Управление и профилирование-->
    <add key="EventLogSource" value="NDS2.Server.TaskScheduler"/>
    <add key="EnableManagement" value="true"/>
    <add key="EnableRealtimeMonitor" value="true"/>
    <add key="EnableCapturingMonitor" value="true"/>

  </appSettings>

  <runtime>
    <assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">
      <probing privatePath="SystemCatalog\CSC\Bin;SystemCatalog\Abs4.0\Bin;Common"/>
    </assemblyBinding>
    <gcServer enabled="true" />
    <connectionManagement>
      <add address="*" maxconnection="1000"/>
    </connectionManagement>
  </runtime>

</configuration>
