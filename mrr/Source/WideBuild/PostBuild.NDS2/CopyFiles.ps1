﻿# ======================== Проверяем что корректно загружен контекст исполнения ===============================================================

if ($OutputPath -eq $null){	write-host "$($MyInvocation.MyCommand.Definition): ERROR 0: Не найдена обязательная переменная [OutputPath]";	return -1;}
if ($TargetDir -eq $null){	write-host "$($MyInvocation.MyCommand.Definition): ERROR 0: Не найдена обязательная переменная [TargetDir]"; return -1; }
if ($BuildProjectDirectory -eq $null){	write-host "$($MyInvocation.MyCommand.Definition): ERROR 0: Не найдена обязательная переменная [BuildProjectDirectory]"; return -1; }
if ($Content -eq $null){	write-host "$($MyInvocation.MyCommand.Definition): ERROR 0: Не найдена обязательная переменная [Content]"; return -1; }

#& dir $TargetDir

#==========================================================================================================================

$MainBinDir = "$($TargetDir)..\..\Bin\"
$DbScriptsDir = "$($TargetDir)..\..\Source\Database\Luxoft.NDS2.Database"
$DbScriptsDirSEOD = "$($TargetDir)..\..\Source\Database\Luxoft.NDS2.Database.SEOD"
$DbScriptsDirCI = "$($TargetDir)..\..\Source\Database\Luxoft.NDS2.Database.CI"
$DbScriptsDirAis = "$($TargetDir)..\..\Source\Database\Luxoft.NDS2.Database.AIS"
$DbScriptsDirFir = "$($TargetDir)..\..\Source\Database\Luxoft.NDS2.Database.FIR"
$DbScriptsDirICam = "$($TargetDir)..\..\Source\Database\Luxoft.NDS2.Database.ICAM"
$DbScriptsDirINds2 = "$($TargetDir)..\..\Source\Database\Luxoft.NDS2.Database.Exchange"
$DbScriptsDirAllInOneDev = "$($TargetDir)..\..\Source\Database\Luxoft.NDS2.Database"
$DbScriptsDirAllInOneRel = "$($TargetDir)..\..\Source\Database\Luxoft.NDS2.Database"
$BuildDir = "$($TargetDir)\NDS2"
$ServerDir = "$($TargetDir)..\..\Build\$configuration"
WriteToHost $MainBinDir -newLine
WriteToHost $configuration -newLine
WriteToHost $ServerDir -newLine
WriteToHost $BuildProjectDirectory -newLine

#========================= формирование конфигурируемого пакета билда (DemoRawConfig) =======================================================

ApplyContentTemplates -targetDirectory "$($TargetDir)NDS2RawConfig\" -sourceDirectory $MainBinDir -templateDirectory "$($BuildProjectDirectory)\NDS2RawConfig\" -contentDirectory $BuildProjectDirectory -contentIn $Content -configuration $Configuration -templateMarker "CopyFiles.lst"

#-------------------------- Формирование ConfigStorage в конфигурируемом пакете билда (DemoRawConfig) -----------------------------------------

$MergeTool			= "{0}BuildRawBin\DevTools\Abs4.0\ConfigMergeTool\CommonComponents.ConfigMerge.Tool.exe" -f $TargetDir
$TargetConfigDir	= "{0}NDS2RawConfig\ConfigStorage\" -f $TargetDir

WriteToHost "____Формирование NotificationsProfile.xml" -newLine



#========================= формирование исполняемого пакета (Demo) =================================================================================================

WriteToHost $TargetDir -newLine
# здесь операции копирования из бинарной части + конфигурационной - CopyFiles.lst
ApplyContentTemplates -targetDirectory "$($TargetDir)NDS2\" -sourceDirectory $MainBinDir -templateDirectory "$($BuildProjectDirectory)\NDS2\" -contentDirectory $BuildProjectDirectory -contentIn $Content -configuration $Configuration -templateMarker "CopyFiles.lst"
ApplyContentTemplates -targetDirectory "$($TargetDir)Deploy\" -sourceDirectory $MainBinDir -templateDirectory "$($BuildProjectDirectory)\Deploy\" -contentDirectory $BuildProjectDirectory -contentIn $Content -configuration $Configuration -templateMarker "CopyFiles.lst"
ApplyContentTemplates -targetDirectory "$($TargetDir)LoadTest\" -sourceDirectory $MainBinDir -templateDirectory "$($BuildProjectDirectory)\LoadTest\" -contentDirectory $BuildProjectDirectory -contentIn $Content -configuration $Configuration -templateMarker "CopyFiles.lst"
ApplyContentTemplates -targetDirectory "$($TargetDir)Tools\" -sourceDirectory $MainBinDir -templateDirectory "$($BuildProjectDirectory)\Tools\" -contentDirectory $BuildProjectDirectory -contentIn $Content -configuration $Configuration -templateMarker "CopyFiles.lst"
ApplyContentTemplates -targetDirectory "$($TargetDir)TFSBuild\" -sourceDirectory $MainBinDir -templateDirectory "$($BuildProjectDirectory)\TFSBuild\" -contentDirectory $BuildProjectDirectory -contentIn $Content -configuration $Configuration -templateMarker "CopyFiles.lst"

& "$($TargetDir)..\..\Bin\DbResultScriptBuilder.exe" "$($DbScriptsDir)" "$($BuildDir)\Database\MRR\" "$($BuildDir)\Database\MRR\DbResultScriptBuilder.cfg" "DatabaseBuildConfig.xml"
if ($lastexitcode -eq 11) {
	throw [System.Exception] "Error in process SQL-file generation"
}

& "$($TargetDir)..\..\Bin\DbResultScriptBuilder.exe" "$($DbScriptsDirSEOD)" "$($BuildDir)\Database\SEOD\" "$($BuildDir)\Database\SEOD\DbResultScriptBuilder.cfg" "DatabaseBuildConfig.xml"
if ($lastexitcode -eq 11) {
	throw [System.Exception] "Error in process SQL-file generation"
}

& "$($TargetDir)..\..\Bin\DbResultScriptBuilder.exe" "$($DbScriptsDirCI)" "$($BuildDir)\Database\CI\" "$($BuildDir)\Database\CI\DbResultScriptBuilder.cfg" "DatabaseBuildConfig.xml"
if ($lastexitcode -eq 11) {
	throw [System.Exception] "Error in process SQL-file generation"
}

& "$($TargetDir)..\..\Bin\DbResultScriptBuilder.exe" "$($DbScriptsDirAis)" "$($BuildDir)\Database\AIS\" "$($BuildDir)\Database\AIS\DbResultScriptBuilder.cfg" "DatabaseBuildConfig.xml"
if ($lastexitcode -eq 11) {
	throw [System.Exception] "Error in process SQL-file generation"
}

& "$($TargetDir)..\..\Bin\DbResultScriptBuilder.exe" "$($DbScriptsDirFir)" "$($BuildDir)\Database\FIR\" "$($BuildDir)\Database\FIR\DbResultScriptBuilder.cfg" "DatabaseBuildConfig.xml"
if ($lastexitcode -eq 11) {
	throw [System.Exception] "Error in process SQL-file generation"
}

& "$($TargetDir)..\..\Bin\DbResultScriptBuilder.exe" "$($DbScriptsDirICam)" "$($BuildDir)\Database\ICAM\" "$($BuildDir)\Database\ICAM\DbResultScriptBuilder.cfg" "DatabaseBuildConfig.xml"
if ($lastexitcode -eq 11) {
	throw [System.Exception] "Error in process SQL-file generation"
}

& "$($TargetDir)..\..\Bin\DbResultScriptBuilder.exe" "$($DbScriptsDirINds2)" "$($BuildDir)\Database\INDS2\" "$($BuildDir)\Database\INDS2\DbResultScriptBuilder.cfg" "DatabaseBuildConfig.xml"
if ($lastexitcode -eq 11) {
	throw [System.Exception] "Error in process SQL-file generation"
}

& "$($TargetDir)..\..\Bin\XMLValidator\XMLValidator.exe" "$($TargetDir)..\..\Configuration\INIAS.AzManStore.xml" "$($TargetDir)..\..\Configuration\INIAS.AzManStore.xsd"
if ($lastexitcode -eq 11) {
	throw [System.Exception] "Error in process AZMAN-file validation"
}

WriteToHost "Выкладка серверов в $ServerDir" -newLine

Copy-Item -Path "$($TargetDir)TFSBuild\Client" -Destination "$($ServerDir)\Client" -Recurse -Force
Copy-Item -Path "$($TargetDir)TFSBuild\NDS2Server" -Destination "$($ServerDir)\NDS2Server" -Recurse -Force
Copy-Item -Path "$($TargetDir)TFSBuild\NDS2Server.TaskScheduler" -Destination "$($ServerDir)\NDS2Server.TaskScheduler" -Recurse -Force

Copy-Item -Path  "$($TargetDir)TFSBuild\Client\AzMan\INIAS.AzManStore.xml" -Destination "$($ServerDir)\Client\AzMan\АСКНДС2_ТС.xml"

WriteToHost Done
#& dir $BuildDir
