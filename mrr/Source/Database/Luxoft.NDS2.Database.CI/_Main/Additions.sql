﻿grant select on nds2_seod.seod_declaration_annulment to nds2_mrr_user with grant option;
grant select on nds2_seod.SEOD_DECL_ANNULMENT_REASON to nds2_mrr_user with grant option;

grant insert on nds2_mrr_user.doc_status_history to nds2_seod;
grant execute on nds2_mrr_user.utl_add_work_days to nds2_seod;
grant select on nds2_mrr_user.V$DOC to nds2_seod;
grant execute on nds2_mrr_user.PAC$REMARKABLE_CHANGES to nds2_seod;
grant execute on NDS2_MRR_USER.utl_get_configuration_number to nds2_seod; 
grant update on nds2_mrr_user.doc to nds2_seod;
grant execute on NDS2_MRR_USER.PAC$ASYNC_SCHEDULER to nds2_seod;
grant execute on NDS2_MRR_USER.NDS2$EXPLAIN_REPLY to nds2_seod;
grant execute on NDS2_MRR_USER.UTL_CLOSE_DOCUMENT to nds2_seod;
grant select on NDS2_MRR_USER.DOC to nds2_seod;
grant update on NDS2_MRR_USER.bank_account_request to nds2_seod;
grant execute on NDS2_MRR_USER.pac$bank_accounts to nds2_seod;
grant select on NDS2_MRR_USER.QUEUE_SEOD_STREAM_PROCESS to nds2_seod;
grant update on NDS2_MRR_USER.QUEUE_SEOD_STREAM_PROCESS to nds2_seod;
grant execute on NDS2_MRR_USER.PAC$LOGICAL_LOCK to nds2_seod;
grant select on nds2_mrr_user.configuration to nds2_seod;
grant select on nds2_mrr_user.v$declaration_history to nds2_seod;
grant select on nds2_mrr_user.v$declaration_annulment to nds2_seod;
grant select on NDS2_MRR_USER.V$ASK_DECLANDJRNL to nds2_seod;

grant execute on nds2_mrr_user.PAC$SEOD TO NDS2_seod;
grant execute on nds2_mrr_user.PAC$SEOD TO NDS2_MC;

grant execute on nds2_seod.pac$support to nds2_mrr_user;
grant execute on nds2_seod.pac$seod_exchange to nds2_mrr_user;
grant execute on nds2_seod.nds2$sys to nds2_mrr_user;

grant select on NDS2_MRR_USER.V$DECL_ANNULMENT_STATUS to NDS2_MC;
grant select on NDS2_MRR_USER.V$DECLARATION_ANNULMENT to NDS2_MC;

truncate table NDS2_MRR_USER.DECLARATION_KNP_STATUS;

BEGIN
  DBMS_UTILITY.COMPILE_SCHEMA('NDS2_MRR_USER');
END;
/
BEGIN
  DBMS_UTILITY.COMPILE_SCHEMA('NDS2_SEOD');
END;
/

