﻿CREATE OR REPLACE PACKAGE NDS2_SEOD.PAC$SEOD_EXCHANGE
as
  procedure P$PROCESS_SEOD_INCOME;
  
  procedure P$REPEAT_PROCESS_FAILED_SEOD (
    p_info_type varchar2
   ,p_result_text_clause varchar2 := '%'
  );
end;
/
CREATE OR REPLACE PACKAGE BODY NDS2_SEOD.PAC$SEOD_EXCHANGE
as

REASON_DOC_CLOSE_BY_CLOSE_KNP number(2) := 3;
LIMIT_SEOD_SESSION_PROCESSED NUMBER(1) := 3;	
err_msg_symbol_limit constant number := 2400;

procedure UTL_LOG
  (
    v_source varchar2,
    v_code varchar2 := 0,
    v_msg varchar2 := null,
    v_entity_id varchar2 := null
  )
as
    pragma autonomous_transaction;
begin
    insert into NDS2_SEOD.system_log(id, type, site, entity_id, message_code, message, log_date)
    values(NDS2_SEOD.seq_sys_log_id.nextval, 0, substr('PAC$SEOD_EXCHANGE.'||v_source,0,128), v_entity_id, v_code, substr(v_msg,0,2048), sysdate);
    commit;
    
    exception when others then
        rollback;
        dbms_output.put_line('UTL_LOG:'||sqlcode||'-'||substr(sqlerrm, 0, 256));
end;

/*log warn message*/
procedure log_info(p_scope varchar2, p_msg varchar2)
as
begin
     UTL_LOG(p_scope, 0, p_scope||': '||p_msg);
end;

/*log error message*/
procedure log_err(p_scope varchar2, p_err_code number, p_msg varchar2, p_stack varchar2 :=null)
as
begin
	 if p_stack is not null then
         if length(p_msg) + length(p_stack) + 20 < err_msg_symbol_limit  then
            UTL_LOG(p_scope, p_err_code, p_msg||'. StackTrace: '||p_stack);
            return;
         end if;
		UTL_LOG(p_scope, p_err_code, 'StackTrace:'||p_stack);
	 end if;

     UTL_LOG(p_scope, p_err_code, p_msg);
end;

procedure execute_short_query(p_sql in varchar2)
as
  v_log_scope constant varchar2(32 char):='execute_short_query';
begin
  execute immediate p_sql;
  exception when others then
    log_err(v_log_scope,sqlcode,substr(sqlerrm, 1, 128)||':'||p_sql);
end;


function UTL_EXTRACT_ATTRIBUTE_STRING
(
  p_xml xmltype,
  p_xpath varchar2
)
return varchar2
is
begin
  if p_xml.extract(p_xpath) is not null then
    return p_xml.extract(p_xpath).getStringVal();
  else
    return null;
  end if;
  exception when others then return null;
end;

function UTL_EXTRACT_ATTRIBUTE_NUM
(
  p_xml xmltype,
  p_xpath varchar2
)
return number
is
begin
  if p_xml.extract(p_xpath) is not null then
    return p_xml.extract(p_xpath).getNumberVal();
  else
    return null;
  end if;
  exception when others then return null;
end;

function UTL_CONVERT_ATTRIBUTE_NUM
(
  p_value varchar2
)
return number
is
begin
  if p_value is not null then
    return to_number(p_value);
  else
    return null;
  end if;
  exception when others then
    dbms_output.put_line('UTL_CONVERT_ATTRIBUTE_NUM:'||sqlcode||'-'||substr(sqlerrm, 256));
    return null;
end;

function UTL_EXTRACT_ATTRIBUTE_DATE
(
  p_xml xmltype,
  p_xpath varchar2
)
return date
is
    v_strVal varchar2(10 CHAR);
    v_pattern varchar2(10 CHAR) := 'dd.mm.yyyy';
begin
    if p_xml.extract(p_xpath) is not null then
        v_strVal := p_xml.extract(p_xpath).getStringVal();
        if length(v_strVal) = 10 then
            return to_date(v_strVal, v_pattern);
        else
            return null;
        end if;
    else
        return null;
    end if;
    
    exception when others then
        dbms_output.put_line('UTL_EXTRACT_ATTRIBUTE_DATE:'||sqlcode||'-'||substr(sqlerrm, 256));
        return null;
end;

function UTL_CONVERT_ATTRIBUTE_DATE
(
  p_value varchar2
)
return date
is
v_pattern varchar2(10 CHAR) := 'dd.mm.yyyy';
begin

  if p_value is not null then
    if length(p_value) = 10 then
      return to_date(p_value, v_pattern);
    else
      return null;
    end if;
  else
    return null;
  end if;
  exception when others then
    dbms_output.put_line('UTL_CONVERT_ATTRIBUTE_DATE:'||sqlcode||'-'||substr(sqlerrm, 256));
    return null;
end;

function CREATE_EXPLAIN
(
  p_doc_id  number,
  p_type_id number,
  p_incoming_num  varchar2,
  p_incoming_date date,
  p_executor_receive_date  date,
  p_send_date_to_iniciator date
)
return number
as
  v_explain_id number;
begin
  v_explain_id := NDS2_SEOD.SEQ_EXPLAIN_REPLY.NEXTVAL();

  insert into NDS2_SEOD.seod_explain_reply (explain_id, doc_id, type_id, status_id, status_set_date, incoming_num, incoming_date, executor_receive_date, send_date_to_iniciator)
  values (v_explain_id, p_doc_id, p_type_id, 1, sysdate, p_incoming_num, p_incoming_date, p_executor_receive_date, p_send_date_to_iniciator);

  NDS2_MRR_USER.PAC$SEOD.P$HIST_EXPLAIN_REPLY_STATUS_I(v_explain_id);

  return v_explain_id;
end;

function F$CHECK_PROCESS_AVAILABILITY(p_doc_id number)
return number
as
	v_count number;
begin
	select count(d.inn) into v_count
	from
	(
	  select dh.inn_contractor as inn,dh.kpp, dh.fiscal_year,dh.period_code, dh.CORRECTION_NUMBER
	  from (select ref_entity_id, sono_code from nds2_mrr_user.doc where doc_id = p_doc_id) d
	  join nds2_mrr_user.v$declaration_history dh on dh.reg_number = d.ref_entity_id and dh.SONO_CODE_SUBMITED = d.SONO_CODE
	) d
	join nds2_mrr_user.v$declaration_annulment da on
			d.inn = da.inn
		and d.kpp = da.kpp
		and d.fiscal_year = da.fiscal_year
		and d.period_code = da.period_code
		and d.CORRECTION_NUMBER = da.CORRECTION_NUMBER
		and da.STATUS_ID not in (4);

	if v_count = 0 then return 1;
	else return 0;
	end if;
end;

function F$CHECK_PROC_AVAIL_BY_REGNUM(p_DeclarationRegNum number, p_Sono number)
return number
as
	v_count number;
begin
	select count(d.inn) into v_count
	from
	(
	  select dh.inn_contractor as inn,dh.kpp, dh.fiscal_year,dh.period_code, dh.CORRECTION_NUMBER
	  from nds2_mrr_user.v$declaration_history dh 
	  where dh.reg_number = p_DeclarationRegNum and dh.SONO_CODE_SUBMITED = p_Sono
	) d
	join nds2_mrr_user.v$declaration_annulment da on
			d.inn = da.inn
		and d.kpp = da.kpp
		and d.fiscal_year = da.fiscal_year
		and d.period_code = da.period_code
		and d.CORRECTION_NUMBER = da.CORRECTION_NUMBER
		and da.STATUS_ID not in (4);

	if v_count = 0 then return 1;
	else return 0;
	end if;

end;

/*########################################################################################################################*/
/*## ЭОД-1 ###############################################################################################################*/
/*#идентификационные данные по налоговой декларации по налогу на добавленную стоимость, передаваемые из СЭОД в АСК НДС-2.#*/
/*########################################################################################################################*/
function CAM_01_01_WRITE
   (
      p_data in clob,
      p_id in number,
      p_sono_code in varchar2,
      p_eod_date in date,
      p_receipt_date in date,
      p_refuse_reason out varchar2
   )
  return boolean
  is
    v_log_scope constant varchar2(16):='CAM_01_01_WRITE';
    v_data xmltype;
    v_nds2_id number;
    v_tax_period varchar2(2 char);
    v_fiscal_year varchar2(4 char);
    v_correction_number varchar2(3 char);
    v_decl_reg_num number;
    v_decl_fid varchar2(128 char);
    v_submission_date date;
    v_tax_payer_type number(1);
    v_inn varchar2(12 char);
    v_kpp varchar2(9 char);
    v_idFile varchar2(512 char);
    v_deliveryType number(1) := 1;
    r_id number;
    r_decl_reg_num number(20);
    r_seod_reg_num number;
    r_seod_reg_number_cancelled number;
    r_exist number(1);
	r_inn varchar2(12 char);
  begin

    v_data                := xmltype(p_data);
    v_tax_period          := v_data.extract('/Файл/Документ/@Период').getStringVal();
    v_fiscal_year         := v_data.extract('/Файл/Документ/@ОтчетГод').getStringVal();
    v_correction_number   := v_data.extract('/Файл/Документ/@НомКорр').getStringVal();
    v_decl_reg_num        := to_number(v_data.extract('/Файл/Документ/@РегНомДек').getStringVal());
    v_decl_fid            := v_data.extract('/Файл/Документ/@ФидДек').getStringVal();
    v_tax_payer_type      := v_data.existsNode('/Файл/Документ/СвНП/НПЮЛ');
    v_idFile              := UTL_EXTRACT_ATTRIBUTE_STRING(v_data, '/Файл/@ИдФайл');
    v_submission_date     := nvl(to_date(UTL_EXTRACT_ATTRIBUTE_STRING(v_data, '/Файл/Документ/@ДатаПредНД'), 'DD.MM.YYYY'), sysdate);

    if length(v_idFile) > 5 then
       v_deliveryType := 0;
    end if;

    if v_tax_payer_type = 1 then
       v_inn := v_data.extract('/Файл/Документ/СвНП/НПЮЛ/@ИННЮЛ').getStringVal();
       v_kpp := v_data.extract('/Файл/Документ/СвНП/НПЮЛ/@КПП').getStringVal();
    else
       v_inn := v_data.extract('/Файл/Документ/СвНП/НПФЛ/@ИННФЛ').getStringVal();
    end if;

    v_nds2_id := to_number(v_tax_period||v_fiscal_year||v_inn);

    merge into NDS2_SEOD.SEOD_DECLARATION sd
    using (select v_decl_reg_num as seod_reg_num, p_sono_code as sono_cd, v_kpp as kpp, v_inn as inn from dual) tmp
        on (tmp.seod_reg_num = sd.decl_reg_num and tmp.sono_cd = sd.sono_code and tmp.inn = sd.inn and nvl(tmp.kpp, '0') = nvl(sd.kpp, '0'))
    when not matched then
        insert (id, nds2_id, type, sono_code, tax_period, fiscal_year, correction_number, decl_reg_num, decl_fid, tax_payer_type, inn, kpp, insert_date, eod_date, date_receipt, submission_date, delivery_type, Id_file)
        values(p_id, v_nds2_id, 0, p_sono_code, v_tax_period, v_fiscal_year, v_correction_number, v_decl_reg_num, v_decl_fid, v_tax_payer_type, v_inn, v_kpp, sysdate, p_eod_date, p_receipt_date, v_submission_date, v_deliveryType, v_idFile);

    select rn.ID, rn.DECL_REG_NUM, rn.INN
      into r_id, r_decl_reg_num, r_inn
      from dual
      left join NDS2_SEOD.SEOD_REG_NUMBER rn 
        on rn.INN = v_inn 
        and rn.TAX_PERIOD = v_tax_period 
        and rn.FISCAL_YEAR = v_fiscal_year
        and rn.SONO_CODE = p_sono_code 
        and rn.CORRECTION_NUMBER = v_correction_number 
        and rn.TYPE = 0 
        and ((rn.ID_FILE is null and v_idFile is null) 
          or (rn.ID_FILE = '' and v_idFile is null) 
          or (rn.ID_FILE is null and v_idFile = '') 
          or (rn.ID_FILE = v_idFile)); -- нельзя использовать сравнение nvl(rn.ID_FILE, '') = nvl(v_idFile, ''), так как nvl для переменной равной NULL и поля таблицы, содержащего NULL работает по разному

    if (r_id is null) then
        begin
            r_seod_reg_num := NDS2_SEOD.SEQ_SEOD_REG_NUMBER.NEXTVAL(); 
            insert into NDS2_SEOD.SEOD_REG_NUMBER ("ID", DECL_REG_NUM, INN, TAX_PERIOD, FISCAL_YEAR, SONO_CODE, CORRECTION_NUMBER, "TYPE", ID_FILE, EOD_DATE, SUBMISSION_DATE, INSERT_DATE)
            values (r_seod_reg_num, v_decl_reg_num, v_inn, v_tax_period, v_fiscal_year, p_sono_code, v_correction_number, 0, v_idFile, p_eod_date, v_submission_date, sysdate); 
        end;
    elsif (r_decl_reg_num != v_decl_reg_num) then
        begin
            update NDS2_SEOD.SEOD_REG_NUMBER 
            set DECL_REG_NUM = v_decl_reg_num 
            where ID = r_id;

			r_seod_reg_number_cancelled := NDS2_SEOD.SEQ_SEOD_REG_NUMBER_CANCELLED.NEXTVAL(); 
            insert into NDS2_SEOD.SEOD_REG_NUMBER_CANCELLED ("ID", SONO_CODE, DECL_REG_NUM, NEW_DECL_REG_NUM, CANCELLED_AT)
            values (r_seod_reg_number_cancelled, p_sono_code, r_decl_reg_num, v_decl_reg_num, sysdate);
    
            select case when exists(select REF_ENTITY_ID from NDS2_MRR_USER.doc where REF_ENTITY_ID = r_decl_reg_num
            and SONO_CODE = p_sono_code and INN = r_inn) then 1 else 0 end into r_exist from dual;
            if (r_exist = 1) then      
              update NDS2_MRR_USER.doc 
              set REF_ENTITY_ID = v_decl_reg_num
              where REF_ENTITY_ID = r_decl_reg_num and SONO_CODE = p_sono_code and INN = r_inn;
            end if;
    
            select case when exists(select DECLARATION_REG_NUM from NDS2_SEOD.seod_knp where DECLARATION_REG_NUM = r_decl_reg_num
            and IFNS_CODE = p_sono_code) then 1 else 0 end into r_exist from dual;
            if (r_exist = 1) then
              update NDS2_SEOD.seod_knp 
              set DECLARATION_REG_NUM = v_decl_reg_num
              where DECLARATION_REG_NUM = r_decl_reg_num and IFNS_CODE = p_sono_code;
            end if;

        end;
    end if;

	NDS2_MRR_USER.PAC$REMARKABLE_CHANGES.RESET_CHANGES(r_decl_reg_num, p_sono_code);

    return true;
  exception when others then
    p_refuse_reason := substr(sqlerrm, 0, 256);
    log_err(v_log_scope, sqlcode, sqlerrm, DBMS_UTILITY.FORMAT_ERROR_BACKTRACE );
    return false;
  end;

/*################################################################################*/
/*## ЭОД-2 #######################################################################*/
/*#данные по статусам исполнения автотребования, передаваемые из СЭОД в АСК НДС-2#*/
/*################################################################################*/
function CAM_02_01_WRITE
   (
      p_data in clob,
      p_id in number,
      p_sono_code in varchar2,
      p_eod_date in date,
      p_receipt_date in date,
      p_refuse_reason out varchar2
   )
  return boolean
  as
    v_log_scope constant varchar2(16):='CAM_02_01_WRITE';  
    v_xml xmltype;
    v_doc_id number(19);
    v_seod_doc_num varchar2(128);
    v_seod_doc_date date;
    v_send_date date;
    v_send_date_limit date;
    v_delivery_date date;
    v_delivery_method varchar(2);
    v_status number(1);
    v_deadline_date date;
    v_inn_contractor varchar2(12);
    v_kpp_effective varchar2(9);
    v_type  number;
    v_period  varchar2(2);
    v_year  varchar2(4);
  begin
    v_xml             := xmltype(p_data);
    v_doc_id          := UTL_EXTRACT_ATTRIBUTE_NUM(v_xml, '/Файл/Документ/ТребПоясн/@УчНомТреб');
    v_seod_doc_num    := UTL_EXTRACT_ATTRIBUTE_STRING(v_xml, '/Файл/Документ/ТребПоясн/@НомДок');
    v_seod_doc_date   := UTL_EXTRACT_ATTRIBUTE_DATE(v_xml, '/Файл/Документ/ТребПоясн/@ДатаДок');
    v_send_date       := UTL_EXTRACT_ATTRIBUTE_DATE(v_xml, '/Файл/Документ/ТребПоясн/@ДатаОтправ');
    v_delivery_date   := UTL_EXTRACT_ATTRIBUTE_DATE(v_xml, '/Файл/Документ/ТребПоясн/@ДатаВруч');
    v_delivery_method := UTL_EXTRACT_ATTRIBUTE_STRING(v_xml, '/Файл/Документ/ТребПоясн/@СпВруч');

    if F$CHECK_PROCESS_AVAILABILITY(v_doc_id) = 0 then
		return true;
	end if;

	/*отправлено налогоплательщику*/
    if v_send_date is not null then
      v_status := 4;
      insert into NDS2_MRR_USER.doc_status_history(doc_id, status, status_date)
      values(v_doc_id, v_status, sysdate);

    /* Проверить просрочку */
    v_send_date_limit := NDS2_MRR_USER.utl_add_work_days(v_seod_doc_date, 3);
    if v_send_date>v_send_date_limit -- Дата отправки позже предельной даты
    then
      /* Calc v_nds2_id */
        select 
            SD.Inn, SD.KPP, SD.Type, SD.Tax_Period, SD.Fiscal_Year
        into v_inn_contractor, v_kpp_effective, v_type, v_period, v_year
        from NDS2_SEOD.SEOD_DECLARATION SD
        inner join NDS2_MRR_USER.V$DOC D 
            ON SD.decl_reg_num = D.REF_ENTITY_ID 
            and SD.sono_code = D.Sono_Code
        Where D.DOC_ID = v_doc_id;
        
        if SQL%ROWCOUNT = 0 then
            p_refuse_reason := 'не найден ИД декларации для требования '||v_doc_id;
            return false;
        end if;
        
        NDS2_MRR_USER.PAC$REMARKABLE_CHANGES.SLIDE_AT_UP_ASYNC(v_inn_contractor, v_kpp_effective, v_type, v_period, v_year);
    end if;

    v_deadline_date := NDS2_MRR_USER.utl_add_work_days(v_send_date,
        NDS2_MRR_USER.utl_get_configuration_number('timeout_answer_for_autoclaim') +
        NDS2_MRR_USER.utl_get_configuration_number('timeout_claim_delivery'));
    end if;
    /*вручено налогоплательщику*/
    if v_delivery_date is not null then
      v_status := 5;
      insert into NDS2_MRR_USER.doc_status_history(doc_id, status, status_date)
      values(v_doc_id, v_status, sysdate);

      v_deadline_date := NDS2_MRR_USER.utl_add_work_days(v_delivery_date,
        NDS2_MRR_USER.utl_get_configuration_number('timeout_answer_for_autoclaim'));
    end if;

    update NDS2_MRR_USER.doc d
    set
     d.tax_payer_send_date = nvl(v_send_date, d.tax_payer_send_date),
     d.external_doc_date = v_seod_doc_date,
     d.tax_payer_delivery_date = nvl(v_delivery_date, d.tax_payer_delivery_date),
     d.tax_payer_delivery_method = nvl(v_delivery_method, d.tax_payer_delivery_method),
     d.external_doc_num = v_seod_doc_num,
     d.status = nvl(v_status, d.status),
     d.status_date = trunc(sysdate),
     d.deadline_date = nvl(v_deadline_date, d.deadline_date), 
	 d.knp_sync_status = decode(d.knp_sync_status, 0, 1, d.knp_sync_status) 
    where   
        d.doc_id = v_doc_id 
        and d.sono_code = p_sono_code;

    if SQL%ROWCOUNT = 0 then
        p_refuse_reason := 'автотребование с номером '||v_doc_id||'('||p_sono_code||') не найдено';
        return false;
    end if;
    
    if v_status in(4,5) then
        NDS2_MRR_USER.PAC$ASYNC_SCHEDULER.P$SUBMIT_TASK_ANALYZE_TAXPAYER(P_DOC_ID => V_DOC_ID);
    end if;
    return true;

  exception when others then
    p_refuse_reason := substr(sqlerrm, 0, 256);
    log_err(v_log_scope, sqlcode, sqlerrm,DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
    return false;
  end;

/*#########################################################################################*/
/*### ЭОД-3 ###############################################################################*/
/*#Данные об ответе налогоплательщика на автотребование, передаваемые из СЭОД в АСК НДС-3.#*/
/*#########################################################################################*/
function CAM_03_01_WRITE
   (
      p_data in clob,
      p_id in number,
      p_sono_code in varchar2,
      p_eod_date in date,
      p_receipt_date in date,
      p_refuse_reason out varchar2
   )
  return boolean
  as
    v_log_scope constant varchar2(16):='CAM_03_01_WRITE';    
    v_xml xmltype;
    v_reply_doc_num varchar2(100 char);
    v_reply_doc_date date;
    v_reply_doc_file varchar2(300 char);
    v_doc_id number;
    v_explain_id number;
    v_receive_by_tks number;
    v_status_id number;
    v_inserted boolean;
    
    TYPE T_STRING_ARRAY IS TABLE OF VARCHAR2(100 char) INDEX BY PLS_INTEGER;
    v_list_doc_id T_STRING_ARRAY;
    TYPE T_DATE_ARRAY IS TABLE OF DATE INDEX BY PLS_INTEGER;
    v_list_reply_doc_date T_DATE_ARRAY;
    v_list_reply_doc_num T_STRING_ARRAY;
    TYPE T_BIG_STRING_ARRAY IS TABLE OF VARCHAR2(300 char) INDEX BY PLS_INTEGER;
    v_list_reply_doc_file T_BIG_STRING_ARRAY;
    v_list_index number;
begin
    log_info(v_log_scope,'start [p_id]='||to_char(p_id));
    
    v_xml := xmltype(p_data);
    
    v_list_index := 0;
    for r in(
    SELECT t.doc_id, t.reply_doc_date, t.reply_doc_num, t.doc_file
    FROM XMLTABLE('/Файл/Документ/ТребПоясн' PASSING (v_xml)
       COLUMNS
        doc_id VARCHAR2(100) PATH '@УчНомТреб',
        reply_doc_date VARCHAR2(100) PATH '@ДатаОтв',
        reply_doc_num VARCHAR2(100) PATH '@НомОтв',
        doc_file VARCHAR2(300) PATH '@ИмяФайлОтв' ) t)
    loop
    v_doc_id := UTL_CONVERT_ATTRIBUTE_NUM(r.doc_id);
    if F$CHECK_PROCESS_AVAILABILITY(v_doc_id) = 1 then
        begin
            v_list_index := v_list_index + 1;
            v_list_doc_id(v_list_index) := v_doc_id;
            v_list_reply_doc_num(v_list_index) := r.reply_doc_num;
             v_list_reply_doc_date(v_list_index) := UTL_CONVERT_ATTRIBUTE_DATE(r.reply_doc_date);
            v_list_reply_doc_file(v_list_index) := r.doc_file;
        end;
    end if;
    end loop;
    
    log_info(v_log_scope,'[documents count]='||to_char(v_list_index));
    
    FOR i IN 1..v_list_index 
    LOOP
        v_doc_id := v_list_doc_id(i);
        v_reply_doc_num := v_list_reply_doc_num(i);
        v_reply_doc_date := v_list_reply_doc_date(i);
        v_reply_doc_file := v_list_reply_doc_file(i);
        v_receive_by_tks := 1;
        v_explain_id := NDS2_SEOD.SEQ_EXPLAIN_REPLY.NEXTVAL();
        
        log_info(v_log_scope,'Document: start [doc_id]='||to_char(v_doc_id));
        
        if LENGTH(v_reply_doc_file) > 0
        then
            v_receive_by_tks := 1;
            v_status_id := 2;
        else
            v_receive_by_tks := 0;
            v_status_id := 1;
        end if;
        
        log_info(v_log_scope,'Document: [doc_id]='||to_char(v_doc_id)||'[v_receive_by_tks]='||to_char(v_receive_by_tks)||', [v_status_id]='||to_char(v_status_id));
        
        merge into NDS2_SEOD.seod_explain_reply ser
        using (select v_doc_id as did, v_reply_doc_num as ser_num from dual) tmp
        on (ser.doc_id = tmp.did and ser.incoming_num = tmp.ser_num)
        when not matched
        then
        insert(
            explain_id,
            doc_id,
            type_id,
            status_id,
            status_set_date,
            incoming_num,
            incoming_date,
            executor_receive_date,
            send_date_to_iniciator,
            FILENAMEOUTPUT,
            RECEIVE_BY_TKS
        )
        values (
            v_explain_id,
            v_doc_id,
            1,
            v_status_id,
            sysdate,
            v_reply_doc_num,
            v_reply_doc_date,
            null,
            null,
            v_reply_doc_file,
            v_receive_by_tks
        );
        
        v_inserted := SQL%ROWCOUNT > 0;
        
        if v_inserted then 
            update NDS2_MRR_USER.doc d 
            set d.TAX_PAYER_EXPLAIN_DATE = 
                case 
                    when d.TAX_PAYER_EXPLAIN_DATE is null then v_reply_doc_date 
                    else GREATEST(d.TAX_PAYER_EXPLAIN_DATE, v_reply_doc_date) 
                end,
                d.knp_sync_status = decode(d.knp_sync_status, 0, 1, d.knp_sync_status)
            where d.doc_id = v_doc_id; 
        
            log_info(v_log_scope,'Document: run PROCESS_EXPLAIN_ASYNC');
            NDS2_MRR_USER.NDS2$EXPLAIN_REPLY.PROCESS_EXPLAIN_ASYNC(v_explain_id,v_receive_by_tks);
        else
            log_info(v_log_scope,'Document: skip PROCESS_EXPLAIN_ASYNC');
            log_info(v_log_scope, 'пропущена обработка документа '||v_reply_doc_num );
        end if;
        
        log_info(v_log_scope,'Document: finished');
    END LOOP;
    
    log_info(v_log_scope,'end');
    return true;
    
    exception when others then
        p_refuse_reason := substr(sqlerrm, 0, 256);
        log_err(v_log_scope, sqlcode, sqlerrm,DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
        return false;
end;

/*#########################################################################################*/
/*### ЭОД-4 ###############################################################################*/
/*# Ход КНП (документы, закрытие КНП) #####################################################*/
/*#########################################################################################*/
function CAM_04_01_WRITE
   (
      p_data in clob,
      p_id in number,
      p_sono_code in varchar2,
      p_eod_date in date,
      p_receipt_date in date,
      p_refuse_reason out varchar2
   )
  return boolean
  as
    v_log_scope constant varchar2(16):='CAM_04_01_WRITE';    
    vDeclarationRegNum number;
    vSono varchar2(4 char);
    vDate date;
    pRawData xmltype;
    vCloseDate date;
    v_count_KnpId number;
    v_new_KnpId number;
  begin
    pRawData := xmltype(p_data);

    select extractValue(value(t),'Документ/@РегНомДек')
    into vDeclarationRegNum
    from table(XMLSequence(pRawData.Extract('Файл/Документ'))) t;

    vSono := UTL_EXTRACT_ATTRIBUTE_STRING(pRawData, '/Файл/Документ/@КодНО');

	if F$CHECK_PROC_AVAIL_BY_REGNUM(vDeclarationRegNum, vSono) = 0 then 
        return true;
	end if;

      /* Проверим, сколько есть записей по данному рег номеру для ТНО */
      v_count_KnpId := 0;
      begin
           select count(KNP_ID)
           into v_count_KnpId
           from NDS2_SEOD.SEOD_KNP
           where 
                DECLARATION_REG_NUM = vDeclarationRegNum 
                and ifns_code = vSono;
           exception when NO_DATA_FOUND then v_count_KnpId := 0;
      end;
      /* Регистрируем КНП, если таких запись равно нулю */
      if v_count_KnpId = 0 then
        begin
           select NDS2_SEOD.SEQ_SEOD_KNP.NEXTVAL into v_new_KnpId from DUAL;
           for line in (select extractValue(value(d), 'Документ/@КодНО') as sono from table(XMLSequence(pRawData.Extract('Файл/Документ'))) d)
          loop
               insert into NDS2_SEOD.SEOD_KNP (knp_id, declaration_reg_num, creation_date, ifns_code)
               values(v_new_KnpId, vDeclarationRegNum, sysdate, line.sono);
          end loop;
        end;
  end if;

  vDate := UTL_EXTRACT_ATTRIBUTE_DATE(pRawData,'/Файл/Документ/КНП/СвЗавКНП/@ДатаЗавКНП');
  -- Цикл по всем KNP_ID
  for itemKNP in (select KNP_ID
        from NDS2_SEOD.SEOD_KNP
        where DECLARATION_REG_NUM = vDeclarationRegNum and ifns_code = vSono)
  loop
    /* Если пришла дата закрытия, то закрывает АТ */
    begin
      select max(completion_date) into vCloseDate from NDS2_SEOD.SEOD_KNP where knp_id = itemKNP.Knp_Id;

      if vCloseDate is null and vDate is not null  then

	    NDS2_MRR_USER.PAC$REMARKABLE_CHANGES.RESET_CHANGES(vDeclarationRegNum, vSono);

        update NDS2_SEOD.SEOD_KNP
            set completion_date = vDate
        where knp_id = itemKNP.Knp_Id;

        NDS2_MRR_USER.UTL_CLOSE_DOCUMENT(vDeclarationRegNum, vSono,  sysdate, REASON_DOC_CLOSE_BY_CLOSE_KNP);

      end if;
    exception when no_data_found then null;
    end;

    for line in (
        select
            itemKNP.Knp_Id as vKnpId,
            to_date(extractValue(value(act), 'АктКНП/@ДатаДок'),'DD.MM.YYYY')   as docDate,
            extractValue(value(act), 'АктКНП/@НомДок')                          as docNum,
            to_number(extractValue(value(act), 'АктКНП/@СумНеупл'))             as SumNeup,
            to_number(extractValue(value(act), 'АктКНП/@СумПени'))              as sumPeni,
            to_number(extractValue(value(act), 'АктКНП/@СумЗавВозм'))           as SumVozm
        from
            table(XMLSequence(pRawData.Extract('Файл/Документ/КНП/АктКНП'))) act)
    loop
      insert into NDS2_SEOD.SEOD_KNP_ACT (knp_id,doc_data,doc_num,sum_unpaid,sum_penalty,sum_overestimated)
      values (line.vKnpId, line.docDate, line.docNum, line.SumNeup, line.sumPeni, line.SumVozm);
    end loop;

    -- РешПриост
    for line in (select
            itemKNP.Knp_Id                                                                      as vKnpId,
            (select x.id from NDS2_SEOD.SEOD_KNP_DECISION_TYPE x where x.name = 'РешПриост')    as typeId,
            to_date(extractValue(value(t),'РешПриост/@ДатаДок'),'DD.MM.YYYY')                   as docDate,
            extractValue(value(t), 'РешПриост/@НомДок')                                         as docNum
           from table(XMLSequence(pRawData.Extract('Файл/Документ/КНП/РешПриост'))) t)
    loop
      insert into NDS2_SEOD.SEOD_KNP_DECISION (knp_id,type_id,doc_date,doc_num)
      values (line.vKnpId, line.typeId, line.docDate, line.docNum);
    end loop;

    -- РешПривлОтв
    for line in (
        select
            itemKNP.Knp_Id                                                                      as vKnpId,
            (select x.id from NDS2_SEOD.SEOD_KNP_DECISION_TYPE x where x.name = 'РешПривлОтв')  as typeId,
            to_date(extractValue(value(t),'РешПривлОтв/@ДатаДок'),'DD.MM.YYYY')                 as docDate,
            extractValue(value(t), 'РешПривлОтв/@НомДок')                                       as docNum,
            to_number(extractValue(value(t), 'РешПривлОтв/@СумНеупл'))                          as Amnt,
            to_number(extractValue(value(t), 'РешПривлОтв/@СумПени'))                           as Amnt2,
            to_number(extractValue(value(t), 'РешПривлОтв/@СумШтраф'))                          as Amnt3
        from table(XMLSequence(pRawData.Extract('Файл/Документ/КНП/РешПривлОтв'))) t)
    loop
      insert into NDS2_SEOD.SEOD_KNP_DECISION (knp_id,type_id,doc_date,doc_num, amount, amount2, amount3)
      values (line.vKnpId, line.typeId, line.docDate, line.docNum, line.amnt, line.amnt2, line.amnt3);
    end loop;

    -- РешОтказПривл
    for line in (
        select
            itemKNP.Knp_Id                                                                          as vKnpId,
            (select x.id from NDS2_SEOD.SEOD_KNP_DECISION_TYPE x where x.name = 'РешОтказПривл')    as typeId,
            to_date(extractValue(value(t),'РешОтказПривл/@ДатаДок'),'DD.MM.YYYY')                   as docDate,
            extractValue(value(t), 'РешОтказПривл/@НомДок')                                         as docNum,
            to_number(extractValue(value(t), 'РешОтказПривл/@СумНеупл'))                            as Amnt,
            to_number(extractValue(value(t), 'РешОтказПривл/@СумПени'))                             as Amnt2
        from table(XMLSequence(pRawData.Extract('Файл/Документ/КНП/РешОтказПривл'))) t)
    loop
      insert into NDS2_SEOD.SEOD_KNP_DECISION (knp_id,type_id,doc_date,doc_num, amount, amount2)
      values (line.vKnpId, line.typeId, line.docDate, line.docNum, line.amnt, line.amnt2);
    end loop;


    -- РешВозмНДС
    for line in (
        select
            itemKNP.Knp_Id                                                                      as vKnpId,
            (select x.id from NDS2_SEOD.SEOD_KNP_DECISION_TYPE x where x.name = 'РешВозмНДС')   as typeId,
            to_date(extractValue(value(t),'РешВозмНДС/@ДатаДок'),'DD.MM.YYYY')                  as docDate,
            extractValue(value(t), 'РешВозмНДС/@НомДок')                                        as docNum,
            to_number(extractValue(value(t), 'РешВозмНДС/@СумНДСВозм'))                         as Amnt
        from table(XMLSequence(pRawData.Extract('Файл/Документ/КНП/РешВозмНДС'))) t)
    loop
      insert into NDS2_SEOD.SEOD_KNP_DECISION (knp_id,type_id,doc_date,doc_num, amount)
      values (line.vKnpId, line.typeId, line.docDate, line.docNum, line.amnt);
    end loop;


    -- РешОтказВозмНДС
    for line in (
        select
            itemKNP.Knp_Id                                                                          as vKnpId,
            (select x.id from NDS2_SEOD.SEOD_KNP_DECISION_TYPE x where x.name = 'РешОтказВозмНДС')  as typeId,
            to_date(extractValue(value(t),'РешОтказВозмНДС/@ДатаДок'),'DD.MM.YYYY')                 as docDate,
            extractValue(value(t), 'РешОтказВозмНДС/@НомДок')                                       as docNum,
            to_number(extractValue(value(t), 'РешОтказВозмНДС/@СумОтказВозм'))                      as Amnt
        from table(XMLSequence(pRawData.Extract('Файл/Документ/КНП/РешОтказВозмНДС'))) t)
    loop
      insert into NDS2_SEOD.SEOD_KNP_DECISION (knp_id,type_id,doc_date,doc_num, amount)
      values (line.vKnpId, line.typeId, line.docDate, line.docNum, line.amnt);
    end loop;


    -- РешВозмНДСЗаявит
    for line in (
        select
            itemKNP.Knp_Id                                                                          as vKnpId,
            (select x.id from NDS2_SEOD.SEOD_KNP_DECISION_TYPE x where x.name = 'РешВозмНДСЗаявит') as typeId,
            to_date(extractValue(value(t),'РешВозмНДСЗаявит/@ДатаДок'),'DD.MM.YYYY')                as docDate,
            extractValue(value(t), 'РешВозмНДСЗаявит/@НомДок')                                      as docNum,
            to_number(extractValue(value(t), 'РешВозмНДСЗаявит/@СумНДСВозм'))                       as Amnt
        from table(XMLSequence(pRawData.Extract('Файл/Документ/КНП/РешВозмНДСЗаявит'))) t)
    loop
      insert into NDS2_SEOD.SEOD_KNP_DECISION (knp_id,type_id,doc_date,doc_num, amount)
      values (line.vKnpId, line.typeId, line.docDate, line.docNum, line.amnt);
    end loop;


    -- РешОтказВозмНДСЗаявит
    for line in (
        select
            itemKNP.Knp_Id                                                                                  as vKnpId,
            (select x.id from NDS2_SEOD.SEOD_KNP_DECISION_TYPE x where x.name = 'РешОтказВозмНДСЗаявит')    as typeId,
            to_date(extractValue(value(t),'РешОтказВозмНДСЗаявит/@ДатаДок'),'DD.MM.YYYY')                   as docDate,
            extractValue(value(t), 'РешОтказВозмНДСЗаявит/@НомДок')                                         as docNum,
            to_number(extractValue(value(t), 'РешОтказВозмНДСЗаявит/@СумОтказВозм'))                        as Amnt
        from table(XMLSequence(pRawData.Extract('Файл/Документ/КНП/РешОтказВозмНДСЗаявит'))) t)
    loop
      insert into NDS2_SEOD.SEOD_KNP_DECISION (knp_id,type_id,doc_date,doc_num, amount)
      values (line.vKnpId, line.typeId, line.docDate, line.docNum, line.amnt);
    end loop;

    -- РешОтказВозмНДСЗаявит
    for line in (
        select
            itemKNP.Knp_Id                                                                                  as vKnpId,
            (select x.id from NDS2_SEOD.SEOD_KNP_DECISION_TYPE x where x.name = 'РешОтменВозмНДСЗаявит')    as typeId,
            to_date(extractValue(value(t),'РешОтменВозмНДСЗаявит/@ДатаДок'),'DD.MM.YYYY')                   as docDate,
            extractValue(value(t), 'РешОтменВозмНДСЗаявит/@НомДок')                                         as docNum,
            to_number(extractValue(value(t), 'РешОтменВозмНДСЗаявит/@СумНДСВозмОтмен'))                     as Amnt
        from table(XMLSequence(pRawData.Extract('Файл/Документ/КНП/РешОтменВозмНДСЗаявит'))) t)
    loop
      insert into NDS2_SEOD.SEOD_KNP_DECISION (knp_id,type_id,doc_date,doc_num, amount)
      values (line.vKnpId, line.typeId, line.docDate, line.docNum, line.amnt);
    end loop;

    -- РешОтменВозмНДСУточн
    for line in (
        select
            itemKNP.Knp_Id                                                                              as vKnpId,
            (select x.id from NDS2_SEOD.SEOD_KNP_DECISION_TYPE x where x.name = 'РешОтменВозмНДСУточн') as typeId,
            to_date(extractValue(value(t),'РешОтменВозмНДСУточн/@ДатаДок'),'DD.MM.YYYY')                as docDate,
            extractValue(value(t), 'РешОтменВозмНДСУточн/@НомДок')                                      as docNum,
            to_number(extractValue(value(t), 'РешОтменВозмНДСУточн/@СумНДСВозмОтмен'))                  as Amnt
        from table(XMLSequence(pRawData.Extract('Файл/Документ/КНП/РешОтменВозмНДСУточн'))) t)
    loop
      insert into NDS2_SEOD.SEOD_KNP_DECISION (knp_id,type_id,doc_date,doc_num, amount)
      values (line.vKnpId, line.typeId, line.docDate, line.docNum, line.amnt);
    end loop;
  end loop;

  return true;
  exception when others then
    p_refuse_reason := substr(sqlerrm, 0, 256);
    log_err(v_log_scope, sqlcode, sqlerrm,DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
    return false;
end;

/*################################################################################*/
/*## ЭОД-5 #######################################################################*/
/*################################################################################*/
function CAM_05_01_WRITE
   (
      p_data in clob,
      p_id in number,
      p_sono_code in varchar2,
      p_eod_date in date,
      p_receipt_date in date,
      p_refuse_reason out varchar2
   )
  return boolean
  as
    v_log_scope constant varchar2(16):='CAM_05_01_WRITE';     
    v_xml xmltype;
    v_doc_num number(20);
    v_doc_accept_date date;
    v_job_id number;
    v_inn_contractor varchar2(12);
    v_kpp varchar2(9);
    v_type  number;
    v_period  varchar2(2);
    v_year  varchar2(4);
    v_doc_type  NDS2_MRR_USER.doc.doc_type%type;
  begin
    v_xml := xmltype(p_data);

    v_doc_accept_date := UTL_EXTRACT_ATTRIBUTE_DATE(v_xml, '/Файл/Документ/@ДатаПолучДан');

    if v_xml.existsNode('/Файл/Документ/Требование/АвтоТреб') = 1 then
      v_doc_num := UTL_EXTRACT_ATTRIBUTE_NUM(v_xml, '/Файл/Документ/Требование/АвтоТреб/@УчНомТреб');
    end if;

    if v_xml.existsNode('/Файл/Документ/Требование/АвтоИстреб') = 1 then
      v_doc_num := UTL_EXTRACT_ATTRIBUTE_NUM(v_xml, '/Файл/Документ/Требование/АвтоИстреб/@УчНомИстреб');
    end if;

    
	if F$CHECK_PROCESS_AVAILABILITY(v_doc_num) = 0 then
		return true;
	end if;
    
    select max(doc_type) 
      into v_doc_type 
      from NDS2_MRR_USER.DOC 
      where DOC_ID = v_doc_num;
    
    if v_doc_type is null then
        log_err(v_log_scope, sqlcode,  'не найдено требование с номером '||v_doc_num );
        p_refuse_reason := 'не найдено требование с номером '||v_doc_num;
        return false;
    end if;

    if v_doc_type = 1 then -- если АТ по СФ, то меняем статус на "Получено СЭОД"
      insert into NDS2_MRR_USER.doc_status_history(doc_id, status, status_date)
        values(v_doc_num, 6, sysdate);

      update NDS2_MRR_USER.doc d 
        set d.seod_accepted = 1, 
            d.seod_accept_date = v_doc_accept_date, 
            d.status = 6, 
            d.knp_sync_status = decode(d.knp_sync_status, 0, 1, d.knp_sync_status) 
        where d.doc_id = v_doc_num; 
    else 
      update NDS2_MRR_USER.doc d 
        set d.seod_accepted = 1, 
            d.seod_accept_date = v_doc_accept_date 
        where d.doc_id = v_doc_num;
    end if;

    /* Calc v_nds2_id */
    select nvl(decl.innreorg,SD.INN), case when decl.innreorg is null then sd.kpp else decl.kppreorg end, SD.TYPE, SD.TAX_PERIOD, SD.FISCAL_YEAR
    into
        v_inn_contractor,
        v_kpp,
        v_type,
        v_period,
        v_year
    from NDS2_SEOD.SEOD_DECLARATION SD
	join NDS2_MRR_USER.V$ASK_DECLANDJRNL decl on decl.idfajl = sd.id_file and sd.inn = decl.innnp and sd.tax_period = decl.period and sd.fiscal_year = decl.otchetgod and sd.correction_number = decl.nomkorr
    inner join NDS2_MRR_USER.DOC D 
        ON SD.decl_reg_num = D.REF_ENTITY_ID 
        and SD.sono_code = D.Sono_Code
    Where D.DOC_ID = v_doc_num;
    
    if SQL%ROWCOUNT = 0
      then
        log_err(v_log_scope, sqlcode,  'не найден ИД декларации для требования '||v_doc_num );
        p_refuse_reason := 'не найден ИД декларации для требования '||v_doc_num;
        return false;
    end if;

    NDS2_MRR_USER.PAC$ASYNC_SCHEDULER.P$SUBMIT_TASK_RECLAIMATION( v_doc_num );
    
    if v_doc_type = 1 then
       NDS2_MRR_USER.PAC$REMARKABLE_CHANGES.SET_HAS_AT_ASYNC(v_inn_contractor,v_kpp,v_type,v_period,v_year);
    end if;

    return true;

    exception when others then
        p_refuse_reason := substr(sqlerrm, 0, 256);
        log_err(v_log_scope, sqlcode, sqlerrm,DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
        return false;
  end;

/*################################################################################*/
/*## ЭОД-6 #######################################################################*/
/*################################################################################*/
function CAM_06_01_WRITE
  (
    p_data in clob, p_id in number,
    p_sono_code in varchar2,
    p_eod_date in date,
    p_receipt_date in date,
    p_refuse_reason out varchar2
  )
  return boolean
  as
    v_log_scope constant varchar2(16):='CAM_06_01_WRITE';     
    v_xml xmltype;
    v_doc_id number(19);
    v_seod_doc_num varchar2(128);
    v_seod_doc_date date;
    v_send_date date;
    v_delivery_date date;
    v_delivery_method varchar(2);
    v_status number(1);
  begin
    v_xml             := xmltype(p_data);
    v_doc_id          := UTL_EXTRACT_ATTRIBUTE_NUM(v_xml, '/Файл/Документ/@УчНомИстреб');
    v_seod_doc_num    := UTL_EXTRACT_ATTRIBUTE_STRING(v_xml, '/Файл/Документ/ИсТреб/@НомДок');
    v_seod_doc_date   := UTL_EXTRACT_ATTRIBUTE_DATE(v_xml, '/Файл/Документ/ИсТреб/@ДатаДок');
    v_send_date       := UTL_EXTRACT_ATTRIBUTE_DATE(v_xml, '/Файл/Документ/ИсТреб/@ДатаОтправ');
    v_delivery_date   := UTL_EXTRACT_ATTRIBUTE_DATE(v_xml, '/Файл/Документ/ИсТреб/@ДатаВруч');
    v_delivery_method := UTL_EXTRACT_ATTRIBUTE_STRING(v_xml, '/Файл/Документ/ИсТреб/@СпВруч');

    if F$CHECK_PROCESS_AVAILABILITY(v_doc_id) = 0 then
		return true;
	end if;

	/*отправлено налогоплательщику*/
    if v_send_date is not null then
      v_status := 4;
      insert into NDS2_MRR_USER.doc_status_history(doc_id, status, status_date)
      values(v_doc_id, v_status, v_send_date);
    end if;
    /*вручено налогоплательщику*/
    if v_delivery_date is not null then
      v_status := 5;
      insert into NDS2_MRR_USER.doc_status_history(doc_id, status, status_date)
      values(v_doc_id, v_status, v_delivery_date);
    end if;

    update NDS2_MRR_USER.doc d
    set
      d.tax_payer_send_date = nvl(v_send_date, d.tax_payer_send_date),
      d.external_doc_date = nvl(v_seod_doc_date, d.external_doc_date),
      d.tax_payer_delivery_date = nvl(v_delivery_date, d.tax_payer_delivery_date),
      d.tax_payer_delivery_method = nvl(v_delivery_method, d.tax_payer_delivery_method),
      d.external_doc_num = nvl(v_seod_doc_num, d.external_doc_num),
      d.status = nvl(v_status, d.status),
      d.status_date = trunc(sysdate) 
    where d.doc_id = v_doc_id;

    if v_status in (4,5) then
      NDS2_MRR_USER.PAC$ASYNC_SCHEDULER.P$CLOSE_RECLAMATION_BY_EOD(v_doc_id, v_status);
    end if;

    return true;

    exception when others then
        p_refuse_reason := substr(sqlerrm, 0, 256);
        log_err(v_log_scope, sqlcode, sqlerrm,DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
        return false;
  end;

/*################################################################################*/
/*## ЭОД-7 #######################################################################*/
/*################################################################################*/
function CAM_07_01_WRITE
  (
    p_data in clob,
    p_id in number,
    p_sono_code in varchar2,
    p_eod_date in date,
    p_receipt_date in date,
    p_refuse_reason out varchar2
  )
  return boolean
  as
    v_log_scope constant varchar2(16):='CAM_07_01_WRITE';   
    v_xml xmltype;
    v_doc_id number(20);
    v_doc_num varchar2(100);
    v_doc_date date;
    v_prolong_date date;
  begin
    v_xml          := xmltype(p_data);
    v_doc_id       := UTL_EXTRACT_ATTRIBUTE_NUM(v_xml, '/Файл/Документ/@УчНомИстреб');
    v_doc_num      := UTL_EXTRACT_ATTRIBUTE_STRING(v_xml, '/Файл/Документ/РешПродлСрок/@НомДок');
    v_doc_date     := UTL_EXTRACT_ATTRIBUTE_DATE(v_xml, '/Файл/Документ/РешПродлСрок/@ДатаДок');
    v_prolong_date := UTL_EXTRACT_ATTRIBUTE_DATE(v_xml, '/Файл/Документ/РешПродлСрок/@ДатаПродлСрок');

    if F$CHECK_PROCESS_AVAILABILITY(v_doc_id) = 0 then
		return true;
	end if;

	merge into NDS2_SEOD.seod_prolong_decision pd
    using
    (
      select
        v_doc_id as doc_id,
        v_doc_num as doc_num,
        v_doc_date as doc_date,
        v_prolong_date as prolong_date
      from dual
    ) d on (d.doc_id = pd.doc_id)
    when matched then update set
      pd.doc_num = nvl(d.doc_num, pd.doc_num),
      pd.doc_date = nvl(d.doc_date, pd.doc_date),
      pd.prolong_date = nvl(d.prolong_date, pd.prolong_date)
    when not matched then insert (pd.doc_id, pd.doc_num, pd.doc_date, pd.prolong_date)
      values (d.doc_id, d.doc_num, d.doc_date, d.prolong_date);

    update NDS2_MRR_USER.doc d
    set d.prolong_answer_date = nvl(v_prolong_date, d.prolong_answer_date)
    where d.doc_id = v_doc_id;

    log_info(v_log_scope, 'success doc_id:='||v_doc_id);
    return true;

    exception when others then
        p_refuse_reason := substr(sqlerrm, 0, 256);
        log_err(v_log_scope, sqlcode, sqlerrm,DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
        return false;
  end;

/*################################################################################*/
/*## ЭОД-8 #######################################################################*/
/*################################################################################*/
function CAM_08_01_WRITE
  (
    p_data in clob,
    p_id in number,
    p_sono_code in varchar2,
    p_eod_date in date,
    p_receipt_date in date,
    p_refuse_reason out varchar2
  )
  return boolean
  as
    v_log_scope constant varchar2(16):='CAM_08_01_WRITE';     
    v_xml xmltype;
    v_doc_id number(20);
    v_reply_date date;
    v_explain_id number;
  begin
    v_xml        := xmltype(p_data);
    v_doc_id     := UTL_EXTRACT_ATTRIBUTE_NUM(v_xml, '/Файл/Документ/@УчНомИстреб');
    v_reply_date := UTL_EXTRACT_ATTRIBUTE_DATE(v_xml, '/Файл/Документ/ОтвТреб93/@ДатаОтв');


	if F$CHECK_PROCESS_AVAILABILITY(v_doc_id) = 0 then
		return true;
	end if;

	v_explain_id := CREATE_EXPLAIN(v_doc_id, 2, null, v_reply_date, null, null);

    NDS2_MRR_USER.PAC$ASYNC_SCHEDULER.P$SUBMIT_TASK_RECLAIM_REPLY(p_doc_id => v_doc_id, p_explain_id => v_explain_id);

    return true;
    exception when others then
        p_refuse_reason := substr(sqlerrm, 0, 256);
        log_err(v_log_scope, sqlcode, sqlerrm,DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
        return false;
  end;

/*################################################################################*/
/*## ЭОД-9 #######################################################################*/
/*################################################################################*/
function CAM_09_01_WRITE
  (
    p_data in clob,
    p_id in number,
    p_sono_code in varchar2,
    p_eod_date in date,
    p_receipt_date in date,
    p_refuse_reason out varchar2
  )
  return boolean
  as
    v_log_scope constant varchar2(16):='CAM_09_01_WRITE';      
    v_xml xmltype;
    v_doc_id number(20);
    v_doc_num varchar2(100);
    v_doc_date date;
    v_executive_soun_send_date date;
    v_executive_soun_recieve_date date;
    v_reject_reason varchar2(2);
    v_status number(1);
  begin
    v_xml                         := xmltype(p_data);
    v_doc_id                      := UTL_EXTRACT_ATTRIBUTE_NUM(v_xml, '/Файл/Документ/@УчНомИстреб');
    v_doc_num                     := UTL_EXTRACT_ATTRIBUTE_STRING(v_xml, '/Файл/Документ/Поруч/@НомПоруч');
    v_reject_reason               := UTL_EXTRACT_ATTRIBUTE_STRING(v_xml, '/Файл/Документ/Поруч/@ПричОтказ');
    v_doc_date                    := UTL_EXTRACT_ATTRIBUTE_DATE(v_xml, '/Файл/Документ/Поруч/@ДатаПоруч');
    v_executive_soun_send_date    := UTL_EXTRACT_ATTRIBUTE_DATE(v_xml, '/Файл/Документ/Поруч/@ДатаНапрПоруч');
    v_executive_soun_recieve_date := UTL_EXTRACT_ATTRIBUTE_DATE(v_xml, '/Файл/Документ/Поруч/@ДатаПолучПоруч');
    v_status := 0;

    if F$CHECK_PROCESS_AVAILABILITY(v_doc_id) = 0 then
		return true;
	end if;

	/*Дата направления Поручения в НО-исполнитель*/
    if v_executive_soun_send_date is not null then
      v_status := 6;
      insert into NDS2_MRR_USER.doc_status_history(doc_id, status, status_date)
      values(v_doc_id, v_status, v_executive_soun_send_date);

	  update NDS2_MRR_USER.doc
		set status = v_status
		   ,status_date = trunc(v_executive_soun_send_date)
		where doc_id = v_doc_id;
    end if;
    /*Дата получения Поручения НО-исполнителем*/
    if v_executive_soun_recieve_date is not null then
      v_status := 7;
      insert into NDS2_MRR_USER.doc_status_history(doc_id, status, status_date)
      values(v_doc_id, v_status, v_executive_soun_recieve_date);

	  update NDS2_MRR_USER.doc
		set status = v_status
		   ,status_date = trunc(v_executive_soun_recieve_date)
		where doc_id = v_doc_id;
    end if;

    merge into NDS2_SEOD.seod_reclaim_order ro
    using
    (
      select
        v_doc_id as doc_id,
        v_doc_num as doc_num,
        v_reject_reason as reject_reason,
        v_doc_date as doc_date,
        v_executive_soun_send_date as executive_soun_send_date,
        v_executive_soun_recieve_date as executive_soun_recieve_date
      from dual
    ) d on (d.doc_id = ro.doc_id)
    when matched then update set
      ro.doc_num = nvl(d.doc_num, ro.doc_num),
      ro.reject_reason = nvl(d.reject_reason, ro.reject_reason),
      ro.doc_date = nvl(d.doc_date, ro.doc_date),
      ro.executive_soun_send_date = nvl(d.executive_soun_send_date, ro.executive_soun_send_date),
      ro.executive_soun_recieve_date = nvl(d.executive_soun_recieve_date, ro.executive_soun_recieve_date)
    when not matched then insert (ro.doc_id, ro.doc_num, ro.doc_date, ro.executive_soun_send_date, ro.executive_soun_recieve_date, ro.reject_reason)
      values (d.doc_id, d.doc_num, d.doc_date, d.executive_soun_send_date, d.executive_soun_recieve_date, d.reject_reason);

    NDS2_MRR_USER.PAC$ASYNC_SCHEDULER.P$CLOSE_RECLAMATION_BY_EOD(p_doc_id => v_doc_id, p_by_status => v_status);

    return true;
    exception when others then
        p_refuse_reason := substr(sqlerrm, 0, 256);
        log_err(v_log_scope, sqlcode, sqlerrm,DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
        return false;
  end;

/*################################################################################*/
/*## ЭОД-10 ######################################################################*/
/*################################################################################*/
function CAM_10_01_WRITE
  (
    p_data in clob,
    p_id in number,
    p_sono_code in varchar2,
    p_eod_date in date,
    p_receipt_date in date,
    p_refuse_reason out varchar2
  )
  return boolean
  as
    v_log_scope constant varchar2(16):='CAM_10_01_WRITE';    
    v_xml xmltype;
    v_doc_id number(20);
    v_reply_date date;
    v_executor_recive_date date;
    v_initiator_send_date date;
    v_explain_id number;
  begin
    v_xml                  := xmltype(p_data);
    v_doc_id               := UTL_EXTRACT_ATTRIBUTE_NUM(v_xml, '/Файл/Документ/@УчНомИстреб');
    v_reply_date           := UTL_EXTRACT_ATTRIBUTE_DATE(v_xml, '/Файл/Документ/ОтвНП93.1/@ДатаОтв');
    v_executor_recive_date := UTL_EXTRACT_ATTRIBUTE_DATE(v_xml, '/Файл/Документ/ОтвНП93.1/@ВхДатаОтвИсп');
    v_initiator_send_date  := UTL_EXTRACT_ATTRIBUTE_DATE(v_xml, '/Файл/Документ/ОтвНП93.1/@ИсхДатаОтвИсп');

    if F$CHECK_PROCESS_AVAILABILITY(v_doc_id) = 0 then
		return true;
	end if;

	v_explain_id := CREATE_EXPLAIN(v_doc_id, 3, null, v_reply_date, v_executor_recive_date, v_initiator_send_date);

    NDS2_MRR_USER.PAC$ASYNC_SCHEDULER.P$SUBMIT_TASK_RECLAIM_REPLY(p_doc_id => v_doc_id, p_explain_id => v_explain_id);

    return true;
    exception when others then
        p_refuse_reason := substr(sqlerrm, 0, 256);
        log_err(v_log_scope, sqlcode, sqlerrm,DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
        return false;
  end;

/*################################################################################*/
/*## ЭОД-11 ######################################################################*/
/*################################################################################*/
function CAM_11_01_WRITE
  (
    p_data in clob,
    p_id in number,
    p_sono_code in varchar2,
    p_eod_date in date,
    p_receipt_date in date,
    p_refuse_reason out varchar2
  )
  return boolean
  as
    v_log_scope constant varchar2(16):='CAM_11_01_WRITE';    
    v_xml xmltype;
    v_inn varchar2(12 char);
    v_kpp varchar2(9 char);
    v_soun_code varchar2(4 char);
    v_tax_payer_type number(1);
    v_tax_payer_id number;
    
    v_tax_period varchar2(2 char);
    v_fiscal_year varchar2(4 char);
    v_reg_num number;
    v_fid_journal varchar2(36 char);
    v_submission_date date;
    v_file_id varchar2(512 char);
    v_nds2_id number;
    v_deliveryType number(1) := 1;
    
    r_id number;
    r_decl_reg_num number(20);
    r_seod_reg_num number;
    r_seod_reg_number_cancelled number;
    r_exist number(1);
	r_inn varchar2(12 char);
  begin
    v_xml            := xmltype(p_data);
    v_file_id        := UTL_EXTRACT_ATTRIBUTE_STRING(v_xml, '/Файл/@ИдФайл');
    v_soun_code      := UTL_EXTRACT_ATTRIBUTE_STRING(v_xml, '/Файл/Документ/@КодНО');
    v_fid_journal    := UTL_EXTRACT_ATTRIBUTE_STRING(v_xml, '/Файл/Документ/@ФидЖурнал');
    v_reg_num        := UTL_EXTRACT_ATTRIBUTE_NUM(v_xml, '/Файл/Документ/@РегНомЖурнал');
    v_tax_period     := UTL_EXTRACT_ATTRIBUTE_STRING(v_xml, '/Файл/Документ/@Период');
    v_fiscal_year    := UTL_EXTRACT_ATTRIBUTE_STRING(v_xml, '/Файл/Документ/@ОтчетГод');
    v_submission_date := nvl(to_date(UTL_EXTRACT_ATTRIBUTE_STRING(v_xml, '/Файл/Документ/@ДатаПостНО'), 'DD.MM.YYYY'), sysdate);
    v_tax_payer_type := v_xml.existsNode('/Файл/Документ/СвНП/НПЮЛ');

    if v_tax_payer_type = 1 then
       v_inn := v_xml.extract('/Файл/Документ/СвНП/НПЮЛ/@ИННЮЛ').getStringVal();
       v_kpp := v_xml.extract('/Файл/Документ/СвНП/НПЮЛ/@КПП').getStringVal();
    else
       v_inn := v_xml.extract('/Файл/Документ/СвНП/НПФЛ/@ИННФЛ').getStringVal();
    end if;

    v_nds2_id := to_number(v_tax_period || v_fiscal_year || v_inn);

    if length(v_file_id) > 5 then
       v_deliveryType := 0;
    end if;

    merge into NDS2_SEOD.seod_declaration seod_decl
    using (select v_reg_num as reg_num,
              v_inn as inn,
              nvl(v_kpp, '0') as kpp,
              v_soun_code as soun_code
              from dual
           ) jrnl
      on
      (
        jrnl.reg_num = seod_decl.decl_reg_num
        and jrnl.inn = seod_decl.inn
        and jrnl.kpp = seod_decl.kpp
        and jrnl.soun_code = seod_decl.sono_code
      )
    when not matched then
      insert (id, nds2_id, type, sono_code, tax_period, fiscal_year, correction_number, decl_reg_num, decl_fid, tax_payer_type, inn, kpp, insert_date, eod_date, date_receipt, submission_date, delivery_type, Id_file)
      values(p_id, v_nds2_id, 1, v_soun_code, v_tax_period, v_fiscal_year, 0, v_reg_num, v_fid_journal, v_tax_payer_type, v_inn, v_kpp, sysdate, p_eod_date, p_receipt_date, v_submission_date, v_deliveryType, v_file_id);

    select rn.ID, rn.DECL_REG_NUM, rn.INN 
      into r_id, r_decl_reg_num, r_inn 
      from dual 
      left join NDS2_SEOD.SEOD_REG_NUMBER rn 
        on rn.INN = v_inn 
        and rn.TAX_PERIOD = v_tax_period 
        and rn.FISCAL_YEAR = v_fiscal_year 
        and rn.SONO_CODE = p_sono_code 
        and rn.CORRECTION_NUMBER = 0 
        and rn.TYPE = 1 
        and ((rn.ID_FILE is null and v_file_id is null) 
          or (rn.ID_FILE = '' and v_file_id is null) 
          or (rn.ID_FILE is null and v_file_id = '') 
          or (rn.ID_FILE = v_file_id)); -- нельзя использовать сравнение nvl(rn.ID_FILE, '') = nvl(v_file_id, ''), так как nvl для переменной равной NULL и поля таблицы, содержащего NULL работает по разному

    if (r_id is null) then
    begin
      r_seod_reg_num := NDS2_SEOD.SEQ_SEOD_REG_NUMBER.NEXTVAL(); 
      insert into NDS2_SEOD.SEOD_REG_NUMBER ("ID", DECL_REG_NUM, INN, TAX_PERIOD, FISCAL_YEAR, SONO_CODE, CORRECTION_NUMBER, "TYPE", ID_FILE, EOD_DATE, SUBMISSION_DATE, INSERT_DATE)
      values (r_seod_reg_num, v_reg_num, v_inn, v_tax_period, v_fiscal_year, p_sono_code, 0, 1, v_file_id, p_eod_date, v_submission_date, sysdate);

    end; 
    elsif (r_decl_reg_num != v_reg_num) then
    begin   
      update NDS2_SEOD.SEOD_REG_NUMBER set
        DECL_REG_NUM = v_reg_num 
      where ID = r_id;

      r_seod_reg_number_cancelled := NDS2_SEOD.SEQ_SEOD_REG_NUMBER_CANCELLED.NEXTVAL(); 
      insert into NDS2_SEOD.SEOD_REG_NUMBER_CANCELLED ("ID", SONO_CODE, DECL_REG_NUM, NEW_DECL_REG_NUM, CANCELLED_AT)
      values (r_seod_reg_number_cancelled, p_sono_code, r_decl_reg_num, v_reg_num, sysdate);

      select case when exists(select REF_ENTITY_ID from NDS2_MRR_USER.doc where REF_ENTITY_ID = r_decl_reg_num
      and SONO_CODE = p_sono_code and INN = r_inn) then 1 else 0 end into r_exist from dual;
      if (r_exist = 1) then      
        update NDS2_MRR_USER.doc set
          REF_ENTITY_ID = v_reg_num
        where REF_ENTITY_ID = r_decl_reg_num and SONO_CODE = p_sono_code and INN = r_inn; 
      end if;

      select case when exists(select DECLARATION_REG_NUM from NDS2_SEOD.seod_knp where DECLARATION_REG_NUM = r_decl_reg_num
      and IFNS_CODE = p_sono_code) then 1 else 0 end into r_exist from dual;
      if (r_exist = 1) then
        update NDS2_SEOD.seod_knp set
          DECLARATION_REG_NUM = v_reg_num
        where DECLARATION_REG_NUM = r_decl_reg_num and IFNS_CODE = p_sono_code;
      end if; 

    end;
    end if;

    /* Obsolette : P$RAISE_DECLARATION_CHANGED(v_nds2_id); */
    return true;
    exception when others then
        p_refuse_reason := substr(sqlerrm, 0, 256);
        log_err(v_log_scope, sqlcode, sqlerrm,DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);

        return false;
  end;

/*################################################################################*/
/*## ЭОД-13 ######################################################################*/
/*################################################################################*/
function CAM_13_01_WRITE
  (
    p_data in clob,
    p_id in number,
    p_reg_number in number,
    p_sono_code in varchar2,
    p_eod_date in date,
    p_receipt_date in date,
    p_refuse_reason out varchar2
  )
  return boolean as
    v_log_scope constant varchar2(16):='CAM_13_01_WRITE';      
    v_xml xmltype;
    v_sono_code varchar2(4 char);
    v_doc_id number;
    v_number number;
    v_date date;
    v_state varchar2(1 char);
  begin
    v_xml            := xmltype(p_data);
    
    v_sono_code      := UTL_EXTRACT_ATTRIBUTE_STRING(v_xml, '/Файл/Документ/@КодНО');
    v_doc_id         := UTL_EXTRACT_ATTRIBUTE_NUM(v_xml, '/Файл/Документ/ЗАПНОВЫПИС/@ИдДок');
    v_number         := UTL_EXTRACT_ATTRIBUTE_NUM(v_xml, '/Файл/Документ/ЗАПНОВЫПИС/@НомЗапр');
    v_date           := UTL_EXTRACT_ATTRIBUTE_DATE(v_xml, '/Файл/Документ/ЗАПНОВЫПИС/@ДатаЗапр');
    v_state          := UTL_EXTRACT_ATTRIBUTE_STRING(v_xml, '/Файл/Документ/ЗАПНОВЫПИС/@СостЗапр');

    update NDS2_MRR_USER.bank_account_request bar
    set bar.status_code = 4 + v_state,
        bar.reg_number = v_number,
        bar.seod_date = v_date
    where bar.doc_id = v_doc_id;

    nds2_mrr_user.pac$bank_accounts.p$log(0, v_log_scope, '/Файл/Документ/ЗАПНОВЫПИС/@СостЗапр = '||v_state);

    return true;

    exception when others then
        p_refuse_reason := substr(sqlerrm, 0, 256);
        log_err(v_log_scope, sqlcode, sqlerrm,DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
        return false;
  end;

/*################################################################################*/
/*## ЭОД-13 ######################################################################*/
/*################################################################################*/
function CAM_14_01_WRITE
  (
    p_data in clob,
    p_id in number,
    p_reg_number in number,
    p_sono_code in varchar2,
    p_eod_date in date,
    p_receipt_date in date,
    p_refuse_reason out varchar2
  )
  return boolean as
    v_log_scope constant varchar2(16):='CAM_14_01_WRITE';   
    v_xml xmltype;
    v_inn varchar2(12);
	v_kpp varchar2(9);
	v_sono_code varchar2(4);
    v_fiscal_year number;
	v_period_code varchar2(2);
	v_correction_number number;
	v_id_file varchar2(1024);
	v_reg_num number;
	v_canceled_since date;
	v_receive_at date;
    v_id number;
	v_reason_id number;
	v_knp_reopen_needed number;
  begin

    v_xml := xmltype(p_data);
    if (v_xml.existsNode('/Файл/Документ/СвНП/НПЮЛ') = 1) then
        v_inn := UTL_EXTRACT_ATTRIBUTE_STRING(v_xml, '/Файл/Документ/СвНП/НПЮЛ/@ИННЮЛ');
        v_kpp := UTL_EXTRACT_ATTRIBUTE_STRING(v_xml, '/Файл/Документ/СвНП/НПЮЛ/@КПП');
    else
        v_inn := UTL_EXTRACT_ATTRIBUTE_STRING(v_xml, '/Файл/Документ/СвНП/НПФЛ/@ИННФЛ');
    end if;
	v_sono_code := UTL_EXTRACT_ATTRIBUTE_STRING(v_xml, '/Файл/Документ/@КодНО');
	v_fiscal_year := UTL_EXTRACT_ATTRIBUTE_STRING(v_xml, '/Файл/Документ/@ОтчетГод');
	v_period_code := UTL_EXTRACT_ATTRIBUTE_STRING(v_xml, '/Файл/Документ/@Период');
	v_correction_number := UTL_EXTRACT_ATTRIBUTE_STRING(v_xml, '/Файл/Документ/@НомКорр');
	v_id_file := UTL_EXTRACT_ATTRIBUTE_STRING(v_xml, '/Файл/@ИдФайл');
	v_reg_num := UTL_EXTRACT_ATTRIBUTE_STRING(v_xml, '/Файл/Документ/@РегНомДек');
	v_canceled_since := UTL_EXTRACT_ATTRIBUTE_DATE(v_xml, '/Файл/Документ/@ДатаАннул');
	v_receive_at := sysdate;
	v_reason_id := UTL_EXTRACT_ATTRIBUTE_STRING(v_xml, '/Файл/Документ/@ПричинаАннул');
	v_knp_reopen_needed := UTL_EXTRACT_ATTRIBUTE_NUM(v_xml, '/Файл/Документ/@Признак');

	v_id := nds2_seod.seq_seod_declaration_annulment.nextval();

	insert into nds2_seod.seod_declaration_annulment(
		ID, INN, KPP, SONO_CODE, FISCAL_YEAR, PERIOD_CODE, CORRECTION_NUMBER, ID_FILE, REG_NUMBER, CANCELLED_SINCE, RECEIVED_AT, REASON_ID, KNP_REOPEN_NEEDED) 
	values (v_id, v_inn, v_kpp, v_sono_code, v_fiscal_year, v_period_code, v_correction_number, v_id_file, v_reg_num, v_canceled_since, v_receive_at, v_reason_id, v_knp_reopen_needed);

	nds2_mrr_user.PAC$SEOD.P$REGISTER_DECL_ANNULMENT(v_id);

	return true;

    exception when others then
        p_refuse_reason := substr(sqlerrm, 0, 256);
        log_err(v_log_scope, sqlcode, sqlerrm,DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
        return false;
  end;

/*################################################################################*/
/*## Обработка потоков ЭОД #######################################################*/
/*################################################################################*/
procedure P$PROCESS_NEW_SEOD_DATA
(
   pSessionId number
)
as
pragma autonomous_transaction;
    v_log_scope constant varchar2(22):='PROCESS_NEW_SEOD_DATA';
    v_result boolean;
    v_result_message varchar2(1024 char);
begin
    /* for correct processing to_number from xs:decimal */
    execute immediate 'alter session set NLS_NUMERIC_CHARACTERS = ''. ''';

    log_info(v_log_scope,'Начало обработки потоков ЭОД с session_id = '||pSessionId);

    for line in (select * from SEOD_INCOMING t where t.processing_result = 0 and session_id = pSessionId order by t.info_type) 
    loop
        begin
            savepoint s;
              v_result_message := null;

            /*########################################################################################################################*/
            /*#идентификационные данные по налоговой декларации по налогу на добавленную стоимость, передаваемые из СЭОД в АСК НДС-2.#*/
            /*########################################################################################################################*/
              if line.info_type = 'CAM_NDS2_01' then
                log_info(v_log_scope,'line : CAM_NDS2_01');

                v_result := CAM_01_01_WRITE(
                  p_data => line.xml_data,
                  p_id => line.eod_id,
                  p_sono_code => line.code_nsi_sono,
                  p_eod_date => line.date_eod,
                  p_receipt_date => line.date_receipt,
                  p_refuse_reason => v_result_message);
              end if;

            /*################################################################################*/
            /*#данные по статусам исполнения автотребования, передаваемые из СЭОД в АСК НДС-2#*/
            /*################################################################################*/
              if line.info_type = 'CAM_NDS2_02' then
                log_info(v_log_scope,'line : CAM_NDS2_02');

                v_result := CAM_02_01_WRITE(
                  p_data => line.xml_data,
                  p_id => line.eod_id,
                  p_sono_code => line.code_nsi_sono,
                  p_eod_date => line.date_eod,
                  p_receipt_date => line.date_receipt,
                  p_refuse_reason => v_result_message);
              end if;

            /*#########################################################################################*/
            /*#данные об ответе налогоплательщика на автотребование, передаваемые из СЭОД в АСК НДС-3.#*/
            /*#########################################################################################*/
              if line.info_type = 'CAM_NDS2_03' then
                log_info(v_log_scope,'line : CAM_NDS2_03');

                v_result := CAM_03_01_WRITE(
                  p_data => line.xml_data,
                  p_id => line.eod_id,
                  p_sono_code => line.code_nsi_sono,
                  p_eod_date => line.date_eod,
                  p_receipt_date => line.date_receipt,
                  p_refuse_reason => v_result_message);
              end if;

            /*########################################################################################################################*/
            /*#Ход КНП (документы, закрытие КНП)                                                                                     #*/
            /*########################################################################################################################*/
              if line.info_type = 'CAM_NDS2_04' then
                log_info(v_log_scope,'line : CAM_NDS2_04');

                v_result := CAM_04_01_WRITE(
                  p_data => line.xml_data,
                  p_id => line.eod_id,
                  p_sono_code => line.code_nsi_sono,
                  p_eod_date => line.date_eod,
                  p_receipt_date => line.date_receipt,
                  p_refuse_reason => v_result_message);
              end if;

              if line.info_type = 'CAM_NDS2_05' then
                log_info(v_log_scope,'line : CAM_NDS2_05');

                v_result := CAM_05_01_WRITE(
                  p_data => line.xml_data,
                  p_id => line.eod_id,
                  p_sono_code => line.code_nsi_sono,
                  p_eod_date => line.date_eod,
                  p_receipt_date => line.date_receipt,
                  p_refuse_reason => v_result_message);
              end if;

              if line.info_type = 'CAM_NDS2_06' then
                log_info(v_log_scope,'line : CAM_NDS2_06');

                v_result := CAM_06_01_WRITE(
                  p_data => line.xml_data,
                  p_id => line.eod_id,
                  p_sono_code => line.code_nsi_sono,
                  p_eod_date => line.date_eod,
                  p_receipt_date => line.date_receipt,
                  p_refuse_reason => v_result_message);
              end if;

              if line.info_type = 'CAM_NDS2_07' then
                log_info(v_log_scope,'line : CAM_NDS2_07');

                v_result := CAM_07_01_WRITE(
                  p_data => line.xml_data,
                  p_id => line.eod_id,
                  p_sono_code => line.code_nsi_sono,
                  p_eod_date => line.date_eod,
                  p_receipt_date => line.date_receipt,
                  p_refuse_reason => v_result_message);
              end if;

              if line.info_type = 'CAM_NDS2_08' then
                log_info(v_log_scope,'line : CAM_NDS2_08');

                v_result := CAM_08_01_WRITE(
                  p_data => line.xml_data,
                  p_id => line.eod_id,
                  p_sono_code => line.code_nsi_sono,
                  p_eod_date => line.date_eod,
                  p_receipt_date => line.date_receipt,
                  p_refuse_reason => v_result_message);
              end if;

              if line.info_type = 'CAM_NDS2_09' then
                log_info(v_log_scope,'line : CAM_NDS2_09');

                v_result := CAM_09_01_WRITE(
                  p_data => line.xml_data,
                  p_id => line.eod_id,
                  p_sono_code => line.code_nsi_sono,
                  p_eod_date => line.date_eod,
                  p_receipt_date => line.date_receipt,
                  p_refuse_reason => v_result_message);
              end if;

              if line.info_type = 'CAM_NDS2_10' then
                log_info(v_log_scope,'line : CAM_NDS2_10');

                v_result := CAM_10_01_WRITE(
                  p_data => line.xml_data,
                  p_id => line.eod_id,
                  p_sono_code => line.code_nsi_sono,
                  p_eod_date => line.date_eod,
                  p_receipt_date => line.date_receipt,
                  p_refuse_reason => v_result_message);
              end if;

              if line.info_type = 'CAM_NDS2_11' then
                log_info(v_log_scope,'line : CAM_NDS2_11');

                v_result := CAM_11_01_WRITE(
                  p_data => line.xml_data,
                  p_id => line.eod_id,
                  p_sono_code => line.code_nsi_sono,
                  p_eod_date => line.date_eod,
                  p_receipt_date => line.date_receipt,
                  p_refuse_reason => v_result_message);
              end if;

              if line.info_type = 'CAM_NDS2_13' then
                log_info(v_log_scope,'line : CAM_NDS2_13');

                v_result := CAM_13_01_WRITE(
                  p_data => line.xml_data,
                  p_id => line.eod_id,
                  p_reg_number => line.reg_number,
                  p_sono_code => line.code_nsi_sono,
                  p_eod_date => line.date_eod,
                  p_receipt_date => line.date_receipt,
                  p_refuse_reason => v_result_message);
              end if;

              if line.info_type = 'CAM_NDS2_14' then
                log_info(v_log_scope,'line : CAM_NDS2_14');

                v_result := CAM_14_01_WRITE(
                  p_data => line.xml_data,
                  p_id => line.eod_id,
                  p_reg_number => line.reg_number,
                  p_sono_code => line.code_nsi_sono,
                  p_eod_date => line.date_eod,
                  p_receipt_date => line.date_receipt,
                  p_refuse_reason => v_result_message);
              end if;

              if v_result then
                 update SEOD_INCOMING n
                 set    n.processing_result = 1
                        ,n.processing_result_text = v_result_message
                 where n.eod_id = line.eod_id;
              else
                 rollback to s;                
                 log_info(v_log_scope,'line : skip');
                 update SEOD_INCOMING n
                 set     n.processing_result = 3
                        ,n.processing_result_text = v_result_message
                 where n.eod_id = line.eod_id;
              end if;
        end;
    end loop;

    update NDS2_MRR_USER.QUEUE_SEOD_STREAM_PROCESS
    set processed = current_timestamp
        ,is_processed = 1
    where session_id = pSessionId;
    commit;

    log_info(v_log_scope,'Завершение обработки потоков ЭОД с session_id = ' || pSessionId);

    exception when others then
      rollback;
      log_err(v_log_scope, sqlcode, sqlerrm, DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
end;

procedure P$PROCESS_SEOD_INCOME
as
v_log_scope constant varchar2(21):= 'P$PROCESS_SEOD_INCOME';
begin

  log_info(v_log_scope,'Начало обработки потоков ЭОД');

  NDS2_MRR_USER.PAC$LOGICAL_LOCK.P$SET_LOCK(NDS2_MRR_USER.PAC$LOGICAL_LOCK.TYPE_SEOD_INCOME, NDS2_MRR_USER.PAC$LOGICAL_LOCK.LOCKED_BY_SEOD_INCOME);

  for item_session in
  (
      select m.session_id
      from
      (
        select p.session_id
        from NDS2_MRR_USER.QUEUE_SEOD_STREAM_PROCESS p
        where p.processed is null
        order by p.created
      ) m
      where rownum <= LIMIT_SEOD_SESSION_PROCESSED
  )
  loop
  begin
      if NDS2_MRR_USER.PAC$LOGICAL_LOCK.P$HAS_LOCK_OTHER(NDS2_MRR_USER.PAC$LOGICAL_LOCK.TYPE_SEOD_INCOME, NDS2_MRR_USER.PAC$LOGICAL_LOCK.LOCKED_BY_SEOD_INCOME) then
         log_info(v_log_scope, 'Прерывание обработки потоков ЭОД, причина: установлена блокировка на прием потоков другим процессом');
         exit;
      else
         P$PROCESS_NEW_SEOD_DATA(item_session.session_id);
      end if;

  end;
  end loop;

  NDS2_MRR_USER.PAC$LOGICAL_LOCK.P$RELEASE_LOCK(NDS2_MRR_USER.PAC$LOGICAL_LOCK.TYPE_SEOD_INCOME, NDS2_MRR_USER.PAC$LOGICAL_LOCK.LOCKED_BY_SEOD_INCOME);

  log_info(v_log_scope,'Завершение обработки потоков ЭОД');

exception when others then
  rollback;
  log_err(v_log_scope, sqlcode, sqlerrm);
  NDS2_MRR_USER.PAC$LOGICAL_LOCK.P$RELEASE_LOCK(NDS2_MRR_USER.PAC$LOGICAL_LOCK.TYPE_SEOD_INCOME, NDS2_MRR_USER.PAC$LOGICAL_LOCK.LOCKED_BY_SEOD_INCOME);
end;

/*############################################################################################*/
/*# процедура для ручного перезапуска обработки потоков, завершившихся с ошибкой             #*/
/*# p_info_type - тип потока в формате 'CAM_NDS2_XX', где XX принимает значения от 01 до 13  #*/
/*# p_result_text_clause - строка с шаблоном для условия LIKE на поле processing_result_text #*/
/*#                        для более точной настройки повторной обработки потоков с ошибкой  #*/
/*############################################################################################*/
procedure P$REPEAT_PROCESS_FAILED_SEOD (
    p_info_type varchar2
   ,p_result_text_clause varchar2 := '%'
) as
  v_log_scope constant varchar2(28):='P$REPEAT_PROCESS_FAILED_SEOD';
  v_sql varchar2(1000);
begin

  log_info(v_log_scope, 'Начало повторной обработки потоков ЭОД');

  v_sql := 'update SEOD_INCOMING 
            set processing_result = 0 
            where 
                processing_result = 3 
                and info_type = ''' || p_info_type || ''' 
                and processing_result_text like ''' || p_result_text_clause || ''''
  ;
  execute_short_query(v_sql);

  for item_session in (
    select distinct q.SESSION_ID
      from NDS2_SEOD.seod_incoming t
      inner join NDS2_MRR_USER.queue_seod_stream_process q
        on q.session_id = t.session_id
      where t.processing_result = 0
        and q.processed is not null
  ) loop
    P$PROCESS_NEW_SEOD_DATA(item_session.SESSION_ID);
  end loop;

  log_info(v_log_scope,'Завершение повторной обработки потоков ЭОД');

  exception when others then
    rollback;
    log_err(v_log_scope, sqlcode, sqlerrm, DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
end;

end;
/
