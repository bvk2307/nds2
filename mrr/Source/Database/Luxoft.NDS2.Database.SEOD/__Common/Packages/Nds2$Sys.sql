﻿create or replace package NDS2_SEOD.Nds2$Sys as
 procedure Drop_object_if_exist(obj_name in varchar2);
 procedure LOG_ERROR
   (
     P_SITE in SYSTEM_LOG.SITE%type,
     P_ENTITY_ID in SYSTEM_LOG.ENTITY_ID%type,
     P_MESSAGE_CODE in SYSTEM_LOG.MESSAGE_CODE%type,
     P_MESSAGE in SYSTEM_LOG.MESSAGE%type
   );
 procedure LOG_WARNING
   (
     P_SITE in SYSTEM_LOG.SITE%type,
     P_ENTITY_ID in SYSTEM_LOG.ENTITY_ID%type,
     P_MESSAGE_CODE in SYSTEM_LOG.MESSAGE_CODE%type,
     P_MESSAGE in SYSTEM_LOG.MESSAGE%type
   );
 procedure LOG_INFO
   (
     P_SITE in SYSTEM_LOG.SITE%type,
     P_ENTITY_ID in SYSTEM_LOG.ENTITY_ID%type,
     P_MESSAGE_CODE in SYSTEM_LOG.MESSAGE_CODE%type,
     P_MESSAGE in SYSTEM_LOG.MESSAGE%type
   );
 procedure DROP_IF_EXISTS
  (
    P_SCHEME in all_tables.owner%type,
    P_TABLE in all_tables.table_name%type
  );

   procedure DROP_IF_EXISTS_EX
  (
    P_SCHEME in varchar2,
    P_OBJ_NAME in varchar2,
    P_OBJ_TYPE in varchar2,
    P_CHILD_OBJ in varchar2
  );

    procedure RECOMPILE_INVALID_OBJECTS;

  procedure P$GATHER_TABLE_STATS( p_table_name varchar2, p_force boolean := false );

end;
/
create or replace package body NDS2_SEOD.Nds2$Sys as

procedure Drop_object_if_exist(obj_name in varchar2)
is
begin
  for line in (
     select
     distinct
     decode(object_type,'PACKAGE BODY', 'PACKAGE' ,object_type) as object_type,
     object_name,
    'drop '||decode(object_type,'PACKAGE BODY', 'PACKAGE' ,object_type)||' '||owner||'.'|| object_name as cmd
     from
     all_objects
     where upper(object_name) = trim(upper(obj_name))
    )
  loop
    begin
      EXECUTE IMMEDIATE line.cmd;
      exception when others then
        LOG_ERROR(P_SITE => 'NDS2$SYS', P_ENTITY_ID => -1, P_MESSAGE_CODE => sqlcode, P_MESSAGE => 'Ошибка удаления объекта '||line.object_type||'.'||line.object_name||':'||sqlerrm);
    end;
  end loop;
end;

 procedure LOG_DATA
   (
     P_SITE in SYSTEM_LOG.SITE%type,
     P_ENTITY_ID in SYSTEM_LOG.ENTITY_ID%type,
     P_MESSAGE_CODE in SYSTEM_LOG.MESSAGE_CODE%type,
     P_MESSAGE in SYSTEM_LOG.MESSAGE%type,
     P_TYPE in SYSTEM_LOG.TYPE%type
   )
   is
pragma autonomous_transaction;
begin
  insert into SYSTEM_LOG(ID, TYPE, SITE, ENTITY_ID, MESSAGE_CODE, MESSAGE, LOG_DATE)
  values (SEQ_SYS_LOG_ID.nextval, P_TYPE, P_SITE, P_ENTITY_ID, P_MESSAGE_CODE, P_MESSAGE, sysdate);
  commit;
exception when others then rollback;
end;

 procedure LOG_ERROR
   (
     P_SITE in SYSTEM_LOG.SITE%type,
     P_ENTITY_ID in SYSTEM_LOG.ENTITY_ID%type,
     P_MESSAGE_CODE in SYSTEM_LOG.MESSAGE_CODE%type,
     P_MESSAGE in SYSTEM_LOG.MESSAGE%type
   )
 is
 begin
   LOG_DATA(P_SITE, P_ENTITY_ID, P_MESSAGE_CODE, P_MESSAGE, 3);
 end;

 procedure LOG_WARNING
   (
     P_SITE in SYSTEM_LOG.SITE%type,
     P_ENTITY_ID in SYSTEM_LOG.ENTITY_ID%type,
     P_MESSAGE_CODE in SYSTEM_LOG.MESSAGE_CODE%type,
     P_MESSAGE in SYSTEM_LOG.MESSAGE%type
   )
   is
 begin
   LOG_DATA(P_SITE, P_ENTITY_ID, P_MESSAGE_CODE, P_MESSAGE, 2);
 end;

 procedure LOG_INFO
   (
     P_SITE in SYSTEM_LOG.SITE%type,
     P_ENTITY_ID in SYSTEM_LOG.ENTITY_ID%type,
     P_MESSAGE_CODE in SYSTEM_LOG.MESSAGE_CODE%type,
     P_MESSAGE in SYSTEM_LOG.MESSAGE%type
   )
   is
 begin
   LOG_DATA(P_SITE, P_ENTITY_ID, P_MESSAGE_CODE, P_MESSAGE, 1);
 end;

  procedure DROP_IF_EXISTS
  (
    P_SCHEME in all_tables.owner%type,
    P_TABLE in all_tables.table_name%type
  )
is
  v_exist number(1);
begin
  select nvl(count(1), 0) into v_exist from all_tables where owner = P_SCHEME and table_name = P_TABLE;
  if v_exist > 0 then
    execute immediate 'drop table '||P_SCHEME||'.'||P_TABLE;
  end if;
end;


procedure DROP_IF_EXISTS_EX
  (
    P_SCHEME in varchar2,
    P_OBJ_NAME in varchar2,
    P_OBJ_TYPE in varchar2,
    P_CHILD_OBJ in varchar2
  )
  as
  v_drop_sql varchar2(1024);
  begin
    if P_OBJ_TYPE = 'TABLE' then
      for line in (select * from all_tables where owner = P_SCHEME and table_name = P_OBJ_NAME)
      loop
          v_drop_sql := 'drop table "'||P_SCHEME||'"."'||P_OBJ_NAME||'" cascade constraints';
      end loop;
    end if;

    if P_OBJ_TYPE = 'TYPE' then
      for line in (select * from all_types where owner = P_SCHEME and type_name = P_OBJ_NAME)
      loop
            v_drop_sql := 'DROP TYPE "'||P_SCHEME||'"."'||P_OBJ_NAME||'"';
      end loop;
    end if;

    if P_OBJ_TYPE = 'MVIEW' then
      for line in (select * from all_types where owner = P_SCHEME and type_name = P_OBJ_NAME)
      loop
            v_drop_sql := 'DROP MATERIALIZED VIEW "'||P_SCHEME||'"."'||P_OBJ_NAME||'"';
      end loop;
    end if;

    if P_OBJ_TYPE = 'COL' then
      v_drop_sql := 'ALTER TABLE "'||P_SCHEME||'"."'||P_OBJ_NAME||'" DROP COLUMN "'||P_CHILD_OBJ||'"';
    end if;

    if length(v_drop_sql) > 0 then
       execute immediate v_drop_sql;
    end if;
  end;

procedure RECOMPILE_INVALID_OBJECTS
 as
begin
  for line in (select
    object_type,
    object_name,
    'alter '||decode(object_type,'PACKAGE BODY', 'PACKAGE' ,object_type)||' '||object_name||' compile' as cmd
    from user_objects where status = 'INVALID')
    loop
       begin
         execute immediate line.cmd;
         exception  when others then
           LOG_ERROR(P_SITE => 'NDS2$SYS', P_ENTITY_ID => -1, P_MESSAGE_CODE => sqlcode, P_MESSAGE => line.object_type||'.'||line.object_name||' '||SQLERRM );
       end;
    end loop;

  exception when others then
   LOG_ERROR(P_SITE => 'NDS2$SYS', P_ENTITY_ID => -1, P_MESSAGE_CODE => sqlcode, P_MESSAGE => SQLERRM );
end;

procedure P$GATHER_TABLE_STATS( p_table_name varchar2, p_force boolean := false )
as
v_table_exist_for_gather_stats number(1);
v_last_analyze_date date;
v_log_site varchar2(128 char) := 'NDS2$SYS::GATHER_TABLE_STATS';
begin

select 
 count(1),
 max(LAST_ANALYZED)
into 
  v_table_exist_for_gather_stats,
  v_last_analyze_date
from
  ALL_TAB_STATISTICS 
where 
  owner = 'NDS2_MRR_USER'
  and PARTITION_NAME is null 
  and table_name = upper(p_table_name)
  and STALE_STATS = 'YES';
  
if v_table_exist_for_gather_stats > 0 or p_force then
  LOG_INFO(P_SITE => v_log_site, P_ENTITY_ID => 0, P_MESSAGE_CODE => 0, P_MESSAGE => 'Сбор стастистики для таблицы '||p_table_name);

  dbms_stats.gather_table_stats(
   ownname => 'NDS2_MRR_USER', 
   tabname => p_table_name, 
   cascade => true, 
   estimate_percent => 30,
   method_opt => 'FOR ALL INDEXED COLUMNS');  
   
   LOG_INFO(P_SITE => v_log_site, P_ENTITY_ID => 0, P_MESSAGE_CODE => 0, P_MESSAGE => 'Завершен сбор стастистики для таблицы '||p_table_name);
end if;
 
exception when others then 
 LOG_ERROR(P_SITE => v_log_site, P_ENTITY_ID => 0, P_MESSAGE_CODE => sqlcode, P_MESSAGE => SQLERRM );  
 dbms_standard.raise_application_error(
 num => -21000, 
 msg => 'Ошибка сбора статистики для таблицы '||p_table_name||' '||sqlcode||' '||sqlerrm);
end;

end;
/

grant execute on nds2_seod.nds2$sys to nds2_mrr_user;
