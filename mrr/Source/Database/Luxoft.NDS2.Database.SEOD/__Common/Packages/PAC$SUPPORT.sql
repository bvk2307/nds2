﻿create or replace package NDS2_SEOD.PAC$SUPPORT is

 procedure P$REMOVE_SONO(
    pSonoToRemove in varchar2,
    pSonoNew in varchar2,
    pRegNumberShift number);

end PAC$SUPPORT;
/
create or replace package body NDS2_SEOD.PAC$SUPPORT is

  procedure P$REMOVE_SONO(
    pSonoToRemove in varchar2,
    pSonoNew in varchar2,
    pRegNumberShift number)
  as
  begin
    NDS2$SYS.LOG_INFO ( 'PAC$SUPPORT.P$REMOVE_SONO', 1, 1, 'BEGIN: pSonoToRemove = '||pSonoToRemove||'; pSonoNew = '||pSonoNew||'; pRegNumberShift = '||pRegNumberShift);
    
    update NDS2_SEOD.SEOD_DECLARATION
    set sono_code = pSonoNew, decl_reg_num = decl_reg_num + pRegNumberShift
    where sono_code = pSonoToRemove;

    update NDS2_SEOD.SEOD_REG_NUMBER
    set sono_code = pSonoNew, decl_reg_num = decl_reg_num + pRegNumberShift
    where sono_code = pSonoToRemove;

    update NDS2_SEOD.SEOD_REG_NUMBER_CANCELLED
    set sono_code = pSonoNew, decl_reg_num = decl_reg_num + pRegNumberShift, new_decl_reg_num = new_decl_reg_num + pRegNumberShift
    where sono_code = pSonoToRemove;

    update NDS2_SEOD.SEOD_KNP
    set ifns_code = pSonoNew, declaration_reg_num = declaration_reg_num + pRegNumberShift
    where ifns_code = pSonoToRemove;
    
    commit;
    
    NDS2$SYS.LOG_INFO ( 'PAC$SUPPORT.P$REMOVE_SONO', 1, 1, 'END');
    
  exception when others then 
    rollback;
    NDS2$SYS.LOG_ERROR ( 'PAC$SUPPORT.P$REMOVE_SONO', 1, sqlcode, 'ERROR: '||substr(sqlerrm, 1, 512));
    raise;
  end;
  
  end PAC$SUPPORT;
/