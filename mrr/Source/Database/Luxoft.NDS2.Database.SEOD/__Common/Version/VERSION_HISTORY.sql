﻿declare v_version varchar2(15) := '8.14.2.1';
begin
	merge into nds2_seod.version_history t
	using (select v_version as VERSION_NUMBER from dual) s
		on (t.VERSION_NUMBER = s.VERSION_NUMBER)
	when not matched then
		insert (VERSION_NUMBER,RELEASE_DATE,CATALOG_REINDEX_REQUIRED) values (v_version, sysdate, 1);

	commit;
end;
/