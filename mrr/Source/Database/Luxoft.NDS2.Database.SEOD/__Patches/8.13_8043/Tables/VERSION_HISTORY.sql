﻿  CREATE TABLE NDS2_SEOD.VERSION_HISTORY 
  (	
	VERSION_NUMBER VARCHAR2(15 BYTE) NOT NULL ENABLE, 
	RELEASE_DATE DATE NOT NULL ENABLE, 
	CATALOG_REINDEX_REQUIRED NUMBER(1,0) NOT NULL ENABLE, 
	RELEASE_FILE_NAME VARCHAR2(250 BYTE)
   );