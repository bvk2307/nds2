﻿begin 
  execute immediate 'drop sequence NDS2_SEOD.SEQ_SEOD_REG_NUMBER';
  exception when others then null;
end;
/

create sequence NDS2_SEOD.SEQ_SEOD_REG_NUMBER
start with 1
increment by 1 
minvalue 1
maxvalue 99999999999999
nocache
nocycle;