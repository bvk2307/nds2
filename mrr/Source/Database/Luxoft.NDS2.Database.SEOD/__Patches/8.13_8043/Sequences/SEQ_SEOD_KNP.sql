﻿declare
	v_cur_value number;
begin
	SELECT last_number into v_cur_value
	  FROM all_sequences
	 WHERE sequence_owner = 'NDS2_MRR_USER'
	   AND sequence_name = 'SEQ_EXPLAIN_REPLY';
	   
	execute immediate 'create sequence NDS2_SEOD.SEQ_EXPLAIN_REPLY minvalue 1 maxvalue 9999999999 start with ' || v_cur_value || ' increment by 1 nocache';
end;
/