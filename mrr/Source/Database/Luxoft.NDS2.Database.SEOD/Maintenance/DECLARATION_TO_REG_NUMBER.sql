﻿truncate table NDS2_SEOD.SEOD_REG_NUMBER;
truncate table NDS2_SEOD.SEOD_REG_NUMBER_CANCELLED;

DECLARE 
TYPE SeodRegNumberType IS RECORD (
  DECL_REG_NUM      NUMBER(20)
 ,INN               VARCHAR2(12 CHAR) 
 ,TAX_PERIOD        VARCHAR2(2 CHAR)
 ,FISCAL_YEAR       VARCHAR2(4 CHAR) 
 ,SONO_CODE         VARCHAR2(4 CHAR)
 ,CORRECTION_NUMBER NUMBER(5)
 ,TYPE              NUMBER(1)
 ,ID_FILE           VARCHAR2(1024 CHAR)
 ,SUBMISSION_DATE   DATE
 ,EOD_DATE			DATE
 ,INSERT_DATE       DATE);
TYPE SeodRegNumberTable IS TABLE OF SeodRegNumberType INDEX BY BINARY_INTEGER;
SeodRegNumberRec SeodRegNumberTable;
v_decl_reg_num number(20);
v_reg_num_date date;
f_decl_reg_num number(20);
r_exist number(1);

CURSOR RegNumberSingle IS 
select max(DECL_REG_NUM) as DECL_REG_NUM, INN, TAX_PERIOD, FISCAL_YEAR, SONO_CODE, CORRECTION_NUMBER, TYPE, ID_FILE
 ,min(nvl(eod_date, sysdate)) as EOD_DATE
 ,min(nvl(SUBMISSION_DATE, nvl(EOD_DATE, nvl(INSERT_DATE, sysdate)))) as SUBMISSION_DATE
 ,min(INSERT_DATE) as INSERT_DATE
from NDS2_SEOD.seod_declaration
group by INN, TAX_PERIOD, FISCAL_YEAR, SONO_CODE, CORRECTION_NUMBER, TYPE, ID_FILE 
having count(*) = 1;

BEGIN
  OPEN RegNumberSingle;
  LOOP
    FETCH RegNumberSingle BULK COLLECT INTO SeodRegNumberRec LIMIT 50000;

    FORALL i IN SeodRegNumberRec.FIRST..SeodRegNumberRec.LAST
    INSERT INTO NDS2_SEOD.SEOD_REG_NUMBER (ID, DECL_REG_NUM, INN, TAX_PERIOD, FISCAL_YEAR, SONO_CODE, 
        CORRECTION_NUMBER, TYPE, ID_FILE, SUBMISSION_DATE, EOD_DATE, INSERT_DATE)
    VALUES (NDS2_SEOD.SEQ_SEOD_REG_NUMBER.NEXTVAL, SeodRegNumberRec(i).DECL_REG_NUM,
    SeodRegNumberRec(i).INN, SeodRegNumberRec(i).TAX_PERIOD, SeodRegNumberRec(i).FISCAL_YEAR,
    SeodRegNumberRec(i).SONO_CODE, SeodRegNumberRec(i).CORRECTION_NUMBER, SeodRegNumberRec(i).TYPE,
    SeodRegNumberRec(i).ID_FILE, SeodRegNumberRec(i).SUBMISSION_DATE, SeodRegNumberRec(i).EOD_DATE,
    SeodRegNumberRec(i).INSERT_DATE);
    
    commit;
    
    EXIT WHEN RegNumberSingle%NOTFOUND;
  END LOOP;
  
  CLOSE RegNumberSingle;

  FOR line IN (  
  select INN, TAX_PERIOD, FISCAL_YEAR, SONO_CODE, CORRECTION_NUMBER, TYPE, ID_FILE 
  from NDS2_SEOD.SEOD_DECLARATION
  group by INN, TAX_PERIOD, FISCAL_YEAR, SONO_CODE, CORRECTION_NUMBER, TYPE, ID_FILE 
  having count(*) > 1)
  LOOP
    FOR dbl IN (
    select row_number() over (order by DECL_REG_NUM, INSERT_DATE desc) as RWNUM, INN, TAX_PERIOD, FISCAL_YEAR, SONO_CODE, CORRECTION_NUMBER, 
      TYPE, ID_FILE, DECL_REG_NUM, nvl(SUBMISSION_DATE, nvl(EOD_DATE, nvl(INSERT_DATE, sysdate))) as SUBMISSION_DATE, eod_date, INSERT_DATE
    from NDS2_SEOD.SEOD_DECLARATION
    where INN = line.INN and TAX_PERIOD = line.TAX_PERIOD and FISCAL_YEAR = line.FISCAL_YEAR and SONO_CODE = line.SONO_CODE
    and CORRECTION_NUMBER = line.CORRECTION_NUMBER and TYPE = line.TYPE and nvl(ID_FILE, '') = nvl(line.ID_FILE, '')
    order by row_number() over (order by DECL_REG_NUM, INSERT_DATE desc) asc
    )
    LOOP
      if (dbl.RWNUM = 1) then
      begin
        INSERT INTO NDS2_SEOD.SEOD_REG_NUMBER (ID, DECL_REG_NUM, INN, TAX_PERIOD, FISCAL_YEAR, SONO_CODE, 
          CORRECTION_NUMBER, TYPE, ID_FILE, SUBMISSION_DATE, EOD_DATE, INSERT_DATE)
        VALUES (NDS2_SEOD.SEQ_SEOD_REG_NUMBER.NEXTVAL, dbl.DECL_REG_NUM,
          dbl.INN, dbl.TAX_PERIOD, dbl.FISCAL_YEAR, dbl.SONO_CODE, dbl.CORRECTION_NUMBER, dbl.TYPE,
          dbl.ID_FILE, dbl.SUBMISSION_DATE, dbl.EOD_DATE, dbl.INSERT_DATE);
          
        f_decl_reg_num := dbl.DECL_REG_NUM;

      end;
      else
      begin
	    if f_decl_reg_num != dbl.DECL_REG_NUM then
		begin
          INSERT INTO NDS2_SEOD.SEOD_REG_NUMBER_CANCELLED (ID, SONO_CODE, DECL_REG_NUM, NEW_DECL_REG_NUM, CANCELLED_AT)
          VALUES (NDS2_SEOD.SEQ_SEOD_REG_NUMBER_CANCELLED.NEXTVAL, dbl.SONO_CODE, dbl.DECL_REG_NUM, v_decl_reg_num, v_reg_num_date);

          select case when exists(select REF_ENTITY_ID from NDS2_MRR_USER.doc where REF_ENTITY_ID = dbl.DECL_REG_NUM
          and SONO_CODE = dbl.SONO_CODE and INN = dbl.INN) then 1 else 0 end into r_exist from dual;
          if (r_exist = 1) then      
            update NDS2_MRR_USER.doc set
              REF_ENTITY_ID = f_decl_reg_num
            where REF_ENTITY_ID = dbl.DECL_REG_NUM and SONO_CODE = dbl.SONO_CODE and INN = dbl.INN; 
          end if;
      
          select case when exists(select DECLARATION_REG_NUM from NDS2_SEOD.seod_knp where DECLARATION_REG_NUM = dbl.DECL_REG_NUM
          and IFNS_CODE = dbl.SONO_CODE) then 1 else 0 end into r_exist from dual;
          if (r_exist = 1) then
            update NDS2_SEOD.seod_knp set
              DECLARATION_REG_NUM = f_decl_reg_num
            where DECLARATION_REG_NUM = dbl.DECL_REG_NUM and IFNS_CODE = dbl.SONO_CODE;
          end if; 

		end;
		end if;
        
      end;
      end if;

      v_decl_reg_num := dbl.DECL_REG_NUM;
      v_reg_num_date := nvl(dbl.INSERT_DATE, sysdate);
    
    END LOOP;

	commit;
    
  END LOOP;

END;
/
