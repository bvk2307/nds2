﻿CREATE OR REPLACE PACKAGE NDS2_MRR_USER.PAC$CLAIM_LIFE_CYCLE
AS
/*******************************************************************************/
/*****  функции получения параметров жизненного цикла АТ                   *****/
/*******************************************************************************/
  FUNCTION F$GET_CLAIM_RESEND_TIMEOUT RETURN NUMBER;    -- время ожидания от СЭОД подтверждения о получении АТ
  FUNCTION F$GET_CLAIM_RESEND_ATTEMPTS RETURN NUMBER;   -- количество попыток повторной отправки АТ в СЭОД
  FUNCTION F$GET_CLAIM_DELIVERY_TIMEOUT RETURN NUMBER;  -- время ожидания вручения АТ НП
  FUNCTION F$GET_CLAIM_EXPLAIN_TIMEOUT RETURN NUMBER;   -- время ожидания ответа НП на АТ

/*******************************************************************************/
/*****  функции процессинга жизненного цикла АТ                            *****/
/*******************************************************************************/
  PROCEDURE P$PROCESS_CLAIMS_LIFE_CYCLE; -- процедура реализации жизненного цикла АТ 
  
  PROCEDURE P$CLOSING_CLAIMS_BY_ANNULMENT(p_annulment_id number, p_closing_date date); 
END; 
/ 


CREATE OR REPLACE PACKAGE BODY NDS2_MRR_USER.PAC$CLAIM_LIFE_CYCLE
AS 
  DEFAULT_CLAIM_RESEND_TIMEOUT number := 3;     -- время ожидания от СЭОД подтверждения о получении АТ
  DEFAULT_CLAIM_RESEND_ATTEMPTS number := 3;    -- количество попыток повторной отправки АТ в СЭОД
  DEFAULT_CLAIM_DELIVERY_TIMEOUT number := 6;   -- время ожидания вручения АТ НП
  DEFAULT_CLAIM_EXPLAIN_TIMEOUT number := 8;    -- время ожидания ответа НП на АТ
  
  LOG_INFO_TYPE number := 0;                    -- тип информационного сообщения в логе
  LOG_WARNING_TYPE number := 1;                 -- тип сообщения с предупреждением в логе
  LOG_ERROR_TYPE number := 2;                   -- тип сообщения об ошибке в логе
  
  LIMIT_ROWS_COMMIT number := 500;              -- ограничение на кол-во незакоммиченных записей при работе в цикле
  LIMIT_AUTOCLOSE_PARAM_NAME varchar(25) := 'at_autoclose_items_limit'; -- параметр ограничения разового автозакрытия АТ
  
  SENDING_STATUS number := 1;                   -- статус АТ "Готово к отправке"
  
  CLOSE_STATUS number := 10;                    -- статус АТ "Закрыто"
  CLOSE_REASON_TIMEOUT number := 1;             -- причина закрытия АТ - вышел срок отработки АТ
  CLOSE_REASON_NEW_CORR number := 2;            -- причина закрытия АТ - получена новая корректировка
  CLOSE_REASON_ANNULMENT number := 5;           -- причина закрытия АТ - заявка на аннулирование корректировки
  CLOSE_REASON_BY_DISCREPANCY number := 4;             -- причина закрытия АТ - вышел срок отработки АТ

/******************************************************************************/
/*****  функции получения парметров жизненного цикла АТ                    *****/
/*******************************************************************************/

/* получаем время ожидания от СЭОД подтверждения о получении АТ в днях */
  FUNCTION F$GET_CLAIM_RESEND_TIMEOUT 
    RETURN NUMBER 
  IS 
    v_claim_resend_timeout number;
  BEGIN 
  
    select nvl(max(claim_resend_timeout_sec) / 60.0 / 60.0 / 24.0, DEFAULT_CLAIM_RESEND_TIMEOUT) 
      into v_claim_resend_timeout 
      from NDS2_MRR_USER.system_settings; 
  
    RETURN v_claim_resend_timeout; 
  END;

/* получаем количество попыток повторной отправки АТ в СЭОД */  
  FUNCTION F$GET_CLAIM_RESEND_ATTEMPTS 
    RETURN NUMBER 
  IS 
    v_claim_resend_attempts number;
  BEGIN 
  
    select nvl(max(claim_resend_attempt_qty), DEFAULT_CLAIM_RESEND_ATTEMPTS) 
      into v_claim_resend_attempts 
      from NDS2_MRR_USER.system_settings; 
  
    RETURN v_claim_resend_attempts; 
  END;
 
/* время ожидания вручения АТ НП в днях */   
  FUNCTION F$GET_CLAIM_DELIVERY_TIMEOUT 
    RETURN NUMBER 
  IS 
    v_claim_delivery_timeout number;
  BEGIN 
  
    select nvl(max(claim_delivery_timeout_sec) / 60.0 / 60.0 / 24.0, DEFAULT_CLAIM_DELIVERY_TIMEOUT) 
      into v_claim_delivery_timeout 
      from NDS2_MRR_USER.system_settings; 
  
    RETURN v_claim_delivery_timeout; 
  END;
    
/* время ожидания ответа НП на АТ в днях */   
  FUNCTION F$GET_CLAIM_EXPLAIN_TIMEOUT 
    RETURN NUMBER 
  IS 
    v_claim_explain_timeout number;
  BEGIN 
    
    select nvl(max(claim_explain_timeout_sec) / 60.0 / 60.0 / 24.0, DEFAULT_CLAIM_EXPLAIN_TIMEOUT) 
      into v_claim_explain_timeout 
      from NDS2_MRR_USER.system_settings; 
  
    RETURN v_claim_explain_timeout; 
  END;

/******************************************************************************/
/*****  процедуры логирования активностей жизненного цикла АТ              *****/
/*******************************************************************************/
/* запись в лог любого сообщения с типом (0 - информационное сообщение, 1 - предупреждение, 2 - ошибка) */   
  PROCEDURE LOG ( -- запись в лог 
    pCodeSite claim_life_cycle_log.code_site%type, 
    pMsgType  claim_life_cycle_log.msg_type%type, 
    pMsgText  claim_life_cycle_log.msg_text%type 
  ) IS 
    PRAGMA autonomous_transaction;
  BEGIN 
    insert into claim_life_cycle_log (id, log_time, code_site, msg_type, msg_text) 
      values (seq$claim_life_cycle_log.nextval, sysdate, pCodeSite, pMsgType, pMsgText); 
    commit;
    
  EXCEPTION 
    when others then NDS2$SYS.LOG_ERROR('PAC$CLAIM_LIFE_CYCLE.LOG', NULL, sqlcode, SUBSTR(sqlerrm, 1, 512));
  END;

/* запись в лог информационного сообщения */   
  PROCEDURE LOG_INFO ( -- запись информационного сообщения в лог
    pCodeSite claim_life_cycle_log.code_site%type, 
    pMsgText  claim_life_cycle_log.msg_text%type 
  ) IS 
  BEGIN 
    log(pCodeSite, LOG_INFO_TYPE, pMsgText); 
    exception when others 
      then NDS2$SYS.LOG_ERROR('PAC$CLAIM_LIFE_CYCLE.LOG_INFO', NULL, sqlcode, SUBSTR(sqlerrm, 1, 512));
  END; 

/* запись в лог сообщения с предупреждением */   
  PROCEDURE LOG_WARNING ( -- запись сообщения с предупреждением в лог
    pCodeSite claim_life_cycle_log.code_site%type, 
    pMsgText  claim_life_cycle_log.msg_text%type 
  ) IS 
  BEGIN 
    log(pCodeSite, LOG_WARNING_TYPE, pMsgText); 
    exception when others 
      then NDS2$SYS.LOG_ERROR('PAC$CLAIM_LIFE_CYCLE.LOG_WARNING', NULL, sqlcode, SUBSTR(sqlerrm, 1, 512));
  END;

/* запись в лог любого сообщения с ошибкой */   
  PROCEDURE LOG_ERROR ( -- запись сообщения об ошибке в лог
    pCodeSite claim_life_cycle_log.code_site%type, 
    pMsgText  claim_life_cycle_log.msg_text%type 
  ) IS 
  BEGIN 
    log(pCodeSite, LOG_ERROR_TYPE, pMsgText); 
    exception when others 
      then NDS2$SYS.LOG_ERROR('PAC$CLAIM_LIFE_CYCLE.LOG_ERROR', NULL, sqlcode, SUBSTR(sqlerrm, 1, 512));
  END;

/******************************************************************************/
/*****  процедуры процессинга жизненного цикла АТ                            *****/
/*******************************************************************************/
  
/* процедура, реализующая повторную отправку АТ НП */ 
  PROCEDURE P$RESENDING_CLAIMS 
  IS 
    v_current_date date := trunc(sysdate);
    
    v_resend_date date := PAC$CALENDAR.F$ADD_WORK_DAYS(v_current_date, -1 * F$GET_CLAIM_RESEND_TIMEOUT());
    v_resend_attempts number := F$GET_CLAIM_RESEND_ATTEMPTS(); 
    
    v_commit_counter number := 0;
  BEGIN 
    LOG_INFO('PAC$CLAIM_LIFE_CYCLE.P$RESENDING_CLAIMS', 'Начало работы процессинга повторной отправки АТ - '||to_char(sysdate, 'dd.mm.yyyy hh24:mi:ss'));
    for cur in ( 
      select doc_id 
        from NDS2_MRR_USER.doc 
        where doc_type = 1 
          and status = 2 
          and status_date < v_resend_date 
          and (TAX_PAYER_SEND_DATE is null or not trunc(TAX_PAYER_SEND_DATE) between status_date and v_current_date) 
          and (TAX_PAYER_DELIVERY_DATE is null or not trunc(TAX_PAYER_DELIVERY_DATE) between status_date and v_current_date) 
          and (TAX_PAYER_EXPLAIN_DATE is null or not trunc(TAX_PAYER_EXPLAIN_DATE) between status_date and v_current_date) 
          and (SEOD_ACCEPT_DATE is null or not trunc(SEOD_ACCEPT_DATE) between status_date and v_current_date) 
          and nvl(dispatch_counter, 0) < v_resend_attempts 
    ) loop 
      
      update NDS2_MRR_USER.doc 
        set status = SENDING_STATUS 
           ,status_date = v_current_date 
           ,dispatch_counter = nvl(dispatch_counter, 0) + 1 
        where doc_id = cur.doc_id; 
      
      insert into NDS2_MRR_USER.doc_status_history(doc_id, status, status_date)
        values(cur.doc_id, SENDING_STATUS, sysdate); 
      
      v_commit_counter := v_commit_counter + 1;
      
      if mod(v_commit_counter, LIMIT_ROWS_COMMIT) = 0 then 
        commit; 
      end if;
      
    end loop;
    commit; 
    
    LOG_INFO('PAC$CLAIM_LIFE_CYCLE.P$RESENDING_CLAIMS', 'Завершение работы процессинга повторной отправки АТ - '||to_char(sysdate, 'dd.mm.yyyy hh24:mi:ss') || '. Обработано записей - ' || trim(to_char(v_commit_counter, '9999999999'))); 
  EXCEPTION 
    when others then LOG_ERROR('PAC$CLAIM_LIFE_CYCLE.P$RESENDING_CLAIMS', SUBSTR(sqlerrm, 1, 1024));
  END;
  
/* процедура закрытия АТ */
  PROCEDURE P$CLOSING_CLAIMS( 
    p_claim_ids T$TABLE_OF_NUMBER, 
    p_close_date date, 
    p_status_closed number, 
    p_reason_closed number 
  ) IS 
  BEGIN 
    --Обновляем статус и причину закрытия АТ
    FORALL indx IN 1 .. p_claim_ids.COUNT
      update doc
        set close_date = p_close_date,
            status = p_status_closed,
            status_date = trunc(p_close_date),
            close_reason = p_reason_closed
        where doc_id = p_claim_ids(indx);

    --Добавляем записи в историю и перечень закрытых АТ
    FORALL indx IN 1 .. p_claim_ids.COUNT
      insert all
        into doc_status_history (doc_id, status, status_date) 
          values (p_claim_ids(indx), p_status_closed, p_close_date)
        into claim_closed (doc_id) 
          values (p_claim_ids(indx))
        select 1 from dual;
    
    commit;
    
  EXCEPTION 
    when others then LOG_ERROR('PAC$CLAIM_LIFE_CYCLE.P$CLOSING_CLAIMS', SUBSTR(sqlerrm, 1, 1024));
  END;
  
  /* процедура, реализующая закрытие АТ НП по заявке на аннулирование */ 
  PROCEDURE P$CLOSING_CLAIMS_BY_ANNULMENT(p_annulment_id number, p_closing_date date) 
  AS
	t_claim_id_col T$TABLE_OF_NUMBER;
  BEGIN 
       
	select d.doc_id 
	bulk collect into t_claim_id_col
	from 
	nds2_seod.seod_declaration_annulment da
	left join nds2_mrr_user.doc d on da.reg_number = d.ref_entity_id and da.sono_code = d.sono_code 
	  and d.doc_type in (1,2) 
	  and d.status not in (3,9,10)
	where d.doc_id is not null and da.ID = p_annulment_id;
      
	  if(t_claim_id_col.count > 0) then
      
		  P$CLOSING_CLAIMS (p_claim_ids => t_claim_id_col,
							p_close_date => p_closing_date,
							p_status_closed => CLOSE_STATUS,
							p_reason_closed => CLOSE_REASON_ANNULMENT);
	  end if;

	EXCEPTION 
    when others then LOG_ERROR('PAC$CLAIM_LIFE_CYCLE.P$CLOSING_CLAIMS_BY_TIMEOUT', SUBSTR(sqlerrm, 1, 1024));
    raise;
  END;
  
/* процедура, реализующая закрытие АТ НП по таймауту */ 
  PROCEDURE P$CLOSING_CLAIMS_BY_TIMEOUT 
  IS 
    v_current_date date := sysdate;
    
    v_delivery_timeout number := PAC$CLAIM_LIFE_CYCLE.F$GET_CLAIM_DELIVERY_TIMEOUT();
    v_explain_timeout number := PAC$CLAIM_LIFE_CYCLE.F$GET_CLAIM_EXPLAIN_TIMEOUT();
    v_resend_attempts number := PAC$CLAIM_LIFE_CYCLE.F$GET_CLAIM_RESEND_ATTEMPTS();
    
    v_items_limit number;
    
    t_claim_id_col T$TABLE_OF_NUMBER;
  BEGIN 
    LOG_INFO('PAC$CLAIM_LIFE_CYCLE.P$CLOSING_CLAIMS_BY_TIMEOUT', 'Начало работы процессинга закрытия АТ по таймауту - '||to_char(sysdate, 'dd.mm.yyyy hh24:mi:ss'));
    
    select cast(value as number) 
      into v_items_limit 
      from configuration 
      where parameter = LIMIT_AUTOCLOSE_PARAM_NAME; 
    
    loop 
      select distinct dcc.doc_id 
        bulk collect into t_claim_id_col
        from 
          ( 
            select dc.doc_id 
                  ,dc.calc_delivery_date
                  ,PAC$CALENDAR.F$ADD_WORK_DAYS(nvl2(dc.TAX_PAYER_DELIVERY_DATE, least(dc.TAX_PAYER_DELIVERY_DATE, dc.calc_delivery_date), dc.calc_delivery_date), v_explain_timeout + 1) as calc_close_date 
              from 
                ( 
                  select doc_id 
                        ,TAX_PAYER_DELIVERY_DATE 
                        ,case 
                          when status = 2 then PAC$CALENDAR.F$ADD_WORK_DAYS(SEOD_SEND_DATE, v_delivery_timeout) 
                          else PAC$CALENDAR.F$ADD_WORK_DAYS(nvl(TAX_PAYER_SEND_DATE, nvl(SEOD_ACCEPT_DATE, SEOD_SEND_DATE)), v_delivery_timeout) 
                         end as calc_delivery_date 
                    from NDS2_MRR_USER.doc 
                    where doc_type = 1 
                      and ((status = 2 and not nvl(dispatch_counter, 0) < v_resend_attempts) 
                        or (status in (4, 5, 6))) 
                ) dc 
          ) dcc 
        where dcc.calc_close_date <= v_current_date
          and rownum <= v_items_limit; 
      
      if(t_claim_id_col.count = 0) then
        exit;
      end if;
      
      P$CLOSING_CLAIMS (p_claim_ids => t_claim_id_col,
                        p_close_date => v_current_date,
                        p_status_closed => CLOSE_STATUS,
                        p_reason_closed => CLOSE_REASON_TIMEOUT);
      
    end loop;
    
    LOG_INFO('PAC$CLAIM_LIFE_CYCLE.P$CLOSING_CLAIMS_BY_TIMEOUT', 'Завершение работы процессинга закрытия АТ по таймауту - '||to_char(sysdate, 'dd.mm.yyyy hh24:mi:ss')); 
  EXCEPTION 
    when others then LOG_ERROR('PAC$CLAIM_LIFE_CYCLE.P$CLOSING_CLAIMS_BY_TIMEOUT', SUBSTR(sqlerrm, 1, 1024));
    raise;
  END;

    
/* процедура, реализующая закрытие АТ если все расхождения закрыты */ 
  PROCEDURE P$CLOSING_CLAIMS_BY_DISCREP
  IS 
    v_current_date date := sysdate;
    
    t_claim_id_col T$TABLE_OF_NUMBER;
  BEGIN 
    LOG_INFO('PAC$CLAIM_LIFE_CYCLE.P$CLOSING_CLAIMS_BY_DISCREP', 'Начало работы процессинга закрытия АТ по по причине "закрылись все расхождения" - '||to_char(sysdate, 'dd.mm.yyyy hh24:mi:ss'));

  loop 
	with t1 as (
		select d.doc_id,
			   dd.discrepancy_id,
               sd.status
		from NDS2_MRR_USER.doc d 
		inner join NDS2_MRR_USER.doc_discrepancy dd on d.doc_id = dd.doc_id
		inner join NDS2_MRR_USER.sov_discrepancy sd on sd.id =  dd.discrepancy_id where	d.doc_type = 1 and d.status not in (3,9,10)) 
    select distinct t.doc_id bulk collect into t_claim_id_col
		from t1 t where not exists (select * from t1 where doc_id = t.doc_id and status = 1 );
		     
    if t_claim_id_col is null or t_claim_id_col.count = 0 then
        exit;
      end if;
      
      P$CLOSING_CLAIMS (p_claim_ids => t_claim_id_col,
                        p_close_date => v_current_date,
                        p_status_closed => CLOSE_STATUS,
                        p_reason_closed => CLOSE_REASON_BY_DISCREPANCY);
      
    end loop;
    
    LOG_INFO('PAC$CLAIM_LIFE_CYCLE.P$CLOSING_CLAIMS_BY_DISCREP', 'Завершение работы процессинга закрытия АТ  по по причине "закрылись все расхождения" - '||to_char(sysdate, 'dd.mm.yyyy hh24:mi:ss')); 
  EXCEPTION 
    when others then LOG_ERROR('PAC$CLAIM_LIFE_CYCLE.P$CLOSING_CLAIMS_BY_DISCREP', SUBSTR(sqlerrm, 1, 1024));
    raise;
  END;

/* Закрывает АТ в случае подачи новой корректировки*/
PROCEDURE P$CLOSING_CLAIMS_BY_NEW_CORR
as
    last_exec_param_name constant varchar2(35):='at_close_by_new_corr_exec_date';
    dt_format constant varchar2(25) := 'DD-MM-YYYY HH24:MI:SS';

    t_claim_id_col T$TABLE_OF_NUMBER;
    v_last_exec_dt date;
    v_items_limit number;
    v_exec_dt date := sysdate;
begin
    LOG_INFO('PAC$CLAIM_LIFE_CYCLE.P$CLOSING_CLAIMS_BY_NEW_CORR', 'Начало работы процессинга закрытия АТ по новой корректировке - '||to_char(sysdate, 'dd.mm.yyyy hh24:mi:ss'));
    --Инициализируем переменные
    select to_date(value, dt_format) 
      into v_last_exec_dt 
      from configuration 
      where parameter = last_exec_param_name; 

    select cast(value as number) 
      into v_items_limit 
      from configuration 
      where parameter = LIMIT_AUTOCLOSE_PARAM_NAME; 

    --Кладем в коллекцию все АТ по которым есть новые корректировки
    loop
        with
            new_corr_decl as
            (
                select distinct
                    inn_declarant,
                    kpp_effective,
                    inn_contractor,
                    period_code,
                    fiscal_year,
                    type_code
                from declaration_history
                where
                    insert_date between v_last_exec_dt and v_exec_dt
                    and is_active = 1
            )
        select distinct
            d.doc_id
        bulk collect into t_claim_id_col
        from new_corr_decl ncd
        join declaration_history dh
            on  ncd.inn_declarant = dh.inn_declarant
                and ncd.kpp_effective = dh.kpp_effective
                and ncd.inn_contractor= dh.inn_contractor
                and ncd.period_code = dh.period_code
                and ncd.fiscal_year = dh.fiscal_year
                and ncd.type_code = dh.type_code
        join doc d
            on dh.zip = d.zip
        where
            d.status not in (3,9,10)    -- ошибка отправки/ошибка передачи/закрыто
            and d.doc_type = 1       --АТ
            and rownum <= v_items_limit;

            if(t_claim_id_col.count = 0) then
                exit;
            end if;

        P$CLOSING_CLAIMS(p_claim_ids => t_claim_id_col,
                        p_close_date => v_exec_dt,
                        p_status_closed => CLOSE_STATUS,
                        p_reason_closed => CLOSE_REASON_NEW_CORR);
    end loop;

    --Обновляем дату запуска
    update nds2_mrr_user.configuration
    set value = TO_CHAR(v_exec_dt, dt_format)
    where parameter = last_exec_param_name;
    commit;
    
    LOG_INFO('PAC$CLAIM_LIFE_CYCLE.P$CLOSING_CLAIMS_BY_NEW_CORR', 'Завершение работы процессинга закрытия АТ по новой корректировке - '||to_char(sysdate, 'dd.mm.yyyy hh24:mi:ss'));
  EXCEPTION 
    when others then LOG_ERROR('PAC$CLAIM_LIFE_CYCLE.P$CLOSING_CLAIMS_BY_NEW_CORR', SUBSTR(sqlerrm, 1, 1024));
    raise;
  END;

/* реализуем жизненный цикл АТ */
  PROCEDURE P$PROCESS_CLAIMS_LIFE_CYCLE 
  IS 
  
  BEGIN 
    LOG_INFO('PAC$CLAIM_LIFE_CYCLE.P$PROCESS_CLAIMS_LIFE_CYCLE', 'Начало работы процессинга жизненного цикла АТ - '||to_char(sysdate, 'dd.mm.yyyy hh24:mi:ss'));
    P$RESENDING_CLAIMS(); 
    P$CLOSING_CLAIMS_BY_TIMEOUT(); 
    P$CLOSING_CLAIMS_BY_NEW_CORR(); 
	P$CLOSING_CLAIMS_BY_DISCREP();
    LOG_INFO('PAC$CLAIM_LIFE_CYCLE.P$PROCESS_CLAIMS_LIFE_CYCLE', 'Завершение работы процессинга жизненного цикла АТ - '||to_char(sysdate, 'dd.mm.yyyy hh24:mi:ss'));
  EXCEPTION 
    when others then LOG_ERROR('PAC$CLAIM_LIFE_CYCLE.P$PROCESS_CLAIMS_LIFE_CYCLE', SUBSTR(sqlerrm, 1, 1024));
    raise;
  END;
  
END; 
/ 
