﻿CREATE OR REPLACE PACKAGE NDS2_MRR_USER.NDS2$DISCREPANCIES is

  procedure SaveDiscrepancyFilter
    (
      p_Cd in Discrepancies_Filters.Cd%type,
      p_UserName in Discrepancies_Filters.User_Name%type,
      p_IsFavourite in Discrepancies_Filters.Is_Favourite%type,
      p_Name in Discrepancies_Filters.Name%type,
      p_Data in Discrepancies_Filters.Data%type,
      p_Id out Discrepancies_Filters.Id%type
    );

  procedure GetDiscrepancyFilterByUserName
    (
      UserName in Discrepancies_Filters.User_Name%type,
      FiltersCursor out sys_refcursor
    );

  procedure GetDiscrepancyFilterBySel
    (
      SelectionId in Discrepancies_Filters.Cd%type,
      FiltersCursor out sys_refcursor
    );

  procedure GetFilterCriterias
    (
      FiltersCursor out sys_refcursor
    );

  procedure SaveBuyBookComment
    (
      p_discrepancy_id in discrepancy_comment.discrepancy_id%type,
      p_chapter in discrepancy_comment.chapter%type,
      p_row_key in discrepancy_comment.row_key%type,
      p_buy_accept_date in discrepancy_comment.buy_accept_date%type,
      p_correction_date in discrepancy_comment.correction_date%type,
      p_correction_num in discrepancy_comment.correction_num%type,
      p_currency_code in discrepancy_comment.currency_code%type,
      p_change_correction_date in discrepancy_comment.change_correction_date%type,
      p_change_correction_num in discrepancy_comment.change_correction_num%type,
      p_change_date in discrepancy_comment.change_date%type,
      p_change_num in discrepancy_comment.change_num%type,
      p_broker_inn in discrepancy_comment.broker_inn%type,
      p_broker_kpp in discrepancy_comment.broker_kpp%type,
      p_invoice_date in discrepancy_comment.invoice_date%type,
      p_invoice_num in discrepancy_comment.invoice_num%type,
      p_customs_declaration_num in discrepancy_comment.customs_declaration_num%type,
      p_operation_code in discrepancy_comment.operation_code%type,
      p_seller_inn in discrepancy_comment.seller_inn%type,
      p_seller_kpp in discrepancy_comment.seller_kpp%type,
      p_price_nds_total in discrepancy_comment.price_nds_total%type,
      p_price_total in discrepancy_comment.price_total%type
    );

  procedure SaveSellBookComment
    (
      p_discrepancy_id in discrepancy_comment.discrepancy_id%type,
      p_chapter in discrepancy_comment.chapter%type,
      p_row_key in discrepancy_comment.row_key%type,
      p_buyer_inn in discrepancy_comment.buyer_inn%type,
      p_buyer_kpp in discrepancy_comment.buyer_kpp%type,
      p_receipt_doc_date in discrepancy_comment.receipt_doc_date%type,
      p_receipt_doc_num in discrepancy_comment.receipt_doc_num%type,
      p_correction_date in discrepancy_comment.correction_date%type,
      p_correction_num in discrepancy_comment.correction_num%type,
      p_currency_code in discrepancy_comment.currency_code%type,
      p_change_correction_date in discrepancy_comment.change_correction_date%type,
      p_change_correction_num in discrepancy_comment.change_correction_num%type,
      p_change_date in discrepancy_comment.change_date%type,
      p_change_num in discrepancy_comment.change_num%type,
      p_broker_inn in discrepancy_comment.broker_inn%type,
      p_broker_kpp in discrepancy_comment.broker_kpp%type,
      p_invoice_date in discrepancy_comment.invoice_date%type,
      p_invoice_num in discrepancy_comment.invoice_num%type,
      p_operation_code in discrepancy_comment.operation_code%type,
      p_price_sell_0 in discrepancy_comment.price_sell_0%type,
      p_price_sell_10 in discrepancy_comment.price_sell_10%type,
      p_price_sell_18 in discrepancy_comment.price_sell_18%type,
      p_price_nds_10 in discrepancy_comment.price_nds_10%type,
      p_price_nds_18 in discrepancy_comment.price_nds_18%type,
      p_price_tax_free in discrepancy_comment.price_tax_free%type,
      p_price_sell_in_curr in discrepancy_comment.price_sell_in_curr%type,
      p_price_sell in discrepancy_comment.price_sell%type
    );

  procedure SaveSentJournalComment
    (
      p_discrepancy_id in discrepancy_comment.discrepancy_id%type,
      p_chapter in discrepancy_comment.chapter%type,
      p_row_key in discrepancy_comment.row_key%type,
      p_price_total in discrepancy_comment.price_total%type,
      p_buyer_inn in discrepancy_comment.buyer_inn%type,
      p_buyer_kpp in discrepancy_comment.buyer_kpp%type,
      p_correction_date in discrepancy_comment.correction_date%type,
      p_correction_num in discrepancy_comment.correction_num%type,
      p_diff_correct_decrease in discrepancy_comment.diff_correct_decrease%type,
      p_diff_correct_increase in discrepancy_comment.diff_correct_increase%type,
      p_currency_code in discrepancy_comment.currency_code%type,
      p_change_correction_date in discrepancy_comment.change_correction_date%type,
      p_change_correction_num in discrepancy_comment.change_correction_num%type,
      p_change_date in discrepancy_comment.change_date%type,
      p_change_num in discrepancy_comment.change_num%type,
      p_invoice_date in discrepancy_comment.invoice_date%type,
      p_invoice_num in discrepancy_comment.invoice_num%type,
      p_operation_code in discrepancy_comment.operation_code%type,
      p_seller_inn in discrepancy_comment.seller_inn%type,
      p_seller_kpp in discrepancy_comment.seller_kpp%type,
      p_create_date in discrepancy_comment.create_date%type,
      p_price_nds_total in discrepancy_comment.price_nds_total%type,
      p_diff_correct_nds_decrease in discrepancy_comment.diff_correct_nds_decrease%type,
      p_diff_correct_nds_increase in discrepancy_comment.diff_correct_nds_increase%type
    );

  procedure SaveRecieveJournalComment
    (
      p_discrepancy_id in discrepancy_comment.discrepancy_id%type,
      p_chapter in discrepancy_comment.chapter%type,
      p_row_key in discrepancy_comment.row_key%type,
      p_price_total in discrepancy_comment.price_total%type,
      p_broker_inn in discrepancy_comment.broker_inn%type,
      p_broker_kpp in discrepancy_comment.broker_kpp%type,
      p_deal_kind_code in discrepancy_comment.deal_kind_code%type,
      p_correction_date in discrepancy_comment.correction_date%type,
      p_correction_num in discrepancy_comment.correction_num%type,
      p_diff_correct_decrease in discrepancy_comment.diff_correct_decrease%type,
      p_diff_correct_increase in discrepancy_comment.diff_correct_increase%type,
      p_currency_code in discrepancy_comment.currency_code%type,
      p_change_correction_date in discrepancy_comment.change_correction_date%type,
      p_change_correction_num in discrepancy_comment.change_correction_num%type,
      p_change_date in discrepancy_comment.change_date%type,
      p_change_num in discrepancy_comment.change_num%type,
      p_invoice_date in discrepancy_comment.invoice_date%type,
      p_invoice_num in discrepancy_comment.invoice_num%type,
      p_operation_code in discrepancy_comment.operation_code%type,
      p_seller_inn in discrepancy_comment.seller_inn%type,
      p_seller_kpp in discrepancy_comment.seller_kpp%type,
      p_receive_date in discrepancy_comment.receive_date%type,
      p_price_nds_total in discrepancy_comment.price_nds_total%type,
      p_diff_correct_nds_decrease in discrepancy_comment.diff_correct_nds_decrease%type,
      p_diff_correct_nds_increase in discrepancy_comment.diff_correct_nds_increase%type
    );

  procedure Get_Discrepancy_Side
  (
    pDiscrepancyId in SOV_Discrepancy.Id%type,
    pCursor out sys_refcursor
  );

  procedure GetBuyBooks
  (
    pRowKey in varchar2,
    pDiscrepancyId in SOV_Discrepancy.Id%type,
    pCursor out sys_refcursor
  );

  procedure GetSellBooks
  (
    pRowKey in varchar2,
    pDiscrepancyId in SOV_Discrepancy.Id%type,
    pCursor out sys_refcursor
  );

  procedure GetSentJournal
  (
    pRowKey in varchar2,
    pDiscrepancyId in SOV_Discrepancy.Id%type,
    pCursor out sys_refcursor
  );

  procedure GetRecieveJournal
  (
    pRowKey in varchar2,
    pDiscrepancyId in SOV_Discrepancy.Id%type,
    pCursor out sys_refcursor
  );

end;
/
CREATE OR REPLACE PACKAGE BODY NDS2_MRR_USER.NDS2$DISCREPANCIES is

  procedure SaveDiscrepancyFilter
    (
      p_Cd in Discrepancies_Filters.Cd%type,
      p_UserName in Discrepancies_Filters.User_Name%type,
      p_IsFavourite in Discrepancies_Filters.Is_Favourite%type,
      p_Name in Discrepancies_Filters.Name%type,
      p_Data in Discrepancies_Filters.Data%type,
      p_Id out Discrepancies_Filters.Id%type
    )
  is
  begin
    update
      Discrepancies_Filters
    set
      Cd = p_Cd,
      User_Name = p_UserName,
      Is_Favourite = p_IsFavourite,
      Name = p_Name,
      Data = p_Data
    where Id = p_Id;

    if SQL%ROWCOUNT = 0 then
    begin
        p_Id := SEQ_DISCREPANCIES_FILTER.NEXTVAL;
        insert into Discrepancies_Filters values(p_Id, p_Cd, p_UserName, p_IsFavourite, p_Name, p_Data);
    end;
    end if;
 end;

  procedure GetDiscrepancyFilterByUserName
    (
      UserName in Discrepancies_Filters.User_Name%type,
      FiltersCursor out sys_refcursor
    )
  is
  begin
    open FiltersCursor for
    select * from Discrepancies_Filters where (User_Name = UserName);
  end;

  procedure GetDiscrepancyFilterBySel
    (
      SelectionId in Discrepancies_Filters.Cd%type,
      FiltersCursor out sys_refcursor
    )
  is
  begin
    open FiltersCursor for
    select * from Discrepancies_Filters where (Cd = SelectionId);
  end;

  procedure GetFilterCriterias
    (
      FiltersCursor out sys_refcursor
    )
  is
  begin
    open FiltersCursor for
    select * from Dict_Filter_Types;
  end;

  procedure SaveComment
    (
      p_discrepancy_id in discrepancy_comment.discrepancy_id%type,
      p_chapter in discrepancy_comment.chapter%type,
      p_row_key in discrepancy_comment.row_key%type,
      p_create_date in discrepancy_comment.create_date%type,
      p_receive_date in discrepancy_comment.receive_date%type,
      p_operation_code in discrepancy_comment.operation_code%type,
      p_invoice_num in discrepancy_comment.invoice_num%type,
      p_invoice_date in discrepancy_comment.invoice_date%type,
      p_change_num in discrepancy_comment.change_num%type,
      p_change_date in discrepancy_comment.change_date%type,
      p_correction_num in discrepancy_comment.correction_num%type,
      p_correction_date in discrepancy_comment.correction_date%type,
      p_change_correction_num in discrepancy_comment.change_correction_num%type,
      p_change_correction_date in discrepancy_comment.change_correction_date%type,
      p_receipt_doc_num in discrepancy_comment.receipt_doc_num%type,
      p_receipt_doc_date in discrepancy_comment.receipt_doc_date%type,
      p_buy_accept_date in discrepancy_comment.buy_accept_date%type,
      p_buyer_inn in discrepancy_comment.buyer_inn%type,
      p_buyer_kpp in discrepancy_comment.buyer_kpp%type,
      p_seller_inn in discrepancy_comment.seller_inn%type,
      p_seller_kpp in discrepancy_comment.seller_kpp%type,
      p_seller_invoice_num in discrepancy_comment.seller_invoice_num%type,
      p_seller_invoice_date in discrepancy_comment.seller_invoice_date%type,
      p_broker_inn in discrepancy_comment.broker_inn%type,
      p_broker_kpp in discrepancy_comment.broker_kpp%type,
      p_deal_kind_code in discrepancy_comment.deal_kind_code%type,
      p_customs_declaration_num in discrepancy_comment.customs_declaration_num%type,
      p_currency_code in discrepancy_comment.currency_code%type,
      p_price_buy_amount in discrepancy_comment.price_buy_amount%type,
      p_price_buy_nds_amount in discrepancy_comment.price_buy_nds_amount%type,
      p_price_sell in discrepancy_comment.price_sell%type,
      p_price_sell_in_curr in discrepancy_comment.price_sell_in_curr%type,
      p_price_sell_18 in discrepancy_comment.price_sell_18%type,
      p_price_sell_10 in discrepancy_comment.price_sell_10%type,
      p_price_sell_0 in discrepancy_comment.price_sell_0%type,
      p_price_nds_18 in discrepancy_comment.price_nds_18%type,
      p_price_nds_10 in discrepancy_comment.price_nds_10%type,
      p_price_tax_free in discrepancy_comment.price_tax_free%type,
      p_price_total in discrepancy_comment.price_total%type,
      p_price_nds_total in discrepancy_comment.price_nds_total%type,
      p_diff_correct_decrease in discrepancy_comment.diff_correct_decrease%type,
      p_diff_correct_increase in discrepancy_comment.diff_correct_increase%type,
      p_diff_correct_nds_decrease in discrepancy_comment.diff_correct_nds_decrease%type,
      p_diff_correct_nds_increase in discrepancy_comment.diff_correct_nds_increase%type
    )
    is
    begin
      insert into discrepancy_comment
      values (p_discrepancy_id, p_chapter, p_row_key, p_create_date, p_receive_date, p_operation_code, p_invoice_num, p_invoice_date, p_change_num, p_change_date, p_correction_num, p_correction_date, p_change_correction_num, p_change_correction_date, p_receipt_doc_num, p_receipt_doc_date, p_buy_accept_date, p_buyer_inn, p_buyer_kpp, p_seller_inn, p_seller_kpp, p_seller_invoice_num, p_seller_invoice_date, p_broker_inn, p_broker_kpp, p_deal_kind_code, p_customs_declaration_num, p_currency_code, p_price_buy_amount, p_price_buy_nds_amount, p_price_sell, p_price_sell_in_curr, p_price_sell_18, p_price_sell_10, p_price_sell_0, p_price_nds_18, p_price_nds_10, p_price_tax_free, p_price_total, p_price_nds_total, p_diff_correct_decrease, p_diff_correct_increase, p_diff_correct_nds_decrease, p_diff_correct_nds_increase);
    end;

  procedure SaveBuyBookComment
    (
      p_discrepancy_id in discrepancy_comment.discrepancy_id%type,
      p_chapter in discrepancy_comment.chapter%type,
      p_row_key in discrepancy_comment.row_key%type,
      p_buy_accept_date in discrepancy_comment.buy_accept_date%type,
      p_correction_date in discrepancy_comment.correction_date%type,
      p_correction_num in discrepancy_comment.correction_num%type,
      p_currency_code in discrepancy_comment.currency_code%type,
      p_change_correction_date in discrepancy_comment.change_correction_date%type,
      p_change_correction_num in discrepancy_comment.change_correction_num%type,
      p_change_date in discrepancy_comment.change_date%type,
      p_change_num in discrepancy_comment.change_num%type,
      p_broker_inn in discrepancy_comment.broker_inn%type,
      p_broker_kpp in discrepancy_comment.broker_kpp%type,
      p_invoice_date in discrepancy_comment.invoice_date%type,
      p_invoice_num in discrepancy_comment.invoice_num%type,
      p_customs_declaration_num in discrepancy_comment.customs_declaration_num%type,
      p_operation_code in discrepancy_comment.operation_code%type,
      p_seller_inn in discrepancy_comment.seller_inn%type,
      p_seller_kpp in discrepancy_comment.seller_kpp%type,
      p_price_nds_total in discrepancy_comment.price_nds_total%type,
      p_price_total in discrepancy_comment.price_total%type
    )
    is
  begin
    SaveComment(p_discrepancy_id, p_chapter, p_row_key, null, null, p_operation_code, p_invoice_num, p_invoice_date, p_change_num, p_change_date, p_correction_num, p_correction_date, p_change_correction_num, p_change_correction_date, null, null, p_buy_accept_date, null, null, p_seller_inn, p_seller_kpp, null, null, p_broker_inn, p_broker_kpp, null, p_customs_declaration_num, p_currency_code, null, null, null, null, null, null, null, null, null, null, p_price_total, p_price_nds_total, null, null, null, null);
  end;

  procedure SaveSellBookComment
    (
      p_discrepancy_id in discrepancy_comment.discrepancy_id%type,
      p_chapter in discrepancy_comment.chapter%type,
      p_row_key in discrepancy_comment.row_key%type,
      p_buyer_inn in discrepancy_comment.buyer_inn%type,
      p_buyer_kpp in discrepancy_comment.buyer_kpp%type,
      p_receipt_doc_date in discrepancy_comment.receipt_doc_date%type,
      p_receipt_doc_num in discrepancy_comment.receipt_doc_num%type,
      p_correction_date in discrepancy_comment.correction_date%type,
      p_correction_num in discrepancy_comment.correction_num%type,
      p_currency_code in discrepancy_comment.currency_code%type,
      p_change_correction_date in discrepancy_comment.change_correction_date%type,
      p_change_correction_num in discrepancy_comment.change_correction_num%type,
      p_change_date in discrepancy_comment.change_date%type,
      p_change_num in discrepancy_comment.change_num%type,
      p_broker_inn in discrepancy_comment.broker_inn%type,
      p_broker_kpp in discrepancy_comment.broker_kpp%type,
      p_invoice_date in discrepancy_comment.invoice_date%type,
      p_invoice_num in discrepancy_comment.invoice_num%type,
      p_operation_code in discrepancy_comment.operation_code%type,
      p_price_sell_0 in discrepancy_comment.price_sell_0%type,
      p_price_sell_10 in discrepancy_comment.price_sell_10%type,
      p_price_sell_18 in discrepancy_comment.price_sell_18%type,
      p_price_nds_10 in discrepancy_comment.price_nds_10%type,
      p_price_nds_18 in discrepancy_comment.price_nds_18%type,
      p_price_tax_free in discrepancy_comment.price_tax_free%type,
      p_price_sell_in_curr in discrepancy_comment.price_sell_in_curr%type,
      p_price_sell in discrepancy_comment.price_sell%type
    )
    is
  begin
    SaveComment(p_discrepancy_id, p_chapter, p_row_key, null, null, p_operation_code, p_invoice_num, p_invoice_date, p_change_num, p_change_date, p_correction_num, p_correction_date, p_change_correction_num, p_change_correction_date, p_receipt_doc_num, p_receipt_doc_date, null, p_buyer_inn, p_buyer_kpp, null, null, null, null, p_broker_inn, p_broker_kpp, null, null, p_currency_code, null, null, p_price_sell, p_price_sell_in_curr, p_price_sell_18, p_price_sell_10, p_price_sell_0, p_price_nds_18, p_price_nds_10, p_price_tax_free, null, null, null, null, null, null);
  end;

  procedure SaveSentJournalComment
    (
      p_discrepancy_id in discrepancy_comment.discrepancy_id%type,
      p_chapter in discrepancy_comment.chapter%type,
      p_row_key in discrepancy_comment.row_key%type,
      p_price_total in discrepancy_comment.price_total%type,
      p_buyer_inn in discrepancy_comment.buyer_inn%type,
      p_buyer_kpp in discrepancy_comment.buyer_kpp%type,
      p_correction_date in discrepancy_comment.correction_date%type,
      p_correction_num in discrepancy_comment.correction_num%type,
      p_diff_correct_decrease in discrepancy_comment.diff_correct_decrease%type,
      p_diff_correct_increase in discrepancy_comment.diff_correct_increase%type,
      p_currency_code in discrepancy_comment.currency_code%type,
      p_change_correction_date in discrepancy_comment.change_correction_date%type,
      p_change_correction_num in discrepancy_comment.change_correction_num%type,
      p_change_date in discrepancy_comment.change_date%type,
      p_change_num in discrepancy_comment.change_num%type,
      p_invoice_date in discrepancy_comment.invoice_date%type,
      p_invoice_num in discrepancy_comment.invoice_num%type,
      p_operation_code in discrepancy_comment.operation_code%type,
      p_seller_inn in discrepancy_comment.seller_inn%type,
      p_seller_kpp in discrepancy_comment.seller_kpp%type,
      p_create_date in discrepancy_comment.create_date%type,
      p_price_nds_total in discrepancy_comment.price_nds_total%type,
      p_diff_correct_nds_decrease in discrepancy_comment.diff_correct_nds_decrease%type,
      p_diff_correct_nds_increase in discrepancy_comment.diff_correct_nds_increase%type
    )
    is
  begin
    SaveComment(p_discrepancy_id, p_chapter, p_row_key, p_create_date, null, p_operation_code, p_invoice_num, p_invoice_date, p_change_num, p_change_date, p_correction_num, p_correction_date, p_change_correction_num, p_change_correction_date, null, null, null, p_buyer_inn, p_buyer_kpp, p_seller_inn, p_seller_kpp, null, null, null, null, null, null, p_currency_code, null, null, null, null, null, null, null, null, null, null, p_price_total, p_price_nds_total, p_diff_correct_decrease, p_diff_correct_increase, p_diff_correct_nds_decrease, p_diff_correct_nds_increase);
  end;

  procedure SaveRecieveJournalComment
    (
      p_discrepancy_id in discrepancy_comment.discrepancy_id%type,
      p_chapter in discrepancy_comment.chapter%type,
      p_row_key in discrepancy_comment.row_key%type,
      p_price_total in discrepancy_comment.price_total%type,
      p_broker_inn in discrepancy_comment.broker_inn%type,
      p_broker_kpp in discrepancy_comment.broker_kpp%type,
    p_deal_kind_code in discrepancy_comment.deal_kind_code%type,
      p_correction_date in discrepancy_comment.correction_date%type,
      p_correction_num in discrepancy_comment.correction_num%type,
      p_diff_correct_decrease in discrepancy_comment.diff_correct_decrease%type,
      p_diff_correct_increase in discrepancy_comment.diff_correct_increase%type,
      p_currency_code in discrepancy_comment.currency_code%type,
      p_change_correction_date in discrepancy_comment.change_correction_date%type,
      p_change_correction_num in discrepancy_comment.change_correction_num%type,
      p_change_date in discrepancy_comment.change_date%type,
      p_change_num in discrepancy_comment.change_num%type,
      p_invoice_date in discrepancy_comment.invoice_date%type,
      p_invoice_num in discrepancy_comment.invoice_num%type,
      p_operation_code in discrepancy_comment.operation_code%type,
      p_seller_inn in discrepancy_comment.seller_inn%type,
      p_seller_kpp in discrepancy_comment.seller_kpp%type,
      p_receive_date in discrepancy_comment.receive_date%type,
      p_price_nds_total in discrepancy_comment.price_nds_total%type,
      p_diff_correct_nds_decrease in discrepancy_comment.diff_correct_nds_decrease%type,
      p_diff_correct_nds_increase in discrepancy_comment.diff_correct_nds_increase%type
    )
    is
  begin
    SaveComment(p_discrepancy_id, p_chapter, p_row_key, null, p_receive_date, p_operation_code, p_invoice_num, p_invoice_date, p_change_num, p_change_date, p_correction_num, p_correction_date, p_change_correction_num, p_change_correction_date, null, null, null, null, null, p_seller_inn, p_seller_kpp, null, null, p_broker_inn, p_broker_kpp, p_deal_kind_code, null, p_currency_code, null, null, null, null, null, null, null, null, null, null, p_price_total, p_price_nds_total, p_diff_correct_decrease, p_diff_correct_increase, p_diff_correct_nds_decrease, p_diff_correct_nds_increase);
  end;

  procedure Get_Discrepancy_Side
  (
    pDiscrepancyId in SOV_Discrepancy.Id%type,
    pCursor out sys_refcursor
  )
  as
  begin
    open pCursor for
    select
      discrepancy_id,
      invoice_row_key as InvoiceId,
      Side as SideName,
      chapter,
      declaration_id as DeclarationId,
      Side1Inn,
      Side1Kpp,
      Side1Name,
      Side2Inn,
      Side2Kpp,
      Side2Name,
      invoice_num as SideInfoInvoiceNumber,
      invoice_date as SideInfoInvoiceDate,
      price_total as SideInfoInvoiceAmount,
      price_nds_total as SideInfoTaxAmount,
      region_code as RegionCode,
      region as RegionName,
      soun_code as InspectionCode,
      soun as InspectionName,
      FormatErrors,
      LogicalErrors
      from V$DISCREPANCY_SIDE where discrepancy_id = pDiscrepancyId;
  end;

  procedure GetBuyBooks
  (
    pRowKey in varchar2,
    pDiscrepancyId in SOV_Discrepancy.Id%type,
    pCursor out sys_refcursor
  ) is
  begin
    open pCursor for
    select
      i.row_key as RowKey,
      i.actual_row_key,
      i.ordinal_number as OrdinalNumber,
      i.invoice_num as InvoiceNumber,
      i.invoice_date as InvoiceDate,
      i.change_num as FixedInvoiceNumber,
      i.change_date as FixedInvoiceDate,
      i.correction_num as CorrectedInvoiceNumber,
      i.correction_date as CorrectedInvoiceDate,
      i.change_correction_num as FixedCorrectedInvoiceNumber,
      i.change_correction_date as FixedCorrectedInvoiceDate,
      i.okv_code as CurrencyCode,
      i.is_dop_list as PriznakDopList,
      i.broker_inn as IntermediaryInfoINN,
      i.broker_kpp as IntermediaryInfoKPP,
      (
        select
         tpName.Np_Name
        from 
        mv$tax_payer_name tpName where tpName.Inn  = i.broker_inn and rownum = 1
      ) as IntermediaryInfoName,
      i.customs_declaration_num as NumberTD,
      i.price_total             as TotalAmount,
      i.price_nds_total         as TaxAmount,
      i.logical_errors          as ErrorCodesString
    from sov_invoice_all i
    where (i.row_key = pRowKey or i.actual_row_key = pRowKey)
    and i.discrepancy_id = pDiscrepancyId
    and i.chapter = 8;
  end;

  procedure GetSellBooks
  (
    pRowKey in varchar2,
    pDiscrepancyId in SOV_Discrepancy.Id%type,
    pCursor out sys_refcursor
  ) is
  begin
    open pCursor for
    select distinct
      i.row_key as RowKey,
      i.actual_row_key,
      i.ordinal_number as OrdinalNumber,
      i.invoice_num     as InvoiceNumber,
      i.invoice_date    as InvoiceDate,
      i.change_num      as FixedInvoiceNumber,
      i.change_date     as FixedInvoiceDate,
      i.correction_num  as CorrectedInvoiceNumber,
      i.correction_date as CorrectedInvoiceDate,
      i.change_correction_num  as FixedCorrectedInvoiceNumber,
      i.change_correction_date as FixedCorrectedInvoiceDate,
      i.okv_code        as CurrencyCode,
      i.is_dop_list     as PriznakDopList,
      i.broker_inn as IntermediaryInfoINN,
      i.broker_kpp as IntermediaryInfoKPP,
      (
        select
         tpName.Np_Name
        from 
        mv$tax_payer_name tpName where tpName.Inn  = i.broker_inn and rownum = 1
      ) as IntermediaryInfoName,
      i.price_sell_18          as TaxableAmount18,
      i.price_sell_10          as TaxableAmount10,
      i.price_sell_0           as TaxableAmount0,
      i.price_nds_18           as TaxAmount18,
      i.price_nds_10           as TaxAmount10,
      i.price_tax_free         as TaxFreeAmount,
      i.price_sell_in_curr     as TotalAmountCurrency,
      i.price_sell             as TotalAmountRouble,
      i.logical_errors         as ErrorCodesString
    from sov_invoice_all i
    where (i.row_key = pRowKey or i.actual_row_key = pRowKey)
    and i.discrepancy_id = pDiscrepancyId
    and i.chapter = 9;
  end;

  procedure GetSentJournal
  (
    pRowKey in varchar2,
    pDiscrepancyId in SOV_Discrepancy.Id%type,
    pCursor out sys_refcursor
  ) is
  begin
    open pCursor for
    select
      i.row_key as RowKey,
      i.actual_row_key,
      i.ordinal_number as OrdinalNumber,
      i.invoice_num  as InvoiceNumber,
      i.invoice_date as InvoiceDate,
      i.change_num  as FixedInvoiceNumber,
      i.change_date as FixedInvoiceDate,
      i.correction_num  as CorrectedInvoiceNumber,
      i.correction_date as CorrectedInvoiceDate,
      i.change_correction_num  as FixedCorrectedInvoiceNumber,
      i.change_correction_date as FixedCorrectedInvoiceDate,
      i.okv_code          as CurrencyCode,
      i.create_date as SendDate,
      i.price_total as Amount,
      i.price_nds_total as TaxAmount,
      i.diff_correct_decrease as CostDifferenceDecrease,
      i.diff_correct_increase as CostDifferenceIncrease,
      i.diff_correct_nds_decrease as TaxDifferenceDecrease,
      i.diff_correct_nds_increase as TaxDifferenceIncrease,
      i.logical_errors as ErrorCodesString
    from sov_invoice_all i
    where (i.row_key = pRowKey or i.actual_row_key = pRowKey)
    and i.discrepancy_id = pDiscrepancyId
    and i.chapter = 10;
  end;

  procedure GetRecieveJournal
  (
    pRowKey in varchar2,
    pDiscrepancyId in SOV_Discrepancy.Id%type,
    pCursor out sys_refcursor
  ) is
  begin
    open pCursor for
    select
      i.row_key RowKey,
      i.actual_row_key,
      i.ordinal_number as OrdinalNumber,
      i.invoice_num as InvoiceNumber,
      i.invoice_date as InvoiceDate,
      i.change_num as FixedInvoiceNumber,
      i.change_date as FixedInvoiceDate,
      i.correction_num as CorrectedInvoiceNumber,
      i.correction_date as CorrectedInvoiceDate,
      i.change_correction_num  as FixedCorrectedInvoiceNumber,
      i.change_correction_date as FixedCorrectedInvoiceDate,
      i.okv_code as CurrencyCode,
      i.broker_inn as SubcommissionAgentINN,
      i.broker_kpp as SubcommissionAgentKPP,
      (
        select
         tpName.Np_Name
        from 
        mv$tax_payer_name tpName where tpName.Inn  = i.broker_inn and rownum = 1
      ) as SubcommissionAgentName,
      i.deal_kind_code as DealTypeCode,
      i.receive_date as RecieveDate,
      i.price_total as Amount,
      i.price_nds_total as TaxAmount,
      i.diff_correct_decrease as CostDifferenceDecrease,
      i.diff_correct_increase as CostDifferenceIncrease,
      i.diff_correct_nds_decrease as TaxDifferenceDecrease,
      i.diff_correct_nds_increase as TaxDifferenceIncrease,
      i.logical_errors as ErrorCodesString
    from sov_invoice_all i
    where (i.row_key = pRowKey or i.actual_row_key = pRowKey)
    and i.discrepancy_id = pDiscrepancyId
    and i.chapter = 11 ;
  end;

end;
/
