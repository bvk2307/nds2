﻿CREATE OR REPLACE PACKAGE NDS2_MRR_USER.PAC$VERSION_HISTORY
as
    procedure P$GET_LATEST(pCursor out SYS_REFCURSOR);

    procedure P$GET_ALL(pReleaseNotesOnly in number, pCursor out SYS_REFCURSOR);
end;
/
CREATE OR REPLACE PACKAGE BODY NDS2_MRR_USER.PAC$VERSION_HISTORY
as

    procedure P$GET_LATEST(pCursor out SYS_REFCURSOR)
    as
    begin
        open pCursor for
            SELECT 
                 version_number as VersionNumber
                ,release_date as ReleaseDate
                ,release_file_name as ReleaseFileName
                ,catalog_reindex_required as CatalogReindexRequired
            FROM  version_history 
            WHERE version_number IN 
                    (SELECT DISTINCT 
                            FIRST_VALUE(version_number) 
                                OVER(ORDER BY release_date DESC) AS recent_date
                    FROM version_history);
    end;

    procedure P$GET_ALL(pReleaseNotesOnly in number, pCursor out SYS_REFCURSOR)
    as
    begin
        if(pReleaseNotesOnly = 1) then
            open pCursor for
                SELECT 
                     version_number as VersionNumber
                    ,release_date as ReleaseDate
                    ,release_file_name as ReleaseFileName
                    ,catalog_reindex_required as CatalogReindexRequired
                FROM  version_history 
                WHERE release_file_name IS NOT  NULL 
                ORDER BY release_date;       
        else
            open pCursor for
                SELECT 
                     version_number as VersionNumber
                    ,release_date as ReleaseDate
                    ,release_file_name as ReleaseFileName
                    ,catalog_reindex_required as CatalogReindexRequired
                FROM  version_history 
                ORDER BY release_date;
        end if;
    end;
end;
/