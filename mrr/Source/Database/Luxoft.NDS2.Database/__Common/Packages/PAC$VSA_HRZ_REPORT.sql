﻿create or replace PACKAGE "NDS2_MRR_USER"."PAC$VSA_HRZ_REPORT"
as

procedure TAXPAYER_RELATION
  (
  p_Inn IN varchar2,              /* »ЌЌ Ќѕ */
  p_MaxContractors IN number,     /*  олво наиболее значимых  */
  p_ByPurchase IN NUMBER,         /* направление - в сторону покупок или продаж */
  p_Year varchar2 := null,        /* отетный год */
  p_Qtr varchar2 := null,         /* отчетный период 1-4 */
  p_levels in number := null,     /* ”ровень автоматического построени¤ */
  p_sharePercentNdsCriteria in number := null, /* ќграничение отбора Ќѕ в дерево по % Ќ?— */
  p_minReturnedContractors in number := null, /* минимальное число отобранных по фильтру узлов */
  p_Cursor OUT SYS_REFCURSOR      /* */
   );
end;
/

CREATE OR REPLACE PACKAGE BODY "NDS2_MRR_USER"."PAC$VSA_HRZ_REPORT" 
as

PROCEDURE TAXPAYER_RELATION
(
  p_Inn IN varchar2,              /* »ЌЌ Ќѕ */
  p_MaxContractors IN number,     /*  олво наиболее значимых  */
  p_ByPurchase IN NUMBER,         /* направление - в сторону покупок или продаж */
  p_Year varchar2 := null,        /* отетный год */
  p_Qtr varchar2 := null,         /* отчетный период 1-4 */
  p_levels in number := null,     /* ”ровень автоматического построени¤ */
  p_sharePercentNdsCriteria in number := null, /* ќграничение отбора Ќѕ в дерево по % Ќ?— */
  p_minReturnedContractors in number := null, /* минимальное число отобранных по фильтру узлов */
  p_Cursor OUT SYS_REFCURSOR      /* */
)
as
v_lvl number := 1;
v_year number(4) := p_Year;
v_period number(1) := p_Qtr;
v_maxLevel number := p_levels;
v_min_vat_share number:= nvl(p_sharePercentNdsCriteria,0);
v_inn varchar (12 char) := trim(p_Inn);
v_byPurchase number := p_ByPurchase;
tree_relations_table_parent T$HRZ_TREE_RELATIONS;
tree_relations_table_children T$HRZ_TREE_RELATIONS;
tree_relations_table T$HRZ_TREE_RELATIONS;
items_separator varchar2(2):='^|';
item_elements_separator varchar2(2):='^~';
declaration_type_code number(1):=0;
processingstage number(1):=2;
maxContractors number := p_MaxContractors;
begin
 if (v_maxLevel > 2) then
	maxContractors := 50;
 end if;

 select
    T$HRZ_TREE_RELATION(1,null,null,null,d.inn,1,null,d.compensation_amnt,d.ch8_nds,d.ch9_nds,d.gap_discrep_amnt,null,null,null,declaration_type_code,null,null,processingstage,null,null,null,d.inn||item_elements_separator||egrn.kpp||item_elements_separator||egrn.np_name,egrn.kpp,egrn.np_name,lpad(rownum,10,'0'),0,0,0,0,0,d.nds_discrep_amnt,d.decl_sign,d.transit,null)
    bulk collect into tree_relations_table_parent
 from HRZ_DECL_INFO d
 left join MV$TAX_PAYER_NAME egrn on egrn.inn = d.inn
 where d.inn = v_inn
       and d.year = v_year
       and d.quarter = v_period;

 tree_relations_table := tree_relations_table_parent;

 v_lvl := v_lvl + 1;

 while v_lvl <= v_maxLevel loop
  if v_byPurchase <> 0 then -- РѕС‚ РїРѕРєСѓРїР°С‚РµР»В¤
   begin
    select
      T$HRZ_TREE_RELATION(rownum
     ,d.inn_1
     ,d.parent_vat_deduction_total
     ,d.parent_vat_calculation_total
     ,d.inn_2
     ,v_lvl
     ,null
     ,d.total_nds
     ,d.deduction_nds
     ,d.calc_nds
     ,d.gap_discrep_amnt
     ,d.gap_amount
     ,d.nba
     ,d.nsa
     ,declaration_type_code
     ,d.soun_code
     ,d.soun
     ,d.processingstage
     ,d.region
     ,d.region_code
     ,d.region_name
     ,d.all_parents||items_separator||d.inn_2||item_elements_separator||egrn.kpp||item_elements_separator||egrn.np_name
     ,egrn.kpp
     ,egrn.np_name
     ,d.path||'|'||lpad(rownum,10,'0')
     ,d.njsa
     ,d.njba
     ,d.before2015_amount
     ,d.after2015_amount
     ,d.nds_amount
     ,d.nds_discrep_amnt
     ,d.decl_sign
     ,d.transit
     ,d.year_quarter_vat_list)
    bulk collect into tree_relations_table_children
    from (
    select data.inn_1
           ,parent_np.vat_deduction_total as parent_vat_deduction_total
           ,parent_np.vat_calculation_total as parent_vat_calculation_total
           ,data.inn_2
           ,data.total_nds
           ,data.deduction_nds
           ,data.calc_nds
           ,data.gap_discrep_amnt
           ,data.gap_amount
           ,data.nba
           ,data.nsa
           ,declaration_type_code
           ,data.soun_code
           ,data.soun
           ,processingstage
           ,data.region
           ,data.region_code
           ,data.region_code||' '||data.region as region_name
           ,parent_np.all_parents as all_parents
           ,data.nba_order
           ,parent_np.path
           ,data.njsa
           ,data.njba
           ,data.before2015_amount
           ,data.after2015_amount
           ,data.nds_amount
           ,data.nds_discrep_amnt
           ,data.decl_sign_2 as decl_sign
           ,data.transit_2 as transit
           ,data.year_quarter_vat_list
    from
     HRZ_RELATIONS data
     join table(tree_relations_table_parent) parent_np
      on   parent_np.lvl = v_lvl - 1
       and data.inn_1 = parent_np.inn
       and data.year = v_year
       and data.quarter = v_period
       and data.nba_order <= maxContractors
     where data.own_relation != 1 and
           data.nba > 0 and
           data.nba_order <= maxContractors and
         (v_min_vat_share = 0 or data.nba_order <= p_minReturnedContractors
      or 100 * data.nba > v_min_vat_share * parent_np.vat_deduction_total)) d
     left join MV$TAX_PAYER_NAME egrn on egrn.inn = d.inn_2
     order by d.nba_order;
   end;
  else -- РѕС‚ РїСЂРѕРґР°РІС†Р°
   begin
    select
      T$HRZ_TREE_RELATION(rownum
     ,d.inn_1
     ,d.parent_vat_deduction_total
     ,d.parent_vat_calculation_total
     ,d.inn_2
     ,v_lvl
     ,null
     ,d.total_nds
     ,d.deduction_nds
     ,d.calc_nds
     ,d.gap_discrep_amnt
     ,d.gap_amount
     ,d.nba
     ,d.nsa
     ,declaration_type_code
     ,d.soun_code
     ,d.soun
     ,d.processingstage
     ,d.region
     ,d.region_code
     ,d.region_name
     ,d.all_parents||items_separator||d.inn_2||item_elements_separator||egrn.kpp||item_elements_separator||egrn.np_name
     ,egrn.kpp
     ,egrn.np_name
     ,d.path||'|'||lpad(rownum,10,'0')
     ,d.njsa
     ,d.njba
     ,d.before2015_amount
     ,d.after2015_amount
     ,d.nds_amount
     ,d.nds_discrep_amnt
     ,d.decl_sign
     ,d.transit
     ,d.year_quarter_vat_list)
    bulk collect into tree_relations_table_children
    from (
    select data.inn_1
           ,parent_np.vat_deduction_total as parent_vat_deduction_total
           ,parent_np.vat_calculation_total as parent_vat_calculation_total
           ,data.inn_2
           ,data.total_nds
           ,data.deduction_nds
           ,data.calc_nds
           ,data.gap_discrep_amnt
           ,data.gap_amount
           ,data.nba
           ,data.nsa
           ,declaration_type_code
           ,data.soun_code
           ,data.soun
           ,processingstage
           ,data.region
           ,data.region_code
           ,data.region_code||' '||data.region as region_name
           ,parent_np.all_parents as all_parents
           ,data.nsa_order
           ,parent_np.path
           ,data.njsa
           ,data.njba
           ,data.before2015_amount
           ,data.after2015_amount
           ,data.nds_amount
           ,data.nds_discrep_amnt
           ,data.decl_sign_2 as decl_sign
           ,data.transit_2 as transit
           ,data.year_quarter_vat_list
     from
     HRZ_RELATIONS data
     join table(tree_relations_table_parent) parent_np
      on   parent_np.lvl = v_lvl - 1
       and data.inn_1 = parent_np.inn
       and data.year = v_year
       and data.quarter = v_period
       and data.nsa_order <= maxContractors
     where data.own_relation != 1 and
          data.nsa > 0 and
          data.nsa_order <= maxContractors and
          (v_min_vat_share = 0 or data.nsa_order <= p_minReturnedContractors
          or 100 * data.nsa > v_min_vat_share * parent_np.vat_calculation_total)) d
      left join MV$TAX_PAYER_NAME egrn on egrn.inn = d.inn_2
      order by d.nsa_order;
   end;
  end if;
  v_lvl := v_lvl + 1;

  select
      T$HRZ_TREE_RELATION(t.id
     ,t.parent_inn
     ,t.vat_deduction_total
     ,t.vat_calculation_total
     ,t.inn
     ,t.lvl
     ,t.declaration_type
     ,t.vat_total
     ,t.vat_deduction_total
     ,t.vat_calculation_total
     ,t.gap_discrepancy_total
     ,t.gap_discrepancy
     ,t.vat_deduction
     ,t.vat_calculation
     ,t.decl_type_code
     ,t.soun_code
     ,t.soun_name
     ,t.processingstage
     ,t.region
	   ,t.region_code
	   ,t.region_name
     ,t.all_parents
     ,t.kpp
     ,t.name
     ,t.path
     ,t.njsa
     ,t.njba
     ,t.before2015_amount
     ,t.after2015_amount
     ,t.nds_amount
     ,t.nds_discrep_amnt
     ,t.decl_sign
     ,t.transit
     ,t.year_quarter_vat_list)
  bulk collect into tree_relations_table_parent
  from table(tree_relations_table_children) t
  where t.inn not in (select t1.inn from table(tree_relations_table) t1);

  if tree_relations_table_children.count > 0 then
    for i in tree_relations_table_children.first..tree_relations_table_children.last
    loop
     tree_relations_table.extend();
     tree_relations_table(tree_relations_table.count) := tree_relations_table_children(i);
    end loop;
  end if;

 end loop;

  open p_Cursor for
  select
    tree.id
   ,tree.parent_inn
   ,tree.lvl
   ,tree.inn
   ,tree.name as name
   ,tree.kpp
   ,tree.declaration_type
   ,nvl(tree.vat_calculation_total,0) as calc_nds
   ,nvl(tree.vat_deduction_total,0) as deduction_nds
   ,summary.buyers_cnt
   ,summary.sellers_cnt
   ,cast((case
     when nvl(tree.vat_calculation_total,0) = 0
     then 0
     else 100 * nvl(tree.vat_deduction_total,0) / tree.vat_calculation_total
    end) as number(19,2)) as share_nds_amnt
   ,nvl(sur.sign_code, 4) as sur
   ,cast((case when v_byPurchase <> 0 then nvl(rev.nsa,0) else nvl(tree.vat_calculation,0) end) as number(19,2)) as mapped_amount
   ,cast((case when v_byPurchase <> 0 then nvl(tree.vat_deduction,0) else nvl(rev.nba,0) end) as number(19,2)) as not_mapped_amount
   ,cast((nvl(case when v_byPurchase <> 0 then tree.gap_discrepancy else rev.gap_amount end,0)) as number(19,2)) as discrepancy_amnt
   ,cast((case
     when v_byPurchase <> 0 and nvl(tree.parent_vat_deduction_total,0) <> 0 then 100*nvl(tree.vat_deduction,0)/tree.parent_vat_deduction_total
     when v_byPurchase <> 0 and nvl(tree.parent_vat_calculation_total,0) <> 0 then 100*nvl(tree.vat_calculation,0)/tree.parent_vat_calculation_total
     else 0.00
   end) as number(19,2)) as vat_share
   ,nvl(tree.vat_total,0) as vat_total
   ,tree.gap_discrepancy_total as gap_discrepancy_total
   ,1 as sell_amnt
   ,2 as buy_amnt
   ,3 as total_purchase_amnt
   ,4 as total_sales_amnt
   ,tree.decl_type_code
   ,tree.soun_code
   ,tree.soun_name
   ,tree.processingstage
   ,surd.color
   ,surd.description
   ,surd.is_default
   ,tree.region
	 ,tree.region_code
	 ,tree.region_name
   ,tree.all_parents
   ,cast((case when v_byPurchase = 0 then nvl(tree.njsa,0) else nvl(rev.njsa,0) end) as number(19,2)) as njsa
   ,cast((case when v_byPurchase = 0 then nvl(rev.njba,0) else nvl(tree.njba,0) end) as number(19,2)) as njba
   ,cast((case when v_byPurchase <> 0 then nvl(tree.before2015_amount,0) else nvl(rev.before2015_amount,0) end) as number(19,2)) as before2015_amount
   ,cast((case when v_byPurchase <> 0 then nvl(tree.after2015_amount,0) else nvl(rev.after2015_amount,0) end) as number(19,2)) as after2015_amount
   ,cast((nvl(case when v_byPurchase <> 0 then tree.nds_amount else rev.nds_amount end,0)) as number(19,2)) as nds_amount
   ,tree.nds_discrep_amnt
   ,cast((case when v_byPurchase <> 0
   then
        (case when nvl(tree.vat_calculation_total, 0) = 0  then 0 else 100 * nvl(rev.nsa,0) / tree.vat_calculation_total end)
   else
        (case when nvl(tree.parent_vat_calculation_total, 0) = 0  then 0 else 100 * nvl(tree.vat_calculation,0) / tree.parent_vat_calculation_total end)
   end) as number(19,2)) as seller_nds_prc
   ,cast((case when v_byPurchase <> 0
   then
        (case when nvl(tree.parent_vat_deduction_total, 0) = 0  then 0 else 100 * nvl(tree.vat_deduction,0) / tree.parent_vat_deduction_total end)
   else
        (case when nvl(tree.vat_deduction_total, 0) = 0  then 0 else 100 * nvl(rev.nba,0) / tree.vat_deduction_total end)
   end) as number(19,2)) as client_nds_prc
   ,tree.decl_sign as sign_code
   ,tree.transit
   ,tree.year_quarter_vat_list
  from
  (select * from table(tree_relations_table)) tree
   left join HRZ_RELATIONS_TOTAL summary
    on   summary.year = v_year
     and summary.quarter = v_period
     and summary.inn = tree.inn
   left join
   (select t.*
    from HRZ_RELATIONS t
    where t.own_relation != 1) rev
     on rev.year = v_year
     and rev.quarter = v_period
     and rev.inn_1 = tree.inn
     and rev.inn_2 = tree.parent_inn
   left join
    (select
      x.sign_code
     ,x.inn
     ,row_number() over (partition by x.inn order by x.update_date desc) as idx
    from EXT_SUR x
    where x.is_actual = 1 and x.fiscal_year = v_year and x.fiscal_period = '2'||v_period
     ) sur on sur.inn = tree.inn and sur.idx = 1
    left join (select s.code as code
                 ,s.description as description
                 ,s.color_argb as color
                 ,s.is_default as is_default
          from dict_sur s) surd on surd.code = nvl(sur.sign_code, 4)
    order by tree.path;
end;
end;
/
