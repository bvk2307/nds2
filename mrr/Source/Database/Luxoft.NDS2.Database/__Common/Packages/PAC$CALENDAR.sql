﻿create or replace package NDS2_MRR_USER.PAC$CALENDAR
as

FUNCTION F$FISCAL_YEAR(P_DATE DATE) RETURN NUMBER;

FUNCTION F$GET_TAX_PERIOD_BY_DATE(P_DATE DATE) RETURN varchar;

FUNCTION F$TAX_PERIOD_TO_QUARTER (P_PERIOD NUMBER) RETURN NUMBER;

function F$ADD_WORK_DAYS(p_date date, p_days_count number) return date;



end;
/
create or replace package body NDS2_MRR_USER.PAC$CALENDAR
as
FUNCTION F$FISCAL_YEAR(P_DATE DATE) RETURN NUMBER
  AS
    v_year number;
    v_day number;
    v_month number;
  BEGIN
    v_year := extract(year from p_date);
    v_month := extract (month from p_date) - 3;
    v_day := extract(day from p_date);
    if (v_day < 26) then
      v_month := v_month - 1;
    end if;
    if (v_month < 1) then
      v_year := v_year - 1;
      v_month := 12 + v_month;
    end if;
    return v_year;
  END;

FUNCTION F$GET_TAX_PERIOD_BY_DATE(P_DATE DATE) RETURN varchar
  AS
    v_year number;
    v_day number;
    v_month number;
  BEGIN
    v_year := extract(year from p_date);
    v_month := extract (month from p_date) - 3;
    v_day := extract(day from p_date);
    if (v_day < 26) then
      v_month := v_month - 1;
    end if;
    if (v_month < 1) then
      v_year := v_year - 1;
      v_month := 12 + v_month;
    end if;
    return to_char(v_month, '00');
  END;

FUNCTION F$TAX_PERIOD_TO_QUARTER (P_PERIOD NUMBER) RETURN NUMBER
AS
  v_period number;
BEGIN
    v_period := case
       when P_PERIOD in ('01', '02', '03', '21', '51', '71', '72', '73') then 1
       when P_PERIOD in ('04', '05', '06', '22', '54', '74', '75', '76') then 2
       when P_PERIOD in ('07', '08', '09', '23', '55', '77', '78', '79') then 3
       when P_PERIOD in ('10', '11', '12', '24', '56', '80', '81', '82') then 4
     end;
  return v_period;
END;
/*добавление рабочих дней к дате */
function F$ADD_WORK_DAYS(
p_date date, --входная дата
p_days_count number --добавляемое число рабочих дней (при передаче отрицательного числа дни будут вычтены из входной даты)
) return date
as
  v_result_date date;
  v_rem_days number; -- осталось вычесть дней
  v_skip_day boolean; -- пропустить выходной день

  TYPE reddday_type IS TABLE OF redday%ROWTYPE INDEX BY PLS_INTEGER;
  t_reddays reddday_type; --список неучитываемых выходных
begin
  if p_days_count = 0 then
    v_result_date := p_date;
  elsif p_days_count > 0 then 
    v_result_date := p_date;
    v_rem_days := p_days_count;

    select * 
    bulk collect into t_reddays
    from redday r where r.red_date between p_date and p_date + (p_days_count + 100);

    while ( v_rem_days > 0 )
    loop
      v_result_date := v_result_date + 1;
      v_skip_day := false;
      for idx in 1..t_reddays.count
      loop
        if trunc(t_reddays(idx).red_date) = trunc(v_result_date) then
          v_skip_day := true;
          exit;
        end if;
      end loop;

      if not v_skip_day then
        v_rem_days := v_rem_days - 1;
      end if;

    end loop;
  else 
    v_result_date := p_date;
    v_rem_days := abs(p_days_count);

    select * 
    bulk collect into t_reddays
    from redday r where r.red_date between p_date + (p_days_count - 100) and p_date;

    while ( v_rem_days > 0 )
    loop
      v_result_date := v_result_date - 1;
      v_skip_day := false;
      for idx in 1..t_reddays.count
      loop
        if trunc(t_reddays(idx).red_date) = trunc(v_result_date) then
          v_skip_day := true;
          exit;
        end if;
      end loop;

      if not v_skip_day then
        v_rem_days := v_rem_days - 1;
      end if;

    end loop;
  end if;

 return v_result_date;
end;



end;
/
