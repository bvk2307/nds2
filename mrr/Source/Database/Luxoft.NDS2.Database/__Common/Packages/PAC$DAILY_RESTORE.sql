﻿create or replace package NDS2_MRR_USER.PAC$DAILY_RESTORE as
  /*Запускает пересчет количества автотребований для признака значимых изменений*/
  procedure RECALC_SLIDE_AT;
  
  /*Обновляет назначения инспекторов в агрегатах деклараций после дэйли джоба*/
  procedure P$RESTORE_ASSIGNMENTS;
end;
/

create or replace package body NDS2_MRR_USER.PAC$DAILY_RESTORE as

procedure RECALC_SLIDE_AT
as
    v_limit_date date;
    v_idx number;
begin
  v_limit_date := PAC$CALENDAR.F$ADD_WORK_DAYS(sysdate,-3);
  NDS2$SYS.LOG_INFO('PAC$DAILY_RESTORE', null, null, 'Обновление количества просроченных отправок АТ. Контрольная дата: '||to_char(v_limit_date,'DD-MON-YYYY HH24:MI:SS'));

  begin
   execute immediate 'truncate table SLIDE_AT_COUNT_TEMP';
   exception when others then
     NDS2$SYS.LOG_ERROR('PAC$DAILY_RESTORE', null, sqlcode, substr(sqlerrm, 1, 256));
  end;

  insert /*+ append parallel(SLIDE_AT_COUNT_TEMP, 16) */ into SLIDE_AT_COUNT_TEMP
   select
    dh.TYPE_CODE,
    dh.INN_CONTRACTOR,
    dh.KPP_EFFECTIVE,
    dh.FISCAL_YEAR,
    dh.PERIOD_CODE,
    sum (case when d.EXTERNAL_DOC_DATE < v_limit_date then 1 else 0 end) as SLIDE_COUNT
    from DOC d
    join NDS2_SEOD.SEOD_DECLARATION SD   on d.REF_ENTITY_ID = sd.DECL_REG_NUM and d.SONO_CODE = sd.SONO_CODE
    join DECLARATION_HISTORY dh          on sd.INN               = dh.INN_CONTRACTOR
                                        and sd.KPP               = dh.KPP
                                        and sd.CORRECTION_NUMBER = dh.CORRECTION_NUMBER
                                        and sd.TAX_PERIOD        = dh.PERIOD_CODE
                                        and sd.FISCAL_YEAR       = dh.FISCAL_YEAR
                                        and dh.IS_ACTIVE = 1
      where (d.EXTERNAL_DOC_DATE is not null) and (d.TAX_PAYER_SEND_DATE is null) and (d.CLOSE_DATE is null) and
             d.DOC_TYPE = 1 -- учитываем только АТ по СФ
    group by dh.INN_CONTRACTOR,
             dh.KPP_EFFECTIVE,
             dh.PERIOD_CODE,
             dh.FISCAL_YEAR,
             dh.TYPE_CODE;

  commit;
  
  select COUNT(1) into v_idx from SLIDE_AT_COUNT_TEMP;
  NDS2$SYS.LOG_ERROR('PAC$DAILY_RESTORE', null, null, 'SLIDE_AT_COUNT_TEMP: Количество просроченных отправок: '||to_char(v_idx));

  update SIGNIFICANT_CHANGES
         set SLIDE_AT_COUNT = 0,
             HAS_CHANGES = (case when (NVL(HAS_AT,0) + NVL(NO_TKS_COUNT,0)) > 0 then 1 else 0 end)
         where SLIDE_AT_COUNT is not null and SLIDE_AT_COUNT>0;
  commit;
  update DECLARATION_ACTIVE
         set SLIDE_AT_COUNT = 0,
             HAS_CHANGES = (case when (NVL(HAS_AT,0) + NVL(NO_TKS_COUNT,0)) > 0 then 1 else 0 end)
         where SLIDE_AT_COUNT is not null and SLIDE_AT_COUNT>0;
  commit;
  update DECLARATION_ACTIVE_SEARCH
         set SLIDE_AT_COUNT = 0,
             HAS_CHANGES = (case when (NVL(HAS_AT,0) + NVL(NO_TKS_COUNT,0)) > 0 then 1 else 0 end)
         where SLIDE_AT_COUNT is not null and SLIDE_AT_COUNT>0;
  commit;

  if ( v_idx>0 ) then
      merge into SIGNIFICANT_CHANGES trg
      using ( select INN_CONTRACTOR, KPP_EFFECTIVE, PERIOD_CODE, FISCAL_YEAR,TYPE_CODE, SLIDE_COUNT From SLIDE_AT_COUNT_TEMP ) src
        on
        (
           trg.TYPE_CODE      = src.TYPE_CODE and
           trg.INN_CONTRACTOR = src.INN_CONTRACTOR and
           trg.KPP_EFFECTIVE  = src.KPP_EFFECTIVE and
           trg.FISCAL_YEAR    = src.FISCAL_YEAR and
           trg.PERIOD_CODE    = src.PERIOD_CODE
        )
      when matched then
        update set
          trg.HAS_CHANGES = 1,
          trg.SLIDE_AT_COUNT = nvl(src.SLIDE_COUNT,0)
        where nvl(trg.SLIDE_AT_COUNT,0) != nvl(src.SLIDE_COUNT,0)
      when not matched then
        insert (  INN_CONTRACTOR,
                KPP_EFFECTIVE,
                PERIOD_CODE,
                FISCAL_YEAR,
                TYPE_CODE,
                HAS_CHANGES,
                SLIDE_AT_COUNT)
        values (src.INN_CONTRACTOR,
                src.KPP_EFFECTIVE,
                src.PERIOD_CODE,
                src.FISCAL_YEAR,
                src.TYPE_CODE,
                1,
                nvl(src.SLIDE_COUNT,0));
      commit;
      NDS2$SYS.LOG_INFO('PAC$DAILY_RESTORE', null, null, 'Обновлена таблица признаков SIGNIFICANT_CHANGES');

      merge into DECLARATION_ACTIVE trg
      using ( select INN_CONTRACTOR, KPP_EFFECTIVE, PERIOD_CODE, FISCAL_YEAR,TYPE_CODE, SLIDE_COUNT from SLIDE_AT_COUNT_TEMP ) src
        on (
               trg.INN_CONTRACTOR = src.INN_CONTRACTOR and
               trg.KPP_EFFECTIVE  = src.KPP_EFFECTIVE and
               trg.PERIOD_CODE    = src.PERIOD_CODE and
               trg.FISCAL_YEAR    = src.FISCAL_YEAR and
               trg.TYPE_CODE      = src.TYPE_CODE
        )
      when matched then
      update set
        trg.HAS_CHANGES = 1,
        trg.SLIDE_AT_COUNT = nvl(src.SLIDE_COUNT,0)
        where nvl(trg.SLIDE_AT_COUNT,0)!= nvl(src.SLIDE_COUNT,0);
      commit;
      NDS2$SYS.LOG_INFO('PAC$DAILY_RESTORE', null, null, 'Обновлен агрегат DECLARATION_ACTIVE');

      merge into DECLARATION_ACTIVE_SEARCH trg
      using ( select INN_CONTRACTOR, KPP_EFFECTIVE, PERIOD_CODE, FISCAL_YEAR,TYPE_CODE, SLIDE_COUNT From SLIDE_AT_COUNT_TEMP ) src
        on (
               trg.INN_CONTRACTOR = src.INN_CONTRACTOR and
               trg.KPP_EFFECTIVE  = src.KPP_EFFECTIVE and
               trg.TAX_PERIOD     = src.PERIOD_CODE and
               trg.FISCAL_YEAR    = src.FISCAL_YEAR and
               trg.DECL_TYPE_CODE = src.TYPE_CODE
        )
        when matched then
        update set
        HAS_CHANGES = 1,
        trg.SLIDE_AT_COUNT = nvl(src.SLIDE_COUNT,0)
        where nvl(trg.SLIDE_AT_COUNT,0)!= nvl(src.SLIDE_COUNT,0);
      commit;
      NDS2$SYS.LOG_INFO('PAC$DAILY_RESTORE', null, null, 'Обновлен агрегат поиска DECLARATION_ACTIVE_SEARCH');
  else
    NDS2$SYS.LOG_INFO('PAC$DAILY_RESTORE', null, null, 'Нет оснований для обновления агрегатов');
  end if;
  NDS2$SYS.LOG_INFO('PAC$DAILY_RESTORE', null, null, 'Завершено обновление количества просроченных отправок АТ.');

  exception when others then
    rollback;
    NDS2$SYS.LOG_ERROR('PAC$DAILY_RESTORE', null, sqlcode, 'RECALC_SLIDE_AT: '||sqlerrm);
end;

procedure P$RESTORE_ASSIGNMENTS
as
    type tmp_assign_type is table of tmp_declaration_assignment%rowtype;
    tmp_assign_col tmp_assign_type;
begin
    select tda.*
    bulk collect into tmp_assign_col
    from tmp_declaration_assignment tda;

    forall i in 1..tmp_assign_col.count
        update declaration_active
        set inspector = tmp_assign_col(i).assigned_to_name,
            inspector_sid = tmp_assign_col(i).assigned_to_sid
        where zip = tmp_assign_col(i).zip;
    commit;
    NDS2$SYS.LOG_INFO('PAC$DAILY_RESTORE', null, null, 'P$RESTORE_ASSIGNMENTS: DECLARATION_ACTIVE');

    forall i in 1..tmp_assign_col.count
        update declaration_active_search
        set assignee_name = tmp_assign_col(i).assigned_to_name,
            assignee_sid = tmp_assign_col(i).assigned_to_sid
        where declaration_version_id = tmp_assign_col(i).zip;
    commit;
    NDS2$SYS.LOG_INFO('PAC$DAILY_RESTORE', null, null, 'P$RESTORE_ASSIGNMENTS: DECLARATION_ACTIVE_SEARCH');

    execute immediate 'truncate table tmp_declaration_assignment';
    commit;
  exception when others then
    rollback;
    NDS2$SYS.LOG_ERROR('PAC$DAILY_RESTORE', null, sqlcode, 'P$RESTORE_ASSIGNMENTS: '||sqlerrm);
end;

end;
/
