﻿create or replace package NDS2_MRR_USER.PAC$KNP_RESULT_DOCUMENTS is

-- Создает Акт для указанной декларации
procedure P$INSERT_ACT(
  pId OUT ACT.ID%type,
  pInnDeclarant IN ACT.INN_DECLARANT%type,
  pInn IN ACT.INN%type,
  pKpp IN ACT.KPP%type,
  pKppEffective IN ACT.KPP_EFFECTIVE%type,
  pPeriodCode IN ACT.PERIOD_CODE%type,
  pFiscalYear IN ACT.FISCAL_YEAR%type
);

procedure P$UPDATE_ACT (
  pId in ACT.ID%type,
  pCloseDate in ACT.CLOSE_DATE%type
);

procedure P$INSERT_DECISION(
  pId out DECISION.ID%type,
  pInnDeclarant in DECISION.INN_DECLARANT%type,
  pInn in DECISION.INN%type,
  pKpp in DECISION.KPP%type,
  pKppEffective in DECISION.KPP_EFFECTIVE%type,
  pFiscalYear in DECISION.FISCAL_YEAR%type,
  pPeriodCode in DECISION.PERIOD_CODE%type
);

procedure P$UPDATE_DECISION(
  pId in DECISION.ID%type,
  pCloseDate in DECISION.CLOSE_DATE%type
);

procedure P$ACT_DELETE_ALL_CLOSED(pDocumentId in ACT_DISCREPANCY.ACT_ID%type);

procedure P$DECISION_DELETE_ALL_CLOSED(pDocumentId in DECISION_DISCREPANCY.DECISION_ID%type);

procedure P$ACT_DISCREPANCY_START_APPEND(
  pId in ACT_DISCREPANCY.ACT_ID%type);

procedure P$ACT_DISCREPANCY_APPEND(
  pId in ACT_DISCREPANCY.ACT_ID%type);

procedure P$ACT_CLOSE(pId in ACT_DISCREPANCY.ACT_ID%type);

procedure P$ACT_DIS_ROLLBACK_APPEND(
  pId in ACT_DISCREPANCY.ACT_ID%type);

procedure P$DECISION_START_APPEND(pId in DECISION.ID%type);

procedure P$DECISION_COMMIT_APPEND(pId in DECISION.ID%type);

procedure P$DECISION_CLOSE(pId in DECISION.ID%type);

procedure P$DECISION_ROLLBACK_APPEND(pId in DECISION.ID%type);

procedure P$UPDATE_ACT_DISCREPANCY(
  pDiscrepancyId IN ACT_DISCREPANCY.ID%type,
  pAmount IN ACT_DISCREPANCY.AMOUNT%type
);

procedure P$UPDATE_DECISION_DISCREPANCY(
  pDiscrepancyId in DECISION_DISCREPANCY.ID%type,
  pAmount in DECISION_DISCREPANCY.AMOUNT%type);

procedure P$GET_SESSION(
  pDocumentId IN ACT_DECISION_APPEND_SESSION.DOCUMENT_ID%type,
  pSession OUT SYS_REFCURSOR
);

procedure P$INSERT_SESSION(
  pDocumentId IN ACT_DECISION_APPEND_SESSION.DOCUMENT_ID%type,
  pUserSid IN ACT_DECISION_APPEND_SESSION.USER_ID%type,
  pUserName IN ACT_DECISION_APPEND_SESSION.USER_NAME%type,
  pExpiredAt in ACT_DECISION_APPEND_SESSION.EXPIRED_AT%type
);

procedure P$UPDATE_SESSION(
  pDocumentId in ACT_DECISION_APPEND_SESSION.DOCUMENT_ID%type,
  pExpiredAt in ACT_DECISION_APPEND_SESSION.EXPIRED_AT%type);

procedure P$DELETE_SESSION(pDocumentId in ACT_DECISION_APPEND_SESSION.DOCUMENT_ID%type);

procedure P$GET_SESSION_DURATION(pDuration out number);

procedure P$KILL_EXPIRED_SESSIONS;

procedure P$CLOSE_DISCREPANCY;

procedure P$UPDATE_DISCREPANCY_STATUS;

procedure P$GET_DECISION_UPDATE_STATE(
  pEnabled out number);

procedure P$ENABLE_DECISION_UPDATE;

procedure P$DISABLE_DECISION_UPDATE;

procedure P$ACT_DIS_INVALID_COUNT(
  pId IN ACT_DISCREPANCY.ID%type,
  pCount out number
);

procedure P$DECISION_DIS_INVALID_COUNT(
  pId IN DECISION_DISCREPANCY.ID%type,
  pCount out number
);

procedure P$ACT_DIS_REMOVE(
  pDocumentId IN ACT_DISCREPANCY.ACT_ID%type,
  pDiscrepancyId IN ACT_DISCREPANCY.ID%type
);

procedure P$DECISION_DIS_REMOVE(
  pDocumentId IN DECISION_DISCREPANCY.DECISION_ID%type,
  pDiscrepancyId IN DECISION_DISCREPANCY.ID%type
);

procedure P$PREPARE_REPORT_DATA(pReportDate in Date);

procedure P$AGGREGATE_DAILY;

procedure P$GET_REPORT_ID (
  pId out KNP_RESULT_REPORT.ID%type,
  pFiscalYear in KNP_RESULT_REPORT.FISCAL_YEAR%type,
  pQuarter in KNP_RESULT_REPORT.QUARTER%type,
  pReportDate in KNP_RESULT_REPORT.REPORT_DATE%type);
  
procedure P$GET_REPORT_PERIODS(
  pResult out sys_refcursor);

procedure P$GET_ACTS_BY_DECLARATION(
  pInnDeclarant in varchar2,
  pInnContractor in varchar2,
  pKppEffective in varchar2,
  pPeriodEffective in number,
  pYear in varchar2,
  pCursor out SYS_REFCURSOR
);

procedure P$GET_DECISIONS_BY_DECLARATION( 
  pInnDeclarant in varchar2,
  pInnContractor in varchar2,
  pKppEffective in varchar2,
  pPeriodEffective in number,
  pYear in varchar2,
  pCursor out SYS_REFCURSOR
);

end;
/
create or replace package body NDS2_MRR_USER.PAC$KNP_RESULT_DOCUMENTS is

procedure P$CLEAR_ACT_STAGE(pActId in ACT_DISCREPANCY_STAGE.ACT_ID%type) as
begin
  delete from ACT_DISCREPANCY_STAGE where act_id = pActId;
end;

procedure P$CLEAR_DECISION_STAGE(pDecisionId in DECISION_DISCREPANCY_STAGE.DECISION_ID%type) as
begin
  delete from DECISION_DISCREPANCY_STAGE where decision_id = pDecisionId;
end;

procedure P$INSERT_ACT(
  pId OUT ACT.ID%type,
  pInnDeclarant IN ACT.INN_DECLARANT%type,
  pInn IN ACT.INN%type,
  pKpp IN ACT.KPP%type,
  pKppEffective IN ACT.KPP_EFFECTIVE%type,
  pPeriodCode IN ACT.PERIOD_CODE%type,
  pFiscalYear IN ACT.FISCAL_YEAR%type
) as
begin

  select SEQ_KNP_RESULT.NEXTVAL into pId from dual;

  insert into ACT (ID, INN_DECLARANT, INN, KPP, KPP_EFFECTIVE, PERIOD_CODE, FISCAL_YEAR)
    values (pId, pInnDeclarant, pInn, pKpp, pKppEffective, pPeriodCode, pFiscalYear);
end;

procedure P$UPDATE_ACT(
  pId in ACT.ID%type,
  pCloseDate in ACT.CLOSE_DATE%type)
as
begin
  update ACT set close_date = pCloseDate where id = pId;
end;

procedure P$INSERT_DECISION(
  pId out DECISION.ID%type,
  pInnDeclarant in DECISION.INN_DECLARANT%type,
  pInn in DECISION.INN%type,
  pKpp in DECISION.KPP%type,
  pKppEffective in DECISION.KPP_EFFECTIVE%type,
  pFiscalYear in DECISION.FISCAL_YEAR%type,
  pPeriodCode in DECISION.PERIOD_CODE%type
) as
begin
  select SEQ_KNP_RESULT.NEXTVAL into pId from dual;

  insert into DECISION (ID, INN_DECLARANT, INN, KPP, KPP_EFFECTIVE, PERIOD_CODE, FISCAL_YEAR)
    values (pId, pInnDeclarant, pInn, pKpp, pKppEffective, pPeriodCode, pFiscalYear);
end;

procedure P$UPDATE_DECISION(
  pId in DECISION.ID%type,
  pCloseDate in DECISION.CLOSE_DATE%type)
as
begin
  update DECISION set close_date = pCloseDate where id = pId;
  insert into DECISION_CLOSED_BUFFER (DECISION_ID, IS_PROCESSED) values (pId,0);
end;

procedure P$ACT_DELETE_ALL_CLOSED(pDocumentId in ACT_DISCREPANCY.ACT_ID%type) as
begin
  update KNP_DISCREPANCY_DECLARATION	
  set
    act_inn = null,
    act_kpp = null
  where
    DISCREPANCY_ID in (select id from ACT_DISCREPANCY where act_id = pDocumentId and status = 2);   
  
  delete from ACT_DISCREPANCY where act_id = pDocumentId and status = 2;
end;

procedure P$DECISION_DELETE_ALL_CLOSED(pDocumentId in DECISION_DISCREPANCY.DECISION_ID%type) as
begin
  delete from DECISION_DISCREPANCY where decision_id = pDocumentId and status = 2;
end;

procedure P$ACT_DISCREPANCY_START_APPEND(
  pId in ACT_DISCREPANCY.ACT_ID%type) as
  vArrPred T$ARRAY_OF_NUMBER;
  vArr T$ARRAY_OF_NUMBER;
  vInnDeclarant varchar2(12 char);
  vInn varchar2(12 char);
  vKppEffective varchar2(9 char);
  vPeriodCode varchar2(2 char);
  vYear varchar2(4 char);
begin
  select
     INN_DECLARANT
    ,INN
    ,KPP_EFFECTIVE
    ,PERIOD_CODE
    ,FISCAL_YEAR
  into
     vInnDeclarant
    ,vInn
    ,vKppEffective
    ,vPeriodCode
    ,vYear
  from ACT
  where ID = pId;

  select b.discrepancy_id
  bulk collect into vArrPred
  from KNP_DISCREPANCY_DECLARATION b 
  where b.discrepancy_id in
  (
    select dis.id as discrepancy_id
    from KNP_DISCREPANCY_DECLARATION dis_decl
      join SOV_DISCREPANCY dis on dis.id = dis_decl.discrepancy_id and dis.status = 1
    where
          dis_decl.INN_DECLARANT = vInnDeclarant
      and dis_decl.inn           = vInn
      and dis_decl.KPP_EFFECTIVE = vKppEffective
      and dis_decl.PERIOD_CODE   = vPeriodCode
      and dis_decl.FISCAL_YEAR   = vYear
  ) 
  for update of b.discrepancy_id skip locked;

  select distinct arr.column_value
  bulk collect into vArr
  from table(vArrPred) arr
    left join ACT_DISCREPANCY_STAGE stg on stg.ID = arr.column_value
    left join ACT_DISCREPANCY a_dis on a_dis.ID = arr.column_value
  where stg.ID is null and a_dis.ID is null;

  insert into /*APPEND*/ ACT_DISCREPANCY_STAGE (
     ACT_ID
    ,INCLUDED
    ,ID
    ,TYPE_NAME
    ,INITIAL_AMT
    ,STATUS
    ,INVOICE_NUMBER
    ,INVOICE_DATE
    ,INVOICE_CHAPTER
    ,INVOICE_AMT
    ,INVOICE_NDS_AMT
    ,CONTRACTOR_INN
    ,CONTRACTOR_KPP
    ,CONTRACTOR_INN_REORG)
  select
     pId
    ,0
    ,arr.column_value
    ,dis_tp.description
    ,nvl(t.discrepancy_amt,0)
    ,1
    ,t.invoice_number
    ,t.invoice_date
    ,t.invoice_chapter
    ,t.invoice_amt
    ,t.invoice_nds_amt
    ,t.contractor_inn_declarant
    ,t.contractor_kpp
    ,t.contractor_inn_reorganized
  from table(vArr) arr
    join KNP_DISCREPANCY_DECLARATION t
         on    t.discrepancy_id = arr.column_value
           and t.inn_declarant = vInnDeclarant
           and t.inn = vInn
           and t.kpp_effective = vKppEffective
           and t.period_code = vPeriodCode
           and t.fiscal_year = vYear
    join DICT_DISCREPANCY_TYPE dis_tp on dis_tp.s_code = t.discrepancy_type;

end;

procedure P$ACT_DISCREPANCY_APPEND(
  pId in ACT_DISCREPANCY.ACT_ID%type) as
  v_inn varchar2(12);
  v_kpp varchar2(9);
begin

  select a.inn_declarant, d.kpp
  into v_inn, v_kpp
  from ACT a
  join DECLARATION_HISTORY d
    on    d.inn_declarant = a.inn_declarant
      and d.inn_contractor = a.inn
      and d.kpp_effective = a.kpp_effective
      and d.fiscal_year = a.fiscal_year
      and d.period_code = a.period_code
      and d.type_code = 0
  where a.id = pId and rownum = 1;

  update KNP_DISCREPANCY_DECLARATION
  set
    act_inn = v_inn,
    act_kpp = v_kpp
  where
        act_inn is null
    and DISCREPANCY_ID in (select id from ACT_DISCREPANCY_STAGE where act_id = pId and included = 1);
  
  insert into /*APPEND*/ ACT_DISCREPANCY (
     ACT_ID
    ,ID
    ,TYPE_NAME
    ,INITIAL_AMT
    ,STATUS
    ,INVOICE_NUMBER
    ,INVOICE_DATE
    ,INVOICE_CHAPTER
    ,INVOICE_AMT
    ,INVOICE_NDS_AMT
    ,CONTRACTOR_INN
    ,CONTRACTOR_KPP
    ,CONTRACTOR_INN_REORG
    ,AMOUNT)
  select
     pId
    ,st.id
    ,st.TYPE_NAME
    ,st.INITIAL_AMT
    ,knp.status
    ,st.INVOICE_NUMBER
    ,st.INVOICE_DATE
    ,st.INVOICE_CHAPTER
    ,st.INVOICE_AMT
    ,st.INVOICE_NDS_AMT
    ,st.CONTRACTOR_INN
    ,st.CONTRACTOR_KPP
    ,st.CONTRACTOR_INN_REORG
    ,st.INITIAL_AMT
  from ACT_DISCREPANCY_STAGE st
  join SOV_DISCREPANCY knp on knp.id = st.id
  where st.act_id = pId and st.included = 1;

  P$CLEAR_ACT_STAGE(pId);
end;

procedure P$ACT_CLOSE(pId in ACT_DISCREPANCY.ACT_ID%type) as
begin
	update
	(select
		dda.knp_doc_type as knp_doc_type_old,
		dda.knp_doc_amount as knp_doc_amount_old,
		1 as knp_doc_type_new,
		ad.amount as knp_doc_amount_new
		from ACT a
		inner join ACT_DISCREPANCY ad on ad.act_id = a.id
		inner join KNP_DISCREPANCY_DECLARATION dda on dda.DISCREPANCY_ID = ad.id
		where a.id = pId) t
	set
		t.knp_doc_type_old = t.knp_doc_type_new,
		t.knp_doc_amount_old = t.knp_doc_amount_new;
end;

procedure P$ACT_DIS_ROLLBACK_APPEND(
  pId in ACT_DISCREPANCY.ACT_ID%type) as
begin
  P$CLEAR_ACT_STAGE(pId);
end;

procedure P$DECISION_START_APPEND(pId in DECISION.ID%type) as
begin
  insert into /*APPEND*/ DECISION_DISCREPANCY_STAGE(
     DECISION_ID
    ,INCLUDED
    ,ID
    ,TYPE_NAME
    ,INITIAL_AMT
    ,STATUS
    ,INVOICE_NUMBER
    ,INVOICE_DATE
    ,INVOICE_CHAPTER
    ,INVOICE_AMT
    ,INVOICE_NDS_AMT
    ,CONTRACTOR_INN
    ,CONTRACTOR_KPP
    ,CONTRACTOR_INN_REORG
    ,ACT_AMOUNT)
  select
     pId
    ,0
    ,act_dis.id
    ,act_dis.type_name
    ,act_dis.initial_amt
    ,act_dis.status
    ,act_dis.invoice_number
    ,act_dis.invoice_date
    ,act_dis.invoice_chapter
    ,act_dis.invoice_amt
    ,act_dis.invoice_nds_amt
    ,act_dis.contractor_inn
    ,act_dis.contractor_kpp
    ,act_dis.contractor_inn_reorg
    ,act_dis.amount
  from ACT act
  join DECISION des
    on    des.id = pId
      and des.inn_declarant = act.inn_declarant
      and des.inn = act.inn
      and des.kpp_effective = act.kpp_effective
      and des.fiscal_year = act.fiscal_year
      and des.period_code = act.period_code
  join ACT_DISCREPANCY act_dis on act_dis.act_id = act.id and act_dis.status = 1
  left join DECISION_DISCREPANCY des_dis
    on    des_dis.decision_id = des.id
      and des_dis.id = act_dis.id
  where des_dis.id is null;
end;

procedure P$DECISION_COMMIT_APPEND(pId in DECISION.ID%type) as
begin

  insert into /*APPEND*/ DECISION_DISCREPANCY(
     DECISION_ID
    ,ID
    ,TYPE_NAME
    ,INITIAL_AMT
    ,STATUS
    ,INVOICE_NUMBER
    ,INVOICE_DATE
    ,INVOICE_CHAPTER
    ,INVOICE_AMT
    ,INVOICE_NDS_AMT
    ,CONTRACTOR_INN
    ,CONTRACTOR_KPP
    ,CONTRACTOR_INN_REORG
    ,AMOUNT
    ,ACT_AMOUNT)
  select
     pId
    ,t.id
    ,t.type_name
    ,t.initial_amt
    ,knp.status
    ,t.invoice_number
    ,t.invoice_date
    ,t.invoice_chapter
    ,t.invoice_amt
    ,t.invoice_nds_amt
    ,t.contractor_inn
    ,t.contractor_kpp
    ,t.contractor_inn_reorg
    ,t.act_amount
    ,t.act_amount
  from DECISION_DISCREPANCY_STAGE t
  join SOV_DISCREPANCY knp on knp.id = t.id
  where t.decision_id = pId and t.included = 1;

  P$CLEAR_DECISION_STAGE(pId);
end;

procedure P$DECISION_CLOSE(pId in DECISION.ID%type) as
begin
	update
	(select
		dda.knp_doc_type as knp_doc_type_old,
		dda.knp_doc_amount as knp_doc_amount_old,
		dda.decision_amount_difference as decision_amount_difference_old,
		2 as knp_doc_type_new,
		dd.amount as knp_doc_amount_new,
		dd.initial_amt - dd.amount as decision_amount_difference_new
	  from DECISION d
		inner join DECISION_DISCREPANCY dd on dd.decision_id = d.id
		inner join KNP_DISCREPANCY_DECLARATION dda on dda.DISCREPANCY_ID = dd.id
	  where d.id = pId) t
	set
	  t.knp_doc_type_old = t.knp_doc_type_new,
	  t.knp_doc_amount_old = t.knp_doc_amount_new,
	  t.decision_amount_difference_old = t.decision_amount_difference_new;
end;

procedure P$DECISION_ROLLBACK_APPEND(pId in DECISION.ID%type) as
begin
  P$CLEAR_DECISION_STAGE(pId);
end;

procedure P$UPDATE_ACT_DISCREPANCY(
  pDiscrepancyId IN ACT_DISCREPANCY.ID%type,
  pAmount IN ACT_DISCREPANCY.AMOUNT%type
) as
begin
  update ACT_DISCREPANCY set amount = pAmount where id = pDiscrepancyId;
end;

procedure P$UPDATE_DECISION_DISCREPANCY(
  pDiscrepancyId in DECISION_DISCREPANCY.ID%type,
  pAmount in DECISION_DISCREPANCY.AMOUNT%type)
as
begin
  update DECISION_DISCREPANCY set amount = pAmount where id = pDiscrepancyId;
end;

procedure P$GET_SESSION(
  pDocumentId IN ACT_DECISION_APPEND_SESSION.DOCUMENT_ID%type,
  pSession OUT SYS_REFCURSOR
) as
begin
  open pSession for
       select
          DOCUMENT_ID as DocumentId
         ,USER_ID as UserSid
         ,EXPIRED_AT as ExpiredAt
       from ACT_DECISION_APPEND_SESSION
       where DOCUMENT_ID = pDocumentId;
end;

procedure P$INSERT_SESSION(
  pDocumentId IN ACT_DECISION_APPEND_SESSION.DOCUMENT_ID%type,
  pUserSid IN ACT_DECISION_APPEND_SESSION.USER_ID%type,
  pUserName IN ACT_DECISION_APPEND_SESSION.USER_NAME%type,
  pExpiredAt in ACT_DECISION_APPEND_SESSION.EXPIRED_AT%type
) as
begin
  insert into ACT_DECISION_APPEND_SESSION (USER_ID,USER_NAME,DOCUMENT_ID,EXPIRED_AT)
    values (pUserSid, pUserName, pDocumentId, pExpiredAt);
end;

procedure P$UPDATE_SESSION(
  pDocumentId in ACT_DECISION_APPEND_SESSION.DOCUMENT_ID%type,
  pExpiredAt in ACT_DECISION_APPEND_SESSION.EXPIRED_AT%type)
  as
begin
  update ACT_DECISION_APPEND_SESSION
  set EXPIRED_AT = pExpiredAt
  where DOCUMENT_ID = pDocumentId;
end;

procedure P$DELETE_SESSION(pDocumentId in ACT_DECISION_APPEND_SESSION.DOCUMENT_ID%type) as
begin
  delete from ACT_DECISION_APPEND_SESSION where DOCUMENT_ID = pDocumentId;
end;

procedure P$GET_SESSION_DURATION(pDuration out number) as
begin
  select to_number(t.value)
  into pDuration
  from CONFIGURATION t
  where t.parameter = 'act_decision_session_duration';
end;

procedure P$KILL_SESSION(pDocumentId in number) as
  v_is_decision number;
  pragma autonomous_transaction;
begin

  begin
    select 1 into v_is_decision from DECISION where id = pDocumentId;
    exception when NO_DATA_FOUND then v_is_decision := 0;
  end;

  if v_is_decision = 1 then
    P$CLEAR_DECISION_STAGE(pDocumentId);
  else
    P$CLEAR_ACT_STAGE(pDocumentId);
  end if;
  P$DELETE_SESSION(pDocumentId);
  commit;
  exception when others then rollback;
end;

procedure P$KILL_EXPIRED_SESSIONS as
begin
  for session in (select document_id from ACT_DECISION_APPEND_SESSION where expired_at < sysdate) loop
    P$KILL_SESSION(session.document_id);
  end loop;
end;

procedure P$CLOSE_DISCREPANCY as
  pragma autonomous_transaction;
begin
  update DECISION_CLOSED_BUFFER set is_processed = 1;
  update SOV_DISCREPANCY set status = 2 where id in (select dis.id from DECISION_CLOSED_BUFFER buf join DECISION_DISCREPANCY dis on dis.decision_id = buf.decision_id where buf.is_processed = 1);
  update HIVE_DISCREPANCY_DECL_CACHE set DISCREPANCY_STATUS = 2 where DISCREPANCY_ID in (select dis.id from DECISION_CLOSED_BUFFER buf join DECISION_DISCREPANCY dis on dis.decision_id = buf.decision_id where buf.is_processed = 1);
  delete from DECISION_CLOSED_BUFFER where is_processed = 1;
  commit;

  exception when others then rollback;
end;

procedure P$UPDATE_DISCREPANCY_STATUS as
  pragma autonomous_transaction;
begin
	update
		(select
			dst.status as status_old,
			src.status as status_new
		from
			SOV_DISCREPANCY src
			join ACT_DISCREPANCY dst on src.id = dst.id
		where src.status <> dst.status) t
	set
	  t.status_old = t.status_new;

	update
		(select
			dst.status as status_old,
			src.status as status_new
		from
			SOV_DISCREPANCY src
			join DECISION_DISCREPANCY dst on src.id = dst.id
		where src.status <> dst.status) t
	set
	  t.status_old = t.status_new;

   for discrepancy in (select sov.id, sov.amnt as amount from SOV_DISCREPANCY sov join ACT_DISCREPANCY act on sov.id = act.id where sov.amnt <> act.amount)
   loop
     update ACT_DISCREPANCY
     set
        initial_amt = discrepancy.amount
     where id = discrepancy.id;

     update DECISION_DISCREPANCY
     set
        initial_amt = discrepancy.amount
     where id = discrepancy.id;

   end loop;

   commit;

   exception when others then rollback;
end;

procedure P$GET_DECISION_UPDATE_STATE(pEnabled out number) as
begin
  select to_number(t.value)
  into pEnabled
  from CONFIGURATION t
  where t.parameter = 'decision_close_enabled';
end;

procedure P$SET_DECISION_UPDATE_STATE (pEnabled in varchar2) as
begin
  update CONFIGURATION
  set value = pEnabled
  where parameter = 'decision_close_enabled';
  commit;
end;

procedure P$ENABLE_DECISION_UPDATE as
begin
  P$SET_DECISION_UPDATE_STATE('1');
end;

procedure P$DISABLE_DECISION_UPDATE as
begin
  P$SET_DECISION_UPDATE_STATE('0');
  execute immediate 'truncate table ACT_DECISION_APPEND_SESSION';
  execute immediate 'truncate table ACT_DISCREPANCY_STAGE';
end;

procedure P$ACT_DIS_INVALID_COUNT(
  pId IN ACT_DISCREPANCY.ID%type,
  pCount out number
) as
begin
  select count(1)
  into pCount
  from ACT_DISCREPANCY t
  where t.act_id = pId
        and t.status = 1
        and ((t.initial_amt < t.amount)
             or
             (t.amount <= 0));
end;

procedure P$DECISION_DIS_INVALID_COUNT(
  pId IN DECISION_DISCREPANCY.ID%type,
  pCount out number
) as
begin
  select count(1)
  into pCount
  from DECISION_DISCREPANCY t
  where t.decision_id = pId
        and t.status = 1
        and ((t.act_amount < t.amount)
             or
             (t.amount <= 0));
end;

procedure P$ACT_DIS_REMOVE(
  pDocumentId IN ACT_DISCREPANCY.ACT_ID%type,
  pDiscrepancyId IN ACT_DISCREPANCY.ID%type
) as
begin
  delete
  from ACT_DISCREPANCY t
  where t.act_id = pDocumentId
        and t.id = pDiscrepancyId;

  update KNP_DISCREPANCY_DECLARATION
  set
    act_inn = null,
    act_kpp = null
  where DISCREPANCY_ID = pDiscrepancyId;

  commit;
end;

procedure P$DECISION_DIS_REMOVE(
  pDocumentId IN DECISION_DISCREPANCY.DECISION_ID%type,
  pDiscrepancyId IN DECISION_DISCREPANCY.ID%type
) as
begin
  delete
  from DECISION_DISCREPANCY t
  where t.decision_id = pDocumentId
        and t.id = pDiscrepancyId;

  commit;
end;

procedure P$AGGREGATE_LOG(
  pType IN number,
  pMessage IN varchar2
) as
pragma autonomous_transaction;
begin
  insert into KNP_RESULT_REPORT_LOG
  values
  (SEQ_KNP_REPORT_LOG.NEXTVAL, sysdate, pType, pMessage);
  commit;
end;

procedure P$AGGREGATE_REPORT_DATA(pReportDate in Date)
is
  c_discrepancy_gap_type_code	constant number := 1;
  c_discrepancy_nds_type_code	constant number := 4;
  v_discrepancy_gap_type_name	varchar2(512);
  v_discrepancy_nds_type_name	varchar2(512);
  v_procedure_strart_timestamp	timestamp;
  v_request_strart_timestamp	timestamp;
  pragma autonomous_transaction;
begin
  P$AGGREGATE_LOG(1, 'F$CREATE_AGREGATE - started ' || pReportDate);

  SELECT systimestamp INTO v_procedure_strart_timestamp FROM dual;

  select description into v_discrepancy_gap_type_name
  from DICT_DISCREPANCY_TYPE
  where s_code = c_discrepancy_gap_type_code;

  select description into v_discrepancy_nds_type_name
  from DICT_DISCREPANCY_TYPE
  where s_code = c_discrepancy_nds_type_code;

  delete from knp_result_report_aggregate
  where report_id in (
	select id from knp_result_report where report_date = trunc(pReportDate, 'DD'));

  delete from knp_result_report where report_date = trunc(pReportDate, 'DD');

  delete from T$KNP_RESULT_REPORT_ACT;
  delete from T$KNP_RESULT_REPORT_DECISION;
  delete from T$KNP_RESULT_REPORT_CURR;
  delete from T$KNP_RESULT_REPORT_PREV;

  SELECT systimestamp INTO v_request_strart_timestamp FROM dual;

  P$AGGREGATE_LOG(1, 'F$CREATE_AGREGATE - T$KNP_RESULT_REPORT_ACT - started');

  insert into T$KNP_RESULT_REPORT_ACT
  select
		substr(dh.sono_code, 1, 2) as region_code,
		dh.sono_code as sono_code,
		a.fiscal_year as fiscal_year,
		dh.period_effective as quarter,
		count(distinct a.id) as act_qty,
		sum(case when ad.type_name = v_discrepancy_nds_type_name then ad.amount else 0 end ) as act_nds_amt,
		sum(case when ad.type_name = v_discrepancy_nds_type_name then 1 else 0 end ) as act_nds_qty,
		sum(case when ad.type_name = v_discrepancy_gap_type_name then ad.amount else 0 end ) as act_gap_amt,
		sum(case when ad.type_name = v_discrepancy_gap_type_name then 1 else 0 end ) as act_gap_qty
  from ACT a
		inner join ACT_DISCREPANCY ad on ad.act_id = a.id
		inner join DECLARATION_HISTORY dh on
			  dh.inn_declarant = a.inn_declarant
			  and dh.inn_contractor = a.inn
			  and dh.kpp_effective = a.kpp_effective
			  and dh.fiscal_year = a.fiscal_year
			  and dh.period_code = a.period_code
			  and dh.type_code = 0
			  and dh.is_active = 1
  where
		trunc(a.close_date) = trunc(pReportDate, 'DD')
  group by
		dh.sono_code,
		a.fiscal_year,
		dh.period_effective;

  P$AGGREGATE_LOG(1, 'F$CREATE_AGREGATE - T$KNP_RESULT_REPORT_ACT - completed '
						|| SQL%ROWCOUNT
						|| ' '
						|| TO_CHAR(systimestamp - v_request_strart_timestamp));

  SELECT systimestamp INTO v_request_strart_timestamp FROM dual;

  P$AGGREGATE_LOG(1, 'F$CREATE_AGREGATE - T$KNP_RESULT_REPORT_DECISION - started');

  insert into T$KNP_RESULT_REPORT_DECISION
  select
		substr(dh.sono_code, 1, 2) as region_code,
		dh.sono_code as sono_code,
		d.fiscal_year as fiscal_year,
		dh.period_effective as quarter,
		count(distinct d.id) as decision_qty,
		sum(case when dd.type_name = v_discrepancy_nds_type_name then dd.amount else 0 end ) as decision_nds_amt,
		sum(case when dd.type_name = v_discrepancy_nds_type_name then 1 else 0 end ) as decision_nds_qty,
		sum(case when dd.type_name = v_discrepancy_gap_type_name then dd.amount else 0 end ) as decision_gap_amt,
		sum(case when dd.type_name = v_discrepancy_gap_type_name then 1 else 0 end ) as decision_gap_qty
  from DECISION d
		inner join DECISION_DISCREPANCY dd on dd.decision_id = d.id
		inner join DECLARATION_HISTORY dh on
			  dh.inn_declarant = d.inn_declarant
			  and dh.inn_contractor = d.inn
			  and dh.kpp_effective = d.kpp_effective
			  and dh.fiscal_year = d.fiscal_year
			  and dh.period_code = d.period_code
			  and dh.type_code = 0
			  and dh.is_active = 1
  where
		trunc(d.close_date) = trunc(pReportDate, 'DD')
  group by
		dh.sono_code,
		d.fiscal_year,
		dh.period_effective;

  P$AGGREGATE_LOG(1, 'F$CREATE_AGREGATE - T$KNP_RESULT_REPORT_DECISION - completed '
						|| SQL%ROWCOUNT
						|| ' '
						|| TO_CHAR(systimestamp - v_request_strart_timestamp));

  insert into T$KNP_RESULT_REPORT_CURR
  select
	nvl(a.region_code, d.region_code) as region_code,
	nvl(a.sono_code, d.sono_code) as sono_code,
	nvl(a.fiscal_year, d.fiscal_year) as fiscal_year,
	nvl(a.quarter, d.quarter) as quarter,
	nvl(a.act_qty, 0) as act_qty,
	nvl(a.act_nds_amt, 0) as act_nds_amt,
	nvl(a.act_nds_qty, 0) as act_nds_qty,
	nvl(a.act_gap_amt, 0) as act_gap_amt,
	nvl(a.act_gap_qty, 0) as act_gap_qty,
	nvl(d.decision_qty, 0) as decision_qty,
	nvl(d.decision_nds_amt, 0) as decision_nds_amt,
	nvl(d.decision_nds_qty, 0) as decision_nds_qty,
	nvl(d.decision_gap_amt, 0) as decision_gap_amt,
	nvl(d.decision_gap_qty, 0) as decision_gap_qty
  from T$KNP_RESULT_REPORT_ACT a
	full outer join T$KNP_RESULT_REPORT_DECISION d on
		a.region_code = d.region_code
		and a.sono_code = d.sono_code
		and a.fiscal_year = d.fiscal_year
		and a.quarter = d.quarter;

  insert into T$KNP_RESULT_REPORT_PREV
  select
		krra.region_code as region_code,
		krra.sono_code as sono_code,
		krr.fiscal_year as fiscal_year,
		krr.quarter as quarter,
		krra.acts_qty as act_qty,
		krra.act_discrepancy_nds_amt as act_nds_amt,
		krra.act_discrepancy_nds_qty as act_nds_qty,
		krra.act_discrepancy_gap_amt as act_gap_amt,
		krra.act_discrepancy_gap_qty as act_gap_qty,
		krra.decisions_qty as decision_qty,
		krra.decision_discrepancy_nds_amt as decision_nds_amt,
		krra.decision_discrepancy_nds_qty as decision_nds_qty,
		krra.decision_discrepancy_gap_amt as decision_gap_amt,
		krra.decision_discrepancy_gap_qty as decision_gap_qty
  from KNP_RESULT_REPORT krr
		inner join KNP_RESULT_REPORT_AGGREGATE krra on krra.report_id = krr.actual_report_id
  where
		krr.report_date = trunc(pReportDate - 1, 'DD');

  insert into KNP_RESULT_REPORT
  select
	  SEQ_KNP_REPORT.NEXTVAL as report_id,
	  trunc(pReportDate, 'DD') as report_date,
	  grp.fiscal_year,
	  grp.quarter,
	  SEQ_KNP_REPORT.CURRVAL as actual_report_id
  from   (
	  select fiscal_year, quarter
	  from T$KNP_RESULT_REPORT_CURR
	  group by fiscal_year, quarter
  ) grp;

  insert into KNP_RESULT_REPORT
  select
	  SEQ_KNP_REPORT.NEXTVAL as report_id,
	  trunc(pReportDate, 'DD') as report_date,
	  krr_prev.fiscal_year,
	  krr_prev.quarter,
	  krr_prev.actual_report_id as actual_report_id
  from KNP_RESULT_REPORT krr_prev
	  left join KNP_RESULT_REPORT krr_curent on
		krr_curent.report_date = trunc(pReportDate, 'DD')
		and krr_curent.fiscal_year = krr_prev.fiscal_year
		and krr_curent.quarter = krr_prev.quarter
  where
	  krr_prev.report_date = trunc(pReportDate - 1, 'DD')
	  and krr_curent.id is null;

  insert into KNP_RESULT_REPORT_AGGREGATE
  select
    krr.id as report_id,
	curr.region_code as region_code,
	curr.sono_code as sono_code,
	curr.act_qty + nvl(prev.act_qty, 0) as act_qty,
	curr.act_nds_amt + nvl(prev.act_nds_amt, 0) as act_nds_amt,
	curr.act_nds_qty + nvl(prev.act_nds_qty, 0) as act_nds_qty,
	curr.act_gap_amt + nvl(prev.act_gap_amt, 0) as act_gap_amt,
	curr.act_gap_qty + nvl(prev.act_gap_qty, 0) as act_gap_qty,
	curr.decision_qty + nvl(prev.decision_qty, 0) as decision_qty,
	curr.decision_nds_amt + nvl(prev.decision_nds_amt, 0) as decision_nds_amt,
	curr.decision_nds_qty + nvl(prev.decision_nds_qty, 0) as decision_nds_qty,
	curr.decision_gap_amt + nvl(prev.decision_gap_amt, 0) as decision_gap_amt,
	curr.decision_gap_qty + nvl(prev.decision_gap_qty, 0) as decision_gap_qty
  from T$KNP_RESULT_REPORT_CURR curr
	left join T$KNP_RESULT_REPORT_PREV prev on
		prev.region_code = curr.region_code
		and prev.sono_code = curr.sono_code
		and prev.fiscal_year = curr.fiscal_year
		and prev.quarter = curr.quarter
		and prev.sono_code is not null
	inner join KNP_RESULT_REPORT krr on
		krr.fiscal_year = curr.fiscal_year
	    and krr.quarter = curr.quarter
		and krr.report_date = trunc(pReportDate, 'DD');

  insert into KNP_RESULT_REPORT_AGGREGATE
  select
    krr.id as report_id,
	curr.region_code as region_code,
	null as sono_code,
	curr.act_qty + nvl(prev.act_qty, 0) as act_qty,
	curr.act_nds_amt + nvl(prev.act_nds_amt, 0) as act_nds_amt,
	curr.act_nds_qty + nvl(prev.act_nds_qty, 0) as act_nds_qty,
	curr.act_gap_amt + nvl(prev.act_gap_amt, 0) as act_gap_amt,
	curr.act_gap_qty + nvl(prev.act_gap_qty, 0) as act_gap_qty,
	curr.decision_qty + nvl(prev.decision_qty, 0) as decision_qty,
	curr.decision_nds_amt + nvl(prev.decision_nds_amt, 0) as decision_nds_amt,
	curr.decision_nds_qty + nvl(prev.decision_nds_qty, 0) as decision_nds_qty,
	curr.decision_gap_amt + nvl(prev.decision_gap_amt, 0) as decision_gap_amt,
	curr.decision_gap_qty + nvl(prev.decision_gap_qty, 0) as decision_gap_qty
  from (
	  select
		region_code,
		fiscal_year,
		quarter,
		sum(act_qty) as act_qty,
		sum(act_nds_amt) as act_nds_amt,
		sum(act_nds_qty) as act_nds_qty,
		sum(act_gap_amt) as act_gap_amt,
		sum(act_gap_qty) as act_gap_qty,
		sum(decision_qty) as decision_qty,
		sum(decision_nds_amt) as decision_nds_amt,
		sum(decision_nds_qty) as decision_nds_qty,
		sum(decision_gap_amt) as decision_gap_amt,
		sum(decision_gap_qty) as decision_gap_qty
	  from T$KNP_RESULT_REPORT_CURR
	  group by
		region_code,
		fiscal_year,
		quarter
	) curr
	left join T$KNP_RESULT_REPORT_PREV prev on
		prev.region_code = curr.region_code
		and prev.fiscal_year = curr.fiscal_year
		and prev.quarter = curr.quarter
		and prev.sono_code is null
	inner join KNP_RESULT_REPORT krr on
		krr.fiscal_year = curr.fiscal_year
	    and krr.quarter = curr.quarter
		and krr.report_date = trunc(pReportDate, 'DD');

	merge into
		knp_result_report_period dst
	using
		(select fiscal_year, quarter
		  from T$KNP_RESULT_REPORT_CURR
		  group by fiscal_year, quarter) src
	on
		(src.fiscal_year = dst.fiscal_year
		  and src.quarter = dst.quarter)
	when not matched then insert (fiscal_year, quarter) values (src.fiscal_year, src.quarter);

  commit;

  P$AGGREGATE_LOG(1, 'F$CREATE_AGREGATE - completed '
						|| pReportDate
						|| ' '
						|| TO_CHAR(systimestamp - v_procedure_strart_timestamp));

exception when others then
  rollback;
  P$AGGREGATE_LOG(-1, 'F$CREATE_AGREGATE - failed ' || substr(sqlerrm, 1, 512));
end;

procedure P$GET_REPORT_ID (
  pId out KNP_RESULT_REPORT.ID%type,
  pFiscalYear in KNP_RESULT_REPORT.FISCAL_YEAR%type,
  pQuarter in KNP_RESULT_REPORT.QUARTER%type,
  pReportDate in KNP_RESULT_REPORT.REPORT_DATE%type)
as
begin
  select actual_report_id into pId
  from KNP_RESULT_REPORT
  where fiscal_year = pFiscalYear
    and quarter = pQuarter
    and report_date = trunc(pReportDate);

  exception when NO_DATA_FOUND then pId := -1;
end;

procedure P$GET_REPORT_PERIODS(
  pResult out sys_refcursor
  ) 
as
begin
  open pResult for
  select fiscal_year as Year, quarter as Quarter
  from knp_result_report_period
  order by fiscal_year desc, quarter desc;
end;

procedure P$AGGREGATE_REPORT_DATA_RECURS(pReportDate in Date)
is
  v_is_prev_report_exists	number;
begin
  
  select count(*) into v_is_prev_report_exists 
  from KNP_RESULT_REPORT t
  where t.report_date = trunc(pReportDate, 'DD') - 1;

  if v_is_prev_report_exists = 0 then
    P$AGGREGATE_REPORT_DATA_RECURS(pReportDate - 1);
  end if;
  
  P$AGGREGATE_REPORT_DATA(pReportDate); 

end;

procedure P$PREPARE_REPORT_DATA(pReportDate in Date)
is
  v_is_prev_reports_exists	number;
begin
   
  select count(*) into v_is_prev_reports_exists 
  from KNP_RESULT_REPORT t
  where t.report_date < trunc(pReportDate, 'DD');
   
  if v_is_prev_reports_exists = 0 then
    P$AGGREGATE_REPORT_DATA(pReportDate);
  else
    P$AGGREGATE_REPORT_DATA_RECURS(pReportDate);
  end if;

end;

procedure P$AGGREGATE_DAILY as
begin
    P$PREPARE_REPORT_DATA(sysdate - 1);
end;

procedure P$GET_ACTS_BY_DECLARATION( 
  pInnDeclarant in varchar2,
  pInnContractor in varchar2,
  pKppEffective in varchar2,
  pPeriodEffective in number,
  pYear in varchar2,
  pCursor out SYS_REFCURSOR
)
as
begin
OPEN pCursor FOR
  select 
  SEOD_KNP.KNP_ID as "Id",
  SEOD_KNP.DECLARATION_REG_NUM as DeclarationRegistrationNumber,
  SEOD_KNP.IFNS_CODE as SonoCode,
  SEOD_KNP.CREATION_DATE as CreatedAt,
  SEOD_KNP_ACT.DOC_NUM as "Number",
  SEOD_KNP_ACT.DOC_DATA as "Date",
  SEOD_KNP.IFNS_CODE as SonoCode
  from NDS2_SEOD.SEOD_KNP
  inner join NDS2_SEOD.SEOD_KNP_ACT on SEOD_KNP_ACT.KNP_ID = SEOD_KNP.KNP_ID
  inner join declaration_history DECL on DECL.Reg_Number = SEOD_KNP.DECLARATION_REG_NUM and decl.SONO_CODE_SUBMITED = seod_knp.ifns_code
     where DECL.INN_DECLARANT = pInnDeclarant 
	  and ((pInnContractor is not null and DECL.INN_CONTRACTOR = pInnContractor) or pInnContractor is null)
	  and DECL.KPP_EFFECTIVE = pKppEffective and DECL.TYPE_CODE = 0
      and DECL.PERIOD_EFFECTIVE = pPeriodEffective and DECL.FISCAL_YEAR = pYear;
end;

procedure P$GET_DECISIONS_BY_DECLARATION( 
  pInnDeclarant in varchar2,
  pInnContractor in varchar2,
  pKppEffective in varchar2,
  pPeriodEffective in number,
  pYear in varchar2,
  pCursor out SYS_REFCURSOR
)
as
begin
OPEN pCursor FOR
  select
  SEOD_KNP.KNP_ID as "Id",
  SEOD_KNP.DECLARATION_REG_NUM as DeclarationRegistrationNumber,
  SEOD_KNP.IFNS_CODE as SonoCode,
  SEOD_KNP.CREATION_DATE as CreatedAt,
  SEOD_KNP_DECISION_TYPE.DESCRIPTION as Name,
  SEOD_KNP_DECISION.DOC_NUM as "Number",
  SEOD_KNP_DECISION.DOC_DATE as "Date"
  from NDS2_SEOD.SEOD_KNP
  inner join NDS2_SEOD.SEOD_KNP_DECISION on SEOD_KNP_DECISION.KNP_ID = SEOD_KNP.KNP_ID
  inner join NDS2_SEOD.SEOD_KNP_DECISION_TYPE on SEOD_KNP_DECISION_TYPE.ID = SEOD_KNP_DECISION.TYPE_ID
  inner join declaration_history DECL on DECL.Reg_Number = SEOD_KNP.DECLARATION_REG_NUM and decl.SONO_CODE_SUBMITED = seod_knp.ifns_code
  where DECL.INN_DECLARANT = pInnDeclarant 
    and ((pInnContractor is not null and DECL.INN_CONTRACTOR = pInnContractor) or pInnContractor is null)
    and DECL.KPP_EFFECTIVE = pKppEffective and DECL.TYPE_CODE = 0
    and DECL.PERIOD_EFFECTIVE = pPeriodEffective and DECL.FISCAL_YEAR = pYear;
end;

end;
/
