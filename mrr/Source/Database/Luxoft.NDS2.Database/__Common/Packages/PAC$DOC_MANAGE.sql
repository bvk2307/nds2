﻿create or replace package NDS2_MRR_USER.PAC$DOC_MANAGE as
  
  procedure P$INCLUDE (pDocId in number, pUser in varchar2);
  
  procedure P$EXCLUDE (pDocId in number, pUser in varchar2);
  
  procedure P$SEND (pDate in date, pUser in varchar2);
  
  procedure P$GET_BASE_DATE (pDate out date);
  
end;
/

create or replace package body NDS2_MRR_USER.PAC$DOC_MANAGE as

  procedure P$SET_STATE (pDocId in number, pUser in varchar2, pExcluded number) as
  begin
    merge into doc_exclude_data trg
    using (select pDocId as DocId, pExcluded as Excluded, sysdate as ChangeTime, pUser as LastUser from dual) src
      on (src.DocId = trg.DocId)
    when matched then update set
      trg.Excluded = src.Excluded,
      trg.ChangeTime = src.ChangeTime,
      trg.LastUser = src.LastUser
    when not matched then insert
      (DocId, Excluded, ChangeTime, LastUser) values
      (src.DocId, src.Excluded, src.ChangeTime, src.LastUser);
      
    commit;
  end;

  procedure P$INCLUDE (pDocId in number, pUser in varchar2) as
  begin
    P$SET_STATE(pDocId, pUser, 0);
  end;
  
  procedure P$EXCLUDE (pDocId in number, pUser in varchar2) as
  begin
    P$SET_STATE(pDocId, pUser, 1);
  end;
  
  procedure P$SEND (pDate in date, pUser in varchar2) as

	  type curDocId is ref cursor;
	  type colDocId is table of number;
	  
	  docIdCursor curDocId;
	  docIdList colDocId;

	  docSendLimit constant number:=5000;
	  docSendCounter number:=0;
  begin
    NDS2$SYS.LOG_INFO(
      'PAC$DOC_MANAGE.P$SEND',
      null, null,
      'Отправка АТ по СФ за '||TO_CHAR(pDate, 'dd.mm.yyyy')
          ||' пользователем '||pUser
      );

	  open docIdCursor for 
	  select distinct t.doc_id
	  from doc t 
	  where 
		trunc(t.create_date) = trunc(pDate)
		and t.doc_type = 1 
		and t.status = -1
		and 1 != (	select 
						coalesce(max(ex.excluded), t.tax_monitoring, 0) 
					from doc_exclude_data ex
					where ex.docid = t.doc_id
		);

		LOOP 
			fetch docIdCursor
			bulk collect into docIdList
			limit docSendLimit;

			forall i in indices of docIdList
				update doc t 
				set t.status = 1, 
					t.status_date = sysdate
				where t.doc_id = docIdList(i);

			forall i in indices of docIdList
				insert into doc_status_history(doc_id,status,status_date)
				values (docIdList(i), 1, sysdate);
			
			docSendCounter := docSendCounter + docIdList.COUNT;
			EXIT WHEN docIdList.COUNT < docSendLimit;
		END LOOP;

		close docIdCursor;
		
		insert into doc_send_data (doctype, senddate, sendtime, username)
		values (1, trunc(pDate), sysdate, pUser);

		commit;

		NDS2$SYS.LOG_INFO(
		  'PAC$DOC_MANAGE.P$SEND',
		  null, null,
		  'Отправлено '|| docSendCounter 
			||' АТ по СФ за ' ||TO_CHAR(pDate, 'dd.mm.yyyy')
			||' пользователем '||pUser
		  );
    
    exception when others then begin
      rollback;

	  if docIdCursor%ISOPEN then
		close docIdCursor;
	  end if;

      NDS2$SYS.LOG_INFO(
        'PAC$DOC_MANAGE.P$SEND',
        null, sqlcode,
        'Ошибка отправка АТ по СФ за '||TO_CHAR(pDate, 'dd.mm.yyyy')
            ||': '||substr(sqlerrm, 1, 256)
      );
      raise;
    end;
  end;

  procedure P$GET_BASE_DATE (pDate out date) as
  begin
    select coalesce(to_date(cfg.value, 'DD-MM-YYYY'), to_date('20-10-2017', 'DD-MM-YYYY')) into pDate from dual
    left join CONFIGURATION cfg on cfg.parameter = 'NewArchitectureATDate';
  end;

end;
/
