﻿CREATE OR REPLACE PACKAGE NDS2_MRR_USER.PAC$KNP_DISCREPANCY IS

PROCEDURE P$SUBMIT_FOR_CLOSING(pReason in VARCHAR2);

PROCEDURE P$CLOSE_SUBMITTED(pDateFrom date);

PROCEDURE P$CLOSE_DISCREPANCY_LIST;

PROCEDURE P$UPDATE_SOV_DISCREP_AGGREGATE;

END PAC$KNP_DISCREPANCY;
/
CREATE OR REPLACE PACKAGE BODY NDS2_MRR_USER.PAC$KNP_DISCREPANCY AS

DAILY_ACTION_KEY_NAME	CONSTANT VARCHAR2(64) := 'pac$knp_discrepancy.p$close_submitted';

PROCEDURE P$SUBMIT_FOR_CLOSING(pReason in VARCHAR2)
AS
  CURSOR discr_status_closing_cursor IS
  select d.DISCREPANCY_ID
    from KNP_DISCREPANCY_CLOSING c join KNP_DISCREPANCY_CLOSED d on d.DISCREPANCY_ID = c.DISCREPANCY_ID
  union
  select c.DISCREPANCY_ID
    from KNP_DISCREPANCY_CLOSING c join SOV_DISCREPANCY d on d.ID = c.DISCREPANCY_ID
  where d.STATUS = 2;

  TYPE discr_status_closing IS TABLE OF discr_status_closing_cursor%rowtype INDEX BY BINARY_INTEGER;
  var_discr_status_closing discr_status_closing;

  pReasonTrim VARCHAR2(50 BYTE) := TRIM(pReason);
  pID number;
  pDate date := SYSTIMESTAMP;
  dID VARCHAR2(64);
BEGIN
  begin
    select ID into pID from R$KNP_DISCREPANCY_CLOSE_REASON
    where DESCRIPTION = pReasonTrim;
	exception when no_data_found then
		pID := null;
  end;

  if (pID is null) then
  begin
    select SEQ$KNP_DISCR_CLOSE_REASON.NEXTVAL into pID from dual;

    insert into R$KNP_DISCREPANCY_CLOSE_REASON (ID, DESCRIPTION) values (pID, pReasonTrim);

	commit;
	exception when others then
	begin
		DBMS_STANDARD.raise_application_error(num => -20100
		, msg => 'Ошибка при попытке добавить новую запись в R$KNP_DISCREPANCY_CLOSE_REASON: ('||pReason||')' );
	end;
  end;
  end if;

  OPEN discr_status_closing_cursor;
  LOOP
    FETCH discr_status_closing_cursor BULK COLLECT INTO var_discr_status_closing LIMIT 10000;

	FOR rec IN 1..var_discr_status_closing.COUNT
	LOOP
	  dID := TO_CHAR(var_discr_status_closing(rec).DISCREPANCY_ID);
      NDS2$SYS.LOG_INFO('PAC$KNP_DISCREPANCY', dID, '2', 'КНП: '||dID||' уже было закрыто');
	END LOOP;

    EXIT WHEN discr_status_closing_cursor%NOTFOUND;
  END LOOP;
  CLOSE discr_status_closing_cursor;

  begin
  	insert into KNP_DISCREPANCY_CLOSED (discrepancy_id, closed_at, reason_id)
	select closing.DISCREPANCY_ID, pDate, pID 
	from KNP_DISCREPANCY_CLOSING closing
	     join SOV_DISCREPANCY d on closing.DISCREPANCY_ID = d.ID
	     left join KNP_DISCREPANCY_CLOSED closed on closed.DISCREPANCY_ID = closing.DISCREPANCY_ID
	where (d.STATUS = 1) and closed.DISCREPANCY_ID is null;

	commit;
	NDS2$SYS.LOG_INFO('PAC$KNP_DISCREPANCY', null, null, 'Номера КНП записаны в KNP_DISCREPANCY_CLOSED');

	exception when others then
	begin
		DBMS_STANDARD.raise_application_error(num => -20101
		, msg => 'Ошибка при попытке добавить записи в KNP_DISCREPANCY_CLOSED' );
	end;
  end;

END;

PROCEDURE P$CLOSE_SUBMITTED(pDateFrom date)
AS
  CURSOR discr_statusone_closing_cursor IS
  select c.DISCREPANCY_ID, d."STATUS"
    from KNP_DISCREPANCY_CLOSED c join SOV_DISCREPANCY d on d.ID = c.DISCREPANCY_ID
	where c.closed_at > nvl(pDateFrom, c.closed_at + interval '-1' day) and d.STATUS = 1;

  TYPE discr_statusone_closing IS TABLE OF discr_statusone_closing_cursor%rowtype INDEX BY BINARY_INTEGER;
  var_discr_statusone_closing discr_statusone_closing;

  pDate date := SYSTIMESTAMP;

  dml_errors EXCEPTION;
  PRAGMA EXCEPTION_INIT (dml_errors, -24381);
BEGIN
  OPEN discr_statusone_closing_cursor;
  LOOP
    FETCH discr_statusone_closing_cursor BULK COLLECT INTO var_discr_statusone_closing LIMIT 10000;

	FORALL i IN 1..var_discr_statusone_closing.COUNT SAVE EXCEPTIONS
      update SOV_DISCREPANCY set
	    CLOSE_DATE = pDate
	   ,"STATUS" = 2
	  where ID = var_discr_statusone_closing(i).DISCREPANCY_ID;

    FORALL i IN 1..var_discr_statusone_closing.COUNT SAVE EXCEPTIONS
	  update HIVE_DISCREPANCY_DECL_CACHE set
	    DISCREPANCY_STATUS = 2
	  where DISCREPANCY_ID = var_discr_statusone_closing(i).DISCREPANCY_ID;

    FORALL i IN 1..var_discr_statusone_closing.COUNT SAVE EXCEPTIONS
      update ACT_DISCREPANCY set
	    "STATUS" = 2
	  where ID = var_discr_statusone_closing(i).DISCREPANCY_ID;

    FORALL i IN 1..var_discr_statusone_closing.COUNT SAVE EXCEPTIONS
      update DECISION_DISCREPANCY set
	    "STATUS" = 2
	  where ID = var_discr_statusone_closing(i).DISCREPANCY_ID;

    FORALL i IN 1..var_discr_statusone_closing.COUNT SAVE EXCEPTIONS
	  update ACT_DISCREPANCY_STAGE set
	    "STATUS" = 2
	  where ID = var_discr_statusone_closing(i).DISCREPANCY_ID;

	EXIT WHEN discr_statusone_closing_cursor%NOTFOUND;
  END LOOP;
  CLOSE discr_statusone_closing_cursor;
  COMMIT;
  NDS2$SYS.LOG_INFO('PAC$KNP_DISCREPANCY', null, null, 'КНП закрыты');
  
  EXCEPTION 
  WHEN dml_errors THEN
  BEGIN
    FOR i IN 1..SQL%BULK_EXCEPTIONS.COUNT
    LOOP
		DBMS_STANDARD.raise_application_error(num => -20102
		, msg => SQLERRM(-SQL%BULK_EXCEPTIONS(i).ERROR_CODE)||' ID:'||var_discr_statusone_closing(SQL%BULK_EXCEPTIONS(i).ERROR_INDEX).DISCREPANCY_ID);
    END LOOP;
  END;
  WHEN others THEN
  BEGIN
    RAISE;
  END;
END;

PROCEDURE P$CLOSE_DISCREPANCY_LIST
AS
vDateSuccess date;
vDate date := SYSTIMESTAMP;
BEGIN
  select da.LAST_SUCCESS into vDateSuccess
  from DUAL left join DAILY_ACTION da on da.ID = DAILY_ACTION_KEY_NAME;

  PAC$KNP_DISCREPANCY.P$CLOSE_SUBMITTED (pDateFrom => vDateSuccess);

  merge into DAILY_ACTION da
  using (select vDate as NEW_DATE, DAILY_ACTION_KEY_NAME as ID from DUAL) src on (src.ID = da.ID)
  when matched then 
    update set da.LAST_SUCCESS = src.NEW_DATE
  when not matched then
    insert values (src.ID, src.NEW_DATE);
  commit;
 
END;

PROCEDURE P$UPDATE_SOV_DISCREP_AGGREGATE
AS
vDate date;
vTotalQty number;
BEGIN
vDate := sysdate;

execute immediate 'truncate table sov_discrepancy_aggregate';

for item in (select t.seller_zip, t.seller_inn,
sum(case when type = 1 then nvl(gap_amt_9, 0) else 0 end) as gap_amt_9,
sum(case when type = 1 then nvl(gap_qty_9, 0) else 0 end) as gap_qty_9,
sum(case when type <> 1 then nvl(other_amt_9, 0) else 0 end) as other_amt_9,
sum(case when type <> 1 then nvl(othe_qty_9, 0) else 0 end) as othe_qty_9,
sum(case when t.type = 1 then 1 else 0 end) as gap_contragent_qty_9,
sum(case when t.type <> 1 then 1 else 0 end) as other_contragent_qty_9
from(
select   
seller_zip,
seller_inn,
buyer_inn,
type,
sum(case when type = 1 and (invoice_contractor_chapter in( 99, 9) or invoice_contractor_chapter is null)  then nvl(amnt, 0) else 0 end) as gap_amt_9,
sum(case when type = 1 and (invoice_contractor_chapter in( 99, 9) or invoice_contractor_chapter is null)  then 1 else 0 end) as gap_qty_9,
sum(case when type <> 1 and (invoice_contractor_chapter in( 99, 9) or invoice_contractor_chapter is null)  then nvl(amnt, 0) else 0 end) as other_amt_9,
sum(case when type <> 1 and (invoice_contractor_chapter in( 99, 9) or invoice_contractor_chapter is null)  then 1 else 0 end) as othe_qty_9
from sov_discrepancy where status = 1 and seller_zip is not null GROUP by seller_zip, seller_inn, buyer_inn, type ) t group by t.seller_zip, t.seller_inn)
 LOOP
 select count(distinct buyer_inn) into vTotalQty from sov_discrepancy where status = 1 and seller_zip = item.seller_zip;
 insert into sov_discrepancy_aggregate (SELLER_ZIP, SELLER_INN, AGGREGATE_DATE, GAP_AMT_9, GAP_QTY_9, GAP_CONTRAGENT_QTY_9, OTHER_AMT_9, OTHER_QTY_9, OTHER_CONTRAGENT_QTY_9, TOTAL_CONTRAGENT_QTY)
 values (item.seller_zip, item.seller_inn, vDate, item.gap_amt_9, item.gap_qty_9, item.gap_contragent_qty_9, item.other_amt_9, item.othe_qty_9, item.other_contragent_qty_9, vTotalQty);
 END LOOP;
 commit;
END;

END PAC$KNP_DISCREPANCY;
/
