﻿CREATE OR REPLACE PACKAGE NDS2_MRR_USER.PAC$REMARKABLE_CHANGES as
procedure RESET_CHANGES(
	P_REG_NUM number,
	P_SONO_CODE varchar2
);
procedure RESET_CHANGES(
  P_INN varchar2,
  P_KPP varchar2,
  P_YEAR  number,
  P_PERIOD  varchar2
  );

procedure RESET_MARK (
  P_INN_CONTRACTOR varchar2,
  P_KPP_EFFECTIVE varchar2,
  P_TYPE  number,
  P_PERIOD  varchar2,
  P_YEAR  varchar2
  );
procedure RESET_HAS_CHANGES (
  P_INN_CONTRACTOR varchar2,
  P_KPP_EFFECTIVE varchar2,
  P_TYPE  number,
  P_PERIOD  varchar2,
  P_YEAR  varchar2
  );
procedure SET_HAS_CHANGES (
  P_INN_CONTRACTOR varchar2,
  P_KPP_EFFECTIVE varchar2,
  P_TYPE  number,
  P_PERIOD  varchar2,
  P_YEAR  varchar2
  );
procedure RESET_HAS_AT (
  P_INN_CONTRACTOR varchar2,
  P_KPP_EFFECTIVE varchar2,
  P_TYPE  number,
  P_PERIOD  varchar2,
  P_YEAR  varchar2
  );
procedure SET_HAS_AT (
  P_INN_CONTRACTOR varchar2,
  P_KPP varchar2,
  P_TYPE  number,
  P_PERIOD  varchar2,
  P_YEAR  varchar2
  );
procedure SLIDE_AT_UP (
  P_INN_CONTRACTOR varchar2,
  P_KPP_EFFECTIVE varchar2,
  P_TYPE  number,
  P_PERIOD  varchar2,
  P_YEAR  varchar2
  );
procedure SLIDE_AT_DOWN (
  P_INN_CONTRACTOR varchar2,
  P_KPP_EFFECTIVE varchar2,
  P_TYPE  number,
  P_PERIOD  varchar2,
  P_YEAR  varchar2
  );
procedure NO_TKS_UP (
  P_INN_CONTRACTOR varchar2,
  P_KPP_EFFECTIVE varchar2,
  P_TYPE  number,
  P_PERIOD  varchar2,
  P_YEAR  varchar2
  );
procedure NO_TKS_DOWN (
  P_INN_CONTRACTOR varchar2,
  P_KPP_EFFECTIVE varchar2,
  P_TYPE  number,
  P_PERIOD  varchar2,
  P_YEAR  varchar2
  );
procedure NO_TKS_DOWN_VERSION (
  P_ZIP number
  );

procedure NO_TKS_UP_VERSION (P_ZIP number);
--Запускает асинхронный (через JOB) пересчет количества автотребований для признака значимых изменений
--Вызов эквивалентен асинхронному запуску  SLIDE_AT_UP
procedure SLIDE_AT_UP_ASYNC(
    --набор параметров представляет собой ключ декларации
    P_INN_CONTRACTOR varchar2,
    P_KPP_EFFECTIVE varchar2,
    P_TYPE  number,
    P_PERIOD  varchar2,
    P_YEAR  varchar2);
    
--Запускает асинхронное (через JOB) выставление признака наличия автотребований
--Вызов эквивалентен асинхронному запуску SET_HAS_AT
procedure SET_HAS_AT_ASYNC (
    --Параметры соответствуют идентификатору декларации
    P_INN_CONTRACTOR varchar2,
    P_KPP varchar2,
    P_TYPE  number,
    P_PERIOD  varchar2,
    P_YEAR  varchar2);

function ADD_WORK_DAYS(p_date date, p_days_count number) return date;

end;
/
CREATE OR REPLACE PACKAGE BODY NDS2_MRR_USER.PAC$REMARKABLE_CHANGES as

/****************************************************************************/
/*****************Внутренние процедуры и функции пакета ********************/
/***************************************************************************/
--Возвращает идентификатор декларации в виде строки, перечисления PARAMETERNAME1: PARAMETERVALUE1, ..., PARAMETERNAME2: PARAMETERVALUE2
function GET_KEY_STRING(
    --Параметры соответствуют идентификатору декларации
    P_INN_CONTRACTOR varchar2,
    P_KPP_EFFECTIVE varchar2,
    P_TYPE  number,
    P_PERIOD  varchar2,
    P_YEAR  varchar2)
return varchar2
as
begin
  return ' P_INN_CONTRACTOR: ''' || P_INN_CONTRACTOR || ''' P_KPP: ''' || P_KPP_EFFECTIVE || ''' P_TYPE: ''' || P_TYPE  || ''' P_PERIOD: ''' || P_PERIOD || ''' P_YEAR: ''' || P_YEAR || ''' ';
end;

/*log smth message*/
procedure LOG(p_msg varchar2, p_type number)
as
pragma autonomous_transaction;
begin
  /* dbms_output.put_line(p_type||'-'||p_msg); */
  insert into SYSTEM_LOG(ID, TYPE, SITE, ENTITY_ID, MESSAGE_CODE, MESSAGE, LOG_DATE)
  values(SEQ_SYS_LOG_ID.NEXTVAL, p_type, 'PAC$REMARKABLE_CHANGES', null, 1, p_msg, sysdate);
  commit;
exception when others then
  rollback;
end;

/*log warn message*/
procedure log_info(p_msg varchar2)
as
begin
     log(p_msg, 0);
end;

/*log error message*/
procedure log_err(p_msg varchar2)
as
begin
     log(p_msg, 3);
end;

/*execute single query*/
procedure execute_short_query(p_sql in varchar2)
as
begin
  execute immediate p_sql;
exception when others then log_err(substr(sqlerrm, 1, 128)||':'||p_sql);
end;

/*close cursor handle*/
procedure close_cursor_handle(p_hndl in out int)
  as
begin
  if DBMS_SQL.is_open(p_hndl) then
       DBMS_SQL.close_cursor(p_hndl);
  end if;
end;

/**********************************************************/

procedure RESET_MARK (
  P_INN_CONTRACTOR varchar2,
  P_KPP_EFFECTIVE varchar2,
  P_TYPE  number,
  P_PERIOD  varchar2,
  P_YEAR  varchar2
  )
  as
  begin
    merge into DROPPED_MARKS trg
    using (select 1 from dual) src
    on (trg.INN_CONTRACTOR = P_INN_CONTRACTOR and trg.KPP_EFFECTIVE = P_KPP_EFFECTIVE
        and trg.TYPE_CODE = P_TYPE and trg.PERIOD_CODE = P_PERIOD and trg.FISCAL_YEAR = P_YEAR)
    when not matched then
    insert (trg.INN_CONTRACTOR, trg.KPP_EFFECTIVE, trg.TYPE_CODE, trg.PERIOD_CODE, trg.FISCAL_YEAR, trg.dropped, trg.drop_date)
    values (P_INN_CONTRACTOR, P_KPP_EFFECTIVE, P_TYPE, P_PERIOD, P_YEAR, 1, sysdate);
    commit;
  exception when others then
    rollback;
    Nds2$sys.LOG_ERROR('PAC$REMARKABLE_CHANGES.RESET_MARK', null, sqlcode, sqlerrm);
  end;

/**********************************************************************************
***********************************************************************************/
procedure RESET_HAS_CHANGES (
  P_INN_CONTRACTOR varchar2,
  P_KPP_EFFECTIVE varchar2,
  P_TYPE  number,
  P_PERIOD  varchar2,
  P_YEAR  varchar2
  )
  as
  begin
    merge into SIGNIFICANT_CHANGES trg
    using (select 1 as ID from dual) src
    on (trg.INN_CONTRACTOR = P_INN_CONTRACTOR and trg.KPP_EFFECTIVE = P_KPP_EFFECTIVE
        and trg.TYPE_CODE = P_TYPE and trg.PERIOD_CODE = P_PERIOD and trg.FISCAL_YEAR = P_YEAR)
    when matched then
    update set HAS_CHANGES=0
  when not matched then
  Insert ( trg.INN_CONTRACTOR, trg.KPP_EFFECTIVE, trg.TYPE_CODE, trg.PERIOD_CODE, trg.FISCAL_YEAR, HAS_CHANGES)
  Values (P_INN_CONTRACTOR, P_KPP_EFFECTIVE, P_TYPE, P_PERIOD, P_YEAR, 0);

    merge into DECLARATION_ACTIVE trg
    using (select 1 as ID from dual) src
    on (trg.INN_CONTRACTOR = P_INN_CONTRACTOR and trg.KPP_EFFECTIVE = P_KPP_EFFECTIVE
        and trg.TYPE_CODE = P_TYPE and trg.PERIOD_CODE = P_PERIOD and trg.FISCAL_YEAR = P_YEAR)
    when matched then
    update set HAS_CHANGES=0;

  merge into DECLARATION_ACTIVE_SEARCH trg
    using (select 1 as ID from dual) src
    on (trg.INN_CONTRACTOR = P_INN_CONTRACTOR and trg.KPP_EFFECTIVE = P_KPP_EFFECTIVE
        and trg.DECL_TYPE_CODE = P_TYPE and trg.TAX_PERIOD = P_PERIOD and trg.FISCAL_YEAR = P_YEAR)
    when matched then
    update set HAS_CHANGES=0;

	log('PAC$REMARKABLE_CHANGES.RESET_HAS_CHANGES была вызвана для '''||P_INN_CONTRACTOR||''' ',0);

    commit;
  exception when others then
    rollback;
    Nds2$sys.LOG_ERROR('PAC$REMARKABLE_CHANGES.RESET_HAS_CHANGES', null, sqlcode, sqlerrm);
  end;

/**********************************************************************************
***********************************************************************************/
procedure SET_HAS_CHANGES (
  P_INN_CONTRACTOR varchar2,
  P_KPP_EFFECTIVE varchar2,
  P_TYPE  number,
  P_PERIOD  varchar2,
  P_YEAR  varchar2
  )
  as
  begin
    merge into SIGNIFICANT_CHANGES trg
    using (select 1 as ID from dual) src
    on (trg.INN_CONTRACTOR = P_INN_CONTRACTOR and trg.KPP_EFFECTIVE = P_KPP_EFFECTIVE
        and trg.TYPE_CODE = P_TYPE and trg.PERIOD_CODE = P_PERIOD and trg.FISCAL_YEAR = P_YEAR)
    when matched then
      update set HAS_CHANGES=1
    when not matched then
      insert ( trg.INN_CONTRACTOR, trg.KPP_EFFECTIVE, trg.TYPE_CODE, trg.PERIOD_CODE, trg.FISCAL_YEAR, HAS_CHANGES)
      values (P_INN_CONTRACTOR, P_KPP_EFFECTIVE, P_TYPE, P_PERIOD, P_YEAR, 1);

    merge into DECLARATION_ACTIVE trg
    using (select 1 as ID from dual) src
    on (trg.INN_CONTRACTOR = P_INN_CONTRACTOR and trg.KPP_EFFECTIVE = P_KPP_EFFECTIVE
        and trg.TYPE_CODE = P_TYPE and trg.PERIOD_CODE = P_PERIOD and trg.FISCAL_YEAR = P_YEAR)
    when matched then
      update set HAS_CHANGES=1;

    merge into DECLARATION_ACTIVE_SEARCH trg
    using (select 1 as ID from dual) src
    on (trg.INN_CONTRACTOR = P_INN_CONTRACTOR and trg.KPP_EFFECTIVE = P_KPP_EFFECTIVE
      and trg.DECL_TYPE_CODE = P_TYPE and trg.TAX_PERIOD = P_PERIOD and trg.FISCAL_YEAR = P_YEAR)
    when matched then
      update set HAS_CHANGES=1;

	log('PAC$REMARKABLE_CHANGES.SET_HAS_CHANGES вызвана для '''||P_INN_CONTRACTOR||''' ',0);

    commit;
  exception when others then
    rollback;
    Nds2$sys.LOG_ERROR('PAC$REMARKABLE_CHANGES.SET_HAS_CHANGES', null, sqlcode, sqlerrm);
  end;

/**********************************************************************************
***********************************************************************************/
procedure RESET_HAS_AT (
  P_INN_CONTRACTOR varchar2,
  P_KPP_EFFECTIVE varchar2,
  P_TYPE  number,
  P_PERIOD  varchar2,
  P_YEAR  varchar2
  )
  as
  begin
    merge into SIGNIFICANT_CHANGES trg
    using (select 1 as ID from dual) src
    on (trg.INN_CONTRACTOR = P_INN_CONTRACTOR and trg.KPP_EFFECTIVE = P_KPP_EFFECTIVE
        and trg.TYPE_CODE = P_TYPE and trg.PERIOD_CODE = P_PERIOD and trg.FISCAL_YEAR = P_YEAR)
    when matched then
      update set HAS_CHANGES=0, HAS_AT=0
    when not matched then
      insert ( trg.INN_CONTRACTOR, trg.KPP_EFFECTIVE, trg.TYPE_CODE, trg.PERIOD_CODE, trg.FISCAL_YEAR, HAS_CHANGES, HAS_AT)
      values (P_INN_CONTRACTOR, P_KPP_EFFECTIVE, P_TYPE, P_PERIOD, P_YEAR, 0, 0);

    merge into DECLARATION_ACTIVE trg
    using (select 1 as ID from dual) src
    on (trg.INN_CONTRACTOR = P_INN_CONTRACTOR and trg.KPP_EFFECTIVE = P_KPP_EFFECTIVE
        and trg.TYPE_CODE = P_TYPE and trg.PERIOD_CODE = P_PERIOD and trg.FISCAL_YEAR = P_YEAR)
    when matched then
      update set HAS_CHANGES=0, HAS_AT=0;

    merge into DECLARATION_ACTIVE_SEARCH trg
    using (select 1 as ID from dual) src
    on (trg.INN_CONTRACTOR = P_INN_CONTRACTOR and trg.KPP_EFFECTIVE = P_KPP_EFFECTIVE
      and trg.DECL_TYPE_CODE = P_TYPE and trg.TAX_PERIOD = P_PERIOD and trg.FISCAL_YEAR = P_YEAR)
    when matched then
      update set HAS_CHANGES=0, HAS_AT=0;

	log('PAC$REMARKABLE_CHANGES.RESET_HAS_AT has been called for '''||P_INN_CONTRACTOR||''' ',0);

  commit;
  exception when others then
    rollback;
    Nds2$sys.LOG_ERROR('PAC$REMARKABLE_CHANGES.RESET_HAS_AT', null, sqlcode, sqlerrm);
  end;

/**********************************************************************************
***********************************************************************************/
procedure SET_HAS_AT (
  P_INN_CONTRACTOR varchar2,
  P_KPP varchar2,
  P_TYPE  number,
  P_PERIOD  varchar2,
  P_YEAR  varchar2
  )
  as
    P_KPP_EFFECTIVE varchar2(9);
  begin

    P_KPP_EFFECTIVE := F$GET_EFFECTIVE_KPP(P_INN_CONTRACTOR, P_KPP);

    merge into SIGNIFICANT_CHANGES trg
    using (select 1 as ID from dual) src
    on (trg.INN_CONTRACTOR = P_INN_CONTRACTOR and trg.KPP_EFFECTIVE = P_KPP_EFFECTIVE
        and trg.TYPE_CODE = P_TYPE and trg.PERIOD_CODE = P_PERIOD and trg.FISCAL_YEAR = P_YEAR)
    when matched then
      update set HAS_CHANGES=1, HAS_AT=1
    when not matched then
      insert ( trg.INN_CONTRACTOR, trg.KPP_EFFECTIVE, trg.TYPE_CODE, trg.PERIOD_CODE, trg.FISCAL_YEAR, HAS_CHANGES, HAS_AT)
      values (P_INN_CONTRACTOR, P_KPP_EFFECTIVE, P_TYPE, P_PERIOD, P_YEAR, 1, 1);

    merge into DECLARATION_ACTIVE trg
    using (select 1 as ID from dual) src
    on (trg.INN_CONTRACTOR = P_INN_CONTRACTOR and trg.KPP_EFFECTIVE = P_KPP_EFFECTIVE
        and trg.TYPE_CODE = P_TYPE and trg.PERIOD_CODE = P_PERIOD and trg.FISCAL_YEAR = P_YEAR)
    when matched then
      update set HAS_CHANGES=1, HAS_AT=1;

    merge into DECLARATION_ACTIVE_SEARCH trg
    using (select 1 as ID from dual) src
    on (trg.INN_CONTRACTOR = P_INN_CONTRACTOR and trg.KPP_EFFECTIVE = P_KPP_EFFECTIVE
      and trg.DECL_TYPE_CODE = P_TYPE and trg.TAX_PERIOD = P_PERIOD and trg.FISCAL_YEAR = P_YEAR)
    when matched then
      update set HAS_CHANGES=1, HAS_AT=1;

	log('PAC$REMARKABLE_CHANGES.SET_HAS_AT has been called for '''||P_INN_CONTRACTOR||''' ',0);

    commit;
  exception when others then
    rollback;
    Nds2$sys.LOG_ERROR('PAC$REMARKABLE_CHANGES.SET_HAS_AT', null, sqlcode, sqlerrm);
  end;

/**********************************************************************************
***********************************************************************************/
procedure SLIDE_AT_UP (
  P_INN_CONTRACTOR varchar2,
  P_KPP_EFFECTIVE varchar2,
  P_TYPE  number,
  P_PERIOD  varchar2,
  P_YEAR  varchar2
  )
as
begin
    merge into SIGNIFICANT_CHANGES trg
    using (select 1 as ID from dual) src
    on (trg.INN_CONTRACTOR = P_INN_CONTRACTOR and trg.KPP_EFFECTIVE = P_KPP_EFFECTIVE
        and trg.TYPE_CODE = P_TYPE and trg.PERIOD_CODE = P_PERIOD and trg.FISCAL_YEAR = P_YEAR)
    when matched then
      update set HAS_CHANGES=1, SLIDE_AT_COUNT=NVL(SLIDE_AT_COUNT,0)+1
    when not matched then
      insert ( trg.INN_CONTRACTOR, trg.KPP_EFFECTIVE, trg.TYPE_CODE, trg.PERIOD_CODE, trg.FISCAL_YEAR, HAS_CHANGES, SLIDE_AT_COUNT)
      values (P_INN_CONTRACTOR, P_KPP_EFFECTIVE, P_TYPE, P_PERIOD, P_YEAR, 1, 1);

    merge into DECLARATION_ACTIVE trg
    using (select 1 as ID from dual) src
    on (trg.INN_CONTRACTOR = P_INN_CONTRACTOR and trg.KPP_EFFECTIVE = P_KPP_EFFECTIVE
        and trg.TYPE_CODE = P_TYPE and trg.PERIOD_CODE = P_PERIOD and trg.FISCAL_YEAR = P_YEAR)
    when matched then
      update set HAS_CHANGES=1, SLIDE_AT_COUNT=NVL(SLIDE_AT_COUNT,0)+1;

    merge into DECLARATION_ACTIVE_SEARCH trg
    using (select 1 as ID from dual) src
    on (trg.INN_CONTRACTOR = P_INN_CONTRACTOR and trg.KPP_EFFECTIVE = P_KPP_EFFECTIVE
      and trg.DECL_TYPE_CODE = P_TYPE and trg.TAX_PERIOD = P_PERIOD and trg.FISCAL_YEAR = P_YEAR)
    when matched then
      update set HAS_CHANGES=1, SLIDE_AT_COUNT=NVL(SLIDE_AT_COUNT,0)+1;

	log('PAC$REMARKABLE_CHANGES.SLIDE_AT_UP has been called for '''||P_INN_CONTRACTOR||''' ',0);

    commit;
  exception when others then
    rollback;
    Nds2$sys.LOG_ERROR('PAC$REMARKABLE_CHANGES.SLIDE_AT_UP', null, sqlcode, sqlerrm);
end;

/**********************************************************************************
***********************************************************************************/
procedure SLIDE_AT_DOWN (
  P_INN_CONTRACTOR varchar2,
  P_KPP_EFFECTIVE varchar2,
  P_TYPE  number,
  P_PERIOD  varchar2,
  P_YEAR  varchar2
  )
as
begin
    merge into SIGNIFICANT_CHANGES trg
    using (select 1 as ID from dual) src
    on (trg.INN_CONTRACTOR = P_INN_CONTRACTOR and trg.KPP_EFFECTIVE = P_KPP_EFFECTIVE
        and trg.TYPE_CODE = P_TYPE and trg.PERIOD_CODE = P_PERIOD and trg.FISCAL_YEAR = P_YEAR)
    when matched then
      update set SLIDE_AT_COUNT = case when NVL(SLIDE_AT_COUNT,0) > 0 then NVL(SLIDE_AT_COUNT,0) - 1 else 0 end,
                 HAS_CHANGES = (case when (nvl(has_at,0) + nvl(NO_TKS_COUNT,0) + 
                                          (case when NVL(SLIDE_AT_COUNT,0) > 0 then NVL(SLIDE_AT_COUNT,0) - 1 else 0 end)) > 0 
                                     then 1 else 0 end)
    when not matched then
      insert ( trg.INN_CONTRACTOR, trg.KPP_EFFECTIVE, trg.TYPE_CODE, trg.PERIOD_CODE, trg.FISCAL_YEAR, HAS_CHANGES, SLIDE_AT_COUNT)
      values (P_INN_CONTRACTOR, P_KPP_EFFECTIVE, P_TYPE, P_PERIOD, P_YEAR, 1, 0);

    merge into DECLARATION_ACTIVE trg
    using (select 1 as ID from dual) src
    on (trg.INN_CONTRACTOR = P_INN_CONTRACTOR and trg.KPP_EFFECTIVE = P_KPP_EFFECTIVE
        and trg.TYPE_CODE = P_TYPE and trg.PERIOD_CODE = P_PERIOD and trg.FISCAL_YEAR = P_YEAR)
    when matched then
      update set SLIDE_AT_COUNT = case when NVL(SLIDE_AT_COUNT,0) > 0 then NVL(SLIDE_AT_COUNT,0) - 1 else 0 end,
                 HAS_CHANGES = (case when (nvl(has_at,0) + nvl(NO_TKS_COUNT,0) + 
                                          (case when NVL(SLIDE_AT_COUNT,0) > 0 then NVL(SLIDE_AT_COUNT,0) - 1 else 0 end)) > 0 
                                     then 1 else 0 end);

    merge into DECLARATION_ACTIVE_SEARCH trg
    using (select 1 as ID from dual) src
    on (trg.INN_CONTRACTOR = P_INN_CONTRACTOR and trg.KPP_EFFECTIVE = P_KPP_EFFECTIVE
      and trg.DECL_TYPE_CODE = P_TYPE and trg.TAX_PERIOD = P_PERIOD and trg.FISCAL_YEAR = P_YEAR)
    when matched then
      update set SLIDE_AT_COUNT = case when NVL(SLIDE_AT_COUNT,0) > 0 then NVL(SLIDE_AT_COUNT,0) - 1 else 0 end,
                 HAS_CHANGES = (case when (nvl(has_at,0) + nvl(NO_TKS_COUNT,0) + 
                                          (case when NVL(SLIDE_AT_COUNT,0) > 0 then NVL(SLIDE_AT_COUNT,0) - 1 else 0 end)) > 0 
                                     then 1 else 0 end);

	log('PAC$REMARKABLE_CHANGES.SLIDE_AT_DOWN has been called for '''||P_INN_CONTRACTOR||''' ',0);

    commit;
  exception when others then
    rollback;
    Nds2$sys.LOG_ERROR('PAC$REMARKABLE_CHANGES.SLIDE_AT_DOWN', null, sqlcode, sqlerrm);
end;

/**********************************************************************************
Увеличивает на единицу счетчик пояснений для декларации 
***********************************************************************************/
procedure NO_TKS_UP (
  P_INN_CONTRACTOR varchar2,
  P_KPP_EFFECTIVE varchar2,
  P_TYPE  number,
  P_PERIOD  varchar2,
  P_YEAR  varchar2
  )
as
begin
    log_info('NO_TKS_UP : start [P_INN_CONTRACTOR]='''||to_char(P_INN_CONTRACTOR)||''', [P_KPP_EFFECTIVE]='''||to_char(P_KPP_EFFECTIVE)||''', [P_TYPE]='''||to_char(P_TYPE)||''', [P_PERIOD]='''||to_char(P_PERIOD)||''', [P_YEAR]='''||to_char(P_YEAR)||''' ');

    merge into SIGNIFICANT_CHANGES trg
    using (select 1 as ID from dual) src
    on (trg.INN_CONTRACTOR = P_INN_CONTRACTOR and trg.KPP_EFFECTIVE = P_KPP_EFFECTIVE
        and trg.TYPE_CODE = P_TYPE and trg.PERIOD_CODE = P_PERIOD and trg.FISCAL_YEAR = P_YEAR)
    when matched then
      update set HAS_CHANGES = 1, NO_TKS_COUNT = NVL(NO_TKS_COUNT,0) + 1
    when not matched then
      insert ( trg.INN_CONTRACTOR, trg.KPP_EFFECTIVE, trg.TYPE_CODE, trg.PERIOD_CODE, trg.FISCAL_YEAR, HAS_CHANGES, NO_TKS_COUNT)
      values (P_INN_CONTRACTOR, P_KPP_EFFECTIVE, P_TYPE, P_PERIOD, P_YEAR, 1, 1);

    log_info('NO_TKS_UP : merge SIGNIFICANT_CHANGES finished');

    merge into DECLARATION_ACTIVE trg
    using (select 1 as ID from dual) src
    on (trg.INN_CONTRACTOR = P_INN_CONTRACTOR and trg.KPP_EFFECTIVE = P_KPP_EFFECTIVE
        and trg.TYPE_CODE = P_TYPE and trg.PERIOD_CODE = P_PERIOD and trg.FISCAL_YEAR = P_YEAR)
    when matched then
      update set HAS_CHANGES=1, NO_TKS_COUNT=NVL(NO_TKS_COUNT,0)+1;

    log_info('NO_TKS_UP : merge DECLARATION_ACTIVE finished');

    merge into DECLARATION_ACTIVE_SEARCH trg
    using (select 1 as ID from dual) src
    on (trg.INN_CONTRACTOR = P_INN_CONTRACTOR and trg.KPP_EFFECTIVE = P_KPP_EFFECTIVE
      and trg.DECL_TYPE_CODE = P_TYPE and trg.TAX_PERIOD = P_PERIOD and trg.FISCAL_YEAR = P_YEAR)
    when matched then
      update set HAS_CHANGES=1, NO_TKS_COUNT=NVL(NO_TKS_COUNT,0)+1;

    log_info('NO_TKS_UP : merge DECLARATION_ACTIVE_SEARCH finished');

	log('PAC$REMARKABLE_CHANGES.NO_TKS_UP has been called for '''||P_INN_CONTRACTOR||''' ',0);

    commit;
    log_info('NO_TKS_UP : end');
  exception when others then
    rollback;
    log_err('NO_TKS_UP : exception [sqlerrm]='||sqlerrm);
    Nds2$sys.LOG_ERROR('PAC$REMARKABLE_CHANGES.NO_TKS_UP', null, sqlcode, sqlerrm);
end;

/**********************************************************************************
Уменьшает на единицу счетчик пояснений для декларации 
***********************************************************************************/
procedure NO_TKS_DOWN (
  --Параметры соответствуют ключу декларации
  P_INN_CONTRACTOR varchar2,
  P_KPP_EFFECTIVE varchar2,
  P_TYPE  number,
  P_PERIOD  varchar2,
  P_YEAR  varchar2
  )
as
begin
    merge into SIGNIFICANT_CHANGES trg
    using (select 1 as ID from dual) src
    on (trg.INN_CONTRACTOR = P_INN_CONTRACTOR and trg.KPP_EFFECTIVE = P_KPP_EFFECTIVE
        and trg.TYPE_CODE = P_TYPE and trg.PERIOD_CODE = P_PERIOD and trg.FISCAL_YEAR = P_YEAR)
    when matched then
      update set NO_TKS_COUNT = case when NVL(NO_TKS_COUNT,0) > 0 then NVL(NO_TKS_COUNT,0) - 1 else 0 end,
                 HAS_CHANGES = (case when (nvl(has_at,0) + nvl(slide_at_count,0) + 
                                          (case when NVL(NO_TKS_COUNT,0) > 0 then NVL(NO_TKS_COUNT,0) - 1 else 0 end)) > 0 
                                     then 1 else 0 end)
    when not matched then
      insert ( trg.INN_CONTRACTOR, trg.KPP_EFFECTIVE, trg.TYPE_CODE, trg.PERIOD_CODE, trg.FISCAL_YEAR, HAS_CHANGES, NO_TKS_COUNT)
      values (P_INN_CONTRACTOR, P_KPP_EFFECTIVE, P_TYPE, P_PERIOD, P_YEAR, 1, 0);

    merge into DECLARATION_ACTIVE trg
    using (select 1 as ID from dual) src
    on (trg.INN_CONTRACTOR = P_INN_CONTRACTOR and trg.KPP_EFFECTIVE = P_KPP_EFFECTIVE
        and trg.TYPE_CODE = P_TYPE and trg.PERIOD_CODE = P_PERIOD and trg.FISCAL_YEAR = P_YEAR)
    when matched then
      update set NO_TKS_COUNT = case when NVL(NO_TKS_COUNT,0) > 0 then NVL(NO_TKS_COUNT,0) - 1 else 0 end,
                 HAS_CHANGES = (case when (nvl(has_at,0) + nvl(slide_at_count,0) + 
                                          (case when NVL(NO_TKS_COUNT,0) > 0 then NVL(NO_TKS_COUNT,0) - 1 else 0 end)) > 0 
                                     then 1 else 0 end);

    merge into DECLARATION_ACTIVE_SEARCH trg
    using (select 1 as ID from dual) src
    on (trg.INN_CONTRACTOR = P_INN_CONTRACTOR and trg.KPP_EFFECTIVE = P_KPP_EFFECTIVE
      and trg.DECL_TYPE_CODE = P_TYPE and trg.TAX_PERIOD = P_PERIOD and trg.FISCAL_YEAR = P_YEAR)
    when matched then
      update set NO_TKS_COUNT = case when NVL(NO_TKS_COUNT,0) > 0 then NVL(NO_TKS_COUNT,0) - 1 else 0 end,
                 HAS_CHANGES = (case when (nvl(has_at,0) + nvl(slide_at_count,0) + 
                                          (case when NVL(NO_TKS_COUNT,0) > 0 then NVL(NO_TKS_COUNT,0) - 1 else 0 end)) > 0 
                                     then 1 else 0 end);

    log('PAC$REMARKABLE_CHANGES.NO_TKS_DOWN has been called for, P_INN_CONTRACTOR = ''' || P_INN_CONTRACTOR || ''', P_KPP_EFFECTIVE = ''' || P_KPP_EFFECTIVE || ''', P_TYPE = ''' || P_TYPE || ''', P_PERIOD = ''' || P_PERIOD || ''', P_YEAR = ''' || P_YEAR||''' ',0);

    commit;
  exception when others then
    rollback;
    Nds2$sys.LOG_ERROR('PAC$REMARKABLE_CHANGES.NO_TKS_DOWN', null, sqlcode, sqlerrm);
end;

/**********************************************************************************
Увеличивает на единицу счетчик пояснений для декларации 
***********************************************************************************/
procedure NO_TKS_UP_VERSION (P_ZIP number)
as 
  v_inn	varchar2(12);
  v_kpp varchar2(9);
  v_type number;
  v_period varchar2(2);
  v_year varchar2(4);
  v_found boolean;
begin
  log_info('NO_TKS_UP_VERSION : start [p_zip]='||to_char(P_ZIP));

  begin
    select d.inn_contractor, d.kpp_effective, d.type_code, d.period_code, d.fiscal_year
    into v_inn, v_kpp, v_type, v_period, v_year
    from DECLARATION_HISTORY d
    where d.ZIP = P_ZIP;
    v_found := true;
  exception when NO_DATA_FOUND then
    v_found := false;
    log_info('NO_TKS_UP_VERSION : declaration not found [zip]='||P_ZIP);
  end;

  if v_found then
    log_info('NO_TKS_UP_VERSION : run REMARKABLE_CHANGES.NO_TKS_UP');
    PAC$REMARKABLE_CHANGES.NO_TKS_UP(  P_INN_CONTRACTOR => v_inn, P_KPP_EFFECTIVE => v_kpp, P_TYPE => v_type, P_PERIOD => v_period, P_YEAR => v_year);  
  else
    log('PAC$REMARKABLE_CHANGES.NO_TKS_UP_VERSION не найден индентификатор DECLARATION_VERSION_ID='||P_ZIP,0);
  end if;
  commit;
  log_info('NO_TKS_UP_VERSION : end');
exception when others then
  rollback;
  log_err('NO_TKS_UP_VERSION : exception [sqlerrm]='||sqlerrm);
  Nds2$sys.LOG_ERROR('PAC$REMARKABLE_CHANGES.NO_TKS_UP_VERSION', null, sqlcode, sqlerrm);
end;

/**********************************************************************************
***********************************************************************************/
procedure NO_TKS_DOWN_VERSION (P_ZIP number)
as
	v_inn	varchar2(12);
  v_kpp varchar2(9);
  v_type number;
  v_period varchar2(2);
  v_year varchar2(4);
begin
	select da.inn_contractor, da.kpp_effective, da.type_code, da.period_code, da.fiscal_year
  into v_inn, v_kpp, v_type, v_period, v_year
  from DECLARATION_ACTIVE DA
  where DA.ZIP = P_ZIP;

	if v_inn is not null Then
     NO_TKS_DOWN(v_inn, v_kpp, v_type, v_period, v_year);
	Else
		log('PAC$REMARKABLE_CHANGES.NO_TKS_DOWN_VERSION cannot find ID for DECLARATION_VERSION_ID='||P_ZIP,0);
	End If;
    commit;
  exception when others then
    rollback;
    Nds2$sys.LOG_ERROR('PAC$REMARKABLE_CHANGES.NO_TKS_DOWN_VERSION', null, sqlcode, sqlerrm);
end;

--Запускает асинхронный (через JOB) пересчет количества автотребований для признака значимых изменений
--Вызов эквивалентен асинхронному запуску SLIDE_AT_UP
procedure SLIDE_AT_UP_ASYNC(
    --Параметры соответствуют идентификатору декларации
    P_INN_CONTRACTOR varchar2,
    P_KPP_EFFECTIVE varchar2,
    P_TYPE  number,
    P_PERIOD  varchar2,
    P_YEAR  varchar2)
as
 v_job_id number;
pragma autonomous_transaction;
begin
  dbms_job.submit(
       job => v_job_id,
       what => 'begin PAC$REMARKABLE_CHANGES.SLIDE_AT_UP('''||P_INN_CONTRACTOR||''','''||P_KPP_EFFECTIVE||''','''|| P_TYPE || ''',''' || P_PERIOD || ''',''' || P_YEAR ||'''); end;');

  log('PAC$REMARKABLE_CHANGES.SLIDE_AT_UP_ASYNC: вызвана для ключа контрагента: ' || GET_KEY_STRING(P_INN_CONTRACTOR, P_KPP_EFFECTIVE, P_TYPE, P_PERIOD, P_YEAR ),0);
  commit;
  exception when others then
    begin
      log_err('PAC$REMARKABLE_CHANGES.SLIDE_AT_UP_ASYNC: ошибка при обработке записи для контрагента: ' || GET_KEY_STRING(P_INN_CONTRACTOR, P_KPP_EFFECTIVE, P_TYPE, P_PERIOD, P_YEAR ) || ': ' || substr(sqlerrm, 0, 256));
    end;
end;

--Запускает асинхронное (через JOB) выставление признака наличия автотребований
--Вызов эквивалентен асинхронному запуску SET_HAS_AT
procedure SET_HAS_AT_ASYNC (
    --Параметры соответствуют идентификатору декларации
    P_INN_CONTRACTOR varchar2,
    P_KPP varchar2,
    P_TYPE  number,
    P_PERIOD  varchar2,
    P_YEAR  varchar2)
as
 v_job_id number;
pragma autonomous_transaction;
begin
  dbms_job.submit(
       job => v_job_id,
       what => 'begin PAC$REMARKABLE_CHANGES.SET_HAS_AT('''||P_INN_CONTRACTOR||''','''||P_KPP||''','''|| P_TYPE || ''',''' || P_PERIOD || ''',''' || P_YEAR || '''); end;');
       log('SET_HAS_AT_ASYNC: ' || ' вызвана для ключа декларации: ' || GET_KEY_STRING(P_INN_CONTRACTOR, P_KPP, P_TYPE, P_PERIOD, P_YEAR )  ,0);
  commit;
  exception when others then
    begin
      log_err('SET_HAS_AT_ASYNC: ' || ' ошибка при обработке записи для ключа декларации: ' || GET_KEY_STRING(P_INN_CONTRACTOR, P_KPP, P_TYPE, P_PERIOD, P_YEAR ) || ': ' || substr(sqlerrm, 0, 256));
    end;
end;

/**********************************************************************************
***********************************************************************************/

procedure RESET_CHANGES(
	P_REG_NUM number,
	P_SONO_CODE varchar2
)
as
v_inn varchar2(12);
v_kpp varchar2(9);
v_fiscal_year number;
v_period varchar2(2);
begin
	begin
		select inn_contractor, kpp_effective, fiscal_year, period_code into v_inn, v_kpp, v_fiscal_year, v_period
		from declaration_history
		where reg_number = p_reg_num and sono_code_submited = p_sono_code;
	exception when NO_DATA_FOUND then
		log_info('RESET_CHANGES : declaration not found [reg_number]='||p_reg_num||', [sono_code]='||p_sono_code);
	end;
	
	reset_changes(v_inn, v_kpp, v_fiscal_year, v_period);
end;

procedure RESET_CHANGES(
  P_INN varchar2,
  P_KPP varchar2,
  P_YEAR  number,
  P_PERIOD  varchar2
  )
 as
 begin
 	merge into significant_changes t
	using (select inn_contractor, kpp_effective, fiscal_year, period_code, type_code from significant_changes where inn_contractor = p_inn and kpp_effective = p_kpp and fiscal_year = p_year and period_code = p_period and type_code = 0) s
		on (
				t.inn_contractor = s.inn_contractor
				and t.kpp_effective = s.kpp_effective
				and t.fiscal_year = s.fiscal_year
				and t.period_code = s.period_code
				and t.type_code = s.type_code
		   )
		when matched then
		update set t.has_changes = 0, t.has_at = 0, t.slide_at_count = 0, t.no_tks_count = 0;
	
 	merge into declaration_active t
	using (select 1 as ID from dual) s
		on (
				t.inn_contractor = p_inn
				and t.kpp_effective = p_kpp
				and t.fiscal_year = p_year
				and t.period_code = p_period
		   )
		when matched then
		update set t.has_changes = 0, t.has_at = 0, t.slide_at_count = 0, t.no_tks_count = 0;		
		
	commit;
 end;

/**********************************************************************************
Change the date (p_date) of a specified number of working days
Parameter 'p_days_count' can be negative.
***********************************************************************************/
function ADD_WORK_DAYS(p_date date, p_days_count number) return date
as
  cnt number;
  dt date := TRUNC(p_date);
  delta number;
  v_exist number;
begin
  if p_days_count>0
  then
    delta := 1;
    cnt := p_days_count;
  else
    delta := -1;
    cnt := -p_days_count;
  end if;
  while (cnt > 0) loop
    select count(1) into v_exist from redday where red_date = dt;
    while (v_exist > 0) loop
		dt := dt + delta;
        select count(1) into v_exist from redday where red_date = dt;
    end loop;
    dt := dt + delta;
    cnt := cnt - 1;
  end loop;
  return dt;
end;
end;
/
