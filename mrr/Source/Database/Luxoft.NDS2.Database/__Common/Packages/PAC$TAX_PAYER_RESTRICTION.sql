﻿create or replace package NDS2_MRR_USER.PAC$TAX_PAYER_RESTRICTION
as
  TYPE T$ARR_STR IS TABLE OF varchar2(12) INDEX BY PLS_INTEGER;

  procedure P$SAVE(pType in NUMBER,
				   pName in VARCHAR2,
				   pCreatedBy in VARCHAR2,
				   pIsActive in NUMBER,
				   pInnList in T$ARR_STR);
 
  procedure P$LOAD_INN_LIST(pId in NUMBER, pCursor OUT SYS_REFCURSOR);

  procedure P$UPDATE(pId in NUMBER, pIsActive in NUMBER);

  procedure P$ALL(pType in Number, pCursor OUT SYS_REFCURSOR);

  procedure P$ACTIVE_WHITE_LISTS(pCursor OUT SYS_REFCURSOR);

end;
/
create or replace package body NDS2_MRR_USER.PAC$TAX_PAYER_RESTRICTION
as

WHITE_LIST_TYPE       CONSTANT number(1)            := 1;
EXCLUDE_LIST_TYPE	  CONSTANT number(1)            := 2;
TAX_MONITOR_LIST_TYPE CONSTANT number(1)            := 3;


function F$VERIFY_LIST_NAME
(
  pName varchar2
) return varchar2
as
vIdx number := 0;
vExist number;
vName varchar2(1024);
begin
vName := trim(pName);
select count(1) into vExist from TAX_PAYER_RESTRICTION where name = vName and list_type = WHITE_LIST_TYPE;

while vExist >= 1 and vIdx < 10000 loop
   vIdx := vIdx+1;
   vName := trim(pName)||' ('||vIdx||')';
select count(1) into vExist from TAX_PAYER_RESTRICTION where name = vName and list_type = WHITE_LIST_TYPE;
end loop;

return vName;
end;
 
 procedure P$SAVE(pType in NUMBER,
				  pName in VARCHAR2,
				  pCreatedBy in VARCHAR2,
				  pIsActive in NUMBER,
				  pInnList in T$ARR_STR)
 as
 vName varchar2(1024);
 vId number;
 vInn varchar2(12);
 begin

 if pType in (EXCLUDE_LIST_TYPE, TAX_MONITOR_LIST_TYPE) then

 begin
 select id into vId from TAX_PAYER_RESTRICTION where list_type = pType;
 exception when others then
 vId := 0;
 end;

 if vId <> 0 then

 delete from TAX_PAYER_RESTRICTION_INN where LIST_ID = vId;
 for i in 1..pInnList.count loop
  vInn := pInnList(i);
  if vInn is not null then
  insert into TAX_PAYER_RESTRICTION_INN (LIST_ID, INN) values (vId, vInn);
  end if;
 end loop;
 
 else
  vId :=  SEQ$TAX_PAYER_RESTRICTION.nextval;
  insert into TAX_PAYER_RESTRICTION (ID, NAME, LIST_TYPE, CREATED_BY, CREATE_DATE, IS_ACTIVE) values
 (vId, pName, pType, pCreatedBy, sysdate, 1);

 for i in 1..pInnList.count loop
  vInn := pInnList(i);
  if vInn is not null then
  insert into TAX_PAYER_RESTRICTION_INN (LIST_ID, INN) values (vId, vInn);
  end if;
 end loop;
 end if;
else
 
 vName := F$VERIFY_LIST_NAME(pName);
 vId :=  SEQ$TAX_PAYER_RESTRICTION.nextval;
 insert into TAX_PAYER_RESTRICTION (ID, NAME, LIST_TYPE, CREATED_BY, CREATE_DATE, IS_ACTIVE) values
 (vId, vName, WHITE_LIST_TYPE, pCreatedBy, sysdate, pIsActive);

for i in 1..pInnList.count loop
  vInn := pInnList(i);
  insert into TAX_PAYER_RESTRICTION_INN (LIST_ID, INN) values (vId, vInn);
 end loop;
  end if;
 end; 

 
  procedure P$LOAD_INN_LIST(pId in NUMBER, pCursor OUT SYS_REFCURSOR)
  as
  begin

    open pCursor for
    select INN
    from TAX_PAYER_RESTRICTION_INN where LIST_ID = pId;

  end;

  procedure P$UPDATE(pId in NUMBER, pIsActive in NUMBER)
  as
  begin
    update TAX_PAYER_RESTRICTION set IS_ACTIVE = pIsActive where ID = pId;
  end;

  procedure P$ALL(pType in Number, pCursor OUT SYS_REFCURSOR)
  as
  begin
   open pCursor for
  select 
  ID, 
  NAME,
  LIST_TYPE as ListType,
  CREATED_BY as CreatedBy,
  CREATE_DATE as CreatedDate,
  IS_ACTIVE as IsActive
  from TAX_PAYER_RESTRICTION where LIST_TYPE = pType;
  end;

  procedure P$ACTIVE_WHITE_LISTS(pCursor OUT SYS_REFCURSOR)
  as
  begin
   open pCursor for
  select 
  ID, 
  NAME,
  LIST_TYPE as ListType,
  CREATED_BY as CreatedBy,
  CREATE_DATE as CreatedDate,
  IS_ACTIVE as IsActive
  from TAX_PAYER_RESTRICTION where LIST_TYPE = 1 and IS_ACTIVE = 1 order by CREATE_DATE;
  end;
end;
/