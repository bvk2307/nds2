﻿-- PAC$EXPORT_EXCEL specification
CREATE OR REPLACE PACKAGE NDS2_MRR_USER.PAC$EXPORT_EXCEL IS

  PROCEDURE P$GET_EXPORT_PATH(pResult OUT SYS_REFCURSOR);
  
  PROCEDURE P$GET_EXPORT_THRESHOLD(pResult OUT SYS_REFCURSOR);
    
  PROCEDURE P$ENQUEUE_SELECTION_DISCREP(
    pId OUT EXPORT_EXCEL_QUEUE.QUEUE_ITEM_ID%TYPE,
    pUserSid IN EXPORT_EXCEL_QUEUE.USER_SID%TYPE,
    pSelectionId IN SELECTION.ID%TYPE,
    pRowFilter IN EXPORT_SELECTION_DISCREPANCY.ROW_FILTER%TYPE
    );
    
  PROCEDURE P$DEQUEUE(
    pProcessId IN EXPORT_EXCEL_QUEUE.STARTED_BY_ID%TYPE,
    pResult OUT SYS_REFCURSOR
    );

  PROCEDURE P$FIND_BY_ID(
    pId IN EXPORT_EXCEL_QUEUE.QUEUE_ITEM_ID%TYPE,
    pResult OUT SYS_REFCURSOR
    );

  PROCEDURE P$UPDATE(
    pId IN EXPORT_EXCEL_QUEUE.QUEUE_ITEM_ID%TYPE,
    pState IN NUMBER,
    pRowCount IN EXPORT_EXCEL_SUCCESS.ROWS_QUANTITY%TYPE,
    pFileName IN EXPORT_EXCEL_SUCCESS.FILENAME%TYPE,
    pErrorMessage IN EXPORT_EXCEL_FAILURE.ERROR_DESCRIPTION%TYPE
    );

  PROCEDURE LOG_INFO(pProcName IN VARCHAR2, pMessage IN VARCHAR2);
  PROCEDURE LOG_ERROR(pProcName IN VARCHAR2, pMessage IN VARCHAR2);
END;
/

CREATE OR REPLACE PACKAGE BODY NDS2_MRR_USER.PAC$EXPORT_EXCEL IS

  PROCEDURE LOG_INFO(pProcName IN VARCHAR2, pMessage IN VARCHAR2)
  AS
  BEGIN
    NDS2$SYS.LOG_INFO('PAC$EXPORT_EXCEL', pProcName, 'INFO', pMessage);
    exception when others then null;
  END;
  
  PROCEDURE LOG_ERROR(pProcName IN VARCHAR2, pMessage IN VARCHAR2)
  AS
  BEGIN
    NDS2$SYS.LOG_ERROR('PAC$EXPORT_EXCEL', pProcName, 'ERROR', pMessage);
    exception when others then null;
  END;
  
  PROCEDURE P$GET_EXPORT_PATH(pResult OUT SYS_REFCURSOR)
  AS
	PRAGMA AUTONOMOUS_TRANSACTION;
  BEGIN
    open pResult for
    select cfg.value from configuration cfg
    where cfg.parameter = 'ExportExcelPath';
  END;
  
  PROCEDURE P$GET_EXPORT_THRESHOLD(pResult OUT SYS_REFCURSOR)
  AS
	PRAGMA AUTONOMOUS_TRANSACTION;
  BEGIN
    open pResult for
    select cfg.value from configuration cfg
    where cfg.parameter = 'ExportExcelThreshold';
  END;
    
  PROCEDURE P$ENQUEUE_SELECTION_DISCREP(
    pId OUT EXPORT_EXCEL_QUEUE.QUEUE_ITEM_ID%TYPE,
    pUserSid IN EXPORT_EXCEL_QUEUE.USER_SID%TYPE,
    pSelectionId IN SELECTION.ID%TYPE,
    pRowFilter IN EXPORT_SELECTION_DISCREPANCY.ROW_FILTER%TYPE
    )
  AS
	PRAGMA AUTONOMOUS_TRANSACTION;
  BEGIN
    pId := SEQ_EXPORT_EXCEL_QUEUE.NEXTVAL;
    insert into EXPORT_EXCEL_QUEUE t
      (t.QUEUE_ITEM_ID, t.USER_SID, t.ENQUEUED)
    values
      (pId, pUserSid, sysdate);
    insert into EXPORT_SELECTION_DISCREPANCY t
      (t.QUEUE_ITEM_ID, t.SELECTION_ID, t.ROW_FILTER)
    values
      (pId, pSelectionId, pRowFilter);
    commit;
    exception when others then
      rollback;
      LOG_ERROR('P$ENQUEUE_SELECTION_DISCREP', sqlerrm);
  END;

  PROCEDURE P$DEQUEUE(
    pProcessId IN EXPORT_EXCEL_QUEUE.STARTED_BY_ID%TYPE,
    pResult OUT SYS_REFCURSOR
    )
  AS
	PRAGMA AUTONOMOUS_TRANSACTION;
  BEGIN
    for item in (select t.QUEUE_ITEM_ID from EXPORT_EXCEL_QUEUE t where t.STARTED_BY_ID is null and rownum = 1)
    loop
      update EXPORT_EXCEL_QUEUE t set t.started_by_id = pProcessId, t.started = sysdate where t.QUEUE_ITEM_ID = item.QUEUE_ITEM_ID;
      commit;
      P$FIND_BY_ID(item.QUEUE_ITEM_ID, pResult);
    end loop;
    
    exception when others then
      LOG_ERROR('P$DEQUEUE', sqlerrm);
      rollback;
  END;

  PROCEDURE P$FIND_BY_ID(
    pId IN EXPORT_EXCEL_QUEUE.QUEUE_ITEM_ID%TYPE,
    pResult OUT SYS_REFCURSOR
    )
  AS
	PRAGMA AUTONOMOUS_TRANSACTION;
  BEGIN
    open pResult for
      select
        eeq.queue_item_id,
        eeq.user_sid,
        eeq.enqueued,
        eeq.started,
        eeq.started_by_id,
        eeq.completed,
        esd.selection_id,
        esd.row_filter,
        ees.filename,
        eef.error_description,
        ees.rows_quantity
      from EXPORT_EXCEL_QUEUE eeq
      left join EXPORT_SELECTION_DISCREPANCY esd on esd.QUEUE_ITEM_ID = eeq.QUEUE_ITEM_ID
      left join EXPORT_EXCEL_SUCCESS ees on ees.QUEUE_ITEM_ID = eeq.QUEUE_ITEM_ID
      left join EXPORT_EXCEL_FAILURE eef on eef.QUEUE_ITEM_ID = eeq.QUEUE_ITEM_ID
      where eeq.QUEUE_ITEM_ID = pId;
    exception when others then
      LOG_ERROR('P$FIND_BY_ID', sqlerrm);
  END;
  
  PROCEDURE P$UPDATE(
    pId IN EXPORT_EXCEL_QUEUE.QUEUE_ITEM_ID%TYPE,
    pState IN NUMBER,
    pRowCount IN EXPORT_EXCEL_SUCCESS.ROWS_QUANTITY%TYPE,
    pFileName IN EXPORT_EXCEL_SUCCESS.FILENAME%TYPE,
    pErrorMessage IN EXPORT_EXCEL_FAILURE.ERROR_DESCRIPTION%TYPE
    )
  AS
	PRAGMA AUTONOMOUS_TRANSACTION;
  BEGIN
    IF pState = 0 THEN
    BEGIN
      merge into EXPORT_EXCEL_SUCCESS t
      using (select pId as QUEUE_ITEM_ID from DUAL) id on (t.QUEUE_ITEM_ID = id.QUEUE_ITEM_ID)
      when matched then
        update set t.ROWS_QUANTITY = pRowCount, t.FILENAME = pFileName
      when not matched then
        insert (t.QUEUE_ITEM_ID, t.FILENAME, t.ROWS_QUANTITY) values (pId, pFileName, pRowCount);
      if LENGTH(pFileName) > 0 then
        update EXPORT_EXCEL_QUEUE t set t.COMPLETED = sysdate where t.QUEUE_ITEM_ID = pId;
      end if;
    END;
    ELSE
    BEGIN
      merge into EXPORT_EXCEL_FAILURE t
      using (select pId as QUEUE_ITEM_ID from DUAL) id on (t.QUEUE_ITEM_ID = id.QUEUE_ITEM_ID)
      when matched then
        update set t.ERROR_DESCRIPTION = pErrorMessage
      when not matched then
        insert (t.QUEUE_ITEM_ID, t.ERROR_DESCRIPTION) values (pId, pErrorMessage);
      delete from EXPORT_EXCEL_SUCCESS where QUEUE_ITEM_ID = pId;
      update EXPORT_EXCEL_QUEUE t set t.COMPLETED = sysdate where t.QUEUE_ITEM_ID = pId;
    END;
    END IF;
    commit;
    exception when others then
      rollback;
      LOG_ERROR('P$UPDATE', sqlerrm);
  END;
  
END;
/
