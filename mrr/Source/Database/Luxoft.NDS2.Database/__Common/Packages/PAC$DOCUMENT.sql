﻿create or replace PACKAGE   NDS2_MRR_USER.PAC$DOCUMENT
as
Type T$SEOD_DISCREP is record
(
 DISCREPANCY_ID NUMBER
,TYPE NUMBER(2)
,RULE NUMBER(2)
,SELLER_INN VARCHAR2(12 CHAR)
,BUYER_INN VARCHAR2(12 CHAR)
,DECLARATION_VERSION_ID NUMBER
,CHAPTER NUMBER
,ORDINAL_NUMBER NUMBER
,OKV_CODE VARCHAR2(12 CHAR)
,CREATE_DATE DATE
,RECEIVE_DATE DATE
,INVOICE_NUM VARCHAR2(4000 CHAR)
,INVOICE_DATE DATE
,CHANGE_NUM VARCHAR2(1024 CHAR)
,CHANGE_DATE DATE
,CORRECTION_NUM VARCHAR2(4000 CHAR)
,CORRECTION_DATE DATE
,CHANGE_CORRECTION_NUM VARCHAR2(1024 CHAR)
,CHANGE_CORRECTION_DATE DATE
,SELLER_INVOICE_NUM VARCHAR2(4000 CHAR)
,SELLER_INVOICE_DATE DATE
,BROKER_INN VARCHAR2(12 CHAR)
,BROKER_KPP VARCHAR2(10 CHAR)
,DEAL_KIND_CODE NUMBER
,CUSTOMS_DECLARATION_NUM VARCHAR2(4000)
,PRICE_BUY_AMOUNT NUMBER
,PRICE_BUY_NDS_AMOUNT NUMBER
,PRICE_SELL NUMBER
,PRICE_SELL_IN_CURR NUMBER
,PRICE_SELL_18 NUMBER
,PRICE_SELL_10 NUMBER
,PRICE_SELL_0 NUMBER
,PRICE_NDS_18 NUMBER
,PRICE_NDS_10 NUMBER
,PRICE_TAX_FREE NUMBER
,PRICE_TOTAL NUMBER
,PRICE_NDS_TOTAL NUMBER
,DIFF_CORRECT_DECREASE NUMBER
,DIFF_CORRECT_INCREASE NUMBER
,DIFF_CORRECT_NDS_DECREASE NUMBER
,DIFF_CORRECT_NDS_INCREASE NUMBER
,PRICE_NDS_BUYER NUMBER
,ROW_KEY VARCHAR2(512 CHAR)
,ACTUAL_ROW_KEY VARCHAR2(512 CHAR)
,COMPARE_ROW_KEY VARCHAR2(4000 CHAR)
,COMPARE_ALGO_ID NUMBER
,FORMAT_ERRORS VARCHAR2(512 CHAR)
,LOGICAL_ERRORS VARCHAR2(512 CHAR)
,SELLER_AGENCY_INFO_INN VARCHAR2(12 CHAR)
,SELLER_AGENCY_INFO_KPP VARCHAR2(10 CHAR)
,SELLER_AGENCY_INFO_NAME VARCHAR2(2048 CHAR)
,SELLER_AGENCY_INFO_NUM VARCHAR2(4000 CHAR)
,SELLER_AGENCY_INFO_DATE DATE
,IS_IMPORT NUMBER(1)
,CLARIFICATION_KEY VARCHAR2(512 CHAR)
,CONTRAGENT_KEY NUMBER
,IS_DOP_LIST NUMBER(1)
,decl_type NUMBER
,tax_period VARCHAR2(2 CHAR)
,fiscal_year VARCHAR2(4 CHAR)
,buyer_kpp_effective VARCHAR2(9 CHAR)
,seller_kpp_effective VARCHAR2(9 CHAR)
,quarter NUMBER
,amount NUMBER(20,2)
,amount_pvp NUMBER(20,2)
,seller_zip NUMBER
,seller_nd_prizn VARCHAR2(4 CHAR)
,seller_sur CHAR(1 CHAR)
);

TYPE T$DOC_INVOICE_FULL IS RECORD(
DECLARATION_VERSION_ID NUMBER,
INVOICE_CHAPTER NUMBER(2,0),
INVOICE_ROW_KEY VARCHAR2(128 BYTE),
DOC_ID NUMBER(38,0),
ORDINAL_NUMBER NUMBER(19,0),
OKV_CODE VARCHAR2(3 CHAR),
CREATE_DATE DATE,
RECEIVE_DATE DATE,
INVOICE_NUM VARCHAR2(1024 CHAR),
INVOICE_DATE DATE,
CHANGE_NUM VARCHAR2(256 CHAR),
CHANGE_DATE DATE,
CORRECTION_NUM VARCHAR2(1000 CHAR),
CORRECTION_DATE DATE,
CHANGE_CORRECTION_NUM VARCHAR2(256 CHAR),
CHANGE_CORRECTION_DATE DATE,
SELLER_INVOICE_NUM VARCHAR2(2048 CHAR),
SELLER_INVOICE_DATE DATE,
BROKER_INN VARCHAR2(12 CHAR),
BROKER_KPP VARCHAR2(9 CHAR),
DEAL_KIND_CODE NUMBER,
CUSTOMS_DECLARATION_NUM VARCHAR2(1000 CHAR),
PRICE_BUY_AMOUNT NUMBER(22,2),
PRICE_BUY_NDS_AMOUNT NUMBER(22,2),
PRICE_SELL NUMBER(22,2),
PRICE_SELL_IN_CURR NUMBER(22,2),
PRICE_SELL_18 NUMBER(22,2),
PRICE_SELL_10 NUMBER(22,2),
PRICE_SELL_0 NUMBER(22,2),
PRICE_NDS_18 NUMBER(22,2),
PRICE_NDS_10 NUMBER(22,2),
PRICE_TAX_FREE NUMBER(22,2),
PRICE_TOTAL NUMBER(22,2),
PRICE_NDS_TOTAL NUMBER(22,2),
DIFF_CORRECT_DECREASE NUMBER(22,2),
DIFF_CORRECT_INCREASE NUMBER(22,2),
DIFF_CORRECT_NDS_DECREASE NUMBER(22,2),
DIFF_CORRECT_NDS_INCREASE NUMBER(22,2),
PRICE_NDS_BUYER NUMBER(22,2),
ACTUAL_ROW_KEY VARCHAR2(128 CHAR),
COMPARE_ROW_KEY VARCHAR2(2048 CHAR),
COMPARE_ALGO_ID NUMBER(2,0),
FORMAT_ERRORS VARCHAR2(128 CHAR),
LOGICAL_ERRORS VARCHAR2(128 CHAR),
SELLER_AGENCY_INFO_INN VARCHAR2(12 CHAR),
SELLER_AGENCY_INFO_KPP VARCHAR2(9 CHAR),
SELLER_AGENCY_INFO_NAME VARCHAR2(512 CHAR),
SELLER_AGENCY_INFO_NUM VARCHAR2(1000 CHAR),
SELLER_AGENCY_INFO_DATE DATE,
IS_IMPORT NUMBER(1,0),
CLARIFICATION_KEY VARCHAR2(128 CHAR),
CONTRAGENT_KEY NUMBER,
IS_DOP_LIST NUMBER,
DECL_TAX_PERIOD VARCHAR2(2 CHAR),
DECL_FISCAL_YEAR VARCHAR2(4 CHAR)
);

  /*########################*/
  /*#Создание АТ по СФ     #*/
  /*########################*/
  PROCEDURE DEMO;

  /*Формирование очереди АТ по СФ*/
  PROCEDURE COLLECT_QUEUE;
  /*Обработка очереди АТ по СФ*/
  PROCEDURE PROCESS_QUEUE;

  /*########################*/
  /*#Закрытие АТ по СФ     #*/
  /*########################*/
  PROCEDURE P$AUTO_CLOSE;

  /*По получении новой корректировки*/
  PROCEDURE CLOSE_DOC_BY_NEW_CORR;
  /*По истечении периода*/
  PROCEDURE CLOSE_DOC_BY_TIMEOUT;

  /*########################*/
  /*#Создание АТ по КС     #*/
  /*########################*/
  PROCEDURE SEND_CLAIM_KS;

  /*вызов из дженкинса*/
  procedure P$UPDATE_CONTROL_RATIO_QUEUE;

  /*########################*/
  /*#Получение данных по АТ#*/
  /*########################*/
  PROCEDURE GET_DOC_INFO (pDocId IN DOC.DOC_ID%type, pRezCursor OUT SYS_REFCURSOR);

  /* Заполнение invoice для АТ / АИ */
  PROCEDURE FILL_DOC_INVOICE
  (
     cf in T$DOC_INVOICE_FULL
  );

  /*#########################################*/
  /*#Получение списка АТ/АИ по корректировке#*/
  /*#########################################*/
  PROCEDURE P$GET_CLAIMS_BY_DECLARATION
  (
  pInnDeclarant in varchar2,
  pInnContractor in varchar2,
  pKppEffective in varchar2,
  pPeriodEffective in number,
  pYear in varchar2,
  pCursor out SYS_REFCURSOR
 );
END;
/
CREATE OR REPLACE PACKAGE BODY NDS2_MRR_USER.PAC$DOCUMENT
as
    TYPE T_LIST_STRING is TABLE OF VARCHAR2(2048 char);

  DATE_FORMAT CONSTANT varchar2(12)             := 'DD.MM.YYYY';
  DECIMAL_FORMAT CONSTANT varchar2(25 char)     := '9999999999999999990.00';

  XSD_NDS2_CAM_01 CONSTANT varchar2(32 char)    := 'TAX3EXCH_NDS2_CAM_01_03.xsd';
  XSD_NDS2_CAM_02 CONSTANT varchar2(32 char)    := 'TAX3EXCH_NDS2_CAM_02_01.xsd';
  XSD_NDS2_CAM_03 CONSTANT varchar2(32 char)    := 'TAX3EXCH_NDS2_CAM_03_01.xsd';
  XSD_NDS2_CAM_04 CONSTANT varchar2(32 char)    := 'TAX3EXCH_NDS2_CAM_04_01.xsd';

  CLAIM_INVOICE_TYPE       CONSTANT number    := 1;
  RECLAIM_TYPE             CONSTANT number    := 2;
  CLAIM_CONTROL_RATIO_TYPE CONSTANT number    := 5;

  LIMIT_AUTOCLOSE_PARAM_NAME CONSTANT varchar(25) := 'at_autoclose_items_limit';


procedure UTL_TRACE(p_msg varchar2)
as
pragma autonomous_transaction;
begin
--  dbms_output.put_line(p_msg);
/*  insert into system_log(id, type, site, entity_id, message_code, message, log_date)
  values(seq_sys_log_id.nextval, 0, 0, 0, 0, p_msg, sysdate);
  commit;
exception when others then
  rollback;*/
  null;
end;

procedure UTL_LOG
  (
    v_source varchar2,
    v_code varchar2 := 0,
    v_msg varchar2 := null,
    v_entity_id varchar2 := null
  )
as
pragma autonomous_transaction;
begin
  insert into system_log(id, type, site, entity_id, message_code, message, log_date)
  values(seq_sys_log_id.nextval, 0, v_source, v_entity_id, v_code, v_msg, sysdate);
  commit;
exception when others then
  rollback;
end;

FUNCTION UTL_FORMAT_DECIMAL(v_val in number)
return varchar2
is
begin
  return trim(to_char(v_val, DECIMAL_FORMAT));
end;

FUNCTION UTL_FORMAT_DATE(v_val in DATE)
return varchar2
is
begin
  return to_char(v_val, DATE_FORMAT);
end;

PROCEDURE UTL_REMOVE_IF_EMPTY(p_parent in xmldom.DOMNode, p_node in xmldom.DOMNode)
as
d_node xmldom.DOMNode;
begin
   if dbms_xmldom.getLength(dbms_xmldom.getChildNodes(p_node)) < 1 then
     d_node := dbms_xmldom.removeChild(p_parent, p_node);
     xmldom.freeNode(d_node);
   end if;
end;

PROCEDURE UTL_GET_TAXPERIOD_BOUNDS
(
  p_period in varchar2,
  p_year in varchar2,
  p_start_date out varchar2,
  p_end_date out varchar2
)
as
begin
  p_start_date := null;
  p_end_date   := null;
  if p_period = '21' then
    p_start_date := '01.01.' || p_year;
    p_end_date   := '31.03.' || p_year;
  else
    if p_period = '22' then
      p_start_date := '01.04.' || p_year;
      p_end_date   := '30.06.' || p_year;
    else
      if p_period = '23' then
        p_start_date := '01.07.' || p_year;
        p_end_date   := '30.09.' || p_year;
      else
        if p_period = '24' then
          p_start_date := '01.10.' || p_year;
          p_end_date   := '31.12.' || p_year;
        end if;
      end if;
    end if;
  end if;
end;

PROCEDURE UTL_XML_APPEND_ATTRIBUTE
  (
    p_elm in xmldom.DOMElement
   ,p_attrName in varchar2
   ,p_attrValue in varchar2
  )
as
begin
if length(nvl(p_attrValue, '')) > 0 then
  dbms_xmldom.setAttribute(p_elm, p_attrName, p_attrValue);
end if;
end;

PROCEDURE UTL_ADD_LIST_ITEM(p_list in out t_list_string, p_value in varchar2, p_unique_only number := 0)
as
  v_cancel number(1);
begin
  v_cancel := 0;
  if p_unique_only <> 0 then
    for i in 1 .. p_list.count
    loop
      if p_list(i) = p_value then
        v_cancel := 1;
        exit;
      end if;
    end loop;
  end if;

  if v_cancel = 0 then
    p_list.extend;
    p_list(p_list.count) := p_value;
  end if;
end;

FUNCTION UTL_COMMA_TO_LIST
(
  p_input_string in varchar2,
  p_remove_empty_entries in number := 1,
  p_pattern in varchar2 := '[^,]+'
)
return t_list_string
as
  v_result t_list_string;
begin
  v_result := t_list_string();

  for v_row in
  (
    select val
    from
    (
      select trim(regexp_substr(p_input_string, p_pattern, 1, level)) as val
      from dual
      connect by regexp_substr(p_input_string, p_pattern, 1, level) is not null
    ) t
    where p_remove_empty_entries = 0 or nvl(length(val), 0) > 0
  )
  loop
    v_result.extend;
    v_result(v_result.count) := v_row.val;
  end loop;

  return v_result;
end;

FUNCTION UTL_LIST_TO_COMMA(p_list in t_list_string, p_separator in varchar2 := ', ') return varchar2
as
  v_result varchar2(2048 char);
begin
  v_result := '';
  if p_list.count > 0 then
    for i in p_list.first .. p_list.last
    loop
    v_result := v_result || p_list(i) || p_separator;
    end loop;
  end if;

  if length(v_result) > length(p_separator) then
    return substr(v_result, 1, length(v_result) - length(p_separator));
  else
    return null;
  end if;
end;


/* Автозакрытие АТ */
PROCEDURE P$AUTO_CLOSE
as
	v_error_source constant varchar2(30):='PAC$RECLAIM.P$AUTO_CLOSE';
	v_error_msg varchar2(700);
begin
    -- 1. закрытие АТ при подаче корректировки в новый НО
	begin
		CLOSE_DOC_BY_NEW_CORR;

		exception when others then
			v_error_msg:=substr(sqlerrm,1,700);
			UTL_LOG(
				v_source => v_error_source,
				v_msg  =>'Ошибка автозакрытия АТ при подаче корректировки в новый НО ' ||v_error_msg
			  );
	end;

	-- 2. закрытие АТ по истечению срока дедлайна
	begin
		CLOSE_DOC_BY_TIMEOUT;

		exception when others then
			v_error_msg:=substr(sqlerrm,1,700);
			UTL_LOG(
				v_source => v_error_source,
				v_msg  =>'Ошибка автозакрытия АТ по истечению срока дедлайна ' ||v_error_msg
			  );
	end;

	if(v_error_msg is not null) then
		raise_application_error(-20100,v_error_source||' '||v_error_msg);
	end if;
end;

PROCEDURE FILL_DOC_INVOICE(
 cf in T$DOC_INVOICE_FULL
)
as
  V_GAP_AMOUNT doc_invoice.GAP_AMOUNT%TYPE;
  V_NDS_AMOUNT doc_invoice.NDS_AMOUNT%TYPE;
begin
  
  select
        sum(case when sd.TYPE = 1 then sd.AMNT else 0 end),
        sum(case when sd.TYPE = 4 then sd.AMNT else 0 end)
    into
        V_GAP_AMOUNT,
        V_NDS_AMOUNT
    from DUAL
        left join doc_discrepancy dd on dd.doc_id = cf.doc_id
            and dd.row_key in (cf.invoice_row_key, cf.actual_row_key)
        left join sov_discrepancy sd on dd.discrepancy_id = sd.id;

  insert into doc_invoice (
              declaration_version_id
             ,invoice_chapter
             ,invoice_row_key
             ,doc_id
             ,ordinal_number
             ,okv_code
             ,create_date
             ,receive_date
             ,invoice_num
             ,invoice_date
             ,change_num
             ,change_date
             ,correction_num
             ,correction_date
             ,change_correction_num
             ,change_correction_date
             ,seller_invoice_num
             ,seller_invoice_date
             ,broker_inn
             ,broker_kpp
             ,deal_kind_code
             ,customs_declaration_num
             ,price_buy_amount
             ,price_buy_nds_amount
             ,price_sell
             ,price_sell_in_curr
             ,price_sell_18
             ,price_sell_10
             ,price_sell_0
             ,price_nds_18
             ,price_nds_10
             ,price_tax_free
             ,price_total
             ,price_nds_total
             ,diff_correct_decrease
             ,diff_correct_increase
             ,diff_correct_nds_decrease
             ,diff_correct_nds_increase
             ,price_nds_buyer
             ,actual_row_key
             ,compare_row_key
             ,compare_algo_id
             ,format_errors
             ,logical_errors
             ,seller_agency_info_inn
             ,seller_agency_info_kpp
             ,seller_agency_info_name
             ,seller_agency_info_num
             ,seller_agency_info_date
             ,is_import
             ,clarification_key
             ,contragent_key
             ,is_dop_list
             ,decl_tax_period
             ,decl_fiscal_year
			 ,GAP_AMOUNT
			 ,NDS_AMOUNT
             )
      values (
              cf.declaration_version_id
             ,cf.invoice_chapter
             ,cf.invoice_row_key
             ,cf.doc_id
             ,cf.ordinal_number
             ,cf.okv_code
             ,cf.create_date
             ,cf.receive_date
             ,cf.invoice_num
             ,cf.invoice_date
             ,cf.change_num
             ,cf.change_date
             ,cf.correction_num
             ,cf.correction_date
             ,cf.change_correction_num
             ,cf.change_correction_date
             ,cf.seller_invoice_num
             ,cf.seller_invoice_date
             ,cf.broker_inn
             ,cf.broker_kpp
             ,cf.deal_kind_code
             ,cf.customs_declaration_num
             ,cf.price_buy_amount
             ,cf.price_buy_nds_amount
             ,cf.price_sell
             ,cf.price_sell_in_curr
             ,cf.price_sell_18
             ,cf.price_sell_10
             ,cf.price_sell_0
             ,cf.price_nds_18
             ,cf.price_nds_10
             ,cf.price_tax_free
             ,cf.price_total
             ,cf.price_nds_total
             ,cf.diff_correct_decrease
             ,cf.diff_correct_increase
             ,cf.diff_correct_nds_decrease
             ,cf.diff_correct_nds_increase
             ,cf.price_nds_buyer
             ,cf.actual_row_key
             ,cf.compare_row_key
             ,cf.compare_algo_id
             ,cf.format_errors
             ,cf.logical_errors
             ,cf.seller_agency_info_inn
             ,cf.seller_agency_info_kpp
             ,cf.seller_agency_info_name
             ,cf.seller_agency_info_num
             ,cf.seller_agency_info_date
             ,cf.is_import
             ,cf.clarification_key
             ,cf.contragent_key
             ,cf.is_dop_list
             ,cf.decl_tax_period
             ,cf.decl_fiscal_year
			 ,V_GAP_AMOUNT
			 ,V_NDS_AMOUNT
             );
             
     if cf.invoice_chapter = 8 and cf.invoice_date is not null then
       merge into DOC_INVOICE_PERIODS trg
       using (select cf.doc_id as doc_id
                    ,ceil(extract (month from cf.invoice_date)/3) as quarter
                    ,extract (year from cf.invoice_date) as fiscal_year
              from dual
			   ) src on (src.doc_id = trg.doc_id and src.fiscal_year = trg.fiscal_year and src.quarter = trg.quarter)
       when not matched then
         insert (doc_id, quarter, fiscal_year) values (src.doc_id, src.quarter, src.fiscal_year);
     end if;

     insert into DOC_INVOICE_BUYER (ROW_KEY_ID,BUYER_INN,BUYER_KPP,DOC_ID)
            select distinct t.row_key_id, t.buyer_inn, t.buyer_kpp, cf.doc_id
            from SOV_INVOICE_BUYER_ALL t
            where t.row_key_id = cf.invoice_row_key;


     insert into DOC_INVOICE_SELLER (ROW_KEY_ID,INVOICE_NUM,SELLER_INN,SELLER_KPP,DOC_ID)
            select distinct t.row_key_id, t.invoice_num, t.seller_inn, t.seller_kpp, cf.doc_id
            from SOV_INVOICE_SELLER_ALL t
            where t.row_key_id = cf.invoice_row_key;


     insert into DOC_INVOICE_BUY_ACCEPT (ROW_KEY_ID,BUY_ACCEPT_DATE,DOC_ID)
            select distinct t.row_key_id, t.buy_accept_date, cf.doc_id
            from SOV_INVOICE_BUY_ACCEPT_All t
            where t.row_key_id = cf.invoice_row_key;

      insert into DOC_INVOICE_OPERATION (ROW_KEY_ID,OPERATION_CODE,DOC_ID)
             select distinct t.row_key_id, t.operation_code, cf.doc_id
             from SOV_INVOICE_OPERATION_ALL t
             where t.row_key_id = cf.invoice_row_key;

      insert into DOC_INVOICE_PAYMENT_DOC (ROW_KEY_ID,DOC_NUM,DOC_DATE,DOC_ID)
             select distinct t.row_key_id, t.doc_num, t.doc_date, cf.doc_id
             from SOV_INVOICE_PAYMENT_DOC_ALL t
             where t.row_key_id = cf.invoice_row_key;
end;


PROCEDURE P$CLOSE_DOCS( p_doc_ids T$TABLE_OF_NUMBER, 
                            p_close_date date,
                            p_status_closed number,
                            p_reason_closed number)
as
begin
    --Обновляем статус и причину закрытия АТ
    FORALL indx IN 1 .. p_doc_ids.COUNT
        update doc
        set
           close_date = p_close_date,
           status = p_status_closed,
		   status_date = trunc(p_close_date),
           close_reason = p_reason_closed
        where doc_id = p_doc_ids(indx);

    --Добавляем записи в историю и перечень закрытых АТ
    FORALL indx IN 1 .. p_doc_ids.COUNT
      insert all
        into doc_status_history(doc_id, status, status_date)
            values (p_doc_ids(indx), p_status_closed, p_close_date)
        into claim_closed(doc_id)
            values (p_doc_ids(indx))
        select 1 from dual;
end;



/* Закрывает АТ в случае подачи новой корректировки*/
PROCEDURE CLOSE_DOC_BY_NEW_CORR
as
    close_status_num constant number := 10;
    close_reason_num constant number := 2;
    last_exec_param_name constant varchar2(35):='at_close_by_new_corr_exec_date';
    dt_format constant varchar2(25) := 'DD-MM-YYYY HH24:MI:SS';

    doc_id_col T$TABLE_OF_NUMBER;
    v_last_exec_dt date;
    v_items_limit number;
    v_exec_dt date := sysdate;
begin
    --Инициализируем переменные
    select
        to_date(value, dt_format )
    into v_last_exec_dt
    from configuration
    where parameter = last_exec_param_name;

    select
        cast(value as number )
    into v_items_limit
    from configuration
    where parameter = LIMIT_AUTOCLOSE_PARAM_NAME ;

    --Кладем в коллекцию все АТ по которым есть новые корректировки
    loop
        with
            new_corr_decl as
            (
                select distinct
                    inn_declarant,
                    kpp_effective,
                    inn_contractor,
                    period_code,
                    fiscal_year,
                    type_code
                from declaration_history
                where
                    insert_date between v_last_exec_dt and v_exec_dt
                    and is_active = 1
            )
        select distinct
            d.doc_id
        bulk collect into doc_id_col
        from new_corr_decl ncd
        join declaration_history dh
            on  ncd.inn_declarant = dh.inn_declarant
                and ncd.kpp_effective = dh.kpp_effective
                and ncd.inn_contractor= dh.inn_contractor
                and ncd.period_code = dh.period_code
                and ncd.fiscal_year = dh.fiscal_year
                and ncd.type_code = dh.type_code
        join doc d
            on dh.zip = d.zip
        where
            d.status not in (3,9,10)    -- ошибка отправки/ошибка передачи/закрыто
            and d.doc_type = 1       --АТ
            and rownum <= v_items_limit;

            if(doc_id_col.count = 0) then
                exit;
            end if;

        P$CLOSE_DOCS(p_doc_ids => doc_id_col, 
                        p_close_date => v_exec_dt,
                        p_status_closed => close_status_num,
                        p_reason_closed => close_reason_num);
    end loop;

    --Обновляем дату запуска
    update nds2_mrr_user.configuration
    set value = TO_CHAR(v_exec_dt, dt_format)
    where parameter = last_exec_param_name;

    exception when others then
        UTL_LOG('PAC$DOCUMENT.CLOSE_DOC_BY_NEW_CORR', 3, sqlerrm, null);
        raise;
end;

/*По истечении периода*/
PROCEDURE CLOSE_DOC_BY_TIMEOUT
as
    close_status_num constant number := 10;
    close_reason_num constant number := 1;
    close_dt date :=sysdate;

    doc_id_col T$TABLE_OF_NUMBER;
    v_items_limit number;
begin
    --Инициализируем переменные
    select
        cast(value as number )
    into v_items_limit
    from configuration
    where parameter = LIMIT_AUTOCLOSE_PARAM_NAME;

    loop
        select doc_id
        bulk collect into doc_id_col
        from doc
        where
            status not in (3,9,10)          -- ошибка отправки/ошибка передачи/закрыто
            and doc_type in (1)             -- АТ
            and deadline_date < close_dt
            and rownum <= v_items_limit;

        if(doc_id_col.count = 0) then
            exit;
        end if;

        P$CLOSE_DOCS(p_doc_ids => doc_id_col, 
                        p_close_date => close_dt,
                        p_status_closed => close_status_num,
                        p_reason_closed => close_reason_num);
    end loop;

    exception when others then
        UTL_LOG('PAC$DOCUMENT.CLOSE_DOC_BY_TIMEOUT', 3, sqlerrm, null);
        raise;
end;

PROCEDURE UTL_CREATE_OPER_CODE_NODES
(
  p_doc in xmldom.DOMDocument,
  p_root_node in xmldom.DOMNode,
  p_node_name in varchar2,
  p_row_key in varchar2
)
as
  v_split_element xmldom.DOMElement;
  v_split_node    xmldom.DOMNode;
  v_text_element  xmldom.DOMText;
  v_text_node     xmldom.DOMNode;
  v_op_code_value varchar2(2 char);
begin
  for line in (select * from sov_invoice_operation_all where row_key_id = p_row_key)
  loop
      v_split_element := xmldom.createElement(p_doc, p_node_name);
      v_split_node := xmldom.appendChild(p_root_node, xmldom.makeNode(v_split_element));

      /*костыль, потому как теряется при выгрузке лидирующий ноль*/
      if length(trim(line.operation_code)) = 1 then
        v_op_code_value := '0'||trim(line.operation_code);
      else
        v_op_code_value := line.operation_code;
      end if;

      v_text_element := xmldom.createTextNode(p_doc, v_op_code_value);
      v_text_node := xmldom.appendChild(v_split_node, xmldom.makeNode(v_text_element));
  end loop;
end;

PROCEDURE UTL_CREATE_BUY_ACCEPT_NODES
(
  p_doc in xmldom.DOMDocument,
  p_root_node in xmldom.DOMNode,
  p_node_name in varchar2,
  p_row_key in varchar2
)
as
  v_split_element xmldom.DOMElement;
  v_split_node    xmldom.DOMNode;
  v_text_element  xmldom.DOMText;
  v_text_node     xmldom.DOMNode;
begin
  for line in (select * from sov_invoice_buy_accept_all where row_key_id = p_row_key)
  loop
      v_split_element := xmldom.createElement(p_doc, p_node_name);
      v_split_node := xmldom.appendChild(p_root_node, xmldom.makeNode(v_split_element));
      v_text_element := xmldom.createTextNode(p_doc, UTL_FORMAT_DATE(line.buy_accept_date));
      v_text_node := xmldom.appendChild(v_split_node, xmldom.makeNode(v_text_element));
  end loop;
end;

PROCEDURE UTL_CREATE_SPLITED_NODES
(
  p_doc in xmldom.DOMDocument,
  p_root_node in xmldom.DOMNode,
  p_node_name in varchar2,
  p_input_string in varchar2
)
as
  v_list          t_list_string;
  v_split_element xmldom.DOMElement;
  v_split_node    xmldom.DOMNode;
  v_text_element  xmldom.DOMText;
  v_text_node     xmldom.DOMNode;
begin
  v_list := UTL_COMMA_TO_LIST(p_input_string);
  if v_list.count > 0 then
    for i in v_list.first .. v_list.last
    loop
    v_split_element := xmldom.createElement(p_doc, p_node_name);
    v_split_node := xmldom.appendChild(p_root_node, xmldom.makeNode(v_split_element));
    v_text_element := xmldom.createTextNode(p_doc, v_list(i));
    v_text_node := xmldom.appendChild(v_split_node, xmldom.makeNode(v_text_element));
    end loop;
  end if;
end;

PROCEDURE UTL_CREATE_DOC_NODES
(
  p_doc in xmldom.DOMDocument,
  p_root_node in xmldom.DOMNode,
  p_node_name in varchar2,
  p_doc_num_caption in varchar2,
  p_doc_date_caption in varchar2,
  p_row_key in varchar2
)
as
  v_doc_element xmldom.DOMElement;
  v_doc_node    xmldom.DOMNode;
begin
  for line in (select * from sov_invoice_payment_doc_all where row_key_id = p_row_key)
  loop
    v_doc_element := xmldom.createElement(p_doc, p_node_name);
    UTL_XML_APPEND_ATTRIBUTE(v_doc_element, p_doc_num_caption, line.doc_num);
    UTL_XML_APPEND_ATTRIBUTE(v_doc_element, p_doc_date_caption, UTL_FORMAT_DATE(line.doc_date));
    v_doc_node := xmldom.appendChild(p_root_node, xmldom.makeNode(v_doc_element));
  end loop;

end;

PROCEDURE UTL_CREATE_TAXPAYER_INFO_NODE
(
  p_doc in xmldom.DOMDocument,
  p_root_node in xmldom.DOMNode,
  p_node_name in varchar2,
  p_inn in varchar2
)
as
  v_taxpayer_info_element xmldom.DOMElement;
  v_taxpayer_info_node    xmldom.DOMNode;
  v_taxpayer_element      xmldom.DOMElement;
  v_taxpayer_node         xmldom.DOMNode;
begin
  if nvl(length(p_inn), 0) in (10, 12) then
    v_taxpayer_info_element := xmldom.createElement(p_doc, p_node_name);
    v_taxpayer_info_node := xmldom.appendChild(p_root_node, xmldom.makeNode(v_taxpayer_info_element));

    if length(p_inn) = 10 then
      v_taxpayer_element := xmldom.createElement(p_doc, 'СведЮЛ');
      xmldom.setAttribute(v_taxpayer_element, 'ИННЮЛ', p_inn);
    else
      v_taxpayer_element := xmldom.createElement(p_doc, 'СведИП');
      xmldom.setAttribute(v_taxpayer_element, 'ИННФЛ', p_inn);
    end if;
    v_taxpayer_node := xmldom.appendChild(v_taxpayer_info_node, xmldom.makeNode(v_taxpayer_element));
  end if;
end;

PROCEDURE UTL_CREATE_SELLER_INFO_NODES
(
  p_doc in xmldom.DOMDocument,
  p_root_node in xmldom.DOMNode,
  p_node_name in varchar2,
  p_row_key in varchar2
)
as
begin
  for line in (select * from sov_invoice_seller_all where row_key_id = p_row_key)
  loop
    UTL_CREATE_TAXPAYER_INFO_NODE(p_doc, p_root_node, p_node_name, line.seller_inn);
  end loop;
end;

PROCEDURE UTL_CREATE_BUYER_INFO_NODES
(
  p_doc in xmldom.DOMDocument,
  p_root_node in xmldom.DOMNode,
  p_node_name in varchar2,
  p_row_key in varchar2
)
as
begin
  for line in (select * from sov_invoice_buyer_all where row_key_id = p_row_key)
  loop
    UTL_CREATE_TAXPAYER_INFO_NODE(p_doc, p_root_node, p_node_name, line.buyer_inn);
  end loop;
end;

PROCEDURE UTL_CREATE_ERROR_NODES
(
  p_doc in xmldom.DOMDocument,
  p_root_node in xmldom.DOMNode,
  p_discrep_invoice_row in T$SEOD_DISCREP --sov_invoice_all%rowtype
)
as
  v_result t_list_string;
  v_graph t_list_string;
  v_inn_1 varchar2(12 char);
  v_inn_2 varchar2(12 char);

  v_header_element xmldom.DOMElement;
  v_header_node    xmldom.DOMNode;
  v_graph_element xmldom.DOMElement;
  v_graph_node    xmldom.DOMNode;
  v_text_element  xmldom.DOMText;
  v_text_node     xmldom.DOMNode;
begin
  v_result := t_list_string();
  v_graph := t_list_string();

    if p_discrep_invoice_row.type = 1 then --Разрыв

      v_inn_1 := p_discrep_invoice_row.BUYER_INN;
      v_inn_2 := p_discrep_invoice_row.SELLER_INN;

      if v_inn_1 = v_inn_2 then
        if p_discrep_invoice_row.CHAPTER in (8, 9) then
          UTL_ADD_LIST_ITEM(v_result, '2', 1);
        end if;
      elsif p_discrep_invoice_row.CHAPTER = 8 then
        UTL_ADD_LIST_ITEM(v_result, '1', 1);
      elsif p_discrep_invoice_row.CHAPTER in (10, 11) then
        UTL_ADD_LIST_ITEM(v_result, '3', 1);
      end if;
    else
      for graph_row in
      (
        select graph
        from dict_compare_graphs
        where discrepancy_type = p_discrep_invoice_row.type
          and invoice_chapter = p_discrep_invoice_row.CHAPTER
          and (p_discrep_invoice_row.type <> 2 or rule_code = p_discrep_invoice_row.RULE)
      )
      loop
        UTL_ADD_LIST_ITEM(v_graph, graph_row.graph, 1);
      end loop;
    end if;

  if v_graph.count > 0 then
    UTL_ADD_LIST_ITEM(v_result, '4', 1);
  end if;

  if v_result.count > 0 then
    v_header_element := xmldom.createElement(p_doc, 'СпрКодОш');
    v_header_node    := xmldom.appendChild(p_root_node, xmldom.makeNode(v_header_element));


    UTL_XML_APPEND_ATTRIBUTE(v_header_element, 'Код', v_result(v_result.first));
    if v_result(v_result.first) = '4' then
      for j in v_graph.first .. v_graph.last
      loop
        v_graph_element := xmldom.createElement(p_doc, 'ГрафОш');
        v_graph_node    := xmldom.appendChild(v_header_node, xmldom.makeNode(v_graph_element));
        v_text_element  := xmldom.createTextNode(p_doc, v_graph(j));
        v_text_node     := xmldom.appendChild(v_graph_node, xmldom.makeNode(v_text_element));
      end loop;
    end if;
  end if;
end;

PROCEDURE UTL_CREATE_PERIOD_REFLECTION
(
  p_doc in xmldom.DOMDocument,
  p_root_node in xmldom.DOMNode,
  p_discrep_invoice_row in T$SEOD_DISCREP
)
as
  v_header_element xmldom.DOMElement;
  v_header_node    xmldom.DOMNode;
  v_tax_period_original VARCHAR2(2 CHAR);
  v_tax_period VARCHAR2(2 CHAR);
  v_fiscal_year VARCHAR2(4 CHAR);
begin

  v_tax_period_original := p_discrep_invoice_row.tax_period;
  v_fiscal_year := p_discrep_invoice_row.fiscal_year;

  case v_tax_period_original
     when '51' then v_tax_period := '21';
     when '54' then v_tax_period := '22';
     when '55' then v_tax_period := '23';
     when '56' then v_tax_period := '24';
     when '71' then v_tax_period := '01';
     when '72' then v_tax_period := '02';
     when '73' then v_tax_period := '03';
     when '74' then v_tax_period := '04';
     when '75' then v_tax_period := '05';
     when '76' then v_tax_period := '06';
     when '77' then v_tax_period := '07';
     when '78' then v_tax_period := '08';
     when '79' then v_tax_period := '09';
     when '80' then v_tax_period := '10';
     when '81' then v_tax_period := '11';
     when '82' then v_tax_period := '12';
     else v_tax_period := v_tax_period_original;
  end case;

  v_header_element := xmldom.createElement(p_doc, 'ПериодОтрЗап');
  v_header_node    := xmldom.appendChild(p_root_node, xmldom.makeNode(v_header_element));
  UTL_XML_APPEND_ATTRIBUTE(v_header_element, 'Период', v_tax_period);
  UTL_XML_APPEND_ATTRIBUTE(v_header_element, 'Год', v_fiscal_year);
end;

/* ############################ */
/* # Формирование строки 1.1 ## */
/* ############################ */
FUNCTION CLAIM_BUILD_T1_1
(
   p_doc in xmldom.DOMDocument,
   p_container_node in xmldom.DOMNode,
   p_invoice_row in T$SEOD_DISCREP --sov_invoice_all%rowtype
)
return boolean
as
  v_itemElement xmldom.DOMElement;
  v_itemNode    xmldom.DOMNode;
begin
  /*Trace*/
  UTL_TRACE('dis: '||p_invoice_row.DISCREPANCY_ID||' rule: '||p_invoice_row.RULE||' ch:'||p_invoice_row.CHAPTER);

  /* Узел строки             */
  v_itemElement := xmldom.createElement(p_doc, 'T1.1Стр');
  v_itemNode := xmldom.appendChild(p_container_node, xmldom.makeNode(v_itemElement));

  /* Обязательные атрибуты   */
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомерПор',        p_invoice_row.ordinal_number);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомСчФПрод',      p_invoice_row.invoice_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаСчФПрод',     UTL_FORMAT_DATE(p_invoice_row.invoice_date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СтоимПокупВ',     UTL_FORMAT_DECIMAL(p_invoice_row.price_buy_amount));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СумНДСВыч',       UTL_FORMAT_DECIMAL(p_invoice_row.price_buy_nds_amount));

  /* Необязательные атрибуты */
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомИспрСчФ',      p_invoice_row.change_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаИспрСчФ',     UTL_FORMAT_DATE(p_invoice_row.change_date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомКСчФПрод',     p_invoice_row.correction_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаКСчФПрод',    UTL_FORMAT_DATE(p_invoice_row.correction_date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомИспрКСчФ',     p_invoice_row.change_correction_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаИспрКСчФ',    UTL_FORMAT_DATE(p_invoice_row.change_correction_date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомТД',           p_invoice_row.customs_declaration_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ОКВ',             p_invoice_row.okv_code);

  /* Элементы                */
  UTL_CREATE_ERROR_NODES(p_doc, v_itemNode, p_invoice_row);
  UTL_CREATE_PERIOD_REFLECTION(p_doc, v_itemNode, p_invoice_row);
  UTL_CREATE_SELLER_INFO_NODES(p_doc, v_itemNode, 'СвПрод', p_invoice_row.ROW_KEY);
  UTL_CREATE_TAXPAYER_INFO_NODE(p_doc, v_itemNode, 'СвПос',  p_invoice_row.BROKER_INN);
  UTL_CREATE_DOC_NODES(p_doc, v_itemNode, 'ДокПдтвУпл', 'НомДокПдтвУпл', 'ДатаДокПдтвУпл', p_invoice_row.ROW_KEY);
  UTL_CREATE_OPER_CODE_NODES(p_doc, v_itemNode, 'КодВидОпер', p_invoice_row.ROW_KEY);
  UTL_CREATE_BUY_ACCEPT_NODES(p_doc, v_itemNode, 'ДатаУчТов',  p_invoice_row.ROW_KEY);

  return true;
  exception when others then
    /*log error*/
    UTL_LOG('CLAIM_BUILD_T1_1', sqlcode, substr(sqlerrm, 0, 256));
    return false;
end;

/* ############################ */
/* # Формирование строки 1.2 ## */
/* ############################ */
FUNCTION CLAIM_BUILD_T1_2
(
   p_doc in xmldom.DOMDocument,
   p_container_node in xmldom.DOMNode,
   p_invoice_row in T$SEOD_DISCREP --sov_invoice_all%rowtype
)
return boolean
as
  v_itemElement xmldom.DOMElement;
  v_itemNode xmldom.DOMNode;
begin
  /* Узел строки             */
  v_itemElement := xmldom.createElement(p_doc, 'T1.2Стр');
  v_itemNode := xmldom.appendChild(p_container_node, xmldom.makeNode(v_itemElement));

  /* Обязательные атрибуты   */
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомерПор',       p_invoice_row.ordinal_number);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомСчФПрод',     p_invoice_row.invoice_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаСчФПрод',    UTL_FORMAT_DATE(p_invoice_row.invoice_date));

  /* Необязательные атрибуты */
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомИспрСчФ',     p_invoice_row.change_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаИспрСчФ',    UTL_FORMAT_DATE(p_invoice_row.change_date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомКСчФПрод',    p_invoice_row.correction_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаКСчФПрод',   UTL_FORMAT_DATE(p_invoice_row.correction_date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомИспрКСчФ',    p_invoice_row.change_correction_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаИспрКСчФ',   UTL_FORMAT_DATE(p_invoice_row.change_correction_date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ОКВ',            p_invoice_row.okv_code);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СтоимПродСФВ',   UTL_FORMAT_DECIMAL(p_invoice_row.price_sell_in_curr));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СтоимПродСФ18',  UTL_FORMAT_DECIMAL(p_invoice_row.price_sell_18));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СтоимПродСФ10',  UTL_FORMAT_DECIMAL(p_invoice_row.price_sell_10));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СтоимПродСФ0',   UTL_FORMAT_DECIMAL(p_invoice_row.price_sell_0));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СумНДССФ18',     UTL_FORMAT_DECIMAL(p_invoice_row.price_nds_18));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СумНДССФ10',     UTL_FORMAT_DECIMAL(p_invoice_row.price_nds_10));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СтоимПродОсв',   UTL_FORMAT_DECIMAL(p_invoice_row.price_tax_free));

  /* Элементы                */
  UTL_CREATE_ERROR_NODES(p_doc, v_itemNode, p_invoice_row);
  UTL_CREATE_PERIOD_REFLECTION(p_doc, v_itemNode, p_invoice_row);
  UTL_CREATE_BUYER_INFO_NODES(p_doc, v_itemNode, 'СвПокуп', p_invoice_row.ROW_KEY);
  UTL_CREATE_TAXPAYER_INFO_NODE(p_doc, v_itemNode, 'СвПос', p_invoice_row.broker_inn);
  UTL_CREATE_DOC_NODES(p_doc, v_itemNode, 'ДокПдтвОпл', 'НомДокПдтвОпл', 'ДатаДокПдтвОпл', p_invoice_row.ROW_KEY);
  UTL_CREATE_OPER_CODE_NODES(p_doc, v_itemNode, 'КодВидОпер', p_invoice_row.ROW_KEY);

  return true;
  exception when others then
    /*log error*/
    UTL_LOG('CLAIM_BUILD_T1_2', sqlcode, substr(sqlerrm, 0, 256));
    return false;
end;

/* ############################ */
/* # Формирование строки 1.3 ## */
/* ############################ */
FUNCTION CLAIM_BUILD_T1_3
(
   p_doc in xmldom.DOMDocument,
   p_container_node in xmldom.DOMNode,
   p_invoice_row in T$SEOD_DISCREP--sov_invoice_all%rowtype
)
return boolean
as
  v_itemElement xmldom.DOMElement;
  v_itemNode xmldom.DOMNode;

  v_broker_activity_element xmldom.DOMElement;
  v_broker_activity_node xmldom.DOMNode;

  v_page_broker_activity_element xmldom.DOMElement;
  v_page_broker_activity_node xmldom.DOMNode;
begin
  /* Узел строки             */
  v_itemElement := xmldom.createElement(p_doc, 'T1.3Стр');
  v_itemNode := xmldom.appendChild(p_container_node, xmldom.makeNode(v_itemElement));

  /* Обязательные атрибуты   */
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомерПор',       p_invoice_row.ordinal_number);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомСчФПрод',     p_invoice_row.invoice_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаСчФПрод',    UTL_FORMAT_DATE(p_invoice_row.invoice_date));

  /* Необязательные атрибуты */
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомИспрСчФ',     p_invoice_row.change_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаИспрСчФ',    UTL_FORMAT_DATE(p_invoice_row.change_date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомКСчФПрод',    p_invoice_row.correction_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаКСчФПрод',   UTL_FORMAT_DATE(p_invoice_row.correction_date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомИспрКСчФ',    p_invoice_row.change_correction_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаИспрКСчФ',   UTL_FORMAT_DATE(p_invoice_row.change_correction_date));

  /* Элементы                */
  UTL_CREATE_ERROR_NODES(p_doc, v_itemNode, p_invoice_row);
  UTL_CREATE_PERIOD_REFLECTION(p_doc, v_itemNode, p_invoice_row);
  UTL_CREATE_BUYER_INFO_NODES(p_doc, v_itemNode, 'СвПокуп', p_invoice_row.ROW_KEY);

  -- Заполнение СвПосрДеят (один элемент)
  v_broker_activity_element := xmldom.createElement(p_doc, 'СвПосрДеят');
  v_broker_activity_node := xmldom.appendChild(v_itemNode, xmldom.makeNode(v_broker_activity_element));
  v_page_broker_activity_element := xmldom.createElement(p_doc, 'СвПосрДеятСтр');
  v_page_broker_activity_node := xmldom.appendChild(v_broker_activity_node, xmldom.makeNode(v_page_broker_activity_element));

  UTL_XML_APPEND_ATTRIBUTE(v_page_broker_activity_element, 'НомСчФОтПрод',  p_invoice_row.SELLER_AGENCY_INFO_NUM);
  UTL_XML_APPEND_ATTRIBUTE(v_page_broker_activity_element, 'ДатаСчФОтПрод', UTL_FORMAT_DATE(p_invoice_row.SELLER_AGENCY_INFO_DATE));
  UTL_XML_APPEND_ATTRIBUTE(v_page_broker_activity_element, 'ОКВ',           p_invoice_row.okv_code);
  UTL_XML_APPEND_ATTRIBUTE(v_page_broker_activity_element, 'СтоимТовСчФВс', UTL_FORMAT_DECIMAL(p_invoice_row.price_total));
  UTL_XML_APPEND_ATTRIBUTE(v_page_broker_activity_element, 'СумНДССчФ',     UTL_FORMAT_DECIMAL(p_invoice_row.price_nds_total));
  UTL_XML_APPEND_ATTRIBUTE(v_page_broker_activity_element, 'РазСтКСчФУм',   UTL_FORMAT_DECIMAL(p_invoice_row.diff_correct_decrease));
  UTL_XML_APPEND_ATTRIBUTE(v_page_broker_activity_element, 'РазСтКСчФУв',   UTL_FORMAT_DECIMAL(p_invoice_row.diff_correct_increase));
  UTL_XML_APPEND_ATTRIBUTE(v_page_broker_activity_element, 'РазНДСКСчФУм',  UTL_FORMAT_DECIMAL(p_invoice_row.diff_correct_nds_decrease));
  UTL_XML_APPEND_ATTRIBUTE(v_page_broker_activity_element, 'РазНДСКСчФУв',  UTL_FORMAT_DECIMAL(p_invoice_row.diff_correct_nds_increase));

  UTL_CREATE_TAXPAYER_INFO_NODE(p_doc, v_page_broker_activity_node, 'СвПрод', p_invoice_row.SELLER_AGENCY_INFO_INN);
  UTL_CREATE_OPER_CODE_NODES(p_doc, v_itemNode, 'КодВидОпер', p_invoice_row.ROW_KEY);
  return true;
  exception when others then
    /*log error*/
    UTL_LOG('CLAIM_BUILD_T1_3', sqlcode, substr(sqlerrm, 0, 256));
    return false;
end;

/* ############################ */
/* # Формирование строки 1.4 ## */
/* ############################ */
FUNCTION CLAIM_BUILD_T1_4
(
   p_doc in xmldom.DOMDocument,
   p_container_node in xmldom.DOMNode,
   p_invoice_row in T$SEOD_DISCREP--sov_invoice_all%rowtype
)
return boolean
as
  v_itemElement xmldom.DOMElement;
  v_itemNode xmldom.DOMNode;
begin
  /* Узел строки             */
  v_itemElement := xmldom.createElement(p_doc, 'T1.4Стр');
  v_itemNode := xmldom.appendChild(p_container_node, xmldom.makeNode(v_itemElement));
  /* Обязательные атрибуты   */
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомерПор',       p_invoice_row.ordinal_number);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомСчФПрод',     p_invoice_row.invoice_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаСчФПрод',    UTL_FORMAT_DATE(p_invoice_row.invoice_date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'КодВидСд',       p_invoice_row.deal_kind_code);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СтоимТовСчФВс',  UTL_FORMAT_DECIMAL(p_invoice_row.price_total));

  /* Необязательные атрибуты */
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомИспрСчФ',     p_invoice_row.change_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаИспрСчФ',    UTL_FORMAT_DATE(p_invoice_row.change_date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомКСчФПрод',    p_invoice_row.correction_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаКСчФПрод',   UTL_FORMAT_DATE(p_invoice_row.correction_date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомИспрКСчФ',    p_invoice_row.change_correction_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаИспрКСчФ',   UTL_FORMAT_DATE(p_invoice_row.change_correction_date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ОКВ',            p_invoice_row.okv_code);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СумНДССчФ',      UTL_FORMAT_DECIMAL(p_invoice_row.price_nds_total));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'РазСтКСчФУм',    UTL_FORMAT_DECIMAL(p_invoice_row.diff_correct_decrease));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'РазСтКСчФУв',    UTL_FORMAT_DECIMAL(p_invoice_row.diff_correct_increase));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'РазНДСКСчФУм',   UTL_FORMAT_DECIMAL(p_invoice_row.diff_correct_nds_decrease));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'РазНДСКСчФУв',   UTL_FORMAT_DECIMAL(p_invoice_row.diff_correct_nds_increase));

  /* Элементы                */
  UTL_CREATE_ERROR_NODES(p_doc, v_itemNode, p_invoice_row);
  UTL_CREATE_PERIOD_REFLECTION(p_doc, v_itemNode, p_invoice_row);
  UTL_CREATE_SELLER_INFO_NODES(p_doc, v_itemNode, 'СвПрод', p_invoice_row.ROW_KEY);
  UTL_CREATE_TAXPAYER_INFO_NODE(p_doc, v_itemNode, 'СвСубком', p_invoice_row.broker_inn);
  UTL_CREATE_OPER_CODE_NODES(p_doc, v_itemNode, 'КодВидОпер', p_invoice_row.ROW_KEY);

  return true;
  exception when others then
    /*log error*/
    UTL_LOG('CLAIM_BUILD_T1_4', sqlcode, substr(sqlerrm, 0, 256));
    return false;
end;

/* ############################ */
/* # Формирование строки 1.5 ## */
/* ############################ */
FUNCTION CLAIM_BUILD_T1_5
(
   p_doc in xmldom.DOMDocument,
   p_container_node in xmldom.DOMNode,
   p_invoice_row in T$SEOD_DISCREP--sov_invoice_all%rowtype
)
return boolean
as
  v_itemElement xmldom.DOMElement;
  v_itemNode xmldom.DOMNode;
begin
  /* Узел строки             */
  v_itemElement := xmldom.createElement(p_doc, 'T1.5Стр');
  v_itemNode := xmldom.appendChild(p_container_node, xmldom.makeNode(v_itemElement));

  /* Обязательные атрибуты   */
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомСчФ',         p_invoice_row.invoice_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаСчФ',        UTL_FORMAT_DATE(p_invoice_row.invoice_date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СтоимТовБНалВс', UTL_FORMAT_DECIMAL(p_invoice_row.price_total));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СумНалПокуп',    UTL_FORMAT_DECIMAL(p_invoice_row.price_nds_buyer));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СтоимТовСНалВс', UTL_FORMAT_DECIMAL(p_invoice_row.price_nds_total));

  /* Необязательные атрибуты */
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ОКВ',            p_invoice_row.okv_code);

  /* Элементы                */
  UTL_CREATE_ERROR_NODES(p_doc, v_itemNode, p_invoice_row);
  UTL_CREATE_PERIOD_REFLECTION(p_doc, v_itemNode, p_invoice_row);
  UTL_CREATE_BUYER_INFO_NODES(p_doc, v_itemNode, 'СвПокуп', p_invoice_row.ROW_KEY);

  return true;
  exception when others then
    /*log error*/
    UTL_LOG('CLAIM_BUILD_T1_5', sqlcode, substr(sqlerrm, 0, 256));
    return false;
end;

/* ############################ */
/* # Формирование строки 1.6 ## */
/* ############################ */
FUNCTION CLAIM_BUILD_T1_6
(
   p_doc in xmldom.DOMDocument,
   p_container_node in xmldom.DOMNode,
   p_invoice_row in T$SEOD_DISCREP--sov_invoice_all%rowtype
) return boolean
as
  v_itemElement xmldom.DOMElement;
  v_itemNode xmldom.DOMNode;

  v_buyer_element xmldom.DOMElement;
  v_buyer_node xmldom.DOMNode;

  v_info_element xmldom.DOMElement;
  v_info_node xmldom.DOMNode;

  v_invoice_element xmldom.DOMElement;
  v_invoice_node xmldom.DOMNode;

  v_contractor_inn  varchar2(12 char);
  v_kpp_effective  varchar2(9 char);

  v_name varchar2(1000 char);
  v_last_name varchar2(128 char);
  v_patronymic varchar2(128 char);
  v_kpp varchar2(9 char);

begin

  /*в случае если первичная сторона была продавцом,
  то ее и используем в качестве данных о контрагенте*/
  v_contractor_inn := p_invoice_row.BUYER_INN;
  v_kpp_effective := p_invoice_row.buyer_kpp_effective;

  if length(v_contractor_inn) = 10 then
    v_itemNode := xslprocessor.selectSingleNode(p_container_node, '/Автотребование/ПризнРсхжд/T1.6/T1.6Стр[./СвКАгент/СведЮЛ/@ИННЮЛ="'||v_contractor_inn||'"]');
  else
    v_itemNode := xslprocessor.selectSingleNode(p_container_node, '/Автотребование/ПризнРсхжд/T1.6/T1.6Стр[./СвКАгент/СведИП/@ИННФЛ="'||v_contractor_inn||'"]');
  end if;

--  UTL_TRACE('Node value:'||xmldom.getNodeName(v_itemNode)||' ');

  if xmldom.getNodeName(v_itemNode) is null then
    v_itemElement := xmldom.createElement(p_doc, 'T1.6Стр');
    v_itemNode := xmldom.appendChild(p_container_node, xmldom.makeNode(v_itemElement));

    v_buyer_element := xmldom.createElement(p_doc, 'СвКАгент');
    v_buyer_node := xmldom.appendChild(v_itemNode, xmldom.makeNode(v_buyer_element));

    begin
        if length(v_contractor_inn) = 10 then
          if (v_kpp_effective is not null) then
            begin
              select name_full, kpp
              into v_name, v_kpp
              from TAX_PAYER t
              where inn = v_contractor_inn
                    and kpp_effective = v_kpp_effective;
            exception when no_data_found then
              null;
            end;
          else
            begin
              select name_full, kpp
              into v_name, v_kpp
              from
              (
                  select name_full, kpp, rownum as row_numner
                  from TAX_PAYER t
                  where inn = v_contractor_inn
              ) t
              where t.row_numner = 1;
            exception when no_data_found then
              null;
            end;
          end if;
          v_info_element := xmldom.createElement(p_doc, 'СведЮЛ');
          v_info_node := xmldom.appendChild(v_buyer_node, xmldom.makeNode(v_info_element));

          UTL_XML_APPEND_ATTRIBUTE(v_info_element, 'НаимКАгент', v_name);
          UTL_XML_APPEND_ATTRIBUTE(v_info_element, 'ИННЮЛ', v_contractor_inn);
          UTL_XML_APPEND_ATTRIBUTE(v_info_element, 'КПП', v_kpp);
        end if;

        if length(v_contractor_inn) = 12 then
          begin
            if (v_kpp_effective is not null) then
              begin
                select familiyanp, imyanp, otchestvonp
                into v_last_name, v_name, v_patronymic
                from TAX_PAYER t
                where inn = v_contractor_inn
                      and kpp_effective = v_kpp_effective;
              exception when no_data_found then
                null;
              end;
            else
              begin
                select familiyanp, imyanp, otchestvonp
                into  v_last_name, v_name, v_patronymic
                from
                (
                    select familiyanp, imyanp, otchestvonp, rownum as row_numner
                    from TAX_PAYER t
                    where inn = v_contractor_inn
                ) t
                where t.row_numner = 1;
              exception when no_data_found then
                null;
              end;
            end if;
          end;
          v_info_element := xmldom.createElement(p_doc, 'СведИП');
          v_info_node := xmldom.appendChild(v_buyer_node, xmldom.makeNode(v_info_element));


          UTL_XML_APPEND_ATTRIBUTE(v_info_element, 'Фамилия', v_last_name);
          UTL_XML_APPEND_ATTRIBUTE(v_info_element, 'Имя', v_name);
          UTL_XML_APPEND_ATTRIBUTE(v_info_element, 'Отчество', v_patronymic);
          UTL_XML_APPEND_ATTRIBUTE(v_info_element, 'ИННФЛ', v_contractor_inn);
        end if;
    end;

  end if;

  v_invoice_element := xmldom.createElement(p_doc, 'СведСФ');
  v_invoice_node := xmldom.appendChild(v_itemNode, xmldom.makeNode(v_invoice_element));

  UTL_XML_APPEND_ATTRIBUTE(v_invoice_element, 'НомСчФ',      p_invoice_row.INVOICE_NUM);
  UTL_XML_APPEND_ATTRIBUTE(v_invoice_element, 'ДатаСчФ',     UTL_FORMAT_DATE(p_invoice_row.INVOICE_DATE));

  return true;
  exception when others then
    /*log error*/
    UTL_LOG('CLAIM_BUILD_T1_6', sqlcode, substr(sqlerrm, 0, 256));
    return false;
end;


/* ############################ */
/* # Формирование строки 1.7 ## */
/* ############################ */
FUNCTION CLAIM_BUILD_T1_7
(
   p_doc in xmldom.DOMDocument,
   p_container_node in xmldom.DOMNode,
   p_violation_info in varchar2,
   p_cr_code in varchar2,
   p_left_side in number,
   p_rigth_side in number
)
return boolean
as
  v_itemElement xmldom.DOMElement;
  v_itemNode xmldom.DOMNode;
begin
  /*нода строки 1.7*/
  v_itemElement := xmldom.createElement(p_doc, 'T1.7Стр');
  v_itemNode := xmldom.appendChild(p_container_node, xmldom.makeNode(v_itemElement));
  /*обязательные*/
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СвНар',       p_violation_info);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'КодКС',       p_cr_code);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СумЛевЧ',     UTL_FORMAT_DECIMAL(p_left_side));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СумПравЧ',    UTL_FORMAT_DECIMAL(p_rigth_side));

  return true;
exception when others then
  /*log error*/
  UTL_LOG('CLAIM_BUILD_T1_7', sqlcode, substr(sqlerrm, 0, 256));
  return false;
end;

/* ############################ */
/* # Формирование строки 1.8 ## */
/* ############################ */
FUNCTION CLAIM_BUILD_T1_8
(
   p_doc in xmldom.DOMDocument,
   p_container_node in xmldom.DOMNode,
   p_invoice_row in T$SEOD_DISCREP--sov_invoice_all%rowtype
) return boolean
as
  v_itemElement xmldom.DOMElement;
  v_itemNode xmldom.DOMNode;
begin
  /* Узел строки             */
  v_itemElement := xmldom.createElement(p_doc, 'Т1.8Стр');
  v_itemNode := xmldom.appendChild(p_container_node, xmldom.makeNode(v_itemElement));

  /* Обязательные атрибуты   */
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомерПор',        p_invoice_row.ordinal_number);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомСчФПрод',      p_invoice_row.invoice_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СтоимПокупВ',     UTL_FORMAT_DECIMAL(p_invoice_row.price_buy_amount));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СумНДСВыч',       UTL_FORMAT_DECIMAL(p_invoice_row.price_buy_nds_amount));

  /* Необязательные атрибуты */
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаСчФПрод',     UTL_FORMAT_DATE(p_invoice_row.invoice_date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомИспрСчФ',      p_invoice_row.change_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаИспрСчФ',     UTL_FORMAT_DATE(p_invoice_row.change_date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомКСчФПрод',     p_invoice_row.correction_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаКСчФПрод',    UTL_FORMAT_DATE(p_invoice_row.correction_date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомИспрКСчФ',     p_invoice_row.change_correction_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаИспрКСчФ',    UTL_FORMAT_DATE(p_invoice_row.change_correction_date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомТД',           p_invoice_row.customs_declaration_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ОКВ',             p_invoice_row.okv_code);

  /* Элементы                */
  UTL_CREATE_ERROR_NODES(p_doc, v_itemNode, p_invoice_row);
  UTL_CREATE_PERIOD_REFLECTION(p_doc, v_itemNode, p_invoice_row);
  UTL_CREATE_SELLER_INFO_NODES(p_doc, v_itemNode, 'СвПрод', p_invoice_row.ROW_KEY);
  UTL_CREATE_TAXPAYER_INFO_NODE(p_doc, v_itemNode, 'СвПос',  p_invoice_row.broker_inn);
  UTL_CREATE_DOC_NODES(p_doc, v_itemNode, 'ДокПдтвУпл', 'НомДокПдтвУпл', 'ДатаДокПдтвУпл', p_invoice_row.ROW_KEY);
  UTL_CREATE_OPER_CODE_NODES(p_doc, v_itemNode, 'КодВидОпер', p_invoice_row.ROW_KEY);
  UTL_CREATE_BUY_ACCEPT_NODES(p_doc, v_itemNode, 'ДатаУчТов',  p_invoice_row.ROW_KEY);

  return true;
  exception when others then
    /*log error*/
    UTL_LOG('CLAIM_BUILD_T1_8', sqlcode, substr(sqlerrm, 0, 256));
    return false;
end;

/* ############################ */
/* # Формирование строки 1.9 ## */
/* ############################ */
FUNCTION CLAIM_BUILD_T1_9
(
   p_doc in xmldom.DOMDocument,
   p_container_node in xmldom.DOMNode,
   p_invoice_row in T$SEOD_DISCREP--sov_invoice_all%rowtype
) return boolean
as
  v_itemElement xmldom.DOMElement;
  v_itemNode xmldom.DOMNode;
begin
  /* Узел строки             */
  v_itemElement := xmldom.createElement(p_doc, 'T1.9Стр');
  v_itemNode := xmldom.appendChild(p_container_node, xmldom.makeNode(v_itemElement));

  /* Обязательные атрибуты   */
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомерПор',       p_invoice_row.ordinal_number);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомСчФПрод',     p_invoice_row.invoice_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаСчФПрод',    UTL_FORMAT_DATE(p_invoice_row.invoice_date));

  /* Необязательные атрибуты */
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомИспрСчФ',     p_invoice_row.change_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаИспрСчФ',    UTL_FORMAT_DATE(p_invoice_row.change_date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомКСчФПрод',    p_invoice_row.correction_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаКСчФПрод',   UTL_FORMAT_DATE(p_invoice_row.correction_date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомИспрКСчФ',    p_invoice_row.change_correction_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаИспрКСчФ',   UTL_FORMAT_DATE(p_invoice_row.change_correction_date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ОКВ',            p_invoice_row.okv_code);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СтоимПродСФВ',   UTL_FORMAT_DECIMAL(p_invoice_row.price_sell_in_curr));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СтоимПродСФ18',  UTL_FORMAT_DECIMAL(p_invoice_row.price_sell_18));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СтоимПродСФ10',  UTL_FORMAT_DECIMAL(p_invoice_row.price_sell_10));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СтоимПродСФ0',   UTL_FORMAT_DECIMAL(p_invoice_row.price_sell_0));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СумНДССФ18',     UTL_FORMAT_DECIMAL(p_invoice_row.price_nds_18));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СумНДССФ10',     UTL_FORMAT_DECIMAL(p_invoice_row.price_nds_10));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СтоимПродОсв',   UTL_FORMAT_DECIMAL(p_invoice_row.price_tax_free));

  /* Элементы                */
  UTL_CREATE_ERROR_NODES(p_doc, v_itemNode, p_invoice_row);
  UTL_CREATE_PERIOD_REFLECTION(p_doc, v_itemNode, p_invoice_row);
  UTL_CREATE_BUYER_INFO_NODES(p_doc, v_itemNode, 'СвПокуп', p_invoice_row.ROW_KEY);
  UTL_CREATE_TAXPAYER_INFO_NODE(p_doc, v_itemNode, 'СвПос', p_invoice_row.broker_inn);
  UTL_CREATE_DOC_NODES(p_doc, v_itemNode, 'ДокПдтвОпл', 'НомДокПдтвОпл', 'ДатаДокПдтвОпл', p_invoice_row.ROW_KEY);
  UTL_CREATE_OPER_CODE_NODES(p_doc, v_itemNode, 'КодВидОпер', p_invoice_row.ROW_KEY);

  return true;
  exception when others then
    /*log error*/
    UTL_LOG('CLAIM_BUILD_T1_9', sqlcode, substr(sqlerrm, 0, 256));
    return false;
end;

FUNCTION RECLAIM_BUILD
(
   p_doc in xmldom.DOMDocument,
   p_container_node in xmldom.DOMNode,
   p_invoice_row in stage_invoice%rowtype,
   p_tax_period in varchar2,
   p_tax_year in varchar2,
   p_element_name in varchar2
)
return boolean
is
  v_itemElement xmldom.DOMElement;
  v_itemNode xmldom.DOMNode;

  v_invoiceElement xmldom.DOMElement;
  v_invoiceNode xmldom.DOMNode;

  v_dateElement xmldom.DOMElement;
  v_dateNode xmldom.DOMNode;

  v_startPeriod varchar2(10 CHAR);
  v_endPeriod varchar2(10 CHAR);

  v_textElement  xmldom.DOMText;
  v_textNode xmldom.DOMNode;
begin
  /*нода строки*/
  v_itemElement := xmldom.createElement(p_doc, p_element_name);
  v_itemNode := xmldom.appendChild(p_container_node, xmldom.makeNode(v_itemElement));
  /*обязательные атрибуты*/
  UTL_GET_TAXPERIOD_BOUNDS(p_tax_period, p_tax_year, v_startPeriod, v_endPeriod);
  if p_invoice_row.correction_num is null then
     v_invoiceElement := xmldom.createElement(p_doc, 'КСФ');
     v_invoiceNode := xmldom.appendChild(v_itemNode, xmldom.makeNode(v_invoiceElement));

     UTL_XML_APPEND_ATTRIBUTE(v_invoiceElement, 'КодДок', '2772');
     UTL_XML_APPEND_ATTRIBUTE(v_invoiceElement, 'НомДок', p_invoice_row.correction_num);

     if p_invoice_row.correction_date is null then
       v_dateElement := xmldom.createElement(p_doc, 'ПериодДок');
       v_dateNode := xmldom.appendChild(v_invoiceNode, xmldom.makeNode(v_dateElement));

       UTL_XML_APPEND_ATTRIBUTE(v_dateElement, 'НачПер', v_startPeriod);
       UTL_XML_APPEND_ATTRIBUTE(v_dateElement, 'ОконПер', v_endPeriod);
     else
       v_dateElement := xmldom.createElement(p_doc, 'ДатаДок');
       v_dateNode := xmldom.appendChild(v_invoiceNode, xmldom.makeNode(v_dateElement));

       v_textElement := xmldom.createTextNode(p_doc, UTL_FORMAT_DATE(p_invoice_row.correction_date));
       v_textNode := xmldom.appendChild(v_dateNode, xmldom.makeNode(v_textElement));
     end if;
  else
     v_invoiceElement := xmldom.createElement(p_doc, 'СФ');
     v_invoiceNode := xmldom.appendChild(v_itemNode, xmldom.makeNode(v_invoiceElement));

     UTL_XML_APPEND_ATTRIBUTE(v_invoiceElement, 'КодДок', '0924');
     UTL_XML_APPEND_ATTRIBUTE(v_invoiceElement, 'НомДок', p_invoice_row.invoice_num);

     if p_invoice_row.invoice_date is null then
       v_dateElement := xmldom.createElement(p_doc, 'ПериодДок');
       v_dateNode := xmldom.appendChild(v_invoiceNode, xmldom.makeNode(v_dateElement));

       UTL_XML_APPEND_ATTRIBUTE(v_dateElement, 'НачПер', v_startPeriod);
       UTL_XML_APPEND_ATTRIBUTE(v_dateElement, 'ОконПер', v_endPeriod);
     else
       v_dateElement := xmldom.createElement(p_doc, 'ДатаДок');
       v_dateNode := xmldom.appendChild(v_invoiceNode, xmldom.makeNode(v_dateElement));

       v_textElement := xmldom.createTextNode(p_doc, UTL_FORMAT_DATE(p_invoice_row.invoice_date));
       v_textNode := xmldom.appendChild(v_dateNode, xmldom.makeNode(v_textElement));
     end if;
  end if;

  return true;

  exception when others then
    return false;
end;

PROCEDURE CREATE_DECL_KS_CLAIM
(
  p_sono_code in varchar2,
  p_seod_decl_regnum in number,
  p_decl_id in number
)
as
  doc xmldom.DOMDocument;
  docXml xmltype;
  docXmlValidation xmltype;

  main_node xmldom.DOMNode;
  root_node xmldom.DOMNode;
  user_node xmldom.DOMNode;
  item_node xmldom.DOMNode;
  root_elmt xmldom.DOMElement;
  item_elmt xmldom.DOMElement;
  item_text xmldom.DOMText;
  discrepElement xmldom.DOMElement;
  discrepNode xmldom.DOMNode;

  v_T17_ListElement xmldom.DOMElement;
  v_T17_ListNode xmldom.DOMNode;

  v_itemNode xmldom.DOMNode;

  v_count_of_processed_data number(19) := 0;
  v_document_id number(19);
  v_status number(2) := 1;
  v_doc_type number(1)  := 0;
  v_success_operation boolean := false;
  v_err_code varchar2(32 char);
  v_err_msg varchar2(128 char);
begin

v_document_id := SEQ_DOC.NEXTVAL;

doc := xmldom.newDOMDocument();
main_node := xmldom.makeNode(doc);
root_elmt := xmldom.createElement(doc, 'Автотребование');
xmldom.setAttribute(root_elmt, 'УчНомТреб', v_document_id);
xmldom.setAttribute(root_elmt, 'ТипИнф', 'TAX3EXCH_NDS2_CAM_01');
xmldom.setAttribute(root_elmt, 'РегНомДек', p_seod_decl_regnum);
xmldom.setAttribute(root_elmt, 'КодНО', p_sono_code);
xmldom.setAttribute(root_elmt, 'xsi:noNamespaceSchemaLocation', XSD_NDS2_CAM_01);
xmldom.setAttribute(root_elmt, 'xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
root_node := xmldom.appendChild(main_node, xmldom.makeNode(root_elmt));

discrepElement := xmldom.createElement(doc, 'ПризнРсхжд');
discrepNode := xmldom.appendChild(root_node, xmldom.makeNode(discrepElement));

v_T17_ListElement := xmldom.createElement(doc, 'T1.7');
v_T17_ListNode := xmldom.appendChild(discrepNode, xmldom.makeNode(v_T17_ListElement));

v_doc_type := 5;

begin

  /*Формирование АСК-1 по КС*/
  for ksLine in
  (
   select
     vw.*,
     decode(vw.Vypoln, 1, 10, 0, 12, vw.Vypoln) as vypolnCode
   from V$ASKKontrSoontosh vw
   where (vw.iddekl = p_decl_id)
     and vw.DeleteDate is null
  )
  loop
   v_count_of_processed_data := v_count_of_processed_data + 1;
   v_success_operation := CLAIM_BUILD_T1_7(doc, v_T17_ListNode, ksLine.vypolnCode, ksLine.KodKs, ksLine.LevyaChast, ksLine.PravyaChast);

   insert into CONTROL_RATIO_DOC values(ksLine.Id, v_document_id, sysdate);

   if not v_success_operation then
     UTL_LOG('CREATE_DECL_KS_CLAIM:', 3, 'Create 1.7 error ('||ksLine.Id||')', p_seod_decl_regnum);
     exit;
   end if;
  end loop;

  /* Валидация данных по схеме */
  if v_count_of_processed_data > 0 then
    UTL_REMOVE_IF_EMPTY(discrepNode, v_T17_ListNode);

    begin
      docXml := dbms_xmldom.getXmlType(doc);
      docXmlValidation := xmltype(docXml.getClobVal());
      if docXmlValidation.isSchemaValid(XSD_NDS2_CAM_01) = 0 then
        v_status := 3;
      end if;

      exception when others then
        v_status := 3;
        UTL_LOG('CREATE_DECL_KS_CLAIM:', 3, 'XSD load error', v_document_id);
    end;
  end if;

  /*Добавим запись в таблицу документов АСК*/
  insert into doc(doc_id, ref_entity_id, doc_type, doc_kind, sono_code, create_date, status, status_date, document_body)
  values (v_document_id, p_seod_decl_regnum, v_doc_type, 1, p_sono_code, sysdate, v_status, trunc(sysdate), replace(docXml.getClobVal(), XSD_NDS2_CAM_01, 'TAX3EXCH_NDS2_CAM_01_03.xsd'));

exception when others then
v_err_code := sqlcode;
v_err_msg := substr(sqlerrm, 1, 128);
UTL_LOG('CREATE_DECL_KS_CLAIM:', 3, 'ошибка формирования АСК-1 для КС для рег номера ('||v_err_code||'-'||v_err_msg||')', p_seod_decl_regnum);
end;

/*освободим ресурсы*/
xmldom.freeDocument(doc);
dbms_session.modify_package_state(dbms_session.free_all_resources);

end;

PROCEDURE CREATE_DECL_CLAIM_SIDE_1
(
  p_sono_code in varchar2,
  p_seod_decl_regnum in number,
  p_zip in number,
  p_success in out boolean
)
as
  doc xmldom.DOMDocument;
  docXml xmltype;

  main_node xmldom.DOMNode;
  root_node xmldom.DOMNode;
  user_node xmldom.DOMNode;
  item_node xmldom.DOMNode;
  root_elmt xmldom.DOMElement;
  item_elmt xmldom.DOMElement;
  item_text xmldom.DOMText;
  discrepElement xmldom.DOMElement;
  discrepNode xmldom.DOMNode;

  v_T11_ListElement xmldom.DOMElement;
  v_T11_ListNode xmldom.DOMNode;
  v_T12_ListElement xmldom.DOMElement;
  v_T12_ListNode xmldom.DOMNode;
  v_T13_ListElement xmldom.DOMElement;
  v_T13_ListNode xmldom.DOMNode;
  v_T14_ListElement xmldom.DOMElement;
  v_T14_ListNode xmldom.DOMNode;
  v_T15_ListElement xmldom.DOMElement;
  v_T15_ListNode xmldom.DOMNode;
  v_T16_ListElement xmldom.DOMElement;
  v_T16_ListNode xmldom.DOMNode;
  v_T18_ListElement xmldom.DOMElement;
  v_T18_ListNode xmldom.DOMNode;
  v_T19_ListElement xmldom.DOMElement;
  v_T19_ListNode xmldom.DOMNode;

  v_itemNode xmldom.DOMNode;

  v_is_T16 number(1);
  v_count_of_processed_data number(19) := 0;
  v_document_id number(19);
  v_exsisting_doc_id number(19);
  v_status number(2) := 1;
  v_doc_type number(1)  := 0;
  v_success_operation boolean := false;
  f_block number(1) := 0;
  v_invoice_already_added number;
  v_start_proc_date date := sysdate;
  cf_data T$DOC_INVOICE_FULL;

  v_decl_inn varchar2(12);
  v_decl_qtr number;
  v_decl_fiscal_year varchar2(4);
  v_decl_period_code varchar2(2);
  v_decl_kpp_effective varchar2(9);
  v_descr_count number := 0;
  v_descr_amount_sum number(22,2) := 0;
  v_descr_amount_pvp_sum number(22,2) := 0;
begin
  v_document_id := SEQ_DOC.NEXTVAL;

  doc := xmldom.newDOMDocument();
  main_node := xmldom.makeNode(doc);
  root_elmt := xmldom.createElement(doc, 'Автотребование');
  xmldom.setAttribute(root_elmt, 'УчНомТреб', v_document_id);
  xmldom.setAttribute(root_elmt, 'ТипИнф', 'TAX3EXCH_NDS2_CAM_01');
  xmldom.setAttribute(root_elmt, 'РегНомДек', p_seod_decl_regnum);
  xmldom.setAttribute(root_elmt, 'КодНО', p_sono_code);
  xmldom.setAttribute(root_elmt, 'xsi:noNamespaceSchemaLocation', XSD_NDS2_CAM_01);
  xmldom.setAttribute(root_elmt, 'xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
  root_node := xmldom.appendChild(main_node, xmldom.makeNode(root_elmt));

  discrepElement := xmldom.createElement(doc, 'ПризнРсхжд');
  discrepNode := xmldom.appendChild(root_node, xmldom.makeNode(discrepElement));

begin

  v_T11_ListElement := xmldom.createElement(doc, 'T1.1');
  v_T11_ListNode := xmldom.appendChild(discrepNode, xmldom.makeNode(v_T11_ListElement));

  v_T12_ListElement := xmldom.createElement(doc, 'T1.2');
  v_T12_ListNode := xmldom.appendChild(discrepNode, xmldom.makeNode(v_T12_ListElement));

  v_T13_ListElement := xmldom.createElement(doc, 'T1.3');
  v_T13_ListNode := xmldom.appendChild(discrepNode, xmldom.makeNode(v_T13_ListElement));

  v_T14_ListElement := xmldom.createElement(doc, 'T1.4');
  v_T14_ListNode := xmldom.appendChild(discrepNode, xmldom.makeNode(v_T14_ListElement));

  v_T15_ListElement := xmldom.createElement(doc, 'T1.5');
  v_T15_ListNode := xmldom.appendChild(discrepNode, xmldom.makeNode(v_T15_ListElement));

  v_T16_ListElement := xmldom.createElement(doc, 'T1.6');
  v_T16_ListNode := xmldom.appendChild(discrepNode, xmldom.makeNode(v_T16_ListElement));

  v_T18_ListElement := xmldom.createElement(doc, 'Т1.8');
  v_T18_ListNode := xmldom.appendChild(discrepNode, xmldom.makeNode(v_T18_ListElement));

  v_T19_ListElement := xmldom.createElement(doc, 'Т1.9');
  v_T19_ListNode := xmldom.appendChild(discrepNode, xmldom.makeNode(v_T19_ListElement));

end;

  --обработка счетов фактур
    delete from T$TMP_AUTO_CLAIM;
    v_doc_type := 1;

    select nd.innnp, nd.period, tp.quarter, nd.otchetgod, nd.kpp_effective
    into v_decl_inn, v_decl_period_code, v_decl_qtr, v_decl_fiscal_year, v_decl_kpp_effective
    from ASK_DECLANDJRNL nd join DICT_TAX_PERIOD tp on tp.code = nd.period
    where nd.zip = p_zip and rownum = 1;

    insert into DOC_INVOICE_PERIODS (doc_id, quarter, fiscal_year) values (v_document_id, v_decl_qtr, v_decl_fiscal_year);

    for discrepancy_invoice_line in
    (
		select
			 base.discrepancy_id
			,base.type
			,base.rule_num as rule
			,base.seller_inn
			,base.buyer_inn
			,sia.DECLARATION_VERSION_ID
			,sia.CHAPTER
			,sia.ORDINAL_NUMBER
			,sia.OKV_CODE
			,sia.CREATE_DATE
			,sia.RECEIVE_DATE
			,sia.INVOICE_NUM
			,sia.INVOICE_DATE
			,sia.CHANGE_NUM
			,sia.CHANGE_DATE
			,sia.CORRECTION_NUM
			,sia.CORRECTION_DATE
			,sia.CHANGE_CORRECTION_NUM
			,sia.CHANGE_CORRECTION_DATE
			,sia.SELLER_INVOICE_NUM
			,sia.SELLER_INVOICE_DATE
			,sia.BROKER_INN
			,sia.BROKER_KPP
			,sia.DEAL_KIND_CODE
			,sia.CUSTOMS_DECLARATION_NUM
			,sia.PRICE_BUY_AMOUNT
			,sia.PRICE_BUY_NDS_AMOUNT
			,sia.PRICE_SELL
			,sia.PRICE_SELL_IN_CURR
			,sia.PRICE_SELL_18
			,sia.PRICE_SELL_10
			,sia.PRICE_SELL_0
			,sia.PRICE_NDS_18
			,sia.PRICE_NDS_10
			,sia.PRICE_TAX_FREE
			,sia.PRICE_TOTAL
			,sia.PRICE_NDS_TOTAL
			,sia.DIFF_CORRECT_DECREASE
			,sia.DIFF_CORRECT_INCREASE
			,sia.DIFF_CORRECT_NDS_DECREASE
			,sia.DIFF_CORRECT_NDS_INCREASE
			,sia.PRICE_NDS_BUYER
			,sia.ROW_KEY
			,sia.ACTUAL_ROW_KEY
			,sia.COMPARE_ROW_KEY
			,sia.COMPARE_ALGO_ID
			,sia.FORMAT_ERRORS
			,sia.LOGICAL_ERRORS
			,sia.SELLER_AGENCY_INFO_INN
			,sia.SELLER_AGENCY_INFO_KPP
			,sia.SELLER_AGENCY_INFO_NAME
			,sia.SELLER_AGENCY_INFO_NUM
			,sia.SELLER_AGENCY_INFO_DATE
			,sia.IS_IMPORT
			,sia.CLARIFICATION_KEY
			,sia.CONTRAGENT_KEY
			,sia.IS_DOP_LIST
			,0 as decl_type
			,nvl(dh.period_code, v_decl_period_code) as tax_period
			,nvl(dh.fiscal_year, v_decl_fiscal_year) as fiscal_year
			,base.buyer_kpp_effective as buyer_kpp_effective
			,base.seller_kpp_effective as seller_kpp_effective
			,v_decl_qtr as quarter
			,base.amnt as amount
			,base.amount_pvp
      ,base.seller_zip
      ,base.seller_nd_prizn
      ,base.seller_sur
		from
		(
			select
			sd.discrepancy_id
			,svd.type
			,svd.rule_num
			,svd.seller_inn
			,svd.buyer_inn
			,svd.amnt
			,svd.amount_pvp
			,sd.INVOICE_ROW_KEY as invoice_rk
			,v_decl_period_code as period_code
			,v_decl_fiscal_year as fiscal_year
			,v_decl_kpp_effective as buyer_kpp_effective
			,ask_seller.kpp_effective as seller_kpp_effective
      ,svd.seller_zip
      ,ask_seller.nd_prizn as seller_nd_prizn
      ,seller_sur.sign_code as seller_sur
			from selection s
			inner join selection_discrepancy sd on sd.selection_id = s.id
			inner join sov_discrepancy svd on svd.id = sd.discrepancy_id
			left join ASK_DECLANDJRNL ask_seller on ask_seller.zip = svd.seller_zip
      left join (
        select sur.*, row_number() over (partition by sur.inn, sur.kpp_effective, sur.fiscal_year, sur.fiscal_period order by sur.sign_code) as rn
        from ext_sur sur) seller_sur
          on    seller_sur.inn = ask_seller.innnp
            and seller_sur.kpp_effective = ask_seller.kpp_effective
            and seller_sur.fiscal_year = ask_seller.otchetgod
            and seller_sur.fiscal_period = ask_seller.period
            and seller_sur.is_actual = 1
            and seller_sur.rn = 1
			where
            s.status = 5
			  and s.discrepancy_stage = 1
			  and sd.zip = p_zip
			  and sd.is_in_process = 1
        and not exists (
           select 1
           from doc_discrepancy dd1
             join doc d1 on d1.doc_id = dd1.doc_id
           where dd1.discrepancy_id = svd.id and d1.inn = v_decl_inn) --очищатор
		) base
		inner join sov_invoice_all sia
      on  ((base.invoice_rk like '%A%' and sia.actual_row_key = base.invoice_rk) or sia.row_key = base.invoice_rk)
	  and sia.discrepancy_id = base.discrepancy_id
    left join declaration_history dh 
      on base.invoice_rk like '%A%'
      and sia.declaration_version_id = dh.zip 
		order by sia.actual_row_key, sia.row_key
    )
    loop

	  merge into doc_discrepancy dest
      using (select discrepancy_invoice_line.DISCREPANCY_ID as dis_id, v_document_id as doc_id from dual) source
      on (source.doc_id = dest.doc_id and source.dis_id = dest.discrepancy_id)
      when not matched then
        insert (doc_id, discrepancy_id, row_key, amount, amount_pvp)
        values (v_document_id, discrepancy_invoice_line.DISCREPANCY_ID, discrepancy_invoice_line.row_key, discrepancy_invoice_line.amount, discrepancy_invoice_line.amount_pvp);

	  if   discrepancy_invoice_line.actual_row_key is null then
		  v_descr_amount_sum := v_descr_amount_sum + discrepancy_invoice_line.amount;
		  v_descr_amount_pvp_sum := v_descr_amount_pvp_sum + discrepancy_invoice_line.amount_pvp;
		  v_descr_count := v_descr_count + 1;
      end if;

      if   discrepancy_invoice_line.row_key like '%A%' then
        continue;
      end if;

      --проверим, что СФ не добавлялась в данный документ АСК-1
      select
          count(1) into v_invoice_already_added
      from T$TMP_AUTO_CLAIM
      where doc_id = v_document_id
            and invoice_rk = discrepancy_invoice_line.row_key;

      if v_invoice_already_added > 0 then
        continue;
      end if;

      insert into T$TMP_AUTO_CLAIM (DOC_ID, INVOICE_RK) values (v_document_id, discrepancy_invoice_line.row_key);

      v_is_T16 := 0;
      
      --Увеличим счетчик обработанных СФ
      v_count_of_processed_data := v_count_of_processed_data + 1;

      if discrepancy_invoice_line.chapter = 8 and discrepancy_invoice_line.is_dop_list = 0 then
        if v_is_T16 = 1 then
          v_success_operation := CLAIM_BUILD_T1_6(doc, v_T16_ListNode, discrepancy_invoice_line);
        else
          v_success_operation := CLAIM_BUILD_T1_1(doc, v_T11_ListNode, discrepancy_invoice_line);
        end if;
      end if;

      if discrepancy_invoice_line.chapter = 8 and discrepancy_invoice_line.is_dop_list = 1 then
        if v_is_T16 = 1 then
          v_success_operation := CLAIM_BUILD_T1_6(doc, v_T16_ListNode, discrepancy_invoice_line);
        else
          v_success_operation := CLAIM_BUILD_T1_8(doc, v_T18_ListNode, discrepancy_invoice_line);
        end if;
      end if;

      if discrepancy_invoice_line.chapter = 9 and discrepancy_invoice_line.is_dop_list = 0 then
        if v_is_T16 = 1 then
          v_success_operation := CLAIM_BUILD_T1_6(doc, v_T16_ListNode, discrepancy_invoice_line);
        else
          v_success_operation := CLAIM_BUILD_T1_2(doc, v_T12_ListNode, discrepancy_invoice_line);
        end if;
      end if;

      if discrepancy_invoice_line.chapter = 9 and discrepancy_invoice_line.is_dop_list = 1 then
        if v_is_T16 = 1 then
          v_success_operation := CLAIM_BUILD_T1_6(doc, v_T16_ListNode, discrepancy_invoice_line);
        else
          v_success_operation := CLAIM_BUILD_T1_9(doc, v_T19_ListNode, discrepancy_invoice_line);
        end if;
      end if;

      if discrepancy_invoice_line.chapter = 10 then
        if v_is_T16 = 1 then
          v_success_operation := CLAIM_BUILD_T1_6(doc, v_T16_ListNode, discrepancy_invoice_line);
        else
          v_success_operation := CLAIM_BUILD_T1_3(doc, v_T13_ListNode, discrepancy_invoice_line);
        end if;
      end if;

      if discrepancy_invoice_line.chapter = 11 then
        v_success_operation := CLAIM_BUILD_T1_4(doc, v_T14_ListNode, discrepancy_invoice_line);
      end if;

      if discrepancy_invoice_line.chapter = 12 then
        if v_is_T16 = 1 then
          v_success_operation := CLAIM_BUILD_T1_6(doc, v_T16_ListNode, discrepancy_invoice_line);
        else
          v_success_operation := CLAIM_BUILD_T1_5(doc, v_T15_ListNode, discrepancy_invoice_line);
        end if;
      end if;

     select
              discrepancy_invoice_line.declaration_version_id
             ,discrepancy_invoice_line.chapter as invoice_chapter
             ,discrepancy_invoice_line.row_key as invoice_row_key
             ,v_document_id as doc_id
             ,discrepancy_invoice_line.ordinal_number
             ,discrepancy_invoice_line.okv_code
             ,discrepancy_invoice_line.create_date
             ,discrepancy_invoice_line.receive_date
             ,discrepancy_invoice_line.invoice_num
             ,discrepancy_invoice_line.invoice_date
             ,discrepancy_invoice_line.change_num
             ,discrepancy_invoice_line.change_date
             ,discrepancy_invoice_line.correction_num
             ,discrepancy_invoice_line.correction_date
             ,discrepancy_invoice_line.change_correction_num
             ,discrepancy_invoice_line.change_correction_date
             ,discrepancy_invoice_line.seller_invoice_num
             ,discrepancy_invoice_line.seller_invoice_date
             ,discrepancy_invoice_line.broker_inn
             ,discrepancy_invoice_line.broker_kpp
             ,discrepancy_invoice_line.deal_kind_code
             ,discrepancy_invoice_line.customs_declaration_num
             ,discrepancy_invoice_line.price_buy_amount
             ,discrepancy_invoice_line.price_buy_nds_amount
             ,discrepancy_invoice_line.price_sell
             ,discrepancy_invoice_line.price_sell_in_curr
             ,discrepancy_invoice_line.price_sell_18
             ,discrepancy_invoice_line.price_sell_10
             ,discrepancy_invoice_line.price_sell_0
             ,discrepancy_invoice_line.price_nds_18
             ,discrepancy_invoice_line.price_nds_10
             ,discrepancy_invoice_line.price_tax_free
             ,discrepancy_invoice_line.price_total
             ,discrepancy_invoice_line.price_nds_total
             ,discrepancy_invoice_line.diff_correct_decrease
             ,discrepancy_invoice_line.diff_correct_increase
             ,discrepancy_invoice_line.diff_correct_nds_decrease
             ,discrepancy_invoice_line.diff_correct_nds_increase
             ,discrepancy_invoice_line.price_nds_buyer
             ,discrepancy_invoice_line.actual_row_key
             ,discrepancy_invoice_line.compare_row_key
             ,discrepancy_invoice_line.compare_algo_id
             ,discrepancy_invoice_line.format_errors
             ,discrepancy_invoice_line.logical_errors
             ,discrepancy_invoice_line.seller_agency_info_inn
             ,discrepancy_invoice_line.seller_agency_info_kpp
             ,discrepancy_invoice_line.seller_agency_info_name
             ,discrepancy_invoice_line.seller_agency_info_num
             ,discrepancy_invoice_line.seller_agency_info_date
             ,discrepancy_invoice_line.is_import
             ,discrepancy_invoice_line.clarification_key
             ,discrepancy_invoice_line.contragent_key
             ,discrepancy_invoice_line.is_dop_list
             ,discrepancy_invoice_line.tax_period as decl_tax_period
             ,discrepancy_invoice_line.fiscal_year as decl_fiscal_year
        into cf_data
        from dual;

        FILL_DOC_INVOICE(cf_data);

      if not v_success_operation then
        p_success := v_success_operation;
        UTL_LOG('CREATE_DECLARATION_CLAIM:', 3, 'Ошибка формирования ', v_document_id);
        exit;
      end if;
    end loop;

	--  UTL_LOG('CREATE_DECLARATION_CLAIM:', 1, 'count', v_count_of_processed_data);
  if v_count_of_processed_data > 0 and v_success_operation then

    UTL_REMOVE_IF_EMPTY(discrepNode, v_T11_ListNode);
    UTL_REMOVE_IF_EMPTY(discrepNode, v_T12_ListNode);
    UTL_REMOVE_IF_EMPTY(discrepNode, v_T13_ListNode);
    UTL_REMOVE_IF_EMPTY(discrepNode, v_T14_ListNode);
    UTL_REMOVE_IF_EMPTY(discrepNode, v_T15_ListNode);
    UTL_REMOVE_IF_EMPTY(discrepNode, v_T16_ListNode);
    UTL_REMOVE_IF_EMPTY(discrepNode, v_T18_ListNode);
    UTL_REMOVE_IF_EMPTY(discrepNode, v_T19_ListNode);

    begin
      docXml := dbms_xmldom.getXmlType(doc);
      docXml := docXml.createSchemaBasedXML(XSD_NDS2_CAM_01);
      xmltype.schemaValidate(docXml);

      v_status := -1;

      exception when others then
        v_status := 3;
        UTL_LOG('CREATE_DECLARATION_CLAIM:', -1, 'XSD validation error : '||substr(sqlerrm, 1, 512), v_document_id);
      end;

    insert into doc(
       doc_id
      ,ref_entity_id
      ,doc_type
      ,doc_kind
      ,sono_code
      ,create_date
      ,status
	  ,status_date
      ,document_body
      ,process_elapsed
      ,processed
      ,inn
      ,kpp_effective
      ,zip
      ,fiscal_year
      ,quarter
      ,discrepancy_amount
      ,discrepancy_amount_pvp
      ,discrepancy_count)
    values (
       v_document_id
      ,p_seod_decl_regnum
      ,v_doc_type
      ,1
      ,p_sono_code
      ,sysdate
      ,v_status
	  ,trunc(sysdate)
      ,replace(docXml.getClobVal(), XSD_NDS2_CAM_01, 'TAX3EXCH_NDS2_CAM_01_03.xsd')
      ,((sysdate - v_start_proc_date) * 24 * 60 * 60 * 1000)
      ,0
      ,v_decl_inn
      ,v_decl_kpp_effective
      ,p_zip
      ,v_decl_fiscal_year
      ,v_decl_qtr
      ,v_descr_amount_sum
      ,v_descr_amount_pvp_sum
      ,v_descr_count);

    xmldom.freeDocument(doc);
    dbms_session.modify_package_state(dbms_session.free_all_resources);


  end if;
  exception when others then
    p_success := false;
    UTL_LOG('CREATE_DECLARATION_CLAIM:', 3, sqlerrm, v_document_id);
end;

PROCEDURE CREATE_DECL_CLAIM_SIDE_2
(
  p_sono_code in varchar2,
  p_seod_decl_regnum in number,
  p_zip in number,
  p_success in out boolean
)
as
  doc xmldom.DOMDocument;
  docXml xmltype;

  main_node xmldom.DOMNode;
  root_node xmldom.DOMNode;
  user_node xmldom.DOMNode;
  item_node xmldom.DOMNode;
  root_elmt xmldom.DOMElement;
  item_elmt xmldom.DOMElement;
  item_text xmldom.DOMText;
  discrepElement xmldom.DOMElement;
  discrepNode xmldom.DOMNode;

  v_T11_ListElement xmldom.DOMElement;
  v_T11_ListNode xmldom.DOMNode;
  v_T12_ListElement xmldom.DOMElement;
  v_T12_ListNode xmldom.DOMNode;
  v_T13_ListElement xmldom.DOMElement;
  v_T13_ListNode xmldom.DOMNode;
  v_T14_ListElement xmldom.DOMElement;
  v_T14_ListNode xmldom.DOMNode;
  v_T15_ListElement xmldom.DOMElement;
  v_T15_ListNode xmldom.DOMNode;
  v_T16_ListElement xmldom.DOMElement;
  v_T16_ListNode xmldom.DOMNode;
  v_T18_ListElement xmldom.DOMElement;
  v_T18_ListNode xmldom.DOMNode;
  v_T19_ListElement xmldom.DOMElement;
  v_T19_ListNode xmldom.DOMNode;

  v_itemNode xmldom.DOMNode;

  v_is_T16 number(1);
  v_count_of_processed_data number(19) := 0;
  v_document_id number(19);
  v_status number(2) := 1;
  v_doc_type number(1)  := 0;
  v_success_operation boolean := false;
  f_block number(1) := 0;
  v_invoice_already_added number;
  v_start_proc_date date := sysdate;
  c_2nd_side_doc_kind number(1) := 2;
  cf_data T$DOC_INVOICE_FULL;
  v_decl_inn varchar2(12);
  v_decl_qtr number;
  v_decl_fiscal_year varchar2(4);
  v_decl_period_code varchar2(2);
  v_decl_kpp_effective varchar2(9);
  v_descr_count number := 0;
  v_descr_amount_sum number(19,2) := 0;
  v_descr_amount_pvp_sum number(19,2) := 0;
begin

  begin
  v_document_id := SEQ_DOC.NEXTVAL;

  doc := xmldom.newDOMDocument();
  main_node := xmldom.makeNode(doc);
  root_elmt := xmldom.createElement(doc, 'Автотребование');
  xmldom.setAttribute(root_elmt, 'УчНомТреб', v_document_id);
  xmldom.setAttribute(root_elmt, 'ТипИнф', 'TAX3EXCH_NDS2_CAM_01');
  xmldom.setAttribute(root_elmt, 'РегНомДек', p_seod_decl_regnum);
  xmldom.setAttribute(root_elmt, 'КодНО', p_sono_code);
  xmldom.setAttribute(root_elmt, 'xsi:noNamespaceSchemaLocation', XSD_NDS2_CAM_01);
  xmldom.setAttribute(root_elmt, 'xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
  root_node := xmldom.appendChild(main_node, xmldom.makeNode(root_elmt));

  discrepElement := xmldom.createElement(doc, 'ПризнРсхжд');
  discrepNode := xmldom.appendChild(root_node, xmldom.makeNode(discrepElement));

  v_T11_ListElement := xmldom.createElement(doc, 'T1.1');
  v_T11_ListNode := xmldom.appendChild(discrepNode, xmldom.makeNode(v_T11_ListElement));

  v_T12_ListElement := xmldom.createElement(doc, 'T1.2');
  v_T12_ListNode := xmldom.appendChild(discrepNode, xmldom.makeNode(v_T12_ListElement));

  v_T13_ListElement := xmldom.createElement(doc, 'T1.3');
  v_T13_ListNode := xmldom.appendChild(discrepNode, xmldom.makeNode(v_T13_ListElement));

  v_T14_ListElement := xmldom.createElement(doc, 'T1.4');
  v_T14_ListNode := xmldom.appendChild(discrepNode, xmldom.makeNode(v_T14_ListElement));

  v_T15_ListElement := xmldom.createElement(doc, 'T1.5');
  v_T15_ListNode := xmldom.appendChild(discrepNode, xmldom.makeNode(v_T15_ListElement));

  v_T16_ListElement := xmldom.createElement(doc, 'T1.6');
  v_T16_ListNode := xmldom.appendChild(discrepNode, xmldom.makeNode(v_T16_ListElement));

  v_T18_ListElement := xmldom.createElement(doc, 'Т1.8');
  v_T18_ListNode := xmldom.appendChild(discrepNode, xmldom.makeNode(v_T18_ListElement));

  v_T19_ListElement := xmldom.createElement(doc, 'Т1.9');
  v_T19_ListNode := xmldom.appendChild(discrepNode, xmldom.makeNode(v_T19_ListElement));
  end;

  /*обработка счетов фактур*/
    delete from T$TMP_AUTO_CLAIM;

    select nd.innnp, nd.period, tp.quarter, nd.otchetgod, nd.kpp_effective
    into v_decl_inn, v_decl_period_code, v_decl_qtr, v_decl_fiscal_year, v_decl_kpp_effective
    from ASK_DECLANDJRNL nd join DICT_TAX_PERIOD tp on tp.code = nd.period
    where nd.zip = p_zip and rownum = 1;

    insert into DOC_INVOICE_PERIODS (doc_id, quarter, fiscal_year) values (v_document_id, v_decl_qtr, v_decl_fiscal_year);

    v_doc_type := 1;
    for discrepancy_invoice_line in
    (
      select
        sd.discrepancy_id
        ,svd.type
        ,svd.rule_num
        ,svd.seller_inn
        ,svd.buyer_inn
        ,sia.DECLARATION_VERSION_ID
        ,sia.CHAPTER
        ,sia.ORDINAL_NUMBER
        ,sia.OKV_CODE
        ,sia.CREATE_DATE
        ,sia.RECEIVE_DATE
        ,sia.INVOICE_NUM
        ,sia.INVOICE_DATE
        ,sia.CHANGE_NUM
        ,sia.CHANGE_DATE
        ,sia.CORRECTION_NUM
        ,sia.CORRECTION_DATE
        ,sia.CHANGE_CORRECTION_NUM
        ,sia.CHANGE_CORRECTION_DATE
        ,sia.SELLER_INVOICE_NUM
        ,sia.SELLER_INVOICE_DATE
        ,sia.BROKER_INN
        ,sia.BROKER_KPP
        ,sia.DEAL_KIND_CODE
        ,sia.CUSTOMS_DECLARATION_NUM
        ,sia.PRICE_BUY_AMOUNT
        ,sia.PRICE_BUY_NDS_AMOUNT
        ,sia.PRICE_SELL
        ,sia.PRICE_SELL_IN_CURR
        ,sia.PRICE_SELL_18
        ,sia.PRICE_SELL_10
        ,sia.PRICE_SELL_0
        ,sia.PRICE_NDS_18
        ,sia.PRICE_NDS_10
        ,sia.PRICE_TAX_FREE
        ,sia.PRICE_TOTAL
        ,sia.PRICE_NDS_TOTAL
        ,sia.DIFF_CORRECT_DECREASE
        ,sia.DIFF_CORRECT_INCREASE
        ,sia.DIFF_CORRECT_NDS_DECREASE
        ,sia.DIFF_CORRECT_NDS_INCREASE
        ,sia.PRICE_NDS_BUYER
        ,sia.ROW_KEY
        ,sia.ACTUAL_ROW_KEY
        ,sia.COMPARE_ROW_KEY
        ,sia.COMPARE_ALGO_ID
        ,sia.FORMAT_ERRORS
        ,sia.LOGICAL_ERRORS
        ,sia.SELLER_AGENCY_INFO_INN
        ,sia.SELLER_AGENCY_INFO_KPP
        ,sia.SELLER_AGENCY_INFO_NAME
        ,sia.SELLER_AGENCY_INFO_NUM
        ,sia.SELLER_AGENCY_INFO_DATE
        ,sia.IS_IMPORT
        ,sia.CLARIFICATION_KEY
        ,sia.CONTRAGENT_KEY
        ,sia.IS_DOP_LIST
        ,dh.type_code as decl_type
        ,dh.period_code as tax_period
        ,dh.fiscal_year as fiscal_year
	      ,ask_buyer.kpp_effective as buyer_kpp_effective
        ,ask_seller.kpp_effective as seller_kpp_effective
		,dtp.quarter
		,svd.amnt as amount
		,svd.amount_pvp
        ,svd.seller_zip
        ,ask_seller.nd_prizn as seller_nd_prizn
        ,'4' as seller_sur
      from selection s
      inner join selection_discrepancy sd on sd.selection_id = s.id
      inner join v$ask_declandjrnl ask on ask.zip = sd.zip
      inner join sov_discrepancy svd on svd.id = sd.discrepancy_id
      inner join sov_invoice_all sia on ((sd.INVOICE_ROW_KEY like '%A%' and sia.actual_row_key = sd.INVOICE_ROW_KEY) or sia.row_key = sd.INVOICE_ROW_KEY) and sia.discrepancy_id = sd.discrepancy_id
      join declaration_history dh on dh.zip = sia.declaration_version_id
	  join dict_tax_period dtp on dtp.code = dh.period_code
      left join v$ask_declandjrnl ask_buyer on ask_buyer.zip = svd.buyer_zip
      left join v$ask_declandjrnl ask_seller on ask_seller.zip = svd.seller_zip
      where
      s.status = 10
      and s.discrepancy_stage = 2
      and sd.zip = p_zip
      and sd.is_in_process = 1
      and not exists --очищатор
      (
        select
        1
        from
        doc_discrepancy dd1
        inner join doc d1 on d1.doc_id = dd1.doc_id
        where dd1.discrepancy_id = svd.id and d1.inn = ask.INNNP
      )
    order by sia.actual_row_key, sia.row_key
    )
    loop

      merge into doc_discrepancy dest
      using (select discrepancy_invoice_line.DISCREPANCY_ID as dis_id, v_document_id as doc_id from dual) source
      on (source.doc_id = dest.doc_id and source.dis_id = dest.discrepancy_id)
      when not matched then
        insert (doc_id, discrepancy_id, row_key, amount, amount_pvp)
        values (v_document_id, discrepancy_invoice_line.DISCREPANCY_ID, discrepancy_invoice_line.row_key, discrepancy_invoice_line.amount, discrepancy_invoice_line.amount_pvp);

	  if   discrepancy_invoice_line.actual_row_key is null then
		  v_descr_amount_sum := v_descr_amount_sum + discrepancy_invoice_line.amount;
		  v_descr_amount_pvp_sum := v_descr_amount_pvp_sum + discrepancy_invoice_line.amount_pvp;
		  v_descr_count := v_descr_count + 1;
      end if;

	  if   discrepancy_invoice_line.row_key like '%A%' then
        continue;
      end if;

      /*проверим, что СФ не добавлялась в данный документ АСК-1*/
      select
          count(1) into v_invoice_already_added
      from T$TMP_AUTO_CLAIM
      where doc_id = v_document_id
            and invoice_rk = discrepancy_invoice_line.row_key;

      if v_invoice_already_added > 0 then
        continue;
      end if;

      insert into T$TMP_AUTO_CLAIM (DOC_ID, INVOICE_RK) values (v_document_id, discrepancy_invoice_line.row_key);

      /* Увеличим счетчик обработанных СФ */
      v_count_of_processed_data := v_count_of_processed_data + 1;

      /* Разрыв */
      if discrepancy_invoice_line.type = 1 then
        begin
          v_success_operation := CLAIM_BUILD_T1_6(doc, v_T16_ListNode, discrepancy_invoice_line);
        end;
      end if;

      /* Неточное сопоставление + НДС */
      if discrepancy_invoice_line.type in (2,4) then
        begin
          /*есть расхождения в ИНН*/
          if discrepancy_invoice_line.rule_num in (9, 10, 11, 12, 13, 14, 15, 16) then
           begin
            if discrepancy_invoice_line.chapter in (9, 10, 12) then
                v_success_operation := CLAIM_BUILD_T1_6(doc, v_T16_ListNode, discrepancy_invoice_line);
            end if;

            if discrepancy_invoice_line.chapter in (11) then
                v_success_operation := CLAIM_BUILD_T1_4(doc, v_T14_ListNode, discrepancy_invoice_line);
            end if;
           end;
          else
           begin
            if discrepancy_invoice_line.chapter = 8 and discrepancy_invoice_line.is_dop_list = 0 then
                v_success_operation := CLAIM_BUILD_T1_1(doc, v_T11_ListNode, discrepancy_invoice_line);
            end if;

            if discrepancy_invoice_line.chapter = 8 and discrepancy_invoice_line.is_dop_list = 1 then
                v_success_operation := CLAIM_BUILD_T1_8(doc, v_T18_ListNode, discrepancy_invoice_line);
            end if;

            if discrepancy_invoice_line.chapter = 9 and discrepancy_invoice_line.is_dop_list = 0 then
                v_success_operation := CLAIM_BUILD_T1_2(doc, v_T12_ListNode, discrepancy_invoice_line);
            end if;

            if discrepancy_invoice_line.chapter = 9 and discrepancy_invoice_line.is_dop_list = 1 then
                v_success_operation := CLAIM_BUILD_T1_9(doc, v_T19_ListNode, discrepancy_invoice_line);
            end if;

            if discrepancy_invoice_line.chapter = 10 then
                v_success_operation := CLAIM_BUILD_T1_3(doc, v_T13_ListNode, discrepancy_invoice_line);
            end if;

            if discrepancy_invoice_line.chapter = 11 then
              v_success_operation := CLAIM_BUILD_T1_4(doc, v_T14_ListNode, discrepancy_invoice_line);
            end if;

            if discrepancy_invoice_line.chapter = 12 then
                v_success_operation := CLAIM_BUILD_T1_5(doc, v_T15_ListNode, discrepancy_invoice_line);
            end if;
           end;
          end if;
        end;
      end if;

      /* Валюта */
      if discrepancy_invoice_line.type = 3 then
        begin
          null;
        end;
      end if;

     select
              discrepancy_invoice_line.declaration_version_id
             ,discrepancy_invoice_line.chapter as invoice_chapter
             ,discrepancy_invoice_line.row_key as invoice_row_key
             ,v_document_id as doc_id
             ,discrepancy_invoice_line.ordinal_number
             ,discrepancy_invoice_line.okv_code
             ,discrepancy_invoice_line.create_date
             ,discrepancy_invoice_line.receive_date
             ,discrepancy_invoice_line.invoice_num
             ,discrepancy_invoice_line.invoice_date
             ,discrepancy_invoice_line.change_num
             ,discrepancy_invoice_line.change_date
             ,discrepancy_invoice_line.correction_num
             ,discrepancy_invoice_line.correction_date
             ,discrepancy_invoice_line.change_correction_num
             ,discrepancy_invoice_line.change_correction_date
             ,discrepancy_invoice_line.seller_invoice_num
             ,discrepancy_invoice_line.seller_invoice_date
             ,discrepancy_invoice_line.broker_inn
             ,discrepancy_invoice_line.broker_kpp
             ,discrepancy_invoice_line.deal_kind_code
             ,discrepancy_invoice_line.customs_declaration_num
             ,discrepancy_invoice_line.price_buy_amount
             ,discrepancy_invoice_line.price_buy_nds_amount
             ,discrepancy_invoice_line.price_sell
             ,discrepancy_invoice_line.price_sell_in_curr
             ,discrepancy_invoice_line.price_sell_18
             ,discrepancy_invoice_line.price_sell_10
             ,discrepancy_invoice_line.price_sell_0
             ,discrepancy_invoice_line.price_nds_18
             ,discrepancy_invoice_line.price_nds_10
             ,discrepancy_invoice_line.price_tax_free
             ,discrepancy_invoice_line.price_total
             ,discrepancy_invoice_line.price_nds_total
             ,discrepancy_invoice_line.diff_correct_decrease
             ,discrepancy_invoice_line.diff_correct_increase
             ,discrepancy_invoice_line.diff_correct_nds_decrease
             ,discrepancy_invoice_line.diff_correct_nds_increase
             ,discrepancy_invoice_line.price_nds_buyer
             ,discrepancy_invoice_line.actual_row_key
             ,discrepancy_invoice_line.compare_row_key
             ,discrepancy_invoice_line.compare_algo_id
             ,discrepancy_invoice_line.format_errors
             ,discrepancy_invoice_line.logical_errors
             ,discrepancy_invoice_line.seller_agency_info_inn
             ,discrepancy_invoice_line.seller_agency_info_kpp
             ,discrepancy_invoice_line.seller_agency_info_name
             ,discrepancy_invoice_line.seller_agency_info_num
             ,discrepancy_invoice_line.seller_agency_info_date
             ,discrepancy_invoice_line.is_import
             ,discrepancy_invoice_line.clarification_key
             ,discrepancy_invoice_line.contragent_key
             ,discrepancy_invoice_line.is_dop_list
             ,discrepancy_invoice_line.tax_period as decl_tax_period
             ,discrepancy_invoice_line.fiscal_year as decl_fiscal_year
        into cf_data
        from dual;

        FILL_DOC_INVOICE(cf_data);

      if not v_success_operation then
        p_success := v_success_operation;
        UTL_LOG('CREATE_DECLARATION_CLAIM:', 3, 'Ошибка формирования ', v_document_id);
        exit;
      end if;
    end loop;

  UTL_LOG('CREATE_DECLARATION_CLAIM:', 1, 'count', v_count_of_processed_data);
  if v_count_of_processed_data > 0 then
    UTL_REMOVE_IF_EMPTY(discrepNode, v_T11_ListNode);
    UTL_REMOVE_IF_EMPTY(discrepNode, v_T12_ListNode);
    UTL_REMOVE_IF_EMPTY(discrepNode, v_T13_ListNode);
    UTL_REMOVE_IF_EMPTY(discrepNode, v_T14_ListNode);
    UTL_REMOVE_IF_EMPTY(discrepNode, v_T15_ListNode);
    UTL_REMOVE_IF_EMPTY(discrepNode, v_T16_ListNode);
    UTL_REMOVE_IF_EMPTY(discrepNode, v_T18_ListNode);
    UTL_REMOVE_IF_EMPTY(discrepNode, v_T19_ListNode);

    begin
      docXml := dbms_xmldom.getXmlType(doc);
      docXml := docXml.createSchemaBasedXML(XSD_NDS2_CAM_01);
      xmltype.schemaValidate(docXml);

      v_status := -1;

      exception when others then
        v_status := 3;
        UTL_LOG('CREATE_DECLARATION_CLAIM:', -1, 'XSD validation error'||substr(sqlerrm, 1, 512), v_document_id);
      end;


    insert into doc(
       doc_id
      ,ref_entity_id
      ,doc_type
      ,doc_kind
      ,sono_code
      ,create_date
      ,status
	  ,status_date
      ,document_body
      ,process_elapsed
      ,processed
      ,inn
      ,kpp_effective
      ,zip
      ,fiscal_year
      ,quarter
      ,discrepancy_amount
      ,discrepancy_amount_pvp
      ,discrepancy_count)
    values (
       v_document_id
      ,p_seod_decl_regnum
      ,v_doc_type
      ,c_2nd_side_doc_kind
      ,p_sono_code
      ,sysdate
      ,v_status
	  ,trunc(sysdate)
      ,replace(docXml.getClobVal(), XSD_NDS2_CAM_01, 'TAX3EXCH_NDS2_CAM_01_03.xsd'), ((sysdate - v_start_proc_date) * 24 * 60 * 60 * 1000)
      ,0
      ,v_decl_inn
      ,v_decl_kpp_effective
      ,p_zip
      ,v_decl_fiscal_year
      ,v_decl_qtr
      ,v_descr_amount_sum
      ,v_descr_amount_pvp_sum
      ,v_descr_count);

    xmldom.freeDocument(doc);
    dbms_session.modify_package_state(dbms_session.free_all_resources);
  end if;

    exception when others then
    p_success := false;
    UTL_LOG('CREATE_DECL_CLAIM_SIDE_2:', 3, sqlerrm, v_document_id);
end;



PROCEDURE CREATE_DECLARATION_RECLAIM
(
  p_decl_version_id in number,
  p_sono_code in varchar2,
  p_seod_decl_regnum in number,
  p_knp_closed in number,
  p_for_stage in number := 0
)
as
  doc xmldom.DOMDocument;
  docXml xmltype;
  docXmlValidation xmltype;
  main_node xmldom.DOMNode;
  root_node xmldom.DOMNode;
  user_node xmldom.DOMNode;
  item_node xmldom.DOMNode;
  root_elmt xmldom.DOMElement;
  source_elmt xmldom.DOMElement;
  source_node xmldom.DOMNode;
  item_elmt xmldom.DOMElement;
  item_text xmldom.DOMText;
  taxpr_node xmldom.DOMNode;
  taxpr_elmt xmldom.DOMElement;
  fio_node xmldom.DOMNode;
  fio_elmt xmldom.DOMElement;

  v_decl_period varchar2(2 char);
  v_decl_year varchar2(4 char);
  v_document_id number;

  v_taxpr_id number := 0;
  v_taxpr_inn varchar2(12 char);
  v_taxpr_kpp varchar2(9 char);
  v_taxpr_code_no varchar2(4 char);

  v_ul_name varchar2(1000 char);
  v_ip_first_name varchar2(60 char);
  v_ip_patronymic varchar2(60 char);
  v_ip_last_name varchar2(60 char);

  v_reclaim_elem_name varchar2(32 char);
  v_schema_name varchar2(128 char);
  v_doc_type number(2);
  v_status number(2) := 1;
  f_block number(1) := 0;
begin
  v_document_id := SEQ_DOC.NEXTVAL;
  doc := xmldom.newDOMDocument();
  main_node := xmldom.makeNode(doc);
  if p_knp_closed = 0 then
    root_elmt := xmldom.createElement(doc, 'Истреб93');
    xmldom.setAttribute(root_elmt, 'КодНО', p_sono_code);
    xmldom.setAttribute(root_elmt, 'РегНомДек', p_seod_decl_regnum);
    xmldom.setAttribute(root_elmt, 'УчНомИстреб', v_document_id);
    xmldom.setAttribute(root_elmt, 'ТипИнф', 'TAX3EXCH_NDS2_CAM_02');
    xmldom.setAttribute(root_elmt, 'xsi:noNamespaceSchemaLocation', 'TAX3EXCH_NDS2_CAM_02_01.xsd');
    xmldom.setAttribute(root_elmt, 'xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
    root_node := xmldom.appendChild(main_node, xmldom.makeNode(root_elmt));

    v_reclaim_elem_name := 'Истреб93Док';
    v_schema_name := XSD_NDS2_CAM_02;
    v_doc_type := 2;
  else
    select ul.id, ul.inn, ul.kpp, ul.name_full, ul.code_no
    into v_taxpr_id, v_taxpr_inn, v_taxpr_kpp, v_ul_name, v_taxpr_code_no
    from v$declaration hist_decl
    inner join v$egrn_ul ul on ul.inn = hist_decl.inn and ul.kpp = hist_decl.kpp
    where hist_decl.declaration_version_id = p_decl_version_id;

    if v_taxpr_id = 0 then
      select ip.id, ip.inn, ip.first_name, ip.patronymic, ip.last_name, ip.code_no
      into v_taxpr_id, v_taxpr_inn, v_ip_first_name, v_ip_patronymic, v_ip_last_name, v_taxpr_code_no
      from v$declaration hist_decl
      inner join v$egrn_ip ip on ip.inn = hist_decl.inn and hist_decl.kpp is null
      where hist_decl.declaration_version_id = p_decl_version_id;
    end if;

    root_elmt := xmldom.createElement(doc, 'Истреб93.1');
    xmldom.setAttribute(root_elmt, 'КодНО', p_sono_code);
    xmldom.setAttribute(root_elmt, 'РегНомДек', p_seod_decl_regnum);
    xmldom.setAttribute(root_elmt, 'УчНомИстреб', v_document_id);
    xmldom.setAttribute(root_elmt, 'КодНОИсполн', v_taxpr_code_no);
    xmldom.setAttribute(root_elmt, 'ТипИнф', 'TAX3EXCH_NDS2_CAM_03');
    xmldom.setAttribute(root_elmt, 'xsi:noNamespaceSchemaLocation', 'TAX3EXCH_NDS2_CAM_03_01.xsd');
    xmldom.setAttribute(root_elmt, 'xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
    root_node := xmldom.appendChild(main_node, xmldom.makeNode(root_elmt));

    source_elmt := xmldom.createElement(doc, 'СведИст');
    source_node := xmldom.appendChild(root_node, xmldom.makeNode(source_elmt));
    if v_taxpr_kpp is not null then
      taxpr_elmt := xmldom.createElement(doc, 'ОргИст');
      xmldom.setAttribute(taxpr_elmt, 'ИННЮЛ', v_taxpr_inn);
      xmldom.setAttribute(taxpr_elmt, 'КПП', v_taxpr_kpp);
      xmldom.setAttribute(taxpr_elmt, 'НаимОрг', v_ul_name);
      taxpr_node := xmldom.appendChild(source_node, xmldom.makeNode(taxpr_elmt));
    else
      taxpr_elmt := xmldom.createElement(doc, 'ФЛИст');
      xmldom.setAttribute(taxpr_elmt, 'ИННФЛ', v_ul_name);
      taxpr_node := xmldom.appendChild(source_node, xmldom.makeNode(taxpr_elmt));

      fio_elmt := xmldom.createElement(doc, 'ФИО');
      xmldom.setAttribute(fio_elmt, 'Фамилия', v_taxpr_inn);
      xmldom.setAttribute(fio_elmt, 'Имя', v_taxpr_kpp);
      xmldom.setAttribute(fio_elmt, 'Отчество', v_ul_name);
      fio_node := xmldom.appendChild(taxpr_node, xmldom.makeNode(fio_elmt));

    end if;
    v_reclaim_elem_name := 'Истреб93.1Док';
    v_schema_name := XSD_NDS2_CAM_03;
    v_doc_type := 3;
  end if;

  select t.PERIOD, t.OTCHETGOD
  into v_decl_period, v_decl_year
  from v$askdekl t
  where t.zip = p_decl_version_id;

  for invLine in
  (
    with t_invoices as
    (
      select
        case
          when (queue.for_stage = 4 and p_knp_closed = 0) or (queue.for_stage = 5 and p_knp_closed = 1) then dis.invoice_rk
          when (queue.for_stage = 5 and p_knp_closed = 0) or (queue.for_stage = 4 and p_knp_closed = 1) then dis.invoice_contractor_rk
        end as row_key
      from seod_data_queue queue
      inner join sov_discrepancy dis on dis.id = queue.discrepancy_id
      where queue.declaration_reg_num = p_seod_decl_regnum
          and queue.for_stage in (4, 5)
          and queue.ref_doc_id is null
    )
    select distinct si.*
    from stage_invoice si
    inner join t_invoices on t_invoices.row_key = si.row_key
  )
  loop
    if RECLAIM_BUILD(doc, root_node, invLine, v_decl_period, v_decl_year, v_reclaim_elem_name) then
      insert into doc_invoice(doc_id, invoice_row_key, invoice_chapter) values(v_document_id, invLine.Row_Key, invLine.Chapter);

      update seod_data_queue queue
      set queue.ref_doc_id = v_document_id
      where queue.ref_doc_id is null
        and queue.for_stage in (4, 5)
        and queue.discrepancy_id in
        (
          select id
          from sov_discrepancy
          where invLine.row_key =
            case
              when (queue.for_stage = 4 and p_knp_closed = 0) or (queue.for_stage = 5 and p_knp_closed = 1) then invoice_rk
              when (queue.for_stage = 5 and p_knp_closed = 0) or (queue.for_stage = 4 and p_knp_closed = 1) then invoice_contractor_rk
            end
        );
    else
      exit;
    end if;
  end loop;

  docXml := dbms_xmldom.getXmlType(doc);
  docXmlValidation := xmltype(docXml.getClobVal());

  if docXmlValidation.isSchemaValid(v_schema_name) = 0 then
     v_status := 3;
     UTL_LOG('CREATE_DECLARATION_RECLAIM:', -1, 'XSD validation error', v_document_id);
  end if;

  insert into doc(doc_id, ref_entity_id, doc_type, doc_kind, sono_code, create_date, status, status_date, document_body)
  values (v_document_id, p_seod_decl_regnum, v_doc_type, v_doc_type, p_sono_code, sysdate, v_status, trunc(sysdate), docXml.getClobVal());

  xmldom.freeDocument(doc);
  dbms_session.modify_package_state(dbms_session.free_all_resources);
end;

procedure P$EXCLUDE_DECLARATIONS
as
pragma autonomous_transaction;
begin

NDS2$SYS.P$GATHER_TABLE_STATS(p_table_name => 'SELECTION_DISCREPANCY');
NDS2$SYS.P$GATHER_TABLE_STATS(p_table_name => 'SEOD_DECLARATION');
NDS2$SYS.P$GATHER_TABLE_STATS(p_table_name => 'SELECTION');
NDS2$SYS.P$GATHER_TABLE_STATS(p_table_name => 'SELECTION_DECLARATION_EXCLUDED', p_force => true);
NDS2$SYS.P$GATHER_TABLE_STATS(p_table_name => 'SELECTION_EXCLUDE_LOG', p_force => true);

begin
   insert /*+APPEND*/ into SELECTION_DECLARATION_EXCLUDED (
        SELECTION_ID
       ,ZIP
       ,EXCLUDED_AT
       ,REASON_ID)
   select distinct
        s.id
       ,sd.zip
       ,SYSDATE
       ,1
   from
        SELECTION s
        inner join SELECTION_DISCREPANCY sd on
             sd.selection_id = s.id and sd.is_in_process = 1
        inner join V$ASKDEKL decl_mc on
             decl_mc.zip = sd.zip and decl_mc.PriznAktKorr = 1
   where s.status = 4 and decl_mc.PriznAktKorr is null
   and not exists ( select 1 from SELECTION_DECLARATION_EXCLUDED ex2 where ex2.selection_id = s.id and ex2.reason_id = 1);
   commit;
end;

NDS2$SYS.P$GATHER_TABLE_STATS(p_table_name => 'SELECTION_DECLARATION_EXCLUDED', p_force => true);

begin
   insert /*+APPEND*/ into SELECTION_DECLARATION_EXCLUDED (
        SELECTION_ID
       ,ZIP
       ,EXCLUDED_AT
       ,REASON_ID)
   select distinct
        s.id
       ,sd.zip
       ,SYSDATE
       ,2
   from
        SELECTION s
        inner join SELECTION_DISCREPANCY sd on
             sd.selection_id = s.id and sd.is_in_process = 1
        inner join V$ASKDEKL decl_mc on
             decl_mc.zip = sd.zip and decl_mc.PriznAktKorr = 1
        left join NDS2_SEOD.SEOD_DECLARATION decl_seod on
             decl_mc.IDFAJL = decl_seod.id_file
             and decl_mc.PERIOD = decl_seod.TAX_PERIOD
             and decl_mc.OTCHETGOD = decl_seod.FISCAL_YEAR
             and decl_mc.INNNP = decl_seod.INN
             and decl_mc.NOMKORR = decl_seod.CORRECTION_NUMBER
   where s.status = 4 and decl_seod.decl_reg_num is null
   and not exists ( select 1 from SELECTION_DECLARATION_EXCLUDED ex2 where ex2.selection_id = s.id and ex2.reason_id = 2);
   commit;
end;

NDS2$SYS.P$GATHER_TABLE_STATS(p_table_name => 'SELECTION_DECLARATION_EXCLUDED', p_force => true);

begin
   -- Добавляем список НД с закрытыми КНП
   insert  into SELECTION_DECLARATION_EXCLUDED (
        SELECTION_ID
       ,ZIP
       ,EXCLUDED_AT
       ,REASON_ID
       ,REG_NUMBER
       ,SONO_CODE)
   select distinct
        s.id
       ,sd.zip
       ,SYSDATE
       ,3
       ,decl_seod.id
       ,decl_seod.sono_code
   from
        SELECTION s
        inner join SELECTION_DISCREPANCY sd on
             sd.selection_id = s.id and sd.is_in_process = 1
        inner join V$ASKDEKL decl_mc on
             decl_mc.zip = sd.zip and decl_mc.PriznAktKorr = 1
        inner join NDS2_SEOD.SEOD_DECLARATION decl_seod on
             decl_mc.IDFAJL = decl_seod.id_file
             and decl_mc.PERIOD = decl_seod.TAX_PERIOD
             and decl_mc.OTCHETGOD = decl_seod.FISCAL_YEAR
             and decl_mc.INNNP = decl_seod.INN
             and decl_mc.NOMKORR = decl_seod.CORRECTION_NUMBER
        left join NDS2_SEOD.SEOD_KNP knp on
             knp.declaration_reg_num = decl_seod.decl_reg_num
             and knp.ifns_code = decl_seod.sono_code
   where s.status = 4 and knp.completion_date is not null
   and not exists ( select 1 from SELECTION_DECLARATION_EXCLUDED ex2 where ex2.selection_id = s.id  and ex2.reason_id = 3);
   commit;
end;

NDS2$SYS.P$GATHER_TABLE_STATS(p_table_name => 'SELECTION_DECLARATION_EXCLUDED', p_force => true);

   for line in (
       select
       s.*
       from
       selection s
       where s.status = 4
       and not exists
       (
        select 1 from SELECTION_EXCLUDE_LOG sel
        where sel.selection_id = s.id
       )
       ) loop
   begin
     update SELECTION_DISCREPANCY sd
     set sd.IS_IN_PROCESS = 0
     where sd.is_in_process = 1
     and sd.selection_id = line.id
     and exists(
           select 1
           from SELECTION s
             join SELECTION_DECLARATION_EXCLUDED sdex on s.id = sdex.selection_id
           where
             s.id = line.id
             and sdex.zip = sd.zip
     );

     update SELECTION_DECLARATION sd
     set sd.IS_IN_PROCESS = 0
     where sd.is_in_process = 1
     and sd.selection_id = line.id
     and exists(
           select 1 from SELECTION s
             join SELECTION_DECLARATION_EXCLUDED sdex on s.id = sdex.selection_id
           where
             s.id = line.id
             and sdex.zip = sd.zip
     );
     insert into SELECTION_EXCLUDE_LOG(DT_PROCESED, SELECTION_ID) values(sysdate, line.id);
     commit;
   end;
   end loop;
commit;
exception when others then
    rollback;
    UTL_LOG('P$EXCLUDE_DECLARATIONS failed', sqlcode, substr(sqlerrm, 256));
end;

FUNCTION DAILY_JOB_IS_RUNNING RETURN BOOLEAN
AS
v_runjob number;
BEGIN
  select count(1) into v_runjob from all_scheduler_running_jobs where job_name = 'J$DAILY_JOB';
  return v_runjob > 0;
END;

--######################################################
--Сбор данных для отправки астотребования первой стороне
--######################################################

PROCEDURE COLLECT_QUEUE
as
pragma autonomous_transaction;
v_count number := 0;
begin

/*исключим из выборок рег номера*/
  P$EXCLUDE_DECLARATIONS();


/*добавим декларации по отправленным выборкам в очередь*/
  merge into ASK_INVOICE_DECL_QUEUE Q
  using  (
    select distinct
       sd.zip as zip
      ,dh.reg_number as SEOD_DECL_ID
      ,dh.SONO_CODE_SUBMITED as SOUN_CODE
      ,dh.kpp as kpp
      ,(nvl(s.discrepancy_stage, 1) + 1) as for_stage
      ,0 as is_processed
      ,dh.inn_declarant as inn
      ,dh.kpp_effective as kpp_effective
      ,dh.period_code as period_code
      ,dh.fiscal_year as year
    from selection_discrepancy sd
      inner join selection s on s.id = sd.selection_id
      inner join declaration_history dh on dh.zip = sd.zip
    where s.status = 4
          and sd.is_in_process = 1
          --and nvl(s.discrepancy_stage, 1) = 1
  ) T
  on (
     T.ZIP = Q.ZIP
     and T.SEOD_DECL_ID = Q.SEOD_DECL_ID
     and T.SOUN_CODE = Q.SOUN_CODE
     and nvl(T.KPP, '-') = nvl(Q.KPP, '-')
     and T.FOR_STAGE = Q.FOR_STAGE
	 and Q.IS_PROCESSED = 0
  )
  when not matched then
    insert (Q.ZIP, Q.SEOD_DECL_ID, Q.SOUN_CODE, Q.KPP, Q.FOR_STAGE, Q.IS_PROCESSED, Q.CREATE_DATE,
            Q.INN, Q.KPP_EFFECTIVE, Q.PERIOD_CODE, Q.YEAR)
    values (T.ZIP, T.SEOD_DECL_ID, T.SOUN_CODE, T.KPP, T.FOR_STAGE, T.IS_PROCESSED, sysdate,
            T.INN, T.KPP_EFFECTIVE, T.PERIOD_CODE, T.YEAR);

/*Обновим статусы выборок для дальнейшей обработки формирования АТ по СФ*/

  update selection s
  set status = 10
  where s.status = 4
  --  and nvl(s.discrepancy_stage, 1) = 1
  and not exists (
     select 1 from selection_discrepancy sd
        left join  ASK_INVOICE_DECL_QUEUE aidq on sd.zip = aidq.zip
     where sd.selection_id = s.id
           and sd.is_in_process = 1
           and aidq.zip is null
  );

  commit;
  exception when others then
    rollback;
    UTL_LOG('COLLECT_QUEUE failed', sqlcode, substr(sqlerrm, 256));
end;

PROCEDURE SEND_CLAIM_KS
as
v_runjob number;
pragma autonomous_transaction;
begin
  select count(1) into v_runjob from all_scheduler_running_jobs where job_name = 'J$CLAIM_KS';
  if v_runjob = 1 then
  for ready_cr_decl in
  (
      select *
      from
      (select
         q.id,
         q.sono_code,
         q.decl_reg_num
      from CONTROL_RATIO_QUEUE q
      where q.is_sent = 0
	    and q.decl_reg_num is not null
      order by id)
      where rownum < 5000
  )
  loop
    CREATE_DECL_KS_CLAIM
    (
      ready_cr_decl.sono_code,
      ready_cr_decl.decl_reg_num,
      ready_cr_decl.id
    );

   update CONTROL_RATIO_QUEUE
     set is_sent = 1
     where id = ready_cr_decl.id;

  end loop;
  end if;
  commit;
end;

PROCEDURE PROCESS_QUEUE
as
pragma autonomous_transaction;
v_success boolean;
v_status_code number(1);
begin

  for decl_line in
  (
    select * from (
      select
        t.zip
       ,t.seod_decl_id
       ,t.soun_code
       ,t.for_stage
       ,case when knp.completion_date is null then 0 else 1 end as knp_closed
      from ASK_INVOICE_DECL_QUEUE t
      left join (
          select
             max(completion_date) as completion_date
            ,declaration_reg_num
            ,ifns_code
          from NDS2_SEOD.SEOD_KNP
          group by declaration_reg_num,ifns_code) knp
                on    knp.declaration_reg_num = t.seod_decl_id
                  and knp.ifns_code = t.soun_code
      where t.is_processed = 0
      order by t.create_date desc
    ) T where rownum < 100
  )
  loop
    if DAILY_JOB_IS_RUNNING() then
      exit;
	end if;

    if decl_line.knp_closed = 0 then

      v_success := true;

      if decl_line.for_stage = 2 then
        CREATE_DECL_CLAIM_SIDE_1 (
           decl_line.soun_code
          ,decl_line.seod_decl_id
          ,decl_line.zip
          ,v_success);
      end if;

      if decl_line.for_stage = 3 then
        CREATE_DECL_CLAIM_SIDE_2 (
           decl_line.soun_code
          ,decl_line.seod_decl_id
          ,decl_line.zip
          ,v_success);
      end if;
      if v_success then v_status_code := 1; else v_status_code := 3; end if;
    else
      v_status_code := 4;
    end if;

    update ASK_INVOICE_DECL_QUEUE
    set is_processed = v_status_code
    where zip = decl_line.zip
          and seod_decl_id = decl_line.seod_decl_id
          and soun_code = decl_line.soun_code
          and for_stage = decl_line.for_stage;
  end loop;

  commit;
  exception when others then
    rollback;
    UTL_LOG('PROCESS_QUEUE failed', sqlcode, substr(sqlerrm, 256));
  null;
end;

PROCEDURE PROCESS_ASK_4
as
  doc xmldom.DOMDocument;
  docXml xmltype;
  main_node xmldom.DOMNode;
  root_node xmldom.DOMNode;
  user_node xmldom.DOMNode;
  item_node xmldom.DOMNode;
  root_elmt xmldom.DOMElement;
  item_elmt xmldom.DOMElement;
  item_text xmldom.DOMText;
  docXmlValidation xmltype;
  v_status number(1):= 1;
  pragma autonomous_transaction;
begin
  for line in (select
  dh.soun_code,
  dh.SEOD_DECL_ID
    from v$declaration dh
    left join doc d on d.ref_entity_id = dh.SEOD_DECL_ID and d.doc_type = 4
    where dh.TOTAL_DISCREP_COUNT = 0
    and dh.CONTROL_RATIO_COUNT = 0
    and to_number(nvl(dh.CORRECTION_NUMBER, '0')) > 0
    and d.doc_id is null)
  loop
    doc := xmldom.newDOMDocument();
    main_node := xmldom.makeNode(doc);
    root_elmt := xmldom.createElement(doc, 'ДанныеРсхжд');
    xmldom.setAttribute(root_elmt, 'КодНО', line.soun_code);
    xmldom.setAttribute(root_elmt, 'РегНомДек', line.SEOD_DECL_ID);
    xmldom.setAttribute(root_elmt, 'ТипИнф', 'TAX3EXCH_NDS2_CAM_04');
    xmldom.setAttribute(root_elmt, 'СвНалРсхжд', 2);
    xmldom.setAttribute(root_elmt, 'xsi:noNamespaceSchemaLocation', 'TAX3EXCH_NDS2_CAM_04_01.xsd');
    xmldom.setAttribute(root_elmt, 'xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
    root_node := xmldom.appendChild(main_node, xmldom.makeNode(root_elmt));

    docXml := dbms_xmldom.getXmlType(doc);
    docXmlValidation := xmltype(docXml.getClobVal());

    if docXmlValidation.isSchemaValid(schurl => XSD_NDS2_CAM_04) = 0 then
      v_status := 3;
    end if;

    insert into doc(doc_id, ref_entity_id, doc_type, doc_kind, sono_code, create_date, status, status_date, document_body)
    values (SEQ_DOC.NEXTVAL, line.SEOD_DECL_ID, 4, 4, line.soun_code, sysdate, v_status, trunc(sysdate), docXml.getClobVal());

  end loop;
  commit;
  xmldom.freeDocument(doc);
  dbms_session.modify_package_state(dbms_session.free_all_resources);
exception when others then
  rollback;

end;

PROCEDURE DEMO
as
v_runjob number;
v_total number;
begin
  select count(1) into v_runjob from all_scheduler_running_jobs where job_name = 'J$DAILY_JOB';
  if v_runjob = 0 then
    COLLECT_QUEUE;
    select count(1) into v_total from ask_invoice_decl_queue where is_processed = 0;
    while v_total > 0 loop
      select count(1) into v_runjob from all_scheduler_running_jobs where job_name = 'J$DAILY_JOB';
      if v_runjob = 0 then
        PROCESS_QUEUE;
        select count(1) into v_total from ask_invoice_decl_queue where is_processed = 0;
      else
        select 0 into v_total from dual;
      end if;
    end loop;
  end if;
end;

procedure P$UPDATE_CONTROL_RATIO_QUEUE
as
v_last_load_id number;
v_actual_load_id number;
v_row_cnt number;
begin

  merge into NDS2_MRR_USER.CONTROL_RATIO_QUEUE Q
  using
  (
      select distinct d.ID 
            ,d.KODNO as SONO_CODE 
            ,sd.decl_reg_num
        from NDS2_MRR_USER.V$ASKDEKL d
        join NDS2_MRR_USER.CONTROL_RATIO_QUEUE b 
          on b.id = d.id and b.sono_code = d.KODNO
        join NDS2_SEOD.SEOD_DECLARATION sd 
          on sd.inn = d.innnp
          and F$GET_EFFECTIVE_KPP(sd.inn, sd.kpp) = F$GET_EFFECTIVE_KPP(d.INNNP, d.KPPNP)
          and sd.correction_number = d.NOMKORR
          and sd.id_file = d.IDFAJL
          and sd.sono_code = d.KODNO
        join NDS2_SEOD.SEOD_REG_NUMBER r on r.sono_code = sd.sono_code and r.decl_reg_num = sd.decl_reg_num
        where b.decl_reg_num is null
          and b.is_sent = 0
  ) T
  on
  (
    Q.id = T.id
  )
  when matched then
    update set Q.decl_reg_num = T.decl_reg_num, Q.is_sent = 0;

  v_row_cnt := SQL%ROWCOUNT;

  commit;
  
  if v_row_cnt > 0 then
    UTL_LOG
      (
        v_source => 'P$UPDATE_CONTROL_RATIO_QUEUE',
        v_code => 1,
        v_msg => 'Обновлены ранее загруженные КС по НД в кол-ве '||v_row_cnt||' строк',
        v_entity_id => null
      );
  end if;
  
  begin
    select nvl(max(LOAD_ID), 0) into v_last_load_id from CONTROL_RATIO_QUEUE_LOG;
    exception when no_data_found then
      v_last_load_id := 0;
  end;

  select nvl(max(op.NomerOp), 0) into v_actual_load_id from v$askoperatsiya op
  where op.NomerOp > v_last_load_id and op.VidOp = 1;

  if v_last_load_id < v_actual_load_id then

    merge into NDS2_MRR_USER.CONTROL_RATIO_QUEUE Q
    using
    (
      select
        MC_DATA.ID,
        MC_DATA.sono_code,
        ssd.decl_reg_num
      from
      (
       select distinct
        d.ID,
        d.INNNP as INN,
        F$GET_EFFECTIVE_KPP(d.INNNP, d.KPPNP) as KPP_EFFECTIVE,
        d.PERIOD as PERIOD_CODE,
        d.OTCHETGOD as FISCAL_YEAR,
        d.NOMKORR as CORRECTION_NUMBER,
        d.IDFAJL as ID_FILE,
        d.KODNO as SONO_CODE
        from NDS2_MRR_USER.V$ASKDEKL d
        inner join NDS2_MRR_USER.V$ASKKONTRSOONTOSH ks on d.ID = ks.IdDekl
        where d.IdZagruzka > v_last_load_id and d.IdZagruzka <= v_actual_load_id
          and ks.DeleteDate is null 
          and d.priznaktkorr <> 2 
      ) MC_DATA
      left join
	  (
	  select sd.inn, F$GET_EFFECTIVE_KPP(sd.inn, sd.kpp) as kpp_effective, sd.correction_number, sd.id_file, sd.sono_code, sd.decl_reg_num
	  from NDS2_SEOD.SEOD_DECLARATION sd
	       join NDS2_SEOD.SEOD_REG_NUMBER r on r.sono_code = sd.sono_code and r.decl_reg_num = sd.decl_reg_num
	  ) ssd on ssd.inn = MC_DATA.inn
           and ssd.kpp_effective = MC_DATA.kpp_effective
           and ssd.correction_number = MC_DATA.correction_number
           and ssd.id_file = MC_DATA.id_file
           and ssd.sono_code = MC_DATA.sono_code
    ) T
    on
    (
      Q.id = T.id
    )
    when not matched then
      insert values(T.id, T.sono_code, T.decl_reg_num, 0);

    v_row_cnt := SQL%ROWCOUNT;

    insert into CONTROL_RATIO_QUEUE_LOG values (v_actual_load_id, v_row_cnt, sysdate);

    commit;

    UTL_LOG
      (
        v_source => 'P$UPDATE_CONTROL_RATIO_QUEUE',
        v_code => 1,
        v_msg => 'Обработаны КС по НД на период загрузки c '||v_last_load_id||' по '||v_actual_load_id||' строк '||v_row_cnt,
        v_entity_id => null
      );
  else
    UTL_LOG
      (
        v_source => 'P$UPDATE_CONTROL_RATIO_QUEUE',
        v_code => 1,
        v_msg => 'не обнаружено новых загрузок',
        v_entity_id => null
      );
  end if;
  exception when others then
    rollback;
    UTL_LOG
      (
        v_source => 'P$UPDATE_CONTROL_RATIO_QUEUE',
        v_code => 3,
        v_msg => substr(sqlerrm, 1, 512),
        v_entity_id => null
      );

end;

procedure GET_DOC_INFO
(
  pDocId IN DOC.DOC_ID%type,
  pRezCursor OUT SYS_REFCURSOR
)
as
begin

  OPEN pRezCursor FOR
  SELECT
      dc.*,
      d.inn_declarant as TaxPayerInn,
      tp.name_full as TaxPayerName,
      dt.description as doc_type_name,
      dst.description as statusDesc,
      d.zip as declaration_version_id
  FROM DOC dc
      inner join declaration_history d 
        on ((dc.zip is not null and d.zip = dc.zip) 
         or (dc.zip is null and dc.ref_entity_id = d.reg_number and dc.sono_code = d.sono_code_submited)) 
        and d.type_code = 0
      left outer join tax_payer tp on tp.inn = d.inn_declarant and tp.kpp_effective = d.kpp_effective
      inner join doc_type dt on dt.id = dc.doc_type
      inner join doc_status dst on dst.id = dc.status and dst.doc_type = dc.doc_type
  WHERE dc.doc_id = pDocId;

end;

procedure p$GET_CLAIMS_BY_DECLARATION
(
  pInnDeclarant in varchar2,
  pInnContractor in varchar2,
  pKppEffective in varchar2,
  pPeriodEffective in number,
  pYear in varchar2,
  pCursor out SYS_REFCURSOR
)
as
begin
	OPEN pCursor FOR
	WITH 
	FAILED_KS as (
		select 
			d.doc_id
			,count(*) as failed_KS_cnt
		from doc d
        join control_ratio_doc crd 
            on crd.doc_id = d.doc_id
        join v$askkontrsoontosh cr
            on cr.id = crd.cr_id
            and cr.Vypoln <> 1
		group by d.doc_id
	)
	SELECT
		DECL.reg_number						as DeclarationRegistrationNumber,
		DOC.sono_code						as SonoCode,
		DOC.doc_id							as "Id",
		DOC.Doc_Type						as "Type",
		decode(DOC.Seod_Accepted, 1, 1, 0)	as SeodAccepted,
		DOC.EXTERNAL_DOC_NUM				as "Number",
		DOC.EXTERNAL_DOC_DATE				as "Date",
		DOC.create_date						as CreatedAt,
		DOC.seod_accept_date				as SeodDate,
		DOC_STATUS.DESCRIPTION				as STATUS,
		DOC.close_reason					as CloseReason,
		case	when DOC.doc_type = CLAIM_INVOICE_TYPE
                    and doc.discrepancy_count > 0
                then 1 
                when DOC.doc_type = CLAIM_CONTROL_RATIO_TYPE 
                    and fks.failed_KS_cnt > 0 
                then 1
				else 0  end
											as HasDiscrepancies
	from DOC doc
	join DECLARATION_HISTORY decl 
		on DECL.REG_NUMBER = doc.ref_entity_id 
		and decl.SONO_CODE_SUBMITED = doc.SONO_CODE
	join DOC_TYPE 
		on DOC_TYPE.ID = DOC.Doc_Type
	left join DOC_STATUS 
		on DOC_STATUS.ID = DOC.STATUS 
		and DOC_STATUS.DOC_TYPE = DOC.DOC_TYPE
	left join FAILED_KS fks
		on doc.doc_id = fks.doc_id
	where DECL.INN_DECLARANT = pInnDeclarant
		and (pInnContractor is not null 
			and DECL.INN_CONTRACTOR = pInnContractor 
			or pInnContractor is null)
		and DECL.KPP_EFFECTIVE = pKppEffective 
		and DECL.TYPE_CODE = 0
		and DECL.PERIOD_EFFECTIVE = pPeriodEffective 
		and DECL.FISCAL_YEAR = pYear
		and ((DOC.Doc_Type in (RECLAIM_TYPE, CLAIM_CONTROL_RATIO_TYPE)) 
		  or (DOC.doc_type = CLAIM_INVOICE_TYPE and DOC.knp_sync_status > 0));
	end;
END;
/
