﻿create or replace package NDS2_MRR_USER.PAC$MVIEW_MANAGER
as
procedure REFRESH_DECLARATONS;
procedure P$UPDATE_DECL_ACTIVE;
procedure P$UPDATE_DECL_ACTIVE_SEARCH;
procedure P$UPDATE_ASK_DECL_AND_JRNL;
/*
Обновление таблицы DECLARATION_HISTORY
*/
procedure UPDATE_DECLARATION_HISTORY;

procedure UPDATE_MACRO_TAXPAYER_INFO;

procedure P$UPDATE_DECLARATION_KNP;

procedure P$UPDATE_DECL_KNP_STATUS;

/*Обновление таблицы */
procedure P$UPDATE_TABLE
(
    p_temp_table_name varchar2,
    p_backup_table_name varchar2,
    p_original_table_name varchar2,
    p_sorce_view varchar2,
    p_index_key varchar2);

/*Сброс назначений и ПЗ при перерегистрации*/
procedure P$DROP_ASSIGN_ON_SONO_CHANGED;

procedure P$CREATE_VDECLARATION;
procedure P$CREATE_VINSP_DECLARATION;

end;
/
create or replace package body NDS2_MRR_USER.PAC$MVIEW_MANAGER
as

DECL_ACTIVE_NAME constant varchar2(32) := 'DECLARATION_ACTIVE';
DECL_ACTIVE_NAME_SEARCH constant varchar2(32) := 'DECLARATION_ACTIVE_SEARCH';

VIEW_ACTIVE_BUILD_NAME constant varchar2(32) := 'V$DECLARATION_ACTIVE_BUILD';
VIEW_ACTIVE_SEARCH_BUILD_NAME constant varchar2(32) := 'V$DECL_ACTIVE_SEARCH_BUILD';

procedure log(p_msg varchar2, p_type number)
as
pragma autonomous_transaction;
begin

  dbms_output.put_line(p_type||'-'||p_msg);

  insert into NDS2_MRR_USER.SYSTEM_LOG(ID, TYPE, SITE, ENTITY_ID, MESSAGE_CODE, MESSAGE, LOG_DATE)
  values(Seq_Sys_Log_Id.Nextval, p_type, 'PAC$MVIEW_MANAGER', null, 1, p_msg, sysdate);
  commit;
end;

procedure log_notification(p_msg varchar2)
as
begin
     log(p_msg, 0);
end;

procedure log_error(p_msg varchar2)
as
begin
     log(p_msg, 3);
end;

procedure compile_dependency( p_mviewName varchar2 )
 as
begin
  for line in (select * from all_mviews where owner = 'NDS2_MRR_USER' and mview_name = p_mviewName and compile_state <> 'VALID')
  loop
    execute immediate 'alter materialized view NDS2_MRR_USER.'||p_mviewName||' compile';
  end loop;
end;

/*выполнение динамического однострочного запроса*/
procedure execute_short_query(p_sql in varchar2)
as
begin
  execute immediate p_sql;
exception when others then log_error(substr(sqlerrm, 1, 128)||':'||p_sql);
end;

procedure close_cursor_handle(p_hndl in out int)
  as
begin
  if DBMS_SQL.is_open(p_hndl) then
       DBMS_SQL.close_cursor(p_hndl);
  end if;
end;

procedure P$UPDATE_TABLE
(
    p_temp_table_name varchar2,
    p_backup_table_name varchar2,
    p_original_table_name varchar2,
    p_sorce_view varchar2,
    p_index_key varchar2
)
as
v_rowcount      number;
v_temp_table_exists number(1);
begin
  -- Отпишемся в лог
  log('Начало обновления таблицы '||p_original_table_name||'.', 0);

  execute_short_query('drop table NDS2_MRR_USER.'||p_temp_table_name);
  execute_short_query('drop table NDS2_MRR_USER.'||p_backup_table_name);

  select count(1) into v_temp_table_exists
  from all_tables
  where owner = 'NDS2_MRR_USER'
  and table_name in (p_temp_table_name, p_backup_table_name);

  if v_temp_table_exists = 0 then

    execute_short_query('create table '||p_temp_table_name||' parallel 16 as select * from '||p_sorce_view);
    -- Отписываемся в Лог
    log('(2) Сформирована новая версия таблицы: '||p_temp_table_name||'.', 0);

    execute immediate 'alter table '||p_temp_table_name||' noparallel';

    -- В этом месте у нас есть уже индексы с теми же именами, но на старой таблице.
    -- Поэтому надо использовать Ultimate_...

    PAC$INDEX_MANAGER.P$ULTIMATE_VALIDATE_INDEXES(p_temp_table_name, p_index_key);
    log('(7) Созданы индексы '||p_temp_table_name,0);

    dbms_stats.gather_table_stats(
             ownname => 'NDS2_MRR_USER',
             tabname => p_temp_table_name,
             cascade => true,
             degree => 16
			);
    log('(6) Собрана статистика '||p_temp_table_name,0);

    -- подменяем таблицы
    -- -- Переименовываем постоянную в DECLARATION_HISTORY_BAK
    execute_short_query('alter table '||p_original_table_name||' rename to '||p_backup_table_name);
    log('(4) Выполнена подмена таблицы '||p_original_table_name||' на '||p_backup_table_name||'.', 0);

    -- -- Переименовываем временную в DECLARATION_HISTORY
    execute_short_query('alter table '||p_temp_table_name||' rename to '||p_original_table_name);
    log('(5) Выполнена подмена таблицы '||p_temp_table_name||' на '||p_original_table_name||'.', 0);

	execute immediate 'Alter session set ddl_lock_timeout = 300';
    -- Удаляем старую постоянную таблицу
    execute_short_query('drop table NDS2_MRR_USER.'||p_backup_table_name);
	execute immediate 'Alter session set ddl_lock_timeout = 0';
    log('(8) Удалена старая: '||p_backup_table_name||'.', 0);

    -- Отписываемся по результатам
    log('(9) Закончено обновление таблицы '||p_original_table_name||'.', 0);
  end if;
exception when others then
  log_error('UPDATE_'||p_original_table_name||':('||sqlcode||'):'||substr(sqlerrm, 1, 256));
  execute immediate 'Alter session set ddl_lock_timeout = 0';
end;

procedure P$BUILD_REORG_AGGREGATE as
 v_date DATE;
begin
  execute immediate 'Alter session set ddl_lock_timeout = 300';
  begin
    execute immediate 'drop table NDS2_MRR_USER.REORG_TMP';
    exception when others then null;
  end;
  execute immediate 'Alter session set ddl_lock_timeout = 0';
  begin
    select cfg.value into v_date from configuration cfg where parameter = 'TaxPayerSyncDate';
    exception when others then v_date := TO_DATE('01.01.2014', 'dd.mm.yyyy');
  end;

  execute immediate 'create table NDS2_MRR_USER.REORG_TMP as '
    ||'select * from NDS2_MRR_USER.V$REORG_TMP_BUILD t where t.insert_date > TO_DATE('''||TO_CHAR(v_date)||''', ''dd.mm.yyyy'')';

  merge into configuration cfg
  using (select sysdate as dt, 'TaxPayerSyncDate' as parameter from dual) src on (src.parameter = cfg.parameter)
  when matched then
    update set cfg.value = src.dt
  when not matched then
    insert (parameter, value, default_value) values (src.parameter, src.dt, src.dt);

  commit;
end;

procedure P$UPDATE_TAX_PAYER
as
begin

 P$UPDATE_TABLE(
  p_temp_table_name => 'TAX_PAYER_TMP'
 ,p_backup_table_name => 'TAX_PAYER_BAK'
 ,p_original_table_name => 'TAX_PAYER'
 ,p_sorce_view => 'V$TAX_PAYER_BUILD'
 ,p_index_key => 'TAX_PAYER');

end;

procedure P$UPDATE_DECLARATION_KNP
as
begin

 P$UPDATE_TABLE(
  p_temp_table_name => 'DECLARATION_KNP_TMP'
 ,p_backup_table_name => 'DECLARATION_KNP_BAK'
 ,p_original_table_name => 'DECLARATION_KNP'
 ,p_sorce_view => 'V$DECLARATION_KNP_BUILD'
 ,p_index_key => 'DECLARATION_KNP');

end;

procedure P$UPDATE_KNP_DISCREPANCY_DECL as
begin
   P$UPDATE_TABLE(
           p_temp_table_name => 'KNP_DISCREPANCY_DECL_TMP'
          ,p_backup_table_name => 'KNP_DISCREPANCY_DECL_BAK'
          ,p_original_table_name => 'KNP_DISCREPANCY_DECLARATION'
          ,p_sorce_view => 'v$knp_discrepancy_decl_build'
          ,p_index_key => 'KNP_DISCREPANCY_DECLARATION');
end;

procedure P$UPDATE_KNP_DECL_SUMMARY as
begin
   P$UPDATE_TABLE(
           p_temp_table_name => 'KNP_DECLARATION_SUMMARY_TMP'
          ,p_backup_table_name => 'KNP_DECLARATION_SUMMARY_BAK'
          ,p_original_table_name => 'KNP_DECLARATION_SUMMARY'
          ,p_sorce_view => 'V$KNP_DECL_SUMMARY_BUILD'
          ,p_index_key => 'KNP_DECLARATION_SUMMARY');
end;

procedure P$UPDATE_KNP_RELATION_SUMMARY as
begin
   P$UPDATE_TABLE(
           p_temp_table_name => 'KNP_RELATION_SUMMARY_TMP'
          ,p_backup_table_name => 'KNP_RELATION_SUMMARY_BAK'
          ,p_original_table_name => 'KNP_RELATION_SUMMARY'
          ,p_sorce_view => 'v$knp_rel_summary_build'
          ,p_index_key => 'KNP_RELATION_SUMMARY');
end;

procedure P$UPDATE_DECL_KNP_STATUS as
begin
   P$UPDATE_TABLE(
           p_temp_table_name => 'DECLARATION_KNP_STATUS_TMP'
          ,p_backup_table_name => 'DECLARATION_KNP_STATUS_BAK'
          ,p_original_table_name => 'DECLARATION_KNP_STATUS'
          ,p_sorce_view => 'V$DECLARATION_KNP_STATUS_BUILD'
          ,p_index_key => 'DECLARATION_KNP_STATUS');
end;

/*
Снимает назначение инспектора и закрывает ПЗ при смене НО*/
procedure P$DROP_ASSIGN_ON_SONO_CHANGED
as
    type t$decl_info_rec is record (
                zip number,
                inn_declarant varchar2(12),
                kpp_effective varchar2(9),
                period_effective number,
                fiscal_year varchar2(4),
                type_code number,
                clear_assignment number(1));

   type t$decl_info_col is table of t$decl_info_rec;
   col_decl_keys t$decl_info_col := t$decl_info_col();
   col_decl_zips t$decl_info_col;
   v_last_exec_param constant varchar2(40):='drop_assign_sono_changed_exec_date';
   v_exec_date date := sysdate;
   v_indexer pls_integer := 1;
   v_moved_item t$decl_info_rec;
   v_scope constant varchar2(30):='P$DROP_ASSIGN_ON_SONO_CHANGED ';
   v_date_frmt constant varchar2(25):='DD.MM.YYYY HH24:MI:SS';

begin
    log_notification(v_scope||'Начало выполнения в '||to_char(v_exec_date,v_date_frmt));
/* Запрос выводит:
    1. Номер корректировки подлежащей сбросу ПЗ
         - определяется как неравенство кода НО корректировки коду НО самой последней корректировки
    2. Признак необходимости сброса назначения инспектора и закрытия ПЗ, выполняются все условия:
        - неравенство кода НО последней корректировки и кода корректировки
            бывшей активной на дату последнего запуска процедуры определения перерегистраций (ПОП) 8-) 
        - наличие назначения на пользователя
        * в случае если невозможно установить код НО корректировки бывшей активной на момент прощлого запуска ПОП,
          признак высчитывается как неравенство кода НО последней корректировки коду НО
          самой старой корректировки.
          Данный сценарий возможен в случае когда между запусками ПОП появляются одна или несколько коррерктировок.
          Последняя дата запуска ПОП хранится в таблице конфигурации.
    3. Ключ НД подлежащей сбросу НД и закрытию связанных с ней ПЗ
        - ИНН, КПП, Период, Год, Тип(журнал/декларация)
*/
    with
    last_call_date as (select
                            to_date(value, 'DD-MM-YYYY HH24:MI:SS' ) as lcd
                        from configuration
                        where parameter = v_last_exec_param),
    cor_active_now as
    (
        select distinct
            inn_declarant,
            kpp_effective,
            period_effective,
            fiscal_year,
            type_code,
            first_value(Sono_code_submited) over (PARTITION BY inn_declarant,
                                                                  kpp_effective,
                                                                  period_effective,
                                                                  fiscal_year,
                                                                  type_code
                                            order by insert_date desc
                                            range between unbounded preceding and unbounded following
                                            ) as Active_Sono
            ,last_value(Sono_code_submited) over (PARTITION BY inn_declarant,
                                                                  kpp_effective,
                                                                  period_effective,
                                                                  fiscal_year,
                                                                  type_code
                                            order by insert_date desc
                                            range between unbounded preceding and unbounded following
                                            ) as Active_next_to_last_call
        from DECLARATION_HISTORY
        where
            insert_date between (select lcd from last_call_date) and v_exec_date
            and is_active = '1'
    ),
    cor_active_last_call as
    (
        select distinct
            dh.inn_declarant,
            dh.kpp_effective,
            dh.period_effective,
            dh.fiscal_year,
            dh.type_code,
            first_value(dh.Sono_code_submited) over (PARTITION BY dh.inn_declarant,
                                                          dh.kpp_effective,
                                                          dh.period_effective,
                                                          dh.fiscal_year,
                                                          dh.type_code
                                                order by dh.insert_date desc
                                                range between unbounded preceding and unbounded following
                                                ) as Last_Call_Active_Sono
        from cor_active_now can
        join DECLARATION_HISTORY dh
            on can.inn_declarant = dh.inn_declarant
            and can.kpp_effective = dh.kpp_effective
            and can.period_effective = dh.period_effective
            and can.fiscal_year = dh.fiscal_year
            and can.type_code = dh.type_code
        where
            dh.insert_date <= (select lcd from last_call_date)
    ),    
    decl_last_assign as
    (
        select distinct
            da.inn_declarant
            ,da.kpp_effective
            ,da.period_effective
            ,da.fiscal_year
            ,da.declaration_type_code as type_code
            ,first_value(da.Assigned_to) over (PARTITION BY da.inn_declarant,
                                                            da.kpp_effective,
                                                            da.period_effective,
                                                            da.fiscal_year,
                                                            da.declaration_type_code
                                                    order by da.assigned_at desc
                                                    range between unbounded preceding and unbounded following
                                                    ) as Last_Assignee_Id
        from cor_active_now can
        join declaration_assignment da
             on can.inn_declarant = da.inn_declarant
            and can.kpp_effective = da.kpp_effective
            and can.period_effective = da.period_effective
            and can.fiscal_year = da.fiscal_year
            and can.type_code = da.declaration_type_code
    )

    select
        x.zip
        ,x.inn_declarant
        ,x.kpp_effective
        ,x.period_effective
        ,x.fiscal_year
        ,x.type_code
        ,case
            when (x.Active_Sono <> nvl(x.Last_Call_Active_Sono, x.Active_next_to_last_call)
                    and Last_Assignee_Id is not null)
                then 1
                else 0
            end as CLR_ASSIGN
	bulk collect into col_decl_zips
    from
    (
        select 
            dh.zip
            ,dh.inn_declarant
            ,dh.kpp_effective
            ,dh.period_effective
            ,dh.fiscal_year
            ,dh.type_code
            ,dh.Sono_code_submited as Sono  -- текущий код НО корректировки
            ,calc.Last_Call_Active_Sono     -- код НО корректировки которая была текущей на момент предыдущего запуска процедуры
            ,can.Active_Sono                -- код НО самой свежей корректировки на текущий момент
            ,can.Active_next_to_last_call   -- код НО корректировки следующей за той что была актуальной на момент пердыдущего запуска
            ,dla.Last_Assignee_Id           -- последний пользователь на которого была назначена декларация
        from cor_active_now can
        join declaration_history dh
            on can.inn_declarant = dh.inn_declarant
            and can.kpp_effective = dh.kpp_effective
            and can.period_effective = dh.period_effective
            and can.fiscal_year = dh.fiscal_year
            and can.type_code = dh.type_code
        left join cor_active_last_call calc
            on can.inn_declarant = calc.inn_declarant
            and can.kpp_effective = calc.kpp_effective
            and can.period_effective = calc.period_effective
            and can.fiscal_year = calc.fiscal_year
            and can.type_code = calc.type_code
        left join  decl_last_assign dla
             on can.inn_declarant = dla.inn_declarant
            and can.kpp_effective = dla.kpp_effective
            and can.period_effective = dla.period_effective
            and can.fiscal_year = dla.fiscal_year
            and can.type_code = dla.type_code
    ) x
    where x.Sono <> x.Active_Sono
    order by  x.inn_declarant
            ,x.kpp_effective
            ,x.period_effective
            ,x.fiscal_year
            ,x.type_code;

	log_notification(v_scope||'Найдено корректировок сменивших НО: '||to_char(col_decl_zips.count));

    if(col_decl_zips.count > 0) then
    /*
        Алгоритм перемещает элементы с установленным признаком сброса назначения инспектора в другую коллекцию.
        Перемещение происходит с отловом дубликатов ключа декларации.
        Алгоритм отлова дубликата исходит из упорядоченности исходной коллекции по ключу декларации.
    */
        for i in 1..col_decl_zips.count
        loop
            if(col_decl_zips(i).clear_assignment = 1) then
                --Фильтруем дубликаты по ключу
                if( col_decl_zips(i).inn_declarant <> v_moved_item.inn_declarant
                        or col_decl_zips(i).kpp_effective <> v_moved_item.kpp_effective
                        or col_decl_zips(i).period_effective <> v_moved_item.period_effective
                        or col_decl_zips(i).fiscal_year <> v_moved_item.fiscal_year
                        or col_decl_zips(i).type_code <> v_moved_item.type_code
                        or i = 1)
                then
                    -- Элемент не является дублем и перемещается
                    col_decl_keys.extend();
                    col_decl_keys(v_indexer) := col_decl_zips(i);
                    v_moved_item := col_decl_zips(i);
                    v_indexer := v_indexer + 1;
                end if;
                -- Удаляем элемент с установленным признаком
                col_decl_zips.delete(i);
            end if;
        end loop;

        -- Снимаем назначение инспектора и ПЗ со всех корректировок по ключу декларации
        for i in 1..col_decl_keys.count
        loop
            pac$declaration.p$unassign_inspector(
                    p_inn_declarant => col_decl_keys(i).inn_declarant
                    ,p_kpp_effective => col_decl_keys(i).kpp_effective
                    ,p_period_effective => col_decl_keys(i).period_effective
                    ,p_fiscal_year => col_decl_keys(i).fiscal_year
                    ,p_type_code => col_decl_keys(i).type_code);

            pac$user_task.p$close_all_tasks_by_decl(
                    p_inn_declarant => col_decl_keys(i).inn_declarant
                    ,p_kpp_effective => col_decl_keys(i).kpp_effective
                    ,p_period_effective => col_decl_keys(i).period_effective
                    ,p_fiscal_year => col_decl_keys(i).fiscal_year
                    ,p_type_code => col_decl_keys(i).type_code
                    ,p_close_reason_id => pac$user_task.close_reason_sono_changed);
        end loop;

		log_notification(v_scope||'Кол-во корректировок со сброшенными назначениями и ПЗ: '||to_char(col_decl_keys.count));


        -- Завершаем ПЗ на корректировках имеющих код НО отличный от текущего
        v_indexer:= col_decl_zips.first;
        while(v_indexer is not null)
        loop
            pac$user_task.p$close_all_tasks_by_corr_id(
                p_declaration_correction_id => col_decl_zips(v_indexer).zip,
                p_close_reason_id => pac$user_task.close_reason_sono_changed);

            v_indexer:= col_decl_zips.next(v_indexer);
        end loop;

		log_notification(v_scope||'Кол-во запросов на сброс ПЗ по корректировкам: '||to_char(col_decl_zips.count));
    end if;

    --Обновляем дату запуска
    update nds2_mrr_user.configuration
    set value = TO_CHAR(v_exec_date, 'DD-MM-YYYY HH24:MI:SS')
    where parameter = v_last_exec_param;

	log_notification(v_scope||'Завершение выполнения в '||to_char(current_date,v_date_frmt));

	exception when others then
		  log_error(v_scope||substr(sqlerrm, 1, 256));
		  raise;
end;

procedure REFRESH_DECLARATONS
as
begin
log_notification('REFRESH_DECLARATONS: UPDATE DECLARATIONS');

begin
  P$UPDATE_ASK_DECL_AND_JRNL;
exception when others then
  log_error('REFRESH_DECLARATONS(P$UPDATE_ASK_DECL_AND_JRNL):('||sqlcode||'):'||substr(sqlerrm, 1, 256));
end;

log_notification('REFRESH_DECLARATONS: UPDATE REORG DATA');

  begin
    P$BUILD_REORG_AGGREGATE;
    exception when others then
      log_error('REFRESH_DECLARATONS(P$BUILD_REORG_AGGREGATE):('||sqlcode||'):'||substr(sqlerrm, 1, 256));
  end;

  begin
    P$UPDATE_TAX_PAYER;
    exception when others then
      log_error('REFRESH_DECLARATONS(P$UPDATE_TAX_PAYER):('||sqlcode||'):'||substr(sqlerrm, 1, 256));
  end;

begin
  UPDATE_DECLARATION_HISTORY;
exception when others then
  log_error('REFRESH_DECLARATONS(UPDATE_DECLARATION_HISTORY):('||sqlcode||'):'||substr(sqlerrm, 1, 256));
end;

begin
  P$UPDATE_DECL_KNP_STATUS;
exception when others then
  log_error('REFRESH_DECLARATONS(P$UPDATE_DECL_KNP_STATUS):('||sqlcode||'):'||substr(sqlerrm, 1, 256));
end;

log_notification('REFRESH_DECLARATONS: DROP ASSIGNMENTS ON SONO CHANGED DECLARATIONS');
begin
    P$DROP_ASSIGN_ON_SONO_CHANGED();
    exception when others then
      log_error('P$DROP_ASSIGN_ON_SONO_CHANGED ERROR:('||sqlcode||'):'||substr(sqlerrm, 1 ,256));
end;

-- Закрытие расхождений, которые вошли в решения
begin
  PAC$KNP_RESULT_DOCUMENTS.P$CLOSE_DISCREPANCY;
  exception when others then
    log_error('PAC$KNP_RESULT_DOCUMENTS.P$CLOSE_DISCREPANCY:('||sqlcode||'):'||substr(sqlerrm, 1 ,256));
end;

-- Кэш и агрегаты КНП расхождений
P$UPDATE_KNP_DISCREPANCY_DECL;
P$UPDATE_KNP_DECL_SUMMARY;
P$UPDATE_KNP_RELATION_SUMMARY;

-- Обновление статуса расхождений в актах/решениях
begin
  PAC$KNP_RESULT_DOCUMENTS.P$UPDATE_DISCREPANCY_STATUS;
    exception when others then
    log_error('PAC$KNP_RESULT_DOCUMENTS.P$UPDATE_DISCREPANCY_STATUS:('||sqlcode||'):'||substr(sqlerrm, 1 ,256));
end;

-- Формирование агрегатов отчетов по актам/решениям
begin
  PAC$KNP_RESULT_DOCUMENTS.P$AGGREGATE_DAILY;
    exception when others then
    log_error('PAC$KNP_RESULT_DOCUMENTS.P$AGGREGATE_DAILY:('||sqlcode||'):'||substr(sqlerrm, 1 ,256));
end;

begin
  PAC$NDS2_FULL_CYCLE.P$ON_COMPLETE;
    exception when others then
      log_error('PAC$NDS2_FULL_CYCLE.P$ON_COMPLETE:('||sqlcode||'):'||substr(sqlerrm, 1 ,256));
end;

P$UPDATE_DECL_ACTIVE();
P$UPDATE_DECL_ACTIVE_SEARCH();

--временно решали вопрос производительности
--P$OVERRIDE_DECLVIEWS;

execute_short_query('truncate table QUERY_ROWCOUNT_CACHE');
execute_short_query('truncate table QUERY_ROWCOUNT_CACHE_STAT');

begin
  UPDATE_MACRO_TAXPAYER_INFO;
exception when others then
  log_error('REFRESH_DECLARATONS(UPDATE_MACRO_TAXPAYER_INFO):('||sqlcode||'):'||substr(sqlerrm, 1, 256));
end;

P$UPDATE_DECLARATION_KNP();

exception when others then
  log_error('REFRESH_DECLARATONS:('||sqlcode||'):'||substr(sqlerrm, 1, 256));
end;

procedure P$OVERRIDE_DECLVIEWS as
begin
  P$CREATE_VDECLARATION();
  P$CREATE_VINSP_DECLARATION();
  exception when others then
    log_error('REFRESH_DECLARATONS:('||sqlcode||'):'||substr(sqlerrm, 1, 256));
end; 

procedure P$UPDATE_DECL_ACTIVE
as
    v_hndl int;
    v_type dbms_sql.varchar2s;
    v_idx number;
    v_temp_table_exists number;
begin
  log(DECL_ACTIVE_NAME||' - Начало обновления',0);

  begin
      select 1
      into v_temp_table_exists
      from user_tables t
      where t.TABLE_NAME = DECL_ACTIVE_NAME||'_TEMP';

      exception when NO_DATA_FOUND then v_temp_table_exists := 0;
  end;

  if v_temp_table_exists = 1 then
    execute_short_query('drop table '||DECL_ACTIVE_NAME||'_TEMP');
  end if;

  v_hndl := dbms_sql.open_cursor;
  v_idx := 0;
  v_idx := v_idx + 1; v_type(v_idx) := 'create table NDS2_MRR_USER.'||DECL_ACTIVE_NAME||'_TEMP ';
  v_idx := v_idx + 1; v_type(v_idx) := 'partition by list (partition_id) ';
  v_idx := v_idx + 1; v_type(v_idx) := 'subpartition by list (subpartition_id) ';
  v_idx := v_idx + 1; v_type(v_idx) := '( ';
  v_idx := v_idx + 1; v_type(v_idx) := '  partition PERIOD20151 values (20151) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ( ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20151_1 values (1), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20151_2 values (2), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20151_DEFAULT values (default) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ), ';
  v_idx := v_idx + 1; v_type(v_idx) := '  partition PERIOD20152 values (20152) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ( ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20152_1 values (1), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20152_2 values (2), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20152_DEFAULT values (default) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ), ';
  v_idx := v_idx + 1; v_type(v_idx) := '  partition PERIOD20153 values (20153) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ( ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20153_1 values (1), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20153_2 values (2), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20153_DEFAULT values (default) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ), ';
  v_idx := v_idx + 1; v_type(v_idx) := '  partition PERIOD20154 values (20154) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ( ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20154_1 values (1), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20154_2 values (2), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20154_DEFAULT values (default) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ), ';
  v_idx := v_idx + 1; v_type(v_idx) := '  partition PERIOD20161 values (20161) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ( ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20161_1 values (1), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20161_2 values (2), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20161_DEFAULT values (default) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ), ';
  v_idx := v_idx + 1; v_type(v_idx) := '  partition PERIOD20162 values (20162) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ( ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20162_1 values (1), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20162_2 values (2), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20162_DEFAULT values (default) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ), ';
  v_idx := v_idx + 1; v_type(v_idx) := '  partition PERIOD20163 values (20163) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ( ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20163_1 values (1), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20163_2 values (2), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20163_DEFAULT values (default) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ), ';
  v_idx := v_idx + 1; v_type(v_idx) := '  partition PERIOD20164 values (20164) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ( ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20164_1 values (1), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20164_2 values (2), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20164_DEFAULT values (default) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ), ';
  v_idx := v_idx + 1; v_type(v_idx) := '  partition PERIOD20171 values (20171) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ( ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20171_1 values (1), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20171_2 values (2), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20171_DEFAULT values (default) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ), ';
  v_idx := v_idx + 1; v_type(v_idx) := '  partition PERIOD20172 values (20172) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ( ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20172_1 values (1), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20172_2 values (2), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20172_DEFAULT values (default) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ), ';
  v_idx := v_idx + 1; v_type(v_idx) := '  partition PERIOD20173 values (20173) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ( ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20173_1 values (1), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20173_2 values (2), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20173_DEFAULT values (default) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ), ';
  v_idx := v_idx + 1; v_type(v_idx) := '  partition PERIOD20174 values (20174) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ( ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20174_1 values (1), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20174_2 values (2), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20174_DEFAULT values (default) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ), ';
  v_idx := v_idx + 1; v_type(v_idx) := '  partition PERIOD201871 values (201871) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ( ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P201871_1 values (1), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P201871_2 values (2), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P201871_DEFAULT values (default) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ), ';
  v_idx := v_idx + 1; v_type(v_idx) := '  partition PERIOD201872 values (201872) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ( ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P201872_1 values (1), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P201872_2 values (2), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P201872_DEFAULT values (default) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ), ';
  v_idx := v_idx + 1; v_type(v_idx) := '  partition PERIOD201873 values (201873) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ( ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P201873_1 values (1), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P201873_2 values (2), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P201873_DEFAULT values (default) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ), ';
  v_idx := v_idx + 1; v_type(v_idx) := '  partition PERIOD201874 values (201874) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ( ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P201874_1 values (1), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P201874_2 values (2), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P201874_DEFAULT values (default) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ), ';
  v_idx := v_idx + 1; v_type(v_idx) := '  partition PERIOD_DEFAULT values (default) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  (';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition PDEFAULT_DEFAULT values (default) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ) ';
  v_idx := v_idx + 1; v_type(v_idx) := ') parallel 16 ';
  v_idx := v_idx + 1; v_type(v_idx) := 'as select * from NDS2_MRR_USER.'||VIEW_ACTIVE_BUILD_NAME||' ';
  
  /*
  for i in 1..v_idx loop
    dbms_output.put_line(v_type(i));
  end loop;
  */

  dbms_sql.parse (
       c  => v_hndl
      ,statement => v_type
      ,lb => 1
      ,ub => v_type.count
      ,lfflg => false
      ,language_flag => dbms_sql.native);
  if DBMS_SQL.is_open(v_hndl) then
     DBMS_SQL.close_cursor(v_hndl);
  end if;
  log(DECL_ACTIVE_NAME||' - Создана временная таблица',0);

  execute immediate 'alter table '||DECL_ACTIVE_NAME||'_TEMP noparallel';

  begin
    execute immediate 'alter table '||DECL_ACTIVE_NAME||'_TEMP modify JOIN_K NOT NULL';
    exception when others then null;
  end;

  begin

    PAC$INDEX_MANAGER.P$ULTIMATE_VALIDATE_INDEXES(DECL_ACTIVE_NAME||'_TEMP', 'DECLARATION_ACTIVE');

    dbms_stats.gather_table_stats(
         ownname => 'NDS2_MRR_USER'
        ,tabname => DECL_ACTIVE_NAME||'_TEMP'
        ,cascade => true
		,degree => 16);

    exception when others then
      log_error(DECL_ACTIVE_NAME||'_TEMP'||' - Ошибка создания индексов :('||sqlcode||'):'||substr(sqlerrm, 1, 256));
  end;

  execute immediate 'Alter session set ddl_lock_timeout = 300';
  execute immediate 'drop table '||DECL_ACTIVE_NAME;
  log(DECL_ACTIVE_NAME||' - Таблица удалена',0);
  execute immediate 'Alter session set ddl_lock_timeout = 0';

  execute immediate 'alter table '||DECL_ACTIVE_NAME||'_TEMP rename to '||DECL_ACTIVE_NAME;
  log(DECL_ACTIVE_NAME||' - Временная таблица переименована',0);

  begin
    execute immediate 'alter view V$DECLARATION compile';
    log(DECL_ACTIVE_NAME||' - Представление перекомпилировано',0);
    exception when others then
      log_error(DECL_ACTIVE_NAME||' - Ошибка компиляции V$DECLARATION:('||sqlcode||'):'||substr(sqlerrm, 1, 256));
  end;

end;

procedure P$UPDATE_ASK_DECL_AND_JRNL
as
begin

 P$UPDATE_TABLE(
  p_temp_table_name => 'ASK_DECLANDJRNL_TMP'
 ,p_backup_table_name => 'ASK_DECLANDJRNL_BAK'
 ,p_original_table_name => 'ASK_DECLANDJRNL'
 ,p_sorce_view => 'V$ASK_DECLANDJRNL_BUILD'
 ,p_index_key => 'V$ASK_DECLANDJRNL');

end;

procedure P$UPDATE_DECL_ACTIVE_SEARCH
as
    v_hndl int;
    v_type dbms_sql.varchar2s;
    v_idx number;
    v_temp_table_exists number;
begin
  log(DECL_ACTIVE_NAME_SEARCH||' - Начало обновления',0);

  begin
      select 1
      into v_temp_table_exists
      from user_tables t
      where t.TABLE_NAME = DECL_ACTIVE_NAME_SEARCH||'_TEMP';

      exception when NO_DATA_FOUND then v_temp_table_exists := 0;
  end;

  if v_temp_table_exists = 1 then
    execute_short_query('drop table '||DECL_ACTIVE_NAME_SEARCH||'_TEMP');
  end if;

  v_hndl := dbms_sql.open_cursor;
  v_idx := 0;
  v_idx := v_idx + 1; v_type(v_idx) := 'create table NDS2_MRR_USER.'||DECL_ACTIVE_NAME_SEARCH||'_TEMP ';
  v_idx := v_idx + 1; v_type(v_idx) := 'partition by list (partition_id) ';
  v_idx := v_idx + 1; v_type(v_idx) := 'subpartition by list (subpartition_id) ';
  v_idx := v_idx + 1; v_type(v_idx) := '( ';
  v_idx := v_idx + 1; v_type(v_idx) := '  partition PERIOD20151 values (20151) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ( ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20151_1 values (1), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20151_2 values (2), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20151_DEFAULT values (default) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ), ';
  v_idx := v_idx + 1; v_type(v_idx) := '  partition PERIOD20152 values (20152) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ( ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20152_1 values (1), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20152_2 values (2), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20152_DEFAULT values (default) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ), ';
  v_idx := v_idx + 1; v_type(v_idx) := '  partition PERIOD20153 values (20153) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ( ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20153_1 values (1), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20153_2 values (2), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20153_DEFAULT values (default) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ), ';
  v_idx := v_idx + 1; v_type(v_idx) := '  partition PERIOD20154 values (20154) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ( ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20154_1 values (1), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20154_2 values (2), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20154_DEFAULT values (default) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ), ';
  v_idx := v_idx + 1; v_type(v_idx) := '  partition PERIOD20161 values (20161) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ( ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20161_1 values (1), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20161_2 values (2), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20161_DEFAULT values (default) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ), ';
  v_idx := v_idx + 1; v_type(v_idx) := '  partition PERIOD20162 values (20162) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ( ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20162_1 values (1), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20162_2 values (2), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20162_DEFAULT values (default) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ), ';
  v_idx := v_idx + 1; v_type(v_idx) := '  partition PERIOD20163 values (20163) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ( ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20163_1 values (1), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20163_2 values (2), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20163_DEFAULT values (default) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ), ';
  v_idx := v_idx + 1; v_type(v_idx) := '  partition PERIOD20164 values (20164) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ( ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20164_1 values (1), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20164_2 values (2), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20164_DEFAULT values (default) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ), ';
  v_idx := v_idx + 1; v_type(v_idx) := '  partition PERIOD20171 values (20171) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ( ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20171_1 values (1), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20171_2 values (2), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20171_DEFAULT values (default) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ), ';
  v_idx := v_idx + 1; v_type(v_idx) := '  partition PERIOD20172 values (20172) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ( ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20172_1 values (1), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20172_2 values (2), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20172_DEFAULT values (default) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ), ';
  v_idx := v_idx + 1; v_type(v_idx) := '  partition PERIOD20173 values (20173) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ( ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20173_1 values (1), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20173_2 values (2), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20173_DEFAULT values (default) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ), ';
  v_idx := v_idx + 1; v_type(v_idx) := '  partition PERIOD20174 values (20174) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ( ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20174_1 values (1), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20174_2 values (2), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P20174_DEFAULT values (default) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ), ';
  v_idx := v_idx + 1; v_type(v_idx) := '  partition PERIOD201871 values (201871) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ( ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P201871_1 values (1), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P201871_2 values (2), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P201871_DEFAULT values (default) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ), ';
  v_idx := v_idx + 1; v_type(v_idx) := '  partition PERIOD201872 values (201872) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ( ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P201872_1 values (1), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P201872_2 values (2), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P201872_DEFAULT values (default) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ), ';
  v_idx := v_idx + 1; v_type(v_idx) := '  partition PERIOD201873 values (201873) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ( ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P201873_1 values (1), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P201873_2 values (2), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P201873_DEFAULT values (default) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ), ';
  v_idx := v_idx + 1; v_type(v_idx) := '  partition PERIOD201874 values (201874) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ( ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P201874_1 values (1), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P201874_2 values (2), ';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition P201874_DEFAULT values (default) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ), ';
  v_idx := v_idx + 1; v_type(v_idx) := '  partition PERIOD_DEFAULT values (default) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  (';
  v_idx := v_idx + 1; v_type(v_idx) := '    subpartition PDEFAULT_DEFAULT values (default) ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ) ';
  v_idx := v_idx + 1; v_type(v_idx) := ') parallel 16 ';
  v_idx := v_idx + 1; v_type(v_idx) := 'as select * from NDS2_MRR_USER.'||VIEW_ACTIVE_SEARCH_BUILD_NAME||'  ';

  DBMS_SQL.PARSE (
       c  => v_hndl
      ,statement => v_type
      ,lb => 1
      ,ub => v_type.count
      ,lfflg => false
      ,language_flag => dbms_sql.native);
  if DBMS_SQL.is_open(v_hndl) then
     DBMS_SQL.close_cursor(v_hndl);
  end if;
  log(DECL_ACTIVE_NAME_SEARCH||' - Создана временная таблица',0);

  execute immediate 'alter table '||DECL_ACTIVE_NAME_SEARCH||'_TEMP noparallel';
  
  begin
    execute immediate 'alter table '||DECL_ACTIVE_NAME_SEARCH||'_TEMP modify JOIN_K NOT NULL';
    exception when others then null;
  end;

  begin

    PAC$INDEX_MANAGER.P$ULTIMATE_VALIDATE_INDEXES(DECL_ACTIVE_NAME_SEARCH||'_TEMP', 'DECLARATION_ACTIVE_SEARCH');

    dbms_stats.gather_table_stats(
         ownname => 'NDS2_MRR_USER'
        ,tabname => DECL_ACTIVE_NAME_SEARCH||'_TEMP'
        ,cascade => true
		,degree => 16);

    exception when others then
      log_error(DECL_ACTIVE_NAME_SEARCH||'_TEMP'||' - Ошибка создания индексов :('||sqlcode||'):'||substr(sqlerrm, 1, 256));
  end;

  execute immediate 'Alter session set ddl_lock_timeout = 300';
  execute_short_query('drop table '||DECL_ACTIVE_NAME_SEARCH);
  log(DECL_ACTIVE_NAME_SEARCH||' - Таблица удалена',0);
  execute immediate 'Alter session set ddl_lock_timeout = 0';

  execute immediate 'alter table '||DECL_ACTIVE_NAME_SEARCH||'_TEMP rename to '||DECL_ACTIVE_NAME_SEARCH;
  log(DECL_ACTIVE_NAME_SEARCH||' - Временная таблица переименована',0);

  begin
    execute immediate 'alter view V$DECLARATION compile';
    log(DECL_ACTIVE_NAME_SEARCH||' - Представление перекомпилировано',0);
    exception when others then
      log_error(DECL_ACTIVE_NAME||' - Ошибка компиляции V$DECLARATION:('||sqlcode||'):'||substr(sqlerrm, 1, 256));
  end;
  
  

end;

procedure UPDATE_DECLARATION_HISTORY
as
v_rowcount      number;
v_temp_table_exists number;
begin

  -- Отпишемся в лог
  log('Начало обновления таблицы DECLARATION_HISTORY.', 0);

  begin
      select 1
      into v_temp_table_exists
      from user_tables t
      where t.TABLE_NAME = 'DECLARATION_HISTORY_NEW';

      exception when NO_DATA_FOUND then v_temp_table_exists := 0;
  end;

  if v_temp_table_exists = 1 then
     begin
         -- удаляем временную таблицу DECLARATION_HISTORY_NEW на случай, если она существует
         execute_short_query('drop table NDS2_MRR_USER.DECLARATION_HISTORY_NEW');
     end;
  end if;

  execute_short_query('create table DECLARATION_HISTORY_NEW parallel 16 as select * from V$SOURCE_DECLARATION_HISTORY');
  execute immediate 'alter table DECLARATION_HISTORY_NEW noparallel';
  -- Отписываемся в Лог
  log('(2) Сформирована новая версия таблицы: DECLARATION_HISTORY_NEW.', 0);

  -- В этом месте у нас есть уже индексы с теми же именами, но на старой таблице.
  -- Поэтому надо использовать Ultimate_...
  PAC$INDEX_MANAGER.P$ULTIMATE_VALIDATE_INDEXES('DECLARATION_HISTORY_NEW','DECLARATION_HISTORY');
  log('(7) Созданы индексы DECLARATION_HISTORY_NEW',0);

  dbms_stats.gather_table_stats(
           ownname => 'NDS2_MRR_USER',
           tabname => 'DECLARATION_HISTORY_NEW',
           cascade => true,
           degree => 16);
  log('(6) Собрана статистика DECLARATION_HISTORY_NEW',0);

  -- подменяем таблицы
  -- -- Переименовываем постоянную в DECLARATION_HISTORY_BAK
  execute_short_query('alter table DECLARATION_HISTORY rename to DECLARATION_HISTORY_BAK');
  log('(4) Выполнена подмена таблицы DECLARATION_HISTORY на DECLARATION_HISTORY_BAK.', 0);

  -- -- Переименовываем временную в DECLARATION_HISTORY
  execute_short_query('alter table DECLARATION_HISTORY_NEW rename to DECLARATION_HISTORY');
  log('(5) Выполнена подмена таблицы DECLARATION_HISTORY_NEW на DECLARATION_HISTORY.', 0);

  execute immediate 'Alter session set ddl_lock_timeout = 300';
  -- Удаляем старую постоянную таблицу
  execute_short_query('drop table NDS2_MRR_USER.DECLARATION_HISTORY_BAK');
  execute immediate 'Alter session set ddl_lock_timeout = 0';
  log('(8) Удалена старая: DECLARATION_HISTORY_BAK.', 0);

  -- Отписываемся по результатам
  log('(9) Закончено обновление таблицы DECLARATION_HISTORY.', 0);

exception when others then
  execute immediate 'Alter session set ddl_lock_timeout = 0';
  log_error('UPDATE_DECLARATION_HISTORY:('||sqlcode||'):'||substr(sqlerrm, 1, 256));
end;

procedure UPDATE_MACRO_TAXPAYER_INFO
as
pragma autonomous_transaction;
begin
  log('Очистка таблицы MACROREPORT_TAXPAYER_DETAIL.', 0);
  execute immediate 'truncate table MACROREPORT_TAXPAYER_DETAIL';

  log('Обновление таблицы MACROREPORT_TAXPAYER_DETAIL.', 0);
  insert /*+ APPEND */ into MACROREPORT_TAXPAYER_DETAIL select * from V$MACRO_TAXPAYER_DETAIL_BUILD;

  log('Сбор статистики MACROREPORT_TAXPAYER_DETAIL.', 0);
  dbms_stats.gather_table_stats(ownname => 'NDS2_MRR_USER',
  tabname => 'MACROREPORT_TAXPAYER_DETAIL',
  estimate_percent => 10,
  method_opt => 'FOR ALL INDEXED COLUMNS');

  commit;

exception when others then
	rollback;
   log_error('UPDATE_MACRO_TAXPAYER_INFO:('||sqlcode||'):'||substr(sqlerrm, 1, 256));
end;

procedure P$CREATE_VDECLARATION as
    v_hndl int;
    v_type dbms_sql.varchar2s;
    v_idx number;
    v_temp_table_exists number;
begin
  begin
    execute immediate 'drop table NDS2_MRR_USER.V$DECLARATION_TEMP';
    exception when others then null;  
  end;

  execute_short_query('create table V$DECLARATION_TEMP parallel 16 as select * from V$DECLARATION_BLD');

  execute immediate 'alter table V$DECLARATION_TEMP noparallel';

  begin
    execute immediate 'alter table V$DECLARATION_TEMP modify ID NOT NULL';
  exception when others then
    log_error('V$DECLARATION_TEMP modify ID NOT NULL: ('||sqlcode||'):'||substr(sqlerrm, 1, 256));
  end;

  log('V$DECLARATION - Создана временная таблица',0);

  PAC$INDEX_MANAGER.P$ULTIMATE_VALIDATE_INDEXES('V$DECLARATION_TEMP','V$DECLARATION');
  log('V$DECLARATION - Созданы индексы V$DECLARATION_TEMP',0);
  
  begin 
    dbms_stats.gather_table_stats(ownname => 'NDS2_MRR_USER', 
                      tabname => 'V$DECLARATION_TEMP', 
                      estimate_percent => DBMS_STATS.AUTO_SAMPLE_SIZE, 
                      method_opt => 'FOR ALL COLUMNS SIZE AUTO',
                      degree => 16);
  end; 
  
  begin
    execute immediate 'drop table V$DECLARATION';
    exception when others then null;
  end;
  
  begin
    execute immediate 'drop view V$DECLARATION';
    exception when others then null;
  end;
  
  log('V$DECLARATION - удалена старая',0);

  begin
    execute immediate 'rename V$DECLARATION_TEMP to V$DECLARATION';
    exception when others then
   log_error('P$CREATE_VDECLARATION: rename TEMP to MAIN ('||sqlcode||'):'||substr(sqlerrm, 1, 256));
  end;

  log('V$DECLARATION - новая переименована',0);

  exception when others then
   log_error('P$CREATE_VDECLARATION:('||sqlcode||'):'||substr(sqlerrm, 1, 256));
end;

procedure P$CREATE_VINSP_DECLARATION as
    v_hndl int;
    v_type dbms_sql.varchar2s;
    v_idx number;
    v_temp_table_exists number;
begin
  begin
    execute immediate 'drop table NDS2_MRR_USER.V$INSPECTOR_DECLARATION_TEMP';
    exception when others then null;  
  end;

  execute_short_query('create table V$INSPECTOR_DECLARATION_TEMP parallel 16 as select * from V$INSPECTOR_DECLARATION_BLD');
  
  execute immediate 'alter table V$INSPECTOR_DECLARATION_TEMP noparallel';
  
  begin
    execute immediate 'alter table V$INSPECTOR_DECLARATION_TEMP modify ID NOT NULL';
  exception when others then
    log_error('V$INSPECTOR_DECLARATION_TEMP modify ID NOT NULL: ('||sqlcode||'):'||substr(sqlerrm, 1, 256));
  end;

  log('V$INSPECTOR_DECLARATION - Создана временная таблица',0);

  PAC$INDEX_MANAGER.P$ULTIMATE_VALIDATE_INDEXES('V$INSPECTOR_DECLARATION_TEMP','V$INSPECTOR_DECLARATION');
  log('V$INSPECTOR_DECLARATION - Созданы индексы V$INSPECTOR_DECLARATION_TEMP',0);
  
  begin 
    dbms_stats.gather_table_stats(ownname => 'NDS2_MRR_USER', 
                      tabname => 'V$INSPECTOR_DECLARATION_TEMP', 
                      estimate_percent => DBMS_STATS.AUTO_SAMPLE_SIZE, 
                      method_opt => 'FOR ALL COLUMNS SIZE AUTO',
                      degree => 16);
  end; 
  
  begin
    execute immediate 'drop table V$INSPECTOR_DECLARATION';
    exception when others then null;
  end;
  
  begin
    execute immediate 'drop view V$INSPECTOR_DECLARATION';
    exception when others then null;
  end;
  
  log('V$INSPECTOR_DECLARATION - удалена старая',0);

  begin
    execute immediate 'rename V$INSPECTOR_DECLARATION_TEMP to V$INSPECTOR_DECLARATION';
    exception when others then
   log_error('P$CREATE_VDECLARATION: rename TEMP to MAIN ('||sqlcode||'):'||substr(sqlerrm, 1, 256));
  end;

  log('V$INSPECTOR_DECLARATION - новая переименована',0);

  exception when others then
   log_error('P$CREATE_VDECLARATION:('||sqlcode||'):'||substr(sqlerrm, 1, 256));
end;

end;
/
