﻿create or replace package NDS2_MRR_USER.Pac$Selections as
TYPE T_ARR_NUM IS TABLE OF NUMBER(19) INDEX BY PLS_INTEGER;

function GenerateUniqueName
(
  p_user_sid varchar2
) return varchar2;

function VERIFY_SELECTION_NAME
(
  p_user_sid varchar2,
  p_selection_name varchar2,
  p_selection_id number
) return varchar2;

PROCEDURE SEND_TO_EOD(p_selection_ids in T_ARR_NUM);

PROCEDURE SAVE_SELECTION
(
  p_selectionId IN OUT selection.id%type,
  p_name IN selection.name%type,
  p_analytic IN selection.analytic%type,
  p_analytic_sid IN selection.analytic_sid%type,
  p_chief IN selection.chief%type,
  p_chief_sid IN selection.chief_sid%type,
  p_sender IN selection.sender%type,
  p_status IN selection.status%type,
  p_status_date IN selection.status_date%type,
  p_creation_date in selection.creation_date%type,
  p_modification_date in selection.modification_date%type,
  p_comment in action_history.action_comment%type,
  p_save_reason in number,/*1 - create, 2 - remove, 3 - status change, 4 - update*/
  p_process_stage in number
);

PROCEDURE                   REMOVE_SELECTION
(pId IN NUMBER, analytic IN VARCHAR2);

PROCEDURE          SELECTION_SET_STATE
(pId IN NUMBER, statusId IN NUMBER, userName in varchar2);

PROCEDURE          SELECT_SELECTION
(selectionCursor OUT SYS_REFCURSOR);

PROCEDURE          SELECT_SELECTION_BY_NAME
(selectionStartName VARCHAR2, selectionCursor OUT SYS_REFCURSOR);

PROCEDURE GET_SELECTION_STATUS
(selectionId IN NUMBER, statusCursor OUT SYS_REFCURSOR);

PROCEDURE GET_FAVORITE_FILTERS (pAnalyst IN FAVORITE_FILTERS.ANALYST%type, pFavCursor OUT SYS_REFCURSOR);

PROCEDURE GET_FAVORITE_FILTER (pId IN FAVORITE_FILTERS.ID%type, pFavCursor OUT SYS_REFCURSOR);

PROCEDURE GET_ALL_FAVORITE_FILTERS (pFavCursor OUT SYS_REFCURSOR);

PROCEDURE SAVE_FAVORITE_FILTER (
  pId IN OUT FAVORITE_FILTERS.ID%type,
  pName IN FAVORITE_FILTERS.NAME%type,
  pAnalyst IN FAVORITE_FILTERS.ANALYST%type,
  pFilter IN FAVORITE_FILTERS.FILTER%type);

PROCEDURE DELETE_FAVORITE_FILTER (pId IN FAVORITE_FILTERS.ID%type);

PROCEDURE UPDATE_DECLARATION_CHECK_STATE (
  pSelectionId IN number,
  pDeclarationId IN number,
  pCheckState IN SELECTION_DECLARATION.IS_IN_PROCESS%type);

PROCEDURE UPDATE_ALL_DECL_CHECK_STATE (
  pSelectionId IN SELECTION.ID%type,
  pCheckState IN SELECTION_DECLARATION.IS_IN_PROCESS%type);

PROCEDURE UPDATE_DISCREPANCY_CHECK_STATE (
  pSelectionId IN SELECTION.ID%type,
  pDiscrepancyId IN SOV_DISCREPANCY.ID%type,
  pCheckState IN SELECTION_DISCREPANCY.IS_IN_PROCESS%type,
  pStatus OUT NUMBER
  );

PROCEDURE ALL_DECLARATIONS_CHECK_STATE (
  pSelectionId IN SELECTION.ID%type,
  pCheckState IN SELECTION_DECLARATION.IS_IN_PROCESS%type
  );

PROCEDURE ALL_DISCREPANCIES_CHECK_STATE (
  pSelectionId IN SELECTION.ID%type,
  pCheckState IN SELECTION_DISCREPANCY.IS_IN_PROCESS%type,
  pStatus OUT NUMBER
  );

PROCEDURE COUNT_CHECKED_DECLARATIONS (
  pSelectionId IN SELECTION.ID%type,
  pChecked OUT NUMBER,
  pAll OUT NUMBER
  );

PROCEDURE COUNT_CHECKED_DISCREPANCIES (
  pSelectionId IN SELECTION.ID%type,
  pDisabled OUT NUMBER,
  pChecked OUT NUMBER,
  pAll OUT NUMBER,
  pInWorked OUT NUMBER
  );

procedure MERGE_SENDED_SELECTIONS;

/*
ведение истории смены состояний выборки
*/
PROCEDURE UPD_HIST_SELECTION_STATUS
(
  p_id in selection.id%type,
  p_status in selection.status%type
);

/*
загрузка переходов состояний
*/
PROCEDURE GET_SELECTION_TRANSITIONS
(pFavCursor OUT SYS_REFCURSOR);

PROCEDURE P$GET_ACTION_HISTORY
    (
      pSelectionId in Selection.Id%type,
      pCursor out sys_refcursor
    );

PROCEDURE INSERT_ACTION_HISTORY (
    pSelectionId IN ACTION_HISTORY.CD%type,
    pComment IN ACTION_HISTORY.ACTION_COMMENT%type,
    pUser IN ACTION_HISTORY.USER_NAME%type,
    pAction IN ACTION_HISTORY.ACTION%type
);

PROCEDURE P$GET_REGIONS (
  pUserSID IN METODOLOG_CHECK_EMPLOYEE.SID%type, -- Идентификатор пользователя
  pData OUT SYS_REFCURSOR -- Список регионов
  ); -- Возвращает список регионов, доступных пользователю (для фильтра выборки)

PROCEDURE P$SEARCH_REGIONS (
  pUserSID IN USER_TO_N_SSRF.USER_SID%type, -- Идентификатор пользователя
  pSearchKey IN VARCHAR2,
  pMaxQuantity IN NUMBER,
  pData OUT SYS_REFCURSOR -- Список регионов
  );

PROCEDURE P$GET_INSPECTIONS (
  pUserSID IN METODOLOG_CHECK_EMPLOYEE.SID%type, -- Идентификатор пользователя
  pData OUT SYS_REFCURSOR -- Список инспекций
  ); -- Возвращает список инспекций, доступных пользователю (для фильтра выборки)

PROCEDURE P$SEARCH_INSPECTIONS (
  pUserSID IN USER_TO_N_SSRF.USER_SID%type,
  pSearchKey IN VARCHAR2,
  pMaxQuantity IN NUMBER,
  pData OUT SYS_REFCURSOR
  );

procedure P$START_CANCEL_SELECTION;
procedure P$DO_CANCEL_SELECTION;

procedure P$REMOVE_SELECTION_RESULTS
(
  p_selection_id IN SELECTION.ID%type
);
procedure P$LOAD_SELECTION ( -- Загружает детализированные данные выборки
  pId IN SELECTION.ID%type,
  pCursor OUT SYS_REFCURSOR
  );

--Возвращает список атрибутов фильтра выборки
procedure P$GET_FILTER_PARAMETERS
(
  pCursor OUT SYS_REFCURSOR
);

--Возвращает данные для редактора фильтра выборки.
procedure P$GET_FILTER_EDITOR_PARAMETERS
(
  pIsManual IN NUMBER,
  pCursor OUT SYS_REFCURSOR
);

--Возвращает допустимые значения атрибута фильтра выборки.
procedure P$GET_FILTER_PARAMETER_OPTIONS
(
   pAttributeId IN NUMBER,
   pCursor OUT SYS_REFCURSOR
);

procedure P$SAVE_SELECTION_REGIONS
(
   pSelectionId IN NUMBER,
   pRegionCodes IN VARCHAR2
);

procedure P$UPDATE_AUTOSELECTION_FILTER
(
  pId IN AUTOSELECTION_FILTER.Id%type,
  pIncludeFilter IN AUTOSELECTION_FILTER.INCLUDE_FILTER%type,
  pExcludeFilter IN AUTOSELECTION_FILTER.EXCLUDE_FILTER%type
);

procedure P$BUILD_SELECTION_FOR_2nd_SIDE;

PROCEDURE P$SAVE_FILTER (
  pSelectionId IN SELECTION_FILTER.SELECTION_ID%type,
  pFilter IN  SELECTION_FILTER.FILTER%type);

PROCEDURE P$GET_FILTER (
  pSelectionId IN SELECTION_FILTER.SELECTION_ID%type,
  pCursor OUT SYS_REFCURSOR);

end Pac$Selections;
/
create or replace package body NDS2_MRR_USER.Pac$Selections as

function GenerateUniqueName
(
  p_user_sid varchar2
) return varchar2
as
v_name varchar2(128);
v_pattern varchar2(32) := 'Выборка ';
v_counter number(10) := 1;
begin
 v_name := v_pattern || v_counter;
 for sel in
   (SELECT distinct name, TO_NUMBER(REGEXP_SUBSTR(selection.name, '[[:digit:]]+')) x FROM selection
      WHERE lower(analytic_sid) = lower(p_user_sid) and regexp_like(selection.name, v_pattern) order by x asc) --(select name from selections where analytic = p_user_name order by name asc)
 loop
  if TO_CHAR(sel.name) = v_name then
    v_counter := v_counter + 1;
    v_name := v_pattern || v_counter;
  end if;
 end loop;
 return v_name;
 exception
   when no_data_found then
     return rawtohex(sys_guid());
end;

function VERIFY_SELECTION_NAME
(
  p_user_sid varchar2,
  p_selection_name varchar2,
  p_selection_id number
) return varchar2
as
v_idx number := 0;
v_exist number;
v_name varchar2(1024);
begin
v_name := trim(p_selection_name);
select count(1) into v_exist from selection where lower(analytic_sid) = lower(p_user_sid) and name = v_name and id <> p_selection_id;
dbms_output.put_line(v_exist);
while v_exist >= 1 and v_idx < 10000 loop
   v_idx := v_idx+1;
   v_name := trim(p_selection_name)||' ('||v_idx||')';
   select count(1) into v_exist from selection where lower(analytic_sid) = lower(p_user_sid) and name = v_name and id <> p_selection_id;
end loop;

return v_name;
end;

PROCEDURE SEND_TO_EOD(p_selection_ids in T_ARR_NUM)
is
  v_tab_ids t$table_of_number := t$table_of_number ();
  v_sel_send_status number := 4;
  v_sel_send_discrep_status number := 4;
begin
    v_tab_ids.extend(p_selection_ids.count);
    for i in 1..p_selection_ids.count
    loop
     v_tab_ids(i) := p_selection_ids(i);
    end loop;

    /*обновляем статус выборок*/
    UPDATE SELECTION s
      SET status = v_sel_send_status
    WHERE EXISTS
    (
      SELECT 1
      FROM SELECTION vw
      inner join table(v_tab_ids) IDS on IDS.column_value = vw.id
      inner join SELECTION_TRANSITION st ON st.state_from = vw.status and st.state_to = v_sel_send_status
      WHERE vw.id = s.id
    );

    /*обновляем статусы расхождений в выборке*/
    /*update selection_discrepancy sd
    set
         sd.stage = v_sel_send_discrep_status,
         sd.is_in_process = 0
    where
         sd.selection_id in (select column_value from table(v_tab_ids))
         and sd.is_in_process = 1;*/

    /*отключаем декларации в других выборках*/
    /*update selection_declaration sdecl
    set
      sdecl.is_in_process = 0
    where
    sdecl.selection_id not in (select column_value from table(v_tab_ids))
    and exists
    (
      select
          1
      from selection_declaration tmp
      inner join (select column_value from table(v_tab_ids)) sids on tmp.selection_id = sids.column_value
      inner join selection s on tmp.selection_id = s.id
      where
          tmp.declaration_id = sdecl.declaration_id
          and s.status not in (13,7,12,14, 10, 4)
    );*/

    /*обновляем статусы расхождений в иных выборках до статуса отправлено*/
    /*update selection_discrepancy sd
    set
      sd.stage = v_sel_send_discrep_status
      ,sd.is_in_process = 0
    where
    sd.selection_id not in (select column_value from table(v_tab_ids))
    and exists
    (
      select
          1
      from selection_discrepancy tmp
      inner join (select column_value from table(v_tab_ids)) sids on tmp.selection_id = sids.column_value
      inner join selection s on tmp.selection_id = s.id
      where
          tmp.discrepancy_id = sd.discrepancy_id
          and s.status not in (13,7,12,14, 10, 4)
    );*/

    /*
    update selection_discrepancy Z
    set
     Z.STAGE = 4,
     Z.Is_In_Process = 0
    where Z.SELECTION_ID <> p_selectionId
    and  exists (
    select
     1
    from
    (
     select
      sd.discrepancy_id
     from
      selection s
     inner join selection_discrepancy sd on sd.selection_id = s.id and sd.is_in_process = 1 and sd.stage < 4
     where s.id = p_selectionId
     ) T
     inner join selection_discrepancy sd1 on T.DISCREPANCY_ID = sd1.discrepancy_id
     inner join selection s1 on s1.id = sd1.selection_id and s1.status not in (13,7,12,14)
     where
      s1.id = z.selection_id and sd1.discrepancy_id = z.discrepancy_id
    );*/


    /*добавляем историю*/

end;

PROCEDURE SAVE_SELECTION
(
  p_selectionId IN OUT selection.id%type,
  p_name IN selection.name%type,
  p_analytic IN selection.analytic%type,
  p_analytic_sid IN selection.analytic_sid%type,
  p_chief IN selection.chief%type,
  p_chief_sid IN selection.chief_sid%type,
  p_sender IN selection.sender%type,
  p_status IN selection.status%type,
  p_status_date IN selection.status_date%type,
  p_creation_date in selection.creation_date%type,
  p_modification_date in selection.modification_date%type,
  p_comment in action_history.action_comment%type,
  p_save_reason in number,/*1 - create, 2 - remove, 3 - status change, 4 - update*/
  p_process_stage in number
)
as
v_prev_status number;
begin

--select status into v_prev_status from selection where id = p_selectionId;

update SELECTION set
   name = p_name,
   analytic = p_analytic,
   analytic_sid = p_analytic_sid,
   chief = p_chief,
   sender = p_sender,
   status = p_status,
   modification_date = p_modification_date,
   status_date = p_status_date
where id = p_selectionId;

if sql%rowcount = 0 then
  begin
    p_selectionId := SELECTIONS_SEQ.NEXTVAL;

    insert into selection(id, name, creation_date, modification_date, status_date, analytic, analytic_sid, chief, chief_sid, sender, status, type, discrepancy_stage)
    values(p_selectionId, p_name, p_creation_date, null, null, p_analytic, p_analytic_sid, p_chief, p_chief_sid, p_sender, p_status, 0, p_process_stage);

    insert into Action_History (id, cd, status, change_date, action_comment, user_name, action) values(SEQ_ACTION_HISTORY.nextval, p_selectionId, p_status, sysdate, p_comment, p_analytic, 1);

    insert into SELECTION_FILTER (id, selection_id) values (p_selectionId, p_selectionId);
  end;
else
  begin
    insert into Action_History (id, cd, status, change_date, action_comment, user_name, action) values(SEQ_ACTION_HISTORY.nextval, p_selectionId, p_status, sysdate, p_comment, p_analytic, 4);
  end;
end if;
    
if p_save_reason = 3 then
  UPD_HIST_SELECTION_STATUS(p_selectionId, p_status);
  /*быстрое решение к показу*/
  if p_status = 6 then --соглансовать
    update selection_discrepancy Z
    set
     Z.STAGE = 3,
     Z.Is_In_Process = 0
    where Z.SELECTION_ID <> p_selectionId
    and  exists (
    select
     1
    from
    (
     select
      sd.discrepancy_id
     from
      selection s
     inner join selection_discrepancy sd on sd.selection_id = s.id and sd.is_in_process = 1 and sd.stage < 3
     where s.id = p_selectionId
     ) T
     inner join selection_discrepancy sd1 on T.DISCREPANCY_ID = sd1.discrepancy_id
     inner join selection s1 on s1.id = sd1.selection_id and s1.status in (1,2,3,5)
     where
      s1.id = z.selection_id and sd1.discrepancy_id = z.discrepancy_id
    );
  end if;

  if p_status = 4 then --отправить
    update selection_discrepancy Z
    set
     Z.STAGE = 4,
     Z.Is_In_Process = 0
    where Z.SELECTION_ID <> p_selectionId
    and  exists (
    select
     1
    from
    (
     select
      sd.discrepancy_id
     from
      selection s
     inner join selection_discrepancy sd on sd.selection_id = s.id and sd.is_in_process = 1 and sd.stage < 4
     where s.id = p_selectionId
     ) T
     inner join selection_discrepancy sd1 on T.DISCREPANCY_ID = sd1.discrepancy_id
     inner join selection s1 on s1.id = sd1.selection_id and s1.status not in (13,7,12,14)
     where
      s1.id = z.selection_id and sd1.discrepancy_id = z.discrepancy_id
    );
  end if;
  /*быстрое решение к показу*/

end if;
end;

PROCEDURE REMOVE_SELECTION
(pId IN NUMBER, analytic IN VARCHAR2)
AS
c1 number;
c2 number;
BEGIN
  select ID into c1 FROM DICT_SELECTION_STATUS WHERE NAME='Удалено';
  select ID into c2 from DICT_ACTIONS where ACTION_NAME='Удаление выборки';
  UPDATE SELECTION SET STATUS = c1 WHERE ID = pId;
  insert into Action_History values(SEQ_ACTION_HISTORY.nextval, pId, c1, sysdate, '', analytic, c2);
  UPD_HIST_SELECTION_STATUS(pId, c1);
END;


PROCEDURE SELECTION_SET_STATE
(pId IN NUMBER, statusId IN NUMBER, userName in varchar2)
AS
c1 number;
BEGIN
  UPDATE SELECTION SET STATUS = statusId WHERE ID = pId;
  select ID into c1 from DICT_ACTIONS where ACTION_NAME='Изменение статуса';
  insert into Action_History values(SEQ_ACTION_HISTORY.nextval, pId, statusId, sysdate, '', userName, c1);
  UPD_HIST_SELECTION_STATUS(pId, statusId);
END;




PROCEDURE SELECT_SELECTION
(selectionCursor OUT SYS_REFCURSOR) AS
BEGIN
  OPEN selectionCursor FOR
  SELECT
    SELECTION.ID,
    SELECTION.ANALYTIC,
    SELECTION.CHIEF,
    SELECTION.CREATION_DATE,
    SELECTION.NAME,
    SS.NAME
    FROM SELECTION
    LEFT JOIN DICT_SELECTION_STATUS SS ON SELECTION.STATUS = SS.ID;
END;

PROCEDURE SELECT_SELECTION_BY_NAME
(selectionStartName VARCHAR2, selectionCursor OUT SYS_REFCURSOR) AS
BEGIN
  OPEN selectionCursor FOR
  SELECT
    SELECTION.ID,
    SELECTION.ANALYTIC,
    SELECTION.CHIEF,
    SELECTION.CREATION_DATE,
    SELECTION.NAME,
    SS.NAME
    FROM SELECTION
    LEFT JOIN DICT_SELECTION_STATUS SS ON SELECTION.STATUS = SS.ID
    WHERE SELECTION.NAME LIKE selectionStartname;
END;

PROCEDURE GET_SELECTION_STATUS
(selectionId IN NUMBER, statusCursor OUT SYS_REFCURSOR)
AS
BEGIN
 OPEN statusCursor FOR
 SELECT STATUS FROM SELECTION WHERE ID = selectionId;
END;

PROCEDURE GET_FAVORITE_FILTERS (pAnalyst IN FAVORITE_FILTERS.ANALYST%type, pFavCursor OUT SYS_REFCURSOR)
AS
BEGIN
  OPEN pFavCursor FOR SELECT ID,NAME,ANALYST,FILTER FROM FAVORITE_FILTERS WHERE ANALYST = pAnalyst;
END;

PROCEDURE GET_FAVORITE_FILTER (pId IN FAVORITE_FILTERS.ID%type, pFavCursor OUT SYS_REFCURSOR)
AS
BEGIN
  OPEN pFavCursor FOR SELECT ID,NAME,ANALYST,FILTER FROM FAVORITE_FILTERS WHERE ID = pId;
END;

PROCEDURE GET_ALL_FAVORITE_FILTERS (pFavCursor OUT SYS_REFCURSOR)
AS
BEGIN
  OPEN pFavCursor FOR SELECT ID,NAME,ANALYST,FILTER FROM FAVORITE_FILTERS;
END;

PROCEDURE SAVE_FAVORITE_FILTER (
  pId IN OUT FAVORITE_FILTERS.ID%type,
  pName IN FAVORITE_FILTERS.NAME%type,
  pAnalyst IN FAVORITE_FILTERS.ANALYST%type,
  pFilter IN FAVORITE_FILTERS.FILTER%type)
AS
BEGIN
  IF pId IS NULL THEN
    BEGIN
      pId := SEQ_FAV_FILTER.NEXTVAL;
      INSERT INTO FAVORITE_FILTERS (ID,NAME,ANALYST,FILTER) VALUES (pId,pName,pAnalyst,pFilter);
    END;
  ELSE
    BEGIN
      UPDATE FAVORITE_FILTERS
      SET
             FILTER = pFilter
      WHERE
             ID = pId;
    END;
   END IF;
END;

PROCEDURE DELETE_FAVORITE_FILTER (pId IN FAVORITE_FILTERS.ID%type)
AS
BEGIN
  DELETE FROM FAVORITE_FILTERS WHERE ID = pId;
END;

PROCEDURE UPDATE_DECLARATION_CHECK_STATE (
  pSelectionId IN number,
  pDeclarationId IN number,
  pCheckState IN SELECTION_DECLARATION.IS_IN_PROCESS%type)
AS
v_selection_discrep_stage number;
v_selection_discrep_max_stage number;
BEGIN

  select
     max(s.discrepancy_stage),
     max(case when s.id = pSelectionId then s.discrepancy_stage else 0 end)
     into v_selection_discrep_max_stage, v_selection_discrep_stage
  from selection s
  inner join selection_declaration sd on sd.selection_id = s.id
  where sd.zip = pDeclarationId;

  /*если попытка обработать событие из выборки низшей стадии отработки для НД
  , то отказываем*/
  if v_selection_discrep_stage >= v_selection_discrep_max_stage then

    UPDATE SELECTION_DISCREPANCY sd
    SET IS_IN_PROCESS = pCheckState
    WHERE
           SELECTION_ID = pSelectionId
           and zip = pDeclarationId
           and stage < 3
           and exists
           (
             select
               1
             from selection s1
             inner join selection_discrepancy sd1
             on sd1.selection_id = s1.id
             where
                s1.discrepancy_stage = v_selection_discrep_stage
                and sd1.zip = pDeclarationId
           ); -- В случае если расхождение надо включить вместе с декларацией, проверяем, что оно еще не согласовано в другой выборке

    UPDATE SELECTION_DECLARATION
    SET IS_IN_PROCESS = pCheckState
    WHERE
         SELECTION_ID = pSelectionId
     AND ZIP = pDeclarationId;

  end if;
END;

PROCEDURE UPDATE_ALL_DECL_CHECK_STATE (
  pSelectionId IN SELECTION.ID%type,
  pCheckState IN SELECTION_DECLARATION.IS_IN_PROCESS%type)
AS
v_SelectionStage number;
BEGIN

  select discrepancy_stage into v_SelectionStage
  from selection where id = pSelectionId;

  UPDATE SELECTION_DECLARATION SET IS_IN_PROCESS = pCheckState
  WHERE SELECTION_ID = pSelectionId;

  UPDATE SELECTION_DISCREPANCY main
  SET IS_IN_PROCESS = pCheckState
  WHERE
      main.SELECTION_ID = pSelectionId
      and main.stage < 3
      and main.is_in_process <> pCheckState
      and main.discrepancy_id not in (
          select d1.discrepancy_id
          from selection_discrepancy d1
          inner join selection s1 on s1.id = d1.selection_id
          inner join selection_discrepancy d2 on d2.discrepancy_id = d1.discrepancy_id
      where d2.selection_id = pSelectionId
      and d1.selection_id <> pSelectionId
      and s1.discrepancy_stage > v_SelectionStage);
END;

PROCEDURE UPDATE_DISCREPANCY_CHECK_STATE (
  pSelectionId IN SELECTION.ID%type,
  pDiscrepancyId IN SOV_DISCREPANCY.ID%type,
  pCheckState IN SELECTION_DISCREPANCY.IS_IN_PROCESS%type,
  pStatus OUT NUMBER
  )
AS
v_selection_discrep_stage number;
v_selection_discrep_max_stage number;
BEGIN
  /*определим текущий этап выборки и максимальный, куда входит расхождение*/
  select
     max(s.discrepancy_stage),
     max(case when s.id = pSelectionId then s.discrepancy_stage else 0 end),
     max(CASE WHEN nvl(sd.stage, 1)< 3 THEN 0 ELSE 1 END)
     into v_selection_discrep_max_stage, v_selection_discrep_stage, pStatus
  from selection s
  inner join selection_discrepancy sd on sd.selection_id = s.id
  where sd.discrepancy_id = pDiscrepancyId;

  /*проводим операцию чек/анчек для расхождения где максимальная стадия выборки равна текучей выборки,
  с которой пошло событие*/
  IF v_selection_discrep_stage >= v_selection_discrep_max_stage THEN
    IF pStatus = 0 then
    BEGIN
       IF pCheckState = 0 THEN
       BEGIN
         dbms_output.put_line(-1);
          UPDATE SELECTION_DECLARATION t_decl_ref
          SET IS_IN_PROCESS = pCheckState
          WHERE
            SELECTION_ID = pSelectionId
            AND NOT EXISTS
            (
               SELECT 1
               FROM SELECTION_DISCREPANCY xRef
               WHERE t_decl_ref.zip = xRef.zip
                     AND xRef.SELECTION_ID = pSelectionId
                     AND xRef.DISCREPANCY_ID <> pDiscrepancyId
                     and xRef.Is_In_Process = 1
            );
       END;
       ELSE
       BEGIN
           UPDATE SELECTION_DECLARATION t_decl_ref
           SET IS_IN_PROCESS = 1
           WHERE t_decl_ref.SELECTION_ID = pSelectionId
           AND EXISTS
           (
               SELECT 1
               FROM SELECTION_DISCREPANCY xRef
               WHERE xRef.zip = t_decl_ref.zip
               AND xRef.DISCREPANCY_ID = pDiscrepancyId
               and xRef.Is_In_Process = 0
           );
      END;
      END IF;

      UPDATE SELECTION_DISCREPANCY
      SET IS_IN_PROCESS = pCheckState
      WHERE
              SELECTION_ID = pSelectionId
          AND DISCREPANCY_ID = pDiscrepancyId;
      END;
     END IF;
  END IF;

END;

PROCEDURE ALL_DECLARATIONS_CHECK_STATE (
  pSelectionId IN SELECTION.ID%type,
  pCheckState IN SELECTION_DECLARATION.IS_IN_PROCESS%type)
AS
v_SelectionStage number;
BEGIN

  select discrepancy_stage into v_SelectionStage
  from selection where id = pSelectionId;

  UPDATE SELECTION_DISCREPANCY main
  SET IS_IN_PROCESS = pCheckState
  WHERE
      main.SELECTION_ID = pSelectionId
      and main.stage < 3
      and main.is_in_process <> pCheckState
      and main.discrepancy_id not in (
          select d1.discrepancy_id
          from selection_discrepancy d1
          inner join selection s1 on s1.id = d1.selection_id
          inner join selection_discrepancy d2 on d2.discrepancy_id = d1.discrepancy_id
      where d2.selection_id = pSelectionId
      and d1.selection_id <> pSelectionId
      and s1.discrepancy_stage > v_SelectionStage);

   UPDATE SELECTION_DECLARATION SET IS_IN_PROCESS = pCheckState WHERE SELECTION_ID = pSelectionId;

END;

PROCEDURE ALL_DISCREPANCIES_CHECK_STATE (
  pSelectionId IN SELECTION.ID%type,
  pCheckState IN SELECTION_DISCREPANCY.IS_IN_PROCESS%type,
  pStatus OUT NUMBER
  )
AS
v_sel_stage number;
BEGIN

   SELECT discrepancy_stage into v_sel_stage
   from SELECTION where id = pSelectionId;

   SELECT SUM(CASE WHEN pCheckState=1 AND STAGE >=3 THEN 1 ELSE 0 END)
   INTO pStatus
   FROM SELECTION_DISCREPANCY
   WHERE SELECTION_ID = pSelectionId;

   UPDATE SELECTION_DECLARATION t_decl_ref
   SET IS_IN_PROCESS = pCheckState
   WHERE  t_decl_ref.SELECTION_ID=pSelectionId
      AND EXISTS(
          SELECT
             1
          FROM
           SELECTION_DISCREPANCY t_disc_ref
          WHERE
             t_disc_ref.zip=t_decl_ref.zip
             AND t_disc_ref.SELECTION_ID=pSelectionId
        AND (pCheckState = 0 OR t_disc_ref.STAGE < 3)
             AND t_disc_ref.discrepancy_id not in (
                 select
                 d1.discrepancy_id
                 from
                 selection_discrepancy d1
                 inner join selection s1 on s1.id = d1.selection_id
                 inner join selection_discrepancy d2 on d1.discrepancy_id = d2.discrepancy_id
                 where d2.selection_id = pSelectionId
                       and d1.selection_id <> pSelectionId
                       and s1.discrepancy_stage > v_sel_stage
             ));

   UPDATE SELECTION_DISCREPANCY t_ref
   SET IS_IN_PROCESS = pCheckState
   WHERE
   t_ref.SELECTION_ID=pSelectionId
   and t_ref.STAGE < 3;

END;

PROCEDURE COUNT_CHECKED_DECLARATIONS (
  pSelectionId IN SELECTION.ID%type,
  pChecked OUT NUMBER,
  pAll OUT NUMBER
  )
AS
BEGIN
  SELECT
       SUM(is_in_process),
       COUNT(1)
  INTO pChecked, pAll
  FROM selection_declaration
  WHERE SELECTION_ID = pSelectionId;
END;

PROCEDURE COUNT_CHECKED_DISCREPANCIES (
  pSelectionId IN SELECTION.ID%type,
  pDisabled OUT NUMBER,
  pChecked OUT NUMBER,
  pAll OUT NUMBER,
  pInWorked OUT NUMBER
  )
AS
BEGIN
  SELECT
       SUM(CASE WHEN (sd.stage >= 3) THEN 1 ELSE 0 END) as disabled,
       SUM(sd.is_in_process) as is_in_process,
       COUNT(1) as all_count,
       SUM(CASE WHEN (sd.stage in (1,2,3)) and (sd.is_in_process = 1) THEN 1 ELSE 0 END) as cnt_in_work
  INTO pDisabled, pChecked, pAll, pInWorked
  FROM selection_discrepancy sd
  where sd.selection_id = pSelectionId;

END;


/*
ведение истории смены состояний выборки
*/
PROCEDURE UPD_HIST_SELECTION_STATUS
(
  p_id in selection.id%type,
  p_status in selection.status%type
)
as
begin

  insert into HIST_SELECTION_STATUS (ID, DT, VAL)
         values(p_id, sysdate, p_status);

end;


/*
загрузка переходов состояний
*/
PROCEDURE GET_SELECTION_TRANSITIONS
(pFavCursor OUT SYS_REFCURSOR)
AS
BEGIN
  OPEN pFavCursor FOR
  SELECT STATE_FROM, STATE_TO, OPERATION FROM SELECTION_TRANSITION;
END;

PROCEDURE P$GET_ACTION_HISTORY
    (
      pSelectionId in Selection.Id%type,
      pCursor out sys_refcursor
    )
  is
BEGIN
  open pCursor for
    select ah.id, ah.cd, ah.status, ah.change_date, ah.action_comment, ah.user_name, ah.action,
       dss.id, dss.name, da.id, da.action_name
    from Action_History ah
           left join Dict_Selection_Status dss on (ah.Status = dss.Id)
           left join Dict_Actions da on (ah.Action = da.Id)
    where ah.Cd = pSelectionId
    order by ah.change_date desc;
END;

PROCEDURE INSERT_ACTION_HISTORY (
    pSelectionId IN ACTION_HISTORY.CD%type,
    pComment IN ACTION_HISTORY.ACTION_COMMENT%type,
    pUser IN ACTION_HISTORY.USER_NAME%type,
    pAction IN ACTION_HISTORY.ACTION%type
)
AS
BEGIN

    INSERT INTO ACTION_HISTORY(ID,CD,STATUS,CHANGE_DATE,ACTION_COMMENT,USER_NAME,ACTION)
           SELECT SEQ_ACTION_HISTORY.nextval, t.ID, t.STATUS, Sysdate, pComment, pUser, pAction
           FROM SELECTION t
           WHERE t.ID = pSelectionId;

END;


PROCEDURE P$GET_REGIONS (
  pUserSID IN METODOLOG_CHECK_EMPLOYEE.SID%type, -- Идентификатор пользователя
  pData OUT SYS_REFCURSOR -- Список регионов
)
AS
BEGIN
  OPEN pData FOR
  SELECT distinct reg.S_CODE, reg.S_NAME, reg.DESCRIPTION
  FROM V$SSRF reg
    LEFT OUTER JOIN METODOLOG_CHECK_EMPLOYEE mce ON mce.Sid = pUserSID
    LEFT OUTER JOIN Metodolog_Region rr ON rr.id = mce.resp_id AND reg.s_code = rr.code
  WHERE (EXISTS(SELECT 1 FROM CONFIGURATION WHERE parameter='restrict_by_region' and value = 'N') OR rr.id IS NOT NULL);
END;

PROCEDURE P$SEARCH_REGIONS (
  pUserSID IN USER_TO_N_SSRF.USER_SID%type, -- Идентификатор пользователя
  pSearchKey IN VARCHAR2,
  pMaxQuantity IN NUMBER,
  pData OUT SYS_REFCURSOR -- Список регионов
)
AS
BEGIN
  OPEN pData FOR
  WITH q AS (
       SELECT ROWNUM as IDX, v.*
       FROM (
              SELECT DISTINCT reg.S_CODE, reg.S_NAME
              FROM
                     V$SSRF reg
                     LEFT OUTER JOIN METODOLOG_CHECK_EMPLOYEE mce ON mce.Sid = pUserSID
                     LEFT OUTER JOIN Metodolog_Region rr ON rr.id = mce.resp_id AND reg.s_code = rr.code
              WHERE
                     (EXISTS(SELECT 1 FROM CONFIGURATION WHERE parameter='restrict_by_region' and value = 'N') OR rr.id IS NOT NULL)
                     AND (reg.S_CODE LIKE pSearchKey OR reg.S_NAME LIKE pSearchKey)
              ORDER BY reg.s_name
            ) v)
    SELECT DISTINCT vw.S_CODE, vw.S_NAME, vw.DESCRIPTION
    FROM
            V$SSRF vw
            JOIN q ON q.S_CODE=vw.S_CODE
    WHERE q.IDX <= pMaxQuantity;
END;



PROCEDURE P$GET_INSPECTIONS (
  pUserSID IN METODOLOG_CHECK_EMPLOYEE.SID%type, -- Идентификатор пользователя
  pData OUT SYS_REFCURSOR -- Список инспекций
  ) -- Возвращает список инспекций, доступных пользователю (для фильтра выборки)
AS
BEGIN
  OPEN pData FOR
  SELECT distinct ins.s_code, ins.s_parent_code, ins.s_name
  FROM v$inspection ins
    LEFT OUTER JOIN METODOLOG_CHECK_EMPLOYEE mce ON mce.Sid = pUserSID
    LEFT OUTER JOIN Metodolog_Region_Inspection mci ON mci.Region_Id = mce.resp_id AND mci.code = ins.s_code
  WHERE (EXISTS(SELECT 1 FROM CONFIGURATION WHERE parameter='restrict_by_region' and value = 'N') OR mci.Region_Id IS NOT NULL);
END;

PROCEDURE P$SEARCH_INSPECTIONS (
  pUserSID IN USER_TO_N_SSRF.USER_SID%type, -- Идентификатор пользователя
  pSearchKey IN VARCHAR2,
  pMaxQuantity IN NUMBER,
  pData OUT SYS_REFCURSOR -- Список инспекций
  ) -- Возвращает список инспекций, доступных пользователю (для фильтра выборки)
AS
BEGIN
  OPEN pData FOR
  WITH q AS (
       SELECT ROWNUM as IDX, v.*
       FROM (
            SELECT distinct ins.s_code, ins.s_parent_code, ins.s_name
            FROM
                   v$inspection ins
                   LEFT OUTER JOIN METODOLOG_CHECK_EMPLOYEE mce ON mce.Sid = pUserSID
                   LEFT OUTER JOIN Metodolog_Region_Inspection mci ON mci.Region_Id = mce.resp_id AND mci.code = ins.s_code
            WHERE
                   (EXISTS(SELECT 1 FROM CONFIGURATION WHERE parameter='restrict_by_region' and value = 'N') OR mci.Region_Id IS NOT NULL) AND
                   (ins.s_code LIKE pSearchKey OR ins.s_name LIKE pSearchKey)
            ORDER BY ins.s_name) v)
   SELECT DISTINCT ins.s_code, ins.s_parent_code, ins.s_name
   FROM v$inspection ins JOIN q ON q.s_code=ins.s_code
   WHERE q.idx <= pMaxQuantity;
END;

procedure P$START_CANCEL_SELECTION
  as
begin
  NDS2$SYS.LOG_INFO(
  P_SITE => 'PAC$SELECTIONS.START_CANCEL_SELECTION',   P_ENTITY_ID => null,
  P_MESSAGE_CODE => SQLCODE,   P_MESSAGE => 'starting');
  begin
    P$DO_CANCEL_SELECTION;
  exception when others then null;
  end;
end;

procedure P$DO_CANCEL_SELECTION
as
  CURSOR selections IS
  SELECT id FROM SELECTION WHERE STATUS in (1, 2, 3);
  v_selections selections%ROWTYPE;
  pCountAll number(19);
  pCountClose number(19);
begin

  OPEN selections;
  FETCH selections INTO v_selections;
  LOOP

    pCountClose := 0;
    pCountAll := 0;
    select a.CountClose, a.CountAll into pCountClose, pCountAll from dual
    left outer join
    (
      select
         count(case when dis.status = 2 then dis.id end) as CountClose
        ,count(dis.id) as CountAll
      from sov_discrepancy dis
      where dis.id in
      (
        select sd.discrepancy_id from SELECTION_DISCREPANCY sd where sd.selection_id = v_selections.id
        and sd.IS_IN_PROCESS != 0
      )
    ) a on 1 = 1;

    if pCountAll > 0 and pCountClose = pCountAll then
    begin
       update SELECTION s set status = 12, status_date = SYS_EXTRACT_UTC(SYSTIMESTAMP)
       where s.id = v_selections.id and s.status in (1, 2, 3);
       commit;
    end;
    end if;

    FETCH selections INTO v_selections;
    EXIT WHEN selections%NOTFOUND;
  END LOOP;

  CLOSE selections;

end;

procedure MERGE_SENDED_SELECTIONS
as
  error_sc  number := 0;
begin
  for row in
  (
    select id
    from selection sel
    where sel.status = 4
  )
  loop

  -- проверка полноты отгрузки СФ по расхожнениям
  select count(*) into error_sc
  from
  (
    select seld.selection_id
    from selection_discrepancy seld
    left join SOV_DISCREPANCY sd on sd.id = seld.discrepancy_id
    left join SOV_INVOICE_ALL inv_all on inv_all.row_key = sd.invoice_rk or inv_all.row_key = sd.invoice_contractor_rk
    where seld.selection_id = row.id and inv_all.row_key is null
  ) a;

  if error_sc = 0 then

    insert into hist_discrepancy_stage(id, submit_date, stage_id, status_id)
    select dis.id, sysdate, 1, 1
    from sov_discrepancy dis
    left join hist_discrepancy_stage hist on hist.id = dis.id and hist.stage_id = 1 and hist.status_id = 1
    where hist.id is null;

    update selection
    set status = 13
    where id = row.id;

  else

    update selection
    set status = 11
    where id = row.id;

    for err in
    (
        select inv_all.row_key as key
        from selection_discrepancy seld
        left join SOV_DISCREPANCY sd on sd.id = seld.discrepancy_id
        left join SOV_INVOICE_ALL inv_all on inv_all.row_key = sd.invoice_rk or inv_all.row_key = sd.invoice_contractor_rk
        where seld.selection_id = row.id and inv_all.row_key is null
    )
    loop
      dbms_output.put_line(TO_CHAR(err.key) || ' - error NOT FOUND invoice key IN SOV_INVOICE_ALL for selection id: ' || TO_CHAR(row.id));

      NDS2$SYS.LOG_INFO(
      P_SITE => 'Pac2$Selections.MERGE_SENDED_SELECTIONS',   P_ENTITY_ID => null,
      P_MESSAGE_CODE => SQLCODE,   P_MESSAGE => TO_CHAR(err.key) || ' - error NOT FOUND invoice key IN SOV_INVOICE_ALL for selection id: ' || TO_CHAR(row.id));
    end loop;

  end if;

  end loop;
  commit;
end;



procedure P$REMOVE_SELECTION_RESULTS
(
  p_selection_id IN SELECTION.ID%type
)
as
begin
  delete from SELECTION_DISCREPANCY where selection_id = p_selection_id;
  delete from SELECTION_DECLARATION where selection_id = p_selection_id;
end;

procedure P$LOAD_SELECTION ( -- Загружает детализированные данные выборки
  pId IN SELECTION.ID%type,
  pCursor OUT SYS_REFCURSOR
)
as
begin

  open pCursor for
       select
               s.id,
               s.name as name,
               s.creation_date as CreationDate,
               s.modification_date as ModificationDate,
               s.status_date as StatusDate,
               s.analytic_sid as Analytic_SID,
               s.analytic as Analytic,
               s.chief_sid as Manager_SID,
               s.chief as Manager,
               flt.filter,
               sel_type.s_code as TypeCode,
               sel_type.s_name as Type,
               sel_status.id as Status,
               sel_status.name as StatusName,
               sel_status.rank,
               -1 as declarationcount,
               -1 as decl_pvp_total,
               -1 as discrepanciescount,
               -1 as totalamount,
               -1 as selecteddiscrepanciespvpamount,
               null as RegionCode,
               '' as RegionName,
               '' as RegionNames,
               s.discrepancy_stage as ProcessStage
        from
                  SELECTION s
             join DICT_SELECTION_STATUS sel_status on s.status = sel_status.id
             join DICT_SELECTION_TYPE sel_type on s.type = sel_type.s_code
             join SELECTION_FILTER flt on flt.selection_id = s.id

              where s.id = pId;
end;

--Возвращает список атрибутов фильтра выборки
--Id  NUMBER  Идентификатор атрибута
--Name  VARCHAR2(30)  Наименование колонки агрегата, соответствующая атрибуту
procedure P$GET_FILTER_PARAMETERS
(
  pCursor OUT SYS_REFCURSOR
)
as
begin

  open pCursor for
  select
  p.id,
  p.aggregate_name as name
  from SELECTION_FILTER_PARAMETER p;
end;

--Возвращает данные для редактора фильтра выборки.
--Id  NUMBER  Идентификатор атрибута
--Name  VARCHAR2(255)  Наименования колонки для редактора
--AreaId  NUMBER  Идентификатор области
--TypeId  NUMBER  Идентификатор редактора атрибута
--ModelId  NUMBER  Идентификатор модели
procedure P$GET_FILTER_EDITOR_PARAMETERS
(
  pIsManual IN NUMBER,
  pCursor OUT SYS_REFCURSOR
)
as
begin

  open pCursor for
  select
    p.id,
    p.editor_name as name,
  p.aggregate_name as aggregate_name,
    p.editor_area_id as AreaId,
    p.editor_type_id as TypeId,
    p.editor_model_id as ModelId
  from SELECTION_FILTER_PARAMETER p
  left join SELECTION_FILTER_APPLICABILITY a on a.parameter_id = p.id
  where a.is_manual = pIsManual;

end;

--Возвращает допустимые значения атрибута фильтра выборки.
procedure P$GET_FILTER_PARAMETER_OPTIONS
(
   pAttributeId IN NUMBER,
   pCursor OUT SYS_REFCURSOR
)
as
begin

  open pCursor for
  select
    p.title as name,
    p.value as value
  from SELECTION_FILTER_OPTION p
  where p.parameter_id = pAttributeId;

end;


procedure P$SAVE_SELECTION_REGIONS
(
   pSelectionId IN NUMBER,
   pRegionCodes IN VARCHAR2
)
as
  TYPE T_STRING_ARRAY IS TABLE OF VARCHAR2(255) INDEX BY PLS_INTEGER;
  pListRegionCodes T_STRING_ARRAY;
  pCountRegionCodes NUMBER;
begin

  select regexp_substr(pRegionCodes,'[^,]+', 1, level) bulk collect into pListRegionCodes from dual
  connect by regexp_substr(pRegionCodes, '[^,]+', 1, level) is not null;

  pCountRegionCodes := pListRegionCodes.count;

  delete from SELECTION_REGION where SELECTION_ID = pSelectionId;

  FOR i IN 1..pCountRegionCodes LOOP
    insert into SELECTION_REGION (SELECTION_ID, СODE)
    values (pSelectionId, pListRegionCodes(i));
  END LOOP;

  commit;

end;

procedure P$UPDATE_AUTOSELECTION_FILTER
(
  pId IN AUTOSELECTION_FILTER.Id%type,
  pIncludeFilter IN AUTOSELECTION_FILTER.INCLUDE_FILTER%type,
  pExcludeFilter IN AUTOSELECTION_FILTER.EXCLUDE_FILTER%type
)
as
begin
 update AUTOSELECTION_FILTER set INCLUDE_FILTER = pIncludeFilter, EXCLUDE_FILTER = pExcludeFilter
 where id = pId;
 commit;
end;

procedure P$BUILD_SELECTION_FOR_2nd_SIDE
as
  v_gap_contr_inn varchar2(12 char);
  TYPE region_sel_type IS TABLE OF NUMBER INDEX BY VARCHAR2(64);
  region_sel_collection region_sel_type;
  v_sel_id number;
  v_cnt_dis number;
  v_cnt_decl number;
  v_doc_dis_count number;
  C_SEL_SIDE_2_STATUS_CODE number(1) := 2;
  v_iteration_num number;
begin
 
  v_iteration_num := seq_sel_2nd_sess_id.nextval;
  
  for region_line in (select * from v$ssrf) loop
    region_sel_collection(region_line.s_code) := SELECTIONS_SEQ.NEXTVAL;
  end loop;

  /* пробегаемся по АТ, по которым можно формировать выборки 2-й стороне */
  for line in (
               select /*+ index(sd) use_nl(sd) */
                 d.*, tpm.period
               from doc d
               inner join seod_declaration sd on sd.decl_reg_num = d.ref_entity_id and d.sono_code = sd.sono_code
               inner join dict_tax_period_map tpm on tpm.code = sd.tax_period
               where d.processed = 0
               and sd.fiscal_year = 2016 
			   and sd.tax_period in ('07', '08', '09', '23', '55', '77', '78', '79') --все возможные варианты третьего квартала
               and d.doc_type = 1
               and d.doc_kind = 1
              )
  loop
    begin
      v_doc_dis_count := 0;
      
      
      for line_dis in
           (
              with doc_dis as
              (
				  select
					   sd.id
            ,case when (sd.side_primary_processing = 2) then decl_buyer.region_code else decl_seller.region_code end as region_code
					  ,case when (sd.side_primary_processing = 2) then sd.buyer_zip else sd.seller_zip end as zip
            ,sd.seller_zip
            ,sd.buyer_zip
					  ,sd.amnt
					  ,sd.amount_pvp
					  ,sd.sov_id
					  ,sd.create_date             
  				  ,sd.type
					  ,sd.rule_num
					  ,case when sd.type = 1 then sd.invoice_rk else
							(case when (sd.side_primary_processing = 2) then sd.invoice_rk else sd.invoice_contractor_rk end)
							end as invoice_rk
					  ,case
						when sd.mult_compare is null then null
						when sd.mult_compare > 1 then 1
						else 0
					   end as subtype_code
					  ,sd.side_primary_processing
					  ,decl_buyer.region_code as buyer_region_code
					  ,decl_buyer.sono_code as buyer_sono_code
					  ,decl_buyer.inn_contractor as buyer_inn
					  ,decl_buyer.inn_declarant as buyer_inn_declarant
					  ,nvl(ask_buyer.kppreorg, ask_buyer.kppnp) as buyer_kpp
					  ,ask_buyer.kppnp as buyer_kpp_declarant
					  ,buyer_ks_agg.r8r3_equality as buyer_ks_r8r3_quality
					  ,decode(ask_buyer.Nd_Prizn, 1, 'К уплате', 2, 'К возмещению', 3, 'Нулевая', '') as buyer_decl_sign
					  ,decl_buyer.submit_date as buyer_decl_submit_date
					  ,ask_buyer.summands as buyer_decl_nds_amount
					  ,UTL_GET_FULL_PERIOD_CODE(decl_buyer.FISCAL_YEAR, decl_buyer.PERIOD_CODE) as buyer_decl_period
					  ,SD.buyer_operation_code as buyer_operation_type
					  ,sd.invoice_chapter as buyer_source
					  ,decl_buyer.sur_code as buyer_sur_code
					  ,decl_seller.inn_contractor as seller_inn
					  ,decl_seller.inn_declarant as seller_inn_declarant
					  ,nvl(ask_seller.kppreorg, ask_seller.kppnp) as seller_kpp
					  ,ask_seller.kppnp as seller_kpp_declarant
					  ,seller_ks_agg.hasdiscrepancy_1_27 as seller_ks_1_27
					  ,decode(ask_seller.Nd_Prizn, 1, 'К уплате', 2, 'К возмещению', 3, 'Нулевая', '') as seller_decl_sign
					  ,decl_seller.submit_date as seller_decl_submit_date
					  ,ask_seller.summands as seller_decl_nds_amount
					  ,UTL_GET_FULL_PERIOD_CODE(decl_seller.FISCAL_YEAR, decl_seller.PERIOD_CODE) as seller_decl_period
					  ,sd.invoice_contractor_chapter as seller_source
					  ,decl_seller.sur_code as seller_sur_code
					  ,decl_seller.region_code as seller_region_code
					  ,decl_seller.sono_code as seller_sono_code
				  from doc_discrepancy dd
					  inner join sov_discrepancy sd on sd.id = dd.discrepancy_id
					  inner join ask_declandjrnl ask_buyer on ask_buyer.zip = sd.buyer_zip
					  inner join declaration_history decl_buyer on decl_buyer.zip = sd.buyer_zip
					  inner join dict_tax_period_map tpm_buyer on tpm_buyer.code = decl_buyer.period_code
					  left join seod_knp knp_buyer on knp_buyer.declaration_reg_num = decl_buyer.reg_number and knp_buyer.ifns_code = decl_buyer.sono_code
					  left join CONTROL_RATIO_AGGREGATE buyer_ks_agg on buyer_ks_agg.zip = decl_buyer.zip
					  inner join ask_declandjrnl ask_seller on ask_seller.zip = sd.seller_zip
					  inner join declaration_history decl_seller on decl_seller.zip = sd.seller_zip
					  inner join dict_tax_period_map tpm_seller on tpm_seller.code = decl_seller.period_code
					  left join seod_knp knp_seller on knp_seller.declaration_reg_num = decl_seller.reg_number and knp_seller.ifns_code = decl_seller.sono_code
					  left join CONTROL_RATIO_AGGREGATE seller_ks_agg on seller_ks_agg.zip = decl_seller.zip
                  where
                       dd.doc_id = line.doc_id
                       and sd.status = 1
					   and (
							   (sd.buyer_inn <> sd.seller_inn)
							or
							(
							   (sd.buyer_inn = sd.seller_inn)
							   and
							   (decl_buyer.kpp_effective <> decl_seller.kpp_effective)
							)
						   )
                       and (
                         (sd.side_primary_processing = 2
                         and decl_buyer.is_active = 1
                         and tpm_buyer.period = line.period
                         and knp_buyer.completion_date is null)
                         or
                         (sd.side_primary_processing <> 2
                         and decl_seller.is_active = 1
                         and tpm_seller.period = line.period
                         and knp_seller.completion_date is null))
              )
              select * from doc_dis
              where not exists
              (
                select doc_dis.id as dis_id
                from selection s
                inner join selection_discrepancy sd on sd.selection_id = s.id
                where sd.discrepancy_id = doc_dis.id
                and s.discrepancy_stage = C_SEL_SIDE_2_STATUS_CODE and rownum = 1
              )
           ) loop

          insert into selection_discrepancy ( 
				   discrepancy_id
                 , is_in_process
                 , selection_id
                 , zip
                 , seller_zip
                 , buyer_zip
                 , amount
                 , amountpvp
                 , sov_id
                 , founddate
                 , rule
                 , type
                 , stage
                 , status
                 , invoice_rk
                 , subtype_code
                 , side_primary_processing
                 , buyer_region_code
                 , buyer_sono_code
                 , buyer_inn
                 , buyer_inn_declarant
                 , buyer_kpp
                 , buyer_kpp_declarant
                 , buyer_ks_r8r3_quality
                 , buyer_decl_sign
                 , buyer_decl_submit_date
                 , buyer_decl_nds_amount
                 , buyer_decl_period
                 , buyer_operation_type
                 , buyer_source
                 , buyer_sur_code
                 , seller_inn
                 , seller_inn_declarant
                 , seller_kpp
                 , seller_kpp_declarant
                 , seller_ks_1_27
                 , seller_decl_sign
                 , seller_decl_submit_date
                 , seller_decl_nds_amount
                 , seller_decl_period
                 , seller_source
                 , seller_sur_code
                 , seller_region_code
                 , seller_sono_code
				 )
          values ( line_dis.id
                 , 1
                 , region_sel_collection(line_dis.region_code)
                 , line_dis.zip
                 , line_dis.seller_zip
                 , line_dis.buyer_zip
                 , line_dis.amnt
                 , line_dis.amount_pvp
                 , line_dis.sov_id
                 , line_dis.create_date
                 , line_dis.rule_num
                 , line_dis.type
                 , 2
                 , 1
                 , line_dis.invoice_rk
                 , line_dis.subtype_code
                 , line_dis.side_primary_processing
                 , line_dis.buyer_region_code
                 , line_dis.buyer_sono_code
                 , line_dis.buyer_inn
                 , line_dis.buyer_inn_declarant
                 , line_dis.buyer_kpp
                 , line_dis.buyer_kpp_declarant
                 , line_dis.buyer_ks_r8r3_quality
                 , line_dis.buyer_decl_sign
                 , line_dis.buyer_decl_submit_date
                 , line_dis.buyer_decl_nds_amount
                 , line_dis.buyer_decl_period
                 , line_dis.buyer_operation_type
                 , line_dis.buyer_source
                 , line_dis.buyer_sur_code
                 , line_dis.seller_inn
                 , line_dis.seller_inn_declarant
                 , line_dis.seller_kpp
                 , line_dis.seller_kpp_declarant
                 , line_dis.seller_ks_1_27
                 , line_dis.seller_decl_sign
                 , line_dis.seller_decl_submit_date
                 , line_dis.seller_decl_nds_amount
                 , line_dis.seller_decl_period
                 , line_dis.seller_source
                 , line_dis.seller_sur_code
                 , line_dis.seller_region_code
                 , line_dis.seller_sono_code );

                if sql%rowcount > 0 then
                  v_doc_dis_count := sql%rowcount;
                end if;
                insert into LOG_SELECTION_2nd_SIDE_BUILD(Process_Id, Doc_Id, Discrep_Processed) values(v_iteration_num, line.doc_id, v_doc_dis_count);
       end loop;
       update doc set processed = 1 where doc_id = line.doc_id;
    end;
  end loop;

  for region_line in (select * from v$ssrf) loop

    v_cnt_dis := 0;
    v_cnt_decl := 0;

    for line in (
					select
						x.zip as ZIP
					  , dh.type_code as TYPE_CODE
					  , dh.sur_code as SUR_CODE
					  , dh.inn_declarant as INN
					  , dh.kpp as KPP
					  , tp.name_full as NAME
					  , dh.sign as SIGN
					  , dh.nds_total as NDS_TOTAL
					  , x.selection_discrepancy_qty as DISCREPANCY_QTY
					  , djs.ch8_discrep_amnt as CH8_DISCREPANCY_AMT
					  , nvl(djs.ch9_discrep_amnt, 0) + nvl(djs.ch12_discrep_amnt, 0) as CH9_CH12_DISCREPANCY_AMT
					  , djs.discrep_total_amnt as DISCREPANCY_AMT
					  , djs.discrep_min_amnt as DISCREPANCY_AMT_MIN
					  , djs.discrep_max_amnt as DISCREPANCY_AMT_MAX
					  , djs.discrep_avg_amnt as DISCREPANCY_AMT_AVG
					  , djs.pvp_total_amnt as PVP
					  , djs.pvp_min_amnt as PVP_MIN
					  , djs.pvp_max_amnt as PVP_MAX
					  , djs.pvp_avg_amnt as PVP_AVG
					  , djs.ch8_pvp_amnt as CH8_PVP
					  , nvl(djs.ch9_pvp_amnt, 0) + nvl(djs.ch12_pvp_amnt, 0) as CH9_CH12_PVP
					  , dh.period_code as PERIOD_CODE
					  , dh.correction_number_effective as CORRECTION_NUMBER_EFFECTIVE
					  , tp.is_large as IS_LARGE
					  , dh.submit_date as SUBMIT_DATE
					  , dh.sono_code as SONO_CODE
					  , dh.region_code as REGION_CODE
					  , dh.inn_reorganized as INN_REORGANIZED
					from (
					   select sd.zip, count(1) as selection_discrepancy_qty
					   from selection_discrepancy sd
             where sd.selection_id = region_sel_collection(region_line.s_code)
					   group by sd.zip) x
					left join declaration_history dh on dh.zip = x.zip
					left join tax_payer tp on tp.inn = dh.inn_declarant and tp.kpp_effective = dh.kpp_effective
					left join declaration_journal_summary djs on djs.version_id = x.zip	
				)
    loop

        v_cnt_dis := v_cnt_dis + line.DISCREPANCY_QTY;
        v_cnt_decl := v_cnt_decl + 1;

        merge into selection_declaration sdecl
        using(select line.zip as zip, region_sel_collection(region_line.s_code) as sid from dual) mt
        on (sdecl.zip = mt.zip and mt.sid = sdecl.selection_id)
        when not matched then
          insert (
                    IS_IN_PROCESS,
                    SELECTION_ID,
                    ZIP,
                    TYPE_CODE,
                    SUR_CODE,
                    INN,
                    KPP,
                    NAME,
                    SIGN,
                    NDS_TOTAL,
                    DISCREPANCY_QTY,
                    CH8_DISCREPANCY_AMT,
                    CH9_CH12_DISCREPANCY_AMT,
                    DISCREPANCY_AMT,
                    DISCREPANCY_AMT_MIN,
                    DISCREPANCY_AMT_MAX,
                    DISCREPANCY_AMT_AVG,
                    PVP,
                    PVP_MIN,
                    PVP_MAX,
                    PVP_AVG,
                    CH8_PVP,
                    CH9_CH12_PVP,
                    PERIOD_CODE,
                    CORRECTION_NUMBER_EFFECTIVE,
                    IS_LARGE,
                    SUBMIT_DATE,
                    SONO_CODE,
                    REGION_CODE,
                    INN_REORGANIZED
                 )
          values (
                    1,
                    region_sel_collection(region_line.s_code), 
                    line.ZIP,
                    line.TYPE_CODE,
                    line.SUR_CODE,
                    line.INN,
                    line.KPP,
                    line.NAME,
                    line.SIGN,
                    line.NDS_TOTAL,
                    line.DISCREPANCY_QTY,
                    line.CH8_DISCREPANCY_AMT,
                    line.CH9_CH12_DISCREPANCY_AMT,
                    line.DISCREPANCY_AMT,
                    line.DISCREPANCY_AMT_MIN,
                    line.DISCREPANCY_AMT_MAX,
                    line.DISCREPANCY_AMT_AVG,
                    line.PVP,
                    line.PVP_MIN,
                    line.PVP_MAX,
                    line.PVP_AVG,
                    line.CH8_PVP,
                    line.CH9_CH12_PVP,
                    line.PERIOD_CODE,
                    line.CORRECTION_NUMBER_EFFECTIVE,
                    line.IS_LARGE,
                    line.SUBMIT_DATE,
                    line.SONO_CODE,
                    line.REGION_CODE,
                    line.INN_REORGANIZED
                 );


    end loop;


    if(v_cnt_dis > 0) then
     insert into selection(id, name, creation_date, status, type, decl_count, discrep_count, regions, discrepancy_stage)
     values(region_sel_collection(region_line.s_code), 'Выборка 2-й стороне регион '||region_line.s_code||' ('||region_sel_collection(region_line.s_code)||')', sysdate, 3, 1, v_cnt_decl, v_cnt_dis, region_line.s_code, C_SEL_SIDE_2_STATUS_CODE);

     insert into selection_filter(id, selection_id, filter) values (region_sel_collection(region_line.s_code), region_sel_collection(region_line.s_code), null);
    end if;

  end loop;
  commit;
end;

PROCEDURE P$SAVE_FILTER (
  pSelectionId IN SELECTION_FILTER.SELECTION_ID%type,
  pFilter IN  SELECTION_FILTER.FILTER%type)
as
begin
  UPDATE SELECTION_FILTER t SET Filter = pFilter
  WHERE t.SELECTION_ID = pSelectionId;
end;

PROCEDURE P$GET_FILTER (
  pSelectionId IN SELECTION_FILTER.SELECTION_ID%type,
  pCursor OUT SYS_REFCURSOR)
as
begin
  OPEN pCursor FOR select filter from selection_filter where selection_id = pSelectionId;
end;

end Pac$Selections;
/
