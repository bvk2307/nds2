﻿create or replace package NDS2_MRR_USER.PAC$TAX_PAYER_SETTINGS
as
procedure ADD_TAX_PAYER_ASSIGNMENT(
  p_sono_code NDS2_MRR_USER.IDA_TAX_PAYER_INDIVIDUAL_ASSG.S_CODE%type,
  p_inn NDS2_MRR_USER.IDA_TAX_PAYER_INDIVIDUAL_ASSG.INN%type,
  p_updated_by NDS2_MRR_USER.IDA_TAX_PAYER_INDIVIDUAL_ASSG.UPDATED_BY%type,
  p_is_active out NDS2_MRR_USER.IDA_TAX_PAYER_INDIVIDUAL_ASSG.IS_ACTIVE%type
  );
procedure REMOVE_TAX_PAYER_ASSIGNMENT(
  p_inn NDS2_MRR_USER.IDA_TAX_PAYER_INDIVIDUAL_ASSG.INN%type,
  p_updated_by NDS2_MRR_USER.IDA_TAX_PAYER_INDIVIDUAL_ASSG.UPDATED_BY%type,
  p_is_active out NDS2_MRR_USER.IDA_TAX_PAYER_INDIVIDUAL_ASSG.IS_ACTIVE%type
  );
procedure ADD_INSPECTOR_ASSIGNMENT(
  p_inn NDS2_MRR_USER.IDA_TAX_PAYER_INDIVIDUAL_ASSG.INN%type,
  p_sid NDS2_MRR_USER.IDA_TAX_PAYER_INDIVIDUAL_ASSG.SID%type,
  p_updated_by NDS2_MRR_USER.IDA_TAX_PAYER_INDIVIDUAL_ASSG.UPDATED_BY%type
  );
procedure CANCEL_ASSIGNMENT(
  p_inn NDS2_MRR_USER.IDA_TAX_PAYER_INDIVIDUAL_ASSG.INN%type,
  p_updated_by NDS2_MRR_USER.IDA_TAX_PAYER_INDIVIDUAL_ASSG.UPDATED_BY%type
  );
procedure UPDATE_WHITE_LIST;

end PAC$TAX_PAYER_SETTINGS;
/
create or replace package body NDS2_MRR_USER.PAC$TAX_PAYER_SETTINGS
as

  procedure Log(p_msg varchar2, p_type number)
  as
  pragma autonomous_transaction;
  begin
  
    dbms_output.put_line(p_type||'-'||p_msg);
  
    insert into NDS2_MRR_USER.SYSTEM_LOG(ID, TYPE, SITE, ENTITY_ID, MESSAGE_CODE, MESSAGE, LOG_DATE)
    values(Seq_Sys_Log_Id.Nextval, p_type, 'PAC$TAX_PAYER_SETTINGS', null, 1, p_msg, sysdate);
    commit;
  end;
  
  procedure LogInfo(p_msg varchar2)
  as
  begin
  	 Log(p_msg, 0);
  end;
  
  procedure LogError(p_msg varchar2)
  as
  begin
  	 Log(p_msg, 3);
  end;

  /*выполнение динамического однострочного запроса*/
  procedure ExecuteQuery(p_sql in varchar2)
  as
  begin
    execute immediate p_sql;
	exception when others then null;
  end;
  
  /*закрытие курсора*/
  procedure CloseCursor(p_hndl in out int)
    as
  begin
    if DBMS_SQL.is_open(p_hndl) then
         DBMS_SQL.close_cursor(p_hndl);
    end if;
  end;
  
  procedure ADD_TAX_PAYER_ASSIGNMENT(
  p_sono_code NDS2_MRR_USER.IDA_TAX_PAYER_INDIVIDUAL_ASSG.S_CODE%type,
  p_inn NDS2_MRR_USER.IDA_TAX_PAYER_INDIVIDUAL_ASSG.INN%type,
  p_updated_by NDS2_MRR_USER.IDA_TAX_PAYER_INDIVIDUAL_ASSG.UPDATED_BY%type,
  p_is_active out NDS2_MRR_USER.IDA_TAX_PAYER_INDIVIDUAL_ASSG.IS_ACTIVE%type
  )
  as
  begin
    merge into NDS2_MRR_USER.IDA_TAX_PAYER_INDIVIDUAL_ASSG tpia using
    (
      select p_sono_code  s_code, p_inn inn, 
      p_updated_by updated_by
      from dual
    ) assg
    on (tpia.inn = assg.inn)
    when matched then update
      set tpia.IS_ACTIVE = 1,
      tpia.updated_by = assg.updated_by,
      tpia.update_date = SYSDATE,
      tpia.sid = null
    when not matched then insert
      (
        tpia.s_Code,
        tpia.INN,
        tpia.Inserted_By,
        tpia.updated_by,
        tpia.Insert_Date,
        tpia.Update_Date,
        tpia.Is_Active
      )
      values
      (
        assg.s_code,
        assg.inn,
        assg.updated_by,
        assg.updated_by,
        SySDATE,
        SYSDATE,
        1
      );
      select IS_ACTIVE into p_is_active from v$taxpayer_assignment
      where inn = p_inn;
  end;

  procedure REMOVE_TAX_PAYER_ASSIGNMENT(
  p_inn NDS2_MRR_USER.IDA_TAX_PAYER_INDIVIDUAL_ASSG.INN%type,
  p_updated_by NDS2_MRR_USER.IDA_TAX_PAYER_INDIVIDUAL_ASSG.UPDATED_BY%type,
  p_is_active out NDS2_MRR_USER.IDA_TAX_PAYER_INDIVIDUAL_ASSG.IS_ACTIVE%type
  )
  as
  begin
    update NDS2_MRR_USER.IDA_TAX_PAYER_INDIVIDUAL_ASSG tpia
    set
      tpia.IS_ACTIVE = 0,
      tpia.Update_Date = SYSDATE,
      tpia.Updated_By = p_updated_by
    where tpia.inn = p_inn;
    select IS_ACTIVE into p_is_active from v$taxpayer_assignment
    where inn = p_inn;
  end;
  
  procedure ADD_INSPECTOR_ASSIGNMENT(
  p_inn NDS2_MRR_USER.IDA_TAX_PAYER_INDIVIDUAL_ASSG.INN%type,
  p_sid NDS2_MRR_USER.IDA_TAX_PAYER_INDIVIDUAL_ASSG.SID%type,
  p_updated_by NDS2_MRR_USER.IDA_TAX_PAYER_INDIVIDUAL_ASSG.UPDATED_BY%type
  )
  as
  begin
    update NDS2_MRR_USER.IDA_TAX_PAYER_INDIVIDUAL_ASSG tpia
    set
      tpia.SID = p_sid,
      tpia.UPDATE_DATE = SYSDATE,
      tpia.UPDATED_BY = p_updated_by
    where tpia.inn = p_inn;
  end;
  
  procedure CANCEL_ASSIGNMENT(
  p_inn NDS2_MRR_USER.IDA_TAX_PAYER_INDIVIDUAL_ASSG.INN%type,
  p_updated_by NDS2_MRR_USER.IDA_TAX_PAYER_INDIVIDUAL_ASSG.UPDATED_BY%type
  )
  as
  begin
    update NDS2_MRR_USER.IDA_TAX_PAYER_INDIVIDUAL_ASSG tpia
    set
      tpia.SID = null,
      tpia.UPDATE_DATE = SYSDATE,
      tpia.UPDATED_BY = p_updated_by
    where tpia.inn = p_inn;
  end;

  procedure UPDATE_WHITE_LIST
  as
	vSysDate		date := sysdate;
	vCurr_Month		number;
	vCurrent_Qtr	number;
	vPrev_Qtr		number;
    vCurr_Year		number;
    vPrev_Year		number;
	v_handle        int;
	v_type          dbms_sql.varchar2s;
	v_idx           number;
	v_rowcount      number;
  v_sql           varchar2(4000);
  begin
	LogInfo('(1) Начата актуализация IDA_TAX_PAYER_WHITE_LIST');
 
    vCurr_Month := to_number(to_char(vSysDate, 'MM'));
    vCurr_Year := to_number(to_char(vSysDate, 'YYYY'));
  	vCurrent_Qtr := (case 
               when vCurr_Month in (1,2,3) then 1 
               when vCurr_Month in (4,5,6) then 2 
               when vCurr_Month in (7,8,9) then 3
               when vCurr_Month in (10,11,12) then 4
               else 0 end);
    
  	if vCurrent_Qtr = 1 then 
  	  vPrev_Qtr := 4; 
  	  vPrev_Year := vCurr_Year - 1; 
  	else 
      vPrev_Qtr := vCurrent_Qtr - 1;
      vPrev_Year := vCurr_Year;
  	end if; 
	
	LogInfo('(2) Актуализация IDA_TAX_PAYER_WHITE_LIST: Квартал: '||vCurrent_Qtr);

	-- ExecuteQuery('');
    -- На всякий случай грохнем старые данные - что-бы наш скрипт не упал!
    ExecuteQuery('drop table NDS2_MRR_USER.IDA_TP_WHITE_LIST_TEMP');
	
    -- Создаем вспомогательную таблицу IDA_TP_WHITE_LIST_TEMP
	v_handle := dbms_sql.open_cursor;
	v_idx := 0;
	v_idx := v_idx + 1; v_type(v_idx) := 'create table NDS2_MRR_USER.IDA_TP_WHITE_LIST_TEMP TableSpace NDS2_DATA ';
	v_idx := v_idx + 1; v_type(v_idx) := 'as ';
	v_idx := v_idx + 1; v_type(v_idx) := 'select INN, KPP, SONO_CODE, NAME ';
	v_idx := v_idx + 1; v_type(v_idx) := 'from ( ';
	v_idx := v_idx + 1; v_type(v_idx) := '  select ';
	v_idx := v_idx + 1; v_type(v_idx) := '  row_number() over (partition by d.INNNP order by d.NOMKORR desc, z.DATAFAJLA asc) as RN, ';
	v_idx := v_idx + 1; v_type(v_idx) := '  d.INNNP as INN, ';
	v_idx := v_idx + 1; v_type(v_idx) := '  d.KPPNP as KPP, ';
	v_idx := v_idx + 1; v_type(v_idx) := '  d.KODNO as SONO_CODE, ';
	v_idx := v_idx + 1; v_type(v_idx) := '  (case when length(d.INNNP) = 10 then d.NAIMORG else d.FAMILIYANP||'' ''||d.IMYANP||'' ''||d.OTCHESTVONP end) as NAME ';
	v_idx := v_idx + 1; v_type(v_idx) := '  from v$askdekl d ';
	v_idx := v_idx + 1; v_type(v_idx) := '  inner join v$askzipfajl z on z.ID = d.ZIP ';
	v_idx := v_idx + 1; v_type(v_idx) := '  inner join dict_tax_period_map tax_per_map on tax_per_map.code = d.period ';
	v_idx := v_idx + 1; v_type(v_idx) := '  where  ';
	v_idx := v_idx + 1; v_type(v_idx) := '  (d.OTCHETGOD = '||vCurr_Year||' and tax_per_map.code = ' || vCurrent_Qtr|| ')   ';
	v_idx := v_idx + 1; v_type(v_idx) := '  or (d.OTCHETGOD = '||vPrev_Year||' and tax_per_map.code = '||vPrev_Qtr||')  ';
	v_idx := v_idx + 1; v_type(v_idx) := ') T ';
	v_idx := v_idx + 1; v_type(v_idx) := 'where T.RN = 1 '||chr(10);
    
    for i in 1 .. v_type.count
    loop
      v_sql := v_sql || v_type (i);
    end loop;
    LogInfo(v_sql);
	DBMS_SQL.PARSE (c  => v_handle, statement => v_type, lb => 1, ub => v_type.count, lfflg => false, language_flag => dbms_sql.native );
	v_rowcount := dbms_sql.execute(v_handle);
	CloseCursor(v_handle);

	-- Отписываемся в Лог
	LogInfo('(3) Успешное создание NDS2_MRR_USER.IDA_TP_WHITE_LIST_TEMP ');

	-- Удалям индексы с IDA_TAX_PAYER_WHITE_LIST
    begin
	    PAC$INDEX_MANAGER.P$DROP_INDEXES('IDA_TAX_PAYER_WHITE_LIST');
		LogInfo('(4) Удалены индексы на IDA_TAX_PAYER_WHITE_LIST');
		exception when others then
		LogError('(4) Ошибка удаления индексов IDA_TAX_PAYER_WHITE_LIST: '||substr(sqlerrm, 1, 256));
	end;

	--  Делаем бэкап: переименовываем IDA_TAX_PAYER_WHITE_LIST в IDA_TP_WHITE_LIST_BAK
	ExecuteQuery('alter table IDA_TAX_PAYER_WHITE_LIST rename to IDA_TP_WHITE_LIST_BAK');
	-- Переименовываем IDA_TP_WHITE_LIST_TEMP в IDA_TAX_PAYER_WHITE_LIST
	ExecuteQuery('alter table IDA_TP_WHITE_LIST_TEMP rename to IDA_TAX_PAYER_WHITE_LIST');
	-- Отписываемся в Лог
	LogInfo('(5) Успешная замена данных на актуальные');

  --Делаем не активными индивидуальные назначения
  --если НП больше не принадлежит инспекции
  update NDS2_MRR_USER.IDA_TAX_PAYER_INDIVIDUAL_ASSG a
  set a.is_active = 0
  where not exists
  (
     select l.inn
       from NDS2_MRR_USER.IDA_TAX_PAYER_WHITE_LIST l
      where l.inn = a.inn and l.sono_code = a.s_code
  );

	-- Стряпаем индексы
	PAC$INDEX_MANAGER.P$VALIDATE_INDEXES('IDA_TAX_PAYER_WHITE_LIST');
	-- Отписываемся в Лог
	LogInfo('(6) Успешное создание индексов на IDA_TAX_PAYER_WHITE_LIST');
    
	-- Соберем статистику
	dbms_stats.gather_table_stats(ownname => 'NDS2_MRR_USER', tabname => 'IDA_TAX_PAYER_WHITE_LIST');
	-- Отписываемся в Лог
	LogInfo('(7) Обновлена статистика');
	
	-- Удаляем IDA_TP_WHITE_LIST_BAK
    execute immediate 'Alter session set ddl_lock_timeout = 300';
	ExecuteQuery('drop table NDS2_MRR_USER.IDA_TP_WHITE_LIST_BAK');
	execute immediate 'Alter session set ddl_lock_timeout = 0';
	
	LogInfo('(8) Окончена актуализация агрегата IDA_TAX_PAYER_WHITE_LIST');
  end;

end PAC$TAX_PAYER_SETTINGS;
/
