﻿CREATE OR REPLACE PACKAGE NDS2_MRR_USER.PAC$SEOD
as
procedure P$HIST_EXPLAIN_REPLY_STATUS_I(v_explain_id number);

procedure P$REGISTER_DECL_ANNULMENT(p_annulment_id number);

procedure P$CHANGE_ANNULMENT_STATUS(p_annulment_id number, status_id number, change_date date);

procedure P$REFRESH_ANNULMENT_REQUESTS;

end;
/

CREATE OR REPLACE PACKAGE BODY NDS2_MRR_USER.PAC$SEOD
as

XSD_NDS2_CAM_06 CONSTANT varchar2(32 char)    := 'TAX3EXCH_NDS2_CAM_06_01.xsd';

procedure P$HIST_EXPLAIN_REPLY_STATUS_I(v_explain_id number)
as
	 v_hist_explain_id number;
begin
	 v_hist_explain_id := NDS2_MRR_USER.SEQ_HIST_EXPLAIN_REPLY.NEXTVAL();
	 
	 insert into NDS2_MRR_USER.hist_explain_reply_status(id, explain_id, submit_date, status_id)
	 values(v_hist_explain_id, v_explain_id, sysdate, 1);
end;

procedure RECALCULATE_KNP_STATUS(p_zip number, p_knp_reopen_needed number, p_close_date date)
as
begin
	update nds2_mrr_user.declaration_knp_status set calc_is_open = 0, calc_status = 3, calc_status_date = p_close_date
	where zip = p_zip and calc_is_open = 1;
	
	update nds2_mrr_user.declaration_active set status_knp = 'Закрыта', status_knp_code = 2, knp_close_reason_id = 3
	where zip = p_zip;
	
	update nds2_mrr_user.declaration_active_search set status = 'Закрыта'
	where declaration_version_id = p_zip;
end;


procedure P$REG_CANCELLATION_FOR_DECL(p_zip number)
as
begin
		update nds2_mrr_user.declaration_active set has_cancelled_correction = 1
		where zip = p_zip;
end;

procedure P$REG_CANCELLATION_FOR_CORR(p_zip number)
as
begin
		update nds2_mrr_user.declaration_active set has_cancelled_correction = 1, cancelled = 1
		where zip = p_zip;
end;

procedure P$REGISTER_DECL_ANNULMENT(p_annulment_id number)
as
	v_id number; 
	v_history_id number;
	v_close_date date;
	v_mc_id number;
	v_inn varchar2(12);
	v_kpp varchar2(9);
	v_year number;
	v_period varchar2(2);
	v_knp_reopen_needed number;
	v_zip number;
begin
	v_id := NDS2_MRR_USER.SEQ_DECL_ANNULMENT.NEXTVAL();
	v_history_id := NDS2_MRR_USER.SEQ_DECL_ANNULMENT_HISTORY.NEXTVAL();
	
  select da.inn, dh.kpp_effective, da.fiscal_year, da.period_code, da.knp_reopen_needed, dh.zip 
  into v_INN, v_KPP, v_year, v_period, v_knp_reopen_needed, v_zip
  from
  (
      select inn,kpp,fiscal_year,period_code, correction_number, knp_reopen_needed 
      from nds2_seod.seod_declaration_annulment
      where id = p_annulment_id
  ) da
  left join nds2_mrr_user.declaration_history dh 
      on dh.inn_contractor = da.inn 
         and dh.kpp = da.kpp
         and dh.fiscal_year = da.fiscal_year
         and dh.period_code = da.period_code
         and dh.correction_number = da.correction_number;
	
	insert into NDS2_MRR_USER.DECLARATION_ANNULMENT(ID, SEOD_ID, ACTIVE_BEFORE, DOCUMENT_BODY)
	values(v_id, p_annulment_id, null, null);
	
    P$CHANGE_ANNULMENT_STATUS(v_id, 1, sysdate());
	
	select ur.cancelled_since, ad.id  into v_close_date, v_mc_id 
	from (select * from nds2_seod.seod_declaration_annulment where id = p_annulment_id) ur
	left join v$askdekl ad 
			               on ad.innnp = ur.inn and ad.kppnp = ur.kpp
							  and ad.period = ur.period_code and ad.otchetgod = ur.fiscal_year
							  and ad.nomkorr = ur.correction_number
	;
	
	if (v_mc_id is not null) then
		nds2_mrr_user.pac$claim_life_cycle.P$CLOSING_CLAIMS_BY_ANNULMENT(p_annulment_id, v_close_date);
		RECALCULATE_KNP_STATUS(v_zip, v_knp_reopen_needed, v_close_date);
		NDS2_MRR_USER.PAC$REMARKABLE_CHANGES.RESET_CHANGES(v_inn, v_kpp, v_year, v_period);
		
		P$REG_CANCELLATION_FOR_CORR(v_zip);
		
	else
		P$CHANGE_ANNULMENT_STATUS(v_id, 4, sysdate());
	end if;	
end;

procedure P$CHANGE_ANNULMENT_STATUS(p_annulment_id number, status_id number, change_date date)
as
	v_count number;
	v_id number;
begin
	select count(1) into v_count from nds2_seod.seod_declaration_annulment where ID = p_annulment_id;
	
	if (v_count <> 1) then
		nds2_mrr_user.nds2$sys.log_error('PAC$SEOD', -1, -20000, 'Заявка на аннулирование не идентифицирована. ID ='||p_annulment_id );
		raise_application_error('Ошибка идентификации заявки на аннулирование', -20001);
	end if;
	
	select count(1) into v_count from nds2_mrr_user.DECLARATION_ANNULMENT_STATUS where ID = status_id;
	
	if (v_count <> 1) then
		nds2_mrr_user.nds2$sys.log_error('PAC$SEOD', -1, -20000, 'Статус заявки на аннулирование не идентифицирован. Status_ID ='||status_id );
		raise_application_error('Ошибка идентификации статуса заявки на аннулирование', -20001);
	end if;

	update nds2_mrr_user.DECLARATION_ANNULMENT_HISTORY set IS_LAST = 0 where ANNULMENT_ID = p_annulment_id;

	v_id := NDS2_MRR_USER.SEQ_DECL_ANNULMENT_HISTORY.NEXTVAL();
	
	insert into NDS2_MRR_USER.DECLARATION_ANNULMENT_HISTORY(ID, Annulment_ID, Status_ID, Status_Date)
	values(v_id, p_annulment_id, status_id, change_date);

	commit;	
end;

procedure P$CREATE_ASK6_RESPONSE(p_annulment_id number, p_result number)
as
v_INN varchar2(12);
v_KPP varchar2(9);
v_sono varchar2(4);
v_period varchar2(2);
v_year varchar2(4);
v_corr_num number;
v_file_id varchar2(100);
v_reg_num number;
v_result number;

doc xmldom.DOMDocument;
docXml xmltype;
main_node xmldom.DOMNode;
root_elmt xmldom.DOMElement;
doc_elmt xmldom.DOMElement;
np_elmt xmldom.DOMElement;
np_details_elmt xmldom.DOMElement;

v_node xmldom.DOMNode;
root_node xmldom.DOMNode;
np_node xmldom.DOMNode;
np_details_node xmldom.DOMNode;
doc_node xmldom.DOMNode;

begin
	select INN, KPP, SONO_CODE, REG_NUMBER, FISCAL_YEAR, PERIOD_CODE, ID_FILE, CORRECTION_NUMBER 
	into v_INN, v_KPP, v_sono, v_reg_num, v_year, v_period, v_file_id, v_corr_num
	from nds2_mrr_user.V$DECLARATION_ANNULMENT
	where ID = p_annulment_id;
	
	v_result := p_result;
	
	doc := xmldom.newDOMDocument();
	main_node := xmldom.makeNode(doc);
	root_elmt := xmldom.createElement(doc, 'Файл');
	
	xmldom.setAttribute(root_elmt, 'ИдФайл', v_file_id);
	xmldom.setAttribute(root_elmt, 'xsi:noNamespaceSchemaLocation', XSD_NDS2_CAM_06);
    xmldom.setAttribute(root_elmt, 'xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
	
	root_node := xmldom.appendChild(main_node, xmldom.makeNode(root_elmt));
	
	doc_elmt := xmldom.CreateElement(doc, 'Документ');
	doc_node := xmldom.appendChild(root_node, xmldom.makeNode(doc_elmt));
	
	xmldom.setAttribute(doc_elmt, 'КодНО', v_sono);
	xmldom.setAttribute(doc_elmt, 'РегНомДек', v_reg_num);
	xmldom.setAttribute(doc_elmt, 'Период', v_period);
	xmldom.setAttribute(doc_elmt, 'ОтчетГод', v_year);
	xmldom.setAttribute(doc_elmt, 'НомКорр', v_corr_num);
	xmldom.setAttribute(doc_elmt, 'КодАннул', v_result);
	
	np_elmt := xmldom.CreateElement(doc, 'СвНП'); 
	
	if (LENGTH(v_INN) = 12) then
		np_details_elmt := xmldom.CreateElement(doc, 'НПФЛ');
		xmldom.setAttribute(np_details_elmt, 'ИННФЛ', v_INN);
	else
		np_details_elmt := xmldom.CreateElement(doc, 'НПЮЛ');
		xmldom.setAttribute(np_details_elmt, 'ИННЮЛ', v_INN);
		xmldom.setAttribute(np_details_elmt, 'КПП', v_KPP);		
	end if;
	
	np_node := xmldom.appendChild(doc_node, xmldom.makeNode(np_elmt));
	np_details_node := xmldom.appendChild(np_node, xmldom.makeNode(np_details_elmt));
	
	begin
	
		docXml := dbms_xmldom.getXmlType(doc);
		docXml := docXml.createSchemaBasedXML(XSD_NDS2_CAM_06);
		xmltype.schemaValidate(docXml);
	
		update nds2_mrr_user.DECLARATION_ANNULMENT set DOCUMENT_BODY = docXml.getClobVal(), SEND_STATUS_ID = 1 WHERE ID = p_annulment_id;
		commit;
		
	exception when others then
        nds2_mrr_user.nds2$sys.log_error('CREATE_ASK6_RESPONSE:', p_annulment_id, -1, 'XSD validation error'||substr(sqlerrm, 1, 512));
		P$CHANGE_ANNULMENT_STATUS(p_annulment_id, 3, sysdate);
    end;
end;

procedure P$REFRESH_ANNULMENT_REQUESTS
as
	v_last_pc_date date;
	v_wait_period number;
begin
	select max("ДатаОконч") into v_last_pc_date from nds2_mc."ASKОперация" where "ВидОп"=3;
	begin
	select value into v_wait_period from nds2_mrr_user.configuration where parameter = 'decl_cancelation_wait_period';
		exception when NO_DATA_FOUND then 
		begin
			nds2_mrr_user.nds2$sys.log_error('PAC$SEOD.P$REFRESH_ANNULMENT_REQUESTS', -1, sqlcode, 'parameter "decl_cancelation_wait_period" was not found');
			raise;
		end;
		when others then
		begin
			nds2_mrr_user.nds2$sys.log_error('PAC$SEOD.P$REFRESH_ANNULMENT_REQUESTS', -1, sqlcode, 'parameter "decl_cancelation_wait_period" is incorrect');
			raise;
		end;	  
	end;  
	  
	  
	  for cur in 
	  (
		select ur.id, ur.active_before, ad.id as decl_id, ur.status_id, ad."PriznAnnul", ad."DataAnnul"
		from
		(select * from V$DECLARATION_ANNULMENT where status_id in (1,4)) ur
		left join nds2_mrr_user.V$ASKDEKL ad 
		                                on ad.innnp = ur.inn and ad.kppnp = ur.kpp
									   and ad.period = ur.period_code and ad.otchetgod = ur.fiscal_year
									   and ad.nomkorr = ur.correction_number
	  ) loop
	  
	  if (cur.status_id = 4) then
		  if (cur.decl_id is not null and (cur.active_before is null or cur.active_before > sysdate())) then
			P$CHANGE_ANNULMENT_STATUS(cur.id, 1, sysdate());
		  else 
			if (cur.decl_id is null and cur.active_before is null) then
				update nds2_mrr_user.declaration_annulment set active_before = sysdate() + v_wait_period where id = cur.id;
			else 
				P$CHANGE_ANNULMENT_STATUS(cur.id, 5, sysdate());
				P$CREATE_ASK6_RESPONSE(cur.id, 0);
			end if;
		  end if;
	  else
		if (cur."PriznAnnul" = 2 and cur."DataAnnul" is not null) then
			P$CHANGE_ANNULMENT_STATUS(cur.id, 2, cur."DataAnnul");
			P$CHANGE_ANNULMENT_STATUS(cur.id, 5, sysdate());
			P$CREATE_ASK6_RESPONSE(cur.id, 1);
		else 
			if (cur."PriznAnnul" = 1 and cur."DataAnnul" is null) then
				P$CHANGE_ANNULMENT_STATUS(cur.id, 3, sysdate());
				P$CREATE_ASK6_RESPONSE(cur.id, 1);
			end if;
		end if;
	  end if;
	  end loop;
	
end;
	
end;
/

grant execute on nds2_mrr_user.PAC$SEOD TO NDS2_seod;
