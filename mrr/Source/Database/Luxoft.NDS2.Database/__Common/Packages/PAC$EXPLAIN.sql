﻿CREATE OR REPLACE PACKAGE NDS2_MRR_USER."PAC$EXPLAIN" IS

PROCEDURE P$UPDATE_COMMENT(pId in NUMBER, pType in NUMBER, pComment in VARCHAR2);

PROCEDURE P$SEARCH_BY_CLAIM(pDocId in DOC.DOC_ID%type, pResult out SYS_REFCURSOR);

PROCEDURE P$GET(pId in SEOD_EXPLAIN_REPLY.EXPLAIN_ID%type, pResult out SYS_REFCURSOR);

PROCEDURE P$GET_COMMENT(pId in SEOD_EXPLAIN_REPLY.EXPLAIN_ID%type, pType in number, pComment out SYS_REFCURSOR);

PROCEDURE P$GET_CONTROL_RATIO_LIST(pZip in number, pResult out SYS_REFCURSOR);

PROCEDURE p$GET_EXPLAINS_BY_DECLARATION( 
  pInnDeclarant in varchar2,
  pInnContractor in varchar2,
  pKppEffective in varchar2,
  pPeriodEffective in number,
  pYear in varchar2,
  pCursor out SYS_REFCURSOR
);

END;
/

CREATE OR REPLACE PACKAGE BODY NDS2_MRR_USER.PAC$EXPLAIN IS
PROCEDURE P$UPDATE_COMMENT(pId in NUMBER, pType in NUMBER, pComment in VARCHAR2)
as
BEGIN 
merge into seod_explain_comment t1 using dual
	           on (explain_id = pId and explain_type_id = pType)
when matched then update set t1.user_comment = pComment
when not matched then insert (explain_id, explain_type_id, user_comment )
					      values (pId, pType, pComment); 
END;

PROCEDURE P$SEARCH_BY_CLAIM(pDocId in DOC.DOC_ID%type, pResult out SYS_REFCURSOR)
as
BEGIN
OPEN pResult FOR
select er.explain_id as "Id",
er.incoming_num as "Number",
er.incoming_date as "Date",
case when er.receive_by_tks = 1 then 1 else 0 end as ReceivedByTks,
nvl(er_local.status_id, er.status_id) as Status,
nvl(er_local.status_set_date,er.status_set_date) as StatusDate,
rs.name as StatusName,
nvl(rt.short_name, '') as TypeName,
case when vp.PoyasnInOsn is not null and er.receive_by_tks = 1 then 1 else 0 end as HasOtherReasonExplain,
case when exists (select 1 from v$askpoyasnenieks ask where ask.zip = vp.zip and ask.zip is not null) and er.receive_by_tks = 1 then 1 else 0 end as HasControlRatioExplain,
case when er.type_id = 1 and vp.KolZapVsego > 0 and er.receive_by_tks = 1 then 1 else 0 end as HasInvoicesExplain
from 
 nds2_seod.seod_explain_reply er
 left join nds2_mrr_user.seod_explain_reply er_local on er_local.explain_id = er.explain_id
 join DOC dc on dc.doc_id = er.doc_id
 join DICT_EXPLAIN_REPLY_TYPE rt on rt.id = er.type_id
 join DICT_EXPLAIN_REPLY_STATUS rs on rs.id = nvl(er_local.status_id, er.status_id)
 left join v$askpoyasnenie vp on (vp.ImyaFaylTreb = er.filenameoutput and vp.KodNO = dc.sono_code and nvl(er_local.status_id, er.status_id) = vp.OBRABOTANMS) or (vp.zip = er.zip)
 where er.doc_id = pDocId;
END;

PROCEDURE P$GET(pId in SEOD_EXPLAIN_REPLY.EXPLAIN_ID%type, pResult out SYS_REFCURSOR)
as
begin
open pResult for 
select er.explain_id as "Id",
nvl(vp.zip, nvl(er_local.zip, er.zip)) as zip, 
er.incoming_num as "Number",
er.incoming_date as "Date",
rs.name  as "Status",
er.doc_id as ClaimId,
decode(dc.Seod_Accepted, 1, TO_CHAR(dc.EXTERNAL_DOC_NUM), '*'||TO_CHAR(dc.DOC_ID)) as ClaimNumber,
decode(dc.Seod_Accepted, 1, dc.SEOD_ACCEPT_DATE, NULL) as ClaimDate,
vp.PoyasnInOsn as Reply,
dh.zip as DeclarationZip
   from nds2_seod.seod_explain_reply er
    left join nds2_mrr_user.seod_explain_reply er_local on er_local.explain_id = er.explain_id
    join DOC dc on dc.doc_id = er.doc_id
	join declaration_history dh on dc.ref_entity_id = dh.reg_number and dc.sono_code = dh.sono_code_submited
	join DICT_EXPLAIN_REPLY_STATUS rs on rs.id = nvl(er_local.status_id, er.status_id)
    left join v$askpoyasnenie vp on (vp.ImyaFaylTreb = er.filenameoutput and vp.KodNO = dc.sono_code and nvl(er_local.status_id, er.status_id) = vp.OBRABOTANMS) or (vp.zip = nvl(er_local.zip,er.zip))
    where er.explain_id = pId and er.type_id = 1;
end;

PROCEDURE P$GET_COMMENT(pId in SEOD_EXPLAIN_REPLY.EXPLAIN_ID%type, pType in number, pComment out SYS_REFCURSOR)
as
begin
open pComment for
select user_comment from seod_explain_comment where explain_id = pId and explain_type_id = pType;
end;

PROCEDURE P$GET_CONTROL_RATIO_LIST(pZip in number, pResult out SYS_REFCURSOR)
as
begin
open pResult for
select NomKs as ControlRatioCode, PoyasnKs as ExplainText from v$askpoyasnenieks where  zip = pZip;
end;

PROCEDURE p$GET_EXPLAINS_BY_DECLARATION( 
  pInnDeclarant in varchar2,
  pInnContractor in varchar2,
  pKppEffective in varchar2,
  pPeriodEffective in number,
  pYear in varchar2,
  pCursor out SYS_REFCURSOR
)
as
begin
OPEN pCursor FOR
select er.explain_id as "Id",
er.incoming_num as "Number",
er.incoming_date as "Date",
nvl(er.receive_by_tks,0) as ReceivedByTks,
nvl(er_local.status_id, er.status_id) as Status,
nvl(er_local.status_set_date, er.status_set_date) as StatusDate,
rs.name as StatusName,
doc.doc_id as ClaimId,
nvl(rt.short_name, '') as TypeName,
case when vp.PoyasnInOsn is not null and er.receive_by_tks = 1 then 1 else 0 end as HasOtherReasonExplain,
case when exists (select 1 from v$askpoyasnenieks ask where ask.zip = vp.zip and ask.zip is not null) and er.receive_by_tks = 1 then 1 else 0 end as HasControlRatioExplain,
case when er.type_id = 1 and vp.KolZapVsego > 0 and er.receive_by_tks = 1 then 1 else 0 end as HasInvoicesExplain
from nds2_seod.seod_explain_reply er
 left join nds2_mrr_user.seod_explain_reply er_local on er.explain_id = er_local.explain_id
 join DOC doc on doc.doc_id = er.doc_id
 join DECLARATION_HISTORY decl on DECL.REG_NUMBER = doc.ref_entity_id and decl.SONO_CODE_SUBMITED = doc.SONO_CODE
 join DICT_EXPLAIN_REPLY_TYPE rt on rt.id = er.type_id
 join DICT_EXPLAIN_REPLY_STATUS rs on rs.id = nvl(er_local.status_id, er.status_id)
 left join v$askpoyasnenie vp on (vp.ImyaFaylTreb = er.filenameoutput and vp.KodNO = doc.sono_code and nvl(er_local.status_id, er.status_id) = vp.OBRABOTANMS) or (vp.zip = er.zip)
        where DECL.INN_DECLARANT = pInnDeclarant 
		 and  ((pInnContractor is not null and DECL.INN_CONTRACTOR = pInnContractor) or pInnContractor is null)
		 and DECL.KPP_EFFECTIVE = pKppEffective and DECL.TYPE_CODE = 0
         and DECL.PERIOD_effective = pPeriodEffective and DECL.FISCAL_YEAR = pYear;
end;

END;
/
