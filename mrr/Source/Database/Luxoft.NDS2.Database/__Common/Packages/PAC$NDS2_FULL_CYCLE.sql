﻿create or replace package NDS2_MRR_USER.PAC$NDS2_FULL_CYCLE
as
/*********************************************
Обновление предагрегата CONTROL_RATIO_R8R3_AGGREGATE
**********************************************/
procedure P$0_UPD_CONTROLRATIOAGGREGATE;
/**********************************************
Обновление витрины для списка расхождений для НД
**********************************************/
procedure P$2_UPDATE_DECL_DISCREP_LIST;
/*********************************************
Обновление таблицы оценок значимых изменений
DECLARATION_EXPLAIN_SUMMARY
**********************************************/
procedure P$9_UPDATE_DECL_EXPL_SUMM;
/*********************************************
Обновление агрегата DECLARATION_REQUEST_COUNTER
**********************************************/
procedure P$11_UPDATE_DECL_REQ_CNT;
/********************************************************
Расширение P$UPDATE_DICREPANCY,
реализующее дополнительные операции:
перестроение агрегатов для ООР.
********************************************************/
procedure P$UPDATE_DICREPANCY_EXT;
/********************************************************
Расширение P$UPDATE_DICREPANCY, реализующее
ежедневные дополнительные операции перестроения
агрегатов для ООР
********************************************************/
procedure P$UPDATE_DICREPANCY_DAILY;

/**********************************************
обновление очереди отправки протоколов КС в ЭОД
**********************************************/
procedure P$QUEUE_NEW_CONTROL_RATIO;
/**********************************************
расчет партиций под новые данные кеша СФ
**********************************************/
procedure P$INVOICE_CACHE_PART_CALC;
/**********************************************
создание новых партиций для кеша СФ
**********************************************/
procedure P$INVOICE_CACHE_BUILD_PART;
/**********************************************
Начало ПЦ
**********************************************/
procedure P$ON_START;
/**********************************************
Окончание ПЦ
**********************************************/
procedure P$ON_COMPLETE;
/**********************************************
Получить текущее значение счетчика ПЦ
**********************************************/
procedure P$GET_COMPARISON_DATA_VERSION( pCount OUT number);
end;
/
create or replace package body NDS2_MRR_USER.PAC$NDS2_FULL_CYCLE
is
/*выполнение динамического однострочного запроса*/
procedure P$EXECUTE_SHORT_QUERY(p_sql in varchar2)
as
begin
  execute immediate p_sql;
exception when others then null;
end;

/*закрытие курсора*/
procedure P$UTL_CLOSE_CURSOR_HANDLE(p_hndl in out int)
  as
begin
  if DBMS_SQL.is_open(p_hndl) then
       DBMS_SQL.close_cursor(p_hndl);
  end if;
end;

/*логирование*/
procedure P$UTL_LOG(
  p_module_name in varchar2,
  p_msg in varchar2,
  p_code number,
  p_msg_code varchar2 := null
  )
  as
  pragma autonomous_transaction;
begin
  dbms_output.put_line('debug log: '||p_msg);
  insert into system_log(id, type, site, entity_id, message_code, message, log_date)
  values(seq_sys_log_id.nextval, p_code, p_module_name, null, p_msg_code, p_msg, sysdate);
  commit;

  exception when others then
    rollback;
end;

/*********************************************
Обновление предагрегата CONTROL_RATIO_R8R3_AGGREGATE
**********************************************/
procedure P$0_UPD_CONTROLRATIOAGGREGATE
as
v_handle        int;
v_type          dbms_sql.varchar2s;
v_idx           number;
v_rowcount      number;
begin

  P$EXECUTE_SHORT_QUERY('drop table NDS2_MRR_USER.CONTROL_RATIO_AGGREGATE');

  P$UTL_LOG(
            p_module_name => 'PAC$NDS2_FULL_CYCLE.P$0_UPD_CONTROLRATIOAGGREGATE',
            p_msg => '(0) Начато обновление агрегата CONTROL_RATIO_AGGREGATE',
            p_code => 1,
            p_msg_code => 0
           );


  -- Создаем вспомогательную таблицу DECLARATION_REQ_CNT_TEMP
  v_handle := dbms_sql.open_cursor;
  v_idx := 0;
  v_idx := v_idx + 1; v_type(v_idx) := 'create table NDS2_MRR_USER.CONTROL_RATIO_AGGREGATE TableSpace NDS2_DATA ';
  v_idx := v_idx + 1; v_type(v_idx) := 'as ';
  v_idx := v_idx + 1; v_type(v_idx) := 'select ';
  v_idx := v_idx + 1; v_type(v_idx) := '  y.ZIP, ';
  v_idx := v_idx + 1; v_type(v_idx) := '  case ';
  v_idx := v_idx + 1; v_type(v_idx) := '    when (y.left_1_28 is null) or (nvl(y.left_1_33, y.left_1_32) is null) then null ';
  v_idx := v_idx + 1; v_type(v_idx) := '    when y.left_1_28 = nvl(y.left_1_33, y.left_1_32) then 1 ';
  v_idx := v_idx + 1; v_type(v_idx) := '    else 0 ';
  v_idx := v_idx + 1; v_type(v_idx) := '  end as r8r3_equality, ';
  v_idx := v_idx + 1; v_type(v_idx) := '  y.left_1_28, ';
  v_idx := v_idx + 1; v_type(v_idx) := '  y.left_1_32, ';
  v_idx := v_idx + 1; v_type(v_idx) := '  y.left_1_33, ';
  v_idx := v_idx + 1; v_type(v_idx) := '  case ';
  v_idx := v_idx + 1; v_type(v_idx) := '      when y.vypoln_1_27 = 0 then 1 ';
  v_idx := v_idx + 1; v_type(v_idx) := '      else 0 ';
  v_idx := v_idx + 1; v_type(v_idx) := '  end as hasdiscrepancy_1_27, ';
  v_idx := v_idx + 1; v_type(v_idx) := '  y.vypoln_1_27 ';
  v_idx := v_idx + 1; v_type(v_idx) := 'from ';
  v_idx := v_idx + 1; v_type(v_idx) := '( ';
  v_idx := v_idx + 1; v_type(v_idx) := '  select ';
  v_idx := v_idx + 1; v_type(v_idx) := '    ZIP, ';
  v_idx := v_idx + 1; v_type(v_idx) := '    MIN(x.vypoln_1_27) as vypoln_1_27, ';
  v_idx := v_idx + 1; v_type(v_idx) := '    MIN(x.left_1_28) as left_1_28, ';
  v_idx := v_idx + 1; v_type(v_idx) := '    MIN(x.left_1_32) as left_1_32, ';
  v_idx := v_idx + 1; v_type(v_idx) := '    MIN(x.left_1_33) as left_1_33 ';
  v_idx := v_idx + 1; v_type(v_idx) := '  from ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ( ';
  v_idx := v_idx + 1; v_type(v_idx) := '    select ';
  v_idx := v_idx + 1; v_type(v_idx) := '      SVDZP.ZIP, ';
  v_idx := v_idx + 1; v_type(v_idx) := '      case when KODKS=''1.27'' then MIN(VYPOLN) else null end as vypoln_1_27, ';
  v_idx := v_idx + 1; v_type(v_idx) := '      case when KODKS=''1.28'' then MIN(LEVYACHAST) else null end as left_1_28, ';
  v_idx := v_idx + 1; v_type(v_idx) := '      case when KODKS=''1.32'' then MIN(LEVYACHAST) else null end as left_1_32, ';
  v_idx := v_idx + 1; v_type(v_idx) := '      case when KODKS=''1.33'' then MIN(LEVYACHAST) else null end as left_1_33 ';
  v_idx := v_idx + 1; v_type(v_idx) := '    from V$ASKKONTRSOONTOSH KS ';
  v_idx := v_idx + 1; v_type(v_idx) := '         inner join V$ASKSVODZAP SVDZP on SVDZP.IdDekl = KS.IdDekl ';
  v_idx := v_idx + 1; v_type(v_idx) := '    where trim(KS.KODKS) in (''1.27'', ''1.28'', ''1.32'', ''1.33'') ';
  v_idx := v_idx + 1; v_type(v_idx) := '    group by SVDZP.ZIP, KODKS ';
  v_idx := v_idx + 1; v_type(v_idx) := '  ) x ';
  v_idx := v_idx + 1; v_type(v_idx) := '  group by x.ZIP ';
  v_idx := v_idx + 1; v_type(v_idx) := ') y '||chr(10);

  DBMS_SQL.PARSE (c  => v_handle, statement => v_type, lb => 1, ub => v_type.count, lfflg => false, language_flag => dbms_sql.native );
  v_rowcount := dbms_sql.execute(v_handle);
  P$UTL_CLOSE_CURSOR_HANDLE(v_handle);

  P$EXECUTE_SHORT_QUERY('alter table NDS2_MRR_USER.CONTROL_RATIO_AGGREGATE add constraint PK$CONTROLRATIO_AGGREGATE primary key (ZIP) using index');

  P$UTL_LOG(
            p_module_name => 'PAC$NDS2_FULL_CYCLE.P$0_UPD_CONTROLRATIOAGGREGATE',
            p_msg => '(1) Успешное создание CONTROL_RATIO_AGGREGATE.',
            p_code => 1,
            p_msg_code => 0
           );


  -- Обработка ошибок
  exception when others then
  rollback;
  P$UTL_LOG(
            p_module_name => 'PAC$NDS2_FULL_CYCLE.P$0_UPD_CONTROLRATIOAGGREGATE',
            p_msg => substr(sqlerrm, 1, 512),
            p_code => 3,
            p_msg_code => sqlcode);
  P$UTL_CLOSE_CURSOR_HANDLE(v_handle);

end;

/***********************************************
Обновление витрины для списка расхождений для НД
************************************************/
procedure P$2_UPDATE_DECL_DISCREP_LIST
as
v_hndl        int;
v_type          dbms_sql.varchar2s;
v_type2          dbms_sql.varchar2s;
v_idx           number;
v_rowcount      number;
begin

v_hndl := dbms_sql.open_cursor;

P$EXECUTE_SHORT_QUERY('drop table MV$DISCREPANCY_DECL_LIST purge');

P$EXECUTE_SHORT_QUERY('drop table NDS2_MRR_USER.BK_SOV_DIS_KEY_ID purge');
P$EXECUTE_SHORT_QUERY('create table NDS2_MRR_USER.BK_SOV_DIS_KEY_ID pctfree 0 compress as select d.id, d.create_date from SOV_DISCREPANCY d');

P$UTL_LOG(
       p_module_name => 'PAC$NDS2_FULL_CYCLE.P$2_UPDATE_DECL_DISCREP_LIST',
       p_msg => '(1)Успешное создание промежуточной таблицы BK_SOV_DIS_KEY_ID',
       p_code => 1,
       p_msg_code => 0
       );

v_idx := 0;

v_idx := v_idx + 1; v_type(v_idx) := 'create TABLE MV$DISCREPANCY_DECL_LIST ';
v_idx := v_idx + 1; v_type(v_idx) := 'pctfree 0 ';
v_idx := v_idx + 1; v_type(v_idx) := 'compress ';
v_idx := v_idx + 1; v_type(v_idx) := 'as ';
v_idx := v_idx + 1; v_type(v_idx) := 'select * from V$DISCREPANCY_DECL_LIST_BUILD ' || chr(10);

DBMS_SQL.PARSE (c  => v_hndl, statement => v_type, lb => 1, ub => v_type.count, lfflg => false, language_flag => dbms_sql.native );
v_rowcount := dbms_sql.execute(v_hndl);
P$UTL_CLOSE_CURSOR_HANDLE(v_hndl);

P$UTL_LOG(
       p_module_name => 'PAC$NDS2_FULL_CYCLE.P$2_UPDATE_DECL_DISCREP_LIST',
       p_msg => '(1)Успешное создание MV$DISCREPANCY_DECL_LIST',
       p_code => 1,
       p_msg_code => 0
       );

P$EXECUTE_SHORT_QUERY('create index idx_0001 on MV$DISCREPANCY_DECL_LIST(declaration_id) nologging parallel 4  tablespace nds2_idx compress');
P$EXECUTE_SHORT_QUERY('create index idx_0002 on MV$DISCREPANCY_DECL_LIST(declaration_version_id) nologging parallel 4  tablespace nds2_idx compress');
/*соберем статистику*/
dbms_stats.gather_table_stats(ownname => 'NDS2_MRR_USER', tabname => 'MV$DISCREPANCY_DECL_LIST');

exception when others then
--TODO: log
P$UTL_LOG(p_module_name => 'PAC$NDS2_FULL_CYCLE.P$2_UPDATE_DECL_DISCREP_LIST', p_msg => substr(sqlerrm, 1, 512), p_code => 3, p_msg_code => sqlcode);
P$UTL_CLOSE_CURSOR_HANDLE(v_hndl);
end;

/*********************************************
Обновление таблицы оценок значимых изменений
DECLARATION_EXPLAIN_SUMMARY
**********************************************/
procedure P$9_UPDATE_DECL_EXPL_SUMM
as
v_handle        int;
v_type          dbms_sql.varchar2s;
v_idx           number;
v_rowcount      number;
begin

  -- Отписываемся в Лог о начале работы
  P$UTL_LOG(
            p_module_name => 'PAC$NDS2_FULL_CYCLE.P$9_UPDATE_DECL_EXPL_SUMM',
            p_msg => '(0) Начато обновление таблицы DECLARATION_EXPLAIN_SUMMARY',
            p_code => 1,
            p_msg_code => 0
           );

  -- На всякий случай грохнем старые данные - что-бы наш скрипт не упал!
  P$EXECUTE_SHORT_QUERY('drop table NDS2_MRR_USER.DECLARATION_EXP_SUMM_BAK');

  -- На всякий случай грохнем старые данные - что-бы наш скрипт не упал!
  P$EXECUTE_SHORT_QUERY('drop table NDS2_MRR_USER.DECLARATION_EXP_SUMM_TMP');

  -- Создаем вспомогательную таблицу DECLARATION_EXP_SUMM_TMP
  v_handle := dbms_sql.open_cursor;
  v_idx := 0;
  v_idx := v_idx + 1; v_type(v_idx) := 'create table NDS2_MRR_USER.DECLARATION_EXP_SUMM_TMP TableSPace NDS2_DATA ';
  v_idx := v_idx + 1; v_type(v_idx) := 'as ';
  v_idx := v_idx + 1; v_type(v_idx) := 'select * From  ';
  v_idx := v_idx + 1; v_type(v_idx) := ' NDS2_MRR_USER.V$DECL_EXP_SUM_BUILD '||chr(10);

  DBMS_SQL.PARSE (c  => v_handle, statement => v_type, lb => 1, ub => v_type.count, lfflg => false, language_flag => dbms_sql.native );
  v_rowcount := dbms_sql.execute(v_handle);
  P$UTL_CLOSE_CURSOR_HANDLE(v_handle);

  -- Отписываемся в Лог
  P$UTL_LOG(
            p_module_name => 'PAC$NDS2_FULL_CYCLE.P$9_UPDATE_DECL_EXPL_SUMM',
            p_msg => '(1) Успешное создание DECLARATION_EXP_SUMM_TMP.  Строк '||v_rowcount,
            p_code => 1,
            p_msg_code => 0
           );

  -- Удалям индексы с DECLARATION_EXPLAIN_SUMMARY
  begin
    PAC$INDEX_MANAGER.P$DROP_INDEXES('DECLARATION_EXPLAIN_SUMMARY');
    P$UTL_LOG(
            p_module_name => 'PAC$NDS2_FULL_CYCLE.P$9_UPDATE_DECL_EXPL_SUMM',
            p_msg => '(2) Удалены индексы DECLARATION_EXPLAIN_SUMMARY',
            p_code => 1,
            p_msg_code => 0
           );
    exception when others then
    P$UTL_LOG(
            p_module_name => 'PAC$NDS2_FULL_CYCLE.P$9_UPDATE_DECL_EXPL_SUMM',
            p_msg => '(2) Ошибка удаления индексов DECLARATION_EXPLAIN_SUMMARY: '||substr(sqlerrm, 1, 256),
            p_code => 1,
            p_msg_code => 0
           );
  end;
  
  --  Делаем бэкап: переименовываем DECLARATION_EXPLAIN_SUMMARY в DECLARATION_EXP_SUMM_BAK
  P$EXECUTE_SHORT_QUERY('alter table DECLARATION_EXPLAIN_SUMMARY rename to DECLARATION_EXP_SUMM_BAK');
  -- Отписываемся в Лог
  P$UTL_LOG(
            p_module_name => 'PAC$NDS2_FULL_CYCLE.P$9_UPDATE_DECL_EXPL_SUMM',
            p_msg => '(3) Успешный бэкап DECLARATION_EXPLAIN_SUMMARY',
            p_code => 1,
            p_msg_code => 0
           );

  -- Переименовываем DECLARATION_EXP_SUMM_TMP в DECLARATION_EXPLAIN_SUMMARY
  P$EXECUTE_SHORT_QUERY('alter table DECLARATION_EXP_SUMM_TMP rename to DECLARATION_EXPLAIN_SUMMARY');
  -- Отписываемся в Лог
  P$UTL_LOG(
           p_module_name => 'PAC$NDS2_FULL_CYCLE.P$9_UPDATE_DECL_EXPL_SUMM',
           p_msg => '(4) Успешная замена данных на актуальные',
           p_code => 1,
           p_msg_code => 0
         );

  -- Стряпаем индекс по DECLARATION_EXPLAIN_SUMMARY
  PAC$INDEX_MANAGER.P$VALIDATE_INDEXES('DECLARATION_EXPLAIN_SUMMARY');
  -- Отписываемся в Лог
  P$UTL_LOG(
           p_module_name => 'PAC$NDS2_FULL_CYCLE.P$9_UPDATE_DECL_EXPL_SUMM',
           p_msg => '(5) Успешное создание индексов по DECLARATION_EXPLAIN_SUMMARY',
           p_code => 1,
           p_msg_code => 0
         );

  -- Соберем статистику
  dbms_stats.gather_table_stats(ownname => 'NDS2_MRR_USER', tabname => 'DECLARATION_EXPLAIN_SUMMARY');
  -- Отписываемся в Лог
  P$UTL_LOG(
           p_module_name => 'PAC$NDS2_FULL_CYCLE.P$9_UPDATE_DECL_EXPL_SUMM',
           p_msg => '(6) Обновлена статистика',
           p_code => 1,
           p_msg_code => 0
         );

  execute immediate 'Alter session set ddl_lock_timeout = 300';
  -- Удаляем DECLARATION_EXP_SUMM_BAK
  P$EXECUTE_SHORT_QUERY('drop table NDS2_MRR_USER.DECLARATION_EXP_SUMM_BAK');
  -- Удаляем DECLARATION_EXP_SUMM_TMP
  P$EXECUTE_SHORT_QUERY('drop table NDS2_MRR_USER.DECLARATION_EXP_SUMM_TMP');
  execute immediate 'Alter session set ddl_lock_timeout = 0';
  -- Отписываемся в Лог об окончании работы
  P$UTL_LOG(
            p_module_name => 'PAC$NDS2_FULL_CYCLE.P$9_UPDATE_DECL_EXPL_SUMM',
            p_msg => '(7) Завершено обновление таблицы DECLARATION_EXPLAIN_SUMMARY',
            p_code => 1,
            p_msg_code => 0
           );

  -- Обработка ошибок
  exception when others then
  rollback;
  execute immediate 'Alter session set ddl_lock_timeout = 0';
  P$UTL_LOG(
            p_module_name => 'PAC$NDS2_FULL_CYCLE.P$9_UPDATE_DECL_EXPL_SUMM',
            p_msg => substr(sqlerrm, 1, 512),
            p_code => 3,
            p_msg_code => sqlcode);
  P$UTL_CLOSE_CURSOR_HANDLE(v_handle);

end;


/*********************************************
Обновление агрегата DECLARATION_REQUEST_COUNTER
**********************************************/
procedure P$11_UPDATE_DECL_REQ_CNT
as
v_handle        int;
v_type          dbms_sql.varchar2s;
v_idx           number;
v_rowcount      number;
begin

  -- На всякий случай грохнем старые данные - что-бы наш скрипт не упал!
  P$EXECUTE_SHORT_QUERY('drop table NDS2_MRR_USER.DECLARATION_REQ_CNT_BAK');
  -- Удалим предыдущую темповую таблицу если таковая осталась
  P$EXECUTE_SHORT_QUERY('drop table NDS2_MRR_USER.DECLARATION_REQ_CNT_TEMP');
  -- Отписываемся в Лог о начале работы
  P$UTL_LOG(
            p_module_name => 'PAC$NDS2_FULL_CYCLE.P$11_UPDATE_DECL_REQ_CNT',
            p_msg => '(0) Начато обновление агрегата DECLARATION_REQUEST_COUNTER',
            p_code => 1,
            p_msg_code => 0
           );

  -- Создаем вспомогательную таблицу DECLARATION_REQ_CNT_TEMP
  v_handle := dbms_sql.open_cursor;
  v_idx := 0;
  v_idx := v_idx + 1; v_type(v_idx) := 'create table NDS2_MRR_USER.DECLARATION_REQ_CNT_TEMP TableSpace NDS2_DATA ';
  v_idx := v_idx + 1; v_type(v_idx) := 'as ';
  v_idx := v_idx + 1; v_type(v_idx) := 'select * From ';
  v_idx := v_idx + 1; v_type(v_idx) := ' NDS2_MRR_USER.V$DECL_REQ_COUNT_BUILD '||chr(10);

  DBMS_SQL.PARSE (c  => v_handle, statement => v_type, lb => 1, ub => v_type.count, lfflg => false, language_flag => dbms_sql.native );
  v_rowcount := dbms_sql.execute(v_handle);
  P$UTL_CLOSE_CURSOR_HANDLE(v_handle);

  -- Отписываемся в Лог
  P$UTL_LOG(
            p_module_name => 'PAC$NDS2_FULL_CYCLE.P$11_UPDATE_DECL_REQ_CNT',
            p_msg => '(1) Успешное создание DECLARATION_REQ_CNT_TEMP.  Строк '||v_rowcount,
            p_code => 1,
            p_msg_code => 0
           );

  -- Удалям индексы с DECLARATION_REQUEST_COUNTER
  begin
    PAC$INDEX_MANAGER.P$DROP_INDEXES('DECLARATION_REQUEST_COUNTER');
    P$UTL_LOG(
            p_module_name => 'PAC$NDS2_FULL_CYCLE.P$11_UPDATE_DECL_REQ_CNT',
            p_msg => '(2) Удален индексы на DECLARATION_REQUEST_COUNTER',
            p_code => 1,
            p_msg_code => 0
           );
    exception when others then
    P$UTL_LOG(
            p_module_name => 'PAC$NDS2_FULL_CYCLE.P$11_UPDATE_DECL_REQ_CNT',
            p_msg => '(2) Ошибка удаления индексов DECLARATION_REQUEST_COUNTER: '||substr(sqlerrm, 1, 256),
            p_code => 1,
            p_msg_code => 0
           );
  end;
  
  --  Делаем бэкап: переименовываем DECLARATION_REQUEST_COUNTER в DECLARATION_REQ_CNT_BAK
  P$EXECUTE_SHORT_QUERY('alter table DECLARATION_REQUEST_COUNTER rename to DECLARATION_REQ_CNT_BAK');
  -- Отписываемся в Лог
  P$UTL_LOG(
            p_module_name => 'PAC$NDS2_FULL_CYCLE.P$11_UPDATE_DECL_REQ_CNT',
            p_msg => '(3) Успешный бэкап DECLARATION_REQUEST_COUNTER',
            p_code => 1,
            p_msg_code => 0
           );

  -- Переименовываем DECLARATION_REQ_CNT_TEMP в DECLARATION_REQUEST_COUNTER
  P$EXECUTE_SHORT_QUERY('alter table DECLARATION_REQ_CNT_TEMP rename to DECLARATION_REQUEST_COUNTER');
  -- Отписываемся в Лог
  P$UTL_LOG(
           p_module_name => 'PAC$NDS2_FULL_CYCLE.P$11_UPDATE_DECL_REQ_CNT',
           p_msg => '(4) Успешная замена данных на актуальные',
           p_code => 1,
           p_msg_code => 0
         );

  -- Стряпаем индексы по DECLARATION_REQUEST_COUNTER
  PAC$INDEX_MANAGER.P$VALIDATE_INDEXES('DECLARATION_REQUEST_COUNTER');

  -- Отписываемся в Лог
  P$UTL_LOG(
           p_module_name => 'PAC$NDS2_FULL_CYCLE.P$11_UPDATE_DECL_REQ_CNT',
           p_msg => '(5) Успешное создание индексов по DECLARATION_REQUEST_COUNTER',
           p_code => 1,
           p_msg_code => 0
         );

  -- Соберем статистику
  dbms_stats.gather_table_stats(ownname => 'NDS2_MRR_USER', tabname => 'DECLARATION_REQUEST_COUNTER');
  -- Отписываемся в Лог
  P$UTL_LOG(
           p_module_name => 'PAC$NDS2_FULL_CYCLE.P$11_UPDATE_DECL_REQ_CNT',
           p_msg => '(6) Обновлена статистика',
           p_code => 1,
           p_msg_code => 0
         );

  execute immediate 'Alter session set ddl_lock_timeout = 300';
  -- Удаляем DECLARATION_REQ_CNT_BAK
  P$EXECUTE_SHORT_QUERY('drop table NDS2_MRR_USER.DECLARATION_REQ_CNT_BAK');
  -- Удаляем DECLARATION_REQ_CNT_TEMP
  P$EXECUTE_SHORT_QUERY('drop table NDS2_MRR_USER.DECLARATION_REQ_CNT_TEMP');
  execute immediate 'Alter session set ddl_lock_timeout = 0';
  -- Отписываемся в Лог об окончании работы
  P$UTL_LOG(
            p_module_name => 'PAC$NDS2_FULL_CYCLE.P$11_UPDATE_DECL_REQ_CNT',
            p_msg => '(7) Завершено обновление агрегата DECLARATION_REQUEST_COUNTER',
            p_code => 1,
            p_msg_code => 0
           );

  -- Обработка ошибок
  exception when others then
  rollback;
  execute immediate 'Alter session set ddl_lock_timeout = 0';
  P$UTL_LOG(
            p_module_name => 'PAC$NDS2_FULL_CYCLE.P$11_UPDATE_DECL_REQ_CNT',
            p_msg => substr(sqlerrm, 1, 512),
            p_code => 3,
            p_msg_code => sqlcode);
  P$UTL_CLOSE_CURSOR_HANDLE(v_handle);

end;

/********************************************************
Расширение P$UPDATE_DICREPANCY,
реализующее дополнительные операции:
перестроение агрегатов для ООР.
********************************************************/
procedure P$UPDATE_DICREPANCY_EXT
as
begin
  -- Агрегаты для V$Inspector_Contragent_Params
  P$11_UPDATE_DECL_REQ_CNT;
  -- Обработка ошибок
  exception when others then
    raise_application_error('Ошибка выполнения расширения ПЦ на шаге', -20001);
end;

/********************************************************
Расширение P$UPDATE_DICREPANCY, реализующее
ежедневные дополнительные операции перестроения
агрегатов для ООР
********************************************************/
procedure P$UPDATE_DICREPANCY_DAILY
as
  v_temp number;
begin
  
  P$11_UPDATE_DECL_REQ_CNT;

  -- Обработка ошибок
  exception when others then
    raise_application_error('Ошибка выполнения ежедневных операций ПЦ', -20001);
end;


/**********************************************
обновление очереди отправки протоколов КС в ЭОД
**********************************************/
procedure P$QUEUE_NEW_CONTROL_RATIO
as
begin
  null;
end;

/**********************************************
расчет партиций под новые данные кеша СФ
**********************************************/
procedure P$INVOICE_CACHE_PART_CALC
as
v_partition_idx number := 1;
v_rows_sum number := 1;
v_partition_limit number := 2000000;
v_composite_prefix varchar2(32 char) := 'sicmp_';
v_big_prefix varchar2(32 char) := 'sisa_';
begin
   v_partition_idx := seq_sov_invoice_partition.nextval;

   for line in (
                select
                sv.ZIP,
                sum(nvl(sv.kolzapisey, 0)) as row_cnt,
                --decode(sv.TIPFAJLA, 0,8,1,9,2,81,3,91,4,10,5,11,6,12,0) as chapter,
                case
                when sum(nvl(sv.kolzapisey, 0)) < 1000000 then 3
                  else 4
                end as priority
                from v$asksvodzap sv
                inner join v$ask_declandjrnl mc_d on mc_d.zip = sv.ZIP
                left join SOV_INVOICE_PARTITION_DETALS existZips on existZips.Zip = mc_d.zip
                where kolzapisey > 100000 and existZips.Zip is null
                group by sv.zip
                order by priority, row_cnt
                ) loop
       if line.priority <> 4 then

         v_rows_sum := v_rows_sum + line.row_cnt;

         insert into SOV_INVOICE_PARTITION_DETALS (PARTITION_ID, ZIP)
         values (v_partition_idx, line.zip);

         if ( v_rows_sum >= v_partition_limit ) then
           insert into SOV_INVOICE_PARTITION_INFO(ID, NAME, ROWS_COUNT, PROCESSED)
           values (v_partition_idx, v_composite_prefix||v_partition_idx, v_rows_sum, 0);
           v_partition_idx := seq_sov_invoice_partition.nextval;
           v_rows_sum := 0;

         end if;

       else

         if v_rows_sum > 0 then
           insert into SOV_INVOICE_PARTITION_INFO(ID, NAME, ROWS_COUNT, PROCESSED)
           values (v_partition_idx, v_composite_prefix||v_partition_idx, v_rows_sum, 0);
           v_partition_idx := seq_sov_invoice_partition.nextval;
           v_rows_sum := 0;
           dbms_output.put_line('hit');
         end if;

         insert into SOV_INVOICE_PARTITION_DETALS (PARTITION_ID, ZIP)
         values (v_partition_idx, line.zip);

        insert into SOV_INVOICE_PARTITION_INFO(ID, NAME, ROWS_COUNT, PROCESSED)
        values (v_partition_idx, v_big_prefix||v_partition_idx, line.row_cnt, 0);

        v_partition_idx := seq_sov_invoice_partition.nextval;

       end if;

     end loop;
     commit;

end;

/**********************************************
создание новых партиций для кеша СФ
**********************************************/
procedure P$INVOICE_CACHE_BUILD_PART
as
v_isFirst boolean := true;
begin
  for pLine in (select * from SOV_INVOICE_PARTITION_INFO where processed = 0)
  loop
    for line in (select * from SOV_INVOICE_PARTITION_DETALS where partition_id = pLine.id)
      loop
        if v_isFirst then
           begin
             execute immediate 'alter table sov_invoice split partition si_def values('||line.zip||')
               into (partition '||pLine.name||' pctfree 10 initrans 10 compress, partition si_def pctfree 10 initrans 10 compress)';
             v_isFirst := false;
             exception when others then
             dbms_output.put_line('cannot add partition '||pLine.name||' '||sqlerrm);
           end;
        else
           begin
             execute immediate 'alter table sov_invoice modify partition '||pLine.name||'  add values ('||line.zip||')';
             exception when others then
             dbms_output.put_line('unable to modify partition '||pLine.name||' '||sqlerrm);
           end;
        end if;
      end loop;
      v_isFirst := true;
      update SOV_INVOICE_PARTITION_INFO set processed = 1 where id = pLine.id;
  end loop;
  commit;
end;

procedure P$ON_START
as
  v_next_val number;
begin
  v_next_val := SEQ_FULL_CYCLE.nextval;
  
  PAC$KNP_RESULT_DOCUMENTS.P$DISABLE_DECISION_UPDATE;
end;

procedure P$ON_COMPLETE
as
begin
  PAC$KNP_RESULT_DOCUMENTS.P$ENABLE_DECISION_UPDATE;
end;

procedure P$GET_COMPARISON_DATA_VERSION( pCount OUT number)
as
begin
	select last_number into pCount from user_sequences
	where sequence_name = 'SEQ_FULL_CYCLE';
end;

end;
/
