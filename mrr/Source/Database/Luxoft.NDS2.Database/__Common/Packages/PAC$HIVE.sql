﻿CREATE OR REPLACE PACKAGE "NDS2_MRR_USER"."PAC$HIVE"


AS
  PROCEDURE P$SAVE_CLAIM(
      p_doc_type                     IN NUMBER
    , p_id                           IN NUMBER
    , p_zip                          IN NUMBER
    , p_inn                          IN VARCHAR2
    , p_kpp_effective                IN VARCHAR2
    , p_fiscal_year                  IN NUMBER
    , p_quarter                      IN NUMBER
    , p_reg_number                   IN NUMBER
    , p_sono_code                    IN VARCHAR2
    , p_discrepancy_count            IN NUMBER
    , p_discrepancy_amount           IN NUMBER
    , p_status                       IN NUMBER
    , p_side                         IN NUMBER
    , p_GAP_DISCREPANCY_COUNT        IN NUMBER
    , p_NDS_DISCREPANCY_COUNT        IN NUMBER
    , p_GAP_DISCREPANCY_AMOUNT       IN NUMBER
    , p_NDS_DISCREPANCY_AMOUNT       IN NUMBER
    , p_BUYER_TOTAL_DISCREPANCY_AMT  IN NUMBER
    , p_BUYER_TOTAL_DISCREPANCY_QTY  IN NUMBER
    , p_BUYER_GAP_DISCREPANCY_AMT    IN NUMBER
    , p_BUYER_GAP_DISCREPANCY_QTY    IN NUMBER
    , p_BUYER_NDS_DISCREPANCY_AMT    IN NUMBER
    , p_BUYER_NDS_DISCREPANCY_QTY    IN NUMBER
    , p_SELLER_TOTAL_DISCREPANCY_AMT IN NUMBER
    , p_SELLER_TOTAL_DISCREPANCY_QTY IN NUMBER
    , p_SELLER_GAP_DISCREPANCY_AMT   IN NUMBER
    , p_SELLER_GAP_DISCREPANCY_QTY   IN NUMBER
    , p_SELLER_NDS_DISCREPANCY_AMT   IN NUMBER
    , p_SELLER_NDS_DISCREPANCY_QTY   IN NUMBER
    , p_xml                          IN CLOB);


  PROCEDURE p$start_invoice_claim;
  PROCEDURE p$finish_invoice_claim;
  PROCEDURE p$error_invoice_claim;

  PROCEDURE p$update_selection_status(
    p_status_id        IN NUMBER,
    p_id IN NUMBER
  );


END "PAC$HIVE";
/
CREATE OR REPLACE PACKAGE BODY "NDS2_MRR_USER"."PAC$HIVE"
AS
  PROCEDURE P$SAVE_CLAIM(
      p_doc_type                     NUMBER
    , p_id                           NUMBER
    , p_zip                          NUMBER
    , p_inn                          VARCHAR2
    , p_kpp_effective                VARCHAR2
    , p_fiscal_year                  NUMBER
    , p_quarter                      NUMBER
    , p_reg_number                   NUMBER
    , p_sono_code                    VARCHAR2
    , p_discrepancy_count            NUMBER
    , p_discrepancy_amount           NUMBER
    , p_status                       NUMBER
    , p_side                         NUMBER
    , p_GAP_DISCREPANCY_COUNT        NUMBER
    , p_NDS_DISCREPANCY_COUNT        NUMBER
    , p_GAP_DISCREPANCY_AMOUNT       NUMBER
    , p_NDS_DISCREPANCY_AMOUNT       NUMBER
    , p_BUYER_TOTAL_DISCREPANCY_AMT  NUMBER
    , p_BUYER_TOTAL_DISCREPANCY_QTY  NUMBER
    , p_BUYER_GAP_DISCREPANCY_AMT    NUMBER
    , p_BUYER_GAP_DISCREPANCY_QTY    NUMBER
    , p_BUYER_NDS_DISCREPANCY_AMT    NUMBER
    , p_BUYER_NDS_DISCREPANCY_QTY    NUMBER
    , p_SELLER_TOTAL_DISCREPANCY_AMT NUMBER
    , p_SELLER_TOTAL_DISCREPANCY_QTY NUMBER
    , p_SELLER_GAP_DISCREPANCY_AMT   NUMBER
    , p_SELLER_GAP_DISCREPANCY_QTY   NUMBER
    , p_SELLER_NDS_DISCREPANCY_AMT   NUMBER
    , p_SELLER_NDS_DISCREPANCY_QTY   NUMBER
    , p_xml                          CLOB)
  AS
    BEGIN
      INSERT INTO doc (
        --static fields
          doc_kind-- same as side
        , create_date
        , status_date
        --parametrised
        , doc_type
        , doc_id
        , zip
        , inn
        , kpp_effective
        , fiscal_year
        , quarter
        , REF_ENTITY_ID
        , sono_code
        , discrepancy_count
        , discrepancy_amount
        , status
        , side
        , GAP_DISCREPANCY_COUNT
        , NDS_DISCREPANCY_COUNT
        , GAP_DISCREPANCY_AMOUNT
        , NDS_DISCREPANCY_AMOUNT
        , BUYER_TOTAL_DISCREPANCY_AMT
        , BUYER_TOTAL_DISCREPANCY_QTY
        , BUYER_GAP_DISCREPANCY_AMT
        , BUYER_GAP_DISCREPANCY_QTY
        , BUYER_NDS_DISCREPANCY_AMT
        , BUYER_NDS_DISCREPANCY_QTY
        , SELLER_TOTAL_DISCREPANCY_AMT
        , SELLER_TOTAL_DISCREPANCY_QTY
        , SELLER_GAP_DISCREPANCY_AMT
        , SELLER_GAP_DISCREPANCY_QTY
        , SELLER_NDS_DISCREPANCY_AMT
        , SELLER_NDS_DISCREPANCY_QTY
        , tax_monitoring
        , document_body)
      VALUES (
          p_side --doc_kind
        , sysdate --create_date
        , sysdate --status_date
        , p_doc_type --doc_type
        , p_id
        , p_zip
        , p_inn
        , p_kpp_effective
        , p_fiscal_year
        , p_quarter
        , p_reg_number
        , p_sono_code
        , p_discrepancy_count
        , p_discrepancy_amount
        , p_status
        , p_side
        , p_GAP_DISCREPANCY_COUNT
        , p_NDS_DISCREPANCY_COUNT
        , p_GAP_DISCREPANCY_AMOUNT
        , p_NDS_DISCREPANCY_AMOUNT
        , p_BUYER_TOTAL_DISCREPANCY_AMT
        , p_BUYER_TOTAL_DISCREPANCY_QTY
        , p_BUYER_GAP_DISCREPANCY_AMT
        , p_BUYER_GAP_DISCREPANCY_QTY
        , p_BUYER_NDS_DISCREPANCY_AMT
        , p_BUYER_NDS_DISCREPANCY_QTY
        , p_SELLER_TOTAL_DISCREPANCY_AMT
        , p_SELLER_TOTAL_DISCREPANCY_QTY
        , p_SELLER_GAP_DISCREPANCY_AMT
        , p_SELLER_GAP_DISCREPANCY_QTY
        , p_SELLER_NDS_DISCREPANCY_AMT
        , p_SELLER_NDS_DISCREPANCY_QTY
        , 0
        , p_xml);

        insert into DOC_STATUS_HISTORY(DOC_ID,STATUS,STATUS_DATE) values (p_id, p_status,SYSDATE);
    END;

  PROCEDURE p$start_invoice_claim
  AS
    BEGIN
      INSERT INTO claim_journal (id, claim_type, start_date) VALUES (seq_claim_journal.nextval, 1, SYSDATE);

    END;

  PROCEDURE p$finish_invoice_claim
  AS
    BEGIN
      DECLARE
        var_num1 NUMBER;
      BEGIN
        SELECT max(id)
        INTO var_num1
        FROM claim_journal;
        UPDATE claim_journal
        SET END_DATE = SYSDATE
        WHERE id = var_num1 AND END_DATE IS NULL AND ERROR_DATE IS NULL;
      END;
    END;

  PROCEDURE p$error_invoice_claim
  AS
    BEGIN
      DECLARE
        var_num1 NUMBER;
      BEGIN
        SELECT max(id)
        INTO var_num1
        FROM claim_journal;
        UPDATE claim_journal
        SET ERROR_DATE = SYSDATE
        WHERE id = var_num1 AND END_DATE IS NULL AND ERROR_DATE IS NULL;
      END;
    END;


  PROCEDURE p$update_selection_status(p_status_id IN NUMBER,
                                      p_id        IN NUMBER
  )
  AS
    BEGIN

      UPDATE selection
      SET status = p_status_id, status_date = SYSDATE
      WHERE id = p_id;

      UPDATE selection_aggregate
      SET status_id = p_status_id,LAST_STATUS_CHANGED_AT=SYSDATE
      WHERE id = p_id;

      insert into Action_History (id, cd, status, change_date, user_sid, action)
      values(SEQ_ACTION_HISTORY.nextval, p_id, p_status_id, SYSDATE, null, 7);


    END;

END "PAC$HIVE";
/