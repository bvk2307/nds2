﻿create or replace package NDS2_MRR_USER.PAC$CACHE_MANAGER
as

function F$GET_ROW_COUNT( p_scope_id in number, p_cache_id in number) return number;

procedure P$SET_ROW_COUNT( p_scope_id in number, p_cache_id in number, p_count number, p_count_elpsd_ms number, p_args clob);

end;

/

create or replace package body NDS2_MRR_USER.PAC$CACHE_MANAGER
as


function F$GET_ROW_COUNT( p_scope_id in number, p_cache_id in number)
return number
as
v_result number := null;
begin
  
  select 
    rows_count into v_result
  from 
    QUERY_ROWCOUNT_CACHE 
  where 
    scope_id = p_scope_id 
    and query_hash_code = p_cache_id
	and rownum = 1; 
  
  insert into QUERY_ROWCOUNT_CACHE_STAT(SCOPE_ID, QUERY_HASH_CODE, DT)
  values (p_scope_id, p_cache_id, sysdate);
  
  return v_result;
  
  exception when no_data_found then 
    return null;
    
end;

procedure P$SET_ROW_COUNT( p_scope_id in number, p_cache_id in number, p_count number, p_count_elpsd_ms number, p_args clob)
as
begin
  
  merge into QUERY_ROWCOUNT_CACHE T1
  using (
   select 
        p_scope_id as scope_id, 
        p_cache_id as hashCode, 
        p_args as args, 
        p_count as rowsCount,
        p_count_elpsd_ms as elpsd_ms
  from dual) T2
  on (T1.scope_id = T2.scope_id and T1.query_hash_code = T2.hashCode)
  when not matched then
  insert values(T2.scope_id, T2.hashCode, T2.rowsCount, T2.elpsd_ms, T2.args);

  insert into QUERY_ROWCOUNT_CACHE_STAT(SCOPE_ID, QUERY_HASH_CODE, DT)
  values (p_scope_id, p_cache_id, sysdate);
    
end;

end;

/
