﻿CREATE OR REPLACE PACKAGE NDS2_MRR_USER.PAC$HIVE_REQUEST
AS
/*##############################################################################*/
/*#####  функции процессинга запросов данных в Hive                        #####*/
/*##############################################################################*/

/* функция создания запроса данных к Hive */ 
  FUNCTION F$CREATE_REQUEST ( 
    pJson               MD_DATA_REQUEST.REQUEST_KEY%type, 
    pCRC32Hash          MD_DATA_REQUEST.REQUEST_HASH_KEY%type, 
    pRequestType        MD_DATA_REQUEST.REQUEST_TYPE_ID%type 
  ) RETURN MD_DATA_REQUEST.ID%type; 

/* процедура получения идентификатора запроса к Hive */ 
  PROCEDURE P$GET_REQUEST_ID ( 
    pJson               MD_DATA_REQUEST.REQUEST_KEY%type, 
    pCRC32Hash          MD_DATA_REQUEST.REQUEST_HASH_KEY%type, 
    pRequestType        MD_DATA_REQUEST.REQUEST_TYPE_ID%type, 
    pStatus         OUT MD_DATA_REQUEST.STATUS%type, 
    pRequestId      OUT MD_DATA_REQUEST.ID%type 
  );
  
/* процедура получения текущего статуса запроса к Hive */
  PROCEDURE P$GET_REQUEST_STATUS ( 
    pRequestId          MD_DATA_REQUEST.ID%type, 
    pStatus         OUT MD_DATA_REQUEST.STATUS%type
  ); 

/*#############################################################################*/
/*#####  функции очистки кэшированных данных запросов к Hive              #####*/
/*#############################################################################*/
  
/* процедура очистки кэшированных данных запросов к Hive в рамках ПЦ */
  PROCEDURE P$CLEAN_HIVE_DATA_CACHE_WEEKLY;

END; 
/ 


CREATE OR REPLACE PACKAGE BODY NDS2_MRR_USER.PAC$HIVE_REQUEST
AS 

  LOG_INFO_TYPE number := 0;                    -- тип информационного сообщения в логе
  LOG_WARNING_TYPE number := 1;                 -- тип сообщения с предупреждением в логе
  LOG_ERROR_TYPE number := 2;                   -- тип сообщения об ошибке в логе
  
  REQUEST_STATUS_NEW number := 1;               -- статус запроса к Hive "Новый запрос"
  REQUEST_STATUS_MARKED number := 2;            -- статус запроса к Hive "Помечен к выгрузке"
  REQUEST_STATUS_LOADING number := 3;           -- статус запроса к Hive "Выгрузка начата"
  REQUEST_STATUS_READY number := 4;             -- статус запроса к Hive "Выгрузка завершена"
  REQUEST_STATUS_ERROR number := 5;             -- статус запроса к Hive "Ошибка выгрузки"
  
/*#############################################################################*/
/*#####  процедуры логирования процессинга запросов данных в Hive          ####*/
/*#############################################################################*/
/* запись в лог любого сообщения с типом (0 - информационное сообщение, 1 - предупреждение, 2 - ошибка) */   
  PROCEDURE LOG ( -- запись в лог 
    pCodeSite hive_request_log.code_site%type, 
    pMsgType  hive_request_log.msg_type%type, 
    pMsgText  hive_request_log.msg_text%type 
  ) IS 
    PRAGMA autonomous_transaction;
  BEGIN 
    insert into hive_request_log (id, log_time, code_site, msg_type, msg_text) 
      values (seq$hive_request_log.nextval, sysdate, pCodeSite, pMsgType, pMsgText); 
    commit;
    
  EXCEPTION 
    when others then NDS2$SYS.LOG_ERROR('PAC$HIVE_REQUEST.LOG', NULL, sqlcode, SUBSTR(sqlerrm, 1, 512));
  END;

/* запись в лог информационного сообщения */   
  PROCEDURE LOG_INFO ( -- запись информационного сообщения в лог
    pCodeSite hive_request_log.code_site%type, 
    pMsgText  hive_request_log.msg_text%type 
  ) IS 
  BEGIN 
    log(pCodeSite, LOG_INFO_TYPE, pMsgText); 
    exception when others 
      then NDS2$SYS.LOG_ERROR('PAC$HIVE_REQUEST.LOG_INFO', NULL, sqlcode, SUBSTR(sqlerrm, 1, 512));
  END; 

/* запись в лог сообщения с предупреждением */   
  PROCEDURE LOG_WARNING ( -- запись сообщения с предупреждением в лог
    pCodeSite hive_request_log.code_site%type, 
    pMsgText  hive_request_log.msg_text%type 
  ) IS 
  BEGIN 
    log(pCodeSite, LOG_WARNING_TYPE, pMsgText); 
    exception when others 
      then NDS2$SYS.LOG_ERROR('PAC$HIVE_REQUEST.LOG_WARNING', NULL, sqlcode, SUBSTR(sqlerrm, 1, 512));
  END;

/* запись в лог любого сообщения с ошибкой */   
  PROCEDURE LOG_ERROR ( -- запись сообщения об ошибке в лог
    pCodeSite hive_request_log.code_site%type, 
    pMsgText  hive_request_log.msg_text%type 
  ) IS 
  BEGIN 
    log(pCodeSite, LOG_ERROR_TYPE, pMsgText); 
    exception when others 
      then NDS2$SYS.LOG_ERROR('PAC$HIVE_REQUEST.LOG_ERROR', NULL, sqlcode, SUBSTR(sqlerrm, 1, 512));
  END;

/*#############################################################################*/
/*#####  функции процессинга запросов данных в Hive                       #####*/
/*#############################################################################*/
  
/* функция создания запроса данных к Hive */ 
  FUNCTION F$CREATE_REQUEST ( 
    pJson               MD_DATA_REQUEST.REQUEST_KEY%type, 
    pCRC32Hash          MD_DATA_REQUEST.REQUEST_HASH_KEY%type, 
    pRequestType        MD_DATA_REQUEST.REQUEST_TYPE_ID%type 
  ) RETURN MD_DATA_REQUEST.ID%type
  IS 
    vRequestId MD_DATA_REQUEST.ID%type;
  BEGIN 
    
    vRequestId := SEQ$MD_DATA_REQUEST.NEXTVAL;
    
    insert into MD_DATA_REQUEST (ID, REQUEST_TYPE_ID, REQUEST_KEY, REQUEST_HASH_KEY, STATUS, STATUS_DATE, PROCESSED_ROWS) 
      values (vRequestId, pRequestType, pJson, pCRC32Hash, REQUEST_STATUS_NEW, sysdate, null);
    LOG_INFO('PAC$HIVE_REQUEST.F$CREATE_REQUEST', 'Запрос ' || vRequestId || ' типа ' || pRequestType || ' для ключа ' || pCRC32Hash);
    
    return vRequestId; 
    
  EXCEPTION 
    when others then LOG_ERROR('PAC$HIVE_REQUEST.F$CREATE_REQUEST', 'Ошибка для запроса типа ' || pRequestType || ' для ключа ' || pCRC32Hash || ' - ' || SUBSTR(sqlerrm, 1, 768));
    raise; 
  END;
  
  
/* процедура получения идентификатора запроса к Hive */ 
  PROCEDURE P$GET_REQUEST_ID ( 
    pJson               MD_DATA_REQUEST.REQUEST_KEY%type, 
    pCRC32Hash          MD_DATA_REQUEST.REQUEST_HASH_KEY%type, 
    pRequestType        MD_DATA_REQUEST.REQUEST_TYPE_ID%type, 
    pStatus         OUT MD_DATA_REQUEST.STATUS%type, 
    pRequestId      OUT MD_DATA_REQUEST.ID%type 
  ) 
  IS
    vLastDate MD_DATA_REQUEST.status_date%type;
  BEGIN
    select max(mdr.id)
          ,max(mdr.status)
          ,max(mdr.status_date)
      into pRequestId
          ,pStatus
          ,vLastDate
      from
        (
          select max(id) as last_id
            from MD_DATA_REQUEST
            where REQUEST_HASH_KEY = pCRC32Hash
              and REQUEST_TYPE_ID = pRequestType
        ) mdl
      join MD_DATA_REQUEST mdr
        on mdl.last_id = mdr.id;
        
    if (pRequestId is null or (pStatus = REQUEST_STATUS_ERROR and vLastDate < sysdate - 1.0 / 24)) then
      pRequestId := F$CREATE_REQUEST (pJson, pCRC32Hash, pRequestType);
      pStatus := REQUEST_STATUS_NEW;
    end if;

  EXCEPTION
    when others then LOG_ERROR('PAC$HIVE_REQUEST.P$GET_REQUEST_ID', 'Ошибка для запроса типа ' || pRequestType || ' для ключа ' || pCRC32Hash || ' - ' || SUBSTR(sqlerrm, 1, 768));
    raise;
  END; 
  
/* процедура получения текущего статуса запроса к Hive */
  PROCEDURE P$GET_REQUEST_STATUS ( 
    pRequestId          MD_DATA_REQUEST.ID%type, 
    pStatus         OUT MD_DATA_REQUEST.STATUS%type
  ) 
  IS 
  BEGIN 
  
    select max(mdr.status) 
      into pStatus 
      from MD_DATA_REQUEST mdr 
      where mdr.id = pRequestId; 
  
  EXCEPTION 
    when others then LOG_ERROR('PAC$HIVE_REQUEST.P$GET_REQUEST_STATUS', 'Ошибка для запроса ' || pRequestId || ' - ' || SUBSTR(sqlerrm, 1, 768));
    raise;     
  END; 

/*#############################################################################*/
/*#####  функции очистки кэшированных данных запросов к Hive              #####*/
/*#############################################################################*/
  PROCEDURE P$CLEAN_TABLE ( 
    pTableName varchar2
  ) 
  IS 
    vTableName varchar2(32 CHAR);
  BEGIN 
    select max(table_name) 
      into vTableName 
      from user_tables 
      where table_name = pTableName;
    if vTableName is null then 
      LOG_WARNING('PAC$HIVE_REQUEST.P$CLEAN_TABLE', 'Таблица ' || pTableName || ' не найдена для очистки.');
    else 
      execute immediate 'truncate table ' || vTableName;
    end if; 
  EXCEPTION 
    when others then LOG_ERROR('PAC$HIVE_REQUEST.P$CLEAN_TABLE', 'Ошибка очистки таблицы ' || pTableName);
    raise;     
    
  END;
  
  PROCEDURE P$CLEAN_HIVE_DATA_CACHE_WEEKLY
  IS
  BEGIN
    for cur in (select id, TARGET_TABLE as str from R$REQUEST_TYPE where CLEAN_WEEKLY = 'Y')
    loop
      for t_cur in (select trim(regexp_substr(cur.str,'[^,]+', 1, level)) as table_name 
                      from dual 
                      connect by regexp_substr(cur.str, '[^,]+', 1, level) is not null)
      loop 
        P$CLEAN_TABLE(t_cur.table_name);
      end loop;
      execute immediate 'delete from MD_DATA_REQUEST where REQUEST_TYPE_ID = ' || cur.id || ' and STATUS in (' || REQUEST_STATUS_READY || ', ' || REQUEST_STATUS_ERROR || ')'; 
    end loop;
  EXCEPTION 
    when others then LOG_ERROR('PAC$HIVE_REQUEST.P$CLEAN_HIVE_DATA_CACHE_WEEKLY', 'Ошибка очистки кэшированных данных');
    raise;     
  END;
END; 
/ 
