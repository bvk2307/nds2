﻿create or replace package NDS2_MRR_USER.PAC$NAVIGATOR is

procedure P$GET_PAIRS (
  pRequestId in SOV_NAVIGATOR_REQUEST.REQUEST_ID%type,
  pResult out SYS_REFCURSOR
  );
  
procedure P$GET_STATUS (
  pRequestId in SOV_NAVIGATOR_REQUEST.REQUEST_ID%type,
  pStatus out SOV_NAVIGATOR_REQUEST.STATUS_ID%type
  );
  
procedure P$GET_DATA (
  pPairId in SOV_NAVIGATOR_PAIR.ID%type,
  pChainId in SOV_NAVIGATOR_CHAIN.ID%type,
  pRequestId in SOV_NAVIGATOR_REQUEST.REQUEST_ID%type,
  pResult out SYS_REFCURSOR);

procedure PROCESS_NAVIGATOR_REQUEST (
  pRequestId in number);
  
procedure CREATE_NAVIGATOR_REQUEST (
  pRequestId out number,
  pMonthFrom in number,
  pYearFrom in number,
  pMonthTo in number,
  pYearTo in number,
  pDepth in number,
  pMinMappedAmount in number,
  pMaxContractorsQuantity in number,
  pMinBuyerAmount in number,
  pMinSellerAmount in number);
  
procedure INSERT_NAVIGATOR_TAXPAYER (
  pRequestId in number,
  pInn in nvarchar2);

end PAC$NAVIGATOR;

/
create or replace package body NDS2_MRR_USER.PAC$NAVIGATOR is

procedure P$GET_PAIRS (
  pRequestId in SOV_NAVIGATOR_REQUEST.REQUEST_ID%type,
  pResult out SYS_REFCURSOR
  )
as
begin

  open pResult for
       select
               c.pair_id as pair_id,
               c.id as chain_id,
               c.head as head_taxpayer_inn,
               (select np_name from MV$TAX_PAYER_NAME v where v.inn = c.head) as head_taxpayer_name,
               c.tail as tail_taxpayer_inn,
               (select np_name from MV$TAX_PAYER_NAME v where v.inn = c.tail) as tail_taxpayer_name,
               c.chain_num as chain_number,
               sum(1) as chain_targets_quantity,
               sum(case when c.is_target = 1 then 0 else 1 end) as chain_links
       from    navigator_chains c
       where
               c.request_id = pRequestId
       group by c.pair_id, c.chain_num, c.id, c.head, c.tail
       order by c.chain_num;

end;

procedure P$GET_STATUS (
  pRequestId in SOV_NAVIGATOR_REQUEST.REQUEST_ID%type,
  pStatus out SOV_NAVIGATOR_REQUEST.STATUS_ID%type
  )
as
begin
  select status_id into pStatus from SOV_NAVIGATOR_REQUEST where request_id = pRequestId;
end;

procedure P$GET_DATA (
  pPairId in SOV_NAVIGATOR_PAIR.ID%type,
  pChainId in SOV_NAVIGATOR_CHAIN.ID%type,
  pRequestId in SOV_NAVIGATOR_REQUEST.REQUEST_ID%type,
  pResult out SYS_REFCURSOR)
as
begin

  open pResult for
  select t.*,
         min(mapped_nds) over (partition by id) as min_mapped_amount,
         min(not_mapped_nds) over (partition by id) as min_not_mapped_amount,
         min(mapped_nds) over (partition by id) as max_mapped_amount,
         min(not_mapped_nds) over (partition by id) as max_not_mapped_amount
  from (
  select  tp.id as id,
          tp.inn,
          tp.np_name as name,
          tp.kpp,
          sum(nds_calculated) as nds_calculated,
          sum(nds_deduction) as nds_deduction,
          sum(nds_sales) as nds_sales,
          sum(nds_purchase) as nds_purchase,
          nvl(max(c.sur_code), 4) as sur_code,
          c.is_target,
          c.id as chain_id,
          c.chain_num as chain_number,          
          case when lvl= 0 then 1 else 0 end as head,
          case when tp.inn = tail then 1 else 0 end as tail,
          c.lvl as order_number,
          sum(mapped_nds) as mapped_nds,
          sum(not_mapped_nds) as not_mapped_nds,
          sum(mapped_nds) as mapped_amount_as_buyer,
          sum(nds_deduction) as amount_per_buyer_as_buyer,
          sum(nds_calculated) as amount_per_seller_as_buyer,
          sum(mapped_nds_rev) as mapped_amount_as_seller,
          sum(nds_deduction_rev) as amount_per_buyer_as_seller,
          sum(nds_calculated_rev) as amount_per_seller_as_seller
  from    navigator_chains c
          left join
          (select rownum as id, n.*
          from MV$TAX_PAYER_NAME n) tp
          on tp.inn = c.inn_2
       where c.request_id = pRequestId
       and (pChainId is null or c.id = pChainId)
       and (pPairId is null or c.pair_id = pPairId)
  group by tp.id, tp.inn, tp.np_name, tp.kpp, c.is_target, c.chain_num, c.id, c.lvl, c.head, c.tail
  order by tp.np_name, c.chain_num) t;

end;

procedure PROCESS_NAVIGATOR_REQUEST (
  pRequestId in number)
as
     requestPrms SOV_NAVIGATOR_REQUEST%ROWTYPE;
     inn_pair NDS2_MRR_USER.T$NAV_HRZ_RELATION;
     all_pairs NDS2_MRR_USER.T$NAV_HRZ_RELATIONS := NDS2_MRR_USER.T$NAV_HRZ_RELATIONS();
     pairs NDS2_MRR_USER.T$NAV_HRZ_RELATIONS;
     type t_chains is table of NDS2_MRR_USER.NAVIGATOR_CHAINS%ROWTYPE;
     chains t_chains;
     pair_num number := 0;
     chain_num number := 0;
     current_inn varchar2(12 char):='';
     head varchar2(12 char):='';
     tail varchar2(12 char):='';
     pfrom number(1):=1;
     pto number(1):=4;
     n number:=0;
begin
  select *
  into requestPrms
  from SOV_NAVIGATOR_REQUEST
  where REQUEST_ID = pRequestId;

  pfrom := (requestPrms.month_from-1)/3+1;
  pto := (requestPrms.month_to-1)/3+1;

  for current_inn
  in (select inn from SOV_NAVIGATOR_REQUEST_TAXPAYER where REQUEST_ID = pRequestId)
  loop

    select T$NAV_HRZ_RELATION(t.inn_1, t.inn_2, level, rownum, SYS_CONNECT_BY_PATH(t.inn_1, '/')||'/'||t.inn_2, '', '', t.njsa, t.njba, nvl(t.gap_amount, 0), nvl(t.nba, 0) - nvl(t.gap_amount, 0), t.nba, t.nsa, nvl(rev.nba, 0) - nvl(rev.gap_amount, 0), rev.nba, rev.nsa, nvl(sur.sign_code, 4))
    bulk collect into pairs
    from HRZ_RELATIONS t
    left join HRZ_RELATIONS rev
         on  rev.year = t.year
         and rev.quarter = t.quarter
         and rev.inn_1 = t.inn_2
         and rev.inn_2 = t.inn_1
    left join (select x.sign_code, x.inn, x.kpp_effective, row_number() over (partition by x.inn, x.kpp_effective order by x.update_date desc) as idx, x.fiscal_year, x.fiscal_period
                from EXT_SUR x
                where x.is_actual = 1) sur
         on sur.inn = t.inn_1 and sur.kpp_effective = t.kpp_1_effective and sur.idx = 1
         and sur.fiscal_year = t.year and sur.fiscal_period = t.quarter
    where level <= requestPrms.DEPTH
    and ((t.year > requestPrms.year_from and t.year < requestPrms.year_to)
        or (t.year = requestPrms.year_from and to_number(t.quarter) > 20 + pfrom)
        or (t.year = requestPrms.year_to and to_number(t.quarter) < 20 + pto))
    start with t.inn_1 = current_inn.INN
    connect by nocycle prior t.inn_2 = t.inn_1;

    if(pairs.count > 0) then
      for i in pairs.first..pairs.last loop
          all_pairs.extend();
          all_pairs(all_pairs.count) := pairs(i);
      end loop;
    end if;

  END LOOP;

  select t2.id, pRequestId, inn_1, inn_2, lvl, 0, 0, path, t2.head, t2.tail,
        (select case when count(*) > 0 then 1 else 0 end from SOV_NAVIGATOR_REQUEST_TAXPAYER where REQUEST_ID = pRequestId and inn = inn_2),
        nds_sales, nds_purchase, not_mapped_nds, mapped_nds, nds_deduction, nds_calculated, mapped_nds_rev, nds_deduction_rev, nds_calculated_rev, sur_code
  bulk collect into chains
  from
  (select inn_1, inn_2, lvl, chain_id, path, head, tail, nds_sales, nds_purchase, not_mapped_nds, mapped_nds, nds_deduction, nds_calculated, mapped_nds_rev, nds_deduction_rev, nds_calculated_rev, sur_code
  from table(all_pairs)
  union
  select '', inn_1, lvl - 1, 0, '/'||inn_1, head, tail, 0, 0, 0, 0, 0, 0, 0, 0, 0,  max(sur_code) as sur_code
  from table(all_pairs)
  where lvl = 1
  group by '', inn_1, lvl - 1, 0, '/'||inn_1, head, tail) t1,
  (select rownum as id, p, head, tail
  from( select t.path as p, substr(t.path, instr(t.path, '/', 1, 1) + 1, instr(t.path, '/', 2, 1) - 2) as head, substr(t.path, instr(t.path, '/', -1, 1) + 1) as tail
        from  table(all_pairs) t
        where substr(t.path, instr(t.path, '/', -1, 1) + 1) in (select inn from SOV_NAVIGATOR_REQUEST_TAXPAYER where REQUEST_ID = pRequestId))
  where head != tail
  ) t2
  where t2.p like t1.path||'%'
  order by t2.head, t2.tail, t2.id, lvl;


  if chains.count > 0 then
    for i in chains.first..chains.last loop
      if (i > chains.first and (chains(i).head <> head or chains(i).tail <> tail)) then
        pair_num := pair_num + 1;
        chain_num := 0;
      end if;

      if (chains(i).lvl = 0) then
        chain_num := chain_num + 1;
      end if;

      head := chains(i).head;
      tail := chains(i).tail;

      insert into navigator_chains values(chains(i).id, pRequestId, chains(i).inn_1, chains(i).inn_2, chains(i).lvl, chain_num, pair_num, chains(i).path, chains(i).head, chains(i).tail, chains(i).is_target, chains(i).nds_sales, chains(i).nds_purchase, chains(i).not_mapped_nds, chains(i).mapped_nds, chains(i).nds_deduction, chains(i).nds_calculated, chains(i).mapped_nds_rev, chains(i).nds_deduction_rev, chains(i).nds_calculated_rev, chains(i).sur_code);
    end loop;
  end if;

  update SOV_NAVIGATOR_PAIR set REQUEST_ID = pRequestId;
  update SOV_NAVIGATOR_REQUEST set STATUS_ID = 2 where REQUEST_ID = pRequestId;
end;

procedure CREATE_NAVIGATOR_REQUEST (
  pRequestId out number,
  pMonthFrom in number,
  pYearFrom in number,
  pMonthTo in number,
  pYearTo in number,
  pDepth in number,
  pMinMappedAmount in number,
  pMaxContractorsQuantity in number,
  pMinBuyerAmount in number,
  pMinSellerAmount in number)
as
begin
  select SEQ_SOV_NAVIGATOR_REQUEST.NEXTVAL into pRequestId from DUAL;
  insert into SOV_NAVIGATOR_REQUEST(REQUEST_ID,MONTH_FROM,YEAR_FROM,MONTH_TO,YEAR_TO,DEPTH,MIN_MAPPED_AMOUNT,MAX_CONTRACTORS_QUANTITY,MIN_BUYER_AMOUNT,MIN_SELLER_AMOUNT,STATUS_ID,REQUEST_DATE)
         values (pRequestId, pMonthFrom, pYearFrom, pMonthTo, pYearTo, pDepth, pMinMappedAmount, pMaxContractorsQuantity,pMinBuyerAmount,pMinSellerAmount,0, Sysdate);
end;

procedure INSERT_NAVIGATOR_TAXPAYER (
  pRequestId in number,
  pInn in nvarchar2)
as
begin
  insert into SOV_NAVIGATOR_REQUEST_TAXPAYER(REQUEST_ID,INN) values (pRequestId, pInn);
end;

end PAC$NAVIGATOR;
/
