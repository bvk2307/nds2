﻿CREATE OR REPLACE PACKAGE NDS2_MRR_USER.PAC$EFFICIENCY_MONITOR
AS

  /*Перечень дат расчетов сгруппированные в кварталы*/
  PROCEDURE GET_CALC_DATES( pRefCursor OUT SYS_REFCURSOR );

  /*Данные показателей по кварталам за всю историю*/
  PROCEDURE GET_EFF_TIME_STAT ( pTnoCode varchar2, pRefCursor OUT SYS_REFCURSOR );

  /*Перечень УФНС в контексте РФ*/
  PROCEDURE GET_CA_UFNS_LIST ( pYear number, pQtr number, pCalcDate date, pRefCursor OUT SYS_REFCURSOR );

  /*Перечень УФНС в контексте ФО*/
  PROCEDURE GET_CA_DISTRICT_UFNS_LIST ( pDistrictCode varchar2, pYear number, pQtr number, pCalcDate date, pRefCursor OUT SYS_REFCURSOR );

  /*Перечень ИФНС в контексте МИ по КН*/
  PROCEDURE GET_CA_MIKN_UFNS_LIST ( pYear number, pQtr number, pCalcDate date,pRefCursor OUT SYS_REFCURSOR );

  /*Перечень ИФНС в контексте УФНС*/
  PROCEDURE GET_UFNS_IFNS_LIST ( pSelectedTno varchar2, pYear number, pQtr number, pCalcDate date, pRefCursor OUT SYS_REFCURSOR );

  /*Показатели конкретного ТНО */
  PROCEDURE GET_IFNS_DATA ( pTnoCode varchar2, pYear number, pQtr number, pCalcDate date, pRefCursor OUT SYS_REFCURSOR );

  PROCEDURE CHART_DATA_RF ( p_fiscal_year number, p_quarter number, p_date date, pRefCursor OUT SYS_REFCURSOR );
  PROCEDURE CHART_DATA_DISTRICT ( p_fiscal_year number, p_quarter number, p_date date, pDistrictCode varchar2, pRefCursor OUT SYS_REFCURSOR );
  PROCEDURE CHART_DATA_UFNS ( p_fiscal_year number, p_quarter number, p_date date, pTnoCode varchar2, pRefCursor OUT SYS_REFCURSOR );
  PROCEDURE CHART_DATA_IFNS ( p_fiscal_year number, p_quarter number, p_date date, pTnoCode varchar2, pRefCursor OUT SYS_REFCURSOR );
  PROCEDURE P$GET_IFNS_RATING_BY_INDEX_24(pYear number, pQtr number, pCalcDate date, pRefCursor OUT SYS_REFCURSOR);

  PROCEDURE P$UPDATE_AGGREGATE;
  PROCEDURE P$UPDATE_AGGREGATE_EXT
  (
    p_year number,
    p_qtr number
  );
END;
/
CREATE OR REPLACE PACKAGE BODY NDS2_MRR_USER.PAC$EFFICIENCY_MONITOR
AS

PROCEDURE LOG(p_msg varchar2, p_type number)
  AS
  PRAGMA AUTONOMOUS_TRANSACTION;
  BEGIN
    dbms_output.put_line(p_type||'-'||p_msg);
    insert into SYSTEM_LOG(ID, TYPE, SITE, ENTITY_ID, MESSAGE_CODE, MESSAGE, LOG_DATE)
    values(Seq_Sys_Log_Id.Nextval, p_type, 'PAC$EFFICIENCY_MONITOR', null, 1, p_msg, sysdate);
    commit;
  END;

  PROCEDURE EXECUTE_SQL(v_sql varchar2, v_supress_ex boolean := false)
  as
  begin
    execute immediate v_sql;
    exception when others then
    LOG('(suppressed)PAC$EFFICIENCY_MONITOR::EXECUTE_SQL - '||sqlerrm, -1);
    if not v_supress_ex then
      DBMS_STANDARD.raise_application_error(msg => sqlerrm, num => -20000);
    end if;
  end;

  FUNCTION GET_UNIX_TS
    RETURN NUMBER
  IS
  BEGIN
    return TO_NUMBER( ( sysdate - TO_DATE( '01.01.1970', 'DD.MM.YYYY' ) ) * 60 * 60 * 24 );
  END;

  PROCEDURE GET_CALC_DATES ( pRefCursor OUT SYS_REFCURSOR )
  as
    vx_prev_tp_year number;
    vx_prev_tp_quarter number;
  begin
  --Вычисляем последний закончившийся НП
    P$GET_PREV_PERIOD_QTR_YEAR(   vp_date=> current_date, 
                                vp_prev_tp_year => vx_prev_tp_year,
                                vp_prev_tp_quarter => vx_prev_tp_quarter);
    /* Если на момент выполнения запроса, расчет агрегата эффективности по предыдущему завершившемуся периоду не проводился,
        то результирующий набор запроса дополняется датами расчета фоновых показателей по предыдущему завершившемуся периоду.
        Дополнение датами расчетов из фоновых показателей происходит только после 1-го числа месяца следующего после окончания декларационной кампании
    */
    open pRefCursor
    for
    WITH
    /*Год и квартал предыдущего завершившегося периода*/
    prev_qtr AS (
        SELECT  vx_prev_tp_year as fiscal_year,
                to_number('2'||vx_prev_tp_quarter) as tax_period,
                vx_prev_tp_quarter as quarter
        FROM dual
    ),
    /*Все даты расчета агрегата отчета эффективности*/
    eff_agr_data as (
          SELECT DISTINCT
            fiscal_year         as fiscal_year,
            period              as period,
            trunc(calc_date)    as calc_date,
            1                   as Is_Eff_Agr_Calc
        FROM efficiency_monitor_data
        WHERE
            calc_date IS NOT NULL
    ),
    /*Даты расчета агрегата фоновых показатедей для предыдущего завершившегося периода.
       - Расчитывается в случае отсутствия расчета агрегата отчета эффективности для предыдущего завершившегося периода
       - Расчитывается только тогда когда новый НП уже наступил.    */
    bckg_report_data as (
         SELECT DISTINCT
            R.fiscal_year           as fiscal_year,
            P.quarter               as period,
            trunc(R.submit_date)    as calc_date,
            0                       as Is_Eff_Agr_Calc
        FROM report_declaration_raw R
        JOIN prev_qtr P
            ON R.fiscal_year = P.fiscal_year
            AND R.tax_period = P.tax_period
        WHERE
            NOT EXISTS (
                        SELECT 1
                        FROM eff_agr_data E
                        JOIN prev_qtr P
                            ON E.fiscal_year = P.fiscal_year
                            AND E.PERIOD = P.quarter
                        WHERE ROWNUM = 1)
            AND EXTRACT(MONTH FROM current_date) > P.quarter * 3
    ),
    /*Данные последнего завершившегося периода по умолчанию
       - Расчитывается в случае отсутствия нет данных по расчету завершившегося периода ни в агрегате эффективности ни в агрегате фоновых показателей
       - Расчитывается только тогда когда новый НП уже наступил.
    */
    default_data as (
        SELECT
            fiscal_year         as fiscal_year,
            quarter             as period,
            trunc(current_date) as calc_date,
            0                   as Is_Eff_Agr_Calc
        FROM prev_qtr
        WHERE
            NOT EXISTS (SELECT 1
                        FROM eff_agr_data E, prev_qtr P
                        WHERE	E.fiscal_year = P.fiscal_year
								AND E.PERIOD = P.quarter)
            AND NOT EXISTS( SELECT 1
                            FROM bckg_report_data
                            WHERE ROWNUM = 1)                            
            AND EXTRACT(MONTH FROM current_date) > quarter * 3
    )
    SELECT  fiscal_year,
            period,
            calc_date,
            Is_Eff_Agr_Calc
    FROM  eff_agr_data
    UNION ALL ( SELECT  fiscal_year,
                        period,
                        calc_date,
                        Is_Eff_Agr_Calc
                FROM  bckg_report_data )
    UNION ALL ( SELECT  fiscal_year,
                        period,
                        calc_date,
                        Is_Eff_Agr_Calc
                FROM  default_data )

    ORDER BY fiscal_year, period, calc_date, Is_Eff_Agr_Calc desc
    ;
  end;

  PROCEDURE GET_EFF_TIME_STAT ( pTnoCode varchar2, pRefCursor OUT SYS_REFCURSOR )
  as
  begin
    if pTnoCode = '0' then
      open pRefCursor for
      select
      '0' as TNO_CODE
      ,t.FISCAL_YEAR
      ,t.QTR
      ,t.eff_idx_2_1
      ,t.eff_idx_2_2
      ,t.eff_idx_2_3
      ,t.eff_idx_2_4_knp as eff_idx_2_4
      from v$efficiency_monitor_by_rf  t;
    end if;

    if length(pTnoCode) = 2 then
      open pRefCursor for
        with ranked_query as
        (
         select
           row_number() over (partition by t.UFNS_CODE, t.FISCAL_YEAR, t.QTR order by t.CALCULATION_DATE desc) as rn
          ,t.UFNS_CODE as TNO_CODE
          ,t.FISCAL_YEAR
          ,t.QTR
          ,t.eff_idx_2_1
          ,t.eff_idx_2_2
          ,t.eff_idx_2_3
          ,t.eff_idx_2_4_knp as eff_idx_2_4
        from
        v$efficiency_monitor_by_ufns t where t.UFNS_CODE = pTnoCode
        ),
        rf_query as
        (
          select
            '0' as TNO_CODE
            ,t.FISCAL_YEAR
            ,t.QTR
            ,t.eff_idx_2_1
            ,t.eff_idx_2_2
            ,t.eff_idx_2_3
            ,t.eff_idx_2_4_knp as eff_idx_2_4
          from v$efficiency_monitor_by_rf  t
        )
        select
           TNO_CODE
          ,FISCAL_YEAR
          ,QTR
          ,eff_idx_2_1
          ,eff_idx_2_2
          ,eff_idx_2_3
          ,eff_idx_2_4
        from ranked_query where rn = 1
        union all
        select
           TNO_CODE
          ,FISCAL_YEAR
          ,QTR
          ,eff_idx_2_1
          ,eff_idx_2_2
          ,eff_idx_2_3
          ,eff_idx_2_4
        from rf_query;
    end if;

    if length(pTnoCode) = 4 then
      null;
    end if;
  end;

  FUNCTION P$IS_LAST_UNCOMPARED_PERIOD( vp_Year number,  vp_Qtr number) RETURN BOOLEAN
    as
        vx_prev_tp_year number;
        vx_prev_tp_quarter number;
        vx_is_not_compared boolean :=false;
        vx_tmp number;
    begin
        P$GET_PREV_PERIOD_QTR_YEAR( vp_date=> current_date, 
                                    vp_prev_tp_year => vx_prev_tp_year,
                                    vp_prev_tp_quarter => vx_prev_tp_quarter);
        begin
            select 1
            into vx_tmp
            from dual
            where exists(   select 1
                            from efficiency_monitor_data
                            where   FISCAL_YEAR = vp_Year
                                    and period = vp_Qtr);

            exception when no_data_found then
                vx_is_not_compared:=true;
        end;

        return vx_prev_tp_year = vp_Year and vx_prev_tp_quarter = vp_Qtr and vx_is_not_compared;
    end;

  PROCEDURE GET_CA_UFNS_LIST (  pYear number, pQtr number, pCalcDate date, pRefCursor OUT SYS_REFCURSOR )
  as
  begin
    IF (P$IS_LAST_UNCOMPARED_PERIOD(pYear, pQtr )) THEN
        open pRefCursor for
        with
        /*Данные из отчета фоновых показателей */
            rep_data as (
                select
                    soun_code                      as SONO_CODE,
                    substr(soun_code,1,2)          as UFNS_CODE,
                    ndsDeduction_sum_compensation 
                        + ndsDeduction_sum_payment as DEDUCTION_AMNT,
                    decl_count_total               as DECL_ALL_CNT,
                    decl_count_payment             as DECL_PAY_CNT,
                    nds_sum_payment                as DECL_PAY_NDS_SUM,
                    decl_count_compensation        as DECL_COMPENSATE_CNT,
                    abs(nds_sum_compensation)      as DECL_COMPENSATE_NDS_SUM,
                    decl_count_null                as DECL_ZERO_CNT

                from report_declaration_raw
                where   fiscal_year = pYear
                        and tax_period = '2'||pQtr
                        and trunc(submit_date) = trunc( pCalcDate)
                        and region_code <> '99'
            )

        select
            '0' as TNO_CODE,
            'Российская Федерация' as TNO_NAME,
            /*Declaration data: */
            sum(DEDUCTION_AMNT)             as DEDUCTION_AMNT,
            sum(DECL_ALL_CNT)               as DECL_ALL_CNT,
            sum(DECL_PAY_CNT)               as DECL_PAY_CNT,
            sum(DECL_PAY_NDS_SUM)           as DECL_PAY_NDS_SUM,
            sum(DECL_COMPENSATE_CNT)        as DECL_COMPENSATE_CNT,
            sum(DECL_COMPENSATE_NDS_SUM)    as DECL_COMPENSATE_NDS_SUM,
            sum(DECL_ZERO_CNT)              as DECL_ZERO_CNT
        from rep_data
        union all
        select
            ds.ufns_code                    as TNO_CODE,
            ds.ufns_name                    as TNO_NAME,
            /*Declaration data: */
            sum(DEDUCTION_AMNT)             as DEDUCTION_AMNT,
            sum(DECL_ALL_CNT)               as DECL_ALL_CNT,
            sum(DECL_PAY_CNT)               as DECL_PAY_CNT,
            sum(DECL_PAY_NDS_SUM)           as DECL_PAY_NDS_SUM,
            sum(DECL_COMPENSATE_CNT)        as DECL_COMPENSATE_CNT,
            sum(DECL_COMPENSATE_NDS_SUM)    as DECL_COMPENSATE_NDS_SUM,
            sum(DECL_ZERO_CNT)              as DECL_ZERO_CNT
        from rep_data r
        inner join v$sono_ufns_district_relation ds
        on  r.sono_code = ds.sono_code

        group by ds.ufns_code, ds.ufns_name;
    ELSE
        open pRefCursor for
          select
          '0' as TNO_CODE,
          'Российская Федерация' as TNO_NAME,
          /*Declaration data: */
          DEDUCTION_AMNT,
          CALCULATION_AMNT,
          DECL_OPERATION_CNT,
          DECL_DISCREPANCY_CNT,
          DECL_ALL_CNT,
          DECL_ALL_NDS_SUM,
          DECL_PAY_CNT,
          DECL_PAY_NDS_SUM,
          DECL_COMPENSATE_CNT,
          abs(DECL_COMPENSATE_NDS_SUM) as DECL_COMPENSATE_NDS_SUM,
          DECL_ZERO_CNT,
          /*Discrepancy data: */
          /*All*/
          DIS_TOTAL_CNT,
          DIS_TOTAL_AMNT,
          DIS_GAP_TOTAL_CNT,
          DIS_GAP_TOTAL_AMNT,
          DIS_NDS_TOTAL_CNT,
          DIS_NDS_TOTAL_AMNT,
          /*Closed*/
          DIS_CLS_TOTAL_CNT,
          DIS_CLS_TOTAL_AMNT,
          DIS_CLS_GAP_TOTAL_CNT,
          DIS_CLS_GAP_TOTAL_AMNT,
          DIS_CLS_NDS_TOTAL_CNT,
          DIS_CLS_NDS_TOTAL_AMNT,
          /*Open*/
          DIS_OPN_TOTAL_CNT,
          DIS_OPN_TOTAL_AMNT,
          DIS_OPN_GAP_TOTAL_CNT,
          DIS_OPN_GAP_TOTAL_AMNT,
          DIS_OPN_NDS_TOTAL_CNT,
          DIS_OPN_NDS_TOTAL_AMNT,
          /*Eff indexes*/
          eff_idx_2_1  ,
          EFF_IDX_2_2  ,
          EFF_IDX_2_3  ,
          EFF_IDX_2_4_KNP as EFF_IDX_2_4,
          /*Rating*/
          0 as RNK_FEDERAL_EFF_IDX_2_1,
          0 as RNK_FEDERAL_EFF_IDX_2_2,
          0 as RNK_FEDERAL_EFF_IDX_2_3,
          0 as RNK_FEDERAL_EFF_IDX_2_4,

          0 as RNK_FEDERAL_EFF_IDX_2_1_PREV,
          0 as RNK_FEDERAL_EFF_IDX_2_2_PREV,
          0 as RNK_FEDERAL_EFF_IDX_2_3_PREV,
          0 as RNK_FEDERAL_EFF_IDX_2_4_PREV,

          0 as RNK_EFF_IDX_2_1,
          0 as RNK_EFF_IDX_2_2,
          0 as RNK_EFF_IDX_2_3,
          0 as RNK_EFF_IDX_2_4,

          0 as RNK_EFF_IDX_2_1_PREV,
          0 as RNK_EFF_IDX_2_2_PREV,
          0 as RNK_EFF_IDX_2_3_PREV,
          0 as RNK_EFF_IDX_2_4_PREV

          from v$efficiency_monitor_by_rf  where FISCAL_YEAR = pYear and QTR = pQtr and CALCULATION_DATE = pCalcDate
        union all
        select
        ufns.UFNS_CODE as TNO_CODE,
        ufns.UFNS_NAME as TNO_NAME,
        /*Declaration data: */
        ufns.DEDUCTION_AMNT,
        ufns.CALCULATION_AMNT,
        ufns.DECL_OPERATION_CNT,
        ufns.DECL_DISCREPANCY_CNT,
        ufns.DECL_ALL_CNT,
        ufns.DECL_ALL_NDS_SUM,
        ufns.DECL_PAY_CNT,
        ufns.DECL_PAY_NDS_SUM,
        ufns.DECL_COMPENSATE_CNT,
        abs(ufns.DECL_COMPENSATE_NDS_SUM) as DECL_COMPENSATE_NDS_SUM,
        ufns.DECL_ZERO_CNT,
        /*Discrepancy data: */
        /*All*/
        ufns.DIS_TOTAL_CNT,
        ufns.DIS_TOTAL_AMNT,
        ufns.DIS_GAP_TOTAL_CNT,
        ufns.DIS_GAP_TOTAL_AMNT,
        ufns.DIS_NDS_TOTAL_CNT,
        ufns.DIS_NDS_TOTAL_AMNT,
        /*Closed*/
        ufns.DIS_CLS_TOTAL_CNT,
        ufns.DIS_CLS_TOTAL_AMNT,
        ufns.DIS_CLS_GAP_TOTAL_CNT,
        ufns.DIS_CLS_GAP_TOTAL_AMNT,
        ufns.DIS_CLS_NDS_TOTAL_CNT,
        ufns.DIS_CLS_NDS_TOTAL_AMNT,
        /*Open*/
        ufns.DIS_OPN_TOTAL_CNT,
        ufns.DIS_OPN_TOTAL_AMNT,
        ufns.DIS_OPN_GAP_TOTAL_CNT,
        ufns.DIS_OPN_GAP_TOTAL_AMNT,
        ufns.DIS_OPN_NDS_TOTAL_CNT,
        ufns.DIS_OPN_NDS_TOTAL_AMNT,
        /*Eff indexes*/
        ufns.eff_idx_2_1 as EFF_IDX_2_1,
        ufns.eff_idx_2_2 as EFF_IDX_2_2,
        ufns.eff_idx_2_3 as EFF_IDX_2_3,
        ufns.eff_idx_2_4_knp as EFF_IDX_2_4,
        /*Rating*/
        ufns.rnk_federal_eff_idx_2_1 as RNK_FEDERAL_EFF_IDX_2_1,
        ufns.rnk_federal_eff_idx_2_2 as RNK_FEDERAL_EFF_IDX_2_2,
        ufns.rnk_federal_eff_idx_2_3 as RNK_FEDERAL_EFF_IDX_2_3,
        ufns.rnk_federal_eff_idx_2_4_knp as RNK_FEDERAL_EFF_IDX_2_4,

        prev.RNK_FEDERAL_EFF_IDX_2_1_PREV,
        prev.RNK_FEDERAL_EFF_IDX_2_2_PREV,
        prev.RNK_FEDERAL_EFF_IDX_2_3_PREV,
        prev.RNK_FEDERAL_EFF_IDX_2_4_PREV,

        ufns.rnk_eff_idx_2_1 as RNK_EFF_IDX_2_1,
        ufns.rnk_eff_idx_2_2 as RNK_EFF_IDX_2_2,
        ufns.rnk_eff_idx_2_3 as RNK_EFF_IDX_2_3,
        ufns.rnk_eff_idx_2_4_knp as RNK_EFF_IDX_2_4,

        prev.RNK_EFF_IDX_2_1_PREV,
        prev.RNK_EFF_IDX_2_2_PREV,
        prev.RNK_EFF_IDX_2_3_PREV,
        prev.RNK_EFF_IDX_2_4_PREV

        from v$efficiency_monitor_by_ufns ufns
        left join
         (
           select
            UFNS_CODE,
            rnk_federal_eff_idx_2_1 as RNK_FEDERAL_EFF_IDX_2_1_PREV,
            rnk_federal_eff_idx_2_2 as RNK_FEDERAL_EFF_IDX_2_2_PREV,
            rnk_federal_eff_idx_2_3 as RNK_FEDERAL_EFF_IDX_2_3_PREV,
            rnk_federal_eff_idx_2_4_knp as RNK_FEDERAL_EFF_IDX_2_4_PREV,

            rnk_eff_idx_2_1 as RNK_EFF_IDX_2_1_PREV,
            rnk_eff_idx_2_2 as RNK_EFF_IDX_2_2_PREV,
            rnk_eff_idx_2_3 as RNK_EFF_IDX_2_3_PREV,
            rnk_eff_idx_2_4_knp as RNK_EFF_IDX_2_4_PREV
           from v$efficiency_monitor_by_ufns
           where
                FISCAL_YEAR = pYear
                and QTR = pQtr
                and CALCULATION_DATE =  (select
                                             max(calc_date)
                                           from efficiency_monitor_data
                                           where fiscal_year = pYear
                                           and period = pQtr and calc_date < pCalcDate)
                and substr(UFNS_CODE, 1,2) <> '99'
         ) prev on prev.ufns_code = ufns.ufns_code
        where
         ufns.FISCAL_YEAR = pYear
         and ufns.QTR = pQtr
         and ufns.CALCULATION_DATE = pCalcDate
         and substr(ufns.UFNS_CODE, 1,2) <> '99'
        order by EFF_IDX_2_3 desc;
    END IF;
  end;

  PROCEDURE GET_CA_DISTRICT_UFNS_LIST ( pDistrictCode varchar2, pYear number, pQtr number, pCalcDate date, pRefCursor OUT SYS_REFCURSOR )
  as
  begin

    IF (P$IS_LAST_UNCOMPARED_PERIOD(pYear, pQtr )) THEN
        open pRefCursor for
            with
            /*Данные из отчета фоновых показателей */
            rep_data as (
                select
                    to_char(ds.district_code)       as DISTRICT_CODE,
                    ds.district_name                as DISTRICT_NAME,
                    ds.ufns_code                    as UFNS_CODE,
                    ds.ufns_name                    as UFNS_NAME,

                    ndsDeduction_sum_compensation  
                        + ndsDeduction_sum_payment as DEDUCTION_AMNT,
                    decl_count_total               as DECL_ALL_CNT,
                    decl_count_payment             as DECL_PAY_CNT,
                    nds_sum_payment                as DECL_PAY_NDS_SUM,
                    decl_count_compensation        as DECL_COMPENSATE_CNT,
                    abs(nds_sum_compensation)      as DECL_COMPENSATE_NDS_SUM,
                    decl_count_null                as DECL_ZERO_CNT

                from report_declaration_raw r
                join v$sono_ufns_district_relation ds
                    on  r.soun_code = ds.sono_code
                where   r.fiscal_year = pYear
                        and r.tax_period = '2'||pQtr
                        and trunc(r.submit_date) = trunc( pCalcDate)
                        and r.region_code <> '99'
                        and r.region_code in ( select region_code from FEDERAL_DISTRICT_REGION where district_id = pDistrictCode)
            )
            select
                district_code                   as TNO_CODE,
                district_name                   as TNO_NAME ,
                /*Итоги по выбранному региону */
                sum(DEDUCTION_AMNT)             as DEDUCTION_AMNT,
                sum(DECL_ALL_CNT)               as DECL_ALL_CNT,
                sum(DECL_PAY_CNT)               as DECL_PAY_CNT,
                sum(DECL_PAY_NDS_SUM)           as DECL_PAY_NDS_SUM,
                sum(DECL_COMPENSATE_CNT)        as DECL_COMPENSATE_CNT,
                sum(DECL_COMPENSATE_NDS_SUM)    as DECL_COMPENSATE_NDS_SUM,
                sum(DECL_ZERO_CNT)              as DECL_ZERO_CNT
            from rep_data
            group by district_code, district_name

            union all
            select
                ufns_code                    as TNO_CODE,
                ufns_name                    as TNO_NAME,
                /*Показатели УФНС в выбранном ФО: */
                sum(DEDUCTION_AMNT)             as DEDUCTION_AMNT,
                sum(DECL_ALL_CNT)               as DECL_ALL_CNT,
                sum(DECL_PAY_CNT)               as DECL_PAY_CNT,
                sum(DECL_PAY_NDS_SUM)           as DECL_PAY_NDS_SUM,
                sum(DECL_COMPENSATE_CNT)        as DECL_COMPENSATE_CNT,
                sum(DECL_COMPENSATE_NDS_SUM)    as DECL_COMPENSATE_NDS_SUM,
                sum(DECL_ZERO_CNT)              as DECL_ZERO_CNT
            from rep_data r
            group by ufns_code, ufns_name;

    ELSE
        open pRefCursor for
          select
          pDistrictCode as TNO_CODE,
          d.description as TNO_NAME,
          /*Declaration data: */
          DEDUCTION_AMNT,
          CALCULATION_AMNT,
          DECL_OPERATION_CNT,
          DECL_DISCREPANCY_CNT,
          DECL_ALL_CNT,
          DECL_ALL_NDS_SUM,
          DECL_PAY_CNT,
          DECL_PAY_NDS_SUM,
          DECL_COMPENSATE_CNT,
          abs(DECL_COMPENSATE_NDS_SUM) as DECL_COMPENSATE_NDS_SUM,
          DECL_ZERO_CNT,
          /*Discrepancy data: */
          /*All*/
          DIS_TOTAL_CNT,
          DIS_TOTAL_AMNT,
          DIS_GAP_TOTAL_CNT,
          DIS_GAP_TOTAL_AMNT,
          DIS_NDS_TOTAL_CNT,
          DIS_NDS_TOTAL_AMNT,
          /*Closed*/
          DIS_CLS_TOTAL_CNT,
          DIS_CLS_TOTAL_AMNT,
          DIS_CLS_GAP_TOTAL_CNT,
          DIS_CLS_GAP_TOTAL_AMNT,
          DIS_CLS_NDS_TOTAL_CNT,
          DIS_CLS_NDS_TOTAL_AMNT,
          /*Open*/
          DIS_OPN_TOTAL_CNT,
          DIS_OPN_TOTAL_AMNT,
          DIS_OPN_GAP_TOTAL_CNT,
          DIS_OPN_GAP_TOTAL_AMNT,
          DIS_OPN_NDS_TOTAL_CNT,
          DIS_OPN_NDS_TOTAL_AMNT,
          /*Eff indexes*/
          EFF_IDX_2_1  ,
          EFF_IDX_2_2  ,
          EFF_IDX_2_3  ,
          eff_idx_2_4_knp as EFF_IDX_2_4  ,
          /*Rating*/
          0 as RNK_FEDERAL_EFF_IDX_2_1,
          0 as RNK_FEDERAL_EFF_IDX_2_2,
          0 as RNK_FEDERAL_EFF_IDX_2_3,
          0 as RNK_FEDERAL_EFF_IDX_2_4,

          0 as RNK_FEDERAL_EFF_IDX_2_1_PREV,
          0 as RNK_FEDERAL_EFF_IDX_2_2_PREV,
          0 as RNK_FEDERAL_EFF_IDX_2_3_PREV,
          0 as RNK_FEDERAL_EFF_IDX_2_4_PREV,

          0 as RNK_EFF_IDX_2_1,
          0 as RNK_EFF_IDX_2_2,
          0 as RNK_EFF_IDX_2_3,
          0 as RNK_EFF_IDX_2_4,

          0 as RNK_EFF_IDX_2_1_PREV,
          0 as RNK_EFF_IDX_2_2_PREV,
          0 as RNK_EFF_IDX_2_3_PREV,
          0 as RNK_EFF_IDX_2_4_PREV

          from v$efficiency_monitor_by_fo  t
          inner join federal_district d on d.district_id= t.district_code
          where FISCAL_YEAR = pYear
          and QTR = pQtr
          and CALCULATION_DATE = pCalcDate
          and district_code = pDistrictCode
        union all
        select
        ufns.UFNS_CODE as TNO_CODE,
        ufns.UFNS_NAME as TNO_NAME,
        /*Declaration data: */
        ufns.DEDUCTION_AMNT,
        ufns.CALCULATION_AMNT,
        ufns.DECL_OPERATION_CNT,
        ufns.DECL_DISCREPANCY_CNT,
        ufns.DECL_ALL_CNT,
        ufns.DECL_ALL_NDS_SUM,
        ufns.DECL_PAY_CNT,
        ufns.DECL_PAY_NDS_SUM,
        ufns.DECL_COMPENSATE_CNT,
        abs(ufns.DECL_COMPENSATE_NDS_SUM) as DECL_COMPENSATE_NDS_SUM,
        ufns.DECL_ZERO_CNT,
        /*Discrepancy data: */
        /*All*/
        ufns.DIS_TOTAL_CNT,
        ufns.DIS_TOTAL_AMNT,
        ufns.DIS_GAP_TOTAL_CNT,
        ufns.DIS_GAP_TOTAL_AMNT,
        ufns.DIS_NDS_TOTAL_CNT,
        ufns.DIS_NDS_TOTAL_AMNT,
        /*Closed*/
        ufns.DIS_CLS_TOTAL_CNT,
        ufns.DIS_CLS_TOTAL_AMNT,
        ufns.DIS_CLS_GAP_TOTAL_CNT,
        ufns.DIS_CLS_GAP_TOTAL_AMNT,
        ufns.DIS_CLS_NDS_TOTAL_CNT,
        ufns.DIS_CLS_NDS_TOTAL_AMNT,
        /*Open*/
        ufns.DIS_OPN_TOTAL_CNT,
        ufns.DIS_OPN_TOTAL_AMNT,
        ufns.DIS_OPN_GAP_TOTAL_CNT,
        ufns.DIS_OPN_GAP_TOTAL_AMNT,
        ufns.DIS_OPN_NDS_TOTAL_CNT,
        ufns.DIS_OPN_NDS_TOTAL_AMNT,
        /*Eff indexes*/
        ufns.eff_idx_2_1 as EFF_IDX_2_1,
        ufns.eff_idx_2_2 as EFF_IDX_2_2,
        ufns.eff_idx_2_3 as EFF_IDX_2_3,
        ufns.eff_idx_2_4_knp as EFF_IDX_2_4,
        /*Rating*/
        ufns.rnk_federal_eff_idx_2_1 as RNK_FEDERAL_EFF_IDX_2_1,
        ufns.rnk_federal_eff_idx_2_2 as RNK_FEDERAL_EFF_IDX_2_2,
        ufns.rnk_federal_eff_idx_2_3 as RNK_FEDERAL_EFF_IDX_2_3,
        ufns.rnk_federal_eff_idx_2_4_knp as RNK_FEDERAL_EFF_IDX_2_4,

        prev.RNK_FEDERAL_EFF_IDX_2_1_PREV,
        prev.RNK_FEDERAL_EFF_IDX_2_2_PREV,
        prev.RNK_FEDERAL_EFF_IDX_2_3_PREV,
        prev.RNK_FEDERAL_EFF_IDX_2_4_PREV,

        ufns.rnk_eff_idx_2_1 as RNK_EFF_IDX_2_1,
        ufns.rnk_eff_idx_2_2 as RNK_EFF_IDX_2_2,
        ufns.rnk_eff_idx_2_3 as RNK_EFF_IDX_2_3,
        ufns.rnk_eff_idx_2_4_knp as RNK_EFF_IDX_2_4,

        prev.RNK_EFF_IDX_2_1_PREV,
        prev.RNK_EFF_IDX_2_2_PREV,
        prev.RNK_EFF_IDX_2_3_PREV,
        prev.RNK_EFF_IDX_2_4_PREV

        from v$efficiency_monitor_by_ufns ufns
        left join
         (
           select
            UFNS_CODE,
            rnk_federal_eff_idx_2_1 as RNK_FEDERAL_EFF_IDX_2_1_PREV,
            rnk_federal_eff_idx_2_2 as RNK_FEDERAL_EFF_IDX_2_2_PREV,
            rnk_federal_eff_idx_2_3 as RNK_FEDERAL_EFF_IDX_2_3_PREV,
            rnk_federal_eff_idx_2_4_knp as RNK_FEDERAL_EFF_IDX_2_4_PREV,

            rnk_eff_idx_2_1 as RNK_EFF_IDX_2_1_PREV,
            rnk_eff_idx_2_2 as RNK_EFF_IDX_2_2_PREV,
            rnk_eff_idx_2_3 as RNK_EFF_IDX_2_3_PREV,
            rnk_eff_idx_2_4_knp as RNK_EFF_IDX_2_4_PREV
           from v$efficiency_monitor_by_ufns
           where
                FISCAL_YEAR = pYear
                and QTR = pQtr
                and CALCULATION_DATE =  (select
                                             max(calc_date)
                                           from efficiency_monitor_data
                                           where fiscal_year = pYear
                                           and period = pQtr and calc_date < pCalcDate)
                and UFNS_CODE <> '9900'
         ) prev on prev.ufns_code = ufns.ufns_code
        where
            ufns.DISTRICT_CODE = pDistrictCode
        and ufns.FISCAL_YEAR = pYear
        and ufns.QTR = pQtr
        and ufns.CALCULATION_DATE = pCalcDate
        and ufns.UFNS_CODE <> '9900';
    END IF;
  end;

  PROCEDURE GET_CA_MIKN_UFNS_LIST ( pYear number, pQtr number, pCalcDate date,pRefCursor OUT SYS_REFCURSOR )
  as
  begin
     IF (P$IS_LAST_UNCOMPARED_PERIOD(pYear, pQtr )) THEN
        open pRefCursor for
            with
            /*Данные из отчета фоновых показателей */
            rep_data as (
                select
                    ds.sono_code                    as SONO_CODE,
                    ds.sono_name                    as SONO_NAME,

                    ndsDeduction_sum_compensation
                        + ndsDeduction_sum_payment as DEDUCTION_AMNT,
                    decl_count_total               as DECL_ALL_CNT,
                    decl_count_payment             as DECL_PAY_CNT,
                    nds_sum_payment                as DECL_PAY_NDS_SUM,
                    decl_count_compensation        as DECL_COMPENSATE_CNT,
                    abs(nds_sum_compensation)      as DECL_COMPENSATE_NDS_SUM,
                    decl_count_null                as DECL_ZERO_CNT

                from report_declaration_raw r
                join v$sono_ufns_district_relation ds
                    on  r.soun_code = ds.sono_code
                where   r.fiscal_year = pYear
                        and r.tax_period = '2'||pQtr
                        and trunc(r.submit_date) = trunc( pCalcDate)
            )
            select
                '9900' as TNO_CODE,
                'Российская Федерация' as TNO_NAME,
                /*Итоги по РФ */
                sum(DEDUCTION_AMNT)             as DEDUCTION_AMNT,
                sum(DECL_ALL_CNT)               as DECL_ALL_CNT,
                sum(DECL_PAY_CNT)               as DECL_PAY_CNT,
                sum(DECL_PAY_NDS_SUM)           as DECL_PAY_NDS_SUM,
                sum(DECL_COMPENSATE_CNT)        as DECL_COMPENSATE_CNT,
                sum(DECL_COMPENSATE_NDS_SUM)    as DECL_COMPENSATE_NDS_SUM,
                sum(DECL_ZERO_CNT)              as DECL_ZERO_CNT
            from rep_data

            union all
            select
                sono_code                       as TNO_CODE,
                sono_name                       as TNO_NAME,
                /*Итоги по МИ по КН */
                sum(DEDUCTION_AMNT)             as DEDUCTION_AMNT,
                sum(DECL_ALL_CNT)               as DECL_ALL_CNT,
                sum(DECL_PAY_CNT)               as DECL_PAY_CNT,
                sum(DECL_PAY_NDS_SUM)           as DECL_PAY_NDS_SUM,
                sum(DECL_COMPENSATE_CNT)        as DECL_COMPENSATE_CNT,
                sum(DECL_COMPENSATE_NDS_SUM)    as DECL_COMPENSATE_NDS_SUM,
                sum(DECL_ZERO_CNT)              as DECL_ZERO_CNT
            from rep_data
            where
                sono_code in ('9971', '9972', '9973', '9974', '9975', '9976', '9977', '9978', '9979')
            group by  sono_code,  sono_name;
    ELSE
          open pRefCursor for
                select
          '9900' as TNO_CODE,
          'Российская Федерация' as TNO_NAME,
          /*Declaration data: */
          DEDUCTION_AMNT,
          CALCULATION_AMNT,
          DECL_OPERATION_CNT,
          DECL_DISCREPANCY_CNT,
          DECL_ALL_CNT,
          DECL_ALL_NDS_SUM,
          DECL_PAY_CNT,
          DECL_PAY_NDS_SUM,
          DECL_COMPENSATE_CNT,
          abs(DECL_COMPENSATE_NDS_SUM) as DECL_COMPENSATE_NDS_SUM,
          DECL_ZERO_CNT,
          /*Discrepancy data: */
          /*All*/
          DIS_TOTAL_CNT,
          DIS_TOTAL_AMNT,
          DIS_GAP_TOTAL_CNT,
          DIS_GAP_TOTAL_AMNT,
          DIS_NDS_TOTAL_CNT,
          DIS_NDS_TOTAL_AMNT,
          /*Closed*/
          DIS_CLS_TOTAL_CNT,
          DIS_CLS_TOTAL_AMNT,
          DIS_CLS_GAP_TOTAL_CNT,
          DIS_CLS_GAP_TOTAL_AMNT,
          DIS_CLS_NDS_TOTAL_CNT,
          DIS_CLS_NDS_TOTAL_AMNT,
          /*Open*/
          DIS_OPN_TOTAL_CNT,
          DIS_OPN_TOTAL_AMNT,
          DIS_OPN_GAP_TOTAL_CNT,
          DIS_OPN_GAP_TOTAL_AMNT,
          DIS_OPN_NDS_TOTAL_CNT,
          DIS_OPN_NDS_TOTAL_AMNT,
          /*Eff indexes*/
          eff_idx_2_1  ,
          EFF_IDX_2_2  ,
          EFF_IDX_2_3  ,
          EFF_IDX_2_4_KNP as EFF_IDX_2_4  ,
          /*Rating*/
          0 as RNK_FEDERAL_EFF_IDX_2_1,
          0 as RNK_FEDERAL_EFF_IDX_2_2,
          0 as RNK_FEDERAL_EFF_IDX_2_3,
          0 as RNK_FEDERAL_EFF_IDX_2_4,

          0 as RNK_FEDERAL_EFF_IDX_2_1_PREV,
          0 as RNK_FEDERAL_EFF_IDX_2_2_PREV,
          0 as RNK_FEDERAL_EFF_IDX_2_3_PREV,
          0 as RNK_FEDERAL_EFF_IDX_2_4_PREV,

          0 as RNK_EFF_IDX_2_1,
          0 as RNK_EFF_IDX_2_2,
          0 as RNK_EFF_IDX_2_3,
          0 as RNK_EFF_IDX_2_4,

          0 as RNK_EFF_IDX_2_1_PREV,
          0 as RNK_EFF_IDX_2_2_PREV,
          0 as RNK_EFF_IDX_2_3_PREV,
          0 as RNK_EFF_IDX_2_4_PREV

          from v$efficiency_monitor_by_rf  where FISCAL_YEAR = pYear and QTR = pQtr and CALCULATION_DATE = pCalcDate
          union all
          select
          t.ufns_code as TNO_CODE,
          t.ufns_name as TNO_NAME,
          /*Declaration data: */
          t.DEDUCTION_AMNT,
          t.CALCULATION_AMNT,
          t.DECL_OPERATION_CNT,
          t.DECL_DISCREPANCY_CNT,
          t.DECL_ALL_CNT,
          t.DECL_ALL_NDS_SUM,
          t.DECL_PAY_CNT,
          t.DECL_PAY_NDS_SUM,
          t.DECL_COMPENSATE_CNT,
          abs(t.DECL_COMPENSATE_NDS_SUM) as DECL_COMPENSATE_NDS_SUM,
          t.DECL_ZERO_CNT,
          /*Discrepancy data: */
          /*All*/
          t.DIS_TOTAL_CNT,
          t.DIS_TOTAL_AMNT,
          t.DIS_GAP_TOTAL_CNT,
          t.DIS_GAP_TOTAL_AMNT,
          t.DIS_NDS_TOTAL_CNT,
          t.DIS_NDS_TOTAL_AMNT,
          /*Closed*/
          t.DIS_CLS_TOTAL_CNT,
          t.DIS_CLS_TOTAL_AMNT,
          t.DIS_CLS_GAP_TOTAL_CNT,
          t.DIS_CLS_GAP_TOTAL_AMNT,
          t.DIS_CLS_NDS_TOTAL_CNT,
          t.DIS_CLS_NDS_TOTAL_AMNT,
          /*Open*/
          t.DIS_OPN_TOTAL_CNT,
          t.DIS_OPN_TOTAL_AMNT,
          t.DIS_OPN_GAP_TOTAL_CNT,
          t.DIS_OPN_GAP_TOTAL_AMNT,
          t.DIS_OPN_NDS_TOTAL_CNT,
          t.DIS_OPN_NDS_TOTAL_AMNT,
          /*Eff indexes*/
          t.eff_idx_2_1 as EFF_IDX_2_1,
          t.eff_idx_2_2 as EFF_IDX_2_2,
          t.eff_idx_2_3 as EFF_IDX_2_3,
          t.eff_idx_2_4_knp as EFF_IDX_2_4,
          /*Rating*/
          t.rnk_federal_eff_idx_2_1 as RNK_FEDERAL_EFF_IDX_2_1,
          t.rnk_federal_eff_idx_2_2 as RNK_FEDERAL_EFF_IDX_2_2,
          t.rnk_federal_eff_idx_2_3 as RNK_FEDERAL_EFF_IDX_2_3,
          t.rnk_federal_eff_idx_2_4_knp as RNK_FEDERAL_EFF_IDX_2_4,

          prev.RNK_FEDERAL_EFF_IDX_2_1_PREV,
          prev.RNK_FEDERAL_EFF_IDX_2_2_PREV,
          prev.RNK_FEDERAL_EFF_IDX_2_3_PREV,
          prev.RNK_FEDERAL_EFF_IDX_2_4_PREV,

          t.RNK_EFF_IDX_2_1 as RNK_EFF_IDX_2_1,
          t.RNK_EFF_IDX_2_2 as RNK_EFF_IDX_2_2,
          t.RNK_EFF_IDX_2_3 as RNK_EFF_IDX_2_3,
          t.RNK_EFF_IDX_2_4_KNP as RNK_EFF_IDX_2_4,

          prev.RNK_EFF_IDX_2_1_PREV,
          prev.RNK_EFF_IDX_2_2_PREV,
          prev.RNK_EFF_IDX_2_3_PREV,
          prev.RNK_EFF_IDX_2_4_PREV
      from v$efficiency_monitor_by_ufns t
        left join
         (
           select
            UFNS_CODE,
            rnk_federal_eff_idx_2_1 as RNK_FEDERAL_EFF_IDX_2_1_PREV,
            rnk_federal_eff_idx_2_2 as RNK_FEDERAL_EFF_IDX_2_2_PREV,
            rnk_federal_eff_idx_2_3 as RNK_FEDERAL_EFF_IDX_2_3_PREV,
            rnk_federal_eff_idx_2_4_knp as RNK_FEDERAL_EFF_IDX_2_4_PREV,

            rnk_eff_idx_2_1 as RNK_EFF_IDX_2_1_PREV,
            rnk_eff_idx_2_2 as RNK_EFF_IDX_2_2_PREV,
            rnk_eff_idx_2_3 as RNK_EFF_IDX_2_3_PREV,
            rnk_eff_idx_2_4_knp as RNK_EFF_IDX_2_4_PREV
           from v$efficiency_monitor_by_ufns
      where
                FISCAL_YEAR = pYear
                and QTR = pQtr
                and CALCULATION_DATE =  (select
                                             max(calc_date)
                                           from efficiency_monitor_data
                                           where fiscal_year = pYear
                                           and period = pQtr and calc_date < pCalcDate)
                and ufns_code in ('9971', '9972', '9973', '9974', '9975', '9976', '9977', '9978', '9979')
         ) prev on prev.ufns_code = t.ufns_code
      where
        t.ufns_code in ('9971', '9972', '9973', '9974', '9975', '9976', '9977', '9978', '9979')
        and t.FISCAL_YEAR = pYear
        and t.QTR = pQtr
        and t.CALCULATION_DATE = pCalcDate;
    END IF;
  end;

  PROCEDURE GET_UFNS_IFNS_LIST ( pSelectedTno varchar2, pYear number, pQtr number, pCalcDate date, pRefCursor OUT SYS_REFCURSOR )
  as
  begin
     IF (P$IS_LAST_UNCOMPARED_PERIOD(pYear, pQtr )) THEN
        open pRefCursor for
            with
            /*Данные из отчета фоновых показателей */
            rep_data as (
                select
                    ds.sono_code                    as SONO_CODE,
                    ds.sono_name                    as SONO_NAME,
                    to_char(ds.district_code)       as DISTRICT_CODE,
                    ds.district_name                as DISTRICT_NAME,
                    ds.ufns_code                    as UFNS_CODE,
                    ds.ufns_name                    as UFNS_NAME,

                    ndsDeduction_sum_compensation
                        + ndsDeduction_sum_payment as DEDUCTION_AMNT,
                    decl_count_total               as DECL_ALL_CNT,
                    decl_count_payment             as DECL_PAY_CNT,
                    nds_sum_payment                as DECL_PAY_NDS_SUM,
                    decl_count_compensation        as DECL_COMPENSATE_CNT,
                    abs(nds_sum_compensation)      as DECL_COMPENSATE_NDS_SUM,
                    decl_count_null                as DECL_ZERO_CNT

                from report_declaration_raw r
                join v$sono_ufns_district_relation ds
                    on  r.soun_code = ds.sono_code
                where   r.fiscal_year = pYear
                        and r.tax_period = '2'||pQtr
                        and trunc(r.submit_date) = trunc( pCalcDate)
                        and r.region_code <> '99'
                        and ds.ufns_code = pSelectedTno
            )
            /* строка РФ - ПРОПУСКАЕМ*/
            /* строка ФО */
            select
                '3' as rating_type,
                '00' as TNO_CODE,
                DISTRICT_NAME                   as TNO_NAME,
                sum(DEDUCTION_AMNT)             as DEDUCTION_AMNT,
                sum(DECL_ALL_CNT)               as DECL_ALL_CNT,
                sum(DECL_PAY_CNT)               as DECL_PAY_CNT,
                sum(DECL_PAY_NDS_SUM)           as DECL_PAY_NDS_SUM,
                sum(DECL_COMPENSATE_CNT)        as DECL_COMPENSATE_CNT,
                sum(DECL_COMPENSATE_NDS_SUM)    as DECL_COMPENSATE_NDS_SUM,
                sum(DECL_ZERO_CNT)              as DECL_ZERO_CNT
            from rep_data
            group by district_name
            /* список УФНС ФО - ПРОПУСКАЕМ* */
            /* список УФНС РФ  - ПРОПУСКАЕМ*/
            union all
            /* строка УФНС */
            select
                '1' as rating_type,
                UFNS_CODE                       as TNO_CODE,
                UFNS_NAME                       as TNO_NAME,
                sum(DEDUCTION_AMNT)             as DEDUCTION_AMNT,
                sum(DECL_ALL_CNT)               as DECL_ALL_CNT,
                sum(DECL_PAY_CNT)               as DECL_PAY_CNT,
                sum(DECL_PAY_NDS_SUM)           as DECL_PAY_NDS_SUM,
                sum(DECL_COMPENSATE_CNT)        as DECL_COMPENSATE_CNT,
                sum(DECL_COMPENSATE_NDS_SUM)    as DECL_COMPENSATE_NDS_SUM,
                sum(DECL_ZERO_CNT)              as DECL_ZERO_CNT
            from rep_data
            group by UFNS_CODE, UFNS_NAME

            union all
            /* строки ИФНС */
            select
                '1' as rating_type,
                sono_code                       as TNO_CODE,
                sono_name                       as TNO_NAME,
                sum(DEDUCTION_AMNT)             as DEDUCTION_AMNT,
                sum(DECL_ALL_CNT)               as DECL_ALL_CNT,
                sum(DECL_PAY_CNT)               as DECL_PAY_CNT,
                sum(DECL_PAY_NDS_SUM)           as DECL_PAY_NDS_SUM,
                sum(DECL_COMPENSATE_CNT)        as DECL_COMPENSATE_CNT,
                sum(DECL_COMPENSATE_NDS_SUM)    as DECL_COMPENSATE_NDS_SUM,
                sum(DECL_ZERO_CNT)              as DECL_ZERO_CNT
            from rep_data
            group by sono_code, sono_name;
    ELSE
        open pRefCursor for
        /* строка РФ */
        select
          '2' as rating_type,
          '00' as TNO_CODE,
          'Российская Федерация' as TNO_NAME,
        /*Declaration data: */
          DEDUCTION_AMNT,
          CALCULATION_AMNT,
          DECL_OPERATION_CNT,
          DECL_DISCREPANCY_CNT,
          DECL_ALL_CNT,
          DECL_ALL_NDS_SUM,
          DECL_PAY_CNT,
          DECL_PAY_NDS_SUM,
          DECL_COMPENSATE_CNT,
          abs(DECL_COMPENSATE_NDS_SUM) as DECL_COMPENSATE_NDS_SUM,
          DECL_ZERO_CNT,
          /*Discrepancy data: */
          /*All*/
          DIS_TOTAL_CNT,
          DIS_TOTAL_AMNT,
          DIS_GAP_TOTAL_CNT,
          DIS_GAP_TOTAL_AMNT,
          DIS_NDS_TOTAL_CNT,
          DIS_NDS_TOTAL_AMNT,
          /*Closed*/
          DIS_CLS_TOTAL_CNT,
          DIS_CLS_TOTAL_AMNT,
          DIS_CLS_GAP_TOTAL_CNT,
          DIS_CLS_GAP_TOTAL_AMNT,
          DIS_CLS_NDS_TOTAL_CNT,
          DIS_CLS_NDS_TOTAL_AMNT,
          /*Open*/
          DIS_OPN_TOTAL_CNT,
          DIS_OPN_TOTAL_AMNT,
          DIS_OPN_GAP_TOTAL_CNT,
          DIS_OPN_GAP_TOTAL_AMNT,
          DIS_OPN_NDS_TOTAL_CNT,
          DIS_OPN_NDS_TOTAL_AMNT,
          /*Eff indexes*/
          eff_idx_2_1  ,
          EFF_IDX_2_2  ,
          EFF_IDX_2_3  ,
          EFF_IDX_2_4_KNP as EFF_IDX_2_4,
          /*Rating*/
          0 as RNK_FEDERAL_EFF_IDX_2_1,
          0 as RNK_FEDERAL_EFF_IDX_2_2,
          0 as RNK_FEDERAL_EFF_IDX_2_3,
          0 as RNK_FEDERAL_EFF_IDX_2_4,

          0 as RNK_FEDERAL_EFF_IDX_2_1_PREV,
          0 as RNK_FEDERAL_EFF_IDX_2_2_PREV,
          0 as RNK_FEDERAL_EFF_IDX_2_3_PREV,
          0 as RNK_FEDERAL_EFF_IDX_2_4_PREV,

          0 as RNK_EFF_IDX_2_1,
          0 as RNK_EFF_IDX_2_2,
          0 as RNK_EFF_IDX_2_3,
          0 as RNK_EFF_IDX_2_4,

          0 as RNK_EFF_IDX_2_1_PREV,
          0 as RNK_EFF_IDX_2_2_PREV,
          0 as RNK_EFF_IDX_2_3_PREV,
          0 as RNK_EFF_IDX_2_4_PREV

          from v$efficiency_monitor_by_rf  where FISCAL_YEAR = pYear and QTR = pQtr and CALCULATION_DATE = pCalcDate
        union all
        /* строка ФО */
        select
          '3' as rating_type,
          '00' as TNO_CODE,
          federal_district.description as TNO_NAME,
          /*Declaration data: */
          DEDUCTION_AMNT,
          CALCULATION_AMNT,
          DECL_OPERATION_CNT,
          DECL_DISCREPANCY_CNT,
          DECL_ALL_CNT,
          DECL_ALL_NDS_SUM,
          DECL_PAY_CNT,
          DECL_PAY_NDS_SUM,
          DECL_COMPENSATE_CNT,
          abs(DECL_COMPENSATE_NDS_SUM) as DECL_COMPENSATE_NDS_SUM,
          DECL_ZERO_CNT,
          /*Discrepancy data: */
          /*All*/
          DIS_TOTAL_CNT,
          DIS_TOTAL_AMNT,
          DIS_GAP_TOTAL_CNT,
          DIS_GAP_TOTAL_AMNT,
          DIS_NDS_TOTAL_CNT,
          DIS_NDS_TOTAL_AMNT,
          /*Closed*/
          DIS_CLS_TOTAL_CNT,
          DIS_CLS_TOTAL_AMNT,
          DIS_CLS_GAP_TOTAL_CNT,
          DIS_CLS_GAP_TOTAL_AMNT,
          DIS_CLS_NDS_TOTAL_CNT,
          DIS_CLS_NDS_TOTAL_AMNT,
          /*Open*/
          DIS_OPN_TOTAL_CNT,
          DIS_OPN_TOTAL_AMNT,
          DIS_OPN_GAP_TOTAL_CNT,
          DIS_OPN_GAP_TOTAL_AMNT,
          DIS_OPN_NDS_TOTAL_CNT,
          DIS_OPN_NDS_TOTAL_AMNT,
          /*Eff indexes*/
          eff_idx_2_1  ,
          EFF_IDX_2_2  ,
          EFF_IDX_2_3  ,
          EFF_IDX_2_4_KNP as EFF_IDX_2_4,
          /*Rating*/
          0 as RNK_FEDERAL_EFF_IDX_2_1,
          0 as RNK_FEDERAL_EFF_IDX_2_2,
          0 as RNK_FEDERAL_EFF_IDX_2_3,
          0 as RNK_FEDERAL_EFF_IDX_2_4,

          0 as RNK_FEDERAL_EFF_IDX_2_1_PREV,
          0 as RNK_FEDERAL_EFF_IDX_2_2_PREV,
          0 as RNK_FEDERAL_EFF_IDX_2_3_PREV,
          0 as RNK_FEDERAL_EFF_IDX_2_4_PREV,

          0 as RNK_EFF_IDX_2_1,
          0 as RNK_EFF_IDX_2_2,
          0 as RNK_EFF_IDX_2_3,
          0 as RNK_EFF_IDX_2_4,

          0 as RNK_EFF_IDX_2_1_PREV,
          0 as RNK_EFF_IDX_2_2_PREV,
          0 as RNK_EFF_IDX_2_3_PREV,
          0 as RNK_EFF_IDX_2_4_PREV

          from v$efficiency_monitor_by_fo  t, federal_district, federal_district_region
          where FISCAL_YEAR = pYear and QTR = pQtr and CALCULATION_DATE = pCalcDate
          and federal_district_region.region_code = pSelectedTno
          and t.district_code = federal_district.district_id
          and federal_district.district_id = federal_district_region.district_id
        union all
        /* список УФНС ФО */
        select
        '3' as rating_type,
        ufns.UFNS_CODE as TNO_CODE,
        ufns.UFNS_NAME as TNO_NAME,
        /*Declaration data: */
        ufns.DEDUCTION_AMNT,
        ufns.CALCULATION_AMNT,
        ufns.DECL_OPERATION_CNT,
        ufns.DECL_DISCREPANCY_CNT,
        ufns.DECL_ALL_CNT,
        ufns.DECL_ALL_NDS_SUM,
        ufns.DECL_PAY_CNT,
        ufns.DECL_PAY_NDS_SUM,
        ufns.DECL_COMPENSATE_CNT,
        abs(ufns.DECL_COMPENSATE_NDS_SUM) as DECL_COMPENSATE_NDS_SUM,
        ufns.DECL_ZERO_CNT,
        /*Discrepancy data: */
        /*All*/
        ufns.DIS_TOTAL_CNT,
        ufns.DIS_TOTAL_AMNT,
        ufns.DIS_GAP_TOTAL_CNT,
        ufns.DIS_GAP_TOTAL_AMNT,
        ufns.DIS_NDS_TOTAL_CNT,
        ufns.DIS_NDS_TOTAL_AMNT,
        /*Closed*/
        ufns.DIS_CLS_TOTAL_CNT,
        ufns.DIS_CLS_TOTAL_AMNT,
        ufns.DIS_CLS_GAP_TOTAL_CNT,
        ufns.DIS_CLS_GAP_TOTAL_AMNT,
        ufns.DIS_CLS_NDS_TOTAL_CNT,
        ufns.DIS_CLS_NDS_TOTAL_AMNT,
        /*Open*/
        ufns.DIS_OPN_TOTAL_CNT,
        ufns.DIS_OPN_TOTAL_AMNT,
        ufns.DIS_OPN_GAP_TOTAL_CNT,
        ufns.DIS_OPN_GAP_TOTAL_AMNT,
        ufns.DIS_OPN_NDS_TOTAL_CNT,
        ufns.DIS_OPN_NDS_TOTAL_AMNT,
        /*Eff indexes*/
        ufns.eff_idx_2_1 as EFF_IDX_2_1,
        ufns.eff_idx_2_2 as EFF_IDX_2_2,
        ufns.eff_idx_2_3 as EFF_IDX_2_3,
        ufns.eff_idx_2_4_knp as EFF_IDX_2_4,
        /*Rating*/
        ufns.rnk_federal_eff_idx_2_1 as RNK_FEDERAL_EFF_IDX_2_1,
        ufns.rnk_federal_eff_idx_2_2 as RNK_FEDERAL_EFF_IDX_2_2,
        ufns.rnk_federal_eff_idx_2_3 as RNK_FEDERAL_EFF_IDX_2_3,
        ufns.rnk_federal_eff_idx_2_4_knp as RNK_FEDERAL_EFF_IDX_2_4,

        prev.RNK_FEDERAL_EFF_IDX_2_1_PREV,
        prev.RNK_FEDERAL_EFF_IDX_2_2_PREV,
        prev.RNK_FEDERAL_EFF_IDX_2_3_PREV,
        prev.RNK_FEDERAL_EFF_IDX_2_4_PREV,

        ufns.rnk_eff_idx_2_1 as RNK_EFF_IDX_2_1,
        ufns.rnk_eff_idx_2_2 as RNK_EFF_IDX_2_2,
        ufns.rnk_eff_idx_2_3 as RNK_EFF_IDX_2_3,
        ufns.rnk_eff_idx_2_4_knp as RNK_EFF_IDX_2_4,

        prev.RNK_EFF_IDX_2_1_PREV,
        prev.RNK_EFF_IDX_2_2_PREV,
        prev.RNK_EFF_IDX_2_3_PREV,
        prev.RNK_EFF_IDX_2_4_PREV

        from v$efficiency_monitor_by_ufns ufns
        left join
        (
          select
            UFNS_CODE,
            rnk_federal_eff_idx_2_1 as RNK_FEDERAL_EFF_IDX_2_1_PREV,
            rnk_federal_eff_idx_2_2 as RNK_FEDERAL_EFF_IDX_2_2_PREV,
            rnk_federal_eff_idx_2_3 as RNK_FEDERAL_EFF_IDX_2_3_PREV,
            rnk_federal_eff_idx_2_4_knp as RNK_FEDERAL_EFF_IDX_2_4_PREV,

            rnk_eff_idx_2_1 as RNK_EFF_IDX_2_1_PREV,
            rnk_eff_idx_2_2 as RNK_EFF_IDX_2_2_PREV,
            rnk_eff_idx_2_3 as RNK_EFF_IDX_2_3_PREV,
            rnk_eff_idx_2_4_knp as RNK_EFF_IDX_2_4_PREV
          from v$efficiency_monitor_by_ufns ufns1
          where ufns1.FISCAL_YEAR = pYear
          and ufns1.QTR = pQtr
          and ufns1.CALCULATION_DATE =  (select
                                             max(calc_date)
                                           from efficiency_monitor_data
                                           where fiscal_year = pYear
                                           and period = pQtr and calc_date < pCalcDate)
          and substr(ufns1.UFNS_CODE, 1,2) <> '99'
          and substr(ufns1.UFNS_CODE, 1,2) IN (
                                          SELECT
                                          region_code
                                          FROM federal_district_region
                                          WHERE district_id = (
                                                              SELECT
                                                               district_id
                                                              FROM federal_district_region
                                                               WHERE region_code = pSelectedTno))
        ) prev on prev.ufns_code = ufns.UFNS_CODE

        where ufns.FISCAL_YEAR = pYear
        and ufns.QTR = pQtr
        and ufns.CALCULATION_DATE = pCalcDate
        and substr(ufns.UFNS_CODE, 1,2) <> '99'
        and substr(ufns.UFNS_CODE, 1,2) IN (
                                        SELECT
                                        region_code
                                        FROM federal_district_region
                                        WHERE district_id = (
                                                            SELECT
                                                             district_id
                                                            FROM federal_district_region
                                                             WHERE region_code = pSelectedTno))
        union all
        /* список УФНС РФ */
        select
        '2' as rating_type,
        ufns.UFNS_CODE as TNO_CODE,
        ufns.UFNS_NAME as TNO_NAME,
        /*Declaration data: */
        ufns.DEDUCTION_AMNT,
        ufns.CALCULATION_AMNT,
        ufns.DECL_OPERATION_CNT,
        ufns.DECL_DISCREPANCY_CNT,
        ufns.DECL_ALL_CNT,
        ufns.DECL_ALL_NDS_SUM,
        ufns.DECL_PAY_CNT,
        ufns.DECL_PAY_NDS_SUM,
        ufns.DECL_COMPENSATE_CNT,
        abs(ufns.DECL_COMPENSATE_NDS_SUM) as DECL_COMPENSATE_NDS_SUM,
        ufns.DECL_ZERO_CNT,
        /*Discrepancy data: */
        /*All*/
        ufns.DIS_TOTAL_CNT,
        ufns.DIS_TOTAL_AMNT,
        ufns.DIS_GAP_TOTAL_CNT,
        ufns.DIS_GAP_TOTAL_AMNT,
        ufns.DIS_NDS_TOTAL_CNT,
        ufns.DIS_NDS_TOTAL_AMNT,
        /*Closed*/
        ufns.DIS_CLS_TOTAL_CNT,
        ufns.DIS_CLS_TOTAL_AMNT,
        ufns.DIS_CLS_GAP_TOTAL_CNT,
        ufns.DIS_CLS_GAP_TOTAL_AMNT,
        ufns.DIS_CLS_NDS_TOTAL_CNT,
        ufns.DIS_CLS_NDS_TOTAL_AMNT,
        /*Open*/
        ufns.DIS_OPN_TOTAL_CNT,
        ufns.DIS_OPN_TOTAL_AMNT,
        ufns.DIS_OPN_GAP_TOTAL_CNT,
        ufns.DIS_OPN_GAP_TOTAL_AMNT,
        ufns.DIS_OPN_NDS_TOTAL_CNT,
        ufns.DIS_OPN_NDS_TOTAL_AMNT,
        /*Eff indexes*/
        ufns.eff_idx_2_1 as EFF_IDX_2_1,
        ufns.eff_idx_2_2 as EFF_IDX_2_2,
        ufns.eff_idx_2_3 as EFF_IDX_2_3,
        ufns.eff_idx_2_4_knp as EFF_IDX_2_4,
        /*Rating*/
        ufns.rnk_eff_idx_2_1 as RNK_EFF_IDX_2_1,
        ufns.rnk_eff_idx_2_2 as RNK_EFF_IDX_2_2,
        ufns.rnk_eff_idx_2_3 as RNK_EFF_IDX_2_3,
        ufns.rnk_eff_idx_2_4_knp as RNK_EFF_IDX_2_4,

        prev.RNK_EFF_IDX_2_1_PREV,
        prev.RNK_EFF_IDX_2_2_PREV,
        prev.RNK_EFF_IDX_2_3_PREV,
        prev.RNK_EFF_IDX_2_4_PREV,

        ufns.rnk_federal_eff_idx_2_1 as RNK_FEDERAL_EFF_IDX_2_1,
        ufns.rnk_federal_eff_idx_2_2 as RNK_FEDERAL_EFF_IDX_2_2,
        ufns.rnk_federal_eff_idx_2_3 as RNK_FEDERAL_EFF_IDX_2_3,
        ufns.rnk_federal_eff_idx_2_4_knp as RNK_FEDERAL_EFF_IDX_2_4,

        prev.RNK_FEDERAL_EFF_IDX_2_1_PREV,
        prev.RNK_FEDERAL_EFF_IDX_2_2_PREV,
        prev.RNK_FEDERAL_EFF_IDX_2_3_PREV,
        prev.RNK_FEDERAL_EFF_IDX_2_4_PREV

        from v$efficiency_monitor_by_ufns ufns
        left join
        (
             select
                UFNS_CODE,
                rnk_federal_eff_idx_2_1 as RNK_FEDERAL_EFF_IDX_2_1_PREV,
                rnk_federal_eff_idx_2_2 as RNK_FEDERAL_EFF_IDX_2_2_PREV,
                rnk_federal_eff_idx_2_3 as RNK_FEDERAL_EFF_IDX_2_3_PREV,
                rnk_federal_eff_idx_2_4_knp as RNK_FEDERAL_EFF_IDX_2_4_PREV,

                rnk_eff_idx_2_1 as RNK_EFF_IDX_2_1_PREV,
                rnk_eff_idx_2_2 as RNK_EFF_IDX_2_2_PREV,
                rnk_eff_idx_2_3 as RNK_EFF_IDX_2_3_PREV,
                rnk_eff_idx_2_4_knp as RNK_EFF_IDX_2_4_PREV
             from v$efficiency_monitor_by_ufns ufns1
              where ufns1.FISCAL_YEAR = pYear
              and ufns1.QTR = pQtr
              and ufns1.CALCULATION_DATE =  (select
                                             max(calc_date)
                                           from efficiency_monitor_data
                                           where fiscal_year = pYear
                                           and period = pQtr and calc_date < pCalcDate)
              and substr(ufns1.UFNS_CODE, 1,2) <> '99'
        ) prev on prev.ufns_code = ufns.UFNS_CODE
        where ufns.FISCAL_YEAR = pYear
        and ufns.QTR = pQtr
        and ufns.CALCULATION_DATE = pCalcDate
        and substr(ufns.UFNS_CODE, 1,2) <> '99'
        union all
        /* строка УФНС */
        select
        '1' as rating_type,
        ufns.UFNS_CODE as TNO_CODE,
        ufns.UFNS_NAME as TNO_NAME,
        /*Declaration data: */
        ufns.DEDUCTION_AMNT,
        ufns.CALCULATION_AMNT,
        ufns.DECL_OPERATION_CNT,
        ufns.DECL_DISCREPANCY_CNT,
        ufns.DECL_ALL_CNT,
        ufns.DECL_ALL_NDS_SUM,
        ufns.DECL_PAY_CNT,
        ufns.DECL_PAY_NDS_SUM,
        ufns.DECL_COMPENSATE_CNT,
        abs(ufns.DECL_COMPENSATE_NDS_SUM) as DECL_COMPENSATE_NDS_SUM,
        ufns.DECL_ZERO_CNT,
        /*Discrepancy data: */
        /*All*/
        ufns.DIS_TOTAL_CNT,
        ufns.DIS_TOTAL_AMNT,
        ufns.DIS_GAP_TOTAL_CNT,
        ufns.DIS_GAP_TOTAL_AMNT,
        ufns.DIS_NDS_TOTAL_CNT,
        ufns.DIS_NDS_TOTAL_AMNT,
        /*Closed*/
        ufns.DIS_CLS_TOTAL_CNT,
        ufns.DIS_CLS_TOTAL_AMNT,
        ufns.DIS_CLS_GAP_TOTAL_CNT,
        ufns.DIS_CLS_GAP_TOTAL_AMNT,
        ufns.DIS_CLS_NDS_TOTAL_CNT,
        ufns.DIS_CLS_NDS_TOTAL_AMNT,
        /*Open*/
        ufns.DIS_OPN_TOTAL_CNT,
        ufns.DIS_OPN_TOTAL_AMNT,
        ufns.DIS_OPN_GAP_TOTAL_CNT,
        ufns.DIS_OPN_GAP_TOTAL_AMNT,
        ufns.DIS_OPN_NDS_TOTAL_CNT,
        ufns.DIS_OPN_NDS_TOTAL_AMNT,
        /*Eff indexes*/
        ufns.eff_idx_2_1 as EFF_IDX_2_1,
        ufns.eff_idx_2_2 as EFF_IDX_2_2,
        ufns.eff_idx_2_3 as EFF_IDX_2_3,
        ufns.eff_idx_2_4_knp as EFF_IDX_2_4,
        /*Rating*/
        ufns.rnk_federal_eff_idx_2_1 as RNK_FEDERAL_EFF_IDX_2_1,
        ufns.rnk_federal_eff_idx_2_2 as RNK_FEDERAL_EFF_IDX_2_2,
        ufns.rnk_federal_eff_idx_2_3 as RNK_FEDERAL_EFF_IDX_2_3,
        ufns.rnk_federal_eff_idx_2_4_knp as RNK_FEDERAL_EFF_IDX_2_4,

        prev.RNK_FEDERAL_EFF_IDX_2_1_PREV,
        prev.RNK_FEDERAL_EFF_IDX_2_2_PREV,
        prev.RNK_FEDERAL_EFF_IDX_2_3_PREV,
        prev.RNK_FEDERAL_EFF_IDX_2_4_PREV,

        ufns.rnk_eff_idx_2_1 as RNK_EFF_IDX_2_1,
        ufns.rnk_eff_idx_2_2 as RNK_EFF_IDX_2_2,
        ufns.rnk_eff_idx_2_3 as RNK_EFF_IDX_2_3,
        ufns.rnk_eff_idx_2_4_knp as RNK_EFF_IDX_2_4,

        prev.RNK_EFF_IDX_2_1_PREV,
        prev.RNK_EFF_IDX_2_2_PREV,
        prev.RNK_EFF_IDX_2_3_PREV,
        prev.RNK_EFF_IDX_2_4_PREV

        from v$efficiency_monitor_by_ufns ufns
        left join
        (
             select
                UFNS_CODE,
                rnk_federal_eff_idx_2_1 as RNK_FEDERAL_EFF_IDX_2_1_PREV,
                rnk_federal_eff_idx_2_2 as RNK_FEDERAL_EFF_IDX_2_2_PREV,
                rnk_federal_eff_idx_2_3 as RNK_FEDERAL_EFF_IDX_2_3_PREV,
                rnk_federal_eff_idx_2_4_knp as RNK_FEDERAL_EFF_IDX_2_4_PREV,

                rnk_eff_idx_2_1 as RNK_EFF_IDX_2_1_PREV,
                rnk_eff_idx_2_2 as RNK_EFF_IDX_2_2_PREV,
                rnk_eff_idx_2_3 as RNK_EFF_IDX_2_3_PREV,
                rnk_eff_idx_2_4_knp as RNK_EFF_IDX_2_4_PREV
             from
             v$efficiency_monitor_by_ufns ufns1
              where
                   ufns1.FISCAL_YEAR = pYear
                   and ufns1.QTR = pQtr
                   and ufns1.CALCULATION_DATE =  (select
                                             max(calc_date)
                                           from efficiency_monitor_data
                                           where fiscal_year = pYear
                                           and period = pQtr and calc_date < pCalcDate)
                   and ufns1.UFNS_CODE = pSelectedTno
        ) prev on prev.ufns_code = ufns.UFNS_CODE
        where
             ufns.FISCAL_YEAR = pYear
             and ufns.QTR = pQtr
             and ufns.CALCULATION_DATE = pCalcDate
             and ufns.UFNS_CODE = pSelectedTno
        union all
        /* строки ИФНС */
        select
        '1' as rating_type,
        ifns.sono_code as TNO_CODE,
        ifns.sono_name as TNO_NAME,
        /*Declaration data: */
        ifns.DEDUCTION_AMNT,
        ifns.CALCULATION_AMNT,
        ifns.DECL_OPERATION_CNT,
        ifns.DECL_DISCREPANCY_CNT,
        ifns.DECL_ALL_CNT,
        ifns.DECL_ALL_NDS_SUM,
        ifns.DECL_PAY_CNT,
        ifns.DECL_PAY_NDS_SUM,
        ifns.DECL_COMPENSATE_CNT,
        abs(ifns.DECL_COMPENSATE_NDS_SUM) as DECL_COMPENSATE_NDS_SUM,
        ifns.DECL_ZERO_CNT,
        /*Discrepancy data: */
        /*All*/
        ifns.DIS_TOTAL_CNT,
        ifns.DIS_TOTAL_AMNT,
        ifns.DIS_GAP_TOTAL_CNT,
        ifns.DIS_GAP_TOTAL_AMNT,
        ifns.DIS_NDS_TOTAL_CNT,
        ifns.DIS_NDS_TOTAL_AMNT,
        /*Closed*/
        ifns.DIS_CLS_TOTAL_CNT,
        ifns.DIS_CLS_TOTAL_AMNT,
        ifns.DIS_CLS_GAP_TOTAL_CNT,
        ifns.DIS_CLS_GAP_TOTAL_AMNT,
        ifns.DIS_CLS_NDS_TOTAL_CNT,
        ifns.DIS_CLS_NDS_TOTAL_AMNT,
        /*Open*/
        ifns.DIS_OPN_TOTAL_CNT,
        ifns.DIS_OPN_TOTAL_AMNT,
        ifns.DIS_OPN_GAP_TOTAL_CNT,
        ifns.DIS_OPN_GAP_TOTAL_AMNT,
        ifns.DIS_OPN_NDS_TOTAL_CNT,
        ifns.DIS_OPN_NDS_TOTAL_AMNT,
        /*Eff indexes*/
        ifns.eff_idx_2_1 as EFF_IDX_2_1,
        ifns.eff_idx_2_2 as EFF_IDX_2_2,
        ifns.eff_idx_2_3 as EFF_IDX_2_3,
        ifns.eff_idx_2_4_knp as EFF_IDX_2_4,
        /*Rating*/
        ifns.RF_RNK_EFF_IDX_2_1 as RNK_FEDERAL_EFF_IDX_2_1,
        ifns.RF_RNK_EFF_IDX_2_2 as RNK_FEDERAL_EFF_IDX_2_2,
        ifns.RF_RNK_EFF_IDX_2_3 as RNK_FEDERAL_EFF_IDX_2_3,
        ifns.RF_RNK_EFF_IDX_2_4_KNP as RNK_FEDERAL_EFF_IDX_2_4,

        prev.RNK_FEDERAL_EFF_IDX_2_1_PREV,
        prev.RNK_FEDERAL_EFF_IDX_2_2_PREV,
        prev.RNK_FEDERAL_EFF_IDX_2_3_PREV,
        prev.RNK_FEDERAL_EFF_IDX_2_4_PREV,

        ifns.RNK_EFF_IDX_2_1 as RNK_EFF_IDX_2_1,
        ifns.RNK_EFF_IDX_2_2 as RNK_EFF_IDX_2_2,
        ifns.RNK_EFF_IDX_2_3 as RNK_EFF_IDX_2_3,
        ifns.RNK_EFF_IDX_2_4_KNP as RNK_EFF_IDX_2_4,

        prev.RNK_EFF_IDX_2_1_PREV,
        prev.RNK_EFF_IDX_2_2_PREV,
        prev.RNK_EFF_IDX_2_3_PREV,
        prev.RNK_EFF_IDX_2_4_PREV
        from v$efficiency_monitor_by_ifns  ifns
        left join
        (
             select
                sono_code,

                RF_RNK_EFF_IDX_2_1 as RNK_FEDERAL_EFF_IDX_2_1_PREV,
                RF_RNK_EFF_IDX_2_2 as RNK_FEDERAL_EFF_IDX_2_2_PREV,
                RF_RNK_EFF_IDX_2_3 as RNK_FEDERAL_EFF_IDX_2_3_PREV,
                RF_RNK_EFF_IDX_2_4_KNP as RNK_FEDERAL_EFF_IDX_2_4_PREV,

                rnk_eff_idx_2_1 as RNK_EFF_IDX_2_1_PREV,
                rnk_eff_idx_2_2 as RNK_EFF_IDX_2_2_PREV,
                rnk_eff_idx_2_3 as RNK_EFF_IDX_2_3_PREV,
                rnk_eff_idx_2_4_knp as RNK_EFF_IDX_2_4_PREV
             from v$efficiency_monitor_by_ifns  ifns1
             where
             ifns1.FISCAL_YEAR = pYear
             and ifns1.QTR = pQtr
             and ifns1.CALCULATION_DATE =  (select
                                             max(calc_date)
                                           from efficiency_monitor_data
                                           where fiscal_year = pYear
                                           and period = pQtr and calc_date < pCalcDate)
             and ifns1.ufns_code = pSelectedTno
        ) prev on prev.sono_code = ifns.sono_code
        where
             ifns.FISCAL_YEAR = pYear
             and ifns.QTR = pQtr
             and ifns.CALCULATION_DATE = pCalcDate
             and ifns.ufns_code = pSelectedTno;
    END IF;
  end;


  PROCEDURE CHART_DATA_RF ( p_fiscal_year number, p_quarter number, p_date date, pRefCursor OUT SYS_REFCURSOR )
   as
  begin
    open pRefCursor for
    with PREV_DATA as
    (
      select
      to_number(fiscal_year||qtr) as idx,
      fiscal_year,
      qtr,
      max(calculation_date) calculation_date
      from v$efficiency_monitor_by_rf
      group by
      fiscal_year,
      qtr
    )
      select
       '0' as TNO_CODE
      ,t.FISCAL_YEAR
      ,t.QTR
      ,round(t.eff_idx_2_1) as eff_idx_2_1
      ,round(t.eff_idx_2_2) as eff_idx_2_2
      ,round(t.eff_idx_2_3) as eff_idx_2_3
      ,round(t.eff_idx_2_4_knp) as eff_idx_2_4
      from v$efficiency_monitor_by_rf  t
      where
      t.CALCULATION_DATE = p_date and t.FISCAL_YEAR = p_fiscal_year and t.QTR = p_quarter
      union all
      select
       '0' as TNO_CODE
      ,t.FISCAL_YEAR
      ,t.QTR
      ,round(t.eff_idx_2_1) as eff_idx_2_1
      ,round(t.eff_idx_2_2) as eff_idx_2_2
      ,round(t.eff_idx_2_3) as eff_idx_2_3
      ,round(t.eff_idx_2_4_knp) as eff_idx_2_4
      from
      v$efficiency_monitor_by_rf  t
      inner join PREV_DATA pd on
            t.CALCULATION_DATE = pd.CALCULATION_DATE
            and t.FISCAL_YEAR = pd.FISCAL_YEAR
            and t.QTR = pd.QTR
      where  pd.idx < to_number(p_fiscal_year||p_quarter);
  end;

  PROCEDURE CHART_DATA_DISTRICT ( p_fiscal_year number, p_quarter number, p_date date, pDistrictCode varchar2, pRefCursor OUT SYS_REFCURSOR )
   as
  begin
      open pRefCursor for
    with PREV_DATA as
    (
      select
      to_number(fiscal_year||qtr) as idx,
      fiscal_year,
      qtr,
      max(calculation_date) calculation_date
      from v$efficiency_monitor_by_fo
      where  DISTRICT_CODE = pDistrictCode
      group by
      fiscal_year,
      qtr
    )
         select
           t.DISTRICT_CODE as TNO_CODE
          ,t.FISCAL_YEAR
          ,t.QTR
          ,round(t.eff_idx_2_1) as eff_idx_2_1
          ,round(t.eff_idx_2_2) as eff_idx_2_2
          ,round(t.eff_idx_2_3) as eff_idx_2_3
          ,round(t.eff_idx_2_4_knp) as eff_idx_2_4
        from
        v$efficiency_monitor_by_fo  t
         where t.DISTRICT_CODE = pDistrictCode and t.CALCULATION_DATE = p_date and t.FISCAL_YEAR = p_fiscal_year and t.QTR = p_quarter
     union all
        select
           t.DISTRICT_CODE as TNO_CODE
          ,t.FISCAL_YEAR
          ,t.QTR
          ,round(t.eff_idx_2_1) as eff_idx_2_1
          ,round(t.eff_idx_2_2) as eff_idx_2_2
          ,round(t.eff_idx_2_3) as eff_idx_2_3
          ,round(t.eff_idx_2_4_knp) as eff_idx_2_4
        from v$efficiency_monitor_by_fo  t
        inner join PREV_DATA pd on
              t.CALCULATION_DATE = pd.CALCULATION_DATE
              and t.FISCAL_YEAR = pd.FISCAL_YEAR
              and t.QTR = pd.QTR
              and pd.idx < to_number(p_fiscal_year||p_quarter)
        where t.DISTRICT_CODE = pDistrictCode;
  end;

  PROCEDURE CHART_DATA_UFNS ( p_fiscal_year number, p_quarter number, p_date date, pTnoCode varchar2, pRefCursor OUT SYS_REFCURSOR )
   as
  begin
      open pRefCursor for
        with PREV_DATA as
        (
          select
          to_number(fiscal_year||qtr) as idx,
          fiscal_year,
          qtr,
          max(calculation_date) calculation_date
          from v$efficiency_monitor_by_ufns
          where  UFNS_CODE = pTnoCode
          group by
          fiscal_year,
          qtr
        )
         select
           t.UFNS_CODE as TNO_CODE
          ,t.FISCAL_YEAR
          ,t.QTR
          ,round(t.eff_idx_2_1) as eff_idx_2_1
          ,round(t.eff_idx_2_2) as eff_idx_2_2
          ,round(t.eff_idx_2_3) as eff_idx_2_3
          ,round(t.eff_idx_2_4_knp) as eff_idx_2_4
        from
        v$efficiency_monitor_by_ufns t
        where t.UFNS_CODE = pTnoCode
         and t.FISCAL_YEAR = p_fiscal_year
         and t.QTR = p_quarter
         and t.CALCULATION_DATE = p_date
        union all
        select
           t.UFNS_CODE as TNO_CODE
          ,t.FISCAL_YEAR
          ,t.QTR
          ,round(t.eff_idx_2_1) as eff_idx_2_1
          ,round(t.eff_idx_2_2) as eff_idx_2_2
          ,round(t.eff_idx_2_3) as eff_idx_2_3
          ,round(t.eff_idx_2_4_knp) as eff_idx_2_4
        from v$efficiency_monitor_by_ufns t
        inner join PREV_DATA pd on
              t.CALCULATION_DATE = pd.CALCULATION_DATE
              and t.FISCAL_YEAR = pd.FISCAL_YEAR
              and t.QTR = pd.QTR
              and pd.idx < to_number(p_fiscal_year||p_quarter)
        where t.UFNS_CODE = pTnoCode;

  end;

  PROCEDURE CHART_DATA_IFNS ( p_fiscal_year number, p_quarter number, p_date date, pTnoCode varchar2, pRefCursor OUT SYS_REFCURSOR )
   as
  begin
      if substr(pTnoCode, 1,2) = '99' and pTnoCode <> '9901' then
       open pRefCursor for
        with PREV_DATA as
        (
          select
          to_number(fiscal_year||qtr) as idx,
          fiscal_year,
          qtr,
          max(calculation_date) calculation_date
          from V$efficiency_monitor_by_mikn
          where  sono_code = pTnoCode
          group by
          fiscal_year,
          qtr
        )
         select
           t.sono_code as TNO_CODE
          ,t.FISCAL_YEAR
          ,t.QTR
          ,round(t.eff_idx_2_1) as eff_idx_2_1
          ,round(t.eff_idx_2_2) as eff_idx_2_2
          ,round(t.eff_idx_2_3) as eff_idx_2_3
          ,round(t.eff_idx_2_4_knp) as eff_idx_2_4
        from
        V$efficiency_monitor_by_mikn t
        where t.sono_code = pTnoCode
        and t.FISCAL_YEAR = p_fiscal_year
        and t.QTR = p_quarter
        and t.CALCULATION_DATE = p_date
     union all
        select
           t.sono_code as TNO_CODE
          ,t.FISCAL_YEAR
          ,t.QTR
          ,round(t.eff_idx_2_1) as eff_idx_2_1
          ,round(t.eff_idx_2_2) as eff_idx_2_2
          ,round(t.eff_idx_2_3) as eff_idx_2_3
          ,round(t.eff_idx_2_4_knp) as eff_idx_2_4
        from V$efficiency_monitor_by_mikn t
        inner join PREV_DATA pd on
              t.CALCULATION_DATE = pd.CALCULATION_DATE
              and t.FISCAL_YEAR = pd.FISCAL_YEAR
              and t.QTR = pd.QTR
              and pd.idx < to_number(p_fiscal_year||p_quarter)
        where t.sono_code = pTnoCode;
      else
              open pRefCursor for
        with PREV_DATA as
        (
          select
          to_number(fiscal_year||qtr) as idx,
          fiscal_year,
          qtr,
          max(calculation_date) calculation_date
          from v$efficiency_monitor_by_ifns
          where  sono_code = pTnoCode
          group by
          fiscal_year,
          qtr
        )
         select
           t.sono_code as TNO_CODE
          ,t.FISCAL_YEAR
          ,t.QTR
          ,round(t.eff_idx_2_1) as eff_idx_2_1
          ,round(t.eff_idx_2_2) as eff_idx_2_2
          ,round(t.eff_idx_2_3) as eff_idx_2_3
          ,round(t.eff_idx_2_4_knp) as eff_idx_2_4
        from
        v$efficiency_monitor_by_ifns  t
        where t.sono_code = pTnoCode
        and t.FISCAL_YEAR = p_fiscal_year
        and t.QTR = p_quarter
        and t.CALCULATION_DATE = p_date
     union all
        select
           t.sono_code as TNO_CODE
          ,t.FISCAL_YEAR
          ,t.QTR
          ,round(t.eff_idx_2_1) as eff_idx_2_1
          ,round(t.eff_idx_2_2) as eff_idx_2_2
          ,round(t.eff_idx_2_3) as eff_idx_2_3
          ,round(t.eff_idx_2_4_knp) as eff_idx_2_4
        from v$efficiency_monitor_by_ifns  t
        inner join PREV_DATA pd on
              t.CALCULATION_DATE = pd.CALCULATION_DATE
              and t.FISCAL_YEAR = pd.FISCAL_YEAR
              and t.QTR = pd.QTR
              and pd.idx < to_number(p_fiscal_year||p_quarter)
        where t.sono_code = pTnoCode;
      end if;
  end;


  PROCEDURE GET_IFNS_DATA
    (
    pTnoCode varchar2,
    pYear number,
    pQtr number,
    pCalcDate date,
    pRefCursor OUT SYS_REFCURSOR
    )
  as
  begin
     IF (P$IS_LAST_UNCOMPARED_PERIOD(pYear, pQtr )) THEN
        open pRefCursor for
            with
            /*Данные из отчета фоновых показателей */
            rep_data as (
                select
                    ds.sono_code                    as SONO_CODE,
                    ds.sono_name                    as SONO_NAME,

                    ndsDeduction_sum_compensation
                        + ndsDeduction_sum_payment as DEDUCTION_AMNT,
                    decl_count_total               as DECL_ALL_CNT,
                    decl_count_payment             as DECL_PAY_CNT,
                    nds_sum_payment                as DECL_PAY_NDS_SUM,
                    decl_count_compensation        as DECL_COMPENSATE_CNT,
                    abs(nds_sum_compensation)      as DECL_COMPENSATE_NDS_SUM,
                    decl_count_null                as DECL_ZERO_CNT

                from report_declaration_raw r
                join v$sono_ufns_district_relation ds
                    on  r.soun_code = ds.sono_code
                where   r.fiscal_year = pYear
                        and r.tax_period = '2'||pQtr
                        and trunc(r.submit_date) =trunc( pCalcDate)
                        and ds.sono_code = pTnoCode
            )
            select
                SONO_CODE                       as TNO_CODE,
                SONO_NAME                       as TNO_NAME,
                sum(DEDUCTION_AMNT)             as DEDUCTION_AMNT,
                sum(DECL_ALL_CNT)               as DECL_ALL_CNT,
                sum(DECL_PAY_CNT)               as DECL_PAY_CNT,
                sum(DECL_PAY_NDS_SUM)           as DECL_PAY_NDS_SUM,
                sum(DECL_COMPENSATE_CNT)        as DECL_COMPENSATE_CNT,
                sum(DECL_COMPENSATE_NDS_SUM)    as DECL_COMPENSATE_NDS_SUM,
                sum(DECL_ZERO_CNT)              as DECL_ZERO_CNT
            from rep_data
            group by SONO_CODE, SONO_NAME;
    ELSE
          if substr(pTnoCode, 1,2) = '99' and pTnoCode <> '9901' then
              open pRefCursor for
              select
              t.sono_code as TNO_CODE,
              t.sono_name as TNO_NAME,
              /*Declaration data: */
              t.DEDUCTION_AMNT,
              t.CALCULATION_AMNT,
              t.DECL_OPERATION_CNT,
              t.DECL_DISCREPANCY_CNT,
              t.DECL_ALL_CNT,
              t.DECL_ALL_NDS_SUM,
              t.DECL_PAY_CNT,
              t.DECL_PAY_NDS_SUM,
              t.DECL_COMPENSATE_CNT,
              abs(t.DECL_COMPENSATE_NDS_SUM) as DECL_COMPENSATE_NDS_SUM,
              t.DECL_ZERO_CNT,
              /*Discrepancy data: */
              /*All*/
              t.DIS_TOTAL_CNT,
              t.DIS_TOTAL_AMNT,
              t.DIS_GAP_TOTAL_CNT,
              t.DIS_GAP_TOTAL_AMNT,
              t.DIS_NDS_TOTAL_CNT,
              t.DIS_NDS_TOTAL_AMNT,
              /*Closed*/
              t.DIS_CLS_TOTAL_CNT,
              t.DIS_CLS_TOTAL_AMNT,
              t.DIS_CLS_GAP_TOTAL_CNT,
              t.DIS_CLS_GAP_TOTAL_AMNT,
              t.DIS_CLS_NDS_TOTAL_CNT,
              t.DIS_CLS_NDS_TOTAL_AMNT,
              /*Open*/
              t.DIS_OPN_TOTAL_CNT,
              t.DIS_OPN_TOTAL_AMNT,
              t.DIS_OPN_GAP_TOTAL_CNT,
              t.DIS_OPN_GAP_TOTAL_AMNT,
              t.DIS_OPN_NDS_TOTAL_CNT,
              t.DIS_OPN_NDS_TOTAL_AMNT,
              /*Eff indexes*/
              t.eff_idx_2_1 as EFF_IDX_2_1,
              t.eff_idx_2_2 as EFF_IDX_2_2,
              t.eff_idx_2_3 as EFF_IDX_2_3,
              t.eff_idx_2_4_knp as EFF_IDX_2_4,
              /*Rating*/
              t.RF_RNK_EFF_IDX_2_1 as RNK_FEDERAL_EFF_IDX_2_1,
              t.RF_RNK_EFF_IDX_2_2 as RNK_FEDERAL_EFF_IDX_2_2,
              t.RF_RNK_EFF_IDX_2_3 as RNK_FEDERAL_EFF_IDX_2_3,
              t.RF_RNK_EFF_IDX_2_4_KNP as RNK_FEDERAL_EFF_IDX_2_4,

              t.RNK_EFF_IDX_2_1 as RNK_EFF_IDX_2_1,
              t.RNK_EFF_IDX_2_2 as RNK_EFF_IDX_2_2,
              t.RNK_EFF_IDX_2_3 as RNK_EFF_IDX_2_3,
              t.RNK_EFF_IDX_2_4_KNP as RNK_EFF_IDX_2_4

          from V$efficiency_monitor_by_mikn t
          where
            sono_code = pTnoCode
            and t.FISCAL_YEAR = pYear
            and t.QTR = pQtr
            and t.CALCULATION_DATE = pCalcDate;
          else
              open pRefCursor for
              select
              t.sono_code as TNO_CODE,
              t.sono_name as TNO_NAME,
              /*Declaration data: */
              t.DEDUCTION_AMNT,
              t.CALCULATION_AMNT,
              t.DECL_OPERATION_CNT,
              t.DECL_DISCREPANCY_CNT,
              t.DECL_ALL_CNT,
              t.DECL_ALL_NDS_SUM,
              t.DECL_PAY_CNT,
              t.DECL_PAY_NDS_SUM,
              t.DECL_COMPENSATE_CNT,
              abs(t.DECL_COMPENSATE_NDS_SUM) as DECL_COMPENSATE_NDS_SUM,
              t.DECL_ZERO_CNT,
              /*Discrepancy data: */
              /*All*/
              t.DIS_TOTAL_CNT,
              t.DIS_TOTAL_AMNT,
              t.DIS_GAP_TOTAL_CNT,
              t.DIS_GAP_TOTAL_AMNT,
              t.DIS_NDS_TOTAL_CNT,
              t.DIS_NDS_TOTAL_AMNT,
              /*Closed*/
              t.DIS_CLS_TOTAL_CNT,
              t.DIS_CLS_TOTAL_AMNT,
              t.DIS_CLS_GAP_TOTAL_CNT,
              t.DIS_CLS_GAP_TOTAL_AMNT,
              t.DIS_CLS_NDS_TOTAL_CNT,
              t.DIS_CLS_NDS_TOTAL_AMNT,
              /*Open*/
              t.DIS_OPN_TOTAL_CNT,
              t.DIS_OPN_TOTAL_AMNT,
              t.DIS_OPN_GAP_TOTAL_CNT,
              t.DIS_OPN_GAP_TOTAL_AMNT,
              t.DIS_OPN_NDS_TOTAL_CNT,
              t.DIS_OPN_NDS_TOTAL_AMNT,
              /*Eff indexes*/
              t.eff_idx_2_1 as EFF_IDX_2_1,
              t.eff_idx_2_2 as EFF_IDX_2_2,
              t.eff_idx_2_3 as EFF_IDX_2_3,
              t.eff_idx_2_4_knp as EFF_IDX_2_4,
              /*Rating*/
              t.RF_RNK_EFF_IDX_2_1 as RNK_FEDERAL_EFF_IDX_2_1,
              t.RF_RNK_EFF_IDX_2_2 as RNK_FEDERAL_EFF_IDX_2_2,
              t.RF_RNK_EFF_IDX_2_3 as RNK_FEDERAL_EFF_IDX_2_3,
              t.RF_RNK_EFF_IDX_2_4_KNP as RNK_FEDERAL_EFF_IDX_2_4,

              t.RNK_EFF_IDX_2_1 as RNK_EFF_IDX_2_1,
              t.RNK_EFF_IDX_2_2 as RNK_EFF_IDX_2_2,
              t.RNK_EFF_IDX_2_3 as RNK_EFF_IDX_2_3,
              t.RNK_EFF_IDX_2_4_KNP as RNK_EFF_IDX_2_4

          from v$efficiency_monitor_by_ifns  t
          where
            sono_code = pTnoCode
            and t.FISCAL_YEAR = pYear
            and t.QTR = pQtr
            and t.CALCULATION_DATE = pCalcDate;

        end if;
    END IF;
  end;

 PROCEDURE P$GET_IFNS_RATING_BY_INDEX_24(pYear number, pQtr number, pCalcDate date, pRefCursor OUT SYS_REFCURSOR)
  as
 begin
   open pRefCursor for
    with Res as
    (
      select
        t.calc_date,
        rank() over (partition by calc_date order by nvl( (buyer_dis_opn_knp_amnt / nullif(deduction_amnt, 0)), 0 ) ) as Rating,
        t.sono_code as SonoCode,
        s.s_name as SonoName,
        t.region_code||'00' as RegionCode,
        sr.s_name as RegionName,
        t.buyer_dis_opn_knp_amnt as DiscrepancyAmount,
        t.deduction_amnt as DeductionAmount,
        round(nvl( (buyer_dis_opn_knp_amnt / nullif(deduction_amnt, 0)), 0 )*100, 2) as ShareValue
      from
        efficiency_monitor_data t
        inner join v$sono s on s.s_code = t.sono_code
        inner join v$sono sr on sr.s_code = t.region_code||'00'
      where t.fiscal_year=pYear and t.period = pQtr and t.sono_code not in ('9971', '9972', '9973', '9974', '9975', '9976', '9977', '9978', '9979')
    ),
    PrevRes as
    (
     select
     *
     from
     Res
     where calc_date = (select max(calc_date) from Res where calc_date < pCalcDate)
    )
    select
      t1.*,
      t2.Rating as RatingPrev
    from Res t1
    left  join PrevRes t2 on t1.SonoCode = t2.SonoCode
    where trunc(t1.calc_date) = trunc(pCalcDate);
 end;

  /*Расхождения покупателя*/
  procedure P$BUILD_BUYER_DECL_STAGE (p_year number, p_qtr number)
  as
  v_exist number;
  v_partition_name varchar2(128 char);
  pragma autonomous_transaction;
  v_start_time number;
  v_end_time number;
  v_query_start_time number;
  begin
  v_start_time := GET_UNIX_TS;
  log('Обновление  EFF_MON_BUYER_DECL_STG ('||p_year||', '||p_qtr||')', 0);
  execute immediate 'truncate table EFF_MON_BUYER_DECL_STG';

  EXECUTE_SQL('drop table EM_TM_MC_DECL', true);
  EXECUTE_SQL('drop table EM_TM_MC_SEOD_DECL', true);
  EXECUTE_SQL('drop table EM_TM_DIS_UNQ', true);
  EXECUTE_SQL('drop table EM_TM_DIS_DECL', true);


  /* Расчитаем перечень уникальных регномеров для НД */
  v_query_start_time := GET_UNIX_TS;
  EXECUTE_SQL('
create table EM_TM_MC_SEOD_DECL
pctfree 0
compress
as
with  
mc_decl as (
    select /*+ MATERIALIZE */
        nd.*
    from v$askdekl nd
    inner join v$askoperatsiya ao 
        on ao.NomerOp = nd.IdZagruzka
    where ao.VidOp = 1
),
all_korrs as (
    select
        nd.zip,
        nd.id as decl_id,
        to_number(nd.OTCHETGOD) as fiscal_year,
        nd.PERIOD as tax_period,
        f$tax_period_to_quarter(nd.PERIOD) as quarter,
        nd.INNNP as inn,
        coalesce(nd.effkpp, f$get_effective_kpp(nd.innnp,nd.KPPNP)) as kpp_effective,
        nd.KODNO as sono_code
    from mc_decl nd
),
actual_korrs as (
    select 
        Z.*,
        row_number() over (partition by z.inn, 
                                        z.fiscal_year, 
                                        z.quarter 
                            order by  z.id desc) as rk
    from (
        select
            nd.ID,
            to_number(nd.OTCHETGOD) as fiscal_year,
            f$tax_period_to_quarter(nd.PERIOD) as quarter,
            nd.INNNP as inn,
            coalesce(nd.effkpp, f$get_effective_kpp(nd.innnp,nd.KPPNP)) as kpp_effective,
            nd.KODNO as sono_code,
            row_number() over (partition by to_number(nd.OTCHETGOD), 
                                            f$tax_period_to_quarter(nd.PERIOD), 
                                            nd.INNNP order by  
                                            nd.NOMKORR desc, 
                                            nd.INNREORG nulls first) as rnk
        from
            mc_decl nd
        where nd.PriznAktKorr = 1
        union all
        select
            nd.ID,
            to_number(nd.OTCHETGOD) as fiscal_year,
            f$tax_period_to_quarter(nd.PERIOD) as quarter,
            nd.INNREORG as inn,
            f$get_effective_kpp(nd.innReorg,nd.kppReorg) as kpp_effective,
            nd.KODNO as sono_code,
            row_number() over ( partition by    to_number(nd.OTCHETGOD), 
                                                f$tax_period_to_quarter(nd.PERIOD), 
                                                nd.INNREORG 
                                order by  nd.NOMKORR desc) as rnk
        from mc_decl nd
        where 
            nd.PriznAktKorr = 1
            and nd.INNREORG is not null
        ) Z 
    where Z.rnk = 1
)
select
    distinct
    t1.zip,
    t1.decl_id,
    t1.inn,
    t1.kpp_effective,
    t1.sono_code,
    t1.quarter,
    t1.tax_period,
    t1.fiscal_year,
    nvl(t2.sono_code, t1.sono_code) as actual_sono_code
from all_korrs t1
left join actual_korrs t2
    on t1.inn = t2.inn
    and t1.kpp_effective = t2.kpp_effective
    and t1.quarter = t2.quarter
    and t1.fiscal_year = t2.fiscal_year
    and t2.rk = 1
');
  log('Success: EM_TM_MC_SEOD_DECL: '||SQL%ROWCOUNT||' записей. Время выполнения: '||(GET_UNIX_TS - v_query_start_time), 0);

  /* Данные расхождений */
  v_query_start_time := GET_UNIX_TS;
  EXECUTE_SQL('
create table EM_TM_DIS_UNQ
pctfree 0
compress
as
with 
    dis as (
	/*Отбираем айдишники КНП расхождения по стороне покупателя 
		и айдишники АТ в которых они направлялись покупателю
	*/
        select
            dd.discrepancy_id,
            min(d.doc_id) as doc_id
        from doc_discrepancy dd
        inner join sov_discrepancy sd
            on dd.discrepancy_id = sd.id
            and sd.invoice_chapter = 8
        inner join doc d
            on d.doc_id = dd.doc_id
            and d.inn = sd.buyer_inn
            and d.status not in (-1,1,3,9,11)
            and d.doc_type = 1
			and d.knp_sync_status > 0
        group by dd.discrepancy_id
)
select /*+ opt_param(''_pred_move_around'',''false'') */
    n1.*,
    svd.type,
    least(dd.amount, svd.amnt) as amnt,
    svd.status,
    svd.buyer_zip as buyer_decl_zip,
    n2.ref_entity_id,
    n2.sono_code,
    sed.fiscal_year,
    sed.tax_period,
    f$tax_period_to_quarter(sed.tax_period) as quarter,
    mcd.INNREORG,
    mcd.innnp,
    sed.inn,
    case 
        when svd.status = 1 and svd.invoice_rk like ''%A%'' and sdiq.discrepancy_id is null then 1
        when svd.status = 1 and svd.invoice_rk like ''%A%'' and sdiq.has_invoice_duplicate = 1 then 2
        when svd.status = 1 and svd.invoice_rk not like ''%A%''
                             and to_number(mcd.OTCHETGOD||f$tax_period_to_quarter(mcd.PERIOD))
                             <> to_number(sed.fiscal_year||f$tax_period_to_quarter(sed.tax_period)) then 3
        else 0
    end as exclude_from_calculation
from dis n1
inner join doc n2 
    on n2.doc_id = n1.doc_id
inner join sov_discrepancy svd 
    on svd.id = n1.discrepancy_id
inner join nds2_seod.seod_declaration sed 
    on sed.decl_reg_num = n2.ref_entity_id 
    and sed.sono_code = n2.sono_code
inner join v$askdekl mcd 
    on mcd.ZIP = svd.buyer_zip
inner join doc_discrepancy dd 
    on dd.doc_id = n1.doc_id 
    and dd.discrepancy_id = n1.discrepancy_id
left join SOV_DISCREPANCY_INVOICE_QTR sdiq
     on sdiq.discrepancy_id = n1.discrepancy_id
     and sdiq.fiscal_year = sed.fiscal_year
     and sdiq.quarter = f$tax_period_to_quarter(sed.tax_period)'
);
  log('Success: EM_TM_DIS_UNQ: '||SQL%ROWCOUNT||' записей. Время выполнения: '||(GET_UNIX_TS - v_query_start_time), 0);

  /* Мердж расхождений и НД*/
  v_query_start_time := GET_UNIX_TS;
  EXECUTE_SQL('
create table EM_TM_DIS_DECL
pctfree 0
compress
as
  select
      dd.discrepancy_id
    , dd.type
    , dd.amnt
    , dd.status
    , dd.fiscal_year as fiscal_year
    , dd.quarter
    , dd.tax_period
    , dd.inn
    , sd.actual_sono_code as sono_code
    , sd.zip
    from EM_TM_DIS_UNQ dd
    inner join EM_TM_MC_SEOD_DECL sd 
        on sd.zip = dd.buyer_decl_zip
    where 
        (dd.inn = dd.innnp or dd.inn = dd.INNREORG) 
        and dd.exclude_from_calculation = 0
');
  log('Success: EM_TM_DIS_DECL: '||SQL%ROWCOUNT||' записей. Время выполнения: '||(GET_UNIX_TS - v_query_start_time), 0);

  /* Мердж расхождений и НД*/
  v_query_start_time := GET_UNIX_TS;
  EXECUTE_SQL('
insert /*APPEND*/ into EFF_MON_BUYER_DECL_STG
   select
    mcdekl.id,
    knp.inn,
    knp.tax_period,
    knp.quarter,
    knp.fiscal_year,
    knp.sono_code,
    trunc(sysdate) as calc_date,
  /*Сумма открытых расхождений(разрыв и НДС) всего в тыс. руб.*/
    sum( case when knp.type in (1,4) and knp.status <> 2 then knp.amnt else 0 end) as buyer_dis_opn_amnt,
  /*Сумма открытых расхождений(разрыв и НДС) КНП в тыс. руб.*/
    sum( case when knp.type in (1,4) and knp.status <> 2 then knp.amnt else 0 end) as buyer_dis_opn_knp_amnt,
  /*Кол-во открытых расхождений(разрыв и НДС) КНП*/
    sum( case when knp.type in (1,4) and knp.status <> 2 then 1 else 0 end) as buyer_dis_opn_knp_cnt,
  /*Сумма открытых расхождений вида разрыв всего в тыс. руб.*/
    sum( case when knp.type = 1 and knp.status <> 2 then knp.amnt else 0 end) as buyer_dis_opn_gap_amnt,
  /*Сумма открытых расхождений КНП вида разрыв в тыс. руб.*/
    sum( case when knp.type = 1 and knp.status <> 2 then knp.amnt else 0 end) as buyer_dis_opn_gap_knp_amnt,
  /*Кол-во открытых расхождений КНП вида разрыв*/
    sum( case when knp.type = 1 and knp.status <> 2 then 1 else 0 end) as buyer_dis_opn_gap_knp_cnt,
  /*Сумма открытых расхождений вида НДС всего в тыс. руб.*/
    sum( case when knp.type = 4 and knp.status <> 2 then knp.amnt else 0 end) as buyer_dis_opn_nds_amnt,
  /*Сумма открытых расхождений КНП вида НДС в тыс. руб.*/
    sum( case when knp.type = 4 and knp.status <> 2 then knp.amnt else 0 end) as buyer_dis_opn_nds_knp_amnt,
  /*Кол-во открытых расхождений КНП вида НДС*/
    sum( case when knp.type = 4 and knp.status <> 2 then 1 else 0 end) as buyer_dis_opn_nds_knp_cnt,
  /*Сумма открытых и закрытых расхождений КНП в тыс. руб.*/
    sum( case when knp.type in (1,4) then knp.amnt else 0 end) as buyer_dis_all_knp_amnt,
  /*Сумма закрытых расхождений*/
    0 as buyer_dis_cls_amnt,
  /*Сумма закрытых расхождений КНП в тыс. руб.*/
    sum( case when knp.discrepancy_id is not null and knp.type in (1,4) and knp.status = 2 then knp.amnt else 0 end) as buyer_dis_cls_knp_amnt,
  /*Кол-во закрытых расхождений КНП*/
    sum( case when knp.type in (1,4) and knp.status = 2 then 1 else 0 end) as buyer_dis_cls_knp_cnt,
  /*Сумма закрытых расхождений разрыв в тыс. руб.*/
    sum( case when knp.type = 1 and knp.status = 2 then knp.amnt else 0 end) as buyer_dis_cls_gap_amnt,
  /*Сумма закрытых расхождений КНП разрыв в тыс. руб.*/
    sum( case when knp.type = 1 and knp.status = 2 then knp.amnt else 0 end) as buyer_dis_cls_gap_knp_amnt,
  /*Кол-во закрытых расхождений КНП разрыв*/
    sum( case when knp.type = 1 and knp.status = 2 then 1 else 0 end) as buyer_dis_cls_gap_knp_cnt,
  /*Сумма закрытых расхождений НДС в тыс. руб.*/
    sum( case when knp.type = 4 and knp.status = 2 then knp.amnt else 0 end) as buyer_dis_cls_nds_amnt,
  /*Кол-во закрытых расхождений КНП НДС*/
    sum( case when knp.type = 4 and knp.status = 2 then 1 else 0 end)  as buyer_dis_cls_nds_knp_cnt,
  /*Сумма закрытых расхождений КНП НДС в тыс. руб.*/
    sum( case when knp.type = 4 and knp.status = 2 then knp.amnt else 0 end) as buyer_dis_cls_nds_knp_amnt
  from EM_TM_DIS_DECL knp
  inner join v$askdekl mcdekl 
    on mcdekl.zip = knp.zip
  group by
      mcdekl.id,
      knp.inn,
      knp.tax_period,
      knp.quarter,
      knp.fiscal_year,
      knp.sono_code');

  log('Success: EFF_MON_BUYER_DECL_STG: '||SQL%ROWCOUNT||' записей. Время выполнения: '||(GET_UNIX_TS - v_query_start_time), 0);

  select
     max(partition_name) into v_partition_name
  from EFFICIENCY_MON_DET_PARTITION
  where calculation_date = trunc(sysdate);

  if v_partition_name is null then
    EXECUTE_SQL('alter table EFFICIENCY_MONITOR_DETAIL
    add partition p_effmon_detail_'||SEQ_EFF_MON_DETAILS.Nextval||'
    values(to_date('''||to_char(sysdate, 'dd.mm.yyyy')||''', ''dd.mm.yyyy''))');
    insert into EFFICIENCY_MON_DET_PARTITION values(trunc(sysdate), 'p_effmon_detail_'||SEQ_EFF_MON_DETAILS.CURRVAL);
  else
    EXECUTE_SQL('alter table EFFICIENCY_MONITOR_DETAIL truncate partition '||v_partition_name);
  end if;

  insert /*+APPEND*/ into EFFICIENCY_MONITOR_DETAIL
  select
    d.*
  from EFF_MON_BUYER_DECL_STG d
  inner join v$askdekl b 
    on b.id = d.id
  where 
    trunc(calc_date) = trunc(sysdate) 
    and to_number(nvl(b.priznaktkorr, 0 )) = 1 
    and d.buyer_dis_opn_knp_cnt > 0;


  v_end_time := GET_UNIX_TS;

  log('Обновлен EFF_MON_BUYER_DECL_STG: '||SQL%ROWCOUNT||' записей. Время выполнения: '||(v_end_time - v_start_time), 0);

    commit;
    exception when others then
      rollback;
      v_end_time := GET_UNIX_TS;
      log('('||(v_end_time - v_start_time)||')Ошибка обновления EFF_MON_BUYER_DECL_STG: '||sqlerrm, 3);
  end;

  /*Расхождения продавца*/
  procedure P$BUILD_SELLER_DECL_STAGE  (p_year number, p_qtr number)
  as
  v_exist number;
  pragma autonomous_transaction;
  begin
    log('Обновление  EFF_MON_SELLER_DECL_STG('||p_year||', '||p_qtr||')', 0);
    execute immediate 'truncate table EFF_MON_SELLER_DECL_STG';

    insert /* +APPEND */ into EFF_MON_SELLER_DECL_STG
    with DECLARATION as (
      select
        distinct
         to_number(da.period||da.otchetgod||da.innnp) as id
         ,da.kodno as sono_code
         ,f$tax_period_to_quarter(da.period) as quarter
         ,to_number(da.otchetgod) as fiscal_year
         ,da.innnp as inn
         ,da.period
      from v$askdekl da
      where da.priznaktkorr = 1
      and da.otchetgod = to_char(p_year)
      and f$tax_period_to_quarter(da.period) = p_qtr
      )
    select
      d.id,
      d.inn,
      d.period,
      d.quarter,
      d.fiscal_year,
      d.sono_code,
      trunc(sysdate) as calc_date,
    /*Сумма открытых расхождений(разрыв и НДС) всего*/
      0 as seller_dis_opn_amnt,
    /*Сумма открытых расхождений(разрыв и НДС) КНП*/
      0 as seller_dis_opn_knp_amnt,
    /*Кол-во открытых расхождений(разрыв и НДС) КНП*/
      0 as seller_dis_opn_knp_cnt,
    /*Сумма открытых расхождений вида разрыв всего*/
      0 as seller_dis_opn_gap_amnt,
    /*Сумма открытых расхождений КНП вида разрыв*/
      0 as seller_dis_opn_gap_knp_amnt,
    /*Кол-во открытых расхождений КНП вида разрыв*/
    0 as seller_dis_opn_gap_knp_cnt,
    /*Сумма открытых расхождений вида НДС всего*/
    0 as seller_dis_opn_nds_amnt,
    /*Сумма открытых расхождений КНП вида НДС*/
    0 as seller_dis_opn_nds_knp_amnt,
    /*Кол-во открытых расхождений КНП вида НДС*/
    0 as seller_dis_opn_nds_knp_cnt,
    /*Сумма открытых и закрытых расхождений КНП*/
    0 as seller_dis_all_knp_amnt,
    /*Сумма закрытых расхождений*/
    0 as seller_dis_cls_amnt,
    /*Сумма закрытых расхождений КНП*/
    0 as seller_dis_cls_knp_amnt,
    /*Кол-во закрытых расхождений КНП*/
    0 as seller_dis_cls_knp_cnt,
    /*Сумма закрытых расхождений разрыв*/
    0 as seller_dis_cls_gap_amnt,
    /*Сумма закрытых расхождений КНП разрыв*/
    0 as seller_dis_cls_gap_knp_amnt,
    /*Кол-во закрытых расхождений КНП разрыв*/
    0 as seller_dis_cls_gap_knp_cnt,
    /*Сумма закрытых расхождений НДС*/
    0 as seller_dis_cls_nds_amnt,
    /*Сумма закрытых расхождений КНП НДС*/
    0 as seller_dis_cls_nds_knp_amnt,
    /*Кол-во закрытых расхождений КНП НДС*/
    0 as seller_dis_cls_nds_knp_cnt
    from DECLARATION d
    group by
    d.id,
    d.inn,
    d.period,
    d.quarter,
    d.fiscal_year,
    d.sono_code;

    log('Обновлен EFF_MON_SELLER_DECL_STG: '||SQL%ROWCOUNT||' записей', 0);

    commit;
    exception when others then
      rollback;
      log('Ошибка обновления EFF_MON_SELLER_DECL_STG: '||sqlerrm, 3);
  end;

  /*выгрузка результатов*/
  procedure P$EXPORT_RESULT
  as
  v_exist number;
  v_max_date_qtr number;
  v_min_date_qtr number;
  v_curr_year number;
  v_curr_qtr number;
  pragma autonomous_transaction;
  begin

    v_curr_year := f$fiscal_year (sysdate);
    v_curr_qtr := f$tax_period_to_quarter (f$get_tax_period_by_date(sysdate));

    v_max_date_qtr := to_number(v_curr_year||v_curr_qtr);
    v_min_date_qtr := to_number((v_curr_year - 3)||v_curr_qtr);


    log('экспорт в efficiency_monitor_data', 0);

    delete from efficiency_monitor_data
    where rowid in (
                    select rowid 
                    from efficiency_monitor_data 
                    where calc_date = trunc(sysdate)
    );

    if(sql%rowcount > 0) then
        log('Удалено записей предыдущего расчета за '||to_char(sysdate)||' из efficiency_monitor_data: '||sql%rowcount , 0);
    end if;

    insert /* +APPEND */ into efficiency_monitor_data select * from V$EFFICIENCY_MONITOR_AGGR_SRC
    where to_number(fiscal_year||quarter) between v_min_date_qtr and v_max_date_qtr;

    delete from efficiency_monitor_data where to_number(fiscal_year||period) < 20152; --чистим все до 2кв 2015

    commit;

	begin
      dbms_stats.gather_table_stats(
         ownname => 'NDS2_MRR_USER'
        ,tabname => 'EFFICIENCY_MONITOR_DATA'
        ,cascade => true
        ,degree => 16
      );
      exception when others then
        log('Ошибка сбора статистики efficiency_monitor_data: '||sqlerrm, 3);
    end;

    exception when others then
      rollback;
      log('Ошибка обновления efficiency_monitor_data: '||sqlerrm, 3);
  end;

  PROCEDURE P$UPDATE_AGGREGATE_EXT
  (
    p_year number,
    p_qtr number
  )
  AS
  pragma autonomous_transaction;
  BEGIN
    log('Обновление  агрегатов мониторинга эффективности ', 0);
    p$build_buyer_decl_stage (p_year, p_qtr);
    p$export_result;

  commit;
    log('Обновление  агрегатов мониторинга эффективности завершено', 0);
  END;


PROCEDURE P$UPDATE_AGGREGATE
  AS
    v_current_year number;
    v_current_qtr number;
    v_prev_qtr_last_upd_date date;
    v_prev_qtr number;
    v_prev_year number;
    v_current_month number;
    v_current_day number;
    v_calc_year number;
    v_current_date date;
  BEGIN

     P$UPDATE_AGGREGATE_EXT( v_current_year, v_current_qtr );

  END;
END;
/
