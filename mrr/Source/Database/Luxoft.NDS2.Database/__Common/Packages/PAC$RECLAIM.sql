﻿CREATE OR REPLACE PACKAGE NDS2_MRR_USER.PAC$RECLAIM
as

PROCEDURE P$LOG_ERROR
(
  pTitle RECLAIM_LOG.TITLE%TYPE,
  pMessage RECLAIM_LOG.MESSAGE%TYPE,
  pZip RECLAIM_LOG.ZIP%TYPE,
  pSonoCode RECLAIM_LOG.SONO_CODE%TYPE,
  pRegNumber RECLAIM_LOG.Reg_Number%TYPE,
  pQueueId RECLAIM_LOG.QUEUE_ID%TYPE
);

PROCEDURE P$LOG_WARNING
(
  pTitle RECLAIM_LOG.TITLE%TYPE,
  pMessage RECLAIM_LOG.MESSAGE%TYPE,
  pZip RECLAIM_LOG.ZIP%TYPE,
  pSonoCode RECLAIM_LOG.SONO_CODE%TYPE,
  pRegNumber RECLAIM_LOG.Reg_Number%TYPE,
  pQueueId RECLAIM_LOG.QUEUE_ID%TYPE
);

PROCEDURE P$LOG_INFORMATION
(
  pTitle RECLAIM_LOG.TITLE%TYPE,
  pMessage RECLAIM_LOG.MESSAGE%TYPE,
  pZip RECLAIM_LOG.ZIP%TYPE,
  pSonoCode RECLAIM_LOG.SONO_CODE%TYPE,
  pRegNumber RECLAIM_LOG.Reg_Number%TYPE,
  pQueueId RECLAIM_LOG.QUEUE_ID%TYPE
);

PROCEDURE P$GET_DISCREPANCIES_SIDE_ONE
(
  pQueueId ASK_RECLAIM_QUEUE.ID%TYPE,
  pCursor out SYS_REFCURSOR
);

PROCEDURE P$GET_DISCREPANCIES_SIDE_TWO
(
  pQueueId ASK_RECLAIM_QUEUE.ID%TYPE,
  pCursor out SYS_REFCURSOR
);

PROCEDURE P$INSERT_DISCREPANCY
(
  VP_DocId DOC.DOC_ID%TYPE,
  VP_DiscrepancyId DOC_DISCREPANCY.DISCREPANCY_ID%TYPE,
  VP_RowKey DOC_DISCREPANCY.ROW_KEY%TYPE,
  VP_Amount DOC_DISCREPANCY.AMOUNT%TYPE,
  VP_AmountPvp DOC_DISCREPANCY.AMOUNT_PVP%TYPE
);

PROCEDURE P$INSERT_INVOICES
(
  pDocId DOC.DOC_ID%TYPE
);

PROCEDURE P$INSERT_DOC
(
  pRegNumber DOC.REF_ENTITY_ID%TYPE,
  pDocType DOC.DOC_TYPE%TYPE,
  pDocKind DOC.DOC_KIND%TYPE,
  pSonoCode DOC.SONO_CODE%TYPE,
  pDiscrepancyCount DOC.Discrepancy_Count%TYPE,
  pDiscrepancyAmount DOC.Discrepancy_Amount%TYPE,
  pProcessed DOC.Processed%TYPE,
  pInn DOC.Inn%TYPE,
  pKppEffective DOC.Kpp_Effective%TYPE,
  pQueueId ASK_RECLAIM_QUEUE.ID%TYPE,
  pZip DOC.ZIP%TYPE,
  pQuarter DOC.QUARTER%TYPE,
  pFiscalYear DOC.FISCAL_YEAR%TYPE,
  pDocId out DOC.DOC_ID%TYPE
);

PROCEDURE P$UPDATE_DOC
(
  VP_DocId DOC.DOC_ID%TYPE,
  VP_DiscrepancyCount DOC.Discrepancy_Count%TYPE,
  VP_DiscrepancyAmount DOC.Discrepancy_Amount%TYPE,
  VP_DiscrepancyAmountPvp DOC.Discrepancy_Amount_Pvp%TYPE,
  VP_HasSchemaValidationErrors NUMBER,
  VP_HasDatabaseError NUMBER,
  VP_HasGeneralError NUMBER,
  VP_DocumentBody CLOB,
  VP_Processed DOC.Processed%TYPE
);

PROCEDURE P$PULL_QUEUE
(
  pQueueId out ASK_RECLAIM_QUEUE.ID%TYPE,
  pCursor out SYS_REFCURSOR
);

PROCEDURE P$EXISTS_IN_QUEUE
(
  pExists out NUMBER
);

PROCEDURE P$UPDATE_QUEUE
(
  pQueueId in ASK_RECLAIM_QUEUE.ID%TYPE,
  pState in ASK_RECLAIM_QUEUE.STATE%TYPE,
  pIsLocked in ASK_RECLAIM_QUEUE.IS_LOCKED%TYPE
);

PROCEDURE P$GET_STATE_SEOD_KNP
(
  pSonoCode in VARCHAR2,
  pRegNumber in NUMBER,
  pStateKnp out NUMBER
);

PROCEDURE P$AUTO_CLOSE;

PROCEDURE P$FILL_QUEUE;

END;
/
CREATE OR REPLACE PACKAGE BODY NDS2_MRR_USER.PAC$RECLAIM
as

LOG_TYPE_ERROR CONSTANT NUMBER(2)  := 0;
LOG_TYPE_WARNING CONSTANT NUMBER(2)  := 1;
LOG_TYPE_INFORMATION CONSTANT NUMBER(2)  := 2;

DOC_STATUS_PREPARE_DATA NUMBER(2) := 12;
DOC_STATUS_MANUAL_CHECK NUMBER(2) := 11;
DOC_STATUS_CREATED NUMBER(2) := 1;
DOC_STATUS_ERROR NUMBER(2) := 3;

DISCREPANCY_TYPE_GAP NUMBER(1) := 1;
DISCREPANCY_TYPE_NDS NUMBER(1) := 4;

LIMIT_AUTOCLOSE_PARAM_NAME CONSTANT varchar(25) := 'ai_autoclose_items_limit';

Type T$RECLAIM_QUEUE_ITEM is record
(
  id           NUMBER,
  doc_id       NUMBER,
  side         NUMBER(1),
  close_reason NUMBER
);

PROCEDURE P$LOG
(
  pType RECLAIM_LOG.TYPE%TYPE,
  pTitle RECLAIM_LOG.TITLE%TYPE,
  pMessage RECLAIM_LOG.MESSAGE%TYPE,
  pZip RECLAIM_LOG.ZIP%TYPE,
  pSonoCode RECLAIM_LOG.SONO_CODE%TYPE,
  pRegNumber RECLAIM_LOG.Reg_Number%TYPE,
  pQueueId RECLAIM_LOG.QUEUE_ID%TYPE
)
AS
pragma autonomous_transaction;
begin
  insert into RECLAIM_LOG(id, type, title, message, zip, sono_code, reg_number, log_date, queue_id)
  values(SEQ_RECLAIM_LOG.NEXTVAL, pType, pTitle, pMessage, pZip, pSonoCode, pRegNumber, sysdate, pQueueId);
  commit;
exception when others then
  rollback;
end;

PROCEDURE P$LOG_ERROR
(
  pTitle RECLAIM_LOG.TITLE%TYPE,
  pMessage RECLAIM_LOG.MESSAGE%TYPE,
  pZip RECLAIM_LOG.ZIP%TYPE,
  pSonoCode RECLAIM_LOG.SONO_CODE%TYPE,
  pRegNumber RECLAIM_LOG.Reg_Number%TYPE,
  pQueueId RECLAIM_LOG.QUEUE_ID%TYPE
)
AS
begin
  P$LOG(LOG_TYPE_ERROR, pTitle, pMessage, pZip, pSonoCode, pRegNumber, pQueueId);
end;

PROCEDURE P$LOG_WARNING
(
  pTitle RECLAIM_LOG.TITLE%TYPE,
  pMessage RECLAIM_LOG.MESSAGE%TYPE,
  pZip RECLAIM_LOG.ZIP%TYPE,
  pSonoCode RECLAIM_LOG.SONO_CODE%TYPE,
  pRegNumber RECLAIM_LOG.Reg_Number%TYPE,
  pQueueId RECLAIM_LOG.QUEUE_ID%TYPE
)
AS
begin
  P$LOG(LOG_TYPE_WARNING, pTitle, pMessage, pZip, pSonoCode, pRegNumber, pQueueId);
end;

PROCEDURE P$LOG_INFORMATION
(
  pTitle RECLAIM_LOG.TITLE%TYPE,
  pMessage RECLAIM_LOG.MESSAGE%TYPE,
  pZip RECLAIM_LOG.ZIP%TYPE,
  pSonoCode RECLAIM_LOG.SONO_CODE%TYPE,
  pRegNumber RECLAIM_LOG.Reg_Number%TYPE,
  pQueueId RECLAIM_LOG.QUEUE_ID%TYPE
)
AS
  p_reclaim_log_info_message_use CONFIGURATION.VALUE%TYPE;
begin
  begin
    select c.value into p_reclaim_log_info_message_use
    from configuration c
    where c.parameter = 'reclaim_log_info_message_use'
          and rownum = 1;
  exception when no_data_found then
    p_reclaim_log_info_message_use := 'Y';
  end;

  if p_reclaim_log_info_message_use is not null and p_reclaim_log_info_message_use = 'Y' then
     P$LOG(LOG_TYPE_INFORMATION, pTitle, pMessage, pZip, pSonoCode, pRegNumber, pQueueId);
  end if;
end;

PROCEDURE P$LOG_INFO_SHORT
(
  pTitle RECLAIM_LOG.TITLE%TYPE,
  pMessage RECLAIM_LOG.MESSAGE%TYPE
)
AS
begin
  P$LOG_INFORMATION(pTitle, pMessage, null, null, null, null);
end;

PROCEDURE P$LOG_ERROR_SHORT
(
  pTitle RECLAIM_LOG.TITLE%TYPE,
  pMessage RECLAIM_LOG.MESSAGE%TYPE
)
AS
begin
  P$LOG_ERROR(pTitle, pMessage, null, null, null, null);
end;

PROCEDURE P$GET_DISCREPANCIES_SIDE_ONE
(
  pQueueId ASK_RECLAIM_QUEUE.ID%TYPE,
  pCursor out SYS_REFCURSOR
)
AS
  VX_KNPClosed number;
begin

  -- проверка, не закрылась ли КНП
  select max(dk.doc_id) into VX_KNPClosed
  from doc_knp_closed dk
  join ask_reclaim_queue q on q.doc_id = dk.doc_id
  where q.id = pQueueId;

  -- берем все расхождения из АТ или АИ
  open pCursor for
  select *
  from 
  (
	  select
		 m.reg_number as RegNumber
		,m.sono_code as SonoCode
		,m.inn as Inn
		,m.kpp as Kpp
		,m.kpp_effective as KppEffective
		,m.zip as Zip
		,m.period_code                 as PeriodCode
		,m.fiscal_year                 as FiscalYear
		,m.discrepancy_id              as DiscrepancyId
		,m.type                        as Type
		,m.amnt                        as Amount
		,m.amount_pvp                  as AmountPvp
		,m.rule_num                    as RuleNum
		,m.seller_inn                  as SellerInn
		,m.buyer_inn                   as BuyerInn
		,m.buyer_kpp_effective         as BuyerKppEffective
		,m.seller_kpp_effective        as SellerKppEffective
		,sia.INVOICE_NUM                  as InvoiceNum
		,sia.INVOICE_DATE                 as InvoiceDate
		,sia.CORRECTION_NUM               as CorrectionNum
		,sia.CORRECTION_DATE              as CorrectionDate
		,sia.ROW_KEY                      as RowKey
		,sia.ACTUAL_ROW_KEY               as ActualRowKey
		,m.seller_inn                  as ContractorInn
		,m.seller_kpp_effective        as ContractorKppEffective
	  from
	  (
		select
			 rq.reg_number
			,rq.sono_code
			,rq.inn
			,rq.kpp
			,rq.kpp_effective
			,rq.zip
			,rq.period_code
			,rq.fiscal_year
			,rq.discrepancy_id
			,sov.type
			,sov.amnt
			,sov.amount_pvp
			,sov.rule_num
			,sov.seller_inn
			,sov.buyer_inn
			,sov.invoice_rk
			,ask_buyer.kpp_effective as buyer_kpp_effective
			,ask_seller.kpp_effective as seller_kpp_effective
		from
		(
		  select distinct
				  d1.doc_id
				 ,dh1.reg_number
				 ,dh1.sono_code_submited as sono_code
				 ,dh1.inn_contractor as inn
				 ,dh1.kpp
				 ,dh1.kpp_effective
				 ,dh1.zip
				 ,dh1.period_code
				 ,dh1.fiscal_year
				 ,sdis1.discrepancy_id
		  from ask_reclaim_queue rq
		  join doc d1 on d1.doc_id = rq.doc_id
		  join doc_discrepancy sdis1 on sdis1.doc_id = d1.doc_id
		  -- ограничение по Инсепкциям для ОЭ 8.10
		  join RECLAIM_SONO_LIMIT m on m.sono_code = d1.sono_code
		  join nds2_seod.seod_declaration seod1 on seod1.decl_reg_num = d1.ref_entity_id
									 and seod1.sono_code = d1.sono_code
		  join ask_declandjrnl ask1 on seod1.tax_period = ask1.period
							   and seod1.fiscal_year = ask1.otchetgod
							   and seod1.inn = ask1.innnp
							   and seod1.correction_number = ask1.nomkorr
							   and seod1.id_file = ask1.idfajl
							   and seod1.sono_code = ask1.kodno
		  join declaration_history dh1 on dh1.inn_contractor =
								   (case when ask1.innreorg is null then ask1.innnp else ask1.innreorg end)
									 and dh1.period_code = ask1.period
									 and dh1.fiscal_year = ask1.otchetgod
									 and dh1.type_code = ask1.type
									 and dh1.kpp_effective = ask1.kpp_effective
									 and dh1.is_active = 1
		  where VX_KNPClosed is null -- КНП не закрыта
		  and rq.id = pQueueId       -- ( id элемента очереди истребований )
		  and not exists             -- очищатор в пределах одного элемента очереди
		  (
			 select 1 from doc_discrepancy dise
			 join doc de on de.doc_id = dise.doc_id and de.queue_item_id = pQueueId
			 where dise.discrepancy_id = sdis1.discrepancy_id and de.inn = dh1.inn_contractor
				   and de.doc_type = 2
		  )
		) rq
		join sov_discrepancy sov on rq.discrepancy_id = sov.id and rq.inn = sov.buyer_inn
		left join ask_declandjrnl ask_buyer on ask_buyer.zip = sov.buyer_zip
		left join ask_declandjrnl ask_seller on ask_seller.zip = sov.seller_zip
		where sov.status = 1  -- ( 1 - расхождение открыто )
			  and sov.type in (DISCREPANCY_TYPE_GAP, DISCREPANCY_TYPE_NDS) -- для покупателя только расхождения вида ("разрыв" и "проверка НДС")
	  ) m
	  join sov_invoice_all sia on sia.row_key = m.invoice_rk and sia.discrepancy_id = m.discrepancy_id
	  union all
		select
		 m.reg_number as RegNumber
		,m.sono_code as SonoCode
		,m.inn as Inn
		,m.kpp as Kpp
		,m.kpp_effective as KppEffective
		,m.zip as Zip
		,m.period_code                 as PeriodCode
		,m.fiscal_year                 as FiscalYear
		,m.discrepancy_id              as DiscrepancyId
		,m.type                        as Type
		,m.amnt                        as Amount
		,m.amount_pvp                  as AmountPvp
		,m.rule_num                    as RuleNum
		,m.seller_inn                  as SellerInn
		,m.buyer_inn                   as BuyerInn
		,m.buyer_kpp_effective         as BuyerKppEffective
		,m.seller_kpp_effective        as SellerKppEffective
		,sia.INVOICE_NUM                  as InvoiceNum
		,sia.INVOICE_DATE                 as InvoiceDate
		,sia.CORRECTION_NUM               as CorrectionNum
		,sia.CORRECTION_DATE              as CorrectionDate
		,sia.ROW_KEY                      as RowKey
		,sia.ACTUAL_ROW_KEY               as ActualRowKey
		,m.seller_inn                  as ContractorInn
		,m.seller_kpp_effective        as ContractorKppEffective
	  from
	  (
		select
			 rq.reg_number
			,rq.sono_code
			,rq.inn
			,rq.kpp
			,rq.kpp_effective
			,rq.zip
			,rq.period_code
			,rq.fiscal_year
			,rq.discrepancy_id
			,sov.type
			,sov.amnt
			,sov.amount_pvp
			,sov.rule_num
			,sov.seller_inn
			,sov.buyer_inn
			,sov.invoice_rk
			,ask_buyer.kpp_effective as buyer_kpp_effective
			,ask_seller.kpp_effective as seller_kpp_effective
		from
		(
		  select distinct
				  dh.reg_number
				 ,dh.sono_code_submited as sono_code
				 ,dh.inn_contractor as inn
				 ,dh.kpp
				 ,dh.kpp_effective
				 ,dh.zip
				 ,dh.period_code
				 ,dh.fiscal_year
				 ,sdis1.discrepancy_id
		  from 
			(select doc_id from nds2_mrr_user.ask_reclaim_queue where id = pQueueId) q
			join nds2_mrr_user.doc d on d.doc_id = q.doc_id and d.close_reason = 5
			join nds2_mrr_user.declaration_knp_status dks on dks.zip = d.zip and dks.calc_is_open = 1
			join doc_discrepancy sdis1 on sdis1.doc_id = d.doc_id
			-- ограничение по Инсепкциям для ОЭ 8.10
			join RECLAIM_SONO_LIMIT m on m.sono_code = d.sono_code
			join nds2_seod.seod_declaration seod1 on seod1.decl_reg_num = d.ref_entity_id
												 and seod1.sono_code = d.sono_code
			join ask_declandjrnl ask1 on seod1.tax_period = ask1.period
							   and seod1.fiscal_year = ask1.otchetgod
							   and seod1.inn = ask1.innnp
							   and seod1.correction_number = ask1.nomkorr
							   and seod1.id_file = ask1.idfajl
							   and seod1.sono_code = ask1.kodno
			join nds2_mrr_user.declaration_history dh on dh.inn_contractor = d.inn 
										 and dh.kpp_effective = d.kpp_effective 
										 and dh.fiscal_year = d.fiscal_year 
										 and dh.period_effective = d.quarter 
										 and dh.is_active = 1
										 and dh.type_code = ask1.type
		) rq
		join sov_discrepancy sov on rq.discrepancy_id = sov.id and rq.inn = sov.buyer_inn
		left join ask_declandjrnl ask_buyer on ask_buyer.zip = sov.buyer_zip
		left join ask_declandjrnl ask_seller on ask_seller.zip = sov.seller_zip
		where sov.status = 1  -- ( 1 - расхождение открыто )
			  and sov.type in (DISCREPANCY_TYPE_GAP, DISCREPANCY_TYPE_NDS) -- для покупателя только расхождения вида ("разрыв" и "проверка НДС")
	  ) m
	  join sov_invoice_all sia on sia.row_key = m.invoice_rk and sia.discrepancy_id = m.discrepancy_id
  ) d
  order by d.inn, d.KppEffective, d.ContractorInn, d.SellerKppEffective, d.ActualRowKey, d.ROWKEY;

end;

PROCEDURE P$GET_DISCREPANCIES_SIDE_TWO
(
  pQueueId ASK_RECLAIM_QUEUE.ID%TYPE,
  pCursor out SYS_REFCURSOR
)
AS
  VX_KNPClosed number;
begin

  -- проверка, не закрылась ли КНП
  select max(dk.doc_id) into VX_KNPClosed
  from doc_knp_closed dk
  join ask_reclaim_queue q on q.doc_id = dk.doc_id
  where q.id = pQueueId;

  -- берем все расхождения из АТ
  open pCursor for
  select *
  from 
  (
	  select
		 m.reg_number as RegNumber
		,m.sono_code as SonoCode
		,m.inn as Inn
		,m.kpp as Kpp
		,m.kpp_effective as KppEffective
		,m.zip as Zip
		,m.period_code                 as PeriodCode
		,m.fiscal_year                 as FiscalYear
		,m.discrepancy_id              as DiscrepancyId
		,m.type                        as Type
		,m.amnt                        as Amount
		,m.amount_pvp                  as AmountPvp
		,m.rule_num                    as RuleNum
		,m.seller_inn                  as SellerInn
		,m.buyer_inn                   as BuyerInn
		,m.buyer_kpp_effective         as BuyerKppEffective
		,m.seller_kpp_effective        as SellerKppEffective
		,sia.INVOICE_NUM                  as InvoiceNum
		,sia.INVOICE_DATE                 as InvoiceDate
		,sia.CORRECTION_NUM               as CorrectionNum
		,sia.CORRECTION_DATE              as CorrectionDate
		,sia.ROW_KEY                      as RowKey
		,sia.ACTUAL_ROW_KEY               as ActualRowKey
		,m.buyer_inn                  as ContractorInn
		,m.buyer_kpp_effective        as ContractorKppEffective
	  from
	  (
		select
             rq.reg_number
			,rq.sono_code
			,rq.inn
			,rq.kpp
			,rq.kpp_effective
			,rq.zip
			,rq.period_code
			,rq.fiscal_year
			,rq.discrepancy_id
			,sov.type
			,sov.amnt
			,sov.amount_pvp
			,sov.rule_num
			,sov.seller_inn
			,sov.buyer_inn
			,sov.invoice_contractor_rk as invoice_rk
			,ask_buyer.kpp_effective as buyer_kpp_effective
			,ask_seller.kpp_effective as seller_kpp_effective
		from
		(
		  select distinct
				  dh1.reg_number
				 ,dh1.sono_code_submited as sono_code
				 ,dh1.inn_contractor as inn
				 ,dh1.kpp
				 ,dh1.kpp_effective
				 ,dh1.zip
				 ,dh1.period_code
				 ,dh1.fiscal_year
				 ,sdis1.discrepancy_id
		  from ask_reclaim_queue rq
		  join doc d1 on d1.doc_id = rq.doc_id
		  join doc_discrepancy sdis1 on sdis1.doc_id = d1.doc_id
		  -- ограничение по Инсепкциям для ОЭ 8.10
		  join RECLAIM_SONO_LIMIT m on m.sono_code = d1.sono_code
		  join nds2_seod.seod_declaration seod1 on seod1.decl_reg_num = d1.ref_entity_id
									 and seod1.sono_code = d1.sono_code
		  join ask_declandjrnl ask1 on seod1.tax_period = ask1.period
							   and seod1.fiscal_year = ask1.otchetgod
							   and seod1.inn = ask1.innnp
							   and seod1.correction_number = ask1.nomkorr
							   and seod1.id_file = ask1.idfajl
							   and seod1.sono_code = ask1.kodno
		  join declaration_history dh1 on dh1.inn_contractor =
								   (case when ask1.innreorg is null then ask1.innnp else ask1.innreorg end)
									 and dh1.period_code = ask1.period
									 and dh1.fiscal_year = ask1.otchetgod
									 and dh1.type_code = ask1.type
									 and dh1.kpp_effective = ask1.kpp_effective
									 and dh1.is_active = 1
		  where VX_KNPClosed is null -- КНП не закрыта
		  and rq.id = pQueueId       -- ( id элемента очереди истребований )
		  and not exists             -- очищатор в пределах одного элемента очереди
		  (
			 select 1 from doc_discrepancy dise
			 join doc de on de.doc_id = dise.doc_id and de.queue_item_id = pQueueId
			 where dise.discrepancy_id = sdis1.discrepancy_id and de.inn = dh1.inn_contractor
				   and de.doc_type = 2
		  )
		) rq
		join sov_discrepancy sov on rq.discrepancy_id = sov.id and rq.inn = sov.seller_inn
		left join ask_declandjrnl ask_buyer on ask_buyer.zip = sov.buyer_zip
		left join ask_declandjrnl ask_seller on ask_seller.zip = sov.seller_zip
		where sov.status = 1  -- ( 1 - расхождение открыто )
			  and sov.type = DISCREPANCY_TYPE_NDS -- для продавца только расхождения вида ("проверка НДС")
	  ) m
	  join sov_invoice_all sia on sia.row_key = m.invoice_rk and sia.discrepancy_id = m.discrepancy_id
	  union all
	  select
		 m.reg_number as RegNumber
		,m.sono_code as SonoCode
		,m.inn as Inn
		,m.kpp as Kpp
		,m.kpp_effective as KppEffective
		,m.zip as Zip
		,m.period_code                 as PeriodCode
		,m.fiscal_year                 as FiscalYear
		,m.discrepancy_id              as DiscrepancyId
		,m.type                        as Type
		,m.amnt                        as Amount
		,m.amount_pvp                  as AmountPvp
		,m.rule_num                    as RuleNum
		,m.seller_inn                  as SellerInn
		,m.buyer_inn                   as BuyerInn
		,m.buyer_kpp_effective         as BuyerKppEffective
		,m.seller_kpp_effective        as SellerKppEffective
		,sia.INVOICE_NUM                  as InvoiceNum
		,sia.INVOICE_DATE                 as InvoiceDate
		,sia.CORRECTION_NUM               as CorrectionNum
		,sia.CORRECTION_DATE              as CorrectionDate
		,sia.ROW_KEY                      as RowKey
		,sia.ACTUAL_ROW_KEY               as ActualRowKey
		,m.buyer_inn                  as ContractorInn
		,m.buyer_kpp_effective        as ContractorKppEffective
	  from
	  (
		select
             rq.reg_number
			,rq.sono_code
			,rq.inn
			,rq.kpp
			,rq.kpp_effective
			,rq.zip
			,rq.period_code
			,rq.fiscal_year
			,rq.discrepancy_id
			,sov.type
			,sov.amnt
			,sov.amount_pvp
			,sov.rule_num
			,sov.seller_inn
			,sov.buyer_inn
			,sov.invoice_contractor_rk as invoice_rk
			,ask_buyer.kpp_effective as buyer_kpp_effective
			,ask_seller.kpp_effective as seller_kpp_effective
		from
		(
		  select distinct
				  dh.reg_number
				 ,dh.sono_code_submited as sono_code
				 ,dh.inn_contractor as inn
				 ,dh.kpp
				 ,dh.kpp_effective
				 ,dh.zip
				 ,dh.period_code
				 ,dh.fiscal_year
				 ,sdis1.discrepancy_id
		  from 
		  (select doc_id from nds2_mrr_user.ask_reclaim_queue where id = pQueueId) q
		  join nds2_mrr_user.doc d on d.doc_id = q.doc_id and d.close_reason = 5
		  join nds2_mrr_user.declaration_knp_status dks on dks.zip = d.zip and dks.calc_is_open = 1
		  join doc_discrepancy sdis1 on sdis1.doc_id = d.doc_id
		  -- ограничение по Инсепкциям для ОЭ 8.10
		  join RECLAIM_SONO_LIMIT m on m.sono_code = d.sono_code
		  join nds2_seod.seod_declaration seod1 on seod1.decl_reg_num = d.ref_entity_id
									 and seod1.sono_code = d.sono_code
		  join ask_declandjrnl ask1 on seod1.tax_period = ask1.period
							   and seod1.fiscal_year = ask1.otchetgod
							   and seod1.inn = ask1.innnp
							   and seod1.correction_number = ask1.nomkorr
							   and seod1.id_file = ask1.idfajl
							   and seod1.sono_code = ask1.kodno
		  join nds2_mrr_user.declaration_history dh on 
		                                     dh.inn_contractor = (case when ask1.innreorg is null then ask1.innnp else ask1.innreorg end) 
										 and dh.kpp_effective = d.kpp_effective 
										 and dh.fiscal_year = d.fiscal_year 
										 and dh.period_effective = d.quarter 
										 and dh.is_active = 1
										 and dh.type_code = ask1.type
		) rq
		join sov_discrepancy sov on rq.discrepancy_id = sov.id and rq.inn = sov.seller_inn
		left join ask_declandjrnl ask_buyer on ask_buyer.zip = sov.buyer_zip
		left join ask_declandjrnl ask_seller on ask_seller.zip = sov.seller_zip
		where sov.status = 1  -- ( 1 - расхождение открыто )
			  and sov.type = DISCREPANCY_TYPE_NDS -- для продавца только расхождения вида ("проверка НДС")
	  ) m
	  join sov_invoice_all sia on sia.row_key = m.invoice_rk and sia.discrepancy_id = m.discrepancy_id
  ) d
  order by d.inn, d.KppEffective, d.BuyerInn, d.BuyerKppEffective, d.ActualRowKey, d.ROWKEY;

end;

PROCEDURE P$INSERT_DISCREPANCY
(
  VP_DocId DOC.DOC_ID%TYPE,
  VP_DiscrepancyId DOC_DISCREPANCY.DISCREPANCY_ID%TYPE,
  VP_RowKey DOC_DISCREPANCY.ROW_KEY%TYPE,
  VP_Amount DOC_DISCREPANCY.AMOUNT%TYPE,
  VP_AmountPvp DOC_DISCREPANCY.AMOUNT_PVP%TYPE
)
AS
begin

  insert into doc_discrepancy (doc_id, discrepancy_id, row_key, amount, amount_pvp)
  values(VP_DocId, VP_DiscrepancyId, VP_RowKey, VP_Amount, VP_AmountPvp);

end;

PROCEDURE P$INSERT_INVOICES
(
  pDocId DOC.DOC_ID%TYPE
)
AS
begin

  for invoice_item in
  (
    select
       sia.declaration_version_id as declaration_version_id
      ,sia.CHAPTER as invoice_chapter
      ,sia.row_key as invoice_row_key
      ,pDocId as doc_id
      ,sia.ORDINAL_NUMBER
      ,sia.OKV_CODE
      ,sia.CREATE_DATE
      ,sia.RECEIVE_DATE
      ,sia.INVOICE_NUM
      ,sia.INVOICE_DATE
      ,sia.CHANGE_NUM
      ,sia.CHANGE_DATE
      ,sia.CORRECTION_NUM
      ,sia.CORRECTION_DATE
      ,sia.CHANGE_CORRECTION_NUM
      ,sia.CHANGE_CORRECTION_DATE
      ,sia.SELLER_INVOICE_NUM
      ,sia.SELLER_INVOICE_DATE
      ,sia.BROKER_INN
      ,sia.BROKER_KPP
      ,sia.DEAL_KIND_CODE
      ,sia.CUSTOMS_DECLARATION_NUM
      ,sia.PRICE_BUY_AMOUNT
      ,sia.PRICE_BUY_NDS_AMOUNT
      ,sia.PRICE_SELL
      ,sia.PRICE_SELL_IN_CURR
      ,sia.PRICE_SELL_18
      ,sia.PRICE_SELL_10
      ,sia.PRICE_SELL_0
      ,sia.PRICE_NDS_18
      ,sia.PRICE_NDS_10
      ,sia.PRICE_TAX_FREE
      ,sia.PRICE_TOTAL
      ,sia.PRICE_NDS_TOTAL
      ,sia.DIFF_CORRECT_DECREASE
      ,sia.DIFF_CORRECT_INCREASE
      ,sia.DIFF_CORRECT_NDS_DECREASE
      ,sia.DIFF_CORRECT_NDS_INCREASE
      ,sia.PRICE_NDS_BUYER
      ,sia.ACTUAL_ROW_KEY
      ,sia.COMPARE_ROW_KEY
      ,sia.COMPARE_ALGO_ID
      ,sia.FORMAT_ERRORS
      ,sia.LOGICAL_ERRORS
      ,sia.SELLER_AGENCY_INFO_INN
      ,sia.SELLER_AGENCY_INFO_KPP
      ,sia.SELLER_AGENCY_INFO_NAME
      ,sia.SELLER_AGENCY_INFO_NUM
      ,sia.SELLER_AGENCY_INFO_DATE
      ,sia.IS_IMPORT
      ,sia.CLARIFICATION_KEY
      ,sia.CONTRAGENT_KEY
      ,sia.IS_DOP_LIST
      ,base.period_code as decl_tax_period
      ,base.fiscal_year as decl_fiscal_year
    from
    (
      select
           sdis.row_key as row_key
          ,ask.period as period_code
          ,ask.otchetgod as fiscal_year
		  ,sdis.discrepancy_id
      from doc_discrepancy sdis
      join sov_discrepancy sov on sov.id = sdis.discrepancy_id
      join doc d on d.doc_id = sdis.doc_id
      join nds2_seod.seod_declaration seod on seod.sono_code = d.sono_code and seod.decl_reg_num = d.ref_entity_id
      join ask_declandjrnl ask on seod.tax_period = ask.period
                           and seod.fiscal_year = ask.otchetgod
                           and seod.inn = ask.innnp
                           and seod.correction_number = ask.nomkorr
                           and seod.id_file = ask.idfajl
      where sdis.doc_id = pDocId
    ) base
    join sov_invoice_all sia on sia.row_key = base.row_key and sia.discrepancy_id = base.discrepancy_id
  )
  loop

    PAC$DOCUMENT.FILL_DOC_INVOICE(invoice_item);

  end loop;

end;

PROCEDURE P$INSERT_DOC
(
  pRegNumber DOC.REF_ENTITY_ID%TYPE,
  pDocType DOC.DOC_TYPE%TYPE,
  pDocKind DOC.DOC_KIND%TYPE,
  pSonoCode DOC.SONO_CODE%TYPE,
  pDiscrepancyCount DOC.Discrepancy_Count%TYPE,
  pDiscrepancyAmount DOC.Discrepancy_Amount%TYPE,
  pProcessed DOC.Processed%TYPE,
  pInn DOC.Inn%TYPE,
  pKppEffective DOC.Kpp_Effective%TYPE,
  pQueueId ASK_RECLAIM_QUEUE.ID%TYPE,
  pZip DOC.ZIP%TYPE,
  pQuarter DOC.QUARTER%TYPE,
  pFiscalYear DOC.FISCAL_YEAR%TYPE,
  pDocId out DOC.DOC_ID%TYPE
)
AS
p_status_date date;
p_status number;
begin

  pDocId := SEQ_DOC.NEXTVAL;

  p_status_date := sysdate;
  p_status := DOC_STATUS_PREPARE_DATA;

  insert into doc
  (
    doc_id,
    external_doc_num,
    external_doc_date,
    ref_entity_id,
    doc_type,
    doc_kind,
    sono_code,
    discrepancy_count,
    discrepancy_amount,
    create_date,
    seod_accepted,
    seod_accept_date,
    tax_payer_send_date,
    tax_payer_delivery_date,
    tax_payer_delivery_method,
    tax_payer_reply_date,
    close_date,
    status_date,
    status,
    stage,
    document_body,
    user_comment,
    deadline_date,
    prolong_answer_date,
    process_elapsed,
    processed,
    inn,
    kpp_effective,
    close_reason,
    zip,
    quarter,
    fiscal_year,
    queue_item_id
  )
  values
  (
    pDocId,                        -- doc_id
    null,                          -- external_doc_num
    null,                          -- external_doc_date
    pRegNumber,                    -- ref_entity_id
    pDocType,                      -- doc_type
    pDocKind,                      -- doc_kind
    pSonoCode,                     -- sono_code
    pDiscrepancyCount,             -- discrepancy_count
    pDiscrepancyAmount,            -- discrepancy_amount
    sysdate,                       -- create_date
    null,                          -- seod_accepted
    null,                          -- seod_accept_date
    null,                          -- tax_payer_send_date
    null,                          -- tax_payer_delivery_date
    null,                          -- tax_payer_delivery_method
    null,                          -- tax_payer_reply_date
    null,                          -- close_date
    trunc(p_status_date),          -- status_date
    p_status,                      -- status
    null,                          -- stage
    null,                          -- document_body
    null,                          -- user_comment
    null,                          -- deadline_date
    null,                          -- prolong_answer_date
    null,                          -- process_elapsed
    pProcessed,                    -- processed
    pInn,                          -- inn
    pKppEffective,                 -- kpp_effective
    null,                          -- close_reason
    pZip,                          -- zip
    pQuarter,                      -- quarter
    pFiscalYear,                   -- fiscal_year
    pQueueId                       -- queue_item_id
  );

  insert into doc_status_history(doc_id, status, status_date)
  values (pDocId, p_status, p_status_date);

end;

PROCEDURE P$UPDATE_DOC
(
  VP_DocId DOC.DOC_ID%TYPE,
  VP_DiscrepancyCount DOC.Discrepancy_Count%TYPE,
  VP_DiscrepancyAmount DOC.Discrepancy_Amount%TYPE,
  VP_DiscrepancyAmountPvp DOC.Discrepancy_Amount_Pvp%TYPE,
  VP_HasSchemaValidationErrors NUMBER,
  VP_HasDatabaseError NUMBER,
  VP_HasGeneralError NUMBER,
  VP_DocumentBody CLOB,
  VP_Processed DOC.Processed%TYPE
)
AS
  p_status_date date;
  p_status number;
  p_reclaim_manual_check_doc_use CONFIGURATION.VALUE%TYPE;
begin

  if VP_HasSchemaValidationErrors = 1 or
     VP_HasDatabaseError = 1 or
     VP_HasGeneralError = 1 then
    p_status := DOC_STATUS_ERROR;
  else
    p_status := DOC_STATUS_CREATED;

    begin
      select c.value into p_reclaim_manual_check_doc_use
      from configuration c
      where c.parameter = 'reclaim_manual_check_doc_use'
            and rownum = 1;
    exception when no_data_found then
      p_reclaim_manual_check_doc_use := 'Y';
    end;

    if p_reclaim_manual_check_doc_use is not null and p_reclaim_manual_check_doc_use = 'Y' then
      p_status := DOC_STATUS_MANUAL_CHECK;
    end if;
  end if;

  p_status_date := sysdate;

  update doc d set
    discrepancy_count = VP_DiscrepancyCount,
    discrepancy_amount = VP_DiscrepancyAmount,
    discrepancy_amount_pvp = VP_DiscrepancyAmountPvp,
    status_date = trunc(p_status_date),
    status = p_status,
    document_body = VP_DocumentBody,
    processed = VP_Processed
  where d.doc_id = VP_DocId;

  insert into doc_status_history(doc_id, status, status_date)
  values (VP_DocId, p_status, p_status_date);

end;

PROCEDURE P$PULL_QUEUE
(
  pQueueId out ASK_RECLAIM_QUEUE.ID%TYPE,
  pCursor out SYS_REFCURSOR
)
AS
pragma autonomous_transaction;
   p_queue_id ASK_RECLAIM_QUEUE.ID%TYPE;
begin
  p_queue_id := null;

  begin
    select id into p_queue_id
    from ASK_RECLAIM_QUEUE q
    where q.is_locked = 0
    AND q.id IN
    (
      select m.id
      from
      (
        select q.id, rownum as rn
        from ASK_RECLAIM_QUEUE q
        where q.is_locked = 0
        order by q.id
      ) m
      where m.rn = 1
    )
    for update;

    update ASK_RECLAIM_QUEUE q set q.is_locked = 1
    where q.id = p_queue_id;
    commit;
  exception when no_data_found then
    rollback;
  end;

  pQueueId := p_queue_id;

  open pCursor for
  select
       q.id                     as Id
      ,d.zip                    as Zip
      ,d.ref_entity_id          as RegNumber
      ,d.sono_code              as SonoCode
      ,q.side                   as Side
      ,q.state                  as State
      ,q.is_locked              as IsLocked
      ,q.create_date            as CreateDate
  from ASK_RECLAIM_QUEUE q
  join doc d on d.doc_id = q.doc_id
  where q.id = p_queue_id;

exception when others then
  rollback;
  P$LOG_ERROR('P$PULL_QUEUE', substr(sqlerrm, 256), null, null, null, pQueueId);
end;

PROCEDURE P$EXISTS_IN_QUEUE
(
  pExists out NUMBER
)
AS
begin
  pExists := 0;

  begin
    select 1 into pExists from dual
    where exists(select q.id from ASK_RECLAIM_QUEUE q where q.state = 1);
  exception when no_data_found then
    pExists := 0;
  end;

exception when others then
  rollback;
  P$LOG_ERROR('P$EXISTS_IN_QUEUE', substr(sqlerrm, 256), null, null, null, null);
end;

PROCEDURE P$UPDATE_QUEUE
(
  pQueueId in ASK_RECLAIM_QUEUE.ID%TYPE,
  pState in ASK_RECLAIM_QUEUE.STATE%TYPE,
  pIsLocked in ASK_RECLAIM_QUEUE.IS_LOCKED%TYPE
)
AS
pragma autonomous_transaction;
begin

  update ASK_RECLAIM_QUEUE q set
         q.state = pState,
         q.is_locked = pIsLocked,
         q.update_date = sysdate
  where q.id = pQueueId;
  commit;

exception when others then
  rollback;
  P$LOG_ERROR('P$UPDATE_QUEUE', substr(sqlerrm, 256), null, null, null, pQueueId);
end;

PROCEDURE P$GET_STATE_SEOD_KNP
(
  pSonoCode in VARCHAR2,
  pRegNumber in NUMBER,
  pStateKnp out NUMBER
)
AS
p_completion_date date;
begin

  pStateKnp := 1;

  p_completion_date := null;

  select max(k.completion_date) into p_completion_date from NDS2_SEOD.SEOD_KNP k
  where k.ifns_code = pSonoCode and k.declaration_reg_num = pRegNumber;

  if p_completion_date is not null then
    pStateKnp := 0;
  end if;

exception when others then
  rollback;
  P$LOG_ERROR('P$GET_SEOD_KNP', substr(sqlerrm, 256), null, null, null, null);
end;

PROCEDURE P$CLOSE_RECLAIMS( p_doc_ids T$TABLE_OF_NUMBER,
                            p_close_date date,
                            p_status_closed number,
                            p_reason_closed number)
as
begin
    --Обновляем статус и причину закрытия АИ
    FORALL indx IN 1 .. p_doc_ids.COUNT
        update doc
        set
           close_date = p_close_date,
           status = p_status_closed,
		   status_date = trunc(p_close_date), 
           close_reason = p_reason_closed
        where doc_id = p_doc_ids(indx);

    --Добавляем записи в историю и перечень закрытых АИ
    FORALL indx IN 1 .. p_doc_ids.COUNT
      insert all
        into doc_status_history(doc_id, status, status_date)
            values (p_doc_ids(indx), p_status_closed, p_close_date)
        into reclaim_closed(doc_id)
            values (p_doc_ids(indx))
        select 1 from dual;
end;

/* Закрывает АИ в случае подачи новой корректировки*/
PROCEDURE P$CLOSE_BY_NEW_CORR
as
    p_status_closed constant number := 10;
    p_reason_closed constant number := 2;
    p_close_date date := sysdate;
    last_exec_param_name constant varchar2(35):='ai_close_by_new_corr_exec_date';
    dt_format constant varchar2(25) := 'DD-MM-YYYY HH24:MI:SS';

    p_doc_ids T$TABLE_OF_NUMBER;
    v_last_exec_dt date;
    v_items_limit number;

begin
    --Инициализируем переменные
    select
        to_date(value, dt_format )
    into v_last_exec_dt
    from configuration
    where parameter = last_exec_param_name;

    select
        cast(value as number )
    into v_items_limit
    from configuration
    where parameter = LIMIT_AUTOCLOSE_PARAM_NAME ;

    --Кладем в коллекцию все АИ по которым есть новые корректировки
    loop
        with
            new_corr_decl as
            (
                select distinct
                    inn_declarant,
                    kpp_effective,
                    inn_contractor,
                    period_code,
                    fiscal_year,
                    type_code
                from declaration_history
                where
                    insert_date between v_last_exec_dt and p_close_date
                    and is_active = 1
            )
        select distinct
            d.doc_id
        bulk collect into p_doc_ids
        from new_corr_decl ncd
        join declaration_history dh
            on  ncd.inn_declarant = dh.inn_declarant
                and ncd.kpp_effective = dh.kpp_effective
                and ncd.inn_contractor= dh.inn_contractor
                and ncd.period_code = dh.period_code
                and ncd.fiscal_year = dh.fiscal_year
                and ncd.type_code = dh.type_code
        join doc d
            on dh.zip = d.zip
        where
            d.status not in (3,9,10)    -- ошибка отправки/ошибка передачи/закрыто
            and d.doc_type = 2          --АИ
            and rownum <= v_items_limit;

        if(p_doc_ids.count = 0) then
            exit;
        end if;

        P$CLOSE_RECLAIMS(p_doc_ids, p_close_date, p_status_closed, p_reason_closed);
    end loop;

    --Обновляем дату запуска
    update nds2_mrr_user.configuration
    set value = TO_CHAR(p_close_date, dt_format)
    where parameter = last_exec_param_name;

    exception when others then
        P$LOG_ERROR('P$CLOSE_DOC_BY_NEW_CORR', substr(sqlerrm, 256), null, null, null, null);
        raise;
end;

PROCEDURE P$CLOSE_ALL_INACTIVE
as
  p_status_closed number := 10;
  p_reason_closed number := 2;
  p_close_date date := sysdate;
  p_doc_ids T$TABLE_OF_NUMBER;
  v_items_limit number;
begin
    select
        cast(value as number )
    into v_items_limit
    from configuration
    where parameter = LIMIT_AUTOCLOSE_PARAM_NAME ;

  LOOP
      select d.doc_id
      bulk collect into p_doc_ids
      from doc d
      join declaration_history dh
        on dh.zip = d.zip
        and dh.is_active = 0
      where
          d.doc_type = 2
          and d.status not in (3,9,10)
          and rownum <= v_items_limit;

    if p_doc_ids.count = 0 then
       EXIT;
    end if;

    P$CLOSE_RECLAIMS(p_doc_ids, p_close_date, p_status_closed, p_reason_closed);
  END LOOP;

  exception when others then
     P$LOG_ERROR('P$CLOSE_ALL_INACTIVE', substr(sqlerrm, 256), null, null, null, null);
     raise;
end;

PROCEDURE P$AUTO_CLOSE
as
	v_error_msg varchar2(200);
	v_error_source constant varchar2(30):='PAC$RECLAIM.P$AUTO_CLOSE';
begin

--1. Закрытие АИ при подаче корректировки в новый НО
	begin
		P$CLOSE_BY_NEW_CORR;

		exception when others then
			v_error_msg:=substr(sqlerrm,1,200);
			P$LOG_ERROR_SHORT(
				pTitle => v_error_source,
				pMessage => 'Ошибка автозакрытия АИ при подаче корректировки в новый НО ' ||v_error_msg
			);
	end;

--2. Закрытие АИ по неактуальным корректировкам
	begin
		P$CLOSE_ALL_INACTIVE;

		exception when others then
			v_error_msg:=substr(sqlerrm,1,200);
			P$LOG_ERROR_SHORT(
				pTitle => v_error_source,
				pMessage => 'Ошибка автозакрытия АИ по неактуальным корректировкам ' ||v_error_msg
			);
	end;

	if(v_error_msg is not null) then
		raise_application_error(-20100,v_error_source||' '||v_error_msg);
	end if;
end;

PROCEDURE P$RECLAIM_QUEUE_ADD
(
   pQueueItem IN T$RECLAIM_QUEUE_ITEM
)
as
begin
   insert into ASK_RECLAIM_QUEUE
   (
      id,
      doc_id,
      side,
      state,
      is_locked,
      create_date,
      update_date
   )
   values
   (
      pQueueItem.id,
      pQueueItem.doc_id,
      pQueueItem.side,
      1,
      0,
      sysdate,
      sysdate
   );
end;

PROCEDURE P$CLOSE_CLAIM
(
   pDoc IN T$RECLAIM_DOC
)
as
   pQueueItem T$RECLAIM_QUEUE_ITEM;
begin

   if pDoc.doc_type = 1 then
     -- АИ
     pQueueItem.id := SEQ_ASK_RECLAIM_QUEUE.Nextval;
     pQueueItem.doc_id := pDoc.doc_id;
     pQueueItem.side := pDoc.doc_kind;
	 pQueueItem.close_reason := pDoc.close_reason;
     P$RECLAIM_QUEUE_ADD(pQueueItem);
   end if;

end;

PROCEDURE P$ADD_QUEUE_ALL
AS
   pFromDate DATE;
   pToDate DATE;
   pDocs T$RECLAIM_DOCS;
   pCountCommit NUMBER;
begin
   pToDate := sysdate;

   select max(a.queue_add_last_date) into pFromDate from RECLAIM_QUEUE_CONFIG a;

   if pFromDate is null then
      pFromDate := TO_DATE('01.01.2014 00:00:01', 'dd.mm.yyyy hh24:mi:ss');
   end if;

   SELECT T$RECLAIM_DOC(m.doc_id, m.doc_type, m.doc_kind, m.close_reason) BULK COLLECT INTO pDocs
   FROM
   (
      select d.doc_id, d.doc_type, d.doc_kind, d.close_reason
      from doc d
      left join doc_knp_closed dc on dc.doc_id = d.doc_id
      where d.status = 10
            and dc.doc_id is null
            and d.doc_type = 1
            and d.doc_kind in (1, 2, 3)
            and d.close_date > pFromDate
            and d.close_date < pToDate
   ) m;

   pCountCommit := 0;
   FOR indx IN 1 .. pDocs.COUNT LOOP
      if pDocs(indx).doc_type = 1 then
            P$CLOSE_CLAIM(pDocs(indx));
            pCountCommit := pCountCommit + 1;
      end if;

      if (MOD(pCountCommit, 1000)) = 0 then
         commit;
      end if;

   END LOOP;

   commit;

   delete from RECLAIM_QUEUE_CONFIG where id = 1;
   insert into RECLAIM_QUEUE_CONFIG (id, queue_add_last_date)
   values (1, pToDate);
   commit;


exception when others then
   P$LOG_ERROR('P$ADD_QUEUE_ALL', substr(sqlerrm, 256), null, null, null, null);
end;

PROCEDURE P$COLLECT_QUEUE_STATS
AS
begin

   dbms_stats.gather_table_stats('NDS2_MRR_USER','ASK_RECLAIM_QUEUE');
exception when others then
   P$LOG_ERROR('P$COLLECT_QUEUE_STATS', substr(sqlerrm, 256), null, null, null, null);
end;


PROCEDURE P$FILL_QUEUE
AS
begin

   P$ADD_QUEUE_ALL;

   P$COLLECT_QUEUE_STATS;

end;

END;
/
