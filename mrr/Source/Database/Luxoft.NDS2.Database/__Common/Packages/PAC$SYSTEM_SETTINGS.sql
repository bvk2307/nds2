﻿create or replace package NDS2_MRR_USER.PAC$SYSTEM_SETTINGS
as
  procedure P$LOAD(pCursor OUT SYS_REFCURSOR);

  procedure P$SAVE(pClaimResendingTimeout in NUMBER,
				  pResendingAttempts in NUMBER,
				  pClaimDeliveryTimeout in NUMBER,
				  pClaimExplainTimeout in NUMBER, 
				  pReclaimReplyTimeout in NUMBER,
				  pReplyEntryTimeout in NUMBER);
end;
/
create or replace package body NDS2_MRR_USER.PAC$SYSTEM_SETTINGS
as

 PROCEDURE P$LOAD(pCursor OUT SYS_REFCURSOR)
  as
  begin
  open pCursor for
  select 
  CLAIM_RESEND_TIMEOUT_SEC as ClaimResendingTimeout, 
  CLAIM_RESEND_ATTEMPT_QTY as ResendingAttempts,
  CLAIM_DELIVERY_TIMEOUT_SEC as ClaimDeliveryTimeout,
  CLAIM_EXPLAIN_TIMEOUT_SEC as ClaimExplainTimeout,
  RECLAIM_REPLY_TIMEOUT_SEC as ReclaimReplyTimeout,
  REPLY_ENTRY_TIMEOUT_SEC as ReplyEntryTimeout
  from SYSTEM_SETTINGS;
 end;

 PROCEDURE P$SAVE(pClaimResendingTimeout in NUMBER,
				  pResendingAttempts in NUMBER,
				  pClaimDeliveryTimeout in NUMBER,
				  pClaimExplainTimeout in NUMBER, 
				  pReclaimReplyTimeout in NUMBER,
				  pReplyEntryTimeout in NUMBER)
  as
  begin
  update SYSTEM_SETTINGS
  set 
  CLAIM_RESEND_TIMEOUT_SEC = pClaimResendingTimeout, 
  CLAIM_RESEND_ATTEMPT_QTY = pResendingAttempts,
  CLAIM_DELIVERY_TIMEOUT_SEC = pClaimDeliveryTimeout,
  CLAIM_EXPLAIN_TIMEOUT_SEC = pClaimExplainTimeout,
  RECLAIM_REPLY_TIMEOUT_SEC = pReclaimReplyTimeout,
  REPLY_ENTRY_TIMEOUT_SEC = pReplyEntryTimeout;
 end;

end;
/