﻿create or replace 
PACKAGE                             NDS2_MRR_USER.PAC$HRZ_REPORT
as

procedure FIND_TXPR_BY_DISCREPANCY
(
 p_sono_code varchar2,
 p_year varchar2,
 p_qtr varchar2,
 p_inn varchar2 := null,
 p_cursor out sys_refcursor
);

procedure FIND_TXPR_BY_NDS_COMPENSATION
(
 p_sono_code varchar2,
 p_year varchar2,
 p_qtr varchar2,
 p_inn varchar2 := null,
 p_cursor out sys_refcursor
);

procedure FIND_TXPR_BY_INN
  (
   p_inn in varchar2,
   p_cursor out sys_refcursor
  );

procedure TAXPAYER_RELATION
  (
    p_Inn IN varchar2,              /* »ЌЌ Ќѕ */
    p_MaxContractors IN number,     /*  олво наиболее значимых  */
    p_ByPurchase IN NUMBER,         /* направление - в сторону покупок или продаж */
    p_Year varchar2 := null,        /* отетный год */
    p_Qtr varchar2 := null,         /* отчетный период 1-4 */
    p_levels in number := null,     /* ”ровень автоматического построени¤ */
    p_sharePercentNdsCriteria in number := null, /* ќграничение отбора Ќѕ в дерево по % Ќ?— */
    p_minReturnedContractors in number := null, /* минимальное число отобранных по фильтру узлов */
    p_Cursor OUT SYS_REFCURSOR      /* */
   );

PROCEDURE TAXPAYER_RELATION_FAKE_ROOT
(
	p_Inn IN varchar2,              				/* ИНН НП 											*/
	p_MaxContractors IN number,     				/* Колво наиболее значимых  						*/
	p_Year varchar2 := null,        				/* Отчетный год 									*/
	p_Qtr varchar2 := null,         				/* Отчетный период 1-4 								*/
	p_isRoot in number := 0, /* Возвращает узел для запрашеваемого ИНН - 1 или нет - 0 */
	p_Cursor OUT SYS_REFCURSOR
);

TYPE T$ARR_STR IS TABLE OF varchar2(12) INDEX BY PLS_INTEGER;

PROCEDURE TAXPAYER_RELATION_REPORT
(
	p_Inn IN T$ARR_STR,              				/* ИНН НП 											*/
	p_Kpp IN T$ARR_STR,              				/* КПП НП 											*/
	p_MaxContractors IN number,     				/* Колво наиболее значимых  						*/
	p_ByPurchase IN NUMBER,         				/* Направление - в сторону покупок или продаж 		*/
	p_Year varchar2 := null,        				/* Отчетный год 									*/
	p_Qtr varchar2 := null,         				/* Отчетный период 1-4 								*/
	p_sharePercentNdsCriteria in number := null,	/* Ограничение отбора НП в дерево по % НДС 			*/
	p_minReturnedContractors in number := null, 	/* Минимальное число отобранных по фильтру узлов 	*/
	p_isRoot in number := 0,
	p_Cursor OUT SYS_REFCURSOR
);

end;
/

create or replace 
PACKAGE BODY                    NDS2_MRR_USER.PAC$HRZ_REPORT
as

procedure log_call
(
p_ev_name varchar2,
p_param varchar2
)
as
pragma autonomous_transaction;
begin
  /*insert into hrz_log(id, ev_name, params , dt )
  values (SEQ_HRZ_LOG.NEXTVAL, p_ev_name,p_param, sysdate );
  commit;

  exception when others then rollback;*/
  null;
end;

procedure FIND_TXPR_BY_DISCREPANCY
(
 p_sono_code varchar2,
 p_year varchar2,
 p_qtr varchar2,
 p_inn varchar2 := null,
 p_cursor out sys_refcursor
)
as
begin
log_call('FIND_TXPR_BY_DISCREPANCY',  'sono:'||p_sono_code||', year:'||p_year||', qtr'||p_qtr);

open p_Cursor for select * from (
    select
     egrn.inn,
     egrn.np_name as name,
     0.0 as gap_amnt
    from mv$tax_payer_name egrn
    order by egrn.inn) t
    where rownum < 50; -- заглушка
    /*from hrz_decl_seller_gap  dsg
      inner join mv$tax_payer_name egrn on egrn.inn = dsg.inn
    where
      dsg.inn like p_inn||'%'
    order by dsg.gap_amnt desc*/
end;

procedure FIND_TXPR_BY_NDS_COMPENSATION
(
 p_sono_code varchar2,
 p_year varchar2,
 p_qtr varchar2,
 p_inn varchar2 := null,
 p_cursor out sys_refcursor
)
as
begin
  log_call('FIND_TXPR_BY_NDS_COMPENSATION',  'sono:'||p_sono_code||', year:'||p_year||', qtr'||p_qtr);
    open p_Cursor for
    select
      egrn.inn,
      egrn.np_name as name,
      0.0 as sumnds
   from mv$tax_payer_name egrn
    /*from hrz_decl_compensation tpc
    where tpc.rn = 1
     and tpc.sono = p_sono_code
     and tpc.year = p_year
     and tpc.qtr = p_qtr
     and (p_inn is null or tpc.inn like p_inn||'%')
    order by tpc.sumnds asc*/;
end;

procedure FIND_TXPR_BY_INN
  (
   p_inn in varchar2,
   p_cursor out sys_refcursor
  )
as
begin
  log_call('FIND_TXPR_BY_INN',  'inn:'||p_inn);
  open p_Cursor for
select
       0 as compensation_sum,
       0 as nds_dif_sum,
       0 as nds_deduction,
       0 as nds_calculated,
       0 as buy_sum,
       0 as sale_sum,
       '  возмещению' as declaration_type,
       inn,
       kpp,
      regexp_replace(regexp_replace(regexp_replace(np_name,
		'[Оо][Бб][Щщ][Ее][Сс][Тт][Вв][Оо][[:blank:]]+[Сс][[:blank:]]+[Оо][Гг][Рр][Аа][Нн][Ии][Чч][Ее][Нн][Нн][Оо][Йй][[:blank:]]+[Оо][Тт][Вв][Ее][Тт][Сс][Тт][Вв][Ее][Нн][Нн][Оо][Сс][Тт][Ьь][Юю]','ООО'),
		'[Оо][Тт][Кк][Рр][ыЫ][Тт][Оо][Ее][[:blank:]]+[Аа][Кк][Цц][Ии][Оо][Нн][Ее][Рр][Нн][Оо][Ее][[:blank:]]+[Оо][Бб][Щщ][Ее][Сс][Тт][Вв][Оо]','ОАО'),
		'[Зз][Аа][Кк][Рр][ыЫ][Тт][Оо][Ее][[:blank:]]+[Аа][Кк][Цц][Ии][Оо][Нн][Ее][Рр][Нн][Оо][Ее][[:blank:]]+[Оо][Бб][Щщ][Ее][Сс][Тт][Вв][Оо]','ЗАО')
        as name
        from mv$tax_payer_name where inn like p_inn||'%';
end;

PROCEDURE TAXPAYER_RELATION
(
  p_Inn IN varchar2,              /* »ЌЌ Ќѕ */
  p_MaxContractors IN number,     /*  олво наиболее значимых  */
  p_ByPurchase IN NUMBER,         /* направление - в сторону покупок или продаж */
  p_Year varchar2 := null,        /* отетный год */
  p_Qtr varchar2 := null,         /* отчетный период 1-4 */
  p_levels in number := null,     /* ”ровень автоматического построени¤ */
  p_sharePercentNdsCriteria in number := null, /* ќграничение отбора Ќѕ в дерево по % Ќ?— */
  p_minReturnedContractors in number := null, /* минимальное число отобранных по фильтру узлов */
  p_Cursor OUT SYS_REFCURSOR      /* */
)
as
v_lvl number := 1;
v_year number(4) := p_Year;
v_period number(1) := p_Qtr;
v_maxLevel number := p_levels;
v_min_vat_share number:= nvl(p_sharePercentNdsCriteria,0);
v_inn varchar (12 char) := trim(p_Inn);
v_byPurchase number := p_ByPurchase;
tree_relations_table_parent T$HRZ_TREE_RELATIONS;
tree_relations_table_children T$HRZ_TREE_RELATIONS;
tree_relations_table T$HRZ_TREE_RELATIONS;
items_separator varchar2(2):='^|';
item_elements_separator varchar2(2):='^~';
declaration_type_code number(1):=0;
processingstage number(1):=2;
maxContractors number(3) := p_MaxContractors;
begin
 if (v_maxLevel > 2) then
	maxContractors := 50;
 end if;

 select
    T$HRZ_TREE_RELATION(1,null,null,null,d.inn,1,null,d.compensation_amnt,d.ch8_nds,d.ch9_nds,d.gap_discrep_amnt,null,null,null,declaration_type_code,d.soun_code,d.soun ,processingstage,d.region,d.region_code,d.region_code||' '||d.region,d.inn||item_elements_separator||egrn.kpp||item_elements_separator||egrn.name_short,egrn.kpp,egrn.name_short,lpad(rownum,10,'0'),0,0,0,0,0,d.nds_discrep_amnt,d.decl_sign,d.transit,null)
    bulk collect into tree_relations_table_parent
 from HRZ_DECL_INFO d
 left join TAX_PAYER egrn 
 on egrn.inn = d.inn
 and egrn.kpp_effective = d.kpp_effective
 where d.inn = v_inn
       and d.year = v_year
       and d.quarter = v_period;

 tree_relations_table := tree_relations_table_parent;

 v_lvl := v_lvl + 1;

 while v_lvl <= v_maxLevel loop
  if v_byPurchase <> 0 then -- РѕС‚ РїРѕРєСѓРїР°С‚РµР»В¤
   begin
    select
      T$HRZ_TREE_RELATION(rownum
     ,d.inn_1
     ,d.parent_vat_deduction_total
     ,d.parent_vat_calculation_total
     ,d.inn_2
     ,v_lvl
     ,null
     ,d.total_nds
     ,d.deduction_nds
     ,d.calc_nds
     ,d.gap_discrep_amnt
     ,d.gap_amount
     ,d.nba
     ,d.nsa
     ,declaration_type_code
     ,d.soun_code
     ,d.soun
     ,d.processingstage
     ,d.region
     ,d.region_code
     ,d.region_name
     ,d.all_parents||items_separator||d.inn_2||item_elements_separator||egrn.kpp||item_elements_separator||egrn.name_short
     ,egrn.kpp
     ,egrn.name_short
     ,d.path||'|'||lpad(rownum,10,'0')
     ,d.njsa
     ,d.njba
     ,d.before2015_amount
     ,d.after2015_amount
     ,d.nds_amount
     ,d.nds_discrep_amnt
     ,d.decl_sign
     ,d.transit
     ,d.year_quarter_vat_list)
    bulk collect into tree_relations_table_children
    from (
    select data.inn_1
           ,parent_np.vat_deduction_total as parent_vat_deduction_total
           ,parent_np.vat_calculation_total as parent_vat_calculation_total
           ,data.inn_2
           ,data.total_nds
           ,data.deduction_nds
           ,data.calc_nds
           ,data.gap_discrep_amnt
           ,data.gap_amount
           ,data.nba
           ,data.nsa
           ,declaration_type_code
           ,data.soun_code
           ,data.soun
           ,processingstage
           ,data.region
           ,data.region_code
           ,data.region_code||' '||data.region as region_name
           ,parent_np.all_parents as all_parents
           ,data.nba_order
           ,parent_np.path
           ,data.njsa
           ,data.njba
           ,data.before2015_amount
           ,data.after2015_amount
           ,data.nds_amount
           ,data.nds_discrep_amnt
           ,data.decl_sign_2 as decl_sign
           ,data.transit_2 as transit
           ,data.year_quarter_vat_list
           ,data.kpp_2_effective
    from
     HRZ_RELATIONS data
     join table(tree_relations_table_parent) parent_np
      on   parent_np.lvl = v_lvl - 1
       and data.inn_1 = parent_np.inn
       and parent_np.inn not in ('0000000001','0000000026','0000000020','0000000019')
       and data.year = v_year
       and data.quarter = v_period
       and data.nba_order <= maxContractors
     where data.own_relation != 1 and
           (data.nba + data.njba) > 0 and
           data.nba_order <= maxContractors and
         (v_min_vat_share = 0 or data.nba_order <= p_minReturnedContractors
      or 100 * data.nba > v_min_vat_share * parent_np.vat_deduction_total)) d
     left join TAX_PAYER egrn 
     on egrn.inn = d.inn_2
     and egrn.kpp_effective = d.kpp_2_effective
     order by d.nba_order;
   end;
  else -- РѕС‚ РїСЂРѕРґР°РІС†Р°
   begin
    select
      T$HRZ_TREE_RELATION(rownum
     ,d.inn_1
     ,d.parent_vat_deduction_total
     ,d.parent_vat_calculation_total
     ,d.inn_2
     ,v_lvl
     ,null
     ,d.total_nds
     ,d.deduction_nds
     ,d.calc_nds
     ,d.gap_discrep_amnt
     ,d.gap_amount
     ,d.nba
     ,d.nsa
     ,declaration_type_code
     ,d.soun_code
     ,d.soun
     ,d.processingstage
     ,d.region
     ,d.region_code
     ,d.region_name
     ,d.all_parents||items_separator||d.inn_2||item_elements_separator||egrn.kpp||item_elements_separator||egrn.name_short
     ,egrn.kpp
     ,egrn.name_short
     ,d.path||'|'||lpad(rownum,10,'0')
     ,d.njsa
     ,d.njba
     ,d.before2015_amount
     ,d.after2015_amount
     ,d.nds_amount
     ,d.nds_discrep_amnt
     ,d.decl_sign
     ,d.transit
     ,d.year_quarter_vat_list)
    bulk collect into tree_relations_table_children
    from (
    select data.inn_1
           ,parent_np.vat_deduction_total as parent_vat_deduction_total
           ,parent_np.vat_calculation_total as parent_vat_calculation_total
           ,data.inn_2
           ,data.total_nds
           ,data.deduction_nds
           ,data.calc_nds
           ,data.gap_discrep_amnt
           ,data.gap_amount
           ,data.nba
           ,data.nsa
           ,declaration_type_code
           ,data.soun_code
           ,data.soun
           ,processingstage
           ,data.region
           ,data.region_code
           ,data.region_code||' '||data.region as region_name
           ,parent_np.all_parents as all_parents
           ,data.nsa_order
           ,parent_np.path
           ,data.njsa
           ,data.njba
           ,data.before2015_amount
           ,data.after2015_amount
           ,data.nds_amount
           ,data.nds_discrep_amnt
           ,data.decl_sign_2 as decl_sign
           ,data.transit_2 as transit
           ,data.year_quarter_vat_list
           ,data.kpp_2_effective
     from
     HRZ_RELATIONS data
     join table(tree_relations_table_parent) parent_np
      on   parent_np.lvl = v_lvl - 1
       and data.inn_1 = parent_np.inn
       and parent_np.inn not in ('0000000001','0000000026','0000000020','0000000019')
       and data.year = v_year
       and data.quarter = v_period
       and data.nsa_order <= maxContractors
     where data.own_relation != 1 and
          (data.nsa + data.njsa) > 0 and
          data.nsa_order <= maxContractors and
          (v_min_vat_share = 0 or data.nsa_order <= p_minReturnedContractors
          or 100 * data.nsa > v_min_vat_share * parent_np.vat_calculation_total)) d
      left join TAX_PAYER egrn 
      on egrn.inn = d.inn_2
      and egrn.kpp_effective = d.kpp_2_effective
      order by d.nsa_order;
   end;
  end if;
  v_lvl := v_lvl + 1;

  select
      T$HRZ_TREE_RELATION(t.id
     ,t.parent_inn
     ,t.vat_deduction_total
     ,t.vat_calculation_total
     ,t.inn
     ,t.lvl
     ,t.declaration_type
     ,t.vat_total
     ,t.vat_deduction_total
     ,t.vat_calculation_total
     ,t.gap_discrepancy_total
     ,t.gap_discrepancy
     ,t.vat_deduction
     ,t.vat_calculation
     ,t.decl_type_code
     ,t.soun_code
     ,t.soun_name
     ,t.processingstage
     ,t.region
	   ,t.region_code
	   ,t.region_name
     ,t.all_parents
     ,t.kpp
     ,t.name
     ,t.path
     ,t.njsa
     ,t.njba
     ,t.before2015_amount
     ,t.after2015_amount
     ,t.nds_amount
     ,t.nds_discrep_amnt
     ,t.decl_sign
     ,t.transit
     ,t.year_quarter_vat_list)
  bulk collect into tree_relations_table_parent
  from table(tree_relations_table_children) t
  where t.inn not in (select t1.inn from table(tree_relations_table) t1);

  if tree_relations_table_children.count > 0 then
    for i in tree_relations_table_children.first..tree_relations_table_children.last
    loop
     tree_relations_table.extend();
     tree_relations_table(tree_relations_table.count) := tree_relations_table_children(i);
    end loop;
  end if;

 end loop;

  open p_Cursor for
  select
    tree.id
   ,tree.parent_inn
   ,tree.lvl
   ,tree.inn
   ,tree.name as name
   ,tree.kpp
   ,tree.declaration_type
   ,nvl(tree.vat_calculation_total,0) as calc_nds
   ,nvl(tree.vat_deduction_total,0) as deduction_nds
   ,summary.buyers_cnt
   ,summary.sellers_cnt
   ,cast((case
     when nvl(tree.vat_calculation_total,0) = 0
     then 0
     else 100 * nvl(tree.vat_deduction_total,0) / tree.vat_calculation_total
    end) as number(19,2)) as share_nds_amnt
   ,nvl(sur.sign_code, 4) as sur
   ,cast((case when v_byPurchase <> 0 then nvl(rev.nsa,0) else nvl(tree.vat_calculation,0) end) as number(19,2)) as mapped_amount
   ,cast((case when v_byPurchase <> 0 then nvl(tree.vat_deduction,0) else nvl(rev.nba,0) end) as number(19,2)) as not_mapped_amount
   ,cast((nvl(case when v_byPurchase <> 0 then tree.gap_discrepancy else rev.gap_amount end,0)) as number(19,2)) as discrepancy_amnt
   ,cast((case
     when v_byPurchase <> 0 and nvl(tree.parent_vat_deduction_total,0) <> 0 then 100*nvl(tree.vat_deduction,0)/tree.parent_vat_deduction_total
     when v_byPurchase <> 0 and nvl(tree.parent_vat_calculation_total,0) <> 0 then 100*nvl(tree.vat_calculation,0)/tree.parent_vat_calculation_total
     else 0.00
   end) as number(19,2)) as vat_share
   ,nvl(tree.vat_total,0) as vat_total
   ,tree.gap_discrepancy_total as gap_discrepancy_total
   ,1 as sell_amnt
   ,2 as buy_amnt
   ,3 as total_purchase_amnt
   ,4 as total_sales_amnt
   ,tree.decl_type_code
   ,tree.soun_code
   ,tree.soun_name
   ,tree.processingstage
   ,surd.color
   ,surd.description
   ,surd.is_default
   ,tree.region
	 ,tree.region_code
	 ,tree.region_name
   ,tree.all_parents
   ,cast((case when v_byPurchase = 0 then nvl(tree.njsa,0) else nvl(rev.njsa,0) end) as number(19,2)) as njsa
   ,cast((case when v_byPurchase = 0 then nvl(rev.njba,0) else nvl(tree.njba,0) end) as number(19,2)) as njba
   ,cast((case when v_byPurchase <> 0 then nvl(tree.before2015_amount,0) else nvl(rev.before2015_amount,0) end) as number(19,2)) as before2015_amount
   ,cast((case when v_byPurchase <> 0 then nvl(tree.after2015_amount,0) else nvl(rev.after2015_amount,0) end) as number(19,2)) as after2015_amount
   ,cast((nvl(case when v_byPurchase <> 0 then tree.nds_amount else rev.nds_amount end,0)) as number(19,2)) as nds_amount
   ,tree.nds_discrep_amnt
   ,cast((case when v_byPurchase <> 0
   then
        (case when nvl(tree.vat_calculation_total, 0) = 0  then 0 else 100 * nvl(rev.nsa,0) / tree.vat_calculation_total end)
   else
        (case when nvl(tree.parent_vat_calculation_total, 0) = 0  then 0 else 100 * nvl(tree.vat_calculation,0) / tree.parent_vat_calculation_total end)
   end) as number(19,2)) as seller_nds_prc
   ,cast((case when v_byPurchase <> 0
   then
        (case when nvl(tree.parent_vat_deduction_total, 0) = 0  then 0 else 100 * nvl(tree.vat_deduction,0) / tree.parent_vat_deduction_total end)
   else
        (case when nvl(tree.vat_deduction_total, 0) = 0  then 0 else 100 * nvl(rev.nba,0) / tree.vat_deduction_total end)
   end) as number(19,2)) as client_nds_prc
  ,tax_payer.IS_LARGE
  ,decode(tree.decl_sign,4,0,tree.decl_sign) as sign_code
  ,tree.transit
  ,tree.year_quarter_vat_list
  from
  (select * from table(tree_relations_table)) tree
   left join HRZ_RELATIONS_TOTAL summary
    on   summary.year = v_year
     and summary.quarter = v_period
     and summary.inn = tree.inn
   left join
   (select t.*
    from HRZ_RELATIONS t
    where t.own_relation != 1) rev
     on rev.year = v_year
     and rev.quarter = v_period
     and rev.inn_1 = tree.inn
     and rev.inn_2 = tree.parent_inn
	left join HRZ_SUR SUR on SUR.INN = tree.inn AND SUR.FISCAL_YEAR = v_year AND SUR.QUARTER = v_period 
    left join (select s.code as code
                 ,s.description as description
                 ,s.color_argb as color
                 ,s.is_default as is_default
          from dict_sur s) surd on surd.code = nvl(sur.sign_code, 4)
    left join TAX_PAYER tax_payer on tax_payer.INN = tree.inn
	order by tree.path;
end;

PROCEDURE TAXPAYER_RELATION_FAKE_ROOT
(
	p_Inn IN varchar2,              				/* ИНН НП 											*/
	p_MaxContractors IN number,     				/* Колво наиболее значимых  						*/
	p_Year varchar2 := null,        				/* Отчетный год 									*/
	p_Qtr varchar2 := null,         				/* Отчетный период 1-4 								*/
  p_isRoot in number := 0, /* Возвращает узел для запрашеваемого ИНН - 1 или нет - 0 */
	p_Cursor OUT SYS_REFCURSOR
)
as
v_lvl number := 1;
v_year number(4) := p_Year;
v_period number(1) :=p_Qtr;
v_min_vat_share number:= 0;
v_inn varchar (12 char) :=trim(p_Inn);


tree_relations_table_parent T$HRZ_TREE_RELATIONS;
tree_relations_table_children T$HRZ_TREE_RELATIONS;
tree_relations_table T$HRZ_TREE_RELATIONS := T$HRZ_TREE_RELATIONS();
items_separator varchar2(2):='^|';
item_elements_separator varchar2(2):='^~';
declaration_type_code number(1):=0;
processingstage number(1):=2;
maxContractors number(3) :=p_MaxContractors;
-------------------------------------------------------------------
begin

 select
    T$HRZ_TREE_RELATION(1,null,null,null,d.inn,1,null,d.compensation_amnt,d.ch8_nds,d.ch9_nds,d.gap_discrep_amnt,null,null,null,declaration_type_code,null,null,processingstage,null,null,null,d.inn||item_elements_separator||egrn.kpp||item_elements_separator||egrn.name_short,egrn.kpp,egrn.name_short,lpad(rownum,10,'0'),0,0,0,0,0,d.nds_discrep_amnt,d.decl_sign,d.transit,null)
    bulk collect into tree_relations_table_parent
 from HRZ_DECL_INFO d
 left join TAX_PAYER egrn 
      on egrn.inn = d.inn
      and egrn.kpp_effective = d.kpp_effective
 where d.inn = v_inn
       and d.year = v_year
       and d.quarter = v_period;

--не нашли декларацию, создаем искуственный корневой узел, от него будем искать

if tree_relations_table_parent  IS  EMPTY then
select
    T$HRZ_TREE_RELATION(1,null,null,null,v_inn,1,null,null,null,null,null,null,null,null,declaration_type_code,null,null,processingstage,null,null,null,v_inn||item_elements_separator||''||item_elements_separator||'','','',lpad(rownum,10,'0'),0,0,0,0,0,0,null,null,null)
    bulk collect into tree_relations_table_parent
    from  DUAL;
END IF;

--корневой узел складываем в результирующий набор
 if p_isRoot = 1 then
    tree_relations_table := tree_relations_table_parent;
 end if;

 v_lvl := v_lvl + 1; -- второй уровень дерева

 -- к уже выбранному узлу из tree_relations_table_parent
 -- находим потомков в HRZ_RELATIONS и складываем найденное в tree_relations_table_children
 -- числовые значения - это значения по данным узла без декларации, оставляем пустыми
    select
      T$HRZ_TREE_RELATION(rownum -- id
     ,d.inn_2 -- parentINN- поменяно местами
     ,0
     ,0
     ,d.inn_1 -- INN- поменяно местами
     ,v_lvl   -- уровень
     ,null ,0 ,0 ,0 ,0 ,0 ,0 ,0
     ,declaration_type_code
     ,0 ,null--d.soun
     ,d.processingstage
     ,null ,null ,null
     ,d.all_parents||items_separator||d.inn_1||item_elements_separator||egrn.kpp||item_elements_separator||egrn.name_short
     ,egrn.kpp -- кпп контрагента
     ,egrn.name_short -- название контрагента
     ,d.path||'|'||lpad(rownum,10,'0') -- для сортировки
     ,0
     ,d.njba
     ,d.before2015_amount -- Вычеты покупателя до 15г
     ,d.after2015_amount -- Вычеты покупателя досле 15г
     ,0
     ,0
     ,d.decl_sign
     ,d.transit
     ,d.year_quarter_vat_list)
    bulk collect into tree_relations_table_children
    from (
    select
            data.inn_1
           ,parent_np.vat_deduction_total as parent_vat_deduction_total
           ,parent_np.vat_calculation_total as parent_vat_calculation_total
           ,data.inn_2
           ,data.nba
           ,data.njba
           ,declaration_type_code
           ,processingstage
           ,parent_np.all_parents as all_parents
           ,data.rev_nba_order
           ,parent_np.path
           ,data.after2015_amount
           ,data.before2015_amount
           ,data.decl_sign_1 as decl_sign
           ,data.transit_1 as transit
           ,data.year_quarter_vat_list
           ,data.kpp_1_effective
      from HRZ_RELATIONS data
      join table (tree_relations_table_parent) parent_np
      on parent_np.lvl = v_lvl - 1
       and data.rev_nba_order <= maxContractors
       and data.year = v_year
       and data.quarter = v_period
       and data.own_relation != 1 -- не ссылается сама на себя
       and data.inn_2 = parent_np.inn -- поиск по второму участнику связи
       and parent_np.inn not in ('0000000001','0000000026','0000000020','0000000019')
       and data.nba + data.njba > 0 -- и обратной сумме (для поиска от продавца - сумма налога с покупки)
    ) d
    left join TAX_PAYER egrn 
     on egrn.inn = d.inn_1
     and egrn.kpp_effective = d.kpp_1_effective
    where d.inn_1 not in (  select data.inn_2
                               from
                               HRZ_RELATIONS data
                               join table(tree_relations_table_parent) parent_np
                               on   parent_np.lvl = v_lvl - 1
                               and data.inn_1 = parent_np.inn
                               where data.own_relation != 1
                               and data.nsa + data.njsa > 0
                               and data.year = v_year
                               and data.quarter = v_period)
    order by rev_nba_order; -- сортируем по сумме ндс

  --если на этой итерации нашли новые результаты перекладываемм их в коллекцию с результатами
  if tree_relations_table_children.count > 0 then
    for i in tree_relations_table_children.first..tree_relations_table_children.last
    loop
     tree_relations_table.extend(); -- расширяем коллекцию
     tree_relations_table(tree_relations_table.count) := tree_relations_table_children(i); -- добавляем запись
    end loop;
  end if;

 open p_Cursor for -- курсор с результатами - Пересечение tree_relations_table с HRZ_RELATIONS, HRZ_RELATIONS_TOTAL, EXT_SUR, dict_sur
  select
    tree.id
   ,tree.parent_inn
   ,tree.lvl
   ,tree.inn
   ,tree.name as name
   ,tree.kpp
   ,tree.declaration_type
   ,0 as calc_nds
   ,summary.buyers_cnt
   ,0 as deduction_nds
   ,summary.sellers_cnt
   ,0	as share_nds_amnt -- Доля вычета в исчисленном НДС по декларации НП
   ,nvl(sur.sign_code, 4) as sur
   ,0 as mapped_amount -- Исчисленный НДС по данным продавца по операциям с покупателем
   , nvl(rev.nba,0)	as not_mapped_amount --НДС к вычету по данным покупателя по операциям с продавцом
   ,cast((nvl(rev.gap_amount ,0)) as number(19,2)) as discrepancy_amnt -- Сумма расхождений типа «разрыв» между НП и родителем
   ,0.00	as vat_share--Доля исчисленного НДС по сделкам между НП и родителем в общей сумме Исчисленного НДС по декларации
   ,0 as vat_total --Сумма НДС к уплате/возмещению по данным декларации НП
   ,cast( nvl(rev.gap_discrep_amnt,0)  as number) as gap_discrepancy_total -- Сумма расхождений типа разрыв всего у НП
   ,1 as sell_amnt
   ,2 as buy_amnt
   ,3 as total_purchase_amnt
   ,4 as total_sales_amnt
   ,tree.decl_type_code --Код типа декларации
   ,null as soun_code -- Код инспекции
   ,null as soun_name -- Название инспекции
   ,tree.processingstage -- Состояние обработки декларации
   ,surd.color --Цвет для СУР из таблицы DICT_SUR
   ,surd.description -- Описание для СУР из таблицы DICT_SUR
   ,surd.is_default --состояние СУР по умолчанию
   ,null as region
	 ,null as region_code
	 ,null as region_name -- Название региона НП
   ,tree.all_parents -- путь к корневому узну
   ,0 as njsa --НДС по журналу учета выставленных СФ
   ,tree.njba as njba --НДС по журналу учета полученных СФ
   ,tree.before2015_amount as before2015_amount
   ,tree.after2015_amount as after2015_amount
   ,0 as nds_amount --Сумма расхождений вида проверка НДС
   ,tree.nds_discrep_amnt as nds_discrep_amnt
   ,0 as seller_nds_prc
   ,0 as client_nds_prc
   ,tax_payer.IS_LARGE
   ,decode(tree.decl_sign,4,0,tree.decl_sign) as sign_code
   ,tree.transit
   ,tree.year_quarter_vat_list
  from
  (select * from table(tree_relations_table)) tree -- дерево связей
   left join HRZ_RELATIONS_TOTAL summary
    on   summary.year = v_year
		 and summary.quarter = v_period
		 and summary.inn = tree.inn
   left join   (select t.*    from HRZ_RELATIONS t    where t.own_relation != 1) rev -- связи
     on rev.year = v_year
		 and rev.quarter = v_period
		 and rev.inn_1 = tree.inn
		 and rev.inn_2 = tree.parent_inn
	left join HRZ_SUR SUR on SUR.INN = tree.inn AND SUR.FISCAL_YEAR = v_year AND SUR.QUARTER = v_period   
    left join (select s.code as code
                 ,s.description as description
                 ,s.color_argb as color
                 ,s.is_default as is_default
          from dict_sur s) surd
	on surd.code = nvl(sur.sign_code, 4)
	left join TAX_PAYER tax_payer on tax_payer.INN = tree.inn
  order by tree.path;
end;

PROCEDURE TAXPAYER_RELATION_REPORT
(
	p_Inn IN T$ARR_STR,              				/* ИНН НП 											*/
  p_Kpp IN T$ARR_STR,              				/* КПП НП 											*/
	p_MaxContractors IN number,     				/* Колво наиболее значимых  						*/
	p_ByPurchase IN NUMBER,         				/* Направление - в сторону от покупателя - 1, от продавца - 0	*/
	p_Year varchar2 := null,        				/* Отчетный год 									*/
	p_Qtr varchar2 := null,         				/* Отчетный период 1-4 								*/
	p_sharePercentNdsCriteria in number := null,	/* Ограничение отбора НП в дерево по % НДС 			*/
	p_minReturnedContractors in number := null, 	/* Минимальное число отобранных по фильтру узлов 	*/
  p_isRoot in number := 0, /* Возвращает узел для запрашеваемого ИНН - 1 или нет - 0 */
	p_Cursor OUT SYS_REFCURSOR
)
as
v_year number(4) := p_Year;
v_period number(1) := p_Qtr;
v_min_vat_share number:= nvl(p_sharePercentNdsCriteria,0);
v_inn varchar (12 char);
v_kpp varchar (12 char);
v_byPurchase number := p_ByPurchase;
declaration_type_code number(1):=0;
processingstage number(1):=2;
maxContractors number(3) := p_MaxContractors;
key_list T$HRZ_TREE_KEY_LIST := T$HRZ_TREE_KEY_LIST();
begin

 for i in 1..p_Inn.count loop
  v_inn := trim(substr(p_Inn(i), 0, 12));
  v_kpp := trim(substr(p_Kpp(i), 0, 12));

  key_list.extend();
  key_list(key_list.count) := T$HRZ_TREE_KEY(v_inn, v_kpp);
 end loop;

 open p_Cursor for -- курсор с результатами - Пересечение HRZ_RELATIONS, HRZ_RELATIONS_TOTAL, EXT_SUR, dict_sur
 select
    tree.amountOrder as id
   ,tree.inn_1 as parent_inn
   ,tree.inn_2 as inn
   ,tree.lvl as lvl
   ,egrn.name_short as name
   ,egrn.kpp as kpp
   ,nvl(tree.calc_nds,0) as calc_nds
   ,summary.buyers_cnt
   ,nvl(tree.deduction_nds,0) as deduction_nds
   ,summary.sellers_cnt
   ,cast((case
			when nvl(tree.calc_nds,0) = 0
			then 0
			else 100 * nvl(tree.deduction_nds,0) / tree.calc_nds end) as number(19,2))
		as share_nds_amnt -- Доля вычета в исчисленном НДС по декларации НП
   ,nvl(sur.sign_code, 4) as sur
   ,cast((case
			when v_byPurchase <> 0
			then nvl(rev.nsa,0)
			else nvl(tree.nsa,0) end) as number(19,2))
		as mapped_amount -- Исчисленный НДС по данным продавца по операциям с покупателем
   ,cast((case
			when v_byPurchase <> 0
			then nvl(tree.nba,0)
			else nvl(rev.nba,0) end) as number(19,2))
		as not_mapped_amount --НДС к вычету по данным покупателя по операциям с продавцом
   ,cast((nvl(case
			when v_byPurchase <> 0
			then tree.gap_amount
			else rev.gap_amount end,0)) as number(19,2))
		as discrepancy_amnt --Сумма расхождений типа «разрыв» между НП и родителем
   ,cast((case
			when v_byPurchase <> 0 and nvl(rev.deduction_nds,0) <> 0
			then 100*nvl(tree.nba,0)/rev.deduction_nds -- Доля вычета НДС по сделкам между НП и родителем в общей сумме НДС к вычету по декларации
			when v_byPurchase = 0 and nvl(rev.calc_nds,0) <> 0
			then 100*nvl(tree.nsa,0)/rev.calc_nds --Доля исчисленного НДС по сделкам между НП и родителем в общей сумме Исчисленного НДС по декларации
			else 0.00   end) as number(19,2))
		as vat_share
   ,nvl(tree.total_nds,0) as vat_total --Сумма НДС к уплате/возмещению по данным декларации НП
   ,tree.gap_discrep_amnt as gap_discrepancy_total -- Сумма расхождений типа разрыв всего у НП
   ,tree.decl_type_code --Код типа декларации
   ,tree.soun_code -- Код инспекции
   ,tree.soun_name -- Название инспекции
   ,tree.processingstage -- Состояние обработки декларации
   ,surd.color --Цвет для СУР из таблицы DICT_SUR
   ,surd.description -- Описание для СУР из таблицы DICT_SUR
   ,surd.is_default --состояние СУР по умолчанию
   ,tree.region --Регион НП (код+название)
   ,tree.region_code -- Код региона НП
   ,tree.region_name -- Название региона НП
   ,cast((case when v_byPurchase = 0 then nvl(tree.njsa,0) else nvl(rev.njsa,0) end) as number(19,2)) as njsa --НДС по журналу учета выставленных СФ
   ,cast((case when v_byPurchase = 0 then nvl(rev.njba,0) else nvl(tree.njba,0) end) as number(19,2)) as njba --НДС по журналу учета полученных СФ
   ,cast((case when v_byPurchase <> 0 then nvl(tree.before2015_amount,0) else nvl(rev.before2015_amount,0) end) as number(19,2)) as before2015_amount --вычет НДС до 2015
   ,cast((case when v_byPurchase <> 0 then nvl(tree.after2015_amount,0) else nvl(rev.after2015_amount,0) end) as number(19,2)) as after2015_amount --вычет НДС после 2015
   ,cast((nvl(case when v_byPurchase <> 0 then tree.nds_amount else rev.nds_amount end,0)) as number(19,2)) as nds_amount --Сумма расхождений вида проверка НДС с контрагентом
   ,tree.nds_discrep_amnt as nds_discrep_amnt --Сумма расхождений вида проверка НДС всего
   ,cast((case when v_byPurchase <> 0
   then
        (case when nvl(tree.calc_nds, 0) = 0  then 0 else 100 * nvl(rev.nsa,0) / tree.calc_nds end)
   else
        (case when nvl(pr.ch9_nds, 0) = 0  then 0 else 100 * nvl(tree.nsa,0) / pr.ch9_nds end)
   end) as number(19,2)) as seller_nds_prc --Доля исчисленного НДС
   ,cast((case when v_byPurchase <> 0
   then
        (case when nvl(pr.ch8_nds, 0) = 0  then 0 else 100 * nvl(tree.nba,0) / pr.ch8_nds end)
   else
        (case when nvl(tree.deduction_nds, 0) = 0  then 0 else 100 * nvl(rev.nba,0) / tree.deduction_nds end)
   end) as number(19,2)) as client_nds_prc --Доля вычетов НДС
   ,decode(tree.decl_sign_2,4,0,tree.decl_sign_2) as sign_code
   ,tree.transit_2 as transit
   ,tree.year_quarter_vat_list
   from
   (
   select 1 as lvl
          ,null as inn_1
          ,d.inn as inn_2
          ,d.compensation_amnt as total_nds
          ,d.ch8_nds as deduction_nds
          ,d.ch9_nds as calc_nds
          ,d.gap_discrep_amnt
          ,0 as gap_amount
          ,0 as nba
          ,0 as nsa
          ,0 decl_type_code
          ,d.soun_code as soun_code
          ,d.soun as soun_name
          ,2 as processingstage
          ,d.region as region
          ,d.region_code as region_code
          ,d.region_code||' '||d.region as region_name
          ,0 as amountOrder
          ,0 as njsa
          ,0 as njba
          ,0 as before2015_amount
          ,0 as after2015_amount
          ,0 as nds_amount
          ,d.nds_discrep_amnt as nds_discrep_amnt
          ,null as decl_sign_1
          ,d.decl_sign as decl_sign_2
          ,null as transit_1
          ,d.transit as transit_2
          ,null as year_quarter_vat_list
          ,d.kpp_effective as kpp_effective
   from HRZ_DECL_INFO d, table(key_list) k
   where d.inn = k.inn
   and d.year = v_year
   and d.quarter = v_period
   and p_isRoot > 0
   union
   select 2 as lvl
          ,data.inn_1
          ,data.inn_2
          ,data.total_nds
          ,data.deduction_nds
          ,data.calc_nds
          ,data.gap_discrep_amnt
          ,data.gap_amount
          ,data.nba
          ,data.nsa
          ,0 as decl_type_code
          ,data.soun_code
          ,data.soun
          ,2 as processingstage
          ,data.region
          ,data.region_code
          ,data.region_code||' '||data.region as region_name
		      ,(case	when v_byPurchase = 0 then data.nsa_order else data.nba_order end) as amountOrder-- индекс упорядочивания в рамках одного квартала от одного инн по суммам
          ,data.njsa
          ,data.njba
          ,data.before2015_amount
          ,data.after2015_amount
          ,data.nds_amount
          ,data.nds_discrep_amnt
          ,data.decl_sign_1
          ,data.decl_sign_2
          ,data.transit_1
          ,data.transit_2
          ,data.year_quarter_vat_list
          ,data.kpp_2_effective as kpp_effective
    from  HRZ_RELATIONS data, table(key_list) k
    where data.own_relation != 1 and
          ((v_byPurchase = 0 and (data.nsa + data.njsa) > 0) or (v_byPurchase <> 0 and (data.nba + data.njba) > 0)) and
          data.inn_1 = k.inn and
          data.inn_1 not in ('0000000001','0000000026','0000000020','0000000019') and
          data.year = v_year and
          data.quarter = v_period) tree -- дерево связей
   left join TAX_PAYER egrn 
     on egrn.inn = tree.inn_2
     and egrn.kpp_effective = tree.kpp_effective
   left join HRZ_DECL_INFO pr
   on pr.inn = tree.inn_1
   and pr.year = v_year
   and pr.quarter = v_period
   left join HRZ_RELATIONS_TOTAL summary
   on summary.year = v_year
	 and summary.quarter = v_period
	 and summary.inn = tree.inn_2
   left join   (select t.* from HRZ_RELATIONS t    where t.own_relation != 1) rev -- связи
     on rev.year = v_year
		 and rev.quarter = v_period
		 and rev.inn_1 = tree.inn_2
		 and rev.inn_2 = tree.inn_1
   left join HRZ_SUR SUR on SUR.INN = tree.inn_2 AND SUR.FISCAL_YEAR = v_year AND SUR.QUARTER = v_period 
   left join (select s.code as code
                     ,s.description as description
                     ,s.color_argb as color
                     ,s.is_default as is_default
              from dict_sur s) surd
   on surd.code = nvl(sur.sign_code, 4)
   where  v_byPurchase = 0 and -- от продавца
          tree.amountOrder <= maxContractors and
          (v_min_vat_share = 0  -- Ограничение по удельному весу НДС не задано
          or tree.amountOrder <= p_minReturnedContractors -- Ограничение по удельному весу НДС игнорируется, если элементов выбрано меньше минимального количества узлов
          or 100 * tree.nsa > v_min_vat_share * pr.ch9_nds) -- проверка Ограничения по удельному весу НДС
    or    v_byPurchase <> 0 and -- от покупателя
          tree.amountOrder <= maxContractors and
          (v_min_vat_share = 0 or
          tree.amountOrder <= p_minReturnedContractors or
          100 * tree.nba > v_min_vat_share * pr.ch8_nds)
   order by lvl, tree.inn_1, tree.amountOrder;
end;

end;
/