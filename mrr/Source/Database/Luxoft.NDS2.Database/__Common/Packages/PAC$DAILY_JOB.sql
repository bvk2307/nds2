﻿CREATE OR REPLACE PACKAGE NDS2_MRR_USER.PAC$DAILY_JOB
AS

function GET_DECL_REFRESH_STATUS return boolean; 

procedure SET_LOCK;
procedure REFRESH_DICTIONARY_FCOD;
procedure CLOSE_DISCREPANCY_KNP;
procedure REFRESH_AGGREGATES_KNP;
procedure REFRESH_DECLARATIONS;
procedure REFRESH_ANNULMENT_REQUESTS;
procedure PROCESS_CLAIMS_LIFE_CYCLE;
procedure AUTOCLOSE_RECLAIM;
procedure DECLARATION_SUMMARY_REPORT;
procedure RECALCULATE_SLIDE_AT;
procedure GENERATE_BANK_REQUEST_QUEUE;
procedure CLOSE_USER_TASKS;
procedure REFRESH_TABLE_STAT;
procedure RECOMPILE_OBJECTS_ASYNC;
procedure RELEASE_LOCK;

procedure Reserve_Start_Daily_Job;

END;
/
CREATE OR REPLACE PACKAGE BODY NDS2_MRR_USER.PAC$DAILY_JOB
AS
DECL_REFRESH_PARAM_NAME constant varchar(12):='decl_refresh';

function GET_DECL_REFRESH_STATUS return boolean
as
  v_retval number;
begin
    select cast(param_value as number) 
    into v_retval
    from STATE_DATA where param_name = DECL_REFRESH_PARAM_NAME;
    
  return v_retval = 1;
end;

procedure SET_DECL_REFRESH_STATUS (p_status boolean)
as
pragma autonomous_transaction;
    v_status varchar2(20);
begin
    if p_status then 
        v_status:= '1';
    else
        v_status:= '0';
    end if;
    
    update STATE_DATA 
    set param_value = v_status,
    ts = current_timestamp
    where param_name = DECL_REFRESH_PARAM_NAME;
  commit;
end;

procedure SET_LOCK as
begin
  PAC$LOGICAL_LOCK.P$SET_LOCK(1, 'DAILY_JOB');
    NDS2$SYS.LOG_INFO('BACKGROUND_DAILY_TASKS', 0, 0, 'P$DAILY_TASK_BEGIN');
end;

procedure REFRESH_DICTIONARY_FCOD as
begin
    NDS2$SYS.LOG_INFO('BACKGROUND_DAILY_TASKS', 0, 0, 'P$DICTIONARY');
    PAC$SYNC.START_WORK;
end;

procedure CLOSE_DISCREPANCY_KNP as
begin
  NDS2$SYS.LOG_INFO('BACKGROUND_DAILY_TASKS', 0, 0, 'PAC$KNP_DISCREPANCY');
    PAC$KNP_DISCREPANCY.P$CLOSE_DISCREPANCY_LIST;
end;

procedure REFRESH_AGGREGATES_KNP as
begin
    NDS2$SYS.LOG_INFO('BACKGROUND_DAILY_TASKS', 0, 0, 'P$KNP_AGGREGATES');
    PAC$NDS2_FULL_CYCLE.P$UPDATE_DICREPANCY_DAILY;
	PAC$KNP_DISCREPANCY.P$UPDATE_SOV_DISCREP_AGGREGATE;
end;

procedure REFRESH_DECLARATIONS as
begin
    SET_DECL_REFRESH_STATUS(true);
    NDS2$SYS.LOG_INFO('BACKGROUND_DAILY_TASKS', 0, 0, 'REFRESH_DECLARATONS');
    PAC$MVIEW_MANAGER.REFRESH_DECLARATONS;
    SET_DECL_REFRESH_STATUS(false);

    begin
	  execute immediate 'alter package PAC$DAILY_RESTORE compile';
      PAC$DAILY_RESTORE.P$RESTORE_ASSIGNMENTS;
    exception when others then 
        NDS2$SYS.LOG_ERROR('BACKGROUND_DAILY_TASKS', 0, 0, 'PAC$DAILY_RESTORE.P$RESTORE_ASSIGNMENTS: ERROR - '||substr(sqlerrm,1,700));
    end;

exception when others then
    NDS2$SYS.LOG_ERROR('BACKGROUND_DAILY_TASKS', 0, 0, 'REFRESH_DECLARATONS: ERROR - '||substr(sqlerrm,1,700));
    SET_DECL_REFRESH_STATUS(false);
end;

procedure REFRESH_ANNULMENT_REQUESTS as
begin
  NDS2$SYS.LOG_INFO('BACKGROUND_DAILY_TASKS', 0, 0, 'PAC$SEOD.P$REFRESH_ANNULMENT_REQUESTS');
  PAC$SEOD.P$REFRESH_ANNULMENT_REQUESTS;
exception when others then
  NDS2$SYS.LOG_ERROR('BACKGROUND_DAILY_TASKS', 0, 0, 'DECLARATION_ANNULMENT: ERROR - '||substr(sqlerrm,1,700));
  SET_DECL_REFRESH_STATUS(false);
end;

procedure PROCESS_CLAIMS_LIFE_CYCLE as
begin
  NDS2$SYS.LOG_INFO('BACKGROUND_DAILY_TASKS', 0, 0, 'PAC$CLAIM_LIFE_CYCLE.P$PROCESS_CLAIMS_LIFE_CYCLE');
  PAC$CLAIM_LIFE_CYCLE.P$PROCESS_CLAIMS_LIFE_CYCLE;
end;

procedure AUTOCLOSE_RECLAIM as
begin
    NDS2$SYS.LOG_INFO('BACKGROUND_DAILY_TASKS', 0, 0, 'PAC$RECLAIM.P$AUTO_CLOSE');
    PAC$RECLAIM.P$AUTO_CLOSE;
end;

procedure DECLARATION_SUMMARY_REPORT as
begin
    NDS2$SYS.LOG_INFO('BACKGROUND_DAILY_TASKS', 0, 0, 'P$DECLARATION_SUMMARY_REPORT');
    NDS2$REPORTS.START_DECLARATION_CALCULATE;
end;

procedure RECALCULATE_SLIDE_AT as
begin
    NDS2$SYS.LOG_INFO('BACKGROUND_DAILY_TASKS', 0, 0, 'P$RECALCULATE_SLIDE_AT');
    PAC$DAILY_RESTORE.RECALC_SLIDE_AT;
end;

procedure GENERATE_BANK_REQUEST_QUEUE as
begin
    NDS2$SYS.LOG_INFO('BACKGROUND_DAILY_TASKS', 0, 0, 'P$AUTO_QUEUE');
    PAC$BANK_ACCOUNTS.P$AUTO_QUEUE;
end;

procedure CLOSE_USER_TASKS as
begin
    NDS2$SYS.LOG_INFO('BACKGROUND_DAILY_TASKS', 0, 0, 'P$USER_TASK_DAILY_PROCESS');
    pac$user_task.p$run_daily_process;
end;

procedure REFRESH_TABLE_STAT as
begin
    NDS2$SYS.LOG_INFO('BACKGROUND_DAILY_TASKS', 0, 0, 'P$REFRESH_STAT');
    PAC$TABLE_STAT_MANAGER.REFRESH_STAT_ALL;
end;

procedure RECOMPILE_OBJECTS_ASYNC
as
v_job number;
pragma autonomous_transaction;
begin
  NDS2$SYS.LOG_INFO('BACKGROUND_DAILY_TASKS', 0, 0, 'P$RECOMPILE_OBJECTS_ASYNC');
  dbms_job.submit(what => 'begin NDS2$SYS.RECOMPILE_INVALID_OBJECTS; end;', force => true, job => v_job);
  dbms_job.submit(what => 'begin NDS2_SEOD.NDS2$SYS.RECOMPILE_INVALID_OBJECTS; end;', force => true, job => v_job);
  commit;
end;

procedure RELEASE_LOCK as
begin
  PAC$LOGICAL_LOCK.P$RELEASE_LOCK(1, 'DAILY_JOB');
    NDS2$SYS.LOG_INFO('BACKGROUND_DAILY_TASKS', 0, 0, 'P$DAILY_TASK_FINISH');
end;


-- запасная процедура выполнения дэйли-джоба, на случай проблемм с jenkins
-- после окончательного перехода на вызов отдельных процедур, эту нужно убрать
procedure Reserve_Start_Daily_Job as
begin
  
  --01. Захват блокировки работы дейли-джоба
  begin
  SET_LOCK;
  exception when others then
    NDS2$SYS.LOG_ERROR('BACKGROUND_DAILY_TASKS', 0, 0, 'P$DAILY_TASK_BEGIN: ERROR - '||substr(sqlerrm,1,700));
  end;

  --02. Обновление справочников ФЦОД
  begin
    REFRESH_DICTIONARY_FCOD;
  exception when others then
    NDS2$SYS.LOG_ERROR('BACKGROUND_DAILY_TASKS', 0, 0, 'P$DICTIONARY: ERROR - '||substr(sqlerrm,1,700));
  end;

  --03. Ручное закрытие КНП расхождений
  begin
    CLOSE_DISCREPANCY_KNP;
  exception when others then
    NDS2$SYS.LOG_ERROR('BACKGROUND_DAILY_TASKS', 0, 0, 'PAC$KNP_DISCREPANCY.P$CLOSE_DISCREPANCY_LIST: ERROR - '||substr(sqlerrm,1,700));
  end;

  --04. Пересоздание агрегатов с данными открытых расхождений для КНП
  begin
    REFRESH_AGGREGATES_KNP;
  exception when others then
    NDS2$SYS.LOG_ERROR('BACKGROUND_DAILY_TASKS', 0, 0, 'P$KNP_AGGREGATES: ERROR - '||substr(sqlerrm,1,700));
  end;

  --05. Пересоздание агрегатов с данными деклараций
  begin
    REFRESH_DECLARATIONS;
  end;
  
  --06. Процессинг аннулирования деклараций
  begin
  REFRESH_ANNULMENT_REQUESTS;
  end;

  --07. Жизненный цикл АТ (повторная отправка, закрытие)
  begin
    PROCESS_CLAIMS_LIFE_CYCLE;
  exception when others then
    NDS2$SYS.LOG_ERROR('BACKGROUND_DAILY_TASKS', 0, 0, 'PAC$CLAIM_LIFE_CYCLE.P$PROCESS_CLAIMS_LIFE_CYCLE: ERROR - '||substr(sqlerrm,1,700));
  end;  

  --08. Автозакрытие  АИ
  begin
    AUTOCLOSE_RECLAIM;
  exception when others then
    NDS2$SYS.LOG_ERROR('BACKGROUND_DAILY_TASKS', 0, 0, 'PAC$RECLAIM.P$AUTO_CLOSE: ERROR - '||substr(sqlerrm,1,700));
  end;

  --09. Построение отчета фоновые показатели
  begin    
    DECLARATION_SUMMARY_REPORT;
  exception when others then
    NDS2$SYS.LOG_ERROR('BACKGROUND_DAILY_TASKS', 0, 0, 'P$DECLARATION_SUMMARY_REPORT: ERROR - '||substr(sqlerrm,1,700));
  end;

  --10. Перерасчет количества просроченных отправок АТ
  begin
    RECALCULATE_SLIDE_AT;
  exception when others then
    NDS2$SYS.LOG_ERROR('BACKGROUND_DAILY_TASKS', 0, 0, 'P$RECALCULATE_SLIDE_AT: ERROR - '||substr(sqlerrm,1,700));
  end;

  --11. Автоматическое формирование очереди запросов в банк
  begin   
    GENERATE_BANK_REQUEST_QUEUE;
  exception when others then
    NDS2$SYS.LOG_ERROR('BACKGROUND_DAILY_TASKS', 0, 0, 'P$AUTO_QUEUE: ERROR - '||substr(sqlerrm,1,700));
  end;

  --12. Автозакрытие ПЗ
  begin    
    CLOSE_USER_TASKS;
  exception when others then
    NDS2$SYS.LOG_ERROR('BACKGROUND_DAILY_TASKS', 0, 0, 'P$USER_TASK_DAILY_PROCESS: ERROR - '||substr(sqlerrm,1,700));
  end;

  --13. Обновление статистики таблиц
  begin    
    REFRESH_TABLE_STAT;
  exception when others then
    NDS2$SYS.LOG_ERROR('BACKGROUND_DAILY_TASKS', 0, 0, 'P$REFRESH_STAT: ERROR - '||substr(sqlerrm,1,700));
  end;

  --14. Асинхронная перекомпиляция объектов
  begin    
    RECOMPILE_OBJECTS_ASYNC;
  exception when others then
    NDS2$SYS.LOG_INFO('BACKGROUND_DAILY_TASKS', 0, 0, 'P$RECOMPILE_OBJECTS_ASYNC: ERROR - '||substr(sqlerrm,1,700));
  end;

  --15. Освобождение блокировки работы дейли-джоба
  begin    
    RELEASE_LOCK;
  exception when others then
    NDS2$SYS.LOG_INFO('BACKGROUND_DAILY_TASKS', 0, 0, 'P$DAILY_TASK_FINISH: ERROR - '||substr(sqlerrm,1,700));
  end;
end;

END;
/
