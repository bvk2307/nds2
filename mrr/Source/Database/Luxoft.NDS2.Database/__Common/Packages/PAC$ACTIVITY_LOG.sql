﻿-- PAC$ACTIVITY_LOG specification
CREATE OR REPLACE PACKAGE NDS2_MRR_USER.PAC$ACTIVITY_LOG
  IS

  PROCEDURE WRITE(pSid        IN ACTIVITY_LOG.SID % TYPE,
                  pEntryId    IN ACTIVITY_LOG.ACTIVITY_ID % TYPE,
                  pElapsMilsc IN ACTIVITY_LOG.ELAPSED_MILLISEC % TYPE,
                  pCount      IN ACTIVITY_LOG.COUNT % TYPE);

  PROCEDURE PREPARE_REPORT(pDate IN DATE);

END;
/

-- PAC$ACTIVITY_LOG body
CREATE OR REPLACE PACKAGE BODY NDS2_MRR_USER.PAC$ACTIVITY_LOG
  IS

  PROCEDURE WRITE(pSid        IN ACTIVITY_LOG.SID % TYPE,
                  pEntryId    IN ACTIVITY_LOG.ACTIVITY_ID % TYPE,
                  pElapsMilsc IN ACTIVITY_LOG.ELAPSED_MILLISEC % TYPE,
                  pCount      IN ACTIVITY_LOG.COUNT % TYPE)
    AS
    BEGIN
      /*INSERT INTO NDS2_MRR_USER.ACTIVITY_LOG (
        ACTIVITY_ID, TIMESTAMP, ELAPSED_MILLISEC, COUNT, SID
      )
      VALUES (pEntryId, sysdate, pElapsMilsc, pCount, pSid);
      COMMIT;
    EXCEPTION
      WHEN OTHERS THEN Nds2$sys.LOG_ERROR('Activity report - Write', NULL, sqlcode, SUBSTR(sqlerrm, 1, 256));*/

	  null;
    END;

  PROCEDURE PREPARE_REPORT(pDate IN DATE)
    AS
      pHour                NUMBER;
      pDay                 DATE;
      pBegin               DATE;
      pEnd                 DATE;
      v_logons             NUMBER;
      v_decl_open          NUMBER;
      v_decl_ext_open      NUMBER;
      v_man_sel_call       NUMBER;
      v_reports            NUMBER;
      vx_max_user_for_hour NUMBER(10, 0);
    BEGIN
      pDay := TRUNC(pDate);
      pHour := 1 / 24;
      EXECUTE IMMEDIATE 'truncate table ACTIVITY_LOG_REPORT';
      FOR i IN 1 .. 24
      LOOP
        pBegin := pDay + (i - 1) * pHour;
        pEnd := pBegin + pHour;
        SELECT SUM(CASE WHEN al.activity_id = 2 THEN 1 ELSE 0 END) AS logons,
               SUM(CASE WHEN al.activity_id = 3 THEN 1 ELSE 0 END) AS decl_open,
               SUM(CASE WHEN al.activity_id = 4 THEN 1 ELSE 0 END) AS decl_ext_open,
               SUM(CASE WHEN al.activity_id = 5 THEN 1 ELSE 0 END) AS man_sel_call,
               SUM(CASE WHEN al.activity_id = 6 THEN 1 ELSE 0 END) AS reports
          INTO v_logons,
               v_decl_open,
               v_decl_ext_open,
               v_man_sel_call,
               v_reports
          FROM (SELECT DISTINCT sid,
                                activity_id,
                                count
              FROM activity_log
              WHERE TIMESTAMP BETWEEN pBegin AND pEnd) al;

        ---Общее кол-во пользователей имеющих активность на данный час
        SELECT COUNT(*)
          INTO vx_max_user_for_hour
          FROM (SELECT DISTINCT sid
              FROM activity_log
              WHERE TIMESTAMP BETWEEN pBegin AND pEnd);

        INSERT INTO ACTIVITY_LOG_REPORT
          SELECT pBegin,
                 pEnd,
                 v_logons,
                 SUM(CASE WHEN al.activity_id = 3 THEN 1 ELSE 0 END) AS decl_open,
                 SUM(CASE WHEN al.activity_id = 3 THEN al.COUNT ELSE 0 END) AS decl_size,
                 SUM(CASE WHEN al.activity_id = 4 THEN 1 ELSE 0 END) AS decl_ext_open,
                 SUM(CASE WHEN al.activity_id = 5 THEN 1 ELSE 0 END) AS man_sel_call,
                 SUM(CASE WHEN al.activity_id = 6 THEN 1 ELSE 0 END) AS reports,
                 SUM(CASE WHEN al.activity_id = 7 THEN al.elapsed_millisec ELSE 0 END) AS decl_time,
                 SUM(CASE WHEN al.activity_id = 8 THEN al.elapsed_millisec ELSE 0 END) AS decl_sov_time,
                 SUM(CASE WHEN al.activity_id = 9 THEN al.elapsed_millisec ELSE 0 END) AS decl_mrr_time,
                 SUM(CASE WHEN al.activity_id = 11 THEN al.elapsed_millisec ELSE 0 END) AS reports_time,
                 MIN(CASE WHEN al.activity_id = 7 THEN al.elapsed_millisec ELSE NULL END) AS decl_time_min,
                 MAX(CASE WHEN al.activity_id = 7 THEN al.elapsed_millisec ELSE NULL END) AS decl_time_max,
                 ROUND(AVG(CASE WHEN al.activity_id = 7 THEN al.elapsed_millisec ELSE NULL END)) AS decl_time_avg,
                 MIN(CASE WHEN al.activity_id = 3 THEN al.COUNT ELSE NULL END) AS decl_size_min,
                 MAX(CASE WHEN al.activity_id = 3 THEN al.COUNT ELSE NULL END) AS decl_size_max,
                 ROUND(AVG(CASE WHEN al.activity_id = 3 THEN al.COUNT ELSE NULL END)) AS decl_size_avg,
                 MIN(CASE WHEN al.activity_id = 8 THEN al.elapsed_millisec ELSE NULL END) AS decl_sov_time_min,
                 MAX(CASE WHEN al.activity_id = 8 THEN al.elapsed_millisec ELSE NULL END) AS decl_sov_time_max,
                 ROUND(AVG(CASE WHEN al.activity_id = 8 THEN al.elapsed_millisec ELSE NULL END)) AS decl_sov_time_avg,
                 MIN(CASE WHEN al.activity_id = 9 THEN al.elapsed_millisec ELSE NULL END) AS decl_mrr_time_min,
                 MAX(CASE WHEN al.activity_id = 9 THEN al.elapsed_millisec ELSE NULL END) AS decl_mrr_time_max,
                 ROUND(AVG(CASE WHEN al.activity_id = 9 THEN al.elapsed_millisec ELSE NULL END)) AS decl_mrr_time_avg,
                 MIN(CASE WHEN al.activity_id = 11 THEN al.elapsed_millisec ELSE NULL END) AS reports_time_min,
                 MAX(CASE WHEN al.activity_id = 11 THEN al.elapsed_millisec ELSE NULL END) AS reports_time_max,
                 ROUND(AVG(CASE WHEN al.activity_id = 11 THEN al.elapsed_millisec ELSE NULL END)) AS reports_time_avg,
                 MIN(CASE WHEN al.activity_id = 10 THEN al.elapsed_millisec ELSE NULL END) AS man_sel_time_min,
                 MAX(CASE WHEN al.activity_id = 10 THEN al.elapsed_millisec ELSE NULL END) AS man_sel_time_max,
                 ROUND(AVG(CASE WHEN al.activity_id = 10 THEN al.elapsed_millisec ELSE NULL END)) AS man_sel_time_avg,
                 v_decl_open,
                 v_decl_ext_open,
                 v_man_sel_call,
                 v_reports,
                 vx_max_user_for_hour
            FROM activity_log al
            WHERE TIMESTAMP BETWEEN pBegin AND pEnd;
      END LOOP;
      COMMIT;
    EXCEPTION
      WHEN OTHERS THEN Nds2$sys.LOG_ERROR('Activity report - Prepare', NULL, sqlcode, SUBSTR(sqlerrm, 1, 256));
    END;

END;
/