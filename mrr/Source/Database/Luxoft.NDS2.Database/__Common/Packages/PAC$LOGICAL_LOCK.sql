﻿CREATE OR REPLACE PACKAGE NDS2_MRR_USER.PAC$LOGICAL_LOCK
as

TYPE_SEOD_INCOME CONSTANT NUMBER(1) := 1;
LOCKED_BY_SEOD_INCOME CONSTANT VARCHAR2(32 char) := 'PROCESS_SEOD_INCOME';

PROCEDURE P$SET_LOCK
(
   pLockId in number,
   pLockedBy in varchar2
);

PROCEDURE P$RELEASE_LOCK
(
   pLockId in number,
   pLockedBy in varchar2
);

FUNCTION P$HAS_ANY_LOCK
(
   pLockId in number
)
return boolean;

FUNCTION P$HAS_LOCK_OTHER
(
   pLockId in number,
   pLockedBy in varchar2
)
return boolean;

END;
/

CREATE OR REPLACE PACKAGE BODY NDS2_MRR_USER.PAC$LOGICAL_LOCK
as

PROCEDURE P$SET_LOCK
(
   pLockId in number,
   pLockedBy in varchar2
)
as
pragma autonomous_transaction;
begin

  insert into LOGICAL_LOCK(lock_id, is_locked, when_is_locked, locked_by, when_is_released) 
  values(pLockId, pLockId, SYSTIMESTAMP, pLockedBy, null);

  commit;
  
end;


PROCEDURE P$RELEASE_LOCK
(
   pLockId in number,
   pLockedBy in varchar2
)
as
pragma autonomous_transaction;
begin

  update LOGICAL_LOCK t set t.is_locked = 0, t.when_is_released = SYSTIMESTAMP
  where t.lock_id = pLockId and t.locked_by = pLockedBy;
  
  commit;

end;


FUNCTION P$HAS_ANY_LOCK
(
   pLockId in number
)
return boolean
is
   hasAnyLock number(1);
begin

   hasAnyLock := 0;
   
   begin
     select 1 into hasAnyLock from LOGICAL_LOCK t
     where t.lock_id = pLockId and 
		   t.is_locked = 1 and
		   rownum = 1;
   exception when no_data_found then
     hasAnyLock := 0;
   end;
  
   return case when hasAnyLock = 1 then true else false end;
end;

FUNCTION P$HAS_LOCK_OTHER
(
   pLockId in number,
   pLockedBy in varchar2
)
return boolean
is
   hasLock number(1);
begin

   hasLock := 0;

   begin
     select 1 into hasLock from LOGICAL_LOCK t
     where t.lock_id = pLockId and 
		   t.locked_by <> pLockedBy and 
		   t.is_locked = 1 and
		   rownum = 1;
   exception when no_data_found then
     hasLock := 0;
   end;

   return case when hasLock = 1 then true else false end;
end;

END;
/
