﻿create or replace package  NDS2_MRR_USER.PAC$DECLARATION is

/*Назначает НД на инспектора*/
procedure P$ASSIGN_INSPECTOR(
    p_inn_declarant varchar2,
    p_kpp_effective varchar2,
    p_period_effective number,
    p_fiscal_year number,
    p_type_code number,
    p_assigned_to_id number,
    p_assigned_by_id number
  );
/*Снимает назначение НД с инспектора */
procedure P$UNASSIGN_INSPECTOR(
	p_inn_declarant varchar2,
	p_kpp_effective varchar2,
	p_period_effective number,
	p_fiscal_year number,
	p_type_code number);

procedure P$GET_COUNT_DOC_KNP
(
  pInnContractor in varchar2,
  pKppEffective in varchar2,
  pType in number,
  pPeriod in varchar2,
  pYear in varchar2,
  pTotalRowsCount out NUMBER
);

procedure P$GET_DECLARATION(pZip in number, pCursor out SYS_REFCURSOR);

procedure P$GET_ACTUAL_DECLARATION(pZip in number, pCursor out SYS_REFCURSOR);

procedure UPDATE_CONTRACTOR_AGGR_START;

procedure P$UPDATE_CONTRACTOR_AGGR;

procedure P$GET_CONTRACTOR_DECL_ZIP(
  pInn in number,
  pYear in varchar2,
  pMonth in dict_tax_period_month.month%type,
  pCursor out SYS_REFCURSOR
  );

procedure P$BOOK_DATA_REQUEST_STATUS (
  pRequestId in BOOK_DATA_REQUEST.ID%type,
  pData out SYS_REFCURSOR
  ); -- Возвращает статус запроса получения данных книги декларации

procedure P$BOOK_DATA_REQUEST_EXISTS (
  pRequestId out BOOK_DATA_REQUEST.ID%type,
  pStatus out BOOK_DATA_REQUEST.Status%type,
  pInn in BOOK_DATA_REQUEST.INN%type,
  pCorrectionNumber in BOOK_DATA_REQUEST.CORRECTIONNUMBER%type,
  pPartitionNumber in BOOK_DATA_REQUEST.PARTITIONNUMBER%type,
  pPeriod in BOOK_DATA_REQUEST.PERIOD%type,
  pYear in BOOK_DATA_REQUEST.YEAR%type
 ); -- Возвращает идентификатор запроса данных с/ф по параметрам запроса


 procedure P$NDSDISCREPANCY_REQ_STATUS (
  pRequestId in DISCREPANCY_REQUEST.ID%type,
  pData out SYS_REFCURSOR
  ); -- Возвращает статус запроса расхождений по декларации


procedure P$NDSDISCREPANCY_REQ_EXISTS (
  pRequestId out DISCREPANCY_REQUEST.ID%type,
  pInn in DISCREPANCY_REQUEST.INN%type,
  pCorrectionNumber in DISCREPANCY_REQUEST.CORRECTION_NUMBER%type,
  pPeriod in DISCREPANCY_REQUEST.PERIOD%type,
  pYear in DISCREPANCY_REQUEST.YEAR%type,
  pKind in DISCREPANCY_REQUEST.KIND%type,
  pType in DISCREPANCY_REQUEST.TYPE%type
 ); -- Возвращает идентификатор запроса расхождений по декларации

procedure P$GET_ACT_KNP(
  pZip in number,
  pCursor out SYS_REFCURSOR);

procedure P$GET_SEOD_DECISION(
  pInnDeclarant in varchar2,
  pInn in varchar2,
  pKppEffective in varchar2,
  pPeriodCode in varchar2,
  pFiscalYear in varchar2,
  pResult out SYS_REFCURSOR);

procedure P$GET_DECLARATION_OWNER(
  P_INN_CONTRACTOR in varchar2,
  P_KPP_EFFECTIVE in varchar2,
  P_TYPE in number,
  P_PERIOD in varchar2,
  P_YEAR in varchar2,
  pCursor out SYS_REFCURSOR);

procedure P$SOV_INV_CACHE_CLEAR_START;

procedure P$GET_VERSIONS
(
  pInnDeclarant in varchar2,
  pInnContractor in varchar2,
  pKppEffective in varchar2,
  pPeriodEffective in number,
  pYear in varchar2,
  pCursor out SYS_REFCURSOR
);

procedure P$GET_ALL_ZIPS
(
  VP_Zip in number,
  VP_Cursor out SYS_REFCURSOR
);

procedure P$GET_CHAPTER_ACTUAL_ZIPS
(
  VP_Zip in number,
  VP_Cursor out SYS_REFCURSOR
);

procedure P$GET_CHAPTER_ACTUAL_ZIP
(
  P_Zip in number,
  P_Chapter in int,
  P_ActualZip out number
);

end PAC$DECLARATION;
/
create or replace package body NDS2_MRR_USER.PAC$DECLARATION is

DECL_REFRESH_PARAM_NAME constant varchar(12):='decl_refresh';

procedure UTL_LOG
  (
    v_source varchar2,
    v_code varchar2 := 0,
    v_msg varchar2 := null,
    v_entity_id varchar2 := null
  )
as
pragma autonomous_transaction;
begin
  insert into system_log(id, type, site, entity_id, message_code, message, log_date)
  values(seq_sys_log_id.nextval, 0, v_source, v_entity_id, v_code, v_msg, sysdate);
  commit;
exception when others then
  rollback;
end;

function F$GET_DECL_REFRESH_STATUS return boolean
as
	v_retval number;
begin
    select cast(param_value as number) 
    into v_retval
    from STATE_DATA where param_name = DECL_REFRESH_PARAM_NAME;
    
	return v_retval = 1;
end;

procedure P$ASSIGN_INSPECTOR(
    p_inn_declarant varchar2,
    p_kpp_effective varchar2,
    p_period_effective number,
    p_fiscal_year number,
    p_type_code number,
    p_assigned_to_id number,
    p_assigned_by_id number)
  as
    type decl_col is table of number;
    col_decl_zip decl_col;
    v_assigned_to_sid mrr_user.SID%type;
    v_assigned_to_name mrr_user.Name%type;
begin
 -- Лочим декларации которые будем обновлять
     select da.zip
     bulk collect into col_decl_zip
     from declaration_active da
     left join declaration_active_search dsa
     on da.zip = dsa.declaration_version_id
     where
        da.inn_declarant = p_inn_declarant
        and da.kpp_effective =  p_kpp_effective
        and da.type_code = p_type_code
        and da.period_effective = p_period_effective
        and da.fiscal_year = p_fiscal_year
        for update of da.zip, dsa.declaration_version_id;

    if(col_decl_zip.count = 0) then
        raise no_data_found;
    end if;

    --Узнаем имя и SID инспектора на которого назначили декларацию
    select  u.Name,
            u.SID
    into    v_assigned_to_name,
            v_assigned_to_sid
    from nds2_mrr_user.mrr_user u
    where u.id = p_assigned_to_id;

	--помечаем текущее назначение как неактивное
    update declaration_assignment
       set is_actual = 0
    where inn_declarant         = p_inn_declarant
	    and kpp_effective         = p_kpp_effective
	    and period_effective      = p_period_effective
	    and fiscal_year           = p_fiscal_year
      and declaration_type_code = p_type_code
      and is_actual             = 1;

	-- Вставлям назначение
    insert into declaration_assignment(
        inn_declarant,
        kpp_effective,
        period_effective,
        fiscal_year,
        declaration_type_code,
        assigned_by,
        assigned_to,
        assigned_at,
		is_actual)
    select
        p_inn_declarant
        ,p_kpp_effective
        ,p_period_effective
        ,p_fiscal_year
        ,p_type_code
        ,p_assigned_by_id
        ,p_assigned_to_id
        ,current_timestamp
        ,1
    from dual;

    -- Если происходит обновление агрегатов деклараций, то пишем в буфер
    if(F$GET_DECL_REFRESH_STATUS) then
    FORALL i IN 1..col_decl_zip.count
        insert into tmp_declaration_assignment(zip, assigned_to_name, assigned_to_sid)
        values(col_decl_zip(i),v_assigned_to_name , v_assigned_to_sid);
    end if;

    -- Обновляем залоченные Декларации
    FORALL i IN 1 .. col_decl_zip.count
        update declaration_active  da
        set da.INSPECTOR = v_assigned_to_name,
            da.INSPECTOR_SID = v_assigned_to_sid
         where
            da.zip = col_decl_zip(i);

    -- Обновляем залоченные Декларации в поисковом агрегате
    FORALL i IN 1 .. col_decl_zip.count
        update declaration_active_search  dsa
        set dsa.assignee_name = v_assigned_to_name,
            dsa.assignee_sid = v_assigned_to_sid
        where
            dsa.declaration_version_id = col_decl_zip(i);

    --переназначим ПЗ
    Pac$user_Task.P$ASSIGN_USER_BY_DECL(
        p_inn_declarant,
        p_kpp_effective,
        p_period_effective,
        p_fiscal_year,
        p_type_code,
        v_assigned_to_sid,
        v_assigned_to_name);
 end;

 procedure P$UNASSIGN_INSPECTOR(
    p_inn_declarant varchar2,
    p_kpp_effective varchar2,
    p_period_effective number,
    p_fiscal_year number,
    p_type_code number)
as
type t$col_number is table of number not null;
v_col_zip t$col_number;
begin
  update declaration_assignment
	set is_actual = 0
  where inn_declarant         = p_inn_declarant
	  and kpp_effective         = p_kpp_effective
	  and period_effective      = p_period_effective
	  and fiscal_year           = p_fiscal_year
    and declaration_type_code = p_type_code
    and is_actual             = 1;
	  
	insert into declaration_assignment(
        inn_declarant,
        kpp_effective,
        period_effective,
        fiscal_year,
        declaration_type_code,
        assigned_at,
		is_actual)
    select
        p_inn_declarant
        ,p_kpp_effective
        ,p_period_effective
        ,p_fiscal_year
        ,p_type_code
        ,current_timestamp
        ,1
    from dual;

    select distinct zip
    bulk collect into v_col_zip
    from declaration_active
    where
        inn_declarant = p_inn_declarant
        and kpp_effective = p_kpp_effective
        and period_effective = p_period_effective
        and fiscal_year = p_fiscal_year
        and type_code = p_type_code;

    forall i in 1.. v_col_zip.count
        update declaration_active
         set inspector = null,
            inspector_sid = null
        where zip = v_col_zip(i);

    forall i in 1.. v_col_zip.count
        update declaration_active_search
         set assignee_name = null,
            assignee_sid = null
        where declaration_version_id = v_col_zip(i);
end;

procedure P$GET_VERSIONS
(
  pInnDeclarant in varchar2,
  pInnContractor in varchar2,
  pKppEffective in varchar2,
  pPeriodEffective in number,
  pYear in varchar2,
  pCursor out SYS_REFCURSOR
)
as
begin
 OPEN pCursor FOR
 select
	DECL.reg_number as RegistrationNumber,
	DECL.SUBMISSION_DATE as SubmittedAt,
	to_number(DECL.is_active) as Status,
	DECL.INN_REORGANIZED as InnReorganized,
	DECL.SONO_CODE_SUBMITED as SonoCodeSubmitted,
	DECL.correction_number as CorrectionNumber,
	ds.calc_status as KnpCloseReasonId,
    case when decl_anl.ID is not null then 1 else 0 end as IsAnnulmentSeod,
    case when decl_anl.STATUS_ID = 2 then 1 else 0 end as IsAnnulment
 from DECLARATION_HISTORY DECL
	left join DECLARATION_KNP_STATUS ds on ds.zip = decl.zip
    left join V$DECLARATION_ANNULMENT decl_anl on
      decl.INN_DECLARANT = decl_anl.INN and
      decl.KPP = decl_anl.KPP and
      decl.SONO_CODE_SUBMITED = decl_anl.SONO_CODE and
      decl.FISCAL_YEAR = decl_anl.FISCAL_YEAR and
      decl.PERIOD_CODE = decl_anl.PERIOD_CODE and
      decl.CORRECTION_NUMBER = decl_anl.CORRECTION_NUMBER
 where DECL.INN_DECLARANT = pInnDeclarant
	and ((pInnContractor is not null and DECL.INN_CONTRACTOR = pInnContractor) or pInnContractor is null)
	and DECL.KPP_EFFECTIVE = pKppEffective and DECL.TYPE_CODE = 0
    and DECL.PERIOD_EFFECTIVE = pPeriodEffective and DECL.FISCAL_YEAR = pYear;
end;

procedure P$GET_COUNT_DOC_KNP
(
  pInnContractor in varchar2,
  pKppEffective in varchar2,
  pType in number,
  pPeriod in varchar2,
  pYear in varchar2,
  pTotalRowsCount out NUMBER
)
as
  pCount01 NUMBER;
  pCount02 NUMBER;
  pCount03 NUMBER;
  pCount04 NUMBER;
  pCount05 NUMBER;
begin
  select count(1) into pCount01
  from DECLARATION_HISTORY DECL
  where DECL.INN_CONTRACTOR = pInnContractor and DECL.KPP_EFFECTIVE = pKppEffective
    and DECL.TYPE_CODE = pType and DECL.PERIOD_CODE = pPeriod and DECL.FISCAL_YEAR = pYear;

  select count(1) into pCount02
  from V$DOC doc
       inner join DECLARATION_HISTORY decl on DECL.REG_NUMBER = doc.ref_entity_id and decl.SONO_CODE_SUBMITED = doc.SONO_CODE
       inner join DOC_TYPE on DOC_TYPE.ID = DOC.Doc_Type
       left join DOC_STATUS on (DOC_STATUS.ID = DOC.STATUS) and (DOC_STATUS.DOC_TYPE = DOC.DOC_TYPE)
  where DECL.INN_CONTRACTOR = pInnContractor and DECL.KPP_EFFECTIVE = pKppEffective
    and DECL.TYPE_CODE = pType and DECL.PERIOD_CODE = pPeriod and DECL.FISCAL_YEAR = pYear AND DOC.DOC_TYPE <> 5;


  select count(1) into pCount03
  from V$DOC doc
    inner join DECLARATION_HISTORY decl on DECL.REG_NUMBER = doc.ref_entity_id and DECL.SONO_CODE_SUBMITED = doc.SONO_CODE
    inner join DOC_TYPE on DOC_TYPE.ID = DOC.Doc_Type
          inner join
          (
                select
                  vd.inn_contractor,
                  vd.kpp_effective,
                  vd.type_code,
                  vd.period_code,
                  vd.fiscal_year,
				  vd.zip
                from DECLARATION_HISTORY vd
                join v$askkontrsoontosh ks on (ks.IdDekl = vd.ask_id) and ks.DeleteDate is null
                where vd.INN_CONTRACTOR = pInnContractor and vd.KPP_EFFECTIVE = pKppEffective
                  and vd.TYPE_CODE = pType and vd.PERIOD_CODE = pPeriod and vd.FISCAL_YEAR = pYear
                group by vd.inn_contractor, vd.kpp_effective, vd.type_code, vd.period_code, vd.fiscal_year, vd.zip
                having sum(case when ks.Vypoln = 0 then 1 else 0 end) > 0
          ) T1 on T1.INN_CONTRACTOR = DECL.INN_CONTRACTOR and T1.KPP_EFFECTIVE = DECL.KPP_EFFECTIVE
              and T1.TYPE_CODE = DECL.TYPE_CODE and T1.PERIOD_CODE = DECL.PERIOD_CODE and T1.FISCAL_YEAR = DECL.FISCAL_YEAR and T1.ZIP = DECL.ZIP
  where DECL.INN_CONTRACTOR = pInnContractor and DECL.KPP_EFFECTIVE = pKppEffective
    and DECL.TYPE_CODE = pType and DECL.PERIOD_CODE = pPeriod and DECL.FISCAL_YEAR = pYear AND DOC.DOC_TYPE = 5;


  select count(1) into pCount04
  from V$SEOD_EXPLAIN_REPLY REPL
    inner join V$DOC doc on DOC.DOC_ID = REPL.DOC_ID
    inner join declaration_history DECL on DECL.Reg_Number = doc.ref_entity_id and decl.SONO_CODE_SUBMITED = doc.SONO_CODE
    inner join DICT_EXPLAIN_REPLY_TYPE dert on dert.id = REPL.TYPE_ID
    inner join DICT_EXPLAIN_REPLY_STATUS ders on ders.id = REPL.status_id
  where DECL.INN_CONTRACTOR = pInnContractor and DECL.KPP_EFFECTIVE = pKppEffective
    and DECL.TYPE_CODE = pType and DECL.PERIOD_CODE = pPeriod and DECL.FISCAL_YEAR = pYear;

  select count(1) into pCount05
  from NDS2_SEOD.SEOD_KNP
    inner join NDS2_SEOD.SEOD_KNP_DECISION on SEOD_KNP_DECISION.KNP_ID = SEOD_KNP.KNP_ID
    inner join NDS2_SEOD.SEOD_KNP_DECISION_TYPE on SEOD_KNP_DECISION_TYPE.ID = SEOD_KNP_DECISION.TYPE_ID
    inner join declaration_history DECL on DECL.Reg_Number = SEOD_KNP.DECLARATION_REG_NUM and decl.sono_code = seod_knp.ifns_code
    where DECL.INN_CONTRACTOR = pInnContractor and DECL.KPP_EFFECTIVE = pKppEffective
      and DECL.TYPE_CODE = pType and DECL.PERIOD_CODE = pPeriod and DECL.FISCAL_YEAR = pYear;

  if (pCount05 > 0) then
    pCount05 := pCount05 + 1;
  end if;


  pTotalRowsCount := nvl(pCount01, 0) + nvl(pCount02, 0) + nvl(pCount03, 0) +
                     nvl(pCount04, 0) + nvl(pCount05, 0);

end;

procedure P$GET_DECLARATION(
  pZip in number,
  pCursor out SYS_REFCURSOR)
as
  v_act_id number;
  v_decision_id number;
  v_act_closed number;
  v_decision_closed number;
  v_has_knp_discrepancy number;
  v_has_seod_decision number;
  v_knp_id number;
  v_knp_closed_at date;
  v_has_act_discrepancy number;
  v_has_decision_discrepancy number;
begin

  begin
    select
       a.id
      ,case when a.close_date is null then 0 else 1 end
      ,dc.id
      ,case when dc.close_date is null then 0 else 1 end
    into
       v_act_id
      ,v_act_closed
      ,v_decision_id
      ,v_decision_closed
    from ASK_DECLANDJRNL d
    join ACT a
         on    a.inn_declarant = d.innnp
           and a.inn = nvl(d.innreorg, d.innnp)
           and a.kpp_effective = d.kpp_effective
           and a.period_code = d.period
           and a.fiscal_year = d.otchetgod
    left join DECISION dc
         on    dc.inn_declarant = d.innnp
           and dc.inn = nvl(d.innreorg, d.innnp)
           and dc.kpp_effective = d.kpp_effective
           and dc.period_code = d.period
           and dc.fiscal_year = d.otchetgod
    where d.zip = pZip;

    exception when NO_DATA_FOUND then
      begin
        v_act_closed := 0;
        v_decision_closed := 0;
      end;
  end;

  begin
    select 1
		into v_has_knp_discrepancy
		from KNP_DISCREPANCY_DECLARATION knp
		where knp.ZIP = pZip and knp.DISCREPANCY_STATUS = 1 and rownum = 1;

    exception 
		when NO_DATA_FOUND then v_has_knp_discrepancy := 0;
  end;

  if v_act_id is not null and v_has_knp_discrepancy = 1 then
  begin
    select 1
    into v_has_act_discrepancy
    from ACT_DISCREPANCY
    where act_id = v_act_id and status = 1 and rownum = 1;
    exception when NO_DATA_FOUND then v_has_act_discrepancy := 0;
  end;
  else
    v_has_act_discrepancy := 0;
  end if;

  if v_decision_id is not null and v_has_act_discrepancy = 1 then
  begin
    select 1
    into v_has_decision_discrepancy
    from DECISION_DISCREPANCY
    where decision_id = v_decision_id and status = 1 and rownum = 1;
    exception when NO_DATA_FOUND then v_has_decision_discrepancy := 0;
  end;
  else
    v_has_decision_discrepancy := 0;
  end if;

  if v_decision_id is not null and v_has_decision_discrepancy = 0 then
  begin
    select 1 into v_has_seod_decision
    from
      ask_declandjrnl d_ask
	  join dict_tax_period dtp
		on d_ask.period = dtp.code
      join declaration_history d_hist
        on    d_hist.inn_declarant = d_ask.innnp
          and d_hist.inn_contractor = nvl(d_ask.innreorg, d_ask.innnp)
		  and d_hist.kpp_effective = d_ask.kpp_effective
          and d_hist.fiscal_year = d_ask.otchetgod
          and d_hist.period_effective = dtp.effective_value
          and d_hist.type_code = d_ask.type
      join nds2_seod.seod_knp knp
        on    knp.declaration_reg_num = d_hist.reg_number
          and knp.ifns_code = d_hist.sono_code_submited
      join nds2_seod.seod_knp_decision dc on dc.knp_id = knp.knp_id
    where d_ask.zip = pZip and rownum = 1;
    exception when NO_DATA_FOUND then v_has_seod_decision := 0;
  end;
  else
    v_has_seod_decision := 0;
  end if;

  begin
    select
       max(knp.knp_id) as v_knp_id
      ,max(case when d_hist.is_active = 1 then knp.completion_date else null end)
    into
       v_knp_id
      ,v_knp_closed_at
    from
      ASK_DECLANDJRNL d_ask
	  join dict_tax_period dtp
		on d_ask.period = dtp.code
      join DECLARATION_HISTORY d_hist
        on    d_hist.inn_contractor = nvl(d_ask.innreorg, d_ask.innnp)
          and d_hist.inn_declarant = d_ask.innnp
          and d_hist.kpp_effective = d_ask.kpp_effective
          and d_hist.period_effective = dtp.effective_value
          and d_hist.fiscal_year = d_ask.otchetgod
          and d_hist.type_code = d_ask.type
      join nds2_seod.SEOD_KNP knp
        on    knp.declaration_reg_num = d_hist.reg_number
          and knp.ifns_code = d_hist.sono_code_submited
    where d_ask.zip = pZip;
    exception when NO_DATA_FOUND then
      begin
        v_knp_id := null;
        v_knp_closed_at := null;
      end;
    end;

  open pCursor for
    with decl_successor_decl_summary as
    (
    select
    pZip as zip
    ,dh.zip as concrete_zip
    ,summary.*
    from v$ask_declandjrnl mc
	inner join dict_tax_period dtp
	  on mc.period = dtp.code
    inner join declaration_history dh
      on dh.inn_declarant = mc.INNNP
      and dh.period_effective = dtp.effective_value
      and dh.fiscal_year = to_number(mc.OTCHETGOD)
    inner join declaration_journal_summary summary on summary.version_id = dh.zip
    where mc.zip = pZip
    and dh.inn_contractor = dh.inn_declarant and dh.inn_reorganized is null and rownum = 1 --нужна сводная только по правопреемнику.
    )
    select
      decl.ZIP as ID
     ,decl.ZIP
     ,decl.ZIP as DECLARATION_VERSION_ID
     ,2 as load_mark
     ,decl.reg_number as SEOD_DECL_ID
     ,ask.id as ASK_DECL_ID
     ,ask.versform as XmlVersion
     ,decode(ask.prizn_akt_korr, 1, '1', '0') as is_active
     ,decl.inn_declarant as inn
     ,decl.inn_contractor
     ,decl.kpp
     ,decl.kpp_effective
     ,tp.name_short as name
     ,tp.address as address1
     ,tp.address as address2
     ,tp.is_large as category
     ,decode(tp.is_large, 1, 'Крупнейший', '') as category_ru
     ,dreg.s_name as region_name
     ,decl.region_code
     ,dsono.s_name as soun
     ,dsono.s_name as soun_name
     ,decl.sono_code as soun_code
     ,decl.submit_date as reg_date
     ,decl.submission_date as submission_date
     ,null as TAX_MODE
     ,ask.okved as okved_code
     ,nvl(tp.ust_capital, 0) as capital
     ,ask.period as tax_period
	 ,decl.period_effective as periodEffective
     ,ask.otchetgod as fiscal_year
     ,dperiod.description || ' ' || ask.otchetgod as full_tax_period
     ,ask.datadok as decl_date
     ,ask.type as decl_type_code
     ,decode(ask.nd_prizn, 1, 'К уплате', 2, 'К возмещению', 3, 'Нулевая', '') as decl_sign
     ,decode(ask.type, 0, 'Декларация', 'Журнал') as decl_type
     ,ask.nomkorr as correction_number
     ,ask.nomkorr_rank as correction_number_rank
     ,decl.correction_number_effective
     ,ask.familiyapodp||' '||ask.imyapodp||' '||ask.otchestvopodp as subscriber_name
     ,DECODE(ask.prpodp, 1, 'Законный представитель', 2, 'Уполномоченный представитель', '') as prpodp
     ,ask.summands as compensation_amnt
     ,ask.nd_prizn as compensation_amnt_sign
     ,ask.nds_weight
     ,0 as NDS_INCREASE_DISCREP_COUNT
     ,0 as NDS_INCREASE_DISCREP_AMNT
     ,ins_ass.SID as INSPECTORSID
     ,ins_ass.NAME as inspector
     ,decl.sur_code
     ,null as CONTROL_RATIO_DATE
     ,0 as CONTROL_RATIO_SEND_TO_SEOD
     ,0 as CONTROL_RATIO_COUNT
     ,0 as CONTROL_RATIO_DISCREP_COUNT
     ,case when v_knp_id is not null then 1 else 0 end as status
     ,dsd.SumNDSPok_8
     ,dsd.SumNDSPok_81
     ,dsd.SumNDSPokDL_81
     ,dsd.StProd18_9
     ,dsd.StProd10_9
     ,dsd.StProd0_9
     ,dsd.SumNDSProd18_9
     ,dsd.SumNDSProd10_9
     ,dsd.StProdOsv_9
     ,dsd.StProd18_91
     ,dsd.StProd10_91
     ,dsd.StProd0_91
     ,dsd.SumNDSProd18_91
     ,dsd.SumNDSProd10_91
     ,dsd.StProdOsv_91
     ,dsd.StProd18DL_91
     ,dsd.StProd10DL_91
     ,dsd.StProd0DL_91
     ,dsd.SumNDSProd18DL_91
     ,dsd.SumNDSProd10DL_91
     ,dsd.StProdOsvDL_91
     ,dsd.AKT_NOMKORR_8
     ,dsd.AKT_NOMKORR_81
     ,dsd.AKT_NOMKORR_9
     ,dsd.AKT_NOMKORR_91
     ,dsd.AKT_NOMKORR_10
     ,dsd.AKT_NOMKORR_11
     ,dsd.AKT_NOMKORR_12
     ,nvl(dsd.StProd, 0) as StProd
     ,v_knp_closed_at as DateCloseKNP
     ,dsd.invoice_count8
     ,dsd.invoice_count81
     ,dsd.invoice_count9
     ,dsd.invoice_count91
     ,nvl(zhd.ch_1_count_total, dsd.invoice_count10) as invoice_count10
     ,nvl(zhd.ch_2_count_total, dsd.invoice_count11) as invoice_count11
     ,dsd.invoice_count12
     ,nvl(rp08.priority, 1) as priority_08
     ,nvl(rp81.priority, 1) as priority_81
     ,nvl(rp09.priority, 1) as priority_09
     ,nvl(rp91.priority, 1) as priority_91
     ,nvl(rp10.priority, 1) as priority_10
     ,nvl(rp11.priority, 1) as priority_11
     ,nvl(rp12.priority, 1) as priority_12
     ,req8.id  as request_key8
     ,req81.id as request_key81
     ,req9.id  as request_key9
     ,req91.id as request_key91
     ,req10.id as request_key10
     ,req11.id as request_key11
     ,req12.id as request_key12
     ,dsd.actual_zip8
     ,dsd.actual_zip81
     ,dsd.actual_zip9
     ,dsd.actual_zip91
     ,decode(decl.Type_Code, 0, dsd.actual_zip10, ask.zip) as actual_zip10
     ,decode(decl.Type_Code, 0, dsd.actual_zip11, ask.zip) as actual_zip11
     ,dsd.actual_zip12
      ,2 as ProcessingStage
      ,case when decl.zip = summary.version_id or decl.correction_number <= summary.correction_number then 2 else case when summary.version_id is null then 0 else 1 end end as SummaryState
      ,summary.ch8_deals_amnt as CH8_DEALS_AMNT_TOTAL
      ,summary.ch9_deals_amnt as CH9_DEALS_AMNT_TOTAL
      ,summary.ch8_nds
      ,summary.ch9_nds
      ,nvl(summary.DISCREP_CURRENCY_AMNT, 0) as DISCREP_CURRENCY_AMNT
      ,nvl(summary.DISCREP_CURRENCY_COUNT, 0) as DISCREP_CURRENCY_COUNT
      ,nvl(summary.discrep_weak_count, 0) as WEAK_DISCREP_COUNT
      ,nvl(summary.discrep_weak_amnt, 0) as WEAK_DISCREP_AMNT
      ,nvl(summary.discrep_gap_count, 0) as GAP_DISCREP_COUNT
      ,nvl(summary.discrep_gap_amnt, 0) as GAP_DISCREP_AMNT
      ,nvl(summary.discrep_total_count, 0) as TOTAL_DISCREP_COUNT
      ,nvl(summary.discrep_total_amnt, 0) as DISCREP_TOTAL_AMNT
      ,nvl(summary.discrep_min_amnt, 0) as DISCREP_MIN_AMNT
      ,nvl(summary.discrep_max_amnt, 0) as DISCREP_MAX_AMNT
      ,nvl(summary.discrep_avg_amnt, 0) as DISCREP_AVG_AMNT
      ,summary.update_date
      ,nvl(summary.ch8_discrep_amnt, 0) as DISCREP_BUY_BOOK_AMNT
      ,nvl(summary.ch9_discrep_amnt, 0) as DISCREP_SELL_BOOK_AMNT
      ,nvl(summary.ch8_pvp_amnt, 0) as PVP_BUY_BOOK_AMNT
      ,nvl(summary.ch9_pvp_amnt, 0) as PVP_SELL_BOOK_AMNT
      ,nvl(summary.pvp_total_amnt, 0) as PVP_TOTAL_AMNT
      ,nvl(summary.pvp_min_amnt, 0) as PVP_DISCREP_MIN_AMNT
      ,nvl(summary.pvp_max_amnt, 0) as PVP_DISCREP_MAX_AMNT
      ,nvl(summary.pvp_avg_amnt, 0) as PVP_DISCREP_AVG_AMNT
      ,v_act_id as ActId
      ,v_has_knp_discrepancy as HasKnpDiscrepancy
      ,v_decision_id as DecisionId
      ,v_act_closed as ActIsClosed
      ,v_decision_closed as DecisionIsClosed
      ,v_has_act_discrepancy as HasActDiscrepancy
      ,v_has_decision_discrepancy as HasDecisionDiscrepancy
      ,v_has_seod_decision as HasSeodDecision
  from DECLARATION_HISTORY decl
  join ASK_DECLANDJRNL ask on ask.zip = pZip
  left join TAX_PAYER tp on tp.inn = decl.inn_declarant and tp.kpp_effective = decl.kpp_effective
  left join V$DECLARATION_CURRENT_ASSIGNEE ins_ass on
        ins_ass.declaration_type_code = decl.type_code
        and ins_ass.inn_declarant = decl.inn_declarant
        and ins_ass.kpp_effective = decl.kpp_effective
        and ins_ass.fiscal_year = decl.fiscal_year
        and ins_ass.period_effective = decl.period_effective
  left join
  (
    select
      src.ZIP,
      max(case when src.TipFajla = 0 then nvl(act.zip, src.zip) end) as actual_zip8,
      max(case when src.TipFajla = 2 then nvl(act.zip, src.zip) end) as actual_zip81,
      max(case when src.TipFajla = 1 then nvl(act.zip, src.zip) end) as actual_zip9,
      max(case when src.TipFajla = 3 then nvl(act.zip, src.zip) end) as actual_zip91,
      max(case when src.TipFajla = 4 then nvl(act.zip, src.zip) end) as actual_zip10,
      max(case when src.TipFajla = 5 then nvl(act.zip, src.zip) end) as actual_zip11,
      max(case when src.TipFajla = 6 then nvl(act.zip, src.zip) end) as actual_zip12,
      --Incoice count
      nvl(sum(case when src.TipFajla = 0 then nvl(act.KolZapisey, nvl(src.KolZapisey, 0)) end), 0) as invoice_count8,
      nvl(sum(case when src.TipFajla = 2 then nvl(act.KolZapisey, nvl(src.KolZapisey, 0)) end), 0) as invoice_count81,
      nvl(sum(case when src.TipFajla = 1 then nvl(act.KolZapisey, nvl(src.KolZapisey, 0)) end), 0) as invoice_count9,
      nvl(sum(case when src.TipFajla = 3 then nvl(act.KolZapisey, nvl(src.KolZapisey, 0)) end), 0) as invoice_count91,
      nvl(sum(case when src.TipFajla = 4 then nvl(act.KolZapisey, nvl(src.KolZapisey, 0)) end), 0) as invoice_count10,
      nvl(sum(case when src.TipFajla = 5 then nvl(act.KolZapisey, nvl(src.KolZapisey, 0)) end), 0) as invoice_count11,
      nvl(sum(case when src.TipFajla = 6 then nvl(act.KolZapisey, nvl(src.KolZapisey, 0)) end), 0) as invoice_count12,
      --Chapter 8 and 8.1
      sum(case when src.TipFajla = 0 then nvl(act.SUMNDSPOK, src.SUMNDSPOK) end) as SumNDSPok_8,
      sum(case when src.TipFajla = 2 then nvl(act.SUMNDSPOK, src.SUMNDSPOK) end) as SumNDSPok_81,
      sum(case when src.TipFajla = 2 then nvl(act.SUMNDSPOKDL, src.SUMNDSPOKDL) end) as SumNDSPokDL_81,
      -- Begin Chapter 9 and Chapter 9.1
      sum(case when src.TipFajla = 1 then nvl(act.STPROD18, src.STPROD18) end) as StProd18_9,
      sum(case when src.TipFajla = 3 then nvl(act.STPROD18, src.STPROD18) end) as StProd18_91,
      sum(case when src.TipFajla = 3 then nvl(act.STPROD18DL, src.STPROD18DL) end) as StProd18DL_91,
      sum(case when src.TipFajla = 1 then nvl(act.STPROD10, src.STPROD10) end) as StProd10_9,
      sum(case when src.TipFajla = 3 then nvl(act.STPROD10, src.STPROD10) end) as StProd10_91,
      sum(case when src.TipFajla = 3 then nvl(act.STPROD10DL, src.STPROD10DL) end) as StProd10DL_91,
      sum(case when src.TipFajla = 1 then nvl(act.STPROD0, src.STPROD0) end) as StProd0_9,
      sum(case when src.TipFajla = 3 then nvl(act.STPROD0, src.STPROD0) end) as StProd0_91,
      sum(case when src.TipFajla = 3 then nvl(act.STPROD0DL, src.STPROD0DL) end) as StProd0DL_91,
      sum(case when src.TipFajla = 1 then nvl(act.SUMNDSPROD18, src.SUMNDSPROD18) end) as SumNDSProd18_9,
      sum(case when src.TipFajla = 3 then nvl(act.SUMNDSPROD18, src.SUMNDSPROD18) end) as SumNDSProd18_91,
      sum(case when src.TipFajla = 3 then nvl(act.SUMNDS18DL, src.SUMNDS18DL) end) as SumNDSProd18DL_91,
      sum(case when src.TipFajla = 1 then nvl(act.SUMNDSPROD10, src.SUMNDSPROD10) end) as SumNDSProd10_9,
      sum(case when src.TipFajla = 3 then nvl(act.SUMNDSPROD10, src.SUMNDSPROD10) end) as SumNDSProd10_91,
      sum(case when src.TipFajla = 3 then nvl(act.SUMNDS10DL, src.SUMNDS10DL) end) as SumNDSProd10DL_91,
      sum(case when src.TipFajla = 1 then nvl(act.STPRODOSV, src.STPRODOSV) end) as StProdOsv_9,
      sum(case when src.TipFajla = 3 then nvl(act.STPRODOSV, src.STPRODOSV) end) as StProdOsv_91,
      sum(case when src.TipFajla = 3 then nvl(act.STPRODOSVDL, src.STPRODOSVDL) end) as StProdOsvDL_91,
      -- End Chapter 9 and Chapter 9.1
      max(case when src.TipFajla = 0 then nvl(act.NOMKORR, nvl(src.NOMKORR, 0)) end) as AKT_NOMKORR_8,
      max(case when src.TipFajla = 2 then nvl(act.NOMKORR, nvl(src.NOMKORR, 0)) end) as AKT_NOMKORR_81,
      max(case when src.TipFajla = 1 then nvl(act.NOMKORR, nvl(src.NOMKORR, 0)) end) as AKT_NOMKORR_9,
      max(case when src.TipFajla = 3 then nvl(act.NOMKORR, nvl(src.NOMKORR, 0)) end) as AKT_NOMKORR_91,
      max(case when src.TipFajla = 4 then nvl(act.NOMKORR, nvl(src.NOMKORR, 0)) end) as AKT_NOMKORR_10,
      max(case when src.TipFajla = 5 then nvl(act.NOMKORR, nvl(src.NOMKORR, 0)) end) as AKT_NOMKORR_11,
      max(case when src.TipFajla = 6 then nvl(act.NOMKORR, nvl(src.NOMKORR, 0)) end) as AKT_NOMKORR_12,
      sum(src.STPROD18) as StProd18,
      sum(src.STPROD10) as StProd10,
      sum(src.STPROD0) as StProd0,
      sum(src.SUMNDSPROD18) as SumNDSProd18,
      sum(src.SUMNDSPROD10) as SumNDSProd10,
      sum(src.STPROD) as StProd,
      sum(src.STPRODOSV) as StProdOsv,
      sum(src.STPROD18DL) as StProd18DL,
      sum(src.STPROD10DL) as StProd10DL,
      sum(src.STPROD0DL) as StProd0DL,
      sum(src.SUMNDS18DL) as SumNDSProd18DL,
      sum(src.SUMNDS10DL) as SumNDSProd10DL,
      sum(src.STPRODOSVDL) as StProdOsvDL,
      nvl(SUM(case when src.TipFajla in ('0', '2', '1', '3', '6') then nvl(src.KolZapisey, 0) end), 0) as COUNT_INVOICE_R08_09_12
    from NDS2_MRR_USER.V$ASKSVODZAP src
    left join NDS2_MRR_USER.V$ASKSVODZAP act on act.id = src.IdAkt
    where src.zip = pZip
    group by src.ZIP
  ) dsd on dsd.zip = decl.zip
  left join
  (
      select
         zh.zip,
         sum(nvl(zhc.KolZapCh1, 0)) as ch_1_count_total,
         sum(nvl(zhc.KolZapCh2, 0)) as ch_2_count_total
      from v$askjournaluch zh
      inner join v$askjournalchast zhc on zhc.IdZhurn = zh.id
      where zh.zip = pZip
      group by zh.zip
  ) zhd on zhd.zip = decl.zip
  join NDS2_MRR_USER.V$SSRF dreg on dreg.s_code = decl.region_code
  join NDS2_MRR_USER.V$SONO dsono on dsono.s_code = decl.sono_code
  join NDS2_MRR_USER.DICT_TAX_PERIOD dperiod on dperiod.code = ask.period
  left join NDS2_MRR_USER.SOV_INVOICE_REQUEST req8 on req8.zip = dsd.actual_zip8 and req8.partition_number = 8
  left join NDS2_MRR_USER.SOV_INVOICE_REQUEST req81 on req81.zip = dsd.actual_zip81 and req81.partition_number = 81
  left join NDS2_MRR_USER.SOV_INVOICE_REQUEST req9 on req9.zip = dsd.actual_zip9 and req9.partition_number = 9
  left join NDS2_MRR_USER.SOV_INVOICE_REQUEST req91 on req91.zip = dsd.actual_zip91 and req91.partition_number = 91
  left join NDS2_MRR_USER.SOV_INVOICE_REQUEST req10 on req10.zip = dsd.actual_zip10 and req10.partition_number = 10
  left join NDS2_MRR_USER.SOV_INVOICE_REQUEST req11 on req11.zip = dsd.actual_zip11 and req11.partition_number = 11
  left join NDS2_MRR_USER.SOV_INVOICE_REQUEST req12 on req12.zip = dsd.actual_zip12 and req12.partition_number = 12
  left outer join NDS2_MRR_USER.BOOK_DATA_REQUEST_PRIORITY rp08 on dsd.invoice_count8 >= rp08.min_count and
       ((rp08.max_count is not null and dsd.invoice_count8 <= rp08.max_count) or (rp08.max_count is null))
  left outer join NDS2_MRR_USER.BOOK_DATA_REQUEST_PRIORITY rp81 on dsd.invoice_count81 >= rp81.min_count and
       ((rp81.max_count is not null and dsd.invoice_count81 <= rp81.max_count) or (rp81.max_count is null))
  left outer join NDS2_MRR_USER.BOOK_DATA_REQUEST_PRIORITY rp09 on dsd.invoice_count9 >= rp09.min_count and
       ((rp09.max_count is not null and dsd.invoice_count9 <= rp09.max_count) or (rp09.max_count is null))
  left outer join NDS2_MRR_USER.BOOK_DATA_REQUEST_PRIORITY rp91 on dsd.invoice_count91 >= rp91.min_count and
       ((rp91.max_count is not null and dsd.invoice_count91 <= rp91.max_count) or (rp91.max_count is null))
  left outer join NDS2_MRR_USER.BOOK_DATA_REQUEST_PRIORITY rp10 on dsd.invoice_count10 >= rp10.min_count and
       ((rp10.max_count is not null and dsd.invoice_count10 <= rp10.max_count) or (rp10.max_count is null))
  left outer join NDS2_MRR_USER.BOOK_DATA_REQUEST_PRIORITY rp11 on dsd.invoice_count11 >= rp11.min_count and
       ((rp11.max_count is not null and dsd.invoice_count11 <= rp11.max_count) or (rp11.max_count is null))
  left outer join NDS2_MRR_USER.BOOK_DATA_REQUEST_PRIORITY rp12 on dsd.invoice_count12 >= rp12.min_count and
       ((rp12.max_count is not null and dsd.invoice_count12 <= rp12.max_count) or (rp12.max_count is null))
  left join decl_successor_decl_summary summary on summary.zip = decl.zip
  where decl.zip = pZip;
end;

procedure P$GET_ACTUAL_DECLARATION(
  pZip in number,
  pCursor out SYS_REFCURSOR)
as
  vZip number;
begin 
  select max(dh.zip) 
    into vZip 
    from 
      ( 
        select inn_contractor 
              ,kpp_effective 
              ,period_code 
              ,fiscal_year 
              ,type_code 
          from declaration_history 
          where zip = pZip 
      ) sd 
    join declaration_history dh 
      on sd.inn_contractor = dh.inn_contractor 
      and sd.kpp_effective = dh.kpp_effective 
      and sd.period_code = dh.period_code 
      and sd.fiscal_year = dh.fiscal_year 
      and sd.type_code = dh.type_code
    where dh.is_active = 1;
  
  P$GET_DECLARATION(vZip, pCursor);
end;

procedure UPDATE_CONTRACTOR_AGGR_START
  as
begin
  NDS2$SYS.LOG_INFO(
  P_SITE => 'PAC$DECLARATION.UPDATE_INVOICE_CONTRACTOR_AGGR_START_WORK',   P_ENTITY_ID => null,
  P_MESSAGE_CODE => SQLCODE,   P_MESSAGE => 'starting');
  begin
    P$UPDATE_CONTRACTOR_AGGR;
  exception when others then null;
  end;
end;

procedure P$UPDATE_CONTRACTOR_AGGR
as
    pInsertDateBegin DATE;
    pInsertDateEnd DATE;
    pParameter VARCHAR2(128);
begin
    pParameter := 'seod_decl_key_last_processed';
    select max(to_date(value, 'DD/MM/YYYY')) into pInsertDateBegin
    from configuration where parameter = pParameter;

    if pInsertDateBegin is null then
      select max(insert_date) into pInsertDateBegin from INVOICE_CONTRACTOR_AGGR;
      if pInsertDateBegin is null then select nvl(min(insert_date), sysdate) into pInsertDateBegin from NDS2_SEOD.SEOD_DECLARATION; end if;
    end if;

    pInsertDateEnd := sysdate;

    UPDATE CONFIGURATION c
      SET c.value = to_char(pInsertDateEnd, 'DD/MM/YYYY'),
          c.default_value = to_char(pInsertDateEnd, 'DD/MM/YYYY')
    WHERE c.parameter = pParameter;

    INSERT INTO INVOICE_CONTRACTOR_AGGR agg
    SELECT q.CONTRACTOR_KEY, 1 as EXISTS_IN_MC, sysdate as INSERT_DATE, sysdate as UPDATE_DATE
    FROM
    (
        SELECT (to_number(ask.otchetgod) * 100000000000000 + tm.month * 1000000000000 + to_number(ask.inn_contractor)) as CONTRACTOR_KEY
        FROM ASK_DECLANDJRNL ask
        JOIN DICT_TAX_PERIOD_MONTH tm on tm.tax_period = ask.period
        WHERE TRUNC(ask.INSERT_DATE) >= TRUNC(pInsertDateBegin)
        GROUP BY ask.otchetgod, tm.month, ask.inn_contractor
    ) q
    WHERE NOT EXISTS (SELECT 1 FROM INVOICE_CONTRACTOR_AGGR t WHERE t.contractor_key = q.CONTRACTOR_KEY);

    COMMIT;
end;

procedure P$GET_CONTRACTOR_DECL_ZIP(
  pInn in number,
  pYear in varchar2,
  pMonth in dict_tax_period_month.month%type,
  pCursor out SYS_REFCURSOR
  )
as
begin
  open pCursor for
    select
      decl.zip,
      decl.CORRECTION_NUMBER as rank
    from declaration_history decl
    join dict_tax_period_month m on m.tax_period = decl.period_code
    where decl.inn_contractor = pInn and decl.FISCAL_YEAR = pYear and m.month = pMonth
  and decl.zip is not null;
end;

procedure P$BOOK_DATA_REQUEST_STATUS (
  pRequestId in BOOK_DATA_REQUEST.ID%type,
  pData out SYS_REFCURSOR
  )
as
begin

  open pData for select dict.* from REQUEST_STATUS dict join BOOK_DATA_REQUEST req on req.STATUS = dict.ID where req.ID = pRequestId;

end;

procedure P$BOOK_DATA_REQUEST_EXISTS (
  pRequestId out BOOK_DATA_REQUEST.ID%type,
  pStatus out BOOK_DATA_REQUEST.Status%type,
  pInn in BOOK_DATA_REQUEST.INN%type,
  pCorrectionNumber in BOOK_DATA_REQUEST.CORRECTIONNUMBER%type,
  pPartitionNumber in BOOK_DATA_REQUEST.PARTITIONNUMBER%type,
  pPeriod in BOOK_DATA_REQUEST.PERIOD%type,
  pYear in BOOK_DATA_REQUEST.YEAR%type
 )
as
begin
 select ID, status
 into pRequestId, pStatus
 from
   (
     select ID, status
     from BOOK_DATA_REQUEST
     where
      INN = pInn
      AND CORRECTIONNUMBER = pCorrectionNumber
      AND PARTITIONNUMBER = pPartitionNumber
      AND PERIOD = pPeriod
      AND YEAR = pYear
      AND STATUS <> 9
      order by requestdate desc
   ) T
  where ROWNUM = 1;

 exception
   when NO_DATA_FOUND then
      pRequestId := null;
      pStatus := null;
end;


 procedure P$NDSDISCREPANCY_REQ_STATUS (
  pRequestId in DISCREPANCY_REQUEST.ID%type,
  pData out SYS_REFCURSOR
  )
as
begin

  open pData for select dict.* from REQUEST_STATUS dict join DISCREPANCY_REQUEST req on req.STATUS = dict.ID where req.ID = pRequestId;

end;


procedure P$NDSDISCREPANCY_REQ_EXISTS (
  pRequestId out DISCREPANCY_REQUEST.ID%type,
  pInn in DISCREPANCY_REQUEST.INN%type,
  pCorrectionNumber in DISCREPANCY_REQUEST.CORRECTION_NUMBER%type,
  pPeriod in DISCREPANCY_REQUEST.PERIOD%type,
  pYear in DISCREPANCY_REQUEST.YEAR%type,
  pKind in DISCREPANCY_REQUEST.KIND%type,
  pType in DISCREPANCY_REQUEST.TYPE%type
 )
as
begin

 select ID
 into pRequestId
 from DISCREPANCY_REQUEST
 where
  INN = pInn AND CORRECTION_NUMBER = pCorrectionNumber AND KIND = pKind AND TYPE = pType AND PERIOD = pPeriod AND YEAR = pYear
  AND STATUS <> 9 AND ROWNUM = 1;

 exception
   when NO_DATA_FOUND then
      pRequestId := null;
end;

procedure P$GET_ACT_KNP(
  pZip in number,
  pCursor out SYS_REFCURSOR)
as
begin

  open pCursor for
  select act.*
  from
    ask_declandjrnl d_ask
    join declaration_history d_hist
      on    d_hist.inn_declarant = d_ask.innnp
        and d_hist.inn_contractor = nvl(d_ask.innreorg, d_ask.innnp)
        and d_hist.kpp_effective = d_ask.kpp_effective
        and d_hist.fiscal_year = d_ask.otchetgod
        and d_hist.period_code = d_ask.period
        and d_hist.type_code = d_ask.type
    join nds2_seod.seod_knp knp
      on    knp.declaration_reg_num = d_hist.reg_number
        and knp.ifns_code = d_hist.sono_code_submited
    join nds2_seod.seod_knp_act act on act.knp_id = knp.knp_id
  WHERE d_ask.zip = pZip;

end;

procedure P$GET_SEOD_DECISION(
  pInnDeclarant in varchar2,
  pInn in varchar2,
  pKppEffective in varchar2,
  pPeriodCode in varchar2,
  pFiscalYear in varchar2,
  pResult out SYS_REFCURSOR) as
begin
  open pResult for
  select knp_dec.type_id as DecisionType
  from
    DECLARATION_HISTORY d
    join NDS2_SEOD.SEOD_KNP knp on knp.declaration_reg_num = d.reg_number and knp.ifns_code = d.sono_code_submited
    join NDS2_SEOD.SEOD_KNP_DECISION knp_dec on knp_dec.knp_id = knp.knp_id
  where d.inn_declarant = pInnDeclarant
    and d.inn_contractor = pInn
    and d.kpp_effective = pKppEffective
    and d.period_code = pPeriodCode
    and d.fiscal_year = pFiscalYear
  group by knp_dec.type_id;
end;

PROCEDURE P$SOV_INVOICE_CACHE_CLEAR
  as
  VP_InsertDateBegin DATE;
  VP_InsertDateEnd DATE;
  VP_Parameter VARCHAR2(128);
  VP_rowsCount NUMBER;
  VP_count NUMBER;
  VP_invoice_count_all NUMBER;
  VP_invoice_count_left NUMBER;

  VP_partition_name varchar2(64 char);
  VP_zips VARCHAR2(32767);
  VP_number_zips T$TABLE_OF_NUMBER;
  VP_number_zips_del T$TABLE_OF_NUMBER;
  VP_number_zips_exists T$TABLE_OF_NUMBER;

  VP_invoice_count_partition number;
  VP_count_zip_del number;
  VP_count_zip_exist number;
  VP_is_truncate_part number;
  VP_equals number;
begin

  VP_Parameter := 'explain_sov_invoice_cache_last_processed';
  select max(to_date(value, 'DD/MM/YYYY')) into VP_InsertDateBegin
  from NDS2_MRR_USER.CONFIGURATION where parameter = VP_Parameter;

  if VP_InsertDateBegin is null then
    VP_InsertDateBegin := TO_DATE('01.04.2014', 'dd.MM.yyyy');
  end if;

  VP_InsertDateEnd := sysdate;

  --сбор ZIP деклараций
  insert into NDS2_MRR_USER.EXPLAIN_SOV_INVOICE_CACHE (explain_zip, decl_zip)
  select p.ZIP as explain_zip, d.ZIP as decl_zip
  from
  (
       select z.ID from NDS2_MRR_USER.V$ASKZIPFAJL z
       where TRUNC(z.DATAFAJLA) >= TRUNC(VP_InsertDateBegin)
  ) z
  join NDS2_MRR_USER.V$ASKPOYASNENIE p on z.ID = p.ZIP
  join NDS2_MRR_USER.V$ASKDEKL d on d.ID =  p.IdDekl
  where not exists(select sov.explain_zip from NDS2_MRR_USER.EXPLAIN_SOV_INVOICE_CACHE sov where sov.explain_zip = p.zip)
        and p.ObrabotanMS = 2 and d.ID is not null;

  -- Удаление из SOV_INVOICE_REQUEST
  delete from NDS2_MRR_USER.SOV_INVOICE_REQUEST
  where zip in
  (
      select sov.decl_zip
      from NDS2_MRR_USER.EXPLAIN_SOV_INVOICE_CACHE sov
      where sov.is_cache_clear = 0
  );

  commit;
  UTL_LOG('PAC$DECLARATION.P$SOV_INVOICE_CACHE_CLEAR', null, 'delete sov_invoice_request complete');

  -- узнаем общее кол-во записей для удаления
  VP_invoice_count_all := 0;
  select count(1) into VP_invoice_count_all from NDS2_MRR_USER.SOV_INVOICE inv
  where inv.declaration_version_id in
  (
      select decl_zip
      from NDS2_MRR_USER.EXPLAIN_SOV_INVOICE_CACHE sov
      where sov.is_cache_clear = 0
  );
  VP_invoice_count_left := VP_invoice_count_all;
  UTL_LOG('PAC$DECLARATION.P$SOV_INVOICE_CACHE_CLEAR', null, 'sov_invoice count for delete all = ' || VP_invoice_count_all);

  if VP_invoice_count_all > 0 then 
	-- проходим по дефолтной партиции 
	LOOP
	  delete from NDS2_MRR_USER.SOV_INVOICE partition (SI_DEF) inv
	    where inv.declaration_version_id in
		  (
		    select decl_zip
			  from NDS2_MRR_USER.EXPLAIN_SOV_INVOICE_CACHE sov
			  where sov.is_cache_clear = 0
		  )
		  and rownum <= 30000;

      VP_rowsCount := SQL%ROWCOUNT;
      VP_count := 0;

      commit;

      VP_invoice_count_left := VP_invoice_count_left - VP_rowsCount;
	  UTL_LOG('PAC$DECLARATION.P$SOV_INVOICE_CACHE_CLEAR', null, 'sov_invoice partition(SI_DEF) rows count deleted = ' || VP_rowsCount || ' left = ' || VP_invoice_count_left);

      -- если кол-во удаляемых записей равно нулю, то выходим из цикла удаления
      IF (VP_rowsCount = 0) THEN
        -- дополнительная проверка, все удалились или нет
        select count(1) 
		  into VP_count 
		  from NDS2_MRR_USER.SOV_INVOICE partition (SI_DEF) inv
          where inv.declaration_version_id in
            (
              select decl_zip
                from NDS2_MRR_USER.EXPLAIN_SOV_INVOICE_CACHE sov
                where sov.is_cache_clear = 0
            );

        UTL_LOG('PAC$DECLARATION.P$SOV_INVOICE_CACHE_CLEAR', null, 'check sov_invoice partition(SI_DEF) count for delete = ' || VP_count);

        -- после доп. проверки точно выходим из цикла удаления
        IF (VP_count = 0) THEN
          EXIT;
        END IF;
      END IF;

    END LOOP;

    -- идем в цикле по созданным партициям с ZIPами
    for item in
    (
       select t.partition_name, t.high_value as zip_values
         from user_tab_partitions t
         where t.table_name = 'SOV_INVOICE' 
		   and NOT t.partition_name = 'SI_DEF'
    )
    loop
      VP_partition_name := item.partition_name;
      VP_zips := item.zip_values;

      -- парсим ZIP-ники входящие в партиции
      SELECT num_value BULK COLLECT INTO VP_number_zips FROM
      (
         SELECT TRIM (REGEXP_SUBSTR (num_csv,'[^,]+', 1, LEVEL)) num_value FROM
         ( SELECT  VP_zips num_csv FROM DUAL )
         CONNECT BY LEVEL <= regexp_count (num_csv, ',', 1) + 1
      )
      WHERE num_value IS NOT NULL;

      -- берем ZIP-ники входящие в партиции, которые необходимо удалить
      select distinct sov.decl_zip BULK COLLECT INTO VP_number_zips_del
      from NDS2_MRR_USER.EXPLAIN_SOV_INVOICE_CACHE sov
      where sov.is_cache_clear = 0
        and sov.decl_zip in (select column_value from table(VP_number_zips));
      -- кол-во ZIP-ников
      VP_count_zip_del := VP_number_zips_del.count;

      -- берем ZIP-ники входящие в партиции, которые существуют для удаления
      select distinct sov.declaration_version_id BULK COLLECT INTO VP_number_zips_exists
      from NDS2_MRR_USER.SOV_INVOICE sov
      where sov.declaration_version_id in (select column_value from table(VP_number_zips));
      -- кол-во ZIP-ников
      VP_count_zip_exist := VP_number_zips_exists.count;

      -- проверяем, можно ли транкейтить, т.е. все существующие ZIP-ники из партиции
      -- входят в список ZIP-ников, которые необходимо удалить
      VP_is_truncate_part := 1;
      FOR i IN 1 .. VP_number_zips_exists.COUNT LOOP
        VP_equals := 0;
        FOR j IN 1 .. VP_number_zips_del.COUNT LOOP
            if VP_number_zips_exists(i) = VP_number_zips_del(j) then
               VP_equals := 1;
               exit;
            end if;
        END LOOP;
        if VP_equals = 0 then
          VP_is_truncate_part := 0;
          exit;
        end if;
      END LOOP;

      -- транкейтим всю партицию
      if VP_count_zip_del > 0 and VP_count_zip_exist > 0 and VP_is_truncate_part = 1 then
         select count(1) into VP_invoice_count_partition from NDS2_MRR_USER.SOV_INVOICE inv
         where inv.declaration_version_id in (select column_value from table(VP_number_zips));

         execute immediate 'alter table NDS2_MRR_USER.SOV_INVOICE truncate partition ' || VP_partition_name;

         VP_invoice_count_left := VP_invoice_count_left - VP_invoice_count_partition;
         UTL_LOG('PAC$DECLARATION.P$SOV_INVOICE_CACHE_CLEAR', null, 'truncate partition ' || VP_partition_name);
         UTL_LOG('PAC$DECLARATION.P$SOV_INVOICE_CACHE_CLEAR', null, 'sov_invoice rows count deleted = ' || VP_invoice_count_partition || ' left = ' || VP_invoice_count_left);

          -- выставляем что обработали у ZIP-ников, у которых только что удалили СФ
          update NDS2_MRR_USER.EXPLAIN_SOV_INVOICE_CACHE set is_cache_clear = 1, dt_processed = sysdate
          where is_cache_clear = 0
            and decl_zip in (select column_value from table(VP_number_zips));
      end if;

      -- если транкейтить нельзя, то удаляем в цикле все необходимые записи в этой партиции
      if VP_count_zip_del > 0 and VP_count_zip_exist > 0 and VP_is_truncate_part = 0 then
      LOOP
            delete from NDS2_MRR_USER.SOV_INVOICE inv
            where inv.declaration_version_id in
            (
                select decl_zip
                from NDS2_MRR_USER.EXPLAIN_SOV_INVOICE_CACHE sov
                where sov.is_cache_clear = 0
                  and sov.decl_zip in (select column_value from table(VP_number_zips))
            )
            and rownum <= 30000;

            VP_rowsCount := SQL%ROWCOUNT;
            VP_count := 0;

            commit;

            VP_invoice_count_left := VP_invoice_count_left - VP_rowsCount;
            UTL_LOG('PAC$DECLARATION.P$SOV_INVOICE_CACHE_CLEAR', null, 'sov_invoice rows count deleted = ' || VP_rowsCount || ' left = ' || VP_invoice_count_left);

            -- если кол-во удаляемых записей равно нулю, то выходим из цикла удаления
            IF (VP_rowsCount = 0) THEN
               -- дополнительная проверка, все удалились или нет
                select count(1) into VP_count from NDS2_MRR_USER.SOV_INVOICE inv
                where inv.declaration_version_id in
                (
                    select decl_zip
                    from NDS2_MRR_USER.EXPLAIN_SOV_INVOICE_CACHE sov
                    where sov.is_cache_clear = 0
                        and sov.decl_zip in (select * from table(VP_number_zips))
                );

                UTL_LOG('PAC$DECLARATION.P$SOV_INVOICE_CACHE_CLEAR', null, 'check sov_invoice count for delete = ' || VP_count);

                -- после доп. проверки точно выходим из цикла удаления
                IF (VP_count = 0) THEN
                    EXIT;
                END IF;
            END IF;

          END LOOP;

          -- выставляем что обработали у ZIP-ников, у которых только что удалили СФ
          update NDS2_MRR_USER.EXPLAIN_SOV_INVOICE_CACHE set is_cache_clear = 1, dt_processed = sysdate
          where is_cache_clear = 0
            and decl_zip in (select column_value from table(VP_number_zips));
      end if;

      commit;
    end loop;

  end if;
  
  -- проставляем флаг обработки у записей, которым не нашлось соответствия в SOV_INVOICE (устаревшие запросы на удаление по какой-то причине ранее не обработанные)
  update NDS2_MRR_USER.EXPLAIN_SOV_INVOICE_CACHE 
	 set is_cache_clear = 1
		,dt_processed = sysdate 
	where is_cache_clear = 0;

  UPDATE NDS2_MRR_USER.CONFIGURATION c
  SET c.value = to_char(VP_InsertDateEnd, 'DD/MM/YYYY'),
      c.default_value = to_char(VP_InsertDateEnd, 'DD/MM/YYYY')
  WHERE c.parameter = VP_Parameter;
  
  commit;

  exception when others then
  rollback;
  UTL_LOG('PAC$DECLARATION.P$SOV_INVOICE_CACHE_CLEAR failed', sqlcode, substr(sqlerrm, 256));
end;

procedure P$SOV_INV_CACHE_CLEAR_START
  as
begin
  UTL_LOG('PAC$DECLARATION.P$SOV_INVOICE_CACHE_CLEAR_START', sqlcode, 'starting');
  begin
    P$SOV_INVOICE_CACHE_CLEAR;
  end;
   UTL_LOG('PAC$DECLARATION.P$SOV_INVOICE_CACHE_CLEAR_START', sqlcode, 'finish');
end;

procedure P$GET_DECLARATION_OWNER(
  P_INN_CONTRACTOR in varchar2,
  P_KPP_EFFECTIVE in varchar2,
  P_TYPE in number,
  P_PERIOD in varchar2,
  P_YEAR in varchar2,
  pCursor out SYS_REFCURSOR)
as
begin

  OPEN pCursor FOR
  SELECT ica.sid as inspector_sid
  FROM V$DECLARATION_CURRENT_ASSIGNEE ica
  JOIN DECLARATION_ACTIVE da on da.inn_declarant = ica.inn_declarant and
                                da.kpp_effective = ica.kpp_effective and
                                da.period_effective = ica.period_effective and
                                da.fiscal_year = ica.fiscal_year and
                                da.type_code = ica.declaration_type_code
  WHERE da.inn_contractor = P_INN_CONTRACTOR
        and ica.kpp_effective = P_KPP_EFFECTIVE
        and ica.fiscal_year = P_YEAR
        and ica.period_effective in (Select distinct effective_value from DICT_TAX_PERIOD where period = P_PERIOD )
        and ica.declaration_type_code = P_TYPE;

end;

procedure P$GET_ALL_ZIPS
(
  VP_Zip in number,
  VP_Cursor out SYS_REFCURSOR
)
as
begin
  OPEN VP_Cursor FOR
  select dho.zip as Zip
         ,dho.correction_number as CorrectionNumber
  from declaration_history dh
  join declaration_history dho on
           dho.inn_contractor = dh.inn_contractor
       and dho.kpp_effective = dh.kpp_effective
       and dho.fiscal_year = dh.fiscal_year
       and dho.period_code = dh.period_code
       and dho.type_code = dh.type_code
  where dh.zip = VP_Zip 
  order by dho.correction_number;

end;  

procedure P$GET_CHAPTER_ACTUAL_ZIPS
(
  VP_Zip in number,
  VP_Cursor out SYS_REFCURSOR
)
as
begin
    OPEN VP_Cursor FOR
    select
      max(case when src.TipFajla = 0 then nvl(act.zip, src.zip) end) as ActualZip8,
      max(case when src.TipFajla = 2 then nvl(act.zip, src.zip) end) as ActualZip81,
      max(case when src.TipFajla = 1 then nvl(act.zip, src.zip) end) as ActualZip9,
      max(case when src.TipFajla = 3 then nvl(act.zip, src.zip) end) as ActualZip91,
      max(case when src.TipFajla = 4 then nvl(act.zip, src.zip) end) as ActualZip10,
      max(case when src.TipFajla = 5 then nvl(act.zip, src.zip) end) as ActualZip11,
      max(case when src.TipFajla = 6 then nvl(act.zip, src.zip) end) as ActualZip12
    from NDS2_MRR_USER.V$ASKSVODZAP src
    left join NDS2_MRR_USER.V$ASKSVODZAP act on act.IdAkt = src.id
    where src.zip = VP_Zip
    group by src.ZIP;

end;

procedure P$GET_CHAPTER_ACTUAL_ZIP
(
  P_Zip in number,
  P_Chapter in int,
  P_ActualZip out number
)
as
begin
    select
      max(case when src.TipFajla = P_Chapter then nvl(act.zip, src.zip) end)
    into P_ActualZip
    from NDS2_MRR_USER.V$ASKSVODZAP src
         left join NDS2_MRR_USER.V$ASKSVODZAP act on act.IdAkt = src.id
    where src.zip = P_Zip
    group by src.ZIP;

end;

end;
/
