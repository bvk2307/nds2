﻿create or replace package NDS2_MRR_USER.PAC$TABLE_STAT_MANAGER is

/* Refresh table statistics by table name */
procedure REFRESH_STAT(p_table_name varchar2);

/* Refresh table statistics of all available tables */
procedure REFRESH_STAT_ALL;

end;
/

create or replace package body NDS2_MRR_USER.PAC$TABLE_STAT_MANAGER is

/*log*/
procedure LOG(p_msg varchar2, p_type number)
as
pragma autonomous_transaction;
begin
  /* dbms_output.put_line(p_type||'-'||p_msg); */
  insert into SYSTEM_LOG(ID, TYPE, SITE, ENTITY_ID, MESSAGE_CODE, MESSAGE, LOG_DATE)
  values(SEQ_SYS_LOG_ID.nextval, p_type, 'PAC$TABLE_STAT_MANAGER', null, 1, p_msg, sysdate);
  commit;
exception when others then
  rollback;
end;

/*log info message*/
procedure LOG_INFO(p_msg varchar2)
as
begin
  LOG(p_msg, 0);
end;

/*log error message*/
procedure LOG_ERR(p_msg varchar2)
as
begin
  LOG(p_msg, 3);
end;

/* -- Strategies -- */

procedure BASE_STARTEGY(p_table_name varchar2)
as
begin
  LOG_INFO('BASE_STARTEGY [table="' || p_table_name || '"]');
  dbms_stats.gather_table_stats(ownname => 'NDS2_MRR_USER', tabname => p_table_name, cascade => true, degree => 16);
exception when others then
  LOG_ERR('BASE_STARTEGY [table="' || p_table_name || '"]: Error: ' || sqlerrm);
end;

procedure SOV_INVOCE_STARTEGY(p_table_name varchar2)
as
begin
  LOG_INFO('SOV_INVOCE_STARTEGY [table="' || p_table_name || '"]');
  /* not implemented */
exception when others then
  LOG_ERR('SOV_INVOCE_STARTEGY [table="' || p_table_name || '"]: Error: ' || sqlerrm);
end;

/* -- private -- */

function CHECK_REFRESH_STAT(p_table_name varchar2)
return boolean
as
  last_analyzed_date date;
begin
  select
    MAX(LAST_ANALYZED) AS LAST_ANALYZED_DATE
  into
    last_analyzed_date
  from ALL_TAB_STATISTICS
  where OWNER = 'NDS2_MRR_USER'
    and TABLE_NAME = p_table_name
    and STALE_STATS = 'YES'
    and PARTITION_NAME is null
    and SUBPARTITION_NAME is null;
  return last_analyzed_date is not null;
end;

procedure REFRESH_STAT_INTERNAL(p_table_name varchar2, p_method varchar2)
as
begin
  LOG_INFO('REFRESH_STAT_INTERNAL [table="' || p_table_name || '", method="' || p_method || '"]');
  if(CHECK_REFRESH_STAT(p_table_name)) then
    if(p_method = 'BASE_STARTEGY') then
      BASE_STARTEGY(p_table_name);
    elsif(p_method = 'SOV_INVOCE_STARTEGY') then
      SOV_INVOCE_STARTEGY(p_table_name);
    else
        LOG_ERR('REFRESH_STAT_INTERNAL [table="' || p_table_name || '", method="' || p_method || '"]: Method is incorrect');
    end if;
  else
    LOG_INFO('REFRESH_STAT_INTERNAL [table="' || p_table_name || '", method="' || p_method || '"]: Refresh statistics is not required');
  end if;
exception when others then
  LOG_ERR('REFRESH_STAT_INTERNAL [table="' || p_table_name || '", method="' || p_method || '"]: Error: ' || sqlerrm);
end;

/* -- public -- */

procedure REFRESH_STAT(p_table_name varchar2)
as
  row_found boolean;
  table_name CFG_TABLE_STAT.TABLE_NAME%type;
  method CFG_TABLE_STAT.METHOD%type;
begin
  begin
    select
      TABLE_NAME,
      METHOD
    into
      table_name,
      method
    from CFG_TABLE_STAT 
    where TABLE_NAME = p_table_name;
    row_found := true;
  exception when NO_DATA_FOUND then
    row_found := false;
  end;
  if(row_found) then
    REFRESH_STAT_INTERNAL(table_name, method);
  else
    LOG_ERR('REFRESH_STAT: table name="' || p_table_name || '" is not found in CFG_TABLE_STAT');
  end if;
end;

procedure REFRESH_STAT_ALL
as
begin
  for line in (select TABLE_NAME, METHOD from CFG_TABLE_STAT)
  loop
    REFRESH_STAT_INTERNAL(line.TABLE_NAME, line.METHOD);
  end loop;
end;

end;
/