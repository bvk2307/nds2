﻿create or replace package nds2_mrr_user.pac$org_structure is

procedure p$get_sono(pData out sys_refcursor);

procedure p$get_regions(pData out sys_refcursor);

procedure p$get_federal_dictricts(pData out sys_refcursor);

end pac$org_structure;
/
create or replace package body nds2_mrr_user.pac$org_structure is

UFNS_TYPE_ID constant number(1) := 6;

IFNS_TYPE_ID constant number(1) := 7;

procedure p$get_sono(pData out sys_refcursor) as
  begin
    open pData for
    select 
	decode(all_sono.s_code, '9901', '50', substr(all_sono.s_code,1,2)) as RegionCode
      ,all_sono.s_code as Code
      ,nvl(spec_sono.name, all_sono.s_name) as Name
      ,nvl(spec_sono.type_id, case when s_code like '__00' then UFNS_TYPE_ID else IFNS_TYPE_ID end) as SonoType
    from ext_sono all_sono
    left join sono_codes spec_sono on spec_sono.sono_code = all_sono.s_code
    order by s_code;
  end;

procedure p$get_regions(pData out sys_refcursor) as
  begin
    open pData for
    select
      d.district_id as FederalDistrictId,
      r.s_code as Code,
      r.s_name as Name
    from ext_ssrf r
      join federal_district_region d on d.region_code = r.s_code
    order by r.s_code;
  end;

procedure p$get_federal_dictricts(pData out sys_refcursor) as
  begin
    open pData for
    select district_id as Id, description as Name 
    from federal_district 
    order by id;
  end;

end pac$org_structure;
/
