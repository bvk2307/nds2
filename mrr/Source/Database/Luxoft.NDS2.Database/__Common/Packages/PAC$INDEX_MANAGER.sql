﻿create or replace package NDS2_MRR_USER.PAC$INDEX_MANAGER is

procedure P$ULTIMATE_VALIDATE_INDEXES(pTableName CFG_INDEX.TABLE_NAME%type, pKey CFG_INDEX.TABLE_NAME%type);

procedure P$VALIDATE_INDEXES(pTableName IN CFG_INDEX.TABLE_NAME%type);

procedure P$DROP_INDEXES(pTableName IN CFG_INDEX.TABLE_NAME%type);

end PAC$INDEX_MANAGER;
/

create or replace package body NDS2_MRR_USER.PAC$INDEX_MANAGER is

INDEX_TABLESPACE constant varchar2(30) := 'NDS2_IDX';

procedure P$LOG(pMessage varchar2)
as
pragma autonomous_transaction;
begin

  dbms_output.put_line('0-'||pMessage);

  insert into NDS2_MRR_USER.SYSTEM_LOG(ID, TYPE, SITE, ENTITY_ID, MESSAGE_CODE, MESSAGE, LOG_DATE)
  values(SEQ_SYS_LOG_ID.NEXTVAL, 0, 'PAC$INDEX_MANAGER', null, 1, pMessage, sysdate);
  commit;
end;

procedure P$DROP_CONSTRAINT (
   pName USER_CONSTRAINTS.CONSTRAINT_NAME%type
  ,pTableName USER_CONSTRAINTS.TABLE_NAME%type)
as
begin
  EXECUTE IMMEDIATE 'alter table '||pTableName||' drop constraint '||pName;
  P$LOG('Constraint '||pName||' on '||pTableName||' dropped');
end;

procedure P$DROP_INDEX (
  pIndexName CFG_INDEX.INDEX_NAME%type)
as
begin
  EXECUTE IMMEDIATE 'drop index '||pIndexName;
  P$LOG('Index '||pIndexName||' dropped');
end;

procedure P$CREATE_INDEX (
  pTableName CFG_INDEX.TABLE_NAME%type,
  pIndexName CFG_INDEX.INDEX_NAME%type,
  pColumnList CFG_INDEX.COLUMN_LIST%type,
  pIndexType CFG_INDEX.INDEX_TYPE%type,
  pStatistics CFG_INDEX.OP_STATISTICS%type,
  pParallelCount CFG_INDEX.OP_PARALLEL_COUNT%type,
  pNoParallelPostBuild CFG_INDEX.OP_NOPARALLEL_POSTBUILD%type,
  pLocal CFG_INDEX.IS_LOCAL%type)
as
  optionStat VARCHAR2(32 char) := '';
  optionParallelCount VARCHAR2(32 char) := '';
  optionLocal VARCHAR2(6 char) := '';
begin

  optionStat := case when pStatistics = 1 then ' compute statistics ' else '' end;
  optionParallelCount := case when pParallelCount > 0 then ' parallel ' || TO_CHAR(pParallelCount) || ' ' else '' end;
  optionLocal := case when pLocal = 1 then ' local' else '' end;

  CASE
    WHEN pIndexType = 'PRIMARY'
    THEN BEGIN
      -- невозможно compute statistics добавить в ALTER TABLE
      -- констрейнт использует явно заданный индекс, и несмотря на другое имя будет при ребилде
      EXECUTE IMMEDIATE (
        'alter table ' || pTableName || ' add constraint ' || pIndexName || '_С primary key (' || pColumnList || ')'
        || ' using index (create unique index ' || pIndexName || ' on '
        || pTableName || '(' || pColumnList || ')' || optionParallelCount || optionStat || optionLocal || ')'
      );
    END;
    WHEN pIndexType = 'UNIQUE'
    THEN
      BEGIN
        -- невозможно compute statistics добавить в ALTER TABLE
        -- констрейнт использует явно заданный индекс, и несмотря на другое имя будет при ребилде
        EXECUTE IMMEDIATE (
          'alter table ' || pTableName || ' add constraint ' || pIndexName || '_С unique (' || pColumnList || ')'
          || ' using index (create unique index ' || pIndexName || ' on '
          || pTableName || '(' || pColumnList || ')' || optionParallelCount || optionStat || optionLocal || ')');
      END;
  ELSE EXECUTE IMMEDIATE (
    'create index ' || pIndexName || ' on ' || pTableName || ' (' || pColumnList || ') tablespace ' || INDEX_TABLESPACE
    || ' compress' || optionParallelCount || optionStat ||  optionLocal );
  END CASE;

  if pNoParallelPostBuild = 1 then
    EXECUTE IMMEDIATE ('alter index '||pIndexName||' noparallel');
  end if;

  P$LOG('Index '||pIndexName||' created on '||pTableName||'('||pColumnList||')');
  exception when others then
    P$LOG('Ошибка создания индекса '||pIndexName||' для '||pTableName||'('||pColumnList||')'||substr(sqlerrm, 1, 128));
end;

procedure P$REBUILD_INDEX(
  pIndexName CFG_INDEX.INDEX_NAME%type,
  pLocal     CFG_INDEX.IS_LOCAL%type) as
  vQty       number := 0;
begin
  if pLocal = 1 then
    P$LOG('Index '||pIndexName||' try to rebuild on subpartitions');
    for part in (select subpartition_name as part from user_ind_subpartitions where index_name = pIndexName) loop
    begin
     vQty := vQty + 1;
     EXECUTE IMMEDIATE ('alter index '||pIndexName||' rebuild subpartition '||part.part||' tablespace '||INDEX_TABLESPACE);
     exception when others then
       P$LOG('Ошибка ребилда индекса '||pIndexName||' на субпартиции '||part.part||': '||sqlerrm);
    end;
    end loop;
    if vQty = 0 then
    P$LOG('Index '||pIndexName||' try to rebuild on partitions');
    for part in (select partition_name as part from user_ind_partitions where index_name = pIndexName) loop
    begin
     vQty := vQty + 1;
     EXECUTE IMMEDIATE ('alter index '||pIndexName||' rebuild partition '||part.part||' tablespace '||INDEX_TABLESPACE);
     exception when others then
       P$LOG('Ошибка ребилда индекса '||pIndexName||' на партиции '||part.part||': '||sqlerrm);
    end;
    end loop;
    end if;
    return;
  end if;
  EXECUTE IMMEDIATE ('alter index '||pIndexName||' rebuild tablespace '||INDEX_TABLESPACE);
  P$LOG('Index '||pIndexName||' rebuilt');
  exception when others then
    P$LOG('Ошибка ребилда индекса '||pIndexName||':'||sqlerrm);
end;

procedure P$CREATE_IF_NOT_EXISTS(
  pTableName CFG_INDEX.TABLE_NAME%type,
  pIndexName CFG_INDEX.INDEX_NAME%type,
  pColumnList CFG_INDEX.COLUMN_LIST%type,
  pIndexType CFG_INDEX.INDEX_TYPE%type,
  pStatistics CFG_INDEX.OP_STATISTICS%type,
  pParallelCount CFG_INDEX.OP_PARALLEL_COUNT%type,
  pNoParallelPostBuild CFG_INDEX.OP_NOPARALLEL_POSTBUILD%type,
  pLocal  CFG_INDEX.IS_LOCAL%type)
as
  vIndexExists number;
begin

  select count(*) into vIndexExists from USER_INDEXES t where t.index_name = pIndexName;

  if vIndexExists = 0 then
    begin
      P$CREATE_INDEX(
        pTableName,
        pIndexName,
        pColumnList,
        pIndexType,
        pStatistics,
        pParallelCount,
        pNoParallelPostBuild,
        pLocal);
    end;
  end if;

end;

procedure P$REBUILD_IF_NEEDED(
  pIndexName CFG_INDEX.INDEX_NAME%type,
  pLocal  CFG_INDEX.IS_LOCAL%type) as
  vTableSpaceValid number;
begin
  select count(*)
  into vTableSpaceValid
  from USER_INDEXES
  where
       index_name = pIndexName
    and tablespace_name = INDEX_TABLESPACE
    and status <> 'UNUSABLE'
    and uniqueness = 'NONUNIQUE';

  if vTableSpaceValid = 0 then
    begin
      P$REBUILD_INDEX(pIndexName, pLocal);
    end;
  end if;
end;

/*
Если индекс уже существует, делается попытка создать такой же с суфиксом сиквенса

*/
procedure P$ULTIMATE_CREATE(
  pTableName CFG_INDEX.TABLE_NAME%type,
  pIndexName CFG_INDEX.INDEX_NAME%type,
  pColumnList CFG_INDEX.COLUMN_LIST%type,
  pIndexType CFG_INDEX.INDEX_TYPE%type,
  pStatistics CFG_INDEX.OP_STATISTICS%type,
  pParallelCount CFG_INDEX.OP_PARALLEL_COUNT%type,
  pNoParallelPostBuild CFG_INDEX.OP_NOPARALLEL_POSTBUILD%type,
  pLocal CFG_INDEX.IS_LOCAL%type)
as
  vIndexExists number;
  vIndexNewName varchar2(128 char);
begin
  select count(*) into vIndexExists from USER_INDEXES t where t.index_name = pIndexName;

  if vIndexExists = 0 then
    begin
      P$CREATE_INDEX(
        pTableName,
        pIndexName,
        pColumnList,
        pIndexType,
        pStatistics,
        pParallelCount,
        pNoParallelPostBuild,
        pLocal);
      P$REBUILD_IF_NEEDED(pIndexName, pLocal);
    end;
  Else
    Begin
	  vIndexNewName := pIndexName||SEQ_INDEX_NAME_GEN.NEXTVAL;
      P$CREATE_INDEX(
        pTableName,
        vIndexNewName,
        pColumnList,
        pIndexType,
        pStatistics,
        pParallelCount,
        pNoParallelPostBuild,
        pLocal);
      P$REBUILD_IF_NEEDED(vIndexNewName, pLocal);
    End;
  end if;
end;

/*
Принудительное создание индексов на таблице (параметр pTableName)
по аналогии с другой (параметр pKey)
*/
procedure P$ULTIMATE_VALIDATE_INDEXES(
  pTableName CFG_INDEX.TABLE_NAME%type,
  pKey CFG_INDEX.TABLE_NAME%type
  ) as
begin
  for idx_meta in (select index_name,column_list,index_type, op_statistics, op_parallel_count, op_noparallel_postbuild, is_local
    from CFG_INDEX where TABLE_NAME = pKey)
  loop
      P$ULTIMATE_CREATE(pTableName,idx_meta.index_name,idx_meta.column_list,idx_meta.index_type,
         idx_meta.op_statistics, idx_meta.op_parallel_count, idx_meta.op_noparallel_postbuild, idx_meta.is_local);
  end loop;
end;

procedure P$VALIDATE_INDEXES(pTableName CFG_INDEX.TABLE_NAME%type) as
begin
  for idx_meta in (select index_name,column_list,index_type, op_statistics, op_parallel_count,
    op_noparallel_postbuild, is_local from CFG_INDEX where TABLE_NAME = pTableName)
  loop
    begin
      P$CREATE_IF_NOT_EXISTS(pTableName,idx_meta.index_name,idx_meta.column_list,idx_meta.index_type,
		idx_meta.op_statistics, idx_meta.op_parallel_count, idx_meta.op_noparallel_postbuild, idx_meta.is_local);
      P$REBUILD_IF_NEEDED(idx_meta.index_name, idx_meta.is_local);
      exception when others then null;
    end;
  end loop;
end;

procedure P$DROP_INDEXES(pTableName IN CFG_INDEX.TABLE_NAME%type) as
begin

  for constraint_meta in (select constraint_name,table_name from USER_CONSTRAINTS where table_name = pTableName)
    loop
      begin
        P$DROP_CONSTRAINT(constraint_meta.constraint_name, constraint_meta.table_name);
        exception when others then null;
      end;
    end loop;

  for idx_meta in (select index_name,table_name,uniqueness from USER_INDEXES where table_name = pTableName)
    loop
      begin
        P$DROP_INDEX(idx_meta.index_name);
        exception when others then null;
      end;
    end loop;
end;

end PAC$INDEX_MANAGER;
/
