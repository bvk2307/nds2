﻿create or replace package NDS2_MRR_USER.PAC$SELECTION as

type t_numbers is table of number index by pls_integer;

procedure P$SELECT_STATUS_DICTIONARY(pData OUT SYS_REFCURSOR);

procedure P$EXCLUDE_REASON_DICTIONARY(pData OUT SYS_REFCURSOR);

PROCEDURE P$GET_FILTER (
  pSelectionId IN SELECTION_FILTER.SELECTION_ID%type,
  pCursor OUT SYS_REFCURSOR);

PROCEDURE P$SAVE_FILTER (
  pSelectionId IN SELECTION_FILTER.SELECTION_ID%type,
  pFilter IN  SELECTION_FILTER.FILTER%type);

PROCEDURE P$GET_PERIODS_TEMPLATE(pCursor OUT SYS_REFCURSOR);

procedure P$SAVE(pId in out number,
  pSelectionTemplateId in number,
  pStatusId in number,
  pName in varchar2,
  pType in number,
  pLastEditedBySid in varchar2,
  pLastEditedBy in varchar2,
  pApprovedBySid in varchar2,
  pApprovedBy in varchar2,
  pRegions in varchar2,
  pAction in number
  );

  
 function F$VERIFY_SELECTION_NAME
(
  pUserSid varchar2,
  pName varchar2,
  pSelectionId number,
  pSelectionType number
) return varchar2;

function F$GenerateUniqueName
(
  pUserSid varchar2,
  pSelectionType number
) return varchar2;


PROCEDURE P$GET_SELECTION_STATUS(
pSelectionId IN NUMBER, 
pCursor OUT SYS_REFCURSOR);

PROCEDURE P$GET_SEL_REQUEST_STATUS(
pSelectionId IN NUMBER, 
pSelectionType in number,
pCursor OUT SYS_REFCURSOR);

PROCEDURE P$SELECTION_SET_STATE(
pSelectionId IN NUMBER,
pSelectionType IN NUMBER,
pStatusId IN NUMBER,
pUserSid in varchar2,
pAction in number
);

PROCEDURE P$APPROVE
(
pSelectionId IN NUMBER,
pUserSid in varchar2);

PROCEDURE P$SEND_FOR_CORRECTION
(
pSelectionId IN NUMBER,
pUserSid in varchar2);

PROCEDURE P$UPDATE_DECL_CHECK_STATE (
  pSelectionId IN number,
  pDeclarationId IN number,
  pUserSid in varchar2,
  pCheckState IN SELECTION_DECLARATION.IS_IN_PROCESS%type);

  
PROCEDURE P$UPDATE_ALL_DECL_CHECK_STATE (
  pSelectionId IN SELECTION.ID%type,
  pCheckState IN SELECTION_DECLARATION.IS_IN_PROCESS%type,
  pUserSid in varchar2,
  pZipList in t_numbers);

  
PROCEDURE P$COUNT_CHECKED_DISCREPANCIES (
  pSelectionId IN SELECTION.ID%type,
  pSelectionType in number,
  pCursor OUT SYS_REFCURSOR
  );

/*
ведение истории смены состояний выборки
*/
PROCEDURE UPD_HIST_SELECTION_STATUS
(
  pId in selection.id%type,
  pStatusId in selection.status%type
);

PROCEDURE P$GET_ACTION_HISTORY
    (
      pSelectionId in selection.Id%type,
      pCursor out sys_refcursor
    );

procedure P$LOAD_SELECTION ( -- Загружает детализированные данные выборки
  pId IN selection.ID%type,
  pCursor OUT SYS_REFCURSOR
);

/*
загрузка переходов состояний
*/
PROCEDURE P$GET_SELECTION_TRANSITIONS
(pCursor OUT SYS_REFCURSOR);


PROCEDURE P$GET_REGIONS (
  pUserSID IN METODOLOG_CHECK_EMPLOYEE.SID%type, -- Идентификатор пользователя
  pData OUT SYS_REFCURSOR -- Список регионов
  ); 
  -- Возвращает список регионов, доступных пользователю (для фильтра выборки)

PROCEDURE P$SEARCH_REGIONS (
  pUserSID IN USER_TO_N_SSRF.USER_SID%type, -- Идентификатор пользователя
  pSearchKey IN VARCHAR2,
  pMaxQuantity IN NUMBER,
  pData OUT SYS_REFCURSOR -- Список регионов
  );

PROCEDURE P$SEARCH_INSPECTIONS (
  pUserSID IN USER_TO_N_SSRF.USER_SID%type,
  pSearchKey IN VARCHAR2,
  pMaxQuantity IN NUMBER,
  pData OUT SYS_REFCURSOR
  );

procedure p$create_request(pId out number, pSelectionId in number, pQuery in clob);
procedure p$set_request_status(p_request_id number, p_status_id number);
procedure p$update_selection_statistic(pSelectionId in number,
  pDiscrQty in number,
  pDiscrAmt in number,
  pDeclQty in number);

  
PROCEDURE P$GET_INSPECTIONS (
  pUserSID IN METODOLOG_CHECK_EMPLOYEE.SID%type, -- Идентификатор пользователя
  pData OUT SYS_REFCURSOR -- Список инспекций
  ); -- Возвращает список инспекций, доступных пользователю (для фильтра выборки)

  
--Возвращает данные для редактора фильтра выборки.
procedure P$GET_FILTER_EDITOR_PARAMETERS
(
  pIsManual IN NUMBER,
  pCursor OUT SYS_REFCURSOR
);


--Возвращает допустимые значения атрибута фильтра выборки.
procedure P$GET_FILTER_PARAMETER_OPTIONS
(
   pAttributeId IN NUMBER,
   pCursor OUT SYS_REFCURSOR
);

--Возвращает список атрибутов фильтра выборки
procedure P$GET_FILTER_PARAMETERS
(
  pCursor OUT SYS_REFCURSOR
);


PROCEDURE P$COUNT_CHECKED_DECLARATIONS (
  pSelectionId IN selection.ID%type,
  pChecked OUT NUMBER,
  pAll OUT NUMBER
  );

  
PROCEDURE P$GET_FAVORITE_FILTERS (pAnalyst IN FAVORITE_FILTERS.ANALYST%type, pFavCursor OUT SYS_REFCURSOR);

PROCEDURE P$GET_FAVORITE_FILTER (pId IN FAVORITE_FILTERS.ID%type, pFavCursor OUT SYS_REFCURSOR);

PROCEDURE P$GET_ALL_FAVORITE_FILTERS (pFavCursor OUT SYS_REFCURSOR);

PROCEDURE P$SAVE_FAVORITE_FILTER (
  pId IN OUT FAVORITE_FILTERS.ID%type,
  pName IN FAVORITE_FILTERS.NAME%type,
  pAnalyst IN FAVORITE_FILTERS.ANALYST%type,
  pFilter IN FAVORITE_FILTERS.FILTER%type);

PROCEDURE P$DELETE_FAVORITE_FILTER (pId IN FAVORITE_FILTERS.ID%type);

PROCEDURE P$UPDATE_REGIONAL_BOUNDS(
pSelectionTemplateId in SELECTION_REGIONAL_BOUNDS.ID%type,
pInclude in SELECTION_REGIONAL_BOUNDS.INCLUDE%type,
pRegionCode in SELECTION_REGIONAL_BOUNDS.REGION_CODE%type, 
pDiscrTotal in SELECTION_REGIONAL_BOUNDS.DISCREP_TOTAL_AMT%type, 
pDiscrMin in SELECTION_REGIONAL_BOUNDS.DISCREP_MIN_AMT%type,
pDiscrGapTotal in SELECTION_REGIONAL_BOUNDS.DISCREP_GAP_TOTAL_AMT%type,
pDiscrGapMin in SELECTION_REGIONAL_BOUNDS.DISCREP_GAP_MIN_AMT%type);


procedure P$DELETE_SEL_BY_TEMPLATE(
pSelectionTemplateId in number);

procedure P$GET_SEL_BY_TEMPL_ID(
pSelectionTemplateId in number,
pRegions in varchar2,
pCursor out SYS_REFCURSOR);

procedure P$UPDATE_AGGREGATE(pSelectionId in number,
pType in number,
pDeclQty out number,
pDiscrQty out number,
pDiscrAmt out number);

procedure P$GET_REGIONAL_BOUND(
pSelectionTemplateId in number,
pRegionCode in varchar2,
pCursor out SYS_REFCURSOR);


procedure P$CAN_DELETE_TEMPLATE(pSelectionTemplateId in number,
                                pCanDelete out number);

/* процедура проверки наличия выборок в статусе "Согласовано" в списке выборок */
PROCEDURE P$EXISTS_APPROVED_SELECTIONS (
  pExists out number 
);

/* процедура подсчета выборок в статусах "Согласовано" и "На согласовании" в списке выборок */
PROCEDURE P$COUNT_APPROVED_SELECTIONS (
  pRequestApproveCount out number, 
  pApprovedCount out number 
);

/* процедура перевода выборок из статуса "Согласовано" в статус "Формирование АТ" */
PROCEDURE P$SET_CLAIM_CREATING_STATUS;

PROCEDURE P$UPDATE_SEL_REGIONS(
pSelectionId in number,
pRegions in varchar2
) ;

end PAC$SELECTION;
/
create or replace package body NDS2_MRR_USER.PAC$SELECTION as

STATUS_LOADING CONSTANT NUMBER(2)  := 1;
STATUS_REQUEST_APPROVE CONSTANT NUMBER(2)  := 2;
STATUS_LOADING_ERROR CONSTANT NUMBER(2)  := 3;
STATUS_APPROVED CONSTANT NUMBER(2)  := 4;
STATUS_CLAIM_CREATED CONSTANT NUMBER(2)  := 5;
STATUS_DELETED CONSTANT NUMBER(2)  := 6;
STATUS_CLAIM_CREATING CONSTANT NUMBER(2)  := 7;
STATUS_DRAFT CONSTANT NUMBER(2)  := 8;

ACTION_EDIT CONSTANT NUMBER(2)  := 2;
ACTION_SECONDARY_APPLY CONSTANT NUMBER(2) := 3;
ACTION_TO_APPROVE CONSTANT NUMBER(2)  := 4;
ACTION_APPROVE CONSTANT NUMBER(2)  := 5;
ACTION_SEND_FOR_CORRECTION CONSTANT NUMBER(2)  := 6;
ACTION_CLAIM_CREATING CONSTANT NUMBER(2)  := 7;

TEMPLATE_TYPE CONSTANT NUMBER(2) := 1;


procedure P$SELECT_STATUS_DICTIONARY(pData OUT SYS_REFCURSOR) as
begin
  OPEN pData FOR
       select id, name as Description from DICT_SELECTION_STATUS;
end;

procedure P$EXCLUDE_REASON_DICTIONARY(pData OUT SYS_REFCURSOR) as
begin
  OPEN pData FOR
       select id, name as Description from R$EXCLUDE_REASON;
end;

PROCEDURE P$GET_FILTER (
  pSelectionId IN SELECTION_FILTER.SELECTION_ID%type,
  pCursor OUT SYS_REFCURSOR)
as
begin
  OPEN pCursor FOR select filter from selection_filter where selection_id = pSelectionId;
end;

PROCEDURE P$SAVE_FILTER (
  pSelectionId IN SELECTION_FILTER.SELECTION_ID%type,
  pFilter IN  SELECTION_FILTER.FILTER%type)
as
v_id number;
begin
  UPDATE SELECTION_FILTER t SET Filter = pFilter
  WHERE t.SELECTION_ID = pSelectionId;

  begin
  select SELECTION_ID into v_id from SELECTION_FILTER where  SELECTION_ID=pSelectionId;
  exception when no_data_found then
  v_id := null;
  end;
  if v_id is null then
  insert into selection_filter (id, selection_id, filter) values (pSelectionId, pSelectionId, pFilter);
  end if;

end;

PROCEDURE P$GET_PERIODS_TEMPLATE(pCursor OUT SYS_REFCURSOR)
as
  v_start_year number;
  v_curr_year number;
  v_curr_quarter number;
  v_curr_month number;
begin
  v_start_year := 2014;
     v_curr_month := to_number(to_char(sysdate, 'MM'));
     v_curr_year :=  to_number(to_char(sysdate, 'YYYY'));
     v_curr_quarter := (case 
                              when v_curr_month in (1,2,3) then 1 
                              when v_curr_month in (4,5,6) then 2 
                              when v_curr_month in (7,8,9) then 3
                              when v_curr_month in (10,11,12) then 4
                              else 0 end);
      open pCursor for
      select  y.current_year || d.code period_code, d.description || ' ' || y.current_year  period  from dict_tax_period d, 
      (
         select current_year 
           from (select rownum current_year from dual connect by level <= v_curr_year)
          where current_year >= v_start_year
      ) y
      where d.is_main_in_quarter = 1 and (y.current_year <> v_curr_year OR d.quarter <= v_curr_quarter)
      order by period_code desc;
end;

procedure P$SAVE(pId in out number,
  pSelectionTemplateId in number,
  pStatusId in number,
  pName in varchar2,
  pType in number,
  pLastEditedBySid in varchar2,
  pLastEditedBy in varchar2,
  pApprovedBySid in varchar2,
  pApprovedBy in varchar2,
  pRegions in varchar2,
  pAction in number
  )
as
v_save_date date;
v_last_edit_date date;
v_last_status_date date;
v_status number;
v_prev_status number;
v_user_changed varchar2(100 char);
begin
   v_save_date := sysdate;

   if pType = TEMPLATE_TYPE then 
     v_status := null;
	 v_user_changed := pApprovedBySid;
   else
	if pAction = ACTION_SEND_FOR_CORRECTION or pAction = ACTION_APPROVE then
		 v_user_changed := pApprovedBySid;
    else     v_user_changed := pLastEditedBySid; end if;
    v_status := pStatusId;
   end if;

   if pId = 0 then
     pId := selections_seq.nextval;
     insert into selection_aggregate(id, name, type, created_at, LAST_STATUS_CHANGED_AT, status_id, INITIAL_DECLARATION_QTY,INITIAL_DISCREPANCY_QTY, INITIAL_DISCREPANCY_AMT, LAST_EDITED_AT, LAST_EDITED_BY, APPROVED_BY )
     values(pId, pName, pType, v_save_date, v_save_date, v_status, 0, 0, 0, v_save_date, pLastEditedBySid, pApprovedBySid);
  
  if pType <> TEMPLATE_TYPE then
     insert into selection(id, name, creation_date, modification_date, status_date, analytic, analytic_sid,chief, chief_sid, sender, status, type, decl_count, discrep_count, regions, discrepancy_stage, template_id )
     values(pId, pName, v_save_date, v_save_date, v_save_date, pLastEditedBy, pLastEditedBy, pApprovedBy, pApprovedBySid, pLastEditedBySid, v_status, pType, 0, 0, pRegions, 1, pSelectionTemplateId);
  end if;

	 insert into Action_History (id, cd, status, change_date, user_sid, action) 
	 values(SEQ_ACTION_HISTORY.nextval, pId, v_status, v_save_date, v_user_changed, pAction);

   else

   select LAST_EDITED_AT, status_id, LAST_STATUS_CHANGED_AT into v_last_edit_date, v_prev_status, v_last_status_date
    from selection_aggregate where id = pId;
	
	if v_prev_status <> v_status or pAction = ACTION_SECONDARY_APPLY then
	    v_last_status_date := v_save_date;
	end if;
   if pAction not in (ACTION_TO_APPROVE, ACTION_APPROVE, ACTION_SEND_FOR_CORRECTION, ACTION_CLAIM_CREATING) then
		v_last_edit_date := v_save_date;
   end if;


     update selection_aggregate
     set name = pName, 
	 status_id = v_status,
	 LAST_STATUS_CHANGED_AT = v_last_status_date,
	 LAST_EDITED_BY = pLastEditedBySid,
	 APPROVED_BY = pApprovedBySid,
	 LAST_EDITED_AT = v_last_edit_date
     where id = pId;

	 if pType <> TEMPLATE_TYPE then
	 update selection
     set name = pName, 
	 status = v_status,
	 regions = pRegions,
	 status_date = v_last_status_date,
	 analytic_sid = pLastEditedBySid,
	 analytic = pLastEditedBy,
	 chief_sid = pApprovedBySid,
	 chief = pApprovedBy,
	 modification_date = v_last_edit_date
     where id = pId;
	 end if;

	 insert into Action_History (id, cd, status, change_date, user_sid, action) 
	 values(SEQ_ACTION_HISTORY.nextval, pId, v_status, v_save_date, v_user_changed, pAction);

	 commit;
   end if;
end;
function F$VERIFY_SELECTION_NAME
(
  pUserSid varchar2,
  pName varchar2,
  pSelectionId number,
  pSelectionType number
) return varchar2
as
v_idx number := 0;
v_exist number;
v_name varchar2(1024);
begin
v_name := trim(pName);
select count(1) into v_exist from selection_aggregate where lower(name) = lower(v_name) and id <> pSelectionId;
dbms_output.put_line(v_exist);
while v_exist >= 1 and v_idx < 10000 loop
   v_idx := v_idx+1;
   v_name := trim(pName)||' ('||v_idx||')';
   select count(1) into v_exist from selection_aggregate where lower(name) = lower(v_name) and id <> pSelectionId;
end loop;

return v_name;
end;

function F$GenerateUniqueName
(
  pUserSid varchar2,
  pSelectionType number
) return varchar2
as
v_name varchar2(128);
v_pattern varchar2(32) := 'Выборка ';
v_counter number(10) := 1;
begin
 if pSelectionType = 1 then v_pattern := 'Шаблон ' ; end if;
 v_name := v_pattern || v_counter;
 for sel in
   (SELECT distinct name, TO_NUMBER(REGEXP_SUBSTR(s.name, '[[:digit:]]+')) x FROM selection_aggregate s
      WHERE regexp_like(lower(s.name), lower(v_pattern)) order by x asc) 
 loop
  if lower(TO_CHAR(sel.name)) = lower(v_name) then
    v_counter := v_counter + 1;
    v_name := v_pattern || v_counter;
  end if;
 end loop;
 return v_name;
 exception
   when no_data_found then
     return rawtohex(sys_guid());
end;

PROCEDURE P$GET_SELECTION_STATUS
(pSelectionId IN NUMBER, pCursor OUT SYS_REFCURSOR)
AS
BEGIN
 OPEN pCursor FOR
 SELECT STATUS_id FROM SELECTION_AGGREGATE WHERE ID = pSelectionId;
END;


PROCEDURE P$GET_SEL_REQUEST_STATUS(
pSelectionId IN NUMBER, 
pSelectionType in number,
pCursor OUT SYS_REFCURSOR) 
as
begin

if pSelectionType = TEMPLATE_TYPE then
open pCursor for select request_id, selection_id, status from selection_request where status <> 9 and selection_id in (select id from selection where template_id = pSelectionId );
else
open pCursor for select request_id, selection_id, status from selection_request where status <> 9 and selection_id = pSelectionId;
end if;
end;


procedure P$LOAD_SELECTION ( -- Загружает детализированные данные выборки
  pId IN selection.ID%type,
  pCursor OUT SYS_REFCURSOR
)
as
begin
  open pCursor for
       select
               s.id,
               s.name as Name,
               s.type as TypeCode,
			   decode(s.type, 0, sel_type.s_name, 1, sel_type.s_name, ag.name) as Type,
               s.created_at as CreationDate,
               s.last_status_changed_at as StatusDate,
               s.last_edited_at as ModificationDate,
               s.last_edited_by as AnalyticSid,
               us.name as Analytic,
               s.approved_by as ManagerSid,
               us1.name as Manager,
               flt.filter as Filter,
               s.status_id as Status,
               sel_status.name as StatusName,
               s.initial_declaration_qty as declarationcount,
               s.initial_discrepancy_qty as discrepanciescount,
               s.initial_discrepancy_amt as totalamount,
               sel.Regions as Regions,
               sel.template_id as TemplateId
        from
             SELECTION_AGGREGATE s
			 join DICT_SELECTION_TYPE sel_type on s.type = sel_type.s_code
             join SELECTION_FILTER flt on flt.selection_id = s.id
			 left join SELECTION sel on sel.id = s.id
			 left join DICT_SELECTION_STATUS sel_status on s.status_id = sel_status.id
			 left join MRR_USER us on s.last_edited_by = us.sid
			 left join MRR_USER us1 on s.approved_by = us1.sid
			 left join SELECTION_AGGREGATE ag on ag.id = sel.template_id
              where s.id = pId;
end;

PROCEDURE P$SELECTION_SET_STATE
(
pSelectionId IN NUMBER,
pSelectionType IN NUMBER,
pStatusId IN NUMBER, 
pUserSid in varchar2,
pAction in number)
AS

BEGIN

  null; 

END;

PROCEDURE P$APPROVE
(
pSelectionId IN NUMBER,
pUserSid in varchar2)
AS
v_analytic varchar(100 CHAR);
v_save_date date;
BEGIN

  v_save_date := sysdate;
  select name into v_analytic from mrr_user where sid = pUserSid;
 
  UPDATE SELECTION_AGGREGATE SET STATUS_ID = STATUS_APPROVED, LAST_STATUS_CHANGED_AT = v_save_date, APPROVED_BY = pUserSid
  WHERE ID = pSelectionId;
  
  UPDATE SELECTION SET STATUS = STATUS_APPROVED, status_date = v_save_date, chief_sid = pUserSid, chief = v_analytic
  where id = pSelectionId;

  insert into Action_History (id, cd, status, change_date, user_sid, action) 
  values(SEQ_ACTION_HISTORY.nextval, pSelectionId, STATUS_APPROVED, v_save_date, pUserSid, ACTION_APPROVE);
  UPD_HIST_SELECTION_STATUS(pSelectionId, STATUS_APPROVED);
 
 commit;
END;


PROCEDURE P$SEND_FOR_CORRECTION
(
pSelectionId IN NUMBER,
pUserSid in varchar2)
AS
v_analytic varchar(100 CHAR);
v_save_date date;
BEGIN

  v_save_date := sysdate;
  select name into v_analytic from mrr_user where sid = pUserSid;
 
  UPDATE SELECTION_AGGREGATE SET STATUS_ID = STATUS_DRAFT, LAST_STATUS_CHANGED_AT = v_save_date, APPROVED_BY = pUserSid
  WHERE ID = pSelectionId;
  
  UPDATE SELECTION SET STATUS = STATUS_DRAFT, status_date = v_save_date, chief_sid = pUserSid, chief = v_analytic
  where id = pSelectionId;

  insert into Action_History (id, cd, status, change_date, user_sid, action) 
  values(SEQ_ACTION_HISTORY.nextval, pSelectionId, STATUS_DRAFT, v_save_date, pUserSid, ACTION_SEND_FOR_CORRECTION);
  UPD_HIST_SELECTION_STATUS(pSelectionId, STATUS_DRAFT);
 
 commit;

END;


/*
ведение истории смены состояний выборки
*/
PROCEDURE UPD_HIST_SELECTION_STATUS
(
  pId in selection.id%type,
  pStatusId in selection.status%type
)
as
begin

  insert into HIST_SELECTION_STATUS (ID, DT, VAL)
         values(pId, sysdate, pStatusId);

end;

PROCEDURE P$GET_ACTION_HISTORY
    (
      pSelectionId in selection.Id%type,
      pCursor out sys_refcursor
    )
  is
BEGIN
  open pCursor for
    select
	 ah.id as id,
	 ah.cd as selectionId,
	 dss.name as statusName,
	 ah.change_date, 
	 ah.user_sid, 
	 da.action_name,
	 u.name
    from Action_History ah
		   left join DICT_ACTIONS da on ah.action = da.id
           left join Dict_Selection_Status dss on (ah.Status = dss.Id)
		   left join MRR_USER u on ah.user_sid = u.sid
    where ah.Cd = pSelectionId
    order by ah.change_date desc;
END;

/*gene
загрузка переходов состояний
*/
PROCEDURE P$GET_SELECTION_TRANSITIONS
(pCursor OUT SYS_REFCURSOR)
AS
BEGIN
  OPEN pCursor FOR
  SELECT STATE_FROM, STATE_TO, OPERATION FROM SELECTION_TRANSITION;
END;

procedure p$create_request(pId out number, pSelectionId in number, pQuery in clob)
as
begin
  update selection_request t set t.status = 9 where t.selection_id = pSelectionId;
  delete from selection_declaration where selection_id = pSelectionId;
  delete from selection_discrepancy where selection_id = pSelectionId;
  pId := seq_selection_request.nextval;
  insert into selection_request (request_id, selection_id, query_text, status, created)
  values (pId, pSelectionId, pQuery, 0, sysdate);
end;

procedure p$set_request_status(p_request_id number, p_status_id number)
as
begin
  update selection_request set status = p_status_id where request_id = p_request_id;
end;

procedure p$update_selection_statistic(pSelectionId in number,
  pDiscrQty in number,
  pDiscrAmt in number,
  pDeclQty in number)
as
begin
  update selection_aggregate set 
    initial_discrepancy_qty = pDiscrQty,
    initial_discrepancy_amt = pDiscrAmt,
    initial_declaration_qty = pDeclQty
  where id = pSelectionId;

   update selection set 
    discrep_count = pDiscrQty,
    decl_count = pDeclQty
  where id = pSelectionId;

  commit;
end;


PROCEDURE P$GET_REGIONS (
  pUserSID IN METODOLOG_CHECK_EMPLOYEE.SID%type, -- Идентификатор пользователя
  pData OUT SYS_REFCURSOR -- Список регионов
)
AS
BEGIN
  OPEN pData FOR
  SELECT distinct reg.S_CODE, reg.S_NAME, reg.DESCRIPTION
  FROM V$SSRF reg
    LEFT OUTER JOIN METODOLOG_CHECK_EMPLOYEE mce ON mce.Sid = pUserSID
    LEFT OUTER JOIN Metodolog_Region rr ON rr.id = mce.resp_id AND reg.s_code = rr.code;
END;

PROCEDURE P$GET_INSPECTIONS (
  pUserSID IN METODOLOG_CHECK_EMPLOYEE.SID%type, -- Идентификатор пользователя
  pData OUT SYS_REFCURSOR -- Список инспекций
  ) -- Возвращает список инспекций, доступных пользователю (для фильтра выборки)
AS
BEGIN
  OPEN pData FOR
  SELECT distinct ins.s_code, ins.s_parent_code, ins.s_name
  FROM v$inspection ins
    LEFT OUTER JOIN METODOLOG_CHECK_EMPLOYEE mce ON mce.Sid = pUserSID
    LEFT OUTER JOIN Metodolog_Region_Inspection mci ON mci.Region_Id = mce.resp_id AND mci.code = ins.s_code
	where  ins.s_type = 0 or ins.s_code in ('0400', '9901', '9971', '9972', '9973', '9974', '9975', '9976', '9977', '9978', '9979');
END;


PROCEDURE P$SEARCH_REGIONS (
  pUserSID IN USER_TO_N_SSRF.USER_SID%type, -- Идентификатор пользователя
  pSearchKey IN VARCHAR2,
  pMaxQuantity IN NUMBER,
  pData OUT SYS_REFCURSOR -- Список регионов
)
AS
BEGIN
  OPEN pData FOR
  WITH q AS (
       SELECT ROWNUM as IDX, v.*
       FROM (
              SELECT DISTINCT reg.S_CODE, reg.S_NAME
              FROM
                     V$SSRF reg
                     LEFT OUTER JOIN METODOLOG_CHECK_EMPLOYEE mce ON mce.Sid = pUserSID
                     LEFT OUTER JOIN Metodolog_Region rr ON rr.id = mce.resp_id AND reg.s_code = rr.code
              WHERE reg.S_CODE LIKE pSearchKey OR reg.S_NAME LIKE pSearchKey
              ORDER BY reg.s_name
            ) v)
    SELECT DISTINCT vw.S_CODE, vw.S_NAME, vw.DESCRIPTION
    FROM
            V$SSRF vw
            JOIN q ON q.S_CODE=vw.S_CODE
    WHERE q.IDX <= pMaxQuantity;
END;

PROCEDURE P$SEARCH_INSPECTIONS (
  pUserSID IN USER_TO_N_SSRF.USER_SID%type, -- Идентификатор пользователя
  pSearchKey IN VARCHAR2,
  pMaxQuantity IN NUMBER,
  pData OUT SYS_REFCURSOR -- Список инспекций
  ) -- Возвращает список инспекций, доступных пользователю (для фильтра выборки)
AS
BEGIN
  OPEN pData FOR
  WITH q AS (
       SELECT ROWNUM as IDX, v.*
       FROM (
            SELECT distinct ins.s_code, ins.s_parent_code, ins.s_name
            FROM
                   v$inspection ins
                   LEFT OUTER JOIN METODOLOG_CHECK_EMPLOYEE mce ON mce.Sid = pUserSID
                   LEFT OUTER JOIN Metodolog_Region_Inspection mci ON mci.Region_Id = mce.resp_id AND mci.code = ins.s_code
            WHERE ins.s_code LIKE pSearchKey OR ins.s_name LIKE pSearchKey and (ins.s_type = 0 or ins.s_code = '0400')
            ORDER BY ins.s_name) v)
   SELECT DISTINCT ins.s_code, ins.s_parent_code, ins.s_name
   FROM v$inspection ins JOIN q ON q.s_code=ins.s_code
   WHERE q.idx <= pMaxQuantity;
END;

--Возвращает данные для редактора фильтра выборки.
--Id  NUMBER  Идентификатор атрибута
--Name  VARCHAR2(255)  Наименования колонки для редактора
--AreaId  NUMBER  Идентификатор области
--TypeId  NUMBER  Идентификатор редактора атрибута
--ModelId  NUMBER  Идентификатор модели
procedure P$GET_FILTER_EDITOR_PARAMETERS
(
  pIsManual IN NUMBER,
  pCursor OUT SYS_REFCURSOR
)
as
begin

  open pCursor for
  select
    p.id,
    p.editor_name as name,
  p.aggregate_name as aggregate_name,
    p.editor_area_id as AreaId,
    p.editor_type_id as TypeId,
    p.editor_model_id as ModelId
  from SELECTION_FILTER_PARAMETER p
  left join SELECTION_FILTER_APPLICABILITY a on a.parameter_id = p.id
  where a.is_manual = pIsManual;

end;

--Возвращает допустимые значения атрибута фильтра выборки.
procedure P$GET_FILTER_PARAMETER_OPTIONS
(
   pAttributeId IN NUMBER,
   pCursor OUT SYS_REFCURSOR
)
as
begin

  open pCursor for
  select
    p.title as name,
    p.value as value
  from SELECTION_FILTER_OPTION p
  where p.parameter_id = pAttributeId;

end;


--Возвращает список атрибутов фильтра выборки
--Id  NUMBER  Идентификатор атрибута
--Name  VARCHAR2(30)  Наименование колонки агрегата, соответствующая атрибуту
procedure P$GET_FILTER_PARAMETERS
(
  pCursor OUT SYS_REFCURSOR
)
as
begin

  open pCursor for
  select
  p.id,
  p.aggregate_name as name
  from SELECTION_FILTER_PARAMETER p;
end;

PROCEDURE P$COUNT_CHECKED_DECLARATIONS (
  pSelectionId IN selection.ID%type,
  pChecked OUT NUMBER,
  pAll OUT NUMBER
  )
AS
BEGIN
  SELECT
       SUM(is_in_process),
       COUNT(1)
  INTO pChecked, pAll
  FROM selection_declaration
  WHERE SELECTION_ID = pSelectionId;
END;

PROCEDURE P$UPDATE_DECL_CHECK_STATE (
  pSelectionId IN number,
  pDeclarationId IN number,
  pUserSid in varchar2,
  pCheckState IN SELECTION_DECLARATION.IS_IN_PROCESS%type
  )
AS
v_exclude_code number;
v_status number;
v_save_date date;
BEGIN

   v_save_date := sysdate;
   if pCheckState = 1 then v_exclude_code := 0; else v_exclude_code := 5; end if;

    UPDATE SELECTION_DISCREPANCY sd
    SET IS_IN_PROCESS = pCheckState, EXCLUSION_CODE = v_exclude_code
    WHERE SELECTION_ID = pSelectionId
           and zip = pDeclarationId;
          
    UPDATE SELECTION_DECLARATION
    SET IS_IN_PROCESS = pCheckState, EXCLUSION_CODE = v_exclude_code
    WHERE
         SELECTION_ID = pSelectionId
     AND ZIP = pDeclarationId;

	 update selection set modification_date = v_save_date where id = pSelectionId;
	 update selection_aggregate set last_edited_at = v_save_date where id = pSelectionId;


    select status into v_status from selection where id = pSelectionId; 
    insert into Action_History (id, cd, status, change_date, user_sid, action) 
    values(SEQ_ACTION_HISTORY.nextval, pSelectionId, v_status, v_save_date, pUserSid, ACTION_EDIT);

	commit;
END;


PROCEDURE P$UPDATE_ALL_DECL_CHECK_STATE (
  pSelectionId IN SELECTION.ID%type,
  pCheckState IN SELECTION_DECLARATION.IS_IN_PROCESS%type,
  pUserSid in varchar2,
  pZipList in t_numbers)
AS
  v_zip_list T$ARRAY_OF_NUMBER;
  v_exclude_code number;
  v_status number;
  v_save_date date;
BEGIN
  v_save_date := sysdate;
  v_zip_list := T$ARRAY_OF_NUMBER() ;
  v_zip_list.extend(pZipList.count);

  for i in 1 .. pZipList.count loop
      v_zip_list(i) := pZipList(i);
   end loop;

  if pCheckState = 1 then v_exclude_code := 0; else v_exclude_code := 5; end if;

  UPDATE SELECTION_DECLARATION SET IS_IN_PROCESS = pCheckState, EXCLUSION_CODE = v_exclude_code
  WHERE SELECTION_ID = pSelectionId and ZIP in (select column_value from TABLE (v_zip_list));

  UPDATE SELECTION_DISCREPANCY 
  SET IS_IN_PROCESS = pCheckState, EXCLUSION_CODE = v_exclude_code
  WHERE
      SELECTION_ID = pSelectionId and is_in_process <> pCheckState and ZIP in (select column_value from TABLE (v_zip_list));

 update selection set modification_date = v_save_date where id = pSelectionId;
 update selection_aggregate set last_edited_at = v_save_date where id = pSelectionId;

  select status into v_status from selection where id = pSelectionId;
  insert into Action_History (id, cd, status, change_date, user_sid, action) 
  values(SEQ_ACTION_HISTORY.nextval, pSelectionId, v_status, v_save_date, pUserSid, ACTION_EDIT);
  commit;
END;

PROCEDURE P$COUNT_CHECKED_DISCREPANCIES (
  pSelectionId IN SELECTION.ID%type,
  pSelectionType in number,
  pCursor OUT SYS_REFCURSOR
  )
AS
BEGIN
if pSelectionType = TEMPLATE_TYPE then
 OPEN pCursor FOR SELECT
       sd.selection_id as SelectionId,
	   s.name as SelectionName, 
       SUM(sd.is_in_process) as InProcessQty,
       COUNT(1) as TotalQty
  FROM selection_discrepancy sd
  left join selection s on id = selection_id
  where sd.selection_id in (select id from selection where template_id = pSelectionId) group by selection_id, s.name;
else
 OPEN pCursor FOR SELECT
       sd.selection_id as SelectionId,
	   null as SelectionName,
       SUM(sd.is_in_process) as InProcessQty,
       COUNT(1) as TotalQty
  FROM selection_discrepancy sd
  where sd.selection_id = pSelectionId group by selection_id;
end if;
END;


PROCEDURE P$GET_FAVORITE_FILTERS (pAnalyst IN FAVORITE_FILTERS.ANALYST%type, pFavCursor OUT SYS_REFCURSOR)
AS
BEGIN
  OPEN pFavCursor FOR SELECT ID,NAME,ANALYST,FILTER FROM FAVORITE_FILTERS WHERE ANALYST = pAnalyst;
END;

PROCEDURE P$GET_FAVORITE_FILTER (pId IN FAVORITE_FILTERS.ID%type, pFavCursor OUT SYS_REFCURSOR)
AS
BEGIN
  OPEN pFavCursor FOR SELECT ID,NAME,ANALYST,FILTER FROM FAVORITE_FILTERS WHERE ID = pId;
END;

PROCEDURE P$GET_ALL_FAVORITE_FILTERS (pFavCursor OUT SYS_REFCURSOR)
AS
BEGIN
  OPEN pFavCursor FOR SELECT ID,NAME,ANALYST,FILTER FROM FAVORITE_FILTERS;
END;

PROCEDURE P$SAVE_FAVORITE_FILTER (
  pId IN OUT FAVORITE_FILTERS.ID%type,
  pName IN FAVORITE_FILTERS.NAME%type,
  pAnalyst IN FAVORITE_FILTERS.ANALYST%type,
  pFilter IN FAVORITE_FILTERS.FILTER%type)
AS
BEGIN
  IF pId IS NULL THEN
    BEGIN
      pId := SEQ_FAV_FILTER.NEXTVAL;
      INSERT INTO FAVORITE_FILTERS (ID,NAME,ANALYST,FILTER) VALUES (pId,pName,pAnalyst,pFilter);
    END;
  ELSE
    BEGIN
      UPDATE FAVORITE_FILTERS
      SET
             FILTER = pFilter
      WHERE
             ID = pId;
    END;
   END IF;
END;

PROCEDURE P$DELETE_FAVORITE_FILTER (pId IN FAVORITE_FILTERS.ID%type)
AS
BEGIN
  DELETE FROM FAVORITE_FILTERS WHERE ID = pId;
END;

PROCEDURE P$UPDATE_REGIONAL_BOUNDS(
pSelectionTemplateId in SELECTION_REGIONAL_BOUNDS.ID%type,
pInclude in SELECTION_REGIONAL_BOUNDS.INCLUDE%type,
pRegionCode in SELECTION_REGIONAL_BOUNDS.REGION_CODE%type, 
pDiscrTotal in SELECTION_REGIONAL_BOUNDS.DISCREP_TOTAL_AMT%type, 
pDiscrMin in SELECTION_REGIONAL_BOUNDS.DISCREP_MIN_AMT%type,
pDiscrGapTotal in SELECTION_REGIONAL_BOUNDS.DISCREP_GAP_TOTAL_AMT%type,
pDiscrGapMin in SELECTION_REGIONAL_BOUNDS.DISCREP_GAP_MIN_AMT%type)
AS

v_id number;

BEGIN

 begin
    select id into v_id from SELECTION_REGIONAL_BOUNDS where selection_template_id = pSelectionTemplateId and region_code = pRegionCode;
    exception when no_data_found then
      v_id := 0;
  end;

if v_id = 0 then
  v_id:=seq$selection_regional_bounds.nextval;
  insert into SELECTION_REGIONAL_BOUNDS (id, selection_template_id, include, region_code, discrep_total_amt, discrep_min_amt, discrep_gap_total_amt, discrep_gap_min_amt)
  values (v_id, pSelectionTemplateId, pinclude, pregioncode, pdiscrtotal, pdiscrmin, pdiscrgaptotal, pdiscrgapmin);
  else
  update SELECTION_REGIONAL_BOUNDS
  set include = pinclude, discrep_total_amt = pdiscrtotal, discrep_min_amt = pdiscrmin, discrep_gap_total_amt = pdiscrgaptotal, discrep_gap_min_amt = pdiscrgapmin
  where selection_template_id = pSelectionTemplateId and region_code = pRegionCode;
end if;

END;

procedure P$DELETE_SEL_BY_TEMPLATE(
pSelectionTemplateId in number)
as
v_save_date date;
begin

v_save_date := sysdate;

for item in ( select id from selection where template_id = pSelectionTemplateId)
loop

     update selection_aggregate
	 set 
	 status_id = STATUS_DELETED,
	 LAST_STATUS_CHANGED_AT = v_save_date
	 where id = item.id;

	 update selection
     set 
	 status = STATUS_DELETED,
	 status_date = v_save_date
     where id = item.id; 

	 insert into Action_History (id, cd, status, change_date, user_sid, action) 
	 values(SEQ_ACTION_HISTORY.nextval, item.id, STATUS_DELETED, v_save_date, '', ACTION_EDIT);

end loop;
 
     update selection_aggregate
	 set 
	 status_id = STATUS_DELETED,
	 LAST_STATUS_CHANGED_AT = v_save_date
	 where id = pSelectionTemplateId;

	 update selection
     set 
	 status = STATUS_DELETED,
	 status_date = v_save_date
     where id = pSelectionTemplateId; 

	 insert into Action_History (id, cd, status, change_date, user_sid, action) 
	 values(SEQ_ACTION_HISTORY.nextval, pSelectionTemplateId, STATUS_DELETED, v_save_date, '', ACTION_EDIT);

	 commit;
end;

procedure P$GET_SEL_BY_TEMPL_ID(
pSelectionTemplateId in number,
pRegions in varchar2,
pCursor out SYS_REFCURSOR)
as

begin
  OPEN pCursor FOR select 
  id, 
  template_id as TemplateId,
  name,
  creation_date as CreationDate,
  modification_date as ModificationDate,
  status_date as StatusDate,
  analytic as Analytic,
  analytic_sid as AnalyticSid,
  chief as Manager,
  chief_sid as ManagerSid,
  status, 
  type as TypeCode,
  regions
   from selection where template_id = pSelectionTemplateId and regions = pRegions and
   status not in(STATUS_CLAIM_CREATING, STATUS_DELETED, STATUS_CLAIM_CREATED);
end;

procedure P$UPDATE_AGGREGATE(pSelectionId in number,
pType in number,
pDeclQty out number,
pDiscrQty out number,
pDiscrAmt out number)
as
begin

pDeclQty := 0;
pDiscrQty := 0;
pDiscrAmt := 0;

end;

procedure P$GET_REGIONAL_BOUND(
pSelectionTemplateId in number,
pRegionCode in varchar2,
pCursor out SYS_REFCURSOR)
as
begin
open pCursor for
select 
 discrep_total_amt as DiscrTotalAmt,
 discrep_min_amt as DiscrMinAmt,
 discrep_gap_total_amt as DiscrGapTotalAmt,
 discrep_gap_min_amt as DiscrGapMinAmt
 from SELECTION_REGIONAL_BOUNDS where selection_template_id = pSelectionTemplateId and region_code = pRegionCode;
end;

procedure P$CAN_DELETE_TEMPLATE(pSelectionTemplateId in number,
								pCanDelete out number)
as
begin
select case when exists(select 1 from selection where 
template_id = pSelectionTemplateId and status in (STATUS_LOADING, STATUS_CLAIM_CREATING, STATUS_CLAIM_CREATED))
 then 0 else 1 end into pCanDelete from dual;
end;

/* процедура проверки наличия выборки(ок) в статусе "Согласовано" в списке выборок */
PROCEDURE P$EXISTS_APPROVED_SELECTIONS (
  pExists out number 
) IS 
BEGIN 
  select nvl2(max(1), 1, 0) 
    into pExists 
    from selection_aggregate 
    where status_id = STATUS_APPROVED
	  and not type = TEMPLATE_TYPE; 
END;

/* процедура подсчета выборок в статусах "Согласовано" и "На согласовании" в списке выборок */
PROCEDURE P$COUNT_APPROVED_SELECTIONS (
  pRequestApproveCount out number, 
  pApprovedCount out number 
) IS 
BEGIN 
  select sum(DECODE(status_id, STATUS_REQUEST_APPROVE, 1, 0)) 
        ,sum(DECODE(status_id, STATUS_APPROVED, 1, 0)) 
    into pRequestApproveCount 
        ,pApprovedCount 
    from selection_aggregate 
    where status_id in (STATUS_REQUEST_APPROVE, STATUS_APPROVED)
	  and not type = TEMPLATE_TYPE; 
END;

/* процедура перевода выборок из статуса "Согласовано" в статус "Формирование АТ" */
PROCEDURE P$SET_CLAIM_CREATING_STATUS 
IS 
v_save_date date;
BEGIN 

v_save_date := sysdate;

 for item in ( select id from selection where status = STATUS_APPROVED and type <> TEMPLATE_TYPE)
  loop
  
  UPDATE SELECTION_AGGREGATE SET STATUS_ID = STATUS_CLAIM_CREATING, LAST_STATUS_CHANGED_AT = v_save_date
  WHERE ID = item.id;
  
  UPDATE SELECTION SET STATUS = STATUS_CLAIM_CREATING, status_date = v_save_date
  where id = item.id;

  insert into Action_History (id, cd, status, change_date, user_sid, action) 
  values(SEQ_ACTION_HISTORY.nextval, item.id, STATUS_CLAIM_CREATING, v_save_date, '', ACTION_CLAIM_CREATING);
  end loop;

 commit;

EXCEPTION 
  when others then 
  begin 
    rollback;
    NDS2$SYS.LOG_ERROR('PAC$SELECTION.P$SET_CLAIM_CREATING_STATUS', null, sqlcode, SUBSTR(sqlerrm, 1, 512));
    raise;
  end;
END; 

PROCEDURE P$UPDATE_SEL_REGIONS(
pSelectionId in number,
pRegions in varchar2
) is

begin
update selection set regions = pRegions where id = pSelectionId;
end;


end PAC$SELECTION;
/
