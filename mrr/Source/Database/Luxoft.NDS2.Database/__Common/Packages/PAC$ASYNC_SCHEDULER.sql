﻿create or replace package NDS2_MRR_USER.PAC$ASYNC_SCHEDULER
as
   /* Запустить создание ПЗ "Ввод ответа на АИ" */
   PROCEDURE P$SUBMIT_TASK_RECLAIM_REPLY( p_doc_id number, p_explain_id number );
   
   /* Запустить создание ПЗ "Истребование документов" */
   PROCEDURE P$SUBMIT_TASK_RECLAIMATION( p_doc_id number );

   /* Запустить создание ПЗ "Анализ налогоплательщика" */
   PROCEDURE P$SUBMIT_TASK_ANALYZE_TAXPAYER( p_doc_id number );

   /* Запустить обработку нового потока ЭОД */
   PROCEDURE P$SUBMIT_PROCESS_SEOD_STREAM ( p_session_id number );

   PROCEDURE P$CLOSE_RECLAMATION_BY_EOD ( p_doc_id number, p_by_status number );
end;
/
create or replace package body NDS2_MRR_USER.PAC$ASYNC_SCHEDULER
as

   procedure UTL_LOG
   (
      v_source varchar2,
      v_code varchar2 := 0,
      v_msg varchar2 := null,
      v_entity_id varchar2 := null
   )
   as
   pragma autonomous_transaction;
   begin
      insert into NDS2_MRR_USER.system_log(id, type, site, entity_id, message_code, message, log_date)
      values(NDS2_MRR_USER.seq_sys_log_id.nextval, 0, v_source, v_entity_id, v_code, v_msg, sysdate);
      commit;
   exception when others then
      rollback;
      dbms_output.put_line('UTL_LOG:'||sqlcode||'-'||substr(sqlerrm, 0, 256));
   end;

   PROCEDURE P$SUBMIT_TASK_RECLAIM_REPLY( p_doc_id number, p_explain_id number )
   as
     v_job_id number;
   begin
     dbms_job.submit(what => 'begin  pac$user_task.P$CREATE_RECLAIM_REPLY_TASK(JOB); end;', job => v_job_id);
     insert into QUEUE_UT_RECLAIM_REPLY(JOB_ID, DOC_ID, EXPLAIN_ID) values(v_job_id, p_doc_id, p_explain_id);     
   end;
   
   /* Запустить создание ПЗ "Истребование документов" */
   PROCEDURE P$SUBMIT_TASK_RECLAIMATION( p_doc_id number )
   as
   v_job_id number;
   begin
     dbms_job.submit(what => 'begin  pac$user_task.P$CREATE_RECLAIMATION_TASK(JOB); end;', job => v_job_id);
     insert into QUEUE_UT_RECLAMATION(JOB_ID, DOC_ID) values(v_job_id, p_doc_id);
   end;

   /* Запустить создание ПЗ "Анализ налогоплательщика" */
   PROCEDURE P$SUBMIT_TASK_ANALYZE_TAXPAYER( p_doc_id number )
   as
   v_job_id number;
   begin
     dbms_job.submit(what => 'begin  pac$user_task.p$create_taxpayer_alalyze_task(JOB); end;', job => v_job_id);
     insert into QUEUE_UT_TAX_PAYER_ANALYZE(JOB_ID, DOC_ID) values(v_job_id, p_doc_id);
   end;

   /* Запустить обработку нового потока ЭОД */
   PROCEDURE P$SUBMIT_PROCESS_SEOD_STREAM
   (
    p_session_id number
   )
   as
   v_job_id number;
   begin
     insert into QUEUE_SEOD_STREAM_PROCESS(SESSION_ID, JOB_ID, CREATED, IS_PROCESSED) values(p_session_id, 1, current_timestamp, 0);

     if not PAC$LOGICAL_LOCK.P$HAS_ANY_LOCK(PAC$LOGICAL_LOCK.TYPE_SEOD_INCOME) then
        UTL_LOG('PAC$ASYNC_SCHEDULER.P$SUBMIT_PROCESS_SEOD_STREAM', 0, 'Запускаем job-а для pac$seod_exchange.P$PROCESS_SEOD_INCOME, блокировок нет', 0);

        dbms_job.submit(what => 'begin  nds2_seod.pac$seod_exchange.P$PROCESS_SEOD_INCOME; end;', job => v_job_id);

        UTL_LOG('PAC$ASYNC_SCHEDULER.P$SUBMIT_PROCESS_SEOD_STREAM', 0, 'Запущен job-а для pac$seod_exchange.P$PROCESS_SEOD_INCOME', 0);
     else
        UTL_LOG('PAC$ASYNC_SCHEDULER.P$SUBMIT_PROCESS_SEOD_STREAM', 0, 'Запуск job-а невозможен, причина: установлена блокировка на прием потоков', 0);
     end if;
   end;

   PROCEDURE P$CLOSE_RECLAMATION_BY_EOD ( p_doc_id number, p_by_status number )
   as
     v_job_id number;
   begin
     dbms_job.submit(what => 'begin  pac$user_task.P$CLOSE_RECLAIMATION_TASK_EOD(JOB); end;', job => v_job_id);
     insert into QUEUE_UT_RECLAMATION_CLOSE(JOB_ID, DOC_ID, BY_STATUS_ID) values (v_job_id, p_doc_id, p_by_status);
   end;
end;
/
