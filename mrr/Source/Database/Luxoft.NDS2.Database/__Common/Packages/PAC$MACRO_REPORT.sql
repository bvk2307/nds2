﻿CREATE OR REPLACE PACKAGE NDS2_MRR_USER.PAC$MACRO_REPORT IS

PROCEDURE P$GET_DECLARATION_BY_SONO
(
  pSonoCode in VARCHAR2,
  pCursor out SYS_REFCURSOR
);

PROCEDURE P$GET_MAP_DATA_FEDERAL(p_calc_date date, p_cursor out sys_refcursor);
PROCEDURE P$GET_MAP_DATA_DISTRICT(p_calc_date date, p_district_id number, p_cursor out sys_refcursor);
PROCEDURE P$GET_MAP_DATA_REGION(p_calc_date date, p_region_id varchar2, p_cursor out sys_refcursor);

END;
/

CREATE OR REPLACE PACKAGE BODY NDS2_MRR_USER.PAC$MACRO_REPORT IS

procedure P$GET_DECLARATION_BY_SONO
(
  pSonoCode in VARCHAR2,
  pCursor out SYS_REFCURSOR
)
as
begin

  open pCursor for
  select
    da.inn_declarant as INN
  ,da.KPP as KPP
    ,da.name_short as TAX_NAME
    ,da.FISCAL_YEAR as TAX_YEAR
    ,da.period_effective as QUARTER
    ,nvl(da.discrep_gap_qty, 0) as GAP_COUNT
    ,nvl(da.discrep_gap_amnt, 0) as GAP_SUM
    ,(case when nvl(da.nds_summanalischisl, 0) > 0 then da.nds_summanalischisl else 0 end) as NDS_CALCULATED
    ,(case when nvl(da.nds_summavych, 0) > 0 then da.nds_summavych else 0 end) as NDS_DEDUCTION
    ,nvl(da.nds_total, 0) as NDS_TOTAL
  from DECLARATION_ACTIVE da
  where da.type_code = 0 and da.sono_code = pSonoCode;

end;

PROCEDURE P$GET_MAP_DATA_FEDERAL(p_calc_date date, p_cursor out sys_refcursor)
as
    vx_prev_year_qtr number;
    vx_prev_tp_year number;
    vx_prev_tp_quarter number;
begin
    P$GET_PREV_PERIOD_QTR_YEAR( vp_date=> current_date, 
                                vp_prev_tp_year => vx_prev_tp_year,
                                vp_prev_tp_quarter => vx_prev_tp_quarter);

    vx_prev_year_qtr := vx_prev_tp_year * 10 + vx_prev_tp_quarter ;
    open  p_cursor for
    with tno_data as
    (
    select
        distinct
        reg.s_code as region_code,
        reg.s_name as region_name,
        distr.district_id as district_code,
        distr.description as district_name
    from
    v$ssrf reg
    inner join FEDERAL_DISTRICT_REGION distr_region on distr_region.region_code = reg.s_code
    inner join FEDERAL_DISTRICT distr on distr.district_id = distr_region.district_id
    )
    select
      d.district_code as aggregate_code,
      d.district_name as aggregate_name,
      e.fiscal_year,
      f$tax_period_to_quarter( e.tax_period ) as quarter,
      sum(nvl(e.decl_count_compensation, 0)) as decl_count_compensation,
      sum(nvl(e.decl_count_payment, 0)) AS decl_count_payment,
      sum(nvl(e.ndscalculated_sum_compensation, 0)) + sum(nvl(e.ndscalculated_sum_payment, 0)) as nds_calculated_sum,
      sum(nvl(e.ndsdeduction_sum_compensation, 0)) + sum(nvl(e.ndsdeduction_sum_payment, 0)) as nds_deduction_sum,
      sum(nvl(e.nds_sum_compensation, 0)) AS nds_compensation_sum,
      sum(nvl(e.nds_sum_payment, 0)) AS nds_payment_sum,
      round(nvl((sum(nvl(e.ndsdeduction_sum_compensation, 0)) + sum(nvl(e.ndsdeduction_sum_payment, 0)))/
      nullif(sum(nvl(e.ndscalculated_sum_compensation, 0)) + sum(nvl(e.ndscalculated_sum_payment, 0)), 0) ,0), 2) * 100 as weight_deduct_to_calc
    from
    report_declaration_raw e
    inner join tno_data d on d.region_code = decode(e.soun_code, '9901', '50', e.region_code)
    where
    trunc(e.submit_date) = trunc(p_calc_date)
    and to_number(e.fiscal_year||f$tax_period_to_quarter( e.tax_period ))
        between GREATEST(vx_prev_year_qtr - 20, 20144) and vx_prev_year_qtr
    group by
    d.district_code,
    d.district_name,
    e.fiscal_year,
    f$tax_period_to_quarter( e.tax_period );
end;

PROCEDURE P$GET_MAP_DATA_DISTRICT(p_calc_date date, p_district_id number, p_cursor out sys_refcursor)
 as
    vx_prev_year_qtr number;
    vx_prev_tp_year number;
    vx_prev_tp_quarter number;
begin
    P$GET_PREV_PERIOD_QTR_YEAR( vp_date=> current_date, 
                                vp_prev_tp_year => vx_prev_tp_year,
                                vp_prev_tp_quarter => vx_prev_tp_quarter);
    
    vx_prev_year_qtr := vx_prev_tp_year * 10 + vx_prev_tp_quarter ;

    open  p_cursor for
    with tno_data as
    (
    select
        distinct
        reg.s_code as region_code,
        reg.s_name as region_name,
        distr.district_id as district_code,
        distr.description as district_name
    from
    v$ssrf reg
    inner join FEDERAL_DISTRICT_REGION distr_region on distr_region.region_code = reg.s_code
    inner join FEDERAL_DISTRICT distr on distr.district_id = distr_region.district_id
    )
    select
      d.region_code as aggregate_code,
      d.region_name as aggregate_name,
      e.fiscal_year,
      f$tax_period_to_quarter( e.tax_period ) as quarter,
      sum(nvl(e.decl_count_compensation, 0)) as decl_count_compensation,
      sum(nvl(e.decl_count_payment, 0)) AS decl_count_payment,
      sum(nvl(e.ndscalculated_sum_compensation, 0)) + sum(nvl(e.ndscalculated_sum_payment, 0)) as nds_calculated_sum,
      sum(nvl(e.ndsdeduction_sum_compensation, 0)) + sum(nvl(e.ndsdeduction_sum_payment, 0)) as nds_deduction_sum,
      sum(nvl(e.nds_sum_compensation, 0)) AS nds_compensation_sum,
      sum(nvl(e.nds_sum_payment, 0)) AS nds_payment_sum,
      round(nvl((sum(nvl(e.ndsdeduction_sum_compensation, 0)) + sum(nvl(e.ndsdeduction_sum_payment, 0)))/
      nullif(sum(nvl(e.ndscalculated_sum_compensation, 0)) + sum(nvl(e.ndscalculated_sum_payment, 0)), 0) ,0), 2) * 100 as weight_deduct_to_calc
    from
    report_declaration_raw e
    inner join tno_data d on d.region_code = decode(e.soun_code, '9901', '50', e.region_code)
    where
    trunc(e.submit_date) = trunc(p_calc_date)
    and d.district_code = p_district_id
    and to_number(e.fiscal_year||f$tax_period_to_quarter( e.tax_period ))
        between GREATEST(vx_prev_year_qtr - 20, 20144) and vx_prev_year_qtr
    group by
    d.region_code,
    d.region_name,
    e.fiscal_year,
    f$tax_period_to_quarter( e.tax_period );
end;


PROCEDURE P$GET_MAP_DATA_REGION(p_calc_date date, p_region_id varchar2, p_cursor out sys_refcursor)
as
    vx_prev_year_qtr number;
    vx_prev_tp_year number;
    vx_prev_tp_quarter number;
begin
    P$GET_PREV_PERIOD_QTR_YEAR( vp_date=> current_date, 
                                vp_prev_tp_year => vx_prev_tp_year,
                                vp_prev_tp_quarter => vx_prev_tp_quarter);
    
    vx_prev_year_qtr := vx_prev_tp_year * 10 + vx_prev_tp_quarter ;
    open  p_cursor for
    select
      s.s_code as aggregate_code,
      s.s_name as aggregate_name,
      e.fiscal_year,
      f$tax_period_to_quarter( e.tax_period ) as quarter,
      sum(nvl(e.decl_count_compensation, 0)) as decl_count_compensation,
      sum(nvl(e.decl_count_payment, 0)) AS decl_count_payment,
      sum(nvl(e.ndscalculated_sum_compensation, 0)) + sum(nvl(e.ndscalculated_sum_payment, 0)) as nds_calculated_sum,
      sum(nvl(e.ndsdeduction_sum_compensation, 0)) + sum(nvl(e.ndsdeduction_sum_payment, 0)) as nds_deduction_sum,
      sum(nvl(e.nds_sum_compensation, 0)) AS nds_compensation_sum,
      sum(nvl(e.nds_sum_payment, 0)) AS nds_payment_sum,
      round(nvl((sum(nvl(e.ndsdeduction_sum_compensation, 0)) + sum(nvl(e.ndsdeduction_sum_payment, 0)))/
      nullif(sum(nvl(e.ndscalculated_sum_compensation, 0)) + sum(nvl(e.ndscalculated_sum_payment, 0)), 0) ,0), 2) * 100 as weight_deduct_to_calc
    from
    report_declaration_raw e
    inner join v$sono s on s.s_code = e.soun_code
    where
    trunc(e.submit_date) = trunc(p_calc_date)
    and decode(e.soun_code, '9901', '50', substr(e.soun_code,1,2)) = p_region_id
    and to_number(e.fiscal_year||f$tax_period_to_quarter( e.tax_period ))
        between GREATEST(vx_prev_year_qtr - 20, 20144) and vx_prev_year_qtr
    group by
    s.s_code,
    s.s_name,
    e.fiscal_year,
    f$tax_period_to_quarter( e.tax_period );

end;

END;
/
