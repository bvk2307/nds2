﻿CREATE OR REPLACE PACKAGE NDS2_MRR_USER."NDS2$METHODOLOGIST" IS

procedure SELECT_ALL_INSPECTIONS
(
  pCursor OUT SYS_REFCURSOR
);

procedure SELECT_INSPECTIONS
(
  pRegionId in Metodolog_Region.Id%type,
  pRegionCode in Metodolog_Region.Code%type,
  pContextId in Metodolog_Region.Context_Id%type,
  pCursor OUT SYS_REFCURSOR
);

procedure DELETE_INSPECTIONS
(
  pRegionId in Metodolog_Region_Inspection.Region_Id%type
);

procedure INSERT_INSPECTION
(
  pRegionId in Metodolog_Region_Inspection.Region_Id%type,
  pSounCode in Metodolog_Region_Inspection.Code%type
);

procedure ADD_REGION
(
  pRegionCode in Metodolog_Region.Code%type,
  pContextId in Metodolog_Region.Context_Id%type
);

procedure REMOVE_REGION
(
   pRegionId in Metodolog_Region.Id%type
);

function GET_INSPECTIONS_COUNT
(
  pRegionId in Metodolog_Region_Inspection.Region_Id%type
) return number;

PROCEDURE GET_REGIONS_OF_USER
(
  userSID IN metodolog_check_employee.sid%type,
  userTypeId IN metodolog_check_employee.type_id%type,
  pFavCursor OUT SYS_REFCURSOR
);

PROCEDURE GET_INSPECTIONS_OF_USER
(
  userSID IN metodolog_check_employee.sid%type,
  userTypeId IN metodolog_check_employee.type_id%type,
  pFavCursor OUT SYS_REFCURSOR
);

procedure P$GET_AUTOSELECTION_FILTER( -- Загружает текущие настройки фильтра автовыборки
  pCursor OUT SYS_REFCURSOR
  );

procedure P$UPDATE_AUTOSELECTION_FILTER( -- Сохраняет текущие настройки фильтра автовыборки
  pIncludeFilter IN CFG_AUTOSELECTION_FILTER.INCLUDE_FILTER%type, -- Фильтр расхождений автовыборок в формате XML
  pExcludeFilter IN CFG_AUTOSELECTION_FILTER.EXCLUDE_FILTER%type -- Исключающий фильтр расхождений автовыборок в формате XML
 );


END NDS2$METHODOLOGIST;
/
CREATE OR REPLACE PACKAGE BODY NDS2_MRR_USER."NDS2$METHODOLOGIST" IS

procedure SELECT_ALL_INSPECTIONS
(
  pCursor OUT SYS_REFCURSOR
)
as
begin
  open pCursor for
  select rr.Id, rr.context_id, mci.code as Code, soun.s_name as Name, 1 as Selected
  from Metodolog_Region rr
  inner join Metodolog_Region_Inspection mci on mci.region_id = rr.id
  inner join V$SONO soun on soun.s_code = mci.code
  order by rr.code, rr.id;
end;

procedure SELECT_INSPECTIONS
(
  pRegionId in Metodolog_Region.Id%type,
  pRegionCode in Metodolog_Region.Code%type,
  pContextId in Metodolog_Region.Context_Id%type,
  pCursor OUT SYS_REFCURSOR
)
as
v_count_regions numeric := 0;
v_count_inspections numeric := 0;
begin
  select count(1)
  into v_count_inspections
  from Metodolog_Region_Inspection mci
  where mci.region_id = pRegionId;

  if v_count_inspections = 0 then
    open pCursor for
    select soun.S_Code as Code, soun.S_Name as Name, 1 as Selected
    from V$SONO soun
    left join V$SSRF ssrf on ssrf.s_code = substr(soun.s_code, 1, 2)
    left join
    (
      select mci.Code as Soun_Code, reg.Code as Region_Code
      from Metodolog_Region reg
      inner join Metodolog_Region_Inspection mci on mci.region_id = reg.id
      where reg.context_id = pContextId
    ) tReg on tReg.Region_Code = ssrf.s_code and tReg.Soun_Code = soun.S_Code
    where ssrf.s_Code = pRegionCode and tReg.Soun_Code is null;
  else
    select count(1)
    into v_count_regions
    from
    (
      select 1
      from Metodolog_Region rr
      inner join Metodolog_Region_Inspection mci on mci.region_id = rr.id
      inner join Metodolog_Check_Employee mce on mce.resp_id = rr.id
      where rr.code = pRegionCode and rr.context_id = 2
      union all
      select 1
      from Metodolog_Region rr
      inner join Metodolog_Region_Inspection mci on mci.region_id = rr.id
      inner join CFG_REGION cfg on cfg.ID = rr.Id and cfg.CFG_PARAM_ID = 0 and cfg.VALUE is null
      where rr.code = pRegionCode and rr.context_id = 1
    ) t;

    if v_count_regions = 0 then
      open pCursor for
      with tSelected as
      (
        select rr.id as region_id, rr.code as region_code, mci.code as Code, soun.s_name as Name
        from Metodolog_Region rr
        inner join Metodolog_Region_Inspection mci on mci.region_id = rr.id
        inner join v$sono soun on soun.s_code = mci.code
        where rr.context_id = pContextId
      )
      select s.Code, s.Name, 1 as Selected

      from tSelected s
      where s.region_id = pRegionId
      union all
      select * from
      (
        select soun.S_Code as Code, soun.S_Name as Name, 0 as Selected
        from V$SONO soun
        left join V$SSRF ssrf on ssrf.s_code = substr(soun.s_code, 1, 2)
        left join tSelected s on s.region_code = ssrf.s_code and s.code = soun.s_code
        where ssrf.s_code = pRegionCode and s.region_id is null
      ) s;
    else
      open pCursor for
      select mci.code, soun.s_name as Name, 1 as Selected
      from Metodolog_Region rr
      inner join Metodolog_Region_Inspection mci on mci.region_id = rr.id
      inner join v$sono soun on soun.s_code = mci.code
      where rr.id = pRegionId;
    end if;
  end if;
end;

procedure DELETE_INSPECTIONS
(
  pRegionId in Metodolog_Region_Inspection.Region_Id%type
)
as
begin
  delete from Metodolog_Region_Inspection where region_id = pRegionId;
end;

procedure INSERT_INSPECTION
(
  pRegionId in Metodolog_Region_Inspection.Region_Id%type,
  pSounCode in Metodolog_Region_Inspection.Code%type
)
as
begin
   insert into Metodolog_Region_Inspection (region_id, Code) values (pRegionId, pSounCode);
end;

procedure ADD_REGION
(
  pRegionCode in Metodolog_Region.Code%type,
  pContextId in Metodolog_Region.Context_Id%type
)
as
v_count numeric := 0;
begin

  select count(1)
  into v_count
  from Metodolog_Region rr
  left join Metodolog_Region_Inspection mci on mci.region_id = rr.id
  where rr.code = pRegionCode and rr.context_id = pContextId and mci.region_id is null;

  if v_count = 0 then
    insert into Metodolog_Region(id, code, context_id) values (SEQ_METODOLOG_REGION.Nextval, pRegionCode, pContextId);
  end if;
end;

procedure REMOVE_REGION
(
   pRegionId in Metodolog_Region.Id%type
)
as
begin
  delete from Metodolog_Region where id = pRegionId;
  delete from Metodolog_Check_Employee where Resp_Id = pRegionId;
  delete from Cfg_Region where Id = pRegionId;
end;

function GET_INSPECTIONS_COUNT
(
  pRegionId in Metodolog_Region_Inspection.Region_Id%type
) return number
as
v_counter number(2) := 0;
begin
  select count(1) into v_counter from Metodolog_Region_Inspection where Region_Id = pRegionId;
  return v_counter;
end;

PROCEDURE GET_REGIONS_OF_USER
(
  userSID IN metodolog_check_employee.sid%type,
  userTypeId IN metodolog_check_employee.type_id%type,
  pFavCursor OUT SYS_REFCURSOR
)
AS
BEGIN
  OPEN pFavCursor FOR
  select mr.Code, mr.Name
  from v$metodolog_region mr
  inner join metodolog_check_employee mce on mce.resp_id = mr.id and mce.sid = userSID and mce.type_id = userTypeId
  left join metodolog_region_inspection mri on mri.region_id = mr.id
  where mr.context_id = 2 and mri.region_id is null;
END;

PROCEDURE GET_INSPECTIONS_OF_USER
(
  userSID IN metodolog_check_employee.sid%type,
  userTypeId IN metodolog_check_employee.type_id%type,
  pFavCursor OUT SYS_REFCURSOR
)
AS
BEGIN
  OPEN pFavCursor FOR
  select mri.Code, mce.type_id
  from v$metodolog_region mr
  inner join metodolog_check_employee mce on mce.resp_id = mr.id and mce.sid = userSID and mce.type_id = userTypeId
  inner join metodolog_region_inspection mri on mri.region_id = mr.id
  where mr.context_id = 2;
END;

procedure P$GET_AUTOSELECTION_FILTER( -- Загружает текущие настройки фильтра автовыборки
  pCursor OUT SYS_REFCURSOR
  )
as
begin
  open pCursor for
       select include_filter, exclude_filter, date_modified
       from CFG_AUTOSELECTION_FILTER;
end;

procedure P$UPDATE_AUTOSELECTION_FILTER( -- Сохраняет текущие настройки фильтра автовыборки
  pIncludeFilter IN CFG_AUTOSELECTION_FILTER.INCLUDE_FILTER%type, -- Фильтр расхождений автовыборок в формате XML
  pExcludeFilter IN CFG_AUTOSELECTION_FILTER.EXCLUDE_FILTER%type -- Исключающий фильтр расхождений автовыборок в формате XML
 )
as
begin
 delete from CFG_AUTOSELECTION_FILTER;
 insert into CFG_AUTOSELECTION_FILTER (INCLUDE_FILTER,EXCLUDE_FILTER,DATE_MODIFIED)
        values (pIncludeFilter, pExcludeFilter, SYSDATE);
end;

END NDS2$METHODOLOGIST;
/
