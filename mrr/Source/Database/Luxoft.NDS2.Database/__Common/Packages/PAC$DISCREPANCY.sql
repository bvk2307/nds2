﻿create or replace package NDS2_MRR_USER.PAC$DISCREPANCY is

  -- Author  : Зыкин Д. (ООО "Люксофт Профешнл" Омск)
  -- Created : 6/10/2015 1:12:21 PM
  -- Purpose : Обработка расхождений в МРР

procedure POST_IMPORT; -- Вызывается СОВ после импорта всех актуальных расхождений из МС

end PAC$DISCREPANCY;
/
create or replace package body NDS2_MRR_USER.PAC$DISCREPANCY is

procedure POST_IMPORT 
as 
begin
null;
end;

end PAC$DISCREPANCY;
/
