﻿create or replace package NDS2_MRR_USER.PAC$SUPPORT is

  /* Выполняет процедуру ликвидации инспекции с переносом всеъ ЭОДов в новую инспекцию и сдвигом рег. номеров */
  procedure P$REMOVE_SONO(
    pSonoToRemove in varchar2,
    pSonoNew in varchar2,
    pRegNumberShift number);

end PAC$SUPPORT;
/
create or replace package body NDS2_MRR_USER.PAC$SUPPORT is

  procedure P$REMOVE_SONO(
    pSonoToRemove in varchar2,
    pSonoNew in varchar2,
    pRegNumberShift number)
  as
  begin
    NDS2$SYS.LOG_INFO ( 'PAC$SUPPORT.P$REMOVE_SONO', 1, 1, 'BEGIN: pSonoToRemove = '||pSonoToRemove||'; pSonoNew = '||pSonoNew||'; pRegNumberShift = '||pRegNumberShift);
    
    NDS2_SEOD.PAC$SUPPORT.P$REMOVE_SONO(pSonoToRemove, pSonoNew, pRegNumberShift);

    update NDS2_MRR_USER.DOC
    set sono_code = pSonoNew, ref_entity_id = ref_entity_id + pRegNumberShift
    where sono_code = pSonoToRemove;

    update NDS2_MRR_USER.ASK_INVOICE_DECL_QUEUE
    set soun_code = pSonoNew, seod_decl_id = seod_decl_id + pRegNumberShift
    where soun_code = pSonoToRemove;

    commit;
    
    NDS2$SYS.LOG_INFO ( 'PAC$SUPPORT.P$REMOVE_SONO', 1, 1, 'END');
    
  exception when others then 
    rollback;
    NDS2$SYS.LOG_ERROR ( 'PAC$SUPPORT.P$REMOVE_SONO', 1, sqlcode, 'ERROR: '||substr(sqlerrm, 1, 512));
    raise;
  end;

end PAC$SUPPORT;
/
