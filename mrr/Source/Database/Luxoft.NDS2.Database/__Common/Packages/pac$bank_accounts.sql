﻿create or replace package NDS2_MRR_USER.PAC$BANK_ACCOUNTS as

  TYPE T$ARR_STR IS TABLE OF varchar2(20) INDEX BY PLS_INTEGER;

  procedure P$LOG(
    pType bank_account_log.msg_type%type,
    pLink bank_account_log.code_link%type,
    pMessage bank_account_log.message%type
    );
  
  function F$GET_SEQUENCE_ID return number;
  
  procedure P$GET_DOC_ID (pCursor out SYS_REFCURSOR);
    
  procedure P$AUTO_QUEUE;
  
  procedure P$FIND (
    pInn in varchar2,
    pKppEffective in varchar2,
    pTypeCode in number,
    pPeriodCode in varchar2,
    pFiscalYear in varchar2,
    pCursor out SYS_REFCURSOR
    );
    
  procedure P$PUSH (
    pDocId in number,
    pInn in varchar2,
    pKppEffective in varchar2,
    pReorganized in number,
    pTypeCode in number,
    pPeriodCode in varchar2,
    pFiscalYear in varchar2,
    pSonoCode in varchar2,
    pUserSid in varchar2,
    pUserName in varchar2,
    pBik in varchar2,
    pBankInn in varchar2,
    pBankKpp in varchar2,
    pBankName in varchar2,
    pBankNumber in varchar2,
    pBankFilial in varchar2,
    pAccountsText in T$ARR_STR,
    pAccounts in CLOB,
    pStatus in number,
    pXml in CLOB,
    pResult out number
    );
    
  procedure P$GET_PULL_LIMIT(pCursor out SYS_REFCURSOR);
    
  procedure P$PULL (pCursor out SYS_REFCURSOR);

  procedure P$SEARCH_ACCOUNTS (
    pInn in varchar2,
    pKpp in varchar2,
    pBik in varchar2,
    pInnBank in varchar2,
    pKppBank in varchar2,
    pBankNumber in varchar2,
    pFilialNumber in varchar2,
    pBeginDate in date,
    pEndDate in date,
    pCursor out SYS_REFCURSOR
  );
      
  procedure P$SET_STATUS (
    pId in number,
    pStatus in number
    );
    
  procedure P$UPDATE_XML(
    pId in number,
    pDocId in number,
    pAccountsText IN T$ARR_STR,
    pText in CLOB,
    pResult out number
    );
    
  procedure P$GET_SCHEMA(
    pName in varchar2,
    pCursor out SYS_REFCURSOR
  );
  
  procedure P$GET_REQUESTED_ACCOUNTS(
    pBik in varchar2,
    pBegin in date,
    pEnd in date,
    pCursor out SYS_REFCURSOR
  );
  
  procedure P$DROP_REQUEST(
    pId number
  );
  
  procedure P$GET_AVAILABLE_PERIODS(
    pInn in varchar2,
    pKpp in varchar2,
    pCursor out SYS_REFCURSOR
  );
  
end;
/

create or replace package body NDS2_MRR_USER.PAC$BANK_ACCOUNTS as

  procedure LOG(
    pType bank_account_log.msg_type%type,
    pLink bank_account_log.code_link%type,
    pMessage bank_account_log.message%type,
    pSqlCode bank_account_log.sql_code%type,
    pSqlError bank_account_log.sql_error%type
    ) as
  begin
    insert into bank_account_log (id, log_time, msg_type, code_link, message, sql_code, sql_error)
    values (seq_bank_account_log.nextval, sysdate, pType, 'PAC$BANK_ACCOUNTS.'||pLink, pMessage, pSqlCode, pSqlError);
    commit;
    exception when others then null;
  end;
  
  procedure LOG_INFO(
    pLink bank_account_log.code_link%type,
    pMessage bank_account_log.message%type
    ) as
  begin
    LOG(0, pLink, pMessage, null, null);
  end;  
  
  procedure LOG_WARNING(
    pLink bank_account_log.code_link%type,
    pMessage bank_account_log.message%type
    ) as
  begin
    LOG(1, pLink, pMessage, null, null);
  end; 
  
  procedure LOG_ERROR(
    pLink bank_account_log.code_link%type,
    pMessage bank_account_log.message%type,
    pSqlCode bank_account_log.sql_code%type,
    pSqlError bank_account_log.sql_error%type
    ) as
  begin
    LOG(2, pLink, pMessage, pSqlCode, pSqlError);
  end; 

  procedure P$LOG(
    pType bank_account_log.msg_type%type,
    pLink bank_account_log.code_link%type,
    pMessage bank_account_log.message%type
    ) as
  begin
    insert into bank_account_log (id, log_time, msg_type, code_link, message)
    values (seq_bank_account_log.nextval, sysdate, pType, '<External>.'||pLink, pMessage);
    commit;
    exception when others then null;
  end;
    
  function F$GET_SEQUENCE_ID return number as
    v_id number;
  begin
    select nvl(t.sequence_id, 1) into v_id
    from dual
    left join bank_account_sequence t on t.sequence_id is not null;
    return v_id;
  end;
  
  procedure P$GET_DOC_ID (pCursor out SYS_REFCURSOR) as
    pragma autonomous_transaction;
  begin
    open pCursor for
    select SEQ_DOC.NEXTVAL value
    from dual;
  end;  
  
  procedure P$INC_SEQUENCE_ID as
    pragma autonomous_transaction;
  begin
    LOG_INFO('P$INC_SEQUENCE_ID', 'Incrementing Configuration.BankAccountATSequenceId');
    merge into bank_account_sequence t
    using (select sysdate as dt from dual) s on (0=0)
    when not matched then
      insert (sequence_id, change_date)
      values (2, s.dt)
    when matched then
      update set
        t.sequence_id = t.sequence_id + 1,
        t.change_date = sysdate;
    commit;
    exception when others then begin
      rollback;
      LOG_ERROR('P$INC_SEQUENCE_ID', 'Incrementing failed!', sqlcode, substr(sqlerrm, 0, 512));
    end;
  end;
  
  procedure P$UNPULL as
  begin
    LOG_INFO('P$UNPULL', 'Unpulling frozen requests (with status = 2)');
    
    update BANK_ACCOUNT_REQUEST bar set bar.status_code = 1 where bar.status_code = 2;
    commit;
    
    exception when others then begin
      rollback;
      LOG_ERROR('P$UNPULL', 'Failed to unpull frozen requests', sqlcode, substr(sqlerrm, 0, 512));
    end;
  end;
  
  procedure P$AUTO_QUEUE as
    v_seq_id number;
    v_dt timestamp;
  begin
    v_seq_id := F$GET_SEQUENCE_ID;
    P$INC_SEQUENCE_ID;
    v_dt := systimestamp;
    
    LOG_INFO('P$AUTO_QUEUE', 'Collecting queue for new declarations and AT with sequence_id = '||v_seq_id);
    
    insert /*+ APPEND PARALLEL(16)*/ into BANK_ACCOUNT_REQUEST
     (id, inn, kpp_effective, reorganized, type_code, fiscal_year, period_code, sono_code,
      status_code, period_begin, period_end, request_actor, request_type,
      innbank, kppbank, namebank, bikbank, filialnumber, banknumber, accounts_list)
    select SEQ_BANK_ACCOUNT_REQUEST.NEXTVAL as id,
      t.inn
      ,t.kpp_effective
      ,t.reorganized
      ,to_char(t.type_code) as type_code
      ,t.fiscal_year
      ,t.period_code
      ,t.sono_code
      ,1 as status_code -- создано
      ,TO_DATE('01.'||d.m1||'.'||t.fiscal_year, 'dd.mm.yyyy') as period_begin
      ,least(trunc(sysdate), LAST_DAY(TO_DATE('01.'||(case when m2 > 9 then TO_CHAR(d.m2) else '0'||TO_CHAR(d.m2) end)||'.'||t.fiscal_year, 'dd.mm.yyyy'))) as period_end
      ,0 as request_actor -- автозапрос
      ,3 as request_type -- авто тип, всегда 3
      ,bank.innbank
      ,bank.kppbank
      ,bank.namebank
      ,bank.bikbank
      ,bank.filialnumber
      ,bank.banknumber
      ,'Все счета'
    from V$BANK_REQUEST_QUEUE t
    left join (select min(month) as m1, max(month) as m2, tax_period from dict_tax_period_month group by tax_period) d on d.tax_period = t.period_code
    join (select * from
      (select
        ul.INN
        ,ul.KPP
        ,ul.BIK as BIKBANK
        ,ul.NAMEKO as NAMEBANK
        ,ul.nfko as FILIALNUMBER
        ,ul.rnko as BANKNUMBER
        ,ul.innko as INNBANK
        ,ul.kppko as KPPBANK
        ,row_number() over (partition by ul.INN, ul.BIK, ul.innko, ul.kppko, ul.rnko, ul.nfko order by ul.DATEOPENSCH desc) as rn
      from BSSCHET_UL ul
      left join VIDSCHET_EXCLUDE ve on ve.vidsch = ul.vidsch
      where ul.prval = 0 and ve.vidsch is null
      union all
      select
        ip.INN
        ,null as KPP
        ,ip.BIK as BIKBANK
        ,ip.NAMEKO as NAMEBANK
        ,ip.nfko as FILIALNUMBER
        ,ip.rnko as BANKNUMBER
        ,ip.innko as INNBANK
        ,ip.kppko as KPPBANK
        ,row_number() over (partition by ip.INN, ip.BIK, ip.innko, ip.kppko, ip.rnko, ip.nfko order by ip.DATEOPENSCH desc) as rn
      from BSSCHET_IP ip
      left join VIDSCHET_EXCLUDE ve on ve.vidsch = ip.vidsch
      where ip.prval = 0 and ve.vidsch is null
     ) where rn = 1) bank on bank.inn = t.inn and bank.kpp = t.kpp
      and (t.fiscal_year < extract(year from sysdate)
           or (t.fiscal_year = extract(year from sysdate) and d.m1 <= extract(month from sysdate)));
    
    update CONFIGURATION cfg
    set cfg.value = TO_CHAR(v_dt, 'DD-MM-YYYY HH24:MI:SS')
    where cfg.parameter = 'BankAccountRequestDate';
  
    commit; 

    LOG_INFO('P$AUTO_QUEUE', 'Collecting finished.');

    LOG_INFO('P$AUTO_QUEUE', 'Gathering stats...');

    DBMS_STATS.GATHER_TABLE_STATS(
	    ownname => 'NDS2_MRR_USER',
		tabname => 'BANK_ACCOUNT_REQUEST',
		cascade => TRUE,
        degree => 16
		);
    
    LOG_INFO('P$AUTO_QUEUE', 'Gathering stats finished.');

    exception when others then begin
      rollback;
      LOG_ERROR('P$AUTO_QUEUE', 'Collecting failed!', sqlcode, substr(sqlerrm, 0, 512));
    end;
  end;
  
  procedure P$FIND (
    pInn in varchar2,
    pKppEffective in varchar2,
    pTypeCode in number,
    pPeriodCode in varchar2,
    pFiscalYear in varchar2,
    pCursor out SYS_REFCURSOR
    ) as
  begin
    open pCursor for
    select
      bar.id as value
    from BANK_ACCOUNT_REQUEST bar
    where bar.inn = pInn and bar.kpp_effective = pKppEffective and bar.period_code = pPeriodCode and bar.fiscal_year = pFiscalYear
      and bar.status_code > 2;
  end;    

procedure P$PUSH (
    pDocId in number,
    pInn in varchar2,
    pKppEffective in varchar2,
    pReorganized in number,
    pTypeCode in number,
    pPeriodCode in varchar2,
    pFiscalYear in varchar2,
    pSonoCode in varchar2,
    pUserSid in varchar2,
    pUserName in varchar2,
    pBik in varchar2,
    pBankInn in varchar2,
    pBankKpp in varchar2,
    pBankName in varchar2,
    pBankNumber in varchar2,
    pBankFilial in varchar2,
    pAccountsText in T$ARR_STR,
    pAccounts in CLOB,
    pStatus in number,
    pXml in CLOB,
    pResult out number
    ) as
    v_start DATE;
    v_end DATE;
    v_id number;
    v_lock_inn varchar2(12 char);
    v_account varchar2(20 char);
    pAccountsSmallText varchar(1014 char);
    v_count_account_max number := 20;
    v_count_account_real number;
  begin
    LOG_INFO('P$PUSH', 'Pushing request for '||pInn||'/'||pKppEffective||'-'||pBik||'-'||pTypeCode||pPeriodCode||pFiscalYear);

    select tp.inn into v_lock_inn from tax_payer tp where tp.inn = pInn and tp.kpp_effective = pKppEffective for update nowait;

    select
      TO_DATE('01.'||(case when m1 > 9 then TO_CHAR(d.m1) else '0'||TO_CHAR(d.m1) end)||'.'||pFiscalYear, 'dd.mm.yyyy'),
      least(trunc(sysdate), LAST_DAY(TO_DATE('01.'||(case when m2 > 9 then TO_CHAR(d.m2) else '0'||TO_CHAR(d.m2) end)||'.'||pFiscalYear, 'dd.mm.yyyy')))
    into v_start, v_end
    from (select min(month) as m1, max(month) as m2 from dict_tax_period_month where tax_period = pPeriodCode) d;

    v_id := SEQ_BANK_ACCOUNT_REQUEST.NEXTVAL;

    pAccountsSmallText := '';
    if (pAccountsText.count > v_count_account_max) then
       v_count_account_real := v_count_account_max;
    else
       v_count_account_real := pAccountsText.count;
    end if;
       
    for i in 1..v_count_account_real loop
      pAccountsSmallText := pAccountsSmallText || pAccountsText(i);
      if i < v_count_account_real then
        pAccountsSmallText := pAccountsSmallText || ',';
      end if;
    end loop;
    if v_count_account_real > 0 and (pAccountsText.count > v_count_account_max) then
      pAccountsSmallText := pAccountsSmallText || ' ...';
    end if;
    
    insert into BANK_ACCOUNT_REQUEST
     (id, request_date, inn, kpp_effective, reorganized, type_code, fiscal_year, period_code, sono_code,
      status_code, period_begin, period_end, request_actor, user_sid, user_name, request_type,
      innbank, kppbank, namebank, bikbank, banknumber, filialnumber, accounts_list, accounts, doc_body, doc_id)
    values (v_id, sysdate, pInn, pKppEffective, pReorganized, pTypeCode, pFiscalYear, pPeriodCode, pSonoCode,
            pStatus, v_start, v_end, 1, pUserSid, pUserName, 3,
            pBankInn, pBankKpp, pBankName, pBik, pBankNumber, pBankFilial, pAccountsSmallText, pAccounts, pXml, pDocId);

    insert into BANK_ACCOUNT_REQUEST_HISTORY (request_id, status_date, status_code)
    values (v_id, sysdate, pStatus);

     for i in 1 .. pAccountsText.count loop
      v_account := pAccountsText(i);

      select case when ra.bik is null then 0 else 1 end into v_id from dual
      left join REQUESTED_ACCOUNTS ra on ra.bik = pBik and ra.account = v_account and ra.periodbegin <= v_start and ra.periodend >= v_end
      where rownum = 1;

      if v_id != 0 then
        DBMS_STANDARD.raise_application_error(
          num => -20000,
          msg => 'Выписка по счёту уже запрошена: '||pBik||'/'||v_account||' '||
                 to_char(v_start, 'dd.mm.yyyy')||'-'||to_char(v_end, 'dd.mm.yyyy')
          );
      end if;

      insert into REQUESTED_ACCOUNTS (BIK, ACCOUNT, PERIODBEGIN, PERIODEND) values
      (pBik, v_account, v_start, v_end);
    end loop;

    pResult := 1;

    exception when others then begin
      rollback;
      LOG_ERROR('P$PUSH', 'Pushing failed!', sqlcode, substr(sqlerrm, 0, 512));
      pResult := -1;
    end;
  end;

  procedure P$GET_PULL_LIMIT(pCursor out SYS_REFCURSOR) as
  begin
    open pCursor for
    select nvl(cfg.value, 3) as value
    from dual
    left join configuration cfg on cfg.parameter = 'BankAccountPullLimit';
  end;
  
  procedure P$PULL (
    pCursor out SYS_REFCURSOR
    ) as
    v_id number;
    pragma autonomous_transaction;
  begin
    select bar.id into v_id from BANK_ACCOUNT_REQUEST bar
    where bar.status_code = 1 and rownum = 1;
    
    update BANK_ACCOUNT_REQUEST bar set bar.status_code = 2 where bar.id = v_id;
    insert into BANK_ACCOUNT_REQUEST_HISTORY (request_id, status_date, status_code)
    values (v_id, sysdate, 2);
    commit;
    
    open pCursor for
    select
      bar.Id
      ,bar.Inn
      ,tp.Kpp
      ,bar.kpp_effective as KppEffective
      ,bar.type_code as TypeCode
      ,bar.fiscal_year FiscalYear
      ,bar.period_code as PeriodCode
      ,bar.sono_code as SonoCode
      ,bar.request_type as RequestType
      ,bar.request_actor as RequestActor
      ,bar.period_begin as PeriodBegin
      ,bar.period_end as PeriodEnd
      ,bar.bikbank as bik
      ,bar.innbank
      ,bar.kppbank
      ,bar.banknumber
      ,bar.filialnumber
      ,bar.accounts
    from BANK_ACCOUNT_REQUEST bar
    join tax_payer tp on tp.inn = bar.inn and tp.kpp_effective = bar.kpp_effective
    where bar.id = v_id;

    exception
      when no_data_found then null;
      when others then
      LOG_ERROR('P$PULL', 'Pull failed!', sqlcode, substr(sqlerrm, 0, 512));
  end;
    
  procedure P$SEARCH_ACCOUNTS (
    pInn in varchar2,
    pKpp in varchar2,
    pBik in varchar2,
    pInnBank in varchar2,
    pKppBank in varchar2,
    pBankNumber in varchar2,
    pFilialNumber in varchar2,
    pBeginDate in date,
    pEndDate in date,
    pCursor out SYS_REFCURSOR
  ) as
  begin
    open pCursor for
    select
      ul.BIK,
      ul.INNBANK,
      ul.KPPBANK,
      ul.BANKNUMBER,
      ul.FILIALNUMBER,
      ul.ACCOUNT
    from V$UL_BANK_ACCOUNT_BRIEF ul
    left join REQUESTED_ACCOUNTS ra on ra.bik = ul.bik and ra.account = ul.account and ra.periodbegin <= pBeginDate and ra.periodend >= pEndDate
    where ul.INN = pInn and ul.KPP = pKPP
      and ul.BIK = pBik and ul.INNBANK = pInnBank and ul.KppBank = pKppBank and ul.BANKNUMBER = pBankNumber and ul.FILIALNUMBER = pFilialNumber
      and ul.ACCOUNTTYPE = 'рубли'
      and (ul.CLOSEDATE >= pBeginDate or ul.CLOSEDATE is null) and (ul.OPENDATE <= pEndDate or ul.OPENDATE is null) and ra.account is null
    union all
    select
      ip.BIK,
      ip.INNBANK,
      ip.KPPBANK,
      ip.BANKNUMBER,
      ip.FILIALNUMBER,
      ip.ACCOUNT
    from V$IP_BANK_ACCOUNT_BRIEF ip
    left join REQUESTED_ACCOUNTS ra on ra.bik = ip.bik and ra.account = ip.account and ra.periodbegin <= pBeginDate and ra.periodend >= pEndDate
    where ip.INN = pInn
      and ip.BIK = pBik and ip.INNBANK = pInnBank and ip.KppBank = pKppBank and ip.BANKNUMBER = pBankNumber and ip.FILIALNUMBER = pFilialNumber
      and ip.ACCOUNTTYPE = 'рубли'
      and (ip.CLOSEDATE > pBeginDate or ip.CLOSEDATE is null) and (ip.OPENDATE <= pEndDate or ip.OPENDATE is null) and ra.account is null;
  end;
    
  procedure P$SET_STATUS (
    pId in number,
    pStatus in number
    ) as
    pragma autonomous_transaction;
  begin
    LOG_INFO('P$SET_STATUS', 'Set status = '||pStatus||' for '||pId);
    
    update BANK_ACCOUNT_REQUEST bar set
        bar.status_code = pStatus
    where bar.id = pId;

    insert into BANK_ACCOUNT_REQUEST_HISTORY (request_id, status_date, status_code)
    values (pId, sysdate, pStatus);
      
    commit;

    exception when others then begin
      rollback;
      LOG_ERROR('P$SET_STATUS', 'Set status failed!', sqlcode, substr(sqlerrm, 0, 512));
    end;
  end;
    
  procedure P$UPDATE_XML(
    pId in number,
    pDocId in number,
    pAccountsText IN T$ARR_STR,
    pText in CLOB,
    pResult out number
    ) as
    v_bik varchar2(9 char);
    v_start date;
    v_end date;
    v_inn varchar2(12 char);
    v_kpp varchar2(12 char);
    v_lock_inn varchar2(12 char);
    pAccountsSmallText varchar(1014 char);
    v_count_account_max number := 20;
    v_count_account_real number;
  begin
    LOG_INFO('P$UPDATE_XML', 'Update XML for '||pId);

    select
      bar.inn, bar.kpp_effective, bar.bikbank, bar.period_begin, bar.period_end
    into
      v_inn, v_kpp, v_bik, v_start, v_end
    from BANK_ACCOUNT_REQUEST bar
    where
      bar.id = pId;

    select tp.inn into v_lock_inn from tax_payer tp where tp.inn = v_inn and tp.kpp_effective = v_kpp for update nowait;

    pAccountsSmallText := '';
    if (pAccountsText.count > v_count_account_max) then
       v_count_account_real := v_count_account_max;
    else
       v_count_account_real := pAccountsText.count;
    end if;
       
    for i in 1..v_count_account_real loop
      pAccountsSmallText := pAccountsSmallText || pAccountsText(i);
      if i < v_count_account_real then
        pAccountsSmallText := pAccountsSmallText || ',';
      end if;
    end loop;
    if v_count_account_real > 0 and (pAccountsText.count > v_count_account_max) then
      pAccountsSmallText := pAccountsSmallText || ' ...';
    end if;

    update BANK_ACCOUNT_REQUEST bar set
      bar.doc_body = pText,
      bar.request_date = trunc(sysdate),
      bar.status_code = 3,
      bar.accounts_list = pAccountsSmallText,
      bar.doc_id = pDocId
    where
      bar.id = pId;

    for i in 1..pAccountsText.count loop
      insert into REQUESTED_ACCOUNTS (BIK, ACCOUNT, PERIODBEGIN, PERIODEND) values
      (v_bik, pAccountsText(i), v_start, v_end);
    end loop;

    pResult := 1;

    exception when others then begin
      rollback;
      LOG_ERROR('P$UPDATE_XML', 'Update failed!', sqlcode, substr(sqlerrm, 0, 512));
      pResult := -1;
    end;
  end;
    
  procedure P$GET_SCHEMA(
    pName in varchar2,
    pCursor out SYS_REFCURSOR
  ) as
  begin
    open pCursor for
    select schema value from SYS.all_xml_schemas
    where schema_url = pName;
  end;
  
  procedure P$GET_REQUESTED_ACCOUNTS(
    pBik in varchar2,
    pBegin in date,
    pEnd in date,
    pCursor out SYS_REFCURSOR
  ) as
  begin
    open pCursor for
    select ra.account as value from requested_accounts ra
    where ra.bik = pBik and ra.periodbegin <= pBegin and ra.periodend >= pEnd;
  end;
  
  procedure P$DROP_REQUEST(
    pId number
  ) as
  begin
    LOG_INFO('P$DROP_REQUEST', 'Delete request #'||pId);
    delete from bank_account_request bar where bar.id = pId;
    commit;
    exception when others then begin
      rollback;
      LOG_ERROR('P$DROP_REQUEST', 'Delete failed!', sqlcode, substr(sqlerrm, 0, 512));
    end;
  end;
  
  procedure P$GET_AVAILABLE_PERIODS(
    pInn in varchar2,
    pKpp in varchar2,
    pCursor out SYS_REFCURSOR
  ) as
  begin
    open pCursor for
    select
      t.FiscalYear, t.PeriodCode, t.Description, t.Quarter, t.Month
    from(
      select
        case when dp.id is null then da.fiscal_year else to_char(dp.god) end  as FiscalYear,
        dtp.code as PeriodCode,
        dtp.description,
        dtp.quarter,
        dtp.month,
        row_number() over (partition by da.fiscal_year, da.period_code order by da.correction_number_effective) as rn
      from DECLARATION_HISTORY da
      left join NDS2_SEOD.SEOD_KNP knp on knp.declaration_reg_num = da.reg_number and knp.ifns_code = da.sono_code and knp.completion_date is null
      left join V$ASKDECLPERIOD dp on dp.iddecl = da.ask_id
      join DICT_TAX_PERIOD dtp on dtp.code = case when dp.id is null then da.period_code else '2'||dp.kvartal end
      where da.inn_contractor = pInn and da.kpp_contractor = pKpp and da.type_code = 0
        and to_number(da.fiscal_year) > 2014) t
    where t.rn = 1 and to_date('01.'||case when t.Month = 0 then t.Quarter*3-2 else t.Month end||'.'||t.FiscalYear, 'DD.MM.YYYY') < sysdate;
  end;
    
end;
/
