﻿create or replace package NDS2_MRR_USER.PAC$SOV_REQUEST is

 procedure P$GET_INVOICE_REQUEST (
   pId in SOV_INVOICE_REQUEST.ID%type,
   pResult out SYS_REFCURSOR);

 procedure P$SEARCH_INVOICE_REQUEST (
   pDeclarationId in SOV_INVOICE_REQUEST.ZIP%type,
   pPartition in SOV_INVOICE_REQUEST.PARTITION_NUMBER%type,
   pResult out SYS_REFCURSOR
   );


end PAC$SOV_REQUEST;
/

create or replace package body NDS2_MRR_USER.PAC$SOV_REQUEST is

 procedure P$STAT_INVOICE_REQUEST(
   pDeclarationId in SOV_INVOICE_REQUEST.ZIP%type,
   pPartition in SOV_INVOICE_REQUEST.PARTITION_NUMBER%type
   ) as
 pragma autonomous_transaction;
   v_status number(2);
 begin
   select status into v_status from (
     select nvl(status, 0) as status from dual
     left join SOV_INVOICE_REQUEST x on x.zip = pDeclarationId and x.partition_number = pPartition
     order by requestdate desc)
   where rownum = 1;
  
   if (v_status = 2) then
     begin
       insert into STAT_INVOICE_REQUEST (REQUEST_DATE, ZIP, CHAPTER) values (sysdate, pDeclarationId, pPartition);
       commit;
       exception when others then null;
     end;
   end if;
  end;   
   

 procedure P$GET_INVOICE_REQUEST (
   pId in SOV_INVOICE_REQUEST.ID%type,
   pResult out SYS_REFCURSOR)
 as
 begin
   open pResult for 
   select 
      id, 
      status,
      price_buy_amount_sum,
      price_buy_nds_amount_sum,
      price_sell_sum,
      price_sell_18_sum,
      price_sell_10_sum,
      price_sell_0_sum,
      price_nds_18_sum,
      price_nds_10_sum,
      price_tax_free_sum,
      price_total_sum,
      price_nds_total_sum 
   from SOV_INVOICE_REQUEST where id = pId;
 end;

 procedure P$SEARCH_INVOICE_REQUEST (
   pDeclarationId in SOV_INVOICE_REQUEST.ZIP%type,
   pPartition in SOV_INVOICE_REQUEST.PARTITION_NUMBER%type,
   pResult out SYS_REFCURSOR
   )
 as
 begin
   P$STAT_INVOICE_REQUEST(pDeclarationId, pPartition);
 
   open pResult for
   select 
      id, 
      status,
      price_buy_amount_sum,
      price_buy_nds_amount_sum,
      price_sell_sum,
      price_sell_18_sum,
      price_sell_10_sum,
      price_sell_0_sum,
      price_nds_18_sum,
      price_nds_10_sum,
      price_tax_free_sum,
      price_total_sum,
      price_nds_total_sum         
   from SOV_INVOICE_REQUEST
   where zip = pDeclarationId and partition_number = pPartition
   order by requestdate desc;

 end;

end PAC$SOV_REQUEST;
/
