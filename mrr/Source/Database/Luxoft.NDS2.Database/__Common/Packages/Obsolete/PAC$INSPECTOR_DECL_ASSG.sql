﻿create or replace package NDS2_MRR_USER.PAC$INSPECTOR_DECL_ASSG
as
  procedure GET_INSPECTOR_SONO_ASSG(p_inspector_cursor OUT SYS_REFCURSOR);
  
  procedure CLEAR_NON_DISTRIB_DECLS(p_s_code NDS2_MRR_USER.DECLARATION_ACTIVE.SOUN_CODE%type);
  
  procedure GET_FREE_ASSIGNMENTS(
    p_s_code NDS2_MRR_USER.IDA_INSPECTOR_SONO_ASSG.S_CODE%type,
    p_inspector_cursor OUT SYS_REFCURSOR);
  
  procedure DEACTIVATE_INSPECTOR(
    p_sid NDS2_MRR_USER.IDA_INSPECTOR.SID%type,
    p_s_code NDS2_MRR_USER.IDA_INSPECTOR_SONO_ASSG.S_CODE%type,
    p_updated_by NDS2_MRR_USER.IDA_INSPECTOR.INSERTED_BY%type);
  
  procedure ADD_INSPECTOR(
    p_sid NDS2_MRR_USER.IDA_INSPECTOR.SID%type,
    p_name  NDS2_MRR_USER.IDA_INSPECTOR.NAME%type,
    p_employee_num NDS2_MRR_USER.IDA_INSPECTOR.EMPLOYEE_NUM%type,
    p_s_code NDS2_MRR_USER.IDA_INSPECTOR_SONO_ASSG.S_CODE%type,
    p_updated_by NDS2_MRR_USER.IDA_INSPECTOR.INSERTED_BY%type);
    
  procedure GET_INSPECTORS_WORKLOAD(
    p_s_code NDS2_MRR_USER.IDA_INSPECTOR_SONO_ASSG.S_CODE%type,
    p_inspector_cursor OUT SYS_REFCURSOR);
  
  procedure GET_DEFAULT_WORKLOAD(
    p_s_code NDS2_MRR_USER.IDA_INSPECTOR_SONO_ASSG.S_CODE%type,
    p_workload_cursor OUT SYS_REFCURSOR);
  
  procedure SET_INSPECTOR_WORKLOAD(
      p_sid NDS2_MRR_USER.IDA_INSPECTOR.SID%type,
      p_s_code NDS2_MRR_USER.IDA_INSPECTOR_SONO_ASSG.S_CODE%type,
      p_max_payment_cap NDS2_MRR_USER.IDA_INSPECTOR_WORKLOAD.MAX_PAYMENT_CAPACITY%type,
      p_max_comp_cap NDS2_MRR_USER.IDA_INSPECTOR_WORKLOAD.MAX_COMPENSATION_CAPACITY%type,
      p_has_payment_perm NDS2_MRR_USER.IDA_INSPECTOR_WORKLOAD.PAYMENT_DECL_PERM%type,
      p_has_compensation_perm NDS2_MRR_USER.IDA_INSPECTOR_WORKLOAD.COMPENSATION_DECL_PERM%type,
      p_updated_by NDS2_MRR_USER.IDA_INSPECTOR_WORKLOAD.UPDATED_BY%type,
      p_is_paused NDS2_MRR_USER.IDA_INSPECTOR_WORKLOAD.IS_PAUSED%type,
      p_is_active out NDS2_MRR_USER.IDA_INSPECTOR_WORKLOAD.IS_ACTIVE%type);
   
  -- p_result - интерпретировать также как и в IS_WKLD_DEACTIVATION_DISABLED
  procedure CLEAR_INSPECTOR_WORK_LOAD(
      p_sid NDS2_MRR_USER.IDA_INSPECTOR.SID%type,
      p_s_code NDS2_MRR_USER.IDA_INSPECTOR_WORKLOAD.S_CODE%type,
      p_updated_by NDS2_MRR_USER.IDA_INSPECTOR_WORKLOAD.UPDATED_BY%type,
      p_is_sono_assg_active out NDS2_MRR_USER.IDA_INSPECTOR_SONO_ASSG.IS_ACTIVE%type,
      p_result out number);
      
  procedure SET_DEFAULT_WORKLOAD(
      p_s_code NDS2_MRR_USER.IDA_DFLT_INSPECTOR_WORKLOAD.S_CODE%type,
      p_has_payment_perm NDS2_MRR_USER.IDA_DFLT_INSPECTOR_WORKLOAD.PAYMENT_DECL_PERM%type,
      p_max_payment_cap NDS2_MRR_USER.IDA_DFLT_INSPECTOR_WORKLOAD.MAX_PAYMENT_CAPACITY%type,
      p_has_compensation_perm NDS2_MRR_USER.IDA_DFLT_INSPECTOR_WORKLOAD.COMPENSATION_DECL_PERM%type,
      p_max_comp_cap NDS2_MRR_USER.IDA_DFLT_INSPECTOR_WORKLOAD.MAX_COMPENSATION_CAPACITY%type,
      p_updated_by NDS2_MRR_USER.IDA_DFLT_INSPECTOR_WORKLOAD.UPDATED_BY%type,
      p_is_active NDS2_MRR_USER.IDA_DFLT_INSPECTOR_WORKLOAD.IS_ACTIVE%type);
      
  procedure GET_ACTUAL_INSPECTOR_WORKLOADS(
          p_s_code NDS2_MRR_USER.IDA_INSPECTOR_WORKLOAD.S_CODE%type,
          p_act_wl_cursor OUT SYS_REFCURSOR);
          
  procedure DISTRIBUTE_INDIVIDUAL_DECL(
     p_s_code NDS2_MRR_USER.IDA_INSPECTOR_WORKLOAD.S_CODE%type);
     
  procedure GET_DECLS_FOR_AUTO_DISTR(p_s_code NDS2_MRR_USER.IDA_INSPECTOR_WORKLOAD.S_CODE%type,
                                     p_decl_cursor OUT SYS_REFCURSOR);
                                     
  procedure ASSIGN_DECLARATION(p_id NDS2_MRR_USER.DECLARATION_ACTIVE.ID%type,
                               p_sid NDS2_MRR_USER.IDA_INSPECTOR.SID%type);
                               
  procedure ADD_NON_ASSIGNED_DECL(p_id NDS2_MRR_USER.DECLARATION_ACTIVE.ID%type);
  
  procedure GET_INSPECTOR(p_s_code NDS2_MRR_USER.IDA_INSPECTOR_WORKLOAD.S_CODE%type,
                          p_sid NDS2_MRR_USER.IDA_INSPECTOR.SID%type,
                          p_inspector_cursor OUT SYS_REFCURSOR);

  procedure ADD_HISTORY_ASSIGN_DECLARATION
  (
    p_id NDS2_MRR_USER.DECLARATION_ACTIVE.ID%type,
    p_sid NDS2_MRR_USER.IDA_INSPECTOR.SID%type
   );

end;
/
create or replace package body NDS2_MRR_USER.PAC$INSPECTOR_DECL_ASSG
as
  --Допустимо ли снимать нагрузку с инспектора
  -- 0 - допустимо 1 - есть закрепленные декларации
  -- 2 - есть привязка к выделенным НП
  function IS_WKLD_DEACTIVATION_DISABLED
    (
      p_sid NDS2_MRR_USER.IDA_INSPECTOR.SID%type,
      p_s_code NDS2_MRR_USER.IDA_INSPECTOR_WORKLOAD.S_CODE%type 
    )
   RETURN NUMBER 
   as 
    v_result number;
   begin
    begin
             select 1 into v_result
               from NDS2_MRR_USER.DECLARATION_OWNER do
         inner join NDS2_MRR_USER.DECLARATION_ACTIVE da
                 on do.DECLARATION_ID = da.id
          left join NDS2_MRR_USER.SEOD_KNP knp
                 on knp.DECLARATION_REG_NUM = da.SEOD_DECL_ID
              where do.INSPECTOR_SID = p_sid and rownum=1 and
                    knp.completion_date is null;
    EXCEPTION WHEN NO_DATA_FOUND THEN
      v_result := 0;
    end;
    begin
     if v_result = 0 then
       select 2 into v_result
         from NDS2_MRR_USER.IDA_TAX_PAYER_INDIVIDUAL_ASSG ia
        where ia.SID = p_sid and ia.S_CODE = p_s_code and ia.is_active = 1 and rownum=1;
     end if;
    EXCEPTION WHEN NO_DATA_FOUND THEN
       v_result := 0;
    end;
    return v_result;
   end;
   
  procedure GET_INSPECTOR_SONO_ASSG(
    p_inspector_cursor OUT SYS_REFCURSOR)
  as 
  begin
      open p_inspector_cursor for
    select ia.*, ins.NAME, ins.EMPLOYEE_NUM
      from NDS2_MRR_USER.IDA_INSPECTOR ins 
inner join NDS2_MRR_USER.IDA_INSPECTOR_SONO_ASSG ia
        on ins.sid = ia.sid     
     where ins.IS_ACTIVE = 1 and ia.IS_ACTIVE = 1;
  end;
  
  procedure GET_FREE_ASSIGNMENTS(
    p_s_code NDS2_MRR_USER.IDA_INSPECTOR_SONO_ASSG.S_CODE%type,
    p_inspector_cursor OUT SYS_REFCURSOR)
  as
  begin
      open p_inspector_cursor for
    select ia.*, ins.NAME, ins.EMPLOYEE_NUM
      from NDS2_MRR_USER.IDA_INSPECTOR ins 
inner join NDS2_MRR_USER.IDA_INSPECTOR_SONO_ASSG ia
        on ins.sid = ia.sid 
     where ins.IS_ACTIVE = 1 and ia.IS_ACTIVE =1 
       and not exists(
                         select 1 
                           from NDS2_MRR_USER.IDA_INSPECTOR_WORKLOAD wl
                          where wl.SID = ia.SID and wl.S_CODE = ia.S_CODE
                                and wl.IS_ACTIVE = 1)      
       and ia.S_CODE = p_s_code
       order by ins.EMPLOYEE_NUM;
  end;
  
  procedure DEACTIVATE_INSPECTOR(
    p_sid NDS2_MRR_USER.IDA_INSPECTOR.SID%type,
    p_s_code NDS2_MRR_USER.IDA_INSPECTOR_SONO_ASSG.S_CODE%type,
    p_updated_by NDS2_MRR_USER.IDA_INSPECTOR.INSERTED_BY%type)
  as
  v_activeAssgCount number;
  begin
      update NDS2_MRR_USER.IDA_INSPECTOR_SONO_ASSG
         set IS_ACTIVE = 0,
             UPDATE_DATE = SYSDATE,
             UPDATED_BY = p_updated_by
       where SID = p_sid and S_CODE = p_s_code;
       
      select count(*) into v_activeAssgCount
        from NDS2_MRR_USER.IDA_INSPECTOR_SONO_ASSG
       where SID = p_sid and IS_ACTIVE = 1;
       
      if v_activeAssgCount = 0 then
         update NDS2_MRR_USER.IDA_INSPECTOR 
            set IS_ACTIVE = 0,
                UPDATED_BY = p_updated_by,
                UPDATE_DATE = SYSDATE
          where SID = p_sid ;
       end if;
  end;
  
  procedure ADD_INSPECTOR(
    p_sid NDS2_MRR_USER.IDA_INSPECTOR.SID%type,
    p_name  NDS2_MRR_USER.IDA_INSPECTOR.NAME%type,
    p_employee_num NDS2_MRR_USER.IDA_INSPECTOR.EMPLOYEE_NUM%type,
    p_s_code NDS2_MRR_USER.IDA_INSPECTOR_SONO_ASSG.S_CODE%type,
    p_updated_by NDS2_MRR_USER.IDA_INSPECTOR.INSERTED_BY%type)
  as
  begin
    --------------Создаем/обновляем запись об инспекторе---------------------
           merge into NDS2_MRR_USER.IDA_INSPECTOR insp using 
                (   
                    select p_sid SID, p_name NAME, 
                           p_employee_num EMP_NUM, 
                           p_updated_by UPDATED_BY
                      from dual   
                ) assg 
                on (assg.SID = insp.SID)
    when matched then update 
                         set insp.NAME = assg.NAME,
                             insp.EMPLOYEE_NUM = assg.EMP_NUM,
                             insp.IS_ACTIVE = 1,
                             insp.UPDATE_DATE = SYSDATE,
                             insp.UPDATED_BY = assg.UPDATED_BY
when not matched then insert 
                        (
                             insp.SID,
                             insp.NAME,insp.EMPLOYEE_NUM,insp.IS_ACTIVE,
                             insp.INSERT_DATE, insp.INSERTED_BY,
                             insp.UPDATE_DATE, insp.UPDATED_BY                 
                        )
                      values 
                        (
                             assg.SID,
                             assg.Name, assg.EMP_NUM,1,
                             SYSDATE, assg.UPDATED_BY,
                             SYSDATE, assg.UPDATED_BY
                        );
 -----------------Создаем/обновляем привязку инспектора к ИФНС-----------------            
           merge into NDS2_MRR_USER.IDA_INSPECTOR_SONO_ASSG ia using 
                (   
                    select p_sid SID, p_s_code S_CODE
                      from dual   
                ) assg
                on (assg.sid = ia.sid and assg.S_CODE = ia.S_CODE)
    when matched then update 
                         set ia.IS_ACTIVE = 1,
                             ia.UPDATE_DATE = SYSDATE,
                             ia.UPDATED_BY = p_updated_by
when not matched then insert 
                           (
                             ia.SID,ia.S_CODE,ia.IS_ACTIVE,
                             ia.INSERT_DATE,ia.UPDATE_DATE,
                             ia.INSERTED_BY, ia.UPDATED_BY              
                           )
                      values 
                           (
                             assg.sid, assg.S_CODE,1,
                             SYSDATE, SYSDATE,
                             p_updated_by,p_updated_by
                           );     
  end;
  
  procedure GET_INSPECTORS_WORKLOAD(
    p_s_code NDS2_MRR_USER.IDA_INSPECTOR_SONO_ASSG.S_CODE%type,
    p_inspector_cursor OUT SYS_REFCURSOR)
  as 
  begin
      open p_inspector_cursor for
    select ins.SID,
           ins.name,
           ins.employee_num,
           ia.is_active,
           ins.insert_date,
           ins.inserted_by,
           ins.update_date,
           ins.updated_by,
           wl.S_CODE, 
           wl.PAYMENT_DECL_PERM,
           wl.COMPENSATION_DECL_PERM, 
           wl.MAX_PAYMENT_CAPACITY,
           wl.MAX_COMPENSATION_CAPACITY,
           wl.IS_PAUSED,
           wl.IS_ACTIVE WL_IS_ACTIVE,
           wl.INSERT_DATE WL_INSERT_DATE, 
           wl.INSERTED_BY WL_INSERTED_BY,
           wl.UPDATE_DATE WL_UPDATE_DATE, 
           wl.UPDATED_BY WL_UPDATED_BY
      from NDS2_MRR_USER.IDA_INSPECTOR ins 
inner join NDS2_MRR_USER.IDA_INSPECTOR_WORKLOAD wl
        on ins.SID = wl.SID 
inner join NDS2_MRR_USER.IDA_INSPECTOR_SONO_ASSG ia
        on ia.SID = wl.SID and ia.S_CODE = wl.S_CODE
   where wl.IS_ACTIVE = 1 and wl.S_CODE = p_s_code
order by ins.EMPLOYEE_NUM; 
  end;
  
  procedure GET_DEFAULT_WORKLOAD(
    p_s_code NDS2_MRR_USER.IDA_INSPECTOR_SONO_ASSG.S_CODE%type,
    p_workload_cursor OUT SYS_REFCURSOR)
  as
  begin
     open p_workload_cursor for
   select dwl.*
     from NDS2_MRR_USER.IDA_DFLT_INSPECTOR_WORKLOAD dwl
    where dwl.s_code = p_s_code and dwl.is_active = 1;
  end;
  
  procedure SET_DEFAULT_WORKLOAD(
      p_s_code NDS2_MRR_USER.IDA_DFLT_INSPECTOR_WORKLOAD.S_CODE%type,
      p_has_payment_perm NDS2_MRR_USER.IDA_DFLT_INSPECTOR_WORKLOAD.PAYMENT_DECL_PERM%type,
      p_max_payment_cap NDS2_MRR_USER.IDA_DFLT_INSPECTOR_WORKLOAD.MAX_PAYMENT_CAPACITY%type,
      p_has_compensation_perm NDS2_MRR_USER.IDA_DFLT_INSPECTOR_WORKLOAD.COMPENSATION_DECL_PERM%type,
      p_max_comp_cap NDS2_MRR_USER.IDA_DFLT_INSPECTOR_WORKLOAD.MAX_COMPENSATION_CAPACITY%type,
      p_updated_by NDS2_MRR_USER.IDA_DFLT_INSPECTOR_WORKLOAD.UPDATED_BY%type,
      p_is_active NDS2_MRR_USER.IDA_DFLT_INSPECTOR_WORKLOAD.IS_ACTIVE%type
  )
  as
  begin
      update NDS2_MRR_USER.IDA_DFLT_INSPECTOR_WORKLOAD 
         set MAX_PAYMENT_CAPACITY = p_max_payment_cap,
             MAX_COMPENSATION_CAPACITY = p_max_comp_cap,
             PAYMENT_DECL_PERM = p_has_payment_perm,
             COMPENSATION_DECL_PERM = p_has_compensation_perm,
             UPDATED_BY = p_updated_by,
             UPDATE_DATE = SYSDATE,
             IS_ACTIVE = p_is_active
       where S_CODE = p_s_code;
       if sql%rowcount = 0 then
            insert into NDS2_MRR_USER.IDA_DFLT_INSPECTOR_WORKLOAD 
            (
              S_CODE,
              MAX_PAYMENT_CAPACITY,
              MAX_COMPENSATION_CAPACITY,
              PAYMENT_DECL_PERM,
              COMPENSATION_DECL_PERM,
              UPDATED_BY,
              INSERTED_BY,
              IS_ACTIVE
            )
            values
            (
              p_s_code,
              p_max_payment_cap,
              p_max_comp_cap,
              p_has_payment_perm,
              p_has_compensation_perm,
              p_updated_by,
              p_updated_by,
              p_is_active
            );
       end if;
  end;
  
  procedure SET_INSPECTOR_WORKLOAD(
      p_sid NDS2_MRR_USER.IDA_INSPECTOR.SID%type,
      p_s_code NDS2_MRR_USER.IDA_INSPECTOR_SONO_ASSG.S_CODE%type,
      p_max_payment_cap NDS2_MRR_USER.IDA_INSPECTOR_WORKLOAD.MAX_PAYMENT_CAPACITY%type,
      p_max_comp_cap NDS2_MRR_USER.IDA_INSPECTOR_WORKLOAD.MAX_COMPENSATION_CAPACITY%type,
      p_has_payment_perm NDS2_MRR_USER.IDA_INSPECTOR_WORKLOAD.PAYMENT_DECL_PERM%type,
      p_has_compensation_perm NDS2_MRR_USER.IDA_INSPECTOR_WORKLOAD.COMPENSATION_DECL_PERM%type,
      p_updated_by NDS2_MRR_USER.IDA_INSPECTOR_WORKLOAD.UPDATED_BY%type,
      p_is_paused NDS2_MRR_USER.IDA_INSPECTOR_WORKLOAD.IS_PAUSED%type,
      p_is_active out NDS2_MRR_USER.IDA_INSPECTOR_WORKLOAD.IS_ACTIVE%type)
  as
  v_recordExists number;
  begin
    select count(*) into v_recordExists
      from NDS2_MRR_USER.IDA_INSPECTOR_WORKLOAD wl 
     where wl.sid = p_sid and wl.s_code = p_s_code;
    if v_recordExists > 0 then
      update NDS2_MRR_USER.IDA_INSPECTOR_WORKLOAD 
         set MAX_PAYMENT_CAPACITY = p_max_payment_cap,
             MAX_COMPENSATION_CAPACITY = p_max_comp_cap,
             PAYMENT_DECL_PERM = p_has_payment_perm,
             COMPENSATION_DECL_PERM = p_has_compensation_perm,
             UPDATED_BY = p_updated_by,
             UPDATE_DATE = SYSDATE,
             IS_PAUSED = p_is_paused,
             IS_ACTIVE = 1
       where SID = p_sid  
         and S_CODE = p_s_code;
    else
      insert into NDS2_MRR_USER.IDA_INSPECTOR_WORKLOAD (
             SID,
             S_CODE,
             MAX_PAYMENT_CAPACITY, 
             MAX_COMPENSATION_CAPACITY,
             PAYMENT_DECL_PERM,
             COMPENSATION_DECL_PERM,
             INSERTED_BY, 
             UPDATED_BY,
             IS_PAUSED,
             IS_ACTIVE)  
      values
      (
             p_sid,
             p_s_code,
             p_max_payment_cap,
             p_max_comp_cap,
             p_has_payment_perm,
             p_has_compensation_perm,
             p_updated_by,
             p_updated_by,
             p_is_paused,
             1
      );
    end if;
    select IS_ACTIVE into p_is_active
      from NDS2_MRR_USER.IDA_INSPECTOR_SONO_ASSG asg 
     where asg.sid = p_sid and asg.s_code = p_s_code;
  end;
   
  procedure CLEAR_NON_DISTRIB_DECLS
     (p_s_code NDS2_MRR_USER.DECLARATION_ACTIVE.SOUN_CODE%type)
    as
  begin
     delete 
       from NDS2_MRR_USER.IDA_NON_ASSIGNED_DECLS 
      where DECLARATION_ID in
       ( 
          select da.id
            from NDS2_MRR_USER.DECLARATION_ACTIVE da 
           where da.SOUN_CODE = p_s_code and 
         to_number(da.correction_number) = 
         (
             select max(to_number(da.correction_number)) 
               from DECLARATION_ACTIVE da1
              where da1.id = da.id 
         ) and da.DECL_SIGN <> 'К возмещению' and nvl(da.claim_amount,0) = 0);
  end;
  
  procedure CLEAR_INSPECTOR_WORK_LOAD(
      p_sid NDS2_MRR_USER.IDA_INSPECTOR.SID%type,
      p_s_code NDS2_MRR_USER.IDA_INSPECTOR_WORKLOAD.S_CODE%type,
      p_updated_by NDS2_MRR_USER.IDA_INSPECTOR_WORKLOAD.UPDATED_BY%type,
      p_is_sono_assg_active out NDS2_MRR_USER.IDA_INSPECTOR_SONO_ASSG.IS_ACTIVE%type,
      p_result out number)
  as 
  v_is_active NDS2_MRR_USER.IDA_INSPECTOR_WORKLOAD.IS_ACTIVE%type;
  begin
     p_result := IS_WKLD_DEACTIVATION_DISABLED(p_sid,p_s_code);
     if p_result > 0 then
       v_is_active := 1;
     else
       v_is_active := 0;
     end if;
     update NDS2_MRR_USER.IDA_INSPECTOR_WORKLOAD
        set IS_ACTIVE = v_is_active,
            UPDATED_BY = p_updated_by,
            UPDATE_DATE = SYSDATE  
      where SID = p_sid and S_CODE = p_s_code;
     select IS_ACTIVE into p_is_sono_assg_active
      from NDS2_MRR_USER.IDA_INSPECTOR_SONO_ASSG asg 
     where asg.sid = p_sid and asg.s_code = p_s_code;
  end;
  
  procedure GET_ACTUAL_INSPECTOR_WORKLOADS(
          p_s_code NDS2_MRR_USER.IDA_INSPECTOR_WORKLOAD.S_CODE%type,
          p_act_wl_cursor OUT SYS_REFCURSOR)
  as
  begin 
      open P_act_wl_cursor for
      select do.INSPECTOR_SID,
             sum(case when da.DECL_SIGN = 'К возмещению' then abs(nvl(da.COMPENSATION_AMNT,0)) else 0 end) COMPENSATION,
             sum(case when da.DECL_SIGN <> 'К возмещению' then abs(nvl(da.CLAIM_AMOUNT,0)) else 0 end) PAYMENT,
             wl.PAYMENT_DECL_PERM,
             wl.COMPENSATION_DECL_PERM
        from NDS2_MRR_USER.DECLARATION_ACTIVE da 
  inner join NDS2_MRR_USER.DECLARATION_OWNER do
          on do.DECLARATION_ID = da.ID
  inner join NDS2_MRR_USER.IDA_INSPECTOR_WORKLOAD wl 
          on wl.SID = do.INSPECTOR_SID and wl.S_CODE = da.SOUN_CODE
  inner join NDS2_MRR_USER.IDA_INSPECTOR_SONO_ASSG ia
          on ia.SID = wl.SID and ia.S_CODE = wl.S_CODE
   left join NDS2_MRR_USER.SEOD_KNP knp
          on knp.DECLARATION_REG_NUM = da.SEOD_DECL_ID
       where da.SOUN_CODE = p_s_code and knp.COMPLETION_DATE is null
         and wl.IS_ACTIVE = 1 and wl.IS_PAUSED = 0 and ia.is_active = 1
         and da.correction_number = 
         (
             select max(da.correction_number) from DECLARATION_ACTIVE da1
             where da1.id = da.id 
         )
    group by do.INSPECTOR_SID, wl.payment_decl_perm, wl.compensation_decl_perm;

  end;
    
  procedure DISTRIBUTE_INDIVIDUAL_DECL(
     p_s_code NDS2_MRR_USER.IDA_INSPECTOR_WORKLOAD.S_CODE%type)
  as 
  begin 
    insert into NDS2_MRR_USER.DECLARATION_OWNER (DECLARATION_ID,INSPECTOR_SID,INSPECTOR_NAME) 
         select distinct da.ID, a.SID, i.NAME
           from NDS2_MRR_USER.DECLARATION_ACTIVE da 
     inner join NDS2_MRR_USER.IDA_TAX_PAYER_INDIVIDUAL_ASSG a
             on da.INN = a.INN and a.SID is not null
                and a.IS_ACTIVE = 1
     inner join NDS2_MRR_USER.IDA_INSPECTOR_SONO_ASSG ia 
             on ia.S_CODE = a.S_CODE and ia.sid = a.sid 
                and ia.IS_ACTIVE = 1
     inner join NDS2_MRR_USER.IDA_INSPECTOR i 
             on i.SID = a.SID and i.IS_ACTIVE = 1
     inner join NDS2_MRR_USER.IDA_INSPECTOR_WORKLOAD wl
             on wl.s_code = ia.s_code and wl.sid = a.SID
             and wl.Is_Active = 1
      left join seod_knp k 
             on k.DECLARATION_REG_NUM = da.SEOD_DECL_ID
          where a.S_CODE = p_s_code and da.SOUN_CODE = p_s_code and 
                da.ID is not null 
            and  wl.Is_Paused = 0 and k.COMPLETION_DATE is null and 
            (
                (da.DECL_SIGN = 'К возмещению') or 
                (da.DECL_SIGN <> 'К возмещению' and nvl(da.claim_amount,0) <> 0) 
            )
            and not exists 
           ( 
             select 1 
               from NDS2_MRR_USER.DECLARATION_OWNER do 
              where do.DECLARATION_ID = da.ID  
           );
      
     insert into NDS2_MRR_USER.IDA_NON_ASSIGNED_DECLS 
          select distinct da.ID
            from NDS2_MRR_USER.DECLARATION_ACTIVE da 
      inner join NDS2_MRR_USER.IDA_TAX_PAYER_INDIVIDUAL_ASSG a
              on da.INN = a.INN
                and a.IS_ACTIVE = 1
      inner join NDS2_MRR_USER.IDA_INSPECTOR_SONO_ASSG ia 
              on ia.S_CODE = a.S_CODE and ia.sid = a.sid 
                and ia.IS_ACTIVE = 1
      inner join NDS2_MRR_USER.IDA_INSPECTOR i 
              on i.SID = a.SID and i.IS_ACTIVE = 1
      inner join NDS2_MRR_USER.IDA_INSPECTOR_WORKLOAD wl
              on wl.s_code = ia.s_code and wl.sid = a.SID
              and wl.Is_Active = 1 
        left join seod_knp k 
               on k.DECLARATION_REG_NUM = da.SEOD_DECL_ID
            where a.S_CODE = p_s_code and da.SOUN_CODE = p_s_code 
            and wl.Is_Paused = 1 and k.COMPLETION_DATE is null and 
            (
                (da.DECL_SIGN = 'К возмещению') or 
                (da.DECL_SIGN <> 'К возмещению' and nvl(da.claim_amount,0) <> 0) 
            )
            and not exists 
           ( 
             select 1 
               from NDS2_MRR_USER.DECLARATION_OWNER do 
              where do.DECLARATION_ID = da.ID  
           ) and not exists
           (
             select 1 
               from NDS2_MRR_USER.IDA_NON_ASSIGNED_DECLS nad
                where nad.DECLARATION_ID = da.ID
           ) and da.ID is not null;
           
     delete 
       from NDS2_MRR_USER.IDA_NON_ASSIGNED_DECLS 
      where DECLARATION_ID in
           (select DECLARATION_ID from NDS2_MRR_USER.DECLARATION_OWNER);
  end;

  procedure GET_DECLS_FOR_AUTO_DISTR(p_s_code NDS2_MRR_USER.IDA_INSPECTOR_WORKLOAD.S_CODE%type,
                                     p_decl_cursor OUT SYS_REFCURSOR)                                    
  as 
  begin 
open p_decl_cursor for
     --Декларации к возмещению
     select 1 IS_COMPENSATION,
            0 IS_PAYMENT,
            abs(da.COMPENSATION_AMNT) AMOUNT,
            p_s_code as SONO_CODE,
            da.id
       from NDS2_MRR_USER.DECLARATION_ACTIVE da 
  left join NDS2_MRR_USER.DECLARATION_OWNER do 
         on do.DECLARATION_ID = da.ID
  left join NDS2_MRR_USER.IDA_TAX_PAYER_INDIVIDUAL_ASSG tpa on tpa.inn = da.inn
            and tpa.Is_Active = 1
  left join seod_knp k 
         on k.DECLARATION_REG_NUM = da.SEOD_DECL_ID
      where da.SOUN_CODE = p_s_code and tpa.inn is null and k.COMPLETION_DATE is null 
        and da.ID is not null
        and do.DECLARATION_ID is null and to_number(da.correction_number) = 
         (
             select max(to_number(da.correction_number)) 
               from DECLARATION_ACTIVE da1
              where da1.id = da.id 
         ) and da.DECL_SIGN = 'К возмещению'
      union
    --Декларации к уплате
     select 0 IS_COMPENSATION,
            1 IS_PAYMENT,
            abs(nvl(da.CLAIM_AMOUNT,0)) AMOUNT,
            p_s_code as SONO_CODE,
            da.id
       from NDS2_MRR_USER.DECLARATION_ACTIVE da 
  left join NDS2_MRR_USER.DECLARATION_OWNER do 
         on do.DECLARATION_ID = da.ID
  left join NDS2_MRR_USER.IDA_TAX_PAYER_INDIVIDUAL_ASSG tpa on tpa.inn = da.inn
            and tpa.Is_Active = 1
  left join seod_knp k 
         on k.DECLARATION_REG_NUM = da.SEOD_DECL_ID
      where da.SOUN_CODE = p_s_code  and tpa.inn is null and k.COMPLETION_DATE is null 
        and do.DECLARATION_ID is null and to_number(da.correction_number) = 
         (
             select max(to_number(da.correction_number)) 
               from DECLARATION_ACTIVE da1
              where da1.id = da.id 
         ) and da.ID is not null and da.DECL_SIGN <> 'К возмещению' ;
         
  end;                             

  procedure CANCEL_DECL_ASSIGN(p_id NDS2_MRR_USER.DECLARATION_ACTIVE.ID%type)
      as
  begin    
    delete from  NDS2_MRR_USER.DECLARATION_OWNER
    where DECLARATION_ID = p_id;
  end;
  
  procedure ASSIGN_DECLARATION(p_id NDS2_MRR_USER.DECLARATION_ACTIVE.ID%type,
                               p_sid NDS2_MRR_USER.IDA_INSPECTOR.SID%type)
  as
  v_inn  NDS2_MRR_USER.IDA_TAX_PAYER_INDIVIDUAL_ASSG.INN%type;
  begin    
    CANCEL_DECL_ASSIGN(p_id);
    begin
         select da.inn 
           into v_inn
           from NDS2_MRR_USER.IDA_TAX_PAYER_INDIVIDUAL_ASSG  ind 
     inner join NDS2_MRR_USER.DECLARATION_ACTIVE da on da.inn = ind.inn
          where da.id = p_id and ind.IS_ACTIVE = 1;
    exception 
      when NO_DATA_FOUND then v_inn := NULL;
    end;
    if v_inn is not null then
       update NDS2_MRR_USER.IDA_TAX_PAYER_INDIVIDUAL_ASSG a 
          set a.SID = p_sid, a.IS_ACTIVE = 1
        where a.inn = v_inn;
    end if;
    insert into NDS2_MRR_USER.DECLARATION_OWNER
    ( 
       DECLARATION_ID,
       INSPECTOR_SID,
       INSPECTOR_NAME    
    )
    select p_id,p_sid, i.NAME 
      from NDS2_MRR_USER.IDA_INSPECTOR i 
     where i.sid = p_sid;
     
    delete 
      from NDS2_MRR_USER.IDA_NON_ASSIGNED_DECLS 
     where DECLARATION_ID = p_id;

    ADD_HISTORY_ASSIGN_DECLARATION(p_id, p_sid);
  end;
  
 
  
  
  procedure ADD_NON_ASSIGNED_DECL(p_id NDS2_MRR_USER.DECLARATION_ACTIVE.ID%type)
  as
  begin
    delete from NDS2_MRR_USER.IDA_NON_ASSIGNED_DECLS where DECLARATION_ID = p_id;
    insert into NDS2_MRR_USER.IDA_NON_ASSIGNED_DECLS
    (
      DECLARATION_ID   
    )
    values
    (
      p_id  
    );
  end;
  
  procedure GET_INSPECTOR(p_s_code NDS2_MRR_USER.IDA_INSPECTOR_WORKLOAD.S_CODE%type,
                          p_sid NDS2_MRR_USER.IDA_INSPECTOR.SID%type,
                          p_inspector_cursor OUT SYS_REFCURSOR)
  as
  begin
  	    open p_inspector_cursor for
      select ia.*, ins.NAME, ins.EMPLOYEE_NUM
        from NDS2_MRR_USER.IDA_INSPECTOR ins 
  inner join NDS2_MRR_USER.IDA_INSPECTOR_SONO_ASSG ia on ins.sid = ia.sid     
       where ins.sid = p_sid and ia.S_CODE = p_s_code;
  end;

  procedure ADD_HISTORY_ASSIGN_DECLARATION
  (
    p_id NDS2_MRR_USER.DECLARATION_ACTIVE.ID%type,
    p_sid NDS2_MRR_USER.IDA_INSPECTOR.SID%type
   )
  as
  begin    

    insert into NDS2_MRR_USER.IDA_HST_DECLARATION_ASSG 
    (
      declaration_id,
      inspector_sid,
      inspector_name,
      insert_date
     )
     select p_id, p_sid, i.NAME, sysdate
     from NDS2_MRR_USER.IDA_INSPECTOR i
     where i.sid = p_sid;

  end;

end;
/
