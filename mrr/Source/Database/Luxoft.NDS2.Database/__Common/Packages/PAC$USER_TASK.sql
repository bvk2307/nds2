﻿create or replace PACKAGE NDS2_MRR_USER.PAC$USER_TASK as
Type T$USER_TASK_COMMON_ATTR is record
(
   quarter number(1),
   period char(2 char),
   fiscal_year  number(4),
   declaration_version_id number,
   sid varchar(256 char),
   user_name varchar(512 char),
   is_actual char(1),
   taxpayer_inn varchar(12 char),
   contractor_inn varchar(12, char),
   taxpayer_kpp varchar(9 char)
);
	CLOSE_REASON_SONO_CHANGED CONSTANT number(1):=9;

  /*
  Создание ПЗ "Истребование документов"
  */
  PROCEDURE P$CREATE_RECLAIMATION_TASK(P_PARAM_KEY NUMBER);
  /*
  Создание ПЗ "Ввод ответа на истребование"
  */
  PROCEDURE P$CREATE_RECLAIM_REPLY_TASK(P_PARAM_KEY NUMBER);
  /*
  Создание ПЗ "Анализ налогоплательщика"
  */
  PROCEDURE P$CREATE_TAXPAYER_ALALYZE_TASK(P_PARAM_KEY NUMBER);
  /*
  Завершение ПЗ "Анализ налогоплательщика"
  */
  PROCEDURE P$CLOSE_TAXPAYER_ALALYZE_TASK(P_TASK_ID NUMBER);
    /*
  Завершение ПЗ "Истребование документов"  по потокам ЭОД
  */
  PROCEDURE P$CLOSE_RECLAIMATION_TASK_EOD ( p_param_id number );
  /*
  Назначение инспектора на ПЗ
  */
  PROCEDURE P$ASSING_USER(P_TASK_ID number, P_USER_SID varchar2, P_USER_NAME varchar2);

  /*
  Назначает на пользователя(инспектора) все открытые ПЗ всех корректировк НД
   с соответсвующим ключом (ИНН, КПП, ТИП, ПЕРИОД, ГОД)*/
PROCEDURE P$ASSIGN_USER_BY_DECL(
    P_INN_DECLARANT VARCHAR2,
    P_KPP_EFFECTIVE VARCHAR2,
    P_PERIOD_EFFECTIVE NUMBER,
    P_FISCAL_YEAR NUMBER,
    P_TYPE_CODE NUMBER,
    P_ASSIGNED_TO_SID VARCHAR2,
    P_ASSIGNED_TO_NAME VARCHAR2);
  /*
  Выставить статус платежеспособности
  */
  PROCEDURE P$SET_SOLVENCY(P_TASK_ID NUMBER, P_SOLVENCY_STATUS_ID NUMBER, P_COMMENT CLOB);
  /*
  Статистика по ПЗ назначенных на инспектора
  */
  PROCEDURE P$GET_SUMMARY_STATISTIC ( p_sid varchar2, p_sono_code varchar2, p_cursor OUT SYS_REFCURSOR );
  /*
  фоновые задачи в рамках daily_job
  */
  PROCEDURE P$RUN_DAILY_PROCESS;
  /*
  пз - Список исполнителей
  */
  PROCEDURE P$GET_ACTOR_LIST (p_task_id number, p_cursor OUT SYS_REFCURSOR );
  /*
  список статусов нп
  */
  PROCEDURE P$GET_DICT_TAX_PAYER_SOLVENCY (p_cursor OUT SYS_REFCURSOR );
  /*
  Сведения о пользовательском задании - для карточки
  */
  PROCEDURE P$GET_DETAILS_DATA (p_task_id number, p_task_type_id number, p_declaraton_zip number, p_cursor OUT SYS_REFCURSOR );

  /*
  Статус налогоплательщика
  */
  PROCEDURE P$GET_TAX_PAYER_STATUS (p_inn in varchar2, p_kpp_effective in varchar2, p_cursor OUT SYS_REFCURSOR );

    /*
  Список статусов ПЗ
  */
  PROCEDURE P$GET_DICT_TASK_STATUS(p_cursor OUT SYS_REFCURSOR );

  /*
  Список типов ПЗ
  */
  PROCEDURE P$GET_DICT_TASK_TYPE(p_cursor OUT SYS_REFCURSOR );

  /*
  Создает агрегатдля ПЗ на определенную дату
  */
  PROCEDURE P$AGREGATE (pReportDate in date);

  /*
  Возвращает идентификатор репорта по дате. Если не существует, то -1.
  Возвращает статус поиска. 1 - найден, 2 - нет агрегата на дату, 3 - нет данных ранее этой даты
  */
  PROCEDURE P$GET_REPORT_ID(pId out number, pStatus out number, pDate in date);

  /*
  Переводит ПЗ Ввод Ответа на истребование в состояние "Выполнено"
  */
  PROCEDURE P$COMPLETE_RECLAIM_REPLY_TASK(ExplainId number);

  /*
    Переводит все открытые ПЗ по всем корректировкам НД в состояние "Закрыто"
  */
  PROCEDURE P$CLOSE_ALL_TASKS_BY_DECL(
    P_INN_DECLARANT VARCHAR2,
    P_KPP_EFFECTIVE VARCHAR2,
    P_PERIOD_EFFECTIVE NUMBER,
    P_FISCAL_YEAR NUMBER,
    P_TYPE_CODE NUMBER,
    P_CLOSE_REASON_ID NUMBER);

  /*
    Переводит все открытые ПЗ по конкретной корректировке НД в состояние "Закрыто"
  */
PROCEDURE P$CLOSE_ALL_TASKS_BY_CORR_ID(
                P_DECLARATION_CORRECTION_ID NUMBER,
                P_CLOSE_REASON_ID NUMBER);
end;
/
create or replace PACKAGE BODY NDS2_MRR_USER.PAC$USER_TASK as
STATUS_COMPLETED  CONSTANT number(1)            := 1;
STATUS_CLOSED	  CONSTANT number(1)            := 2;
STATUS_CREATED    CONSTANT number(1)            := 3;

TASK_TYPE_RECLAIMATION           CONSTANT number(1)         := 1;
TASK_TYPE_RECLAIM_REPLY          CONSTANT number(1)         := 2;
TASK_TYPE_TAXPAYER_ANALYZE       CONSTANT number(1)         := 3;

TASK_NOT_ASSIGNED_SID	CONSTANT varchar2(128)	:= '00000000-0000-0000-0000-000000000000';

TASK_ALREADY_CLOSED EXCEPTION;
PRAGMA EXCEPTION_INIT(TASK_ALREADY_CLOSED,-20001);

TYPE T$NUMBER_COL IS TABLE OF NUMBER NOT NULL;

  PROCEDURE P$LOG
  (
     p_side_id varchar2,
     p_msg_code varchar2,
     p_message varchar2,
     p_entity_id number

  )
  as
  pragma autonomous_transaction;
  begin
    insert into USER_TASK_SYSTEM_LOG(SITE_ID,
                                     MSG_CODE,
                                     MSG_TEXT,
                                     ENTITY_ID,
                                     CREATE_DATE)
    values (p_side_id, p_msg_code, p_message, p_entity_id, sysdate);
    commit;
  end;

  PROCEDURE P$TRACE
  (
     p_side_id varchar2,
     p_msg_code varchar2,
     p_message varchar2,
     p_entity_id number
  )
  as
  begin
    P$LOG(p_side_id,
     p_msg_code,
     p_message,
     p_entity_id);
  end;

  FUNCTION F$GET_COMMON_ATTR_BY_DOC_ID( p_doc_id number )
  return T$USER_TASK_COMMON_ATTR
  is
  v_result T$USER_TASK_COMMON_ATTR;
  begin
      select
         f$tax_period_to_quarter(dh.period_code),
         dtp.period,
         to_number(dh.fiscal_year),
         dh.zip,
         ins_ass.sid as inspector_sid,
         ins_ass.name as inspector_name,
         to_number(nvl(dh.is_active, '0')),
         dh.inn_declarant,
		 dh.inn_contractor,
         dh.kpp_effective
      into v_result
      from
         doc d
      inner join declaration_history dh on dh.zip = d.zip
      inner join dict_tax_period dtp on dtp.code = dh.period_code
      left join V$DECLARATION_CURRENT_ASSIGNEE ins_ass on
        ins_ass.declaration_type_code = dh.type_code
        and ins_ass.inn_declarant = dh.inn_declarant
        and ins_ass.kpp_effective = dh.kpp_effective
        and ins_ass.fiscal_year = dh.fiscal_year
        and ins_ass.period_effective = dtp.effective_value
      where doc_id = p_doc_id;

    return v_result;

    exception when no_data_found then
      P$LOG('F$GET_COMMON_ATTR_BY_DOC_ID', sqlcode, 'Не найдено ПЗ или один из атрибутов.', p_doc_id);
      DBMS_STANDARD.raise_application_error(num => -20000, msg => 'Ошибка получения списка общих атрибутов по АТ/АИ ('||p_doc_id||')' );
  end;



  FUNCTION F$GET_USER_TASK_ROW ( P_TASK_ID NUMBER ) return USER_TASK%ROWTYPE
  is
  v_result USER_TASK%ROWTYPE;
  begin
    select * into v_result
    from USER_TASK where TASK_ID = P_TASK_ID;

  return v_result;
  exception when no_data_found then
      DBMS_STANDARD.raise_application_error(
          num => -20000,
          msg => 'ПЗ не найдено.'||P_TASK_ID
          );
  end;

  FUNCTION F$GET_USER_TASK_TAXPAYER_ROW ( P_TASK_ID NUMBER ) return USER_TASK_TAX_PAYER%ROWTYPE
  is
  v_result USER_TASK_TAX_PAYER%ROWTYPE;
  begin
    select * into v_result
    from User_Task_Tax_Payer where TASK_ID = P_TASK_ID;

  return v_result;
  exception when no_data_found then
      DBMS_STANDARD.raise_application_error(
          num => -20000,
          msg => 'Данные налогоплательщика не найдены для ПЗ.'||P_TASK_ID
          );
  end;

    FUNCTION F$GET_OPENED_TASKS_BY_DECL(
        P_INN_DECLARANT VARCHAR2,
        P_KPP_EFFECTIVE VARCHAR2,
        P_PERIOD_EFFECTIVE NUMBER,
        P_FISCAL_YEAR NUMBER,
        P_TYPE_CODE NUMBER) RETURN T$NUMBER_COL
    AS
        v_retval t$number_col;
    BEGIN
        select distinct
            ut.task_id
        bulk collect into v_retval
        from user_task ut
            inner join user_task_declaration_rel utd on utd.task_id = ut.task_id
            inner join declaration_history dh on dh.zip = utd.declaration_correction_id
            inner join dict_tax_period dtp on dtp.period = dh.period_code
        where
            dh.inn_declarant = p_inn_declarant
            and dh.kpp_effective = p_kpp_effective
            and dh.type_code = p_type_code
            and dtp.effective_value = p_period_effective
            and dh.fiscal_year = p_fiscal_year
            and ut.status = STATUS_CREATED;
        return v_retval;
    END;

  PROCEDURE P$CREATE_TASK
  (
    p_task_id out number,
    p_type_id number,
    p_inn varchar2,
	p_contractor_inn varchar2,
    p_kpp_effective varchar2,
    p_quarter number,
    p_period varchar2,
    p_fiscal_year number,
    p_assegnee_sid varchar2,
    p_assegnee_name varchar2,
    p_assign_date timestamp,
    p_end_date_planning timestamp
   )
   as
  begin
    p_task_id := SEQ_USER_TASK_ID.Nextval;

    insert into USER_TASK(
      TASK_ID,
      TYPE_ID,
      INN,
	  INN_CONTRACTOR,
      KPP_EFFECTIVE,
      QUARTER,
      PERIOD,
      FISCAL_YEAR,
      ASSIGNEE_SID,
      ASSIGN_DATE,
      ASSIGNEE_NAME,
      END_DATE_PLANNING,
      STATUS,
      CREATE_DATE
      )
    values(
      p_task_id,
      p_type_id,
      p_inn,
	  p_contractor_inn,
      p_kpp_effective,
      p_quarter,
      p_period,
      p_fiscal_year,
      p_assegnee_sid,
      p_assign_date,
      p_assegnee_name,
      p_end_date_planning,
      STATUS_CREATED,
      sysdate
    );

  if p_assegnee_sid is null then
    insert into USER_TASK_ASSIGNMENT_HISTORY(task_id, user_sid, USER_NAME, ASSIGN_DATE)
    values(P_TASK_ID, TASK_NOT_ASSIGNED_SID, 'Не назначено', sysdate);
  else
    insert into USER_TASK_ASSIGNMENT_HISTORY(task_id, user_sid, USER_NAME, ASSIGN_DATE)
    values(p_task_id, p_assegnee_sid, p_assegnee_name, sysdate);
  end if;

  P$TRACE('P$CREATE_TASK', 1, 'Создана задача с типом '||p_type_id||' ', P_TASK_ID);

  end;

  PROCEDURE P$CLOSE_TASK ( P_TASK_ID number, P_REASON_ID number, P_CLOSE_DATE date default sysdate, P_CLOSE_STATUS number default STATUS_COMPLETED)
  as
  v_current_status number(2);
  v_planned_end_date timestamp;
  begin

    select
    ut.status,
    ut.end_date_planning
    into v_current_status, v_planned_end_date
    from USER_TASK ut
    where ut.task_id = P_TASK_ID;

  if v_current_status = STATUS_COMPLETED or v_current_status = STATUS_CLOSED then
      DBMS_STANDARD.raise_application_error(
          num => -20001,
          msg => 'Ошибка завершения ПЗ. ПЗ уже завершено.'||P_TASK_ID
          );
  end if;

  update user_task set
  status = P_CLOSE_STATUS ,
  close_date = P_CLOSE_DATE
  where task_id = P_TASK_ID;

  insert into USER_TASK_STATUS_HISTORY(TASK_ID, STATUS_ID, CHANGE_REASON, CHANGED_BY, CHANGED_DATE)
  values (P_TASK_ID, P_CLOSE_STATUS, P_REASON_ID, 'система', sysdate);

  P$LOG('P$CLOSE_TASK', 1, 'Успешное закрытие ПЗ. Причина закрытия ('||P_REASON_ID||')', P_TASK_ID);

  exception when no_data_found then
    P$LOG('P$CLOSE_TASK', sqlcode, 'Ошибка завершения ПЗ. ПЗ не найдено.', P_TASK_ID);
      DBMS_STANDARD.raise_application_error(
          num => -20000,
          msg => 'Ошибка завершения ПЗ. ПЗ не найдено.'||P_TASK_ID
          );
  end;

  PROCEDURE P$ASSIGN_DECL_CORRECTION(p_task_id number, p_declaration_version_id number)
   as
  begin
   insert into USER_TASK_DECLARATION_REL(TASK_ID, DECLARATION_CORRECTION_ID)
   values (p_task_id, p_declaration_version_id);
  end;

  PROCEDURE P$ASSIGN_DOC(p_task_id number, p_doc_id number)
   as
  begin
    insert into USER_TASK_DOC_REL(TASK_ID, DOC_ID)
    values (p_task_id, p_doc_id);
  end;

  PROCEDURE P$ASSIGN_TAX_PAYER(p_task_id number, p_inn varchar2, p_kpp_effective varchar2)
   as
  begin
   insert into USER_TASK_TAX_PAYER(TASK_ID, INN, EFFECTIVE_KPP)
   values (p_task_id, p_inn, p_kpp_effective);
  end;

  PROCEDURE P$ASSIGN_EXPLAIN_REPLY(p_task_id number, p_explain_id number)
   as
  begin
    insert into user_task_doc_reply(TASK_ID, reply_id)
    values (p_task_id, p_explain_id);
  end;

  /*Создание задачи истребования документов*/
  PROCEDURE P$CREATE_RECLAIMATION_TASK(P_PARAM_KEY NUMBER) as
  v_reclaim_doc_id number;
  v_log_entity_key number;
  v_task_id number;
  v_planned_end_date date;
  v_doc_type_id number;
  v_common_attrs  T$USER_TASK_COMMON_ATTR;
  begin
    v_log_entity_key := P_PARAM_KEY;

    begin
      select
        Q.doc_id,
        d.doc_type
      into
        v_reclaim_doc_id, v_doc_type_id
      from
        QUEUE_UT_RECLAMATION Q
        inner join doc d on d.doc_id = Q.Doc_Id
      where Q.job_id = P_PARAM_KEY;

      v_log_entity_key := v_reclaim_doc_id;

    exception when no_data_found then
      DBMS_STANDARD.raise_application_error(
          num => -20000,
          msg => 'Не найден идентификатор истребования в таблице параметров job_id='||P_PARAM_KEY
          );
    end;

/* Только автоистребования */
if v_doc_type_id in (2,3) then

    /*Проверим, что ПЗ на это АИ еще не создавалось*/
    select
          max(ut.task_id) into v_task_id
    from user_task ut
    inner join user_task_doc_rel utr on utr.task_id = ut.task_id and utr.doc_id = v_reclaim_doc_id and ut.type_id = TASK_TYPE_RECLAIMATION;

     if v_task_id > 0 then
       DBMS_STANDARD.raise_application_error(
        num => -20000,
        msg => 'Пользовательское задание ('||v_task_id||') для doc_id='||v_reclaim_doc_id||' уже создано. Создание отклонено'
        );
     end if;

    /* Выберем все параметры для создания задания */
    v_common_attrs := F$GET_COMMON_ATTR_BY_DOC_ID(v_reclaim_doc_id);

    --определим кол-во дней до просроченной даты
    select
    PAC$CALENDAR.F$ADD_WORK_DAYS(trunc(sysdate), DAYS_TO_PROCESS)
    into v_planned_end_date
    from USER_TASK_REGULATION
    where TASK_TYPE_ID = TASK_TYPE_RECLAIMATION;



     v_task_id := SEQ_USER_TASK_ID.NEXTVAL;
      P$CREATE_TASK(
            v_task_id,
            TASK_TYPE_RECLAIMATION,
            v_common_attrs.taxpayer_inn,
            v_common_attrs.contractor_inn,
            v_common_attrs.taxpayer_kpp,
            v_common_attrs.quarter,
            v_common_attrs.period,
            v_common_attrs.fiscal_year,
            v_common_attrs.sid,
            v_common_attrs.user_name,
            sysdate,
            cast(v_planned_end_date as timestamp));

      P$ASSIGN_DECL_CORRECTION(v_task_id, v_common_attrs.declaration_version_id);
      P$ASSIGN_DOC(v_task_id, v_reclaim_doc_id);
      P$ASSIGN_TAX_PAYER(v_task_id, v_common_attrs.taxpayer_inn, v_common_attrs.taxpayer_kpp);

end if;
  exception when others then
  P$LOG('P$CREATE_RECLAIMATION_TASK', sqlcode, sqlerrm, v_log_entity_key);
--  null;
  end;

  /*Создание задачи ввода ответа на истребование*/
  PROCEDURE P$CREATE_RECLAIM_REPLY_TASK(P_PARAM_KEY NUMBER)
  as
    v_queue_entry_row QUEUE_UT_RECLAIM_REPLY%rowtype;
    v_common_attrs  T$USER_TASK_COMMON_ATTR;
    v_planned_end_date date;
    v_task_id number;
  begin

   select *
   into v_queue_entry_row
   from QUEUE_UT_RECLAIM_REPLY
   where job_id = P_PARAM_KEY;

   v_common_attrs := F$GET_COMMON_ATTR_BY_DOC_ID(v_queue_entry_row.doc_id);

       --определим кол-во дней до просроченной даты
    select
    PAC$CALENDAR.F$ADD_WORK_DAYS(trunc(sysdate), DAYS_TO_PROCESS)
    into v_planned_end_date
    from USER_TASK_REGULATION
    where TASK_TYPE_ID = TASK_TYPE_RECLAIM_REPLY;


   v_task_id := SEQ_USER_TASK_ID.NEXTVAL;
      P$CREATE_TASK(
            v_task_id,
            TASK_TYPE_RECLAIM_REPLY,
            v_common_attrs.taxpayer_inn,
            v_common_attrs.contractor_inn,
            v_common_attrs.taxpayer_kpp,
            v_common_attrs.quarter,
            v_common_attrs.period,
            v_common_attrs.fiscal_year,
            v_common_attrs.sid,
            v_common_attrs.user_name,
            sysdate,
            cast(v_planned_end_date as timestamp));

      P$ASSIGN_DECL_CORRECTION(v_task_id, v_common_attrs.declaration_version_id);
      P$ASSIGN_DOC(v_task_id, v_queue_entry_row.doc_id);
      P$ASSIGN_TAX_PAYER(v_task_id, v_common_attrs.taxpayer_inn, v_common_attrs.taxpayer_kpp);
      P$ASSIGN_EXPLAIN_REPLY(v_task_id, v_queue_entry_row.explain_id);
   commit;
  exception when others then
      P$LOG('P$CREATE_RECLAIM_REPLY_TASK', sqlcode, sqlerrm, P_PARAM_KEY);
  end;

  /*Создание задачи анализа налогоплательщика*/
  PROCEDURE P$CREATE_TAXPAYER_ALALYZE_TASK(P_PARAM_KEY NUMBER) as
    v_common_attrs  T$USER_TASK_COMMON_ATTR;
    v_task_id number;
    v_planned_end_date date;
    v_already_exist_task_id number;
    v_doc_id number;
    v_site_name varchar2(128 char) := 'P$CREATE_TAXPAYER_ALALYZE_TASK';
  begin

    select
    doc_id
    into v_doc_id
    from QUEUE_UT_TAX_PAYER_ANALYZE where job_id = P_PARAM_KEY;

    --инициализируем основные атрибуты
    v_common_attrs := F$GET_COMMON_ATTR_BY_DOC_ID(v_doc_id);
	
	--проверка на существовение ПЗ по этому АТ
	select max(T.TASK_ID) into v_already_exist_task_id
	from 
	 user_task  t
	 join USER_TASK_DOC_REL r on r.task_id = t.task_id
	 join doc d on d.doc_id = r.doc_id and not d.close_reason = 5
	where t. type_id = 3 
		and t.inn_contractor = v_common_attrs.taxpayer_inn
		and t.kpp_effective = v_common_attrs.taxpayer_kpp
		and t.period = v_common_attrs.period
		and t.fiscal_year = v_common_attrs.fiscal_year;

    if v_already_exist_task_id > 0 then
      DBMS_STANDARD.raise_application_error(num => -20000, msg => 'Пользовательское задание по данному НП за данный период уже создано под номером '||v_already_exist_task_id );
    end if;

    --определим кол-во дней до просроченной даты
    select
    PAC$CALENDAR.F$ADD_WORK_DAYS(trunc(sysdate), DAYS_TO_PROCESS)
    into v_planned_end_date
    from USER_TASK_REGULATION
    where TASK_TYPE_ID = TASK_TYPE_TAXPAYER_ANALYZE;

    begin
    --инициализируем основные атрибуты
 
      v_task_id := SEQ_USER_TASK_ID.NEXTVAL;
      P$CREATE_TASK(
            v_task_id,
            TASK_TYPE_TAXPAYER_ANALYZE,
            v_common_attrs.taxpayer_inn,
            v_common_attrs.contractor_inn,
            v_common_attrs.taxpayer_kpp,
            v_common_attrs.quarter,
            v_common_attrs.period,
            v_common_attrs.fiscal_year,
            v_common_attrs.sid,
            v_common_attrs.user_name,
            sysdate,
            cast(v_planned_end_date as timestamp));

      P$ASSIGN_DECL_CORRECTION(v_task_id, v_common_attrs.declaration_version_id);
      P$ASSIGN_DOC(v_task_id, v_doc_id);
      P$ASSIGN_TAX_PAYER(v_task_id, v_common_attrs.taxpayer_inn, v_common_attrs.taxpayer_kpp);

      commit;
      exception
        when no_data_found then
          rollback;
          P$LOG(v_site_name, sqlcode, 'Не найдено автотребования с номером '||v_doc_id, v_doc_id);
        when others then
          rollback;
          P$LOG(v_site_name, sqlcode, 'Необработанная ошибка '||sqlerrm, v_doc_id);
    end;

    exception
      when others then
        rollback;
        P$LOG(v_site_name, sqlcode, 'Необработанная ошибка '||sqlerrm, P_PARAM_KEY);

  end;

  PROCEDURE P$CLOSE_TAXPAYER_ALALYZE_TASK(P_TASK_ID NUMBER)
  as
  v_task_id number;
  v_current_status number(2);
  v_end_date_planned date;
  v_assess_exist number;
  begin


  begin
    select
     ut.status,
     ut.end_date_planning
     into v_current_status, v_end_date_planned
    from
    USER_TASK UT
    where UT.Task_Id = P_TASK_ID;
  exception when no_data_found then
    DBMS_STANDARD.raise_application_error(
                                          num => -20000,
                                          msg => 'Пользовательское задание не найдено' );
  end;

  -- Closing is not required
  if v_current_status = STATUS_COMPLETED or v_current_status = STATUS_CLOSED then
    return;
  end if;

  select
    count(1) into v_assess_exist
  from
    USER_TASK_SOLVENCY_REL uta
  where uta.task_id = P_TASK_ID;

  if v_assess_exist = 0 then
    DBMS_STANDARD.raise_application_error(
                                          num => -20000,
                                          msg => 'Не указан статус платежеспособности' );
  end if;

  P$CLOSE_TASK(P_TASK_ID, 1);

  exception when others then
    P$LOG('P$CLOSE_TAXPAYER_ALALYZE_TASK', sqlcode, sqlerrm, P_TASK_ID);
    DBMS_STANDARD.raise_application_error(num => -20000, msg => sqlerrm, keeperrorstack => true);
  end;

  PROCEDURE P$COMPLETE_RECLAIM_REPLY_TASK(ExplainId number)
  as
  c_task_close_by_close_doc_id number(1) := 2;
  v_task_id number;
  begin
    P$TRACE('P$COMPLETE_RECLAIM_REPLY_TASK', 1, 'Старт', 0);

  select ut.task_id into v_task_id
  from  seod_explain_reply er
          inner join user_task_doc_reply ute on ute.reply_id = er.explain_id
          inner join user_task ut on ut.task_id = ute.task_id
          inner join user_task_declaration_rel utnd on utnd.task_id = ut.task_id
          inner join declaration_history dh on dh.zip = utnd.declaration_correction_id
    where er.explain_id = ExplainId and ut.status = STATUS_CREATED and ut.type_id = TASK_TYPE_RECLAIM_REPLY;
	begin
	  P$CLOSE_TASK(v_task_id, c_task_close_by_close_doc_id);
      exception when others then
        P$LOG('P$AUTOCLOSE_RECLAIMATION_TASK', sqlcode, sqlerrm, v_task_id);
	end;
    P$LOG('P$COMPLETE_RECLAIM_REPLY_TASK', 1, 'Завершение', 0);
  end;

  PROCEDURE P$AUTOCLOSE_RECLAIMATION_TASK
  as
  c_task_close_by_close_doc_id number(1) := 8;
  v_cntr number := 0;
  v_commit_row_count number := 500;
  pragma autonomous_transaction;
  begin
   P$TRACE('P$AUTOCLOSE_RECLAIMATION_TASK', 1, 'Старт', 0);

   for line in (
    select
     ut.task_id,
	 case when d.close_reason in (1, 3) then STATUS_CLOSED else STATUS_COMPLETED end as close_status
    from
    doc d
    inner join user_task_doc_rel utdr on utdr.doc_id = d.doc_id
    inner join user_task ut on ut.task_id = utdr.task_id
    where d.doc_type in (2,3)
    and d.status = 10
    and ut.type_id = 1
    and ut.status = STATUS_CREATED)
    loop
      begin
        P$CLOSE_TASK(line.task_id, c_task_close_by_close_doc_id, sysdate, line.close_status);
      exception when others then
        P$LOG('P$AUTOCLOSE_RECLAIMATION_TASK', sqlcode, sqlerrm, line.task_id);
      end;

     if v_cntr >= v_commit_row_count then
       commit;
       v_cntr := 0;
     end if;
    end loop;

   if v_cntr >= 0 then
     commit;
   end if;
  P$TRACE('P$AUTOCLOSE_RECLAIMATION_TASK', 1, 'Завершение', 0);
  exception when others then
    P$LOG('P$AUTOCLOSE_RECLAIMATION_TASK', sqlcode, sqlerrm, 0);
  end;

  PROCEDURE P$AUTOCLOSE_TASK
  as
   type t_tmp is table of number;
   id_table t_tmp;
   begin
    update user_task tsk
    set tsk.status = STATUS_CLOSED,
        tsk.close_date = sysdate
    where tsk.status = STATUS_CREATED  and tsk.task_id in
    (select tsk.task_id from
    declaration_history dh
    inner join nds2_seod.seod_knp knp on dh.reg_number = knp.declaration_reg_num and dh.sono_code_submited = knp.ifns_code
    where knp.completion_date is not null
      and dh.inn_declarant = tsk.inn
      and dh.inn_contractor = tsk.inn_contractor
      and dh.kpp_effective = tsk.kpp_effective
      and dh.period_code = tsk.period
      and dh.fiscal_year = tsk.fiscal_year
      and dh.is_active = 1)
    RETURNING task_id BULK COLLECT INTO id_table;

   if id_table.count > 0 then
	FOR i IN id_table.first .. id_table.last LOOP
			insert into user_task_status_history(task_id, status_id, change_reason, changed_by, changed_date)
    values (id_table(i), STATUS_CLOSED, 8, 'система', sysdate);
	END LOOP;
  end if;
  commit;
   exception when others then
  rollback;
       P$LOG('P$AUTOCLOSE_TASK', sqlcode, sqlerrm, 0);
 end;

  PROCEDURE P$CLOSE_RECLAIMATION_TASK_EOD ( p_param_id number )
   as
  v_queue_entry QUEUE_UT_RECLAMATION_CLOSE%rowtype;
  v_task_to_close number;
  v_doc_type number(2);
  v_task_status number(1);
  v_reason_id number(2);
  pragma autonomous_transaction;
  begin

    select * into v_queue_entry
    from QUEUE_UT_RECLAMATION_CLOSE where job_id = p_param_id;

  begin
    select
      ut.task_id,
      d.doc_type,
      ut.status
    into v_task_to_close, v_doc_type, v_task_status
    from
      doc d
    inner join user_task_doc_rel utdr on utdr.doc_id = d.doc_id
    inner join user_task ut on ut.task_id = utdr.task_id
    where d.doc_id = v_queue_entry.doc_id
    and ut.type_id = TASK_TYPE_RECLAIMATION
    and d.doc_type in (2,3);
--    and ut.close_date <> STATUS_COMPLETED;
--    and d.status not in (-1,1,3,11,10)

    exception when no_data_found then
      DBMS_STANDARD.raise_application_error(-20000, 'Не найдено ПЗ или ссылка на АИ.');
  end;

  if v_task_status = STATUS_COMPLETED or v_task_status = STATUS_CLOSED then
    P$LOG('P$CLOSE_RECLAIMATION_TASK_EOD', 1, 'Задание '||v_task_to_close||' уже закрыто.', p_param_id);
  else
    v_reason_id := v_queue_entry.by_status_id;
    P$CLOSE_TASK(v_task_to_close, v_reason_id);
  end if;

  commit;
  exception when others then
  rollback;
       P$LOG('P$CLOSE_RECLAIMATION_TASK_EOD', sqlcode, sqlerrm, p_param_id);
  end;

  /**/
  PROCEDURE P$ASSING_USER(P_TASK_ID number, P_USER_SID varchar2, P_USER_NAME varchar2)
  as
  v_user_task_row USER_TASK%ROWTYPE;
  begin
  v_user_task_row := F$GET_USER_TASK_ROW(P_TASK_ID);

  if v_user_task_row.assignee_sid = P_USER_SID then
    DBMS_STANDARD.raise_application_error(
    num => -20000,
    msg => 'Повторное назначение на одно и того же пользователя.');
  end if;

  update
    user_task ut
  set ut.assignee_sid = P_USER_SID,
    ut.assign_date = sysdate,
    ut.assignee_name = P_USER_NAME
  where ut.task_id = P_TASK_ID;

  if P_USER_SID is null then
    insert into USER_TASK_ASSIGNMENT_HISTORY(task_id, user_sid, USER_NAME, ASSIGN_DATE)
    values(P_TASK_ID, TASK_NOT_ASSIGNED_SID, 'Не назначено', sysdate);
  else
    insert into USER_TASK_ASSIGNMENT_HISTORY(task_id, user_sid, USER_NAME, ASSIGN_DATE)
    values(P_TASK_ID, P_USER_SID, P_USER_NAME, sysdate);
  end if;

  exception when others then
    P$LOG('P$ASSING_USER', sqlcode, sqlerrm, P_TASK_ID);
  end;

  /*Назначает на пользователя(инспектора) все ПЗ связанные с ключом декларации (ИНН, КПП, ТИП, ПЕРИОД, ГОД)*/
PROCEDURE P$ASSIGN_USER_BY_DECL(
    p_inn_declarant varchar2,
    p_kpp_effective varchar2,
    p_period_effective number,
    p_fiscal_year number,
    p_type_code number,
    p_assigned_to_sid varchar2,
    p_assigned_to_name varchar2
)
as
    col_tasks_id t$number_col;
	BEGIN
    /*Перечень открытых ПЗ по ключу декларации*/
    col_tasks_id:= F$GET_OPENED_TASKS_BY_DECL(
                    P_INN_DECLARANT => p_inn_declarant,
                    P_KPP_EFFECTIVE => p_kpp_effective,
                    P_PERIOD_EFFECTIVE => p_period_effective,
                    P_FISCAL_YEAR => p_fiscal_year,
                    P_TYPE_CODE =>p_type_code);

    for i in 1..col_tasks_id.count
     loop
       P$ASSING_USER(	P_TASK_ID => col_tasks_id(i),
                        P_USER_SID => p_assigned_to_sid,
                        P_USER_NAME => p_assigned_to_name);
     end loop;
     commit;
END;



  PROCEDURE P$SET_SOLVENCY(P_TASK_ID NUMBER, P_SOLVENCY_STATUS_ID NUMBER, P_COMMENT CLOB)
  as
  v_task_row USER_TASK%ROWTYPE;
  v_task_tax_payer_row USER_TASK_TAX_PAYER%ROWTYPE;
  v_existing_solvency_id number;
  begin

     v_task_row := F$GET_USER_TASK_ROW(P_TASK_ID);

     if v_task_row.type_id <> TASK_TYPE_TAXPAYER_ANALYZE then
       DBMS_STANDARD.raise_application_error(
       num => -20000,
       msg => 'Невозможно выставить статус платежеспособности, так как пользовательскоей задание не относится к типу "Анализ Налогоплательщика"',
       keeperrorstack => true);
     end if;


   begin
     select
      SOLVENCY_ID into v_existing_solvency_id
     from
     USER_TASK_SOLVENCY_REL solv_rel
     where solv_rel.task_id = P_TASK_ID;
   exception when no_data_found then null;
   end;

   if v_existing_solvency_id > 0 then

     update TAXPAYER_SOLVENCY ts
     set ts.SOLVENCY_STATUS_ID = P_SOLVENCY_STATUS_ID,
     ts.decision_date = sysdate
     where id = v_existing_solvency_id;

     update TAXPAYER_SOLVENCY_COMMENT set USER_COMMENT = P_COMMENT
     where SOLVENCY_ID = v_existing_solvency_id;

   else

     select
      nvl(max(ts.id), 0) into v_existing_solvency_id
     from TAXPAYER_SOLVENCY ts
     where ts.inn = v_task_row.inn
     and ts.kpp_effective = v_task_row.kpp_effective
     and ts.period = v_task_row.period;

     if v_existing_solvency_id = 0 then

       v_existing_solvency_id := SEQ_TP_SOLVENCY_ID.NEXTVAL;

       select * into v_task_tax_payer_row from USER_TASK_TAX_PAYER where TASK_ID = P_TASK_ID;

		insert into taxpayer_solvency(
			id,
			inn,
			kpp_effective,
			fiscal_year,
			period,
			solvency_status_id,
			decision_date)
		values(
			v_existing_solvency_id,
			v_task_tax_payer_row.inn,
			v_task_tax_payer_row.effective_kpp,
			v_task_row.fiscal_year,
			v_task_row.period,
			P_SOLVENCY_STATUS_ID,
			sysdate);

     end if;

    insert into taxpayer_solvency_comment (solvency_id, user_comment)
	values (v_existing_solvency_id, p_comment);

    insert into user_task_solvency_rel(task_id, solvency_id, decision_date)
    values (p_task_id, v_existing_solvency_id, sysdate);

   end if;

  end;


  PROCEDURE P$GET_SUMMARY_STATISTIC ( p_sid varchar2, p_sono_code varchar2, p_cursor OUT SYS_REFCURSOR )
  as
  begin
    open p_cursor for
    with calculated_data as
    (
    select
      ut.type_id,
      sum(case when ut.status = STATUS_CREATED then 1 else 0 end) as ActiveTaskCount,
      sum(case when ut.status = STATUS_CREATED and ut.end_date_planning < trunc(sysdate) then 1 else 0 end) as ActiveOverdueTaskCount
    from
    user_task ut
    inner join user_task_declaration_rel d on d.task_id = ut.task_id
    inner join declaration_history h on h.zip = d.declaration_correction_id
    where
	(
		p_sono_code = '0000' --ца
		or
		(h.sono_code like substr(p_sono_code, 1,2) || '%' and p_sono_code like '%00') -- уфнс
		or
		(h.sono_code like p_sono_code)
	)
    and ((p_sid is null) or ut.assignee_sid = p_sid)
    group by
    ut.type_id
    )
    select
    utt.id as TaskType,
    utt.description as TaskTypeName,
    nvl(t.ActiveTaskCount, 0) as UnprocessedQty,
    nvl(t.ActiveOverdueTaskCount, 0) as ExpiredQty
    from
    user_task_type utt
    left join calculated_data t on t.type_id = utt.id;
  end;

  PROCEDURE P$RUN_DAILY_PROCESS
   as
   begin
     P$AUTOCLOSE_RECLAIMATION_TASK;
	 P$AUTOCLOSE_TASK;
	 P$AGREGATE(sysdate-1);
   end;

  PROCEDURE P$GET_ACTOR_LIST (p_task_id number, p_cursor OUT SYS_REFCURSOR )
  as
  begin
    open p_cursor for
      select *
      from user_task_assignment_history
      where task_id = p_task_id
        and	user_name <> 'Не назначено'
      order by assign_date desc;
  end;

  PROCEDURE P$GET_DICT_TAX_PAYER_SOLVENCY (p_cursor OUT SYS_REFCURSOR )
  as
  begin
    open p_cursor for
    select *
    from  dict_tax_payer_solvency;
  end;


  --Сведения о пользовательском задании - для карточки
  PROCEDURE P$GET_DETAILS_DATA (
  p_task_id       number,
  p_task_type_id     number,
  p_declaraton_zip   number,
  p_cursor       OUT SYS_REFCURSOR )
  as
  v_ask_declandjrnl     ask_declandjrnl%ROWTYPE;
  v_declaration_history   declaration_history%ROWTYPE;


  --Срок выполнения задания (в днях)
  v_days_to_process number;
  v_status_knp varchar2(50);
  v_sign varchar2(50);

  v_external_doc_num		varchar2(50);
  v_claimdoc_date				timestamp;
  v_claimdoc_id					number;--«Номер» данный при создании в АСК НДС-2
  v_claimdoc_type_name			varchar2(50);

  v_answer_incoming_date  	timestamp;--дата из связанного ответа
  v_explain_id    			number;--статус из связанного  ответа
  v_answer_status_id    	number;--статус из связанного  ответа
  v_answer_status      		varchar2(50);
  v_other_answer_notexists 	number; --Ввести ответ - Доступна

  v_tax_payer_solvency    	number;--Статус  налогоплательщика
  v_comment          		varchar2(200);--Комментарий
  v_sur_code				char(1 char);
  v_nds_total				number;

  v_knp_close_reason_id				number;

  begin
	--определим кол-во дней до просроченной даты
	begin
	select days_to_process
	into v_days_to_process
	from user_task_regulation
	where task_type_id = p_task_type_id;
	exception when no_data_found then null;
	end;
	begin
		select days_to_process
		into v_days_to_process
		from user_task_regulation
		where task_type_id = p_task_type_id;
	exception when no_data_found then
		DBMS_STANDARD.raise_application_error(
			num => -20000,
			msg => 'Не найдено значене регламента по типу задания:'||p_task_type_id
			);
	end;



	select
		decode(ask.nd_prizn, 1, 'К уплате', 2, 'К возмещению', 'Нулевая'),
		case
		  when knp.knp_id is null and seod.decl_reg_num is null then ''
		  else case when knp.completion_date is null then 'Открыта' else 'Закрыта' end
		  end as status_knp,
		decl.sur_code,
		decl.nds_total,
    ds.calc_status
	into v_sign, v_status_knp, v_sur_code, v_nds_total,  v_knp_close_reason_id
	from user_task_declaration_rel tdr
		inner join ask_declandjrnl ask on tdr.declaration_correction_id  = ask.zip
		inner join declaration_history decl on decl.zip  = ask.zip
		left join nds2_seod.seod_declaration seod
			  on seod.tax_period = ask.period
			  and seod.fiscal_year = ask.otchetgod
			  and seod.inn = ask.innnp and seod.correction_number = ask.nomkorr
			  and seod.id_file = ask.idfajl
		left join nds2_seod.seod_knp knp
			  on knp.declaration_reg_num = seod.decl_reg_num
			  and knp.ifns_code = seod.sono_code
    left join DECLARATION_KNP_STATUS ds on ds.zip = decl.zip
	where tdr.task_id = p_task_id and rownum = 1;

	begin
	select
		doc.external_doc_num,
		doc.create_date as seod_date,
		doc.doc_id,
		dtp.description as seod_type_name
	into v_external_doc_num, v_claimdoc_date, v_claimdoc_id, v_claimdoc_type_name
	from user_task_doc_rel drl
		inner join doc on doc.doc_id = drl.doc_id
		inner join doc_type dtp on dtp.id = doc.doc_type
	where drl.task_id = p_task_id;
	exception when no_data_found then null;
	end;



	if p_task_type_id = TASK_TYPE_RECLAIM_REPLY then

		select
			er.explain_id,
			er.incoming_date,
			nvl(er_mrr.status_id, er.status_id) as status_id,
			dst.name
		into v_explain_id, v_answer_incoming_date, v_answer_status_id, v_answer_status
		from
			user_task_doc_reply ute
			inner join  nds2_seod.seod_explain_reply er on ute.reply_id = er.explain_id
			left join nds2_mrr_user.seod_explain_reply er_mrr on er_mrr.explain_id = er.explain_id
			inner join dict_explain_reply_status dst on nvl(er_mrr.status_id,er.status_id) = dst.id
		where ute.task_id = p_task_id;



		--Ввести ответ - Доступна, если ранее созданные ПЗ с данным типом по данному связанному документу  выполнены
			--(т.е. если на одно автоистребование было создано несколько ПЗ данного типа и эти задания не выполнены,
			--то кнопка «Ввести ответ» будет доступна только на первом задании).
		select case when count(*) = 0 then 1 else 0 end
		into v_other_answer_notexists
		from user_task ut
			inner join user_task_doc_rel drl on drl.task_id = ut.task_id
			where
				ut.type_id = p_task_type_id and
				ut.status = STATUS_CREATED and
				drl.doc_id = v_claimdoc_id and
				ut.task_id < p_task_id;
	end if;

	if p_task_type_id = TASK_TYPE_TAXPAYER_ANALYZE then
		begin
		select
			slv.solvency_status_id,
			cmt.user_comment
			into v_tax_payer_solvency, v_comment
		from
			user_task_solvency_rel rel
			inner join taxpayer_solvency slv on rel.solvency_id = slv.id
			inner join taxpayer_solvency_comment cmt on rel.solvency_id = cmt.solvency_id
		where rel.task_id = p_task_id;
		exception when no_data_found then null;
		end;
	end if;

	open p_cursor for
	select
		ut.task_id,
		ut.status, --Статус - идентификатор
		uts.description as status_name, --Статус
		ut.end_date_planning as planning_complete_date,
		case when ut.status = STATUS_CLOSED then null else ut.close_date end as actual_complete_date,  --Дата выполнения: Фактическая
		case
		  when trunc(nvl(ut.close_date, sysdate)) > ut.end_date_planning then
		  trunc(nvl(ut.close_date, sysdate))
        - ut.end_date_planning
        - (select count(1) from redday where red_date between ut.end_date_planning and nvl(ut.close_date, sysdate))
		  else 0
		  end as days_after_expire,  --Кол-во просроченных дней
		v_days_to_process as days_to_process,

		v_sur_code as sur_code,
		v_status_knp as status_knp,
		v_sign as sign,
		v_nds_total as nds_total,

		v_external_doc_num as external_doc_num,
		v_claimdoc_date as claimdoc_date,
		v_claimdoc_id as claimdoc_id,
		v_claimdoc_type_name as claimdoc_type_name,

		v_explain_id as explain_id,
		v_answer_incoming_date as answer_incoming_date,
		v_answer_status_id as answer_status_id,
		v_answer_status as answer_status,
		v_other_answer_notexists as other_answer_notexists,

		v_tax_payer_solvency as v_tax_payer_solvency,
		v_comment as v_comment,
        v_knp_close_reason_id as v_knp_close_reason_id
	from user_task ut
		inner join user_task_status uts on uts.id = ut.status
	where ut.task_id = p_task_id;
	end;

  PROCEDURE P$GET_TAX_PAYER_STATUS (p_inn in varchar2, p_kpp_effective in varchar2, p_cursor OUT SYS_REFCURSOR ) AS
  BEGIN
    open p_cursor for
    select
     title.description as Status,
     tps.decision_date as StatusDate,
     tpsc.user_comment as StatusComment
    from (select t.*,
                 row_number() over (partition by t.inn, t.kpp_effective order by t.decision_date desc) rn
          from TAXPAYER_SOLVENCY t
          where t.inn = p_inn and t.kpp_effective = p_kpp_effective) tps
    left join TAXPAYER_SOLVENCY_COMMENT tpsc on tpsc.solvency_id = tps.id
    left join DICT_TAX_PAYER_SOLVENCY title on title.solvency_id = tps.solvency_status_id
    where tps.rn = 1;

    exception when others then null;
  END;

  PROCEDURE P$GET_DICT_TASK_STATUS (p_cursor OUT SYS_REFCURSOR )
  as
  begin
    open p_cursor for
    select ID, DESCRIPTION
    from  user_task_status;
  end;

  PROCEDURE P$GET_DICT_TASK_TYPE (p_cursor OUT SYS_REFCURSOR )
  as
  begin
    open p_cursor for
    select ID, DESCRIPTION
    from  user_task_type;
  end;

FUNCTION F$CREATE_AGREGATE(pReportDate in date, pPrevReportId in number) return number
  is
  v_report_id number;
  v_completed_onschedule number;
  v_completed_overdue number;
  begin
  v_report_id := seq_user_task_report_id.nextval;

  insert into user_task_report (id, report_date, aggregate_date) values (v_report_id, pReportDate, systimestamp);

  for line in (
        select decl.sono_code, hist.user_sid, t.type_id,
        sum(case when t.status = STATUS_COMPLETED and trunc(t.close_date) = trunc(pReportDate) and trunc(t.close_date) > trunc(t.end_date_planning) then 1 else 0 end ) as completed_overdue,
        sum(case when t.status = STATUS_COMPLETED and trunc(t.close_date) = trunc(pReportDate) and trunc(t.close_date) <= trunc(t.end_date_planning) then 1 else 0 end) as completed_onschedule,
        sum(case when t.status = STATUS_CREATED and  trunc(t.end_date_planning) < trunc(pReportDate) then 1 else 0 end) as inprogress_overdue,
        sum(case when t.status = STATUS_CREATED  and  trunc(t.end_date_planning) >= trunc(pReportDate) then 1 else 0 end) as inprogress_onschedule
     from USER_TASK t
      join  USER_TASK_DECLARATION_REL t_decl on t_decl.task_id = t.task_id
      join  DECLARATION_HISTORY decl on decl.zip = t_decl.declaration_correction_id
      join (select distinct task_id, user_sid, assign_date, max(assign_date) over (PARTITION by task_id) as max_assign_date
                from (select * from USER_TASK_ASSIGNMENT_HISTORY where trunc(assign_date) <= trunc(pReportDate) and user_sid <> TASK_NOT_ASSIGNED_SID)) hist
				on hist.task_id = t.task_id and trunc(hist.assign_date) = trunc(max_assign_date)
      group by decl.sono_code, hist.user_sid, t.type_id)
loop
    begin
    select completed_onschedule, completed_overdue into v_completed_onschedule, v_completed_overdue from user_task_report_user
    where task_type_id = line.type_id and user_sid = line.user_sid and sono_code = line.sono_code and report_id = pPrevReportId;
   	exception when others then
    v_completed_onschedule := 0;
    v_completed_overdue :=0;
    end;

    if line.completed_onschedule is null
    then line.completed_onschedule := 0;
    end if;

    v_completed_onschedule := v_completed_onschedule + line.completed_onschedule;

    if line.completed_overdue is null
    then line.completed_overdue := 0;
    end if;

    v_completed_overdue := v_completed_overdue + line.completed_overdue;

    insert into user_task_report_user (report_id, sono_code, user_sid, task_type_id, completed_onschedule, completed_overdue, inprogress_onschedule, inprogress_overdue)
    values (v_report_id, line.sono_code, line.user_sid, line.type_id, v_completed_onschedule, v_completed_overdue, line.inprogress_onschedule, line.inprogress_overdue );

end loop;

for line in (
        select decl.sono_code, t.type_id, decl.region_code,
        sum(case when t.status = STATUS_COMPLETED and trunc(t.close_date) = trunc(pReportDate) and trunc(t.close_date) > trunc(t.end_date_planning) and hist.user_sid is not null then 1 else 0 end ) as completed_overdue,
        sum(case when t.status = STATUS_COMPLETED and trunc(t.close_date) = trunc(pReportDate) and trunc(t.close_date) <= trunc(t.end_date_planning) and hist.user_sid is not null then 1 else 0 end) as completed_onschedule,
        sum(case when t.status = STATUS_CREATED and  trunc(t.end_date_planning) < trunc(pReportDate) and hist.user_sid is not null then 1 else 0 end) as inprogress_overdue,
        sum(case when t.status = STATUS_CREATED and  trunc(t.end_date_planning) >= trunc(pReportDate) and hist.user_sid is not null then 1 else 0 end) as inprogress_onschedule,
        sum(case when t.status = STATUS_CREATED and  hist.user_sid is null then 1 else 0 end ) as unassigned
     from USER_TASK t
      join  USER_TASK_DECLARATION_REL t_decl on t_decl.task_id = t.task_id
      join  DECLARATION_HISTORY decl on decl.zip = t_decl.declaration_correction_id
      left join (select distinct task_id, user_sid, assign_date, max(assign_date) over (PARTITION by task_id) as max_assign_date
                from (select * from USER_TASK_ASSIGNMENT_HISTORY where trunc(assign_date) <= trunc(pReportDate) and user_sid <> TASK_NOT_ASSIGNED_SID )) hist
				on hist.task_id = t.task_id and trunc(hist.assign_date) = trunc(max_assign_date)
      group by decl.sono_code, t.type_id, decl.region_code)
loop
    begin

	select completed_onschedule, completed_overdue into v_completed_onschedule, v_completed_overdue from user_task_report_sono
    where task_type_id = line.type_id and region_code = line.region_code and sono_code = line.sono_code and report_id = pPrevReportId;
   	exception when others then
    v_completed_onschedule := 0;
    v_completed_overdue := 0;
    end;

    if line.completed_onschedule is null
    then line.completed_onschedule := 0;
    end if;

    v_completed_onschedule := v_completed_onschedule + line.completed_onschedule;

    if line.completed_overdue is null
    then line.completed_overdue := 0;
    end if;

    v_completed_overdue := v_completed_overdue + line.completed_overdue;

    insert into user_task_report_sono (report_id, sono_code, region_code, task_type_id, completed_onschedule, completed_overdue, inprogress_onschedule, inprogress_overdue, unassigned)
    values (v_report_id, line.sono_code, line.region_code, line.type_id, v_completed_onschedule, v_completed_overdue, line.inprogress_onschedule, line.inprogress_overdue, line.unassigned );

end loop;

    commit;
	return v_report_id;

	exception when others then
    rollback;
    P$LOG('F$CREATE_AGREGATE', sqlcode, sqlerrm, 0);
    return null;
END;

 FUNCTION F$CALCULATE_AGREGATE ( pReportDate in Date, pDateForCalculate in Date ) return number
  is
  v_prev_report_id number;
  v_days_in_past number;
  v_start_first_time number;
  v_deep_recursion number;
  v_report_exist_id number;
  begin
  v_days_in_past := 7;

  begin
  select id into v_report_exist_id from user_task_report where  trunc(report_date) = pDateForCalculate;
  exception when no_data_found then
  v_report_exist_id := null;
  end;

  if v_report_exist_id is not null then
  delete from user_task_report where id = v_report_exist_id;
  delete from user_task_report_user where report_id = v_report_exist_id;
  delete from user_task_report_sono where report_id = v_report_exist_id;
  end if;

  select case when exists (select 1 from user_task_report) then 0 else 1 end into v_start_first_time from dual ;

  if v_start_first_time = 1
  then
	return F$CREATE_AGREGATE(trunc(pDateForCalculate), 0);
  end if;

  v_deep_recursion := pReportDate - pDateForCalculate;

  if v_deep_recursion > v_days_in_past
  then
    return null;
  else
  	begin
	select id into v_prev_report_id from user_task_report where trunc(report_date) = pDateForCalculate-1;
	exception when no_data_found then
	v_prev_report_id := F$CALCULATE_AGREGATE(trunc(pReportDate), trunc(pDateForCalculate)-1);
	if v_prev_report_id is not null and v_prev_report_id > 0
	then return F$CREATE_AGREGATE(trunc(pDateForCalculate), v_prev_report_id);
	else return null;
	end if;
	end;

	if v_prev_report_id is not null
	then return F$CREATE_AGREGATE(trunc(pDateForCalculate), v_prev_report_id);
	else
		v_prev_report_id := F$CALCULATE_AGREGATE(trunc(pReportDate), trunc(pDateForCalculate)-1);
		if v_prev_report_id is not null and v_prev_report_id > 0
		then return f$create_agregate(trunc(pDateForCalculate), v_prev_report_id);
		else return null;
		end if;
	end if;
  end if;

  exception when others then
      p$log('F$CALCULATE_AGREGATE', sqlcode, sqlerrm, 0);
	  return null;
  end;


  PROCEDURE P$AGREGATE (pReportDate in date) AS
  v_report_id number;
  BEGIN
  v_report_id := F$CALCULATE_AGREGATE ( trunc(pReportDate), trunc(pReportDate) );
  if v_report_id is null then
   P$LOG('P$AGREGATE', sqlcode, 'Не возможно построить агрегат на дату '||pReportDate, 0);
  end if;
  exception when others then
    P$LOG('P$AGREGATE', sqlcode, sqlerrm, 0);
  END;

 PROCEDURE P$GET_REPORT_ID(pId out number, pStatus out number, pDate in date) as
 BEGIN
  select id into pId from user_task_report where report_date = trunc(pDate);
  pStatus := 1;
  exception when others then begin
    P$LOG('P$GET_REPORT_ID', sqlcode, sqlerrm, 0);
  	select id into pId from user_task_report where report_date < trunc(pDate) and rownum = 1;
    pId:=-1;
	  pStatus := 2;
    exception when others then begin
	    pStatus := 3;
    end;
  end;
 END;

PROCEDURE P$CLOSE_ALL_TASKS_BY_DECL(
    P_INN_DECLARANT VARCHAR2,
    P_KPP_EFFECTIVE VARCHAR2,
    P_PERIOD_EFFECTIVE NUMBER,
    P_FISCAL_YEAR NUMBER,
    P_TYPE_CODE NUMBER,
    P_CLOSE_REASON_ID NUMBER)
AS
col_tasks_id t$number_col;
BEGIN
    /*Перечень открытых ПЗ по ключу декларации*/
  col_tasks_id:= F$GET_OPENED_TASKS_BY_DECL(
                    P_INN_DECLARANT => P_INN_DECLARANT,
                    P_KPP_EFFECTIVE => P_KPP_EFFECTIVE,
                    P_PERIOD_EFFECTIVE => P_PERIOD_EFFECTIVE,
                    P_FISCAL_YEAR => P_FISCAL_YEAR,
                    P_TYPE_CODE =>P_TYPE_CODE);

    for i in 1..col_tasks_id.count
    loop
        begin
            P$CLOSE_TASK ( P_TASK_ID => col_tasks_id(i),
                            P_REASON_ID => P_CLOSE_REASON_ID,
                            P_CLOSE_DATE => sysdate,
                            P_CLOSE_STATUS => STATUS_CLOSED);
            exception when task_already_closed then
                        null;
        end;
    end loop;
END;

PROCEDURE P$CLOSE_ALL_TASKS_BY_CORR_ID(
                P_DECLARATION_CORRECTION_ID NUMBER,
                P_CLOSE_REASON_ID NUMBER)
as
col_tasks_id t$number_col;
BEGIN
    select distinct
        ut.task_id
    bulk collect into col_tasks_id
    from user_task ut
    inner join user_task_declaration_rel utd on utd.task_id = ut.task_id
    where
        utd.Declaration_Correction_Id = P_DECLARATION_CORRECTION_ID;

    for i in 1..col_tasks_id.count
    loop
        begin
            P$CLOSE_TASK ( P_TASK_ID => col_tasks_id(i),
                            P_REASON_ID => P_CLOSE_REASON_ID,
                            P_CLOSE_DATE => sysdate,
                            P_CLOSE_STATUS => STATUS_CLOSED);
            exception when task_already_closed then
                        null;
        end;
    end loop;
END;
end;
/
