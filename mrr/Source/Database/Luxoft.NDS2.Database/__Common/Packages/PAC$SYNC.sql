﻿create or replace package NDS2_MRR_USER.PAC$SYNC
as
  procedure START_WORK;

  procedure SYNC_EGRN;

  procedure SYNC_BSSCHET;

  procedure SYNC_SURS;

  procedure SYNC_NSI;
end;
/
create or replace package body NDS2_MRR_USER.PAC$SYNC
as

procedure log(p_msg varchar2, p_type number)
as
pragma autonomous_transaction;
begin

  dbms_output.put_line(p_type||'-'||p_msg);

  insert into NDS2_MRR_USER.SYSTEM_LOG(ID, TYPE, SITE, ENTITY_ID, MESSAGE_CODE, MESSAGE, LOG_DATE)
  values(Seq_Sys_Log_Id.Nextval, p_type, 'PAC$SYNC', null, 1, p_msg, sysdate);
  commit;
end;

procedure log_notification(p_msg varchar2)
as
begin
     log(p_msg, 0);
end;

procedure log_error(p_msg varchar2)
as
begin
     log(p_msg, 3);
end;

procedure EXECUTE_SQL( p_sql varchar2, p_context_name varchar2 := null, p_log_on_error boolean := false)
 as
begin
  execute immediate p_sql;
  exception when others then
    if p_log_on_error then
       log_error('Ошибка исполнения динамического запроса ('||p_context_name||'):'||sqlerrm);
    end if;
end;

procedure DROP_IF_EXIST
(
  p_table_name varchar2
)
as
begin
  execute immediate 'Alter session set ddl_lock_timeout = 300';
  for line in (select * from all_tables where owner = 'NDS2_MRR_USER' and table_name = p_table_name)
  loop
    execute immediate 'truncate table NDS2_MRR_USER.'||p_table_name;
    execute immediate 'drop table NDS2_MRR_USER.'||p_table_name||' purge';
  end loop;
  execute immediate 'Alter session set ddl_lock_timeout = 0';
  exception when others then null;
end;


procedure SYNC_EGRN_IP as
begin

  NDS2$SYS.LOG_INFO('PAC$SYNC.SYNC_EGRN_IP', '0', '0', '1. Выкачиваем исходные данные во временную таблицу');
  DROP_IF_EXIST('STAGE_EGRN_IP');
  execute immediate 'create table NDS2_MRR_USER.STAGE_EGRN_IP as select ' ||
  'cast(id as INTEGER) as id, ' ||
  'cast(ogrnip as CHAR(45)) as ogrnip, ' ||
  'cast(last_name as VARCHAR2(180)) as last_name, ' ||
  'cast(first_name as VARCHAR2(180)) as first_name, ' ||
  'cast(patronymic as VARCHAR2(180)) as patronymic, ' ||
  'cast(date_birth as DATE) as date_birth, ' ||
  'cast(doc_code as CHAR(6)) as doc_code, ' ||
  'cast(doc_num as VARCHAR2(75)) as doc_num, ' ||
  'cast(doc_date as DATE) as doc_date, ' ||
  'cast(doc_org as VARCHAR2(762)) as doc_org, ' ||
  'cast(doc_org_code as VARCHAR2(21)) as doc_org_code, ' ||
  'cast(adr as VARCHAR2(762)) as adr, ' ||
  'cast(code_no as CHAR(12)) as code_no, ' ||
  'cast(date_on as DATE) as date_on, ' ||
  'cast(reason_on as CHAR(9)) as reason_on, ' ||
  'cast(date_off as DATE) as date_off, ' ||
  'cast(reason_off as CHAR(6)) as reason_off, ' ||
  'cast(innfl as VARCHAR2(12 CHAR)) as innfl ' ||
  'from V_EGRN_IP@NDS2_FCOD';

  NDS2$SYS.LOG_INFO('PAC$SYNC.SYNC_EGRN_IP', '0', '0', '2. Чистим данные по фильтру');
  DROP_IF_EXIST('STAGE_EGRN_IP_FILTERED');
  execute immediate 'create table NDS2_MRR_USER.STAGE_EGRN_IP_FILTERED as (select * from (select ip.*, row_number() over (partition by innfl order by id desc) is_actual from NDS2_MRR_USER.STAGE_EGRN_IP ip ) T where T.is_actual = 1)';

  NDS2$SYS.LOG_INFO('PAC$SYNC.SYNC_EGRN_IP', '0', '0', '3. Добавляем индекс по ИНН');
  PAC$INDEX_MANAGER.P$ULTIMATE_VALIDATE_INDEXES('STAGE_EGRN_IP_FILTERED', 'STAGE_EGRN_IP');

  NDS2$SYS.LOG_INFO('PAC$SYNC.SYNC_EGRN_IP', '0', '0', '4. Производим подмену целевой таблицы');
  DROP_IF_EXIST('V_EGRN_IP_BAK');
  execute immediate 'alter table NDS2_MRR_USER.V_EGRN_IP rename to V_EGRN_IP_BAK';
  execute immediate 'alter table NDS2_MRR_USER.STAGE_EGRN_IP_FILTERED rename to V_EGRN_IP';

  NDS2$SYS.LOG_INFO('PAC$SYNC.SYNC_EGRN_IP', '0', '0', '5. Чистим все временные данные');
  DROP_IF_EXIST('STAGE_EGRN_IP');
  DROP_IF_EXIST('STAGE_EGRN_IP_FILTERED');
  DROP_IF_EXIST('V_EGRN_IP_BAK');

  NDS2$SYS.LOG_INFO('PAC$SYNC.SYNC_EGRN_IP', '0', '0', 'success');
  execute immediate 'truncate table NDS2_MRR_USER.STAGE_EGRN_IP';
  exception when others then
  begin
    rollback;
    NDS2$SYS.LOG_ERROR('PAC$SYNC.SYNC_EGRUL_IP', null, null, SUBSTR(SQLERRM, 1 , 2040));
  end;
end;


procedure SYNC_EGRN_UL as
begin

  NDS2$SYS.LOG_INFO('PAC$SYNC.SYNC_EGRN_UL', '0', '0', '1. Выкачиваем исходные данные во временную таблицу');
  DROP_IF_EXIST('STAGE_EGRN_UL');
  execute immediate 'create table NDS2_MRR_USER.STAGE_EGRN_UL as select ' ||
  'cast(id as NUMBER) as id, ' ||
  'cast(inn as VARCHAR2(10)) as inn, ' ||
  'cast(kpp as VARCHAR2(9)) as kpp, ' ||
  'cast(ogrn as VARCHAR2(13)) as ogrn, ' ||
  'cast(name_full as VARCHAR2(1000)) as name_full, ' ||
  'cast(date_reg as DATE) as date_reg, ' ||
  'cast(code_no as CHAR(4)) as code_no, ' ||
  q'[cast(nvl(date_on, to_date('01.01.1900', 'dd.mm.yyyy')) as DATE) as date_on,]' ||
  'cast(date_off as DATE) as date_off, ' ||
  'cast(reason_off as CHAR(2)) as reason_off, ' ||
  'cast(adr as VARCHAR2(254)) as adr, ' ||
  'cast(ust_capital as NUMBER(20,4)) as ust_capital, ' ||
  'cast(is_knp as CHAR(1)) as is_knp, ' ||
  'cast(F$GET_SHORT_TAXPAYER_NAME(name_full) as varchar2(1000)) as short_name '  ||
  'from V_EGRN_UL@NDS2_FCOD';

  NDS2$SYS.LOG_INFO('PAC$SYNC.SYNC_EGRN_UL', '0', '0', '2. Чистим данные по фильтру от Любы');
  DROP_IF_EXIST('STAGE_EGRN_UL_FILTERED');
  execute immediate '
  create table NDS2_MRR_USER.STAGE_EGRN_UL_FILTERED as (
  select
  egrn_ul.ID,
  egrn_ul.INN,
  egrn_ul.KPP,
  egrn_ul.OGRN,
  egrn_ul.NAME_FULL,
  egrn_ul.DATE_REG,
  egrn_ul.CODE_NO,
  egrn_ul.DATE_ON,
  egrn_ul.DATE_OFF,
  egrn_ul.REASON_OFF,
  egrn_ul.ADR,
  egrn_ul.UST_CAPITAL,
  egrn_ul.IS_KNP,
  egrn_ul.short_name
  from (
  select egrn_ul_cnt.*, substr(egrn_ul_cnt.kpp, 5,2) as kpp_sign,
  row_number() over (partition by egrn_ul_cnt.inn order by (
   case
    when egrn_ul_cnt.is_knp = 1 and egrn_ul_cnt.date_off is null then 1
    when substr(egrn_ul_cnt.kpp, 5,2) = ''01'' and egrn_ul_cnt.date_off is null then 2
    when substr(egrn_ul_cnt.kpp, 5,2) = ''01'' and egrn_ul_cnt.date_off is not null then 3
    else 4 end), egrn_ul_cnt.date_on desc
  ) as cnt
  from NDS2_MRR_USER.STAGE_EGRN_UL egrn_ul_cnt
  ) egrn_ul
  where egrn_ul.cnt = 1 and kpp_sign in (''01'', ''50'', ''51'', ''52'', ''53''))
  ';

  NDS2$SYS.LOG_INFO('PAC$SYNC.SYNC_EGRN_UL', '0', '0', '3. Добавляем индекс по ИНН');
  PAC$INDEX_MANAGER.P$ULTIMATE_VALIDATE_INDEXES('STAGE_EGRN_UL_FILTERED', 'STAGE_EGRN_UL');

  NDS2$SYS.LOG_INFO('PAC$SYNC.SYNC_EGRN_UL', '0', '0', '4. Производим подмену целевой таблицы');
  DROP_IF_EXIST('V_EGRN_UL_BAK');
  execute immediate 'alter table NDS2_MRR_USER.V_EGRN_UL rename to V_EGRN_UL_BAK';
  execute immediate 'alter table NDS2_MRR_USER.STAGE_EGRN_UL_FILTERED rename to V_EGRN_UL';

  NDS2$SYS.LOG_INFO('PAC$SYNC.SYNC_EGRN_UL', '0', '0', '5. Чистим все временные данные');
  DROP_IF_EXIST('STAGE_EGRN_UL');
  DROP_IF_EXIST('STAGE_EGRN_UL_FILTERED');
  DROP_IF_EXIST('V_EGRN_UL_BAK');

  NDS2$SYS.LOG_INFO('PAC$SYNC.SYNC_EGRN_UL', '0', '0', 'success');

  exception when others then
  begin
    rollback;
    NDS2$SYS.LOG_ERROR('PAC$SYNC.SYNC_EGRUL_UL', null, null, SUBSTR(SQLERRM, 1 , 2040));
  end;
end;

procedure RECAL_TAX_PAYER_MVIEW as
begin
  EXECUTE_SQL('DROP TABLE NDS2_MRR_USER.MV$TAX_PAYER_NAME_TMP' );
  log_notification('создание агрегата MV$TAX_PAYER_NAME_TMP');
  EXECUTE_SQL('create table NDS2_MRR_USER.MV$TAX_PAYER_NAME_TMP
  pctfree 0
  as
  (select
  ip.innfl as INN,
  nvl(ip.last_name, '''')||'' ''||nvl(ip.first_name, '''')||'' ''||nvl(ip.patronymic, '''') as NP_NAME,
  0 as TP_TYPE,
  '''' as KPP,
  ''111111111'' as KPP_EFFECTIVE,
  adr as address,
  ip.ogrnip as OGRN,
  null as ust_capital,
  ip.date_birth as date_reg,
  ip.date_on,
  ip.date_off,
  nvl(ip.last_name, '''')||'' ''||nvl(ip.first_name, '''')||'' ''||nvl(ip.patronymic, '''') as name_full,
  ip.code_no as sono_code
from
  v_egrn_ip ip
union
select
  ul.inn,
  ul.short_name as NP_NAME,
  1 as TP_TYPE,
  ul.KPP,
  NDS2_MRR_USER.F$GET_EFFECTIVE_KPP(ul.inn, ul.KPP) as KPP_EFFECTIVE,
  ul.adr as address,
  ul.ogrn,
  ul.ust_capital,
  ul.date_reg,
  ul.date_on,
  ul.date_off,
  ul.name_full,
  ul.code_no as sono_code
from
  v_egrn_ul ul)', 'MV$TAX_PAYER_NAME_TMP', true);

  log_notification('создание индекса idx_mvtp_name_inn');
  PAC$INDEX_MANAGER.P$ULTIMATE_VALIDATE_INDEXES('MV$TAX_PAYER_NAME_TMP', 'MV$TAX_PAYER_NAME');

  log_notification('подмена MV$TAX_PAYER_NAME_TMP на MV$TAX_PAYER_NAME');
  execute immediate 'Alter session set ddl_lock_timeout = 300';
  EXECUTE_SQL('DROP MATERIALIZED VIEW NDS2_MRR_USER.MV$TAX_PAYER_NAME');
  EXECUTE_SQL('DROP TABLE NDS2_MRR_USER.MV$TAX_PAYER_NAME');
  execute immediate 'Alter session set ddl_lock_timeout = 0';
  EXECUTE_SQL('alter table NDS2_MRR_USER.MV$TAX_PAYER_NAME_TMP RENAME TO MV$TAX_PAYER_NAME');

  log_notification('обновление MV$TAX_PAYER_NAME завершено');
end;

procedure SYNC_SUR as
begin
  DROP_IF_EXIST('STAGE_SUR');
  execute immediate 'create table NDS2_MRR_USER.STAGE_SUR as (select ' ||
  'cast(inn as VARCHAR2(12 CHAR)) as inn, ' ||
  'F$GET_EFFECTIVE_KPP(cast(inn as VARCHAR2(12 CHAR)), cast(kpp as VARCHAR2(9 CHAR))) as kpp_effective, ' ||
  'cast(fiscal_year as VARCHAR2(4 CHAR)) as fiscal_year, ' ||
  'cast(fiscal_period as VARCHAR2(2 CHAR)) as fiscal_period, ' ||
  'cast(sign_code as CHAR(1 CHAR)) as sign_code, ' ||
  'cast(update_date as DATE) as update_date ' ||
  'from NDS2_SUR.EXT_SUR)';

  DROP_IF_EXIST('EXT_SUR');
  execute immediate 'create table NDS2_MRR_USER.EXT_SUR as (select * from (select ul.*, row_number() over (partition by inn, kpp_effective, fiscal_year, fiscal_period order by update_date desc) is_actual from STAGE_SUR ul ) T where T.is_actual = 1)';
  execute immediate 'create index NDS2_MRR_USER.idx_sur_inn_y_p on NDS2_MRR_USER.EXT_SUR(inn, kpp_effective, fiscal_year, fiscal_period)';
  commit;
  NDS2$SYS.LOG_INFO('PAC$SYNC.SYNC_SUR', '0', '0', 'success');
  execute immediate 'truncate table NDS2_MRR_USER.STAGE_SUR';

  exception when others then
  begin
    rollback;
    NDS2$SYS.LOG_ERROR('PAC$SYNC.SYNC_SUR', null, null, SUBSTR(SQLERRM, 1 , 2040));
  end;
end;

procedure SYNC_BSSCHET_IP as
begin
  DROP_IF_EXIST('STAGE_BSSCHET_IP');
  execute immediate 'create table NDS2_MRR_USER.STAGE_BSSCHET_IP as select ' ||
  'cast(INN as VARCHAR2(12)) as INN, ' ||
  'cast(BIK as VARCHAR2(9)) as BIK, ' ||
  'cast(INNKO as VARCHAR2(10 char)) as INNKO, ' ||
  'cast(KPPKO as VARCHAR2(9 char)) as KPPKO, ' ||
  'cast(RNKO as VARCHAR2(12 char)) as RNKO, ' ||
  'cast(NFKO as VARCHAR2(12 char)) as NFKO, ' ||
  'cast(NAMEKO as VARCHAR2(1000)) as NAMEKO, ' ||
  'cast(NOMSCH as VARCHAR2(20)) as NOMSCH, ' ||
  'cast(PRVAL as VARCHAR2(1)) as PRVAL, ' ||
  'cast(DATEOPENSCH as DATE) as DATEOPENSCH, ' ||
  'cast(DATECLOSESCH as DATE) as DATECLOSESCH, ' ||
  'cast(VIDSCH as VARCHAR2(15 char)) as VIDSCH ' ||
  'from BSSCHET_IP@NDS2_FCOD';

  DROP_IF_EXIST('BSSCHET_IP');
  execute immediate 'create table NDS2_MRR_USER.BSSCHET_IP as select * from NDS2_MRR_USER.STAGE_BSSCHET_IP';
  execute immediate 'create index NDS2_MRR_USER.idx_bsschip_inn on NDS2_MRR_USER.BSSCHET_IP(inn)';

  commit;
  NDS2$SYS.LOG_INFO('PAC$SYNC.SYNC_BSSCHET_IP', '0', '0', 'success');
  execute immediate 'truncate table NDS2_MRR_USER.STAGE_BSSCHET_IP';

  exception when others then
  begin
    rollback;
    NDS2$SYS.LOG_ERROR('PAC$SYNC.SYNC_BSSCHET_IP', null, null, SUBSTR(SQLERRM, 1 , 2040));
  end;
end;

procedure SYNC_BSSCHET_UL as
begin
  DROP_IF_EXIST('STAGE_BSSCHET_UL');
  execute immediate 'create table NDS2_MRR_USER.STAGE_BSSCHET_UL as select ' ||
  'cast(INN as VARCHAR2(10)) as INN, ' ||
  'cast(KPP as VARCHAR2(9)) as KPP, ' ||
  'cast(BIK as VARCHAR2(9)) as BIK, ' ||
  'cast(INNKO as VARCHAR2(10 char)) as INNKO, ' ||
  'cast(KPPKO as VARCHAR2(9 char)) as KPPKO, ' ||
  'cast(RNKO as VARCHAR2(12 char)) as RNKO, ' ||
  'cast(NFKO as VARCHAR2(12 char)) as NFKO, ' ||
  'cast(NAMEKO as VARCHAR2(1000)) as NAMEKO, ' ||
  'cast(NOMSCH as VARCHAR2(20)) as NOMSCH, ' ||
  'cast(PRVAL as VARCHAR2(1)) as PRVAL, ' ||
  'cast(DATEOPENSCH as DATE) as DATEOPENSCH, ' ||
  'cast(DATECLOSESCH as DATE) as DATECLOSESCH, ' ||
  'cast(VIDSCH as VARCHAR2(15 char)) as VIDSCH ' ||
  'from BSSCHET_UL@NDS2_FCOD';

  DROP_IF_EXIST('BSSCHET_UL');
  execute immediate 'create table NDS2_MRR_USER.BSSCHET_UL as select * from NDS2_MRR_USER.STAGE_BSSCHET_UL';
  execute immediate 'create index NDS2_MRR_USER.idx_bsschul_inn on NDS2_MRR_USER.BSSCHET_UL(inn)';

  commit;
  NDS2$SYS.LOG_INFO('PAC$SYNC.SYNC_BSSCHET_UL', '0', '0', 'success');
  execute immediate 'truncate table NDS2_MRR_USER.STAGE_BSSCHET_UL';

  exception when others then
  begin
    rollback;
    NDS2$SYS.LOG_ERROR('PAC$SYNC.SYNC_BSSCHET_UL', null, null, SUBSTR(SQLERRM, 1 , 2040));
  end;
end;

procedure SONO_ADD_NEW_RELATION
as
pragma autonomous_transaction;
begin
/*создание записи нового управления*/
for line in 
(
  select s.s_code, s.s_name
  from 
  v$sono s
  left join sono_relation sr  on sr.sono_code = s.s_code
  where sr.sono_level = 2 and s.s_code like '%00' and s.s_code <> '0000'
  and sr.sono_code is null
) loop
    begin
      insert into sono_relation(sono_code, sono_level, sono_name, sono_parent, is_active, create_date)
      values (line.s_code, 2, line.s_name, '0000', 1, sysdate);
    end;
end loop;
/*создание записи новой инспекции*/
for line in 
(
  select s.s_code, s.s_name
  from 
  v$sono s
  left join sono_relation sr  on sr.sono_code = s.s_code and sr.sono_level = 3
  where sr.sono_code is null and s.s_code not like '%00' and s.s_code <> '0000'
) loop
    begin
      insert into sono_relation(sono_code, sono_level, sono_name, sono_parent, is_active, create_date)
      values (line.s_code, 3, line.s_name, substr(line.s_code, 1,2)||'00', 1, sysdate);
    end;  
end loop;
commit;
exception when  others  then 
    rollback;
    NDS2$SYS.LOG_ERROR('PAC$SYNC.SONO_ADD_NEW_RELATION', null, null, SQLERRM);
end;

procedure SYNC_SONO as
begin
  merge into ext_sono trg using (select * from v$sono_alias where ActualSign = 1) src
  on (src.s_code = trg.s_code)
  when matched then
  update set
    trg.s_name = replace(replace(replace(replace(replace(src.s_name,
          'Межрайонная инспекция Федеральной налоговой службы', 'МРИ ФНС России'),
          'Инспекция Федеральной налоговой службы', 'ИФНС России'),
          'Управление Федеральной налоговой службы', 'УФНС России'),
          'Межрегиональная инспекция Федеральной налоговой службы по крупнейшим налогоплательщикам', 'МИ ФНС России по КН'),
          'Межрегиональная инспекция Федеральной налоговой службы', 'МИ ФНС России'),
    trg.full_name = src.s_name
  when not matched then
  insert (
    trg.s_code,
    trg.s_name,
    trg.full_name
  ) values (
    src.s_code,
    replace(replace(replace(replace(replace(src.s_name,
          'Межрайонная инспекция Федеральной налоговой службы', 'МРИ ФНС России'),
          'Инспекция Федеральной налоговой службы', 'ИФНС России'),
          'Управление Федеральной налоговой службы', 'УФНС России'),
          'Межрегиональная инспекция Федеральной налоговой службы по крупнейшим налогоплательщикам', 'МИ ФНС России по КН'),
          'Межрегиональная инспекция Федеральной налоговой службы', 'МИ ФНС России'),
    src.s_name
  );
  commit;
  
  begin
    SONO_ADD_NEW_RELATION;
  end;
  
  exception when others then
  begin
    rollback;
    NDS2$SYS.LOG_ERROR('PAC$SYNC.SYNC_SONO', null, null, SUBSTR(SQLERRM, 1 , 2040));
  end;
end;

procedure SYNC_SSRF as
begin
  merge into ext_ssrf trg using (
    select
      cast(s.s_code as varchar2(4)) as s_code,
      cast(s.s_name as varchar2(512)) as s_name 
    from v$ssrf_alias s
      left join region_codes r on r.s_code = s.s_code
    where s.ActualSign = 1 and r.s_code is null
    union all
    select r.s_code, r.s_name
    from region_codes r
    ) src
  on (src.s_code = trg.s_code)
  when matched then
  update set
    trg.s_name = src.s_name
  when not matched then
  insert (
    trg.s_code,
    trg.s_name
  ) values (
    src.s_code,
    src.s_name
  );
  commit;
  exception when others then
  begin
    rollback;
    NDS2$SYS.LOG_ERROR('PAC$SYNC.SYNC_SSRF', null, null, SUBSTR(SQLERRM, 1 , 2040));
  end;
end;

procedure SYNC_OKVED as
begin
  merge into ext_okved trg using (select * from v$okved_alias where ActualSign = 1) src
  on (src.s_code = trg.s_code)
  when matched then
  update set
    trg.s_name = src.s_name
  when not matched then
  insert (
    trg.s_code,
    trg.s_name
  ) values (
    src.s_code,
    src.s_name
  );
  commit;
  exception when others then
  begin
    rollback;
    NDS2$SYS.LOG_ERROR('PAC$SYNC.SYNC_OKVED', null, null, SUBSTR(SQLERRM, 1 , 2040));
  end;
end;

procedure SYNC_EGRN as
begin
  SYNC_EGRN_UL;
  SYNC_EGRN_IP;
  RECAL_TAX_PAYER_MVIEW;
  NDS2$SYS.RECOMPILE_INVALID_OBJECTS;
end;

procedure SYNC_BSSCHET as
begin
  SYNC_BSSCHET_IP;
  SYNC_BSSCHET_UL;
  NDS2$SYS.RECOMPILE_INVALID_OBJECTS;
end;

procedure SYNC_NSI as
begin
  SYNC_SONO;
  SYNC_SSRF;
  SYNC_OKVED;
  NDS2$SYS.RECOMPILE_INVALID_OBJECTS;
end;

procedure SYNC_SURS as
begin
  SYNC_SUR;
  NDS2$SYS.RECOMPILE_INVALID_OBJECTS;
end;

procedure START_WORK as
begin
  SYNC_NSI;
  SYNC_BSSCHET;
end;

end;
/
