﻿CREATE OR REPLACE PACKAGE NDS2_MRR_USER."NDS2$EXPLAIN_REPLY" IS

PROCEDURE P$STAGE_CORRECT_UPDATE
(
  pExplainId IN NUMBER,
  pInvoiceId IN VARCHAR2,
  pFieldName IN VARCHAR2,
  pFieldValue IN VARCHAR2,
  pTypeOperation IN NUMBER
);

PROCEDURE P$STAGE_CORRECT_UPDATE_STATE
(
  pExplainId IN NUMBER,
  pInvoiceId IN VARCHAR2,
  pInvoiceState IN NUMBER
);

PROCEDURE P$STAGE_CORRECT_DELETE
(
  pExplainId IN NUMBER,
  pInvoiceId IN VARCHAR2
);

PROCEDURE P$STAGE_CORRECT_DELETE_ALL
(
  pExplainId IN NUMBER
);

PROCEDURE P$EXISTS_INVOICE_CORRECT
(
  pExplainId IN NUMBER,
  IsExists OUT NUMBER
);

--Обрабатывает пояснения, полученные из СЭОД
PROCEDURE PROCESS_EXPLAIN
(
    p_explainId IN NUMBER,
    p_receive_by_tks number
);

--Запускает асинхронную (через JOB) обработку пояснений, полученных из СЭОД.
--Вызов эквивалентен асинхронному запуску PROCESS_EXPLAIN
PROCEDURE PROCESS_EXPLAIN_ASYNC
(
    p_explainId IN NUMBER,
    p_receive_by_tks number
);

END NDS2$EXPLAIN_REPLY;
/

CREATE OR REPLACE PACKAGE BODY NDS2_MRR_USER."NDS2$EXPLAIN_REPLY" IS

/*log smth message*/
procedure log(p_msg varchar2, p_type number)
as
pragma autonomous_transaction;
begin
  /* dbms_output.put_line(p_type||'-'||p_msg); */
  insert into NDS2_MRR_USER.SYSTEM_LOG(ID, TYPE, SITE, ENTITY_ID, MESSAGE_CODE, MESSAGE, LOG_DATE)
  values(Seq_Sys_Log_Id.Nextval, p_type, 'NDS2$EXPLAIN_REPLY', null, 1, p_msg, sysdate);
  commit;
exception when others then
  rollback;
end;

/*log info message*/
procedure log_info(p_msg varchar2)
as
begin
  log(p_msg, 0);
end;

/*log error message*/
procedure log_err(p_msg varchar2)
as
begin
  log(p_msg, 3);
end;

PROCEDURE P$STAGE_CORRECT_UPDATE
(
  pExplainId IN NUMBER,
  pInvoiceId IN VARCHAR2,
  pFieldName IN VARCHAR2,
  pFieldValue IN VARCHAR2,
  pTypeOperation IN NUMBER
)
as
  stage_invoice_correct_id number(10);
  pExistsId NUMBER := -1;
begin

  if (pTypeOperation = 1) then
  begin

      SELECT id into pExistsId FROM dual
      left outer join (select id from STAGE_INVOICE_CORRECTED where explain_id = pExplainId and invoice_original_id = pInvoiceId and field_name = pFieldName) r on 1 = 1;

      if (pExistsId >= 0) then
      begin
        update STAGE_INVOICE_CORRECTED set field_value = pFieldValue where explain_id = pExplainId and invoice_original_id = pInvoiceId and field_name = pFieldName;
      end;
      else
      begin
        stage_invoice_correct_id := SEQ_STAGE_INVOICE_CORRECT.NEXTVAL;
        insert into STAGE_INVOICE_CORRECTED (id, explain_id, invoice_original_id, field_name, field_value)
        values(stage_invoice_correct_id, pExplainId, pInvoiceId, pFieldName, pFieldValue);
      end;
      end if;
      commit;
  end;
  else
  begin
      if (pTypeOperation = 2) then
      begin
        delete from STAGE_INVOICE_CORRECTED where explain_id = pExplainId and invoice_original_id = pInvoiceId and field_name = pFieldName;
        commit;
      end;
      end if;
  end;
  end if;

end;

PROCEDURE P$STAGE_CORRECT_UPDATE_STATE
(
  pExplainId IN NUMBER,
  pInvoiceId IN VARCHAR2,
  pInvoiceState IN NUMBER
)
as
  stage_invoice_correct_state_id number(10);
  pExistsId NUMBER := -1;
begin
  LOCK TABLE STAGE_INVOICE_CORRECTED_STATE IN EXCLUSIVE MODE;

  SELECT id into pExistsId FROM dual
  left outer join (select id from STAGE_INVOICE_CORRECTED_STATE where explain_id = pExplainId and invoice_original_id = pInvoiceId) r on 1 = 1;

  if (pExistsId >= 0) then
  begin
    update STAGE_INVOICE_CORRECTED_STATE set invoice_state = pInvoiceState where explain_id = pExplainId and invoice_original_id = pInvoiceId;
  end;
  else
  begin
    stage_invoice_correct_state_id := SEQ_STAGE_INVOICE_CORRECT_ST.NEXTVAL;
    insert into STAGE_INVOICE_CORRECTED_STATE (id, explain_id, invoice_original_id, invoice_state)
    values(stage_invoice_correct_state_id, pExplainId, pInvoiceId, pInvoiceState);
  end;
  end if;
  commit;
  
end;

PROCEDURE P$STAGE_CORRECT_DELETE
(
  pExplainId IN NUMBER,
  pInvoiceId IN VARCHAR2
)
as
begin
  
  delete from STAGE_INVOICE_CORRECTED_STATE where explain_id = pExplainId and invoice_original_id = pInvoiceId;
  delete from STAGE_INVOICE_CORRECTED where explain_id = pExplainId and invoice_original_id = pInvoiceId;
  commit;
  
end;

PROCEDURE P$STAGE_CORRECT_DELETE_ALL
(
  pExplainId IN NUMBER
)
as
begin
  
  delete from STAGE_INVOICE_CORRECTED_STATE where explain_id = pExplainId;
  delete from STAGE_INVOICE_CORRECTED where explain_id = pExplainId;
  commit;
  
end;

PROCEDURE P$EXISTS_INVOICE_CORRECT
(
  pExplainId IN NUMBER,
  IsExists OUT NUMBER
)
as
  countCorret NUMBER := -1;
  countCorretState NUMBER := -1;
begin
  
  SELECT countRec into countCorret FROM dual
  left outer join (select count(id) as countRec from STAGE_INVOICE_CORRECTED where explain_id = pExplainId) r on 1 = 1;

  SELECT countRec into countCorretState FROM dual
  left outer join (select count(id) as countRec from STAGE_INVOICE_CORRECTED_STATE where explain_id = pExplainId) r on 1 = 1;
  
  IsExists := 0;  
  if ((countCorret is not null and countCorret > 0) or
      (countCorretState is not null and countCorretState > 0)) then
  begin
      IsExists := 1;  
  end;
  end if;
  
end;

--Обрабатывает пояснения, полученные из СЭОД
PROCEDURE PROCESS_EXPLAIN
(
    p_explainId IN NUMBER,
    p_receive_by_tks number
)
as
 v_zip number; 
 v_doc_type number;
 v_found boolean;
begin
  log_info('PROCESS_EXPLAIN : ['||to_char(p_explainId)||'] Обработка пояснения');

  begin
    select decl.zip, doc.DOC_TYPE into v_zip, v_doc_type 
      from NDS2_SEOD.SEOD_EXPLAIN_REPLY REPL
        join NDS2_MRR_USER.V$DOC doc on DOC.DOC_ID = REPL.DOC_ID
        join NDS2_MRR_USER.declaration_history DECL on DECL.REG_NUMBER = doc.ref_entity_id and DECL.SONO_CODE_SUBMITED = DOC.SONO_CODE
      where repl.explain_id = p_explainId;
      v_found := TRUE;
    exception when NO_DATA_FOUND then
      v_found := FALSE;
      log_err('PROCESS_EXPLAIN : ['||to_char(p_explainId)||'] Документ не найден');
  end;
   
  if v_found then
    log_info('PROCESS_EXPLAIN : ['||to_char(p_explainId)||'] Документ найден: ZIP='||to_char(v_zip)||', TYPE='||to_char(v_doc_type));
    begin
      log_info('PROCESS_EXPLAIN : ['||to_char(p_explainId)||'] Добавление информации в историю изменения статусов');
      insert into NDS2_MRR_USER.hist_explain_reply_status(  id,   explain_id,   submit_date,   status_id) values (NDS2_MRR_USER.SEQ_HIST_EXPLAIN_REPLY.NEXTVAL,   p_explainId,   sysdate,   1);
    exception when others then
      log_err('PROCESS_EXPLAIN : ['||to_char(p_explainId)||'] Ошибка при добавлении информации в историю изменения статусов: '||sqlerrm);
    end;
    /* Проверка, что пришло пояснение не по ТКС */
    if ((p_receive_by_tks is null) or (p_receive_by_tks = 0)) and (v_doc_type = 1) then -- только для АТ по СФ
	  begin
	    log_info('PROCESS_EXPLAIN : ['||to_char(p_explainId)||'] Запуск обновления признака ПЗИ-П (увеличение)');
	    NDS2_MRR_USER.PAC$REMARKABLE_CHANGES.NO_TKS_UP_VERSION(v_zip);
	  exception when others then
	    log_err('PROCESS_EXPLAIN : ['||to_char(p_explainId)||'] Ошибка запуска обновления признака ПЗИ-П (увеличение): '||sqlerrm);
	  end;
    end if;
  end if;
  log_info('PROCESS_EXPLAIN : ['||to_char(p_explainId)||'] Обработка пояснения закончена');
exception when others then
  log_err('PROCESS_EXPLAIN : ['||to_char(p_explainId)||'] Ошибка при обработке пояснения: '||sqlerrm);
end;

--Запускает асинхронную (через JOB) обработку пояснений, полученных из СЭОД.
--Вызов эквивалентен асинхронному запуску PROCESS_EXPLAIN
PROCEDURE PROCESS_EXPLAIN_ASYNC
(
    p_explainId IN NUMBER,
    p_receive_by_tks number
)
as
v_job_id number;
begin
  log_info('PROCESS_EXPLAIN_ASYNC : Запуск асинхронной обработки пояснения. Параметры: [p_explainId]='||to_char(p_explainId)||', [p_receive_by_tks]='||to_char(p_receive_by_tks));

  dbms_job.submit(
       job => v_job_id,
       what => 'begin NDS2_MRR_USER.NDS2$EXPLAIN_REPLY.PROCESS_EXPLAIN(' || p_explainId || ',' || p_receive_by_tks || '); end;');
  log_info('PROCESS_EXPLAIN_ASYNC : Запущена асинхронная обработка пояснения. Параметры: [p_explainId]='||to_char(p_explainId)||', [p_receive_by_tks]='||to_char(p_receive_by_tks));
	   
  --Nds2$sys.LOG_INFO('NDS2$EXPLAIN_REPLY.PROCESS_EXPLAIN', null, 0,' Вызвана обработка пояснения: ' || p_explainId);

  commit;
exception when others then
  log_err('PROCESS_EXPLAIN_ASYNC : Ошибка запуска асинхронной обработки пояснения: '||sqlerrm);
  --Nds2$sys.LOG_ERROR('NDS2$EXPLAIN_REPLY.PROCESS_EXPLAIN' , null, sqlcode, 'Ошибка при обработке пояснения: ' || sqlerrm);
end;


END "NDS2$EXPLAIN_REPLY";
/
