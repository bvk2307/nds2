﻿create or replace package NDS2_MRR_USER.PAC$USERS as
function P$MERGE(
  p_user_name varchar2,
  p_user_sid varchar2
  ) return number;
end  PAC$USERS;
/
create or replace package body NDS2_MRR_USER.PAC$USERS as
function P$MERGE(
  p_user_name varchar2,
  p_user_sid varchar2
  ) return number
  as
PRAGMA AUTONOMOUS_TRANSACTION;

  retval mrr_user.Id%type;
  userName mrr_user.Name%type;
  begin
    select u.ID, u.Name
    into retval, userName
    from nds2_mrr_user.mrr_user u
    where u.sid = p_user_sid;
    
	if(userName != p_user_name and p_user_name is not null) then 
		update nds2_mrr_user.mrr_user 
		set name = p_user_name
		where id = retval;
		commit;
	end if;

    return retval;
    
    exception when no_data_found then
                    insert into nds2_mrr_user.mrr_user(ID,SID,NAME)
                    values(seq$mrr_user.nextval,p_user_sid,p_user_name)
                    returning ID into retval;
                    commit;
                    return retval;
                when others then
                    rollback;
                    raise;
  end;
end PAC$USERS;
/