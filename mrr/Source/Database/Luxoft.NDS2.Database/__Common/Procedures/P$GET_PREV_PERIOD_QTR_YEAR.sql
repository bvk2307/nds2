﻿create or replace PROCEDURE NDS2_MRR_USER.P$GET_PREV_PERIOD_QTR_YEAR(vp_date in date, vp_prev_tp_year out number, vp_prev_tp_quarter out number)
  as
    vx_year number;
    vx_month number;
  begin
    /*
    ПРОЦЕДУРА ВОЗВРАЩАЕТ ПРЕДЫДУЩИЙ ЗАВЕРШИВШИЙСЯ НАЛОГОВЫЙ ПЕРИОД ДЛЯ ДАТЫ ПЕРЕДАННОЙ В ПРОЦЕДУРУ
    Пример (аргумент - результат): 31.03.2017 - 4 кв. 2016; 01.04.2017 - 1кв. 2017
    */
    vx_year := extract(year from vp_date);
    vx_month := extract (month from vp_date) - 3;

    if (vx_month < 1) then
        vx_year := vx_year - 1;
        vx_month := 12 + vx_month;
    end if;

    vp_prev_tp_quarter := F$TAX_PERIOD_TO_QUARTER(vx_month);
    vp_prev_tp_year := vx_year;
  end;
  /