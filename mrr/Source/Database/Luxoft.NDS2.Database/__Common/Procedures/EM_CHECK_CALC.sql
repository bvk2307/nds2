﻿create or replace procedure 
/**
Процедура проверки корректности расчета агрегата МЭ и ДНР<br/>
Агрегат МЭ и детализация необработанных расхождений(ДНР) расчитывается в несколько этапов:
	1.Этап отбора корректирвок (результат шага в таблице EM_TM_MC_SEOD_DECL)
	2.Этап отбора КНП расхождений (результат шага в таблице EM_TM_DIS_UNQ)
	3.Слияние результатов отбора расхождений и отбора корректировок (результат шага в таблице EM_TM_DIS_DECL)
	4.Агрегация расхождений по ИНН и периоду (результат шага в таблице EFF_MON_BUYER_DECL_STG)
	5.Вставка данных в результирующую таблицу (результат шага в таблице EFFICIENCY_MONITOR_DETAIL)
	6.Отбор данных из V$EFFICIENCY_MONITOR_DETAIL для отображения в окне ДНР на клиенте
На каждом из этих этапов происходят внутренние слияния с разными таблицами, в результате чего расхождения 
могут исчезать из расчета.

Процедура проверяет появление расхождений, отобранных на предыдущем шаге расчета, 
в результирующем наборе текущего шага расчета. 
Если расхождение отобранное на предыдущем шаге не появляется в результатах текущего шага расчета, 
то оно логируется в таблицах:
	EM_CHECK_LOST_DIS - для открытых КНП расхождений не прошедших шаг отбора расхождений;
	EM_CHECK_LOST_DIS_MERGE - для открытых КНП расхождений отобранных на шаге отбора расхождений, но не прошедших шаг слияния с корректировками;
	EM_CHECK_LOST_DECL - для корерктировок отобранных на шаге агрегации данных, но не вошедших в V$EFFICIENCY_MONITOR_DETAIL.
Процедура анализирует выборку расхождений за последний законченный налоговый период.
*/  
NDS2_MRR_USER.EM_CHECK_CALC 
as
    v_site constant varchar2(30):='NDS2_MRR_USER.EM_CHECK_CALC';
    v_last_quarter number;
    v_fiscal_year number;
    v_calc_date date;
    pragma autonomous_transaction;
begin
    Nds2$Sys.Log_info(v_site,null,null,'Старт анализа расчета МЭ');

    -- Чистим от прошлого расчета
    nds2$sys.drop_if_exists('NDS2_MRR_USER','EM_CHECK_ZIP');
    nds2$sys.drop_if_exists('NDS2_MRR_USER','EM_CHECK_LOST_DIS');
    nds2$sys.drop_if_exists('NDS2_MRR_USER','EM_CHECK_LOST_DIS_MERGE');
    nds2$sys.drop_if_exists('NDS2_MRR_USER','EM_CHECK_LOST_DECL');

    -- Определяем последний законченый налоговый период  
    v_fiscal_year := extract(year from current_date);
    v_last_quarter := ceil(extract(month from current_date)/3)-1;
    if(v_last_quarter < 1) then
        v_fiscal_year := v_fiscal_year -1;
        v_last_quarter := v_last_quarter + 4; 
    end if;

    select max(calc_date) 
    into v_calc_date
    from efficiency_monitor_detail;
    
    if(v_calc_date is null) then
        Nds2$Sys.Log_info(v_site,null,null,'Последний расчет МЭ не найден.');
        return;
    end if;

    Nds2$Sys.Log_info(v_site,null,null,'Расчет МЭ от '||to_char(v_calc_date, 'DD.MM.YYYY HH24:MI:SS')||' анализируется по '
        ||v_last_quarter||' кварталу '||v_fiscal_year||' года');

    -- Отбираем идентификаторы корректировок за последний законченный период
	execute immediate '
    create table EM_CHECK_ZIP as
    select distinct 
        zip 
    from declaration_history
    where 
        fiscal_year = to_char('|| v_fiscal_year ||')
        and period_effective in (   select distinct 
                                        effective_value 
                                    from dict_tax_period 
                                    where quarter = '|| v_last_quarter ||')'
    ;

    Nds2$Sys.Log_info(v_site,null,null,'Кол-во корректировок по налоговму периоду: '
    ||sql%rowcount||' шт.');

    /*
    Находим расхождения не вошедшие в результирующий набор шага "Отбор расхождений"
    */
	execute immediate '
    create table EM_CHECK_LOST_DIS as 
    with 
    decl as (
        select * from EM_CHECK_ZIP
    )
    ,dis_to_eff as (
        select distinct
            dd.discrepancy_id
        from doc_discrepancy dd
		join sov_discrepancy sd
			on dd.discrepancy_id = sd.id
            and sd.invoice_chapter = 8
        join doc d 
            on d.doc_id = dd.doc_id
        where
			d.status not in (-1,1,3,11)			-- Отсекаем невалидные состояния
            and d.doc_type = 1					-- АТ по Сф
            and d.doc_kind in (1,2)				-- АТ по 1-й,2-й сторонам
			and seod_accepted = 1				-- Квитанция от СС получена (ЭОД-5)
    )
    ,dis_lost as (
        select discrepancy_id from dis_to_eff
        minus
        select discrepancy_id from EM_TM_DIS_UNQ
    )
    select 
        sd.*
    from dis_lost dl
    join sov_discrepancy sd 
        on dl.discrepancy_id = sd.id'
    ;

    Nds2$Sys.Log_info(v_site,null,null,'Найдено расхождений не прошедших в отобранные: '
        ||sql%rowcount||' шт.');    

    /*
    Находим расхождения которые были отобраны на шаге "отбор расхождений", но не попали в результирующий набор шага "МЕРЖ"
    */
	execute immediate '
    create table EM_CHECK_LOST_DIS_MERGE as 
    with 
    decl as (
        select * from EM_CHECK_ZIP
    )
    ,dis_collected as (
        select distinct 
            du.discrepancy_id
        from EM_TM_DIS_UNQ du
        join decl decl
            on decl.zip = du.buyer_decl_zip
		where du.exclude_from_calculation = 0
    )
    ,lost_dis as (
        select discrepancy_id from dis_collected
        minus
        select discrepancy_id from EM_TM_DIS_DECL
    )
    select 
        sd.*
    from lost_dis dl
    join sov_discrepancy sd 
        on dl.discrepancy_id = sd.id'    
    ;

    Nds2$Sys.Log_info(v_site,null,null,'Найдено расхождений не прошедших слияние с декларациями: '
        ||sql%rowcount||' шт.');    

    /*
    Перечень прошедших деклараций прошедших аггрегацию, но не попавших в V$EFFICIENCY_MONITOR_DETAIL ДНР
    */
	execute immediate '    
    create table EM_CHECK_LOST_DECL as 
    with
    decl as (
        select * from EM_CHECK_ZIP
    )
    ,decl_in_eff as (
        select distinct 
            IdDekl   
        from V$efficiency_monitor_detail
        where 
            fiscalyear = '|| v_fiscal_year ||'
            and quarter = '|| v_last_quarter ||'
            and calculationdate = to_date('' '|| v_calc_date ||' '') 
    )
    ,lost_decl as (
        select
            a.id
        from decl d
        join v$askdekl a
            on d.zip = a.zip
        join  EFF_MON_BUYER_DECL_STG e
            on a.id = e.id
        minus
        select IdDekl from decl_in_eff
    )
    select 
        dh.*
    from lost_decl ld
    join v$askdekl a
        on ld.id = a.id
    join declaration_history dh
        on a.zip = dh.zip'
    ;

    Nds2$Sys.Log_info(v_site,null,null,'Найдено корректировок не прошедших во вьюху ДНР: '
        ||sql%rowcount||' шт.');  

    Nds2$Sys.Log_info(v_site,null,null,'Завершение анализа расчета МЭ');   
    commit;
    exception when others then
        Nds2$Sys.LOG_ERROR(v_site,null,null,'Ошибка: '||substr(0,255,sqlerrm));
        commit;
end;
/

    