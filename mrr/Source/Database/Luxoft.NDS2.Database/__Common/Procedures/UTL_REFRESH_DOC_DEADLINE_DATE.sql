﻿create or replace procedure NDS2_MRR_USER.UTL_REFRESH_DOC_DEADLINE_DATE
(
p_doc_id in number
)
as
  v_doc_type number;
  v_timeout_answer_days number;
  v_timeout_delivery_days number;
  v_deadline_date date;
begin

  select doc_type into v_doc_type
  from doc
  where doc_id = p_doc_id;

  case v_doc_type
    when 1 then v_timeout_answer_days :=  utl_get_configuration_number('timeout_answer_for_autoclaim');
    else v_timeout_answer_days := 8;
  end case;
  v_deadline_date := utl_add_work_days(sysdate, v_timeout_answer_days);

  v_timeout_delivery_days := utl_get_configuration_number('timeout_claim_delivery');
  v_deadline_date := utl_add_work_days(v_deadline_date, v_timeout_delivery_days);

  if v_doc_type = 2 or v_doc_type = 3 then
     v_deadline_date := null;
  end if;

  update doc
  set deadline_date = v_deadline_date
  where doc_id = p_doc_id;

  commit;

end;
/
