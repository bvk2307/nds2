﻿create or replace procedure nds2_mrr_user.UTL_CLOSE_DOCUMENT
(
p_reg_num in number,
p_sono_code in varchar2,
p_date in date := sysdate,
p_close_reason IN number := NULL
)
as
begin
  update doc
  set status = 10, status_date = trunc(p_date), close_date = p_date, close_reason = p_close_reason
  where ref_entity_id = p_reg_num
		and sono_code = p_sono_code
		and status not in (3,9,10) ;

  if (sql%rowcount > 0) then

     for doc_item in
     (
        select d.doc_id from doc d
        where d.ref_entity_id = p_reg_num and d.sono_code = p_sono_code
     )
     loop
        -- если документ закрыт по причине закрытия КНП, 
        -- то заносим в doc_closed_knp (КНП для документа закрыта)
        if p_close_reason = 3 then
          insert into doc_knp_closed (doc_id)
          values (doc_item.doc_id);
        end if;
        
        insert into claim_closed (doc_id)
        values (doc_item.doc_id);

        insert into doc_status_history(doc_id, status, status_date)
        select doc.doc_id, doc.status, doc.status_date from
        (
           select doc_item.doc_id as doc_id, 10 as status, p_date as status_date from dual
        ) doc
        left join doc_status_history hist on hist.doc_id = doc.doc_id and hist.status = 10
        where hist.doc_id is null;
     end loop;

  else
    if p_close_reason = 3 then
        -- если документ закрыт по причине закрытия КНП, 
        -- то заносим в doc_closed_knp (КНП для документа закрыта)
       for doc_item in
       (
          select d.doc_id from doc d
          where d.ref_entity_id = p_reg_num and d.sono_code = p_sono_code
       )
       loop
          insert into doc_knp_closed (doc_id)
          values (doc_item.doc_id);
       end loop;
    end if;
  end if;

  commit;

end;
/
