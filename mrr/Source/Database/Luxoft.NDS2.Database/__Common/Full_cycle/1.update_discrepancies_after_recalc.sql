﻿create table NDS2_MRR_USER.sov_discrepancy_build
PCTFREE 0
COMPRESS FOR ALL OPERATIONS
NOLOGGING
as
  select 
  sd.ID
  ,sd.CREATE_DATE
  ,sd.TYPE
  ,case when sda.RULE_GROUP is null then sd.RULE_GROUP else sda.RULE_GROUP end as RULE_GROUP
  ,case when sda.DEAL_AMNT is null then sd.DEAL_AMNT else sda.DEAL_AMNT end as DEAL_AMNT
  ,case when sda.AMNT is null then sd.AMNT else sda.AMNT end as AMNT
  ,case when sda.AMOUNT_PVP is null then sd.AMOUNT_PVP else sda.AMOUNT_PVP end as AMOUNT_PVP
  ,sd.INVOICE_CHAPTER
  ,case when sda.INVOICE_RK is null then sd.INVOICE_RK else sda.INVOICE_RK end as INVOICE_RK
  ,case when seod_decl.nds2_id is null then seod_decl.nds2_id else seod_decl.nds2_id end as DECL_ID
  ,case when sda.INVOICE_CONTRACTOR_CHAPTER is null then sd.INVOICE_CONTRACTOR_CHAPTER else sda.INVOICE_CONTRACTOR_CHAPTER end as INVOICE_CONTRACTOR_CHAPTER
  ,case when sda.INVOICE_CONTRACTOR_RK is null then sd.INVOICE_CONTRACTOR_RK else sda.INVOICE_CONTRACTOR_RK end as INVOICE_CONTRACTOR_RK
  ,case when seod_decl2.nds2_id is null then seod_decl2.nds2_id else seod_decl2.nds2_id end as DECL_CONTRACTOR_ID
  ,case when sda.id is not null then 1 else 0 end as STATUS
  ,sd.USER_COMMENT
  ,case when sda.RULE_NUM is null then sd.RULE_NUM else sda.RULE_NUM end as RULE_NUM
from sov_discrepancy sd
left join sov_discrepancy_all sda on sda.id = sd.id
join 
(
     select row_number() over (partition by a.zip order by a.NOMKORR desc, a.id desc) rn, 
            a.* from NDS2_MRR_USER.V$ASK_DECLANDJRNL a 
) ask_decl on ask_decl.zip = sda.zip
join NDS2_MRR_USER.SEOD_DECLARATION seod_decl
     on     seod_decl.tax_period = ask_decl.period
        and seod_decl.fiscal_year = ask_decl.otchetgod
        and seod_decl.inn = ask_decl.innnp
        and seod_decl.correction_number = ask_decl.nomkorr      
join 
(
     select row_number() over (partition by a.zip order by a.NOMKORR desc, a.id desc) rn,
     a.* from NDS2_MRR_USER.V$ASK_DECLANDJRNL a 
) ask_decl2 on ask_decl2.zip = sda.contractor_zip  
join NDS2_MRR_USER.SEOD_DECLARATION seod_decl2
     on     seod_decl2.tax_period = ask_decl2.period
        and seod_decl2.fiscal_year = ask_decl2.otchetgod
        and seod_decl2.inn = ask_decl2.innnp
        and seod_decl2.correction_number = ask_decl2.nomkorr;

insert /*+ APPEND*/ into  NDS2_MRR_USER.sov_discrepancy_build 
select 
seq_discrepancy_id.nextval
,sda.CREATE_DATE
,sda.TYPE
,sda.RULE_GROUP
,sda.DEAL_AMNT
,sda.AMNT
,sda.AMOUNT_PVP
,sda.INVOICE_CHAPTER
,sda.INVOICE_RK
,seod_decl.nds2_id
,sda.INVOICE_CONTRACTOR_CHAPTER
,sda.INVOICE_CONTRACTOR_RK
,seod_decl2.nds2_id
,1
,null
,sda.RULE_NUM
from sov_discrepancy_all sda
left join  sov_discrepancy sd on sda.id = sd.id
join 
(
     select row_number() over (partition by a.zip order by a.NOMKORR desc, a.id desc) rn, 
            a.* from NDS2_MRR_USER.V$ASK_DECLANDJRNL a 
) ask_decl on ask_decl.zip = sda.zip
join NDS2_MRR_USER.SEOD_DECLARATION seod_decl
     on     seod_decl.tax_period = ask_decl.period
        and seod_decl.fiscal_year = ask_decl.otchetgod
        and seod_decl.inn = ask_decl.innnp
        and seod_decl.correction_number = ask_decl.nomkorr      
join 
(
     select row_number() over (partition by a.zip order by a.NOMKORR desc, a.id desc) rn,
     a.* from NDS2_MRR_USER.V$ASK_DECLANDJRNL a 
) ask_decl2 on ask_decl2.zip = sda.contractor_zip  
join NDS2_MRR_USER.SEOD_DECLARATION seod_decl2
     on     seod_decl2.tax_period = ask_decl2.period
        and seod_decl2.fiscal_year = ask_decl2.otchetgod
        and seod_decl2.inn = ask_decl2.innnp
        and seod_decl2.correction_number = ask_decl2.nomkorr
where sd.id is null;

commit;

alter table sov_discrepancy rename to t3_sov_discrepancy;
alter table sov_discrepancy_build rename to sov_discrepancy;
