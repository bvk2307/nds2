-- MC full cycle step - refresh materialized view
-- Author - Mekhryakov Maxim
-- Date of creation 13.08.2015
-- Modified By Zykin Dmitry at 17.09.2015

WHENEVER SQLERROR EXIT SQL.SQLCODE

insert /*+ APPEND */ into NDS2_MRR_USER.CONTROL_RATIO_QUEUE (id,sono_code,decl_reg_num,is_sent)
select distinct
   mc_decl.id
  ,mc_decl.kodno
  ,seod_decl.decl_reg_num
  ,0
from (
  select distinct 
     d.id
    ,d.idfajl
    ,d.nomkorr
    ,d.innnp
    ,d.period
    ,d.otchetgod
    ,d.kodno
  from 
    NDS2_MRR_USER.V$ASKKontrSoontosh ks
  inner join NDS2_MRR_USER.v$askdekl d on d.id = ks.iddekl
  ) mc_decl
  inner join (
    select
       sd.inn
      ,sd.tax_period
      ,sd.fiscal_year
      ,sd.id_file
      ,sd.correction_number
      ,sd.decl_reg_num
      ,row_number() over (partition by sd.inn,sd.tax_period,sd.fiscal_year,sd.id_file,sd.correction_number order by sd.decl_reg_num asc) as actual_reg_num
    from NDS2_SEOD.SEOD_DECLARATION sd  
  ) seod_decl 
    on    seod_decl.actual_reg_num = 1
      and seod_decl.inn = mc_decl.innnp
      and seod_decl.tax_period = mc_decl.period
      and seod_decl.fiscal_year = mc_decl.otchetgod
      and seod_decl.correction_number = mc_decl.nomkorr
      and seod_decl.id_file = mc_decl.idfajl
  left join NDS2_MRR_USER.CONTROL_RATIO_QUEUE q on q.id = mc_decl.id
where q.id is null;
 
commit;

exit;
