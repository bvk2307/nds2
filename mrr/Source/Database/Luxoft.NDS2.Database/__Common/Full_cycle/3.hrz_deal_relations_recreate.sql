WHENEVER SQLERROR EXIT SQL.SQLCODE

begin
  NDS2_MRR_USER.NDS2$SYS.DROP_IF_EXISTS('NDS2_MRR_USER', 'TMP_HRZ_DECL_INFO');
end;
/

create table NDS2_MRR_USER.TMP_HRZ_DECL_INFO
pctfree 0
compress
as
select * from
(
select d.ch8_deals_amnt_total,
       d.ch9_deals_amnt_total,
       d.decl_sign,
       d.compensation_amnt,
       d.gap_discrep_amnt,
       z.CH8_NDS,
       z.CH9_NDS,
       case when z.ch8_nds is null or z.ch9_nds is null or z.ch9_nds = 0 then null else 100 * z.ch8_nds / z.ch9_nds end as share_nds_amnt,
       d.inn,
       d.fiscal_year,
       d.tax_period,
       d.is_active,
       d.decl_type_code,
       d.soun_code,
       d.soun_name,
       d.processingstage,
    d.region,
    d.region_code,
    d.region_name,
    row_number() over (partition by d.inn,d.fiscal_year,d.tax_period order by d.decl_type_code asc,d.is_active desc) rn
  from declaration_history d
  inner join 
(
select 
t.id as decl_id,
t.inn,
sum(case when nvl(z1.PRIZNAKAKT, 0) = 0 then nvl(z1.SUMNDSPROD18, 0) + nvl(z1.SUMNDSPROD10, 0) else nvl(z2.SUMNDSPROD18, 0) + nvl(z2.SUMNDSPROD10, 0) end) as CH9_NDS,
sum(case when nvl(z1.PRIZNAKAKT, 0) = 0 then nvl(z1.SUMNDSPOK, 0) else nvl(z2.SUMNDSPOK, 0)  end) as CH8_NDS
from v$asksvodzap z1
inner join 
(
      select
     row_number() over (partition by d.innnp, d.OTCHETGOD, d.PERIOD, d.NOMKORR order by d.ID desc) as RN,
      d.ID,
      d.INNNP as inn
      from  
      v$askdekl d 
) t on t.id = z1.IdDekl and t.rn = 1
left join v$asksvodzap z2 on z2.ID = z1.IdAkt
where z1.TIPFAJLA in (1,3,0,2)
group by t.id, t.inn
) z on z.decl_id = d.ask_decl_id
where d.is_active = 1) t
where t.rn = 1;

create index NDS2_MRR_USER.idx_hrz_decl_info  on NDS2_MRR_USER.TMP_HRZ_DECL_INFO(inn, fiscal_year, tax_period, is_active)  compress;

DECLARE
  v_exist number(1);
BEGIN
   select nvl(count(1), 0) into v_exist from all_tables where owner = 'NDS2_MRR_USER' and table_name = 'HRZ_RELATIONS';
   if v_exist > 0 then
      NDS2_MRR_USER.NDS2$SYS.DROP_IF_EXISTS('NDS2_MRR_USER', 'HRZ_RELATIONS_BACKUP');
      EXECUTE IMMEDIATE 'alter table NDS2_MRR_USER.HRZ_RELATIONS rename to HRZ_RELATIONS_BACKUP';
   end if;
  begin
  EXECUTE IMMEDIATE 'drop index NDS2_MRR_USER.IX_RELATIONS_BA'; 
  EXECUTE IMMEDIATE 'drop index NDS2_MRR_USER.IX_RELATIONS_SA'; 
  EXECUTE IMMEDIATE 'drop index NDS2_MRR_USER.IX_RELATIONS_REV';  
  EXECUTE IMMEDIATE 'drop index NDS2_MRR_USER.IX_RELATIONS_CH8_AMNT'; 
  EXECUTE IMMEDIATE 'drop index NDS2_MRR_USER.IX_RELATIONS_CH11_AMNT'; 
  EXECUTE IMMEDIATE 'drop index NDS2_MRR_USER.IX_RELATIONS_CH9_AMNT'; 
  EXECUTE IMMEDIATE 'drop index NDS2_MRR_USER.IX_RELATIONS_CH10_AMNT'; 
  EXECUTE IMMEDIATE 'drop index NDS2_MRR_USER.IX_RELATIONS_CH12_AMNT'; 
  EXECUTE IMMEDIATE 'drop index NDS2_MRR_USER.IX_RELATIONS_NDS_CH9';
  EXECUTE IMMEDIATE 'drop index NDS2_MRR_USER.IX_RELATIONS_NDS_CH12';
  exception when others then null;
  end;
END;
/

create table NDS2_MRR_USER.HRZ_RELATIONS
pctfree 0
compress
as
select 
  t.inn_1
,t.inn_2
,t.year
,t.qtr + 20 as period
,t.CHAPTER8_AMOUNT as CHAPTER8_AMOUNT
,t.CHAPTER9_AMOUNT as CHAPTER9_AMOUNT
,t.CHAPTER10_AMOUNT as CHAPTER10_AMOUNT
,t.CHAPTER11_AMOUNT as CHAPTER11_AMOUNT
,t.CHAPTER12_AMOUNT as CHAPTER12_AMOUNT
,t.CHAPTER8_COUNT as CHAPTER8_COUNT
,t.CHAPTER9_COUNT as CHAPTER9_COUNT
,t.CHAPTER10_COUNT as CHAPTER10_COUNT
,t.CHAPTER11_COUNT as CHAPTER11_COUNT
,t.CHAPTER12_COUNT as CHAPTER12_COUNT
,t.NDS_CHAPTER9 as NDS_CHAPTER9
,t.NDS_CHAPTER12 as NDS_CHAPTER12
,(nvl(t.CHAPTER9_AMOUNT, 0) + nvl(t.CHAPTER10_AMOUNT, 0) + nvl(t.CHAPTER12_AMOUNT, 0)) as sa
,(nvl(t.CHAPTER8_AMOUNT, 0) + nvl(t.CHAPTER11_AMOUNT, 0)) as ba
,t.NDS_BUYER as nba  
 ,(nvl(t.NDS_CHAPTER9, 0) + nvl(t.NDS_CHAPTER12, 0)) as nsa
,t.njsa as njsa
,t.njba as njba
,t.gap_amount_noagent as gap_amount
,decl.ch8_deals_amnt_total as total_purchase_amnt
,decl.ch9_deals_amnt_total as total_sales_amnt
,decl.ch8_nds as deduction_nds
,decl.ch9_nds as calc_nds
,decl.decl_sign as declaration_type
,decl.compensation_amnt as total_nds
,decl.gap_discrep_amnt
,decl.share_nds_amnt
,row_number() over (partition by t.year,t.qtr,t.inn_1 order by t.NDS_BUYER desc,inn_2 asc) as nba_order
,row_number() over (partition by t.year,t.qtr,t.inn_1 order by (nvl(t.NDS_CHAPTER9, 0) + nvl(t.NDS_CHAPTER12, 0)) desc,inn_2 asc) as nsa_order
,to_number(decl.tax_period||t.year||substr(t.inn_1,13)) as decl_id
,decl.decl_type_code as decl_type_code
,decl.soun_code as soun_code
,decl.soun_name as soun_name
,decl.processingstage
,decl.region
,decl.region_code
,decl.region_name
,case when inn_1 = inn_2 then 1 else 0 end as rev_flag
from (select * from NAVIGATOR_AGGREGATE x where (x.inn_1 is not null and x.inn_2 is not null)) t
left join TMP_HRZ_DECL_INFO decl 
  on   decl.inn = T.inn_2 
   and decl.fiscal_year = t.year 
   and decl.tax_period = t.qtr + 20
   and decl.is_active = 1;

create index NDS2_MRR_USER.IX_RELATIONS_BA on NDS2_MRR_USER.HRZ_RELATIONS (year,period,inn_1,nba_order) compress;
create index NDS2_MRR_USER.IX_RELATIONS_SA on NDS2_MRR_USER.HRZ_RELATIONS (year,period,inn_1,nsa_order) compress;
create index NDS2_MRR_USER.IX_RELATIONS_REV on NDS2_MRR_USER.HRZ_RELATIONS (year,period,inn_1,inn_2) compress;  
create index NDS2_MRR_USER.IX_RELATIONS_CH8_AMNT on NDS2_MRR_USER.HRZ_RELATIONS (year,period,inn_1,CHAPTER8_AMOUNT) compress;
create index NDS2_MRR_USER.IX_RELATIONS_CH11_AMNT on NDS2_MRR_USER.HRZ_RELATIONS (year,period,inn_1,CHAPTER11_AMOUNT) compress;
create index NDS2_MRR_USER.IX_RELATIONS_CH9_AMNT on NDS2_MRR_USER.HRZ_RELATIONS (year,period,inn_1,CHAPTER9_AMOUNT) compress;
create index NDS2_MRR_USER.IX_RELATIONS_CH10_AMNT on NDS2_MRR_USER.HRZ_RELATIONS (year,period,inn_1,CHAPTER10_AMOUNT) compress;
create index NDS2_MRR_USER.IX_RELATIONS_CH12_AMNT on NDS2_MRR_USER.HRZ_RELATIONS (year,period,inn_1,CHAPTER12_AMOUNT) compress;
create index NDS2_MRR_USER.IX_RELATIONS_NDS_CH9 on NDS2_MRR_USER.HRZ_RELATIONS (year,period,inn_1,NDS_CHAPTER9) compress;
create index NDS2_MRR_USER.IX_RELATIONS_NDS_CH12 on NDS2_MRR_USER.HRZ_RELATIONS (year,period,inn_1,NDS_CHAPTER12) compress;


begin
  NDS2_MRR_USER.NDS2$SYS.DROP_IF_EXISTS('NDS2_MRR_USER', 'HRZ_RELATIONS_TOTAL');
end;
/

create table NDS2_MRR_USER.HRZ_RELATIONS_TOTAL
pctfree 0
compress
as
select
inn_1 as inn, 
 year, 
 period,
count(case when ba > 0 then 1 end) as sellers_cnt,
count(case when sa > 0 then 1 end) as buyers_cnt
from HRZ_RELATIONS
group by inn_1, year, period;

create index NDS2_MRR_USER.IX_RELATIONS_TOTAL on NDS2_MRR_USER.HRZ_RELATIONS_TOTAL (year,period,inn) compress;


exit;
