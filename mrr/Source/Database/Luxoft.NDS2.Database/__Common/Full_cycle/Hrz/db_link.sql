﻿begin
for line in (select * from all_db_links where db_link like  'NDS2_MRR_PROM%') loop
  execute immediate 'drop public database link '||line.db_link;
end loop;
end;
/
 
create public database link NDS2_MRR_PROM
CONNECT TO NDS2_MRR_USER IDENTIFIED BY NDS2_MRR_USER
using '(DESCRIPTION =(ADDRESS = (PROTOCOL = TCP)(HOST = m9965-ais097)(PORT = 1521))(CONNECT_DATA = (SERVER = DEDICATED)(SERVICE_NAME = nds2)))';

