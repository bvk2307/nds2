﻿
CREATE OR REPLACE PACKAGE NDS2_MRR_USER.PAC$HRZ_REPORT
as

procedure FIND_TXPR_BY_DISCREPANCY
(
 p_sono_code varchar2,
 p_year varchar2,
 p_qtr varchar2,
 p_inn varchar2 := null,
 p_cursor out sys_refcursor
);

procedure FIND_TXPR_BY_NDS_COMPENSATION
(
 p_sono_code varchar2,
 p_year varchar2,
 p_qtr varchar2,
 p_inn varchar2 := null,
 p_cursor out sys_refcursor
);

procedure FIND_TXPR_BY_INN
  (
   p_inn in varchar2,
   p_cursor out sys_refcursor
  );


procedure TAXPAYER_RELATION
  (
    p_Inn IN varchar2,              /* »ЌЌ Ќѕ */
    p_MaxContractors IN number,     /*  олво наиболее значимых  */
    p_ByPurchase IN NUMBER,         /* направление - в сторону покупок или продаж */
    p_Year varchar2 := null,        /* отетный год */
    p_Qtr varchar2 := null,         /* отчетный период 1-4 */
    p_levels in number := null,     /* ”ровень автоматического построени¤ */
    p_sharePercentNdsCriteria in number := null, /* ќграничение отбора Ќѕ в дерево по % Ќ?— */
    p_Cursor OUT SYS_REFCURSOR      /* */
   );

end;
/

CREATE OR REPLACE PACKAGE BODY NDS2_MRR_USER.PAC$HRZ_REPORT
as

procedure log_call
(
p_ev_name varchar2,
p_param varchar2
)
as
pragma autonomous_transaction;
begin
  insert into hrz_log(id, ev_name, params , dt )
  values (SEQ_HRZ_LOG.NEXTVAL, p_ev_name,p_param, sysdate );
  commit;

  exception when others then rollback;
end;

procedure FIND_TXPR_BY_DISCREPANCY
(
 p_sono_code varchar2,
 p_year varchar2,
 p_qtr varchar2,
 p_inn varchar2 := null,
 p_cursor out sys_refcursor
)
as
begin
log_call('FIND_TXPR_BY_DISCREPANCY',  'sono:'||p_sono_code||', year:'||p_year||', qtr'||p_qtr);

open p_Cursor for select * from (
    select
     egrn.inn,
     egrn.np_name as name,
     0.0 as gap_amnt
    from mv$tax_payer_name egrn
    order by egrn.inn) t
    where rownum < 50; -- заглушка
    /*from hrz_decl_seller_gap  dsg
      inner join mv$tax_payer_name egrn on egrn.inn = dsg.inn
    where
      dsg.inn like p_inn||'%'
    order by dsg.gap_amnt desc*/
end;

procedure FIND_TXPR_BY_NDS_COMPENSATION
(
 p_sono_code varchar2,
 p_year varchar2,
 p_qtr varchar2,
 p_inn varchar2 := null,
 p_cursor out sys_refcursor
)
as
begin
  log_call('FIND_TXPR_BY_NDS_COMPENSATION',  'sono:'||p_sono_code||', year:'||p_year||', qtr'||p_qtr);
    open p_Cursor for
    select
      egrn.inn,
      egrn.np_name as name,
      0.0 as sumnds
   from mv$tax_payer_name egrn
    /*from hrz_decl_compensation tpc
    where tpc.rn = 1
     and tpc.sono = p_sono_code
     and tpc.year = p_year
     and tpc.qtr = p_qtr
     and (p_inn is null or tpc.inn like p_inn||'%')
    order by tpc.sumnds asc*/;
end;

procedure FIND_TXPR_BY_INN
  (
   p_inn in varchar2,
   p_cursor out sys_refcursor
  )
as
begin
  log_call('FIND_TXPR_BY_INN',  'inn:'||p_inn);
  open p_Cursor for
select
       0 as compensation_sum,
       0 as nds_dif_sum,
       0 as nds_deduction,
       0 as nds_calculated,
       0 as buy_sum,
       0 as sale_sum,
       '  возмещению' as declaration_type,
       inn,
       kpp,
      regexp_replace(regexp_replace(regexp_replace(np_name,
		'[Оо][Бб][Щщ][Ее][Сс][Тт][Вв][Оо][[:blank:]]+[Сс][[:blank:]]+[Оо][Гг][Рр][Аа][Нн][Ии][Чч][Ее][Нн][Нн][Оо][Йй][[:blank:]]+[Оо][Тт][Вв][Ее][Тт][Сс][Тт][Вв][Ее][Нн][Нн][Оо][Сс][Тт][Ьь][Юю]','ООО'),
		'[Оо][Тт][Кк][Рр][ыЫ][Тт][Оо][Ее][[:blank:]]+[Аа][Кк][Цц][Ии][Оо][Нн][Ее][Рр][Нн][Оо][Ее][[:blank:]]+[Оо][Бб][Щщ][Ее][Сс][Тт][Вв][Оо]','ОАО'),
		'[Зз][Аа][Кк][Рр][ыЫ][Тт][Оо][Ее][[:blank:]]+[Аа][Кк][Цц][Ии][Оо][Нн][Ее][Рр][Нн][Оо][Ее][[:blank:]]+[Оо][Бб][Щщ][Ее][Сс][Тт][Вв][Оо]','ЗАО')
        as name
        from mv$tax_payer_name where inn like p_inn||'%';
end;

PROCEDURE TAXPAYER_RELATION
(
  p_Inn IN varchar2,              /* »ЌЌ Ќѕ */
  p_MaxContractors IN number,     /*  олво наиболее значимых  */
  p_ByPurchase IN NUMBER,         /* направление - в сторону покупок или продаж */
  p_Year varchar2 := null,        /* отетный год */
  p_Qtr varchar2 := null,         /* отчетный период 1-4 */
  p_levels in number := null,     /* ”ровень автоматического построени¤ */
  p_sharePercentNdsCriteria in number := null, /* ќграничение отбора Ќѕ в дерево по % Ќ?— */
  p_Cursor OUT SYS_REFCURSOR      /* */
)
as
v_lvl number(2) := 1;
v_year number(4) := p_Year;
v_period varchar(2 char);
v_maxLevel number(1) := 2;--p_levels;
v_levelCapacity number(3):= 100; -- техническое ограничение
v_min_vat_share number(5,2):= nvl(p_sharePercentNdsCriteria,0);
v_inn varchar (12 char) := trim(p_Inn);
v_byPurchase number := p_ByPurchase;
tree_relations_table_parent T$HRZ_TREE_RELATIONS;
tree_relations_table_children T$HRZ_TREE_RELATIONS;
begin

 log_call('TAXPAYER_RELATION',  'sono:'||trim(p_Inn)||', year:'||p_MaxContractors||', p_ByPurchase:'||p_ByPurchase||', p_Year:'||p_Year||', p_Qtr:'||p_Qtr||', p_levels:'||p_levels||', p_sharePercentNdsCriteria:'||p_sharePercentNdsCriteria);

 select decode(p_Qtr, 1 , '21', 2, '22', 3, '23', 4, '24', '00') into v_period from dual;

 select
    T$HRZ_TREE_RELATION(1,null,null,null,d.inn,1,d.decl_sign,d.compensation_amnt,d.ch8_nds,d.ch9_nds,d.gap_discrep_amnt,null,null,null,d.decl_type_code,d.soun_code,d.soun_name,d.processingstage)
    bulk collect into tree_relations_table_parent
 from TMP_HRZ_DECL_INFO d
 where d.inn = v_inn
       and d.fiscal_year = v_year
       and d.tax_period = v_period
       and d.is_active = 1;

 v_lvl := v_lvl + 1;

 while v_lvl <= v_maxLevel loop
  if v_byPurchase <> 0 then -- РѕС‚ РїРѕРєСѓРїР°С‚РµР»В¤
   begin
    select
      T$HRZ_TREE_RELATION(0
     ,data.inn_1
     ,parent_np.vat_deduction_total
     ,parent_np.vat_calculation_total
     ,data.inn_2
     ,v_lvl
     ,data.declaration_type
     ,data.total_nds
     ,data.deduction_nds
     ,data.calc_nds
     ,data.gap_discrep_amnt
     ,data.gap_amount
     ,data.nba
     ,data.nsa
     ,data.decl_type_code
     ,data.soun_code
     ,data.soun_name
     ,data.processingstage)
    bulk collect into tree_relations_table_children
    from
     HRZ_RELATIONS data
     join table(tree_relations_table_parent) parent_np
      on   parent_np.lvl = v_lvl - 1
       and data.inn_1 = parent_np.inn
       and data.year = v_year
       and data.period = v_period
       and data.nba_order <= v_levelCapacity
     where data.nba > 0 and
         (v_min_vat_share = 0
      or data.nba_order <= p_MaxContractors
      or 100 * data.nba > v_min_vat_share * parent_np.vat_deduction_total);
   end;
  else -- РѕС‚ РїСЂРѕРґР°РІС†Р°
   begin
    select
      T$HRZ_TREE_RELATION(0
     ,data.inn_1
     ,parent_np.vat_deduction_total
     ,parent_np.vat_calculation_total
     ,data.inn_2
     ,v_lvl
     ,data.declaration_type
     ,data.total_nds
     ,data.deduction_nds
     ,data.calc_nds
     ,data.gap_discrep_amnt
     ,data.gap_amount
     ,data.nba
     ,data.nsa
     ,data.decl_type_code
     ,data.soun_code
     ,data.soun_name
     ,data.processingstage)
    bulk collect into tree_relations_table_children
    from
     HRZ_RELATIONS data
     join table(tree_relations_table_parent) parent_np
      on   parent_np.lvl = v_lvl - 1
       and data.inn_1 = parent_np.inn
       and data.year = v_year
       and data.period = v_period
       and data.nsa_order <= v_levelCapacity
     where data.nsa > 0 and (
         v_min_vat_share = 0
      or data.nsa_order <= p_MaxContractors
      or 100 * data.nsa > v_min_vat_share * parent_np.vat_calculation_total);
   end;
  end if;
  v_lvl := v_lvl + 1;
 end loop;

  open p_Cursor for
  select
    tree.id
   ,tree.parent_inn
   ,tree.lvl
   ,tree.inn
   ,egrn.np_name as name
   ,egrn.kpp
   ,tree.declaration_type
   ,nvl(tree.vat_calculation_total,0) as calc_nds
   ,summary.buyers_cnt
   ,nvl(tree.vat_deduction_total,0) as deduction_nds
   ,summary.sellers_cnt
   ,cast((case
     when nvl(tree.vat_calculation_total,0) = 0
     then 0
     else 100 * nvl(tree.vat_deduction_total,0) / tree.vat_calculation_total
    end) as number(19,2)) as share_nds_amnt
   ,nvl(sur.sign_code, 4) as sur
   ,cast((case when v_byPurchase <> 0 then nvl(rev.nsa,0) else nvl(tree.vat_calculation,0) end) as number(19,2)) as mapped_amount
   ,cast((case when v_byPurchase <> 0 then nvl(tree.vat_deduction,0) else nvl(rev.nba,0) end) as number(19,2)) as not_mapped_amount
   ,cast((nvl(case when v_byPurchase <> 0 then tree.gap_discrepancy else rev.gap_amount end,0)) as number(19,2)) as discrepancy_amnt
   ,cast((case
     when v_byPurchase <> 0 and nvl(tree.parent_vat_deduction_total,0) <> 0 then 100*nvl(tree.vat_deduction,0)/tree.parent_vat_deduction_total
     when v_byPurchase = 0 and nvl(tree.parent_vat_calculation_total,0) <> 0 then 100*nvl(tree.vat_calculation,0)/tree.parent_vat_calculation_total
     else 0.00
   end) as number(19,2)) as vat_share
   ,nvl(tree.vat_total,0) as vat_total
   ,nvl(tree.gap_discrepancy_total,0) as gap_discrepancy_total
   ,1 as sell_amnt
   ,2 as buy_amnt
   ,3 as total_purchase_amnt
   ,4 as total_sales_amnt
   ,tree.decl_type_code
   ,tree.soun_code
   ,tree.soun_name
   ,tree.processingstage
  from
  (select * from table(tree_relations_table_parent)
  union
  select * from table(tree_relations_table_children)) tree
   left join HRZ_RELATIONS_TOTAL summary
    on   summary.year = v_year
     and summary.period = v_period
     and summary.inn = tree.inn
   left join HRZ_RELATIONS rev
    on   rev.year = v_year
     and rev.period = v_period
     and rev.inn_1 = tree.inn
     and rev.inn_2 = tree.parent_inn
   left join MV$TAX_PAYER_NAME egrn on egrn.inn = tree.inn
   left join
    (select
      x.sign_code
     ,x.inn
     ,row_number() over (partition by x.inn order by x.update_date desc) as idx
    from EXT_SUR x
    where x.is_actual = 1 and x.fiscal_year = v_year and x.fiscal_period = v_period
     ) sur on sur.inn = tree.inn and sur.idx = 1
  order by tree.lvl, case when v_byPurchase <> 0 then tree.vat_deduction else tree.vat_calculation end desc;
end;
end;
/
