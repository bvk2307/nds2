﻿--*************************************
--Синхронизация данных по макроотчетам*
--*************************************
declare
v_cnt number;
procedure execute_sql(p_sql varchar2)
 as
begin
  execute immediate p_sql;
  exception when others then
    dbms_output.put_line('error occured. ErrCode:'||sqlcode||', ErrMsg:'||substr(sqlerrm, 1, 512)||', sql:'||p_sql);
end;
begin
    
--Создадим талицу логов-отладки
execute_sql('drop NDS2_MRR_USER.HRZ_LOG');
execute_sql('create table NDS2_MRR_USER.HRZ_LOG(id NUMBER, ev_name VARCHAR2(128),params VARCHAR2(512),dt DATE)');
 
--*****************************************************
--Удаляем таблицы, которые будем синхронизировать заново
--*****************************************************
execute_sql('drop table NDS2_MRR_USER.TMP_HRZ_DECL_INFO');
execute_sql('drop table NDS2_MRR_USER.NAVIGATOR_AGGREGATE');
execute_sql('drop table NDS2_MRR_USER.MV$TAX_PAYER_NAME');
execute_sql('drop table NDS2_MRR_USER.EXT_SUR');
execute_sql('drop table NDS2_MRR_USER.EXT_SONO');
execute_sql('drop table NDS2_MRR_USER.EXT_SSRF');
execute_sql('drop table NDS2_MRR_USER.V$SONO');
execute_sql('drop table NDS2_MRR_USER.V$SSRF');
execute_sql('drop table NDS2_MRR_USER.SOV_MATCH_RESULT_SUMMARY');
execute_sql('drop table NDS2_MRR_USER.FEDERAL_DISTRICT');
execute_sql('drop table NDS2_MRR_USER.FEDERAL_DISTRICT_REGION');
execute_sql('drop table NDS2_MRR_USER.REPORT_DECLARATION_RAW');
execute_sql('drop table NDS2_MRR_USER.HRZ_RELATIONS_HIV06');
execute_sql('drop table NDS2_MRR_USER.HRZ_RELATIONS_TOTAL_HIV06');
execute_sql('drop table NDS2_MRR_USER.HRZ_RELATIONS');
execute_sql('drop table NDS2_MRR_USER.HRZ_RELATIONS_TOTAL');
execute_sql('drop table NDS2_MRR_USER.IFNS_AGGREGATE');
execute_sql('drop table NDS2_MRR_USER.DICT_SUR');

end;
/

create table NDS2_MRR_USER.NAVIGATOR_AGGREGATE pctfree 0 compress as select * from NAVIGATOR_AGGREGATE@NDS2_MRR_PROM;
create table  NDS2_MRR_USER.MV$TAX_PAYER_NAME  pctfree 0 compress as 
select
inn,
tp_type,
kpp,
address,
cast(
regexp_replace(
            regexp_replace(
               regexp_replace(
                  regexp_replace(
                        regexp_replace(
                            regexp_replace(upper(np_name),'ПУБЛИЧНОЕ АКЦИОНЕРНОЕ ОБЩЕСТВО','ПАО'),
                            'ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ', 'ООО'),
                        'ЗАКРЫТОЕ АКЦИОНЕРНОЕ ОБЩЕСТВО', 'ЗАО'),
                  'ОТКРЫТОЕ АКЦИОНЕРНОЕ ОБЩЕСТВО', 'ОАО'),
               'ИНДИВИДУАЛЬНОЕ ЧАСТНОЕ ПРЕДПРИЯТИЕ', 'ИЧП'),
             'АКЦИОНЕРНОЕ ОБЩЕСТВО', 'АО') as varchar2(1000)
             ) as np_name
from NDS2_MRR_USER.MV$TAX_PAYER_NAME@NDS2_MRR_PROM;
create table NDS2_MRR_USER.EXT_SUR pctfree 0 compress as select * from EXT_SUR@NDS2_MRR_PROM;
create table NDS2_MRR_USER.EXT_SONO pctfree 0 compress as select * from EXT_SONO@NDS2_MRR_PROM;
create table NDS2_MRR_USER.EXT_SSRF pctfree 0 compress as select * from EXT_SSRF@NDS2_MRR_PROM;
create table NDS2_MRR_USER.V$SONO pctfree 0 compress as select * from V$SONO@NDS2_MRR_PROM;
create table NDS2_MRR_USER.V$SSRF pctfree 0 compress as select * from V$SSRF@NDS2_MRR_PROM;
create table NDS2_MRR_USER.FEDERAL_DISTRICT pctfree 0 compress as select * from FEDERAL_DISTRICT@NDS2_MRR_PROM;
create table NDS2_MRR_USER.FEDERAL_DISTRICT_REGION pctfree 0 compress as select * from FEDERAL_DISTRICT_REGION@NDS2_MRR_PROM;
create table NDS2_MRR_USER.TMP_HRZ_DECL_INFO pctfree 0 compress as select * from TMP_HRZ_DECL_INFO@NDS2_MRR_PROM;
create table NDS2_MRR_USER.SOV_MATCH_RESULT_SUMMARY pctfree 0 compress as select * from SOV_MATCH_RESULT_SUMMARY@NDS2_MRR_PROM;
create table NDS2_MRR_USER.REPORT_DECLARATION_RAW pctfree 0 compress as select * from REPORT_DECLARATION_RAW@NDS2_MRR_PROM;
create table NDS2_MRR_USER.HRZ_RELATIONS pctfree 0 compress as select * from HRZ_RELATIONS@NDS2_MRR_PROM;
create table NDS2_MRR_USER.HRZ_RELATIONS_TOTAL pctfree 0 compress as select * from HRZ_RELATIONS_TOTAL@NDS2_MRR_PROM;
create table NDS2_MRR_USER.IFNS_AGGREGATE pctfree 0 compress as select * from IFNS_AGGREGATE@NDS2_MRR_PROM;
create table NDS2_MRR_USER.DICT_SUR pctfree 0 compress as select * from DICT_SUR@NDS2_MRR_PROM;

CREATE INDEX NDS2_MRR_USER.IDX_HRZ_DECL_INFO ON NDS2_MRR_USER.TMP_HRZ_DECL_INFO (INN, FISCAL_YEAR, TAX_PERIOD, IS_ACTIVE) TABLESPACE NDS2_IDX;
CREATE INDEX NDS2_MRR_USER.IDX_MVTPN_INN ON NDS2_MRR_USER.MV$TAX_PAYER_NAME (INN) TABLESPACE NDS2_IDX;
CREATE INDEX NDS2_MRR_USER.IX_RELATIONS_TOTAL ON NDS2_MRR_USER.HRZ_RELATIONS_TOTAL (YEAR, PERIOD, INN) TABLESPACE NDS2_IDX;
CREATE INDEX NDS2_MRR_USER.IX_RELATIONS_BA ON NDS2_MRR_USER.HRZ_RELATIONS (YEAR, PERIOD, INN_1, NBA_ORDER) TABLESPACE NDS2_IDX;
CREATE INDEX NDS2_MRR_USER.IX_RELATIONS_SA ON NDS2_MRR_USER.HRZ_RELATIONS (YEAR, PERIOD, INN_1, NSA_ORDER) TABLESPACE NDS2_IDX;
CREATE INDEX NDS2_MRR_USER.IX_RELATIONS_REV ON NDS2_MRR_USER.HRZ_RELATIONS (YEAR, PERIOD, INN_1, INN_2) TABLESPACE NDS2_IDX;
CREATE INDEX NDS2_MRR_USER.IDX_FEDDISTREG_REG ON NDS2_MRR_USER.FEDERAL_DISTRICT_REGION (REGION_CODE) TABLESPACE NDS2_IDX;
CREATE INDEX NDS2_MRR_USER.IDX_FEDDISTRICT_DISTR ON NDS2_MRR_USER.FEDERAL_DISTRICT (DISTRICT_ID) TABLESPACE NDS2_IDX;
CREATE INDEX NDS2_MRR_USER.IDX_SUR_INN_Y_P ON NDS2_MRR_USER.EXT_SUR (INN, FISCAL_YEAR, FISCAL_PERIOD) TABLESPACE NDS2_IDX;
CREATE INDEX NDS2_MRR_USER.IDX_SSRF_CODE ON NDS2_MRR_USER.EXT_SSRF (S_CODE) TABLESPACE NDS2_IDX;
CREATE INDEX NDS2_MRR_USER.IDX_EXT_SONO_CODE ON NDS2_MRR_USER.EXT_SONO (S_CODE) TABLESPACE NDS2_IDX;

begin
  dbms_stats.gather_schema_stats(ownname => 'NDS2_MRR_USER');
end;
/
