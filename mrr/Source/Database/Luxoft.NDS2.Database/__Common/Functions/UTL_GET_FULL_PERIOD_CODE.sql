﻿create or replace function NDS2_MRR_USER.UTL_GET_FULL_PERIOD_CODE(p_year varchar2, p_period varchar2) return varchar2
as
  v_ret varchar2(6);
begin
  v_ret := p_year ||
			 case
			   when p_period in ('01', '02', '03', '21', '51', '71', '72', '73') then '21'
			   when p_period in ('04', '05', '06', '22', '54', '74', '75', '76') then '22'
			   when p_period in ('07', '08', '09', '23', '55', '77', '78', '79') then '23'
			   when p_period in ('10', '11', '12', '24', '56', '80', '81', '82') then '24'
			 end;

  return v_ret;
end;
/
