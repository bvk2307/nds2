﻿CREATE OR REPLACE FUNCTION NDS2_MRR_USER.F$FISCAL_YEAR(P_DATE DATE) RETURN NUMBER
  AS
    v_year number;
    v_day number;
    v_month number;
  BEGIN
    v_year := extract(year from p_date);
    v_month := extract (month from p_date) - 3;
    v_day := extract(day from p_date);
    if (v_day < 26) then
      v_month := v_month - 1;
    end if;
    if (v_month < 1) then
      v_year := v_year - 1;
      v_month := 12 + v_month;
    end if;
    return v_year;
  END;
/
