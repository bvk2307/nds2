﻿create or replace function NDS2_MRR_USER.UTL_GET_DECL_SIGN_NAME(p_decl_sign varchar2) return varchar2
as
  v_ret varchar2(23);
begin
  v_ret := case trim(p_decl_sign)
             when '1' then 'К уплате'
             when '2' then 'К возмещению'
             when '3' then 'Нулевая'
             else null
           end;

  return v_ret;
end;
/
