﻿CREATE OR REPLACE FUNCTION NDS2_MRR_USER.UTL_GET_LAST_REG_NUM(p_reg_num number) return number
as
  v_result number(20);
begin
  select t.reg_num_new
  into v_result
  from
  (
    select
      seod_decl_new.decl_reg_num as reg_num_new,
      row_number() over(partition by seod_decl_new.nds2_id order by seod_decl_new.correction_number desc) as rn
    from nds2_seod.seod_declaration seod_decl_new
    inner join nds2_seod.seod_declaration seod_decl_old on seod_decl_old.nds2_id = seod_decl_new.nds2_id
    where seod_decl_old.decl_reg_num = p_reg_num
  ) t
  where t.rn = 1;
  
  return v_result;
end;
/