﻿CREATE OR REPLACE FUNCTION NDS2_MRR_USER.F$PARSE_OPERATION_CODE(P_CODE_MASK NUMBER) RETURN VARCHAR2 AS
  V_CODE VARCHAR2(1024 CHAR);
BEGIN
  V_CODE := '';
  for c in (select * from dict_operation_codes)
  loop
    if bitand(P_CODE_MASK, c.bit_code) = c.bit_code then
      if length(V_CODE) > 0 then V_CODE := V_CODE||'; '; end if;
      V_CODE := V_CODE||c.code;
    end if;
  end loop;
  return V_CODE;
END;
/
