﻿create or replace function NDS2_MRR_USER.UTL_ADD_WORK_DAYS(p_date date, p_days_count number) return date
as
  cnt number;
  dt date := trunc(p_date);
  v_exist number;
  v_increment number(1);
begin

  if p_days_count >= 0 then
     v_increment := 1;
  else
     v_increment := -1;
  end if;
  cnt := abs(p_days_count);

  while (cnt > 0) loop
    dt := dt + v_increment;
    cnt := cnt - 1;
    select count(1) into v_exist from redday where red_date = dt;
    while (v_exist > 0) loop
      dt := dt + v_increment;
      select count(1) into v_exist from redday where red_date = dt;
    end loop;
  end loop;
  return dt;
end;
/
