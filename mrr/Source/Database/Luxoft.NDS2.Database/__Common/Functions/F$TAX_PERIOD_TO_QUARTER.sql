﻿CREATE OR REPLACE FUNCTION NDS2_MRR_USER.F$TAX_PERIOD_TO_QUARTER (P_PERIOD NUMBER) RETURN NUMBER
AS
  v_period number;
BEGIN
    v_period := case
       when P_PERIOD in ('01', '02', '03', '21', '51', '71', '72', '73') then 1
       when P_PERIOD in ('04', '05', '06', '22', '54', '74', '75', '76') then 2
       when P_PERIOD in ('07', '08', '09', '23', '55', '77', '78', '79') then 3
       when P_PERIOD in ('10', '11', '12', '24', '56', '80', '81', '82') then 4
     end;
  return v_period;
END;
/
