﻿create or replace function NDS2_MRR_USER.F$GET_SHORT_TAXPAYER_NAME(p_name_full varchar2) return varchar2
as
  v_ret varchar2(2000);
begin

  v_ret := trim(regexp_replace(p_name_full,'( ){2,}','\1'));

  v_ret := upper(v_ret);
  
  v_ret := regexp_replace(v_ret,'НЕПУБЛИЧНОЕ АКЦИОНЕРНОЕ ОБЩЕСТВО','НАО');
  v_ret := regexp_replace(v_ret,'ПУБЛИЧНОЕ АКЦИОНЕРНОЕ ОБЩЕСТВО','ПАО');
  v_ret := regexp_replace(v_ret,'ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ','ООО');
  v_ret := regexp_replace(v_ret,'ЗАКРЫТОЕ АКЦИОНЕРНОЕ ОБЩЕСТВО','ЗАО');
  v_ret := regexp_replace(v_ret,'ОТКРЫТОЕ АКЦИОНЕРНОЕ ОБЩЕСТВО','ОАО');
  v_ret := regexp_replace(v_ret,'ИНДИВИДУАЛЬНОЕ ЧАСТНОЕ ПРЕДПРИЯТИЕ','ИЧП');
  v_ret := regexp_replace(v_ret,'ИНДИВИДУАЛЬНЫЙ ПРЕДПРИНИМАТЕЛЬ','ИП');
  v_ret := regexp_replace(v_ret,'АКЦИОНЕРНОЕ ОБЩЕСТВО','АО');

  return v_ret;
end;
/
