﻿prompt Loading CFG_INSPECTION_GROUP_N...
truncate table NDS2_MRR_USER.CFG_INSPECTION_GROUP_N;
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (2000, 'МРИ ФНС России № 14 по Республике Дагестан', '0554', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (2010, 'МРИ ФНС России № 14 по Кировской области', '4350', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (2020, 'МРИ ФНС России № 15 по Нижегородской области', '5275', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (2030, 'МРИ ФНС России № 7 по Томской области', '7031', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (2040, 'МРИ ФНС России №16 по Краснодарскому краю', '2375', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (300, 'Межрайонная инспекция Федеральной налоговой службы №5 по Архангельской области и Ненецкому автономному округу', '2918', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (301, 'Межрайонная инспекция Федеральной налоговой службы №6 по Архангельской области и Ненецкому автономному округу', '2920', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (302, 'Межрайонная инспекция Федеральной налоговой службы по крупнейшим налогоплательщикам по Архангельской области и Ненецкому автономному округу', '2931', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (303, 'Межрайонная инспекция Федеральной налоговой службы № 9 по Архангельской области и Ненецкому автономному округу', '2932', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (304, 'Межрайонная инспекция Федеральной налоговой службы №4 по Архангельской области и Ненецкому автономному округу', '2983', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (305, 'Управление Федеральной налоговой службы по Астраханской области', '3000', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (306, 'Инспекция Федеральной налоговой службы по Кировскому району г.Астрахани', '3015', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (307, 'Межрайонная инспекция Федеральной налоговой службы № 1 по Астраханской области', '3019', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (308, 'Межрайонная инспекция Федеральной налоговой службы № 4 по Астраханской области', '3022', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (309, 'Межрайонная инспекция Федеральной налоговой службы № 5 по Астраханской области', '3023', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (310, 'Межрайонная инспекция Федеральной налоговой службы № 6 по Астраханской области', '3025', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (311, 'Управление Федеральной налоговой службы по Белгородской области', '3100', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (312, 'Межрайонная инспекция Федеральной налоговой службы №6 по Белгородской области', '3114', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (313, 'Межрайонная инспекция Федеральной налоговой службы №5 по Белгородской области', '3116', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (314, 'Межрайонная инспекция Федеральной налоговой службы №7 по Белгородской области', '3120', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (315, 'Межрайонная инспекция Федеральной налоговой службы №1 по Белгородской области', '3122', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (316, 'Инспекция Федеральной налоговой службы по г.Белгороду', '3123', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (317, 'Межрайонная инспекция Федеральной налоговой службы №3 по Белгородской области', '3126', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (318, 'Межрайонная инспекция Федеральной налоговой службы №8 по Белгородской области', '3127', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (319, 'Межрайонная инспекция Федеральной налоговой службы №4 по Белгородской области', '3128', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (320, 'Межрайонная инспекция Федеральной налоговой службы №2 по Белгородской области', '3130', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (321, 'Управление Федеральной налоговой службы по Брянской области', '3200', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (322, 'Межрайонная инспекция Федеральной налоговой службы № 1 по Брянской области', '3241', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (323, 'Межрайонная инспекция Федеральной налоговой службы № 5 по Брянской области', '3245', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (324, 'Межрайонная инспекция Федеральной налоговой службы № 7 по Брянской области', '3252', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (325, 'Межрайонная инспекция Федеральной налоговой службы № 8 по Брянской области', '3253', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (326, 'Межрайонная инспекция Федеральной налоговой службы № 10 по Брянской области', '3256', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (327, 'Инспекция Федеральной налоговой службы по г. Брянску', '3257', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (328, 'Управление Федеральной налоговой службы по Владимирской области', '3300', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (329, 'Межрайонная инспекция Федеральной налоговой службы по крупнейшим налогоплательщикам по Владимирской области', '3302', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (330, 'Межрайонная инспекция Федеральной налоговой службы № 1 по Владимирской области', '3304', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (331, 'Межрайонная инспекция Федеральной налоговой службы № 11 по Владимирской области', '3316', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (332, 'Межрайонная инспекция Федеральной налоговой службы № 3 по Владимирской области', '3326', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (333, 'Межрайонная инспекция Федеральной налоговой службы № 12 по Владимирской области', '3327', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (334, 'Инспекция Федеральной налоговой службы по Октябрьскому району г.Владимира', '3328', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (335, 'Межрайонная инспекция Федеральной налоговой службы № 2 по Владимирской области', '3332', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (336, 'Межрайонная инспекция Федеральной налоговой службы № 4 по Владимирской области', '3334', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (337, 'Межрайонная инспекция Федеральной налоговой службы № 9 по Владимирской области', '3339', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (338, 'Межрайонная инспекция Федеральной налоговой службы № 10 по Владимирской области', '3340', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (339, 'Управление Федеральной налоговой службы по Волгоградской области', '3400', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (340, 'Инспекция Федеральной налоговой службы по г.Волжскому Волгоградской области', '3435', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (341, 'Инспекция Федеральной налоговой службы по Дзержинскому району г.Волгограда', '3443', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (342, 'Инспекция Федеральной налоговой службы по Центральному району г.Волгограда', '3444', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (343, 'Межрайонная инспекция Федеральной налоговой службы №2 по Волгоградской области', '3452', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (344, 'Межрайонная инспекция Федеральной налоговой службы №3 по Волгоградской области', '3453', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (345, 'Межрайонная инспекция Федеральной налоговой службы №4 по Волгоградской области', '3454', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (346, 'Межрайонная инспекция Федеральной налоговой службы №5 по Волгоградской области', '3455', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (347, 'Межрайонная инспекция Федеральной налоговой службы №6 по Волгоградской области', '3456', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (348, 'Межрайонная инспекция Федеральной налоговой службы №7 по Волгоградской области', '3457', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (349, 'Межрайонная инспекция Федеральной налоговой службы №8 по Волгоградской области', '3458', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (350, 'Межрайонная инспекция Федеральной налоговой службы №9 по Волгоградской области', '3459', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (351, 'Межрайонная инспекция Федеральной налоговой службы №10 по Волгоградской области', '3460', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (352, 'Межрайонная инспекция Федеральной налоговой службы №11 по Волгоградской области', '3461', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (353, 'Управление Федеральной налоговой службы по Вологодской области', '3500', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (354, 'Межрайонная инспекция Федеральной налоговой службы № 11 по Вологодской области', '3525', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (355, 'Межрайонная инспекция Федеральной налоговой службы № 12 по Вологодской области', '3528', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (356, 'Межрайонная инспекция Федеральной налоговой службы № 1 по Вологодской области', '3529', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (357, 'Межрайонная инспекция Федеральной налоговой службы № 4 по Вологодской области', '3532', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (358, 'Межрайонная инспекция Федеральной налоговой службы № 5 по Вологодской области', '3533', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (359, 'Межрайонная инспекция Федеральной налоговой службы № 7 по Вологодской области', '3535', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (360, 'Межрайонная инспекция Федеральной налоговой службы № 8 по Вологодской области', '3536', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (361, 'Межрайонная инспекция Федеральной налоговой службы № 9 по Вологодской области', '3537', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (362, 'Межрайонная инспекция Федеральной налоговой службы № 10 по Вологодской области', '3538', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (363, 'Межрайонная инспекция Федеральной налоговой службы по крупнейшим налогоплательщикам по Вологодской области', '3539', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (364, 'Управление Федеральной налоговой службы по Воронежской области', '3600', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (365, 'Межрайонная инспекция Федеральной налоговой службы № 5 по Воронежской области', '3601', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (366, 'Межрайонная инспекция Федеральной налоговой службы № 3 по Воронежской области', '3604', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (367, 'Межрайонная инспекция Федеральной налоговой службы № 9 по Воронежской области', '3610', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (368, 'Межрайонная инспекция Федеральной налоговой службы № 6 по Воронежской области', '3620', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (369, 'Межрайонная инспекция Федеральной налоговой службы № 4 по Воронежской области', '3627', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (370, 'Межрайонная инспекция Федеральной налоговой службы № 8 по Воронежской области', '3628', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (371, 'Межрайонная инспекция Федеральной налоговой службы № 2 по Воронежской области', '3629', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (372, 'Межрайонная инспекция Федеральной налоговой службы № 14 по Воронежской области', '3652', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (373, 'Межрайонная инспекция Федеральной налоговой службы № 13 по Воронежской области', '3661', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (374, 'Инспекция Федеральной налоговой службы по Коминтерновскому району г. Воронежа', '3662', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (375, 'Инспекция Федеральной налоговой службы по Левобережному району г. Воронежа', '3663', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (376, 'Инспекция Федеральной налоговой службы по Ленинскому району г. Воронежа', '3664', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (377, 'Инспекция Федеральной налоговой службы по Советскому району г. Воронежа', '3665', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (378, 'Межрайонная инспекция Федеральной налоговой службы № 1 по Воронежской области', '3666', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (379, 'Межрайонная инспекция Федеральной налоговой службы по крупнейшим налогоплательщикам по Воронежской области', '3667', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (380, 'Межрайонная инспекция Федеральной налоговой службы № 12 по Воронежской области', '3668', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (381, 'Управление Федеральной налоговой службы по Ивановской области', '3700', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (382, 'Межрайонная инспекция Федеральной налоговой службы № 1 по Ивановской области', '3701', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (383, 'Инспекция Федеральной налоговой службы по г.Иваново', '3702', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (384, 'Межрайонная инспекция Федеральной налоговой службы № 5 по Ивановской области', '3703', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (385, 'Межрайонная инспекция Федеральной налоговой службы № 2 по Ивановской области', '3704', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (386, 'Межрайонная инспекция Федеральной налоговой службы № 4 по Ивановской области', '3705', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (387, 'Межрайонная инспекция Федеральной налоговой службы № 3 по Ивановской области', '3706', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (388, 'Межрайонная инспекция Федеральной налоговой службы № 6 по Ивановской области', '3711', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (389, 'Межрайонная инспекция Федеральной налоговой службы № 7 по Ивановской области', '3720', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (390, 'Управление Федеральной налоговой службы по Иркутской области', '3800', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (391, 'Инспекция Федеральной налоговой службы по г. Ангарску Иркутской области', '3801', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (392, 'Межрайонная инспекция Федеральной налоговой службы № 3 по Иркутской области', '3802', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (393, 'Инспекция Федеральной налоговой службы по Центральному округу г. Братска Иркутской области', '3804', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (394, 'Межрайонная инспекция Федеральной налоговой службы № 15 по Иркутской области', '3805', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (395, 'Инспекция Федеральной налоговой службы по Правобережному округу г. Иркутска', '3808', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (396, 'Межрайонная инспекция Федеральной налоговой службы № 19 по Иркутской области', '3810', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (397, 'Инспекция Федеральной налоговой службы по Октябрьскому округу г. Иркутска', '3811', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (398, 'Инспекция Федеральной налоговой службы по Свердловскому округу г. Иркутска', '3812', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (399, 'Межрайонная инспекция Федеральной налоговой службы № 14 по Иркутской области', '3814', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (400, 'Межрайонная инспекция Федеральной налоговой службы № 6 по Иркутской области', '3816', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (401, 'Межрайонная инспекция Федеральной налоговой службы № 9 по Иркутской области', '3817', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (402, 'Межрайонная инспекция Федеральной налоговой службы № 13 по Иркутской области', '3818', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (403, 'Межрайонная инспекция Федеральной налоговой службы № 12 по Иркутской области', '3827', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (404, 'Межрайонная инспекция Федеральной налоговой службы № 16 по Иркутской области', '3849', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (405, 'Межрайонная инспекция Федеральной налоговой службы № 17 по Иркутской области', '3850', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (406, 'Межрайонная инспекция Федеральной налоговой службы № 18 по Иркутской области', '3851', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (407, 'Управление Федеральной налоговой службы по Калининградской области', '3900', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (408, 'Межрайонная инспекция Федеральной налоговой службы №8 по городу Калининграду', '3905', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (409, 'Межрайонная инспекция Федеральной налоговой службы №9 по городу Калининграду', '3906', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (410, 'Межрайонная инспекция Федеральной налоговой службы №2 по Калининградской области', '3914', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (411, 'Межрайонная инспекция Федеральной налоговой службы №10 по Калининградской области', '3917', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (412, 'Межрайонная инспекция Федеральной налоговой службы по крупнейшим налогоплательщикам по Калининградской области', '3925', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (413, 'Межрайонная инспекция Федеральной налоговой службы № 1 по Калининградской области', '3926', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (414, 'Управление Федеральной налоговой службы по Калужской области', '4000', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (415, 'Межрайонная инспекция Федеральной налоговой службы № 1 по Калужской области', '4001', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (416, 'Межрайонная инспекция Федеральной налоговой службы № 2 по Калужской области', '4004', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (417, 'Межрайонная инспекция Федеральной налоговой службы № 3 по Калужской области', '4011', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (418, 'Межрайонная инспекция Федеральной налоговой службы № 4 по Калужской области', '4023', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (419, 'Межрайонная инспекция Федеральной налоговой службы № 5 по Калужской области', '4024', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (420, 'Межрайонная инспекция Федеральной налоговой службы № 6 по Калужской области', '4025', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (421, 'Инспекция Федеральной налоговой службы по Ленинскому округу г. Калуги', '4027', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (422, 'Инспекция Федеральной налоговой службы по Московскому округу г. Калуги', '4028', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (423, 'Межрайонная инспекция Федеральной налоговой службы № 7 по Калужской области', '4029', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (424, 'Управление Федеральной налоговой службы по Камчатскому краю', '4100', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (425, 'Инспекция Федеральной налоговой службы по г.Петропавловску-Камчатскому', '4101', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (426, 'Межрайонная инспекция Федеральной налоговой службы № 3 по Камчатскому краю', '4177', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (427, 'Управление Федеральной налоговой службы по Кемеровской области', '4200', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (428, 'Межрайонная инспекция Федеральной налоговой службы № 3 по Кемеровской области', '4202', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (429, 'Инспекция Федеральной налоговой службы по г. Кемерово', '4205', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (430, 'Межрайонная инспекция Федеральной налоговой службы № 2 по Кемеровской области', '4212', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (431, 'Межрайонная инспекция Федеральной налоговой службы № 1 по Кемеровской области', '4213', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (432, 'Межрайонная инспекция Федеральной налоговой службы № 8 по Кемеровской области', '4214', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (433, 'Межрайонная инспекция Федеральной налоговой службы по крупнейшим налогоплательщикам № 2 по Кемеровской области', '4216', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (434, 'Инспекция Федеральной налоговой службы по Центральному району г. Новокузнецка Кемеровской области', '4217', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (435, 'Межрайонная инспекция Федеральной налоговой службы № 5 по Кемеровской области', '4222', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (436, 'Межрайонная инспекция Федеральной налоговой службы № 11 по Кемеровской области', '4223', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (437, 'Межрайонная инспекция Федеральной налоговой службы № 7 по Кемеровской области', '4230', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (438, 'Межрайонная инспекция Федеральной налоговой службы № 9 по Кемеровской области', '4246', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (439, 'Межрайонная инспекция Федеральной налоговой службы по крупнейшим налогоплательщикам № 1 по Кемеровской области', '4249', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (440, 'Межрайонная инспекция Федеральной налоговой службы № 12 по Кемеровской области', '4250', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (441, 'Межрайонная инспекция Федеральной налоговой службы № 13 по Кемеровской области', '4252', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (442, 'Межрайонная инспекция Федеральной налоговой службы № 4 по Кемеровской области', '4253', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (443, 'Управление Федеральной налоговой службы по Кировской области', '4300', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (444, 'Межрайонная инспекция Федеральной налоговой службы №2 по Кировской области', '4303', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (445, 'Межрайонная инспекция Федеральной налоговой службы №4 по Кировской области', '4307', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (446, 'Межрайонная инспекция Федеральной налоговой службы №7 по Кировской области', '4312', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (447, 'Межрайонная инспекция Федеральной налоговой службы №8 по Кировской области', '4313', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (448, 'Межрайонная инспекция Федеральной налоговой службы №1 по Кировской области', '4316', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (449, 'Межрайонная инспекция Федеральной налоговой службы №10 по Кировской области', '4321', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (450, 'Межрайонная инспекция Федеральной налоговой службы №3 по Кировской области', '4322', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (451, 'Межрайонная инспекция Федеральной налоговой службы №13 по Кировской области', '4329', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (452, 'Межрайонная инспекция Федеральной налоговой службы №11 по Кировской области', '4330', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (453, 'Межрайонная инспекция Федеральной налоговой службы №12 по Кировской области', '4334', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (454, 'Межрайонная инспекция Федеральной налоговой службы №5 по Кировской области', '4339', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (455, 'Инспекция Федеральной налоговой службы по г.Кирову', '4345', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (456, 'Управление Федеральной налоговой службы по Костромской области', '4400', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (457, 'Инспекция Федеральной налоговой службы по г. Костроме', '4401', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (458, 'Межрайонная инспекция Федеральной налоговой службы № 2 по Костромской области', '4433', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (459, 'Межрайонная инспекция Федеральной налоговой службы № 3 по Костромской области', '4434', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (460, 'Межрайонная инспекция Федеральной налоговой службы № 6 по Костромской области', '4436', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (461, 'Межрайонная инспекция Федеральной налоговой службы № 7 по Костромской области', '4437', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (462, 'Управление Федеральной налоговой службы по Курганской области', '4500', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (463, 'Инспекция Федеральной налоговой службы по г.Кургану', '4501', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (464, 'Межрайонная инспекция Федеральной налоговой службы №1 по Курганской области', '4502', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (465, 'Межрайонная инспекция Федеральной налоговой службы №2 по Курганской области', '4506', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (466, 'Межрайонная инспекция Федеральной налоговой службы №3 по Курганской области', '4508', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (467, 'Межрайонная инспекция Федеральной налоговой службы №7 по Курганской области', '4510', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (468, 'Межрайонная инспекция Федеральной налоговой службы №5 по Курганской области', '4512', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (469, 'Межрайонная инспекция Федеральной налоговой службы №6 по Курганской области', '4524', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (470, 'Межрайонная инспекция Федеральной налоговой службы №4 по Курганской области', '4526', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (471, 'Управление Федеральной налоговой службы по Курской области', '4600', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (516, 'Инспекция Федеральной налоговой службы по г.Домодедово Московской области', '5009', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (517, 'Межрайонная инспекция Федеральной налоговой службы №12 по Московской области', '5010', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (518, 'Инспекция Федеральной налоговой службы по г.Егорьевску Московской области', '5011', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (519, 'Межрайонная инспекция Федеральной налоговой службы №20 по Московской области', '5012', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (520, 'Инспекция Федеральной налоговой службы по г.Истре Московской области', '5017', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (521, 'Межрайонная инспекция Федеральной налоговой службы №2 по Московской области', '5018', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (522, 'Межрайонная инспекция Федеральной налоговой службы №18 по Московской области', '5019', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (523, 'Инспекция Федеральной налоговой службы по г.Клину Московской области', '5020', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (524, 'Межрайонная инспекция Федеральной налоговой службы №7 по Московской области', '5022', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (525, 'Инспекция Федеральной налоговой службы по г.Красногорску Московской области', '5024', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (526, 'Межрайонная инспекция Федеральной налоговой службы №17 по Московской области', '5027', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (527, 'Инспекция Федеральной налоговой службы по г.Мытищи Московской области', '5029', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (528, 'Инспекция Федеральной налоговой службы по г.Наро-Фоминску Московской области', '5030', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (529, 'Инспекция Федеральной налоговой службы по г.Ногинску Московской области', '5031', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (530, 'Межрайонная инспекция Федеральной налоговой службы №22 по Московской области', '5032', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (531, 'Межрайонная инспекция Федеральной налоговой службы №10 по Московской области', '5034', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (532, 'Инспекция Федеральной налоговой службы по г.Павловскому Посаду Московской области', '5035', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (533, 'Межрайонная инспекция Федеральной налоговой службы №3 по Московской области', '5038', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (534, 'Межрайонная инспекция Федеральной налоговой службы №1 по Московской области', '5040', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (535, 'Инспекция Федеральной налоговой службы по г.Сергиеву Посаду Московской области', '5042', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (536, 'Межрайонная инспекция Федеральной налоговой службы №11 по Московской области', '5043', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (537, 'Инспекция Федеральной налоговой службы по г.Солнечногорску Московской области', '5044', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (538, 'Инспекция Федеральной налоговой службы по г.Ступино Московской области', '5045', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (539, 'Межрайонная инспекция Федеральной налоговой службы №13 по Московской области', '5047', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (540, 'Инспекция Федеральной налоговой службы по г.Чехову Московской области', '5048', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (541, 'Межрайонная инспекция Федеральной налоговой службы №4 по Московской области', '5049', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (542, 'Межрайонная инспекция Федеральной налоговой службы №16 по Московской области', '5050', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (543, 'Инспекция Федеральной налоговой службы по г.Электростали Московской области', '5053', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (544, 'Межрайонная инспекция Федеральной налоговой службы №8 по Московской области', '5072', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (545, 'Межрайонная инспекция Федеральной налоговой службы №5 по Московской области', '5074', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (546, 'Межрайонная инспекция Федеральной налоговой службы №21 по Московской области', '5075', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (547, 'Межрайонная инспекция Федеральной налоговой службы по крупнейшим налогоплательщикам по Московской области', '5099', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (548, 'Управление Федеральной налоговой службы по Мурманской области', '5100', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (549, 'Межрайонная инспекция Федеральной налоговой службы № 1 по Мурманской области', '5102', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (550, 'Межрайонная инспекция Федеральной налоговой службы № 7 по Мурманской области', '5105', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (551, 'Инспекция Федеральной налоговой службы по г. Мончегорску Мурманской области', '5107', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (552, 'Межрайонная инспекция Федеральной налоговой службы № 5 по Мурманской области', '5108', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (553, 'Межрайонная инспекция Федеральной налоговой службы № 2 по Мурманской области', '5110', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (554, 'Межрайонная инспекция Федеральной налоговой службы № 8 по Мурманской области', '5118', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (555, 'Инспекция Федеральной налоговой службы по г. Мурманску', '5190', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (556, 'Межрайонная инспекция Федеральной налоговой службы по крупнейшим налогоплательщикам по Мурманской области', '5199', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (557, 'Управление Федеральной налоговой службы по Нижегородской области', '5200', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (558, 'Межрайонная инспекция Федеральной налоговой службы № 10 по Нижегородской области', '5222', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (472, 'Межрайонная инспекция Федеральной налоговой службы №5 по Курской области', '4611', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (473, 'Межрайонная инспекция Федеральной налоговой службы №2 по Курской области', '4613', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (474, 'Межрайонная инспекция Федеральной налоговой службы №9 по Курской области', '4614', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (475, 'Межрайонная инспекция Федеральной налоговой службы №7 по Курской области', '4619', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (476, 'Межрайонная инспекция Федеральной налоговой службы №1 по Курской области', '4620', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (477, 'Межрайонная инспекция Федеральной налоговой службы №4 по Курской области', '4623', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (478, 'Межрайонная инспекция Федеральной налоговой службы №8 по Курской области', '4628', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (479, 'Инспекция Федеральной налоговой службы по г.Курску', '4632', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (480, 'Межрайонная инспекция Федеральной налоговой службы №3 по Курской области', '4633', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (481, 'Управление Федеральной налоговой службы по Ленинградской области', '4700', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (482, 'Межрайонная инспекция Федеральной налоговой службы №5 по Ленинградской области', '4702', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (483, 'Инспекция Федеральной налоговой службы по Всеволожскому району Ленинградской области', '4703', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (484, 'Инспекция Федеральной налоговой службы по Выборгскому району Ленинградской области', '4704', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (485, 'Межрайонная инспекция Федеральной налоговой службы №7 по Ленинградской области', '4705', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (486, 'Межрайонная инспекция Федеральной налоговой службы №2 по Ленинградской области', '4706', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (487, 'Межрайонная инспекция Федеральной налоговой службы №3 по Ленинградской области', '4707', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (488, 'Инспекция Федеральной налоговой службы по Лужскому району Ленинградской области', '4710', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (489, 'Межрайонная инспекция Федеральной налоговой службы №4 по Ленинградской области', '4711', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (490, 'Инспекция Федеральной налоговой службы по Приозерскому району Ленинградской области', '4712', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (491, 'Межрайонная инспекция Федеральной налоговой службы №6 по Ленинградской области', '4715', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (492, 'Инспекция Федеральной налоговой службы по Тосненскому району Ленинградской области', '4716', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (493, 'Межрайонная инспекция Федеральной налоговой службы №8 по Ленинградской области', '4725', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (494, 'Инспекция Федеральной налоговой службы по г.Сосновый Бор Ленинградской области', '4726', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (495, 'Инспекция Федеральной налоговой службы по Киришскому району Ленинградской области', '4727', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (496, 'Управление Федеральной налоговой службы по Липецкой области', '4800', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (497, 'Межрайонная инспекция Федеральной налоговой службы № 1 по Липецкой области', '4802', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (498, 'Межрайонная инспекция Федеральной налоговой службы № 4 по Липецкой области', '4811', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (499, 'Межрайонная инспекция Федеральной налоговой службы № 5 по Липецкой области', '4813', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (500, 'Межрайонная инспекция Федеральной налоговой службы № 3 по Липецкой области', '4816', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (501, 'Межрайонная инспекция Федеральной налоговой службы по крупнейшим налогоплательщикам по Липецкой области', '4822', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (502, 'Инспекция Федеральной налоговой службы по Октябрьскому району г.Липецка', '4824', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (503, 'Инспекция Федеральной налоговой службы по Правобережному району г.Липецка', '4825', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (504, 'Межрайонная инспекция Федеральной налоговой службы № 6 по Липецкой области', '4827', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (505, 'Межрайонная инспекция Федеральной налоговой службы № 7 по Липецкой области', '4828', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (506, 'Управление Федеральной налоговой службы по Магаданской области', '4900', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (507, 'Межрайонная инспекция Федеральной налоговой службы №1 по Магаданской области', '4910', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (508, 'Межрайонная инспекция Федеральной налоговой службы №2 по Магаданской области', '4911', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (509, 'Межрайонная инспекция Федеральной налоговой службы №3 по Магаданской области', '4912', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (510, 'Управление Федеральной налоговой службы по Московской области', '5000', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (511, 'Инспекция Федеральной налоговой службы по г.Балашихе Московской области', '5001', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (512, 'Межрайонная инспекция Федеральной налоговой службы №14 по Московской области', '5003', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (513, 'Межрайонная инспекция Федеральной налоговой службы №19 по Московской области', '5004', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (514, 'Инспекция Федеральной налоговой службы по г.Воскресенску Московской области', '5005', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (515, 'Инспекция Федеральной налоговой службы по г.Дмитрову Московской области', '5007', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (1, 'Федеральная налоговая служба', '0000', 'Y');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (2, 'Управление Федеральной налоговой службы по Республике Адыгея', '0100', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (3, 'Межрайонная инспекция Федеральной налоговой службы №2 по Республике Адыгея', '0101', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (4, 'Межрайонная инспекция Федеральной налоговой службы №1 по Республике Адыгея', '0105', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (5, 'Межрайонная инспекция Федеральной налоговой службы №3 по Республике Адыгея', '0107', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (6, 'Управление Федеральной налоговой службы по Республике Башкортостан', '0200', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (7, 'Межрайонная инспекция Федеральной налоговой службы по крупнейшим налогоплательщикам по Республике Башкортостан', '0252', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (8, 'Межрайонная инспекция Федеральной налоговой службы № 20 по Республике Башкортостан', '0256', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (9, 'Межрайонная инспекция Федеральной налоговой службы № 25 по Республике Башкортостан', '0261', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (10, 'Межрайонная инспекция Федеральной налоговой службы № 29 по Республике Башкортостан', '0264', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (11, 'Межрайонная инспекция Федеральной налоговой службы № 37 по Республике Башкортостан', '0267', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (12, 'Межрайонная инспекция Федеральной налоговой службы № 3 по Республике Башкортостан', '0268', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (13, 'Межрайонная инспекция Федеральной налоговой службы № 27 по Республике Башкортостан', '0269', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (14, 'Межрайонная инспекция Федеральной налоговой службы № 30 по Республике Башкортостан', '0272', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (15, 'Межрайонная инспекция Федеральной налоговой службы № 31 по Республике Башкортостан', '0273', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (16, 'Межрайонная инспекция Федеральной налоговой службы № 40 по Республике Башкортостан', '0274', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (17, 'Межрайонная инспекция Федеральной налоговой службы № 2 по Республике Башкортостан', '0276', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (18, 'Межрайонная инспекция Федеральной налоговой службы № 33 по Республике Башкортостан', '0277', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (19, 'Межрайонная инспекция Федеральной налоговой службы № 1 по Республике Башкортостан', '0278', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (20, 'Межрайонная инспекция Федеральной налоговой службы № 39 по Республике Башкортостан', '0280', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (21, 'Управление Федеральной налоговой службы по Республике Бурятия', '0300', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (22, 'Межрайонная инспекция Федеральной налоговой службы №8 по Республике Бурятия', '0309', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (23, 'Межрайонная инспекция Федеральной налоговой службы №4 по Республике Бурятия', '0317', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (24, 'Межрайонная инспекция Федеральной налоговой службы №3 по Республике Бурятия', '0318', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (25, 'Межрайонная инспекция Федеральной налоговой службы №1 по Республике Бурятия', '0323', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (26, 'Межрайонная инспекция Федеральной налоговой службы №2 по Республике Бурятия', '0326', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (27, 'Межрайонная инспекция Федеральной налоговой службы №9 по Республике Бурятия', '0327', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (28, 'Управление Федеральной налоговой службы по Республике Алтай', '0400', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (29, 'Межрайонная инспекция Федеральной налоговой службы №2 по Республике Алтай', '0404', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (30, 'Межрайонная инспекция Федеральной налоговой службы №5 по Республике Алтай', '0411', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (31, 'Управление Федеральной налоговой службы по Республике Дагестан', '0500', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (32, 'Межрайонная инспекция Федеральной налоговой службы № 12 по Республике Дагестан', '0506', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (33, 'Межрайонная инспекция Федеральной налоговой службы № 7 по Республике Дагестан', '0507', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (34, 'Межрайонная инспекция Федеральной налоговой службы № 9 по Республике Дагестан', '0521', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (35, 'Межрайонная инспекция Федеральной налоговой службы № 5 по Республике Дагестан', '0522', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (36, 'Межрайонная инспекция Федеральной налоговой службы № 1 по Республике Дагестан', '0523', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (37, 'Межрайонная инспекция Федеральной налоговой службы № 2 по Республике Дагестан', '0529', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (38, 'Межрайонная инспекция Федеральной налоговой службы № 16 по Республике Дагестан', '0531', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (39, 'Межрайонная инспекция Федеральной налоговой службы № 10 по Республике Дагестан', '0533', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (40, 'Межрайонная инспекция Федеральной налоговой службы № 13 по Республике Дагестан', '0536', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (41, 'Межрайонная инспекция Федеральной налоговой службы по крупнейшим налогоплательщикам по Республике Дагестан', '0541', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (42, 'Межрайонная инспекция Федеральной налоговой службы № 3 по Республике Дагестан', '0542', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (43, 'Межрайонная инспекция Федеральной налоговой службы № 17 по Республике Дагестан', '0544', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (44, 'Инспекция Федеральной налоговой службы по г.Каспийску Республики Дагестан', '0545', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (45, 'Межрайонная инспекция Федеральной налоговой службы № 8 по Республике Дагестан', '0546', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (46, 'Межрайонная инспекция Федеральной налоговой службы № 15 по Республике Дагестан', '0547', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (47, 'Межрайонная инспекция Федеральной налоговой службы № 6 по Республике Дагестан', '0548', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (48, 'Межрайонная инспекция Федеральной налоговой службы № 4 по Республике Дагестан', '0550', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (49, 'Инспекция Федеральной налоговой службы по Ленинскому району г.Махачкалы', '0571', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (50, 'Инспекция Федеральной налоговой службы по Советскому району г.Махачкалы', '0572', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (51, 'Инспекция Федеральной налоговой службы по Кировскому району г.Махачкалы', '0573', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (52, 'Управление Федеральной налоговой службы по Республике Ингушетия', '0600', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (53, 'Межрайонная инспекция Федеральной налоговой службы №3 по Республике Ингушетия', '0601', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (54, 'Межрайонная инспекция Федеральной налоговой службы №2 по Республике Ингушетия', '0603', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (55, 'Межрайонная инспекция Федеральной налоговой службы №1 по Республике Ингушетия', '0608', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (56, 'Управление Федеральной налоговой службы по Кабардино-Балкарской Республике', '0700', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (57, 'Межрайонная инспекция Федеральной налоговой службы №4 по Кабардино-Балкарской Республике', '0716', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (58, 'Межрайонная инспекция Федеральной налоговой службы №2 по Кабардино-Балкарской Республике', '0718', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (59, 'Межрайонная инспекция Федеральной налоговой службы №5 по Кабардино-Балкарской Республике', '0720', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (60, 'Межрайонная инспекция Федеральной налоговой службы №6 по Кабардино-Балкарской Республике', '0724', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (61, 'Инспекция Федеральной налоговой службы №1 по г. Нальчику Кабардино-Балкарской Республики', '0725', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (62, 'Инспекция Федеральной налоговой службы №2 по г. Нальчику Кабардино-Балкарской Республики', '0726', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (63, 'Управление Федеральной налоговой службы по Республике Калмыкия', '0800', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (64, 'Межрайонная инспекция Федеральной налоговой службы № 1 по Республике Калмыкия', '0801', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (65, 'Межрайонная инспекция Федеральной налоговой службы № 3 по Республике Калмыкия', '0813', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (66, 'Инспекция Федеральной налоговой службы по г.Элисте', '0816', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (67, 'Межрайонная инспекция Федеральной налоговой службы № 2 по Республике Калмыкия', '0817', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (68, 'Управление Федеральной налоговой службы по Карачаево-Черкесской Республике', '0900', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (69, 'Межрайонная инспекция Федеральной налоговой службы № 2 по Карачаево-Черкесской Республике', '0912', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (70, 'Межрайонная инспекция Федеральной налоговой службы № 1 по Карачаево-Черкесской Республике', '0916', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (71, 'Межрайонная инспекция Федеральной налоговой службы № 3 по Карачаево-Черкесской Республике', '0917', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (72, 'Межрайонная инспекция Федеральной налоговой службы № 4 по Карачаево-Черкесской Республике', '0918', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (73, 'Межрайонная инспекция Федеральной налоговой службы № 5 по Карачаево-Черкесской Республике', '0919', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (74, 'Межрайонная инспекция Федеральной налоговой службы № 6 по Карачаево-Черкесской Республике', '0920', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (75, 'Управление Федеральной налоговой службы по Республике Карелия', '1000', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (76, 'Инспекция Федеральной налоговой службы по г.Петрозаводску', '1001', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (77, 'Межрайонная инспекция Федеральной налоговой службы №1 по Республике Карелия', '1031', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (78, 'Межрайонная инспекция Федеральной налоговой службы № 2 по Республике Карелия', '1032', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (79, 'Межрайонная инспекция Федеральной налоговой службы № 5 по Республике Карелия', '1035', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (80, 'Межрайонная инспекция Федеральной налоговой службы №9 по Республике Карелия', '1039', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (81, 'Межрайонная инспекция Федеральной налоговой службы № 10 по Республике Карелия', '1040', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (82, 'Управление Федеральной налоговой службы по Республике Коми', '1100', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (83, 'Инспекция Федеральной налоговой службы по г. Сыктывкару', '1101', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (84, 'Межрайонная инспекция Федеральной налоговой службы № 3 по Республике Коми', '1102', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (85, 'Инспекция Федеральной налоговой службы по г. Воркуте Республики Коми', '1103', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (86, 'Инспекция Федеральной налоговой службы по г. Инте Республики Коми', '1104', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (87, 'Межрайонная инспекция Федеральной налоговой службы № 2 по Республике Коми', '1105', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (88, 'Инспекция Федеральной налоговой службы по г. Усинску Республики Коми', '1106', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (89, 'Межрайонная инспекция Федеральной налоговой службы № 4 по Республике Коми', '1108', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (90, 'Межрайонная инспекция Федеральной налоговой службы № 1 по Республике Коми', '1109', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (91, 'Межрайонная инспекция Федеральной налоговой службы № 5 по Республике Коми', '1121', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (92, 'Межрайонная инспекция Федеральной налоговой службы по крупнейшим налогоплательщикам по Республике Коми', '1122', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (93, 'Управление Федеральной налоговой службы по Республике Марий Эл', '1200', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (94, 'Инспекция Федеральной налоговой службы по г. Йошкар-Оле', '1215', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (95, 'Межрайонная инспекция Федеральной налоговой службы № 1 по Республике Марий Эл', '1218', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (96, 'Межрайонная инспекция Федеральной налоговой службы № 6 по Республике Марий Эл', '1223', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (97, 'Межрайонная инспекция Федеральной налоговой службы № 4 по Республике Марий Эл', '1224', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (98, 'Межрайонная инспекция Федеральной налоговой службы № 2 по Республике Марий Эл', '1225', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (99, 'Межрайонная инспекция Федеральной налоговой службы № 3 по Республике Марий Эл', '1226', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (100, 'Управление Федеральной налоговой службы по Республике Мордовия', '1300', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (101, 'Межрайонная инспекция Федеральной налоговой службы № 6 по Республике Мордовия', '1308', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (102, 'Межрайонная инспекция Федеральной налоговой службы № 4 по Республике Мордовия', '1310', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (103, 'Межрайонная инспекция Федеральной налоговой службы № 7 по Республике Мордовия', '1314', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (104, 'Межрайонная инспекция Федеральной налоговой службы № 3 по Республике Мордовия', '1322', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (105, 'Межрайонная инспекция Федеральной налоговой службы № 5 по Республике Мордовия', '1323', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (106, 'Межрайонная инспекция Федеральной налоговой службы № 2 по Республике Мордовия', '1324', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (107, 'Инспекция Федеральной налоговой службы по Ленинскому району г. Саранска', '1326', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (108, 'Межрайонная инспекция Федеральной налоговой службы № 1 по Республике Мордовия', '1327', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (109, 'Инспекция Федеральной налоговой службы по Октябрьскому району г. Саранска', '1328', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (110, 'Управление Федеральной налоговой службы по Республике Саха (Якутия)', '1400', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (111, 'Инспекция Федеральной налоговой службы по Алданскому району Республики Саха (Якутия)', '1402', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (112, 'Межрайонная инспекция Федеральной налоговой службы № 9 по Республике Саха (Якутия)', '1426', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (113, 'Инспекция Федеральной налоговой службы по Нерюнгринскому району Республики Саха (Якутия)', '1434', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (114, 'Межрайонная инспекция Федеральной налоговой службы № 1 по Республике Саха (Якутия)', '1436', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (115, 'Межрайонная инспекция Федеральной налоговой службы № 3 по Республике Саха (Якутия)', '1445', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (116, 'Межрайонная инспекция Федеральной налоговой службы № 4 по Республике Саха (Якутия)', '1446', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (117, 'Межрайонная инспекция Федеральной налоговой службы № 5 по Республике Саха (Якутия)', '1447', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (118, 'Межрайонная инспекция Федеральной налоговой службы № 8 по Республике Саха (Якутия)', '1448', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (119, 'Межрайонная инспекция Федеральной налоговой службы по крупнейшим налогоплательщикам по Республике Саха (Якутия)', '1449', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (120, 'Межрайонная инспекция Федеральной налоговой службы № 2 по Республике Саха (Якутия)', '1450', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (121, 'Управление Федеральной налоговой службы по Республике Северная Осетия-Алания', '1500', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (122, 'Инспекция Федеральной налоговой службы по Моздокскому району Республики Северная Осетия - Алания', '1510', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (123, 'Межрайонная инспекция Федеральной налоговой службы №3 по Республике Северная Осетия - Алания', '1511', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (124, 'Инспекция Федеральной налоговой службы по Пригородному району Республики Северная Осетия - Алания', '1512', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (125, 'Межрайонная инспекция Федеральной налоговой службы по г.Владикавказу', '1513', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (126, 'Межрайонная инспекция Федеральной налоговой службы №4 по Республике Северная Осетия - Алания', '1514', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (127, 'Управление Федеральной налоговой службы по Республике Татарстан', '1600', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (128, 'Межрайонная инспекция Федеральной налоговой службы № 16 по Республике Татарстан', '1644', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (129, 'Инспекция Федеральной налоговой службы по г. Набережные Челны Республики Татарстан', '1650', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (130, 'Межрайонная инспекция Федеральной налоговой службы № 11 по Республике Татарстан', '1651', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (131, 'Межрайонная инспекция Федеральной налоговой службы № 14 по Республике Татарстан', '1655', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (132, 'Межрайонная инспекция Федеральной налоговой службы № 19 по Республике Татарстан', '1656', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (133, 'Инспекция Федеральной налоговой службы по Московскому району г. Казани', '1658', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (134, 'Межрайонная инспекция Федеральной налоговой службы № 8 по Республике Татарстан', '1673', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (135, 'Межрайонная инспекция Федеральной налоговой службы № 9 по Республике Татарстан', '1674', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (136, 'Межрайонная инспекция Федеральной налоговой службы № 10 по Республике Татарстан', '1675', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (137, 'Межрайонная инспекция Федеральной налоговой службы № 12 по Республике Татарстан', '1677', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (138, 'Межрайонная инспекция Федеральной налоговой службы по крупнейшим налогоплательщикам по Республике Татарстан', '1681', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (139, 'Межрайонная инспекция Федеральной налоговой службы № 3 по Республике Татарстан', '1683', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (140, 'Межрайонная инспекция Федеральной налоговой службы № 4 по Республике Татарстан', '1684', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (141, 'Межрайонная инспекция Федеральной налоговой службы № 5 по Республике Татарстан', '1685', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (142, 'Межрайонная инспекция Федеральной налоговой службы № 6 по Республике Татарстан', '1686', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (143, 'Межрайонная инспекция Федеральной налоговой службы № 17 по Республике Татарстан', '1689', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (144, 'Межрайонная инспекция Федеральной налоговой службы № 18 по Республике Татарстан', '1690', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (145, 'Управление Федеральной налоговой службы по Республике Тыва', '1700', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (146, 'Межрайонная инспекция Федеральной налоговой службы № 1 по Республике Тыва', '1719', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (147, 'Межрайонная инспекция Федеральной налоговой службы № 2 по Республике Тыва', '1720', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (148, 'Межрайонная инспекция Федеральной налоговой службы № 3 по Республике Тыва', '1721', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (149, 'Межрайонная инспекция Федеральной налоговой службы № 4 по Республике Тыва', '1722', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (150, 'Управление Федеральной налоговой службы по Удмуртской Республике', '1800', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (151, 'Межрайонная инспекция Федеральной налоговой службы № 6 по Удмуртской Республике', '1821', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (152, 'Межрайонная инспекция Федеральной налоговой службы № 3 по Удмуртской Республике', '1828', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (153, 'Межрайонная инспекция Федеральной налоговой службы № 10 по Удмуртской Республике', '1831', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (154, 'Инспекция Федеральной налоговой службы по Ленинскому району г.Ижевска', '1832', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (155, 'Межрайонная инспекция Федеральной налоговой службы по крупнейшим налогоплательщикам по Удмуртской Республике', '1836', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (156, 'Межрайонная инспекция Федеральной налоговой службы № 2 по Удмуртской Республике', '1837', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (157, 'Межрайонная инспекция Федеральной налоговой службы № 5 по Удмуртской Республике', '1838', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (158, 'Межрайонная инспекция Федеральной налоговой службы № 7 по Удмуртской Республике', '1839', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (159, 'Межрайонная инспекция Федеральной налоговой службы № 8 по Удмуртской Республике', '1840', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (160, 'Межрайонная инспекция Федеральной налоговой службы № 9 по Удмуртской Республике', '1841', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (161, 'Управление Федеральной налоговой службы по Республике Хакасия', '1900', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (162, 'Межрайонная инспекция Федеральной налоговой службы №1 по Республике Хакасия', '1901', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (163, 'Межрайонная инспекция Федеральной налоговой службы №2 по Республике Хакасия', '1902', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (164, 'Межрайонная инспекция Федеральной налоговой службы №3 по Республике Хакасия', '1903', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (165, 'Управление Федеральной налоговой службы по Чеченской Республике', '2000', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (166, 'Межрайонная инспекция Федеральной налоговой службы №1 по Чеченской Республике', '2031', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (167, 'Межрайонная инспекция Федеральной налоговой службы №2 по Чеченской Республике', '2032', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (168, 'Межрайонная инспекция Федеральной налоговой службы №3 по Чеченской Республике', '2033', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (169, 'Межрайонная инспекция Федеральной налоговой службы №4 по Чеченской Республике', '2034', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (170, 'Межрайонная инспекция Федеральной налоговой службы №5 по Чеченской Республике', '2035', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (171, 'Межрайонная инспекция Федеральной налоговой службы №6 по Чеченской Республике', '2036', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (172, 'Управление Федеральной налоговой службы по Чувашской Республике', '2100', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (173, 'Инспекция Федеральной налоговой службы по г.Новочебоксарску Чувашской Республики', '2124', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (174, 'Инспекция Федеральной налоговой службы по г.Чебоксары', '2130', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (175, 'Межрайонная инспекция Федеральной налоговой службы № 1 по Чувашской Республике', '2131', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (176, 'Межрайонная инспекция Федеральной налоговой службы № 2 по Чувашской Республике', '2132', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (177, 'Межрайонная инспекция Федеральной налоговой службы № 3 по Чувашской Республике', '2133', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (178, 'Межрайонная инспекция Федеральной налоговой службы № 4 по Чувашской Республике', '2134', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (179, 'Межрайонная инспекция Федеральной налоговой службы № 5 по Чувашской Республике', '2135', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (180, 'Межрайонная инспекция Федеральной налоговой службы № 7 по Чувашской Республике', '2137', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (181, 'Межрайонная инспекция Федеральной налоговой службы № 8 по Чувашской Республике', '2138', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (182, 'Управление Федеральной налоговой службы по Алтайскому краю', '2200', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (183, 'Межрайонная инспекция Федеральной налоговой службы № 10 по Алтайскому краю', '2201', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (184, 'Межрайонная инспекция Федеральной налоговой службы по крупнейшим налогоплательщикам Алтайского края', '2202', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (185, 'Межрайонная инспекция Федеральной налоговой службы №1 по Алтайскому краю', '2204', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (186, 'Межрайонная инспекция Федеральной налоговой службы № 6 по Алтайскому краю', '2207', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (187, 'Межрайонная инспекция Федеральной налоговой службы № 4 по Алтайскому краю', '2208', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (188, 'Межрайонная инспекция Федеральной налоговой службы № 12 по Алтайскому краю', '2209', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (189, 'Межрайонная инспекция Федеральной налоговой службы № 9 по Алтайскому краю', '2210', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (190, 'Межрайонная инспекция Федеральной налоговой службы № 14 по Алтайскому краю', '2223', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (191, 'Инспекция Федеральной налоговой службы по Октябрьскому району г. Барнаула', '2224', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (192, 'Межрайонная инспекция Федеральной налоговой службы № 15 по Алтайскому краю', '2225', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (193, 'Межрайонная инспекция Федеральной налоговой службы № 8 по Алтайскому краю', '2235', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (194, 'Межрайонная инспекция Федеральной налоговой службы № 7 по Алтайскому краю', '2261', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (195, 'Управление Федеральной налоговой службы по Краснодарскому краю', '2300', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (196, 'Инспекция Федеральной налоговой службы по городу-курорту Анапа Краснодарского края', '2301', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (197, 'Инспекция Федеральной налоговой службы по городу-курорту Геленджику Краснодарского края', '2304', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (198, 'Межрайонная инспекция Федеральной налоговой службы по крупнейшим налогоплательщикам по Краснодарскому краю', '2307', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (199, 'Инспекция Федеральной налоговой службы №1 по г. Краснодару', '2308', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (200, 'Инспекция Федеральной налоговой службы №3 по г. Краснодару', '2309', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (201, 'Инспекция Федеральной налоговой службы №2 по г. Краснодару', '2310', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (202, 'Инспекция Федеральной налоговой службы №4 по г. Краснодару', '2311', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (203, 'Инспекция Федеральной налоговой службы №5 по г. Краснодару', '2312', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (204, 'Инспекция Федеральной налоговой службы по г. Новороссийску Краснодарского края', '2315', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (205, 'Инспекция Федеральной налоговой службы по Абинскому району Краснодарского края', '2323', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (206, 'Инспекция Федеральной налоговой службы по г. Крымску Краснодарского края', '2337', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (207, 'Инспекция Федеральной налоговой службы по Курганинскому району Краснодарского края', '2339', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (208, 'Инспекция Федеральной налоговой службы по Северскому району Краснодарского края', '2348', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (209, 'Инспекция Федеральной налоговой службы по Темрюкскому району Краснодарского края', '2352', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (210, 'Межрайонная инспекция Федеральной налоговой службы № 1 по Краснодарскому краю', '2360', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (211, 'Межрайонная инспекция Федеральной налоговой службы № 2 по Краснодарскому краю', '2361', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (212, 'Межрайонная инспекция Федеральной налоговой службы № 3 по Краснодарскому краю', '2362', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (213, 'Межрайонная инспекция Федеральной налоговой службы № 4 по Краснодарскому краю', '2363', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (214, 'Межрайонная инспекция Федеральной налоговой службы № 5 по Краснодарскому краю', '2364', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (215, 'Межрайонная инспекция Федеральной налоговой службы № 6 по Краснодарскому краю', '2365', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (216, 'Межрайонная инспекция Федеральной налоговой службы № 7 по Краснодарскому краю', '2366', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (217, 'Межрайонная инспекция Федеральной налоговой службы № 8 по Краснодарскому краю', '2367', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (218, 'Межрайонная инспекция Федеральной налоговой службы №9 по Краснодарскому краю', '2368', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (219, 'Межрайонная инспекция Федеральной налоговой службы № 10 по Краснодарскому краю', '2369', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (220, 'Межрайонная инспекция Федеральной налоговой службы № 11 по Краснодарскому краю', '2370', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (221, 'Межрайонная инспекция Федеральной налоговой службы № 12 по Краснодарскому краю', '2371', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (222, 'Межрайонная инспекция Федеральной налоговой службы № 13 по Краснодарскому краю', '2372', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (223, 'Межрайонная инспекция Федеральной налоговой службы № 14 по Краснодарскому краю', '2373', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (224, 'Межрайонная инспекция Федеральной налоговой службы № 15 по Краснодарскому краю', '2374', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (225, 'Управление Федеральной налоговой службы по Красноярскому краю', '2400', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (226, 'Межрайонная инспекция Федеральной налоговой службы № 17 по Красноярскому краю', '2411', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (227, 'Межрайонная инспекция Федеральной налоговой службы № 18 по Красноярскому краю', '2420', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (228, 'Межрайонная инспекция Федеральной налоговой службы № 4 по Красноярскому краю', '2443', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (229, 'Межрайонная инспекция Федеральной налоговой службы № 7 по Красноярскому краю', '2448', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (230, 'Межрайонная инспекция Федеральной налоговой службы № 8 по Красноярскому краю', '2450', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (231, 'Межрайонная инспекция Федеральной налоговой службы № 26 по Красноярскому краю', '2452', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (232, 'Межрайонная инспекция Федеральной налоговой службы № 9 по Красноярскому краю', '2454', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (233, 'Межрайонная инспекция Федеральной налоговой службы № 10 по Красноярскому краю', '2455', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (234, 'Межрайонная инспекция Федеральной налоговой службы № 25 по Красноярскому краю', '2457', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (235, 'Межрайонная инспекция Федеральной налоговой службы № 12 по Красноярскому краю', '2459', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (236, 'Инспекция Федеральной налоговой службы по Железнодорожному району г.Красноярска', '2460', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (237, 'Межрайонная инспекция Федеральной налоговой службы № 24 по Красноярскому краю', '2461', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (238, 'Инспекция Федеральной налоговой службы по Октябрьскому району г.Красноярска', '2463', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (239, 'Межрайонная инспекция Федеральной налоговой службы № 22 по Красноярскому краю', '2464', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (240, 'Инспекция Федеральной налоговой службы по Советскому району г.Красноярска', '2465', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (241, 'Инспекция Федеральной налоговой службы по Центральному району г.Красноярска', '2466', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (242, 'Межрайонная инспекция Федеральной налоговой службы по крупнейшим налогоплательщикам по Красноярскому краю', '2467', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (243, 'Межрайонная инспекция Федеральной налоговой службы № 23 по Красноярскому краю', '2468', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (244, 'Управление Федеральной налоговой службы по Приморскому краю', '2500', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (245, 'Межрайонная инспекция Федеральной налоговой службы №4 по Приморскому краю', '2501', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (246, 'Межрайонная инспекция Федеральной налоговой службы № 10 по Приморскому краю', '2502', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (247, 'Межрайонная инспекция Федеральной налоговой службы № 1 по Приморскому краю', '2503', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (248, 'Межрайонная инспекция Федеральной налоговой службы № 6 по Приморскому краю', '2505', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (249, 'Межрайонная инспекция Федеральной налоговой службы №2 по Приморскому краю', '2506', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (250, 'Межрайонная инспекция Федеральной налоговой службы № 7 по Приморскому краю', '2507', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (251, 'Инспекция Федеральной налоговой службы по г.Находке Приморского края', '2508', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (252, 'Межрайонная инспекция Федеральной налоговой службы №8 по Приморскому краю', '2509', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (253, 'Межрайонная инспекция Федеральной налоговой службы № 3 по Приморскому краю', '2510', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (254, 'Межрайонная инспекция Федеральной налоговой службы №9 по Приморскому краю', '2511', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (255, 'Межрайонная инспекция Федеральной налоговой службы № 5 по Приморскому краю', '2515', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (256, 'Межрайонная инспекция Федеральной налоговой службы №11 по Приморскому краю', '2533', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (257, 'Инспекция Федеральной налоговой службы по Ленинскому району г.Владивостока', '2536', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (258, 'Инспекция Федеральной налоговой службы по Первомайскому району г. Владивостока', '2537', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (259, 'Инспекция Федеральной налоговой службы по Фрунзенскому району г.Владивостока', '2540', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (260, 'Межрайонная инспекция Федеральной налоговой службы по крупнейшим налогоплательщикам по Приморскому краю', '2542', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (261, 'Межрайонная инспекция Федеральной налоговой службы № 12 по Приморскому краю', '2543', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (262, 'Управление Федеральной налоговой службы по Ставропольскому краю', '2600', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (263, 'Инспекция Федеральной налоговой службы по г.Георгиевску Ставропольского края', '2625', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (264, 'Инспекция Федеральной налоговой службы по г.Кисловодску Ставропольского края', '2628', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (265, 'Инспекция Федеральной налоговой службы по г.Пятигорску Ставропольского края', '2632', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (266, 'Инспекция Федеральной налоговой службы по Ленинскому району г. Ставрополя', '2634', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (267, 'Межрайонная инспекция Федеральной налоговой службы № 12 по Ставропольскому краю', '2635', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (268, 'Межрайонная инспекция Федеральной налоговой службы № 1 по Ставропольскому краю', '2641', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (269, 'Межрайонная инспекция Федеральной налоговой службы № 3 по Ставропольскому краю', '2643', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (270, 'Межрайонная инспекция Федеральной налоговой службы № 4 по Ставропольскому краю', '2644', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (271, 'Межрайонная инспекция Федеральной налоговой службы № 5 по Ставропольскому краю', '2645', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (272, 'Межрайонная инспекция Федеральной налоговой службы № 6 по Ставропольскому краю', '2646', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (273, 'Межрайонная инспекция Федеральной налоговой службы № 8 по Ставропольскому краю', '2648', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (274, 'Межрайонная инспекция Федеральной налоговой службы № 9 по Ставропольскому краю', '2649', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (275, 'Межрайонная инспекция Федеральной налоговой службы № 10 по Ставропольскому краю', '2650', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (276, 'Межрайонная инспекция Федеральной налоговой службы № 11 по Ставропольскому краю', '2651', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (277, 'Управление Федеральной налоговой службы по Хабаровскому краю', '2700', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (278, 'Инспекция Федеральной налоговой службы по г.Комсомольску-на-Амуре Хабаровского края', '2703', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (279, 'Межрайонная инспекция Федеральной налоговой службы №1 по Хабаровскому краю', '2705', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (280, 'Межрайонная инспекция Федеральной налоговой службы №5 по Хабаровскому краю', '2709', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (281, 'Межрайонная инспекция Федеральной налоговой службы №3 по Хабаровскому краю', '2720', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (282, 'Инспекция Федеральной налоговой службы по Центральному району г.Хабаровска', '2721', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (283, 'Межрайонная инспекция Федеральной налоговой службы №6 по Хабаровскому краю', '2722', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (284, 'Инспекция Федеральной налоговой службы по Индустриальному району г.Хабаровска', '2723', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (285, 'Инспекция Федеральной налоговой службы по Железнодорожному району г.Хабаровска', '2724', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (286, 'Межрайонная инспекция Федеральной налоговой службы № 8 по Хабаровскому краю', '2728', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (287, 'Управление Федеральной налоговой службы по Амурской области', '2800', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (288, 'Межрайонная инспекция Федеральной налоговой службы № 1 по Амурской области', '2801', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (289, 'Межрайонная инспекция Федеральной налоговой службы № 3 по Амурской области', '2804', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (290, 'Межрайонная инспекция Федеральной налоговой службы № 5 по Амурской области', '2807', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (291, 'Межрайонная инспекция Федеральной налоговой службы № 7 по Амурской области', '2808', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (292, 'Межрайонная инспекция Федеральной налоговой службы № 2 по Амурской области', '2813', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (293, 'Межрайонная инспекция Федеральной налоговой службы № 4 по Амурской области', '2815', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (294, 'Межрайонная инспекция Федеральной налоговой службы № 6 по Амурской области', '2827', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (295, 'Управление Федеральной налоговой службы по Архангельской области и Ненецкому автономному округу', '2900', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (296, 'Инспекция Федеральной налоговой службы по г.Архангельску', '2901', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (297, 'Межрайонная инспекция Федеральной налоговой службы №3 по Архангельской области и Ненецкому автономному округу', '2903', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (298, 'Межрайонная инспекция Федеральной налоговой службы №1 по Архангельской области и Ненецкому автономному округу', '2904', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (299, 'Межрайонная инспекция Федеральной налоговой службы №8 по Архангельской области и Ненецкому автономному округу', '2907', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (688, 'Инспекция Федеральной налоговой службы по Октябрьскому району г.Ростова-на-Дону', '6165', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (689, 'Межрайонная инспекция Федеральной налоговой службы № 1 по Ростовской области', '6171', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (690, 'Межрайонная инспекция Федеральной налоговой службы № 3 по Ростовской области', '6173', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (691, 'Межрайонная инспекция Федеральной налоговой службы № 4 по Ростовской области', '6174', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (692, 'Межрайонная инспекция Федеральной налоговой службы № 9 по Ростовской области', '6179', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (693, 'Межрайонная инспекция Федеральной налоговой службы № 11 по Ростовской области', '6181', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (694, 'Межрайонная инспекция Федеральной налоговой службы № 12 по Ростовской области', '6182', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (695, 'Межрайонная инспекция Федеральной налоговой службы № 13 по Ростовской области', '6183', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (696, 'Межрайонная инспекция Федеральной налоговой службы № 16 по Ростовской области', '6186', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (697, 'Межрайонная инспекция Федеральной налоговой службы № 18 по Ростовской области', '6188', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (698, 'Межрайонная инспекция Федеральной налоговой службы № 21 по Ростовской области', '6191', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (699, 'Межрайонная инспекция Федеральной налоговой службы № 22 по Ростовской области', '6192', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (700, 'Межрайонная инспекция Федеральной налоговой службы № 23 по Ростовской области', '6193', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (701, 'Межрайонная инспекция Федеральной налоговой службы № 24 по Ростовской области', '6194', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (702, 'Межрайонная инспекция Федеральной налоговой службы № 25 по Ростовской области', '6195', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (703, 'Межрайонная инспекция Федеральной налоговой службы № 26 по Ростовской области', '6196', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (704, 'Управление Федеральной налоговой службы по Рязанской области', '6200', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (705, 'Межрайонная инспекция Федеральной налоговой службы №7 по Рязанской области', '6214', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (706, 'Межрайонная инспекция Федеральной налоговой службы №6 по Рязанской области', '6215', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (707, 'Межрайонная инспекция Федеральной налоговой службы №5 по Рязанской области', '6219', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (708, 'Межрайонная инспекция Федеральной налоговой службы №10 по Рязанской области', '6225', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (709, 'Межрайонная инспекция Федеральной налоговой службы №9 по Рязанской области', '6226', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (710, 'Межрайонная инспекция Федеральной налоговой службы №1 по Рязанской области', '6229', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (711, 'Межрайонная инспекция Федеральной налоговой службы №3 по Рязанской области', '6230', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (712, 'Межрайонная инспекция Федеральной налоговой службы №4 по Рязанской области', '6232', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (713, 'Межрайонная инспекция Федеральной налоговой службы №2 по Рязанской области', '6234', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (714, 'Управление Федеральной налоговой службы по Самарской области', '6300', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (715, 'Межрайонная инспекция Федеральной налоговой службы по крупнейшим налогоплательщикам по Самарской области', '6310', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (716, 'Инспекция Федеральной налоговой службы по Железнодорожному району г. Самары', '6311', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (717, 'Инспекция Федеральной налоговой службы по Кировскому району г. Самары', '6312', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (718, 'Инспекция Федеральной налоговой службы по Красноглинскому району г. Самары', '6313', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (719, 'Инспекция Федеральной налоговой службы по Ленинскому району г. Самары', '6315', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (720, 'Инспекция Федеральной налоговой службы по Октябрьскому району г. Самары', '6316', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (721, 'Межрайонная инспекция Федеральной налоговой службы № 18 по Самарской области', '6317', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (722, 'Инспекция Федеральной налоговой службы по Советскому району г. Самары', '6318', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (723, 'Инспекция Федеральной налоговой службы по Промышленному району г. Самары', '6319', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (724, 'Межрайонная инспекция Федеральной налоговой службы № 2 по Самарской области', '6320', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (725, 'Межрайонная инспекция Федеральной налоговой службы № 19 по Самарской области', '6324', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (726, 'Межрайонная инспекция Федеральной налоговой службы № 3 по Самарской области', '6325', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (727, 'Межрайонная инспекция Федеральной налоговой службы № 16 по Самарской области', '6330', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (728, 'Межрайонная инспекция Федеральной налоговой службы № 4 по Самарской области', '6350', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (729, 'Межрайонная инспекция Федеральной налоговой службы № 14 по Самарской области', '6372', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (730, 'Межрайонная инспекция Федеральной налоговой службы № 11 по Самарской области', '6375', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (731, 'Межрайонная инспекция Федеральной налоговой службы № 7 по Самарской области', '6376', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (732, 'Межрайонная инспекция Федеральной налоговой службы № 8 по Самарской области', '6377', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (733, 'Межрайонная инспекция Федеральной налоговой службы № 17 по Самарской области', '6381', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (734, 'Межрайонная инспекция Федеральной налоговой службы № 15 по Самарской области', '6382', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (735, 'Управление Федеральной налоговой службы по Саратовской области', '6400', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (736, 'Межрайонная инспекция Федеральной налоговой службы №9 по Саратовской области', '6413', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (737, 'Межрайонная инспекция Федеральной налоговой службы №12 по Саратовской области', '6432', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (738, 'Межрайонная инспекция Федеральной налоговой службы №13 по Саратовской области', '6438', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (739, 'Межрайонная инспекция Федеральной налоговой службы №2 по Саратовской области', '6439', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (740, 'Межрайонная инспекция Федеральной налоговой службы №1 по Саратовской области', '6440', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (741, 'Межрайонная инспекция Федеральной налоговой службы №3 по Саратовской области', '6441', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (742, 'Межрайонная инспекция Федеральной налоговой службы №10 по Саратовской области', '6444', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (743, 'Межрайонная инспекция Федеральной налоговой службы №6 по Саратовской области', '6445', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (744, 'Межрайонная инспекция Федеральной налоговой службы №5 по Саратовской области', '6446', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (745, 'Межрайонная инспекция Федеральной налоговой службы по крупнейшим налогоплательщикам по Саратовской области', '6447', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (746, 'Межрайонная инспекция Федеральной налоговой службы №7 по Саратовской области', '6449', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (747, 'Межрайонная инспекция Федеральной налоговой службы №8 по Саратовской области', '6450', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (748, 'Инспекция Федеральной налоговой службы по Заводскому району г.Саратова', '6451', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (749, 'Инспекция Федеральной налоговой службы по Ленинскому району г.Саратова', '6453', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (750, 'Инспекция Федеральной налоговой службы по Октябрьскому району г.Саратова', '6454', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (751, 'Инспекция Федеральной налоговой службы по Фрунзенскому району г.Саратова', '6455', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (752, 'Управление Федеральной налоговой службы по Сахалинской области', '6500', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (753, 'Межрайонная инспекция Федеральной налоговой службы № 1 по Сахалинской области', '6501', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (754, 'Межрайонная инспекция Федеральной налоговой службы № 5 по Сахалинской области', '6504', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (755, 'Межрайонная инспекция Федеральной налоговой службы № 3 по Сахалинской области', '6507', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (756, 'Межрайонная инспекция Федеральной налоговой службы № 2 по Сахалинской области', '6509', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (757, 'Межрайонная инспекция Федеральной налоговой службы № 4 по Сахалинской области', '6517', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (758, 'Управление Федеральной налоговой службы по Свердловской области', '6600', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (759, 'Межрайонная инспекция Федеральной налоговой службы по крупнейшим налогоплательщикам по Свердловской области', '6608', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (760, 'Межрайонная инспекция Федеральной налоговой службы №22 по Свердловской области', '6612', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (761, 'Межрайонная инспекция Федеральной налоговой службы №14 по Свердловской области', '6617', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (762, 'Межрайонная инспекция Федеральной налоговой службы №2 по Свердловской области', '6619', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (763, 'Межрайонная инспекция Федеральной налоговой службы №16 по Свердловской области', '6623', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (764, 'Межрайонная инспекция Федеральной налоговой службы №19 по Свердловской области', '6633', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (765, 'Инспекция Федеральной налоговой службы по Верх-Исетскому району г.Екатеринбурга', '6658', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (766, 'Инспекция Федеральной налоговой службы по Кировскому району г.Екатеринбурга', '6670', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (767, 'Инспекция Федеральной налоговой службы по Ленинскому району г.Екатеринбурга', '6671', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (768, 'Межрайонная инспекция Федеральной налоговой службы №13 по Свердловской области', '6676', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (769, 'Межрайонная инспекция Федеральной налоговой службы №23 по Свердловской области', '6677', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (770, 'Межрайонная инспекция Федеральной налоговой службы №24 по Свердловской области', '6678', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (771, 'Межрайонная инспекция Федеральной налоговой службы №25 по Свердловской области', '6679', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (772, 'Межрайонная инспекция Федеральной налоговой службы №26 по Свердловской области', '6680', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (773, 'Межрайонная инспекция Федеральной налоговой службы №27 по Свердловской области', '6681', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (774, 'Межрайонная инспекция Федеральной налоговой службы №28 по Свердловской области', '6682', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (821, 'Межрайонная инспекция Федеральной налоговой службы № 5 по Тульской области', '7151', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (822, 'Межрайонная инспекция Федеральной налоговой службы № 9 по Тульской области', '7153', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (823, 'Межрайонная инспекция Федеральной налоговой службы № 10 по Тульской области', '7154', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (824, 'Межрайонная инспекция Федеральной налоговой службы № 11 по Тульской области', '7155', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (825, 'Управление Федеральной налоговой службы по Тюменской области', '7200', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (826, 'Инспекция Федеральной налоговой службы по г. Тюмени № 2', '7202', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (827, 'Инспекция Федеральной налоговой службы по г. Тюмени № 3', '7203', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (828, 'Инспекция Федеральной налоговой службы по г. Тюмени № 4', '7204', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (829, 'Межрайонная инспекция Федеральной налоговой службы № 12 по Тюменской области', '7205', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (830, 'Межрайонная инспекция Федеральной налоговой службы № 7 по Тюменской области', '7206', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (831, 'Межрайонная инспекция Федеральной налоговой службы № 8 по Тюменской области', '7207', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (832, 'Межрайонная инспекция Федеральной налоговой службы № 10 по Тюменской области', '7220', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (833, 'Межрайонная инспекция Федеральной налоговой службы № 6 по Тюменской области', '7224', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (834, 'Инспекция Федеральной налоговой службы по г.Тюмени № 1', '7230', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (835, 'Межрайонная инспекция Федеральной налоговой службы № 14 по Тюменской области', '7232', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (836, 'Управление Федеральной налоговой службы по Ульяновской области', '7300', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (837, 'Межрайонная инспекция Федеральной налоговой службы по крупнейшим налогоплательщикам по Ульяновской области', '7303', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (838, 'Межрайонная инспекция Федеральной налоговой службы №4 по Ульяновской области', '7309', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (839, 'Межрайонная инспекция Федеральной налоговой службы №5 по Ульяновской области', '7313', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (840, 'Межрайонная инспекция Федеральной налоговой службы №2 по Ульяновской области', '7321', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (841, 'Инспекция Федеральной налоговой службы по Ленинскому району г.Ульяновска', '7325', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (842, 'Инспекция Федеральной налоговой службы по Железнодорожному району г.Ульяновска', '7326', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (843, 'Инспекция Федеральной налоговой службы по Засвияжскому району г.Ульяновска', '7327', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (844, 'Инспекция Федеральной налоговой службы по Заволжскому району г.Ульяновска', '7328', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (845, 'Межрайонная инспекция Федеральной налоговой службы №7 по Ульяновской области', '7329', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (846, 'Управление Федеральной налоговой службы по Челябинской области', '7400', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (847, 'Межрайонная инспекция Федеральной налоговой службы № 21 по Челябинской области', '7404', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (848, 'Межрайонная инспекция Федеральной налоговой службы № 3 по Челябинской области', '7413', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (849, 'Межрайонная инспекция Федеральной налоговой службы № 23 по Челябинской области', '7415', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (850, 'Межрайонная инспекция Федеральной налоговой службы № 15 по Челябинской области', '7424', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (851, 'Межрайонная инспекция Федеральной налоговой службы № 10 по Челябинской области', '7430', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (852, 'Инспекция Федеральной налоговой службы по Калининскому району г. Челябинска', '7447', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (853, 'Инспекция Федеральной налоговой службы по Курчатовскому району г. Челябинска', '7448', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (854, 'Инспекция Федеральной налоговой службы по Ленинскому району г. Челябинска', '7449', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (855, 'Инспекция Федеральной налоговой службы по Советскому району г. Челябинска', '7451', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (856, 'Инспекция Федеральной налоговой службы по Тракторозаводскому району г. Челябинска', '7452', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (857, 'Инспекция Федеральной налоговой службы по Центральному району г. Челябинска', '7453', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (858, 'Межрайонная инспекция Федеральной налоговой службы по крупнейшим налогоплательщикам по Челябинской области', '7454', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (859, 'Межрайонная инспекция Федеральной налоговой службы № 16 по Челябинской области', '7455', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (860, 'Межрайонная инспекция Федеральной налоговой службы № 17 по Челябинской области', '7456', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (861, 'Межрайонная инспекция Федеральной налоговой службы № 18 по Челябинской области', '7457', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (862, 'Межрайонная инспекция Федеральной налоговой службы № 19 по Челябинской области', '7458', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (863, 'Межрайонная инспекция Федеральной налоговой службы № 20 по Челябинской области', '7459', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (864, 'Межрайонная инспекция Федеральной налоговой службы № 22 по Челябинской области', '7460', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (775, 'Межрайонная инспекция Федеральной налоговой службы №29 по Свердловской области', '6683', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (776, 'Межрайонная инспекция Федеральной налоговой службы №30 по Свердловской области', '6684', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (777, 'Межрайонная инспекция Федеральной налоговой службы №31 по Свердловской области', '6685', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (778, 'Межрайонная инспекция Федеральной налоговой службы №32 по Свердловской области', '6686', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (779, 'Управление Федеральной налоговой службы по Смоленской области', '6700', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (780, 'Межрайонная инспекция Федеральной налоговой службы №8 по Смоленской области', '6712', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (781, 'Межрайонная инспекция Федеральной налоговой службы №7 по Смоленской области', '6713', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (782, 'Межрайонная инспекция Федеральной налоговой службы №6 по Смоленской области', '6714', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (783, 'Межрайонная инспекция Федеральной налоговой службы №2 по Смоленской области', '6722', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (784, 'Межрайонная инспекция Федеральной налоговой службы №1 по Смоленской области', '6725', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (785, 'Межрайонная инспекция Федеральной налоговой службы №4 по Смоленской области', '6726', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (786, 'Межрайонная инспекция Федеральной налоговой службы №3 по Смоленской области', '6727', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (787, 'Инспекция Федеральной налоговой службы по г.Смоленску', '6732', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (788, 'Межрайонная инспекция Федеральной налоговой службы № 5 по Смоленской области', '6733', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (789, 'Управление Федеральной налоговой службы по Тамбовской области', '6800', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (790, 'Межрайонная инспекция Федеральной налоговой службы №7 по Тамбовской области', '6809', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (791, 'Межрайонная инспекция Федеральной налоговой службы №4 по Тамбовской области', '6820', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (792, 'Межрайонная инспекция Федеральной налоговой службы №9 по Тамбовской области', '6827', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (793, 'Межрайонная инспекция Федеральной налоговой службы №3 по Тамбовской области', '6828', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (794, 'Инспекция Федеральной налоговой службы по г.Тамбову', '6829', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (795, 'Управление Федеральной налоговой службы по Тверской области', '6900', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (796, 'Межрайонная инспекция Федеральной налоговой службы № 2 по Тверской области', '6906', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (797, 'Межрайонная инспекция Федеральной налоговой службы № 3 по Тверской области', '6908', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (798, 'Межрайонная инспекция Федеральной налоговой службы № 4 по Тверской области', '6910', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (799, 'Межрайонная инспекция Федеральной налоговой службы № 5 по Тверской области', '6912', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (800, 'Межрайонная инспекция Федеральной налоговой службы № 6 по Тверской области', '6913', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (801, 'Межрайонная инспекция Федеральной налоговой службы № 7 по Тверской области', '6914', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (802, 'Межрайонная инспекция Федеральной налоговой службы № 8 по Тверской области', '6915', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (803, 'Межрайонная инспекция Федеральной налоговой службы № 9 по Тверской области', '6949', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (804, 'Межрайонная инспекция Федеральной налоговой службы № 10 по Тверской области', '6950', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (805, 'Межрайонная инспекция Федеральной налоговой службы № 12 по Тверской области', '6952', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (806, 'Управление Федеральной налоговой службы по Томской области', '7000', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (807, 'Инспекция Федеральной налоговой службы по Томскому району Томской области', '7014', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (808, 'Инспекция Федеральной налоговой службы по г.Томску', '7017', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (809, 'Межрайонная инспекция Федеральной налоговой службы № 5 по Томской области', '7022', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (810, 'Инспекция Федеральной налоговой службы по ЗАТО Северск Томской области', '7024', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (811, 'Межрайонная инспекция Федеральной налоговой службы № 1 по Томской области', '7025', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (812, 'Межрайонная инспекция Федеральной налоговой службы № 2 по Томской области', '7026', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (813, 'Межрайонная инспекция Федеральной налоговой службы № 4 по Томской области', '7028', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (814, 'Межрайонная инспекция Федеральной налоговой службы № 6 по Томской области', '7030', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (815, 'Управление Федеральной налоговой службы по Тульской области', '7100', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (816, 'Межрайонная инспекция Федеральной налоговой службы по крупнейшим налогоплательщикам Тульской области', '7101', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (817, 'Межрайонная ИФНС России № 12 по Тульской области', '7104', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (818, 'Инспекция Федеральной налоговой службы по Центральному району г.Тулы', '7107', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (819, 'Межрайонная инспекция Федеральной налоговой службы № 8 по Тульской области', '7148', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (820, 'Межрайонная инспекция Федеральной налоговой службы № 1 по Тульской области', '7150', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (559, 'Межрайонная инспекция Федеральной налоговой службы № 8 по Нижегородской области', '5228', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (560, 'Межрайонная инспекция Федеральной налоговой службы № 12 по Нижегородской области', '5229', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (561, 'Межрайонная инспекция Федеральной налоговой службы № 13 по Нижегородской области', '5235', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (562, 'Межрайонная инспекция Федеральной налоговой службы № 1 по Нижегородской области', '5243', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (563, 'Инспекция Федеральной налоговой службы по Борскому району Нижегородской области', '5246', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (564, 'Межрайонная инспекция Федеральной налоговой службы № 4 по Нижегородской области', '5247', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (565, 'Межрайонная инспекция Федеральной налоговой службы № 5 по Нижегородской области', '5248', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (566, 'Межрайонная инспекция Федеральной налоговой службы № 2 по Нижегородской области', '5249', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (567, 'Межрайонная инспекция Федеральной налоговой службы № 6 по Нижегородской области', '5250', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (568, 'Межрайонная инспекция Федеральной налоговой службы № 7 по Нижегородской области', '5252', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (569, 'Межрайонная инспекция Федеральной налоговой службы по крупнейшим налогоплательщикам по Нижегородской области', '5253', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (570, 'Межрайонная инспекция Федеральной налоговой службы № 3 по Нижегородской области', '5254', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (571, 'Инспекция Федеральной налоговой службы по Автозаводскому району г.Нижнего Новгорода', '5256', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (572, 'Инспекция Федеральной налоговой службы по Канавинскому району г.Нижнего Новгорода', '5257', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (573, 'Инспекция Федеральной налоговой службы по Ленинскому району г.Нижнего Новгорода', '5258', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (574, 'Инспекция Федеральной налоговой службы по Московскому району г.Нижнего Новгорода', '5259', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (575, 'Инспекция Федеральной налоговой службы по Нижегородскому району г.Нижнего Новгорода', '5260', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (576, 'Инспекция Федеральной налоговой службы по Приокскому району г.Нижнего Новгорода', '5261', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (577, 'Инспекция Федеральной налоговой службы по Советскому району г.Нижнего Новгорода', '5262', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (578, 'Инспекция Федеральной налоговой службы по Сормовскому району г.Нижнего Новгорода', '5263', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (579, 'Межрайонная инспекция Федеральной налоговой службы по централизованной обработке данных по Нижегородской области', '5270', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (580, 'Управление Федеральной налоговой службы по Новгородской области', '5300', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (581, 'Межрайонная инспекция Федеральной налоговой службы №9 по Новгородской области', '5321', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (582, 'Межрайонная инспекция Федеральной налоговой службы №1 по Новгородской области', '5331', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (583, 'Межрайонная инспекция Федеральной налоговой службы №2 по Новгородской области', '5332', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (584, 'Межрайонная инспекция Федеральной налоговой службы №6 по Новгородской области', '5336', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (585, 'Управление Федеральной налоговой службы по Новосибирской области', '5400', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (586, 'Инспекция Федеральной налоговой службы по Дзержинскому району г. Новосибирска', '5401', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (587, 'Инспекция Федеральной налоговой службы по Заельцовскому району г. Новосибирска', '5402', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (588, 'Инспекция Федеральной налоговой службы по Кировскому району г. Новосибирска', '5403', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (589, 'Инспекция Федеральной налоговой службы по Ленинскому району г. Новосибирска', '5404', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (590, 'Инспекция Федеральной налоговой службы по Октябрьскому району г. Новосибирска', '5405', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (591, 'Инспекция Федеральной налоговой службы по Центральному району г. Новосибирска', '5406', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (592, 'Инспекция Федеральной налоговой службы по Железнодорожному району г. Новосибирска', '5407', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (593, 'Инспекция Федеральной налоговой службы по Калининскому району г. Новосибирска', '5410', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (594, 'Межрайонная инспекция Федеральной налоговой службы № 6 по Новосибирской области', '5456', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (595, 'Межрайонная инспекция Федеральной налоговой службы по крупнейшим налогоплательщикам по Новосибирской области', '5460', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (596, 'Межрайонная инспекция Федеральной налоговой службы № 13 по г. Новосибирску', '5473', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (597, 'Межрайонная инспекция Федеральной налоговой службы № 14 по Новосибирской области', '5474', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (598, 'Межрайонная инспекция Федеральной налоговой службы № 15 по Новосибирской области', '5475', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (599, 'Межрайонная инспекция Федеральной налоговой службы № 16 по Новосибирской области', '5476', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (600, 'Межрайонная инспекция Федеральной налоговой службы № 3 по Новосибирской области', '5483', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (601, 'Межрайонная инспекция Федеральной налоговой службы № 5 по Новосибирской области', '5485', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (602, 'Управление Федеральной налоговой службы по Омской области', '5500', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (603, 'Инспекция Федеральной налоговой службы по Советскому административному округу г. Омска', '5501', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (604, 'Инспекция Федеральной налоговой службы № 1 по Центральному административному округу г. Омска', '5503', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (605, 'Инспекция Федеральной налоговой службы № 2 по Центральному административному округу г. Омска', '5504', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (606, 'Межрайонная инспекция Федеральной налоговой службы № 4 по Омской области', '5505', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (607, 'Инспекция Федеральной налоговой службы по Октябрьскому административному округу г. Омска', '5506', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (608, 'Инспекция Федеральной налоговой службы по Кировскому административному округу г. Омска', '5507', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (609, 'Межрайонная инспекция Федеральной налоговой службы № 6 по Омской области', '5509', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (610, 'Межрайонная инспекция Федеральной налоговой службы № 3 по Омской области', '5514', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (611, 'Межрайонная инспекция Федеральной налоговой службы № 1 по Омской области', '5515', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (612, 'Межрайонная инспекция Федеральной налоговой службы № 2 по Омской области', '5535', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (613, 'Межрайонная инспекция Федеральной налоговой службы по крупнейшим налогоплательщикам по Омской области', '5542', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (614, 'Межрайонная инспекция Федеральной налоговой службы № 12 по Омской области', '5543', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (615, 'Управление Федеральной налоговой службы по Оренбургской области', '5600', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (616, 'Межрайонная инспекция Федеральной налоговой службы №2 по Оренбургской области', '5601', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (617, 'Межрайонная инспекция Федеральной налоговой службы №1 по Оренбургской области', '5602', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (618, 'Межрайонная инспекция Федеральной налоговой службы №3 по Оренбургской области', '5603', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (619, 'Межрайонная инспекция Федеральной налоговой службы №8 по Оренбургской области', '5607', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (620, 'Инспекция Федеральной налоговой службы по Дзержинскому району г.Оренбурга', '5609', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (621, 'Инспекция Федеральной налоговой службы по Ленинскому району г.Оренбурга', '5610', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (622, 'Инспекция Федеральной налоговой службы по Промышленному району г.Оренбурга', '5611', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (623, 'Инспекция Федеральной налоговой службы по Центральному району г.Оренбурга', '5612', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (624, 'Межрайонная инспекция Федеральной налоговой службы по крупнейшим налогоплательщикам по Оренбургской области', '5613', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (625, 'Инспекция Федеральной налоговой службы по г.Орску Оренбургской области', '5614', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (626, 'Межрайонная инспекция Федеральной налоговой службы №4 по Оренбургской области', '5617', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (627, 'Межрайонная инспекция Федеральной налоговой службы №9 по Оренбургской области', '5635', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (628, 'Межрайонная инспекция Федеральной налоговой службы №6 по Оренбургской области', '5636', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (629, 'Межрайонная инспекция Федеральной налоговой службы №7 по Оренбургской области', '5638', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (630, 'Межрайонная инспекция Федеральной налоговой службы №5 по Оренбургской области', '5646', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (631, 'Межрайонная инспекция Федеральной налоговой службы №10 по Оренбургской области', '5658', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (632, 'Управление Федеральной налоговой службы по Орловской области', '5700', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (633, 'Инспекция Федеральной налоговой службы по г.Орлу', '5740', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (634, 'Межрайонная инспекция Федеральной налоговой службы №3 по Орловской области', '5743', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (635, 'Межрайонная инспекция Федеральной налоговой службы № 4 по Орловской области', '5744', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (636, 'Межрайонная инспекция Федеральной налоговой службы №5 по Орловской области', '5745', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (637, 'Межрайонная инспекция Федеральной налоговой службы №6 по Орловской области', '5746', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (638, 'Межрайонная инспекция Федеральной налоговой службы №8 по Орловской области', '5748', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (639, 'Межрайонная инспекция Федеральной налоговой службы №9 по Орловской области', '5749', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (640, 'Управление Федеральной налоговой службы по Пензенской области', '5800', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (641, 'Межрайонная инспекция Федеральной налоговой службы №2 по Пензенской области', '5802', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (642, 'Межрайонная инспекция Федеральной налоговой службы №1 по Пензенской области', '5803', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (643, 'Межрайонная инспекция Федеральной налоговой службы №4 по Пензенской области', '5805', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (644, 'Межрайонная инспекция Федеральной налоговой службы №3 по Пензенской области', '5809', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (645, 'Межрайонная инспекция Федеральной налоговой службы №5 по Пензенской области', '5826', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (646, 'Межрайонная инспекция Федеральной налоговой службы № 6 по Пензенской области', '5827', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (647, 'Инспекция Федеральной налоговой службы по Железнодорожному району г.Пензы', '5834', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (648, 'Инспекция Федеральной налоговой службы по Октябрьскому району г.Пензы', '5835', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (649, 'Инспекция Федеральной налоговой службы по Ленинскому району г.Пензы', '5836', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (650, 'Инспекция Федеральной налоговой службы по Первомайскому району г.Пензы', '5837', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (651, 'Инспекция Федеральной налоговой службы по г.Заречному Пензенской области', '5838', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (652, 'Управление Федеральной налоговой службы по Пермскому краю', '5900', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (653, 'Межрайонная инспекция Федеральной налоговой службы по крупнейшим налогоплательщикам по Пермскому краю', '5901', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (654, 'Инспекция Федеральной налоговой службы по Ленинскому району г. Перми', '5902', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (655, 'Инспекция Федеральной налоговой службы по Дзержинскому району г. Перми', '5903', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (656, 'Инспекция Федеральной налоговой службы по Свердловскому району г. Перми', '5904', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (657, 'Инспекция Федеральной налоговой службы по Индустриальному району г. Перми', '5905', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (658, 'Инспекция Федеральной налоговой службы по Мотовилихинскому району г. Перми', '5906', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (659, 'Межрайонная инспекция Федеральной налоговой службы № 9 по Пермскому краю', '5907', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (660, 'Инспекция Федеральной налоговой службы по Кировскому району г. Перми', '5908', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (661, 'Межрайонная инспекция Федеральной налоговой службы № 2 по Пермскому краю', '5911', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (662, 'Инспекция Федеральной налоговой службы по г. Добрянке Пермского края', '5914', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (663, 'Межрайонная инспекция Федеральной налоговой службы № 16 по Пермскому краю', '5916', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (664, 'Межрайонная инспекция Федеральной налоговой службы № 5 по Пермскому краю', '5917', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (665, 'Межрайонная инспекция Федеральной налоговой службы № 6 по Пермскому краю', '5918', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (666, 'Межрайонная инспекция Федеральной налоговой службы № 11 по Пермскому краю', '5919', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (667, 'Инспекция Федеральной налоговой службы по г. Чайковскому Пермского края', '5920', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (668, 'Межрайонная инспекция Федеральной налоговой службы № 14 по Пермскому краю', '5921', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (669, 'Межрайонная инспекция Федеральной налоговой службы № 3 по Пермскому краю', '5933', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (670, 'Межрайонная инспекция Федеральной налоговой службы № 8 по Пермскому краю', '5944', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (671, 'Межрайонная инспекция Федеральной налоговой службы № 10 по Пермскому краю', '5947', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (672, 'Инспекция Федеральной налоговой службы по Пермскому району Пермского края', '5948', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (673, 'Межрайонная инспекция Федеральной налоговой службы № 12 по Пермскому краю', '5951', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (674, 'Межрайонная инспекция Федеральной налоговой службы № 13 по Пермскому краю', '5957', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (675, 'Межрайонная инспекция Федеральной налоговой службы № 17 по Пермскому краю', '5958', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (676, 'Межрайонная инспекция Федеральной налоговой службы № 1 по Пермскому краю', '5981', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (677, 'Управление Федеральной налоговой службы по Псковской области', '6000', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (678, 'Межрайонная инспекция Федеральной налоговой службы № 6 по Псковской области', '6009', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (679, 'Межрайонная инспекция Федеральной налоговой службы № 2 по Псковской области', '6025', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (680, 'Межрайонная инспекция Федеральной налоговой службы № 1 по Псковской области', '6027', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (681, 'Межрайонная инспекция Федеральной налоговой службы № 3 по Псковской области', '6030', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (682, 'Межрайонная инспекция Федеральной налоговой службы № 4 по Псковской области', '6031', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (683, 'Межрайонная инспекция Федеральной налоговой службы № 5 по Псковской области', '6032', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (684, 'Управление Федеральной налоговой службы по Ростовской области', '6100', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (685, 'Межрайонная инспекция Федеральной налоговой службы по крупнейшим налогоплательщикам по Ростовской области', '6152', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (686, 'Инспекция Федеральной налоговой службы по г.Таганрогу Ростовской области', '6154', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (687, 'Инспекция Федеральной налоговой службы по Ленинскому району г.Ростова-на-Дону', '6164', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (865, 'Управление Федеральной налоговой службы по Забайкальскому краю', '7500', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (866, 'Межрайонная инспекция Федеральной налоговой службы № 5 по Забайкальскому краю', '7505', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (867, 'Межрайонная инспекция Федеральной налоговой службы № 6 по Забайкальскому краю', '7513', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (868, 'Межрайонная инспекция Федеральной налоговой службы № 3 по Забайкальскому краю', '7524', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (869, 'Межрайонная инспекция Федеральной налоговой службы № 7 по Забайкальскому краю', '7527', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (870, 'Межрайонная инспекция Федеральной налоговой службы № 4 по Забайкальскому краю', '7530', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (871, 'Межрайонная инспекция Федеральной налоговой службы № 2 по г.Чите', '7536', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (872, 'Межрайонная инспекция Федеральной налоговой службы № 8 по Забайкальскому краю', '7538', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (873, 'Межрайонная инспекция Федеральной налоговой службы № 1 по Забайкальскому краю', '7580', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (874, 'Управление Федеральной налоговой службы по Ярославской области', '7600', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (875, 'Инспекция Федеральной налоговой службы по Дзержинскому району г.Ярославля', '7602', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (876, 'Инспекция Федеральной налоговой службы по Заволжскому району г.Ярославля', '7603', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (877, 'Межрайонная инспекция Федеральной налоговой службы №5 по Ярославской области', '7604', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (878, 'Инспекция Федеральной налоговой службы по Ленинскому району г.Ярославля', '7606', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (879, 'Межрайонная инспекция Федеральной налоговой службы №1 по Ярославской области', '7608', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (880, 'Межрайонная инспекция Федеральной налоговой службы №2 по Ярославской области', '7609', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (881, 'Межрайонная инспекция Федеральной налоговой службы №3 по Ярославской области', '7610', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (882, 'Межрайонная инспекция Федеральной налоговой службы №4 по Ярославской области', '7611', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (883, 'Межрайонная инспекция Федеральной налоговой службы №8 по Ярославской области', '7612', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (884, 'Межрайонная инспекция Федеральной налоговой службы №7 по Ярославской области', '7627', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (885, 'Управление Федеральной налоговой службы по г.Москве', '7700', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (886, 'Инспекция Федеральной налоговой службы № 1 по г.Москве', '7701', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (887, 'Инспекция Федеральной налоговой службы № 2 по г.Москве', '7702', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (888, 'Инспекция Федеральной налоговой службы № 3 по г.Москве', '7703', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (889, 'Инспекция Федеральной налоговой службы № 4 по г.Москве', '7704', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (890, 'Инспекция Федеральной налоговой службы № 5 по г. Москве', '7705', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (891, 'Инспекция Федеральной налоговой службы № 6 по г. Москве', '7706', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (892, 'Инспекция Федеральной налоговой службы № 7 по г. Москве', '7707', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (893, 'Инспекция Федеральной налоговой службы № 8 по г. Москве', '7708', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (894, 'Инспекция Федеральной налоговой службы № 9 по г.Москве', '7709', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (895, 'Инспекция Федеральной налоговой службы № 10 по г. Москве', '7710', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (896, 'Инспекция Федеральной налоговой службы № 13 по г.Москве', '7713', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (897, 'Инспекция Федеральной налоговой службы № 14 по г.Москве', '7714', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (898, 'Инспекция Федеральной налоговой службы № 15 по г. Москве', '7715', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (899, 'Инспекция Федеральной налоговой службы № 16 по г. Москве', '7716', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (900, 'Инспекция Федеральной налоговой службы № 17 по г.Москве', '7717', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (901, 'Инспекция Федеральной налоговой службы № 18 по г.Москве', '7718', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (902, 'Инспекция Федеральной налоговой службы № 19 по г.Москве', '7719', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (903, 'Инспекция Федеральной налоговой службы № 20 по г.Москве', '7720', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (904, 'Инспекция Федеральной налоговой службы № 21 по г.Москве', '7721', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (905, 'Инспекция Федеральной налоговой службы № 22 по г.Москве', '7722', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (906, 'Инспекция Федеральной налоговой службы № 23 по г.Москве', '7723', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (907, 'Инспекция Федеральной налоговой службы № 24 по г.Москве', '7724', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (908, 'Инспекция Федеральной налоговой службы № 25 по г.Москве', '7725', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (909, 'Инспекция Федеральной налоговой службы № 26 по г.Москве', '7726', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (910, 'Инспекция Федеральной налоговой службы № 27 по г.Москве', '7727', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (911, 'Инспекция Федеральной налоговой службы № 28 по г.Москве', '7728', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (912, 'Инспекция Федеральной налоговой службы № 29 по г. Москве', '7729', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (913, 'Инспекция Федеральной налоговой службы № 30 по г. Москве', '7730', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (914, 'Инспекция Федеральной налоговой службы № 31 по г.Москве', '7731', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (915, 'Инспекция Федеральной налоговой службы № 33 по г.Москве', '7733', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (916, 'Инспекция Федеральной налоговой службы № 34 по г.Москве', '7734', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (917, 'Инспекция Федеральной налоговой службы № 35 по г.Москве', '7735', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (918, 'Инспекция Федеральной налоговой службы № 36 по г.Москве', '7736', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (919, 'Инспекция Федеральной налоговой службы № 43 по г. Москве', '7743', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (920, 'Межрайонная инспекция Федеральной налоговой службы № 45 по г.Москве', '7745', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (921, 'Межрайонная инспекция Федеральной налоговой службы № 46 по г. Москве', '7746', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (922, 'Межрайонная инспекция Федеральной налоговой службы № 47 по г.Москве', '7747', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (923, 'Межрайонная инспекция Федеральной налоговой службы № 48 по г.Москве', '7748', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (924, 'Межрайонная инспекция Федеральной налоговой службы № 49 по г. Москве', '7749', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (925, 'Межрайонная инспекция Федеральной налоговой службы № 50 по г.Москве', '7750', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (926, 'Межрайонная инспекция Федеральной налоговой службы № 51 по г. Москве', '7751', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (927, 'Управление Федеральной налоговой службы по Санкт-Петербургу', '7800', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (928, 'Межрайонная инспекция Федеральной налоговой службы №16 по Санкт-Петербургу', '7801', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (929, 'Межрайонная инспекция Федеральной налоговой службы №17 по Санкт-Петербургу', '7802', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (930, 'Межрайонная инспекция Федеральной налоговой службы №18 по Санкт-Петербургу', '7804', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (931, 'Межрайонная инспекция Федеральной налоговой службы №19 по Санкт-Петербургу', '7805', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (932, 'Межрайонная инспекция Федеральной налоговой службы №21 по Санкт-Петербургу', '7806', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (933, 'Межрайонная инспекция Федеральной налоговой службы №22 по Санкт-Петербургу', '7807', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (934, 'Межрайонная инспекция Федеральной налоговой службы №23 по Санкт-Петербургу', '7810', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (935, 'Межрайонная инспекция Федеральной налоговой службы №24 по Санкт-Петербургу', '7811', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (936, 'Межрайонная инспекция Федеральной налоговой службы №25 по Санкт-Петербургу', '7813', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (937, 'Межрайонная инспекция Федеральной налоговой службы №26 по Санкт-Петербургу', '7814', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (938, 'Межрайонная инспекция Федеральной налоговой службы №27 по Санкт-Петербургу', '7816', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (939, 'Межрайонная инспекция Федеральной налоговой службы №20 по Санкт-Петербургу', '7817', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (940, 'Межрайонная инспекция Федеральной налоговой службы №3 по Санкт-Петербургу', '7819', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (941, 'Межрайонная инспекция Федеральной налоговой службы №2 по Санкт-Петербургу', '7820', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (942, 'Межрайонная инспекция Федеральной налоговой службы по крупнейшим налогоплательщикам №1 по Санкт-Петербургу', '7834', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (943, 'Межрайонная инспекция Федеральной налоговой службы №4 по Санкт-Петербургу', '7835', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (944, 'Межрайонная инспекция Федеральной налоговой службы №6 по Санкт-Петербургу', '7837', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (945, 'Межрайонная инспекция Федеральной налоговой службы №7 по Санкт-Петербургу', '7838', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (946, 'Межрайонная инспекция Федеральной налоговой службы №8 по Санкт-Петербургу', '7839', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (947, 'Межрайонная инспекция Федеральной налоговой службы №9 по Санкт-Петербургу', '7840', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (948, 'Межрайонная инспекция Федеральной налоговой службы №10 по Санкт-Петербургу', '7841', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (949, 'Межрайонная инспекция Федеральной налоговой службы №11 по Санкт-Петербургу', '7842', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (950, 'Межрайонная инспекция Федеральной налоговой службы №12 по Санкт-Петербургу', '7843', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (951, 'Межрайонная инспекция Федеральной налоговой службы №15 по Санкт-Петербургу', '7847', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (952, 'Межрайонная инспекция Федеральной налоговой службы №28 по Санкт-Петербургу', '7848', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (953, 'Межрайонная инспекция Федеральной налоговой службы по крупнейшим налогоплательщикам №2 по Санкт-Петербургу', '7850', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (954, 'Управление Федеральной налоговой службы по Еврейской автономной области', '7900', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (955, 'Инспекция Федеральной налоговой службы по г.Биробиджану Еврейской автономной области', '7901', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (956, 'Межрайонная инспекция Федеральной налоговой службы №1 по Еврейской автономной области', '7907', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (957, 'Управление Федеральной налоговой службы по Ханты-Мансийскому автономному округу - Югре', '8600', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (958, 'Межрайонная инспекция Федеральной налоговой службы № 1 по Ханты-Мансийскому автономному округу - Югре', '8601', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (959, 'Инспекция Федеральной налоговой службы по г. Сургуту Ханты-Мансийского автономного округа - Югры', '8602', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (960, 'Межрайонная инспекция Федеральной налоговой службы № 6 по Ханты-Мансийскому автономному округу - Югре', '8603', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (961, 'Межрайонная инспекция Федеральной налоговой службы № 2 по Ханты-Мансийскому автономному округу - Югре', '8606', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (962, 'Межрайонная инспекция Федеральной налоговой службы № 5 по Ханты-Мансийскому автономному округу - Югре', '8607', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (963, 'Инспекция Федеральной налоговой службы по г. Когалыму Ханты-Мансийского автономного округа - Югры', '8608', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (964, 'Межрайонная инспекция Федеральной налоговой службы № 3 по Ханты-Мансийскому автономному округу - Югре', '8610', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (965, 'Межрайонная инспекция Федеральной налоговой службы № 8 по Ханты-Мансийскому автономному округу - Югре', '8611', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (966, 'Инспекция Федеральной налоговой службы по Сургутскому району Ханты-Мансийского автономного округа - Югры', '8617', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (967, 'Межрайонная инспекция Федеральной налоговой службы № 7 по Ханты-Мансийскому автономному округу - Югре', '8619', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (968, 'Межрайонная инспекция Федеральной налоговой службы № 4 по Ханты-Мансийскому округу - Югре', '8622', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (969, 'Межрайонная инспекция Федеральной налоговой службы по крупнейшим налогоплательщикам по Ханты-Мансийскому автономному округу - Югре', '8624', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (970, 'Управление Федеральной налоговой службы по Чукотскому автономному округу', '8700', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (971, 'Межрайонная инспекция Федеральной налоговой службы № 2 по Чукотскому автономному округу', '8706', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (972, 'Межрайонная инспекция Федеральной налоговой службы № 1 по Чукотскому автономному округу', '8709', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (973, 'Управление Федеральной налоговой службы по Ямало-Ненецкому автономному округу', '8900', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (974, 'Межрайонная инспекция Федеральной налоговой службы № 1 по Ямало-Ненецкому автономному округу', '8901', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (975, 'Межрайонная инспекция Федеральной налоговой службы № 4 по Ямало-Ненецкому автономному округу', '8903', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (976, 'Межрайонная инспекция Федеральной налоговой службы №2 по Ямало-Ненецкому автономному округу', '8904', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (977, 'Межрайонная инспекция Федеральной налоговой службы №5 по Ямало-Ненецкому автономному округу', '8905', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (978, 'Межрайонная инспекция Федеральной налоговой службы № 3 по Ямало-Ненецкому автономному округу', '8911', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (979, 'Межрайонная инспекция Федеральной налоговой службы по крупнейшим налогоплательщикам по Ямало-Ненецкому автономному округу', '8914', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (980, 'Управление Федеральной налоговой службы по Республике Крым', '9100', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (981, 'Инспекция Федеральной налоговой службы по г. Алуште Республики Крым', '9101', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (982, 'Инспекция Федеральной налоговой службы по г.Симферополю Республики Крым', '9102', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (983, 'Инспекция Федеральной налоговой службы по г. Ялте Республики Крым', '9103', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (984, 'Инспекция Федеральной налоговой службы по Бахчисарайскому району Республики Крым', '9104', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (985, 'Межрайонная инспекция Федеральной налоговой службы № 1 по Республике Крым', '9105', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (986, 'Межрайонная инспекция Федеральной налоговой службы № 2 по Республике Крым', '9106', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (987, 'Межрайонная инспекция Федеральной налоговой службы № 3 по Республике Крым', '9107', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (988, 'Межрайонная инспекция Федеральной налоговой службы № 4 по Республике Крым', '9108', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (989, 'Межрайонная инспекция Федеральной налоговой службы № 5 по Республике Крым', '9109', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (990, 'Межрайонная инспекция Федеральной налоговой службы № 6 по Республике Крым', '9110', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (991, 'Межрайонная инспекция Федеральной налоговой службы № 7 по Республике Крым', '9111', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (992, 'Управление Федеральной налоговой службы по г. Севастополю', '9200', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (993, 'Инспекция Федеральной налоговой службы по Гагаринскому району г. Севастополя', '9201', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (994, 'Инспекция Федеральной налоговой службы по Балаклавскому району г. Севастополя', '9202', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (995, 'Инспекция Федеральной налоговой службы по Нахимовскому району г. Севастополя', '9203', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (996, 'Инспекция Федеральной налоговой службы по Ленинскому району г.Севастополя', '9204', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (997, 'Другие налоговые органы, подчиненные ФНС России', '9900', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (998, 'Инспекция Федеральной налоговой службы по городу и космодрому Байконуру', '9901', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (999, 'Межрегиональная инспекция Федеральной налоговой службы по Центральному федеральному округу', '9951', 'Y');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (1000, 'Межрегиональная инспекция Федеральной налоговой службы по Северо-Западному федеральному округу', '9952', 'Y');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (1001, 'Межрегиональная инспекция Федеральной налоговой службы по Южному федеральному округу', '9953', 'Y');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (1002, 'Межрегиональная инспекция Федеральной налоговой службы по Приволжскому федеральному округу', '9954', 'Y');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (1003, 'Межрегиональная инспекция Федеральной налоговой службы по Уральскому федеральному округу', '9955', 'Y');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (1004, 'Межрегиональная инспекция Федеральной налоговой службы по Сибирскому федеральному округу', '9956', 'Y');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (1005, 'Межрегиональная инспекция Федеральной налоговой службы по Дальневосточному федеральному округу', '9957', 'Y');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (1006, 'Межрегиональная инспекция Федеральной налоговой службы по Северо-Кавказскому федеральному округу', '9958', 'Y');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (1007, 'Межрегиональная инспекция Федеральной налоговой службы по Крымскому федеральному округу', '9959', 'Y');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (1008, 'МИ ФНС России по камеральному контролю', '9962', 'Y');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (1009, 'Межрегиональная инспекция Федеральной налоговой службы по централизованной обработке данных', '9965', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (1010, 'Межрегиональная инспекция Федеральной налоговой службы по крупнейшим налогоплательщикам № 1', '9971', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (1011, 'Межрегиональная инспекция Федеральной налоговой службы по крупнейшим налогоплательщикам № 2', '9972', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (1012, 'Межрегиональная инспекция Федеральной налоговой службы по крупнейшим налогоплательщикам № 3', '9973', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (1013, 'Межрегиональная инспекция Федеральной налоговой службы по крупнейшим налогоплательщикам № 4', '9974', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (1014, 'Межрегиональная инспекция Федеральной налоговой службы по крупнейшим налогоплательщикам № 5', '9975', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (1015, 'Межрегиональная инспекция Федеральной налоговой службы по крупнейшим налогоплательщикам № 6', '9976', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (1016, 'Межрегиональная инспекция Федеральной налоговой службы по крупнейшим налогоплательщикам № 7', '9977', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (1017, 'Межрегиональная инспекция Федеральной налоговой службы по крупнейшим налогоплательщикам № 8', '9978', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (1018, 'Межрегиональная инспекция Федеральной налоговой службы по крупнейшим налогоплательщикам № 9', '9979', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (1019, 'Федеральная налоговая служба (Р)', '9998', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (1020, 'МИ ФНС России по ценообразованию для целей налогообложения', '9961', 'Y');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (1021, 'Инспекция Федеральной налоговой службы по г. Чайковскому Пермского края', '5959', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (1022, 'Инспекция Федеральной налоговой службы по Курганинскому району Краснодарского края', '2377', 'N');
insert into NDS2_MRR_USER.CFG_INSPECTION_GROUP_N (id, name, sono_code, all_inspections_available)
values (1023, 'Инспекция Федеральной налоговой службы по г. Крымску Краснодарского края', '2376', 'N');
commit;
prompt Loading CFG_INSPECTION_GROUP_N...Done.
