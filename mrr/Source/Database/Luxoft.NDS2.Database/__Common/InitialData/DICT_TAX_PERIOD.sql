﻿truncate table NDS2_MRR_USER.DICT_TAX_PERIOD;
insert into NDS2_MRR_USER.dict_tax_period (code, period, description, quarter, is_main_in_quarter, sort_order, month, effective_value) 
values ('01', '01', 'январь', 1, 0, 10, 1, 101);
insert into NDS2_MRR_USER.dict_tax_period (code, period, description, quarter, is_main_in_quarter, sort_order, month, effective_value) 
values ('02', '02', 'февраль', 1, 0, 20, 2, 102);
insert into NDS2_MRR_USER.dict_tax_period (code, period, description, quarter, is_main_in_quarter, sort_order, month, effective_value) 
values ('03', '03', 'март', 1, 0, 30, 3, 103);
insert into NDS2_MRR_USER.dict_tax_period (code, period, description, quarter, is_main_in_quarter, sort_order, month, effective_value) 
values ('04', '04', 'апрель', 2, 0, 40, 4, 104);
insert into NDS2_MRR_USER.dict_tax_period (code, period, description, quarter, is_main_in_quarter, sort_order, month, effective_value) 
values ('05', '05', 'май', 2, 0, 50, 5, 105);
insert into NDS2_MRR_USER.dict_tax_period (code, period, description, quarter, is_main_in_quarter, sort_order, month, effective_value) 
values ('06', '06', 'июнь', 2, 0, 60, 6, 106);
insert into NDS2_MRR_USER.dict_tax_period (code, period, description, quarter, is_main_in_quarter, sort_order, month, effective_value) 
values ('07', '07', 'июль', 3, 0, 70, 7, 107);
insert into NDS2_MRR_USER.dict_tax_period (code, period, description, quarter, is_main_in_quarter, sort_order, month, effective_value) 
values ('08', '08', 'август', 3, 0, 80, 8, 108);
insert into NDS2_MRR_USER.dict_tax_period (code, period, description, quarter, is_main_in_quarter, sort_order, month, effective_value) 
values ('09', '09', 'сентябрь', 3, 0, 90, 9, 109);
insert into NDS2_MRR_USER.dict_tax_period (code, period, description, quarter, is_main_in_quarter, sort_order, month, effective_value) 
values ('10', '10', 'октябрь', 4, 0, 100, 10, 110);
insert into NDS2_MRR_USER.dict_tax_period (code, period, description, quarter, is_main_in_quarter, sort_order, month, effective_value) 
values ('11', '11', 'ноябрь', 4, 0, 110, 11, 111);
insert into NDS2_MRR_USER.dict_tax_period (code, period, description, quarter, is_main_in_quarter, sort_order, month, effective_value) 
values ('12', '12', 'декабрь', 4, 0, 120, 12, 112);
insert into NDS2_MRR_USER.dict_tax_period (code, period, description, quarter, is_main_in_quarter, sort_order, month, effective_value) 
values ('21', '21', '1 кв.', 1, 1, 35, 0, 1);
insert into NDS2_MRR_USER.dict_tax_period (code, period, description, quarter, is_main_in_quarter, sort_order, month, effective_value) 
values ('22', '22', '2 кв.', 2, 1, 65, 0, 2);
insert into NDS2_MRR_USER.dict_tax_period (code, period, description, quarter, is_main_in_quarter, sort_order, month, effective_value) 
values ('23', '23', '3 кв.', 3, 1, 95, 0, 3);
insert into NDS2_MRR_USER.dict_tax_period (code, period, description, quarter, is_main_in_quarter, sort_order, month, effective_value) 
values ('24', '24', '4 кв.', 4, 1, 125, 0, 4);
insert into NDS2_MRR_USER.dict_tax_period (code, period, description, quarter, is_main_in_quarter, sort_order, month, effective_value) 
values ('51', '21', '1 кв.', 1, 0, 35, 0, 1);
insert into NDS2_MRR_USER.dict_tax_period (code, period, description, quarter, is_main_in_quarter, sort_order, month, effective_value) 
values ('54', '24', '2 кв.', 2, 0, 65, 0, 2);
insert into NDS2_MRR_USER.dict_tax_period (code, period, description, quarter, is_main_in_quarter, sort_order, month, effective_value) 
values ('55', '25', '3 кв.', 3, 0, 95, 0, 3);
insert into NDS2_MRR_USER.dict_tax_period (code, period, description, quarter, is_main_in_quarter, sort_order, month, effective_value) 
values ('56', '26', '4 кв.', 4, 0, 125, 0, 4);
insert into NDS2_MRR_USER.dict_tax_period (code, period, description, quarter, is_main_in_quarter, sort_order, month, effective_value) 
values ('71', '01', 'январь', 1, 0, 10, 1, 101);
insert into NDS2_MRR_USER.dict_tax_period (code, period, description, quarter, is_main_in_quarter, sort_order, month, effective_value) 
values ('72', '02', 'февраль', 1, 0, 20, 2, 102);
insert into NDS2_MRR_USER.dict_tax_period (code, period, description, quarter, is_main_in_quarter, sort_order, month, effective_value) 
values ('73', '03', 'март', 1, 0, 30, 3, 103);
insert into NDS2_MRR_USER.dict_tax_period (code, period, description, quarter, is_main_in_quarter, sort_order, month, effective_value) 
values ('74', '04', 'апрель', 2, 0, 40, 4, 104);
insert into NDS2_MRR_USER.dict_tax_period (code, period, description, quarter, is_main_in_quarter, sort_order, month, effective_value) 
values ('75', '05', 'май', 2, 0, 50, 5, 105);
insert into NDS2_MRR_USER.dict_tax_period (code, period, description, quarter, is_main_in_quarter, sort_order, month, effective_value) 
values ('76', '06', 'июнь', 2, 0, 60, 6, 106);
insert into NDS2_MRR_USER.dict_tax_period (code, period, description, quarter, is_main_in_quarter, sort_order, month, effective_value) 
values ('77', '07', 'июль', 3, 0, 70, 7, 107);
insert into NDS2_MRR_USER.dict_tax_period (code, period, description, quarter, is_main_in_quarter, sort_order, month, effective_value) 
values ('78', '08', 'август', 3, 0, 80, 8, 108);
insert into NDS2_MRR_USER.dict_tax_period (code, period, description, quarter, is_main_in_quarter, sort_order, month, effective_value) 
values ('79', '09', 'сентябрь', 3, 0, 90, 9, 109);
insert into NDS2_MRR_USER.dict_tax_period (code, period, description, quarter, is_main_in_quarter, sort_order, month, effective_value) 
values ('80', '10', 'октябрь', 4, 0, 100, 10, 110);
insert into NDS2_MRR_USER.dict_tax_period (code, period, description, quarter, is_main_in_quarter, sort_order, month, effective_value) 
values ('81', '11', 'ноябрь', 4, 0, 110, 11, 111);
insert into NDS2_MRR_USER.dict_tax_period (code, period, description, quarter, is_main_in_quarter, sort_order, month, effective_value) 
values ('82', '12', 'декабрь', 4, 0, 120, 12, 112);
commit;
