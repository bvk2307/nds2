﻿merge into NDS2_MRR_USER.VERSION_HISTORY t
using (select '8.14.3.1' as VERSION_NUMBER from dual) s
   on (t.VERSION_NUMBER = s.VERSION_NUMBER)
when not matched then
   insert (VERSION_NUMBER, RELEASE_DATE, CATALOG_REINDEX_REQUIRED, RELEASE_FILE_NAME)
   values (s.VERSION_NUMBER, sysdate, 1, null);
commit;
