﻿truncate table NDS2_MRR_USER.DOC_CLOSE_REASON;
insert into NDS2_MRR_USER.DOC_CLOSE_REASON (id, description)
values (1, 'вышел срок отработки АТ (дедлайн)');
insert into NDS2_MRR_USER.DOC_CLOSE_REASON (id, description)
values (2, 'корректировка с которой связана АТ стала неактуальной');
insert into NDS2_MRR_USER.DOC_CLOSE_REASON (id, description)
values (3, 'закрылась КНП по декларации (ЭОД4)');
insert into nds2_mrr_user.doc_close_reason (ID, DESCRIPTION) 
values (5, 'Корректировка аннулирована');
commit;
