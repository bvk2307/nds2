﻿truncate table NDS2_MRR_USER.DICT_DISCREPANCY_TYPE;

insert into NDS2_MRR_USER.DICT_DISCREPANCY_TYPE (s_code, s_name, description, caption)
values (1, 'Разрыв', 'Разрыв', 'Расхождение вида разрыв');
insert into NDS2_MRR_USER.DICT_DISCREPANCY_TYPE (s_code, s_name, description, caption)
values (4, 'НДС', 'Проверка НДС', 'Расхождение по НДС');

commit;
