﻿alter table NDS2_MRR_USER.SELECTION_FILTER_PARAMETER disable constraint FK$SEL_FILTER_PARAM_AREA;
alter table NDS2_MRR_USER.SELECTION_FILTER_PARAMETER disable constraint FK$SEL_FILTER_PARAM_MODEL;
alter table NDS2_MRR_USER.SELECTION_FILTER_PARAMETER disable constraint FK$SEL_FILTER_PARAM_TYPE;
alter table NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY disable constraint FK$SEL_FILTER_APPLICABILITY;
alter table NDS2_MRR_USER.SELECTION_FILTER_OPTION disable constraint FK$SEL_FILTER_OPTION_PARAM;

truncate table NDS2_MRR_USER.SELECTION_FILTER_OPTION;
truncate table NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY;
truncate table NDS2_MRR_USER.SELECTION_FILTER_PARAMETER;
truncate table NDS2_MRR_USER.SELECTION_FILTER_EDITOR_MODEL;
truncate table NDS2_MRR_USER.SELECTION_FILTER_EDITOR_TYPE;
truncate table NDS2_MRR_USER.SELECTION_FILTER_EDITOR_AREA;

alter table NDS2_MRR_USER.SELECTION_FILTER_PARAMETER enable constraint FK$SEL_FILTER_PARAM_AREA;
alter table NDS2_MRR_USER.SELECTION_FILTER_PARAMETER enable constraint FK$SEL_FILTER_PARAM_MODEL;
alter table NDS2_MRR_USER.SELECTION_FILTER_PARAMETER enable constraint FK$SEL_FILTER_PARAM_TYPE;
alter table NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY enable constraint FK$SEL_FILTER_APPLICABILITY;
alter table NDS2_MRR_USER.SELECTION_FILTER_OPTION enable constraint FK$SEL_FILTER_OPTION_PARAM;

insert into NDS2_MRR_USER.SELECTION_FILTER_EDITOR_AREA (id, name)
values (1, 'BuyerSide');
insert into NDS2_MRR_USER.SELECTION_FILTER_EDITOR_AREA (id, name)
values (2, 'SellerSide');
insert into NDS2_MRR_USER.SELECTION_FILTER_EDITOR_AREA (id, name)
values (3, 'DiscrepancyParameters');

insert into NDS2_MRR_USER.SELECTION_FILTER_EDITOR_TYPE (ID, CLASS, DESCRIPTION)
values (1, 'RegionEditor', 'RegionEditor');
insert into NDS2_MRR_USER.SELECTION_FILTER_EDITOR_TYPE (ID, CLASS, DESCRIPTION)
values (2, 'TaxAuthorityEditor', 'TaxAuthorityEditor');
insert into NDS2_MRR_USER.SELECTION_FILTER_EDITOR_TYPE (ID, CLASS, DESCRIPTION)
values (3, 'SurEditor', 'SurEditor');
insert into NDS2_MRR_USER.SELECTION_FILTER_EDITOR_TYPE (ID, CLASS, DESCRIPTION)
values (4, 'PeriodEditor', 'PeriodEditor');
insert into NDS2_MRR_USER.SELECTION_FILTER_EDITOR_TYPE (ID, CLASS, DESCRIPTION)
values (5, 'OperationCodeEditor', 'OperationCodeEditor');
insert into NDS2_MRR_USER.SELECTION_FILTER_EDITOR_TYPE (ID, CLASS, DESCRIPTION)
values (6, 'InnEditor', 'InnEditor');
insert into NDS2_MRR_USER.SELECTION_FILTER_EDITOR_TYPE (ID, CLASS, DESCRIPTION)
values (7, 'CheckBoxEditor', 'CheckBoxEditor');
insert into NDS2_MRR_USER.SELECTION_FILTER_EDITOR_TYPE (ID, CLASS, DESCRIPTION)
values (8, 'ListMultiEditor', 'ListMultiEditor');
insert into NDS2_MRR_USER.SELECTION_FILTER_EDITOR_TYPE (ID, CLASS, DESCRIPTION)
values (9, 'UntitledCheckBoxEditor', 'UntitledCheckBoxEditor');
insert into NDS2_MRR_USER.SELECTION_FILTER_EDITOR_TYPE (ID, CLASS, DESCRIPTION)
values (10, 'IntRangeEditor', 'IntRangeEditor');
insert into NDS2_MRR_USER.SELECTION_FILTER_EDITOR_TYPE (ID, CLASS, DESCRIPTION)
values (11, 'SimpleListEditor', 'SimpleListEditor');
insert into NDS2_MRR_USER.SELECTION_FILTER_EDITOR_TYPE (ID, CLASS, DESCRIPTION)
values (12, 'DataRangeEditor', 'DataRangeEditor');
insert into NDS2_MRR_USER.SELECTION_FILTER_EDITOR_TYPE (ID, CLASS, DESCRIPTION)
values (13, 'UnEditor', 'UnEditor');
insert into NDS2_MRR_USER.SELECTION_FILTER_EDITOR_TYPE (ID, CLASS, DESCRIPTION)
values (14, 'NdsRangeEditor', 'NdsRangeEditor');
insert into NDS2_MRR_USER.SELECTION_FILTER_EDITOR_TYPE (ID, CLASS, DESCRIPTION)
values (15, 'WhiteListEditor', 'WhiteListEditor');
insert into NDS2_MRR_USER.SELECTION_FILTER_EDITOR_TYPE (ID, CLASS, DESCRIPTION)
values (16, 'MultiDiscrepancyAmountControl', 'MultiDiscrepancyAmountControl');

insert into NDS2_MRR_USER.SELECTION_FILTER_EDITOR_MODEL (ID, NAME)
values (1, 'RangeOfIntModel');
insert into NDS2_MRR_USER.SELECTION_FILTER_EDITOR_MODEL (ID, NAME)
values (2, 'RangeOfDateModel');
insert into NDS2_MRR_USER.SELECTION_FILTER_EDITOR_MODEL (ID, NAME)
values (3, 'MultiRangeItemOfIntModel');
insert into NDS2_MRR_USER.SELECTION_FILTER_EDITOR_MODEL (ID, NAME)
values (4, 'MultiRangeOfIntModel');
insert into NDS2_MRR_USER.SELECTION_FILTER_EDITOR_MODEL (ID, NAME)
values (5, 'RegionsModel');
insert into NDS2_MRR_USER.SELECTION_FILTER_EDITOR_MODEL (ID, NAME)
values (6, 'TaxAuthorityModel');
insert into NDS2_MRR_USER.SELECTION_FILTER_EDITOR_MODEL (ID, NAME)
values (7, 'ListOfStringModel');
insert into NDS2_MRR_USER.SELECTION_FILTER_EDITOR_MODEL (ID, NAME)
values (8, 'ListModel');
insert into NDS2_MRR_USER.SELECTION_FILTER_EDITOR_MODEL (ID, NAME)
values (9, 'LabelModel');
insert into NDS2_MRR_USER.SELECTION_FILTER_EDITOR_MODEL (ID, NAME)
values (10, 'MultiRangeOfNDSModel');
insert into NDS2_MRR_USER.SELECTION_FILTER_EDITOR_MODEL (ID, NAME)
values (11, 'DiscrepancyTypeModel');
insert into NDS2_MRR_USER.SELECTION_FILTER_EDITOR_MODEL (ID, NAME)
values (12, 'CompareRuleModel');
insert into NDS2_MRR_USER.SELECTION_FILTER_EDITOR_MODEL (ID, NAME)
values (13, 'OperationCodesListModel');
insert into NDS2_MRR_USER.SELECTION_FILTER_EDITOR_MODEL (ID, NAME)
values (14, 'PeriodsModel');
insert into NDS2_MRR_USER.SELECTION_FILTER_EDITOR_MODEL (ID, NAME)
values (15, 'InnFilterModel');
insert into NDS2_MRR_USER.SELECTION_FILTER_EDITOR_MODEL (ID, NAME)
values (16, 'WhiteListModel');
insert into NDS2_MRR_USER.SELECTION_FILTER_EDITOR_MODEL (ID, NAME)
values (17, 'MultiDiscrepancyTypeParameterModel');


-- область "Сторона покупателя"
insert into NDS2_MRR_USER.SELECTION_FILTER_PARAMETER (id, editor_name, aggregate_name, editor_area_id, editor_type_id, EDITOR_MODEL_ID)
values (1, 'Регион', 'region_code', 1, 1, 5);
insert into NDS2_MRR_USER.SELECTION_FILTER_PARAMETER (id, editor_name, aggregate_name, editor_area_id, editor_type_id, EDITOR_MODEL_ID)
values (2, 'Инспекция', 'sono_code', 1, 2, 6);
insert into NDS2_MRR_USER.SELECTION_FILTER_PARAMETER (id, editor_name, aggregate_name, editor_area_id, editor_type_id, EDITOR_MODEL_ID)
values (3, 'ИНН', 'decl_inn', 1, 6, 15);
insert into NDS2_MRR_USER.SELECTION_FILTER_PARAMETER (id, editor_name, aggregate_name, editor_area_id, editor_type_id, EDITOR_MODEL_ID)
values (4, 'Значение СУР', 'sur_code', 1, 3, 8);
insert into NDS2_MRR_USER.SELECTION_FILTER_PARAMETER (id, editor_name, aggregate_name, editor_area_id, editor_type_id, EDITOR_MODEL_ID)
values (5, 'Отчетный период', 'period_full', 1, 8, 14);    
insert into NDS2_MRR_USER.SELECTION_FILTER_PARAMETER (id, editor_name, aggregate_name, editor_area_id, editor_type_id, EDITOR_MODEL_ID)
values (6, 'Дата представления в НО', 'submit_date', 1, 12, 2); 
insert into NDS2_MRR_USER.SELECTION_FILTER_PARAMETER (id, editor_name, aggregate_name, editor_area_id, editor_type_id, EDITOR_MODEL_ID)
values (7, 'Список БС', 'decl_inn', 1, 15, 16); 
insert into NDS2_MRR_USER.SELECTION_FILTER_PARAMETER (id, editor_name, aggregate_name, editor_area_id, editor_type_id, EDITOR_MODEL_ID)
values (17, 'Признак «Крупнейший»', 'category', 1, 9, 8);
insert into NDS2_MRR_USER.SELECTION_FILTER_PARAMETER (id, editor_name, aggregate_name, editor_area_id, editor_type_id, EDITOR_MODEL_ID)
values (21, 'Признак НД', 'decl_nds', 1, 14, 10); 


insert into NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY(parameter_id, is_manual) values (1, 1);
insert into NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY(parameter_id, is_manual) values (2, 1);
insert into NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY(parameter_id, is_manual) values (3, 1);
insert into NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY(parameter_id, is_manual) values (4, 1);
insert into NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY(parameter_id, is_manual) values (5, 1);
insert into NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY(parameter_id, is_manual) values (6, 1);
insert into NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY(parameter_id, is_manual) values (17, 1);
insert into NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY(parameter_id, is_manual) values (21, 1);


insert into NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY(parameter_id, is_manual) values (4, 0);
insert into NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY(parameter_id, is_manual) values (5, 0);
insert into NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY(parameter_id, is_manual) values (6, 0);
insert into NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY(parameter_id, is_manual) values (7, 0);
insert into NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY(parameter_id, is_manual) values (17, 0);
insert into NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY(parameter_id, is_manual) values (21, 0);

-- область "Сторона продавца"
insert into NDS2_MRR_USER.SELECTION_FILTER_PARAMETER (id, editor_name, aggregate_name, editor_area_id, editor_type_id, EDITOR_MODEL_ID)
values (101, 'Регион', 'contractor_region_code', 2, 1, 5);
insert into NDS2_MRR_USER.SELECTION_FILTER_PARAMETER (id, editor_name, aggregate_name, editor_area_id, editor_type_id, EDITOR_MODEL_ID)
values (102, 'Инспекция', 'contractor_sono_code', 2, 2, 6);
insert into NDS2_MRR_USER.SELECTION_FILTER_PARAMETER (id, editor_name, aggregate_name, editor_area_id, editor_type_id, EDITOR_MODEL_ID)
values (103, 'ИНН', 'contractor_inn', 2, 6, 15);
insert into NDS2_MRR_USER.SELECTION_FILTER_PARAMETER (id, editor_name, aggregate_name, editor_area_id, editor_type_id, EDITOR_MODEL_ID)
values (104, 'Значение СУР', 'contractor_sur_code', 2, 3, 8);
insert into NDS2_MRR_USER.SELECTION_FILTER_PARAMETER (id, editor_name, aggregate_name, editor_area_id, editor_type_id, EDITOR_MODEL_ID)
values (105, 'Отчетный период', 'contractor_period_full', 2, 8, 14);
insert into NDS2_MRR_USER.SELECTION_FILTER_PARAMETER (id, editor_name, aggregate_name, editor_area_id, editor_type_id, EDITOR_MODEL_ID)
values (106, 'Дата представления в НО', 'contractor_submit_date', 2, 12, 2); 
insert into NDS2_MRR_USER.SELECTION_FILTER_PARAMETER (id, editor_name, aggregate_name, editor_area_id, editor_type_id, EDITOR_MODEL_ID)
values (107, 'Список БС', 'contractor_inn', 2, 15, 16); 
insert into NDS2_MRR_USER.SELECTION_FILTER_PARAMETER (id, editor_name, aggregate_name, editor_area_id, editor_type_id, EDITOR_MODEL_ID)
values (117, 'Признак «Крупнейший»', 'contractor_category', 2, 9, 8);
insert into NDS2_MRR_USER.SELECTION_FILTER_PARAMETER (id, editor_name, aggregate_name, editor_area_id, editor_type_id, EDITOR_MODEL_ID)
values (121, 'Признак НД', 'contractor_decl_nds', 2, 14, 10); 
insert into NDS2_MRR_USER.SELECTION_FILTER_PARAMETER (id, editor_name, aggregate_name, editor_area_id, editor_type_id, EDITOR_MODEL_ID)
values (122, 'Не представлена НД / Журнал', 'contractor_zip', 2, 9, 8); 


insert into NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY(parameter_id, is_manual) values (101, 1);
insert into NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY(parameter_id, is_manual) values (102, 1);
insert into NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY(parameter_id, is_manual) values (103, 1);
insert into NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY(parameter_id, is_manual) values (104, 1);
insert into NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY(parameter_id, is_manual) values (105, 1);
insert into NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY(parameter_id, is_manual) values (106, 1);
insert into NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY(parameter_id, is_manual) values (117, 1);
insert into NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY(parameter_id, is_manual) values (121, 1);
insert into NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY(parameter_id, is_manual) values (122, 1);

insert into NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY(parameter_id, is_manual) values (104, 0);
insert into NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY(parameter_id, is_manual) values (105, 0);
insert into NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY(parameter_id, is_manual) values (106, 0);
insert into NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY(parameter_id, is_manual) values (107, 0);
insert into NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY(parameter_id, is_manual) values (117, 0);
insert into NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY(parameter_id, is_manual) values (121, 0);
insert into NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY(parameter_id, is_manual) values (122, 0);

-- область "Параметры для отбора расхождений"
insert into NDS2_MRR_USER.SELECTION_FILTER_PARAMETER (id, editor_name, aggregate_name, editor_area_id, editor_type_id, EDITOR_MODEL_ID)
values (205, 'Тип расхождения', 'type_code', 3, 9, 11); 
insert into NDS2_MRR_USER.SELECTION_FILTER_PARAMETER (id, editor_name, aggregate_name, editor_area_id, editor_type_id, EDITOR_MODEL_ID)
values (206, 'Тип расхождения', 'type_code', 3, 16, 17);
insert into NDS2_MRR_USER.SELECTION_FILTER_PARAMETER (id, editor_name, aggregate_name, editor_area_id, editor_type_id, EDITOR_MODEL_ID)
values (207, 'Вид расхождения', 'multiple', 3, 9, 11); 
insert into NDS2_MRR_USER.SELECTION_FILTER_PARAMETER (id, editor_name, aggregate_name, editor_area_id, editor_type_id, EDITOR_MODEL_ID)
values (210, 'Правило сопоставления', 'compare_rule', 3, 8, 12); 
insert into NDS2_MRR_USER.SELECTION_FILTER_PARAMETER (id, editor_name, aggregate_name, editor_area_id, editor_type_id, EDITOR_MODEL_ID)
values (211, 'Источник  записи по стороне покупателя', 'invoice_chapter', 3, 11, 8); 
insert into NDS2_MRR_USER.SELECTION_FILTER_PARAMETER (id, editor_name, aggregate_name, editor_area_id, editor_type_id, EDITOR_MODEL_ID)
values (212, 'Источник записи по стороне продавца', 'contractor_invoice_chapter', 3, 11, 8); 
insert into NDS2_MRR_USER.SELECTION_FILTER_PARAMETER (id, editor_name, aggregate_name, editor_area_id, editor_type_id, EDITOR_MODEL_ID)
values (217, 'Код вида операции по стороне покупателя', 'kvo', 3, 5, 13);
insert into NDS2_MRR_USER.SELECTION_FILTER_PARAMETER (id, editor_name, aggregate_name, editor_area_id, editor_type_id, EDITOR_MODEL_ID)
values (218, 'Код вида операции по стороне продавца', 'contractor_kvo', 3, 5, 13);

insert into NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY(parameter_id, is_manual) values (206, 1);
insert into NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY(parameter_id, is_manual) values (207, 1);
insert into NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY(parameter_id, is_manual) values (210, 1);
insert into NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY(parameter_id, is_manual) values (211, 1);
insert into NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY(parameter_id, is_manual) values (212, 1);
insert into NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY(parameter_id, is_manual) values (217, 1);
insert into NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY(parameter_id, is_manual) values (218, 1);

insert into NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY(parameter_id, is_manual) values (205, 0);
insert into NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY(parameter_id, is_manual) values (207, 0);
insert into NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY(parameter_id, is_manual) values (210, 0);
insert into NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY(parameter_id, is_manual) values (211, 0);
insert into NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY(parameter_id, is_manual) values (212, 0);
insert into NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY(parameter_id, is_manual) values (217, 0);
insert into NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY(parameter_id, is_manual) values (218, 0);
--СУР
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (101, 4, '1', 'Высокий риск');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (102, 4, '2', 'Средний риск');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (103, 4, '3', 'Низкий риск');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (104, 4, '4', 'Неопределенный');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (111, 104, '1', 'Высокий риск');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (112, 104, '2', 'Средний риск');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (113, 104, '3', 'Низкий риск');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (114, 104, '4', 'Неопределенный');

--Отчетный период
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (201, 5, '212015', '1 кв. 2015');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (202, 5, '222015', '2 кв. 2015');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (203, 5, '232015', '3 кв. 2015');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (204, 5, '242015', '4 кв. 2015');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (205, 5, '212016', '1 кв. 2016');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (206, 5, '222016', '2 кв. 2016');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (207, 5, '232016', '3 кв. 2016');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (208, 5, '242016', '4 кв. 2016');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (209, 5, '212017', '1 кв. 2017');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (210, 5, '222017', '2 кв. 2017');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (211, 5, '232017', '3 кв. 2017');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (212, 5, '242017', '4 кв. 2017');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (213, 5, '212018', '1 кв. 2018');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (214, 5, '222018', '2 кв. 2018');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (215, 5, '232018', '3 кв. 2018');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (216, 5, '242018', '4 кв. 2018');


insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (301, 105, '212015', '1 кв. 2015');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (302, 105, '222015', '2 кв. 2015');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (303, 105, '232015', '3 кв. 2015');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (304, 105, '242015', '4 кв. 2015');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (305, 105, '212016', '1 кв. 2016');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (306, 105, '222016', '2 кв. 2016');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (307, 105, '232015', '3 кв. 2016');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (308, 105, '242016', '4 кв. 2016');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (309, 105, '212017', '1 кв. 2017');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (310, 105, '222017', '2 кв. 2017');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (311, 105, '232017', '3 кв. 2017');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (312, 105, '242017', '4 кв. 2017');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (313, 105, '212018', '1 кв. 2018');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (314, 105, '222018', '2 кв. 2018');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (315, 105, '232018', '3 кв. 2018');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (316, 105, '242018', '4 кв. 2018');

-- вид расхождения
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (401, 205, '1', 'Разрыв');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (404, 205, '4', 'Проверка НДС');

-- подвид расхождения
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (1201, 207, '1', 'Множественный');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (1202, 207, '0', 'Не множественный');

-- правила сопоставления
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (501, 210, '1', 'Правило 1');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (502, 210, '2', 'Правило 2');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (503, 210, '3', 'Правило 3');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (504, 210, '4', 'Правило 4');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (505, 210, '5', 'Правило 5');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (506, 210, '6', 'Правило 6');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (507, 210, '7', 'Правило 7');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (508, 210, '8', 'Правило 8');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (509, 210, '9', 'Правило 9');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (510, 210, '10', 'Правило 10');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (511, 210, '11', 'Правило 11');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (512, 210, '12', 'Правило 12');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (513, 210, '13', 'Правило 13');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (514, 210, '14', 'Правило 14');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (515, 210, '15', 'Правило 15');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (516, 210, '16', 'Правило 16');

-- Коды вида операции по стороне покупателя
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (601, 217, '23', '23. Приобретение услуг, офорcмленных бланками строгой, отчетности в случаях, предусмотренных пунктом 7 статьи 171 Налогового кодекса Российской Федерации.');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (602, 217, '24', '24. Регистрация счетов-фактур в книге покупок в случаях, предусмотренных абзацем вторым пункта 9 статьи 165 и пунктом 10 статьи 171 Налогового кодекса Российской Федерации.');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (603, 217, '25', '25. Регистрация счетов-фактур в книге покупок в отношении сумм налога, ранее восстановленных при совершении операций, облагаемых по налоговой ставке 0 процентов.');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (604, 217, '26', '26. Составление первичных учетных документов при реализации товаров (работ, услуг), имущественных прав лицам, не являющимся налогоплательщиками налога на добавленную стоимость, и налогоплательщикам, освобожденным от исполнения обязанностей налогоплательщика, связанной с исчислением и уплатой налога.');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (605, 217, '27', '27. Составление счета-фактуры на основании двух и более счетов-фактур при реализации и (или) приобретении товаров (работ, услуг), имущественных прав в случае, предусмотренном пунктом 3.1 статьи 169 Налогового кодекса Российской Федерации, а также получение указанного счета-фактуры налогоплательщиком.');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (606, 217, '28', '28. Составление счета-фактуры на основании двух и более счетов-фактур при получении оплаты, частичной оплаты в счет предстоящих поставок товаров (работ, услуг), имущественных прав, в случае, предусмотренном пунктом 3.1 статьи 169 Налогового кодекса Российской Федерации, а также получение указанного счета-фактуры налогоплательщиком.');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (607, 217, '1', '1. Отгрузка (передача) или приобретение товаров, работ, услуг (включая посреднические услуги), имущественных прав, за исключением операций, перечисленных по кодам 03, 04, 06, 10, 11, 13.');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (608, 217, '2', '2. Оплата, частичная оплата (полученная или переданная) в счет предстоящих поставок товаров (выполнения работ, оказания услуг (включая посреднические услуги)), передачи имущественных прав, за исключением операций, перечисленных по кодам 05, 06, 12.');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (609, 217, '3', '3. Возврат покупателем товаров продавцу или получение продавцом возвращенных покупателем товаров.');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (610, 217, '4', '4. Отгрузка (передача) или приобретение товаров, работ, услуг (за исключением посреднических услуг), имущественных прав на основе договора комиссии (агентского договора, в случае, если агент совершает действия от своего имени), за исключением операций, перечисленных по коду 06.');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (611, 217, '5', '5. Оплата, частичная оплата (полученная или переданная) в счет предстоящих поставок товаров (выполнения работ, оказания услуг (за исключением посреднических услуг)), передачи имущественных прав на основе договора комиссии (агентского договора, в случае, если агент совершает действия от своего имени), за исключением операций, перечисленных по коду 06.');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (612, 217, '6', '6. Операции, совершаемые налоговыми агентами, перечисленными в статье 161 Налогового кодекса Российской Федерации (Собрание законодательства Российской Федерации, 2000, N 32, ст. 3340; 2011, N 50, ст. 7359).');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
VALUES (626, 217, '7', '7. Операции, перечисленные в подпункте 2 пункта 1 статьи 146 Налогового кодекса Российской Федерации.');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (613, 217, '8', '8. Операции, перечисленные в подпункте 3 пункта 1 статьи 146 Налогового кодекса Российской Федерации.');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (614, 217, '9', '9. Получение сумм, указанных в статье 162 Налогового кодекса Российской Федерации.');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (615, 217, '10', '10. Отгрузка (передача) или получение товаров, работ, услуг, имущественных прав на безвозмездной основе.');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (616, 217, '11', '11. Отгрузка (передача) или приобретение товаров, имущественных прав, перечисленных в пунктах 3, 4, 5.1 статьи 154, в подпунктах 1 - 4 статьи 155 Налогового кодекса Российской Федерации.');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (617, 217, '12', '12. Оплата, частичная оплата (полученная или переданная) в счет предстоящих поставок товаров, передачи имущественных прав, перечисленных в пунктах 3, 4, 5.1 статьи 154, в подпунктах 1 - 4 статьи 155 Налогового кодекса Российской Федерации.');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (618, 217, '13', '13. Проведение подрядными организациями (застройщиками или техническими заказчиками) капитального строительства, модернизации (реконструкции) объектов недвижимости.');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (619, 217, '16', '16. Получение продавцом товаров, возвращенных покупателем, не являющимся налогоплательщиком налога на добавленную стоимость, за исключением операций, перечисленных по коду 17.');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (620, 217, '17', '17. Получение продавцом товаров, возвращенных покупателем-физическим лицом, оплаченных наличным расчетом.');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (621, 217, '18', '18. Составление или получение корректировочного счета-фактуры в связи с изменением стоимости отгруженных товаров (работ, услуг), переданных имущественных прав в сторону уменьшения, в том числе в случае уменьшения цен (тарифов) и (или) уменьшения количества (объема) отгруженных товаров (работ, услуг), переданных имущественных прав.');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (622, 217, '19', '19. Ввоз товаров на территорию Российской Федерации и иные территории, находящиеся под ее юрисдикцией, с территории государств Евразийского экономического союза.');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (623, 217, '20', '20. Ввоз товаров на территорию Российской Федерации и иные территории, находящиеся под ее юрисдикцией, в таможенных процедурах выпуска для внутреннего потребления, переработки для внутреннего потребления, временного ввоза и переработки вне таможенной территории.');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (624, 217, '21', '21. Операции по восстановлению сумм налога, перечисленные в пункте 8 статьи 145, пункте 3 статьи 170, статье 171.1 Налогового кодекса Российской Федерации, а также при совершении операций, облагаемых по налоговой ставке 0 процентов.');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (625, 217, '22', '22. Операции по возврату авансовых платежей в случаях, перечисленных в абзаце втором пункта 5 статьи 171, а также операции, перечисленные в пункте 6 статьи 172 Налогового кодекса Российской Федерации.');

insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (630, 217, '14', '14. Передача имущественных прав, перечисленных в пунктах 1-4 статьи 155 НК РФ');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (631, 217, '15', '15. Составление (получение) счета-фактуры комиссионером (агентом) при реализации (получении) товаров (работ, услуг), имущественных прав от своего имени, в которых отражены данные в отношении собственных товаров (работ, услуг), имущественных прав, и данные в отношении товаров (работ, услуг), имущественных прав, реализуемых (приобретаемых) по договору комиссии');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (632, 217, '29', '29. Корректировка реализации товаров (работ, услуг), передача имущественных прав предприятия в целом, как имущественного комплекса, на основании пункта 6 статьи 105.3 НК РФ ');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (633, 217, '30', '30. Отгрузка товаров, в отношении которых при таможенном декларировании был исчислен НДС в соответствии с абзацем первым подпункта 1.1. пункта 1 статьи 151 НК РФ ');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (634, 217, '31', '31. Операция по уплате сумм НДС, исчисленных при таможенном декларировании товаров, в случаях, предусмотренных абзацем вторым подпункта 1.1. пункта 1 статьи 151 НК РФ');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (635, 217, '32', '32. Принятие к вычету сумм налога на добавленную стоимость, уплаченных или подлежащих к уплате в случаях предусмотренных пунктом 14 статьи 171  НК РФ');


-- Коды вида операции по стороне продавца
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (701, 218, '23', '23. Приобретение услуг, офорcмленных бланками строгой, отчетности в случаях, предусмотренных пунктом 7 статьи 171 Налогового кодекса Российской Федерации.');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (702, 218, '24', '24. Регистрация счетов-фактур в книге покупок в случаях, предусмотренных абзацем вторым пункта 9 статьи 165 и пунктом 10 статьи 171 Налогового кодекса Российской Федерации.');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (703, 218, '25', '25. Регистрация счетов-фактур в книге покупок в отношении сумм налога, ранее восстановленных при совершении операций, облагаемых по налоговой ставке 0 процентов.');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (704, 218, '26', '26. Составление первичных учетных документов при реализации товаров (работ, услуг), имущественных прав лицам, не являющимся налогоплательщиками налога на добавленную стоимость, и налогоплательщикам, освобожденным от исполнения обязанностей налогоплательщика, связанной с исчислением и уплатой налога.');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (705, 218, '27', '27. Составление счета-фактуры на основании двух и более счетов-фактур при реализации и (или) приобретении товаров (работ, услуг), имущественных прав в случае, предусмотренном пунктом 3.1 статьи 169 Налогового кодекса Российской Федерации, а также получение указанного счета-фактуры налогоплательщиком.');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (706, 218, '28', '28. Составление счета-фактуры на основании двух и более счетов-фактур при получении оплаты, частичной оплаты в счет предстоящих поставок товаров (работ, услуг), имущественных прав, в случае, предусмотренном пунктом 3.1 статьи 169 Налогового кодекса Российской Федерации, а также получение указанного счета-фактуры налогоплательщиком.');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (707, 218, '1', '1. Отгрузка (передача) или приобретение товаров, работ, услуг (включая посреднические услуги), имущественных прав, за исключением операций, перечисленных по кодам 03, 04, 06, 10, 11, 13.');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (708, 218, '2', '2. Оплата, частичная оплата (полученная или переданная) в счет предстоящих поставок товаров (выполнения работ, оказания услуг (включая посреднические услуги)), передачи имущественных прав, за исключением операций, перечисленных по кодам 05, 06, 12.');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (709, 218, '3', '3. Возврат покупателем товаров продавцу или получение продавцом возвращенных покупателем товаров.');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (710, 218, '4', '4. Отгрузка (передача) или приобретение товаров, работ, услуг (за исключением посреднических услуг), имущественных прав на основе договора комиссии (агентского договора, в случае, если агент совершает действия от своего имени), за исключением операций, перечисленных по коду 06.');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (711, 218, '5', '5. Оплата, частичная оплата (полученная или переданная) в счет предстоящих поставок товаров (выполнения работ, оказания услуг (за исключением посреднических услуг)), передачи имущественных прав на основе договора комиссии (агентского договора, в случае, если агент совершает действия от своего имени), за исключением операций, перечисленных по коду 06.');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (712, 218, '6', '6. Операции, совершаемые налоговыми агентами, перечисленными в статье 161 Налогового кодекса Российской Федерации (Собрание законодательства Российской Федерации, 2000, N 32, ст. 3340; 2011, N 50, ст. 7359).');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (713, 218, '8', '8. Операции, перечисленные в подпункте 3 пункта 1 статьи 146 Налогового кодекса Российской Федерации.');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (714, 218, '9', '9. Получение сумм, указанных в статье 162 Налогового кодекса Российской Федерации.');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (715, 218, '10', '10. Отгрузка (передача) или получение товаров, работ, услуг, имущественных прав на безвозмездной основе.');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (716, 218, '11', '11. Отгрузка (передача) или приобретение товаров, имущественных прав, перечисленных в пунктах 3, 4, 5.1 статьи 154, в подпунктах 1 - 4 статьи 155 Налогового кодекса Российской Федерации.');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (717, 218, '12', '12. Оплата, частичная оплата (полученная или переданная) в счет предстоящих поставок товаров, передачи имущественных прав, перечисленных в пунктах 3, 4, 5.1 статьи 154, в подпунктах 1 - 4 статьи 155 Налогового кодекса Российской Федерации.');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (718, 218, '13', '13. Проведение подрядными организациями (застройщиками или техническими заказчиками) капитального строительства, модернизации (реконструкции) объектов недвижимости.');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (719, 218, '16', '16. Получение продавцом товаров, возвращенных покупателем, не являющимся налогоплательщиком налога на добавленную стоимость, за исключением операций, перечисленных по коду 17.');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (720, 218, '17', '17. Получение продавцом товаров, возвращенных покупателем-физическим лицом, оплаченных наличным расчетом.');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (721, 218, '18', '18. Составление или получение корректировочного счета-фактуры в связи с изменением стоимости отгруженных товаров (работ, услуг), переданных имущественных прав в сторону уменьшения, в том числе в случае уменьшения цен (тарифов) и (или) уменьшения количества (объема) отгруженных товаров (работ, услуг), переданных имущественных прав.');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (722, 218, '19', '19. Ввоз товаров на территорию Российской Федерации и иные территории, находящиеся под ее юрисдикцией, с территории государств Евразийского экономического союза.');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (723, 218, '20', '20. Ввоз товаров на территорию Российской Федерации и иные территории, находящиеся под ее юрисдикцией, в таможенных процедурах выпуска для внутреннего потребления, переработки для внутреннего потребления, временного ввоза и переработки вне таможенной территории.');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (724, 218, '21', '21. Операции по восстановлению сумм налога, перечисленные в пункте 8 статьи 145, пункте 3 статьи 170, статье 171.1 Налогового кодекса Российской Федерации, а также при совершении операций, облагаемых по налоговой ставке 0 процентов.');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (725, 218, '22', '22. Операции по возврату авансовых платежей в случаях, перечисленных в абзаце втором пункта 5 статьи 171, а также операции, перечисленные в пункте 6 статьи 172 Налогового кодекса Российской Федерации.');

insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (730, 218, '14', '14. Передача имущественных прав, перечисленных в пунктах 1-4 статьи 155 НК РФ');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (731, 218, '15', '15. Составление (получение) счета-фактуры комиссионером (агентом) при реализации (получении) товаров (работ, услуг), имущественных прав от своего имени, в которых отражены данные в отношении собственных товаров (работ, услуг), имущественных прав, и данные в отношении товаров (работ, услуг), имущественных прав, реализуемых (приобретаемых) по договору комиссии');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (732, 218, '29', '29. Корректировка реализации товаров (работ, услуг), передача имущественных прав предприятия в целом, как имущественного комплекса, на основании пункта 6 статьи 105.3 НК РФ ');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (733, 218, '30', '30. Отгрузка товаров, в отношении которых при таможенном декларировании был исчислен НДС в соответствии с абзацем первым подпункта 1.1. пункта 1 статьи 151 НК РФ ');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (734, 218, '31', '31. Операция по уплате сумм НДС, исчисленных при таможенном декларировании товаров, в случаях, предусмотренных абзацем вторым подпункта 1.1. пункта 1 статьи 151 НК РФ');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (735, 218, '32', '32. Принятие к вычету сумм налога на добавленную стоимость, уплаченных или подлежащих к уплате в случаях предусмотренных пунктом 14 статьи 171  НК РФ');


-- Источник записи по стороне покупателя
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (801, 211, '8', 'Книга покупок');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (802, 211, '10', 'Журнал выставленных СФ');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (803, 211, '11', 'Журнал полученных СФ');

-- Источник записи о СФ по стороне продавца
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (901, 212, '9', 'Книга продаж');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (902, 212, '10', 'Журнал выставленных СФ');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (903, 212, '11', 'Журнал полученных СФ');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (904, 212, '12', 'Раздел 12');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (905, 212, '99', 'Неизвестен');

-- Признак "Крупнейший"
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (1001, 17, '1', 'Крупнейший');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (1002, 17, '0', 'Не крупнейший');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (1101, 117, '1', 'Крупнейший');
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (1102, 117, '0', 'Не крупнейший');


-- Признак «Не представлена НД / Журнал»
insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (1501, 122, '1', 'Не представлена НД / Журнал');

commit;
