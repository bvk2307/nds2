﻿merge into nds2_mrr_user.configuration d
using (select 'decl_cancelation_wait_period' as parameter from dual) s on (s.parameter = d.parameter)
when not matched then
	insert values('decl_cancelation_wait_period', 5, 5, 'Период в днях, в течение которого заявка на аннулирование декларации активна');

commit;