﻿truncate table NDS2_MRR_USER.DICT_TAX_PAYER_SOLVENCY;
insert into NDS2_MRR_USER.DICT_TAX_PAYER_SOLVENCY values (1, 'Платёжеспособный');
insert into NDS2_MRR_USER.DICT_TAX_PAYER_SOLVENCY values (2, 'Неплатёжеспособный');
insert into NDS2_MRR_USER.DICT_TAX_PAYER_SOLVENCY values (3, 'Наличие основных средств');
commit;
