﻿truncate table NDS2_MRR_USER.CFG_INDEX;

insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (50, 'SELECTION_DISCREPANCY', 'IDX$SEL_DIS_SEL_ID', 'SELECTION_ID', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (51, 'SELECTION_DISCREPANCY', 'IDX$SEL_DIS_DID_1', 'DISCREPANCY_ID', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (52, 'SELECTION_DISCREPANCY', 'IDX$SEL_DIS_DIDINPROC', 'DISCREPANCY_ID, IS_IN_PROCESS', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (54, 'SELECTION_DISCREPANCY', 'IDX$SEL_DIS_SIDINPROC', 'SELECTION_ID, IS_IN_PROCESS', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (55, 'SELECTION_DISCREPANCY', 'IDX$SEL_DIS_ZIPINPROC', 'ZIP, IS_IN_PROCESS', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (115,'SELECTION_DECLARATION', 'IDX_SDECL', 'SELECTION_ID', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (116,'SELECTION_DECLARATION', 'IDX_SDECL_KEY', 'DECLARATION_ID, INN_DECLARANT, KPP_CALCULATED', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (117,'SELECTION_DECLARATION', 'IDX_SDECL_KEY', 'SELECTION_ID, IS_IN_PROCESS', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (1, 'SOV_DISCREPANCY_ALL', 'PK_SOV_DISCREPANCY_ALL', 'ID', 'PRIMARY');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (2, 'SOV_INVOICE_ALL', 'IDX_SIA_ACTUAL_ROW_KEY', 'ACTUAL_ROW_KEY', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (3, 'SOV_INVOICE_ALL', 'IDX_SIA_ROW_KEY', 'ROW_KEY', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (11, 'SOV_INVOICE_ALL', 'IDX_SIA_DISCREPANCY_ID', 'DISCREPANCY_ID', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (4, 'SOV_INVOICE_BUYER_ALL', 'IDX_SIAB_RID', 'ROW_KEY_ID', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (15, 'SOV_INVOICE_BUYER_ALL', 'IDX_SIAB_DISCREPANCY_ID', 'DISCREPANCY_ID', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (5, 'SOV_INVOICE_BUY_ACCEPT_ALL', 'IDX_SIABAA_RID', 'ROW_KEY_ID', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (14, 'SOV_INVOICE_BUY_ACCEPT_ALL', 'IDX_SIABAA_DISCREPANCY_ID', 'DISCREPANCY_ID', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (6, 'SOV_INVOICE_OPERATION_ALL', 'IDX_SIAOA_RID', 'ROW_KEY_ID', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (18, 'SOV_INVOICE_OPERATION_ALL', 'IDX_SIAOA_DISCREPANCY_ID', 'DISCREPANCY_ID', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (7, 'SOV_INVOICE_PAYMENT_DOC_ALL', 'IDX_SIAPD_RID', 'ROW_KEY_ID', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (22, 'SOV_INVOICE_PAYMENT_DOC_ALL', 'IDX_SIAPD_DISCREPANCY_ID', 'DISCREPANCY_ID', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (8, 'SOV_INVOICE_SELLER_ALL', 'IDX_SISA_ROW_KEY_ID', 'ROW_KEY_ID', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (9, 'SOV_INVOICE_SELLER_ALL', 'IDX_SISA_SELLER_INN', 'SELLER_INN', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (23, 'SOV_INVOICE_SELLER_ALL', 'IDX_SISA_DISCREPANCY_ID', 'DISCREPANCY_ID', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (10, 'DECLARATION_HISTORY', 'IX_DH_ZIP', 'ZIP', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (12, 'DECLARATION_HISTORY', 'IX_DH_CORR_GROUP_ID', 'INN_DECLARANT, INN_CONTRACTOR, KPP_EFFECTIVE, FISCAL_YEAR, PERIOD_CODE, TYPE_CODE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (13, 'DECLARATION_HISTORY', 'IX_DH_CORR_ID', 'INN_DECLARANT, KPP_EFFECTIVE, FISCAL_YEAR, PERIOD_EFFECTIVE, TYPE_CODE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (16, 'DECLARATION_HISTORY', 'IX_DH_SUBMIT_DATE', 'SUBMIT_DATE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (34, 'DECLARATION_HISTORY', 'IX_DH_SUBMISSION_DATE', 'SUBMISSION_DATE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (17, 'DECLARATION_HISTORY', 'IX_DH_KPP', 'KPP', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (19, 'DECLARATION_HISTORY', 'IX_DH_REGION', 'REGION_CODE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (20, 'DECLARATION_HISTORY', 'IX_DH_SONO', 'SONO_CODE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (31, 'DECLARATION_HISTORY', 'IX_DH_SONO', 'SONO_CODE_SUBMITED', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (21, 'DECLARATION_HISTORY', 'IX_DH_NDS_TOTAL', 'NDS_TOTAL', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (27, 'DECLARATION_HISTORY', 'IX_DH_INN_DECLARANT', 'INN_DECLARANT', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (28, 'DECLARATION_HISTORY', 'IX_DH_INN_CONTRACTOR', 'INN_CONTRACTOR', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (29, 'DECLARATION_HISTORY', 'IX_DH_REG_NUMBER', 'REG_NUMBER', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (30, 'DECLARATION_HISTORY', 'IX_DH_DOC_JOIN', 'REG_NUMBER, SONO_CODE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (32, 'DECLARATION_HISTORY', 'IX_DH_DOC_JOIN_ACTUAL', 'SONO_CODE_SUBMITED, REG_NUMBER, IS_ACTIVE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (33, 'DECLARATION_HISTORY', 'IX_DH_CORR_ID_INS_DATE', 'INN_DECLARANT, KPP_EFFECTIVE, FISCAL_YEAR, PERIOD_EFFECTIVE, TYPE_CODE, INSERT_DATE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (34, 'DECLARATION_HISTORY', 'IX_DH_INS_DATE_ACT', 'INSERT_DATE, IS_ACTIVE', 'NONUNIQUE');

insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (42, 'V$ASK_DECLANDJRNL', 'IDX_ASKDJ_ZIP', 'ZIP', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (43, 'V$ASK_DECLANDJRNL', 'IDX_PERIOD', 'PERIOD', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (44, 'CONTROL_RATIO_QUEUE', 'IX_CONTROL_RATIO_QUEUE', 'ID', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (47, 'SOV_DISCREPANCY', 'IDX_SD_INVCONTR_RK', 'INVOICE_CONTRACTOR_RK', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (48, 'SOV_DISCREPANCY', 'IDX_SOV_DIS_ID', 'ID', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (49, 'SOV_DISCREPANCY', 'SOV_DISCREPANCY_INVOICE_RK', 'INVOICE_RK', 'NONUNIQUE');


insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type) 
values (100,	'DECLARATION_EXPLAIN_SUMMARY',	'IDX_DECL_EXPL_SUMM_ID',	'DECL_ID', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type) 
values (101,	'DECLARATION_CHANGES',			'IDX_DECL_CHANGES_ID',		'DECL_ID', 'UNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type) 
values (102,	'DECLARATION_REQUEST_COUNTER',	'IDX_DECL_REQ_COUNTER',		'REF_ENTITY_ID, SONO_CODE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type) 
values (103,	'KNP_DECLARATION_SUMMARY',	'IDX_KNP_DECL_SUM_PK',		'INN, KPP_EFFECTIVE, TYPE_CODE, PERIOD_CODE, FISCAL_YEAR', 'UNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type) 
values (111,	'SOV_DISCREPANCY', 'IDX_SOV_DIS_ID_STATUS', 'ID, status', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (112,	'STAGE_EGRN_UL', 'IDX_EGRNUL_INN', 'INN', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (113,	'STAGE_EGRN_IP', 'IDX_EGRNIP_INN', 'INNFL', 'NONUNIQUE');

-- HRZ_RELATIONS
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type) 
values (120,  'HRZ_RELATIONS', 'IX_RELATIONS_BA', 'YEAR, QUARTER, INN_1, KPP_1_EFFECTIVE, NBA_ORDER', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type) 
values (121,  'HRZ_RELATIONS', 'IX_RELATIONS_CH10_AMNT', 'YEAR, QUARTER, INN_1, KPP_1_EFFECTIVE, CHAPTER10_AMOUNT', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type) 
values (122,  'HRZ_RELATIONS', 'IX_RELATIONS_CH11_AMNT', 'YEAR, QUARTER, INN_1, KPP_1_EFFECTIVE, CHAPTER11_AMOUN', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type) 
values (123,  'HRZ_RELATIONS', 'IX_RELATIONS_CH12_AMNT', 'YEAR, QUARTER, INN_1, KPP_1_EFFECTIVE, CHAPTER12_AMOUNT', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type) 
values (124,  'HRZ_RELATIONS', 'IX_RELATIONS_CH8_AMNT', 'YEAR, QUARTER, INN_1, KPP_1_EFFECTIVE, CHAPTER8_AMOUNT', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type) 
values (125,  'HRZ_RELATIONS', 'IX_RELATIONS_CH9_AMNT', 'YEAR, QUARTER, INN_1, KPP_1_EFFECTIVE, CHAPTER9_AMOUNT', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type) 
values (126,  'HRZ_RELATIONS', 'IX_RELATIONS_NDS_CH12', 'YEAR, QUARTER, INN_1, KPP_1_EFFECTIVE, NDS_CHAPTER12', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type) 
values (127,  'HRZ_RELATIONS', 'IX_RELATIONS_NDS_CH9', 'YEAR, QUARTER, INN_1, KPP_1_EFFECTIVE, NDS_CHAPTER9', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type) 
values (128,  'HRZ_RELATIONS', 'IX_RELATIONS_REV', 'YEAR, QUARTER, INN_1, KPP_1_EFFECTIVE, INN_2, KPP_2_EFFECTIVE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type) 
values (129,  'HRZ_RELATIONS', 'IX_RELATIONS_SA', 'YEAR, QUARTER, INN_1, KPP_1_EFFECTIVE, NSA_ORDER', 'NONUNIQUE');


insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (131, 'V$ASK_DECLANDJRNL', 'IDX_INN_CONTRACTOR', 'INN_CONTRACTOR', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (132, 'V$ASK_DECLANDJRNL', 'IDX_OTCHETGOD', 'OTCHETGOD', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (133, 'TAX_PAYER', 'IDX_INN_KPP', 'INN,KPP_EFFECTIVE,KPP,NAME_SHORT,INN_SUCCESSOR_TOP,KPP_EFFECTIVE_SUCCESSOR_TOP', 'UNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type) 
values (136, 'V$ASK_DECLANDJRNL', 'IDX_ASK_DECL_JRNL_ID_TYPE', 'ID, TYPE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type) 
values (135, 'ASK_DECLANDJRNL', 'IDX_ASK_ID_FILE', 'IDFAJL', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type, op_parallel_count) 
values (137, 'V$ASK_DECLANDJRNL', 'IDX_ASK_FILEINNPERIODCORR', 'IDFAJL, INNNP, PERIOD, OTCHETGOD, NOMKORR', 'NONUNIQUE', 16);

-- KNP_RELATION_SUMMARY
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type) 
values (160, 'KNP_RELATION_SUMMARY', 'IX_KNPRELSUM_PK','INN, INN_REORGANIZED, KPP_EFFECTIVE, TYPE_CODE, PERIOD_CODE, FISCAL_YEAR', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type) 
values (161, 'KNP_RELATION_SUMMARY', 'IX_KNPRELSUM_CH8_CNT',  'INN, INN_REORGANIZED, KPP_EFFECTIVE, TYPE_CODE, PERIOD_CODE, FISCAL_YEAR, CHAPTER8_COUNT', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type) 
values (162, 'KNP_RELATION_SUMMARY', 'IX_KNPRELSUM_CH9_CNT',  'INN, INN_REORGANIZED, KPP_EFFECTIVE, TYPE_CODE, PERIOD_CODE, FISCAL_YEAR, CHAPTER9_COUNT_EFFECTIVE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type) 
values (163, 'KNP_RELATION_SUMMARY', 'IX_KNPRELSUM_CH10_CNT', 'INN, INN_REORGANIZED, KPP_EFFECTIVE, TYPE_CODE, PERIOD_CODE, FISCAL_YEAR, CHAPTER10_COUNT', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type) 
values (164, 'KNP_RELATION_SUMMARY', 'IX_KNPRELSUM_CH11_CNT', 'INN, INN_REORGANIZED, KPP_EFFECTIVE, TYPE_CODE, PERIOD_CODE, FISCAL_YEAR, CHAPTER11_COUNT', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type) 
values (165, 'KNP_RELATION_SUMMARY', 'IX_KNPRELSUM_CH12_CNT', 'INN, INN_REORGANIZED, KPP_EFFECTIVE, TYPE_CODE, PERIOD_CODE, FISCAL_YEAR, CHAPTER12_COUNT', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type) 
values (166, 'KNP_RELATION_SUMMARY', 'IX_KNPRELSUM_CH8_AMNT',  'INN, INN_REORGANIZED, KPP_EFFECTIVE, TYPE_CODE, PERIOD_CODE, FISCAL_YEAR, CHAPTER8_AMOUNT', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type) 
values (167, 'KNP_RELATION_SUMMARY', 'IX_KNPRELSUM_CH9_AMNT',  'INN, INN_REORGANIZED, KPP_EFFECTIVE, TYPE_CODE, PERIOD_CODE, FISCAL_YEAR, CHAPTER9_AMOUNT', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type) 
values (168, 'KNP_RELATION_SUMMARY', 'IX_KNPRELSUM_CH10_AMNT', 'INN, INN_REORGANIZED, KPP_EFFECTIVE, TYPE_CODE, PERIOD_CODE, FISCAL_YEAR, CHAPTER10_AMOUNT', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type) 
values (169, 'KNP_RELATION_SUMMARY', 'IX_KNPRELSUM_CH11_AMNT', 'INN, INN_REORGANIZED, KPP_EFFECTIVE, TYPE_CODE, PERIOD_CODE, FISCAL_YEAR, CHAPTER11_AMOUNT', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type) 
values (170, 'KNP_RELATION_SUMMARY', 'IX_KNPRELSUM_CH12_AMNT', 'INN, INN_REORGANIZED, KPP_EFFECTIVE, TYPE_CODE, PERIOD_CODE, FISCAL_YEAR, CHAPTER12_AMOUNT', 'NONUNIQUE');

-- MV$TAX_PAYER_NAME 
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type) 
values (171, 'MV$TAX_PAYER_NAME', 'IDX_MVTPNAME_INN', 'INN', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type) 
values (172, 'MV$TAX_PAYER_NAME', 'IDX_MVTPNAME_INN_KPP', 'INN, KPP_EFFECTIVE', 'NONUNIQUE');

insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type) 
values (173, 'KNP_RESULT_DISCREPANCY_AGGR ', 'IX_ACT_AGGR', 'DISCREPANCY_ID', 'UNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (174, 'KNP_RESULT_AGGREGATE', 'KNPRES_AGGR_DECL', 'INN,KPP_EFFECTIVE,QUARTER,FISCAL_YEAR', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (175, 'KNP_DISCREPANCY', 'IX_KNP_DIS_ID', 'ID', 'UNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (176, 'KNP_DISCREPANCY', 'IX_KNP_DIS', 'ID,STATUS', 'UNIQUE');

--SOV_INVOICE_CUSTOM_AGGREGATE
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type) 
values (180, 'SOV_INVOICE_CUSTOM_AGGREGATE', 'IDX_SOV_INVOICE_CUSTOM_RK', 'INVOICE_ROW_KEY', 'NONUNIQUE');

-- explain_tks_receive_date
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type) 
values (181, 'EXPLAIN_TKS_RECEIVE_DATE', 'IX_TKS_RECDT', 'EXPLAIN_ZIP,INVOICE_ROW_KEY', 'NONUNIQUE');

-- explain_tks_receipt_document
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type) 
values (182, 'EXPLAIN_TKS_RECEIPT_DOCUMENT', 'IX_TKS_RECDOC', 'EXPLAIN_ZIP,INVOICE_ROW_KEY', 'NONUNIQUE');

-- explain_tks_contractor
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type) 
values (183, 'EXPLAIN_TKS_CONTRACTOR', 'IX_TKS_CTR', 'EXPLAIN_ZIP,INVOICE_ROW_KEY', 'NONUNIQUE');

-- explain_tks_invoice
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (184, 'EXPLAIN_TKS_INVOICE', 'IX_TKS_INV_CH', 'EXPLAIN_ZIP,CHAPTER', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (185, 'EXPLAIN_TKS_INVOICE', 'IX_TKS_INV_ORD', 'EXPLAIN_ZIP,CHAPTER,ORDINAL_NUMBER', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (186, 'EXPLAIN_TKS_INVOICE', 'IX_TKS_INV_STS', 'EXPLAIN_ZIP,CHAPTER,STATUS', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (187, 'EXPLAIN_TKS_INVOICE', 'IX_TKS_INV_NUM', 'EXPLAIN_ZIP,CHAPTER,INVOICE_NUM', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (188, 'EXPLAIN_TKS_INVOICE', 'IX_TKS_INV_DT', 'EXPLAIN_ZIP,CHAPTER,INVOICE_DATE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (189, 'EXPLAIN_TKS_INVOICE', 'IX_TKS_INV_INN', 'EXPLAIN_ZIP,CHAPTER,CONTRACTOR_INN_FIRST', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (190, 'EXPLAIN_TKS_INVOICE', 'IX_TKS_INV_KPP', 'EXPLAIN_ZIP,CHAPTER,CONTRACTOR_KPP_FIRST', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (191, 'EXPLAIN_TKS_INVOICE', 'IX_TKS_INV_INVROWKEY', 'INVOICE_ROW_KEY', 'NONUNIQUE');

-- explain_invoice_confirmed
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (192, 'EXPLAIN_INVOICE_CONFIRMED', 'IX_TKS_CF_ZIP', 'EXPLAIN_ZIP', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (193, 'EXPLAIN_INVOICE_CONFIRMED', 'IX_TKS_CF_BUYER', 'EXPLAIN_ZIP,BUYER_INN', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (194, 'EXPLAIN_INVOICE_CONFIRMED', 'IX_TKS_CF_INVROWKEY', 'INVOICE_ROW_KEY', 'NONUNIQUE');

--SOV_INVOICE_CUSTOM_AGGREGATE
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type) 
values (195, 'SOV_INVOICE_CUSTOM_AGGREGATE', 'IDX_SOV_INVOICE_CUSTOM_RK', 'INVOICE_ROW_KEY', 'NONUNIQUE');

-- DECLARATION_KNP
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type) 
values (196, 'DECLARATION_KNP', 'IDX_DECLARATION_KNP_ZIP', 'ZIP', 'NONUNIQUE');

--USER TASK REPORT
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type) 
values (211, 'USER_TASK_REPORT', 'IDX_USER_TASK_REPORT_DATE', 'REPORT_DATE', 'UNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type) 
values (212, 'USER_TASK_REPORT_SONO', 'IDX_USERTASKREPORTSONO_ID', 'REPORT_ID', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type) 
values (213, 'USER_TASK_REPORT_SONO', 'IDX_USERTASKREPORTSONO_REGION', 'REPORT_ID,REGION_CODE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type) 
values (214, 'USER_TASK_REPORT_SONO', 'IDX_USERTASKREPORTSONO_SONO', 'REPORT_ID,REGION_CODE, SONO_CODE', 'UNIQUE');

--KNP_DISCREPANCY_DECLARATION
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (104, 'KNP_DISCREPANCY_DECLARATION', 'IX_KNPDD_DISCREP_ID', 'DISCREPANCY_ID', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (105, 'KNP_DISCREPANCY_DECLARATION', 'IX_KNPDD_ZIP', 'ZIP', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (106, 'KNP_DISCREPANCY_DECLARATION', 'IX_KNPDD_CONTR_ZIP', 'CONTRACTOR_ZIP', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (107, 'KNP_DISCREPANCY_DECLARATION', 'IX_KNPDD_DECL_ID', 'INN_DECLARANT, KPP_EFFECTIVE, FISCAL_YEAR, PERIOD_CODE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (108, 'KNP_DISCREPANCY_DECLARATION', 'IX_KNPDD_ACT_INN', 'INN_DECLARANT, KPP_EFFECTIVE, PERIOD_CODE, FISCAL_YEAR, ACT_INN', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (109, 'KNP_DISCREPANCY_DECLARATION', 'IX_KNPDD_DIS_INN', 'DISCREPANCY_ID, ACT_INN', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (110, 'KNP_DISCREPANCY_DECLARATION', 'IX_KNPDD_KNPDOCTYPE', 'INN_DECLARANT, KPP_EFFECTIVE, PERIOD_CODE, FISCAL_YEAR, KNP_DOC_TYPE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (151, 'KNP_DISCREPANCY_DECLARATION', 'IX_KNPDD_KNPDOCAMT', 'INN_DECLARANT, KPP_EFFECTIVE, PERIOD_CODE, FISCAL_YEAR, KNP_DOC_AMOUNT', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (177, 'KNP_DISCREPANCY_DECLARATION', 'IX_KNPDD_DECISAMTDIFF', 'INN_DECLARANT, KPP_EFFECTIVE, PERIOD_CODE, FISCAL_YEAR, DECISION_AMOUNT_DIFFERENCE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (178, 'KNP_DISCREPANCY_DECLARATION', 'IX_KNPDD_INNKPPYEAR', 'INN_DECLARANT, KPP_EFFECTIVE, FISCAL_YEAR', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (179, 'KNP_DISCREPANCY_DECLARATION', 'IX_KNPDD_INNKPP', 'INN_DECLARANT, KPP_EFFECTIVE', 'NONUNIQUE');

--HIVE_DISCREPANCY_DECL_CACHE
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (218, 'HIVE_DISCREPANCY_DECL_CACHE', 'IX_HDDC_REQUEST_ID', 'REQUEST_ID', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (219, 'HIVE_DISCREPANCY_DECL_CACHE', 'IX_HDDC_DISCREP_ID', 'DISCREPANCY_ID', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (220, 'HIVE_DISCREPANCY_DECL_CACHE', 'IX_HDDC_ZIP', 'ZIP', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (221, 'HIVE_DISCREPANCY_DECL_CACHE', 'IX_HDDC_CONTR_ZIP', 'CONTRACTOR_ZIP', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (222, 'HIVE_DISCREPANCY_DECL_CACHE', 'IX_HDDC_DECL_ID', 'INN_DECLARANT, KPP_EFFECTIVE, FISCAL_YEAR, PERIOD_CODE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (223, 'HIVE_DISCREPANCY_DECL_CACHE', 'IX_HDDC_ACT_INN', 'INN_DECLARANT, KPP_EFFECTIVE, PERIOD_CODE, FISCAL_YEAR, ACT_INN', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (224, 'HIVE_DISCREPANCY_DECL_CACHE', 'IX_HDDC_DIS_INN', 'DISCREPANCY_ID, ACT_INN', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (225, 'HIVE_DISCREPANCY_DECL_CACHE', 'IX_HDDC_KNPDOCTYPE', 'INN_DECLARANT, KPP_EFFECTIVE, PERIOD_CODE, FISCAL_YEAR, KNP_DOC_TYPE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (226, 'HIVE_DISCREPANCY_DECL_CACHE', 'IX_HDDC_KNPDOCAMT', 'INN_DECLARANT, KPP_EFFECTIVE, PERIOD_CODE, FISCAL_YEAR, KNP_DOC_AMOUNT', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (227, 'HIVE_DISCREPANCY_DECL_CACHE', 'IX_HDDC_DECISAMTDIFF', 'INN_DECLARANT, KPP_EFFECTIVE, PERIOD_CODE, FISCAL_YEAR, DECISION_AMOUNT_DIFFERENCE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (228, 'HIVE_DISCREPANCY_DECL_CACHE', 'IX_HDDC_INNKPPYEAR', 'INN_DECLARANT, KPP_EFFECTIVE, FISCAL_YEAR', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (229, 'HIVE_DISCREPANCY_DECL_CACHE', 'IX_HDDC_INNKPP', 'INN_DECLARANT, KPP_EFFECTIVE', 'NONUNIQUE');

-- KNP_RELATION_SUMMARY
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type) 
values (230, 'KNP_RELATION_SUMMARY', 'IX_KNPRELSUM_KNP_CH8_CNT',  'INN, INN_REORGANIZED, KPP_EFFECTIVE, TYPE_CODE, PERIOD_CODE, FISCAL_YEAR, GAP_QUANTITY_8+OTHER_QUANTITY_8', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type) 
values (231, 'KNP_RELATION_SUMMARY', 'IX_KNPRELSUM_KNP_CH9_CNT',  'INN, INN_REORGANIZED, KPP_EFFECTIVE, TYPE_CODE, PERIOD_CODE, FISCAL_YEAR, GAP_QUANTITY_9+OTHER_QUANTITY_9', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type) 
values (232, 'KNP_RELATION_SUMMARY', 'IX_KNPRELSUM_KNP_CH10_CNT',  'INN, INN_REORGANIZED, KPP_EFFECTIVE, TYPE_CODE, PERIOD_CODE, FISCAL_YEAR, GAP_QUANTITY_10+OTHER_QUANTITY_10', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type) 
values (233, 'KNP_RELATION_SUMMARY', 'IX_KNPRELSUM_KNP_CH11_CNT',  'INN, INN_REORGANIZED, KPP_EFFECTIVE, TYPE_CODE, PERIOD_CODE, FISCAL_YEAR, GAP_QUANTITY_11+OTHER_QUANTITY_11', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type) 
values (234, 'KNP_RELATION_SUMMARY', 'IX_KNPRELSUM_KNP_CH12_CNT',  'INN, INN_REORGANIZED, KPP_EFFECTIVE, TYPE_CODE, PERIOD_CODE, FISCAL_YEAR, GAP_QUANTITY_12+OTHER_QUANTITY_12', 'NONUNIQUE');

-- DECLARATION_KNP_STATUS
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type, op_statistics, op_parallel_count, op_noparallel_postbuild)
values(235, 'DECLARATION_KNP_STATUS', 'IX_DECLKNPST_ZIP', 'ZIP', 'NONUNIQUE', 1, 4, 1);
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type, op_statistics, op_parallel_count, op_noparallel_postbuild)
values(236, 'DECLARATION_KNP_STATUS', 'IX_DECLKNPST_DECL_DATE', 'DECL_INSERT_DATE', 'NONUNIQUE', 1, 4, 1);
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type, op_statistics, op_parallel_count, op_noparallel_postbuild)
values(237, 'DECLARATION_KNP_STATUS', 'IX_DECLKNPST_SEOD_DATE', 'SEOD_CREATION_DATE', 'NONUNIQUE', 1, 4, 1);

insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type, op_statistics, op_parallel_count, op_noparallel_postbuild)
values(238, 'SOV_DISCREPANCY', 'IX_SOVDIS_STATUS_SEL_BUY', 'STATUS, SELLER_ZIP, BUYER_INN', 'NONUNIQUE', 1, 4, 1);

insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type, op_parallel_count)
values(239, 'SOV_INVOICE_ALL', 'IX_SIA_RK_DISID', 'ROW_KEY, DISCREPANCY_ID', 'NONUNIQUE', 16);

insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values(240, 'MRR_USER', 'IX_MRRUSER_ID', 'ID', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values(241, 'SELECTION', 'IX_SEL_TEMPL_ID', 'TEMPLATE_ID', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values(242, 'SELECTION', 'IX_SEL_ID_TEMPL_ID', 'ID,TEMPLATE_ID', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values(243, 'SELECTION_AGGREGATE', 'IX_SELAG_ID_STATUS', 'ID,STATUS_ID', 'NONUNIQUE');

commit;
