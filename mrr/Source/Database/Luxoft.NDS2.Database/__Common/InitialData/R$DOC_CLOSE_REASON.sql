﻿truncate table NDS2_MRR_USER.R$DOC_CLOSE_REASON;

insert into NDS2_MRR_USER.R$DOC_CLOSE_REASON(ID, DOC_TYPE, DESCRIPTION) VALUES (1, 1, 'Истек срок отработки');
insert into NDS2_MRR_USER.R$DOC_CLOSE_REASON(ID, DOC_TYPE, DESCRIPTION) VALUES (2, 1, 'Новая корректировка');
insert into NDS2_MRR_USER.R$DOC_CLOSE_REASON(ID, DOC_TYPE, DESCRIPTION) VALUES (3, 1, 'Закрылась КНП');
insert into NDS2_MRR_USER.R$DOC_CLOSE_REASON(ID, DOC_TYPE, DESCRIPTION) VALUES (4, 1, 'Закрылись все расхождения');
insert into NDS2_MRR_USER.R$DOC_CLOSE_REASON(ID, DOC_TYPE, DESCRIPTION) VALUES (5, 1, 'Корректировка аннулирована');

commit;
