﻿truncate table NDS2_MRR_USER.DICT_ACTIONS;

insert into NDS2_MRR_USER.DICT_ACTIONS (id, action_name)
values (1, 'Создание');
insert into NDS2_MRR_USER.DICT_ACTIONS (id, action_name)
values (2, 'Редактирование');
insert into NDS2_MRR_USER.DICT_ACTIONS (id, action_name)
values (3, 'Повторное применение фильтра');
insert into NDS2_MRR_USER.DICT_ACTIONS (id, action_name)
values (4, 'Отправка на согласование');
insert into NDS2_MRR_USER.DICT_ACTIONS (id, action_name)
values (5, 'Согласование');
insert into NDS2_MRR_USER.DICT_ACTIONS (id, action_name)
values (6, 'Возврат на доработку');
insert into NDS2_MRR_USER.DICT_ACTIONS (id, action_name)
values (7, 'Формирование АТ');
commit;
