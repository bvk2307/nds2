﻿begin 
  execute immediate 'drop table nds2_mrr_user.seod_declaration';
  exception when others then null;
end;
/

begin 
  execute immediate 'drop table nds2_mrr_user.seod_incoming';
  exception when others then null;
end;
/

begin 
  execute immediate 'drop table nds2_mrr_user.seod_knp';
  exception when others then null;
end;
/

begin 
  execute immediate 'drop table nds2_mrr_user.seod_knp_act';
  exception when others then null;
end;
/

begin 
  execute immediate 'drop table nds2_mrr_user.seod_knp_decision';
  exception when others then null;
end;
/

begin 
  execute immediate 'drop table nds2_mrr_user.seod_knp_decision_type';
  exception when others then null;
end;
/

begin 
  execute immediate 'drop table nds2_mrr_user.seod_prolong_decision';
  exception when others then null;
end;
/

begin 
  execute immediate 'drop table nds2_mrr_user.seod_reclaim_order';
  exception when others then null;
end;
/