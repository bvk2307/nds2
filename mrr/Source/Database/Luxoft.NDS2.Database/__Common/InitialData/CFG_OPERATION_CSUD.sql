﻿prompt Loading CFG_OPERATION_CSUD...
truncate table NDS2_MRR_USER.CFG_OPERATION_CSUD;
insert into NDS2_MRR_USER.CFG_OPERATION_CSUD (id, operation_id, name)
values (1, 1, 'Декларация.Просмотр_списка.ЦА');
insert into NDS2_MRR_USER.CFG_OPERATION_CSUD (id, operation_id, name)
values (2, 1, 'Декларация.Просмотр_списка.МИпоФО');
insert into NDS2_MRR_USER.CFG_OPERATION_CSUD (id, operation_id, name)
values (3, 1, 'Декларация.Просмотр_списка.МИпоКК');
insert into NDS2_MRR_USER.CFG_OPERATION_CSUD (id, operation_id, name)
values (4, 1, 'Декларация.Просмотр_списка.МИпоКН');
insert into NDS2_MRR_USER.CFG_OPERATION_CSUD (id, operation_id, name)
values (5, 1, 'Декларация.Просмотр_списка.УФНС');
insert into NDS2_MRR_USER.CFG_OPERATION_CSUD (id, operation_id, name)
values (6, 1, 'Декларация.Просмотр_списка.ИФНС');
insert into NDS2_MRR_USER.CFG_OPERATION_CSUD (id, operation_id, name)
values (30, 3, 'ООР.Сбросить_признак');
insert into NDS2_MRR_USER.CFG_OPERATION_CSUD (id, operation_id, name)
values (40, 4, 'НД.Выгрузка_в_EXCEL.ИФНС_УФНС_МИпоКН');
insert into NDS2_MRR_USER.CFG_OPERATION_CSUD (id, operation_id, name)
values (41, 4, 'НД.Выгрузка_в_EXCEL.МИпоФО');
insert into NDS2_MRR_USER.CFG_OPERATION_CSUD (id, operation_id, name)
values (42, 4, 'НД.Выгрузка_в_EXCEL.МИпоКК');
insert into NDS2_MRR_USER.CFG_OPERATION_CSUD (id, operation_id, name)
values (43, 4, 'НД.Выгрузка_в_EXCEL.ЦА');
insert into NDS2_MRR_USER.CFG_OPERATION_CSUD (id, operation_id, name)
values (50, 5, 'Журнал.Выгрузка_в_EXCEL.УФНС_МИпоКН');
insert into NDS2_MRR_USER.CFG_OPERATION_CSUD (id, operation_id, name)
values (51, 5, 'Журнал.Выгрузка_в_EXCEL.МИпоФО');
insert into NDS2_MRR_USER.CFG_OPERATION_CSUD (id, operation_id, name)
values (52, 5, 'Журнал.Выгрузка_в_EXCEL.МИпоКК');
insert into NDS2_MRR_USER.CFG_OPERATION_CSUD (id, operation_id, name)
values (53, 5, 'Журнал.Выгрузка_в_EXCEL.ЦА');
insert into NDS2_MRR_USER.CFG_OPERATION_CSUD (id, operation_id, name)
values (60, 6, 'НД_Журнал.Дерево_связей.УФНС');
insert into NDS2_MRR_USER.CFG_OPERATION_CSUD (id, operation_id, name)
values (61, 6, 'НД_Журнал.Дерево_связей.МИпоФО');
insert into NDS2_MRR_USER.CFG_OPERATION_CSUD (id, operation_id, name)
values (62, 6, 'НД_Журнал.Дерево_связей.ЦА');
insert into NDS2_MRR_USER.CFG_OPERATION_CSUD (id, operation_id, name)
values (63, 6, 'НД_Журнал.Дерево_связей.МИпоКК');
insert into NDS2_MRR_USER.CFG_OPERATION_CSUD (id, operation_id, name)
values (64, 6, 'НД_Журнал.Дерево_связей.МИпоКН');
insert into NDS2_MRR_USER.CFG_OPERATION_CSUD (ID, OPERATION_ID, NAME)
values (65, 6, 'НД_Журнал.Дерево_связей.ИФНС');
Insert into NDS2_MRR_USER.CFG_OPERATION_CSUD (ID,OPERATION_ID,NAME) 
values (70, 7, 'Пояснение_Ответ.Редактирование');
Insert into NDS2_MRR_USER.CFG_OPERATION_CSUD (ID,OPERATION_ID,NAME) 
values (80, 8, 'Навигатор.Просмотр_УФНС');
Insert into NDS2_MRR_USER.CFG_OPERATION_CSUD (ID,OPERATION_ID,NAME) 
values (81, 8, 'Навигатор.Просмотр_МИпоКН');
Insert into NDS2_MRR_USER.CFG_OPERATION_CSUD (ID,OPERATION_ID,NAME) 
values (82, 8, 'Навигатор.Просмотр_ЦА');
Insert into NDS2_MRR_USER.CFG_OPERATION_CSUD (ID,OPERATION_ID,NAME) 
values (83, 8, 'Навигатор.Просмотр_МИпоФО');
Insert into NDS2_MRR_USER.CFG_OPERATION_CSUD (ID,OPERATION_ID,NAME) 
values (84, 8, 'Навигатор.Просмотр_МИпоКК');
Insert into NDS2_MRR_USER.CFG_OPERATION_CSUD (ID,OPERATION_ID,NAME) 
values (90, 9, 'Налогоплательщик.Запросы_в_банк.ИФНС');
Insert into NDS2_MRR_USER.CFG_OPERATION_CSUD (ID,OPERATION_ID,NAME) 
values (91, 9, 'Налогоплательщик.Запросы_в_банк.УФНС');
Insert into NDS2_MRR_USER.CFG_OPERATION_CSUD (ID,OPERATION_ID,NAME) 
values (92, 9, 'Налогоплательщик.Запросы_в_банк.МИпоКН');
commit;
prompt CFG_OPERATION_CSUD...Done.
