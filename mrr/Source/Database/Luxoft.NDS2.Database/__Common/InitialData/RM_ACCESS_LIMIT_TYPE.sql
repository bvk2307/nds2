﻿truncate table NDS2_MRR_USER.RM_ACCESS_LIMIT_TYPE;

insert into NDS2_MRR_USER.RM_ACCESS_LIMIT_TYPE
  values(1, 'Без ограничений');

insert into NDS2_MRR_USER.RM_ACCESS_LIMIT_TYPE
  values(2, 'Свой НО');

insert into NDS2_MRR_USER.RM_ACCESS_LIMIT_TYPE
  values(3, 'Федеральный округ');

insert into NDS2_MRR_USER.RM_ACCESS_LIMIT_TYPE
  values(4, 'Регион');

insert into NDS2_MRR_USER.RM_ACCESS_LIMIT_TYPE
  values(5, 'Инспекция');

commit;
