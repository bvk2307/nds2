﻿prompt Loading RM_OPERATION_TO_GROUP...
truncate table NDS2_MRR_USER.RM_OPERATION_TO_GROUP;

/* Отчеты по пользовательским заданиям */
-- По стране (Администратор ЦА без ограничений)
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (1, 1, 1, 1);
-- По стране с разбивкой по региону (Администратор ЦА без ограничений)
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (2, 1, 1, 1);
-- По стране с разбивкой по инспекции (Администратор ЦА без ограничений)
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (3, 1, 1, 1);
-- По инспекции с разбивкой по инспекторам
-- Администратор ИФНС/МРИ только свой НО
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (4, 2, 1, 2);
-- Администратор МИ по КН только свой НО
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (4, 4, 1, 2);
-- Администратор УФНС 0400 только свой НО
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (4, 8, 1, 2);
-- По инспекциям региона (Администратор УФНС кроме 0400 с ограничением на регион)
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (5, 3, 1, 4);
-- Инспектор ИФНС только свой НО
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (4, 2, 3, 1);
-- Инспектор МИ по КН только свой НО
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (4, 4, 3, 1);

/* Отчеты по актам и решениям */
-- По стране
-- Аналитик ЦА без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (6, 1, 2, 1);
-- Аналитик МИ по КК без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (6, 5, 2, 1);
-- Методолог ЦА без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (6, 1, 4, 1);
-- Наблюдатель ЦА без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (6, 1, 5, 1);
-- Наблюдатель МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (6, 5, 5, 1);
-- Наблюдатель МИ по ФО
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (6, 6, 5, 1);
-- Наблюдатель МИ по ценам
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (6, 7, 5, 1);
-- Методолог МИ по КК без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (6, 5, 4, 1);
-- По федеральноым округам
-- Аналитик ЦА без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (7, 1, 2, 1);
-- Методолог ЦА без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (7, 1, 4, 1);
-- Наблюдатель ЦА без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (7, 1, 5, 1);
-- Наблюдатель МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (7, 5, 5, 1);
-- Наблюдатель МИ по ФО
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (7, 6, 5, 1);
-- Наблюдатель МИ по ценам
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (7, 7, 5, 1);
-- По регионам
-- Аналитик ЦА без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (8, 1, 2, 1);
-- Методолог ЦА без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (8, 1, 4, 1);
-- Наблюдатель ЦА без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (8, 1, 5, 1);
-- Наблюдатель МИ по КК без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (8, 5, 5, 1);
-- Наблюдатель МИ по ФО без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (8, 6, 5, 1);
-- Наблюдатель МИ по ценам без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (8, 7, 5, 1);
-- По инспекциям
-- Аналитик ЦА без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (9, 1, 2, 1);
-- Методолог ЦА без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (9, 1, 4, 1);
-- Наблюдатель ЦА без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (9, 1, 5, 1);
-- Наблюдатель МИ по КК без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (9, 5, 5, 1);
-- Наблюдатель МИ по ФО без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (9, 6, 5, 1);
-- Наблюдатель МИ по ценам без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (9, 7, 5, 1);
-- Наблюдатель УФНС по региону
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (9, 3, 5, 4);
-- Наблюдатель 0400 по региону
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (9, 8, 5, 4);
-- Наблюдатель ИФНС по своему НО
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (9, 2, 5, 2);
-- Наблюдатель МИ по КН по своему НО
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (9, 4, 5, 2);
-- Инспектор 0400 по своему НО
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (9, 8, 3, 2);
-- Инспектор ИФНС по своему НО
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (9, 2, 3, 2);
-- Инспектор МИ по КН по своему НО
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (9, 4, 3, 2);

/* Ввод/просмотр пояснений и ответов на истребования*/
-- Ввод
-- Инспектор ИФНС по своему НО
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (10, 2, 3, 2);
-- Инспектор МИ по КН по своему НО
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (10, 4, 3, 2);
-- Инспектор 0400 по своему НО
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (10, 8, 3, 2);

-- Просмотр

-- Инспектор ЦА без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (11, 1, 3, 1);
-- Инспектор ИФНС без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (11, 2, 3, 1);
-- Инспектор МИ по КН без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (11, 4, 3, 1);
-- Инспектор 0400 без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (11, 8, 3, 1);

-- Администратор ИФНС без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (11, 2, 1, 1);
-- Администратор МИ по КН без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (11, 4, 1, 1);
-- Администратор 0400 без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (11, 8, 1, 1);
-- Администратор УФНС без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (11, 3, 1, 1);
-- Администратор ЦА без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (11, 1, 1, 1);

-- Аналитик ЦА без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (11, 1, 2, 1);
-- Аналитик МИ по КК без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (11, 5, 2, 1);

-- Согласующий МИ по КК без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (11, 5, 8, 1);

-- Методолог ЦА без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (11, 1, 4, 1);
-- Методолог МИ по КК без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (11, 5, 4, 1);

-- Наблюдатель ЦА без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (11, 1, 5, 1);
-- Наблюдатель МИ по КК без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (11, 5, 5, 1);
-- Наблюдатель МИ по ФО без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (11, 6, 5, 1);
-- Наблюдатель МИ по ценам без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (11, 7, 5, 1);
-- Наблюдатель ИФНС без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (11, 2, 5, 1);
-- Наблюдатель МИ по КН без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (11, 4, 5, 1);
-- Наблюдатель 0400 без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (11, 8, 5, 1);
-- Наблюдатель УФНС без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (11, 3, 5, 1);

-- Руководитель ЦА без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (11, 1, 7, 1);
-- Руководитель МИ по КК без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (11, 5, 7, 1);
-- Руководитель МИ по ФО без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (11, 6, 7, 1);
-- Руководитель ИФНС без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (11, 2, 7, 1);
-- Руководитель МИ по КН без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (11, 4, 7, 1);
-- Руководитель 0400 без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (11, 8, 7, 1);
-- Руководитель УФНС без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (11, 3, 7, 1);

/* Доступ к окну оперативной работы */
-- Методолог ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (12, 1, 4, 1);
-- Аналитик ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (12, 1, 2, 1);
-- Наблюдатель ЦА без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (12, 1, 5, 1);
-- Руководитель ЦА без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (12, 1, 7, 1);
-- Инспектор ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (12, 1, 3, 1); 

-- Методолог МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (12, 5, 4, 1);
-- Аналитик МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (12, 5, 2, 1);
-- Наблюдатель МИ по КК без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (12, 5, 5, 1);
-- Руководитель МИ по КК без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (12, 5, 7, 1);

-- Наблюдатель МИ по ФО без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (12, 6, 5, 1);
-- Руководитель МИ по ФО без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (12, 6, 7, 1);

-- Наблюдатель МИ по ценам без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (12, 7, 5, 1);

-- Наблюдатель УФНС (свой НО)
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (12, 3, 5, 4); 
-- Руководитель УФНС (свой НО)
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (12, 3, 7, 4); 
-- Наблюдатель УФНС 0400 (свой НО)
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (12, 8, 5, 2);
-- Руководитель УФНС 0400 (свой НО)
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (12, 8, 7, 2); 

-- Наблюдатель МИ по КН (свой НО)
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (12, 4, 5, 2); 
-- Руководитель МИ по КН (свой НО)
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (12, 4, 7, 2); 
-- Инспектор МИ по КН (свой НО)
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (12, 4, 3, 2); 
-- Администратор МИ по КН
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (12, 4, 1, 2);

-- Наблюдатель ИФНС (свой НО)
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (12, 2, 5, 2); 
-- Руководитель ИФНС (свой НО)
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (12, 2, 7, 2); 
-- Администратор ИФНС
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (12, 2, 1, 2);
-- Инспектор ИФНС (свой НО)
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (12, 2, 3, 2); 


/* Доступ к назначение НД/Журналов */
-- Администратор ИФНС (свой НО)
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (13, 2, 1, 2);
-- Администратор УФНС с кодом 0400 (свой НО)
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (13, 8, 1, 2);
-- Администратор МИ по КН (свой НО)
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (13, 4, 1, 2);

/*Доступ к отчету Фоновые показатели по стране*/
-- Методолог ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (14, 1, 4, 1);
-- Аналитик ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (14, 1, 2, 1);
-- Наблюдатель ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (14, 1, 5, 1);
-- Аналитик МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (14, 5, 2, 1);
-- Методолог МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (14, 5, 4, 1);
-- Наблюдатель Ми по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (14, 5, 5, 1);
-- Наблюдатель МИ по ФО
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (14, 6, 5, 1);
-- Наблюдатель МИ по ценам
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (14, 7, 5, 1);


/*Доступ к отчету Фоновые показатели по федеральным округам*/
-- Методолог ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (15, 1, 4, 1);
-- Аналитик ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (15, 1, 2, 1);
-- Наблюдатель ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (15, 1, 5, 1);
-- Аналитик МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (15, 5, 2, 1);
-- Методолог МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (15, 5, 4, 1);
-- Наблюдатель Ми по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (15, 5, 5, 1);
-- Наблюдатель МИ по ФО
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (15, 6, 5, 1);
-- Наблюдатель МИ по ценам
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (15, 7, 5, 1);


/*Доступ к отчету Фоновые показатели по регионам*/
-- Методолог ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (16, 1, 4, 1);
-- Аналитик ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (16, 1, 2, 1);
-- Наблюдатель ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (16, 1, 5, 1);
-- Аналитик МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (16, 5, 2, 1);
-- Методолог МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (16, 5, 4, 1);
-- Наблюдатель Ми по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (16, 5, 5, 1);
-- Наблюдатель МИ по ФО
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (16, 6, 5, 1);
-- Наблюдатель МИ по ценам
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (16, 7, 5, 1);
-- Наблюдатель УФНС (свой НО)
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (16, 3, 5, 2);
-- Наблюдатель УФНС 0400 (свой НО)
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (16, 8, 5, 2);

--Доступ к отчету Фоновые показатели по инспекциям
-- Методолог ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (17, 1, 4, 1);
-- Аналитик ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (17, 1, 2, 1);
-- Наблюдатель ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (17, 1, 5, 1);
-- Аналитик МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (17, 5, 2, 1);
-- Методолог МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (17, 5, 4, 1);
-- Наблюдатель Ми по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (17, 5, 5, 1);
-- Наблюдатель МИ по ФО
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (17, 6, 5, 1);
-- Наблюдатель МИ по ценам
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (17, 7, 5, 1);
-- Наблюдатель УФНС (свой НО)
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (17, 3, 5, 2);
-- Инспектор ИФНС (свой НО)
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (17, 2, 3, 2);
-- Наблюдатель ИФНС (свой НО)
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (17, 2, 5, 2);
-- Инспектор МИ по КН (свой НО)
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (17, 4, 3, 2);
-- Наблюдатель МИ по КН (свой НО)
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (17, 4, 5, 2);
-- Наблюдатель УФНС 0400 (свой НО)
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (17, 8, 5, 2);
-- Инспектор УФНС 0400 (свой НО)
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (17, 8, 3, 2);

-- Изменения в версии
-- Методолог ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (operation_id, group_id, role_id, limit_type_id) values(18, 1, 4, 1);
-- Аналитик ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (operation_id, group_id, role_id, limit_type_id) values(18, 1, 2, 1);
-- Наблюдатель ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (operation_id, group_id, role_id, limit_type_id) values(18, 1, 5, 1);
-- Руководитель ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (operation_id, group_id, role_id, limit_type_id) values(18, 1, 7, 1);
-- Администратор ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (operation_id, group_id, role_id, limit_type_id) values(18, 1, 1, 1);
-- Инспектор ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (operation_id, group_id, role_id, limit_type_id) values(18, 1, 3, 1);

-- Методолог МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (operation_id, group_id, role_id, limit_type_id) values(18, 5, 4, 1);
-- Аналитик МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (operation_id, group_id, role_id, limit_type_id) values(18, 5, 2, 1);
-- Согласующий МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (operation_id, group_id, role_id, limit_type_id) values(18, 5, 8, 1);
-- Отправитель МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (operation_id, group_id, role_id, limit_type_id) values(18, 5, 6, 1);
-- Наблюдатель МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (operation_id, group_id, role_id, limit_type_id) values(18, 5, 5, 1);
-- Руководитель МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (operation_id, group_id, role_id, limit_type_id) values(18, 5, 7, 1);

-- Наблюдатель МИ по ФО
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (operation_id, group_id, role_id, limit_type_id) values(18, 6, 5, 1);
-- Руководитель МИ по ФО
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (operation_id, group_id, role_id, limit_type_id) values(18, 6, 7, 1);

-- Наблюдатель МИ по ценам
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (operation_id, group_id, role_id, limit_type_id) values(18, 7, 5, 1);

-- Наблюдатель УФНС
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (operation_id, group_id, role_id, limit_type_id) values(18, 3, 5, 1);
-- Руководитель УФНС
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (operation_id, group_id, role_id, limit_type_id) values(18, 3, 7, 1);
-- Администратор УФНС
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (operation_id, group_id, role_id, limit_type_id) values(18, 3, 1, 1);

-- Наблюдатель МИ по КН
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (operation_id, group_id, role_id, limit_type_id) values(18, 4, 5, 1);
-- Руководитель МИ по КН
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (operation_id, group_id, role_id, limit_type_id) values(18, 4, 7, 1);
-- Администратор МИ по КН
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (operation_id, group_id, role_id, limit_type_id) values(18, 4, 1, 1);
-- Инспектор МИ по КН
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (operation_id, group_id, role_id, limit_type_id) values(18, 4, 3, 1);

-- Наблюдатель ИФНС
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (operation_id, group_id, role_id, limit_type_id) values(18, 2, 5, 1);
-- Руководитель ИФНС
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (operation_id, group_id, role_id, limit_type_id) values(18, 2, 7, 1);
-- Администратор ИФНС
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (operation_id, group_id, role_id, limit_type_id) values(18, 2, 1, 1);
-- Инспектор ИФНС
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (operation_id, group_id, role_id, limit_type_id) values(18, 2, 3, 1);

-- Наблюдатель УФНС 0400
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (operation_id, group_id, role_id, limit_type_id) values(18, 8, 5, 1);
-- Руководитель УФНС 0400
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (operation_id, group_id, role_id, limit_type_id) values(18, 8, 7, 1);
-- Администратор УФНС 0400
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (operation_id, group_id, role_id, limit_type_id) values(18, 8, 1, 1);
-- Инспектор УФНС 0400
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (operation_id, group_id, role_id, limit_type_id) values(18, 8, 3, 1);

-- Декларации
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (19, 1, 4, 1);
-- Методолог ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (19, 5, 4, 1);
-- Методолог МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (19, 1, 2, 1);
-- Аналитик ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (19, 5, 2, 1);
-- Аналитик МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (19, 5, 8, 1);
-- Согласующий МИ по КК

-- Список выборок
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (20, 5, 4, 1);
-- Методолог МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (20, 1, 2, 1);
-- Аналитик ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (20, 5, 2, 1);
-- Аналитик МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (20, 5, 8, 1);
-- Согласующий МИ по КК

-- Список АТ
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (21, 1, 4, 1);
-- Методолог ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (21, 5, 4, 1);
-- Методолог МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (21, 1, 2, 1);
-- Аналитик ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (21, 5, 2, 1);
-- Аналитик МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (21, 5, 8, 1);
-- Согласующий МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (21, 5, 6, 1);
-- Отправитель МИ по КК

-- Макроотчёты.НДС и расхождения
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (22, 1, 4, 1);
-- Методолог ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (22, 5, 4, 1);
-- Методолог МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (22, 1, 2, 1);
-- Аналитик ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (22, 5, 2, 1);
-- Аналитик МИ по КК

-- Параметры АСК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (24, 1, 4, 1);
-- Методолог ЦА

-- Настройка списков
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (25, 5, 4, 1);
-- Методолог МИ по КК

-- Календарь
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (26, 1, 4, 1);
-- Методолог ЦА

-- Справочники
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (27, 1, 4, 1);
-- Методолог ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (27, 5, 4, 1);
-- Методолог МИ по КК

-- НД.Открыть
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (28, 1, 4, 1);
-- Методолог ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (28, 5, 4, 1);
-- Методолог МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (28, 1, 2, 1);
-- Аналитик ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (28, 5, 2, 1);
-- Аналитик МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (28, 5, 8, 1);
-- Согласующий МИ по КК

-- НД.Контрагент
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (29, 5, 4, 1);
-- Методолог МИ по КК без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (29, 5, 2, 1);
-- Аналитик МИ по КК без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (29, 5, 8, 1);
-- Согласующий МИ по КК без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (29, 5, 5, 1);
-- Наблюдатель Ми по КК без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (29, 5, 7, 1);
-- Руководитель Ми по КК без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (29, 1, 2, 1);
-- Аналитик ЦА без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (29, 1, 4, 1);
-- Методолог ЦА без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (29, 1, 5, 1);
-- Наблюдатель ЦА без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (29, 1, 7, 1);
-- Руководитель ЦА без ограничений
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (29, 1, 3, 2);
-- Инспектор ЦА c ограничениями
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (29, 6, 5, 2);
-- Наблюдатель Ми по Фо с ограничениями
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (29, 6, 7, 2);
-- Руководитель Ми по Фо с ограничениями
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (29, 7, 5, 2);
-- Наблюдатель Ми по ценам с ограничениями
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (29, 3, 5, 2);
-- Наблюдатель УФНС с ограничениями
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (29, 3, 7, 2);
-- Руководитель УФНС с ограничениями
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (29, 4, 5, 2);
-- Наблюдатель Ми по КН c ограничениями
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (29, 4, 7, 2);
-- Руководитель МИ по КН с ограничениями
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (29, 4, 3, 2);
-- Инспектор Ми по КН c ограничениями
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (29, 2, 5, 2);
-- Наблюдатель ИФНС c ограничениями
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (29, 2, 7, 2);
-- Руководитель ИФНС с ограничениями
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (29, 2, 3, 2);
-- Инспектор ИФНС c ограничениями

-- НД.Отчёт.Выгрузка из НД/Журналов разделов с записями по СФ в MS Excel
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (30, 1, 4, 1);
-- Методолог ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (30, 1, 2, 1);
-- Аналитик ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (30, 1, 5, 1);
-- Наблюдатель ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (30, 1, 7, 1);
-- Руководитель ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (30, 1, 3, 1);
-- Инспектор ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (30, 5, 4, 1);
-- Методолог МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (30, 5, 2, 1);
-- Аналитик МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (30, 5, 8, 1);
-- Согласующий МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (30, 5, 5, 1);
-- Наблюдатель МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (30, 5, 7, 1);
-- Руководитель МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (30, 6, 5, 1);
-- Наблюдатель МИ по ФО
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (30, 6, 7, 1);
-- Руководитель МИ по ФО
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (30, 7, 5, 1);
-- Наблюдатель МИ по ценам
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (30, 3, 5, 2);
-- Наблюдатель УФНС
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (30, 3, 7, 2);
-- Руководитель УФНС
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (30, 8, 5, 2);
-- Наблюдатель УФНС 0400
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (30, 8, 7, 2);
-- Руководитель УФНС 0400
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (30, 4, 5, 2);
-- Наблюдатель Ми по Кн
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (30, 4, 7, 2);
-- Руководитель Ми по Кн
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (30, 4, 3, 2);
-- Инспектор Ми по Кн
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (30, 2, 5, 2);
-- Наблюдатель Ми по Кн
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (30, 2, 7, 2);
-- Руководитель Ми по Кн
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (30, 2, 3, 2);
-- Инспектор Ми по Кн

-- НД.Общие сведения
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (31, 1, 4, 1);
-- Методолог ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (31, 5, 4, 1);
-- Методолог МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (31, 1, 2, 1);
-- Аналитик ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (31, 5, 2, 1);
-- Аналитик МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (31, 5, 8, 1);
-- Согласующий МИ по КК

-- Расхождения.Все
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (32, 1, 4, 1);
-- Методолог ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (32, 5, 4, 1);
-- Методолог МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (32, 1, 2, 1);
-- Аналитик ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (32, 5, 2, 1);
-- Аналитик МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (32, 5, 8, 1);
-- Согласующий МИ по КК

-- Акты.Расхождения.Просмотр
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (34, 1, 4, 1);
-- Методолог ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (34, 5, 4, 1);
-- Методолог МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (34, 1, 2, 1);
-- Аналитик ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (34, 5, 2, 1);
-- Аналитик МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (34, 5, 8, 1);
-- Согласующий МИ по КК

-- Решения.Расхождения.Просмотр
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (36, 1, 4, 1);
-- Методолог ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (36, 5, 4, 1);
-- Методолог МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (36, 1, 2, 1);
-- Аналитик ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (36, 5, 2, 1);
-- Аналитик МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (36, 5, 8, 1);
-- Согласующий МИ по КК

-- НП.Кол-во расхождений.Все
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (39, 1, 4, 1);
-- Методолог ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (39, 1, 2, 1);
-- Аналитик ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (39, 5, 4, 1);
-- Методолог МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (39, 5, 2, 1);
-- Аналитик МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (39, 5, 8, 1);
-- Согласующий МИ по КК

-- НП.Кол-во расхождений.КНП
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (65, 1, 5, 1);
-- Наблюдатель ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (65, 1, 7, 1);
-- Руководитель ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (65, 1, 3, 1);
-- Инспектор ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (65, 5, 5, 1);
-- Наблюдатель МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (65, 5, 7, 1);
-- Руководитель МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (65, 6, 5, 1);
-- Наблюдатель МИ по ФО
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (65, 6, 7, 1);
-- Руководитель МИ по ФО
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (65, 7, 5, 1);
-- Наблюдатель МИ по Ценам
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (65, 4, 5, 1);
-- Наблюдатель МИ по КН
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (65, 4, 7, 1);
-- Руководитель МИ по КН
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (65, 4, 1, 1);
-- Администратор МИ по КН
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (65, 4, 3, 1);
-- Инспектор МИ по КН
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (65, 3, 5, 1);
-- Наблюдатель УФНС
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (65, 3, 7, 1);
-- Руководитель УФНС
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (65, 2, 5, 1);
-- Наблюдатель ИФНС
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (65, 2, 7, 1);
-- Руководитель ИФНС
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (65, 2, 1, 1);
-- Администратор ИФНС
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (65, 2, 3, 1);
-- Инспектор ИФНС

-- Выборки.Создать шаблон
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (40, 5, 8, 1);
-- Согласующий МИ по КК

-- Выборки.Создать выборку
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (41, 5, 2, 1);
-- Аналитик МИ по КК

-- Выборки.Изменить шаблон
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (42, 5, 8, 1);
-- Согласующий МИ по КК

-- Выборки.Изменить выборку
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (43, 5, 2, 1);
-- Аналитик МИ по КК

-- Выборки.Изменить шаблонную выборку
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (44, 5, 2, 1);
-- Аналитик МИ по КК

-- Выборки.Удалить шаблон
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (45, 5, 8, 1);
-- Согласующий МИ по КК

-- Выборки.Удалить выборку.Черновик
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (46, 5, 2, 1);
-- Аналитик МИ по КК

-- Выборки.Удалить выборку.Согласована
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (47, 5, 8, 1);
-- Согласующий МИ по КК
-- Выборки.Копировать шаблон
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (48, 5, 8, 1);
-- Согласующий МИ по КК

-- Выборки.Просмотреть шаблон
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (49, 5, 4, 1);
-- Методолог МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (49, 1, 2, 1);
-- Аналитик ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (49, 5, 2, 1);
-- Аналитик МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (49, 5, 8, 1);
-- Согласующий МИ по КК

-- Выборки.Просмотреть выборку
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (50, 5, 4, 1);
-- Методолог МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (50, 1, 2, 1);
-- Аналитик ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (50, 5, 2, 1);
-- Аналитик МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (50, 5, 8, 1);
-- Согласующий МИ по КК

-- Выборки.На доработку
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (51, 5, 8, 1);
-- Согласующий МИ по КК

-- Выборки.На согласование
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (52, 5, 2, 1);
-- Аналитик МИ по КК

-- Выборки.Согласовать
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (53, 5, 8, 1);
-- Согласующий МИ по КК

-- Выборки.Сформировать АТ
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (54, 5, 8, 1);
-- Согласующий МИ по КК

-- Выборки.Включить АТ
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (55, 5, 8, 1);
-- Согласующий МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (55, 5, 6, 1);
-- Отправитель МИ по КК

-- Выборки.Отправить АТ
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (56, 5, 6, 1);
-- Отправитель МИ по КК

-- НД_Журнал.Дерево_связей
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (61, 1, 4, 1);
-- Методолог ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (61, 5, 4, 1);
-- Методолог МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (61, 1, 2, 1);
-- Аналитик ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (61, 5, 2, 1);
-- Аналитик МИ по КК

-- НД_Журнал.Дерево_связей.Выгрузка.Все
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (62, 1, 4, 1);
-- Методолог ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (62, 5, 4, 1);
-- Методолог МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (62, 1, 2, 1);
-- Аналитик ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (62, 5, 2, 1);
-- Аналитик МИ по КК

-- НД_Журнал.Дерево_связей.Выгрузка.Открытые
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (63, 1, 4, 1);
-- Методолог ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (63, 5, 4, 1);
-- Методолог МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (63, 1, 2, 1);
-- Аналитик ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (63, 5, 2, 1);
-- Аналитик МИ по КК

-- Налогоплательщик.Запросы_в_банк
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (60, 2, 3, 2);
-- Инспектор ИФНС
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (60, 8, 3, 2);
-- Инспектор УФНС 0400
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (60, 4, 3, 2);
-- Инспектор МИ по КН

-- НД.КНП Документы.Открыть АТ
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (66, 1, 4, 1);
-- Методолог ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (66, 1, 2, 1);
-- Аналитик ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (66, 1, 5, 1);
-- Наблюдатель ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (66, 1, 7, 1);
-- Руководитель ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (66, 1, 3, 1);
-- Инспектор ЦА

insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (66, 5, 4, 1);
-- Методолог МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (66, 5, 2, 1);
-- Аналитик МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (66, 5, 8, 1);
-- Согласующий МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (66, 5, 5, 1);
-- Наблюдатель МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (66, 5, 7, 1);
-- Руководитель МИ по КК

insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (66, 6, 5, 1);
-- Наблюдатель МИ по ФО
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (66, 6, 7, 1);
-- Руководитель МИ по ФО

insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (66, 7, 5, 1);
-- Наблюдатель МИ по Ценам

insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (66, 3, 5, 1);
-- Наблюдатель УФНС кроме 0400
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (66, 3, 7, 1);
-- Руководитель УФНС кроме 0400

insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (66, 4, 5, 1);
-- Наблюдатель МИ по КН
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (66, 4, 7, 1);
-- Руководитель МИ по КН
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (66, 4, 1, 1);
-- Администратор МИ по КН
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (66, 4, 3, 1);
-- Инспектор МИ по КН

insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (66, 2, 5, 1);
-- Наблюдатель ИФНС
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (66, 2, 7, 1);
-- Руководитель ИФНС
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (66, 2, 1, 1);
-- Администратор ИФНС
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (66, 2, 3, 1);
-- Инспектор ИФНС

insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (66, 8, 5, 1);
-- Наблюдатель УФНС 0400
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (66, 8, 7, 1);
-- Руководитель УФНС 0400
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (66, 8, 1, 1);
-- Администратор УФНС 0400
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (66, 8, 3, 1);
-- Инспектор УФНС 0400

-- Список_АТ.Открыть АТ
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (67, 1, 4, 1);
-- Методолог ЦА
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (67, 1, 2, 1);
-- Аналитик ЦА

insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (67, 5, 4, 1);
-- Методолог МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (67, 5, 2, 1);
-- Аналитик МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (67, 5, 8, 1);
-- Согласующий МИ по КК
insert into NDS2_MRR_USER.RM_OPERATION_TO_GROUP (OPERATION_ID,GROUP_ID,ROLE_ID,LIMIT_TYPE_ID) values (67, 5, 6, 1);
-- Отправитель МИ по КК

commit;
prompt RM_OPERATION_TO_GROUP finished
