﻿delete from NDS2_MRR_USER.CFG_INDEX t where t.table_name in ('DECLARATION_ACTIVE', 'DECLARATION_ACTIVE_SEARCH');

insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (1011, 'DECLARATION_ACTIVE', 'IX_DA_ZIP', 'ZIP, INN_CONTRACTOR, INN_DECLARANT, KPP_EFFECTIVE, FISCAL_YEAR, PERIOD_CODE, TYPE_CODE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (1012, 'DECLARATION_ACTIVE', 'IX_DA_CORR_GROUP_ID', 'INN_CONTRACTOR, INN_DECLARANT, INN_REORGANIZED, KPP_EFFECTIVE, FISCAL_YEAR, PERIOD_CODE, TYPE_CODE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (1013, 'DECLARATION_ACTIVE', 'IX_DA_PERIOD_CORR_ID', 'INN_CONTRACTOR, INN_DECLARANT, KPP_EFFECTIVE, FISCAL_YEAR, PERIOD_EFFECTIVE, TYPE_CODE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (1014, 'DECLARATION_ACTIVE', 'IX_DA_REG_NAME', 'REGION_TITLE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (1015, 'DECLARATION_ACTIVE', 'IX_DA_SOUN_NAME', 'INSPECTION_TITLE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (1016, 'DECLARATION_ACTIVE', 'IX_DA_SUBMIT_DATE', 'SUBMIT_DATE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (1017, 'DECLARATION_ACTIVE', 'IX_DA_SUBMISSION_DATE', 'SUBMISSION_DATE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (1018, 'DECLARATION_ACTIVE', 'IX_DA_KPP', 'KPP', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (1019, 'DECLARATION_ACTIVE', 'IX_DA_NAME', 'NAME_SHORT', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (1020, 'DECLARATION_ACTIVE', 'IX_DA_REGION', 'REGION_CODE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (1021, 'DECLARATION_ACTIVE', 'IX_DA_SONO', 'SONO_CODE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (1022, 'DECLARATION_ACTIVE', 'IX_DA_NDS_TOTAL', 'NDS_TOTAL', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (1023, 'DECLARATION_ACTIVE', 'IX_DA_CH8_AMNT', 'CH8_DISCREPANCY_AMT', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (1024, 'DECLARATION_ACTIVE', 'IX_DA_CH9_AMNT', 'CH9_DISCREPANCY_AMT', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (1025, 'DECLARATION_ACTIVE', 'IX_DA_CH8_VAT', 'CH8_NDS', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (1026, 'DECLARATION_ACTIVE', 'IX_DA_CH9_VAT', 'CH9_NDS', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (1027, 'DECLARATION_ACTIVE', 'IX_DA_SUR', 'SUR_CODE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (1028, 'DECLARATION_ACTIVE', 'IX_DA_INN', 'INN_DECLARANT', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (1029, 'DECLARATION_ACTIVE', 'IX_DA_REG_NUMBER', 'REG_NUMBER', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (1031, 'DECLARATION_ACTIVE', 'IX_DA_ZIP_TYPE', 'ZIP, TYPE_CODE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (1032, 'DECLARATION_ACTIVE', 'IX_DA_ZIP_KNP', 'ZIP, STATUS_KNP_CODE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type, op_parallel_count)
values (1033, 'DECLARATION_ACTIVE', 'IX_DA_INND_KPPE', 'INN_DECLARANT, KPP_EFFECTIVE', 'NONUNIQUE', 16);
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type, op_parallel_count)
values (1034, 'DECLARATION_ACTIVE', 'IX_DA_INNC_KPPE', 'INN_CONTRACTOR, KPP_EFFECTIVE', 'NONUNIQUE', 16);
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type, op_parallel_count)
values (1035, 'DECLARATION_ACTIVE', 'IX_DA_SID_SONO', 'INSPECTOR_SID, SONO_CODE', 'NONUNIQUE', 16);
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type, op_parallel_count)
values (1036, 'DECLARATION_ACTIVE', 'IX_DA_ACTORKEY', 'INN_CONTRACTOR, KPP_EFFECTIVE, PERIOD_CODE, FISCAL_YEAR, TYPE_CODE', 'NONUNIQUE', 16);

insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type) 
values (1051, 'DECLARATION_ACTIVE_SEARCH', 'IX_DA_ID_CORNUM_SRCH', 'ID, CORRECTION_NUMBER', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (1052, 'DECLARATION_ACTIVE_SEARCH', 'IX_DA_CORR_ID_SRCH', 'NDS2_DECL_CORR_ID', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (1053, 'DECLARATION_ACTIVE_SEARCH', 'IX_DA_ZIP_SRCH', 'DECLARATION_VERSION_ID', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (1054, 'DECLARATION_ACTIVE_SEARCH', 'IX_DA_REG_NAME_SRCH', 'REGION_NAME', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (1055, 'DECLARATION_ACTIVE_SEARCH', 'IX_DA_SOUN_NAME_SRCH', 'SOUN_NAME', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (1056, 'DECLARATION_ACTIVE_SEARCH', 'IX_DA_DECL_DATE_SRCH', 'DECL_DATE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (1057, 'DECLARATION_ACTIVE_SEARCH', 'IX_DA_SUBMISSION_DATE_SRCH', 'SUBMISSION_DATE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (1058, 'DECLARATION_ACTIVE_SEARCH', 'IX_DA_KPP_SRCH', 'KPP', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (1059, 'DECLARATION_ACTIVE_SEARCH', 'IX_DA_NAME_SRCH', 'NAME', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (1060, 'DECLARATION_ACTIVE_SEARCH', 'IX_DA_REGION_SRCH', 'REGION_CODE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (1061, 'DECLARATION_ACTIVE_SEARCH', 'IX_DA_SOUN_SRCH', 'SOUN_CODE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (1062, 'DECLARATION_ACTIVE_SEARCH', 'IX_DA_COMPENSATION_SRCH', 'COMPENSATION_AMNT', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (1063, 'DECLARATION_ACTIVE_SEARCH', 'IX_DA_CH8_AMNT_SRCH', 'CH8_DEALS_AMNT_TOTAL', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (1064, 'DECLARATION_ACTIVE_SEARCH', 'IX_DA_CH9_AMNT_SRCH', 'CH9_DEALS_AMNT_TOTAL', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (1065, 'DECLARATION_ACTIVE_SEARCH', 'IX_DA_CH8_VAT_SRCH', 'CH8_NDS', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (1066, 'DECLARATION_ACTIVE_SEARCH', 'IX_DA_CH9_VAT_SRCH', 'CH9_NDS', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (1067, 'DECLARATION_ACTIVE_SEARCH', 'IX_DA_SUR_SRCH', 'SUR_CODE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (1068, 'DECLARATION_ACTIVE_SEARCH', 'IX_DA_INN_SRCH', 'INN', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (1069, 'DECLARATION_ACTIVE_SEARCH', 'IX_DA_SEOD_ID_SRCH', 'SEOD_DECL_ID', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (1070, 'DECLARATION_ACTIVE_SEARCH', 'IX_DA_ASK_ID_SRCH', 'ASK_DECL_ID', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (1071, 'DECLARATION_ACTIVE_SEARCH', 'IX_DAS_ACTORKEY', 'INN_CONTRACTOR, KPP_EFFECTIVE, TAX_PERIOD, FISCAL_YEAR, DECL_TYPE_CODE', 'NONUNIQUE');

insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (1072, 'DECLARATION_ACTIVE_SEARCH', 'IX_DAS_SID_ONE', 'ASSIGNEE_SID', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values (1073, 'DECLARATION_ACTIVE_SEARCH', 'IX_DAS_SID_REGION', 'ASSIGNEE_SID, region_code', 'NONUNIQUE');

insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type, op_parallel_count, is_local)
values (1001, 'DECLARATION_ACTIVE', 'IX_DA_SUR_L', 'PARTITION_ID, SUR_CODE', 'NONUNIQUE', 16, 1);
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type, op_parallel_count, is_local)
values (1002, 'DECLARATION_ACTIVE', 'IX_DA_SONOSUR_L', 'PARTITION_ID, SUBPARTITION_ID, SONO_CODE, SUR_CODE', 'NONUNIQUE', 16, 1);
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type, op_parallel_count, is_local)
values (1003, 'DECLARATION_ACTIVE', 'IX_DA_JOINK', 'JOIN_K', 'UNIQUE', 16, 0);
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type, op_parallel_count, is_local)
values (1004, 'DECLARATION_ACTIVE', 'IX_DA_NDSW_L', 'PARTITION_ID, SUBPARTITION_ID, NDS_WEIGHT', 'NONUNIQUE', 16, 1);
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type, op_parallel_count, is_local)
values (1005, 'DECLARATION_ACTIVE', 'IX_DA_SIGN_L', 'PARTITION_ID, SUBPARTITION_ID, SIGN_CODE', 'NONUNIQUE', 16, 1);


insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type, op_parallel_count, is_local)
values (1041, 'DECLARATION_ACTIVE_SEARCH', 'IX_DAS_SUR_L', 'PARTITION_ID, SUR_CODE', 'NONUNIQUE', 16, 1);
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type, op_parallel_count, is_local)
values (1042, 'DECLARATION_ACTIVE_SEARCH', 'IX_DAS_SONOSUR_L', 'PARTITION_ID, SUBPARTITION_ID, SOUN_CODE, SUR_CODE', 'NONUNIQUE', 16, 1);
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type, op_parallel_count, is_local)
values (1043, 'DECLARATION_ACTIVE_SEARCH', 'IX_DAS_SONO_L', 'PARTITION_ID, SUBPARTITION_ID, SOUN_CODE', 'NONUNIQUE', 16, 1);
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type, op_parallel_count, is_local)
values (1044, 'DECLARATION_ACTIVE_SEARCH', 'IX_DAS_NDSW_L', 'PARTITION_ID, SUBPARTITION_ID, NDS_WEIGHT', 'NONUNIQUE', 16, 1);
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type, op_parallel_count, is_local)
values (1045, 'DECLARATION_ACTIVE_SEARCH', 'IX_DAS_SIGN_L', 'PARTITION_ID, SUBPARTITION_ID, DECL_SIGN', 'NONUNIQUE', 16, 1);
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type, op_parallel_count, is_local)
values (1046, 'DECLARATION_ACTIVE_SEARCH', 'IX_DAS_JOINK', 'JOIN_K', 'UNIQUE', 16, 0);

insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type, op_parallel_count, is_local)
values (1081, 'DECLARATION_HISTORY', 'IX_DH_INN_FY', 'TO_NUMBER(INN_CONTRACTOR), FISCAL_YEAR', 'NONUNIQUE', 16, 0);

insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type, op_parallel_count, is_local)
values (1082, 'DECLARATION_HISTORY', 'IX_DH_SEODKEY', 'INN_CONTRACTOR, KPP, CORRECTION_NUMBER, PERIOD_CODE, FISCAL_YEAR, IS_ACTIVE', 'NONUNIQUE', 16, 0);

update NDS2_MRR_USER.CFG_INDEX set op_parallel_count = 16;

commit;
