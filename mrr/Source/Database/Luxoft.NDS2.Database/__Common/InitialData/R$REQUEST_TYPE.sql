﻿begin
  execute immediate 'alter table NDS2_MRR_USER.MD_DATA_REQUEST drop constraint FK$MD_DATA_REQUEST$TYPE_ID';
  exception when others then null;
end;
/

truncate table NDS2_MRR_USER.R$REQUEST_TYPE;

insert into NDS2_MRR_USER.R$REQUEST_TYPE (id, TARGET_TABLE, Description, Clean_Weekly, Create_Date)
values
(1, 'HIVE_DISCREPANCY_DECL_CACHE', 'Кэш расхождений НД', 'Y', sysdate);

insert into NDS2_MRR_USER.R$REQUEST_TYPE (id, TARGET_TABLE, Description, Clean_Weekly, Create_Date)
values
(2, 'SOV_INVOICE_ALL, SOV_INVOICE_BUY_ACCEPT_ALL, SOV_INVOICE_BUYER_ALL, SOV_INVOICE_OPERATION_ALL, SOV_INVOICE_PAYMENT_DOC_ALL, SOV_INVOICE_SELLER_ALL', 'Кэш записей о СФ расхождения', 'Y', sysdate);

commit;
