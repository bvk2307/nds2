﻿truncate table NDS2_MRR_USER.RM_OPERATION;

insert into NDS2_MRR_USER.RM_OPERATION
  values(1, 'ПЗ.Отчет.Страна', 'По стране');

insert into NDS2_MRR_USER.RM_OPERATION
  values(2, 'ПЗ.Отчет.Регион', 'По региону');

insert into NDS2_MRR_USER.RM_OPERATION
  values(3, 'ПЗ.Отчет.Инспекция', 'По инспекции');

insert into NDS2_MRR_USER.RM_OPERATION
  values(4, 'ПЗ.Отчет.Инспектор', 'По инспекции');

insert into NDS2_MRR_USER.RM_OPERATION
  values(5, 'ПЗ.Отчет.Инспекции_региона', 'По региону');

insert into NDS2_MRR_USER.RM_OPERATION
  values(6, 'Акты.Отчет по стране', 'По стране');

insert into NDS2_MRR_USER.RM_OPERATION
  values(7, 'Акты.Отчет по федеральному округу', 'По федеральному округу');

insert into NDS2_MRR_USER.RM_OPERATION
  values(8, 'Акты.Отчет по региону', 'По региону');

insert into NDS2_MRR_USER.RM_OPERATION
  values(9, 'Акты.Отчет по инспекции', 'По инспекции');

insert into NDS2_MRR_USER.RM_OPERATION
  values(10, 'Пояснение_Ответ.Редактирование', 'Ввод неформализованных пояснений и ответов на АИ');

insert into NDS2_MRR_USER.RM_OPERATION
  values(11, 'Пояснение_Ответ.Просмотр', 'Просмотр пояснений и ответов на АИ');

insert into NDS2_MRR_USER.RM_OPERATION 
  values(12, 'ООР', 'Доступ к окну оперативной работы');

insert into NDS2_MRR_USER.RM_OPERATION 
  values(13, 'НД.Назначить', 'Назначить ответственного для НД');

insert into NDS2_MRR_USER.RM_OPERATION 
  values(14, 'Макроотчеты.Фоновые показатели.Страна', 'Отчет Фоновые показатели по стране');

insert into NDS2_MRR_USER.RM_OPERATION 
  values(15, 'Макроотчеты.Фоновые показатели.Федеральные округа', 'Отчет Фоновые показатели по федеральным округам');

insert into NDS2_MRR_USER.RM_OPERATION 
  values(16, 'Макроотчеты.Фоновые показатели.Регионы', 'Отчет Фоновые показатели по регионам');

insert into NDS2_MRR_USER.RM_OPERATION 
  values(17, 'Макроотчеты.Фоновые показатели.Инспекции', 'Отчет Фоновые показатели по инспекциям');

insert into NDS2_MRR_USER.RM_OPERATION(id, name, description)
  values (18, 'Изменения в версии', 'Доступ к узлу "Изменения в версии"');

insert into NDS2_MRR_USER.RM_OPERATION(id, name, description)
  values (19, 'Декларации', 'Доступ к узлу "Декларации"');

insert into NDS2_MRR_USER.RM_OPERATION(id, name, description)
  values (20, 'Список выборок', 'Доступ к узлу "Список выборок"');

insert into NDS2_MRR_USER.RM_OPERATION(id, name, description)
  values (21, 'Список АТ', 'Доступ к узлу "Список автотребований по СФ"');

insert into NDS2_MRR_USER.RM_OPERATION(id, name, description)
  values (22, 'Макроотчёты.НДС и расхождения', 'Доступ к узлу "Макроотчёты. Отчёты по НДС и расхождениям"');

insert into NDS2_MRR_USER.RM_OPERATION(id, name, description)
  values (23, 'Макроотчёты.Эффективность', 'Доступ к узлу "Макроотчёты. Показатели эффективности"');

insert into NDS2_MRR_USER.RM_OPERATION(id, name, description)
  values (24, 'Параметры АСК', 'Доступ к узлу "Параметры АСК"');  

insert into NDS2_MRR_USER.RM_OPERATION(id, name, description)
  values (25, 'Настройка списков', 'Доступ к узлу "Настройка списков"');

insert into NDS2_MRR_USER.RM_OPERATION(id, name, description)
  values (26, 'Календарь', 'Доступ к узлу "Настройка календаря"');

insert into NDS2_MRR_USER.RM_OPERATION(id, name, description)
  values (27, 'Справочники', 'Доступ к узлу "Справочники"');

insert into NDS2_MRR_USER.RM_OPERATION(id, name, description)
  values (28, 'НД.Открыть', 'Открытие карточки НД/Журнала');

insert into NDS2_MRR_USER.RM_OPERATION(id, name, description)
  values (29, 'НД.Контрагент', 'Переход к данным контрагента из раздела НД/Журнала');

insert into NDS2_MRR_USER.RM_OPERATION(id, name, description)
  values (30, 'НД.Отчёт', 'Выгрузка из НД/Журналов разделов с записями по СФ в MS Excel');

insert into NDS2_MRR_USER.RM_OPERATION(id, name, description)
  values (31, 'НД.Общие сведения', 'Просмотр области «Общие сведения» в НД/Журнале');

insert into NDS2_MRR_USER.RM_OPERATION(id, name, description)
  values (32, 'Расхождения.Все', 'Просмотр полного списка расхождений в НД/Журнале');

insert into NDS2_MRR_USER.RM_OPERATION(id, name, description)
  values (33, 'Акты.Расхождения.Изменить', 'Редактирование расхождений, включенных в акт');

insert into NDS2_MRR_USER.RM_OPERATION(id, name, description)
  values (34, 'Акты.Расхождения.Просмотр', 'Просмотр расхождений, включенных в акт');

insert into NDS2_MRR_USER.RM_OPERATION(id, name, description)
  values (35, 'Решения.Расхождения.Изменить', 'Редактирование расхождений, включенных в решение');

insert into NDS2_MRR_USER.RM_OPERATION(id, name, description)
  values (36, 'Решения.Расхождения.Просмотр', 'Просмотр расхождений, включенных в решение');

insert into NDS2_MRR_USER.RM_OPERATION(id, name, description)
  values (37, 'Учёт расхождений КНП', 'Учёт расхождений КНП');

insert into NDS2_MRR_USER.RM_OPERATION(id, name, description)
  values (38, 'НД.Сброс ПЗИ', 'Сброс признаков значимых изменений');

insert into NDS2_MRR_USER.RM_OPERATION(id, name, description)
  values (39, 'НП.Кол-во расхождений.Все', 'Просмотр общего кол-ва расхождений, включая КНП');

insert into NDS2_MRR_USER.RM_OPERATION(id, name, description)
  values (40, 'Выборки.Создать шаблон', 'Создание шаблона выборки');

insert into NDS2_MRR_USER.RM_OPERATION(id, name, description)
  values (41, 'Выборки.Создать выборку', 'Создание выборки');

insert into NDS2_MRR_USER.RM_OPERATION(id, name, description)
  values (42, 'Выборки.Изменить шаблон', 'Изменение шаблона выборки');

insert into NDS2_MRR_USER.RM_OPERATION(id, name, description)
  values (43, 'Выборки.Изменить выборку', 'Изменение выборки');

insert into NDS2_MRR_USER.RM_OPERATION(id, name, description)
  values (44, 'Выборки.Изменить шаблонную выборку', 'Изменение выборки, созданной по шаблону');

insert into NDS2_MRR_USER.RM_OPERATION(id, name, description)
  values (45, 'Выборки.Удалить шаблон', 'Удаление шаблона выборки');

insert into NDS2_MRR_USER.RM_OPERATION(id, name, description)
  values (46, 'Выборки.Удалить выборку.Черновик', 'Удаление выборки в статусе черновик и ошибка загрузки');

insert into NDS2_MRR_USER.RM_OPERATION(id, name, description)
  values (47, 'Выборки.Удалить выборку.Согласована', 'Удаление выборки в статусе согласована и на согласовании');

insert into NDS2_MRR_USER.RM_OPERATION(id, name, description)
  values (48, 'Выборки.Копировать шаблон', 'Копирование шаблона выборки');

insert into NDS2_MRR_USER.RM_OPERATION(id, name, description)
  values (49, 'Выборки.Просмотреть шаблон', 'Просмотр шаблона выборки');

insert into NDS2_MRR_USER.RM_OPERATION(id, name, description)
  values (50, 'Выборки.Просмотреть выборку', 'Просмотр выборки');

insert into NDS2_MRR_USER.RM_OPERATION(id, name, description)
  values (51, 'Выборки.На доработку', 'Отправка выборки на доработку');

insert into NDS2_MRR_USER.RM_OPERATION(id, name, description)
  values (52, 'Выборки.На согласование', 'Отправка выборки на согласование');

insert into NDS2_MRR_USER.RM_OPERATION(id, name, description)
  values (53, 'Выборки.Согласовать', 'Согласование выборки');

insert into NDS2_MRR_USER.RM_OPERATION(id, name, description)
  values (54, 'Выборки.Сформировать АТ', 'Формирование АТ по СФ');

insert into NDS2_MRR_USER.RM_OPERATION(id, name, description)
  values (55, 'Выборки.Включить АТ', 'Включение/исключения АТ по СФ из отправки налогоплательщику');

insert into NDS2_MRR_USER.RM_OPERATION(id, name, description)
  values (56, 'Выборки.Отправить АТ', 'Отправить АТ по СФ налогоплательщику');

insert into NDS2_MRR_USER.RM_OPERATION(id, name, description)
  values (60, 'Налогоплательщик.Запросы_в_банк', 'Формирование запроса в банк');
  
insert into NDS2_MRR_USER.RM_OPERATION(id, name, description)
  values (61, 'НД_Журнал.Дерево_связей', 'Построение дерева связей');

insert into NDS2_MRR_USER.RM_OPERATION(id, name, description)
  values (62, 'НД_Журнал.Дерево_связей.Выгрузка.Все', 'Выгрузка всех узлов дерева связей в MS Excel (6 уровней)');

insert into NDS2_MRR_USER.RM_OPERATION(id, name, description)
  values (63, 'НД_Журнал.Дерево_связей.Выгрузка.Открытые', 'Выгрузка открытых узлов дерева связей в MS Excel');

insert into NDS2_MRR_USER.RM_OPERATION(id, name, description)
  values (64, 'НД_Журнал.Дерево_связей.Выгрузка.Открытые_6', 'Выгрузка открытых узлов дерева связей в MS Excel (6 уровней)');
 
insert into NDS2_MRR_USER.RM_OPERATION(id, name, description)
  values (65, 'НП.Кол-во расхождений.КНП', 'Просмотр кол-ва расхождений КНП');

insert into NDS2_MRR_USER.RM_OPERATION(id, name, description)
  values (66, 'НД.КНП Документы.Открыть АТ_АИ', 'Открытие карточки АТ_АИ из вкладки КНП документы карточки декларации');

insert into NDS2_MRR_USER.RM_OPERATION(id, name, description)
  values (67, 'Список_АТ.Открыть АТ', 'Открытие карточки АТ из списка АТ');

commit;
