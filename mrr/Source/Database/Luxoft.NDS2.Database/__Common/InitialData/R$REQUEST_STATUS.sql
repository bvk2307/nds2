﻿begin
  execute immediate 'alter table NDS2_MRR_USER.MD_DATA_REQUEST drop constraint FK$MD_DATA_REQUEST$STATUS';
  exception when others then null;
end;
/
truncate table NDS2_MRR_USER.R$REQUEST_STATUS;

insert into NDS2_MRR_USER.R$REQUEST_STATUS (id, description) values (1, 'Новый запрос');
insert into NDS2_MRR_USER.R$REQUEST_STATUS (id, description) values (2, 'Помечен к выгрузке');
insert into NDS2_MRR_USER.R$REQUEST_STATUS (id, description) values (3, 'Выгрузка начата');
insert into NDS2_MRR_USER.R$REQUEST_STATUS (id, description) values (4, 'Выгрузка завершена');
insert into NDS2_MRR_USER.R$REQUEST_STATUS (id, description) values (5, 'Ошибка выгрузки');

commit;
