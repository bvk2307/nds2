﻿truncate table NDS2_MRR_USER.DICT_EXPLAIN_TYPE;

insert into NDS2_MRR_USER.DICT_EXPLAIN_TYPE values(1, 'Пояснение по СФ');
insert into NDS2_MRR_USER.DICT_EXPLAIN_TYPE values(2, 'Пояснение по КС');
insert into NDS2_MRR_USER.DICT_EXPLAIN_TYPE values(3, 'Пояснение по ИнОсн');

commit;