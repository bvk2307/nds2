﻿truncate table NDS2_MRR_USER.RM_GROUP;

insert into NDS2_MRR_USER.RM_GROUP (ID, DESCRIPTION)
  values    ( 1, 'ЦА');
insert into NDS2_MRR_USER.RM_GROUP (ID, DESCRIPTION)
  values    (2, 'ИФНС / МРИ');
insert into NDS2_MRR_USER.RM_GROUP (ID, DESCRIPTION)
  values    (3, 'УФНС кроме 0400');
insert into NDS2_MRR_USER.RM_GROUP (ID, DESCRIPTION)
  values    (4, 'МИ по КН');
insert into NDS2_MRR_USER.RM_GROUP (ID, DESCRIPTION)
  values    (5, 'МИ по КК');
insert into NDS2_MRR_USER.RM_GROUP (ID, DESCRIPTION)
  values    (6, 'МИ по ФО');
insert into NDS2_MRR_USER.RM_GROUP (ID, DESCRIPTION)
  values    (7, 'МИ по Ценам');
insert into NDS2_MRR_USER.RM_GROUP (ID, DESCRIPTION)
  values    (8, 'УФНС 0400');

commit;
