﻿truncate table NDS2_MRR_USER.CFG_RESTRICTION_CHAINS;

insert into NDS2_MRR_USER.CFG_RESTRICTION_CHAINS (role_id, group_id, not_restricted, max_level_sales, max_level_purchase, max_navigator_chains, allow_purchase, description)
values (1, null, 0, 6, 6, 7, 1, 'Аналитик, по умолчанию - не более 6 уровней в обоих направлениях');
insert into NDS2_MRR_USER.CFG_RESTRICTION_CHAINS (role_id, group_id, not_restricted, max_level_sales, max_level_purchase, max_navigator_chains, allow_purchase, description)
values (1, 0, 1, 0, 0, 0, 1, 'Аналитик, ЦА - без ограничений');
insert into NDS2_MRR_USER.CFG_RESTRICTION_CHAINS (role_id, group_id, not_restricted, max_level_sales, max_level_purchase, max_navigator_chains, allow_purchase, description)
values (1, 1, 1, 0, 0, 0, 1, 'Аналитик, МИ по КК - без ограничений');
insert into NDS2_MRR_USER.CFG_RESTRICTION_CHAINS (role_id, group_id, not_restricted, max_level_sales, max_level_purchase, max_navigator_chains, allow_purchase, description)
values (2, null, 1, 0, 0, 0, 1, 'Согласующий - без ограничений');
insert into NDS2_MRR_USER.CFG_RESTRICTION_CHAINS (role_id, group_id, not_restricted, max_level_sales, max_level_purchase, max_navigator_chains, allow_purchase, description)
values (3, null, 1, 0, 0, 0, 1, 'Методолог - без ограничений');
insert into NDS2_MRR_USER.CFG_RESTRICTION_CHAINS (role_id, group_id, not_restricted, max_level_sales, max_level_purchase, max_navigator_chains, allow_purchase, description)
values (4, null, 0, 6, 6, 3, 0, 'Инспектор, по умолчанию - не более 6 уровней в обоих направлениях. Совещание у Егорова от 12.01.2016');
insert into NDS2_MRR_USER.CFG_RESTRICTION_CHAINS (role_id, group_id, not_restricted, max_level_sales, max_level_purchase, max_navigator_chains, allow_purchase, description)
values (4, 0, 0, 6, 6, 6, 1, 'Инспектор, ЦА - не более 6 уровней в обоих направлениях');
insert into NDS2_MRR_USER.CFG_RESTRICTION_CHAINS (role_id, group_id, not_restricted, max_level_sales, max_level_purchase, max_navigator_chains, allow_purchase, description)
values (4, 2, 0, 6, 6, 6, 1, 'Инспектор, МИ по крупнейшим - не более 6 уровней в обоих направлениях');
insert into NDS2_MRR_USER.CFG_RESTRICTION_CHAINS (role_id, group_id, not_restricted, max_level_sales, max_level_purchase, max_navigator_chains, allow_purchase, description)
values (4, 3, 0, 6, 6, 6, 1, 'Инспектор, УФНС - не более 6 уровней в обоих направлениях');
insert into NDS2_MRR_USER.CFG_RESTRICTION_CHAINS (role_id, group_id, not_restricted, max_level_sales, max_level_purchase, max_navigator_chains, allow_purchase, description)
values (6, 0, 1, 0, 0, 0, 1, 'Руководитель ЦА - без ограничений');
insert into NDS2_MRR_USER.CFG_RESTRICTION_CHAINS (role_id, group_id, not_restricted, max_level_sales, max_level_purchase, max_navigator_chains, allow_purchase, description)
values (6, 1, 1, 0, 0, 0, 1, 'Руководитель МИ по КК - без ограничений');
insert into NDS2_MRR_USER.CFG_RESTRICTION_CHAINS (role_id, group_id, not_restricted, max_level_sales, max_level_purchase, max_navigator_chains, allow_purchase, description)
values (6, null, 0, 6, 6, 3, 1, 'Руководитель (по умолчанию) - не более 6 уровней в обоих направлениях');
insert into NDS2_MRR_USER.CFG_RESTRICTION_CHAINS (role_id, group_id, not_restricted, max_level_sales, max_level_purchase, max_navigator_chains, allow_purchase, description)
values (7, null, 0, 6, 6, 3, 1, 'Наблюдатель - не более 6 уровней в обоих направлениях');
insert into NDS2_MRR_USER.CFG_RESTRICTION_CHAINS (role_id, group_id, not_restricted, max_level_sales, max_level_purchase, max_navigator_chains, allow_purchase, description)
values (7, 0, 1, 0, 0, 0, 1, 'Наблюдатель ЦА - без ограничений');
insert into NDS2_MRR_USER.CFG_RESTRICTION_CHAINS (role_id, group_id, not_restricted, max_level_sales, max_level_purchase, max_navigator_chains, allow_purchase, description)
values (7, 1, 1, 0, 0, 0, 1, 'Наблюдатель МИ по КК - без ограничений');

commit;
       
       
