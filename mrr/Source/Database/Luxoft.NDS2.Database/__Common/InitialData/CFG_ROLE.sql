﻿truncate table NDS2_MRR_USER.CFG_ROLE;

insert into NDS2_MRR_USER.CFG_ROLE (id, name)
values (1, 'Аналитик');
insert into NDS2_MRR_USER.CFG_ROLE (id, name)
values (2, 'Согласующий');
insert into NDS2_MRR_USER.CFG_ROLE (id, name)
values (3, 'Методолог');
insert into NDS2_MRR_USER.CFG_ROLE (id, name)
values (4, 'Инспектор');
insert into NDS2_MRR_USER.CFG_ROLE (id, name)
values (6, 'Руководитель');
insert into NDS2_MRR_USER.CFG_ROLE (id, name)
values (7, 'Наблюдатель');

commit;
