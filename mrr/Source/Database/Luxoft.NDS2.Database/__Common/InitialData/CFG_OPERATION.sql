﻿prompt Loading CFG_OPERATION...
truncate table NDS2_MRR_USER.CFG_OPERATION;
insert into NDS2_MRR_USER.CFG_OPERATION (id, name)
values (1, 'Декларация.Просмотр_списка');
insert into NDS2_MRR_USER.CFG_OPERATION (id, name)
values (3, 'ООР.Сбросить_признак');
insert into NDS2_MRR_USER.CFG_OPERATION (id, name)
values (4, 'НД.Выгрузка_в_EXCEL');
insert into NDS2_MRR_USER.CFG_OPERATION (id, name)
values (5, 'Журнал.Выгрузка_в_EXCEL');
insert into NDS2_MRR_USER.CFG_OPERATION (id, name)
values (6, 'Декларация.Дерево_связей');
Insert into NDS2_MRR_USER.CFG_OPERATION (ID,NAME) 
values (7, 'Пояснение_Ответ.Редактирование');
Insert into NDS2_MRR_USER.CFG_OPERATION (ID,NAME) 
values (8, 'Навигатор.Просмотр');
Insert into NDS2_MRR_USER.CFG_OPERATION (ID,NAME) 
values (9, 'Налогоплательщик.Запросы_в_банк');
commit;
prompt Loading CFG_OPERATION...Done.
