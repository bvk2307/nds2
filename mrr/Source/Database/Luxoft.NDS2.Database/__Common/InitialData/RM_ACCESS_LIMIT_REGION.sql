﻿truncate table NDS2_MRR_USER.RM_ACCESS_LIMIT_REGION;

-- УФНС
insert into NDS2_MRR_USER.RM_ACCESS_LIMIT_REGION(GROUP_ID, SONO_CODE, REGION_CODE)
select 3,t.s_code,nvl(s.region_code, substr(t.s_code,1,2))
from nds2_mrr_user.ext_sono t
left join nds2_mrr_user.sono_codes s on s.sono_code = t.s_code and s.sono_code = 6
where t.s_code like '__00'
  and t.s_code not in (select sono_code from nds2_mrr_user.sono_codes where type_id != 6);

-- УФНС 0400
insert into NDS2_MRR_USER.RM_ACCESS_LIMIT_REGION(GROUP_ID, SONO_CODE, REGION_CODE)
select 8,x1.sono_code,x1.region_code
 from nds2_mrr_user.sono_codes x1
 join nds2_mrr_user.sono_codes x2 on x2.sono_code = x1.sono_code and x2.type_id = 7
 where x1.type_id = 6;

commit;
