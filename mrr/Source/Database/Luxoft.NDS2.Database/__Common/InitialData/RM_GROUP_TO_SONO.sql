﻿delete from nds2_mrr_user.rm_group_to_sono;
-- Группа ЦА
insert into nds2_mrr_user.rm_group_to_sono (group_id,sono_code) select 1,sono_code from nds2_mrr_user.sono_codes where type_id = 1;

-- Группа ИФНС/МРИ
insert into nds2_mrr_user.rm_group_to_sono (group_id,sono_code) 
select 2,all_sono.s_code
from nds2_mrr_user.ext_sono all_sono
where all_sono.s_code not like '__00' 
  and all_sono.s_code not in (select sono_code from nds2_mrr_user.sono_codes where type_id != 7);
  
-- Группа УФНС кроме 0400
insert into nds2_mrr_user.rm_group_to_sono (group_id,sono_code)
select 3,t.s_code
from nds2_mrr_user.ext_sono t
where t.s_code like '__00'
  and t.s_code not in (select sono_code from nds2_mrr_user.sono_codes where type_id != 6);
  
-- Группа МИ по КН
insert into nds2_mrr_user.rm_group_to_sono (group_id,sono_code) select 4,sono_code from nds2_mrr_user.sono_codes where type_id = 2;

-- Группа МИ по КК
insert into nds2_mrr_user.rm_group_to_sono (group_id,sono_code) select 5,sono_code from nds2_mrr_user.sono_codes where type_id = 9;

-- Группа МИ по ФО
insert into nds2_mrr_user.rm_group_to_sono (group_id,sono_code) select 6,sono_code from nds2_mrr_user.sono_codes where type_id = 4;

-- Группа МИ по Ценам
insert into nds2_mrr_user.rm_group_to_sono (group_id,sono_code) select 7,sono_code from nds2_mrr_user.sono_codes where type_id = 3;

-- Группа 0400
insert into nds2_mrr_user.rm_group_to_sono (group_id,sono_code)
select 8,x1.sono_code
 from nds2_mrr_user.sono_codes x1
 join nds2_mrr_user.sono_codes x2 on x2.sono_code = x1.sono_code and x2.type_id = 7
 where x1.type_id = 6;
commit;
  
