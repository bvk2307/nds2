﻿create or replace force view NDS2_MRR_USER.V$SELECTION_DISCREPANCY as
select
     sd.selection_id as SelectionId
    ,sd.zip as Zip
    ,sd.is_in_process as Included
    ,sd.create_date as FoundDate
    ,sd.TYPE_CODE as DiscrepancyType
    ,nvl(sd.SUBTYPE_CODE, 0) as Subtype
    ,dictRule.description as CompareRule
    ,sd.amount
    ,sd.discrepancy_id as DiscrepancyId
    ,sd.TAXPAYER_SIDE as SidePrimaryProcessing
    ,sd.region_code as BuyerRegionCode
    ,buyer_ssrf.s_name as BuyerRegionName
    ,sd.SONO_CODE as BuyerSonoCode
    ,buyer_sono.s_name as BuyerSonoName
    ,sd.inn as BuyerInn 
    ,sd.inn_declarant as BuyerInnDeclarant 
    ,sd.kpp as BuyerKpp
    ,sd.kpp_declarant as BuyerKppDeclarant
    ,nvl(sd.KS_R8R3_EQUALITY, 0) as BuyerControlRatio
    ,sd.decl_sign as BuyerDeclarationSign 
    ,sd.decl_submit_date as BuyerDeclarationSubmitDate 
    ,sd.decl_nds_amount as BuyerDeclarationNdsAmount 
    ,dtp.description || ' ' || sd.decl_year as BuyerDeclarationPeriod
    ,(to_number(sd.decl_year)*1000 + dtp.sort_order) as BuyerDeclarationPeriodId 
    ,sd.kvo as OperationCodesBit 
    ,nvl(sd.INVOICE_CHAPTER, 99) as BuyerChapter
    ,case when sd.sur_code is not null and sd.sur_code < 4 then sd.sur_code else 4 end as BuyerSur 
    ,sd.CONTRACTOR_INN as SellerInn 
    ,sd.contractor_inn_declarant as SellerInnDeclarant 
    ,sd.contractor_kpp as SellerKpp 
    ,sd.contractor_kpp_declarant as SellerKppDeclarant 
    ,nvl(sd.CONTRACTOR_KS_1_27, 0) as SellerControlRatio
    ,sd.contractor_decl_sign as SellerDeclarationSign 
    ,sd.contractor_decl_submit_date as SellerDeclarationSubmitDate 
    ,sd.contractor_decl_nds_amount as SellerDeclarationNdsAmount 
    ,seller_dtp.description || ' ' || sd.contractor_decl_year as SellerDeclarationPeriod
    ,(to_number(sd.contractor_decl_year)*1000 + seller_dtp.sort_order) as SellerDeclarationPeriodId 
    ,nvl(sd.CONTRACTOR_INVOICE_CHAPTER, 99) as SellerChapter 
    ,case when sd.contractor_sur_code is not null and sd.contractor_sur_code < 4 then sd.contractor_sur_code else 4 end as SellerSur 
    ,sd.contractor_region_code as SellerRegionCode 
    ,seller_ssrf.s_name as SellerRegionName
    ,sd.contractor_sono_code as SellerSonoCode 
    ,sd.INVOICE_NUMBER as CreatedByInvoiceNumber
    ,sd.INVOICE_DATE as CreatedByInvoiceDate
    ,seller_sono.s_name as SellerSonoName
	,sd.EXCLUSION_CODE as ExcludeFromClaimType
    ,ers.SORT_INDEX 
from
    SELECTION_DISCREPANCY sd
    left join DICT_COMPARE_RULES dictRule on dictRule.code = sd.COMPARE_RULE
    left join DICT_TAX_PERIOD dtp on dtp.code = sd.decl_period
    left join DICT_TAX_PERIOD seller_dtp on seller_dtp.code = sd.contractor_decl_period
    left join EXT_SSRF buyer_ssrf on buyer_ssrf.s_code = sd.region_code
    left join EXT_SSRF seller_ssrf on seller_ssrf.s_code = sd.contractor_region_code
    left join EXT_SONO buyer_sono on buyer_sono.s_code = sd.sono_code
    left join EXT_SONO seller_sono on seller_sono.s_code = sd.contractor_sono_code	
	left join R$EXCLUDE_REASON ers on ers.id = sd.EXCLUSION_CODE;
