﻿create or replace force view nds2_mrr_user.v$sono_relation
as
select 
s.sono_code,
s.sono_level,
decode(s.sono_level, 1, 'ЦА', 2, 'УФНС', 3, 'ИФНС', '-') as sono_level_description,
s.sono_name,
s.sono_parent,
p.sono_level as sono_parent_level,
decode(p.sono_level, 1, 'ЦА', 2, 'УФНС', 3, 'ИФНС', '-') as sono_parent_level_description,
p.sono_name as sono_parent_name,
r.s_code as region_code,
r.s_name as region_name,
fd.district_id as district_code,
fd.description as district_name
from sono_relation s
left join sono_relation p on s.sono_parent = p.sono_code and s.sono_level - 1 = p.sono_level and p.is_active = 1
left join v$ssrf r on r.s_code = decode(s.sono_level, 3, substr(s.sono_parent, 1,2), 2, substr(s.sono_code, 1,2), 1, '00', '-') 
left join federal_district_region fdr on fdr.region_code = r.s_code
left join federal_district fd on fd.district_id = fdr.district_id
where s.is_active = 1;
