﻿create or replace force view NDS2_MRR_USER.v$Control_Ratio_doc
as
select /*+ leading(dh cr crc crt) use_nl(cr) use_nl(crc) use_nl(crt) use_nl(dh) index(cr) index(crc) index(crt)  */
  d.DECLARATION_VERSION_ID as decl_version_id,
  d.SEOD_DECL_ID as decl_reg_num,
  vw.KodKs,
  vw.LevyaChast,
  vw.PravyaChast,
  crd.doc_id,
  d.SOUN_CODE as KODNO,
  decode(vw.Vypoln, 1, 10, 0, 12, vw.Vypoln) as vypolnCode 
from V$ASKKontrSoontosh vw 
inner join v$declaration d on d.ASK_DECL_ID = vw.IdDekl
left join control_ratio_doc crd on crd.cr_id = vw.ID
where vw.DeleteDate is null;
