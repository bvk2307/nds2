﻿create or replace force view NDS2_MRR_USER.V$RM_ACCESS_CONTEXT as
select
   opr.id as Id
  ,opr.name as Name
  ,opr.description as Description
  ,opr_grp.limit_type_id as LimitType
  ,grp_sono.sono_code as SonoCode
  ,rl.name as RoleName
  ,lmt_rg.region_code as AccessToRegionCode
  ,null as AccessToSonoCode
  ,null as AccessToDistrictId
from  
       RM_OPERATION opr
  join RM_OPERATION_TO_GROUP opr_grp on opr_grp.operation_id = opr.id
  join RM_GROUP_TO_SONO grp_sono on grp_sono.group_id = opr_grp.group_id
  join RM_ROLE rl on rl.id = opr_grp.role_id
  left join RM_ACCESS_LIMIT_REGION lmt_rg 
       on    lmt_rg.group_id = opr_grp.group_id 
         and lmt_rg.sono_code = grp_sono.sono_code
         and opr_grp.limit_type_id = 4;
