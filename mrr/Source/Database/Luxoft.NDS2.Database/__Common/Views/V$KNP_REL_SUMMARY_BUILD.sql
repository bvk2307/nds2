﻿CREATE OR REPLACE VIEW NDS2_MRR_USER.V$KNP_REL_SUMMARY_BUILD
AS select /*+ NO_INDEX(T) */
     q.INN
	,q.INN_DECLARANT
    ,q.INN_REORGANIZED
    ,q.KPP_EFFECTIVE
    ,q.TYPE_CODE
    ,q.PERIOD_CODE
    ,q.FISCAL_YEAR
    ,q.INN_CONTRACTOR
    ,q.INN_CONTRACTOR_DECLARANT
    ,q.KPP_EFFECTIVE_CONTRACTOR
    ,q.IS_IMPORT
    ,q.GAP_AMOUNT_8
    ,q.GAP_QUANTITY_8
    ,q.OTHER_AMOUNT_8
    ,q.OTHER_QUANTITY_8
    ,q.GAP_AMOUNT_9
    ,q.GAP_QUANTITY_9
    ,q.OTHER_AMOUNT_9
    ,q.OTHER_QUANTITY_9
    ,q.GAP_AMOUNT_10
    ,q.GAP_QUANTITY_10
    ,q.OTHER_AMOUNT_10
    ,q.OTHER_QUANTITY_10
    ,q.GAP_AMOUNT_11
    ,q.GAP_QUANTITY_11
    ,q.OTHER_AMOUNT_11
    ,q.OTHER_QUANTITY_11
    ,q.GAP_AMOUNT_12
    ,q.GAP_QUANTITY_12
    ,q.OTHER_AMOUNT_12
    ,q.OTHER_QUANTITY_12
    ,tp.kpp as contractor_kpp
    ,tp.name_short as contractor_name
    ,dtp.quarter||decl.fiscal_year||decl.inn_contractor as DECLARATION_QUARTER_ID
    ,decl.ZIP
    ,nvl(T.nba, 0) as DECLARED_TAX_AMNT
    ,nvl(T.NDS_CHAPTER9, 0) as CALCULATED_TAX_AMNT_R9
    ,nvl(T.NDS_CHAPTER12, 0) as CALCULATED_TAX_AMNT_R12
    ,nvl(T.NJSA, 0) as CHAPTER10_AMOUNT_NDS
    ,nvl(T.NJBA, 0) as CHAPTER11_AMOUNT_NDS
    ,nvl(T.DEDUCTION_NDS, 0) as DECLARED_TAX_AMNT_TOTAL
    ,nvl(T.CHAPTER8_AMOUNT, 0) as CHAPTER8_AMOUNT
    ,nvl(T.CHAPTER8_COUNT, 0) as CHAPTER8_COUNT
    ,nvl(T.CHAPTER9_AMOUNT, 0) as CHAPTER9_AMOUNT
    ,nvl(T.CHAPTER9_COUNT, 0) as CHAPTER9_COUNT
    ,case when t.decl_quarter_id is null then 1 else nvl(T.CHAPTER9_COUNT, 0) end as CHAPTER9_COUNT_EFFECTIVE -- все контракторы с расхождениями без СФ попадают в 9 раздел (Д.З.)
    ,nvl(T.CHAPTER10_AMOUNT, 0) as CHAPTER10_AMOUNT
    ,nvl(T.CHAPTER10_COUNT, 0) as CHAPTER10_COUNT
    ,nvl(T.CHAPTER11_AMOUNT, 0) as CHAPTER11_AMOUNT
    ,nvl(T.CHAPTER11_COUNT, 0) as CHAPTER11_COUNT
    ,nvl(T.CHAPTER12_AMOUNT, 0) as CHAPTER12_AMOUNT
    ,nvl(T.CHAPTER12_COUNT, 0) as CHAPTER12_COUNT
from
  (select
     KNP.INN
	,KNP.INN_DECLARANT
    ,KNP.INN_REORGANIZED
    ,KNP.KPP_EFFECTIVE
    ,KNP.PERIOD_CODE
    ,KNP.FISCAL_YEAR
    ,KNP.CONTRACTOR_INN as INN_CONTRACTOR
    ,KNP.CONTRACTOR_INN_DECLARANT as INN_CONTRACTOR_DECLARANT
    ,KNP.CONTRACTOR_KPP_EFFECTIVE as KPP_EFFECTIVE_CONTRACTOR
    ,0 as TYPE_CODE 
    ,KNP.IS_IMPORT
    ,SUM(case when knp.invoice_chapter=8  and KNP.DISCREPANCY_TYPE=1  Then NVL(KNP.DISCREPANCY_AMT,0)  END)  as GAP_AMOUNT_8
    ,SUM(case when knp.invoice_chapter=8  and KNP.DISCREPANCY_TYPE=1  Then 1 ELSE 0        END)  as GAP_QUANTITY_8
    ,SUM(case when knp.invoice_chapter=8  and KNP.DISCREPANCY_TYPE<>1 Then NVL(KNP.DISCREPANCY_AMT,0)  END)  as OTHER_AMOUNT_8
    ,SUM(case when knp.invoice_chapter=8  and KNP.DISCREPANCY_TYPE<>1 Then 1 ELSE 0        END)  as OTHER_QUANTITY_8
    ,SUM(case when knp.invoice_chapter=9  and KNP.DISCREPANCY_TYPE=1  Then NVL(KNP.DISCREPANCY_AMT,0)  END)  as GAP_AMOUNT_9
    ,SUM(case when knp.invoice_chapter=9  and KNP.DISCREPANCY_TYPE=1  Then 1 ELSE 0        END)  as GAP_QUANTITY_9
    ,SUM(case when knp.invoice_chapter=9  and KNP.DISCREPANCY_TYPE<>1 Then NVL(KNP.DISCREPANCY_AMT,0)  END)  as OTHER_AMOUNT_9
    ,SUM(case when knp.invoice_chapter=9  and KNP.DISCREPANCY_TYPE<>1 Then 1 ELSE 0        END)  as OTHER_QUANTITY_9
    ,SUM(case when knp.invoice_chapter=10 and KNP.DISCREPANCY_TYPE=1  Then NVL(KNP.DISCREPANCY_AMT,0)  END)  as GAP_AMOUNT_10
    ,SUM(case when knp.invoice_chapter=10 and KNP.DISCREPANCY_TYPE=1  Then 1 ELSE 0        END)  as GAP_QUANTITY_10
    ,SUM(case when knp.invoice_chapter=10 and KNP.DISCREPANCY_TYPE<>1 Then NVL(KNP.DISCREPANCY_AMT,0)  END)  as OTHER_AMOUNT_10
    ,SUM(case when knp.invoice_chapter=10 and KNP.DISCREPANCY_TYPE<>1 Then 1 ELSE 0        END)  as OTHER_QUANTITY_10
    ,SUM(case when knp.invoice_chapter=11 and KNP.DISCREPANCY_TYPE=1  Then NVL(KNP.DISCREPANCY_AMT,0)  END)  as GAP_AMOUNT_11
    ,SUM(case when knp.invoice_chapter=11 and KNP.DISCREPANCY_TYPE=1  Then 1 ELSE 0        END)  as GAP_QUANTITY_11
    ,SUM(case when knp.invoice_chapter=11 and KNP.DISCREPANCY_TYPE<>1 Then NVL(KNP.DISCREPANCY_AMT,0)  END)  as OTHER_AMOUNT_11
    ,SUM(case when knp.invoice_chapter=11 and KNP.DISCREPANCY_TYPE<>1 Then 1 ELSE 0        END)  as OTHER_QUANTITY_11
    ,SUM(case when knp.invoice_chapter=12 and KNP.DISCREPANCY_TYPE=1  Then NVL(KNP.DISCREPANCY_AMT,0)  END)  as GAP_AMOUNT_12
    ,SUM(case when knp.invoice_chapter=12 and KNP.DISCREPANCY_TYPE=1  Then 1 ELSE 0        END)  as GAP_QUANTITY_12
    ,SUM(case when knp.invoice_chapter=12 and KNP.DISCREPANCY_TYPE<>1 Then NVL(KNP.DISCREPANCY_AMT,0)  END)  as OTHER_AMOUNT_12
    ,SUM(case when knp.invoice_chapter=12 and KNP.DISCREPANCY_TYPE<>1 Then 1 ELSE 0        END)  as OTHER_QUANTITY_12
  from NDS2_MRR_USER.KNP_DISCREPANCY_DECLARATION KNP
  where KNP.DISCREPANCY_STATUS <> 2
  group by knp.inn, knp.inn_declarant, knp.inn_reorganized, knp.kpp_effective, knp.period_code, knp.fiscal_year,
           knp.contractor_inn, knp.contractor_inn_declarant, knp.contractor_kpp_effective, knp.is_import
) q
join NDS2_MRR_USER.DECLARATION_ACTIVE decl on decl.inn_contractor = q.inn and decl.kpp_effective = q.kpp_effective
         and decl.type_code = q.type_code and decl.period_code = q.period_code and decl.fiscal_year = q.fiscal_year
join NDS2_MRR_USER.DICT_TAX_PERIOD dtp on dtp.code = decl.period_code
left join NDS2_MRR_USER.HRZ_RELATIONS T on dtp.quarter||decl.fiscal_year||decl.inn_contractor = t.decl_quarter_id
                                           and q.KPP_EFFECTIVE = t.kpp_1_effective
                                           and q.INN_CONTRACTOR = t.inn_2
                                           and q.KPP_EFFECTIVE_CONTRACTOR = t.kpp_2_effective
left join NDS2_MRR_USER.TAX_PAYER tp on tp.inn = q.inn_contractor_declarant and tp.kpp_effective = q.KPP_EFFECTIVE_CONTRACTOR;
