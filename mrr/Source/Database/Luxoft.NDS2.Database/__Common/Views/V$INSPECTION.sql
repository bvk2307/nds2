﻿create or replace force view NDS2_MRR_USER.v$inspection as
select
  s_code,
  s_parent_code,
  s_code||' - '||s_name as s_name,
  nvl(type, case when s_code like '__00' or s_code like '99__' then 1 else 0 end) as s_type
from v$sono;