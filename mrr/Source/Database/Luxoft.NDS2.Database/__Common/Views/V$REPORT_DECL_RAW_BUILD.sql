﻿create or replace force view NDS2_MRR_USER.V$REPORT_DECL_RAW_BUILD (Region_Code, Soun_Code, Fiscal_Year, Tax_period, Decl_Count_Total, Decl_Count_Null, 
    Decl_Count_Compensation, Decl_Count_Payment, Decl_Count_Not_Submit, Decl_Count_Revised_Total, TaxBase_Sum_Compensation, TaxBase_Sum_Payment, 
    NdsCalculated_Sum_Compensation, NdsCalculated_Sum_Payment, NdsDeduction_Sum_Compensation, NdsDeduction_Sum_Payment, Nds_Sum_Compensation, Nds_Sum_Payment) as 
  with t as
    (
      select SD.ID as ID
            ,SD.ZIP as DECLARATION_VERSION_ID
            ,case when sd.PriznAktKorr = 1 then 1 else 0 end as IS_ACTIVE
            ,decode((row_number() over (partition by sd.PERIOD, sd.OTCHETGOD, sd.INNNP order by sd.NOMKORR)), 1, 1, null, 1, 0) as IS_FIRST
            ,substr(sd.KODNO,1,2) as REGION_CODE
            ,sd.KODNO as SOUN_CODE
            ,sd.PERIOD as TAX_PERIOD
            ,sd.OTCHETGOD as FISCAL_YEAR
            ,nvl(sd.DATADOK, sysdate) as DECL_DATE
            ,sd.NOMKORR as CORRECTION_NUMBER
            -- 1 = 'К уплате', 2 = 'К возмещению', 3 = 'Нулевая'
            ,sd.ND_Prizn as DECL_SIGN_CODE
            ,sd.SUMMANDS as COMPENSATION_AMNT
            ,nvl(sd.REALTOV18NALBAZA, 0) + nvl(sd.REALTOV10NALBAZA, 0) + nvl(sd.REALTOV118NALBAZA, 0) + nvl(sd.REALTOV110NALBAZA, 0) +
              nvl(sd.REALPREDIKNALBAZA, 0) + nvl(sd.VYPSMRSOBNALBAZA, 0) + nvl(sd.OPLPREDPOSTNALBAZA, 0) + nvl(sd.KORREALTOV18NALBAZA, 0) +
              nvl(sd.KORREALTOV10NALBAZA, 0) + nvl(sd.KORREALTOV118NALBAZA, 0) + nvl(sd.KORREALTOV110NALBAZA, 0) +
              nvl(sd.KORREALPREDIKNALBAZA, 0) as TAX_BASE_AMNT
            ,case when nvl(sd.SUMMAVYCH, 0) > 0 then sd.SUMMAVYCH else 0 end as TAX_DEDUCTION_AMNT
            ,case when nvl(sd.SUMMANALISCHISL, 0) > 0 then sd.SUMMANALISCHISL else 0 end as TAX_CALCULATED_AMNT
        from NDS2_MRR_USER.V$ASKDEKL sd
        join NDS2_MRR_USER.v$sono sono
          on sono.s_code = sd.KODNO
    )
  select m.Region_Code
        ,m.Soun_Code
        ,m.Fiscal_Year
        ,m.Tax_period
        ,m.Decl_Count_Total
        ,m.Decl_Count_Null
        ,m.Decl_Count_Compensation
        ,m.Decl_Count_Payment
        ,m.Decl_Count_Not_Submit
        ,cor.countTotal as Decl_Count_Revised_Total --Подано уточненных деклараций
        ,m.TaxBase_Sum_Compensation
        ,m.TaxBase_Sum_Payment
        ,m.NdsCalculated_Sum_Compensation
        ,m.NdsCalculated_Sum_Payment
        ,m.NdsDeduction_Sum_Compensation
        ,m.NdsDeduction_Sum_Payment
        ,m.Nds_Sum_Compensation
        ,m.Nds_Sum_Payment
  from
    (
      select vd.REGION_CODE as Region_Code
            ,vd.SOUN_CODE as Soun_Code
            ,vd.FISCAL_YEAR as Fiscal_Year
            ,vd.TAX_PERIOD as Tax_period
            --Подано деклараций к уплате
            ,count(case when decl_sign_code = 1 then vd.ID end) as Decl_Count_Payment
            --Подано деклараций к возмещению
            ,count(case when decl_sign_code = 2 then vd.ID end) as Decl_Count_Compensation
            --Подано нулевых деклараций
            ,count(case when decl_sign_code = 3 then vd.ID end) as Decl_Count_Null
            --Не подано деклараций НП по которым есть упоминания в декларациях контрагентов(Нулевая декларация)
            ,0 as Decl_Count_Not_Submit
            --Всего подано деклараций
            ,count(vd.ID) as Decl_Count_Total
            --Налоговая база в декларациях к уплате
            ,sum(case when decl_sign_code = 1 then nvl(TAX_BASE_AMNT, 0) else 0 end) as TaxBase_Sum_Payment
            --Налоговая база в декларациях к возмещению
            ,sum(case when decl_sign_code = 2 then nvl(TAX_BASE_AMNT, 0) else 0 end) as TaxBase_Sum_Compensation
            --Сумма исчисленного НДС в декларациях к уплате
            ,sum(case when decl_sign_code = 1 then nvl(TAX_CALCULATED_AMNT, 0) else 0 end) as NdsCalculated_Sum_Payment
            --Сумма исчисленного НДС в декларациях к возмещению
            ,sum(case when decl_sign_code = 2 then nvl(TAX_CALCULATED_AMNT, 0) else 0 end) as NdsCalculated_Sum_Compensation
            --Сумма вычетов по НДС в декларациях к уплате (нулевые тоже поместим)
            ,sum(case when decl_sign_code = 1 or decl_sign_code = 3 then nvl(TAX_DEDUCTION_AMNT, 0) else 0 end) as NdsDeduction_Sum_Payment
            --Сумма вычетов по НДС в декларациях к возмещению
            ,sum(case when decl_sign_code = 2 then nvl(TAX_DEDUCTION_AMNT, 0) else 0 end) as NdsDeduction_Sum_Compensation
            --Сумма НДС к уплате
            ,sum(case when decl_sign_code = 1 then nvl(COMPENSATION_AMNT, 0) else 0 end) as Nds_Sum_Payment
            --Сумма НДС к возмещению
            ,sum(case when decl_sign_code = 2 then nvl(COMPENSATION_AMNT, 0) else 0 end) as Nds_Sum_Compensation
        from t vd 
        where vd.IS_ACTIVE = 1 
        group by vd.REGION_CODE, vd.SOUN_CODE, vd.FISCAL_YEAR, vd.TAX_PERIOD 
    ) m
  left outer join
    ( --Подано уточненных деклараций
      select count(*) as countTotal
            ,vd.REGION_CODE as Region_Code
            ,vd.SOUN_CODE as Soun_Code
            ,vd.FISCAL_YEAR as Fiscal_Year
            ,vd.TAX_PERIOD as Tax_period
        from t vd
        where vd.IS_FIRST = 0
        group by vd.REGION_CODE, vd.SOUN_CODE, vd.FISCAL_YEAR, vd.TAX_PERIOD
    ) cor 
    on cor.REGION_CODE = m.REGION_CODE 
    and cor.SOUN_CODE = m.SOUN_CODE
    and cor.FISCAL_YEAR = m.FISCAL_YEAR 
    and cor.TAX_PERIOD = m.TAX_PERIOD;