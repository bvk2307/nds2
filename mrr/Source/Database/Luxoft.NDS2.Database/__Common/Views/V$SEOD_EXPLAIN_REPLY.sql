﻿CREATE OR replace force view NDS2_MRR_USER.V$SEOD_EXPLAIN_REPLY AS
select
	 er.explain_id
	,er.doc_id
	,er.type_id
	,er.incoming_num
	,er.incoming_date
	,er.executor_receive_date
	,er.send_date_to_iniciator
	,er.receive_by_tks
	,(case when er.receive_by_tks = 0 and er.status_id = 2 and vp.ObrabotanMS is not null and vp.ObrabotanMS = 2 then 3
			when er.receive_by_tks = 1 and er.status_id in (1,2) and vp.ObrabotanMS is not null and vp.ObrabotanMS = 2 then 3
			when er.receive_by_tks = 1 and er.status_id = 1 then 2
			else er.status_id end) as status_id  
	,er.status_set_date
	,er.user_comment
	,er.filenameoutput
	,er.filesizeoutput
	,er.file_id
	,nvl(er.zip, vp.ZIP) as zip
from nds2_seod.seod_explain_reply er
join DOC dc on dc.doc_id = er.doc_id
join declaration_history d on d.reg_number = dc.ref_entity_id and d.sono_code_submited = dc.sono_code and d.type_code = 0
left join v$askpoyasnenie vp on (er.receive_by_tks = 0 and vp.ImyaFaylTreb = er.filenameoutput and vp.KodNO = dc.sono_code) or
     (er.receive_by_tks = 1 and vp.ImyaFaylTreb = er.filenameoutput and vp.KodNO = dc.sono_code)
where (er.type_id = 1 and ((er.receive_by_tks = 0) or (er.receive_by_tks = 1 and vp.ObrabotanMS = 2)))
	  or er.type_id <> 1;

