﻿create or replace force view NDS2_MRR_USER.V$INVOICE_EXPLAIN
as
select  /*+ leading(cl ap ex) use_nl(cl) use_nl(ap) use_nl(ex) index(cl) index(ap) index(ex)*/ 
		cl.invoice_row_key as ROW_KEY
       ,cl.request_id
       ,seod.type_id as doc_type
       ,seod.incoming_num as INCOMING_NUM
       ,seod.incoming_date as INCOMING_DATE
       ,row_number() over (partition by cl.invoice_row_key
                    order by seod.incoming_date desc)
       as rank
from SOV_INVOICE_CLARIFICATION cl
join V$ASKPOYASNENIE ap on cl.clarification_row_key is not null 
     and ap.zip = substr(cl.clarification_row_key, 1, 20)
join SEOD_EXPLAIN_REPLY ex on (ex.zip is not null and ex.zip = ap.zip) or
(ex.zip is null and ex.filenameoutput = ap.ImyaFaylTreb)
join nds2_seod.seod_explain_reply seod on seod.explain_id = ex.explain_id;
