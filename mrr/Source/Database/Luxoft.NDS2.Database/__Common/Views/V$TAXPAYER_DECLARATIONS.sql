﻿CREATE OR REPLACE FORCE VIEW NDS2_MRR_USER.V$TAXPAYER_DECLARATIONS AS
SELECT
     decl.zip as Zip
    ,decl.inn_declarant as InnDeclarant
    ,decl.kpp_contractor as KppDeclarant
    ,decl.kpp_effective as KppEffectiveDeclarant
    ,decl.sur_code as SurCode
    ,decl.reg_number as RegNumber
    ,period.description||' '||decl.fiscal_year as TaxPeriod
    ,decl.type_code as TypeCode
    ,decode(decl.type_code, 0, 'Декларация', 1, 'Журнал', '') as TypeDescription
   ,case when knp_status.zip is null then ''
    when knp_status.zip is not null and knp_status.calc_is_open is null then 'Открыта'
		when knp_status.calc_is_open = 0 then 'Закрыта'
		else 'Открыта'
   end as KnpStatus 	 
	,decl.correction_number_effective as CorrectionNumber
    ,decode(decl.sign, 1, 'К уплате', 2, 'К возмещению', 3, 'Нулевая', '') as Sign
    ,decl.nds_total as NdsTotal   
    ,decode(decl_sum.version_id, null, null,
          nvl(decl_sum.discrep_currency_count,0)
        + nvl(decl_sum.discrep_gap_count,0)
        + nvl(decl_sum.discrep_weak_count,0)
        + nvl(decl_sum.discrep_nds_count,0)) as DiscrepancyQuantity
    ,knp_sum.total_discrepancy_count as KnpDiscrepancyQuantity
    ,decl.is_large as IsLarge
    ,case when cr.qnty is null and decl.is_active = 1 and decl.type_code = 0 then 0 else cr.qnty end as ControlRatioQuantity
    ,decl.inn_reorganized as InnReorganized
    ,decl.kpp_reorganized as KppReorganized
    ,period.sort_order as PeriodWeight
    ,decl.period_code as PeriodCode
    ,decl.fiscal_year as FiscalYear
    ,decl.correction_number as CorrectionNumberShort
    ,to_number(decl.is_active) as IsActive
    ,decl_anl.CANCELLED_SINCE as AnnulmentDate
    ,decl_anl.REASON_ID as AnnulmentReasonId
FROM 
              DECLARATION_HISTORY decl
    JOIN      DICT_TAX_PERIOD period ON period.code = decl.period_code
	left join nds2_mrr_user.declaration_knp_status knp_status on knp_status.zip = decl.zip and decl.type_code = 0
    LEFT JOIN DECLARATION_JOURNAL_SUMMARY decl_sum ON decl_sum.version_id =  decl.zip
    LEFT JOIN KNP_DECLARATION_SUMMARY knp_sum 
         ON     knp_sum.inn = decl.inn_contractor 
            AND knp_sum.kpp_effective = decl.kpp_effective
            AND knp_sum.period_code = decl.period_code
            AND knp_sum.fiscal_year = decl.fiscal_year
			and decl.is_active = 1
	LEFT JOIN (
    select vks.IdDekl, count(1) as qnty
    from V$ASKKONTRSOONTOSH vks
    where (vks.vypoln = 0) and vks.DeleteDate is null
    group by vks.IdDekl
    ) cr on cr.IdDekl = decl.ask_id and decl.is_active = 1
    left join V$DECLARATION_ANNULMENT decl_anl on 
      decl.INN_DECLARANT = decl_anl.INN and
      decl.KPP = decl_anl.KPP and
      decl.SONO_CODE_SUBMITED = decl_anl.SONO_CODE and
      decl.FISCAL_YEAR = decl_anl.FISCAL_YEAR and
      decl.PERIOD_CODE = decl_anl.PERIOD_CODE;
