﻿create or replace force view nds2_mrr_user.v$explain as
select
   er.explain_id
  ,er.doc_id
  ,er.type_id
  ,er.incoming_num
  ,er.incoming_date
  ,er.executor_receive_date
  ,er.send_date_to_iniciator
  ,er.receive_by_tks
  ,er.status_id
  ,er.status_set_date
  ,er.user_comment
  ,er.filenameoutput
  ,er.filesizeoutput
  ,er.file_id
  ,er.zip
  ,(case when er.type_id = 1 and er.receive_by_tks = 1 then 'Формализованное пояснение'
        else nvl(rt.short_name, '') end) as type_name
  ,rs.name as status_name
  ,er.declaration_version_id
  ,er.doc_num
  ,er.doc_date
  ,er.ExplainOtherReason
from
(
    select
       er.explain_id
      ,er.doc_id
      ,er.type_id
      ,er.incoming_num
      ,er.incoming_date
      ,er.executor_receive_date
      ,er.send_date_to_iniciator
      ,er.receive_by_tks
      ,(case when er.receive_by_tks = 0 and er.status_id = 2 and vp.ObrabotanMS is not null and vp.ObrabotanMS = 2 then 3
          when er.receive_by_tks = 1 and nvl(er_local.status_id, er.status_id) in (1,2) and vp.ObrabotanMS is not null and vp.ObrabotanMS = 2 then 3
          when er.receive_by_tks = 1 and nvl(er_local.status_id, er.status_id) = 1 then 2
          else nvl(er_local.status_id, er.status_id) end) as status_id
      ,nvl(er_local.status_set_date, er.status_set_date) as status_set_date
      ,sec.user_comment
      ,nvl(er_local.filenameoutput, er.filenameoutput) as filenameoutput
      ,nvl(er_local.filesizeoutput, er.filesizeoutput) as filesizeoutput
      ,nvl(er_local.file_id, er.file_id) as file_id
      ,nvl(vp.zip, nvl(er_local.zip, er.zip)) as zip
      ,d.zip as declaration_version_id
      ,decode(dc.Seod_Accepted, 1, TO_CHAR(dc.EXTERNAL_DOC_NUM), '*'||TO_CHAR(dc.DOC_ID)) as doc_num
      ,decode(dc.Seod_Accepted, 1, dc.SEOD_ACCEPT_DATE, NULL) as doc_date
      ,vp.PoyasnInOsn as ExplainOtherReason
    from nds2_seod.seod_explain_reply er
	left join nds2_mrr_user.seod_explain_reply er_local on er_local.explain_id = er.explain_id
    join DOC dc on dc.doc_id = er.doc_id
    join declaration_history d on d.reg_number = dc.ref_entity_id and d.sono_code_submited = dc.sono_code and d.type_code = 0
	left join seod_explain_comment sec on sec.explain_id = er.explain_id and sec.explain_type_id = 1
    left join v$askpoyasnenie vp on (vp.ImyaFaylTreb = er.filenameoutput and vp.KodNO = dc.sono_code and nvl(er_local.status_id, er.status_id) = vp.OBRABOTANMS) or (vp.zip = nvl(er_local.zip, er.zip))
    where (er.type_id = 1 and (er.receive_by_tks = 0 or vp.ZIP is not null))
          or er.type_id <> 1
) er
join DICT_EXPLAIN_REPLY_TYPE rt on rt.id = er.type_id
join DICT_EXPLAIN_REPLY_STATUS rs on rs.id = er.status_id;
