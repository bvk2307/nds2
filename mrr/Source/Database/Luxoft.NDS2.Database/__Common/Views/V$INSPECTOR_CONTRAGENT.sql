﻿CREATE OR REPLACE FORCE VIEW NDS2_MRR_USER.V$INSPECTOR_CONTRAGENT
AS select
  null as MARK                                          -- признак
  ,KRS.ZIP as DECLARATION_VERSION_ID                    -- id декларации (для поиска для конкретной декл.)
  ,KRS.INN_DECLARANT as INN_DECLARANT                   -- ИНН подавшего декларацию
  ,KRS.INN_REORGANIZED                                  -- ИНН реорганизованного юр. лица
  ,KRS.KPP_EFFECTIVE                                    -- КПП
  ,KRS.FISCAL_YEAR                                      -- год налогового периода
  ,KRS.PERIOD_CODE                                      -- код налогового периода
  ,KRS.type_code as DOC_TYPE_CODE                       -- тип декларции (код)
  ,decode(KRS.type_code, 0, 'Декларация', 'Журнал') as DOC_TYPE                       -- тип декларации
  ,sur.sign_code as SUR_CODE                            -- Контрагент: СУР
  ,KRS.INN_CONTRACTOR_DECLARANT as CONTRACTOR_INN       -- Контрагент: ИНН
  ,KRS.CONTRACTOR_KPP as CONTRACTOR_KPP                 -- Контрагент: КПП
  ,KRS.CONTRACTOR_NAME as CONTRACTOR_NAME               -- Контрагент: Наименование
  ,KRS.IS_IMPORT                                        -- Признак "Импортная сделка"
  ,KRS.DECLARED_TAX_AMNT                                -- Заявленная к вычету НДС
  ,KRS.CALCULATED_TAX_AMNT_R9                           -- Исчисленная к вычету НДС (раздел 9)
  ,KRS.CALCULATED_TAX_AMNT_R12                          -- Исчисленная к вычету НДС (раздел 12)
  ,KRS.CHAPTER10_AMOUNT_NDS                             -- Сумма НДС: по выставленным СФ
  ,KRS.CHAPTER11_AMOUNT_NDS                             -- Сумма НДС: по полученным СФ
  ,KRS.DECLARED_TAX_AMNT_TOTAL                          -- Заявленная к вычету НДС по всему разделу
  -- Раздел 8
  ,(nvl(KRS.GAP_AMOUNT_8, 0)
     + nvl(KRS.OTHER_AMOUNT_8, 0)) as CHAPTER8_AMOUNT   -- Стоимость покупок, включая НДС
  ,(KRS.GAP_QUANTITY_8
     + KRS.OTHER_QUANTITY_8) as CHAPTER8_COUNT  -- Количество покупок
  ,KRS.GAP_AMOUNT_8                                     -- Количество расхождений: РАЗРЫВ
  ,KRS.GAP_QUANTITY_8                                   -- Сумма расхождений: РАЗРЫВ
  ,KRS.OTHER_AMOUNT_8                                   -- количество расхождений: ДРУГИЕ
  ,KRS.OTHER_QUANTITY_8                                 -- Количество расхождений: ДРУГИЕ
  -- Раздел 9
  ,(nvl(KRS.GAP_AMOUNT_9, 0)
     + nvl(KRS.OTHER_AMOUNT_9, 0)) as CHAPTER9_AMOUNT   -- Стоимость продаж, включая НДС
  ,(KRS.GAP_QUANTITY_9
     + KRS.OTHER_QUANTITY_9) as CHAPTER9_COUNT          -- Количество продаж
  ,KRS.CHAPTER9_COUNT_EFFECTIVE              -- Количество продаж - только для фильтрации
  ,sda.GAP_AMT_9 as GAP_AMOUNT_9                        -- Сумма расхождений: РАЗРЫВ
  ,sda.GAP_QTY_9 as GAP_QUANTITY_9                      -- Количество расхождений: РАЗРЫВ
  ,KRS.OTHER_AMOUNT_9                                   -- Сумма расхождений: ДРУГИЕ
  ,KRS.OTHER_QUANTITY_9                                 -- Количество расхождений: ДРУГИЕ
  -- Раздел 10
  ,(nvl(KRS.GAP_AMOUNT_10, 0)
     + nvl(KRS.OTHER_AMOUNT_10, 0)) as CHAPTER10_AMOUNT  -- Стоимость по выставленным СФ
  ,(KRS.GAP_QUANTITY_10
     + KRS.OTHER_QUANTITY_10) as CHAPTER10_COUNT        -- Количество выставленных СФ
  ,KRS.GAP_AMOUNT_10                                    -- Сумма расхождений: РАЗРЫВ
  ,KRS.GAP_QUANTITY_10                                  -- Количество расхождений: РАЗРЫВ
  ,KRS.OTHER_AMOUNT_10                                  -- Сумма расхождений: ДРУГИЕ
  ,KRS.OTHER_QUANTITY_10                                -- Количество расхождений: ДРУГИЕ
  -- Раздел 11
  ,(nvl(KRS.GAP_AMOUNT_11, 0)
     + nvl(KRS.OTHER_AMOUNT_11, 0)) as CHAPTER11_AMOUNT -- Стоимость по полученным СФ
  ,(KRS.GAP_QUANTITY_11
     + KRS.OTHER_QUANTITY_11) as CHAPTER11_COUNT        -- Количество полученных СФ
  ,KRS.GAP_AMOUNT_11                                    -- Сумма расхождений: РАЗРЫВ
  ,KRS.GAP_QUANTITY_11                                  -- Количество расхождений: РАЗРЫВ
  ,KRS.OTHER_AMOUNT_11                                  -- Сумма расхождений: ДРУГИЕ
  ,KRS.OTHER_QUANTITY_11                                -- Количество расхождений: ДРУГИЕ
  -- Раздел 12
  ,(nvl(KRS.GAP_AMOUNT_12, 0)
     + nvl(KRS.OTHER_AMOUNT_12, 0)) as CHAPTER12_AMOUNT -- Стоимость СФ (п.5. ст.173 НК РФ)
  ,(KRS.GAP_QUANTITY_12
     + KRS.OTHER_QUANTITY_12) as CHAPTER12_COUNT        -- Количество СФ (п.5. ст.173 НК РФ)
  ,KRS.GAP_AMOUNT_12                                    -- Сумма расхождений: РАЗРЫВ
  ,KRS.GAP_QUANTITY_12                                  -- Количество расхождений: РАЗРЫВ
  ,KRS.OTHER_AMOUNT_12                                  -- Сумма расхождений: ДРУГИЕ
  ,KRS.OTHER_QUANTITY_12                                -- Количество расхождений: ДРУГИЕ
  ,null as AMOUNT_TOTAL                                 -- удаленное поле (для поддержки сохраненной сортировки)
  ,null as AMOUNT_NDS                                   -- удаленное поле (для поддержки сохраненной сортировки)
  ,null as NDS_WEIGHT                                   -- удаленное поле (для поддержки сохраненной сортировки)
from NDS2_MRR_USER.KNP_RELATION_SUMMARY krs
  left join NDS2_MRR_USER.SOV_DISCREPANCY_AGGREGATE sda on KRS.ZIP = sda.seller_zip and krs.INN = sda.seller_inn
  left  join NDS2_MRR_USER.EXT_SUR sur on sur.inn = krs.INN_CONTRACTOR
                                       and sur.kpp_effective = krs.kpp_effective_contractor
                                       and sur.fiscal_year = krs.fiscal_year
                                       and sur.fiscal_period = krs.period_code;
