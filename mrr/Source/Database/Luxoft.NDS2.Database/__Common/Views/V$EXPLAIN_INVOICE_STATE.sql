﻿create or replace force view NDS2_MRR_USER.V$EXPLAIN_INVOICE_STATE
as
select 
   st.explain_id
  ,st.invoice_original_id
  ,st.invoice_state
  ,ex.incoming_date
  ,ex.RECEIVE_BY_TKS
from STAGE_INVOICE_CORRECTED_STATE st 
join nds2_seod.SEOD_EXPLAIN_REPLY ex on ex.explain_id = st.explain_id;
