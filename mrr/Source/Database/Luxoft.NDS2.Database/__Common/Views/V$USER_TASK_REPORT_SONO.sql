﻿create or replace force view NDS2_MRR_USER.V$USER_TASK_REPORT_SONO as
select
   t.report_id as ReportId,
   t.sono_code as SonoCode,
   t.region_code as RegionCode,
   t.task_type_id as TaskType,
   t.completed_onschedule as CompletedOnSchedule,
   t.completed_overdue as CompletedOverdue,
   t.inprogress_onschedule as InprogressOnSchedule, 
   t.inprogress_overdue as InprogressOverdue,
   t.unassigned as Unassigned
 from USER_TASK_REPORT_SONO t;
