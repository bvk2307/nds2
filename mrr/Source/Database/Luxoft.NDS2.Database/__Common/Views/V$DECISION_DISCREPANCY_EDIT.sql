﻿create or replace force view NDS2_MRR_USER.V$DECISION_DISCREPANCY_EDIT as
select
   1 as Included
  ,NULL as LockedByUserId
  ,t.act_amount as AmountByAct
  ,case when t.status = 2 then 0 else t.amount end as AmountByDecision
  ,t.ID as Id
  ,t.type_name as Type
  ,t.initial_amt as Amount
  ,t.status as Status
  ,t.invoice_number as InvoiceNumber
  ,t.invoice_date as InvoiceDate
  ,t.invoice_chapter as InvoiceChapter
  ,t.invoice_amt as InvoiceAmount
  ,t.invoice_nds_amt as InvoiceAmountNds
  ,t.contractor_inn as ContractorInn
  ,t.contractor_kpp as ContractorKpp
  ,t.contractor_inn_reorg as ContractorInnReorganized
  ,t.decision_id as DocumentId
from DECISION_DISCREPANCY t;