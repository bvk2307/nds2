﻿CREATE OR REPLACE FORCE VIEW NDS2_MRR_USER.V$INSPECTOR_CONTRAGENT_PARAMS AS
select
  null                            as MARK
  ,dis.status                     as DIS_STATUS
  ,KD.ZIP                         as DECLARATION_VERSION_ID
  ,'Декларация'                   as DOC_TYPE  
  ,tp.SUR_CODE                    as SUR_CODE
  ,NULL                           as MARK_CODE
  ,KD.INVOICE_ROW_KEY             as ROW_KEY
  ,KD.INVOICE_CHAPTER             as CHAPTER
  ,KD.INN
  ,KD.INN_DECLARANT
  ,KD.INN_REORGANIZED
  ,KD.KPP_EFFECTIVE
  ,KD.FISCAL_YEAR
  ,KD.PERIOD_CODE
  ,0 as TYPE_CODE
  ,KD.CONTRACTOR_INN
  ,KD.CONTRACTOR_KPP_EFFECTIVE
  ,KD.KPP                         as CONTRACTOR_KPP
  ,tp.name_short                  as CONTRACTOR_NAME
  ,I.invoice_num                  as INVOICE_NUM
  ,I.invoice_date                 as INVOICE_DATE
  ,I.price_buy_amount             as AMOUNT_TOTAL
  ,I.price_buy_nds_amount         as AMOUNT_NDS
  ,DIS.type                       as DIS_TYPE
  ,DIS_TYPE.s_name                as DIS_TYPE_NAME
  ,DIS.id                         as DIS_ID
  ,DIS.amnt                       as DIS_AMOUNT
  ,DIS_STATUS.name                as DIS_STATUS_NAME
from        NDS2_MRR_USER.KNP_DISCREPANCY_DECLARATION KD
  join      NDS2_MRR_USER.SOV_INVOICE_ALL I              on KD.INVOICE_ROW_KEY = I.ROW_KEY and KD.DISCREPANCY_ID = I.DISCREPANCY_ID
  join      NDS2_MRR_USER.SOV_DISCREPANCY DIS            on KD.DISCREPANCY_ID = DIS.ID
  left join NDS2_MRR_USER.TAX_PAYER tp                   on tp.inn = kd.inn_declarant and tp.kpp_effective = kd.kpp_effective
  join      NDS2_MRR_USER.dict_discrepancy_type DIS_TYPE      on dis.type = dis_type.s_code
  join      NDS2_MRR_USER.dict_discrepancy_status DIS_STATUS  on dis.status = dis_status.id;
