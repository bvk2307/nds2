﻿create or replace force view NDS2_MRR_USER.V$DECLARATION_CURRENT_ASSIGNEE as
select
    t.declaration_type_code
    ,t.inn_declarant
    ,t.kpp_effective
    ,t.fiscal_year
    ,t.period_effective
    ,u.NAME
    ,u.SID
from declaration_assignment t
join mrr_user u on u.id = t.assigned_to
where t.is_actual = 1;
