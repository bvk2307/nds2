﻿create or replace force view NDS2_MRR_USER.v$control_ratio as
select /*+ leading(cr ask crc crt) use_nl(cr) use(ask) use_nl(crc) use_nl(crt)  index(cr) index(ask) index(crc) index(crt)  */
  cr.Id                as Id,
  cr.IdDekl            as ask_decl_Id,
  cr.DataRascheta      as CalculationDate,
  ask.zip              as Decl_Version_Id,
  case when cr.Vypoln = 0 then 1 else 0 end as HasDiscrepancy,
  case when cr.Vypoln = 0 then 'Да' else null end as HasDiscrepancyString,
  cr.LevyaChast        as DisparityLeft,
  cr.PravyaChast       as DisparityRight,
  crc.User_Comment     as UserComment,
  crt.Code             as TypeCode,
  crt.Formulation      as TypeFormulation,
  crt.Description      as TypeDescription,
  crt.Calculation_Condition   as TypeCalculationCondition,
  crt.Calculation_Operator    as TypeCalculationOperator,
  crt.Calculation_Left_Side   as TypeCalculationLeftSide,
  crt.Calculation_Right_Side  as TypeCalculationRightSide
from V$ASKKontrSoontosh cr
join ask_declandjrnl ask on ask.id = cr.IdDekl and ask.type = 0
join control_ratio_type crt on crt.code = cr.KodKs and crt.versform = ask.versform
left join control_ratio_comment crc on crc.id = cr.id
where cr.DeleteDate is null;
