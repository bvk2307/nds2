﻿create or replace force view NDS2_MRR_USER.v$taxpayer_assignment as
select
  tpia.s_code 		as SONO_CODE,
  tpwl.inn 		as INN,
  tpwl.kpp 		as KPP,
  tpwl.name 		as TAX_PAYER_NAME,
  idai.SID 		as SID,
  idai.employee_num 	as EMPLOYEE_NUM,
  idai.NAME 		as INSPECTOR_NAME,
  tpia.Is_Active 	as IS_ACTIVE,
  tpia.Update_Date 	as UPDATE_DATE,
  nvl(isa.Is_Active,0) as IS_INSPECTOR_ACTIVE
from NDS2_MRR_USER.IDA_TAX_PAYER_INDIVIDUAL_ASSG tpia
inner join NDS2_MRR_USER.IDA_TAX_PAYER_WHITE_LIST tpwl on tpwl.inn = tpia.inn
left join NDS2_MRR_USER.IDA_INSPECTOR_SONO_ASSG isa on isa.sid = tpia.sid and isa.s_code = tpia.s_code
left join NDS2_MRR_USER.IDA_INSPECTOR idai on idai.SID = tpia.SID;
