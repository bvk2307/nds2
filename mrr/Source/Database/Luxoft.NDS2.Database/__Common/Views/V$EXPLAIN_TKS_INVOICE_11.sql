﻿create or replace force view NDS2_MRR_USER.V$EXPLAIN_TKS_INVOICE_11 as
select 
   explain_zip as ExplainZip
  ,row_key as RowKey
  ,status 
  ,chapter
  ,ordinal_number as OrdinalNumber
  ,operation_codes_bit as OperationCodesBit
  ,operation_codes_bit_order as OperationCodesBitOrder
  ,operation_codes_bit_new as NewOperationCodesBit
  ,invoice_num as InvoiceNumber
  ,invoice_num_new as NewInvoiceNumber
  ,invoice_date as InvoiceDate
  ,invoice_date_new as NewInvoiceDate
  ,change_num as ChangeNumber
  ,change_num_new as NewChangeNumber
  ,change_date as ChangeDate
  ,change_date_new as NewChangeDate
  ,correction_num as CorrectionNumber
  ,correction_num_new as NewCorrectionNumber
  ,correction_date as CorrectionDate
  ,correction_date_new as NewCorrectionDate
  ,change_correction_num as ChangeCorrectionNumber
  ,change_correction_num_new as NewChangeCorrectionNumber
  ,change_correction_date as ChangeCorrectionDate
  ,change_correction_date_new as NewChangeCorrectionDate
  ,okv_code as OkvCode
  ,okv_code_new as NewOkvCode
  ,contractor_inn_first as SellerInn
  ,contractor_inn_new as NewSellerInn
  ,contractor_kpp_first as SellerKpp
  ,contractor_kpp_new as NewSellerKpp
  ,broker_inn as BrokerInn
  ,broker_kpp as BrokerKpp
  ,broker_inn_new as NewBrokerInn
  ,broker_kpp_new as NewBrokerKpp
  ,deal_code as DealCode
  ,deal_code_new as NewDealCode
  ,amt as Amount
  ,amt_new as NewAmount
from EXPLAIN_TKS_INVOICE
where chapter = 11;
  
