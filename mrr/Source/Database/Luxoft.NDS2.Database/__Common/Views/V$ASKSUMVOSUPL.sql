﻿create or replace force view NDS2_MRR_USER.V$ASKSUMVOSUPL
as
select
"Ид" as ID
,"ИдДекл" as IDDEKL
,"НаимНедв" as NAIMNEDV
,"КодОпНедв" as KODOPNEDV
,"ДатаВводОН" as DATAVVODON
,"ДатаНачАмОтч" as DATANACHAMOTCH
,"СтВводОН" as STVVODON
,"НалВычОН" as NALVYCHON
,"Индекс" as INDEKS
,"КодРегион" as KODREGION
,"Район" as RAJON
,"Город" as GOROD
,"НаселПункт" as NASELPUNKT
,"Улица" as ULICA
,"Дом" as DOM
,"Корпус" as KORPUS
,"Кварт" as KVART
,"НаимООС" as NaimOOS
,"КодОпООС" as KodOpOOS
,"ДатаВводООС" as DataVvodOOC
,"СтВводООС" as StVvodOOS
,"НалВычООС" as NalVychOOS
from NDS2_MC."ASKСумВосУпл";
