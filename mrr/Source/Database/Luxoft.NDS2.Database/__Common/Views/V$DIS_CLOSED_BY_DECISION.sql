﻿create or replace force view NDS2_MRR_USER.V$DIS_CLOSED_BY_DECISION as
select dis.id
from 
  DECISION t
  join DECISION_DISCREPANCY dis on dis.decision_id = t.id
where t.close_date is not null
union all
select discrepancy_id from KNP_DISCREPANCY_CLOSED;
