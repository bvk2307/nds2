﻿create or replace force view NDS2_MRR_USER.V$BANK_ACCOUNT_REQUEST as
select /*+ index(bar IDX_BANK_ACCOUNT_REQ_KEY)*/
  decode(bar.reorganized, 1, nvl(tp.inn_successor_top, tp.inn), tp.inn) as Inn
  ,decode(bar.reorganized, 1, nvl(tp.kpp_effective_successor_top, tp.kpp_effective), tp.kpp_effective) as KppEffective
  ,bar.inn as InnContractor
  ,tp.kpp as KppContractor
  ,bar.kpp_effective as KppEffectiveContractor
  ,bar.request_date as CreateDate
  ,bar.innbank as BankInn
  ,bar.kppbank as BankKpp
  ,bar.bikbank as Bik
  ,bar.banknumber as BankNumber
  ,bar.filialnumber as BankFilial
  ,bar.namebank as BankName
  ,bar.accounts_list as AccountsList
  ,dtp.description||' '||bar.fiscal_year as FullTaxPeriod
  ,bar.reg_number as RequestNumber
  ,bar.seod_date as RequestDate
  ,ds.ui_name as RequestStatus
  ,case when bar.request_actor = 0 then 'Автозапрос' else bar.user_name end as Requester
  ,bar.period_begin as RequestBeginDate
  ,bar.period_end as RequestEndDate
  ,CAST(bar.fiscal_year as number) as FiscalYear
  ,dtp.sort_order as SortOrderPeriod
from BANK_ACCOUNT_REQUEST bar
join tax_payer tp on tp.inn = bar.inn and tp.kpp_effective = bar.kpp_effective
join dict_tax_period dtp on dtp.code = bar.period_code
left join dict_bank_account_status ds on ds.status_code = bar.status_code;
