﻿create or replace force view NDS2_MRR_USER.V$DECLARATION_ANNULMENT as
select 
    da.ID
  , sa.INN
  , sa.KPP
  , sa.SONO_CODE
  , sa.FISCAL_YEAR
  , sa.PERIOD_CODE
  , sa.CORRECTION_NUMBER
  , sa.ID_FILE
  , sa.REG_NUMBER
  , sa.CANCELLED_SINCE
  , sa.RECEIVED_AT
  , da.ACTIVE_BEFORE
  , ah.STATUS_ID
  , sa.REASON_ID
  , ar.NAME as REASON_DESCRIPTION
from 
nds2_mrr_user.declaration_annulment da
join nds2_seod.seod_declaration_annulment sa on da.SEOD_ID = sa.ID
join nds2_mrr_user.declaration_annulment_history ah on ah.annulment_id = da.id and ah.is_last = 1
join nds2_seod.SEOD_DECL_ANNULMENT_REASON ar on ar.ID = sa.reason_id;

grant select on NDS2_MRR_USER.V$DECLARATION_ANNULMENT to NDS2_MC;