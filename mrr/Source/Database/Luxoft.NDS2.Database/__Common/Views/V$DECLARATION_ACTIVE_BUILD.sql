﻿create or replace force view NDS2_MRR_USER.V$DECLARATION_ACTIVE_BUILD as
select
  rownum as join_k
  ,ask.zip
  ,ask.id as ask_id
  ,seod.decl_reg_num as reg_number
  ,case when ask.innreorg is null then ask.innnp else ask.innreorg end as inn_contractor
  ,ask.innnp as inn_declarant
  ,ask.innreorg as inn_reorganized
  ,ask.kppnp as kpp
  ,ask.kpp_effective as kpp_effective
  ,ask.otchetgod as fiscal_year
  ,ask.period as period_code
  ,dtp.effective_value as period_effective
  ,dtp.description || ' ' || ask.otchetgod as full_tax_period
  ,NVL(dtp.sort_order, 0) as TAX_PERIOD_SORT_ORDER
  ,to_number(nvl(ask.otchetgod, '0')||dtp.quarter) as partition_id
  ,nvl(rg.group_id, 0) as subpartition_id
  ,ask.type as type_code
  ,decode(ask.type, 1, 'Журнал', 'Декларация') as type
  ,dsur.sign_code as sur_code
  ,sur.description as sur_description
  ,tp.name_short as name_short
  ,ask.nomkorr as correction_number
  ,case when summary.id is not null then 1 else 0 end as correction_processed
  ,ask.nd_prizn as sign_code
  ,decode(ask.nd_prizn, 1, 'К уплате', 2, 'К возмещению', 'Нулевая') as sign
  ,case when ask.innreorg is null then to_char(ask.nomkorr) else to_char(ask.nomkorr)||' ('||ask.innreorg||')' end as correction_number_effective
  ,to_number(substr(nvl(tp.sono_code_successor_top, tp.sono_code), 1, 2)) as region_code
  ,nvl(tp.sono_code_successor_top, tp.sono_code) as sono_code
  ,dreg.s_name as region_title
  ,dsono.s_name as inspection_title
  ,ins_ass.name as inspector
  ,ins_ass.SID as inspector_sid
  ,seod.eod_date as submit_date
  ,seod.submission_date as submission_date
  ,ask.familiyapodp||' '||ask.imyapodp||' '||ask.otchestvopodp as signatory_name
  ,DECODE(ask.prpodp, 1, 'Законный представитель', 2, 'Уполномоченный представитель', '') as signatory_status
  ,case when knp_status.zip is null then ''
    when knp_status.zip is not null and knp_status.calc_is_open is null then 'Открыта'
		when knp_status.calc_is_open = 0 then 'Закрыта'
		else 'Открыта'
   end as status_knp 
  ,cast(
   case when knp_status.zip is null then 0  
    when knp_status.zip is not null and knp_status.calc_is_open is null then 1
	  when knp_status.calc_is_open = 1 then 1
	  else 2
   end as number(1))  as status_knp_code
  ,tp.is_large
  ,ask.okved as okved_code
  ,ask.summanalischisl as nds_summanalischisl
  ,ask.summavych as nds_summavych
  ,ask.summands as nds_total
  ,ask.nds_weight as nds_weight
  ,summary.discrep_total_count as discrepancy_qty
  ,summary.discrep_total_amnt as discrepancy_amt
  ,summary.discrep_gap_count as discrep_gap_qty
  ,summary.discrep_gap_amnt as discrep_gap_amnt
  ,summary.discrep_min_amnt as discrepancy_amt_min
  ,summary.discrep_max_amnt as discrepancy_amt_max
  ,summary.discrep_avg_amnt as discrepancy_amt_avg
  ,DECODE(ask.TYPE, 0, nvl(KSAggr.KSCount, 0), null) as control_ratio_qty--?
  ,summary.pvp_total_amnt as pvp
  ,summary.pvp_min_amnt as pvp_min
  ,summary.pvp_max_amnt as pvp_max
  ,summary.pvp_avg_amnt as pvp_avg
  ,case when knps.inn is not null then 1 else 0 end as has_discrepancy_knp
  ,ask.priznnal8 as ch8_provided
  ,summary.ch8_discrep_amnt as ch8_discrepancy_amt
  ,summary.ch8_pvp_amnt as ch8_pvp
  ,summary.ch8_nds as ch8_nds
  ,knpc.all_contractor_count_8 as ch8_knp_contractor_qty
  ,summary.CH8_CONTRACTOR_COUNT as ch8_contractor_qty
  ,knps.gap_count_8 as ch8_knp_gap_qty
  ,knps.gap_amount_8 as ch8_knp_gap_amt
  ,knps.other_count_8 as ch8_knp_other_qty
  ,knps.other_amount_8 as ch8_knp_other_amt
  ,knpc.gap_contractor_count_8 as ch8_knp_gap_contractor_qty
  ,knpc.other_contractor_count_8 as ch8_knp_other_contractor_qty
  ,ask.priznnal9 as ch9_provided
  ,summary.ch9_discrep_amnt as ch9_discrepancy_amt
  ,summary.ch9_pvp_amnt as ch9_pvp
  ,summary.ch9_nds as ch9_nds
  ,sda.total_contragent_qty as ch9_knp_contractor_qty
  ,summary.CH9_CONTRACTOR_COUNT as ch9_contractor_qty
  ,sda.gap_qty_9 as ch9_knp_gap_qty
  ,sda.gap_amt_9 as ch9_knp_gap_amt
  ,sda.other_qty_9 as ch9_knp_other_qty
  ,sda.other_amt_9 as ch9_knp_other_amt
  ,sda.gap_contragent_qty_9 as ch9_knp_gap_contractor_qty
  ,sda.other_contragent_qty_9 as ch9_knp_other_contractor_qty
  ,summary.ch10_discrep_amnt as ch10_discrepancy_amt
  ,summary.ch10_pvp_amnt as ch10_pvp
  ,knpc.all_contractor_count_10 as ch10_knp_contractor_qty
  ,summary.CH10_CONTRACTOR_COUNT as ch10_contractor_qty
  ,knps.gap_count_10 as ch10_knp_gap_qty
  ,knps.gap_amount_10 as ch10_knp_gap_amt
  ,knps.other_count_10 as ch10_knp_other_qty
  ,knps.other_amount_10 as ch10_knp_other_amt
  ,knpc.gap_contractor_count_10 as ch10_knp_gap_contractor_qty
  ,knpc.other_contractor_count_10 as ch10_knp_other_contractor_qty
  ,summary.ch11_discrep_amnt as ch11_discrepancy_amt
  ,summary.ch11_pvp_amnt as ch11_pvp
  ,knpc.all_contractor_count_11 as ch11_knp_contractor_qty
  ,summary.CH11_CONTRACTOR_COUNT as ch11_contractor_qty
  ,knps.gap_count_11 as ch11_knp_gap_qty
  ,knps.gap_amount_11 as ch11_knp_gap_amt
  ,knps.other_count_11 as ch11_knp_other_qty
  ,knps.other_amount_11 as ch11_knp_other_amt
  ,knpc.gap_contractor_count_11 as ch11_knp_gap_contractor_qty
  ,knpc.other_contractor_count_11 as ch11_knp_other_contractor_qty
  ,summary.ch12_discrep_amnt as ch12_discrepancy_amt
  ,summary.ch12_pvp_amnt as ch12_pvp
  ,knpc.all_contractor_count_12 as ch12_knp_contractor_qty
  ,summary.CH12_CONTRACTOR_COUNT as ch12_contractor_qty
  ,knps.gap_count_12 as ch12_knp_gap_qty
  ,knps.gap_amount_12 as ch12_knp_gap_amt
  ,knps.other_count_12 as ch12_knp_other_qty
  ,knps.other_amount_12 as ch12_knp_other_amt
  ,knpc.gap_contractor_count_12 as ch12_knp_gap_contractor_qty
  ,knpc.other_contractor_count_12 as ch12_knp_other_contractor_qty
  ,summary.ch9_discrep_amnt + summary.ch12_discrep_amnt as ch9_ch12_discrepancy_amt
  ,summary.ch9_pvp_amnt + summary.ch12_pvp_amnt as ch9_ch12_pvp
  ,changes.has_changes
  ,changes.has_at
  ,changes.slide_at_count
  ,changes.no_tks_count
  ,nvl(knp_status.calc_status,0) as KNP_CLOSE_REASON_ID
  ,nvl2(dah.inn, 1, 0) as HAS_CANCELLED_CORRECTION
  ,nvl2(ak.inn, 1, 0) as CANCELLED
  ,case when ask.type != 0 then null
		when ask.priznnal8 > 0 or ask.priznnal81 > 0 then 'Подана'
		else 'Не подана'
   end as CH8_PRESENT_DESCRIPTION
  ,case when ask.type != 0 then null
		when ask.priznnal9 > 0 or ask.priznnal91 > 0 then 'Подана'
   else 'Не подана'
   end as CH9_PRESENT_DESCRIPTION
from NDS2_MRR_USER.ASK_DECLANDJRNL ask
left join 
(
	select sda.inn, sda.kpp, sda.fiscal_year, sda.period_code, sda.correction_number
	from nds2_mrr_user.declaration_annulment da
	join nds2_mrr_user.declaration_annulment_history dah on dah.annulment_id = da.id and dah.is_last = 1 and dah.status_id not in (3,4)
	join nds2_seod.seod_declaration_annulment sda on sda.id = da.seod_id
	group by sda.inn, sda.kpp, sda.fiscal_year, sda.period_code, sda.correction_number
) ak
	on ak.inn = ask.innnp
	and ak.kpp = ask.kppnp
    and ak.fiscal_year = ask.otchetgod 
    and ak.period_code = ask.period
	and ak.correction_number = ask.nomkorr
left join 
(
		select sda.inn, sda.kpp, sda.fiscal_year, sda.period_code
		from nds2_mrr_user.declaration_annulment da
		join nds2_mrr_user.declaration_annulment_history dah on dah.annulment_id = da.id and dah.is_last = 1
        left join nds2_mrr_user.declaration_annulment_history dar on dar.annulment_id = da.id and dar.status_id = 2  		
		join nds2_seod.seod_declaration_annulment sda on sda.id = da.seod_id
		where dah.status_id = 5 and dar.id is not null or dah.status_id in (1,2,3)
		group by sda.inn, sda.kpp, sda.fiscal_year, sda.period_code
) dah on
	    dah.inn = ask.innnp
	and dah.kpp = ask.kppnp
    and dah.fiscal_year = ask.otchetgod 
    and dah.period_code = ask.period
left join NDS2_SEOD.SEOD_REG_NUMBER seod on
    seod.tax_period = ask.period
    and seod.fiscal_year = ask.otchetgod
    and seod.inn = ask.innnp
    and seod.correction_number = ask.nomkorr
    and seod.id_file = ask.idfajl 
	and seod.sono_code = ask.KODNO
left join nds2_mrr_user.declaration_knp_status knp_status on knp_status.zip = ask.zip and ask.type = 0
join NDS2_MRR_USER.DICT_TAX_PERIOD dtp on dtp.code = ask.period
join NDS2_MRR_USER.TAX_PAYER tp on tp.inn = ask.innnp and tp.kpp_effective = ask.kpp_effective
left join NDS2_MRR_USER.CFG_REGION_GROUP rg on rg.region_code = substr(nvl(tp.sono_code_successor_top, tp.sono_code), 1, 2)
left join NDS2_MRR_USER.DECLARATION_JOURNAL_SUMMARY summary on summary.version_id = ask.zip and summary.mc_id = ask.id
left join NDS2_MRR_USER.DECLARATION_HISTORY dh on dh.zip = ask.zip  and dh.ASK_ID = ask.id
left join NDS2_MRR_USER.SOV_DISCREPANCY_AGGREGATE sda on ask.zip = sda.seller_zip and ask.innnp = sda.seller_inn
left join NDS2_MRR_USER.KNP_DECLARATION_SUMMARY knps on
    knps.inn = dh.inn_contractor
    and knps.kpp_effective = dh.kpp_effective
    and knps.type_code = dh.type_code
    and knps.period_code = dh.period_code
    and knps.fiscal_year = dh.fiscal_year
left join NDS2_MRR_USER.V$DECLARATION_CURRENT_ASSIGNEE ins_ass on
    ins_ass.declaration_type_code = ask.type
    and ins_ass.inn_declarant = ask.innnp
    and ins_ass.kpp_effective = ask.kpp_effective
    and ins_ass.fiscal_year = ask.otchetgod
    and ins_ass.period_effective = dtp.EFFECTIVE_VALUE
left join NDS2_MRR_USER.EXT_SUR dsur on
    dsur.inn = ask.innnp
    and dsur.kpp_effective = ask.kpp_effective
    and dsur.fiscal_year = ask.otchetgod
    and dsur.fiscal_period = ask.period
left join NDS2_MRR_USER.DICT_SUR sur on sur.code = dsur.sign_code
join NDS2_MRR_USER.V$SSRF dreg on dreg.s_code = to_number(substr(nvl(tp.sono_code_successor_top, tp.sono_code), 1, 2))
join NDS2_MRR_USER.V$SONO dsono on dsono.s_code = nvl(tp.sono_code_successor_top, tp.sono_code)
left join (
  select
    changes.inn_contractor,
    changes.kpp_effective,
    changes.fiscal_year,
    changes.period_code,
    max(changes.has_changes)    as has_changes,
    max(changes.has_at)         as has_at,
    sum(changes.slide_at_count) as slide_at_count,
    sum(changes.no_tks_count)   as no_tks_count
  from NDS2_MRR_USER.SIGNIFICANT_CHANGES changes
  group by
    changes.inn_contractor, changes.kpp_effective, changes.fiscal_year, changes.period_code
  ) changes on changes.inn_contractor = case when ask.innreorg is null then ask.innnp else ask.innreorg end
    and changes.kpp_effective = ask.kpp_effective
    and changes.fiscal_year = ask.otchetgod
    and changes.period_code = ask.period
left join (
  select
    knp_group.inn, knp_group.kpp_effective, knp_group.type_code, knp_group.period_code, knp_group.fiscal_year,
    sum(case when knp_group.gap_quantity_8 > 0 then 1 else 0 end) as gap_contractor_count_8,
    sum(case when knp_group.gap_quantity_9 > 0 then 1 else 0 end) as gap_contractor_count_9,
    sum(case when knp_group.gap_quantity_10 > 0 then 1 else 0 end) as gap_contractor_count_10,
    sum(case when knp_group.gap_quantity_11 > 0 then 1 else 0 end) as gap_contractor_count_11,
    sum(case when knp_group.gap_quantity_12 > 0 then 1 else 0 end) as gap_contractor_count_12,
    sum(case when knp_group.other_quantity_8 > 0 then 1 else 0 end) as other_contractor_count_8,
    sum(case when knp_group.other_quantity_9 > 0 then 1 else 0 end) as other_contractor_count_9,
    sum(case when knp_group.other_quantity_10 > 0 then 1 else 0 end) as other_contractor_count_10,
    sum(case when knp_group.other_quantity_11 > 0 then 1 else 0 end) as other_contractor_count_11,
    sum(case when knp_group.other_quantity_12 > 0 then 1 else 0 end) as other_contractor_count_12,
    sum(case when knp_group.gap_quantity_8 + knp_group.other_quantity_8 > 0 then 1 else 0 end) as all_contractor_count_8,
    sum(case when knp_group.gap_quantity_9 + knp_group.other_quantity_9 > 0 then 1 else 0 end) as all_contractor_count_9,
    sum(case when knp_group.gap_quantity_10 + knp_group.other_quantity_10 > 0 then 1 else 0 end) as all_contractor_count_10,
    sum(case when knp_group.gap_quantity_11 + knp_group.other_quantity_11 > 0 then 1 else 0 end) as all_contractor_count_11,
    sum(case when knp_group.gap_quantity_12 + knp_group.other_quantity_12 > 0 then 1 else 0 end) as all_contractor_count_12
  from (
    select
      knp.inn, knp.kpp_effective, knp.type_code, knp.period_code, knp.fiscal_year,
      knp.inn_contractor, knp.kpp_effective_contractor,
      sum(knp.gap_quantity_8) as gap_quantity_8,
      sum(knp.gap_quantity_9) as gap_quantity_9,
      sum(knp.gap_quantity_10) as gap_quantity_10,
      sum(knp.gap_quantity_11) as gap_quantity_11,
      sum(knp.gap_quantity_12) as gap_quantity_12,
      sum(knp.other_quantity_8) as other_quantity_8,
      sum(knp.other_quantity_9) as other_quantity_9,
      sum(knp.other_quantity_10) as other_quantity_10,
      sum(knp.other_quantity_11) as other_quantity_11,
      sum(knp.other_quantity_12) as other_quantity_12
    from KNP_RELATION_SUMMARY knp
    group by knp.inn, knp.kpp_effective, knp.type_code, knp.period_code, knp.fiscal_year,
      knp.inn_contractor, knp.kpp_effective_contractor
    ) knp_group
  group by knp_group.inn, knp_group.kpp_effective, knp_group.type_code, knp_group.period_code, knp_group.fiscal_year
) knpc on
    knpc.inn = nvl(ask.innreorg, ask.innnp)
    and knpc.kpp_effective = ask.kpp_effective
    and knpc.period_code = ask.period
    and knpc.fiscal_year = ask.otchetgod
left join (
  select ksotn.IdDekl as decl_id, count(1) as KSCount from v$askkontrsoontosh ksotn where (ksotn.Vypoln = 0) and ksotn.DeleteDate is null group by ksotn.IdDekl
) KSAggr on KSAggr.decl_id = ask.ID
where ask.prizn_akt_korr = 1;
