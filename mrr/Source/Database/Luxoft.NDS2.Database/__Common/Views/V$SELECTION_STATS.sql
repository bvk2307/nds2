﻿create or replace force view NDS2_MRR_USER.V$SELECTION_STATS as
select
  rownum as LineNumber
  ,trunc(t.createdate) as CreateDate
  ,t.DocId
  ,dtp.description||' '||t.taxyear as TaxPeriod
  ,t.RegionCode
  ,t.InspectionCode
  ,t.Inn
  ,t.Kpp
  ,t.Side1TotalAmount
  ,t.Side1TotalQuantity
  ,t.Side1GapAmount
  ,t.Side1GapQuantity
  ,t.Side1NdsAmount
  ,t.Side1NdsQuantity
  ,t.Side2TotalAmount
  ,t.Side2TotalQuantity
  ,t.Side2GapAmount
  ,t.Side2GapQuantity
  ,t.Side2NdsAmount
  ,t.Side2NdsQuantity
  ,t.TotalAmount
  ,t.TotalQuantity
  ,t.GapAmount
  ,t.GapQuantity
  ,t.NdsAmount
  ,t.NdsQuantity
  ,t.ClaimTotalAmount
  ,t.ClaimTotalQuantity
  ,t.ClaimGapAmount
  ,t.ClaimGapQuantity
  ,t.ClaimNdsAmount
  ,t.ClaimNdsQuantity
from selection_stats t
left join dict_tax_period dtp on dtp.code = t.taxperiod;
