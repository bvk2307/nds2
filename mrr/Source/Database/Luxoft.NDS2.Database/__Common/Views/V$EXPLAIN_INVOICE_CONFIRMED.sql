﻿create or replace force view NDS2_MRR_USER.V$EXPLAIN_INVOICE_CONFIRMED as
select 
   explain_zip as ExplainZip
  ,status
  ,operation_codes_bit as OperationCodesBit
  ,operation_codes_bit_new as NewOperationCodesBit
  ,invoice_number as InvoiceNumber
  ,invoice_number_new as NewInvoiceNumber
  ,invoice_date as InvoiceDate
  ,invoice_date_new as NewInvoiceDate
  ,invoice_chapter as Chapter  
  ,buyer_inn as BuyerInn
  ,buyer_inn_new as NewBuyerInn
  ,buyer_kpp as BuyerKpp
  ,buyer_kpp_new as NewBuyerKpp
  ,broker_inn as BrokerInn
  ,broker_inn_new as NewBrokerInn
  ,broker_kpp as BrokerKpp
  ,broker_kpp_new as NewBrokerKpp
  ,amt as Amount
  ,amt_new as NewAmount
  ,amt_rur as AmountRur
  ,amt_rur_new as NewAmountRur
from EXPLAIN_INVOICE_CONFIRMED;
