﻿create or replace force view NDS2_MRR_USER.V$MACRO_TAXPAYER_DETAIL_BUILD as
select
    da.inn_declarant as INN
  , da.KPP as KPP
  , da.name_short as TAX_NAME
  , da.FISCAL_YEAR as TAX_YEAR
  , dtp.QUARTER
  , da.sono_code as SONO_CODE
  , nvl(da.discrep_gap_qty, 0) as GAP_COUNT
  , Round(nvl(da.discrep_gap_amnt, 0),0) as GAP_SUM  
  , Round((case when nvl(da.nds_summanalischisl, 0) > 0 then da.nds_summanalischisl else 0 end),0) as NDS_CALCULATED
  , Round((case when nvl(da.nds_summavych, 0) > 0 then da.nds_summavych else 0 end),0) as NDS_DEDUCTION
  , (case when nvl(da.nds_summanalischisl, 0) <> 0 then Round(nvl(da.nds_summavych, 0) / nvl(da.nds_summanalischisl, 0) * 100, 2) else 0 end) as SHARE_DEDUCTIONS
  , Round(nvl(da.nds_total, 0),0) as NDS_TOTAL
from 
DECLARATION_ACTIVE da 
join (select distinct EFFECTIVE_VALUE, QUARTER FROM DICT_TAX_PERIOD) dtp on dtp.EFFECTIVE_VALUE = da.period_effective
where da.type_code = 0;
