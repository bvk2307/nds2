﻿create or replace force view NDS2_MRR_USER.V$EXPLAIN_INVOICE_UNCONFIRMED as
select 
   zip as ExplainZip
  ,nomschfprod as InvoiceNumber
  ,dataschfprod as InvoiceDate
  ,inn as ContractorInn
  ,kpp as ContractorKpp
from v$askPoyasnenieNepodtv;
