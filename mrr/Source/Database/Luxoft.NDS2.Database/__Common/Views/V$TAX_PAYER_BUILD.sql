﻿create or replace force view NDS2_MRR_USER.V$TAX_PAYER_BUILD as
select
  cast(vtp.name_full as varchar2(1024 CHAR)) as name_full,
  cast(vtp.np_name as varchar2(1024 CHAR)) as name_short,
  cast(vtp.inn as varchar2(12 CHAR)) as inn,
  cast(coalesce(decl.kppnp, vtp.kpp) as varchar2(9 CHAR)) as kpp,
  cast(vtp.kpp_effective as varchar2(9 CHAR)) as kpp_effective,
  cast(vtp.address as varchar2(1024 CHAR)) as address,
  cast(substr(nvl(decl.sono_code, vtp.sono_code), 1, 4) as varchar2(4 CHAR)) as sono_code,
  cast(substr(nvl(decl.sono_code, vtp.sono_code), 1, 2) as varchar2(2 CHAR)) as region_code,
  cast(vtp.date_reg as DATE) as date_reg,
  cast(vtp.date_on as DATE) as date_on,
  cast(vtp.date_off as DATE) as date_off,
  cast(vtp.ogrn as varchar2(45 CHAR)) as ogrn,
  cast(vtp.ust_capital as NUMBER) as ust_capital,
  cast(sur.sign_code as char(1 CHAR)) as sur_code,
  cast(decl.okved as varchar2(8 CHAR)) as okved_code,
  case length(vtp.inn) when 10 then 0 else 1 end as type,
  decode(substr(vtp.kpp, 5, 2), '50', '1', '0') as is_large,
  cast(nvl(reorg.inn_successor, tp.inn_successor) as varchar2(12 CHAR)) as inn_successor,
  cast(nvl(reorg.kpp_successor, tp.kpp_successor) as varchar2(9 CHAR)) as kpp_successor,
  cast(nvl(reorg.name_successor, tp.name_successor) as varchar2(1024 CHAR)) as name_successor,
  cast(nvl(reorg.kpp_effective_successor, tp.kpp_effective_successor) as varchar2(9 CHAR)) as kpp_effective_successor,
  cast(nvl(reorgult.inn_successor, nvl(reorgtop.inn_successor, tp.inn_successor)) as varchar2(12 CHAR)) as inn_successor_top,
  cast(nvl(reorgult.kpp_effective_successor, nvl(reorgtop.kpp_effective_successor, tp.kpp_effective_successor)) as varchar2(9 CHAR)) as kpp_effective_successor_top,
  cast(nvl(reorgult.sono_code, nvl(reorgtop.sono_code, nvl(reorg.sono_code, decl.sono_code))) as varchar2(4 CHAR)) as sono_code_successor_top,
  cast(decl.familiyanp as VARCHAR2(60 CHAR)) as familiyanp,
  cast(decl.imyanp as VARCHAR2(60 CHAR)) as imyanp,
  cast(decl.otchestvonp as VARCHAR2(60 CHAR)) as otchestvonp
from NDS2_MRR_USER.MV$TAX_PAYER_NAME vtp
left join NDS2_MRR_USER.TAX_PAYER tp on tp.inn = vtp.inn and tp.kpp_effective = vtp.kpp_effective
left join NDS2_MRR_USER.REORG_TMP reorg on reorg.inn_reorganized = vtp.inn and reorg.kpp_effective_reorganized = vtp.kpp_effective
left join NDS2_MRR_USER.REORG_TMP reorgtop on reorgtop.inn_reorganized = tp.inn_successor and reorgtop.kpp_effective_reorganized = tp.kpp_effective_successor
left join NDS2_MRR_USER.REORG_TMP reorgult on reorgult.inn_reorganized = tp.inn_successor_top and reorgult.kpp_effective_reorganized = tp.kpp_effective_successor_top
left join (
  select
    t.okved, t.innnp, t.kppnp, t.kpp_effective, t.kodno as sono_code, t.familiyanp, t.imyanp, t.otchestvonp,
    row_number() over (partition by t.innnp, t.kpp_effective order by t.insert_date desc) as rn
  from NDS2_MRR_USER.ASK_DECLANDJRNL t
  where t.prizn_akt_korr = 1) decl on decl.innnp = vtp.inn and decl.kpp_effective = vtp.kpp_effective and decl.rn = 1
left join ( 
	select x.* from (
	select row_number() over (partition by inn, kpp_effective order by fiscal_year desc, fiscal_period desc) as num, t.*
	from EXT_SUR t where t.is_actual = 1
	) x  where x.num = 1
)sur on sur.inn = vtp.inn and sur.kpp_effective = vtp.kpp_effective
union  --Из деклараций
select
  cast(decl.name_full as varchar2(1024 CHAR)) as name_full,
  cast(decl.name_full as varchar2(1024 CHAR)) as name_short,
  cast(decl.inn as varchar2(12 CHAR)) as inn,
  cast(decl.kpp as varchar2(9 CHAR)) as kpp,
  cast(decl.kpp_effective as varchar2(9 CHAR)) as kpp_effective,
  cast(null as varchar2(1024 CHAR)) as address,
  cast(substr(decl.sono_code, 1, 4) as varchar2(4 CHAR)) as sono_code,
  cast(substr(decl.sono_code, 1, 2) as varchar2(2 CHAR)) as region_code,
  cast(null as DATE) as date_reg,
  cast(null as DATE) as date_on,
  cast(null as DATE) as date_off,
  cast(null as varchar2(45 CHAR)) as ogrn,
  cast(null as NUMBER) as ust_capital,
  cast(sur.sign_code as char(1 CHAR)) as sur_code,
  cast(decl.okved as varchar2(8 CHAR)) as okved_code,
  case length(decl.inn) when 10 then 0 else 1 end as type,
  decode(substr(decl.kpp, 5, 2), '50', '1', '0') as is_large,
  cast(reorg.inn_successor as varchar2(12 CHAR)) as inn_successor,
  cast(reorg.kpp_successor as varchar2(9 CHAR)) as kpp_successor,
  cast(reorg.name_successor as varchar2(1024 CHAR)) as name_successor,
  cast(reorg.kpp_effective_successor as varchar2(9 CHAR)) as kpp_effective_successor,
  cast(reorg.inn_successor as varchar2(12 CHAR)) as inn_successor_top,
  cast(reorg.kpp_effective_successor as varchar2(9 CHAR)) as kpp_effective_successor_top,
  cast(nvl(reorg.sono_code, decl.sono_code) as varchar2(4 CHAR)) as sono_code_successor_top,
  cast(decl.familiyanp as VARCHAR2(60 CHAR)) as familiyanp,
  cast(decl.imyanp as VARCHAR2(60 CHAR)) as imyanp,
  cast(decl.otchestvonp as VARCHAR2(60 CHAR)) as otchestvonp
from (
  select
    t.okved, t.innnp as inn, t.kpp_effective, t.kppnp as kpp, t.kodno as sono_code,
    decode(t.type, 0, t.naimorg, t.familiyanp||' '||t.imyanp||' '||t.otchestvonp) as name_full,
    t.familiyanp, t.imyanp, t.otchestvonp,
    row_number() over (partition by t.innnp, t.kpp_effective order by t.insert_date desc) as rn
  from NDS2_MRR_USER.ASK_DECLANDJRNL t
  where t.prizn_akt_korr = 1
) decl
left join NDS2_MRR_USER.MV$TAX_PAYER_NAME vtp on vtp.inn = decl.inn and vtp.kpp_effective = decl.kpp_effective
left join NDS2_MRR_USER.REORG_TMP reorg on reorg.inn_reorganized = decl.inn and reorg.kpp_effective_reorganized = decl.kpp_effective
left join ( 
	select x.* from (
	select row_number() over (partition by inn, kpp_effective order by fiscal_year desc, fiscal_period desc) as num, t.*
	from EXT_SUR t where t.is_actual = 1
	) x  where x.num = 1
)sur on sur.inn = decl.inn and sur.kpp_effective = decl.kpp_effective
where vtp.inn is null and decl.rn = 1
union --Из деклараций по реорганизованным
select
  cast('' as varchar2(1024 CHAR)) as name_full,
  cast('' as varchar2(1024 CHAR)) as name_short,
  cast(decl.inn as varchar2(12 CHAR)) as inn,
  cast(decl.kpp as varchar2(9 CHAR)) as kpp,
  cast(decl.kpp_effective as varchar2(9 CHAR)) as kpp_effective,
  cast(null as varchar2(1024 CHAR)) as address,
  cast(null as varchar2(4 CHAR)) as sono_code,
  cast(null as varchar2(2 CHAR)) as region_code,
  cast(null as DATE) as date_reg,
  cast(null as DATE) as date_on,
  cast(null as DATE) as date_off,
  cast(null as varchar2(45 CHAR)) as ogrn,
  cast(null as NUMBER) as ust_capital,
  cast(sur.sign_code as char(1 CHAR)) as sur_code,
  cast(null as varchar2(8 CHAR)) as okved_code,
  case length(decl.inn) when 10 then 0 else 1 end as type,
  decode(substr(decl.kpp, 5, 2), '50', '1', '0') as is_large,
  cast(reorg.inn_successor as varchar2(12 CHAR)) as inn_successor,
  cast(reorg.kpp_successor as varchar2(9 CHAR)) as kpp_successor,
  cast(reorg.name_successor as varchar2(1024 CHAR)) as name_successor,
  cast(reorg.kpp_effective_successor as varchar2(9 CHAR)) as kpp_effective_successor,
  cast(reorg.inn_successor as varchar2(12 CHAR)) as inn_successor_top,
  cast(reorg.kpp_effective_successor as varchar2(9 CHAR)) as kpp_effective_successor_top,
  cast(reorg.sono_code as varchar2(4 CHAR)) as sono_code_successor_top,
  cast('' as VARCHAR2(60 CHAR)) as familiyanp,
  cast('' as VARCHAR2(60 CHAR)) as imyanp,
  cast('' as VARCHAR2(60 CHAR)) as otchestvonp
from (
  select
    t.innreorg as inn, t.kpp_effective as kpp_effective, t.kppreorg as kpp,
    row_number() over (partition by t.innreorg, t.kpp_effective order by t.insert_date desc) as rn
  from NDS2_MRR_USER.ASK_DECLANDJRNL t
  where t.prizn_akt_korr = 1
) decl
left join (
   select
    t.innnp as inn, t.kpp_effective,
    row_number() over (partition by t.innnp, t.kpp_effective order by t.insert_date desc) as rn
  from NDS2_MRR_USER.ASK_DECLANDJRNL t
  where t.prizn_akt_korr = 1
) decl2 on decl2.inn = decl.inn and decl2.kpp_effective = decl.kpp_effective and decl2.rn = 1
left join NDS2_MRR_USER.MV$TAX_PAYER_NAME vtp on vtp.inn = decl.inn and vtp.kpp_effective = decl.kpp_effective
left join NDS2_MRR_USER.REORG_TMP reorg on reorg.inn_reorganized = decl.inn and reorg.kpp_effective_reorganized = decl.kpp_effective
left join ( 
	select x.* from (
	select row_number() over (partition by inn, kpp_effective order by fiscal_year desc, fiscal_period desc) as num, t.*
	from EXT_SUR t where t.is_actual = 1
	) x  where x.num = 1
)sur on sur.inn = decl.inn and sur.kpp_effective = decl.kpp_effective
where vtp.inn is null and decl2.inn is null and decl.rn = 1;
