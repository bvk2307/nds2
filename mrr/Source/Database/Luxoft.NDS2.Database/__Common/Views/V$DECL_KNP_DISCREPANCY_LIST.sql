﻿CREATE OR REPLACE force VIEW NDS2_MRR_USER.V$DECL_KNP_DISCREPANCY_LIST as
SELECT
        knpd.DISCREPANCY_ID as DiscrepancyId
       ,knpd.FOUND_DATE as FoundDate
       ,knpd.ZIP as TaxPayerZip
       ,knpd.CONTRACTOR_ZIP as ContractorZip
       ,knpd.INN_DECLARANT as TaxPayerInn
       ,knpd.KPP_EFFECTIVE as KppEffective
       ,knpd.FISCAL_YEAR as FiscalYear
       ,knpd.PERIOD_CODE as PeriodCode
       ,knpd.INVOICE_ROW_KEY as TaxPayerInvoiceKey
       ,knpd.CONTRACTOR_INVOICE_ROW_KEY as ContractorInvoiceKey
       ,case 
           when knpd.INVOICE_CHAPTER is null then '' else
                 'Раздел ' 
              || knpd.INVOICE_CHAPTER 
              || case knpd.INVOICE_CHAPTER
                      when 8 then ' (КнПок)'
                      when 9 then ' (КнПрод)'
                      when 10 then ' (ЖвСФ)'
                      when 11 then ' (ЖпСФ)'
                      when 12 then ' (КнПрод)'
                      else '' 
                 end
        end as TaxPayerInvoiceSource
       ,decode(knpd.TAXPAYER_DECL_SIGN, 1, 'К уплате', 2, 'К возмещению', 3, 'Нулевая', '')  as TaxPayerDeclSign
       ,knpd.CONTRACTOR_INN_DECLARANT as ContractorInn
       ,knpd.CONTRACTOR_KPP as ContractorKpp
       ,knpd.CONTRACTOR_NAME as ContractorName
       ,case when knpd.CONTRACTOR_INVOICE_CHAPTER is null then '' else
                'Раздел ' 
             || knpd.CONTRACTOR_INVOICE_CHAPTER 
             || case knpd.CONTRACTOR_INVOICE_CHAPTER
                     when 8 then ' (КнПок)'
                     when 9 then ' (КнПрод)'
                     when 10 then ' (ЖвСФ)'
                     when 11 then ' (ЖпСФ)'
                     when 12 then ' (КнПрод)'
                     else ''
                end
        end as ContractorInvoiceSource
       ,decode(knpd.CONTRACTOR_DECL_SIGN, 1, 'К уплате', 2, 'К возмещению', 3, 'Нулевая', '')  as ContractorDeclSign
       ,knpd.DISCREPANCY_AMT as Amount
       ,knpd.DISCREPANCY_AMT_PVP as AmountPvp
       ,knpd.DISCREPANCY_STATUS as StatusCode
       ,knpd.DISCREPANCY_TYPE as TypeCode       
       ,st.name as StatusName
       ,knpd.CONTRACTOR_NDS_AMT as ContractorNdsAmount
       ,tp.DESCRIPTION as TypeName
       ,tp.CAPTION as TypeNameForCaption
       ,knpd.CONTRACTOR_INN_REORGANIZED as ContractorInnReorganized
       ,decode(ds.CALC_IS_OPEN, 0, 'Закрыта', 1, 'Открыта', '') as KnpStatus
       ,knpd.INVOICE_NUMBER as CreatedByInvoiceNumber
       ,knpd.INVOICE_DATE as CreatedByInvoiceDate
       ,knpd.ACT_INN as ActInn
       ,knpd.ACT_KPP as ActKpp
       ,kdt.name as KnpDocType
       ,knpd.KNP_DOC_AMOUNT as KnpDocAmount
       ,knpd.DECISION_AMOUNT_DIFFERENCE as DecisionAmountDifference
       ,dtp.EFFECTIVE_VALUE as PeriodEffective
       ,knpd.CLOSE_DATE as CloseDate 
       ,duc.COMMENT_TEXT as UserComment 
       ,knpd.INVOICE_CHAPTER as WorkSideInvoiceChapter 
       ,knpd.CONTRACTOR_INVOICE_CHAPTER as ContractorInvoiceChapter 
	   ,decode(knpd.contractor_type_code, 0, 'Декларация', 'Журнал') as ContractorDocType
       ,knpd.INN_REORGANIZED as TaxPayerReorgInn 
       ,knpd.KPP as TaxPayerKpp 
       ,knpd.CONTRACTOR_INN as OtherSideINN 
FROM KNP_DISCREPANCY_DECLARATION knpd 
JOIN DICT_DISCREPANCY_TYPE tp on tp.s_code = knpd.DISCREPANCY_TYPE
LEFT JOIN DICT_DISCREPANCY_KNPDOC_TYPE kdt on kdt.id = knpd.KNP_DOC_TYPE
LEFT JOIN DICT_DISCREPANCY_STATUS st on st.id = knpd.DISCREPANCY_STATUS
JOIN DICT_TAX_PERIOD dtp on knpd.PERIOD_CODE = dtp.CODE
LEFT JOIN DISCREPANCY_USER_COMMENTS duc on knpd.DISCREPANCY_ID = duc.DISCREPANCY_ID
LEFT JOIN DECLARATION_KNP_STATUS ds on ds.zip = knpd.CONTRACTOR_ZIP;
