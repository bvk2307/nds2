﻿CREATE OR REPLACE FORCE VIEW NDS2_MRR_USER.V$DOC_INVOICE_LIGHT AS
select
   di.doc_id
  ,null as IS_CHANGED
  ,di.INVOICE_CHAPTER as CHAPTER
  ,di.INVOICE_NUM
  ,di.INVOICE_DATE
  ,di.INVOICE_ROW_KEY as ROW_KEY
  ,(case when di.invoice_chapter = 8 then di.PRICE_BUY_AMOUNT
        when di.invoice_chapter = 9 then (case when di.PRICE_SELL is not null then di.PRICE_SELL
                                    when di.PRICE_SELL_IN_CURR is not null then di.PRICE_SELL_IN_CURR else 0 end)
        when di.invoice_chapter = 10 then di.PRICE_TOTAL
        when di.invoice_chapter = 11 then di.PRICE_TOTAL
        when di.invoice_chapter = 12 then di.PRICE_TOTAL else 0 end) as CALC_PRICE_WITH_NDS
  ,(case when di.invoice_chapter = 8 then di.PRICE_NDS_TOTAL
        when di.invoice_chapter = 9 then nvl(di.PRICE_NDS_18, 0) + nvl(di.PRICE_NDS_10, 0)
        when di.invoice_chapter = 10 then di.PRICE_NDS_TOTAL
        when di.invoice_chapter = 11 then di.PRICE_NDS_TOTAL
        when di.invoice_chapter = 12 then di.PRICE_NDS_TOTAL else 0 end) as CALC_PRICE_NDS
  ,(tp.description || ' ' || di.DECL_FISCAL_YEAR) as DISPLAY_FULL_TAX_PERIOD
  ,di.GAP_AMOUNT
  ,di.NDS_AMOUNT
from doc_invoice di
inner join doc d
	on d.doc_id = di.doc_id
left join DICT_TAX_PERIOD tp 
	on tp.code = di.Decl_Tax_Period
where 
	-- если АТ по Сф то учитываем признак - "Неотраженная операция", т.к. он заполняется только для них в хиве
	decode(d.doc_type, 1, di.is_affected, 1) = 1; 
