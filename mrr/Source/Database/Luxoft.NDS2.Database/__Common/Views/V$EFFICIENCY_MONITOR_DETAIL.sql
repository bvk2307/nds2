﻿create or replace force view NDS2_MRR_USER.V$EFFICIENCY_MONITOR_DETAIL as
  select
    stg.Id as IdDekl,
    stg.calc_date as CalculationDate,
    stg.sono_code as SonoCode,
    substr(stg.sono_code, 1,2)||'00' as UfnsCode,
    stg.quarter as Quarter,
    stg.period as Period,
    stg.fiscal_year as FiscalYear,
    sono.s_name as SonoName,
    ssrf.s_code as RegionCode,
    ssrf.s_name as RegionName,
    stg.inn as  Inn,
    tp.name_short as TaxPayerName,
    d.SUMMANDS as NdsSum,
    stg.opn_knp_cnt as OpenKnpDiscrepancyCount,
    stg.opn_knp_amnt as OpenKnpDiscrepancyAmount,
    stg.opn_gap_knp_cnt as OpenKnpGapDiscrepancyCount,
    stg.opn_gap_knp_amnt as OpenKnpGapDiscrepancyAmount,
    stg.opn_nds_knp_cnt as OpenKnpNdsDiscrepancyCount,
    stg.opn_nds_knp_amnt as OpenKnpNdsDiscrepancyAmount,
    ins_ass.name as AssignedInspectorName,
    sur.sign_code as SurCode,
    to_number(nvl(d.priznaktkorr, 0 )) as IsActual
from efficiency_monitor_detail stg
join v$sono sono on sono.s_code = stg.sono_code
join v$ssrf ssrf on ssrf.s_code = substr(stg.sono_code, 1,2)
join v$askdekl d on d.id = stg.id
join tax_payer tp
    on tp.inn = d.innnp
    and tp.kpp_effective = coalesce(d.EFFKPP, F$GET_EFFECTIVE_KPP(d.INNNP, d.KPPNP))
join nds2_mrr_user.dict_tax_period dtp on dtp.code = d.period
left join nds2_mrr_user.V$DECLARATION_CURRENT_ASSIGNEE ins_ass
    on ins_ass.declaration_type_code = 0
    and ins_ass.inn_declarant = d.innnp
    and ins_ass.kpp_effective = d.effkpp
    and ins_ass.fiscal_year = d.otchetgod
    and ins_ass.period_effective = dtp.effective_value
left join ext_sur sur on
    sur.inn = d.innnp
    and sur.fiscal_period = d.period
    and sur.fiscal_year = d.otchetgod;
