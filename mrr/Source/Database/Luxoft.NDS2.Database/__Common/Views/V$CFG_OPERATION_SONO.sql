﻿create or replace force view NDS2_MRR_USER.v$cfg_operation_sono
as
select 
  cross.operation_csud_id as operation_csud_id,
  inspgrp.sono_code as GroupSONO,
  case
    when inspchild.sono_code is null and inspgrp.all_inspections_available = 'Y' then '****'
    when inspchild.sono_code is null and inspgrp.all_inspections_available = 'N' then inspgrp.sono_code
    else inspchild.sono_code
  end as ChildSONO
from CFG_OPERCSUD_TO_INSPECTGROUP cross
  inner join CFG_INSPECTION_GROUP_N inspgrp on inspgrp.id = cross.inspection_group_id
  left join CFG_INSPECTION_GROUP_N_TO_SONO inspchild on inspchild.inspection_group_id = inspgrp.id;
  
