﻿create or replace force view NDS2_MRR_USER.V$SELECTION_REGIONAL_BOUNDS
as
select 
id,
selection_template_id as SelectionTemplateId, 
include,
region_code as RegionCode,
discrep_total_amt as DiscrTotalAmt, 
discrep_min_amt as DiscrMinAmt,
discrep_gap_total_amt as DiscrGapTotalAmt,
discrep_gap_min_amt as DiscrGapMinAmt
from selection_regional_bounds;
