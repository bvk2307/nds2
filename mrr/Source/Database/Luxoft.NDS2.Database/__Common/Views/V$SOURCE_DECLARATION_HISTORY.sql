﻿create or replace force view NDS2_MRR_USER.V$SOURCE_DECLARATION_HISTORY as
  select
  ask.id as ask_id,
  ask.zip,
  seod.decl_reg_num as reg_number,
  seod.submission_date as submission_date,
  case when ask.innreorg is null then ask.innnp else ask.innreorg end as inn_contractor,
  ask.innnp as inn_declarant,
  ask.innreorg as inn_reorganized,
  ask.kppnp as kpp,
  ask.kppreorg as kpp_reorganized,
  case when ask.kppreorg is null then ask.kppnp else ask.kppreorg end as kpp_contractor,
  ask.kpp_effective as kpp_effective,
  ask.otchetgod as fiscal_year,
  ask.period as period_code,
  dtp.effective_value as period_effective,
  ask.prizn_akt_korr as is_active,
  ask.type as type_code,
  sur.sign_code as sur_code,
  ask.nd_prizn as sign,
  ask.summands as nds_total,
  ask.nomkorr as correction_number,
  ask.nomkorr_rank as correction_number_rank,
  case when ask.innreorg is null then to_char(ask.nomkorr) else to_char(ask.nomkorr)||' ('||ask.innreorg||')' end as correction_number_effective,
  CAST(ask.kodno as VARCHAR2(4 CHAR)) as sono_code_submited,
  CAST(nvl(tp.sono_code_successor_top, tp.sono_code) as VARCHAR2(4 CHAR)) as sono_code,
  CAST(substr(nvl(tp.sono_code_successor_top, tp.sono_code), 1, 2) as VARCHAR2(2 CHAR)) as region_code,
  seod.EOD_DATE as submit_date,
  tp.is_large,
  ask.insert_date
from NDS2_MRR_USER.ASK_DECLANDJRNL ask
join NDS2_MRR_USER.TAX_PAYER tp on tp.inn = ask.innnp and tp.kpp_effective = ask.kpp_effective
left join NDS2_SEOD.SEOD_REG_NUMBER seod on seod.tax_period = ask.period and seod.fiscal_year = ask.otchetgod and seod.inn = ask.innnp
                          and seod.correction_number = ask.nomkorr and seod.id_file = ask.idfajl and seod.sono_code = ask.KODNO
join NDS2_MRR_USER.DICT_TAX_PERIOD dtp on dtp.code = ask.period
left join NDS2_MRR_USER.EXT_SUR sur on sur.inn = ask.innnp and sur.kpp_effective = ask.kpp_effective
                                   and sur.fiscal_year = ask.otchetgod and sur.fiscal_period = ask.period;
