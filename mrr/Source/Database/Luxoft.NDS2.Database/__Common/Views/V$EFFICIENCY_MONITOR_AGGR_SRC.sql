﻿create or replace force view NDS2_MRR_USER.v$efficiency_monitor_aggr_src as
select
  substr(sono.s_code, 1,2) as region_code
  ,d.fiscal_year
  ,d.quarter
  ,d.calc_date
  ,sono.s_code as sono_code
  /* Доля закрытых расхождений КНП в расхождениях КНП всего (по сумме) */
  ,nvl((s.seller_dis_cls_knp_amnt + b.buyer_dis_cls_knp_amnt)/  nullif((b.buyer_dis_opn_knp_amnt + s.seller_dis_opn_knp_amnt + b.buyer_dis_cls_knp_amnt + s.seller_dis_cls_knp_amnt), 0), 0) as eff_idx_2_1
  /* Доля закрытых расхождений КНП в расхождениях КНП всего (по количеству) */
  ,nvl((s.seller_dis_cls_knp_cnt + b.buyer_dis_cls_knp_cnt) / nullif( ( b.buyer_dis_opn_knp_cnt + s.seller_dis_opn_knp_cnt + b.buyer_dis_cls_knp_cnt + s.seller_dis_cls_knp_cnt ), 0), 0) as eff_idx_2_2
  /* Доля деклараций с открытыми КНП расхождениями в декларациях с операциями (по количеству)*/
  ,nvl( d.opn_knp_dis_buyer_decl_cnt / nullif( d.operation_decl_cnt, 0), 0) as eff_idx_2_3
  /* Доля расхождений в сумме вычетов по НДС(КНП)*/
  ,nvl( (b.buyer_dis_opn_knp_amnt / nullif(d.deduction_amnt, 0)), 0 ) as eff_idx_2_4_knp
  /* Доля расхождений в сумме вычетов по НДС(Все)*/
  ,nvl( (b.buyer_dis_opn_amnt / nullif(d.deduction_amnt, 0)), 0 ) as eff_idx_2_4_all
  /*Данные по покупателю*/
  ,nvl(b.buyer_dis_opn_amnt, 0) as buyer_dis_opn_amnt
  ,nvl(b.buyer_dis_opn_knp_amnt, 0) as buyer_dis_opn_knp_amnt
  ,nvl(b.buyer_dis_opn_gap_amnt, 0) as buyer_dis_opn_gap_amnt
  ,nvl(b.buyer_dis_opn_gap_knp_amnt, 0) as buyer_dis_opn_gap_knp_amnt
  ,nvl(b.buyer_dis_opn_nds_amnt, 0) as buyer_dis_opn_nds_amnt
  ,nvl(b.buyer_dis_opn_nds_knp_amnt, 0) as buyer_dis_opn_nds_knp_amnt
  ,nvl(b.buyer_dis_all_knp_amnt, 0) as buyer_dis_all_knp_amnt
  ,nvl(b.buyer_dis_cls_amnt, 0) as buyer_dis_cls_amnt
  ,nvl(b.buyer_dis_cls_knp_amnt, 0) as buyer_dis_cls_knp_amnt
  ,nvl(b.buyer_dis_cls_gap_amnt, 0) as buyer_dis_cls_gap_amnt
  ,nvl(b.buyer_dis_cls_gap_knp_amnt, 0) as buyer_dis_cls_gap_knp_amnt
  ,nvl(b.buyer_dis_cls_nds_amnt, 0) as buyer_dis_cls_nds_amnt
  ,nvl(b.buyer_dis_cls_nds_knp_amnt, 0) as buyer_dis_cls_nds_knp_amnt
  ,nvl(b.buyer_dis_opn_knp_cnt, 0) as buyer_dis_opn_knp_cnt
  ,nvl(b.buyer_dis_opn_gap_knp_cnt, 0) as buyer_dis_opn_gap_knp_cnt
  ,nvl(b.buyer_dis_opn_nds_knp_cnt, 0) as buyer_dis_opn_nds_knp_cnt
  ,nvl(b.buyer_dis_cls_knp_cnt, 0) as buyer_dis_cls_knp_cnt
  ,nvl(b.buyer_dis_cls_gap_knp_cnt, 0) as buyer_dis_cls_gap_knp_cnt
  ,nvl(b.buyer_dis_cls_nds_knp_cnt, 0) as buyer_dis_cls_nds_knp_cnt
  /*Данные по продавцу*/
  ,nvl(s.seller_dis_opn_amnt, 0) as seller_dis_opn_amnt
  ,nvl(s.seller_dis_opn_knp_amnt, 0) as seller_dis_opn_knp_amnt
  ,nvl(s.seller_dis_opn_gap_amnt, 0) as seller_dis_opn_gap_amnt
  ,nvl(s.seller_dis_opn_gap_knp_amnt, 0) as seller_dis_opn_gap_knp_amnt
  ,nvl(s.seller_dis_opn_nds_amnt, 0) as seller_dis_opn_nds_amnt
  ,nvl(s.seller_dis_opn_nds_knp_amnt, 0) as seller_dis_opn_nds_knp_amnt
  ,nvl(s.seller_dis_all_knp_amnt, 0) as seller_dis_all_knp_amnt
  ,nvl(s.seller_dis_cls_amnt, 0) as seller_dis_cls_amnt
  ,nvl(s.seller_dis_cls_knp_amnt, 0) as seller_dis_cls_knp_amnt
  ,nvl(s.seller_dis_cls_gap_amnt, 0) as seller_dis_cls_gap_amnt
  ,nvl(s.seller_dis_cls_gap_knp_amnt, 0) as seller_dis_cls_gap_knp_amnt
  ,nvl(s.seller_dis_cls_nds_amnt, 0) as seller_dis_cls_nds_amnt
  ,nvl(s.seller_dis_cls_nds_knp_amnt, 0) as  seller_dis_cls_nds_knp_amnt
  ,nvl(s.seller_dis_opn_knp_cnt, 0) as seller_dis_opn_knp_cnt
  ,nvl(s.seller_dis_opn_gap_knp_cnt, 0) as seller_dis_opn_gap_knp_cnt
  ,nvl(s.seller_dis_opn_nds_knp_cnt, 0) as seller_dis_opn_nds_knp_cnt
  ,nvl(s.seller_dis_cls_knp_cnt, 0) as seller_dis_cls_knp_cnt
  ,nvl(s.seller_dis_cls_gap_knp_cnt, 0) as  seller_dis_cls_gap_knp_cnt
  ,nvl(s.seller_dis_cls_nds_knp_cnt, 0) as  seller_dis_cls_nds_knp_cnt
  /*Данные по декларации*/
  ,nvl(d.deduction_amnt, 0) as deduction_amnt
  ,nvl(d.calculation_amnt, 0) as calculation_amnt
  ,nvl(d.all_decl_cnt, 0) as decl_all_cnt
  ,nvl(d.all_decl_nds_sum, 0) as decl_all_nds_sum
  ,nvl(d.operation_decl_cnt, 0) as decl_operation_cnt
  ,nvl(dd.cnt, 0) as decl_discrepancy_cnt
  ,nvl(d.pay_decl_cnt, 0) as decl_pay_cnt
  ,nvl(pay_decl_nds_sum , 0 ) as decl_pay_nds_sum
  ,nvl(d.compensate_decl_cnt, 0) as decl_compensate_cnt
  ,nvl(compensate_decl_nds_sum, 0 ) as decl_compensate_nds_sum
  ,nvl(d.zero_decl_cnt, 0) as decl_zero_cnt
  ,nvl(d.opn_knp_dis_buyer_decl_cnt, 0) as decl_opn_knp_dis_buyer_cnt
from NDS2_MRR_USER.v$sono sono
join V$EFFICIENCY_MONITOR_DECL_SRC d
	on d.sono_code = sono.s_code
left join V$EFFICIENCY_MONITOR_BUYER_SRC b 
	on b.sono_code = d.sono_code 
	and trunc(b.calc_date) = trunc(d.calc_date) 
	and b.fiscal_year = d.fiscal_year 
	and b.quarter = d.quarter
left join V$EFFICIENCY_MONITOR_SLR_SRC s 
	on	s.sono_code = d.sono_code 
	and trunc(s.calc_date) = trunc(d.calc_date) 
	and s.fiscal_year = d.fiscal_year 
	and s.quarter = d.quarter
left join V$EFFICIENCY_MONITOR_DISC_DECL dd 
	on dd.sono_code = d.sono_code  
	and dd.fiscal_year = d.fiscal_year 
	and dd.qtr = d.quarter;
/

