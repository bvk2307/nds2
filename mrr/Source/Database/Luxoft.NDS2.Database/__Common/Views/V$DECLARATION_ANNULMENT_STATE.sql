﻿create or replace force view NDS2_MRR_USER.V$DECLARATION_ANNULMENT_STATE as
select 
    da.ID
  , da.INN
  , da.KPP
  , da.SONO_CODE
  , da.FISCAL_YEAR
  , da.PERIOD_CODE
  , da.CORRECTION_NUMBER
  , das_last.ID_FILE
  , das_last.REG_NUMBER
  , das_last.CANCELLED_SINCE
  , das_last.RECEIVED_AT
  , da_last.ACTIVE_BEFORE
  , dah_last.STATUS_ID
  , das_last.REASON_ID
  , ar.name as REASON_DESCRIPTION
  , NVL2(da_fin.INN, 1, 0) as IS_ANNULED
from 
(
	select sda.INN, sda.KPP, sda.SONO_CODE, sda.FISCAL_YEAR, sda.PERIOD_CODE, sda.CORRECTION_NUMBER, MAX(dai.ID) as ID
	from nds2_mrr_user.declaration_annulment dai
	join nds2_seod.seod_declaration_annulment sda on sda.id = dai.seod_id
	group by sda.INN, sda.KPP, sda.SONO_CODE, sda.FISCAL_YEAR, sda.PERIOD_CODE, sda.CORRECTION_NUMBER
) da
join nds2_mrr_user.declaration_annulment da_last on da_last.id = da.id
join nds2_mrr_user.declaration_annulment_history dah_last on dah_last.annulment_id = da_last.ID and dah_last.is_last = 1
join nds2_seod.seod_declaration_annulment das_last on das_last.id = da_last.seod_id
join nds2_seod.seod_decl_annulment_reason ar on ar.id = das_last.reason_id
left join 
(
    select sda2.inn, sda2.kpp, sda2.sono_code, sda2.fiscal_year, sda2.period_code,sda2.correction_number
    from
    (
      select annulment_id
      from nds2_mrr_user.declaration_annulment_history
      where status_id = 2
      group by annulment_id
    ) ah
    join nds2_mrr_user.declaration_annulment da2 on da2.id = ah.annulment_id
    join nds2_seod.seod_declaration_annulment sda2 on sda2.id = da2.seod_id
 ) da_fin
	on da.INN = da_fin.INN
	   and da.KPP = da_fin.KPP
	   and da.SONO_CODE = da_fin.SONO_CODE
	   and da.FISCAL_YEAR = da_fin.FISCAL_YEAR
	   and da.PERIOD_CODE = da_fin.PERIOD_CODE
	   and da.CORRECTION_NUMBER = da_fin.CORRECTION_NUMBER;
