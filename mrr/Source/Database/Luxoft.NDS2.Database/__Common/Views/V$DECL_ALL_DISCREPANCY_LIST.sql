﻿CREATE OR REPLACE force VIEW NDS2_MRR_USER.V$DECL_ALL_DISCREPANCY_LIST as
SELECT
        dfd.DISCREPANCY_ID as DiscrepancyId
       ,dfd.FOUND_DATE as FoundDate
       ,dfd.ZIP as TaxPayerZip
       ,dfd.CONTRACTOR_ZIP as ContractorZip
       ,dfd.INN_DECLARANT as TaxPayerInn
       ,dfd.INVOICE_ROW_KEY as TaxPayerInvoiceKey
       ,dfd.CONTRACTOR_INVOICE_ROW_KEY as ContractorInvoiceKey
       ,case 
           when dfd.INVOICE_CHAPTER is null then '' else
                 'Раздел ' 
              || dfd.INVOICE_CHAPTER 
              || case dfd.INVOICE_CHAPTER
                      when 8 then ' (КнПок)'
                      when 9 then ' (КнПрод)'
                      when 10 then ' (ЖвСФ)'
                      when 11 then ' (ЖпСФ)'
                      when 12 then ' (КнПрод)'
                      else '' 
                 end
        end as TaxPayerInvoiceSource
       ,decode(dfd.TAXPAYER_DECL_SIGN, 1, 'К уплате', 2, 'К возмещению', 3, 'Нулевая', '')  as TaxPayerDeclSign
       ,nvl(dfd.CONTRACTOR_INN_DECLARANT, nvl(taxp.INN_SUCCESSOR_TOP, CONTRACTOR_INN)) as ContractorInn
       ,dfd.CONTRACTOR_KPP as ContractorKpp
       ,dfd.CONTRACTOR_NAME as ContractorName
       ,case when dfd.CONTRACTOR_INVOICE_CHAPTER is null then '' else
                'Раздел ' 
             || dfd.CONTRACTOR_INVOICE_CHAPTER 
             || case dfd.CONTRACTOR_INVOICE_CHAPTER
                     when 8 then ' (КнПок)'
                     when 9 then ' (КнПрод)'
                     when 10 then ' (ЖвСФ)'
                     when 11 then ' (ЖпСФ)'
                     when 12 then ' (КнПрод)'
                     else ''
                end
        end as ContractorInvoiceSource
       ,decode(dfd.CONTRACTOR_DECL_SIGN, 1, 'К уплате', 2, 'К возмещению', 3, 'Нулевая', '')  as ContractorDeclSign
       ,dfd.DISCREPANCY_AMT as Amount
       ,dfd.DISCREPANCY_AMT_PVP as AmountPvp
       ,dfd.DISCREPANCY_STATUS as StatusCode
       ,dfd.DISCREPANCY_TYPE as TypeCode
       ,st.name as StatusName
       ,dfd.CONTRACTOR_NDS_AMT as ContractorNdsAmount
       ,tp.DESCRIPTION as TypeName
       ,tp.CAPTION as TypeNameForCaption
       ,dfd.CONTRACTOR_INN_REORGANIZED as ContractorInnReorganized
       ,decode(ds.CALC_IS_OPEN, 0, 'Закрыта', 1, 'Открыта', '') as KnpStatus
       ,dfd.INVOICE_NUMBER as CreatedByInvoiceNumber
       ,dfd.INVOICE_DATE as CreatedByInvoiceDate
       ,addl.ACT_INN as ActInn
       ,addl.ACT_KPP as ActKpp
       ,kdt.name as KnpDocType
       ,addl.KNP_DOC_AMOUNT as KnpDocAmount
       ,addl.DECISION_AMOUNT_DIFFERENCE as DecisionAmountDifference
       ,dfd.CLOSE_DATE as CloseDate 
       ,duc.COMMENT_TEXT as UserComment 
       ,dfd.INVOICE_CHAPTER as WorkSideInvoiceChapter 
       ,dfd.CONTRACTOR_INVOICE_CHAPTER as ContractorInvoiceChapter
	   ,decode(dh.type_code, 0, 'Декларация', 'Журнал') as ContractorDocType 
       ,dfd.INN_REORGANIZED as TaxPayerReorgInn 
       ,dfd.KPP as TaxPayerKpp 
       ,dfd.CONTRACTOR_INN as OtherSideINN 
	   ,dfd.REQUEST_ID as RequestId 
FROM HIVE_DISCREPANCY_DECL_CACHE dfd 
JOIN DICT_DISCREPANCY_TYPE tp on tp.s_code = dfd.DISCREPANCY_TYPE
left join DECLARATION_HISTORY dh on dfd.CONTRACTOR_ZIP = dh.zip
LEFT JOIN DICT_DISCREPANCY_STATUS st on st.id = dfd.DISCREPANCY_STATUS
LEFT JOIN V$ACT_DISCREPANCY_DECL_LIST addl on dfd.DISCREPANCY_ID = addl.id 
LEFT JOIN DICT_DISCREPANCY_KNPDOC_TYPE kdt on kdt.id = addl.KNP_DOC_TYPE
LEFT JOIN DECLARATION_KNP_STATUS ds on ds.zip = dfd.CONTRACTOR_ZIP
LEFT JOIN DISCREPANCY_USER_COMMENTS duc on dfd.DISCREPANCY_ID = duc.DISCREPANCY_ID
LEFT JOIN TAX_PAYER taxp on dfd.CONTRACTOR_INN = taxp.INN and dfd.CONTRACTOR_KPP = taxp.KPP;
