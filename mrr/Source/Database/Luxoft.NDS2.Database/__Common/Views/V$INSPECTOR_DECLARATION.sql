﻿create or replace force view NDS2_MRR_USER.V$INSPECTOR_DECLARATION as
select 
  /*+ leading(mdi sc)
	use_nl(mdi) index(mdi)
	use_nl(sc) index(sc)
	*/
       mdi.join_k
       ,mdi.partition_id
       ,mdi.subpartition_id
       ,nvl(mdi.zip, 0)                        as ID
       ,nvl(mdi.zip, 0)                        as NDS2_DECL_CORR_ID
       ,nvl(mdi.zip, 0)                        as DECLARATION_VERSION_ID
       ,null                                   as RECORD_MARK
       ,mdi.type_code                          as DECL_TYPE_CODE
       ,mdi.type                               as DECL_TYPE
       ,mdi.CANCELLED
       ,mdi.HAS_CANCELLED_CORRECTION
       ,2                                      as PROCESSINGSTAGE
       ,2                                      as LOAD_MARK
       ,mdi.status_knp                         as STATUS_KNP
       ,decode(mdi.has_discrepancy_knp, 0, 'Нет', 'Есть') as STATUS
       ,1                                      as IS_ACTIVE
       ,mdi.SUR_CODE                           as SUR_CODE
       ,mdi.inn_declarant                      as INN
       ,mdi.inn_contractor
       ,mdi.KPP                                as KPP
       ,mdi.kpp_effective
       ,mdi.kpp_effective                      as KPP_EFFECTIVE_CONTRACTOR
       ,mdi.name_short                         as NAME
       ,mdi.reg_number                         as SEOD_DECL_ID
       ,mdi.is_large                           as CATEGORY
       ,mdi.period_effective
       ,mdi.period_code                        as TAX_PERIOD
       ,mdi.FISCAL_YEAR                        as FISCAL_YEAR
       ,mdi.FULL_TAX_PERIOD					           as FULL_TAX_PERIOD
       ,mdi.TAX_PERIOD_SORT_ORDER			         as TAX_PERIOD_SORT_ORDER
       ,mdi.submit_date                        as DECL_DATE
       ,mdi.submission_date                    as SUBMISSION_DATE
       ,mdi.correction_number_effective        as CORRECTION_NUMBER
       ,0                                      as CORRECTION_NUMBER_RANK
       ,decode(mdi.correction_processed, 1, 'Да', 'Нет') as CORRECTION_PROCESSED
       ,mdi.signatory_name                     as SUBSCRIBER_NAME       
       ,mdi.signatory_status                   as PRPODP                
       ,mdi.sign                               as DECL_SIGN             
       ,mdi.nds_total                          as COMPENSATION_AMNT     
       ,mdi.CH8_NDS                            as NDS_CHAPTER8
       ,mdi.CH9_NDS                            as NDS_CHAPTER9
       ,mdi.NDS_WEIGHT                         as NDS_WEIGHT
       ,mdi.nds_total                          as NDS_AMOUNT
       ,null                                   as CHAPTER8_MARK
       ,mdi.ch8_contractor_qty             as CHAPTER8_CONTRAGENT_CNT
       ,mdi.ch8_knp_gap_contractor_qty         as CHAPTER8_GAP_CONTRAGENT_CNT
       ,mdi.ch8_knp_gap_qty                    as CHAPTER8_GAP_CNT
       ,mdi.ch8_knp_gap_amt                    as CHAPTER8_GAP_AMNT
       ,mdi.ch8_knp_other_contractor_qty       as CHAPTER8_OTHER_CONTRAGENT_CNT
       ,mdi.ch8_knp_other_qty                  as CHAPTER8_OTHER_CNT
       ,mdi.ch8_knp_other_amt                  as CHAPTER8_OTHER_AMNT
       ,null                                   as CHAPTER9_MARK
       ,mdi.ch9_contractor_qty             as CHAPTER9_CONTRAGENT_CNT
       ,mdi.ch9_knp_gap_contractor_qty         as CHAPTER9_GAP_CONTRAGENT_CNT
       ,mdi.ch9_knp_gap_qty                    as CHAPTER9_GAP_CNT
       ,mdi.ch9_knp_gap_amt                    as CHAPTER9_GAP_AMNT
       ,mdi.ch9_knp_other_contractor_qty       as CHAPTER9_OTHER_CONTRAGENT_CNT
       ,mdi.ch9_knp_other_qty                  as CHAPTER9_OTHER_CNT
       ,mdi.ch9_knp_other_amt                  as CHAPTER9_OTHER_AMNT
       ,null                                   as CHAPTER10_MARK
       ,mdi.ch10_contractor_qty            as CHAPTER10_CONTRAGENT_CNT
       ,mdi.ch10_knp_gap_contractor_qty        as CHAPTER10_GAP_CONTRAGENT_CNT
       ,mdi.ch10_knp_gap_qty                   as CHAPTER10_GAP_CNT
       ,mdi.ch10_knp_gap_amt                   as CHAPTER10_GAP_AMNT
       ,mdi.ch10_knp_other_contractor_qty      as CHAPTER10_OTHER_CONTRAGENT_CNT
       ,mdi.ch10_knp_other_qty                 as CHAPTER10_OTHER_CNT
       ,mdi.ch10_knp_other_amt                 as CHAPTER10_OTHER_AMNT
       ,null                                   as CHAPTER11_MARK
       ,mdi.ch11_contractor_qty            as CHAPTER11_CONTRAGENT_CNT
       ,mdi.ch11_knp_gap_contractor_qty        as CHAPTER11_GAP_CONTRAGENT_CNT
       ,mdi.ch11_knp_gap_qty                   as CHAPTER11_GAP_CNT
       ,mdi.ch11_knp_gap_amt                   as CHAPTER11_GAP_AMNT
       ,mdi.ch11_knp_other_contractor_qty      as CHAPTER11_OTHER_CONTRAGENT_CNT
       ,mdi.ch11_knp_other_qty                 as CHAPTER11_OTHER_CNT
       ,mdi.ch11_knp_other_amt                 as CHAPTER11_OTHER_AMNT
       ,null                                   as CHAPTER12_MARK
       ,mdi.ch12_contractor_qty            as CHAPTER12_CONTRAGENT_CNT
       ,mdi.ch12_knp_gap_contractor_qty        as CHAPTER12_GAP_CONTRAGENT_CNT
       ,mdi.ch12_knp_gap_qty                   as CHAPTER12_GAP_CNT
       ,mdi.ch12_knp_gap_amt                   as CHAPTER12_GAP_AMNT
       ,mdi.ch12_knp_other_contractor_qty      as CHAPTER12_OTHER_CONTRAGENT_CNT
       ,mdi.ch12_knp_other_qty                 as CHAPTER12_OTHER_CNT
       ,mdi.ch12_knp_other_amt                 as CHAPTER12_OTHER_AMNT
       ,mdi.discrep_gap_amnt                   as DISCREP_GAP_AMNT
       ,mdi.discrep_gap_qty                    as DISCREP_GAP_CNT
       ,(mdi.discrepancy_amt-mdi.discrep_gap_amnt) as DISCREP_OTHER_AMNT
       ,(mdi.discrepancy_qty-mdi.discrep_gap_qty) as DISCREP_OTHER_CNT
       ,mdi.sono_code                          as SOUN_CODE
       ,mdi.INSPECTION_TITLE                   as SOUN_NAME
       ,mdi.REGION_CODE                        as REGION_CODE
       ,mdi.REGION_TITLE                       as REGION_NAME
       ,mdi.inspector						         as INSPECTOR
       ,mdi.inspector_sid			           as INSPECTOR_SID
       ,mdi.HAS_CHANGES
       ,mdi.HAS_AT
       ,mdi.SLIDE_AT_COUNT
       ,mdi.NO_TKS_COUNT
from NDS2_MRR_USER.DECLARATION_ACTIVE mdi;
