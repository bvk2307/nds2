﻿create or replace force view NDS2_MRR_USER.V$UL_BANK_ACCOUNT_BRIEF as
select
 t.INN,
 t.KPP,
 t.BIK,
 t.NAMEKO as NameBank,
 t.INNKO as INNBANK,
 t.KPPKO as KPPBANK,
 t.RNKO as BANKNUMBER,
 t.NFKO as FILIALNUMBER,
 t.NOMSCH as ACCOUNT,
 case t.PRVAL when  '0' then 'рубли' else 'валюта' end as ACCOUNTTYPE,
 t.DATECLOSESCH as CLOSEDATE,
 t.DATEOPENSCH as OPENDATE
from BSSCHET_UL t
left join VIDSCHET_EXCLUDE vs on vs.vidsch = t.vidsch
where vs.vidsch is null;
