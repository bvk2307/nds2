﻿create or replace force view nds2_mrr_user.v$explain_tks_receipt_document as
select
   explain_zip as ExplainZip
  ,invoice_row_key as InvoiceRowKey
  ,state
  ,doc_num as DocumentNumber
  ,doc_date as DocumentDate
from EXPLAIN_TKS_RECEIPT_DOCUMENT;

