﻿create or replace force view nds2_mrr_user.v$doc_create_dates as
select trunc(end_date) as create_date 
      ,claim_type as doc_type 
  from claim_journal 
  where end_date is not null
  group by trunc(end_date), claim_type 
  order by claim_type; 
