﻿create or replace force view NDS2_MRR_USER.V$EXPLAIN_TKS_INVOICE_10 as
select 
   explain_zip as ExplainZip
  ,row_key as RowKey
  ,status 
  ,chapter
  ,ordinal_number as OrdinalNumber
  ,operation_codes_bit as OperationCodesBit
  ,operation_codes_bit_order as OperationCodesBitOrder
  ,operation_codes_bit_new as NewOperationCodesBit
  ,invoice_num as InvoiceNumber
  ,invoice_num_new as NewInvoiceNumber
  ,invoice_date as InvoiceDate
  ,invoice_date_new as NewInvoiceDate
  ,change_num as ChangeNumber
  ,change_num_new as NewChangeNumber
  ,change_date as ChangeDate
  ,change_date_new as NewChangeDate
  ,correction_num as CorrectionNumber
  ,correction_num_new as NewCorrectionNumber
  ,correction_date as CorrectionDate
  ,correction_date_new as NewCorrectionDate
  ,change_correction_num as ChangeCorrectionNumber
  ,change_correction_num_new as NewChangeCorrectionNumber
  ,change_correction_date as ChangeCorrectionDate
  ,change_correction_date_new as NewChangeCorrectionDate
  ,okv_code as OkvCode
  ,okv_code_new as NewOkvCode
  ,contractor_inn_first as BuyerInn
  ,contractor_inn_new as NewBuyerInn
  ,contractor_kpp_first as BuyerKpp
  ,contractor_kpp_new as NewBuyerKpp
  ,broker_inn as SellerInn
  ,broker_kpp as SellerKpp
  ,broker_inn_new as NewSellerInn
  ,broker_kpp_new as NewSellerKpp
  ,agency_invoice_num as AgencyInvoiceNumber
  ,agency_invoice_num_new as NewAgencyInvoiceNumber
  ,agency_invoice_date as AgencyInvoiceDate
  ,agency_invoice_date_new as NewAgencyInvoiceDate
  ,amt as Amount
  ,amt_new as NewAmount
from EXPLAIN_TKS_INVOICE
where chapter = 10;
  
