﻿create or replace force view nds2_mrr_user.v$knp_result_report_agregate as
select
   t.report_id as ReportId
  ,t.region_code as RegionCode
  ,t.sono_code as SonoCode
  ,t.acts_qty as ActQuantity
  ,t.act_discrepancy_nds_amt as ActNdsDiscrepancyAmount
  ,t.act_discrepancy_nds_qty as ActNdsDiscrepancyQuantity
  ,t.act_discrepancy_gap_amt as ActGapAmount
  ,t.act_discrepancy_gap_qty as ActGapQuantity
  ,t.decisions_qty as DecisionQuantity
  ,t.decision_discrepancy_nds_amt as DecisionNdsDiscrepancyAmount
  ,t.decision_discrepancy_nds_qty as DecisionNdsDiscrepancyQuantity
  ,t.decision_discrepancy_gap_amt as DecisionGapAmount
  ,t.decision_discrepancy_gap_qty as DecisionGapQuantity
from KNP_RESULT_REPORT_AGGREGATE t;
