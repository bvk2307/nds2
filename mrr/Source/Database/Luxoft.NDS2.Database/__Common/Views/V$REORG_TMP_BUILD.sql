﻿create or replace force view NDS2_MRR_USER.V$REORG_TMP_BUILD as
select * from (
select
  ask.innnp as inn_successor,
  ask.kppnp as kpp_successor,
  tp.name_short as name_successor,
  ask.kpp_effective as kpp_effective_successor,
  ask.innreorg as inn_reorganized,
  ask.kpp_effective as kpp_effective_reorganized,
  ask.insert_date as insert_date,
  ask.kodno as sono_code,
  row_number() over (partition by ask.innreorg, ask.kpp_effective order by ask.otchetgod desc, dtp.quarter desc, ask.insert_date desc) as rn
from ask_declandjrnl ask
join dict_tax_period dtp on dtp.code = ask.period
left join tax_payer tp on tp.inn = ask.innnp and tp.kpp_effective = ask.kpp_effective
where ask.prizn_akt_korr = 1 and ask.innreorg is not null
) t where t.rn = 1;

