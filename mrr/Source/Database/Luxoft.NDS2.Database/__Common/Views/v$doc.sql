﻿create or replace force view nds2_mrr_user.v$doc as
select 
  case when t.doc_type in (1, 2, 3, 5) then
    -- для АТ пришел ЭОД-2 (квитанция о принятии, номер и дата)
    -- для АИ пришел ЭОД-5 (квитанция) и ЭОД-6 (номер , дата)
    case when t.Seod_Accepted = 1 and t.EXTERNAL_DOC_NUM is not null 
         then TO_CHAR(t.EXTERNAL_DOC_NUM)
         else '*'||TO_CHAR(t.DOC_ID) end 
  else null end as NUM,
  case when t.doc_type in (1, 2, 3, 5) then 
    -- для АТ пришел ЭОД-2 (квитанция о принятии, номер и дата)
    -- для АИ пришел ЭОД-5 (квитанция) и ЭОД-6 (номер , дата)
    case when t.Seod_Accepted = 1 and t.EXTERNAL_DOC_DATE is not null 
         then t.EXTERNAL_DOC_DATE
         else null end
  else null end as DT,
  t.*
from DOC t;