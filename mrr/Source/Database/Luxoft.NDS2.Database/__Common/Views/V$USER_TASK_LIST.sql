﻿create or replace force view NDS2_MRR_USER.v$user_task_list as
select
  ut.task_id as id,
  ut.fiscal_year as FiscalYear, 
  /*Назначенный инспектор*/
  ut.assignee_sid as AssignedSid,
  ut.assignee_name as AssignedName,
  /*Статус и тип*/
  ut.status,
  uts.description as StatusName,
   ut.type_id as TaskType,
  utt.description as TaskTypeName,
  /*Даты*/
  ut.create_date as CreateDate,
  ut.end_date_planning as PlannedCompleteDate,
  case when ut.status = 2 then null else ut.close_date end as ActualCompleteDate,
  case
    when trunc(nvl(ut.close_date, sysdate)) > ut.end_date_planning then
      trunc(nvl(ut.close_date, sysdate)) 
	  - ut.end_date_planning 
	  - (select count(1) from redday where red_date between ut.end_date_planning and nvl(ut.close_date, sysdate))
    else 0
  end as DaysAfterExpire,
  /*Налогоплательщик*/
  tp.inn as TaxPayerInn,
  tp.kpp as TaxPayerKpp,
  tp.name_short as TaxPayerName,
  /*Сведения о НД или Журнале*/
  nd.zip as DocumentId,
  nd.type_code as DocumentTypeCode,
  nd.reg_number as DocumentNumber,
  nd.correction_number_effective as DocumentCorrectionNumber,
  /*документ - период*/
  nd.period_code as DocumentPeriodCode,
  ut.fiscal_year || ut.quarter as DocumentPeriodId,
  /*ТНО и регион*/
  nd.sono_code as SonoCode,
  sono.s_name as SonoName,
  ssrf.s_code as RegionCode,
  ssrf.s_name as RegionName,
  case 
    when ut.status = 1 then 4 --Выполнено
    when ut.status = 2 then 5 --Закрыто
    when 
       trunc(sysdate) <= ut.end_date_planning 
       and (ut.end_date_planning 
			- trunc(sysdate) 
			- (select count(1) from redday where red_date between trunc(sysdate) and ut.end_date_planning)
			) <= reg.days_to_notify 
		then 2	   --Истекает срок выполнения задания
    when ut.close_date is null and trunc(sysdate) > ut.end_date_planning then 1 --Задание просрочено
	else 3 --Новое
	end as CurrentProcessState
from user_task ut
inner join user_task_status uts on uts.id = ut.status
inner join user_task_type utt on utt.id = ut.type_id
inner join user_task_declaration_rel decl_rel on decl_rel.task_id = ut.task_id
inner join declaration_history nd on nd.zip = decl_rel.declaration_correction_id
inner join v$sono sono on sono.s_code = nd.sono_code
inner join v$ssrf ssrf on ssrf.s_code = nd.region_code
inner join user_task_tax_payer tp_rel on tp_rel.task_id = ut.task_id
inner join tax_payer tp on tp.inn = tp_rel.inn and tp.kpp_effective = tp_rel.effective_kpp
inner join user_task_regulation reg on reg.task_type_id = ut.type_id;
