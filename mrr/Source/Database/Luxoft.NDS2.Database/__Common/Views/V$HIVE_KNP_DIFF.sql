﻿CREATE OR REPLACE FORCE VIEW NDS2_MRR_USER.V$HIVE_KNP_DIFF (DOC_ID, DISCREPANCY_ID) AS
SELECT dc.doc_id
      ,dd.discrepancy_id
  FROM DOC dc
  INNER JOIN DOC_DISCREPANCY dd 
    ON dc.doc_id = dd.doc_id 
	AND dc.doc_type = 1
	AND dc.knp_sync_status = 1;