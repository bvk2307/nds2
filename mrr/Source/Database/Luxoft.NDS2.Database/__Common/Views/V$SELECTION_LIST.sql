﻿create or replace force view NDS2_MRR_USER.V$SELECTION_LIST
as
select
  ag.id,
  ag.name,
  ag.type as TypeCode,
  decode(ag.type, 0, t.s_name, 1, t.s_name, s.name) as TypeName,
  trunc(ag.created_at) as CreatedAt,
  trunc(ag.last_status_changed_at) as LastStatusChangedAt,
  ag.status_id as StatusId,
  ag.initial_declaration_qty as InitialDeclarationQuantity,
  ag.initial_discrepancy_qty as InitialDiscrepancyQuantity,
  ag.initial_discrepancy_amt as InitialDiscrepancyAmount,
  ag.last_edited_by as LastEditedBySid,
  ag.approved_by as ApprovedBySid,
  us.name as LastEditedBy,
  us1.name as ApprovedBy,
  sel.template_id as TemplateId
from 
  NDS2_MRR_USER.SELECTION_AGGREGATE ag
  join NDS2_MRR_USER.DICT_SELECTION_TYPE t on ag.type = t.s_code
  left join NDS2_MRR_USER.SELECTION sel on sel.id = ag.id
  left join NDS2_MRR_USER.mrr_user us on us.sid = ag.last_edited_by
  left join NDS2_MRR_USER.mrr_user us1 on us1.sid = ag.approved_by
  left join NDS2_MRR_USER.SELECTION_AGGREGATE s on s.id = sel.template_id
;
