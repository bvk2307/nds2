﻿create or replace force view NDS2_MRR_USER.V$SONO_UFNS_DISTRICT_RELATION as
select
    reg.s_code                  as region_code,
    sono.s_code                 as sono_code,
    sono_ufns.s_code            as ufns_code,
    sono_ufns.s_name            as ufns_name,
    sono.s_name                 as sono_name,
    to_char(distr.district_id)  as district_code,
    distr.description as district_name
from (  select 
            substr(s_code, 1,2) as s_code, 
            s_name 
        from EXT_SONO 
        where s_code like '__00'
    ) reg
    inner join EXT_SONO sono 
        on reg.s_code = (case when sono.s_code = '9901' then '50' else substr(sono.s_code, 1,2) end)
    inner join EXT_SONO sono_ufns 
        on sono_ufns.s_code = (case when sono.s_code = '9901' then '50' else substr(sono.s_code, 1,2) end)||'00'
    inner join FEDERAL_DISTRICT_REGION distr_region 
        on distr_region.region_code = decode(sono.s_code, '9901', '50', substr(sono.s_code, 1,2))
    inner join FEDERAL_DISTRICT distr 
        on distr.district_id = distr_region.district_id;
