﻿create or replace force view NDS2_MRR_USER.V$DECL_ANNULMENT_STATUS as
select 
    s.ID
  , s.NAME
  , s.DESCRIPTION
from 
nds2_mrr_user.DECLARATION_ANNULMENT_STATUS s;

grant select on NDS2_MRR_USER.V$DECL_ANNULMENT_STATUS to NDS2_MC;