﻿create or replace force view NDS2_MRR_USER.V$EFF_MON_BUYER_DECL_SRC as
with DECLARATION as
(
  select
    distinct
     da.zip
     ,da.kodno as sono_code
     ,f$tax_period_to_quarter(da.period) as quarter
     ,to_number(da.otchetgod) as fiscal_year
     ,da.innnp as inn
     ,da.period
  from v$askdekl da
  where da.priznaktkorr = 1
)
select
  d.zip as id,
  d.inn,
  d.period,
  d.quarter,
  d.fiscal_year,
  d.sono_code,
  trunc(sysdate) as calc_date,
  sum(/*Сумма открытых расхождений(разрыв и НДС) всего*/
    case
      when sov.type in (1,4)
        and sov.status <> 2
      then nvl(sov.amnt, 0) else 0 end) as buyer_dis_opn_amnt,
  sum(/*Сумма открытых расхождений(разрыв и НДС) КНП*/
    case
      when knp.discrepancy_id is not null
        and sov.type in (1,4)
        and sov.status <> 2
      then nvl(sov.amnt, 0) else 0 end) as buyer_dis_opn_knp_amnt,
  sum(/*Кол-во открытых расхождений(разрыв и НДС) КНП*/
    case
      when knp.discrepancy_id is not null
        and sov.type in (1,4)
        and sov.status <> 2
      then 1 else 0 end) as buyer_dis_opn_knp_cnt,
  sum(/*Сумма открытых расхождений вида разрыв всего*/
    case
      when sov.type = 1
        and sov.status <> 2
      then nvl(sov.amnt, 0) else 0 end) as buyer_dis_opn_gap_amnt,
  sum(/*Сумма открытых расхождений КНП вида разрыв*/
    case
      when knp.discrepancy_id is not null
        and sov.type = 1
        and sov.status <> 2
      then nvl(sov.amnt, 0) else 0 end) as buyer_dis_opn_gap_knp_amnt,
  sum(/*Кол-во открытых расхождений КНП вида разрыв*/
    case
      when knp.discrepancy_id is not null
        and sov.type = 1
        and sov.status <> 2
      then 1 else 0 end) as buyer_dis_opn_gap_knp_cnt,
  sum(/*Сумма открытых расхождений вида НДС всего*/
    case
      when sov.type = 4
        and sov.status <> 2
      then nvl(sov.amnt, 0) else 0 end) as buyer_dis_opn_nds_amnt,
  sum(/*Сумма открытых расхождений КНП вида НДС*/
    case
      when knp.discrepancy_id is not null
        and sov.type = 4
        and sov.status <> 2
      then nvl(sov.amnt, 0) else 0 end) as buyer_dis_opn_nds_knp_amnt,
  sum(/*Кол-во открытых расхождений КНП вида НДС*/
    case
      when knp.discrepancy_id is not null
        and sov.type = 4
        and sov.status <> 2
      then 1 else 0 end) as buyer_dis_opn_nds_knp_cnt,
  sum(/*Сумма открытых и закрытых расхождений КНП*/
    case
      when knp.discrepancy_id is not null
        and sov.type in (1,4)
      then nvl(sov.amnt, 0) else 0 end) as buyer_dis_all_knp_amnt,
  sum(/*Сумма закрытых расхождений*/
    case
      when sov.status = 2
      then nvl(sov.amnt, 0) else 0 end) as buyer_dis_cls_amnt,
  sum(/*Сумма закрытых расхождений КНП*/
    case
      when knp.discrepancy_id is not null
        and sov.type in (1,4)
        and sov.status = 2
      then nvl(sov.amnt, 0) else 0 end) as buyer_dis_cls_knp_amnt,
  sum(/*Кол-во закрытых расхождений КНП*/
    case
      when knp.discrepancy_id is not null
        and sov.type in (1,4)
        and sov.status = 2
      then 1 else 0 end) as buyer_dis_cls_knp_cnt,
  sum(/*Сумма закрытых расхождений разрыв*/
    case
      when sov.type = 1
        and sov.status = 2
      then nvl(sov.amnt, 0) else 0 end) as buyer_dis_cls_gap_amnt,
  sum(/*Сумма закрытых расхождений КНП разрыв*/
    case
      when knp.discrepancy_id is not null
        and sov.type = 1
        and sov.status = 2
      then nvl(sov.amnt, 0) else 0 end) as buyer_dis_cls_gap_knp_amnt,
  sum(/*Кол-во закрытых расхождений КНП разрыв*/
    case
      when knp.discrepancy_id is not null
        and sov.type = 1
        and sov.status = 2
      then 1 else 0 end) as buyer_dis_cls_gap_knp_cnt,
  sum(/*Сумма закрытых расхождений НДС*/
    case
      when sov.type = 4
        and sov.status = 2
      then nvl(sov.amnt, 0) else 0 end) as buyer_dis_cls_nds_amnt,
  sum(/*Кол-во закрытых расхождений КНП НДС*/
    case
      when knp.discrepancy_id is not null
        and sov.type = 4
        and sov.status = 2
      then 1 else 0 end)  as buyer_dis_cls_nds_knp_cnt,
  sum(/*Сумма закрытых расхождений КНП НДС*/
    case
      when knp.discrepancy_id is not null
        and sov.type = 4
        and sov.status = 2
      then nvl(sov.amnt, 0) else 0 end) as buyer_dis_cls_nds_knp_amnt
from sov_discrepancy sov
join DECLARATION d on d.zip = sov.buyer_zip
left join (
    select
      distinct dd.discrepancy_id
    from doc_discrepancy dd
	inner join doc d on d.doc_id = dd.doc_id
	where d.status not in (1, 3, 11)
    ) knp on knp.discrepancy_id = sov.id
group by
d.zip,
d.inn,
d.period,
d.quarter,
d.fiscal_year,
d.sono_code;
