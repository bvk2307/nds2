﻿create or replace force view NDS2_MRR_USER.V$INVOICE_CLAIM_LIST as
select
  trunc(t.create_date) as CreateDate,
  t.status as StatusCode,
  case 
  when (ex.excluded is null and nvl(t.tax_monitoring, 0) = 1) or ex.excluded = 1 then 1 else null end as Excluded,
  case when nvl(t.tax_monitoring, 0) = 1 then 1 else null end as TaxMonitoring,
  coalesce(ex.lastuser, case when nvl(t.tax_monitoring, 0) = 1 then 'Автоматическое' else null end) as LastUser,
  t.doc_id as DocId,
  r_reg.s_code  as RegionCode,
  r_reg.s_name  as RegionName,
  r_sono.s_code as SonoCode,
  r_sono.s_name as SonoName,
  r_tp.inn        as Inn,
  r_tp.kpp        as Kpp,
  r_tp.name_full  as Name,
  t.buyer_total_discrepancy_qty  as BuyerDiscrepancyCount,
  t.buyer_total_discrepancy_amt as BuyerDiscrepancyAmount,
  t.seller_total_discrepancy_qty  as SellerDiscrepancyCount,
  t.seller_total_discrepancy_amt as SellerDiscrepancyAmount,
  dh.inn_reorganized                                                   as InnReorg,
  dtp.description||' '||dh.fiscal_year                                 as TaxPeriod,
  dh.fiscal_year||dh.period_code                                       as TaxPeriodCode,
  dh.fiscal_year||dtp.sort_order                                       as TaxPeriodSort,
  dh.correction_number                                                 as CorrectionNumber,
  dh.submission_date                                                   as CorrectionDate,
  dh.sign                                                              as DeclSignCode,
  decode(dh.sign, 1, 'К уплате', 2, 'К возмещению', 3, 'Нулевая', '')  as DeclarationSign,
  dh.nds_total                                                         as NdsAmount,
  dsh_fault.FaultFormingDate,
  dsh_ready.ReadyToSendDate,
  t.seod_send_date                      as SentToSeodDate,
  t.fault_send_date                     as FaultSendToSeodDate,
  t.seod_accept_date                    as ReceivedBySeodDate,
  t.external_doc_num                    as SeodRegNumber,
  t.external_doc_date                   as SeodDocDate,
  t.tax_payer_send_date                 as SendToTaxpayerDate,
  t.tax_payer_delivery_date             as TaxpayerReceiveDate,
  t.tax_payer_delivery_method           as DeliveryType,
  t.close_date                          as CloseDate,
  t.close_reason                        as CloseTypeId,
  dcr.description                       as CloseType
from doc t
left join doc_exclude_data ex on ex.DocId = t.doc_id
join declaration_history dh on dh.zip = t.zip
join ext_sono r_sono on r_sono.s_code = t.sono_code
join ext_ssrf r_reg on r_reg.s_code = substr(t.sono_code, 1, 2)
join tax_payer r_tp on r_tp.inn = t.inn and r_tp.kpp_effective = t.kpp_effective
join dict_tax_period dtp on dtp.code = dh.period_code
left join r$doc_close_reason dcr on dcr.id = t.close_reason and dcr.doc_type = 1
left join 
  ( 
    select doc_id, max(status_date) as FaultFormingDate from doc_status_history where status = 3 group by doc_id 
  ) dsh_fault 
  on t.doc_id = dsh_fault.doc_id
left join 
  ( 
    select doc_id, max(status_date) as ReadyToSendDate from doc_status_history where status = 1 group by doc_id 
  ) dsh_ready 
  on t.doc_id = dsh_ready.doc_id
where t.doc_type = 1;