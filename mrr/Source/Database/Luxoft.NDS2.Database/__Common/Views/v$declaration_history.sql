﻿CREATE OR REPLACE FORCE VIEW nds2_mrr_user.v$declaration_history(
ASK_ID, ZIP, REG_NUMBER, SUBMISSION_DATE, INN_CONTRACTOR, 
INN_DECLARANT, INN_REORGANIZED, KPP, KPP_REORGANIZED, KPP_CONTRACTOR, 
KPP_EFFECTIVE, FISCAL_YEAR, PERIOD_CODE, PERIOD_EFFECTIVE, IS_ACTIVE, 
TYPE_CODE, SUR_CODE, SIGN, NDS_TOTAL, CORRECTION_NUMBER, 
CORRECTION_NUMBER_RANK, CORRECTION_NUMBER_EFFECTIVE, SONO_CODE_SUBMITED, SONO_CODE, REGION_CODE, 
SUBMIT_DATE, IS_LARGE, INSERT_DATE
) as
select ASK_ID, ZIP, REG_NUMBER, SUBMISSION_DATE, INN_CONTRACTOR, 
INN_DECLARANT, INN_REORGANIZED, KPP, KPP_REORGANIZED, KPP_CONTRACTOR, 
KPP_EFFECTIVE, FISCAL_YEAR, PERIOD_CODE, PERIOD_EFFECTIVE, IS_ACTIVE, 
TYPE_CODE, SUR_CODE, SIGN, NDS_TOTAL, CORRECTION_NUMBER, 
CORRECTION_NUMBER_RANK, CORRECTION_NUMBER_EFFECTIVE, SONO_CODE_SUBMITED, SONO_CODE, REGION_CODE, 
SUBMIT_DATE, IS_LARGE, INSERT_DATE
from nds2_mrr_user.declaration_history;  

grant select on nds2_mrr_user.v$declaration_history to nds2_seod;