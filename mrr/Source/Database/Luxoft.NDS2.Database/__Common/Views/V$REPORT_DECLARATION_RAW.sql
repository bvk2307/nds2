﻿create or replace force view NDS2_MRR_USER.V$REPORT_DECLARATION_RAW as
select
    r.id,
    r.submit_date,
    (case when r.region_code = '99' and r.soun_code = '9901' then '50' else r.region_code end) as region_code,
    r.soun_code,
    r.fiscal_year,
    r.tax_period,
    r.decl_count_total,
    r.decl_count_null,
    r.decl_count_compensation,
    r.decl_count_payment,
    r.decl_count_not_submit,
    r.decl_count_revised_total,
    r.taxbase_sum_compensation,
    r.taxbase_sum_payment,
    r.ndscalculated_sum_compensation,
    r.ndscalculated_sum_payment,
    r.ndsdeduction_sum_compensation,
    r.ndsdeduction_sum_payment,
    r.nds_sum_compensation,
    r.nds_sum_payment
from NDS2_MRR_USER.REPORT_DECLARATION_RAW r;
