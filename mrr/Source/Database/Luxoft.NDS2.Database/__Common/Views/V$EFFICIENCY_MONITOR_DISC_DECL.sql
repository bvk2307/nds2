﻿CREATE OR REPLACE FORCE VIEW NDS2_MRR_USER.V$EFFICIENCY_MONITOR_DISC_DECL (SONO_CODE, QTR, FISCAL_YEAR, CNT) AS 
with q as (
	select /*+MATERIALIZE*/
		sed.nds2_id as nd_id,
		sed.fiscal_year,
		f$tax_period_to_quarter (sed.tax_period) qtr,
		sed.sono_code,
		row_number() over (	partition by sed.nds2_id 
							order by	sed.correction_number desc, 
										sed.insert_date desc, 
										q.create_date
							) as rnk
	from ask_invoice_decl_queue q
	join nds2_seod.seod_declaration sed 
		on sed.decl_reg_num = q.seod_decl_id 
		and sed.sono_code = q.soun_code
	where 
		sed.type = 0 
		and q.for_stage = 2
)
select
	sono_code,
	qtr,
	fiscal_year,
	count(nd_id) as cnt
from q
where rnk = 1
group by sono_code,	qtr, fiscal_year
;
