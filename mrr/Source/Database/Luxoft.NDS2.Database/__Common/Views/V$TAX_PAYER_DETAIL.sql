﻿create or replace force view NDS2_MRR_USER.V$TAX_PAYER_DETAIL as
select
 0 as ID,
 tp.inn,
 tp.kpp,
 tp.kpp_effective,
 tp.inn_successor,
 tp.kpp_successor,
 tp.kpp_effective_successor,
 tp.name_successor,
 tp.name_full,
 tp.sono_code,
 tp.sur_code,
 tp.ogrn,
 tp.address,
 tp.date_on,
 tp.date_off,
 sono.s_name as sono_name,
 tp.ust_capital,
 tp.date_reg,
 tp.okved_code,
 okv.s_name as okved_name
from tax_payer tp
left join ext_sono sono on sono.s_code = tp.sono_code
left outer join V$OKVED okv on okv.s_code = tp.okved_code;
