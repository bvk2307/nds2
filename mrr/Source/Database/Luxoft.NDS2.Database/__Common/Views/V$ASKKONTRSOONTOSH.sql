﻿CREATE OR REPLACE FORCE VIEW NDS2_MRR_USER.V$ASKKONTRSOONTOSH
(
   ID,
   IDDEKL,
   KODKS,
   DATARASCHETA,
   LEVYACHAST,
   PRAVYACHAST,
   VYPOLN,
   DELETEDATE
)
AS
   SELECT "Ид" AS ID,
          "ИдДекл" AS IdDekl,
          "КодКС" AS KodKs,
          "ДатаРасчета" AS DataRascheta,
          "ЛеваяЧасть" AS LevyaChast,
          "ПраваяЧасть" AS PravyaChast,
          "Выполн" AS Vypoln,
		  "ДатаУдаления" AS DeleteDate
     FROM NDS2_MC."ASKКонтрСоотн";
