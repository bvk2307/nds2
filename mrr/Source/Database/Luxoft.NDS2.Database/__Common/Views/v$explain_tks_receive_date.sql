﻿create or replace force view nds2_mrr_user.v$explain_tks_receive_date as
select
   explain_zip as ExplainZip
  ,invoice_row_key as InvoiceRowKey
  ,state
  ,receive_date as ReceiveDate
from EXPLAIN_TKS_RECEIVE_DATE;

