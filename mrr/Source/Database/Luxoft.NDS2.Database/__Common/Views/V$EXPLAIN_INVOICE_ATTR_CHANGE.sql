﻿create or replace force view NDS2_MRR_USER.V$EXPLAIN_INVOICE_ATTR_CHANGE
as
select 
        sic.explain_id
       ,sic.field_name
       ,sic.field_value
       ,sic.invoice_original_id
       ,se.incoming_date 
from 
       STAGE_INVOICE_CORRECTED sic
       join nds2_seod.SEOD_EXPLAIN_REPLY se 
            on se.explain_id = sic.explain_id;
