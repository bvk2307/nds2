﻿create or replace force view NDS2_MRR_USER.V$I_MC_AT_REPLY as
select
sd.inn,
sd.kpp,
sd.fiscal_year as year,
sd.tax_period as period,
sd.correction_number,
ser.filenameoutput as file_name,
sd.sono_code as sono_code
from NDS2_SEOD.seod_explain_reply ser
inner join doc d on d.doc_id = ser.doc_id
inner join  NDS2_SEOD.seod_declaration sd on sd.decl_reg_num = d.ref_entity_id and sd.sono_code = d.sono_code;
