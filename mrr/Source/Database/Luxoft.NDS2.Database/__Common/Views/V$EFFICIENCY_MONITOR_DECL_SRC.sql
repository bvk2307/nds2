﻿create or replace force view NDS2_MRR_USER.V$EFFICIENCY_MONITOR_DECL_SRC as
select vrdrb.fiscal_year 
      ,f$tax_period_to_quarter(vrdrb.tax_period) as quarter 
      ,trunc(sysdate) as calc_date 
      ,vrdrb.soun_code as sono_code 
      /*НДС вычет*/
      ,sum(vrdrb.ndsdeduction_sum_payment + vrdrb.ndsdeduction_sum_compensation) as deduction_amnt 
      /*НДС уплата*/
      ,sum(vrdrb.ndscalculated_sum_payment + vrdrb.ndscalculated_sum_compensation) as calculation_amnt 
      /*Кол-во всех НД*/
      ,sum(vrdrb.decl_count_total) as all_decl_cnt 
      /*Сумма НДС всех НД*/
      ,sum(vrdrb.nds_sum_payment + vrdrb.nds_sum_compensation) as all_decl_nds_sum 
      /*Кол-во с операциями*/
      ,sum(vrdrb.decl_count_payment + vrdrb.decl_count_compensation) as operation_decl_cnt 
      /*Доля деклараций с открытыми КНП расхождениями по стороне покупателя в декларациях с операциями (по количеству)*/
      ,sum(embds.opn_knp_dis_buyer_decl_cnt) as opn_knp_dis_buyer_decl_cnt  
      /*Кол-во к уплате*/
      ,sum(vrdrb.decl_count_payment) as pay_decl_cnt 
      /*Кол-во к возмещению*/
      ,sum(vrdrb.decl_count_compensation) as compensate_decl_cnt 
      /*Кол-во нулевых*/
      ,sum(vrdrb.decl_count_null) as zero_decl_cnt 
      /*Сумма НДС к уплате*/
      ,sum(vrdrb.nds_sum_payment) as pay_decl_nds_sum 
      /*Сумма НДС к возмещению*/
      ,sum(vrdrb.nds_sum_compensation) as compensate_decl_nds_sum 
  from NDS2_MRR_USER.V$REPORT_DECL_RAW_BUILD vrdrb 
  left outer join 
    ( 
      select fiscal_year 
            ,quarter 
            ,period
            ,sono_code 
            ,sum(case when buyer_dis_opn_knp_cnt > 0 then 1 else 0 end) as opn_knp_dis_buyer_decl_cnt
        from NDS2_MRR_USER.EFF_MON_BUYER_DECL_STG 
        group by fiscal_year, quarter, period, sono_code 
    ) embds 
    on vrdrb.fiscal_year = embds.fiscal_year 
    and vrdrb.tax_period = embds.period 
    and vrdrb.soun_code = embds.sono_code 
  group by vrdrb.fiscal_year, f$tax_period_to_quarter(vrdrb.tax_period), vrdrb.soun_code 
  order by vrdrb.fiscal_year, f$tax_period_to_quarter(vrdrb.tax_period), vrdrb.soun_code;