﻿create or replace force view NDS2_MRR_USER.V$WHITE_LISTS
as
select 
  ID, 
  NAME,
  LIST_TYPE as ListType,
  CREATED_BY as CreatedBy,
  CREATE_DATE as CreatedDate,
  IS_ACTIVE as IsActive
  from TAX_PAYER_RESTRICTION where LIST_TYPE = 1;
