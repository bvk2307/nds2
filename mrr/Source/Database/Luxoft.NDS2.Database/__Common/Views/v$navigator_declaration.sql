﻿create or replace force view NDS2_MRR_USER.v$navigator_declaration as
select
       ask.innnp,
       ask.kppnp,
       sur.sign_code as sur_code,
       sov.ch8_deals_amnt ch8_deals_amnt_total,
       sov.ch9_deals_amnt ch9_deals_amnt_total,
       sov.ch8_nds,
       sov.ch9_nds,
       100 * ask.otchetgod + ask.period as Period,
       decode((row_number() over (partition by ask.PERIOD, ask.OTCHETGOD, ask.INNNP order by ask.zip desc)),1,1,0) as is_active
from
       DECLARATION_JOURNAL_SUMMARY sov
       join V$ASK_DECLANDJRNL ask on ask.zip = sov.version_id
       left join ext_sur sur on sur.inn = ask.innnp and sur.kpp_effective = ask.KPP_EFFECTIVE
                            and sur.fiscal_year = ask.otchetgod and sur.fiscal_period = ask.period;
