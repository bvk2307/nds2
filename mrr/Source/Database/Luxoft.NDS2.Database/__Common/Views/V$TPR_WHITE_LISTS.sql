﻿create or replace force view NDS2_MRR_USER.V$TPR_WHITE_LISTS
as
select 
id, name 
from TAX_PAYER_RESTRICTION 
where LIST_TYPE = 1 and IS_ACTIVE = 1;
