﻿create or replace force view NDS2_MRR_USER.V$EXPLAIN_TKS_INVOICE_12 as
select 
   explain_zip as ExplainZip
  ,row_key as RowKey
  ,status 
  ,chapter
  ,invoice_num as InvoiceNumber
  ,invoice_num_new as NewInvoiceNumber
  ,invoice_date as InvoiceDate
  ,invoice_date_new as NewInvoiceDate
  ,okv_code as OkvCode
  ,okv_code_new as NewOkvCode
  ,contractor_inn_first as BuyerInn
  ,contractor_inn_new as NewBuyerInn
  ,contractor_kpp_first as BuyerKpp
  ,contractor_kpp_new as NewBuyerKpp
  ,amt as Amount
  ,amt_new as NewAmount
  ,amt2 as AmountNoNds
  ,amt2_new as NewAmountNoNds
from EXPLAIN_TKS_INVOICE
where chapter = 12;
