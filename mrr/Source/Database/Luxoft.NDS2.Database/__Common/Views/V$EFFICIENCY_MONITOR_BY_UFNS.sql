﻿create or replace force view NDS2_MRR_USER.v$efficiency_monitor_by_ufns as
with district_sono as
(
select
    reg.s_code as region_code,
    sono.s_code as sono_code,
    sono_ufns.s_code as ufns_code,
    sono_ufns.s_name as ufns_name,
    reg.s_name as region_name,
    sono.s_name as sono_name,
    distr.district_id as district_code,
    distr.description as district_name
from
  (select substr(s_code, 1,2) as s_code, s_name from EXT_SONO where s_code like '__00') reg
inner join NDS2_MRR_USER.EXT_SONO sono on reg.s_code = (case when sono.s_code = '9901' then '50' else substr(sono.s_code, 1,2) end)
inner join NDS2_MRR_USER.EXT_SONO sono_ufns on sono_ufns.s_code = (case when sono.s_code = '9901' then '50' else substr(sono.s_code, 1,2) end)||'00'
inner join FEDERAL_DISTRICT_REGION distr_region on distr_region.region_code = decode(sono.s_code, '9901', '50', substr(sono.s_code, 1,2))
inner join FEDERAL_DISTRICT distr on distr.district_id = distr_region.district_id
),
rating_query as
(
select
      district.district_code as DISTRICT_CODE
    , district.ufns_code as UFNS_CODE
    , district.region_name as UFNS_NAME
    , CALC_DATE as CALCULATION_DATE
    , FISCAL_YEAR
    , PERIOD as QTR
    , sum(DEDUCTION_AMNT) as DEDUCTION_AMNT
    , sum(CALCULATION_AMNT) as CALCULATION_AMNT
    /*Показатели*/
  /* Доля закрытых расхождений КНП в расхождениях КНП всего (по сумме) */
  ,nvl((sum(seller_dis_cls_knp_amnt) + sum(buyer_dis_cls_knp_amnt))/  nullif((sum(buyer_dis_opn_knp_amnt) + sum(seller_dis_opn_knp_amnt) + sum(buyer_dis_cls_knp_amnt) + sum(seller_dis_cls_knp_amnt)), 0), 0) as eff_idx_2_1
  /* Доля закрытых расхождений КНП в расхождениях КНП всего (по количеству) */
  ,nvl((sum(seller_dis_cls_knp_cnt) + sum(buyer_dis_cls_knp_cnt)) / nullif( ( sum(buyer_dis_opn_knp_cnt) + sum(seller_dis_opn_knp_cnt) + sum(buyer_dis_cls_knp_cnt) + sum(seller_dis_cls_knp_cnt) ), 0), 0) as eff_idx_2_2
  /* Доля деклараций с расхождениями в декларациях с операциями (по количеству)*/
  ,nvl( sum(decl_opn_knp_dis_buyer_cnt) / nullif( sum(decl_operation_cnt), 0), 0) as eff_idx_2_3
  /* Доля расхождений в сумме вычетов по НДС(КНП)*/
  ,nvl( (sum(buyer_dis_opn_knp_amnt) / nullif(sum(deduction_amnt), 0)), 0 ) as eff_idx_2_4_knp
  /* Доля расхождений в сумме вычетов по НДС(Все)*/
  ,nvl( (sum(buyer_dis_opn_amnt) / nullif(sum(deduction_amnt), 0)), 0 ) as eff_idx_2_4_all
    /*Покупатель*/
  /* Общие сведения о расхождениях */
  /*Все*/
  , sum(BUYER_DIS_OPN_KNP_AMNT) + sum(BUYER_DIS_CLS_KNP_AMNT) + sum(SELLER_DIS_OPN_KNP_AMNT) + sum(SELLER_DIS_CLS_KNP_AMNT) as DIS_TOTAL_AMNT
  , sum(BUYER_DIS_OPN_KNP_CNT) + sum(BUYER_DIS_CLS_KNP_CNT) + sum(SELLER_DIS_OPN_KNP_CNT) + sum(SELLER_DIS_CLS_KNP_CNT) as DIS_TOTAL_CNT
  /*Разрыв*/
  , sum(BUYER_DIS_OPN_GAP_KNP_AMNT) + sum(BUYER_DIS_CLS_GAP_KNP_AMNT) + sum(SELLER_DIS_OPN_GAP_KNP_AMNT) + sum(SELLER_DIS_CLS_GAP_KNP_AMNT) as DIS_GAP_TOTAL_AMNT
  , sum(BUYER_DIS_OPN_GAP_KNP_CNT) + sum(BUYER_DIS_CLS_GAP_KNP_CNT) + sum(SELLER_DIS_OPN_GAP_KNP_CNT) + sum(SELLER_DIS_CLS_GAP_KNP_CNT) as DIS_GAP_TOTAL_CNT
  /*НДС*/
  , sum(BUYER_DIS_OPN_NDS_KNP_AMNT) + sum(BUYER_DIS_CLS_NDS_KNP_AMNT) + sum(SELLER_DIS_OPN_NDS_KNP_AMNT) + sum(SELLER_DIS_CLS_NDS_KNP_AMNT) as DIS_NDS_TOTAL_AMNT
  , sum(BUYER_DIS_OPN_NDS_KNP_CNT) + sum(BUYER_DIS_CLS_NDS_KNP_CNT) + sum(SELLER_DIS_OPN_NDS_KNP_CNT) + sum(SELLER_DIS_CLS_NDS_KNP_CNT) as DIS_NDS_TOTAL_CNT
  /* Закрытые */
  /*Все*/
  , sum(BUYER_DIS_CLS_KNP_AMNT) + sum(SELLER_DIS_CLS_KNP_AMNT) as DIS_CLS_TOTAL_AMNT
  , sum(BUYER_DIS_CLS_KNP_CNT) + sum(SELLER_DIS_CLS_KNP_CNT) as DIS_CLS_TOTAL_CNT
  /*Разрыв*/
  , sum(BUYER_DIS_CLS_GAP_KNP_AMNT) + sum(SELLER_DIS_CLS_GAP_KNP_AMNT) as DIS_CLS_GAP_TOTAL_AMNT
  , sum(BUYER_DIS_CLS_GAP_KNP_CNT) + sum(SELLER_DIS_CLS_GAP_KNP_CNT) as DIS_CLS_GAP_TOTAL_CNT
  /*НДС*/
  , sum(BUYER_DIS_CLS_NDS_KNP_AMNT) + sum(SELLER_DIS_CLS_NDS_KNP_AMNT) as DIS_CLS_NDS_TOTAL_AMNT
  , sum(BUYER_DIS_CLS_NDS_KNP_CNT) + sum(SELLER_DIS_CLS_NDS_KNP_CNT) as DIS_CLS_NDS_TOTAL_CNT
  /* Открытые */
  /*Все*/
  , sum(BUYER_DIS_OPN_KNP_AMNT) + sum(SELLER_DIS_OPN_KNP_AMNT) as DIS_OPN_TOTAL_AMNT
  , sum(BUYER_DIS_OPN_KNP_CNT) +  sum(SELLER_DIS_OPN_KNP_CNT)  as DIS_OPN_TOTAL_CNT
  /*Разрыв*/
  , sum(BUYER_DIS_OPN_GAP_KNP_AMNT) + sum(SELLER_DIS_OPN_GAP_KNP_AMNT)  as DIS_OPN_GAP_TOTAL_AMNT
  , sum(BUYER_DIS_OPN_GAP_KNP_CNT) + sum(SELLER_DIS_OPN_GAP_KNP_CNT)  as DIS_OPN_GAP_TOTAL_CNT
  /*НДС*/
  , sum(BUYER_DIS_OPN_NDS_KNP_AMNT)  + sum(SELLER_DIS_OPN_NDS_KNP_AMNT) as DIS_OPN_NDS_TOTAL_AMNT
  , sum(BUYER_DIS_OPN_NDS_KNP_CNT)  + sum(SELLER_DIS_OPN_NDS_KNP_CNT) as DIS_OPN_NDS_TOTAL_CNT
  /*Декларация*/
  , sum(DECL_ALL_CNT) as DECL_ALL_CNT
  , sum(DECL_ALL_NDS_SUM) as DECL_ALL_NDS_SUM
  , sum(DECL_OPERATION_CNT) as DECL_OPERATION_CNT
  , sum(DECL_DISCREPANCY_CNT) as DECL_DISCREPANCY_CNT
  , sum(DECL_PAY_CNT) as DECL_PAY_CNT
  , sum(DECL_PAY_NDS_SUM) as DECL_PAY_NDS_SUM
  , sum(DECL_COMPENSATE_CNT) as DECL_COMPENSATE_CNT
  , sum(DECL_COMPENSATE_NDS_SUM) as DECL_COMPENSATE_NDS_SUM
  , sum(DECL_ZERO_CNT) as DECL_ZERO_CNT
  from
    NDS2_MRR_USER.EFFICIENCY_MONITOR_DATA mon
    inner join district_sono district on district.sono_code = mon.sono_code
  where district.region_code <> '99'
 group by district.district_code, district.ufns_code, district.region_name, fiscal_year, PERIOD, CALC_DATE
 ),
rating_mikn_query as
(
select
      district.district_code as DISTRICT_CODE
    , district.sono_code as UFNS_CODE
    , district.sono_name as UFNS_NAME
    , CALC_DATE as CALCULATION_DATE
    , FISCAL_YEAR
    , PERIOD as QTR
    , DEDUCTION_AMNT as DEDUCTION_AMNT
    , CALCULATION_AMNT as CALCULATION_AMNT
    /*Показатели*/
  /* Доля закрытых расхождений КНП в расхождениях КНП всего (по сумме) */
  ,nvl((seller_dis_cls_knp_amnt + buyer_dis_cls_knp_amnt)/  nullif((buyer_dis_opn_knp_amnt + seller_dis_opn_knp_amnt + buyer_dis_cls_knp_amnt + seller_dis_cls_knp_amnt), 0), 0) as eff_idx_2_1
  /* Доля закрытых расхождений КНП в расхождениях КНП всего (по количеству) */
  ,nvl((seller_dis_cls_knp_cnt + buyer_dis_cls_knp_cnt) / nullif( (buyer_dis_opn_knp_cnt + seller_dis_opn_knp_cnt + buyer_dis_cls_knp_cnt + seller_dis_cls_knp_cnt ), 0), 0) as eff_idx_2_2
  /* Доля деклараций с открытыми КНП расхождениями по стороне покупателя в декларациях с операциями (по количеству)*/
  ,nvl( decl_opn_knp_dis_buyer_cnt / nullif( decl_operation_cnt, 0), 0) as eff_idx_2_3
  /* Доля расхождений в сумме вычетов по НДС(КНП)*/
  ,nvl( (buyer_dis_opn_knp_amnt) / nullif(deduction_amnt, 0), 0 ) as eff_idx_2_4_knp
  /* Доля расхождений в сумме вычетов по НДС(Все)*/
  ,nvl( (buyer_dis_opn_amnt) / nullif(deduction_amnt, 0), 0 ) as eff_idx_2_4_all
    /*Покупатель*/
  /* Общие сведения о расхождениях */
  /*Все*/
  , BUYER_DIS_OPN_KNP_AMNT + BUYER_DIS_CLS_KNP_AMNT + SELLER_DIS_OPN_KNP_AMNT + SELLER_DIS_CLS_KNP_AMNT as DIS_TOTAL_AMNT
  , BUYER_DIS_OPN_KNP_CNT + BUYER_DIS_CLS_KNP_CNT + SELLER_DIS_OPN_KNP_CNT + SELLER_DIS_CLS_KNP_CNT as DIS_TOTAL_CNT
  /*Разрыв*/
  , BUYER_DIS_OPN_GAP_KNP_AMNT + BUYER_DIS_CLS_GAP_KNP_AMNT + SELLER_DIS_OPN_GAP_KNP_AMNT + SELLER_DIS_CLS_GAP_KNP_AMNT as DIS_GAP_TOTAL_AMNT
  , BUYER_DIS_OPN_GAP_KNP_CNT + BUYER_DIS_CLS_GAP_KNP_CNT + SELLER_DIS_OPN_GAP_KNP_CNT + SELLER_DIS_CLS_GAP_KNP_CNT as DIS_GAP_TOTAL_CNT
  /*НДС*/
  , BUYER_DIS_OPN_NDS_KNP_AMNT + BUYER_DIS_CLS_NDS_KNP_AMNT + SELLER_DIS_OPN_NDS_KNP_AMNT + SELLER_DIS_CLS_NDS_KNP_AMNT as DIS_NDS_TOTAL_AMNT
  , BUYER_DIS_OPN_NDS_KNP_CNT + BUYER_DIS_CLS_NDS_KNP_CNT + SELLER_DIS_OPN_NDS_KNP_CNT + SELLER_DIS_CLS_NDS_KNP_CNT as DIS_NDS_TOTAL_CNT
  /* Закрытые */
  /*Все*/
  , BUYER_DIS_CLS_KNP_AMNT + SELLER_DIS_CLS_KNP_AMNT as DIS_CLS_TOTAL_AMNT
  , BUYER_DIS_CLS_KNP_CNT + SELLER_DIS_CLS_KNP_CNT as DIS_CLS_TOTAL_CNT
  /*Разрыв*/
  , BUYER_DIS_CLS_GAP_KNP_AMNT + SELLER_DIS_CLS_GAP_KNP_AMNT as DIS_CLS_GAP_TOTAL_AMNT
  , BUYER_DIS_CLS_GAP_KNP_CNT + SELLER_DIS_CLS_GAP_KNP_CNT as DIS_CLS_GAP_TOTAL_CNT
  /*НДС*/
  , BUYER_DIS_CLS_NDS_KNP_AMNT + SELLER_DIS_CLS_NDS_KNP_AMNT as DIS_CLS_NDS_TOTAL_AMNT
  , BUYER_DIS_CLS_NDS_KNP_CNT + SELLER_DIS_CLS_NDS_KNP_CNT as DIS_CLS_NDS_TOTAL_CNT
  /* Открытые */
  /*Все*/
  , BUYER_DIS_OPN_KNP_AMNT + SELLER_DIS_OPN_KNP_AMNT as DIS_OPN_TOTAL_AMNT
  , BUYER_DIS_OPN_KNP_CNT +  SELLER_DIS_OPN_KNP_CNT  as DIS_OPN_TOTAL_CNT
  /*Разрыв*/
  , BUYER_DIS_OPN_GAP_KNP_AMNT + SELLER_DIS_OPN_GAP_KNP_AMNT  as DIS_OPN_GAP_TOTAL_AMNT
  , BUYER_DIS_OPN_GAP_KNP_CNT + SELLER_DIS_OPN_GAP_KNP_CNT  as DIS_OPN_GAP_TOTAL_CNT
  /*НДС*/
  , BUYER_DIS_OPN_NDS_KNP_AMNT + SELLER_DIS_OPN_NDS_KNP_AMNT as DIS_OPN_NDS_TOTAL_AMNT
  , BUYER_DIS_OPN_NDS_KNP_CNT + SELLER_DIS_OPN_NDS_KNP_CNT as DIS_OPN_NDS_TOTAL_CNT
  /*Декларация*/
  , DECL_ALL_CNT
  , DECL_ALL_NDS_SUM
  , DECL_OPERATION_CNT
  , DECL_DISCREPANCY_CNT
  , DECL_PAY_CNT
  , DECL_PAY_NDS_SUM
  , DECL_COMPENSATE_CNT
  , DECL_COMPENSATE_NDS_SUM
  , DECL_ZERO_CNT
  from
    NDS2_MRR_USER.EFFICIENCY_MONITOR_DATA mon
    inner join district_sono district on district.sono_code = mon.sono_code
  where district.sono_code in ('9971', '9972', '9973', '9974', '9975', '9976', '9977', '9978', '9979'))
select
      DISTRICT_CODE
    , UFNS_CODE
    , UFNS_NAME
    , CALCULATION_DATE
    , FISCAL_YEAR
    , QTR
    , DEDUCTION_AMNT
    , CALCULATION_AMNT
    /*Рейтинги*/
    , rank() over (partition by fiscal_year, QTR, CALCULATION_DATE order by nvl(round( eff_idx_2_1 * 100, 6), 0) desc) as rnk_federal_eff_idx_2_1
    , rank() over (partition by fiscal_year, QTR, CALCULATION_DATE order by nvl(round( eff_idx_2_2 * 100, 6), 0) desc) as rnk_federal_eff_idx_2_2
    , rank() over (partition by fiscal_year, QTR, CALCULATION_DATE order by nvl(round( eff_idx_2_3 * 100, 6), 0)) as rnk_federal_eff_idx_2_3
    , rank() over (partition by fiscal_year, QTR, CALCULATION_DATE order by nvl(round( eff_idx_2_4_knp * 100, 6), 0)) as rnk_federal_eff_idx_2_4_knp
    , rank() over (partition by fiscal_year, QTR, CALCULATION_DATE order by nvl(round( eff_idx_2_4_all * 100, 6), 0)) as rnk_federal_eff_idx_2_4_all
    , rank() over (partition by fiscal_year, QTR, DISTRICT_CODE, CALCULATION_DATE order by nvl(round( eff_idx_2_1 * 100, 6), 0) desc) as rnk_eff_idx_2_1
    , rank() over (partition by fiscal_year, QTR, DISTRICT_CODE, CALCULATION_DATE order by nvl(round( eff_idx_2_2 * 100, 6), 0) desc) as rnk_eff_idx_2_2
    , rank() over (partition by fiscal_year, QTR, DISTRICT_CODE, CALCULATION_DATE order by nvl(round( eff_idx_2_3 * 100, 6), 0)) as rnk_eff_idx_2_3
    , rank() over (partition by fiscal_year, QTR, DISTRICT_CODE, CALCULATION_DATE order by nvl(round( eff_idx_2_4_knp * 100, 6), 0)) as rnk_eff_idx_2_4_knp
    , rank() over (partition by fiscal_year, QTR, DISTRICT_CODE, CALCULATION_DATE order by nvl(round( eff_idx_2_4_all * 100, 6), 0)) as rnk_eff_idx_2_4_all
    /*Показатели*/
  /* Доля закрытых расхождений КНП в расхождениях КНП всего (по сумме) */
  ,round( eff_idx_2_1 * 100, 2) as eff_idx_2_1
  /* Доля закрытых расхождений КНП в расхождениях КНП всего (по количеству) */
  ,round( eff_idx_2_2 * 100, 2) as eff_idx_2_2
  /* Доля деклараций с расхождениями в декларациях с операциями (по количеству)*/
  ,round( eff_idx_2_3  * 100, 2) as eff_idx_2_3
  /* Доля расхождений в сумме вычетов по НДС(КНП)*/
  ,round( eff_idx_2_4_knp * 100, 2) as eff_idx_2_4_knp
  /* Доля расхождений в сумме вычетов по НДС(Все)*/
  ,round( eff_idx_2_4_all * 100, 2) as eff_idx_2_1_all
    /*Покупатель*/
  /* Общие сведения о расхождениях */
  /*Все*/
  , DIS_TOTAL_AMNT
  , DIS_TOTAL_CNT
  /*Разрыв*/
  , DIS_GAP_TOTAL_AMNT
  , DIS_GAP_TOTAL_CNT
  /*НДС*/
  , DIS_NDS_TOTAL_AMNT
  , DIS_NDS_TOTAL_CNT
  /* Закрытые */
  /*Все*/
  , DIS_CLS_TOTAL_AMNT
  , DIS_CLS_TOTAL_CNT
  /*Разрыв*/
  , DIS_CLS_GAP_TOTAL_AMNT
  , DIS_CLS_GAP_TOTAL_CNT
  /*НДС*/
  , DIS_CLS_NDS_TOTAL_AMNT
  , DIS_CLS_NDS_TOTAL_CNT
  /* Открытые */
  /*Все*/
  , DIS_OPN_TOTAL_AMNT
  , DIS_OPN_TOTAL_CNT
  /*Разрыв*/
  , DIS_OPN_GAP_TOTAL_AMNT
  , DIS_OPN_GAP_TOTAL_CNT
  /*НДС*/
  , DIS_OPN_NDS_TOTAL_AMNT
  , DIS_OPN_NDS_TOTAL_CNT
  /*Декларация*/
  , DECL_ALL_CNT
  , DECL_ALL_NDS_SUM
  , DECL_OPERATION_CNT
  , DECL_DISCREPANCY_CNT
  , DECL_PAY_CNT
  , DECL_PAY_NDS_SUM
  , DECL_COMPENSATE_CNT
  , DECL_COMPENSATE_NDS_SUM
  , DECL_ZERO_CNT
from  rating_query where UFNS_CODE <> '9900'
union all
select
  DISTRICT_CODE
, UFNS_CODE
, UFNS_NAME
, CALCULATION_DATE
, FISCAL_YEAR
, QTR
, DEDUCTION_AMNT
, CALCULATION_AMNT
/*Показатели*/
    /*Рейтинги*/
    , rank() over (partition by fiscal_year, QTR, CALCULATION_DATE order by nvl(round( eff_idx_2_1 * 100, 6), 0) desc) as rnk_federal_eff_idx_2_1
    , rank() over (partition by fiscal_year, QTR, CALCULATION_DATE order by nvl(round( eff_idx_2_2 * 100, 6), 0) desc) as rnk_federal_eff_idx_2_2
    , rank() over (partition by fiscal_year, QTR, CALCULATION_DATE order by nvl(round( eff_idx_2_3 * 100, 6), 0)) as rnk_federal_eff_idx_2_3
    , rank() over (partition by fiscal_year, QTR, CALCULATION_DATE order by nvl(round( eff_idx_2_4_knp * 100, 6), 0)) as rnk_federal_eff_idx_2_4_knp
    , rank() over (partition by fiscal_year, QTR, CALCULATION_DATE order by nvl(round( eff_idx_2_4_all * 100, 6), 0)) as rnk_federal_eff_idx_2_4_all
    , 0 as rnk_eff_idx_2_1
    , 0 as rnk_eff_idx_2_2
    , 0 as rnk_eff_idx_2_3
    , 0 as rnk_eff_idx_2_4_knp
    , 0 as rnk_eff_idx_2_4_all
    /*Показатели*/
  /* Доля закрытых расхождений КНП в расхождениях КНП всего (по сумме) */
  ,round( eff_idx_2_1 * 100, 2) as eff_idx_2_1
  /* Доля закрытых расхождений КНП в расхождениях КНП всего (по количеству) */
  ,round( eff_idx_2_2 * 100, 2) as eff_idx_2_2
  /* Доля деклараций с расхождениями в декларациях с операциями (по количеству)*/
  ,round( eff_idx_2_3  * 100, 2) as eff_idx_2_3
  /* Доля расхождений в сумме вычетов по НДС(КНП)*/
  ,round( eff_idx_2_4_knp * 100, 2) as eff_idx_2_4_knp
  /* Доля расхождений в сумме вычетов по НДС(Все)*/
  ,round( eff_idx_2_4_all * 100, 2) as eff_idx_2_1_all
/*Покупатель*/
/* Общие сведения о расхождениях */
/*Все*/
, DIS_TOTAL_AMNT
, DIS_TOTAL_CNT
/*Разрыв*/
, DIS_GAP_TOTAL_AMNT
, DIS_GAP_TOTAL_CNT
/*НДС*/
, DIS_NDS_TOTAL_AMNT
, DIS_NDS_TOTAL_CNT
/* Закрытые */
/*Все*/
, DIS_CLS_TOTAL_AMNT
, DIS_CLS_TOTAL_CNT
/*Разрыв*/
, DIS_CLS_GAP_TOTAL_AMNT
, DIS_CLS_GAP_TOTAL_CNT
/*НДС*/
, DIS_CLS_NDS_TOTAL_AMNT
, DIS_CLS_NDS_TOTAL_CNT
/* Открытые */
/*Все*/
, DIS_OPN_TOTAL_AMNT
, DIS_OPN_TOTAL_CNT
/*Разрыв*/
, DIS_OPN_GAP_TOTAL_AMNT
, DIS_OPN_GAP_TOTAL_CNT
/*НДС*/
, DIS_OPN_NDS_TOTAL_AMNT
, DIS_OPN_NDS_TOTAL_CNT
/*Декларация*/
, DECL_ALL_CNT
, DECL_ALL_NDS_SUM
, DECL_OPERATION_CNT
, DECL_DISCREPANCY_CNT
, DECL_PAY_CNT
, DECL_PAY_NDS_SUM
, DECL_COMPENSATE_CNT
, DECL_COMPENSATE_NDS_SUM
, DECL_ZERO_CNT
from  rating_mikn_query;
