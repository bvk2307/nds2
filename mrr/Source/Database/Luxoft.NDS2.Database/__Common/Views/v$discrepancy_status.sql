﻿create or replace force view nds2_mrr_user.v$discrepancy_status as
select
  dd.DOC_ID  as DocumentId,
  dd.ROW_KEY as RowKey,
  sd.TYPE    as DiscrepancyType,
  sd.STATUS  as DiscrepancyStatus,
  count(*)   as Count
from DOC_DISCREPANCY dd
  left join SOV_DISCREPANCY sd on sd.ID = dd.DISCREPANCY_ID
group by
  dd.DOC_ID,
  dd.ROW_KEY,
  sd.TYPE,
  sd.STATUS;
