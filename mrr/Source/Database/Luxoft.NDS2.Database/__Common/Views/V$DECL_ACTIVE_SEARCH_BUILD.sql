﻿create or replace force view NDS2_MRR_USER.V$DECL_ACTIVE_SEARCH_BUILD as
select
   DA.JOIN_K
  ,da.partition_id
  ,da.subpartition_id
  ,NVL(DA.ZIP, 0) as NDS2_DECL_CORR_ID
  ,NVL(DA.ZIP, 0) as ID
  ,NVL(DA.reg_number, 0) as SEOD_DECL_ID
  ,da.ask_id as ASK_DECL_ID
  ,NVL(DA.ZIP, 0) as DECLARATION_VERSION_ID
  ,NVL(1, 0) as IS_ACTIVE
  ,NVL(2, 0) as PROCESSINGSTAGE
  ,2 as LOAD_MARK
  ,NVL(2, 0) as MAX_PROCESSING_STAGE
  ,trunc(NVL(DA.submit_date, to_date('01.01.1900', 'dd.mm.yyyy'))) as UPDATE_DATE
  ,NVL(DA.inn_declarant, '-') as INN
  ,NVL(DA.inn_contractor, '-') as INN_CONTRACTOR
  ,NVL(DA.kpp, '-') as KPP
  ,NVL(DA.kpp_effective, '-') as KPP_EFFECTIVE
  ,NVL(DA.name_short, '-') as NAME
  ,NVL('', '-') as ADDRESS1
  ,NVL('', '-') as ADDRESS2
  ,NVL(DA.REGION_CODE, 0) as REGION_CODE
  ,NVL(DA.REGION_TITLE, '-') as REGION
  ,NVL(DA.REGION_TITLE, '-') as REGION_NAME
  ,NVL(DA.sono_code, 0) as SOUN_CODE
  ,NVL(DA.inspection_title, '-') as SOUN
  ,NVL(DA.inspection_title, '-') as SOUN_NAME
  ,NVL(DA.is_large, 0) as CATEGORY
  ,NVL('', '-') as CATEGORY_RU
  ,trunc(NVL(DA.submit_date, to_date('01.01.1900', 'dd.mm.yyyy'))) as REG_DATE
  ,NVL('', '-') as TAX_MODE
  ,NVL(DA.OKVED_CODE, '-') as OKVED_CODE
  ,NVL(null, '-') as CAPITAL
  ,NVL(DA.period_code, '-') as TAX_PERIOD
  ,NVL(DA.FISCAL_YEAR, '-') as FISCAL_YEAR
  ,NVL(DA.FULL_TAX_PERIOD, '-') as FULL_TAX_PERIOD
  ,NVL(DA.TAX_PERIOD_SORT_ORDER, 0) as TAX_PERIOD_SORT_ORDER
  ,trunc(NVL(DA.submit_date, to_date('01.01.1900', 'dd.mm.yyyy'))) as DECL_DATE
  ,trunc(NVL(DA.submission_date, to_date('01.01.1900', 'dd.mm.yyyy'))) as SUBMISSION_DATE
  ,NVL(DA.type, '-') as DECL_TYPE
  ,NVL(DA.type_code, 0) as DECL_TYPE_CODE
  ,DA.correction_number_effective as CORRECTION_NUMBER
  ,DA.correction_processed as CORRECTION_PROCESSED
  ,NVL(null, 0) as CORRECTION_NUMBER_RANK
  ,NVL(DA.signatory_status, '-') as PRPODP
  ,NVL(DA.signatory_name, '-') as SUBSCRIBER_NAME
  ,NVL(DA.sign, '-') as DECL_SIGN
  ,DA.nds_total as COMPENSATION_AMNT
  ,NVL(null, '-') as COMPENSATION_AMNT_SIGN
  ,NVL(0, 0) as LK_ERRORS_COUNT
  ,NVL(0, 0) as CH8_DEALS_AMNT_TOTAL
  ,NVL(0, 0) as CH9_DEALS_AMNT_TOTAL
  ,NVL(DA.CH8_NDS, 0) as CH8_NDS
  ,NVL(DA.CH9_NDS, 0) as CH9_NDS
  ,NVL(DA.Ch8_Contractor_Qty, 0) as CHAPTER8_CONTRAGENT_CNT
  ,NVL(DA.Ch9_Contractor_Qty, 0) as CHAPTER9_CONTRAGENT_CNT
  ,NVL(DA.Ch10_Contractor_Qty, 0) as CHAPTER10_CONTRAGENT_CNT
  ,NVL(DA.Ch11_Contractor_Qty, 0) as CHAPTER11_CONTRAGENT_CNT
  ,NVL(da.ch12_contractor_qty, 0) as CHAPTER12_CONTRAGENT_CNT
  ,DA.NDS_WEIGHT as NDS_WEIGHT
  ,NVL(DA.discrepancy_amt, 0) as DISCREP_CURRENCY_AMNT
  ,NVL(DA.discrepancy_qty, 0) as DISCREP_CURRENCY_COUNT
  ,NVL(0, 0) as GAP_DISCREP_COUNT
  ,NVL(0, 0) as GAP_DISCREP_AMNT
  ,NVL(0, 0) as WEAK_DISCREP_COUNT
  ,NVL(0, 0) as WEAK_DISCREP_AMNT
  ,NVL(0, 0) as NDS_INCREASE_DISCREP_COUNT
  ,NVL(0, 0) as NDS_INCREASE_DISCREP_AMNT
  ,NVL(0, 0) as GAP_MULTIPLE_DISCREP_COUNT
  ,NVL(0, 0) as GAP_MULTIPLE_DISCREP_AMNT
  ,NVL(DA.discrepancy_qty, 0) as TOTAL_DISCREP_COUNT
  ,NVL(DA.ch8_discrepancy_amt, 0) as DISCREP_BUY_BOOK_AMNT
  ,NVL(DA.ch8_discrepancy_amt, 0) as DISCREP_SELL_BOOK_AMNT
  ,NVL(DA.discrepancy_amt_min, 0) as DISCREP_MIN_AMNT
  ,NVL(DA.discrepancy_amt_max, 0) as DISCREP_MAX_AMNT
  ,NVL(DA.discrepancy_amt_max, 0) as DISCREP_AVG_AMNT
  ,NVL(DA.discrepancy_amt, 0) as DISCREP_TOTAL_AMNT
  ,NVL(DA.pvp, 0) as PVP_TOTAL_AMNT
  ,NVL(DA.pvp_min, 0) as PVP_DISCREP_MIN_AMNT
  ,NVL(DA.pvp_max, 0) as PVP_DISCREP_MAX_AMNT
  ,NVL(DA.pvp_avg, 0) as PVP_DISCREP_AVG_AMNT
  ,NVL(DA.ch8_pvp, 0) as PVP_BUY_BOOK_AMNT
  ,NVL(DA.ch9_pvp, 0) as PVP_SELL_BOOK_AMNT
  ,NVL(DA.ch10_pvp, 0) as PVP_RECIEVE_JOURNAL_AMNT
  ,NVL(DA.ch11_pvp, 0) as PVP_SENT_JOURNAL_AMNT
  ,NVL(DA.SUR_CODE, 0) as SUR_CODE
  ,NVL(DA.SUR_DESCRIPTION, '-') as SUR_DESCRIPTION
  ,NVL(null, to_date('01.01.1900', 'dd.mm.yyyy')) as CONTROL_RATIO_DATE
  ,NVL(null, 0) as CONTROL_RATIO_SEND_TO_SEOD
  ,NVL(DA.control_ratio_qty, 0) as CONTROL_RATIO_COUNT
  ,NVL(null, to_date('01.01.1900', 'dd.mm.yyyy')) as DATECLOSEKNP
  ,NVL(DA.status_knp, 0) as STATUS
  ,NVL(DA.has_discrepancy_knp, 0) as has_discrepancy_knp
  ,NVL(0, 0) as INVOICE_COUNT8
  ,NVL(0, 0) as INVOICE_COUNT81
  ,NVL(0, 0) as INVOICE_COUNT9
  ,NVL(0, 0) as INVOICE_COUNT91
  ,NVL(0, 0) as INVOICE_COUNT10
  ,NVL(0, 0) as INVOICE_COUNT11
  ,NVL(0, 0) as INVOICE_COUNT12
  ,NVL(0, 0) as ACTUAL_ZIP8
  ,NVL(0, 0) as ACTUAL_ZIP81
  ,NVL(0, 0) as ACTUAL_ZIP9
  ,NVL(0, 0) as ACTUAL_ZIP91
  ,NVL(0, 0) as ACTUAL_ZIP10
  ,NVL(0, 0) as ACTUAL_ZIP11
  ,NVL(0, 0) as ACTUAL_ZIP12
  ,NVL(0, 0) as SUMNDSPOK_8
  ,NVL(0, 0) as SUMNDSPOK_81
  ,NVL(0, 0) as SUMNDSPOKDL_81
  ,NVL(0, 0) as STPROD18_9
  ,NVL(0, 0) as STPROD18_91
  ,NVL(0, 0) as STPROD18DL_91
  ,NVL(0, 0) as STPROD10_9
  ,NVL(0, 0) as STPROD10_91
  ,NVL(0, 0) as STPROD10DL_91
  ,NVL(0, 0) as STPROD0_9
  ,NVL(0, 0) as STPROD0_91
  ,NVL(0, 0) as STPROD0DL_91
  ,NVL(0, 0) as SUMNDSPROD18_9
  ,NVL(0, 0) as SUMNDSPROD18_91
  ,NVL(0, 0) as SUMNDSPROD18DL_91
  ,NVL(0, 0) as SUMNDSPROD10_9
  ,NVL(0, 0) as SUMNDSPROD10_91
  ,NVL(0, 0) as SUMNDSPROD10DL_91
  ,NVL(0, 0) as STPRODOSV_9
  ,NVL(0, 0) as STPRODOSV_91
  ,NVL(0, 0) as STPRODOSVDL_91
  ,NVL(0, 0) as AKT_NOMKORR_8
  ,NVL(0, 0) as AKT_NOMKORR_81
  ,NVL(0, 0) as AKT_NOMKORR_9
  ,NVL(0, 0) as AKT_NOMKORR_91
  ,NVL(0, 0) as AKT_NOMKORR_10
  ,NVL(0, 0) as AKT_NOMKORR_11
  ,NVL(0, 0) as AKT_NOMKORR_12
  ,NVL(0, 0) as STPROD18
  ,NVL(0, 0) as STPROD10
  ,NVL(0, 0) as STPROD0
  ,NVL(0, 0) as SUMNDSPROD18
  ,NVL(0, 0) as SUMNDSPROD10
  ,NVL(0, 0) as STPROD
  ,NVL(0, 0) as STPRODOSV
  ,NVL(0, 0) as STPROD18DL
  ,NVL(0, 0) as STPROD10DL
  ,NVL(0, 0) as STPROD0DL
  ,NVL(0, 0) as SUMNDSPROD18DL
  ,NVL(0, 0) as SUMNDSPROD10DL
  ,NVL(0, 0) as STPRODOSVDL
  ,NVL(null, '-') as RECORD_MARK
  ,NVL(da.ch8_knp_gap_contractor_qty, 0) as ch8_knp_gap_contractor_qty
  ,NVL(da.ch9_knp_gap_contractor_qty, 0) as ch9_knp_gap_contractor_qty
  ,NVL(da.ch10_knp_gap_contractor_qty, 0) as ch10_knp_gap_contractor_qty
  ,NVL(da.ch11_knp_gap_contractor_qty, 0) as ch11_knp_gap_contractor_qty
  ,NVL(da.ch12_knp_gap_contractor_qty, 0) as ch12_knp_gap_contractor_qty
  ,NVL(da.ch8_knp_other_contractor_qty, 0) as ch8_knp_other_contractor_qty
  ,NVL(da.ch9_knp_other_contractor_qty, 0) as ch9_knp_other_contractor_qty
  ,NVL(da.ch10_knp_other_contractor_qty, 0) as ch10_knp_other_contractor_qty
  ,NVL(da.ch11_knp_other_contractor_qty, 0) as ch11_knp_other_contractor_qty
  ,NVL(da.ch12_knp_other_contractor_qty, 0) as ch12_knp_other_contractor_qty
  ,NVL(da.ch8_knp_gap_qty, 0)	as GAP_COUNT_8
  ,NVL(da.ch8_knp_gap_amt, 0)	as GAP_AMOUNT_8
  ,NVL(da.ch8_knp_other_qty, 0)	as OTHER_COUNT_8
  ,NVL(da.ch8_knp_other_amt, 0)	as OTHER_AMOUNT_8
  ,NVL(da.ch9_knp_gap_qty, 0)	as GAP_COUNT_9
  ,NVL(da.ch9_knp_gap_amt, 0)	as GAP_AMOUNT_9
  ,NVL(da.ch9_knp_other_qty, 0)	as OTHER_COUNT_9
  ,NVL(da.ch9_knp_other_amt, 0)	as OTHER_AMOUNT_9
  ,NVL(da.ch10_knp_gap_qty, 0)	as GAP_COUNT_10
  ,NVL(da.ch10_knp_gap_amt, 0)	as GAP_AMOUNT_10
  ,NVL(da.ch10_knp_other_qty, 0)	as OTHER_COUNT_10
  ,NVL(da.ch10_knp_other_amt, 0)	as OTHER_AMOUNT_10
  ,NVL(da.ch11_knp_gap_qty, 0)	    as GAP_COUNT_11
  ,NVL(da.ch11_knp_gap_amt, 0)	    as GAP_AMOUNT_11
  ,NVL(da.ch11_knp_other_qty, 0)	as OTHER_COUNT_11
  ,NVL(da.ch11_knp_other_amt, 0)	as OTHER_AMOUNT_11
  ,NVL(da.ch12_knp_gap_qty, 0)	    as GAP_COUNT_12
  ,NVL(da.ch12_knp_gap_amt, 0)	    as GAP_AMOUNT_12
  ,NVL(da.ch12_knp_other_qty, 0)	as OTHER_COUNT_12
  ,NVL(da.ch12_knp_other_amt, 0)	as OTHER_AMOUNT_12
  ,NVL(da.has_changes,0)			as HAS_CHANGES
  ,NVL(da.has_at,0)					as HAS_AT
  ,NVL(da.slide_at_count,0)			as SLIDE_AT_COUNT
  ,NVL(da.no_tks_count,0)			as NO_TKS_COUNT
  ,da.knp_close_reason_id
  ,da.has_cancelled_correction
  ,da.cancelled
  ,NVL(0,0)			as CLAIM_AMOUNT
  ,NVL(0,0)		as TOTAL_KNP_COUNT
  ,nvl(da.CH8_PRESENT_DESCRIPTION, '-')		as CH8_PRESENT_DESCRIPTION
  ,nvl(da.CH9_PRESENT_DESCRIPTION, '-')		as CH9_PRESENT_DESCRIPTION
  ,da.INSPECTOR_SID as ASSIGNEE_SID
  ,da.INSPECTOR as ASSIGNEE_NAME
from NDS2_MRR_USER.DECLARATION_ACTIVE DA;
