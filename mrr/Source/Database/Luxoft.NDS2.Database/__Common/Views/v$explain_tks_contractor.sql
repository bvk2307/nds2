﻿create or replace force view nds2_mrr_user.v$explain_tks_contractor as
select
   explain_zip as ExplainZip
  ,invoice_row_key as InvoiceRowKey
  ,state
  ,inn
  ,kpp
from EXPLAIN_TKS_CONTRACTOR;

