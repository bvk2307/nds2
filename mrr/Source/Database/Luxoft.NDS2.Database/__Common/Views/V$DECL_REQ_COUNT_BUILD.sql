﻿create or replace force view NDS2_MRR_USER.V$DECL_REQ_COUNT_BUILD
as 
select  
	dc.ref_entity_id   as REF_ENTITY_ID  
	,dc.sono_code      as SONO_CODE   
	,count(*)          as AT_COUNTER  
	From 
		NDS2_MRR_USER.DOC dc  
	Where 
		dc.close_date is null  
	Group By 
		dc.ref_entity_id, 
		dc.sono_code;
