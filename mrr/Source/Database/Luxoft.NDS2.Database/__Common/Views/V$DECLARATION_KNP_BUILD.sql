﻿create or replace force view NDS2_MRR_USER.V$DECLARATION_KNP_BUILD as
select
  ask.zip
from NDS2_MRR_USER.ASK_DECLANDJRNL ask
left join NDS2_SEOD.SEOD_DECLARATION seod on seod.tax_period = ask.period 
                          and seod.fiscal_year = ask.otchetgod 
                          and seod.inn = ask.innnp
                          and seod.correction_number = ask.nomkorr 
                          and seod.id_file = ask.idfajl
                          and seod.sono_code = ask.kodno
left join 
(
    select
      knp.declaration_reg_num,
      knp.ifns_code,
      max(knp.knp_id) as knp_id,
      max(knp.completion_date) as completion_date
    from NDS2_SEOD.SEOD_KNP knp
    group by knp.declaration_reg_num, knp.ifns_code
) knp2 on knp2.declaration_reg_num = seod.decl_reg_num and knp2.ifns_code = seod.sono_code
left join
(
	select da.REG_NUMBER, da.SONO_CODE
	from
	NDS2_SEOD.SEOD_DECLARATION_ANNULMENT da
	join NDS2_MRR_USER.DECLARATION_ANNULMENT_HISTORY dah on dah.annulment_id = da.id and dah.is_last = 1 and dah.status_id = 1
) dan on dan.reg_number = seod.decl_reg_num and dan.sono_code = seod.sono_code
where ask.prizn_akt_korr = 1
      and knp2.completion_date is null
	  and dan.reg_number is null
group by ask.zip;