﻿create or replace force view NDS2_MRR_USER.v$knp_decl_summary_build as
select
   KNP.inn
  ,KNP.kpp_effective as kpp_effective
  ,0 as type_code
  ,KNP.period_code as period_code
  ,KNP.fiscal_year as fiscal_year
  ,SUM(Case When knp.invoice_chapter=8 and KNP.DISCREPANCY_TYPE = 1 Then NVL(KNP.DISCREPANCY_AMT,0) END) As gap_amount_8
  ,SUM(Case When knp.invoice_chapter=9 and KNP.DISCREPANCY_TYPE = 1 Then NVL(KNP.DISCREPANCY_AMT,0) END) As gap_amount_9
  ,SUM(Case When knp.invoice_chapter=10 and KNP.DISCREPANCY_TYPE = 1 Then NVL(KNP.DISCREPANCY_AMT,0) END) As gap_amount_10
  ,SUM(Case When knp.invoice_chapter=11 and KNP.DISCREPANCY_TYPE = 1 Then NVL(KNP.DISCREPANCY_AMT,0) END) As gap_amount_11
  ,SUM(Case When knp.invoice_chapter=12 and KNP.DISCREPANCY_TYPE = 1 Then NVL(KNP.DISCREPANCY_AMT,0) END) As gap_amount_12
  ,SUM(Case When knp.invoice_chapter=8 and KNP.DISCREPANCY_TYPE <> 1 Then NVL(KNP.DISCREPANCY_AMT,0) END) As other_amount_8
  ,SUM(Case When knp.invoice_chapter=9 and KNP.DISCREPANCY_TYPE <> 1 Then NVL(KNP.DISCREPANCY_AMT,0) END) As other_amount_9
  ,SUM(Case When knp.invoice_chapter=10 and KNP.DISCREPANCY_TYPE <> 1 Then NVL(KNP.DISCREPANCY_AMT,0) END) As other_amount_10
  ,SUM(Case When knp.invoice_chapter=11 and KNP.DISCREPANCY_TYPE <> 1 Then NVL(KNP.DISCREPANCY_AMT,0) END) As other_amount_11
  ,SUM(Case When knp.invoice_chapter=12 and KNP.DISCREPANCY_TYPE <> 1 Then NVL(KNP.DISCREPANCY_AMT,0) END) As other_amount_12
  ,SUM(Case When knp.invoice_chapter=8 and KNP.DISCREPANCY_TYPE = 1 Then  1 ELSE 0 END) As gap_count_8
  ,SUM(Case When knp.invoice_chapter=9 and KNP.DISCREPANCY_TYPE = 1 Then  1 ELSE 0 END) As gap_count_9
  ,SUM(Case When knp.invoice_chapter=10 and KNP.DISCREPANCY_TYPE = 1 Then 1 ELSE 0  END) As gap_count_10
  ,SUM(Case When knp.invoice_chapter=11 and KNP.DISCREPANCY_TYPE = 1 Then 1 ELSE 0  END) As gap_count_11
  ,SUM(Case When knp.invoice_chapter=12 and KNP.DISCREPANCY_TYPE = 1 Then 1 ELSE 0  END) As gap_count_12
  ,SUM(Case When knp.invoice_chapter=8 and KNP.DISCREPANCY_TYPE <> 1 Then 1 ELSE 0  END) As other_count_8
  ,SUM(Case When knp.invoice_chapter=9 and KNP.DISCREPANCY_TYPE <> 1 Then 1 ELSE 0  END) As other_count_9
  ,SUM(Case When knp.invoice_chapter=10 and KNP.DISCREPANCY_TYPE <> 1 Then 1 ELSE 0 END) As other_count_10
  ,SUM(Case When knp.invoice_chapter=11 and KNP.DISCREPANCY_TYPE <> 1 Then 1 ELSE 0 END) As other_count_11
  ,SUM(Case When knp.invoice_chapter=12 and KNP.DISCREPANCY_TYPE <> 1 Then 1 ELSE 0 END) As other_count_12
  ,SUM(Case When knp.invoice_chapter between 8 and 12 or invoice_chapter is null or invoice_chapter = 99 Then 1 ELSE 0 END) As total_discrepancy_count
from
       KNP_DISCREPANCY_DECLARATION KNP
  where KNP.DISCREPANCY_STATUS <> 2
  group By knp.inn, knp.kpp_effective, knp.period_code, knp.fiscal_year;
