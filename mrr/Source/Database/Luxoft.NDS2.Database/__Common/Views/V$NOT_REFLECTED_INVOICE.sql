﻿create or replace force view NDS2_MRR_USER.V$NOT_REFLECTED_INVOICE as
select
  inv.declaration_version_id
  ,nd.inn_contractor as inn
  ,nd.kpp_contractor as kpp
  ,tp.name_short as NAME
  ,inv.invoice_num
  ,inv.invoice_date
  ,inv.doc_id
  ,inv.invoice_chapter as chapter
  ,inv.compare_row_key
  ,0 as DISCREPANCY_STATUS
from doc_invoice inv
join doc d on d.doc_id = inv.doc_id
join declaration_history nd on nd.zip = inv.declaration_version_id
join tax_payer tp on tp.inn = nd.inn_contractor and tp.kpp_effective = nd.kpp_effective
where d.doc_type in (1, 2) 
and inv.is_affected = 0;
