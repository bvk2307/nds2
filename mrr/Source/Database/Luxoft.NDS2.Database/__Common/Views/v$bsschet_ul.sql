﻿create or replace force view nds2_mrr_user.v$bsschet_ul as
select
 INN,
 KPP,
 BIK,
 INNKO,
 KPPKO,
 RNKO,
 NFKO,
 NAMEKO,
 NOMSCH,
 PRVAL,
 case PRVAL when  '0' then 'рубли' else 'валюта' end as PRVALTEXT,
 DATEOPENSCH,
 DATECLOSESCH,
 VIDSCH
from BSSCHET_UL;