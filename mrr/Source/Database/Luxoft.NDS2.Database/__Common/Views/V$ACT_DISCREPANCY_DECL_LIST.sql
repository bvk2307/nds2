﻿create or replace force view NDS2_MRR_USER.V$ACT_DISCREPANCY_DECL_LIST as
select 
   ad.id as id
  ,a.inn_declarant as act_inn
  ,a.kpp as act_kpp
  ,case 
		when d.close_date is not null then 2 
		when a.close_date is not null then 1 
		else 1 
   end as knp_doc_type
  ,case 
		when d.close_date is not null then dd.amount 
		when a.close_date is not null then ad.amount 
		else ad.amount 
   end as knp_doc_amount
  ,nvl(dd.initial_amt, 0) - nvl(dd.amount, 0) as decision_amount_difference
from ACT a
   inner join ACT_DISCREPANCY ad on a.id = ad.act_id
   left join DECISION_DISCREPANCY dd on ad.id = dd.id
   left join DECISION d on d.id = dd.decision_id;
