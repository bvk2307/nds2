﻿create or replace force view NDS2_MRR_USER.v$efficiency_monitor_by_ifns as
with region_sono as
(
select
    reg.s_code as region_code,
    sono.s_code as sono_code,
    sono_ufns.s_code as ufns_code,
    reg.s_name as region_name,
    sono.s_name as sono_name,
    sono_ufns.s_name as ufns_name,
    distr.district_id as district_code,
    distr.description as district_name
from
  NDS2_MRR_USER.EXT_SSRF reg
inner join NDS2_MRR_USER.EXT_SONO sono on reg.s_code = (case when sono.s_code = '9901' then '50' else substr(sono.s_code, 1,2) end)
inner join NDS2_MRR_USER.EXT_SONO sono_ufns on sono_ufns.s_code = (case when sono.s_code = '9901' then '50' else substr(sono.s_code, 1,2) end)||'00'
inner join FEDERAL_DISTRICT_REGION distr_region on distr_region.region_code = decode(sono.s_code, '9901', '50', substr(sono.s_code, 1,2))
inner join FEDERAL_DISTRICT distr on distr.district_id = distr_region.district_id
),
rating_query as
(
select
  rs.REGION_CODE
, rs.REGION_NAME
, rs.SONO_CODE
, rs.SONO_NAME
, rs.UFNS_CODE
, rs.UFNS_NAME
, rs.DISTRICT_CODE
, rs.DISTRICT_NAME
, CALC_DATE as CALCULATION_DATE
, FISCAL_YEAR
, PERIOD as QTR
, DEDUCTION_AMNT
, CALCULATION_AMNT
/* Доля закрытых расхождений КНП в расхождениях КНП всего (по сумме) */
,nvl((seller_dis_cls_knp_amnt + buyer_dis_cls_knp_amnt)/  nullif((buyer_dis_opn_knp_amnt + seller_dis_opn_knp_amnt + buyer_dis_cls_knp_amnt + seller_dis_cls_knp_amnt), 0), 0) as eff_idx_2_1
/* Доля закрытых расхождений КНП в расхождениях КНП всего (по количеству) */
,nvl((seller_dis_cls_knp_cnt + buyer_dis_cls_knp_cnt) / nullif( ( buyer_dis_opn_knp_cnt + seller_dis_opn_knp_cnt + buyer_dis_cls_knp_cnt + seller_dis_cls_knp_cnt ), 0), 0) as eff_idx_2_2
/* Доля деклараций с открытыми КНП расхождениями по стороне покупателя в декларациях с операциями (по количеству)*/
,nvl( decl_opn_knp_dis_buyer_cnt / nullif( decl_operation_cnt, 0), 0) as eff_idx_2_3
/* Доля расхождений в сумме вычетов по НДС(КНП)*/
,nvl( (buyer_dis_opn_knp_amnt / nullif(deduction_amnt, 0)), 0 ) as eff_idx_2_4_knp
/* Доля расхождений в сумме вычетов по НДС(Все)*/
,nvl( (buyer_dis_opn_amnt / nullif(deduction_amnt, 0)), 0 ) as eff_idx_2_4_all
/* Общие сведения о расхождениях */
/*Все*/
, BUYER_DIS_OPN_KNP_AMNT + BUYER_DIS_CLS_KNP_AMNT + SELLER_DIS_OPN_KNP_AMNT + SELLER_DIS_CLS_KNP_AMNT as DIS_TOTAL_AMNT
, BUYER_DIS_OPN_KNP_CNT + BUYER_DIS_CLS_KNP_CNT + SELLER_DIS_OPN_KNP_CNT + SELLER_DIS_CLS_KNP_CNT as DIS_TOTAL_CNT
/*Разрыв*/
, BUYER_DIS_OPN_GAP_KNP_AMNT + BUYER_DIS_CLS_GAP_KNP_AMNT + SELLER_DIS_OPN_GAP_KNP_AMNT + SELLER_DIS_CLS_GAP_KNP_AMNT as DIS_GAP_TOTAL_AMNT
, BUYER_DIS_OPN_GAP_KNP_CNT + BUYER_DIS_CLS_GAP_KNP_CNT + SELLER_DIS_OPN_GAP_KNP_CNT + SELLER_DIS_CLS_GAP_KNP_CNT as DIS_GAP_TOTAL_CNT
/*НДС*/
, BUYER_DIS_OPN_NDS_KNP_AMNT + BUYER_DIS_CLS_NDS_KNP_AMNT + SELLER_DIS_OPN_NDS_KNP_AMNT + SELLER_DIS_CLS_NDS_KNP_AMNT as DIS_NDS_TOTAL_AMNT
, BUYER_DIS_OPN_NDS_KNP_CNT + BUYER_DIS_CLS_NDS_KNP_CNT + SELLER_DIS_OPN_NDS_KNP_CNT + SELLER_DIS_CLS_NDS_KNP_CNT as DIS_NDS_TOTAL_CNT
/* Закрытые */
/*Все*/
, BUYER_DIS_CLS_KNP_AMNT + SELLER_DIS_CLS_KNP_AMNT as DIS_CLS_TOTAL_AMNT
, BUYER_DIS_CLS_KNP_CNT + SELLER_DIS_CLS_KNP_CNT as DIS_CLS_TOTAL_CNT
/*Разрыв*/
, BUYER_DIS_CLS_GAP_KNP_AMNT + SELLER_DIS_CLS_GAP_KNP_AMNT as DIS_CLS_GAP_TOTAL_AMNT
, BUYER_DIS_CLS_GAP_KNP_CNT + SELLER_DIS_CLS_GAP_KNP_CNT as DIS_CLS_GAP_TOTAL_CNT
/*НДС*/
, BUYER_DIS_CLS_NDS_KNP_AMNT + SELLER_DIS_CLS_NDS_KNP_AMNT as DIS_CLS_NDS_TOTAL_AMNT
, BUYER_DIS_CLS_NDS_KNP_CNT + SELLER_DIS_CLS_NDS_KNP_CNT as DIS_CLS_NDS_TOTAL_CNT
/*Открытые*/
/*Все*/
, BUYER_DIS_OPN_KNP_AMNT + SELLER_DIS_OPN_KNP_AMNT as DIS_OPN_TOTAL_AMNT
, BUYER_DIS_OPN_KNP_CNT +  SELLER_DIS_OPN_KNP_CNT  as DIS_OPN_TOTAL_CNT
/*Разрыв*/
, BUYER_DIS_OPN_GAP_KNP_AMNT + SELLER_DIS_OPN_GAP_KNP_AMNT  as DIS_OPN_GAP_TOTAL_AMNT
, BUYER_DIS_OPN_GAP_KNP_CNT + SELLER_DIS_OPN_GAP_KNP_CNT  as DIS_OPN_GAP_TOTAL_CNT
/*НДС*/
, BUYER_DIS_OPN_NDS_KNP_AMNT  + SELLER_DIS_OPN_NDS_KNP_AMNT as DIS_OPN_NDS_TOTAL_AMNT
, BUYER_DIS_OPN_NDS_KNP_CNT  + SELLER_DIS_OPN_NDS_KNP_CNT as DIS_OPN_NDS_TOTAL_CNT
/*Декларация*/
, DECL_ALL_CNT
, DECL_ALL_NDS_SUM
, DECL_OPERATION_CNT
, DECL_DISCREPANCY_CNT
, DECL_PAY_CNT
, DECL_PAY_NDS_SUM
, DECL_COMPENSATE_CNT
, DECL_COMPENSATE_NDS_SUM
, DECL_ZERO_CNT
from NDS2_MRR_USER.EFFICIENCY_MONITOR_DATA mon
inner join region_sono rs on mon.sono_code = rs.sono_code
where rs.region_code <> '99'
)
select
  REGION_CODE
, REGION_NAME
, SONO_CODE
, SONO_NAME
, UFNS_CODE
, UFNS_NAME
, CALCULATION_DATE
, FISCAL_YEAR
, QTR
, DEDUCTION_AMNT
, CALCULATION_AMNT
  /* Доля закрытых расхождений КНП в расхождениях КНП всего (по сумме) */
  ,round( eff_idx_2_1 * 100, 2) as eff_idx_2_1
  /* Доля закрытых расхождений КНП в расхождениях КНП всего (по количеству) */
  ,round( eff_idx_2_2 * 100, 2) as eff_idx_2_2
  /* Доля деклараций с расхождениями в декларациях с операциями (по количеству)*/
  ,round( eff_idx_2_3  * 100, 2) as eff_idx_2_3
  /* Доля расхождений в сумме вычетов по НДС(КНП)*/
  ,round( eff_idx_2_4_knp * 100, 2) as eff_idx_2_4_knp
  /* Доля расхождений в сумме вычетов по НДС(Все)*/
  ,round( eff_idx_2_4_all * 100, 2) as eff_idx_2_1_all
/*Рейтинг в РФ*/
, rank() over (partition by fiscal_year, QTR, CALCULATION_DATE order by EFF_IDX_2_1 desc)  as RF_RNK_EFF_IDX_2_1
, rank() over (partition by fiscal_year, QTR, CALCULATION_DATE order by EFF_IDX_2_2 desc)  as RF_RNK_EFF_IDX_2_2
, rank() over (partition by fiscal_year, QTR, CALCULATION_DATE order by EFF_IDX_2_3)  as RF_RNK_EFF_IDX_2_3
, rank() over (partition by fiscal_year, QTR, CALCULATION_DATE order by EFF_IDX_2_4_knp)  as RF_RNK_EFF_IDX_2_4_KNP
, rank() over (partition by fiscal_year, QTR, CALCULATION_DATE order by EFF_IDX_2_4_all)  as RF_RNK_EFF_IDX_2_4_ALL
/*Рейтинг в ФО*/
, rank() over (partition by fiscal_year, QTR, CALCULATION_DATE, district_code order by EFF_IDX_2_1 desc)  as FO_RNK_EFF_IDX_2_1
, rank() over (partition by fiscal_year, QTR, CALCULATION_DATE, district_code order by EFF_IDX_2_2 desc)  as FO_RNK_EFF_IDX_2_2
, rank() over (partition by fiscal_year, QTR, CALCULATION_DATE, district_code order by EFF_IDX_2_3)  as FO_RNK_EFF_IDX_2_3
, rank() over (partition by fiscal_year, QTR, CALCULATION_DATE, district_code order by EFF_IDX_2_4_knp)  as FO_RNK_EFF_IDX_2_4_KNP
, rank() over (partition by fiscal_year, QTR, CALCULATION_DATE, district_code order by EFF_IDX_2_4_all)  as FO_RNK_EFF_IDX_2_4_ALL
/*Рейтинг в Регионе/УФНС */
, rank() over (partition by fiscal_year, QTR, CALCULATION_DATE, region_code order by EFF_IDX_2_1 desc)  as RNK_EFF_IDX_2_1
, rank() over (partition by fiscal_year, QTR, CALCULATION_DATE, region_code order by EFF_IDX_2_2 desc)  as RNK_EFF_IDX_2_2
, rank() over (partition by fiscal_year, QTR, CALCULATION_DATE, region_code order by EFF_IDX_2_3)  as RNK_EFF_IDX_2_3
, rank() over (partition by fiscal_year, QTR, CALCULATION_DATE, region_code order by EFF_IDX_2_4_knp)  as RNK_EFF_IDX_2_4_KNP
, rank() over (partition by fiscal_year, QTR, CALCULATION_DATE, region_code order by EFF_IDX_2_4_all)  as RNK_EFF_IDX_2_4_ALL
/* Общие сведения о расхождениях */
/*Все*/
,DIS_TOTAL_AMNT
,DIS_TOTAL_CNT
/*Разрыв*/
,DIS_GAP_TOTAL_AMNT
,DIS_GAP_TOTAL_CNT
/*НДС*/
,DIS_NDS_TOTAL_AMNT
,DIS_NDS_TOTAL_CNT
/* Закрытые */
/*Все*/
,DIS_CLS_TOTAL_AMNT
,DIS_CLS_TOTAL_CNT
/*Разрыв*/
,DIS_CLS_GAP_TOTAL_AMNT
,DIS_CLS_GAP_TOTAL_CNT
/*НДС*/
,DIS_CLS_NDS_TOTAL_AMNT
,DIS_CLS_NDS_TOTAL_CNT
/*Открытые*/
/*Все*/
,DIS_OPN_TOTAL_AMNT
,DIS_OPN_TOTAL_CNT
/*Разрыв*/
,DIS_OPN_GAP_TOTAL_AMNT
,DIS_OPN_GAP_TOTAL_CNT
/*НДС*/
,DIS_OPN_NDS_TOTAL_AMNT
,DIS_OPN_NDS_TOTAL_CNT
/*Декларация*/
, DECL_ALL_CNT
, DECL_ALL_NDS_SUM
, DECL_OPERATION_CNT
, DECL_DISCREPANCY_CNT
, DECL_PAY_CNT
, DECL_PAY_NDS_SUM
, DECL_COMPENSATE_CNT
, DECL_COMPENSATE_NDS_SUM
, DECL_ZERO_CNT
from rating_query;
