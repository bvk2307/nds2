﻿CREATE OR REPLACE FORCE VIEW NDS2_MRR_USER.V$INSPECTOR_DECLARATION_SEARCH AS 
  select
       mdi.join_k
       ,mdi.partition_id
       ,mdi.subpartition_id 
       ,mdi.ID                                  as ID
       ,mdi.nds2_decl_corr_id                  as NDS2_DECL_CORR_ID
       ,mdi.DECLARATION_VERSION_ID             as DECLARATION_VERSION_ID
       ,null                                   as RECORD_MARK
       ,mdi.DECL_TYPE_CODE                     as DECL_TYPE_CODE
       ,mdi.DECL_TYPE                          as DECL_TYPE
       ,mdi.PROCESSINGSTAGE                    as PROCESSINGSTAGE
       ,mdi.LOAD_MARK
       ,mdi.STATUS                             as STATUS_KNP
	     ,decode(mdi.has_discrepancy_knp, 0, 'Нет', 'Есть') as STATUS
       ,mdi.IS_ACTIVE                          as IS_ACTIVE
       ,mdi.SUR_CODE                           as SUR_CODE
       ,mdi.INN                                as INN
       ,mdi.inn_contractor
       ,mdi.KPP                                as KPP
       ,mdi.kpp_effective
       ,mdi.kpp_effective                      as KPP_EFFECTIVE_CONTRACTOR
       ,mdi.NAME                               as NAME
       ,mdi.SEOD_DECL_ID                       as SEOD_DECL_ID
       ,mdi.TAX_PERIOD                         as TAX_PERIOD
       ,mdi.FISCAL_YEAR                        as FISCAL_YEAR
       ,mdi.FULL_TAX_PERIOD                    as FULL_TAX_PERIOD
	     ,mdi.TAX_PERIOD_SORT_ORDER			         as TAX_PERIOD_SORT_ORDER
       ,mdi.DECL_DATE                          as DECL_DATE
       ,mdi.SUBMISSION_DATE                    as SUBMISSION_DATE
       ,mdi.CORRECTION_NUMBER                  as CORRECTION_NUMBER
       ,mdi.CORRECTION_NUMBER_RANK             as CORRECTION_NUMBER_RANK
   	   ,mdi.CORRECTION_PROCESSED               as CORRECTION_PROCESSED
       ,mdi.SUBSCRIBER_NAME                    as SUBSCRIBER_NAME
       ,mdi.PRPODP                             as PRPODP
       ,mdi.DECL_SIGN                          as DECL_SIGN
       ,mdi.COMPENSATION_AMNT                  as COMPENSATION_AMNT
       ,mdi.CH8_NDS                            as NDS_CHAPTER8
       ,mdi.CH9_NDS                            as NDS_CHAPTER9
       ,mdi.NDS_WEIGHT                         as NDS_WEIGHT
       ,mdi.COMPENSATION_AMNT                  as NDS_AMOUNT
       ,null                                   as CHAPTER8_MARK
       ,nvl(mdi.CHAPTER8_CONTRAGENT_CNT, 0)    as CHAPTER8_CONTRAGENT_CNT
       ,nvl(mdi.CH8_KNP_GAP_CONTRACTOR_QTY, 0) as CHAPTER8_GAP_CONTRAGENT_CNT
       ,nvl(mdi.GAP_COUNT_8, 0)                as CHAPTER8_GAP_CNT
       ,nvl(mdi.GAP_AMOUNT_8, 0)               as CHAPTER8_GAP_AMNT
       ,nvl(mdi.CH8_KNP_OTHER_CONTRACTOR_QTY, 0) as CHAPTER8_OTHER_CONTRAGENT_CNT
       ,nvl(mdi.OTHER_COUNT_8, 0)              as CHAPTER8_OTHER_CNT
       ,nvl(mdi.OTHER_AMOUNT_8, 0)             as CHAPTER8_OTHER_AMNT
       ,null                                   as CHAPTER9_MARK
       ,nvl(mdi.CHAPTER9_CONTRAGENT_CNT, 0)    as CHAPTER9_CONTRAGENT_CNT
       ,nvl(mdi.CH9_KNP_GAP_CONTRACTOR_QTY, 0) as CHAPTER9_GAP_CONTRAGENT_CNT
       ,nvl(mdi.GAP_COUNT_9, 0)                as CHAPTER9_GAP_CNT
       ,nvl(mdi.GAP_AMOUNT_9, 0)               as CHAPTER9_GAP_AMNT
       ,nvl(mdi.CH9_KNP_OTHER_CONTRACTOR_QTY, 0) as CHAPTER9_OTHER_CONTRAGENT_CNT
       ,nvl(mdi.OTHER_COUNT_9, 0)              as CHAPTER9_OTHER_CNT
       ,nvl(mdi.OTHER_AMOUNT_9, 0)             as CHAPTER9_OTHER_AMNT
       ,null                                   as CHAPTER10_MARK
       ,nvl(mdi.CHAPTER10_CONTRAGENT_CNT, 0)   as CHAPTER10_CONTRAGENT_CNT
       ,nvl(mdi.CH10_KNP_GAP_CONTRACTOR_QTY, 0) as CHAPTER10_GAP_CONTRAGENT_CNT
       ,nvl(mdi.GAP_COUNT_10, 0)               as CHAPTER10_GAP_CNT
       ,nvl(mdi.GAP_AMOUNT_10, 0)              as CHAPTER10_GAP_AMNT
       ,nvl(mdi.CH10_KNP_OTHER_CONTRACTOR_QTY, 0) as CHAPTER10_OTHER_CONTRAGENT_CNT
       ,nvl(mdi.OTHER_COUNT_10, 0)             as CHAPTER10_OTHER_CNT
       ,nvl(mdi.OTHER_AMOUNT_10, 0)            as CHAPTER10_OTHER_AMNT
       ,null                                   as CHAPTER11_MARK
       ,nvl(mdi.CHAPTER11_CONTRAGENT_CNT, 0)   as CHAPTER11_CONTRAGENT_CNT
       ,nvl(mdi.CH11_KNP_GAP_CONTRACTOR_QTY, 0) as CHAPTER11_GAP_CONTRAGENT_CNT
       ,nvl(mdi.GAP_COUNT_11, 0)               as CHAPTER11_GAP_CNT
       ,nvl(mdi.GAP_AMOUNT_11, 0)              as CHAPTER11_GAP_AMNT
       ,nvl(mdi.CH11_KNP_OTHER_CONTRACTOR_QTY, 0) as CHAPTER11_OTHER_CONTRAGENT_CNT
       ,nvl(mdi.OTHER_COUNT_11, 0)             as CHAPTER11_OTHER_CNT
       ,nvl(mdi.OTHER_AMOUNT_11, 0)            as CHAPTER11_OTHER_AMNT
       ,null                                   as CHAPTER12_MARK
       ,nvl(mdi.CHAPTER12_CONTRAGENT_CNT, 0)   as CHAPTER12_CONTRAGENT_CNT
       ,nvl(mdi.ch12_knp_other_contractor_qty, 0) as CHAPTER12_GAP_CONTRAGENT_CNT
       ,nvl(mdi.GAP_COUNT_12, 0)               as CHAPTER12_GAP_CNT
       ,nvl(mdi.GAP_AMOUNT_12, 0)              as CHAPTER12_GAP_AMNT
       ,nvl(mdi.ch12_knp_other_contractor_qty, 0) as CHAPTER12_OTHER_CONTRAGENT_CNT
       ,nvl(mdi.OTHER_COUNT_12, 0)             as CHAPTER12_OTHER_CNT
       ,nvl(mdi.OTHER_AMOUNT_12, 0)            as CHAPTER12_OTHER_AMNT
       ,mdi.SOUN_CODE                          as SOUN_CODE
       ,mdi.REGION_CODE                        as REGION_CODE
       ,mdi.ASSIGNEE_NAME                      as INSPECTOR
       ,mdi.ASSIGNEE_SID                       as INSPECTOR_SID
       ,mdi.HAS_CHANGES
       ,mdi.HAS_AT
       ,mdi.SLIDE_AT_COUNT
       ,mdi.NO_TKS_COUNT
       ,mdi.HAS_CANCELLED_CORRECTION
from nds2_mrr_user.DECLARATION_ACTIVE_SEARCH mdi;
