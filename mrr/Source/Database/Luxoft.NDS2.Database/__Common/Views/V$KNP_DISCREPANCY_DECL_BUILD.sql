﻿create or replace force view NDS2_MRR_USER.V$KNP_DISCREPANCY_DECL_BUILD as
select
     dis.id as discrepancy_id
    ,b_decl.innnp as inn_declarant
    ,dis.buyer_inn as inn
    ,CAST(b_decl.innreorg as varchar2(12 char)) as inn_reorganized
    ,b_decl.kpp_effective
    ,b_decl.period as period_code
    ,b_decl.otchetgod as fiscal_year
    ,tp.quarter
    ,nvl(s_decl.innnp,dis.seller_inn) as contractor_inn_declarant
    ,dis.seller_inn as contractor_inn
    ,CAST(s_decl.innreorg as varchar2(12 char)) as contractor_inn_reorganized
    ,s_decl.kppnp as contractor_kpp
    ,nvl(s_decl.kpp_effective, '111111111') as contractor_kpp_effective
    ,s_decl.period as contractor_period_code
    ,s_decl.otchetgod as contractor_fiscal_year 
    ,dis.type as discrepancy_type
    ,CAST(dis.amnt as number(22,2)) as discrepancy_amt
    ,dis.amount_pvp as discrepancy_amt_pvp
    ,dis.status as discrepancy_status
    ,dis.invoice_rk as invoice_row_key
    ,inv.invoice_num as invoice_number
    ,inv.invoice_date
    ,dis.invoice_chapter
    ,CAST(inv.price_buy_amount as number(22,2)) as invoice_amt
    ,CAST(inv.price_buy_nds_amount as number(22,2)) as invoice_nds_amt
    ,nvl(inv.is_import, 0) as is_import
    ,dis.create_date as found_date
    ,dis.buyer_zip as zip
    ,b_decl.nd_prizn as taxpayer_decl_sign
    ,dis.seller_zip as contractor_zip
    ,s_decl.nd_prizn as contractor_decl_sign
    ,tp.name_short as contractor_name
    ,s_decl.summands as contractor_nds_amt
	,s_decl.type as contractor_type_code
    ,dis.invoice_contractor_rk as contractor_invoice_row_key
    ,dis.invoice_contractor_chapter as contractor_invoice_chapter
    ,addl.act_inn 
    ,addl.act_kpp 
    ,addl.knp_doc_type 
    ,addl.knp_doc_amount 
    ,addl.decision_amount_difference 
    ,dis.close_date 
    ,b_decl.kppnp as kpp
  from SOV_DISCREPANCY dis 
    join ASK_DECLANDJRNL b_decl on b_decl.zip = dis.buyer_zip
    join DICT_TAX_PERIOD tp on tp.code = b_decl.period
    left join ASK_DECLANDJRNL s_decl on s_decl.zip = dis.seller_zip
    left join SOV_INVOICE_ALL inv on inv.row_key = dis.invoice_rk and inv.discrepancy_id = dis.id
    left join TAX_PAYER tp on tp.inn = s_decl.innnp and tp.kpp_effective = s_decl.kpp_effective 
    left join V$ACT_DISCREPANCY_DECL_LIST addl on dis.id = addl.id 
  union all
  select
     dis.id as discrepancy_id
    ,s_decl.innnp as inn_declarant 
    ,dis.seller_inn as inn 
	,CAST(s_decl.innreorg as varchar2(12 char)) as inn_reorganized
    ,s_decl.kpp_effective
    ,s_decl.period as period_code 
    ,s_decl.otchetgod as fiscal_year
    ,tp.quarter
    ,b_decl.innnp as contractor_inn_declarant 
    ,dis.buyer_inn as contractor_inn 
	,CAST(b_decl.innreorg as varchar2(12 char)) as contractor_inn_reorganized
    ,b_decl.kppnp as contractor_kpp 
    ,b_decl.kpp_effective as contractor_kpp_effective 
    ,b_decl.period as contractor_period_code 
    ,b_decl.otchetgod as contractor_fiscal_year
    ,dis.type as discrepancy_type 
	,CAST(dis.amnt as number(22,2)) as discrepancy_amt
    ,dis.amount_pvp as discrepancy_amt_pvp 
    ,dis.status as discrepancy_status
    ,dis.invoice_contractor_rk as invoice_row_key
    ,inv.invoice_num as invoice_number 
    ,inv.invoice_date
    ,dis.invoice_contractor_chapter as invoice_chapter
	,CAST(inv.price_sell as number(22,2)) as invoice_amt
	,CAST((nvl(inv.price_sell_18,0) + nvl(inv.price_sell_10,0)) as number(22,2)) as invoice_nds_amt
    ,nvl(inv.is_import, 0) as is_import
    ,dis.create_date as found_date
    ,dis.seller_zip as zip
    ,s_decl.nd_prizn as taxpayer_decl_sign
    ,dis.buyer_zip as contractor_zip
    ,b_decl.nd_prizn as contractor_decl_sign
    ,tp.name_short as contractor_name
    ,b_decl.summands as contractor_nds_amt
	,b_decl.type as contractor_type_code
    ,dis.invoice_rk as contractor_invoice_row_key
    ,dis.invoice_chapter as contractor_invoice_chapter
    ,addl.act_inn 
    ,addl.act_kpp 
    ,addl.knp_doc_type 
    ,addl.knp_doc_amount 
    ,addl.decision_amount_difference 
    ,dis.close_date 
    ,s_decl.kppnp as kpp
  from SOV_DISCREPANCY dis 
    join ASK_DECLANDJRNL b_decl on b_decl.zip = dis.buyer_zip    
    join ASK_DECLANDJRNL s_decl on s_decl.zip = dis.seller_zip
    join DICT_TAX_PERIOD tp on tp.code = s_decl.period
    left join SOV_INVOICE_ALL inv on inv.row_key = dis.invoice_contractor_rk and inv.discrepancy_id = dis.id
    left join TAX_PAYER tp on tp.inn = b_decl.innnp and tp.kpp_effective = b_decl.kpp_effective 
    left join V$ACT_DISCREPANCY_DECL_LIST addl on dis.id = addl.id 
  where dis.buyer_inn <> dis.seller_inn 
     or b_decl.kpp_effective <> s_decl.kpp_effective
     or b_decl.period <> s_decl.period
     or b_decl.otchetgod <> s_decl.otchetgod;
