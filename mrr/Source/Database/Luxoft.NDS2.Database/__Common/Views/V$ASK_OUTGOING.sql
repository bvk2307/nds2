﻿create or replace force view NDS2_MRR_USER.V$ASK_OUTGOING as
select
      d.SONO_CODE,
      d.ref_entity_id as DECLARTION_REG_NUM,
      d.doc_id as OBJECT_REG_NUM,
      dt.flow_number as OBJECT_TYPE_ID,
      dt.description as OBJECT_TYPE_NAME,
      d.create_date as CREATE_DATE,
      d.status as OBJECT_STATUS_ID,
      cls.description as OBJECT_STATUS_NAME,
      d.document_body as XML_DATA
    from DOC d
    join DOC_STATUS cls on cls.id = d.status and cls.doc_type = d.doc_type
    join DOC_TYPE dt on dt.id = d.doc_type
    where d.doc_kind between 1 and 4 and d.status = 1
          and rownum <= 30000
union all
select
      bar.sono_code,
      bar.id,
      bar.doc_id,
      dt.flow_number,
      dt.description,
      bar.request_date,
      cls.id,
      cls.description,
      bar.doc_body
    from BANK_ACCOUNT_REQUEST bar
    join DOC_TYPE dt on dt.id = 6
    join DOC_STATUS cls on cls.id = bar.status_code and cls.doc_type = 6
    where bar.status_code = 3 and rownum <= 30000
union all
select 
		d.SONO_CODE
   ,d.REG_NUMBER as DECLARATION_REG_NUM
   ,da.ID as OBJECT_REG_NUM
   ,6  as OBJECT_TYPE_ID
   ,'Результат закрытия корректировки' as OBJECT_TYPE_NAME
   ,dah.status_date as CREATE_DATE
   ,dah.STATUS_ID as OBJECT_STATUS_ID
   ,das.NAME as OBJECT_STATUS_NAME
   ,da.DOCUMENT_BODY as XML_DATA 
from 
nds2_mrr_user.declaration_annulment da
join nds2_seod.seod_declaration_annulment d on d.id = da.seod_id
join nds2_mrr_user.declaration_annulment_history dah on dah.annulment_id = da.id and dah.is_last = 1
join nds2_mrr_user.declaration_annulment_status das on das.id = dah.status_id
where dah.STATUS_ID = 5 and da.send_status_id = 1 and rownum <= 30000;
