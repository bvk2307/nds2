﻿create or replace force view NDS2_MRR_USER.v$cfg_operation_csud
as
select 
  op.name as MasterName,
  opcsud.id as CSUDOperationId,
  opcsud.name as CSUDOperationName
from CFG_OPERATION op
  inner join CFG_OPERATION_CSUD opcsud on opcsud.operation_id = op.id;

