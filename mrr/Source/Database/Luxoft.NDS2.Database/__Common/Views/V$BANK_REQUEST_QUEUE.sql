﻿create or replace force view NDS2_MRR_USER.V$BANK_REQUEST_QUEUE as
select
  decl.inn_contractor as inn,
  nvl(decl.kppreorg, decl.kppnp) as kpp,
  case when decl.innnp != decl.inn_contractor then 1 else 0 end as reorganized,
  decl.kpp_effective,
  decl.type as type_code,
  case when dip.doc_id is not null then '2'||dip.quarter when dp.id is null then decl.period else '2'||dp.kvartal end as period_code,
  case  when dip.doc_id is not null then to_char(dip.fiscal_year) when dp.id is null then decl.otchetgod else to_char(dp.god) end as fiscal_year,
  dh.sono_code
from ask_declandjrnl decl
join (select to_timestamp(t.value,'DD-MM-YYYY HH24:MI:SS') as last_date from configuration t where t.parameter = 'BankAccountRequestDate') cfg on 1=1
join (select m.tax_period, min(m.month) as month
      from dict_tax_period_month m group by m.tax_period
     ) dpm on dpm.tax_period = decl.period and to_date('01.'||dpm.month||'.'||decl.otchetgod, 'DD.MM.YYYY') < sysdate
join (select t.zip, t.reg_number, t.sono_code_submited as sono_code,
             row_number() over (partition by t.zip order by t.submit_date) rn
      from declaration_history t
      where to_number(t.fiscal_year) > 2014 and t.type_code = 0
        and t.sono_code_submited not in ('9971', '9972', '9973', '9974', '9975', '9976', '9977', '9978', '9979')
        and t.is_large = 0
      ) dh on dh.zip = decl.zip and dh.rn = 1
join SONO_BANK_LIMIT limit on limit.sono_code = dh.sono_code
left join doc d on d.ref_entity_id = dh.reg_number and d.sono_code = decl.kodno and d.doc_type in (1)
      and (d.seod_accept_date > cfg.last_date or d.external_doc_date > cfg.last_date)
left join doc_invoice_periods dip on dip.doc_id = d.doc_id
left join
  (
    select
      src.ZIP,
      nvl(sum(case when src.TipFajla = 0 then nvl(act.KolZapisey, nvl(src.KolZapisey, 0)) end), 0) as invoice_count8,
      nvl(sum(case when src.TipFajla = 2 then nvl(act.KolZapisey, nvl(src.KolZapisey, 0)) end), 0) as invoice_count81,
      nvl(sum(case when src.TipFajla = 1 then nvl(act.KolZapisey, nvl(src.KolZapisey, 0)) end), 0) as invoice_count9,
      nvl(sum(case when src.TipFajla = 3 then nvl(act.KolZapisey, nvl(src.KolZapisey, 0)) end), 0) as invoice_count91
    from V$ASKSVODZAP src
    left join V$ASKSVODZAP act on act.id = src.IdAkt
    group by src.ZIP
  ) dsd on dsd.zip = decl.zip
left join V$ASKDECLPERIOD dp on dp.iddecl = decl.id and d.doc_id is null
where
  decl.prizn_akt_korr = 1
  and decl.type = 0
  and (
    (d.doc_id is not null)
    or (decl.insert_date > cfg.last_date
      and (decl.nd_prizn = 2
        or (dsd.invoice_count8 + dsd.invoice_count81 = 0 and dsd.invoice_count9 + dsd.invoice_count91 > 0))
      ));
