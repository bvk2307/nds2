﻿create or replace force view nds2_mrr_user.v$askPoyasnenieNepodtv as
select
   "Ид" as id
  ,zip
  ,"Пояснение" as poyasnenie
  ,"НомСчФПрод" as nomschfprod
  ,"ДатаСчФПрод" as dataschfprod
  ,"ИНН" as inn
  ,"КПП" as kpp
from nds2_mc."ASKПояснНеподтв";
