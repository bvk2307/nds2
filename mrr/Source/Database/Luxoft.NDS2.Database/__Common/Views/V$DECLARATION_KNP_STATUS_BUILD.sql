﻿create or replace force view NDS2_MRR_USER.V$DECLARATION_KNP_STATUS_BUILD as
with max_date as (
	select
		(select max(decl_insert_date) from declaration_knp_status) as decl_insert_date,
		(select max(seod_creation_date) from declaration_knp_status) as seod_creation_date
	from dual
), group_new as (
	select
		hist.inn_declarant,
		hist.inn_contractor,
		hist.kpp_effective,
		hist.fiscal_year,
		hist.period_effective,
		hist.type_code
	from declaration_history hist
	where (select decl_insert_date from max_date) is null or hist.insert_date >= (select decl_insert_date from max_date)
  group by
		hist.inn_declarant,
		hist.inn_contractor,
		hist.kpp_effective,
		hist.fiscal_year,
		hist.period_effective,
		hist.type_code
), decl_new as (
	select
    row_number() over (partition by hist.zip order by hist.inn_declarant) as zip_num,
		hist.zip,
		hist.inn_declarant,
		hist.inn_contractor,
		hist.kpp_effective,
		hist.fiscal_year,
		hist.period_effective,
		hist.type_code,
		row_number() over(
			partition by
				hist.inn_declarant,
				hist.inn_contractor,
				hist.kpp_effective,
				hist.fiscal_year,
				hist.period_effective,
				hist.type_code
			order by hist.correction_number
		) as correct_num,
		hist.insert_date,
		hist.is_active
	from group_new group_new
		left join declaration_history hist on
				hist.inn_declarant    = group_new.inn_declarant
			and hist.inn_contractor   = group_new.inn_contractor
			and hist.kpp_effective    = group_new.kpp_effective
			and hist.fiscal_year      = group_new.fiscal_year
			and hist.period_effective = group_new.period_effective
			and hist.type_code        = group_new.type_code
), decl_calc as (
	select
		t.zip,
		t.insert_date,
		case when t_next.zip is not null then 1 else 0 end as has_next,
		case when t_next.zip is not null then t_next.insert_date else null end as next_insert_date
	from decl_new t
		left join decl_new t_next on
          t_next.zip_num          = 1
      and t_next.inn_declarant    = t.inn_declarant
			and t_next.inn_contractor   = t.inn_contractor
			and t_next.kpp_effective    = t.kpp_effective
			and t_next.fiscal_year      = t.fiscal_year
			and t_next.period_effective = t.period_effective
			and t_next.type_code        = t.type_code
			and t_next.correct_num      = t.correct_num + 1
  where t.zip_num = 1
), seod_calc as (
	select
		hist.zip,
		min(seod.creation_date) as creation_date
	from nds2_seod.seod_knp seod
		inner join declaration_history hist on
				seod.declaration_reg_num = hist.reg_number
			and seod.ifns_code           = hist.sono_code_submited
	where (select seod_creation_date from max_date) is null or seod.creation_date >= (select seod_creation_date from max_date)
		and seod.completion_date is not null	
	group by hist.zip
)
, seod14_calc as (
	select hist.zip,
	       min(seod.received_at) as creation_date,
		   min(seod.cancelled_since) as cancelled_since
	from nds2_seod.SEOD_DECLARATION_ANNULMENT seod
	join nds2_mrr_user.DECLARATION_ANNULMENT_HISTORY ah
		on ah.ANNULMENT_ID = seod.id and ah.IS_LAST = 1 and ah.STATUS_ID in (1)
    join nds2_mrr_user.declaration_history hist	on
		   seod.REG_NUMBER = hist.reg_number and seod.SONO_CODE = hist.sono_code_submited
	where (select seod_creation_date from max_date) is null or seod.received_at >= (select seod_creation_date from max_date)
	group by hist.zip
)
, hist as (
  select h.zip
  from declaration_history h
  group by h.zip
)
select
	h.zip,
	coalesce(d_c.insert_date, t.decl_insert_date) as decl_insert_date,
	coalesce(s_c.creation_date, t.seod_creation_date) as seod_creation_date,
	case
		when t.zip is not null and t.calc_status in (1,2,3) then 0
		when s_c.zip is not null then 0
		when s14_c.zip is not null then 0
		when d_c.zip is not null then (case when d_c.has_next = 1 then 0 else 1 end)
		else t.calc_is_open
	end as calc_is_open,
	case
		when t.zip is not null then t.calc_status
		when s_c.zip is not null then 2
		when s14_c.zip is not null then 3
		when d_c.zip is not null then (case when d_c.has_next = 1 then 1 else 0 end)
		else t.calc_status
	end as calc_status,
	case
		when t.zip is not null and t.calc_status = 1 then t.calc_status_date
		when s_c.zip is not null then s_c.creation_date
		when d_c.zip is not null then (case when d_c.has_next = 1 then d_c.next_insert_date else d_c.insert_date end)
		when s14_c.zip is not null then s14_c.cancelled_since
		else t.calc_status_date
	end as calc_status_date
from hist h
	left join declaration_knp_status t on t.zip = h.zip
	left join decl_calc d_c on d_c.zip = h.zip
	left join seod_calc s_c on s_c.zip = h.zip
	left join seod14_calc s14_c on s14_c.zip = h.zip;
