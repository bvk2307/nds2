﻿create or replace force view NDS2_MRR_USER.V$SELECTION_DECLARATION as
select /*+ index(sd) index(dh) index(dtp) index(ext_ssrf) index(ext_sono) use_nl(dh) use_nl(dtp) use_nl(ext_ssrf) use_nl(ext_sono) */
     sd.selection_id as SelectionId
    ,sd.zip as Zip
    ,sd.IS_IN_PROCESS as Included
    ,decode(dh.is_active, '1', 1, 0) as IsActual
	,dh.TYPE_CODE as TypeId
    ,case when sd.sur_code is not null and sd.sur_code < 4 then sd.sur_code else 4 end as Sur
    ,dh.inn_declarant as Inn
    ,dh.kpp as Kpp
	,tp.name_short as TaxPayer
    ,dh.inn_reorganized as InnReorganized
    ,dh.reg_number as RegNumber
    ,dh.sign as DeclarationSign
    ,dh.Nds_Total as NdsAmount
    ,sd.DISCREPANCY_QTY as DiscrepancyCount
    ,sd.CH8_DISCREPANCY_AMT as DiscrepancyAmountChapter8
    ,sd.CH9_CH12_DISCREPANCY_AMT as DiscrepancyAmountChapter9
    ,sd.discrepancy_amt as DiscrepancyAmountTotal
    ,sd.discrepancy_amt_min as DiscrepancyAmountMin
    ,sd.discrepancy_amt_max as DiscrepancyAmountMax
    ,sd.discrepancy_amt_avg as DiscrepancyAmountAvg
  	,dtp.description || ' ' || dh.fiscal_year as TaxPeriod
	,(to_number(dh.fiscal_year)*1000 + dtp.sort_order) as TaxPeriodId
    ,dh.correction_number_effective as DeclarationVersion
    ,dh.submit_date as SubmitDate
    ,dh.region_code as RegionCode
    ,ext_ssrf.s_name as RegionName
    ,dh.sono_code as SonoCode
    ,ext_sono.s_name as SonoName
    ,da.name as Inspector
	,ds.calc_is_open as StatusCode
	,ds.calc_status_date as StatusDate
	,decode(dh.is_large, '0', 0, 1) as Category
	,sd.EXCLUSION_CODE as ExcludeFromClaimType
	,ers.SORT_INDEX 
	,ds.calc_status as CalcStatus
from SELECTION_DECLARATION sd
    join DECLARATION_HISTORY dh on dh.zip = sd.zip
	left join TAX_PAYER  tp on tp.inn = dh.inn_declarant and tp.kpp_effective = dh.kpp_effective
	left join V$DECLARATION_CURRENT_ASSIGNEE da on 
	da.declaration_type_code = dh.type_code and 
	da.inn_declarant = dh.inn_declarant and 
	da.kpp_effective = dh.kpp_effective and 
	da.fiscal_year = dh.fiscal_year and 
	da.period_effective = dh.period_effective
	left join DICT_TAX_PERIOD dtp on dtp.code = dh.period_code
    left join EXT_SSRF on ext_ssrf.s_code = dh.region_code
    left join EXT_SONO on ext_sono.s_code = dh.sono_code
	left join DECLARATION_KNP_STATUS ds on ds.zip = sd.zip
	left join R$EXCLUDE_REASON ers on ers.id = sd.EXCLUSION_CODE;