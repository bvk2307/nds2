﻿create or replace force view NDS2_MRR_USER.V$EXPLAIN_INVOICE_EDIT
as
  select (
     case 
       when x.StateThis is not null 
         then (case when x.StateThis = 1 then 2 when x.StateThis = 2 then 4 end)
       when x.StateNotThis is not null 
         then (case when x.StateNotThis = 1 then 3 when x.StateNotThis = 2 then 5 end)
       else 1 end) as State 
    ,x.*
  from (
    select
       1 as Type
      ,(select min(invoice_state) 
        from 
          nds2_seod.SEOD_EXPLAIN_REPLY er
          join STAGE_INVOICE_CORRECTED_STATE st on st.explain_id = er.explain_id
        where ((er.incoming_date < rp.incoming_date) or (er.incoming_date = rp.incoming_date and er.explain_id < rp.explain_id))
          and st.invoice_original_id = inv.invoice_row_key
        ) as StateNotThis
      ,stThis.Invoice_State as StateThis
      ,inv.*
      ,sd.correction_number as decl_correction_num
      ,tp.description||' '||sd.fiscal_year as display_full_tax_period
      ,inv.decl_tax_period as tax_period
      ,inv.decl_fiscal_year as fiscal_year
      ,rp.explain_id
    from DOC_INVOICE inv
    join DOC d 
		on d.doc_id = inv.doc_id
    join nds2_seod.SEOD_EXPLAIN_REPLY rp 
		on rp.doc_id = d.doc_id
    join DECLARATION_HISTORY sd 
		on sd.reg_number = d.ref_entity_id 
		and sd.sono_code_submited = d.sono_code 
		and sd.type_code = 0
    left join DICT_TAX_PERIOD tp 
		on tp.code = inv.decl_tax_period
    left join STAGE_INVOICE_CORRECTED_STATE stThis 
		on stThis.invoice_original_id = inv.invoice_row_key 
        and stThis.explain_id = rp.explain_id
    where 
		decode(d.doc_type, 1, inv.is_affected, 1) = 1 
  ) x;
