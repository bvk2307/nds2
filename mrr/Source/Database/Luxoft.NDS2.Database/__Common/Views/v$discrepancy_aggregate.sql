﻿create or replace force view NDS2_MRR_USER.v$discrepancy_aggregate
as 
select 
      dis.id
    , 1 as is_in_process
    , dis.buyer_zip as zip
    , dis.seller_zip
    , dis.buyer_zip
    , dis.amount
    , dis.pvp
    , dis.create_date
    , dis.compare_rule
    , dis.type_code
    , 1 as status
    , dis.invoice_rk as invoice_rk
    , dis.subtype_code as subtype_code
    , dis.region_code
    , dis.sono_code
    , dis.buyer_inn
    , dis.buyer_inn_declarant
    , dis.buyer_kpp
    , dis.buyer_kpp_declarant
    , dis.ks_r8r3_equality
    , UTL_GET_DECL_SIGN_NAME(dis.side1mark_rule) as buyer_decl_sign
    , dis.submit_date
    , dis.side1nds_amount as buyer_decl_nds_amount
    , dis.period_full
    , dis.side1operationcodes as buyer_operation_type
    , dis.invoice_chapter
    , dis.sur_code
    , dis.seller_inn
    , dis.seller_inn_declarant
    , dis.seller_kpp
    , dis.seller_kpp_declarant
    , dis.contractor_ks_1_27
    , UTL_GET_DECL_SIGN_NAME(dis.side2mark_rule) as seller_decl_sign
    , dis.contractor_submit_date
    , dis.side2nds_amount as seller_decl_nds_amount
    , dis.contractor_period_full
    , dis.invoice_contractor_chapter
    , dis.contractor_sur_code
    , dis.contractor_region_code
    , dis.contractor_sono_code
    , dis.created_by_invoice_no
    , dis.created_by_invoice_date
from DISCREPANCY_AGGREGATE dis
inner join DECLARATION_KNP dk on dk.zip = dis.buyer_zip
where not dis.buyer_zip is null;