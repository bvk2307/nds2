﻿declare 
  v_count number;
begin 
  SELECT count(*)
  into v_count
  FROM
      ALL_TAB_COLUMNS 
  WHERE
      OWNER = 'NDS2_MRR_USER'
      AND TABLE_NAME = 'DECLARATION_ACTIVE'
      AND COLUMN_NAME = 'KNP_CLOSE_REASON_ID';
    
  if v_count = 0 then
    execute immediate('alter table nds2_mrr_user.declaration_active add KNP_CLOSE_REASON_ID number null');
	execute immediate('COMMENT ON COLUMN nds2_mrr_user.declaration_active.KNP_CLOSE_REASON_ID IS ''Код причины закрытия КНП''');
  end if;
end;
/

declare 
  v_count number;
begin 
  SELECT count(*)
  into v_count
  FROM
      ALL_TAB_COLUMNS 
  WHERE
      OWNER = 'NDS2_MRR_USER'
      AND TABLE_NAME = 'DECLARATION_ACTIVE'
      AND COLUMN_NAME = 'HAS_CANCELLED_CORRECTION';
    
  if v_count = 0 then
    execute immediate('alter table nds2_mrr_user.declaration_active add HAS_CANCELLED_CORRECTION NUMBER(1) default 0 not null');
	execute immediate('COMMENT ON COLUMN nds2_mrr_user.declaration_active.HAS_CANCELLED_CORRECTION IS ''Наличие аннулированной корректировки для декларации''');
  end if;
end;
/

declare 
  v_count number;
begin 
  SELECT count(*)
  into v_count
  FROM
      ALL_TAB_COLUMNS 
  WHERE
      OWNER = 'NDS2_MRR_USER'
      AND TABLE_NAME = 'DECLARATION_ACTIVE'
      AND COLUMN_NAME = 'CANCELLED';
    
  if v_count = 0 then
    execute immediate('alter table nds2_mrr_user.declaration_active add CANCELLED NUMBER(1) default 0 not null');
	execute immediate('COMMENT ON COLUMN nds2_mrr_user.declaration_active.CANCELLED IS ''Корректировка аннулирована''');
  end if;
end;
/