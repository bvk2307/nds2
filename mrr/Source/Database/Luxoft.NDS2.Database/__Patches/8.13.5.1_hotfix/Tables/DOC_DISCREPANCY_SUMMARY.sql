﻿declare
pExist number := 0;
begin 
  select count(*) into pExist
  from all_objects
  where object_type in ('TABLE') and owner='NDS2_MRR_USER' and object_name = 'DOC_DISCREPANCY_SUMMARY';
  if pExist = 0 then
   EXECUTE IMMEDIATE 'create table NDS2_MRR_USER.DOC_DISCREPANCY_SUMMARY(
                      DOC_ID number(38, 0),
					  BUYER_CH8_DISCREP_AMT number(22, 2),
					  BUYER_CH8_DISCREP_QTY number(19, 0),
					  BUYER_CH8_GAP_DISCREP_AMT number(22, 2),
					  BUYER_CH8_GAP_DISCREP_QTY number(19, 0),
					  BUYER_CH8_NDS_DISCREP_AMT number(22, 2),
					  BUYER_CH8_NDS_DISCREP_QTY number(19, 0),
					  BUYER_CH10_11_DISCREP_AMT number(22, 2),
					  BUYER_CH10_11_DISCREP_QTY number(19, 0),
					  BUYER_CH10_11_GAP_DISCREP_AMT number(22, 2),
					  BUYER_CH10_11_GAP_DISCREP_QTY number(19, 0),
					  BUYER_CH10_11_NDS_DISCREP_AMT number(22, 2),
					  BUYER_CH10_11_NDS_DISCREP_QTY number(19, 0),
					  SELLER_CH9_10_GAP_DISCREP_AMT number(22, 2),
					  SELLER_CH9_10_GAP_DISCREP_QTY number(19, 0),
					  SELLER_CH9_NDS_DISCREP_AMT number(22, 2),
					  SELLER_CH9_NDS_DISCREP_QTY number(19, 0),
					  SELLER_CH10_NDS_DISCREP_AMT number(22, 2),
					  SELLER_CH10_NDS_DISCREP_QTY number(19, 0))';
   EXECUTE IMMEDIATE 'COMMENT ON COLUMN NDS2_MRR_USER.DOC_DISCREPANCY_SUMMARY.BUYER_CH8_DISCREP_AMT IS ''Сумма расхождение по стороне покупателя по книге покупок с точностью до копеек''';
   EXECUTE IMMEDIATE 'COMMENT ON COLUMN NDS2_MRR_USER.DOC_DISCREPANCY_SUMMARY.BUYER_CH8_DISCREP_QTY IS ''Кол-во расхождений по стороне покупателя по книге покупок''';
   EXECUTE IMMEDIATE 'COMMENT ON COLUMN NDS2_MRR_USER.DOC_DISCREPANCY_SUMMARY.BUYER_CH8_GAP_DISCREP_AMT IS ''Сумма расхождений вида "Разрыв" по стороне покупателя по книге покупок с точностью до копеек''';
   EXECUTE IMMEDIATE 'COMMENT ON COLUMN NDS2_MRR_USER.DOC_DISCREPANCY_SUMMARY.BUYER_CH8_GAP_DISCREP_QTY IS ''Кол-во расхождений вида "Разрыв" по стороне покупателя по книге покупок''';
   EXECUTE IMMEDIATE 'COMMENT ON COLUMN NDS2_MRR_USER.DOC_DISCREPANCY_SUMMARY.BUYER_CH8_NDS_DISCREP_AMT IS ''Сумма расхождений вида "Проверка НДС" по стороне покупателя по книге покупок с точностью до копеек''';
   EXECUTE IMMEDIATE 'COMMENT ON COLUMN NDS2_MRR_USER.DOC_DISCREPANCY_SUMMARY.BUYER_CH8_NDS_DISCREP_QTY IS ''Кол-во расхождений вида "Проверка НДС" по стороне покупателя по книге покупок''';
   EXECUTE IMMEDIATE 'COMMENT ON COLUMN NDS2_MRR_USER.DOC_DISCREPANCY_SUMMARY.BUYER_CH10_11_DISCREP_AMT IS ''Сумма расхождение по стороне покупателя по журналам с точностью до копеек''';
   EXECUTE IMMEDIATE 'COMMENT ON COLUMN NDS2_MRR_USER.DOC_DISCREPANCY_SUMMARY.BUYER_CH10_11_DISCREP_QTY IS ''Кол-во расхождений по стороне покупателя по журналам''';
   EXECUTE IMMEDIATE 'COMMENT ON COLUMN NDS2_MRR_USER.DOC_DISCREPANCY_SUMMARY.BUYER_CH10_11_GAP_DISCREP_AMT IS ''Сумма расхождений вида "Разрыв" по стороне покупателя по журналам с точностью до копеек''' ;
   EXECUTE IMMEDIATE 'COMMENT ON COLUMN NDS2_MRR_USER.DOC_DISCREPANCY_SUMMARY.BUYER_CH10_11_GAP_DISCREP_QTY IS ''Кол-во расхождений вида "Разрыв" по стороне покупателя по журналам''' ;
   EXECUTE IMMEDIATE 'COMMENT ON COLUMN NDS2_MRR_USER.DOC_DISCREPANCY_SUMMARY.BUYER_CH10_11_NDS_DISCREP_AMT IS ''Сумма расхождений вида "Проверка НДС" по стороне покупателя по журналам с точностью до копеек''' ;
   EXECUTE IMMEDIATE 'COMMENT ON COLUMN NDS2_MRR_USER.DOC_DISCREPANCY_SUMMARY.BUYER_CH10_11_NDS_DISCREP_QTY IS ''Кол-во расхождений вида "Проверка НДС" по стороне покупателя по журналам''' ;
   EXECUTE IMMEDIATE 'COMMENT ON COLUMN NDS2_MRR_USER.DOC_DISCREPANCY_SUMMARY.SELLER_CH9_10_GAP_DISCREP_AMT IS ''Сумма расхождений с точностью до копеек Разпыв/Неотраженные''' ;
   EXECUTE IMMEDIATE 'COMMENT ON COLUMN NDS2_MRR_USER.DOC_DISCREPANCY_SUMMARY.SELLER_CH9_10_GAP_DISCREP_QTY IS ''Кол-во расхождений Разпыв/Неотраженные''' ;
   EXECUTE IMMEDIATE 'COMMENT ON COLUMN NDS2_MRR_USER.DOC_DISCREPANCY_SUMMARY.SELLER_CH9_NDS_DISCREP_AMT IS ''Сумма расхождений вида "Проверка НДС" по стороне продавца по книге продаж с точностью до копеек''' ;
   EXECUTE IMMEDIATE 'COMMENT ON COLUMN NDS2_MRR_USER.DOC_DISCREPANCY_SUMMARY.SELLER_CH9_NDS_DISCREP_QTY IS ''Кол-во расхождений вида "Проверка НДС" по стороне продавца по книге продаж''' ;
   EXECUTE IMMEDIATE 'COMMENT ON COLUMN NDS2_MRR_USER.DOC_DISCREPANCY_SUMMARY.SELLER_CH10_NDS_DISCREP_AMT IS ''Сумма расхождений вида "Проверка НДС" по стороне продавца с точностью до копеек''' ;
   EXECUTE IMMEDIATE 'COMMENT ON COLUMN NDS2_MRR_USER.DOC_DISCREPANCY_SUMMARY.SELLER_CH10_NDS_DISCREP_QTY IS ''Кол-во расхождений вида "Проверка НДС" по стороне продавца''' ;
end if;
end;
/