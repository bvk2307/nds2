﻿-- актуализируем таблицу с историей реорганизаций ТНО
insert /*+APPEND*/ into nds2_mrr_user.ifns_reorg_history (	
  codeno_old
 ,codeno_new
 ,shift_value
) select codeno_old
        ,codeno_new
        ,shift_value
    from conv.reorg_shift@nds2_ais3_i$nds2 
    where col_name = 'D270'
      and d85 > to_date('01.01.2018', 'dd.mm.yyyy')
      and codeno_old in ('1656', '3701', '3720', '6031', '6179', '7150', '7107', '7155');
commit;

-- актуализируем таблицы с историей изменений в схеме nds2_mc
insert /*+APPEND*/ into nds2_mrr_user.askzipfile_reorg 
  select rg.codeno_new as "КодНО_Новый", t.* from nds2_mc."ASKZIPФайл" t join nds2_mrr_user.ifns_reorg_history rg on rg.codeno_old = t."КодНО" and rg.executed_at is null;

insert /*+APPEND*/ into nds2_mrr_user.askdekl_reorg 
  select rg.codeno_new as "КодНО_Новый", t."Ид", t."ZIP", t."ИдФайл", t."ВерсПрог", t."ВерсФорм", t."ПризнНал8-12", 
         t."ПризнНал8", t."ПризнНал81", t."ПризнНал9", t."ПризнНал91", t."ПризнНал10", t."ПризнНал11", t."ПризнНал12", 
         t."КНД", t."ДатаДок", t."Период", t."ОтчетГод", t."КодНО", t."ПоМесту", t."ОКВЭД", t."Тлф", t."НаимОрг", t."ИНННП", 
         t."КППНП", t."ФормРеорг", t."ИННРеорг", t."КППРеорг", t."ФамилияНП", t."ИмяНП", t."ОтчествоНП", t."ПрПодп", t."ФамилияПодп", 
         t."ИмяПодп", t."ОтчествоПодп", t."НаимДок", t."НаимОргПред", t."ОКТМО", t."КБК", t."СумПУ173.5", t."СумПУ173.1", t."НомДогИТ", 
         t."ДатаДогИТ", t."ДатаНачДогИТ", t."ДатаКонДогИТ", t."НалПУ164", t."НалВосстОбщ", t."РлТв18НалБаз", t."РеалТов18СумНал", 
         t."РлТв10НалБаз", t."РеалТов10СумНал", t."РлТв118НалБаз", t."РлТв118СумНал", t."РлТв110НалБаз", t."РлТв110СумНал", 
         t."РлПрдИКНалБаз", t."РлПрдИКСумНал", t."ВыпСМРСобНалБаз", t."ВыпСМРСобСумНал", t."ОпПрдПстНлБаз", t."ОплПрдПстСумНал", 
         t."СумНалВс", t."СумНал170.3.5", t."СумНал170.3.3", t."КорРлТв18НалБаз", t."КорРлТв18СумНал", t."КорРлТв10НалБаз", 
         t."КорРлТв10СумНал", t."КорРлТв118НлБз", t."КорРлТв118СмНл", t."КорРлТв110НлБз", t."КорРлТв110СмНл", t."КорРлПрдИКНлБз", 
         t."КорРлПрдИКСмНл", t."НалПредНППриоб", t."НалПредНППок", t."НалИсчСМР", t."НалУплТамож", t."НалУплНОТовТС", t."НалИсчПрод", 
         t."НалУплПокНА", t."НалВычОбщ", t."СумИсчислИтог", t."СумВозмПдтв", t."СумВозмНеПдтв", t."СумНал164Ит", t."НалВычНеПодИт", 
         t."НалИсчислИт", t."СмОп1010449КдОп", t."СмОп1010449НлБз", t."СмОп1010449КрИсч", t."СмОп1010449НлВст", t."ОплПостСв6Мес", 
         t."НаимКнПок", t."НаимКнПокДЛ", t."НаимКнПрод", t."НаимКнПродДЛ", t."НаимЖУчВыстСчФ", t."НаимЖУчПолучСчФ", t."НмВстСчФ173_5", 
         t."Публ", t."ИдЗагрузка", t."КодОпер47", t."НалБаза47", t."НалВосст47", t."КодОпер48", t."КорНалБазаУв48", t."КорНалБазаУм48", 
         t."КодОпер50", t."КорНалБазаУв50", t."КорИсчУв50", t."КорНалБазаУм50", t."КорИсчУм50", t."КлючДекл", t."НомКорр", 
         t."ДатаНачПер", t."ПризнакНД", t."СуммаНДС", t."СуммаНалИсчисл", t."СуммаВыч", t."ПризнАктКорр", t."ЭффКПП", 
         t."РлСр118СумНал", t."РлСр118НалБаза", t."РлСр110СумНал", t."РлСр110НалБаза", t."УплД151СумНал", t."УплД151НалБаза", 
         t."УплД173СумНал", t."УплД173НалБаза", t."НалПредНПКапСтр", t."НалВыч171.14" 
    from nds2_mc."ASKДекл" t join nds2_mrr_user.ifns_reorg_history rg on rg.codeno_old = t."КодНО";

insert /*+APPEND*/ into nds2_mrr_user.askpoyasnenie_reorg
  select rg.codeno_new as "КодНО_Новый", t.* from nds2_mc."ASKПояснение" t join nds2_mrr_user.ifns_reorg_history rg on rg.codeno_old = t."КодНО" and rg.executed_at is null;
 
insert /*+APPEND*/ into nds2_mrr_user.askjrnl_reorg
  select rg.codeno_new as "КодНО_Новый", t.* from nds2_mc."ASKЖурналУч" t join nds2_mrr_user.ifns_reorg_history rg on rg.codeno_old = t."КодНО" and rg.executed_at is null;

insert /*+APPEND*/ into nds2_mrr_user.askjrnlchast_reorg
  select rg.codeno_new as "КодНО_Новый", t.* from nds2_mc."ASKЖурналЧасть" t join nds2_mrr_user.ifns_reorg_history rg on rg.codeno_old = t."КодНО" and rg.executed_at is null;
 
commit;

-- производим собственно миграцию данных
begin
  
  for reorg in (
    select codeno_old
          ,codeno_new
          ,shift_value
      from conv.reorg_shift@nds2_ais3_i$nds2 
      where col_name = 'D270'
        and d85 > to_date('01.01.2018', 'dd.mm.yyyy')
        and codeno_old in ('1656', '3701', '3720', '6031', '6179', '7150', '7107', '7155')
  ) loop
    begin 
      nds2_mrr_user.pac$support.P$REMOVE_SONO(reorg.codeno_old, reorg.codeno_new, reorg.shift_value);      
      update nds2_mc."ASKZIPФайл" set "КодНО" = reorg.codeno_new where "КодНО" = reorg.codeno_old;
      update nds2_mc."ASKДекл" set "КодНО" = reorg.codeno_new where "КодНО" = reorg.codeno_old;
      update nds2_mc."ASKПояснение" set "КодНО" = reorg.codeno_new where "КодНО" = reorg.codeno_old;
      update nds2_mc."ASKЖурналУч" set "КодНО" = reorg.codeno_new where "КодНО" = reorg.codeno_old;
      update nds2_mc."ASKЖурналЧасть" set "КодНО" = reorg.codeno_new where "КодНО" = reorg.codeno_old;
      update nds2_mrr_user.ifns_reorg_history set executed_at = sysdate where codeno_old = reorg.codeno_old;  
         
      commit;
    end;
  end loop; 
  
end;
/
