﻿begin 
  execute immediate 'drop sequence NDS2_MRR_USER.SEQ_DECL_ANNULMENT_HISTORY';
  exception when others then null;
end;
/

begin
	execute immediate 'create sequence NDS2_MRR_USER.SEQ_DECL_ANNULMENT_HISTORY minvalue 1 maxvalue 9999999999 start with 1 increment by 1 nocache';
end;
/