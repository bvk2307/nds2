﻿alter table nds2_mrr_user.seod_explain_reply drop column doc_id;
alter table nds2_mrr_user.seod_explain_reply drop column type_id;
alter table nds2_mrr_user.seod_explain_reply drop column incoming_num;
alter table nds2_mrr_user.seod_explain_reply drop column incoming_date;
alter table nds2_mrr_user.seod_explain_reply drop column executor_receive_date;
alter table nds2_mrr_user.seod_explain_reply drop column send_date_to_iniciator;
alter table nds2_mrr_user.seod_explain_reply drop column receive_by_tks;
alter table nds2_mrr_user.seod_explain_reply drop column user_comment;