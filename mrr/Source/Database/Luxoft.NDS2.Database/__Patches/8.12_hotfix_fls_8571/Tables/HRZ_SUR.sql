﻿begin
  execute immediate 'drop table NDS2_MRR_USER.HRZ_SUR';
  exception when others then null;
end;
/

create table NDS2_MRR_USER.HRZ_SUR 
pctfree 0 as
select SIGN_CODE, INN, FISCAL_YEAR, QUARTER
from (
  select 
  X.SIGN_CODE,
  X.INN,
  X.FISCAL_YEAR, 
  P.QUARTER,
  ROW_NUMBER() OVER (PARTITION BY X.INN, X.FISCAL_YEAR, P.QUARTER ORDER BY X.UPDATE_DATE DESC) AS IDX 
  FROM NDS2_MRR_USER.EXT_SUR X
  inner join  NDS2_MRR_USER.DICT_TAX_PERIOD P on X.FISCAL_PERIOD = P.CODE 
  WHERE X.IS_ACTUAL = 1 
  ) where idx = 1;
