﻿begin
  execute immediate 'ALTER TABLE NDS2_MRR_USER.DOC_DISCREPANCY_EXCLUDE ADD IS_DOUBLE NUMBER(1)';
  exception when others then null;
end;
/

begin
  execute immediate 'ALTER TABLE NDS2_MRR_USER.DOC_DISCREPANCY_EXCLUDE ADD DOUBLE_CNT NUMBER(6)';
  exception when others then null;
end;
/

begin
  execute immediate 'ALTER TABLE NDS2_MRR_USER.DOC_DISCREPANCY_EXCLUDE ADD IS_NOT_FOUND NUMBER(1)';
  exception when others then null;
end;
/

begin
  execute immediate 'ALTER TABLE NDS2_MRR_USER.DOC_DISCREPANCY_EXCLUDE ADD IS_EXCLUDED_BEFORE NUMBER(1)';
  exception when others then null;
end;
/

begin
  execute immediate 'ALTER TABLE NDS2_MRR_USER.DOC_DISCREPANCY_EXCLUDE ADD IS_CLOSED_ALREADY NUMBER(1)';
  exception when others then null;
end;
/
