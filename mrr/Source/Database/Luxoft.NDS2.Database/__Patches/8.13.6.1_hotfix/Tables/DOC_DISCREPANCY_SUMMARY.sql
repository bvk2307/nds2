﻿begin
  execute immediate 'ALTER TABLE NDS2_MRR_USER.DOC_DISCREPANCY_SUMMARY ADD SELLER_CH12_NDS_DISCREP_AMT NUMBER (22, 2)';
  exception when others then null;
end;
/
begin
  execute immediate 'ALTER TABLE NDS2_MRR_USER.DOC_DISCREPANCY_SUMMARY ADD SELLER_CH12_NDS_DISCREP_QTY NUMBER (19, 0)';
  exception when others then null;
end;
/
begin
  execute immediate 'ALTER TABLE NDS2_MRR_USER.DOC_DISCREPANCY_SUMMARY ADD KPP VARCHAR2 (9 CHAR)';
  exception when others then null;
end;
/