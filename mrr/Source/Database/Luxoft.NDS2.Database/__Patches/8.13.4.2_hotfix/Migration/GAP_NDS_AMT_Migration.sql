﻿declare
  cursor cur_invoice is
    select
        di.doc_id, 
        di.invoice_row_key,
        di.ordinal_number,
        sum(case when TYPE = 1 then AMNT else 0 end) as GAP_AMOUNT,
        sum(case when TYPE = 4 then AMNT else 0 end) as NDS_AMOUNT
      from (select doc_id, invoice_row_key, actual_row_key, ordinal_number from nds2_mrr_user.doc_invoice where gap_amount is null and nds_amount is null ) di
      join nds2_mrr_user.doc_discrepancy dd 
        on dd.doc_id = di.doc_id and dd.row_key in (di.invoice_row_key, di.actual_row_key)
      left join nds2_mrr_user.sov_discrepancy sd 
        on dd.discrepancy_id = sd.id 
      group by di.doc_id, di.invoice_row_key, di.ordinal_number;

  type inv_type is record (doc_id NUMBER, invoice_row_key VARCHAR2(128 CHAR), ordinal_number number(19, 0), GAP_AMOUNT number(22, 2), NDS_AMOUNT number(22, 2));
  type inv_list_type is table of inv_type;
  inv_list inv_list_type;
  v_limit number := 10000;
begin
  open cur_invoice;
  loop
    fetch cur_invoice bulk collect into inv_list limit v_limit;
    
    forall i in 1..inv_list.count
      update nds2_mrr_user.doc_invoice
        set GAP_AMOUNT = inv_list(i).GAP_AMOUNT, NDS_AMOUNT = inv_list(i).NDS_AMOUNT
        where doc_id = inv_list(i).doc_id and ordinal_number = inv_list(i).ordinal_number
          and invoice_row_key = inv_list(i).invoice_row_key;
    
    commit;
    exit when inv_list.count < v_limit;
  end loop;
end;
/   
