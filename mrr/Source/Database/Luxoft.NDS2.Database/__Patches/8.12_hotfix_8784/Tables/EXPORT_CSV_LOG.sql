﻿create table NDS2_MRR_USER.EXPORT_CSV_LOG
(
  id         NUMBER not null,
  type       NUMBER(2) not null,
  title      VARCHAR2(256 CHAR),
  message    VARCHAR2(2048 CHAR),
  log_date   DATE
);