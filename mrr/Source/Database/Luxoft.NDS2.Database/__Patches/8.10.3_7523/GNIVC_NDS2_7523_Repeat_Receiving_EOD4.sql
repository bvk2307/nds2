﻿-- Сбрасываем дату закрытия КНП для корректной повторной обработки потоков ЭОД4, завершившихся со специфической ошибкой
merge into NDS2_MRR_USER.seod_knp tgt using ( 
  select sk.DECLARATION_REG_NUM 
        ,sk.IFNS_CODE 
        ,sk.KNP_ID
    from 
      ( 
        select code_nsi_sono 
              ,reg_number 
          from NDS2_MRR_USER.seod_incoming 
          where processing_result = 3 
            and info_type = 'CAM_NDS2_04' 
            and processing_result_text like 'ORA-01400%' 
      ) si 
    join NDS2_MRR_USER.doc dc 
      on si.code_nsi_sono = dc.sono_code 
      and si.reg_number = dc.ref_entity_id 
    join NDS2_MRR_USER.SEOD_KNP sk 
      on si.code_nsi_sono = sk.ifns_code 
      and si.reg_number = sk.declaration_reg_num 
    left outer join NDS2_MRR_USER.DOC_KNP_CLOSED dkc 
      on dc.doc_id = dkc.doc_id 
    where dkc.doc_id is null
) src 
on (tgt.knp_id = src.knp_id and tgt.declaration_reg_num = src.declaration_reg_num and tgt.ifns_code = src.ifns_code) 
when matched then 
  update set tgt.completion_date = null;

-- Запускаем повторную обработку потоков ЭОД4, завершившихся со специфической ошибкой
begin 
  NDS2_MRR_USER.PAC$SEOD_EXCHANGE.P$REPEAT_PROCESS_FAILED_SEOD (p_info_type => 'CAM_NDS2_04', p_result_text_clause => 'ORA-01400%');
end;
/
