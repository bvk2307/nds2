﻿delete from NDS2_MRR_USER.CONFIGURATION where PARAMETER in ('restrict_by_region','auto_approve_selections', 'auto_send_selections', 'period_forming_autoselection',
 'period_sending_claim', 'send_autoclame_onepart', 'send_autoclame_twopart', 'send_autoreclame_onepart', 'repeat_sending_claim_attempts', 'repeat_sending_claim_timeout', 
 'max_countrec_autoclaim', 'max_countrec_autooverclaim', 'timeout_input_answer_for_autooverclaim', 'timeout_answer_for_autooverclaim', 'timeout_answer_for_autoclaim', 
 'timeout_answer_for_autoclaim', 'timeout_claim_delivery');

merge into NDS2_MRR_USER.CONFIGURATION cfg
using (select 'NewArchitectureATDate' as parameter, TO_CHAR(trunc(sysdate), 'DD-MM-YYYY') as value from dual) src on (src.parameter = cfg.parameter)
when not matched then
  insert (parameter, value, default_value, description)
  values (src.parameter, src.value, src.value, 'Дата начала формирования АТ по правилам новой архитектуры');

commit;
