﻿begin 
  execute immediate 'drop sequence NDS2_MRR_USER.SEQ_CLAIM_JOURNAL';
exception when others then null;
end;
/
create sequence NDS2_MRR_USER.SEQ_CLAIM_JOURNAL
start with 1
increment by 1 
minvalue 1
maxvalue 99999999999999
nocache
nocycle;
