﻿begin 
  execute immediate 'drop sequence NDS2_MRR_USER.SEQ$HIVE_REQUEST_LOG';
exception when others then null;
end;
/

create sequence NDS2_MRR_USER.SEQ$HIVE_REQUEST_LOG
start with 1
increment by 1 
minvalue 1
maxvalue 99999999999999
nocache
nocycle;
