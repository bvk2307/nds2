﻿begin 
  execute immediate 'drop sequence NDS2_MRR_USER.SEQ$MD_DATA_REQUEST';
exception when others then null;
end;
/

create sequence NDS2_MRR_USER.SEQ$MD_DATA_REQUEST
start with 1
increment by 1 
minvalue 1
maxvalue 99999999999999
nocache
nocycle;
