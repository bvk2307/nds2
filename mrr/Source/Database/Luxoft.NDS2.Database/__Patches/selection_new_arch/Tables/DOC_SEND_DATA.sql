﻿create table NDS2_MRR_USER.DOC_SEND_DATA
(
  DocType number not null,
  SendDate date not null,
  SendTime date not null,
  Username varchar2(200 char)
);
