﻿begin
NDS2_MRR_USER.NDS2$SYS.DROP_IF_EXISTS('NDS2_MRR_USER', 'SELECTION_REGIONAL_BOUNDS');
end;
/
create table NDS2_MRR_USER.SELECTION_REGIONAL_BOUNDS
(
   ID					 NUMBER,
   SELECTION_TEMPLATE_ID NUMBER,
   INCLUDE  			 NUMBER default 1,
   REGION_CODE			 VARCHAR2(4 char),
   DISCREP_TOTAL_AMT	 NUMBER,
   DISCREP_MIN_AMT		 NUMBER, 
   DISCREP_GAP_TOTAL_AMT NUMBER,
   DISCREP_GAP_MIN_AMT	 NUMBER
);

