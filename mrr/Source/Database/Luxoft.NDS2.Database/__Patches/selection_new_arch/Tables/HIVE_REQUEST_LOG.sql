﻿begin
  NDS2_MRR_USER.NDS2$SYS.DROP_IF_EXISTS('NDS2_MRR_USER', 'HIVE_REQUEST_LOG');
end;
/
create table NDS2_MRR_USER.HIVE_REQUEST_LOG
(
  ID                        NUMBER,
  LOG_TIME                  DATE,
  CODE_SITE                 VARCHAR2(512 CHAR),
  MSG_TYPE                  NUMBER,
  MSG_TEXT                  VARCHAR2(2048 char)
) nologging;

