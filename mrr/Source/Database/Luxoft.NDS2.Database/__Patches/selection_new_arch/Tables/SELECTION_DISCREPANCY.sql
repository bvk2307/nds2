﻿begin
NDS2_MRR_USER.NDS2$SYS.DROP_IF_EXISTS('NDS2_MRR_USER', 'SELECTION_DISCREPANCY');
end;
/
begin
  EXECUTE IMMEDIATE 'drop index NDS2_MRR_USER.IDX_SELDIS_DID_1'; 
  EXECUTE IMMEDIATE 'drop index NDS2_MRR_USER.IDX_SELDIS_DIDINPROC'; 
  EXECUTE IMMEDIATE 'drop index NDS2_MRR_USER.IDX_SELDIS_SID';  
  EXECUTE IMMEDIATE 'drop index NDS2_MRR_USER.IDX_SELDIS_SIDINPROC';  
  EXECUTE IMMEDIATE 'drop index NDS2_MRR_USER.IDX_SELDIS_ZIPINPROC';   
  exception when others then null;
 end;
 /
  CREATE TABLE NDS2_MRR_USER.SELECTION_DISCREPANCY 
   (	
   	SELECTION_ID NUMBER NOT NULL,  
    DISCREPANCY_ID NUMBER NOT NULL, 
	IS_IN_PROCESS NUMBER(1,0) default 1, 
    EXCLUSION_CODE number(1, 0) default 0,
	STATUS NUMBER(1, 0),
	TAXPAYER_SIDE NUMBER(1, 0) NOT NULL,
	ZIP NUMBER NOT NULL,
	AMOUNT NUMBER(22,2),
	CREATE_DATE DATE,
	COMPARE_RULE NUMBER(3,0),
	TYPE_CODE NUMBER(1,0), 
	SUBTYPE_CODE NUMBER, 
	KS_R8R3_EQUALITY NUMBER,
	SONO_CODE VARCHAR2(4 CHAR),
	KVO NUMBER,
	INVOICE_CHAPTER NUMBER(2,0),
	INVOICE_ROW_KEY VARCHAR2(64 CHAR),
	CONTRACTOR_ZIP NUMBER, 
	CONTRACTOR_KS_1_27 NUMBER, 
	CONTRACTOR_INVOICE_CHAPTER NUMBER(2, 0), 
	CONTRACTOR_INVOICE_ROW_KEY VARCHAR2(64 CHAR),
    CONTRACTOR_KVO NUMBER,
    INVOICE_NUMBER VARCHAR2(1000 CHAR), 
	INVOICE_DATE DATE,
	REGION_CODE VARCHAR2(2 BYTE),
	INN VARCHAR2(12 CHAR), 
	INN_DECLARANT VARCHAR2(12 CHAR), 
	KPP VARCHAR2(9 CHAR),
	KPP_DECLARANT VARCHAR2(9 CHAR),
	DECL_SIGN number(1, 0), 
	DECL_SUBMIT_DATE DATE,
	DECL_NDS_AMOUNT NUMBER(22,2), 
	DECL_PERIOD VARCHAR2(2 CHAR),
	DECL_YEAR NUMBER(4, 0),
	SUR_CODE NUMBER(1, 0), 
	CONTRACTOR_INN VARCHAR2(12 CHAR), 
	CONTRACTOR_INN_DECLARANT VARCHAR2(12 CHAR), 
	CONTRACTOR_KPP VARCHAR2(9 CHAR), 
	CONTRACTOR_KPP_DECLARANT VARCHAR2(9 CHAR), 
	CONTRACTOR_DECL_SIGN NUMBER(1, 0), 
	CONTRACTOR_DECL_SUBMIT_DATE DATE, 
	CONTRACTOR_DECL_NDS_AMOUNT NUMBER(22,2), 
	CONTRACTOR_DECL_PERIOD VARCHAR2(2 CHAR), 
	CONTRACTOR_DECL_YEAR NUMBER(4), 
	CONTRACTOR_SUR_CODE NUMBER(1, 0), 
	CONTRACTOR_REGION_CODE VARCHAR2(2 CHAR), 
	CONTRACTOR_SONO_CODE VARCHAR2(4 CHAR)	
   );