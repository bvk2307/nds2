﻿begin
NDS2_MRR_USER.NDS2$SYS.DROP_IF_EXISTS('NDS2_MRR_USER', 'dict_selection_request_status');
end;
/
create table NDS2_MRR_USER.dict_selection_request_status
(
 id number(2),
 name varchar2(100 char)
);
