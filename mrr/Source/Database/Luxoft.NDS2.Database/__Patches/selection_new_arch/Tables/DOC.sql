﻿alter table NDS2_MRR_USER.DOC add tax_monitoring number(1);
alter table NDS2_MRR_USER.DOC add dispatch_counter number(2);
alter table NDS2_MRR_USER.DOC add side number(2);
alter table NDS2_MRR_USER.DOC add fault_send_date date;
alter table NDS2_MRR_USER.DOC add gap_discrepancy_amount number(22,2);
alter table NDS2_MRR_USER.DOC add TAX_PAYER_EXPLAIN_DATE date;
alter table NDS2_MRR_USER.DOC add nds_discrepancy_amount number(22,2);
alter table NDS2_MRR_USER.DOC add SEOD_SEND_DATE date;
alter table NDS2_MRR_USER.DOC add gap_discrepancy_count number(19);
alter table NDS2_MRR_USER.DOC add nds_discrepancy_count number(19);
alter table NDS2_MRR_USER.DOC add buyer_total_discrepancy_amt number(22,2);
alter table NDS2_MRR_USER.DOC add buyer_total_discrepancy_qty number(19);
alter table NDS2_MRR_USER.DOC add buyer_gap_discrepancy_amt number(22,2);
alter table NDS2_MRR_USER.DOC add buyer_gap_discrepancy_qty number(19);
alter table NDS2_MRR_USER.DOC add buyer_nds_discrepancy_amt number(22,2);
alter table NDS2_MRR_USER.DOC add buyer_nds_discrepancy_qty number(19);
alter table NDS2_MRR_USER.DOC add seller_total_discrepancy_amt number(22,2);
alter table NDS2_MRR_USER.DOC add seller_total_discrepancy_qty number(19);
alter table NDS2_MRR_USER.DOC add seller_gap_discrepancy_amt number(22,2);
alter table NDS2_MRR_USER.DOC add seller_gap_discrepancy_qty number(19);
alter table NDS2_MRR_USER.DOC add seller_nds_discrepancy_amt number(22,2);
alter table NDS2_MRR_USER.DOC add seller_nds_discrepancy_qty number(19);


