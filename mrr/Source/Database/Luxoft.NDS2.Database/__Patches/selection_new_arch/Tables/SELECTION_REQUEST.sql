﻿begin
NDS2_MRR_USER.NDS2$SYS.DROP_IF_EXISTS('NDS2_MRR_USER', 'selection_request');
end;
/
create table NDS2_MRR_USER.selection_request
(
 request_id number(22),
 selection_id number(22),
 version_id number(22),
 query_text clob,
 query_plan clob,
 status number,
 created date,
 processed date
);
