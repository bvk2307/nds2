﻿create table NDS2_MRR_USER.DOC_EXCLUDE_DATA
(
  DocId number not null,
  Excluded number(1) not null,
  ChangeTime date not null,
  LastUser varchar2(200 char)
);
