﻿CREATE OR REPLACE FORCE VIEW NDS2_MRR_USER.V$HIVE_SEL_DECLARATION_TO_SEND
AS
select
t.selection_id,
t.zip,
decode(s.template_id, 0, s.id, s.template_id) as selection_context_id
from selection_declaration t
inner join selection s
on s.id = t.selection_id
where s.status = 7 and t.is_in_process = 1;