﻿create or replace function NDS2_MRR_USER.UTL_GET_CONFIGURATION_NUMBER(p_key varchar2) return number
as
v_result number;
begin
v_result := null;

  begin
  select nvl(value, default_value) into v_result  from configuration where parameter = p_key;
  exception when no_data_found then
  v_result := null;
  end;
  
  if v_result is not null then
  return v_result;
  else
  select 
  case p_key 
    when 'timeout_claim_delivery' then claim_delivery_timeout_sec 
	when 'timeout_answer_for_autoclaim' then  claim_explain_timeout_sec 
	when 'timeout_answer_for_autooverclaim' then reclaim_reply_timeout_sec 
	when 'timeout_input_answer_for_autooverclaim' then reply_entry_timeout_sec 
	end into v_result  from system_settings;
	v_result := v_result/86400;
  end if;

  return v_result;
end;
/
