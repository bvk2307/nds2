﻿create table NDS2_MRR_USER.BKP_DOC compress nologging as select * from NDS2_MRR_USER.doc;

update NDS2_MRR_USER.DOC t
set
  t.dispatch_counter = 3,
  t.close_reason = null
where t.doc_type = 1;

commit;
