﻿begin
NDS2_MRR_USER.NDS2$SYS.DROP_IF_EXISTS('NDS2_MRR_USER', 'DAILY_JOB_RUN');
end;
/
CREATE TABLE NDS2_MRR_USER.DAILY_JOB_RUN(
  RUN_ID number not null,
  STARTED_AT date not null,
  COMPLETED_AT date,
  STATUS number
);

COMMENT ON COLUMN NDS2_MRR_USER.DAILY_JOB_RUN.RUN_ID IS 'Идентификатор запуска';
COMMENT ON COLUMN NDS2_MRR_USER.DAILY_JOB_RUN.STARTED_AT IS 'Дата и время запуска';
COMMENT ON COLUMN NDS2_MRR_USER.DAILY_JOB_RUN.COMPLETED_AT IS 'Дата и время завершения (в т.ч. с ошибкой)';
COMMENT ON COLUMN NDS2_MRR_USER.DAILY_JOB_RUN.STATUS IS 'Статус завершения: 0 – успех, 1 – есть ошибки, 2 – критическая ошибка, 3 - прервано';
COMMENT ON TABLE NDS2_MRR_USER.DAILY_JOB_RUN  IS 'Таблица содержит все запуски процедуры DAILY_JOB';