﻿begin
NDS2_MRR_USER.NDS2$SYS.DROP_IF_EXISTS('NDS2_MRR_USER', 'DAILY_JOB_STEP');
end;
/
CREATE TABLE NDS2_MRR_USER.DAILY_JOB_STEP(
  STEP_ID number not null,
  ORDINAL_NUMBER number not null,
  NAME varchar2(100) not null,
  USER_DESCRIPTION varchar2(1000) not null,
  IS_CRITICAL_FOR_DAILY number not null,
  IS_CRITICAL_FOR_FC number not null,
  EXECUTE_PACKAGE_NAME varchar2(30) not null,
  EXECUTE_PROCEDURE_NAME varchar2(30) not null,
  CONSTRAINT DAILY_JOB_STEP_PK PRIMARY KEY (ORDINAL_NUMBER)
);

COMMENT ON COLUMN NDS2_MRR_USER.DAILY_JOB_STEP.STEP_ID IS 'Идентификатор шага (латиница, сокр. описание шага)';
COMMENT ON COLUMN NDS2_MRR_USER.DAILY_JOB_STEP.ORDINAL_NUMBER IS 'Порядковый номер шага (уникальный индекс)';
COMMENT ON COLUMN NDS2_MRR_USER.DAILY_JOB_STEP.NAME IS 'Краткое название шага (на русском для разработчиков)';
COMMENT ON COLUMN NDS2_MRR_USER.DAILY_JOB_STEP.USER_DESCRIPTION IS 'Описание понятное пользователю';
COMMENT ON COLUMN NDS2_MRR_USER.DAILY_JOB_STEP.IS_CRITICAL_FOR_DAILY IS 'Признак того, что в случае неуспешного выполнения шага, необходимо прекратить выполнение DAILY_JOB';
COMMENT ON COLUMN NDS2_MRR_USER.DAILY_JOB_STEP.IS_CRITICAL_FOR_FC IS 'Признак того, что в случае неуспешного выполнения шага, необходимо прекратить выполнение полного цикла';
COMMENT ON COLUMN NDS2_MRR_USER.DAILY_JOB_STEP.EXECUTE_PACKAGE_NAME IS 'Имя пакета, в котором реализована процедура, выполняющая шаг';
COMMENT ON COLUMN NDS2_MRR_USER.DAILY_JOB_STEP.EXECUTE_PROCEDURE_NAME IS 'Имя процедуры, которую необходимо вызвать';
COMMENT ON TABLE NDS2_MRR_USER.DAILY_JOB_STEP  IS 'Таблица содержит все шаги DAILY_JOB';
