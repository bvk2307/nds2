﻿begin 
  execute immediate 'drop sequence NDS2_MRR_USER.DAILY_JOB_RUN_SEQ';
exception when others then null;
end;
/

CREATE SEQUENCE NDS2_MRR_USER.DAILY_JOB_RUN_SEQ
  INCREMENT BY 1
  START WITH 1
  NOMAXVALUE
  NOCYCLE;
