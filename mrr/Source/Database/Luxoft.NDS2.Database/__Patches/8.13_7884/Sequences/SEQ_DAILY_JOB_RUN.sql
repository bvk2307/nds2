﻿begin 
  execute immediate 'drop sequence NDS2_MRR_USER.SEQ_DAILY_JOB_RUN';
  exception when others then null;
end;
/

begin
	execute immediate 'create sequence NDS2_MRR_USER.SEQ_DAILY_JOB_RUN minvalue 1 maxvalue 9999999999 start with 1 increment by 1 nocache';
end;
/