﻿-- актуализируем таблицу с историей реорганизаций ТНО
insert /*+APPEND*/ into nds2_mrr_user.ifns_reorg_history (	
  codeno_old
 ,codeno_new
 ,shift_value
) 
  -- дополняем таблицу двумя ТНО, для которых не будет сдвига в регномерах 
  select '5107' as codeno_old 
        ,'5108' as codeno_new 
        ,0 as shift_value 
    from dual;

commit;

-- актуализируем таблицы с историей изменений в схеме nds2_mc
insert /*+APPEND*/ into nds2_mrr_user.askzipfile_reorg 
  select rg.codeno_new as "КодНО_Новый", t.* from nds2_mc."ASKZIPФайл" t join nds2_mrr_user.ifns_reorg_history rg on rg.codeno_old = t."КодНО";

insert /*+APPEND*/ into nds2_mrr_user.askdekl_reorg 
  select rg.codeno_new as "КодНО_Новый", t.* from nds2_mc."ASKДекл" t join nds2_mrr_user.ifns_reorg_history rg on rg.codeno_old = t."КодНО";

insert /*+APPEND*/ into nds2_mrr_user.askpoyasnenie_reorg
  select rg.codeno_new as "КодНО_Новый", t.* from nds2_mc."ASKПояснение" t join nds2_mrr_user.ifns_reorg_history rg on rg.codeno_old = t."КодНО";
 
insert /*+APPEND*/ into nds2_mrr_user.askjrnl_reorg
  select rg.codeno_new as "КодНО_Новый", t.* from nds2_mc."ASKЖурналУч" t join nds2_mrr_user.ifns_reorg_history rg on rg.codeno_old = t."КодНО";

insert /*+APPEND*/ into nds2_mrr_user.askjrnlchast_reorg
  select rg.codeno_new as "КодНО_Новый", t.* from nds2_mc."ASKЖурналЧасть" t join nds2_mrr_user.ifns_reorg_history rg on rg.codeno_old = t."КодНО";
 
commit;

-- производим собственно миграцию данных
begin
  
  for reorg in (
    -- дополняем таблицу двумя ТНО, для которых не будет сдвига в регномерах 
    select '5107' as codeno_old 
          ,'5108' as codeno_new 
          ,0 as shift_value 
      from dual
  ) loop
    begin 
      update nds2_mc."ASKZIPФайл" set "КодНО" = reorg.codeno_new where "КодНО" = reorg.codeno_old;
      update nds2_mc."ASKДекл" set "КодНО" = reorg.codeno_new where "КодНО" = reorg.codeno_old;
      update nds2_mc."ASKПояснение" set "КодНО" = reorg.codeno_new where "КодНО" = reorg.codeno_old;
      update nds2_mc."ASKЖурналУч" set "КодНО" = reorg.codeno_new where "КодНО" = reorg.codeno_old;
      update nds2_mc."ASKЖурналЧасть" set "КодНО" = reorg.codeno_new where "КодНО" = reorg.codeno_old;
      update nds2_mrr_user.ifns_reorg_history set executed_at = sysdate where codeno_old = reorg.codeno_old;  
         
      commit;
    end;
  end loop; 
  
end;
/
