﻿begin
  execute immediate 'drop index NDS2_MRR_USER.IDX_SEL_STAT_CREATE_DATE ';
  exception when others then null;
end;
/

create index NDS2_MRR_USER.IDX_SEL_STAT_CREATE_DATE on NDS2_MRR_USER.SELECTION_STATS (CREATEDATE);