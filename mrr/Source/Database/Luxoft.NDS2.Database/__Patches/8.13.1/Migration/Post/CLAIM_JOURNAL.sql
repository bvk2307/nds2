﻿insert into NDS2_MRR_USER.CLAIM_JOURNAL
(ID, CLAIM_TYPE, START_DATE, END_DATE)
select NDS2_MRR_USER.SEQ_CLAIM_JOURNAL.nextval, d.doc_type, d.create_date, d.create_date
from (select t.doc_type, trunc(t.create_date) as create_date
  from NDS2_MRR_USER.doc t
  group by t.doc_type, trunc(t.create_date)) d;
commit;
