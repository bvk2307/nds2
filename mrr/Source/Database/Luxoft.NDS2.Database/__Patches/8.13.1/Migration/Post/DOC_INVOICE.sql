﻿declare
cursor cur_invoice is
select
  di.rowid,
  case when (dh1.inn_contractor <> dh2.inn_contractor)
         or (dh1.inn_contractor = dh2.inn_contractor and dh1.kpp_effective <> dh2.kpp_effective) then 0
  else 1 end as is_affected
from nds2_mrr_user.doc_invoice di
join nds2_mrr_user.doc d on d.doc_id = di.doc_id
join nds2_mrr_user.declaration_history dh1 on dh1.reg_number = d.ref_entity_id and dh1.sono_code_submited = d.sono_code
join nds2_mrr_user.declaration_history dh2 on dh2.zip = di.declaration_version_id
where d.doc_type in (1,2) and d.doc_kind in (1,2) and di.is_affected is null;

type inv_type is record (rid rowid, is_affected number(1));

type inv_list_type is table of inv_type index by binary_integer;

inv_list inv_list_type;

v_limit number := 10000;

begin
  open cur_invoice;
  loop
    fetch cur_invoice bulk collect into inv_list limit v_limit;
    
    forall i in 1..inv_list.count
    update nds2_mrr_user.doc_invoice
    set is_affected = inv_list(i).is_affected
    where rowid = inv_list(i).rid;
    
    commit;
    
    exit when inv_list.count < v_limit;
  end loop;
  close cur_invoice;
end;
/ 