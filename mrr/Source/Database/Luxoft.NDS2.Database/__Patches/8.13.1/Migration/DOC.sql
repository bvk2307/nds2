﻿update NDS2_MRR_USER.DOC 
   set KNP_SYNC_STATUS = 1 
   where doc_id in (
                     select dc.doc_id 
                       from NDS2_MRR_USER.doc dc
                       where dc.doc_type = 1 
                         and dc.doc_kind in (1, 2)
                         and dc.status not in (-1,1,3) 
                         and dc.knp_sync_status = 0 
                         and (dc.seod_accepted = 1 or
                              dc.tax_payer_send_date is not null or
                              dc.tax_payer_delivery_date is not null or
                              dc.tax_payer_explain_date is not null)
        );

commit;