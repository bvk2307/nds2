﻿CREATE TABLE NDS2_MRR_USER.DISCREPANCY_USER_COMMENTS (
    DISCREPANCY_ID NUMBER NOT NULL,
    COMMENT_TEXT VARCHAR2(2048 CHAR)
) NOCOMPRESS LOGGING
TABLESPACE NDS2_DATA; 

COMMENT ON COLUMN NDS2_MRR_USER.DISCREPANCY_USER_COMMENTS.DISCREPANCY_ID IS 'Идентификатор расхождения';
COMMENT ON COLUMN NDS2_MRR_USER.DISCREPANCY_USER_COMMENTS.COMMENT_TEXT IS 'Текст пользовательского комментария к расхождению';

COMMENT ON TABLE NDS2_MRR_USER.DISCREPANCY_USER_COMMENTS  IS 'Пользовательские комментарии к расхождению';
