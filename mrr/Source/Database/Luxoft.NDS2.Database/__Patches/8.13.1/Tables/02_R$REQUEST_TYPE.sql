﻿create table NDS2_MRR_USER.R$REQUEST_TYPE
(
  ID NUMBER not null,
  TARGET_TABLE VARCHAR2(320 CHAR) not null,
  DESCRIPTION VARCHAR2(200 CHAR),
  CLEAN_WEEKLY VARCHAR2(1 CHAR) not null,
  CREATE_DATE DATE, 
  constraint PK$R$REQUEST_TYPE primary key (ID) using index tablespace NDS2_IDX
);
