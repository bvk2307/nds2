﻿create table NDS2_MRR_USER.SOV_DISCREPANCY_AGGREGATE
(
 SELLER_ZIP NUMBER,
 SELLER_INN VARCHAR2(12 CHAR),
 AGGREGATE_DATE DATE,
 GAP_AMT_9 NUMBER,
 GAP_QTY_9 NUMBER,
 GAP_CONTRAGENT_QTY_9 NUMBER,
 OTHER_AMT_9 NUMBER,
 OTHER_QTY_9 NUMBER,
 OTHER_CONTRAGENT_QTY_9 NUMBER,
 TOTAL_CONTRAGENT_QTY NUMBER
);
