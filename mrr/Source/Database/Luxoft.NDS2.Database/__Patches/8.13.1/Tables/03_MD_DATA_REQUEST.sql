﻿create table NDS2_MRR_USER.MD_DATA_REQUEST
(
  ID NUMBER not null,
  REQUEST_TYPE_ID NUMBER not null,
  REQUEST_KEY CLOB not null,
  REQUEST_HASH_KEY NUMBER not null,
  STATUS NUMBER not null,
  STATUS_DATE DATE,
  PROCESSED_ROWS NUMBER, 
  constraint PK$MD_DATA_REQUEST primary key (ID) using index tablespace NDS2_IDX,
  constraint FK$MD_DATA_REQUEST$TYPE_ID foreign key (REQUEST_TYPE_ID) references NDS2_MRR_USER.R$REQUEST_TYPE(ID),
  constraint FK$MD_DATA_REQUEST$STATUS foreign key (STATUS) references NDS2_MRR_USER.R$REQUEST_STATUS(ID)
);
