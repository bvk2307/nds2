﻿-- добавляем в таблицу DOC_INVOICE столбцs с суммами расхождений типа "Разрыв" и "Проверка НДС"
ALTER TABLE NDS2_MRR_USER.DOC_INVOICE ADD (
    GAP_AMOUNT NUMBER(22,2)
   ,NDS_AMOUNT NUMBER(22,2)
   ,IS_AFFECTED NUMBER (1,0)
   ,ERROR_CODE NUMBER(1,0)
); 

COMMENT ON COLUMN NDS2_MRR_USER.DOC_INVOICE.GAP_AMOUNT IS 'Сумма расхождений типа "Разрыв" данного СФ'; 
COMMENT ON COLUMN NDS2_MRR_USER.DOC_INVOICE.NDS_AMOUNT IS 'Сумма расхождений типа "Проверка НДС" данного СФ'; 
COMMENT ON COLUMN NDS2_MRR_USER.DOC_INVOICE.IS_AFFECTED IS 'Признак отраженной записи о СФ (0 - "неотраженная", 1 - "отраженная")'; 
COMMENT ON COLUMN NDS2_MRR_USER.DOC_INVOICE.ERROR_CODE IS 'Код ошибки)';
 
ALTER TABLE NDS2_MRR_USER.DOC_INVOICE DROP (
    CALC_PVP_CURRENCY
   ,CALC_PVP_NDS
   ,CALC_PVP_NOT_EXACT
   ,CALC_PVP_GAP 
); 
