﻿CREATE TABLE NDS2_MRR_USER.R$KNP_SYNC_STATUS (
	ID NUMBER NOT NULL, 
	NAME VARCHAR2(40 CHAR) NOT NULL, 
	DESCRIPTION VARCHAR2(200 CHAR) NOT NULL
) NOCOMPRESS LOGGING
TABLESPACE NDS2_DATA; 

COMMENT ON COLUMN NDS2_MRR_USER.R$KNP_SYNC_STATUS.ID IS 'Идентификатор статуса синхронизации КНП с Hive';
COMMENT ON COLUMN NDS2_MRR_USER.R$KNP_SYNC_STATUS.NAME IS 'Наименование статуса';
COMMENT ON COLUMN NDS2_MRR_USER.R$KNP_SYNC_STATUS.DESCRIPTION IS 'Описание статуса';
COMMENT ON TABLE NDS2_MRR_USER.R$KNP_SYNC_STATUS  IS 'Справочник статусов синхронизации КНП расхождений с Hive';

INSERT INTO NDS2_MRR_USER.R$KNP_SYNC_STATUS (ID, NAME, DESCRIPTION) VALUES (0, 'Нет КНП', 'КНП по Автотребованию не начата, либо не применима к этому типу АТ');
INSERT INTO NDS2_MRR_USER.R$KNP_SYNC_STATUS (ID, NAME, DESCRIPTION) VALUES (1, 'Данные КНП готовы для импорта', 'Данные КНП по Автотребованию есть, но еще не импортировались в Hive');
INSERT INTO NDS2_MRR_USER.R$KNP_SYNC_STATUS (ID, NAME, DESCRIPTION) VALUES (2, 'Данные КНП проимпортированы', 'Данные КНП по Автотребованию проимпортированы в Hive');
