﻿create table NDS2_MRR_USER.R$REQUEST_STATUS
(
  ID NUMBER not null,
  DESCRIPTION VARCHAR2(100 CHAR) not null, 
  constraint PK$R$REQUEST_STATUS primary key (ID) using index tablespace NDS2_IDX
);
