﻿-- очищаем таблицу для навешивания ограничения на новое поле
TRUNCATE TABLE NDS2_MRR_USER.SOV_INVOICE_SELLER_ALL;

-- добавляем в таблицу SOV_INVOICE_SELLER_ALL столбец с идентификатором расхождения, высчитанного по записи о СФ
ALTER TABLE NDS2_MRR_USER.SOV_INVOICE_SELLER_ALL ADD DISCREPANCY_ID NUMBER NOT NULL; 

COMMENT ON COLUMN NDS2_MRR_USER.SOV_INVOICE_SELLER_ALL.DISCREPANCY_ID IS 'Идентификатор расхождения, высчитанного по записи о СФ'; 