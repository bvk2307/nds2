﻿-- добавляем в таблицу DOC столбец с признаком обработки КНП расхождений данного АТ
ALTER TABLE NDS2_MRR_USER.DOC ADD KNP_SYNC_STATUS NUMBER(1,0) DEFAULT 0 NOT NULL; 

COMMENT ON COLUMN NDS2_MRR_USER.DOC.KNP_SYNC_STATUS IS 'Статус синхронизации данных КНП c Hive (справочник R$KNP_SYNC_STATUS)'; 