﻿-- Кол-во деклараций с открытыми КНП расхождениями по стороне покупателя
alter table NDS2_MRR_USER.EFFICIENCY_MONITOR_DATA add DECL_OPN_KNP_DIS_BUYER_CNT number(22,0) default 0 not null;

