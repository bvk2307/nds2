﻿merge into NDS2_MRR_USER.DECLARATION_ASSIGNMENT trg
using (select da.declaration_type_code
              ,da.inn_declarant
              ,da.kpp_effective
              ,da.fiscal_year
              ,da.period_effective
              ,max(da.assigned_at) as assigned_at
    from NDS2_MRR_USER.DECLARATION_ASSIGNMENT da
    group by  da.declaration_type_code
              ,da.inn_declarant
              ,da.kpp_effective
              ,da.fiscal_year
              ,da.period_effective
    ) src on (src.declaration_type_code = trg.declaration_type_code
          and src.inn_declarant         = trg.inn_declarant
          and src.kpp_effective         = trg.kpp_effective
          and src.fiscal_year           = trg.fiscal_year
          and src.period_effective      = trg.period_effective
          and src.assigned_at           = trg.assigned_at)
when matched then
  update set trg.IS_ACTUAL = 1;
commit;

