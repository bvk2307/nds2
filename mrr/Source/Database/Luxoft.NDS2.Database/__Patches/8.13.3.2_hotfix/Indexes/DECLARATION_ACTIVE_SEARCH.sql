﻿begin
  execute immediate 'create index NDS2_MRR_USER.IX_DAS_ACTORKEY on NDS2_MRR_USER.DECLARATION_ACTIVE_SEARCH(INN_CONTRACTOR, KPP_EFFECTIVE, TAX_PERIOD, FISCAL_YEAR, DECL_TYPE_CODE) tablespace NDS2_IDX';
  exception when others then null;
end;
/