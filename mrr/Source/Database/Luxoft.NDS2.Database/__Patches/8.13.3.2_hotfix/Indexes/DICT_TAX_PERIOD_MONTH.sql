﻿begin
  execute immediate 'drop index NDS2_MRR_USER.DICT_TAX_P_MON_TAX_P';
  exception when others then null;
end;
/
begin
  execute immediate 'create index NDS2_MRR_USER.IX_DTPMON_PERIOD on NDS2_MRR_USER.DICT_TAX_PERIOD_MONTH (TAX_PERIOD) tablespace NDS2_IDX';
  exception when others then null;
end;
/
