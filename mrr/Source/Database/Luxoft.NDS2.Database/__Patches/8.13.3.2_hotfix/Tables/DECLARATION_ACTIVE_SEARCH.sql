﻿begin
  execute immediate 'alter table nds2_mrr_user.declaration_active_search add join_k number';
  exception when others then null;
end;
/
begin
  execute immediate 'alter table nds2_mrr_user.declaration_active_search add partition_id number';
  exception when others then null;
end;
/
begin
  execute immediate 'alter table nds2_mrr_user.declaration_active_search add subpartition_id number';
  exception when others then null;
end;
/
begin
  execute immediate 'alter table nds2_mrr_user.declaration_active_search add LOAD_MARK number(1)';
  exception when others then null;
end;
/
--online fields
begin
  execute immediate('alter table nds2_mrr_user.declaration_active_search add KNP_CLOSE_REASON_ID number null');
  exception when others then null;
end;
/
begin
  execute immediate('alter table nds2_mrr_user.declaration_active_search add HAS_CANCELLED_CORRECTION NUMBER(1) default 0 not null');
  exception when others then null;
end;
/
begin
  execute immediate('alter table nds2_mrr_user.declaration_active_search add CANCELLED NUMBER(1) default 0 not null');
  exception when others then null;
end;
/