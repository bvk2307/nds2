﻿delete from NDS2_MRR_USER.CFG_INDEX t where t.id > 1000 and t.id < 1039;

insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values(1001, 'V$DECLARATION', 'IX_V$DDDA_ZIP_TYPE', 'DECL_TYPE_CODE,ID', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values(1002, 'V$DECLARATION', 'IX_V$DDDA_ZIP_TYPE_1', 'NDS2_DECL_CORR_ID,DECL_TYPE_CODE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values(1003, 'V$DECLARATION', 'IX_V$DDDA_REGION', 'REGION_CODE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values(1004, 'V$DECLARATION', 'IX_V$DDDA_SONO', 'SOUN_CODE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values(1005, 'V$DECLARATION', 'IX_V$DDDA_SOUN_NAME', 'SOUN_NAME', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values(1006, 'V$DECLARATION', 'IX_V$DDDA_SUBMIT_DATE', 'UPDATE_DATE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values(1007, 'V$DECLARATION', 'IX_V$DDDA_SUBMISSION_DATE', 'SUBMISSION_DATE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values(1008, 'V$DECLARATION', 'IX_V$DDDA_NDS_TOTAL', 'COMPENSATION_AMNT', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values(1009, 'V$DECLARATION', 'IX_V$DDDA_CH8_AMNT', 'DISCREP_BUY_BOOK_AMNT', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values(1010, 'V$DECLARATION', 'IX_V$DDDA_CH9_AMNT', 'DISCREP_SELL_BOOK_AMNT', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values(1011, 'V$DECLARATION', 'IX_V$DDDA_CH8_VAT', 'CH8_NDS', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values(1012, 'V$DECLARATION', 'IX_V$DDDA_CH9_VAT', 'CH9_NDS', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values(1013, 'V$DECLARATION', 'IX_V$DDDA_SUR', 'SUR_CODE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values(1014, 'V$DECLARATION', 'IX_V$DDDA_INN', 'INN', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values(1015, 'V$DECLARATION', 'IX_V$DDDA_REG_NUMBER', 'SEOD_DECL_ID', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values(1016, 'V$DECLARATION', 'IX_V$DDDA_ZIP', 'ID', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values(1017, 'V$DECLARATION', 'IX_V$DDDA_KPP', 'KPP', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values(1018, 'V$DECLARATION', 'IX_V$DDDA_NAME', 'NAME', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values(1019, 'V$DECLARATION', 'IX_V$DDDA_ZIP_FULL', 'KPP_EFFECTIVE,FISCAL_YEAR,TAX_PERIOD,DECL_TYPE_CODE,INN,ID,INN_CONTRACTOR', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values(1020, 'V$DECLARATION', 'IX_V$DDDA_REG_NAME', 'REGION_NAME', 'NONUNIQUE');

insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values(1021, 'V$INSPECTOR_DECLARATION', 'IX_V$IDA_NAME', 'NAME', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values(1022, 'V$INSPECTOR_DECLARATION', 'IX_V$IDA_REGION', 'REGION_CODE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values(1023, 'V$INSPECTOR_DECLARATION', 'IX_V$IDA_SONO', 'SOUN_CODE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values(1024, 'V$INSPECTOR_DECLARATION', 'IX_V$IDA_NDS_TOTAL', 'COMPENSATION_AMNT', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values(1025, 'V$INSPECTOR_DECLARATION', 'IX_V$IDA_CH8_VAT', 'NDS_CHAPTER8', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values(1026, 'V$INSPECTOR_DECLARATION', 'IX_V$IDA_CH9_VAT', 'NDS_CHAPTER9', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values(1027, 'V$INSPECTOR_DECLARATION', 'IX_V$IDA_SUR', 'SUR_CODE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values(1028, 'V$INSPECTOR_DECLARATION', 'IX_V$IDA_INN', 'INN', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values(1029, 'V$INSPECTOR_DECLARATION', 'IX_V$IDA_REG_NUMBER', 'SEOD_DECL_ID', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values(1030, 'V$INSPECTOR_DECLARATION', 'IX_V$IDA_ZIP', 'ID', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values(1031, 'V$INSPECTOR_DECLARATION', 'IX_V$IDA_ZIP_TYPE', 'ID,DECL_TYPE_CODE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values(1032, 'V$INSPECTOR_DECLARATION', 'IX_V$IDA_ZIP_TYPE_1', 'NDS2_DECL_CORR_ID,DECL_TYPE_CODE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values(1033, 'V$INSPECTOR_DECLARATION', 'IX_V$IDA_ZIP_FULL', 'ID,INN_CONTRACTOR,INN,KPP_EFFECTIVE,FISCAL_YEAR,TAX_PERIOD,DECL_TYPE_CODE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values(1034, 'V$INSPECTOR_DECLARATION', 'IX_V$IDA_REG_NAME', 'REGION_NAME', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values(1035, 'V$INSPECTOR_DECLARATION', 'IX_V$IDA_SOUN_NAME', 'SOUN_NAME', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values(1036, 'V$INSPECTOR_DECLARATION', 'IX_V$IDA_SUBMIT_DATE', 'DECL_DATE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values(1037, 'V$INSPECTOR_DECLARATION', 'IX_V$IDA_SUBMISSION_DATE', 'SUBMISSION_DATE', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values(1038, 'V$INSPECTOR_DECLARATION', 'IX_V$IDA_KPP', 'KPP', 'NONUNIQUE');

delete from NDS2_MRR_USER.CFG_INDEX t where t.id >= 240 and t.id <= 243;

insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values(240, 'MRR_USER', 'IX_MRRUSER_ID', 'ID', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values(241, 'SELECTION', 'IX_SEL_TEMPL_ID', 'TEMPLATE_ID', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values(242, 'SELECTION', 'IX_SEL_ID_TEMPL_ID', 'ID,TEMPLATE_ID', 'NONUNIQUE');
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type)
values(243, 'SELECTION_AGGREGATE', 'IX_SELAG_ID_STATUS', 'ID,STATUS_ID', 'NONUNIQUE');

delete from NDS2_MRR_USER.CFG_INDEX t where t.id > 1100 and t.id < 1120;

insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type, op_parallel_count, is_local)
values(1101, 'DECLARATION_ACTIVE_SEARCH', 'IX_DAS_SONOSUR_L', 'FISCAL_YEAR, TAX_PERIOD, SOUN_CODE, SUR_CODE', 'NONUNIQUE', 8, 1);
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type, op_parallel_count, is_local)
values(1102, 'DECLARATION_ACTIVE_SEARCH', 'IX_DAS_SONO_L', 'FISCAL_YEAR, TAX_PERIOD, SOUN_CODE', 'NONUNIQUE', 8, 1);
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type, op_parallel_count, is_local)
values(1103, 'DECLARATION_ACTIVE_SEARCH', 'IX_DAS_SONOINN_L', 'FISCAL_YEAR, TAX_PERIOD, SOUN_CODE,INN', 'NONUNIQUE', 8, 1);
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type, op_parallel_count, is_local)
values(1104, 'DECLARATION_ACTIVE_SEARCH', 'IX_DAS_SUR_L', 'FISCAL_YEAR, TAX_PERIOD, SUR_CODE', 'NONUNIQUE', 8, 1);
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type, op_parallel_count, is_local)
values(1105, 'DECLARATION_ACTIVE_SEARCH', 'IX_DAS_INN_L', 'FISCAL_YEAR, TAX_PERIOD, INN', 'NONUNIQUE', 8, 1);
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type, op_parallel_count, is_local)
values(1106, 'DECLARATION_ACTIVE_SEARCH', 'IX_DAS_NDSW_L', 'FISCAL_YEAR, TAX_PERIOD, NDS_WEIGHT', 'NONUNIQUE', 8, 1);
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type, op_parallel_count, is_local)
values(1107, 'DECLARATION_ACTIVE_SEARCH', 'IX_DAS_TPS_L', 'FISCAL_YEAR, TAX_PERIOD, TAX_PERIOD_SORT_ORDER', 'NONUNIQUE', 8, 1);
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type, op_parallel_count, is_local)
values(1108, 'DECLARATION_ACTIVE_SEARCH', 'IX_DAS_SIGN_L', 'FISCAL_YEAR, TAX_PERIOD, DECL_SIGN', 'NONUNIQUE', 8, 1);
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type, op_parallel_count, is_local)
values(1109, 'DECLARATION_ACTIVE_SEARCH', 'IX_DAS_NDS_L', 'FISCAL_YEAR, TAX_PERIOD, COMPENSATION_AMNT', 'NONUNIQUE', 8, 1);



commit;
