﻿drop view v_nds10;
create view v_nds10
as
select 
 rowkey as row_key
,rowkey_zip as DECLARATION_VERSION_ID
,rowkey_type
,rowkey_num
,rowkey_type as CHAPTER
,1000000000000 * cast(invoice_date/100 as bigint) +cast(invoice_sellerinn as bigint) as contractor_key
,cast((case when match is null then 0 else 1 end) as int) as IS_MATCHING_ROW
,match as COMPARE_ROW_KEY
,action_date as CREATE_DATE 
,rownum as ORDINAL_NUMBER
,op_kinds as OPERATION_CODE
,invoice_no as INVOICE_NUM
,invoice_date as INVOICE_DATE
,correction_fixno as CHANGE_NUM
,correction_fixdate as CHANGE_DATE
,correction_corrno as CORRECTION_NUM
,correction_corrdate as CORRECTION_DATE
,correction_corrfixno as CHANGE_CORRECTION_NUM
,correction_corrfixdate as CHANGE_CORRECTION_DATE
,invoice_sellerinn as SELLER_INN
,invoice_sellerkpp as SELLER_KPP
,invoice_buyerinn as BUYER_INN
,invoice_buyerkpp as BUYER_KPP
,invoice2_sellerinn as SELLER_AGENCY_INFO_INN
,invoice2_sellerkpp as SELLER_AGENCY_INFO_KPP
,invoice2_no as SELLER_AGENCY_INFO_NUM
,invoice2_date as SELLER_AGENCY_INFO_DATE
,amounts_currencycode as OKV_CODE
,invoice_sum as PRICE_TOTAL
,amounts_vat as PRICE_NDS_TOTAL
,correction_sumdiffdec as DIFF_CORRECT_DECREASE
,correction_sumdiffinc as DIFF_CORRECT_INCREASE
,correction_vatdiffdec as DIFF_CORRECT_NDS_DECREASE
,correction_vatdiffinc as DIFF_CORRECT_NDS_INCREASE
,partzip
from target_nds10 where not rowkey_agg;
