﻿drop view v_nds8;
create view v_nds8 as 
select
 rowkey as row_key
,rowkey_zip as DECLARATION_VERSION_ID
,rowkey_type
,rowkey_num
,1000000000000 * cast(invoice_date/100 as bigint) +cast(invoice_sellerinn as bigint) as contractor_key
,cast((case when match is null then 0 else 1 end) as int) as IS_MATCHING_ROW
,match as COMPARE_ROW_KEY
,rowkey_type as CHAPTER
,rownum as ORDINAL_NUMBER
,op_kinds as OPERATION_CODE
,invoice_no as INVOICE_NUM
,invoice_date as INVOICE_DATE
,correction_fixno as CHANGE_NUM
,correction_fixdate as CHANGE_DATE
,correction_corrno as CORRECTION_NUM
,correction_corrdate as CORRECTION_DATE
,correction_corrfixno as CHANGE_CORRECTION_NUM
,correction_corrfixdate as CHANGE_CORRECTION_DATE
,payment_docs as RECEIPT_DOC_DATE_STR
,action_date as BUY_ACCEPT_DATE
,invoice_sellerinn as SELLER_INN_RESOLVED
,invoice_sellerinn as SELLER_INN
,invoice_sellerkpp as SELLER_KPP
,invoice_agentkpp as BROKER_KPP 
,invoice_agentinn as BROKER_INN
,customs_declar as CUSTOMS_DECLARATION_NUM
,invoice_sum as PRICE_BUY_AMOUNT
,amounts_vat as PRICE_BUY_NDS_AMOUNT
,case when rowkey_type = 2 then 1 else 0 end as IS_DOP_LIST
,amounts_currencycode as OKV_CODE
,partzip
from target_nds8 where not rowkey_agg;
