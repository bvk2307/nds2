﻿drop view v_nds9;
create view v_nds9
as
select 
 rowkey as row_key
,rowkey_zip as DECLARATION_VERSION_ID
,rowkey_type
,rowkey_num
,1000000000000 * cast(invoice_date/100 as bigint) +cast(invoice_buyerinn as bigint) as contractor_key
,cast((case when match is null then 0 else 1 end) as int) as IS_MATCHING_ROW
,match as COMPARE_ROW_KEY
,rowkey_type as CHAPTER
,rownum as ORDINAL_NUMBER
,op_kinds as OPERATION_CODE
,invoice_no as INVOICE_NUM
,invoice_date as INVOICE_DATE
,correction_fixno as CHANGE_NUM
,correction_fixdate as CHANGE_DATE
,correction_corrno as CORRECTION_NUM
,correction_corrdate as CORRECTION_DATE
,correction_corrfixno as CHANGE_CORRECTION_NUM
,correction_corrfixdate as CHANGE_CORRECTION_DATE
,payment_docs as RECEIPT_DOC_DATE_STR
,action_date as BUY_ACCEPT_DATE
,invoice_buyerinn as BUYER_INN
,invoice_buyerkpp as BUYER_KPP
,invoice_agentinn as BROKER_INN
,invoice_agentkpp as BROKER_KPP
,amounts_currencycode as OKV_CODE
,amounts_sumcurr as PRICE_SELL_IN_CURR
,amounts_sumrub as PRICE_SELL
,amounts_sum18 as PRICE_SELL_18
,amounts_sum10 as PRICE_SELL_10
,amounts_sum0 as PRICE_SELL_0
,amounts_vat as PRICE_NDS_18
,amounts_vat10 as PRICE_NDS_10
,customs_declar as CUSTOMS_DECLARATION_NUM
,invoice_sum as PRICE_BUY_AMOUNT
,amounts_vat as PRICE_BUY_NDS_AMOUNT
,amounts_sumnovat as PRICE_TAX_FREE
,case when rowkey_type = 3 then 1 else 0 end as IS_DOP_LIST
,partzip
from target_nds9 where not rowkey_agg;
