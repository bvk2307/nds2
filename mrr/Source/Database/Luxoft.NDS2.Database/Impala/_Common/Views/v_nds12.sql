﻿drop view v_nds12;
create  view v_nds12
as
select 
 rowkey as row_key
,rowkey_zip as DECLARATION_VERSION_ID
,rowkey_type
,rowkey_num
,1000000000000 * cast(invoice_date/100 as bigint) +cast(invoice_sellerinn as bigint) as contractor_key
,cast((case when match is null then 0 else 1 end) as int) as IS_MATCHING_ROW
,match as COMPARE_ROW_KEY
,rowkey_type as CHAPTER
,invoice_no as INVOICE_NUM
,invoice_date as SELLER_INVOICE_DATE
,amounts_currencycode as OKV_CODE
,invoice_sum as PRICE_BUY_AMOUNT
,amounts_vat as PRICE_BUY_NDS_AMOUNT
,amounts_sumnovat as PRICE_TAX_FREE
,partzip
from target_nds12 where not rowkey_agg;
