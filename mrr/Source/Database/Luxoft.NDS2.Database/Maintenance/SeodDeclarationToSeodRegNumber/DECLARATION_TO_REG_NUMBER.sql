﻿begin 
  NDS2_MRR_USER.NDS2$SYS.LOG_INFO('Migration_RegNumbers', 1, 1, 'Start migration');
  NDS2_MRR_USER.NDS2$SYS.LOG_INFO('Migration_RegNumbers', 1, 1, 'Drop SEQ_SEOD_REG_NUMBER');
  execute immediate 'drop sequence NDS2_MRR_USER.SEQ_SEOD_REG_NUMBER';
  exception when others then null;
end;
/
begin 
  NDS2_MRR_USER.NDS2$SYS.LOG_INFO('Migration_RegNumbers', 1, 1, 'Create SEQ_SEOD_REG_NUMBER');
end;
/
create sequence NDS2_MRR_USER.SEQ_SEOD_REG_NUMBER
start with 1
increment by 1 
minvalue 1
maxvalue 99999999999999
nocache
nocycle;

begin 
  NDS2_MRR_USER.NDS2$SYS.LOG_INFO('Migration_RegNumbers', 1, 1, 'Drop SEOD_REG_NUMBER');
  execute immediate 'drop table NDS2_MRR_USER.SEOD_REG_NUMBER';
  exception when others then null;
end;
/

begin 
  NDS2_MRR_USER.NDS2$SYS.LOG_INFO('Migration_RegNumbers', 1, 1, 'Create SEOD_REG_NUMBER');
end;
/

CREATE TABLE NDS2_MRR_USER.SEOD_REG_NUMBER 
(
  "ID"               NUMBER                     NOT NULL,
  DECL_REG_NUM       NUMBER(20)                 NOT NULL,
  INN                VARCHAR2(12 CHAR)          NOT NULL,
  TAX_PERIOD         VARCHAR2(2 CHAR)           NOT NULL,
  FISCAL_YEAR        VARCHAR2(4 CHAR)           NOT NULL,
  SONO_CODE          VARCHAR2(4 CHAR)           NOT NULL,
  CORRECTION_NUMBER  NUMBER(5)                  NOT NULL,
  "TYPE"             NUMBER(1)                  NOT NULL,
  ID_FILE            VARCHAR2(1024 CHAR),
  SUBMISSION_DATE    DATE                       NOT NULL,
  EOD_DATE           DATE                       NOT NULL,
  INSERT_DATE        DATE                       NOT NULL
);

ALTER TABLE NDS2_MRR_USER.SEOD_REG_NUMBER ADD CONSTRAINT PK_SEOD_REG_NUMBER PRIMARY KEY (ID)
USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
TABLESPACE NDS2_IDX ENABLE;

CREATE UNIQUE INDEX NDS2_MRR_USER.IDX_UNIQ_SEOD_REG_NUMBER ON NDS2_MRR_USER.SEOD_REG_NUMBER (INN, TAX_PERIOD, FISCAL_YEAR, SONO_CODE, CORRECTION_NUMBER, ID_FILE, TYPE) 
PCTFREE 10 INITRANS 2 MAXTRANS 161 COMPUTE STATISTICS 
TABLESPACE NDS2_IDX ;

CREATE INDEX NDS2_MRR_USER.IDX_DECL_REG_NUM_SONO_CODE ON NDS2_MRR_USER.SEOD_REG_NUMBER (DECL_REG_NUM, SONO_CODE) 
PCTFREE 10 INITRANS 2 MAXTRANS 161 COMPUTE STATISTICS 
TABLESPACE NDS2_IDX ;


begin 
  NDS2_MRR_USER.NDS2$SYS.LOG_INFO('Migration_RegNumbers', 1, 1, 'Drop SEQ_SEOD_REG_NUMBER_CANCELLED');
  execute immediate 'drop sequence NDS2_MRR_USER.SEQ_SEOD_REG_NUMBER_CANCELLED';
  exception when others then null;
end;
/

begin 
  NDS2_MRR_USER.NDS2$SYS.LOG_INFO('Migration_RegNumbers', 1, 1, 'Create SEQ_SEOD_REG_NUMBER_CANCELLED');
end;
/

create sequence NDS2_MRR_USER.SEQ_SEOD_REG_NUMBER_CANCELLED
start with 1
increment by 1 
minvalue 1
maxvalue 99999999999999
nocache
nocycle;

begin 
  NDS2_MRR_USER.NDS2$SYS.LOG_INFO('MIgration_RegNumbers', 1, 1, 'Drop SEOD_REG_NUMBER_CANCELLED');
  execute immediate 'drop table NDS2_MRR_USER.SEOD_REG_NUMBER_CANCELLED';
  exception when others then null;
end;
/

begin 
  NDS2_MRR_USER.NDS2$SYS.LOG_INFO('Migration_RegNumbers', 1, 1, 'Create SEOD_REG_NUMBER_CANCELLED');
end;
/

CREATE TABLE NDS2_MRR_USER.SEOD_REG_NUMBER_CANCELLED 
(
  "ID"               NUMBER                     NOT NULL,
  SONO_CODE          VARCHAR2(4 CHAR)           NOT NULL,
  DECL_REG_NUM       NUMBER(20)                 NOT NULL,
  NEW_DECL_REG_NUM   NUMBER(20)                 NOT NULL,
  CANCELLED_AT       DATE                       NOT NULL
);

ALTER TABLE NDS2_MRR_USER.SEOD_REG_NUMBER_CANCELLED ADD CONSTRAINT PK_SEOD_REG_NUMBER_CANCELLED PRIMARY KEY (ID)
USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
TABLESPACE NDS2_IDX  ENABLE;

begin 
  NDS2_MRR_USER.NDS2$SYS.LOG_INFO('MIgration_RegNumbers', 1, 1, 'Drop CLAIMS_FOR_REGNUM_MIGRATION');
  execute immediate 'drop table NDS2_MRR_USER.CLAIMS_FOR_REGNUM_MIGRATION';
  exception when others then null;
end;
/

begin 
  NDS2_MRR_USER.NDS2$SYS.LOG_INFO('Migration_RegNumbers', 1, 1, 'Create CLAIMS_FOR_REGNUM_MIGRATION');
end;
/

CREATE TABLE NDS2_MRR_USER.CLAIMS_FOR_REGNUM_MIGRATION 
(
  DOC_ID             NUMBER                     NOT NULL,
  CREATE_DATE        DATE                       NOT NULL,
  SONO_CODE          VARCHAR2(4 CHAR)           NOT NULL,
  DECL_REG_NUM       NUMBER(20)                 NOT NULL,
  NEW_DECL_REG_NUM   NUMBER(20)                 NOT NULL
);


DECLARE 
TYPE SeodRegNumberType IS RECORD (
  DECL_REG_NUM      NUMBER(20)
 ,INN               VARCHAR2(12 CHAR) 
 ,TAX_PERIOD        VARCHAR2(2 CHAR)
 ,FISCAL_YEAR       VARCHAR2(4 CHAR) 
 ,SONO_CODE         VARCHAR2(4 CHAR)
 ,CORRECTION_NUMBER NUMBER(5)
 ,TYPE              NUMBER(1)
 ,ID_FILE           VARCHAR2(1024 CHAR)
 ,SUBMISSION_DATE   DATE
 ,EOD_DATE          DATE
 ,INSERT_DATE       DATE);
TYPE SeodRegNumberTable IS TABLE OF SeodRegNumberType INDEX BY BINARY_INTEGER;
SeodRegNumberRec SeodRegNumberTable;
v_decl_reg_num number(20);
v_reg_num_date date;
f_decl_reg_num number(20);
r_exist number(1);

CURSOR RegNumberSingle IS 
select max(DECL_REG_NUM) as DECL_REG_NUM, INN, TAX_PERIOD, FISCAL_YEAR, SONO_CODE, CORRECTION_NUMBER, TYPE, ID_FILE
 ,min(nvl(eod_date, sysdate)) as EOD_DATE
 ,min(nvl(SUBMISSION_DATE, nvl(EOD_DATE, nvl(INSERT_DATE, sysdate)))) as SUBMISSION_DATE
 ,min(INSERT_DATE) as INSERT_DATE
from NDS2_MRR_USER.seod_declaration
group by INN, TAX_PERIOD, FISCAL_YEAR, SONO_CODE, CORRECTION_NUMBER, TYPE, ID_FILE 
having count(*) = 1;

BEGIN
  

  NDS2_MRR_USER.NDS2$SYS.LOG_INFO('Migration_RegNumbers', 1, 1, 'Migrate single RegNumbers beginning');

  OPEN RegNumberSingle;
  LOOP
    FETCH RegNumberSingle BULK COLLECT INTO SeodRegNumberRec LIMIT 50000;

    FORALL i IN SeodRegNumberRec.FIRST..SeodRegNumberRec.LAST
    INSERT INTO NDS2_MRR_USER.SEOD_REG_NUMBER (ID, DECL_REG_NUM, INN, TAX_PERIOD, FISCAL_YEAR, SONO_CODE, 
        CORRECTION_NUMBER, TYPE, ID_FILE, SUBMISSION_DATE, EOD_DATE, INSERT_DATE)
    VALUES (NDS2_MRR_USER.SEQ_SEOD_REG_NUMBER.NEXTVAL, SeodRegNumberRec(i).DECL_REG_NUM,
    SeodRegNumberRec(i).INN, SeodRegNumberRec(i).TAX_PERIOD, SeodRegNumberRec(i).FISCAL_YEAR,
    SeodRegNumberRec(i).SONO_CODE, SeodRegNumberRec(i).CORRECTION_NUMBER, SeodRegNumberRec(i).TYPE,
    SeodRegNumberRec(i).ID_FILE, SeodRegNumberRec(i).SUBMISSION_DATE, SeodRegNumberRec(i).EOD_DATE,
    SeodRegNumberRec(i).INSERT_DATE);
    
    commit;
    
    EXIT WHEN RegNumberSingle%NOTFOUND;
    NDS2_MRR_USER.NDS2$SYS.LOG_INFO('Migration_RegNumbers', 1, 1, 'Migrate count ' || SeodRegNumberRec.LAST);
  END LOOP;
  
  NDS2_MRR_USER.NDS2$SYS.LOG_INFO('Migration_RegNumbers', 1, 1, 'Migrate NOT single RegNumbers ending');
  CLOSE RegNumberSingle;
  
  NDS2_MRR_USER.NDS2$SYS.LOG_INFO('Migration_RegNumbers', 1, 1, 'Migrate NOT single RegNumbers beginning');
  
  FOR line IN (  
  select INN, TAX_PERIOD, FISCAL_YEAR, SONO_CODE, CORRECTION_NUMBER, TYPE, ID_FILE 
  from NDS2_MRR_USER.SEOD_DECLARATION
  group by INN, TAX_PERIOD, FISCAL_YEAR, SONO_CODE, CORRECTION_NUMBER, TYPE, ID_FILE 
  having count(*) > 1)
  LOOP
    NDS2_MRR_USER.NDS2$SYS.LOG_INFO('Migration_RegNumbers', 1, 1, 'Migrate RegNumbers for INN=' || line.INN || ', TAX_PERIOD=' || line.TAX_PERIOD || ', FISCAL_YEAR=' || line.FISCAL_YEAR || ', ID_FILE=' || line.ID_FILE);
    FOR dbl IN (
    select row_number() over (order by DECL_REG_NUM, INSERT_DATE desc) as RWNUM, INN, TAX_PERIOD, FISCAL_YEAR, SONO_CODE, CORRECTION_NUMBER, 
      TYPE, ID_FILE, DECL_REG_NUM, nvl(SUBMISSION_DATE, nvl(EOD_DATE, nvl(INSERT_DATE, sysdate))) as SUBMISSION_DATE, eod_date, INSERT_DATE
    from NDS2_MRR_USER.SEOD_DECLARATION
    where INN = line.INN and TAX_PERIOD = line.TAX_PERIOD and FISCAL_YEAR = line.FISCAL_YEAR and SONO_CODE = line.SONO_CODE
    and CORRECTION_NUMBER = line.CORRECTION_NUMBER and TYPE = line.TYPE and nvl(ID_FILE, '') = nvl(line.ID_FILE, '')
    order by row_number() over (order by DECL_REG_NUM, INSERT_DATE desc) asc
    )
    LOOP
      if (dbl.RWNUM = 1) then
      begin
        INSERT INTO NDS2_MRR_USER.SEOD_REG_NUMBER (ID, DECL_REG_NUM, INN, TAX_PERIOD, FISCAL_YEAR, SONO_CODE, 
          CORRECTION_NUMBER, TYPE, ID_FILE, SUBMISSION_DATE, EOD_DATE, INSERT_DATE)
        VALUES (NDS2_MRR_USER.SEQ_SEOD_REG_NUMBER.NEXTVAL, dbl.DECL_REG_NUM,
          dbl.INN, dbl.TAX_PERIOD, dbl.FISCAL_YEAR, dbl.SONO_CODE, dbl.CORRECTION_NUMBER, dbl.TYPE,
          dbl.ID_FILE, dbl.SUBMISSION_DATE, dbl.EOD_DATE, dbl.INSERT_DATE);
          
        f_decl_reg_num := dbl.DECL_REG_NUM;

      end;
      else
      begin
        if f_decl_reg_num != dbl.DECL_REG_NUM then
        begin
          INSERT INTO NDS2_MRR_USER.SEOD_REG_NUMBER_CANCELLED (ID, SONO_CODE, DECL_REG_NUM, NEW_DECL_REG_NUM, CANCELLED_AT)
          VALUES (NDS2_MRR_USER.SEQ_SEOD_REG_NUMBER_CANCELLED.NEXTVAL, dbl.SONO_CODE, dbl.DECL_REG_NUM, v_decl_reg_num, v_reg_num_date);

          select case when exists(select REF_ENTITY_ID from NDS2_MRR_USER.doc where REF_ENTITY_ID = dbl.DECL_REG_NUM
          and SONO_CODE = dbl.SONO_CODE and INN = dbl.INN) then 1 else 0 end into r_exist from dual;
          if (r_exist = 1) then      
            INSERT INTO NDS2_MRR_USER.CLAIMS_FOR_REGNUM_MIGRATION 
            (
              DOC_ID,
              CREATE_DATE,
              SONO_CODE,
              DECL_REG_NUM,
              NEW_DECL_REG_NUM
            ) select doc_id 
                    ,create_date
                    ,sono_code
                    ,ref_entity_id
                    ,v_decl_reg_num
                from NDS2_MRR_USER.doc 
                where REF_ENTITY_ID = dbl.DECL_REG_NUM and SONO_CODE = dbl.SONO_CODE and INN = dbl.INN; 
          end if;
      
          select case when exists(select DECLARATION_REG_NUM from NDS2_MRR_USER.seod_knp where DECLARATION_REG_NUM = dbl.DECL_REG_NUM
          and IFNS_CODE = dbl.SONO_CODE) then 1 else 0 end into r_exist from dual;
          if (r_exist = 1) then
            update NDS2_MRR_USER.seod_knp set
              DECLARATION_REG_NUM = f_decl_reg_num
            where DECLARATION_REG_NUM = dbl.DECL_REG_NUM and IFNS_CODE = dbl.SONO_CODE;
          end if; 

        end;
        end if;
        
      end;
      end if;

      v_decl_reg_num := dbl.DECL_REG_NUM;
      v_reg_num_date := nvl(dbl.INSERT_DATE, sysdate);
    
    END LOOP;

    commit;
    
  END LOOP;
  NDS2_MRR_USER.NDS2$SYS.LOG_INFO('Migration_RegNumbers', 1, 1, 'End migration');
END;
/
