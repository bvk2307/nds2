begin
  dbms_stats.gather_table_stats(
    ownname => 'NDS2_MRR_USER',
    tabname => 'DECLARATION_HISTORY',
    cascade => true);
  dbms_stats.gather_table_stats(
    ownname => 'NDS2_MRR_USER',
    tabname => 'V$ASK_DECLANDJRNL',
    cascade => true);
  dbms_stats.gather_table_stats(
    ownname => 'NDS2_MRR_USER',
    tabname => 'MV$TAX_PAYER_NAME',
    cascade => true);
  dbms_stats.gather_table_stats(
    ownname => 'NDS2_MRR_USER',
    tabname => 'DOC',
    cascade => true);
  dbms_stats.gather_table_stats(
    ownname => 'NDS2_MRR_USER',
    tabname => 'DOC_INVOICE',
    cascade => true);
  dbms_stats.gather_table_stats(
    ownname => 'NDS2_MRR_USER',
    tabname => 'SOV_INVOICE_BUY_ACCEPT',
    cascade => true);
  dbms_stats.gather_table_stats(
    ownname => 'NDS2_MRR_USER',
    tabname => 'SOV_INVOICE',
    cascade => true);
end;
/
