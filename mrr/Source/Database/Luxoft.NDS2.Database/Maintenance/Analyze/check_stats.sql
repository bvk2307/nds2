select
  ut.TABLE_NAME, ut.NUM_ROWS, ut.LAST_ANALYZED
from user_tables ut
where ut.LAST_ANALYZED >= trunc(sysdate)
order by ut.LAST_ANALYZED desc;
