select
  cfg.id,
  nvl(cfg.table_name, ui.table_name),
  nvl(cfg.index_name, ui.INDEX_NAME),
  nvl(cfg.column_list, ui.INCLUDE_COLUMN),
ui.status from cfg_index cfg
full join user_indexes ui on ui.table_name = cfg.table_name and ui.INDEX_NAME = cfg.index_name and ui.TABLE_OWNER = 'NDS2_MRR_USER'
where ui.status is NULL or cfg.id is NULL
order by ui.status desc, cfg.table_name
