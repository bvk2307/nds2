begin
  dbms_stats.gather_table_stats(
    ownname => 'NDS2_MRR_USER',
    tabname => 'DECLARATION_HISTORY');
  dbms_stats.gather_table_stats(
    ownname => 'NDS2_MRR_USER',
    tabname => 'V$ASK_DECLANDJRNL');
  dbms_stats.gather_table_stats(
    ownname => 'NDS2_MRR_USER',
    tabname => 'MV$TAX_PAYER_NAME');
  dbms_stats.gather_table_stats(
    ownname => 'NDS2_MRR_USER',
    tabname => 'DOC');
  dbms_stats.gather_table_stats(
    ownname => 'NDS2_MRR_USER',
    tabname => 'DOC_INVOICE');
  dbms_stats.gather_table_stats(
    ownname => 'NDS2_MRR_USER',
    tabname => 'SOV_INVOICE_BUY_ACCEPT');
  dbms_stats.gather_table_stats(
    ownname => 'NDS2_MRR_USER',
    tabname => 'SOV_INVOICE');
end;
/
