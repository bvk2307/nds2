create public database link NDS2_PROD
CONNECT TO NDS2_MRR_USER IDENTIFIED BY NDS2_MRR_USER
using '(DESCRIPTION =
      (ADDRESS_LIST =
         (ADDRESS = (PROTOCOL = TCP)(HOST = m9965-ais097)(PORT = 1521))
      )
      (CONNECT_DATA =
         (SERVER = DEDICATED)
         (SERVICE_NAME = nds2))
   )';
