﻿create tablespace nds2_data datafile 'nds2_data.tbs' size 256M autoextend on next 64M;
create tablespace nds2_idx datafile 'nds2_idx.tbs' size 256M autoextend on next 64M;
create tablespace nds2_mc_data datafile 'nds2_mc_data.tbs' size 256M autoextend on next 64M;
create tablespace nds2_mc_idx datafile 'nds2_mc_idx.tbs' size 256M autoextend on next 64M;
create tablespace mdm datafile 'mdm.tbs' size 64M autoextend on next 64M;
