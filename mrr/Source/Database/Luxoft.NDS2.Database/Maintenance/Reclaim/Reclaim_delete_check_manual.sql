﻿-- Создание временной таблицы RECLAIM_QUEUE_DOC_REMOVED_LOG
declare
  p_exists_table number(1) := null;
begin
  select max(1) into p_exists_table from user_tables where table_name = 'RECLAIM_QUEUE_DOC_REMOVED_LOG';

  if p_exists_table is null then
    begin
       EXECUTE IMMEDIATE 'CREATE TABLE NDS2_MRR_USER.RECLAIM_QUEUE_DOC_REMOVED_LOG(doc_id number not null, queue_item_id number, log_date date not null)';
    exception when others then null;
    end;
  end if;
end;
/

-- Удаление АИ в статусе 11 - "Ручная проверка"
declare
  p_doc_ids T$TABLE_OF_NUMBER;
begin

  SELECT m.doc_id BULK COLLECT INTO p_doc_ids
  FROM
  (
    select d.doc_id 
    from doc d 
    where d.doc_type = 2 and d.status = 11
          -- Задается код НО (d.sono_code) и список рег. номеров (d.ref_entity_id)
          and d.sono_code = 'XXXX' and d.ref_entity_id in (99999999999999999)
  ) m; 

  insert into NDS2_MRR_USER.RECLAIM_QUEUE_DOC_REMOVED_LOG
  select d.doc_id, d.queue_item_id, sysdate from NDS2_MRR_USER.doc d
  where d.doc_id in (select * from table(p_doc_ids));
  
  delete from NDS2_MRR_USER.DOC_DISCREPANCY t
  where t.doc_id in (select * from table(p_doc_ids));

  delete from NDS2_MRR_USER.doc_invoice t
  where t.doc_id in (select * from table(p_doc_ids));

  delete from NDS2_MRR_USER.doc_invoice_buyer t
  where t.doc_id in (select * from table(p_doc_ids));

  delete from NDS2_MRR_USER.doc_invoice_buy_accept t
  where t.doc_id in (select * from table(p_doc_ids));

  delete from NDS2_MRR_USER.doc_invoice_operation t
  where t.doc_id in (select * from table(p_doc_ids));

  delete from NDS2_MRR_USER.doc_invoice_payment_doc t
  where t.doc_id in (select * from table(p_doc_ids));

  delete from NDS2_MRR_USER.doc_invoice_seller t
  where t.doc_id in (select * from table(p_doc_ids));

  delete from NDS2_MRR_USER.doc d
  where d.doc_id in (select * from table(p_doc_ids));

  commit;

end;
/