﻿select * from
(
  select
    (decode(d.sono_code, '9901', '50', substr(d.sono_code, 1, 2)) || '00') 
        as sono_ufns                                          -- УФНС
    ,d.sono_code                                              -- Код НО
    ,d.doc_id                                                 -- № АТ
    ,d.doc_kind as side                                       -- Сторона АТ
    ,d.inn                                                    -- ИНН
    ,dis.all_cnt as claim_all_cnt                             -- Кол-во расхождений всего
    ,dis.all_amnt as claim_all_amnt                           -- Сумма расхождений всего
    ,f.all_contr_cnt as claim_all_contr_cnt                   -- Кол-во контрагентов всего
    ,dis.open_cnt as claim_open_cnt                           -- Кол-во открытых расхождений в АТ
    ,dis.open_amnt as claim_open_amnt                         -- Сумма открытых расхождений в АТ
    ,f.open_contr_cnt as claim_open_contr_cnt                 -- Кол-во контрагентов по открытым расхождениям в АТ
    ,r.all_dis_cnt as reclaim_cnt                             -- Кол-во расхождений попавших в АИ
    ,r.all_dis_amnt as reclaim_amnt                           -- Сумма расхождений по всем АИ
    ,r.all_doc_cnt as reclaim_doc_cnt                         -- Кол-во АИ
    ,k.all_contr_cnt as reclaim_all_contr_cnt                 -- Кол-во контрагентов в АИ             
    ,abs(dis.open_cnt - r.all_dis_cnt) delta_cnt              -- Разница по кол-ву расхождений
    ,abs(dis.open_amnt - r.all_dis_amnt) delta_amnt           -- Разница по сумме 
    ,abs(f.open_contr_cnt - k.all_contr_cnt) delta_contr_cnt  -- Разница по кол-ву контрагентов
  from doc d
  -- кол-во и сумма расхождений (всего и открытых) в АТ
  left join
  (
       select
            dd.doc_id
           ,count(dd.discrepancy_id) as all_cnt
           ,sum(nvl(dd.amount, 0)) as all_amnt
           ,count(case when sov.status = 1 then dd.discrepancy_id end) as open_cnt
           ,sum(case when sov.status = 1 then nvl(dd.amount, 0) else 0 end) as open_amnt 
       from doc_discrepancy dd
       join sov_discrepancy sov on sov.id = dd.discrepancy_id
       group by dd.doc_id
  ) dis on dis.doc_id = d.doc_id
  -- кол-во контрагентов в АТ
  left join
  (
    select m.doc_id
           ,count(1) as all_contr_cnt
           ,sum(case when m.dis_status = 1 then 1 else 0 end) as open_contr_cnt
    from
    (
      select m.doc_id, m.dis_status, m.contr_inn, m.contr_kpp_effective from
      (
        select
             d.doc_id
            ,sov.status as dis_status
            ,case when d.doc_kind = 1 then sov.seller_inn 
                  when d.doc_kind = 2 then sov.buyer_inn
                  end as contr_inn
            ,case when d.doc_kind = 1 then
                    case when ask_seller.kpp_effective is null then '111111111' 
                    else ask_seller.kpp_effective end 
                  when d.doc_kind = 2 then  
                    case when ask_buyer.kpp_effective is null then '111111111' 
                    else ask_buyer.kpp_effective end 
                  end as contr_kpp_effective
        from doc d 
        join doc_discrepancy sdis on sdis.doc_id = d.doc_id
        join sov_discrepancy sov on sdis.discrepancy_id = sov.id
        left join ask_declandjrnl ask_seller on ask_seller.zip = sov.seller_zip
        left join ask_declandjrnl ask_buyer on ask_buyer.zip = sov.buyer_zip
        where d.doc_type = 1 
      ) m
      group by m.doc_id, m.dis_status, m.contr_inn, m.contr_kpp_effective
    ) m 
    group by m.doc_id   
  ) f on f.doc_id = d.doc_id
  -- берем только АТ из очереди
  join ask_reclaim_queue q on q.doc_id = d.doc_id
  -- кол-во и сумма расхождений в АИ
  left join
  (
    select 
          t.queue_item_id
         ,count(t.doc_id) as all_doc_cnt
         ,sum(nvl(discrepancy_count, 0)) as all_dis_cnt
         ,sum(nvl(discrepancy_amount, 0)) as all_dis_amnt
    from doc t
    where t.doc_type = 2
    group by t.queue_item_id, t.sono_code
  ) r on r.queue_item_id = q.id
  -- кол-во контрагентов в АИ
  left join
  (
    select m.queue_item_id
           ,count(1) as all_contr_cnt
    from
    (
      select m.queue_item_id, m.contr_inn, m.contr_kpp_effective from
      (
        select
             d.queue_item_id
            ,case when d.doc_kind = 1 then sov.seller_inn 
                  when d.doc_kind = 2 then sov.buyer_inn
                  end as contr_inn
            ,case when d.doc_kind = 1 then
                    case when ask_seller.kpp_effective is null then '111111111' 
                    else ask_seller.kpp_effective end 
                  when d.doc_kind = 2 then  
                    case when ask_buyer.kpp_effective is null then '111111111' 
                    else ask_buyer.kpp_effective end 
                  end as contr_kpp_effective
        from doc d
        join ask_reclaim_queue q on q.id = d.queue_item_id 
        join doc_discrepancy sdis on sdis.doc_id = d.doc_id
        join sov_discrepancy sov on sdis.discrepancy_id = sov.id
        left join ask_declandjrnl ask_seller on ask_seller.zip = sov.seller_zip
        left join ask_declandjrnl ask_buyer on ask_buyer.zip = sov.buyer_zip
        where d.doc_type = 2 
      ) m
      group by m.queue_item_id, m.contr_inn, m.contr_kpp_effective
    ) m 
    group by m.queue_item_id     
  ) k on k.queue_item_id = q.id
  -- условие для АТ 
  where d.doc_type = 1 and d.status = 10
        -- условие для очереди АИ (берем только за определенный период)
        and q.create_date >= TO_DATE('07.04.2017 00:00:01', 'dd.mm.yyyy hh24:mi:ss')
) a
order by a.sono_ufns, a.sono_code, a.doc_id, a.side, a.inn
