﻿begin
for line in (select * from all_db_links where db_link like 'NDS2_FCOD%') loop
  execute immediate 'drop public database link '||line.db_link;
end loop;
end;
/
create public database link NDS2_FCOD
CONNECT TO FIR IDENTIFIED BY FIR
using 'NDS2';

begin
for line in (select * from all_db_links where db_link like  'NDS2_AIS3_MDM%') loop
  execute immediate 'drop public database link '||line.db_link;
end loop;
end;
/
create public database link NDS2_AIS3_MDM
CONNECT TO MDM_LOCAL IDENTIFIED BY MDM_LOCAL
using 'NDS2';

begin
for line in (select * from all_db_links where db_link like  'NDS2_AIS3_I$NDS2%') loop
  execute immediate 'drop public database link '||line.db_link;
end loop; 
end;
/
create public database link NDS2_AIS3_I$NDS2
CONNECT TO I$NDS2 IDENTIFIED BY NDS2
using 'NDS2';
