﻿declare
  CURSOR explains IS
  select r.filenameoutput, d.sono_code
  from SEOD_EXPLAIN_REPLY r
  join doc d on d.doc_id = r.doc_id
  where r.receive_by_tks = 0 
	and r.status_id = 2 
	and r.filenameoutput is not null;
  v_explains explains%ROWTYPE;
  
  pPathDirectory VARCHAR2(300);
begin
  
  -- необходимо указать путь к шаре с zip-контейнерами
  pPathDirectory := 'D:\';

  -- берем все пояснения из seod_explain_reply
  -- полученные не по ТКС, со статусом 2 (отправлено в МС)
  -- и указанным файлом
  OPEN explains;
  FETCH explains INTO v_explains;
  LOOP
      -- добавляем в ASKZipФайл запись
      NDS2_MC.P$POJASNENIE_ADD_ASKZIPFAJL
      (
         replace(v_explains.filenameoutput, '.xml', '.zip'), pPathDirectory, v_explains.sono_code
      );
        
      FETCH explains INTO v_explains;
      EXIT WHEN explains%NOTFOUND;
  END LOOP;

  CLOSE explains;      
end;