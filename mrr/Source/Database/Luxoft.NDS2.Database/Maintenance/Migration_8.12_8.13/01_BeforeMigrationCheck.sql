begin
  NDS2_MRR_USER.NDS2$SYS.DROP_IF_EXISTS('NDS2_MRR_USER', 'MIGRATION_CHECK_BEFORE');
end;
/
create TABLE nds2_mrr_user.migration_check_before(ID number, CHECK_CONDITION varchar2(255 char), CHECK_VALUE number, SYS_TIMESTAMP date);
/
declare param number;
begin
select count (decl.reg_number) into param from nds2_mrr_user.DECLARATION_HISTORY decl; 
insert into nds2_mrr_user.migration_check_before (ID, CHECK_CONDITION, CHECK_VALUE, SYS_TIMESTAMP) values (1, '�-�� ������� � ������� DECLARATION_HISTORY c ���. ��������', param, sysdate);
select count (distinct decl.reg_number) into param from nds2_mrr_user.DECLARATION_HISTORY decl; 
insert into nds2_mrr_user.migration_check_before (ID, CHECK_CONDITION, CHECK_VALUE, SYS_TIMESTAMP) values (2, '�-�� ������� � ������� DECLARATION_HISTORY c ����������� ���. ��������', param, sysdate);
select count(*)  into param from nds2_mrr_user.doc;
insert into nds2_mrr_user.migration_check_before (ID, CHECK_CONDITION, CHECK_VALUE, SYS_TIMESTAMP) values (3, '�-�� ������� � ������� DOC', param, sysdate);
select count(distinct ref_entity_id) into param from nds2_mrr_user.DOC; 
insert into nds2_mrr_user.migration_check_before (ID, CHECK_CONDITION, CHECK_VALUE, SYS_TIMESTAMP) values (4, '�-�� ������� � ������� DOC c ����������� ���. ��������', param, sysdate);
select count (DECLARATION_REG_NUM) into param from nds2_mrr_user.seod_knp; 
insert into nds2_mrr_user.migration_check_before (ID, CHECK_CONDITION, CHECK_VALUE, SYS_TIMESTAMP) values (5, '�-�� ������� � ������� SEOD_KNP c ���. ��������', param, sysdate);
select count (distinct DECLARATION_REG_NUM) into param from nds2_mrr_user.seod_knp; 
insert into nds2_mrr_user.migration_check_before (ID, CHECK_CONDITION, CHECK_VALUE, SYS_TIMESTAMP) values (6, '�-�� ������� � ������� SEOD_KNP c ����������� ���. ��������', param, sysdate);
select count (*) into param from nds2_mrr_user.DOC doc join nds2_mrr_user.DECLARATION_HISTORY decl on decl.REG_NUMBER = doc.ref_entity_id and decl.SONO_CODE_SUBMITED = doc.SONO_CODE;
insert into nds2_mrr_user.migration_check_before (ID, CHECK_CONDITION, CHECK_VALUE, SYS_TIMESTAMP) values (7, '�-�� ������� ����������� DOC � DECLARATION_HISTORY �� ���.�������', param, sysdate);
select count (*) into param from NDS2_MRR_USER.STAGE_INVOICE_CORRECTED_STATE;
insert into nds2_mrr_user.migration_check_before (ID, CHECK_CONDITION, CHECK_VALUE, SYS_TIMESTAMP) values (8, '�-�� ������� � ������� STAGE_INVOICE_CORRECTED_STATE', param, sysdate);
select count(*) into param from (select EXPLAIN_ID, INVOICE_ORIGINAL_ID, MAX(ID) as ID
  from NDS2_MRR_USER.STAGE_INVOICE_CORRECTED_STATE
  group by EXPLAIN_ID, INVOICE_ORIGINAL_ID
  having count(ID) > 1);
insert into nds2_mrr_user.migration_check_before (ID, CHECK_CONDITION, CHECK_VALUE, SYS_TIMESTAMP) values (9, '�-�� ������ � ������� STAGE_INVOICE_CORRECTED_STATE', param, sysdate);
select count (*) into param from NDS2_MRR_USER.STAGE_INVOICE_CORRECTED;
insert into nds2_mrr_user.migration_check_before (ID, CHECK_CONDITION, CHECK_VALUE, SYS_TIMESTAMP) values (10, '�-�� ������� � ������� STAGE_INVOICE_CORRECTED', param, sysdate);
select count(*) into param from (select EXPLAIN_ID, INVOICE_ORIGINAL_ID, FIELD_NAME, MAX(ID) as ID
  from NDS2_MRR_USER.STAGE_INVOICE_CORRECTED
  group by EXPLAIN_ID, INVOICE_ORIGINAL_ID, FIELD_NAME
  having count(ID) > 1);
insert into nds2_mrr_user.migration_check_before (ID, CHECK_CONDITION, CHECK_VALUE, SYS_TIMESTAMP) values (11, '�-�� ������ � ������� STAGE_INVOICE_CORRECTED', param, sysdate);
select count(*) into param from nds2_mrr_user.doc where doc_type = 1 and (close_reason is not null);
insert into nds2_mrr_user.migration_check_before (ID, CHECK_CONDITION, CHECK_VALUE, SYS_TIMESTAMP) values (12, '�-�� ��, � ������� ��������� ���� CLOSE_REASON', param, sysdate);
commit;
end;
