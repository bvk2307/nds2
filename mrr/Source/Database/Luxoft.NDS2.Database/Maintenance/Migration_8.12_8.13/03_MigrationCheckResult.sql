begin
  NDS2_MRR_USER.NDS2$SYS.DROP_IF_EXISTS('NDS2_MRR_USER', 'MIGRATION_CHECK_RESULT');
end;
/
create TABLE nds2_mrr_user.migration_check_result(ID number, CHECK_CONDITION varchar2(255 char), CHECK_RESULT varchar2(10 char), SYS_TIMESTAMP date);
/
declare 
check_value_after number :=0;
check_result varchar2(10 char);
begin
for item in (select * from nds2_mrr_user.migration_check_before)
loop

select CHECK_VALUE into check_value_after from nds2_mrr_user.migration_check_after where id = item.id;

check_result :=
case when item.ID in (9, 11) and check_value_after = 0 then 'SUCCESS'
     when item.ID in (9, 11) and check_value_after <> 0 then 'FAIL'
     when item.ID not in (9, 11) and item.CHECK_VALUE = check_value_after then 'SUCCESS' else 'FAIL' end;
insert into nds2_mrr_user.migration_check_result (ID, CHECK_CONDITION, CHECK_RESULT, SYS_TIMESTAMP) values(item.ID, item.CHECK_CONDITION, check_result, sysdate );
end loop;
commit;
end;