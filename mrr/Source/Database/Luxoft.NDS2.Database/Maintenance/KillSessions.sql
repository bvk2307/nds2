﻿declare
v_sessionExist number(5) := 0;
v_max_attempts number(3) := 100;
v_unableToKillSessionEx EXCEPTION;
begin
select count(1) into v_sessionExist from v$session where username in ('NDS2_MRR_USER', 'NDS2_MC', 'FIR', 'I$CAM', 'I$NDS2');
while (v_sessionExist > 0 and v_max_attempts > 0) 
  loop
    for line in 
      (select serial#, sid, username, machine, program,terminal from v$session where username in  ('NDS2_MRR_USER', 'NDS2_MC', 'FIR', 'I$CAM', 'I$NDS2')) loop
          begin
              dbms_output.put_line('attempt to kill session sid:'||line.sid||' serial:'||line.serial#||' host:'||line.machine||' app:'||line.program||' user:'||line.username);
              execute immediate 'ALTER SYSTEM KILL SESSION '''||line.sid||','||line.serial#||''' IMMEDIATE';
          exception when others then 
              dbms_output.put_line('unable to kill session ');
          end;
     end loop;
  select count(1) into v_sessionExist from v$session where username in ('NDS2_MRR_USER', 'NDS2_MC', 'FIR', 'I$CAM', 'I$NDS2');
  v_max_attempts := v_max_attempts - 1;
end loop;

if v_sessionExist > 0 then
begin
 dbms_output.put_line('unable to continue. kill alive session manually.');
 for line in (select serial#, sid, username, machine, program,terminal from v$session where username in  ('NDS2_MRR_USER', 'NDS2_MC', 'FIR', 'I$CAM', 'I$NDS2'))
   loop
     dbms_output.put_line('sid:'||line.sid||' serial:'||line.serial#||' host:'||line.machine||' app:'||line.program||' user:'||line.username);
   end loop;  
 RAISE v_unableToKillSessionEx;
end;
end if;
end;
/
