﻿BEGIN
DBMS_SCHEDULER.STOP_JOB (
job_name => 'NDS2_MRR_USER.J$BACKGOUND_PROCESS');
exception when others then null;
END;
/

BEGIN
DBMS_SCHEDULER.DROP_JOB (
job_name => 'NDS2_MRR_USER.J$BACKGOUND_PROCESS');
exception when others then null;
END;
/
begin 
  sys.dbms_scheduler.create_job(job_name => 'NDS2_MRR_USER.J$BACKGOUND_PROCESS', 
  job_type => 'STORED_PROCEDURE', 
  job_action => 'PAC$JOB.START_WORK', 
  start_date => SYSDATE + INTERVAL '1' SECOND,
  repeat_interval => 'freq=Secondly;Interval=15',
  end_date => to_date('01-01-2100 00:00:00', 'dd-mm-yyyy hh24:mi:ss'), 
  job_class => 'DEFAULT_JOB_CLASS', 
  enabled => false, 
  auto_drop => false, 
  comments => 'фоновые задания МРР'); 
end; 
/

BEGIN
DBMS_SCHEDULER.STOP_JOB (
job_name => 'NDS2_MRR_USER.J$CANCEL_SELECTION_BACKGROUND');
exception when others then null;
END;
/

BEGIN
DBMS_SCHEDULER.DROP_JOB (
job_name => 'NDS2_MRR_USER.J$CANCEL_SELECTION_BACKGROUND');
exception when others then null;
END;

BEGIN
DBMS_SCHEDULER.STOP_JOB (
job_name => 'NDS2_MRR_USER.J$REPORT_DECL_BACKGROUND');
exception when others then null;
END;
/

BEGIN
DBMS_SCHEDULER.DROP_JOB (
job_name => 'NDS2_MRR_USER.J$REPORT_DECL_BACKGROUND');
exception when others then null;
END;
/
begin 
  sys.dbms_scheduler.create_job(job_name => 'NDS2_MRR_USER.J$REPORT_DECL_BACKGROUND', 
  job_type => 'STORED_PROCEDURE', 
  job_action => 'NDS2$REPORTS.START_DECLARATION_CALCULATE', 
  start_date => TO_DATE(TO_CHAR(SYSDATE, 'dd/mm/yyyy') || ' 23:00:00', 'dd/mm/yyyy HH24:mi:ss') + INTERVAL '1' MINUTE,
  repeat_interval => 'freq=Secondly;Interval=5',
  end_date => to_date('01-01-2100 00:00:00', 'dd-mm-yyyy hh24:mi:ss'), 
  job_class => 'DEFAULT_JOB_CLASS', 
  enabled => false, 
  auto_drop => false, 
  comments => 'аннулирование выборок'); 
end; 
/

BEGIN
DBMS_SCHEDULER.STOP_JOB (
job_name => 'NDS2_MRR_USER.J$SYNCDATA_DAILY');
exception when others then null;
END;
/

BEGIN
DBMS_SCHEDULER.DROP_JOB (
job_name => 'NDS2_MRR_USER.J$SYNCDATA_DAILY');
exception when others then null;
END;
/
begin 
  sys.dbms_scheduler.create_job(job_name => 'NDS2_MRR_USER.J$SYNCDATA_DAILY', 
  job_type => 'STORED_PROCEDURE', 
  job_action => 'PAC$SYNC.START_WORK', 
  start_date => SYSDATE + INTERVAL '1' SECOND,
  repeat_interval => 'freq=Hourly;Interval=1',
  end_date => to_date('01-01-2100 00:00:00', 'dd-mm-yyyy hh24:mi:ss'), 
  job_class => 'DEFAULT_JOB_CLASS', 
  enabled => false, 
  auto_drop => false, 
  comments => 'фоновые задания МРР: Синхронизация'); 
end; 
/

BEGIN
DBMS_SCHEDULER.STOP_JOB (
job_name => 'NDS2_MRR_USER.J$DEMO_JOB');
exception when others then null;
END;
/

BEGIN
DBMS_SCHEDULER.DROP_JOB (
job_name => 'NDS2_MRR_USER.J$DEMO_JOB');
exception when others then null;
END;
/
begin 
  sys.dbms_scheduler.create_job(job_name => 'NDS2_MRR_USER.J$DEMO_JOB', 
  job_type => 'PLSQL_BLOCK', 
  job_action => 'declare v_runjob number := 0; v_jobName varchar2(32 CHAR) := ''J$DEMO_JOB''; v_jobOwner varchar2(32 CHAR) := ''NDS2_MRR_USER'';
begin
  select count(1) into v_runjob from all_scheduler_running_jobs where job_name = v_jobName and owner = v_jobOwner;
  if v_runjob = 0 then
    nds2_mrr_user.PAC$DOCUMENT.DEMO;
  end if;
end;', 
  start_date => SYSDATE + INTERVAL '5' SECOND,
  repeat_interval => 'freq=Secondly;Interval=15',
  end_date => to_date('01-01-2100 00:00:00', 'dd-mm-yyyy hh24:mi:ss'), 
  job_class => 'DEFAULT_JOB_CLASS', 
  enabled => false, 
  auto_drop => false, 
  comments => 'фоновые задания МРР'); 
end; 
/

BEGIN
DBMS_SCHEDULER.STOP_JOB (
job_name => 'NDS2_MRR_USER.J$CLAIM_KS');
exception when others then null;
END;
/

BEGIN
DBMS_SCHEDULER.DROP_JOB (
job_name => 'NDS2_MRR_USER.J$CLAIM_KS');
exception when others then null;
END;
/
begin 
  sys.dbms_scheduler.create_job(job_name => 'NDS2_MRR_USER.J$CLAIM_KS', 
  job_type => 'PLSQL_BLOCK', 
  job_action => 'declare v_runjob number := 0; v_jobName varchar2(32 CHAR) := ''J$CLAIM_KS''; v_jobOwner varchar2(32 CHAR) := ''NDS2_MRR_USER'';
begin
  select count(1) into v_runjob from all_scheduler_running_jobs where job_name = v_jobName and owner = v_jobOwner;
  if v_runjob = 0 then
    nds2_mrr_user.pac$document.SEND_CLAIM_KS;
  end if;
end;', 
  start_date => SYSDATE + INTERVAL '5' SECOND,
  repeat_interval => 'freq=Secondly;Interval=10',
  end_date => to_date('01-01-2100 00:00:00', 'dd-mm-yyyy hh24:mi:ss'), 
  job_class => 'DEFAULT_JOB_CLASS', 
  enabled => false, 
  auto_drop => false, 
  comments => 'фоновые задания МРР'); 
end; 
/

BEGIN
DBMS_SCHEDULER.STOP_JOB (
job_name => 'NDS2_MRR_USER.J$REFRESH_MV_DECL');
exception when others then null;
END;
/

BEGIN
DBMS_SCHEDULER.DROP_JOB (
job_name => 'NDS2_MRR_USER.J$REFRESH_MV_DECL');
exception when others then null;
END;
/
begin 
  sys.dbms_scheduler.create_job(job_name => 'NDS2_MRR_USER.J$REFRESH_MV_DECL', 
  job_type => 'STORED_PROCEDURE', 
  job_action => 'PAC$MVIEW_MANAGER.REFRESH_DECLARATONS', 
  start_date => SYSDATE + INTERVAL '1' SECOND,
  repeat_interval => 'freq=Secondly;Interval=5',
  end_date => to_date('01-01-2100 00:00:00', 'dd-mm-yyyy hh24:mi:ss'), 
  job_class => 'DEFAULT_JOB_CLASS', 
  enabled => false, 
  auto_drop => false, 
  comments => 'фоновые задания МРР: обновление мат. представлений по декларациям'); 
end; 
/
