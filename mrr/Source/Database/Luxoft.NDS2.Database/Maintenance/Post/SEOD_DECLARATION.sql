﻿truncate table NDS2_MRR_USER.seod_declaration;
INSERT INTO NDS2_MRR_USER.seod_declaration (
id,
type,
nds2_id,
sono_code,
tax_period,
fiscal_year,
correction_number,
decl_reg_num,
decl_fid,
tax_payer_type,
inn,
kpp,
insert_date,
eod_date,
date_receipt,
id_file)
SELECT
d.ID + 1000,
type,
(d.PERIOD||d.OTCHETGOD||d.INNNP),
d.KODNO,
d.PERIOD,
d.OTCHETGOD,
d.nomkorr,
d.ID,
d.ID,
1,
d.INNNP,
d.KPPNP,
SYSDATE,
SYSDATE,
SYSDATE,
d.IDFAJL
FROM NDS2_MRR_USER.V$ASK_DECLANDJRNL d;
commit;
/
