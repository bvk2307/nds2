﻿truncate table NDS2_MRR_USER.SOV_INVOICE_ALL;
insert into NDS2_MRR_USER.SOV_INVOICE_ALL
(
  declaration_version_id, chapter, ordinal_number,
  okv_code, create_date, receive_date,
  invoice_num, invoice_date, change_num,
  change_date, correction_num, correction_date,
  change_correction_num, change_correction_date, seller_invoice_num,
  seller_invoice_date, broker_inn, broker_kpp,
  deal_kind_code, customs_declaration_num, price_buy_amount,
  price_buy_nds_amount, price_sell, price_sell_in_curr,
  price_sell_18, price_sell_10, price_sell_0,
  price_nds_18, price_nds_10, price_tax_free,
  price_total, price_nds_total, diff_correct_decrease,
  diff_correct_increase, diff_correct_nds_decrease,
  diff_correct_nds_increase, price_nds_buyer, row_key,
  actual_row_key, compare_row_key, compare_algo_id,
  format_errors, logical_errors, seller_agency_info_inn,
  seller_agency_info_kpp, seller_agency_info_name, seller_agency_info_num,
  seller_agency_info_date, is_import,
  clarification_key, contragent_key
)
select  
  a.declaration_version_id, a.chapter, a.ordinal_number,
  a.okv_code, a.create_date, a.receive_date, 
  a.invoice_num, a.invoice_date, a.change_num,
  a.change_date, a.correction_num, a.correction_date,
  a.change_correction_num, a.change_correction_date, a.seller_invoice_num,
  a.seller_invoice_date, a.broker_inn, a.broker_kpp,
  a.deal_kind_code, a.customs_declaration_num, a.price_buy_amount,
  a.price_buy_nds_amount, a.price_sell, a.price_sell_in_curr,
  a.price_sell_18, a.price_sell_10, a.price_sell_0,
  a.price_nds_18, a.price_nds_10, a.price_tax_free,
  a.price_total, a.price_nds_total, a.diff_correct_decrease,
  a.diff_correct_increase, a.diff_correct_nds_decrease,
  a.diff_correct_nds_increase, a.price_nds_buyer, a.row_key,
  a.actual_row_key, a.compare_row_key, a.compare_algo_id,
  a.format_errors, a.logical_errors, a.seller_agency_info_inn,
  a.seller_agency_info_kpp, a.seller_agency_info_name, a.seller_agency_info_num,
  a.seller_agency_info_date, null,
  null, null
from fir.invoice_raw a;
commit;