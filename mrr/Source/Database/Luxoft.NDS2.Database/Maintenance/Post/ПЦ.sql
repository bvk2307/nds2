﻿select '--------------------------- FULL CYCLE - start --------------------------' as Message from dual;

BEGIN
	NDS2_MRR_USER.pac$nds2_full_cycle.P$0_UPD_CONTROLRATIOAGGREGATE;
	NDS2_MRR_USER.pac$nds2_full_cycle.P$2_UPDATE_DECL_DISCREP_LIST;
	NDS2_MRR_USER.pac$nds2_full_cycle.p$update_dicrepancy_ext;
END;
/

BEGIN
   --Временный вариант для обновления сводной. Нужно убрать, когда составной ключ по группе корректировок будет заполняться хивой
   execute immediate 'create table decl_jrnl_summary_new as select * from v$decl_jrnl_summary_build';
   execute immediate 'drop table declaration_journal_summary purge';
   execute immediate 'alter table decl_jrnl_summary_new rename to declaration_journal_summary'; 
END;
/

select '--------------------------- FULL CYCLE - end --------------------------' as Message from dual;

