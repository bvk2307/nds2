﻿declare 
  v_sql varchar2(512);
begin
  for l in (select * from all_objects where owner in ('NDS2_MRR_USER') and OBJECT_TYPE in ('TABLE','MATERIALIZED VIEW', 'INDEX', 'SEQUENCE', 'JOB', 'TYPE', 'VIEW', 'PACKAGE', 'PACKAGE BODY', 'FUNCTION', 'PROCEDURE'))
  loop
    select
      case l.object_type
        when 'JOB' then 'begin DBMS_SCHEDULER.DROP_JOB(''' || l.owner || '."' || l.object_name || '"''); end;'
        else 'drop '|| l.object_type || ' ' || l.owner || '."' || l.object_name || '"' || DECODE(l.OBJECT_TYPE, 'TABLE', ' CASCADE CONSTRAINTS', '')
      end
    into v_sql from dual;
    
    begin
      execute immediate v_sql;
      --DBMS_OUTPUT.put_line('Success: ' || v_sql);
      exception when others then DBMS_OUTPUT.put_line('Failed:  ' || v_sql);
    end;
  end loop;
end;
/
begin
  for row in (select qual_schema_url from all_xml_schemas where owner = 'NDS2_MRR_USER' and schema_url like 'TAX3EXCH_NDS2_CAM_%.xsd')
  loop
    begin
      DBMS_XMLSCHEMA.deleteSchema(row.qual_schema_url, dbms_xmlschema.DELETE_CASCADE_FORCE);
      exception when others then DBMS_OUTPUT.put_line(row.qual_schema_url || ': ' || substr(sqlerrm, 256));
    end;
  end loop;
end;
/
