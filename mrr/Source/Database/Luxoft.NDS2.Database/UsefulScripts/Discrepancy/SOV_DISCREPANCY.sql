truncate table NDS2_MRR_USER.SOV_DISCREPANCY;
insert into NDS2_MRR_USER.SOV_DISCREPANCY
select 
Seq_Discrepancy_Id.Nextval
,null as ifns
,f.CREATE_DATE
,f.TYPE
,f.RULE_GROUP
,f.DEAL_AMNT
,f.AMNT
,f.AMOUNT_PVP
,f.INVOICE_CHAPTER
,f.INVOICE_RK
,f.DECL_ID
,f.INVOICE_CONTRACTOR_CHAPTER
,f.INVOICE_CONTRACTOR_RK
,f.DECL_CONTRACTOR_ID
,f.STATUS
,3 as rule_num
,null as close_date
,d1.INN as buyer_inn
,d2.INN as seller_inn
,0
from FIR.DISCREPANCY_RAW f
left join v$declaration d1 on d1.ID = f.decl_id
left join v$declaration d2 on d2.ID = f.decl_contractor_id;
commit;