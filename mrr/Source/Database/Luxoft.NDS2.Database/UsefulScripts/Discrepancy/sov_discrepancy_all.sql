﻿truncate table SOV_DISCREPANCY_ALL;
insert into SOV_DISCREPANCY_ALL
(
  status, 
  type, create_date,
  rule_group, deal_amnt, amnt,
  buyer_inn, seller_inn, 
  invoice_chapter, invoice_rk, invoice_contractor_rk,
  invoice_contractor_chapter,  
  amount_pvp, rule_num,
  buyer_kpp, zip,
  contractor_zip
)
select distinct
  a.status, 
  a.type, a.create_date,
  a.rule_group, a.deal_amnt, a.amnt,
  d1.INN, d2.INN, 
  a.invoice_chapter, a.invoice_rk, a.invoice_contractor_rk,
  a.invoice_contractor_chapter,  
  a.amount_pvp, 3,
  null, d1.DECLARATION_VERSION_ID,
  d2.DECLARATION_VERSION_ID
from fir.discrepancy_raw a
left join sov_invoice_all inv1 on inv1.row_key = a.invoice_rk 
left join sov_invoice_all inv2 on inv2.row_key = a.invoice_contractor_rk 
left join v$declaration d1 on d1.DECLARATION_VERSION_ID = inv1.declaration_version_id
left join v$declaration d2 on d2.DECLARATION_VERSION_ID = inv2.declaration_version_id;
commit;