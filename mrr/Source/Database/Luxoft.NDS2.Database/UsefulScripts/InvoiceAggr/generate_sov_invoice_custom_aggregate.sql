﻿-- Генерация случайных таможенных номеров по sov_invoice
declare
  v_num number;
  v_decl_num varchar2(6 char);
  v_decl_nums varchar2(1000 char);
begin
  delete from SOV_INVOICE_CUSTOM_AGGREGATE;
  update SOV_INVOICE si set si.customs_declaration_num = '';
  
  for invoice in (select si.row_key from SOV_INVOICE si)
  loop
    v_num := round(dbms_random.value(0, 60) - 20);
    
    if v_num > 0 then
      for i in 1..v_num loop
        v_decl_num := to_char(round(dbms_random.value(100000, 999999)));
        insert into SOV_INVOICE_CUSTOM_AGGREGATE (INVOICE_ROW_KEY, CUSTOMS_DECLARATION_NUM)
        values(invoice.row_key, v_decl_num);
        
        if (i = 1) then v_decl_nums := v_decl_num;
        elsif (i < 5) then v_decl_nums := v_decl_nums||', '||v_decl_num;
        elsif (i = 5) then v_decl_nums := v_decl_nums||'...';
        end if;
      end loop;
      
      update SOV_INVOICE si set si.customs_declaration_num = v_decl_nums where si.row_key = invoice.row_key;
    end if;
    
  end loop;
  
  commit;
end;
/
