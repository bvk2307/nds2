--��������� ������ �����, ���� �� ���
--���������, ����������� �� ���� �������, ���� � ���, ��� ���� ��� ����
update 
 sov_invoice inv
 set inv.contragent_key = to_number(case
    when inv.chapter = 8 then inv.seller_inn
    when inv.chapter = 9 then inv.buyer_inn
    when inv.chapter = 10 then inv.seller_inn
    when inv.chapter = 11 then inv.buyer_inn
    when inv.chapter = 12 then inv.buyer_inn
  end)
  + EXTRACT(MONTH FROM inv.invoice_date) * POWER(10, 12)
  + EXTRACT(YEAR FROM inv.invoice_date) * POWER(10, 14);
commit;
  
