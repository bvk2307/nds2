﻿ALTER TABLE "ASKДекл"
     ADD "РлСр118СумНал" NUMBER
      ADD "РлСр118НалБаза" NUMBER
      ADD "РлСр110СумНал" NUMBER
      ADD "РлСр110НалБаза" NUMBER
      ADD "УплД151СумНал" NUMBER
      ADD "УплД151НалБаза" NUMBER
      ADD "УплД173СумНал" NUMBER
      ADD "УплД173НалБаза" NUMBER
      ADD "НалПредНПКапСтр" NUMBER
      ADD "НалВыч171.14" NUMBER;
   
  ALTER TABLE "ASKСумВосУпл"
      ADD "НаимООС" VARCHAR2(100)
      ADD "КодОпООС" VARCHAR2(7)
      ADD "ДатаВводООС" DATE
      ADD "СтВводООС" NUMBER
      ADD "НалВычООС" NUMBER;
   
  CREATE TABLE "ASKКорНБВозвр" (
      "Ид"       NUMBER NOT NULL,
      "ИдДекл"   NUMBER NOT NULL,
      "КодОпер"  VARCHAR(7),
      "НалБаза"  NUMBER,
      "НалВосст" NUMBER,
      CONSTRAINT "PK_ASKКорНБВозвр" PRIMARY KEY ("Ид")
  );
   
  CREATE TABLE "ASKКорНБИзмЦен" (
      "Ид"           NUMBER NOT NULL,
      "ИдДекл"       NUMBER NOT NULL,
      "КодОпер"      VARCHAR(7),
      "КорНалБазаУв" NUMBER,
      "КорНалБазаУм" NUMBER,
      CONSTRAINT "PK_ASKКорНБИзм" PRIMARY KEY ("Ид")
  );
   
  CREATE TABLE "ASKКорНалВозвр" (
      "Ид"       NUMBER NOT NULL,
      "ИдДекл"   NUMBER NOT NULL,
      "КодОпер"  VARCHAR(7),
      "НалБаза"  NUMBER,
      "КорИсч"   NUMBER,
      "НалВосст" NUMBER,
      CONSTRAINT "PK_ASKКорНалВоз" PRIMARY KEY ("Ид")
  );
   
  CREATE TABLE "ASKКорНалИзмЦен" (
      "Ид"           NUMBER NOT NULL,
      "ИдДекл"       NUMBER NOT NULL,
      "КодОпер"      VARCHAR(7),
      "КорНалБазаУв" NUMBER,
      "КорИсчУв"     NUMBER,
      "КорНалБазаУм" NUMBER,
      "КорИсчУм"     NUMBER,
      CONSTRAINT "PK_ASKКорНалИзм" PRIMARY KEY ("Ид")
  );
 
  ALTER TABLE "ASKКорНБВозвр"
      ADD CONSTRAINT "FK_ASKКорНБВозвр" FOREIGN KEY ("ИдДекл") REFERENCES "ASKДекл" ("Ид") ON DELETE CASCADE;
   
  ALTER TABLE "ASKКорНБИзмЦен"
      ADD CONSTRAINT "FK_ASKКорНБИзм" FOREIGN KEY ("ИдДекл") REFERENCES "ASKДекл" ("Ид") ON DELETE CASCADE;
   
  ALTER TABLE "ASKКорНалВозвр"
      ADD CONSTRAINT "FK_ASKКорНалВоз" FOREIGN KEY ("ИдДекл") REFERENCES "ASKДекл" ("Ид") ON DELETE CASCADE;
   
  ALTER TABLE "ASKКорНалИзмЦен"
      ADD CONSTRAINT "FK_ASKКорНалИзм" FOREIGN KEY ("ИдДекл") REFERENCES "ASKДекл" ("Ид") ON DELETE CASCADE;
