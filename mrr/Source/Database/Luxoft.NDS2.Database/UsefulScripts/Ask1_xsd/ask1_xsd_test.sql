-- ��������� ����� ��������
-- ����� �������� ���� ������ PAC$DOCUMENT_LOGGED
-- ��� ���� �������������� �����������, �� ��� �� ����������� - ��������� � ����������� ����� ����� � ���.
declare
  v_decl_id number;
  v_decl_c_id number;
  v_dvi number;
begin

-- ������� ���������� ����� � �����������
select distinct id into v_decl_id from v$declaration where inn='5245000448';
select distinct id into v_decl_c_id from v$declaration d where d.DECLARATION_VERSION_ID = 1200336;
select max(declaration_version_id) into v_dvi from v$declaration where inn='5245000448';
 
-- ������� �������
insert into selection
values
(
  SELECTIONS_SEQ.NEXTVAL
  ,'�������� �������'
  ,sysdate
  ,sysdate
  ,sysdate
  ,'OMSK\dnavrotskiy' --analytic
  ,'S-1-5-21-2876771337-70127850-1913825905-22660' --analytic_sid
  ,'OMSK\dnavrotskiy' --manager
  ,'S-1-5-21-2876771337-70127850-1913825905-22660' --manager_sid
  ,''
  ,13
  ,0
  ,4242
  ,''
  ,4242
  ,null
  ,null
);
-- ������� �����������
insert into sov_discrepancy
(ID, CREATE_DATE, TYPE, RULE_GROUP, DEAL_AMNT, AMNT, AMOUNT_PVP, COURSE, COURSE_COST, SUR_CODE, INVOICE_CHAPTER, INVOICE_RK, DECL_ID, INVOICE_CONTRACTOR_CHAPTER, INVOICE_CONTRACTOR_RK, DECL_CONTRACTOR_ID, STATUS, USER_COMMENT, RULE_NUM)
values (14242, to_date('11-06-2015 14:01:40', 'dd-mm-yyyy hh24:mi:ss'), 1, 4, 0.37, 0.49, 0.60, 0.39, 0.26, 2, 8, '445964493', v_decl_id, 9, '653167487', v_decl_c_id, 1, null, null);
-- ����� � ��������
insert into selection_discrepancy values (4242, 14242, 1);
-- � ������� ��� ��
insert into SOV_INVOICE
(REQUEST_ID, DECLARATION_VERSION_ID, CHAPTER, ORDINAL_NUMBER, OKV_CODE, CREATE_DATE, RECEIVE_DATE, OPERATION_CODE, INVOICE_NUM, INVOICE_DATE, CHANGE_NUM, CHANGE_DATE, CORRECTION_NUM, CORRECTION_DATE, CHANGE_CORRECTION_NUM, CHANGE_CORRECTION_DATE, RECEIPT_DOC_NUM, RECEIPT_DOC_DATE, BUY_ACCEPT_DATE, BUYER_INN, BUYER_KPP, SELLER_INN, SELLER_KPP, SELLER_INVOICE_NUM, SELLER_INVOICE_DATE, BROKER_INN, BROKER_KPP, DEAL_KIND_CODE, CUSTOMS_DECLARATION_NUM, PRICE_BUY_AMOUNT, PRICE_BUY_NDS_AMOUNT, PRICE_SELL, PRICE_SELL_IN_CURR, PRICE_SELL_18, PRICE_SELL_10, PRICE_SELL_0, PRICE_NDS_18, PRICE_NDS_10, PRICE_TAX_FREE, PRICE_TOTAL, PRICE_NDS_TOTAL, DIFF_CORRECT_DECREASE, DIFF_CORRECT_INCREASE, DIFF_CORRECT_NDS_DECREASE, DIFF_CORRECT_NDS_INCREASE, PRICE_NDS_BUYER, ROW_KEY, ACTUAL_ROW_KEY, COMPARE_ROW_KEY, COMPARE_ALGO_ID, FORMAT_ERRORS, LOGICAL_ERRORS, SELLER_AGENCY_INFO_INN, SELLER_AGENCY_INFO_KPP, SELLER_AGENCY_INFO_NAME, SELLER_AGENCY_INFO_NUM, SELLER_AGENCY_INFO_DATE, IS_DOP_LIST, IS_IMPORT, CLARIFICATION_KEY, CONTRAGENT_KEY)
values (4242, v_dvi, 8, 11408, '810', to_date('06-04-2015', 'dd-mm-yyyy'), to_date('02-06-2012', 'dd-mm-yyyy'), '13', '5308603', to_date('24-08-2014', 'dd-mm-yyyy'), 2, to_date('29-09-2013', 'dd-mm-yyyy'), 8, to_date('06-02-2012', 'dd-mm-yyyy'), 8, to_date('26-12-2013', 'dd-mm-yyyy'), '5976262', to_date('25-09-2013', 'dd-mm-yyyy'), to_date('15-04-2013', 'dd-mm-yyyy'), '5245000448', '524501001', '1928543079', '470305835', '8053919', to_date('28-01-2015', 'dd-mm-yyyy'), '5252031528', '525201001', 2, 'f812ddef-1897-4a7c-9322-ca691c0a46a0', 0.26, 0.14, 0.69, 0.87, 0.03, 0.45, 0.56, 0.16, 0.84, 0.23, 0.06, 0.70, 0.72, 0.47, 0.78, 0.85, 0.21, '445964493', null, null, 0, null, '8002', null, null, null, null, to_date('01-01-0001', 'dd-mm-yyyy'), 0, 0, null, null);

insert into SOV_INVOICE 
(REQUEST_ID, DECLARATION_VERSION_ID, CHAPTER, ORDINAL_NUMBER, OKV_CODE, CREATE_DATE, RECEIVE_DATE, OPERATION_CODE, INVOICE_NUM, INVOICE_DATE, CHANGE_NUM, CHANGE_DATE, CORRECTION_NUM, CORRECTION_DATE, CHANGE_CORRECTION_NUM, CHANGE_CORRECTION_DATE, RECEIPT_DOC_NUM, RECEIPT_DOC_DATE, BUY_ACCEPT_DATE, BUYER_INN, BUYER_KPP, SELLER_INN, SELLER_KPP, SELLER_INVOICE_NUM, SELLER_INVOICE_DATE, BROKER_INN, BROKER_KPP, DEAL_KIND_CODE, CUSTOMS_DECLARATION_NUM, PRICE_BUY_AMOUNT, PRICE_BUY_NDS_AMOUNT, PRICE_SELL, PRICE_SELL_IN_CURR, PRICE_SELL_18, PRICE_SELL_10, PRICE_SELL_0, PRICE_NDS_18, PRICE_NDS_10, PRICE_TAX_FREE, PRICE_TOTAL, PRICE_NDS_TOTAL, DIFF_CORRECT_DECREASE, DIFF_CORRECT_INCREASE, DIFF_CORRECT_NDS_DECREASE, DIFF_CORRECT_NDS_INCREASE, PRICE_NDS_BUYER, ROW_KEY, ACTUAL_ROW_KEY, COMPARE_ROW_KEY, COMPARE_ALGO_ID, FORMAT_ERRORS, LOGICAL_ERRORS, SELLER_AGENCY_INFO_INN, SELLER_AGENCY_INFO_KPP, SELLER_AGENCY_INFO_NAME, SELLER_AGENCY_INFO_NUM, SELLER_AGENCY_INFO_DATE, IS_DOP_LIST, IS_IMPORT, CLARIFICATION_KEY, CONTRAGENT_KEY)
values (4242, 1200336, 9, 11409, '810', to_date('13-04-2014', 'dd-mm-yyyy'), to_date('14-04-2015', 'dd-mm-yyyy'), '19', '7706826', to_date('08-08-2014', 'dd-mm-yyyy'), 6, to_date('06-12-2014', 'dd-mm-yyyy'), 7, to_date('24-03-2014', 'dd-mm-yyyy'), 7, to_date('09-01-2013', 'dd-mm-yyyy'), '7724235', to_date('04-06-2013', 'dd-mm-yyyy'), to_date('13-09-2013', 'dd-mm-yyyy'), '5245000448', '524501001', '1928543079', '470305835', '3775590', to_date('12-04-2013', 'dd-mm-yyyy'), '8614002935', '861401001', 1, 'c9d06dff-fa46-4d52-ad3c-50a23688350f', 0.39, 0.96, 0.48, 0.30, 0.05, 0.81, 0.09, 0.56, 0.05, 0.73, 0.39, 0.12, 0.94, 0.51, 0.52, 0.77, 0.50, '653167487', null, null, 0, null, '9020', null, null, null, null, to_date('01-01-0001', 'dd-mm-yyyy'), 0, 0, null, null);
commit;
end;
/
-- �����, ��� ��������� ������ �������� ������� (��� ������)
  select
   distinct
    i.row_key, i.declaration_version_id, i.chapter, i.ordinal_number, i.okv_code, i.create_date, i.receive_date, i.operation_code,
    i.invoice_num, i.invoice_date, i.change_num, i.change_date, i.correction_num, i.correction_date, i.change_correction_num,
    i.change_correction_date, i.receipt_doc_num, i.receipt_doc_date, i.buy_accept_date, i.buyer_inn, i.buyer_kpp, i.seller_inn, i.seller_kpp,
    i.seller_invoice_num, i.seller_invoice_date, i.broker_inn, i.broker_kpp, i.deal_kind_code, i.customs_declaration_num, i.price_buy_amount,
    i.price_buy_nds_amount, i.price_sell, i.price_sell_in_curr, i.price_sell_18, i.price_sell_10, i.price_sell_0, i.price_nds_18, i.price_nds_10,
    i.price_tax_free, i.price_total, i.price_nds_total, i.diff_correct_decrease, i.diff_correct_increase, i.diff_correct_nds_decrease,
    i.diff_correct_nds_increase, i.price_nds_buyer, i.actual_row_key, i.compare_row_key, i.compare_algo_id, i.format_errors, i.logical_errors,
    i.seller_agency_info_inn, i.seller_agency_info_kpp, i.seller_agency_info_name, i.seller_agency_info_num, i.seller_agency_info_date, i.is_import,
    i.is_dop_list
  from selection sel
  inner join selection_discrepancy dis_ref on dis_ref.request_id = sel.request_id
  inner join sov_discrepancy dis on dis.id = dis_ref.discrepancy_id
  left join sov_invoice i on i.request_id = sel.invoice_request_id
  inner join v$declaration hist_decl on hist_decl.declaration_version_id = i.declaration_version_id
  left join  stage_invoice si on si.row_key = i.row_key
  where dis.status = 1
    and dis.stage = 1
    and sel.status = 13
    and dis_ref.is_in_process = 1
    and ((hist_decl.id = dis.decl_id and i.row_key = dis.invoice_rk) or (hist_decl.id = dis.decl_contractor_id and i.row_key = dis.invoice_contractor_rk))
    and si.row_key is null
	  and not exists (select siagg.row_key from sov_invoice siagg where siagg.actual_row_key = i.row_key);
    
-- �������� ����
begin
  pac$document.DEMO();
end;
/
-- � ������� � ���
select * from system_log
--where site like 'COLLECT_QUEUE%' or site like 'PROCESS_QUEUE' order by id;
where log_date is not null and site not like 'PAC$SYNC%' and site not like 'NDS2$REPORTS%' and site not like 'PAC$JOB%' and site not like 'PAC$MVIEW_MANAGER%'
order by id desc;
