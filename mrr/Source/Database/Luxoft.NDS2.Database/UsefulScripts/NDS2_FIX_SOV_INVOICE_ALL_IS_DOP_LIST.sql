create table NDS2_MRR_USER.t_sov_invoice_all
nologging
pctfree 0
as
select  t.*
       ,case when 
             substr(row_key,instr(row_key,'/',1,1) + 1, instr(row_key,'/',1,2) - instr(row_key,'/',1,1) - 1) in ('2','3') 
             then 1 
               else 0 
                 end as IS_EXTENSION 
from NDS2_MRR_USER.SOV_INVOICE_ALL t;

drop index NDS2_MRR_USER.IDX_SIA_RK2;
drop index NDS2_MRR_USER.IDX_SIA_RK3;
create index NDS2_MRR_USER.IDX_SIA_RK2 on NDS2_MRR_USER.t_sov_invoice_all (row_key);
create index NDS2_MRR_USER.IDX_SIA_RK3 on NDS2_MRR_USER.t_sov_invoice_all (actual_row_key);
      
alter table NDS2_MRR_USER.t_sov_invoice_all drop column IS_DOP_LIST;
alter table NDS2_MRR_USER.t_sov_invoice_all rename column IS_EXTENSION to IS_DOP_LIST;
alter table NDS2_MRR_USER.SOV_INVOICE_ALL rename to SOV_INVOICE_ALL_BK;
alter table NDS2_MRR_USER.t_sov_invoice_all rename to SOV_INVOICE_ALL;
