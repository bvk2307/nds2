delete from NDS2_MRR_USER.SELECTION
where id in (999001, 999002, 999003, 999004, 999005, 999101);
delete from NDS2_MRR_USER.FAVORITE_FILTERS
where id in (999001, 999002, 999003, 999004, 999005);
delete from NDS2_MRR_USER.AUTOSELECTION_FILTER
where id in (999101);
delete from NDS2_MRR_USER.selection_filter
where id in (999001, 999002, 999003, 999004, 999005);
delete from NDS2_MRR_USER.CFG_AUTOSELECTION_FILTER;

insert into NDS2_MRR_USER.SELECTION (id, name, creation_date, modification_date, status_date, analytic, analytic_sid, chief, chief_sid, sender, status, type, decl_count, discrep_count, regions)
values (999001, 'Viborka test 001', to_date('21-10-2015 04:17:32', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-10-2015 04:48:41', 'dd-mm-yyyy hh24:mi:ss'), null, 'Analitik', 'S-1-5-21-2524094259-2834220340-594338894-1153', null, null, null, 1, 0, 0, 0, null);
insert into NDS2_MRR_USER.SELECTION (id, name, creation_date, modification_date, status_date, analytic, analytic_sid, chief, chief_sid, sender, status, type, decl_count, discrep_count, regions)
values (999002, 'Viborka test 002', to_date('21-10-2015 04:49:35', 'dd-mm-yyyy hh24:mi:ss'), null, null, 'Analitik', 'S-1-5-21-2524094259-2834220340-594338894-1153', null, null, null, 1, 0, 0, 0, null);
insert into NDS2_MRR_USER.SELECTION (id, name, creation_date, modification_date, status_date, analytic, analytic_sid, chief, chief_sid, sender, status, type, decl_count, discrep_count, regions)
values (999003, 'Viborka test 003', to_date('21-10-2015 04:52:06', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-10-2015 04:52:38', 'dd-mm-yyyy hh24:mi:ss'), null, 'Analitik', 'S-1-5-21-2524094259-2834220340-594338894-1153', null, null, null, 1, 0, 0, 0, null);
insert into NDS2_MRR_USER.SELECTION (id, name, creation_date, modification_date, status_date, analytic, analytic_sid, chief, chief_sid, sender, status, type, decl_count, discrep_count, regions)
values (999004, 'Viborka test 004', to_date('21-10-2015 04:53:11', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-10-2015 04:54:58', 'dd-mm-yyyy hh24:mi:ss'), null, 'Analitik', 'S-1-5-21-2524094259-2834220340-594338894-1153', null, null, null, 1, 0, 0, 0, null);
insert into NDS2_MRR_USER.SELECTION (id, name, creation_date, modification_date, status_date, analytic, analytic_sid, chief, chief_sid, sender, status, type, decl_count, discrep_count, regions)
values (999005, 'Viborka test 005', to_date('21-10-2015 04:58:03', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-10-2015 04:58:25', 'dd-mm-yyyy hh24:mi:ss'), null, 'Analitik', 'S-1-5-21-2524094259-2834220340-594338894-1153', null, null, null, 1, 0, 0, 0, null);

insert into NDS2_MRR_USER.SELECTION (id, name, creation_date, modification_date, status_date, analytic, analytic_sid, chief, chief_sid, sender, status, type, decl_count, discrep_count, regions)
values (999101, 'Viborka test 101', to_date('21-10-2015 04:53:11', 'dd-mm-yyyy hh24:mi:ss'), to_date('21-10-2015 04:54:58', 'dd-mm-yyyy hh24:mi:ss'), null, 'Analitik', 'S-1-5-21-2524094259-2834220340-594338894-1153', null, null, null, 1, 1, 0, 0, null);
commit;

insert into NDS2_MRR_USER.selection_filter (id, selection_id, filter)
values(999001, 999001, to_clob('<?xml version="1.0" encoding="utf-16"?>
<SelectionFilter xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <Id>0</Id>
  <SelectionId xsi:nil="true"/>
  <IsActive>false</IsActive>
  <IsFavourite>false</IsFavourite>
  <GroupsFilters>
    <GroupFilter>
      <Filters>
        <FilterCriteria>
          <Name>������</Name>
          <ValueTypeCode>RegionsMultiSelect</ValueTypeCode>
          <LookupTableName>v$ssrf</LookupTableName>
          <InternalName>Region</InternalName>
          <FirInternalName>region_code</FirInternalName>
          <IsRequired>true</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>16</ValueOne>
              <ValueTwo>16 - ���������� ��������� (���������)</ValueTwo>
            </FilterElement>
          </Values>
        </FilterCriteria>') || to_clob('
        <FilterCriteria>
          <Name>���������</Name>
          <ValueTypeCode>TaxAuthoritiesMultiSelect</ValueTypeCode>
          <LookupTableName>v$sono</LookupTableName>
          <InternalName>TaxOrgan</InternalName>
          <FirInternalName>sono_code</FirInternalName>
          <IsRequired>false</IsRequired>
          <IsActive>true</IsActive>
          <Values>') || to_clob('
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1651</ValueOne>
              <ValueTwo>1651 - ����������� ��������� ����������� ��������� ������ � 11 �� ���������� ���������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1674</ValueOne>
              <ValueTwo>1674 - ����������� ��������� ����������� ��������� ������ � 9 �� ���������� ���������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1683</ValueOne>
              <ValueTwo>1683 - ����������� ��������� ����������� ��������� ������ � 3 �� ���������� ���������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1689</ValueOne>
              <ValueTwo>1689 - ����������� ��������� ����������� ��������� ������ � 17 �� ���������� ���������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1681</ValueOne>
              <ValueTwo>1681 - ����������� ��������� ����������� ��������� ������ �� ���������� ������������������ �� ���������� ���������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1656</ValueOne>
              <ValueTwo>1656 - ����������� ��������� ����������� ��������� ������ � 19 �� ���������� ���������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1686</ValueOne>
              <ValueTwo>1686 - ����������� ��������� ����������� ��������� ������ � 6 �� ���������� ���������</ValueTwo>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1685</ValueOne>
              <ValueTwo>1685 - ����������� ��������� ����������� ��������� ������ � 5 �� ���������� ���������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1600</ValueOne>
              <ValueTwo>1600 - ���������� ����������� ��������� ������ �� ���������� ���������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1677</ValueOne>
              <ValueTwo>1677 - ����������� ��������� ����������� ��������� ������ � 12 �� ���������� ���������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1658</ValueOne>
              <ValueTwo>1658 - ��������� ����������� ��������� ������ �� ����������� ������ �. ������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1675</ValueOne>
              <ValueTwo>1675 - ����������� ��������� ����������� ��������� ������ � 10 �� ���������� ���������</ValueTwo>
            </FilterElement>
            <FilterElement>') || to_clob('
              <Operator>Equals</Operator>
              <ValueOne>1650</ValueOne>
              <ValueTwo>1650 - ��������� ����������� ��������� ������ �� �. ���������� ����� ���������� ���������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1673</ValueOne>
              <ValueTwo>1673 - ����������� ��������� ����������� ��������� ������ � 8 �� ���������� ���������</ValueTwo>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1684</ValueOne>
              <ValueTwo>1684 - ����������� ��������� ����������� ��������� ������ � 4 �� ���������� ���������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1644</ValueOne>
              <ValueTwo>1644 - ����������� ��������� ����������� ��������� ������ � 16 �� ���������� ���������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1655</ValueOne>
              <ValueTwo>1655 - ����������� ��������� ����������� ��������� ������ � 14 �� ���������� ���������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1690</ValueOne>
              <ValueTwo>1690 - ����������� ��������� ����������� ��������� ������ � 18 �� ���������� ���������</ValueTwo>
            </FilterElement>
          </Values>
        </FilterCriteria>') || to_clob('
        <FilterCriteria>
          <Name>��� ��</Name>
          <ValueTypeCode>TextMultipleInput</ValueTypeCode>
          <LookupTableName/>
          <InternalName>TaxPayerInn</InternalName>
          <FirInternalName>decl_inn</FirInternalName>
          <IsRequired>false</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>5252031528</ValueOne>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>3664088086</ValueOne>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>5609066470</ValueOne>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>5245000448</ValueOne>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>8614002935</ValueOne>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>5410037050</ValueOne>
            </FilterElement>
          </Values>
        </FilterCriteria>') || to_clob('
        <FilterCriteria>
          <Name>������������ ��</Name>
          <ValueTypeCode>EditWithComparisonStr</ValueTypeCode>
          <LookupTableName/>
          <InternalName>TaxPayerName</InternalName>
          <FirInternalName>name</FirInternalName>
          <IsRequired>false</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>
              <Operator>StartWith</Operator>
              <ValueOne>���</ValueOne>
              <ValueTwo/>
            </FilterElement>
          </Values>
        </FilterCriteria>
        <FilterCriteria>
          <Name>��� ����������</Name>
          <ValueTypeCode>ListMultpleChoice</ValueTypeCode>
          <LookupTableName>DICT_SUR</LookupTableName>
          <InternalName>Sur</InternalName>
          <FirInternalName>sur_code</FirInternalName>
          <IsRequired>false</IsRequired>
          <IsActive>true</IsActive>
          <Values>') || to_clob('
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>2</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>3</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>4</ValueOne>
              <ValueTwo/>
            </FilterElement>
          </Values>
        </FilterCriteria>
        <FilterCriteria>') || to_clob('
          <Name>��� ��������</Name>
          <ValueTypeCode>ListMultpleChoice</ValueTypeCode>
          <LookupTableName>DICT_SUR</LookupTableName>
          <InternalName>SurContractor</InternalName>
          <FirInternalName>contractor_sur_code</FirInternalName>
          <IsRequired>false</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>4</ValueOne>
              <ValueTwo/>
            </FilterElement>
          </Values>
        </FilterCriteria>
        <FilterCriteria>') || to_clob('
          <Name>�������� ������</Name>
          <ValueTypeCode>ListMultpleChoice</ValueTypeCode>
          <LookupTableName>DICT_NALOG_PERIODS</LookupTableName>
          <InternalName>TaxPeriod</InternalName>
          <FirInternalName>period</FirInternalName>
          <IsRequired>false</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>21</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>22</ValueOne>
              <ValueTwo/>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>23</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>24</ValueOne>
              <ValueTwo/>
            </FilterElement>
          </Values>
        </FilterCriteria>') || to_clob('
        <FilterCriteria>
          <Name>���� ������ ����������</Name>
          <ValueTypeCode>EditIntervalDates</ValueTypeCode>
          <LookupTableName/>
          <InternalName>DeclarationDate</InternalName>
          <FirInternalName>submit_date</FirInternalName>
          <IsRequired>false</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>
              <Operator>Between</Operator>
              <ValueOne>01.08.2013 0:00:00</ValueOne>
              <ValueTwo>21.10.2015 23:59:59</ValueTwo>
            </FilterElement>
          </Values>
        </FilterCriteria>') || to_clob('
        <FilterCriteria>
          <Name>����� ����� ����������� ��</Name>
          <ValueTypeCode>EditIntervalPrice</ValueTypeCode>
          <LookupTableName/>
          <InternalName>MismatchAmntTotal</InternalName>
          <FirInternalName>decl_total_amount</FirInternalName>
          <IsRequired>false</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>
              <Operator>Between</Operator>
              <ValueOne>0</ValueOne>
              <ValueTwo>999�999�999</ValueTwo>
            </FilterElement>
          </Values>
        </FilterCriteria>') || to_clob('
        <FilterCriteria>
          <Name>����������� ����� �����������</Name>
          <ValueTypeCode>EditIntervalPrice</ValueTypeCode>
          <LookupTableName/>
          <InternalName>MismatchAmntMin</InternalName>
          <FirInternalName>decl_min_amount</FirInternalName>
          <IsRequired>false</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>
              <Operator>Between</Operator>
              <ValueOne>0</ValueOne>
              <ValueTwo>999�999�999</ValueTwo>
            </FilterElement>
          </Values>
        </FilterCriteria>') || to_clob('
        <FilterCriteria>
          <Name>������������ ����� �����������</Name>
          <ValueTypeCode>EditIntervalPrice</ValueTypeCode>
          <LookupTableName/>
          <InternalName>MismatchAmntMax</InternalName>
          <FirInternalName>decl_max_amount</FirInternalName>
          <IsRequired>false</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>
              <Operator>Between</Operator>
              <ValueOne>0</ValueOne>
              <ValueTwo>7�777�777</ValueTwo>
            </FilterElement>
          </Values>
        </FilterCriteria>') || to_clob('
        <FilterCriteria>
          <Name>������� ����� �����������</Name>
          <ValueTypeCode>EditIntervalPrice</ValueTypeCode>
          <LookupTableName/>
          <InternalName>MismatchAmntAvg</InternalName>
          <FirInternalName>decl_avg_amount</FirInternalName>
          <IsRequired>false</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>
              <Operator>Between</Operator>
              <ValueOne>0</ValueOne>
              <ValueTwo>8�888�888</ValueTwo>
            </FilterElement>
          </Values>
        </FilterCriteria>
      </Filters>
      <IsActive>true</IsActive>
    </GroupFilter>
    <GroupFilter>') || to_clob('
      <Filters>
        <FilterCriteria>
          <Name>������</Name>
          <ValueTypeCode>RegionsMultiSelect</ValueTypeCode>
          <LookupTableName>v$ssrf</LookupTableName>
          <InternalName>Region</InternalName>
          <FirInternalName>region_code</FirInternalName>
          <IsRequired>true</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>16</ValueOne>
              <ValueTwo>16 - ���������� ��������� (���������)</ValueTwo>
            </FilterElement>
          </Values>
        </FilterCriteria>
        <FilterCriteria>') || to_clob('
          <Name>����� ���������� ����������� �� ��</Name>
          <ValueTypeCode>EditIntervalInt</ValueTypeCode>
          <LookupTableName/>
          <InternalName>MismatchCountTotal</InternalName>
          <FirInternalName>decl_total_count</FirInternalName>
          <IsRequired>false</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>
              <Operator>Between</Operator>
              <ValueOne>0</ValueOne>
              <ValueTwo>999999999</ValueTwo>
            </FilterElement>
          </Values>
        </FilterCriteria>
        <FilterCriteria>') || to_clob('
          <Name>����� ����������� �� ����� �������</Name>
          <ValueTypeCode>EditIntervalInt</ValueTypeCode>
          <LookupTableName/>
          <InternalName>MismatchCountBuyBook</InternalName>
          <FirInternalName>decl_purchase_count</FirInternalName>
          <IsRequired>false</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>
              <Operator>Between</Operator>
              <ValueOne>0</ValueOne>
              <ValueTwo>999999999</ValueTwo>
            </FilterElement>
          </Values>
        </FilterCriteria>
        <FilterCriteria>') || to_clob('
          <Name>����� ����������� �� ����� ������</Name>
          <ValueTypeCode>EditIntervalInt</ValueTypeCode>
          <LookupTableName/>
          <InternalName>MismatchCountSellBook</InternalName>
          <FirInternalName>decl_sales_count</FirInternalName>
          <IsRequired>false</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>
              <Operator>Between</Operator>
              <ValueOne>0</ValueOne>
              <ValueTwo>999999999</ValueTwo>
            </FilterElement>
          </Values>
        </FilterCriteria>
        <FilterCriteria>') || to_clob('
          <Name>������</Name>
          <ValueTypeCode>RegionsMultiSelect</ValueTypeCode>
          <LookupTableName>v$ssrf</LookupTableName>
          <InternalName>Region</InternalName>
          <FirInternalName>region_code</FirInternalName>
          <IsRequired>true</IsRequired>
          <IsActive>true</IsActive>
          <Values/>
        </FilterCriteria>
        <FilterCriteria>
          <Name>������</Name>
          <ValueTypeCode>RegionsMultiSelect</ValueTypeCode>
          <LookupTableName>v$ssrf</LookupTableName>
          <InternalName>Region</InternalName>
          <FirInternalName>region_code</FirInternalName>
          <IsRequired>true</IsRequired>
          <IsActive>true</IsActive>
          <Values/>
        </FilterCriteria>
        <FilterCriteria>
          <Name>������</Name>
          <ValueTypeCode>RegionsMultiSelect</ValueTypeCode>
          <LookupTableName>v$ssrf</LookupTableName>
          <InternalName>Region</InternalName>
          <FirInternalName>region_code</FirInternalName>
          <IsRequired>true</IsRequired>
          <IsActive>true</IsActive>
          <Values/>
        </FilterCriteria>') || to_clob('
        <FilterCriteria>
          <Name>����� ����� ��� �� ��</Name>
          <ValueTypeCode>EditIntervalInt</ValueTypeCode>
          <LookupTableName/>
          <InternalName>DeclarationPvpSumTotal</InternalName>
          <FirInternalName>decl_total_pvp</FirInternalName>
          <IsRequired>false</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>
              <Operator>Between</Operator>
              <ValueOne>0</ValueOne>
              <ValueTwo>999999999</ValueTwo>
            </FilterElement>
          </Values>
        </FilterCriteria>') || to_clob('
        <FilterCriteria>
          <Name>����������� ����� ��� ����������� � ��</Name>
          <ValueTypeCode>EditIntervalInt</ValueTypeCode>
          <LookupTableName/>
          <InternalName>DeclarationPvpSumMin</InternalName>
          <FirInternalName>decl_min_pvp</FirInternalName>
          <IsRequired>false</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>
              <Operator>Between</Operator>
              <ValueOne>0</ValueOne>
              <ValueTwo>999999999</ValueTwo>
            </FilterElement>
          </Values>
        </FilterCriteria>') || to_clob('
        <FilterCriteria>
          <Name>������������ ����� ��� ����������� � ��</Name>
          <ValueTypeCode>EditIntervalInt</ValueTypeCode>
          <LookupTableName/>
          <InternalName>DeclarationPvpSumMax</InternalName>
          <FirInternalName>decl_max_pvp</FirInternalName>
          <IsRequired>false</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>
              <Operator>Between</Operator>
              <ValueOne>0</ValueOne>
              <ValueTwo>999999999</ValueTwo>
            </FilterElement>
          </Values>
        </FilterCriteria>') || to_clob('
        <FilterCriteria>
          <Name>����� ��� �� ����� �������</Name>
          <ValueTypeCode>EditIntervalInt</ValueTypeCode>
          <LookupTableName/>
          <InternalName>BuyBookPvpSum</InternalName>
          <FirInternalName>decl_purchase_pvp</FirInternalName>
          <IsRequired>false</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>
              <Operator>Between</Operator>
              <ValueOne>0</ValueOne>
              <ValueTwo>999999999</ValueTwo>
            </FilterElement>
          </Values>
        </FilterCriteria>') || to_clob('
        <FilterCriteria>
          <Name>����� ��� �� ����� ������</Name>
          <ValueTypeCode>EditIntervalInt</ValueTypeCode>
          <LookupTableName/>
          <InternalName>SellBookPvpSum</InternalName>
          <FirInternalName>decl_sales_pvp</FirInternalName>
          <IsRequired>false</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>
              <Operator>Between</Operator>
              <ValueOne>0</ValueOne>
              <ValueTwo>999999999</ValueTwo>
            </FilterElement>
          </Values>
        </FilterCriteria>') || to_clob('
        <FilterCriteria>
          <Name>��� �����������</Name>
          <ValueTypeCode>ListMultpleChoice</ValueTypeCode>
          <LookupTableName>DICT_DISCREPANCY_TYPE</LookupTableName>
          <InternalName>DiscrepancyKind</InternalName>
          <FirInternalName>type_code</FirInternalName>
          <IsRequired>false</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>2</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>3</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>4</ValueOne>
              <ValueTwo/>
            </FilterElement>
          </Values>
        </FilterCriteria>') || to_clob('
        <FilterCriteria>
          <Name>��� �����������</Name>
          <ValueTypeCode>EditIntervalInt</ValueTypeCode>
          <LookupTableName/>
          <InternalName>DiscrepancyPvp</InternalName>
          <FirInternalName>pvp</FirInternalName>
          <IsRequired>false</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>
              <Operator>Between</Operator>
              <ValueOne>0</ValueOne>
              <ValueTwo>999999990</ValueTwo>
            </FilterElement>
          </Values>
        </FilterCriteria>') || to_clob('
        <FilterCriteria>
          <Name>�������� � ������ �������</Name>
          <ValueTypeCode>List</ValueTypeCode>
          <LookupTableName>DICT_SELECTED_STATE</LookupTableName>
          <InternalName>DiscrepancyStatus</InternalName>
          <FirInternalName>is_in_other_selections</FirInternalName>
          <IsRequired>false</IsRequired>
          <IsActive>false</IsActive>
          <Values>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1</ValueOne>
              <ValueTwo/>
            </FilterElement>
          </Values>
        </FilterCriteria>') || to_clob('
        <FilterCriteria>
          <Name>���� �����������</Name>
          <ValueTypeCode>EditIntervalDates</ValueTypeCode>
          <LookupTableName/>
          <InternalName>MismatchDate</InternalName>
          <FirInternalName>create_date</FirInternalName>
          <IsRequired>false</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>
              <Operator>Between</Operator>
              <ValueOne>01.08.2013 0:00:00</ValueOne>
              <ValueTwo>21.12.2015 23:59:59</ValueTwo>
            </FilterElement>
          </Values>
        </FilterCriteria>') || to_clob('
        <FilterCriteria>
          <Name>������� �������������</Name>
          <ValueTypeCode>ListMultpleChoice</ValueTypeCode>
          <LookupTableName>DICT_COMPARE_RULES</LookupTableName>
          <InternalName>CompareRule</InternalName>
          <FirInternalName>compare_rule</FirInternalName>
          <IsRequired>false</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>2</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>') || to_clob('
              <Operator>Equals</Operator>
              <ValueOne>3</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>4</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>5</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>6</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>7</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>8</ValueOne>
              <ValueTwo/>
            </FilterElement>
          </Values>
        </FilterCriteria>') || to_clob('
        <FilterCriteria>
          <Name>�������� ������ � �� �� ������� ����������</Name>
          <ValueTypeCode>List</ValueTypeCode>
          <LookupTableName>DICT_CHAPTER1</LookupTableName>
          <InternalName>Side1ChapterRule</InternalName>
          <FirInternalName>invoice_chapter</FirInternalName>
          <IsRequired>false</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>8</ValueOne>
              <ValueTwo/>
            </FilterElement>
          </Values>
        </FilterCriteria>
        <FilterCriteria>') || to_clob('
          <Name>�������� ������ � �� �� ������� ��������</Name>
          <ValueTypeCode>List</ValueTypeCode>
          <LookupTableName>DICT_CHAPTER2</LookupTableName>
          <InternalName>Side2ChapterRule</InternalName>
          <FirInternalName>invoice_contractor_chapter</FirInternalName>
          <IsRequired>false</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>9</ValueOne>
              <ValueTwo/>
            </FilterElement>
          </Values>
        </FilterCriteria>
        <FilterCriteria>') || to_clob('
          <Name>������� �� �� ������� ����������</Name>
          <ValueTypeCode>List</ValueTypeCode>
          <LookupTableName>DICT_MARK1</LookupTableName>
          <InternalName>Side1MarkRule</InternalName>
          <FirInternalName>side1mark_rule</FirInternalName>
          <IsRequired>false</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1</ValueOne>
              <ValueTwo/>
            </FilterElement>
          </Values>
        </FilterCriteria>') || to_clob('
        <FilterCriteria>
          <Name>������� �� �� ������� ��������</Name>
          <ValueTypeCode>List</ValueTypeCode>
          <LookupTableName>DICT_MARK2</LookupTableName>
          <InternalName>Side2MarkRule</InternalName>
          <FirInternalName>side2mark_rule</FirInternalName>
          <IsRequired>false</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>2</ValueOne>
              <ValueTwo/>
            </FilterElement>
          </Values>
        </FilterCriteria>') || to_clob('
        <FilterCriteria>
          <Name>����� ��� �� ������� ����������</Name>
          <ValueTypeCode>EditIntervalPrice</ValueTypeCode>
          <LookupTableName/>
          <InternalName>Side1NdsAmount</InternalName>
          <FirInternalName>side1nds_amount</FirInternalName>
          <IsRequired>false</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>
              <Operator>Between</Operator>
              <ValueOne>0</ValueOne>
              <ValueTwo>999�999�999</ValueTwo>
            </FilterElement>
          </Values>
        </FilterCriteria>') || to_clob('
        <FilterCriteria>
          <Name>����������� ��� ������� ����������</Name>
          <ValueTypeCode>TextMultipleInput</ValueTypeCode>
          <LookupTableName/>
          <InternalName>ExcludeSide1Inn</InternalName>
          <FirInternalName>inn</FirInternalName>
          <IsRequired>false</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1111111111</ValueOne>
            </FilterElement>
          </Values>
        </FilterCriteria>') || to_clob('
        <FilterCriteria>
          <Name>����������� ��� ������� ��������</Name>
          <ValueTypeCode>TextMultipleInput</ValueTypeCode>
          <LookupTableName/>
          <InternalName>ExcludeSide2Inn</InternalName>
          <FirInternalName>contractor_inn</FirInternalName>
          <IsRequired>false</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>2222222222</ValueOne>
            </FilterElement>
          </Values>
        </FilterCriteria>') || to_clob('
        <FilterCriteria>
          <Name>���� ���� �������� �� ������� ����������</Name>
          <ValueTypeCode>ListMultipleCompirison</ValueTypeCode>
          <LookupTableName>DICT_OPERATION_CODES</LookupTableName>
          <InternalName>Side1OperationCodes</InternalName>
          <FirInternalName>side1operationCodes</FirInternalName>
          <IsRequired>false</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>1</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>2</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>4</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>8</ValueOne>
              <ValueTwo/>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>16</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>32</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>64</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>128</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>256</ValueOne>
              <ValueTwo/>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>512</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>1024</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>2048</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>4096</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>32768</ValueOne>
              <ValueTwo/>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>65536</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>131072</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>262144</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>524288</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>') || to_clob('
              <Operator>Contains</Operator>
              <ValueOne>1048576</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>2097152</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>4194304</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>8388608</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>16777216</ValueOne>
              <ValueTwo/>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>33554432</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>67108864</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>134217728</ValueOne>
              <ValueTwo/>
            </FilterElement>
          </Values>
        </FilterCriteria>
        <FilterCriteria>') || to_clob('
          <Name>���� ���� �������� �� ������� ��������</Name>
          <ValueTypeCode>ListMultipleCompirison</ValueTypeCode>
          <LookupTableName>DICT_OPERATION_CODES</LookupTableName>
          <InternalName>Side2OperationCodes</InternalName>
          <FirInternalName>side2operationCodes</FirInternalName>
          <IsRequired>false</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>1</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>2</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>4</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>') || to_clob('
              <Operator>Contains</Operator>
              <ValueOne>8</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>16</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>32</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>64</ValueOne>
              <ValueTwo/>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>128</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>256</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>512</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>1024</ValueOne>
              <ValueTwo/>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>2048</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>4096</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>32768</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>65536</ValueOne>
              <ValueTwo/>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>131072</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>262144</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>524288</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>1048576</ValueOne>
              <ValueTwo/>') || to_clob('
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>2097152</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>4194304</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>8388608</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>16777216</ValueOne>
              <ValueTwo/>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>33554432</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>67108864</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>134217728</ValueOne>
              <ValueTwo/>') || to_clob('
            </FilterElement>
          </Values>
        </FilterCriteria>
      </Filters>
      <IsActive>true</IsActive>
    </GroupFilter>
  </GroupsFilters>
</SelectionFilter>'));

insert into NDS2_MRR_USER.selection_filter (id, selection_id, filter)
values(999002, 999002, to_clob('<?xml version="1.0" encoding="utf-16"?>
<SelectionFilter xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <Id>0</Id>
  <SelectionId xsi:nil="true"/>
  <IsActive>false</IsActive>
  <IsFavourite>false</IsFavourite>
  <GroupsFilters>
    <GroupFilter>
      <Filters>
        <FilterCriteria>
          <Name>������</Name>
          <ValueTypeCode>RegionsMultiSelect</ValueTypeCode>
          <LookupTableName>v$ssrf</LookupTableName>
          <InternalName>Region</InternalName>
          <FirInternalName>region_code</FirInternalName>
          <IsRequired>true</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>16</ValueOne>
              <ValueTwo>16 - ���������� ��������� (���������)</ValueTwo>
            </FilterElement>
          </Values>
        </FilterCriteria>') || to_clob('
        <FilterCriteria>
          <Name>���������</Name>
          <ValueTypeCode>TaxAuthoritiesMultiSelect</ValueTypeCode>
          <LookupTableName>v$sono</LookupTableName>
          <InternalName>TaxOrgan</InternalName>
          <FirInternalName>sono_code</FirInternalName>
          <IsRequired>false</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1683</ValueOne>
              <ValueTwo>1683 - ����������� ��������� ����������� ��������� ������ � 3 �� ���������� ���������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1651</ValueOne>
              <ValueTwo>1651 - ����������� ��������� ����������� ��������� ������ � 11 �� ���������� ���������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1674</ValueOne>
              <ValueTwo>1674 - ����������� ��������� ����������� ��������� ������ � 9 �� ���������� ���������</ValueTwo>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1689</ValueOne>
              <ValueTwo>1689 - ����������� ��������� ����������� ��������� ������ � 17 �� ���������� ���������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1681</ValueOne>
              <ValueTwo>1681 - ����������� ��������� ����������� ��������� ������ �� ���������� ������������������ �� ���������� ���������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1686</ValueOne>
              <ValueTwo>1686 - ����������� ��������� ����������� ��������� ������ � 6 �� ���������� ���������</ValueTwo>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1656</ValueOne>
              <ValueTwo>1656 - ����������� ��������� ����������� ��������� ������ � 19 �� ���������� ���������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1685</ValueOne>
              <ValueTwo>1685 - ����������� ��������� ����������� ��������� ������ � 5 �� ���������� ���������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1600</ValueOne>
              <ValueTwo>1600 - ���������� ����������� ��������� ������ �� ���������� ���������</ValueTwo>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1677</ValueOne>
              <ValueTwo>1677 - ����������� ��������� ����������� ��������� ������ � 12 �� ���������� ���������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1675</ValueOne>
              <ValueTwo>1675 - ����������� ��������� ����������� ��������� ������ � 10 �� ���������� ���������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1658</ValueOne>
              <ValueTwo>1658 - ��������� ����������� ��������� ������ �� ����������� ������ �. ������</ValueTwo>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1684</ValueOne>
              <ValueTwo>1684 - ����������� ��������� ����������� ��������� ������ � 4 �� ���������� ���������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1650</ValueOne>
              <ValueTwo>1650 - ��������� ����������� ��������� ������ �� �. ���������� ����� ���������� ���������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1673</ValueOne>
              <ValueTwo>1673 - ����������� ��������� ����������� ��������� ������ � 8 �� ���������� ���������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1655</ValueOne>
              <ValueTwo>1655 - ����������� ��������� ����������� ��������� ������ � 14 �� ���������� ���������</ValueTwo>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1690</ValueOne>
              <ValueTwo>1690 - ����������� ��������� ����������� ��������� ������ � 18 �� ���������� ���������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1644</ValueOne>
              <ValueTwo>1644 - ����������� ��������� ����������� ��������� ������ � 16 �� ���������� ���������</ValueTwo>
            </FilterElement>
          </Values>
        </FilterCriteria>') || to_clob('
        <FilterCriteria>
          <Name>�������� � ������ �������</Name>
          <ValueTypeCode>List</ValueTypeCode>
          <LookupTableName>DICT_SELECTED_STATE</LookupTableName>
          <InternalName>DiscrepancyStatus</InternalName>
          <FirInternalName>is_in_other_selections</FirInternalName>
          <IsRequired>false</IsRequired>
          <IsActive>false</IsActive>
          <Values>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1</ValueOne>
              <ValueTwo/>
            </FilterElement>
          </Values>
        </FilterCriteria>
      </Filters>
      <IsActive>true</IsActive>
    </GroupFilter>
  </GroupsFilters>
</SelectionFilter>'));

insert into NDS2_MRR_USER.selection_filter (id, selection_id, filter)
values(999003, 999003, to_clob('<?xml version="1.0" encoding="utf-16"?>
<SelectionFilter xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <Id>0</Id>
  <SelectionId xsi:nil="true"/>
  <IsActive>false</IsActive>
  <IsFavourite>false</IsFavourite>
  <GroupsFilters>
    <GroupFilter>
      <Filters>
        <FilterCriteria>') || to_clob('
          <Name>������</Name>
          <ValueTypeCode>RegionsMultiSelect</ValueTypeCode>
          <LookupTableName>v$ssrf</LookupTableName>
          <InternalName>Region</InternalName>
          <FirInternalName>region_code</FirInternalName>
          <IsRequired>true</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>05</ValueOne>
              <ValueTwo>05 - ���������� ��������</ValueTwo>
            </FilterElement>
          </Values>
        </FilterCriteria>') || to_clob('
        <FilterCriteria>
          <Name>���������</Name>
          <ValueTypeCode>TaxAuthoritiesMultiSelect</ValueTypeCode>
          <LookupTableName>v$sono</LookupTableName>
          <InternalName>TaxOrgan</InternalName>
          <FirInternalName>sono_code</FirInternalName>
          <IsRequired>false</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>0571</ValueOne>
              <ValueTwo>0571 - ��������� ����������� ��������� ������ �� ���������� ������ �.���������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>0541</ValueOne>
              <ValueTwo>0541 - ����������� ��������� ����������� ��������� ������ �� ���������� ������������������ �� ���������� ��������</ValueTwo>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>0523</ValueOne>
              <ValueTwo>0523 - ����������� ��������� ����������� ��������� ������ � 1 �� ���������� ��������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>0544</ValueOne>
              <ValueTwo>0544 - ����������� ��������� ����������� ��������� ������ � 17 �� ���������� ��������</ValueTwo>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>0531</ValueOne>
              <ValueTwo>0531 - ����������� ��������� ����������� ��������� ������ � 16 �� ���������� ��������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>0545</ValueOne>
              <ValueTwo>0545 - ��������� ����������� ��������� ������ �� �.��������� ���������� ��������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>0500</ValueOne>
              <ValueTwo>0500 - ���������� ����������� ��������� ������ �� ���������� ��������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>0521</ValueOne>
              <ValueTwo>0521 - ����������� ��������� ����������� ��������� ������ � 9 �� ���������� ��������</ValueTwo>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>0548</ValueOne>
              <ValueTwo>0548 - ����������� ��������� ����������� ��������� ������ � 6 �� ���������� ��������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>0522</ValueOne>
              <ValueTwo>0522 - ����������� ��������� ����������� ��������� ������ � 5 �� ���������� ��������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>0533</ValueOne>
              <ValueTwo>0533 - ����������� ��������� ����������� ��������� ������ � 10 �� ���������� ��������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>0536</ValueOne>
              <ValueTwo>0536 - ����������� ��������� ����������� ��������� ������ � 13 �� ���������� ��������</ValueTwo>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>0550</ValueOne>
              <ValueTwo>0550 - ����������� ��������� ����������� ��������� ������ � 4 �� ���������� ��������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>0507</ValueOne>
              <ValueTwo>0507 - ����������� ��������� ����������� ��������� ������ � 7 �� ���������� ��������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>0506</ValueOne>
              <ValueTwo>0506 - ����������� ��������� ����������� ��������� ������ � 12 �� ���������� ��������</ValueTwo>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>0546</ValueOne>
              <ValueTwo>0546 - ����������� ��������� ����������� ��������� ������ � 8 �� ���������� ��������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>0542</ValueOne>
              <ValueTwo>0542 - ����������� ��������� ����������� ��������� ������ � 3 �� ���������� ��������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>0572</ValueOne>
              <ValueTwo>0572 - ��������� ����������� ��������� ������ �� ���������� ������ �.���������</ValueTwo>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>0547</ValueOne>
              <ValueTwo>0547 - ����������� ��������� ����������� ��������� ������ � 15 �� ���������� ��������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>0529</ValueOne>
              <ValueTwo>0529 - ����������� ��������� ����������� ��������� ������ � 2 �� ���������� ��������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>0573</ValueOne>
              <ValueTwo>0573 - ��������� ����������� ��������� ������ �� ���������� ������ �.���������</ValueTwo>
            </FilterElement>
          </Values>
        </FilterCriteria>') || to_clob('
        <FilterCriteria>
          <Name>��� ����������</Name>
          <ValueTypeCode>ListMultpleChoice</ValueTypeCode>
          <LookupTableName>DICT_SUR</LookupTableName>
          <InternalName>Sur</InternalName>
          <FirInternalName>sur_code</FirInternalName>
          <IsRequired>false</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>2</ValueOne>
              <ValueTwo/>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>3</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>4</ValueOne>
              <ValueTwo/>
            </FilterElement>
          </Values>
        </FilterCriteria>') || to_clob('
        <FilterCriteria>
          <Name>��� �����������</Name>
          <ValueTypeCode>ListMultpleChoice</ValueTypeCode>
          <LookupTableName>DICT_DISCREPANCY_TYPE</LookupTableName>
          <InternalName>DiscrepancyKind</InternalName>
          <FirInternalName>type_code</FirInternalName>
          <IsRequired>false</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>2</ValueOne>
              <ValueTwo/>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>3</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>4</ValueOne>
              <ValueTwo/>
            </FilterElement>
          </Values>
        </FilterCriteria>
      </Filters>
      <IsActive>true</IsActive>
    </GroupFilter>') || to_clob('
    <GroupFilter>
      <Filters>
        <FilterCriteria>
          <Name>������</Name>
          <ValueTypeCode>RegionsMultiSelect</ValueTypeCode>
          <LookupTableName>v$ssrf</LookupTableName>
          <InternalName>Region</InternalName>
          <FirInternalName>region_code</FirInternalName>
          <IsRequired>true</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>16</ValueOne>
              <ValueTwo>16 - ���������� ��������� (���������)</ValueTwo>
            </FilterElement>
          </Values>
        </FilterCriteria>') || to_clob('
        <FilterCriteria>
          <Name>���������</Name>
          <ValueTypeCode>TaxAuthoritiesMultiSelect</ValueTypeCode>
          <LookupTableName>v$sono</LookupTableName>
          <InternalName>TaxOrgan</InternalName>
          <FirInternalName>sono_code</FirInternalName>
          <IsRequired>false</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1683</ValueOne>
              <ValueTwo>1683 - ����������� ��������� ����������� ��������� ������ � 3 �� ���������� ���������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1651</ValueOne>
              <ValueTwo>1651 - ����������� ��������� ����������� ��������� ������ � 11 �� ���������� ���������</ValueTwo>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1674</ValueOne>
              <ValueTwo>1674 - ����������� ��������� ����������� ��������� ������ � 9 �� ���������� ���������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1689</ValueOne>
              <ValueTwo>1689 - ����������� ��������� ����������� ��������� ������ � 17 �� ���������� ���������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1681</ValueOne>
              <ValueTwo>1681 - ����������� ��������� ����������� ��������� ������ �� ���������� ������������������ �� ���������� ���������</ValueTwo>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1686</ValueOne>
              <ValueTwo>1686 - ����������� ��������� ����������� ��������� ������ � 6 �� ���������� ���������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1656</ValueOne>
              <ValueTwo>1656 - ����������� ��������� ����������� ��������� ������ � 19 �� ���������� ���������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1685</ValueOne>
              <ValueTwo>1685 - ����������� ��������� ����������� ��������� ������ � 5 �� ���������� ���������</ValueTwo>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1600</ValueOne>
              <ValueTwo>1600 - ���������� ����������� ��������� ������ �� ���������� ���������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1677</ValueOne>
              <ValueTwo>1677 - ����������� ��������� ����������� ��������� ������ � 12 �� ���������� ���������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1675</ValueOne>
              <ValueTwo>1675 - ����������� ��������� ����������� ��������� ������ � 10 �� ���������� ���������</ValueTwo>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1658</ValueOne>
              <ValueTwo>1658 - ��������� ����������� ��������� ������ �� ����������� ������ �. ������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1684</ValueOne>
              <ValueTwo>1684 - ����������� ��������� ����������� ��������� ������ � 4 �� ���������� ���������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1650</ValueOne>
              <ValueTwo>1650 - ��������� ����������� ��������� ������ �� �. ���������� ����� ���������� ���������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1673</ValueOne>
              <ValueTwo>1673 - ����������� ��������� ����������� ��������� ������ � 8 �� ���������� ���������</ValueTwo>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1655</ValueOne>
              <ValueTwo>1655 - ����������� ��������� ����������� ��������� ������ � 14 �� ���������� ���������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1690</ValueOne>
              <ValueTwo>1690 - ����������� ��������� ����������� ��������� ������ � 18 �� ���������� ���������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1644</ValueOne>
              <ValueTwo>1644 - ����������� ��������� ����������� ��������� ������ � 16 �� ���������� ���������</ValueTwo>
            </FilterElement>
          </Values>
        </FilterCriteria>') || to_clob('
        <FilterCriteria>
          <Name>��� �����������</Name>
          <ValueTypeCode>ListMultpleChoice</ValueTypeCode>
          <LookupTableName>DICT_DISCREPANCY_TYPE</LookupTableName>
          <InternalName>DiscrepancyKind</InternalName>
          <FirInternalName>type_code</FirInternalName>
          <IsRequired>false</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>2</ValueOne>
              <ValueTwo/>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>3</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>4</ValueOne>
              <ValueTwo/>
            </FilterElement>
          </Values>
        </FilterCriteria>') || to_clob('
      </Filters>
      <IsActive>true</IsActive>
    </GroupFilter>
  </GroupsFilters>
</SelectionFilter>'));

insert into NDS2_MRR_USER.selection_filter (id, selection_id, filter)
values(999004, 999004, to_clob('<?xml version="1.0" encoding="utf-16"?>
<SelectionFilter xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <Id>0</Id>
  <SelectionId xsi:nil="true"/>
  <IsActive>false</IsActive>
  <IsFavourite>false</IsFavourite>
  <GroupsFilters>
    <GroupFilter>
      <Filters>
        <FilterCriteria>
          <Name>������</Name>
          <ValueTypeCode>RegionsMultiSelect</ValueTypeCode>
          <LookupTableName>v$ssrf</LookupTableName>
          <InternalName>Region</InternalName>
          <FirInternalName>region_code</FirInternalName>
          <IsRequired>true</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>16</ValueOne>
              <ValueTwo>16 - ���������� ��������� (���������)</ValueTwo>
            </FilterElement>
          </Values>
        </FilterCriteria>') || to_clob('
        <FilterCriteria>
          <Name>����� ����� ����������� ��</Name>
          <ValueTypeCode>EditIntervalPrice</ValueTypeCode>
          <LookupTableName/>
          <InternalName>MismatchAmntTotal</InternalName>
          <FirInternalName>decl_total_amount</FirInternalName>
          <IsRequired>false</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>
              <Operator>Between</Operator>
              <ValueOne>0</ValueOne>
              <ValueTwo>999�999�999</ValueTwo>
            </FilterElement>
          </Values>
        </FilterCriteria>
      </Filters>
      <IsActive>true</IsActive>
    </GroupFilter>') || to_clob('
    <GroupFilter>
      <Filters>
        <FilterCriteria>
          <Name>������</Name>
          <ValueTypeCode>RegionsMultiSelect</ValueTypeCode>
          <LookupTableName>v$ssrf</LookupTableName>
          <InternalName>Region</InternalName>
          <FirInternalName>region_code</FirInternalName>
          <IsRequired>true</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>05</ValueOne>
              <ValueTwo>05 - ���������� ��������</ValueTwo>
            </FilterElement>
          </Values>
        </FilterCriteria>') || to_clob('
        <FilterCriteria>
          <Name>����� ����� ����������� ��</Name>
          <ValueTypeCode>EditIntervalPrice</ValueTypeCode>
          <LookupTableName/>
          <InternalName>MismatchAmntTotal</InternalName>
          <FirInternalName>decl_total_amount</FirInternalName>
          <IsRequired>false</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>
              <Operator>Between</Operator>
              <ValueOne>0</ValueOne>
              <ValueTwo>999�999�999</ValueTwo>
            </FilterElement>
          </Values>
        </FilterCriteria>') || to_clob('
      </Filters>
      <IsActive>true</IsActive>
    </GroupFilter>
    <GroupFilter>
      <Filters>
        <FilterCriteria>
          <Name>������</Name>
          <ValueTypeCode>RegionsMultiSelect</ValueTypeCode>
          <LookupTableName>v$ssrf</LookupTableName>
          <InternalName>Region</InternalName>
          <FirInternalName>region_code</FirInternalName>
          <IsRequired>true</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>14</ValueOne>
              <ValueTwo>14 - ���������� ���� (������)</ValueTwo>
            </FilterElement>') || to_clob('
          </Values>
        </FilterCriteria>
        <FilterCriteria>
          <Name>����� ����� ����������� ��</Name>
          <ValueTypeCode>EditIntervalPrice</ValueTypeCode>
          <LookupTableName/>
          <InternalName>MismatchAmntTotal</InternalName>
          <FirInternalName>decl_total_amount</FirInternalName>
          <IsRequired>false</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>
              <Operator>Between</Operator>
              <ValueOne>0</ValueOne>
              <ValueTwo>999�999�999</ValueTwo>
            </FilterElement>') || to_clob('
          </Values>
        </FilterCriteria>
      </Filters>
      <IsActive>true</IsActive>
    </GroupFilter>
  </GroupsFilters>
</SelectionFilter>'));

insert into NDS2_MRR_USER.selection_filter (id, selection_id, filter)
values(999005, 999005, to_clob('<?xml version="1.0" encoding="utf-16"?>
<SelectionFilter xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <Id>0</Id>
  <SelectionId xsi:nil="true"/>
  <IsActive>false</IsActive>
  <IsFavourite>false</IsFavourite>
  <GroupsFilters>
    <GroupFilter>
      <Filters>
        <FilterCriteria>
          <Name>������</Name>
          <ValueTypeCode>RegionsMultiSelect</ValueTypeCode>
          <LookupTableName>v$ssrf</LookupTableName>
          <InternalName>Region</InternalName>
          <FirInternalName>region_code</FirInternalName>
          <IsRequired>true</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>') || to_clob('
              <Operator>Equals</Operator>
              <ValueOne>16</ValueOne>
              <ValueTwo>16 - ���������� ��������� (���������)</ValueTwo>
            </FilterElement>
          </Values>
        </FilterCriteria>
        <FilterCriteria>
          <Name>���� ���� �������� �� ������� ����������</Name>
          <ValueTypeCode>ListMultipleCompirison</ValueTypeCode>
          <LookupTableName>DICT_OPERATION_CODES</LookupTableName>
          <InternalName>Side1OperationCodes</InternalName>
          <FirInternalName>side1operationCodes</FirInternalName>
          <IsRequired>false</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>') || to_clob('
              <Operator>Contains</Operator>
              <ValueOne>1</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>2</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>4</ValueOne>
              <ValueTwo/>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>8</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>16</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>32</ValueOne>
              <ValueTwo/>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>64</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>128</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>256</ValueOne>
              <ValueTwo/>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>512</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>1024</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>2048</ValueOne>
              <ValueTwo/>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>4096</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>32768</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>65536</ValueOne>
              <ValueTwo/>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>131072</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>262144</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>524288</ValueOne>
              <ValueTwo/>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>1048576</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>2097152</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>4194304</ValueOne>
              <ValueTwo/>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>8388608</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>16777216</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>33554432</ValueOne>
              <ValueTwo/>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>67108864</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>134217728</ValueOne>
              <ValueTwo/>
            </FilterElement>
          </Values>
        </FilterCriteria>
      </Filters>
      <IsActive>true</IsActive>
    </GroupFilter>') || to_clob('
    <GroupFilter>
      <Filters>
        <FilterCriteria>
          <Name>������</Name>
          <ValueTypeCode>RegionsMultiSelect</ValueTypeCode>
          <LookupTableName>v$ssrf</LookupTableName>
          <InternalName>Region</InternalName>
          <FirInternalName>region_code</FirInternalName>
          <IsRequired>true</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>05</ValueOne>
              <ValueTwo>05 - ���������� ��������</ValueTwo>
            </FilterElement>
          </Values>
        </FilterCriteria>') || to_clob('
        <FilterCriteria>
          <Name>���������</Name>
          <ValueTypeCode>TaxAuthoritiesMultiSelect</ValueTypeCode>
          <LookupTableName>v$sono</LookupTableName>
          <InternalName>TaxOrgan</InternalName>
          <FirInternalName>sono_code</FirInternalName>
          <IsRequired>false</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>0571</ValueOne>
              <ValueTwo>0571 - ��������� ����������� ��������� ������ �� ���������� ������ �.���������</ValueTwo>
            </FilterElement>
            <FilterElement>') || to_clob('
              <Operator>Equals</Operator>
              <ValueOne>0541</ValueOne>
              <ValueTwo>0541 - ����������� ��������� ����������� ��������� ������ �� ���������� ������������������ �� ���������� ��������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>0523</ValueOne>
              <ValueTwo>0523 - ����������� ��������� ����������� ��������� ������ � 1 �� ���������� ��������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>0544</ValueOne>
              <ValueTwo>0544 - ����������� ��������� ����������� ��������� ������ � 17 �� ���������� ��������</ValueTwo>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>0531</ValueOne>
              <ValueTwo>0531 - ����������� ��������� ����������� ��������� ������ � 16 �� ���������� ��������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>0545</ValueOne>
              <ValueTwo>0545 - ��������� ����������� ��������� ������ �� �.��������� ���������� ��������</ValueTwo>
            </FilterElement>
            <FilterElement>') || to_clob('
              <Operator>Equals</Operator>
              <ValueOne>0500</ValueOne>
              <ValueTwo>0500 - ���������� ����������� ��������� ������ �� ���������� ��������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>0521</ValueOne>
              <ValueTwo>0521 - ����������� ��������� ����������� ��������� ������ � 9 �� ���������� ��������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>0548</ValueOne>
              <ValueTwo>0548 - ����������� ��������� ����������� ��������� ������ � 6 �� ���������� ��������</ValueTwo>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>0522</ValueOne>
              <ValueTwo>0522 - ����������� ��������� ����������� ��������� ������ � 5 �� ���������� ��������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>0533</ValueOne>
              <ValueTwo>0533 - ����������� ��������� ����������� ��������� ������ � 10 �� ���������� ��������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>0536</ValueOne>
              <ValueTwo>0536 - ����������� ��������� ����������� ��������� ������ � 13 �� ���������� ��������</ValueTwo>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>0550</ValueOne>
              <ValueTwo>0550 - ����������� ��������� ����������� ��������� ������ � 4 �� ���������� ��������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>0507</ValueOne>
              <ValueTwo>0507 - ����������� ��������� ����������� ��������� ������ � 7 �� ���������� ��������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>0506</ValueOne>
              <ValueTwo>0506 - ����������� ��������� ����������� ��������� ������ � 12 �� ���������� ��������</ValueTwo>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>0546</ValueOne>
              <ValueTwo>0546 - ����������� ��������� ����������� ��������� ������ � 8 �� ���������� ��������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>0542</ValueOne>
              <ValueTwo>0542 - ����������� ��������� ����������� ��������� ������ � 3 �� ���������� ��������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>0572</ValueOne>
              <ValueTwo>0572 - ��������� ����������� ��������� ������ �� ���������� ������ �.���������</ValueTwo>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>0547</ValueOne>
              <ValueTwo>0547 - ����������� ��������� ����������� ��������� ������ � 15 �� ���������� ��������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>0529</ValueOne>
              <ValueTwo>0529 - ����������� ��������� ����������� ��������� ������ � 2 �� ���������� ��������</ValueTwo>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>0573</ValueOne>
              <ValueTwo>0573 - ��������� ����������� ��������� ������ �� ���������� ������ �.���������</ValueTwo>
            </FilterElement>') || to_clob('
          </Values>
        </FilterCriteria>
        <FilterCriteria>
          <Name>���� ���� �������� �� ������� ��������</Name>
          <ValueTypeCode>ListMultipleCompirison</ValueTypeCode>
          <LookupTableName>DICT_OPERATION_CODES</LookupTableName>
          <InternalName>Side2OperationCodes</InternalName>
          <FirInternalName>side2operationCodes</FirInternalName>
          <IsRequired>false</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>') || to_clob('
              <Operator>Contains</Operator>
              <ValueOne>1</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>2</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>4</ValueOne>
              <ValueTwo/>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>8</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>16</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>32</ValueOne>
              <ValueTwo/>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>64</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>128</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>256</ValueOne>
              <ValueTwo/>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>512</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>1024</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>2048</ValueOne>
              <ValueTwo/>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>4096</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>32768</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>65536</ValueOne>
              <ValueTwo/>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>131072</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>262144</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>524288</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>') || to_clob('
              <Operator>Contains</Operator>
              <ValueOne>1048576</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>2097152</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>4194304</ValueOne>
              <ValueTwo/>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>8388608</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>16777216</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>33554432</ValueOne>
              <ValueTwo/>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>67108864</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Contains</Operator>
              <ValueOne>134217728</ValueOne>
              <ValueTwo/>
            </FilterElement>') || to_clob('
          </Values>
        </FilterCriteria>
      </Filters>
      <IsActive>true</IsActive>
    </GroupFilter>
    <GroupFilter>
      <Filters>
        <FilterCriteria>
          <Name>������</Name>
          <ValueTypeCode>RegionsMultiSelect</ValueTypeCode>
          <LookupTableName>v$ssrf</LookupTableName>
          <InternalName>Region</InternalName>
          <FirInternalName>region_code</FirInternalName>
          <IsRequired>true</IsRequired>
          <IsActive>true</IsActive>
          <Values>
            <FilterElement>') || to_clob('
              <Operator>Equals</Operator>
              <ValueOne>14</ValueOne>
              <ValueTwo>14 - ���������� ���� (������)</ValueTwo>
            </FilterElement>
          </Values>
        </FilterCriteria>
        <FilterCriteria>
          <Name>��� ��������</Name>
          <ValueTypeCode>ListMultpleChoice</ValueTypeCode>
          <LookupTableName>DICT_SUR</LookupTableName>
          <InternalName>SurContractor</InternalName>
          <FirInternalName>contractor_sur_code</FirInternalName>
          <IsRequired>false</IsRequired>
          <IsActive>true</IsActive>') || to_clob('
          <Values>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>1</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>2</ValueOne>
              <ValueTwo/>
            </FilterElement>') || to_clob('
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>3</ValueOne>
              <ValueTwo/>
            </FilterElement>
            <FilterElement>
              <Operator>Equals</Operator>
              <ValueOne>4</ValueOne>
              <ValueTwo/>
            </FilterElement>
          </Values>
        </FilterCriteria>') || to_clob('
      </Filters>
      <IsActive>true</IsActive>
    </GroupFilter>
  </GroupsFilters>
</SelectionFilter>'));
commit;

insert into NDS2_MRR_USER.CFG_AUTOSELECTION_FILTER (include_filter, exclude_filter, date_modified)
values(to_clob('<?xml version="1.0" encoding="utf-16"?>
<ArrayOfGroupFilter xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <GroupFilter>
    <Filters>
      <FilterCriteria>
        <Name>������ �������, ����������� ��</Name>
        <ValueTypeCode>RegionsMultiSelect</ValueTypeCode>
        <LookupTableName>v$ssrf</LookupTableName>
        <InternalName>region_code</InternalName>
        <IsRequired>false</IsRequired>
        <IsActive>true</IsActive>
        <Values>
          <FilterElement>') || to_clob('
            <Operator>Equals</Operator>
            <ValueOne>16</ValueOne>
            <ValueTwo>16 - ���������� ��������� (���������)</ValueTwo>
          </FilterElement>
        </Values>
      </FilterCriteria>
      <FilterCriteria>
        <Name>��������� �������, ����������� ��</Name>
        <ValueTypeCode>TaxAuthoritiesMultiSelect</ValueTypeCode>
        <LookupTableName>v$sono</LookupTableName>
        <InternalName>sono_code</InternalName>
        <IsRequired>false</IsRequired>
        <IsActive>true</IsActive>
        <Values>
          <FilterElement>') || to_clob('
            <Operator>Equals</Operator>
            <ValueOne>1683</ValueOne>
            <ValueTwo>1683 - ����������� ��������� ����������� ��������� ������ � 3 �� ���������� ���������</ValueTwo>
          </FilterElement>
          <FilterElement>
            <Operator>Equals</Operator>
            <ValueOne>1651</ValueOne>
            <ValueTwo>1651 - ����������� ��������� ����������� ��������� ������ � 11 �� ���������� ���������</ValueTwo>
          </FilterElement>
          <FilterElement>') || to_clob('
            <Operator>Equals</Operator>
            <ValueOne>1674</ValueOne>
            <ValueTwo>1674 - ����������� ��������� ����������� ��������� ������ � 9 �� ���������� ���������</ValueTwo>
          </FilterElement>
          <FilterElement>
            <Operator>Equals</Operator>
            <ValueOne>1689</ValueOne>
            <ValueTwo>1689 - ����������� ��������� ����������� ��������� ������ � 17 �� ���������� ���������</ValueTwo>
          </FilterElement>
          <FilterElement>') || to_clob('
            <Operator>Equals</Operator>
            <ValueOne>1681</ValueOne>
            <ValueTwo>1681 - ����������� ��������� ����������� ��������� ������ �� ���������� ������������������ �� ���������� ���������</ValueTwo>
          </FilterElement>
          <FilterElement>
            <Operator>Equals</Operator>
            <ValueOne>1686</ValueOne>
            <ValueTwo>1686 - ����������� ��������� ����������� ��������� ������ � 6 �� ���������� ���������</ValueTwo>
          </FilterElement>
          <FilterElement>') || to_clob('
            <Operator>Equals</Operator>
            <ValueOne>1656</ValueOne>
            <ValueTwo>1656 - ����������� ��������� ����������� ��������� ������ � 19 �� ���������� ���������</ValueTwo>
          </FilterElement>
          <FilterElement>
            <Operator>Equals</Operator>
            <ValueOne>1685</ValueOne>
            <ValueTwo>1685 - ����������� ��������� ����������� ��������� ������ � 5 �� ���������� ���������</ValueTwo>
          </FilterElement>
          <FilterElement>') || to_clob('
            <Operator>Equals</Operator>
            <ValueOne>1600</ValueOne>
            <ValueTwo>1600 - ���������� ����������� ��������� ������ �� ���������� ���������</ValueTwo>
          </FilterElement>
          <FilterElement>
            <Operator>Equals</Operator>
            <ValueOne>1677</ValueOne>
            <ValueTwo>1677 - ����������� ��������� ����������� ��������� ������ � 12 �� ���������� ���������</ValueTwo>
          </FilterElement>
          <FilterElement>') || to_clob('
            <Operator>Equals</Operator>
            <ValueOne>1675</ValueOne>
            <ValueTwo>1675 - ����������� ��������� ����������� ��������� ������ � 10 �� ���������� ���������</ValueTwo>
          </FilterElement>
          <FilterElement>
            <Operator>Equals</Operator>
            <ValueOne>1658</ValueOne>
            <ValueTwo>1658 - ��������� ����������� ��������� ������ �� ����������� ������ �. ������</ValueTwo>
          </FilterElement>
          <FilterElement>') || to_clob('
            <Operator>Equals</Operator>
            <ValueOne>1684</ValueOne>
            <ValueTwo>1684 - ����������� ��������� ����������� ��������� ������ � 4 �� ���������� ���������</ValueTwo>
          </FilterElement>
          <FilterElement>
            <Operator>Equals</Operator>
            <ValueOne>1650</ValueOne>
            <ValueTwo>1650 - ��������� ����������� ��������� ������ �� �. ���������� ����� ���������� ���������</ValueTwo>
          </FilterElement>
          <FilterElement>') || to_clob('
            <Operator>Equals</Operator>
            <ValueOne>1673</ValueOne>
            <ValueTwo>1673 - ����������� ��������� ����������� ��������� ������ � 8 �� ���������� ���������</ValueTwo>
          </FilterElement>
          <FilterElement>
            <Operator>Equals</Operator>
            <ValueOne>1655</ValueOne>
            <ValueTwo>1655 - ����������� ��������� ����������� ��������� ������ � 14 �� ���������� ���������</ValueTwo>
          </FilterElement>
          <FilterElement>') || to_clob('
            <Operator>Equals</Operator>
            <ValueOne>1690</ValueOne>
            <ValueTwo>1690 - ����������� ��������� ����������� ��������� ������ � 18 �� ���������� ���������</ValueTwo>
          </FilterElement>
          <FilterElement>
            <Operator>Equals</Operator>
            <ValueOne>1644</ValueOne>
            <ValueTwo>1644 - ����������� ��������� ����������� ��������� ������ � 16 �� ���������� ���������</ValueTwo>
          </FilterElement>
        </Values>
      </FilterCriteria>') || to_clob('
      <FilterCriteria>
        <Name>��� �������, ����������� ��</Name>
        <ValueTypeCode>EditWithComparisonStr</ValueTypeCode>
        <LookupTableName/>
        <InternalName>inn</InternalName>
        <IsRequired>false</IsRequired>
        <IsActive>true</IsActive>
        <Values>
          <FilterElement>
            <Operator>Equals</Operator>
            <ValueOne>5252031528,3664088086,5609066470,5245000448,8614002935,5410037050</ValueOne>
            <ValueTwo/>
          </FilterElement>') || to_clob('
        </Values>
      </FilterCriteria>
      <FilterCriteria>
        <Name>��� �������, ����������� ��</Name>
        <ValueTypeCode>ListMultpleChoice</ValueTypeCode>
        <LookupTableName>DICT_SUR</LookupTableName>
        <InternalName>sur_code</InternalName>
        <IsRequired>false</IsRequired>
        <IsActive>true</IsActive>
        <Values>
          <FilterElement>') || to_clob('
            <Operator>Equals</Operator>
            <ValueOne>1</ValueOne>
            <ValueTwo/>
          </FilterElement>
          <FilterElement>
            <Operator>Equals</Operator>
            <ValueOne>2</ValueOne>
            <ValueTwo/>
          </FilterElement>
          <FilterElement>') || to_clob('
            <Operator>Equals</Operator>
            <ValueOne>3</ValueOne>
            <ValueTwo/>
          </FilterElement>
          <FilterElement>
            <Operator>Equals</Operator>
            <ValueOne>4</ValueOne>
            <ValueTwo/>
          </FilterElement>') || to_clob('
        </Values>
      </FilterCriteria>
      <FilterCriteria>
        <Name>��� �����������</Name>
        <ValueTypeCode>ListMultpleChoice</ValueTypeCode>
        <LookupTableName>DICT_DISCREPANCY_TYPE</LookupTableName>
        <InternalName>type_code</InternalName>
        <IsRequired>false</IsRequired>
        <IsActive>true</IsActive>
        <Values>
          <FilterElement>') || to_clob('
            <Operator>Equals</Operator>
            <ValueOne>1</ValueOne>
            <ValueTwo/>
          </FilterElement>
          <FilterElement>
            <Operator>Equals</Operator>
            <ValueOne>2</ValueOne>
            <ValueTwo/>
          </FilterElement>
          <FilterElement>') || to_clob('
            <Operator>Equals</Operator>
            <ValueOne>3</ValueOne>
            <ValueTwo/>
          </FilterElement>
          <FilterElement>
            <Operator>Equals</Operator>
            <ValueOne>4</ValueOne>
            <ValueTwo/>
          </FilterElement>
        </Values>
      </FilterCriteria>') || to_clob('
      <FilterCriteria>
        <Name>��������� ������</Name>
        <ValueTypeCode>ListMultpleChoice</ValueTypeCode>
        <LookupTableName>DICT_NALOG_PERIODS</LookupTableName>
        <InternalName>period</InternalName>
        <IsRequired>false</IsRequired>
        <IsActive>true</IsActive>
        <Values>
          <FilterElement>
            <Operator>Equals</Operator>
            <ValueOne>21</ValueOne>
            <ValueTwo/>
          </FilterElement>') || to_clob('
          <FilterElement>
            <Operator>Equals</Operator>
            <ValueOne>22</ValueOne>
            <ValueTwo/>
          </FilterElement>
          <FilterElement>
            <Operator>Equals</Operator>
            <ValueOne>23</ValueOne>
            <ValueTwo/>
          </FilterElement>') || to_clob('
          <FilterElement>
            <Operator>Equals</Operator>
            <ValueOne>24</ValueOne>
            <ValueTwo/>
          </FilterElement>
        </Values>
      </FilterCriteria>
      <FilterCriteria>
        <Name>������� ��</Name>
        <ValueTypeCode>ListMultpleChoice</ValueTypeCode>
        <LookupTableName>DICT_DECLARATION_SIGN</LookupTableName>
        <InternalName>decl_sign</InternalName>
        <IsRequired>false</IsRequired>
        <IsActive>true</IsActive>
        <Values>
          <FilterElement>') || to_clob('
            <Operator>Equals</Operator>
            <ValueOne>1</ValueOne>
            <ValueTwo/>
          </FilterElement>
          <FilterElement>
            <Operator>Equals</Operator>
            <ValueOne>2</ValueOne>
            <ValueTwo/>
          </FilterElement>
        </Values>
      </FilterCriteria>') || to_clob('
      <FilterCriteria>
        <Name>��� �����������</Name>
        <ValueTypeCode>EditIntervalInt</ValueTypeCode>
        <LookupTableName/>
        <InternalName>decl_total_pvp</InternalName>
        <IsRequired>false</IsRequired>
        <IsActive>true</IsActive>
        <Values>
          <FilterElement>
            <Operator>Between</Operator>
            <ValueOne>0</ValueOne>
            <ValueTwo>999999999</ValueTwo>
          </FilterElement>
        </Values>
      </FilterCriteria>') || to_clob('
      <FilterCriteria>
        <Name>������ �������, ���������� ��</Name>
        <ValueTypeCode>RegionsMultiSelect</ValueTypeCode>
        <LookupTableName>v$ssrf</LookupTableName>
        <InternalName/>
        <IsRequired>false</IsRequired>
        <IsActive>true</IsActive>
        <Values>
          <FilterElement>
            <Operator>Equals</Operator>
            <ValueOne>14</ValueOne>
            <ValueTwo>14 - ���������� ���� (������)</ValueTwo>
          </FilterElement>') || to_clob('
        </Values>
      </FilterCriteria>
      <FilterCriteria>
        <Name>��������� �������, ���������� ��</Name>
        <ValueTypeCode>TaxAuthoritiesMultiSelect</ValueTypeCode>
        <LookupTableName>v$sono</LookupTableName>
        <InternalName/>
        <IsRequired>false</IsRequired>
        <IsActive>true</IsActive>
        <Values>
          <FilterElement>
            <Operator>Equals</Operator>
            <ValueOne>1448</ValueOne>
            <ValueTwo>1448 - ����������� ��������� ����������� ��������� ������ � 8 �� ���������� ���� (������)</ValueTwo>
          </FilterElement>') || to_clob('
          <FilterElement>
            <Operator>Equals</Operator>
            <ValueOne>1400</ValueOne>
            <ValueTwo>1400 - ���������� ����������� ��������� ������ �� ���������� ���� (������)</ValueTwo>
          </FilterElement>
          <FilterElement>
            <Operator>Equals</Operator>
            <ValueOne>1447</ValueOne>
            <ValueTwo>1447 - ����������� ��������� ����������� ��������� ������ � 5 �� ���������� ���� (������)</ValueTwo>
          </FilterElement>
          <FilterElement>') || to_clob('
            <Operator>Equals</Operator>
            <ValueOne>1426</ValueOne>
            <ValueTwo>1426 - ����������� ��������� ����������� ��������� ������ � 9 �� ���������� ���� (������)</ValueTwo>
          </FilterElement>
          <FilterElement>
            <Operator>Equals</Operator>
            <ValueOne>1434</ValueOne>
            <ValueTwo>1434 - ��������� ����������� ��������� ������ �� �������������� ������ ���������� ���� (������)</ValueTwo>
          </FilterElement>
          <FilterElement>') || to_clob('
            <Operator>Equals</Operator>
            <ValueOne>1402</ValueOne>
            <ValueTwo>1402 - ��������� ����������� ��������� ������ �� ���������� ������ ���������� ���� (������)</ValueTwo>
          </FilterElement>
          <FilterElement>
            <Operator>Equals</Operator>
            <ValueOne>1449</ValueOne>
            <ValueTwo>1449 - ����������� ��������� ����������� ��������� ������ �� ���������� ������������������ �� ���������� ���� (������)</ValueTwo>
          </FilterElement>
          <FilterElement>
            <Operator>Equals</Operator>
            <ValueOne>1450</ValueOne>
            <ValueTwo>1450 - ����������� ��������� ����������� ��������� ������ � 2 �� ���������� ���� (������)</ValueTwo>
          </FilterElement>') || to_clob('
          <FilterElement>
            <Operator>Equals</Operator>
            <ValueOne>1436</ValueOne>
            <ValueTwo>1436 - ����������� ��������� ����������� ��������� ������ � 1 �� ���������� ���� (������)</ValueTwo>
          </FilterElement>
          <FilterElement>
            <Operator>Equals</Operator>
            <ValueOne>1445</ValueOne>
            <ValueTwo>1445 - ����������� ��������� ����������� ��������� ������ � 3 �� ���������� ���� (������)</ValueTwo>
          </FilterElement>
          <FilterElement>
            <Operator>Equals</Operator>
            <ValueOne>1446</ValueOne>
            <ValueTwo>1446 - ����������� ��������� ����������� ��������� ������ � 4 �� ���������� ���� (������)</ValueTwo>
          </FilterElement>') || to_clob('
        </Values>
      </FilterCriteria>
      <FilterCriteria>
        <Name>��� �������, ���������� ��</Name>
        <ValueTypeCode>EditWithComparisonStr</ValueTypeCode>
        <LookupTableName/>
        <InternalName/>
        <IsRequired>false</IsRequired>
        <IsActive>true</IsActive>
        <Values>
          <FilterElement>') || to_clob('
            <Operator>Equals</Operator>
            <ValueOne>5252031528,3664088086,5609066470,5245000448,8614002935,5410037050</ValueOne>
            <ValueTwo/>
          </FilterElement>
        </Values>
      </FilterCriteria>
      <FilterCriteria>
        <Name>��� �������, ���������� ��</Name>
        <ValueTypeCode>ListMultpleChoice</ValueTypeCode>
        <LookupTableName>DICT_SUR</LookupTableName>
        <InternalName>contractor_sur_code</InternalName>
        <IsRequired>false</IsRequired>
        <IsActive>true</IsActive>') || to_clob('
        <Values>
          <FilterElement>
            <Operator>Equals</Operator>
            <ValueOne>1</ValueOne>
            <ValueTwo/>
          </FilterElement>
          <FilterElement>
            <Operator>Equals</Operator>
            <ValueOne>2</ValueOne>
            <ValueTwo/>
          </FilterElement>') || to_clob('
          <FilterElement>
            <Operator>Equals</Operator>
            <ValueOne>3</ValueOne>
            <ValueTwo/>
          </FilterElement>
          <FilterElement>
            <Operator>Equals</Operator>
            <ValueOne>4</ValueOne>
            <ValueTwo/>
          </FilterElement>') || to_clob('
        </Values>
      </FilterCriteria>
    </Filters>
    <IsActive>true</IsActive>
  </GroupFilter>
</ArrayOfGroupFilter>'), 
to_clob('<?xml version="1.0" encoding="utf-16"?>
<ArrayOfGroupFilter xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <GroupFilter>
    <Filters>
      <FilterCriteria>
        <Name>������ �������, ����������� ��</Name>
        <ValueTypeCode>RegionsMultiSelect</ValueTypeCode>
        <LookupTableName>v$ssrf</LookupTableName>
        <InternalName>region_code</InternalName>
        <IsRequired>false</IsRequired>
        <IsActive>true</IsActive>
        <Values>
          <FilterElement>') || to_clob('
            <Operator>Equals</Operator>
            <ValueOne>05</ValueOne>
            <ValueTwo>05 - ���������� ��������</ValueTwo>
          </FilterElement>
        </Values>
      </FilterCriteria>
      <FilterCriteria>
        <Name>��� �������, ����������� ��</Name>
        <ValueTypeCode>EditWithComparisonStr</ValueTypeCode>
        <LookupTableName/>
        <InternalName>inn</InternalName>
        <IsRequired>false</IsRequired>
        <IsActive>true</IsActive>
        <Values>
          <FilterElement>') || to_clob('
            <Operator>Equals</Operator>
            <ValueOne>1111111111</ValueOne>
            <ValueTwo/>
          </FilterElement>
        </Values>
      </FilterCriteria>
      <FilterCriteria>
        <Name>��� �������, ���������� ��</Name>
        <ValueTypeCode>EditWithComparisonStr</ValueTypeCode>
        <LookupTableName/>
        <InternalName/>
        <IsRequired>false</IsRequired>
        <IsActive>true</IsActive>
        <Values>
          <FilterElement>') || to_clob('
            <Operator>Equals</Operator>
            <ValueOne>2222222222</ValueOne>
            <ValueTwo/>
          </FilterElement>
        </Values>
      </FilterCriteria>
    </Filters>
    <IsActive>true</IsActive>
  </GroupFilter>
</ArrayOfGroupFilter>'),
sysdate);
commit;

insert into NDS2_MRR_USER.AUTOSELECTION_FILTER(ID, INCLUDE_FILTER, EXCLUDE_FILTER, DATE_CREATED)
select 999101, include_filter, exclude_filter, sysdate from NDS2_MRR_USER.CFG_AUTOSELECTION_FILTER;
commit;

insert into NDS2_MRR_USER.FAVORITE_FILTERS(id, name, analyst, filter)
select selection_id, 'favorite ' || s.name, 'FNSTEST\Analitik', filter 
from NDS2_MRR_USER.selection_filter sf
left join NDS2_MRR_USER.selection s on s.id = sf.selection_id
where s.id in (999001, 999002, 999003, 999004, 999005);
commit;
