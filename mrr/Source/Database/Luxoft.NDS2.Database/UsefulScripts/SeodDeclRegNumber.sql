BEGIN
INSERT INTO NDS2_SEOD.seod_declaration (
id,
type,
nds2_id,
sono_code,
tax_period,
fiscal_year,
correction_number,
decl_reg_num,
decl_fid,
tax_payer_type,
inn,
kpp,
insert_date,
eod_date,
date_receipt,
id_file)
select
  a.id,
  a.type,
  a.nds2_id,
  a.sono_code,
  a.tax_period,
  a.fiscal_year,
  a.correction_number,
  a.decl_reg_num,
  a.decl_fid,
  a.tax_payer_type,
  a.inn,
  a.kpp,
  a.insert_date,
  a.eod_date,
  a.date_receipt,
  a.id_file
from
(
  SELECT
  (d.ID + 1000000000) as id,
  type as type,
  (d.PERIOD||d.OTCHETGOD||d.INNNP) as nds2_id,
  d.KODNO as sono_code,
  d.PERIOD as tax_period,
  d.OTCHETGOD as fiscal_year,
  d.nomkorr as correction_number,
  (d.ID + 1000000000) as decl_reg_num,
  d.ID as decl_fid,
  1 as tax_payer_type,
  d.INNNP as inn,
  d.KPPNP as kpp,
  SYSDATE as insert_date,
  SYSDATE as eod_date,
  SYSDATE as date_receipt,
  d.IDFAJL as id_file
  FROM NDS2_MRR_USER.ASK_DECLANDJRNL d
  join NDS2_MRR_USER.declaration_history dh on dh.zip = d.zip and dh.reg_number is null
) a
left join NDS2_SEOD.seod_declaration seod on seod.sono_code = a.sono_code and seod.decl_reg_num = a.decl_reg_num
where seod.id is null;

insert into NDS2_SEOD.SEOD_REG_NUMBER 
(ID, DECL_REG_NUM, INN, TAX_PERIOD, FISCAL_YEAR, SONO_CODE, CORRECTION_NUMBER, TYPE, ID_FILE, EOD_DATE, SUBMISSION_DATE, INSERT_DATE)
select
  NDS2_SEOD.SEQ_SEOD_REG_NUMBER.NEXTVAL,
  a.decl_reg_num,
  a.inn,
  a.tax_period,
  a.fiscal_year,
  a.sono_code,
  a.correction_number,
  a.type,
  a.id_file,
  a.eod_date,
  a.insert_date,
  a.insert_date
from
(
  SELECT 
 (d.ID + 1000000000) as decl_reg_num,
  d.INNNP as inn,
  d.PERIOD as tax_period,
  d.OTCHETGOD as fiscal_year,
  d.KODNO as sono_code,
  d.nomkorr as correction_number,
  type as type,
  d.IDFAJL as id_file,
  SYSDATE as eod_date,
  SYSDATE as insert_date  
  FROM NDS2_MRR_USER.ASK_DECLANDJRNL d
  join NDS2_MRR_USER.declaration_history dh on dh.zip = d.zip and dh.reg_number is null
) a
left join NDS2_SEOD.SEOD_REG_NUMBER seod on seod.sono_code = a.sono_code and seod.decl_reg_num = a.decl_reg_num
where seod.id is null;

END;
