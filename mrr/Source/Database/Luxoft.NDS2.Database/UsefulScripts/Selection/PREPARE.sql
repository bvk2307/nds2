-- ������� �� � ������� SOV_INVOICE_ALL
truncate table SOV_INVOICE_ALL;
insert into SOV_INVOICE_ALL
(
declaration_version_id, chapter, ordinal_number,
okv_code, create_date, receive_date,
invoice_num, invoice_date, change_num,
change_date, correction_num, correction_date,
change_correction_num, change_correction_date, seller_invoice_num,
seller_invoice_date, broker_inn, broker_kpp,
deal_kind_code, customs_declaration_num, price_buy_amount,
price_buy_nds_amount, price_sell, price_sell_in_curr,
price_sell_18, price_sell_10, price_sell_0,
price_nds_18, price_nds_10, price_tax_free,
price_total, price_nds_total, diff_correct_decrease,
diff_correct_increase, diff_correct_nds_decrease,
diff_correct_nds_increase, price_nds_buyer, row_key,
actual_row_key, compare_row_key, compare_algo_id,
format_errors, logical_errors, seller_agency_info_inn,
seller_agency_info_kpp, seller_agency_info_name, seller_agency_info_num,
seller_agency_info_date, is_dop_list, is_import,
clarification_key, contragent_key
)
select
a.declaration_version_id, a.chapter, a.ordinal_number,
a.okv_code, a.create_date, a.receive_date,
a.invoice_num, a.invoice_date, a.change_num,
a.change_date, a.correction_num, a.correction_date,
a.change_correction_num, a.change_correction_date, a.seller_invoice_num,
a.seller_invoice_date, a.broker_inn, a.broker_kpp,
a.deal_kind_code, a.customs_declaration_num, a.price_buy_amount,
a.price_buy_nds_amount, a.price_sell, a.price_sell_in_curr,
a.price_sell_18, a.price_sell_10, a.price_sell_0,
a.price_nds_18, a.price_nds_10, a.price_tax_free,
a.price_total, a.price_nds_total, a.diff_correct_decrease,
a.diff_correct_increase, a.diff_correct_nds_decrease,
a.diff_correct_nds_increase, a.price_nds_buyer, a.row_key,
a.actual_row_key, a.compare_row_key, a.compare_algo_id,
a.format_errors, a.logical_errors, a.seller_agency_info_inn,
a.seller_agency_info_kpp, a.seller_agency_info_name, a.seller_agency_info_num,
a.seller_agency_info_date, null, null,
null, null
from fir.invoice_raw a;
commit;
-- ������� ����������� � ������� SOV_DISCREPANCY_ALL, ���� ������ ��� ����� ����������, �� ��������� ������� �� �������, ���, ��� �����
truncate table SOV_DISCREPANCY_ALL;
insert into SOV_DISCREPANCY_ALL
(
status, tax_period,
tax_year, type, create_date,
rule_group, deal_amnt, amnt,
buyer_inn, seller_inn, 
invoice_chapter, invoice_rk, invoice_contractor_rk,
invoice_contractor_chapter, region, ifns,
inn, np_name, nd_pris,
mismatch_nd_sum, mismatch_min_sum, mismatch_max_sum,
mismatch_avg_sum, sur_code, nd_submission_date,
mismatch_count, amount_pvp, rule_num,
buyer_kpp, decl_id,
decl_contractor_id
)
select
a.status, 21,
2014, a.type, a.create_date,
a.rule_group, a.deal_amnt, a.amnt,
d.INN, null, 
a.invoice_chapter, a.invoice_rk, a.invoice_contractor_rk,
a.invoice_contractor_chapter, null, null,
null, null, null,
null, null, null,
null, a.sur_code, null,
null, a.amount_pvp, '01',
null, a.decl_id,
a.decl_contractor_id
from fir.discrepancy_raw a
left join sov_invoice_all inv on inv.row_key = a.invoice_rk
left join v$declaration d on d.DECLARATION_VERSION_ID = inv.declaration_version_id;
commit;
-- ������ ��������� post_import (������� ������ �������� ���, ����� ���������� ������ SOV_INVOICE_ALL � SOV_DISCREPANCY_ALL)
begin
pac$discrepancy.post_import;
end;
/
