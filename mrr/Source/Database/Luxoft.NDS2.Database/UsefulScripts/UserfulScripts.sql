﻿/************Locked objects********************/
SELECT vh.sid locking_sid,
 vs.status status,
 vs.program program_holding,
 vw.sid waiter_sid,
 vsw.program program_waiting
FROM v$lock vh,
 v$lock vw,
 v$session vs,
 v$session vsw
WHERE     (vh.id1, vh.id2) IN (SELECT id1, id2
 FROM v$lock
 WHERE request = 0
 INTERSECT
 SELECT id1, id2
 FROM v$lock
 WHERE lmode = 0)
 AND vh.id1 = vw.id1
 AND vh.id2 = vw.id2
 AND vh.request = 0
 AND vw.lmode = 0
 AND vh.sid = vs.sid
 AND vw.sid = vsw.sid;


 select session_id "sid",SERIAL#  "Serial",
substr(object_name,1,20) "Object",
  substr(os_user_name,1,10) "Terminal",
  substr(oracle_username,1,10) "Locker",
  nvl(lockwait,'active') "Wait",
  decode(locked_mode,
    2, 'row share',
    3, 'row exclusive',
    4, 'share',
    5, 'share row exclusive',
    6, 'exclusive',  'unknown') "Lockmode",
  OBJECT_TYPE "Type"
FROM
  SYS.V_$LOCKED_OBJECT A,
  SYS.ALL_OBJECTS B,
  SYS.V_$SESSION c
WHERE
  A.OBJECT_ID = B.OBJECT_ID AND
  C.SID = A.SESSION_ID
ORDER BY 1 ASC, 5 Desc


#######################################
#How to find the current running jobs.#
#######################################
1. If you used DBMS_SCHEDULER then try this 

select * from USER_SCHEDULER_JOBS
select * from user_scheduler_job_run_details

2.for regular jobs try this 
select * from user_jobs

/*
--drop CAM users
drop user I$CAM cascade;
drop user CAM$CONV cascade;
drop user CAM$NSI cascade;

drop user NDS2_MRR_USER cascade;
drop user NDS2_MC cascade;
drop user FIR cascade;
*/


/*
--run on every friday the 13th
truncate table ACTION_HISTORY;
truncate table BOOK_DATA_REQUEST;
truncate table DOC;
truncate table DOC_INVOICE;
truncate table DOC_STATUS_HISTORY;
truncate table seod_declaration;
--truncate table seod_doc_taxpayer_reply;
truncate table seod_knp;
truncate table seod_knp_act;
truncate table seod_knp_decision;
truncate table seod_data_queue;
truncate table DECLARATION_OWNER;
truncate table DISCREPANCY_COMMENT;
truncate table DISCREPANCY_REQUEST;
truncate table FAVORITE_FILTERS;
truncate table HIST_SELECTION_STATUS;
truncate table INVOICE_REQUEST;
truncate table INVOICE_REQUEST_DATA;
truncate table OBJECT_LOCK;
truncate table SELECTION;
truncate table SELECTION_DECLARATION;
truncate table SELECTION_DISCREPANCY;
truncate table SOV_DISCREPANCY;
truncate table SOV_DISCREPANCY_TMP;
truncate table SOV_INVOICE;
truncate table SOV_SELECTION_REQUEST;
truncate table SYSTEM_LOG;
truncate table USER_TO_N_SSRF;
*/

#######################################
#Заполнить декларацию.#
#######################################
/*
UPDATE "NDS2_MC"."ASKДекл" 
 SET   	
	"ИдФайл" = 'SFDREFILE', 
  "ВерсПрог" = 'New2',
  "ВерсФорм" = 'M3',
  "ПризнНал8-12"  = '1',
  "ПризнНал8"  = '2',
	"ПризнНал81"  = '3',
	"ПризнНал9"  = '4', 
	"ПризнНал91"  = '5',
	"ПризнНал10"  = '6', 
	"ПризнНал11"  = '7',
	"ПризнНал12"  = '7', 
	"КНД"  = '1151001',
	"ДатаДок" = '10-APR-14', 
	"ПоМесту"  = '116',
	"ОКВЭД"  = '5874961',
	"Тлф"  = '58796124',
	"ФормРеорг"  = '2',
	"ИННРеорг"  = '5124785695',
	"КППРеорг"  = '654785298', 
	"ФамилияНП"  = 'Ivanov', 
	"ИмяНП"  = 'Ivan',
	"ОтчествоНП"  = 'Petrovich',
	"ПрПодп"  = '1',
	"ФамилияПодп"  = 'Petrov',
	"ИмяПодп"  = 'Petr',
	"ОтчествоПодп"  = 'Ivanovich',
	"НаимДок"  = 'Contract 8',
	"НаимОргПред"  = 'Story LTD',
	"ОКТМО"  = '45862478',
	"КБК"  = '25475487772', 
	"СумПУ173.5"  = 254885,
	"СумПУ173.1"  = 587632,
	"НомДогИТ"  = '587954',
	"ДатаДогИТ" = '11-APR-14', 
	"ДатаНачДогИТ" = '12-APR-14', 
	"ДатаКонДогИТ" = '24-APR-15', 
	"НалПУ164"  = 54654677,
	"НалВосстОбщ"  = 54687,
	"РлТв18НалБаз" = 546875,
	"РеалТов18СумНал" = 546879,
	"РлТв10НалБаз" = 546871,
	"РеалТов10СумНал" = 554687,
	"РлТв118НалБаз" = 543, 
	"РлТв118СумНал" = 5344,
	"РлТв110НалБаз" = 587,
	"РлТв110СумНал" = 4587,
	"РлПрдИКНалБаз" = 548687,
	"РлПрдИКСумНал" = 54877,
	"ВыпСМРСобНалБаз" = 41, 
	"ВыпСМРСобСумНал" = 54687,
	"ОпПрдПстНлБаз" = 5469987,
	"ОплПрдПстСумНал" = 54681,
	"СумНалВс" = 54680,
	"СумНал170.3.5" = 54683,
	"СумНал170.3.3" = 546879, 
	"КорРлТв18НалБаз" = 546287, 
	"КорРлТв18СумНал" = 54687,
	"КорРлТв10НалБаз" = 5447,
	"КорРлТв10СумНал" = 54677, 
	"КорРлТв118НлБз" = 54447,
	"КорРлТв118СмНл" = 54, 
	"КорРлТв110НлБз" = 5484687,
	"КорРлТв110СмНл" = 54687,
	"КорРлПрдИКНлБз" = 587,
	"КорРлПрдИКСмНл" = 54687,
	"НалПредНППриоб" = 5468,
	"НалПредНППок" = 54,
	"НалИсчСМР" = 54687, 
	"НалУплТамож" = 5468,
	"НалУплНОТовТС" = 5, 
	"НалИсчПрод" = 5468,
	"НалУплПокНА" = 54687,
	"НалВычОбщ" = 547,
	"СумИсчислИтог" = 587,
	"СумВозмПдтв" = 5468,
	"СумВозмНеПдтв" = 54687,
	"СумНал164Ит" = 54,
	"НалВычНеПодИт" = 687,
	"НалИсчислИт" = 57,
	"СмОп1010449КдОп"  = '1010449', 
	"СмОп1010449НлБз" = 0,
	"СмОп1010449КрИсч" = 1010449,
	"СмОп1010449НлВст" = 0,
	"ОплПостСв6Мес" = 54687,
	"НаимКнПок"  = 'sddds',
	"НаимКнПокДЛ"  = 's25',
	"НаимКнПрод"  = '1ew', 
	"НаимКнПродДЛ"  = 'vvb',
	"НаимЖУчВыстСчФ"  = 'aqs',
	"НаимЖУчПолучСчФ"  = 'trr',
	"НмВстСчФ173_5"  = 'ttt',
	"Публ"  = '1',
	"ИдЗагрузка" = 547, 
	"КодОпер47"  = '1010447',
	"НалБаза47" = 587,
	"НалВосст47" = 687,
	"КодОпер48"  = '1010448',
	"КорНалБазаУв48" = 7,
	"КорНалБазаУм48" = 5,
	"КодОпер50"  = '0',
	"КорНалБазаУв50" = 57,
	"КорИсчУв50" = 546,
	"КорНалБазаУм50" = 87,
	"КорИсчУм50" = 546871,
	"КлючДекл"  = '1144'  
  WHERE ZIP = (select  max(nd."ZIP") zip
from "NDS2_MC"."ASKДекл" nd
where nd."ИНННП" = 5245000448
group by nd."ИНННП");

delete from NDS2_MC."ASKСумОпер4" where "Ид" = '1';
INSERT INTO NDS2_MC."ASKСумОпер4" ("Ид", "ИдДекл", "КодОпер", "НалБаза", "НалВычПод", "НалНеПод", "НалВосст") VALUES ('1', (select max(nd."Ид") id
from "NDS2_MC"."ASKДекл" nd
where nd."ИНННП" = 5245000448
group by nd."ИНННП"), '1', '2', '3', '4', '5');
delete from NDS2_MC."ASKСумОпер6" where "Ид" = '1';
INSERT INTO NDS2_MC."ASKСумОпер6" ("Ид", "ИдДекл", "КодОпер", "НалБаза", "СумНал164", "НалВычНеПод") VALUES ('1', (select max(nd."Ид") id
from "NDS2_MC"."ASKДекл" nd
where nd."ИНННП" = 5245000448
group by nd."ИНННП"), '1', '2', '3', '4');
delete from NDS2_MC."ASKСумОпер7" where "Ид" = '1';
INSERT INTO NDS2_MC."ASKСумОпер7" ("Ид", "ИдДекл", "КодОпер", "СтРеалТов", "СтПриобТов", "НалНеВыч") VALUES ('1', (select max(nd."Ид") id
from "NDS2_MC"."ASKДекл" nd
where nd."ИНННП" = 5245000448
group by nd."ИНННП"), '1', '2', '3', '4');
delete from NDS2_MC."ASKСумПер" where "Ид" = '1';
INSERT INTO NDS2_MC."ASKСумПер" ("Ид", "ИдДекл", "ОтчетГод", "Период") VALUES ('1', (select max(nd."Ид") id
from "NDS2_MC"."ASKДекл" nd
where nd."ИНННП" = 5245000448
group by nd."ИНННП"), '2014', '1');
delete from NDS2_MC."ASKСумОпер5" where "Ид" = '1';
INSERT INTO NDS2_MC."ASKСумОпер5" ("Ид", "ИдСумПер", "КодОпер", "НалБазаПод", "НалВычПод", "НалБазаНеПод", "НалВычНеПод") VALUES ('1', '1', '1', '2', '3', '4', '5');
delete from NDS2_MC."ASKСумУплНА" where "Ид" = '1';
INSERT INTO NDS2_MC."ASKСумУплНА" ("Ид", "ИдДекл", "КППИно", "КБК", "ОКТМО", "СумИсчисл", "КодОпер", "СумИсчислОтгр", "СумИсчислОпл", "СумИсчислНА", "НаимПрод", "ИННПрод", "Фамилия", "Имя", "Отчество") VALUES ('1', (select max(nd."Ид") id
from "NDS2_MC"."ASKДекл" nd
where nd."ИНННП" = 5245000448
group by nd."ИНННП"), '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13');
delete from NDS2_MC."ASKСумВосУпл" where "Ид" = '1';
INSERT INTO NDS2_MC."ASKСумВосУпл" ("Ид", "ИдДекл", "НаимНедв", "КодОпНедв", "ДатаВводОН", "ДатаНачАмОтч", "СтВводОН", "НалВычОН", "Индекс", "КодРегион", "Район", "Город", "НаселПункт", "Улица", "Дом", "Корпус", "Кварт") VALUES ('1', 
(select max(nd."Ид") id
from "NDS2_MC"."ASKДекл" nd
where nd."ИНННП" = 5245000448
group by nd."ИНННП"), '1', '2', TO_DATE('3.01.01', 'DD.MM.RR'), TO_DATE('4.01.01', 'DD.MM.RR'), '5', '6', '7', '8', '9', '10', '11', '12', '13', '15', '15');
delete from NDS2_MC."ASKСведНалГод" where "Ид" = '1';
INSERT INTO NDS2_MC."ASKСведНалГод" ("Ид", "ИдСумВосУпл", "ГодОтч", "ДатаИсп170", "ДоляНеОбл", "НалГод") VALUES ('1', '1', '1', TO_DATE('2.01.01', 'DD.MM.YY'), '3', '4');
delete from NDS2_MC."ASKСведНалГодИ" where "Ид" = '1';
INSERT INTO NDS2_MC."ASKСведНалГодИ" ("Ид", "ИдДекл", "КППИнУч", "СумНалИсч", "СумНалВыч") VALUES ('1', (select max(nd."Ид") id
from "NDS2_MC"."ASKДекл" nd
where nd."ИНННП" = 5245000448
group by nd."ИНННП"), '1', '2', '3');
commit;
*/