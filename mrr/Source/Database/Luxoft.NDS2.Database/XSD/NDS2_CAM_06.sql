﻿declare
  v_doc clob;
begin
  v_doc := '<?xml version="1.0" encoding="windows-1251"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="Файл">
	  <xs:complexType>
        <xs:sequence minOccurs="1">
			<xs:element name="Документ" type="ДокТип"/>
		</xs:sequence>
		<xs:attribute name="ИдФайл" type="ИдФайлТип" use="required"/>
	  </xs:complexType>
  </xs:element>
  
	<xs:simpleType name="ИННФЛТип">
		<xs:annotation>
			<xs:documentation>Идентификационный номер налогоплательщика - физического лица</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:string">
			<xs:length value="12"/>
			<xs:pattern value="([0-9]{1}[1-9]{1}|[1-9]{1}[0-9]{1})[0-9]{10}"/>
		</xs:restriction>
	</xs:simpleType>  
  
	<xs:simpleType name="ИННЮЛТип">
		<xs:annotation>
			<xs:documentation>Идентификационный номер налогоплательщика - организации</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:string">
			<xs:length value="10"/>
			<xs:pattern value="([0-9]{1}[1-9]{1}|[1-9]{1}[0-9]{1})[0-9]{8}"/>
		</xs:restriction>
	</xs:simpleType>
	
  <xs:simpleType name="КППТип">
		<xs:annotation>
			<xs:documentation>Код причины постановки на учет (КПП) - 5 и 6 знаки от 0-9 и A-Z</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:string">
			<xs:length value="9"/>
			<xs:pattern value="([0-9]{1}[1-9]{1}|[1-9]{1}[0-9]{1})([0-9]{2})([0-9A-Z]{2})([0-9]{3})"/>
		</xs:restriction>
	</xs:simpleType>

  	<xs:simpleType name="ИдФайлТип">
		<xs:annotation>
			<xs:documentation>Имя файла основного раздела декларации</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:string">
			<xs:minLength value="1"/>
			<xs:maxLength value="100"/>
		</xs:restriction>
	</xs:simpleType>
	
	<xs:complexType name="СвНПТип">
		<xs:annotation>
			<xs:documentation>Сведения о налогоплательщике</xs:documentation>
		</xs:annotation>
		<xs:choice>
			<xs:element name="НПЮЛ">
				<xs:complexType>
				  <xs:attribute name="ИННЮЛ" type="ИННЮЛТип"/>
				  <xs:attribute name="КПП" type="КППТип"/>
				</xs:complexType>
			</xs:element>
			<xs:element name="НПФЛ">
				<xs:complexType>
					<xs:attribute name="ИННФЛ" type="ИННФЛТип"/>
				</xs:complexType>
			</xs:element>
		</xs:choice>       		
	</xs:complexType>
	
	<xs:complexType name="ДокТип">
		<xs:annotation>
			<xs:documentation>Запрос на аннулирование</xs:documentation>
		</xs:annotation>
		<xs:sequence minOccurs="1" maxOccurs="1">
			<xs:element name="СвНП" type="СвНПТип"/>
		</xs:sequence>
	    <xs:attribute name="КодНО" use="required">
			<xs:simpleType>
				<xs:restriction base="xs:string">
					<xs:length value="4"/>
				</xs:restriction>
			</xs:simpleType>
		</xs:attribute>
		<xs:attribute name="Период" use="required">
			<xs:simpleType>
				<xs:restriction base="xs:string">
					<xs:length value="2"/>
				</xs:restriction>
			</xs:simpleType>
		</xs:attribute>
		<xs:attribute name="ОтчетГод" use="required">
			<xs:simpleType>
				<xs:restriction base="xs:string">
					<xs:length value="4"/>
					<xs:pattern value="[0-9]{4}"/>
				</xs:restriction>
			</xs:simpleType>
		</xs:attribute>
		<xs:attribute name="НомКорр" use="required">
			<xs:simpleType>
				<xs:restriction base="xs:int">
				<xs:minInclusive value="0"/>
				<xs:maxInclusive value="999"/>
				</xs:restriction>
			</xs:simpleType>
		</xs:attribute>
		<xs:attribute name="РегНомДек" use="required">
			<xs:annotation>
				<xs:documentation>Регистрационный номер декларации</xs:documentation>
			</xs:annotation>
			<xs:simpleType>
				<xs:restriction base="xs:int">
					<xs:totalDigits value="13"/>
				</xs:restriction>
			</xs:simpleType>
		</xs:attribute>
		<xs:attribute name="КодАннул" use="required">
			<xs:simpleType>
				<xs:restriction base="xs:int">
					<xs:minInclusive value="0"/>
					<xs:maxInclusive value="1"/>
				</xs:restriction>
			</xs:simpleType>
		</xs:attribute>
	</xs:complexType>

</xs:schema>';

  DBMS_XMLSCHEMA.REGISTERSCHEMA(
      SCHEMAURL => 'TAX3EXCH_NDS2_CAM_06_01.xsd',
      SCHEMADOC => v_doc,
      genTables => false, 
      local => true,
      owner => 'NDS2_MRR_USER'
  );
end;
/
