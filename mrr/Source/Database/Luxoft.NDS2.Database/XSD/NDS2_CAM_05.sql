﻿declare
  v_doc clob;
begin
  v_doc := '<?xml version="1.0" encoding="windows-1251"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:element name="ЗАПНОВЫПИС">
  <xs:complexType>
    <xs:sequence>
      <xs:element name="ИдДок" type="ИдДокТип"/>
      <xs:element name="КодНО" type="СОНОТ"/>
      <xs:element name="ДатаНач" type="ДАТАТ"/>
      <xs:element name="ДатаКон" type="ДАТАТ"/>
      <xs:element name="ИННКО" type="ИННЮЛТип"/>
      <xs:element name="КППКО" type="КППТип"/>
      <xs:element name="БИК" type="БИКТ"/>
      <xs:element name="НомБ" type="НОМ4Т"/>
      <xs:element name="НомФ" type="НОМ4Т"/>
    <xs:choice>
    <xs:sequence>
      <xs:element name="ИННЮЛ" type="ИННЮЛТип"/>
      <xs:element name="КПП" type="КППТип"/>
    </xs:sequence>
    <xs:sequence>
      <xs:element name="ИННФЛ" type="ИННФЛТип"/>
    </xs:sequence>
    </xs:choice>
      <xs:element name="ВидЗапр" type="ЗНАЧ3Т"/>
      <xs:element name="ТипЗапр" type="ЗАПРОСТ"/>
      <xs:element name="НомСч" type="НОМ20Т" minOccurs="0" maxOccurs="unbounded"/>
    </xs:sequence>
  </xs:complexType>
  </xs:element>
  <xs:simpleType name="ИдДокТип">
      <xs:restriction base="xs:integer"><xs:totalDigits value="20"/></xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="СОНОТ">
    <xs:annotation>
      <xs:documentation>Коды из Классификатора системы обозначений налоговых органов</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string"><xs:pattern value="[0-9]{4}" /></xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="ДАТАТ">
    <xs:annotation>
      <xs:documentation>Дата в формате ДД.ММ.ГГГГ (01.01.1900 - 31.12.2099)</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:length value="10"/>
      <xs:pattern value="((((0[1-9]{1}|1[0-9]{1}|2[0-8]{1})\.(0[1-9]{1}|1[0-2]{1}))|((29|30)\.(01|0[3-9]{1}|1[0-2]{1}))|(31\.(01|03|05|07|08|10|12)))\.((19|20)[0-9]{2}))|(29\.02\.((19|20)(((0|2|4|6|8)(0|4|8))|((1|3|5|7|9)(2|6)))))"/>
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="ИННФЛТип">
    <xs:annotation>
      <xs:documentation>Идентификационный номер налогоплательщика - физического лица</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:length value="12"/>
      <xs:pattern value="([0-9]{1}[1-9]{1}|[1-9]{1}[0-9]{1})[0-9]{10}"/>
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="ИННЮЛТип">
    <xs:annotation>
      <xs:documentation>Идентификационный номер налогоплательщика - организации</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:length value="10"/>
      <xs:pattern value="([0-9]{1}[1-9]{1}|[1-9]{1}[0-9]{1})[0-9]{8}"/>
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="КППТип">
    <xs:annotation>
      <xs:documentation>Код причины постановки на учет (КПП) - 5 и 6 знаки от 0-9 и A-Z</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:length value="9"/>
      <xs:pattern value="([0-9]{1}[1-9]{1}|[1-9]{1}[0-9]{1})([0-9]{2})([0-9A-Z]{2})([0-9]{3})"/>
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="БИКТ">
    <xs:annotation>
      <xs:documentation>БИК банка (филиала) или учреждения Банка России</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string"><xs:pattern value="[0-9]{9}"/></xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="НОМ4Т">
    <xs:annotation>
      <xs:documentation>Номер до 4 знаков</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:integer"><xs:minInclusive value="0"/><xs:maxInclusive value="9999"/></xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="НОМ20Т">
    <xs:annotation>
      <xs:documentation>Номер счета 20 знаков</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string"><xs:pattern value="[0-9]{20}"/></xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="ЗНАЧ3Т">
    <xs:annotation>
      <xs:documentation>Значение = 3</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string"><xs:pattern value="3" /></xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="ЗАПРОСТ">
    <xs:annotation>
      <xs:documentation>Тип запроса (1 - по всем счетам, 2 - по указанным в запросе)</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string"><xs:pattern value="[1-2]{1}" /></xs:restriction>
  </xs:simpleType>
</xs:schema>';

  DBMS_XMLSCHEMA.REGISTERSCHEMA(
      SCHEMAURL => 'TAX3EXCH_NDS2_CAM_05_01.xsd',
      SCHEMADOC => v_doc,
      genTables => false, 
      local => true,
      owner => 'NDS2_MRR_USER'
  );
end;
/
