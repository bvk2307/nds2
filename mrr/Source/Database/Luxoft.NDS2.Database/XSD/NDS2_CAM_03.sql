﻿BEGIN
	DBMS_XMLSCHEMA.registerSchema(
    SCHEMAURL => 'TAX3EXCH_NDS2_CAM_03_01.xsd',
    SCHEMADOC => '<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:sql="urn:schemas-microsoft-com:mapping-schema" elementFormDefault="qualified" attributeFormDefault="unqualified">
	<xs:element name="Истреб93.1">
		<xs:annotation>
			<xs:documentation>Данные для истребования документов в рамках ст.93.1 НК РФ</xs:documentation>
		</xs:annotation>
		<xs:complexType>
			<xs:sequence>
				<xs:element name="СведИст">
					<xs:complexType>
						<xs:choice>
							<xs:element name="ОргИст">
								<xs:complexType>
									<xs:attribute name="ИННЮЛ" type="ИННЮЛТип" use="required"/>
									<xs:attribute name="КПП" type="КППТип" use="required"/>
									<xs:attribute name="НаимОрг" use="required">
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:minLength value="1"/>
												<xs:maxLength value="1000"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:attribute>
								</xs:complexType>
							</xs:element>
							<xs:element name="ФЛИст">
								<xs:complexType>
									<xs:sequence>
										<xs:element name="ФИО" type="ФИОТип"/>
									</xs:sequence>
									<xs:attribute name="ИННФЛ" type="ИННФЛТип" use="required"/>
								</xs:complexType>
							</xs:element>
						</xs:choice>
					</xs:complexType>
				</xs:element>
				<xs:element name="Истреб93.1Док" type="ДокТип" maxOccurs="99"/>
			</xs:sequence>
			<xs:attribute name="КодНО" type="СОНОТип" use="required"/>
			<xs:attribute name="РегНомДек" use="required">
				<xs:annotation>
					<xs:documentation>Регистрационный номер декларации</xs:documentation>
				</xs:annotation>
				<xs:simpleType>
					<xs:restriction base="xs:integer">
						<xs:totalDigits value="13"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:attribute>
			<xs:attribute name="УчНомИстреб" use="required">
				<xs:annotation>
					<xs:documentation>Учетный номер автоистребования</xs:documentation>
				</xs:annotation>
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:minLength value="1"/>
						<xs:maxLength value="20"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:attribute>
			<xs:attribute name="КодНОИсполн" type="СОНОТип" use="required"/>
			<xs:attribute name="ТипИнф" use="required">
				<xs:annotation>
					<xs:documentation>Тип информации</xs:documentation>
				</xs:annotation>
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:enumeration value="TAX3EXCH_NDS2_CAM_03"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:attribute>
		</xs:complexType>
	</xs:element>
	<xs:complexType name="ДокТип">
		<xs:sequence>
			<xs:choice>
				<xs:element name="СФ">
					<xs:complexType>
						<xs:choice>
							<xs:element name="ДатаДок" type="ДатаТип"/>
							<xs:element name="ПериодДок">
								<xs:complexType>
									<xs:attribute name="НачПер" type="ДатаТип" use="required"/>
									<xs:attribute name="ОконПер" type="ДатаТип" use="required"/>
								</xs:complexType>
							</xs:element>
						</xs:choice>
						<xs:attribute name="КодДок" use="required" fixed="0924">
							<xs:annotation>
								<xs:documentation>Код документа</xs:documentation>
							</xs:annotation>
							<xs:simpleType>
								<xs:restriction base="xs:string">
									<xs:length value="4"/>
								</xs:restriction>
							</xs:simpleType>
						</xs:attribute>
						<xs:attribute name="НомДок" use="required">
							<xs:annotation>
								<xs:documentation>Номер документа</xs:documentation>
							</xs:annotation>
							<xs:simpleType>
								<xs:restriction base="xs:string">
									<xs:minLength value="1"/>
									<xs:maxLength value="50"/>
								</xs:restriction>
							</xs:simpleType>
						</xs:attribute>
					</xs:complexType>
				</xs:element>
				<xs:element name="КСФ">
					<xs:complexType>
						<xs:choice>
							<xs:element name="ДатаДок" type="ДатаТип"/>
							<xs:element name="ПериодДок">
								<xs:complexType>
									<xs:attribute name="НачПер" type="ДатаТип" use="required"/>
									<xs:attribute name="ОконПер" type="ДатаТип" use="required"/>
								</xs:complexType>
							</xs:element>
						</xs:choice>
						<xs:attribute name="КодДок" use="required" fixed="2772">
							<xs:annotation>
								<xs:documentation>Код документа</xs:documentation>
							</xs:annotation>
							<xs:simpleType>
								<xs:restriction base="xs:string">
									<xs:length value="4"/>
								</xs:restriction>
							</xs:simpleType>
						</xs:attribute>
						<xs:attribute name="НомДок" use="required">
							<xs:annotation>
								<xs:documentation>Номер документа</xs:documentation>
							</xs:annotation>
							<xs:simpleType>
								<xs:restriction base="xs:string">
									<xs:minLength value="1"/>
									<xs:maxLength value="50"/>
								</xs:restriction>
							</xs:simpleType>
						</xs:attribute>
					</xs:complexType>
				</xs:element>
			</xs:choice>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="ФИОТип">
		<xs:annotation>
			<xs:documentation>Фамилия, имя, отчество</xs:documentation>
		</xs:annotation>
		<xs:attribute name="Фамилия" use="required">
			<xs:annotation>
				<xs:documentation>Фамилия</xs:documentation>
			</xs:annotation>
			<xs:simpleType>
				<xs:restriction base="xs:string">
					<xs:minLength value="1"/>
					<xs:maxLength value="60"/>
				</xs:restriction>
			</xs:simpleType>
		</xs:attribute>
		<xs:attribute name="Имя" use="required">
			<xs:annotation>
				<xs:documentation>Имя</xs:documentation>
			</xs:annotation>
			<xs:simpleType>
				<xs:restriction base="xs:string">
					<xs:minLength value="1"/>
					<xs:maxLength value="60"/>
				</xs:restriction>
			</xs:simpleType>
		</xs:attribute>
		<xs:attribute name="Отчество" use="optional">
			<xs:annotation>
				<xs:documentation>Отчество</xs:documentation>
			</xs:annotation>
			<xs:simpleType>
				<xs:restriction base="xs:string">
					<xs:minLength value="1"/>
					<xs:maxLength value="60"/>
				</xs:restriction>
			</xs:simpleType>
		</xs:attribute>
	</xs:complexType>
	<xs:simpleType name="ИННФЛТип">
		<xs:annotation>
			<xs:documentation>Идентификационный номер налогоплательщика - физического лица</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:string">
			<xs:length value="12"/>
			<xs:pattern value="([0-9]{1}[1-9]{1}|[1-9]{1}[0-9]{1})[0-9]{10}"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="ИННЮЛТип">
		<xs:annotation>
			<xs:documentation>Идентификационный номер налогоплательщика - организации</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:string">
			<xs:length value="10"/>
			<xs:pattern value="([0-9]{1}[1-9]{1}|[1-9]{1}[0-9]{1})[0-9]{8}"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="КППТип">
		<xs:annotation>
			<xs:documentation>Код причины постановки на учет (КПП) - 5 и 6 знаки от 0-9 и A-Z</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:string">
			<xs:length value="9"/>
			<xs:pattern value="([0-9]{1}[1-9]{1}|[1-9]{1}[0-9]{1})([0-9]{2})([0-9A-Z]{2})([0-9]{3})"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="СОНОТип">
		<xs:annotation>
			<xs:documentation>Коды из Классификатора системы обозначений налоговых органов</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:string">
			<xs:length value="4"/>
			<xs:pattern value="[0-9]{4}"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="ДатаТип">
		<xs:annotation>
			<xs:documentation>Дата в формате ДД.ММ.ГГГГ (01.01.1900 - 31.12.2099)</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:string">
			<xs:length value="10"/>
			<xs:pattern value="((((0[1-9]{1}|1[0-9]{1}|2[0-8]{1})\.(0[1-9]{1}|1[0-2]{1}))|((29|30)\.(01|0[3-9]{1}|1[0-2]{1}))|(31\.(01|03|05|07|08|10|12)))\.((19|20)[0-9]{2}))|(29\.02\.((19|20)(((0|2|4|6|8)(0|4|8))|((1|3|5|7|9)(2|6)))))"/>
		</xs:restriction>
	</xs:simpleType>
</xs:schema>',
     genTypes => false, 
     genTables => false, 
     local => true,
	 owner => 'NDS2_MRR_USER'
    );
exception when others then null;
END;
/
