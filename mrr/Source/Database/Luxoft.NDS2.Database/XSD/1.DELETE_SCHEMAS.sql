﻿begin
  for row in (select qual_schema_url from dba_xml_schemas where owner = 'NDS2_MRR_USER' and schema_url like 'TAX3EXCH_NDS2_CAM_%.xsd')
  loop
    begin
      DBMS_XMLSCHEMA.deleteSchema(row.qual_schema_url, dbms_xmlschema.DELETE_CASCADE_FORCE);
      exception when others then DBMS_OUTPUT.put_line(row.qual_schema_url || ': ' || substr(sqlerrm, 256));
    end;
  end loop;
end;
/
