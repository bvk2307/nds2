﻿DECLARE
	C1 clob;
BEGIN
c1 := '<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:sql="urn:schemas-microsoft-com:mapping-schema" elementFormDefault="qualified" attributeFormDefault="unqualified">
	<xs:element name="Автотребование">
		<xs:annotation>
			<xs:documentation>Данные о выявленных расхождениях</xs:documentation>
		</xs:annotation>
		<xs:complexType>
			<xs:sequence>
				<xs:element name="ПризнРсхжд">
					<xs:annotation>
						<xs:documentation>Признак расхождений</xs:documentation>
					</xs:annotation>
					<xs:complexType>
						<xs:sequence>
							<xs:element name="T1.1" minOccurs="0">
								<xs:complexType>
									<xs:sequence>
										<xs:element name="T1.1Стр" maxOccurs="unbounded">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="СпрКодОш">
														<xs:complexType>
															<xs:sequence>
																<xs:element name="ГрафОш" type="СпрКодОшГрафТип" minOccurs="0" maxOccurs="unbounded"/>
															</xs:sequence>
															<x';
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, 's:attribute name="Код" type="СпрКодОшТипТ1" use="required"/>
														</xs:complexType>
													</xs:element>
													<xs:element name="СвПрод" type="СвНплТип" minOccurs="0" maxOccurs="unbounded"/>
													<xs:element name="СвПос" type="СвНплТип" minOccurs="0"/>
													<xs:element name="ДокПдтвУпл" minOccurs="0" maxOccurs="unbounded">
														<xs:complexType>
															<xs:attribute name="НомДокПдтвУпл" use="optional">
																<xs:annotation>
																	<xs:documentation>Номер документа, подтверждающего оплату</xs:documentation>
																</xs:annotation>
																<xs:simpleType>
																	<xs:restriction base="xs:string">
																		<xs:minLength value="1"/>
																		<xs:maxLength value="256"/>
																	</xs:restriction>
																</xs:simpleType>
															</xs:attribute>
															<xs:attribute name="ДатаДокПдтвУпл" type="ДатаТип" use="optional"/>
														</xs:complexType>');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, '
													</xs:element>
													<xs:element name="КодВидОпер" type="КодВидОперТип" minOccurs="0" maxOccurs="unbounded"/>
													<xs:element name="ДатаУчТов" type="ДатаТип" minOccurs="0" maxOccurs="unbounded"/>
												</xs:sequence>
												<xs:attribute name="НомерПор" use="required">
													<xs:annotation>
														<xs:documentation>Порядковый номер записи Книги покупок (доп.листа)</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:int">
															<xs:totalDigits value="12"/>
															<xs:minInclusive value="1"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="НомСчФПрод" use="required">
													<xs:annotation>
														<xs:documentation>Номер счета-фактуры продавца</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:string">
				');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, '											<xs:minLength value="1"/>
															<xs:maxLength value="1000"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="ДатаСчФПрод" type="ДатаТип" use="optional"/>
												<xs:attribute name="НомИспрСчФ" use="optional">
													<xs:annotation>
														<xs:documentation>Номер исправления счета-фактуры продавца</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:int">
															<xs:totalDigits value="3"/>
															<xs:minInclusive value="1"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="ДатаИспрСчФ" type="ДатаТип" use="optional"/>
												<xs:attribute name="НомКСчФПрод" use="optional">
													<xs:annotation>
														<xs:documentation>Номер корректировочного счета-фактуры продавца</xs:documentation>
													</xs:annotation');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, '>
													<xs:simpleType>
														<xs:restriction base="xs:string">
															<xs:minLength value="1"/>
															<xs:maxLength value="256"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="ДатаКСчФПрод" type="ДатаТип" use="optional"/>
												<xs:attribute name="НомИспрКСчФ" use="optional">
													<xs:annotation>
														<xs:documentation>Номер исправления корректировочного счета-фактуры продавца</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:int">
															<xs:totalDigits value="3"/>
															<xs:minInclusive value="1"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="ДатаИспрКСчФ" type="ДатаТип" use="optional"/>
												<xs:attribute name="НомТД" use="optional">
													<xs:annotation>
														<xs:documenta');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, 'tion>Номер таможенной декларации</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:string">
															<xs:minLength value="1"/>
															<xs:maxLength value="1000"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="ОКВ" type="ОКВТип" use="optional"/>
												<xs:attribute name="СтоимПокупВ" use="required">
													<xs:annotation>
														<xs:documentation>Стоимость покупок по счету-фактуре, разница стоимости по корректировочному счету-фактуре (включая НДС), в валюте счета-фактуры</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:decimal">
															<xs:totalDigits value="19"/>
															<xs:fractionDigits value="2"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="СумНДСВыч" us');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, 'e="required">
												  <xs:annotation>
													<xs:documentation>Сумма налога по счету-фактуре, разница суммы налога по корректировочному счету-фактуре, принимаемая к вычету, в руб. и коп.</xs:documentation>
												  </xs:annotation>
												  <xs:simpleType>
													<xs:restriction base="xs:decimal">
													  <xs:totalDigits value="19"/>
													  <xs:fractionDigits value="2"/>
													</xs:restriction>
												  </xs:simpleType>
												</xs:attribute>
											</xs:complexType>
										</xs:element>
									</xs:sequence>
								</xs:complexType>
							</xs:element>
							<xs:element name="T1.2" minOccurs="0">
								<xs:complexType>
									<xs:sequence>
										<xs:element name="T1.2Стр" maxOccurs="unbounded">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="СпрКодОш">
														<xs:complexType>
															<xs:sequence>
																<xs:element name="ГрафОш" type="СпрКодОшГр');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, 'афТип" minOccurs="0" maxOccurs="unbounded"/>
															</xs:sequence>
															<xs:attribute name="Код" type="СпрКодОшТипТ2" use="required"/>
														</xs:complexType>
													</xs:element>
													<xs:element name="СвПокуп" type="СвНплТип" minOccurs="0" maxOccurs="unbounded"/>
													<xs:element name="СвПос" type="СвНплТип" minOccurs="0"/>
													<xs:element name="ДокПдтвОпл" minOccurs="0" maxOccurs="unbounded">
														<xs:complexType>
															<xs:attribute name="НомДокПдтвОпл" use="optional">
																<xs:annotation>
																	<xs:documentation>Номер документа, подтверждающего оплату</xs:documentation>
																</xs:annotation>
																<xs:simpleType>
																	<xs:restriction base="xs:string">
																		<xs:minLength value="1"/>
																		<xs:maxLength value="256"/>
																	</xs:restriction>
																</xs:simpleType>
															</xs:attribute>
															<xs:a');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, 'ttribute name="ДатаДокПдтвОпл" type="ДатаТип" use="optional"/>
														</xs:complexType>
													</xs:element>
													<xs:element name="КодВидОпер" type="КодВидОперТип" minOccurs="0" maxOccurs="unbounded"/>
												</xs:sequence>
												<xs:attribute name="НомерПор" use="required">
													<xs:annotation>
														<xs:documentation>Порядковый номер записи Книги продаж (доп.листа)</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:int">
															<xs:totalDigits value="12"/>
															<xs:minInclusive value="1"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="НомСчФПрод" use="required">
													<xs:annotation>
														<xs:documentation>Номер счета-фактуры продавца</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:string">
						');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, '									<xs:minLength value="1"/>
															<xs:maxLength value="1000"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="ДатаСчФПрод" type="ДатаТип" use="required"/>
												<xs:attribute name="НомИспрСчФ" use="optional">
													<xs:annotation>
														<xs:documentation>Номер исправления счета-фактуры продавца</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:int">
															<xs:totalDigits value="3"/>
															<xs:minInclusive value="1"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="ДатаИспрСчФ" type="ДатаТип" use="optional"/>
												<xs:attribute name="НомКСчФПрод" use="optional">
													<xs:annotation>
														<xs:documentation>Номер корректировочного счета-фактуры продавца</xs:documentation>
													</xs:annotation>');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, '
													<xs:simpleType>
														<xs:restriction base="xs:string">
															<xs:minLength value="1"/>
															<xs:maxLength value="256"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="ДатаКСчФПрод" type="ДатаТип" use="optional"/>
												<xs:attribute name="НомИспрКСчФ" use="optional">
													<xs:annotation>
														<xs:documentation>Номер исправления корректировочного счета-фактуры продавца</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:int">
															<xs:totalDigits value="3"/>
															<xs:minInclusive value="1"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="ДатаИспрКСчФ" type="ДатаТип" use="optional"/>
												<xs:attribute name="ОКВ" type="ОКВТип" use="optional"/>
												<xs:attribute name="СтоимПродСФВ" ');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, 'use="optional">
													<xs:annotation>
														<xs:documentation>Стоимость продаж по счету-фактуре, разница стоимости по корректировочному счету-фактуре (включая НДС), в валюте счета-фактуры</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:decimal">
															<xs:totalDigits value="19"/>
															<xs:fractionDigits value="2"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="СтоимПродСФ18" use="optional">
													<xs:annotation>
														<xs:documentation>Стоимость продаж, облагаемых налогом, по счету-фактуре, разница стоимости по корректировочному счету-фактуре (без НДС), в руб. и коп., по ставке 18%</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:decimal">
															<xs:totalDigits value="19"/>
															<xs:fractionDigits value="2"/>
					');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, '									</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="СтоимПродСФ10" use="optional">
													<xs:annotation>
														<xs:documentation>Стоимость продаж, облагаемых налогом, по счету-фактуре, разница стоимости по корректировочному счету-фактуре (без НДС), в руб. и коп., по ставке 10%</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:decimal">
															<xs:totalDigits value="19"/>
															<xs:fractionDigits value="2"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="СтоимПродСФ0" use="optional">
													<xs:annotation>
														<xs:documentation>Стоимость продаж, облагаемых налогом, по счету-фактуре, разница стоимости по корректировочному счету-фактуре (без НДС), в руб. и коп., по ставке 0%</xs:documentation>
													</xs:annotation>
													<xs:simp');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, 'leType>
														<xs:restriction base="xs:decimal">
															<xs:totalDigits value="19"/>
															<xs:fractionDigits value="2"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="СумНДССФ18" use="optional">
													<xs:annotation>
														<xs:documentation>Сумма НДС по счету-фактуре, разница стоимости по корректировочному счету-фактуре, в руб. и коп., по ставке 18%</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:decimal">
															<xs:totalDigits value="19"/>
															<xs:fractionDigits value="2"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="СумНДССФ10" use="optional">
													<xs:annotation>
														<xs:documentation>Сумма НДС по счету-фактуре, разница стоимости по корректировочному счету-фактуре, в руб. и коп., по ставке ');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, '10%</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:decimal">
															<xs:totalDigits value="19"/>
															<xs:fractionDigits value="2"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="СтоимПродОсв" use="optional">
													<xs:annotation>
														<xs:documentation>Стоимость продаж, освобождаемых от налога, по счету-фактуре, разница стоимости по корректировочному счету-фактуре, в руб. и коп.</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:decimal">
															<xs:totalDigits value="19"/>
															<xs:fractionDigits value="2"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
											</xs:complexType>
										</xs:element>
									</xs:sequence>
								</xs:complexType>
							</xs:element>
					');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, '		<xs:element name="T1.3" minOccurs="0">
								<xs:complexType>
									<xs:sequence>
										<xs:element name="T1.3Стр" maxOccurs="unbounded">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="СпрКодОш">
														<xs:complexType>
															<xs:sequence>
																<xs:element name="ГрафОш" type="СпрКодОшГрафТип" minOccurs="0" maxOccurs="unbounded"/>
															</xs:sequence>
															<xs:attribute name="Код" type="СпрКодОшТипТ3" use="required"/>
														</xs:complexType>
													</xs:element>
													<xs:element name="СвПокуп" type="СвНплТип" minOccurs="0"/>
													<xs:element name="СвПосрДеят" minOccurs="0" maxOccurs="unbounded">
														<xs:complexType>
															<xs:sequence>
																<xs:element name="СвПосрДеятСтр" minOccurs="0" maxOccurs="unbounded">
																	<xs:complexType>
																		<xs:sequence>
																			<xs:element name="СвПрод" type="СвНплТип" min');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, 'Occurs="0"/>
																		</xs:sequence>
																		<xs:attribute name="НомСчФОтПрод" use="required">
																			<xs:annotation>
																				<xs:documentation>Номер счета-фактуры, полученного от продавца</xs:documentation>
																			</xs:annotation>
																			<xs:simpleType>
																				<xs:restriction base="xs:string">
																					<xs:minLength value="1"/>
																					<xs:maxLength value="1000"/>
																				</xs:restriction>
																			</xs:simpleType>
																		</xs:attribute>
																		<xs:attribute name="ДатаСчФОтПрод" type="ДатаТип" use="required"/>
																		<xs:attribute name="ОКВ" type="ОКВТип" use="optional"/>
																		<xs:attribute name="СтоимТовСчФВс" use="optional">
																			<xs:annotation>
																				<xs:documentation>Стоимость товаров (работ, услуг), имущественных прав по счету-фактуре – всего</xs:documentation>
																			</xs:ann');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, 'otation>
																			<xs:simpleType>
																				<xs:restriction base="xs:decimal">
																					<xs:totalDigits value="19"/>
																					<xs:fractionDigits value="2"/>
																				</xs:restriction>
																			</xs:simpleType>
																		</xs:attribute>
																		<xs:attribute name="СумНДССчФ" use="optional">
																			<xs:annotation>
																				<xs:documentation>В том числе сумма налога по счету-фактуре</xs:documentation>
																			</xs:annotation>
																			<xs:simpleType>
																				<xs:restriction base="xs:decimal">
																					<xs:totalDigits value="19"/>
																					<xs:fractionDigits value="2"/>
																				</xs:restriction>
																			</xs:simpleType>
																		</xs:attribute>
																		<xs:attribute name="РазСтКСчФУм" use="optional">
																			<xs:annotation>
																				<xs:documentation>Разница стоимости с');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, ' учетом НДС по корректировочному счету-фактуре – уменьшение</xs:documentation>
																			</xs:annotation>
																			<xs:simpleType>
																				<xs:restriction base="xs:decimal">
																					<xs:totalDigits value="19"/>
																					<xs:fractionDigits value="2"/>
																				</xs:restriction>
																			</xs:simpleType>
																		</xs:attribute>
																		<xs:attribute name="РазСтКСчФУв" use="optional">
																			<xs:annotation>
																				<xs:documentation>Разница стоимости с учетом НДС по корректировочному счету-фактуре – увеличение</xs:documentation>
																			</xs:annotation>
																			<xs:simpleType>
																				<xs:restriction base="xs:decimal">
																					<xs:totalDigits value="19"/>
																					<xs:fractionDigits value="2"/>
																				</xs:restriction>
																			</xs:simpleType>
																		</xs:attribute>
															');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, '			<xs:attribute name="РазНДСКСчФУм" use="optional">
																			<xs:annotation>
																				<xs:documentation>Разница НДС по корректировочному счету-фактуре – уменьшение</xs:documentation>
																			</xs:annotation>
																			<xs:simpleType>
																				<xs:restriction base="xs:decimal">
																					<xs:totalDigits value="19"/>
																					<xs:fractionDigits value="2"/>
																				</xs:restriction>
																			</xs:simpleType>
																		</xs:attribute>
																		<xs:attribute name="РазНДСКСчФУв" use="optional">
																			<xs:annotation>
																				<xs:documentation>Разница НДС по корректировочному счету-фактуре – увеличение</xs:documentation>
																			</xs:annotation>
																			<xs:simpleType>
																				<xs:restriction base="xs:decimal">
																					<xs:totalDigits value="19"/>
																					<xs:fractionDigits value="2"/>
																');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, '				</xs:restriction>
																			</xs:simpleType>
																		</xs:attribute>
																	</xs:complexType>
																</xs:element>
															</xs:sequence>
														</xs:complexType>
													</xs:element>
													<xs:element name="КодВидОпер" type="КодВидОперТип" minOccurs="0" maxOccurs="unbounded"/>
												</xs:sequence>
												<xs:attribute name="НомерПор" use="required">
													<xs:annotation>
														<xs:documentation>Порядковый номер записи Журнала выставленных счетов-фактур</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:int">
															<xs:totalDigits value="12"/>
															<xs:minInclusive value="1"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="НомСчФПрод" use="required">
													<xs:annotation>
														<xs:documentation>Номер счета-фактуры продав');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, 'ца</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:string">
															<xs:minLength value="1"/>
															<xs:maxLength value="1000"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="ДатаСчФПрод" type="ДатаТип" use="required"/>
												<xs:attribute name="НомИспрСчФ" use="optional">
													<xs:annotation>
														<xs:documentation>Номер исправления счета-фактуры продавца</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:int">
															<xs:totalDigits value="3"/>
															<xs:minInclusive value="1"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="ДатаИспрСчФ" type="ДатаТип" use="optional"/>
												<xs:attribute name="НомКСчФПрод" use="optional">
													<xs:ann');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, 'otation>
														<xs:documentation>Номер корректировочного счета-фактуры продавца</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:string">
															<xs:minLength value="1"/>
															<xs:maxLength value="256"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="ДатаКСчФПрод" type="ДатаТип" use="optional"/>
												<xs:attribute name="НомИспрКСчФ" use="optional">
													<xs:annotation>
														<xs:documentation>Номер исправления корректировочного счета-фактуры продавца</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:int">
															<xs:totalDigits value="3"/>
															<xs:minInclusive value="1"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="ДатаИспрКСчФ" type="Дат');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, 'аТип" use="optional"/>
											</xs:complexType>
										</xs:element>
									</xs:sequence>
								</xs:complexType>
							</xs:element>
							<xs:element name="T1.4" minOccurs="0">
								<xs:complexType>
									<xs:sequence>
										<xs:element name="T1.4Стр" maxOccurs="unbounded">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="СпрКодОш">
														<xs:complexType>
															<xs:sequence>
																<xs:element name="ГрафОш" type="СпрКодОшГрафТип" minOccurs="0" maxOccurs="unbounded"/>
															</xs:sequence>
															<xs:attribute name="Код" type="СпрКодОшТипТ4" use="required"/>
														</xs:complexType>
													</xs:element>
													<xs:element name="СвПрод" type="СвНплТип" minOccurs="0"/>
													<xs:element name="СвСубком" type="СвНплТип" minOccurs="0"/>
													<xs:element name="КодВидОпер" type="КодВидОперТип" minOccurs="0" maxOccurs="unbounded"/>
												</xs:sequence>
');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, '												<xs:attribute name="НомерПор" use="required">
													<xs:annotation>
														<xs:documentation>Порядковый номер записи Журнала выставленных счетов-фактур</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:int">
															<xs:totalDigits value="12"/>
															<xs:minInclusive value="1"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="НомСчФПрод" use="required">
													<xs:annotation>
														<xs:documentation>Номер счета-фактуры</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:string">
															<xs:minLength value="1"/>
															<xs:maxLength value="1000"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="ДатаСчФПрод" type="ДатаТип" use="required"/>
			');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, '									<xs:attribute name="НомИспрСчФ" use="optional">
													<xs:annotation>
														<xs:documentation>Номер исправления счета-фактуры</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:int">
															<xs:totalDigits value="3"/>
															<xs:minInclusive value="1"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="ДатаИспрСчФ" type="ДатаТип" use="optional"/>
												<xs:attribute name="НомКСчФПрод" use="optional">
													<xs:annotation>
														<xs:documentation>Номер корректировочного счета-фактуры</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:string">
															<xs:minLength value="1"/>
															<xs:maxLength value="256"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<x');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, 's:attribute name="ДатаКСчФПрод" type="ДатаТип" use="optional"/>
												<xs:attribute name="НомИспрКСчФ" use="optional">
													<xs:annotation>
														<xs:documentation>Номер исправления корректировочного счета-фактуры</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:int">
															<xs:totalDigits value="3"/>
															<xs:minInclusive value="1"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="ДатаИспрКСчФ" type="ДатаТип" use="optional"/>
												<xs:attribute name="КодВидСд" use="required">
													<xs:annotation>
														<xs:documentation>Код вида сделки</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:string">
															<xs:length value="1"/>
															<xs:enumeration value="1"/>
															<xs:enumeration value="2"/>
			');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, '												<xs:enumeration value="3"/>
															<xs:enumeration value="4"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="ОКВ" type="ОКВТип" use="optional"/>
												<xs:attribute name="СтоимТовСчФВс" use="required">
													<xs:annotation>
														<xs:documentation>Стоимость товаров (работ, услуг), имущественных прав по счету-фактуре – всего</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:decimal">
															<xs:totalDigits value="19"/>
															<xs:fractionDigits value="2"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="СумНДССчФ" use="optional">
													<xs:annotation>
														<xs:documentation>В том числе сумма налога по счету-фактуре</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
												');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, '		<xs:restriction base="xs:decimal">
															<xs:totalDigits value="19"/>
															<xs:fractionDigits value="2"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="РазСтКСчФУм" use="optional">
													<xs:annotation>
														<xs:documentation>Разница стоимости с учетом НДС по корректировочному счету-фактуре – уменьшение</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:decimal">
															<xs:totalDigits value="19"/>
															<xs:fractionDigits value="2"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="РазСтКСчФУв" use="optional">
													<xs:annotation>
														<xs:documentation>Разница стоимости с учетом НДС по корректировочному счету-фактуре – увеличение</xs:documentation>
													</xs:annotation>
													<xs:simpleType>');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, '
														<xs:restriction base="xs:decimal">
															<xs:totalDigits value="19"/>
															<xs:fractionDigits value="2"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="РазНДСКСчФУм" use="optional">
													<xs:annotation>
														<xs:documentation>Разница налога по корректировочному счету-фактуре – уменьшение</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:decimal">
															<xs:totalDigits value="19"/>
															<xs:fractionDigits value="2"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="РазНДСКСчФУв" use="optional">
													<xs:annotation>
														<xs:documentation>Разница налога по корректировочному счету-фактуре – увеличение</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, '<xs:restriction base="xs:decimal">
															<xs:totalDigits value="19"/>
															<xs:fractionDigits value="2"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
											</xs:complexType>
										</xs:element>
									</xs:sequence>
								</xs:complexType>
							</xs:element>
							<xs:element name="T1.5" minOccurs="0">
								<xs:complexType>
									<xs:sequence>
										<xs:element name="T1.5Стр" maxOccurs="unbounded">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="СпрКодОш">
														<xs:complexType>
															<xs:sequence>
																<xs:element name="ГрафОш" type="СпрКодОшГрафТип" minOccurs="0" maxOccurs="unbounded"/>
															</xs:sequence>
															<xs:attribute name="Код" type="СпрКодОшТипТ5" use="required"/>
														</xs:complexType>
													</xs:element>
													<xs:element name="СвПокуп" type="СвНплТип" minOccurs="0"/>
							');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, '					</xs:sequence>
												<xs:attribute name="НомСчФ" use="required">
													<xs:annotation>
														<xs:documentation>Номер счета-фактуры</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:string">
															<xs:minLength value="1"/>
															<xs:maxLength value="1000"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="ДатаСчФ" type="ДатаТип" use="required"/>
												<xs:attribute name="ОКВ" type="ОКВТип" use="optional"/>
												<xs:attribute name="СтоимТовБНалВс" use="required">
													<xs:annotation>
														<xs:documentation>Стоимость товаров (работ, услуг), имущественных прав без налога – всего</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:decimal">
															<xs:totalDigits value="19"/>
															<xs:fractionDigits va');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, 'lue="2"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="СумНалПокуп" use="required">
													<xs:annotation>
														<xs:documentation>Сумма налога, предъявляемая покупателю</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:decimal">
															<xs:totalDigits value="19"/>
															<xs:fractionDigits value="2"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="СтоимТовСНалВс" use="required">
													<xs:annotation>
														<xs:documentation>Стоимость товаров (работ, услуг), имущественных прав с налогом  - всего</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:decimal">
															<xs:totalDigits value="19"/>
															<xs:fractionDigits value="2"/>
														</xs:');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, 'restriction>
													</xs:simpleType>
												</xs:attribute>
											</xs:complexType>
										</xs:element>
									</xs:sequence>
								</xs:complexType>
							</xs:element>
							<xs:element name="T1.6" minOccurs="0">
								<xs:complexType>
									<xs:sequence>
										<xs:element name="T1.6Стр" maxOccurs="unbounded">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="СвКАгент" type="СвУчСдТип"/>
													<xs:element name="СведСФ" maxOccurs="unbounded">
														<xs:complexType>
															<xs:attribute name="НомСчФ" use="required">
																<xs:annotation>
																	<xs:documentation>Номер счета-фактуры</xs:documentation>
																</xs:annotation>
																<xs:simpleType>
																	<xs:restriction base="xs:string">
																		<xs:minLength value="1"/>
																		<xs:maxLength value="256"/>
																	</xs:restriction>
																</xs:simpleType>
		');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, '													</xs:attribute>
															<xs:attribute name="ДатаСчФ" type="ДатаТип" use="required">
																<xs:annotation>
																	<xs:documentation>Дата счета-фактуры</xs:documentation>
																</xs:annotation>
															</xs:attribute>
														</xs:complexType>
													</xs:element>
												</xs:sequence>
											</xs:complexType>
										</xs:element>
									</xs:sequence>
								</xs:complexType>
							</xs:element>
							<xs:element name="T1.7" minOccurs="0">
								<xs:complexType>
									<xs:sequence>
										<xs:element name="T1.7Стр" maxOccurs="unbounded">
											<xs:complexType>
												<xs:attribute name="СвНар" use="required">
													<xs:annotation>
														<xs:documentation>Сведения о нарушении</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:string">
															<xs:length value="2"/>
															<xs:enumeration v');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, 'alue="10"/>
															<xs:enumeration value="12"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="КодКС" use="required">
													<xs:annotation>
														<xs:documentation>Код контрольного соотношения</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:string">
															<xs:minLength value="3"/>
															<xs:maxLength value="6"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="СумЛевЧ" use="required">
													<xs:annotation>
														<xs:documentation>Левая часть (сумма)</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:decimal">
															<xs:totalDigits value="19"/>
															<xs:fractionDigits value="2"/>
														</xs:restriction>
													</xs:simpl');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, 'eType>
												</xs:attribute>
												<xs:attribute name="СумПравЧ" use="required">
													<xs:annotation>
														<xs:documentation>Правая часть (сумма)</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:decimal">
															<xs:totalDigits value="19"/>
															<xs:fractionDigits value="2"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
											</xs:complexType>
										</xs:element>
									</xs:sequence>
								</xs:complexType>
							</xs:element>
							<xs:element name="Т1.8" minOccurs="0">
								<xs:complexType>
									<xs:sequence>
										<xs:element name="Т1.8Стр" maxOccurs="unbounded">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="СпрКодОш">
														<xs:complexType>
															<xs:sequence>
																<xs:element name="ГрафОш" type="СпрКодОшГрафТип" minOccurs="0" ma');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, 'xOccurs="unbounded"/>
															</xs:sequence>
															<xs:attribute name="Код" type="СпрКодОшТипТ8" use="required"/>
														</xs:complexType>
													</xs:element>
													<xs:element name="СвПрод" type="СвНплТип" minOccurs="0" maxOccurs="unbounded"/>
													<xs:element name="СвПос" type="СвНплТип" minOccurs="0"/>
													<xs:element name="ДокПдтвУпл" minOccurs="0" maxOccurs="unbounded">
														<xs:complexType>
															<xs:attribute name="НомДокПдтвУпл" use="optional">
																<xs:annotation>
																	<xs:documentation>Номер документа, подтверждающего оплату</xs:documentation>
																</xs:annotation>
																<xs:simpleType>
																	<xs:restriction base="xs:string">
																		<xs:minLength value="1"/>
																		<xs:maxLength value="256"/>
																	</xs:restriction>
																</xs:simpleType>
															</xs:attribute>
															<xs:attribute name="ДатаДокПд');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, 'твУпл" type="ДатаТип" use="optional"/>
														</xs:complexType>
													</xs:element>
													<xs:element name="КодВидОпер" type="КодВидОперТип" minOccurs="0" maxOccurs="unbounded"/>
													<xs:element name="ДатаУчТов" type="ДатаТип" minOccurs="0" maxOccurs="unbounded"/>
												</xs:sequence>
												<xs:attribute name="НомерПор" use="required">
													<xs:annotation>
														<xs:documentation>Порядковый номер записи Книги покупок (доп.листа)</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:int">
															<xs:totalDigits value="12"/>
															<xs:minInclusive value="1"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="НомСчФПрод" use="required">
													<xs:annotation>
														<xs:documentation>Номер счета-фактуры продавца</xs:documentation>
													</xs:annotation>
												');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, '	<xs:simpleType>
														<xs:restriction base="xs:string">
															<xs:minLength value="1"/>
															<xs:maxLength value="1000"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="ДатаСчФПрод" type="ДатаТип" use="optional"/>
												<xs:attribute name="НомИспрСчФ" use="optional">
													<xs:annotation>
														<xs:documentation>Номер исправления счета-фактуры продавца</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:int">
															<xs:totalDigits value="3"/>
															<xs:minInclusive value="1"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="ДатаИспрСчФ" type="ДатаТип" use="optional"/>
												<xs:attribute name="НомКСчФПрод" use="optional">
													<xs:annotation>
														<xs:documentation>Номер корректировочного ');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, 'счета-фактуры продавца</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:string">
															<xs:minLength value="1"/>
															<xs:maxLength value="256"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="ДатаКСчФПрод" type="ДатаТип" use="optional"/>
												<xs:attribute name="НомИспрКСчФ" use="optional">
													<xs:annotation>
														<xs:documentation>Номер исправления корректировочного счета-фактуры продавца</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:int">
															<xs:totalDigits value="3"/>
															<xs:minInclusive value="1"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="ДатаИспрКСчФ" type="ДатаТип" use="optional"/>
												<xs:attribute name="НомТД" use');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, '="optional">
													<xs:annotation>
														<xs:documentation>Номер таможенной декларации</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:string">
															<xs:minLength value="1"/>
															<xs:maxLength value="1000"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="ОКВ" type="ОКВТип" use="optional"/>
												<xs:attribute name="СтоимПокупВ" use="required">
													<xs:annotation>
														<xs:documentation>Стоимость покупок по счету-фактуре, разница стоимости по корректировочному счету-фактуре (включая НДС), в валюте счета-фактуры</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:decimal">
															<xs:totalDigits value="19"/>
															<xs:fractionDigits value="2"/>
														</xs:restriction>
													</xs:simpleType>
			');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, '									</xs:attribute>
												<xs:attribute name="СумНДСВыч" use="required">
													<xs:annotation>
														<xs:documentation>Сумма налога по счету-фактуре, разница суммы налога по корректировочному счету-фактуре, принимаемая к вычету, в руб. и коп.</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:decimal">
															<xs:totalDigits value="19"/>
															<xs:fractionDigits value="2"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
											</xs:complexType>
										</xs:element>
									</xs:sequence>
								</xs:complexType>
							</xs:element>
							<xs:element name="Т1.9" minOccurs="0">
								<xs:complexType>
									<xs:sequence>
										<xs:element name="T1.9Стр" maxOccurs="unbounded">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="СпрКодОш">
														<xs:complexType>
															<xs');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, ':sequence>
																<xs:element name="ГрафОш" type="СпрКодОшГрафТип" minOccurs="0" maxOccurs="unbounded"/>
															</xs:sequence>
															<xs:attribute name="Код" type="СпрКодОшТипТ9" use="required"/>
														</xs:complexType>
													</xs:element>
													<xs:element name="СвПокуп" type="СвНплТип" minOccurs="0" maxOccurs="unbounded"/>
													<xs:element name="СвПос" type="СвНплТип" minOccurs="0"/>
													<xs:element name="ДокПдтвОпл" minOccurs="0" maxOccurs="unbounded">
														<xs:complexType>
															<xs:attribute name="НомДокПдтвОпл" use="optional">
																<xs:annotation>
																	<xs:documentation>Номер документа, подтверждающего оплату</xs:documentation>
																</xs:annotation>
																<xs:simpleType>
																	<xs:restriction base="xs:string">
																		<xs:minLength value="1"/>
																		<xs:maxLength value="256"/>
																	</xs:restriction>
																');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, '</xs:simpleType>
															</xs:attribute>
															<xs:attribute name="ДатаДокПдтвОпл" type="ДатаТип" use="optional"/>
														</xs:complexType>
													</xs:element>
													<xs:element name="КодВидОпер" type="КодВидОперТип" minOccurs="0" maxOccurs="unbounded"/>
												</xs:sequence>
												<xs:attribute name="НомерПор" use="required">
													<xs:annotation>
														<xs:documentation>Порядковый номер записи Книги продаж (доп.листа)</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:int">
															<xs:totalDigits value="12"/>
															<xs:minInclusive value="1"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="НомСчФПрод" use="required">
													<xs:annotation>
														<xs:documentation>Номер счета-фактуры продавца</xs:documentation>
													</xs:annotation>
													<x');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, 's:simpleType>
														<xs:restriction base="xs:string">
															<xs:minLength value="1"/>
															<xs:maxLength value="1000"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="ДатаСчФПрод" type="ДатаТип" use="required"/>
												<xs:attribute name="НомИспрСчФ" use="optional">
													<xs:annotation>
														<xs:documentation>Номер исправления счета-фактуры продавца</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:int">
															<xs:totalDigits value="3"/>
															<xs:minInclusive value="1"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="ДатаИспрСчФ" type="ДатаТип" use="optional"/>
												<xs:attribute name="НомКСчФПрод" use="optional">
													<xs:annotation>
														<xs:documentation>Номер корректировочного сче');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, 'та-фактуры продавца</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:string">
															<xs:minLength value="1"/>
															<xs:maxLength value="256"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="ДатаКСчФПрод" type="ДатаТип" use="optional"/>
												<xs:attribute name="НомИспрКСчФ" use="optional">
													<xs:annotation>
														<xs:documentation>Номер исправления корректировочного счета-фактуры продавца</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:int">
															<xs:totalDigits value="3"/>
															<xs:minInclusive value="1"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="ДатаИспрКСчФ" type="ДатаТип" use="optional"/>
												<xs:attribute name="ОКВ" type="ОК');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, 'ВТип" use="optional"/>
												<xs:attribute name="СтоимПродСФВ" use="optional">
													<xs:annotation>
														<xs:documentation>Стоимость продаж по счету-фактуре, разница стоимости по корректировочному счету-фактуре (включая НДС), в валюте счета-фактуры</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:decimal">
															<xs:totalDigits value="19"/>
															<xs:fractionDigits value="2"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="СтоимПродСФ18" use="optional">
													<xs:annotation>
														<xs:documentation>Стоимость продаж, облагаемых налогом, по счету-фактуре, разница стоимости по корректировочному счету-фактуре (без НДС), в руб. и коп., по ставке 18%</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:decimal">
															<xs:totalDig');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, 'its value="19"/>
															<xs:fractionDigits value="2"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="СтоимПродСФ10" use="optional">
													<xs:annotation>
														<xs:documentation>Стоимость продаж, облагаемых налогом, по счету-фактуре, разница стоимости по корректировочному счету-фактуре (без НДС), в руб. и коп., по ставке 10%</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:decimal">
															<xs:totalDigits value="19"/>
															<xs:fractionDigits value="2"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="СтоимПродСФ0" use="optional">
													<xs:annotation>
														<xs:documentation>Стоимость продаж, облагаемых налогом, по счету-фактуре, разница стоимости по корректировочному счету-фактуре (без НДС), в руб. и коп., по ставке 0%</x');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, 's:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:decimal">
															<xs:totalDigits value="19"/>
															<xs:fractionDigits value="2"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="СумНДССФ18" use="optional">
													<xs:annotation>
														<xs:documentation>Сумма НДС по счету-фактуре, разница стоимости по корректировочному счету-фактуре, в руб. и коп., по ставке 18%</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:decimal">
															<xs:totalDigits value="19"/>
															<xs:fractionDigits value="2"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="СумНДССФ10" use="optional">
													<xs:annotation>
														<xs:documentation>Сумма НДС по счету-фактуре, разница с');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, 'тоимости по корректировочному счету-фактуре, в руб. и коп., по ставке 10%</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:decimal">
															<xs:totalDigits value="19"/>
															<xs:fractionDigits value="2"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="СтоимПродОсв" use="optional">
													<xs:annotation>
														<xs:documentation>Стоимость продаж, освобождаемых от налога, по счету-фактуре, разница стоимости по корректировочному счету-фактуре, в руб. и коп.</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:decimal">
															<xs:totalDigits value="19"/>
															<xs:fractionDigits value="2"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
											</xs:complexType>
										</xs:element>
									');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, '</xs:sequence>
								</xs:complexType>
							</xs:element>
						</xs:sequence>
					</xs:complexType>
				</xs:element>
			</xs:sequence>
			<xs:attribute name="КодНО" type="СОНОТип" use="required"/>
			<xs:attribute name="РегНомДек" use="required">
				<xs:annotation>
					<xs:documentation>Регистрационный номер декларации</xs:documentation>
				</xs:annotation>
				<xs:simpleType>
					<xs:restriction base="xs:int">
						<xs:totalDigits value="13"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:attribute>
			<xs:attribute name="УчНомТреб" use="required">
				<xs:annotation>
					<xs:documentation>Учетный номер автотребования</xs:documentation>
				</xs:annotation>
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:minLength value="1"/>
						<xs:maxLength value="20"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:attribute>
			<xs:attribute name="ТипИнф" use="required">
				<xs:annotation>
					<xs:documentation>Тип информации</xs:documentati');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, 'on>
				</xs:annotation>
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:enumeration value="TAX3EXCH_NDS2_CAM_01"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:attribute>
		</xs:complexType>
	</xs:element>
	<xs:complexType name="СвУчСдТип">
		<xs:annotation>
			<xs:documentation>Сведения об участнике сделки</xs:documentation>
		</xs:annotation>
		<xs:choice>
			<xs:element name="СведЮЛ">
				<xs:annotation>
					<xs:documentation>Сведения об организации</xs:documentation>
				</xs:annotation>
				<xs:complexType>
					<xs:attribute name="НаимКАгент" use="required">
						<xs:simpleType>
							<xs:restriction base="xs:string">
								<xs:minLength value="1"/>
								<xs:maxLength value="1000"/>
							</xs:restriction>
						</xs:simpleType>
					</xs:attribute>
					<xs:attribute name="ИННЮЛ" type="ИННЮЛТип" use="required">
						<xs:annotation>
							<xs:documentation>ИНН организации</xs:documentation>
						</xs:annotation>
					</xs:attribute');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, '>
					<xs:attribute name="КПП" type="КППТип">
						<xs:annotation>
							<xs:documentation>КПП</xs:documentation>
						</xs:annotation>
					</xs:attribute>
				</xs:complexType>
			</xs:element>
			<xs:element name="СведИП">
				<xs:annotation>
					<xs:documentation>Сведения об индивидуальном предпринимателе</xs:documentation>
				</xs:annotation>
				<xs:complexType>
					<xs:attribute name="Фамилия" use="required">
						<xs:simpleType>
							<xs:restriction base="xs:string">
								<xs:maxLength value="60"/>
								<xs:minLength value="1"/>
							</xs:restriction>
						</xs:simpleType>
					</xs:attribute>
					<xs:attribute name="Имя" use="required">
						<xs:simpleType>
							<xs:restriction base="xs:string">
								<xs:maxLength value="60"/>
								<xs:minLength value="1"/>
							</xs:restriction>
						</xs:simpleType>
					</xs:attribute>
					<xs:attribute name="Отчество">
						<xs:simpleType>
							<xs:restriction base="xs:string">
								<xs:maxLen');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, 'gth value="60"/>
								<xs:minLength value="1"/>
							</xs:restriction>
						</xs:simpleType>
					</xs:attribute>
					<xs:attribute name="ИННФЛ" type="ИННФЛТип" use="required">
						<xs:annotation>
							<xs:documentation>ИНН физического лица</xs:documentation>
						</xs:annotation>
					</xs:attribute>
				</xs:complexType>
			</xs:element>
		</xs:choice>
	</xs:complexType>
	<xs:complexType name="СвНплТип">
		<xs:annotation>
			<xs:documentation>Сведения об участнике сделки</xs:documentation>
		</xs:annotation>
		<xs:choice>
			<xs:element name="СведЮЛ">
				<xs:annotation>
					<xs:documentation>Сведения об организации</xs:documentation>
				</xs:annotation>
				<xs:complexType>
					<xs:attribute name="ИННЮЛ" type="ИННЮЛТип" use="required">
						<xs:annotation>
							<xs:documentation>ИНН организации</xs:documentation>
						</xs:annotation>
					</xs:attribute>
				</xs:complexType>
			</xs:element>
			<xs:element name="СведИП">
				<xs:annotation>
					<xs:doc');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, 'umentation>Сведения об индивидуальном предпринимателе</xs:documentation>
				</xs:annotation>
				<xs:complexType>
					<xs:attribute name="ИННФЛ" type="ИННФЛТип" use="required">
						<xs:annotation>
							<xs:documentation>ИНН физического лица</xs:documentation>
						</xs:annotation>
					</xs:attribute>
				</xs:complexType>
			</xs:element>
		</xs:choice>
	</xs:complexType>
	<xs:simpleType name="КодВидОперТип">
		<xs:restriction base="xs:string">
			<xs:length value="2"/>
			<xs:enumeration value="01"/>
			<xs:enumeration value="02"/>
			<xs:enumeration value="03"/>
			<xs:enumeration value="04"/>
			<xs:enumeration value="05"/>
			<xs:enumeration value="06"/>
			<xs:enumeration value="07"/>
			<xs:enumeration value="08"/>
			<xs:enumeration value="09"/>
			<xs:enumeration value="10"/>
			<xs:enumeration value="11"/>
			<xs:enumeration value="12"/>
			<xs:enumeration value="13"/>
			<xs:enumeration value="14"/>
			<xs:enumeration value="15"/>
			<xs:enumeration value="16"/>');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, '
			<xs:enumeration value="17"/>
			<xs:enumeration value="18"/>
			<xs:enumeration value="19"/>
			<xs:enumeration value="20"/>
			<xs:enumeration value="21"/>
			<xs:enumeration value="22"/>
			<xs:enumeration value="23"/>
			<xs:enumeration value="24"/>
			<xs:enumeration value="25"/>
			<xs:enumeration value="26"/>
			<xs:enumeration value="27"/>
			<xs:enumeration value="28"/>
			<xs:enumeration value="99"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="СпрКодОшГрафТип">
		<xs:restriction base="xs:string">
			<xs:minLength value="1"/>
			<xs:maxLength value="2"/>
			<xs:enumeration value="1"/>
			<xs:enumeration value="2"/>
			<xs:enumeration value="3"/>
			<xs:enumeration value="4"/>
			<xs:enumeration value="5"/>
			<xs:enumeration value="6"/>
			<xs:enumeration value="7"/>
			<xs:enumeration value="8"/>
			<xs:enumeration value="9"/>
			<xs:enumeration value="10"/>
			<xs:enumeration value="11"/>
			<xs:enumeration value="12"/>
			<xs:enumeration value="13');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, '"/>
			<xs:enumeration value="14"/>
			<xs:enumeration value="15"/>
			<xs:enumeration value="16"/>
			<xs:enumeration value="17"/>
			<xs:enumeration value="18"/>
			<xs:enumeration value="19"/>
			<xs:enumeration value="20"/>
			<xs:enumeration value="21"/>
			<xs:enumeration value="22"/>
			<xs:enumeration value="23"/>
			<xs:enumeration value="24"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="СпрКодОшТипТ1">
		<xs:restriction base="xs:string">
			<xs:enumeration value="1"/>
			<xs:enumeration value="2"/>
			<xs:enumeration value="4"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="СпрКодОшТипТ2">
		<xs:restriction base="xs:string">
			<xs:enumeration value="2"/>
			<xs:enumeration value="4"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="СпрКодОшТипТ3">
		<xs:restriction base="xs:string">
			<xs:enumeration value="3"/>
			<xs:enumeration value="4"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="СпрКодОшТипТ4">
		<xs');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, ':restriction base="xs:string">
			<xs:enumeration value="1"/>
			<xs:enumeration value="3"/>
			<xs:enumeration value="4"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="СпрКодОшТипТ5">
		<xs:restriction base="xs:string">
			<xs:enumeration value="4"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="СпрКодОшТипТ8">
		<xs:restriction base="xs:string">
			<xs:enumeration value="1"/>
			<xs:enumeration value="2"/>
			<xs:enumeration value="4"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="СпрКодОшТипТ9">
		<xs:restriction base="xs:string">
			<xs:enumeration value="2"/>
			<xs:enumeration value="4"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:complexType name="СвКАгент">
		<xs:annotation>
			<xs:documentation>Сведения об участнике сделки</xs:documentation>
		</xs:annotation>
		<xs:choice>
			<xs:element name="СведЮЛ">
				<xs:annotation>
					<xs:documentation>Сведения об организации</xs:documentation>
				</xs:annotation>
				<xs:complexT');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, 'ype>
					<xs:attribute name="ИННЮЛ" type="ИННЮЛТип" use="required">
						<xs:annotation>
							<xs:documentation>ИНН организации</xs:documentation>
						</xs:annotation>
					</xs:attribute>
					<xs:attribute name="КПП" type="КППТип" use="required">
						<xs:annotation>
							<xs:documentation>КПП</xs:documentation>
						</xs:annotation>
					</xs:attribute>
				</xs:complexType>
			</xs:element>
			<xs:element name="СведИП">
				<xs:annotation>
					<xs:documentation>Сведения об индивидуальном предпринимателе</xs:documentation>
				</xs:annotation>
				<xs:complexType>
					<xs:attribute name="ИННФЛ" type="ИННФЛТип" use="required">
						<xs:annotation>
							<xs:documentation>ИНН физического лица</xs:documentation>
						</xs:annotation>
					</xs:attribute>
				</xs:complexType>
			</xs:element>
		</xs:choice>
		<xs:attribute name="НаимКАгент" use="required">
			<xs:annotation>
				<xs:documentation>Наименование (ФИО) контрагента</xs:documentation>
			</xs:annotation>');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, '
			<xs:simpleType>
				<xs:restriction base="xs:string">
					<xs:minLength value="1"/>
					<xs:maxLength value="1000"/>
				</xs:restriction>
			</xs:simpleType>
		</xs:attribute>
	</xs:complexType>
	<xs:complexType name="ФИОТип">
		<xs:annotation>
			<xs:documentation>Фамилия, имя, отчество</xs:documentation>
		</xs:annotation>
		<xs:attribute name="Фамилия" use="required">
			<xs:annotation>
				<xs:documentation>Фамилия</xs:documentation>
			</xs:annotation>
			<xs:simpleType>
				<xs:restriction base="xs:string">
					<xs:minLength value="1"/>
					<xs:maxLength value="60"/>
				</xs:restriction>
			</xs:simpleType>
		</xs:attribute>
		<xs:attribute name="Имя" use="required">
			<xs:annotation>
				<xs:documentation>Имя</xs:documentation>
			</xs:annotation>
			<xs:simpleType>
				<xs:restriction base="xs:string">
					<xs:minLength value="1"/>
					<xs:maxLength value="60"/>
				</xs:restriction>
			</xs:simpleType>
		</xs:attribute>
		<xs:attribute name="Отчество" us');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, 'e="optional">
			<xs:annotation>
				<xs:documentation>Отчество</xs:documentation>
			</xs:annotation>
			<xs:simpleType>
				<xs:restriction base="xs:string">
					<xs:minLength value="1"/>
					<xs:maxLength value="60"/>
				</xs:restriction>
			</xs:simpleType>
		</xs:attribute>
	</xs:complexType>
	<xs:simpleType name="ИННФЛТип">
		<xs:annotation>
			<xs:documentation>Идентификационный номер налогоплательщика - физического лица</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:string">
			<xs:length value="12"/>
			<xs:pattern value="([0-9]{1}[1-9]{1}|[1-9]{1}[0-9]{1})[0-9]{10}"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="ИННЮЛТип">
		<xs:annotation>
			<xs:documentation>Идентификационный номер налогоплательщика - организации</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:string">
			<xs:length value="10"/>
			<xs:pattern value="([0-9]{1}[1-9]{1}|[1-9]{1}[0-9]{1})[0-9]{8}"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleTyp');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, 'e name="КППТип">
		<xs:annotation>
			<xs:documentation>Код причины постановки на учет (КПП) - 5 и 6 знаки от 0-9 и A-Z</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:string">
			<xs:length value="9"/>
			<xs:pattern value="([0-9]{1}[1-9]{1}|[1-9]{1}[0-9]{1})([0-9]{2})([0-9A-Z]{2})([0-9]{3})"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="СОНОТип">
		<xs:annotation>
			<xs:documentation>Коды из Классификатора системы обозначений налоговых органов</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:string">
			<xs:length value="4"/>
			<xs:pattern value="[0-9]{4}"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="ОКВТип">
		<xs:annotation>
			<xs:documentation>Код из Общероссийского классификатора валют</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:string">
			<xs:length value="3"/>
			<xs:pattern value="[0-9]{3}"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="ДатаТип">
		<xs:annotation>
			');
dbms_output.put_line(dbms_lob.getlength(c1));
dbms_lob.append(c1, '<xs:documentation>Дата в формате ДД.ММ.ГГГГ (01.01.1900 - 31.12.2099)</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:string">
			<xs:length value="10"/>
			<xs:pattern value="((((0[1-9]{1}|1[0-9]{1}|2[0-8]{1})\.(0[1-9]{1}|1[0-2]{1}))|((29|30)\.(01|0[3-9]{1}|1[0-2]{1}))|(31\.(01|03|05|07|08|10|12)))\.((19|20)[0-9]{2}))|(29\.02\.((19|20)(((0|2|4|6|8)(0|4|8))|((1|3|5|7|9)(2|6)))))"/>
		</xs:restriction>
	</xs:simpleType>
</xs:schema>
');
dbms_output.put_line(dbms_lob.getlength(c1));

DBMS_XMLSCHEMA.registerSchema
(
	SCHEMAURL => 'TAX3EXCH_NDS2_CAM_01_01.xsd',
	SCHEMADOC => C1,
	genTypes => false,
	genTables => false,
	local => true,
	owner => 'NDS2_MRR_USER'
);
exception when others then null;
END;
/
