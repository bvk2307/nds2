﻿BEGIN
DBMS_XMLSCHEMA.registerSchema(
    SCHEMAURL => 'TAX3EXCH_NDS2_CAM_04_01.xsd',
    SCHEMADOC => '<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:sql="urn:schemas-microsoft-com:mapping-schema" elementFormDefault="qualified" attributeFormDefault="unqualified">
	<xs:element name="ДанныеРсхжд">
		<xs:annotation>
			<xs:documentation>Данные об наличии расхождений</xs:documentation>
		</xs:annotation>
		<xs:complexType>
			<xs:attribute name="КодНО" type="СОНОТип" use="required"/>
			<xs:attribute name="РегНомДек" use="required">
				<xs:annotation>
					<xs:documentation>Регистрационный номер декларации</xs:documentation>
				</xs:annotation>
				<xs:simpleType>
					<xs:restriction base="xs:int">
						<xs:totalDigits value="13"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:attribute>
			<xs:attribute name="СвНалРсхжд" use="required">
				<xs:annotation>
					<xs:documentation>Сведения о наличии расхождений </xs:documentation>
				</xs:annotation>
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:length value="1"/>
						<xs:enumeration value="2"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:attribute>
			<xs:attribute name="ТипИнф" use="required">
				<xs:annotation>
					<xs:documentation>Тип информации</xs:documentation>
				</xs:annotation>
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:enumeration value="TAX3EXCH_NDS2_CAM_04"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:attribute>
		</xs:complexType>
	</xs:element>
	<xs:simpleType name="СОНОТип">
		<xs:annotation>
			<xs:documentation>Коды из Классификатора системы обозначений налоговых органов</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:string">
			<xs:length value="4"/>
			<xs:pattern value="[0-9]{4}"/>
		</xs:restriction>
	</xs:simpleType>
</xs:schema>',
     genTypes => false, 
     genTables => false, 
     local => true,
	 owner => 'NDS2_MRR_USER'
    );
exception when others then null;
END;
/
