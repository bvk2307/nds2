﻿drop materialized view NDS2_MRR_USER.MV$TAX_PAYER_NAME;
create materialized view NDS2_MRR_USER.MV$TAX_PAYER_NAME
build deferred
refresh force on demand
as
select
trim(ip.innfl) as INN,
'' as kpp,
nvl(ip.last_name, '')||' '||nvl(ip.first_name, '')||' '||nvl(ip.patronymic, '') as NP_NAME,
0 as TP_TYPE
from v_egrn_ip ip
union all
select
trim(ul.inn) as inn,
trim(ul.kpp) as kpp,
ul.name_full as NP_NAME,
1 as TP_TYPE
from
v_egrn_ul ul;
