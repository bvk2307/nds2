drop materialized view NDS2_MRR_USER.MV$Declaration_history;
create materialized view NDS2_MRR_USER.MV$Declaration_history 
nologging
build DEFERRED 
as
select
to_number(sed.tax_period||sed.fiscal_year||sed.inn||sed.correction_number) as nds2_decl_corr_id
,sed.nds2_id																				as ID
,sed.decl_reg_num																		as SEOD_DECL_ID
,SD.ID																					as ASK_DECL_ID
,SD.ZIP																					as DECLARATION_VERSION_ID
,decode((row_number() 
over (partition by sed.TAX_PERIOD, 
	sed.FISCAL_YEAR, 
	sed.INN 
	order by SED.CORRECTION_NUMBER desc, SED.CORRECTION_NUMBER_RANK)),1,1,0)										as IS_ACTIVE
,case when D.DECLARATION_VERSION_ID is not null 
		then 2 else (case when  SD.ID is not null then 1 else 0 end) 
end																						as ProcessingStage
    ,MAX(case when D.DECLARATION_VERSION_ID is not null then 2 else (case when  SD.ID is not null then 1 else 0 end) end)
       OVER(PARTITION BY sed.inn, sed.tax_period, sed.fiscal_year) as max_processing_stage
,d.UPDATE_DATE																			as UPDATE_DATE
,sed.INN																				as INN
,sed.KPP																				as KPP
,vtp.NAME																				as NAME
,vtp.ADDRESS																			as ADDRESS1
,'-'																						as ADDRESS2
,ssrf.s_code																			as REGION_CODE
,ssrf.s_name																			as REGION
,ssrf.s_code || '-' || ssrf.s_name														as REGION_NAME
,soun.s_code																			as SOUN_CODE
,soun.s_name																			as SOUN
,soun.s_code || '-' || soun.s_name														as SOUN_NAME
,DECODE(sed.TYPE, 0, vtp.CATEGORY, null)												as CATEGORY
,DECODE(sed.TYPE, 0, case vtp.CATEGORY when 1 then '����������' else '' end, null)				as CATEGORY_RU
,sed.EOD_DATE																			as REG_DATE
,'-'																						as TAX_MODE
,sd.OKVED																				as OKVED_CODE
,'0'																					as CAPITAL
,sed.TAX_PERIOD																			as TAX_PERIOD
,sed.FISCAL_YEAR																		as FISCAL_YEAR
,dtp.description || ' ' || sed.FISCAL_YEAR												as FULL_TAX_PERIOD 
,DECODE(sed.TYPE, 0, sed.EOD_DATE, 1, sed.date_receipt, null)							as DECL_DATE
,DECODE(sed.TYPE, 0, '����������', 1, '������', '')										as DECL_TYPE
,sed.TYPE																				as DECL_TYPE_CODE
,sed.CORRECTION_NUMBER																	as CORRECTION_NUMBER
,sed.CORRECTION_NUMBER_RANK																as CORRECTION_NUMBER_RANK
,decode(sd.Nd_Prizn, 1, '� ������', 2, '� ����������', 3, '�������', '')				as DECL_SIGN
,sd.SUMMANDS																			as COMPENSATION_AMNT
,DECODE(sed.TYPE, 0, '0', null)															as COMPENSATION_AMNT_SIGN
,d.LK_ERRORS_COUNT																		as LK_ERRORS_COUNT
,d.CH8_DEALS_AMNT_TOTAL																	as CH8_DEALS_AMNT_TOTAL
,d.CH9_DEALS_AMNT_TOTAL
,d.CH8_NDS
,d.CH9_NDS
,d.DISCREP_CURRENCY_AMNT
,d.DISCREP_CURRENCY_COUNT
,d.GAP_DISCREP_COUNT
,d.GAP_DISCREP_AMNT
,d.WEAK_DISCREP_COUNT
,d.WEAK_DISCREP_AMNT
,d.NDS_INCREASE_DISCREP_COUNT
,d.NDS_INCREASE_DISCREP_AMNT
,d.GAP_MULTIPLE_DISCREP_COUNT
,d.GAP_MULTIPLE_DISCREP_AMNT
,(nvl(d.DISCREP_CURRENCY_COUNT, 0) 
	+ nvl(d.GAP_DISCREP_COUNT, 0) 
	+ nvl(d.WEAK_DISCREP_COUNT, 0) 
	+ nvl(d.NDS_INCREASE_DISCREP_COUNT, 0) 
	+ nvl(d.GAP_MULTIPLE_DISCREP_COUNT, 0))												as TOTAL_DISCREP_COUNT
,DECODE(sed.TYPE, 0, d.DISCREP_BUY_BOOK_AMNT, null)										as DISCREP_BUY_BOOK_AMNT
,DECODE(sed.TYPE, 0, d.DISCREP_SELL_BOOK_AMNT, null)									as DISCREP_SELL_BOOK_AMNT
,d.DISCREP_MIN_AMNT
,d.DISCREP_MAX_AMNT
,d.DISCREP_AVG_AMNT
,d.DISCREP_TOTAL_AMNT
,DECODE(sed.TYPE, 0, d.PVP_TOTAL_AMNT, null)											as PVP_TOTAL_AMNT
,DECODE(sed.TYPE, 0, d.PVP_DISCREP_MIN_AMNT, null)										as PVP_DISCREP_MIN_AMNT
,DECODE(sed.TYPE, 0, d.PVP_DISCREP_MAX_AMNT, null)										as PVP_DISCREP_MAX_AMNT
,DECODE(sed.TYPE, 0, d.PVP_DISCREP_AVG_AMNT, null)										as PVP_DISCREP_AVG_AMNT
,DECODE(sed.TYPE, 0, d.PVP_BUY_BOOK_AMNT, null)											as PVP_BUY_BOOK_AMNT
,DECODE(sed.TYPE, 0, d.PVP_SELL_BOOK_AMNT, null)										as PVP_SELL_BOOK_AMNT
,d.PVP_RECIEVE_JOURNAL_AMNT
,d.PVP_SENT_JOURNAL_AMNT
,nvl(sur.code, 4)																		as SUR_CODE
,sur.description																		as SUR_DESCRIPTION
,d.CONTROL_RATIO_DATE
,d.CONTROL_RATIO_SEND_TO_SEOD
,DECODE(sed.TYPE, 0, nvl(KSAggr.KSCount, 0), null)										as CONTROL_RATIO_COUNT
,knp.completion_date																	as DateCloseKNP
,DECODE(sed.TYPE, 0, 
  case
      when (knp.knp_id IS NOT NULL and knp.completion_date is not null) then '�������'
      else '�������'
  end, null)	
  																		as STATUS
-- invoice count
,dsd.invoice_count8
,dsd.invoice_count81
,dsd.invoice_count9
,dsd.invoice_count91
,dsd.invoice_count10
,dsd.invoice_count11
,dsd.invoice_count12
,dsd.actual_zip8
,dsd.actual_zip81
,dsd.actual_zip9
,dsd.actual_zip91
,dsd.actual_zip10
,dsd.actual_zip11
,dsd.actual_zip12
-- Chapter8 and 8.1
,dsd.SumNDSPok_8
,dsd.SumNDSPok_81
,dsd.SumNDSPokDL_81
-- Begin Chapter 9 and Chapter 9.1
,dsd.StProd18_9
,dsd.StProd18_91
,dsd.StProd18DL_91
,dsd.StProd10_9
,dsd.StProd10_91
,dsd.StProd10DL_91
,dsd.StProd0_9
,dsd.StProd0_91
,dsd.StProd0DL_91
,dsd.SumNDSProd18_9
,dsd.SumNDSProd18_91
,dsd.SumNDSProd18DL_91
,dsd.SumNDSProd10_9
,dsd.SumNDSProd10_91
,dsd.SumNDSProd10DL_91
,dsd.StProdOsv_9
,dsd.StProdOsv_91
,dsd.StProdOsvDL_91
-- End Chapter 9 and Chapter 9.1
,dsd.AKT_NOMKORR_8
,dsd.AKT_NOMKORR_81
,dsd.AKT_NOMKORR_9
,dsd.AKT_NOMKORR_91
,dsd.AKT_NOMKORR_10
,dsd.AKT_NOMKORR_11
,dsd.AKT_NOMKORR_12
,dsd.StProd18
,dsd.StProd10
,dsd.StProd0
,dsd.SumNDSProd18
,dsd.SumNDSProd10
,dsd.StProd
,dsd.StProdOsv
,dsd.StProd18DL
,dsd.StProd10DL
,dsd.StProd0DL
,dsd.SumNDSProd18DL
,dsd.SumNDSProd10DL
,dsd.StProdOsvDL
-- ��� ������������, ����� ����� �� ������� ���������
,'-' AS RECORD_MARK                                            --������� ������ [�, �, �-�, �, ���, NULL]
,NDS_INCREASE_DISCREP_COUNT*3 AS BUYBOOK_CONTRAGENT_CNT        --����� �������: ���-�� ������������
,NDS_INCREASE_DISCREP_COUNT*2 AS BUYBOOK_DK_GAP_CNT            --����� �������: ��� ����������� - ������: ���-�� ��������
,NDS_INCREASE_DISCREP_COUNT   AS BUYBOOK_DK_GAP_CAGNT_CNT    --����� �������: ��� ����������� - ������: ���-�� ������������
,NDS_INCREASE_DISCREP_COUNT*4 AS BUYBOOK_DK_GAP_AMNT        --����� �������: ��� ����������� - ������: ����� ��������, ���.
,NDS_INCREASE_DISCREP_COUNT   AS BUYBOOK_DK_OTHR_CNT        --����� �������: ��� ����������� - ������: ���-�� ����������� [���������� ����������� ���� ��������� ������������� � ��������� ��ѻ � �������������-����������]
,NDS_INCREASE_DISCREP_COUNT*2 AS BUYBOOK_DK_OTHR_CAGNT_CNT    --����� �������: ��� ����������� - ������: ���-�� ������������
,NDS_INCREASE_DISCREP_COUNT*5 AS BUYBOOK_DK_OTHR_AMNT        --����� �������: ��� ����������� - ������: ����� �����������, ���.
--
from
(
   select (row_number() over (partition by sed.TAX_PERIOD, sed.FISCAL_YEAR,sed.INN,SED.CORRECTION_NUMBER order by SED.ID)) as CORRECTION_NUMBER_RANK
          ,sed.* from seod_declaration sed
) sed
inner join v$sono soun on soun.s_code = sed.sono_code
inner join v$ssrf ssrf on ssrf.s_code = substr(sed.sono_code,1,2)
inner join dict_tax_period dtp on dtp.code = sed.tax_period
left join V$ASK_DECLANDJRNL sd on SD.PERIOD = SED.TAX_PERIOD and SD.OTCHETGOD = SED.FISCAL_YEAR and SD.INNNP = SED.INN and SD.NOMKORR = SED.CORRECTION_NUMBER  
								AND sd.NOMKORR_RANK = sed.CORRECTION_NUMBER_RANK
left join SOV_DECLARATION_INFO d on D.DECLARATION_VERSION_ID = SD.ZIP
left join (select ksotn.IdDekl as decl_id, count(1) as KSCount from v$askkontrsoontosh ksotn where ksotn.Vypoln = 0 group by ksotn.IdDekl )  KSAggr on KSAggr.decl_id = sd.ID
left join seod_knp knp on knp.declaration_reg_num = sed.decl_reg_num and knp.ifns_code = sed.sono_code
left join v$taxpayer vtp on vtp.inn = sed.INN
left join EXT_SUR esur on esur.inn = sed.INN and sed.TAX_PERIOD = esur.fiscal_period and sed.FISCAL_YEAR = esur.fiscal_year
left join DICT_SUR sur on sur.code = esur.sign_code
left join
(
  select
    src.ZIP as Declaration_Version_Id,
    max(case when src.TipFajla = 0 then nvl(act.zip, src.zip) end) as actual_zip8,
    max(case when src.TipFajla = 2 then nvl(act.zip, src.zip) end) as actual_zip81,
    max(case when src.TipFajla = 1 then nvl(act.zip, src.zip) end) as actual_zip9,
    max(case when src.TipFajla = 3 then nvl(act.zip, src.zip) end) as actual_zip91,
    max(case when src.TipFajla = 4 then nvl(act.zip, src.zip) end) as actual_zip10,
    max(case when src.TipFajla = 5 then nvl(act.zip, src.zip) end) as actual_zip11,
    max(case when src.TipFajla = 6 then nvl(act.zip, src.zip) end) as actual_zip12,
    --Incoice count
    nvl(sum(case when src.TipFajla = 0 then nvl(act.KolZapisey, nvl(src.KolZapisey, 0)) end), 0) as invoice_count8,
    nvl(sum(case when src.TipFajla = 2 then nvl(act.KolZapisey, nvl(src.KolZapisey, 0)) end), 0) as invoice_count81,
    nvl(sum(case when src.TipFajla = 1 then nvl(act.KolZapisey, nvl(src.KolZapisey, 0)) end), 0) as invoice_count9,
    nvl(sum(case when src.TipFajla = 3 then nvl(act.KolZapisey, nvl(src.KolZapisey, 0)) end), 0) as invoice_count91,
    nvl(sum(case when src.TipFajla = 4 then nvl(act.KolZapisey, nvl(src.KolZapisey, 0)) end), 0) as invoice_count10,
    nvl(sum(case when src.TipFajla = 5 then nvl(act.KolZapisey, nvl(src.KolZapisey, 0)) end), 0) as invoice_count11,
    nvl(sum(case when src.TipFajla = 6 then nvl(act.KolZapisey, nvl(src.KolZapisey, 0)) end), 0) as invoice_count12,
    --Chapter 8 and 8.1
    sum(case when src.TipFajla = 0 then nvl(act.SUMNDSPOK, nvl(src.SUMNDSPOK, 0)) end) as SumNDSPok_8, 
    sum(case when src.TipFajla = 2 then nvl(act.SUMNDSPOK, nvl(src.SUMNDSPOK, 0)) end) as SumNDSPok_81,
    sum(case when src.TipFajla = 2 then nvl(act.SUMNDSPOKDL, nvl(src.SUMNDSPOKDL, 0)) end) as SumNDSPokDL_81,
    -- Begin Chapter 9 and Chapter 9.1
    sum(case when src.TipFajla = 1 then nvl(act.STPROD18, nvl(src.STPROD18, 0)) end) as StProd18_9,
    sum(case when src.TipFajla = 3 then nvl(act.STPROD18, nvl(src.STPROD18, 0)) end) as StProd18_91,
    sum(case when src.TipFajla = 3 then nvl(act.STPROD18DL, nvl(src.STPROD18DL, 0)) end) as StProd18DL_91,
    sum(case when src.TipFajla = 1 then nvl(act.STPROD10, nvl(src.STPROD10, 0)) end) as StProd10_9,
    sum(case when src.TipFajla = 3 then nvl(act.STPROD10, nvl(src.STPROD10, 0)) end) as StProd10_91,
    sum(case when src.TipFajla = 3 then nvl(act.STPROD10DL, nvl(src.STPROD10DL, 0)) end) as StProd10DL_91,
    sum(case when src.TipFajla = 1 then nvl(act.STPROD0, nvl(src.STPROD0, 0)) end) as StProd0_9,
    sum(case when src.TipFajla = 3 then nvl(act.STPROD0, nvl(src.STPROD0, 0)) end) as StProd0_91,
    sum(case when src.TipFajla = 3 then nvl(act.STPROD0DL, nvl(src.STPROD10DL, 0)) end) as StProd0DL_91,
    sum(case when src.TipFajla = 1 then nvl(act.SUMNDSPROD18, nvl(src.SUMNDSPROD18, 0)) end) as SumNDSProd18_9,
    sum(case when src.TipFajla = 3 then nvl(act.SUMNDSPROD18, nvl(src.SUMNDSPROD18, 0)) end) as SumNDSProd18_91,
    sum(case when src.TipFajla = 3 then nvl(act.SUMNDS18DL, nvl(src.SUMNDS18DL, 0)) end) as SumNDSProd18DL_91,
    sum(case when src.TipFajla = 1 then nvl(act.SUMNDSPROD10, nvl(src.SUMNDSPROD10, 0)) end) as SumNDSProd10_9,
    sum(case when src.TipFajla = 3 then nvl(act.SUMNDSPROD10, nvl(src.SUMNDSPROD10, 0)) end) as SumNDSProd10_91,
    sum(case when src.TipFajla = 3 then nvl(act.SUMNDS10DL, nvl(src.SUMNDS10DL, 0)) end) as SumNDSProd10DL_91,
    sum(case when src.TipFajla = 1 then nvl(act.STPRODOSV, nvl(src.STPRODOSV, 0)) end) as StProdOsv_9,
    sum(case when src.TipFajla = 3 then nvl(act.STPRODOSV, nvl(src.STPRODOSV, 0)) end) as StProdOsv_91,
    sum(case when src.TipFajla = 3 then nvl(act.STPRODOSVDL, nvl(src.STPRODOSVDL, 0)) end) as StProdOsvDL_91,
    -- End Chapter 9 and Chapter 9.1
    max(case when src.TipFajla = 0 then nvl(act.NOMKORR, nvl(src.NOMKORR, 0)) end) as AKT_NOMKORR_8,
    max(case when src.TipFajla = 2 then nvl(act.NOMKORR, nvl(src.NOMKORR, 0)) end) as AKT_NOMKORR_81,
    max(case when src.TipFajla = 1 then nvl(act.NOMKORR, nvl(src.NOMKORR, 0)) end) as AKT_NOMKORR_9,
    max(case when src.TipFajla = 3 then nvl(act.NOMKORR, nvl(src.NOMKORR, 0)) end) as AKT_NOMKORR_91,
    max(case when src.TipFajla = 4 then nvl(act.NOMKORR, nvl(src.NOMKORR, 0)) end) as AKT_NOMKORR_10,
    max(case when src.TipFajla = 5 then nvl(act.NOMKORR, nvl(src.NOMKORR, 0)) end) as AKT_NOMKORR_11,
    max(case when src.TipFajla = 6 then nvl(act.NOMKORR, nvl(src.NOMKORR, 0)) end) as AKT_NOMKORR_12,
    sum(src.STPROD18) as StProd18,
    sum(src.STPROD10) as StProd10,
    sum(src.STPROD0) as StProd0,
    sum(src.SUMNDSPROD18) as SumNDSProd18,
    sum(src.SUMNDSPROD10) as SumNDSProd10,
    sum(src.STPROD) as StProd,
    sum(src.STPRODOSV) as StProdOsv,
    sum(src.STPROD18DL) as StProd18DL,
    sum(src.STPROD10DL) as StProd10DL,
    sum(src.STPROD0DL) as StProd0DL,
    sum(src.SUMNDS18DL) as SumNDSProd18DL,
    sum(src.SUMNDS10DL) as SumNDSProd10DL,
    sum(src.STPRODOSVDL) as StProdOsvDL,
    nvl(SUM(case when src.TipFajla in ('0', '2', '1', '3', '6') then nvl(src.KolZapisey, 0) end), 0) as COUNT_INVOICE_R08_09_12
  from v$asksvodzap src
  left join v$asksvodzap act on act.id = src.IdAkt
  group by src.ZIP
) dsd on dsd.declaration_version_id = sd.zip;

create index NDS2_MRR_USER.IDX_MVDECL_ID on NDS2_MRR_USER.MV$DECLARATION_HISTORY (ID);
