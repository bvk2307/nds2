﻿drop materialized view NDS2_MRR_USER.MV$INSPECTOR_DECLARATION;
create materialized view NDS2_MRR_USER.MV$INSPECTOR_DECLARATION
build DEFERRED 
refresh force on demand
as
select
     to_number(seod.tax_period||seod.fiscal_year||seod.inn||seod.correction_number) as nds2_decl_corr_id
	,seod.IS_ACTIVE
    ,seod.nds2_id as ID
    ,sd.zip as declaration_version_id
    ,seod.TYPE as DECL_TYPE_CODE
    ,DECODE(seod.TYPE, 0, 'Декларация', 1, 'Журнал', '') as DECL_TYPE
    ,case when sovd.DECLARATION_VERSION_ID is not null then 2 else (case when  sd.ID is not null then 1 else 0 end) end as ProcessingStage
    ,MAX(case when sovd.DECLARATION_VERSION_ID is not null then 2 else (case when  sd.ID is not null then 1 else 0 end) end)
       OVER(PARTITION BY seod.inn, seod.tax_period, seod.fiscal_year) as max_processing_stage
    ,case when seod.TYPE=0 and (knp.knp_id IS NULL or knp.completion_date is null) then 'Открыта'
          when seod.TYPE=0 and (knp.knp_id IS NOT NULL and knp.completion_date is not null) then 'Закрыта'
          else NULL
       end as STATUS_KNP
    ,nvl(esur.sign_code, 4) as SUR_CODE
    ,seod.inn as INN
    ,seod.kpp as KPP
    ,vtp.NAME
    ,case
       when seod.TYPE=0 then seod.decl_reg_num
       else null end as SEOD_DECL_ID
    ,seod.TAX_PERIOD
    ,seod.FISCAL_YEAR
    ,dtp.description||' '||seod.FISCAL_YEAR as FULL_TAX_PERIOD
    ,case when seod.TYPE=0 then nvl(sd.DATADOK, seod.EOD_DATE) else null end as DECL_DATE
    ,case
       when seod.TYPE=0 then seod.CORRECTION_NUMBER
       else null end as CORRECTION_NUMBER
	,seod.CORRECTION_NUMBER_RANK
    ,decode(sd.Nd_Prizn, 1, 'К уплате', 2, 'К возмещению', 3, 'Нулевая', '')   as COMPENSATION_AMNT_SIGN
    ,sovd.ch8_deals_cnt_total
    ,sovd.ch9_deals_cnt_total
    ,sovd.ch10_deals_cnt_total
    ,sovd.ch11_deals_cnt_total
    ,sovd.ch12_deals_cnt_total
    ,sovd.ch8_deals_cnt_total + sovd.ch9_deals_cnt_total + sovd.ch10_deals_cnt_total + sovd.ch11_deals_cnt_total + sovd.ch12_deals_cnt_total as deals_cnt_total
    ,dsd.NDS_AMOUNT
    ,seod.SONO_CODE as SOUN_CODE
    ,substr(sd.KODNO,1,2) as REGION_CODE
from
(
   select 
		(row_number() over (partition by seod.TAX_PERIOD, seod.FISCAL_YEAR, seod.INN, seod.CORRECTION_NUMBER order by seod.ID)) as CORRECTION_NUMBER_RANK
		,decode((row_number() over (partition by seod.TAX_PERIOD, seod.FISCAL_YEAR, seod.INN order by seod.CORRECTION_NUMBER desc, seod.ID)),1,1,0) as IS_ACTIVE
		,seod.*
   from seod_declaration seod
) seod
inner join DICT_TAX_PERIOD dtp on dtp.code = seod.TAX_PERIOD
left join V$ASK_DECLANDJRNL sd on SD.PERIOD = seod.TAX_PERIOD and SD.OTCHETGOD = seod.FISCAL_YEAR and SD.INNNP = seod.INN and SD.NOMKORR = seod.CORRECTION_NUMBER
								AND sd.NOMKORR_RANK = seod.CORRECTION_NUMBER_RANK
left join seod_knp knp on knp.declaration_reg_num = seod.decl_reg_num and knp.ifns_code = seod.sono_code
left join sov_declaration_info sovd on SOVD.DECLARATION_VERSION_ID = SD.ZIP
left join v$taxpayer vtp on vtp.inn = seod.inn
left join EXT_SUR esur on esur.inn = seod.INN and seod.TAX_PERIOD = esur.fiscal_period and seod.FISCAL_YEAR = esur.fiscal_year
left join
(
  select
    ZIP,
    sum(case TipFajla when '0' then SumNDSPok when '2' then SumNDSPokDL else 0 end) as NDS_AMOUNT,
  nvl(SUM(case when TipFajla in ('0', '2', '1', '3', '6') then nvl(KolZapisey, 0) end), 0) as COUNT_INVOICE_R08_09_12
  from nds2_mrr_user.v$asksvodzap
  group by ZIP
) dsd on dsd.zip = sd.zip
where seod.IS_ACTIVE = 1;
