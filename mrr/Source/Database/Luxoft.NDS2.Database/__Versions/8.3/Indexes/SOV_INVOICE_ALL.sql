create index NDS2_MRR_USER.IDX_SIA_RAW_KEY on NDS2_MRR_USER.SOV_INVOICE_ALL (ROW_KEY) tablespace nds2_idx  compress;
create index NDS2_MRR_USER.IDX_SIA_ACTUAL_RAW_KEY on NDS2_MRR_USER.SOV_INVOICE_ALL (ACTUAL_ROW_KEY) tablespace nds2_idx  compress;
create index NDS2_MRR_USER.IDX_SISA_RAW_KEY_ID on NDS2_MRR_USER.SOV_INVOICE_SELLER_ALL (ROW_KEY_ID) tablespace nds2_idx  compress;
create index NDS2_MRR_USER.IDX_SISA_SELLER_INN on NDS2_MRR_USER.SOV_INVOICE_SELLER_ALL (SELLER_INN) tablespace nds2_idx  compress;
create index NDS2_MRR_USER.idx_siab_rid on NDS2_MRR_USER.SOV_INVOICE_BUYER_ALL(ROW_KEY_ID) tablespace nds2_idx compress;
create index NDS2_MRR_USER.idx_siabaa_rid on  NDS2_MRR_USER.SOV_INVOICE_BUY_ACCEPT_ALL (ROW_KEY_ID) tablespace nds2_idx compress;
create index NDS2_MRR_USER.idx_siaoa_rid on  NDS2_MRR_USER.SOV_INVOICE_OPERATION_ALL (ROW_KEY_ID) tablespace nds2_idx compress;
create index NDS2_MRR_USER.idx_siapd_rid on  NDS2_MRR_USER.SOV_INVOICE_PAYMENT_DOC_ALL (ROW_KEY_ID) tablespace nds2_idx compress;
