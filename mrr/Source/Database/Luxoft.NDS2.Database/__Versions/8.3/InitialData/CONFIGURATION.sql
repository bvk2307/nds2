﻿merge into NDS2_MRR_USER.CONFIGURATION c
    using (select 'seod_decl_key_last_processed' as parameter from dual) par
      on (par.parameter = c.parameter)
    when not matched then
      insert (parameter, value, default_value, description)
      values ('seod_decl_key_last_processed', to_char(sysdate - 1, 'DD/MM/YYYY'), to_char(sysdate - 1, 'DD/MM/YYYY'), 'Дата последнего запуска процесса создания ключей контрагента');