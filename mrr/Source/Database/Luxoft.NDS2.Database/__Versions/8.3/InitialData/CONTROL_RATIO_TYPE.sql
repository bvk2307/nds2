﻿truncate table NDS2_MRR_USER.control_ratio_type;

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description )
values ('1.1', 'НК РФ ст. 161, 171, 172, 174. Не обоснованное применение вычетов сумм НДС налоговым агентом. ',
'При условии (р. 2 ст. 070 = 1011711) или (р. 2 ст. 070 = 1011703). р.3.ст. 180 гр.3 онп. <= max (р.2. ст. 060 пнп. за последние 3 года) - максимальное значение за прошлые периоды в диапазоне 3-х лет) Проверка начинается с 3-ого квартала 2015 ',
'р.3.ст. 180 гр.3 онп.', 'max (р.2. ст. 060 пнп. за последние 3 года) - максимальное значение за прошлые периоды в диапазоне 3-х лет', '<=', 'Данные КС должны расчитываться АСК НДС2 только с 3го квартала 2015. Если в 3 кв.2015 года или последующих кварталах для расчета данного КС не будет данных по предыдущим периодам, то это должно считаться расхождением, а в СЭОД должны быть переданы два нуля.');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description )
values ('1.2', 'НК РФ ст. 161, 171, 172. Не обоснованное применение вычетов сумм НДС налоговым агентом. ',
'При условии, что в р. 2 ст. 070 за последние 3 года присутствуют только коды 1011705 и 1011707. р.3 ст.180 гр.3 онп <= 0 Проверка начинается с 3-ого квартала 2015 ',
'р.3 ст.180 гр.3 онп', '0', '<=', '');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description )
values ('1.3.1', 'НК РФ ст. 161, 173, 174. Занижение суммы налога к уплате в бюджет.',
'При условии, что (р.2 ст.070 = 1011705 или р.2 ст.070 = 1011707). р.2.ст.060 = р.2 (ст.080+ст.090-ст.100) онп',
'р.2.ст.060', 'р.2 (ст.080+ст.090-ст.100) онп ', '=', 'Данные КС должны расчитываться АСК НДС2 только с 3го квартала 2015. Если в 3 кв.2015 года или последующих кварталах для расчета данного КС не будет данных по предыдущим периодам, то это должно считаться расхождением, а в СЭОД должны быть переданы два нуля.');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description )
values ('1.3.2', 'НК РФ ст. 161, 173, 174. Занижение суммы налога к уплате в бюджет. ',
'При условии, что (р.2 ст.070 = 1011705 или р.2 ст.070 = 1011707) и (max(р.2 ст.090 пнп, р.2 ст.090 ппнп) > 0). р.2ст.060 = р.2 ст.080 - р.2 ст.100 Проверка начинается с 3-ого квартала 2015 ',
'р.2ст.060', 'р.2 ст.080 - р.2 ст.100', '=', 'Данные КС должны расчитываться АСК НДС2 только с 3го квартала 2015. Если в 3 кв.2015 года или последующих кварталах для расчета данного КС не будет данных по предыдущим периодам, то это должно считаться расхождением, а в СЭОД должны быть переданы два нуля.');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description )
values ('1.3.3', 'НК РФ ст. 161, 173, 174. Занижение суммы налога к уплате в бюджет. ',
'При условии, что (р.2 ст.070 = 1011705 или р.2 ст.070 = 1011707) и (р.2 ст.080 онп = 0 и р.2 ст.100 онп = 0). р.2 ст.060 = р.2 ст.090 онп ',
'р.2 ст.060', 'р.2 ст.090 онп', '=', '');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description )
values ('1.3.4', 'НК РФ ст. 161, 173, 174. Занижение суммы налога к уплате в бюджет. ',
'При условии, что (р.2 ст.070 = 1011705 или р.2 ст.070 = 1011707) и (р.2 ст.090 онп = 0 и р.2 ст.100 онп = 0). р.2ст.060 = р.2 ст.080 ',
'р.2ст.060', 'р.2 ст.080 ', '=', '');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description )
values ('1.4', 'НК РФ ст. 149, 170 п.4. Возможно не обоснованное применение налоговых вычетов.',
'При условии, что строки в знаменателе обеих частей неравенства > 0. (р.3. (ст.010 гр.3 + ст.020 гр.3 + ст.030 гр.3+ст.040 гр.3) / [р.3.(ст.010 гр.3 + ст.020 гр.3+ст.030 гр.3+ ст.040 гр.3) + (р.7 сумма гр.2 - сумма р.7 гр.2, где коды ( 1010800-1010821)]) = (р.3 ст.(120 + 150 + 160) гр.3 / [р.3.ст.(120 + 150 + 160) гр.3 + сумма р.7 гр.4 - сумма р.7 гр.4, где коды (1010800 - 1010821)]) ', 'р.3. (ст.010гр.3+020гр.3+ст.030гр.3+ст. 040гр.3) / [р.3.(ст.010гр.3+ст.020гр.3+ст.030гр.3+ ст. 040гр.3)+ (р.7 сумма гр.2 - сумма р.7 гр.2, где коды ( 1010800-1010821)]',
'р.3. ст.(120+150+160)гр.3 / [р.3.ст.(120+150+160)гр.3+ сумма р.7 гр.4 - сумма р.7 гр.4, где коды (1010800 - 1010821)]', '=', '');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description )
values ('1.5', 'НК РФ ст.170, 171. Занижение суммы подлежащей восстановлению и уплате в бюджет. ',
'При условии, что хотя бы одна из стр. р.3 или Прил.№1 р.3 заполнена. р.3 ст.080 гр.5 >= (р.3(ст.090 гр.5 + ст.100 гр.5)+ (сумма Прил.№1 р.3 ст.080 гр.4, где Прил.№1 р.3 ст.080 гр.4 = Отчетный год, указанный на ТЛ и Прил.№1 р.3 ст.030 =1011801, 1011802, 1011803, 1011805)) ',
'р.3 ст. 080 гр.5', 'р.3(ст.090 гр.5+ст.100 гр.5)+ (сумма Прил.№1 р.3 ст.080 гр.4, где Прил.№1 р.3 ст.080 гр.1= Отчетный год, указанный на ТЛ и Прил.№1 р.3 ст.030 =1011801, 1011802, 1011803, 1011805 )', '>=', '');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description )
values ('1.6', 'НК РФ ст.170, 171. Занижение суммы подлежащей восстановлению и уплате в бюджет. ',
'При условии, что хотя бы одна из стр. р.3 или Прил.№1 р.3 заполнена. р.3 ст.(080 - 090 - 100) гр.5 >= (Прил.№1 к р.3 дНДС суммы ст.080 гр.4 по кодам 1011801, 1011802, 1011803, 1011805 за отчетный год, указанный по ст.080 гр.1 и соответствующий отчетному году на ТЛ дНДС) ',
'р.3 ст.(080 - 090 - 100)гр.5', 'Прил.№1 к р.3 дНДС суммы ст.080 гр.4 по кодам 1011801, 1011802, 1011803, 1011805 за отчетный год, указанный по ст.080гр.1 и соответствующий отчетному году на ТЛ дНДС', '>=', '');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description )
values ('1.7', 'НК РФ ст. 146, 154, 155, 156, 158, 159, 162. Занижение суммы НДС вследствие неполного отражения налоговой базы по операциям, указанным в строках 010-080 и 105-109. ',
'При условии, что хотя бы одна из р.3 ст.(010 или 020 или 030 или 040 или 050 или 060 или 070 или 080 или 105 или 106 или 107 или 108 или 109) гр.5 заполнена. (р.3 ст.(010 + 020 + 030 + 040 + 050 + 060 + 070 + 080 + 105 + 106 + 107 + 108 + 109) гр.5) <= р.3 ст.110 гр.5 ',
'р.3 ст.(010+020+030+040+050+060+070+080+105+106+107+ 108 +109) гр.5', 'р.3.ст.110гр.5 ', '<=', '');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description )
values ('1.8', 'НК РФ ст. 171, 172. Необоснованные налоговые вычеты. ',
'При условии, что хотя бы одна строка из р.3 ст.(120, 130, 140, 150, 160, 170, 180) гр.3 заполнена. р.3. ст.190 гр.3 <= (р.3 ст.(120 + 130 + 140 + 150 + 160 + 170 + 180) гр.3) ',
'р.3. ст.190 гр.3', 'р.3.ст. (120+130+140+150+160+170+180) гр.3', '<=', '');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description )
values ('1.9', 'НК РФ ст. 173. Занижение суммы НДС, исчисленной к уплате в бюджет по р.3. ',
'При условии, что р.3. ст.200 гр. 3 заполнена. р.3 ст.200 гр.3 >= р.3 ст.(110 гр.5 - 190 гр.3) ',
'р.3. ст.200гр.3', ' р.3.ст.(110 гр.5-190 гр.3)', '>=', '');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description )
values ('1.10', 'НК РФ ст. 173. Занижение суммы НДС, исчисленной к уплате в бюджет по р.3. ',
'При условии, что р.3 ст.210 гр.3 заполнена. ст.210 гр.3 <= р.3 ст.(190 гр.3 - 110 гр.5) ', 'ст. 210 гр.3 ', 'р.3.ст.( 190 гр.3 - 110 гр.5)', '<=', '');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description )
values ('1.11', 'НК РФ ст.171 п.8, НК РФ ст.172 п.6, либо НК РФ ст. 146 п.1. Налоговые вычеты не обоснованы, либо налоговая база занижена, так как суммы отработанных авансов не включены в реализацию. ',
'При условии, что р.3. ст.170 гр.3 заполнена. р.3 ст.170 гр.3 <= р.3 ст.(010 + 020 + 030 + 040)гр.5 ', 'р.3. ст.170 гр.3', 'р.3. ст. (010+020+030+040) гр.5', '<=', '');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description )
values ('1.12', 'НК РФ ст. 171 п. 6, ст.172 п.5. Не обоснованное применение налоговых вычетов при выполнении СМР для собственного потребления. ',
'При условии, что р.3 ст.140 гр.3 онп заполнена. р.3 ст.060 гр.5 онп >= р.3 ст.140 гр.3 онп ', 'р.3 ст. 060 гр. 5 онп', 'р.3.ст.140 гр.3 онп', '>=', '');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description )
values ('1.13', 'НК РФ ст. 146, 153, 154. Занижение суммы налога вследствие не полного отражения налоговой базы. ',
'При условии, что заполнено хотя бы одно значение в гр.2 Прил.№2 к р.3 дНДС онп заполнена. Сумма всех гр.2 Прил.№2 к р.3 дНДС онп <= р.3 стр.110 гр.5 ',
'сумма всех гр.2 прил.№2 к р.3 дНДС онп', 'р.3 стр. 110 гр.5', '<=', '');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description )
values ('1.14', 'НК РФ ст. 170, 171, 172. Занижение суммы налога подлежащей вычету. ',
'При условии, что заполнено хотя бы одно значение в гр.3 Прил.№2 к р.3 дНДС онп заполнена. Сумма всех гр.3 Прил.№2 к р.3 дНДС онп <= р.3 стр.190 гр.3 ',
'сумма всех гр.3 прил.№2 к р.3 дНДС онп', 'р.3 стр. 190 гр.3', '<=', '');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description )
values ('1.15', 'НК РФ ст. 171, 172, Протокол р.II. Завышение суммы НДС, подлежащей вычету. ',
'При условии, что хотя бы одна из строк 040 или 050 р.4 заполнена. р.4 ст.120 <= (сумма величин р.4 ст.030 и р.4 ст.040) - (сумма величин р.4 ст.050 + р.4 ст.080) ',
'р.4 ст.120', '(сумма величин р.4 ст.030 и р.4 040) - (сумма величин р.4 ст.050 + р.4 ст. 080)', '<=', '');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description )
values ('1.16', 'НК РФ ст. 171, 172, Протокол р.II. Завышение суммы НДС, подлежащей вычету. ',
'При условии, что хотя бы одна из строк 050 р.5 заполнена. р.5 ст.080 <= сумма величин р.5 ст.050 ', 'р.5 ст.080', 'сумма величин р.5 ст.050', '<=', '');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description )
values ('1.17', 'НК РФ ст. 171, 172, Протокол р.II. Завышение суммы НДС, подлежащей вычету. ',
'При условии, что хотя бы одна из строк 070 р.5 заполнена. р.5 ст.090 <= сумма строк р.5 ст.070 ', 'р.5 ст.090', 'сумма строк р.5 ст.070', '<=', '');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description )
values ('1.18', 'НК РФ ст. 153, 164, 165, 167, Протокол р.II. Занижение исчисленной суммы НДС, вследствие неполного отражения НБ, либо неверное применение ставки по НДС. ',
'При условии, что хотя бы одна из строк 030 р.6 заполнена. р.6 ст.050 >= сумма строк р.6 ст.030 ', 'р.6 ст.050', 'сумма строк р.6 ст.030', '>=', '');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description )
values ('1.19', 'НК РФ ст. 171, 172, Протокол р.II. Завышение суммы НДС подлежащей вычету. ',
'При условии, что хотя бы одна из строк 040 р.6 заполнена. р. 6 ст.060 <= р.6 сумма строк ст.040 ', 'р. 6 ст.060', 'р.6 сумма строк ст.040 ', '<=', '');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description )
values ('1.20', 'НК РФ ст.173. Занижение суммы НДС, подлежащей уплате в бюджет за онп. ',
'При условии, что ст.200 р.3 + ст.130 р.4 + ст.160 р.6 >= ст.210 р.3 + ст.120 р.4 + ст.080 р.5 + ст.090 р.5 + ст.170 р.6 ст.040 р.1 >= (ст.200 р.3 + ст.130 р.4 + ст.160 р.6) - (ст.210 р.3 + ст.120 р.4 + ст.080 р.5 + ст.090 р.5 + ст.170 р.6) ',
'ст.040 р.1', ' (ст.200 р.3 + ст.130 р.4 + ст.160 р.6) - (ст.210 р.3 + ст.120 р.4 + ст.080 р.5 + ст.090 р.5 + ст.170 р.6)', '>=', '');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description )
values ('1.21', 'НК РФ ст. 173. Завышение суммы НДС подлежащей возмещению за онп.',
'При условии, что ст.200 р.3 + ст.130 р.4 + ст.160 р.6 < ст.210 р.3 + ст.120 р.4 + ст.080 р.5 + ст.090 р.5 + ст.170 р.6 р.1 ст.050 <= (ст.210 р.3 + ст.120 р.4 + ст.080 р.5 + ст.090 р.5 + ст.170 р.6) - (ст.200 р.3 + ст. 130 р.4 + ст.160 р.6)',
'р.1 ст.050', '(ст.210 р.3 + ст.120 р.4 + ст.080 р.5 + ст.090 р.5 + ст.170 р.6) - (ст.200 р.3 + ст. 130 р.4 + ст.160 р.6)', '<=', '');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description ) 
values ('1.22', 'НК РФ ст.171, 172 п.2 Раздела IV Приложения 4 к Постановлению № 1137. Выявлены несоответствия между сведениями раздела 8 и приложения №1 к разделу 8.', 
'При условии, что Прил.№1 к р.8 ст.005 заполнена. Прил.№1 к р.8 ст.005 = р.8 ст.190 ', 
'Прил.№1 к р.8 ст.005', 'р.8 ст.190 ', '=', '');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description ) 
values ('1.23.1', 'НК РФ ст.173. Выявлены несоответствия между сведениями раздела 9 и приложения №1 к разделу 9', 
'При условии, что Прил.№1 к р. 9 ст.020 заполнена. Прил.№1 к р.9 ст.020 = р.9 ст.230 ', 
'Прил.№1 к р. 9 ст.020', 'р.9 ст.230', '=', '');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description ) 
values ('1.23.2', 'НК РФ ст.173. Выявлены несоответствия между сведениями раздела 9 и приложения №1 к разделу 9', 
'При условии, что Прил.№1 к р.9 ст.030 заполнена. Прил.№1 к р.9 ст.030 = р.9 ст.240 ', 
'Прил.№1 к р. 9 ст.030', 'р.9 ст.240', '=', '');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description ) 
values ('1.23.3', 'НК РФ ст.173. Выявлены несоответствия между сведениями раздела 9 и приложения №1 к разделу 9', 
'При условии, что Прил.№1 к р.9 ст.040 заполнена. Прил.№1 к р.9 ст.040 = р.9 ст.250 ', 
'Прил.№1 к р. 9 ст.040', 'р.9 ст.250', '=', '');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description ) 
values ('1.23.4', 'НК РФ ст.173. Выявлены несоответствия между сведениями раздела 9 и приложения №1 к разделу 9', 
'При условии, что Прил.№1 к р.9 ст.050 заполнена. Прил.№1 к р.9 ст.050 = р.9 ст.260 ', 
'Прил.№1 к р. 9 ст.050', 'р.9 ст.260', '=', '');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description )
values ('1.23.5', 'НК РФ ст.173. Выявлены несоответствия между сведениями раздела 9 и приложения №1 к разделу 9', 
'При условии, что Прил.№1 к р.9 ст.060 заполнена. Прил.№1 к р.9 ст.060 = р.9 ст.270 ', 
'Прил.№1 к р. 9 ст.060', 'р.9 ст.270', '=', '');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description ) 
values ('1.23.6', 'НК РФ ст.173. Выявлены несоответствия между сведениями раздела 9 и приложения №1 к разделу 9',
'При условии, что Прил.№1 к р.9 ст.070 заполнена. Прил.№1 к р.9 ст.070 = р.9 ст.280 ',
'Прил.№1 к р. 9 ст.070', 'р.9 ст.280', '=', '');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description )
values ('1.24', 'НК РФ п.5 ст. 173. Занижение суммы НДС, подлежащей уплате в бюджет. ',
'При условии, что ст.030 р.1 > 0. р.1 ст.030 >= сумма р.12 ст.070 ', 'р.1 ст.030', 'сумма р.12 ст.070', '>=', '');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description ) 
values ('1.25', 'НК РФ ст. 173. Завышение суммы НДС, подлежащей возмещению за онп.', 
'При условии, что р.1 ст.050 > 0. ([р.8 ст.190 + (Прил.№1 к р.8 ст.190 – Прил.№1 к р.8 ст.005)] – [(р.9 ст.260 + р.9 ст.270) – (сумма р.9 ст.200 + р.9 ст.210, в которых р.9 ст.010 = 06)] + [Прил.№1 к р.9 ст.340 + Прил.№1 к р.9 ст.350 – Прил.№1 к р.9 ст.050 – Прил.№1 к р.9 ст.060] – [сумма Прил.№1 к р.9 ст.280 + Прил.№1 к р.9 ст.290, в которых Прил.№1 к р.9 ст.090 = 06]) > 0 ', 
'[р.8 ст.190 + (Прил.№1 к р.8 ст.190 – Прил.№1 к р.8 ст.005)] – ([(р.9 ст.260 + р.9 ст.270) – (сумма р.9 ст.200 + р.9 ст.210, в которых р.9 ст.010 = 06)] + [Прил.№1 к р.9 ст.340 + Прил.№1 к р.9 ст.350 – Прил.№1 к р.9 ст.050 – Прил.№1 к р.9 ст.060] – [сумма Прил.№1 к р.9 ст.280 + Прил.№1 к р.9 ст.290, в которых Прил.№1 к р.9 ст.090 = 06])',
'0', '>', '');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description )
values ('1.26', 'НК РФ ст. 161, п.4 ст.173. Занижение суммы НДС, подлежащей уплате в бюджет. ',
'При условии, что ст.060 хотя бы одного из листов р.2 заполнена или хотя бы в одном листе р.9 ст.010 = 06 или хотя бы в одном листе Прил.№1 к р.9 ст.090 = 06. Сумма ст.060 по всем листам р.2 >= ([Сумма ст.200 и 210 по всем листам р.9, в которых ст. 010 = 06] + [Сумма ст.280 и 290 по всем листам Прил.№1 к р.9, в которых ст. 090 = 06]) ',
'Сумма ст.060 по всем листам р.2', '[Сумма ст.200 и 210 по всем листам р.9, в которых ст. 010 = 06] + [Сумма ст.280 и 290 по всем листам Прил.№1 к р.9, в которых ст. 090 = 06]', '>=', '');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description ) 
values ('1.27', ' НК РФ ст. 153, 161, 164, 165, 166, 167, 173. Выявлены несоответствия между сведениями раздела 3 и 9 налоговой декларации', 
'При условии, что [ст. 040 р.1] > 0 или [ст.110 р.3] > 0 или  [ст.050 р.4] > 0 или  [ст.080 р.4] > 0 или [ст. 050 р.6] > 0 или [ст. 130 р.6] > 0 или Заполнен хотя бы один лист раздела 9 или Заполнен хотя бы один лист приложения к разделу 9, (гр.5 ст.110 р.3 + сумма ст.050 и 130 р.6 + сумма ст.060 по всем листам р.2 + сумма ст.050 р.4 + ст.080 р.4) = ([ст.260 + ст.270] р.9 + [ст.340 Прил.1 к р.9 дНДС + ст.350 Прил.1 к р.9 дНДС – ст.050 Прил.1 к р.9 дНДС – ст.060 Прил.1 к р.9 дНДС])',
'гр.5 ст.110 р.3 + сумма ст.050 и 130 р.6 + сумма ст.060 по всем листам р.2 + сумма ст.050 р.4 + ст.080 р.4', '[ст.260 + ст.270] р.9 + [ст.340 прил.1 к р.9 дНДС + ст.350 прил.1 к р.9 дНДС – ст.050 прил.1 к р.9 дНДС – ст.060 прил.1 к р.9 дНДС ]', 
'=', '');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description ) 
values ('1.28', 'НК РФ ст. 171, 172. Выявлены несоответствия между сведениями раздела 3 и 8 налоговой декларации',
'При условии, что заполнен хотя бы 1 лист р.8 или Прил. к р.8. (ст.190 р.3 + сумма стр.030 и 040 р.4 + ст.080 и 090 р.5 + ст.060 р.6 + ст. 090 р.6 + ст. 150 р.6) = (р.8 ст.190 + [ст.190 Прил.№1 р.8 – ст.005 Прил.№1 р.8]) ', 
'ст.190 р.3 + сумма стр.030 и 040 р.4 + ст.080 и 090 р.5 + ст.060 р.6 + ст. 090 р.6 + ст. 150 р.6', 'р.8 ст.190 + [ст.190 Прил.№1 р.8 – ст.005 Прил.№1 р.8] ',
'=', '');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description )
values ('1.29', 'НК РФ ст. 171, 172. Завышение суммы НДС, подлежащей вычету. ',
'При условии, что р.3 ст.150 заполнена. р.3 ст.150 <= [Сумма по ст.180 всех листов р.8, в которых ст.010 = 20] + [Сумма по ст.180 всех листов Прил.№1 р.8, в которых ст.010 = 20] ',
'р.3 ст.150', '[Сумма по ст.180 всех листов р.8, в которых ст.010 = 20] + [Сумма по ст.180 всех листов Прил.№1 р.8, в которых ст.010 = 20]', '<=', '');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description )
values ('1.30', 'НК РФ п.1 ст.153, ст. 171, 172, Протокол р. III. Завышение суммы НДС, подлежащей вычету в связи с ввозом товаров на таможенную территорию ТС. ',
'При условии, что р.3 ст.160 заполнена. р3 ст.160 <= [Сумма по ст.180 всех листов р.8, в которых р.8 ст.010 = 19] + [Сумма по ст.180 всех листов Прил.№1 р.8, в которых Прил.№1 р.8 ст.010 = 19] ',
'р3 ст.160 ', ' [Сумма по ст.180 всех листов р.8, в которых р.8 ст.010 = 19] + [Сумма по ст.180 всех листов Прил.№1 р.8, в которых Прил.№1 р.8 ст.010 = 19]', '<=', '');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description )
values ('1.31', 'НК РФ ст.161, 171, 172. Завышение суммы НДС, подлежащей вычету. ',
'При условии, что р.3 ст.180 заполнена. р.3 ст.180 <= [Сумма по ст.180 всех листов р.8, в которых р.8 ст.010 = 06] + [Сумма по ст.180 всех листов Прил.№1 р.8, в которых Прил.№1 р.8 ст.010 = 06] ',
'р.3 ст.180', ' [Сумма по ст.180 всех листов р.8, в которых р.8 ст.010 = 06] + [Сумма по ст.180 всех листов Прил.№1 р.8, в которых Прил.№1 р.8 ст.010 = 06]', '<=', '');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description ) 
values ('1.32', 'НК РФ ст.171, 172, п.7 Раздела II Приложения 4 к Постановлению №1137. Выявлены несоответствия сведений в разделе 8 ', 
'При условии, что заполнена хотя бы одна из ст. 180 всех листов р.8. сумма р.8 ст.180 = ст.190 на последней странице р.8 ', 
'сумма р.8 ст.180', 'ст.190 на последней странице р.8', '=', '');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description ) 
values ('1.33', 'НК РФ ст. 171, 172. Выявлены несоответствия сведений в приложении № 1 к разделу 8', 
'При условии, что заполнена хотя бы одна из ст.180 Прил.№1 к р.8. Прил.№1 р.8 ст.005 + Сумма Прил.№1 р.8 ст.180 = ст.190 на последней странице Прил.№1 р.8 ', 
'Прил.№1 р.8 ст. 005 + Сумма Прил.№1 р.8 ст. 180 ', 'ст.190 на последней странице Прил.№1 р.8', '=', '');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description ) 
values ('1.34', 'НК РФ ст.153, 173, п.8 Раздела II Приложения 5 к Постановлению №1137. Выявлены несоответствия сведений в разделе 9. ', 
'При условии, что хотя бы на одном листе р.9 заполнена ст.170. Сумма р.9 ст.170 = р.9 ст.230 ', 
'Сумма р.9 ст.170 ', 'р.9 ст.230', '=', '');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description ) 
values ('1.35', 'НК РФ ст.153, 173, п.8 Раздела II Приложения 5 к Постановлению №1137. Выявлены несоответствия сведений в разделе 9 (при условии, что соотношение 1.32 и 1.47 выполняется).', 
'При условии, что хотя бы на одном листе р.9 заполнена ст.180. Сумма р.9 ст.180 = р.9 ст.240 ', 
'Сумма р.9 ст.180', 'р.9 ст.240', '=', '');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description ) 
values ('1.36', 'НК РФ ст.164, 165, 167, 173. Выявлены несоответствия сведений в разделе 9 (при условии, что соотношение 1.37 выполняется).', 
'При условии, что хотя бы на одном листе р.9 заполнена ст. 190. Сумма р.9 ст.190 = р.9 ст.250 ',
'Сумма р.9 ст.190', 'р.9 ст.250', '=', '');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description ) 
values ('1.37', 'НК РФ ст.153, 173, п.8 Раздела II Приложения 5 к Постановлению №1137. Выявлены несоответствия сведений в разделе 9.', 
'При условии, что хотя бы на одном листе р.9 заполнена ст.200. Сумма р.9 ст.200 = р.9 ст.260 ', 
'Сумма р.9 ст.200', 'р.9 ст.260', '=', '');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description ) 
values ('1.38', 'НК РФ ст.153, 173, п.8 Раздела II Приложения 5 к Постановлению №1137. Выявлены несоответствия сведений в разделе 9 (при условии, что соотношение 1.32 выполняется).', 
'При условии, что хотя бы на одном листе р.9 заполнена ст.210. Сумма р.9 ст.210 = р.9 ст.270 ',
'Сумма р.9 ст.210', 'р.9 ст.270', '=', '');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description ) 
values ('1.39', 'НК РФ ст.153, 173, п.3 Раздела IV Приложения 5 к Постановлению №1137. Выявлены несоответствия сведений в приложении № 1 к разделу 9 (при условии, что соотношение 1.32 и 1.49 выполняются).', 
'При условии, что хотя бы одно Прил.№1 р.9 ст.250 > 0. Прил.№1 р.9 ст.020 + сумма Прил.№1 р.9 ст.250 = Прил.№1 р.9 ст.310 ', 
'Прил.№1 р.9 ст.020 + сумма Прил.№1 р.9 ст.250', 
'Прил.№1 р.9 ст.310', '=', '');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description ) 
values ('1.40', 'НК РФ ст.153, 173, п.3 Раздела IV Приложения 5 к Постановлению №1137. Выявлены несоответствия сведений в приложении № 1 к разделу 9.', 
'При условии, что хотя бы одно Прил.№1 р.9 ст.260 > 0. Прил.№1 р.9 ст.030 + сумма Прил.№1 р.9 ст.260 = Прил.№1 р.9 ст.320 ', 
'Прил.№1 р.9 ст.030 + сумма Прил.№1 р.9 ст.260', 
'Прил.№1 р.9 ст.320', '=', '');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description ) 
values ('1.41', 'НК РФ ст.164, 165, 167, 173. Выявлены несоответствия сведений в приложении № 1 к разделу 9.', 
'При условии, что хотя бы одно Прил.№1 р.9 ст.270 > 0. Прил.№1 р.9 ст.040 + сумма Прил.№1 р.9 ст.270 = Прил.№1 р.9 ст.330 ', 
'Прил.№1 р.9 ст.040 + сумма Прил.№1 р.9 ст.270', 
'Прил.№1 р.9 ст.330', '=', '');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description ) 
values ('1.42', 'НК РФ ст.153, 173, п.3 Раздела IV Приложения 5 к Постановлению №1137. Выявлены несоответствия сведений в приложении № 1 к разделу 9 (при условии, что соотношение 1.32 выполняется).', 
'При условии, что хотя бы одно Прил.№1 р.9 ст.280 > 0. Прил.№1 р.9 ст.050 + сумма Прил.№1 р.9 ст.280 = Прил.№1 р.9 ст.340 ', 
'Прил.№1 р.9 ст.050 + сумма Прил.№1 р.9 ст.280', 
'Прил.№1 р.9 ст.340', '=', '');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description ) 
values ('1.43', 'НК РФ ст.153, 173, п.3 Раздела IV Приложения 5 к Постановлению №1137. Выявлены несоответствия сведений в приложении № 1 к разделу 9', 
'При условии, что хотя бы одно Прил.№1 р.9 ст.290 > 0. Прил.№1 р.9 ст.060 + сумма Прил.№1 р.9 ст.290 = Прил.№1 р.9 ст.350 ', 
'Прил.№1 р.9 ст.060 + сумма Прил.№1 р.9 ст.290', 
'Прил.№1 р.9 ст.350', '=', '');

insert into nds2_mrr_user.control_ratio_type(code,formulation,calculation_condition,calculation_left_side,calculation_right_side, calculation_operator, description ) 
values ('1.44', 'НК РФ ст.173 Выявлены несоответствия сведений в разделе 12.', 
'При условии, что р.12 ст.060 > 0. Сумма р.12 ст.070 = сумма р.12 ст.080 - сумма р.12 ст.060 ', 
'сумма р.12 ст.070', 'сумма р.12 ст.080 - сумма р.12 ст.060', '=', '');

commit;