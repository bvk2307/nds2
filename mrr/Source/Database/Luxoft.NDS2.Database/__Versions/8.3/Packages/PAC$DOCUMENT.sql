CREATE OR REPLACE PACKAGE NDS2_MRR_USER.PAC$DOCUMENT
as
Type T$SEOD_DISCREP is record
(
DECLARATION_VERSION_ID NUMBER
,CHAPTER NUMBER
,ORDINAL_NUMBER NUMBER
,OKV_CODE VARCHAR2(12 CHAR)
,CREATE_DATE DATE
,RECEIVE_DATE DATE
,INVOICE_NUM VARCHAR2(4000 CHAR)
,INVOICE_DATE DATE
,CHANGE_NUM VARCHAR2(1024 CHAR)
,CHANGE_DATE DATE
,CORRECTION_NUM VARCHAR2(4000 CHAR)
,CORRECTION_DATE DATE
,CHANGE_CORRECTION_NUM VARCHAR2(1024 CHAR)
,CHANGE_CORRECTION_DATE DATE
,SELLER_INVOICE_NUM VARCHAR2(4000 CHAR)
,SELLER_INVOICE_DATE DATE
,BROKER_INN VARCHAR2(12 CHAR)
,BROKER_KPP VARCHAR2(10 CHAR)
,DEAL_KIND_CODE NUMBER
,CUSTOMS_DECLARATION_NUM VARCHAR2(4000)
,PRICE_BUY_AMOUNT NUMBER
,PRICE_BUY_NDS_AMOUNT NUMBER
,PRICE_SELL NUMBER
,PRICE_SELL_IN_CURR NUMBER
,PRICE_SELL_18 NUMBER
,PRICE_SELL_10 NUMBER
,PRICE_SELL_0 NUMBER
,PRICE_NDS_18 NUMBER
,PRICE_NDS_10 NUMBER
,PRICE_TAX_FREE NUMBER
,PRICE_TOTAL NUMBER
,PRICE_NDS_TOTAL NUMBER
,DIFF_CORRECT_DECREASE NUMBER
,DIFF_CORRECT_INCREASE NUMBER
,DIFF_CORRECT_NDS_DECREASE NUMBER
,DIFF_CORRECT_NDS_INCREASE NUMBER
,PRICE_NDS_BUYER NUMBER
,ROW_KEY VARCHAR2(512 CHAR)
,ACTUAL_ROW_KEY VARCHAR2(512 CHAR)
,COMPARE_ROW_KEY VARCHAR2(4000 CHAR)
,COMPARE_ALGO_ID NUMBER
,FORMAT_ERRORS VARCHAR2(512 CHAR)
,LOGICAL_ERRORS VARCHAR2(512 CHAR)
,SELLER_AGENCY_INFO_INN VARCHAR2(12 CHAR)
,SELLER_AGENCY_INFO_KPP VARCHAR2(10 CHAR)
,SELLER_AGENCY_INFO_NAME VARCHAR2(2048 CHAR)
,SELLER_AGENCY_INFO_NUM VARCHAR2(4000 CHAR)
,SELLER_AGENCY_INFO_DATE DATE
,IS_DOP_LIST NUMBER(1)
,IS_IMPORT NUMBER(1)
,CLARIFICATION_KEY VARCHAR2(512 CHAR)
,CONTRAGENT_KEY NUMBER);



  PROCEDURE DEMO;
  PROCEDURE CLOSE_EXPIRED_DOCUMENTS;
  PROCEDURE SEND_CLAIM_KS;
  PROCEDURE PROCESS_QUEUE;
  PROCEDURE COLLECT_QUEUE;
END;
/

CREATE OR REPLACE PACKAGE BODY NDS2_MRR_USER.PAC$DOCUMENT
as
  TYPE T_LIST_STRING is TABLE OF VARCHAR2(2048 char);

  SEL_STATUS_REQUST_SENDING CONSTANT NUMBER(2)  := 13;
  SOV_REQUEST_STATUS_DONE CONSTANT NUMBER(1)    := 2;
  DATE_FORMAT CONSTANT varchar2(12)             := 'DD.MM.YYYY';
  DECIMAL_FORMAT CONSTANT varchar2(25 char)     := '9999999999999999990.00';

  XSD_NDS2_CAM_01 CONSTANT varchar2(32 char)    := 'TAX3EXCH_NDS2_CAM_01_01.xsd';
  XSD_NDS2_CAM_02 CONSTANT varchar2(32 char)    := 'TAX3EXCH_NDS2_CAM_02_01.xsd';
  XSD_NDS2_CAM_03 CONSTANT varchar2(32 char)    := 'TAX3EXCH_NDS2_CAM_03_01.xsd';
  XSD_NDS2_CAM_04 CONSTANT varchar2(32 char)    := 'TAX3EXCH_NDS2_CAM_04_01.xsd';

procedure UTL_LOG
  (
    v_source varchar2,
    v_code varchar2 := 0,
    v_msg varchar2 := null,
    v_entity_id varchar2 := null
  )
as
pragma autonomous_transaction;
begin
  insert into system_log(id, type, site, entity_id, message_code, message, log_date)
  values(seq_sys_log_id.nextval, 0, v_source, v_entity_id, v_code, v_msg, sysdate);
  commit;
exception when others then
  rollback;
  dbms_output.put_line('UTL_LOG:'||sqlcode||'-'||substr(sqlerrm, 0, 256));
end;

FUNCTION UTL_FORMAT_DECIMAL(v_val in number)
return varchar2
is
begin
  return trim(to_char(v_val, DECIMAL_FORMAT));
end;

FUNCTION UTL_FORMAT_DATE(v_val in DATE)
return varchar2
is
begin
  return to_char(v_val, DATE_FORMAT);
end;

PROCEDURE UTL_REMOVE_IF_EMPTY(p_parent in xmldom.DOMNode, p_node in xmldom.DOMNode)
as
d_node xmldom.DOMNode;
begin
   if dbms_xmldom.getLength(dbms_xmldom.getChildNodes(p_node)) < 1 then
     d_node := dbms_xmldom.removeChild(p_parent, p_node);
     xmldom.freeNode(d_node);
   end if;
end;

PROCEDURE UTL_GET_TAXPERIOD_BOUNDS
(
  p_period in varchar2,
  p_year in varchar2,
  p_start_date out varchar2,
  p_end_date out varchar2
)
as
begin
  p_start_date := null;
  p_end_date   := null;
  if p_period = '21' then
    p_start_date := '01.01.' || p_year;
    p_end_date   := '31.03.' || p_year;
  else
    if p_period = '22' then
      p_start_date := '01.04.' || p_year;
      p_end_date   := '30.06.' || p_year;
    else
      if p_period = '23' then
        p_start_date := '01.07.' || p_year;
        p_end_date   := '30.09.' || p_year;
      else
        if p_period = '24' then
          p_start_date := '01.10.' || p_year;
          p_end_date   := '31.12.' || p_year;
        end if;
      end if;
    end if;
  end if;
end;

PROCEDURE UTL_XML_APPEND_ATTRIBUTE
  (
    p_elm in xmldom.DOMElement
   ,p_attrName in varchar2
   ,p_attrValue in varchar2
  )
as
begin
if length(nvl(p_attrValue, '')) > 0 then
  dbms_xmldom.setAttribute(p_elm, p_attrName, p_attrValue);
end if;
end;

function UTL_ADD_CONFIG_WORK_DAYS(p_date in date, p_config_key varchar2) return date
as
begin
  return utl_add_work_days(p_date, utl_get_configuration_number(p_config_key));
end;

function UTL_GET_DEADLINE_CLAIM
return date
as
begin
  return utl_add_config_work_days(utl_add_config_work_days(sysdate, 'timeout_answer_for_autoclaim'), 'timeout_claim_delivery');
  exception when others then
    return sysdate + 8;
end;

function UTL_GET_DEADLINE_RECLAIM
return date
as
begin
  return utl_add_config_work_days(utl_add_config_work_days(sysdate, 'timeout_answer_for_autooverclaim'), 'timeout_claim_delivery');
  exception when others then
    return sysdate + 8;
end;

PROCEDURE UTL_ADD_LIST_ITEM(p_list in out t_list_string, p_value in varchar2, p_unique_only number := 0)
as
  v_cancel number(1);
begin
  v_cancel := 0;
  if p_unique_only <> 0 then
    for i in 1 .. p_list.count
    loop
      if p_list(i) = p_value then
        v_cancel := 1;
        exit;
      end if;
    end loop;
  end if;

  if v_cancel = 0 then
    p_list.extend;
    p_list(p_list.count) := p_value;
  end if;
end;

FUNCTION UTL_COMMA_TO_LIST
(
  p_input_string in varchar2,
  p_remove_empty_entries in number := 1,
  p_pattern in varchar2 := '[^,]+'
)
return t_list_string
as
  v_result t_list_string;
begin
  v_result := t_list_string();

  for v_row in
  (
    select val
    from
    (
      select trim(regexp_substr(p_input_string, p_pattern, 1, level)) as val
      from dual
      connect by regexp_substr(p_input_string, p_pattern, 1, level) is not null
    ) t
    where p_remove_empty_entries = 0 or nvl(length(val), 0) > 0
  )
  loop
    v_result.extend;
    v_result(v_result.count) := v_row.val;
  end loop;

  return v_result;
end;

FUNCTION UTL_LIST_TO_COMMA(p_list in t_list_string, p_separator in varchar2 := ', ') return varchar2
as
  v_result varchar2(2048 char);
begin
  v_result := '';
  if p_list.count > 0 then
    for i in p_list.first .. p_list.last
    loop
    v_result := v_result || p_list(i) || p_separator;
    end loop;
  end if;

  if length(v_result) > length(p_separator) then
    return substr(v_result, 1, length(v_result) - length(p_separator));
  else
    return null;
  end if;
end;

PROCEDURE UTL_CREATE_OPER_CODE_NODES
(
  p_doc in xmldom.DOMDocument,
  p_root_node in xmldom.DOMNode,
  p_node_name in varchar2,
  p_row_key in varchar2
)
as
  v_list          t_list_string;
  v_split_element xmldom.DOMElement;
  v_split_node    xmldom.DOMNode;
  v_text_element  xmldom.DOMText;
  v_text_node     xmldom.DOMNode;
begin
  for line in (select * from sov_invoice_operation_all where row_key_id = p_row_key)
  loop
      v_split_element := xmldom.createElement(p_doc, p_node_name);
      v_split_node := xmldom.appendChild(p_root_node, xmldom.makeNode(v_split_element));
      v_text_element := xmldom.createTextNode(p_doc, line.operation_code);
      v_text_node := xmldom.appendChild(v_split_node, xmldom.makeNode(v_text_element));
  end loop;
end;

PROCEDURE UTL_CREATE_BUY_ACCEPT_NODES
(
  p_doc in xmldom.DOMDocument,
  p_root_node in xmldom.DOMNode,
  p_node_name in varchar2,
  p_row_key in varchar2
)
as
  v_list          t_list_string;
  v_split_element xmldom.DOMElement;
  v_split_node    xmldom.DOMNode;
  v_text_element  xmldom.DOMText;
  v_text_node     xmldom.DOMNode;
begin
  for line in (select * from sov_invoice_buy_accept_all where row_key_id = p_row_key)
  loop
      v_split_element := xmldom.createElement(p_doc, p_node_name);
      v_split_node := xmldom.appendChild(p_root_node, xmldom.makeNode(v_split_element));
      v_text_element := xmldom.createTextNode(p_doc, UTL_FORMAT_DATE(line.buy_accept_date));
      v_text_node := xmldom.appendChild(v_split_node, xmldom.makeNode(v_text_element));
  end loop;
end;

PROCEDURE UTL_CREATE_SPLITED_NODES
(
  p_doc in xmldom.DOMDocument,
  p_root_node in xmldom.DOMNode,
  p_node_name in varchar2,
  p_input_string in varchar2
)
as
  v_list          t_list_string;
  v_split_element xmldom.DOMElement;
  v_split_node    xmldom.DOMNode;
  v_text_element  xmldom.DOMText;
  v_text_node     xmldom.DOMNode;
begin
  v_list := UTL_COMMA_TO_LIST(p_input_string);
  if v_list.count > 0 then
    for i in v_list.first .. v_list.last
    loop
    v_split_element := xmldom.createElement(p_doc, p_node_name);
    v_split_node := xmldom.appendChild(p_root_node, xmldom.makeNode(v_split_element));
    v_text_element := xmldom.createTextNode(p_doc, v_list(i));
    v_text_node := xmldom.appendChild(v_split_node, xmldom.makeNode(v_text_element));
    end loop;
  end if;
end;

PROCEDURE UTL_CREATE_DOC_NODES
(
  p_doc in xmldom.DOMDocument,
  p_root_node in xmldom.DOMNode,
  p_node_name in varchar2,
  p_doc_num_caption in varchar2,
  p_doc_date_caption in varchar2,
  p_row_key in varchar2
)
as
  v_doc_element xmldom.DOMElement;
  v_doc_node    xmldom.DOMNode;

  v_dates t_list_string;
  v_numbers t_list_string;
begin
  for line in (select * from sov_invoice_payment_doc_all where row_key_id = p_row_key)
  loop
    v_doc_element := xmldom.createElement(p_doc, p_node_name);
    UTL_XML_APPEND_ATTRIBUTE(v_doc_element, p_doc_num_caption, line.doc_num);
    UTL_XML_APPEND_ATTRIBUTE(v_doc_element, p_doc_date_caption, UTL_FORMAT_DATE(line.doc_date));
    v_doc_node := xmldom.appendChild(p_root_node, xmldom.makeNode(v_doc_element));
  end loop;

end;

/*PROCEDURE UTL_CREATE_DOC_NODES
(
  p_doc in xmldom.DOMDocument,
  p_root_node in xmldom.DOMNode,
  p_node_name in varchar2,
  p_doc_num_caption in varchar2,
  p_doc_date_caption in varchar2,
  p_doc_num_value in varchar2,
  p_doc_date_value in varchar2
)
as
  v_doc_element xmldom.DOMElement;
  v_doc_node    xmldom.DOMNode;

  v_dates t_list_string;
  v_numbers t_list_string;
begin
  v_dates := UTL_COMMA_TO_LIST(p_doc_date_value, 0);
  v_numbers := UTL_COMMA_TO_LIST(p_doc_num_value, 0);

  if v_numbers.count > 0 then
    for i in v_numbers.first .. v_numbers.last
    loop
    v_doc_element := xmldom.createElement(p_doc, p_node_name);
    UTL_XML_APPEND_ATTRIBUTE(v_doc_element, p_doc_num_caption, v_numbers(i));
    UTL_XML_APPEND_ATTRIBUTE(v_doc_element, p_doc_date_caption, v_dates(i));
    v_doc_node := xmldom.appendChild(p_root_node, xmldom.makeNode(v_doc_element));
    end loop;
  end if;
end;*/

PROCEDURE UTL_CREATE_TAXPAYER_INFO_NODE
(
  p_doc in xmldom.DOMDocument,
  p_root_node in xmldom.DOMNode,
  p_node_name in varchar2,
  p_inn in varchar2
)
as
  v_taxpayer_info_element xmldom.DOMElement;
  v_taxpayer_info_node    xmldom.DOMNode;
  v_taxpayer_element      xmldom.DOMElement;
  v_taxpayer_node         xmldom.DOMNode;
begin
  if nvl(length(p_inn), 0) in (10, 12) then
    v_taxpayer_info_element := xmldom.createElement(p_doc, p_node_name);
    v_taxpayer_info_node := xmldom.appendChild(p_root_node, xmldom.makeNode(v_taxpayer_info_element));

    if length(p_inn) = 10 then
      v_taxpayer_element := xmldom.createElement(p_doc, '������');
      xmldom.setAttribute(v_taxpayer_element, '�����', p_inn);
    else
      v_taxpayer_element := xmldom.createElement(p_doc, '������');
      xmldom.setAttribute(v_taxpayer_element, '�����', p_inn);
    end if;
    v_taxpayer_node := xmldom.appendChild(v_taxpayer_info_node, xmldom.makeNode(v_taxpayer_element));
  end if;
end;

/*PROCEDURE UTL_CREATE_TAXPAYER_INFO_NODE
(
  p_doc in xmldom.DOMDocument,
  p_root_node in xmldom.DOMNode,
  p_node_name in varchar2,
  p_inn in varchar2
)
as
  v_taxpayer_info_element xmldom.DOMElement;
  v_taxpayer_info_node    xmldom.DOMNode;
  v_taxpayer_element      xmldom.DOMElement;
  v_taxpayer_node         xmldom.DOMNode;
begin
  if nvl(length(p_inn), 0) in (10, 12) then
    v_taxpayer_info_element := xmldom.createElement(p_doc, p_node_name);
    v_taxpayer_info_node := xmldom.appendChild(p_root_node, xmldom.makeNode(v_taxpayer_info_element));

    if length(p_inn) = 10 then
      v_taxpayer_element := xmldom.createElement(p_doc, '������');
      xmldom.setAttribute(v_taxpayer_element, '�����', p_inn);
    else
      v_taxpayer_element := xmldom.createElement(p_doc, '������');
      xmldom.setAttribute(v_taxpayer_element, '�����', p_inn);
    end if;
    v_taxpayer_node := xmldom.appendChild(v_taxpayer_info_node, xmldom.makeNode(v_taxpayer_element));
  end if;
end;*/

PROCEDURE UTL_CREATE_SELLER_INFO_NODES
(
  p_doc in xmldom.DOMDocument,
  p_root_node in xmldom.DOMNode,
  p_node_name in varchar2,
  p_row_key in varchar2
)
as
  v_inn t_list_string;
begin
  for line in (select * from sov_invoice_seller_all where row_key_id = p_row_key)
  loop
    UTL_CREATE_TAXPAYER_INFO_NODE(p_doc, p_root_node, p_node_name, line.seller_inn);
  end loop;
end;

PROCEDURE UTL_CREATE_BUYER_INFO_NODES
(
  p_doc in xmldom.DOMDocument,
  p_root_node in xmldom.DOMNode,
  p_node_name in varchar2,
  p_row_key in varchar2
)
as
  v_inn t_list_string;
begin
  for line in (select * from sov_invoice_buyer_all where row_key_id = p_row_key)
  loop
    UTL_CREATE_TAXPAYER_INFO_NODE(p_doc, p_root_node, p_node_name, line.buyer_inn);
  end loop;
end;
/*
PROCEDURE UTL_CREATE_TAXPAYER_INFO_NODES
(
  p_doc in xmldom.DOMDocument,
  p_root_node in xmldom.DOMNode,
  p_node_name in varchar2,
  p_inn_raw in varchar2
)
as
  v_inn t_list_string;
begin
  v_inn := UTL_COMMA_TO_LIST(p_inn_raw);

  if v_inn.count > 0 then
    for i in v_inn.first .. v_inn.last
    loop
    UTL_CREATE_TAXPAYER_INFO_NODE(p_doc, p_root_node, p_node_name, v_inn(i));
    end loop;
  end if;
end;
*/
PROCEDURE UTL_CREATE_ERROR_NODES
(
  p_doc in xmldom.DOMDocument,
  p_root_node in xmldom.DOMNode,
--  p_row_key in varchar2
   p_invoice_row in sov_invoice_all%rowtype
)
as
  v_result t_list_string;
  v_graph t_list_string;
  v_inn_1 varchar2(12 char);
  v_inn_2 varchar2(12 char);

  v_header_element xmldom.DOMElement;
  v_header_node    xmldom.DOMNode;
  v_graph_element xmldom.DOMElement;
  v_graph_node    xmldom.DOMNode;
  v_text_element  xmldom.DOMText;
  v_text_node     xmldom.DOMNode;
begin
  v_result := t_list_string();
  v_graph := t_list_string();

  for discrepancy_row in
  (
      select
        dis.id                                    as id,
        dis.type                                  as type,
        dis.rule_num                              as rule_num,
        dis.invoice_rk                            as invoice_row_key,
        dis.invoice_chapter                       as invoice_chapter,
        dis.decl_id                               as declaration_id
      from sov_discrepancy dis
--      inner join seod_data_queue queue on queue.discrepancy_id = dis.id and queue.for_stage = dis.stage
--        inner join sov_invoice_all sia on sia.row_key
      where dis.invoice_rk in (p_invoice_row.ROW_KEY, p_invoice_row.ACTUAL_ROW_KEY)
  )
  loop
    if discrepancy_row.type = 1 then --������
      select decl_1.inn, decl_2.inn
      into v_inn_1, v_inn_2
      from sov_discrepancy dis
      inner join v$declaration decl_1 on decl_1.id = dis.decl_id
      left join v$declaration decl_2 on decl_2.id = dis.decl_contractor_id
      where dis.id = discrepancy_row.id;

      if v_inn_1 = v_inn_2 then
        if discrepancy_row.invoice_chapter in (8, 9) then
          UTL_ADD_LIST_ITEM(v_result, '2', 1);
        end if;
      elsif discrepancy_row.invoice_chapter = 8 then
        UTL_ADD_LIST_ITEM(v_result, '1', 1);
      elsif discrepancy_row.invoice_chapter in (10, 11) then
        UTL_ADD_LIST_ITEM(v_result, '3', 1);
      end if;
    else
      for graph_row in
      (
        select graph
        from dict_compare_graphs
        where discrepancy_type = discrepancy_row.type
          and invoice_chapter = discrepancy_row.invoice_chapter
          and (discrepancy_row.type <> 2 or rule_code = discrepancy_row.rule_num)
      )
      loop
        UTL_ADD_LIST_ITEM(v_graph, graph_row.graph, 1);
      end loop;
    end if;
  end loop;

  if v_graph.count > 0 then
    UTL_ADD_LIST_ITEM(v_result, '4', 1);
  end if;

  if v_result.count > 0 then
    v_header_element := xmldom.createElement(p_doc, '��������');
    v_header_node    := xmldom.appendChild(p_root_node, xmldom.makeNode(v_header_element));

    /*
    if v_result.count > 1 then
      select cast(multiset(select * from table(v_result) order by 1) as t_list_string)
      into v_result
      from dual;
    end if;
    */

    UTL_XML_APPEND_ATTRIBUTE(v_header_element, '���', v_result(v_result.first));
    if v_result(v_result.first) = '4' then
      for j in v_graph.first .. v_graph.last
      loop
        v_graph_element := xmldom.createElement(p_doc, '������');
        v_graph_node    := xmldom.appendChild(v_header_node, xmldom.makeNode(v_graph_element));
        v_text_element  := xmldom.createTextNode(p_doc, v_graph(j));
        v_text_node     := xmldom.appendChild(v_graph_node, xmldom.makeNode(v_text_element));
      end loop;
    end if;
  end if;
end;

/* ############################ */
/* # ������������ ������ 1.1 ## */
/* ############################ */
FUNCTION CLAIM_BUILD_T1_1
(
   p_doc in xmldom.DOMDocument,
   p_container_node in xmldom.DOMNode,
   p_invoice_row in sov_invoice_all%rowtype
)
return boolean
as
  v_itemElement xmldom.DOMElement;
  v_itemNode    xmldom.DOMNode;
begin
  /* ���� ������             */
  v_itemElement := xmldom.createElement(p_doc, 'T1.1���');
  v_itemNode := xmldom.appendChild(p_container_node, xmldom.makeNode(v_itemElement));

  /* ������������ ��������   */
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '��������',        p_invoice_row.ordinal_number);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '����������',      p_invoice_row.invoice_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '�����������',     UTL_FORMAT_DATE(p_invoice_row.invoice_date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '�����������',     UTL_FORMAT_DECIMAL(p_invoice_row.price_buy_amount));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '���������',       UTL_FORMAT_DECIMAL(p_invoice_row.price_buy_nds_amount));

  /* �������������� �������� */
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '����������',      p_invoice_row.change_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '�����������',     UTL_FORMAT_DATE(p_invoice_row.change_date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '�����������',     p_invoice_row.correction_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '������������',    UTL_FORMAT_DATE(p_invoice_row.correction_date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '�����������',     p_invoice_row.change_correction_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '������������',    UTL_FORMAT_DATE(p_invoice_row.change_correction_date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '�����',           p_invoice_row.customs_declaration_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '���',             p_invoice_row.okv_code);

  /* ��������                */
  UTL_CREATE_ERROR_NODES(p_doc, v_itemNode, p_invoice_row);
  UTL_CREATE_SELLER_INFO_NODES(p_doc, v_itemNode, '������', p_invoice_row.ROW_KEY);
  UTL_CREATE_TAXPAYER_INFO_NODE(p_doc, v_itemNode, '�����',  p_invoice_row.BROKER_INN);
  UTL_CREATE_DOC_NODES(p_doc, v_itemNode, '����������', '�������������', '��������������', p_invoice_row.ROW_KEY);
  UTL_CREATE_OPER_CODE_NODES(p_doc, v_itemNode, '����������', p_invoice_row.ROW_KEY);
  UTL_CREATE_BUY_ACCEPT_NODES(p_doc, v_itemNode, '���������',  p_invoice_row.ROW_KEY);

  return true;
  exception when others then
    /*log error*/
    UTL_LOG('CLAIM_BUILD_T1_1', sqlcode, substr(sqlerrm, 0, 256));
    return false;
end;

/* ############################ */
/* # ������������ ������ 1.2 ## */
/* ############################ */
FUNCTION CLAIM_BUILD_T1_2
(
   p_doc in xmldom.DOMDocument,
   p_container_node in xmldom.DOMNode,
   p_invoice_row in sov_invoice_all%rowtype
)
return boolean
as
  v_itemElement xmldom.DOMElement;
  v_itemNode xmldom.DOMNode;
begin
  /* ���� ������             */
  v_itemElement := xmldom.createElement(p_doc, 'T1.2���');
  v_itemNode := xmldom.appendChild(p_container_node, xmldom.makeNode(v_itemElement));

  /* ������������ ��������   */
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '��������',       p_invoice_row.ordinal_number);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '����������',     p_invoice_row.invoice_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '�����������',    UTL_FORMAT_DATE(p_invoice_row.invoice_date));

  /* �������������� �������� */
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '����������',     p_invoice_row.change_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '�����������',    UTL_FORMAT_DATE(p_invoice_row.change_date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '�����������',    p_invoice_row.correction_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '������������',   UTL_FORMAT_DATE(p_invoice_row.correction_date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '�����������',    p_invoice_row.change_correction_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '������������',   UTL_FORMAT_DATE(p_invoice_row.change_correction_date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '���',            p_invoice_row.okv_code);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '������������',   UTL_FORMAT_DECIMAL(p_invoice_row.price_sell_in_curr));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '�����������18',  UTL_FORMAT_DECIMAL(p_invoice_row.price_sell_18));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '�����������10',  UTL_FORMAT_DECIMAL(p_invoice_row.price_sell_10));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '�����������0',   UTL_FORMAT_DECIMAL(p_invoice_row.price_sell_0));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '��������18',     UTL_FORMAT_DECIMAL(p_invoice_row.price_nds_18));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '��������10',     UTL_FORMAT_DECIMAL(p_invoice_row.price_nds_10));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '������������',   UTL_FORMAT_DECIMAL(p_invoice_row.price_tax_free));

  /* ��������                */
  UTL_CREATE_ERROR_NODES(p_doc, v_itemNode, p_invoice_row);
  UTL_CREATE_BUYER_INFO_NODES(p_doc, v_itemNode, '�������', p_invoice_row.ROW_KEY);
  UTL_CREATE_TAXPAYER_INFO_NODE(p_doc, v_itemNode, '�����', p_invoice_row.broker_inn);
  UTL_CREATE_DOC_NODES(p_doc, v_itemNode, '����������', '�������������', '��������������', p_invoice_row.ROW_KEY);
  UTL_CREATE_OPER_CODE_NODES(p_doc, v_itemNode, '����������', p_invoice_row.ROW_KEY);

  return true;
  exception when others then
    /*log error*/
    UTL_LOG('CLAIM_BUILD_T1_2', sqlcode, substr(sqlerrm, 0, 256));
    return false;
end;

/* ############################ */
/* # ������������ ������ 1.3 ## */
/* ############################ */
FUNCTION CLAIM_BUILD_T1_3
(
   p_doc in xmldom.DOMDocument,
   p_container_node in xmldom.DOMNode,
   p_invoice_row in sov_invoice_all%rowtype
)
return boolean
as
  v_itemElement xmldom.DOMElement;
  v_itemNode xmldom.DOMNode;

  v_broker_activity_element xmldom.DOMElement;
  v_broker_activity_node xmldom.DOMNode;

  v_page_broker_activity_element xmldom.DOMElement;
  v_page_broker_activity_node xmldom.DOMNode;

  v_seller_inn t_list_string;
begin
  /* ���� ������             */
  v_itemElement := xmldom.createElement(p_doc, 'T1.3���');
  v_itemNode := xmldom.appendChild(p_container_node, xmldom.makeNode(v_itemElement));

  /* ������������ ��������   */
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '��������',       p_invoice_row.ordinal_number);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '����������',     p_invoice_row.invoice_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '�����������',    UTL_FORMAT_DATE(p_invoice_row.invoice_date));

  /* �������������� �������� */
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '����������',     p_invoice_row.change_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '�����������',    UTL_FORMAT_DATE(p_invoice_row.change_date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '�����������',    p_invoice_row.correction_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '������������',   UTL_FORMAT_DATE(p_invoice_row.correction_date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '�����������',    p_invoice_row.change_correction_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '������������',   UTL_FORMAT_DATE(p_invoice_row.change_correction_date));

  /* ��������                */
  UTL_CREATE_ERROR_NODES(p_doc, v_itemNode, p_invoice_row);
--  UTL_CREATE_TAXPAYER_INFO_NODE(p_doc, v_itemNode, '�������', p_invoice_row.buyer_inn);

  /*!!!!TODO!!!!! ���������� - ��� ���������?*/
  /*
  v_seller_inn := UTL_COMMA_TO_LIST(p_invoice_row.seller_inn);
  for i in v_seller_inn.first .. v_seller_inn.last
  loop
    v_broker_activity_element := xmldom.createElement(p_doc, '����������');
    v_broker_activity_node := xmldom.appendChild(v_itemNode, xmldom.makeNode(v_broker_activity_element));
    v_page_broker_activity_element := xmldom.createElement(p_doc, '�������������');
    v_page_broker_activity_node := xmldom.appendChild(v_broker_activity_node, xmldom.makeNode(v_page_broker_activity_element));

    UTL_XML_APPEND_ATTRIBUTE(v_page_broker_activity_element, '������������',  p_invoice_row.seller_invoice_num); --?
    UTL_XML_APPEND_ATTRIBUTE(v_page_broker_activity_element, '�������������', UTL_FORMAT_DATE(p_invoice_row.seller_invoice_date)); --?
    UTL_XML_APPEND_ATTRIBUTE(v_page_broker_activity_element, '���',           p_invoice_row.okv_code);
    UTL_XML_APPEND_ATTRIBUTE(v_page_broker_activity_element, '�������������', UTL_FORMAT_DECIMAL(p_invoice_row.price_total));
    UTL_XML_APPEND_ATTRIBUTE(v_page_broker_activity_element, '���������',     UTL_FORMAT_DECIMAL(p_invoice_row.price_nds_total));
    UTL_XML_APPEND_ATTRIBUTE(v_page_broker_activity_element, '�����������',   UTL_FORMAT_DECIMAL(p_invoice_row.diff_correct_decrease));
    UTL_XML_APPEND_ATTRIBUTE(v_page_broker_activity_element, '�����������',   UTL_FORMAT_DECIMAL(p_invoice_row.diff_correct_increase));
    UTL_XML_APPEND_ATTRIBUTE(v_page_broker_activity_element, '������������',  UTL_FORMAT_DECIMAL(p_invoice_row.diff_correct_nds_decrease));
    UTL_XML_APPEND_ATTRIBUTE(v_page_broker_activity_element, '������������',  UTL_FORMAT_DECIMAL(p_invoice_row.diff_correct_nds_increase));

    UTL_CREATE_TAXPAYER_INFO_NODE(p_doc, v_page_broker_activity_node, '������', v_seller_inn(i));
  end loop;
  */
--  UTL_CREATE_SPLITED_NODES(p_doc, v_itemNode, '����������', p_invoice_row.operation_code);
  return true;
  exception when others then
    /*log error*/
    UTL_LOG('CLAIM_BUILD_T1_3', sqlcode, substr(sqlerrm, 0, 256));
    return false;
end;

/* ############################ */
/* # ������������ ������ 1.4 ## */
/* ############################ */
FUNCTION CLAIM_BUILD_T1_4
(
   p_doc in xmldom.DOMDocument,
   p_container_node in xmldom.DOMNode,
   p_invoice_row in sov_invoice_all%rowtype
)
return boolean
as
  v_itemElement xmldom.DOMElement;
  v_itemNode xmldom.DOMNode;
begin
  /* ���� ������             */
  v_itemElement := xmldom.createElement(p_doc, 'T1.4���');
  v_itemNode := xmldom.appendChild(p_container_node, xmldom.makeNode(v_itemElement));
  /* ������������ ��������   */
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '��������',       p_invoice_row.ordinal_number);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '����������',     p_invoice_row.invoice_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '�����������',    UTL_FORMAT_DATE(p_invoice_row.invoice_date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '��������',       p_invoice_row.deal_kind_code);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '�������������',  UTL_FORMAT_DECIMAL(p_invoice_row.price_total));

  /* �������������� �������� */
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '����������',     p_invoice_row.change_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '�����������',    UTL_FORMAT_DATE(p_invoice_row.change_date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '�����������',    p_invoice_row.correction_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '������������',   UTL_FORMAT_DATE(p_invoice_row.correction_date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '�����������',    p_invoice_row.change_correction_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '������������',   UTL_FORMAT_DATE(p_invoice_row.change_correction_date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '���',            p_invoice_row.okv_code);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '���������',      UTL_FORMAT_DECIMAL(p_invoice_row.price_nds_total));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '�����������',    UTL_FORMAT_DECIMAL(p_invoice_row.diff_correct_decrease));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '�����������',    UTL_FORMAT_DECIMAL(p_invoice_row.diff_correct_increase));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '������������',   UTL_FORMAT_DECIMAL(p_invoice_row.diff_correct_nds_decrease));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '������������',   UTL_FORMAT_DECIMAL(p_invoice_row.diff_correct_nds_increase));

  /* ��������                */
  UTL_CREATE_ERROR_NODES(p_doc, v_itemNode, p_invoice_row);
  UTL_CREATE_SELLER_INFO_NODES(p_doc, v_itemNode, '������', p_invoice_row.ROW_KEY);
  UTL_CREATE_TAXPAYER_INFO_NODE(p_doc, v_itemNode, '��������', p_invoice_row.broker_inn);
  UTL_CREATE_OPER_CODE_NODES(p_doc, v_itemNode, '����������', p_invoice_row.ROW_KEY);

  return true;
  exception when others then
    /*log error*/
    UTL_LOG('CLAIM_BUILD_T1_4', sqlcode, substr(sqlerrm, 0, 256));
    return false;
end;

/* ############################ */
/* # ������������ ������ 1.5 ## */
/* ############################ */
FUNCTION CLAIM_BUILD_T1_5
(
   p_doc in xmldom.DOMDocument,
   p_container_node in xmldom.DOMNode,
   p_invoice_row in sov_invoice_all%rowtype
)
return boolean
as
  v_itemElement xmldom.DOMElement;
  v_itemNode xmldom.DOMNode;
begin
  /* ���� ������             */
  v_itemElement := xmldom.createElement(p_doc, 'T1.5���');
  v_itemNode := xmldom.appendChild(p_container_node, xmldom.makeNode(v_itemElement));

  /* ������������ ��������   */
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '������',         p_invoice_row.invoice_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '�������',        UTL_FORMAT_DATE(p_invoice_row.invoice_date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '��������������', UTL_FORMAT_DECIMAL(p_invoice_row.price_total));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '�����������',    UTL_FORMAT_DECIMAL(p_invoice_row.price_nds_buyer));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '��������������', UTL_FORMAT_DECIMAL(p_invoice_row.price_nds_total));

  /* �������������� �������� */
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '���',            p_invoice_row.okv_code);

  /* ��������                */
  UTL_CREATE_ERROR_NODES(p_doc, v_itemNode, p_invoice_row);
  UTL_CREATE_BUYER_INFO_NODES(p_doc, v_itemNode, '�������', p_invoice_row.ROW_KEY);

  return true;
  exception when others then
    /*log error*/
    UTL_LOG('CLAIM_BUILD_T1_5', sqlcode, substr(sqlerrm, 0, 256));
    return false;
end;

/* ############################ */
/* # ������������ ������ 1.6 ## */
/* ############################ */
FUNCTION CLAIM_BUILD_T1_6
(
   p_doc in xmldom.DOMDocument,
   p_container_node in xmldom.DOMNode,
   p_invoice_row in sov_invoice_all%rowtype
) return boolean
as
  v_itemElement xmldom.DOMElement;
  v_itemNode xmldom.DOMNode;

  v_buyer_element xmldom.DOMElement;
  v_buyer_node xmldom.DOMNode;

  v_info_element xmldom.DOMElement;
  v_info_node xmldom.DOMNode;

  v_invoice_element xmldom.DOMElement;
  v_invoice_node xmldom.DOMNode;

  v_name varchar2(128 char);
  v_last_name varchar2(128 char);
  v_patronymic varchar2(128 char);
begin
 /* if length(p_invoice_row.buyer_inn) not in (10, 12) then
    return true;
  end if;


  v_itemElement := xmldom.createElement(p_doc, 'T1.6���');
  v_itemNode := xmldom.appendChild(p_container_node, xmldom.makeNode(v_itemElement));


  v_buyer_element := xmldom.createElement(p_doc, '��������');
  v_buyer_node := xmldom.appendChild(v_itemNode, xmldom.makeNode(v_buyer_element));

  if length(p_invoice_row.buyer_inn) = 10 then
    v_info_element := xmldom.createElement(p_doc, '������');
    v_info_node := xmldom.appendChild(v_buyer_node, xmldom.makeNode(v_info_element));

    select name_full
    into v_name
    from v_egrn_ul
    where inn = p_invoice_row.buyer_inn
      and kpp = p_invoice_row.buyer_kpp;

    UTL_XML_APPEND_ATTRIBUTE(v_info_element, '����������', v_name);
    UTL_XML_APPEND_ATTRIBUTE(v_info_element, '�����', p_invoice_row.buyer_inn);
    UTL_XML_APPEND_ATTRIBUTE(v_info_element, '���', p_invoice_row.buyer_kpp);


  elsif length(p_invoice_row.buyer_inn) = 12 then
    v_info_element := xmldom.createElement(p_doc, '������');
    v_info_node := xmldom.appendChild(v_buyer_node, xmldom.makeNode(v_info_element));

    select first_name, patronymic, last_name
    into v_name, v_patronymic, v_last_name
    from v_egrn_ip
    where innfl = p_invoice_row.buyer_inn;

    UTL_XML_APPEND_ATTRIBUTE(v_info_element, '�������', v_last_name);
    UTL_XML_APPEND_ATTRIBUTE(v_info_element, '���', v_name);
    UTL_XML_APPEND_ATTRIBUTE(v_info_element, '��������', v_patronymic);
    UTL_XML_APPEND_ATTRIBUTE(v_info_element, '�����', p_invoice_row.buyer_inn);
  end if;

  for inv_row in
  (
    with t_invoices as
    (
      select
        case
          when dis.stage = 2 then dis.invoice_rk
          when dis.stage = 3 then dis.invoice_contractor_rk
        end as row_key,
        case
          when dis.stage = 2 then dis.decl_id
          when dis.stage = 3 then dis.decl_contractor_id
        end as decl_id
      from seod_data_queue queue
      inner join sov_discrepancy dis on dis.id = queue.discrepancy_id
      where queue.for_stage in (2, 3)
        and queue.ref_doc_id is null
    )
    select si.invoice_num, si.invoice_date
    from stage_invoice si
    inner join t_invoices on t_invoices.row_key = si.row_key
    inner join v$declaration_history decl on decl.id = t_invoices.decl_id
      and decl.declaration_version_id = si.declaration_version_id
      and si.buyer_inn = p_invoice_row.buyer_inn
      and (si.buyer_kpp = p_invoice_row.buyer_kpp or (si.buyer_kpp is null and p_invoice_row.buyer_kpp is null))
  )
  loop
    v_invoice_element := xmldom.createElement(p_doc, '������');
    v_invoice_node := xmldom.appendChild(v_itemNode, xmldom.makeNode(v_invoice_element));

    UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '������',      inv_row.invoice_num);
    UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '�������',     UTL_FORMAT_DATE(inv_row.invoice_date));
  end loop;

  return true;
  exception when others then*/
    /*log error*/
    UTL_LOG('CLAIM_BUILD_T1_6', sqlcode, substr(sqlerrm, 0, 256));
    return false;
end;

/* ############################ */
/* # ������������ ������ 1.7 ## */
/* ############################ */
FUNCTION CLAIM_BUILD_T1_7
(
   p_doc in xmldom.DOMDocument,
   p_container_node in xmldom.DOMNode,
   p_violation_info in varchar2,
   p_cr_code in varchar2,
   p_left_side in number,
   p_rigth_side in number
)
return boolean
as
  v_itemElement xmldom.DOMElement;
  v_itemNode xmldom.DOMNode;
begin
  /*���� ������ 1.7*/
  v_itemElement := xmldom.createElement(p_doc, 'T1.7���');
  v_itemNode := xmldom.appendChild(p_container_node, xmldom.makeNode(v_itemElement));
  /*������������*/
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '�����',       p_violation_info);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '�����',       p_cr_code);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '�������',     UTL_FORMAT_DECIMAL(p_left_side));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '��������',    UTL_FORMAT_DECIMAL(p_rigth_side));

  return true;
exception when others then
  /*log error*/
  UTL_LOG('CLAIM_BUILD_T1_7', sqlcode, substr(sqlerrm, 0, 256));
  return false;
end;

/* ############################ */
/* # ������������ ������ 1.8 ## */
/* ############################ */
FUNCTION CLAIM_BUILD_T1_8
(
   p_doc in xmldom.DOMDocument,
   p_container_node in xmldom.DOMNode,
   p_invoice_row in sov_invoice_all%rowtype
) return boolean
as
  v_itemElement xmldom.DOMElement;
  v_itemNode xmldom.DOMNode;
begin
  /* ���� ������             */
  v_itemElement := xmldom.createElement(p_doc, '�1.8���');
  v_itemNode := xmldom.appendChild(p_container_node, xmldom.makeNode(v_itemElement));

  /* ������������ ��������   */
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '��������',        p_invoice_row.ordinal_number);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '����������',      p_invoice_row.invoice_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '�����������',     UTL_FORMAT_DECIMAL(p_invoice_row.price_buy_amount));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '���������',       UTL_FORMAT_DECIMAL(p_invoice_row.price_buy_nds_amount));

  /* �������������� �������� */
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '�����������',     UTL_FORMAT_DATE(p_invoice_row.invoice_date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '����������',      p_invoice_row.change_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '�����������',     UTL_FORMAT_DATE(p_invoice_row.change_date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '�����������',     p_invoice_row.correction_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '������������',    UTL_FORMAT_DATE(p_invoice_row.correction_date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '�����������',     p_invoice_row.change_correction_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '������������',    UTL_FORMAT_DATE(p_invoice_row.change_correction_date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '�����',           p_invoice_row.customs_declaration_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '���',             p_invoice_row.okv_code);

  /* ��������                */
  UTL_CREATE_ERROR_NODES(p_doc, v_itemNode, p_invoice_row);
  UTL_CREATE_SELLER_INFO_NODES(p_doc, v_itemNode, '������', p_invoice_row.ROW_KEY);
  UTL_CREATE_TAXPAYER_INFO_NODE(p_doc, v_itemNode, '�����',  p_invoice_row.broker_inn);
  UTL_CREATE_DOC_NODES(p_doc, v_itemNode, '����������', '�������������', '��������������', p_invoice_row.ROW_KEY);
  UTL_CREATE_OPER_CODE_NODES(p_doc, v_itemNode, '����������', p_invoice_row.ROW_KEY);
  UTL_CREATE_BUY_ACCEPT_NODES(p_doc, v_itemNode, '���������',  p_invoice_row.ROW_KEY);

  return true;
  exception when others then
    /*log error*/
    UTL_LOG('CLAIM_BUILD_T1_8', sqlcode, substr(sqlerrm, 0, 256));
    return false;
end;

/* ############################ */
/* # ������������ ������ 1.9 ## */
/* ############################ */
FUNCTION CLAIM_BUILD_T1_9
(
   p_doc in xmldom.DOMDocument,
   p_container_node in xmldom.DOMNode,
   p_invoice_row in sov_invoice_all%rowtype
) return boolean
as
  v_itemElement xmldom.DOMElement;
  v_itemNode xmldom.DOMNode;
begin
  /* ���� ������             */
  v_itemElement := xmldom.createElement(p_doc, '�1.9���');
  v_itemNode := xmldom.appendChild(p_container_node, xmldom.makeNode(v_itemElement));

  /* ������������ ��������   */
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '��������',       p_invoice_row.ordinal_number);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '����������',     p_invoice_row.invoice_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '�����������',    UTL_FORMAT_DATE(p_invoice_row.invoice_date));

  /* �������������� �������� */
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '����������',     p_invoice_row.change_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '�����������',    UTL_FORMAT_DATE(p_invoice_row.change_date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '�����������',    p_invoice_row.correction_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '������������',   UTL_FORMAT_DATE(p_invoice_row.correction_date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '�����������',    p_invoice_row.change_correction_num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '������������',   UTL_FORMAT_DATE(p_invoice_row.change_correction_date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '���',            p_invoice_row.okv_code);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '������������',   UTL_FORMAT_DECIMAL(p_invoice_row.price_sell_in_curr));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '�����������18',  UTL_FORMAT_DECIMAL(p_invoice_row.price_sell_18));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '�����������10',  UTL_FORMAT_DECIMAL(p_invoice_row.price_sell_10));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '�����������0',   UTL_FORMAT_DECIMAL(p_invoice_row.price_sell_0));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '��������18',     UTL_FORMAT_DECIMAL(p_invoice_row.price_nds_18));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '��������10',     UTL_FORMAT_DECIMAL(p_invoice_row.price_nds_10));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, '������������',   UTL_FORMAT_DECIMAL(p_invoice_row.price_tax_free));

  /* ��������                */
  UTL_CREATE_ERROR_NODES(p_doc, v_itemNode, p_invoice_row);
  UTL_CREATE_BUYER_INFO_NODES(p_doc, v_itemNode, '�������', p_invoice_row.ROW_KEY);
  UTL_CREATE_TAXPAYER_INFO_NODE(p_doc, v_itemNode, '�����', p_invoice_row.broker_inn);
  UTL_CREATE_DOC_NODES(p_doc, v_itemNode, '����������', '�������������', '��������������', p_invoice_row.ROW_KEY);
  UTL_CREATE_OPER_CODE_NODES(p_doc, v_itemNode, '����������', p_invoice_row.ROW_KEY);

  return true;
  exception when others then
    /*log error*/
    UTL_LOG('CLAIM_BUILD_T1_9', sqlcode, substr(sqlerrm, 0, 256));
    return false;
end;

FUNCTION RECLAIM_BUILD
(
   p_doc in xmldom.DOMDocument,
   p_container_node in xmldom.DOMNode,
   p_invoice_row in stage_invoice%rowtype,
   p_tax_period in varchar2,
   p_tax_year in varchar2,
   p_element_name in varchar2
)
return boolean
is
  v_itemElement xmldom.DOMElement;
  v_itemNode xmldom.DOMNode;

  v_invoiceElement xmldom.DOMElement;
  v_invoiceNode xmldom.DOMNode;

  v_dateElement xmldom.DOMElement;
  v_dateNode xmldom.DOMNode;

  v_startPeriod varchar2(10 CHAR);
  v_endPeriod varchar2(10 CHAR);

  v_textElement  xmldom.DOMText;
  v_textNode xmldom.DOMNode;
begin
  /*���� ������*/
  v_itemElement := xmldom.createElement(p_doc, p_element_name);
  v_itemNode := xmldom.appendChild(p_container_node, xmldom.makeNode(v_itemElement));
  /*������������ ��������*/
  UTL_GET_TAXPERIOD_BOUNDS(p_tax_period, p_tax_year, v_startPeriod, v_endPeriod);
  if p_invoice_row.correction_num is null then
     v_invoiceElement := xmldom.createElement(p_doc, '���');
     v_invoiceNode := xmldom.appendChild(v_itemNode, xmldom.makeNode(v_invoiceElement));

     UTL_XML_APPEND_ATTRIBUTE(v_invoiceElement, '������', '2772');
     UTL_XML_APPEND_ATTRIBUTE(v_invoiceElement, '������', p_invoice_row.correction_num);

     if p_invoice_row.correction_date is null then
       v_dateElement := xmldom.createElement(p_doc, '���������');
       v_dateNode := xmldom.appendChild(v_invoiceNode, xmldom.makeNode(v_dateElement));

       UTL_XML_APPEND_ATTRIBUTE(v_dateElement, '������', v_startPeriod);
       UTL_XML_APPEND_ATTRIBUTE(v_dateElement, '�������', v_endPeriod);
     else
       v_dateElement := xmldom.createElement(p_doc, '�������');
       v_dateNode := xmldom.appendChild(v_invoiceNode, xmldom.makeNode(v_dateElement));

       v_textElement := xmldom.createTextNode(p_doc, UTL_FORMAT_DATE(p_invoice_row.correction_date));
       v_textNode := xmldom.appendChild(v_dateNode, xmldom.makeNode(v_textElement));
     end if;
  else
     v_invoiceElement := xmldom.createElement(p_doc, '��');
     v_invoiceNode := xmldom.appendChild(v_itemNode, xmldom.makeNode(v_invoiceElement));

     UTL_XML_APPEND_ATTRIBUTE(v_invoiceElement, '������', '0924');
     UTL_XML_APPEND_ATTRIBUTE(v_invoiceElement, '������', p_invoice_row.invoice_num);

     if p_invoice_row.invoice_date is null then
       v_dateElement := xmldom.createElement(p_doc, '���������');
       v_dateNode := xmldom.appendChild(v_invoiceNode, xmldom.makeNode(v_dateElement));

       UTL_XML_APPEND_ATTRIBUTE(v_dateElement, '������', v_startPeriod);
       UTL_XML_APPEND_ATTRIBUTE(v_dateElement, '�������', v_endPeriod);
     else
       v_dateElement := xmldom.createElement(p_doc, '�������');
       v_dateNode := xmldom.appendChild(v_invoiceNode, xmldom.makeNode(v_dateElement));

       v_textElement := xmldom.createTextNode(p_doc, UTL_FORMAT_DATE(p_invoice_row.invoice_date));
       v_textNode := xmldom.appendChild(v_dateNode, xmldom.makeNode(v_textElement));
     end if;
  end if;

  return true;

  exception when others then
    return false;
end;

PROCEDURE CREATE_DECLARATION_CLAIM
(
  p_sono_code in varchar2,
  p_seod_decl_regnum in number,
  p_decl_id in number,
  p_claim_mode in number := 1, /*1 - ��, 2 - ��*/
  p_for_stage in number := 0
)
as
  doc xmldom.DOMDocument;
  docXml xmltype;
  docXmlValidation xmltype;

  main_node xmldom.DOMNode;
  root_node xmldom.DOMNode;
  user_node xmldom.DOMNode;
  item_node xmldom.DOMNode;
  root_elmt xmldom.DOMElement;
  item_elmt xmldom.DOMElement;
  item_text xmldom.DOMText;
  discrepElement xmldom.DOMElement;
  discrepNode xmldom.DOMNode;

  v_T11_ListElement xmldom.DOMElement;
  v_T11_ListNode xmldom.DOMNode;
  v_T12_ListElement xmldom.DOMElement;
  v_T12_ListNode xmldom.DOMNode;
  v_T13_ListElement xmldom.DOMElement;
  v_T13_ListNode xmldom.DOMNode;
  v_T14_ListElement xmldom.DOMElement;
  v_T14_ListNode xmldom.DOMNode;
  v_T15_ListElement xmldom.DOMElement;
  v_T15_ListNode xmldom.DOMNode;
  v_T16_ListElement xmldom.DOMElement;
  v_T16_ListNode xmldom.DOMNode;
  v_T17_ListElement xmldom.DOMElement;
  v_T17_ListNode xmldom.DOMNode;
  v_T18_ListElement xmldom.DOMElement;
  v_T18_ListNode xmldom.DOMNode;
  v_T19_ListElement xmldom.DOMElement;
  v_T19_ListNode xmldom.DOMNode;

  v_itemNode xmldom.DOMNode;

  v_is_T16 number(1);
  v_count_of_processed_data number(19) := 0;
  v_document_id number(19);
  v_status number(2) := 1;
  v_doc_type number(1)  := 0;
  v_success_operation boolean := false;
  v_deadline_date date;
  f_block number(1) := 0;
begin
  v_document_id := SEQ_DOC.NEXTVAL;
  v_deadline_date := UTL_GET_DEADLINE_CLAIM;

  doc := xmldom.newDOMDocument();
  main_node := xmldom.makeNode(doc);
  root_elmt := xmldom.createElement(doc, '��������������');
  xmldom.setAttribute(root_elmt, '���������', v_document_id);
  xmldom.setAttribute(root_elmt, '������', 'TAX3EXCH_NDS2_CAM_01');
  xmldom.setAttribute(root_elmt, '���������', p_seod_decl_regnum);
  xmldom.setAttribute(root_elmt, '�����', p_sono_code);
  xmldom.setAttribute(root_elmt, 'xsi:noNamespaceSchemaLocation', XSD_NDS2_CAM_01);
  xmldom.setAttribute(root_elmt, 'xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
  root_node := xmldom.appendChild(main_node, xmldom.makeNode(root_elmt));

  discrepElement := xmldom.createElement(doc, '����������');
  discrepNode := xmldom.appendChild(root_node, xmldom.makeNode(discrepElement));

  v_T11_ListElement := xmldom.createElement(doc, 'T1.1');
  v_T11_ListNode := xmldom.appendChild(discrepNode, xmldom.makeNode(v_T11_ListElement));

  v_T12_ListElement := xmldom.createElement(doc, 'T1.2');
  v_T12_ListNode := xmldom.appendChild(discrepNode, xmldom.makeNode(v_T12_ListElement));

  v_T13_ListElement := xmldom.createElement(doc, 'T1.3');
  v_T13_ListNode := xmldom.appendChild(discrepNode, xmldom.makeNode(v_T13_ListElement));

  v_T14_ListElement := xmldom.createElement(doc, 'T1.4');
  v_T14_ListNode := xmldom.appendChild(discrepNode, xmldom.makeNode(v_T14_ListElement));

  v_T15_ListElement := xmldom.createElement(doc, 'T1.5');
  v_T15_ListNode := xmldom.appendChild(discrepNode, xmldom.makeNode(v_T15_ListElement));

  v_T16_ListElement := xmldom.createElement(doc, 'T1.6');
  v_T16_ListNode := xmldom.appendChild(discrepNode, xmldom.makeNode(v_T16_ListElement));

  v_T17_ListElement := xmldom.createElement(doc, 'T1.7');
  v_T17_ListNode := xmldom.appendChild(discrepNode, xmldom.makeNode(v_T17_ListElement));

  v_T18_ListElement := xmldom.createElement(doc, '�1.8');
  v_T18_ListNode := xmldom.appendChild(discrepNode, xmldom.makeNode(v_T18_ListElement));

  v_T19_ListElement := xmldom.createElement(doc, '�1.9');
  v_T19_ListNode := xmldom.appendChild(discrepNode, xmldom.makeNode(v_T19_ListElement));
/*��������� ������ ������*/
utl_log(p_claim_mode);
if p_claim_mode = 1 then
    v_doc_type := 1;
    utl_log('0');
    v_status := -1;
    for discrepancy_invoice_line in
    (
      select
      distinct
        sia.*
      from selection s
      inner join selection_discrepancy sd on sd.selection_id = s.id
      inner join sov_invoice_all sia on (sd.invoice_rk like '%A%' and sia.actual_row_key = sd.invoice_rk) or sia.row_key = sd.invoice_rk
      where s.status = 10 and sd.decl_id = p_decl_id and sd.is_in_process = 1
    )
    loop
      if   discrepancy_invoice_line.row_key like '%A%' then
        continue;
      end if;

      v_count_of_processed_data := v_count_of_processed_data + 1;
/*
      select case when count(1) > 0 then 1 else 0 end
      into v_is_T16
      from sov_discrepancy dis
      where dis.rule_num in (9, 10, 11, 12, 13, 14, 15, 16)
        and discrepancy_invoice_line.row_key =
          case
            when dis.stage = 2 then invoice_rk
            when dis.stage = 3 then invoice_contractor_rk
          end;
*/

      if discrepancy_invoice_line.chapter = 8 then
          v_success_operation := CLAIM_BUILD_T1_1(doc, v_T11_ListNode, discrepancy_invoice_line);
      end if;

      if discrepancy_invoice_line.chapter = 81 then
          v_success_operation := CLAIM_BUILD_T1_8(doc, v_T18_ListNode, discrepancy_invoice_line);
      end if;

      if discrepancy_invoice_line.chapter = 9 then
        if v_is_T16 = 1 then
          v_success_operation := CLAIM_BUILD_T1_6(doc, v_T16_ListNode, discrepancy_invoice_line);
        else
          v_success_operation := CLAIM_BUILD_T1_2(doc, v_T12_ListNode, discrepancy_invoice_line);
        end if;
      end if;

      if discrepancy_invoice_line.chapter = 91 then
        if v_is_T16 = 1 then
          v_success_operation := CLAIM_BUILD_T1_6(doc, v_T16_ListNode, discrepancy_invoice_line);
        else
          v_success_operation := CLAIM_BUILD_T1_9(doc, v_T19_ListNode, discrepancy_invoice_line);
        end if;
      end if;

      if discrepancy_invoice_line.chapter = 10 then
        if v_is_T16 = 1 then
          v_success_operation := CLAIM_BUILD_T1_6(doc, v_T16_ListNode, discrepancy_invoice_line);
        else
          v_success_operation := CLAIM_BUILD_T1_3(doc, v_T13_ListNode, discrepancy_invoice_line);
        end if;
      end if;

      if discrepancy_invoice_line.chapter = 11 then
        v_success_operation := CLAIM_BUILD_T1_4(doc, v_T14_ListNode, discrepancy_invoice_line);
      end if;

      if discrepancy_invoice_line.chapter = 12 then
        if v_is_T16 = 1 then
          v_success_operation := CLAIM_BUILD_T1_6(doc, v_T16_ListNode, discrepancy_invoice_line);
        else
          v_success_operation := CLAIM_BUILD_T1_5(doc, v_T15_ListNode, discrepancy_invoice_line);
        end if;
      end if;

      insert into doc_invoice(doc_id, invoice_row_key, invoice_chapter)
        values (v_document_id, discrepancy_invoice_line.row_key, discrepancy_invoice_line.chapter);

      if not v_success_operation then
        exit;
      end if;
    end loop;
end if;

/*��������� ����������� �����������*/
if p_claim_mode = 2 then
   v_doc_type := 5;
   for ksLine in
   (
     select
       vw.*,
       decode(vw.Vypoln, 1, 10, 0, 12, vw.Vypoln) as vypolnCode
     from V$ASKKontrSoontosh vw
     where vw.iddekl = p_decl_id
   )
   loop
     v_count_of_processed_data := v_count_of_processed_data + 1;
     v_success_operation := CLAIM_BUILD_T1_7(doc, v_T17_ListNode, ksLine.vypolnCode, ksLine.KodKs, ksLine.LevyaChast, ksLine.PravyaChast);

     insert into CONTROL_RATIO_DOC values(ksLine.Id, v_document_id, sysdate);

     if not v_success_operation then
       dbms_output.put_line('errors');
       exit;
     end if;
   end loop;
end if;
  --UTL_LOG('CREATE_DECLARATION_CLAIM - processed '||v_count_of_processed_data||' items');

  if v_count_of_processed_data > 0 then
    --UTL_LOG('PROCESSED - '||v_count_of_processed_data || ' invoices for declaration ' || p_seod_decl_regnum);
    UTL_REMOVE_IF_EMPTY(discrepNode, v_T11_ListNode);
    UTL_REMOVE_IF_EMPTY(discrepNode, v_T12_ListNode);
    UTL_REMOVE_IF_EMPTY(discrepNode, v_T13_ListNode);
    UTL_REMOVE_IF_EMPTY(discrepNode, v_T14_ListNode);
    UTL_REMOVE_IF_EMPTY(discrepNode, v_T15_ListNode);
    UTL_REMOVE_IF_EMPTY(discrepNode, v_T16_ListNode);
    UTL_REMOVE_IF_EMPTY(discrepNode, v_T17_ListNode);
    UTL_REMOVE_IF_EMPTY(discrepNode, v_T18_ListNode);
    UTL_REMOVE_IF_EMPTY(discrepNode, v_T19_ListNode);

    begin
      docXml := dbms_xmldom.getXmlType(doc);
      docXmlValidation := xmltype(docXml.getClobVal());
      if docXmlValidation.isSchemaValid(XSD_NDS2_CAM_01) = 0 then
        v_status := 3;
      end if;
      exception when others then
        v_status := 3;
        UTL_LOG('CREATE_DECLARATION_CLAIM:', -1, 'XSD load error', v_document_id);
      end;
    if p_for_stage = 2 then
    begin
      select count(*) into f_block from CONFIGURATION
      where PARAMETER = 'send_autoclame_onepart' and VALUE = 'N';
    exception
      when NO_DATA_FOUND then f_block := 0;
    end;
    if f_block > 0 then
      v_status := 11;
    end if;
  end if;

  if p_for_stage = 3 then
    begin
      select count(*) into f_block from CONFIGURATION
      where PARAMETER = 'send_autoclame_twopart' and VALUE = 'N';
    exception
      when NO_DATA_FOUND then f_block := 0;
    end;
    if f_block > 0 then
      v_status := 11;
    end if;
  end if;
--UTL_LOG('100');

    insert into doc(doc_id, ref_entity_id, doc_type, doc_kind, sono_code, create_date, status, document_body, deadline_date)
    values (v_document_id, p_seod_decl_regnum, v_doc_type, 1, p_sono_code, sysdate, v_status, replace(docXml.getClobVal(), XSD_NDS2_CAM_01, 'TAX3EXCH_NDS2_CAM_01_01.xsd'), v_deadline_date);

    update CONTROL_RATIO_QUEUE
     set is_sent = 1
     where id = p_decl_id;

    xmldom.freeDocument(doc);
    dbms_session.modify_package_state(dbms_session.free_all_resources);
  end if;
end;

PROCEDURE CREATE_DECLARATION_RECLAIM
(
  p_decl_version_id in number,
  p_sono_code in varchar2,
  p_seod_decl_regnum in number,
  p_knp_closed in number,
  p_for_stage in number := 0
)
as
  doc xmldom.DOMDocument;
  docXml xmltype;
  docXmlValidation xmltype;
  main_node xmldom.DOMNode;
  root_node xmldom.DOMNode;
  user_node xmldom.DOMNode;
  item_node xmldom.DOMNode;
  root_elmt xmldom.DOMElement;
  source_elmt xmldom.DOMElement;
  source_node xmldom.DOMNode;
  item_elmt xmldom.DOMElement;
  item_text xmldom.DOMText;
  taxpr_node xmldom.DOMNode;
  taxpr_elmt xmldom.DOMElement;
  fio_node xmldom.DOMNode;
  fio_elmt xmldom.DOMElement;

  v_decl_period varchar2(2 char);
  v_decl_year varchar2(4 char);
  v_document_id number;

  v_taxpr_id number := 0;
  v_taxpr_inn varchar2(12 char);
  v_taxpr_kpp varchar2(9 char);
  v_taxpr_code_no varchar2(4 char);

  v_ul_name varchar2(1000 char);
  v_ip_first_name varchar2(60 char);
  v_ip_patronymic varchar2(60 char);
  v_ip_last_name varchar2(60 char);

  v_reclaim_elem_name varchar2(32 char);
  v_schema_name varchar2(128 char);
  v_doc_type number(2);
  v_status number(2) := 1;
  v_deadline_date date;
  f_block number(1) := 0;
begin
  v_deadline_date := UTL_GET_DEADLINE_RECLAIM;
  v_document_id := SEQ_DOC.NEXTVAL;
  doc := xmldom.newDOMDocument();
  main_node := xmldom.makeNode(doc);
  if p_knp_closed = 0 then
    root_elmt := xmldom.createElement(doc, '������93');
    xmldom.setAttribute(root_elmt, '�����', p_sono_code);
    xmldom.setAttribute(root_elmt, '���������', p_seod_decl_regnum);
    xmldom.setAttribute(root_elmt, '�����������', v_document_id);
    xmldom.setAttribute(root_elmt, '������', 'TAX3EXCH_NDS2_CAM_02');
    xmldom.setAttribute(root_elmt, 'xsi:noNamespaceSchemaLocation', 'TAX3EXCH_NDS2_CAM_02_01.xsd');
    xmldom.setAttribute(root_elmt, 'xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
    root_node := xmldom.appendChild(main_node, xmldom.makeNode(root_elmt));

    v_reclaim_elem_name := '������93���';
    v_schema_name := XSD_NDS2_CAM_02;
    v_doc_type := 2;
  else
    select ul.id, ul.inn, ul.kpp, ul.name_full, ul.code_no
    into v_taxpr_id, v_taxpr_inn, v_taxpr_kpp, v_ul_name, v_taxpr_code_no
    from v$declaration hist_decl
    inner join v$egrn_ul ul on ul.inn = hist_decl.inn and ul.kpp = hist_decl.kpp
    where hist_decl.declaration_version_id = p_decl_version_id;

    if v_taxpr_id = 0 then
      select ip.id, ip.inn, ip.first_name, ip.patronymic, ip.last_name, ip.code_no
      into v_taxpr_id, v_taxpr_inn, v_ip_first_name, v_ip_patronymic, v_ip_last_name, v_taxpr_code_no
      from v$declaration hist_decl
      inner join v$egrn_ip ip on ip.inn = hist_decl.inn and hist_decl.kpp is null
      where hist_decl.declaration_version_id = p_decl_version_id;
    end if;

    root_elmt := xmldom.createElement(doc, '������93.1');
    xmldom.setAttribute(root_elmt, '�����', p_sono_code);
    xmldom.setAttribute(root_elmt, '���������', p_seod_decl_regnum);
    xmldom.setAttribute(root_elmt, '�����������', v_document_id);
    xmldom.setAttribute(root_elmt, '�����������', v_taxpr_code_no);
    xmldom.setAttribute(root_elmt, '������', 'TAX3EXCH_NDS2_CAM_03');
    xmldom.setAttribute(root_elmt, 'xsi:noNamespaceSchemaLocation', 'TAX3EXCH_NDS2_CAM_03_01.xsd');
    xmldom.setAttribute(root_elmt, 'xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
    root_node := xmldom.appendChild(main_node, xmldom.makeNode(root_elmt));

    source_elmt := xmldom.createElement(doc, '�������');
    source_node := xmldom.appendChild(root_node, xmldom.makeNode(source_elmt));
    if v_taxpr_kpp is not null then
      taxpr_elmt := xmldom.createElement(doc, '������');
      xmldom.setAttribute(taxpr_elmt, '�����', v_taxpr_inn);
      xmldom.setAttribute(taxpr_elmt, '���', v_taxpr_kpp);
      xmldom.setAttribute(taxpr_elmt, '�������', v_ul_name);
      taxpr_node := xmldom.appendChild(source_node, xmldom.makeNode(taxpr_elmt));
    else
      taxpr_elmt := xmldom.createElement(doc, '�����');
      xmldom.setAttribute(taxpr_elmt, '�����', v_ul_name);
      taxpr_node := xmldom.appendChild(source_node, xmldom.makeNode(taxpr_elmt));

      fio_elmt := xmldom.createElement(doc, '���');
      xmldom.setAttribute(fio_elmt, '�������', v_taxpr_inn);
      xmldom.setAttribute(fio_elmt, '���', v_taxpr_kpp);
      xmldom.setAttribute(fio_elmt, '��������', v_ul_name);
      fio_node := xmldom.appendChild(taxpr_node, xmldom.makeNode(fio_elmt));

    end if;
    v_reclaim_elem_name := '������93.1���';
    v_schema_name := XSD_NDS2_CAM_03;
    v_doc_type := 3;
  end if;

  select t.PERIOD, t.OTCHETGOD
  into v_decl_period, v_decl_year
  from v$askdekl t
  where t.zip = p_decl_version_id;

  for invLine in
  (
    with t_invoices as
    (
      select
        case
          when (queue.for_stage = 4 and p_knp_closed = 0) or (queue.for_stage = 5 and p_knp_closed = 1) then dis.invoice_rk
          when (queue.for_stage = 5 and p_knp_closed = 0) or (queue.for_stage = 4 and p_knp_closed = 1) then dis.invoice_contractor_rk
        end as row_key
      from seod_data_queue queue
      inner join sov_discrepancy dis on dis.id = queue.discrepancy_id
      where queue.declaration_reg_num = p_seod_decl_regnum
          and queue.for_stage in (4, 5)
          and queue.ref_doc_id is null
    )
    select distinct si.*
    from stage_invoice si
    inner join t_invoices on t_invoices.row_key = si.row_key
  )
  loop
    if RECLAIM_BUILD(doc, root_node, invLine, v_decl_period, v_decl_year, v_reclaim_elem_name) then
      insert into doc_invoice(doc_id, invoice_row_key, invoice_chapter) values(v_document_id, invLine.Row_Key, invLine.Chapter);

      update seod_data_queue queue
      set queue.ref_doc_id = v_document_id
      where queue.ref_doc_id is null
        and queue.for_stage in (4, 5)
        and queue.discrepancy_id in
        (
          select id
          from sov_discrepancy
          where invLine.row_key =
            case
              when (queue.for_stage = 4 and p_knp_closed = 0) or (queue.for_stage = 5 and p_knp_closed = 1) then invoice_rk
              when (queue.for_stage = 5 and p_knp_closed = 0) or (queue.for_stage = 4 and p_knp_closed = 1) then invoice_contractor_rk
            end
        );
    else
      exit;
    end if;
  end loop;

  docXml := dbms_xmldom.getXmlType(doc);
  docXmlValidation := xmltype(docXml.getClobVal());

  if docXmlValidation.isSchemaValid(v_schema_name) = 0 then
     v_status := 3;
     UTL_LOG('CREATE_DECLARATION_RECLAIM:', -1, 'XSD validation error', v_document_id);
  end if;

  if p_for_stage = 4 then
  begin
    select count(*) into f_block from CONFIGURATION
    where PARAMETER = 'send_autoreclame_onepart' and VALUE = 'N';
  exception
    when NO_DATA_FOUND then f_block := 0;
  end;
  if f_block > 0 then
    v_status := 11;
  end if;
  end if;

  if p_for_stage = 5 then
  begin
    select count(*) into f_block from CONFIGURATION
    where PARAMETER = 'send_autoreclame_twopart' and VALUE = 'N';
  exception
    when NO_DATA_FOUND then f_block := 0;
  end;
  if f_block > 0 then
    v_status := 11;
  end if;
  end if;


  insert into doc(doc_id, ref_entity_id, doc_type, doc_kind, sono_code, create_date, status, document_body, deadline_date)
  values (v_document_id, p_seod_decl_regnum, v_doc_type, v_doc_type, p_sono_code, sysdate, v_status, docXml.getClobVal(), v_deadline_date);

  xmldom.freeDocument(doc);
  dbms_session.modify_package_state(dbms_session.free_all_resources);
end;

/*######################################################*/
/*���� ������ ��� �������� �������������� ������ �������*/
/*######################################################*/

PROCEDURE COLLECT_QUEUE
as
pragma autonomous_transaction;
v_count number := 0;
begin

/*������� ���������� �� ������������ �������� � �������*/
  merge into ASK_INVOICE_DECL_QUEUE Q
  using
  (select
  distinct
    sd.decl_id
  , mdh.seod_decl_id
  , mdh.soun_code
  , mdh.kpp
  , 2 for_stage
  , 0 is_processed
  from
  selection_discrepancy sd
  inner join selection s on s.id = sd.selection_id
  inner join mv$declaration_history mdh on sd.decl_id = mdh.id and mdh.is_active = 1
  where s.status = 4 and sd.is_in_process = 1) T
  on (T.DECL_ID = Q.DECL_ID and T.SEOD_DECL_ID = Q.SEOD_DECL_ID and T.SOUN_CODE = Q.SOUN_CODE and nvl(T.KPP, '-') = nvl(Q.KPP, '-') and T.FOR_STAGE = Q.FOR_STAGE)
  when not matched then
  insert values(T.Decl_Id, T.SEOD_DECL_ID, T.SOUN_CODE, T.KPP, T.FOR_STAGE, T.IS_PROCESSED, sysdate);

/*������� ������� ������� ��� ���������� ��������� ������������ �� �� ��*/

  update selection s
  set status = 10
  where s.status = 4 and not exists
  (
   select 1 from selection_discrepancy sd
   left join  ASK_INVOICE_DECL_QUEUE aidq on sd.decl_id = aidq.decl_id
   where sd.selection_id = s.id and sd.is_in_process = 1 and aidq.decl_id is null
  );

  commit;
  exception when others then
    rollback;
    UTL_LOG('COLLECT_QUEUE failed', sqlcode, substr(sqlerrm, 256));
end;

PROCEDURE SEND_CLAIM_KS
as
pragma autonomous_transaction;
begin
  --UTL_LOG('PROCESS_QUEUE - control ratio');
  for ready_cr_decl in
  (
      select *
      from
      (select
         q.id,
         q.sono_code,
         q.decl_reg_num
      from CONTROL_RATIO_QUEUE q
      where q.is_sent = 0
      order by id)
      where rownum < 5000
  )
  loop
    --dbms_output.put_line('claim');
    CREATE_DECLARATION_CLAIM
    (
      ready_cr_decl.sono_code,
      ready_cr_decl.decl_reg_num,
      ready_cr_decl.id,
      2
    );
  end loop;
  commit;
end;

PROCEDURE PROCESS_QUEUE
as
pragma autonomous_transaction;
begin
  UTL_LOG('PROCESS_QUEUE - discrepancies');
  for decl_line in
  (
    select * from
    (
    select
    t.decl_id,
    t.seod_decl_id,
    t.soun_code,
    t.kpp,
    t.for_stage,
    case when knp.completion_date is not null then 1 else 0 end as knp_closed
    from
    ASK_INVOICE_DECL_QUEUE t
    left join seod_knp knp on knp.declaration_reg_num = t.seod_decl_id and knp.ifns_code = t.soun_code
    where t.for_stage = 2 and t.is_processed = 0
    order by t.create_date
    ) T where rownum < 100
  )
  loop
    if decl_line.knp_closed = 0 then
    CREATE_DECLARATION_CLAIM
    (
      decl_line.soun_code,
      decl_line.seod_decl_id,
      decl_line.decl_id,
      1
    );
    end if;
    update ASK_INVOICE_DECL_QUEUE set is_processed = 1 where
    decl_id = decl_line.decl_id and
    seod_decl_id = decl_line.seod_decl_id and
    soun_code = decl_line.soun_code and
    nvl(kpp, '-') = nvl(decl_line.kpp, '-');
  end loop;

  commit;
  UTL_LOG('PROCESS_QUEUE - complete');
  exception when others then
    rollback;
    UTL_LOG('PROCESS_QUEUE failed', sqlcode, substr(sqlerrm, 256));
  null;
end;

PROCEDURE PROCESS_ASK_4
as
  doc xmldom.DOMDocument;
  docXml xmltype;
  main_node xmldom.DOMNode;
  root_node xmldom.DOMNode;
  user_node xmldom.DOMNode;
  item_node xmldom.DOMNode;
  root_elmt xmldom.DOMElement;
  item_elmt xmldom.DOMElement;
  item_text xmldom.DOMText;
  docXmlValidation xmltype;
  v_status number(1):= 1;
  pragma autonomous_transaction;
begin
  for line in (select
  dh.soun_code,
  dh.SEOD_DECL_ID
    from v$declaration dh
    left join doc d on d.ref_entity_id = dh.SEOD_DECL_ID and d.doc_type = 4
    where dh.TOTAL_DISCREP_COUNT = 0
    and dh.CONTROL_RATIO_COUNT = 0
    and to_number(nvl(dh.CORRECTION_NUMBER, '0')) > 0
    and d.doc_id is null)
  loop
    doc := xmldom.newDOMDocument();
    main_node := xmldom.makeNode(doc);
    root_elmt := xmldom.createElement(doc, '�����������');
    xmldom.setAttribute(root_elmt, '�����', line.soun_code);
    xmldom.setAttribute(root_elmt, '���������', line.SEOD_DECL_ID);
    xmldom.setAttribute(root_elmt, '������', 'TAX3EXCH_NDS2_CAM_04');
    xmldom.setAttribute(root_elmt, '����������', 2);
    xmldom.setAttribute(root_elmt, 'xsi:noNamespaceSchemaLocation', 'TAX3EXCH_NDS2_CAM_04_01.xsd');
    xmldom.setAttribute(root_elmt, 'xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
    root_node := xmldom.appendChild(main_node, xmldom.makeNode(root_elmt));

    docXml := dbms_xmldom.getXmlType(doc);
    docXmlValidation := xmltype(docXml.getClobVal());

    if docXmlValidation.isSchemaValid(schurl => XSD_NDS2_CAM_04) = 0 then
      v_status := 3;
    end if;

    insert into doc(doc_id, ref_entity_id, doc_type, doc_kind, sono_code, create_date, status, document_body)
    values (SEQ_DOC.NEXTVAL, line.SEOD_DECL_ID, 4, 4, line.soun_code, sysdate, v_status, docXml.getClobVal());

  end loop;
  commit;
  xmldom.freeDocument(doc);
  dbms_session.modify_package_state(dbms_session.free_all_resources);
exception when others then
  rollback;
  dbms_output.put_line('PROCESS_ASK_4:' || sqlcode || '-' || substr(sqlerrm, 0, 256));
end;

procedure CLOSE_EXPIRED_DOCUMENTS
as
begin
  for doc_row in
  (
    select ref_entity_id, deadline_date
    from doc
    where deadline_date < sysdate
      and status <> 10
  )
  loop
    UTL_CLOSE_DOCUMENT(doc_row.ref_entity_id, doc_row.deadline_date);
  end loop;
end;

PROCEDURE DEMO
as
begin
--  Nds2$Selections.MERGE_SENDED_SELECTIONS;
--  CLOSE_EXPIRED_DOCUMENTS;
  COLLECT_QUEUE;
  PROCESS_QUEUE;
--  PROCESS_ASK_4;
end;
END;
/
