﻿CREATE OR REPLACE PACKAGE NDS2_MRR_USER.NDS2$DISCREPANCIES is

  procedure SaveDiscrepancyFilter
    (
      p_Cd in Discrepancies_Filters.Cd%type,
      p_UserName in Discrepancies_Filters.User_Name%type,
      p_IsFavourite in Discrepancies_Filters.Is_Favourite%type,
      p_Name in Discrepancies_Filters.Name%type,
      p_Data in Discrepancies_Filters.Data%type,
      p_Id out Discrepancies_Filters.Id%type
    );

  procedure GetDiscrepancyFilterByUserName
    (
      UserName in Discrepancies_Filters.User_Name%type,
      FiltersCursor out sys_refcursor
    );

  procedure GetDiscrepancyFilterBySel
    (
      SelectionId in Discrepancies_Filters.Cd%type,
      FiltersCursor out sys_refcursor
    );

  procedure GetFilterCriterias
    (
      FiltersCursor out sys_refcursor
    );

  procedure GetActionsHistry
    (
      SelectionId in Selection.Id%type,
      ActionHistoryCursor out sys_refcursor
    );

  procedure SaveBuyBookComment
    (
      p_discrepancy_id in discrepancy_comment.discrepancy_id%type,
      p_chapter in discrepancy_comment.chapter%type,
      p_row_key in discrepancy_comment.row_key%type,
      p_buy_accept_date in discrepancy_comment.buy_accept_date%type,
      p_correction_date in discrepancy_comment.correction_date%type,
      p_correction_num in discrepancy_comment.correction_num%type,
      p_currency_code in discrepancy_comment.currency_code%type,
      p_change_correction_date in discrepancy_comment.change_correction_date%type,
      p_change_correction_num in discrepancy_comment.change_correction_num%type,
      p_change_date in discrepancy_comment.change_date%type,
      p_change_num in discrepancy_comment.change_num%type,
      p_broker_inn in discrepancy_comment.broker_inn%type,
      p_broker_kpp in discrepancy_comment.broker_kpp%type,
      p_invoice_date in discrepancy_comment.invoice_date%type,
      p_invoice_num in discrepancy_comment.invoice_num%type,
      p_customs_declaration_num in discrepancy_comment.customs_declaration_num%type,
      p_operation_code in discrepancy_comment.operation_code%type,
      p_seller_inn in discrepancy_comment.seller_inn%type,
      p_seller_kpp in discrepancy_comment.seller_kpp%type,
      p_price_nds_total in discrepancy_comment.price_nds_total%type,
      p_price_total in discrepancy_comment.price_total%type
    );

  procedure SaveSellBookComment
    (
      p_discrepancy_id in discrepancy_comment.discrepancy_id%type,
      p_chapter in discrepancy_comment.chapter%type,
      p_row_key in discrepancy_comment.row_key%type,
      p_buyer_inn in discrepancy_comment.buyer_inn%type,
      p_buyer_kpp in discrepancy_comment.buyer_kpp%type,
      p_receipt_doc_date in discrepancy_comment.receipt_doc_date%type,
      p_receipt_doc_num in discrepancy_comment.receipt_doc_num%type,
      p_correction_date in discrepancy_comment.correction_date%type,
      p_correction_num in discrepancy_comment.correction_num%type,
      p_currency_code in discrepancy_comment.currency_code%type,
      p_change_correction_date in discrepancy_comment.change_correction_date%type,
      p_change_correction_num in discrepancy_comment.change_correction_num%type,
      p_change_date in discrepancy_comment.change_date%type,
      p_change_num in discrepancy_comment.change_num%type,
      p_broker_inn in discrepancy_comment.broker_inn%type,
      p_broker_kpp in discrepancy_comment.broker_kpp%type,
      p_invoice_date in discrepancy_comment.invoice_date%type,
      p_invoice_num in discrepancy_comment.invoice_num%type,
      p_operation_code in discrepancy_comment.operation_code%type,
      p_price_sell_0 in discrepancy_comment.price_sell_0%type,
      p_price_sell_10 in discrepancy_comment.price_sell_10%type,
      p_price_sell_18 in discrepancy_comment.price_sell_18%type,
      p_price_nds_10 in discrepancy_comment.price_nds_10%type,
      p_price_nds_18 in discrepancy_comment.price_nds_18%type,
      p_price_tax_free in discrepancy_comment.price_tax_free%type,
      p_price_sell_in_curr in discrepancy_comment.price_sell_in_curr%type,
      p_price_sell in discrepancy_comment.price_sell%type
    );

  procedure SaveSentJournalComment
    (
      p_discrepancy_id in discrepancy_comment.discrepancy_id%type,
      p_chapter in discrepancy_comment.chapter%type,
      p_row_key in discrepancy_comment.row_key%type,
      p_price_total in discrepancy_comment.price_total%type,
      p_buyer_inn in discrepancy_comment.buyer_inn%type,
      p_buyer_kpp in discrepancy_comment.buyer_kpp%type,
      p_correction_date in discrepancy_comment.correction_date%type,
      p_correction_num in discrepancy_comment.correction_num%type,
      p_diff_correct_decrease in discrepancy_comment.diff_correct_decrease%type,
      p_diff_correct_increase in discrepancy_comment.diff_correct_increase%type,
      p_currency_code in discrepancy_comment.currency_code%type,
      p_change_correction_date in discrepancy_comment.change_correction_date%type,
      p_change_correction_num in discrepancy_comment.change_correction_num%type,
      p_change_date in discrepancy_comment.change_date%type,
      p_change_num in discrepancy_comment.change_num%type,
      p_invoice_date in discrepancy_comment.invoice_date%type,
      p_invoice_num in discrepancy_comment.invoice_num%type,
      p_operation_code in discrepancy_comment.operation_code%type,
      p_seller_inn in discrepancy_comment.seller_inn%type,
      p_seller_kpp in discrepancy_comment.seller_kpp%type,
      p_create_date in discrepancy_comment.create_date%type,
      p_price_nds_total in discrepancy_comment.price_nds_total%type,
      p_diff_correct_nds_decrease in discrepancy_comment.diff_correct_nds_decrease%type,
      p_diff_correct_nds_increase in discrepancy_comment.diff_correct_nds_increase%type
    );

  procedure SaveRecieveJournalComment
    (
      p_discrepancy_id in discrepancy_comment.discrepancy_id%type,
      p_chapter in discrepancy_comment.chapter%type,
      p_row_key in discrepancy_comment.row_key%type,
      p_price_total in discrepancy_comment.price_total%type,
      p_broker_inn in discrepancy_comment.broker_inn%type,
      p_broker_kpp in discrepancy_comment.broker_kpp%type,
    p_deal_kind_code in discrepancy_comment.deal_kind_code%type,
      p_correction_date in discrepancy_comment.correction_date%type,
      p_correction_num in discrepancy_comment.correction_num%type,
      p_diff_correct_decrease in discrepancy_comment.diff_correct_decrease%type,
      p_diff_correct_increase in discrepancy_comment.diff_correct_increase%type,
      p_currency_code in discrepancy_comment.currency_code%type,
      p_change_correction_date in discrepancy_comment.change_correction_date%type,
      p_change_correction_num in discrepancy_comment.change_correction_num%type,
      p_change_date in discrepancy_comment.change_date%type,
      p_change_num in discrepancy_comment.change_num%type,
      p_invoice_date in discrepancy_comment.invoice_date%type,
      p_invoice_num in discrepancy_comment.invoice_num%type,
      p_operation_code in discrepancy_comment.operation_code%type,
      p_seller_inn in discrepancy_comment.seller_inn%type,
      p_seller_kpp in discrepancy_comment.seller_kpp%type,
      p_receive_date in discrepancy_comment.receive_date%type,
      p_price_nds_total in discrepancy_comment.price_nds_total%type,
      p_diff_correct_nds_decrease in discrepancy_comment.diff_correct_nds_decrease%type,
      p_diff_correct_nds_increase in discrepancy_comment.diff_correct_nds_increase%type
    );

procedure P$MoveDiscrepancyFromTmpToWork
(
   p_discrepancy_sov_id in sov_discrepancy.sov_id%type,
   p_discrepancy_id out sov_discrepancy.id%type
);

function F$GET_STAGE_STATUS_NAME
(
   pStageId in dict_discrepancy_stage.id%type,
   pStageStatusId in hist_discrepancy_stage.status_id%type
)
return VARCHAR2;

  procedure Get_Discrepancy_Side
  (
    pDiscrepancyId in SOV_Discrepancy.Id%type,
    pCursor out sys_refcursor
  );

  procedure GetBuyBooks
  (
    pRowKey in varchar2,
    pCursor out sys_refcursor
  );

  procedure GetSellBooks
  (
    pRowKey in varchar2,
    pCursor out sys_refcursor
  );

  procedure GetSentJournal
  (
    pRowKey in varchar2,
    pCursor out sys_refcursor
  );

  procedure GetRecieveJournal
  (
    pRowKey in varchar2,
    pCursor out sys_refcursor
  );

end;
/
CREATE OR REPLACE PACKAGE BODY NDS2_MRR_USER.NDS2$DISCREPANCIES is

  procedure SaveDiscrepancyFilter
    (
      p_Cd in Discrepancies_Filters.Cd%type,
      p_UserName in Discrepancies_Filters.User_Name%type,
      p_IsFavourite in Discrepancies_Filters.Is_Favourite%type,
      p_Name in Discrepancies_Filters.Name%type,
      p_Data in Discrepancies_Filters.Data%type,
      p_Id out Discrepancies_Filters.Id%type
    )
  is
  begin
    update
      Discrepancies_Filters
    set
      Cd = p_Cd,
      User_Name = p_UserName,
      Is_Favourite = p_IsFavourite,
      Name = p_Name,
      Data = p_Data
    where Id = p_Id;

    if SQL%ROWCOUNT = 0 then
    begin
        p_Id := SEQ_DISCREPANCIES_FILTER.NEXTVAL;
        insert into Discrepancies_Filters values(p_Id, p_Cd, p_UserName, p_IsFavourite, p_Name, p_Data);
    end;
    end if;
 end;

  procedure GetDiscrepancyFilterByUserName
    (
      UserName in Discrepancies_Filters.User_Name%type,
      FiltersCursor out sys_refcursor
    )
  is
  begin
    open FiltersCursor for
    select * from Discrepancies_Filters where (User_Name = UserName);
  end;

  procedure GetDiscrepancyFilterBySel
    (
      SelectionId in Discrepancies_Filters.Cd%type,
      FiltersCursor out sys_refcursor
    )
  is
  begin
    open FiltersCursor for
    select * from Discrepancies_Filters where (Cd = SelectionId);
  end;

  procedure GetFilterCriterias
    (
      FiltersCursor out sys_refcursor
    )
  is
  begin
    open FiltersCursor for
    select * from Dict_Filter_Types;
  end;

  procedure GetActionsHistry
    (
      SelectionId in Selection.Id%type,
      ActionHistoryCursor out sys_refcursor
    )
  is
  begin
    open ActionHistoryCursor for
    select ah.id, ah.cd, ah.status, ah.change_date, ah.action_comment, ah.user_name, ah.action,
       dss.id, dss.name, da.id, da.action_name
    from Action_History ah
           left join Dict_Selection_Status dss on (ah.Status = dss.Id)
           left join Dict_Actions da on (ah.Action = da.Id)
    where ah.Cd = SelectionId
    order by ah.change_date desc;
  end;

  procedure SaveComment
    (
      p_discrepancy_id in discrepancy_comment.discrepancy_id%type,
      p_chapter in discrepancy_comment.chapter%type,
      p_row_key in discrepancy_comment.row_key%type,
      p_create_date in discrepancy_comment.create_date%type,
      p_receive_date in discrepancy_comment.receive_date%type,
      p_operation_code in discrepancy_comment.operation_code%type,
      p_invoice_num in discrepancy_comment.invoice_num%type,
      p_invoice_date in discrepancy_comment.invoice_date%type,
      p_change_num in discrepancy_comment.change_num%type,
      p_change_date in discrepancy_comment.change_date%type,
      p_correction_num in discrepancy_comment.correction_num%type,
      p_correction_date in discrepancy_comment.correction_date%type,
      p_change_correction_num in discrepancy_comment.change_correction_num%type,
      p_change_correction_date in discrepancy_comment.change_correction_date%type,
      p_receipt_doc_num in discrepancy_comment.receipt_doc_num%type,
      p_receipt_doc_date in discrepancy_comment.receipt_doc_date%type,
      p_buy_accept_date in discrepancy_comment.buy_accept_date%type,
      p_buyer_inn in discrepancy_comment.buyer_inn%type,
      p_buyer_kpp in discrepancy_comment.buyer_kpp%type,
      p_seller_inn in discrepancy_comment.seller_inn%type,
      p_seller_kpp in discrepancy_comment.seller_kpp%type,
      p_seller_invoice_num in discrepancy_comment.seller_invoice_num%type,
      p_seller_invoice_date in discrepancy_comment.seller_invoice_date%type,
      p_broker_inn in discrepancy_comment.broker_inn%type,
      p_broker_kpp in discrepancy_comment.broker_kpp%type,
      p_deal_kind_code in discrepancy_comment.deal_kind_code%type,
      p_customs_declaration_num in discrepancy_comment.customs_declaration_num%type,
      p_currency_code in discrepancy_comment.currency_code%type,
      p_price_buy_amount in discrepancy_comment.price_buy_amount%type,
      p_price_buy_nds_amount in discrepancy_comment.price_buy_nds_amount%type,
      p_price_sell in discrepancy_comment.price_sell%type,
      p_price_sell_in_curr in discrepancy_comment.price_sell_in_curr%type,
      p_price_sell_18 in discrepancy_comment.price_sell_18%type,
      p_price_sell_10 in discrepancy_comment.price_sell_10%type,
      p_price_sell_0 in discrepancy_comment.price_sell_0%type,
      p_price_nds_18 in discrepancy_comment.price_nds_18%type,
      p_price_nds_10 in discrepancy_comment.price_nds_10%type,
      p_price_tax_free in discrepancy_comment.price_tax_free%type,
      p_price_total in discrepancy_comment.price_total%type,
      p_price_nds_total in discrepancy_comment.price_nds_total%type,
      p_diff_correct_decrease in discrepancy_comment.diff_correct_decrease%type,
      p_diff_correct_increase in discrepancy_comment.diff_correct_increase%type,
      p_diff_correct_nds_decrease in discrepancy_comment.diff_correct_nds_decrease%type,
      p_diff_correct_nds_increase in discrepancy_comment.diff_correct_nds_increase%type
    )
    is
    begin
      insert into discrepancy_comment
      values (p_discrepancy_id, p_chapter, p_row_key, p_create_date, p_receive_date, p_operation_code, p_invoice_num, p_invoice_date, p_change_num, p_change_date, p_correction_num, p_correction_date, p_change_correction_num, p_change_correction_date, p_receipt_doc_num, p_receipt_doc_date, p_buy_accept_date, p_buyer_inn, p_buyer_kpp, p_seller_inn, p_seller_kpp, p_seller_invoice_num, p_seller_invoice_date, p_broker_inn, p_broker_kpp, p_deal_kind_code, p_customs_declaration_num, p_currency_code, p_price_buy_amount, p_price_buy_nds_amount, p_price_sell, p_price_sell_in_curr, p_price_sell_18, p_price_sell_10, p_price_sell_0, p_price_nds_18, p_price_nds_10, p_price_tax_free, p_price_total, p_price_nds_total, p_diff_correct_decrease, p_diff_correct_increase, p_diff_correct_nds_decrease, p_diff_correct_nds_increase);
    end;

  procedure SaveBuyBookComment
    (
      p_discrepancy_id in discrepancy_comment.discrepancy_id%type,
      p_chapter in discrepancy_comment.chapter%type,
      p_row_key in discrepancy_comment.row_key%type,
      p_buy_accept_date in discrepancy_comment.buy_accept_date%type,
      p_correction_date in discrepancy_comment.correction_date%type,
      p_correction_num in discrepancy_comment.correction_num%type,
      p_currency_code in discrepancy_comment.currency_code%type,
      p_change_correction_date in discrepancy_comment.change_correction_date%type,
      p_change_correction_num in discrepancy_comment.change_correction_num%type,
      p_change_date in discrepancy_comment.change_date%type,
      p_change_num in discrepancy_comment.change_num%type,
      p_broker_inn in discrepancy_comment.broker_inn%type,
      p_broker_kpp in discrepancy_comment.broker_kpp%type,
      p_invoice_date in discrepancy_comment.invoice_date%type,
      p_invoice_num in discrepancy_comment.invoice_num%type,
      p_customs_declaration_num in discrepancy_comment.customs_declaration_num%type,
      p_operation_code in discrepancy_comment.operation_code%type,
      p_seller_inn in discrepancy_comment.seller_inn%type,
      p_seller_kpp in discrepancy_comment.seller_kpp%type,
      p_price_nds_total in discrepancy_comment.price_nds_total%type,
      p_price_total in discrepancy_comment.price_total%type
    )
    is
  begin
    SaveComment(p_discrepancy_id, p_chapter, p_row_key, null, null, p_operation_code, p_invoice_num, p_invoice_date, p_change_num, p_change_date, p_correction_num, p_correction_date, p_change_correction_num, p_change_correction_date, null, null, p_buy_accept_date, null, null, p_seller_inn, p_seller_kpp, null, null, p_broker_inn, p_broker_kpp, null, p_customs_declaration_num, p_currency_code, null, null, null, null, null, null, null, null, null, null, p_price_total, p_price_nds_total, null, null, null, null);
  end;

  procedure SaveSellBookComment
    (
      p_discrepancy_id in discrepancy_comment.discrepancy_id%type,
      p_chapter in discrepancy_comment.chapter%type,
      p_row_key in discrepancy_comment.row_key%type,
      p_buyer_inn in discrepancy_comment.buyer_inn%type,
      p_buyer_kpp in discrepancy_comment.buyer_kpp%type,
      p_receipt_doc_date in discrepancy_comment.receipt_doc_date%type,
      p_receipt_doc_num in discrepancy_comment.receipt_doc_num%type,
      p_correction_date in discrepancy_comment.correction_date%type,
      p_correction_num in discrepancy_comment.correction_num%type,
      p_currency_code in discrepancy_comment.currency_code%type,
      p_change_correction_date in discrepancy_comment.change_correction_date%type,
      p_change_correction_num in discrepancy_comment.change_correction_num%type,
      p_change_date in discrepancy_comment.change_date%type,
      p_change_num in discrepancy_comment.change_num%type,
      p_broker_inn in discrepancy_comment.broker_inn%type,
      p_broker_kpp in discrepancy_comment.broker_kpp%type,
      p_invoice_date in discrepancy_comment.invoice_date%type,
      p_invoice_num in discrepancy_comment.invoice_num%type,
      p_operation_code in discrepancy_comment.operation_code%type,
      p_price_sell_0 in discrepancy_comment.price_sell_0%type,
      p_price_sell_10 in discrepancy_comment.price_sell_10%type,
      p_price_sell_18 in discrepancy_comment.price_sell_18%type,
      p_price_nds_10 in discrepancy_comment.price_nds_10%type,
      p_price_nds_18 in discrepancy_comment.price_nds_18%type,
      p_price_tax_free in discrepancy_comment.price_tax_free%type,
      p_price_sell_in_curr in discrepancy_comment.price_sell_in_curr%type,
      p_price_sell in discrepancy_comment.price_sell%type
    )
    is
  begin
    SaveComment(p_discrepancy_id, p_chapter, p_row_key, null, null, p_operation_code, p_invoice_num, p_invoice_date, p_change_num, p_change_date, p_correction_num, p_correction_date, p_change_correction_num, p_change_correction_date, p_receipt_doc_num, p_receipt_doc_date, null, p_buyer_inn, p_buyer_kpp, null, null, null, null, p_broker_inn, p_broker_kpp, null, null, p_currency_code, null, null, p_price_sell, p_price_sell_in_curr, p_price_sell_18, p_price_sell_10, p_price_sell_0, p_price_nds_18, p_price_nds_10, p_price_tax_free, null, null, null, null, null, null);
  end;

  procedure SaveSentJournalComment
    (
      p_discrepancy_id in discrepancy_comment.discrepancy_id%type,
      p_chapter in discrepancy_comment.chapter%type,
      p_row_key in discrepancy_comment.row_key%type,
      p_price_total in discrepancy_comment.price_total%type,
      p_buyer_inn in discrepancy_comment.buyer_inn%type,
      p_buyer_kpp in discrepancy_comment.buyer_kpp%type,
      p_correction_date in discrepancy_comment.correction_date%type,
      p_correction_num in discrepancy_comment.correction_num%type,
      p_diff_correct_decrease in discrepancy_comment.diff_correct_decrease%type,
      p_diff_correct_increase in discrepancy_comment.diff_correct_increase%type,
      p_currency_code in discrepancy_comment.currency_code%type,
      p_change_correction_date in discrepancy_comment.change_correction_date%type,
      p_change_correction_num in discrepancy_comment.change_correction_num%type,
      p_change_date in discrepancy_comment.change_date%type,
      p_change_num in discrepancy_comment.change_num%type,
      p_invoice_date in discrepancy_comment.invoice_date%type,
      p_invoice_num in discrepancy_comment.invoice_num%type,
      p_operation_code in discrepancy_comment.operation_code%type,
      p_seller_inn in discrepancy_comment.seller_inn%type,
      p_seller_kpp in discrepancy_comment.seller_kpp%type,
      p_create_date in discrepancy_comment.create_date%type,
      p_price_nds_total in discrepancy_comment.price_nds_total%type,
      p_diff_correct_nds_decrease in discrepancy_comment.diff_correct_nds_decrease%type,
      p_diff_correct_nds_increase in discrepancy_comment.diff_correct_nds_increase%type
    )
    is
  begin
    SaveComment(p_discrepancy_id, p_chapter, p_row_key, p_create_date, null, p_operation_code, p_invoice_num, p_invoice_date, p_change_num, p_change_date, p_correction_num, p_correction_date, p_change_correction_num, p_change_correction_date, null, null, null, p_buyer_inn, p_buyer_kpp, p_seller_inn, p_seller_kpp, null, null, null, null, null, null, p_currency_code, null, null, null, null, null, null, null, null, null, null, p_price_total, p_price_nds_total, p_diff_correct_decrease, p_diff_correct_increase, p_diff_correct_nds_decrease, p_diff_correct_nds_increase);
  end;

  procedure SaveRecieveJournalComment
    (
      p_discrepancy_id in discrepancy_comment.discrepancy_id%type,
      p_chapter in discrepancy_comment.chapter%type,
      p_row_key in discrepancy_comment.row_key%type,
      p_price_total in discrepancy_comment.price_total%type,
      p_broker_inn in discrepancy_comment.broker_inn%type,
      p_broker_kpp in discrepancy_comment.broker_kpp%type,
    p_deal_kind_code in discrepancy_comment.deal_kind_code%type,
      p_correction_date in discrepancy_comment.correction_date%type,
      p_correction_num in discrepancy_comment.correction_num%type,
      p_diff_correct_decrease in discrepancy_comment.diff_correct_decrease%type,
      p_diff_correct_increase in discrepancy_comment.diff_correct_increase%type,
      p_currency_code in discrepancy_comment.currency_code%type,
      p_change_correction_date in discrepancy_comment.change_correction_date%type,
      p_change_correction_num in discrepancy_comment.change_correction_num%type,
      p_change_date in discrepancy_comment.change_date%type,
      p_change_num in discrepancy_comment.change_num%type,
      p_invoice_date in discrepancy_comment.invoice_date%type,
      p_invoice_num in discrepancy_comment.invoice_num%type,
      p_operation_code in discrepancy_comment.operation_code%type,
      p_seller_inn in discrepancy_comment.seller_inn%type,
      p_seller_kpp in discrepancy_comment.seller_kpp%type,
      p_receive_date in discrepancy_comment.receive_date%type,
      p_price_nds_total in discrepancy_comment.price_nds_total%type,
      p_diff_correct_nds_decrease in discrepancy_comment.diff_correct_nds_decrease%type,
      p_diff_correct_nds_increase in discrepancy_comment.diff_correct_nds_increase%type
    )
    is
  begin
    SaveComment(p_discrepancy_id, p_chapter, p_row_key, null, p_receive_date, p_operation_code, p_invoice_num, p_invoice_date, p_change_num, p_change_date, p_correction_num, p_correction_date, p_change_correction_num, p_change_correction_date, null, null, null, null, null, p_seller_inn, p_seller_kpp, null, null, p_broker_inn, p_broker_kpp, p_deal_kind_code, null, p_currency_code, null, null, null, null, null, null, null, null, null, null, p_price_total, p_price_nds_total, p_diff_correct_decrease, p_diff_correct_increase, p_diff_correct_nds_decrease, p_diff_correct_nds_increase);
  end;

procedure P$MoveDiscrepancyFromTmpToWork
(
   p_discrepancy_sov_id in sov_discrepancy.sov_id%type,
   p_discrepancy_id out sov_discrepancy.id%type
)
is
begin
  p_discrepancy_id := SEQ_DISCREPANCY_ID.NEXTVAL();
    insert into SOV_DISCREPANCY
    (
      id,
      sov_id,
      create_date,
      type,
      compare_kind,
      rule_group,
      deal_amnt,
      amnt,
      amount_pvp,
      invoice_chapter,
      invoice_rk,
      decl_id,
      invoice_contractor_chapter,
      invoice_contractor_rk,
      decl_contractor_id,
      status,
      side_primary_processing,
    stage
    )
    select
      p_discrepancy_id,
      st.sov_id,
      st.create_date,
      st.type,
      st.compare_kind,
      st.rule_group,
      st.deal_amnt,
      st.amnt,
      st.amount_pvp,
      st.invoice_chapter,
      st.invoice_rk,
      st.decl_id,
      st.invoice_contractor_chapter,
      st.invoice_contractor_rk,
      st.decl_contractor_id,
      st.status,
      st.side_primary_processing,
    NVL(st.stage, 1) as stage
    from SOV_DISCREPANCY st
    left join SOV_DISCREPANCY sd on sd.sov_id = st.sov_id
    where st.sov_id = p_discrepancy_sov_id and sd.id is null;
end;


function F$GET_STAGE_STATUS_NAME
(
   pStageId in dict_discrepancy_stage.id%type,
   pStageStatusId in hist_discrepancy_stage.status_id%type
)
return VARCHAR2
is
Result VARCHAR2(128);
DocTypeId number;
begin

if (pStageId = 2 or pStageId = 3) then
   DocTypeId := 1;
else
  If (pStageId = 4 or pStageId = 5) then
   DocTypeId := 2;
  end If;
end If;

select
  case NVL(pStageId, 1)
      when 1 then replace(
                      replace(
                         replace(selstat.name, '×åðíîâèê', 'Â ðàáîòå'),
                      'Íà ñîãëàñîâàíèè', 'Â ðàáîòå'),
                  'Íà äîðàáîòêå', 'Â ðàáîòå')
      when 2 then docstatStage2.description
      when 3 then docstatStage3.description
      when 4 then docstatStage4.description
      when 5 then docstatStage5.description
      when 6 then null
  end as STAGE_STATUS_NAME into Result
from dual
     left join DICT_SELECTION_STATUS selstat on selstat.id = pStageStatusId
     left join DOC_STATUS docstatStage2 on (docstatStage2.id = pStageStatusId) and (docstatStage2.doc_type = DocTypeId)
     left join DOC_STATUS docstatStage3 on (docstatStage3.id = pStageStatusId) and (docstatStage3.doc_type = DocTypeId)
     left join DOC_STATUS docstatStage4 on (docstatStage4.id = pStageStatusId) and (docstatStage4.doc_type = DocTypeId)
     left join DOC_STATUS docstatStage5 on (docstatStage5.id = pStageStatusId) and (docstatStage5.doc_type = DocTypeId);

     Return Result;
end;

  procedure Get_Discrepancy_Side
  (
    pDiscrepancyId in SOV_Discrepancy.Id%type,
    pCursor out sys_refcursor
  )
  as
  begin
    open pCursor for
    select
      discrepancy_id,
      discrepancy_sov_id,
      invoice_row_key as InvoiceId,
      Side as SideName,
      chapter,
      declaration_id as DeclarationId,
      Side1Inn,
      Side1Kpp,
      Side1Name,
      Side2Inn,
      Side2Kpp,
      Side2Name,
      invoice_num as SideInfoInvoiceNumber,
      invoice_date as SideInfoInvoiceDate,
      price_total as SideInfoInvoiceAmount,
      price_nds_total as SideInfoTaxAmount,
      region_code as RegionCode,
      region as RegionName,
      soun_code as InspectionCode,
      soun as InspectionName,
      FormatErrors,
      LogicalErrors
      from V$DISCREPANCY_SIDE where discrepancy_id = pDiscrepancyId;
  end;



  procedure GetBuyBooks
  (
    pRowKey in varchar2,
    pCursor out sys_refcursor
  ) is
  begin
    open pCursor for
    select
      i.row_key as RowKey,
      i.actual_row_key,
      i.ordinal_number as OrdinalNumber,
      i.invoice_num as InvoiceNumber,
      i.invoice_date as InvoiceDate,
      i.change_num as FixedInvoiceNumber,
      i.change_date as FixedInvoiceDate,
      i.correction_num as CorrectedInvoiceNumber,
      i.correction_date as CorrectedInvoiceDate,
      i.change_correction_num as FixedCorrectedInvoiceNumber,
      i.change_correction_date as FixedCorrectedInvoiceDate,
      i.okv_code as CurrencyCode,
      case when i.chapter = 81 then 1 else 0 end as PriznakDopList,
      i.broker_inn as IntermediaryInfoINN,
      i.broker_kpp as IntermediaryInfoKPP,
      (
        select
         tpName.Np_Name
        from 
        mv$tax_payer_name tpName where tpName.Inn  = i.broker_inn and rownum = 1
      ) as IntermediaryInfoName,
      i.customs_declaration_num as NumberTD,
      i.price_total             as TotalAmount,
      i.price_nds_total         as TaxAmount,
      i.logical_errors          as ErrorCodesString

    from sov_invoice_all i
    where
    (i.row_key = pRowKey or i.actual_row_key = pRowKey)
    and i.chapter = 8;
  end;

  procedure GetSellBooks
  (
    pRowKey in varchar2,
    pCursor out sys_refcursor
  ) is
  begin
    open pCursor for
    select distinct
      i.row_key as RowKey,
      i.actual_row_key,
      i.ordinal_number as OrdinalNumber,
      i.invoice_num     as InvoiceNumber,
      i.invoice_date    as InvoiceDate,
      i.change_num      as FixedInvoiceNumber,
      i.change_date     as FixedInvoiceDate,
      i.correction_num  as CorrectedInvoiceNumber,
      i.correction_date as CorrectedInvoiceDate,
      i.change_correction_num  as FixedCorrectedInvoiceNumber,
      i.change_correction_date as FixedCorrectedInvoiceDate,
      i.okv_code        as CurrencyCode,
      case when i.chapter = 91 then 1 else 0 end as PriznakDopList,
      i.broker_inn as IntermediaryInfoINN,
      i.broker_kpp as IntermediaryInfoKPP,
      (
        select
         tpName.Np_Name
        from 
        mv$tax_payer_name tpName where tpName.Inn  = i.broker_inn and rownum = 1
      ) as IntermediaryInfoName,
      i.price_sell_18          as TaxableAmount18,
      i.price_sell_10          as TaxableAmount10,
      i.price_sell_0           as TaxableAmount0,
      i.price_nds_18           as TaxAmount18,
      i.price_nds_10           as TaxAmount10,
      i.price_tax_free         as TaxFreeAmount,
      i.price_sell_in_curr     as TotalAmountCurrency,
      i.price_sell             as TotalAmountRouble,
      i.logical_errors         as ErrorCodesString
    from sov_invoice_all i
    where
    (i.row_key = pRowKey or i.actual_row_key = pRowKey)
    and i.chapter = 9;
  end;

  procedure GetSentJournal
  (
    pRowKey in varchar2,
    pCursor out sys_refcursor
  ) is
  begin
    open pCursor for
    select
      i.row_key as RowKey,
      i.actual_row_key,
      i.ordinal_number as OrdinalNumber,
      i.invoice_num  as InvoiceNumber,
      i.invoice_date as InvoiceDate,
      i.change_num  as FixedInvoiceNumber,
      i.change_date as FixedInvoiceDate,
      i.correction_num  as CorrectedInvoiceNumber,
      i.correction_date as CorrectedInvoiceDate,
      i.change_correction_num  as FixedCorrectedInvoiceNumber,
      i.change_correction_date as FixedCorrectedInvoiceDate,
      i.okv_code          as CurrencyCode,
      i.create_date as SendDate,
      i.price_total as Amount,
      i.price_nds_total as TaxAmount,
      i.diff_correct_decrease as CostDifferenceDecrease,
      i.diff_correct_increase as CostDifferenceIncrease,
      i.diff_correct_nds_decrease as TaxDifferenceDecrease,
      i.diff_correct_nds_increase as TaxDifferenceIncrease,
      i.logical_errors as ErrorCodesString
    from sov_invoice_all i
    where
    (i.row_key = pRowKey or i.actual_row_key = pRowKey)
    and i.chapter = 10;
  end;

  procedure GetRecieveJournal
  (
    pRowKey in varchar2,
    pCursor out sys_refcursor
  ) is
  begin
    open pCursor for
    select
      i.row_key RowKey,
      i.actual_row_key,
      i.ordinal_number as OrdinalNumber,
      i.invoice_num as InvoiceNumber,
      i.invoice_date as InvoiceDate,
      i.change_num as FixedInvoiceNumber,
      i.change_date as FixedInvoiceDate,
      i.correction_num as CorrectedInvoiceNumber,
      i.correction_date as CorrectedInvoiceDate,
      i.change_correction_num  as FixedCorrectedInvoiceNumber,
      i.change_correction_date as FixedCorrectedInvoiceDate,
      i.okv_code as CurrencyCode,
      i.broker_inn as SubcommissionAgentINN,
      i.broker_kpp as SubcommissionAgentKPP,
      (
        select
         tpName.Np_Name
        from 
        mv$tax_payer_name tpName where tpName.Inn  = i.broker_inn and rownum = 1
      ) as SubcommissionAgentName,
      i.deal_kind_code as DealTypeCode,
      i.receive_date as RecieveDate,
      i.price_total as Amount,
      i.price_nds_total as TaxAmount,
      i.diff_correct_decrease as CostDifferenceDecrease,
      i.diff_correct_increase as CostDifferenceIncrease,
      i.diff_correct_nds_decrease as TaxDifferenceDecrease,
      i.diff_correct_nds_increase as TaxDifferenceIncrease,
      i.logical_errors as ErrorCodesString
    from sov_invoice_all i
    where
    (i.row_key = pRowKey or i.actual_row_key = pRowKey)
    and i.chapter = 11 ;
  end;

end;
/
