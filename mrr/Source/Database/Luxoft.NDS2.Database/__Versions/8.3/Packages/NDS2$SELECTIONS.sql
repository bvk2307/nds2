create or replace package nds2_mrr_user.Nds2$Selections as
TYPE T_ARR_NUM IS TABLE OF NUMBER(19) INDEX BY PLS_INTEGER;

function GenerateUniqueName
(
  p_user_sid varchar2
) return varchar2;

function VERIFY_SELECTION_NAME
(
  p_user_sid varchar2,
  p_selection_name varchar2,
  p_selection_id number
) return varchar2;

PROCEDURE SEND_TO_EOD(p_selection_ids in T_ARR_NUM);

PROCEDURE SAVE_SELECTION
(
  p_selectionId IN OUT selection.id%type,
  p_name IN selection.name%type,
  p_analytic IN selection.analytic%type,
  p_analytic_sid IN selection.analytic_sid%type,
  p_chief IN selection.chief%type,
  p_chief_sid IN selection.chief_sid%type,
  p_sender IN selection.sender%type,
  p_status IN selection.status%type,
  p_status_date IN selection.status_date%type,
  p_creation_date in selection.creation_date%type,
  p_modification_date in selection.modification_date%type,
  p_comment in action_history.action_comment%type,
  p_save_reason in number/*1 - create, 2 - remove, 3 - status change, 4 - update*/
);

PROCEDURE                   REMOVE_SELECTION
(pId IN NUMBER, analytic IN VARCHAR2);

PROCEDURE          SELECTION_SET_STATE
(pId IN NUMBER, statusId IN NUMBER, userName in varchar2);

PROCEDURE          SELECT_SELECTION
(selectionCursor OUT SYS_REFCURSOR);

PROCEDURE          SELECT_SELECTION_BY_NAME
(selectionStartName VARCHAR2, selectionCursor OUT SYS_REFCURSOR);

PROCEDURE GET_SELECTION_STATUS
(selectionId IN NUMBER, statusCursor OUT SYS_REFCURSOR);

PROCEDURE GET_FAVORITE_FILTERS (pAnalyst IN FAVORITE_FILTERS.ANALYST%type, pFavCursor OUT SYS_REFCURSOR);

PROCEDURE SAVE_FAVORITE_FILTER (
  pId IN OUT FAVORITE_FILTERS.ID%type,
  pName IN FAVORITE_FILTERS.NAME%type,
  pAnalyst IN FAVORITE_FILTERS.ANALYST%type,
  pFilter IN FAVORITE_FILTERS.FILTER%type);

PROCEDURE DELETE_FAVORITE_FILTER (pId IN FAVORITE_FILTERS.ID%type);

PROCEDURE UPDATE_DECLARATION_CHECK_STATE (
  pSelectionId IN SELECTION.ID%type,
  pDeclarationId IN SOV_DECLARATION_INFO.ID%type,
  pCheckState IN SELECTION_DECLARATION.IS_IN_PROCESS%type);

PROCEDURE UPDATE_DISCREPANCY_CHECK_STATE (
  pSelectionId IN SELECTION.ID%type,
  pDiscrepancyId IN SOV_DISCREPANCY.ID%type,
  pCheckState IN SELECTION_DISCREPANCY.IS_IN_PROCESS%type,
  pStatus OUT NUMBER
  );

PROCEDURE ALL_DECLARATIONS_CHECK_STATE (
  pSelectionId IN SELECTION.ID%type,
  pCheckState IN SELECTION_DECLARATION.IS_IN_PROCESS%type
  );

PROCEDURE ALL_DISCREPANCIES_CHECK_STATE (
  pSelectionId IN SELECTION.ID%type,
  pCheckState IN SELECTION_DISCREPANCY.IS_IN_PROCESS%type,
  pStatus OUT NUMBER
  );

PROCEDURE SYNC_DECLARATIONS_CHECK_STATE (
  pSelectionId IN SELECTION.ID%type,
  pTargetCheckState IN SELECTION_DECLARATION.IS_IN_PROCESS%type
  );

PROCEDURE COUNT_CHECKED_DECLARATIONS (
  pSelectionId IN SELECTION.ID%type,
  pChecked OUT NUMBER,
  pAll OUT NUMBER
  );

PROCEDURE COUNT_CHECKED_DISCREPANCIES (
  pSelectionId IN SELECTION.ID%type,
  pDisabled OUT NUMBER,
  pChecked OUT NUMBER,
  pAll OUT NUMBER,
  pInWorked OUT NUMBER
  );

procedure MERGE_SENDED_SELECTIONS;

/*
������� ������� ����� ��������� �������
*/
PROCEDURE UPD_HIST_SELECTION_STATUS
(
  p_id in selection.id%type,
  p_status in selection.status%type
);

/*
�������� ��������� ���������
*/
PROCEDURE GET_SELECTION_TRANSITIONS
(pFavCursor OUT SYS_REFCURSOR);

PROCEDURE INSERT_ACTION_HISTORY (
    pSelectionId IN ACTION_HISTORY.CD%type,
    pComment IN ACTION_HISTORY.ACTION_COMMENT%type,
    pUser IN ACTION_HISTORY.USER_NAME%type,
    pAction IN ACTION_HISTORY.ACTION%type
);

PROCEDURE GET_DISCREPANCY (
  pDiscrepancyId IN SELECTION_DISCREPANCY.DISCREPANCY_ID%type,
  pSelectionId IN SELECTION.ID%type,
  pCursor OUT SYS_REFCURSOR
  );

PROCEDURE P$GET_REGIONS (
  pUserSID IN METODOLOG_CHECK_EMPLOYEE.SID%type, -- ������������� ������������
  pData OUT SYS_REFCURSOR -- ������ ��������
  ); -- ���������� ������ ��������, ��������� ������������ (��� ������� �������)

PROCEDURE P$SEARCH_REGIONS (
  pUserSID IN USER_TO_N_SSRF.USER_SID%type, -- ������������� ������������
  pSearchKey IN VARCHAR2,
  pMaxQuantity IN NUMBER,
  pData OUT SYS_REFCURSOR -- ������ ��������
  );

PROCEDURE P$GET_INSPECTIONS (
  pUserSID IN METODOLOG_CHECK_EMPLOYEE.SID%type, -- ������������� ������������
  pData OUT SYS_REFCURSOR -- ������ ���������
  ); -- ���������� ������ ���������, ��������� ������������ (��� ������� �������)

PROCEDURE P$SEARCH_INSPECTIONS (
  pUserSID IN USER_TO_N_SSRF.USER_SID%type,
  pSearchKey IN VARCHAR2,
  pMaxQuantity IN NUMBER,
  pData OUT SYS_REFCURSOR
  );

procedure P$START_CANCEL_SELECTION;
procedure P$DO_CANCEL_SELECTION;

procedure P$REMOVE_SELECTION_RESULTS
(
  p_selection_id IN SELECTION.ID%type
);
procedure P$LOAD_SELECTION ( -- ��������� ���������������� ������ �������
  pId IN SELECTION.ID%type,
  pCursor OUT SYS_REFCURSOR
  );

procedure P$FILL_SELECTION_DECLARATION
(
  p_selection_id IN SELECTION.ID%type
);

end Nds2$Selections;
/
create or replace package body nds2_mrr_user.Nds2$Selections as

function GenerateUniqueName
(
  p_user_sid varchar2
) return varchar2
as
v_name varchar2(128);
v_pattern varchar2(32) := '������� ';
v_counter number(10) := 1;
begin
 v_name := v_pattern || v_counter;
 for sel in
   (SELECT distinct name, TO_NUMBER(REGEXP_SUBSTR(selection.name, '[[:digit:]]+')) x FROM selection
      WHERE lower(analytic_sid) = lower(p_user_sid) and regexp_like(selection.name, v_pattern) order by x asc) --(select name from selections where analytic = p_user_name order by name asc)
 loop
  if TO_CHAR(sel.name) = v_name then
    v_counter := v_counter + 1;
    v_name := v_pattern || v_counter;
  end if;
 end loop;
 return v_name;
 exception
   when no_data_found then
     return rawtohex(sys_guid());
end;

function VERIFY_SELECTION_NAME
(
  p_user_sid varchar2,
  p_selection_name varchar2,
  p_selection_id number
) return varchar2
as
v_idx number := 0;
v_exist number;
v_name varchar2(1024);
begin
v_name := trim(p_selection_name);
select count(1) into v_exist from selection where lower(analytic_sid) = lower(p_user_sid) and name = v_name and id <> p_selection_id;
dbms_output.put_line(v_exist);
while v_exist >= 1 and v_idx < 10000 loop
   v_idx := v_idx+1;
   v_name := trim(p_selection_name)||' ('||v_idx||')';
   select count(1) into v_exist from selection where lower(analytic_sid) = lower(p_user_sid) and name = v_name and id <> p_selection_id;
end loop;

return v_name;
end;

PROCEDURE SEND_TO_EOD(p_selection_ids in T_ARR_NUM)
is
  v_tab_ids t$table_of_number := t$table_of_number ();
  v_sel_send_status number := 4;
  v_sel_send_discrep_status number := 4;
begin
    v_tab_ids.extend(p_selection_ids.count);
    for i in 1..p_selection_ids.count
    loop
     v_tab_ids(i) := p_selection_ids(i);
    end loop;  
    
    /*��������� ������ �������*/
    UPDATE SELECTION s
      SET status = v_sel_send_status
    WHERE EXISTS 
    (
      SELECT 1
      FROM SELECTION vw
      inner join table(v_tab_ids) IDS on IDS.column_value = vw.id
      inner join SELECTION_TRANSITION st ON st.state_from = vw.status and st.state_to = v_sel_send_status
      WHERE vw.id = s.id
    );
    
    /*��������� ������� ����������� � �������*/
    /*update selection_discrepancy sd
    set 
         sd.stage = v_sel_send_discrep_status,
         sd.is_in_process = 0
    where 
         sd.selection_id in (select column_value from table(v_tab_ids))
         and sd.is_in_process = 1;*/
         
    /*��������� ���������� � ������ ��������*/
    /*update selection_declaration sdecl
    set
      sdecl.is_in_process = 0
    where 
    sdecl.selection_id not in (select column_value from table(v_tab_ids))
    and exists
    (
      select 
          1 
      from selection_declaration tmp 
      inner join (select column_value from table(v_tab_ids)) sids on tmp.selection_id = sids.column_value
      inner join selection s on tmp.selection_id = s.id
      where 
          tmp.declaration_id = sdecl.declaration_id
          and s.status not in (13,7,12,14, 10, 4) 
    );*/
    
    /*��������� ������� ����������� � ���� �������� �� ������� ����������*/
    /*update selection_discrepancy sd
    set
      sd.stage = v_sel_send_discrep_status
      ,sd.is_in_process = 0
    where 
    sd.selection_id not in (select column_value from table(v_tab_ids))
    and exists
    (
      select 
          1 
      from selection_discrepancy tmp 
      inner join (select column_value from table(v_tab_ids)) sids on tmp.selection_id = sids.column_value
      inner join selection s on tmp.selection_id = s.id
      where 
          tmp.discrepancy_id = sd.discrepancy_id
          and s.status not in (13,7,12,14, 10, 4)
    );*/
    
    /*
    update selection_discrepancy Z
    set 
     Z.STAGE = 4,
     Z.Is_In_Process = 0
    where Z.SELECTION_ID <> p_selectionId 
    and  exists (
    select 
     1
    from 
    (
     select 
      sd.discrepancy_id
     from
      selection s
     inner join selection_discrepancy sd on sd.selection_id = s.id and sd.is_in_process = 1 and sd.stage < 4
     where s.id = p_selectionId
     ) T
     inner join selection_discrepancy sd1 on T.DISCREPANCY_ID = sd1.discrepancy_id
     inner join selection s1 on s1.id = sd1.selection_id and s1.status not in (13,7,12,14)  
     where 
      s1.id = z.selection_id and sd1.discrepancy_id = z.discrepancy_id
    );*/
    
    
    /*��������� �������*/
    
end;

PROCEDURE SAVE_SELECTION
(
  p_selectionId IN OUT selection.id%type,
  p_name IN selection.name%type,
  p_analytic IN selection.analytic%type,
  p_analytic_sid IN selection.analytic_sid%type,
  p_chief IN selection.chief%type,
  p_chief_sid IN selection.chief_sid%type,
  p_sender IN selection.sender%type,
  p_status IN selection.status%type,
  p_status_date IN selection.status_date%type,
  p_creation_date in selection.creation_date%type,
  p_modification_date in selection.modification_date%type,
  p_comment in action_history.action_comment%type,
  p_save_reason in number/*1 - create, 2 - remove, 3 - status change, 4 - update*/
)
as
v_prev_status number;
begin

--select status into v_prev_status from selection where id = p_selectionId;

update SELECTION set
   name = p_name,
   analytic = p_analytic,
   analytic_sid = p_analytic_sid,
   chief = p_chief,
   sender = p_sender,
   status = p_status,
   modification_date = p_modification_date,
   status_date = p_status_date
where id = p_selectionId;

if sql%rowcount = 0 then
  begin
    p_selectionId := SELECTIONS_SEQ.NEXTVAL;

    insert into selection(id, name, creation_date, modification_date, status_date, analytic, analytic_sid, chief, chief_sid, sender, status, type)
    values(p_selectionId, p_name, p_creation_date, null, null, p_analytic, p_analytic_sid, p_chief, p_chief_sid, p_sender, p_status, 0);

    insert into Action_History (id, cd, status, change_date, action_comment, user_name, action) values(SEQ_ACTION_HISTORY.nextval, p_selectionId, p_status, sysdate, p_comment, p_analytic, 1);

    insert into SELECTION_FILTER (id, selection_id) values (p_selectionId, p_selectionId);
  end;
else
  begin
    insert into Action_History (id, cd, status, change_date, action_comment, user_name, action) values(SEQ_ACTION_HISTORY.nextval, p_selectionId, p_status, sysdate, p_comment, p_analytic, 4);
  end;
end if;
    
if p_save_reason = 3 then
  UPD_HIST_SELECTION_STATUS(p_selectionId, p_status);
  /*������� ������� � ������*/
  if p_status = 6 then --������������
    update selection_discrepancy Z
    set 
     Z.STAGE = 3,
     Z.Is_In_Process = 0
    where Z.SELECTION_ID <> p_selectionId 
    and  exists (
    select 
     1
    from 
    (
     select 
      sd.discrepancy_id
     from
      selection s
     inner join selection_discrepancy sd on sd.selection_id = s.id and sd.is_in_process = 1 and sd.stage < 3
     where s.id = p_selectionId
     ) T
     inner join selection_discrepancy sd1 on T.DISCREPANCY_ID = sd1.discrepancy_id
     inner join selection s1 on s1.id = sd1.selection_id and s1.status in (1,2,3,5) 
     where 
      s1.id = z.selection_id and sd1.discrepancy_id = z.discrepancy_id
    );
  end if;
  
  if p_status = 4 then --���������
    update selection_discrepancy Z
    set 
     Z.STAGE = 4,
     Z.Is_In_Process = 0
    where Z.SELECTION_ID <> p_selectionId 
    and  exists (
    select 
     1
    from 
    (
     select 
      sd.discrepancy_id
     from
      selection s
     inner join selection_discrepancy sd on sd.selection_id = s.id and sd.is_in_process = 1 and sd.stage < 4
     where s.id = p_selectionId
     ) T
     inner join selection_discrepancy sd1 on T.DISCREPANCY_ID = sd1.discrepancy_id
     inner join selection s1 on s1.id = sd1.selection_id and s1.status not in (13,7,12,14)  
     where 
      s1.id = z.selection_id and sd1.discrepancy_id = z.discrepancy_id
    );
  end if;
  /*������� ������� � ������*/

end if;
end;

PROCEDURE REMOVE_SELECTION
(pId IN NUMBER, analytic IN VARCHAR2)
AS
c1 number;
c2 number;
BEGIN
  select ID into c1 FROM DICT_SELECTION_STATUS WHERE NAME='�������';
  select ID into c2 from DICT_ACTIONS where ACTION_NAME='�������� �������';
  UPDATE SELECTION SET STATUS = c1 WHERE ID = pId;
  insert into Action_History values(SEQ_ACTION_HISTORY.nextval, pId, c1, sysdate, '', analytic, c2);
  UPD_HIST_SELECTION_STATUS(pId, c1);
END;


PROCEDURE SELECTION_SET_STATE
(pId IN NUMBER, statusId IN NUMBER, userName in varchar2)
AS
c1 number;
BEGIN
  UPDATE SELECTION SET STATUS = statusId WHERE ID = pId;
  select ID into c1 from DICT_ACTIONS where ACTION_NAME='��������� �������';
  insert into Action_History values(SEQ_ACTION_HISTORY.nextval, pId, statusId, sysdate, '', userName, c1);
  UPD_HIST_SELECTION_STATUS(pId, statusId);
END;




PROCEDURE SELECT_SELECTION
(selectionCursor OUT SYS_REFCURSOR) AS
BEGIN
  OPEN selectionCursor FOR
  SELECT
    SELECTION.ID,
    SELECTION.ANALYTIC,
    SELECTION.CHIEF,
    SELECTION.CREATION_DATE,
    SELECTION.NAME,
    SS.NAME
    FROM SELECTION
    LEFT JOIN DICT_SELECTION_STATUS SS ON SELECTION.STATUS = SS.ID;
END;

PROCEDURE SELECT_SELECTION_BY_NAME
(selectionStartName VARCHAR2, selectionCursor OUT SYS_REFCURSOR) AS
BEGIN
  OPEN selectionCursor FOR
  SELECT
    SELECTION.ID,
    SELECTION.ANALYTIC,
    SELECTION.CHIEF,
    SELECTION.CREATION_DATE,
    SELECTION.NAME,
    SS.NAME
    FROM SELECTION
    LEFT JOIN DICT_SELECTION_STATUS SS ON SELECTION.STATUS = SS.ID
    WHERE SELECTION.NAME LIKE selectionStartname;
END;

PROCEDURE GET_SELECTION_STATUS
(selectionId IN NUMBER, statusCursor OUT SYS_REFCURSOR)
AS
BEGIN
 OPEN statusCursor FOR
 SELECT STATUS FROM SELECTION WHERE ID = selectionId;
END;

PROCEDURE GET_FAVORITE_FILTERS (pAnalyst IN FAVORITE_FILTERS.ANALYST%type, pFavCursor OUT SYS_REFCURSOR)
AS
BEGIN
  OPEN pFavCursor FOR SELECT ID,NAME,ANALYST,FILTER FROM FAVORITE_FILTERS WHERE ANALYST = pAnalyst;
END;

PROCEDURE SAVE_FAVORITE_FILTER (
  pId IN OUT FAVORITE_FILTERS.ID%type,
  pName IN FAVORITE_FILTERS.NAME%type,
  pAnalyst IN FAVORITE_FILTERS.ANALYST%type,
  pFilter IN FAVORITE_FILTERS.FILTER%type)
AS
BEGIN
  IF pId IS NULL THEN
    BEGIN
      pId := SEQ_FAV_FILTER.NEXTVAL;
      INSERT INTO FAVORITE_FILTERS (ID,NAME,ANALYST,FILTER) VALUES (pId,pName,pAnalyst,pFilter);
    END;
  ELSE
    BEGIN
      UPDATE FAVORITE_FILTERS
      SET
             FILTER = pFilter
      WHERE
             ID = pId;
    END;
   END IF;
END;

PROCEDURE DELETE_FAVORITE_FILTER (pId IN FAVORITE_FILTERS.ID%type)
AS
BEGIN
  DELETE FROM FAVORITE_FILTERS WHERE ID = pId;
END;

PROCEDURE UPDATE_DECLARATION_CHECK_STATE (
  pSelectionId IN SELECTION.ID%type,
  pDeclarationId IN SOV_DECLARATION_INFO.ID%type,
  pCheckState IN SELECTION_DECLARATION.IS_IN_PROCESS%type)
AS
BEGIN
  UPDATE SELECTION_DISCREPANCY
  SET IS_IN_PROCESS = pCheckState
  WHERE
         SELECTION_ID = pSelectionId
         and decl_id = pDeclarationId
         and stage < 3; -- � ������ ���� ����������� ���� �������� ������ � �����������, ���������, ��� ��� ��� �� ����������� � ������ �������
         
  UPDATE SELECTION_DECLARATION
  SET IS_IN_PROCESS = pCheckState
  WHERE
       SELECTION_ID = pSelectionId
   AND DECLARATION_ID = pDeclarationId;
END;

PROCEDURE UPDATE_DISCREPANCY_CHECK_STATE (
  pSelectionId IN SELECTION.ID%type,
  pDiscrepancyId IN SOV_DISCREPANCY.ID%type,
  pCheckState IN SELECTION_DISCREPANCY.IS_IN_PROCESS%type,
  pStatus OUT NUMBER
  )
AS
BEGIN
  SELECT
  CASE WHEN max(nvl(t.stage, 1)) < 3 THEN 0 ELSE 1 END
  INTO pStatus
  from selection_discrepancy t
  WHERE t.discrepancy_id = pDiscrepancyId;

  IF pStatus = 0 THEN
  BEGIN
     IF pCheckState = 0 THEN
     BEGIN
        UPDATE SELECTION_DECLARATION t_decl_ref
        SET IS_IN_PROCESS = pCheckState
        WHERE
          SELECTION_ID = pSelectionId
          AND NOT EXISTS
          (
             SELECT 1
             FROM SELECTION_DISCREPANCY xRef
             WHERE t_decl_ref.declaration_id = xRef.DECL_ID 
                   AND xRef.SELECTION_ID = pSelectionId 
                   AND xRef.DISCREPANCY_ID <> pDiscrepancyId
                   and xRef.Is_In_Process = 1
          );
     END;
     ELSE
     BEGIN
         UPDATE SELECTION_DECLARATION t_decl_ref
         SET IS_IN_PROCESS = 1
         WHERE t_decl_ref.SELECTION_ID = pSelectionId
         AND EXISTS
         (
             SELECT 1
             FROM SELECTION_DISCREPANCY xRef
             WHERE xRef.DECL_ID = t_decl_ref.DECLARATION_ID
             AND xRef.DISCREPANCY_ID = pDiscrepancyId
             and xRef.Is_In_Process = 0
         );
    END;
    END IF;

    UPDATE SELECTION_DISCREPANCY
    SET IS_IN_PROCESS = pCheckState
    WHERE
            SELECTION_ID = pSelectionId
        AND DISCREPANCY_ID = pDiscrepancyId;
    END;
END IF;
END;

PROCEDURE ALL_DECLARATIONS_CHECK_STATE (
  pSelectionId IN SELECTION.ID%type,
  pCheckState IN SELECTION_DECLARATION.IS_IN_PROCESS%type)
AS
BEGIN

  UPDATE SELECTION_DISCREPANCY main
  SET IS_IN_PROCESS = pCheckState
  WHERE
      main.SELECTION_ID = pSelectionId
      and main.stage < 3
      and main.is_in_process <> pCheckState;
      
   UPDATE SELECTION_DECLARATION SET IS_IN_PROCESS = pCheckState WHERE SELECTION_ID = pSelectionId;
END;

PROCEDURE ALL_DISCREPANCIES_CHECK_STATE (
  pSelectionId IN SELECTION.ID%type,
  pCheckState IN SELECTION_DISCREPANCY.IS_IN_PROCESS%type,
  pStatus OUT NUMBER
  )
AS
BEGIN

   SELECT SUM(CASE WHEN pCheckState=1 AND STAGE >=3 THEN 1 ELSE 0 END)
   INTO pStatus
   FROM SELECTION_DISCREPANCY 
   WHERE SELECTION_ID = pSelectionId;

   UPDATE SELECTION_DECLARATION t_decl_ref
   SET IS_IN_PROCESS = pCheckState
   WHERE  t_decl_ref.SELECTION_ID=pSelectionId
      AND EXISTS(
          SELECT 1
          FROM
           SELECTION_DISCREPANCY t_disc_ref
          WHERE
             t_disc_ref.DECL_ID=t_decl_ref.DECLARATION_ID
             AND t_disc_ref.SELECTION_ID=pSelectionId
        AND (pCheckState = 0 OR t_disc_ref.STAGE < 3)
      );

   UPDATE SELECTION_DISCREPANCY t_ref
   SET IS_IN_PROCESS = pCheckState
   WHERE     
   t_ref.SELECTION_ID=pSelectionId
   and t_ref.STAGE < 3;
         
END;

PROCEDURE SYNC_DECLARATIONS_CHECK_STATE (
  pSelectionId IN SELECTION.ID%type,
  pTargetCheckState IN SELECTION_DECLARATION.IS_IN_PROCESS%type
  )
AS
BEGIN
  IF pTargetCheckState = 0 THEN
     BEGIN
       UPDATE SELECTION_DECLARATION main
       SET IS_IN_PROCESS = 0
       WHERE main.SELECTION_ID = pSelectionId
             AND main.IS_IN_PROCESS = 1
             AND NOT EXISTS (
                 SELECT 1
                 FROM
                        SELECTION_DISCREPANCY xRef
                        JOIN SOV_DISCREPANCY x ON x.ID = xRef.DISCREPANCY_ID
                 WHERE xRef.SELECTION_ID = pSelectionId
                       AND x.DECL_ID = main.Declaration_Id
                       AND xRef.IS_IN_PROCESS = 1
             );
     END;
  ELSE
     BEGIN
       UPDATE SELECTION_DECLARATION main
       SET IS_IN_PROCESS = 1
       WHERE main.SELECTION_ID = pSelectionId
             AND main.IS_IN_PROCESS = 0
             AND EXISTS (
                 SELECT 1
                 FROM
                        SELECTION_DISCREPANCY xRef
                        JOIN SOV_DISCREPANCY x ON x.ID = xRef.DISCREPANCY_ID
                 WHERE xRef.SELECTION_ID = pSelectionId
                       AND x.DECL_ID = main.Declaration_Id
                       AND xRef.IS_IN_PROCESS = 1
             );
     END;
  END IF;
END;

PROCEDURE COUNT_CHECKED_DECLARATIONS (
  pSelectionId IN SELECTION.ID%type,
  pChecked OUT NUMBER,
  pAll OUT NUMBER
  )
AS
BEGIN
  SELECT
       SUM(is_in_process),
       COUNT(1)
  INTO pChecked, pAll
  FROM selection_declaration
  WHERE SELECTION_ID = pSelectionId;
END;

PROCEDURE COUNT_CHECKED_DISCREPANCIES (
  pSelectionId IN SELECTION.ID%type,
  pDisabled OUT NUMBER,
  pChecked OUT NUMBER,
  pAll OUT NUMBER,
  pInWorked OUT NUMBER
  )
AS
BEGIN
  SELECT
       SUM(CASE WHEN (sd.stage >= 3) THEN 1 ELSE 0 END) as disabled,
       SUM(sd.is_in_process) as is_in_process,
       COUNT(1) as all_count,
       SUM(CASE WHEN (sd.stage in (1,2,3)) and (sd.is_in_process = 1) THEN 1 ELSE 0 END) as cnt_in_work
  INTO pDisabled, pChecked, pAll, pInWorked
  FROM selection_discrepancy sd
  where sd.selection_id = pSelectionId;

END;


/*
������� ������� ����� ��������� �������
*/
PROCEDURE UPD_HIST_SELECTION_STATUS
(
  p_id in selection.id%type,
  p_status in selection.status%type
)
as
begin

  insert into HIST_SELECTION_STATUS (ID, DT, VAL)
         values(p_id, sysdate, p_status);

end;


/*
�������� ��������� ���������
*/
PROCEDURE GET_SELECTION_TRANSITIONS
(pFavCursor OUT SYS_REFCURSOR)
AS
BEGIN
  OPEN pFavCursor FOR
  SELECT STATE_FROM, STATE_TO, OPERATION FROM SELECTION_TRANSITION;
END;

PROCEDURE INSERT_ACTION_HISTORY (
    pSelectionId IN ACTION_HISTORY.CD%type,
    pComment IN ACTION_HISTORY.ACTION_COMMENT%type,
    pUser IN ACTION_HISTORY.USER_NAME%type,
    pAction IN ACTION_HISTORY.ACTION%type
)
AS
BEGIN

    INSERT INTO ACTION_HISTORY(ID,CD,STATUS,CHANGE_DATE,ACTION_COMMENT,USER_NAME,ACTION)
           SELECT SEQ_ACTION_HISTORY.nextval, t.ID, t.STATUS, Sysdate, pComment, pUser, pAction
           FROM SELECTION t
           WHERE t.ID = pSelectionId;

END;

PROCEDURE GET_DISCREPANCY (
  pDiscrepancyId IN SELECTION_DISCREPANCY.DISCREPANCY_ID%type,
  pSelectionId IN SELECTION.ID%type,
  pCursor OUT SYS_REFCURSOR
  )
AS
BEGIN
  OPEN pCursor FOR SELECT * FROM V$SELECTION_DISCREPANCY WHERE DISCREPANCYID = pDiscrepancyId AND selection_id = pSelectionId;
END;

PROCEDURE P$GET_REGIONS (
  pUserSID IN METODOLOG_CHECK_EMPLOYEE.SID%type, -- ������������� ������������
  pData OUT SYS_REFCURSOR -- ������ ��������
)
AS
BEGIN
  OPEN pData FOR
  SELECT distinct reg.S_CODE, reg.S_NAME, reg.DESCRIPTION
  FROM V$SSRF reg
    LEFT OUTER JOIN METODOLOG_CHECK_EMPLOYEE mce ON mce.Sid = pUserSID
    LEFT OUTER JOIN Metodolog_Region rr ON rr.id = mce.resp_id AND reg.s_code = rr.code
  WHERE (EXISTS(SELECT 1 FROM CONFIGURATION WHERE parameter='restrict_by_region' and value = 'N') OR rr.id IS NOT NULL);
END;

PROCEDURE P$SEARCH_REGIONS (
  pUserSID IN USER_TO_N_SSRF.USER_SID%type, -- ������������� ������������
  pSearchKey IN VARCHAR2,
  pMaxQuantity IN NUMBER,
  pData OUT SYS_REFCURSOR -- ������ ��������
)
AS
BEGIN
  OPEN pData FOR
  WITH q AS (
       SELECT ROWNUM as IDX, v.*
       FROM (
              SELECT DISTINCT reg.S_CODE, reg.S_NAME
              FROM
                     V$SSRF reg
                     LEFT OUTER JOIN METODOLOG_CHECK_EMPLOYEE mce ON mce.Sid = pUserSID
                     LEFT OUTER JOIN Metodolog_Region rr ON rr.id = mce.resp_id AND reg.s_code = rr.code
              WHERE
                     (EXISTS(SELECT 1 FROM CONFIGURATION WHERE parameter='restrict_by_region' and value = 'N') OR rr.id IS NOT NULL)
                     AND (reg.S_CODE LIKE pSearchKey OR reg.S_NAME LIKE pSearchKey)
              ORDER BY reg.s_name
            ) v)
    SELECT DISTINCT vw.S_CODE, vw.S_NAME, vw.DESCRIPTION
    FROM
            V$SSRF vw
            JOIN q ON q.S_CODE=vw.S_CODE
    WHERE q.IDX <= pMaxQuantity;
END;



PROCEDURE P$GET_INSPECTIONS (
  pUserSID IN METODOLOG_CHECK_EMPLOYEE.SID%type, -- ������������� ������������
  pData OUT SYS_REFCURSOR -- ������ ���������
  ) -- ���������� ������ ���������, ��������� ������������ (��� ������� �������)
AS
BEGIN
  OPEN pData FOR
  SELECT distinct ins.s_code, ins.s_parent_code, ins.s_name
  FROM v$inspection ins
    LEFT OUTER JOIN METODOLOG_CHECK_EMPLOYEE mce ON mce.Sid = pUserSID
    LEFT OUTER JOIN Metodolog_Region_Inspection mci ON mci.Region_Id = mce.resp_id AND mci.code = ins.s_code
  WHERE (EXISTS(SELECT 1 FROM CONFIGURATION WHERE parameter='restrict_by_region' and value = 'N') OR mci.Region_Id IS NOT NULL);
END;

PROCEDURE P$SEARCH_INSPECTIONS (
  pUserSID IN USER_TO_N_SSRF.USER_SID%type, -- ������������� ������������
  pSearchKey IN VARCHAR2,
  pMaxQuantity IN NUMBER,
  pData OUT SYS_REFCURSOR -- ������ ���������
  ) -- ���������� ������ ���������, ��������� ������������ (��� ������� �������)
AS
BEGIN
  OPEN pData FOR
  WITH q AS (
       SELECT ROWNUM as IDX, v.*
       FROM (
            SELECT distinct ins.s_code, ins.s_parent_code, ins.s_name
            FROM
                   v$inspection ins
                   LEFT OUTER JOIN METODOLOG_CHECK_EMPLOYEE mce ON mce.Sid = pUserSID
                   LEFT OUTER JOIN Metodolog_Region_Inspection mci ON mci.Region_Id = mce.resp_id AND mci.code = ins.s_code
            WHERE
                   (EXISTS(SELECT 1 FROM CONFIGURATION WHERE parameter='restrict_by_region' and value = 'N') OR mci.Region_Id IS NOT NULL) AND
                   (ins.s_code LIKE pSearchKey OR ins.s_name LIKE pSearchKey)
            ORDER BY ins.s_name) v)
   SELECT DISTINCT ins.s_code, ins.s_parent_code, ins.s_name
   FROM v$inspection ins JOIN q ON q.s_code=ins.s_code
   WHERE q.idx <= pMaxQuantity;
END;

procedure P$START_CANCEL_SELECTION
  as
begin
  NDS2$SYS.LOG_INFO(
  P_SITE => 'NDS2$REPORTS.START_CANCEL_SELECTION',   P_ENTITY_ID => null,
  P_MESSAGE_CODE => SQLCODE,   P_MESSAGE => 'starting');
  begin
    P$DO_CANCEL_SELECTION;
  exception when others then null;
  end;
end;

procedure P$DO_CANCEL_SELECTION
as
  CURSOR selections IS
  SELECT id FROM SELECTION WHERE STATUS in (1, 2, 3);
  v_selections selections%ROWTYPE;
  pCountAll number(19);
  pCountClose number(19);
begin

  OPEN selections;
  FETCH selections INTO v_selections;
  LOOP

    pCountClose := 0;
    pCountAll := 0;
    select a.CountClose, a.CountAll into pCountClose, pCountAll from dual
    left outer join
    (
      select
         count(case when dis.status = 2 then dis.id end) as CountClose
        ,count(dis.id) as CountAll
      from sov_discrepancy dis
      where dis.id in
      (
        select sd.discrepancy_id from SELECTION_DISCREPANCY sd where sd.selection_id = v_selections.id
        and sd.IS_IN_PROCESS != 0
      )
    ) a on 1 = 1;

    if pCountAll > 0 and pCountClose = pCountAll then
    begin
       update SELECTION s set status = 12, status_date = SYS_EXTRACT_UTC(SYSTIMESTAMP)
       where s.id = v_selections.id and s.status in (1, 2, 3);
       commit;
    end;
    end if;

    FETCH selections INTO v_selections;
    EXIT WHEN selections%NOTFOUND;
  END LOOP;

  CLOSE selections;

end;

procedure MERGE_SENDED_SELECTIONS
as
  error_sc  number := 0;
begin
  for row in
  (
    select id
    from selection sel
    where sel.status = 4
  )
  loop

  -- �������� ������� �������� �� �� ������������
  select count(*) into error_sc
  from
  (
    select seld.selection_id
    from selection_discrepancy seld
    left join SOV_DISCREPANCY sd on sd.id = seld.discrepancy_id
    left join SOV_INVOICE_ALL inv_all on inv_all.row_key = sd.invoice_rk or inv_all.row_key = sd.invoice_contractor_rk
    where seld.selection_id = row.id and inv_all.row_key is null
  ) a;

  if error_sc = 0 then

    insert into hist_discrepancy_stage(id, submit_date, stage_id, status_id)
    select dis.id, sysdate, 1, 1
    from sov_discrepancy dis
    left join hist_discrepancy_stage hist on hist.id = dis.id and hist.stage_id = 1 and hist.status_id = 1
    where hist.id is null;

    update selection
    set status = 13
    where id = row.id;

  else

    update selection
    set status = 11
    where id = row.id;

    for err in
    (
        select inv_all.row_key as key
        from selection_discrepancy seld
        left join SOV_DISCREPANCY sd on sd.id = seld.discrepancy_id
        left join SOV_INVOICE_ALL inv_all on inv_all.row_key = sd.invoice_rk or inv_all.row_key = sd.invoice_contractor_rk
        where seld.selection_id = row.id and inv_all.row_key is null
    )
    loop
      dbms_output.put_line(TO_CHAR(err.key) || ' - error NOT FOUND invoice key IN SOV_INVOICE_ALL for selection id: ' || TO_CHAR(row.id));

      NDS2$SYS.LOG_INFO(
      P_SITE => 'Nds2$Selections.MERGE_SENDED_SELECTIONS',   P_ENTITY_ID => null,
      P_MESSAGE_CODE => SQLCODE,   P_MESSAGE => TO_CHAR(err.key) || ' - error NOT FOUND invoice key IN SOV_INVOICE_ALL for selection id: ' || TO_CHAR(row.id));
    end loop;

  end if;

  end loop;
  commit;
end;



procedure P$REMOVE_SELECTION_RESULTS
(
  p_selection_id IN SELECTION.ID%type
)
as
begin
  delete from SELECTION_DISCREPANCY where selection_id = p_selection_id;
  delete from SELECTION_DECLARATION where selection_id = p_selection_id;
end;

procedure P$FILL_SELECTION_DECLARATION
(
  p_selection_id IN SELECTION.ID%type
)
as
begin
  insert into SELECTION_DECLARATION (selection_id, declaration_id, is_in_process)
  select p_selection_id, decl_id, 1 from
  (
    select distinct dis.decl_id as decl_id from SELECTION_DISCREPANCY sd
    join sov_discrepancy dis on dis.id = sd.discrepancy_id
    where selection_id = p_selection_id
  );
end;

procedure P$LOAD_SELECTION ( -- ��������� ���������������� ������ �������
  pId IN SELECTION.ID%type,
  pCursor OUT SYS_REFCURSOR
)
as
begin

  open pCursor for
       select
               s.id,
               s.name as name,
               s.creation_date as CreationDate,
               s.modification_date as ModificationDate,
               s.status_date as StatusDate,
               s.analytic_sid as Analytic_SID,
               s.analytic as Analytic,
               s.chief_sid as Manager_SID,
               s.chief as Manager,
               flt.filter,
               sel_type.s_code as TypeCode,
               sel_type.s_name as Type,
               sel_status.id as Status,
               sel_status.name as StatusName,
               sel_status.rank,
               -1 as declarationcount,
               -1 as decl_pvp_total,
               -1 as discrepanciescount,
               -1 as totalamount,
               -1 as selecteddiscrepanciespvpamount,
               null as RegionCode,
               '' as RegionName,
               '' as RegionNames
        from
                  SELECTION s
             join DICT_SELECTION_STATUS sel_status on s.status = sel_status.id
             join DICT_SELECTION_TYPE sel_type on s.type = sel_type.s_code
             join SELECTION_FILTER flt on flt.selection_id = s.id
/*             left join (
                  select
                       sd.selection_id,
                       count(1) as declarationcount,
                       sum(d.pvp_total_amnt) as decl_pvp_total
                  from
                       SELECTION_DECLARATION sd
                       join SOV_DECLARATION_INFO d on d.id = sd.declaration_id
                  where sd.is_in_process = 1 and sd.selection_id = pId
                  group by sd.selection_id
             ) sel_decl on sel_decl.selection_id = s.id
             left join (
                  select
                       sdis.selection_id,
                       count(1) as discrepancycount,
                       sum(dis.amnt) as discrepancyamount,
                       sum(dis.amount_pvp) as discrepancypvp
                  from
                       SELECTION_DISCREPANCY sdis
--                       join SOV_DISCREPANCY dis on dis.id = sdis.discrepancy_id
                  where sdis.is_in_process = 1 and sdis.selection_id = pId
                  group by sdis.selection_id
             ) sel_discrep on sel_discrep.selection_id = s.id*/
/*             left join (
                  select
                       selection_id,
                       substr(max(sys_connect_by_path(regionInfo, ', ' )), 3) regions,
                       substr(min(sys_connect_by_path(regionInfo, ', ' )), 3) || (case when count(*) > 1 then ' ...' else '' end) as RegionFirst
                       from (
                            select
                                rf.selection_id,
                                d.regionInfo,
                                min(d.id) as id,
                                row_number() over (partition by rf.selection_id order by d.regionInfo) rn
                            from SELECTION_DECLARATION rf
                                 inner join (
                                     select
                                       v.*,
                                       (v.REGION_CODE || ' - ' || v.REGION) as regionInfo
                                     from V$Declaration v
                                  ) d on d.id = rf.declaration_id
                             where rf.selection_id = pId
                             group by rf.selection_id, d.regionInfo
                             order by rf.selection_id, d.regionInfo
                       )
                       start with rn = 1
                       connect by prior rn = rn-1 and prior selection_id = selection_id
                       group by selection_id
                       order by selection_id
               ) regionAgr on regionAgr.selection_id = s.id*/
              where s.id = pId;
end;



end Nds2$Selections;
/
