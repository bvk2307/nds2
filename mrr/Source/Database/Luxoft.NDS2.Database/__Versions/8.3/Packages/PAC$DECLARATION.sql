create or replace package NDS2_MRR_USER.PAC$DECLARATION is

procedure P$GET_DOC_KNP
(
  pDeclId in mv$declaration_history.id%type, 
  pRowFrom in NUMBER,
  pRowTo in NUMBER,
  pCursor out SYS_REFCURSOR
);

procedure P$GET_COUNT_DOC_KNP
(
  pDeclId in mv$declaration_history.id%type, 
  pTotalRowsCount out NUMBER
);

procedure P$GET_DECLARATION(pZip in mv$declaration_history.declaration_version_id%type, pCursor out SYS_REFCURSOR);

procedure UPDATE_CONTRACTOR_AGGR_START;

procedure P$UPDATE_CONTRACTOR_AGGR;

procedure P$GET_CONTRACTOR_DECL_ZIP(
  pInn in V$ask_Declandjrnl.Innnp%type,
  pYear in V$ask_Declandjrnl.Otchetgod%type,
  pMonth in dict_tax_period_month.month%type,
  pCursor out SYS_REFCURSOR
  );
  
procedure P$BOOK_DATA_REQUEST_STATUS (
  pRequestId in BOOK_DATA_REQUEST.ID%type,
  pData out SYS_REFCURSOR
  ); -- ���������� ������ ������� ��������� ������ ����� ����������

procedure P$BOOK_DATA_REQUEST_EXISTS (
  pRequestId out BOOK_DATA_REQUEST.ID%type,
  pStatus out BOOK_DATA_REQUEST.Status%type,
  pInn in BOOK_DATA_REQUEST.INN%type,
  pCorrectionNumber in BOOK_DATA_REQUEST.CORRECTIONNUMBER%type,
  pPartitionNumber in BOOK_DATA_REQUEST.PARTITIONNUMBER%type,
  pPeriod in BOOK_DATA_REQUEST.PERIOD%type,
  pYear in BOOK_DATA_REQUEST.YEAR%type
 ); -- ���������� ������������� ������� ������ �/� �� ���������� �������


 procedure P$NDSDISCREPANCY_REQ_STATUS (
  pRequestId in DISCREPANCY_REQUEST.ID%type,
  pData out SYS_REFCURSOR
  ); -- ���������� ������ ������� ����������� �� ����������


procedure P$NDSDISCREPANCY_REQ_EXISTS (
  pRequestId out DISCREPANCY_REQUEST.ID%type,
  pInn in DISCREPANCY_REQUEST.INN%type,
  pCorrectionNumber in DISCREPANCY_REQUEST.CORRECTION_NUMBER%type,
  pPeriod in DISCREPANCY_REQUEST.PERIOD%type,
  pYear in DISCREPANCY_REQUEST.YEAR%type,
  pKind in DISCREPANCY_REQUEST.KIND%type,
  pType in DISCREPANCY_REQUEST.TYPE%type
 ); -- ���������� ������������� ������� ����������� �� ����������

end PAC$DECLARATION;
/
create or replace package body NDS2_MRR_USER.PAC$DECLARATION is

procedure P$GET_DOC_KNP
(
  pDeclId in mv$declaration_history.id%type, 
  pRowFrom in NUMBER,
  pRowTo in NUMBER,
  pCursor out SYS_REFCURSOR
)
as
begin
  open pCursor for
  WITH mnk AS (
  select
    SEOD_KNP_DECISION_TYPE.DESCRIPTION as NAME,
    SEOD_KNP_DECISION.DOC_NUM as NUM,
    SEOD_KNP_DECISION.DOC_DATE as DT,
    DECL.ID as DECL_ID,
    DECL.DECL_DATE as LEVEL1_DATE,
    DECL.CORRECTION_NUMBER as LEVEL1_NUM,
    SEOD_KNP_DECISION.DOC_DATE as LEVEL3_DATE,
    SEOD_KNP_DECISION.DOC_NUM as LEVEL3_NUM
  from SEOD_KNP
    inner join SEOD_KNP_DECISION on SEOD_KNP_DECISION.KNP_ID = SEOD_KNP.KNP_ID
    inner join SEOD_KNP_DECISION_TYPE on SEOD_KNP_DECISION_TYPE.ID = SEOD_KNP_DECISION.TYPE_ID
    inner join v$declaration_history DECL on DECL.SEOD_DECL_ID = SEOD_KNP.DECLARATION_REG_NUM 
    where DECL.ID = pDeclId
  )
  select m.*
  from
  (
     select  
         row_number() over (ORDER BY LEVEL1_DATE, LEVEL1_NUM, LEVEL2_DATE, LEVEL2_NUM, LEVEL3_DATE, LEVEL3_NUM ASC) rn,
         t.*
     from
     (
        select
          '�������������' as name,
          DECL.correction_number as num,
          DECL.decl_date as dt,
          case DECL.is_active when 0 then '�� ����������' else '����������' end as status,
          DECL.ID as DECL_ID,
          DECL.DECL_DATE as LEVEL1_DATE,
          DECL.CORRECTION_NUMBER as LEVEL1_NUM,
          TO_DATE ('01-JAN-1800', 'DD-MON-YYYY') as LEVEL2_DATE,
          '0' as LEVEL2_NUM,
          TO_DATE ('01-JAN-1800', 'DD-MON-YYYY') as LEVEL3_DATE,
          '0' as LEVEL3_NUM,
          null as ID,
          null as type_doc_knp,
          null as explain_can_open
        from V$DECLARATION_HISTORY DECL
        where DECL.ID = pDeclId
        union
        select
          '&nbsp;&nbsp;&nbsp;&nbsp;' ||
          DOC_TYPE.DESCRIPTION as name,
          DOC.NUM as num,
          DOC.DT as DT,
          DOC_STATUS.DESCRIPTION as STATUS,
          DECL.ID as DECL_ID,
          DECL.DECL_DATE as LEVEL1_DATE,
          DECL.CORRECTION_NUMBER as LEVEL1_NUM,
          nvl(DOC.DT, TO_DATE ('01-DEC-4712', 'DD-MON-YYYY'))  as LEVEL2_DATE,
          DOC.NUM as LEVEL2_NUM,
          TO_DATE ('01-JAN-1800', 'DD-MON-YYYY') as LEVEL3_DATE,
          '0' as LEVEL3_NUM,
          DOC.DOC_ID as ID,
          1 as type_doc_knp,
          null as explain_can_open
        from V$DOC doc
             inner join V$DECLARATION_HISTORY decl on DECL.SEOD_DECL_ID = doc.ref_entity_id
             inner join DOC_TYPE on DOC_TYPE.ID = DOC.Doc_Type
             left join DOC_STATUS on (DOC_STATUS.ID = DOC.STATUS) and (DOC_STATUS.DOC_TYPE = DOC.DOC_TYPE)
        where DECL.ID = pDeclId AND DOC.DOC_TYPE <> 5
        union
        select
          '&nbsp;&nbsp;&nbsp;&nbsp;' ||
          DOC_TYPE.DESCRIPTION as name,
          DOC.NUM as num,
          DOC.DT as DT,
          DOC_STATUS.DESCRIPTION as STATUS,
          DECL.ID as DECL_ID,
          DECL.DECL_DATE as LEVEL1_DATE,
          DECL.CORRECTION_NUMBER as LEVEL1_NUM,
          nvl(DOC.DT, TO_DATE ('01-DEC-4712', 'DD-MON-YYYY'))  as LEVEL2_DATE,
          DOC.NUM as LEVEL2_NUM,
          TO_DATE ('01-JAN-1800', 'DD-MON-YYYY') as LEVEL3_DATE,
          '0' as LEVEL3_NUM,
          DOC.DOC_ID as ID,
          1 as type_doc_knp,
          null as explain_can_open
        from V$DOC doc
          inner join V$DECLARATION_HISTORY decl on DECL.SEOD_DECL_ID = doc.ref_entity_id
          inner join DOC_TYPE on DOC_TYPE.ID = DOC.Doc_Type
          inner join
          (
                select vd.ID as DECL_ID
                from V$DECLARATION_HISTORY vd
                join v$askkontrsoontosh ks on ks.IdDekl = vd.ASK_DECL_ID
                group by vd.ID
                having sum(case when ks.Vypoln = 0 then 1 else 0 end) > 0 
          ) T1 on T1.DECL_ID = decl.ID
            left join DOC_STATUS on (DOC_STATUS.ID = DOC.STATUS) and (DOC_STATUS.DOC_TYPE = DOC.DOC_TYPE)
        where DECL.ID = pDeclId AND DOC.DOC_TYPE = 5
        union
        select
          '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' ||
          case
            when (REPL.RECEIVE_BY_TKS = 0) and (REPL.STATUS_ID = 1)
              then '<span style="color:Red; font-family:Segoe UI Symbol;">&#x2691;</span>'
            else '<span style="font-family:Segoe UI Symbol;">&nbsp;</span>'
          end ||
          nvl(dert.short_name, '')
          as NAME,
          nvl(REPL.INCOMING_NUM, '') as NUM,
          REPL.INCOMING_DATE as DT,
          ders.NAME as STATUS,
          DECL.ID as DECL_ID,
          DECL.DECL_DATE as LEVEL1_DATE,
          DECL.CORRECTION_NUMBER as LEVEL1_NUM,
          nvl(DOC.DT, TO_DATE ('01-DEC-4712', 'DD-MON-YYYY')) as LEVEL2_DATE,
          DOC.NUM as LEVEL2_NUM,
          REPL.INCOMING_DATE as LEVEL3_DATE,
          REPL.INCOMING_NUM as LEVEL3_NUM,
          REPL.EXPLAIN_ID as ID,
          2 as type_doc_knp,
          (case when (REPL.Status_Id = 2 OR REPL.Status_Id = 3) then 1
               when (SELECT count(explain_id) FROM SEOD_EXPLAIN_REPLY se
                     WHERE se.doc_id = REPL.DOC_ID AND se.explain_id <> REPL.EXPLAIN_ID
                     AND se.incoming_date < REPL.INCOMING_DATE) = 0 then 1
               when (SELECT count(explain_id) FROM SEOD_EXPLAIN_REPLY se
                     WHERE se.doc_id = REPL.DOC_ID AND se.explain_id <> REPL.EXPLAIN_ID AND se.status_id = 1
                          AND se.incoming_date < REPL.INCOMING_DATE) = 0 then 1
              else 0 end) as explain_can_open
        from SEOD_EXPLAIN_REPLY REPL
          inner join V$DOC doc on DOC.DOC_ID = REPL.DOC_ID
          inner join v$declaration_history DECL on DECL.SEOD_DECL_ID = doc.ref_entity_id
          inner join DICT_EXPLAIN_REPLY_TYPE dert on dert.id = REPL.TYPE_ID
          inner join DICT_EXPLAIN_REPLY_STATUS ders on ders.id = REPL.status_id
        where DECL.ID = pDeclId
        union
        select
          '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' ||
          NAME as NAME,
          NUM,
          DT,
          null as STATUS,
          DECL_ID,
          LEVEL1_DATE,
          LEVEL1_NUM,
          TO_DATE ('01-DEC-4712', 'DD-MON-YYYY') as LEVEL2_DATE,
          '100000000' as LEVEL2_NUM,
          LEVEL3_DATE,
          LEVEL3_NUM,
          null as ID,
          null as type_doc_knp,
          null as explain_can_open
        from mnk
        union
        select
          '&nbsp;&nbsp;&nbsp;&nbsp;' ||
          '������ ���' as NAME,
          null as NUM,
          null as DT,
          null as STATUS,
          DECL_ID,
          LEVEL1_DATE,
          LEVEL1_NUM,
          TO_DATE ('01-DEC-4712', 'DD-MON-YYYY') as LEVEL2_DATE,
          '100000000' as LEVEL2_NUM,
          TO_DATE ('01-JAN-1800', 'DD-MON-YYYY') as LEVEL3_DATE,
          '0' as LEVEL3_NUM,
          null as ID,
          null as type_doc_knp,
          null as explain_can_open
        from mnk
        group by DECL_ID, LEVEL1_DATE, LEVEL1_NUM
        order by LEVEL1_DATE, LEVEL1_NUM, LEVEL2_DATE, LEVEL2_NUM, LEVEL3_DATE, LEVEL3_NUM
     ) t
  ) m
  where m.rn >= pRowFrom
  and m.rn <= pRowTo 
  order by m.rn asc;
end;

procedure P$GET_COUNT_DOC_KNP
(
  pDeclId in mv$declaration_history.id%type, 
  pTotalRowsCount out NUMBER
)
as
  pCount01 NUMBER;
  pCount02 NUMBER;
  pCount03 NUMBER;
  pCount04 NUMBER;
  pCount05 NUMBER;
  pCount06 NUMBER;
begin
  select count(*) into pCount01
  from V$DECLARATION_HISTORY DECL
  where DECL.ID = pDeclId;

  select count(*) into pCount02
  from V$DOC doc
       inner join v$declaration_history DECL on DECL.SEOD_DECL_ID = doc.ref_entity_id
       inner join DOC_TYPE on DOC_TYPE.ID = DOC.Doc_Type
       left join DOC_STATUS on (DOC_STATUS.ID = DOC.STATUS) and (DOC_STATUS.DOC_TYPE = DOC.DOC_TYPE)
  where DECL.ID = pDeclId AND DOC.DOC_TYPE <> 5;
        
  select count(*) into pCount03
  from V$DOC doc
    inner join v$declaration_history DECL on DECL.SEOD_DECL_ID = doc.ref_entity_id
    inner join DOC_TYPE on DOC_TYPE.ID = DOC.Doc_Type
          inner join
          (
                select vd.ID as DECL_ID
                from V$DECLARATION_HISTORY vd
                join v$askkontrsoontosh ks on ks.IdDekl = vd.ASK_DECL_ID
                group by vd.ID
                having sum(case when ks.Vypoln = 0 then 1 else 0 end) > 0 
          ) T1 on T1.DECL_ID = decl.ID
      left join DOC_STATUS on (DOC_STATUS.ID = DOC.STATUS) and (DOC_STATUS.DOC_TYPE = DOC.DOC_TYPE)
  where DECL.ID = pDeclId AND DOC.DOC_TYPE = 5;
        
  select count(*) into pCount04
  from SEOD_EXPLAIN_REPLY REPL
    inner join V$DOC doc on DOC.DOC_ID = REPL.DOC_ID
    inner join v$declaration_history DECL on DECL.SEOD_DECL_ID = doc.ref_entity_id
    inner join DICT_EXPLAIN_REPLY_TYPE dert on dert.id = REPL.TYPE_ID
    inner join DICT_EXPLAIN_REPLY_STATUS ders on ders.id = REPL.status_id
  where DECL.ID = pDeclId;
        
  select count(*) into pCount05
  from SEOD_KNP
    inner join SEOD_KNP_DECISION on SEOD_KNP_DECISION.KNP_ID = SEOD_KNP.KNP_ID
    inner join SEOD_KNP_DECISION_TYPE on SEOD_KNP_DECISION_TYPE.ID = SEOD_KNP_DECISION.TYPE_ID
    inner join v$declaration_history DECL on DECL.SEOD_DECL_ID = SEOD_KNP.DECLARATION_REG_NUM 
    where DECL.ID = pDeclId;
        
  select count(*) into pCount06
  from
  (
    select DECL_ID, LEVEL1_DATE, LEVEL1_NUM from
    (
      select 
        SEOD_KNP_DECISION_TYPE.DESCRIPTION as NAME,
        SEOD_KNP_DECISION.DOC_NUM as NUM,
        SEOD_KNP_DECISION.DOC_DATE as DT,
        DECL.ID as DECL_ID,
        DECL.DECL_DATE as LEVEL1_DATE,
        DECL.CORRECTION_NUMBER as LEVEL1_NUM,
        SEOD_KNP_DECISION.DOC_DATE as LEVEL3_DATE,
        SEOD_KNP_DECISION.DOC_NUM as LEVEL3_NUM
      from SEOD_KNP
        inner join SEOD_KNP_DECISION on SEOD_KNP_DECISION.KNP_ID = SEOD_KNP.KNP_ID
        inner join SEOD_KNP_DECISION_TYPE on SEOD_KNP_DECISION_TYPE.ID = SEOD_KNP_DECISION.TYPE_ID
        inner join v$declaration_history DECL on DECL.SEOD_DECL_ID = SEOD_KNP.DECLARATION_REG_NUM 
      where DECL.ID = pDeclId  
    )
    group by DECL_ID, LEVEL1_DATE, LEVEL1_NUM
  );
  
  pTotalRowsCount := nvl(pCount01, 0) + nvl(pCount02, 0) + nvl(pCount03, 0) +
                     nvl(pCount04, 0) + nvl(pCount05, 0) + nvl(pCount06, 0);
    
end;


procedure P$GET_DECLARATION(pZip in mv$declaration_history.declaration_version_id%type, pCursor out SYS_REFCURSOR)
  as
begin
  open pCursor for
    select vd.*
           ,rp08.priority as priority_08 
           ,rp81.priority as priority_81 
           ,rp09.priority as priority_09 
           ,rp91.priority as priority_91 
           ,rp10.priority as priority_10 
           ,rp11.priority as priority_11 
           ,rp12.priority as priority_12
           ,req8.id as request_key8
           ,req81.id as request_key81
           ,req9.id as request_key9
           ,req91.id as request_key91
           ,req10.id as request_key10
           ,req11.id as request_key11
           ,req12.id as request_key12
    from v$declaration_history vd
    left join sov_invoice_request req8 on req8.zip = vd.actual_zip8 and req8.partition_number = 8
    left join sov_invoice_request req81 on req81.zip = vd.actual_zip81 and req81.partition_number = 81
    left join sov_invoice_request req9 on req9.zip = vd.actual_zip9 and req9.partition_number = 9
    left join sov_invoice_request req91 on req91.zip = vd.actual_zip91 and req91.partition_number = 91
    left join sov_invoice_request req10 on req10.zip = vd.actual_zip10 and req10.partition_number = 10
    left join sov_invoice_request req11 on req11.zip = vd.actual_zip11 and req11.partition_number = 11
    left join sov_invoice_request req12 on req12.zip = vd.actual_zip12 and req12.partition_number = 12
    left outer join BOOK_DATA_REQUEST_PRIORITY rp08 on vd.invoice_count8 >= rp08.min_count and 
         ((rp08.max_count is not null and vd.invoice_count8 <= rp08.max_count) or (rp08.max_count is null))
    left outer join BOOK_DATA_REQUEST_PRIORITY rp81 on vd.invoice_count81 >= rp81.min_count and 
         ((rp81.max_count is not null and vd.invoice_count81 <= rp81.max_count) or (rp81.max_count is null))
    left outer join BOOK_DATA_REQUEST_PRIORITY rp09 on vd.invoice_count9 >= rp09.min_count and 
         ((rp09.max_count is not null and vd.invoice_count9 <= rp09.max_count) or (rp09.max_count is null))
    left outer join BOOK_DATA_REQUEST_PRIORITY rp91 on vd.invoice_count91 >= rp91.min_count and 
         ((rp91.max_count is not null and vd.invoice_count91 <= rp91.max_count) or (rp91.max_count is null))
    left outer join BOOK_DATA_REQUEST_PRIORITY rp10 on vd.invoice_count10 >= rp10.min_count and 
         ((rp10.max_count is not null and vd.invoice_count10 <= rp10.max_count) or (rp10.max_count is null))
    left outer join BOOK_DATA_REQUEST_PRIORITY rp11 on vd.invoice_count11 >= rp11.min_count and 
         ((rp11.max_count is not null and vd.invoice_count11 <= rp11.max_count) or (rp11.max_count is null))
    left outer join BOOK_DATA_REQUEST_PRIORITY rp12 on vd.invoice_count12 >= rp12.min_count and 
         ((rp12.max_count is not null and vd.invoice_count12 <= rp12.max_count) or (rp12.max_count is null))
    where vd.declaration_version_id = pZip;
end;

procedure UPDATE_CONTRACTOR_AGGR_START
  as
begin
  NDS2$SYS.LOG_INFO(
  P_SITE => 'PAC$DECLARATION.UPDATE_INVOICE_CONTRACTOR_AGGR_START_WORK',   P_ENTITY_ID => null,
  P_MESSAGE_CODE => SQLCODE,   P_MESSAGE => 'starting');
  begin
    P$UPDATE_CONTRACTOR_AGGR;
  exception when others then null;
  end;
end;

procedure P$UPDATE_CONTRACTOR_AGGR
as
    pInsertDateBegin DATE;
    pInsertDateEnd DATE;
    pParameter VARCHAR2(128);
begin
    pParameter := 'seod_decl_key_last_processed';
    select max(to_date(value, 'DD/MM/YYYY')) into pInsertDateBegin
    from configuration where parameter = pParameter;

    if pInsertDateBegin is null then
      select max(insert_date) into pInsertDateBegin from INVOICE_CONTRACTOR_AGGR;
      if pInsertDateBegin is null then select nvl(min(insert_date), sysdate) into pInsertDateBegin from SEOD_DECLARATION; end if;
    end if;
    
    pInsertDateEnd := sysdate;

    UPDATE CONFIGURATION c
      SET c.value = to_char(pInsertDateEnd, 'DD/MM/YYYY'),
          c.default_value = to_char(pInsertDateEnd, 'DD/MM/YYYY')
    WHERE c.parameter = pParameter;

    INSERT INTO INVOICE_CONTRACTOR_AGGR agg
      (CONTRACTOR_KEY, EXISTS_IN_MC, INSERT_DATE, UPDATE_DATE)
    SELECT q.CONTRACTOR_KEY, 0 as EXISTS_IN_MC, sysdate as INSERT_DATE, sysdate as UPDATE_DATE FROM
      (SELECT
        (to_number(sed.fiscal_year) * 100000000000000 + tm.month * 1000000000000 + to_number(sed.inn)) as CONTRACTOR_KEY
        FROM SEOD_DECLARATION sed
      JOIN DICT_TAX_PERIOD_MONTH tm on tm.tax_period = sed.tax_period
      WHERE TRUNC(sed.INSERT_DATE) >= TRUNC(pInsertDateBegin)
      GROUP BY sed.fiscal_year, tm.month, sed.inn) q
    WHERE NOT EXISTS (SELECT 1 FROM INVOICE_CONTRACTOR_AGGR t WHERE t.contractor_key = q.CONTRACTOR_KEY);

    UPDATE INVOICE_CONTRACTOR_AGGR aggr SET EXISTS_IN_MC = 1, UPDATE_DATE = sysdate
    WHERE EXISTS_IN_MC = 0 AND EXISTS
    (
      select 1 from V$ASK_DECLANDJRNL sd
      join dict_tax_period_month m on m.tax_period = sd.period
      where sd.innnp = aggr.contractor_key - trunc(aggr.contractor_key, -12)
        and sd.otchetgod = trunc(aggr.contractor_key, -14)/power(10, 14)
        and m.month = trunc(aggr.contractor_key - trunc(aggr.contractor_key, -14), -12)/power(10, 12)
    );
    COMMIT;
end;

procedure P$GET_CONTRACTOR_DECL_ZIP(
  pInn in V$ask_Declandjrnl.Innnp%type,
  pYear in V$ask_Declandjrnl.Otchetgod%type,
  pMonth in dict_tax_period_month.month%type,
  pCursor out SYS_REFCURSOR
  )
as
begin
  open pCursor for
    select
      decl.zip,
      decl.nomkorr_rank as rank
    from V$ask_Declandjrnl decl
    join dict_tax_period_month m on m.tax_period = decl.period
    where decl.innnp = pInn and decl.otchetgod = pYear and m.month = pMonth;
end;

procedure P$BOOK_DATA_REQUEST_STATUS (
  pRequestId in BOOK_DATA_REQUEST.ID%type,
  pData out SYS_REFCURSOR
  )
as
begin

  open pData for select dict.* from REQUEST_STATUS dict join BOOK_DATA_REQUEST req on req.STATUS = dict.ID where req.ID = pRequestId;

end;

procedure P$BOOK_DATA_REQUEST_EXISTS (
  pRequestId out BOOK_DATA_REQUEST.ID%type,
  pStatus out BOOK_DATA_REQUEST.Status%type,
  pInn in BOOK_DATA_REQUEST.INN%type,
  pCorrectionNumber in BOOK_DATA_REQUEST.CORRECTIONNUMBER%type,
  pPartitionNumber in BOOK_DATA_REQUEST.PARTITIONNUMBER%type,
  pPeriod in BOOK_DATA_REQUEST.PERIOD%type,
  pYear in BOOK_DATA_REQUEST.YEAR%type
 )
as
begin 
 select ID, status
 into pRequestId, pStatus
 from
   (
     select ID, status
     from BOOK_DATA_REQUEST
     where
      INN = pInn 
      AND CORRECTIONNUMBER = pCorrectionNumber 
      AND PARTITIONNUMBER = pPartitionNumber 
      AND PERIOD = pPeriod 
      AND YEAR = pYear
      AND STATUS <> 9
      order by requestdate desc
   ) T
  where ROWNUM = 1;

 exception
   when NO_DATA_FOUND then
      pRequestId := null;
      pStatus := null;
end;


 procedure P$NDSDISCREPANCY_REQ_STATUS (
  pRequestId in DISCREPANCY_REQUEST.ID%type,
  pData out SYS_REFCURSOR
  )
as
begin

  open pData for select dict.* from REQUEST_STATUS dict join DISCREPANCY_REQUEST req on req.STATUS = dict.ID where req.ID = pRequestId;

end;


procedure P$NDSDISCREPANCY_REQ_EXISTS (
  pRequestId out DISCREPANCY_REQUEST.ID%type,
  pInn in DISCREPANCY_REQUEST.INN%type,
  pCorrectionNumber in DISCREPANCY_REQUEST.CORRECTION_NUMBER%type,
  pPeriod in DISCREPANCY_REQUEST.PERIOD%type,
  pYear in DISCREPANCY_REQUEST.YEAR%type,
  pKind in DISCREPANCY_REQUEST.KIND%type,
  pType in DISCREPANCY_REQUEST.TYPE%type
 )
as
begin

 select ID
 into pRequestId
 from DISCREPANCY_REQUEST
 where
  INN = pInn AND CORRECTION_NUMBER = pCorrectionNumber AND KIND = pKind AND TYPE = pType AND PERIOD = pPeriod AND YEAR = pYear
  AND STATUS <> 9 AND ROWNUM = 1;

 exception
   when NO_DATA_FOUND then
      pRequestId := null;
end;

end PAC$DECLARATION;
/
