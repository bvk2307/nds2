create table NDS2_MRR_USER.CONTROL_RATIO_QUEUE as
select
distinct
T.id,
T.sono_code,
sd.decl_reg_num,
0 as is_sent
from
(
select
distinct 
  d.ID,
  to_number(d.PERIOD||d.OTCHETGOD||d.INNNP) as nds2_id,
  d.NOMKORR as correction_number,
  d.KODNO as sono_code
from 
  NDS2_MRR_USER.V$ASKKontrSoontosh ks
inner join NDS2_MRR_USER.v$askdekl d on d.ID = ks.IdDekl
) T
inner join NDS2_MRR_USER.seod_declaration sd on sd.nds2_id = T.nds2_id and sd.correction_number = T.correction_number
left join NDS2_MRR_USER.doc dc on dc.ref_entity_id = sd.decl_reg_num and dc.sono_code = sd.sono_code and dc.doc_type = 1 and dc.doc_kind = 5
where dc.doc_id is null;

create index NDS2_MRR_USER.IX_CONTROL_RATIO_QUEUE on NDS2_MRR_USER.CONTROL_RATIO_QUEUE (id) nologging;
