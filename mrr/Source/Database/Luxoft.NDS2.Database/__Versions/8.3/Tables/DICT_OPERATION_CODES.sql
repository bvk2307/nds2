--drop table NDS2_MRR_USER.DICT_OPERATION_CODES;
create table NDS2_MRR_USER.DICT_OPERATION_CODES
(
  CODE                   VARCHAR2(2) not null, -- ��� ��� (01, 02 � �.�.)
  NUM_CODE               NUMBER not null,      -- �������� ��������
  BIT_CODE               NUMBER not null,      -- ������� ������
  NAME                   VARCHAR2(700) not null,
  constraint DICT_OPERATION_CODES_PK primary key (CODE)
);
