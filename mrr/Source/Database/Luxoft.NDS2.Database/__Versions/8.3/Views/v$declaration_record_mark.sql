create or replace force view nds2_mrr_user.v$declaration_record_mark
as
select decl.declaration_version_id,
  case
    when nvl(decl_tp.cnt, 0)  > 0 then '�-�'
    when nvl(decl_t.cnt, 0)   > 0 then '�'
    when nvl(decl_p.cnt, 0)   > 0 then '�'
    when nvl(decl_n.cnt, 0)   > 0 then '�'
    when nvl(decl_mnk.cnt, 0) > 0 then '���'
    else null
  end as record_mark
from sov_declaration_info decl
left join --�-�
(
  select decl.declaration_version_id, count(1) as cnt
  from sov_declaration_info decl
  inner join seod_declaration seod_decl on seod_decl.nds2_id = decl.id
    and seod_decl.correction_number = decl.correction_number
  inner join doc on doc.ref_entity_id = seod_decl.id
  inner join seod_explain_reply reply on reply.doc_id = doc.doc_id and reply.status_id = 1
  inner join doc_status_history sent on sent.doc_id = doc.doc_id
    and sent.status = 4      -- ���������� �����������������
  left join doc_status_history delivered on delivered.doc_id = doc.doc_id
    and delivered.status = 5 -- �������� ������������������
  where delivered.doc_id is null
    and utl_add_work_days(sent.status_date, utl_get_configuration_number('timeout_claim_delivery')) < sysdate
  group by decl.declaration_version_id
) decl_tp on decl_tp.declaration_version_id = decl.declaration_version_id
left join --�
(
  select decl.declaration_version_id, count(1) as cnt
  from sov_declaration_info decl
  inner join seod_declaration seod_decl on seod_decl.nds2_id = decl.id
    and seod_decl.correction_number = decl.correction_number
  inner join doc on doc.ref_entity_id = seod_decl.id
  inner join doc_status_history sent on sent.doc_id = doc.doc_id
    and sent.status = 4      -- ���������� �����������������
  left join doc_status_history delivered on delivered.doc_id = doc.doc_id
    and delivered.status = 5 -- �������� ������������������
  left join seod_explain_reply reply on reply.doc_id = doc.doc_id and reply.status_id = 1
  where delivered.doc_id is null
    and reply.doc_id is null
    and utl_add_work_days(sent.status_date, utl_get_configuration_number('timeout_claim_delivery')) < sysdate
  group by decl.declaration_version_id
) decl_t on decl_t.declaration_version_id = decl.declaration_version_id
left join --�
(
  select decl.declaration_version_id, count(1) as cnt
  from sov_declaration_info decl
  inner join seod_declaration seod_decl on seod_decl.nds2_id = decl.id
    and seod_decl.correction_number = decl.correction_number
  inner join doc on doc.ref_entity_id = seod_decl.id
  inner join seod_explain_reply reply on reply.doc_id = doc.doc_id and reply.status_id = 1
  group by decl.declaration_version_id
) decl_p on decl_p.declaration_version_id = decl.declaration_version_id
left join --�
(
  select decl.declaration_version_id, count(1) as cnt
  from sov_declaration_info decl
  left join declaration_owner owner on owner.declaration_id = decl.id
  left join sov_declaration_info decl_next on decl_next.id = decl.id and decl_next.correction_number > decl.correction_number
  where owner.declaration_id is null or decl_next.id is not null
  group by decl.declaration_version_id
) decl_n on decl_n.declaration_version_id = decl.declaration_version_id
left join --���
(
  select decl.declaration_version_id, count(1) as cnt
  from sov_declaration_info decl
  inner join sov_discrepancy dis on dis.decl_id = decl.id or dis.decl_contractor_id = decl.id
  left join seod_declaration seod_decl on seod_decl.nds2_id = decl.id
  left join seod_knp knp on knp.declaration_reg_num = seod_decl.id and knp.ifns_code = seod_decl.sono_code
  where dis.stage = 6 and knp.completion_date is null
  group by decl.declaration_version_id
) decl_mnk on decl_mnk.declaration_version_id = decl.declaration_version_id;
