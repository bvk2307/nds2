﻿CREATE OR REPLACE FORCE VIEW NDS2_MRR_USER.V$SELECTION_DISCREPANCY_LIST 
AS 
  select
sd.discrepancy_id as discrepancyid
,sd.is_in_process as ischecked
,sd.rule as compare_rule
,sd.DISCREPANCY_ID,
sd.IS_IN_PROCESS,
sd.SELECTION_ID,
sd.DECL_ID,
sd.AMOUNT,
sd.AMOUNTPVP,
sd.SOV_ID,
sd.FOUNDDATE,
sd.TYPE,
sd.RULE,
sd.STAGE,
sd.TAXPAYERINN,
sd.TAXPERIOD,
sd.SURCODE,
sd.TAXPAYERDECLSIGN,
sd.STATUS,
sd.invoice_chapter as TaxPayerSource
,null as TAXPAYERKPP
,null as TAXPAYERNAME
,null as TAXPAYERSOUN
from selection_discrepancy sd
;
