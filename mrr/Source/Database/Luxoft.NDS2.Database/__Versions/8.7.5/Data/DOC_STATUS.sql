﻿truncate table NDS2_MRR_USER.DOC_STATUS;
insert into NDS2_MRR_USER.DOC_STATUS values(-1, 1, 'Ручная проверка');
insert into NDS2_MRR_USER.DOC_STATUS values(1, 1, 'Создано');
insert into NDS2_MRR_USER.DOC_STATUS values(2, 1, 'Передано в СЭОД');
insert into NDS2_MRR_USER.DOC_STATUS values(3, 1, 'Ошибка отправки');
insert into NDS2_MRR_USER.DOC_STATUS values(4, 1, 'Отправлено налогоплательщику');
insert into NDS2_MRR_USER.DOC_STATUS values(5, 1, 'Получено налогоплательщиком');
insert into NDS2_MRR_USER.DOC_STATUS values(9, 1, 'Ошибка передачи в СЭОД');
insert into NDS2_MRR_USER.DOC_STATUS values(10, 1, 'Закрыто');

insert into NDS2_MRR_USER.DOC_STATUS values(-1, 2, 'Ручная проверка');
insert into NDS2_MRR_USER.DOC_STATUS values(1, 2, 'Создано');
insert into NDS2_MRR_USER.DOC_STATUS values(2, 2, 'Передано в СЭОД');
insert into NDS2_MRR_USER.DOC_STATUS values(3, 2, 'Ошибка отправки');
insert into NDS2_MRR_USER.DOC_STATUS values(4, 2, 'Отправлено налогоплательщику');
insert into NDS2_MRR_USER.DOC_STATUS values(5, 2, 'Получено налогоплательщиком');
insert into NDS2_MRR_USER.DOC_STATUS values(9, 2, 'Ошибка передачи в СЭОД');
insert into NDS2_MRR_USER.DOC_STATUS values(10, 2, 'Закрыто');

insert into NDS2_MRR_USER.DOC_STATUS values(-1, 3, 'Ручная проверка');
insert into NDS2_MRR_USER.DOC_STATUS values(1, 3, 'Создано');
insert into NDS2_MRR_USER.DOC_STATUS values(2, 3, 'Передано в СЭОД');
insert into NDS2_MRR_USER.DOC_STATUS values(3, 3, 'Ошибка отправки');
insert into NDS2_MRR_USER.DOC_STATUS values(4, 3, 'Отправлено налогоплательщику');
insert into NDS2_MRR_USER.DOC_STATUS values(5, 3, 'Получено налогоплательщиком');
insert into NDS2_MRR_USER.DOC_STATUS values(6, 3, 'Отправлено в НО-исполнитель');
insert into NDS2_MRR_USER.DOC_STATUS values(7, 3, 'Получено в НО-исполнителем');
insert into NDS2_MRR_USER.DOC_STATUS values(9, 3, 'Ошибка передачи в СЭОД');
insert into NDS2_MRR_USER.DOC_STATUS values(10, 3, 'Закрыто');

insert into NDS2_MRR_USER.DOC_STATUS values(-1, 4, 'Ручная проверка');
insert into NDS2_MRR_USER.DOC_STATUS values(1, 4, 'Создано');
insert into NDS2_MRR_USER.DOC_STATUS values(2, 4, 'Передано в СЭОД');
insert into NDS2_MRR_USER.DOC_STATUS values(3, 4, 'Ошибка отправки');
insert into NDS2_MRR_USER.DOC_STATUS values(4, 4, 'Отправлено налогоплательщику');
insert into NDS2_MRR_USER.DOC_STATUS values(5, 4, 'Получено налогоплательщиком');
insert into NDS2_MRR_USER.DOC_STATUS values(9, 4, 'Ошибка передачи в СЭОД');
insert into NDS2_MRR_USER.DOC_STATUS values(10, 4, 'Закрыто');

insert into NDS2_MRR_USER.DOC_STATUS values(-1, 5, 'Ручная проверка');
insert into NDS2_MRR_USER.DOC_STATUS values(1, 5, 'Создано');
insert into NDS2_MRR_USER.DOC_STATUS values(2, 5, 'Передано в СЭОД');
insert into NDS2_MRR_USER.DOC_STATUS values(3, 5, 'Ошибка отправки');
insert into NDS2_MRR_USER.DOC_STATUS values(4, 5, 'Отправлено налогоплательщику');
insert into NDS2_MRR_USER.DOC_STATUS values(5, 5, 'Получено налогоплательщиком');
insert into NDS2_MRR_USER.DOC_STATUS values(9, 5, 'Ошибка передачи в СЭОД');
insert into NDS2_MRR_USER.DOC_STATUS values(10, 5, 'Закрыто');

commit;
