﻿create table NDS2_MRR_USER.CFG_INSPECTION_GROUP_N
(
  id        NUMBER not null,
  name      NVARCHAR2(512) not null,
  sono_code VARCHAR2(4 CHAR) not null,
  all_inspections_available CHAR(1) default 'N' not null
);
