﻿declare
v_exists number;
begin
 select count(1) into v_exists from all_indexes where index_name = 'IX_DA_TAX_PERIOD_SORT_SRCH' and owner = 'NDS2_MRR_USER';
 if v_exists = 0 then
   execute immediate 'create index NDS2_MRR_USER.IX_DA_TAX_PERIOD_SORT_SRCH on NDS2_MRR_USER.DECLARATION_ACTIVE_SEARCH (TAX_PERIOD_SORT_ORDER) nologging tablespace NDS2_IDX';
 end if;
end;
/
