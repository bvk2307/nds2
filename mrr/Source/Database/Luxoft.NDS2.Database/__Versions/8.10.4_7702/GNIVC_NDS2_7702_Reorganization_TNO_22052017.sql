﻿-- актуализируем таблицу с историей реорганизаций ТНО
insert /*+APPEND*/ into nds2_mrr_user.ifns_reorg_history (	
  codeno_old
 ,codeno_new
 ,shift_value
) select codeno_old
        ,codeno_new
        ,shift_value
    from conv.reorg_shift@nds2_ais3_i$nds2 
    where col_name = 'D270'
      and d85 > to_date('22.05.2017', 'dd.mm.yyyy')
  union all 
  select '2339' as CODENO_OLD 
        ,'2377' as CODENO_NEW 
        ,0 as SHIFT_VALUE  
    from dual; 

commit;

-- актуализируем таблицы с историей изменений в схеме nds2_mc
insert /*+APPEND*/ into nds2_mrr_user.askzipfile_reorg 
  select rg.codeno_new as "КодНО_Новый", t.* from nds2_mc."ASKZIPФайл" t join nds2_mrr_user.ifns_reorg_history rg on rg.codeno_old = t."КодНО" and rg.executed_at is null;

insert /*+APPEND*/ into nds2_mrr_user.askdekl_reorg 
  select rg.codeno_new as "КодНО_Новый", t.* from nds2_mc."ASKДекл" t join nds2_mrr_user.ifns_reorg_history rg on rg.codeno_old = t."КодНО" and rg.executed_at is null;

insert /*+APPEND*/ into nds2_mrr_user.askpoyasnenie_reorg
  select rg.codeno_new as "КодНО_Новый", t.* from nds2_mc."ASKПояснение" t join nds2_mrr_user.ifns_reorg_history rg on rg.codeno_old = t."КодНО" and rg.executed_at is null;
 
insert /*+APPEND*/ into nds2_mrr_user.askjrnl_reorg
  select rg.codeno_new as "КодНО_Новый", t.* from nds2_mc."ASKЖурналУч" t join nds2_mrr_user.ifns_reorg_history rg on rg.codeno_old = t."КодНО" and rg.executed_at is null;

insert /*+APPEND*/ into nds2_mrr_user.askjrnlchast_reorg
  select rg.codeno_new as "КодНО_Новый", t.* from nds2_mc."ASKЖурналЧасть" t join nds2_mrr_user.ifns_reorg_history rg on rg.codeno_old = t."КодНО" and rg.executed_at is null;
 
commit;

-- производим собственно миграцию данных
begin
  
  for reorg in (
    select codeno_old
          ,codeno_new
          ,shift_value
      from conv.reorg_shift@nds2_ais3_i$nds2 
      where col_name = 'D270'
        and d85 > to_date('22.05.2017', 'dd.mm.yyyy')
    union all 
    select '2339' as CODENO_OLD 
          ,'2377' as CODENO_NEW 
          ,0 as SHIFT_VALUE  
      from dual 
  ) loop
    begin 
      nds2_mrr_user.pac$support.P$REMOVE_SONO(reorg.codeno_old, reorg.codeno_new, reorg.shift_value);      
      update nds2_mc."ASKZIPФайл" set "КодНО" = reorg.codeno_new where "КодНО" = reorg.codeno_old;
      update nds2_mc."ASKДекл" set "КодНО" = reorg.codeno_new where "КодНО" = reorg.codeno_old;
      update nds2_mc."ASKПояснение" set "КодНО" = reorg.codeno_new where "КодНО" = reorg.codeno_old;
      update nds2_mc."ASKЖурналУч" set "КодНО" = reorg.codeno_new where "КодНО" = reorg.codeno_old;
      update nds2_mc."ASKЖурналЧасть" set "КодНО" = reorg.codeno_new where "КодНО" = reorg.codeno_old;
      update nds2_mrr_user.ifns_reorg_history set executed_at = sysdate where codeno_old = reorg.codeno_old;  
         
      commit;
    end;
  end loop; 
  
end;
/
