drop  materialized view NDS2_MRR_USER.MV$DECLARATIONINSPECT;
create materialized view NDS2_MRR_USER.MV$DECLARATIONINSPECT
build DEFERRED 
refresh force on demand
as
select
     to_number(seod.tax_period||seod.fiscal_year||seod.inn||seod.correction_number) as nds2_decl_corr_id
	  ,seod.IS_ACTIVE
    ,seod.nds2_id as ID
    ,sd.zip as declaration_version_id
    ,seod.TYPE as DECL_TYPE_CODE
    ,DECODE(seod.TYPE, 0, '����������', 1, '������', '') as DECL_TYPE
    ,case when sovd.DECLARATION_VERSION_ID is not null then 2 else (case when  sd.ID is not null then 1 else 0 end) end as ProcessingStage
    ,MAX(case when sovd.DECLARATION_VERSION_ID is not null then 2 else (case when  sd.ID is not null then 1 else 0 end) end)
       OVER(PARTITION BY seod.inn, seod.tax_period, seod.fiscal_year) as max_processing_stage
    ,case when seod.TYPE=0 and (knp.knp_id IS NULL or knp.completion_date is null) then '�������'
          when seod.TYPE=0 and (knp.knp_id IS NOT NULL and knp.completion_date is not null) then '�������'
          else NULL
       end as STATUS_KNP
    ,case
       when sovd.declaration_version_id is null then null
       else case when chapter8.BUYBOOK_DK_OTHR_CNT + chapter8.BUYBOOK_DK_OTHR_CNT > 0 then '����'
       else '���' end end as STATUS
    ,nvl(esur.sign_code, 4) as SUR_CODE
    ,seod.inn as INN
    ,seod.kpp as KPP
    ,vtp.NAME
    ,case
       when seod.TYPE=0 then seod.decl_reg_num
       else null end as SEOD_DECL_ID
    ,seod.TAX_PERIOD
    ,seod.FISCAL_YEAR
    ,dtp.description||' '||seod.FISCAL_YEAR as FULL_TAX_PERIOD
    ,case when seod.TYPE=0 then nvl(sd.DATADOK, seod.EOD_DATE) else null end as DECL_DATE
    ,case
       when seod.TYPE=0 then seod.CORRECTION_NUMBER
       else null end as CORRECTION_NUMBER
    ,(DECODE(seod.TYPE, 0, 
      (case when nvl(sd.SUMPU173_5, 0) = 0 and nvl(sd.SUMPU173_1, 0) = 0 and nvl(COUNT_INVOICE_R08_09_12, 0) = 0 then 0
       when nvl(sd.SUMPU173_5, 0) = 0 and nvl(sd.SUMPU173_1, 0) = 0 and nvl(COUNT_INVOICE_R08_09_12, 0) > 0 then 0
       when nvl(sd.SUMPU173_5, 0) > 0 and nvl(sd.SUMPU173_1, 0) = 0 then nvl(sd.SUMPU173_5, 0)
       when nvl(sd.SUMPU173_5, 0) = 0 and nvl(sd.SUMPU173_1, 0) > 0 then nvl(sd.SUMPU173_1, 0)
       when nvl(sd.SUMPU173_5, 0) = 0 and nvl(sd.SUMPU173_1, 0) < 0 then nvl(sd.SUMPU173_1, 0)
       when nvl(sd.SUMPU173_5, 0) > 0 and nvl(sd.SUMPU173_1, 0) > 0 then (nvl(sd.SUMPU173_5, 0) + nvl(sd.SUMPU173_1, 0))
       when nvl(sd.SUMPU173_5, 0) > 0 and nvl(sd.SUMPU173_1, 0) < 0 and (nvl(sd.SUMPU173_5, 0) + nvl(sd.SUMPU173_1, 0)) >= 0 then  (nvl(sd.SUMPU173_5, 0) + nvl(sd.SUMPU173_1, 0))
       when nvl(sd.SUMPU173_5, 0) > 0 and nvl(sd.SUMPU173_1, 0) < 0 and (nvl(sd.SUMPU173_5, 0) + nvl(sd.SUMPU173_1, 0)) < 0 then (nvl(sd.SUMPU173_5, 0) + nvl(sd.SUMPU173_1, 0))
       when nvl(sd.SUMPU173_5, 0) < 0 and nvl(sd.SUMPU173_1, 0) < 0 then nvl(sd.SUMPU173_1, 0)
       when nvl(sd.SUMPU173_5, 0) < 0 and nvl(sd.SUMPU173_1, 0) > 0 then nvl(sd.SUMPU173_1, 0)
       when nvl(sd.SUMPU173_5, 0) < 0 and nvl(sd.SUMPU173_1, 0) = 0 and nvl(COUNT_INVOICE_R08_09_12, 0) = 0 then 0
       when nvl(sd.SUMPU173_5, 0) < 0 and nvl(sd.SUMPU173_1, 0) = 0 and nvl(COUNT_INVOICE_R08_09_12, 0) > 0 then 0
   else null end), null))   as COMPENSATION_AMNT_SIGN
    ,dsd.NDS_AMOUNT
    ,chapter8.BUYBOOK_CONTRAGENT_CNT
    ,chapter8.BUYBOOK_DK_GAP_CAGNT_CNT
    ,chapter8.BUYBOOK_DK_GAP_CNT
    ,chapter8.BUYBOOK_DK_GAP_AMNT
    ,chapter8.BUYBOOK_DK_OTHR_CAGNT_CNT
    ,chapter8.BUYBOOK_DK_OTHR_CNT
    ,chapter8.BUYBOOK_DK_OTHR_AMNT
    ,chapter9.SELLBOOK_CONTRAGENT_CNT
    ,chapter9.SELLBOOK_DK_GAP_CAGNT_CNT
    ,chapter9.SELLBOOK_DK_GAP_CNT
    ,chapter9.SELLBOOK_DK_GAP_AMNT
    ,chapter9.SELLBOOK_DK_OTHR_CAGNT_CNT
    ,chapter9.SELLBOOK_DK_OTHR_CNT
    ,chapter9.SELLBOOK_DK_OTHR_AMNT
    ,chapter10.ININV_CONTRAGENT_CNT
    ,chapter10.ININV_DK_GAP_CAGNT_CNT
    ,chapter10.ININV_DK_GAP_CNT
    ,chapter10.ININV_DK_GAP_AMNT
    ,chapter10.ININV_DK_OTHR_CAGNT_CNT
    ,chapter10.ININV_DK_OTHR_CNT
    ,chapter10.ININV_DK_OTHR_AMNT
    ,chapter11.OUTINV_CONTRAGENT_CNT
    ,chapter11.OUTINV_DK_GAP_CAGNT_CNT
    ,chapter11.OUTINV_DK_GAP_CNT
    ,chapter11.OUTINV_DK_GAP_AMNT
    ,chapter11.OUTINV_DK_OTHR_CAGNT_CNT
    ,chapter11.OUTINV_DK_OTHR_CNT
    ,chapter11.OUTINV_DK_OTHR_AMNT
    ,chapter12.INVDATA_CONTRAGENT_CNT
    ,chapter12.INVDATA_DK_GAP_CAGNT_CNT
    ,chapter12.INVDATA_DK_GAP_CNT
    ,chapter12.INVDATA_DK_GAP_AMNT
    ,chapter12.INVDATA_DK_OTHR_CAGNT_CNT
    ,chapter12.INVDATA_DK_OTHR_CNT
    ,chapter12.INVDATA_DK_OTHR_AMNT
    ,seod.SONO_CODE as SOUN_CODE
    ,substr(sd.KODNO,1,2) as REGION_CODE
from
(
   select 
    decode((row_number() over (partition by seod.TAX_PERIOD, seod.FISCAL_YEAR, seod.INN order by seod.CORRECTION_NUMBER desc)),1,1,0) as IS_ACTIVE,
    seod.*
   from seod_declaration seod
) seod
inner join DICT_TAX_PERIOD dtp on dtp.code = seod.TAX_PERIOD
left join V$ASK_DECLANDJRNL sd on SD.PERIOD = seod.TAX_PERIOD and SD.OTCHETGOD = seod.FISCAL_YEAR and SD.INNNP = seod.INN and SD.NOMKORR = seod.CORRECTION_NUMBER
left join nds2_mrr_user.seod_knp knp on knp.declaration_reg_num = seod.decl_reg_num
left join sov_declaration_info sovd on SOVD.DECLARATION_VERSION_ID = SD.ZIP
left join v$taxpayer vtp on vtp.inn = seod.inn
left join EXT_SUR esur on esur.inn = seod.INN and seod.TAX_PERIOD = esur.fiscal_period and seod.FISCAL_YEAR = esur.fiscal_year
left join
(
  select
    ZIP,
    sum(case TipFajla when '0' then SumNDSPok when '2' then SumNDSPokDL else 0 end) as NDS_AMOUNT,
  nvl(SUM(case when TipFajla in ('0', '2', '1', '3', '6') then nvl(KolZapisey, 0) end), 0) as COUNT_INVOICE_R08_09_12
  from nds2_mrr_user.v$asksvodzap
  group by ZIP
) dsd on dsd.zip = sd.zip
left join -- ����� �������, ������ 8 
(
    select
        t.DECLARATION_VERSION_ID,
        COUNT(*) AS BUYBOOK_CONTRAGENT_CNT,
        SUM(case when t.GAPS = 0 then 0 else 1 end) AS BUYBOOK_DK_GAP_CAGNT_CNT,
        SUM(t.GAPS) as BUYBOOK_DK_GAP_CNT,
        SUM(t.GAPS_AMT) as BUYBOOK_DK_GAP_AMNT,
        SUM(case when t.OTHRS = 0 then 0 else 1 end) AS BUYBOOK_DK_OTHR_CAGNT_CNT,
        SUM(t.OTHRS) as BUYBOOK_DK_OTHR_CNT,
        SUM(t.OTHRS_AMT) as BUYBOOK_DK_OTHR_AMNT
    from (
         select
             decl.DECLARATION_VERSION_ID,
             inv.seller_inn,
             inv.seller_kpp,
             SUM(case ds.type when 1 then 1 else 0 end) as GAPS,
             SUM(case when ds.type=1 then 0 when ds.type IS NULL then 0 else 1 end) as OTHRS,
             SUM(case ds.type when 1 then ds.amnt else 0 end) as GAPS_AMT,
             SUM(case when ds.type=1 then 0 when ds.type IS NULL then 0 else ds.amnt end) as OTHRS_AMT
         from nds2_mrr_user.sov_declaration_info decl
         join nds2_mrr_user.stage_invoice inv on inv.DECLARATION_VERSION_ID = decl.DECLARATION_VERSION_ID
                                              and inv.chapter = 8
         left join nds2_mrr_user.sov_discrepancy ds on ds.invoice_rk = inv.row_key
        group by decl.DECLARATION_VERSION_ID, inv.seller_inn, inv.seller_kpp
         ) t
    group by t.DECLARATION_VERSION_ID
) chapter8 on chapter8.declaration_version_id = sd.zip
left join -- ����� �������, ������ 9 
(
    select
        t.DECLARATION_VERSION_ID,
        COUNT(*) AS SELLBOOK_CONTRAGENT_CNT,
        SUM(case when t.GAPS = 0 then 0 else 1 end) AS SELLBOOK_DK_GAP_CAGNT_CNT,
        SUM(t.GAPS) as SELLBOOK_DK_GAP_CNT,
        SUM(t.GAPS_AMT) as SELLBOOK_DK_GAP_AMNT,
        SUM(case when t.OTHRS = 0 then 0 else 1 end) AS SELLBOOK_DK_OTHR_CAGNT_CNT,
        SUM(t.OTHRS) as SELLBOOK_DK_OTHR_CNT,
        SUM(t.OTHRS_AMT) as SELLBOOK_DK_OTHR_AMNT
    from (
         select
             decl.DECLARATION_VERSION_ID,
             inv.buyer_inn,
             inv.buyer_kpp,
             SUM(case ds.type when 1 then 1 else 0 end) as GAPS,
             SUM(case when ds.type=1 then 0 when ds.type IS NULL then 0 else 1 end) as OTHRS,
             SUM(case ds.type when 1 then ds.amnt else 0 end) as GAPS_AMT,
             SUM(case when ds.type=1 then 0 when ds.type IS NULL then 0 else ds.amnt end) as OTHRS_AMT
         from nds2_mrr_user.sov_declaration_info decl
         join nds2_mrr_user.stage_invoice inv on inv.DECLARATION_VERSION_ID = decl.DECLARATION_VERSION_ID
                                              and inv.chapter = 9
         left join nds2_mrr_user.sov_discrepancy ds on ds.invoice_rk = inv.row_key
        group by decl.DECLARATION_VERSION_ID, inv.buyer_inn, inv.buyer_kpp
         ) t
    group by t.DECLARATION_VERSION_ID
) chapter9 on chapter9.declaration_version_id = sd.zip
left join -- ������ ������������ ��, ������ 10 
(
    select
        t.DECLARATION_VERSION_ID,
        COUNT(*) AS ININV_CONTRAGENT_CNT,
        SUM(case when t.GAPS = 0 then 0 else 1 end) AS ININV_DK_GAP_CAGNT_CNT,
        SUM(t.GAPS) as ININV_DK_GAP_CNT,
        SUM(t.GAPS_AMT) as ININV_DK_GAP_AMNT,
        SUM(case when t.OTHRS = 0 then 0 else 1 end) AS ININV_DK_OTHR_CAGNT_CNT,
        SUM(t.OTHRS) as ININV_DK_OTHR_CNT,
        SUM(t.OTHRS_AMT) as ININV_DK_OTHR_AMNT
    from (
         select
             decl.DECLARATION_VERSION_ID,
             inv.seller_inn,
             inv.seller_kpp,
             SUM(case ds.type when 1 then 1 else 0 end) as GAPS,
             SUM(case when ds.type=1 then 0 when ds.type IS NULL then 0 else 1 end) as OTHRS,
             SUM(case ds.type when 1 then ds.amnt else 0 end) as GAPS_AMT,
             SUM(case when ds.type=1 then 0 when ds.type IS NULL then 0 else ds.amnt end) as OTHRS_AMT
         from nds2_mrr_user.sov_declaration_info decl
         join nds2_mrr_user.stage_invoice inv on inv.DECLARATION_VERSION_ID = decl.DECLARATION_VERSION_ID
                                              and inv.chapter = 10
         left join nds2_mrr_user.sov_discrepancy ds on ds.invoice_rk = inv.row_key
        group by decl.DECLARATION_VERSION_ID, inv.seller_inn, inv.seller_kpp
         ) t
    group by t.DECLARATION_VERSION_ID
) chapter10 on chapter10.declaration_version_id = sd.zip
left join -- ������ ���������� ��, ������ 11 
(
    select
        t.DECLARATION_VERSION_ID,
        COUNT(*) AS OUTINV_CONTRAGENT_CNT,
        SUM(case when t.GAPS = 0 then 0 else 1 end) AS OUTINV_DK_GAP_CAGNT_CNT,
        SUM(t.GAPS) as OUTINV_DK_GAP_CNT,
        SUM(t.GAPS_AMT) as OUTINV_DK_GAP_AMNT,
        SUM(case when t.OTHRS = 0 then 0 else 1 end) AS OUTINV_DK_OTHR_CAGNT_CNT,
        SUM(t.OTHRS) as OUTINV_DK_OTHR_CNT,
        SUM(t.OTHRS_AMT) as OUTINV_DK_OTHR_AMNT
    from (
         select
             decl.DECLARATION_VERSION_ID,
             inv.buyer_inn,
             inv.buyer_kpp,
             SUM(case ds.type when 1 then 1 else 0 end) as GAPS,
             SUM(case when ds.type=1 then 0 when ds.type IS NULL then 0 else 1 end) as OTHRS,
             SUM(case ds.type when 1 then ds.amnt else 0 end) as GAPS_AMT,
             SUM(case when ds.type=1 then 0 when ds.type IS NULL then 0 else ds.amnt end) as OTHRS_AMT
         from nds2_mrr_user.sov_declaration_info decl
         join nds2_mrr_user.stage_invoice inv on inv.DECLARATION_VERSION_ID = decl.DECLARATION_VERSION_ID
                                              and inv.chapter = 11
         left join nds2_mrr_user.sov_discrepancy ds on ds.invoice_rk = inv.row_key
        group by decl.DECLARATION_VERSION_ID, inv.buyer_inn, inv.buyer_kpp
         ) t
    group by t.DECLARATION_VERSION_ID
) chapter11 on chapter11.declaration_version_id = sd.zip
left join -- �� (�.5 ��.173 �� ��), ������ 12 
(
    select
        t.DECLARATION_VERSION_ID,
        COUNT(*) AS INVDATA_CONTRAGENT_CNT,
        SUM(case when t.GAPS = 0 then 0 else 1 end) AS INVDATA_DK_GAP_CAGNT_CNT,
        SUM(t.GAPS) as INVDATA_DK_GAP_CNT,
        SUM(t.GAPS_AMT) as INVDATA_DK_GAP_AMNT,
        SUM(case when t.OTHRS = 0 then 0 else 1 end) AS INVDATA_DK_OTHR_CAGNT_CNT,
        SUM(t.OTHRS) as INVDATA_DK_OTHR_CNT,
        SUM(t.OTHRS_AMT) as INVDATA_DK_OTHR_AMNT
    from (
         select
             decl.DECLARATION_VERSION_ID,
             inv.buyer_inn,
             inv.buyer_kpp,
             SUM(case ds.type when 1 then 1 else 0 end) as GAPS,
             SUM(case when ds.type=1 then 0 when ds.type IS NULL then 0 else 1 end) as OTHRS,
             SUM(case ds.type when 1 then ds.amnt else 0 end) as GAPS_AMT,
             SUM(case when ds.type=1 then 0 when ds.type IS NULL then 0 else ds.amnt end) as OTHRS_AMT
         from nds2_mrr_user.sov_declaration_info decl
         join nds2_mrr_user.stage_invoice inv on inv.DECLARATION_VERSION_ID = decl.DECLARATION_VERSION_ID
                                              and inv.chapter = 11
         left join nds2_mrr_user.sov_discrepancy ds on ds.invoice_rk = inv.row_key
        group by decl.DECLARATION_VERSION_ID, inv.buyer_inn, inv.buyer_kpp
         ) t
    group by t.DECLARATION_VERSION_ID
) chapter12 on chapter12.declaration_version_id = sd.zip
where seod.IS_ACTIVE = 1;
