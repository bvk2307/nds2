create table NDS2_MRR_USER.VERSION_HISTORY
(
  version_number           varchar2(15) not null,
  release_date             date not null,
  catalog_reindex_required number(1) not null
);
