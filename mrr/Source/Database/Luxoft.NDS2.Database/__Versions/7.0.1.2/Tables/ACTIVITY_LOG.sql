alter table NDS2_MRR_USER.ACTIVITY_LOG add SID VARCHAR2(128);

alter table NDS2_MRR_USER.ACTIVITY_LOG_REPORT add decl_open_uniq number;
alter table NDS2_MRR_USER.ACTIVITY_LOG_REPORT add decl_ext_open_uniq number;
alter table NDS2_MRR_USER.ACTIVITY_LOG_REPORT add man_sel_call_uniq number;
alter table NDS2_MRR_USER.ACTIVITY_LOG_REPORT add reports_uniq number;
