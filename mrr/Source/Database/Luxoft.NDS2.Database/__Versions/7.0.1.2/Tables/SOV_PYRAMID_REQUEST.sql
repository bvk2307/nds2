alter table NDS2_MRR_USER.SOV_PYRAMID_REQUEST drop column min_sum;
alter table NDS2_MRR_USER.SOV_PYRAMID_REQUEST drop column min_nds_percent;
alter table NDS2_MRR_USER.SOV_PYRAMID_REQUEST add (DEPTH NUMBER default 0 NOT NULL);
