﻿    CREATE OR REPLACE PROCEDURE "NDS2_MRR_USER"."APPEND_DISCREPANCIES" 
     IS
    BEGIN
      DECLARE IDD NUMERIC;
        cnt NUMBER;
      l_status NUMBER;
      l_stage NUMBER;
      BEGIN
  
    FOR r in (select * from SOV_DISCREPANCY_BUF)
    LOOP
        select count(1) into cnt from SOV_DISCREPANCY where SOV_ID=r.SOV_ID;
        IF cnt > 0 THEN
  
        update SOV_DISCREPANCY SET status = r.STATUS where SOV_ID=r.SOV_ID and status < 2 returning ID into IDD;
  
        ELSE
  
        insert into SOV_DISCREPANCY
        (ID,
         SOV_ID,
         CREATE_DATE,
         TYPE,
         compare_kind,
         rule_group,
         rule_num,
         deal_amnt,
         amnt,
         INVOICE_CHAPTER,
         invoice_rk,
         decl_id,
         invoice_contractor_rk,
         INVOICE_CONTRACTOR_CHAPTER,
         decl_contractor_id,
         status,
         stage, amount_pvp, SIDE_PRIMARY_PROCESSING)
        values
          (SEQ_DISCREPANCY_ID.NEXTVAL,
           r.SOV_ID,
           r.CREATE_DATE,
           r.TYPE,
           r.COMPARE_KIND,
           r.RULE_GROUP,
           r.RULE_NUM,
           r.DEAL_AMNT,
           r.AMNT,
           r.INVOICE_CHAPTER,
           r.INVOICE_RK,
           r.DECL_ID,
           r.INVOICE_CONTRACTOR_RK,
           r.INVOICE_CONTRACTOR_CHAPTER,
           (select "Период"||"ОтчетГод"||"ИНННП" from NDS2_MC."ASKДекл" where zip = r.DECL_CONTRACTOR_ID),
           r.STATUS, 1, r.AMOUNT_PVP, r.SIDE_PRIMARY_PROCESSING)  RETURNING ID into IDD;
  
  
        END IF;
  
    --insert links
        SELECT status, stage INTO l_status, l_stage FROM SOV_DISCREPANCY where SOV_ID=r.SOV_ID;
        IF (l_status = 1 AND l_stage < 2) 
        THEN
          SELECT COUNT(1) INTO cnt FROM SELECTION_DISCREPANCY WHERE REQUEST_ID = r.REQUEST_ID AND DISCREPANCY_ID = IDD;
          IF(cnt = 0)
          THEN
          INSERT INTO SELECTION_DISCREPANCY  (REQUEST_ID, DISCREPANCY_ID, IS_IN_PROCESS) values (r.REQUEST_ID, IDD, 1);
          END IF;
  
  
          SELECT COUNT(1) INTO cnt FROM SELECTION_DECLARATION WHERE REQUEST_ID = r.REQUEST_ID AND DECLARATION_ID = r.DECL_ID;
          IF(cnt = 0 AND l_status = 1 AND l_stage < 2)
          THEN
            INSERT INTO SELECTION_DECLARATION  (REQUEST_ID, DECLARATION_ID, IS_IN_PROCESS) values (r.REQUEST_ID, r.DECL_ID, 1);
          END IF;
        END IF;
      END LOOP;
    END;
  END APPEND_DISCREPANCIES;
/