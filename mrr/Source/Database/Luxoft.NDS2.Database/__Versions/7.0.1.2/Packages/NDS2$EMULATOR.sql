create or replace package NDS2_MRR_USER."NDS2$EMULATOR" is

procedure CREATE_INVOICE_REQUEST (
  pId out INVOICE_REQUEST.ID%type
);

procedure CREATE_MISMATCH_REQUEST (
  pId out SOV_SELECTION_REQUEST.REQUEST_ID%type,
  pBody in SOV_SELECTION_REQUEST.REQUEST_BODY%type,
  pStatus in SOV_SELECTION_REQUEST.STATUS_ID%type
  );

procedure UPDATE_MISMATCH_REQUEST (
  pId in SOV_SELECTION_REQUEST.REQUEST_ID%type,
  pStatus in SOV_SELECTION_REQUEST.STATUS_ID%type
  );

procedure CREATE_BOOK_DATA_REQUEST (
  pId out BOOK_DATA_REQUEST.ID%type,
  pInn in BOOK_DATA_REQUEST.INN%type,
  pCorrectionNumber in BOOK_DATA_REQUEST.CORRECTIONNUMBER%type,
  pPartitionNumber in BOOK_DATA_REQUEST.PARTITIONNUMBER%type,
  pPeriod in BOOK_DATA_REQUEST.PERIOD%type,
  pYear in BOOK_DATA_REQUEST.YEAR%type,
  pPriority in BOOK_DATA_REQUEST.PRIORITY%type,
  pResultRecordQuantity out number,
  pDeclarationId out SOV_DECLARATION_INFO.DECLARATION_VERSION_ID%type
  );

procedure UPDATE_BOOK_DATA_REQUEST(
  pId in BOOK_DATA_REQUEST.ID%type,
  pStatus in BOOK_DATA_REQUEST.STATUS%type
  );

procedure CREATE_CAGNT_DATA_REQUEST (
  pId out CONTRAGENT_DATA_REQUEST.ID%type,
  pInn in CONTRAGENT_DATA_REQUEST.INN%type,
  pCorrectionNumber in CONTRAGENT_DATA_REQUEST.CORRECTION_NUMBER%type,
  pPeriod in CONTRAGENT_DATA_REQUEST.PERIOD%type,
  pYear in CONTRAGENT_DATA_REQUEST.TAX_YEAR%type
  );

procedure GENERATE_CAGNT_DATA (
  pId in CONTRAGENT_DATA_REQUEST.ID%type,
  pInn in V$DECLARATIONINSPECT.INN%type,
  pCorrectionNumber in V$DECLARATIONINSPECT.CORRECTION_NUMBER%type,
  pPeriod in V$DECLARATIONINSPECT.TAX_PERIOD%type,
  pYear in CONTRAGENT_DATA_REQUEST.TAX_YEAR%type
  );

procedure CREATE_CAGNT_PARAMS_REQUEST (
  pId out CONTRAGENT_PARAMS_REQUEST.ID%type,
  pInn in CONTRAGENT_PARAMS_REQUEST.INN%type,
  pContractorInn in CONTRAGENT_PARAMS_REQUEST.CONTRACTOR_INN%type,
  pCorrectionNumber in CONTRAGENT_PARAMS_REQUEST.CORRECTION_NUMBER%type,
  pPeriod in CONTRAGENT_PARAMS_REQUEST.PERIOD%type,
  pYear in CONTRAGENT_PARAMS_REQUEST.TAX_YEAR%type
  );

procedure GENERATE_CAGNT_PARAMS (
  pId in CONTRAGENT_PARAMS_REQUEST.ID%type,
  pInn in V$DECLARATIONINSPECT.INN%type,
  pContractorInn in CONTRAGENT_PARAMS_REQUEST.CONTRACTOR_INN%type,  
  pCorrectionNumber in V$DECLARATIONINSPECT.CORRECTION_NUMBER%type,
  pPeriod in V$DECLARATIONINSPECT.TAX_PERIOD%type,
  pYear in CONTRAGENT_PARAMS_REQUEST.TAX_YEAR%type
  );

procedure COPY_BOOK_DATA (
  pRequestId in BOOK_DATA_REQUEST.ID%type,
  pDeclarationId in SOV_DECLARATION_INFO.ID%type,
  pIndexFrom in NUMBER,
  pIndexTo in NUMBER
  );

procedure CREATE_DISCREPANCY_REQUEST (
  pId OUT DISCREPANCY_REQUEST.ID%type,
  pInn IN DISCREPANCY_REQUEST.INN%type,
  pPeriod IN DISCREPANCY_REQUEST.PERIOD%type,
  pYear IN DISCREPANCY_REQUEST.YEAR%type,
  pCorrectionNumber IN DISCREPANCY_REQUEST.CORRECTION_NUMBER%type,
  pType IN DISCREPANCY_REQUEST.TYPE%type,
  pKind IN DISCREPANCY_REQUEST.KIND%type
  );

procedure COPY_DISCREPANCIES (
  pRequestId  IN DISCREPANCY_REQUEST.ID%type
  );

procedure PROCESS_INVOICE_REQUEST (
  pRequestId IN INVOICE_REQUEST.ID%type
  );

procedure CREATE_PYRAMID_REQUEST(
  pId OUT SOV_PYRAMID_REQUEST.REQUEST_ID%type,
  pInn IN SOV_PYRAMID_REQUEST.CONTRACTOR_INN%type,
  pFiscalYear IN SOV_PYRAMID_REQUEST.FISCAL_YEAR%type,
  pPeriod IN SOV_PYRAMID_REQUEST.PERIOD%type
   );

procedure GENERATE_CONTRACTOR(
  pId IN NUMBER,
  pName IN VARCHAR2,
  pInn VARCHAR2,
  pKpp IN VARCHAR2
  );

procedure PROCESS_PYRAMID_REQUEST(
  pId IN SOV_PYRAMID_REQUEST.REQUEST_ID%type
  );

procedure CREATE_LK_REPORT_REQUEST (
  pId out SOV_LK_REPORT_REQUEST.REQUEST_ID%type,
  pBody in SOV_LK_REPORT_REQUEST.REQUEST_BODY%type,
  pStatus in SOV_LK_REPORT_REQUEST.STATUS_ID%type
  );

procedure UPDATE_LK_REPORT_REQUEST (
  pId in SOV_LK_REPORT_REQUEST.REQUEST_ID%type,
  pStatus in SOV_LK_REPORT_REQUEST.STATUS_ID%type
  );

procedure FILL_REPORT_LK (
  pRequestId in SOV_LK_REPORT_REQUEST.REQUEST_ID%type
  );
  
procedure CREATE_NAVIGATOR_REQUEST (
  pRequestId out number,
  pMonthFrom in number,
  pYearFrom in number,
  pMonthTo in number,
  pYearTo in number,
  pDepth in number,
  pMinMappedAmount in number,
  pMaxContractorsQuantity in number,
  pMinBuyerAmount in number,
  pMinSellerAmount in number);
  
procedure INSERT_NAVIGATOR_TAXPAYER (
  pRequestId in number,
  pInn in nvarchar2);
  
procedure PROCESS_NAVIGATOR_REQUEST (
  pRequestId in number);

end "NDS2$EMULATOR";
/
create or replace package body NDS2_MRR_USER."NDS2$EMULATOR" is

procedure CREATE_INVOICE_REQUEST (
  pId out INVOICE_REQUEST.ID%type
)
as
begin

  select SEQ_SOV_REQUEST.NEXTVAL into pId from DUAL;
  insert into INVOICE_REQUEST (Id, STATUS, requestdate) values (pId, 0, sysdate);

END;

procedure CREATE_MISMATCH_REQUEST (
  pId out SOV_SELECTION_REQUEST.REQUEST_ID%type,
  pBody in SOV_SELECTION_REQUEST.REQUEST_BODY%type,
  pStatus in SOV_SELECTION_REQUEST.STATUS_ID%type
  )
as
begin

  select FIR.MRR_REQ_SEQ.NEXTVAL into pId from DUAL;
  insert into SOV_SELECTION_REQUEST (REQUEST_ID, REQUEST_BODY, STATUS_ID) values (pId, pBody, pStatus);
end;


procedure UPDATE_MISMATCH_REQUEST (
  pId in SOV_SELECTION_REQUEST.REQUEST_ID%type,
  pStatus in SOV_SELECTION_REQUEST.STATUS_ID%type
  )
as
begin

  update SOV_SELECTION_REQUEST set STATUS_ID = pStatus where REQUEST_ID = pId;

end;

procedure CREATE_BOOK_DATA_REQUEST (
  pId out BOOK_DATA_REQUEST.ID%type,
  pInn in BOOK_DATA_REQUEST.INN%type,
  pCorrectionNumber in BOOK_DATA_REQUEST.CORRECTIONNUMBER%type,
  pPartitionNumber in BOOK_DATA_REQUEST.PARTITIONNUMBER%type,
  pPeriod in BOOK_DATA_REQUEST.PERIOD%type,
  pYear in BOOK_DATA_REQUEST.YEAR%type,
  pPriority in BOOK_DATA_REQUEST.PRIORITY%type,
  pResultRecordQuantity out number,
  pDeclarationId out SOV_DECLARATION_INFO.DECLARATION_VERSION_ID%type
  )
as
begin

  select MAX(DECLARATION_VERSION_ID)
  into pDeclarationId
  from V$Declaration
  where INN = pInn AND TAX_PERIOD = pPeriod AND FISCAL_YEAR = pYear;

  if pDeclarationId IS NULL then
    begin
      select MAX(DECLARATION_VERSION_ID) into pDeclarationId from SOV_DECLARATION_INFO;
    end;
  end if;

  select SEQ_SOV_REQUEST.Nextval into pId from DUAL;

  select COUNT(*) into pResultRecordQuantity from FIR.INVOICE_RAW where DECLARATION_VERSION_ID = pDeclarationId;

  insert into BOOK_DATA_REQUEST values (pId,pInn,pCorrectionNumber,pPartitionNumber,pPeriod,pYear,0, Sysdate, null, pPriority);

end;


procedure UPDATE_BOOK_DATA_REQUEST(
  pId in BOOK_DATA_REQUEST.ID%type,
  pStatus in BOOK_DATA_REQUEST.STATUS%type
  )
as
begin
  update BOOK_DATA_REQUEST
  set STATUS = pStatus
  where ID = pId;
end;

procedure CREATE_CAGNT_DATA_REQUEST (
  pId out CONTRAGENT_DATA_REQUEST.ID%type,
  pInn in CONTRAGENT_DATA_REQUEST.INN%type,
  pCorrectionNumber in CONTRAGENT_DATA_REQUEST.CORRECTION_NUMBER%type,
  pPeriod in CONTRAGENT_DATA_REQUEST.PERIOD%type,
  pYear in CONTRAGENT_DATA_REQUEST.TAX_YEAR%type
  )
as begin
  select SEQ_SOV_REQUEST.Nextval into pId from DUAL;
  insert into CONTRAGENT_DATA_REQUEST (id, inn, correction_number, period, tax_year, request_status, request_date)
  values (pId, pInn, pCorrectionNumber, pPeriod, pYear, 1, Sysdate);
end;

procedure GENERATE_CAGNT_DATA (
  pId in CONTRAGENT_DATA_REQUEST.ID%type,
  pInn in V$DECLARATIONINSPECT.INN%type,
  pCorrectionNumber in V$DECLARATIONINSPECT.CORRECTION_NUMBER%type,
  pPeriod in V$DECLARATIONINSPECT.TAX_PERIOD%type,
  pYear in CONTRAGENT_DATA_REQUEST.TAX_YEAR%type
  )
as begin
  insert into nds2_mrr_user.sov_contragents
  select
	pId,
    decl.declaration_version_id,
    inv.seller_inn as contractor_inn,
    sum(inv.price_buy_amount) as total_amount,
    sum(inv.price_buy_nds_amount) as nds_amount,
    round(sum(inv.price_buy_nds_amount)/
      (select sum(i.price_buy_nds_amount) from fir.invoice_raw i
       where i.declaration_version_id = decl.declaration_version_id and i.chapter = 8), 2),
    count(*)
  from nds2_mrr_user.v$declarationinspect decl
  join fir.invoice_raw inv on inv.declaration_version_id = decl.declaration_version_id and inv.chapter = 8
  where decl.INN = pInn and decl.CORRECTION_NUMBER = pCorrectionNumber
    and decl.TAX_PERIOD = pPeriod and decl.FISCAL_YEAR = pYear
  group by decl.declaration_version_id, inv.seller_inn;
  update CONTRAGENT_DATA_REQUEST set REQUEST_STATUS = 2, process_date = sysdate
  where ID = pId;
end;

procedure CREATE_CAGNT_PARAMS_REQUEST (
  pId out CONTRAGENT_PARAMS_REQUEST.ID%type,
  pInn in CONTRAGENT_PARAMS_REQUEST.INN%type,
  pContractorInn in CONTRAGENT_PARAMS_REQUEST.CONTRACTOR_INN%type,
  pCorrectionNumber in CONTRAGENT_PARAMS_REQUEST.CORRECTION_NUMBER%type,
  pPeriod in CONTRAGENT_PARAMS_REQUEST.PERIOD%type,
  pYear in CONTRAGENT_PARAMS_REQUEST.TAX_YEAR%type
  )
as begin
  select SEQ_SOV_REQUEST.Nextval into pId from DUAL;
  insert into CONTRAGENT_PARAMS_REQUEST (id, inn, CONTRACTOR_INN, correction_number, period, tax_year, request_status, request_date)
  values (pId, pInn, pContractorInn, pCorrectionNumber, pPeriod, pYear, 1, Sysdate);
end;


procedure GENERATE_CAGNT_PARAMS (
  pId in CONTRAGENT_PARAMS_REQUEST.ID%type,
  pInn in V$DECLARATIONINSPECT.INN%type,
  pContractorInn in CONTRAGENT_PARAMS_REQUEST.CONTRACTOR_INN%type,  
  pCorrectionNumber in V$DECLARATIONINSPECT.CORRECTION_NUMBER%type,
  pPeriod in V$DECLARATIONINSPECT.TAX_PERIOD%type,
  pYear in CONTRAGENT_PARAMS_REQUEST.TAX_YEAR%type
  )
as begin
  insert into nds2_mrr_user.SOV_CONTRAGENT_PARAMS
    (ROW_KEY, DECLARATION_VERSION_ID, seller_inn, seller_kpp, nds_weight)
  select
    inv.row_key,
    decl.declaration_version_id,
    inv.seller_inn,
    inv.seller_kpp,
    round(inv.price_buy_nds_amount/
      (select sum(i.price_buy_nds_amount) from fir.invoice_raw i
       where i.declaration_version_id = decl.declaration_version_id and i.chapter = 8), 2)
  from nds2_mrr_user.v$declarationinspect decl
  join fir.invoice_raw inv on inv.declaration_version_id = decl.declaration_version_id and inv.chapter = 8
  where decl.INN = pInn and decl.CORRECTION_NUMBER = pCorrectionNumber
    and decl.TAX_PERIOD = pPeriod and decl.FISCAL_YEAR = pYear;
  update CONTRAGENT_PARAMS_REQUEST set REQUEST_STATUS = 2, process_date = sysdate
  where ID = pId;
end;

procedure COPY_BOOK_DATA (
  pRequestId in BOOK_DATA_REQUEST.ID%type,
  pDeclarationId in SOV_DECLARATION_INFO.ID%type,
  pIndexFrom in NUMBER,
  pIndexTo in NUMBER
  )
as
begin

 insert into SOV_INVOICE
       with indexed_invoices as (
            select ROWNUM as row_index, x.*
            from (
                 select inv.ROW_KEY
                 from FIR.INVOICE_RAW inv
                 where inv.DECLARATION_VERSION_ID=pDeclarationId
                 order by inv.ROW_KEY) x)
        select pRequestId, data.*, 0 as IS_DOP_LIST, 0 as IS_IMPORT, null as CLARIFICATION_KEY, null as contragent_key
        from
               indexed_invoices idx
               join FIR.INVOICE_RAW data on data.ROW_KEY=idx.ROW_KEY
        where idx.row_index > pIndexFrom and idx.row_index < pIndexTo;
end;


procedure CREATE_DISCREPANCY_REQUEST (
  pId OUT DISCREPANCY_REQUEST.ID%type,
  pInn IN DISCREPANCY_REQUEST.INN%type,
  pPeriod IN DISCREPANCY_REQUEST.PERIOD%type,
  pYear IN DISCREPANCY_REQUEST.YEAR%type,
  pCorrectionNumber IN DISCREPANCY_REQUEST.CORRECTION_NUMBER%type,
  pType IN DISCREPANCY_REQUEST.TYPE%type,
  pKind IN DISCREPANCY_REQUEST.KIND%type
  )
as
begin
  select SEQ_SOV_REQUEST.NEXTVAL into pId from DUAL;
  insert into DISCREPANCY_REQUEST (ID, STATUS, REQUEST_DATE, INN, PERIOD, YEAR, CORRECTION_NUMBER, KIND, TYPE)
         values (pId, 0, SYSDATE, pInn, pPeriod, pYear, pCorrectionNumber, pKind, pType);
end;


procedure COPY_DISCREPANCIES (
  pRequestId  IN DISCREPANCY_REQUEST.ID%type
  )
as
begin
  insert into SOV_DISCREPANCY_TMP
         (SOV_ID,CREATE_DATE,TYPE,COMPARE_KIND,RULE_GROUP,DEAL_AMNT,AMNT,AMOUNT_PVP,COURSE,COURSE_COST,SUR_CODE,INVOICE_CHAPTER,INVOICE_RK,DECL_ID,INVOICE_CONTRACTOR_CHAPTER,INVOICE_CONTRACTOR_RK,DECL_CONTRACTOR_ID,STATUS,REQUEST_ID,SIDE_PRIMARY_PROCESSING)
   select
         misMatch.SOV_ID,
         misMatch.CREATE_DATE,
         misMatch.TYPE,
         misMatch.COMPARE_KIND,
         misMatch.RULE_GROUP,
         misMatch.DEAL_AMNT,
         misMatch.AMNT,
     misMatch.AMOUNT_PVP,
         misMatch.COURSE,
         misMatch.COURSE_COST,
         misMatch.SUR_CODE,
         misMatch.INVOICE_CHAPTER,
         misMatch.INVOICE_RK,
         misMatch.DECL_ID,
         misMatch.INVOICE_CONTRACTOR_CHAPTER,
         misMatch.INVOICE_CONTRACTOR_RK,
         misMatch.DECL_CONTRACTOR_ID,
         misMatch.STATUS,
         rq.ID,
         misMatch.SIDE_PRIMARY_PROCESSING
   from
         FIR.DISCREPANCY_RAW misMatch
         join V$DECLARATION decl ON decl.ID = misMatch.DECL_ID
         join DISCREPANCY_REQUEST rq ON trim(rq.INN) = decl.INN AND rq.PERIOD = decl.TAX_PERIOD AND rq.YEAR = decl.FISCAL_YEAR
   where
         rq.ID = pRequestId;
end;

procedure PROCESS_INVOICE_REQUEST (
  pRequestId IN INVOICE_REQUEST.ID%type
  )
as
begin

  insert into SOV_INVOICE
         select pRequestId, src.*, 0 as IS_DOP_LIST, 0 as IS_IMPORT, null as CLARIFICATION_KEY, null as contragent_key
         from
                FIR.INVOICE_RAW src
         where exists(
               select 1
               from INVOICE_REQUEST_DATA ids
               where
                    ids.request_id=pRequestId and (ids.invoice_rk=src.row_key or ids.invoice_rk=src.actual_row_key));

  insert into SOV_DISCREPANCY
         (ID,SOV_ID,CREATE_DATE,TYPE,COMPARE_KIND,RULE_GROUP,DEAL_AMNT,AMNT,AMOUNT_PVP,COURSE,COURSE_COST,SUR_CODE,INVOICE_CHAPTER,INVOICE_RK,DECL_ID,INVOICE_CONTRACTOR_CHAPTER,INVOICE_CONTRACTOR_RK,DECL_CONTRACTOR_ID,STATUS,SIDE_PRIMARY_PROCESSING)
   select distinct
         src.ID,
         src.ID,
         src.CREATE_DATE,
         src.TYPE,
         src.COMPARE_KIND,
         src.RULE_GROUP,
         src.DEAL_AMNT,
         src.AMNT,
         src.AMOUNT_PVP,
         src.COURSE,
         src.COURSE_COST,
         src.SUR_CODE,
         src.INVOICE_CHAPTER,
         src.INVOICE_RK,
         src.DECL_ID,
         src.INVOICE_CONTRACTOR_CHAPTER,
         src.INVOICE_CONTRACTOR_RK,
         src.DECL_CONTRACTOR_ID,
         src.STATUS,
     src.SIDE_PRIMARY_PROCESSING
   from
         FIR.DISCREPANCY_RAW src
         join SOV_INVOICE mrr_inv on src.invoice_rk=mrr_inv.row_key or src.invoice_rk=mrr_inv.actual_row_key
         left join SOV_DISCREPANCY mrr on mrr.id=src.id
   where mrr.id is null;

   update invoice_request set status = 2 where id = pRequestId;

end;

procedure CREATE_PYRAMID_REQUEST(
  pId OUT SOV_PYRAMID_REQUEST.REQUEST_ID%type,
  pInn IN SOV_PYRAMID_REQUEST.CONTRACTOR_INN%type,
  pFiscalYear IN SOV_PYRAMID_REQUEST.FISCAL_YEAR%type,
  pPeriod IN SOV_PYRAMID_REQUEST.PERIOD%type
   )
as
   requestExists int;
begin

  select COUNT(*)
  into requestExists
  from SOV_PYRAMID_REQUEST
  where contractor_inn = pInn AND period = pPeriod AND fiscal_year = pFiscalYear;

  if requestExists = 1 then
    begin
     select request_id
     into pId
     from SOV_PYRAMID_REQUEST
     where contractor_inn = pInn AND period = pPeriod AND fiscal_year = pFiscalYear;
    end;
  else
    begin
     select SEQ_SOV_REQUEST.NEXTVAL into pId from DUAL;

     insert into SOV_PYRAMID_REQUEST(
         REQUEST_ID,
         STATUS_ID,
         CONTRACTOR_INN,
         REQUEST_TYPE,
         FISCAL_YEAR,
         PERIOD,
         REQUEST_DATE)
    values(
         pId,
         0,
         pInn,
         0,
         pFiscalYear,
         pPeriod,
         SysDate);
    end;
  end if;

end;

procedure GENERATE_CONTRACTOR(
  pId IN NUMBER,
  pName IN VARCHAR2,
  pInn IN VARCHAR2,
  pKpp IN VARCHAR2
  )
as
begin

  insert into TEST_PYRAMID_SUMMARY(
         ID,
         CONTRACTOR_NAME,
         CONTRACTOR_INN,
         CONTRACTOR_KPP)
   values (
          pId,
          pName,
          pInn,
          pKpp);

end;

procedure PROCESS_PYRAMID_REQUEST(
  pId IN SOV_PYRAMID_REQUEST.REQUEST_ID%type
  )
as
  tmp int;
begin

  select count(*)
  into tmp
  from
            SOV_PYRAMID_REQUEST rq
       join TEST_PYRAMID_TREE tst on tst.parent_inn=rq.contractor_inn
  where rq.request_id=pId;

  if tmp = 0 then
    begin
      select count(*) into tmp from TEST_PYRAMID_SUMMARY;
      select dbms_random.value(1, tmp - 20) into tmp from DUAL;

      insert into TEST_PYRAMID_TREE (parent_inn,parent_kpp,child_id)
      with q as (
           select rownum as idx, t.id
           from (
                select * from TEST_PYRAMID_SUMMARY order by contractor_inn
           ) t)
      select rq.contractor_inn, null as contractor_kpp,q.id
      from SOV_PYRAMID_REQUEST rq, q
      where rq.request_id=pId and q.idx > tmp and q.idx < tmp + dbms_random.value(5,20);
    end;
  end if;

  insert into SOV_PYRAMID_INFO(
        id,
        request_id,
        contractor_name,
        contractor_inn,
        contractor_kpp,
        decl_provided,
        SYR,
        DECL_NDS_8,
        DECL_NDS_9,
        SUM_NDS,
        SUM_RUB,
        SUM_RUB_MATCHED,
        DISCR_TOTAL_SUM,
        DISCR_TOTAL_DEAL_COUNT,
        DISCR_TOTAL_DEAL_SUM,
        DISCR_GAP_TOTAL_COUNT,
        DISCR_GAP_TOTAL_SUM,
        DISCR_GAP_DEAL_COUNT,
        DISCR_GAP_DEAL_SUM,
        DISCR_WEAK_TOTAL_COUNT,
        DISCR_WEAK_TOTAL_SUM,
        DISCR_WEAK_DEAL_COUNT,
        DISCR_WEAK_DEAL_SUM,
        DISCR_NDS_TOTAL_COUNT,
        DISCR_NDS_TOTAL_SUM,
        DISCR_NDS_DEAL_COUNT,
        DISCR_NDS_DEAL_SUM
             )
       select
             SEQ_SOV_PYRAMID_INFO.NEXTVAL,
             pId,
             data.contractor_name,
             data.contractor_inn,
             data.contractor_kpp,
             '1',
			 round(dbms_random.value(1,4),1),
             round(dbms_random.value(0,100),2),
             round(dbms_random.value(0,100),2),
             round(dbms_random.value(0,100),2),
             round(dbms_random.value(0,100),2),
             round(dbms_random.value(0,100),2),
             round(dbms_random.value(0,100),2),
             round(dbms_random.value(0,100),2),
             round(dbms_random.value(0,100),2),
             round(dbms_random.value(0,100),2),
             round(dbms_random.value(0,100),2),
             round(dbms_random.value(0,100),2),
             round(dbms_random.value(0,100),2),
             round(dbms_random.value(0,100),2),
             round(dbms_random.value(0,100),2),
             round(dbms_random.value(0,100),2),
             round(dbms_random.value(0,100),2),
             round(dbms_random.value(0,100),2),
             round(dbms_random.value(0,100),2),
             round(dbms_random.value(0,100),2),
             round(dbms_random.value(0,100))
       from
             SOV_PYRAMID_REQUEST rq
             join TEST_PYRAMID_TREE tree on tree.parent_inn=rq.contractor_inn
             join TEST_PYRAMID_SUMMARY data on tree.child_id=data.id
       where rq.request_id=pId and rq.status_id = 0;

  update SOV_PYRAMID_REQUEST
  set status_id = 2
  where request_id = pId;

end;

procedure CREATE_LK_REPORT_REQUEST (
  pId out SOV_LK_REPORT_REQUEST.REQUEST_ID%type,
  pBody in SOV_LK_REPORT_REQUEST.REQUEST_BODY%type,
  pStatus in SOV_LK_REPORT_REQUEST.STATUS_ID%type
  )
as
begin

  select FIR.MRR_REQ_SEQ.NEXTVAL into pId from DUAL;
  insert into SOV_LK_REPORT_REQUEST (REQUEST_ID, REQUEST_BODY, STATUS_ID) values (pId, pBody, pStatus);
end;


procedure UPDATE_LK_REPORT_REQUEST (
  pId in SOV_LK_REPORT_REQUEST.REQUEST_ID%type,
  pStatus in SOV_LK_REPORT_REQUEST.STATUS_ID%type
  )
as
begin

  update SOV_LK_REPORT_REQUEST set STATUS_ID = pStatus where REQUEST_ID = pId;

end;

procedure FILL_REPORT_LK (
  pRequestId in SOV_LK_REPORT_REQUEST.REQUEST_ID%type
  )
as
begin

      INSERT INTO SOV_LK_REPORT (request_id, error_code, cnt, cnt_bad_mismatch, cnt_break_mismatch, cnt_in, cnt_out)
      VALUES (pRequestId, 8101,91,7,9,11,13);
      INSERT INTO SOV_LK_REPORT (request_id, error_code, cnt, cnt_bad_mismatch, cnt_break_mismatch, cnt_in, cnt_out)
      VALUES (pRequestId, 8102,91,7,10,15,18);
      INSERT INTO SOV_LK_REPORT (request_id, error_code, cnt, cnt_bad_mismatch, cnt_break_mismatch, cnt_in, cnt_out)
      VALUES (pRequestId, 8011,36,3,5,8,11);
      INSERT INTO SOV_LK_REPORT (request_id, error_code, cnt, cnt_bad_mismatch, cnt_break_mismatch, cnt_in, cnt_out)
      VALUES (pRequestId, 8014, 362,27,29,32,37);
      INSERT INTO SOV_LK_REPORT (request_id, error_code, cnt, cnt_bad_mismatch, cnt_break_mismatch, cnt_in, cnt_out)
      VALUES (pRequestId, 8017,91,7,9,12,15);
      INSERT INTO SOV_LK_REPORT (request_id, error_code, cnt, cnt_bad_mismatch, cnt_break_mismatch, cnt_in, cnt_out)
      VALUES (pRequestId, 8020,18,1,3,6,9);
      INSERT INTO SOV_LK_REPORT (request_id, error_code, cnt, cnt_bad_mismatch, cnt_break_mismatch, cnt_in, cnt_out)
      VALUES (pRequestId, 8023,362,27,31,35,38);
      INSERT INTO SOV_LK_REPORT (request_id, error_code, cnt, cnt_bad_mismatch, cnt_break_mismatch, cnt_in, cnt_out)
      VALUES (pRequestId, 9023,181,14,17,19,23);
      INSERT INTO SOV_LK_REPORT (request_id, error_code, cnt, cnt_bad_mismatch, cnt_break_mismatch, cnt_in, cnt_out)
      VALUES (pRequestId, 8029,18,1,3,6,9);
      INSERT INTO SOV_LK_REPORT (request_id, error_code, cnt, cnt_bad_mismatch, cnt_break_mismatch, cnt_in, cnt_out)
      VALUES (pRequestId, 8029,18,1,3,6,9);
      INSERT INTO SOV_LK_REPORT (request_id, error_code, cnt, cnt_bad_mismatch, cnt_break_mismatch, cnt_in, cnt_out)
      VALUES (pRequestId, 8024,72,5,9,12,15);

end;

procedure CREATE_NAVIGATOR_REQUEST (
  pRequestId out number,
  pMonthFrom in number,
  pYearFrom in number,
  pMonthTo in number,
  pYearTo in number,
  pDepth in number,
  pMinMappedAmount in number,
  pMaxContractorsQuantity in number,
  pMinBuyerAmount in number,
  pMinSellerAmount in number)
as
begin
  select SEQ_SOV_NAVIGATOR_REQUEST.NEXTVAL into pRequestId from DUAL;  
  insert into SOV_NAVIGATOR_REQUEST(REQUEST_ID,MONTH_FROM,YEAR_FROM,MONTH_TO,YEAR_TO,DEPTH,MIN_MAPPED_AMOUNT,MAX_CONTRACTORS_QUANTITY,MIN_BUYER_AMOUNT,MIN_SELLER_AMOUNT,STATUS_ID,REQUEST_DATE)
         values (pRequestId, pMonthFrom, pYearFrom, pMonthTo, pYearTo, pDepth, pMinMappedAmount, pMaxContractorsQuantity,pMinBuyerAmount,pMinSellerAmount,0, Sysdate);
end;
  
procedure INSERT_NAVIGATOR_TAXPAYER (
  pRequestId in number,
  pInn in nvarchar2)
as
begin
  insert into SOV_NAVIGATOR_REQUEST_TAXPAYER(REQUEST_ID,INN) values (pRequestId, pInn);
end;

procedure PROCESS_NAVIGATOR_REQUEST (
  pRequestId in number)
as
begin
  update SOV_NAVIGATOR_PAIR set REQUEST_ID = pRequestId;
  update SOV_NAVIGATOR_REQUEST set STATUS_ID = 2 where REQUEST_ID = pRequestId;
end;

end "NDS2$EMULATOR";
/
