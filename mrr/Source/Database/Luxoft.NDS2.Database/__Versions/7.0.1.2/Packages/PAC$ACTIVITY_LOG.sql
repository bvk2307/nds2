CREATE OR REPLACE PACKAGE NDS2_MRR_USER.PAC$ACTIVITY_LOG IS

PROCEDURE WRITE (
  pSid in ACTIVITY_LOG.SID%TYPE,
  pEntryId in ACTIVITY_LOG.ACTIVITY_ID%TYPE,
  pElapsMilsc in ACTIVITY_LOG.ELAPSED_MILLISEC%TYPE,
  pCount in ACTIVITY_LOG.COUNT%TYPE
);

PROCEDURE PREPARE_REPORT (
  pDate in DATE
);

END;
/

CREATE OR REPLACE PACKAGE BODY NDS2_MRR_USER.PAC$ACTIVITY_LOG IS

PROCEDURE WRITE (
  pSid in ACTIVITY_LOG.SID%TYPE,
  pEntryId in ACTIVITY_LOG.ACTIVITY_ID%TYPE,
  pElapsMilsc in ACTIVITY_LOG.ELAPSED_MILLISEC%TYPE,
  pCount in ACTIVITY_LOG.COUNT%TYPE
) as
BEGIN
  INSERT INTO NDS2_MRR_USER.ACTIVITY_LOG
         (ACTIVITY_ID, TIMESTAMP, ELAPSED_MILLISEC, COUNT, SID)
  VALUES
         (pEntryId, sysdate, pElapsMilsc, pCount, pSid);
  commit;
  exception when others then
    Nds2$sys.LOG_ERROR('Activity report - Write', null, sqlcode, substr(sqlerrm, 256));
END;

PROCEDURE PREPARE_REPORT (
  pDate in DATE
) as
  pHour number;
  pDay date;
  pBegin date;
  pEnd date;
  v_logons number;
  v_decl_open number;
  v_decl_ext_open number;
  v_man_sel_call number;
  v_reports number;
BEGIN
  pDay := trunc(pDate);
  pHour := 1/24;
  EXECUTE IMMEDIATE 'truncate table ACTIVITY_LOG_REPORT';
  FOR i IN 1..24 LOOP
    pBegin := pDay + (i - 1)*pHour;
    pEnd := pBegin + pHour;
    select
        sum(case when al.activity_id = 2 then 1 else 0 end) as logons,
        sum(case when al.activity_id = 3 then 1 else 0 end) as decl_open,
        sum(case when al.activity_id = 4 then 1 else 0 end) as decl_ext_open,
        sum(case when al.activity_id = 5 then 1 else 0 end) as man_sel_call,
        sum(case when al.activity_id = 6 then 1 else 0 end) as reports
    into v_logons, v_decl_open, v_decl_ext_open, v_man_sel_call, v_reports
    from (
        select distinct sid, activity_id, count from activity_log
        where timestamp between pBegin and pEnd
         ) al;
      
    insert into ACTIVITY_LOG_REPORT 
    select
        pBegin, pEnd,
        v_logons,
        sum(case when al.activity_id = 3 then 1 else 0 end) as decl_open,
        sum(case when al.activity_id = 3 then al.count else 0 end) as decl_size,
        sum(case when al.activity_id = 4 then 1 else 0 end) as decl_ext_open,
        sum(case when al.activity_id = 5 then 1 else 0 end) as man_sel_call,
        sum(case when al.activity_id = 6 then 1 else 0 end) as reports,
        sum(case when al.activity_id = 7 then al.elapsed_millisec else 0 end) as decl_time,
        sum(case when al.activity_id = 8 then al.elapsed_millisec else 0 end) as decl_sov_time,
        sum(case when al.activity_id = 9 then al.elapsed_millisec else 0 end) as decl_mrr_time,
        sum(case when al.activity_id = 11 then al.elapsed_millisec else 0 end) as reports_time,
        min(case when al.activity_id = 7 then al.elapsed_millisec else null end) as decl_time_min,
        max(case when al.activity_id = 7 then al.elapsed_millisec else null end) as decl_time_max,
        round(avg(case when al.activity_id = 7 then al.elapsed_millisec else null end)) as decl_time_avg,
        min(case when al.activity_id = 3 then al.count else null end) as decl_size_min,
        max(case when al.activity_id = 3 then al.count else null end) as decl_size_max,
        round(avg(case when al.activity_id = 3 then al.count else null end)) as decl_size_avg,
        min(case when al.activity_id = 8 then al.elapsed_millisec else null end) as decl_sov_time_min,
        max(case when al.activity_id = 8 then al.elapsed_millisec else null end) as decl_sov_time_max,
        round(avg(case when al.activity_id = 8 then al.elapsed_millisec else null end)) as decl_sov_time_avg,
        min(case when al.activity_id = 9 then al.elapsed_millisec else null end) as decl_mrr_time_min,
        max(case when al.activity_id = 9 then al.elapsed_millisec else null end) as decl_mrr_time_max,
        round(avg(case when al.activity_id = 9 then al.elapsed_millisec else null end)) as decl_mrr_time_avg,
        min(case when al.activity_id = 11 then al.elapsed_millisec else null end) as reports_time_min,
        max(case when al.activity_id = 11 then al.elapsed_millisec else null end) as reports_time_max,
        round(avg(case when al.activity_id = 11 then al.elapsed_millisec else null end)) as reports_time_avg,
        min(case when al.activity_id = 10 then al.elapsed_millisec else null end) as man_sel_time_min,
        max(case when al.activity_id = 10 then al.elapsed_millisec else null end) as man_sel_time_max,
        round(avg(case when al.activity_id = 10 then al.elapsed_millisec else null end)) as man_sel_time_avg,
        v_decl_open,
        v_decl_ext_open,
        v_man_sel_call,
        v_reports
    from activity_log al
    where timestamp between pBegin and pEnd;
  END LOOP;
  COMMIT;
  exception when others then
    Nds2$sys.LOG_ERROR('Activity report - Prepare', null, sqlcode, substr(sqlerrm, 256));
END;

END;
/
