﻿CREATE OR REPLACE PACKAGE NDS2_MRR_USER."NDS2$REPORTS" IS

procedure START_WORK;
procedure START_DECLARATION_CALCULATE;

TYPE T_ASSOCIATIVE_ARRAY IS TABLE OF VARCHAR(50) INDEX BY PLS_INTEGER;

PROCEDURE P$REPORT_SAVE_TASK
(
      pReportNum IN NUMBER,
      pFederalCodes IN T_ASSOCIATIVE_ARRAY,
      pRegionCodes IN T_ASSOCIATIVE_ARRAY,
      pSounCodes IN T_ASSOCIATIVE_ARRAY,
      pNalogPeriod IN NUMBER,
      pFiscalYear IN NUMBER,
      pDateBegin IN Date,
      pDateEnd IN Date,
      pDateCreateReport IN Date,
      pDeclStatisticType IN NUMBER,
      pDeclStatisticRegim IN NUMBER,      
      pTaskId OUT NUMBER
);

PROCEDURE P$REPORT_DP_SAVE_TASK
(
      pReportNum IN NUMBER,
      pDynamicParameters IN T_ASSOCIATIVE_ARRAY,
      pDynamicAggregateLevel IN NUMBER,
      pDynamicTimeCounting IN NUMBER,
      pDateBegin IN Date,
      pTaskId OUT NUMBER
);

PROCEDURE P$REPORT_DDS_SAVE_TASK
(
      pReportNum IN NUMBER,
      pDynamicParameters IN T_ASSOCIATIVE_ARRAY,
      pFederalCodes IN T_ASSOCIATIVE_ARRAY,
      pRegionCodes IN T_ASSOCIATIVE_ARRAY,
      pSounCodes IN T_ASSOCIATIVE_ARRAY,
      pAggregateByNalogOrgan IN NUMBER,
      pAggregateByTime IN NUMBER,
      pTypeReport IN NUMBER,
      pNalogPeriod IN VARCHAR,
      pFiscalYear IN NUMBER,
      pDateBeginDay IN DATE,
      pDateEndDay IN DATE,
      pDateBeginWeek IN DATE,
      pDateEndWeek IN DATE,
      pYearBegin IN NUMBER,
      pMonthBegin IN NUMBER,
      pYearEnd IN NUMBER,
      pMonthEnd IN NUMBER,
      pTaskId OUT NUMBER
);

PROCEDURE P$REPORT_IRM_SAVE_TASK
(
      pReportNum IN NUMBER,
      pFederalCodes IN T_ASSOCIATIVE_ARRAY,
      pRegionCodes IN T_ASSOCIATIVE_ARRAY,
      pSounCodes IN T_ASSOCIATIVE_ARRAY,
      pNalogPeriods IN T_ASSOCIATIVE_ARRAY,
      pAggregateByNalogOrgan IN NUMBER,
      pTimePeriod IN NUMBER,
      pDateOneDay IN DATE,
      pDateBegin IN DATE,
      pDateEnd IN DATE,
      pTaskId OUT NUMBER
);

PROCEDURE P$REPORT_MR_SAVE_TASK
(
      pReportNum IN NUMBER,
      pFederalCodes IN T_ASSOCIATIVE_ARRAY,
      pRegionCodes IN T_ASSOCIATIVE_ARRAY,
      pSounCodes IN T_ASSOCIATIVE_ARRAY,
      pAggregateByNalogOrgan IN NUMBER,
      pDateOneDay IN DATE,
      pTaskId OUT NUMBER
);

function F$GET_LOAD_INSP_NEXTVAL return number;
function F$GET_MONITOR_DECL_NEXTVAL return number;
function F$GET_INSPEC_MONITOR_NEXTVAL return number;
function F$GET_CHECK_CR_NEXTVAL return number;
function F$GET_MATCHING_RULE_NEXTVAL return number;
function F$GET_CHECK_LC_NEXTVAL return number;
function F$GET_DECLARATION_RAW_NEXTVAL return number;
function F$GET_DECLARATION_NEXTVAL return number;
function F$GET_DYNAMIC_PARAMS_NEXTVAL return number;
function F$GET_INFO_RES_MATCH_NEXTVAL return number;
procedure P$DO_REPORT_CREATE;
procedure P$REPORT_CREATE
(
    pReportNum IN NUMBER,
    pTaskId IN NUMBER,
    pRequestId IN NUMBER
);
procedure P$CREATE_INSPEC_MONITOR_WORK
(
  pTaskId IN NUMBER
);
procedure P$CREATE_MONITOR_DECLARATION
(
  pTaskId IN NUMBER
);
procedure P$CREATE_LOADING_INSPECTION(
  pTaskId IN NUMBER
);
procedure P$CREATE_CHECK_CONTROL_RATIO
(
  pTaskId IN NUMBER
);
procedure P$CREATE_MATCHING_RULE
(
  pTaskId IN NUMBER
);
procedure P$CREATE_CHECK_LOGIC_CONTROL
(
  pTaskId IN NUMBER,
  pRequestId IN NUMBER
);
procedure P$REPORT_LK_REQUEST_STATUS (
  pRequestId in SOV_LK_REPORT_REQUEST.REQUEST_ID%type,
  pData out SYS_REFCURSOR
);
procedure P$CALCULATE_DECLARATION_RAW
(
   pSubmitDate IN DATE
);
procedure P$CREATE_DECLARATION
(
  pTaskId IN NUMBER
);
procedure P$CREATE_DYNAMIC_TECHNO_PARAMS
(
  pTaskId IN NUMBER
);
function F$TECHNO_PARAM_TO_CHAR 
(
   pValue IN NUMBER,
   pParameter_id IN NUMBER
)
return varchar;

function F$TECHNO_PARAM_TO_CHAR_TYPE 
(
   pValue IN NUMBER,
   pDataType IN VARCHAR2
)
return varchar;

function F$GET_LAST_DAY_IN_MONTH
(
   pMonth IN NUMBER
)
return NUMBER;

function F$IS_DATE_TIME
(
   pValue IN VARCHAR2
)
return NUMBER;

function F$IS_NUMBER
(
   pValue IN VARCHAR2
)
return NUMBER;

procedure P$CREATE_DYNAMIC_DECLARATION
(
  pTaskId IN NUMBER
);

procedure P$CREATE_INFO_RESULT_MATCHING
(
  pTaskId IN NUMBER
);

END NDS2$REPORTS;
/

CREATE OR REPLACE PACKAGE BODY  NDS2_MRR_USER."NDS2$REPORTS" IS

procedure START_WORK
  as
begin
  NDS2$SYS.LOG_INFO(
  P_SITE => 'NDS2$REPORTS.START_WORK',   P_ENTITY_ID => null,
  P_MESSAGE_CODE => SQLCODE,   P_MESSAGE => 'starting');
  begin
    P$DO_REPORT_CREATE;
  exception when others then null;
  end;
end;

procedure START_DECLARATION_CALCULATE
  as
  pDateCalculate DATE;
begin
  NDS2$SYS.LOG_INFO(
  P_SITE => 'NDS2$REPORTS.START_DECLARATION_CALCULATE',   P_ENTITY_ID => null,
  P_MESSAGE_CODE => SQLCODE,   P_MESSAGE => 'starting');
  begin
    if SYSDATE >= TO_DATE(TO_CHAR(SYSDATE, 'dd/mm/yyyy') || ' 22:00:00', 'dd/mm/yyyy HH24:mi:ss') then
	   pDateCalculate := SYSDATE;    
    else
	   pDateCalculate := TO_DATE(TO_CHAR(SYSDATE - 1, 'dd/mm/yyyy') || ' 23:59:59', 'dd/mm/yyyy HH24:mi:ss');    
    end if;
    P$CALCULATE_DECLARATION_RAW(pDateCalculate);
  exception when others then null;
  end;
end;

PROCEDURE P$REPORT_SAVE_TASK
(
      pReportNum IN NUMBER,
      pFederalCodes IN T_ASSOCIATIVE_ARRAY,
      pRegionCodes IN T_ASSOCIATIVE_ARRAY,
      pSounCodes IN T_ASSOCIATIVE_ARRAY,
      pNalogPeriod IN NUMBER,
      pFiscalYear IN NUMBER,
      pDateBegin IN Date,
      pDateEnd IN Date,
      pDateCreateReport IN Date,
      pDeclStatisticType IN NUMBER,
      pDeclStatisticRegim IN NUMBER,
      pTaskId OUT NUMBER
)
as
  task_id number(10);
begin
  task_id := SEQ_REPORT_TASK.NEXTVAL;
  INSERT INTO REPORT_TASK VALUES(task_id, pReportNum, SYSDATE, 0, NULL);

  if pNalogPeriod is not null then
     INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', pNalogPeriod);
     if pReportNum in (51, 13, 14, 21, 17, 16, 12) then
     begin
       if pNalogPeriod = 21 then
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 51);
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 01);
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 71);
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 02);
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 72);
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 03);
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 73);
       end if;
       if pNalogPeriod = 22 then
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 54);
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 04);
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 74);
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 05);
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 75);
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 06);
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 76);
       end if;
       if pNalogPeriod = 23 then
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 55);
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 07);
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 77);
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 08);
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 78);
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 09);
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 79);
       end if;
       if pNalogPeriod = 24 then
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 56);
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 10);
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 80);
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 11);
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 81);
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 12);
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 82);
       end if;
     end;
     end if;
  End If;
  if pFiscalYear is not null then
     INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'FISCAL_YEAR', pFiscalYear);
  End If;
  if pDateBegin is not null then
    INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'DATE_BEGIN', pDateBegin);
  End If;
  if pDateEnd is not null then
    INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'DATE_END', pDateEnd);
  End If;
  if pDateCreateReport is not null then
     INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'DATE_CREATE_REPORT', pDateCreateReport);
  End If;

  if pDeclStatisticType is not null then
     INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'DECL_STATISTIC_TYPE', pDeclStatisticType);
  End If;
  if pDeclStatisticRegim is not null then
     INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'DECL_STATISTIC_REGIM', pDeclStatisticRegim);
  End If;
  
  FORALL i IN pFederalCodes.FIRST .. pFederalCodes.LAST
  INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'FEDERAL_CODE', pFederalCodes(i));

  FORALL i IN pRegionCodes.FIRST .. pRegionCodes.LAST
  INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'REGION_CODE', pRegionCodes(i));

  FORALL i IN pSounCodes.FIRST .. pSounCodes.LAST
  INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'SOUN_CODE', pSounCodes(i));

  commit;

  pTaskId := task_id;
end;

PROCEDURE P$REPORT_DP_SAVE_TASK
(
      pReportNum IN NUMBER,
      pDynamicParameters IN T_ASSOCIATIVE_ARRAY,
      pDynamicAggregateLevel IN NUMBER,
      pDynamicTimeCounting IN NUMBER,
      pDateBegin IN Date,
      pTaskId OUT NUMBER
)
as
  task_id number(10);
begin
  task_id := SEQ_REPORT_TASK.NEXTVAL;
  INSERT INTO REPORT_TASK VALUES(task_id, pReportNum, SYSDATE, 0, NULL);

  if pDynamicAggregateLevel is not null then
     INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'DYNAMIC_AGGREGATE_LEVEL', pDynamicAggregateLevel);
  End If;
  if pDynamicTimeCounting is not null then
     INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'DYNAMIC_TIME_COUNTING', pDynamicTimeCounting);
  End If;
  if pDateBegin is not null then
    INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'DYNAMIC_DATE_BEGIN', pDateBegin);
  End If;

  FORALL i IN pDynamicParameters.FIRST .. pDynamicParameters.LAST
  INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'DYNAMIC_PARAMETERS', pDynamicParameters(i));

  commit;

  pTaskId := task_id;
end;

PROCEDURE P$REPORT_DDS_SAVE_TASK
(
      pReportNum IN NUMBER,
      pDynamicParameters IN T_ASSOCIATIVE_ARRAY,
      pFederalCodes IN T_ASSOCIATIVE_ARRAY,
      pRegionCodes IN T_ASSOCIATIVE_ARRAY,
      pSounCodes IN T_ASSOCIATIVE_ARRAY,
      pAggregateByNalogOrgan IN NUMBER,
      pAggregateByTime IN NUMBER,
      pTypeReport IN NUMBER,
      pNalogPeriod IN VARCHAR,
      pFiscalYear IN NUMBER,
      pDateBeginDay IN DATE,
      pDateEndDay IN DATE,
      pDateBeginWeek IN DATE,
      pDateEndWeek IN DATE,
      pYearBegin IN NUMBER,
      pMonthBegin IN NUMBER,
      pYearEnd IN NUMBER,
      pMonthEnd IN NUMBER,
      pTaskId OUT NUMBER
)
as
  task_id number(10);
begin
  task_id := SEQ_REPORT_TASK.NEXTVAL;
  INSERT INTO REPORT_TASK VALUES(task_id, pReportNum, SYSDATE, 0, NULL);

  if pAggregateByNalogOrgan is not null then
     INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'AGGREGATE_BY_NALOG_ORGAN', pAggregateByNalogOrgan);
  End If;
  if pAggregateByTime is not null then
     INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'AGGREGATE_BY_TIME', pAggregateByTime);
  End If;
  if pTypeReport is not null then
     INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'TYPE_REPORT', pTypeReport);
  End If;
  if pFiscalYear is not null then
     INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'FISCAL_YEAR', pFiscalYear);
  End If;
  
  if pDateBeginDay is not null then
     INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'DATE_BEGIN_DAY', pDateBeginDay);
  End If;
  if pDateEndDay is not null then
     INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'DATE_END_DAY', pDateEndDay);
  End If;
  if pDateBeginWeek is not null then
     INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'DATE_BEGIN_WEEK', pDateBeginWeek);
  End If;
  if pDateEndWeek is not null then
     INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'DATE_END_WEEK', pDateEndWeek);
  End If;
  
  if pYearBegin is not null then
     INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'YEAR_BEGIN', pYearBegin);
  End If;
  if pMonthBegin is not null then
     INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'MONTH_BEGIN', pMonthBegin);
  End If;
  if pYearEnd is not null then
     INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'YEAR_END', pYearEnd);
  End If;
  if pMonthEnd is not null then
     INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'MONTH_END', pMonthEnd);
  End If;

  if pNalogPeriod is not null then
     INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', pNalogPeriod);

	if pNalogPeriod = 21 then
		INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 51);
		INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 01);
		INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 71);
		INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 02);
		INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 72);
		INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 03);
		INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 73);
	end if;
	if pNalogPeriod = 22 then
		INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 54);
		INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 04);
		INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 74);
		INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 05);
		INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 75);
		INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 06);
		INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 76);
	end if;
	if pNalogPeriod = 23 then
		INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 55);
		INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 07);
		INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 77);
		INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 08);
		INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 78);
		INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 09);
		INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 79);
	end if;
	if pNalogPeriod = 24 then
		INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 56);
		INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 10);
		INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 80);
		INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 11);
		INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 81);
		INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 12);
		INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 82);
	end if;
  End If;

  FORALL i IN pDynamicParameters.FIRST .. pDynamicParameters.LAST
  INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'DYNAMIC_PARAMETERS', pDynamicParameters(i));

  FORALL i IN pFederalCodes.FIRST .. pFederalCodes.LAST
  INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'FEDERAL_CODE', pFederalCodes(i));

  FORALL i IN pRegionCodes.FIRST .. pRegionCodes.LAST
  INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'REGION_CODE', pRegionCodes(i));

  FORALL i IN pSounCodes.FIRST .. pSounCodes.LAST
  INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'SOUN_CODE', pSounCodes(i));

  commit;

  pTaskId := task_id;
end;

PROCEDURE P$REPORT_IRM_SAVE_TASK
(
      pReportNum IN NUMBER,
      pFederalCodes IN T_ASSOCIATIVE_ARRAY,
      pRegionCodes IN T_ASSOCIATIVE_ARRAY,
      pSounCodes IN T_ASSOCIATIVE_ARRAY,
      pNalogPeriods IN T_ASSOCIATIVE_ARRAY,
      pAggregateByNalogOrgan IN NUMBER,
      pTimePeriod IN NUMBER,
      pDateOneDay IN DATE,
      pDateBegin IN DATE,
      pDateEnd IN DATE,
      pTaskId OUT NUMBER
)
as
  task_id number(10);
begin
  task_id := SEQ_REPORT_TASK.NEXTVAL;
  INSERT INTO REPORT_TASK VALUES(task_id, pReportNum, SYSDATE, 0, NULL);

  if pAggregateByNalogOrgan is not null then
     INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'AGGREGATE_BY_NALOG_ORGAN', pAggregateByNalogOrgan);
  End If;
  if pTimePeriod is not null then
     INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'TIME_PERIOD', pTimePeriod);
  End If;

  if pDateOneDay is not null then
     INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'DATE_ONE_DAY', pDateOneDay);
  End If;
  if pDateBegin is not null then
     INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'DATE_BEGIN', pDateBegin);
  End If;
  if pDateEnd is not null then
     INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'DATE_END', pDateEnd);
  End If;

  FORALL i IN pFederalCodes.FIRST .. pFederalCodes.LAST
  INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'FEDERAL_CODE', pFederalCodes(i));

  FORALL i IN pRegionCodes.FIRST .. pRegionCodes.LAST
  INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'REGION_CODE', pRegionCodes(i));

  FORALL i IN pSounCodes.FIRST .. pSounCodes.LAST
  INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'SOUN_CODE', pSounCodes(i));
  
  FORALL i IN pNalogPeriods.FIRST .. pNalogPeriods.LAST
  INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD_CHECK', pNalogPeriods(i));
  
  commit;

  pTaskId := task_id;
end;

PROCEDURE P$REPORT_MR_SAVE_TASK
(
      pReportNum IN NUMBER,
      pFederalCodes IN T_ASSOCIATIVE_ARRAY,
      pRegionCodes IN T_ASSOCIATIVE_ARRAY,
      pSounCodes IN T_ASSOCIATIVE_ARRAY,
      pAggregateByNalogOrgan IN NUMBER,
      pDateOneDay IN DATE,
      pTaskId OUT NUMBER
)
as
  task_id number(10);
begin
  task_id := SEQ_REPORT_TASK.NEXTVAL;
  INSERT INTO REPORT_TASK VALUES(task_id, pReportNum, SYSDATE, 0, NULL);

  if pAggregateByNalogOrgan is not null then
     INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'AGGREGATE_BY_NALOG_ORGAN', pAggregateByNalogOrgan);
  End If;

  if pDateOneDay is not null then
     INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'DATE_ONE_DAY', pDateOneDay);
  End If;

  FORALL i IN pFederalCodes.FIRST .. pFederalCodes.LAST
  INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'FEDERAL_CODE', pFederalCodes(i));

  FORALL i IN pRegionCodes.FIRST .. pRegionCodes.LAST
  INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'REGION_CODE', pRegionCodes(i));

  FORALL i IN pSounCodes.FIRST .. pSounCodes.LAST
  INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'SOUN_CODE', pSounCodes(i));

  commit;
  pTaskId := task_id;
end;


function F$GET_LOAD_INSP_NEXTVAL return number
is
Result number;
begin
  select SEQ_REPORT_LOAD_INSP.NEXTVAL into Result from dual;
  return Result;
end;

function F$GET_MONITOR_DECL_NEXTVAL return number
is
Result number;
begin
  select SEQ_REPORT_MONITOR_DECL.NEXTVAL into Result from dual;
  return Result;
end;

function F$GET_INSPEC_MONITOR_NEXTVAL return number
is
Result number;
begin
  select SEQ_REPORT_INSPEC_MONITOR_WORK.NEXTVAL into Result from dual;
  return Result;
end;

function F$GET_CHECK_CR_NEXTVAL return number
is
Result number;
begin
  select SEQ_REPORT_CHECK_CR.NEXTVAL into Result from dual;
  return Result;
end;

function F$GET_MATCHING_RULE_NEXTVAL return number
is
Result number;
begin
  select SEQ_REPORT_MATCHING_RULE.NEXTVAL into Result from dual;
  return Result;
end;

function F$GET_CHECK_LC_NEXTVAL return number
is
Result number;
begin
  select SEQ_REPORT_LOGIC_CONTROL.NEXTVAL into Result from dual;
  return Result;
end;

function F$GET_DECLARATION_RAW_NEXTVAL return number
is
Result number;
begin
  select SEQ_REPORT_DECLARATION_RAW.NEXTVAL into Result from dual;
  return Result;
end;


function F$GET_DECLARATION_NEXTVAL return number
is
Result number;
begin
  select SEQ_REPORT_DECLARATION.NEXTVAL into Result from dual;
  return Result;
end;

function F$GET_DYNAMIC_PARAMS_NEXTVAL return number
is
Result number;
begin
  select SEQ_REPORT_DYNAMIC_PARAMETERS.NEXTVAL into Result from dual;
  return Result;
end;

function F$GET_INFO_RES_MATCH_NEXTVAL return number
is
Result number;
begin
  select SEQ_REPORT_INFO_RES_MATCHING.NEXTVAL into Result from dual;
  return Result;
end;

procedure P$DO_REPORT_CREATE
as
  CURSOR tasks IS
	SELECT * FROM REPORT_TASK WHERE TASK_STATUS = 0;
	v_tasks tasks%ROWTYPE;
begin

  OPEN tasks;
	FETCH tasks INTO v_tasks;
	LOOP

    --P$REPORT_CREATE(v_tasks.report_num, v_tasks.ID);

		FETCH tasks INTO v_tasks;
		EXIT WHEN tasks%NOTFOUND;
	END LOOP;

	CLOSE tasks;

end;

procedure P$REPORT_CREATE
(
    pReportNum IN NUMBER,
    pTaskId IN NUMBER,
    pRequestId IN NUMBER
)
as
begin

    if pReportNum = 16 then
        P$CREATE_INSPEC_MONITOR_WORK (pTaskId);
    elsif pReportNum = 17 then
        P$CREATE_MONITOR_DECLARATION (pTaskId);
    elsif pReportNum = 21 then
        P$CREATE_LOADING_INSPECTION (pTaskId);
    elsif pReportNum = 14 then
        P$CREATE_CHECK_CONTROL_RATIO (pTaskId);
    elsif pReportNum = 12 then
        P$CREATE_MATCHING_RULE (pTaskId);
    elsif pReportNum = 13 then
        P$CREATE_CHECK_LOGIC_CONTROL (pTaskId, pRequestId);
    elsif pReportNum = 51 then
        P$CREATE_DECLARATION (pTaskId);
    elsif pReportNum = 52 then
        P$CREATE_DYNAMIC_TECHNO_PARAMS (pTaskId);
    elsif pReportNum = 53 then
        P$CREATE_DYNAMIC_DECLARATION (pTaskId);  
    elsif pReportNum = 54 then
        P$CREATE_INFO_RESULT_MATCHING (pTaskId);
	end if;

end;

-- Создание отчета Мониторинг работы налоговых инспекторов
procedure P$CREATE_INSPEC_MONITOR_WORK
(
  pTaskId IN NUMBER
)
as
  pTaskStatus NUMBER := -1;
  pDateBegin DATE;
  pDateEnd DATE;
  pFiscalYear NUMBER(5);
  pActualNumActive Number(1);

  CURSOR reports IS
  SELECT * FROM REPORT_INSPECTOR_MONITOR_WORK WHERE TASK_ID = pTaskId;
  v_reports reports%ROWTYPE;

  pNotEliminated_GeneralCount NUMBER(19);
  pNotEliminated_GeneralSum NUMBER(19,2);
  pNotElimin_CurrencyCount NUMBER(19);
  pNotElimin_CurrencySum NUMBER(19,2);
  pNotElimin_NDSCount NUMBER(19);
  pNotElimin_NDSSum NUMBER(19,2);
  pNotElimin_NotExactCount NUMBER(19);
  pNotElimin_NotExactSum NUMBER(19,2);
  pNotElimin_GapCount NUMBER(19);
  pNotElimin_GapSum NUMBER(19,2);
  pIdentNotElimin_GeneralCount NUMBER(19);
  pIdentNotElimin_GeneralSum NUMBER(19,2);
  pIdentNotElimin_CurrencyCount NUMBER(19);
  pIdentNotElimin_CurrencySum NUMBER(19,2);
  pIdentNotElimin_NDSCount NUMBER(19);
  pIdentNotElimin_NDSSum NUMBER(19,2);
  pIdentNotElimin_NotExactCount NUMBER(19);
  pIdentNotElimin_NotExactSum NUMBER(19,2);
  pIdentNotElimin_GapCount NUMBER(19);
  pIdentNotElimin_GapSum NUMBER(19,2);
  pIdentElimin_GeneralCount NUMBER(19);
  pIdentElimin_GeneralSum NUMBER(19,2);
  pIdentElimin_CurrencyCount NUMBER(19);
  pIdentElimin_CurrencySum NUMBER(19,2);
  pIdentElimin_NDSCount NUMBER(19);
  pIdentElimin_NDSSum NUMBER(19,2);
  pIdentElimin_NotExactCount NUMBER(19);
  pIdentElimin_NotExactSum NUMBER(19,2);
  pIdentElimin_GapCount NUMBER(19);
  pIdentElimin_GapSum NUMBER(19,2);
begin
  pActualNumActive := 1;

  SELECT task_status into pTaskStatus FROM dual
  left outer join (SELECT task_status FROM REPORT_TASK WHERE ID = pTaskId) r on 1 = 1;

  if pTaskStatus = 0 then
  begin
      -- получение одиночных параметров поиска
      SELECT PARAM_VALUE into pDateBegin FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'DATE_BEGIN';
      SELECT PARAM_VALUE into pDateEnd FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'DATE_END';
      SELECT PARAM_VALUE into pFiscalYear FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'FISCAL_YEAR';

      -- создание уникальных строк РЕГИОН-ИНСПЕКЦИЯ
      INSERT INTO REPORT_INSPECTOR_MONITOR_WORK (ID, TASK_ID, ACTUAL_NUM, AGG_ROW, FEDERALCODE, FEDERALDISTRICT, REGIONCODE, REGION, INSPECTIONCODE, INSPECTION, INSPECTOR)
      SELECT
        NDS2$REPORTS.F$GET_LOAD_INSP_NEXTVAL
        ,pTaskId
        ,pActualNumActive
        ,0 as AGG_ROW
        ,fed.district_id as FederalCode
        ,fed.description as FederalDistrict
        ,reg.S_CODE as RegionCode
        ,reg.S_CODE || '-' || reg.S_NAME as Region
        ,s.S_CODE as InspectionCode
        ,s.S_CODE || '-' || s.S_NAME as Inspection
        ,(case when (d.INSPECTOR is null) or (d.INSPECTOR is not null and trim(d.INSPECTOR) = '') then 'Не назначен' else d.INSPECTOR end) as Inspector
      FROM
      (
        SELECT dis.TAXPAYERREGIONCODE, dis.TAXPAYERSOUNCODE, dis.INSPECTOR
        FROM DOC dc
        JOIN V$DISCREPANCY dis on dc.DOC_ID = dis.CLAIM_ID
        WHERE dis.TaxPayerSounCode in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'SOUN_CODE')
          AND dis.TaxPayerRegionCode in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'REGION_CODE')
          AND dis.TaxPayerPeriodCode in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD')
          AND dis.TaxPayerYearCode = pFiscalYear
        GROUP BY dis.TAXPAYERREGIONCODE, dis.TAXPAYERSOUNCODE, dis.INSPECTOR
      ) d
      LEFT OUTER JOIN V$SSRF reg on reg.S_CODE = d.TAXPAYERREGIONCODE
      LEFT OUTER JOIN V$SONO s on s.S_CODE = d.TAXPAYERSOUNCODE
      LEFT OUTER JOIN 
      (
            SELECT fdr.region_code as region_code, fd.district_id as district_id, fd.description as description 
            FROM FEDERAL_DISTRICT_REGION fdr
            JOIN FEDERAL_DISTRICT fd on fd.district_id = fdr.district_id
      ) fed on fed.region_code = reg.S_CODE
      WHERE (d.TAXPAYERREGIONCODE is not null) and (d.TAXPAYERSOUNCODE is not null);

      -- Наполнение колонок(кол-во, сумма) всех уникальных строк
      OPEN reports;
      FETCH reports INTO v_reports;
      LOOP
        select nvl(ch01.NotEliminated_GeneralCount, 0), nvl(ch01.NotEliminated_GeneralSum, 0),
               nvl(ch01.NotElimin_CurrencyCount, 0), nvl(ch01.NotElimin_CurrencySum, 0),
               nvl(ch01.NotElimin_NDSCount, 0), nvl (ch01.NotElimin_NDSSum, 0), nvl(ch01.NotElimin_NotExactCount, 0),
               nvl(ch01.NotElimin_NotExactSum, 0), nvl(ch01.NotElimin_GapCount, 0), nvl(ch01.NotElimin_GapSum, 0)
        into pNotEliminated_GeneralCount, pNotEliminated_GeneralSum, pNotElimin_CurrencyCount, pNotElimin_CurrencySum,
             pNotElimin_NDSCount, pNotElimin_NDSSum, pNotElimin_NotExactCount, pNotElimin_NotExactSum,
             pNotElimin_GapCount, pNotElimin_GapSum from dual
        left outer join
        (
          SELECT distinct TAXPAYERREGIONCODE, TAXPAYERSOUNCODE
            ,COUNT(DISCREPANCYID) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) NotEliminated_GeneralCount
            ,SUM(Amount) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) NotEliminated_GeneralSum
            ,COUNT(case when typecode=3 then DISCREPANCYID end) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) NotElimin_CurrencyCount
            ,SUM(case when typecode=3 then Amount end) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) NotElimin_CurrencySum
            ,COUNT(case when typecode=4 then DISCREPANCYID end) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) NotElimin_NDSCount
            ,SUM(case when typecode=4 then Amount end) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) NotElimin_NDSSum
            ,COUNT(case when typecode=2 then DISCREPANCYID end) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) NotElimin_NotExactCount
            ,SUM(case when typecode=2 then Amount end) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) NotElimin_NotExactSum
            ,COUNT(case when typecode=1 then DISCREPANCYID end) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) NotElimin_GapCount
            ,SUM(case when typecode=1 then Amount end) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) NotElimin_GapSum
          FROM DOC dc
          JOIN V$DISCREPANCY dis on dc.DOC_ID = dis.CLAIM_ID
          WHERE dis.TaxPayerSounCode in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'SOUN_CODE')
            AND dis.TaxPayerRegionCode in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'REGION_CODE')
            AND dis.TaxPayerPeriodCode in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD')
            AND dis.TaxPayerYearCode = pFiscalYear
            AND trunc(dis.FoundDate) <= trunc(pDateBegin) AND trunc(dc.Create_Date) <= trunc(pDateBegin)
            AND (trunc(dis.Close_Date) > trunc(pDateBegin) or dis.Close_Date is null)
        ) ch01 on ch01.TAXPAYERREGIONCODE = v_reports.RegionCode and ch01.TAXPAYERSOUNCODE = v_reports.InspectionCode;

        select nvl(ch01a.IdentNotElimin_GeneralCount, 0), nvl(ch01a.IdentNotElimin_GeneralSum, 0), nvl(ch01a.IdentNotElimin_CurrencyCount, 0),
               nvl(ch01a.IdentNotElimin_CurrencySum, 0), nvl(ch01a.IdentNotElimin_NDSCount, 0), nvl(ch01a.IdentNotElimin_NDSSum, 0),
               nvl(ch01a.IdentNotElimin_NotExactCount, 0), nvl(ch01a.IdentNotElimin_NotExactSum, 0),
               nvl(ch01a.IdentNotElimin_GapCount, 0), nvl(ch01a.IdentNotElimin_GapSum, 0)
        into pIdentNotElimin_GeneralCount, pIdentNotElimin_GeneralSum, pIdentNotElimin_CurrencyCount, pIdentNotElimin_CurrencySum,
             pIdentNotElimin_NDSCount, pIdentNotElimin_NDSSum, pIdentNotElimin_NotExactCount, pIdentNotElimin_NotExactSum,
             pIdentNotElimin_GapCount, pIdentNotElimin_GapSum
         from dual
        left outer join
        (
          SELECT distinct TAXPAYERREGIONCODE, TAXPAYERSOUNCODE
            ,COUNT(DISCREPANCYID) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) IdentNotElimin_GeneralCount
            ,SUM(Amount) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) IdentNotElimin_GeneralSum
            ,COUNT(case when typecode=3 then DISCREPANCYID end) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) IdentNotElimin_CurrencyCount
            ,SUM(case when typecode=3 then Amount end) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) IdentNotElimin_CurrencySum
            ,COUNT(case when typecode=4 then DISCREPANCYID end) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) IdentNotElimin_NDSCount
            ,SUM(case when typecode=4 then Amount end) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) IdentNotElimin_NDSSum
            ,COUNT(case when typecode=2 then DISCREPANCYID end) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) IdentNotElimin_NotExactCount
            ,SUM(case when typecode=2 then Amount end) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) IdentNotElimin_NotExactSum
            ,COUNT(case when typecode=1 then DISCREPANCYID end) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) IdentNotElimin_GapCount
            ,SUM(case when typecode=1 then Amount end) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) IdentNotElimin_GapSum
          FROM DOC dc
          JOIN V$DISCREPANCY dis on dc.DOC_ID = dis.CLAIM_ID
          WHERE dis.TaxPayerSounCode in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'SOUN_CODE')
            AND dis.TaxPayerRegionCode in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'REGION_CODE')
            AND dis.TaxPayerPeriodCode in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD')
            AND dis.TaxPayerYearCode = pFiscalYear
            AND trunc(dis.FoundDate) <= trunc(pDateBegin) AND trunc(dc.Create_Date) <= trunc(pDateBegin)
            AND (trunc(dis.Close_Date) > trunc(pDateEnd) or dis.Close_Date is null)
        ) ch01a on ch01a.TAXPAYERREGIONCODE = v_reports.RegionCode and ch01a.TAXPAYERSOUNCODE = v_reports.InspectionCode;

        pIdentElimin_GeneralCount := pNotEliminated_GeneralCount - pIdentNotElimin_GeneralCount;
        pIdentElimin_GeneralSum := pNotEliminated_GeneralSum - pIdentNotElimin_GeneralSum;
        pIdentElimin_CurrencyCount := pNotElimin_CurrencyCount - pIdentNotElimin_CurrencyCount;
        pIdentElimin_CurrencySum := pNotElimin_CurrencySum - pIdentNotElimin_CurrencySum;
        pIdentElimin_NDSCount := pNotElimin_NDSCount - pIdentNotElimin_NDSCount;
        pIdentElimin_NDSSum := pNotElimin_NDSSum - pIdentNotElimin_NDSSum;
        pIdentElimin_NotExactCount := pNotElimin_NotExactCount - pIdentNotElimin_NotExactCount;
        pIdentElimin_NotExactSum := pNotElimin_NotExactSum - pIdentNotElimin_NotExactSum;
        pIdentElimin_GapCount := pNotElimin_GapCount - pIdentNotElimin_GapCount;
        pIdentElimin_GapSum := pNotElimin_GapSum - pIdentNotElimin_GapSum;

        UPDATE REPORT_INSPECTOR_MONITOR_WORK r SET
                NotEliminated_GeneralCount = pNotEliminated_GeneralCount,
                NotEliminated_GeneralSum = pNotEliminated_GeneralSum,
                NotElimin_CurrencyCount = pNotElimin_CurrencyCount,
                NotElimin_CurrencySum = pNotElimin_CurrencySum,
                NotElimin_NDSCount = pNotElimin_NDSCount,
                NotElimin_NDSSum = pNotElimin_NDSSum,
                NotElimin_NotExactCount = pNotElimin_NotExactCount,
                NotElimin_NotExactSum = pNotElimin_NotExactSum,
                NotElimin_GapCount = pNotElimin_GapCount,
                NotElimin_GapSum = pNotElimin_GapSum,
                IdentNotElimin_GeneralCount = pIdentNotElimin_GeneralCount,
                IdentNotElimin_GeneralSum = pIdentNotElimin_GeneralSum,
                IdentNotElimin_CurrencyCount = pIdentNotElimin_CurrencyCount,
                IdentNotElimin_CurrencySum = pIdentNotElimin_CurrencySum,
                IdentNotElimin_NDSCount = pIdentNotElimin_NDSCount,
                IdentNotElimin_NDSSum = pIdentNotElimin_NDSSum,
                IdentNotElimin_NotExactCount = pIdentNotElimin_NotExactCount,
                IdentNotElimin_NotExactSum = pIdentNotElimin_NotExactSum,
                IdentNotElimin_GapCount = pIdentNotElimin_GapCount,
                IdentNotElimin_GapSum = pIdentNotElimin_GapSum,
                IdentElimin_GeneralCount =  pIdentElimin_GeneralCount,
                IdentElimin_GeneralSum  = pIdentElimin_GeneralSum,
                IdentElimin_CurrencyCount = pIdentElimin_CurrencyCount,
                IdentElimin_CurrencySum = pIdentElimin_CurrencySum,
                IdentElimin_NDSCount = pIdentElimin_NDSCount,
                IdentElimin_NDSSum = pIdentElimin_NDSSum,
                IdentElimin_NotExactCount = pIdentElimin_NotExactCount,
                IdentElimin_NotExactSum = pIdentElimin_NotExactSum,
                IdentElimin_GapCount = pIdentElimin_GapCount,
                IdentElimin_GapSum = pIdentElimin_GapSum
        WHERE r.Region = v_reports.Region and r.Inspection = v_reports.Inspection;

        FETCH reports INTO v_reports;
        EXIT WHEN reports%NOTFOUND;
      END LOOP;

      CLOSE reports;

      -- Создание агрегирующих строк (один регион, одна инспекция, все инспекторы)
      INSERT INTO REPORT_INSPECTOR_MONITOR_WORK
      SELECT NDS2$REPORTS.F$GET_LOAD_INSP_NEXTVAL
            ,pTaskId
            ,pActualNumActive
            ,1 as AGG_ROW
            ,MIN(i.FederalCode) as FederalCode
            ,MIN(i.FederalDistrict) as FederalDistrict
            ,MIN(i.REGIONCODE) as RegionCode
            ,MIN(i.Region) as Region
            ,i.InspectionCode as InspectionCode
            ,MIN(i.Inspection) as Inspection
            ,'*' as Inspector
            ,SUM(i.NotEliminated_GeneralCount) as NotEliminated_GeneralCount
            ,SUM(i.NotEliminated_GeneralSum) as NotEliminated_GeneralSum
            ,SUM(i.NotElimin_CurrencyCount) as NotElimin_CurrencyCount
            ,SUM(i.NotElimin_CurrencySum) as NotElimin_CurrencySum
            ,SUM(i.NotElimin_NDSCount) as NotElimin_NDSCount
            ,SUM(i.NotElimin_NDSSum) as NotElimin_NDSSum
            ,SUM(i.NotElimin_NotExactCount) as NotElimin_NotExactCount
            ,SUM(i.NotElimin_NotExactSum) as NotElimin_NotExactSum
            ,SUM(i.NotElimin_GapCount) as NotElimin_GapCount
            ,SUM(i.NotElimin_GapSum) as NotElimin_GapSum
            ,SUM(i.IdentNotElimin_GeneralCount) as IdentNotElimin_GeneralCount
            ,SUM(i.IdentNotElimin_GeneralSum) as IdentNotElimin_GeneralSum
            ,SUM(i.IdentNotElimin_CurrencyCount) as IdentNotElimin_CurrencyCount
            ,SUM(i.IdentNotElimin_CurrencySum) as IdentNotElimin_CurrencySum
            ,SUM(i.IdentNotElimin_NDSCount) as IdentNotElimin_NDSCount
            ,SUM(i.IdentNotElimin_NDSSum) as IdentNotElimin_NDSSum
            ,SUM(i.IdentNotElimin_NotExactCount) as IdentNotElimin_NotExactCount
            ,SUM(i.IdentNotElimin_NotExactSum) as IdentNotElimin_NotExactSum
            ,SUM(i.IdentNotElimin_GapCount) as IdentNotElimin_GapCount
            ,SUM(i.IdentNotElimin_GapSum) as IdentNotElimin_GapSum
            ,SUM(i.IdentElimin_GeneralCount) as IdentElimin_GeneralCount
            ,SUM(i.IdentElimin_GeneralSum) as IdentElimin_GeneralSum
            ,SUM(i.IdentElimin_CurrencyCount) as IdentElimin_CurrencyCount
            ,SUM(i.IdentElimin_CurrencySum) as IdentElimin_CurrencySum
            ,SUM(i.IdentElimin_NDSCount) as IdentElimin_NDSCount
            ,SUM(i.IdentElimin_NDSSum) as IdentElimin_NDSSum
            ,SUM(i.IdentElimin_NotExactCount) as IdentElimin_NotExactCount
            ,SUM(i.IdentElimin_NotExactSum) as IdentElimin_NotExactSum
            ,SUM(i.IdentElimin_GapCount) as IdentElimin_GapCount
            ,SUM(i.IdentElimin_GapSum) as IdentElimin_GapSum
      FROM REPORT_INSPECTOR_MONITOR_WORK i
      WHERE i.TASK_ID = pTaskId AND i.AGG_ROW = 0

      GROUP BY i.Inspectioncode;

      -- Создание агрегирующих строк (один регион, все инспекции)
      INSERT INTO REPORT_INSPECTOR_MONITOR_WORK
      SELECT NDS2$REPORTS.F$GET_LOAD_INSP_NEXTVAL
            ,pTaskId
            ,pActualNumActive
            ,1 as AGG_ROW
            ,MIN(i.FederalCode) as FederalCode
            ,MIN(i.FederalDistrict) as FederalDistrict
            ,i.REGIONCODE as RegionCode
            ,MIN(i.Region) as Region
            ,'*' as InspectionCode
            ,'*' as Inspection
            ,'*' as Inspector
            ,SUM(i.NotEliminated_GeneralCount) as NotEliminated_GeneralCount
            ,SUM(i.NotEliminated_GeneralSum) as NotEliminated_GeneralSum
            ,SUM(i.NotElimin_CurrencyCount) as NotElimin_CurrencyCount
            ,SUM(i.NotElimin_CurrencySum) as NotElimin_CurrencySum
            ,SUM(i.NotElimin_NDSCount) as NotElimin_NDSCount
            ,SUM(i.NotElimin_NDSSum) as NotElimin_NDSSum
            ,SUM(i.NotElimin_NotExactCount) as NotElimin_NotExactCount
            ,SUM(i.NotElimin_NotExactSum) as NotElimin_NotExactSum
            ,SUM(i.NotElimin_GapCount) as NotElimin_GapCount
            ,SUM(i.NotElimin_GapSum) as NotElimin_GapSum
            ,SUM(i.IdentNotElimin_GeneralCount) as IdentNotElimin_GeneralCount
            ,SUM(i.IdentNotElimin_GeneralSum) as IdentNotElimin_GeneralSum
            ,SUM(i.IdentNotElimin_CurrencyCount) as IdentNotElimin_CurrencyCount
            ,SUM(i.IdentNotElimin_CurrencySum) as IdentNotElimin_CurrencySum
            ,SUM(i.IdentNotElimin_NDSCount) as IdentNotElimin_NDSCount
            ,SUM(i.IdentNotElimin_NDSSum) as IdentNotElimin_NDSSum
            ,SUM(i.IdentNotElimin_NotExactCount) as IdentNotElimin_NotExactCount
            ,SUM(i.IdentNotElimin_NotExactSum) as IdentNotElimin_NotExactSum
            ,SUM(i.IdentNotElimin_GapCount) as IdentNotElimin_GapCount
            ,SUM(i.IdentNotElimin_GapSum) as IdentNotElimin_GapSum
            ,SUM(i.IdentElimin_GeneralCount) as IdentElimin_GeneralCount
            ,SUM(i.IdentElimin_GeneralSum) as IdentElimin_GeneralSum
            ,SUM(i.IdentElimin_CurrencyCount) as IdentElimin_CurrencyCount
            ,SUM(i.IdentElimin_CurrencySum) as IdentElimin_CurrencySum
            ,SUM(i.IdentElimin_NDSCount) as IdentElimin_NDSCount
            ,SUM(i.IdentElimin_NDSSum) as IdentElimin_NDSSum
            ,SUM(i.IdentElimin_NotExactCount) as IdentElimin_NotExactCount
            ,SUM(i.IdentElimin_NotExactSum) as IdentElimin_NotExactSum
            ,SUM(i.IdentElimin_GapCount) as IdentElimin_GapCount
            ,SUM(i.IdentElimin_GapSum) as IdentElimin_GapSum
      FROM REPORT_INSPECTOR_MONITOR_WORK i
      WHERE i.TASK_ID = pTaskId AND i.AGG_ROW = 0
      GROUP BY i.RegionCode;

      -- Создание агрегирующих строк (все регионы)
      INSERT INTO REPORT_INSPECTOR_MONITOR_WORK
      SELECT NDS2$REPORTS.F$GET_LOAD_INSP_NEXTVAL
            ,pTaskId
            ,pActualNumActive
            ,1 as AGG_ROW
            ,'*' as FederalCode
            ,'ИТОГО' as FederalDistrict
            ,'*' as RegionCode
            ,'*' as Region
            ,'*' as InspectionCode
            ,'*' as Inspection
            ,'*' as Inspector
            ,SUM(i.NotEliminated_GeneralCount) as NotEliminated_GeneralCount
            ,SUM(i.NotEliminated_GeneralSum) as NotEliminated_GeneralSum
            ,SUM(i.NotElimin_CurrencyCount) as NotElimin_CurrencyCount
            ,SUM(i.NotElimin_CurrencySum) as NotElimin_CurrencySum
            ,SUM(i.NotElimin_NDSCount) as NotElimin_NDSCount
            ,SUM(i.NotElimin_NDSSum) as NotElimin_NDSSum
            ,SUM(i.NotElimin_NotExactCount) as NotElimin_NotExactCount
            ,SUM(i.NotElimin_NotExactSum) as NotElimin_NotExactSum
            ,SUM(i.NotElimin_GapCount) as NotElimin_GapCount
            ,SUM(i.NotElimin_GapSum) as NotElimin_GapSum
            ,SUM(i.IdentNotElimin_GeneralCount) as IdentNotElimin_GeneralCount
            ,SUM(i.IdentNotElimin_GeneralSum) as IdentNotElimin_GeneralSum
            ,SUM(i.IdentNotElimin_CurrencyCount) as IdentNotElimin_CurrencyCount
            ,SUM(i.IdentNotElimin_CurrencySum) as IdentNotElimin_CurrencySum
            ,SUM(i.IdentNotElimin_NDSCount) as IdentNotElimin_NDSCount
            ,SUM(i.IdentNotElimin_NDSSum) as IdentNotElimin_NDSSum
            ,SUM(i.IdentNotElimin_NotExactCount) as IdentNotElimin_NotExactCount
            ,SUM(i.IdentNotElimin_NotExactSum) as IdentNotElimin_NotExactSum
            ,SUM(i.IdentNotElimin_GapCount) as IdentNotElimin_GapCount
            ,SUM(i.IdentNotElimin_GapSum) as IdentNotElimin_GapSum
            ,SUM(i.IdentElimin_GeneralCount) as IdentElimin_GeneralCount
            ,SUM(i.IdentElimin_GeneralSum) as IdentElimin_GeneralSum
            ,SUM(i.IdentElimin_CurrencyCount) as IdentElimin_CurrencyCount
            ,SUM(i.IdentElimin_CurrencySum) as IdentElimin_CurrencySum
            ,SUM(i.IdentElimin_NDSCount) as IdentElimin_NDSCount
            ,SUM(i.IdentElimin_NDSSum) as IdentElimin_NDSSum
            ,SUM(i.IdentElimin_NotExactCount) as IdentElimin_NotExactCount
            ,SUM(i.IdentElimin_NotExactSum) as IdentElimin_NotExactSum
            ,SUM(i.IdentElimin_GapCount) as IdentElimin_GapCount
            ,SUM(i.IdentElimin_GapSum) as IdentElimin_GapSum
      FROM REPORT_INSPECTOR_MONITOR_WORK i
      WHERE i.TASK_ID = pTaskId AND i.AGG_ROW = 0;

      UPDATE REPORT_TASK SET TASK_STATUS = 1, COMPLETE_DATE = SYSDATE WHERE ID = pTaskId;
      commit;
  end;
  end if;
end;


-- Создание отчета Мониторинг обработки налоговых деклараций
procedure P$CREATE_MONITOR_DECLARATION
(
  pTaskId IN NUMBER
)
as
  pTaskStatus NUMBER := -1;
  pSendDate DATE;
  pCloseDate DATE;
  pFiscalYear NUMBER(5);
  pActualNumActive Number(1);
begin
  pActualNumActive := 1;

  SELECT task_status into pTaskStatus FROM dual
  left outer join (SELECT task_status FROM REPORT_TASK WHERE ID = pTaskId) r on 1 = 1;

  if pTaskStatus = 0 then
  begin
      SELECT PARAM_VALUE into pSendDate FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'DATE_BEGIN';
      SELECT PARAM_VALUE into pCloseDate FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'DATE_END';
      SELECT PARAM_VALUE into pFiscalYear FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'FISCAL_YEAR';

      INSERT INTO REPORT_MONITOR_DECLARATION
      select
          F$GET_MONITOR_DECL_NEXTVAL,
          pTaskId,
          pActualNumActive,
          0 as AGG_ROW,
          fed.district_id as FederalCode,
          fed.description as FederalDistrict,
          dis.REGION_CODE as RegionCode,
          dis.REGION_CODE || '-' || dis.REGION_NAME as Region,
          dis.SOUN_CODE as InspectionCode,
          dis.SOUN_CODE || '-' || dis.SOUN_NAME as Inspection,
          dis.CORRECTION_NUMBER as Corrections,
          dis.INN as INN,
          dis.KPP as KPP,
          dis.NAME as NAME,
          dis.INSPECTOR as INSPECTOR,

          nvl(r1.totalCount, 0) as r1totalCount,
          nvl(r1.totalSum, 0) as r1totalSum,
          nvl(r1lk.sc, 0) as r1lkError,
          nvl(r1.valuteCount, 0) as r1valuteCount,
          nvl(r1.valuteSum, 0) as r1valuteSum,
          nvl(r1.ndsCount, 0) as r1ndsCount,
          nvl(r1.ndsSum, 0) as r1ndsSum,
          nvl(r1.nsCount, 0) as r1nsCount,
          nvl(r1.nsSum, 0) as r1nsSum,
          nvl(r1.breakCount, 0) as r1breakCount,
          nvl(r1.breakSum, 0) as r1breakSum,

          nvl(r2.totalCount, 0) as r2totalCount,
          nvl(r2.totalSum, 0) as r2totalSum,
          nvl(r2lk.sc, 0) as r2lkError,
          nvl(r2.valuteCount, 0) as r2valuteCount,
          nvl(r2.valuteSum, 0) as r2valuteSum,
          nvl(r2.ndsCount, 0) as r2ndsCount,
          nvl(r2.ndsSum, 0) as r2ndsSum,
          nvl(r2.nsCount, 0) as r2nsCount,
          nvl(r2.nsSum, 0) as r2nsSum,
          nvl(r2.breakCount, 0) as r2breakCount,
          nvl(r2.breakSum, 0) as r2breakSum,

          nvl(r3.totalCount, 0) as r3totalCount,
          nvl(r3.totalSum, 0) as r3totalSum,
          nvl(r3.valuteCount, 0) as r3valuteCount,
          nvl(r3.valuteSum, 0) as r3valuteSum,
          nvl(r3.ndsCount, 0) as r3ndsCount,
          nvl(r3.ndsSum, 0) as r3ndsSum,
          nvl(r3.nsCount, 0) as r3nsCount,
          nvl(r3.nsSum, 0) as r3nsSum,
          nvl(r3.breakCount, 0) as r3breakCount,
          nvl(r3.breakSum, 0) as r3breakSum
        from V$DECLARATION dis
        left join (
          select
          distinct
            declaration_id,
            count(*) over (partition by declaration_id) as totalCount,
            sum(amount) over (partition by declaration_id) as totalSum,
            count(case when typecode=3 then typecode end) over(partition by declaration_id) as valuteCount,
            sum(case when typecode=3 then amount else 0 end) over(partition by declaration_id) as valuteSum,
            count(case when typecode=4 then typecode end) over(partition by declaration_id) as ndsCount,
            sum(case when typecode=4 then amount else 0 end) over(partition by declaration_id) as ndsSum,
            count(case when typecode=2 then typecode end) over(partition by declaration_id) as nsCount,
            sum(case when typecode=2 then amount else 0 end) over(partition by declaration_id) as nsSum,
            count(case when typecode=1 then typecode end) over(partition by declaration_id) as breakCount,
            sum(case when typecode=1 then amount else 0 end) over(partition by declaration_id) as breakSum
          from V$DISCREPANCY
          where
            (trunc(FoundDate) <= trunc(pSendDate))
            and
            (
                (CLOSE_DATE > trunc(pSendDate))
                or
                (CLOSE_DATE is null)
            )
        ) r1 on r1.declaration_id = dis.id
        left join (
          select
          distinct
            declaration_id,
            count(*) over (partition by declaration_id) as totalCount,
            sum(amount) over (partition by declaration_id) as totalSum,
            count(case when typecode=3 then typecode end) over(partition by declaration_id) as valuteCount,
            sum(case when typecode=3 then amount else 0 end) over(partition by declaration_id) as valuteSum,
            count(case when typecode=4 then typecode end) over(partition by declaration_id) as ndsCount,
            sum(case when typecode=4 then amount else 0 end) over(partition by declaration_id) as ndsSum,
            count(case when typecode=2 then typecode end) over(partition by declaration_id) as nsCount,
            sum(case when typecode=2 then amount else 0 end) over(partition by declaration_id) as nsSum,
            count(case when typecode=1 then typecode end) over(partition by declaration_id) as breakCount,
            sum(case when typecode=1 then amount else 0 end) over(partition by declaration_id) as breakSum
          from V$DISCREPANCY
          where
            (trunc(FoundDate) <= trunc(pCloseDate))
            and
            (
                (CLOSE_DATE > trunc(pCloseDate))
                or
                (CLOSE_DATE is null)
            )
        ) r2 on r2.declaration_id = dis.id
        left join (
          select
          distinct
            declaration_id,
            count(*) over (partition by declaration_id) as totalCount,
            sum(amount) over (partition by declaration_id) as totalSum,
            count(case when typecode=3 then typecode end) over(partition by declaration_id) as valuteCount,
            sum(case when typecode=3 then amount else 0 end) over(partition by declaration_id) as valuteSum,
            count(case when typecode=4 then typecode end) over(partition by declaration_id) as ndsCount,
            sum(case when typecode=4 then amount else 0 end) over(partition by declaration_id) as ndsSum,
            count(case when typecode=2 then typecode end) over(partition by declaration_id) as nsCount,
            sum(case when typecode=2 then amount else 0 end) over(partition by declaration_id) as nsSum,
            count(case when typecode=1 then typecode end) over(partition by declaration_id) as breakCount,
            sum(case when typecode=1 then amount else 0 end) over(partition by declaration_id) as breakSum
          from V$DISCREPANCY
          where
            STAGE_STATUS_CODE = 10 AND doc_type in (1,5)
            and trunc(CLOSE_DATE) >= trunc(pSendDate)
            and trunc(CLOSE_DATE) <= trunc(pCloseDate)
        ) r3 on r3.declaration_id = dis.id
        left join (
          select id, lk_errors_count as sc
          from V$DECLARATION_HISTORY
          where (trunc(decl_date) < trunc(pSendDate)) and (rownum = 1)
          order by decl_date desc
        ) r1lk on r1lk.id = dis.id
        left join (
          select id, lk_errors_count as sc
          from V$DECLARATION_HISTORY
          where (trunc(decl_date) < trunc(pCloseDate)) and (rownum = 1)
          order by decl_date desc
        ) r2lk on r2lk.id = dis.id
        LEFT OUTER JOIN 
        (
              SELECT fdr.region_code as region_code, fd.district_id as district_id, fd.description as description 
              FROM FEDERAL_DISTRICT_REGION fdr
              JOIN FEDERAL_DISTRICT fd on fd.district_id = fdr.district_id
        ) fed on fed.region_code = dis.REGION_CODE 
        WHERE dis.SOUN_CODE in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'SOUN_CODE')
          AND dis.REGION_CODE in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'REGION_CODE')
          AND dis.TAX_PERIOD in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD')
          AND dis.FISCAL_YEAR = pFiscalYear;

      UPDATE REPORT_TASK SET TASK_STATUS = 1, COMPLETE_DATE = SYSDATE WHERE ID = pTaskId;
      commit;

  end;
  end if;
end;

-- Создание отчета о загруженности инспекций
procedure P$CREATE_LOADING_INSPECTION
(
  pTaskId IN NUMBER
)
as
  pTaskStatus NUMBER := -1;
  pDateBegin DATE;
  pDateEnd DATE;
  pFiscalYear NUMBER(5);

  CURSOR reports IS
  SELECT * FROM REPORT_LOADING_INSPECTION WHERE TASK_ID = pTaskId;
  v_reports reports%ROWTYPE;

  pGeneralCount NUMBER(19);
  pGeneralSum NUMBER(19,2);
  pSentToWorkGeneralCount NUMBER(19);
  pSentToWorkGeneralSum NUMBER(19,2);
  pRepairedGeneralCount NUMBER(19);
  pRepairedGeneralSum NUMBER(19,2);
  pUnrepairedGeneralCount NUMBER(19);
  pUnrepairedGeneralSum NUMBER(19,2);
  pActualNumActive Number(1);
begin
  pActualNumActive := 1;

  SELECT task_status into pTaskStatus FROM dual
  left outer join (SELECT task_status FROM REPORT_TASK WHERE ID = pTaskId) r on 1 = 1;

  if pTaskStatus = 0 then
  begin
      DELETE FROM REPORT_LOADING_INSPECTION WHERE TASK_ID = pTaskId;

      -- получение одиночных параметров поиска
      SELECT PARAM_VALUE into pDateBegin FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'DATE_BEGIN';
      SELECT PARAM_VALUE into pDateEnd FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'DATE_END';
      SELECT PARAM_VALUE into pFiscalYear FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'FISCAL_YEAR';

      -- создание уникальных строк РЕГИОН-ИНСПЕКЦИЯ
      INSERT INTO REPORT_LOADING_INSPECTION (ID, TASK_ID, ACTUAL_NUM, AGG_ROW, FEDERALCODE, FEDERALDISTRICT, REGIONCODE, REGION, INSPECTIONCODE, INSPECTION, INSPECTORCOUNT)
      SELECT
        NDS2$REPORTS.F$GET_LOAD_INSP_NEXTVAL
        ,pTaskId
        ,pActualNumActive
        ,0 as AGG_ROW
        ,fed.district_id as FederalCode
        ,fed.description as FederalDistrict
        ,reg.S_CODE as RegionCode
        ,reg.S_CODE || '-' || reg.S_NAME as Region
        ,s.S_CODE as InspectionCode
        ,s.S_CODE || '-' || s.S_NAME as Inspection
        ,cast(s.Employers_Count as Number(6)) as InspectorCount
      FROM
      (
        SELECT dis.TAXPAYERREGIONCODE, dis.TAXPAYERSOUNCODE
        FROM DOC dc
        JOIN V$DISCREPANCY dis on dc.DOC_ID = dis.CLAIM_ID
        WHERE dis.TaxPayerSounCode in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'SOUN_CODE')
          AND dis.TaxPayerRegionCode in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'REGION_CODE')
          AND dis.TaxPayerPeriodCode in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD')
          AND dis.TaxPayerYearCode = pFiscalYear
        GROUP BY dis.TAXPAYERREGIONCODE, dis.TAXPAYERSOUNCODE
      ) d
      LEFT OUTER JOIN V$SSRF reg on reg.S_CODE = d.TAXPAYERREGIONCODE
      LEFT OUTER JOIN V$SONO s on s.S_CODE = d.TAXPAYERSOUNCODE
      LEFT OUTER JOIN 
      (
            SELECT fdr.region_code as region_code, fd.district_id as district_id, fd.description as description 
            FROM FEDERAL_DISTRICT_REGION fdr
            JOIN FEDERAL_DISTRICT fd on fd.district_id = fdr.district_id
      ) fed on fed.region_code = reg.S_CODE 
      WHERE (d.TAXPAYERREGIONCODE is not null) and (d.TAXPAYERSOUNCODE is not null);

      -- Наполнение колонок(кол-во, сумма) всех уникальных строк
      OPEN reports;
      FETCH reports INTO v_reports;
      LOOP
        select nvl(ch01.GeneralCount, 0), nvl(ch01.GeneralSum, 0) into pGeneralCount, pGeneralSum from dual
        left outer join
        (
            SELECT distinct TAXPAYERREGIONCODE, TAXPAYERSOUNCODE
            ,COUNT(DISCREPANCYID) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) GeneralCount
            ,SUM(Amount) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) GeneralSum
            FROM DOC dc
            JOIN V$DISCREPANCY dis on dc.DOC_ID = dis.CLAIM_ID
            WHERE dis.TaxPayerSounCode in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'SOUN_CODE')
            AND dis.TaxPayerRegionCode in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'REGION_CODE')
            AND dis.TaxPayerPeriodCode in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD')
            AND dis.TaxPayerYearCode = pFiscalYear
            AND trunc(dis.DECL_DATE) >= trunc(pDateBegin) and trunc(dis.DECL_DATE) <= trunc(pDateEnd)
        ) ch01 on ch01.TAXPAYERREGIONCODE = v_reports.RegionCode and ch01.TAXPAYERSOUNCODE = v_reports.InspectionCode;

        select ch02.GeneralCount, ch02.GeneralSum into pSentToWorkGeneralCount, pSentToWorkGeneralSum from dual
        left outer join
        (
          SELECT distinct TAXPAYERREGIONCODE, TAXPAYERSOUNCODE
          ,COUNT(DISCREPANCYID) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) GeneralCount
          ,SUM(Amount) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) GeneralSum
          FROM DOC dc
          JOIN V$DISCREPANCY dis on dc.DOC_ID = dis.CLAIM_ID
          WHERE dis.TaxPayerSounCode in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'SOUN_CODE')
            AND dis.TaxPayerRegionCode in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'REGION_CODE')
            AND dis.TaxPayerPeriodCode in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD')
            AND dis.TaxPayerYearCode = pFiscalYear
			AND trunc(dis.DECL_DATE) >= trunc(pDateBegin) and trunc(dis.DECL_DATE) <= trunc(pDateEnd)
            AND trunc(dc.Create_Date) >= trunc(pDateBegin) and trunc(dc.Create_Date) <= trunc(pDateEnd)
        ) ch02 on ch02.TAXPAYERREGIONCODE = v_reports.RegionCode and ch02.TAXPAYERSOUNCODE = v_reports.InspectionCode;

        select ch03.GeneralCount, ch03.GeneralSum into pRepairedGeneralCount, pRepairedGeneralSum from dual
        left outer join
        (
          SELECT distinct TAXPAYERREGIONCODE, TAXPAYERSOUNCODE
          ,COUNT(DISCREPANCYID) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) GeneralCount
          ,SUM(Amount) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) GeneralSum
          FROM DOC dc
          JOIN V$DISCREPANCY dis on dc.DOC_ID = dis.CLAIM_ID
          WHERE dis.TaxPayerSounCode in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'SOUN_CODE')
            AND dis.TaxPayerRegionCode in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'REGION_CODE')
            AND dis.TaxPayerPeriodCode in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD')
            AND dis.TaxPayerYearCode = pFiscalYear
			AND trunc(dis.DECL_DATE) >= trunc(pDateBegin) and trunc(dis.DECL_DATE) <= trunc(pDateEnd)
            AND trunc(dc.Create_Date) >= trunc(pDateBegin) and trunc(dc.Create_Date) <= trunc(pDateEnd)
            AND (dis.STAGE_STATUS_CODE = 10 AND dc.doc_type in (1,5))
            AND trunc(dis.CLOSE_DATE) >= trunc(pDateBegin) AND trunc(dis.CLOSE_DATE) <= trunc(pDateEnd)
        ) ch03 on ch03.TAXPAYERREGIONCODE = v_reports.RegionCode and ch03.TAXPAYERSOUNCODE = v_reports.InspectionCode;

        select ch04.GeneralCount, ch04.GeneralSum into pUnrepairedGeneralCount, pUnrepairedGeneralSum from dual
        left outer join
        (
          SELECT distinct TAXPAYERREGIONCODE, TAXPAYERSOUNCODE
          ,COUNT(DISCREPANCYID) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) GeneralCount
          ,SUM(Amount) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) GeneralSum
          FROM DOC dc
          JOIN V$DISCREPANCY dis on dc.DOC_ID = dis.CLAIM_ID
          WHERE dis.TaxPayerSounCode in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'SOUN_CODE')
            AND dis.TaxPayerRegionCode in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'REGION_CODE')
            AND dis.TaxPayerPeriodCode in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD')
            AND dis.TaxPayerYearCode = pFiscalYear
			AND trunc(dis.DECL_DATE) >= trunc(pDateBegin) and trunc(dis.DECL_DATE) <= trunc(pDateEnd)
            AND trunc(dc.Create_Date) >= trunc(pDateBegin) and trunc(dc.Create_Date) <= trunc(pDateEnd)
            AND (trunc(dis.CLOSE_DATE) > trunc(pDateEnd) or dis.CLOSE_DATE is null)
        ) ch04 on ch04.TAXPAYERREGIONCODE = v_reports.RegionCode and ch04.TAXPAYERSOUNCODE = v_reports.InspectionCode;

        UPDATE REPORT_LOADING_INSPECTION r SET GeneralCount = pGeneralCount, GeneralSum = pGeneralSum,
               SentToWorkGeneralCount = pSentToWorkGeneralCount, SentToWorkGeneralSum = pSentToWorkGeneralSum,
               RepairedGeneralCount = pRepairedGeneralCount, RepairedGeneralSum = pRepairedGeneralSum,
               UnrepairedGeneralCount = pUnrepairedGeneralCount, UnrepairedGeneralSum = pUnrepairedGeneralSum
        WHERE r.Region = v_reports.Region and r.Inspection = v_reports.Inspection;

        FETCH reports INTO v_reports;
        EXIT WHEN reports%NOTFOUND;
      END LOOP;

      CLOSE reports;

      -- Создание агрегирующих строк (один регион, все инспекции)
      INSERT INTO REPORT_LOADING_INSPECTION
      SELECT NDS2$REPORTS.F$GET_LOAD_INSP_NEXTVAL
            ,pTaskId
            ,pActualNumActive
            ,1 as AGG_ROW
            ,MIN(i.FederalCode) as FederalCode
            ,MIN(i.FederalDistrict) as FederalDistrict
            ,i.REGIONCODE as RegionCode
            ,MIN(i.Region) as Region
            ,'*' as InspectionCode
            ,'*' as Inspection
            ,SUM(i.InspectorCount) as InspectorCount
            ,SUM(i.GeneralCount) as GeneralCount
            ,SUM(i.GeneralSum) as GeneralSum
            ,SUM(i.SentToWorkGeneralCount) as SentToWorkGeneralCount
            ,SUM(i.SentToWorkGeneralSum) as SentToWorkGeneralSum
            ,null as SentToWorkPercent
            ,null as SentToWorkPercentSum
            ,SUM(i.RepairedGeneralCount) as RepairedGeneralCount
            ,SUM(i.RepairedGeneralSum) as RepairedGeneralSum
            ,null as RepairedPercent
            ,null as RepairedPercentSum
            ,SUM(i.UnrepairedGeneralCount) as UnrepairedGeneralCount
            ,SUM(i.UnrepairedGeneralSum) as UnrepairedGeneralSum
      FROM REPORT_LOADING_INSPECTION i
      WHERE i.TASK_ID = pTaskId AND i.AGG_ROW = 0
      GROUP BY i.REGIONCODE;

      -- Создание агрегирующих строк (один федеральный округ, все регионы, все инспекции)
      INSERT INTO REPORT_LOADING_INSPECTION
      SELECT NDS2$REPORTS.F$GET_LOAD_INSP_NEXTVAL
            ,pTaskId
            ,pActualNumActive
            ,1 as AGG_ROW
            ,i.FEDERALCODE as FederalCode
            ,MIN(i.FederalDistrict) as FederalDistrict
            ,'*' as RegionCode
            ,'*' as Region
            ,'*' as InspectionCode
            ,'*' as Inspection
            ,SUM(i.InspectorCount) as InspectorCount
            ,SUM(i.GeneralCount) as GeneralCount
            ,SUM(i.GeneralSum) as GeneralSum
            ,SUM(i.SentToWorkGeneralCount) as SentToWorkGeneralCount
            ,SUM(i.SentToWorkGeneralSum) as SentToWorkGeneralSum
            ,null as SentToWorkPercent
            ,null as SentToWorkPercentSum
            ,SUM(i.RepairedGeneralCount) as RepairedGeneralCount
            ,SUM(i.RepairedGeneralSum) as RepairedGeneralSum
            ,null as RepairedPercent
            ,null as RepairedPercentSum
            ,SUM(i.UnrepairedGeneralCount) as UnrepairedGeneralCount
            ,SUM(i.UnrepairedGeneralSum) as UnrepairedGeneralSum
      FROM REPORT_LOADING_INSPECTION i
      WHERE i.TASK_ID = pTaskId AND i.AGG_ROW = 0
      GROUP BY i.FEDERALCODE;

      -- Создание агрегирующих строк (ИТОГО, все федеральные округа, все регионы, все инспекции)
      INSERT INTO REPORT_LOADING_INSPECTION
      SELECT NDS2$REPORTS.F$GET_LOAD_INSP_NEXTVAL
            ,pTaskId
            ,pActualNumActive
            ,1 as AGG_ROW
            ,'*' as FederalCode
            ,'ИТОГО' as FederalDistrict
            ,'*' as RegionCode
            ,'*' as Region
            ,'*' as InspectionCode
            ,'*' as Inspection
            ,SUM(i.InspectorCount) as InspectorCount
            ,SUM(i.GeneralCount) as GeneralCount
            ,SUM(i.GeneralSum) as GeneralSum
            ,SUM(i.SentToWorkGeneralCount) as SentToWorkGeneralCount
            ,SUM(i.SentToWorkGeneralSum) as SentToWorkGeneralSum
            ,null as SentToWorkPercent
            ,null as SentToWorkPercentSum
            ,SUM(i.RepairedGeneralCount) as RepairedGeneralCount
            ,SUM(i.RepairedGeneralSum) as RepairedGeneralSum
            ,null as RepairedPercent
            ,null as RepairedPercentSum
            ,SUM(i.UnrepairedGeneralCount) as UnrepairedGeneralCount
            ,SUM(i.UnrepairedGeneralSum) as UnrepairedGeneralSum
      FROM REPORT_LOADING_INSPECTION i
      WHERE i.TASK_ID = pTaskId AND i.AGG_ROW = 0;

      UPDATE REPORT_TASK SET TASK_STATUS = 1, COMPLETE_DATE = SYSDATE WHERE ID = pTaskId;
      commit;
  end;
  end if;
end;


-- Создание отчета по проверкам КС
procedure P$CREATE_CHECK_CONTROL_RATIO
(
  pTaskId IN NUMBER
)
as
  pTaskStatus NUMBER := -1;
  pFiscalYear NUMBER(5);
  pActualNumActive Number(1);
begin
  pActualNumActive := 1;

  SELECT task_status into pTaskStatus FROM dual
  left outer join (SELECT task_status FROM REPORT_TASK WHERE ID = pTaskId) r on 1 = 1;

  if pTaskStatus = 0 then
  begin
      DELETE FROM REPORT_CHECK_CONTROL_RATIO WHERE TASK_ID = pTaskId;

      -- получение одиночных параметров поиска
      SELECT PARAM_VALUE into pFiscalYear FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'FISCAL_YEAR';

      INSERT INTO REPORT_CHECK_CONTROL_RATIO (ID, TASK_ID, ACTUAL_NUM, AGG_ROW, NUM, DESCRIPTION, CountDiscrepancy)
      SELECT
        NDS2$REPORTS.F$GET_CHECK_CR_NEXTVAL
        ,pTaskId
        ,pActualNumActive
        ,0 as AGG_ROW
        ,m.typecode as num
        ,crt.description as Description
        ,m.CountKS as CountDiscrepancy
      FROM
      (
        SELECT
            cr.typecode,
			      count(cr.id) as CountKS
        FROM V$CONTROL_RATIO cr
        JOIN V$DECLARATION vd on vd.DECLARATION_VERSION_ID = cr.Decl_Version_Id
        WHERE vd.SOUN_CODE in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'SOUN_CODE')
              AND vd.REGION_CODE in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'REGION_CODE')
              AND vd.TAX_PERIOD in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD')
              AND vd.FISCAL_YEAR = pFiscalYear
              AND cr.HASDISCREPANCY = 1
		          and cr.typecode is not null
        group by cr.typecode
      ) m
      left outer join CONTROL_RATIO_TYPE crt on crt.code = m.typecode;

      -- Создание агрегирующих строк (ИТОГО)
      INSERT INTO REPORT_CHECK_CONTROL_RATIO
      SELECT NDS2$REPORTS.F$GET_CHECK_CR_NEXTVAL
            ,pTaskId
            ,pActualNumActive
            ,1 as AGG_ROW
            ,'ИТОГО' as num
            ,'' as Description
            ,SUM(i.CountDiscrepancy) as CountDiscrepancy
      FROM REPORT_CHECK_CONTROL_RATIO i
      WHERE i.TASK_ID = pTaskId AND i.AGG_ROW = 0;

      UPDATE REPORT_TASK SET TASK_STATUS = 1, COMPLETE_DATE = SYSDATE WHERE ID = pTaskId;
      commit;
  end;
  end if;
end;




-- Создание отчета по правилам сопоставлений
procedure P$CREATE_MATCHING_RULE
(
  pTaskId IN NUMBER
)
as
  pTaskStatus NUMBER := -1;
  pFiscalYear NUMBER(5);
  pDateCurrent DATE;
  pAggrNO NUMBER(1);  

  CURSOR reports IS
  SELECT * FROM REPORT_MATCHING_RULE WHERE TASK_ID = pTaskId;
  v_reports reports%ROWTYPE;

  pActualNumActive Number(1);
  pMatchCount NUMBER(19);
  pMatchAmount NUMBER(19,2);
  pPercentByCount NUMBER(3);
  pPercentByAmount NUMBER(3);
  pCountInnerErrors NUMBER(19);
  pCountOuterErrors NUMBER(19);
  pCountInnerOuterErrors NUMBER(19);
  pPercentByInnerErrors NUMBER(5);
  pPercentByOuterErrors NUMBER(5);
  pPercentByInnerOuterErrors NUMBER(5);
  pPercentInnerErrITOGO NUMBER(5);
  pPercentOuterErrITOGO NUMBER(5);
  pPercentInnerOuterErrITOGO NUMBER(5);    
begin
  pActualNumActive := 1;

  SELECT task_status into pTaskStatus FROM dual
  left outer join (SELECT task_status FROM REPORT_TASK WHERE ID = pTaskId) r on 1 = 1;

  if pTaskStatus = 0 then
  begin
      DELETE FROM REPORT_MATCHING_RULE WHERE TASK_ID = pTaskId;
      
      -- получение одиночных параметров поиска
      SELECT PARAM_VALUE into pAggrNO FROM dual
         left outer join (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'AGGREGATE_BY_NALOG_ORGAN') r on 1 = 1;
      SELECT PARAM_VALUE into pFiscalYear FROM dual
         left outer join (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'FISCAL_YEAR') r on 1 = 1;
      SELECT PARAM_VALUE into pDateCurrent FROM dual
         left outer join (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'DATE_ONE_DAY') r on 1 = 1;

      -- создание основных строк
      INSERT INTO REPORT_MATCHING_RULE (ID, TASK_ID, ACTUAL_NUM, AGG_ROW, Rule_Group_Caption, Rule_Group_number, Rule_Number_Caption, 
             Rule_Number, Rule_Number_Comment, MatchCount, MatchAmount, count_inner_errors, count_outer_errors, count_innerouter_errors)
      with t as
      (
          SELECT sm.* 
          from
          (
             select sm.*, SUBSTR(sm.inspection_code, 1, 2) as region_code_r
             from SOV_MATCH_RULE_SUMMARY sm
          ) sm          
          JOIN FEDERAL_DISTRICT_REGION fdr on fdr.region_code = sm.region_code_r
          JOIN FEDERAL_DISTRICT fd on fd.district_id = fdr.district_id      
          WHERE (case
                    when pAggrNO = 1 then 1
                    when pAggrNO = 2 and (fd.district_id in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'FEDERAL_CODE')) then 1
                    when pAggrNO = 3 and(sm.region_code_r in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'REGION_CODE')) then 1
                    when pAggrNO = 4 and (sm.inspection_code in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'SOUN_CODE')) then 1
                    else 0 end) = 1
                AND trunc(sm.process_date) = trunc(pDateCurrent)                
                --AND sm.fiscal_period in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD')
                --AND sm.fiscal_year = pFiscalYear
      )
      SELECT NDS2$REPORTS.F$GET_MATCHING_RULE_NEXTVAL
            ,pTaskId
            ,pActualNumActive
            ,0 as AGG_ROW
            ,to_char(m.rule_group_number) || ' группа' as Rule_Group_Caption
            ,m.rule_group_number
            ,to_char(m.rule_number) || ' правило' as Rule_Number_Caption
            ,m.rule_number
            ,dr.description as Rule_Number_Comment
            ,m.MatchCount
            ,m.MatchAmount
            ,m.count_buyer_only as count_inner_errors
            ,m.count_seller_only as count_outer_errors
            ,m.count_buyer_seller as count_innerouter_errors
      FROM
      (
          SELECT t.rule_group_number
                 ,t.rule_number 
                 ,SUM(count_lk_errors_buyer_only) as count_buyer_only
                 ,SUM(count_lk_errors_seller_only) as count_seller_only
                 ,SUM(count_lk_errors_buyer_seller) as count_buyer_seller
                 ,SUM(t.count_lk_errors_buyer_seller + t.count_lk_errors_buyer_only + t.count_lk_errors_seller_only + t.count_no_lk_errors)
                    as MatchCount
                 ,SUM(t.vat_lk_errors_buyer_seller + t.vat_lk_errors_buyer_only + t.vat_lk_errors_seller_only + t.vat_no_lk_errors)
                    as MatchAmount
          FROM t 
          group by t.rule_group_number, t.rule_number
      ) m
      left outer join DICT_COMPARE_RULES dr on dr.group_code = m.rule_group_number and dr.code = rule_number;

      -- подсчет общего количества сопоставлений, сумм сопоставлений, 
      -- ошибок среди входящих, исходящих, входящих вместе с исходящими
      pMatchCount := 0;
      pMatchAmount := 0;
      pCountInnerErrors := 0;
      pCountOuterErrors := 0;
      pCountInnerOuterErrors := 0;
      SELECT s.MatchCount, s.MatchAmount, s.count_inner_errors, s.count_outer_errors, s.count_innerouter_errors 
        into pMatchCount, pMatchAmount, pCountInnerErrors, pCountOuterErrors, pCountInnerOuterErrors from dual
      left outer join
      (
           SELECT  SUM(i.MatchCount) as MatchCount
                  ,SUM(i.MatchAmount) as MatchAmount
                  ,SUM(i.count_inner_errors) as count_inner_errors
                  ,SUM(i.count_outer_errors) as count_outer_errors                  
                  ,SUM(i.count_innerouter_errors) as count_innerouter_errors
           FROM REPORT_MATCHING_RULE i
           WHERE i.TASK_ID = pTaskId AND i.AGG_ROW = 0
      ) s on 1 = 1;
     
      pPercentInnerErrITOGO := 0; pPercentOuterErrITOGO := 0; pPercentInnerOuterErrITOGO := 0;
      if (pMatchCount > 0) then
          pPercentInnerErrITOGO := (pCountInnerErrors * 100) / pMatchCount;
          pPercentOuterErrITOGO := (pCountOuterErrors * 100) / pMatchCount;
          pPercentInnerOuterErrITOGO := (pCountInnerOuterErrors * 100) / pMatchCount;
      end if;
      
      -- Наполнение колонок(кол-во, сумма) всех уникальных строк
      OPEN reports;
      FETCH reports INTO v_reports;
      LOOP
          -- подсчет процента кол-ва сопоставлений относительно ИТОГО
          pPercentByCount := 0; pPercentByAmount := 0;
          if (pMatchCount > 0) then
            pPercentByCount := (v_reports.MatchCount * 100) / pMatchCount;
          end if;
          if (pMatchAmount > 0) then
            pPercentByAmount := (v_reports.MatchAmount * 100) / pMatchAmount;
          end if;

          pPercentByInnerErrors := 0; pPercentByOuterErrors := 0; pPercentByInnerOuterErrors := 0;
          if (v_reports.MatchCount > 0) then
            pPercentByInnerErrors := (v_reports.count_inner_errors * 100) / v_reports.MatchCount;
            pPercentByOuterErrors := (v_reports.count_outer_errors * 100) / v_reports.MatchCount;
            pPercentByInnerOuterErrors := (v_reports.count_innerouter_errors * 100) / v_reports.MatchCount;
          end if;

          -- Заполнение колонок
          UPDATE REPORT_MATCHING_RULE r SET PercentByCount = pPercentByCount, PercentByAmount = pPercentByAmount, 
                 PercentInnerInvoiceErrorLC = pPercentByInnerErrors, PercentOuterInvoiceErrorLC = pPercentByOuterErrors,
                 PercentInnerOuterInvErrorLC = pPercentByInnerOuterErrors
          WHERE r.ID = v_reports.ID;

          FETCH reports INTO v_reports;
          EXIT WHEN reports%NOTFOUND;
      END LOOP;

      -- Создание агрегирующих строк (все правила одной группы)
      INSERT INTO REPORT_MATCHING_RULE (ID, TASK_ID, ACTUAL_NUM, AGG_ROW, Rule_Group_Caption, Rule_Group_number, Rule_Number_Caption, 
             Rule_Number, Rule_Number_Comment, MatchCount, MatchAmount, PercentByCount, PercentByAmount,
             PercentInnerInvoiceErrorLC, PercentOuterInvoiceErrorLC, PercentInnerOuterInvErrorLC)
      SELECT NDS2$REPORTS.F$GET_MATCHING_RULE_NEXTVAL
            ,pTaskId
            ,pActualNumActive
            ,1 as AGG_ROW
            ,max(rule_group_caption) as Rule_Group_Caption
            ,rule_group_number as Rule_Group_number
            ,'*' as Rule_Number_Caption
            ,0 as Rule_Number
            ,'' as Rule_Number_Comment
            ,SUM(MatchCount) as MatchCount
            ,SUM(MatchAmount) as MatchAmount
            ,((SUM(MatchCount) * 100) / pMatchCount) as PercentByCount
            ,((SUM(MatchAmount) * 100) / pMatchAmount) as PercentByAmount
            ,((SUM(count_inner_errors) * 100) / SUM(MatchCount)) as PercentInnerInvoiceErrorLC
            ,((SUM(count_outer_errors) * 100) / SUM(MatchCount)) as PercentOuterInvoiceErrorLC
            ,((SUM(count_innerouter_errors) * 100) / SUM(MatchCount)) as PercentInnerOuterInvErrorLC
      FROM REPORT_MATCHING_RULE i
      WHERE i.TASK_ID = pTaskId AND i.AGG_ROW = 0
      GROUP BY i.rule_group_number;

      -- Создание агрегирующих строк (ИТОГО)
      INSERT INTO REPORT_MATCHING_RULE (ID, TASK_ID, ACTUAL_NUM, AGG_ROW, Rule_Group_Caption, Rule_Group_number, Rule_Number_Caption, 
             Rule_Number, Rule_Number_Comment, MatchCount, MatchAmount, PercentByCount, PercentByAmount,
             PercentInnerInvoiceErrorLC, PercentOuterInvoiceErrorLC, PercentInnerOuterInvErrorLC)
      SELECT NDS2$REPORTS.F$GET_MATCHING_RULE_NEXTVAL
            ,pTaskId
            ,pActualNumActive
            ,1 as AGG_ROW
            ,'ИТОГО' as Rule_Group_Caption
            ,0 as Rule_Group_number
            ,'' as Rule_Number_Caption
            ,null as Rule_Number
            ,'' as Rule_Number_Comment
            ,pMatchCount as MatchDiscrepancy
            ,pMatchAmount as MatchAmount
            ,100
            ,100
            ,pPercentInnerErrITOGO as PercentInnerInvoiceErrorLC
            ,pPercentOuterErrITOGO as PercentOuterInvoiceErrorLC
            ,pPercentInnerOuterErrITOGO as PercentInnerOuterInvErrorLC
      FROM DUAL;

      UPDATE REPORT_TASK SET TASK_STATUS = 1, COMPLETE_DATE = SYSDATE WHERE ID = pTaskId;
      commit;
  end;
  end if;
end;



-- Создание отчета по проверкам ЛК
procedure P$CREATE_CHECK_LOGIC_CONTROL
(
  pTaskId IN NUMBER,
  pRequestId IN NUMBER
)
as
  pTaskStatus NUMBER := -1;
  pActualNumActive Number(1);
  pSummaryErrorCount Number;
  pSummaryBadMismatch Number;
  pSummaryBreakMismatch Number;
  pSummaryExactMismatch Number;
  pSummaryInnerInvoice Number;
  pSummaryOuterInvoice Number;
begin
  pActualNumActive := 1;

  SELECT task_status into pTaskStatus FROM dual
  left outer join (SELECT task_status FROM REPORT_TASK WHERE ID = pTaskId) r on 1 = 1;

  if pTaskStatus = 0 then
  begin
      DELETE FROM REPORT_CHECK_LOGIC_CONTROL WHERE TASK_ID = pTaskId;

      -- Подсчет различных агрегирующих сумм 
      SELECT s.SummaryErrorCount, s.SummaryBadMismatch, s.SummaryBreakMismatch, s.SummaryExactMismatch, s.SummaryInnerInvoice, s.SummaryOuterInvoice 
             into pSummaryErrorCount, pSummaryBadMismatch, pSummaryBreakMismatch, pSummaryExactMismatch, pSummaryInnerInvoice, pSummaryOuterInvoice from dual
      left outer join
      (
           SELECT SUM(i.cnt) as SummaryErrorCount
                  ,SUM(i.cnt_bad_mismatch) as SummaryBadMismatch
                  ,SUM(i.cnt_break_mismatch) as SummaryBreakMismatch
                  ,SUM(i.cnt - i.cnt_bad_mismatch - i.cnt_break_mismatch) as SummaryExactMismatch
                  ,SUM(i.cnt_in) as SummaryInnerInvoice
                  ,SUM(i.cnt_out) as SummaryOuterInvoice
           FROM SOV_LK_REPORT i
           WHERE request_id = pRequestId
      ) s on 1 = 1;
     
      INSERT INTO REPORT_CHECK_LOGIC_CONTROL (ID, TASK_ID, ACTUAL_NUM, AGG_ROW, Num, iNum, Name, ErrorCount, ErrorCountPercentage, ErrorFrequencyInvoiceNotExact,
             ErrorFrequencyInvoiceGap, ErrorFrequencyInvoiceExact, ErrorFrequencyInnerInvoice, ErrorFrequencyOuterInvoice)
      select NDS2$REPORTS.F$GET_CHECK_CR_NEXTVAL
             ,pTaskId
             ,pActualNumActive
             ,0 as AGG_ROW
             ,to_char(slr.error_code) as num
             ,slr.error_code as iNUM
             ,lk.Name_Check as name
             ,cnt as errorcount
             ,((cnt * 100) / pSummaryErrorCount) as ErrorCountPercentage
             ,((cnt_bad_mismatch * 100) / pSummaryErrorCount) as ErrorFrequencyInvoiceNotExact
             ,((cnt_break_mismatch * 100) / pSummaryErrorCount) as ErrorFrequencyInvoiceGap
             ,(((cnt - cnt_bad_mismatch - cnt_break_mismatch) * 100) / pSummaryErrorCount) as ErrorFrequencyInvoiceExact
             ,((cnt_in * 100) / pSummaryErrorCount) as ErrorFrequencyInnerInvoice
             ,((cnt_out * 100) / pSummaryErrorCount) as ErrorFrequencyOuterInvoice
      from SOV_LK_REPORT slr
	  left outer join dict_logical_errors lk on lk.error_code = slr.error_code
      WHERE request_id = pRequestId;

      -- Создание агрегирующих строк (ИТОГО)
      INSERT INTO REPORT_CHECK_LOGIC_CONTROL (ID, TASK_ID, ACTUAL_NUM, AGG_ROW, Num, iNum, Name, ErrorCount, ErrorCountPercentage, ErrorFrequencyInvoiceNotExact,
             ErrorFrequencyInvoiceGap, ErrorFrequencyInvoiceExact, ErrorFrequencyInnerInvoice, ErrorFrequencyOuterInvoice)
      SELECT NDS2$REPORTS.F$GET_CHECK_CR_NEXTVAL
            ,pTaskId
            ,pActualNumActive
            ,1 as AGG_ROW
            ,'ИТОГО' as num
            ,0 as iNUM
            ,'' as Name
            ,SUM(i.ErrorCount) as ErrorCount
            ,100 as ErrorCountPercentage
            ,((pSummaryBadMismatch * 100) / pSummaryErrorCount) as ErrorFrequencyInvoiceNotExact
            ,((pSummaryBreakMismatch * 100) / pSummaryErrorCount) as ErrorFrequencyInvoiceGap
            ,((pSummaryExactMismatch * 100) / pSummaryErrorCount) as ErrorFrequencyInvoiceExact
            ,((pSummaryInnerInvoice * 100) / pSummaryErrorCount) as ErrorFrequencyInnerInvoice
            ,((pSummaryOuterInvoice * 100) / pSummaryErrorCount) as ErrorFrequencyOuterInvoice
      FROM REPORT_CHECK_LOGIC_CONTROL i
      WHERE i.TASK_ID = pTaskId AND i.AGG_ROW = 0;

      UPDATE REPORT_TASK SET TASK_STATUS = 1, COMPLETE_DATE = SYSDATE WHERE ID = pTaskId;
      commit;
  end;
  end if;
end;

procedure P$REPORT_LK_REQUEST_STATUS (
  pRequestId in SOV_LK_REPORT_REQUEST.REQUEST_ID%type,
  pData out SYS_REFCURSOR
  )
as
begin

  open pData for select dict.* from REQUEST_STATUS dict join SOV_LK_REPORT_REQUEST req on req.status_id = dict.ID where req.request_id = pRequestId;

end;


-- Расчет статистических данных для отчета Фоновые показатели (По поданным декларациям)
procedure P$CALCULATE_DECLARATION_RAW
(
   pSubmitDate IN DATE
)
as
begin

  delete from REPORT_DECLARATION_RAW where trunc(Submit_Date) = trunc(pSubmitDate);

  insert into REPORT_DECLARATION_RAW (ID, Submit_Date, Region_Code, Soun_Code, Fiscal_Year, Tax_period, Decl_Count_Total,
         Decl_Count_Null, Decl_Count_Compensation, Decl_Count_Payment, Decl_Count_Not_Submit,
         Decl_Count_Revised_Total, TaxBase_Sum_Compensation, TaxBase_Sum_Payment, NdsCalculated_Sum_Compensation,
         NdsCalculated_Sum_Payment, NdsDeduction_Sum_Compensation, NdsDeduction_Sum_Payment,
         Nds_Sum_Compensation, Nds_Sum_Payment)
  with t as
  (
      select
       SD.ID as ID
      ,SD.ZIP as DECLARATION_VERSION_ID
      ,decode((row_number() over (partition by sd.PERIOD, sd.OTCHETGOD, sd.INNNP order by sd.NOMKORR desc)),1,1,0) as IS_ACTIVE
      ,decode((row_number() over (partition by sd.PERIOD, sd.OTCHETGOD, sd.INNNP order by sd.NOMKORR)),1,1,null,1,0) as IS_FIRST
      ,substr(sd.KODNO,1,2) as REGION_CODE
      ,sd.KODNO as SOUN_CODE
      ,sd.PERIOD as TAX_PERIOD
      ,sd.OTCHETGOD as FISCAL_YEAR
      ,nvl(sd.DATADOK, sysdate) as DECL_DATE
      ,sd.NOMKORR as CORRECTION_NUMBER
      ,(case when nvl(sd.SUMPU173_5, 0) = 0 and nvl(sd.SUMPU173_1, 0) = 0 and nvl(anyOf_8_9_12, 0) = 0 then 'Нулевая'
             when nvl(sd.SUMPU173_5, 0) = 0 and nvl(sd.SUMPU173_1, 0) = 0 and nvl(anyOf_8_9_12, 0) > 0 then 'К уплате'
             when nvl(sd.SUMPU173_5, 0) > 0 and nvl(sd.SUMPU173_1, 0) = 0 then 'К уплате'
             when nvl(sd.SUMPU173_5, 0) = 0 and nvl(sd.SUMPU173_1, 0) > 0 then 'К уплате'
             when nvl(sd.SUMPU173_5, 0) = 0 and nvl(sd.SUMPU173_1, 0) < 0 then 'К возмещению'
             when nvl(sd.SUMPU173_5, 0) > 0 and nvl(sd.SUMPU173_1, 0) > 0 then 'К уплате'
             when nvl(sd.SUMPU173_5, 0) > 0 and nvl(sd.SUMPU173_1, 0) < 0 and (nvl(sd.SUMPU173_5, 0) + nvl(sd.SUMPU173_1, 0)) >= 0 then 'К уплате'
             when nvl(sd.SUMPU173_5, 0) > 0 and nvl(sd.SUMPU173_1, 0) < 0 and (nvl(sd.SUMPU173_5, 0) + nvl(sd.SUMPU173_1, 0)) < 0 then 'К возмещению'
             when nvl(sd.SUMPU173_5, 0) < 0 and nvl(sd.SUMPU173_1, 0) < 0 then 'К возмещению'
             when nvl(sd.SUMPU173_5, 0) < 0 and nvl(sd.SUMPU173_1, 0) > 0 then 'К уплате'
             when nvl(sd.SUMPU173_5, 0) < 0 and nvl(sd.SUMPU173_1, 0) = 0 and nvl(anyOf_8_9_12, 0) = 0 then 'Нулевая'
             when nvl(sd.SUMPU173_5, 0) < 0 and nvl(sd.SUMPU173_1, 0) = 0 and nvl(anyOf_8_9_12, 0) > 0 then 'К уплате'
      end) as DECL_SIGN
      ,(case when nvl(sd.SUMPU173_5, 0) = 0 and nvl(sd.SUMPU173_1, 0) = 0 and nvl(anyOf_8_9_12, 0) = 0 then 0
             when nvl(sd.SUMPU173_5, 0) = 0 and nvl(sd.SUMPU173_1, 0) = 0 and nvl(anyOf_8_9_12, 0) > 0 then 0
             when nvl(sd.SUMPU173_5, 0) > 0 and nvl(sd.SUMPU173_1, 0) = 0 then nvl(sd.SUMPU173_5, 0)
             when nvl(sd.SUMPU173_5, 0) = 0 and nvl(sd.SUMPU173_1, 0) > 0 then nvl(sd.SUMPU173_1, 0)
             when nvl(sd.SUMPU173_5, 0) = 0 and nvl(sd.SUMPU173_1, 0) < 0 then nvl(sd.SUMPU173_1, 0)
             when nvl(sd.SUMPU173_5, 0) > 0 and nvl(sd.SUMPU173_1, 0) > 0 then (nvl(sd.SUMPU173_5, 0) + nvl(sd.SUMPU173_1, 0))
             when nvl(sd.SUMPU173_5, 0) > 0 and nvl(sd.SUMPU173_1, 0) < 0 and (nvl(sd.SUMPU173_5, 0) + nvl(sd.SUMPU173_1, 0)) >= 0 then  (nvl(sd.SUMPU173_5, 0) + nvl(sd.SUMPU173_1, 0))
             when nvl(sd.SUMPU173_5, 0) > 0 and nvl(sd.SUMPU173_1, 0) < 0 and (nvl(sd.SUMPU173_5, 0) + nvl(sd.SUMPU173_1, 0)) < 0 then (nvl(sd.SUMPU173_5, 0) + nvl(sd.SUMPU173_1, 0))
             when nvl(sd.SUMPU173_5, 0) < 0 and nvl(sd.SUMPU173_1, 0) < 0 then nvl(sd.SUMPU173_1, 0)
             when nvl(sd.SUMPU173_5, 0) < 0 and nvl(sd.SUMPU173_1, 0) > 0 then nvl(sd.SUMPU173_1, 0)
             when nvl(sd.SUMPU173_5, 0) < 0 and nvl(sd.SUMPU173_1, 0) = 0 and nvl(anyOf_8_9_12, 0) = 0 then 0
             when nvl(sd.SUMPU173_5, 0) < 0 and nvl(sd.SUMPU173_1, 0) = 0 and nvl(anyOf_8_9_12, 0) > 0 then 0
      else null end)                                as COMPENSATION_AMNT
      ,(nvl(sd.REALTOV18NALBAZA, 0) + nvl(sd.REALTOV10NALBAZA, 0) + nvl(sd.REALTOV118NALBAZA, 0) + nvl(sd.REALTOV110NALBAZA, 0) +
      nvl(sd.REALPREDIKNALBAZA, 0) + nvl(sd.VYPSMRSOBNALBAZA, 0) + nvl(sd.OPLPREDPOSTNALBAZA, 0) + nvl(sd.KORREALTOV18NALBAZA, 0) +
      nvl(sd.KORREALTOV10NALBAZA, 0) + nvl(sd.KORREALTOV118NALBAZA, 0) + nvl(sd.KORREALTOV110NALBAZA, 0) +
      nvl(sd.KORREALPREDIKNALBAZA, 0)) as TAX_BASE_AMNT
      ,sd.NALVYCHOBSHH as TAX_DEDUCTION_AMNT
      ,sd.NALVOSSTOBSHH as TAX_CALCULATED_AMNT
      from
      (
          select d.*, (nvl(d.PRIZNNAL8, 0) + nvl(d.PRIZNNAL81, 0) + nvl(d.PRIZNNAL9, 0) + nvl(d.PRIZNNAL91, 0) + nvl(d.PRIZNNAL12, 0)) as anyOf_8_9_12
          from V$ASKDEKL d
      ) sd 
  )
  select
         m.ID
        ,m.Submit_Date
        ,m.Region_Code
        ,m.Soun_Code
        ,m.Fiscal_Year
        ,m.Tax_period
        ,m.Decl_Count_Total
        ,m.Decl_Count_Null
        ,m.Decl_Count_Compensation
        ,m.Decl_Count_Payment
        ,m.Decl_Count_Not_Submit
        ,cor.countTotal as Decl_Count_Revised_Total --Подано уточненных деклараций
        ,m.TaxBase_Sum_Compensation
        ,m.TaxBase_Sum_Payment
        ,m.NdsCalculated_Sum_Compensation
        ,m.NdsCalculated_Sum_Payment
        ,m.NdsDeduction_Sum_Compensation
        ,m.NdsDeduction_Sum_Payment
        ,m.Nds_Sum_Compensation
        ,m.Nds_Sum_Payment
  from
  (
    select
      F$GET_DECLARATION_RAW_NEXTVAL as ID
      ,pSubmitDate as Submit_Date
      ,vd.REGION_CODE as Region_Code
      ,vd.SOUN_CODE as Soun_Code
      ,vd.FISCAL_YEAR as Fiscal_Year
      ,vd.TAX_PERIOD as Tax_period
      --Подано деклараций к уплате
      ,count(case when decl_sign = 'К уплате' then vd.ID end) as Decl_Count_Payment
      --Подано деклараций к возмещению
      ,count(case when decl_sign = 'К возмещению' then vd.ID end) as Decl_Count_Compensation
      --Подано нулевых деклараций
      ,count(case when decl_sign = 'Нулевая' then vd.ID end) as Decl_Count_Null
      --Не подано деклараций НП по которым есть упоминания в декларациях контрагентов(Нулевая декларация)
      ,0 as Decl_Count_Not_Submit
      --Всего подано деклараций
      ,count(vd.ID) as Decl_Count_Total
      --Налоговая база в декларациях к уплате
      ,sum(case when decl_sign = 'К уплате' then nvl(TAX_BASE_AMNT, 0) else 0 end) as TaxBase_Sum_Payment
      --Налоговая база в декларациях к возмещению
      ,sum(case when decl_sign = 'К возмещению' then nvl(TAX_BASE_AMNT, 0) else 0 end) as TaxBase_Sum_Compensation
      --Сумма исчисленного НДС в декларациях к уплате
      ,sum(case when decl_sign = 'К уплате' then nvl(TAX_CALCULATED_AMNT, 0) else 0 end) as NdsCalculated_Sum_Payment
      --Сумма исчисленного НДС в декларациях к возмещению
      ,sum(case when decl_sign = 'К возмещению' then nvl(TAX_CALCULATED_AMNT, 0) else 0 end) as NdsCalculated_Sum_Compensation
      --Сумма вычетов по НДС в декларациях к уплате
      ,sum(case when decl_sign = 'К уплате' then nvl(TAX_DEDUCTION_AMNT, 0) else 0 end) as NdsDeduction_Sum_Payment
      --Сумма вычетов по НДС в декларациях к возмещению
      ,sum(case when decl_sign = 'К возмещению' then nvl(TAX_DEDUCTION_AMNT, 0) else 0 end) as NdsDeduction_Sum_Compensation
      --Сумма НДС к уплате
      ,sum(case when decl_sign = 'К уплате' then nvl(COMPENSATION_AMNT, 0) else 0 end) as Nds_Sum_Payment
      --Сумма НДС к возмещению
      ,sum(case when decl_sign = 'К возмещению' then nvl(COMPENSATION_AMNT, 0) else 0 end) as Nds_Sum_Compensation
    from t vd
    where vd.IS_ACTIVE = 1 
    group by vd.REGION_CODE, vd.SOUN_CODE, vd.FISCAL_YEAR, vd.TAX_PERIOD
  ) m
  left outer join
  (
    --Подано уточненных деклараций
    select count(*) as countTotal
      ,vd.REGION_CODE as Region_Code
      ,vd.SOUN_CODE as Soun_Code
      ,vd.FISCAL_YEAR as Fiscal_Year
      ,vd.TAX_PERIOD as Tax_period
    from t vd
    where vd.IS_FIRST = 0
    group by vd.REGION_CODE, vd.SOUN_CODE, vd.FISCAL_YEAR, vd.TAX_PERIOD
  ) cor on cor.REGION_CODE = m.REGION_CODE and cor.SOUN_CODE = m.SOUN_CODE
       and cor.FISCAL_YEAR = m.FISCAL_YEAR and cor.TAX_PERIOD = m.TAX_PERIOD
  order by m.REGION_CODE, m.SOUN_CODE, m.FISCAL_YEAR, m.TAX_PERIOD;

  commit;

end;


-- Создание отчета Фоновые показатели (По поданным декларациям)
procedure P$CREATE_DECLARATION
(
  pTaskId IN NUMBER
)
as
  pTaskStatus NUMBER := -1;
  pActualNumActive Number(1);
  pDeclStatisticType NUMBER(1);
  pDeclStatisticRegim NUMBER(1);
  pDateCurrent DATE;
  pDateBegin DATE;
  pDateEnd DATE;
  pFiscalYear NUMBER(5);
begin
  pActualNumActive := 1;

  SELECT task_status into pTaskStatus FROM dual
  left outer join (SELECT task_status FROM REPORT_TASK WHERE ID = pTaskId) r on 1 = 1;

  if pTaskStatus = 0 then
  begin
      DELETE FROM REPORT_DECLARATION WHERE TASK_ID = pTaskId;

      SELECT PARAM_VALUE into pFiscalYear FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'FISCAL_YEAR';

      SELECT PARAM_VALUE into pDeclStatisticType FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'DECL_STATISTIC_TYPE';
      SELECT PARAM_VALUE into pDeclStatisticRegim FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'DECL_STATISTIC_REGIM';
      SELECT PARAM_VALUE into pDateCurrent FROM dual
         left outer join (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'DATE_CREATE_REPORT') r on 1 = 1;
      SELECT PARAM_VALUE into pDateBegin FROM dual
         left outer join (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'DATE_BEGIN') r on 1 = 1;
      SELECT PARAM_VALUE into pDateEnd FROM dual
         left outer join (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'DATE_END') r on 1 = 1;

      --На конкретную дату
      if pDeclStatisticRegim = 1 then
      begin
        --По стране
        if pDeclStatisticType = 1 then
          insert into REPORT_DECLARATION (ID, TASK_ID, ACTUAL_NUM, AGG_ROW, Name_Group,Decl_Count_Total, Decl_Count_Null,
                Decl_Count_Compensation, Decl_Count_Payment, Decl_Count_Not_Submit, Decl_Count_Revised_Total, TaxBase_Sum_Compensation,
                TaxBase_Sum_Payment, NdsCalculated_Sum_Compensation, NdsCalculated_Sum_Payment, NdsDeduction_Sum_Compensation,
                NdsDeduction_Sum_Payment, Nds_Sum_Compensation, Nds_Sum_Payment)
          select NDS2$REPORTS.F$GET_DECLARATION_NEXTVAL
            ,pTaskId
            ,pActualNumActive
            ,0 as AGG_ROW
            ,'вся страна' as name_group
            ,sum(decl_count_total) as decl_count_total
            ,sum(decl_count_null) as decl_count_null
            ,sum(decl_count_compensation) as decl_count_compensation
            ,sum(decl_count_payment) as decl_count_payment
            ,sum(decl_count_not_submit) as decl_count_not_submit
            ,sum(decl_count_revised_total) as decl_count_revised_total
            ,sum(taxbase_sum_compensation) as taxbase_sum_compensation
            ,sum(taxbase_sum_payment) as taxbase_sum_payment
            ,sum(ndscalculated_sum_compensation) as ndscalculated_sum_compensation
            ,sum(ndscalculated_sum_payment) as ndscalculated_sum_payment
            ,sum(ndsdeduction_sum_compensation) as ndsdeduction_sum_compensation
            ,sum(ndsdeduction_sum_payment) as ndsdeduction_sum_payment
            ,sum(nds_sum_compensation) as nds_sum_compensation
            ,sum(nds_sum_payment) as nds_sum_payment
          from REPORT_DECLARATION_RAW r
          where trunc(r.submit_date) = trunc(pDateCurrent) and r.fiscal_year = pFiscalYear
                and r.tax_period in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD');
        end if;
        --По федеральным округам
        if pDeclStatisticType = 2 then
          insert into REPORT_DECLARATION (ID, TASK_ID, ACTUAL_NUM, AGG_ROW, Name_Group,Decl_Count_Total, Decl_Count_Null,
                Decl_Count_Compensation, Decl_Count_Payment, Decl_Count_Not_Submit, Decl_Count_Revised_Total, TaxBase_Sum_Compensation,
                TaxBase_Sum_Payment, NdsCalculated_Sum_Compensation, NdsCalculated_Sum_Payment, NdsDeduction_Sum_Compensation,
                NdsDeduction_Sum_Payment, Nds_Sum_Compensation, Nds_Sum_Payment)
          select NDS2$REPORTS.F$GET_DECLARATION_NEXTVAL
                 ,pTaskId
                 ,pActualNumActive
                 ,0 as AGG_ROW
                 ,fd.description as name_group
                 ,a.decl_count_total
                 ,a.decl_count_null
                 ,a.decl_count_compensation
                 ,a.decl_count_payment
                 ,a.decl_count_not_submit
                 ,a.decl_count_revised_total
                 ,a.taxbase_sum_compensation
                 ,a.taxbase_sum_payment
                 ,a.ndscalculated_sum_compensation
                 ,a.ndscalculated_sum_payment
                 ,a.ndsdeduction_sum_compensation
                 ,a.ndsdeduction_sum_payment
                 ,a.nds_sum_compensation
                 ,a.nds_sum_payment
          from
          (
            select fdr.district_id
            from REPORT_DECLARATION_RAW r
            join FEDERAL_DISTRICT_REGION fdr on fdr.region_code = r.region_code
            where trunc(r.submit_date) = trunc(pDateCurrent) and r.fiscal_year = pFiscalYear
                  and r.tax_period in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD')
            group by fdr.district_id
          ) m
          left outer join
          (
               select distinct r.district_id
                     ,SUM(r.decl_count_total) OVER (PARTITION BY r.district_id) decl_count_total
                     ,SUM(r.decl_count_null) OVER (PARTITION BY r.district_id) decl_count_null
                     ,SUM(r.decl_count_compensation) OVER (PARTITION BY r.district_id) decl_count_compensation
                     ,SUM(r.decl_count_payment) OVER (PARTITION BY r.district_id) decl_count_payment
                     ,SUM(r.decl_count_not_submit) OVER (PARTITION BY r.district_id) decl_count_not_submit
                     ,SUM(r.decl_count_revised_total) OVER (PARTITION BY r.district_id) decl_count_revised_total
                     ,SUM(r.taxbase_sum_compensation) OVER (PARTITION BY r.district_id) taxbase_sum_compensation
                     ,SUM(r.taxbase_sum_payment) OVER (PARTITION BY r.district_id) taxbase_sum_payment
                     ,SUM(r.ndscalculated_sum_compensation) OVER (PARTITION BY r.district_id) ndscalculated_sum_compensation
                     ,SUM(r.ndscalculated_sum_payment) OVER (PARTITION BY r.district_id) ndscalculated_sum_payment
                     ,SUM(r.ndsdeduction_sum_compensation) OVER (PARTITION BY r.district_id) ndsdeduction_sum_compensation
                     ,SUM(r.ndsdeduction_sum_payment) OVER (PARTITION BY r.district_id) ndsdeduction_sum_payment
                     ,SUM(r.nds_sum_compensation) OVER (PARTITION BY r.district_id) nds_sum_compensation
                     ,SUM(r.nds_sum_payment) OVER (PARTITION BY r.district_id) nds_sum_payment
               from
               (
                  select r.*, fd.district_id as district_id
                  from REPORT_DECLARATION_RAW r
                  join FEDERAL_DISTRICT_REGION fdr on fdr.region_code = r.region_code
                  join FEDERAL_DISTRICT fd on fd.district_id = fdr.district_id
                  where trunc(r.submit_date) = trunc(pDateCurrent) and r.fiscal_year = pFiscalYear
                        and r.tax_period in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD')
               )r
          ) a on a.district_id = m.district_id
          join FEDERAL_DISTRICT fd on fd.district_id = m.district_id;
        end if;
        --По регионам
        if pDeclStatisticType = 3 then
          insert into REPORT_DECLARATION (ID, TASK_ID, ACTUAL_NUM, AGG_ROW, Name_Group,Decl_Count_Total, Decl_Count_Null,
                Decl_Count_Compensation, Decl_Count_Payment, Decl_Count_Not_Submit, Decl_Count_Revised_Total, TaxBase_Sum_Compensation,
                TaxBase_Sum_Payment, NdsCalculated_Sum_Compensation, NdsCalculated_Sum_Payment, NdsDeduction_Sum_Compensation,
                NdsDeduction_Sum_Payment, Nds_Sum_Compensation, Nds_Sum_Payment)
          select NDS2$REPORTS.F$GET_DECLARATION_NEXTVAL
                 ,pTaskId
                 ,pActualNumActive
                 ,0 as AGG_ROW
                 ,s.s_code || '-' || s.s_name as name_group
                 ,a.decl_count_total
                 ,a.decl_count_null
                 ,a.decl_count_compensation
                 ,a.decl_count_payment
                 ,a.decl_count_not_submit
                 ,a.decl_count_revised_total
                 ,a.taxbase_sum_compensation
                 ,a.taxbase_sum_payment
                 ,a.ndscalculated_sum_compensation
                 ,a.ndscalculated_sum_payment
                 ,a.ndsdeduction_sum_compensation
                 ,a.ndsdeduction_sum_payment
                 ,a.nds_sum_compensation
                 ,a.nds_sum_payment
          from
          (
            select r.region_code
            from REPORT_DECLARATION_RAW r
            where trunc(r.submit_date) = trunc(pDateCurrent) and r.fiscal_year = pFiscalYear
                  and r.tax_period in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD')
            group by r.region_code
          ) m
          left outer join
          (
               select distinct r.region_code
                     ,SUM(r.decl_count_total) OVER (PARTITION BY r.region_code) decl_count_total
                     ,SUM(r.decl_count_null) OVER (PARTITION BY r.region_code) decl_count_null
                     ,SUM(r.decl_count_compensation) OVER (PARTITION BY r.region_code) decl_count_compensation
                     ,SUM(r.decl_count_payment) OVER (PARTITION BY r.region_code) decl_count_payment
                     ,SUM(r.decl_count_not_submit) OVER (PARTITION BY r.region_code) decl_count_not_submit
                     ,SUM(r.decl_count_revised_total) OVER (PARTITION BY r.region_code) decl_count_revised_total
                     ,SUM(r.taxbase_sum_compensation) OVER (PARTITION BY r.region_code) taxbase_sum_compensation
                     ,SUM(r.taxbase_sum_payment) OVER (PARTITION BY r.region_code) taxbase_sum_payment
                     ,SUM(r.ndscalculated_sum_compensation) OVER (PARTITION BY r.region_code) ndscalculated_sum_compensation
                     ,SUM(r.ndscalculated_sum_payment) OVER (PARTITION BY r.region_code) ndscalculated_sum_payment
                     ,SUM(r.ndsdeduction_sum_compensation) OVER (PARTITION BY r.region_code) ndsdeduction_sum_compensation
                     ,SUM(r.ndsdeduction_sum_payment) OVER (PARTITION BY r.region_code) ndsdeduction_sum_payment
                     ,SUM(r.nds_sum_compensation) OVER (PARTITION BY r.region_code) nds_sum_compensation
                     ,SUM(r.nds_sum_payment) OVER (PARTITION BY r.region_code) nds_sum_payment
               from REPORT_DECLARATION_RAW r
               where trunc(r.submit_date) = trunc(pDateCurrent) and r.fiscal_year = pFiscalYear
                     and r.tax_period in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD')
          ) a on a.region_code = m.region_code
          join V$SSRF s on s.s_code = m.region_code;
        end if;
        --По инспекциям
        if pDeclStatisticType = 4 then
          insert into REPORT_DECLARATION (ID, TASK_ID, ACTUAL_NUM, AGG_ROW, Name_Group,Decl_Count_Total, Decl_Count_Null,
                Decl_Count_Compensation, Decl_Count_Payment, Decl_Count_Not_Submit, Decl_Count_Revised_Total, TaxBase_Sum_Compensation,
                TaxBase_Sum_Payment, NdsCalculated_Sum_Compensation, NdsCalculated_Sum_Payment, NdsDeduction_Sum_Compensation,
                NdsDeduction_Sum_Payment, Nds_Sum_Compensation, Nds_Sum_Payment)
          select NDS2$REPORTS.F$GET_DECLARATION_NEXTVAL
                 ,pTaskId
                 ,pActualNumActive
                 ,0 as AGG_ROW
                 ,s.s_code || '-' || s.s_name
                 ,a.decl_count_total
                 ,a.decl_count_null
                 ,a.decl_count_compensation
                 ,a.decl_count_payment
                 ,a.decl_count_not_submit
                 ,a.decl_count_revised_total
                 ,a.taxbase_sum_compensation
                 ,a.taxbase_sum_payment
                 ,a.ndscalculated_sum_compensation
                 ,a.ndscalculated_sum_payment
                 ,a.ndsdeduction_sum_compensation
                 ,a.ndsdeduction_sum_payment
                 ,a.nds_sum_compensation
                 ,a.nds_sum_payment
          from
          (
            select r.soun_code
            from REPORT_DECLARATION_RAW r
            where r.region_code in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'REGION_CODE')
                  and trunc(r.submit_date) = trunc(pDateCurrent) and r.fiscal_year = pFiscalYear
                  and r.tax_period in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD')
            group by r.soun_code
          ) m
          left outer join
          (
               select distinct r.soun_code
                     ,SUM(r.decl_count_total) OVER (PARTITION BY r.soun_code) decl_count_total
                     ,SUM(r.decl_count_null) OVER (PARTITION BY r.soun_code) decl_count_null
                     ,SUM(r.decl_count_compensation) OVER (PARTITION BY r.soun_code) decl_count_compensation
                     ,SUM(r.decl_count_payment) OVER (PARTITION BY r.soun_code) decl_count_payment
                     ,SUM(r.decl_count_not_submit) OVER (PARTITION BY r.soun_code) decl_count_not_submit
                     ,SUM(r.decl_count_revised_total) OVER (PARTITION BY r.soun_code) decl_count_revised_total
                     ,SUM(r.taxbase_sum_compensation) OVER (PARTITION BY r.soun_code) taxbase_sum_compensation
                     ,SUM(r.taxbase_sum_payment) OVER (PARTITION BY r.soun_code) taxbase_sum_payment
                     ,SUM(r.ndscalculated_sum_compensation) OVER (PARTITION BY r.soun_code) ndscalculated_sum_compensation
                     ,SUM(r.ndscalculated_sum_payment) OVER (PARTITION BY r.soun_code) ndscalculated_sum_payment
                     ,SUM(r.ndsdeduction_sum_compensation) OVER (PARTITION BY r.soun_code) ndsdeduction_sum_compensation
                     ,SUM(r.ndsdeduction_sum_payment) OVER (PARTITION BY r.soun_code) ndsdeduction_sum_payment
                     ,SUM(r.nds_sum_compensation) OVER (PARTITION BY r.soun_code) nds_sum_compensation
                     ,SUM(r.nds_sum_payment) OVER (PARTITION BY r.soun_code) nds_sum_payment
               from REPORT_DECLARATION_RAW r
               where r.region_code in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'REGION_CODE')
                     and trunc(r.submit_date) = trunc(pDateCurrent) and r.fiscal_year = pFiscalYear
                     and r.tax_period in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD')
          ) a on a.soun_code = m.soun_code
          join V$SONO s on s.s_code = m.soun_code;
        end if;
      end;
      end if;

      --Разница между двумя датами
      if pDeclStatisticRegim = 2 then
      begin
          --По стране
          if pDeclStatisticType = 1 then
            insert into REPORT_DECLARATION (ID, TASK_ID, ACTUAL_NUM, AGG_ROW, Name_Group,Decl_Count_Total, Decl_Count_Null,
                  Decl_Count_Compensation, Decl_Count_Payment, Decl_Count_Not_Submit, Decl_Count_Revised_Total, TaxBase_Sum_Compensation,
                  TaxBase_Sum_Payment, NdsCalculated_Sum_Compensation, NdsCalculated_Sum_Payment, NdsDeduction_Sum_Compensation,
                  NdsDeduction_Sum_Payment, Nds_Sum_Compensation, Nds_Sum_Payment)
            select NDS2$REPORTS.F$GET_DECLARATION_NEXTVAL
              ,pTaskId
              ,pActualNumActive
              ,0 as AGG_ROW
              ,'вся страна' as name_group
              ,nvl(b.decl_count_total, 0) - nvl(a.decl_count_total, 0) as decl_count_total
              ,nvl(b.decl_count_null, 0) - nvl(a.decl_count_null, 0) as decl_count_null
              ,nvl(b.decl_count_compensation, 0) - nvl(a.decl_count_compensation, 0) as decl_count_compensation
              ,nvl(b.decl_count_payment, 0) - nvl(a.decl_count_payment, 0) as decl_count_payment
              ,nvl(b.decl_count_not_submit, 0) - nvl(a.decl_count_not_submit, 0) as decl_count_not_submit
              ,nvl(b.decl_count_revised_total, 0) - nvl(a.decl_count_revised_total, 0) as decl_count_revised_total
              ,nvl(b.taxbase_sum_compensation, 0) - nvl(a.taxbase_sum_compensation, 0) as taxbase_sum_compensation
              ,nvl(b.taxbase_sum_payment, 0) - nvl(a.taxbase_sum_payment, 0) as taxbase_sum_payment
              ,nvl(b.ndscalculated_sum_compensation, 0) - nvl(a.ndscalculated_sum_compensation, 0) as ndscalculated_sum_compensation
              ,nvl(b.ndscalculated_sum_payment, 0) - nvl(a.ndscalculated_sum_payment, 0) as ndscalculated_sum_payment
              ,nvl(b.ndsdeduction_sum_compensation, 0) - nvl(a.ndsdeduction_sum_compensation, 0) as ndsdeduction_sum_compensation
              ,nvl(b.ndsdeduction_sum_payment, 0) - nvl(a.ndsdeduction_sum_payment, 0) as ndsdeduction_sum_payment
              ,nvl(b.nds_sum_compensation, 0) - nvl(a.nds_sum_compensation, 0) as nds_sum_compensation
              ,nvl(b.nds_sum_payment, 0) - nvl(a.nds_sum_payment, 0) as nds_sum_payment
            from dual
            left outer join
            (
                 select
                   sum(decl_count_total) as decl_count_total
                  ,sum(decl_count_null) as decl_count_null
                  ,sum(decl_count_compensation) as decl_count_compensation
                  ,sum(decl_count_payment) as decl_count_payment
                  ,sum(decl_count_not_submit) as decl_count_not_submit
                  ,sum(decl_count_revised_total) as decl_count_revised_total
                  ,sum(taxbase_sum_compensation) as taxbase_sum_compensation
                  ,sum(taxbase_sum_payment) as taxbase_sum_payment
                  ,sum(ndscalculated_sum_compensation) as ndscalculated_sum_compensation
                  ,sum(ndscalculated_sum_payment) as ndscalculated_sum_payment
                  ,sum(ndsdeduction_sum_compensation) as ndsdeduction_sum_compensation
                  ,sum(ndsdeduction_sum_payment) as ndsdeduction_sum_payment
                  ,sum(nds_sum_compensation) as nds_sum_compensation
                  ,sum(nds_sum_payment) as nds_sum_payment
                from REPORT_DECLARATION_RAW r
                where trunc(r.submit_date) = trunc(pDateBegin) and r.fiscal_year = pFiscalYear
                      and r.tax_period in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD')
            ) a on 1 = 1
            left outer join
            (
                 select
                   sum(decl_count_total) as decl_count_total
                  ,sum(decl_count_null) as decl_count_null
                  ,sum(decl_count_compensation) as decl_count_compensation
                  ,sum(decl_count_payment) as decl_count_payment
                  ,sum(decl_count_not_submit) as decl_count_not_submit
                  ,sum(decl_count_revised_total) as decl_count_revised_total
                  ,sum(taxbase_sum_compensation) as taxbase_sum_compensation
                  ,sum(taxbase_sum_payment) as taxbase_sum_payment
                  ,sum(ndscalculated_sum_compensation) as ndscalculated_sum_compensation
                  ,sum(ndscalculated_sum_payment) as ndscalculated_sum_payment
                  ,sum(ndsdeduction_sum_compensation) as ndsdeduction_sum_compensation
                  ,sum(ndsdeduction_sum_payment) as ndsdeduction_sum_payment
                  ,sum(nds_sum_compensation) as nds_sum_compensation
                  ,sum(nds_sum_payment) as nds_sum_payment
                from REPORT_DECLARATION_RAW r
                where trunc(r.submit_date) = trunc(pDateEnd) and r.fiscal_year = pFiscalYear
                      and r.tax_period in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD')
            ) b on 1 = 1;            
          end if;

          --По федеральным округам
          if pDeclStatisticType = 2 then
            insert into REPORT_DECLARATION (ID, TASK_ID, ACTUAL_NUM, AGG_ROW, Name_Group,Decl_Count_Total, Decl_Count_Null,
                  Decl_Count_Compensation, Decl_Count_Payment, Decl_Count_Not_Submit, Decl_Count_Revised_Total, TaxBase_Sum_Compensation,
                  TaxBase_Sum_Payment, NdsCalculated_Sum_Compensation, NdsCalculated_Sum_Payment, NdsDeduction_Sum_Compensation,
                  NdsDeduction_Sum_Payment, Nds_Sum_Compensation, Nds_Sum_Payment)
            select NDS2$REPORTS.F$GET_DECLARATION_NEXTVAL
                   ,pTaskId
                   ,pActualNumActive
                   ,0 as AGG_ROW
                   ,fd.description as name_group
                   ,nvl(b.decl_count_total, 0) - nvl(a.decl_count_total, 0)
                   ,nvl(b.decl_count_null, 0) - nvl(a.decl_count_null, 0)
                   ,nvl(b.decl_count_compensation, 0) - nvl(a.decl_count_compensation, 0)
                   ,nvl(b.decl_count_payment, 0) - nvl(a.decl_count_payment, 0)
                   ,nvl(b.decl_count_not_submit, 0) - nvl(a.decl_count_not_submit, 0)
                   ,nvl(b.decl_count_revised_total, 0) - nvl(a.decl_count_revised_total, 0)
                   ,nvl(b.taxbase_sum_compensation, 0) - nvl(a.taxbase_sum_compensation, 0)
                   ,nvl(b.taxbase_sum_payment, 0) - nvl(a.taxbase_sum_payment, 0)
                   ,nvl(b.ndscalculated_sum_compensation, 0) - nvl(a.ndscalculated_sum_compensation, 0)
                   ,nvl(b.ndscalculated_sum_payment, 0) - nvl(a.ndscalculated_sum_payment, 0)
                   ,nvl(b.ndsdeduction_sum_compensation, 0) - nvl(a.ndsdeduction_sum_compensation, 0)
                   ,nvl(b.ndsdeduction_sum_payment, 0) - nvl(a.ndsdeduction_sum_payment, 0)
                   ,nvl(b.nds_sum_compensation, 0) - nvl(a.nds_sum_compensation, 0)
                   ,nvl(b.nds_sum_payment, 0) - nvl(a.nds_sum_payment, 0)
            from
            (
              select fdr.district_id
              from REPORT_DECLARATION_RAW r
              join FEDERAL_DISTRICT_REGION fdr on fdr.region_code = r.region_code
              where (trunc(r.submit_date) = trunc(pDateBegin) or trunc(r.submit_date) = trunc(pDateEnd)) and r.fiscal_year = pFiscalYear
                    and r.tax_period in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD')
              group by fdr.district_id
            ) m
            left outer join
            (
                 select distinct r.district_id
                       ,SUM(r.decl_count_total) OVER (PARTITION BY r.district_id) decl_count_total
                       ,SUM(r.decl_count_null) OVER (PARTITION BY r.district_id) decl_count_null
                       ,SUM(r.decl_count_compensation) OVER (PARTITION BY r.district_id) decl_count_compensation
                       ,SUM(r.decl_count_payment) OVER (PARTITION BY r.district_id) decl_count_payment
                       ,SUM(r.decl_count_not_submit) OVER (PARTITION BY r.district_id) decl_count_not_submit
                       ,SUM(r.decl_count_revised_total) OVER (PARTITION BY r.district_id) decl_count_revised_total
                       ,SUM(r.taxbase_sum_compensation) OVER (PARTITION BY r.district_id) taxbase_sum_compensation
                       ,SUM(r.taxbase_sum_payment) OVER (PARTITION BY r.district_id) taxbase_sum_payment
                       ,SUM(r.ndscalculated_sum_compensation) OVER (PARTITION BY r.district_id) ndscalculated_sum_compensation
                       ,SUM(r.ndscalculated_sum_payment) OVER (PARTITION BY r.district_id) ndscalculated_sum_payment
                       ,SUM(r.ndsdeduction_sum_compensation) OVER (PARTITION BY r.district_id) ndsdeduction_sum_compensation
                       ,SUM(r.ndsdeduction_sum_payment) OVER (PARTITION BY r.district_id) ndsdeduction_sum_payment
                       ,SUM(r.nds_sum_compensation) OVER (PARTITION BY r.district_id) nds_sum_compensation
                       ,SUM(r.nds_sum_payment) OVER (PARTITION BY r.district_id) nds_sum_payment
                 from
                 (
                    select r.*, fd.district_id as district_id
                    from REPORT_DECLARATION_RAW r
                    join FEDERAL_DISTRICT_REGION fdr on fdr.region_code = r.region_code
                    join FEDERAL_DISTRICT fd on fd.district_id = fdr.district_id
                    where trunc(r.submit_date) = trunc(pDateBegin) and r.fiscal_year = pFiscalYear
                          and r.tax_period in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD')
                 )r
            ) a on a.district_id = m.district_id
            left outer join
            (
                 select distinct r.district_id
                       ,SUM(r.decl_count_total) OVER (PARTITION BY r.district_id) decl_count_total
                       ,SUM(r.decl_count_null) OVER (PARTITION BY r.district_id) decl_count_null
                       ,SUM(r.decl_count_compensation) OVER (PARTITION BY r.district_id) decl_count_compensation
                       ,SUM(r.decl_count_payment) OVER (PARTITION BY r.district_id) decl_count_payment
                       ,SUM(r.decl_count_not_submit) OVER (PARTITION BY r.district_id) decl_count_not_submit
                       ,SUM(r.decl_count_revised_total) OVER (PARTITION BY r.district_id) decl_count_revised_total
                       ,SUM(r.taxbase_sum_compensation) OVER (PARTITION BY r.district_id) taxbase_sum_compensation
                       ,SUM(r.taxbase_sum_payment) OVER (PARTITION BY r.district_id) taxbase_sum_payment
                       ,SUM(r.ndscalculated_sum_compensation) OVER (PARTITION BY r.district_id) ndscalculated_sum_compensation
                       ,SUM(r.ndscalculated_sum_payment) OVER (PARTITION BY r.district_id) ndscalculated_sum_payment
                       ,SUM(r.ndsdeduction_sum_compensation) OVER (PARTITION BY r.district_id) ndsdeduction_sum_compensation
                       ,SUM(r.ndsdeduction_sum_payment) OVER (PARTITION BY r.district_id) ndsdeduction_sum_payment
                       ,SUM(r.nds_sum_compensation) OVER (PARTITION BY r.district_id) nds_sum_compensation
                       ,SUM(r.nds_sum_payment) OVER (PARTITION BY r.district_id) nds_sum_payment
                 from
                 (
                    select r.*, fd.district_id as district_id
                    from REPORT_DECLARATION_RAW r
                    join FEDERAL_DISTRICT_REGION fdr on fdr.region_code = r.region_code
                    join FEDERAL_DISTRICT fd on fd.district_id = fdr.district_id
                    where trunc(r.submit_date) = trunc(pDateEnd) and r.fiscal_year = pFiscalYear
                          and r.tax_period in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD')
                 )r
            ) b on b.district_id = m.district_id
            left outer join FEDERAL_DISTRICT fd on fd.district_id = m.district_id;
          end if;            
          --По регионам
          if pDeclStatisticType = 3 then
            insert into REPORT_DECLARATION (ID, TASK_ID, ACTUAL_NUM, AGG_ROW, Name_Group,Decl_Count_Total, Decl_Count_Null,
                  Decl_Count_Compensation, Decl_Count_Payment, Decl_Count_Not_Submit, Decl_Count_Revised_Total, TaxBase_Sum_Compensation,
                  TaxBase_Sum_Payment, NdsCalculated_Sum_Compensation, NdsCalculated_Sum_Payment, NdsDeduction_Sum_Compensation,
                  NdsDeduction_Sum_Payment, Nds_Sum_Compensation, Nds_Sum_Payment)
            select NDS2$REPORTS.F$GET_DECLARATION_NEXTVAL
                   ,pTaskId
                   ,pActualNumActive
                   ,0 as AGG_ROW
                   ,s.s_code || '-' || s.s_name as name_group
                   ,nvl(b.decl_count_total,0) - nvl(a.decl_count_total ,0)
                   ,nvl(b.decl_count_null,0) - nvl(a.decl_count_null ,0)
                   ,nvl(b.decl_count_compensation,0) - nvl(a.decl_count_compensation ,0)
                   ,nvl(b.decl_count_payment,0) - nvl(a.decl_count_payment ,0)
                   ,nvl(b.decl_count_not_submit,0) - nvl(a.decl_count_not_submit ,0)
                   ,nvl(b.decl_count_revised_total,0) - nvl(a.decl_count_revised_total ,0)
                   ,nvl(b.taxbase_sum_compensation,0) - nvl(a.taxbase_sum_compensation ,0)
                   ,nvl(b.taxbase_sum_payment,0) - nvl(a.taxbase_sum_payment ,0)
                   ,nvl(b.ndscalculated_sum_compensation,0) - nvl(a.ndscalculated_sum_compensation ,0)
                   ,nvl(b.ndscalculated_sum_payment,0) - nvl(a.ndscalculated_sum_payment ,0)
                   ,nvl(b.ndsdeduction_sum_compensation,0) - nvl(a.ndsdeduction_sum_compensation ,0)
                   ,nvl(b.ndsdeduction_sum_payment,0) - nvl(a.ndsdeduction_sum_payment ,0)
                   ,nvl(b.nds_sum_compensation,0) - nvl(a.nds_sum_compensation ,0)
                   ,nvl(b.nds_sum_payment,0) - nvl(a.nds_sum_payment ,0)
            from
            (
              select r.region_code
              from REPORT_DECLARATION_RAW r
              where (trunc(r.submit_date) = trunc(pDateBegin) or trunc(r.submit_date) = trunc(pDateEnd)) and r.fiscal_year = pFiscalYear
                    and r.tax_period in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD')
              group by r.region_code
            ) m
            left outer join
            (
                 select distinct r.region_code
                       ,SUM(r.decl_count_total) OVER (PARTITION BY r.region_code) decl_count_total
                       ,SUM(r.decl_count_null) OVER (PARTITION BY r.region_code) decl_count_null
                       ,SUM(r.decl_count_compensation) OVER (PARTITION BY r.region_code) decl_count_compensation
                       ,SUM(r.decl_count_payment) OVER (PARTITION BY r.region_code) decl_count_payment
                       ,SUM(r.decl_count_not_submit) OVER (PARTITION BY r.region_code) decl_count_not_submit
                       ,SUM(r.decl_count_revised_total) OVER (PARTITION BY r.region_code) decl_count_revised_total
                       ,SUM(r.taxbase_sum_compensation) OVER (PARTITION BY r.region_code) taxbase_sum_compensation
                       ,SUM(r.taxbase_sum_payment) OVER (PARTITION BY r.region_code) taxbase_sum_payment
                       ,SUM(r.ndscalculated_sum_compensation) OVER (PARTITION BY r.region_code) ndscalculated_sum_compensation
                       ,SUM(r.ndscalculated_sum_payment) OVER (PARTITION BY r.region_code) ndscalculated_sum_payment
                       ,SUM(r.ndsdeduction_sum_compensation) OVER (PARTITION BY r.region_code) ndsdeduction_sum_compensation
                       ,SUM(r.ndsdeduction_sum_payment) OVER (PARTITION BY r.region_code) ndsdeduction_sum_payment
                       ,SUM(r.nds_sum_compensation) OVER (PARTITION BY r.region_code) nds_sum_compensation
                       ,SUM(r.nds_sum_payment) OVER (PARTITION BY r.region_code) nds_sum_payment
                 from REPORT_DECLARATION_RAW r
                 where trunc(r.submit_date) = trunc(pDateBegin) and r.fiscal_year = pFiscalYear
                       and r.tax_period in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD')
            ) a on a.region_code = m.region_code
            left outer join
            (
                 select distinct r.region_code
                       ,SUM(r.decl_count_total) OVER (PARTITION BY r.region_code) decl_count_total
                       ,SUM(r.decl_count_null) OVER (PARTITION BY r.region_code) decl_count_null
                       ,SUM(r.decl_count_compensation) OVER (PARTITION BY r.region_code) decl_count_compensation
                       ,SUM(r.decl_count_payment) OVER (PARTITION BY r.region_code) decl_count_payment
                       ,SUM(r.decl_count_not_submit) OVER (PARTITION BY r.region_code) decl_count_not_submit
                       ,SUM(r.decl_count_revised_total) OVER (PARTITION BY r.region_code) decl_count_revised_total
                       ,SUM(r.taxbase_sum_compensation) OVER (PARTITION BY r.region_code) taxbase_sum_compensation
                       ,SUM(r.taxbase_sum_payment) OVER (PARTITION BY r.region_code) taxbase_sum_payment
                       ,SUM(r.ndscalculated_sum_compensation) OVER (PARTITION BY r.region_code) ndscalculated_sum_compensation
                       ,SUM(r.ndscalculated_sum_payment) OVER (PARTITION BY r.region_code) ndscalculated_sum_payment
                       ,SUM(r.ndsdeduction_sum_compensation) OVER (PARTITION BY r.region_code) ndsdeduction_sum_compensation
                       ,SUM(r.ndsdeduction_sum_payment) OVER (PARTITION BY r.region_code) ndsdeduction_sum_payment
                       ,SUM(r.nds_sum_compensation) OVER (PARTITION BY r.region_code) nds_sum_compensation
                       ,SUM(r.nds_sum_payment) OVER (PARTITION BY r.region_code) nds_sum_payment
                 from REPORT_DECLARATION_RAW r
                 where trunc(r.submit_date) = trunc(pDateEnd) and r.fiscal_year = pFiscalYear
                       and r.tax_period in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD')
            ) b on b.region_code = m.region_code
            left outer join V$SSRF s on s.s_code = m.region_code;
          end if;
          --По инспекциям
          if pDeclStatisticType = 4 then
            insert into REPORT_DECLARATION (ID, TASK_ID, ACTUAL_NUM, AGG_ROW, Name_Group,Decl_Count_Total, Decl_Count_Null,
                  Decl_Count_Compensation, Decl_Count_Payment, Decl_Count_Not_Submit, Decl_Count_Revised_Total, TaxBase_Sum_Compensation,
                  TaxBase_Sum_Payment, NdsCalculated_Sum_Compensation, NdsCalculated_Sum_Payment, NdsDeduction_Sum_Compensation,
                  NdsDeduction_Sum_Payment, Nds_Sum_Compensation, Nds_Sum_Payment)
            select NDS2$REPORTS.F$GET_DECLARATION_NEXTVAL
                   ,pTaskId
                   ,pActualNumActive
                   ,0 as AGG_ROW
                   ,s.s_code || '-' || s.s_name
                   ,nvl(b.decl_count_total, 0) - nvl(a.decl_count_total, 0)
                   ,nvl(b.decl_count_null, 0) - nvl(a.decl_count_null, 0)
                   ,nvl(b.decl_count_compensation, 0) - nvl(a.decl_count_compensation, 0)
                   ,nvl(b.decl_count_payment, 0) - nvl(a.decl_count_payment, 0)
                   ,nvl(b.decl_count_not_submit, 0) - nvl(a.decl_count_not_submit, 0)
                   ,nvl(b.decl_count_revised_total, 0) - nvl(a.decl_count_revised_total, 0)
                   ,nvl(b.taxbase_sum_compensation, 0) - nvl(a.taxbase_sum_compensation, 0)
                   ,nvl(b.taxbase_sum_payment, 0) - nvl(a.taxbase_sum_payment, 0)
                   ,nvl(b.ndscalculated_sum_compensation, 0) - nvl(a.ndscalculated_sum_compensation, 0)
                   ,nvl(b.ndscalculated_sum_payment, 0) - nvl(a.ndscalculated_sum_payment, 0)
                   ,nvl(b.ndsdeduction_sum_compensation, 0) - nvl(a.ndsdeduction_sum_compensation, 0)
                   ,nvl(b.ndsdeduction_sum_payment, 0) - nvl(a.ndsdeduction_sum_payment, 0)
                   ,nvl(b.nds_sum_compensation, 0) - nvl(a.nds_sum_compensation, 0)
                   ,nvl(b.nds_sum_payment, 0) - nvl(a.nds_sum_payment, 0)
            from
            (
              select r.soun_code
              from REPORT_DECLARATION_RAW r
              where r.region_code in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'REGION_CODE')
                    and (trunc(r.submit_date) = trunc(pDateBegin) or trunc(r.submit_date) = trunc(pDateEnd)) and r.fiscal_year = pFiscalYear
                    and r.tax_period in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD')
              group by r.soun_code
            ) m
            left outer join
            (
                 select distinct r.soun_code
                       ,SUM(r.decl_count_total) OVER (PARTITION BY r.soun_code) decl_count_total
                       ,SUM(r.decl_count_null) OVER (PARTITION BY r.soun_code) decl_count_null
                       ,SUM(r.decl_count_compensation) OVER (PARTITION BY r.soun_code) decl_count_compensation
                       ,SUM(r.decl_count_payment) OVER (PARTITION BY r.soun_code) decl_count_payment
                       ,SUM(r.decl_count_not_submit) OVER (PARTITION BY r.soun_code) decl_count_not_submit
                       ,SUM(r.decl_count_revised_total) OVER (PARTITION BY r.soun_code) decl_count_revised_total
                       ,SUM(r.taxbase_sum_compensation) OVER (PARTITION BY r.soun_code) taxbase_sum_compensation
                       ,SUM(r.taxbase_sum_payment) OVER (PARTITION BY r.soun_code) taxbase_sum_payment
                       ,SUM(r.ndscalculated_sum_compensation) OVER (PARTITION BY r.soun_code) ndscalculated_sum_compensation
                       ,SUM(r.ndscalculated_sum_payment) OVER (PARTITION BY r.soun_code) ndscalculated_sum_payment
                       ,SUM(r.ndsdeduction_sum_compensation) OVER (PARTITION BY r.soun_code) ndsdeduction_sum_compensation
                       ,SUM(r.ndsdeduction_sum_payment) OVER (PARTITION BY r.soun_code) ndsdeduction_sum_payment
                       ,SUM(r.nds_sum_compensation) OVER (PARTITION BY r.soun_code) nds_sum_compensation
                       ,SUM(r.nds_sum_payment) OVER (PARTITION BY r.soun_code) nds_sum_payment
                 from REPORT_DECLARATION_RAW r
                 where r.region_code in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'REGION_CODE')
                       and trunc(r.submit_date) = trunc(pDateBegin) and r.fiscal_year = pFiscalYear
                       and r.tax_period in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD')
            ) a on a.soun_code = m.soun_code
            left outer join
            (
                 select distinct r.soun_code
                       ,SUM(r.decl_count_total) OVER (PARTITION BY r.soun_code) decl_count_total
                       ,SUM(r.decl_count_null) OVER (PARTITION BY r.soun_code) decl_count_null
                       ,SUM(r.decl_count_compensation) OVER (PARTITION BY r.soun_code) decl_count_compensation
                       ,SUM(r.decl_count_payment) OVER (PARTITION BY r.soun_code) decl_count_payment
                       ,SUM(r.decl_count_not_submit) OVER (PARTITION BY r.soun_code) decl_count_not_submit
                       ,SUM(r.decl_count_revised_total) OVER (PARTITION BY r.soun_code) decl_count_revised_total
                       ,SUM(r.taxbase_sum_compensation) OVER (PARTITION BY r.soun_code) taxbase_sum_compensation
                       ,SUM(r.taxbase_sum_payment) OVER (PARTITION BY r.soun_code) taxbase_sum_payment
                       ,SUM(r.ndscalculated_sum_compensation) OVER (PARTITION BY r.soun_code) ndscalculated_sum_compensation
                       ,SUM(r.ndscalculated_sum_payment) OVER (PARTITION BY r.soun_code) ndscalculated_sum_payment
                       ,SUM(r.ndsdeduction_sum_compensation) OVER (PARTITION BY r.soun_code) ndsdeduction_sum_compensation
                       ,SUM(r.ndsdeduction_sum_payment) OVER (PARTITION BY r.soun_code) ndsdeduction_sum_payment
                       ,SUM(r.nds_sum_compensation) OVER (PARTITION BY r.soun_code) nds_sum_compensation
                       ,SUM(r.nds_sum_payment) OVER (PARTITION BY r.soun_code) nds_sum_payment
                 from REPORT_DECLARATION_RAW r
                 where r.region_code in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'REGION_CODE')
                       and trunc(r.submit_date) = trunc(pDateEnd) and r.fiscal_year = pFiscalYear
                       and r.tax_period in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD')
            ) b on b.soun_code = m.soun_code
            left outer join V$SONO s on s.s_code = m.soun_code;
          end if;
      end;
      end if;

      UPDATE REPORT_TASK SET TASK_STATUS = 1, COMPLETE_DATE = SYSDATE WHERE ID = pTaskId;

      commit;
  end;
  end if;
end;

function F$TECHNO_PARAM_TO_CHAR 
(
   pValue IN NUMBER,
   pParameter_id IN NUMBER
)
return varchar
is
Result varchar(128);
  pDataType VARCHAR(32);
begin
    
   SELECT data_type into pDataType FROM dual
   left outer join (SELECT data_type FROM V$TECHNO_REPORT_PARAMETER where id = pParameter_id) r on 1 = 1;

   Result := F$TECHNO_PARAM_TO_CHAR_TYPE(pValue, pDataType);

   return Result;
end;

function F$TECHNO_PARAM_TO_CHAR_TYPE
(
   pValue IN NUMBER,
   pDataType IN VARCHAR2
)
return varchar
is
Result varchar(128);
begin
   if pDataType is not null then
   begin  
     if pDataType = 'DATE' then
       if pValue is not null then
         Result :=TO_CHAR(TRUNC(pValue/3600),'FM9900') || ':' ||
                  TO_CHAR(TRUNC(MOD(pValue,3600)/60),'FM00') || ':' ||
                  TO_CHAR(MOD(pValue,60),'FM00');
       else
         Result := '';
       end if;
     else
       if pDataType = 'NUMBER_SUMMARY' then
          Result := TO_CHAR(pValue, '9999999999999999990.9');
       else
          Result := TO_CHAR(pValue);
       end if;
     end if;
   end;
   else
      Result := TO_CHAR(pValue);
   end if;
   return Result;
end;

function F$GET_LAST_DAY_IN_MONTH
(
   pMonth IN NUMBER
)
return NUMBER
is
Result NUMBER;
begin
    Result := case when pMonth = 1 then 31 when pMonth = 2 then 28 when pMonth = 3 then 31
                  when pMonth = 4 then 30 when pMonth = 5 then 31 when pMonth = 6 then 30
                  when pMonth = 7 then 31 when pMonth = 8 then 31 when pMonth = 9 then 30
                  when pMonth = 10 then 31 when pMonth = 11 then 30 when pMonth = 12 then 31 end;
   return Result;
end;

function F$IS_DATE_TIME
(
   pValue IN VARCHAR2
)
return NUMBER
is
 pTimeStampValue varchar(128 char);
 pHour varchar(128 char);
 pMinute varchar(128 char);
 pSecond varchar(128 char);
 nCifra number(5);
 time_is_correct number(1);
 nPos number;
 isCheck BOOLEAN;
begin
  pTimeStampValue := pValue;
  pHour := '';
  pMinute := '';
  pSecond := '';
  time_is_correct := 1;

  nPos := INSTR(pTimeStampValue, ':');
  if (nPos >=0) then
      pHour := SUBSTR(pTimeStampValue, 1, nPos - 1);
      pTimeStampValue := SUBSTR(pTimeStampValue, nPos + 1, length(pTimeStampValue) - nPos);
      nPos := INSTR(pTimeStampValue, ':');
      if (nPos >=0) then
         pMinute := SUBSTR(pTimeStampValue, 1, nPos - 1);
         pSecond := SUBSTR(pTimeStampValue, nPos + 1, length(pTimeStampValue) - nPos);
      end if;
  end if;

  ----- Hour ------------------
  isCheck := FALSE;
  if length(trim(pHour)) > 0 then
     isCheck := TRANSLATE(pHour, '_0123456789', '_') is null;
  end if;
  if not isCheck then
    begin
      time_is_correct := 0;
    end;
  end if;
  
  ----- Minute ------------------
  isCheck := FALSE;
  if length(trim(pMinute)) > 0 then
     isCheck := TRANSLATE(pMinute, '_0123456789', '_') is null;
  end if;
  if isCheck then
    begin
      nCifra := to_number(pMinute);
      if (nCifra < 0 or nCifra > 59) then
         time_is_correct := 0;
      end if;
    end;
    else time_is_correct := 0;
  end if;

  ----- Second ------------------
  isCheck := FALSE;
  if length(trim(pSecond)) > 0 then
     isCheck := TRANSLATE(pSecond, '_0123456789', '_') is null;
  end if;
  if isCheck is not null then
    begin
      nCifra := to_number(pSecond);
      if (nCifra < 0 or nCifra > 59) then
         time_is_correct := 0;
      end if;
    end;
    else time_is_correct := 0;
  end if;

  return time_is_correct;
end;

function F$IS_NUMBER
(
   pValue IN VARCHAR2
)
return NUMBER
is
 integer_is_correct number(1);
begin
  integer_is_correct := 0;
  if REGEXP_LIKE(pValue,'^\s*[\Q+-\E]?[[:digit:]]+[\Q.,\E]?[[:digit:]]*\s*$') then
      integer_is_correct := 1;
  end if;
  return integer_is_correct;
end;

-- Создание отчета по динамике изменения технологических параметров
procedure P$CREATE_DYNAMIC_TECHNO_PARAMS
(
  pTaskId IN NUMBER
)
as
  pTaskStatus NUMBER := -1;
  pActualNumActive Number(1);
  pDateBeginRoot DATE;
  pMonthIndex NUMBER(2);
  pYearIndex NUMBER(4);
  pDate1 DATE;
  pDate2 DATE;
  pDateBegin DATE;
  pDateEnd DATE;
  pAggregationLevel NUMBER(1);
  pTimeCounting NUMBER;
  pDayCount NUMBER(2);
  -- массив
  TYPE T_ASSOCIATIVE_ARRAY IS TABLE OF NUMBER INDEX BY PLS_INTEGER;
  p T_ASSOCIATIVE_ARRAY;
  indexArray NUMBER(3);
begin
  pActualNumActive := 1;

  SELECT task_status into pTaskStatus FROM dual
  left outer join (SELECT task_status FROM REPORT_TASK WHERE ID = pTaskId) r on 1 = 1;

  if pTaskStatus = 0 then
  begin
      SELECT PARAM_VALUE into pDateBeginRoot FROM dual
         left outer join (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'DYNAMIC_DATE_BEGIN') r on 1 = 1;
      SELECT PARAM_VALUE into pAggregationLevel FROM dual
         left outer join (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'DYNAMIC_AGGREGATE_LEVEL') r on 1 = 1;
      SELECT PARAM_VALUE into pTimeCounting FROM dual
         left outer join (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'DYNAMIC_TIME_COUNTING') r on 1 = 1;

      delete from REPORT_DYNAMIC_PERIOD WHERE TASK_ID = pTaskId;
      if pDateBeginRoot is not null and pAggregationLevel is not null and pTimeCounting is not null then
        if pAggregationLevel = 1 then
          pDayCount := 1;
        end if;
        if pAggregationLevel = 2 then
          pDayCount := 7;
        end if;
        
        pMonthIndex := EXTRACT(month FROM pDateBeginRoot);
        pYearIndex := EXTRACT(year FROM pDateBeginRoot);

        FOR i IN 1..pTimeCounting LOOP
          if pAggregationLevel = 3 then
              pDate1 := TO_DATE(TO_CHAR(pYearIndex) || '/' || TO_CHAR(pMonthIndex) || '/' || '01', 'yyyy/mm/dd');
              pDate2 := TO_DATE(TO_CHAR(pYearIndex) || '/' || TO_CHAR(pMonthIndex) || '/' || TO_CHAR(F$GET_LAST_DAY_IN_MONTH(pMonthIndex)), 'yyyy/mm/dd');
              insert into REPORT_DYNAMIC_PERIOD(ID, TASK_ID, Date_Begin, Date_End, Period_Number)
                     values(SEQ_REPORT_DYNAMIC_PERIOD.NEXTVAL, pTaskId, pDate1, pDate2, i);
              if pMonthIndex < 12 then
                pMonthIndex := pMonthIndex + 1;
              else
                pMonthIndex := 1;
                pYearIndex := pYearIndex + 1;
              end if;
          else
            insert into REPORT_DYNAMIC_PERIOD(ID, TASK_ID, Date_Begin, Date_End, Period_Number)
                   values(SEQ_REPORT_DYNAMIC_PERIOD.NEXTVAL, pTaskId, pDateBeginRoot + pDayCount*(i-1), pDateBeginRoot + pDayCount*i-1, i);
          end if;
        END LOOP;
      end if;

      SELECT Date_Begin into pDateBegin FROM dual
      left outer join (SELECT min(Date_Begin) as Date_Begin FROM REPORT_DYNAMIC_PERIOD WHERE TASK_ID = pTaskId) r on 1 = 1;
      SELECT Date_End into pDateEnd FROM dual
      left outer join (SELECT max(Date_End) as Date_End FROM REPORT_DYNAMIC_PERIOD WHERE TASK_ID = pTaskId) r on 1 = 1;

      indexArray := 1;
      FOR r IN (SELECT param_value FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId and PARAM_KEY = 'DYNAMIC_PARAMETERS') LOOP
        p(indexArray) := r.param_value;
        indexArray := indexArray + 1;
      END LOOP;

      FOR i IN indexArray..30 LOOP
        p(i) := 0;
      END LOOP;

      delete from REPORT_DYNAMIC_PARAMETERS WHERE TASK_ID = pTaskId;
      insert into REPORT_DYNAMIC_PARAMETERS (ID, TASK_ID, ACTUAL_NUM, AGG_ROW, Period_Rank, Period, P01, P02, P03, P04, P05, P06,
         P07, P08, P09, P10, P11, P12, P13, P14, P15, P16, P17, P18, P19, P20, P21, P22, P23, P24, P25, P26, P27, P28, P29, P30)
      with t as
      (
          select
                 period
                ,parameter_id
                ,sum(n_value) as aggr_value
          from
          (
            select rv.parameter_id as parameter_id
               ,(case when rp.data_type is not null and rp.data_type = 'DATE' then
                           (case when F$IS_DATE_TIME(rv.value) = 1 then
                                extract(hour from(TO_TIMESTAMP(rv.value, 'HH24:MI:SS')))*3600 +
                                extract(minute from (TO_TIMESTAMP(rv.value, 'HH24:MI:SS')))*60 +
                                extract(second from (TO_TIMESTAMP(rv.value, 'HH24:MI:SS')))
                           else 0 end)
                      else (case when F$IS_NUMBER(rv.value) = 1 then to_number(replace(rv.value,',','.')) else 0 end)
                 end) as n_value
                   ,dp.period_number as period
            from V$TECHNO_REPORT_VALUE rv
            join TECHNO_REPORT_ITERATION ri on ri.id = rv.iteration_id
            join V$TECHNO_REPORT_PARAMETER rp on rp.id = rv.parameter_id
            left outer join REPORT_DYNAMIC_PERIOD dp on dp.task_id = pTaskId and ri.create_date >= dp.date_begin and ri.create_date <= dp.date_end
            where ri.create_date >= pDateBegin
                  and ri.create_date <= pDateEnd
          ) m
          group by m.period, m.parameter_id
          order by m.period
      )
      SELECT
        NDS2$REPORTS.F$GET_DYNAMIC_PARAMS_NEXTVAL, pTaskId, pActualNumActive, 0,
        x.period, b.period_name, x.P01, x.P02, x.P03, x.P04, x.P05, x.P06, x.P07, x.P08, x.P09, x.P10, x.P11, x.P12, x.P13,
        x.P14, x.P15, x.P16, x.P17, x.P18, x.P19, x.P20, x.P21, x.P22, x.P23, x.P24, x.P25, x.P26, x.P27, x.P28, x.P29, x.P30
      FROM
      (
        SELECT
             period,
             F$TECHNO_PARAM_TO_CHAR(SUM(DECODE(parameter_id, p(1), aggr_value, NULL)), p(1)) "P01", F$TECHNO_PARAM_TO_CHAR(SUM(DECODE(parameter_id, p(2), aggr_value, NULL)), p(2)) "P02",
             F$TECHNO_PARAM_TO_CHAR(SUM(DECODE(parameter_id, p(3), aggr_value, NULL)), p(3)) "P03", F$TECHNO_PARAM_TO_CHAR(SUM(DECODE(parameter_id, p(4), aggr_value, NULL)), p(4)) "P04",
             F$TECHNO_PARAM_TO_CHAR(SUM(DECODE(parameter_id, p(5), aggr_value, NULL)), p(5)) "P05", F$TECHNO_PARAM_TO_CHAR(SUM(DECODE(parameter_id, p(6), aggr_value, NULL)), p(6)) "P06",
             F$TECHNO_PARAM_TO_CHAR(SUM(DECODE(parameter_id, p(7), aggr_value, NULL)), p(7)) "P07", F$TECHNO_PARAM_TO_CHAR(SUM(DECODE(parameter_id, p(8), aggr_value, NULL)), p(8)) "P08",
             F$TECHNO_PARAM_TO_CHAR(SUM(DECODE(parameter_id, p(9), aggr_value, NULL)), p(9)) "P09", F$TECHNO_PARAM_TO_CHAR(SUM(DECODE(parameter_id, p(10), aggr_value, NULL)), p(10)) "P10",
             F$TECHNO_PARAM_TO_CHAR(SUM(DECODE(parameter_id, p(11), aggr_value, NULL)), p(11)) "P11", F$TECHNO_PARAM_TO_CHAR(SUM(DECODE(parameter_id, p(12), aggr_value, NULL)), p(12)) "P12",
             F$TECHNO_PARAM_TO_CHAR(SUM(DECODE(parameter_id, p(13), aggr_value, NULL)), p(13)) "P13", F$TECHNO_PARAM_TO_CHAR(SUM(DECODE(parameter_id, p(14), aggr_value, NULL)), p(14)) "P14",
             F$TECHNO_PARAM_TO_CHAR(SUM(DECODE(parameter_id, p(15), aggr_value, NULL)), p(15)) "P15", F$TECHNO_PARAM_TO_CHAR(SUM(DECODE(parameter_id, p(16), aggr_value, NULL)), p(16)) "P16",
             F$TECHNO_PARAM_TO_CHAR(SUM(DECODE(parameter_id, p(17), aggr_value, NULL)), p(17)) "P17", F$TECHNO_PARAM_TO_CHAR(SUM(DECODE(parameter_id, p(18), aggr_value, NULL)), p(18)) "P18",
             F$TECHNO_PARAM_TO_CHAR(SUM(DECODE(parameter_id, p(19), aggr_value, NULL)), p(19)) "P19", F$TECHNO_PARAM_TO_CHAR(SUM(DECODE(parameter_id, p(20), aggr_value, NULL)), p(20)) "P20",
             F$TECHNO_PARAM_TO_CHAR(SUM(DECODE(parameter_id, p(21), aggr_value, NULL)), p(21)) "P21", F$TECHNO_PARAM_TO_CHAR(SUM(DECODE(parameter_id, p(22), aggr_value, NULL)), p(22)) "P22",
             F$TECHNO_PARAM_TO_CHAR(SUM(DECODE(parameter_id, p(23), aggr_value, NULL)), p(23)) "P23", F$TECHNO_PARAM_TO_CHAR(SUM(DECODE(parameter_id, p(24), aggr_value, NULL)), p(24)) "P24",
             F$TECHNO_PARAM_TO_CHAR(SUM(DECODE(parameter_id, p(25), aggr_value, NULL)), p(25)) "P25", F$TECHNO_PARAM_TO_CHAR(SUM(DECODE(parameter_id, p(26), aggr_value, NULL)), p(26)) "P26",
             F$TECHNO_PARAM_TO_CHAR(SUM(DECODE(parameter_id, p(27), aggr_value, NULL)), p(27)) "P27", F$TECHNO_PARAM_TO_CHAR(SUM(DECODE(parameter_id, p(28), aggr_value, NULL)), p(28)) "P28",
             F$TECHNO_PARAM_TO_CHAR(SUM(DECODE(parameter_id, p(29), aggr_value, NULL)), p(29)) "P29", F$TECHNO_PARAM_TO_CHAR(SUM(DECODE(parameter_id, p(30), aggr_value, NULL)), p(30)) "P30"
        FROM
        (
            select * from t
            union
            select dp.period_number as period, 0 as parameter_id, 0 as aggr_value
            from REPORT_DYNAMIC_PERIOD dp
            where dp.task_id = pTaskId and dp.period_number not in ( select distinct period from t )
        )
        GROUP BY period
      ) x
      left outer join
      (
           SELECT period_number
                  ,(case when pAggregationLevel = 1 then to_char(date_begin, 'DD.MM.YYYY') else
                  ('(' || to_char(date_begin, 'DD.MM.YYYY') || ' - ' || to_char(date_end, 'DD.MM.YYYY') || ')') end) as period_name
           FROM REPORT_DYNAMIC_PERIOD
           WHERE task_id = pTaskId
      ) b on b.period_number = x.period;

      UPDATE REPORT_TASK SET TASK_STATUS = 1, COMPLETE_DATE = SYSDATE WHERE ID = pTaskId;
      commit;
  end;
  end if;
end;

-- Создание отчёта по динамике изменения фоновых показателей (по поданным декларациям)
procedure P$CREATE_DYNAMIC_DECLARATION
(
  pTaskId IN NUMBER
)
as
  pTaskStatus NUMBER := -1;
  pActualNumActive Number(1);
  pDateBegin DATE;
  pDateEnd DATE;
  pFiscalYear NUMBER(4);  
  pAggregateByTime NUMBER(1);
  pDateBeginDay DATE;
  pDateEndDay DATE;
  pDateBeginWeek DATE;
  pDateEndWeek DATE;
  pTimeCounting NUMBER(10);
  pDayCount NUMBER(10);
  pDateBeginRoot DATE;
  pYearBegin NUMBER(4);
  pYearEnd NUMBER(4);  
  pMonthBegin NUMBER(2);
  pMonthEnd NUMBER(2);
  pMonthIndex NUMBER(2);
  pYearIndex NUMBER(4);  
  pDate1 DATE;
  pDate2 DATE;  
  pAggregateByNO NUMBER(1);
  pT NUMBER(1);   
  -- массив
  TYPE T_ASSOCIATIVE_ARRAY IS TABLE OF NUMBER INDEX BY PLS_INTEGER;
  p T_ASSOCIATIVE_ARRAY;
  indexArray NUMBER(3);
begin
  pActualNumActive := 1;

  SELECT task_status into pTaskStatus FROM dual
  left outer join (SELECT task_status FROM REPORT_TASK WHERE ID = pTaskId) r on 1 = 1;

  if pTaskStatus = 0 then
  begin
      SELECT PARAM_VALUE into pFiscalYear FROM dual
         left outer join (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'FISCAL_YEAR') r on 1 = 1;
      SELECT PARAM_VALUE into pAggregateByTime FROM dual
         left outer join (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'AGGREGATE_BY_TIME') r on 1 = 1;
      SELECT PARAM_VALUE into pAggregateByNO FROM dual
         left outer join (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'AGGREGATE_BY_NALOG_ORGAN') r on 1 = 1;
      SELECT PARAM_VALUE into pT FROM dual
         left outer join (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'TYPE_REPORT') r on 1 = 1;
                           
      -- создание периодов агрегации   
      delete from REPORT_DYNAMIC_PERIOD WHERE TASK_ID = pTaskId;   
      if pAggregateByTime = 1 or pAggregateByTime = 2 then
        -- по дням     
        if pAggregateByTime = 1 then
          SELECT PARAM_VALUE into pDateBeginDay FROM dual
             left outer join (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'DATE_BEGIN_DAY') r on 1 = 1;
          SELECT PARAM_VALUE into pDateEndDay FROM dual
             left outer join (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'DATE_END_DAY') r on 1 = 1;
          pDayCount := 1;   
          pDateBeginRoot := pDateBeginDay;
          pTimeCounting := TRUNC(pDateEndDay) - TRUNC(pDateBeginDay) + 1;
        end if;   
        -- понедельно
        if pAggregateByTime = 2 then
          SELECT PARAM_VALUE into pDateBeginWeek FROM dual
             left outer join (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'DATE_BEGIN_WEEK') r on 1 = 1;
          SELECT PARAM_VALUE into pDateEndWeek FROM dual
             left outer join (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'DATE_END_WEEK') r on 1 = 1;
          pDayCount := 7;   
          pDateBeginRoot := pDateBeginWeek;   
          pTimeCounting := ROUND((TRUNC(pDateEndWeek) - TRUNC(pDateBeginWeek)) / 7);
        end if;   
        FOR i IN 1..pTimeCounting LOOP
            insert into REPORT_DYNAMIC_PERIOD(ID, TASK_ID, Date_Begin, Date_End, Period_Number)
            values(SEQ_REPORT_DYNAMIC_PERIOD.NEXTVAL, pTaskId, pDateBeginRoot + pDayCount*(i-1), pDateBeginRoot + pDayCount*i-1, i);
        END LOOP;  
      end if;
         
      -- помесячно
      if pAggregateByTime = 3 then
        SELECT PARAM_VALUE into pYearBegin FROM dual
           left outer join (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'YEAR_BEGIN') r on 1 = 1;
        SELECT PARAM_VALUE into pYearEnd FROM dual
           left outer join (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'YEAR_END') r on 1 = 1;
        SELECT PARAM_VALUE into pMonthBegin FROM dual
           left outer join (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'MONTH_BEGIN') r on 1 = 1;
        SELECT PARAM_VALUE into pMonthEnd FROM dual
           left outer join (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'MONTH_END') r on 1 = 1;
             
        pMonthIndex := pMonthBegin;
        pYearIndex := pYearBegin;             
        pDateBeginRoot := TO_DATE(TO_CHAR(pYearBegin) || '/' || TO_CHAR(pMonthBegin) || '/' || '01', 'yyyy/mm/dd');
        pTimeCounting := months_between(TO_DATE(TO_CHAR(pYearEnd) || '/' || TO_CHAR(pMonthEnd) || '/' || TO_CHAR(F$GET_LAST_DAY_IN_MONTH(pMonthEnd)), 'yyyy/mm/dd'), pDateBeginRoot);
        
        FOR i IN 1..pTimeCounting LOOP
          pDate1 := TO_DATE(TO_CHAR(pYearIndex) || '/' || TO_CHAR(pMonthIndex) || '/' || '01', 'yyyy/mm/dd');
          pDate2 := TO_DATE(TO_CHAR(pYearIndex) || '/' || TO_CHAR(pMonthIndex) || '/' || TO_CHAR(F$GET_LAST_DAY_IN_MONTH(pMonthIndex)), 'yyyy/mm/dd');
          insert into REPORT_DYNAMIC_PERIOD(ID, TASK_ID, Date_Begin, Date_End, Period_Number)
                 values(SEQ_REPORT_DYNAMIC_PERIOD.NEXTVAL, pTaskId, pDate1, pDate2, i);
          if pMonthIndex < 12 then
            pMonthIndex := pMonthIndex + 1;
          else
            pMonthIndex := 1;
            pYearIndex := pYearIndex + 1;
          end if;
        END LOOP;
      end if;        
      
      -- взять дату начала и дату окончания обобщенного интервала
      SELECT Date_Begin into pDateBegin FROM dual
      left outer join (SELECT min(Date_Begin) as Date_Begin FROM REPORT_DYNAMIC_PERIOD WHERE TASK_ID = pTaskId) r on 1 = 1;
      SELECT Date_End into pDateEnd FROM dual
      left outer join (SELECT max(Date_End) as Date_End FROM REPORT_DYNAMIC_PERIOD WHERE TASK_ID = pTaskId) r on 1 = 1;
            
      -- формирование массива столбцов отчета 
      indexArray := 1;
      FOR r IN (SELECT param_value FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId and PARAM_KEY = 'DYNAMIC_PARAMETERS') LOOP
        p(indexArray) := r.param_value;
        indexArray := indexArray + 1;
      END LOOP;

      FOR i IN indexArray..14 LOOP
        p(i) := 0;
      END LOOP;
      
      -- создание отчета 
      delete from REPORT_DYNAMIC_DECLARATION where TASK_ID = pTaskId; 
      INSERT INTO REPORT_DYNAMIC_DECLARATION (id, task_id, actual_num, agg_row, period_rank, period, p01, p02, p03, p04, p05, p06, p07, 
                p08, p09, p10, p11, p12, p13, p14)
      with t as
      (          
         select period
                ,(case when p = 'P01' then 1 when p = 'P02' then 2 when p = 'P03' then 3 when p = 'P04' then 4 when p = 'P05' then 5
                       when p = 'P06' then 6 when p = 'P07' then 7 when p = 'P08' then 8 when p = 'P09' then 9 when p = 'P10' then 10 
                       when p = 'P11' then 11 when p = 'P12' then 12 when p = 'P13' then 13 when p = 'P14' then 14 end) as parameter_id
                ,value as aggr_value
         from 
         (
             -- либо абсолютное значение(значения последней даты периода), либо разницу между значениями последних дат периодов
             select period
                  ,(case when pT = 1 then p01 when pT = 2 then p01 - (LAG(p01, 1, 0) OVER (ORDER BY period)) end) AS p01
                  ,(case when pT = 1 then p02 when pT = 2 then p02 - (LAG(p02, 1, 0) OVER (ORDER BY period)) end) AS p02
                  ,(case when pT = 1 then p03 when pT = 2 then p03 - (LAG(p03, 1, 0) OVER (ORDER BY period)) end) AS p03
                  ,(case when pT = 1 then p04 when pT = 2 then p04 - (LAG(p04, 1, 0) OVER (ORDER BY period)) end) AS p04
                  ,(case when pT = 1 then p05 when pT = 2 then p05 - (LAG(p05, 1, 0) OVER (ORDER BY period)) end) AS p05
                  ,(case when pT = 1 then p06 when pT = 2 then p06 - (LAG(p06, 1, 0) OVER (ORDER BY period)) end) AS p06
                  ,(case when pT = 1 then p07 when pT = 2 then p07 - (LAG(p07, 1, 0) OVER (ORDER BY period)) end) AS p07
                  ,(case when pT = 1 then p08 when pT = 2 then p08 - (LAG(p08, 1, 0) OVER (ORDER BY period)) end) AS p08
                  ,(case when pT = 1 then p09 when pT = 2 then p09 - (LAG(p09, 1, 0) OVER (ORDER BY period)) end) AS p09
                  ,(case when pT = 1 then p10 when pT = 2 then p10 - (LAG(p10, 1, 0) OVER (ORDER BY period)) end) AS p10
                  ,(case when pT = 1 then p11 when pT = 2 then p11 - (LAG(p11, 1, 0) OVER (ORDER BY period)) end) AS p11
                  ,(case when pT = 1 then p12 when pT = 2 then p12 - (LAG(p12, 1, 0) OVER (ORDER BY period)) end) AS p12
                  ,(case when pT = 1 then p13 when pT = 2 then p13 - (LAG(p13, 1, 0) OVER (ORDER BY period)) end) AS p13
                  ,(case when pT = 1 then p14 when pT = 2 then p14 - (LAG(p14, 1, 0) OVER (ORDER BY period)) end) AS p14
             from 
             (
                select period
                      ,sum(decl_count_total) as p01
                      ,sum(decl_count_null) as p02
                      ,sum(decl_count_compensation) as p03
                      ,sum(decl_count_payment) as p04
                      ,sum(decl_count_not_submit) as p05
                      ,sum(decl_count_revised_total) as p06
                      ,sum(taxbase_sum_compensation) as p07
                      ,sum(taxbase_sum_payment) as p08
                      ,sum(ndscalculated_sum_compensation) as p09
                      ,sum(ndscalculated_sum_payment) as p10
                      ,sum(ndsdeduction_sum_compensation) as p11
                      ,sum(ndsdeduction_sum_payment) as p12
                      ,sum(nds_sum_compensation) as p13
                      ,sum(nds_sum_payment) as p14
                from
                (
                   select * from
                   (
                      select r.*, dp.period_number as period
                             ,max(r.submit_date) OVER (PARTITION BY dp.period_number) as max_submit_in_period
                      from REPORT_DECLARATION_RAW r
                      join FEDERAL_DISTRICT_REGION fdr on fdr.region_code = r.region_code
                      join FEDERAL_DISTRICT fd on fd.district_id = fdr.district_id
                      join REPORT_DYNAMIC_PERIOD dp on dp.task_id = pTaskId and trunc(r.submit_date) >= trunc(dp.date_begin) and trunc(r.submit_date) <= trunc(dp.date_end)
                      where trunc(r.submit_date) >= trunc(pDateBegin)
                            and trunc(r.submit_date) <= trunc(pDateEnd)
                            and r.fiscal_year = pFiscalYear
                            and r.tax_period in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD')          
                            and (case 
                                when pAggregateByNO = 1 then 1
                                when pAggregateByNO = 2 and (fd.district_id in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'FEDERAL_CODE')) then 1
                                when pAggregateByNO = 3 and(r.region_code in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'REGION_CODE')) then 1
                                when pAggregateByNO = 4 and (r.soun_code in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'SOUN_CODE')) then 1
                                else 0 end) = 1
                   ) e where trunc(e.max_submit_in_period) = trunc(e.submit_date)
                ) m group by period
             ) u
         ) x unpivot (value for p in (p01, p02, p03, p04, p05, p06, p07, p08, p09, p10, p11, p12, p13, p14))
      )          
      SELECT NDS2$REPORTS.F$GET_DYNAMIC_PARAMS_NEXTVAL, pTaskId, pActualNumActive, 0,
             x.period, b.period_name, x.P01, x.P02, x.P03, x.P04, x.P05, x.P06, x.P07, x.P08, x.P09, x.P10, x.P11, x.P12, x.P13, x.P14
      FROM
      (        
          SELECT period,
             TO_CHAR(SUM(DECODE(parameter_id, p(1), aggr_value, NULL))) "P01", TO_CHAR(SUM(DECODE(parameter_id, p(2), aggr_value, NULL))) "P02",
             TO_CHAR(SUM(DECODE(parameter_id, p(3), aggr_value, NULL))) "P03", TO_CHAR(SUM(DECODE(parameter_id, p(4), aggr_value, NULL))) "P04",
             TO_CHAR(SUM(DECODE(parameter_id, p(5), aggr_value, NULL))) "P05", TO_CHAR(SUM(DECODE(parameter_id, p(6), aggr_value, NULL))) "P06",
             TO_CHAR(SUM(DECODE(parameter_id, p(7), aggr_value, NULL))) "P07", TO_CHAR(SUM(DECODE(parameter_id, p(8), aggr_value, NULL))) "P08",
             TO_CHAR(SUM(DECODE(parameter_id, p(9), aggr_value, NULL))) "P09", TO_CHAR(SUM(DECODE(parameter_id, p(10), aggr_value, NULL))) "P10",
             TO_CHAR(SUM(DECODE(parameter_id, p(11), aggr_value, NULL))) "P11", TO_CHAR(SUM(DECODE(parameter_id, p(12), aggr_value, NULL))) "P12",
             TO_CHAR(SUM(DECODE(parameter_id, p(13), aggr_value, NULL))) "P13", TO_CHAR(SUM(DECODE(parameter_id, p(14), aggr_value, NULL))) "P14"
          FROM
          (
             select * from t
             union
             select dp.period_number as period, 0 as parameter_id, 0 as aggr_value
             from REPORT_DYNAMIC_PERIOD dp
             where dp.task_id = pTaskId and dp.period_number not in ( select distinct period from t )               
          ) s
          group by period  
      ) x
      left outer join
      (
           SELECT period_number
                  ,(case when pAggregateByTime = 1 then to_char(date_begin, 'DD.MM.YYYY') else
                  (to_char(date_begin, 'DD.MM.YYYY') || ' - ' || to_char(date_end, 'DD.MM.YYYY')) end) as period_name
           FROM REPORT_DYNAMIC_PERIOD
           WHERE task_id = pTaskId
      ) b on b.period_number = x.period;            

      UPDATE REPORT_TASK SET TASK_STATUS = 1, COMPLETE_DATE = SYSDATE WHERE ID = pTaskId;
      commit;
  end;
  end if;
end;

-- Создание отчёта Информация о результатах сопоставлений (В2)
procedure P$CREATE_INFO_RESULT_MATCHING
(
  pTaskId IN NUMBER
)
as
  pTaskStatus NUMBER := -1;
  pActualNumActive Number(1);
  pAggrNO NUMBER(1);
  pTimePeriod NUMBER(1);
  pDateOneDay DATE;
  pDateBegin DATE;
  pDateEnd DATE;
begin
  pActualNumActive := 1;

  SELECT task_status into pTaskStatus FROM dual
  left outer join (SELECT task_status FROM REPORT_TASK WHERE ID = pTaskId) r on 1 = 1;

  if pTaskStatus = 0 then
  begin
     SELECT PARAM_VALUE into pAggrNO FROM dual
         left outer join (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'AGGREGATE_BY_NALOG_ORGAN') r on 1 = 1;
      SELECT PARAM_VALUE into pTimePeriod FROM dual
         left outer join (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'TIME_PERIOD') r on 1 = 1;
      SELECT PARAM_VALUE into pDateOneDay FROM dual
         left outer join (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'DATE_ONE_DAY') r on 1 = 1;
      SELECT PARAM_VALUE into pDateBegin FROM dual
         left outer join (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'DATE_BEGIN') r on 1 = 1;
      SELECT PARAM_VALUE into pDateEnd FROM dual
         left outer join (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'DATE_END') r on 1 = 1;

      delete from REPORT_INFO_RESULT_MATCHING where TASK_ID = pTaskId;
      insert into REPORT_INFO_RESULT_MATCHING (id, task_id, actual_num, agg_row, federal_district, region_code, soun_code, i_summary,
        aggregate_code, aggregate_name, primary_declaration_count,
        correction_declaration_count, decl_with_mediator_ops_count, journal_count, invoice_record_count, invoice_record_count_chapter8,
        invoice_record_count_chapter9, invoice_record_count_chapter10, invoice_record_count_chapter11, invoice_record_count_chapter12,
        exact_match_count, prim_decl_with_mismatch_count, corr_decl_with_mismatch_count, decl_with_kc_mismatch_count, mismatch_total_count,
        mismatch_total_sum, weak_and_nds_count, weak_and_nds_sum, weak_and_no_nds_count, exact_and_nds_count, exact_and_nds_sum, gap_count,
        gap_amount, currency_count, currency_amount, mismatch_tno_count, mismatch_tno_sum, weight_mismatch_in_total_oper)
      -- общий блок запроса
      with t
      as
      (
          select r.*, fd.district_id
          from
          (
             select sm.*, SUBSTR(sm.inspection_code, 1, 2) as region_code_r
             from SOV_MATCH_RESULT_SUMMARY sm
          ) r
          join FEDERAL_DISTRICT_REGION fdr on fdr.region_code = r.region_code_r
          join FEDERAL_DISTRICT fd on fd.district_id = fdr.district_id
          where (to_char(fiscal_period) || '-' || to_char(fiscal_year)) in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD_CHECK')
                and (case
                    when pAggrNO = 1 then 1
                    when pAggrNO = 2 and (fd.district_id in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'FEDERAL_CODE')) then 1
                    when pAggrNO = 3 and(r.region_code_r in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'REGION_CODE')) then 1
                    when pAggrNO = 4 and (r.inspection_code in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'SOUN_CODE')) then 1
                    else 0 end) = 1
                and (case
                    when pTimePeriod = 1 and trunc(r.process_date) = trunc(pDateOneDay) then 1
                    when pTimePeriod = 2 and (trunc(r.process_date) = trunc(pDateBegin) or trunc(r.process_date) = trunc(pDateEnd)) then 1
                    else 0 end) = 1
      )
      select
        F$GET_INFO_RES_MATCH_NEXTVAL, pTaskId, pActualNumActive, 0, m.federal_district, m.region_code, m.soun_code, 3 as i_summary, m.aggregate_code
        ,(case when pAggrNO = 1 then 'Вся страна'
               when pAggrNO = 2 then fd.description
               when pAggrNO = 3 then ssrf.s_code || ' - ' || ssrf.s_name
               when pAggrNO = 4 then sono.s_code || ' - ' || sono.s_name end) as aggregate_name
        -- разница в случае запроса за период (дата окончания периода - дата начала периода)
        ,m.primary_declaration_count             - nvl(b.primary_declaration_count, 0)
        ,m.correction_declaration_count          - nvl(b.correction_declaration_count, 0)
        ,m.decl_with_mediator_ops_count          - nvl(b.decl_with_mediator_ops_count, 0)
        ,m.journal_count                         - nvl(b.journal_count, 0)
        ,m.invoice_record_count                  - nvl(b.invoice_record_count, 0)
        ,m.invoice_record_count_chapter8         - nvl(b.invoice_record_count_chapter8, 0)
        ,m.invoice_record_count_chapter9         - nvl(b.invoice_record_count_chapter9, 0)
        ,m.invoice_record_count_chapter10        - nvl(b.invoice_record_count_chapter10, 0)
        ,m.invoice_record_count_chapter11        - nvl(b.invoice_record_count_chapter11, 0)
        ,m.invoice_record_count_chapter12        - nvl(b.invoice_record_count_chapter12, 0)
        ,m.exact_match_count                     - nvl(b.exact_match_count, 0)
        ,m.prim_decl_with_mismatch_count         - nvl(b.prim_decl_with_mismatch_count, 0)
        ,m.corr_decl_with_mismatch_count         - nvl(b.corr_decl_with_mismatch_count, 0)
        ,m.decl_with_kc_mismatch_count           - nvl(b.decl_with_kc_mismatch_count, 0)
        ,m.mismatch_total_count                  - nvl(b.mismatch_total_count, 0)
        ,m.mismatch_total_sum                    - nvl(b.mismatch_total_sum, 0)
        ,m.weak_and_nds_count                    - nvl(b.weak_and_nds_count, 0)
        ,m.weak_and_nds_sum                      - nvl(b.weak_and_nds_sum, 0)
        ,m.weak_and_no_nds_count                 - nvl(b.weak_and_no_nds_count, 0)
        ,m.exact_and_nds_count                   - nvl(b.exact_and_nds_count, 0)
        ,m.exact_and_nds_sum                     - nvl(b.exact_and_nds_sum, 0)
        ,m.gap_count                             - nvl(b.gap_count, 0)
        ,m.gap_amount                            - nvl(b.gap_amount, 0)
        ,m.currency_count                        - nvl(b.currency_count, 0)
        ,m.currency_amount                       - nvl(b.currency_amount, 0)
        ,m.mismatch_tno_count                    - nvl(b.mismatch_tno_count, 0)
        ,m.mismatch_tno_sum                      - nvl(b.mismatch_tno_sum, 0)
        ,ROUND((m.mismatch_total_count - nvl(b.mismatch_total_count, 0)) /
         (m.invoice_record_count - nvl(b.invoice_record_count, 0)), 2) as weight_mismatch_in_total_oper
      from
      (
            -- данные на одиночную дату или на дату окончания периода
            select
               (case when pAggrNO = 2 then TO_CHAR(e.district_id)
                     when pAggrNO = 3 then e.region_code_r
                     when pAggrNO = 4 then e.inspection_code end) as aggregate_code
              ,(case when pAggrNO = 3 then max(e.district_id)
                     when pAggrNO = 4 then max(e.district_id) end) as federal_district
              ,(case when pAggrNO = 4 then max(e.region_code_r) end) as region_code
              ,(case when pAggrNO = 4 then max(e.inspection_code) end) as soun_code
              ,sum(primary_declaration_count) as primary_declaration_count
              ,sum(correction_declaration_count) as correction_declaration_count
              ,sum(decl_with_mediator_ops_count) as decl_with_mediator_ops_count
              ,sum(journal_count) as journal_count
              ,sum(invoice_record_count) as invoice_record_count
              ,sum(invoice_record_count_chapter8) as invoice_record_count_chapter8
              ,sum(invoice_record_count_chapter9) as invoice_record_count_chapter9
              ,sum(invoice_record_count_chapter10) as invoice_record_count_chapter10
              ,sum(invoice_record_count_chapter11) as invoice_record_count_chapter11
              ,sum(invoice_record_count_chapter12) as invoice_record_count_chapter12
              ,sum(exact_match_count) as exact_match_count
              ,sum(prim_decl_with_mismatch_count) as prim_decl_with_mismatch_count
              ,sum(corr_decl_with_mismatch_count) as corr_decl_with_mismatch_count
              ,sum(decl_with_kc_mismatch_count) as decl_with_kc_mismatch_count
              ,sum(mismatch_total_count) as mismatch_total_count
              ,sum(mismatch_total_sum) as mismatch_total_sum
              ,sum(weak_and_nds_count) as weak_and_nds_count
              ,sum(weak_and_nds_sum) as weak_and_nds_sum
              ,sum(weak_and_no_nds_count) as weak_and_no_nds_count
              ,sum(exact_and_nds_count) as exact_and_nds_count
              ,sum(exact_and_nds_sum) as exact_and_nds_sum
              ,sum(gap_count) as gap_count
              ,sum(gap_sum) as gap_amount
              ,sum(currency_count) as currency_count
              ,sum(currency_sum) as currency_amount
              ,sum(mismatch_tno_count) as mismatch_tno_count
              ,sum(mismatch_tno_sum) as mismatch_tno_sum
            from
            (
                select * from t
                where (case when pTimePeriod = 1 then 1
                            when pTimePeriod = 2 and trunc(t.process_date) = trunc(pDateEnd) then 1 else 0 end) = 1
            ) e
            group by (case when pAggrNO = 2 then TO_CHAR(e.district_id)
                           when pAggrNO = 3 then e.region_code_r
                           when pAggrNO = 4 then e.inspection_code end)
      ) m
      -- данные на дату начала периода
      left outer join
      (
           select
             (case when pAggrNO = 2 then TO_CHAR(i.district_id)
                   when pAggrNO = 3 then i.region_code_r
                   when pAggrNO = 4 then i.inspection_code end) as aggregate_code
            ,sum(primary_declaration_count) as primary_declaration_count
            ,sum(correction_declaration_count) as correction_declaration_count
            ,sum(decl_with_mediator_ops_count) as decl_with_mediator_ops_count
            ,sum(journal_count) as journal_count
            ,sum(invoice_record_count) as invoice_record_count
            ,sum(invoice_record_count_chapter8) as invoice_record_count_chapter8
            ,sum(invoice_record_count_chapter9) as invoice_record_count_chapter9
            ,sum(invoice_record_count_chapter10) as invoice_record_count_chapter10
            ,sum(invoice_record_count_chapter11) as invoice_record_count_chapter11
            ,sum(invoice_record_count_chapter12) as invoice_record_count_chapter12
            ,sum(exact_match_count) as exact_match_count
            ,sum(prim_decl_with_mismatch_count) as prim_decl_with_mismatch_count
            ,sum(corr_decl_with_mismatch_count) as corr_decl_with_mismatch_count
            ,sum(decl_with_kc_mismatch_count) as decl_with_kc_mismatch_count
            ,sum(mismatch_total_count) as mismatch_total_count
            ,sum(mismatch_total_sum) as mismatch_total_sum
            ,sum(weak_and_nds_count) as weak_and_nds_count
            ,sum(weak_and_nds_sum) as weak_and_nds_sum
            ,sum(weak_and_no_nds_count) as weak_and_no_nds_count
            ,sum(exact_and_nds_count) as exact_and_nds_count
            ,sum(exact_and_nds_sum) as exact_and_nds_sum
            ,sum(gap_count) as gap_count
            ,sum(gap_sum) as gap_amount
            ,sum(currency_count) as currency_count
            ,sum(currency_sum) as currency_amount
            ,sum(mismatch_tno_count) as mismatch_tno_count
            ,sum(mismatch_tno_sum) as mismatch_tno_sum
          from
          (
              select * from t
              where (case when pTimePeriod = 2 and trunc(t.process_date) = trunc(pDateBegin) then 1 else 0 end) = 1
          ) i
          group by (case when pAggrNO = 2 then TO_CHAR(i.district_id)
                         when pAggrNO = 3 then i.region_code_r
                         when pAggrNO = 4 then i.inspection_code end)
      ) b on pTimePeriod = 2 and b.aggregate_code = m.aggregate_code
      left outer join FEDERAL_DISTRICT fd on fd.district_id = m.aggregate_code
      left outer join V$SSRF ssrf on ssrf.s_code = m.aggregate_code
      left outer join V$SONO sono on sono.s_code = m.aggregate_code;

      if pAggrNO = 2 or pAggrNO = 3 or pAggrNO = 4 then
          -- Создание агрегирующих строк (ИТОГО)
          insert into REPORT_INFO_RESULT_MATCHING (id, task_id, actual_num, agg_row, federal_district, region_code, soun_code,
            i_summary, aggregate_code,
            aggregate_name, primary_declaration_count, correction_declaration_count, decl_with_mediator_ops_count, journal_count,
            invoice_record_count, invoice_record_count_chapter8, invoice_record_count_chapter9, invoice_record_count_chapter10,
            invoice_record_count_chapter11, invoice_record_count_chapter12, exact_match_count, prim_decl_with_mismatch_count,
            corr_decl_with_mismatch_count, decl_with_kc_mismatch_count, mismatch_total_count, mismatch_total_sum, weak_and_nds_count,
            weak_and_nds_sum, weak_and_no_nds_count, exact_and_nds_count, exact_and_nds_sum, gap_count, gap_amount, currency_count,
            currency_amount, mismatch_tno_count, mismatch_tno_sum, weight_mismatch_in_total_oper)
          SELECT F$GET_INFO_RES_MATCH_NEXTVAL
                ,pTaskId
                ,pActualNumActive
                ,1 as AGG_ROW
                ,0 as federal_district
                ,'' as region_code
                ,'' as soun_code
                ,0 as i_summary
                ,'' as aggregate_code
                ,'ИТОГО' as aggregate_name
                ,nvl(sum(m.primary_declaration_count), 0)
                ,nvl(sum(m.correction_declaration_count), 0)
                ,nvl(sum(m.decl_with_mediator_ops_count), 0)
                ,nvl(sum(m.journal_count), 0)
                ,nvl(sum(m.invoice_record_count), 0)
                ,nvl(sum(m.invoice_record_count_chapter8), 0)
                ,nvl(sum(m.invoice_record_count_chapter9), 0)
                ,nvl(sum(m.invoice_record_count_chapter10), 0)
                ,nvl(sum(m.invoice_record_count_chapter11), 0)
                ,nvl(sum(m.invoice_record_count_chapter12), 0)
                ,nvl(sum(m.exact_match_count), 0)
                ,nvl(sum(m.prim_decl_with_mismatch_count), 0)
                ,nvl(sum(m.corr_decl_with_mismatch_count), 0)
                ,nvl(sum(m.decl_with_kc_mismatch_count), 0)
                ,nvl(sum(m.mismatch_total_count), 0)
                ,nvl(sum(m.mismatch_total_sum), 0)
                ,nvl(sum(m.weak_and_nds_count), 0)
                ,nvl(sum(m.weak_and_nds_sum), 0)
                ,nvl(sum(m.weak_and_no_nds_count), 0)
                ,nvl(sum(m.exact_and_nds_count), 0)
                ,nvl(sum(m.exact_and_nds_sum), 0)
                ,nvl(sum(m.gap_count), 0)
                ,nvl(sum(m.gap_amount), 0)
                ,nvl(sum(m.currency_count), 0)
                ,nvl(sum(m.currency_amount), 0)
                ,nvl(sum(m.mismatch_tno_count), 0)
                ,nvl(sum(m.mismatch_tno_sum), 0)
                ,(case when sum(nvl(m.invoice_record_count, 0)) <> 0 then 
                      ROUND((sum(nvl(m.mismatch_total_count, 0)) / sum(nvl(m.invoice_record_count, 0))), 2) 
                 end) as weight_mismatch_in_total_oper
          FROM REPORT_INFO_RESULT_MATCHING m
          WHERE m.TASK_ID = pTaskId AND m.AGG_ROW = 0;
      end if;

      if pAggrNO = 3 or pAggrNO = 4 then
          -- Создание агрегирующих строк (ИТОГО по федеральному округу)
          insert into REPORT_INFO_RESULT_MATCHING (id, task_id, actual_num, agg_row, federal_district, region_code, soun_code, i_summary, aggregate_code,
              aggregate_name, primary_declaration_count, correction_declaration_count, decl_with_mediator_ops_count, journal_count, invoice_record_count,
              invoice_record_count_chapter8, invoice_record_count_chapter9, invoice_record_count_chapter10, invoice_record_count_chapter11,
              invoice_record_count_chapter12, exact_match_count, prim_decl_with_mismatch_count, corr_decl_with_mismatch_count,
              decl_with_kc_mismatch_count, mismatch_total_count, mismatch_total_sum, weak_and_nds_count, weak_and_nds_sum, weak_and_no_nds_count,
              exact_and_nds_count, exact_and_nds_sum, gap_count, gap_amount, currency_count, currency_amount, mismatch_tno_count,
              mismatch_tno_sum, weight_mismatch_in_total_oper)
          select F$GET_INFO_RES_MATCH_NEXTVAL
                ,pTaskId
                ,pActualNumActive
                ,1 as AGG_ROW
                ,federal_district as federal_district
                ,'' as region_code
                ,'' as soun_code
                ,1 as i_summary
                ,'' as aggregate_code
                ,('ИТОГО по ' || fd.description) as aggregate_name
                ,p01, p02, p03, p04, p05, p06, p07, p08, p09, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19
                ,p20, p21, p22, p23, p24, p25, p26, p27, p28
          from
          (
              SELECT m.federal_district
                    ,sum(m.primary_declaration_count) as p01
                    ,sum(m.correction_declaration_count) as p02
                    ,sum(m.decl_with_mediator_ops_count) as p03
                    ,sum(m.journal_count) as p04
                    ,sum(m.invoice_record_count) as p05
                    ,sum(m.invoice_record_count_chapter8) as p06
                    ,sum(m.invoice_record_count_chapter9) as p07
                    ,sum(m.invoice_record_count_chapter10) as p08
                    ,sum(m.invoice_record_count_chapter11) as p09
                    ,sum(m.invoice_record_count_chapter12) as p10
                    ,sum(m.exact_match_count) as p11
                    ,sum(m.prim_decl_with_mismatch_count) as p12
                    ,sum(m.corr_decl_with_mismatch_count) as p13
                    ,sum(m.decl_with_kc_mismatch_count) as p14
                    ,sum(m.mismatch_total_count) as p15
                    ,sum(m.mismatch_total_sum) as p16
                    ,sum(m.weak_and_nds_count) as p17
                    ,sum(m.weak_and_nds_sum) as p18
                    ,sum(m.weak_and_no_nds_count) as p19
                    ,sum(m.exact_and_nds_count) as p20
                    ,sum(m.exact_and_nds_sum) as p21
                    ,sum(m.gap_count) as p22
                    ,sum(m.gap_amount) as p23
                    ,sum(m.currency_count) as p24
                    ,sum(m.currency_amount) as p25
                    ,sum(m.mismatch_tno_count) as p26
                    ,sum(m.mismatch_tno_sum) as p27
                    ,ROUND(((sum(m.mismatch_total_count))/(sum(m.invoice_record_count))), 2) as p28
              FROM REPORT_INFO_RESULT_MATCHING m
              WHERE m.TASK_ID = pTaskId AND m.AGG_ROW = 0
              GROUP BY m.federal_district
          ) i
          left outer join FEDERAL_DISTRICT fd on fd.district_id = i.federal_district;
      end if;

      if pAggrNO = 4 then
          -- Создание агрегирующих строк (ИТОГО по региону)
          insert into REPORT_INFO_RESULT_MATCHING (id, task_id, actual_num, agg_row, federal_district, region_code, soun_code, i_summary, aggregate_code,
              aggregate_name, primary_declaration_count, correction_declaration_count, decl_with_mediator_ops_count, journal_count, invoice_record_count,
              invoice_record_count_chapter8, invoice_record_count_chapter9, invoice_record_count_chapter10, invoice_record_count_chapter11,
              invoice_record_count_chapter12, exact_match_count, prim_decl_with_mismatch_count, corr_decl_with_mismatch_count,
              decl_with_kc_mismatch_count, mismatch_total_count, mismatch_total_sum, weak_and_nds_count, weak_and_nds_sum, weak_and_no_nds_count,
              exact_and_nds_count, exact_and_nds_sum, gap_count, gap_amount, currency_count, currency_amount, mismatch_tno_count,
              mismatch_tno_sum, weight_mismatch_in_total_oper)
          select F$GET_INFO_RES_MATCH_NEXTVAL
                ,pTaskId
                ,pActualNumActive
                ,1 as AGG_ROW
                ,federal_district as federal_district
                ,region_code as region_code
                ,'' as soun_code
                ,2 as i_summary
                ,'' as aggregate_code
                ,('ИТОГО по ' || ssrf.s_code || ' - ' || ssrf.s_name) as aggregate_name
                ,p01, p02, p03, p04, p05, p06, p07, p08, p09, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19
                ,p20, p21, p22, p23, p24, p25, p26, p27, p28
          from
          (
              SELECT max(m.federal_district) as federal_district, m.region_code
                    ,sum(m.primary_declaration_count) as p01
                    ,sum(m.correction_declaration_count) as p02
                    ,sum(m.decl_with_mediator_ops_count) as p03
                    ,sum(m.journal_count) as p04
                    ,sum(m.invoice_record_count) as p05
                    ,sum(m.invoice_record_count_chapter8) as p06
                    ,sum(m.invoice_record_count_chapter9) as p07
                    ,sum(m.invoice_record_count_chapter10) as p08
                    ,sum(m.invoice_record_count_chapter11) as p09
                    ,sum(m.invoice_record_count_chapter12) as p10
                    ,sum(m.exact_match_count) as p11
                    ,sum(m.prim_decl_with_mismatch_count) as p12
                    ,sum(m.corr_decl_with_mismatch_count) as p13
                    ,sum(m.decl_with_kc_mismatch_count) as p14
                    ,sum(m.mismatch_total_count) as p15
                    ,sum(m.mismatch_total_sum) as p16
                    ,sum(m.weak_and_nds_count) as p17
                    ,sum(m.weak_and_nds_sum) as p18
                    ,sum(m.weak_and_no_nds_count) as p19
                    ,sum(m.exact_and_nds_count) as p20
                    ,sum(m.exact_and_nds_sum) as p21
                    ,sum(m.gap_count) as p22
                    ,sum(m.gap_amount) as p23
                    ,sum(m.currency_count) as p24
                    ,sum(m.currency_amount) as p25
                    ,sum(m.mismatch_tno_count) as p26
                    ,sum(m.mismatch_tno_sum) as p27
                    ,ROUND(((sum(m.mismatch_total_count))/(sum(m.invoice_record_count))), 2) as p28
              FROM REPORT_INFO_RESULT_MATCHING m
              WHERE m.TASK_ID = pTaskId AND m.AGG_ROW = 0
              GROUP BY m.region_code
          ) i
          left outer join V$SSRF ssrf on ssrf.s_code = i.region_code;
      end if;

      UPDATE REPORT_TASK SET TASK_STATUS = 1, COMPLETE_DATE = SYSDATE WHERE ID = pTaskId;
      commit;
  end;
  end if;
end;

END "NDS2$REPORTS";
/