﻿CREATE OR REPLACE PACKAGE NDS2_MRR_USER."PAC$DECL_MATCHING_CONTRAGENT" IS

procedure START_WORK;
procedure P$DO_INSERT_DECL_CONTR;
procedure P$DO_UPDATE_DECL_CONTR;

END PAC$DECL_MATCHING_CONTRAGENT;
/

CREATE OR REPLACE PACKAGE BODY  NDS2_MRR_USER."PAC$DECL_MATCHING_CONTRAGENT" IS

procedure START_WORK
  as
begin
  NDS2$SYS.LOG_INFO(
  P_SITE => 'PAC$DECL_MATCHING_CONTRAGENT.START_WORK',   P_ENTITY_ID => null,
  P_MESSAGE_CODE => SQLCODE,   P_MESSAGE => 'starting');
  begin
    P$DO_INSERT_DECL_CONTR;
    P$DO_UPDATE_DECL_CONTR;
  exception when others then null;
  end;
end;

procedure P$DO_INSERT_DECL_CONTR
as
    pInsertDateBegin DATE;
    pInsertDateEnd DATE;
    pParameter VARCHAR2(128);
    pDescription VARCHAR2(128);
begin
    pParameter := 'seod_decl_key_last_processed';
    pDescription := 'дата последнего запуска процесса создания ключей контрагента';
    pInsertDateEnd := sysdate;

    select to_date(value, 'DD/MM/YYYY') into pInsertDateBegin FROM dual
       left outer join (select value from CONFIGURATION where parameter = pParameter) r on 1 = 1;

    if pInsertDateBegin is null then
        select insert_date into pInsertDateBegin FROM dual
           left outer join
           (select insert_date from (select insert_date, rownum as row_number from SEOD_DECLARATION order by insert_date)
            where row_number = 1) r on 1 = 1;
       if  pInsertDateBegin is null then
         pInsertDateBegin := sysdate - 2;
       end if;
    end if;

    INSERT INTO DECL_MATCHING_CONTRAGENT dmc (ID, ZIP, IS_ASK, KEY_DECL_CONTR, SEOD_DECL_ID, insert_date)
    SELECT NDS2_MRR_USER.SEQ_DECL_MATCHING_CONTRAGENT.NEXTVAL
           ,null as ZIP
           ,0 as IS_ASK
           ,(to_number(sed.fiscal_year) * 100000000000000 + tm.month * 1000000000000 + to_number(sed.inn)) as key_decl_contr
           ,sed.ID as SEOD_DECL_ID
           ,sysdate
    FROM SEOD_DECLARATION sed
    JOIN DICT_TAX_PERIOD_MONTH tm on tm.tax_period = sed.tax_period
    WHERE INSERT_DATE >= pInsertDateBegin;

    merge into CONFIGURATION c
    using (select pParameter as parameter from dual) par
      on
      (
        par.parameter = c.parameter
      )
    when matched then
      update set c.value = to_char(pInsertDateEnd, 'DD/MM/YYYY'), c.default_value = to_char(pInsertDateEnd, 'DD/MM/YYYY')
    when not matched then
      insert (parameter, value, default_value, description)
      values (pParameter, to_char(pInsertDateEnd, 'DD/MM/YYYY'), to_char(pInsertDateEnd, 'DD/MM/YYYY'), pDescription);

    commit;
end;

procedure P$DO_UPDATE_DECL_CONTR
as
begin
  update DECL_MATCHING_CONTRAGENT set (ZIP, IS_ASK, update_Date) =
  (
    select sd.ZIP, 1, sysdate
    from DECL_MATCHING_CONTRAGENT dmc
    join SEOD_DECLARATION sed on sed.ID = dmc.SEOD_DECL_ID
    join V$ASK_DECLANDJRNL sd on SD.PERIOD = SED.TAX_PERIOD and SD.OTCHETGOD = SED.FISCAL_YEAR and SD.INNNP = SED.INN and SD.NOMKORR = SED.CORRECTION_NUMBER
    where IS_ASK = 0 and dmc.id = DECL_MATCHING_CONTRAGENT.ID
  )
  WHERE EXISTS (select sd.ZIP, 1
    from DECL_MATCHING_CONTRAGENT dmc
    join SEOD_DECLARATION sed on sed.ID = dmc.SEOD_DECL_ID
    join V$ASK_DECLANDJRNL sd on SD.PERIOD = SED.TAX_PERIOD and SD.OTCHETGOD = SED.FISCAL_YEAR and SD.INNNP = SED.INN and SD.NOMKORR = SED.CORRECTION_NUMBER
    where IS_ASK = 0 and dmc.id = DECL_MATCHING_CONTRAGENT.ID);  
  commit;
end;

END "PAC$DECL_MATCHING_CONTRAGENT";
/