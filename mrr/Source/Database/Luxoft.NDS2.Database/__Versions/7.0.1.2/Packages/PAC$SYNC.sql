﻿create or replace package NDS2_MRR_USER.PAC$SYNC
as
  procedure START_WORK;
end;
/
create or replace package body NDS2_MRR_USER.PAC$SYNC
as

procedure RECOMPILE_INVALIDS
as
begin
     for line in (select owner, object_name, decode(object_type, 'PACKAGE BODY', 'PACKAGE', object_type) as object_type from all_objects where status = 'INVALID' and owner = 'NDS2_MRR_USER') loop
        begin     
            execute immediate 'alter '||line.object_type||' '||line.owner||'."'||line.object_name||'" compile';
        exception when others then null;
        end;
    end loop;
end;

procedure DROP_IF_EXIST
(
  p_table_name varchar2
)
as
begin
  for line in (select * from all_tables where owner = 'NDS2_MRR_USER' and table_name = p_table_name)
  loop
    execute immediate 'truncate table NDS2_MRR_USER.'||p_table_name;
    execute immediate 'drop table NDS2_MRR_USER.'||p_table_name||' purge';
  end loop;
  exception when others then null;
end;


procedure SYNC_EGRN_IP as
begin
  DROP_IF_EXIST('STAGE_EGRN_IP');
  execute immediate 'create table NDS2_MRR_USER.STAGE_EGRN_IP as select * from V_EGRN_IP@NDS2_FCOD';
  DROP_IF_EXIST('V_EGRN_IP');
  execute immediate 'create table NDS2_MRR_USER.V_EGRN_IP as (select * from (select ip.*, row_number() over (partition by innfl order by id desc) is_actual from STAGE_EGRN_IP ip ) T where T.is_actual = 1)';
  execute immediate 'create index NDS2_MRR_USER.idx_egrnip_inn on NDS2_MRR_USER.V_EGRN_IP(innfl)';
  commit;
  NDS2$SYS.LOG_INFO('PAC$SYNC.SYNC_EGRN_IP', '0', '0', 'success');
  execute immediate 'truncate table NDS2_MRR_USER.STAGE_EGRN_IP';
  exception when others then
  begin
    rollback;
    NDS2$SYS.LOG_ERROR('PAC$SYNC.SYNC_EGRUL_IP', null, null, SUBSTR(SQLERRM, 1 , 2040));
  end;
end;

procedure SYNC_EGRN_UL as
begin
  DROP_IF_EXIST('STAGE_EGRN_UL');
  execute immediate 'create table NDS2_MRR_USER.STAGE_EGRN_UL as select * from V_EGRN_UL@NDS2_FCOD';
  DROP_IF_EXIST('V_EGRN_UL');
  execute immediate 'create table NDS2_MRR_USER.V_EGRN_UL as (select * from (select ul.*, row_number() over (partition by inn order by id desc) is_actual from STAGE_EGRN_UL ul ) T where T.is_actual = 1)';
  execute immediate 'create index NDS2_MRR_USER.idx_egrnul_inn on NDS2_MRR_USER.V_EGRN_UL(inn)';
  commit;
  NDS2$SYS.LOG_INFO('PAC$SYNC.SYNC_EGRN_UL', '0', '0', 'success');
  execute immediate 'truncate table NDS2_MRR_USER.STAGE_EGRN_UL';
  exception when others then
  begin
    rollback;
    NDS2$SYS.LOG_ERROR('PAC$SYNC.SYNC_EGRUL_UL', null, null, SUBSTR(SQLERRM, 1 , 2040));
  end;
end;

procedure SYNC_SUR as
begin
  DROP_IF_EXIST('STAGE_SUR');
  execute immediate 'create table NDS2_MRR_USER.STAGE_SUR as (select inn, pyear as fiscal_year, nperiod as fiscal_period, pr as sign_code, dpr  as update_date from ASKNDS1PR@NDS2_FCOD)';
  DROP_IF_EXIST('EXT_SUR');
  execute immediate 'create table NDS2_MRR_USER.EXT_SUR as (select * from (select ul.*, row_number() over (partition by inn, fiscal_year, fiscal_period order by update_date desc) is_actual from STAGE_SUR ul ) T where T.is_actual = 1)';
  execute immediate 'create index NDS2_MRR_USER.idx_sur_inn_y_p on NDS2_MRR_USER.EXT_SUR(inn, fiscal_year, fiscal_period)';
  commit;
  NDS2$SYS.LOG_INFO('PAC$SYNC.SYNC_SUR', '0', '0', 'success');
  execute immediate 'truncate table NDS2_MRR_USER.STAGE_SUR';

  exception when others then
  begin
    rollback;
    NDS2$SYS.LOG_ERROR('PAC$SYNC.SYNC_SUR', null, null, SUBSTR(SQLERRM, 1 , 2040));
  end;
end;

procedure SYNC_BSSCHET_IP as
begin
  DROP_IF_EXIST('STAGE_BSSCHET_IP');
  execute immediate 'create table NDS2_MRR_USER.STAGE_BSSCHET_IP as select * from BSSCHET_IP@NDS2_FCOD';
  DROP_IF_EXIST('BSSCHET_IP');
  execute immediate 'create table NDS2_MRR_USER.BSSCHET_IP as select * from NDS2_MRR_USER.STAGE_BSSCHET_IP';
  execute immediate 'create index NDS2_MRR_USER.idx_bsschip_inn on NDS2_MRR_USER.BSSCHET_IP(inn)';
  commit;
  NDS2$SYS.LOG_INFO('PAC$SYNC.SYNC_BSSCHET_IP', '0', '0', 'success');
  execute immediate 'truncate table NDS2_MRR_USER.STAGE_BSSCHET_IP';

  exception when others then
  begin
    rollback;
    NDS2$SYS.LOG_ERROR('PAC$SYNC.SYNC_BSSCHET_IP', null, null, SUBSTR(SQLERRM, 1 , 2040));
  end;
end;

procedure SYNC_BSSCHET_UL as
begin
  DROP_IF_EXIST('STAGE_BSSCHET_UL');
  execute immediate 'create table NDS2_MRR_USER.STAGE_BSSCHET_UL as select * from BSSCHET_UL@NDS2_FCOD';
  DROP_IF_EXIST('BSSCHET_UL');
  execute immediate 'create table NDS2_MRR_USER.BSSCHET_UL as select * from NDS2_MRR_USER.STAGE_BSSCHET_UL';
  execute immediate 'create index NDS2_MRR_USER.idx_bsschul_inn on NDS2_MRR_USER.BSSCHET_UL(inn)';
  commit;
  NDS2$SYS.LOG_INFO('PAC$SYNC.SYNC_BSSCHET_UL', '0', '0', 'success');
  execute immediate 'truncate table NDS2_MRR_USER.STAGE_BSSCHET_UL';

  exception when others then
  begin
    rollback;
    NDS2$SYS.LOG_ERROR('PAC$SYNC.SYNC_BSSCHET_UL', null, null, SUBSTR(SQLERRM, 1 , 2040));
  end;
end;

procedure SYNC_SONO as
begin
  merge into ext_sono trg using (select * from v$sono_alias where ActualSign = 1) src
  on (src.s_code = trg.s_code)
  when matched then
  update set
    trg.s_name = src.s_name
  when not matched then
  insert (
    trg.s_code,
    trg.s_name
  ) values (
    src.s_code,
    src.s_name
  );
  commit;
  exception when others then
  begin
    rollback;
    NDS2$SYS.LOG_ERROR('PAC$SYNC.SYNC_SONO', null, null, SUBSTR(SQLERRM, 1 , 2040));
  end;
end;

procedure SYNC_SSRF as
begin
  merge into ext_ssrf trg using (select * from v$ssrf_alias where ActualSign = 1) src
  on (src.s_code = trg.s_code)
  when matched then
  update set
    trg.s_name = src.s_name
  when not matched then
  insert (
    trg.s_code,
    trg.s_name
  ) values (
    src.s_code,
    src.s_name
  );
  commit;
  exception when others then
  begin
    rollback;
    NDS2$SYS.LOG_ERROR('PAC$SYNC.SYNC_SSRF', null, null, SUBSTR(SQLERRM, 1 , 2040));
  end;
end;


procedure SYNC_OKVED as
begin
  merge into ext_okved trg using (select * from v$okved_alias where ActualSign = 1) src
  on (src.s_code = trg.s_code)
  when matched then
  update set
    trg.s_name = src.s_name
  when not matched then
  insert (
    trg.s_code,
    trg.s_name
  ) values (
    src.s_code,
    src.s_name
  );
  commit;
  exception when others then
  begin
    rollback;
    NDS2$SYS.LOG_ERROR('PAC$SYNC.SYNC_OKVED', null, null, SUBSTR(SQLERRM, 1 , 2040));
  end;
end;


procedure START_WORK as
begin
  SYNC_SONO;
  SYNC_SSRF;
  SYNC_OKVED;
  SYNC_SUR;
  SYNC_BSSCHET_IP;
  SYNC_BSSCHET_UL;
  SYNC_EGRN_UL;
  SYNC_EGRN_IP;
  RECOMPILE_INVALIDS;
end;
end;
/
