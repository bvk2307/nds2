﻿CREATE OR REPLACE FUNCTION NDS2_MRR_USER.F$GET_EFFECTIVE_KPP(P_INN VARCHAR2, P_KPP VARCHAR2) RETURN VARCHAR2 AS
  V_KPP_EFFECTIVE VARCHAR2(9 CHAR);
  V_CODE NUMBER;
BEGIN
  IF P_KPP = '111111111' THEN RETURN P_KPP; END IF;

  select
    decode(translate(substr(P_KPP, 5, 2), '_0123456789', '_'), null, to_number(substr(P_KPP, 5, 2)), 0)
  into
    V_CODE
  from dual;

  IF V_CODE < 51 THEN
    RETURN '111111111';
  END IF;

  select
    case when kpp_exl.inn is null then nvl(P_KPP, '111111111') else '111111111' end
  into
    V_KPP_EFFECTIVE
  from dual
  left join EFFECTIVE_KPP_EXCLUSION kpp_exl on kpp_exl.inn = P_INN;

  RETURN V_KPP_EFFECTIVE;
END;
/
