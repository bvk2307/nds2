﻿create or replace 
TYPE NDS2_MRR_USER.T$HRZ_TREE_KEY as object (
  INN                    VARCHAR2(12 CHAR),
  KPP                    VARCHAR2(12 CHAR)  
);
/