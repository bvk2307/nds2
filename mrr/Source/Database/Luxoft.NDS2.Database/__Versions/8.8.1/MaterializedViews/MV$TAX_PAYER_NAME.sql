﻿begin
  execute immediate 'drop materialized view NDS2_MRR_USER.MV$TAX_PAYER_NAME';
  exception when others then null;
end;
/

CREATE MATERIALIZED VIEW NDS2_MRR_USER.MV$TAX_PAYER_NAME
nologging
pctfree 0
build DEFERRED 
refresh force on demand
AS 
select
  ip.innfl as INN,
  nvl(ip.last_name, '')||' '||nvl(ip.first_name, '')||' '||nvl(ip.patronymic, '') as NP_NAME,
  0 as TP_TYPE,
  '' as KPP,
  '111111111' as KPP_EFFECTIVE,
  adr as address,
  ip.ogrnip as OGRN,
  null as ust_capital,
  ip.date_birth as date_reg,
  ip.date_on,
  ip.date_off,
  nvl(ip.last_name, '')||' '||nvl(ip.first_name, '')||' '||nvl(ip.patronymic, '') as name_full,
  ip.code_no as sono_code
from
  v_egrn_ip ip
union all
select
  ul.inn,
  ul.short_name as NP_NAME,
  1 as TP_TYPE,
  ul.KPP,
  NDS2_MRR_USER.F$GET_EFFECTIVE_KPP(ul.inn, ul.KPP) as KPP_EFFECTIVE,
  ul.adr as address,
  ul.ogrn,
  ul.ust_capital,
  ul.date_reg,
  ul.date_on,
  ul.date_off,
  ul.name_full,
  ul.code_no as sono_code
from
  v_egrn_ul ul;

create index NDS2_MRR_USER.idx_mvtpname_inn on NDS2_MRR_USER.MV$TAX_PAYER_NAME(inn);
