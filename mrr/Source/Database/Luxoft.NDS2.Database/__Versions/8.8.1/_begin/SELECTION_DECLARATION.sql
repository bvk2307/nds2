﻿create table NDS2_MRR_USER.SELECTION_DECLARATION_MGR compress basic as
select
  cast(sd.IS_IN_PROCESS as NUMBER(1)) as IS_IN_PROCESS,
  cast(sd.SELECTION_ID as NUMBER) as SELECTION_ID,
  cast(d.DECLARATION_VERSION_ID as NUMBER) as ZIP,
  cast(d.decl_type_code as NUMBER(1)) as TYPE_CODE,
  cast(d.sur_code as NUMBER(1)) as SUR_CODE,
  cast(nds2_mrr_user.pac$migration.F$GET_INN(sd.declaration_id) as varchar2(12)) as INN,
  cast(d.kpp as VARCHAR2(9 char)) as KPP,
  cast(d.name as VARCHAR2(1000 char)) as NAME,
  cast(d.DECL_SIGN as VARCHAR2(23 char)) as sign,
  cast(d.COMPENSATION_AMNT as NUMBER) as nds_total,
  cast(d.TOTAL_DISCREP_COUNT as NUMBER) as discrepancy_qty,
  cast(d.DISCREP_BUY_BOOK_AMNT as NUMBER(21,2)) as ch8_discrepancy_amt,
  cast(d.DISCREP_SELL_BOOK_AMNT as NUMBER(21,2)) as CH9_CH12_DISCREPANCY_AMT,
  cast(d.DISCREP_TOTAL_AMNT as NUMBER(21,2)) as discrepancy_amt,
  cast(d.DISCREP_MIN_AMNT as NUMBER(21,2)) as discrepancy_amt_min,
  cast(d.DISCREP_MAX_AMNT as NUMBER(21,2)) as discrepancy_amt_max,
  cast(d.DISCREP_AVG_AMNT as NUMBER(21,2)) as discrepancy_amt_avg,
  cast(d.PVP_TOTAL_AMNT as NUMBER(21,2)) as pvp,
  cast(d.PVP_DISCREP_MIN_AMNT as NUMBER(21,2)) as pvp_min,
  cast(d.PVP_DISCREP_MAX_AMNT as NUMBER(21,2)) as pvp_max,
  cast(d.PVP_DISCREP_AVG_AMNT as NUMBER(21,2)) as pvp_avg,
  cast(d.PVP_BUY_BOOK_AMNT as NUMBER(21,2)) as ch8_pvp,
  cast(d.PVP_SELL_BOOK_AMNT as NUMBER(21,2)) as CH9_CH12_PVP,
  cast(nds2_mrr_user.pac$migration.F$GET_PERIOD_CODE(sd.declaration_id) as VARCHAR2(2)) as PERIOD_CODE,
  cast(d.correction_number as VARCHAR2(100 char)) as CORRECTION_NUMBER_EFFECTIVE,
  cast(case d.CATEGORY when 1 then 'Да' else '' end as VARCHAR2(2 CHAR)) as IS_LARGE,
  cast(d.decl_date as DATE) as SUBMIT_DATE,
  cast(d.soun_code as VARCHAR2(4)) as SONO_CODE,
  cast(d.region_code as VARCHAR2(2)) as REGION_CODE,
  cast(NULL as VARCHAR2(12)) as INN_REORGANIZED
from nds2_mrr_user.selection_declaration sd
left join nds2_mrr_user.declaration_active d on d.id = sd.declaration_id;

ALTER TABLE NDS2_MRR_USER.SELECTION_DECLARATION_MGR MODIFY (ZIP NOT NULL);
ALTER TABLE NDS2_MRR_USER.SELECTION_DECLARATION_MGR MODIFY (TYPE_CODE NOT NULL);

create index NDS2_MRR_USER.IDX_SELDECL_ZIP on NDS2_MRR_USER.SELECTION_DECLARATION_MGR (ZIP);
create index NDS2_MRR_USER.IDX_SELDECL_ZIPINPROC on NDS2_MRR_USER.SELECTION_DECLARATION_MGR (ZIP, IS_IN_PROCESS);
create index NDS2_MRR_USER.IDX_SELDECL_INPROC on NDS2_MRR_USER.SELECTION_DECLARATION_MGR (IS_IN_PROCESS);
create index NDS2_MRR_USER.IDX_SELDECL_ID on NDS2_MRR_USER.SELECTION_DECLARATION_MGR (SELECTION_ID);
create index NDS2_MRR_USER.IDX_SELDECL_SELIDINPROC on NDS2_MRR_USER.SELECTION_DECLARATION_MGR (SELECTION_ID, IS_IN_PROCESS);

drop table NDS2_MRR_USER.SELECTION_DECLARATION;
alter table NDS2_MRR_USER.SELECTION_DECLARATION_MGR rename to SELECTION_DECLARATION;
