﻿create or replace package NDS2_MRR_USER.PAC$MIGRATION is

function F$GET_INN(P_DECL_ID NUMBER) return varchar2;

function F$GET_PERIOD_CODE(P_DECL_ID NUMBER) return varchar2;

function F$GET_YEAR(P_DECL_ID NUMBER) return varchar2;

function F$GET_KPP_CALCULATED return varchar2;

end PAC$MIGRATION;
/
create or replace package body NDS2_MRR_USER.PAC$MIGRATION is

function F$GET_INN(P_DECL_ID NUMBER) return varchar2
is
begin
  return case LENGTH(TO_CHAR(P_DECL_ID))
    when 15 then SUBSTR(TO_CHAR(P_DECL_ID), 6)
    when 16 then SUBSTR(TO_CHAR(P_DECL_ID), 7)
    when 17 then SUBSTR(TO_CHAR(P_DECL_ID), 6)
    when 18 then SUBSTR(TO_CHAR(P_DECL_ID), 7)
    else NULL
  end;
end;

function F$GET_PERIOD_CODE(P_DECL_ID NUMBER) return varchar2
is
begin
  return case LENGTH(TO_CHAR(P_DECL_ID))
    when 15 then '0'||SUBSTR(TO_CHAR(P_DECL_ID), 1, 1)
    when 16 then SUBSTR(TO_CHAR(P_DECL_ID), 1, 2)
    when 17 then '0'||SUBSTR(TO_CHAR(P_DECL_ID), 1, 1)
    when 18 then SUBSTR(TO_CHAR(P_DECL_ID), 1, 2)
    else NULL
  end;
end;

function F$GET_YEAR(P_DECL_ID NUMBER) return varchar2
is
begin
  return case LENGTH(TO_CHAR(P_DECL_ID))
    when 15 then SUBSTR(TO_CHAR(P_DECL_ID), 2, 4)
    when 16 then SUBSTR(TO_CHAR(P_DECL_ID), 3, 4)
    when 17 then SUBSTR(TO_CHAR(P_DECL_ID), 2, 4)
    when 18 then SUBSTR(TO_CHAR(P_DECL_ID), 3, 4)
    else NULL
  end;
end;

function F$GET_KPP_CALCULATED return varchar2
is
begin
  return '111111111';
end;


end PAC$MIGRATION;
/
