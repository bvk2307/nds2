﻿create table NDS2_MRR_USER.DECLARATION_OWNER_NEW as
select
  da.type_code
  ,da.inn_declarant
  ,da.kpp_effective
  ,da.fiscal_year
  ,da.period_code
  ,do.inspector_sid
  ,do.inspector_name
from
(select
  substr(do.declaration_id, 1, 2) as period_code,
  substr(do.declaration_id, 3, 4) as fiscal_year,
  substr(do.declaration_id, 7, 13) as inn,
  do.INSPECTOR_SID,
  do.INSPECTOR_NAME
from nds2_mrr_user.declaration_owner do) do
join nds2_mrr_user.declaration_active da on da.inn_declarant = do.inn and da.period_code = do.period_code and da.fiscal_year = do.fiscal_year;

drop table NDS2_MRR_USER.DECLARATION_OWNER;
alter table NDS2_MRR_USER.DECLARATION_OWNER_NEW rename to DECLARATION_OWNER;
