﻿begin
  execute immediate 'drop table NDS2_MRR_USER.KNP_DECLARATION_SUMMARY';
  exception when others then null;
end;
/

create table NDS2_MRR_USER.KNP_DECLARATION_SUMMARY
(
  inn                     VARCHAR2(12 CHAR),
  kpp_effective           VARCHAR2(9 CHAR),
  type_code               NUMBER,
  period_code             VARCHAR2(2 CHAR),
  fiscal_year             VARCHAR2(4 CHAR),
  gap_amount_8            NUMBER,
  gap_amount_9            NUMBER,
  gap_amount_10           NUMBER,
  gap_amount_11           NUMBER,
  gap_amount_12           NUMBER,
  other_amount_8          NUMBER,
  other_amount_9          NUMBER,
  other_amount_10         NUMBER,
  other_amount_11         NUMBER,
  other_amount_12         NUMBER,
  gap_count_8             NUMBER,
  gap_count_9             NUMBER,
  gap_count_10            NUMBER,
  gap_count_11            NUMBER,
  gap_count_12            NUMBER,
  other_count_8           NUMBER,
  other_count_9           NUMBER,
  other_count_10          NUMBER,
  other_count_11          NUMBER,
  other_count_12          NUMBER,
  total_discrepancy_count NUMBER
);