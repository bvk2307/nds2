﻿create table NDS2_MRR_USER.REORG_TMP
(
  inn_successor             VARCHAR2(12 CHAR),
  kpp_effective_successor   VARCHAR2(9 CHAR),
  inn_reorganized           VARCHAR2(12 CHAR),
  kpp_effective_reorganized VARCHAR2(9 CHAR),
  insert_date               DATE,
  sono_code                 VARCHAR2(4 CHAR),
  kpp_successor             VARCHAR2(9 CHAR),
  name_successor            VARCHAR2(1024 CHAR),
  rn                        NUMBER
);
