﻿begin
	execute immediate 'drop table NDS2_MRR_USER.DROPPED_MARKS';
	exception when others then null;
end;
/
	
create table NDS2_MRR_USER.DROPPED_MARKS
(
  TYPE_CODE NUMBER,
  INN_CONTRACTOR VARCHAR2(12 char),
  KPP_EFFECTIVE VARCHAR2(10 char),
  FISCAL_YEAR VARCHAR2(4 char),
  PERIOD_CODE VARCHAR2(2 char),
  DROPPED      NUMBER,
  DROP_DATE    DATE
);
