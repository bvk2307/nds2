﻿begin
	execute immediate 'drop table NDS2_MRR_USER.DECLARATION_HISTORY';
	exception when others then null;
end;
/
	
create table NDS2_MRR_USER.DECLARATION_HISTORY
(
  zip               NUMBER,
  ask_id            NUMBER,
  reg_number        NUMBER(20),
  inn_contractor    VARCHAR2(12 CHAR),
  inn_declarant     VARCHAR2(12 CHAR),
  inn_reorganized   VARCHAR2(10 CHAR),
  kpp               VARCHAR2(9 CHAR),
  kpp_effective     VARCHAR2(9 CHAR),
  fiscal_year       VARCHAR2(4 CHAR),
  period_code       VARCHAR2(2 CHAR),
  period_effective  VARCHAR2(2 CHAR),
  is_active         VARCHAR2(4 CHAR),
  type_code         NUMBER,
  sur_code          CHAR(1 CHAR),
  sign              VARCHAR2(1 CHAR),
  nds_total         NUMBER,
  correction_number NUMBER,
  correction_number_rank NUMBER,
  correction_number_effective VARCHAR2(100 CHAR),
  sono_code         VARCHAR2(4 CHAR),
  sono_code_submited VARCHAR2(4 CHAR),
  region_code       VARCHAR2(2 CHAR),
  submit_date       DATE,
  is_large          NUMBER
);
