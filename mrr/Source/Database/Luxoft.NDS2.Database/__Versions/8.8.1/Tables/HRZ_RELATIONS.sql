﻿-- Create table
drop table NDS2_MRR_USER.HRZ_RELATIONS;

create table NDS2_MRR_USER.HRZ_RELATIONS
(
  inn_1             VARCHAR2(12 CHAR),
  kpp_1_effective   VARCHAR2(9 CHAR) NOT NULL,
  inn_2             VARCHAR2(12 CHAR),
  kpp_2_effective   VARCHAR2(9 CHAR) NOT NULL,
  year              NUMBER(4),
  quarter           NUMBER(1),
  decl_quarter_id   NUMBER,
  chapter8_amount   NUMBER,
  chapter9_amount   NUMBER,
  chapter10_amount  NUMBER,
  chapter11_amount  NUMBER,
  chapter12_amount  NUMBER,
  chapter8_count    NUMBER,
  chapter9_count    NUMBER,
  chapter10_count   NUMBER,
  chapter11_count   NUMBER,
  chapter12_count   NUMBER,
  nds_chapter9      NUMBER,
  nds_chapter12     NUMBER,
  sa                NUMBER,
  ba                NUMBER,
  nba               NUMBER,
  nsa               NUMBER,
  njsa              NUMBER,
  njba              NUMBER,
  gap_amount        NUMBER,
  nds_amount        NUMBER,
  before2015_amount NUMBER,
  after2015_amount  NUMBER,
  deduction_nds     NUMBER,
  calc_nds          NUMBER,
  total_nds         NUMBER,
  gap_discrep_amnt  NUMBER(21,2),
  nba_order         NUMBER,
  nsa_order         NUMBER,
  soun_code         VARCHAR2(4),
  soun              VARCHAR2(600 CHAR),
  region_code       VARCHAR2(2),
  region            VARCHAR2(600 CHAR),
  own_relation      NUMBER(1),
  nds_discrep_amnt  NUMBER(21,2)
)
tablespace NDS2_DATA
compress
nologging;

-- Create/Recreate indexes 
create index NDS2_MRR_USER.IX_RELATIONS_BA on NDS2_MRR_USER.HRZ_RELATIONS (YEAR, QUARTER, INN_1, KPP_1_EFFECTIVE, NBA_ORDER)
  tablespace NDS2_IDX
  compress;
  
create index NDS2_MRR_USER.IX_RELATIONS_CH10_AMNT on NDS2_MRR_USER.HRZ_RELATIONS (YEAR, QUARTER, INN_1, KPP_1_EFFECTIVE, CHAPTER10_AMOUNT)
  tablespace NDS2_IDX
  compress;
  
create index NDS2_MRR_USER.IX_RELATIONS_CH11_AMNT on NDS2_MRR_USER.HRZ_RELATIONS (YEAR, QUARTER, INN_1, KPP_1_EFFECTIVE, CHAPTER11_AMOUNT)
  tablespace NDS2_IDX
  compress;
  
create index NDS2_MRR_USER.IX_RELATIONS_CH12_AMNT on NDS2_MRR_USER.HRZ_RELATIONS (YEAR, QUARTER, INN_1, KPP_1_EFFECTIVE, CHAPTER12_AMOUNT)
  tablespace NDS2_IDX
  compress;
  
create index NDS2_MRR_USER.IX_RELATIONS_CH8_AMNT on NDS2_MRR_USER.HRZ_RELATIONS (YEAR, QUARTER, INN_1, KPP_1_EFFECTIVE, CHAPTER8_AMOUNT)
  tablespace NDS2_IDX
  compress;
  
create index NDS2_MRR_USER.IX_RELATIONS_CH9_AMNT on NDS2_MRR_USER.HRZ_RELATIONS (YEAR, QUARTER, INN_1, KPP_1_EFFECTIVE, CHAPTER9_AMOUNT)
  tablespace NDS2_IDX
  compress;
  
create index NDS2_MRR_USER.IX_RELATIONS_NDS_CH12 on NDS2_MRR_USER.HRZ_RELATIONS (YEAR, QUARTER, INN_1, KPP_1_EFFECTIVE, NDS_CHAPTER12)
  tablespace NDS2_IDX
  compress;
  
create index NDS2_MRR_USER.IX_RELATIONS_NDS_CH9 on NDS2_MRR_USER.HRZ_RELATIONS (YEAR, QUARTER, INN_1, KPP_1_EFFECTIVE, NDS_CHAPTER9)
  tablespace NDS2_DATA 
  compress;
  
create index NDS2_MRR_USER.IX_RELATIONS_REV on NDS2_MRR_USER.HRZ_RELATIONS (YEAR, QUARTER, INN_1, KPP_1_EFFECTIVE, INN_2, KPP_2_EFFECTIVE)
  tablespace NDS2_IDX
  compress;
  
create index NDS2_MRR_USER.IX_RELATIONS_SA on NDS2_MRR_USER.HRZ_RELATIONS (YEAR, QUARTER, INN_1, KPP_1_EFFECTIVE, NSA_ORDER)
  tablespace NDS2_IDX
  compress;

ALTER TABLE "NDS2_MRR_USER"."HRZ_RELATIONS" ADD "REV_NBA_ORDER" NUMBER;
