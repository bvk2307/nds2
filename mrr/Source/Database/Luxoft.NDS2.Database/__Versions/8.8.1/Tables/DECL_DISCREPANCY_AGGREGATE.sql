﻿-- Create table
begin
	execute immediate 'drop table NDS2_MRR_USER.MV$DISCREPANCY_DECL_LIST';
		exception when others then null;
end;
/

begin
	execute immediate 'drop table NDS2_MRR_USER.DECL_DISCREPANCY_AGGREGATE';
		exception when others then null;
end;
/
create table NDS2_MRR_USER.DECL_DISCREPANCY_AGGREGATE
(
  id                             NUMBER,
  founddate                      DATE,
  invoice_id                     VARCHAR2(128),
  contractor_invoice_id          VARCHAR2(128),
  typecode                       NUMBER(1),
  zip                            NUMBER NOT NULL,
  contractor_zip                 NUMBER,
  invoice_chapter_src            NUMBER(2),
  taxpayerdeclsign               VARCHAR2(23),
  contractorinn                  VARCHAR2(12 CHAR),
  contractorkpp                  VARCHAR2(9 CHAR),
  contractorname                 VARCHAR2(3000),
  invoice_contractor_chapter_src NUMBER(2),
  contractordeclsign             VARCHAR2(23),
  amount                         NUMBER(20,2),
  amountpvp                      NUMBER(20,2),
  statuscode                     NUMBER(2),
  contractordoctype              VARCHAR2(20),
  sum_nds                        NUMBER,
  inn                            VARCHAR2(12 CHAR) NOT NULL,
  kpp_effective                  VARCHAR2(9 CHAR) NOT NULL,
  side_primary_processing        NUMBER,
  inn_reorganized                VARCHAR2 (12),
  fiscal_year                    VARCHAR2(4) NOT NULL,
  period_code                    VARCHAR2(2) NOT NULL,
  contractor_inn_reorg           VARCHAR2(12)
);


-- Create/Recreate indexes 
create index NDS2_MRR_USER.IDX_DD_ZIP on NDS2_MRR_USER.DECL_DISCREPANCY_AGGREGATE (ZIP) tablespace NDS2_IDX compress;

create index NDS2_MRR_USER.IDX_DD_CONTRACTOR_ZIP on NDS2_MRR_USER.DECL_DISCREPANCY_AGGREGATE (CONTRACTOR_ZIP) tablespace NDS2_IDX compress;

create index NDS2_MRR_USER.IDX_DD_DECL_ID on NDS2_MRR_USER.DECL_DISCREPANCY_AGGREGATE (INN,KPP_EFFECTIVE,FISCAL_YEAR,PERIOD_CODE) tablespace NDS2_IDX compress;
