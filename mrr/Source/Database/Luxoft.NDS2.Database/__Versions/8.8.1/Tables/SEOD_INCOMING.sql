﻿CREATE TABLE "NDS2_MRR_USER"."SEOD_INCOMING" 
   (	
       "INFO_TYPE" VARCHAR2(11 BYTE), 
       "INFO_TYPE_NAME" VARCHAR2(100 CHAR), 
       "DATE_EOD" DATE, 
       "DATE_RECEIPT" DATE, 
       "CODE_NSI_SONO" VARCHAR2(4 CHAR), 
	   "REG_NUMBER" NUMBER(38,0) NOT NULL ENABLE, 
       "XML_DATA" CLOB, 
       "ID" NUMBER(38,0) NOT NULL ENABLE, 
       "PROCESSING_RESULT" NUMBER,
       "PROCESSING_RESULT_TEXT"  VARCHAR2(4000), 
	 CONSTRAINT "SEOD_INCOMING_PK" PRIMARY KEY ("ID") ENABLE
   );
 
COMMENT ON TABLE "NDS2_MRR_USER"."SEOD_INCOMING"  IS 'Таблица для хранения данных поступающих из СЭОД.';
COMMENT ON COLUMN "NDS2_MRR_USER"."SEOD_INCOMING"."INFO_TYPE" IS 'Тип входящего потока. Определяет формат файла обмена данными.';
COMMENT ON COLUMN "NDS2_MRR_USER"."SEOD_INCOMING"."CODE_NSI_SONO" IS 'Код ТНО из справочника налоговых органов';
COMMENT ON COLUMN "NDS2_MRR_USER"."SEOD_INCOMING"."XML_DATA" IS 'Содержимое Xml файла обмена данными.';
COMMENT ON COLUMN "NDS2_MRR_USER"."SEOD_INCOMING"."ID" IS 'Идентификатор поступившей записи. Значения определяются последовательностью "NDS2_MRR_USER".""SEQ_SEOD_INCOMING"';
COMMENT ON COLUMN "NDS2_MRR_USER"."SEOD_INCOMING"."PROCESSING_RESULT" IS '1 - запись успешно обработана. 0 - при обработке записи произошла ошибка. NULL запись еще не обработана';
COMMENT ON COLUMN "NDS2_MRR_USER"."SEOD_INCOMING"."PROCESSING_RESULT_TEXT" IS 'Идентификатор поступившей записи. Значения определяются последовательностью "NDS2_MRR_USER"."SEOD_INCOME_SEQ"';