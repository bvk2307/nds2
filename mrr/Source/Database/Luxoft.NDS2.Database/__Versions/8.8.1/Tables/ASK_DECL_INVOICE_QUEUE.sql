﻿alter table NDS2_MRR_USER.ASK_INVOICE_DECL_QUEUE add ZIP NUMBER;

alter table NDS2_MRR_USER.ASK_INVOICE_DECL_QUEUE add INN VARCHAR2(12);
alter table NDS2_MRR_USER.ASK_INVOICE_DECL_QUEUE add KPP_EFFECTIVE VARCHAR2(9);
alter table NDS2_MRR_USER.ASK_INVOICE_DECL_QUEUE add PERIOD_CODE VARCHAR2(2);
alter table NDS2_MRR_USER.ASK_INVOICE_DECL_QUEUE add YEAR VARCHAR2(4);


update NDS2_MRR_USER.ASK_INVOICE_DECL_QUEUE set
  INN = NDS2_MRR_USER.PAC$MIGRATION.F$GET_INN(DECL_ID),
  KPP_EFFECTIVE = NDS2_MRR_USER.PAC$MIGRATION.F$GET_KPP_CALCULATED,
  PERIOD_CODE = NDS2_MRR_USER.PAC$MIGRATION.F$GET_PERIOD_CODE(DECL_ID),
  YEAR = NDS2_MRR_USER.PAC$MIGRATION.F$GET_YEAR(DECL_ID);
commit;


alter table NDS2_MRR_USER.ASK_INVOICE_DECL_QUEUE modify (INN NOT NULL);
alter table NDS2_MRR_USER.ASK_INVOICE_DECL_QUEUE modify (KPP_EFFECTIVE NOT NULL);
alter table NDS2_MRR_USER.ASK_INVOICE_DECL_QUEUE modify (PERIOD_CODE NOT NULL);
alter table NDS2_MRR_USER.ASK_INVOICE_DECL_QUEUE modify (YEAR NOT NULL);

alter table NDS2_MRR_USER.ASK_INVOICE_DECL_QUEUE drop column DECL_ID;