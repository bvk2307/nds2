﻿create table NDS2_MRR_USER.EXPLAIN_SOV_INVOICE_CACHE
(
  explain_zip        NUMBER not null,
  decl_zip           NUMBER,
  is_cache_clear     NUMBER(1) default 0 not null,
  dt_processed       DATE
);

create index NDS2_MRR_USER.IX_SOV_INVOICE_CACHE_CLEAR on NDS2_MRR_USER.EXPLAIN_SOV_INVOICE_CACHE (is_cache_clear)
compress
tablespace NDS2_IDX;

create index NDS2_MRR_USER.IX_SOV_INVOICE_CACHE_DECL_ZIP on NDS2_MRR_USER.EXPLAIN_SOV_INVOICE_CACHE (decl_zip)
compress
tablespace NDS2_IDX;