﻿begin
  execute immediate 'drop table NDS2_MRR_USER.KNP_DISCREPANCY';
  exception when others then null;
end;
/

create table NDS2_MRR_USER.KNP_DISCREPANCY
(
  discrepancy_id  NUMBER,
  invoice_row_key VARCHAR2(2048),
  chapter         NUMBER,
  inn_1           VARCHAR2(12 CHAR),
  kpp_effective_1 VARCHAR2(9 CHAR),
  type_code_1     NUMBER,
  period_code_1   VARCHAR2(2 CHAR),
  fiscal_year_1   VARCHAR2(4 CHAR),
  inn_2           VARCHAR2(12 CHAR),
  inn_2_declarant VARCHAR2(12 CHAR),
  kpp_effective_2 VARCHAR2(9 CHAR),
  type_code_2     NUMBER,
  period_code_2   VARCHAR2(2 CHAR),
  fiscal_year_2   VARCHAR2(4 CHAR)
);
