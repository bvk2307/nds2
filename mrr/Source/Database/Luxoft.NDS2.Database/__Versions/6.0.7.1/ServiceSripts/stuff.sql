﻿
drop table TMP1;
create table TMP1 as
select row_number() over (partition by t.idfajl  order by t.zip desc) as RN, t.NOMKORR, t.OTCHETGOD, t.PERIOD, t.INNNP, t.zip from v$askdekl t;

create index idx_uk_tmp1 on TMP1(RN);