﻿DROP TABLE NDS2_MRR_USER.REPORT_MATCHING_RULE;
CREATE TABLE NDS2_MRR_USER.REPORT_MATCHING_RULE
(   
  ID NUMBER(12) NOT NULL ENABLE, 
  TASK_ID NUMBER NOT NULL,
  ACTUAL_NUM NUMBER(1) NOT NULL,
  AGG_ROW NUMBER(1) NOT NULL,
  Rule_Group_Caption VARCHAR(32 char),
  Rule_Group_number NUMBER(5),
  Rule_Number_Caption VARCHAR(32 char),
  Rule_Number NUMBER(5),
  Rule_Number_Comment VARCHAR(255 char),
  MatchCount NUMBER(19),
  MatchAmount NUMBER(19,2),
  PercentByCount NUMBER(19,5),
  PercentByAmount NUMBER(19,5),
  PercentInnerInvoiceErrorLC NUMBER(19,5),
  PercentOuterInvoiceErrorLC NUMBER(19,5),
  PercentInnerOuterInvErrorLC NUMBER(19,5),
  count_inner_errors NUMBER(19),
  count_outer_errors NUMBER(19),
  count_innerouter_errors NUMBER(19),
  CONSTRAINT REPORT_MATCHING_RULE_PK PRIMARY KEY (ID) ENABLE
);