-- Create table
create table NDS2_MRR_USER.SOV_MATCH_RULE_SUMMARY
(
  id                             number not null,
  process_date                   date not null,
  fiscal_year                    number not null,
  fiscal_period                  number not null,
  inspection_code                number not null, /*?*/
  rule_number                    number not null,
  rule_group_number              number not null,
  count_lk_errors_buyer_seller   number not null,
  vat_lk_errors_buyer_seller     number not null,
  count_lk_errors_buyer_only     number not null,
  vat_lk_errors_buyer_only       number not null,
  count_lk_errors_seller_only    number not null,
  vat_lk_errors_seller_only      number not null,
  count_no_lk_errors             number not null,
  vat_no_lk_errors               number not null
);
-- Create/Recreate primary, unique and foreign key constraints 
alter table NDS2_MRR_USER.SOV_MATCH_RULE_SUMMARY
  add constraint PK_SOV_MATCH_RULE_SUMMARY primary key (ID);
