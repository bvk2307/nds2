create table nds2_mrr_user.dict_compare_graphs
(
  discrepancy_type number(1),
  rule_code varchar2(2 char),
  invoice_chapter number(2),
  graph varchar2(2 char)
);