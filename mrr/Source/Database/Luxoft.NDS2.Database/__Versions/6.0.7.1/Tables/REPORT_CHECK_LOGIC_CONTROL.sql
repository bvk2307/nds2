﻿alter table NDS2_MRR_USER.REPORT_CHECK_LOGIC_CONTROL 
      MODIFY (ErrorCountPercentage NUMBER(19,5),
              ErrorFrequencyInvoiceNotExact NUMBER(19,5),
              ErrorFrequencyInvoiceGap NUMBER(19,5),
              ErrorFrequencyInvoiceExact NUMBER(19,5),
              ErrorFrequencyInnerInvoice NUMBER(19,5),
              ErrorFrequencyOuterInvoice NUMBER(19,5));