-- Create table
create table NDS2_MRR_USER.CFG_CHAIN_VIEW_RESTRICTION
(
  ROLE_ID number not null,
  NOT_RESTRICTED number not null,
  MAX_LEVEL_SALES number not null,
  MAX_LEVEL_PURCHASE number not null,
  MAX_NAVIGATOR_CHAINS number not null,
  RESTRICT_PURCHASE number not null,
  ROLE_DESCRIPTION varchar(50) null
)
;
-- Create/Recreate primary, unique and foreign key constraints 
alter table NDS2_MRR_USER.CFG_CHAIN_VIEW_RESTRICTION
  add constraint PK_CFG_CHAIN_VIEW_RESTRICTION primary key (ROLE_ID);
