﻿create table NDS2_MRR_USER.DICT_LOGICAL_ERRORS
(
  Error_Code    NUMBER(4) not null,
  Name_Check    VARCHAR2(256) not null,
  Descreption   VARCHAR2(4000) not null,
  Constraint PK_DICT_LOGICAL_ERRORS primary key (Error_Code)
);