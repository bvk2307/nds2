﻿create or replace force view NDS2_MRR_USER.V$TECHNO_REPORT_VALUE as
select iteration_id
       ,parameter_id
       ,value
from TECHNO_REPORT_VALUE rv
union
select rv.iteration_id
       ,995
       ,TO_CHAR(case when b.summa <> 0 then a.summa / b.summa else 0 end) as value
from TECHNO_REPORT_VALUE rv
left outer join
(
  select iteration_id, sum(case when REGEXP_LIKE(value,'^\s*[\Q+-\E]?[[:digit:]]+[\Q.,\E]?[[:digit:]]*\s*$') then TO_NUMBER(replace(value,',','.')) else 0 end) as summa
  from TECHNO_REPORT_VALUE rv
  where parameter_id in (102, 104)
  group by iteration_id
) a on a.iteration_id = rv.iteration_id
left outer join
(
  select iteration_id, sum(case when REGEXP_LIKE(value,'^\s*[\Q+-\E]?[[:digit:]]+[\Q.,\E]?[[:digit:]]*\s*$') then TO_NUMBER(replace(value,',','.')) else 0 end) as summa
  from TECHNO_REPORT_VALUE rv
  where parameter_id in (101, 103)
  group by iteration_id
) b on b.iteration_id = rv.iteration_id
union
select iteration_id
       ,996
       ,TO_CHAR(sum(case when REGEXP_LIKE(value,'^\s*[\Q+-\E]?[[:digit:]]+[\Q.,\E]?[[:digit:]]*\s*$') then TO_NUMBER(replace(value,',','.')) else 0 end))
from TECHNO_REPORT_VALUE rv
where parameter_id  in (137, 132, 133, 134, 125, 126, 128, 129, 118)
group by iteration_id
union
select iteration_id
       ,parameter_id
       ,(case when pValue is not null then
         TO_CHAR(TRUNC(pValue/3600),'FM9900') || ':' ||
         TO_CHAR(TRUNC(MOD(pValue,3600)/60),'FM00') || ':' ||
         TO_CHAR(MOD(pValue,60),'FM00') else TO_CHAR(pValue) end) as value
from
(
   select iteration_id
         ,997 as parameter_id
         ,sum(case when REGEXP_LIKE(value,'^([[:digit:]]{1,2}:[012345]?[[:digit:]]{1}:[[:digit:]]{1,2})$') then
                   extract(hour from(TO_TIMESTAMP(rv.value, 'HH24:MI:SS')))*3600 +
                   extract(minute from (TO_TIMESTAMP(rv.value, 'HH24:MI:SS')))*60 +
                   extract(second from (TO_TIMESTAMP(rv.value, 'HH24:MI:SS')))
              else 0 end) as pValue
   from TECHNO_REPORT_VALUE rv
   where parameter_id  in (138, 135, 119)
   group by iteration_id
);