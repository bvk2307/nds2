create or replace force view nds2_mrr_user.v$egrn_ul as
select
  ul.id,
  ul.inn,
  ul.kpp,
  ul.ogrn,
  ul.name_full,
  ul.date_reg,
  ul.code_no,
  ul.date_on,
  ul.date_off,
  ul.reason_off,
  ul.adr,
  ul.ust_capital,
  ul.is_knp as is_biggest,
  soun.S_Name as SOUN_NAME,
  ssrf.s_code as  Region_Code,
  ssrf.s_name as Region_Name
from V_EGRN_UL ul
  left join V$SONO soun on soun.s_code = ul.code_no
  left join V$SSRF ssrf on ssrf.s_code = substr(ul.code_no, 1, 2);
