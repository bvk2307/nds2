create or replace force view NDS2_MRR_USER.v$navigator_tax_payer as
select egrn.*, dis.description as FederalDistrict
from
     (select
          ul.inn,
          ul.kpp,
          ul.Name_Full as name,
          nvl(ul.code_no,'')||' - '||nvl(ul.SOUN_NAME,'') as inspection,
          nvl(ul.region_code,'')||' - '||nvl(ul.region_name,'') as region,
          ul.code_no as inspectioncode,
          ul.region_code as regioncode
      from V$EGRN_UL ul
      union all
      select
          ip.inn,
          null,
          ip.LAST_NAME || ' ' || ip.first_name || ' ' || ip.patronymic NAME,
          nvl(ip.code_no,'')||' - '||nvl(ip.SOUN_NAME,''),
          nvl(ip.region_code,'')||' - '||nvl(ip.region_name,''),
          ip.code_no as inspectioncode,
          ip.region_code as regioncode
      from V$EGRN_IP ip) egrn
      left join FEDERAL_DISTRICT_REGION regToDis on regToDis.Region_Code = egrn.regioncode
      left join FEDERAL_DISTRICT dis on dis.district_id = regToDis.district_Id;
