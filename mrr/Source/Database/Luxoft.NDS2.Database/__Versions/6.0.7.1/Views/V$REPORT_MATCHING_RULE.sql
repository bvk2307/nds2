﻿create or replace force view NDS2_MRR_USER.V$REPORT_MATCHING_RULE AS
SELECT
	 r.TASK_ID
	,r.Rule_Group_Caption
	,r.Rule_Number_Caption
	,r.Rule_Number_Comment
	,r.MatchCount
	,r.MatchAmount
	,r.PercentByCount
	,r.PercentByAmount
	,r.PercentInnerInvoiceErrorLC
	,r.PercentOuterInvoiceErrorLC
	,r.PercentInnerOuterInvErrorLC
from NDS2_MRR_USER.REPORT_MATCHING_RULE r
where r.ACTUAL_NUM = 1
order by r.Rule_Group_number, r.Rule_Number;