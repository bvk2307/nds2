﻿
create or replace force view NDS2_MRR_USER.V$ASKPoyasnenie
as
select
   mc."Ид" as Id
  ,mc."ZIP" as ZIP
  ,mc."ИдФайл" as IdFayl
  ,mc."ВерсПрог" as VersProg
  ,mc."ВерсФорм" as VersForm
  ,mc."КНД" as KND
  ,mc."ДатаДок" as DataDok
  ,mc."ПризОтпрУП" as PrizOtprUP
  ,mc."НаимОргОтпр" as NaimOrgOtpr
  ,mc."ИННОтпр" as INNOtpr
  ,mc."КППОтпр" as KPPOtpr
  ,mc."ФамилияОтпр" as FamiliyaOtpr
  ,mc."ИмяОтпр" as ImyaOtpr
  ,mc."ОтчествоОтпр" as OtchestvoOtpr
  ,mc."КодНО" as KodNO
  ,mc."НаимОргНП" as NaimOrgNP
  ,mc."ИНННП" as INNNP
  ,mc."КППНП" as KPPNP
  ,mc."ФамилияНП" as FamiliyaNP
  ,mc."ИмяНП" as ImyaNP
  ,mc."ОтчествоНП" as OtchestvoNP
  ,mc."ПрПодп" as PrPodp
  ,mc."Должн" as Dolzhn
  ,mc."Тлф" as Tlf
  ,mc."E-mail" as Email
  ,mc."ФамилияПодп" as FamiliyaPodp
  ,mc."ИмяПодп" as ImyaPodp
  ,mc."ОтчествоПодп" as OtchestvoPodp
  ,mc."НаимДок" as NaimDok
  ,mc."НомТреб" as NomTreb
  ,mc."ДатаТреб" as DataTreb
  ,mc."Период" as Period
  ,mc."ОтчетГод" as OtchetGod
  ,mc."НомКорр" as NomKorr
  ,mc."ИмяФайлТреб" as ImyaFaylTreb
  ,mc."ИдЗагрузка" as IdZagruzka
  ,mc."ИдДекл" as IdDekl
  ,mc."ОбработанМС" as ObrabotanMS
  ,mc."КолЗапВсего" as KolZapVsego
  ,mc."КолЗапПодтв" as KolZapPodtv
  ,mc."КолЗапИзм" as KolZapIzm
  ,mc."КолЗапНесовп" as KolZapNesovp
from NDS2_MC."ASKПояснение" mc;
