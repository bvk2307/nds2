﻿create or replace force view nds2_mrr_user.v$nds2_decl_correction_id as
select
  to_number(d.tax_period||d.fiscal_year||d.inn||d.correction_number) as nds2_corr_id
from seod_declaration d
union all
select
  to_number(msd.PERIOD||msd.OTCHETGOD||msd.INNNP||msd.NOMKORR) as nds2_corr_id
from v$askdekl msd;

comment on table nds2_mrr_user.V$NDS2_DECL_CORRECTION_ID is 'Объединенный список идентификаторов деклараций/журналов АСК-НДС2';
