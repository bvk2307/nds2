create or replace force view NDS2_MRR_USER.V$NOT_REFLECTED_INVOICE as
select
  inv.declaration_version_id
  ,inv.inn
  ,inv.kpp
  ,vtp.NAME
  ,inv.invoice_num
  ,inv.invoice_date
  ,inv.doc_id
  ,inv.chapter
  ,inv.compare_row_key
  ,max(inv.status) as DISCREPANCY_STATUS
from (
  select
    si.declaration_version_id
    ,case 
      when si.chapter = 8 then si.seller_inn
      when si.chapter = 9 then si.buyer_inn
      when si.chapter = 10 then si.buyer_inn
      when si.chapter = 11 then si.buyer_inn
      when si.chapter = 12 then si.buyer_inn
     end as inn
    ,case 
      when si.chapter = 8 then si.seller_kpp
      when si.chapter = 9 then si.buyer_kpp
      when si.chapter = 10 then si.buyer_kpp
      when si.chapter = 11 then si.buyer_kpp
      when si.chapter = 12 then si.buyer_kpp
      end as kpp
    ,si.invoice_num
    ,si.invoice_date
    ,t_invoices.doc_id
    ,si.chapter
    ,si.compare_row_key
    ,t_invoices.status
  from stage_invoice si
  inner join 
  (
    select
      case
        when queue.for_stage = 2 then dis.invoice_rk
        when queue.for_stage = 3 then dis.invoice_contractor_rk
      end as row_key
      ,doc.doc_id
      ,dis.status
    from seod_data_queue queue
    inner join sov_discrepancy dis on dis.id = queue.discrepancy_id
    inner join doc on doc.doc_id = queue.ref_doc_id
          and queue.for_stage in (2, 3)
          and doc.doc_type = 1
  ) t_invoices on t_invoices.row_key = si.row_key
) inv
left join v$taxpayer vtp on vtp.inn = inv.inn
group by
  inv.declaration_version_id
  ,inv.inn
  ,inv.kpp
  ,vtp.NAME
  ,inv.invoice_num
  ,inv.invoice_date
  ,inv.doc_id
  ,inv.chapter
  ,inv.compare_row_key
;

