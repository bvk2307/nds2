create or replace force view nds2_mrr_user.v$taxpayer as
select
  ul.Name_Full as NAME,
  ul.inn,
  ul.kpp,
  ul.REGION_CODE,
  null as LEGAL_ADDRESS,
  ul.adr as ADDRESS,
  ul.code_no as SOUN_CODE,
  case when ul.is_biggest = '1' then 1 else null end as CATEGORY,
  ul.date_reg as DATE_RECORDED,
  null as TAXATION,
  null as OKVED_CODE,
  ul.ust_capital as REGULATION_FUND,
  ul.SOUN_NAME,
  ul.region_name
from V$EGRN_UL ul
union
select
  trim(nvl(ip.last_name,'') || nvl(' '||ip.first_name, '') || nvl(' '||ip.patronymic,'')) as NAME,
  ip.inn,
  null as kpp,
  ip.REGION_CODE,
  null as LEGAL_ADDRESS,
  ip.adr as ADDRESS,
  ip.code_no as SOUN_CODE,
  null as CATEGORY,
  ip.date_on as DATE_RECORDED,
  null as TAXATION,
  null as OKVED_CODE,
  null as REGULATION_FUND,
  ip.SOUN_NAME,
  ip.region_name
from V$EGRN_IP ip;