CREATE OR REPLACE PACKAGE NDS2_MRR_USER.PAC$TECHNOLOGICAL_REPORT
as
  function GET_CLOB_SIZE(p_clob in clob) return number;

  procedure CREATE_ITERATIONS_ON_DATE(p_date in date);

  procedure CLOSE_ITERATIONS_ON_DATE(p_date in date);

  function GET_ITERATION_ID
  (
    p_date in techno_report_iteration.create_date%type,
    p_module_name in techno_report_iteration.module_name%type
  ) return number;

  procedure FILL_CC_REPORT(p_date in date);

  procedure FILL_MRR_REPORT(p_date in date);

  procedure FILL_MC_REPORT(p_date in date);

  PROCEDURE SET_TIMER(p_start in varchar2, p_wait in varchar2);
  
  PROCEDURE CALC(p_start in date, p_end in date);
end;
/

CREATE OR REPLACE PACKAGE BODY NDS2_MRR_USER.PAC$TECHNOLOGICAL_REPORT
as
  function GET_DATE_DIFF(p_diff_in_days in number) return varchar2
  as
    v_hour number;
    v_minute number;
    v_second number;
    v_diff number;
  begin
    v_diff := p_diff_in_days * 24;
    v_hour := trunc(v_diff);
    v_diff := (v_diff - v_hour) * 60;
    v_minute := trunc(v_diff);
    v_diff := (v_diff - v_minute) * 60;
    v_second := round(v_diff);

    return trim(to_char(v_hour, '00')) || ':' || trim(to_char(v_minute, '00')) || ':' || trim(to_char(v_second, '00'));
  end;

  function GET_CLOB_SIZE(p_clob in clob) return number
  as
    v_blob BLOB;
    v_offset NUMBER DEFAULT 1;
    v_amount NUMBER DEFAULT 4096;
    v_offsetwrite NUMBER DEFAULT 1;
    v_amountwrite NUMBER;
    v_buffer VARCHAR2(4096 CHAR);
    v_size NUMBER;
  begin
    dbms_lob.createtemporary(v_blob, true);
    begin
      loop
        dbms_lob.read(p_clob, v_amount, v_offset, v_buffer);
        v_amountwrite := utl_raw.length(utl_raw.cast_to_raw(v_buffer));
        dbms_lob.write(v_blob, v_amountwrite, v_offsetwrite, utl_raw.cast_to_raw(v_buffer));

        v_offsetwrite := v_offsetwrite + v_amountwrite;
        v_offset := v_offset + v_amount;
        v_amount := 4096;
      end loop;
      exception when no_data_found then null;
    end;

    v_size := dbms_lob.getlength(v_blob);
    dbms_lob.freetemporary(v_blob);
    return v_size;
    exception when others then
      return null;
  end;
  
  function GET_MC_PARAMETER_VALUE
  (
    p_date in date,
    p_counter in varchar2,
    p_process in varchar2,
    p_iteration in varchar2 := null
  ) return number
  as
  v_result number;
  begin
    select nvl(mc_cnt.Znachenie, 0)
    into v_result
    from V$ASKZhurnal mc_jrnl
    left join V$ASKSchetchiki mc_cnt on mc_cnt.IdZhurnal = mc_jrnl.Id
    where trunc(mc_jrnl.DataOkonch) = trunc(p_date)
      and mc_cnt.Schetchik = p_counter
      and mc_jrnl.NaimProtsess = p_process
      and (p_iteration is null or mc_jrnl.Iteratsiya = p_iteration);

    return v_result;
    exception when others then
      Nds2$sys.LOG_ERROR('Technological report - Get MC value', null, sqlcode, p_counter||': '||sqlerrm);
      return 0;
  end;

  procedure PUT_REPORT_VALUE(p_iteration_id in number, p_parameter_id in number, p_value in varchar2)
  as
  begin
    insert into techno_report_value
      (iteration_id, parameter_id, value)
    values
       (p_iteration_id, p_parameter_id, p_value);
    exception when others then
      Nds2$sys.LOG_ERROR('Technological report - Put value', null, sqlcode, '#'||p_parameter_id||': '||sqlerrm);
  end;

  procedure CREATE_ITERATION_ON_DATE
  (
    p_date in techno_report_iteration.create_date%type,
    p_module_name in techno_report_iteration.module_name%type
  )
  as
  begin
    insert into techno_report_iteration (Id, Module_Name, Create_Date, Is_Closed)
    values (seq_techno_report_iteration.nextval, p_module_name, p_date, 0);
  end;

  procedure CREATE_ITERATIONS_ON_DATE(p_date in date)
  as
    pragma autonomous_transaction;
    v_date date := trunc(p_date);
  begin
    Nds2$sys.LOG_INFO('Technological report - Create Iterations', null, sqlcode, 'Start on date '||p_date);
    delete from techno_report_value trv where trv.iteration_id in
    (select tri.id from techno_report_iteration tri where tri.create_date = p_date and tri.module_name != '���');
    delete from techno_report_iteration tri where tri.create_date = p_date and tri.module_name != '���';
    commit;
    CREATE_ITERATION_ON_DATE(v_date, '��-3');
    CREATE_ITERATION_ON_DATE(v_date, '��');
    CREATE_ITERATION_ON_DATE(v_date, '���');
    CREATE_ITERATION_ON_DATE(v_date, '��');
    commit;
    exception when others then
      rollback;
      Nds2$sys.LOG_ERROR('Technological report - Create Iterations', null, sqlcode, sqlerrm);
  end;

  procedure CLOSE_ITERATIONS_ON_DATE(p_date in date)
  as
    pragma autonomous_transaction;
  begin
    update techno_report_iteration
    set is_closed = 1
    where create_date = trunc(p_date);
    commit;
    Nds2$sys.LOG_INFO('Technological report - Close Iterations', null, sqlcode, 'Close on date '||p_date);
    exception when others then
      rollback;
      Nds2$sys.LOG_ERROR('Technological report - Close Iterations', null, sqlcode, sqlerrm);
  end;

  function GET_ITERATION_ID
  (
    p_date in techno_report_iteration.create_date%type,
    p_module_name in techno_report_iteration.module_name%type
  ) return number
  as
    v_id number;
  begin
    select Id
    into v_id
    from techno_report_iteration
    where create_date = trunc(p_date) and module_name = p_module_name and is_closed = 0;

    return v_id;
    exception when no_data_found then
      return null;
  end;

  procedure FILL_CC_REPORT(p_date in date)
  as
    pragma autonomous_transaction;
    v_id number;
    v_date date;
    v_count number;
    v_param number;
  begin
    v_date := trunc(p_date);
    v_id := GET_ITERATION_ID(v_date, '��');
    if v_id is null then
       Nds2$sys.LOG_WARNING('Technological report - FILL_CC_REPORT', null, sqlcode, 'Iteration not started');
      return;
    end if;

  Nds2$sys.LOG_INFO('Technological report - FILL_CC_REPORT', null, sqlcode, 'Start');
  /**/
  v_param := 2001;
  select nvl(max(src.RECEIPT_EOD), 0) into v_count from v_report_eod@nds2_ais_i$nds2 src where src.DATE_REPORT = v_date and src.INFO_TYPE = 'CAM_NDS2_01';
  put_report_value(v_id, v_param, v_count);

  v_param := 2003;
  select nvl(max(src.RECEIPT_EOD), 0) into v_count from v_report_eod@nds2_ais_i$nds2 src where src.DATE_REPORT = v_date and src.INFO_TYPE = 'CAM_NDS2_11';
	put_report_value(v_id, v_param, v_count);

  v_param := 2011;
  select nvl(max(src.ACCEPT_NDS2), 0) into v_count from v_report_eod@nds2_ais_i$nds2 src where src.DATE_REPORT = v_date and src.INFO_TYPE = 'CAM_NDS2_01';
	put_report_value(v_id, v_param, v_count);

  v_param := 2012;
  select nvl(max(src.ACCEPT_NDS2), 0) into v_count from v_report_eod@nds2_ais_i$nds2 src where src.DATE_REPORT = v_date and src.INFO_TYPE = 'CAM_NDS2_11';
	put_report_value(v_id, v_param, v_count);

  v_param := 2013;
  select nvl(max(src.REFUSE_NDS2), 0) into v_count from v_report_eod@nds2_ais_i$nds2 src where src.DATE_REPORT = v_date and src.INFO_TYPE = 'CAM_NDS2_01';
	put_report_value(v_id, v_param, v_count);

  v_param := 2014;
  select nvl(max(src.REFUSE_NDS2), 0) into v_count from v_report_eod@nds2_ais_i$nds2 src where src.DATE_REPORT = v_date and src.INFO_TYPE = 'CAM_NDS2_11';
	put_report_value(v_id, v_param, v_count);
  /**/
  
    commit;
    exception when others then
      rollback;
      Nds2$sys.LOG_ERROR('Technological report - Fill CC report', null, sqlcode, '#'||v_param||': '||sqlerrm);
  end;
  
  procedure FILL_MRR_REPORT(p_date in date)
  as
    pragma autonomous_transaction;
    v_id number;
    v_date date;

    v_count number;
    v_count_data number;
    v_size_data number;
    v_param number;
  begin
    v_date := trunc(p_date);
    v_id := GET_ITERATION_ID(v_date, '���');
    if v_id is null then
       Nds2$sys.LOG_WARNING('Technological report - FILL_MRR_REPORT', null, sqlcode, 'Iteration not started');
      return;
    end if;

    Nds2$sys.LOG_INFO('Technological report - FILL_MRR_REPORT', null, sqlcode, 'Start');
    
    v_param := 1501;
    select count(doc.doc_id), nvl(sum(GET_CLOB_SIZE(doc.document_body)), 0)
    into v_count, v_size_data
    from doc
    inner join doc_status_history hist on hist.doc_id = doc.doc_id
    where hist.status = 2
      and trunc(hist.status_date) = v_date
      and doc.doc_type = 5;

    put_report_value(v_id, 1501, v_count);
    put_report_value(v_id, 1502, v_size_data);

    v_param := 1503;
    select
      count(doc.doc_id),
      nvl(sum(doc_inv.invoice_count), 0),
      nvl(sum(GET_CLOB_SIZE(doc.document_body)), 0)
    into v_count, v_count_data, v_size_data
    from doc
    inner join doc_status_history hist on hist.doc_id = doc.doc_id
    inner join
    (
      select doc_id, count(invoice_row_key) as invoice_count
      from doc_invoice
      group by doc_id
    ) doc_inv on doc_inv.doc_id = doc.doc_id
    where hist.status = 2
      and trunc(hist.status_date) = v_date
      and doc.doc_type = 1;

    put_report_value(v_id, 1503, v_count);
    put_report_value(v_id, 1504, v_count_data);
    put_report_value(v_id, 1505, v_size_data);

    v_param := 1506;
    select
      count(doc.doc_id),
      nvl(sum(doc_inv.invoice_count), 0),
      nvl(sum(GET_CLOB_SIZE(doc.document_body)), 0)
    into v_count, v_count_data, v_size_data
    from doc
    inner join doc_status_history hist on hist.doc_id = doc.doc_id
    inner join
    (
      select doc_id, count(invoice_row_key) as invoice_count
      from doc_invoice
      group by doc_id
    ) doc_inv on doc_inv.doc_id = doc.doc_id
    where hist.status = 2
      and trunc(hist.status_date) = v_date
      and doc.doc_type in (2, 3);

    put_report_value(v_id, 1506, v_count);
    put_report_value(v_id, 1507, v_count_data);
    put_report_value(v_id, 1508, v_size_data);

    v_param := 1509;
    select
      count(reply.explain_id),
      nvl(sum(explain_invoice.invoice_count), 0),
      nvl(sum(reply.FileSizeOutput), 0)
    into v_count, v_count_data, v_size_data
    from seod_explain_reply reply
    inner join hist_explain_reply_status hist on hist.explain_id = reply.explain_id
    inner join
    (
      select explain_id, count(invoice_original_id) as invoice_count
      from stage_invoice_corrected_state
      group by explain_id
    ) explain_invoice on explain_invoice.explain_id = reply.explain_id
    where hist.status_id = 2 and reply.FileNameOutput is not null and trunc(hist.submit_date) = v_date;

    put_report_value(v_id, 1509, v_count);
    put_report_value(v_id, 1510, v_count_data);
    put_report_value(v_id, 1511, v_size_data);

    v_param := 1601;
    select count(1) into v_count from seod_declaration sd where trunc(sd.eod_date) = v_date and sd.type=0;
	  put_report_value(v_id, 1601, v_count);

    v_param := 1602;
	  select count(distinct sd.decl_reg_num) into v_count from seod_declaration sd where sd.type=0 and trunc(sd.eod_date) <= v_date;
	  put_report_value(v_id, 1602, v_count);

    v_param := 1603;
	  select count(1) into v_count from seod_declaration sd where trunc(sd.eod_date) = v_date and sd.type=1;
	  put_report_value(v_id, 1603, v_count);

    v_param := 1604;
	  select count(distinct sd.decl_reg_num) into v_count from seod_declaration sd where sd.type=1 and trunc(sd.eod_date) <= v_date;
	  put_report_value(v_id, 1604, v_count);

    v_param := 1611;

	  select count(1) into v_count from seod_declaration sd
	  left join V$ASK_DECLANDJRNL mc on mc.PERIOD = sd.TAX_PERIOD and mc.OTCHETGOD = sd.FISCAL_YEAR and mc.INNNP = sd.INN and mc.NOMKORR = sd.correction_number
	  where (mc.id is null and trunc(sd.eod_date) + 3 < v_date) or mc.datadok > sd.eod_date + 3;

    put_report_value(v_id, 1611, v_count);

    v_param := 1612;

	  select count(1) into v_count from seod_declaration sd 
	  left join V$ASK_DECLANDJRNL mc on mc.PERIOD = sd.TAX_PERIOD and mc.OTCHETGOD = sd.FISCAL_YEAR and mc.INNNP = sd.INN and mc.NOMKORR = sd.correction_number
	  where mc.id is not null and sd.decl_reg_num is null and mc.datadok + 3 < v_date and mc.datadok + 3 < sd.insert_date;

	  put_report_value(v_id, 1612, v_count);
    
    -- ������ �� SOV_MATCH_RESULT_SUMMARY ��� ������� ����������
    v_param := 4201;
    select nvl(sum(smrs.invoice_record_count), 0) into v_count from SOV_MATCH_RESULT_SUMMARY smrs where smrs.process_date = v_date;
	  put_report_value(v_id, v_param, v_count);

    v_param := 4208;
    select nvl(sum(smrs.invoice_record_count_chapter8), 0) into v_count from SOV_MATCH_RESULT_SUMMARY smrs where smrs.process_date = v_date;
	  put_report_value(v_id, v_param, v_count);

    v_param := 4209;
    select nvl(sum(smrs.invoice_record_count_chapter9), 0) into v_count from SOV_MATCH_RESULT_SUMMARY smrs where smrs.process_date = v_date;
	  put_report_value(v_id, v_param, v_count);

    v_param := 4210;
    select nvl(sum(smrs.invoice_record_count_chapter10), 0) into v_count from SOV_MATCH_RESULT_SUMMARY smrs where smrs.process_date = v_date;
	  put_report_value(v_id, v_param, v_count);

    v_param := 4211;
    select nvl(sum(smrs.invoice_record_count_chapter11), 0) into v_count from SOV_MATCH_RESULT_SUMMARY smrs where smrs.process_date = v_date;
	  put_report_value(v_id, v_param, v_count);

    v_param := 4212;
    select nvl(sum(smrs.invoice_record_count_chapter12), 0) into v_count from SOV_MATCH_RESULT_SUMMARY smrs where smrs.process_date = v_date;
	  put_report_value(v_id, v_param, v_count);

    commit;
    exception when others then
      rollback;
      Nds2$sys.LOG_ERROR('Technological report - Fill MRR report', null, sqlcode, '#'||v_param||': '||sqlerrm);
  end;

  procedure FILL_MC_REPORT(p_date in date)
  as
    v_id number;
    v_date date;

    v_value1 varchar2(128);
    v_value2 varchar2(128);
    v_value3 varchar2(128);
    v_value4 varchar2(128);

    v_group number;
    v_num_1 number;
    v_num_2 number;
    v_num_3 number;
    v_num_4 number;
    v_param number;
  begin
    v_date := trunc(p_date);
    v_id := GET_ITERATION_ID(v_date, '��');
    if v_id is null then
      Nds2$sys.LOG_WARNING('Technological report - FILL_MC_REPORT', null, sqlcode, 'Iteration not started');
      return;
    end if;

    Nds2$sys.LOG_INFO('Technological report - FILL_MC_REPORT', null, sqlcode, 'Start');

    --101, 103
    v_param := 101;
    
    select
      nvl(sum(case when nvl(mc_decl.NomKorr, '0') = '0' then 1 else 0 end), 0) as count_primary,
      nvl(sum(case when nvl(mc_decl.NomKorr, '0') = '0' then 0 else 1 end), 0) as count_secondary
    into v_value1, v_value2
    from V$ASKDekl mc_decl
    inner join V$ASKOperatsiya mc_oper on mc_oper.NomerOp = mc_decl.IdZagruzka and mc_oper.vidop = 1
    where trunc(mc_oper.DataNach) = v_date;

    put_report_value(v_id, 101, v_value1);
    put_report_value(v_id, 103, v_value2);

    --102, 104
    v_param := 102;
    select
      nvl(sum(case when nvl(mc_decl.NOMKORR, '0') = '0' then nvl(mc_aggr.KolZapisey, 0) else 0 end), 0) as sum_primary,
      nvl(sum(case when nvl(mc_decl.NOMKORR, '0') = '0' then 0 else nvl(mc_aggr.KolZapisey, 0) end), 0) as sum_secondary
    into v_value1, v_value2
    from V$ASKSvodZap mc_aggr
    inner join V$ASKDekl mc_decl on mc_aggr.IdDekl = mc_decl.Id
    inner join V$ASKOperatsiya mc_oper on mc_oper.NomerOp = mc_decl.IdZagruzka
    where trunc(mc_oper.DataOkonch) = v_date;

    put_report_value(v_id, 102, v_value1);
    put_report_value(v_id, 104, v_value2);

    --105, 106
    v_param := 105;
    select count(*), nvl(sum(mc_expl.KolZapVsego), 0)
    into v_value1, v_value2
    from V$ASKPoyasnenie mc_expl
    inner join V$ASKDekl mc_decl on mc_expl.IdDekl = mc_decl.Id
    inner join V$ASKOperatsiya mc_oper on mc_oper.NomerOp = mc_decl.IdZagruzka
    where trunc(mc_oper.DataOkonch) = v_date;

    put_report_value(v_id, 105, v_value1);
    put_report_value(v_id, 106, v_value2);

    --107
    v_param := 107;
    select count(*)
    into v_value1
    from V$ASKDekl mc_decl
    inner join V$ASKOperatsiya mc_oper on mc_oper.NomerOp = mc_decl.IdZagruzka
    left join V$ASKSvodZap mc_aggr on mc_aggr.IdDekl = mc_decl.ID and mc_aggr.KolZapisey > 0
    where trunc(mc_oper.DataOkonch) = v_date and mc_aggr.ID is null;

    put_report_value(v_id, 107, v_value1);

    --108, 109
    v_param := 108;

    --� ����� � ���������� ����������� � ��
    --select count(*), nvl(sum(mc_jrnl.KolZapCh1), 0) + nvl(sum(mc_jrnl.KolZapCh2), 0)
    --into v_value1, v_value2
    --from V$ASKJournalUch mc_jrnl
    --inner join V$ASKOperatsiya mc_oper on mc_oper.NomerOp = mc_jrnl.IdZagruzka
    --where trunc(mc_oper.DataOkonch) = v_date;

    v_value1 := 0;
    v_value2 := 0;

    put_report_value(v_id, 108, v_value1);
    put_report_value(v_id, 109, v_value2);

    --110
    v_param := 110;
    put_report_value(v_id, 110, get_mc_parameter_value(v_date, 'LOGICAL_ERRORS', 'load'));

    --111, 112
    v_param := 111;
    select count(*), count(distinct mc_decl.Id)
    into v_value1, v_value2
    from V$ASKKontrSoontosh mc_cr
    inner join V$ASKDekl mc_decl on mc_cr.IdDekl = mc_decl.Id
    inner join V$ASKOperatsiya mc_oper on mc_oper.NomerOp = mc_decl.IdZagruzka
    where trunc(mc_oper.DataOkonch) = v_date and mc_cr.Vypoln <> '1';

    put_report_value(v_id, 111, v_value1);
    put_report_value(v_id, 112, v_value2);

    --113
    v_param := 113;
    begin
      select DataOkonch - DataNach
      into v_value1
      from V$ASKOperatsiya
      where Id =
      (
        select max(Id)
        from V$ASKOperatsiya
        where trunc(DataOkonch) = v_date and VidOp = '1'
      );
      exception when no_data_found then
        v_value1 := 0;
    end;
    put_report_value(v_id, 113, get_date_diff(v_value1));

    --114, 115
    v_param := 114;
    put_report_value(v_id, 114, get_mc_parameter_value(v_date, 'SKIP_MATCH_ROWS', 'load'));
    put_report_value(v_id, 115, get_mc_parameter_value(v_date, 'AGGREGATES', 'aggregate'));

    --116
    v_param := 116;
    begin
      select DataOkonch - DataNach
      into v_value1
      from V$ASKOperatsiya
      where Id =
      (
        select max(Id)
        from V$ASKOperatsiya
        where trunc(DataOkonch) = v_date and VidOp = '2'
      );
      exception when no_data_found then
        v_value1 := 0;
    end;

    put_report_value(v_id, 116, get_date_diff(v_value1));

    --117, 118
    v_param := 117;
    put_report_value(v_id, 117, get_mc_parameter_value(v_date, 'MATCH_INPUT_ROWS', 'j-match'));
    put_report_value
    (
      v_id,
      118,
      get_mc_parameter_value(v_date, 'MATCHES_0_11', 'j-update-sales')
      + get_mc_parameter_value(v_date, 'MATCHES_0_1N', 'j-update-sales')
      + get_mc_parameter_value(v_date, 'MATCHES_0_M1', 'j-update-sales')
    );

    --119, 124, 138
    v_param := 119;
    select
      nvl(sum(case when mc_jrnl.NaimProtsess like 'j-%' then mc_jrnl.DataOkonch - mc_jrnl.DataNach else 0 end), 0) as id_119,
      nvl(sum(case when mc_jrnl.NaimProtsess like 'agent-%' then mc_jrnl.DataOkonch - mc_jrnl.DataNach else 0 end), 0) as id_124,
      nvl(sum(case when mc_jrnl.NaimProtsess like 'advance-match%' then mc_jrnl.DataOkonch - mc_jrnl.DataNach else 0 end), 0) as id_138
    into v_value1, v_value2, v_value4
    from V$ASKZhurnal  mc_jrnl
    inner join V$ASKOperatsiya mc_oper on mc_jrnl.IdOper = mc_oper.Id
    where mc_oper.Id =
    (
      select max(Id)
      from V$ASKOperatsiya
      where trunc(DataOkonch) = v_date and VidOp='3'
    );

    put_report_value(v_id, 119, get_date_diff(v_value1));
    put_report_value(v_id, 124, get_date_diff(v_value2));
    put_report_value(v_id, 138, get_date_diff(v_value4));

    --120, 121, 122, 123
    v_param := 120;
    put_report_value(v_id, 120, get_mc_parameter_value(v_date, 'BELARUS_INVOICES', 'agent-match'));
    put_report_value(v_id, 121, get_mc_parameter_value(v_date, 'GTD_INVOICES', 'agent-match'));
    put_report_value(v_id, 122, get_mc_parameter_value(v_date, 'GTD_MATCHED', 'agent-match'));
    put_report_value(v_id, 123, get_mc_parameter_value(v_date, 'BELARUS_MATCHED', 'agent-match'));

     --125, 126, 128, 129
    v_param := 125;
    put_report_value(v_id, 125, get_mc_parameter_value(v_date, 'NO_BUYER_INPUT_ROWS', 'code25-match'));
    put_report_value
    (
      v_id,
      126,
      get_mc_parameter_value(v_date, 'NO_BUYER_INPUT_ROWS', 'code25-match')
      + get_mc_parameter_value(v_date, 'MATCHES_NO_BUYER_11', 'update-sales')
      + get_mc_parameter_value(v_date, 'MATCHES_NO_BUYER_1N', 'update-sales')
      + get_mc_parameter_value(v_date, 'MATCHES_NO_BUYER_M1', 'update-sales')
    );
    put_report_value(v_id, 128, get_mc_parameter_value(v_date, 'NO_SELLER_INPUT_ROWS', 'code25-match'));
    put_report_value
    (
      v_id,
      129,
      get_mc_parameter_value(v_date, 'NO_SELLER_INPUT_ROWS', 'code25-match')
      + get_mc_parameter_value(v_date, 'MATCHES_NO_SELLER_11', 'update-sales')
      + get_mc_parameter_value(v_date, 'MATCHES_NO_SELLER_1N', 'update-sales')
      + get_mc_parameter_value(v_date, 'MATCHES_NO_SELLER_M1', 'update-sales')
    );

    --131, 132, 133, 134
    v_param := 131;
    v_group := 7;
    v_num_1 := 0;
    v_num_2 := 0;
    v_num_3 := 0;
    v_num_4 := 0;
    while v_group >= 0
    loop
      v_num_1 := v_num_1 + get_mc_parameter_value(v_date, 'MATCH_INPUT_ROWS', 'match', v_group);
      v_num_2 := v_num_2 + get_mc_parameter_value(v_date, 'MATCHES_' || v_group || '_11', 'update-sales', v_group);
      v_num_3 := v_num_3 + get_mc_parameter_value(v_date, 'MATCHES_' || v_group || '_1N', 'update-sales', v_group);
      v_num_4 := v_num_4 + get_mc_parameter_value(v_date, 'MATCHES_' || v_group || '_M1', 'update-sales', v_group);
      v_group := v_group - 1;
    end loop;

    put_report_value(v_id, 131, v_num_1);
    put_report_value(v_id, 132, v_num_2);
    put_report_value(v_id, 133, v_num_3);
    put_report_value(v_id, 134, v_num_4);

    --135
    v_param := 135;
    select
      nvl(sum(case when mc_jrnl.NaimProtsess like 'match%' then mc_jrnl.DataOkonch - mc_jrnl.DataNach else 0 end), 0) as id_135
    into v_value1
    from V$ASKZhurnal mc_jrnl
    inner join V$ASKOperatsiya mc_oper on mc_jrnl.IdOper = mc_oper.Id
    where mc_jrnl.Iteratsiya in (0, 1, 2, 3, 4, 5, 6, 7)
      and mc_oper.Id =
      (
        select max(Id)
        from V$ASKOperatsiya
        where trunc(DataOkonch) = v_date and VidOp ='3'
      );

    put_report_value(v_id, 135, v_value1);

    --136, 137
    v_param := 136;
    put_report_value(v_id, 136, get_mc_parameter_value(v_date, 'MATCH_INPUT_ROWS', 'advance-match'));
    put_report_value(v_id, 137,
      get_mc_parameter_value(v_date, 'MATCHES_ADVANCE_11', 'update-sales')
      + get_mc_parameter_value(v_date, 'MATCHES_ADVANCE_1N', 'update-sales')
      + get_mc_parameter_value(v_date, 'MATCHES_ADVANCE_M1', 'update-sales'));

    --161, 162
    v_param := 161;
    SELECT count(*) INTO v_num_1 FROM V$ASKZIPFAJL WHERE STATUS = 1 AND trunc(DATASTATUSA) = v_date;
    put_report_value(v_id, 161, v_num_1);

    SELECT count(*) INTO v_num_1 FROM V$ASKZIPFAJL WHERE STATUS in (4,5) AND trunc(DATASTATUSA) = v_date;
    put_report_value(v_id, 162, v_num_1);
    
    --2x1 - 2x5
    v_param := 201;
    v_group := 0;
    while v_group < 8
    loop
      v_num_2 := 200 + v_group*10;
      put_report_value(v_id, v_num_2 + 1, get_mc_parameter_value(v_date, 'MATCH_INPUT_ROWS', 'match', v_group));
      put_report_value(v_id, v_num_2 + 2, get_mc_parameter_value(v_date, 'MATCHES_' || v_group || '_11', 'update-sales', v_group));
      put_report_value(v_id, v_num_2 + 3, get_mc_parameter_value(v_date, 'MATCHES_' || v_group || '_1N', 'update-sales', v_group));
      put_report_value(v_id, v_num_2 + 4, get_mc_parameter_value(v_date, 'MATCHES_' || v_group || '_M1', 'update-sales', v_group));
      select
        nvl(sum(case when mc_jrnl.NaimProtsess like 'match%' then mc_jrnl.DataOkonch - mc_jrnl.DataNach else 0 end), 0) as id_135
        into v_value1
      from V$ASKZhurnal mc_jrnl
      inner join V$ASKOperatsiya mc_oper on mc_jrnl.IdOper = mc_oper.Id
      where mc_jrnl.Iteratsiya = v_group
        and mc_oper.Id = (select max(Id) from V$ASKOperatsiya where trunc(DataOkonch) = v_date and VidOp ='3');
      put_report_value(v_id, v_num_2 + 5, v_value1);

      v_group := v_group + 1;
    end loop;

    commit;
    exception when others then
      rollback;
      Nds2$sys.LOG_ERROR('Technological report - Fill MC report', null, sqlcode, '#'||v_param||': '||sqlerrm);
  end;

  PROCEDURE SET_TIMER(p_start in varchar2, p_wait in varchar2)
  as
  begin
  UPDATE NDS2_MRR_USER.CONFIGURATION SET VALUE = P_WAIT WHERE PARAMETER = 'tr_timer_period_sec';
  UPDATE NDS2_MRR_USER.CONFIGURATION SET VALUE = P_START WHERE PARAMETER = 'tr_report_build_time_begin';
  COMMIT;
  end;
  
  PROCEDURE CALC(p_start in date, p_end in date)
  as
    v_date date;
  begin
    v_date := trunc(p_start);
    while v_date <= p_end
    loop
      CREATE_ITERATIONS_ON_DATE (v_date);
      
      FILL_MRR_REPORT(v_date);
      FILL_MC_REPORT(v_date);
      FILL_CC_REPORT(v_date);
      
      CLOSE_ITERATIONS_ON_DATE(v_date);
      v_date := v_date + 1;
    end loop;
  end;

end;
/
