create or replace package NDS2_MRR_USER.PAC$SOV is
    procedure SAVE_AGGREGATE_INVOICE_PARTS(
	  aggregate_invoice_key VARCHAR2,
	  aggregate_parts COLLECTION_OF_ROW_KEYS
	);
end PAC$SOV;
/
create or replace package body NDS2_MRR_USER.PAC$SOV is
    procedure SAVE_AGGREGATE_INVOICE_PARTS(
	  aggregate_invoice_key VARCHAR2,
	  aggregate_parts COLLECTION_OF_ROW_KEYS
	)
	as
	  cnt NUMBER;
	begin
    select count(*) into cnt from SOV_INVOICE_AGGREGATE a where a.aggregate_row_key = aggregate_invoice_key;
	  if (cnt = 0) then
	    for part_row_key in aggregate_parts.first .. aggregate_parts.last loop
	      insert into SOV_INVOICE_AGGREGATE (aggregate_row_key, invoice_row_key) values (aggregate_invoice_key, part_row_key);
	    end loop;
	  end if;
	end;
end PAC$SOV;
/
