﻿create or replace package NDS2_MRR_USER.PAC$MVIEW_MANAGER
as
procedure REFRESH_DECLARATONS;
end;
/
create or replace package body NDS2_MRR_USER.PAC$MVIEW_MANAGER
as
DECL_INSPECT_NAME constant varchar2(32) := 'MV$DECLARATIONINSPECT';
DECL_ANALYTIC_NAME constant varchar2(32) := 'MV$DECLARATION_HISTORY';
DECL_JRNL_NAME constant varchar2(32) := 'V$ASK_DECLANDJRNL';

procedure log(p_msg varchar2, p_type number)
as
pragma autonomous_transaction;
begin

  dbms_output.put_line(p_type||'-'||p_msg);

  insert into NDS2_MRR_USER.SYSTEM_LOG(ID, TYPE, SITE, ENTITY_ID, MESSAGE_CODE, MESSAGE, LOG_DATE)
  values(Seq_Sys_Log_Id.Nextval, p_type, 'PAC$MVIEW_MANAGER', null, 1, p_msg, sysdate);
  commit;
end;

procedure log_notification(p_msg varchar2)
as
begin
     log(p_msg, 0);
end;

procedure log_error(p_msg varchar2)
as
begin
     log(p_msg, 3);
end;

procedure compile_dependency( p_mviewName varchar2 )
 as
begin
  for line in (select * from all_mviews where owner = 'NDS2_MRR_USER' and mview_name = p_mviewName and compile_state <> 'VALID')
  loop
    execute immediate 'alter materialized view NDS2_MRR_USER.'||p_mviewName||' compile';
  end loop;
end;

procedure REFRESH_DECLARATONS
as
begin
log_notification('REFRESH_DECLARATONS: UPDATE');
compile_dependency(DECL_ANALYTIC_NAME);
compile_dependency(DECL_INSPECT_NAME);

begin
compile_dependency(DECL_JRNL_NAME);
dbms_mview.refresh(DECL_JRNL_NAME, 'C');
exception when others then
  log_error('REFRESH_DECLARATONS(DECL_JRNL_NAME):('||sqlcode||'):'||substr(sqlerrm, 256));
end;

begin
compile_dependency(DECL_JRNL_NAME);
compile_dependency(DECL_ANALYTIC_NAME);
dbms_mview.refresh(DECL_ANALYTIC_NAME, 'C');
exception when others then
  log_error('REFRESH_DECLARATONS(DECL_ANALYTIC_NAME):('||sqlcode||'):'||substr(sqlerrm, 256));
end;

begin
compile_dependency(DECL_JRNL_NAME);
compile_dependency(DECL_INSPECT_NAME);
dbms_mview.refresh(DECL_INSPECT_NAME, 'C');
exception when others then
  log_error('REFRESH_DECLARATONS(DECL_INSPECT_NAME):('||sqlcode||'):'||substr(sqlerrm, 256));
end;

exception when others then
  log_error('REFRESH_DECLARATONS:('||sqlcode||'):'||substr(sqlerrm, 256));
end;
end;
/
