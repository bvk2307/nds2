﻿create or replace package NDS2_MRR_USER.PAC$DECLARATION is

procedure P$BOOK_DATA_REQUEST_STATUS (
  pRequestId in BOOK_DATA_REQUEST.ID%type,
  pData out SYS_REFCURSOR
  ); -- Возвращает статус запроса получения данных книги декларации

procedure P$BOOK_DATA_REQUEST_EXISTS (
  pRequestId out BOOK_DATA_REQUEST.ID%type,
  pStatus out BOOK_DATA_REQUEST.Status%type,
  pInn in BOOK_DATA_REQUEST.INN%type,
  pCorrectionNumber in BOOK_DATA_REQUEST.CORRECTIONNUMBER%type,
  pPartitionNumber in BOOK_DATA_REQUEST.PARTITIONNUMBER%type,
  pPeriod in BOOK_DATA_REQUEST.PERIOD%type,
  pYear in BOOK_DATA_REQUEST.YEAR%type
 ); -- Возвращает идентификатор запроса данных с/ф по параметрам запроса


 procedure P$NDSDISCREPANCY_REQ_STATUS (
  pRequestId in DISCREPANCY_REQUEST.ID%type,
  pData out SYS_REFCURSOR
  ); -- Возвращает статус запроса расхождений по декларации


procedure P$NDSDISCREPANCY_REQ_EXISTS (
  pRequestId out DISCREPANCY_REQUEST.ID%type,
  pInn in DISCREPANCY_REQUEST.INN%type,
  pCorrectionNumber in DISCREPANCY_REQUEST.CORRECTION_NUMBER%type,
  pPeriod in DISCREPANCY_REQUEST.PERIOD%type,
  pYear in DISCREPANCY_REQUEST.YEAR%type,
  pKind in DISCREPANCY_REQUEST.KIND%type,
  pType in DISCREPANCY_REQUEST.TYPE%type
 ); -- Возвращает идентификатор запроса расхождений по декларации

end PAC$DECLARATION;
/
create or replace package body NDS2_MRR_USER.PAC$DECLARATION is

procedure P$BOOK_DATA_REQUEST_STATUS (
  pRequestId in BOOK_DATA_REQUEST.ID%type,
  pData out SYS_REFCURSOR
  )
as
begin

  open pData for select dict.* from REQUEST_STATUS dict join BOOK_DATA_REQUEST req on req.STATUS = dict.ID where req.ID = pRequestId;

end;

procedure P$BOOK_DATA_REQUEST_EXISTS (
  pRequestId out BOOK_DATA_REQUEST.ID%type,
  pStatus out BOOK_DATA_REQUEST.Status%type,
  pInn in BOOK_DATA_REQUEST.INN%type,
  pCorrectionNumber in BOOK_DATA_REQUEST.CORRECTIONNUMBER%type,
  pPartitionNumber in BOOK_DATA_REQUEST.PARTITIONNUMBER%type,
  pPeriod in BOOK_DATA_REQUEST.PERIOD%type,
  pYear in BOOK_DATA_REQUEST.YEAR%type
 )
as
begin 
 select ID, status
 into pRequestId, pStatus
 from
   (
     select ID, status
     from BOOK_DATA_REQUEST
     where
      INN = pInn 
      AND CORRECTIONNUMBER = pCorrectionNumber 
      AND PARTITIONNUMBER = pPartitionNumber 
      AND PERIOD = pPeriod 
      AND YEAR = pYear
      AND STATUS <> 9
      order by requestdate desc
   ) T
  where ROWNUM = 1;

 exception
   when NO_DATA_FOUND then
      pRequestId := null;
      pStatus := null;
end;


 procedure P$NDSDISCREPANCY_REQ_STATUS (
  pRequestId in DISCREPANCY_REQUEST.ID%type,
  pData out SYS_REFCURSOR
  )
as
begin

  open pData for select dict.* from REQUEST_STATUS dict join DISCREPANCY_REQUEST req on req.STATUS = dict.ID where req.ID = pRequestId;

end;


procedure P$NDSDISCREPANCY_REQ_EXISTS (
  pRequestId out DISCREPANCY_REQUEST.ID%type,
  pInn in DISCREPANCY_REQUEST.INN%type,
  pCorrectionNumber in DISCREPANCY_REQUEST.CORRECTION_NUMBER%type,
  pPeriod in DISCREPANCY_REQUEST.PERIOD%type,
  pYear in DISCREPANCY_REQUEST.YEAR%type,
  pKind in DISCREPANCY_REQUEST.KIND%type,
  pType in DISCREPANCY_REQUEST.TYPE%type
 )
as
begin

 select ID
 into pRequestId
 from DISCREPANCY_REQUEST
 where
  INN = pInn AND CORRECTION_NUMBER = pCorrectionNumber AND KIND = pKind AND TYPE = pType AND PERIOD = pPeriod AND YEAR = pYear
  AND STATUS <> 9 AND ROWNUM = 1;

 exception
   when NO_DATA_FOUND then
      pRequestId := null;
end;

end PAC$DECLARATION;
/
