﻿CREATE TABLE NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY
(
  PARAMETER_ID      NUMBER,
  IS_MANUAL         NUMBER(1),
  CONSTRAINT FK$SEL_FILTER_APPLICABILITY FOREIGN KEY (PARAMETER_ID) REFERENCES NDS2_MRR_USER.SELECTION_FILTER_PARAMETER(ID)
);