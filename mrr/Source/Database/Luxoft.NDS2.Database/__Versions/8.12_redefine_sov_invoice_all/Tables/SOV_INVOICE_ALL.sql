﻿alter table NDS2_MRR_USER.SOV_INVOICE_ALL modify PRICE_BUY_AMOUNT NUMBER(22,2);
alter table NDS2_MRR_USER.SOV_INVOICE_ALL modify PRICE_BUY_NDS_AMOUNT NUMBER(22,2);
alter table NDS2_MRR_USER.SOV_INVOICE_ALL modify PRICE_SELL NUMBER(22,2);
alter table NDS2_MRR_USER.SOV_INVOICE_ALL modify PRICE_SELL_IN_CURR NUMBER(22,2);
alter table NDS2_MRR_USER.SOV_INVOICE_ALL modify PRICE_SELL_18 NUMBER(22,2);
alter table NDS2_MRR_USER.SOV_INVOICE_ALL modify PRICE_SELL_10 NUMBER(22,2);
alter table NDS2_MRR_USER.SOV_INVOICE_ALL modify PRICE_SELL_0 NUMBER(22,2);
alter table NDS2_MRR_USER.SOV_INVOICE_ALL modify PRICE_NDS_18 NUMBER(22,2);
alter table NDS2_MRR_USER.SOV_INVOICE_ALL modify PRICE_NDS_10 NUMBER(22,2);
alter table NDS2_MRR_USER.SOV_INVOICE_ALL modify PRICE_TAX_FREE NUMBER(22,2);
alter table NDS2_MRR_USER.SOV_INVOICE_ALL modify PRICE_TOTAL NUMBER(22,2);
alter table NDS2_MRR_USER.SOV_INVOICE_ALL modify PRICE_NDS_TOTAL NUMBER(22,2);
alter table NDS2_MRR_USER.SOV_INVOICE_ALL modify DIFF_CORRECT_DECREASE NUMBER(22,2);
alter table NDS2_MRR_USER.SOV_INVOICE_ALL modify DIFF_CORRECT_INCREASE NUMBER(22,2);
alter table NDS2_MRR_USER.SOV_INVOICE_ALL modify DIFF_CORRECT_NDS_DECREASE NUMBER(22,2);
alter table NDS2_MRR_USER.SOV_INVOICE_ALL modify DIFF_CORRECT_NDS_INCREASE NUMBER(22,2);
alter table NDS2_MRR_USER.SOV_INVOICE_ALL modify PRICE_NDS_BUYER NUMBER(22,2);
