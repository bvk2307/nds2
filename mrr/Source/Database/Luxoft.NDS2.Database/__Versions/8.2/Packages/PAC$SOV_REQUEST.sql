create or replace package NDS2_MRR_USER.PAC$SOV_REQUEST is

 procedure P$GET_INVOICE_REQUEST (
   pId in SOV_INVOICE_REQUEST.ID%type,
   pResult out SYS_REFCURSOR);
   
 procedure P$SEARCH_INVOICE_REQUEST (
   pDeclarationId in SOV_INVOICE_REQUEST.ZIP%type,
   pPartition in SOV_INVOICE_REQUEST.PARTITION_NUMBER%type,
   pResult out SYS_REFCURSOR
   );
   

end PAC$SOV_REQUEST;
/
create or replace package body NDS2_MRR_USER.PAC$SOV_REQUEST is

 procedure P$GET_INVOICE_REQUEST (
   pId in SOV_INVOICE_REQUEST.ID%type,
   pResult out SYS_REFCURSOR)
 as
 begin
   open pResult for select id, status from SOV_INVOICE_REQUEST where id = pId;
 end;
   
 procedure P$SEARCH_INVOICE_REQUEST (
   pDeclarationId in SOV_INVOICE_REQUEST.ZIP%type,
   pPartition in SOV_INVOICE_REQUEST.PARTITION_NUMBER%type,
   pResult out SYS_REFCURSOR
   )
 as
 begin
   
   open pResult for
   select id, status
   from SOV_INVOICE_REQUEST
   where zip = pDeclarationId and partition_number = pPartition
   order by requestdate desc;
   
 end;
   
end PAC$SOV_REQUEST;
/
