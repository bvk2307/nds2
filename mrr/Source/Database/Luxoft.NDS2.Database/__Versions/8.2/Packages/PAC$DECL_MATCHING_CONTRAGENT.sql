﻿CREATE OR REPLACE PACKAGE NDS2_MRR_USER."PAC$DECL_MATCHING_CONTRAGENT" IS

procedure START_WORK;
procedure P$DO_INSERT_DECL_CONTR;
procedure P$DO_UPDATE_DECL_CONTR;

END PAC$DECL_MATCHING_CONTRAGENT;
/

CREATE OR REPLACE PACKAGE BODY  NDS2_MRR_USER."PAC$DECL_MATCHING_CONTRAGENT" IS

procedure START_WORK
  as
begin
  NDS2$SYS.LOG_INFO(
  P_SITE => 'PAC$DECL_MATCHING_CONTRAGENT.START_WORK',   P_ENTITY_ID => null,
  P_MESSAGE_CODE => SQLCODE,   P_MESSAGE => 'starting');
  begin
    P$DO_INSERT_DECL_CONTR;
    P$DO_UPDATE_DECL_CONTR;
  exception when others then null;
  end;
end;

procedure P$DO_INSERT_DECL_CONTR
as
    pInsertDateBegin DATE;
    pInsertDateEnd DATE;
    pParameter VARCHAR2(128);
    pDescription VARCHAR2(128);
begin
    pParameter := 'seod_decl_key_last_processed';
    pDescription := 'дата последнего запуска процесса создания ключей контрагента';
    pInsertDateEnd := sysdate + 1;

    select max(to_date(value, 'DD/MM/YYYY')) into pInsertDateBegin from configuration where parameter = pParameter;

    if pInsertDateBegin is null then
      select max(insert_date) into pInsertDateBegin from seod_declaration;
      if pInsertDateBegin is null then pInsertDateBegin := sysdate - 2; end if;
    end if;

    INSERT INTO DECL_MATCHING_CONTRAGENT dmc (ID, ZIP, IS_ASK, KEY_DECL_CONTR, SEOD_DECL_ID, insert_date, correction_number)
    SELECT NDS2_MRR_USER.SEQ_DECL_MATCHING_CONTRAGENT.NEXTVAL
           ,null as ZIP
           ,0 as IS_ASK
           ,(to_number(sed.fiscal_year) * 100000000000000 + tm.month * 1000000000000 + to_number(sed.inn)) as key_decl_contr
           ,sed.ID as SEOD_DECL_ID
           ,sysdate
           ,correction_number
    FROM SEOD_DECLARATION sed
    JOIN DICT_TAX_PERIOD_MONTH tm on tm.tax_period = sed.tax_period
    WHERE trunc(INSERT_DATE) >= trunc(pInsertDateBegin);

    merge into CONFIGURATION c
    using (select pParameter as parameter from dual) par
      on
      (
        par.parameter = c.parameter
      )
    when matched then
      update set c.value = to_char(pInsertDateEnd, 'DD/MM/YYYY'), c.default_value = to_char(pInsertDateEnd, 'DD/MM/YYYY')
    when not matched then
      insert (parameter, value, default_value, description)
      values (pParameter, to_char(pInsertDateEnd, 'DD/MM/YYYY'), to_char(pInsertDateEnd, 'DD/MM/YYYY'), pDescription);

    commit;
end;

procedure P$DO_UPDATE_DECL_CONTR
as
pUpdateDateBefore DATE;
begin
  pUpdateDateBefore := sysdate;
  update DECL_MATCHING_CONTRAGENT set (ZIP, IS_ASK, update_Date) =
  (
    select sd.ZIP, 1, sysdate
    from DECL_MATCHING_CONTRAGENT dmc
    join SEOD_DECLARATION sed on sed.ID = dmc.SEOD_DECL_ID
    join V$ASK_DECLANDJRNL sd on SD.PERIOD = SED.TAX_PERIOD and SD.OTCHETGOD = SED.FISCAL_YEAR and SD.INNNP = SED.INN and SD.NOMKORR = SED.CORRECTION_NUMBER
    where IS_ASK = 0 and dmc.id = DECL_MATCHING_CONTRAGENT.ID
  )
  WHERE EXISTS (select sd.ZIP, 1
    from DECL_MATCHING_CONTRAGENT dmc
    join SEOD_DECLARATION sed on sed.ID = dmc.SEOD_DECL_ID
    join V$ASK_DECLANDJRNL sd on SD.PERIOD = SED.TAX_PERIOD and SD.OTCHETGOD = SED.FISCAL_YEAR and SD.INNNP = SED.INN and SD.NOMKORR = SED.CORRECTION_NUMBER
    where IS_ASK = 0 and dmc.id = DECL_MATCHING_CONTRAGENT.ID);

    -- установим is_last_correction_ask = null
    update DECL_MATCHING_CONTRAGENT set is_last_correction_ask = null
    where exists
    (
        select dmc.id
        from DECL_MATCHING_CONTRAGENT dmc
        where exists
        (
            select b.key_decl_contr from DECL_MATCHING_CONTRAGENT b
            where update_Date >= pUpdateDateBefore and dmc.key_decl_contr = b.key_decl_contr
        )
        and dmc.id = DECL_MATCHING_CONTRAGENT.ID
    );

    -- установим is_last_correction_ask = 1 (у последней корректировки у которой есть ZIP, IS_ASK = 1)
    update DECL_MATCHING_CONTRAGENT d set is_last_correction_ask = 1
    where exists
    (
        select dmc.id from DECL_MATCHING_CONTRAGENT dmc
        join
        (
            select dmc.seod_decl_id
                   ,dmc.key_decl_contr
                   ,max(dmc.is_ask) as is_ask
                   ,max(dmc.correction_number) as correction_number
                   ,max(dmc.update_date) as update_date
                   ,max(case when dmc.is_ask = 1 then id end) as id
            from DECL_MATCHING_CONTRAGENT dmc
            where exists
            (
                select b.key_decl_contr from DECL_MATCHING_CONTRAGENT b
                where update_Date >= pUpdateDateBefore and dmc.key_decl_contr = b.key_decl_contr
            )
            group by dmc.seod_decl_id, dmc.key_decl_contr
        ) m on m.seod_decl_id = dmc.seod_decl_id and m.key_decl_contr = dmc.key_decl_contr
            and m.correction_number = dmc.correction_number and m.update_date = dmc.update_date
            and m.id = dmc.id and m.is_ask = dmc.is_ask and m.is_ask = 1
        where d.id = dmc.id
   );

  commit;
end;

END "PAC$DECL_MATCHING_CONTRAGENT";
/