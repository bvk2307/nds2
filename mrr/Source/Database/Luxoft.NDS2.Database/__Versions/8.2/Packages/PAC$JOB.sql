﻿CREATE OR REPLACE PACKAGE NDS2_MRR_USER.PAC$JOB
AS
procedure APPROVE_SELECTION;
procedure SEND_SELECTION;
procedure ANNULE_SELECTION;
procedure START_WORK;
END;
/
CREATE OR REPLACE PACKAGE BODY NDS2_MRR_USER.PAC$JOB
AS

SEL_FOR_APPROVE_STATUS constant number(1) := 3;
SEL_APPROVED_STATUS constant number(2) := 6;
SEL_PREPARE_TO_SEND_STATUS constant number(2) := 4;
DISCREP_APPROVED_STATUS constant number(1):= 3;
SYSTEM_NAME constant varchar(32) := 'система';

procedure APPROVE_SELECTION
  as
v_option char(1);
type tmptab is table of number;
id_table tmptab;
pragma autonomous_transaction;
begin

  select c.value into v_option
  from dual
  left join configuration c on c.parameter='auto_approve_selections';

  NDS2$SYS.LOG_INFO('PAC$JOB.APPROVE_SELECTION', v_option, SQLCODE, 'option value');
  if v_option = 'Y' then
    begin
      update selection set status = SEL_APPROVED_STATUS, chief = SYSTEM_NAME where status = SEL_FOR_APPROVE_STATUS
      returning id  bulk collect into id_table;
        if (id_table.COUNT > 0) then
          FOR I IN id_table.FIRST .. id_table.LAST
          LOOP
            Nds2$Selections.UPD_HIST_SELECTION_STATUS(id_table(I), SEL_APPROVED_STATUS);

            insert into Action_History (id, cd, status, change_date, action_comment, user_name, action)
            values(SEQ_ACTION_HISTORY.nextval, id_table(I), SEL_APPROVED_STATUS, sysdate, 'автоматическое согласование', SYSTEM_NAME, 4);

          END LOOP;
        end if;
        commit;
    end;
  else
      rollback;
  end if;
  exception when others then
  begin
    rollback;
    NDS2$SYS.LOG_ERROR('PAC$JOB.APPROVE_SELECTION', null, null, SUBSTR(SQLERRM, 1 , 2040));
  end;
end;

procedure SEND_SELECTION
as
v_option char(1);
type tmptab is table of number;
id_table tmptab;
v_invoice_req_id number;
v_exist_records number := 0;
pragma autonomous_transaction;
begin

  select c.value into v_option
  from dual
  left join configuration c on c.parameter='auto_send_selections';

  NDS2$SYS.LOG_INFO('PAC$JOB.SEND_SELECTION', v_option, null, 'option value');
  if v_option = 'Y' then
    begin
      select count(1) into v_exist_records from selection where status = SEL_APPROVED_STATUS;
      if v_exist_records > 0 then

        update selection set status = SEL_PREPARE_TO_SEND_STATUS, chief = SYSTEM_NAME 
        where status = SEL_APPROVED_STATUS
        returning id bulk collect into id_table;

        NDS2$SYS.LOG_INFO('PAC$JOB.SEND_SELECTION', id_table.count, null, 'ready to send data');

        begin
          FOR I IN id_table.FIRST .. id_table.LAST
          LOOP
            for discrepLine in (
                WITH tInvoice as
                (
                  SELECT
                    v_invoice_req_id,
                    d.invoice_chapter,
                    d.invoice_rk,
                    d.invoice_contractor_chapter,
                    d.invoice_contractor_rk
                  FROM SELECTION s
                  INNER JOIN SELECTION_DISCREPANCY sd ON sd.selection_id = s.id AND sd.Is_In_Process = 1
                  INNER JOIN SOV_DISCREPANCY d ON d.id = sd.discrepancy_id
                  WHERE s.id = id_table(I)
                )
                select
                    v_invoice_req_id,
                    invoice_chapter,
                    invoice_rk
                from tInvoice
                union
                select
                    v_invoice_req_id,
                    invoice_contractor_chapter,
                    invoice_contractor_rk
                from tInvoice
            )
            loop
              insert into INVOICE_REQUEST_DATA values(v_invoice_req_id, discrepLine.invoice_chapter, discrepLine.Invoice_Rk);
            end loop;

            Nds2$Selections.UPD_HIST_SELECTION_STATUS(id_table(I), SEL_PREPARE_TO_SEND_STATUS);

            insert into Action_History (id, cd, status, change_date, action_comment, user_name, action)
            values(SEQ_ACTION_HISTORY.nextval, id_table(I), SEL_PREPARE_TO_SEND_STATUS, sysdate, 'автоматическая отправка в ЭОД', SYSTEM_NAME, 4);


          END LOOP;
          commit;
      end;
      else
        rollback; --даже если UPDATE не обновил записей, то транзакция будет считаться активной
      end if;
    end;
  end if;
  exception when others then
  begin
    rollback;
    NDS2$SYS.LOG_ERROR('PAC$JOB.SEND_SELECTION', null, null, SUBSTR(SQLERRM, 1 , 2040));
  end;
end;


procedure ANNULE_SELECTION
as
pragma autonomous_transaction;
begin
  for line in (
                select
                s.id,
                count(case when d.status = 5 then 1 end) as count_of_fixed,
                count(case when d.status <> 5 then 1 end) as count_of_active
                from sov_discrepancy d
                inner join selection_discrepancy sd on sd.discrepancy_id = d.id
                inner join selection s on s.id = sd.selection_id
                where sd.is_in_process = 1 and s.status = 10
                group by s.id)
   loop
      if line.count_of_fixed > 0 and line.count_of_active = 0 then
        begin
          update selection set status = 12 where id = line.id;
          insert into Action_History (id, cd, status, change_date, action_comment, user_name, action)
          values(SEQ_ACTION_HISTORY.nextval, line.id, 10, sysdate, 'аннулирование выборки', 'система', 4);
        end;
       end if;
   end loop;
  commit;
  exception when others then
  begin
    rollback;
    NDS2$SYS.LOG_ERROR('PAC$JOB.ANNULE_SELECTION', null, null, SUBSTR(SQLERRM, 1 , 2040));
  end;
end;

procedure START_WORK
  as
begin
  NDS2$SYS.LOG_INFO(
  P_SITE => 'PAC$JOB.START_WORK',   P_ENTITY_ID => null,
  P_MESSAGE_CODE => SQLCODE,   P_MESSAGE => 'starting');
  --Аннулирование выборки
  begin
    ANNULE_SELECTION;
  exception when others then null; --Log
  end;
  --Автосогласование выборок
  begin
    APPROVE_SELECTION;
  exception when others then null; --Log
  end;
  --Автоотправка выборок
  begin
    SEND_SELECTION;
  exception when others then null; --Log
  end;
end;

END;
/
