create table NDS2_MRR_USER.TMP_HRZ_DECL_INFO
(
  ch8_deals_amnt_total NUMBER(20,2),
  ch9_deals_amnt_total NUMBER(20,2),
  decl_sign            VARCHAR2(23),
  compensation_amnt    NUMBER,
  gap_discrep_amnt     NUMBER(20,2),
  ch8_nds              NUMBER,
  ch9_nds              NUMBER,
  share_nds_amnt       NUMBER,
  inn                  VARCHAR2(12) not null,
  fiscal_year          VARCHAR2(4) not null,
  tax_period           VARCHAR2(2) not null,
  is_active            NUMBER
);

