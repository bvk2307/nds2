﻿create table NDS2_MRR_USER.DISCREPANCY_AGGREGATE (
        id not null,
        sov_id not null,
        decl_id,
        region_code not null,
        sono_code not null,
        inn null,
        name null,
        sur_code null,
        contractor_sur_code null,
        period not null,
        submit_date null,
        decl_sign not null,
        decl_total_amount not null,
        decl_total_count not null,
        decl_min_amount not null,
        decl_max_amount not null,
        decl_avg_amount not null,
        decl_purchase_count not null,
        decl_sales_count not null,
        decl_total_pvp not null,
        decl_max_pvp not null,
        decl_min_pvp not null,
        decl_sales_pvp not null,
        decl_purchase_pvp not null,
        amount  not null,
        pvp not null,
        type_code not null,
        compare_rule not null,
        create_date null,
        invoice_chapter,
        invoice_rk,
        invoice_contractor_chapter null,
        invoice_contractor_rk null)
pctfree 0
nologging
as select
        t.id,
        t_all.sov_id,
        t_all.decl_id,
        substr(seod_decl.sono_code, 1, 2),
        seod_decl.sono_code,
        t_all.buyer_inn,
        t_all.np_name,
        nvl(buyer_sur.sign_code, sur.code),
        nvl(seller_sur.sign_code, sur.code),
        t_all.tax_period,
        seod_decl.eod_date,
        1,
        nvl(sum_decl.discrep_currency_amnt,0),
        nvl(sum_decl.discrep_currency_count,0),
        nvl(sum_decl.discrep_min_amnt,0),
        nvl(sum_decl.discrep_max_amnt,0),
        nvl(sum_decl.discrep_avg_amnt,0),
        nvl(sum_decl.discrep_buy_book_amnt,0),
        nvl(sum_decl.discrep_sell_book_amnt,0),
        nvl(sum_decl.pvp_total_amnt,0),
        nvl(sum_decl.pvp_discrep_max_amnt,0),
        nvl(sum_decl.pvp_discrep_min_amnt,0),
        nvl(sum_decl.pvp_sell_book_amnt,0),
        nvl(sum_decl.pvp_buy_book_amnt,0),
        t_all.amnt,
        t_all.amount_pvp,
        t_all.type,
        t_all.rule_num,
        t_all.create_date,
        t_all.invoice_chapter,
        t_all.invoice_rk,
        t_all.invoice_contractor_chapter,
        t_all.invoice_contractor_rk
    from NDS2_MRR_USER.SOV_DISCREPANCY_ALL t_all
      join NDS2_MRR_USER.SOV_DISCREPANCY t on t.sov_id = t_all.sov_id
      join
      (
             select row_number() over (partition by a.nds2_id order by a.correction_number desc, a.insert_date desc, a.id desc) rn,
                    a.* from NDS2_MRR_USER.SEOD_DECLARATION a
      ) seod_decl on seod_decl.nds2_id = t_all.decl_id and rn = 1
      join NDS2_MRR_USER.V$ASKDEKL ask_decl
           on     ask_decl.period = seod_decl.tax_period
              and ask_decl.otchetgod = seod_decl.fiscal_year
              and ask_decl.innnp = seod_decl.inn
              and ask_decl.nomkorr = seod_decl.correction_number
      join NDS2_MRR_USER.SOV_DECLARATION_INFO sum_decl on sum_decl.declaration_version_id = ask_decl.zip
      join NDS2_MRR_USER.DICT_SUR sur on sur.is_default = 1
      left join NDS2_MRR_USER.EXT_SUR buyer_sur
           on     buyer_sur.inn = t_all.buyer_inn
              and buyer_sur.fiscal_year = t_all.tax_year
              and buyer_sur.fiscal_period = t_all.tax_period
      left join NDS2_MRR_USER.EXT_SUR seller_sur
           on     seller_sur.inn = t_all.buyer_inn
              and seller_sur.fiscal_year = t_all.tax_year
              and seller_sur.fiscal_period = t_all.tax_period;

create table NDS2_MRR_USER.selection_discrepancy_n
as
select
sd.discrepancy_id,
sd.is_in_process,
sd.selection_id,
da.decl_id,
da.amount as Amount,
da.pvp as AmountPVP,
da.sov_id as sov_id,
da.create_date as FoundDate,
da.type_code as type,
da.compare_rule as rule,
1 as stage,
da.inn as TaxPayerInn,
da.period as TaxPeriod,
da.sur_code as SURCode,
da.decl_sign as TaxPayerDeclSign,
1 as Status
from NDS2_MRR_USER.selection_discrepancy sd
inner join NDS2_MRR_USER.discrepancy_aggregate da on da.id = sd.discrepancy_id where rownum <1;

drop table NDS2_MRR_USER.selection_discrepancy;
alter table NDS2_MRR_USER.selection_discrepancy_n rename to selection_discrepancy;

create table NDS2_MRR_USER.sov_discrepancy_build
PCTFREE 0
NOLOGGING
as
  select 
  sd.ID
  ,sd.SOV_ID
  ,sd.CREATE_DATE
  ,sd.TYPE
  ,sd.COMPARE_KIND
  ,case when sda.RULE_GROUP is null then sd.RULE_GROUP else sda.RULE_GROUP end as RULE_GROUP
  ,case when sda.DEAL_AMNT is null then sd.DEAL_AMNT else sda.DEAL_AMNT end as DEAL_AMNT
  ,case when sda.AMNT is null then sd.AMNT else sda.AMNT end as AMNT
  ,case when sda.AMOUNT_PVP is null then sd.AMOUNT_PVP else sda.AMOUNT_PVP end as AMOUNT_PVP
  ,sd.INVOICE_CHAPTER
  ,case when sda.INVOICE_RK is null then sd.INVOICE_RK else sda.INVOICE_RK end as INVOICE_RK
  ,case when sda.DECL_ID is null then sd.DECL_ID else sda.DECL_ID end as DECL_ID
  ,case when sda.INVOICE_CONTRACTOR_CHAPTER is null then sd.INVOICE_CONTRACTOR_CHAPTER else sda.INVOICE_CONTRACTOR_CHAPTER end as INVOICE_CONTRACTOR_CHAPTER
  ,case when sda.INVOICE_CONTRACTOR_RK is null then sd.INVOICE_CONTRACTOR_RK else sda.INVOICE_CONTRACTOR_RK end as INVOICE_CONTRACTOR_RK
  ,case when sda.DECL_CONTRACTOR_ID is null then sd.DECL_CONTRACTOR_ID else sda.DECL_CONTRACTOR_ID end as DECL_CONTRACTOR_ID
  ,case when sda.sov_id is not null then 1 else 0 end as STATUS
  ,sd.STAGE
  ,sd.USER_COMMENT
  ,sd.SIDE_PRIMARY_PROCESSING
  ,case when sda.RULE_NUM is null then sd.RULE_NUM else sda.RULE_NUM end as RULE_NUM
from NDS2_MRR_USER.sov_discrepancy sd
left join NDS2_MRR_USER.sov_discrepancy_all sda on sda.sov_id = sd.sov_id;

insert /*+ APPEND*/ into  NDS2_MRR_USER.sov_discrepancy_build 
select 
NDS2_MRR_USER.seq_discrepancy_id.nextval
,sda.SOV_ID
,sda.CREATE_DATE
,sda.TYPE
,sda.COMPARE_KIND
,sda.RULE_GROUP
,sda.DEAL_AMNT
,sda.AMNT
,sda.AMOUNT_PVP
,sda.INVOICE_CHAPTER
,sda.INVOICE_RK
,sda.DECL_ID
,sda.INVOICE_CONTRACTOR_CHAPTER
,sda.INVOICE_CONTRACTOR_RK
,sda.DECL_CONTRACTOR_ID
,1
,sda.STAGE
,null
,sda.SIDE_PRIMARY_PROCESSING
,sda.RULE_NUM
from NDS2_MRR_USER.sov_discrepancy_all sda
left join  NDS2_MRR_USER.sov_discrepancy sd on sda.sov_id = sd.sov_id
where sd.id is null;

commit;

alter table NDS2_MRR_USER.sov_discrepancy rename to t3_sov_discrepancy;
alter table NDS2_MRR_USER.sov_discrepancy_build rename to sov_discrepancy;

create table NDS2_MRR_USER.selection_discrepancy_n
as
select
sd.discrepancy_id,
sd.is_in_process,
sd.selection_id,
sd.decl_id,
sd.amount,
sd.amountpvp,
sd.sov_id,
sd.founddate,
sd.type,
sd.rule,
sd.stage,
sd.taxpayerinn,
sd.taxperiod,
sd.surcode,
sd.taxpayerdeclsign,
sd.status,
d.invoice_rk,
d.invoice_chapter,
d.invoice_contractor_rk,
d.invoice_contractor_chapter
from 
NDS2_MRR_USER.selection_discrepancy sd
inner join NDS2_MRR_USER.sov_discrepancy d on d.id = sd.discrepancy_id;

alter table NDS2_MRR_USER.selection_discrepancy rename to t3_selection_discrepancy;
alter table NDS2_MRR_USER.selection_discrepancy_n rename to selection_discrepancy;
