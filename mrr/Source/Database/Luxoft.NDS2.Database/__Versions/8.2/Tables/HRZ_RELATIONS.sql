-- Create table
create table NDS2_MRR_USER.HRZ_RELATIONS
(
  id                  NUMBER,
  inn_1               VARCHAR2(12 CHAR),
  inn_2               VARCHAR2(12 CHAR),
  year                NUMBER(4),
  period              NUMBER,
  ba                  NUMBER,
  sa                  NUMBER,
  nba                 NUMBER,
  nsa                 NUMBER,
  gap_amount          NUMBER,
  total_purchase_amnt NUMBER(20,2),
  total_sales_amnt    NUMBER(20,2),
  deduction_nds       NUMBER,
  calc_nds            NUMBER,
  declaration_type    VARCHAR2(23),
  total_nds           NUMBER,
  gap_discrep_amnt    NUMBER(20,2),
  share_nds_amnt      NUMBER,
  nba_order           NUMBER,
  nsa_order           NUMBER
);
