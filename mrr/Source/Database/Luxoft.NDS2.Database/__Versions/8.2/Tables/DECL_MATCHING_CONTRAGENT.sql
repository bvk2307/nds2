﻿alter table NDS2_MRR_USER.DECL_MATCHING_CONTRAGENT add correction_number VARCHAR2(3);
alter table NDS2_MRR_USER.DECL_MATCHING_CONTRAGENT add is_last_correction_ask NUMBER(1);