-- Create table
create global temporary table NDS2_MRR_USER.HRZ_RELATIONS_TREE
(
  id                           NUMBER,
  parent_inn                   VARCHAR2(12 CHAR),
  parent_vat_deduction_total   NUMBER(19,2),
  parent_vat_calculation_total NUMBER(19,2),
  inn                          VARCHAR2(12 CHAR),
  lvl                          NUMBER(2),
  declaration_type             VARCHAR(24 CHAR),
  vat_total                    NUMBER(19,2),
  vat_deduction_total          NUMBER(19,2),
  vat_calculation_total        NUMBER(19,2),
  gap_discrepancy_total        NUMBER(19,2),
  gap_discrepancy              NUMBER(19,2),
  vat_deduction                NUMBER(19,2),
  vat_calculation              NUMBER(19,2) 
) 
on commit preserve rows;
