-- Create table
create table NDS2_MRR_USER.HRZ_RELATIONS_TOTAL
(
  inn         VARCHAR2(12 CHAR),
  year        NUMBER(4),
  period      NUMBER,
  sellers_cnt NUMBER,
  buyers_cnt  NUMBER
);
