create table NDS2_MRR_USER.SOV_INVOICE_REQUEST (
       ID number not null,
       ZIP number not null,
       PARTITION_NUMBER number not null,
       PRIORITY number not null,
       STATUS number not null,
       REQUESTDATE date not null,
       PROCESS_DATE date, 
       EXPORTED_ROWS number,
       MESSAGE varchar2(2048 CHAR));
