﻿create or replace force view NDS2_MRR_USER.V$ASKSVODZAP
as
select
   mc."Ид" as ID
  ,mc."ZIP" as ZIP
  ,mc."Индекс" as INDEKS
  ,mc."НомКорр" as NOMKORR
  ,mc."ПризнакАкт" as PRIZNAKAKT
  ,mc."ТипФайла" as TIPFAJLA
  ,mc."СумНДСПок" as SUMNDSPOK
  ,mc."СумНДСПокДЛ" as SUMNDSPOKDL
  ,mc."СтПрод18" as STPROD18
  ,mc."СтПрод10" as STPROD10
  ,mc."СтПрод0" as STPROD0
  ,mc."СумНДСПрод18" as SUMNDSPROD18
  ,mc."СумНДСПрод10" as SUMNDSPROD10
  ,mc."СтПрод" as STPROD
  ,mc."СтПродОсв" as STPRODOSV
  ,mc."СтПрод18ДЛ" as STPROD18DL
  ,mc."СтПрод10ДЛ" as STPROD10DL
  ,mc."СтПрод0ДЛ" as STPROD0DL
  ,mc."СумНДС18ДЛ" as SUMNDS18DL
  ,mc."СумНДС10ДЛ" as SUMNDS10DL
  ,mc."СтПродОсвДЛ" as STPRODOSVDL
  ,mc."ИдЗагрузка" as IdZagruzka
  ,mc."ИдЗамена" as IdZamena
  ,mc."ОшибкиЛК" as OshibkiLK
  ,mc."ОшибкиФК" as OshibkiFK
  ,mc."ИдДекл" as IdDekl
  ,mc."КолЗаписей" as KolZapisey
  ,mc."ПрНеобр0" as PrNeobr0
  ,mc."ПрНеобр1" as PrNeobr1
  ,mc."ИдАкт" as IdAkt
from NDS2_MC."ASKСводЗап" mc;
