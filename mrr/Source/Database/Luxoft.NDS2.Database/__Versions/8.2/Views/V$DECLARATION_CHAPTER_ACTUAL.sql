create or replace force view NDS2_MRR_USER.V$DECLARATION_CHAPTER_ACTUAL
as
select
       chapter_cur.zip as initialzip,
       chapter_cur.tipfajla as askchapter,
       nvl(chapter_act.zip, chapter_cur.zip) as actualzip,
       nvl(chapter_act.kolzapisey, nvl(chapter_cur.kolzapisey,0)) as invoicequantity,
       nvl(priority.priority,4) as priority
from
       V$ASKSVODZAP chapter_cur
       left join V$ASKSVODZAP chapter_act 
            on chapter_cur.IdAkt = chapter_act.ID
       left join BOOK_DATA_REQUEST_PRIORITY priority
            on priority.max_count >= nvl(chapter_act.kolzapisey, nvl(chapter_cur.kolzapisey,0))
               and priority.min_count <= nvl(chapter_act.kolzapisey, nvl(chapter_cur.kolzapisey,0));

       
