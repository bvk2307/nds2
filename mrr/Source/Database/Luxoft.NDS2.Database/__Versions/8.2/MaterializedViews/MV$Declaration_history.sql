﻿drop materialized view NDS2_MRR_USER.MV$Declaration_history;
create materialized view NDS2_MRR_USER.MV$Declaration_history 
nologging
build DEFERRED 
as
select
to_number(sed.tax_period||sed.fiscal_year||sed.inn||sed.correction_number) as nds2_decl_corr_id
,sed.nds2_id																				as ID
,sed.decl_reg_num																		as SEOD_DECL_ID
,SD.ID																					as ASK_DECL_ID
,SD.ZIP																					as DECLARATION_VERSION_ID
,decode((row_number() 
over (partition by sed.TAX_PERIOD, 
	sed.FISCAL_YEAR, 
	sed.INN 
	order by SED.CORRECTION_NUMBER desc)),1,1,0)										as IS_ACTIVE
,case when D.DECLARATION_VERSION_ID is not null 
		then 2 else (case when  SD.ID is not null then 1 else 0 end) 
end																						as ProcessingStage
    ,MAX(case when D.DECLARATION_VERSION_ID is not null then 2 else (case when  SD.ID is not null then 1 else 0 end) end)
       OVER(PARTITION BY sed.inn, sed.tax_period, sed.fiscal_year) as max_processing_stage
,d.UPDATE_DATE																			as UPDATE_DATE
,sed.INN																				as INN
,sed.KPP																				as KPP
,vtp.NAME																				as NAME
,vtp.ADDRESS																			as ADDRESS1
,'-'																						as ADDRESS2
,ssrf.s_code																			as REGION_CODE
,ssrf.s_name																			as REGION
,ssrf.s_code || '-' || ssrf.s_name														as REGION_NAME
,soun.s_code																			as SOUN_CODE
,soun.s_name																			as SOUN
,soun.s_code || '-' || soun.s_name														as SOUN_NAME
,DECODE(sed.TYPE, 0, vtp.CATEGORY, null)												as CATEGORY
,DECODE(sed.TYPE, 0, case vtp.CATEGORY when 1 then 'Крупнейший' else '' end, null)				as CATEGORY_RU
,sed.EOD_DATE																			as REG_DATE
,'-'																						as TAX_MODE
,sd.OKVED																				as OKVED_CODE
,'0'																					as CAPITAL
,sed.TAX_PERIOD																			as TAX_PERIOD
,sed.FISCAL_YEAR																		as FISCAL_YEAR
,dtp.description || ' ' || sed.FISCAL_YEAR												as FULL_TAX_PERIOD 
,DECODE(sed.TYPE, 0, sed.EOD_DATE, 1, sed.date_receipt, null)							as DECL_DATE
,DECODE(sed.TYPE, 0, 'Декларация', 1, 'Журнал', '')										as DECL_TYPE
,sed.TYPE																				as DECL_TYPE_CODE
,(case when nvl(sd.SUMPU173_5, 0) = 0 and nvl(sd.SUMPU173_1, 0) = 0 and nvl(COUNT_INVOICE_R08_09_12, 0) = 0 then 'Нулевая' 
       when nvl(sd.SUMPU173_5, 0) = 0 and nvl(sd.SUMPU173_1, 0) = 0 and nvl(COUNT_INVOICE_R08_09_12, 0) > 0 then 'К уплате'
       when nvl(sd.SUMPU173_5, 0) > 0 and nvl(sd.SUMPU173_1, 0) = 0 then 'К уплате'
       when nvl(sd.SUMPU173_5, 0) = 0 and nvl(sd.SUMPU173_1, 0) > 0 then 'К уплате'
       when nvl(sd.SUMPU173_5, 0) = 0 and nvl(sd.SUMPU173_1, 0) < 0 then 'К возмещению'
       when nvl(sd.SUMPU173_5, 0) > 0 and nvl(sd.SUMPU173_1, 0) > 0 then 'К уплате'
       when nvl(sd.SUMPU173_5, 0) > 0 and nvl(sd.SUMPU173_1, 0) < 0 and (nvl(sd.SUMPU173_5, 0) + nvl(sd.SUMPU173_1, 0)) >= 0 then 'К уплате'
       when nvl(sd.SUMPU173_5, 0) > 0 and nvl(sd.SUMPU173_1, 0) < 0 and (nvl(sd.SUMPU173_5, 0) + nvl(sd.SUMPU173_1, 0)) < 0 then 'К возмещению'
       when nvl(sd.SUMPU173_5, 0) < 0 and nvl(sd.SUMPU173_1, 0) < 0 then 'К возмещению'
       when nvl(sd.SUMPU173_5, 0) < 0 and nvl(sd.SUMPU173_1, 0) > 0 then 'К уплате'
       when nvl(sd.SUMPU173_5, 0) < 0 and nvl(sd.SUMPU173_1, 0) = 0 and nvl(COUNT_INVOICE_R08_09_12, 0) = 0 then 'Нулевая'
       when nvl(sd.SUMPU173_5, 0) < 0 and nvl(sd.SUMPU173_1, 0) = 0 and nvl(COUNT_INVOICE_R08_09_12, 0) > 0 then 'К уплате'
end) as DECL_SIGN
,sed.CORRECTION_NUMBER																	as CORRECTION_NUMBER
,DECODE(sed.TYPE, 0, 
 (case when nvl(sd.SUMPU173_5, 0) = 0 and nvl(sd.SUMPU173_1, 0) = 0 and nvl(COUNT_INVOICE_R08_09_12, 0) = 0 then 0 
       when nvl(sd.SUMPU173_5, 0) = 0 and nvl(sd.SUMPU173_1, 0) = 0 and nvl(COUNT_INVOICE_R08_09_12, 0) > 0 then 0
       when nvl(sd.SUMPU173_5, 0) > 0 and nvl(sd.SUMPU173_1, 0) = 0 then nvl(sd.SUMPU173_5, 0)
       when nvl(sd.SUMPU173_5, 0) = 0 and nvl(sd.SUMPU173_1, 0) > 0 then nvl(sd.SUMPU173_1, 0)
       when nvl(sd.SUMPU173_5, 0) = 0 and nvl(sd.SUMPU173_1, 0) < 0 then nvl(sd.SUMPU173_1, 0)
       when nvl(sd.SUMPU173_5, 0) > 0 and nvl(sd.SUMPU173_1, 0) > 0 then (nvl(sd.SUMPU173_5, 0) + nvl(sd.SUMPU173_1, 0))
       when nvl(sd.SUMPU173_5, 0) > 0 and nvl(sd.SUMPU173_1, 0) < 0 and (nvl(sd.SUMPU173_5, 0) + nvl(sd.SUMPU173_1, 0)) >= 0 then  (nvl(sd.SUMPU173_5, 0) + nvl(sd.SUMPU173_1, 0))
       when nvl(sd.SUMPU173_5, 0) > 0 and nvl(sd.SUMPU173_1, 0) < 0 and (nvl(sd.SUMPU173_5, 0) + nvl(sd.SUMPU173_1, 0)) < 0 then (nvl(sd.SUMPU173_5, 0) + nvl(sd.SUMPU173_1, 0))
       when nvl(sd.SUMPU173_5, 0) < 0 and nvl(sd.SUMPU173_1, 0) < 0 then nvl(sd.SUMPU173_1, 0)
       when nvl(sd.SUMPU173_5, 0) < 0 and nvl(sd.SUMPU173_1, 0) > 0 then nvl(sd.SUMPU173_1, 0)
       when nvl(sd.SUMPU173_5, 0) < 0 and nvl(sd.SUMPU173_1, 0) = 0 and nvl(COUNT_INVOICE_R08_09_12, 0) = 0 then 0
       when nvl(sd.SUMPU173_5, 0) < 0 and nvl(sd.SUMPU173_1, 0) = 0 and nvl(COUNT_INVOICE_R08_09_12, 0) > 0 then 0
else null end), null)																as COMPENSATION_AMNT
,DECODE(sed.TYPE, 0, '0', null)																as COMPENSATION_AMNT_SIGN
,d.LK_ERRORS_COUNT																		as LK_ERRORS_COUNT
,d.CH8_DEALS_AMNT_TOTAL																	as CH8_DEALS_AMNT_TOTAL
,d.CH9_DEALS_AMNT_TOTAL
,d.CH8_NDS
,d.CH9_NDS
,d.DISCREP_CURRENCY_AMNT
,d.DISCREP_CURRENCY_COUNT
,d.GAP_DISCREP_COUNT
,d.GAP_DISCREP_AMNT
,d.WEAK_DISCREP_COUNT
,d.WEAK_DISCREP_AMNT
,d.NDS_INCREASE_DISCREP_COUNT
,d.NDS_INCREASE_DISCREP_AMNT
,d.GAP_MULTIPLE_DISCREP_COUNT
,d.GAP_MULTIPLE_DISCREP_AMNT
,(nvl(d.DISCREP_CURRENCY_COUNT, 0) 
	+ nvl(d.GAP_DISCREP_COUNT, 0) 
	+ nvl(d.WEAK_DISCREP_COUNT, 0) 
	+ nvl(d.NDS_INCREASE_DISCREP_COUNT, 0) 
	+ nvl(d.GAP_MULTIPLE_DISCREP_COUNT, 0))												as TOTAL_DISCREP_COUNT
,DECODE(sed.TYPE, 0, d.DISCREP_BUY_BOOK_AMNT, null)										as DISCREP_BUY_BOOK_AMNT
,DECODE(sed.TYPE, 0, d.DISCREP_SELL_BOOK_AMNT, null)									as DISCREP_SELL_BOOK_AMNT
,d.DISCREP_MIN_AMNT
,d.DISCREP_MAX_AMNT
,d.DISCREP_AVG_AMNT
,d.DISCREP_TOTAL_AMNT
,DECODE(sed.TYPE, 0, d.PVP_TOTAL_AMNT, null)											as PVP_TOTAL_AMNT
,DECODE(sed.TYPE, 0, d.PVP_DISCREP_MIN_AMNT, null)										as PVP_DISCREP_MIN_AMNT
,DECODE(sed.TYPE, 0, d.PVP_DISCREP_MAX_AMNT, null)										as PVP_DISCREP_MAX_AMNT
,DECODE(sed.TYPE, 0, d.PVP_DISCREP_AVG_AMNT, null)										as PVP_DISCREP_AVG_AMNT
,DECODE(sed.TYPE, 0, d.PVP_BUY_BOOK_AMNT, null)											as PVP_BUY_BOOK_AMNT
,DECODE(sed.TYPE, 0, d.PVP_SELL_BOOK_AMNT, null)										as PVP_SELL_BOOK_AMNT
,d.PVP_RECIEVE_JOURNAL_AMNT
,d.PVP_SENT_JOURNAL_AMNT
,nvl(sur.code, 4)																		as SUR_CODE
,sur.description																		as SUR_DESCRIPTION
,d.CONTROL_RATIO_DATE
,d.CONTROL_RATIO_SEND_TO_SEOD
,DECODE(sed.TYPE, 0, nvl(KSAggr.KSCount, 0), null)										as CONTROL_RATIO_COUNT
,knp.completion_date																	as DateCloseKNP
,DECODE(sed.TYPE, 0, 
  case
      when (knp.knp_id IS NOT NULL and knp.completion_date is not null) then 'Закрыта'
      else 'Открыта'
  end, null)	
  																		as STATUS
,dsd.SumNDSPok_8
,dsd.SumNDSPok_81
,dsd.SumNDSPokDL_81
-- Begin Chapter 9 and Chapter 9.1
,dsd.StProd18_9
,dsd.StProd18_91
,dsd.StProd18DL_91
,dsd.StProd10_9
,dsd.StProd10_91
,dsd.StProd10DL_91
,dsd.StProd0_9
,dsd.StProd0_91
,dsd.StProd0DL_91
,dsd.SumNDSProd18_9
,dsd.SumNDSProd18_91
,dsd.SumNDSProd18DL_91
,dsd.SumNDSProd10_9
,dsd.SumNDSProd10_91
,dsd.SumNDSProd10DL_91
,dsd.StProdOsv_9
,dsd.StProdOsv_91
,dsd.StProdOsvDL_91
-- End Chapter 9 and Chapter 9.1
,dsd.StProd18
,dsd.StProd10
,dsd.StProd0
,dsd.SumNDSProd18
,dsd.SumNDSProd10
,dsd.StProd
,dsd.StProdOsv
,dsd.StProd18DL
,dsd.StProd10DL
,dsd.StProd0DL
,dsd.SumNDSProd18DL
,dsd.SumNDSProd10DL
,dsd.StProdOsvDL
-- Тут псевдоданные, потом будем их считать нормально
,'-' AS RECORD_MARK                                            --Признак записи [Т, П, Т-П, Н, МНК, NULL]
,NDS_INCREASE_DISCREP_COUNT*3 AS BUYBOOK_CONTRAGENT_CNT        --Книга покупок: Кол-во контрагентов
,NDS_INCREASE_DISCREP_COUNT*2 AS BUYBOOK_DK_GAP_CNT            --Книга покупок: Вид расхождения - РАЗРЫВ: Кол-во разрывов
,NDS_INCREASE_DISCREP_COUNT   AS BUYBOOK_DK_GAP_CAGNT_CNT    --Книга покупок: Вид расхождения - РАЗРЫВ: Кол-во контрагентов
,NDS_INCREASE_DISCREP_COUNT*4 AS BUYBOOK_DK_GAP_AMNT        --Книга покупок: Вид расхождения - РАЗРЫВ: Сумма разрывов, руб.
,NDS_INCREASE_DISCREP_COUNT   AS BUYBOOK_DK_OTHR_CNT        --Книга покупок: Вид расхождения - ДРУГИЕ: Кол-во расхождений [Количество расхождений вида «Неточное сопоставление» и «Проверка НДС» с контрагентами-продавцами]
,NDS_INCREASE_DISCREP_COUNT*2 AS BUYBOOK_DK_OTHR_CAGNT_CNT    --Книга покупок: Вид расхождения - ДРУГИЕ: Кол-во контрагентов
,NDS_INCREASE_DISCREP_COUNT*5 AS BUYBOOK_DK_OTHR_AMNT        --Книга покупок: Вид расхождения - ДРУГИЕ: Сумма расхождений, руб.
--
from 
seod_declaration sed 
inner join v$sono soun on soun.s_code = sed.sono_code
inner join v$ssrf ssrf on ssrf.s_code = substr(sed.sono_code,1,2)
inner join dict_tax_period dtp on dtp.code = sed.tax_period
left join V$ASK_DECLANDJRNL sd on SD.PERIOD = SED.TAX_PERIOD and SD.OTCHETGOD = SED.FISCAL_YEAR and SD.INNNP = SED.INN and SD.NOMKORR = SED.CORRECTION_NUMBER  
left join SOV_DECLARATION_INFO d on D.DECLARATION_VERSION_ID = SD.ZIP
left join (select ksotn.IdDekl as decl_id, count(1) as KSCount from v$askkontrsoontosh ksotn where ksotn.Vypoln = 0 group by ksotn.IdDekl )  KSAggr on KSAggr.decl_id = sd.ID
left join seod_knp knp on knp.declaration_reg_num = sed.decl_reg_num
left join v$taxpayer vtp on vtp.inn = sed.INN
left join EXT_SUR esur on esur.inn = sed.INN and sed.TAX_PERIOD = esur.fiscal_period and sed.FISCAL_YEAR = esur.fiscal_year
left join DICT_SUR sur on sur.code = esur.sign_code
left join
(
  select
    ZIP as Declaration_Version_Id,
    sum(case when TipFajla = 0 then nvl(SUMNDSPOK, 0) end) as SumNDSPok_8, 
    sum(case when TipFajla = 2 then nvl(SUMNDSPOK, 0) end) as SumNDSPok_81,
    sum(case when TipFajla = 2 then nvl(SUMNDSPOKDL, 0) end) as SumNDSPokDL_81,
    -- Begin Chapter 9 and Chapter 9.1
    sum(case when TipFajla = 1 then nvl(STPROD18, 0) end) as StProd18_9,
    sum(case when TipFajla = 3 then nvl(STPROD18, 0) end) as StProd18_91,
    sum(case when TipFajla = 3 then nvl(STPROD18DL, 0) end) as StProd18DL_91,
    sum(case when TipFajla = 1 then nvl(STPROD10, 0) end) as StProd10_9,
    sum(case when TipFajla = 3 then nvl(STPROD10, 0) end) as StProd10_91,
    sum(case when TipFajla = 3 then nvl(STPROD10DL, 0) end) as StProd10DL_91,
    sum(case when TipFajla = 1 then nvl(STPROD0, 0) end) as StProd0_9,
    sum(case when TipFajla = 3 then nvl(STPROD0, 0) end) as StProd0_91,
    sum(case when TipFajla = 3 then nvl(STPROD10DL, 0) end) as StProd0DL_91,
    sum(case when TipFajla = 1 then nvl(SUMNDSPROD18, 0) end) as SumNDSProd18_9,
    sum(case when TipFajla = 3 then nvl(SUMNDSPROD18, 0) end) as SumNDSProd18_91,
    sum(case when TipFajla = 3 then nvl(SUMNDS18DL, 0) end) as SumNDSProd18DL_91,
    sum(case when TipFajla = 1 then nvl(SUMNDSPROD10, 0) end) as SumNDSProd10_9,
    sum(case when TipFajla = 3 then nvl(SUMNDSPROD10, 0) end) as SumNDSProd10_91,
    sum(case when TipFajla = 3 then nvl(SUMNDS10DL, 0) end) as SumNDSProd10DL_91,
    sum(case when TipFajla = 1 then nvl(STPRODOSV, 0) end) as StProdOsv_9,
    sum(case when TipFajla = 3 then nvl(STPRODOSV, 0) end) as StProdOsv_91,
    sum(case when TipFajla = 3 then nvl(STPRODOSVDL, 0) end) as StProdOsvDL_91,
    -- End Chapter 9 and Chapter 9.1
    sum(STPROD18) as StProd18,
    sum(STPROD10) as StProd10,
    sum(STPROD0) as StProd0,
    sum(SUMNDSPROD18) as SumNDSProd18,
    sum(SUMNDSPROD10) as SumNDSProd10,
    sum(STPROD) as StProd,
    sum(STPRODOSV) as StProdOsv,
    sum(STPROD18DL) as StProd18DL,
    sum(STPROD10DL) as StProd10DL,
    sum(STPROD0DL) as StProd0DL,
    sum(SUMNDS18DL) as SumNDSProd18DL,
    sum(SUMNDS10DL) as SumNDSProd10DL,
    sum(STPRODOSVDL) as StProdOsvDL,
    nvl(SUM(case when TipFajla in ('0', '2', '1', '3', '6') then nvl(KolZapisey, 0) end), 0) as COUNT_INVOICE_R08_09_12
  from v$asksvodzap
  group by ZIP
) dsd on dsd.declaration_version_id = sd.zip;
