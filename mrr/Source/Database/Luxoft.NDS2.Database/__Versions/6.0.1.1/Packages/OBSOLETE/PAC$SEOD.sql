﻿create or replace package PAC$SEOD is

  -- Author  : Д. Зыкин
  -- Created : 11/1/2014 5:39:26 PM
  -- Purpose : Функции обработки данных из СЭОД  

  -- Public function and procedure declarations
  procedure P$PARSE_KNP(
    pRawData xmltype
    );

end PAC$SEOD;
/
create or replace package body PAC$SEOD is

  procedure P$PARSE_KNP (
    pRawData xmltype
    )
  as
    vDeclarationRegNum number(13);
    vKnpId number;    
  begin
    
    select extractValue(value(t),'Документ/@РегНомДек')
    into vDeclarationRegNum
    from table(XMLSequence(pRawData.Extract('Файл/Документ'))) t;
    
    begin
         select KNP_ID
         into vKnpId
         from SEOD_KNP
         where DECLARATION_REG_NUM = vDeclarationRegNum;
         exception when NO_DATA_FOUND then vKnpId := null;
    end;

    if vKnpId is NULL then
      begin
       select SEQ_SEOD_KNP.NEXTVAL into vKnpId from DUAL;
       insert into SEOD_KNP (knp_id, declaration_reg_num, creation_date, ifns_code)
              select
                   vKnpId,
                   vDeclarationRegNum,
                   SysDate,
                   extractValue(value(d), 'Документ/@КодНО')
              from table(XMLSequence(pRawData.Extract('Файл/Документ'))) d;
      end;
    else
      begin
        delete from SEOD_KNP_ACT where knp_id = vKnpId;
        delete from SEOD_KNP_DECISION where knp_id = vKnpId;        
      end;
    end if;
  
    update SEOD_KNP 
    set 
           completion_date = 
                           (select to_date(extractValue(value(x),'СвЗавКНП/@ДатаЗавКНП'), 'DD.MM.YYYY') 
                           from table(XMLSequence(pRawData.Extract('Файл/Документ/КНП/СвЗавКНП'))) x) 
           where knp_id = vKnpId;
     
    insert into SEOD_KNP_ACT (knp_id,doc_data,doc_num,sum_unpaid,sum_overestimated,sum_compensation)
           select
                vKnpId,
                to_date(extractValue(value(act), 'АктКНП/@ДатаДок'),'DD.MM.YYYY'),
                extractValue(value(act), 'АктКНП/@НомДок'),
                to_number(extractValue(value(act), 'АктКНП/@СумНеупл')),
                to_number(extractValue(value(act), 'АктКНП/@СумЗавНДС')),
                to_number(extractValue(value(act), 'АктКНП/@СумЗавВозм'))
           from
                table(XMLSequence(pRawData.Extract('Файл/Документ/КНП/АктКНП'))) act;
     
    insert into SEOD_KNP_DECISION (knp_id,type_id,doc_date,doc_num)
           select
                vKnpId,
                (select x.id from SEOD_KNP_DECISION_TYPE x where x.name = 'РешПриост'),
                to_date(extractValue(value(t),'РешПриост/@ДатаДок'),'DD.MM.YYYY'),
                extractValue(value(t), 'РешПриост/@НомДок')
           from table(XMLSequence(pRawData.Extract('Файл/Документ/КНП/РешПриост'))) t;
           
    insert into SEOD_KNP_DECISION (knp_id,type_id,doc_date,doc_num,amount)
           select
                vKnpId,
                (select x.id from SEOD_KNP_DECISION_TYPE x where x.name = 'РешПривлОтв'),
                to_date(extractValue(value(t),'РешПривлОтв/@ДатаДок'),'DD.MM.YYYY'),
                extractValue(value(t), 'РешПривлОтв/@НомДок'),
                to_number(extractValue(value(t), 'РешПривлОтв/@СумНеупл'))
           from table(XMLSequence(pRawData.Extract('Файл/Документ/КНП/РешПривлОтв'))) t;
           
    insert into SEOD_KNP_DECISION (knp_id,type_id,doc_date,doc_num,amount)
           select
                vKnpId,
                (select x.id from SEOD_KNP_DECISION_TYPE x where x.name = 'РешОтказПривл'),
                to_date(extractValue(value(t),'РешОтказПривл/@ДатаДок'),'DD.MM.YYYY'),
                extractValue(value(t), 'РешОтказПривл/@НомДок'),
                to_number(extractValue(value(t), 'РешОтказПривл/@СумНеупл'))
           from table(XMLSequence(pRawData.Extract('Файл/Документ/КНП/РешОтказПривл'))) t;
           
    insert into SEOD_KNP_DECISION (knp_id,type_id,doc_date,doc_num,amount)
           select
                vKnpId,
                (select x.id from SEOD_KNP_DECISION_TYPE x where x.name = 'РешВозмНДС'),
                to_date(extractValue(value(t),'РешВозмНДС/@ДатаДок'),'DD.MM.YYYY'),
                extractValue(value(t), 'РешВозмНДС/@НомДок'),
                to_number(extractValue(value(t), 'РешВозмНДС/@СумНДСВозм'))
           from table(XMLSequence(pRawData.Extract('Файл/Документ/КНП/РешВозмНДС'))) t;
           
    insert into SEOD_KNP_DECISION (knp_id,type_id,doc_date,doc_num,amount)
           select
                vKnpId,
                (select x.id from SEOD_KNP_DECISION_TYPE x where x.name = 'РешОтказВозмНДС'),
                to_date(extractValue(value(t),'РешОтказВозмНДС/@ДатаДок'),'DD.MM.YYYY'),
                extractValue(value(t), 'РешОтказВозмНДС/@НомДок'),
                to_number(extractValue(value(t), 'РешОтказВозмНДС/@СумОтказВозм'))
           from table(XMLSequence(pRawData.Extract('Файл/Документ/КНП/РешОтказВозмНДС'))) t;
           
    insert into SEOD_KNP_DECISION (knp_id,type_id,doc_date,doc_num,amount)
           select
                vKnpId,
                (select x.id from SEOD_KNP_DECISION_TYPE x where x.name = 'РешВозмНДСЗаявит'),
                to_date(extractValue(value(t),'РешВозмНДСЗаявит/@ДатаДок'),'DD.MM.YYYY'),
                extractValue(value(t), 'РешВозмНДСЗаявит/@НомДок'),
                to_number(extractValue(value(t), 'РешВозмНДСЗаявит/@СумНДСВозм'))
           from table(XMLSequence(pRawData.Extract('Файл/Документ/КНП/РешВозмНДСЗаявит'))) t;
           
    insert into SEOD_KNP_DECISION (knp_id,type_id,doc_date,doc_num,amount)
           select
                vKnpId,
                (select x.id from SEOD_KNP_DECISION_TYPE x where x.name = 'РешОтказВозмНДСЗаявит'),
                to_date(extractValue(value(t),'РешОтказВозмНДСЗаявит/@ДатаДок'),'DD.MM.YYYY'),
                extractValue(value(t), 'РешОтказВозмНДСЗаявит/@НомДок'),
                to_number(extractValue(value(t), 'РешОтказВозмНДСЗаявит/@СумОтказВозм'))
           from table(XMLSequence(pRawData.Extract('Файл/Документ/КНП/РешОтказВозмНДСЗаявит'))) t;
           
    insert into SEOD_KNP_DECISION (knp_id,type_id,doc_date,doc_num,amount)
           select
                vKnpId,
                (select x.id from SEOD_KNP_DECISION_TYPE x where x.name = 'РешОтменВозмНДСЗаявит'),
                to_date(extractValue(value(t),'РешОтменВозмНДСЗаявит/@ДатаДок'),'DD.MM.YYYY'),
                extractValue(value(t), 'РешОтменВозмНДСЗаявит/@НомДок'),
                to_number(extractValue(value(t), 'РешОтменВозмНДСЗаявит/@СумНДСВозмОтмен'))
           from table(XMLSequence(pRawData.Extract('Файл/Документ/КНП/РешОтменВозмНДСЗаявит'))) t;
           
    insert into SEOD_KNP_DECISION (knp_id,type_id,doc_date,doc_num,amount)
           select
                vKnpId,
                (select x.id from SEOD_KNP_DECISION_TYPE x where x.name = 'РешОтменВозмНДСУточн'),
                to_date(extractValue(value(t),'РешОтменВозмНДСУточн/@ДатаДок'),'DD.MM.YYYY'),
                extractValue(value(t), 'РешОтменВозмНДСУточн/@НомДок'),
                to_number(extractValue(value(t), 'РешОтменВозмНДСУточн/@СумНДСВозмОтмен'))
           from table(XMLSequence(pRawData.Extract('Файл/Документ/КНП/РешОтменВозмНДСУточн'))) t;
  end;  

end PAC$SEOD;
/
