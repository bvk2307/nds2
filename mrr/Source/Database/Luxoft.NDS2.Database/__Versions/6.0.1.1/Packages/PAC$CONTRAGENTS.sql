﻿create or replace package NDS2_MRR_USER.PAC$CONTRAGENTS is

procedure P$REQUEST_STATUS (
  pRequestId in CONTRAGENT_DATA_REQUEST.ID%type,
  pData out SYS_REFCURSOR
  ); -- Возвращает статус запроса получения данных контрагента

procedure P$REQUEST_EXISTS (
  pRequestId out CONTRAGENT_DATA_REQUEST.ID%type,
  pInn in CONTRAGENT_DATA_REQUEST.INN%type,
  pCorrectionNumber in CONTRAGENT_DATA_REQUEST.CORRECTION_NUMBER%type,
  pPeriod in CONTRAGENT_DATA_REQUEST.PERIOD%type,
  pYear in CONTRAGENT_DATA_REQUEST.TAX_YEAR%type
 ); -- Возвращает идентификатор запроса данных контрагента
 
procedure P$REQUEST_PARAMS_STATUS (
  pRequestId in CONTRAGENT_DATA_REQUEST.ID%type,
  pData out SYS_REFCURSOR
  ); -- Возвращает статус запроса получения данных контрагента

procedure P$REQUEST_PARAMS_EXISTS (
  pRequestId out CONTRAGENT_DATA_REQUEST.ID%type,
  pInn in CONTRAGENT_DATA_REQUEST.INN%type,
  pCorrectionNumber in CONTRAGENT_DATA_REQUEST.CORRECTION_NUMBER%type,
  pPeriod in CONTRAGENT_DATA_REQUEST.PERIOD%type,
  pYear in CONTRAGENT_DATA_REQUEST.TAX_YEAR%type
 ); -- Возвращает идентификатор запроса данных контрагента

end PAC$CONTRAGENTS;
/
create or replace package body NDS2_MRR_USER.PAC$CONTRAGENTS is

procedure P$REQUEST_STATUS (
  pRequestId in CONTRAGENT_DATA_REQUEST.ID%type,
  pData out SYS_REFCURSOR
  )
as
begin

  open pData for select dict.* from REQUEST_STATUS dict join CONTRAGENT_DATA_REQUEST req on req.REQUEST_STATUS = dict.ID where req.ID = pRequestId;

end;

procedure P$REQUEST_EXISTS (
  pRequestId out CONTRAGENT_DATA_REQUEST.ID%type,
  pInn in CONTRAGENT_DATA_REQUEST.INN%type,
  pCorrectionNumber in CONTRAGENT_DATA_REQUEST.CORRECTION_NUMBER%type,
  pPeriod in CONTRAGENT_DATA_REQUEST.PERIOD%type,
  pYear in CONTRAGENT_DATA_REQUEST.TAX_YEAR%type
 )
as
begin

 select ID
 into pRequestId
 from CONTRAGENT_DATA_REQUEST
 where
  INN = pInn AND CORRECTION_NUMBER = pCorrectionNumber AND PERIOD = pPeriod AND TAX_YEAR = pYear
  AND REQUEST_STATUS <> 9 AND ROWNUM = 1;

 exception
   when NO_DATA_FOUND then
      pRequestId := null;
end;

procedure P$REQUEST_PARAMS_STATUS (
  pRequestId in CONTRAGENT_DATA_REQUEST.ID%type,
  pData out SYS_REFCURSOR
  )
as
begin

  open pData for select dict.* from REQUEST_STATUS dict join CONTRAGENT_PARAMS_REQUEST req on req.REQUEST_STATUS = dict.ID where req.ID = pRequestId;

end;

procedure P$REQUEST_PARAMS_EXISTS (
  pRequestId out CONTRAGENT_DATA_REQUEST.ID%type,
  pInn in CONTRAGENT_DATA_REQUEST.INN%type,
  pCorrectionNumber in CONTRAGENT_DATA_REQUEST.CORRECTION_NUMBER%type,
  pPeriod in CONTRAGENT_DATA_REQUEST.PERIOD%type,
  pYear in CONTRAGENT_DATA_REQUEST.TAX_YEAR%type
 )
as
begin

 select ID
 into pRequestId
 from CONTRAGENT_PARAMS_REQUEST
 where
  INN = pInn AND CORRECTION_NUMBER = pCorrectionNumber AND PERIOD = pPeriod AND TAX_YEAR = pYear
  AND REQUEST_STATUS <> 9 AND ROWNUM = 1;

 exception
   when NO_DATA_FOUND then
      pRequestId := null;
end;

end PAC$CONTRAGENTS;
/
