﻿CREATE OR REPLACE PACKAGE NDS2_MRR_USER.PAC$DOCUMENT
as
  PROCEDURE DEMO;
END;
/
CREATE OR REPLACE PACKAGE BODY NDS2_MRR_USER.PAC$DOCUMENT
as
  SEL_STATUS_PREP_SENT CONSTANT NUMBER(1)       := 4;
  SEL_STATUS_SENT CONSTANT NUMBER(2)            := 10;
  SOV_REQUEST_STATUS_DONE CONSTANT NUMBER(1)    := 2;
  DATE_FORMAT CONSTANT varchar2(12)             := 'DD.MM.YYYY';
  DECIMAL_FORMAT CONSTANT varchar2(18)          := '9999999990.00';

  XSD_NDS2_CAM_01 CONSTANT varchar2(32 char)    := 'TAX3EXCH_NDS2_CAM_01_01.xsd';
  XSD_NDS2_CAM_02 CONSTANT varchar2(32 char)    := 'TAX3EXCH_NDS2_CAM_02_01.xsd';
  XSD_NDS2_CAM_03 CONSTANT varchar2(32 char)    := 'TAX3EXCH_NDS2_CAM_03_01.xsd';
  XSD_NDS2_CAM_04 CONSTANT varchar2(32 char)    := 'TAX3EXCH_NDS2_CAM_04_01.xsd';

procedure UTL_LOG
  (
    v_source varchar2,
    v_code varchar2 := 0,
    v_msg varchar2 := null,
    v_entity_id varchar2 := null
  )
as
pragma autonomous_transaction;
begin
  insert into system_log(id, type, site, entity_id, message_code, message)
  values(seq_sys_log_id.nextval, 0, v_source, v_entity_id, v_code, v_msg);
  commit;
exception when others then
  rollback;
  dbms_output.put_line('UTL_LOG:'||sqlcode||'-'||substr(sqlerrm, 0, 256));
end;

FUNCTION UTL_FORMAT_DECIMAL(v_val in number)
return varchar2
is
begin
  return trim(to_char(v_val, DECIMAL_FORMAT));
end;

FUNCTION UTL_FORMAT_DATE(v_val in DATE)
return varchar2
is
begin
  return to_char(v_val, DATE_FORMAT);
end;

PROCEDURE UTL_REMOVE_IF_EMPTY(p_parent in xmldom.DOMNode, p_node in xmldom.DOMNode)
as
d_node xmldom.DOMNode;
begin
   if dbms_xmldom.getLength(dbms_xmldom.getChildNodes(p_node)) < 1 then
     d_node:= dbms_xmldom.removeChild(p_parent, p_node);
   end if;
end;

PROCEDURE UTL_GET_TAXPERIOD_BOUNDS
(
  p_period in varchar2,
  p_year in varchar2,
  p_start_date out varchar2,
  p_end_date out varchar2
)
as
begin
 p_start_date := '01.01.2001';
 p_end_date := '01.01.2001';
end;

PROCEDURE UTL_XML_APPEND_ATTRIBUTE
  (
    p_elm in xmldom.DOMElement
   ,p_attrName in varchar2
   ,p_attrValue in varchar2
  )
as
begin
if length(nvl(p_attrValue, '')) > 0 then
  dbms_xmldom.setAttribute(p_elm, p_attrName, p_attrValue);
end if;
end;

function UTL_GET_DATESHIFT (days in number) return date
as
   cnt number := days;
   dt date := trunc(sysdate);
   v_exist number;
begin
    while (cnt > 0) loop
      dt := dt + 1;
      cnt := cnt - 1;
      select count(1) into v_exist from redday where red_date = dt;
      while (v_exist > 0) loop
        dt := dt + 1;
        select count(1) into v_exist from redday where red_date = dt;
      end loop;
    end loop;
    return dt;
end;

function UTL_GET_DEADLINE_CLAIM
return date
as
begin
  return UTL_GET_DATESHIFT (UTL_GET_CONFIGURATION_NUMBER('timeout_answer_for_autoclaim'));
  exception when others then return sysdate + 8;
end;  
  
function UTL_GET_DEADLINE_RECLAIM
return date
as
begin
  return UTL_GET_DATESHIFT (UTL_GET_CONFIGURATION_NUMBER('timeout_answer_for_autooverclaim'));
  exception when others then return sysdate + 8;
end;

PROCEDURE LOG(p_msg varchar2)
as
begin
  dbms_output.put_line(p_msg);
end;


FUNCTION GET_ERROR_CODE(p_row_key in varchar2) return varchar2
as
  v_line_numbers varchar2(256);
  v_sql varchar2(1024);
  v_error varchar2(128);
  v_result varchar2(512);
begin
  v_result := '';
  for discrepancy_row in
  (
    select
      dis.id,
      dis.type,
      dis.Amount,
      dis.invoice_chapter,
      dis.invoice_contractor_chapter,
      dis.invoice_rk,
      dis.invoice_contractor_rk,
      case dis.type
        when 1 then -- Разрыв
          case
            when invoice.chapter in (8, 11) then '1 [' || to_char(trunc(dis.Amount), 'TM') || ']'
            when invoice.chapter in (9, 10, 12) then '2 [' || to_char(trunc(dis.Amount), 'TM') || ']'
            else null
          end
        when 4 then -- НДС
          case
            when invoice.chapter in (8, 11) then '3 [' || to_char(trunc(dis.Amount), 'TM') || ']'
            when invoice.chapter in (9, 10, 12) then '4 [' || to_char(trunc(dis.Amount), 'TM') || ']'
            else null
          end
        when 3 then -- Валюта
          case
            when invoice.chapter in (9, 10, 12) then '5'
            else null
          end
        when 2 then '6' -- НС
        else null
      end as error_code
    from stage_invoice invoice
    inner join v$discrepancy_union dis on dis.invoice_rk = invoice.row_key
      or dis.invoice_contractor_rk = invoice.row_key
    where invoice.row_key = p_row_key
  )
  loop
    if v_result <> '' then
      v_result := v_result || ', ';
    end if;
    v_result := v_result || discrepancy_row.error_code;
    if discrepancy_row.error_code = '6' then
      v_line_numbers := '';

      for error_row in
      (
        select
          dis.id as dis_id,
          invoice_1.row_key as i1_row_key,
          invoice_2.row_key as i2_row_key,
          ln_1.name as property_name
        from sov_discrepancy dis
        inner join dict_invoice_line_number ln_1 on ln_1.chapter = dis.invoice_chapter
        inner join dict_invoice_line_number ln_2 on ln_2.chapter = dis.invoice_contractor_chapter
        inner join stage_invoice invoice_1 on invoice_1.row_key = dis.invoice_rk
        inner join stage_invoice invoice_2 on invoice_2.row_key = dis.invoice_contractor_rk
        where ln_1.name = ln_2.name
          and dis.id = discrepancy_row.id
      )
      loop
        v_sql := '
          select count(1)
          from dict_invoice_line_number ln
          inner join stage_invoice invoice_1 on invoice_1.row_key = ''' || error_row.i1_row_key || '''
          inner join stage_invoice invoice_2 on invoice_2.row_key = ''' || error_row.i2_row_key || '''
          where ln.name = ''' || error_row.property_name || '''
            and ln.chapter = case when invoice_1.row_key = ''' || p_row_key || ''' then invoice_1.chapter else invoice_2.chapter end
            and
            (
                 (invoice_1.' || error_row.property_name || ' is null and invoice_2.' || error_row.property_name || ' is not null)
              or (invoice_1.' || error_row.property_name || ' is not null and invoice_2.' || error_row.property_name || ' is null)
              or (invoice_1.' || error_row.property_name || ' <> invoice_2.' || error_row.property_name || ')
            )';

        execute immediate v_sql into v_error;
        if v_error = '0' then
          continue;
        end if;

        v_sql := replace(v_sql, 'count(1)', 'ln.line_number');
        execute immediate v_sql into v_error;

        if v_line_numbers <> '' then
          v_line_numbers := v_line_numbers || ', ';
        end if;
        v_line_numbers := v_line_numbers || v_error;
      end loop;
      if v_line_numbers <> '' then
        v_result := v_result || ' [' || v_line_numbers || ']';
      end if;
    end if;
  end loop;
  return v_result;
end;

/* ############################ */
/* # Формирование строки 1.1 ## */
/* ############################ */
FUNCTION CLAIM_BUILD_T1_1
(
   p_doc in xmldom.DOMDocument,
   p_container_node in xmldom.DOMNode,
   p_invoice_row in stage_invoice%rowtype
)
return boolean
as
  v_itemElement xmldom.DOMElement;
  v_itemNode xmldom.DOMNode;

  v_taxPayerInfoElement xmldom.DOMElement;
  v_taxPayerInfoNode xmldom.DOMNode;

  v_taxPayerElement xmldom.DOMElement;
  v_taxPayerNode xmldom.DOMNode;
begin
  /*нода строки 1.1*/
  v_itemElement := xmldom.createElement(p_doc, 'T1.1Стр');
  v_itemNode := xmldom.appendChild(p_container_node, xmldom.makeNode(v_itemElement));
  /*обязательные атрибуты*/

  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомерПор',        p_invoice_row.ordinal_number);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'КодВидОпер',      p_invoice_row.Operation_Code);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомСчФПрод',      p_invoice_row.Invoice_Num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СтоимПокупВ',     UTL_FORMAT_DECIMAL(p_invoice_row.Price_Buy_Amount));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СумНДСВыч',       UTL_FORMAT_DECIMAL(p_invoice_row.Price_Buy_Nds_Amount));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СпрКодОш',        GET_ERROR_CODE(p_invoice_row.row_key));
  /*Не обязательные атрибуты*/
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаСчФПрод',     UTL_FORMAT_DATE(p_invoice_row.Invoice_Date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомИспрСчФ',      p_invoice_row.Change_Num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаИспрСчФ',     UTL_FORMAT_DATE(p_invoice_row.Change_Date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомКСчФПрод',     p_invoice_row.Correction_Num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаКСчФПрод',    UTL_FORMAT_DATE(p_invoice_row.Correction_Date ));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомИспрКСчФ',     p_invoice_row.Change_Correction_Num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаИспрКСчФ',    UTL_FORMAT_DATE(p_invoice_row.Change_Correction_Date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомДокПдтвУпл',   p_invoice_row.Receipt_Doc_Num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаДокПдтвУпл',  UTL_FORMAT_DATE(p_invoice_row.Receipt_Doc_Date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаУчТов',       UTL_FORMAT_DATE(p_invoice_row.Buy_Accept_Date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомТД',           p_invoice_row.Customs_Declaration_Num);
  --              xmldom.setAttribute(ch8elmItem, 'ОКВ', invLine.);


  /*сведения о продавце*/
  if length(p_invoice_row.seller_inn) > 1 then /*при расхждении типа Разрыв, данной информации может не быть*/
    v_taxPayerInfoElement := xmldom.createElement(p_doc, 'СвПрод');
    v_taxPayerInfoNode := xmldom.appendChild(v_itemNode, xmldom.makeNode(v_taxPayerInfoElement));
    if length(p_invoice_row.seller_kpp) > 1 then
      v_taxPayerElement := xmldom.createElement(p_doc, 'СведЮЛ');
      xmldom.setAttribute(v_taxPayerElement, 'ИННЮЛ',    p_invoice_row.seller_inn);
      xmldom.setAttribute(v_taxPayerElement, 'КПП',      p_invoice_row.seller_kpp);
      v_taxPayerNode := xmldom.appendChild(v_taxPayerInfoNode, xmldom.makeNode(v_taxPayerElement));
    else
      v_taxPayerElement := xmldom.createElement(p_doc, 'СведИП');
      xmldom.setAttribute(v_taxPayerElement, 'ИННЮЛ', p_invoice_row.seller_inn);
      v_taxPayerNode := xmldom.appendChild(v_taxPayerInfoNode, xmldom.makeNode(v_taxPayerElement));
    end if;
  end if;

  /*сведения о посреднике*/
  if length(p_invoice_row.broker_inn) > 1 then
     v_taxPayerInfoElement := xmldom.createElement(p_doc, 'СвПос');
     v_taxPayerInfoNode := xmldom.appendChild(v_itemNode, xmldom.makeNode(v_taxPayerInfoElement));

    if length(p_invoice_row.broker_kpp) > 1 then
      v_taxPayerElement := xmldom.createElement(p_doc, 'СведЮЛ');
      xmldom.setAttribute(v_taxPayerElement, 'ИННЮЛ', p_invoice_row.broker_inn);
      xmldom.setAttribute(v_taxPayerElement, 'КПП',   p_invoice_row.broker_kpp);
      v_taxPayerNode := xmldom.appendChild(v_taxPayerInfoNode, xmldom.makeNode(v_taxPayerElement));
    else
      v_taxPayerElement := xmldom.createElement(p_doc, 'СведИП');
      xmldom.setAttribute(v_taxPayerElement, 'ИННЮЛ', p_invoice_row.broker_inn);
      v_taxPayerNode := xmldom.appendChild(v_taxPayerInfoNode, xmldom.makeNode(v_taxPayerElement));
    end if;
  end if;

  return true;
exception when others then
  /*log error*/
  UTL_LOG('CLAIM_BUILD_T1_1', sqlcode, substr(sqlerrm, 0, 256));
  return false;
end;

/* ############################ */
/* # Формирование строки 1.2 ## */
/* ############################ */
FUNCTION CLAIM_BUILD_T1_2
(
   p_doc in xmldom.DOMDocument,
   p_container_node in xmldom.DOMNode,
   p_invoice_row in stage_invoice%rowtype
)
return boolean
as
  v_itemElement xmldom.DOMElement;
  v_itemNode xmldom.DOMNode;

  v_taxPayerInfoElement xmldom.DOMElement;
  v_taxPayerInfoNode xmldom.DOMNode;

  v_taxPayerElement xmldom.DOMElement;
  v_taxPayerNode xmldom.DOMNode;
begin
  /*нода строки 1.2*/
  v_itemElement := xmldom.createElement(p_doc, 'T1.2Стр');
  v_itemNode := xmldom.appendChild(p_container_node, xmldom.makeNode(v_itemElement));
  /*обязательные*/
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомерПор',       p_invoice_row.ordinal_number);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'КодВидОпер',     p_invoice_row.Operation_Code);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомСчФПрод',     p_invoice_row.Invoice_Num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаСчФПрод',    UTL_FORMAT_DATE(p_invoice_row.Invoice_Date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СпрКодОш',       GET_ERROR_CODE(p_invoice_row.row_key));
  /*Не обязательные*/
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомИспрСчФ',     p_invoice_row.Change_Num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаИспрСчФ',    UTL_FORMAT_DATE(p_invoice_row.Change_Date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомКСчФПрод',    p_invoice_row.Correction_Num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаКСчФПрод',   UTL_FORMAT_DATE(p_invoice_row.Correction_Date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомИспрКСчФ',    p_invoice_row.Change_Correction_Num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаИспрКСчФ',   UTL_FORMAT_DATE(p_invoice_row.Change_Correction_Date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомДокПдтвОпл',  p_invoice_row.Receipt_Doc_Num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаДокПдтвОпл', UTL_FORMAT_DATE(p_invoice_row.Receipt_Doc_Date));
  --UTL_XML_APPEND_ATTRIBUTE(ch9elmItem, 'ОКВ', invLine.);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СтоимПродСФВ',   UTL_FORMAT_DECIMAL(p_invoice_row.Price_Sell_In_Curr));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СтоимПродСФ18',  UTL_FORMAT_DECIMAL(p_invoice_row.Price_Sell_18));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СтоимПродСФ10',  UTL_FORMAT_DECIMAL(p_invoice_row.Price_Sell_10));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СтоимПродСФ0',   UTL_FORMAT_DECIMAL(p_invoice_row.Price_Sell_0));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СумНДССФ18',     UTL_FORMAT_DECIMAL(p_invoice_row.Price_Nds_18));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СумНДССФ10',     UTL_FORMAT_DECIMAL(p_invoice_row.Price_Nds_10));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СтоимПродОсв',   UTL_FORMAT_DECIMAL(p_invoice_row.Price_Tax_Free));

  /*сведения о покупателе*/
  if length(p_invoice_row.buyer_inn) > 1 then
    v_taxPayerInfoElement := xmldom.createElement(p_doc, 'СвПокуп');
    v_taxPayerInfoNode := xmldom.appendChild(v_itemNode, xmldom.makeNode(v_taxPayerInfoElement));
    if length(p_invoice_row.buyer_kpp) > 1 then
      v_taxPayerElement := xmldom.createElement(p_doc, 'СведЮЛ');
      xmldom.setAttribute(v_taxPayerElement, 'ИННЮЛ',    p_invoice_row.buyer_inn);
      xmldom.setAttribute(v_taxPayerElement, 'КПП',      p_invoice_row.buyer_kpp);
      v_taxPayerNode := xmldom.appendChild(v_taxPayerInfoNode, xmldom.makeNode(v_taxPayerElement));
    else
      v_taxPayerElement := xmldom.createElement(p_doc, 'СведИП');
      xmldom.setAttribute(v_taxPayerElement, 'ИННЮЛ', p_invoice_row.buyer_inn);
      v_taxPayerNode := xmldom.appendChild(v_taxPayerInfoNode, xmldom.makeNode(v_taxPayerElement));
    end if;
  end if;

  /*сведения о посреднике*/
  if length(p_invoice_row.broker_inn) > 1 then
     v_taxPayerInfoElement := xmldom.createElement(p_doc, 'СвПос');
     v_taxPayerInfoNode := xmldom.appendChild(v_itemNode, xmldom.makeNode(v_taxPayerInfoElement));

    if length(p_invoice_row.broker_kpp) > 1 then
      v_taxPayerElement := xmldom.createElement(p_doc, 'СведЮЛ');
      xmldom.setAttribute(v_taxPayerElement, 'ИННЮЛ', p_invoice_row.broker_inn);
      xmldom.setAttribute(v_taxPayerElement, 'КПП',   p_invoice_row.broker_kpp);
      v_taxPayerNode := xmldom.appendChild(v_taxPayerInfoNode, xmldom.makeNode(v_taxPayerElement));
    else
      v_taxPayerElement := xmldom.createElement(p_doc, 'СведИП');
      xmldom.setAttribute(v_taxPayerElement, 'ИННЮЛ', p_invoice_row.broker_inn);
      v_taxPayerNode := xmldom.appendChild(v_taxPayerInfoNode, xmldom.makeNode(v_taxPayerElement));
    end if;
  end if;

  return true;
exception when others then
  /*log error*/
  UTL_LOG('CLAIM_BUILD_T1_2', sqlcode, substr(sqlerrm, 0, 256));
  return false;
end;

/* ############################ */
/* # Формирование строки 1.3 ## */
/* ############################ */
FUNCTION CLAIM_BUILD_T1_3
(
   p_doc in xmldom.DOMDocument,
   p_container_node in xmldom.DOMNode,
   p_invoice_row in stage_invoice%rowtype
)
return boolean
as
  v_itemElement xmldom.DOMElement;
  v_itemNode xmldom.DOMNode;

  v_taxPayerInfoElement xmldom.DOMElement;
  v_taxPayerInfoNode xmldom.DOMNode;

  v_taxPayerElement xmldom.DOMElement;
  v_taxPayerNode xmldom.DOMNode;
begin
  /*нода строки 1.3*/
  v_itemElement := xmldom.createElement(p_doc, 'T1.3Стр');
  v_itemNode := xmldom.appendChild(p_container_node, xmldom.makeNode(v_itemElement));
  /*обязательные*/
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомерПор',       p_invoice_row.ordinal_number);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'КодВидОпер',     p_invoice_row.Operation_Code);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомСчФПрод',     p_invoice_row.Invoice_Num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаСчФПрод',    UTL_FORMAT_DATE(p_invoice_row.Invoice_Date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СпрКодОш',       GET_ERROR_CODE(p_invoice_row.row_key));
  /*Не обязательные*/
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомИспрСчФ',     p_invoice_row.Change_Num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаИспрСчФ',    UTL_FORMAT_DATE(p_invoice_row.Change_Date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомКСчФПрод',    p_invoice_row.Correction_Num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаКСчФПрод',   UTL_FORMAT_DATE(p_invoice_row.Correction_Date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомИспрКСчФ',    p_invoice_row.Change_Correction_Num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаИспрКСчФ',   UTL_FORMAT_DATE(p_invoice_row.Change_Correction_Date));
--              xmldom.setAttribute(ch10elmItem, 'ОКВ', invLine.);

  /*сведения о покупателе*/
  if length(p_invoice_row.buyer_inn) > 1 then /*при расхждении типа Разрыв, данной тнформации может не быть*/
    v_taxPayerInfoElement := xmldom.createElement(p_doc, 'СвПокуп');
    v_taxPayerInfoNode := xmldom.appendChild(v_itemNode, xmldom.makeNode(v_taxPayerInfoElement));
    if length(p_invoice_row.buyer_kpp) > 1 then
      v_taxPayerElement := xmldom.createElement(p_doc, 'СведЮЛ');
      xmldom.setAttribute(v_taxPayerElement, 'ИННЮЛ',    p_invoice_row.buyer_inn);
      xmldom.setAttribute(v_taxPayerElement, 'КПП',      p_invoice_row.buyer_kpp);
      v_taxPayerNode := xmldom.appendChild(v_taxPayerInfoNode, xmldom.makeNode(v_taxPayerElement));
    else
      v_taxPayerElement := xmldom.createElement(p_doc, 'СведИП');
      xmldom.setAttribute(v_taxPayerElement, 'ИННЮЛ', p_invoice_row.buyer_inn);
      v_taxPayerNode := xmldom.appendChild(v_taxPayerInfoNode, xmldom.makeNode(v_taxPayerElement));
    end if;
  end if;
  /*!!!!TODO!!!!! СвПосрДеят - как заполняем?*/
  return true;
exception when others then
  /*log error*/
  UTL_LOG('CLAIM_BUILD_T1_3', sqlcode, substr(sqlerrm, 0, 256));
  return false;
end;

/* ############################ */
/* # Формирование строки 1.4 ## */
/* ############################ */
FUNCTION CLAIM_BUILD_T1_4
(
   p_doc in xmldom.DOMDocument,
   p_container_node in xmldom.DOMNode,
   p_invoice_row in stage_invoice%rowtype
)
return boolean
as
  v_itemElement xmldom.DOMElement;
  v_itemNode xmldom.DOMNode;

  v_taxPayerInfoElement xmldom.DOMElement;
  v_taxPayerInfoNode xmldom.DOMNode;

  v_taxPayerElement xmldom.DOMElement;
  v_taxPayerNode xmldom.DOMNode;
begin
  /*нода строки 1.4*/
  v_itemElement := xmldom.createElement(p_doc, 'T1.4Стр');
  v_itemNode := xmldom.appendChild(p_container_node, xmldom.makeNode(v_itemElement));
  /*обязательные*/
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомерПор',       p_invoice_row.ordinal_number);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'КодВидОпер',     p_invoice_row.Operation_Code);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомСчФПрод',     p_invoice_row.Invoice_Num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаСчФПрод',    UTL_FORMAT_DATE(p_invoice_row.Invoice_Date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СтоимТовСчФВс',  UTL_FORMAT_DECIMAL(p_invoice_row.Price_Total));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'КодВидСд',       p_invoice_row.Deal_Kind_Code);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СпрКодОш',       GET_ERROR_CODE(p_invoice_row.row_key));
  /*Не обязательные*/
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомИспрСчФ',     p_invoice_row.Change_Num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаИспрСчФ',    UTL_FORMAT_DATE(p_invoice_row.Change_Date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомКСчФПрод',    p_invoice_row.Correction_Num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаКСчФПрод',   UTL_FORMAT_DATE(p_invoice_row.Correction_Date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомИспрКСчФ',    p_invoice_row.Change_Correction_Num);
--              UTL_XML_APPEND_ATTRIBUTE(ch10elmItem, 'ОКВ', invLine.);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СумНДССчФ',      UTL_FORMAT_DECIMAL(p_invoice_row.Price_Nds_Total));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'РазСтКСчФУм',    UTL_FORMAT_DECIMAL(p_invoice_row.Diff_Correct_Decrease));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'РазСтКСчФУв',    UTL_FORMAT_DECIMAL(p_invoice_row.Diff_Correct_Increase));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'РазНДСКСчФУм',   UTL_FORMAT_DECIMAL(p_invoice_row.Diff_Correct_Nds_Decrease));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'РазНДСКСчФУв',   UTL_FORMAT_DECIMAL(p_invoice_row.Diff_Correct_Nds_Increase));


  /*сведения о продавце*/
  if length(p_invoice_row.seller_inn) > 1 then /*при расхждении типа Разрыв, данной тнформации может не быть*/
    v_taxPayerInfoElement := xmldom.createElement(p_doc, 'СвПрод');
    v_taxPayerInfoNode := xmldom.appendChild(v_itemNode, xmldom.makeNode(v_taxPayerInfoElement));
    if length(p_invoice_row.seller_kpp) > 1 then
      v_taxPayerElement := xmldom.createElement(p_doc, 'СведЮЛ');
      xmldom.setAttribute(v_taxPayerElement, 'ИННЮЛ',    p_invoice_row.seller_inn);
      xmldom.setAttribute(v_taxPayerElement, 'КПП',      p_invoice_row.seller_kpp);
      v_taxPayerNode := xmldom.appendChild(v_taxPayerInfoNode, xmldom.makeNode(v_taxPayerElement));
    else
      v_taxPayerElement := xmldom.createElement(p_doc, 'СведИП');
      xmldom.setAttribute(v_taxPayerElement, 'ИННЮЛ', p_invoice_row.seller_inn);
      v_taxPayerNode := xmldom.appendChild(v_taxPayerInfoNode, xmldom.makeNode(v_taxPayerElement));
    end if;
  end if;

  /*сведения о СвСубком*/
  /*if length(p_invoice_row.broker_inn) > 1 then
     v_taxPayerInfoElement := xmldom.createElement(p_doc, 'СвСубком');
     v_taxPayerInfoNode := xmldom.appendChild(v_itemNode, xmldom.makeNode(v_taxPayerInfoElement));

    if length(p_invoice_row.broker_kpp) > 1 then
      v_taxPayerElement := xmldom.createElement(p_doc, 'СведЮЛ');
      xmldom.setAttribute(v_taxPayerElement, 'ИННЮЛ', p_invoice_row.broker_inn);
      xmldom.setAttribute(v_taxPayerElement, 'КПП',   p_invoice_row.broker_kpp);
      v_taxPayerNode := xmldom.appendChild(v_taxPayerInfoNode, xmldom.makeNode(v_taxPayerElement));
    else
      v_taxPayerElement := xmldom.createElement(p_doc, 'СведИП');
      xmldom.setAttribute(v_taxPayerElement, 'ИННЮЛ', p_invoice_row.broker_inn);
      v_taxPayerNode := xmldom.appendChild(v_taxPayerInfoNode, xmldom.makeNode(v_taxPayerElement));
    end if;
  end if;*/

  return true;
exception when others then
  /*log error*/
  UTL_LOG('CLAIM_BUILD_T1_4', sqlcode, substr(sqlerrm, 0, 256));
  return false;
end;

/* ############################ */
/* # Формирование строки 1.5 ## */
/* ############################ */
FUNCTION CLAIM_BUILD_T1_5
(
   p_doc in xmldom.DOMDocument,
   p_container_node in xmldom.DOMNode,
   p_invoice_row in stage_invoice%rowtype
)
return boolean
as
  v_itemElement xmldom.DOMElement;
  v_itemNode xmldom.DOMNode;

  v_taxPayerInfoElement xmldom.DOMElement;
  v_taxPayerInfoNode xmldom.DOMNode;

  v_taxPayerElement xmldom.DOMElement;
  v_taxPayerNode xmldom.DOMNode;
begin
  /*нода строки 1.5*/
  v_itemElement := xmldom.createElement(p_doc, 'T1.5Стр');
  v_itemNode := xmldom.appendChild(p_container_node, xmldom.makeNode(v_itemElement));
  /*обязательные*/
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомСчФ',         p_invoice_row.Invoice_Num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаСчФ',        UTL_FORMAT_DATE(p_invoice_row.Invoice_Date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СтоимТовБНалВс', UTL_FORMAT_DECIMAL(p_invoice_row.Price_Total));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СумНалПокуп',    UTL_FORMAT_DECIMAL(p_invoice_row.Price_Nds_Buyer));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СтоимТовСНалВс', UTL_FORMAT_DECIMAL(p_invoice_row.Price_Nds_Total));

  /*сведения о покупателе*/
  if length(p_invoice_row.buyer_inn) > 1 then
    v_taxPayerInfoElement := xmldom.createElement(p_doc, 'СвПокуп');
    v_taxPayerInfoNode := xmldom.appendChild(v_itemNode, xmldom.makeNode(v_taxPayerInfoElement));
    if length(p_invoice_row.buyer_kpp) > 1 then
      v_taxPayerElement := xmldom.createElement(p_doc, 'СведЮЛ');
      xmldom.setAttribute(v_taxPayerElement, 'ИННЮЛ',    p_invoice_row.buyer_inn);
      xmldom.setAttribute(v_taxPayerElement, 'КПП',      p_invoice_row.buyer_kpp);
      v_taxPayerNode := xmldom.appendChild(v_taxPayerInfoNode, xmldom.makeNode(v_taxPayerElement));
    else
      v_taxPayerElement := xmldom.createElement(p_doc, 'СведИП');
      xmldom.setAttribute(v_taxPayerElement, 'ИННЮЛ', p_invoice_row.buyer_inn);
      v_taxPayerNode := xmldom.appendChild(v_taxPayerInfoNode, xmldom.makeNode(v_taxPayerElement));
    end if;
  end if;

  return true;
exception when others then
  /*log error*/
  UTL_LOG('CLAIM_BUILD_T1_5', sqlcode, substr(sqlerrm, 0, 256));
  return false;
end;

/* ############################ */
/* # Формирование строки 1.7 ## */
/* ############################ */
FUNCTION CLAIM_BUILD_T1_7
(
   p_doc in xmldom.DOMDocument,
   p_container_node in xmldom.DOMNode,
   p_violation_info in varchar2,
   p_cr_code in varchar2,
   p_left_side in number,
   p_rigth_side in number
)
return boolean
as
  v_itemElement xmldom.DOMElement;
  v_itemNode xmldom.DOMNode;


begin
  /*нода строки 1.7*/
  v_itemElement := xmldom.createElement(p_doc, 'T1.7Стр');
  v_itemNode := xmldom.appendChild(p_container_node, xmldom.makeNode(v_itemElement));
  /*обязательные*/
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СвНар',       p_violation_info);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'КодКС',       p_cr_code);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СумЛевЧ',     UTL_FORMAT_DECIMAL(p_left_side));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СумПравЧ',    UTL_FORMAT_DECIMAL(p_rigth_side));

  return true;
exception when others then
  /*log error*/
  UTL_LOG('CLAIM_BUILD_T1_7', sqlcode, substr(sqlerrm, 0, 256));
  return false;
end;

FUNCTION RECLAIM_BUILD
(
   p_doc in xmldom.DOMDocument,
   p_container_node in xmldom.DOMNode,
   p_invoice_row in stage_invoice%rowtype,
   p_tax_period in varchar2,
   p_tax_year in varchar2,
   p_element_name in varchar2
)
return boolean
is
  v_itemElement xmldom.DOMElement;
  v_itemNode xmldom.DOMNode;

  v_invoiceElement xmldom.DOMElement;
  v_invoiceNode xmldom.DOMNode;

  v_dateElement xmldom.DOMElement;
  v_dateNode xmldom.DOMNode;

  v_startPeriod varchar2(10 CHAR);
  v_endPeriod varchar2(10 CHAR);

  v_textElement  xmldom.DOMText;
  v_textNode xmldom.DOMNode;
begin
  /*нода строки*/
  v_itemElement := xmldom.createElement(p_doc, p_element_name);
  v_itemNode := xmldom.appendChild(p_container_node, xmldom.makeNode(v_itemElement));
  /*обязательные атрибуты*/
  UTL_GET_TAXPERIOD_BOUNDS(p_tax_period, p_tax_year, v_startPeriod, v_endPeriod);
  if p_invoice_row.correction_num is null then
     v_invoiceElement := xmldom.createElement(p_doc, 'КСФ');
     v_invoiceNode := xmldom.appendChild(v_itemNode, xmldom.makeNode(v_invoiceElement));

     UTL_XML_APPEND_ATTRIBUTE(v_invoiceElement, 'КодДок', '2772');
     UTL_XML_APPEND_ATTRIBUTE(v_invoiceElement, 'НомДок', p_invoice_row.correction_num);

     if p_invoice_row.correction_date is null then
       v_dateElement := xmldom.createElement(p_doc, 'ПериодДок');
       v_dateNode := xmldom.appendChild(v_invoiceNode, xmldom.makeNode(v_dateElement));

       UTL_XML_APPEND_ATTRIBUTE(v_dateElement, 'НачПер', v_startPeriod);
       UTL_XML_APPEND_ATTRIBUTE(v_dateElement, 'ОконПер', v_endPeriod);
     else
       v_dateElement := xmldom.createElement(p_doc, 'ДатаДок');
       v_dateNode := xmldom.appendChild(v_invoiceNode, xmldom.makeNode(v_dateElement));

       v_textElement := xmldom.createTextNode(p_doc, UTL_FORMAT_DATE(p_invoice_row.correction_date));
       v_textNode := xmldom.appendChild(v_dateNode, xmldom.makeNode(v_textElement));
     end if;
  else
     v_invoiceElement := xmldom.createElement(p_doc, 'СФ');
     v_invoiceNode := xmldom.appendChild(v_itemNode, xmldom.makeNode(v_invoiceElement));

     UTL_XML_APPEND_ATTRIBUTE(v_invoiceElement, 'КодДок', '0924');
     UTL_XML_APPEND_ATTRIBUTE(v_invoiceElement, 'НомДок', p_invoice_row.invoice_num);

     if p_invoice_row.correction_date is null then
       v_dateElement := xmldom.createElement(p_doc, 'ПериодДок');
       v_dateNode := xmldom.appendChild(v_invoiceNode, xmldom.makeNode(v_dateElement));

       UTL_XML_APPEND_ATTRIBUTE(v_dateElement, 'НачПер', v_startPeriod);
       UTL_XML_APPEND_ATTRIBUTE(v_dateElement, 'ОконПер', v_endPeriod);
     else
       v_dateElement := xmldom.createElement(p_doc, 'ДатаДок');
       v_dateNode := xmldom.appendChild(v_invoiceNode, xmldom.makeNode(v_dateElement));

       v_textElement := xmldom.createTextNode(p_doc, UTL_FORMAT_DATE(p_invoice_row.correction_date));
       v_textNode := xmldom.appendChild(v_dateNode, xmldom.makeNode(v_textElement));
     end if;
  end if;

  return true;

  exception when others then
    return false;
end;

PROCEDURE CREATE_DECLARATION_CLAIM
(
  p_decl_ver_id in number,
  p_sono_code in varchar2,
  p_seod_decl_regnum in number,
  p_claim_mode in number := 1 /*1 - СФ, 2 - КС*/
)
as
  doc xmldom.DOMDocument;
  docXml xmltype;
  docXmlValidation xmltype;

  main_node xmldom.DOMNode;
  root_node xmldom.DOMNode;
  user_node xmldom.DOMNode;
  item_node xmldom.DOMNode;
  root_elmt xmldom.DOMElement;
  item_elmt xmldom.DOMElement;
  item_text xmldom.DOMText;
  discrepElement xmldom.DOMElement;
  discrepNode xmldom.DOMNode;

  v_T11_ListElement xmldom.DOMElement;
  v_T11_ListNode xmldom.DOMNode;
  v_T12_ListElement xmldom.DOMElement;
  v_T12_ListNode xmldom.DOMNode;
  v_T13_ListElement xmldom.DOMElement;
  v_T13_ListNode xmldom.DOMNode;
  v_T14_ListElement xmldom.DOMElement;
  v_T14_ListNode xmldom.DOMNode;
  v_T15_ListElement xmldom.DOMElement;
  v_T15_ListNode xmldom.DOMNode;
  v_T16_ListElement xmldom.DOMElement;
  v_T16_ListNode xmldom.DOMNode;
  v_T17_ListElement xmldom.DOMElement;
  v_T17_ListNode xmldom.DOMNode;

  v_itemNode xmldom.DOMNode;

  v_count_of_errors number(3) := 0;
  v_count_of_processed_data number(19) := 0;
  v_document_id number(19);
  v_nds_decl_id  number(38);
  v_status number(2) := 1;
  v_doc_type number(1)  := 0;
  v_success_operation boolean := false;
  v_deadline_date date;
begin
  v_document_id := SEQ_DOC.NEXTVAL;
  v_deadline_date := UTL_GET_DEADLINE_CLAIM;
  select max(id) into v_nds_decl_id from v$declaration where declaration_version_id = p_decl_ver_id;


  doc := xmldom.newDOMDocument();
  main_node := xmldom.makeNode(doc);
  root_elmt := xmldom.createElement(doc, 'Автотребование');
  xmldom.setAttribute(root_elmt, 'УчНомТреб', v_document_id);
  xmldom.setAttribute(root_elmt, 'ТипИнф', 'TAX3EXCH_NDS2_CAM_01');
  xmldom.setAttribute(root_elmt, 'РегНомДек', p_seod_decl_regnum);
  xmldom.setAttribute(root_elmt, 'КодНО', p_sono_code);
  xmldom.setAttribute(root_elmt, 'xsi:noNamespaceSchemaLocation', 'TAX3EXCH_NDS2_CAM_01_01.xsd');
  xmldom.setAttribute(root_elmt, 'xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
  root_node := xmldom.appendChild(main_node, xmldom.makeNode(root_elmt));

  discrepElement := xmldom.createElement(doc, 'ПризнРсхжд');
  discrepNode := xmldom.appendChild(root_node, xmldom.makeNode(discrepElement));

  v_T11_ListElement := xmldom.createElement(doc, 'T1.1');
  v_T11_ListNode := xmldom.appendChild(discrepNode, xmldom.makeNode(v_T11_ListElement));

  v_T12_ListElement := xmldom.createElement(doc, 'T1.2');
  v_T12_ListNode := xmldom.appendChild(discrepNode, xmldom.makeNode(v_T12_ListElement));

  v_T13_ListElement := xmldom.createElement(doc, 'T1.3');
  v_T13_ListNode := xmldom.appendChild(discrepNode, xmldom.makeNode(v_T13_ListElement));

  v_T14_ListElement := xmldom.createElement(doc, 'T1.4');
  v_T14_ListNode := xmldom.appendChild(discrepNode, xmldom.makeNode(v_T14_ListElement));

  v_T15_ListElement := xmldom.createElement(doc, 'T1.5');
  v_T15_ListNode := xmldom.appendChild(discrepNode, xmldom.makeNode(v_T15_ListElement));

  v_T16_ListElement := xmldom.createElement(doc, 'T1.6');
  v_T16_ListNode := xmldom.appendChild(discrepNode, xmldom.makeNode(v_T16_ListElement));

  v_T17_ListElement := xmldom.createElement(doc, 'T1.7');
  v_T17_ListNode := xmldom.appendChild(discrepNode, xmldom.makeNode(v_T17_ListElement));
/*обработка счетов фактур*/
if p_claim_mode = 1 then
    v_doc_type := 1;
    for discrepancy_invoice_line in
    (
      select si.*
      from SEOD_DATA_QUEUE sdq
      inner join stage_invoice si on si.row_key = sdq.target_invoice_row_key
      inner join v$declaration d on d.ID = sdq.target_declaration_id and d.DECLARATION_VERSION_ID = p_decl_ver_id
      where sdq.for_stage in (2, 3) and sdq.ref_doc_id is null
    )
    loop
      v_count_of_processed_data := v_count_of_processed_data + 1;

      if discrepancy_invoice_line.chapter = 8 then
         v_success_operation := CLAIM_BUILD_T1_1(doc, v_T11_ListNode, discrepancy_invoice_line);
      end if;

      if discrepancy_invoice_line.chapter = 9 then
         v_success_operation := CLAIM_BUILD_T1_2(doc, v_T12_ListNode, discrepancy_invoice_line);
      end if;

      if discrepancy_invoice_line.chapter = 10 then
         v_success_operation := CLAIM_BUILD_T1_3(doc, v_T13_ListNode, discrepancy_invoice_line);
      end if;

      if discrepancy_invoice_line.chapter = 11 then
         v_success_operation := CLAIM_BUILD_T1_4(doc, v_T14_ListNode, discrepancy_invoice_line);
      end if;

      if discrepancy_invoice_line.chapter = 12 then
         v_success_operation := CLAIM_BUILD_T1_5(doc, v_T15_ListNode, discrepancy_invoice_line);
      end if;

      if v_success_operation then
        insert into doc_invoice(doc_id, invoice_row_key, invoice_chapter)
        values (v_document_id, discrepancy_invoice_line.row_key, discrepancy_invoice_line.chapter);

        update SEOD_DATA_QUEUE sdq set sdq.ref_doc_id = v_document_id
        where
          sdq.target_invoice_row_key = discrepancy_invoice_line.row_key
          and sdq.for_stage in (2, 3)
          and sdq.target_declaration_id = v_nds_decl_id;
      else
        dbms_output.put_line('errors');
        exit;
      end if;
    end loop;
end if;

/*обработка контрольный соотношений*/
if p_claim_mode = 2 then
   v_doc_type := 5;
   for ksLine in
     (
       select
       vw.*,
       decode(vw.Vypoln, 1, 10, 0, 12, vw.Vypoln) as vypolnCode
       from V$ASKKontrSoontosh vw
       inner join v$askdekl d on d.ID = vw.IdDekl
     where d.ZIP = p_decl_ver_id /*and vw.Vypoln = 0*/
     )
       loop
      v_count_of_processed_data := v_count_of_processed_data + 1;
      v_success_operation := CLAIM_BUILD_T1_7(doc, v_T17_ListNode, ksLine.vypolnCode, ksLine.KodKs, ksLine.LevyaChast, ksLine.PravyaChast);

      insert into CONTROL_RATIO_DOC values(ksLine.Id, v_document_id, sysdate);

      if not v_success_operation then
        dbms_output.put_line('errors');
        exit;
      end if;
   end loop;
end if;

  UTL_LOG('CREATE_DECLARATION_CLAIM - processed '||v_count_of_processed_data||' items');

  if v_count_of_processed_data > 0 then
    LOG('PROCESSED - '||v_count_of_processed_data || ' invoices for declaration ' || p_seod_decl_regnum);
    UTL_REMOVE_IF_EMPTY(discrepNode, v_T11_ListNode);
    UTL_REMOVE_IF_EMPTY(discrepNode, v_T12_ListNode);
    UTL_REMOVE_IF_EMPTY(discrepNode, v_T13_ListNode);
    UTL_REMOVE_IF_EMPTY(discrepNode, v_T14_ListNode);
    UTL_REMOVE_IF_EMPTY(discrepNode, v_T15_ListNode);
    UTL_REMOVE_IF_EMPTY(discrepNode, v_T16_ListNode);
    UTL_REMOVE_IF_EMPTY(discrepNode, v_T17_ListNode);

    docXml := dbms_xmldom.getXmlType(doc);
    docXmlValidation := xmltype(docXml.getClobVal());

    if docXmlValidation.isSchemaValid(XSD_NDS2_CAM_01) = 0 then
       v_status := 3;
       UTL_LOG('CREATE_DECLARATION_CLAIM:', -1, 'XSD validation error', v_document_id);
    end if;

    insert into doc(doc_id, ref_entity_id, doc_type, doc_kind, sono_code, create_date, status, document_body, deadline_date)
    values (v_document_id, p_seod_decl_regnum, v_doc_type, 1, p_sono_code, sysdate, v_status, docXml.getClobVal(), v_deadline_date);

    xmldom.freeDocument(doc);
  end if;
end;

PROCEDURE CREATE_DECLARATION_RECLAIM
(
  p_decl_version_id in number,
  p_sono_code in varchar2,
  p_seod_decl_regnum in number,
  p_knp_closed in number
)
as
  doc xmldom.DOMDocument;
  docXml xmltype;
  docXmlValidation xmltype;
  main_node xmldom.DOMNode;
  root_node xmldom.DOMNode;
  user_node xmldom.DOMNode;
  item_node xmldom.DOMNode;
  root_elmt xmldom.DOMElement;
  source_elmt xmldom.DOMElement;
  source_node xmldom.DOMNode;
  item_elmt xmldom.DOMElement;
  item_text xmldom.DOMText;
  taxpr_node xmldom.DOMNode;
  taxpr_elmt xmldom.DOMElement;
  fio_node xmldom.DOMNode;
  fio_elmt xmldom.DOMElement;

  v_decl_period varchar2(2 char);
  v_decl_year varchar2(4 char);
  v_document_id number;

  v_taxpr_id number := 0;
  v_taxpr_inn varchar2(12 char);
  v_taxpr_kpp varchar2(9 char);
  v_taxpr_code_no varchar2(4 char);

  v_ul_name varchar2(1000 char);
  v_ip_first_name varchar2(60 char);
  v_ip_patronymic varchar2(60 char);
  v_ip_last_name varchar2(60 char);

  v_reclaim_elem_name varchar2(32 char);
  v_schema_name varchar2(128 char);
  v_doc_type number(2);
  v_status number(2) := 1;
  v_deadline_date date;
begin
  v_deadline_date := UTL_GET_DEADLINE_RECLAIM;
  v_document_id := SEQ_DOC.NEXTVAL;
  doc := xmldom.newDOMDocument();
  main_node := xmldom.makeNode(doc);
  if p_knp_closed = 0 then
    root_elmt := xmldom.createElement(doc, 'Истреб93');
    xmldom.setAttribute(root_elmt, 'КодНО', p_sono_code);
    xmldom.setAttribute(root_elmt, 'РегНомДек', p_seod_decl_regnum);
    xmldom.setAttribute(root_elmt, 'УчНомИстреб', v_document_id);
    xmldom.setAttribute(root_elmt, 'ТипИнф', 'TAX3EXCH_NDS2_CAM_02');
    xmldom.setAttribute(root_elmt, 'xsi:noNamespaceSchemaLocation', 'TAX3EXCH_NDS2_CAM_02_01.xsd');
    xmldom.setAttribute(root_elmt, 'xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
    root_node := xmldom.appendChild(main_node, xmldom.makeNode(root_elmt));

    v_reclaim_elem_name := 'Истреб93Док';
    v_schema_name := XSD_NDS2_CAM_02;
    v_doc_type := 2;
  else
    select ul.id, ul.inn, ul.kpp, ul.name_full, ul.code_no
    into v_taxpr_id, v_taxpr_inn, v_taxpr_kpp, v_ul_name, v_taxpr_code_no
    from v$declaration hist_decl
    inner join v$egrn_ul ul on ul.inn = hist_decl.inn and ul.kpp = hist_decl.kpp
    where hist_decl.declaration_version_id = p_decl_version_id;

    if v_taxpr_id = 0 then
      select ip.id, ip.inn, ip.first_name, ip.patronymic, ip.last_name, ip.code_no
      into v_taxpr_id, v_taxpr_inn, v_ip_first_name, v_ip_patronymic, v_ip_last_name, v_taxpr_code_no
      from v$declaration hist_decl
      inner join v$egrn_ip ip on ip.inn = hist_decl.inn and hist_decl.kpp is null
      where hist_decl.declaration_version_id = p_decl_version_id;
    end if;

    root_elmt := xmldom.createElement(doc, 'Истреб93.1');
    xmldom.setAttribute(root_elmt, 'КодНО', p_sono_code);
    xmldom.setAttribute(root_elmt, 'РегНомДек', p_seod_decl_regnum);
    xmldom.setAttribute(root_elmt, 'УчНомИстреб', v_document_id);
    xmldom.setAttribute(root_elmt, 'КодНОИсполн', v_taxpr_code_no);
    xmldom.setAttribute(root_elmt, 'ТипИнф', 'TAX3EXCH_NDS2_CAM_03');
    xmldom.setAttribute(root_elmt, 'xsi:noNamespaceSchemaLocation', 'TAX3EXCH_NDS2_CAM_03_01.xsd');
    xmldom.setAttribute(root_elmt, 'xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
    root_node := xmldom.appendChild(main_node, xmldom.makeNode(root_elmt));

    source_elmt := xmldom.createElement(doc, 'СведИст');
    source_node := xmldom.appendChild(root_node, xmldom.makeNode(source_elmt));
    if v_taxpr_kpp is not null then
      taxpr_elmt := xmldom.createElement(doc, 'ОргИст');
      xmldom.setAttribute(taxpr_elmt, 'ИННЮЛ', v_taxpr_inn);
      xmldom.setAttribute(taxpr_elmt, 'КПП', v_taxpr_kpp);
      xmldom.setAttribute(taxpr_elmt, 'НаимОрг', v_ul_name);
      taxpr_node := xmldom.appendChild(source_node, xmldom.makeNode(taxpr_elmt));
    else
      taxpr_elmt := xmldom.createElement(doc, 'ФЛИст');
      xmldom.setAttribute(taxpr_elmt, 'ИННФЛ', v_ul_name);
      taxpr_node := xmldom.appendChild(source_node, xmldom.makeNode(taxpr_elmt));

      fio_elmt := xmldom.createElement(doc, 'ФИО');
      xmldom.setAttribute(fio_elmt, 'Фамилия', v_taxpr_inn);
      xmldom.setAttribute(fio_elmt, 'Имя', v_taxpr_kpp);
      xmldom.setAttribute(fio_elmt, 'Отчество', v_ul_name);
      fio_node := xmldom.appendChild(taxpr_node, xmldom.makeNode(fio_elmt));
    end if;
    v_reclaim_elem_name := 'Истреб93.1Док';
    v_schema_name := XSD_NDS2_CAM_03;
    v_doc_type := 3;
  end if;

  --stub
  /*
  select t.PERIOD, t.OTCHETGOD
  into v_decl_period, v_decl_year
  from v$askdekl t
  where t.zip = p_decl_version_id;
  */

  for invLine in
  (
    select si.*
    from stage_invoice si
    inner join seod_data_queue sdq on sdq.target_invoice_row_key = si.row_key
    inner join v$declaration hist_decl on hist_decl.declaration_version_id = p_decl_version_id
      and si.declaration_version_id = hist_decl.declaration_version_id
    where sdq.for_stage in (4, 5)
      and sdq.ref_doc_id is null
      and hist_decl.soun_code = p_sono_code
  )
  loop
    if RECLAIM_BUILD(doc, root_node, invLine, v_decl_period, v_decl_year, v_reclaim_elem_name) then
      insert into doc_invoice(doc_id, invoice_row_key, invoice_chapter) values(v_document_id, invLine.Row_Key, invLine.Chapter);

      update seod_data_queue sdq
      set sdq.ref_doc_id = v_document_id
      where sdq.ref_doc_id is null
        and sdq.for_stage in (4, 5)
        and sdq.target_invoice_row_key = invLine.Row_Key
        and sdq.target_declaration_id in
        (
          select hist_decl.id
          from v$declaration hist_decl
          where hist_decl.declaration_version_id = invLine.Declaration_Version_Id
        );
    else
      exit;
    end if;
  end loop;

  docXml := dbms_xmldom.getXmlType(doc);
  docXmlValidation := xmltype(docXml.getClobVal());
  if docXmlValidation.isSchemaValid(v_schema_name) = 0 then
     v_status := 3;
     UTL_LOG('CREATE_DECLARATION_RECLAIM:', -1, 'XSD validation error', v_document_id);
  end if;

  insert into doc(doc_id, ref_entity_id, doc_type, doc_kind, sono_code, create_date, status, document_body, deadline_date)
  values (v_document_id, p_seod_decl_regnum, v_doc_type, v_doc_type, p_sono_code, sysdate, v_status, docXml.getClobVal(), v_deadline_date);

  xmldom.freeDocument(doc);
end;

/*######################################################*/
/*Сбор данных для отправки астотребования первой стороне*/
/*######################################################*/

PROCEDURE COLLECT_QUEUE
as
pragma autonomous_transaction;
v_count number := 0;
begin
  --Выгрузка СФ
  insert into stage_invoice (row_key, declaration_version_id, chapter, ordinal_number, okv_code, create_date, receive_date, operation_code, invoice_num, invoice_date, change_num, change_date, correction_num, correction_date, change_correction_num, change_correction_date, receipt_doc_num, receipt_doc_date, buy_accept_date, buyer_inn, buyer_kpp, seller_inn, seller_kpp, seller_invoice_num, seller_invoice_date, broker_inn, broker_kpp, deal_kind_code, customs_declaration_num, price_buy_amount, price_buy_nds_amount, price_sell, price_sell_in_curr, price_sell_18, price_sell_10, price_sell_0, price_nds_18, price_nds_10, price_tax_free, price_total, price_nds_total, diff_correct_decrease, diff_correct_increase, diff_correct_nds_decrease, diff_correct_nds_increase, price_nds_buyer, actual_row_key, compare_row_key, compare_algo_id, format_errors, logical_errors, seller_agency_info_inn, seller_agency_info_kpp, seller_agency_info_name, seller_agency_info_num, seller_agency_info_date)
  select distinct i.row_key, i.declaration_version_id, i.chapter, i.ordinal_number, i.okv_code, i.create_date, i.receive_date, i.operation_code, i.invoice_num, i.invoice_date, i.change_num, i.change_date, i.correction_num, i.correction_date, i.change_correction_num, i.change_correction_date, i.receipt_doc_num, i.receipt_doc_date, i.buy_accept_date, i.buyer_inn, i.buyer_kpp, i.seller_inn, i.seller_kpp, i.seller_invoice_num, i.seller_invoice_date, i.broker_inn, i.broker_kpp, i.deal_kind_code, i.customs_declaration_num, i.price_buy_amount, i.price_buy_nds_amount, i.price_sell, i.price_sell_in_curr, i.price_sell_18, i.price_sell_10, i.price_sell_0, i.price_nds_18, i.price_nds_10, i.price_tax_free, i.price_total, i.price_nds_total, i.diff_correct_decrease, i.diff_correct_increase, i.diff_correct_nds_decrease, i.diff_correct_nds_increase, i.price_nds_buyer, i.actual_row_key, i.compare_row_key, i.compare_algo_id, i.format_errors, i.logical_errors, i.seller_agency_info_inn, i.seller_agency_info_kpp, i.seller_agency_info_name, i.seller_agency_info_num, i.seller_agency_info_date
  from selection sel
  inner join invoice_request ir on ir.id = sel.invoice_request_id
  inner join selection_discrepancy dis_ref on dis_ref.request_id = sel.request_id
  inner join sov_discrepancy dis on dis.id = dis_ref.discrepancy_id
  inner join sov_invoice i on i.request_id = sel.invoice_request_id
  inner join v$declaration hist_decl on hist_decl.declaration_version_id = i.declaration_version_id
  left join  stage_invoice si on si.row_key = i.row_key
  where dis.status = 1
    and ir.status = 2
	and dis_ref.is_in_process = 1
    and ((hist_decl.id = dis.decl_id and i.row_key = dis.invoice_rk) or (hist_decl.id = dis.decl_contractor_id and i.row_key = dis.invoice_contractor_rk))
    and si.row_key is null;
  v_count := SQL%ROWCOUNT;
  UTL_LOG('Copy invoice done for ' || v_count || ' records', 0, null);

  --Переход с этапа 1 на этап 2 (автотребования первой стороне)
  v_count := 0;
  for queue_item in
  (
    select distinct
      case when knp.creation_date is null then dis.decl_id else dis.decl_contractor_id end as decl_id,
      case when knp.creation_date is null then dis.invoice_rk else dis.invoice_contractor_rk end as invoice_rk
    from selection sel
    inner join invoice_request ir on ir.id = sel.invoice_request_id
    inner join selection_discrepancy sd on sd.request_id = sel.request_id
    inner join sov_discrepancy dis on dis.id = sd.discrepancy_id
    inner join v$declaration decl on decl.id = dis.decl_id
    inner join seod_declaration seod_decl on seod_decl.nds2_id = decl.id and seod_decl.correction_number = decl.correction_number
    left join  seod_knp knp on knp.declaration_reg_num = seod_decl.decl_reg_num
    left join  v$declaration hist_decl_2 on hist_decl_2.id = dis.decl_contractor_id
    left join  seod_data_queue queue on queue.for_stage = 2 and queue.target_invoice_row_key = case when knp.creation_date is null then dis.invoice_rk else dis.invoice_contractor_rk end
    where dis.status = 1
      and dis.stage = 1
      and ir.status = 2
	  and sd.is_in_process = 1
      and queue.target_invoice_row_key is null
      and (knp.creation_date is null or hist_decl_2.id is not null)
  )
  loop
    insert into seod_data_queue(target_declaration_id, target_invoice_row_key, for_stage, ref_doc_id)
    values (queue_item.decl_id, queue_item.invoice_rk, 2, null);

    update sov_discrepancy
    set stage = 2
    where id in
    (
      select dis.id
      from sov_discrepancy dis
      inner join v$declaration decl on decl.id = dis.decl_id
      inner join seod_declaration seod_decl on seod_decl.nds2_id = decl.id and seod_decl.correction_number = decl.correction_number
      left join  seod_knp knp on knp.declaration_reg_num = seod_decl.decl_reg_num
      where dis.stage = 1
        and queue_item.invoice_rk = case when knp.creation_date is null then dis.invoice_rk else dis.invoice_contractor_rk end
    );

    insert into hist_discrepancy_stage(id, submit_date, stage_id, status_id)
    select dis.id, sysdate, 2, 1
    from sov_discrepancy dis
    inner join v$declaration decl on decl.id = dis.decl_id
    inner join seod_declaration seod_decl on seod_decl.nds2_id = decl.id and seod_decl.correction_number = decl.correction_number
    left join  seod_knp knp on knp.declaration_reg_num = seod_decl.decl_reg_num
    where dis.stage = 2
      and queue_item.invoice_rk = case when knp.creation_date is null then dis.invoice_rk else dis.invoice_contractor_rk end;

    v_count := v_count + 1;
  end loop;
  UTL_LOG('Queue for stages 1 -> 2 done for ' || v_count || ' records', 0, null);

  --Переход с этапа 2 на этап 3 (автотребования второй стороне)
  v_count := 0;
  for queue_item in
  (
    select distinct
      dis.decl_contractor_id as decl_id,
      dis.invoice_contractor_rk as invoice_rk
    from sov_discrepancy dis
    inner join stage_invoice s_inv_2 on s_inv_2.row_key = dis.invoice_contractor_rk
    inner join v$declaration hist_decl_1 on hist_decl_1.id = dis.decl_id
    inner join seod_declaration seod_decl_1 on seod_decl_1.nds2_id = hist_decl_1.id and seod_decl_1.correction_number = hist_decl_1.correction_number
    inner join doc doc_1 on doc_1.ref_entity_id = seod_decl_1.decl_reg_num and doc_1.doc_type = 1
    left join  seod_knp knp_1 on knp_1.declaration_reg_num = seod_decl_1.decl_reg_num
    inner join v$declaration hist_decl_2 on hist_decl_2.id = dis.decl_contractor_id
    inner join seod_declaration seod_decl_2 on seod_decl_2.nds2_id = hist_decl_2.id and seod_decl_2.correction_number = hist_decl_2.correction_number
    left join  seod_knp knp_2 on knp_2.declaration_reg_num = seod_decl_2.decl_reg_num
    left join  seod_data_queue queue on queue.target_invoice_row_key = dis.invoice_contractor_rk and queue.for_stage = 3
    where dis.stage = 2
      and dis.type <> 3
      and dis.invoice_contractor_rk is not null
      and s_inv_2.operation_code not in (14, 15)
      and doc_1.status <> 10
      and (doc_1.deadline_date < sysdate or 1 = 0) --Получен ответ на все расхождения документа (stub)
      and knp_1.completion_date is null
      and knp_2.completion_date is null
      and queue.target_invoice_row_key is null
  )
  loop
    insert into seod_data_queue(target_declaration_id, target_invoice_row_key, for_stage, ref_doc_id)
    values (queue_item.decl_id, queue_item.invoice_rk, 3, null);

    update sov_discrepancy
    set stage = 3
    where id in
    (
      select dis.id
      from sov_discrepancy dis
      inner join v$declaration decl on decl.id = dis.decl_id
      inner join seod_declaration seod_decl on seod_decl.nds2_id = decl.id and seod_decl.correction_number = decl.correction_number
      left join  seod_knp knp on knp.declaration_reg_num = seod_decl.decl_reg_num
      where dis.stage = 2
        and queue_item.invoice_rk = dis.invoice_contractor_rk
    );

    insert into hist_discrepancy_stage(id, submit_date, stage_id, status_id)
    select dis.id, sysdate, 3, 1
    from sov_discrepancy dis
    inner join v$declaration decl on decl.id = dis.decl_id
    inner join seod_declaration seod_decl on seod_decl.nds2_id = decl.id and seod_decl.correction_number = decl.correction_number
    left join  seod_knp knp on knp.declaration_reg_num = seod_decl.decl_reg_num
    where dis.stage = 3
      and queue_item.invoice_rk = dis.invoice_contractor_rk;

    v_count := v_count + 1;
  end loop;
  UTL_LOG('Queue for stages 2 -> 3 done for ' || v_count || ' records', 0, null);

  --Переход с этапа 2 на этап 4 (автоистребования первой стороне)
  v_count := 0;
  for queue_item in
  (
    select dis.decl_id, dis.invoice_rk
    from sov_discrepancy dis
    inner join stage_invoice s_inv_1 on s_inv_1.row_key = dis.invoice_rk
    inner join v$declaration hist_decl_1 on hist_decl_1.id = dis.decl_id
    inner join seod_declaration seod_decl_1 on seod_decl_1.nds2_id = hist_decl_1.id and seod_decl_1.correction_number = hist_decl_1.correction_number
    inner join doc doc_1 on doc_1.ref_entity_id = seod_decl_1.decl_reg_num
    left join  v$declaration hist_decl_2 on hist_decl_2.id = dis.decl_contractor_id
    left join  seod_declaration seod_decl_2 on seod_decl_2.nds2_id = hist_decl_2.id and seod_decl_2.correction_number = hist_decl_2.correction_number
    left join  doc doc_2 on doc_2.ref_entity_id = seod_decl_2.decl_reg_num
    left join  seod_knp knp_2 on knp_2.declaration_reg_num = seod_decl_2.decl_reg_num
    left join  seod_data_queue queue on queue.target_invoice_row_key = dis.invoice_rk and queue.for_stage = 4
    where dis.stage = 2
      and dis.type <> 3
      and s_inv_1.operation_code not in (14, 15)
      and doc_1.doc_type = 1
      and doc_1.deadline_date < sysdate
      and doc_1.status <> 10
      and (seod_decl_2.id is null or knp_2.completion_date is not null)
      and queue.target_invoice_row_key is null
  )
  loop
    insert into seod_data_queue(target_declaration_id, target_invoice_row_key, for_stage, ref_doc_id)
    values (queue_item.decl_id, queue_item.invoice_rk, 4, null);

    update sov_discrepancy
    set stage = 4
    where id in
    (
      select dis.id
      from sov_discrepancy dis
      inner join v$declaration decl on decl.id = dis.decl_id
      inner join seod_declaration seod_decl on seod_decl.nds2_id = decl.id and seod_decl.correction_number = decl.correction_number
      left join  seod_knp knp on knp.declaration_reg_num = seod_decl.decl_reg_num
      where dis.stage = 2
        and queue_item.invoice_rk = dis.invoice_rk
    );

    insert into hist_discrepancy_stage(id, submit_date, stage_id, status_id)
    select dis.id, sysdate, 4, 1
    from sov_discrepancy dis
    inner join v$declaration decl on decl.id = dis.decl_id
    inner join seod_declaration seod_decl on seod_decl.nds2_id = decl.id and seod_decl.correction_number = decl.correction_number
    left join  seod_knp knp on knp.declaration_reg_num = seod_decl.decl_reg_num
    where dis.stage = 4
      and queue_item.invoice_rk = dis.invoice_rk;

    v_count := v_count + 1;
  end loop;
  UTL_LOG('Queue for stages 2 -> 4 done for ' || v_count || ' records', 0, null);

  --Переход с этапа 3 на этап 4 (автоистребования первой стороне)
  v_count := 0;
  for queue_item in
  (
    select distinct dis.decl_id, dis.invoice_rk
    from sov_discrepancy dis
    inner join v$declaration hist_decl_2 on hist_decl_2.id = dis.decl_contractor_id
    inner join seod_declaration seod_decl_2 on seod_decl_2.nds2_id = hist_decl_2.id and seod_decl_2.correction_number = hist_decl_2.correction_number
    inner join doc doc_2 on doc_2.ref_entity_id = seod_decl_2.decl_reg_num
    left join  seod_knp knp_2 on knp_2.declaration_reg_num = seod_decl_2.decl_reg_num
    left join  seod_data_queue queue on queue.target_invoice_row_key = dis.invoice_rk and queue.for_stage = 4
    where dis.stage = 3
      and doc_2.doc_type = 1
      and doc_2.deadline_date < sysdate
      and doc_2.status <> 10
      and knp_2.completion_date is null
      and queue.target_invoice_row_key is null
  )
  loop
    insert into seod_data_queue(target_declaration_id, target_invoice_row_key, for_stage, ref_doc_id)
    values (queue_item.decl_id, queue_item.invoice_rk, 4, null);

    update sov_discrepancy
    set stage = 4
    where id in
    (
      select dis.id
      from sov_discrepancy dis
      inner join v$declaration decl on decl.id = dis.decl_id
      inner join seod_declaration seod_decl on seod_decl.nds2_id = decl.id and seod_decl.correction_number = decl.correction_number
      left join  seod_knp knp on knp.declaration_reg_num = seod_decl.decl_reg_num
      where dis.stage = 3
        and queue_item.invoice_rk = dis.invoice_rk
    );

    insert into hist_discrepancy_stage(id, submit_date, stage_id, status_id)
    select dis.id, sysdate, 4, 1
    from sov_discrepancy dis
    inner join v$declaration decl on decl.id = dis.decl_id
    inner join seod_declaration seod_decl on seod_decl.nds2_id = decl.id and seod_decl.correction_number = decl.correction_number
    left join  seod_knp knp on knp.declaration_reg_num = seod_decl.decl_reg_num
    where dis.stage = 4 and queue_item.invoice_rk = dis.invoice_rk;

    v_count := v_count + 1;
  end loop;
  UTL_LOG('Queue for stages 3 -> 4 done for ' || v_count || ' records', 0, null);

  --Переход с этапа 4 на этап 5 (автоистребования второй стороне)
  v_count := 0;
  for queue_item in
  (
    select distinct
      dis.decl_contractor_id as decl_id,
      dis.invoice_contractor_rk as invoice_rk
    from sov_discrepancy dis
    inner join stage_invoice s_inv_2 on s_inv_2.row_key = dis.invoice_contractor_rk
    inner join v$declaration hist_decl_1 on hist_decl_1.id = dis.decl_id
    inner join seod_declaration seod_decl_1 on seod_decl_1.nds2_id = hist_decl_1.id and seod_decl_1.correction_number = hist_decl_1.correction_number
    inner join doc doc_1 on doc_1.ref_entity_id = seod_decl_1.decl_reg_num and doc_1.doc_type = 2
    left join  seod_knp knp_1 on knp_1.declaration_reg_num = seod_decl_1.decl_reg_num
    left join  v$declaration hist_decl_2 on hist_decl_2.id = dis.decl_contractor_id
    left join  seod_declaration seod_decl_2 on seod_decl_2.nds2_id = hist_decl_2.id and seod_decl_2.correction_number = hist_decl_2.correction_number
    left join  seod_data_queue queue on queue.target_invoice_row_key = dis.invoice_rk and queue.for_stage = 5
    where dis.stage = 4
      and dis.type <> 3
      and s_inv_2.operation_code not in (14, 15)
      and doc_1.doc_type = 2
      and doc_1.status <> 10
      and doc_1.deadline_date < sysdate
      and knp_1.completion_date is null
      and seod_decl_2.id is not null
      and queue.target_invoice_row_key is null
  )
  loop
    insert into seod_data_queue(target_declaration_id, target_invoice_row_key, for_stage, ref_doc_id)
    values (queue_item.decl_id, queue_item.invoice_rk, 5, null);

    update sov_discrepancy
    set stage = 5
    where id in
    (
      select dis.id
      from sov_discrepancy dis
      inner join v$declaration decl on decl.id = dis.decl_id
      inner join seod_declaration seod_decl on seod_decl.nds2_id = decl.id and seod_decl.correction_number = decl.correction_number
      left join  seod_knp knp on knp.declaration_reg_num = seod_decl.decl_reg_num
      where dis.stage = 4
        and queue_item.invoice_rk = dis.invoice_contractor_rk
    );

    insert into hist_discrepancy_stage(id, submit_date, stage_id, status_id)
    select dis.id, sysdate, 5, 1
    from sov_discrepancy dis
    inner join v$declaration decl on decl.id = dis.decl_id
    inner join seod_declaration seod_decl on seod_decl.nds2_id = decl.id and seod_decl.correction_number = decl.correction_number
    left join  seod_knp knp on knp.declaration_reg_num = seod_decl.decl_reg_num
    where dis.stage = 5 and queue_item.invoice_rk = dis.invoice_contractor_rk;

    v_count := v_count + 1;
  end loop;
  UTL_LOG('Queue for stages 4 -> 5 done for ' || v_count || ' records', 0, null);
  commit;

  UTL_LOG('COLLECT_QUEUE completed' || v_count || ' records', 0, null);
  exception when others then
    rollback;
    UTL_LOG('COLLECT_QUEUE failtd', sqlcode, substr(sqlerrm,256));
  null;
end;

PROCEDURE PROCESS_QUEUE
as
pragma autonomous_transaction;
begin
  UTL_LOG('PROCESS_QUEUE - discrepancies');
  for decl_line in
  (
    select distinct
      decl.DECLARATION_VERSION_ID,
      decl.SOUN_CODE,
      sdecl.decl_reg_num
    from SEOD_DATA_QUEUE sdq
    inner join v$declaration decl on decl.ID = sdq.target_declaration_id
    inner join seod_declaration sdecl on sdecl.nds2_id = decl.id and sdecl.correction_number = decl.correction_number
    where sdq.for_stage in (2, 3) and sdq.ref_doc_id is null
  )
  loop
    CREATE_DECLARATION_CLAIM
    (
      decl_line.DECLARATION_VERSION_ID,
      decl_line.soun_code,
      decl_line.decl_reg_num,
      1
    );
  end loop;

  UTL_LOG('PROCESS_QUEUE - control ratio');
  for ready_cr_decl in
  (
    select distinct decl_version_id, kodno, decl_reg_num
    from v$Control_Ratio_doc
    where decl_reg_num is not null and doc_id is null
  )
  loop
    CREATE_DECLARATION_CLAIM
    (
      ready_cr_decl.decl_version_id,
      ready_cr_decl.kodno,
      ready_cr_decl.decl_reg_num,
      2
    );
  end loop;

  UTL_LOG('PROCESS_QUEUE - reclaim');
  for declLine in
  (
     select distinct
       decl.declaration_version_id,
       decl.soun_code,
       decl.seod_decl_id,
       case when knp.completion_date is null then 0 else 1 end as knp_closed
     from seod_data_queue sdq
     inner join v$declaration decl on decl.id = sdq.target_declaration_id
     inner join seod_declaration seod_decl on seod_decl.nds2_id = decl.id
       and seod_decl.correction_number = decl.correction_number
     left join  seod_knp knp on knp.declaration_reg_num = seod_decl.decl_reg_num
     where sdq.for_stage in (4, 5)
       and sdq.ref_doc_id is null
  )
  loop
    CREATE_DECLARATION_RECLAIM(declLine.Declaration_Version_Id, declLine.Soun_Code, declLine.Seod_Decl_Id, declLine.knp_closed);
  end loop;

  insert into DOC_STATUS_HISTORY (doc_id, status, status_date)
  select doc.doc_id, 1, sysdate
  from doc
  left join DOC_STATUS_HISTORY dsh on dsh.doc_id = doc.doc_id and dsh.status = 1
  where dsh.doc_id is null;

  commit;
  UTL_LOG('PROCESS_QUEUE - complete');
  exception when others then
    rollback;
    UTL_LOG('PROCESS_QUEUE failed', sqlcode, substr(sqlerrm, 256));
  null;
end;

PROCEDURE PROCESS_ASK_4
as
  doc xmldom.DOMDocument;
  docXml xmltype;
  main_node xmldom.DOMNode;
  root_node xmldom.DOMNode;
  user_node xmldom.DOMNode;
  item_node xmldom.DOMNode;
  root_elmt xmldom.DOMElement;
  item_elmt xmldom.DOMElement;
  item_text xmldom.DOMText;
  docXmlValidation xmltype;
  v_status number(1):= 1;
  pragma autonomous_transaction;
begin
  for line in (select
  dh.soun_code,
  dh.SEOD_DECL_ID
    from v$declaration dh
    left join doc d on d.ref_entity_id = dh.SEOD_DECL_ID and d.doc_type = 4
    where dh.TOTAL_DISCREP_COUNT = 0
    and dh.CONTROL_RATIO_COUNT = 0
    and to_number(nvl(dh.CORRECTION_NUMBER, '0')) > 0
    and d.doc_id is null)
  loop
    doc := xmldom.newDOMDocument();
    main_node := xmldom.makeNode(doc);
    root_elmt := xmldom.createElement(doc, 'ДанныеРсхжд');
    xmldom.setAttribute(root_elmt, 'КодНО', line.soun_code);
    xmldom.setAttribute(root_elmt, 'РегНомДек', line.SEOD_DECL_ID);
    xmldom.setAttribute(root_elmt, 'ТипИнф', 'TAX3EXCH_NDS2_CAM_04');
    xmldom.setAttribute(root_elmt, 'СвНалРсхжд', 2);
    xmldom.setAttribute(root_elmt, 'xsi:noNamespaceSchemaLocation', 'TAX3EXCH_NDS2_CAM_04_01.xsd');
    xmldom.setAttribute(root_elmt, 'xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
    root_node := xmldom.appendChild(main_node, xmldom.makeNode(root_elmt));

    docXml := dbms_xmldom.getXmlType(doc);
    docXmlValidation := xmltype(docXml.getClobVal());

    if docXmlValidation.isSchemaValid(schurl => 'TAX3EXCH_NDS2_CAM_04_01.xsd') = 0 then
      v_status := 3;
    end if;

    insert into doc(doc_id, ref_entity_id, doc_type, doc_kind, sono_code, create_date, status, document_body)
    values (SEQ_DOC.NEXTVAL, line.SEOD_DECL_ID, 4, 4, line.soun_code, sysdate, v_status, docXml.getClobVal());

  end loop;
  commit;
  exception when others then
  rollback;
  dbms_output.put_line('PROCESS_ASK_4:' || sqlcode || '-' || substr(sqlerrm, 0, 256));
end;

PROCEDURE DEMO
as
begin
  COLLECT_QUEUE;
  PROCESS_QUEUE;
  PROCESS_ASK_4;
end;
END;
/
