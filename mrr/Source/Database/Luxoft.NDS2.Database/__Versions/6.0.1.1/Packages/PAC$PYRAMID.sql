﻿create or replace package NDS2_MRR_USER.PAC$PYRAMID is

  -- Author  : Зыкин Д.С. (Luxoft, Russia, Omsk)
  -- Created : 11/22/2014 2:05:35 PM
  -- Purpose : Чтение данных отчета Пирамида  

  
  function F$GET_REQUEST_STATUS(
    pRequestId IN SOV_PYRAMID_REQUEST.REQUEST_ID%type -- Идентификатор запроса связей НП в СОВ
  ) return int; -- Возвращает статус обработки запроса на стороне СОВ
  
  function F$COUNT_SUMMARY(
    pRequestId IN SOV_PYRAMID_REQUEST.REQUEST_ID%type, -- Идентификатор запроса связей НП в СОВ
	pMinSum IN NUMBER,
	pMinNdsPercent IN NUMBER
  ) return int; -- Возвращает общее количество контрагентов, которые СОВ вернул по запросу
  
  procedure P$LOAD_SUMMARY(
    pRequestId IN SOV_PYRAMID_REQUEST.REQUEST_ID%type, -- Идентификатор запроса связей НП в СОВ
    pIndexFrom IN NUMBER, -- Начальный порядковый номер контрагента
    pIndexTo IN NUMBER, -- Максимальный порядковый номер контрагента
	pMinSum IN NUMBER,
	pMinNdsPercent IN NUMBER, 
    pResultSet OUT SYS_REFCURSOR -- Курсор с данными
  ); -- Возвращает данные о контрагентах, связанных с НП
  
  procedure P$LOAD_CONTRACTOR(
    pDeclarationId IN V$DECLARATION_HISTORY.ID%type, -- Идентификатор версии декларации
    pResultSet OUT SYS_REFCURSOR -- Курсор с данными
  ); -- Загружает данные контрагента по декларации
    
  
end PAC$PYRAMID;
/
create or replace package body NDS2_MRR_USER.PAC$PYRAMID is

  function F$GET_REQUEST_STATUS(
    pRequestId IN SOV_PYRAMID_REQUEST.REQUEST_ID%type
  ) return int
  as
    retValue int;
  begin
    select rq.status_id into retValue from SOV_PYRAMID_REQUEST rq where rq.request_id=pRequestId;
    return retValue;
  end;

  function F$COUNT_SUMMARY(
    pRequestId IN SOV_PYRAMID_REQUEST.REQUEST_ID%type, -- Идентификатор запроса связей НП в СОВ
  	pMinSum IN NUMBER,
	pMinNdsPercent IN NUMBER
) return int
  as
    retValue int;
  begin
    select count(*) into retValue from 
    (select data.SUM_RUB,
            DECODE(data.SUM_NDS,0,0, data.SUM_NDS / (SUM(data.SUM_NDS) OVER(PARTITION BY data.REQUEST_ID))) * 100 as WEIGHT
     from SOV_PYRAMID_INFO data where request_id=pRequestId)
    where (((pMinSum >= 0) and (SUM_RUB >= pMinSum)) or (pMinSum < 0))
          and 
          (((pMinNdsPercent >= 0) and (WEIGHT >= pMinNdsPercent)) or (pMinNdsPercent < 0));
    return retValue;
  end;

  procedure P$LOAD_SUMMARY(
    pRequestId IN SOV_PYRAMID_REQUEST.REQUEST_ID%type,
    pIndexFrom IN NUMBER,
    pIndexTo IN NUMBER,
	pMinSum IN NUMBER,
	pMinNdsPercent IN NUMBER,
    pResultSet OUT SYS_REFCURSOR
  )
  as
  begin
    open pResultSet for
    with indexed_data as (
         select rownum as idx, t.id from (
                select id from SOV_PYRAMID_INFO where request_id=pRequestId order by contractor_name
    ) t)
    select * from
    (select
      data.*, selfReq.request_id as selfRequestId,
      DECODE(data.SUM_NDS,0,0, data.SUM_NDS / (SUM(data.SUM_NDS) OVER(PARTITION BY data.REQUEST_ID))) * 100 as WEIGHT
    from
           SOV_PYRAMID_INFO data
      join indexed_data
           on     indexed_data.id=data.id
              and indexed_data.idx >= pIndexFrom
              and indexed_data.idx <= pIndexTo
      join SOV_PYRAMID_REQUEST req on req.REQUEST_ID = data.REQUEST_ID
      left join SOV_PYRAMID_REQUEST selfReq
           on     selfReq.contractor_inn = data.contractor_inn
              and selfReq.fiscal_year = req.fiscal_year
              and selfReq.period = req.period) t
    where (((pMinSum >= 0) and (SUM_RUB >= pMinSum)) or (pMinSum < 0))
          and 
          (((pMinNdsPercent >= 0) and (WEIGHT >= pMinNdsPercent)) or (pMinNdsPercent < 0));
  end;

  procedure P$LOAD_CONTRACTOR(
    pDeclarationId IN V$DECLARATION_HISTORY.ID%type,
    pResultSet OUT SYS_REFCURSOR
  )
  as
  begin
    open pResultSet for
    select
         d.name as CONTRACTOR_NAME,
         d.inn as CONTRACTOR_INN,
         d.kpp as CONTRACTOR_KPP,
         d.region as REGION,
         d.soun as TAX_AUTHORITY,
         fd.description as FEDERAL_DISTRICT
    from
         V$DECLARATION_HISTORY d
         left join FEDERAL_DISTRICT_REGION fToR on fToR.region_code=d.region_code
         left join FEDERAL_DISTRICT fd on fd.district_id=fToR.district_id
    where d.id=pDeclarationId;
  end;

end PAC$PYRAMID;
/
