﻿create or replace package NDS2_MRR_USER.PAC$SYNC
as
  procedure START_WORK;
end;
/
create or replace package body NDS2_MRR_USER.PAC$SYNC
as
procedure SYNC_EGRN_IP as
begin
  insert into EGRN_IP_STAGE(ID, INNFL, OGRNIP, LAST_NAME, FIRST_NAME, PATRONYMIC, DATE_BIRTH, DOC_CODE, DOC_NUM, DOC_DATE, DOC_ORG, DOC_ORG_CODE, ADR,CODE_NO,DATE_ON, REASON_ON,DATE_OFF,REASON_OFF)
  select
  ID,
  INNFL,
  OGRNIP,
  LAST_NAME,
  FIRST_NAME,
  PATRONYMIC,
  DATE_BIRTH,
  DOC_CODE,
  DOC_NUM,
  DOC_DATE,
  DOC_ORG,
  DOC_ORG_CODE,
  ADR,
  CODE_NO,
  DATE_ON,
  REASON_ON,
  DATE_OFF,
  REASON_OFF
  from v$egrn_ip_alias where ActualSign = 1;
  merge into V_EGRN_IP trg using (select * from EGRN_IP_STAGE) src on (src.innfl = trg.innfl)
  when matched then
  update set
    trg.ogrnip = src.ogrnip,
    trg.last_name = src.last_name,
    trg.first_name = src.first_name,
    trg.patronymic = src.patronymic,
    trg.date_birth = src.date_birth,
    trg.doc_code = src.doc_code,
    trg.doc_num = src.doc_num,
    trg.doc_date = src.doc_date,
    trg.doc_org = src.doc_org,
    trg.doc_org_code = src.doc_org_code,
    trg.adr = src.adr,
    trg.code_no = src.code_no,
    trg.date_on = src.date_on,
    trg.reason_on = src.reason_on,
    trg.date_off = src.date_off,
    trg.reason_off = src.reason_off
  when not matched then
  insert (
    trg.id,
    trg.innfl,
    trg.ogrnip,
    trg.last_name,
    trg.first_name,
    trg.patronymic,
    trg.date_birth,
    trg.doc_code,
    trg.doc_num,
    trg.doc_date,
    trg.doc_org,
    trg.doc_org_code,
    trg.adr,
    trg.code_no,
    trg.date_on,
    trg.reason_on,
    trg.date_off,
    trg.reason_off
    ) values (
    src.id,
    src.innfl,
    src.ogrnip,
    src.last_name,
    src.first_name,
    src.patronymic,
    src.date_birth,
    src.doc_code,
    src.doc_num,
    src.doc_date,
    src.doc_org,
    src.doc_org_code,
    src.adr,
    src.code_no,
    src.date_on,
    src.reason_on,
    src.date_off,
    src.reason_off
    );
  commit;
  execute immediate 'truncate table EGRN_IP_STAGE';
  exception when others then
  begin
    rollback;
    NDS2$SYS.LOG_ERROR('PAC$SYNC.SYNC_EGRUL_IP', null, null, SUBSTR(SQLERRM, 1 , 2040));
  end;
end;

procedure SYNC_EGRN_UL as
begin
  insert into EGRN_UL_STAGE(ID,INN,KPP,OGRN,NAME_FULL,DATE_REG,CODE_NO,DATE_ON,DATE_OFF,REASON_OFF,ADR,UST_CAPITAL,IS_BIGGEST)
  select
    a.ID,
    a.INN,
    a.KPP,
    a.OGRN,
    a.NAME_FULL,
    a.DATE_REG,
    a.CODE_NO,
    a.DATE_ON,
    a.DATE_OFF,
    a.REASON_OFF,
    a.ADR,
    a.UST_CAPITAL,
    a.IS_KNP
  from v$egrn_ul_alias a where a.ActualSign = 1;
  merge into V_EGRN_UL trg using (select * from EGRN_UL_STAGE) src on (src.inn = trg.inn and src.kpp = trg.kpp)
  when matched then
  update set
    trg.ogrn = src.ogrn,
    trg.name_full = src.name_full,
    trg.date_reg = src.date_reg,
    trg.code_no = src.code_no,
    trg.date_on = src.date_on,
    trg.date_off = src.date_off,
    trg.reason_off = src.reason_off,
    trg.adr = src.adr,
    trg.ust_capital = src.ust_capital,
    trg.is_biggest = src.is_biggest
  when not matched then
  insert (
    trg.id,
    trg.inn,
    trg.kpp,
    trg.ogrn,
    trg.name_full,
    trg.date_reg,
    trg.code_no,
    trg.date_on,
    trg.date_off,
    trg.reason_off,
    trg.adr,
    trg.ust_capital,
    trg.is_biggest
    ) values (
    src.id,
    src.inn,
    src.kpp,
    src.ogrn,
    src.name_full,
    src.date_reg,
    src.code_no,
    src.date_on,
    src.date_off,
    src.reason_off,
    src.adr,
    src.ust_capital,
    src.is_biggest
    );
  commit;
  execute immediate 'truncate table EGRN_UL_STAGE';
  exception when others then
  begin
    rollback;
    NDS2$SYS.LOG_ERROR('PAC$SYNC.SYNC_EGRUL_UL', null, null, SUBSTR(SQLERRM, 1 , 2040));
  end;
end;

procedure SYNC_SUR as
begin

  insert into SUR_STAGE(inn, fiscal_year, fiscal_period, sign_code, update_date)  select a.INN, a.PYEAR, a.NPERIOD, a.PR, a.DPR from v$sur_alias a where a.ActualSign = 1;

  merge into EXT_SUR trg using (select * from SUR_STAGE) src on (src.inn = trg.inn and src.fiscal_year = trg.fiscal_year and src.fiscal_period = trg.fiscal_period)
  when matched then
  update set
    trg.sign_code = src.sign_code,
    trg.update_date = src.update_date
  when not matched then
  insert (
    trg.inn,
    trg.fiscal_year,
    trg.fiscal_period,
    trg.sign_code,
    trg.update_date
  ) values (
    src.inn,
    src.fiscal_year,
    src.fiscal_period,
    src.sign_code,
    src.update_date
  );
  commit;
  execute immediate 'truncate table SUR_STAGE';
  exception when others then
  begin
    rollback;
    NDS2$SYS.LOG_ERROR('PAC$SYNC.SYNC_SUR', null, null, SUBSTR(SQLERRM, 1 , 2040));
  end;
end;

procedure SYNC_SONO as
begin
  merge into ext_sono trg using (select * from v$sono_alias where ActualSign = 1) src
  on (src.s_code = trg.s_code)
  when matched then
  update set
    trg.s_name = src.s_name
  when not matched then
  insert (
    trg.s_code,
    trg.s_name
  ) values (
    src.s_code,
    src.s_name
  );
  commit;
  exception when others then
  begin
    rollback;
    NDS2$SYS.LOG_ERROR('PAC$SYNC.SYNC_SONO', null, null, SUBSTR(SQLERRM, 1 , 2040));
  end;
end;

procedure SYNC_SSRF as
begin
  merge into ext_ssrf trg using (select * from v$ssrf_alias where ActualSign = 1) src
  on (src.s_code = trg.s_code)
  when matched then
  update set
    trg.s_name = src.s_name
  when not matched then
  insert (
    trg.s_code,
    trg.s_name
  ) values (
    src.s_code,
    src.s_name
  );
  commit;
  exception when others then
  begin
    rollback;
    NDS2$SYS.LOG_ERROR('PAC$SYNC.SYNC_SSRF', null, null, SUBSTR(SQLERRM, 1 , 2040));
  end;
end;

procedure START_WORK as
begin
  SYNC_SONO;
  SYNC_SSRF;
  SYNC_EGRN_IP;
  SYNC_EGRN_UL;
  SYNC_SUR;
end;
end;
/
