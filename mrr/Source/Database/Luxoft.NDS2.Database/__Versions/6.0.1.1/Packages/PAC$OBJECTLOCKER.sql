﻿create or replace package NDS2_MRR_USER.PAC$OBJECTLOCKER as
function LOCK_OBJECT
(
  p_object_id in Object_Lock.Object_Id%type,
  p_object_type_id in Object_Lock.Object_Type_Id%type,
  p_user_name in Object_Lock.User_Name%type,
  p_host_name in Object_Lock.Host_Name%type
) return varchar2;

procedure UNLOCK_OBJECT
(
  p_object_key in Object_Lock.Object_Key%type,
  p_user_name in Object_Lock.User_Name%type,
  p_host_name in Object_Lock.Host_Name%type
);

function CHECK_LOCK
(
  p_object_id in Object_Lock.Object_Id%type,
  p_object_type_id in Object_Lock.Object_Type_Id%type,
  p_object_key in Object_Lock.Object_Key%type
) return decimal;

function GET_LOCKERS
(
  p_object_id in Object_Lock.Object_Id%type,
  p_object_type_id in Object_Lock.Object_Type_Id%type
) return varchar2;

end PAC$OBJECTLOCKER;
/
create or replace package body NDS2_MRR_USER.PAC$OBJECTLOCKER as
function LOCK_OBJECT
(
  p_object_id in Object_Lock.Object_Id%type,
  p_object_type_id in Object_Lock.Object_Type_Id%type,
  p_user_name in Object_Lock.User_Name%type,
  p_host_name in Object_Lock.Host_Name%type
) return varchar2
as
  v_count number := 0;
  v_name varchar2(128);
begin
  delete from Object_Lock
  where (to_date(to_char(SysTimestamp, 'dd/mm/yyyy  hh24:mi:ss'), 'dd/mm/yyyy  hh24:mi:ss')
      - to_date(to_char(LockTime, 'dd/mm/yyyy  hh24:mi:ss'), 'dd/mm/yyyy  hh24:mi:ss'))
      * (24 * 60) > 60;

  select count(*) into v_count 
  from dual 
  inner join Object_Lock ol on (ol.Object_Id = p_object_id and ol.Object_Type_Id = p_object_type_id);

  if v_count = 0 then
	select rawtohex(sys_guid()) into v_name from dual;
	insert into Object_Lock values (p_object_id, p_object_type_id, v_name, p_user_name, p_host_name, SysTimestamp);
    return v_name;
  else
    return null;
  end if;
end;

procedure UNLOCK_OBJECT
(
  p_object_key in Object_Lock.Object_Key%type,
  p_user_name in Object_Lock.User_Name%type,
  p_host_name in Object_Lock.Host_Name%type
)
as
begin
  delete from Object_Lock
  where Object_Key = p_object_key
    and User_Name = p_user_name
    and Host_Name = p_host_name;
end;

function CHECK_LOCK
(
  p_object_id in Object_Lock.Object_Id%type,
  p_object_type_id in Object_Lock.Object_Type_Id%type,
  p_object_key in Object_Lock.Object_Key%type
) return decimal
as
  v_count decimal := 0;
begin

  delete from Object_Lock
  where (to_date(to_char(SysTimestamp, 'dd/mm/yyyy  hh24:mi:ss'), 'dd/mm/yyyy  hh24:mi:ss')
      - to_date(to_char(LockTime, 'dd/mm/yyyy  hh24:mi:ss'), 'dd/mm/yyyy  hh24:mi:ss'))
      * (24 * 60) > 60;

  select count(*) into v_count 
  from dual 
  inner join Object_Lock ol on (ol.Object_Id = p_object_id and ol.Object_Type_Id = p_object_type_id and ol.Object_Key = p_object_key);

  return v_count;
end;



function GET_LOCKERS
(
  p_object_id in Object_Lock.Object_Id%type,
  p_object_type_id in Object_Lock.Object_Type_Id%type
) return varchar2
as
  v_name varchar2(2048);
begin
  v_name := '';

  delete from Object_Lock
  where (to_date(to_char(SysTimestamp, 'dd/mm/yyyy  hh24:mi:ss'), 'dd/mm/yyyy  hh24:mi:ss')
      - to_date(to_char(LockTime, 'dd/mm/yyyy  hh24:mi:ss'), 'dd/mm/yyyy  hh24:mi:ss'))
      * (24 * 60) > 60;

  for obj_lock in (select User_Name, Host_Name from Object_Lock where Object_Id = p_object_id and Object_Type_Id = p_object_type_id)
  loop
    v_name := v_name || obj_lock.user_name || ' ' || obj_lock.host_name || ';';
  end loop;

  return v_name;
end;

end PAC$OBJECTLOCKER;
/
