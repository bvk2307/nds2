﻿CREATE OR REPLACE PACKAGE NDS2_MRR_USER."NDS2$REPORTS" IS

procedure START_WORK;
procedure START_DECLARATION_CALCULATE;

TYPE T_ASSOCIATIVE_ARRAY IS TABLE OF VARCHAR(50) INDEX BY PLS_INTEGER;

PROCEDURE P$REPORT_SAVE_TASK
(
      pReportNum IN NUMBER,
      pFederalCodes IN T_ASSOCIATIVE_ARRAY,
      pRegionCodes IN T_ASSOCIATIVE_ARRAY,
      pSounCodes IN T_ASSOCIATIVE_ARRAY,
      pNalogPeriod IN NUMBER,
      pFiscalYear IN NUMBER,
      pDateBegin IN Date,
      pDateEnd IN Date,
      pDateCreateReport IN Date,
      pDeclStatisticType IN NUMBER,
      pDeclStatisticRegim IN NUMBER,      
      pTaskId OUT NUMBER
);

function F$GET_LOAD_INSP_NEXTVAL return number;
function F$GET_MONITOR_DECL_NEXTVAL return number;
function F$GET_INSPEC_MONITOR_NEXTVAL return number;
function F$GET_CHECK_CR_NEXTVAL return number;
function F$GET_MATCHING_RULE_NEXTVAL return number;
function F$GET_CHECK_LC_NEXTVAL return number;
function F$GET_DECLARATION_RAW_NEXTVAL return number;
function F$GET_DECLARATION_NEXTVAL return number;
procedure P$DO_REPORT_CREATE;
procedure P$REPORT_CREATE
(
    pReportNum IN NUMBER,
    pTaskId IN NUMBER,
    pRequestId IN NUMBER
);
procedure P$CREATE_INSPEC_MONITOR_WORK
(
  pTaskId IN NUMBER
);
procedure P$CREATE_MONITOR_DECLARATION
(
  pTaskId IN NUMBER
);
procedure P$CREATE_LOADING_INSPECTION(
  pTaskId IN NUMBER
);
procedure P$CREATE_CHECK_CONTROL_RATIO
(
  pTaskId IN NUMBER
);
procedure P$CREATE_MATCHING_RULE
(
  pTaskId IN NUMBER
);
procedure P$CREATE_CHECK_LOGIC_CONTROL
(
  pTaskId IN NUMBER,
  pRequestId IN NUMBER
);
procedure P$REPORT_LK_REQUEST_STATUS (
  pRequestId in SOV_LK_REPORT_REQUEST.REQUEST_ID%type,
  pData out SYS_REFCURSOR
);
procedure P$CALCULATE_DECLARATION_RAW
(
   pSubmitDate IN DATE
);
procedure P$CREATE_DECLARATION
(
  pTaskId IN NUMBER
);

END NDS2$REPORTS;
/

CREATE OR REPLACE PACKAGE BODY  NDS2_MRR_USER."NDS2$REPORTS" IS

procedure START_WORK
  as
begin
  NDS2$SYS.LOG_INFO(
  P_SITE => 'NDS2$REPORTS.START_WORK',   P_ENTITY_ID => null,
  P_MESSAGE_CODE => SQLCODE,   P_MESSAGE => 'starting');
  begin
    P$DO_REPORT_CREATE;
  exception when others then null;
  end;
end;

procedure START_DECLARATION_CALCULATE
  as
begin
  NDS2$SYS.LOG_INFO(
  P_SITE => 'NDS2$REPORTS.START_DECLARATION_CALCULATE',   P_ENTITY_ID => null,
  P_MESSAGE_CODE => SQLCODE,   P_MESSAGE => 'starting');
  begin
    P$CALCULATE_DECLARATION_RAW (sysdate);
  exception when others then null;
  end;
end;

PROCEDURE P$REPORT_SAVE_TASK
(
      pReportNum IN NUMBER,
      pFederalCodes IN T_ASSOCIATIVE_ARRAY,
      pRegionCodes IN T_ASSOCIATIVE_ARRAY,
      pSounCodes IN T_ASSOCIATIVE_ARRAY,
      pNalogPeriod IN NUMBER,
      pFiscalYear IN NUMBER,
      pDateBegin IN Date,
      pDateEnd IN Date,
      pDateCreateReport IN Date,
      pDeclStatisticType IN NUMBER,
      pDeclStatisticRegim IN NUMBER,
      pTaskId OUT NUMBER
)
as
  task_id number(10);
begin
  task_id := SEQ_REPORT_TASK.NEXTVAL;
  INSERT INTO REPORT_TASK VALUES(task_id, pReportNum, SYSDATE, 0, NULL);

  if pNalogPeriod is not null then
     INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', pNalogPeriod);
     if pReportNum = 51 then
     begin
       if pNalogPeriod = 21 then
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 51);
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 01);
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 71);
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 02);
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 72);
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 03);
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 73);
       end if;
       if pNalogPeriod = 22 then
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 54);
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 04);
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 74);
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 05);
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 75);
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 06);
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 76);
       end if;
       if pNalogPeriod = 23 then
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 55);
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 07);
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 77);
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 08);
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 78);
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 09);
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 79);
       end if;
       if pNalogPeriod = 24 then
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 56);
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 10);
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 80);
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 11);
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 81);
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 12);
         INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'NALOG_PERIOD', 82);
       end if;
     end;
     end if;
  End If;
  if pFiscalYear is not null then
     INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'FISCAL_YEAR', pFiscalYear);
  End If;
  if pDateBegin is not null then
    INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'DATE_BEGIN', pDateBegin);
  End If;
  if pDateEnd is not null then
    INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'DATE_END', pDateEnd);
  End If;
  if pDateCreateReport is not null then
     INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'DATE_CREATE_REPORT', pDateCreateReport);
  End If;

  if pDeclStatisticType is not null then
     INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'DECL_STATISTIC_TYPE', pDeclStatisticType);
  End If;
  if pDeclStatisticRegim is not null then
     INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'DECL_STATISTIC_REGIM', pDeclStatisticRegim);
  End If;
  
  FORALL i IN pFederalCodes.FIRST .. pFederalCodes.LAST
  INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'FEDERAL_CODE', pFederalCodes(i));

  FORALL i IN pRegionCodes.FIRST .. pRegionCodes.LAST
  INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'REGION_CODE', pRegionCodes(i));

  FORALL i IN pSounCodes.FIRST .. pSounCodes.LAST
  INSERT INTO REPORT_TASK_PARAM VALUES(SEQ_REPORT_TASK_PARAM.NEXTVAL, task_id, 'SOUN_CODE', pSounCodes(i));

  commit;

  pTaskId := task_id;
end;


function F$GET_LOAD_INSP_NEXTVAL return number
is
Result number;
begin
  select SEQ_REPORT_LOAD_INSP.NEXTVAL into Result from dual;
  return Result;
end;

function F$GET_MONITOR_DECL_NEXTVAL return number
is
Result number;
begin
  select SEQ_REPORT_MONITOR_DECL.NEXTVAL into Result from dual;
  return Result;
end;

function F$GET_INSPEC_MONITOR_NEXTVAL return number
is
Result number;
begin
  select SEQ_REPORT_INSPEC_MONITOR_WORK.NEXTVAL into Result from dual;
  return Result;
end;

function F$GET_CHECK_CR_NEXTVAL return number
is
Result number;
begin
  select SEQ_REPORT_CHECK_CR.NEXTVAL into Result from dual;
  return Result;
end;

function F$GET_MATCHING_RULE_NEXTVAL return number
is
Result number;
begin
  select SEQ_REPORT_MATCHING_RULE.NEXTVAL into Result from dual;
  return Result;
end;

function F$GET_CHECK_LC_NEXTVAL return number
is
Result number;
begin
  select SEQ_REPORT_LOGIC_CONTROL.NEXTVAL into Result from dual;
  return Result;
end;

function F$GET_DECLARATION_RAW_NEXTVAL return number
is
Result number;
begin
  select SEQ_REPORT_DECLARATION_RAW.NEXTVAL into Result from dual;
  return Result;
end;


function F$GET_DECLARATION_NEXTVAL return number
is
Result number;
begin
  select SEQ_REPORT_DECLARATION.NEXTVAL into Result from dual;
  return Result;
end;

procedure P$DO_REPORT_CREATE
as
  CURSOR tasks IS
	SELECT * FROM REPORT_TASK WHERE TASK_STATUS = 0;
	v_tasks tasks%ROWTYPE;
begin

  OPEN tasks;
	FETCH tasks INTO v_tasks;
	LOOP

    --P$REPORT_CREATE(v_tasks.report_num, v_tasks.ID);

		FETCH tasks INTO v_tasks;
		EXIT WHEN tasks%NOTFOUND;
	END LOOP;

	CLOSE tasks;

end;

procedure P$REPORT_CREATE
(
    pReportNum IN NUMBER,
    pTaskId IN NUMBER,
    pRequestId IN NUMBER
)
as
begin

    if pReportNum = 16 then
        P$CREATE_INSPEC_MONITOR_WORK (pTaskId);
    elsif pReportNum = 17 then
        P$CREATE_MONITOR_DECLARATION (pTaskId);
    elsif pReportNum = 21 then
        P$CREATE_LOADING_INSPECTION (pTaskId);
    elsif pReportNum = 14 then
        P$CREATE_CHECK_CONTROL_RATIO (pTaskId);
    elsif pReportNum = 12 then
        P$CREATE_MATCHING_RULE (pTaskId);
    elsif pReportNum = 13 then
        P$CREATE_CHECK_LOGIC_CONTROL (pTaskId, pRequestId);
    elsif pReportNum = 51 then
        P$CREATE_DECLARATION (pTaskId);
    end if;

end;

-- Создание отчета Мониторинг работы налоговых инспекторов
procedure P$CREATE_INSPEC_MONITOR_WORK
(
  pTaskId IN NUMBER
)
as
  pTaskStatus NUMBER := -1;
  pDateBegin DATE;
  pDateEnd DATE;
  pNalogPeriod NUMBER(3);
  pFiscalYear NUMBER(5);
  pActualNumActive Number(1);

  CURSOR reports IS
  SELECT * FROM REPORT_INSPECTOR_MONITOR_WORK WHERE TASK_ID = pTaskId;
  v_reports reports%ROWTYPE;

  pNotEliminated_GeneralCount NUMBER(19);
  pNotEliminated_GeneralSum NUMBER(19,2);
  pNotElimin_CurrencyCount NUMBER(19);
  pNotElimin_CurrencySum NUMBER(19,2);
  pNotElimin_NDSCount NUMBER(19);
  pNotElimin_NDSSum NUMBER(19,2);
  pNotElimin_NotExactCount NUMBER(19);
  pNotElimin_NotExactSum NUMBER(19,2);
  pNotElimin_GapCount NUMBER(19);
  pNotElimin_GapSum NUMBER(19,2);
  pIdentNotElimin_GeneralCount NUMBER(19);
  pIdentNotElimin_GeneralSum NUMBER(19,2);
  pIdentNotElimin_CurrencyCount NUMBER(19);
  pIdentNotElimin_CurrencySum NUMBER(19,2);
  pIdentNotElimin_NDSCount NUMBER(19);
  pIdentNotElimin_NDSSum NUMBER(19,2);
  pIdentNotElimin_NotExactCount NUMBER(19);
  pIdentNotElimin_NotExactSum NUMBER(19,2);
  pIdentNotElimin_GapCount NUMBER(19);
  pIdentNotElimin_GapSum NUMBER(19,2);
  pIdentElimin_GeneralCount NUMBER(19);
  pIdentElimin_GeneralSum NUMBER(19,2);
  pIdentElimin_CurrencyCount NUMBER(19);
  pIdentElimin_CurrencySum NUMBER(19,2);
  pIdentElimin_NDSCount NUMBER(19);
  pIdentElimin_NDSSum NUMBER(19,2);
  pIdentElimin_NotExactCount NUMBER(19);
  pIdentElimin_NotExactSum NUMBER(19,2);
  pIdentElimin_GapCount NUMBER(19);
  pIdentElimin_GapSum NUMBER(19,2);
begin
  pActualNumActive := 1;

  SELECT task_status into pTaskStatus FROM dual
  left outer join (SELECT task_status FROM REPORT_TASK WHERE ID = pTaskId) r on 1 = 1;

  if pTaskStatus = 0 then
  begin
      -- получение одиночных параметров поиска
      SELECT PARAM_VALUE into pDateBegin FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'DATE_BEGIN';
      SELECT PARAM_VALUE into pDateEnd FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'DATE_END';
      SELECT PARAM_VALUE into pNalogPeriod FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD';
      SELECT PARAM_VALUE into pFiscalYear FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'FISCAL_YEAR';

      -- создание уникальных строк РЕГИОН-ИНСПЕКЦИЯ
      INSERT INTO REPORT_INSPECTOR_MONITOR_WORK (ID, TASK_ID, ACTUAL_NUM, AGG_ROW, FEDERALCODE, FEDERALDISTRICT, REGIONCODE, REGION, INSPECTIONCODE, INSPECTION, INSPECTOR)
      SELECT
        NDS2$REPORTS.F$GET_LOAD_INSP_NEXTVAL
        ,pTaskId
        ,pActualNumActive
        ,0 as AGG_ROW
        ,'01' as FederalCode
        ,'Тестовый ФО' as FederalDistrict
        ,reg.S_CODE as RegionCode
        ,reg.S_CODE || '-' || reg.S_NAME as Region
        ,s.S_CODE as InspectionCode
        ,s.S_CODE || '-' || s.S_NAME as Inspection
        ,(case when (d.INSPECTOR is null) or (d.INSPECTOR is not null and trim(d.INSPECTOR) = '') then 'Не назначен' else d.INSPECTOR end) as Inspector
      FROM
      (
        SELECT dis.TAXPAYERREGIONCODE, dis.TAXPAYERSOUNCODE, dis.INSPECTOR
        FROM DOC dc
        JOIN V$DISCREPANCY dis on dc.DOC_ID = dis.CLAIM_ID
        WHERE dis.TaxPayerSounCode in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'SOUN_CODE')
          AND dis.TaxPayerRegionCode in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'REGION_CODE')
          AND dis.TaxPayerPeriodCode = pNalogPeriod
          AND dis.TaxPayerYearCode = pFiscalYear
        GROUP BY dis.TAXPAYERREGIONCODE, dis.TAXPAYERSOUNCODE, dis.INSPECTOR
      ) d
      LEFT OUTER JOIN V$SSRF reg on reg.S_CODE = d.TAXPAYERREGIONCODE
      LEFT OUTER JOIN V$SONO s on s.S_CODE = d.TAXPAYERSOUNCODE
      WHERE (d.TAXPAYERREGIONCODE is not null) and (d.TAXPAYERSOUNCODE is not null);

      -- Наполнение колонок(кол-во, сумма) всех уникальных строк
      OPEN reports;
      FETCH reports INTO v_reports;
      LOOP
        select nvl(ch01.NotEliminated_GeneralCount, 0), nvl(ch01.NotEliminated_GeneralSum, 0),
               nvl(ch01.NotElimin_CurrencyCount, 0), nvl(ch01.NotElimin_CurrencySum, 0),
               nvl(ch01.NotElimin_NDSCount, 0), nvl (ch01.NotElimin_NDSSum, 0), nvl(ch01.NotElimin_NotExactCount, 0),
               nvl(ch01.NotElimin_NotExactSum, 0), nvl(ch01.NotElimin_GapCount, 0), nvl(ch01.NotElimin_GapSum, 0)
        into pNotEliminated_GeneralCount, pNotEliminated_GeneralSum, pNotElimin_CurrencyCount, pNotElimin_CurrencySum,
             pNotElimin_NDSCount, pNotElimin_NDSSum, pNotElimin_NotExactCount, pNotElimin_NotExactSum,
             pNotElimin_GapCount, pNotElimin_GapSum from dual
        left outer join
        (
          SELECT distinct TAXPAYERREGIONCODE, TAXPAYERSOUNCODE
            ,COUNT(DISCREPANCYID) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) NotEliminated_GeneralCount
            ,SUM(Amount) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) NotEliminated_GeneralSum
            ,COUNT(case when typecode=3 then DISCREPANCYID end) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) NotElimin_CurrencyCount
            ,SUM(case when typecode=3 then Amount end) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) NotElimin_CurrencySum
            ,COUNT(case when typecode=4 then DISCREPANCYID end) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) NotElimin_NDSCount
            ,SUM(case when typecode=4 then Amount end) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) NotElimin_NDSSum
            ,COUNT(case when typecode=2 then DISCREPANCYID end) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) NotElimin_NotExactCount
            ,SUM(case when typecode=2 then Amount end) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) NotElimin_NotExactSum
            ,COUNT(case when typecode=1 then DISCREPANCYID end) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) NotElimin_GapCount
            ,SUM(case when typecode=1 then Amount end) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) NotElimin_GapSum
          FROM DOC dc
          JOIN V$DISCREPANCY dis on dc.DOC_ID = dis.CLAIM_ID
          WHERE dis.TaxPayerSounCode in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'SOUN_CODE')
            AND dis.TaxPayerRegionCode in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'REGION_CODE')
            AND dis.TaxPayerPeriodCode = pNalogPeriod
            AND dis.TaxPayerYearCode = pFiscalYear
            AND trunc(dis.FoundDate) <= trunc(pDateBegin) AND trunc(dc.Create_Date) <= trunc(pDateBegin)
            AND (trunc(dis.Close_Date) > trunc(pDateBegin) or dis.Close_Date is null)
        ) ch01 on ch01.TAXPAYERREGIONCODE = v_reports.RegionCode and ch01.TAXPAYERSOUNCODE = v_reports.InspectionCode;

        select nvl(ch01a.IdentNotElimin_GeneralCount, 0), nvl(ch01a.IdentNotElimin_GeneralSum, 0), nvl(ch01a.IdentNotElimin_CurrencyCount, 0),
               nvl(ch01a.IdentNotElimin_CurrencySum, 0), nvl(ch01a.IdentNotElimin_NDSCount, 0), nvl(ch01a.IdentNotElimin_NDSSum, 0),
               nvl(ch01a.IdentNotElimin_NotExactCount, 0), nvl(ch01a.IdentNotElimin_NotExactSum, 0),
               nvl(ch01a.IdentNotElimin_GapCount, 0), nvl(ch01a.IdentNotElimin_GapSum, 0)
        into pIdentNotElimin_GeneralCount, pIdentNotElimin_GeneralSum, pIdentNotElimin_CurrencyCount, pIdentNotElimin_CurrencySum,
             pIdentNotElimin_NDSCount, pIdentNotElimin_NDSSum, pIdentNotElimin_NotExactCount, pIdentNotElimin_NotExactSum,
             pIdentNotElimin_GapCount, pIdentNotElimin_GapSum
         from dual
        left outer join
        (
          SELECT distinct TAXPAYERREGIONCODE, TAXPAYERSOUNCODE
            ,COUNT(DISCREPANCYID) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) IdentNotElimin_GeneralCount
            ,SUM(Amount) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) IdentNotElimin_GeneralSum
            ,COUNT(case when typecode=3 then DISCREPANCYID end) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) IdentNotElimin_CurrencyCount
            ,SUM(case when typecode=3 then Amount end) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) IdentNotElimin_CurrencySum
            ,COUNT(case when typecode=4 then DISCREPANCYID end) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) IdentNotElimin_NDSCount
            ,SUM(case when typecode=4 then Amount end) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) IdentNotElimin_NDSSum
            ,COUNT(case when typecode=2 then DISCREPANCYID end) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) IdentNotElimin_NotExactCount
            ,SUM(case when typecode=2 then Amount end) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) IdentNotElimin_NotExactSum
            ,COUNT(case when typecode=1 then DISCREPANCYID end) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) IdentNotElimin_GapCount
            ,SUM(case when typecode=1 then Amount end) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) IdentNotElimin_GapSum
          FROM DOC dc
          JOIN V$DISCREPANCY dis on dc.DOC_ID = dis.CLAIM_ID
          WHERE dis.TaxPayerSounCode in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'SOUN_CODE')
            AND dis.TaxPayerRegionCode in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'REGION_CODE')
            AND dis.TaxPayerPeriodCode = pNalogPeriod
            AND dis.TaxPayerYearCode = pFiscalYear
            AND trunc(dis.FoundDate) <= trunc(pDateBegin) AND trunc(dc.Create_Date) <= trunc(pDateBegin)
            AND (trunc(dis.Close_Date) > trunc(pDateEnd) or dis.Close_Date is null)
        ) ch01a on ch01a.TAXPAYERREGIONCODE = v_reports.RegionCode and ch01a.TAXPAYERSOUNCODE = v_reports.InspectionCode;

        pIdentElimin_GeneralCount := pNotEliminated_GeneralCount - pIdentNotElimin_GeneralCount;
        pIdentElimin_GeneralSum := pNotEliminated_GeneralSum - pIdentNotElimin_GeneralSum;
        pIdentElimin_CurrencyCount := pNotElimin_CurrencyCount - pIdentNotElimin_CurrencyCount;
        pIdentElimin_CurrencySum := pNotElimin_CurrencySum - pIdentNotElimin_CurrencySum;
        pIdentElimin_NDSCount := pNotElimin_NDSCount - pIdentNotElimin_NDSCount;
        pIdentElimin_NDSSum := pNotElimin_NDSSum - pIdentNotElimin_NDSSum;
        pIdentElimin_NotExactCount := pNotElimin_NotExactCount - pIdentNotElimin_NotExactCount;
        pIdentElimin_NotExactSum := pNotElimin_NotExactSum - pIdentNotElimin_NotExactSum;
        pIdentElimin_GapCount := pNotElimin_GapCount - pIdentNotElimin_GapCount;
        pIdentElimin_GapSum := pNotElimin_GapSum - pIdentNotElimin_GapSum;

        UPDATE REPORT_INSPECTOR_MONITOR_WORK r SET
                NotEliminated_GeneralCount = pNotEliminated_GeneralCount,
                NotEliminated_GeneralSum = pNotEliminated_GeneralSum,
                NotElimin_CurrencyCount = pNotElimin_CurrencyCount,
                NotElimin_CurrencySum = pNotElimin_CurrencySum,
                NotElimin_NDSCount = pNotElimin_NDSCount,
                NotElimin_NDSSum = pNotElimin_NDSSum,
                NotElimin_NotExactCount = pNotElimin_NotExactCount,
                NotElimin_NotExactSum = pNotElimin_NotExactSum,
                NotElimin_GapCount = pNotElimin_GapCount,
                NotElimin_GapSum = pNotElimin_GapSum,
                IdentNotElimin_GeneralCount = pIdentNotElimin_GeneralCount,
                IdentNotElimin_GeneralSum = pIdentNotElimin_GeneralSum,
                IdentNotElimin_CurrencyCount = pIdentNotElimin_CurrencyCount,
                IdentNotElimin_CurrencySum = pIdentNotElimin_CurrencySum,
                IdentNotElimin_NDSCount = pIdentNotElimin_NDSCount,
                IdentNotElimin_NDSSum = pIdentNotElimin_NDSSum,
                IdentNotElimin_NotExactCount = pIdentNotElimin_NotExactCount,
                IdentNotElimin_NotExactSum = pIdentNotElimin_NotExactSum,
                IdentNotElimin_GapCount = pIdentNotElimin_GapCount,
                IdentNotElimin_GapSum = pIdentNotElimin_GapSum,
                IdentElimin_GeneralCount =  pIdentElimin_GeneralCount,
                IdentElimin_GeneralSum  = pIdentElimin_GeneralSum,
                IdentElimin_CurrencyCount = pIdentElimin_CurrencyCount,
                IdentElimin_CurrencySum = pIdentElimin_CurrencySum,
                IdentElimin_NDSCount = pIdentElimin_NDSCount,
                IdentElimin_NDSSum = pIdentElimin_NDSSum,
                IdentElimin_NotExactCount = pIdentElimin_NotExactCount,
                IdentElimin_NotExactSum = pIdentElimin_NotExactSum,
                IdentElimin_GapCount = pIdentElimin_GapCount,
                IdentElimin_GapSum = pIdentElimin_GapSum
        WHERE r.Region = v_reports.Region and r.Inspection = v_reports.Inspection;

        FETCH reports INTO v_reports;
        EXIT WHEN reports%NOTFOUND;
      END LOOP;

      CLOSE reports;

      -- Создание агрегирующих строк (один регион, одна инспекция, все инспекторы)
      INSERT INTO REPORT_INSPECTOR_MONITOR_WORK
      SELECT NDS2$REPORTS.F$GET_LOAD_INSP_NEXTVAL
            ,pTaskId
            ,pActualNumActive
            ,1 as AGG_ROW
            ,MIN(i.FederalCode) as FederalCode
            ,MIN(i.FederalDistrict) as FederalDistrict
            ,MIN(i.REGIONCODE) as RegionCode
            ,MIN(i.Region) as Region
            ,i.InspectionCode as InspectionCode
            ,MIN(i.Inspection) as Inspection
            ,'*' as Inspector
            ,SUM(i.NotEliminated_GeneralCount) as NotEliminated_GeneralCount
            ,SUM(i.NotEliminated_GeneralSum) as NotEliminated_GeneralSum
            ,SUM(i.NotElimin_CurrencyCount) as NotElimin_CurrencyCount
            ,SUM(i.NotElimin_CurrencySum) as NotElimin_CurrencySum
            ,SUM(i.NotElimin_NDSCount) as NotElimin_NDSCount
            ,SUM(i.NotElimin_NDSSum) as NotElimin_NDSSum
            ,SUM(i.NotElimin_NotExactCount) as NotElimin_NotExactCount
            ,SUM(i.NotElimin_NotExactSum) as NotElimin_NotExactSum
            ,SUM(i.NotElimin_GapCount) as NotElimin_GapCount
            ,SUM(i.NotElimin_GapSum) as NotElimin_GapSum
            ,SUM(i.IdentNotElimin_GeneralCount) as IdentNotElimin_GeneralCount
            ,SUM(i.IdentNotElimin_GeneralSum) as IdentNotElimin_GeneralSum
            ,SUM(i.IdentNotElimin_CurrencyCount) as IdentNotElimin_CurrencyCount
            ,SUM(i.IdentNotElimin_CurrencySum) as IdentNotElimin_CurrencySum
            ,SUM(i.IdentNotElimin_NDSCount) as IdentNotElimin_NDSCount
            ,SUM(i.IdentNotElimin_NDSSum) as IdentNotElimin_NDSSum
            ,SUM(i.IdentNotElimin_NotExactCount) as IdentNotElimin_NotExactCount
            ,SUM(i.IdentNotElimin_NotExactSum) as IdentNotElimin_NotExactSum
            ,SUM(i.IdentNotElimin_GapCount) as IdentNotElimin_GapCount
            ,SUM(i.IdentNotElimin_GapSum) as IdentNotElimin_GapSum
            ,SUM(i.IdentElimin_GeneralCount) as IdentElimin_GeneralCount
            ,SUM(i.IdentElimin_GeneralSum) as IdentElimin_GeneralSum
            ,SUM(i.IdentElimin_CurrencyCount) as IdentElimin_CurrencyCount
            ,SUM(i.IdentElimin_CurrencySum) as IdentElimin_CurrencySum
            ,SUM(i.IdentElimin_NDSCount) as IdentElimin_NDSCount
            ,SUM(i.IdentElimin_NDSSum) as IdentElimin_NDSSum
            ,SUM(i.IdentElimin_NotExactCount) as IdentElimin_NotExactCount
            ,SUM(i.IdentElimin_NotExactSum) as IdentElimin_NotExactSum
            ,SUM(i.IdentElimin_GapCount) as IdentElimin_GapCount
            ,SUM(i.IdentElimin_GapSum) as IdentElimin_GapSum
      FROM REPORT_INSPECTOR_MONITOR_WORK i
      WHERE i.TASK_ID = pTaskId AND i.AGG_ROW = 0

      GROUP BY i.Inspectioncode;

      -- Создание агрегирующих строк (один регион, все инспекции)
      INSERT INTO REPORT_INSPECTOR_MONITOR_WORK
      SELECT NDS2$REPORTS.F$GET_LOAD_INSP_NEXTVAL
            ,pTaskId
            ,pActualNumActive
            ,1 as AGG_ROW
            ,MIN(i.FederalCode) as FederalCode
            ,MIN(i.FederalDistrict) as FederalDistrict
            ,i.REGIONCODE as RegionCode
            ,MIN(i.Region) as Region
            ,'*' as InspectionCode
            ,'*' as Inspection
            ,'*' as Inspector
            ,SUM(i.NotEliminated_GeneralCount) as NotEliminated_GeneralCount
            ,SUM(i.NotEliminated_GeneralSum) as NotEliminated_GeneralSum
            ,SUM(i.NotElimin_CurrencyCount) as NotElimin_CurrencyCount
            ,SUM(i.NotElimin_CurrencySum) as NotElimin_CurrencySum
            ,SUM(i.NotElimin_NDSCount) as NotElimin_NDSCount
            ,SUM(i.NotElimin_NDSSum) as NotElimin_NDSSum
            ,SUM(i.NotElimin_NotExactCount) as NotElimin_NotExactCount
            ,SUM(i.NotElimin_NotExactSum) as NotElimin_NotExactSum
            ,SUM(i.NotElimin_GapCount) as NotElimin_GapCount
            ,SUM(i.NotElimin_GapSum) as NotElimin_GapSum
            ,SUM(i.IdentNotElimin_GeneralCount) as IdentNotElimin_GeneralCount
            ,SUM(i.IdentNotElimin_GeneralSum) as IdentNotElimin_GeneralSum
            ,SUM(i.IdentNotElimin_CurrencyCount) as IdentNotElimin_CurrencyCount
            ,SUM(i.IdentNotElimin_CurrencySum) as IdentNotElimin_CurrencySum
            ,SUM(i.IdentNotElimin_NDSCount) as IdentNotElimin_NDSCount
            ,SUM(i.IdentNotElimin_NDSSum) as IdentNotElimin_NDSSum
            ,SUM(i.IdentNotElimin_NotExactCount) as IdentNotElimin_NotExactCount
            ,SUM(i.IdentNotElimin_NotExactSum) as IdentNotElimin_NotExactSum
            ,SUM(i.IdentNotElimin_GapCount) as IdentNotElimin_GapCount
            ,SUM(i.IdentNotElimin_GapSum) as IdentNotElimin_GapSum
            ,SUM(i.IdentElimin_GeneralCount) as IdentElimin_GeneralCount
            ,SUM(i.IdentElimin_GeneralSum) as IdentElimin_GeneralSum
            ,SUM(i.IdentElimin_CurrencyCount) as IdentElimin_CurrencyCount
            ,SUM(i.IdentElimin_CurrencySum) as IdentElimin_CurrencySum
            ,SUM(i.IdentElimin_NDSCount) as IdentElimin_NDSCount
            ,SUM(i.IdentElimin_NDSSum) as IdentElimin_NDSSum
            ,SUM(i.IdentElimin_NotExactCount) as IdentElimin_NotExactCount
            ,SUM(i.IdentElimin_NotExactSum) as IdentElimin_NotExactSum
            ,SUM(i.IdentElimin_GapCount) as IdentElimin_GapCount
            ,SUM(i.IdentElimin_GapSum) as IdentElimin_GapSum
      FROM REPORT_INSPECTOR_MONITOR_WORK i
      WHERE i.TASK_ID = pTaskId AND i.AGG_ROW = 0
      GROUP BY i.RegionCode;

      -- Создание агрегирующих строк (все регионы)
      INSERT INTO REPORT_INSPECTOR_MONITOR_WORK
      SELECT NDS2$REPORTS.F$GET_LOAD_INSP_NEXTVAL
            ,pTaskId
            ,pActualNumActive
            ,1 as AGG_ROW
            ,'*' as FederalCode
            ,'ИТОГО' as FederalDistrict
            ,'*' as RegionCode
            ,'*' as Region
            ,'*' as InspectionCode
            ,'*' as Inspection
            ,'*' as Inspector
            ,SUM(i.NotEliminated_GeneralCount) as NotEliminated_GeneralCount
            ,SUM(i.NotEliminated_GeneralSum) as NotEliminated_GeneralSum
            ,SUM(i.NotElimin_CurrencyCount) as NotElimin_CurrencyCount
            ,SUM(i.NotElimin_CurrencySum) as NotElimin_CurrencySum
            ,SUM(i.NotElimin_NDSCount) as NotElimin_NDSCount
            ,SUM(i.NotElimin_NDSSum) as NotElimin_NDSSum
            ,SUM(i.NotElimin_NotExactCount) as NotElimin_NotExactCount
            ,SUM(i.NotElimin_NotExactSum) as NotElimin_NotExactSum
            ,SUM(i.NotElimin_GapCount) as NotElimin_GapCount
            ,SUM(i.NotElimin_GapSum) as NotElimin_GapSum
            ,SUM(i.IdentNotElimin_GeneralCount) as IdentNotElimin_GeneralCount
            ,SUM(i.IdentNotElimin_GeneralSum) as IdentNotElimin_GeneralSum
            ,SUM(i.IdentNotElimin_CurrencyCount) as IdentNotElimin_CurrencyCount
            ,SUM(i.IdentNotElimin_CurrencySum) as IdentNotElimin_CurrencySum
            ,SUM(i.IdentNotElimin_NDSCount) as IdentNotElimin_NDSCount
            ,SUM(i.IdentNotElimin_NDSSum) as IdentNotElimin_NDSSum
            ,SUM(i.IdentNotElimin_NotExactCount) as IdentNotElimin_NotExactCount
            ,SUM(i.IdentNotElimin_NotExactSum) as IdentNotElimin_NotExactSum
            ,SUM(i.IdentNotElimin_GapCount) as IdentNotElimin_GapCount
            ,SUM(i.IdentNotElimin_GapSum) as IdentNotElimin_GapSum
            ,SUM(i.IdentElimin_GeneralCount) as IdentElimin_GeneralCount
            ,SUM(i.IdentElimin_GeneralSum) as IdentElimin_GeneralSum
            ,SUM(i.IdentElimin_CurrencyCount) as IdentElimin_CurrencyCount
            ,SUM(i.IdentElimin_CurrencySum) as IdentElimin_CurrencySum
            ,SUM(i.IdentElimin_NDSCount) as IdentElimin_NDSCount
            ,SUM(i.IdentElimin_NDSSum) as IdentElimin_NDSSum
            ,SUM(i.IdentElimin_NotExactCount) as IdentElimin_NotExactCount
            ,SUM(i.IdentElimin_NotExactSum) as IdentElimin_NotExactSum
            ,SUM(i.IdentElimin_GapCount) as IdentElimin_GapCount
            ,SUM(i.IdentElimin_GapSum) as IdentElimin_GapSum
      FROM REPORT_INSPECTOR_MONITOR_WORK i
      WHERE i.TASK_ID = pTaskId AND i.AGG_ROW = 0;

      UPDATE REPORT_TASK SET TASK_STATUS = 1, COMPLETE_DATE = SYSDATE WHERE ID = pTaskId;
      commit;
  end;
  end if;
end;

-- Создание отчета Мониторинг обработки налоговых деклараций
procedure P$CREATE_MONITOR_DECLARATION
(
  pTaskId IN NUMBER
)
as
  pTaskStatus NUMBER := -1;
  pSendDate DATE;
  pCloseDate DATE;
  pNalogPeriod NUMBER(3);
  pFiscalYear NUMBER(5);
  pActualNumActive Number(1);
begin
  pActualNumActive := 1;

  SELECT task_status into pTaskStatus FROM dual
  left outer join (SELECT task_status FROM REPORT_TASK WHERE ID = pTaskId) r on 1 = 1;

  if pTaskStatus = 0 then
  begin
      SELECT PARAM_VALUE into pSendDate FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'DATE_BEGIN';
      SELECT PARAM_VALUE into pCloseDate FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'DATE_END';
      SELECT PARAM_VALUE into pNalogPeriod FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD';
      SELECT PARAM_VALUE into pFiscalYear FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'FISCAL_YEAR';

      INSERT INTO REPORT_MONITOR_DECLARATION
      select
          F$GET_MONITOR_DECL_NEXTVAL,
          pTaskId,
          pActualNumActive,
          0 as AGG_ROW,
          '01' as FederalCode,
          'Тестовый ФО' as Federalditrict,
          dis.REGION_CODE as RegionCode,
          dis.REGION_CODE || '-' || dis.REGION_NAME as Region,
          dis.SOUN_CODE as InspectionCode,
          dis.SOUN_CODE || '-' || dis.SOUN_NAME as Inspection,
          dis.CORRECTION_NUMBER as Corrections,
          dis.INN as INN,
          dis.KPP as KPP,
          dis.NAME as NAME,
          dis.INSPECTOR as INSPECTOR,

          nvl(r1.totalCount, 0) as r1totalCount,
          nvl(r1.totalSum, 0) as r1totalSum,
          nvl(r1lk.sc, 0) as r1lkError,
          nvl(r1.valuteCount, 0) as r1valuteCount,
          nvl(r1.valuteSum, 0) as r1valuteSum,
          nvl(r1.ndsCount, 0) as r1ndsCount,
          nvl(r1.ndsSum, 0) as r1ndsSum,
          nvl(r1.nsCount, 0) as r1nsCount,
          nvl(r1.nsSum, 0) as r1nsSum,
          nvl(r1.breakCount, 0) as r1breakCount,
          nvl(r1.breakSum, 0) as r1breakSum,

          nvl(r2.totalCount, 0) as r2totalCount,
          nvl(r2.totalSum, 0) as r2totalSum,
          nvl(r2lk.sc, 0) as r2lkError,
          nvl(r2.valuteCount, 0) as r2valuteCount,
          nvl(r2.valuteSum, 0) as r2valuteSum,
          nvl(r2.ndsCount, 0) as r2ndsCount,
          nvl(r2.ndsSum, 0) as r2ndsSum,
          nvl(r2.nsCount, 0) as r2nsCount,
          nvl(r2.nsSum, 0) as r2nsSum,
          nvl(r2.breakCount, 0) as r2breakCount,
          nvl(r2.breakSum, 0) as r2breakSum,

          nvl(r3.totalCount, 0) as r3totalCount,
          nvl(r3.totalSum, 0) as r3totalSum,
          nvl(r3.valuteCount, 0) as r3valuteCount,
          nvl(r3.valuteSum, 0) as r3valuteSum,
          nvl(r3.ndsCount, 0) as r3ndsCount,
          nvl(r3.ndsSum, 0) as r3ndsSum,
          nvl(r3.nsCount, 0) as r3nsCount,
          nvl(r3.nsSum, 0) as r3nsSum,
          nvl(r3.breakCount, 0) as r3breakCount,
          nvl(r3.breakSum, 0) as r3breakSum
        from V$DECLARATION dis
        left join (
          select
          distinct
            declaration_id,
            count(*) over (partition by declaration_id) as totalCount,
            sum(amount) over (partition by declaration_id) as totalSum,
            count(case when typecode=3 then typecode end) over(partition by declaration_id) as valuteCount,
            sum(case when typecode=3 then amount else 0 end) over(partition by declaration_id) as valuteSum,
            count(case when typecode=4 then typecode end) over(partition by declaration_id) as ndsCount,
            sum(case when typecode=4 then amount else 0 end) over(partition by declaration_id) as ndsSum,
            count(case when typecode=2 then typecode end) over(partition by declaration_id) as nsCount,
            sum(case when typecode=2 then amount else 0 end) over(partition by declaration_id) as nsSum,
            count(case when typecode=1 then typecode end) over(partition by declaration_id) as breakCount,
            sum(case when typecode=1 then amount else 0 end) over(partition by declaration_id) as breakSum
          from V$DISCREPANCY
          where
            (trunc(FoundDate) <= trunc(pSendDate))
            and
            (
                (CLOSE_DATE > trunc(pSendDate))
                or
                (CLOSE_DATE is null)
            )
        ) r1 on r1.declaration_id = dis.id
        left join (
          select
          distinct
            declaration_id,
            count(*) over (partition by declaration_id) as totalCount,
            sum(amount) over (partition by declaration_id) as totalSum,
            count(case when typecode=3 then typecode end) over(partition by declaration_id) as valuteCount,
            sum(case when typecode=3 then amount else 0 end) over(partition by declaration_id) as valuteSum,
            count(case when typecode=4 then typecode end) over(partition by declaration_id) as ndsCount,
            sum(case when typecode=4 then amount else 0 end) over(partition by declaration_id) as ndsSum,
            count(case when typecode=2 then typecode end) over(partition by declaration_id) as nsCount,
            sum(case when typecode=2 then amount else 0 end) over(partition by declaration_id) as nsSum,
            count(case when typecode=1 then typecode end) over(partition by declaration_id) as breakCount,
            sum(case when typecode=1 then amount else 0 end) over(partition by declaration_id) as breakSum
          from V$DISCREPANCY
          where
            (trunc(FoundDate) <= trunc(pCloseDate))
            and
            (
                (CLOSE_DATE > trunc(pCloseDate))
                or
                (CLOSE_DATE is null)
            )
        ) r2 on r2.declaration_id = dis.id
        left join (
          select
          distinct
            declaration_id,
            count(*) over (partition by declaration_id) as totalCount,
            sum(amount) over (partition by declaration_id) as totalSum,
            count(case when typecode=3 then typecode end) over(partition by declaration_id) as valuteCount,
            sum(case when typecode=3 then amount else 0 end) over(partition by declaration_id) as valuteSum,
            count(case when typecode=4 then typecode end) over(partition by declaration_id) as ndsCount,
            sum(case when typecode=4 then amount else 0 end) over(partition by declaration_id) as ndsSum,
            count(case when typecode=2 then typecode end) over(partition by declaration_id) as nsCount,
            sum(case when typecode=2 then amount else 0 end) over(partition by declaration_id) as nsSum,
            count(case when typecode=1 then typecode end) over(partition by declaration_id) as breakCount,
            sum(case when typecode=1 then amount else 0 end) over(partition by declaration_id) as breakSum
          from V$DISCREPANCY
          where
            STAGE_STATUS_CODE = 10 AND doc_type in (1,5)
            and trunc(CLOSE_DATE) >= trunc(pSendDate)
            and trunc(CLOSE_DATE) <= trunc(pCloseDate)
        ) r3 on r3.declaration_id = dis.id
        left join (
          select id, lk_errors_count as sc
          from V$DECLARATION_HISTORY
          where (trunc(decl_date) < trunc(pSendDate)) and (rownum = 1)
          order by decl_date desc
        ) r1lk on r1lk.id = dis.id
        left join (
          select id, lk_errors_count as sc
          from V$DECLARATION_HISTORY
          where (trunc(decl_date) < trunc(pCloseDate)) and (rownum = 1)
          order by decl_date desc
        ) r2lk on r2lk.id = dis.id
        WHERE dis.SOUN_CODE in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'SOUN_CODE')
          AND dis.REGION_CODE in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'REGION_CODE')
          AND dis.TAX_PERIOD = pNalogPeriod
          AND dis.FISCAL_YEAR = pFiscalYear;

      UPDATE REPORT_TASK SET TASK_STATUS = 1, COMPLETE_DATE = SYSDATE WHERE ID = pTaskId;
      commit;

  end;
  end if;
end;

-- Создание отчета о загруженности инспекций
procedure P$CREATE_LOADING_INSPECTION
(
  pTaskId IN NUMBER
)
as
  pTaskStatus NUMBER := -1;
  pDateBegin DATE;
  pDateEnd DATE;
  pNalogPeriod VARCHAR2(16);
  pFiscalYear NUMBER(5);

  CURSOR reports IS
  SELECT * FROM REPORT_LOADING_INSPECTION WHERE TASK_ID = pTaskId;
  v_reports reports%ROWTYPE;

  pGeneralCount NUMBER(19);
  pGeneralSum NUMBER(19,2);
  pSentToWorkGeneralCount NUMBER(19);
  pSentToWorkGeneralSum NUMBER(19,2);
  pRepairedGeneralCount NUMBER(19);
  pRepairedGeneralSum NUMBER(19,2);
  pUnrepairedGeneralCount NUMBER(19);
  pUnrepairedGeneralSum NUMBER(19,2);
  pActualNumActive Number(1);
begin
  pActualNumActive := 1;

  SELECT task_status into pTaskStatus FROM dual
  left outer join (SELECT task_status FROM REPORT_TASK WHERE ID = pTaskId) r on 1 = 1;

  if pTaskStatus = 0 then
  begin
      DELETE FROM REPORT_LOADING_INSPECTION WHERE TASK_ID = pTaskId;

      -- получение одиночных параметров поиска
      SELECT PARAM_VALUE into pDateBegin FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'DATE_BEGIN';
      SELECT PARAM_VALUE into pDateEnd FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'DATE_END';
      SELECT PARAM_VALUE into pNalogPeriod FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD';
      SELECT PARAM_VALUE into pFiscalYear FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'FISCAL_YEAR';

      -- создание уникальных строк РЕГИОН-ИНСПЕКЦИЯ
      INSERT INTO REPORT_LOADING_INSPECTION (ID, TASK_ID, ACTUAL_NUM, AGG_ROW, FEDERALCODE, FEDERALDISTRICT, REGIONCODE, REGION, INSPECTIONCODE, INSPECTION, INSPECTORCOUNT)
      SELECT
        NDS2$REPORTS.F$GET_LOAD_INSP_NEXTVAL
        ,pTaskId
        ,pActualNumActive
        ,0 as AGG_ROW
        ,'01' as FederalCode
        ,'Тестовый ФО' as FederalDistrict
        ,reg.S_CODE as RegionCode
        ,reg.S_CODE || '-' || reg.S_NAME as Region
        ,s.S_CODE as InspectionCode
        ,s.S_CODE || '-' || s.S_NAME as Inspection
        ,cast(s.Employers_Count as Number(6)) as InspectorCount
      FROM
      (
        SELECT dis.TAXPAYERREGIONCODE, dis.TAXPAYERSOUNCODE
        FROM DOC dc
        JOIN V$DISCREPANCY dis on dc.DOC_ID = dis.CLAIM_ID
        WHERE dis.TaxPayerSounCode in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'SOUN_CODE')
          AND dis.TaxPayerRegionCode in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'REGION_CODE')
          AND dis.TaxPayerPeriodCode = pNalogPeriod
          AND dis.TaxPayerYearCode = pFiscalYear
        GROUP BY dis.TAXPAYERREGIONCODE, dis.TAXPAYERSOUNCODE
      ) d
      LEFT OUTER JOIN V$SSRF reg on reg.S_CODE = d.TAXPAYERREGIONCODE
      LEFT OUTER JOIN V$SONO s on s.S_CODE = d.TAXPAYERSOUNCODE
      WHERE (d.TAXPAYERREGIONCODE is not null) and (d.TAXPAYERSOUNCODE is not null);

      -- Наполнение колонок(кол-во, сумма) всех уникальных строк
      OPEN reports;
      FETCH reports INTO v_reports;
      LOOP
        select nvl(ch01.GeneralCount, 0), nvl(ch01.GeneralSum, 0) into pGeneralCount, pGeneralSum from dual
        left outer join
        (
            SELECT distinct TAXPAYERREGIONCODE, TAXPAYERSOUNCODE
            ,COUNT(DISCREPANCYID) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) GeneralCount
            ,SUM(Amount) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) GeneralSum
            FROM DOC dc
            JOIN V$DISCREPANCY dis on dc.DOC_ID = dis.CLAIM_ID
            WHERE dis.TaxPayerSounCode in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'SOUN_CODE')
            AND dis.TaxPayerRegionCode in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'REGION_CODE')
            AND dis.TaxPayerPeriodCode = pNalogPeriod
            AND dis.TaxPayerYearCode = pFiscalYear
            AND trunc(dis.DECL_DATE) >= trunc(pDateBegin) and trunc(dis.DECL_DATE) <= trunc(pDateEnd)
        ) ch01 on ch01.TAXPAYERREGIONCODE = v_reports.RegionCode and ch01.TAXPAYERSOUNCODE = v_reports.InspectionCode;

        select ch02.GeneralCount, ch02.GeneralSum into pSentToWorkGeneralCount, pSentToWorkGeneralSum from dual
        left outer join
        (
          SELECT distinct TAXPAYERREGIONCODE, TAXPAYERSOUNCODE
          ,COUNT(DISCREPANCYID) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) GeneralCount
          ,SUM(Amount) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) GeneralSum
          FROM DOC dc
          JOIN V$DISCREPANCY dis on dc.DOC_ID = dis.CLAIM_ID
          WHERE dis.TaxPayerSounCode in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'SOUN_CODE')
            AND dis.TaxPayerRegionCode in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'REGION_CODE')
            AND dis.TaxPayerPeriodCode = pNalogPeriod
            AND dis.TaxPayerYearCode = pFiscalYear
			AND trunc(dis.DECL_DATE) >= trunc(pDateBegin) and trunc(dis.DECL_DATE) <= trunc(pDateEnd)
            AND trunc(dc.Create_Date) >= trunc(pDateBegin) and trunc(dc.Create_Date) <= trunc(pDateEnd)
        ) ch02 on ch02.TAXPAYERREGIONCODE = v_reports.RegionCode and ch02.TAXPAYERSOUNCODE = v_reports.InspectionCode;

        select ch03.GeneralCount, ch03.GeneralSum into pRepairedGeneralCount, pRepairedGeneralSum from dual
        left outer join
        (
          SELECT distinct TAXPAYERREGIONCODE, TAXPAYERSOUNCODE
          ,COUNT(DISCREPANCYID) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) GeneralCount
          ,SUM(Amount) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) GeneralSum
          FROM DOC dc
          JOIN V$DISCREPANCY dis on dc.DOC_ID = dis.CLAIM_ID
          WHERE dis.TaxPayerSounCode in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'SOUN_CODE')
            AND dis.TaxPayerRegionCode in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'REGION_CODE')
            AND dis.TaxPayerPeriodCode = pNalogPeriod
            AND dis.TaxPayerYearCode = pFiscalYear
			AND trunc(dis.DECL_DATE) >= trunc(pDateBegin) and trunc(dis.DECL_DATE) <= trunc(pDateEnd)
            AND trunc(dc.Create_Date) >= trunc(pDateBegin) and trunc(dc.Create_Date) <= trunc(pDateEnd)
            AND (dis.STAGE_STATUS_CODE = 10 AND dc.doc_type in (1,5))
            AND trunc(dis.CLOSE_DATE) >= trunc(pDateBegin) AND trunc(dis.CLOSE_DATE) <= trunc(pDateEnd)
        ) ch03 on ch03.TAXPAYERREGIONCODE = v_reports.RegionCode and ch03.TAXPAYERSOUNCODE = v_reports.InspectionCode;

        select ch04.GeneralCount, ch04.GeneralSum into pUnrepairedGeneralCount, pUnrepairedGeneralSum from dual
        left outer join
        (
          SELECT distinct TAXPAYERREGIONCODE, TAXPAYERSOUNCODE
          ,COUNT(DISCREPANCYID) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) GeneralCount
          ,SUM(Amount) OVER (PARTITION BY TAXPAYERREGIONCODE, TAXPAYERSOUNCODE) GeneralSum
          FROM DOC dc
          JOIN V$DISCREPANCY dis on dc.DOC_ID = dis.CLAIM_ID
          WHERE dis.TaxPayerSounCode in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'SOUN_CODE')
            AND dis.TaxPayerRegionCode in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'REGION_CODE')
            AND dis.TaxPayerPeriodCode = pNalogPeriod
            AND dis.TaxPayerYearCode = pFiscalYear
			AND trunc(dis.DECL_DATE) >= trunc(pDateBegin) and trunc(dis.DECL_DATE) <= trunc(pDateEnd)
            AND trunc(dc.Create_Date) >= trunc(pDateBegin) and trunc(dc.Create_Date) <= trunc(pDateEnd)
            AND (trunc(dis.CLOSE_DATE) > trunc(pDateEnd) or dis.CLOSE_DATE is null)
        ) ch04 on ch04.TAXPAYERREGIONCODE = v_reports.RegionCode and ch04.TAXPAYERSOUNCODE = v_reports.InspectionCode;

        UPDATE REPORT_LOADING_INSPECTION r SET GeneralCount = pGeneralCount, GeneralSum = pGeneralSum,
               SentToWorkGeneralCount = pSentToWorkGeneralCount, SentToWorkGeneralSum = pSentToWorkGeneralSum,
               RepairedGeneralCount = pRepairedGeneralCount, RepairedGeneralSum = pRepairedGeneralSum,
               UnrepairedGeneralCount = pUnrepairedGeneralCount, UnrepairedGeneralSum = pUnrepairedGeneralSum
        WHERE r.Region = v_reports.Region and r.Inspection = v_reports.Inspection;

        FETCH reports INTO v_reports;
        EXIT WHEN reports%NOTFOUND;
      END LOOP;

      CLOSE reports;

      -- Создание агрегирующих строк (один регион, все инспекции)
      INSERT INTO REPORT_LOADING_INSPECTION
      SELECT NDS2$REPORTS.F$GET_LOAD_INSP_NEXTVAL
            ,pTaskId
            ,pActualNumActive
            ,1 as AGG_ROW
            ,MIN(i.FederalCode) as FederalCode
            ,MIN(i.FederalDistrict) as FederalDistrict
            ,i.REGIONCODE as RegionCode
            ,MIN(i.Region) as Region
            ,'*' as InspectionCode
            ,'*' as Inspection
            ,SUM(i.InspectorCount) as InspectorCount
            ,SUM(i.GeneralCount) as GeneralCount
            ,SUM(i.GeneralSum) as GeneralSum
            ,SUM(i.SentToWorkGeneralCount) as SentToWorkGeneralCount
            ,SUM(i.SentToWorkGeneralSum) as SentToWorkGeneralSum
            ,null as SentToWorkPercent
            ,null as SentToWorkPercentSum
            ,SUM(i.RepairedGeneralCount) as RepairedGeneralCount
            ,SUM(i.RepairedGeneralSum) as RepairedGeneralSum
            ,null as RepairedPercent
            ,null as RepairedPercentSum
            ,SUM(i.UnrepairedGeneralCount) as UnrepairedGeneralCount
            ,SUM(i.UnrepairedGeneralSum) as UnrepairedGeneralSum
      FROM REPORT_LOADING_INSPECTION i
      WHERE i.TASK_ID = pTaskId AND i.AGG_ROW = 0
      GROUP BY i.REGIONCODE;

      -- Создание агрегирующих строк (один федеральный округ, все регионы, все инспекции)
      INSERT INTO REPORT_LOADING_INSPECTION
      SELECT NDS2$REPORTS.F$GET_LOAD_INSP_NEXTVAL
            ,pTaskId
            ,pActualNumActive
            ,1 as AGG_ROW
            ,i.FEDERALCODE as FederalCode
            ,MIN(i.FederalDistrict) as FederalDistrict
            ,'*' as RegionCode
            ,'*' as Region
            ,'*' as InspectionCode
            ,'*' as Inspection
            ,SUM(i.InspectorCount) as InspectorCount
            ,SUM(i.GeneralCount) as GeneralCount
            ,SUM(i.GeneralSum) as GeneralSum
            ,SUM(i.SentToWorkGeneralCount) as SentToWorkGeneralCount
            ,SUM(i.SentToWorkGeneralSum) as SentToWorkGeneralSum
            ,null as SentToWorkPercent
            ,null as SentToWorkPercentSum
            ,SUM(i.RepairedGeneralCount) as RepairedGeneralCount
            ,SUM(i.RepairedGeneralSum) as RepairedGeneralSum
            ,null as RepairedPercent
            ,null as RepairedPercentSum
            ,SUM(i.UnrepairedGeneralCount) as UnrepairedGeneralCount
            ,SUM(i.UnrepairedGeneralSum) as UnrepairedGeneralSum
      FROM REPORT_LOADING_INSPECTION i
      WHERE i.TASK_ID = pTaskId AND i.AGG_ROW = 0
      GROUP BY i.FEDERALCODE;

      -- Создание агрегирующих строк (ИТОГО, все федеральные округа, все регионы, все инспекции)
      INSERT INTO REPORT_LOADING_INSPECTION
      SELECT NDS2$REPORTS.F$GET_LOAD_INSP_NEXTVAL
            ,pTaskId
            ,pActualNumActive
            ,1 as AGG_ROW
            ,'*' as FederalCode
            ,'ИТОГО' as FederalDistrict
            ,'*' as RegionCode
            ,'*' as Region
            ,'*' as InspectionCode
            ,'*' as Inspection
            ,SUM(i.InspectorCount) as InspectorCount
            ,SUM(i.GeneralCount) as GeneralCount
            ,SUM(i.GeneralSum) as GeneralSum
            ,SUM(i.SentToWorkGeneralCount) as SentToWorkGeneralCount
            ,SUM(i.SentToWorkGeneralSum) as SentToWorkGeneralSum
            ,null as SentToWorkPercent
            ,null as SentToWorkPercentSum
            ,SUM(i.RepairedGeneralCount) as RepairedGeneralCount
            ,SUM(i.RepairedGeneralSum) as RepairedGeneralSum
            ,null as RepairedPercent
            ,null as RepairedPercentSum
            ,SUM(i.UnrepairedGeneralCount) as UnrepairedGeneralCount
            ,SUM(i.UnrepairedGeneralSum) as UnrepairedGeneralSum
      FROM REPORT_LOADING_INSPECTION i
      WHERE i.TASK_ID = pTaskId AND i.AGG_ROW = 0;

      UPDATE REPORT_TASK SET TASK_STATUS = 1, COMPLETE_DATE = SYSDATE WHERE ID = pTaskId;
      commit;
  end;
  end if;
end;

-- Создание отчета по проверкам КС
procedure P$CREATE_CHECK_CONTROL_RATIO
(
  pTaskId IN NUMBER
)
as
  pTaskStatus NUMBER := -1;
  pDateCreateReport DATE;
  pNalogPeriod VARCHAR2(16);
  pFiscalYear NUMBER(5);
  pActualNumActive Number(1);
begin
  pActualNumActive := 1;

  SELECT task_status into pTaskStatus FROM dual
  left outer join (SELECT task_status FROM REPORT_TASK WHERE ID = pTaskId) r on 1 = 1;

  if pTaskStatus = 0 then
  begin
      DELETE FROM REPORT_CHECK_CONTROL_RATIO WHERE TASK_ID = pTaskId;

      -- получение одиночных параметров поиска
      SELECT PARAM_VALUE into pDateCreateReport FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'DATE_CREATE_REPORT';
      SELECT PARAM_VALUE into pNalogPeriod FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD';
      SELECT PARAM_VALUE into pFiscalYear FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'FISCAL_YEAR';

      INSERT INTO REPORT_CHECK_CONTROL_RATIO (ID, TASK_ID, ACTUAL_NUM, AGG_ROW, NUM, DESCRIPTION, CountDiscrepancy)
      SELECT
        NDS2$REPORTS.F$GET_CHECK_CR_NEXTVAL
        ,pTaskId
        ,pActualNumActive
        ,0 as AGG_ROW
        ,m.typecode as num
        ,crt.description as Description
        ,ch.CountKS as CountDiscrepancy
      FROM
      (
        SELECT
            cr.typecode
        FROM V$CONTROL_RATIO cr
        JOIN V$DECLARATION vd on vd.DECLARATION_VERSION_ID = cr.Decl_Version_Id
        WHERE vd.SOUN_CODE in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'SOUN_CODE')
              AND vd.REGION_CODE in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'REGION_CODE')
              AND vd.TAX_PERIOD = pNalogPeriod 
              AND vd.FISCAL_YEAR = pFiscalYear
              AND cr.HASDISCREPANCY = 1
              AND trunc(vd.UPDATE_DATE) <= trunc(pDateCreateReport)
        group by cr.typecode
      ) m
      left outer join
      (
        select
           distinct typecode
           ,count(cr.Id) over(partition by cr.typecode) as CountKS
        FROM V$CONTROL_RATIO cr
        JOIN V$DECLARATION vd on vd.DECLARATION_VERSION_ID = cr.Decl_Version_Id
        WHERE vd.SOUN_CODE in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'SOUN_CODE')
            AND vd.REGION_CODE in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'REGION_CODE')
            AND vd.TAX_PERIOD = pNalogPeriod 
            AND vd.FISCAL_YEAR = pFiscalYear
            AND cr.HASDISCREPANCY = 1
            AND trunc(vd.UPDATE_DATE) <= trunc(pDateCreateReport)
      ) ch on ch.typecode = m.TypeCode
      left outer join CONTROL_RATIO_TYPE crt on crt.code = m.typecode;

      -- Создание агрегирующих строк (ИТОГО)
      INSERT INTO REPORT_CHECK_CONTROL_RATIO
      SELECT NDS2$REPORTS.F$GET_CHECK_CR_NEXTVAL
            ,pTaskId
            ,pActualNumActive
            ,1 as AGG_ROW
            ,'ИТОГО' as num
            ,'' as Description
            ,SUM(i.CountDiscrepancy) as CountDiscrepancy
      FROM REPORT_CHECK_CONTROL_RATIO i
      WHERE i.TASK_ID = pTaskId AND i.AGG_ROW = 0;

      UPDATE REPORT_TASK SET TASK_STATUS = 1, COMPLETE_DATE = SYSDATE WHERE ID = pTaskId;
      commit;
  end;
  end if;
end;

-- Создание отчета по правилам сопоставлений
procedure P$CREATE_MATCHING_RULE
(
  pTaskId IN NUMBER
)
as
  pTaskStatus NUMBER := -1;
  pNalogPeriod VARCHAR2(16);
  pFiscalYear NUMBER(5);

  CURSOR reports IS
  SELECT * FROM REPORT_MATCHING_RULE WHERE TASK_ID = pTaskId;
  v_reports reports%ROWTYPE;

  pActualNumActive Number(1);
  pDisTypeNotExact NUMBER(1);
  pDiscrepancyCount NUMBER(19);
  pDiscrepancyAmount NUMBER(19,2);
  pPercentByCount NUMBER(3);
  pPercentByAmount NUMBER(3);
  
  pDiscCountCloseClaimAll NUMBER(19);
  pDiscCountCloseClaim NUMBER(19);
  pPercentCloseClaimAll NUMBER(3);
  pPercentCloseClaim NUMBER(3);
  
  pDiscCountCloseReclaimAll NUMBER(19);
  pDiscCountCloseReclaim NUMBER(19);
  pPercentCloseReclaimAll NUMBER(3);
  pPercentCloseReclaim NUMBER(3);
  
  pDiscCountCloseMNKAll NUMBER(19);
  pDiscCountCloseMNK NUMBER(19);
  pPercentCloseMNKAll NUMBER(3);
  pPercentCloseMNK NUMBER(3);
  
  pDiscCountCloseAll NUMBER(19);
  pDiscCountClose NUMBER(19);
  pPercentCloseAll NUMBER(3);
  pPercentClose NUMBER(3);
  
  pDiscCountOpenAll NUMBER(19);
  pDiscCountOpen NUMBER(19);
  pPercentOpenAll NUMBER(3);
  pPercentOpen NUMBER(3);
begin
  pActualNumActive := 1;
  pDisTypeNotExact := 2; -- Тип расхождения = Неточное сопоставление

  SELECT task_status into pTaskStatus FROM dual
  left outer join (SELECT task_status FROM REPORT_TASK WHERE ID = pTaskId) r on 1 = 1;

  if pTaskStatus = 0 then
  begin
      DELETE FROM REPORT_MATCHING_RULE WHERE TASK_ID = pTaskId;

      -- получение одиночных параметров поиска
      SELECT PARAM_VALUE into pNalogPeriod FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD';
      SELECT PARAM_VALUE into pFiscalYear FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'FISCAL_YEAR';

      -- создание основных строк
      INSERT INTO REPORT_MATCHING_RULE (ID, TASK_ID, ACTUAL_NUM, AGG_ROW, GroupNumNotExact, RULE_GROUP, RuleNum, DiscrepancyCount, 
             DiscrepancyAmount)
      SELECT NDS2$REPORTS.F$GET_MATCHING_RULE_NEXTVAL
            ,pTaskId
            ,pActualNumActive
            ,0 as AGG_ROW
            ,to_char(m.rule_group) || ' группа'
            ,m.rule_group as RULE_GROUP
            ,'' as RuleNum
            ,a.DiscrepancyCount
            ,a.DiscrepancyAmount
      FROM
      (
        SELECT dis.rule_group
        FROM V$DISCREPANCY dis
        WHERE dis.taxpayersouncode in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'SOUN_CODE')
              AND dis.taxpayerregioncode in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'REGION_CODE')
              AND dis.TaxPayerPeriodCode = pNalogPeriod
              AND dis.TaxPayerYearCode = pFiscalYear
              AND dis.TypeCode = pDisTypeNotExact
        group by dis.rule_group
      ) m
      LEFT OUTER JOIN
      (
          SELECT distinct dis.rule_group
             ,COUNT(dis.id) OVER (PARTITION BY dis.rule_group) DiscrepancyCount
             ,SUM(dis.Amount) OVER (PARTITION BY dis.rule_group) DiscrepancyAmount
          FROM 
          (
             select sd.* 
                    ,d1.soun_code as TaxPayerSounCode 
                    ,d1.region_code as TaxPayerRegionCode
                    ,d1.tax_period as TaxPayerPeriodCode
                    ,d1.FISCAL_YEAR as TaxPayerYearCode
                    ,sd.type as TypeCode
                    ,sd.AMNT as Amount
             from SOV_DISCREPANCY sd
             inner join V$Declaration d1 on d1.id = sd.decl_id
          ) dis
          WHERE dis.taxpayersouncode in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'SOUN_CODE')
                AND dis.taxpayerregioncode in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'REGION_CODE')
                AND dis.TaxPayerPeriodCode = pNalogPeriod
                AND dis.TaxPayerYearCode = pFiscalYear
                AND dis.TypeCode = pDisTypeNotExact
      ) a on a.rule_group = m.rule_group
      order by m.rule_group;

      -- подсчет общего количества расхождений и сумм расхождений
      pDiscrepancyCount := 0;
      pDiscrepancyAmount := 0;
      SELECT s.DiscrepancyCount, s.DiscrepancyAmount into pDiscrepancyCount, pDiscrepancyAmount from dual
      left outer join
      (
           SELECT SUM(i.DiscrepancyCount) as DiscrepancyCount
                  ,SUM(i.DiscrepancyAmount) as DiscrepancyAmount
           FROM REPORT_MATCHING_RULE i
           WHERE i.TASK_ID = pTaskId AND i.AGG_ROW = 0
      ) s on 1 = 1;

      -- подсчет процента закрытых расхождений для АВТОТРЕБОВАНИЯ/АВТОИСТРЕБОВАНИЯ/МНК для ИТОГО
      pDiscCountCloseClaimAll := 0; pDiscCountCloseReclaimAll:=0; pDiscCountCloseMNKAll := 0; pDiscCountCloseAll := 0; pDiscCountOpenAll := 0;
      SELECT s.DiscClaimCount, s.DiscReclaimCount, s.DiscMNKCount, s.DiscCloseCount, s.DiscOpenCount into pDiscCountCloseClaimAll, 
             pDiscCountCloseReclaimAll, pDiscCountCloseMNKAll, pDiscCountCloseAll, pDiscCountOpenAll  from dual
      left outer join
      (
          SELECT 
            -- Этап Расхождения = "Автотребование первой стороне" или "Автотребование второй стороне" и Статус расхождения = "Закрыто" (10)
            COUNT(distinct case when (dis.STAGE_CODE = 2 or dis.STAGE_CODE = 3) and dis.STAGE_STATUS_CODE = 10 then dis.id end) DiscClaimCount
            -- Этап Расхождения = "Автоистребование первой стороне" или "Автоистребование второй стороне" и Статус расхождения = "Закрыто" (10)
            ,COUNT(distinct case when (dis.STAGE_CODE = 4 or dis.STAGE_CODE = 5) and dis.STAGE_STATUS_CODE = 10 then dis.id end) DiscReclaimCount
            -- Этап Расхождения = "Прочие МНК"
            ,COUNT(distinct case when dis.STAGE_CODE = 6 then dis.id end) DiscMNKCount
            -- статус расхождения = "Закрыто"
            ,COUNT(distinct case when dis.StatusCode = 2 then dis.id end) DiscCloseCount
            -- статус расхождения = "Открыто"
            ,COUNT(distinct case when dis.StatusCode = 1 then dis.id end) DiscOpenCount
          FROM (select distinct * from V$DISCREPANCY) dis
          JOIN DOC dc on dc.DOC_ID = dis.CLAIM_ID
          WHERE dis.taxpayersouncode in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'SOUN_CODE')
                AND dis.taxpayerregioncode in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'REGION_CODE')
                AND dis.TaxPayerPeriodCode = pNalogPeriod
                AND dis.TaxPayerYearCode = pFiscalYear
                AND dis.TypeCode = pDisTypeNotExact
      ) s on 1 = 1;
    
      pPercentCloseClaimAll := 0; pPercentCloseReclaimAll := 0; pPercentCloseMNKAll := 0; pPercentCloseAll := 0; pPercentOpenAll := 0;
      if (pDiscrepancyCount > 0) then
        pPercentCloseClaimAll := (pDiscCountCloseClaimAll * 100) / pDiscrepancyCount;
        pPercentCloseReclaimAll := (pDiscCountCloseReclaimAll * 100) / pDiscrepancyCount;
        pPercentCloseMNKAll := (pDiscCountCloseMNKAll * 100) / pDiscrepancyCount;
        pPercentCloseAll := (pDiscCountCloseAll * 100) / pDiscrepancyCount;
        pPercentOpenAll := (pDiscCountOpenAll * 100) / pDiscrepancyCount;
      end if;
      
      -- Наполнение колонок(кол-во, сумма) всех уникальных строк
      OPEN reports;
      FETCH reports INTO v_reports;
      LOOP

        -- подсчет процента кол-ва расхожденрий относительно ИТОГО
        pPercentByCount := 0;    
        if (pDiscrepancyCount > 0) then
          pPercentByCount := (v_reports.discrepancycount * 100) / pDiscrepancyCount;
        end if;
        pPercentByAmount := 0;
        if (pDiscrepancyAmount > 0) then
          pPercentByAmount := (v_reports.discrepancyamount * 100) / pDiscrepancyAmount;
        end if;

        -- подсчет процента закрытых расхождений для каждой группы относительно всего расхождений 
        pDiscCountCloseClaim := 0; pDiscCountCloseReclaim := 0; pDiscCountCloseMNK := 0; pDiscCountClose := 0; pDiscCountOpen := 0;
        SELECT s.DiscClaimCount, s.DiscReclaimCount, s.DiscMNKCount, s.DiscCloseCount, s.DiscOpenCount into pDiscCountCloseClaim, 
               pDiscCountCloseReclaim, pDiscCountCloseMNK, pDiscCountClose, pDiscCountOpen from dual
        left outer join
        (
            SELECT distinct dis.rule_group
              -- Этап Расхождения = "Автотребование первой стороне" или "Автотребование второй стороне" и  Статус расхождения = "Закрыто" (10)
              ,COUNT(distinct case when (dis.STAGE_CODE = 2 or dis.STAGE_CODE = 3) and dis.STAGE_STATUS_CODE = 10 then dis.id end) 
                   OVER (PARTITION BY dis.rule_group) DiscClaimCount
              -- Этап Расхождения = "Автоистребование первой стороне" или "Автоистребование второй стороне" и Статус расхождения = "Закрыто" (10)
              ,COUNT(distinct case when (dis.STAGE_CODE = 4 or dis.STAGE_CODE = 5) and dis.STAGE_STATUS_CODE = 10 then dis.id end) 
                   OVER (PARTITION BY dis.rule_group) DiscReclaimCount      
              -- Этап Расхождения = "Прочие МНК" и Статус расхождения = "Закрыто" (10)
              ,COUNT(distinct case when dis.STAGE_CODE = 6 then dis.id end) OVER (PARTITION BY dis.rule_group) DiscMNKCount
              -- Статус расхождения = "Закрыто"
              ,COUNT(distinct case when dis.StatusCode = 2 then dis.id end) OVER (PARTITION BY dis.rule_group) DiscCloseCount
              -- Статус расхождения = "Открыто"
              ,COUNT(distinct case when dis.StatusCode = 1 then dis.id end) OVER (PARTITION BY dis.rule_group) DiscOpenCount
            FROM (select distinct * from V$DISCREPANCY) dis
            JOIN DOC dc on dc.DOC_ID = dis.CLAIM_ID
            WHERE dis.taxpayersouncode in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'SOUN_CODE')
                  AND dis.taxpayerregioncode in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'REGION_CODE')
                  AND dis.TaxPayerPeriodCode = pNalogPeriod
                  AND dis.TaxPayerYearCode = pFiscalYear
                  AND dis.TypeCode = pDisTypeNotExact
                  AND dis.rule_group = v_reports.RULE_GROUP      
        ) s on 1 = 1;

        pPercentCloseClaim := 0; pPercentCloseReclaim := 0; pPercentCloseMNK := 0; pPercentClose := 0; pPercentOpen := 0;
        if (pDiscrepancyCount > 0) then
          pPercentCloseClaim := (pDiscCountCloseClaim * 100) / pDiscrepancyCount;
          pPercentCloseReclaim := (pDiscCountCloseReclaim * 100) / pDiscrepancyCount;
          pPercentCloseMNK := (pDiscCountCloseMNK * 100) / pDiscrepancyCount;
          pPercentClose := (pDiscCountClose * 100) / pDiscrepancyCount;
          pPercentOpen := (pDiscCountOpen * 100) / pDiscrepancyCount;
        end if;
        
        -- Заполнение колонок
        UPDATE REPORT_MATCHING_RULE r SET PercentByCount = pPercentByCount, PercentByAmount = pPercentByAmount, 
                PercentDiscrepRequirement = pPercentCloseClaim, PercentDiscrepReclaimDoc = pPercentCloseReclaim,
                PercentDiscrepOtherMNK = pPercentCloseMNK, PercentDiscrepRemoveTotal = pPercentClose, 
                PercentDiscrepNotRemoveTotal = pPercentOpen
        WHERE r.RULE_GROUP = v_reports.RULE_GROUP;

        FETCH reports INTO v_reports;
        EXIT WHEN reports%NOTFOUND;
      END LOOP;

      -- Создание агрегирующих строк (ИТОГО)
      INSERT INTO REPORT_MATCHING_RULE (ID, TASK_ID, ACTUAL_NUM, AGG_ROW, GroupNumNotExact, RuleNum, DiscrepancyCount, DiscrepancyAmount,
             PercentByCount, PercentByAmount, PercentDiscrepRequirement, PercentDiscrepReclaimDoc, PercentDiscrepOtherMNK,
             PercentDiscrepRemoveTotal, PercentDiscrepNotRemoveTotal)
      SELECT NDS2$REPORTS.F$GET_MATCHING_RULE_NEXTVAL
            ,pTaskId
            ,pActualNumActive
            ,1 as AGG_ROW
            ,'ИТОГО' as GroupNumNotExact
            ,'' as rulenum
            ,pDiscrepancyCount as CountDiscrepancy
            ,pDiscrepancyAmount as pDiscrepancyAmount
            ,100
            ,100
            ,pPercentCloseClaimAll
            ,pPercentCloseReclaimAll
            ,pPercentCloseMNKAll
            ,pPercentCloseAll
            ,pPercentOpenAll
        FROM DUAL;

      UPDATE REPORT_TASK SET TASK_STATUS = 1, COMPLETE_DATE = SYSDATE WHERE ID = pTaskId;
      commit;
  end;
  end if;
end;



-- Создание отчета по проверкам ЛК
procedure P$CREATE_CHECK_LOGIC_CONTROL
(
  pTaskId IN NUMBER,
  pRequestId IN NUMBER
)
as
  pTaskStatus NUMBER := -1;
  pActualNumActive Number(1);
  pSummaryErrorCount Number;
  pSummaryBadMismatch Number;
  pSummaryBreakMismatch Number;
  pSummaryExactMismatch Number;
  pSummaryInnerInvoice Number;
  pSummaryOuterInvoice Number;
begin
  pActualNumActive := 1;

  SELECT task_status into pTaskStatus FROM dual
  left outer join (SELECT task_status FROM REPORT_TASK WHERE ID = pTaskId) r on 1 = 1;

  if pTaskStatus = 0 then
  begin
      DELETE FROM REPORT_CHECK_LOGIC_CONTROL WHERE TASK_ID = pTaskId;

      -- Подсчет различных агрегирующих сумм 
      SELECT s.SummaryErrorCount, s.SummaryBadMismatch, s.SummaryBreakMismatch, s.SummaryExactMismatch, s.SummaryInnerInvoice, s.SummaryOuterInvoice 
             into pSummaryErrorCount, pSummaryBadMismatch, pSummaryBreakMismatch, pSummaryExactMismatch, pSummaryInnerInvoice, pSummaryOuterInvoice from dual
      left outer join
      (
           SELECT SUM(i.cnt) as SummaryErrorCount
                  ,SUM(i.cnt_bad_mismatch) as SummaryBadMismatch
                  ,SUM(i.cnt_break_mismatch) as SummaryBreakMismatch
                  ,SUM(i.cnt - i.cnt_bad_mismatch - i.cnt_break_mismatch) as SummaryExactMismatch
                  ,SUM(i.cnt_in) as SummaryInnerInvoice
                  ,SUM(i.cnt_out) as SummaryOuterInvoice
           FROM SOV_LK_REPORT i
           WHERE request_id = pRequestId
      ) s on 1 = 1;
     
      INSERT INTO REPORT_CHECK_LOGIC_CONTROL (ID, TASK_ID, ACTUAL_NUM, AGG_ROW, Num, iNum, Name, ErrorCount, ErrorCountPercentage, ErrorFrequencyInvoiceNotExact,
             ErrorFrequencyInvoiceGap, ErrorFrequencyInvoiceExact, ErrorFrequencyInnerInvoice, ErrorFrequencyOuterInvoice)
      select NDS2$REPORTS.F$GET_CHECK_CR_NEXTVAL
             ,pTaskId
             ,pActualNumActive
             ,0 as AGG_ROW
             ,rownum as num
             ,rownum as iNUM
             ,lk.Name_Check as name
             ,cnt as errorcount
             ,((cnt * 100) / pSummaryErrorCount) as ErrorCountPercentage
             ,((cnt_bad_mismatch * 100) / pSummaryErrorCount) as ErrorFrequencyInvoiceNotExact
             ,((cnt_break_mismatch * 100) / pSummaryErrorCount) as ErrorFrequencyInvoiceGap
             ,(((cnt - cnt_bad_mismatch - cnt_break_mismatch) * 100) / pSummaryErrorCount) as ErrorFrequencyInvoiceExact
             ,((cnt_in * 100) / pSummaryErrorCount) as ErrorFrequencyInnerInvoice
             ,((cnt_out * 100) / pSummaryErrorCount) as ErrorFrequencyOuterInvoice
      from SOV_LK_REPORT slr
      left outer join 
      (
           select distinct lem.error_code, lem.name_check from LOGICAL_ERRORS_MAPPING lem 
      ) lk on lk.error_code = slr.error_code 
      WHERE request_id = pRequestId;

      -- Создание агрегирующих строк (ИТОГО)
      INSERT INTO REPORT_CHECK_LOGIC_CONTROL (ID, TASK_ID, ACTUAL_NUM, AGG_ROW, Num, iNum, Name, ErrorCount, ErrorCountPercentage, ErrorFrequencyInvoiceNotExact,
             ErrorFrequencyInvoiceGap, ErrorFrequencyInvoiceExact, ErrorFrequencyInnerInvoice, ErrorFrequencyOuterInvoice)
      SELECT NDS2$REPORTS.F$GET_CHECK_CR_NEXTVAL
            ,pTaskId
            ,pActualNumActive
            ,1 as AGG_ROW
            ,'ИТОГО' as num
            ,0 as iNUM
            ,'' as Name
            ,SUM(i.ErrorCount) as ErrorCount
            ,100 as ErrorCountPercentage
            ,((pSummaryBadMismatch * 100) / pSummaryErrorCount) as ErrorFrequencyInvoiceNotExact
            ,((pSummaryBreakMismatch * 100) / pSummaryErrorCount) as ErrorFrequencyInvoiceGap
            ,((pSummaryExactMismatch * 100) / pSummaryErrorCount) as ErrorFrequencyInvoiceExact
            ,((pSummaryInnerInvoice * 100) / pSummaryErrorCount) as ErrorFrequencyInnerInvoice
            ,((pSummaryOuterInvoice * 100) / pSummaryErrorCount) as ErrorFrequencyOuterInvoice
      FROM REPORT_CHECK_LOGIC_CONTROL i
      WHERE i.TASK_ID = pTaskId AND i.AGG_ROW = 0;

      UPDATE REPORT_TASK SET TASK_STATUS = 1, COMPLETE_DATE = SYSDATE WHERE ID = pTaskId;
      commit;
  end;
  end if;
end;

procedure P$REPORT_LK_REQUEST_STATUS (
  pRequestId in SOV_LK_REPORT_REQUEST.REQUEST_ID%type,
  pData out SYS_REFCURSOR
  )
as
begin

  open pData for select dict.* from REQUEST_STATUS dict join SOV_LK_REPORT_REQUEST req on req.status_id = dict.ID where req.request_id = pRequestId;

end;


-- Расчет статистических данных для отчета Фоновые показатели (По поданным декларациям)
procedure P$CALCULATE_DECLARATION_RAW
(
   pSubmitDate IN DATE
)
as
begin

  delete from REPORT_DECLARATION_RAW where trunc(Submit_Date) = trunc(pSubmitDate);

  insert into REPORT_DECLARATION_RAW (ID, Submit_Date, Region_Code, Soun_Code, Fiscal_Year, Tax_period, Decl_Count_Total,
         Decl_Count_Null, Decl_Count_Compensation, Decl_Count_Payment, Decl_Count_Not_Submit,
         Decl_Count_Revised_Total, TaxBase_Sum_Compensation, TaxBase_Sum_Payment, NdsCalculated_Sum_Compensation,
         NdsCalculated_Sum_Payment, NdsDeduction_Sum_Compensation, NdsDeduction_Sum_Payment,
         Nds_Sum_Compensation, Nds_Sum_Payment)
  with t as 
  (
      select
      sed.nds2_id as ID
      ,sed.decl_reg_num as SEOD_DECL_ID
      ,SD.ID as ASK_DECL_ID
      ,SD.ZIP as DECLARATION_VERSION_ID
      ,decode((row_number() over (partition by sed.TAX_PERIOD, sed.FISCAL_YEAR, sed.INN order by SED.CORRECTION_NUMBER desc)),1,1,0) as IS_ACTIVE
      ,substr(sed.sono_code,1,2) as REGION_CODE
      ,sed.sono_code as SOUN_CODE
      ,sed.TAX_PERIOD
      ,sed.FISCAL_YEAR
      ,nvl(sd.DATADOK, sed.EOD_DATE) as DECL_DATE
      ,DECODE(sed.TYPE, 0, 'Декларация', 1, 'Журнал', '') as DECL_TYPE
      ,sed.TYPE as DECL_TYPE_CODE
      ,case
          when sd.SUMPU173_5 > 0 then 'К уплате'
            when sd.SUMPU173_1 > 0 then 'К уплате'
            when sd.SUMPU173_1 < 0 then 'К возмещению'
            else 'Не определено'
      end as DECL_SIGN
      ,sed.CORRECTION_NUMBER
      ,case
            when sd.SUMPU173_5 > 0 then sd.SUMPU173_5
            when sd.SUMPU173_1 > 0 then sd.SUMPU173_1
            when sd.SUMPU173_1 < 0 then sd.SUMPU173_1
            else null end as COMPENSATION_AMNT
      ,(nvl(sd.REALTOV18NALBAZA, 0) + nvl(sd.REALTOV10NALBAZA, 0) + nvl(sd.REALTOV118NALBAZA, 0) + nvl(sd.REALTOV110NALBAZA, 0) + 
      nvl(sd.REALPREDIKNALBAZA, 0) + nvl(sd.VYPSMRSOBNALBAZA, 0) + nvl(sd.OPLPREDPOSTNALBAZA, 0) + nvl(sd.KORREALTOV18NALBAZA, 0) + 
      nvl(sd.KORREALTOV10NALBAZA, 0) + nvl(sd.KORREALTOV118NALBAZA, 0) + nvl(sd.KORREALTOV110NALBAZA, 0) + 
      nvl(sd.KORREALPREDIKNALBAZA, 0)) as TAX_BASE_AMNT
      ,sd.NALVYCHOBSHH as TAX_DEDUCTION_AMNT
      ,sd.NALVOSSTOBSHH as TAX_CALCULATED_AMNT
      from seod_declaration sed
      left join V$ASKDEKL sd on SD.PERIOD = SED.TAX_PERIOD and SD.OTCHETGOD = SED.FISCAL_YEAR and SD.INNNP = SED.INN and SD.NOMKORR = SED.CORRECTION_NUMBER
  )
  select
         m.ID
        ,m.Submit_Date
        ,m.Region_Code
        ,m.Soun_Code
        ,m.Fiscal_Year
        ,m.Tax_period
        ,m.Decl_Count_Total
        ,m.Decl_Count_Null
        ,m.Decl_Count_Compensation
        ,m.Decl_Count_Payment
        ,m.Decl_Count_Not_Submit
        ,cor.countTotal as Decl_Count_Revised_Total --Подано уточненных деклараций
        ,m.TaxBase_Sum_Compensation
        ,m.TaxBase_Sum_Payment
        ,m.NdsCalculated_Sum_Compensation
        ,m.NdsCalculated_Sum_Payment
        ,m.NdsDeduction_Sum_Compensation
        ,m.NdsDeduction_Sum_Payment
        ,m.Nds_Sum_Compensation
        ,m.Nds_Sum_Payment
  from
  (
    select
      F$GET_DECLARATION_RAW_NEXTVAL as ID
      ,pSubmitDate as Submit_Date
      ,vd.REGION_CODE as Region_Code
      ,vd.SOUN_CODE as Soun_Code
      ,vd.FISCAL_YEAR as Fiscal_Year
      ,vd.TAX_PERIOD as Tax_period
      --Подано деклараций к уплате
      ,count(case when decl_sign = 'К уплате' then vd.ID end) as Decl_Count_Payment
      --Подано деклараций к возмещению
      ,count(case when decl_sign = 'К возмещению' then vd.ID end) as Decl_Count_Compensation
      --Подано нулевых деклараций
      ,count(case when decl_sign = 'Не определено' then vd.ID end) as Decl_Count_Null
      --Не подано деклараций НП по которым есть упоминания в декларациях контрагентов(Нулевая декларация)
      ,0 as Decl_Count_Not_Submit
      --Всего подано деклараций
      ,count(vd.ID) as Decl_Count_Total
      --Налоговая база в декларациях к уплате
      ,sum(case when decl_sign = 'К уплате' then nvl(TAX_BASE_AMNT, 0) else 0 end) as TaxBase_Sum_Payment
      --Налоговая база в декларациях к возмещению
      ,sum(case when decl_sign = 'К возмещению' then nvl(TAX_BASE_AMNT, 0) else 0 end) as TaxBase_Sum_Compensation
      --Сумма исчисленного НДС в декларациях к уплате
      ,sum(case when decl_sign = 'К уплате' then nvl(TAX_CALCULATED_AMNT, 0) else 0 end) as NdsCalculated_Sum_Payment
      --Сумма исчисленного НДС в декларациях к возмещению
      ,sum(case when decl_sign = 'К возмещению' then nvl(TAX_CALCULATED_AMNT, 0) else 0 end) as NdsCalculated_Sum_Compensation
      --Сумма вычетов по НДС в декларациях к уплате
      ,sum(case when decl_sign = 'К уплате' then nvl(TAX_DEDUCTION_AMNT, 0) else 0 end) as NdsDeduction_Sum_Payment
      --Сумма вычетов по НДС в декларациях к возмещению
      ,sum(case when decl_sign = 'К возмещению' then nvl(TAX_DEDUCTION_AMNT, 0) else 0 end) as NdsDeduction_Sum_Compensation
      --Сумма НДС к уплате
      ,sum(case when decl_sign = 'К уплате' then nvl(COMPENSATION_AMNT, 0) else 0 end) as Nds_Sum_Payment
      --Сумма НДС к возмещению
      ,sum(case when decl_sign = 'К возмещению' then nvl(COMPENSATION_AMNT, 0) else 0 end) as Nds_Sum_Compensation
    from t vd
    where vd.IS_ACTIVE = 1 and trunc(vd.DECL_DATE) <= trunc(pSubmitDate)
    group by vd.REGION_CODE, vd.SOUN_CODE, vd.FISCAL_YEAR, vd.TAX_PERIOD
  ) m
  left outer join
  (
    --Подано уточненных деклараций
    select count(*) as countTotal
      ,vd.REGION_CODE as Region_Code
      ,vd.SOUN_CODE as Soun_Code
      ,vd.FISCAL_YEAR as Fiscal_Year
      ,vd.TAX_PERIOD as Tax_period
    from t vd
    where trunc(vd.DECL_DATE) <= trunc(pSubmitDate)
    group by vd.REGION_CODE, vd.SOUN_CODE, vd.FISCAL_YEAR, vd.TAX_PERIOD
  ) cor on cor.REGION_CODE = m.REGION_CODE and cor.SOUN_CODE = m.SOUN_CODE
       and cor.FISCAL_YEAR = m.FISCAL_YEAR and cor.TAX_PERIOD = m.TAX_PERIOD
  order by m.REGION_CODE, m.SOUN_CODE, m.FISCAL_YEAR, m.TAX_PERIOD;

  commit;

end;

-- Создание отчета Фоновые показатели (По поданным декларациям)
procedure P$CREATE_DECLARATION
(
  pTaskId IN NUMBER
)
as
  pTaskStatus NUMBER := -1;
  pActualNumActive Number(1);
  pDeclStatisticType NUMBER(1);
  pDeclStatisticRegim NUMBER(1);
  pDateCurrent DATE;
  pDateBegin DATE;
  pDateEnd DATE;
  pFiscalYear NUMBER(5);
begin
  pActualNumActive := 1;

  SELECT task_status into pTaskStatus FROM dual
  left outer join (SELECT task_status FROM REPORT_TASK WHERE ID = pTaskId) r on 1 = 1;

  if pTaskStatus = 0 then
  begin
      DELETE FROM REPORT_DECLARATION WHERE TASK_ID = pTaskId;

      SELECT PARAM_VALUE into pFiscalYear FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'FISCAL_YEAR';

      SELECT PARAM_VALUE into pDeclStatisticType FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'DECL_STATISTIC_TYPE';
      SELECT PARAM_VALUE into pDeclStatisticRegim FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'DECL_STATISTIC_REGIM';
      SELECT PARAM_VALUE into pDateCurrent FROM dual
         left outer join (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'DATE_CREATE_REPORT') r on 1 = 1;
      SELECT PARAM_VALUE into pDateBegin FROM dual
         left outer join (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'DATE_BEGIN') r on 1 = 1;
      SELECT PARAM_VALUE into pDateEnd FROM dual
         left outer join (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'DATE_END') r on 1 = 1;

      --На конкретную дату
      if pDeclStatisticRegim = 1 then
      begin
        --По стране
        if pDeclStatisticType = 1 then
          insert into REPORT_DECLARATION (ID, TASK_ID, ACTUAL_NUM, AGG_ROW, Name_Group,Decl_Count_Total, Decl_Count_Null,
                Decl_Count_Compensation, Decl_Count_Payment, Decl_Count_Not_Submit, Decl_Count_Revised_Total, TaxBase_Sum_Compensation,
                TaxBase_Sum_Payment, NdsCalculated_Sum_Compensation, NdsCalculated_Sum_Payment, NdsDeduction_Sum_Compensation,
                NdsDeduction_Sum_Payment, Nds_Sum_Compensation, Nds_Sum_Payment)
          select NDS2$REPORTS.F$GET_DECLARATION_NEXTVAL
            ,pTaskId
            ,pActualNumActive
            ,0 as AGG_ROW
            ,'вся страна' as name_group
            ,sum(decl_count_total) as decl_count_total
            ,sum(decl_count_null) as decl_count_null
            ,sum(decl_count_compensation) as decl_count_compensation
            ,sum(decl_count_payment) as decl_count_payment
            ,sum(decl_count_not_submit) as decl_count_not_submit
            ,sum(decl_count_revised_total) as decl_count_revised_total
            ,sum(taxbase_sum_compensation) as taxbase_sum_compensation
            ,sum(taxbase_sum_payment) as taxbase_sum_payment
            ,sum(ndscalculated_sum_compensation) as ndscalculated_sum_compensation
            ,sum(ndscalculated_sum_payment) as ndscalculated_sum_payment
            ,sum(ndsdeduction_sum_compensation) as ndsdeduction_sum_compensation
            ,sum(ndsdeduction_sum_payment) as ndsdeduction_sum_payment
            ,sum(nds_sum_compensation) as nds_sum_compensation
            ,sum(nds_sum_payment) as nds_sum_payment
          from REPORT_DECLARATION_RAW r
          where trunc(r.submit_date) = trunc(pDateCurrent) and r.fiscal_year = pFiscalYear
                and r.tax_period in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD');
        end if;
        --По федеральным округам
        if pDeclStatisticType = 2 then
          insert into REPORT_DECLARATION (ID, TASK_ID, ACTUAL_NUM, AGG_ROW, Name_Group,Decl_Count_Total, Decl_Count_Null,
                Decl_Count_Compensation, Decl_Count_Payment, Decl_Count_Not_Submit, Decl_Count_Revised_Total, TaxBase_Sum_Compensation,
                TaxBase_Sum_Payment, NdsCalculated_Sum_Compensation, NdsCalculated_Sum_Payment, NdsDeduction_Sum_Compensation,
                NdsDeduction_Sum_Payment, Nds_Sum_Compensation, Nds_Sum_Payment)
          select NDS2$REPORTS.F$GET_DECLARATION_NEXTVAL
                 ,pTaskId
                 ,pActualNumActive
                 ,0 as AGG_ROW
                 ,fd.description as name_group
                 ,a.decl_count_total
                 ,a.decl_count_null
                 ,a.decl_count_compensation
                 ,a.decl_count_payment
                 ,a.decl_count_not_submit
                 ,a.decl_count_revised_total
                 ,a.taxbase_sum_compensation
                 ,a.taxbase_sum_payment
                 ,a.ndscalculated_sum_compensation
                 ,a.ndscalculated_sum_payment
                 ,a.ndsdeduction_sum_compensation
                 ,a.ndsdeduction_sum_payment
                 ,a.nds_sum_compensation
                 ,a.nds_sum_payment
          from
          (
            select fdr.district_id
            from REPORT_DECLARATION_RAW r
            join FEDERAL_DISTRICT_REGION fdr on fdr.region_code = r.region_code
            where trunc(r.submit_date) = trunc(pDateCurrent) and r.fiscal_year = pFiscalYear
                  and r.tax_period in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD')
            group by fdr.district_id
          ) m
          left outer join
          (
               select distinct r.district_id
                     ,SUM(r.decl_count_total) OVER (PARTITION BY r.district_id) decl_count_total
                     ,SUM(r.decl_count_null) OVER (PARTITION BY r.district_id) decl_count_null
                     ,SUM(r.decl_count_compensation) OVER (PARTITION BY r.district_id) decl_count_compensation
                     ,SUM(r.decl_count_payment) OVER (PARTITION BY r.district_id) decl_count_payment
                     ,SUM(r.decl_count_not_submit) OVER (PARTITION BY r.district_id) decl_count_not_submit
                     ,SUM(r.decl_count_revised_total) OVER (PARTITION BY r.district_id) decl_count_revised_total
                     ,SUM(r.taxbase_sum_compensation) OVER (PARTITION BY r.district_id) taxbase_sum_compensation
                     ,SUM(r.taxbase_sum_payment) OVER (PARTITION BY r.district_id) taxbase_sum_payment
                     ,SUM(r.ndscalculated_sum_compensation) OVER (PARTITION BY r.district_id) ndscalculated_sum_compensation
                     ,SUM(r.ndscalculated_sum_payment) OVER (PARTITION BY r.district_id) ndscalculated_sum_payment
                     ,SUM(r.ndsdeduction_sum_compensation) OVER (PARTITION BY r.district_id) ndsdeduction_sum_compensation
                     ,SUM(r.ndsdeduction_sum_payment) OVER (PARTITION BY r.district_id) ndsdeduction_sum_payment
                     ,SUM(r.nds_sum_compensation) OVER (PARTITION BY r.district_id) nds_sum_compensation
                     ,SUM(r.nds_sum_payment) OVER (PARTITION BY r.district_id) nds_sum_payment
               from
               (
                  select r.*, fd.district_id as district_id
                  from REPORT_DECLARATION_RAW r
                  join FEDERAL_DISTRICT_REGION fdr on fdr.region_code = r.region_code
                  join FEDERAL_DISTRICT fd on fd.district_id = fdr.district_id
                  where trunc(r.submit_date) = trunc(pDateCurrent) and r.fiscal_year = pFiscalYear
                        and r.tax_period in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD')
               )r
          ) a on a.district_id = m.district_id
          join FEDERAL_DISTRICT fd on fd.district_id = m.district_id;
        end if;
        --По регионам
        if pDeclStatisticType = 3 then
          insert into REPORT_DECLARATION (ID, TASK_ID, ACTUAL_NUM, AGG_ROW, Name_Group,Decl_Count_Total, Decl_Count_Null,
                Decl_Count_Compensation, Decl_Count_Payment, Decl_Count_Not_Submit, Decl_Count_Revised_Total, TaxBase_Sum_Compensation,
                TaxBase_Sum_Payment, NdsCalculated_Sum_Compensation, NdsCalculated_Sum_Payment, NdsDeduction_Sum_Compensation,
                NdsDeduction_Sum_Payment, Nds_Sum_Compensation, Nds_Sum_Payment)
          select NDS2$REPORTS.F$GET_DECLARATION_NEXTVAL
                 ,pTaskId
                 ,pActualNumActive
                 ,0 as AGG_ROW
                 ,s.s_code || '-' || s.s_name as name_group
                 ,a.decl_count_total
                 ,a.decl_count_null
                 ,a.decl_count_compensation
                 ,a.decl_count_payment
                 ,a.decl_count_not_submit
                 ,a.decl_count_revised_total
                 ,a.taxbase_sum_compensation
                 ,a.taxbase_sum_payment
                 ,a.ndscalculated_sum_compensation
                 ,a.ndscalculated_sum_payment
                 ,a.ndsdeduction_sum_compensation
                 ,a.ndsdeduction_sum_payment
                 ,a.nds_sum_compensation
                 ,a.nds_sum_payment
          from
          (
            select r.region_code
            from REPORT_DECLARATION_RAW r
            where trunc(r.submit_date) = trunc(pDateCurrent) and r.fiscal_year = pFiscalYear
                  and r.tax_period in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD')
            group by r.region_code
          ) m
          left outer join
          (
               select distinct r.region_code
                     ,SUM(r.decl_count_total) OVER (PARTITION BY r.region_code) decl_count_total
                     ,SUM(r.decl_count_null) OVER (PARTITION BY r.region_code) decl_count_null
                     ,SUM(r.decl_count_compensation) OVER (PARTITION BY r.region_code) decl_count_compensation
                     ,SUM(r.decl_count_payment) OVER (PARTITION BY r.region_code) decl_count_payment
                     ,SUM(r.decl_count_not_submit) OVER (PARTITION BY r.region_code) decl_count_not_submit
                     ,SUM(r.decl_count_revised_total) OVER (PARTITION BY r.region_code) decl_count_revised_total
                     ,SUM(r.taxbase_sum_compensation) OVER (PARTITION BY r.region_code) taxbase_sum_compensation
                     ,SUM(r.taxbase_sum_payment) OVER (PARTITION BY r.region_code) taxbase_sum_payment
                     ,SUM(r.ndscalculated_sum_compensation) OVER (PARTITION BY r.region_code) ndscalculated_sum_compensation
                     ,SUM(r.ndscalculated_sum_payment) OVER (PARTITION BY r.region_code) ndscalculated_sum_payment
                     ,SUM(r.ndsdeduction_sum_compensation) OVER (PARTITION BY r.region_code) ndsdeduction_sum_compensation
                     ,SUM(r.ndsdeduction_sum_payment) OVER (PARTITION BY r.region_code) ndsdeduction_sum_payment
                     ,SUM(r.nds_sum_compensation) OVER (PARTITION BY r.region_code) nds_sum_compensation
                     ,SUM(r.nds_sum_payment) OVER (PARTITION BY r.region_code) nds_sum_payment
               from REPORT_DECLARATION_RAW r
               where trunc(r.submit_date) = trunc(pDateCurrent) and r.fiscal_year = pFiscalYear
                     and r.tax_period in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD')
          ) a on a.region_code = m.region_code
          join V$SSRF s on s.s_code = m.region_code;
        end if;
        --По инспекциям
        if pDeclStatisticType = 4 then
          insert into REPORT_DECLARATION (ID, TASK_ID, ACTUAL_NUM, AGG_ROW, Name_Group,Decl_Count_Total, Decl_Count_Null,
                Decl_Count_Compensation, Decl_Count_Payment, Decl_Count_Not_Submit, Decl_Count_Revised_Total, TaxBase_Sum_Compensation,
                TaxBase_Sum_Payment, NdsCalculated_Sum_Compensation, NdsCalculated_Sum_Payment, NdsDeduction_Sum_Compensation,
                NdsDeduction_Sum_Payment, Nds_Sum_Compensation, Nds_Sum_Payment)
          select NDS2$REPORTS.F$GET_DECLARATION_NEXTVAL
                 ,pTaskId
                 ,pActualNumActive
                 ,0 as AGG_ROW
                 ,s.s_code || '-' || s.s_name
                 ,a.decl_count_total
                 ,a.decl_count_null
                 ,a.decl_count_compensation
                 ,a.decl_count_payment
                 ,a.decl_count_not_submit
                 ,a.decl_count_revised_total
                 ,a.taxbase_sum_compensation
                 ,a.taxbase_sum_payment
                 ,a.ndscalculated_sum_compensation
                 ,a.ndscalculated_sum_payment
                 ,a.ndsdeduction_sum_compensation
                 ,a.ndsdeduction_sum_payment
                 ,a.nds_sum_compensation
                 ,a.nds_sum_payment
          from
          (
            select r.soun_code
            from REPORT_DECLARATION_RAW r
            where r.region_code in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'REGION_CODE')
                  and trunc(r.submit_date) = trunc(pDateCurrent) and r.fiscal_year = pFiscalYear
                  and r.tax_period in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD')
            group by r.soun_code
          ) m
          left outer join
          (
               select distinct r.soun_code
                     ,SUM(r.decl_count_total) OVER (PARTITION BY r.soun_code) decl_count_total
                     ,SUM(r.decl_count_null) OVER (PARTITION BY r.soun_code) decl_count_null
                     ,SUM(r.decl_count_compensation) OVER (PARTITION BY r.soun_code) decl_count_compensation
                     ,SUM(r.decl_count_payment) OVER (PARTITION BY r.soun_code) decl_count_payment
                     ,SUM(r.decl_count_not_submit) OVER (PARTITION BY r.soun_code) decl_count_not_submit
                     ,SUM(r.decl_count_revised_total) OVER (PARTITION BY r.soun_code) decl_count_revised_total
                     ,SUM(r.taxbase_sum_compensation) OVER (PARTITION BY r.soun_code) taxbase_sum_compensation
                     ,SUM(r.taxbase_sum_payment) OVER (PARTITION BY r.soun_code) taxbase_sum_payment
                     ,SUM(r.ndscalculated_sum_compensation) OVER (PARTITION BY r.soun_code) ndscalculated_sum_compensation
                     ,SUM(r.ndscalculated_sum_payment) OVER (PARTITION BY r.soun_code) ndscalculated_sum_payment
                     ,SUM(r.ndsdeduction_sum_compensation) OVER (PARTITION BY r.soun_code) ndsdeduction_sum_compensation
                     ,SUM(r.ndsdeduction_sum_payment) OVER (PARTITION BY r.soun_code) ndsdeduction_sum_payment
                     ,SUM(r.nds_sum_compensation) OVER (PARTITION BY r.soun_code) nds_sum_compensation
                     ,SUM(r.nds_sum_payment) OVER (PARTITION BY r.soun_code) nds_sum_payment
               from REPORT_DECLARATION_RAW r
               where r.region_code in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'REGION_CODE')
                     and trunc(r.submit_date) = trunc(pDateCurrent) and r.fiscal_year = pFiscalYear
                     and r.tax_period in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD')
          ) a on a.soun_code = m.soun_code
          join V$SONO s on s.s_code = m.soun_code;
        end if;
      end;
      end if;

      --Разница между двумя датами
      if pDeclStatisticRegim = 2 then
      begin
          --По стране
          if pDeclStatisticType = 1 then
            insert into REPORT_DECLARATION (ID, TASK_ID, ACTUAL_NUM, AGG_ROW, Name_Group,Decl_Count_Total, Decl_Count_Null,
                  Decl_Count_Compensation, Decl_Count_Payment, Decl_Count_Not_Submit, Decl_Count_Revised_Total, TaxBase_Sum_Compensation,
                  TaxBase_Sum_Payment, NdsCalculated_Sum_Compensation, NdsCalculated_Sum_Payment, NdsDeduction_Sum_Compensation,
                  NdsDeduction_Sum_Payment, Nds_Sum_Compensation, Nds_Sum_Payment)
            select NDS2$REPORTS.F$GET_DECLARATION_NEXTVAL
              ,pTaskId
              ,pActualNumActive
              ,0 as AGG_ROW
              ,'вся страна' as name_group
              ,nvl(b.decl_count_total, 0) - nvl(a.decl_count_total, 0) as decl_count_total
              ,nvl(b.decl_count_null, 0) - nvl(a.decl_count_null, 0) as decl_count_null
              ,nvl(b.decl_count_compensation, 0) - nvl(a.decl_count_compensation, 0) as decl_count_compensation
              ,nvl(b.decl_count_payment, 0) - nvl(a.decl_count_payment, 0) as decl_count_payment
              ,nvl(b.decl_count_not_submit, 0) - nvl(a.decl_count_not_submit, 0) as decl_count_not_submit
              ,nvl(b.decl_count_revised_total, 0) - nvl(a.decl_count_revised_total, 0) as decl_count_revised_total
              ,nvl(b.taxbase_sum_compensation, 0) - nvl(a.taxbase_sum_compensation, 0) as taxbase_sum_compensation
              ,nvl(b.taxbase_sum_payment, 0) - nvl(a.taxbase_sum_payment, 0) as taxbase_sum_payment
              ,nvl(b.ndscalculated_sum_compensation, 0) - nvl(a.ndscalculated_sum_compensation, 0) as ndscalculated_sum_compensation
              ,nvl(b.ndscalculated_sum_payment, 0) - nvl(a.ndscalculated_sum_payment, 0) as ndscalculated_sum_payment
              ,nvl(b.ndsdeduction_sum_compensation, 0) - nvl(a.ndsdeduction_sum_compensation, 0) as ndsdeduction_sum_compensation
              ,nvl(b.ndsdeduction_sum_payment, 0) - nvl(a.ndsdeduction_sum_payment, 0) as ndsdeduction_sum_payment
              ,nvl(b.nds_sum_compensation, 0) - nvl(a.nds_sum_compensation, 0) as nds_sum_compensation
              ,nvl(b.nds_sum_payment, 0) - nvl(a.nds_sum_payment, 0) as nds_sum_payment
            from dual
            left outer join
            (
                 select
                   sum(decl_count_total) as decl_count_total
                  ,sum(decl_count_null) as decl_count_null
                  ,sum(decl_count_compensation) as decl_count_compensation
                  ,sum(decl_count_payment) as decl_count_payment
                  ,sum(decl_count_not_submit) as decl_count_not_submit
                  ,sum(decl_count_revised_total) as decl_count_revised_total
                  ,sum(taxbase_sum_compensation) as taxbase_sum_compensation
                  ,sum(taxbase_sum_payment) as taxbase_sum_payment
                  ,sum(ndscalculated_sum_compensation) as ndscalculated_sum_compensation
                  ,sum(ndscalculated_sum_payment) as ndscalculated_sum_payment
                  ,sum(ndsdeduction_sum_compensation) as ndsdeduction_sum_compensation
                  ,sum(ndsdeduction_sum_payment) as ndsdeduction_sum_payment
                  ,sum(nds_sum_compensation) as nds_sum_compensation
                  ,sum(nds_sum_payment) as nds_sum_payment
                from REPORT_DECLARATION_RAW r
                where trunc(r.submit_date) = trunc(pDateBegin) and r.fiscal_year = pFiscalYear
                      and r.tax_period in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD')
            ) a on 1 = 1
            left outer join
            (
                 select
                   sum(decl_count_total) as decl_count_total
                  ,sum(decl_count_null) as decl_count_null
                  ,sum(decl_count_compensation) as decl_count_compensation
                  ,sum(decl_count_payment) as decl_count_payment
                  ,sum(decl_count_not_submit) as decl_count_not_submit
                  ,sum(decl_count_revised_total) as decl_count_revised_total
                  ,sum(taxbase_sum_compensation) as taxbase_sum_compensation
                  ,sum(taxbase_sum_payment) as taxbase_sum_payment
                  ,sum(ndscalculated_sum_compensation) as ndscalculated_sum_compensation
                  ,sum(ndscalculated_sum_payment) as ndscalculated_sum_payment
                  ,sum(ndsdeduction_sum_compensation) as ndsdeduction_sum_compensation
                  ,sum(ndsdeduction_sum_payment) as ndsdeduction_sum_payment
                  ,sum(nds_sum_compensation) as nds_sum_compensation
                  ,sum(nds_sum_payment) as nds_sum_payment
                from REPORT_DECLARATION_RAW r
                where trunc(r.submit_date) = trunc(pDateEnd) and r.fiscal_year = pFiscalYear
                      and r.tax_period in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD')
            ) b on 1 = 1;            
          end if;

          --По федеральным округам
          if pDeclStatisticType = 2 then
            insert into REPORT_DECLARATION (ID, TASK_ID, ACTUAL_NUM, AGG_ROW, Name_Group,Decl_Count_Total, Decl_Count_Null,
                  Decl_Count_Compensation, Decl_Count_Payment, Decl_Count_Not_Submit, Decl_Count_Revised_Total, TaxBase_Sum_Compensation,
                  TaxBase_Sum_Payment, NdsCalculated_Sum_Compensation, NdsCalculated_Sum_Payment, NdsDeduction_Sum_Compensation,
                  NdsDeduction_Sum_Payment, Nds_Sum_Compensation, Nds_Sum_Payment)
            select NDS2$REPORTS.F$GET_DECLARATION_NEXTVAL
                   ,pTaskId
                   ,pActualNumActive
                   ,0 as AGG_ROW
                   ,fd.description as name_group
                   ,nvl(b.decl_count_total, 0) - nvl(a.decl_count_total, 0)
                   ,nvl(b.decl_count_null, 0) - nvl(a.decl_count_null, 0)
                   ,nvl(b.decl_count_compensation, 0) - nvl(a.decl_count_compensation, 0)
                   ,nvl(b.decl_count_payment, 0) - nvl(a.decl_count_payment, 0)
                   ,nvl(b.decl_count_not_submit, 0) - nvl(a.decl_count_not_submit, 0)
                   ,nvl(b.decl_count_revised_total, 0) - nvl(a.decl_count_revised_total, 0)
                   ,nvl(b.taxbase_sum_compensation, 0) - nvl(a.taxbase_sum_compensation, 0)
                   ,nvl(b.taxbase_sum_payment, 0) - nvl(a.taxbase_sum_payment, 0)
                   ,nvl(b.ndscalculated_sum_compensation, 0) - nvl(a.ndscalculated_sum_compensation, 0)
                   ,nvl(b.ndscalculated_sum_payment, 0) - nvl(a.ndscalculated_sum_payment, 0)
                   ,nvl(b.ndsdeduction_sum_compensation, 0) - nvl(a.ndsdeduction_sum_compensation, 0)
                   ,nvl(b.ndsdeduction_sum_payment, 0) - nvl(a.ndsdeduction_sum_payment, 0)
                   ,nvl(b.nds_sum_compensation, 0) - nvl(a.nds_sum_compensation, 0)
                   ,nvl(b.nds_sum_payment, 0) - nvl(a.nds_sum_payment, 0)
            from
            (
              select fdr.district_id
              from REPORT_DECLARATION_RAW r
              join FEDERAL_DISTRICT_REGION fdr on fdr.region_code = r.region_code
              where (trunc(r.submit_date) = trunc(pDateBegin) or trunc(r.submit_date) = trunc(pDateEnd)) and r.fiscal_year = pFiscalYear
                    and r.tax_period in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD')
              group by fdr.district_id
            ) m
            left outer join
            (
                 select distinct r.district_id
                       ,SUM(r.decl_count_total) OVER (PARTITION BY r.district_id) decl_count_total
                       ,SUM(r.decl_count_null) OVER (PARTITION BY r.district_id) decl_count_null
                       ,SUM(r.decl_count_compensation) OVER (PARTITION BY r.district_id) decl_count_compensation
                       ,SUM(r.decl_count_payment) OVER (PARTITION BY r.district_id) decl_count_payment
                       ,SUM(r.decl_count_not_submit) OVER (PARTITION BY r.district_id) decl_count_not_submit
                       ,SUM(r.decl_count_revised_total) OVER (PARTITION BY r.district_id) decl_count_revised_total
                       ,SUM(r.taxbase_sum_compensation) OVER (PARTITION BY r.district_id) taxbase_sum_compensation
                       ,SUM(r.taxbase_sum_payment) OVER (PARTITION BY r.district_id) taxbase_sum_payment
                       ,SUM(r.ndscalculated_sum_compensation) OVER (PARTITION BY r.district_id) ndscalculated_sum_compensation
                       ,SUM(r.ndscalculated_sum_payment) OVER (PARTITION BY r.district_id) ndscalculated_sum_payment
                       ,SUM(r.ndsdeduction_sum_compensation) OVER (PARTITION BY r.district_id) ndsdeduction_sum_compensation
                       ,SUM(r.ndsdeduction_sum_payment) OVER (PARTITION BY r.district_id) ndsdeduction_sum_payment
                       ,SUM(r.nds_sum_compensation) OVER (PARTITION BY r.district_id) nds_sum_compensation
                       ,SUM(r.nds_sum_payment) OVER (PARTITION BY r.district_id) nds_sum_payment
                 from
                 (
                    select r.*, fd.district_id as district_id
                    from REPORT_DECLARATION_RAW r
                    join FEDERAL_DISTRICT_REGION fdr on fdr.region_code = r.region_code
                    join FEDERAL_DISTRICT fd on fd.district_id = fdr.district_id
                    where trunc(r.submit_date) = trunc(pDateBegin) and r.fiscal_year = pFiscalYear
                          and r.tax_period in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD')
                 )r
            ) a on a.district_id = m.district_id
            left outer join
            (
                 select distinct r.district_id
                       ,SUM(r.decl_count_total) OVER (PARTITION BY r.district_id) decl_count_total
                       ,SUM(r.decl_count_null) OVER (PARTITION BY r.district_id) decl_count_null
                       ,SUM(r.decl_count_compensation) OVER (PARTITION BY r.district_id) decl_count_compensation
                       ,SUM(r.decl_count_payment) OVER (PARTITION BY r.district_id) decl_count_payment
                       ,SUM(r.decl_count_not_submit) OVER (PARTITION BY r.district_id) decl_count_not_submit
                       ,SUM(r.decl_count_revised_total) OVER (PARTITION BY r.district_id) decl_count_revised_total
                       ,SUM(r.taxbase_sum_compensation) OVER (PARTITION BY r.district_id) taxbase_sum_compensation
                       ,SUM(r.taxbase_sum_payment) OVER (PARTITION BY r.district_id) taxbase_sum_payment
                       ,SUM(r.ndscalculated_sum_compensation) OVER (PARTITION BY r.district_id) ndscalculated_sum_compensation
                       ,SUM(r.ndscalculated_sum_payment) OVER (PARTITION BY r.district_id) ndscalculated_sum_payment
                       ,SUM(r.ndsdeduction_sum_compensation) OVER (PARTITION BY r.district_id) ndsdeduction_sum_compensation
                       ,SUM(r.ndsdeduction_sum_payment) OVER (PARTITION BY r.district_id) ndsdeduction_sum_payment
                       ,SUM(r.nds_sum_compensation) OVER (PARTITION BY r.district_id) nds_sum_compensation
                       ,SUM(r.nds_sum_payment) OVER (PARTITION BY r.district_id) nds_sum_payment
                 from
                 (
                    select r.*, fd.district_id as district_id
                    from REPORT_DECLARATION_RAW r
                    join FEDERAL_DISTRICT_REGION fdr on fdr.region_code = r.region_code
                    join FEDERAL_DISTRICT fd on fd.district_id = fdr.district_id
                    where trunc(r.submit_date) = trunc(pDateEnd) and r.fiscal_year = pFiscalYear
                          and r.tax_period in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD')
                 )r
            ) b on b.district_id = m.district_id
            left outer join FEDERAL_DISTRICT fd on fd.district_id = m.district_id;
          end if;            
          --По регионам
          if pDeclStatisticType = 3 then
            insert into REPORT_DECLARATION (ID, TASK_ID, ACTUAL_NUM, AGG_ROW, Name_Group,Decl_Count_Total, Decl_Count_Null,
                  Decl_Count_Compensation, Decl_Count_Payment, Decl_Count_Not_Submit, Decl_Count_Revised_Total, TaxBase_Sum_Compensation,
                  TaxBase_Sum_Payment, NdsCalculated_Sum_Compensation, NdsCalculated_Sum_Payment, NdsDeduction_Sum_Compensation,
                  NdsDeduction_Sum_Payment, Nds_Sum_Compensation, Nds_Sum_Payment)
            select NDS2$REPORTS.F$GET_DECLARATION_NEXTVAL
                   ,pTaskId
                   ,pActualNumActive
                   ,0 as AGG_ROW
                   ,s.s_code || '-' || s.s_name as name_group
                   ,nvl(b.decl_count_total,0) - nvl(a.decl_count_total ,0)
                   ,nvl(b.decl_count_null,0) - nvl(a.decl_count_null ,0)
                   ,nvl(b.decl_count_compensation,0) - nvl(a.decl_count_compensation ,0)
                   ,nvl(b.decl_count_payment,0) - nvl(a.decl_count_payment ,0)
                   ,nvl(b.decl_count_not_submit,0) - nvl(a.decl_count_not_submit ,0)
                   ,nvl(b.decl_count_revised_total,0) - nvl(a.decl_count_revised_total ,0)
                   ,nvl(b.taxbase_sum_compensation,0) - nvl(a.taxbase_sum_compensation ,0)
                   ,nvl(b.taxbase_sum_payment,0) - nvl(a.taxbase_sum_payment ,0)
                   ,nvl(b.ndscalculated_sum_compensation,0) - nvl(a.ndscalculated_sum_compensation ,0)
                   ,nvl(b.ndscalculated_sum_payment,0) - nvl(a.ndscalculated_sum_payment ,0)
                   ,nvl(b.ndsdeduction_sum_compensation,0) - nvl(a.ndsdeduction_sum_compensation ,0)
                   ,nvl(b.ndsdeduction_sum_payment,0) - nvl(a.ndsdeduction_sum_payment ,0)
                   ,nvl(b.nds_sum_compensation,0) - nvl(a.nds_sum_compensation ,0)
                   ,nvl(b.nds_sum_payment,0) - nvl(a.nds_sum_payment ,0)
            from
            (
              select r.region_code
              from REPORT_DECLARATION_RAW r
              where (trunc(r.submit_date) = trunc(pDateBegin) or trunc(r.submit_date) = trunc(pDateEnd)) and r.fiscal_year = pFiscalYear
                    and r.tax_period in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD')
              group by r.region_code
            ) m
            left outer join
            (
                 select distinct r.region_code
                       ,SUM(r.decl_count_total) OVER (PARTITION BY r.region_code) decl_count_total
                       ,SUM(r.decl_count_null) OVER (PARTITION BY r.region_code) decl_count_null
                       ,SUM(r.decl_count_compensation) OVER (PARTITION BY r.region_code) decl_count_compensation
                       ,SUM(r.decl_count_payment) OVER (PARTITION BY r.region_code) decl_count_payment
                       ,SUM(r.decl_count_not_submit) OVER (PARTITION BY r.region_code) decl_count_not_submit
                       ,SUM(r.decl_count_revised_total) OVER (PARTITION BY r.region_code) decl_count_revised_total
                       ,SUM(r.taxbase_sum_compensation) OVER (PARTITION BY r.region_code) taxbase_sum_compensation
                       ,SUM(r.taxbase_sum_payment) OVER (PARTITION BY r.region_code) taxbase_sum_payment
                       ,SUM(r.ndscalculated_sum_compensation) OVER (PARTITION BY r.region_code) ndscalculated_sum_compensation
                       ,SUM(r.ndscalculated_sum_payment) OVER (PARTITION BY r.region_code) ndscalculated_sum_payment
                       ,SUM(r.ndsdeduction_sum_compensation) OVER (PARTITION BY r.region_code) ndsdeduction_sum_compensation
                       ,SUM(r.ndsdeduction_sum_payment) OVER (PARTITION BY r.region_code) ndsdeduction_sum_payment
                       ,SUM(r.nds_sum_compensation) OVER (PARTITION BY r.region_code) nds_sum_compensation
                       ,SUM(r.nds_sum_payment) OVER (PARTITION BY r.region_code) nds_sum_payment
                 from REPORT_DECLARATION_RAW r
                 where trunc(r.submit_date) = trunc(pDateBegin) and r.fiscal_year = pFiscalYear
                       and r.tax_period in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD')
            ) a on a.region_code = m.region_code
            left outer join
            (
                 select distinct r.region_code
                       ,SUM(r.decl_count_total) OVER (PARTITION BY r.region_code) decl_count_total
                       ,SUM(r.decl_count_null) OVER (PARTITION BY r.region_code) decl_count_null
                       ,SUM(r.decl_count_compensation) OVER (PARTITION BY r.region_code) decl_count_compensation
                       ,SUM(r.decl_count_payment) OVER (PARTITION BY r.region_code) decl_count_payment
                       ,SUM(r.decl_count_not_submit) OVER (PARTITION BY r.region_code) decl_count_not_submit
                       ,SUM(r.decl_count_revised_total) OVER (PARTITION BY r.region_code) decl_count_revised_total
                       ,SUM(r.taxbase_sum_compensation) OVER (PARTITION BY r.region_code) taxbase_sum_compensation
                       ,SUM(r.taxbase_sum_payment) OVER (PARTITION BY r.region_code) taxbase_sum_payment
                       ,SUM(r.ndscalculated_sum_compensation) OVER (PARTITION BY r.region_code) ndscalculated_sum_compensation
                       ,SUM(r.ndscalculated_sum_payment) OVER (PARTITION BY r.region_code) ndscalculated_sum_payment
                       ,SUM(r.ndsdeduction_sum_compensation) OVER (PARTITION BY r.region_code) ndsdeduction_sum_compensation
                       ,SUM(r.ndsdeduction_sum_payment) OVER (PARTITION BY r.region_code) ndsdeduction_sum_payment
                       ,SUM(r.nds_sum_compensation) OVER (PARTITION BY r.region_code) nds_sum_compensation
                       ,SUM(r.nds_sum_payment) OVER (PARTITION BY r.region_code) nds_sum_payment
                 from REPORT_DECLARATION_RAW r
                 where trunc(r.submit_date) = trunc(pDateEnd) and r.fiscal_year = pFiscalYear
                       and r.tax_period in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD')
            ) b on b.region_code = m.region_code
            left outer join V$SSRF s on s.s_code = m.region_code;
          end if;
          --По инспекциям
          if pDeclStatisticType = 4 then
            insert into REPORT_DECLARATION (ID, TASK_ID, ACTUAL_NUM, AGG_ROW, Name_Group,Decl_Count_Total, Decl_Count_Null,
                  Decl_Count_Compensation, Decl_Count_Payment, Decl_Count_Not_Submit, Decl_Count_Revised_Total, TaxBase_Sum_Compensation,
                  TaxBase_Sum_Payment, NdsCalculated_Sum_Compensation, NdsCalculated_Sum_Payment, NdsDeduction_Sum_Compensation,
                  NdsDeduction_Sum_Payment, Nds_Sum_Compensation, Nds_Sum_Payment)
            select NDS2$REPORTS.F$GET_DECLARATION_NEXTVAL
                   ,pTaskId
                   ,pActualNumActive
                   ,0 as AGG_ROW
                   ,s.s_code || '-' || s.s_name
                   ,nvl(b.decl_count_total, 0) - nvl(a.decl_count_total, 0)
                   ,nvl(b.decl_count_null, 0) - nvl(a.decl_count_null, 0)
                   ,nvl(b.decl_count_compensation, 0) - nvl(a.decl_count_compensation, 0)
                   ,nvl(b.decl_count_payment, 0) - nvl(a.decl_count_payment, 0)
                   ,nvl(b.decl_count_not_submit, 0) - nvl(a.decl_count_not_submit, 0)
                   ,nvl(b.decl_count_revised_total, 0) - nvl(a.decl_count_revised_total, 0)
                   ,nvl(b.taxbase_sum_compensation, 0) - nvl(a.taxbase_sum_compensation, 0)
                   ,nvl(b.taxbase_sum_payment, 0) - nvl(a.taxbase_sum_payment, 0)
                   ,nvl(b.ndscalculated_sum_compensation, 0) - nvl(a.ndscalculated_sum_compensation, 0)
                   ,nvl(b.ndscalculated_sum_payment, 0) - nvl(a.ndscalculated_sum_payment, 0)
                   ,nvl(b.ndsdeduction_sum_compensation, 0) - nvl(a.ndsdeduction_sum_compensation, 0)
                   ,nvl(b.ndsdeduction_sum_payment, 0) - nvl(a.ndsdeduction_sum_payment, 0)
                   ,nvl(b.nds_sum_compensation, 0) - nvl(a.nds_sum_compensation, 0)
                   ,nvl(b.nds_sum_payment, 0) - nvl(a.nds_sum_payment, 0)
            from
            (
              select r.soun_code
              from REPORT_DECLARATION_RAW r
              where r.region_code in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'REGION_CODE')
                    and (trunc(r.submit_date) = trunc(pDateBegin) or trunc(r.submit_date) = trunc(pDateEnd)) and r.fiscal_year = pFiscalYear
                    and r.tax_period in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD')
              group by r.soun_code
            ) m
            left outer join
            (
                 select distinct r.soun_code
                       ,SUM(r.decl_count_total) OVER (PARTITION BY r.soun_code) decl_count_total
                       ,SUM(r.decl_count_null) OVER (PARTITION BY r.soun_code) decl_count_null
                       ,SUM(r.decl_count_compensation) OVER (PARTITION BY r.soun_code) decl_count_compensation
                       ,SUM(r.decl_count_payment) OVER (PARTITION BY r.soun_code) decl_count_payment
                       ,SUM(r.decl_count_not_submit) OVER (PARTITION BY r.soun_code) decl_count_not_submit
                       ,SUM(r.decl_count_revised_total) OVER (PARTITION BY r.soun_code) decl_count_revised_total
                       ,SUM(r.taxbase_sum_compensation) OVER (PARTITION BY r.soun_code) taxbase_sum_compensation
                       ,SUM(r.taxbase_sum_payment) OVER (PARTITION BY r.soun_code) taxbase_sum_payment
                       ,SUM(r.ndscalculated_sum_compensation) OVER (PARTITION BY r.soun_code) ndscalculated_sum_compensation
                       ,SUM(r.ndscalculated_sum_payment) OVER (PARTITION BY r.soun_code) ndscalculated_sum_payment
                       ,SUM(r.ndsdeduction_sum_compensation) OVER (PARTITION BY r.soun_code) ndsdeduction_sum_compensation
                       ,SUM(r.ndsdeduction_sum_payment) OVER (PARTITION BY r.soun_code) ndsdeduction_sum_payment
                       ,SUM(r.nds_sum_compensation) OVER (PARTITION BY r.soun_code) nds_sum_compensation
                       ,SUM(r.nds_sum_payment) OVER (PARTITION BY r.soun_code) nds_sum_payment
                 from REPORT_DECLARATION_RAW r
                 where r.region_code in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'REGION_CODE')
                       and trunc(r.submit_date) = trunc(pDateBegin) and r.fiscal_year = pFiscalYear
                       and r.tax_period in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD')
            ) a on a.soun_code = m.soun_code
            left outer join
            (
                 select distinct r.soun_code
                       ,SUM(r.decl_count_total) OVER (PARTITION BY r.soun_code) decl_count_total
                       ,SUM(r.decl_count_null) OVER (PARTITION BY r.soun_code) decl_count_null
                       ,SUM(r.decl_count_compensation) OVER (PARTITION BY r.soun_code) decl_count_compensation
                       ,SUM(r.decl_count_payment) OVER (PARTITION BY r.soun_code) decl_count_payment
                       ,SUM(r.decl_count_not_submit) OVER (PARTITION BY r.soun_code) decl_count_not_submit
                       ,SUM(r.decl_count_revised_total) OVER (PARTITION BY r.soun_code) decl_count_revised_total
                       ,SUM(r.taxbase_sum_compensation) OVER (PARTITION BY r.soun_code) taxbase_sum_compensation
                       ,SUM(r.taxbase_sum_payment) OVER (PARTITION BY r.soun_code) taxbase_sum_payment
                       ,SUM(r.ndscalculated_sum_compensation) OVER (PARTITION BY r.soun_code) ndscalculated_sum_compensation
                       ,SUM(r.ndscalculated_sum_payment) OVER (PARTITION BY r.soun_code) ndscalculated_sum_payment
                       ,SUM(r.ndsdeduction_sum_compensation) OVER (PARTITION BY r.soun_code) ndsdeduction_sum_compensation
                       ,SUM(r.ndsdeduction_sum_payment) OVER (PARTITION BY r.soun_code) ndsdeduction_sum_payment
                       ,SUM(r.nds_sum_compensation) OVER (PARTITION BY r.soun_code) nds_sum_compensation
                       ,SUM(r.nds_sum_payment) OVER (PARTITION BY r.soun_code) nds_sum_payment
                 from REPORT_DECLARATION_RAW r
                 where r.region_code in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'REGION_CODE')
                       and trunc(r.submit_date) = trunc(pDateEnd) and r.fiscal_year = pFiscalYear
                       and r.tax_period in (SELECT PARAM_VALUE FROM REPORT_TASK_PARAM WHERE TASK_ID = pTaskId AND PARAM_KEY = 'NALOG_PERIOD')
            ) b on b.soun_code = m.soun_code
            left outer join V$SONO s on s.s_code = m.soun_code;
          end if;
      end;
      end if;

      UPDATE REPORT_TASK SET TASK_STATUS = 1, COMPLETE_DATE = SYSDATE WHERE ID = pTaskId;

      commit;
  end;
  end if;
end;



END "NDS2$REPORTS";
/