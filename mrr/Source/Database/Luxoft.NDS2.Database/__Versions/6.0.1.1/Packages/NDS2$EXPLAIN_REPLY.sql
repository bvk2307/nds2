﻿CREATE OR REPLACE PACKAGE NDS2_MRR_USER."NDS2$EXPLAIN_REPLY" IS

PROCEDURE P$STAGE_CORRECT_UPDATE
(
  pExplainId IN NUMBER,
  pInvoiceId IN VARCHAR2,
  pFieldName IN VARCHAR2,
  pFieldValue IN VARCHAR2,
  pTypeOperation IN NUMBER
);

PROCEDURE P$STAGE_CORRECT_UPDATE_STATE
(
  pExplainId IN NUMBER,
  pInvoiceId IN VARCHAR2,
  pInvoiceState IN NUMBER
);


PROCEDURE P$STAGE_CORRECT_DELETE
(
  pExplainId IN NUMBER,
  pInvoiceId IN VARCHAR2
);

PROCEDURE P$STAGE_CORRECT_DELETE_ALL
(
  pExplainId IN NUMBER
);

PROCEDURE P$EXISTS_INVOICE_CORRECT
(
  pExplainId IN NUMBER,
  IsExists OUT NUMBER
);


END NDS2$EXPLAIN_REPLY;
/

CREATE OR REPLACE PACKAGE BODY NDS2_MRR_USER."NDS2$EXPLAIN_REPLY" IS

PROCEDURE P$STAGE_CORRECT_UPDATE
(
  pExplainId IN NUMBER,
  pInvoiceId IN VARCHAR2,
  pFieldName IN VARCHAR2,
  pFieldValue IN VARCHAR2,
  pTypeOperation IN NUMBER
)
as
  stage_invoice_correct_id number(10);
  pExistsId NUMBER := -1;
begin

  if (pTypeOperation = 1) then
  begin

      SELECT id into pExistsId FROM dual
      left outer join (select id from STAGE_INVOICE_CORRECTED where explain_id = pExplainId and invoice_original_id = pInvoiceId and field_name = pFieldName) r on 1 = 1;

      if (pExistsId >= 0) then
      begin
        update STAGE_INVOICE_CORRECTED set field_value = pFieldValue where explain_id = pExplainId and invoice_original_id = pInvoiceId and field_name = pFieldName;
      end;
      else
      begin
        stage_invoice_correct_id := SEQ_STAGE_INVOICE_CORRECT.NEXTVAL;
        insert into STAGE_INVOICE_CORRECTED (id, explain_id, invoice_original_id, field_name, field_value)
        values(stage_invoice_correct_id, pExplainId, pInvoiceId, pFieldName, pFieldValue);
      end;
      end if;
      commit;
  end;
  else
  begin
      if (pTypeOperation = 2) then
      begin
        delete from STAGE_INVOICE_CORRECTED where explain_id = pExplainId and invoice_original_id = pInvoiceId and field_name = pFieldName;
        commit;
      end;
      end if;
  end;
  end if;

end;

PROCEDURE P$STAGE_CORRECT_UPDATE_STATE
(
  pExplainId IN NUMBER,
  pInvoiceId IN VARCHAR2,
  pInvoiceState IN NUMBER
)
as
  stage_invoice_correct_state_id number(10);
  pExistsId NUMBER := -1;
begin
  
  SELECT id into pExistsId FROM dual
  left outer join (select id from STAGE_INVOICE_CORRECTED_STATE where explain_id = pExplainId and invoice_original_id = pInvoiceId) r on 1 = 1;

  if (pExistsId >= 0) then
  begin
    update STAGE_INVOICE_CORRECTED_STATE set invoice_state = pInvoiceState where explain_id = pExplainId and invoice_original_id = pInvoiceId;
  end;
  else
  begin
    stage_invoice_correct_state_id := SEQ_STAGE_INVOICE_CORRECT_ST.NEXTVAL;
    insert into STAGE_INVOICE_CORRECTED_STATE (id, explain_id, invoice_original_id, invoice_state)
    values(stage_invoice_correct_state_id, pExplainId, pInvoiceId, pInvoiceState);
  end;
  end if;
  commit;
  
end;

PROCEDURE P$STAGE_CORRECT_DELETE
(
  pExplainId IN NUMBER,
  pInvoiceId IN VARCHAR2
)
as
begin
  
  delete from STAGE_INVOICE_CORRECTED_STATE where explain_id = pExplainId and invoice_original_id = pInvoiceId;
  delete from STAGE_INVOICE_CORRECTED where explain_id = pExplainId and invoice_original_id = pInvoiceId;
  commit;
  
end;

PROCEDURE P$STAGE_CORRECT_DELETE_ALL
(
  pExplainId IN NUMBER
)
as
begin
  
  delete from STAGE_INVOICE_CORRECTED_STATE where explain_id = pExplainId;
  delete from STAGE_INVOICE_CORRECTED where explain_id = pExplainId;
  commit;
  
end;

PROCEDURE P$EXISTS_INVOICE_CORRECT
(
  pExplainId IN NUMBER,
  IsExists OUT NUMBER
)
as
  countCorret NUMBER := -1;
  countCorretState NUMBER := -1;
begin
  
  SELECT countRec into countCorret FROM dual
  left outer join (select count(id) as countRec from STAGE_INVOICE_CORRECTED where explain_id = pExplainId) r on 1 = 1;

  SELECT countRec into countCorretState FROM dual
  left outer join (select count(id) as countRec from STAGE_INVOICE_CORRECTED_STATE where explain_id = pExplainId) r on 1 = 1;
  
  IsExists := 0;  
  if ((countCorret is not null and countCorret > 0) or
      (countCorretState is not null and countCorretState > 0)) then
  begin
      IsExists := 1;  
  end;
  end if;
  
end;

END "NDS2$EXPLAIN_REPLY";
/
