﻿BEGIN
DBMS_SCHEDULER.DROP_JOB (
job_name => 'NDS2_MRR_USER.J$REPORT_DECL_BACKGROUND');
exception when others then null;
END;
/
begin 
  sys.dbms_scheduler.create_job(job_name => 'NDS2_MRR_USER.J$REPORT_DECL_BACKGROUND', 
  job_type => 'STORED_PROCEDURE', 
  job_action => 'NDS2$REPORTS.START_DECLARATION_CALCULATE', 
  start_date => TO_DATE(TO_CHAR(SYSDATE, 'dd/mm/yyyy') || ' 23:00:00', 'dd/mm/yyyy HH24:mi:ss') + INTERVAL '1' MINUTE, 
  repeat_interval => 'freq=minutely;Interval=1', 
  end_date => to_date('01-01-2100 00:00:00', 'dd-mm-yyyy hh24:mi:ss'), 
  job_class => 'DEFAULT_JOB_CLASS', 
  enabled => false, 
  auto_drop => false, 
  comments => 'аннулирование выборок'); 
end; 
/