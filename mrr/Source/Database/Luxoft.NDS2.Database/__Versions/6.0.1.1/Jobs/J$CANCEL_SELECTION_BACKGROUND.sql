﻿BEGIN
DBMS_SCHEDULER.DROP_JOB (
job_name => 'NDS2_MRR_USER.J$CANCEL_SELECTION_BACKGROUND');
exception when others then null;
END;
/
begin 
  sys.dbms_scheduler.create_job(job_name => 'NDS2_MRR_USER.J$CANCEL_SELECTION_BACKGROUND', 
  job_type => 'STORED_PROCEDURE', 
  job_action => 'Nds2$Selections.P$START_CANCEL_SELECTION', 
  start_date => SYSDATE + INTERVAL '1' MINUTE, 
  repeat_interval => 'freq=secondly;Interval=10', 
  end_date => to_date('01-01-2100 00:00:00', 'dd-mm-yyyy hh24:mi:ss'), 
  job_class => 'DEFAULT_JOB_CLASS', 
  enabled => false, 
  auto_drop => false, 
  comments => 'аннулирование выборок'); 
end; 
/