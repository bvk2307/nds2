﻿BEGIN
DBMS_SCHEDULER.DROP_JOB (
job_name => 'NDS2_MRR_USER.J$DEMO_JOB');
exception when others then null;
END;
/
begin 
  sys.dbms_scheduler.create_job(job_name => 'NDS2_MRR_USER.J$DEMO_JOB', 
  job_type => 'STORED_PROCEDURE', 
  job_action => 'PAC$DOCUMENT.DEMO', 
  start_date => SYSDATE + INTERVAL '600' SECOND, 
  repeat_interval => 'freq=hourly;Interval=6',
  end_date => to_date('01-01-2100 00:00:00', 'dd-mm-yyyy hh24:mi:ss'), 
  job_class => 'DEFAULT_JOB_CLASS', 
  enabled => false, 
  auto_drop => false, 
  comments => 'фоновые задания МРР'); 
end; 
/

BEGIN
DBMS_SCHEDULER.DROP_JOB (
job_name => 'NDS2_MRR_USER.J$CLAIM_KS');
exception when others then null;
END;
/
begin 
  sys.dbms_scheduler.create_job(job_name => 'NDS2_MRR_USER.J$CLAIM_KS', 
  job_type => 'STORED_PROCEDURE', 
  job_action => 'PAC$DOCUMENT.SEND_CLAIM_KS', 
  start_date => SYSDATE + INTERVAL '300' SECOND, 
  repeat_interval => 'freq=hourly;Interval=1',
  end_date => to_date('01-01-2100 00:00:00', 'dd-mm-yyyy hh24:mi:ss'), 
  job_class => 'DEFAULT_JOB_CLASS', 
  enabled => false, 
  auto_drop => false, 
  comments => 'фоновые задания МРР'); 
end; 
/
