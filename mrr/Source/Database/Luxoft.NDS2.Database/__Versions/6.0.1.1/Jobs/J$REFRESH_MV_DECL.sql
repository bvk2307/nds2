﻿BEGIN
DBMS_SCHEDULER.DROP_JOB (
job_name => 'NDS2_MRR_USER.J$REFRESH_MV_DECL');
exception when others then null;
END;
/
begin 
  sys.dbms_scheduler.create_job(job_name => 'NDS2_MRR_USER.J$REFRESH_MV_DECL', 
  job_type => 'STORED_PROCEDURE', 
  job_action => 'PAC$MVIEW_MANAGER.REFRESH_DECLARATONS', 
  start_date => SYSDATE + INTERVAL '3' second, 
  repeat_interval => 'freq=secondly;Interval=5', 
  end_date => to_date('01-01-2100 00:00:00', 'dd-mm-yyyy hh24:mi:ss'), 
  job_class => 'DEFAULT_JOB_CLASS', 
  enabled => false, 
  auto_drop => false, 
  comments => 'фоновые задания МРР: обновление мат. представлений по декларациям'); 
end; 
/
