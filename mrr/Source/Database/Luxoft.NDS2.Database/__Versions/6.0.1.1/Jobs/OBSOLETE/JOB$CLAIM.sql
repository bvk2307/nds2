﻿BEGIN
DBMS_SCHEDULER.DROP_JOB (
job_name => 'NDS2_MRR_USER.J$CREATE_NEW_CLAIMS');
exception when others then null;
END;
/
begin 
	sys.dbms_scheduler.create_job(job_name => 'NDS2_MRR_USER.J$CREATE_NEW_CLAIMS', 
	job_type => 'STORED_PROCEDURE', 
	job_action => 'PAC$CLAIM.process_new_claim', 
	start_date => SYSDATE + INTERVAL '1' MINUTE, 
	repeat_interval => 'freq=Minutely;Interval=1', 
	end_date => to_date('01-01-2100 00:00:00', 'dd-mm-yyyy hh24:mi:ss'), 
	job_class => 'DEFAULT_JOB_CLASS', 
	enabled => false, 
	auto_drop => false, 
	comments => 'Формирование новых требований к налогоплательщику'); 
end; 
/
