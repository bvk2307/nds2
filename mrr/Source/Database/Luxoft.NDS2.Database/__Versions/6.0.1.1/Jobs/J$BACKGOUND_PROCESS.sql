﻿BEGIN
DBMS_SCHEDULER.DROP_JOB (
job_name => 'NDS2_MRR_USER.J$BACKGOUND_PROCESS');
exception when others then null;
END;
/
begin 
  sys.dbms_scheduler.create_job(job_name => 'NDS2_MRR_USER.J$BACKGOUND_PROCESS', 
  job_type => 'STORED_PROCEDURE', 
  job_action => 'PAC$JOB.START_WORK', 
  start_date => SYSDATE + INTERVAL '3' SECOND, 
  repeat_interval => 'freq=Secondly;Interval=30', 
  end_date => to_date('01-01-2100 00:00:00', 'dd-mm-yyyy hh24:mi:ss'), 
  job_class => 'DEFAULT_JOB_CLASS', 
  enabled => false, 
  auto_drop => false, 
  comments => 'фоновые задания МРР'); 
end; 
/