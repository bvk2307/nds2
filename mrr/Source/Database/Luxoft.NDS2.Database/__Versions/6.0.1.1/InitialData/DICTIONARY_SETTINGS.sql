﻿truncate table NDS2_MRR_USER.Dictionary_Column_Settings;
truncate table NDS2_MRR_USER.Dictionary_Table_Settings;

INSERT INTO NDS2_MRR_USER.Dictionary_Table_Settings (Id, Name, Caption, IsEditable) VALUES(1, 'V$SSRF', 'Регионы', 1);
INSERT INTO NDS2_MRR_USER.Dictionary_Table_Settings (Id, Name, Caption, IsEditable) VALUES(2, 'V$SONO', 'Налоговые органы', 1);
INSERT INTO NDS2_MRR_USER.Dictionary_Table_Settings (Id, Name, Caption, IsEditable) VALUES(3, 'CONTROL_RATIO_TYPE', 'Виды контрольных соотношений', 1);
INSERT INTO NDS2_MRR_USER.Dictionary_Table_Settings (Id, Name, Caption, IsEditable) VALUES(4, 'V$DICT_LOGICAL_ERRORS_MAPPING', 'Ошибки ЛК', 1);
INSERT INTO NDS2_MRR_USER.Dictionary_Table_Settings (Id, Name, Caption, IsEditable) VALUES(5, 'FEDERAL_DISTRICT', 'Федеральные округа', 1);


INSERT INTO NDS2_MRR_USER.Dictionary_Column_Settings (Id, Table_Name, Table_Name_Edit, Column_Name, Caption, Rank, IsKey, IsEditable, Width, Sorting, Visible) 
  VALUES(1, 'V$SSRF', 'V$SSRF', 'S_CODE', 'Код', 0, 0, 0, 70, 'Asc', 1);
INSERT INTO NDS2_MRR_USER.Dictionary_Column_Settings (Id, Table_Name, Table_Name_Edit, Column_Name, Caption, Rank, IsKey, IsEditable, Width, Sorting, Visible) 
  VALUES(2, 'V$SSRF', 'V$SSRF', 'S_NAME', 'Название', 1, 0, 0, 500, '', 1);

INSERT INTO NDS2_MRR_USER.Dictionary_Column_Settings (Id, Table_Name, Table_Name_Edit, Column_Name, Caption, Rank, IsKey, IsEditable, Width, Sorting, Visible) 
  VALUES(3, 'V$SONO','V$SONO', 'S_CODE', 'Код ИФСН', 0, 0, 0, 70, 'Asc', 1);
INSERT INTO NDS2_MRR_USER.Dictionary_Column_Settings (Id, Table_Name, Table_Name_Edit, Column_Name, Caption, Rank, IsKey, IsEditable, Width, Sorting, Visible) 
  VALUES(4, 'V$SONO', 'V$SONO','S_NAME', 'Название', 1, 0, 1, 450, '', 1);
INSERT INTO NDS2_MRR_USER.Dictionary_Column_Settings (Id, Table_Name, Table_Name_Edit, Column_Name, Caption, Rank, IsKey, IsEditable, Width, Sorting, Visible) 
  VALUES(5, 'V$SONO', 'SONO_EXT', 'TYPE', 'Тип', 2, 0, 0, 60, '', 1);
INSERT INTO NDS2_MRR_USER.Dictionary_Column_Settings (Id, Table_Name, Table_Name_Edit, Column_Name, Caption, Rank, IsKey, IsEditable, Width, Sorting, Visible) 
  VALUES(6, 'V$SONO', 'SONO_EXT', 'EMPLOYERS_COUNT', 'Количество инспекторов в инспекции', 3, 0, 0, 90, '', 1);
INSERT INTO NDS2_MRR_USER.Dictionary_Column_Settings (Id, Table_Name, Table_Name_Edit, Column_Name, Caption, Rank, IsKey, IsEditable, Width, Sorting, Visible) 
  VALUES(7, 'V$SONO', 'SONO_EXT', 'S_PARENT_CODE', 'Код родительского НО', 4, 0, 0, 110, '', 1);
INSERT INTO NDS2_MRR_USER.Dictionary_Column_Settings (Id, Table_Name, Table_Name_Edit, Column_Name, Caption, Rank, IsKey, IsEditable, Width, Sorting, Visible) 
  VALUES(8, 'V$SONO', 'SONO_EXT', 'REGION_CODE', 'Регион', 5, 0, 0, 80, '', 1);

INSERT INTO NDS2_MRR_USER.Dictionary_Column_Settings (Id, Table_Name, Table_Name_Edit, Column_Name, Caption, Rank, IsKey, IsEditable, Width, Sorting, Visible) 
  VALUES(10, 'CONTROL_RATIO_TYPE', 'CONTROL_RATIO_TYPE', 'CODE', 'Официальный номер КС', 20, 1, 0, 75, 'Asc', 1);
INSERT INTO NDS2_MRR_USER.Dictionary_Column_Settings (Id, Table_Name, Table_Name_Edit, Column_Name, Caption, Rank, IsKey, IsEditable, Width, Sorting, Visible) 
  VALUES(11, 'CONTROL_RATIO_TYPE', 'CONTROL_RATIO_TYPE', 'FORMULATION', 'Формулировка нарушений', 30, 0, 0, 150, '', 1);
INSERT INTO NDS2_MRR_USER.Dictionary_Column_Settings (Id, Table_Name, Table_Name_Edit, Column_Name, Caption, Rank, IsKey, IsEditable, Width, Sorting, Visible) 
  VALUES(12, 'CONTROL_RATIO_TYPE', 'CONTROL_RATIO_TYPE', 'CALCULATION_LEFT_SIDE', 'Формула расчета - левая часть', 40, 0, 0, 150, '', 1);
INSERT INTO NDS2_MRR_USER.Dictionary_Column_Settings (Id, Table_Name, Table_Name_Edit, Column_Name, Caption, Rank, IsKey, IsEditable, Width, Sorting, Visible) 
  VALUES(13, 'CONTROL_RATIO_TYPE', 'CONTROL_RATIO_TYPE', 'CALCULATION_RIGHT_SIDE', 'Формула расчета - правая часть', 50, 0, 0, 150, '', 1);
INSERT INTO NDS2_MRR_USER.Dictionary_Column_Settings (Id, Table_Name, Table_Name_Edit, Column_Name, Caption, Rank, IsKey, IsEditable, Width, Sorting, Visible) 
  VALUES(14, 'CONTROL_RATIO_TYPE', 'CONTROL_RATIO_TYPE', 'DESCRIPTION', 'Описание соотношения', 60, 0, 0, 200, '', 1);
INSERT INTO NDS2_MRR_USER.Dictionary_Column_Settings (Id, Table_Name, Table_Name_Edit, Column_Name, Caption, Rank, IsKey, IsEditable, Width, Sorting, Visible) 
  VALUES(15, 'CONTROL_RATIO_TYPE', 'CONTROL_RATIO_TYPE', 'CALCULATION_OPERATOR', 'Формула расчета - знак равенства', 70, 0, 0, 75, '', 1);

INSERT INTO NDS2_MRR_USER.Dictionary_Column_Settings (Id, Table_Name, Table_Name_Edit, Column_Name, Caption, Rank, IsKey, IsEditable, Width, Sorting, Visible) 
  VALUES(20, 'V$DICT_LOGICAL_ERRORS_MAPPING', 'LOGICAL_ERRORS_MAPPING', 'error_code', 'Код ошибки', 10, 1, 0, 75, 'Asc', 1);
INSERT INTO NDS2_MRR_USER.Dictionary_Column_Settings (Id, Table_Name, Table_Name_Edit, Column_Name, Caption, Rank, IsKey, IsEditable, Width, Sorting, Visible) 
  VALUES(21, 'V$DICT_LOGICAL_ERRORS_MAPPING', 'LOGICAL_ERRORS_MAPPING', 'chapter', 'Номер раздела', 20, 0, 0, 75, '', 1);
INSERT INTO NDS2_MRR_USER.Dictionary_Column_Settings (Id, Table_Name, Table_Name_Edit, Column_Name, Caption, Rank, IsKey, IsEditable, Width, Sorting, Visible) 
  VALUES(22, 'V$DICT_LOGICAL_ERRORS_MAPPING', 'LOGICAL_ERRORS_MAPPING', 'name_check', 'Название ошибки', 30, 0, 0, 260, '', 1);
INSERT INTO NDS2_MRR_USER.Dictionary_Column_Settings (Id, Table_Name, Table_Name_Edit, Column_Name, Caption, Rank, IsKey, IsEditable, Width, Sorting, Visible) 
  VALUES(23, 'V$DICT_LOGICAL_ERRORS_MAPPING', 'LOGICAL_ERRORS_MAPPING', 'descreption', 'Отображаемое название ошибки', 40, 0, 1, 260, '', 1);
INSERT INTO NDS2_MRR_USER.Dictionary_Column_Settings (Id, Table_Name, Table_Name_Edit, Column_Name, Caption, Rank, IsKey, IsEditable, Width, Sorting, Visible) 
  VALUES(24, 'V$DICT_LOGICAL_ERRORS_MAPPING', 'LOGICAL_ERRORS_MAPPING', 'line_numbers', 'Перечень атрибутов записи о СФ, к которым относится ошибка', 50, 0, 0, 100, '', 1);

INSERT INTO NDS2_MRR_USER.Dictionary_Column_Settings (Id, Table_Name, Table_Name_Edit, Column_Name, Caption, Rank, IsKey, IsEditable, Width, Sorting, Visible) 
  VALUES(30, 'FEDERAL_DISTRICT', 'FEDERAL_DISTRICT', 'district_id', 'district_id', 10, 1, 0, 75, '', 0);
INSERT INTO NDS2_MRR_USER.Dictionary_Column_Settings (Id, Table_Name, Table_Name_Edit, Column_Name, Caption, Rank, IsKey, IsEditable, Width, Sorting, Visible) 
  VALUES(31, 'FEDERAL_DISTRICT', 'FEDERAL_DISTRICT', 'description', 'Название региона', 20, 0, 0, 250, '', 1);
  
commit;