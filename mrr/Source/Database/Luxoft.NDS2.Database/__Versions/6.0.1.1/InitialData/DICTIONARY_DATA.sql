﻿
truncate table NDS2_MRR_USER.DICT_TAX_PERIOD;
insert into NDS2_MRR_USER.DICT_TAX_PERIOD VALUES('01', 'январь');
insert into NDS2_MRR_USER.DICT_TAX_PERIOD VALUES('02', 'февраль');
insert into NDS2_MRR_USER.DICT_TAX_PERIOD VALUES('03', 'март');
insert into NDS2_MRR_USER.DICT_TAX_PERIOD VALUES('04', 'апрель');
insert into NDS2_MRR_USER.DICT_TAX_PERIOD VALUES('05', 'май');
insert into NDS2_MRR_USER.DICT_TAX_PERIOD VALUES('06', 'июнь');
insert into NDS2_MRR_USER.DICT_TAX_PERIOD VALUES('07', 'июль');
insert into NDS2_MRR_USER.DICT_TAX_PERIOD VALUES('08', 'август');
insert into NDS2_MRR_USER.DICT_TAX_PERIOD VALUES('09', 'сентябрь');
insert into NDS2_MRR_USER.DICT_TAX_PERIOD VALUES('10', 'октябрь');
insert into NDS2_MRR_USER.DICT_TAX_PERIOD VALUES('11', 'ноябрь');
insert into NDS2_MRR_USER.DICT_TAX_PERIOD VALUES('12', 'декабрь');
insert into NDS2_MRR_USER.DICT_TAX_PERIOD VALUES('21', '1 кв.');
insert into NDS2_MRR_USER.DICT_TAX_PERIOD VALUES('22', '2 кв.');
insert into NDS2_MRR_USER.DICT_TAX_PERIOD VALUES('23', '3 кв.');
insert into NDS2_MRR_USER.DICT_TAX_PERIOD VALUES('24', '4 кв.');
insert into NDS2_MRR_USER.DICT_TAX_PERIOD VALUES('51', '1 кв.');
insert into NDS2_MRR_USER.DICT_TAX_PERIOD VALUES('54', '2 кв.');
insert into NDS2_MRR_USER.DICT_TAX_PERIOD VALUES('55', '3 кв.');
insert into NDS2_MRR_USER.DICT_TAX_PERIOD VALUES('56', '4 кв.');
insert into NDS2_MRR_USER.DICT_TAX_PERIOD VALUES('71', 'январь');
insert into NDS2_MRR_USER.DICT_TAX_PERIOD VALUES('72', 'февраль');
insert into NDS2_MRR_USER.DICT_TAX_PERIOD VALUES('73', 'март');
insert into NDS2_MRR_USER.DICT_TAX_PERIOD VALUES('74', 'апрель');
insert into NDS2_MRR_USER.DICT_TAX_PERIOD VALUES('75', 'май');
insert into NDS2_MRR_USER.DICT_TAX_PERIOD VALUES('76', 'июнь');
insert into NDS2_MRR_USER.DICT_TAX_PERIOD VALUES('77', 'июль');
insert into NDS2_MRR_USER.DICT_TAX_PERIOD VALUES('78', 'август');
insert into NDS2_MRR_USER.DICT_TAX_PERIOD VALUES('79', 'сентябрь');
insert into NDS2_MRR_USER.DICT_TAX_PERIOD VALUES('80', 'октябрь');
insert into NDS2_MRR_USER.DICT_TAX_PERIOD VALUES('81', 'ноябрь');
insert into NDS2_MRR_USER.DICT_TAX_PERIOD VALUES('82', 'декабрь');
commit;

truncate table NDS2_MRR_USER.doc_type;
INSERT INTO NDS2_MRR_USER.doc_type VALUES(1, 'Автотребование по СФ');
INSERT INTO NDS2_MRR_USER.doc_type VALUES(2, 'Автоистребование по ст. 93');
INSERT INTO NDS2_MRR_USER.doc_type VALUES(3, 'Автоистребование по ст. 93.1');
INSERT INTO NDS2_MRR_USER.doc_type VALUES(5, 'Автотребование по КС');
commit;

truncate table NDS2_MRR_USER.DOC_STATUS;
insert into NDS2_MRR_USER.DOC_STATUS values(1, 1, 'Создано');
insert into NDS2_MRR_USER.DOC_STATUS values(2, 1, 'Передано в СЭОД');
insert into NDS2_MRR_USER.DOC_STATUS values(3, 1, 'Ошибка отправки');
insert into NDS2_MRR_USER.DOC_STATUS values(4, 1, 'Отправлено налогоплательщику');
insert into NDS2_MRR_USER.DOC_STATUS values(5, 1, 'Получено налогоплательщиком');
insert into NDS2_MRR_USER.DOC_STATUS values(10, 1, 'Закрыто');

insert into NDS2_MRR_USER.DOC_STATUS values(1, 2, 'Создано');
insert into NDS2_MRR_USER.DOC_STATUS values(2, 2, 'Передано в СЭОД');
insert into NDS2_MRR_USER.DOC_STATUS values(3, 2, 'Ошибка отправки');
insert into NDS2_MRR_USER.DOC_STATUS values(4, 2, 'Отправлено налогоплательщику');
insert into NDS2_MRR_USER.DOC_STATUS values(5, 2, 'Получено налогоплательщиком');
insert into NDS2_MRR_USER.DOC_STATUS values(10, 2, 'Закрыто');

insert into NDS2_MRR_USER.DOC_STATUS values(1, 3, 'Создано');
insert into NDS2_MRR_USER.DOC_STATUS values(2, 3, 'Передано в СЭОД');
insert into NDS2_MRR_USER.DOC_STATUS values(3, 3, 'Ошибка отправки');
insert into NDS2_MRR_USER.DOC_STATUS values(4, 3, 'Отправлено налогоплательщику');
insert into NDS2_MRR_USER.DOC_STATUS values(5, 3, 'Получено налогоплательщиком');
insert into NDS2_MRR_USER.DOC_STATUS values(6, 3, 'Отправлено в НО-исполнитель');
insert into NDS2_MRR_USER.DOC_STATUS values(7, 3, 'Получено в НО-исполнителем');
insert into NDS2_MRR_USER.DOC_STATUS values(10, 3, 'Закрыто');

insert into NDS2_MRR_USER.DOC_STATUS values(1, 4, 'Создано');
insert into NDS2_MRR_USER.DOC_STATUS values(2, 4, 'Передано в СЭОД');
insert into NDS2_MRR_USER.DOC_STATUS values(3, 4, 'Ошибка отправки');
insert into NDS2_MRR_USER.DOC_STATUS values(4, 4, 'Отправлено налогоплательщику');
insert into NDS2_MRR_USER.DOC_STATUS values(5, 4, 'Получено налогоплательщиком');
insert into NDS2_MRR_USER.DOC_STATUS values(10, 4, 'Закрыто');

insert into NDS2_MRR_USER.DOC_STATUS values(1, 5, 'Создано');
insert into NDS2_MRR_USER.DOC_STATUS values(2, 5, 'Передано в СЭОД');
insert into NDS2_MRR_USER.DOC_STATUS values(3, 5, 'Ошибка отправки');
insert into NDS2_MRR_USER.DOC_STATUS values(4, 5, 'Отправлено налогоплательщику');
insert into NDS2_MRR_USER.DOC_STATUS values(5, 5, 'Получено налогоплательщиком');
insert into NDS2_MRR_USER.DOC_STATUS values(10, 5, 'Закрыто');

commit;

truncate table NDS2_MRR_USER.CONFIGURATION;
INSERT INTO NDS2_MRR_USER.CONFIGURATION (PARAMETER, VALUE, DEFAULT_VALUE, DESCRIPTION) values ('restrict_by_region', 'N', 'N', 'Признак ограничения ответственности аналитиков и согласующих по регионам');
INSERT INTO NDS2_MRR_USER.CONFIGURATION (PARAMETER, VALUE, DEFAULT_VALUE, DESCRIPTION) values ('auto_approve_selections', 'N', 'N', 'Признак автосогласования выборок');
INSERT INTO NDS2_MRR_USER.CONFIGURATION (PARAMETER, VALUE, DEFAULT_VALUE, DESCRIPTION) values ('auto_send_selections', 'N', 'N', 'Признак автоотправки выборок');
INSERT INTO NDS2_MRR_USER.CONFIGURATION (PARAMETER, VALUE, DEFAULT_VALUE, DESCRIPTION) values ('period_forming_autoselection', '3', '3', 'Период формирования автовыборок');
INSERT INTO NDS2_MRR_USER.CONFIGURATION (PARAMETER, VALUE, DEFAULT_VALUE, DESCRIPTION) values ('period_sending_claim', '3', '3', 'Период отправки в СЭОД автотребований/автоистребований');
INSERT INTO NDS2_MRR_USER.CONFIGURATION (PARAMETER, VALUE, DEFAULT_VALUE, DESCRIPTION) values ('max_countrec_autoclaim', '5000', '5000', 'Максимальное количество записей о СФ в автотребовании');
INSERT INTO NDS2_MRR_USER.CONFIGURATION (PARAMETER, VALUE, DEFAULT_VALUE, DESCRIPTION) values ('max_countrec_autooverclaim', '99', '99','Максимальное количество записей о СФ в автоистребовании');
INSERT INTO NDS2_MRR_USER.CONFIGURATION (PARAMETER, VALUE, DEFAULT_VALUE, DESCRIPTION) values ('timeout_claim_delivery', '8', '8', 'Время ожидания вручения автотребования/автоистребования');
INSERT INTO NDS2_MRR_USER.CONFIGURATION (PARAMETER, VALUE, DEFAULT_VALUE, DESCRIPTION) values ('timeout_answer_for_autoclaim', '5', '5', 'Время ожидания ответа на автотребование');
INSERT INTO NDS2_MRR_USER.CONFIGURATION (PARAMETER, VALUE, DEFAULT_VALUE, DESCRIPTION) values ('timeout_answer_for_autooverclaim', '5', '5', 'Время ожидания ответа на автоистребование');
INSERT INTO NDS2_MRR_USER.CONFIGURATION (PARAMETER, VALUE, DEFAULT_VALUE, DESCRIPTION) values ('timeout_input_answer_for_autooverclaim', '1', '1', 'Время ожидания ввода ответа на автотребование/автоистребование');
INSERT INTO NDS2_MRR_USER.CONFIGURATION (PARAMETER, VALUE, DEFAULT_VALUE, DESCRIPTION) values ('tr_gp3_input_file_name_format', 'GP3_TR_{0:yyyy_MM_dd}.xlsx', 'GP3_TR_{0:yyyy_MM_dd}.xlsx', 'Формат имени файла с отчетом от ГП-3');
INSERT INTO NDS2_MRR_USER.CONFIGURATION (PARAMETER, VALUE, DEFAULT_VALUE, DESCRIPTION) values ('tr_gp3_input_files_location', 'C:\Work', 'C:\Work', 'Путь к папке с файлами отчетов от ГП-3');
INSERT INTO NDS2_MRR_USER.CONFIGURATION (PARAMETER, VALUE, DEFAULT_VALUE, DESCRIPTION) values ('tr_process_machine_name', 'igomanovskyi', 'igomanovskyi', 'Имя ноды, с которой будет запускаться формирование отчета');
INSERT INTO NDS2_MRR_USER.CONFIGURATION (PARAMETER, VALUE, DEFAULT_VALUE, DESCRIPTION) values ('tr_report_file_name_format', 'Технологический отчет за {0:dd MM yyyy}.xlsx', 'Технологический отчет за {0:dd MM yyyy}.xlsx', 'Формат имени файла технологического отчета');
INSERT INTO NDS2_MRR_USER.CONFIGURATION (PARAMETER, VALUE, DEFAULT_VALUE, DESCRIPTION) values ('tr_report_files_location', 'C:\Work', 'C:\Work', 'Путь к папке с технологическими отчетами');
INSERT INTO NDS2_MRR_USER.CONFIGURATION (PARAMETER, VALUE, DEFAULT_VALUE, DESCRIPTION) values ('tr_timer_period_sec', '600', '600', 'Период срабатывания таймера');
INSERT INTO NDS2_MRR_USER.CONFIGURATION (PARAMETER, VALUE, DEFAULT_VALUE, DESCRIPTION) values ('tr_report_build_time_begin', '02:00:00', '02:00:00', 'Время начала сбора информиции для отчета');
INSERT INTO NDS2_MRR_USER.CONFIGURATION (PARAMETER, VALUE, DEFAULT_VALUE, DESCRIPTION) values ('tr_report_build_time_end', '04:00:00', '04:00:00', 'Время формирования отчета, даже при недостатке данных');
INSERT INTO NDS2_MRR_USER.CONFIGURATION (PARAMETER, VALUE, DEFAULT_VALUE, DESCRIPTION) values ('number_invoice_warning', '10000', '10000', 'Количество записей для предупреждения о загрузке в карточке декларации');
COMMIT;

delete from NDS2_MRR_USER.DICT_BOOK_TYPE;
INSERT INTO NDS2_MRR_USER.DICT_BOOK_TYPE (ID, NAME, CODE) VALUES (1, 'Книга покупок', '8');
INSERT INTO NDS2_MRR_USER.DICT_BOOK_TYPE (ID, NAME, CODE) VALUES (2, 'Книга продаж', '9');


delete from NDS2_MRR_USER.DICT_NALOG_PERIODS;
INSERT INTO NDS2_MRR_USER.DICT_NALOG_PERIODS (ID, NAME, CODE) VALUES (1, '1кв.', '21');
INSERT INTO NDS2_MRR_USER.DICT_NALOG_PERIODS (ID, NAME, CODE) VALUES (2, '2кв.', '22');
INSERT INTO NDS2_MRR_USER.DICT_NALOG_PERIODS (ID, NAME, CODE) VALUES (3, '3кв.', '23');
INSERT INTO NDS2_MRR_USER.DICT_NALOG_PERIODS (ID, NAME, CODE) VALUES (4, '4кв.', '24');


delete from NDS2_MRR_USER.DICT_SELECTION_STATUS;
INSERT INTO NDS2_MRR_USER.DICT_SELECTION_STATUS (ID, NAME, RANK) VALUES (1, 'Черновик', 50);                                                                 
INSERT INTO NDS2_MRR_USER.DICT_SELECTION_STATUS (ID, NAME, RANK) VALUES (2, 'На доработке', 40);                                                               
INSERT INTO NDS2_MRR_USER.DICT_SELECTION_STATUS (ID, NAME, RANK) VALUES (3, 'На согласовании', 30);
INSERT INTO NDS2_MRR_USER.DICT_SELECTION_STATUS (ID, NAME, RANK) VALUES (4, 'На отправке', 60);
INSERT INTO NDS2_MRR_USER.DICT_SELECTION_STATUS (ID, NAME, RANK) VALUES (5, 'Отклонено', 70);
INSERT INTO NDS2_MRR_USER.DICT_SELECTION_STATUS (ID, NAME, RANK) VALUES (6, 'Согласовано', 80);
INSERT INTO NDS2_MRR_USER.DICT_SELECTION_STATUS (ID, NAME, RANK) VALUES (7, 'Удалено', 90);   
INSERT INTO NDS2_MRR_USER.DICT_SELECTION_STATUS (ID, NAME, RANK) VALUES (8, 'Загружается', 20);
INSERT INTO NDS2_MRR_USER.DICT_SELECTION_STATUS (ID, NAME, RANK) VALUES (9, 'Ошибка загрузки', 10);
INSERT INTO NDS2_MRR_USER.DICT_SELECTION_STATUS (ID, NAME, RANK) VALUES (10, 'Отправлено', 62);
INSERT INTO NDS2_MRR_USER.DICT_SELECTION_STATUS (ID, NAME, RANK) VALUES (11, 'Ошибка отправки', 63);
INSERT INTO NDS2_MRR_USER.DICT_SELECTION_STATUS (ID, NAME, RANK) VALUES (12, 'Аннулирована', 100);
INSERT INTO NDS2_MRR_USER.DICT_SELECTION_STATUS (ID, NAME, RANK) VALUES (13, 'На отправке', 61);

delete from NDS2_MRR_USER.DICT_SELECTION_TYPE;
INSERT INTO NDS2_MRR_USER.DICT_SELECTION_TYPE (ID, S_CODE, S_NAME) VALUES (1, 0, 'Ручная');                                                                 
INSERT INTO NDS2_MRR_USER.DICT_SELECTION_TYPE (ID, S_CODE, S_NAME) VALUES (2, 1, 'Автоматическая');

delete from NDS2_MRR_USER.SELECTION_TRANSITION;
--Черновик
INSERT INTO NDS2_MRR_USER.SELECTION_TRANSITION values(1, 1, 7, 'Выборка.Удалить');
INSERT INTO NDS2_MRR_USER.SELECTION_TRANSITION values(2, 1, 8, '');
INSERT INTO NDS2_MRR_USER.SELECTION_TRANSITION values(3, 1, 3, 'Выборка.На_Согласование');
--На доработке
INSERT INTO NDS2_MRR_USER.SELECTION_TRANSITION values(4, 2, 3, 'Выборка.На_Согласование');
INSERT INTO NDS2_MRR_USER.SELECTION_TRANSITION values(5, 2, 7, 'Выборка.Удалить');
INSERT INTO NDS2_MRR_USER.SELECTION_TRANSITION values(6, 2, 8, '');
--На согласовании
INSERT INTO NDS2_MRR_USER.SELECTION_TRANSITION values(7, 3, 5, 'Выборка.Отклонить');
INSERT INTO NDS2_MRR_USER.SELECTION_TRANSITION values(8, 3, 2, 'Выборка.Отправить_на_доработку');
INSERT INTO NDS2_MRR_USER.SELECTION_TRANSITION values(9, 3, 6, 'Выборка.Согласовать');
INSERT INTO NDS2_MRR_USER.SELECTION_TRANSITION values(10, 3, 7, 'Выборка.Удалить');
--согласована
INSERT INTO NDS2_MRR_USER.SELECTION_TRANSITION values(11, 6, 4, 'Выборка.Отправить_в_СЭОД');
INSERT INTO NDS2_MRR_USER.SELECTION_TRANSITION values(12, 6, 7, 'Выборка.Удалить');
--Загрузка
INSERT INTO NDS2_MRR_USER.SELECTION_TRANSITION values(13, 8, 9, 'salt 1');
INSERT INTO NDS2_MRR_USER.SELECTION_TRANSITION values(14, 9, 8, 'salt 2');
--подготовка к отправке
INSERT INTO NDS2_MRR_USER.SELECTION_TRANSITION values(15, 4, 10, 'salt 3');
INSERT INTO NDS2_MRR_USER.SELECTION_TRANSITION values(16, 4, 11, 'salt 4');

delete from NDS2_MRR_USER.DICT_FILTER_TYPES;
INSERT INTO NDS2_MRR_USER.DICT_FILTER_TYPES (ID, CD, FILTER_TYPE, VALUE_TYPE_CODE, LOOKUP_TABLE_NAME, INTERNAL_NAME, FIR_INTERNAL_NAME, IS_REQUIRED) VALUES ('1',  '0', 'Регион',										'8', 'v$ssrf',					'Region',					'REGION',						1);
INSERT INTO NDS2_MRR_USER.DICT_FILTER_TYPES (ID, CD, FILTER_TYPE, VALUE_TYPE_CODE, LOOKUP_TABLE_NAME, INTERNAL_NAME, FIR_INTERNAL_NAME, IS_REQUIRED) VALUES ('2',  '0', 'Инспекция',									'9', 'v$sono',					'TaxOrgan',					'IFNS_NUMBER',					0);
INSERT INTO NDS2_MRR_USER.DICT_FILTER_TYPES (ID, CD, FILTER_TYPE, VALUE_TYPE_CODE, LOOKUP_TABLE_NAME, INTERNAL_NAME, FIR_INTERNAL_NAME, IS_REQUIRED) VALUES ('3',  '0', 'ИНН НП',										'0', '',						'TaxPayerInn',				'INN',							0);
INSERT INTO NDS2_MRR_USER.DICT_FILTER_TYPES (ID, CD, FILTER_TYPE, VALUE_TYPE_CODE, LOOKUP_TABLE_NAME, INTERNAL_NAME, FIR_INTERNAL_NAME, IS_REQUIRED) VALUES ('4',  '0', 'Наименование НП',								'3', '',						'TaxPayerName',				'NP_NAME',						0);
INSERT INTO NDS2_MRR_USER.DICT_FILTER_TYPES (ID, CD, FILTER_TYPE, VALUE_TYPE_CODE, LOOKUP_TABLE_NAME, INTERNAL_NAME, FIR_INTERNAL_NAME, IS_REQUIRED) VALUES ('5',  '0', 'СУР НП',										'6', 'DICT_SUR',				'Sur',						'NP_SUR',						0);
INSERT INTO NDS2_MRR_USER.DICT_FILTER_TYPES (ID, CD, FILTER_TYPE, VALUE_TYPE_CODE, LOOKUP_TABLE_NAME, INTERNAL_NAME, FIR_INTERNAL_NAME, IS_REQUIRED) VALUES ('6',  '0', 'Отчетный период',								'6', 'DICT_NALOG_PERIODS',		'TaxPeriod',				'PERIOD',						0);  
INSERT INTO NDS2_MRR_USER.DICT_FILTER_TYPES (ID, CD, FILTER_TYPE, VALUE_TYPE_CODE, LOOKUP_TABLE_NAME, INTERNAL_NAME, FIR_INTERNAL_NAME, IS_REQUIRED) VALUES ('7',  '0', 'Дата подачи декларации',						'5', '',						'DeclarationDate',			'DECLARATION_SUBMISSION_DATE',	0); 
INSERT INTO NDS2_MRR_USER.DICT_FILTER_TYPES (ID, CD, FILTER_TYPE, VALUE_TYPE_CODE, LOOKUP_TABLE_NAME, INTERNAL_NAME, FIR_INTERNAL_NAME, IS_REQUIRED) VALUES ('8',  '0', 'Признак НД',									'1', 'DICT_DECLARATION_SIGN',	'DiscrepancyType',			'ND_PRIS',						0);
INSERT INTO NDS2_MRR_USER.DICT_FILTER_TYPES (ID, CD, FILTER_TYPE, VALUE_TYPE_CODE, LOOKUP_TABLE_NAME, INTERNAL_NAME, FIR_INTERNAL_NAME, IS_REQUIRED) VALUES ('9',  '0', 'Общая сумма расхождений НД',					'4', '',						'MismatchAmntTotal',		'MISMATCH_COMMON_SUM',			0); 
INSERT INTO NDS2_MRR_USER.DICT_FILTER_TYPES (ID, CD, FILTER_TYPE, VALUE_TYPE_CODE, LOOKUP_TABLE_NAME, INTERNAL_NAME, FIR_INTERNAL_NAME, IS_REQUIRED) VALUES ('10', '0', 'Минимальная сумма расхождения',				'4', '',						'MismatchAmntMin',			'MISMATCH_MIN_SUM',				0); 
INSERT INTO NDS2_MRR_USER.DICT_FILTER_TYPES (ID, CD, FILTER_TYPE, VALUE_TYPE_CODE, LOOKUP_TABLE_NAME, INTERNAL_NAME, FIR_INTERNAL_NAME, IS_REQUIRED) VALUES ('11', '0', 'Максимальная сумма расхождения',				'4', '',						'MismatchAmntMax',			'MISMATCH_MAX_SUM',				0); 
INSERT INTO NDS2_MRR_USER.DICT_FILTER_TYPES (ID, CD, FILTER_TYPE, VALUE_TYPE_CODE, LOOKUP_TABLE_NAME, INTERNAL_NAME, FIR_INTERNAL_NAME, IS_REQUIRED) VALUES ('12', '0', 'Средняя сумма расхождения',					'4', '',						'MismatchAmntAvg',			'MISMATCH_AVG_SUM',				0); 
INSERT INTO NDS2_MRR_USER.DICT_FILTER_TYPES (ID, CD, FILTER_TYPE, VALUE_TYPE_CODE, LOOKUP_TABLE_NAME, INTERNAL_NAME, FIR_INTERNAL_NAME, IS_REQUIRED) VALUES ('13', '0', 'Общее количество расхождений по НД',			'7', '',						'MismatchCountTotal',		'MISMATCH_COMMON_CNT',			0); 
INSERT INTO NDS2_MRR_USER.DICT_FILTER_TYPES (ID, CD, FILTER_TYPE, VALUE_TYPE_CODE, LOOKUP_TABLE_NAME, INTERNAL_NAME, FIR_INTERNAL_NAME, IS_REQUIRED) VALUES ('14', '0', 'Сумма расхождений по книге покупок',			'7', '',						'MismatchCountBuyBook',		'MISMATCH_BUYBOOK_CNT',			0); 
INSERT INTO NDS2_MRR_USER.DICT_FILTER_TYPES (ID, CD, FILTER_TYPE, VALUE_TYPE_CODE, LOOKUP_TABLE_NAME, INTERNAL_NAME, FIR_INTERNAL_NAME, IS_REQUIRED) VALUES ('15', '0', 'Сумма расхождений по книге продаж',			'7', '',						'MismatchCountSellBook',	'MISMATCH_SELLBOOK_CNT',		0); 
INSERT INTO NDS2_MRR_USER.DICT_FILTER_TYPES (ID, CD, FILTER_TYPE, VALUE_TYPE_CODE, LOOKUP_TABLE_NAME, INTERNAL_NAME, FIR_INTERNAL_NAME, IS_REQUIRED) VALUES ('16', '0', 'Общая сумма ПВП по НД',						'7', '',						'DeclarationPvpSumTotal',	'DECLARATION_COMMON_PVPSUM',	0); 
INSERT INTO NDS2_MRR_USER.DICT_FILTER_TYPES (ID, CD, FILTER_TYPE, VALUE_TYPE_CODE, LOOKUP_TABLE_NAME, INTERNAL_NAME, FIR_INTERNAL_NAME, IS_REQUIRED) VALUES ('17', '0', 'Минимальная сумма ПВП расхождения в НД',		'7', '',						'DeclarationPvpSumMin',		'DECLARATION_MIN_PVPSUM',		0); 
INSERT INTO NDS2_MRR_USER.DICT_FILTER_TYPES (ID, CD, FILTER_TYPE, VALUE_TYPE_CODE, LOOKUP_TABLE_NAME, INTERNAL_NAME, FIR_INTERNAL_NAME, IS_REQUIRED) VALUES ('18', '0', 'Максимальная сумма ПВП расхождения в НД',		'7', '',						'DeclarationPvpSumMax',		'DECLARATION_MAX_PVPSUM',		0); 
INSERT INTO NDS2_MRR_USER.DICT_FILTER_TYPES (ID, CD, FILTER_TYPE, VALUE_TYPE_CODE, LOOKUP_TABLE_NAME, INTERNAL_NAME, FIR_INTERNAL_NAME, IS_REQUIRED) VALUES ('19', '0', 'Сумма ПВП по книге покупок',					'7', '',						'BuyBookPvpSum',			'BUYBOOK_PVPSUM',				0); 
INSERT INTO NDS2_MRR_USER.DICT_FILTER_TYPES (ID, CD, FILTER_TYPE, VALUE_TYPE_CODE, LOOKUP_TABLE_NAME, INTERNAL_NAME, FIR_INTERNAL_NAME, IS_REQUIRED) VALUES ('20', '0', 'Сумма ПВП по книге продаж',					'7', '',						'SellBookPvpSum',			'SELLBOOK_PVPSUM',				0); 
INSERT INTO NDS2_MRR_USER.DICT_FILTER_TYPES (ID, CD, FILTER_TYPE, VALUE_TYPE_CODE, LOOKUP_TABLE_NAME, INTERNAL_NAME, FIR_INTERNAL_NAME, IS_REQUIRED) VALUES ('21', '0', 'Вид расхождения',								'6', 'DICT_DISCREPANCY_TYPE',	'DiscrepancyKind',			'MISMATCH_TYPE',				0); 
INSERT INTO NDS2_MRR_USER.DICT_FILTER_TYPES (ID, CD, FILTER_TYPE, VALUE_TYPE_CODE, LOOKUP_TABLE_NAME, INTERNAL_NAME, FIR_INTERNAL_NAME, IS_REQUIRED) VALUES ('22', '0', 'Сумма расхождения',							'4', '',						'DiscrepancySum',			'MISMATCH_SUM',					0); 
INSERT INTO NDS2_MRR_USER.DICT_FILTER_TYPES (ID, CD, FILTER_TYPE, VALUE_TYPE_CODE, LOOKUP_TABLE_NAME, INTERNAL_NAME, FIR_INTERNAL_NAME, IS_REQUIRED) VALUES ('23', '0', 'ПВП расхождения',								'7', '',						'DiscrepancyPvp',			'MISMATCH_PVP',					0); 
INSERT INTO NDS2_MRR_USER.DICT_FILTER_TYPES (ID, CD, FILTER_TYPE, VALUE_TYPE_CODE, LOOKUP_TABLE_NAME, INTERNAL_NAME, FIR_INTERNAL_NAME, IS_REQUIRED) VALUES ('24', '0', 'Статус расхождения',							'1', 'dict_discrep_filter_status', 'DiscrepancyStatus',		'MISMATCH_STATUS',				0); 
INSERT INTO NDS2_MRR_USER.DICT_FILTER_TYPES (ID, CD, FILTER_TYPE, VALUE_TYPE_CODE, LOOKUP_TABLE_NAME, INTERNAL_NAME, FIR_INTERNAL_NAME, IS_REQUIRED) VALUES ('25', '0', 'Дата расхождения',								'5', '',						'MismatchDate',				'MISMATCH_DATE',				0); 
commit;


delete from NDS2_MRR_USER.DICT_ACTIONS;
INSERT INTO NDS2_MRR_USER.DICT_ACTIONS (ID, ACTION_NAME) VALUES (1, 'Создание выборки');                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
INSERT INTO NDS2_MRR_USER.DICT_ACTIONS (ID, ACTION_NAME) VALUES (2, 'Удаление выборки');                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
INSERT INTO NDS2_MRR_USER.DICT_ACTIONS (ID, ACTION_NAME) VALUES (3, 'Изменение статуса');      
INSERT INTO NDS2_MRR_USER.DICT_ACTIONS (ID, ACTION_NAME) VALUES (4, 'Обновление выборки'); 
INSERT INTO NDS2_MRR_USER.DICT_ACTIONS (ID, ACTION_NAME) VALUES (5, 'Изменение состава расхождений');                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
commit;

delete from NDS2_MRR_USER.DICT_DECLARATION_SIGN;
INSERT INTO NDS2_MRR_USER.DICT_DECLARATION_SIGN (s_code, s_name) VALUES ('1', 'К уплате');
INSERT INTO NDS2_MRR_USER.DICT_DECLARATION_SIGN (s_code, s_name) VALUES ('2', 'К возмещению');
commit;

delete from NDS2_MRR_USER.DICT_DISCREPANCY_TYPE;
INSERT INTO NDS2_MRR_USER.DICT_DISCREPANCY_TYPE (s_code, s_name, description) VALUES ('1', 'Разрыв', 'Разрыв');
INSERT INTO NDS2_MRR_USER.DICT_DISCREPANCY_TYPE (s_code, s_name, description) VALUES ('2', 'НС', 'Неточное сопоставление');
INSERT INTO NDS2_MRR_USER.DICT_DISCREPANCY_TYPE (s_code, s_name, description) VALUES ('3', 'Валюта', 'Расхождение по валюте');
INSERT INTO NDS2_MRR_USER.DICT_DISCREPANCY_TYPE (s_code, s_name, description) VALUES ('4', 'НДС', 'Проверка НДС');
INSERT INTO NDS2_MRR_USER.DICT_DISCREPANCY_TYPE (s_code, s_name, description) VALUES ('5', 'Разрыв МС', 'Разрыв из-за множественного сопоставления');
commit;

delete from NDS2_MRR_USER.DICT_DISCREPANCY_STATUS;
INSERT INTO NDS2_MRR_USER.DICT_DISCREPANCY_STATUS VALUES (1, 'Открыто', 10);
INSERT INTO NDS2_MRR_USER.DICT_DISCREPANCY_STATUS VALUES (2, 'Закрыто', 20);
commit;

delete from NDS2_MRR_USER.dict_discrep_filter_status;
INSERT INTO NDS2_MRR_USER.dict_discrep_filter_status VALUES (1, 'В работе', 10);
INSERT INTO NDS2_MRR_USER.dict_discrep_filter_status VALUES (2, 'Не обработано', 20);
commit;

delete from NDS2_MRR_USER.dict_sel2discr_status_rel;
--В работе
INSERT INTO NDS2_MRR_USER.dict_sel2discr_status_rel values(1, 2);
INSERT INTO NDS2_MRR_USER.dict_sel2discr_status_rel values(2, 2);
INSERT INTO NDS2_MRR_USER.dict_sel2discr_status_rel values(3, 2);
--Согласовано
INSERT INTO NDS2_MRR_USER.dict_sel2discr_status_rel values(6, 3); 
--Отправлено
INSERT INTO NDS2_MRR_USER.dict_sel2discr_status_rel values(4, 4); 
--Не обработано
INSERT INTO NDS2_MRR_USER.dict_sel2discr_status_rel values(5, 1); 
INSERT INTO NDS2_MRR_USER.dict_sel2discr_status_rel values(7, 1); 
INSERT INTO NDS2_MRR_USER.dict_sel2discr_status_rel values(8, 1);
INSERT INTO NDS2_MRR_USER.dict_sel2discr_status_rel values(9, 1);
commit;

INSERT INTO NDS2_MRR_USER.REQUEST_STATUS VALUES (0, 'Запрос получен МС');
INSERT INTO NDS2_MRR_USER.REQUEST_STATUS VALUES (1, 'Запрос обрабатывается на стороне МС');
INSERT INTO NDS2_MRR_USER.REQUEST_STATUS VALUES (2, 'Запрос выполнен на стороне МС');
INSERT INTO NDS2_MRR_USER.REQUEST_STATUS VALUES (3, 'Результаты запроса обработаны на стороне МРР');
INSERT INTO NDS2_MRR_USER.REQUEST_STATUS VALUES (4, 'Запрос на получение данных от МРР');
INSERT INTO NDS2_MRR_USER.REQUEST_STATUS VALUES (8, 'Выполнение запросов в ночном режиме недоступно');
INSERT INTO NDS2_MRR_USER.REQUEST_STATUS VALUES (9, 'Ошибка выполнения запроса на стороне МС');
commit;

delete from NDS2_MRR_USER.DICT_DISCREPANCY_KIND;
INSERT INTO NDS2_MRR_USER.DICT_DISCREPANCY_KIND (id, code, name) VALUES (1, '1', 'Точное сопоставление');
INSERT INTO NDS2_MRR_USER.DICT_DISCREPANCY_KIND (id, code, name) VALUES (2, '2', 'Неточное сопоставление');
INSERT INTO NDS2_MRR_USER.DICT_DISCREPANCY_KIND (id, code, name) VALUES (3, '3', 'Разрыв');
commit;

truncate table NDS2_MRR_USER.Dict_SUR;
INSERT INTO NDS2_MRR_USER.dict_sur values (0, 'Средний риск', -256);
INSERT INTO NDS2_MRR_USER.dict_sur values (1, 'Высокий риск', -65536);
INSERT INTO NDS2_MRR_USER.dict_sur values (2, 'Средний риск', -256);
INSERT INTO NDS2_MRR_USER.dict_sur values (3, 'Низкий риск', -6632142);
INSERT INTO NDS2_MRR_USER.dict_sur values (4, 'Неактуальная декларация', -8943463);
INSERT INTO NDS2_MRR_USER.dict_sur values (5, 'Аннулированная декларация', -7722014);
INSERT INTO NDS2_MRR_USER.dict_sur values (6, 'Уточненная декларация с суммой налога равной нулю (строка 040,050 = 0) или декларация с суммой налога к уплате (строка 040>0)', -14774017);
commit;


delete from NDS2_MRR_USER.DICT_AUTOSELECTION_FILTER;
INSERT INTO NDS2_MRR_USER.DICT_AUTOSELECTION_FILTER (ID, FILTER_TYPE, VALUE_TYPE_CODE, LOOKUP_TABLE_NAME, INTERNAL_NAME, IS_REQUIRED) VALUES ('1',  'Регион стороны, выставившей СФ',				'8', 'v$ssrf',					'REGION_SELLER',				0);
INSERT INTO NDS2_MRR_USER.DICT_AUTOSELECTION_FILTER (ID, FILTER_TYPE, VALUE_TYPE_CODE, LOOKUP_TABLE_NAME, INTERNAL_NAME, IS_REQUIRED) VALUES ('2',  'Регион стороны, получившей СФ',				'8', 'v$ssrf',					'REGION_BUYER',					0);
INSERT INTO NDS2_MRR_USER.DICT_AUTOSELECTION_FILTER (ID, FILTER_TYPE, VALUE_TYPE_CODE, LOOKUP_TABLE_NAME, INTERNAL_NAME, IS_REQUIRED) VALUES ('3',  'Инспекция стороны, выставившей СФ',			'9', 'v$sono',					'IFNS_SELLER',					0);
INSERT INTO NDS2_MRR_USER.DICT_AUTOSELECTION_FILTER (ID, FILTER_TYPE, VALUE_TYPE_CODE, LOOKUP_TABLE_NAME, INTERNAL_NAME, IS_REQUIRED) VALUES ('4',  'Инспекция стороны, получившей СФ',				'9', 'v$sono',					'IFNS_BUYER',					0);
INSERT INTO NDS2_MRR_USER.DICT_AUTOSELECTION_FILTER (ID, FILTER_TYPE, VALUE_TYPE_CODE, LOOKUP_TABLE_NAME, INTERNAL_NAME, IS_REQUIRED) VALUES ('5',  'ИНН стороны, выставившей СФ',					'3', '',						'INN_SELLER',					0);
INSERT INTO NDS2_MRR_USER.DICT_AUTOSELECTION_FILTER (ID, FILTER_TYPE, VALUE_TYPE_CODE, LOOKUP_TABLE_NAME, INTERNAL_NAME, IS_REQUIRED) VALUES ('6',  'ИНН стороны, получившей СФ',					'3', '',						'INN_BUYER',					0);  
INSERT INTO NDS2_MRR_USER.DICT_AUTOSELECTION_FILTER (ID, FILTER_TYPE, VALUE_TYPE_CODE, LOOKUP_TABLE_NAME, INTERNAL_NAME, IS_REQUIRED) VALUES ('7',  'КПП стороны, выставившей СФ',					'3', '',						'KPP_SELLER',					0); 
INSERT INTO NDS2_MRR_USER.DICT_AUTOSELECTION_FILTER (ID, FILTER_TYPE, VALUE_TYPE_CODE, LOOKUP_TABLE_NAME, INTERNAL_NAME, IS_REQUIRED) VALUES ('8',  'КПП стороны, получившей СФ',					'3', '',						'KPP_BUYER',					0);
INSERT INTO NDS2_MRR_USER.DICT_AUTOSELECTION_FILTER (ID, FILTER_TYPE, VALUE_TYPE_CODE, LOOKUP_TABLE_NAME, INTERNAL_NAME, IS_REQUIRED) VALUES ('9',  'СУР стороны, выставившей СФ',					'6', 'DICT_SUR',				'SUR_SELLER',					0); 
INSERT INTO NDS2_MRR_USER.DICT_AUTOSELECTION_FILTER (ID, FILTER_TYPE, VALUE_TYPE_CODE, LOOKUP_TABLE_NAME, INTERNAL_NAME, IS_REQUIRED) VALUES ('10', 'СУР стороны, получившей СФ',					'6', 'DICT_SUR',				'SUR_BUYER',					0); 
INSERT INTO NDS2_MRR_USER.DICT_AUTOSELECTION_FILTER (ID, FILTER_TYPE, VALUE_TYPE_CODE, LOOKUP_TABLE_NAME, INTERNAL_NAME, IS_REQUIRED) VALUES ('11', 'Вид расхождения',								'6', 'DICT_DISCREPANCY_TYPE',	'MISMATCH_TYPE',				0); 
INSERT INTO NDS2_MRR_USER.DICT_AUTOSELECTION_FILTER (ID, FILTER_TYPE, VALUE_TYPE_CODE, LOOKUP_TABLE_NAME, INTERNAL_NAME, IS_REQUIRED) VALUES ('12', 'Налоговый период',								'6', 'DICT_NALOG_PERIODS',		'PERIOD',						0);  
INSERT INTO NDS2_MRR_USER.DICT_AUTOSELECTION_FILTER (ID, FILTER_TYPE, VALUE_TYPE_CODE, LOOKUP_TABLE_NAME, INTERNAL_NAME, IS_REQUIRED) VALUES ('13', 'Признак НД',									'6', 'DICT_DECLARATION_SIGN',	'ND_PRIS',						0);
INSERT INTO NDS2_MRR_USER.DICT_AUTOSELECTION_FILTER (ID, FILTER_TYPE, VALUE_TYPE_CODE, LOOKUP_TABLE_NAME, INTERNAL_NAME, IS_REQUIRED) VALUES ('14', 'ПВП расхождения',								'7', '',						'MISMATCH_PVP',					0); 
commit;

delete from NDS2_MRR_USER.SEOD_KNP_DECISION_TYPE;
INSERT INTO NDS2_MRR_USER.SEOD_KNP_DECISION_TYPE (id, name, description) values (1, 'РешПриост', 'Решение о приостановлении операций по счетам');
INSERT INTO NDS2_MRR_USER.SEOD_KNP_DECISION_TYPE (id, name, description) values (2, 'РешПривлОтв', 'Решение о привлечении к ответственности');
INSERT INTO NDS2_MRR_USER.SEOD_KNP_DECISION_TYPE (id, name, description) values (3, 'РешОтказПривл', 'Решение об отказе в привлечении к ответственности за совершение налогового правонарушения');
INSERT INTO NDS2_MRR_USER.SEOD_KNP_DECISION_TYPE (id, name, description) values (4, 'РешВозмНДС', 'Решение о возмещении (полностью или частично) суммы НДС, заявленной к возмещению');
INSERT INTO NDS2_MRR_USER.SEOD_KNP_DECISION_TYPE (id, name, description) values (5, 'РешОтказВозмНДС', 'Решение об отказе в возмещении (полностью или частично) суммы НДС, заявленной к возмещению');
INSERT INTO NDS2_MRR_USER.SEOD_KNP_DECISION_TYPE (id, name, description) values (6, 'РешВозмНДСЗаявит', 'Решение о возмещении (полностью или частично) суммы НДС, заявленной к возмещению в заявительносм порядке');
INSERT INTO NDS2_MRR_USER.SEOD_KNP_DECISION_TYPE (id, name, description) values (7, 'РешОтказВозмНДСЗаявит', 'Решение об отказе в возмещении (полностью или частично) суммы НДС, заявленной к возмещению в заявительном порядке');
INSERT INTO NDS2_MRR_USER.SEOD_KNP_DECISION_TYPE (id, name, description) values (8, 'РешОтменВозмНДСЗаявит', 'Решение об отмене решения о возмещении (полностью или частично) суммы НДС, заявленной к возмещению в заявительном порядке');
INSERT INTO NDS2_MRR_USER.SEOD_KNP_DECISION_TYPE (id, name, description) values (9, 'РешОтменВозмНДСУточн', 'Решение об отмене решения о возмещении (полностью или частично) суммы НДС, заявленной к возмещению в заявительном порядке, в связи с представлением уточненной декларации');
commit;

truncate table NDS2_MRR_USER.Metodolog_Employee_Type;
INSERT INTO NDS2_MRR_USER.Metodolog_Employee_Type values(0, 'Analyst');
INSERT INTO NDS2_MRR_USER.Metodolog_Employee_Type values(1, 'Approver');
commit;

truncate table NDS2_MRR_USER.CFG_PARAM;
INSERT INTO NDS2_MRR_USER.CFG_PARAM (id, name, description) values(0, 'DiscrepancyPVP', 'ПВП расхождения (руб.)');
commit;

truncate table NDS2_MRR_USER.Metodolog_Region_Context;
INSERT INTO NDS2_MRR_USER.Metodolog_Region_Context values (1, 'Параметры нагрузки на инспекции');
INSERT INTO NDS2_MRR_USER.Metodolog_Region_Context values (2, 'Назначение аналитиков и согласующих на регионы и инспекции');
commit;

truncate table NDS2_MRR_USER.DICT_DISCREPANCY_STAGE;
insert into NDS2_MRR_USER.DICT_DISCREPANCY_STAGE (ID, NAME) values (0, '-');
insert into NDS2_MRR_USER.DICT_DISCREPANCY_STAGE (ID, NAME) values (1, 'Выборка');
insert into NDS2_MRR_USER.DICT_DISCREPANCY_STAGE (ID, NAME) values (2, 'Автотребование первой стороне');
insert into NDS2_MRR_USER.DICT_DISCREPANCY_STAGE (ID, NAME) values (3, 'Автотребование второй стороне');
insert into NDS2_MRR_USER.DICT_DISCREPANCY_STAGE (ID, NAME) values (4, 'Автоистребование первой стороне');
insert into NDS2_MRR_USER.DICT_DISCREPANCY_STAGE (ID, NAME) values (5, 'Автоистребование второй стороне');
insert into NDS2_MRR_USER.DICT_DISCREPANCY_STAGE (ID, NAME) values (6, 'Прочие МНК');
commit;

truncate table NDS2_MRR_USER.DICT_EXPLAIN_REPLY_TYPE;
insert into NDS2_MRR_USER.DICT_EXPLAIN_REPLY_TYPE (ID, NAME, short_name) values (1, 'Пояснение', 'Пояснение');
insert into NDS2_MRR_USER.DICT_EXPLAIN_REPLY_TYPE (ID, NAME, short_name) values (2, 'Ответ по истребованию 93', 'Ответ');
insert into NDS2_MRR_USER.DICT_EXPLAIN_REPLY_TYPE (ID, NAME, short_name) values (3, 'Ответ по истребованию 93.1', 'Ответ');
commit;

truncate table NDS2_MRR_USER.DICT_EXPLAIN_REPLY_STATUS;
insert into NDS2_MRR_USER.DICT_EXPLAIN_REPLY_STATUS (ID, NAME) values (1, 'Необработано');
insert into NDS2_MRR_USER.DICT_EXPLAIN_REPLY_STATUS (ID, NAME) values (2, 'Отправлено в МС');
insert into NDS2_MRR_USER.DICT_EXPLAIN_REPLY_STATUS (ID, NAME) values (3, 'Обработано в МС');
insert into NDS2_MRR_USER.DICT_EXPLAIN_REPLY_STATUS (ID, NAME) values (4, 'Отправлен ответ из НО-исполнителя в НО-инициатор');
insert into NDS2_MRR_USER.DICT_EXPLAIN_REPLY_STATUS (ID, NAME) values (5, 'Получен ответ НО-инициатором');
commit;
