﻿begin
  DBMS_XMLSCHEMA.deleteSchema('TAX3EXCH_NDS2_CAM_01_01.xsd', 4);
exception when others then null;
end;
/
DECLARE
C1 clob;
BEGIN
c1:= '<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:sql="urn:schemas-microsoft-com:mapping-schema" elementFormDefault="qualified" attributeFormDefault="unqualified">
  <xs:element name="Автотребование">
    <xs:annotation>
      <xs:documentation>Данные о выявленных расхождениях</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="ПризнРсхжд">
          <xs:annotation>
            <xs:documentation>Признак расхождений</xs:documentation>
          </xs:annotation>
          <xs:complexType>
            <xs:sequence>
              <xs:element name="T1.1" minOccurs="0">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element name="T1.1Стр" maxOccurs="unbounded">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element name="СвПрод" type="СвУчСдТип" minOccurs="0"/>
                          <xs:element name="СвПос" type="СвУчСдТип" minOccurs="0"/>
                        </xs:sequence>
                        <xs:attribute name="НомерПор" use="required">
                          <xs:annotation>
                            <xs:documentation>Порядковый номер записи Книги покупок (доп.листа)</xs:documentation>
                          </xs:annotation>
                          <xs:simpleType>
                            <xs:restriction base="xs:int">
                              <xs:totalDigits value="12"/>
                              <xs:minInclusive value="1"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                        <xs:attribute name="КодВидОпер" use="required">
                          <xs:annotation>
                            <xs:documentation>Код вида операции</xs:documentation>
                          </xs:annotation>
                          <xs:simpleType>
                            <xs:restriction base="xs:string">
                              <xs:length value="2"/>
                              <xs:enumeration value="01"/>
                              <xs:enumeration value="02"/>
                              <xs:enumeration value="03"/>
                              <xs:enumeration value="04"/>
                              <xs:enumeration value="05"/>
                              <xs:enumeration value="06"/>
                              <xs:enumeration value="07"/>
                              <xs:enumeration value="08"/>
                              <xs:enumeration value="09"/>
                              <xs:enumeration value="10"/>
                              <xs:enumeration value="11"/>
                              <xs:enumeration value="12"/>
                              <xs:enumeration value="13"/>
                              <xs:enumeration value="14"/>
                              <xs:enumeration value="15"/>
                              <xs:enumeration value="16"/>
                              <xs:enumeration value="17"/>
                              <xs:enumeration value="18"/>
                              <xs:enumeration value="19"/>
                              <xs:enumeration value="20"/>
                              <xs:enumeration value="21"/>
                              <xs:enumeration value="22"/>
                              <xs:enumeration value="23"/>
                              <xs:enumeration value="24"/>
                              <xs:enumeration value="25"/>
                              <xs:enumeration value="26"/>
                              <xs:enumeration value="27"/>
                              <xs:enumeration value="28"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                        <xs:attribute name="НомСчФПрод" use="required">
                          <xs:annotation>
                            <xs:documentation>Номер счета-фактуры продавца</xs:documentation>
                          </xs:annotation>
                          <xs:simpleType>
                            <xs:restriction base="xs:string">
                              <xs:minLength value="1"/>
                              <xs:maxLength value="1000"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                        <xs:attribute name="ДатаСчФПрод" type="ДатаТип" use="optional"/>
                        <xs:attribute name="НомИспрСчФ" use="optional">
                          <xs:annotation>
                            <xs:documentation>Номер исправления счета-фактуры продавца</xs:documentation>
                          </xs:annotation>
                          <xs:simpleType>
                            <xs:restriction base="xs:int">
                              <xs:totalDigits value="3"/>
                              <xs:minInclusive value="1"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                        <xs:attribute name="ДатаИспрСчФ" type="ДатаТип" use="optional"/>
                        <xs:attribute name="НомКСчФПрод" use="optional">
                          <xs:annotation>
                            <xs:documentation>Номер корректировочного счета-фактуры продавца</xs:documentation>
                          </xs:annotation>
                          <xs:simpleType>
                            <xs:restriction base="xs:string">
                              <xs:minLength value="1"/>
                              <xs:maxLength value="256"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                        <xs:attribute name="ДатаКСчФПрод" type="ДатаТип" use="optional"/>
                        <xs:attribute name="НомИспрКСчФ" use="optional">
                          <xs:annotation>
                            <xs:documentation>Номер исправления корректировочного счета-фактуры продавца</xs:documentation>
                          </xs:annotation>
                          <xs:simpleType>
                            <xs:restriction base="xs:int">
                              <xs:totalDigits value="3"/>
                              <xs:minInclusive value="1"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                        <xs:attribute name="ДатаИспрКСчФ" type="ДатаТип" use="optional"/>
                        <xs:attribute name="НомДокПдтвОпл" use="optional">
                          <xs:annotation>
                            <xs:documentation>Номер документа, подтверждающего оплату</xs:documentation>
                          </xs:annotation>
                          <xs:simpleType>
                            <xs:restriction base="xs:string">
                              <xs:minLength value="1"/>
                              <xs:maxLength value="256"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                        <xs:attribute name="ДатаДокПдтвОпл" type="ДатаТип" use="optional"/>
                        <xs:attribute name="ДатаУчТов" type="ДатаТип" use="optional"/>
                        <xs:attribute name="НомТД" use="optional">
                          <xs:annotation>
                            <xs:documentation>Номер таможенной декларации</xs:documentation>
                          </xs:annotation>
                          <xs:simpleType>
                            <xs:restriction base="xs:string">
                              <xs:minLength value="1"/>
                              <xs:maxLength value="1000"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                        <xs:attribute name="ОКВ" type="ОКВТип" use="optional"/>
                        <xs:attribute name="СтоимПокупВ" use="required">
                          <xs:annotation>
                            <xs:documentation>Стоимость покупок по счету-фактуре, разница стоимости по корректировочному счету-фактуре (включая НДС), в валюте счета-фактуры</xs:documentation>
                          </xs:annotation>
                          <xs:simpleType>
                            <xs:restriction base="xs:decimal">
                              <xs:totalDigits value="19"/>
                              <xs:fractionDigits value="2"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                        <xs:attribute name="СумНДС" type="Сум17.2Тип" use="required"/>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element name="T1.2" minOccurs="0">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element name="T1.2Стр" maxOccurs="unbounded">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element name="СвПокуп" type="СвУчСдТип" minOccurs="0"/>
                          <xs:element name="СвПос" type="СвУчСдТип" minOccurs="0"/>
                        </xs:sequence>
                        <xs:attribute name="НомерПор" use="required">
                          <xs:annotation>
                            <xs:documentation>Порядковый номер записи Книги продаж (доп.листа)</xs:documentation>
                          </xs:annotation>
                          <xs:simpleType>
                            <xs:restriction base="xs:int">
                              <xs:totalDigits value="12"/>
                              <xs:minInclusive value="1"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                        <xs:attribute name="КодВидОпер" use="required">
                          <xs:annotation>
                            <xs:documentation>Код вида операции</xs:documentation>
                          </xs:annotation>
                          <xs:simpleType>
                            <xs:restriction base="xs:string">
                              <xs:length value="2"/>
                              <xs:enumeration value="01"/>
                              <xs:enumeration value="02"/>
                              <xs:enumeration value="03"/>
                              <xs:enumeration value="04"/>
                              <xs:enumeration value="05"/>
                              <xs:enumeration value="06"/>
                              <xs:enumeration value="07"/>
                              <xs:enumeration value="08"/>
                              <xs:enumeration value="09"/>
                              <xs:enumeration value="10"/>
                              <xs:enumeration value="11"/>
                              <xs:enumeration value="12"/>
                              <xs:enumeration value="13"/>
                              <xs:enumeration value="14"/>
                              <xs:enumeration value="15"/>
                              <xs:enumeration value="16"/>
                              <xs:enumeration value="17"/>
                              <xs:enumeration value="18"/>
                              <xs:enumeration value="19"/>
                              <xs:enumeration value="20"/>
                              <xs:enumeration value="21"/>
                              <xs:enumeration value="22"/>
                              <xs:enumeration value="23"/>
                              <xs:enumeration value="24"/>
                              <xs:enumeration value="25"/>
                              <xs:enumeration value="26"/>
                              <xs:enumeration value="27"/>
                              <xs:enumeration value="28"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                        <xs:attribute name="НомСчФПрод" use="required">
                          <xs:annotation>
                            <xs:documentation>Номер счета-фактуры продавца</xs:documentation>
                          </xs:annotation>
                          <xs:simpleType>
                            <xs:restriction base="xs:string">
                              <xs:minLength value="1"/>
                              <xs:maxLength value="1000"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                        <xs:attribute name="ДатаСчФПрод" type="ДатаТип" use="required"/>
                        <xs:attribute name="НомИспрСчФ" use="optional">
                          <xs:annotation>
                            <xs:documentation>Номер исправления счета-фактуры продавца</xs:documentation>
                          </xs:annotation>
                          <xs:simpleType>
                            <xs:restriction base="xs:int">
                              <xs:totalDigits value="3"/>
                              <xs:minInclusive value="1"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                        <xs:attribute name="ДатаИспрСчФ" type="ДатаТип" use="optional"/>
                        <xs:attribute name="НомКСчФПрод" use="optional">
                          <xs:annotation>
                            <xs:documentation>Номер корректировочного счета-фактуры продавца</xs:documentation>
                          </xs:annotation>
                          <xs:simpleType>
                            <xs:restriction base="xs:string">
                              <xs:minLength value="1"/>
                              <xs:maxLength value="256"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                        <xs:attribute name="ДатаКСчФПрод" type="ДатаТип" use="optional"/>
                        <xs:attribute name="НомИспрКСчФ" use="optional">
                          <xs:annotation>
                            <xs:documentation>Номер исправления корректировочного счета-фактуры продавца</xs:documentation>
                          </xs:annotation>
                          <xs:simpleType>
                            <xs:restriction base="xs:int">
                              <xs:totalDigits value="3"/>
                              <xs:minInclusive value="1"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                        <xs:attribute name="ДатаИспрКСчФ" type="ДатаТип" use="optional"/>
                        <xs:attribute name="НомДокПдтвОпл" use="optional">
                          <xs:annotation>
                            <xs:documentation>Номер документа, подтверждающего оплату</xs:documentation>
                          </xs:annotation>
                          <xs:simpleType>
                            <xs:restriction base="xs:string">
                              <xs:minLength value="1"/>
                              <xs:maxLength value="256"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                        <xs:attribute name="ДатаДокПдтвОпл" type="ДатаТип" use="optional"/>
                        <xs:attribute name="ОКВ" type="ОКВТип" use="optional"/>
                        <xs:attribute name="СтоимПродСФВ" use="optional">
                          <xs:annotation>
                            <xs:documentation>Стоимость продаж по счету-фактуре, разница стоимости по корректировочному счету-фактуре (включая НДС), в валюте счета-фактуры</xs:documentation>
                          </xs:annotation>
                          <xs:simpleType>
                            <xs:restriction base="xs:decimal">
                              <xs:totalDigits value="19"/>
                              <xs:fractionDigits value="2"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                        <xs:attribute name="СтоимПродСФ" use="optional">
                          <xs:annotation>
                            <xs:documentation>Стоимость продаж по счету-фактуре, разница стоимости по корректировочному счету-фактуре (включая НДС), в руб. и коп.</xs:documentation>
                          </xs:annotation>
                          <xs:simpleType>
                            <xs:restriction base="xs:decimal">
                              <xs:totalDigits value="19"/>
                              <xs:fractionDigits value="2"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                        <xs:attribute name="СтоимПродСФ18" use="optional">
                          <xs:annotation>
                            <xs:documentation>Стоимость продаж, облагаемых налогом, по счету-фактуре, разница стоимости по корректировочному счету-фактуре (без НДС), в руб. и коп., по ставке 18%</xs:documentation>
                          </xs:annotation>
                          <xs:simpleType>
                            <xs:restriction base="xs:decimal">
                              <xs:totalDigits value="19"/>
                              <xs:fractionDigits value="2"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                        <xs:attribute name="СтоимПродСФ10" use="optional">
                          <xs:annotation>
                            <xs:documentation>Стоимость продаж, облагаемых налогом, по счету-фактуре, разница стоимости по корректировочному счету-фактуре (без НДС), в руб. и коп., по ставке 10%</xs:documentation>
                          </xs:annotation>
                          <xs:simpleType>
                            <xs:restriction base="xs:decimal">
                              <xs:totalDigits value="19"/>
                              <xs:fractionDigits value="2"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                        <xs:attribute name="СтоимПродСФ0" use="optional">
                          <xs:annotation>
                            <xs:documentation>Стоимость продаж, облагаемых налогом, по счету-фактуре, разница стоимости по корректировочному счету-фактуре (без НДС), в руб. и коп., по ставке 0%</xs:documentation>
                          </xs:annotation>
                          <xs:simpleType>
                            <xs:restriction base="xs:decimal">
                              <xs:totalDigits value="19"/>
                              <xs:fractionDigits value="2"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                        <xs:attribute name="СумНДССФ18" use="optional">
                          <xs:annotation>
                            <xs:documentation>Сумма НДС по счету-фактуре, разница стоимости по корректировочному счету-фактуре, в руб. и коп., по ставке 18%</xs:documentation>
                          </xs:annotation>
                          <xs:simpleType>
                            <xs:restriction base="xs:decimal">
                              <xs:totalDigits value="19"/>
                              <xs:fractionDigits value="2"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                        <xs:attribute name="СумНДССФ10" use="optional">
                          <xs:annotation>
                            <xs:documentation>Сумма НДС по счету-фактуре, разница стоимости по корректировочному счету-фактуре, в руб. и коп., по ставке 10%</xs:documentation>
                          </xs:annotation>
                          <xs:simpleType>
                            <xs:restriction base="xs:decimal">
                              <xs:totalDigits value="19"/>
                              <xs:fractionDigits value="2"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                        <xs:attribute name="СтоимПродОсв" use="optional">
                          <xs:annotation>
                            <xs:documentation>Стоимость продаж, освобождаемых от налога, по счету-фактуре, разница стоимости по корректировочному счету-фактуре, в руб. и коп.</xs:documentation>
                          </xs:annotation>
                          <xs:simpleType>
                            <xs:restriction base="xs:decimal">
                              <xs:totalDigits value="19"/>
                              <xs:fractionDigits value="2"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element name="T1.3" minOccurs="0">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element name="T1.3Стр" maxOccurs="unbounded">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element name="СвПокуп" type="СвУчСдТип" minOccurs="0"/>
                          <xs:element name="СвПосрДеят" minOccurs="0">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element name="СвПосрДеятСтр" minOccurs="0" maxOccurs="unbounded">
                                  <xs:complexType>
                                    <xs:sequence>
                                      <xs:element name="СвПрод" type="СвУчСдТип" minOccurs="0"/>
                                    </xs:sequence>
                                    <xs:attribute name="НомСчФОтПрод" use="required">
                                      <xs:annotation>
                                        <xs:documentation>Номер счета-фактуры, полученного от продавца</xs:documentation>
                                      </xs:annotation>
                                      <xs:simpleType>
                                        <xs:restriction base="xs:string">
                                          <xs:minLength value="1"/>
                                          <xs:maxLength value="1000"/>
                                        </xs:restriction>
                                      </xs:simpleType>
                                    </xs:attribute>
                                    <xs:attribute name="ДатаСчФОтПрод" type="ДатаТип" use="required"/>
                                    <xs:attribute name="ОКВ" type="ОКВТип" use="optional"/>
                                    <xs:attribute name="СтоимТовСчФВс" use="optional">
                                      <xs:annotation>
                                        <xs:documentation>Стоимость товаров (работ, услуг), имущественных прав по счету-фактуре – всего</xs:documentation>
                                      </xs:annotation>
                                      <xs:simpleType>
                                        <xs:restriction base="xs:decimal">
                                          <xs:totalDigits value="19"/>
                                          <xs:fractionDigits value="2"/>
                                        </xs:restriction>
                                      </xs:simpleType>
                                    </xs:attribute>
                                    <xs:attribute name="СумНДССчФ" type="Сум17.2Тип" use="optional"/>
                                    <xs:attribute name="РазСтКСчФУм" use="optional">
                                      <xs:annotation>
                                        <xs:documentation>Разница стоимости с учетом НДС по корректировочному счету-фактуре – уменьшение</xs:documentation>
                                      </xs:annotation>
                                      <xs:simpleType>
                                        <xs:restriction base="xs:decimal">
                                          <xs:totalDigits value="19"/>
                                          <xs:fractionDigits value="2"/>
                                        </xs:restriction>
                                      </xs:simpleType>
                                    </xs:attribute>
                                    <xs:attribute name="РазСтКСчФУв" use="optional">
                                      <xs:annotation>
                                        <xs:documentation>Разница стоимости с учетом НДС по корректировочному счету-фактуре – увеличение</xs:documentation>
                                      </xs:annotation>
                                      <xs:simpleType>
                                        <xs:restriction base="xs:decimal">
                                          <xs:totalDigits value="19"/>
                                          <xs:fractionDigits value="2"/>
                                        </xs:restriction>
                                      </xs:simpleType>
                                    </xs:attribute>
                                    <xs:attribute name="РазНДСКСчФУм" use="optional">
                                      <xs:annotation>
                                        <xs:documentation>Разница НДС по корректировочному счету-фактуре – уменьшение</xs:documentation>
                                      </xs:annotation>
                                      <xs:simpleType>
                                        <xs:restriction base="xs:decimal">
                                          <xs:totalDigits value="19"/>
                                          <xs:fractionDigits value="2"/>
                                        </xs:restriction>
                                      </xs:simpleType>
                                    </xs:attribute>
                                    <xs:attribute name="РазНДСКСчФУв" use="optional">
                                      <xs:annotation>
                                        <xs:documentation>Разница НДС по корректировочному счету-фактуре – увеличение</xs:documentation>
                                      </xs:annotation>
                                      <xs:simpleType>
                                        <xs:restriction base="xs:decimal">
                                          <xs:totalDigits value="19"/>
                                          <xs:fractionDigits value="2"/>
                                        </xs:restriction>
                                      </xs:simpleType>
                                    </xs:attribute>
                                  </xs:complexType>
																</xs:element>
															</xs:sequence>
														</xs:complexType>
													</xs:element>
												</xs:sequence>';
  DBMS_LOB.append(c1, '<xs:attribute name="НомерПор" use="required">
													<xs:annotation>
														<xs:documentation>Порядковый номер записи Журнала выставленных счетов-фактур</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:int">
															<xs:totalDigits value="12"/>
															<xs:minInclusive value="1"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="ДатаВыст" type="ДатаТип" use="required"/>
												<xs:attribute name="КодВидОпер" use="required">
													<xs:annotation>
														<xs:documentation>Код вида операции</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:string">
															<xs:length value="2"/>
															<xs:enumeration value="01"/>
															<xs:enumeration value="02"/>
															<xs:enumeration value="03"/>
															<xs:enumeration value="04"/>
															<xs:enumeration value="05"/>
															<xs:enumeration value="06"/>
															<xs:enumeration value="07"/>
															<xs:enumeration value="08"/>
															<xs:enumeration value="09"/>
															<xs:enumeration value="10"/>
															<xs:enumeration value="11"/>
															<xs:enumeration value="12"/>
															<xs:enumeration value="13"/>
															<xs:enumeration value="14"/>
															<xs:enumeration value="15"/>
															<xs:enumeration value="16"/>
															<xs:enumeration value="17"/>
															<xs:enumeration value="18"/>
															<xs:enumeration value="19"/>
															<xs:enumeration value="20"/>
															<xs:enumeration value="21"/>
															<xs:enumeration value="22"/>
															<xs:enumeration value="23"/>
															<xs:enumeration value="24"/>
															<xs:enumeration value="25"/>
															<xs:enumeration value="26"/>
															<xs:enumeration value="27"/>
															<xs:enumeration value="28"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="НомСчФПрод" use="required">
													<xs:annotation>
														<xs:documentation>Номер счета-фактуры продавца</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:string">
															<xs:minLength value="1"/>
															<xs:maxLength value="1000"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="ДатаСчФПрод" type="ДатаТип" use="required"/>
												<xs:attribute name="НомИспрСчФ" use="optional">
													<xs:annotation>
														<xs:documentation>Номер исправления счета-фактуры продавца</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:int">
															<xs:totalDigits value="3"/>
															<xs:minInclusive value="1"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="ДатаИспрСчФ" type="ДатаТип" use="optional"/>
												<xs:attribute name="НомКСчФПрод" use="optional">
													<xs:annotation>
														<xs:documentation>Номер корректировочного счета-фактуры продавца</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:string">
															<xs:minLength value="1"/>
															<xs:maxLength value="256"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="ДатаКСчФПрод" type="ДатаТип" use="optional"/>
												<xs:attribute name="НомИспрКСчФ" use="optional">
													<xs:annotation>
														<xs:documentation>Номер исправления корректировочного счета-фактуры продавца</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:int">
															<xs:totalDigits value="3"/>
															<xs:minInclusive value="1"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="ДатаИспрКСчФ" type="ДатаТип" use="optional"/>
											</xs:complexType>
										</xs:element>
									</xs:sequence>
								</xs:complexType>
							</xs:element>
							<xs:element name="T1.4" minOccurs="0">
								<xs:complexType>
									<xs:sequence>
										<xs:element name="T1.4Стр" maxOccurs="unbounded">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="СвПрод" type="СвУчСдТип" minOccurs="0"/>
													<xs:element name="СвСубком" type="СвУчСдТип" minOccurs="0"/>
												</xs:sequence>
												<xs:attribute name="НомерПор" use="required">
													<xs:annotation>
														<xs:documentation>Порядковый номер записи Журнала выставленных счетов-фактур</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:int">
															<xs:totalDigits value="12"/>
															<xs:minInclusive value="1"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="ДатаПолуч" type="ДатаТип" use="required"/>
												<xs:attribute name="КодВидОпер" use="required">
													<xs:annotation>
														<xs:documentation>Код вида операции</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:string">
															<xs:length value="2"/>
															<xs:enumeration value="01"/>
															<xs:enumeration value="02"/>
															<xs:enumeration value="03"/>
															<xs:enumeration value="04"/>
															<xs:enumeration value="05"/>
															<xs:enumeration value="06"/>
															<xs:enumeration value="07"/>
															<xs:enumeration value="08"/>
															<xs:enumeration value="09"/>
															<xs:enumeration value="10"/>
															<xs:enumeration value="11"/>
															<xs:enumeration value="12"/>
															<xs:enumeration value="13"/>
															<xs:enumeration value="14"/>
															<xs:enumeration value="15"/>
															<xs:enumeration value="16"/>
															<xs:enumeration value="17"/>
															<xs:enumeration value="18"/>
															<xs:enumeration value="19"/>
															<xs:enumeration value="20"/>
															<xs:enumeration value="21"/>
															<xs:enumeration value="22"/>
															<xs:enumeration value="23"/>
															<xs:enumeration value="24"/>
															<xs:enumeration value="25"/>
															<xs:enumeration value="26"/>
															<xs:enumeration value="27"/>
															<xs:enumeration value="28"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="НомСчФПрод" use="required">
													<xs:annotation>
														<xs:documentation>Номер счета-фактуры</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:string">
															<xs:minLength value="1"/>
															<xs:maxLength value="1000"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="ДатаСчФПрод" type="ДатаТип" use="required"/>
												<xs:attribute name="НомИспрСчФ" use="optional">
													<xs:annotation>
														<xs:documentation>Номер исправления счета-фактуры</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:int">
															<xs:totalDigits value="3"/>
															<xs:minInclusive value="1"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="ДатаИспрСчФ" type="ДатаТип" use="optional"/>
												<xs:attribute name="НомКСчФПрод" use="optional">
													<xs:annotation>
														<xs:documentation>Номер корректировочного счета-фактуры</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:string">
															<xs:minLength value="1"/>
															<xs:maxLength value="256"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="ДатаКСчФПрод" type="ДатаТип" use="optional"/>
												<xs:attribute name="НомИспрКСчФ" use="optional">
													<xs:annotation>
														<xs:documentation>Номер исправления корректировочного счета-фактуры</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:int">
															<xs:totalDigits value="3"/>
															<xs:minInclusive value="1"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="ДатаИспрКСчФ" type="ДатаТип" use="optional"/>
												<xs:attribute name="КодВидСд" use="required">
													<xs:annotation>
														<xs:documentation>Код вида сделки</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:string">
															<xs:length value="1"/>
															<xs:enumeration value="1"/>
															<xs:enumeration value="2"/>
															<xs:enumeration value="3"/>
															<xs:enumeration value="4"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="ОКВ" type="ОКВТип" use="optional"/>
												<xs:attribute name="СтоимТовСчФВс" use="required">
													<xs:annotation>
														<xs:documentation>Стоимость товаров (работ, услуг), имущественных прав по счету-фактуре – всего</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:decimal">
															<xs:totalDigits value="19"/>
															<xs:fractionDigits value="2"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="СумНДССчФ" type="Сум17.2Тип" use="optional"/>
												<xs:attribute name="РазСтКСчФУм" use="optional">
													<xs:annotation>
														<xs:documentation>Разница стоимости с учетом НДС по корректировочному счету-фактуре – уменьшение</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:decimal">
															<xs:totalDigits value="19"/>
															<xs:fractionDigits value="2"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="РазСтКСчФУв" use="optional">
													<xs:annotation>
														<xs:documentation>Разница стоимости с учетом НДС по корректировочному счету-фактуре – увеличение</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:decimal">
															<xs:totalDigits value="19"/>
															<xs:fractionDigits value="2"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="РазНДСКСчФУм" type="Сум17.2Тип" use="optional"/>
												<xs:attribute name="РазНДСКСчФУв" type="Сум17.2Тип" use="optional"/>
											</xs:complexType>
										</xs:element>
									</xs:sequence>
								</xs:complexType>
							</xs:element>
							<xs:element name="T1.5" minOccurs="0">
								<xs:complexType>
									<xs:sequence>
										<xs:element name="T1.5Стр" maxOccurs="unbounded">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="СвПокуп" type="СвУчСдТип" minOccurs="0"/>
												</xs:sequence>
												<xs:attribute name="НомСчФ" use="required">
													<xs:annotation>
														<xs:documentation>Номер счета-фактуры</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:string">
															<xs:minLength value="1"/>
															<xs:maxLength value="1000"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="ДатаСчФ" type="ДатаТип" use="required"/>
												<xs:attribute name="ОКВ" type="ОКВТип" use="optional"/>
												<xs:attribute name="СтоимТовБНалВс" use="required">
													<xs:annotation>
														<xs:documentation>Стоимость товаров (работ, услуг), имущественных прав без налога – всего</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:decimal">
															<xs:totalDigits value="19"/>
															<xs:fractionDigits value="2"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="СумНалПокуп" type="Сум17.2Тип" use="required"/>
												<xs:attribute name="СтоимТовСНалВс" use="required">
													<xs:annotation>
														<xs:documentation>Стоимость товаров (работ, услуг), имущественных прав с налогом  - всего</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:decimal">
															<xs:totalDigits value="19"/>
															<xs:fractionDigits value="2"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
											</xs:complexType>
										</xs:element>
									</xs:sequence>
								</xs:complexType>
							</xs:element>
							<xs:element name="T1.6" minOccurs="0" maxOccurs="3">
								<xs:complexType>
									<xs:sequence>
										<xs:element name="T1.6Стр" maxOccurs="unbounded">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="СвКАгент">
														<xs:complexType>
															<xs:complexContent>
																<xs:extension base="СвУчСдТип">
																	<xs:attribute name="НаимКАгент" use="required">
																		<xs:annotation>
																			<xs:documentation>Наименование (ФИО) контрагента</xs:documentation>
																		</xs:annotation>
																		<xs:simpleType>
																			<xs:restriction base="xs:string">
																				<xs:minLength value="1"/>
																				<xs:maxLength value="1000"/>
																			</xs:restriction>
																		</xs:simpleType>
																	</xs:attribute>
																</xs:extension>
															</xs:complexContent>
														</xs:complexType>
													</xs:element>
													<xs:element name="СведСФ" maxOccurs="unbounded">
														<xs:complexType>
															<xs:attribute name="НомСчФ" use="required">
																<xs:annotation>
																	<xs:documentation>Номер счета-фактуры</xs:documentation>
																</xs:annotation>
																<xs:simpleType>
																	<xs:restriction base="xs:string">
																		<xs:minLength value="1"/>
																		<xs:maxLength value="256"/>
																	</xs:restriction>
																</xs:simpleType>
															</xs:attribute>
															<xs:attribute name="ДатаСчФ" type="ДатаТип" use="required">
																<xs:annotation>
																	<xs:documentation>Дата счета-фактуры</xs:documentation>
																</xs:annotation>
															</xs:attribute>
														</xs:complexType>
													</xs:element>
												</xs:sequence>
											</xs:complexType>
										</xs:element>
									</xs:sequence>');
  DBMS_LOB.append(c1, '<xs:attribute name="РазделДекл" use="required">
										<xs:annotation>
											<xs:documentation>Раздел декларации, в котором отсутствуют данные</xs:documentation>
										</xs:annotation>
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:minLength value="1"/>
												<xs:maxLength value="2"/>
												<xs:enumeration value="9"/>
												<xs:enumeration value="10"/>
												<xs:enumeration value="12"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:attribute>
								</xs:complexType>
							</xs:element>
							<xs:element name="T1.7" minOccurs="0">
								<xs:complexType>
									<xs:sequence>
										<xs:element name="T1.7Стр" maxOccurs="unbounded">
											<xs:complexType>
												<xs:attribute name="СвНар" use="required">
													<xs:annotation>
														<xs:documentation>Сведения о нарушении</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:string">
															<xs:length value="2"/>
															<xs:enumeration value="10"/>
															<xs:enumeration value="12"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="КодКС" use="required">
													<xs:annotation>
														<xs:documentation>Код контрольного соотношения</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:string">
															<xs:minLength value="3"/>
															<xs:maxLength value="6"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="СумЛевЧ" use="required">
													<xs:annotation>
														<xs:documentation>Левая часть (сумма)</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:decimal">
															<xs:totalDigits value="19"/>
															<xs:fractionDigits value="2"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="СумПравЧ" use="required">
													<xs:annotation>
														<xs:documentation>Правая часть (сумма)</xs:documentation>
													</xs:annotation>
													<xs:simpleType>
														<xs:restriction base="xs:decimal">
															<xs:totalDigits value="19"/>
															<xs:fractionDigits value="2"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
											</xs:complexType>
										</xs:element>
									</xs:sequence>
								</xs:complexType>
							</xs:element>
						</xs:sequence>
					</xs:complexType>
				</xs:element>
			</xs:sequence>
			<xs:attribute name="КодНО" type="СОНОТип" use="required"/>
			<xs:attribute name="РегНомДек" use="required">
				<xs:annotation>
					<xs:documentation>Регистрационный номер декларации</xs:documentation>
				</xs:annotation>
				<xs:simpleType>
					<xs:restriction base="xs:int">
						<xs:totalDigits value="13"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:attribute>
			<xs:attribute name="УчНомТреб" use="required">
				<xs:annotation>
					<xs:documentation>Учетный номер автотребования</xs:documentation>
				</xs:annotation>
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:minLength value="1"/>
						<xs:maxLength value="20"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:attribute>
			<xs:attribute name="ТипИнф" use="required">
				<xs:annotation>
					<xs:documentation>Тип информации</xs:documentation>
				</xs:annotation>
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:enumeration value="TAX3EXCH_NDS2_CAM_01"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:attribute>
		</xs:complexType>
	</xs:element>
	<xs:complexType name="СвУчСдТип">
		<xs:annotation>
			<xs:documentation>Сведения об участнике сделки</xs:documentation>
		</xs:annotation>
		<xs:choice>
			<xs:element name="СведЮЛ">
				<xs:annotation>
					<xs:documentation>Сведения об организации</xs:documentation>
				</xs:annotation>
				<xs:complexType>
					<xs:attribute name="ИННЮЛ" type="ИННЮЛТип" use="required">
						<xs:annotation>
							<xs:documentation>ИНН организации</xs:documentation>
						</xs:annotation>
					</xs:attribute>
					<xs:attribute name="КПП" type="КППТип" use="required">
						<xs:annotation>
							<xs:documentation>КПП</xs:documentation>
						</xs:annotation>
					</xs:attribute>
				</xs:complexType>
			</xs:element>
			<xs:element name="СведИП">
				<xs:annotation>
					<xs:documentation>Сведения об индивидуальном предпринимателе</xs:documentation>
				</xs:annotation>
				<xs:complexType>
					<xs:attribute name="ИННФЛ" type="ИННФЛТип" use="required">
						<xs:annotation>
							<xs:documentation>ИНН физического лица</xs:documentation>
						</xs:annotation>
					</xs:attribute>
				</xs:complexType>
			</xs:element>
		</xs:choice>
	</xs:complexType>
	<xs:complexType name="ФИОТип">
		<xs:annotation>
			<xs:documentation>Фамилия, имя, отчество</xs:documentation>
		</xs:annotation>
		<xs:attribute name="Фамилия" use="required">
			<xs:annotation>
				<xs:documentation>Фамилия</xs:documentation>
			</xs:annotation>
			<xs:simpleType>
				<xs:restriction base="xs:string">
					<xs:minLength value="1"/>
					<xs:maxLength value="60"/>
				</xs:restriction>
			</xs:simpleType>
		</xs:attribute>
		<xs:attribute name="Имя" use="required">
			<xs:annotation>
				<xs:documentation>Имя</xs:documentation>
			</xs:annotation>
			<xs:simpleType>
				<xs:restriction base="xs:string">
					<xs:minLength value="1"/>
					<xs:maxLength value="60"/>
				</xs:restriction>
			</xs:simpleType>
		</xs:attribute>
		<xs:attribute name="Отчество" use="optional">
			<xs:annotation>
				<xs:documentation>Отчество</xs:documentation>
			</xs:annotation>
			<xs:simpleType>
				<xs:restriction base="xs:string">
					<xs:minLength value="1"/>
					<xs:maxLength value="60"/>
				</xs:restriction>
			</xs:simpleType>
		</xs:attribute>
	</xs:complexType>
	<xs:simpleType name="ИННФЛТип">
		<xs:annotation>
			<xs:documentation>Идентификационный номер налогоплательщика - физического лица</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:string">
			<xs:length value="12"/>
			<xs:pattern value="([0-9]{1}[1-9]{1}|[1-9]{1}[0-9]{1})[0-9]{10}"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="ИННЮЛТип">
		<xs:annotation>
			<xs:documentation>Идентификационный номер налогоплательщика - организации</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:string">
			<xs:length value="10"/>
			<xs:pattern value="([0-9]{1}[1-9]{1}|[1-9]{1}[0-9]{1})[0-9]{8}"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="КППТип">
		<xs:annotation>
			<xs:documentation>Код причины постановки на учет (КПП) - 5 и 6 знаки от 0-9 и A-Z</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:string">
			<xs:length value="9"/>
			<xs:pattern value="([0-9]{1}[1-9]{1}|[1-9]{1}[0-9]{1})([0-9]{2})([0-9A-Z]{2})([0-9]{3})"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="СОНОТип">
		<xs:annotation>
			<xs:documentation>Коды из Классификатора системы обозначений налоговых органов</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:string">
			<xs:length value="4"/>
			<xs:pattern value="[0-9]{4}"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="ОКВТип">
		<xs:annotation>
			<xs:documentation>Код из Общероссийского классификатора валют</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:string">
			<xs:length value="3"/>
			<xs:pattern value="[0-9]{3}"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="Сум17.2Тип">
		<xs:annotation>
			<xs:documentation>Сумма</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:string">
			<xs:minLength value="1"/>
			<xs:maxLength value="20"/>
			<xs:pattern value="\d{1,17}\.\d{0,2}"/>
			<xs:pattern value="\d{1,17}"/>
			<xs:pattern value="без НДС"/>
			<xs:pattern value="-"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="ДатаТип">
		<xs:annotation>
			<xs:documentation>Дата в формате ДД.ММ.ГГГГ (01.01.1900 - 31.12.2099)</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:string">
			<xs:length value="10"/>
			<xs:pattern value="((((0[1-9]{1}|1[0-9]{1}|2[0-8]{1})\.(0[1-9]{1}|1[0-2]{1}))|((29|30)\.(01|0[3-9]{1}|1[0-2]{1}))|(31\.(01|03|05|07|08|10|12)))\.((19|20)[0-9]{2}))|(29\.02\.((19|20)(((0|2|4|6|8)(0|4|8))|((1|3|5|7|9)(2|6)))))"/>
		</xs:restriction>
	</xs:simpleType>
</xs:schema>');    
  DBMS_XMLSCHEMA.registerSchema(
    SCHEMAURL => 'TAX3EXCH_NDS2_CAM_01_01.xsd',
    SCHEMADOC => C1,
     --genTypes => false, 
     genTables => false, 
     local => true,
	 owner => 'NDS2_MRR_USER'
    );
END;
/
