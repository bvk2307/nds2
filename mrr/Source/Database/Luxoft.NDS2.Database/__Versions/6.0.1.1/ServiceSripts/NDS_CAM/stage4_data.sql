﻿begin
for line in (select
          distinct
          vd.SEOD_DECL_ID,
          sysdate as d1,
          '0505' as n1,
          sysdate as d2
          from
          doc d
          inner join doc_invoice di on di.doc_id = d.doc_id
          inner join sov_discrepancy sd on sd.invoice_rk = di.invoice_row_key
          inner join v$declaration vd on vd.ID = sd.decl_contractor_id
          where 
          d.doc_type = 1 
          and d.close_date is not null)
loop 
  insert into seod_knp(knp_id, declaration_reg_num, creation_date, ifns_code, completion_date)
  values(seq_seod_knp.nextval, line.SEOD_DECL_ID, line.d1, line.n1, line.d2);
end loop;
end;
