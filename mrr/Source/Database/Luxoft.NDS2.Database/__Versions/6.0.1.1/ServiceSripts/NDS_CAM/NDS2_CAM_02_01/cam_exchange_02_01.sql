﻿declare
  v_path varchar2(128);
  doc xmldom.DOMDocument;
  main_node xmldom.DOMNode;
  root_node xmldom.DOMNode;
  user_node xmldom.DOMNode;
  item_node xmldom.DOMNode;
  root_elmt xmldom.DOMElement;
  item_elmt xmldom.DOMElement;
  item_text xmldom.DOMText;
  
  claimListElement xmldom.DOMElement;
  claimListNode xmldom.DOMNode;
/*СФ*/
  sfElmItem xmldom.DOMElement;
  sfNodeItem xmldom.DOMNode;
/*КСФ*/
  ksfElmItem xmldom.DOMElement;
  ksfNodeItem xmldom.DOMNode;  
/*Книга продаж*/
  dtDocElmItem xmldom.DOMElement;
  dtDocNodeItem xmldom.DOMNode;
/*Книга продаж*/
  dtPeriodDocElmItem xmldom.DOMElement;
  dtPeriodDocNodeItem xmldom.DOMNode;
  domText xmldom.DOMText;
    domTextNode xmldom.DOMNode;
/*taxpayer*/
  TaxPayerInfoElement xmldom.DOMElement;
  TaxPayerInfoNode xmldom.DOMNode;
  TaxPayerElement xmldom.DOMElement;
  TaxPayerNode xmldom.DOMNode;  
  claim_id  number(12);
  invStrNum number(12);
  xmlNode XmlType;
  buf varchar2(1000);
  buff clob;
  date_format varchar2(12) := 'DD.MM.YYYY';
  decimal_format varchar2(18) := '9999999990.00';
  ks_mode boolean := false;
  v_ch varchar2(2);
  v_svNar varchar2(2 char);
  v_nomAlg varchar2(4);
  v_res number;
begin
  claim_id := 0;

  for declLine in (select * from v$declaration)
    loop
      begin
        invStrNum := 0;
        claim_id := claim_id + 1;
        doc := xmldom.newDOMDocument();
        main_node := xmldom.makeNode(doc);
        root_elmt := xmldom.createElement(doc, 'Истреб93');
        xmldom.setAttribute(root_elmt, 'КодНО', '5252');
        xmldom.setAttribute(root_elmt, 'РегНомДек', declLine.Declaration_Version_Id);
        xmldom.setAttribute(root_elmt, 'УчНомИстреб', claim_id);
        xmldom.setAttribute(root_elmt, 'ТипИнф', 'TAX3EXCH_NDS2_CAM_02');
        
        
        xmldom.setAttribute(root_elmt, 'xsi:noNamespaceSchemaLocation', 'TAX3EXCH_NDS2_CAM_02_01.xsd');
        xmldom.setAttribute(root_elmt, 'xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
        root_node := xmldom.appendChild(main_node, xmldom.makeNode(root_elmt));


        for i in 1..substr(DBMS_RANDOM.value(1, 2), 3, 2) loop
          claimListElement := xmldom.createElement(doc, 'Истреб93Док');
          claimListNode := xmldom.appendChild(root_node, xmldom.makeNode(claimListElement));
          if mod(i,3) = 1 then
            sfElmItem := xmldom.createElement(doc, 'СФ');
            sfNodeItem := xmldom.appendChild(claimListNode, xmldom.makeNode(sfElmItem));
            xmldom.setAttribute(sfElmItem, 'КодДок', '0924');
            xmldom.setAttribute(sfElmItem, 'НомДок', substr(DBMS_RANDOM.value(1, 2), 3, 20));
            if mod(i,2) = 1 then 
                dtDocElmItem := xmldom.createElement(doc, 'ДатаДок');
                dtDocNodeItem := xmldom.appendChild(sfNodeItem, xmldom.makeNode(dtDocElmItem));
                domText := xmldom.createTextNode(doc, to_char((sysdate - substr(DBMS_RANDOM.value(1, 2), 3, 2)), date_format));
                domTextNode := xmldom.appendChild(dtDocNodeItem, xmldom.makeNode(domText));
            else
                dtDocElmItem := xmldom.createElement(doc, 'ПериодДок');
                dtDocNodeItem := xmldom.appendChild(sfNodeItem, xmldom.makeNode(dtDocElmItem));
                xmldom.setAttribute(dtDocElmItem, 'НачПер', to_char((sysdate - substr(DBMS_RANDOM.value(1, 2), 3, 2)), date_format));
                xmldom.setAttribute(dtDocElmItem, 'ОконПер', to_char((sysdate - substr(DBMS_RANDOM.value(1, 2), 3, 2)), date_format));
            end if;
          else
            sfElmItem := xmldom.createElement(doc, 'КСФ');
            sfNodeItem := xmldom.appendChild(claimListNode, xmldom.makeNode(sfElmItem));
            xmldom.setAttribute(sfElmItem, 'КодДок', '2772');
            xmldom.setAttribute(sfElmItem, 'НомДок', substr(DBMS_RANDOM.value(1, 2), 3, 20));
            if mod(i,2) = 1 then 
                dtDocElmItem := xmldom.createElement(doc, 'ДатаДок');
                dtDocNodeItem := xmldom.appendChild(sfNodeItem, xmldom.makeNode(dtDocElmItem));
                domText := xmldom.createTextNode(doc, to_char((sysdate - substr(DBMS_RANDOM.value(1, 2), 3, 2)), date_format));
                domTextNode := xmldom.appendChild(dtDocNodeItem, xmldom.makeNode(domText));
            else
                dtDocElmItem := xmldom.createElement(doc, 'ПериодДок');
                dtDocNodeItem := xmldom.appendChild(sfNodeItem, xmldom.makeNode(dtDocElmItem));
                xmldom.setAttribute(dtDocElmItem, 'НачПер', to_char((sysdate - substr(DBMS_RANDOM.value(1, 2), 3, 2)), date_format));
                xmldom.setAttribute(dtDocElmItem, 'ОконПер', to_char((sysdate - substr(DBMS_RANDOM.value(1, 2), 3, 2)), date_format));
            end if;
          end if;
        end loop;

        xmlNode := dbms_xmldom.getXmlType(doc);
          --v_res := xmlNode.isSchemaValid('TAX3EXCH_NDS2_CAM_01_01.xsd');
          --dbms_output.put_line(declLine.Declaration_Version_Id||'  '||XmlType.createXML(xmlNode.getClobVal()).isSchemaValid('TAX3EXCH_NDS2_CAM_01_01.xsd'));
          --
          --dbms_output.put_line(substr(xmlNode.getClobVal(), 0, 500));
          v_path := declLine.Declaration_Version_Id||'.xml';
          --xmlNode.isSchemaValid('http://www.oracle.com/PO.xsd');
-- select * from doc order by 1 desc
--          DBMS_XSLProcessor.Clob2File(xmlNode.getClobVal(),'EXP_DIR',v_path);
          insert into doc(doc_id, ref_entity_id, doc_type, sono_code, create_date, status, document_body)
          values (SEQ_CLAIM.NEXTVAL, declLine.Declaration_Version_Id, 2, '5252', sysdate, 51, xmlNode.getClobVal());
--          dbms_output.put_line(v_path);
          xmldom.freeDocument(doc);
        end;
    end loop;
    commit;
end;
