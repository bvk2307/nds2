﻿begin
	DBMS_XMLSCHEMA.deleteSchema('TAX3EXCH_NDS2_CAM_01_01.xsd', 4);
exception when others then null;
end;
/
begin
	DBMS_XMLSCHEMA.deleteSchema('TAX3EXCH_NDS2_CAM_02_01.xsd', 4);
exception when others then null;
end;
/
begin
	DBMS_XMLSCHEMA.deleteSchema('TAX3EXCH_NDS2_CAM_03_01.xsd', 4);
exception when others then null;
end;
/  
begin
	DBMS_XMLSCHEMA.deleteSchema('TAX3EXCH_NDS2_CAM_04_01.xsd', 4);
exception when others then null;
end;  
/
BEGIN

  DBMS_XMLSCHEMA.registerSchema(
    SCHEMAURL => 'TAX3EXCH_NDS2_CAM_01_01.xsd',
    SCHEMADOC => bfilename('EXP_DIR','TAX3EXCH_NDS2_CAM_01_01.xsd'),
     --genTypes => false, 
     genTables => false, 
     local => true
    );
  DBMS_XMLSCHEMA.registerSchema(
    SCHEMAURL => 'TAX3EXCH_NDS2_CAM_02_01.xsd',
    SCHEMADOC => bfilename('EXP_DIR','TAX3EXCH_NDS2_CAM_02_01.xsd'),
     --genTypes => false, 
     genTables => false, 
     local => true
    );
  DBMS_XMLSCHEMA.registerSchema(
    SCHEMAURL => 'TAX3EXCH_NDS2_CAM_03_01.xsd',
    SCHEMADOC => bfilename('EXP_DIR','TAX3EXCH_NDS2_CAM_03_01.xsd'),
     --genTypes => false, 
     genTables => false, 
     local => true
    );
END;
/
