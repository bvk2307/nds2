﻿/*rollback stage 2*/
truncate table doc;
truncate table doc_invoice;
truncate table control_ratio_doc;
truncate table seod_data_queue;
truncate table system_log;
update sov_discrepancy set stage = null where stage = 2;
commit;
