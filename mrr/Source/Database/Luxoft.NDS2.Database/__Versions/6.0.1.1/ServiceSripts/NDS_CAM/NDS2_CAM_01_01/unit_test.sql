﻿declare
begin
  for line in (select * from v$cam_outgoing)
    loop
      PAC$CAM_SYNC.ACCEPT(P_OBJ_ID => line.object_reg_num, P_OBJ_TYPE_ID => line.object_type_id);
    end loop;
end;


/*Validate xml*/
declare 
xmlT XmlType;
decId number;
begin
for line in (select * from i$nds2.v$cam_outgoing)
  loop
    begin
        xmlT := XmlType(line.xml_data);
        dbms_output.put_line(line.declartion_reg_num||'-'||xmlT.isSchemaValid('TAX3EXCH_NDS2_CAM_01_01.xsd'));
    exception when others then dbms_output.put_line('error '||line.declartion_reg_num);
    end;
  end loop;
end;


  begin
  DBMS_XMLSCHEMA.deleteSchema('TAX3EXCH_NDS2_CAM_01_01.xsd', 4);
  exception when others then null;
  end;
  

BEGIN

  DBMS_XMLSCHEMA.registerSchema(
    SCHEMAURL => 'TAX3EXCH_NDS2_CAM_01_01.xsd',
    SCHEMADOC => bfilename('EXP_DIR','TAX3EXCH_NDS2_CAM_02_01.xsd'),
     genTypes => false, 
     genTables => false, 
     local => true
    --CSID      => nls_charset_id('AL32UTF8')
    );
END;
