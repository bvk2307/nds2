﻿CREATE OR REPLACE PACKAGE NDS2_MRR_USER.PAC$DOCUMENT
as
   PROCEDURE PROCESS_CLAIM_FOR_SIDE_1;
   PROCEDURE PROCESS_PREP_CLAIM_FOR_SIDE_2;
   PROCEDURE PROCESS_CLAIM_FOR_SIDE_2;
   PROCEDURE PROCESS_RECLAIM_FOR_SIDE_1;
   PROCEDURE PROCESS_RECLAIM_FOR_SIDE_2;
END;
/
CREATE OR REPLACE PACKAGE BODY NDS2_MRR_USER.PAC$DOCUMENT
as

  SEL_STATUS_PREP_SENT CONSTANT NUMBER(1)       := 4;
  SEL_STATUS_SENT CONSTANT NUMBER(2)            := 10;
  SOV_REQUEST_STATUS_DONE CONSTANT NUMBER(1)    := 2;
  DATE_FORMAT CONSTANT varchar2(12)             := 'DD.MM.YYYY';
  DECIMAL_FORMAT CONSTANT varchar2(18)          := '9999999990.00';

FUNCTION UTL_FORMAT_DECIMAL(v_val in number)
return varchar2
is
begin
  return trim(to_char(v_val, DECIMAL_FORMAT));
end;

FUNCTION UTL_FORMAT_DATE(v_val in DATE)
return varchar2
is
begin
  return to_char(v_val, DATE_FORMAT);
end;

PROCEDURE UTL_REMOVE_IF_EMPTY(p_parent in xmldom.DOMNode, p_node in xmldom.DOMNode)
as
d_node xmldom.DOMNode;
begin
   if dbms_xmldom.getLength(dbms_xmldom.getChildNodes(p_node)) < 1 then
     d_node:= dbms_xmldom.removeChild(p_parent, p_node);
   end if;
end;

PROCEDURE UTL_GET_TAXPERIOD_BOUNDS
(
  p_period in varchar2,
  p_year in varchar2,
  p_start_date out varchar2,
  p_end_date out varchar2
)
as
begin
 p_start_date := '01.01.2001';
 p_end_date := '01.01.2001';
end;

PROCEDURE UTL_XML_APPEND_ATTRIBUTE
  (
    p_elm in xmldom.DOMElement
   ,p_attrName in varchar2
   ,p_attrValue in varchar2
  )
as
begin
if length(nvl(p_attrValue, '')) > 0 then
  dbms_xmldom.setAttribute(p_elm, p_attrName, p_attrValue);
end if;
end;



PROCEDURE LOG(p_msg varchar2)
as
begin
  dbms_output.put_line(p_msg);
end;

/* ############################ */
/* # Формирование строки 1.1 ## */
/* ############################ */
FUNCTION CLAIM_BUILD_T1_1
(
   p_doc in xmldom.DOMDocument,
   p_container_node in xmldom.DOMNode,
   p_invoice_row in sov_invoice%rowtype
)
return boolean
as
  v_itemElement xmldom.DOMElement;
  v_itemNode xmldom.DOMNode;

  v_taxPayerInfoElement xmldom.DOMElement;
  v_taxPayerInfoNode xmldom.DOMNode;

  v_taxPayerElement xmldom.DOMElement;
  v_taxPayerNode xmldom.DOMNode;
begin
  /*нода строки 1.1*/
  v_itemElement := xmldom.createElement(p_doc, 'T1.1Стр');
  v_itemNode := xmldom.appendChild(p_container_node, xmldom.makeNode(v_itemElement));
  /*обязательные атрибуты*/

  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомерПор',        p_invoice_row.ordinal_number);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'КодВидОпер',      p_invoice_row.Operation_Code);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомСчФПрод',      p_invoice_row.Invoice_Num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СтоимПокупВ',     UTL_FORMAT_DECIMAL(p_invoice_row.Price_Buy_Amount));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СумНДС',          UTL_FORMAT_DECIMAL(p_invoice_row.Price_Buy_Nds_Amount));
  /*Не обязательные атрибуты*/
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаСчФПрод',     UTL_FORMAT_DATE(p_invoice_row.Invoice_Date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомИспрСчФ',      p_invoice_row.Change_Num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаИспрСчФ',     UTL_FORMAT_DATE(p_invoice_row.Change_Date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомКСчФПрод',     p_invoice_row.Correction_Num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаКСчФПрод',    UTL_FORMAT_DATE(p_invoice_row.Correction_Date ));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомИспрКСчФ',     p_invoice_row.Change_Correction_Num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаИспрКСчФ',    UTL_FORMAT_DATE(p_invoice_row.Change_Correction_Date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомДокПдтвОпл',   p_invoice_row.Receipt_Doc_Num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаДокПдтвОпл',  UTL_FORMAT_DATE(p_invoice_row.Receipt_Doc_Date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаУчТов',       UTL_FORMAT_DATE(p_invoice_row.Buy_Accept_Date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомТД',           p_invoice_row.Customs_Declaration_Num);
  --              xmldom.setAttribute(ch8elmItem, 'ОКВ', invLine.);


  /*сведения о продавце*/
  if length(p_invoice_row.seller_inn) > 1 then /*при расхждении типа Разрыв, данной тнформации может не быть*/
    v_taxPayerInfoElement := xmldom.createElement(p_doc, 'СвПрод');
    v_taxPayerInfoNode := xmldom.appendChild(v_itemNode, xmldom.makeNode(v_taxPayerInfoElement));
    if length(p_invoice_row.seller_kpp) > 1 then
      v_taxPayerElement := xmldom.createElement(p_doc, 'СведЮЛ');
      xmldom.setAttribute(v_taxPayerElement, 'ИННЮЛ',    p_invoice_row.seller_inn);
      xmldom.setAttribute(v_taxPayerElement, 'КПП',      p_invoice_row.seller_kpp);
      v_taxPayerNode := xmldom.appendChild(v_taxPayerInfoNode, xmldom.makeNode(v_taxPayerElement));
    else
      v_taxPayerElement := xmldom.createElement(p_doc, 'СведИП');
      xmldom.setAttribute(v_taxPayerElement, 'ИННЮЛ', p_invoice_row.seller_inn);
      v_taxPayerNode := xmldom.appendChild(v_taxPayerInfoNode, xmldom.makeNode(v_taxPayerElement));
    end if;
  end if;

  /*сведения о посреднике*/
  if length(p_invoice_row.broker_inn) > 1 then
     v_taxPayerInfoElement := xmldom.createElement(p_doc, 'СвПос');
     v_taxPayerInfoNode := xmldom.appendChild(v_itemNode, xmldom.makeNode(v_taxPayerInfoElement));

    if length(p_invoice_row.broker_kpp) > 1 then
      v_taxPayerElement := xmldom.createElement(p_doc, 'СведЮЛ');
      xmldom.setAttribute(v_taxPayerElement, 'ИННЮЛ', p_invoice_row.broker_inn);
      xmldom.setAttribute(v_taxPayerElement, 'КПП',   p_invoice_row.broker_kpp);
      v_taxPayerNode := xmldom.appendChild(v_taxPayerInfoNode, xmldom.makeNode(v_taxPayerElement));
    else
      v_taxPayerElement := xmldom.createElement(p_doc, 'СведИП');
      xmldom.setAttribute(v_taxPayerElement, 'ИННЮЛ', p_invoice_row.broker_inn);
      v_taxPayerNode := xmldom.appendChild(v_taxPayerInfoNode, xmldom.makeNode(v_taxPayerElement));
    end if;
  end if;

  return true;
exception when others then
  /*log error*/
  dbms_output.put_line('CLAIM_BUILD_T1_1:'||sqlcode||'-'||substr(sqlerrm, 0, 256));
  return false;
end;

/* ############################ */
/* # Формирование строки 1.2 ## */
/* ############################ */
FUNCTION CLAIM_BUILD_T1_2
(
   p_doc in xmldom.DOMDocument,
   p_container_node in xmldom.DOMNode,
   p_invoice_row in sov_invoice%rowtype
)
return boolean
as
  v_itemElement xmldom.DOMElement;
  v_itemNode xmldom.DOMNode;

  v_taxPayerInfoElement xmldom.DOMElement;
  v_taxPayerInfoNode xmldom.DOMNode;

  v_taxPayerElement xmldom.DOMElement;
  v_taxPayerNode xmldom.DOMNode;
begin
  /*нода строки 1.2*/
  v_itemElement := xmldom.createElement(p_doc, 'T1.2Стр');
  v_itemNode := xmldom.appendChild(p_container_node, xmldom.makeNode(v_itemElement));
  /*обязательные*/
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомерПор',       p_invoice_row.ordinal_number);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'КодВидОпер',     p_invoice_row.Operation_Code);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомСчФПрод',     p_invoice_row.Invoice_Num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаСчФПрод',    UTL_FORMAT_DATE(p_invoice_row.Invoice_Date));
  /*Не обязательные*/
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомИспрСчФ',     p_invoice_row.Change_Num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаИспрСчФ',    UTL_FORMAT_DATE(p_invoice_row.Change_Date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомКСчФПрод',    p_invoice_row.Correction_Num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаКСчФПрод',   UTL_FORMAT_DATE(p_invoice_row.Correction_Date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомИспрКСчФ',    p_invoice_row.Change_Correction_Num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаИспрКСчФ',   UTL_FORMAT_DATE(p_invoice_row.Change_Correction_Date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомДокПдтвОпл',  p_invoice_row.Receipt_Doc_Num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаДокПдтвОпл', UTL_FORMAT_DATE(p_invoice_row.Receipt_Doc_Date));
  --UTL_XML_APPEND_ATTRIBUTE(ch9elmItem, 'ОКВ', invLine.);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СтоимПродСФВ',   UTL_FORMAT_DECIMAL(p_invoice_row.Price_Sell_In_Curr));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СтоимПродСФ',    UTL_FORMAT_DECIMAL(p_invoice_row.Price_Sell));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СтоимПродСФ18',  UTL_FORMAT_DECIMAL(p_invoice_row.Price_Sell_18));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СтоимПродСФ10',  UTL_FORMAT_DECIMAL(p_invoice_row.Price_Sell_10));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СтоимПродСФ0',   UTL_FORMAT_DECIMAL(p_invoice_row.Price_Sell_0));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СумНДССФ18',     UTL_FORMAT_DECIMAL(p_invoice_row.Price_Nds_18));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СумНДССФ10',     UTL_FORMAT_DECIMAL(p_invoice_row.Price_Nds_10));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СтоимПродОсв',   UTL_FORMAT_DECIMAL(p_invoice_row.Price_Tax_Free));

  /*сведения о покупателе*/
  if length(p_invoice_row.buyer_inn) > 1 then
    v_taxPayerInfoElement := xmldom.createElement(p_doc, 'СвПокуп');
    v_taxPayerInfoNode := xmldom.appendChild(v_itemNode, xmldom.makeNode(v_taxPayerInfoElement));
    if length(p_invoice_row.buyer_kpp) > 1 then
      v_taxPayerElement := xmldom.createElement(p_doc, 'СведЮЛ');
      xmldom.setAttribute(v_taxPayerElement, 'ИННЮЛ',    p_invoice_row.buyer_inn);
      xmldom.setAttribute(v_taxPayerElement, 'КПП',      p_invoice_row.buyer_kpp);
      v_taxPayerNode := xmldom.appendChild(v_taxPayerInfoNode, xmldom.makeNode(v_taxPayerElement));
    else
      v_taxPayerElement := xmldom.createElement(p_doc, 'СведИП');
      xmldom.setAttribute(v_taxPayerElement, 'ИННЮЛ', p_invoice_row.buyer_inn);
      v_taxPayerNode := xmldom.appendChild(v_taxPayerInfoNode, xmldom.makeNode(v_taxPayerElement));
    end if;
  end if;

  /*сведения о посреднике*/
  if length(p_invoice_row.broker_inn) > 1 then
     v_taxPayerInfoElement := xmldom.createElement(p_doc, 'СвПос');
     v_taxPayerInfoNode := xmldom.appendChild(v_itemNode, xmldom.makeNode(v_taxPayerInfoElement));

    if length(p_invoice_row.broker_kpp) > 1 then
      v_taxPayerElement := xmldom.createElement(p_doc, 'СведЮЛ');
      xmldom.setAttribute(v_taxPayerElement, 'ИННЮЛ', p_invoice_row.broker_inn);
      xmldom.setAttribute(v_taxPayerElement, 'КПП',   p_invoice_row.broker_kpp);
      v_taxPayerNode := xmldom.appendChild(v_taxPayerInfoNode, xmldom.makeNode(v_taxPayerElement));
    else
      v_taxPayerElement := xmldom.createElement(p_doc, 'СведИП');
      xmldom.setAttribute(v_taxPayerElement, 'ИННЮЛ', p_invoice_row.broker_inn);
      v_taxPayerNode := xmldom.appendChild(v_taxPayerInfoNode, xmldom.makeNode(v_taxPayerElement));
    end if;
  end if;

  return true;
exception when others then
  /*log error*/
  dbms_output.put_line('CLAIM_BUILD_T1_2:'||sqlcode||'-'||substr(sqlerrm, 0, 256));
  return false;
end;

/* ############################ */
/* # Формирование строки 1.3 ## */
/* ############################ */
FUNCTION CLAIM_BUILD_T1_3
(
   p_doc in xmldom.DOMDocument,
   p_container_node in xmldom.DOMNode,
   p_invoice_row in sov_invoice%rowtype
)
return boolean
as
  v_itemElement xmldom.DOMElement;
  v_itemNode xmldom.DOMNode;

  v_taxPayerInfoElement xmldom.DOMElement;
  v_taxPayerInfoNode xmldom.DOMNode;

  v_taxPayerElement xmldom.DOMElement;
  v_taxPayerNode xmldom.DOMNode;
begin
  /*нода строки 1.3*/
  v_itemElement := xmldom.createElement(p_doc, 'T1.3Стр');
  v_itemNode := xmldom.appendChild(p_container_node, xmldom.makeNode(v_itemElement));
  /*обязательные*/
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомерПор',       p_invoice_row.ordinal_number);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаВыст',       UTL_FORMAT_DATE(p_invoice_row.Create_Date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'КодВидОпер',     p_invoice_row.Operation_Code);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомСчФПрод',     p_invoice_row.Invoice_Num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаСчФПрод',    UTL_FORMAT_DATE(p_invoice_row.Invoice_Date));
  /*Не обязательные*/
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомИспрСчФ',     p_invoice_row.Change_Num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаИспрСчФ',    UTL_FORMAT_DATE(p_invoice_row.Change_Date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомКСчФПрод',    p_invoice_row.Correction_Num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаКСчФПрод',   UTL_FORMAT_DATE(p_invoice_row.Correction_Date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомИспрКСчФ',    p_invoice_row.Change_Correction_Num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаИспрКСчФ',   UTL_FORMAT_DATE(p_invoice_row.Change_Correction_Date));
--              xmldom.setAttribute(ch10elmItem, 'ОКВ', invLine.);

  /*сведения о покупателе*/
  if length(p_invoice_row.buyer_inn) > 1 then /*при расхждении типа Разрыв, данной тнформации может не быть*/
    v_taxPayerInfoElement := xmldom.createElement(p_doc, 'СвПокуп');
    v_taxPayerInfoNode := xmldom.appendChild(v_itemNode, xmldom.makeNode(v_taxPayerInfoElement));
    if length(p_invoice_row.buyer_kpp) > 1 then
      v_taxPayerElement := xmldom.createElement(p_doc, 'СведЮЛ');
      xmldom.setAttribute(v_taxPayerElement, 'ИННЮЛ',    p_invoice_row.buyer_inn);
      xmldom.setAttribute(v_taxPayerElement, 'КПП',      p_invoice_row.buyer_kpp);
      v_taxPayerNode := xmldom.appendChild(v_taxPayerInfoNode, xmldom.makeNode(v_taxPayerElement));
    else
      v_taxPayerElement := xmldom.createElement(p_doc, 'СведИП');
      xmldom.setAttribute(v_taxPayerElement, 'ИННЮЛ', p_invoice_row.buyer_inn);
      v_taxPayerNode := xmldom.appendChild(v_taxPayerInfoNode, xmldom.makeNode(v_taxPayerElement));
    end if;
  end if;
  /*!!!!TODO!!!!! СвПосрДеят - как заполняем?*/
  return true;
exception when others then
  /*log error*/
  dbms_output.put_line('CLAIM_BUILD_T1_2:'||sqlcode||'-'||substr(sqlerrm, 0, 256));
  return false;
end;

/* ############################ */
/* # Формирование строки 1.4 ## */
/* ############################ */
FUNCTION CLAIM_BUILD_T1_4
(
   p_doc in xmldom.DOMDocument,
   p_container_node in xmldom.DOMNode,
   p_invoice_row in sov_invoice%rowtype
)
return boolean
as
  v_itemElement xmldom.DOMElement;
  v_itemNode xmldom.DOMNode;

  v_taxPayerInfoElement xmldom.DOMElement;
  v_taxPayerInfoNode xmldom.DOMNode;

  v_taxPayerElement xmldom.DOMElement;
  v_taxPayerNode xmldom.DOMNode;
begin
  /*нода строки 1.4*/
  v_itemElement := xmldom.createElement(p_doc, 'T1.4Стр');
  v_itemNode := xmldom.appendChild(p_container_node, xmldom.makeNode(v_itemElement));
  /*обязательные*/
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомерПор',       p_invoice_row.ordinal_number);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаПолуч',      UTL_FORMAT_DATE(p_invoice_row.Receive_Date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'КодВидОпер',     p_invoice_row.Operation_Code);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомСчФПрод',     p_invoice_row.Invoice_Num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаСчФПрод',    UTL_FORMAT_DATE(p_invoice_row.Invoice_Date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СтоимТовСчФВс',  UTL_FORMAT_DECIMAL(p_invoice_row.Price_Total));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'КодВидСд',       p_invoice_row.Deal_Kind_Code);
  /*Не обязательные*/
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомИспрСчФ',     p_invoice_row.Change_Num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаИспрСчФ',    UTL_FORMAT_DATE(p_invoice_row.Change_Date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомКСчФПрод',    p_invoice_row.Correction_Num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаКСчФПрод',   UTL_FORMAT_DATE(p_invoice_row.Correction_Date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомИспрКСчФ',    p_invoice_row.Change_Correction_Num);
--              UTL_XML_APPEND_ATTRIBUTE(ch10elmItem, 'ОКВ', invLine.);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СумНДССчФ',      UTL_FORMAT_DECIMAL(p_invoice_row.Price_Nds_Total));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'РазСтКСчФУм',    UTL_FORMAT_DECIMAL(p_invoice_row.Diff_Correct_Decrease));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'РазСтКСчФУв',    UTL_FORMAT_DECIMAL(p_invoice_row.Diff_Correct_Increase));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'РазНДСКСчФУм',   UTL_FORMAT_DECIMAL(p_invoice_row.Diff_Correct_Nds_Decrease));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'РазНДСКСчФУв',   UTL_FORMAT_DECIMAL(p_invoice_row.Diff_Correct_Nds_Increase));


  /*сведения о продавце*/
  if length(p_invoice_row.seller_inn) > 1 then /*при расхждении типа Разрыв, данной тнформации может не быть*/
    v_taxPayerInfoElement := xmldom.createElement(p_doc, 'СвПрод');
    v_taxPayerInfoNode := xmldom.appendChild(v_itemNode, xmldom.makeNode(v_taxPayerInfoElement));
    if length(p_invoice_row.seller_kpp) > 1 then
      v_taxPayerElement := xmldom.createElement(p_doc, 'СведЮЛ');
      xmldom.setAttribute(v_taxPayerElement, 'ИННЮЛ',    p_invoice_row.seller_inn);
      xmldom.setAttribute(v_taxPayerElement, 'КПП',      p_invoice_row.seller_kpp);
      v_taxPayerNode := xmldom.appendChild(v_taxPayerInfoNode, xmldom.makeNode(v_taxPayerElement));
    else
      v_taxPayerElement := xmldom.createElement(p_doc, 'СведИП');
      xmldom.setAttribute(v_taxPayerElement, 'ИННЮЛ', p_invoice_row.seller_inn);
      v_taxPayerNode := xmldom.appendChild(v_taxPayerInfoNode, xmldom.makeNode(v_taxPayerElement));
    end if;
  end if;

  /*сведения о СвСубком*/
  /*if length(p_invoice_row.broker_inn) > 1 then
     v_taxPayerInfoElement := xmldom.createElement(p_doc, 'СвСубком');
     v_taxPayerInfoNode := xmldom.appendChild(v_itemNode, xmldom.makeNode(v_taxPayerInfoElement));

    if length(p_invoice_row.broker_kpp) > 1 then
      v_taxPayerElement := xmldom.createElement(p_doc, 'СведЮЛ');
      xmldom.setAttribute(v_taxPayerElement, 'ИННЮЛ', p_invoice_row.broker_inn);
      xmldom.setAttribute(v_taxPayerElement, 'КПП',   p_invoice_row.broker_kpp);
      v_taxPayerNode := xmldom.appendChild(v_taxPayerInfoNode, xmldom.makeNode(v_taxPayerElement));
    else
      v_taxPayerElement := xmldom.createElement(p_doc, 'СведИП');
      xmldom.setAttribute(v_taxPayerElement, 'ИННЮЛ', p_invoice_row.broker_inn);
      v_taxPayerNode := xmldom.appendChild(v_taxPayerInfoNode, xmldom.makeNode(v_taxPayerElement));
    end if;
  end if;*/

  return true;
exception when others then
  /*log error*/
  dbms_output.put_line('CLAIM_BUILD_T1_1:'||sqlcode||'-'||substr(sqlerrm, 0, 256));
  return false;
end;

/* ############################ */
/* # Формирование строки 1.5 ## */
/* ############################ */
FUNCTION CLAIM_BUILD_T1_5
(
   p_doc in xmldom.DOMDocument,
   p_container_node in xmldom.DOMNode,
   p_invoice_row in sov_invoice%rowtype
)
return boolean
as
  v_itemElement xmldom.DOMElement;
  v_itemNode xmldom.DOMNode;

  v_taxPayerInfoElement xmldom.DOMElement;
  v_taxPayerInfoNode xmldom.DOMNode;

  v_taxPayerElement xmldom.DOMElement;
  v_taxPayerNode xmldom.DOMNode;
begin
  /*нода строки 1.5*/
  v_itemElement := xmldom.createElement(p_doc, 'T1.5Стр');
  v_itemNode := xmldom.appendChild(p_container_node, xmldom.makeNode(v_itemElement));
  /*обязательные*/
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'НомСчФ',         p_invoice_row.Invoice_Num);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'ДатаСчФ',        UTL_FORMAT_DATE(p_invoice_row.Invoice_Date));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СтоимТовБНалВс', UTL_FORMAT_DECIMAL(p_invoice_row.Price_Total));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СумНалПокуп',    UTL_FORMAT_DECIMAL(p_invoice_row.Price_Nds_Buyer));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СтоимТовСНалВс', UTL_FORMAT_DECIMAL(p_invoice_row.Price_Nds_Total));

  /*сведения о покупателе*/
  if length(p_invoice_row.buyer_inn) > 1 then
    v_taxPayerInfoElement := xmldom.createElement(p_doc, 'СвПокуп');
    v_taxPayerInfoNode := xmldom.appendChild(v_itemNode, xmldom.makeNode(v_taxPayerInfoElement));
    if length(p_invoice_row.buyer_kpp) > 1 then
      v_taxPayerElement := xmldom.createElement(p_doc, 'СведЮЛ');
      xmldom.setAttribute(v_taxPayerElement, 'ИННЮЛ',    p_invoice_row.buyer_inn);
      xmldom.setAttribute(v_taxPayerElement, 'КПП',      p_invoice_row.buyer_kpp);
      v_taxPayerNode := xmldom.appendChild(v_taxPayerInfoNode, xmldom.makeNode(v_taxPayerElement));
    else
      v_taxPayerElement := xmldom.createElement(p_doc, 'СведИП');
      xmldom.setAttribute(v_taxPayerElement, 'ИННЮЛ', p_invoice_row.buyer_inn);
      v_taxPayerNode := xmldom.appendChild(v_taxPayerInfoNode, xmldom.makeNode(v_taxPayerElement));
    end if;
  end if;

  return true;
exception when others then
  /*log error*/
  dbms_output.put_line('CLAIM_BUILD_T1_2:'||sqlcode||'-'||substr(sqlerrm, 0, 256));
  return false;
end;

/* ############################ */
/* # Формирование строки 1.7 ## */
/* ############################ */
FUNCTION CLAIM_BUILD_T1_7
(
   p_doc in xmldom.DOMDocument,
   p_container_node in xmldom.DOMNode,
   p_violation_info in varchar2,
   p_cr_code in varchar2,
   p_left_side in number,
   p_rigth_side in number
)
return boolean
as
  v_itemElement xmldom.DOMElement;
  v_itemNode xmldom.DOMNode;


begin
  /*нода строки 1.7*/
  v_itemElement := xmldom.createElement(p_doc, 'T1.7Стр');
  v_itemNode := xmldom.appendChild(p_container_node, xmldom.makeNode(v_itemElement));
  /*обязательные*/
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СвНар',       p_violation_info);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'КодКС',       p_cr_code);
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СумЛевЧ',     UTL_FORMAT_DECIMAL(p_left_side));
  UTL_XML_APPEND_ATTRIBUTE(v_itemElement, 'СумПравЧ',    UTL_FORMAT_DECIMAL(p_rigth_side));

  return true;
exception when others then
  /*log error*/
  dbms_output.put_line('CLAIM_BUILD_T1_2:'||sqlcode||'-'||substr(sqlerrm, 0, 256));
  return false;
end;

FUNCTION RECLAIM_BUILD_93
(
   p_doc in xmldom.DOMDocument,
   p_container_node in xmldom.DOMNode,
   p_invoice_row in sov_invoice%rowtype,
   p_tax_period in varchar2,
   p_tax_year in varchar2
)
return boolean
is
  v_itemElement xmldom.DOMElement;
  v_itemNode xmldom.DOMNode;
  
  v_invoiceElement xmldom.DOMElement;
  v_invoiceNode xmldom.DOMNode;

  v_dateElement xmldom.DOMElement;
  v_dateNode xmldom.DOMNode;
  
  v_startPeriod varchar2(10 CHAR);
  v_endPeriod varchar2(10 CHAR);
begin
  /*нода строки*/
  v_itemElement := xmldom.createElement(p_doc, 'Истреб93Док');
  v_itemNode := xmldom.appendChild(p_container_node, xmldom.makeNode(v_itemElement));
  /*обязательные атрибуты*/
  UTL_GET_TAXPERIOD_BOUNDS(p_tax_period, p_tax_year, v_startPeriod, v_endPeriod);
  if p_invoice_row.correction_num is null then
     v_invoiceElement := xmldom.createElement(p_doc, 'КСФ');
     v_invoiceNode := xmldom.appendChild(v_itemNode, xmldom.makeNode(v_invoiceElement));
     
     UTL_XML_APPEND_ATTRIBUTE(v_invoiceElement, 'КодДок', '0924');
     UTL_XML_APPEND_ATTRIBUTE(v_invoiceElement, 'НомДок', p_invoice_row.correction_num);
     
     if p_invoice_row.correction_date is null then
       v_dateElement := xmldom.createElement(p_doc, 'ПериодДок');
       v_dateNode := xmldom.appendChild(v_invoiceNode, xmldom.makeNode(v_dateElement));
       
       UTL_XML_APPEND_ATTRIBUTE(v_dateElement, 'НачПер', v_startPeriod);
       UTL_XML_APPEND_ATTRIBUTE(v_dateElement, 'ОконПер', v_endPeriod);
     else
       v_dateElement := xmldom.createElement(p_doc, 'ДатаДок');
       v_dateNode := xmldom.appendChild(v_invoiceNode, xmldom.makeNode(v_dateElement));
     end if;
  else
     v_invoiceElement := xmldom.createElement(p_doc, 'СФ');
     v_invoiceNode := xmldom.appendChild(v_itemNode, xmldom.makeNode(v_invoiceElement));
     
     UTL_XML_APPEND_ATTRIBUTE(v_invoiceElement, 'КодДок', '2772');
     UTL_XML_APPEND_ATTRIBUTE(v_invoiceElement, 'НомДок', p_invoice_row.invoice_num);    
      
     if p_invoice_row.correction_date is null then
       v_dateElement := xmldom.createElement(p_doc, 'ПериодДок');
       v_dateNode := xmldom.appendChild(v_invoiceNode, xmldom.makeNode(v_dateElement));
       
       UTL_XML_APPEND_ATTRIBUTE(v_dateElement, 'НачПер', v_startPeriod);
       UTL_XML_APPEND_ATTRIBUTE(v_dateElement, 'ОконПер', v_endPeriod);
     else
       v_dateElement := xmldom.createElement(p_doc, 'ДатаДок');
       v_dateNode := xmldom.appendChild(v_invoiceNode, xmldom.makeNode(v_dateElement));
     end if;    
  end if;
  
  return true;
  
  exception when others then 
    return false;
  
end;

PROCEDURE CREATE_DECLARATION_CLAIM
(
  p_decl_ver_id in number,
  p_sono_code in varchar2,
  p_seod_decl_regnum in number,
  p_claim_mode in number := 1 /*1 - СФ, 2 - КС*/
)
as
  doc xmldom.DOMDocument;
  docXml xmltype;
  main_node xmldom.DOMNode;
  root_node xmldom.DOMNode;
  user_node xmldom.DOMNode;
  item_node xmldom.DOMNode;
  root_elmt xmldom.DOMElement;
  item_elmt xmldom.DOMElement;
  item_text xmldom.DOMText;
  discrepElement xmldom.DOMElement;
  discrepNode xmldom.DOMNode;

  v_T11_ListElement xmldom.DOMElement;
  v_T11_ListNode xmldom.DOMNode;
  v_T12_ListElement xmldom.DOMElement;
  v_T12_ListNode xmldom.DOMNode;
  v_T13_ListElement xmldom.DOMElement;
  v_T13_ListNode xmldom.DOMNode;
  v_T14_ListElement xmldom.DOMElement;
  v_T14_ListNode xmldom.DOMNode;
  v_T15_ListElement xmldom.DOMElement;
  v_T15_ListNode xmldom.DOMNode;
  v_T16_ListElement xmldom.DOMElement;
  v_T16_ListNode xmldom.DOMNode;
  v_T17_ListElement xmldom.DOMElement;
  v_T17_ListNode xmldom.DOMNode;

  v_itemNode xmldom.DOMNode;

  v_count_of_errors number(3) := 0;
  v_count_of_processed_data number(19) := 0;
  v_document_id number(19);
  v_nds_decl_id  number(38);

  v_success_operation boolean := false;
begin
  v_document_id := SEQ_DOC.NEXTVAL;
  select id into v_nds_decl_id from v$declaration_history where declaration_version_id = p_decl_ver_id;
  
  
  doc := xmldom.newDOMDocument();
  main_node := xmldom.makeNode(doc);
  root_elmt := xmldom.createElement(doc, 'Автотребование');
  xmldom.setAttribute(root_elmt, 'УчНомТреб', v_document_id);
  xmldom.setAttribute(root_elmt, 'ТипИнф', 'TAX3EXCH_NDS2_CAM_01');
  xmldom.setAttribute(root_elmt, 'РегНомДек', p_seod_decl_regnum);
  xmldom.setAttribute(root_elmt, 'КодНО', p_sono_code);
  xmldom.setAttribute(root_elmt, 'xsi:noNamespaceSchemaLocation', 'TAX3EXCH_NDS2_CAM_01_01.xsd');
  xmldom.setAttribute(root_elmt, 'xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
  root_node := xmldom.appendChild(main_node, xmldom.makeNode(root_elmt));

  discrepElement := xmldom.createElement(doc, 'ПризнРсхжд');
  discrepNode := xmldom.appendChild(root_node, xmldom.makeNode(discrepElement));

  v_T11_ListElement := xmldom.createElement(doc, 'T1.1');
  v_T11_ListNode := xmldom.appendChild(discrepNode, xmldom.makeNode(v_T11_ListElement));

  v_T12_ListElement := xmldom.createElement(doc, 'T1.2');
  v_T12_ListNode := xmldom.appendChild(discrepNode, xmldom.makeNode(v_T12_ListElement));

  v_T13_ListElement := xmldom.createElement(doc, 'T1.3');
  v_T13_ListNode := xmldom.appendChild(discrepNode, xmldom.makeNode(v_T13_ListElement));

  v_T14_ListElement := xmldom.createElement(doc, 'T1.4');
  v_T14_ListNode := xmldom.appendChild(discrepNode, xmldom.makeNode(v_T14_ListElement));

  v_T15_ListElement := xmldom.createElement(doc, 'T1.5');
  v_T15_ListNode := xmldom.appendChild(discrepNode, xmldom.makeNode(v_T15_ListElement));

  v_T16_ListElement := xmldom.createElement(doc, 'T1.6');
  v_T16_ListNode := xmldom.appendChild(discrepNode, xmldom.makeNode(v_T16_ListElement));

  v_T17_ListElement := xmldom.createElement(doc, 'T1.7');
  v_T17_ListNode := xmldom.appendChild(discrepNode, xmldom.makeNode(v_T17_ListElement));
/*обработка счетов фактур*/
if p_claim_mode = 1 then

    for discrepancy_invoice_line in
    (
      select
        si.*
      from
      SEOD_DATA_QUEUE sdq
      inner join sov_invoice si on si.request_id = sdq.invoice_request_id
      inner join v$declaration d on d.ID = sdq.target_declaration_id and d.DECLARATION_VERSION_ID = p_decl_ver_id
      where sdq.for_stage in (2,3) and sdq.ref_doc_id is null
    )
    loop
      v_count_of_processed_data := v_count_of_processed_data + 1;

      if discrepancy_invoice_line.chapter = 8 then
         v_success_operation := CLAIM_BUILD_T1_1(doc, v_T11_ListNode, discrepancy_invoice_line);
      end if;

      if discrepancy_invoice_line.chapter = 9 then
         v_success_operation := CLAIM_BUILD_T1_2(doc, v_T12_ListNode, discrepancy_invoice_line);
      end if;

      if discrepancy_invoice_line.chapter = 10 then
         v_success_operation := CLAIM_BUILD_T1_3(doc, v_T13_ListNode, discrepancy_invoice_line);
      end if;

      if discrepancy_invoice_line.chapter = 11 then
         v_success_operation := CLAIM_BUILD_T1_4(doc, v_T14_ListNode, discrepancy_invoice_line);
      end if;

      if discrepancy_invoice_line.chapter = 12 then
         v_success_operation := CLAIM_BUILD_T1_5(doc, v_T15_ListNode, discrepancy_invoice_line);
      end if;

      if v_success_operation then
        insert into doc_invoice(doc_id, invoice_row_key, invoice_chapter)
        values (v_document_id, discrepancy_invoice_line.row_key, discrepancy_invoice_line.chapter);
        
        update SEOD_DATA_QUEUE sdq set sdq.ref_doc_id = v_document_id 
        where 
          sdq.target_invoice_row_key = discrepancy_invoice_line.row_key
          and sdq.for_stage in(2,3)
          and sdq.target_declaration_id = v_nds_decl_id;
      else
        dbms_output.put_line('errors');
        exit;
      end if;
    end loop;
end if;

/*обработка контрольный соотношений*/
if p_claim_mode = 2 then

   for ksLine in
     (
       select
       vw.*,
       decode(vw.Vypoln, 0, 10, 1, 12, vw.Vypoln) as vypolnCode
       from V$ASKKontrSoontosh vw
       inner join v$askdekl d on d.ID = vw.IdDekl
       where d.ZIP = p_decl_ver_id
     )
       loop
      v_count_of_processed_data := v_count_of_processed_data + 1;
      v_success_operation := CLAIM_BUILD_T1_7(doc, v_T17_ListNode, ksLine.vypolnCode, ksLine.KodKs, ksLine.LevyaChast, ksLine.PravyaChast);

      insert into CONTROL_RATIO_DOC values(ksLine.Id, v_document_id, sysdate);

      if not v_success_operation then
        dbms_output.put_line('errors');
        exit;
      end if;
   end loop;
end if;

  if v_count_of_processed_data > 0 then
    LOG('PROCESSED - '||v_count_of_processed_data||' invoices for declaration '||p_seod_decl_regnum);
    UTL_REMOVE_IF_EMPTY(discrepNode, v_T11_ListNode);
    UTL_REMOVE_IF_EMPTY(discrepNode, v_T12_ListNode);
    UTL_REMOVE_IF_EMPTY(discrepNode, v_T13_ListNode);
    UTL_REMOVE_IF_EMPTY(discrepNode, v_T14_ListNode);
    UTL_REMOVE_IF_EMPTY(discrepNode, v_T15_ListNode);
    UTL_REMOVE_IF_EMPTY(discrepNode, v_T16_ListNode);
    UTL_REMOVE_IF_EMPTY(discrepNode, v_T17_ListNode);

    docXml := dbms_xmldom.getXmlType(doc);
    insert into doc(doc_id, ref_entity_id, doc_type, sono_code, create_date, status, document_body, stage)
    values (v_document_id, p_decl_ver_id, 1, p_sono_code, sysdate, 1, docXml.getClobVal(), 2);
  else
    null;
  end if;

end;

PROCEDURE CREATE_DECLARATION_RECLAIM
(
  p_decl_version_id in number,
  p_sono_code in varchar2,
  p_seod_decl_regnum in number,
  p_invoice_request_id in number
)
as
  doc xmldom.DOMDocument;
  docXml xmltype;
  main_node xmldom.DOMNode;
  root_node xmldom.DOMNode;
  user_node xmldom.DOMNode;
  item_node xmldom.DOMNode;
  root_elmt xmldom.DOMElement;
  item_elmt xmldom.DOMElement;
  item_text xmldom.DOMText;
  reclaimElement xmldom.DOMElement;
  reclaimNode xmldom.DOMNode;
  
  v_decl_period varchar2(2 char);
  v_decl_year varchar2(4 char);
  v_document_id number;
begin
  v_document_id := SEQ_DOC.NEXTVAL;

  doc := xmldom.newDOMDocument();
  main_node := xmldom.makeNode(doc);
  root_elmt := xmldom.createElement(doc, 'Истреб93');
  xmldom.setAttribute(root_elmt, 'КодНО', p_sono_code);
  xmldom.setAttribute(root_elmt, 'РегНомДек', p_seod_decl_regnum);
  xmldom.setAttribute(root_elmt, 'УчНомИстреб', v_document_id);
  xmldom.setAttribute(root_elmt, 'ТипИнф', 'TAX3EXCH_NDS2_CAM_02');
  xmldom.setAttribute(root_elmt, 'xsi:noNamespaceSchemaLocation', 'TAX3EXCH_NDS2_CAM_02_01.xsd');
  xmldom.setAttribute(root_elmt, 'xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
  
  root_node := xmldom.appendChild(main_node, xmldom.makeNode(root_elmt));

  reclaimElement := xmldom.createElement(doc, 'Истреб93Док');
  reclaimNode := xmldom.appendChild(root_node, xmldom.makeNode(reclaimElement));
  
  select t.PERIOD, t.OTCHETGOD into v_decl_period, v_decl_year from v$askdekl t where t.zip = p_decl_version_id;
  
  for invLine in (select * from sov_invoice where request_id = p_invoice_request_id)
  loop
    if RECLAIM_BUILD_93(doc, root_node, invLine, v_decl_period, v_decl_year) then
      insert into doc_invoice(doc_id, invoice_row_key, invoice_chapter) values(v_document_id, invLine.Row_Key, invLine.Chapter);
    else
      exit;
    end if;
  end loop;
end;

/*######################################################*/
/*Сбор данных для отправки астотребования первой стороне*/
/*######################################################*/
PROCEDURE STAGE_2_COLLECT
as
begin
  merge into SEOD_DATA_QUEUE sdq
  using
  (
   select
    dscrp.invoice_rk,
    dscrp.decl_id,
    s.invoice_request_id
    from
    selection s
    inner join selection_discrepancy sd on sd.request_id = s.request_id
    inner join sov_discrepancy dscrp on dscrp.id = sd.discrepancy_id
    inner join invoice_request ir on ir.id = s.invoice_request_id
    left join SEOD_DATA_QUEUE sdq1 on sdq1.invoice_request_id = ir.id
    where 
    sdq1.invoice_request_id is null and ir.status = 2
  ) trg
  on (trg.invoice_rk = sdq.target_invoice_row_key and sdq.for_stage = 2)
  when not matched then
  insert(sdq.target_invoice_row_key, sdq.target_declaration_id, sdq.invoice_request_id, sdq.for_stage, sdq.ref_doc_id) 
  values(trg.invoice_rk, trg.decl_id, trg.invoice_request_id, 2, null);
end;

PROCEDURE STAGE2_PROCESS
as
begin
  for decl_line in (select 
    distinct 
    decl.DECLARATION_VERSION_ID,
    decl.SOUN_CODE,
    sdecl.decl_reg_num
    from SEOD_DATA_QUEUE sdq 
    inner join v$declaration decl on decl.ID = sdq.target_declaration_id
    inner join seod_declaration sdecl on sdecl.nds2_id = decl.ID and sdecl.correction_number = decl.CORRECTION_NUMBER
    where sdq.for_stage in (2,3) 
    and sdq.ref_doc_id is null)
   loop
        CREATE_DECLARATION_CLAIM
        (
          decl_line.DECLARATION_VERSION_ID,
          decl_line.soun_code,
          decl_line.decl_reg_num
        );     
   end loop;
end;

PROCEDURE PROCESS_CLAIM_FOR_SIDE_1
as
pragma autonomous_transaction;
begin
  begin
  /*получим список выборок на отправку*/
   for ready_selection_lines in (select s.* from selection s
     inner join invoice_request ir on ir.id = s.invoice_request_id
     where s.status = SEL_STATUS_PREP_SENT and  ir.status = SOV_REQUEST_STATUS_DONE)
   loop
      /*получим список деклараций выборки на отправку*/
      for sel_declaration_line in
      (
        select vd.*, sed.decl_reg_num from V$DECLARATION vd
        inner join SELECTION_DECLARATION sd on sd.declaration_id = vd.ID
        inner join seod_declaration sed on sed.nds2_id = vd.ID and sed.correction_number = vd.CORRECTION_NUMBER
        where sd.request_id = ready_selection_lines.request_id
      )
      loop
        CREATE_DECLARATION_CLAIM(
        sel_declaration_line.declaration_version_id,
        sel_declaration_line.soun_code,
        sel_declaration_line.decl_reg_num,
        1
        );
      end loop;

      update selection set status = SEL_STATUS_SENT where id = ready_selection_lines.id;

   end loop;
   end;

   begin
     for ready_cr_decl in (select distinct decl_version_id, kodno, decl_reg_num
       from v$Control_Ratio_doc where doc_id is null) loop
      CREATE_DECLARATION_CLAIM(
      ready_cr_decl.decl_version_id,
      ready_cr_decl.kodno,
      ready_cr_decl.decl_reg_num, 2);
     end loop;
   end;
  commit;

  exception when others then
    LOG('PROCESS_CLAIM_FOR_SIDE_1 FAILED:'||SQLCODE||' - '||substr(sqlerrm,128));
    rollback;
end;

PROCEDURE PROCESS_PREP_CLAIM_FOR_SIDE_2
as
v_invRequestId number(38) := null;
v_successed_rows number(38) := 0;
pragma autonomous_transaction;
begin
  for line in (select
                distinct
                sd.id,
                sd.sov_id,
                sed.decl_reg_num,
                sd.invoice_contractor_rk,
                sd.invoice_contractor_chapter
                from
                sov_discrepancy sd 
                inner join doc_invoice di on di.invoice_row_key = sd.invoice_rk
                inner join doc d on d.doc_id = di.doc_id and d.close_date is not null
                inner join v$declaration vd on vd.id = sd.decl_contractor_id
                inner join seod_declaration sed on sed.nds2_id = vd.ID and vd.CORRECTION_NUMBER = sed.correction_number
                left join discrep_stage_invoice_request sir on sir.seod_decl_id = sed.decl_reg_num 
                                                               and sir.invoice_rk = sd.invoice_contractor_rk 
                                                               and sir.stage = 3
                                                               and sir.processed = 0
                where sd.status <> 0
                and sd.stage = 2
                and sd.type <> 3
                and sir.request_id is null)
   loop
     if v_invRequestId is null then v_invRequestId := seq_sov_request.nextval; end if;
     
     insert into invoice_request_data(request_id, chapter, invoice_rk)
     values (v_invRequestId, line.invoice_contractor_chapter, line.invoice_contractor_rk);
     
     insert into discrep_stage_invoice_request(request_id, seod_decl_id, invoice_rk, stage, submit_date, processed)
     values (v_invRequestId, line.decl_reg_num, line.invoice_contractor_rk, 3, sysdate, 0);
     v_successed_rows := v_successed_rows + 1;
   end loop;
   
   if v_invRequestId > 0 and v_successed_rows > 0 then 
     insert into invoice_request(id, status, requestdate, load_mismatches, load_only_targets)
     values (v_invRequestId, SEL_STATUS_PREP_SENT, sysdate, 0, 1);
   end if;
   
   commit;
  
   exception when others then rollback;
end;

PROCEDURE PROCESS_CLAIM_FOR_SIDE_2
as
begin
  null;
end;
    
PROCEDURE PROCESS_RECLAIM_FOR_SIDE_1
as
begin
    null;
end;

PROCEDURE PROCESS_RECLAIM_FOR_SIDE_2
as
begin
  null;
end;



END;
/
