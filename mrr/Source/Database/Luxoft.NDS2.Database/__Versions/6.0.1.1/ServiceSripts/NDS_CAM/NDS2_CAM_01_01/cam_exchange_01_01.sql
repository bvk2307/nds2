﻿declare
  v_path varchar2(128);
  doc xmldom.DOMDocument;
  main_node xmldom.DOMNode;
  root_node xmldom.DOMNode;
  user_node xmldom.DOMNode;
  item_node xmldom.DOMNode;
  root_elmt xmldom.DOMElement;
  item_elmt xmldom.DOMElement;
  item_text xmldom.DOMText;
  
  discrepElement xmldom.DOMElement;
  discrepNode xmldom.DOMNode;
/*Книга покупок*/
  ch8elmList xmldom.DOMElement;
  ch8elmItem xmldom.DOMElement;
  ch8nodeList xmldom.DOMNode;
  ch8nodeItem xmldom.DOMNode;
/*Книга продаж*/
  ch9elmList xmldom.DOMElement;
  ch9elmItem xmldom.DOMElement;
  ch9nodeList xmldom.DOMNode;
  ch9nodeItem xmldom.DOMNode;
/*Журнал выставленых СФ раздел 10*/
  ch10elmList xmldom.DOMElement;
  ch10elmItem xmldom.DOMElement;
  ch10nodeList xmldom.DOMNode;
  ch10nodeItem xmldom.DOMNode;
/*Журнал выставленых СФ раздел 11*/
  ch11elmList xmldom.DOMElement;
  ch11elmItem xmldom.DOMElement;
  ch11nodeList xmldom.DOMNode;
  ch11nodeItem xmldom.DOMNode;
/*СФ раздел 12*/
  ch12elmList xmldom.DOMElement;
  ch12elmItem xmldom.DOMElement;
  ch12nodeList xmldom.DOMNode;
  ch12nodeItem xmldom.DOMNode;
/*СФ раздел 1.6*/
  ch16elmList xmldom.DOMElement;
  ch16elmItem xmldom.DOMElement;
  ch16nodeList xmldom.DOMNode;
  ch16nodeItem xmldom.DOMNode;  
  ch16DeclElm xmldom.DOMElement;
  ch16DeclNode xmldom.DOMNode;
  ch16SfElm xmldom.DOMElement;
  ch16SfNode xmldom.DOMNode;  
/*СФ раздел KS*/
  chKSelmList xmldom.DOMElement;
  chKSelmItem xmldom.DOMElement;
  chKSnodeList xmldom.DOMNode;
  chKSnodeItem xmldom.DOMNode;   
/*taxpayer*/
  TaxPayerInfoElement xmldom.DOMElement;
  TaxPayerInfoNode xmldom.DOMNode;
  TaxPayerElement xmldom.DOMElement;
  TaxPayerNode xmldom.DOMNode;  

  ContrElmList xmldom.DOMElement;
  ContrNodeList xmldom.DOMNode;
  ContrElmItem xmldom.DOMElement;
  ContrNodeItem xmldom.DOMNode;

  claim_id  number(12);
  invStrNum number(12);
  xmlNode XmlType;
  buf varchar2(1000);
  buff clob;
  date_format varchar2(12) := 'DD.MM.YYYY';
  decimal_format varchar2(18) := '9999999990.00';
  ks_mode boolean := false;
  v_ch varchar2(2);
  v_svNar varchar2(2 char);
  v_nomAlg varchar2(4);
  v_res number;
begin

  for declLine in (select * from v$declaration)
    loop
      begin
        invStrNum := 0;
        claim_id := SEQ_CLAIM.NEXTVAL;
        doc := xmldom.newDOMDocument();
        main_node := xmldom.makeNode(doc);
        root_elmt := xmldom.createElement(doc, 'Автотребование');
        xmldom.setAttribute(root_elmt, 'УчНомТреб', claim_id);
        xmldom.setAttribute(root_elmt, 'ТипИнф', 'TAX3EXCH_NDS2_CAM_01');
        xmldom.setAttribute(root_elmt, 'РегНомДек', declLine.Declaration_Version_Id);
        xmldom.setAttribute(root_elmt, 'КодНО', '5252');
        xmldom.setAttribute(root_elmt, 'xsi:noNamespaceSchemaLocation', 'TAX3EXCH_NDS2_CAM_01_01.xsd');
        xmldom.setAttribute(root_elmt, 'xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
        root_node := xmldom.appendChild(main_node, xmldom.makeNode(root_elmt));

        discrepElement := xmldom.createElement(doc, 'ПризнРсхжд');
        discrepNode := xmldom.appendChild(root_node, xmldom.makeNode(discrepElement));
   
               
      
if ks_mode then
        chKSelmList := xmldom.createElement(doc, 'T1.7');
        chKSnodeList := xmldom.appendChild(discrepNode, xmldom.makeNode(chKSelmList));
        for ksCntr in 1..5 loop
            select decode(v_svNar, '10', '12', '12', '10', '10') into v_svNar from dual;
            select decode(v_nomAlg, '1.1', '1.2', '1.2', '1.3', '1.3', '1.32', '1.32', '1.1', '1.1') into v_nomAlg from dual;
            chKSelmItem := xmldom.createElement(doc, 'T1.7Стр');
            chKSnodeItem := xmldom.appendChild(chKSnodeList, xmldom.makeNode(chKSelmItem));
            xmldom.setAttribute(chKSelmItem, 'СвНар',        v_svNar);
            xmldom.setAttribute(chKSelmItem, 'КодКС',        v_nomAlg);
            xmldom.setAttribute(chKSelmItem, 'СумЛевЧ',      substr(DBMS_RANDOM.value(100, 999), 1, 6));
            xmldom.setAttribute(chKSelmItem, 'СумПравЧ',     substr(DBMS_RANDOM.value(100, 999), 1, 6));         
        end loop;
  dbms_output.put_line('KS');
else
  
        ch8elmList := xmldom.createElement(doc, 'T1.1');
        ch8nodeList := xmldom.appendChild(discrepNode, xmldom.makeNode(ch8elmList));

        ch9elmList := xmldom.createElement(doc, 'T1.2');
        ch9nodeList := xmldom.appendChild(discrepNode, xmldom.makeNode(ch9elmList));

        ch10elmList := xmldom.createElement(doc, 'T1.3');
        ch10nodeList := xmldom.appendChild(discrepNode, xmldom.makeNode(ch10elmList));

        ch11elmList := xmldom.createElement(doc, 'T1.4');
        ch11nodeList := xmldom.appendChild(discrepNode, xmldom.makeNode(ch11elmList));
        
        ch12elmList := xmldom.createElement(doc, 'T1.5');
        ch12nodeList := xmldom.appendChild(discrepNode, xmldom.makeNode(ch12elmList));

        for invLine in (select 
 DECLARATION_VERSION_ID
, CHAPTER
,to_char(CREATE_DATE,  date_format) as CREATE_DATE
,to_char(RECEIVE_DATE,  date_format) as RECEIVE_DATE
, OPERATION_CODE
, INVOICE_NUM
,to_char(INVOICE_DATE,  date_format) as INVOICE_DATE
, CHANGE_NUM
,to_char(CHANGE_DATE,  date_format) as CHANGE_DATE
, CORRECTION_NUM
,to_char(CORRECTION_DATE,  date_format) as CORRECTION_DATE
, CHANGE_CORRECTION_NUM
,to_char(CHANGE_CORRECTION_DATE,  date_format) as CHANGE_CORRECTION_DATE
, RECEIPT_DOC_NUM
,to_char(RECEIPT_DOC_DATE,  date_format) as RECEIPT_DOC_DATE
,to_char(BUY_ACCEPT_DATE,  date_format) as BUY_ACCEPT_DATE
, BUYER_INN
, BUYER_KPP
, SELLER_INN
, SELLER_KPP
, SELLER_INVOICE_NUM
,to_char(SELLER_INVOICE_DATE,  date_format) as SELLER_INVOICE_DATE
, BROKER_INN
, BROKER_KPP
, DEAL_KIND_CODE
, CUSTOMS_DECLARATION_NUM
, OKV_CODE
,trim(to_char(PRICE_BUY_AMOUNT*1000,  decimal_format)) as PRICE_BUY_AMOUNT
,trim(to_char(PRICE_BUY_NDS_AMOUNT*1000,  decimal_format)) as PRICE_BUY_NDS_AMOUNT
,trim(to_char(PRICE_SELL*1000,  decimal_format)) as PRICE_SELL
,trim(to_char(PRICE_SELL_IN_CURR*1000,  decimal_format)) as PRICE_SELL_IN_CURR
,trim(to_char(PRICE_SELL_18*1000,  decimal_format)) as PRICE_SELL_18
,trim(to_char(PRICE_SELL_10*1000,  decimal_format)) as PRICE_SELL_10
,trim(to_char(PRICE_SELL_0*1000,  decimal_format)) as PRICE_SELL_0
,trim(to_char(PRICE_NDS_18*1000,  decimal_format)) as PRICE_NDS_18
,trim(to_char(PRICE_NDS_10*1000,  decimal_format)) as PRICE_NDS_10
,trim(to_char(PRICE_TAX_FREE*1000,  decimal_format)) as PRICE_TAX_FREE
,trim(to_char(PRICE_TOTAL*1000,  decimal_format)) as PRICE_TOTAL
,trim(to_char(PRICE_NDS_TOTAL*1000,  decimal_format)) as PRICE_NDS_TOTAL
,trim(to_char(DIFF_CORRECT_DECREASE*1000,  decimal_format)) as DIFF_CORRECT_DECREASE
,trim(to_char(DIFF_CORRECT_INCREASE*1000,  decimal_format)) as DIFF_CORRECT_INCREASE
,trim(to_char(DIFF_CORRECT_NDS_DECREASE*1000,  decimal_format)) as DIFF_CORRECT_NDS_DECREASE
,trim(to_char(DIFF_CORRECT_NDS_INCREASE*1000,  decimal_format)) as DIFF_CORRECT_NDS_INCREASE
,trim(to_char(PRICE_NDS_BUYER*1000,  decimal_format)) as PRICE_NDS_BUYER
, ROW_KEY
, ACTUAL_ROW_KEY
, COMPARE_ROW_KEY
, COMPARE_ALGO_ID
, FORMAT_ERRORS
, LOGICAL_ERRORS
    from fir.invoice_raw ir where ir.declaration_version_id = declLine.Declaration_Version_Id)
          loop
/*###########*/
/* РАЗДЕЛ 8  */
/*###########*/
            if invLine.Chapter = 8 then
              invStrNum := invStrNum + 1;/*Stub*/
              ch8elmItem := xmldom.createElement(doc, 'T1.1Стр');
              ch8nodeItem := xmldom.appendChild(ch8nodeList, xmldom.makeNode(ch8elmItem));
              /*обязательные*/
              xmldom.setAttribute(ch8elmItem, 'НомерПор',        invStrNum);
              xmldom.setAttribute(ch8elmItem, 'КодВидОпер',      invLine.Operation_Code);
              xmldom.setAttribute(ch8elmItem, 'НомСчФПрод',      invLine.Invoice_Num);
              xmldom.setAttribute(ch8elmItem, 'СтоимПокупВ',     invLine.Price_Buy_Amount);
              xmldom.setAttribute(ch8elmItem, 'СумНДС',          invLine.Price_Buy_Nds_Amount);
              /*Не обязательные*/
              xmldom.setAttribute(ch8elmItem, 'ДатаСчФПрод',     invLine.Invoice_Date);
              xmldom.setAttribute(ch8elmItem, 'НомИспрСчФ',      invLine.Change_Num);
              xmldom.setAttribute(ch8elmItem, 'ДатаИспрСчФ',     invLine.Change_Date);
              xmldom.setAttribute(ch8elmItem, 'НомКСчФПрод',     invLine.Correction_Num);
              xmldom.setAttribute(ch8elmItem, 'ДатаКСчФПрод',     invLine.Correction_Date );
              xmldom.setAttribute(ch8elmItem, 'НомИспрКСчФ',     invLine.Change_Correction_Num);
              xmldom.setAttribute(ch8elmItem, 'ДатаИспрКСчФ',     invLine.Change_Correction_Date);
              xmldom.setAttribute(ch8elmItem, 'НомДокПдтвОпл',     invLine.Receipt_Doc_Num);
              xmldom.setAttribute(ch8elmItem, 'ДатаДокПдтвОпл',   invLine.Receipt_Doc_Date);
              xmldom.setAttribute(ch8elmItem, 'ДатаУчТов',       invLine.Buy_Accept_Date);
              xmldom.setAttribute(ch8elmItem, 'НомТД',           invLine.Customs_Declaration_Num);
--              xmldom.setAttribute(ch8elmItem, 'ОКВ', invLine.); 
              TaxPayerInfoElement := xmldom.createElement(doc, 'СвПрод');
              TaxPayerInfoNode := xmldom.appendChild(ch8nodeItem, xmldom.makeNode(TaxPayerInfoElement));
              TaxPayerElement := xmldom.createElement(doc, 'СведЮЛ');
              xmldom.setAttribute(TaxPayerElement, 'ИННЮЛ', '5252'||substr(DBMS_RANDOM.value(1, 2), 3, 6));
              xmldom.setAttribute(TaxPayerElement, 'КПП', '123456789');
              TaxPayerNode := xmldom.appendChild(TaxPayerInfoNode, xmldom.makeNode(TaxPayerElement));
            end if;
/*###########*/
/* РАЗДЕЛ 9  */
/*###########*/
            if invLine.Chapter = 9 then
              invStrNum := invStrNum + 1;/*Stub*/
              ch9elmItem := xmldom.createElement(doc, 'T1.2Стр');
              ch9nodeItem := xmldom.appendChild(ch9nodeList, xmldom.makeNode(ch9elmItem));
              /*обязательные*/
              xmldom.setAttribute(ch9elmItem, 'НомерПор',       invStrNum);
              xmldom.setAttribute(ch9elmItem, 'КодВидОпер',     invLine.Operation_Code);
              xmldom.setAttribute(ch9elmItem, 'НомСчФПрод',     invLine.Invoice_Num);
              xmldom.setAttribute(ch9elmItem, 'ДатаСчФПрод',     invLine.Invoice_Date);
              /*Не обязательные*/
              xmldom.setAttribute(ch9elmItem, 'НомИспрСчФ',     invLine.Change_Num);
              xmldom.setAttribute(ch9elmItem, 'ДатаИспрСчФ',     invLine.Change_Date);
              xmldom.setAttribute(ch9elmItem, 'НомКСчФПрод',     invLine.Correction_Num);
              xmldom.setAttribute(ch9elmItem, 'ДатаКСчФПрод',     invLine.Correction_Date);
              xmldom.setAttribute(ch9elmItem, 'НомИспрКСчФ',     invLine.Change_Correction_Num);
              xmldom.setAttribute(ch9elmItem, 'ДатаИспрКСчФ',     invLine.Change_Correction_Date);
              xmldom.setAttribute(ch9elmItem, 'НомДокПдтвОпл',     invLine.Receipt_Doc_Num);
              xmldom.setAttribute(ch9elmItem, 'ДатаДокПдтвОпл',   invLine.Receipt_Doc_Date);
              --xmldom.setAttribute(ch9elmItem, 'ОКВ', invLine.);
              xmldom.setAttribute(ch9elmItem, 'СтоимПродСФВ',     invLine.Price_Sell_In_Curr);
              xmldom.setAttribute(ch9elmItem, 'СтоимПродСФ',     invLine.Price_Sell);              
              xmldom.setAttribute(ch9elmItem, 'СтоимПродСФ18',     invLine.Price_Sell_18);
              xmldom.setAttribute(ch9elmItem, 'СтоимПродСФ10',     invLine.Price_Sell_10);
              xmldom.setAttribute(ch9elmItem, 'СтоимПродСФ0',     invLine.Price_Sell_0);
              xmldom.setAttribute(ch9elmItem, 'СумНДССФ18',     invLine.Price_Nds_18);
              xmldom.setAttribute(ch9elmItem, 'СумНДССФ10',     invLine.Price_Nds_10);
              xmldom.setAttribute(ch9elmItem, 'СтоимПродОсв',     invLine.Price_Tax_Free);
              
              TaxPayerInfoElement := xmldom.createElement(doc, 'СвПокуп');
              TaxPayerInfoNode := xmldom.appendChild(ch9nodeItem, xmldom.makeNode(TaxPayerInfoElement));
              TaxPayerElement := xmldom.createElement(doc, 'СведИП');
              xmldom.setAttribute(TaxPayerElement, 'ИННФЛ', '5252'||substr(DBMS_RANDOM.value(1, 2), 3, 8));
              TaxPayerNode := xmldom.appendChild(TaxPayerInfoNode, xmldom.makeNode(TaxPayerElement));
            end if;
/*###########*/
/* РАЗДЕЛ 10 */
/*###########*/
            if invLine.Chapter = 10 then
              invStrNum := invStrNum + 1;/*Stub*/
              ch10elmItem := xmldom.createElement(doc, 'T1.3Стр');
              ch10nodeItem := xmldom.appendChild(ch10nodeList, xmldom.makeNode(ch10elmItem));
              /*обязательные*/
              xmldom.setAttribute(ch10elmItem, 'НомерПор',       invStrNum);
              xmldom.setAttribute(ch10elmItem, 'ДатаВыст',       invLine.Create_Date);
              xmldom.setAttribute(ch10elmItem, 'КодВидОпер',     invLine.Operation_Code);
              xmldom.setAttribute(ch10elmItem, 'НомСчФПрод',     invLine.Invoice_Num);
              xmldom.setAttribute(ch10elmItem, 'ДатаСчФПрод',     invLine.Invoice_Date);
              /*Не обязательные*/
              xmldom.setAttribute(ch10elmItem, 'НомИспрСчФ',     invLine.Change_Num);
              xmldom.setAttribute(ch10elmItem, 'ДатаИспрСчФ',     invLine.Change_Date);
              xmldom.setAttribute(ch10elmItem, 'НомКСчФПрод',     invLine.Correction_Num);
              xmldom.setAttribute(ch10elmItem, 'ДатаКСчФПрод',     invLine.Correction_Date);
              xmldom.setAttribute(ch10elmItem, 'НомИспрКСчФ',     invLine.Change_Correction_Num);
              xmldom.setAttribute(ch10elmItem, 'ДатаИспрКСчФ',     invLine.Change_Correction_Date);
--              xmldom.setAttribute(ch10elmItem, 'ОКВ', invLine.); 
              
if mod(substr(dbms_random.value(1,2), 3,2), 2) = 1 then
              TaxPayerInfoElement := xmldom.createElement(doc, 'СвПокуп');
              TaxPayerInfoNode := xmldom.appendChild(ch10nodeItem, xmldom.makeNode(TaxPayerInfoElement));
              TaxPayerElement := xmldom.createElement(doc, 'СведЮЛ');
              xmldom.setAttribute(TaxPayerElement, 'ИННЮЛ', '5252'||substr(DBMS_RANDOM.value(1, 2), 3, 6));
              xmldom.setAttribute(TaxPayerElement, 'КПП', '123456789');
              TaxPayerNode := xmldom.appendChild(TaxPayerInfoNode, xmldom.makeNode(TaxPayerElement));  
else
              ContrElmList := xmldom.createElement(doc, 'СвПосрДеят');
              ContrNodeList := xmldom.appendChild(ch10nodeItem, xmldom.makeNode(ContrElmList));
              for ci in 1..substr(dbms_random.value(1,2), 3,1) loop
                  ContrElmItem := xmldom.createElement(doc, 'СвПосрДеятСтр');
                  ContrNodeItem := xmldom.appendChild(ContrNodeList, xmldom.makeNode(ContrElmItem));
                  xmldom.setAttribute(ContrElmItem, 'НомСчФОтПрод', substr(DBMS_RANDOM.value(1, 2), 3, 8));
                  xmldom.setAttribute(ContrElmItem, 'ДатаСчФОтПрод', to_char(sysdate - substr(DBMS_RANDOM.value(1, 2), 3, 4), date_format));
                  xmldom.setAttribute(ContrElmItem, 'ОКВ', '810');
                  xmldom.setAttribute(ContrElmItem, 'СтоимТовСчФВс', substr(DBMS_RANDOM.value(1, 2), 3, 4)||'.00');
                  xmldom.setAttribute(ContrElmItem, 'СумНДССчФ', substr(DBMS_RANDOM.value(1, 2), 3, 4)||'.00');
                  xmldom.setAttribute(ContrElmItem, 'РазСтКСчФУм', substr(DBMS_RANDOM.value(1, 2), 3, 2)||'.00');
                  xmldom.setAttribute(ContrElmItem, 'РазСтКСчФУв', substr(DBMS_RANDOM.value(1, 2), 3, 2)||'.00');
                  xmldom.setAttribute(ContrElmItem, 'РазНДСКСчФУм', substr(DBMS_RANDOM.value(1, 2), 3, 2)||'.00');
                  xmldom.setAttribute(ContrElmItem, 'РазНДСКСчФУв', substr(DBMS_RANDOM.value(1, 2), 3, 2)||'.00');
                  
                  
                  TaxPayerInfoElement := xmldom.createElement(doc, 'СвПрод');
                  TaxPayerInfoNode := xmldom.appendChild(ContrNodeItem, xmldom.makeNode(TaxPayerInfoElement));
                  
                  TaxPayerElement := xmldom.createElement(doc, 'СведЮЛ');
                  xmldom.setAttribute(TaxPayerElement, 'ИННЮЛ', '5252'||substr(DBMS_RANDOM.value(1, 2), 3, 6));
                  xmldom.setAttribute(TaxPayerElement, 'КПП', '123456789');
                  TaxPayerNode := xmldom.appendChild(TaxPayerInfoNode, xmldom.makeNode(TaxPayerElement));  
              
              end loop;
  /*            
              TaxPayerInfoElement := xmldom.createElement(doc, 'СвПосрДеят');
              TaxPayerInfoNode := xmldom.appendChild(ch10nodeItem, xmldom.makeNode(TaxPayerInfoElement));
              TaxPayerElement := xmldom.createElement(doc, 'СведЮЛ');
              xmldom.setAttribute(TaxPayerElement, 'ИННЮЛ', '5252'||substr(DBMS_RANDOM.value(1, 2), 3, 6));
              xmldom.setAttribute(TaxPayerElement, 'КПП', '123456789');
              TaxPayerNode := xmldom.appendChild(TaxPayerInfoNode, xmldom.makeNode(TaxPayerElement));*/
end if;


            end if;
/*###########*/
/* РАЗДЕЛ 11 */
/*###########*/
            if invLine.Chapter = 11 then
              invStrNum := invStrNum + 1;/*Stub*/
              ch11elmItem := xmldom.createElement(doc, 'T1.4Стр');
              ch11nodeItem := xmldom.appendChild(ch11nodeList, xmldom.makeNode(ch11elmItem));
              /*обязательные*/
              xmldom.setAttribute(ch11elmItem, 'НомерПор',       invStrNum);
              xmldom.setAttribute(ch11elmItem, 'ДатаПолуч',     invLine.Receive_Date);
              xmldom.setAttribute(ch11elmItem, 'КодВидОпер',     invLine.Operation_Code);
              xmldom.setAttribute(ch11elmItem, 'НомСчФПрод',     invLine.Invoice_Num);
              xmldom.setAttribute(ch11elmItem, 'ДатаСчФПрод',     invLine.Invoice_Date);
              xmldom.setAttribute(ch11elmItem, 'СтоимТовСчФВс',   invLine.Price_Total);
              xmldom.setAttribute(ch11elmItem, 'КодВидСд',       invLine.Deal_Kind_Code);
              /*Не обязательные*/
              xmldom.setAttribute(ch11elmItem, 'НомИспрСчФ',     invLine.Change_Num);
              xmldom.setAttribute(ch11elmItem, 'ДатаИспрСчФ',     invLine.Change_Date);
              xmldom.setAttribute(ch11elmItem, 'НомКСчФПрод',     invLine.Correction_Num);
              xmldom.setAttribute(ch11elmItem, 'ДатаКСчФПрод',     invLine.Correction_Date);
              xmldom.setAttribute(ch11elmItem, 'НомИспрКСчФ',     invLine.Change_Correction_Num);
--              xmldom.setAttribute(ch10elmItem, 'ОКВ', invLine.); 
              xmldom.setAttribute(ch11elmItem, 'СумНДССчФ',     invLine.Price_Nds_Total);
              xmldom.setAttribute(ch11elmItem, 'РазСтКСчФУм',     invLine.Diff_Correct_Decrease);
              xmldom.setAttribute(ch11elmItem, 'РазСтКСчФУв',     invLine.Diff_Correct_Increase);
              xmldom.setAttribute(ch11elmItem, 'РазНДСКСчФУм',     invLine.Diff_Correct_Nds_Decrease);
              xmldom.setAttribute(ch11elmItem, 'РазНДСКСчФУв',     invLine.Diff_Correct_Nds_Increase);

              TaxPayerInfoElement := xmldom.createElement(doc, 'СвПрод');
              TaxPayerInfoNode := xmldom.appendChild(ch11nodeItem, xmldom.makeNode(TaxPayerInfoElement));
              TaxPayerElement := xmldom.createElement(doc, 'СведЮЛ');
              xmldom.setAttribute(TaxPayerElement, 'ИННЮЛ', '5252'||substr(DBMS_RANDOM.value(1, 2), 3, 6));
              xmldom.setAttribute(TaxPayerElement, 'КПП', '123456789');
              TaxPayerNode := xmldom.appendChild(TaxPayerInfoNode, xmldom.makeNode(TaxPayerElement));
            end if;
/*###########*/
/* РАЗДЕЛ 12 */
/*###########*/
            if invLine.Chapter = 12 then
              invStrNum := invStrNum + 1;/*Stub*/
              ch12elmItem := xmldom.createElement(doc, 'T1.5Стр');
              ch12nodeItem := xmldom.appendChild(ch12nodeList, xmldom.makeNode(ch12elmItem));
              /*обязательные*/
              xmldom.setAttribute(ch12elmItem, 'НомСчФ',       invLine.Invoice_Num);
              xmldom.setAttribute(ch12elmItem, 'ДатаСчФ',       invLine.Invoice_Date);
              xmldom.setAttribute(ch12elmItem, 'СтоимТовБНалВс',   invLine.Price_Total);
              xmldom.setAttribute(ch12elmItem, 'СумНалПокуп',     invLine.Price_Nds_Buyer);
              xmldom.setAttribute(ch12elmItem, 'СтоимТовСНалВс',   invLine.Price_Nds_Total);
              /*Не обязательные*/
--              xmldom.setAttribute(ch12elmItem, 'ОКВ', invLine.); 
              TaxPayerInfoElement := xmldom.createElement(doc, 'СвПокуп');
              TaxPayerInfoNode := xmldom.appendChild(ch12nodeItem, xmldom.makeNode(TaxPayerInfoElement));
              TaxPayerElement := xmldom.createElement(doc, 'СведЮЛ');
              xmldom.setAttribute(TaxPayerElement, 'ИННЮЛ', '5252'||substr(DBMS_RANDOM.value(1, 2), 3, 6));
              xmldom.setAttribute(TaxPayerElement, 'КПП', '123456789');
              TaxPayerNode := xmldom.appendChild(TaxPayerInfoNode, xmldom.makeNode(TaxPayerElement));
            end if; 
          end loop;
          
/*###########*/
/* РАЗДЕЛ 16 */
/*###########*/

for chptr in 1..3 loop
    ch16elmList := xmldom.createElement(doc, 'T1.6');
    ch16nodeList := xmldom.appendChild(discrepNode, xmldom.makeNode(ch16elmList));
    select  decode(chptr, 1, 9, 2, 10, 3, 12, 9) into v_ch from dual;
                   
    xmldom.setAttribute(ch16elmList, 'РазделДекл',  v_ch);
--              ch16DeclElm := xmldom.createElement(doc, 'РазделДекл');
--              ch16DeclNode := xmldom.appendChild(ch16nodeList, xmldom.makeNode(ch16DeclElm));
                  
    for cntr in 1../*substr(DBMS_RANDOM.value(1, 2), 3, 2)*/10 loop
        ch16elmItem := xmldom.createElement(doc, 'T1.6Стр');
        ch16nodeItem := xmldom.appendChild(ch16nodeList, xmldom.makeNode(ch16elmItem));
        --NP
        TaxPayerInfoElement := xmldom.createElement(doc, 'СвКАгент');
        xmldom.setAttribute(TaxPayerInfoElement, 'НаимКАгент',   DBMS_RANDOM.string('a', 15));        
        TaxPayerInfoNode := xmldom.appendChild(ch16nodeItem, xmldom.makeNode(TaxPayerInfoElement));
        TaxPayerElement := xmldom.createElement(doc, 'СведЮЛ');
        xmldom.setAttribute(TaxPayerElement, 'ИННЮЛ', '5252'||substr(DBMS_RANDOM.value(1, 2), 3, 6));
        xmldom.setAttribute(TaxPayerElement, 'КПП', '123456789');
        TaxPayerNode := xmldom.appendChild(TaxPayerInfoNode, xmldom.makeNode(TaxPayerElement));
        for npSf in 1..3 loop
          ch16SfElm := xmldom.createElement(doc, 'СведСФ');
          ch16SfNode := xmldom.appendChild(ch16nodeItem, xmldom.makeNode(ch16SfElm));  
          xmldom.setAttribute(ch16SfElm, 'НомСчФ',       substr(DBMS_RANDOM.value(1, 2), 3, 8));
          xmldom.setAttribute(ch16SfElm, 'ДатаСчФ',   to_char((sysdate - substr(DBMS_RANDOM.value(1, 2), 3, 2)), date_format));        
        end loop;              
    end loop;            
end loop;
        end if;                  
--        DBMS_LOB.CreateTemporary(buf, TRUE);
--        DBMS_XMLDOM.WRITETOCLOB(root_elmt, buf);
--        xmlNode := SYS.XMLTYPE(nodeBuffer);
--        insert into xmlOut values(claim_id, xmlNode);
--        DBMS_LOB.FreeTemporary(buf);
--        xmldom.writeToBuffer(node, buf);
--        xmldom.writeToBuffer(doc, buff);
--doc.validate();
          xmlNode := dbms_xmldom.getXmlType(doc);
          --v_res := xmlNode.isSchemaValid('TAX3EXCH_NDS2_CAM_01_01.xsd');
--          dbms_output.put_line(declLine.Declaration_Version_Id||'  '||XmlType.createXML(xmlNode.getClobVal()).isSchemaValid('TAX3EXCH_NDS2_CAM_01_01.xsd'));
          --
          --dbms_output.put_line(substr(xmlNode.getClobVal(), 0, 500));
--          v_path := declLine.Declaration_Version_Id||'.xml';
          --xmlNode.isSchemaValid('http://www.oracle.com/PO.xsd');
-- select * from doc order by 1 desc
--          DBMS_XSLProcessor.Clob2File(xmlNode.getClobVal(),'EXP_DIR',v_path );
          insert into doc(doc_id, ref_entity_id, doc_type, sono_code, create_date, status, document_body)
          values (claim_id, declLine.Declaration_Version_Id, 1, '5252', sysdate, 51, xmlNode.getClobVal());
--          insert into claim(claim_id, sono_code, declaration_id,process_id,create_date,status,discrepancy_data)
--          values(,'5252', declLine.Declaration_Version_Id,SEQ_CLAIM.NEXTVAL,sysdate, 51, xmlNode.getClobVal());
          dbms_output.put_line(v_path);
--          xmldom.writeToFile(doc, v_path);
          xmldom.freeDocument(doc);
--          DBMS_LOB.FreeTemporary(buff);
        end;
    end loop;
    commit;
end;

