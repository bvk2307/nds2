﻿/*Validate xml*/
declare 
xmlT XmlType;
decId number;
begin
for line in (select t.*, decode(t.doc_type, 1, 'TAX3EXCH_NDS2_CAM_01_01.xsd',
                                                  2, 'TAX3EXCH_NDS2_CAM_02_01.xsd',
                                                  3, 'TAX3EXCH_NDS2_CAM_03_01.xsd', '') as xsd
  from doc t)
  loop
    begin
        xmlT := XmlType(line.document_body);
        dbms_output.put(line.xsd);
        dbms_output.put_line('-'||line.ref_entity_id||'-'||xmlT.isSchemaValid(line.xsd));
    exception when others then dbms_output.put_line('error '||line.ref_entity_id);
    end;
  end loop;
end;
/*ACCEPT DATA*/
declare 
xmlT XmlType;
decId number;
begin
for line in (select t.* from i$nds2.v$cam_outgoing t)
  loop
    begin
        I$NDS2.PAC$CAM_SYNC.ACCEPT(P_OBJ_ID => line.object_reg_num, P_OBJ_TYPE_ID => line.object_type_id);
    exception when others then dbms_output.put_line('error '||line.declartion_reg_num);
    end;
  end loop;
end;
