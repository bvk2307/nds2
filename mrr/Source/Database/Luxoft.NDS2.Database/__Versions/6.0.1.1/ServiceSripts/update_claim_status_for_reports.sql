﻿update claim set status = 54 
where MOD(claim_id, 2) = 1;


update claim set SEND_DATE = TO_DATE('18-SEP-14', 'DD-MON-RR'),
       CLOSE_DATE = null
where status = 52;

update claim set SEND_DATE = TO_DATE('18-SEP-14', 'DD-MON-RR'),
       CLOSE_DATE = TO_DATE('20-NOV-14', 'DD-MON-RR')
where status = 54;
commit;


update i$stub_cam.stg_claim set status = 53, ready_to_sync = 1;
commit;


-- перевод из физика в юрики для ООО КБЭР "Банк КАЗАНИ"
update NDS2_MC."ASKДекл" set КППНП = null, ИНННП = 165301866100
where ИНННП = 1653018661;

delete from NDS2_MRR_USER.V_EGRN_IP;
insert into NDS2_MRR_USER.V_EGRN_IP (ID, INNFL, OGRNIP, LAST_NAME, FIRST_NAME, PATRONYMIC, DATE_BIRTH, DOC_CODE, DOC_NUM, DOC_DATE, DOC_ORG, DOC_ORG_CODE, ADR, CODE_NO, DATE_ON, REASON_ON, DATE_OFF, REASON_OFF)
values (0, '165301866100', null, 'Иванов', 'Моисей', 'Камазов', to_date('01-12-2014', 'dd-mm-yyyy'), '11', '12', to_date('02-12-2014', 'dd-mm-yyyy'), '13', null, 'г.Омск ул.Строителей д.5', '4013', to_date('04-12-2014', 'dd-mm-yyyy'), null, to_date('07-12-2014', 'dd-mm-yyyy'), null);

delete from NDS2_MRR_USER.BSSCHET_IP;
insert into NDS2_MRR_USER.BSSCHET_IP (INN, BIK, NAMEKO, NOMSCH, PRVAL, DATEOPENSCH)
values ('165301866100', '123456789', 'МММ 2014', '890881', '1', to_date('01-12-2014', 'dd-mm-yyyy'));
insert into NDS2_MRR_USER.BSSCHET_IP (INN, BIK, NAMEKO, NOMSCH, PRVAL, DATEOPENSCH)
values ('165301866100', '987654321', 'МММ 2014', '890882', '0', to_date('01-12-2014', 'dd-mm-yyyy'));
insert into NDS2_MRR_USER.BSSCHET_IP (INN, BIK, NAMEKO, NOMSCH, PRVAL, DATEOPENSCH)
values ('165301866100', '111111111', 'МММ 2014', '890883', '1', to_date('01-12-2014', 'dd-mm-yyyy'));

commit;