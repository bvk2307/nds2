﻿declare
v_name varchar2(32);
begin
  FOR i IN ( select object_name from all_objects where object_type = 'SEQUENCE' and owner = 'NDS2_MRR_USER' )
  LOOP
    execute immediate 'drop sequence NDS2_MRR_USER.'||i.object_name;
  END LOOP;
end;
/

create sequence NDS2_MRR_USER.SEQ_DOC
minvalue 0
maxvalue 9999999999
start with 1
increment by 1
nocache;

create sequence NDS2_MRR_USER.SEQ_MISM_QUERY
minvalue 0
maxvalue 9999999999
start with 1
increment by 1
nocache;


create sequence NDS2_MRR_USER.SEQ_RES_CNTR_CHK
minvalue 0
maxvalue 9999999999
start with 1
increment by 1
nocache;

create sequence NDS2_MRR_USER.SEQ_RES_FLK
minvalue 0
maxvalue 9999999999
start with 1
increment by 1
nocache;


create sequence NDS2_MRR_USER.SEQ_MIS_INV
minvalue 0
maxvalue 9999999999
start with 1
increment by 1
nocache;


create sequence NDS2_MRR_USER.DISCREPANCIES_SEQ
minvalue 0
maxvalue 9999999999
start with 1
increment by 1
nocache;


create sequence NDS2_MRR_USER.SELECTIONS_SEQ
minvalue 0
maxvalue 9999999999
start with 1
increment by 1
nocache;

create sequence NDS2_MRR_USER.SEQ_DISCREPANCIES_FILTER
minvalue 1
maxvalue 9999999999
start with 1
increment by 1
nocache;

create sequence NDS2_MRR_USER.SEQ_DICT_FILTER_TYPE
minvalue 1
maxvalue 9999999999
start with 1
increment by 1
nocache;

create sequence NDS2_MRR_USER.SEQ_ACTION_HISTORY
minvalue 1
maxvalue 9999999999
start with 1
increment by 1
nocache;

create sequence NDS2_MRR_USER.SEQ_FAV_FILTER
minvalue 1
maxvalue 9999999999
start with 1
increment by 1
nocache;

create sequence NDS2_MRR_USER.SEQ_SYS_LOG_ID
minvalue 1
maxvalue 9999999999
start with 1
increment by 1
nocache;

create sequence NDS2_MRR_USER.SEQ_DISCREPANCY_ID
minvalue 1
maxvalue 9999999999
start with 1
increment by 1
nocache;

create sequence NDS2_MRR_USER.SEQ_SOV_REQUEST
minvalue 1
maxvalue 9999999999
start with 1
increment by 1
nocache; 

create sequence NDS2_MRR_USER.SEQ_REPORT_LOAD_INSP
minvalue 1
maxvalue 9999999999
start with 1
increment by 1
nocache;

create sequence NDS2_MRR_USER.SEQ_REPORT_TASK
minvalue 1
maxvalue 9999999999
start with 1
increment by 1
nocache;

create sequence NDS2_MRR_USER.SEQ_REPORT_TASK_PARAM
minvalue 1
maxvalue 9999999999
start with 1
increment by 1
nocache;

create sequence NDS2_MRR_USER.SEQ_REPORT_MONITOR_DECL
minvalue 1
maxvalue 9999999999
start with 1
increment by 1
nocache;

create sequence NDS2_MRR_USER.SEQ_REPORT_INSPEC_MONITOR_WORK
minvalue 1
maxvalue 9999999999
start with 1
increment by 1
nocache;

create sequence NDS2_MRR_USER.SEQ_SEOD_KNP
minvalue 1
maxvalue 9999999999
start with 1
increment by 1
nocache;

create sequence NDS2_MRR_USER.SEQ_METODOLOG_REGION
minvalue 1000
maxvalue 9999999999
start with 1000
increment by 1
nocache;

create sequence NDS2_MRR_USER.SEQ_SOV_PYRAMID_INFO
minvalue 1
nomaxvalue
start with 1
increment by 1
nocache;

create sequence NDS2_MRR_USER.SEQ_REPORT_CHECK_CR
minvalue 1
maxvalue 9999999999
start with 1
increment by 1
nocache;

create sequence NDS2_MRR_USER.SEQ_REPORT_MATCHING_RULE
minvalue 1
maxvalue 9999999999
start with 1
increment by 1
nocache;

create sequence NDS2_MRR_USER.SEQ_REPORT_LOGIC_CONTROL
minvalue 1
maxvalue 9999999999
start with 1
increment by 1
nocache;

create sequence NDS2_MRR_USER.SEQ_EXPLAIN_REPLY
minvalue 1
maxvalue 9999999999
start with 1
increment by 1
nocache;

create sequence NDS2_MRR_USER.SEQ_STAGE_INVOICE_CORRECT
minvalue 1
maxvalue 9999999999
start with 1
increment by 1
nocache;

create sequence NDS2_MRR_USER.SEQ_HIST_EXPLAIN_REPLY
minvalue 1
maxvalue 9999999999
start with 1
increment by 1
nocache;

create sequence NDS2_MRR_USER.SEQ_STAGE_INVOICE_CORRECT_ST
minvalue 1
maxvalue 9999999999
start with 1
increment by 1
nocache;

create sequence NDS2_MRR_USER.SEQ_TECH_REPORT_ITERATION
minvalue 0
maxvalue 9999999999
start with 1
increment by 1
nocache;

create sequence NDS2_MRR_USER.SEQ_REPORT_DECLARATION
minvalue 1
maxvalue 9999999999
start with 1
increment by 1
nocache;

create sequence NDS2_MRR_USER.SEQ_REPORT_DECLARATION_RAW
minvalue 1
maxvalue 9999999999
start with 1
increment by 1
nocache;

create sequence NDS2_MRR_USER.seq_techno_report_iteration
minvalue 1
maxvalue 9999999999
start with 1
increment by 1
nocache;