﻿create table NDS2_MRR_USER.SIDE_SELECTION_SETTINGS
(
  side_selection_rule_id number(38) not null,
  is_seller number(1),
  CONSTRAINT side_selection_rule_pk PRIMARY KEY (side_selection_rule_id)
);

insert into NDS2_MRR_USER.SIDE_SELECTION_SETTINGS (side_selection_rule_id, is_seller) values (1,0);
insert into NDS2_MRR_USER.SIDE_SELECTION_SETTINGS (side_selection_rule_id, is_seller) values (2,0);
insert into NDS2_MRR_USER.SIDE_SELECTION_SETTINGS (side_selection_rule_id, is_seller) values (3,1);
insert into NDS2_MRR_USER.SIDE_SELECTION_SETTINGS (side_selection_rule_id, is_seller) values (4,0);
insert into NDS2_MRR_USER.SIDE_SELECTION_SETTINGS (side_selection_rule_id, is_seller) values (5,1);
insert into NDS2_MRR_USER.SIDE_SELECTION_SETTINGS (side_selection_rule_id, is_seller) values (6,1);
