﻿CREATE TABLE NDS2_MRR_USER.SOV_DECLARATION_INFO
(
	ID NUMBER,
	ASK_DECL_ID					NUMBER, -- ID ASKDecl
	DECLARATION_VERSION_ID		NUMBER, -- ID ASKDecl
	LK_ERRORS_COUNT				NUMBER, --КОЛ-ВО ОШИБОК ФЛК
	CH8_DEALS_AMNT_TOTAL		NUMBER(19,2),--СУММА СДЕЛОК ПО РАЗДЕЛУ 8(КАКОЕ ПОЛЕ?)
	CH9_DEALS_AMNT_TOTAL		NUMBER(19,2),--СУММА СДЕЛОК ПО РАЗДЕЛУ 9(КАКОЕ ПОЛЕ?)
	CH8_NDS						NUMBER(19,2),--НДС ПО РАЗДЕЛУ 8(КАКОЕ ПОЛЕ?)
	CH9_NDS						NUMBER(19,2),--НДС ПО РАЗДЕЛУ 9(КАКОЕ ПОЛЕ?)
	DISCREP_CURRENCY_AMNT		NUMBER(19,2), --СУММА РАСХОЖДЕНИЙ ПО ВАЛЮТЕ
	DISCREP_CURRENCY_COUNT		NUMBER, --КОЛ-ВО РАСХОЖДЕНИЙ ПО ВАЛЮТЕ
	GAP_DISCREP_COUNT			NUMBER,--КОЛ-ВО РАСХОЖДЕНИЙ ПО ТИПУ РАЗРЫВ
	GAP_DISCREP_AMNT			NUMBER(19,2),--СУММА РАСХОЖДЕНИЙ ПО ТИПУ РАЗРЫВ
	WEAK_DISCREP_COUNT			NUMBER,-- ЧИСЛО РАСХ. ПО НЕТОЧНОМУ СОПОСТАВЛЕНИЮ
	WEAK_DISCREP_AMNT			NUMBER(19,2), -- СУММА ПО НЕТОЧНОМУ СОПОСТАЛВЕНИЮ
	NDS_INCREASE_DISCREP_COUNT	NUMBER,--КОЛ-ВО РАСХ ВИДА ЗАВЫШЕННОГО НДС
	NDS_INCREASE_DISCREP_AMNT	NUMBER(19,2), --СУММА РАСХОЖДЕНИЙ ВИДА ЗАВЫШЕННОГО НДС
	GAP_MULTIPLE_DISCREP_COUNT	NUMBER, -- Количество расхождений вида разрыв по множественному сопоставлению
	GAP_MULTIPLE_DISCREP_AMNT	NUMBER(19,2), -- Сумма расхождений вида разрыв по множественному сопоставлению
	DISCREP_BUY_BOOK_AMNT		NUMBER(19,2), -- Сумма расхождений по книге покупок
	DISCREP_SELL_BOOK_AMNT		NUMBER(19,2), -- Сумма расхождений по книге продаж
	DISCREP_MIN_AMNT			NUMBER(19,2), 
	DISCREP_MAX_AMNT			NUMBER(19,2),
	DISCREP_AVG_AMNT			NUMBER(19,2),
	DISCREP_TOTAL_AMNT			NUMBER(19,2),
	PVP_TOTAL_AMNT				NUMBER(19,2),	--Общая сумма ПВП в НД
	PVP_DISCREP_MIN_AMNT		NUMBER(19,2),	--Минимальная сумма ПВП расхождения
	PVP_DISCREP_MAX_AMNT		NUMBER(19,2),	--Максимальная сумма ПВП расхождения
	PVP_DISCREP_AVG_AMNT		NUMBER(19,2),	--Средняя сумма ПВП расхождения
	PVP_BUY_BOOK_AMNT			NUMBER(19,2),	--Общая сумма ПВП по книге покупок
	PVP_SELL_BOOK_AMNT			NUMBER(19,2),	--Общая сумма ПВП по книге продаж
	PVP_RECIEVE_JOURNAL_AMNT	NUMBER(19,2),	--Общая сумма ПВП по журналу полученных СФ
	PVP_SENT_JOURNAL_AMNT		NUMBER(19,2),	--Общая сумма ПВП по журналу выставленных СФ
	SUR_CODE					NUMBER(1),		--СУР НП
	CONTROL_RATIO_COUNT			NUMBER,			-- Количество расхождений по КС
	CONTROL_RATIO_DATE			DATE,			--Дата и время проверки КС (опционально, заполняется после завершения расчета КС для корректировки декларации)
	CONTROL_RATIO_SEND_TO_SEOD	NUMBER(1),		--КС переданы в СЭОД (флаг, по умолчанию не установлен)
	UPDATE_DATE					DATE,			--ДАТА ИЗМЕНЕНИЯ СВОДНОЙ ИНФОРМАЦИИ
	CORRECTION_NUMBER			varchar2(3)		--Номер версии
);