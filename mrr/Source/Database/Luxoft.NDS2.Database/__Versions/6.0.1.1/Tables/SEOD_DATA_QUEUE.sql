﻿create table NDS2_MRR_USER.seod_data_queue
(
  declaration_reg_num number(20) not null,
  discrepancy_id number not null,
  for_stage number(2) not null,
  ref_doc_id number null
);