﻿--Клиентский запрос отчета по ЛК
create table NDS2_MRR_USER.SOV_LK_REPORT_REQUEST
(
   REQUEST_ID           NUMBER               not null,
   STATUS_ID            NUMBER(2)            not null,
   REQUEST_BODY         CLOB,
   REQUEST_DATE                                               DATE,
   PROCESS_DATE                                                DATE,
   constraint PK_SOV_LK_REPORT_REQUEST primary key (REQUEST_ID)
);
