﻿create table NDS2_MRR_USER.DICT_ACTIONS
(
  id         NUMBER not null,
  action_name VARCHAR2(256),
  constraint DICT_ACTIONS_PK primary key (ID)
);
