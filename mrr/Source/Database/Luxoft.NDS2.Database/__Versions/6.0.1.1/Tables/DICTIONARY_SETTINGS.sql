﻿create table NDS2_MRR_USER.Dictionary_Table_Settings
(
  Id number not null,
  Name varchar2(32) not null,
  Caption varchar2(256) not null,
  IsEditable number(1) not null,
  Constraint PK_DictionaryTableSettings primary key (Id) ENABLE
);

create table NDS2_MRR_USER.Dictionary_Column_Settings
(
  Id number not null,
  Table_Name varchar2(32) not null,
  Table_Name_Edit varchar2(32),
  Column_Name varchar2(32) not null,
  Caption varchar2(256) not null,
  Rank number(10) not null,
  IsKey number(1) not null,
  IsEditable number(1) not null,
  Width number(10) not null,
  Sorting varchar2(16),
  Visible number(1) not null,
  Constraint PK_DictionaryColumnSettings primary key (Id) ENABLE
);