﻿CREATE TABLE NDS2_MRR_USER.REPORT_DECLARATION_RAW
(   
  ID NUMBER(12) NOT NULL ENABLE, 
  Submit_Date DATE NOT NULL,
  Region_Code VARCHAR(4) NOT NULL,
  Soun_Code VARCHAR(4) NOT NULL,
  Fiscal_Year NUMBER(4) NOT NULL,
  Tax_period NUMBER(2) NOT NULL,
  Decl_Count_Total NUMBER(19),
  Decl_Count_Null NUMBER(19),
  Decl_Count_Compensation NUMBER(19),
  Decl_Count_Payment NUMBER(19),
  Decl_Count_Not_Submit NUMBER(19),
  Decl_Count_Revised_Total NUMBER(19),
  TaxBase_Sum_Compensation NUMBER(19,2),
  TaxBase_Sum_Payment NUMBER(19,2),
  NdsCalculated_Sum_Compensation NUMBER(19,2),
  NdsCalculated_Sum_Payment NUMBER(19,2),
  NdsDeduction_Sum_Compensation NUMBER(19,2),
  NdsDeduction_Sum_Payment NUMBER(19,2),
  Nds_Sum_Compensation NUMBER(19,2),
  Nds_Sum_Payment NUMBER(19,2),
  CONSTRAINT REPORT_REPORT_DECL_RAW_PK PRIMARY KEY (ID) ENABLE
);