﻿create table NDS2_MRR_USER.DOC_STATUS_HISTORY
(
  doc_id number(38) not null,
  status number(2) not null,
  status_date date not null
);
