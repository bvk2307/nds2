﻿create table NDS2_MRR_USER.SELECTION_DISCREPANCY 
(
   REQUEST_ID           NUMBER,
   DISCREPANCY_ID       NUMBER,
   IS_IN_PROCESS        NUMBER(1)
);
