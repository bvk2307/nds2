﻿create table NDS2_MRR_USER.CLAIM_KS_SOUN_PERMISSION
(
  SOUN_CODE varchar2(4 char) not null,
  PERMISSION number(1) default 1 not null,
  constraint CLAIM_KS_SOUN_PERMISSION_PK primary key (SOUN_CODE)
);