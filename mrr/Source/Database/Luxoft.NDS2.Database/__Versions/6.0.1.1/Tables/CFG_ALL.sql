﻿create table NDS2_MRR_USER.CFG_PARAM
(
  Id numeric not null,
  Name varchar2(256  CHAR) not null,
  Description varchar2(512),
  Constraint PK$CFG_PARAM primary key (Id) using index
);

create table NDS2_MRR_USER.CFG_REGION
(
  Id numeric not null,
  Region_Code varchar2(4  CHAR) not null,
  Cfg_Param_Id numeric not null,
  Value varchar2(128),
  Constraint PK$CFG_REGION primary key (Id) using index
);
