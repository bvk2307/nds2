﻿CREATE TABLE NDS2_MRR_USER.INVOICE_REQUEST
(   
 ID NUMBER(10) NOT NULL ENABLE,
    STATUS NUMBER(1) NOT NULL,
    requestdate date not null,
    process_date date,
    load_mismatches NUMBER(1) default 0,
    load_only_targets NUMBER(1) default 0,
    request_type number(1)  default 1 not null, /*1-запрос по СФ, 2-запрос расхождений*/
 CONSTRAINT INVOICE_REQUEST_PK PRIMARY KEY (ID) ENABLE
);
