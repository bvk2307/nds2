﻿create table NDS2_MRR_USER.SEOD_PROLONG_DECISION
(
  doc_id NUMBER(38) not null,
  doc_num varchar2(30) not null,
  doc_date date not null,
  prolong_date date not null,
  constraint pk_doc_prolong_decision primary key (doc_id)
);