﻿create table NDS2_MRR_USER.SEOD_DECLARATION
(
   ID                 NUMBER,
   NDS2_ID            NUMBER not null,
   TYPE				  NUMBER(1) not null,
   SONO_CODE          varchar2(4) not null,
   TAX_PERIOD         varchar2(2) not null,
   FISCAL_YEAR        varchar2(4) not null,
   CORRECTION_NUMBER  varchar2(3),
   DECL_REG_NUM       number(20) not null,
   DECL_FID           varchar2(128) not null,
   TAX_PAYER_TYPE     number(1) not null,
   INN                varchar2(12) not null,
   KPP                varchar2(9),
   INSERT_DATE        DATE not null,
   EOD_DATE           DATE,
   DATE_RECEIPT       DATE
);
