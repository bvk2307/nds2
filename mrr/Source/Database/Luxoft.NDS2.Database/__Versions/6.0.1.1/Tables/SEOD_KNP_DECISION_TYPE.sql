﻿-- Create table
create table NDS2_MRR_USER.SEOD_KNP_DECISION_TYPE
(
  id   number not null,
  name varchar2(50) not null,
  description varchar2(500) null
)
;
-- Add comments to the table 
comment on table NDS2_MRR_USER.SEOD_KNP_DECISION_TYPE
  is 'Справочник типов решений по КПН';
-- Add comments to the columns 
comment on column NDS2_MRR_USER.SEOD_KNP_DECISION_TYPE.id
  is 'Идентификатор типа решения по КНП';
comment on column NDS2_MRR_USER.SEOD_KNP_DECISION_TYPE.name
  is 'Наименование типа решения по КНП';
comment on column NDS2_MRR_USER.SEOD_KNP_DECISION_TYPE.description
  is 'Описание типа решения по КНП';
-- Create/Recreate primary, unique and foreign key constraints 
alter table NDS2_MRR_USER.SEOD_KNP_DECISION_TYPE
  add constraint PK_SEOD_KNP_DECISION_TYPE primary key (ID);
