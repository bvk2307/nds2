﻿
create table NDS2_MRR_USER.TEST_PYRAMID_SUMMARY
(
  ID                      NUMBER NOT NULL,
  CONTRACTOR_NAME         VARCHAR2(256),
  CONTRACTOR_INN          VARCHAR2(12),
  CONTRACTOR_KPP          VARCHAR2(9),
  FEDERAL_DISTRICT        VARCHAR2(128),
  REGION                  VARCHAR2(512),
  TAX_AUTHORITY           VARCHAR2(512),
  NDS_IN                  NUMBER,
  NDS_OUT                 NUMBER,
  DISCREPANCY_TOTAL_COUNT NUMBER,
  DISCREPANCY_TOTAL_SUM   NUMBER,
  DECLARATION_PROVIDED    CHAR(1)
);

