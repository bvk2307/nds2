﻿-- Create table
create table NDS2_MRR_USER.REQUEST_STATUS
(
  id   number not null,
  name varchar2(250)
)
;
-- Add comments to the table 
comment on table NDS2_MRR_USER.REQUEST_STATUS
  is 'Справочник статусов обработки запросов данных в МС';
-- Add comments to the columns 
comment on column NDS2_MRR_USER.REQUEST_STATUS.name
  is 'Описание статуса';
-- Create/Recreate primary, unique and foreign key constraints 
alter table NDS2_MRR_USER.REQUEST_STATUS
  add constraint PK$REQUEST_STATUS primary key (ID);
