﻿create table nds2_mrr_user.dict_invoice_line_number
(
  name varchar2(100 char),
  chapter number(2),
  line_number varchar2(3 char),
  constraint pk_dict_invoice_fields primary key (name, chapter)
);