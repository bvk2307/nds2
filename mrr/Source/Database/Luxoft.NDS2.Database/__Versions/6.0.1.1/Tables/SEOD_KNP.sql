﻿-- Create table
create table NDS2_MRR_USER.SEOD_KNP
(
  knp_id              number not null,
  declaration_reg_num number(13) not null,
  creation_date       date not null,
  ifns_code           varchar2(4) null,
  completion_date     date
);

-- Add comments to the table 
comment on table NDS2_MRR_USER.SEOD_KNP
  is 'Данные о статусе камеральной проверки декларации';
-- Add comments to the columns 
comment on column NDS2_MRR_USER.SEOD_KNP.knp_id
  is 'Идентификатор КНП в МРР';
comment on column NDS2_MRR_USER.SEOD_KNP.declaration_reg_num
  is 'Регистрационный номер декларации в СЭОД';
comment on column NDS2_MRR_USER.SEOD_KNP.creation_date
  is 'Дата загрузки КНП в МРР';
comment on column NDS2_MRR_USER.SEOD_KNP.completion_date
  is 'Дата завершения КНП';
comment on column NDS2_MRR_USER.SEOD_KNP.ifns_code
  is 'Код налогового органа';
-- Create/Recreate primary, unique and foreign key constraints 
alter table NDS2_MRR_USER.SEOD_KNP
  add constraint PK_SEOD_KNP primary key (KNP_ID);
  
