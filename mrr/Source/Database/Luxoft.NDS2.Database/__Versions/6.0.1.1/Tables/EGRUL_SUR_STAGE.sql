﻿create table NDS2_MRR_USER.EGRN_IP_STAGE
(
  id           NUMBER not null,
  innfl        VARCHAR2(12 CHAR),
  ogrnip       CHAR(15),
  last_name    VARCHAR2(60),
  first_name   VARCHAR2(60),
  patronymic   VARCHAR2(60),
  date_birth   DATE,
  doc_code     CHAR(2),
  doc_num      VARCHAR2(25),
  doc_date     DATE,
  doc_org      VARCHAR2(254),
  doc_org_code VARCHAR2(7),
  adr          VARCHAR2(254),
  code_no      CHAR(4),
  date_on      DATE,
  reason_on    CHAR(3),
  date_off     DATE,
  reason_off   CHAR(2)
);

comment on table NDS2_MRR_USER.EGRN_IP_STAGE
  is 'Сведения о индивидуальных предпринимателях';
comment on column NDS2_MRR_USER.EGRN_IP_STAGE.id
  is 'Уникальный идентификатор';
comment on column NDS2_MRR_USER.EGRN_IP_STAGE.innfl
  is 'ИННФЛ';
comment on column NDS2_MRR_USER.EGRN_IP_STAGE.ogrnip
  is 'ОГРНИП';
comment on column NDS2_MRR_USER.EGRN_IP_STAGE.last_name
  is 'Фамилия';
comment on column NDS2_MRR_USER.EGRN_IP_STAGE.first_name
  is 'Имя';
comment on column NDS2_MRR_USER.EGRN_IP_STAGE.patronymic
  is 'Отчество';
comment on column NDS2_MRR_USER.EGRN_IP_STAGE.date_birth
  is 'Дата рождения';
comment on column NDS2_MRR_USER.EGRN_IP_STAGE.doc_code
  is 'Код документа, удостоверяющего личность, по СПДУЛ';
comment on column NDS2_MRR_USER.EGRN_IP_STAGE.doc_num
  is 'Номер документа, удостоверяющего личность';
comment on column NDS2_MRR_USER.EGRN_IP_STAGE.doc_date
  is 'Дата выдачи документа, удостоверяющего личность';
comment on column NDS2_MRR_USER.EGRN_IP_STAGE.doc_org
  is 'Орган, выдавший документ, удостоверяющий личность';
comment on column NDS2_MRR_USER.EGRN_IP_STAGE.doc_org_code
  is 'Код органа, выдавшего документ, удостоверяющий личность';
comment on column NDS2_MRR_USER.EGRN_IP_STAGE.adr
  is 'Адрес места жительства';
comment on column NDS2_MRR_USER.EGRN_IP_STAGE.code_no
  is 'Код налогового органа по месту постановки на учет в качестве ИП (по СОУН)';
comment on column NDS2_MRR_USER.EGRN_IP_STAGE.date_on
  is 'Дата постановки на учет в качестве ИП';
comment on column NDS2_MRR_USER.EGRN_IP_STAGE.reason_on
  is 'Код причины постановки на учет в качестве ИП (по СППУФЛ)';
comment on column NDS2_MRR_USER.EGRN_IP_STAGE.date_off
  is 'Дата снятия с учета в качестве ИН';
comment on column NDS2_MRR_USER.EGRN_IP_STAGE.reason_off
  is 'Код причины снятия с учета в качестве ИП (по СПСУФЛ)';

alter table NDS2_MRR_USER.EGRN_IP_STAGE  add constraint PK_EGRN_IP_STAGE_ID primary key (ID);


create table NDS2_MRR_USER.EGRN_UL_STAGE
(
  id          NUMBER not null,
  inn         VARCHAR2(10),
  kpp         VARCHAR2(9),
  ogrn        VARCHAR2(13),
  name_full   VARCHAR2(1000),
  date_reg    DATE,
  code_no     CHAR(4),
  date_on     DATE,
  date_off    DATE,
  reason_off  CHAR(2),
  adr         VARCHAR2(254),
  ust_capital NUMBER(20,4),
  is_biggest   CHAR(1)	
)
;
comment on table NDS2_MRR_USER.EGRN_UL_STAGE
  is 'Сведения о юридических лицах';
comment on column NDS2_MRR_USER.EGRN_UL_STAGE.id
  is 'Уникальный идентификатор';
comment on column NDS2_MRR_USER.EGRN_UL_STAGE.inn
  is 'ИНН';
comment on column NDS2_MRR_USER.EGRN_UL_STAGE.kpp
  is 'КПП по месту нахождения';
comment on column NDS2_MRR_USER.EGRN_UL_STAGE.ogrn
  is 'ОГРН';
comment on column NDS2_MRR_USER.EGRN_UL_STAGE.name_full
  is 'Полное наименование';
comment on column NDS2_MRR_USER.EGRN_UL_STAGE.date_reg
  is 'Дата регистрации';
comment on column NDS2_MRR_USER.EGRN_UL_STAGE.code_no
  is 'Код НО по месту нахождения (по СОУН)';
comment on column NDS2_MRR_USER.EGRN_UL_STAGE.date_on
  is 'Дата постановки на учет по месту нахождения';
comment on column NDS2_MRR_USER.EGRN_UL_STAGE.date_off
  is 'Дата снятия с учета по месту нахождения';
comment on column NDS2_MRR_USER.EGRN_UL_STAGE.reason_off
  is 'Код причины снятия с учета по месту нахождения (по СПСУНО)';
comment on column NDS2_MRR_USER.EGRN_UL_STAGE.adr
  is 'Юридический адрес';
comment on column NDS2_MRR_USER.EGRN_UL_STAGE.ust_capital
  is 'Размер уставного капитала';

alter table NDS2_MRR_USER.EGRN_UL_STAGE add constraint PK_EGRN_UL_STAGE_ID primary key (ID);

create table NDS2_MRR_USER.SUR_STAGE
(
 inn			VARCHAR2(12 CHAR),
 fiscal_year    VARCHAR2(4 CHAR),
 fiscal_period  VARCHAR2(2 CHAR),
 sign_code      CHAR(1 CHAR),
 update_date    DATE
);
