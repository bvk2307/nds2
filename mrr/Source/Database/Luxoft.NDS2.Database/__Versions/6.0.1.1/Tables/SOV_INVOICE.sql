﻿create table NDS2_MRR_USER.SOV_INVOICE
(
   REQUEST_ID           NUMBER               not null,
   DECLARATION_VERSION_ID       NUMBER,
   CHAPTER              NUMBER(2),
   ORDINAL_NUMBER	    NUMBER(19),
   OKV_CODE				VARCHAR2(3 CHAR),/*Код валюты*/
   CREATE_DATE          DATE,/*Дата выставления(10-010)*/
   RECEIVE_DATE         DATE,/*Дата получения(11-010)*/
   OPERATION_CODE       VARCHAR2(512 CHAR),/*Код вида операции(8-010,9-010,10-020,11-020)*/
   INVOICE_NUM          VARCHAR2(128 CHAR),/*Номер СФ(8-020,9-020,10-030,11-030)*/
   INVOICE_DATE         DATE,/*Дата СФ(8-030,9-030,10-040,11-040)*/
   CHANGE_NUM           VARCHAR2(256 CHAR),/*Номер исправления СФ(8-040,9-040,10-050,11-050)*/
   CHANGE_DATE          DATE,/*Дата исправления СФ(8-050,9-050,10-060,11-060)*/
   CORRECTION_NUM       VARCHAR2(256 CHAR),/*Номер корректировачного СФ(8-060,9-060,10-070,11-070)*/
   CORRECTION_DATE      DATE,/*Дата корректировачного СФ(8-070,9-070,10-080,11-080)*/
   CHANGE_CORRECTION_NUM VARCHAR2(256 CHAR),/*Номер испр.корректировачного СФ(8-080,9-080,10-090,11-090)*/
   CHANGE_CORRECTION_DATE DATE,/*Дата испр.корректировачного СФ(8-090,9-090,10-100,11-100)*/
   RECEIPT_DOC_NUM      VARCHAR2(32 CHAR),/*Номер документа подтв. оплаты(8-100,9-120)*/
   RECEIPT_DOC_DATE     DATE,/*Дата документа подтв. оплаты(8-110,9-130)*/
   BUY_ACCEPT_DATE      DATE,/*Дата принятия на учет товаров (работ, услуг), имущественных прав(8-120)*/
   BUYER_INN            VARCHAR2(12 CHAR), /*ИНН покупателя*/
   BUYER_KPP            VARCHAR2(9 CHAR),/*КПП покупателя*/
   SELLER_INN           VARCHAR2(2048 CHAR),/*ИНН Продавца(множествненное сопоставление)*/
   SELLER_KPP           VARCHAR2(2048 CHAR),/*КПП продавца(множествненное сопоставление)*/
   SELLER_INVOICE_NUM   VARCHAR2(2048 CHAR), /*Номер СФ получ. от продавца(10-130)*/
   SELLER_INVOICE_DATE  DATE,/*Дата СФ получ. от продавца(10-140)*/
   BROKER_INN           VARCHAR2(12 CHAR),/*ИНН посредника (8-140,9-100,11-120)*/
   BROKER_KPP           VARCHAR2(9 CHAR),/*КПП посредника (8-140,9-100,11-120)*/
   DEAL_KIND_CODE       NUMBER,/*Код вида сделки(11-130)*/
   CUSTOMS_DECLARATION_NUM VARCHAR2(64 CHAR),/*Номер таможенной декларации(8-150)*/
   PRICE_BUY_AMOUNT     NUMBER(19,2),
   PRICE_BUY_NDS_AMOUNT NUMBER(19,2),
   PRICE_SELL           NUMBER(19,2),
   PRICE_SELL_IN_CURR   NUMBER(19,2),
   PRICE_SELL_18        NUMBER(19,2),
   PRICE_SELL_10        NUMBER(19,2),
   PRICE_SELL_0         NUMBER(19,2),
   PRICE_NDS_18         NUMBER(19,2),
   PRICE_NDS_10         NUMBER(19,2),
   PRICE_TAX_FREE       NUMBER(19,2),
   PRICE_TOTAL          NUMBER(19,2),
   PRICE_NDS_TOTAL      NUMBER(19,2),
   DIFF_CORRECT_DECREASE NUMBER(19,2),
   DIFF_CORRECT_INCREASE NUMBER(19,2),
   DIFF_CORRECT_NDS_DECREASE NUMBER(19,2),
   DIFF_CORRECT_NDS_INCREASE NUMBER(19,2),
   PRICE_NDS_BUYER      NUMBER(19,2),
   ROW_KEY              VARCHAR2(128 CHAR) not null,
   ACTUAL_ROW_KEY       VARCHAR2(128 CHAR),
   COMPARE_ROW_KEY		VARCHAR2(128 CHAR),
   COMPARE_ALGO_ID		NUMBER,
   FORMAT_ERRORS		VARCHAR2(128 CHAR),
   LOGICAL_ERRORS		VARCHAR2(128 CHAR),
   SELLER_AGENCY_INFO_INN VARCHAR2(12 CHAR), /*посредрнеческая деятельность из раздела 10(генерит множественноть СФ, за искл данных атрибутов)*/
   SELLER_AGENCY_INFO_KPP VARCHAR2(9 CHAR),/*посредрнеческая деятельность из раздела 10(генерит множественноть СФ, за искл данных атрибутов)*/
   SELLER_AGENCY_INFO_NAME VARCHAR2(512 CHAR),/*посредрнеческая деятельность из раздела 10(генерит множественноть СФ, за искл данных атрибутов)*/
   SELLER_AGENCY_INFO_NUM VARCHAR2(128 CHAR), /*посредрнеческая деятельность из раздела 10(генерит множественноть СФ, за искл данных атрибутов)*/
   SELLER_AGENCY_INFO_DATE DATE, /*посредрнеческая деятельность из раздела 10(генерит множественноть СФ, за искл данных атрибутов)*/
   IS_DOP_LIST			NUMBER(1), /*признак доп.листа*/
   IS_IMPORT            NUMBER(1)
);
