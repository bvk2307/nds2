﻿CREATE TABLE NDS2_MRR_USER.REPORT_CHECK_LOGIC_CONTROL
(   
  ID NUMBER(12) NOT NULL ENABLE, 
  TASK_ID NUMBER NOT NULL,
  ACTUAL_NUM NUMBER(1) NOT NULL,
  AGG_ROW NUMBER(1) NOT NULL,
  NUM VARCHAR2(32),
  iNUM NUMBER,
  Name VARCHAR2(1024),
  ErrorCount NUMBER(19),
  ErrorCountPercentage NUMBER(3),
  ErrorFrequencyInvoiceNotExact NUMBER(3),
  ErrorFrequencyInvoiceGap NUMBER(3),
  ErrorFrequencyInvoiceExact NUMBER(3),
  ErrorFrequencyInnerInvoice NUMBER(3),
  ErrorFrequencyOuterInvoice NUMBER(3),
  CONSTRAINT REPORT_CHECK_LOGIC_CONTROL_PK PRIMARY KEY (ID) ENABLE
);