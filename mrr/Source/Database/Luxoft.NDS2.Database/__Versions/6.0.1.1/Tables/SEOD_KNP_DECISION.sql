﻿-- Create table
create table NDS2_MRR_USER.SEOD_KNP_DECISION
(
  knp_id   number not null,
  type_id  number not null,
  doc_date date,
  doc_num  varchar2(100),
  amount   number(19,2)
)
;
-- Add comments to the table 
comment on table NDS2_MRR_USER.SEOD_KNP_DECISION
  is 'Решения по КНП';
-- Add comments to the columns 
comment on column NDS2_MRR_USER.SEOD_KNP_DECISION.knp_id
  is 'Идентификатор КНП (ссылка на SEOD_KNP)';
comment on column NDS2_MRR_USER.SEOD_KNP_DECISION.type_id
  is 'Идентификатор типа решения (ссылка на SEOD_KNP_DECISION_TYPE)';
comment on column NDS2_MRR_USER.SEOD_KNP_DECISION.doc_date
  is 'Дата Документа';
comment on column NDS2_MRR_USER.SEOD_KNP_DECISION.doc_num
  is 'Номер Документа';
comment on column NDS2_MRR_USER.SEOD_KNP_DECISION.amount
  is 'Сумма (если решение предполагает взыскание или возмещение)';
