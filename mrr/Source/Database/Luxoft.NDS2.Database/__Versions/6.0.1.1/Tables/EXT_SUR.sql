create table NDS2_MRR_USER.EXT_SUR
(
  inn           VARCHAR2(12 CHAR),
  fiscal_year   VARCHAR2(4 CHAR),
  fiscal_period VARCHAR2(2 CHAR),
  sign_code     CHAR(1 CHAR),
  update_date   DATE,
  is_actual     NUMBER
);

alter table NDS2_MRR_USER.EXT_SUR add constraint pk_ext_sur primary key (inn, fiscal_year, fiscal_period);
