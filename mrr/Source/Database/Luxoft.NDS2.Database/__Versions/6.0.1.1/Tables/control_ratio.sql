﻿create table NDS2_MRR_USER.claim_control_ratio
(
  claim_id numeric not null,
  control_ratio_id numeric not null,
  Constraint pk_claim_control_ratio primary key (claim_id, control_ratio_id)
);

create table NDS2_MRR_USER.control_ratio_comment
(
  Id numeric not null,
  User_Comment varchar2(2048 CHAR),
  constraint pk_control_ratio_comment primary key (Id)
);

create table NDS2_MRR_USER.control_ratio_type
(
  Code varchar(10) not null,
  Formulation varchar2(1024),
  Calculation_Condition varchar2(2048 CHAR),
  Calculation_Left_Side varchar2(2048 CHAR),
  Calculation_Right_Side varchar2(2048 CHAR),
  Calculation_Operator varchar2(2 CHAR),
  Description varchar2(2048 CHAR),
  Constraint pk_control_ratio_type primary key (Code)
);