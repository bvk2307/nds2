﻿create table NDS2_MRR_USER.Object_Lock
(
    Object_Id number not null,
    Object_Type_Id number not null,
	Object_Key varchar2(128) not null,
    User_Name varchar2(128) not null,
	Host_Name varchar2(128) not null,
    LockTime timestamp,
    constraint PK_Object_Lock primary key (Object_Id, Object_Type_Id)
);

create table NDS2_MRR_USER.Object_Lock_Type
(
    Object_Type_Id number not null,
    Name varchar2(32) not null,
    constraint PK_Object_Lock_Type primary key (Object_Type_Id)
);