﻿create table NDS2_MRR_USER.LOGICAL_ERRORS_MAPPING
(
  Error_Code    NUMBER(4) not null,
  Chapter       NUMBER(2) not null,
  Is_Dop_List   NUMBER(2) not null,
  Line_Number   VARCHAR2(8) not null,
  Property_Name VARCHAR2(256) not null,
  Name_Check    VARCHAR2(256) not null,
  Descreption   VARCHAR2(4000) not null,
  Constraint PK_LOGICAL_ERRORS_MAPPING primary key (Error_Code, Chapter, Is_Dop_List, Line_Number)
);