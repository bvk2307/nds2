﻿create table NDS2_MRR_USER.Action_History
(
  id             NUMBER not null,
  cd             NUMBER,
  status         NUMBER,
  change_date    DATE,
  action_comment VARCHAR2(2000 CHAR),
  user_name      VARCHAR2(128),
  action         NUMBER,
  constraint ACTION_HISTORY_PK primary key (ID)
);

create table NDS2_MRR_USER.Comment_History
(
  Object_Id       NUMBER not null,
  Object_Type_Id  NUMBER not null,
  Issue_Date      DATE not null,
  Author          VARCHAR2(128 CHAR) not null,
  Comment_Text    VARCHAR2(2000 CHAR) not null,
  constraint PK_Comment_History primary key (Object_Id, Object_Type_Id, Issue_Date)
);