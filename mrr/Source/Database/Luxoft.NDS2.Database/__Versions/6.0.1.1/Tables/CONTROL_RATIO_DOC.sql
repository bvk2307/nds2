﻿create table NDS2_MRR_USER.CONTROL_RATIO_DOC
(
  cr_id          NUMBER not null,
  doc_id         NUMBER not null,
  submit_date    DATE not null,
  constraint CTRL_RATIO_PK primary key (cr_id, doc_id)
);
