﻿CREATE TABLE NDS2_MRR_USER.REPORT_DECLARATION
(   
  ID NUMBER(12) NOT NULL ENABLE, 
  TASK_ID NUMBER NOT NULL,
  ACTUAL_NUM NUMBER(1) NOT NULL,
  AGG_ROW NUMBER(1) NOT NULL,
  Name_Group VARCHAR(255),
  Decl_Count_Total NUMBER(19),
  Decl_Count_Null NUMBER(19),
  Decl_Count_Compensation NUMBER(19),
  Decl_Count_Payment NUMBER(19),
  Decl_Count_Not_Submit NUMBER(19),
  Decl_Count_Revised_Total NUMBER(19),
  TaxBase_Sum_Compensation NUMBER(19,2),
  TaxBase_Sum_Payment NUMBER(19,2),
  NdsCalculated_Sum_Compensation NUMBER(19,2),
  NdsCalculated_Sum_Payment NUMBER(19,2),
  NdsDeduction_Sum_Compensation NUMBER(19,2),
  NdsDeduction_Sum_Payment NUMBER(19,2),
  Nds_Sum_Compensation NUMBER(19,2),
  Nds_Sum_Payment NUMBER(19,2),
  CONSTRAINT REPORT_REPORT_DECLARATION_PK PRIMARY KEY (ID) ENABLE
);