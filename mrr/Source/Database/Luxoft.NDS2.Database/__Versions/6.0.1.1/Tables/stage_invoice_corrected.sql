﻿create table NDS2_MRR_USER.stage_invoice_corrected
(
   id number not null,	
   explain_id number not null,
   invoice_original_id VARCHAR2(128 CHAR) not null,
   field_name varchar(32 CHAR) not null,
   field_value varchar(256 CHAR), 
   constraint pk_stage_invoice_corrected primary key (id) enable
);