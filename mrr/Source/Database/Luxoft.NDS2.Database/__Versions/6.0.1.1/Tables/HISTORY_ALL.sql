﻿create table NDS2_MRR_USER.HIST_SELECTION_STATUS
(
  id  NUMBER(10) not null,
  dt  DATE not null,
  val NUMBER(2)
);
