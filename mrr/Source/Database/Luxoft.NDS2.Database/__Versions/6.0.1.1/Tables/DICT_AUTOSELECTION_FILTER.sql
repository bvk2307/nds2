﻿create table NDS2_MRR_USER.DICT_AUTOSELECTION_FILTER
(
  id                NUMBER not null,
  filter_type       VARCHAR2(256),
  value_type_code   NUMBER,
  lookup_table_name VARCHAR2(64),
  internal_name     VARCHAR2(256),
  is_required       NUMBER(1) default 0
);
