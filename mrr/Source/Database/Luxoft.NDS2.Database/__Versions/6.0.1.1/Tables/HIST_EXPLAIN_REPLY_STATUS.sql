﻿create table NDS2_MRR_USER.HIST_EXPLAIN_REPLY_STATUS
(
  id NUMBER(12) not null,
  explain_id NUMBER not null,
  SUBMIT_DATE  DATE not null,
  STATUS_ID NUMBER(2) not null,
  constraint PK_HIST_EXPLAIN_REPLY_STATUS primary key (id)
);
