﻿create table NDS2_MRR_USER.DICT_FILTER_TYPES
(
  id              NUMBER not null,
  cd              NUMBER,
  filter_type     VARCHAR2(256),
  value_type_code NUMBER,
  lookup_table_name VARCHAR2(64),
  internal_name     VARCHAR2(256),
  fir_internal_name VARCHAR2(64),
  is_required	number(1) default 0,
  constraint DICT_FILTER_TYPES_PK primary key (ID)
);
