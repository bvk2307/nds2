﻿create table NDS2_MRR_USER.seod_reclaim_order
(
  Doc_Id number(38) not null,
  Doc_Num varchar2(100),
  Doc_Date date,
  Executive_Soun_Send_Date date,
  Executive_Soun_Recieve_Date date,
  Reject_Reason varchar2(2)
);