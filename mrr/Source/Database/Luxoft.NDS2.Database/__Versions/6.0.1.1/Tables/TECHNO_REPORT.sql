﻿create table NDS2_MRR_USER.TECHNO_REPORT_ITERATION
(
  Id number not null,
  Module_Name varchar2(16 char) not null,
  Create_Date date not null,
  Is_Closed number(1) not null ,
  constraint TECHNO_REPORT_ITERATION_PK primary key (Id)
);

create table NDS2_MRR_USER.TECHNO_REPORT_VALUE
(
  Iteration_Id number not null,
  Parameter_Id number not null,
  Value varchar2(128 char),
  constraint TECHNO_REPORT_VALUES_PK primary key (Iteration_Id, Parameter_Id)
);

create table NDS2_MRR_USER.TECHNO_REPORT_PARAMETER
(
  Id number not null,
  Name varchar2(1024 char),
  constraint TECHNO_REPORT_PARAMETER_PK primary key (Id)
);

create table NDS2_MRR_USER.TECHNO_REPORT_GP3_INPUT_DATA
(
  iteration_id number not null,
  parameter_id number not null,
  hour number not null,
  value number not null,
  constraint PK_TECHNO_REPORT_GP3_ID primary key (iteration_id, parameter_id, hour)
);

create table NDS2_MRR_USER.TECHNO_REPORT_TEMPLATE
(
  Name varchar2(128 char) not null,
  Value blob not null,
  constraint PK_Templates primary key (Name)
);