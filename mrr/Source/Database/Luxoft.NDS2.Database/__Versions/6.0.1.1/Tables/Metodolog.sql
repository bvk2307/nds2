﻿create table NDS2_MRR_USER.Metodolog_Region
(
  Id numeric not null,
  Context_Id numeric(2) not null,
  Code varchar2(2 CHAR) not null,
  Constraint PK_Metodolog_Region primary key (Id)
);

create table NDS2_MRR_USER.Metodolog_Region_Context
(
  Id numeric not null,
  Name varchar2(128 CHAR) not null,
  Constraint PK_Metodolog_Region_Context primary key (Id)
);

create table NDS2_MRR_USER.Metodolog_Region_Inspection
(
  Region_Id numeric not null,
  Code varchar2(4 CHAR) not null,
  Constraint PK_Metodolog_Region_Inspection primary key (Region_Id, Code)
);

create table NDS2_MRR_USER.Metodolog_Check_Employee
(
  Resp_Id numeric not null,
  SID varchar2(128 CHAR) not null,
  Type_Id numeric(1) not null,
  Constraint PK_Metodolog_Check_Employee primary key (Resp_Id, SID, Type_Id)
);