﻿CREATE TABLE NDS2_MRR_USER.REPORT_MATCHING_RULE
(   
  ID NUMBER(12) NOT NULL ENABLE, 
  TASK_ID NUMBER NOT NULL,
  ACTUAL_NUM NUMBER(1) NOT NULL,
  AGG_ROW NUMBER(1) NOT NULL,
  GroupNumNotExact VARCHAR(32),
  RULE_GROUP NUMBER(5),
  RuleNum VARCHAR(32),
  DiscrepancyCount NUMBER(19),
  DiscrepancyAmount NUMBER(19,2),
  PercentByCount NUMBER(3),
  PercentByAmount NUMBER(3),
  PercentInnerInvoiceErrorLC NUMBER(3),
  PercentOuterInvoiceErrorLC NUMBER(3),
  PercentDiscrepRequirement NUMBER(3),
  PercentDiscrepReclaimDoc NUMBER(3),
  PercentDiscrepOtherMNK NUMBER(3),
  PercentDiscrepRemoveTotal NUMBER(3),
  PercentDiscrepNotRemoveTotal NUMBER(3),
  CONSTRAINT REPORT_MATCHING_RULE_PK PRIMARY KEY (ID) ENABLE
);