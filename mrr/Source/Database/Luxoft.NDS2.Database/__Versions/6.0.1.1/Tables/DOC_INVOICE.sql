﻿create table NDS2_MRR_USER.DOC_INVOICE
(
  doc_id number(38) not null,
  invoice_row_key varchar2(128) not null,
  invoice_chapter number(2)
);
