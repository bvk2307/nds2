﻿create table NDS2_MRR_USER.DISCREP_STAGE_INVOICE_REQUEST
(
 request_id number not null,
 seod_decl_id number,
 invoice_rk varchar2(128) not null,
 stage number,
 submit_date date,
 processed number(1) not null
);
