﻿--таблица результатов с отчетом ЛК
create table NDS2_MRR_USER.SOV_LK_REPORT
(
   REQUEST_ID           NUMBER               not null,
   ERROR_CODE           NUMBER               not null,
   CNT                  NUMBER               not null,
   CNT_BAD_MISMATCH     NUMBER,
   CNT_BREAK_MISMATCH   NUMBER,
   CNT_IN               NUMBER,
   CNT_OUT              NUMBER
);
 
comment on column NDS2_MRR_USER.SOV_LK_REPORT.REQUEST_ID
  is 'Ид клиентского запроса';
comment on column NDS2_MRR_USER.SOV_LK_REPORT.ERROR_CODE
  is 'Код ошибки';
comment on column NDS2_MRR_USER.SOV_LK_REPORT.CNT
  is 'Количество ошибок';
comment on column NDS2_MRR_USER.SOV_LK_REPORT.CNT_BAD_MISMATCH
  is 'Количество ошибок в неточно сопоставленных записях о СФ';
comment on column NDS2_MRR_USER.SOV_LK_REPORT.CNT_BREAK_MISMATCH
  is 'Количество  ошибок в несопоставленных записях о СФ (“разрыв”) ';
comment on column NDS2_MRR_USER.SOV_LK_REPORT.CNT_IN
  is 'Количество  NDS2_MRR_USER.ошибок во входящих СФ';
comment on column NDS2_MRR_USER.SOV_LK_REPORT.CNT_OUT
  is 'Количество  ошибок в исходящих СФ';
