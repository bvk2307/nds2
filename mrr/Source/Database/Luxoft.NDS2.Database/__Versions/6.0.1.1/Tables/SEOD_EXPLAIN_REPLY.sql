﻿create table NDS2_MRR_USER.seod_explain_reply
(
   explain_id number not null,
   doc_id number not null,
   type_id number(2) not null,
   incoming_num varchar(128 CHAR),
   incoming_date date not null,
   executor_receive_date date,
   send_date_to_iniciator date,
   receive_by_tks number(1),
   status_id number(2),
   status_set_date date,
   user_comment varchar2(4000),
   FileNameOutput varchar(256 CHAR),
   FileSizeOutput number,
   constraint pk_explain_reply primary key (explain_id) enable
);