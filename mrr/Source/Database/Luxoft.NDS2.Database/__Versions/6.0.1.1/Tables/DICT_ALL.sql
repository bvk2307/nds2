﻿create table NDS2_MRR_USER.DICT_TAX_PERIOD
(
    code varchar2(2 CHAR) not null,
    description varchar2(512 CHAR) not null,
    CONSTRAINT PK_DICT_TAX_PERIOD_CODE PRIMARY KEY (code) ENABLE
);

create table NDS2_MRR_USER.federal_district
(
district_id number(4) not null,
description varchar2(512 CHAR) not null
);

create table NDS2_MRR_USER.federal_district_region
(
district_id number(4) not null,
region_code varchar2(2 CHAR) not null
);

create table NDS2_MRR_USER.invoice_request_status
(
 id number(1),
 description varchar2(64 char)
);

create table NDS2_MRR_USER.dict_discrepancy_status
(
	id number not null,
	name varchar2(64 CHAR) not null,
	rank number(2) not null,
	constraint pk_dict_discr_stat primary key (id)
);

create table NDS2_MRR_USER.dict_discrep_filter_status
(
	id number not null,
	name varchar2(128 CHAR) not null,
	rank number(2) not null,
	constraint pk_dict_discr_flt_stat primary key (id)
);

CREATE TABLE NDS2_MRR_USER.DICT_BOOK_TYPE
(	ID NUMBER NOT NULL ENABLE, 
	NAME VARCHAR2(128 CHAR) NOT NULL ENABLE, 
	CODE NUMBER NOT NULL ENABLE, 
	CONSTRAINT DICT_BOOK_TYPE_PK PRIMARY KEY (ID) ENABLE
);


CREATE TABLE NDS2_MRR_USER.DICT_CONTROL_TYPE 
(	ID NUMBER NOT NULL ENABLE, 
	NAME VARCHAR2(128 CHAR) NOT NULL ENABLE, 
	CONSTRAINT DICT_CONTROL_TYPE_PK PRIMARY KEY (ID) ENABLE
);


CREATE TABLE NDS2_MRR_USER.DICT_NALOG_PERIODS 
(	ID NUMBER NOT NULL ENABLE, 
	NAME VARCHAR2(256 CHAR) NOT NULL ENABLE, 
	CODE VARCHAR2(4 CHAR) NOT NULL ENABLE, 
	CONSTRAINT DICT_NALOG_PERIODS_PK PRIMARY KEY (ID) ENABLE
);


CREATE TABLE NDS2_MRR_USER.DICT_SELECTION_STATUS
(	ID NUMBER NOT NULL ENABLE, 
	NAME VARCHAR2(256 CHAR) NOT NULL ENABLE,
	RANK NUMBER NOT NULL ENABLE,
	CONSTRAINT DICT_SELECTION_STATUS_PK PRIMARY KEY (ID) ENABLE
);

create table NDS2_MRR_USER.SONO_EXT
(
	s_code varchar2(4 CHAR),
	s_parent_code varchar2(4 CHAR),
	s_name varchar2(512 CHAR),
	employers_count varchar2(512 CHAR),
	region_code varchar2(2 CHAR),
	type number(1)
);

CREATE TABLE NDS2_MRR_USER.DICT_DECLARATION_SIGN 
(
	s_code number(3),
	s_name varchar2(512 CHAR)
);

CREATE TABLE NDS2_MRR_USER.DICT_DISCREPANCY_TYPE 
(
	s_code number(3),
	s_name varchar2(512 CHAR),
	description varchar2(512 CHAR)
);

create table NDS2_MRR_USER.dict_sel2discr_status_rel
(
 sel_status_id number(2),
 discr_status_id number(2),
 constraint uk_sel2descr_ids unique (sel_status_id, discr_status_id)
);

CREATE TABLE NDS2_MRR_USER.USER_TO_N_SSRF 
(	
	"USER_SID" VARCHAR2(64 CHAR) NOT NULL ENABLE, 
	"N_SSRF_CODE" VARCHAR2(3) NOT NULL ENABLE
); 

CREATE INDEX NDS2_MRR_USER.USER_TO_N_SSRF_INDEX_MAIN ON NDS2_MRR_USER.USER_TO_N_SSRF ("USER_SID", "N_SSRF_CODE");

CREATE TABLE NDS2_MRR_USER.DICT_DISCREPANCY_KIND 
(
    ID NUMBER NOT NULL ENABLE, 
    CODE NUMBER NOT NULL ENABLE,
    NAME VARCHAR2(512 CHAR) NOT NULL ENABLE,
    CONSTRAINT DICT_DISCREPANCY_KIND_PK PRIMARY KEY (ID) ENABLE
);

CREATE TABLE NDS2_MRR_USER.DICT_SELECTION_TYPE
(	
	ID NUMBER NOT NULL ENABLE, 
    s_code NUMBER(3) NOT NULL ENABLE,
	s_name VARCHAR2(128 CHAR) NOT NULL ENABLE,
	CONSTRAINT DICT_SELECTION_TYPE_PK PRIMARY KEY (ID) ENABLE
);

create table NDS2_MRR_USER.Dict_SUR
(
  Code number(1) not null,
  Description varchar2(256 CHAR) not null,
  Color_ARGB number not null,
  constraint pk_dict_sur primary key (Code) ENABLE
);

create table NDS2_MRR_USER.Metodolog_Employee_Type
(
  Id numeric not null,
  Name varchar2(256 CHAR) not null,
  Constraint PK_Metodolog_Employee_Type primary key (Id) ENABLE
);
