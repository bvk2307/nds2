﻿-- Create table
create table NDS2_MRR_USER.BOOK_DATA_REQUEST
(
  id          number not null,
  inn         varchar2(12) not null,
  correctionnumber         varchar2(50) null,
  partitionnumber number null,
  period      varchar2(2) not null,
  year        varchar2(4) not null,
  status      number not null,
  requestdate date not null,
  process_date date
)
;
-- Add comments to the table 
comment on table NDS2_MRR_USER.BOOK_DATA_REQUEST
  is 'Содержит запросы со стороны МРР к МС для получения книг СФ по декларации';
-- Add comments to the columns 
comment on column NDS2_MRR_USER.BOOK_DATA_REQUEST.inn
  is 'ИНН подавшего декларацию (входной параметр запроса)';
comment on column NDS2_MRR_USER.BOOK_DATA_REQUEST.correctionnumber
  is 'Номер правки декларации (входной параметр запроса)';
comment on column NDS2_MRR_USER.BOOK_DATA_REQUEST.partitionnumber
  is 'Номер раздела книги декларации (входной параметр запроса)';
comment on column NDS2_MRR_USER.BOOK_DATA_REQUEST.period
  is 'Период подачи декларации (входной параметр запроса)';
comment on column NDS2_MRR_USER.BOOK_DATA_REQUEST.year
  is 'Год подачи декларации (входной параметр запроса)';
comment on column NDS2_MRR_USER.BOOK_DATA_REQUEST.status
  is 'Статус обработки запроса';
comment on column NDS2_MRR_USER.BOOK_DATA_REQUEST.requestdate
  is 'Дата регистрации запроса';
-- Create/Recreate primary, unique and foreign key constraints 
alter table NDS2_MRR_USER.BOOK_DATA_REQUEST
  add constraint PK$BOOK_DATA_REQUEST primary key (ID);
