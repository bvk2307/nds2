﻿create table NDS2_MRR_USER.SYSTEM_LOG
(
  ID number not null,
  TYPE number(2) not null,
  SITE varchar2(128),
  ENTITY_ID VARCHAR2(64),
  MESSAGE_CODE VARCHAR2(32),
  MESSAGE VARCHAR2(2048),
  LOG_DATE DATE
);
