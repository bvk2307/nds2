﻿create table NDS2_MRR_USER.V_EGRN_IP
(
  id           INTEGER not null,
  ogrnip       CHAR(45),
  last_name    VARCHAR2(180),
  first_name   VARCHAR2(180),
  patronymic   VARCHAR2(180),
  date_birth   DATE,
  doc_code     CHAR(6),
  doc_num      VARCHAR2(75),
  doc_date     DATE,
  doc_org      VARCHAR2(762),
  doc_org_code VARCHAR2(21),
  adr          VARCHAR2(762),
  code_no      CHAR(12),
  date_on      DATE,
  reason_on    CHAR(9),
  date_off     DATE,
  reason_off   CHAR(6),
  innfl        VARCHAR2(12 CHAR)
);

create table NDS2_MRR_USER.V_EGRN_UL
(
  id          NUMBER not null,
  inn         VARCHAR2(10),
  kpp         VARCHAR2(9),
  ogrn        VARCHAR2(13),
  name_full   VARCHAR2(1000),
  date_reg    DATE,
  code_no     CHAR(4),
  date_on     DATE,
  date_off    DATE,
  reason_off  CHAR(2),
  adr         VARCHAR2(254),
  ust_capital NUMBER(20,4),
  is_knp      VARCHAR2(1)
);


create table  NDS2_MRR_USER.BSSCHET_UL
(INN         VARCHAR2(10), --ИНН НП
 KPP         VARCHAR2(9), --КПП НП
 BIK         VARCHAR2(9), -- БИК КО
 NAMEKO      VARCHAR2(1000), -- Наименеование КО
 NOMSCH      VARCHAR2(20), --Номер счета
 PRVAL       VARCHAR2(1), --0-рублевый, 1-валютный
 DATEOPENSCH DATE --дата открытия счета
 );

create table  NDS2_MRR_USER.BSSCHET_IP
(INN         VARCHAR2(12), --ИНН НП
 BIK         VARCHAR2(9), -- БИК КО
 NAMEKO      VARCHAR2(1000), -- Наименеование КО
 NOMSCH      VARCHAR2(20), --Номер счета
 PRVAL       VARCHAR2(1), --0-рублевый, 1-валютный
 DATEOPENSCH DATE --дата открытия счета
 );
