﻿create table NDS2_MRR_USER.DISCREPANCIES_FILTERS
(
  id           NUMBER not null,
  cd           NUMBER,
  user_name    VARCHAR2(256),
  is_favourite VARCHAR2(7),
  name         VARCHAR2(256),
  data         NCLOB,
  CONSTRAINT DISCREPANCIES_FILTERS_PK PRIMARY KEY (ID) ENABLE
);