﻿create table NDS2_MRR_USER.AUTOSELECTION_FILTER
(
  id      NUMBER(10) not null,
  include CLOB,
  exclude CLOB,
  dt      DATE default CURRENT_TIMESTAMP not null
);
