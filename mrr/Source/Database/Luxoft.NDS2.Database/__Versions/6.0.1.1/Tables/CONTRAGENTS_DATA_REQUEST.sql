﻿-- Create table
create table NDS2_MRR_USER.CONTRAGENT_DATA_REQUEST
(
  id          number not null,
  inn         varchar2(12) not null,
  correction_number         number null,
  period      varchar2(4) not null,
  tax_year        varchar2(4) not null,
  request_status      number not null,
  request_date date not null,
  process_date date
)
;
-- Add comments to the table 
comment on table NDS2_MRR_USER.CONTRAGENT_DATA_REQUEST
  is 'Содержит запросы со стороны МРР к МС для получения данных по контрагентам-продавцам';
-- Add comments to the columns 
comment on column NDS2_MRR_USER.CONTRAGENT_DATA_REQUEST.inn
  is 'ИНН контрагента-продавца (входной параметр запроса)';
comment on column NDS2_MRR_USER.CONTRAGENT_DATA_REQUEST.correction_number
  is 'Номер правки декларации (входной параметр запроса)';
comment on column NDS2_MRR_USER.CONTRAGENT_DATA_REQUEST.period
  is 'Период подачи декларации (входной параметр запроса)';
comment on column NDS2_MRR_USER.CONTRAGENT_DATA_REQUEST.tax_year
  is 'Год подачи декларации (входной параметр запроса)';
comment on column NDS2_MRR_USER.CONTRAGENT_DATA_REQUEST.request_status
  is 'Статус обработки запроса';
comment on column NDS2_MRR_USER.CONTRAGENT_DATA_REQUEST.request_date
  is 'Дата регистрации запроса';
alter table NDS2_MRR_USER.CONTRAGENT_DATA_REQUEST
  add constraint PK$CONTRAGENT_DATA_REQUEST primary key (ID);

-- Create table
create table NDS2_MRR_USER.CONTRAGENT_PARAMS_REQUEST
(
  id          number not null,
  inn         varchar2(12) not null,
  contractor_inn         varchar2(12),
  correction_number         number null,
  period      varchar2(2) not null,
  tax_year        varchar2(4) not null,
  request_status      number not null,
  request_date date not null,
  process_date date
)
;
-- Add comments to the table 
comment on table NDS2_MRR_USER.CONTRAGENT_PARAMS_REQUEST
  is 'Содержит запросы со стороны МРР к МС для получения данных по контрагентам-продавцам';
-- Add comments to the columns 
comment on column NDS2_MRR_USER.CONTRAGENT_PARAMS_REQUEST.inn
  is 'ИНН контрагента-продавца (входной параметр запроса)';
comment on column NDS2_MRR_USER.CONTRAGENT_PARAMS_REQUEST.contractor_inn
  is 'ИНН покупателя (входной параметр запроса)';
comment on column NDS2_MRR_USER.CONTRAGENT_PARAMS_REQUEST.correction_number
  is 'Номер правки декларации (входной параметр запроса)';
comment on column NDS2_MRR_USER.CONTRAGENT_PARAMS_REQUEST.period
  is 'Период подачи декларации (входной параметр запроса)';
comment on column NDS2_MRR_USER.CONTRAGENT_PARAMS_REQUEST.tax_year
  is 'Год подачи декларации (входной параметр запроса)';
comment on column NDS2_MRR_USER.CONTRAGENT_PARAMS_REQUEST.request_status
  is 'Статус обработки запроса';
comment on column NDS2_MRR_USER.CONTRAGENT_PARAMS_REQUEST.request_date
  is 'Дата регистрации запроса';
alter table NDS2_MRR_USER.CONTRAGENT_PARAMS_REQUEST
  add constraint PK$CONTRAGENT_PARAMS_REQUEST primary key (ID);

CREATE TABLE NDS2_MRR_USER.SOV_CONTRAGENTS 
(
	REQUEST_ID NUMBER,
	DECLARATION_VERSION_ID NUMBER,
	CONTRACTOR_INN VARCHAR2(12 CHAR),
    AMOUNT_TOTAL NUMBER(19,2),
    AMOUNT_NDS NUMBER(19,2),
    NDS_WEIGHT NUMBER(5,2),
    OPERATIONS_COUNT NUMBER
)
;

CREATE TABLE NDS2_MRR_USER.SOV_CONTRAGENT_PARAMS
  (
	  REQUEST_ID NUMBER,
      ROW_KEY VARCHAR2(128 CHAR), 
      DECLARATION_VERSION_ID NUMBER, 
      CHAPTER NUMBER(2,0), 
      ORDINAL_NUMBER NUMBER(19,0), 
      OPERATION_CODE VARCHAR2(512 CHAR), 
      INVOICE_NUM VARCHAR2(128 CHAR), 
      INVOICE_DATE DATE, 
      CHANGE_NUM NUMBER(4,0), 
      CHANGE_DATE DATE, 
      CORRECTION_NUM VARCHAR2(256 CHAR), 
      CORRECTION_DATE DATE, 
      CHANGE_CORRECTION_NUM NUMBER(4,0), 
      CHANGE_CORRECTION_DATE DATE, 
      RECEIPT_DOC_NUM VARCHAR2(32), 
      RECEIPT_DOC_DATE DATE, 
      BUY_ACCEPT_DATE DATE, 
      SELLER_INN VARCHAR2(12 CHAR), 
      SELLER_KPP VARCHAR2(12 CHAR), 
      BROKER_INN VARCHAR2(12 CHAR), 
      BROKER_KPP VARCHAR2(9 CHAR), 
      CUSTOMS_DECLARATION_NUM VARCHAR2(64 CHAR), 
      OKV_CODE VARCHAR2(3), 
      PRICE_BUY_AMOUNT NUMBER(19,2), 
      PRICE_BUY_NDS_AMOUNT NUMBER(19,2), 
      PRICE_TOTAL NUMBER(19,2), 
      PRICE_NDS_TOTAL NUMBER(19,2), 
      DIFF_CORRECT_DECREASE NUMBER(19,2), 
      DIFF_CORRECT_INCREASE NUMBER(19,2), 
      DIFF_CORRECT_NDS_DECREASE NUMBER(19,2), 
      DIFF_CORRECT_NDS_INCREASE NUMBER(19,2), 
      NDS_WEIGHT NUMBER(5,2)
  );