﻿create table NDS2_MRR_USER.DICT_EXPLAIN_REPLY_STATUS
(
  id   NUMBER not null,
  name VARCHAR2(256) not null,
  constraint PK_DICT_EXPLAIN_REPLY_STATUS primary key (ID)
);