﻿-- Create table
create table NDS2_MRR_USER.CONFIGURATION
(
  parameter   VARCHAR2(128) not null,
  value       VARCHAR2(128) not null,
  default_value       VARCHAR2(128) not null,
  description VARCHAR2(256),
  Constraint PK$CONFIGURATION primary key (PARAMETER) using index
);
