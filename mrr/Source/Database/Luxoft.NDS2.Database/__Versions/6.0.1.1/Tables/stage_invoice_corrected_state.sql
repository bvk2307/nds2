﻿create table NDS2_MRR_USER.stage_invoice_corrected_state
(
   id number not null,	
   explain_id number not null,
   invoice_original_id VARCHAR2(128 CHAR) not null,
   invoice_state number(1), 
   constraint pk_stage_invoice_corrected_st primary key (id) enable
);