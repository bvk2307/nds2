﻿-- Create table
create table NDS2_MRR_USER.SOV_DISCREPANCY_TMP
(
   SOV_ID               VARCHAR2(128)  not null,
   CREATE_DATE          DATE,/*дата выявления*/
   TYPE                 NUMBER(1),
   COMPARE_KIND         NUMBER(1),
   RULE_GROUP           NUMBER(5),
   DEAL_AMNT            NUMBER(19,2),
   AMNT                 NUMBER(19,2),
   AMOUNT_PVP      NUMBER(19,2),
   COURSE        NUMBER(19,2),
   COURSE_COST      NUMBER(19,2),
   SUR_CODE         NUMBER(1),
   INVOICE_CHAPTER    NUMBER(2),
   INVOICE_RK           VARCHAR2(128),
   DECL_ID              NUMBER,
   INVOICE_CONTRACTOR_CHAPTER NUMBER(2),
   INVOICE_CONTRACTOR_RK VARCHAR2(128),
   DECL_CONTRACTOR_ID    NUMBER,
   STATUS                NUMBER(2),
   STAGE                 NUMBER DEFAULT 1,
   request_id            NUMBER not null,
   SIDE_PRIMARY_PROCESSING    NUMBER(1)
);
-- Create/Recreate primary, unique and foreign key constraints 
alter table NDS2_MRR_USER.SOV_DISCREPANCY_TMP
  add constraint PK_SOV_DISCREPANCY_TMP primary key (SOV_ID, REQUEST_ID);
