﻿-- Create table
create table NDS2_MRR_USER.SEOD_KNP_ACT
(
  knp_id            number not null,
  doc_data          date,
  doc_num           varchar2(100),
  sum_unpaid        number(19,2),
  sum_overestimated number(19,2),
  sum_compensation  number(19,2),
  sum_penalty		number(19,2)
)
;
-- Add comments to the table 
comment on table NDS2_MRR_USER.SEOD_KNP_ACT
  is 'Акты КНП';
-- Add comments to the columns 
comment on column NDS2_MRR_USER.SEOD_KNP_ACT.knp_id
  is 'Идентификатор КНП (ссылка на ПК SEOD_KNP)';
comment on column NDS2_MRR_USER.SEOD_KNP_ACT.doc_data
  is 'Дата документа';
comment on column NDS2_MRR_USER.SEOD_KNP_ACT.doc_num
  is 'Номер документа';
comment on column NDS2_MRR_USER.SEOD_KNP_ACT.sum_unpaid
  is 'Сумма неуплаченного (не полностью уплаченного налога (сбора))';
comment on column NDS2_MRR_USER.SEOD_KNP_ACT.sum_overestimated
  is 'Сумма завышения НДС, предъявленного к возмещению';
comment on column NDS2_MRR_USER.SEOD_KNP_ACT.sum_compensation
  is 'Сумма налога (сбора), исчисленная в завышенном размере';
comment on column NDS2_MRR_USER.SEOD_KNP_ACT.sum_penalty
  is 'Сумма пени';