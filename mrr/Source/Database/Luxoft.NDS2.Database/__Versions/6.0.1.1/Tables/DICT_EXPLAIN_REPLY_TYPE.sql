﻿create table NDS2_MRR_USER.DICT_EXPLAIN_REPLY_TYPE
(
  id   NUMBER not null,
  name VARCHAR2(64) not null,
  short_name VARCHAR2(32) not null,
  constraint PK_DICT_EXPLAIN_REPLY_TYPE primary key (ID)
);