﻿-- Create table
create table NDS2_MRR_USER.DISCREPANCY_REQUEST
(
  id                NUMBER not null,
  request_date       DATE not null,
  process_date       DATE null,
  status            NUMBER not null,
  inn               VARCHAR2(12 CHAR),
  period            NUMBER,
  year              NUMBER,
  correction_number NUMBER,
  kind              NUMBER,
  type              NUMBER
);
-- Add comments to the table 
comment on table NDS2_MRR_USER.DISCREPANCY_REQUEST
  is 'Запросы расхождений по декларациям к SOV';
-- Add comments to the columns 
comment on column NDS2_MRR_USER.DISCREPANCY_REQUEST.request_date
  is 'Дата создания запроса';
comment on column NDS2_MRR_USER.DISCREPANCY_REQUEST.process_date
  is 'Дата выполнения запроса';
comment on column NDS2_MRR_USER.DISCREPANCY_REQUEST.status
  is 'Статус запроса';
comment on column NDS2_MRR_USER.DISCREPANCY_REQUEST.inn
  is 'ИНН НП';
comment on column NDS2_MRR_USER.DISCREPANCY_REQUEST.period
  is 'Период подачи декларации';
comment on column NDS2_MRR_USER.DISCREPANCY_REQUEST.year
  is 'Год подачи декларации';
comment on column NDS2_MRR_USER.DISCREPANCY_REQUEST.correction_number
  is 'Корректировочный номер';
comment on column NDS2_MRR_USER.DISCREPANCY_REQUEST.kind
  is 'Вид расхождения';
comment on column NDS2_MRR_USER.DISCREPANCY_REQUEST.type
  is 'Тип расхождения';
-- Create/Recreate primary, unique and foreign key constraints 
alter table NDS2_MRR_USER.DISCREPANCY_REQUEST
  add constraint PK_DISCREPANCY_REQUEST primary key (ID);
