﻿create table NDS2_MRR_USER.DICT_DISCREPANCY_STAGE
(
  id   NUMBER not null,
  name VARCHAR2(64 CHAR) not null,
  constraint PK_DICT_DISCREPANCY_STAGE primary key (ID)
);