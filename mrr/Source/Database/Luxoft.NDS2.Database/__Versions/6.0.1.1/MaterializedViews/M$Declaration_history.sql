﻿create materialized view NDS2_MRR_USER.MV$Declaration_history as
select
sed.nds2_id as ID
,sed.decl_reg_num as SEOD_DECL_ID
,SD.ID as ASK_DECL_ID
,SD.ZIP as DECLARATION_VERSION_ID
,decode((row_number() over (partition by sed.TAX_PERIOD, sed.FISCAL_YEAR, sed.INN order by SED.CORRECTION_NUMBER desc)),1,1,0) as IS_ACTIVE
,case when D.DECLARATION_VERSION_ID is not null then 2 else (case when  SD.ID is not null then 1 else 0 end) end as ProcessingStage
,d.UPDATE_DATE
,sed.INN
,sed.KPP
,case when sd.KPPNP is null then (nvl(sd.FAMILIYANP, '')||' '||nvl(sd.IMYANP, '')||' '||nvl(sd.OTCHESTVONP, '')) else sd.NAIMORG end as NAME
,'-' as ADDRESS1
,'-' as ADDRESS2
,ssrf.s_code as REGION_CODE
,ssrf.s_name as REGION_NAME
,ssrf.s_code || '-' || ssrf.s_name as REGION
,soun.s_code as SOUN_CODE
,soun.s_name as SOUN_NAME
,soun.s_code || '-' || soun.s_name as SOUN
,case substr(sd.KPPNP, 5,2) when  '50' then 1 else 0 end as CATEGORY
,case substr(sd.KPPNP, 5,2) when  '50' then 'Да' else '' end as CATEGORY_RU
,sed.EOD_DATE as REG_DATE
,'-' as TAX_MODE
,sd.OKVED as OKVED_CODE
,'0' as CAPITAL
,sed.TAX_PERIOD
,sed.FISCAL_YEAR
,dtp.description || ' ' || sed.FISCAL_YEAR as  FULL_TAX_PERIOD 
,nvl(sd.DATADOK, sed.EOD_DATE) as DECL_DATE
,DECODE(sed.TYPE, 0, 'Декларация', 1, 'Журнал', '') as DECL_TYPE
,sed.TYPE as DECL_TYPE_CODE
,case
    when sd.SUMPU173_5 > 0 then 'К уплате'
      when sd.SUMPU173_1 > 0 then 'К уплате'
      when sd.SUMPU173_1 < 0 then 'К возмещению'
      else 'Не определено'
end as DECL_SIGN
,sed.CORRECTION_NUMBER
,case
      when sd.SUMPU173_5 > 0 then sd.SUMPU173_5
      when sd.SUMPU173_1 > 0 then sd.SUMPU173_1
      when sd.SUMPU173_1 < 0 then sd.SUMPU173_1
      else null end as COMPENSATION_AMNT
,case
      when sd.SUMPU173_5 > 0 then REPLACE(TO_CHAR(sd.SUMPU173_5, 'SFM999999999G999D009'),',',' ')
      when sd.SUMPU173_1 > 0 then REPLACE(TO_CHAR(sd.SUMPU173_1, 'SFM999999999G999D009'),',',' ')
      when sd.SUMPU173_1 < 0 then REPLACE(TO_CHAR(sd.SUMPU173_1, 'SFM999999999G999D009'),',',' ')
      else null end as COMPENSATION_AMNT_SIGN
,d.LK_ERRORS_COUNT
,d.CH8_DEALS_AMNT_TOTAL
,d.CH9_DEALS_AMNT_TOTAL
,d.CH8_NDS
,d.CH9_NDS
,d.DISCREP_CURRENCY_AMNT
,d.DISCREP_CURRENCY_COUNT
,d.GAP_DISCREP_COUNT
,d.GAP_DISCREP_AMNT
,d.WEAK_DISCREP_COUNT
,d.WEAK_DISCREP_AMNT
,d.NDS_INCREASE_DISCREP_COUNT
,d.NDS_INCREASE_DISCREP_AMNT
,d.GAP_MULTIPLE_DISCREP_COUNT
,d.GAP_MULTIPLE_DISCREP_AMNT
,(nvl(d.DISCREP_CURRENCY_COUNT, 0) + nvl(d.GAP_DISCREP_COUNT, 0) + nvl(d.WEAK_DISCREP_COUNT, 0) + nvl(d.NDS_INCREASE_DISCREP_COUNT, 0) + nvl(d.GAP_MULTIPLE_DISCREP_COUNT, 0)) as TOTAL_DISCREP_COUNT
,d.DISCREP_BUY_BOOK_AMNT
,d.DISCREP_SELL_BOOK_AMNT
,d.DISCREP_MIN_AMNT
,d.DISCREP_MAX_AMNT
,d.DISCREP_AVG_AMNT
,d.DISCREP_TOTAL_AMNT
,d.PVP_TOTAL_AMNT
,d.PVP_DISCREP_MIN_AMNT
,d.PVP_DISCREP_MAX_AMNT
,d.PVP_DISCREP_AVG_AMNT
,d.PVP_BUY_BOOK_AMNT
,d.PVP_SELL_BOOK_AMNT
,d.PVP_RECIEVE_JOURNAL_AMNT
,d.PVP_SENT_JOURNAL_AMNT
,d.SUR_CODE
,sur.description as SUR_DESCRIPTION
,d.CONTROL_RATIO_DATE
,d.CONTROL_RATIO_SEND_TO_SEOD
,nvl(KSAggr.KSCount, 0) as CONTROL_RATIO_COUNT
,knp.completion_date as DateCloseKNP
, case when knp.declaration_reg_num is null then '' 
  else
    (case when knp.completion_date is null 
    then 'Открыта КНП' else 'Закрыта КНП' end)
end as STATUS
,dsd.TOTAL_NDS_AMNT_BUYBOOK
,dsd.TOTAL_COST_SELL_18
,dsd.TOTAL_COST_SELL_10
,dsd.TOTAL_COST_SELL_0
,dsd.TOTAL_TAX_AMNT_18
,dsd.TOTAL_TAX_AMNT_10
,dsd.TOTAL_COST_SELL_TAXFREE
-- Тут псевдоданные, потом будем их считать нормально
,'-' AS RECORD_MARK                                            --Признак записи [Т, П, Т-П, Н, МНК, NULL]
,NDS_INCREASE_DISCREP_COUNT*3 AS BUYBOOK_CONTRAGENT_CNT        --Книга покупок: Кол-во контрагентов
,NDS_INCREASE_DISCREP_COUNT*2 AS BUYBOOK_DK_GAP_CNT            --Книга покупок: Вид расхождения - РАЗРЫВ: Кол-во разрывов
,NDS_INCREASE_DISCREP_COUNT   AS BUYBOOK_DK_GAP_CAGNT_CNT    --Книга покупок: Вид расхождения - РАЗРЫВ: Кол-во контрагентов
,NDS_INCREASE_DISCREP_COUNT*4 AS BUYBOOK_DK_GAP_AMNT        --Книга покупок: Вид расхождения - РАЗРЫВ: Сумма разрывов, руб.
,NDS_INCREASE_DISCREP_COUNT   AS BUYBOOK_DK_OTHR_CNT        --Книга покупок: Вид расхождения - ДРУГИЕ: Кол-во расхождений [Количество расхождений вида «Неточное сопоставление» и «Проверка НДС» с контрагентами-продавцами]
,NDS_INCREASE_DISCREP_COUNT*2 AS BUYBOOK_DK_OTHR_CAGNT_CNT    --Книга покупок: Вид расхождения - ДРУГИЕ: Кол-во контрагентов
,NDS_INCREASE_DISCREP_COUNT*5 AS BUYBOOK_DK_OTHR_AMNT        --Книга покупок: Вид расхождения - ДРУГИЕ: Сумма расхождений, руб.
--
from 
seod_declaration sed 
inner join v$sono soun on soun.s_code = sed.sono_code
inner join v$ssrf ssrf on ssrf.s_code = substr(sed.sono_code,1,2)
inner join dict_tax_period dtp on dtp.code = sed.tax_period
left join V$ASK_DECLANDJRNL sd on SD.PERIOD = SED.TAX_PERIOD and SD.OTCHETGOD = SED.FISCAL_YEAR and SD.INNNP = SED.INN and SD.NOMKORR = SED.CORRECTION_NUMBER  
left join SOV_DECLARATION_INFO d on D.DECLARATION_VERSION_ID = SD.ZIP
left join (select ksotn.IdDekl as decl_id, count(1) as KSCount from v$askkontrsoontosh ksotn where ksotn.Vypoln = 0 group by ksotn.IdDekl )  KSAggr on KSAggr.decl_id = sd.ID
left join seod_knp knp on knp.declaration_reg_num = sed.decl_reg_num
left join DICT_SUR sur on sur.code = nvl(d.Sur_Code, 0)
left join
(
  select
    ZIP as Declaration_Version_Id,
    sum(case TipFajla when '0' then SumNDSPok when '2' then SumNDSPokDL else 0 end) as TOTAL_NDS_AMNT_BUYBOOK,
    sum(case TipFajla when '1' then StProd18 when '3' then StProd18DL else 0 end) as TOTAL_COST_SELL_18,
    sum(case TipFajla when '1' then StProd10 when '3' then StProd10DL else 0 end) as TOTAL_COST_SELL_10,
    sum(case TipFajla when '1' then StProd0 when '3' then StProd0DL else 0 end) as TOTAL_COST_SELL_0,
    sum(case TipFajla when '1' then SumNDSProd18 when '3' then SumNDS18DL else 0 end) as TOTAL_TAX_AMNT_18,
    sum(case TipFajla when '1' then SumNDSProd10 when '3' then SumNDS10DL else 0 end) as TOTAL_TAX_AMNT_10,
    sum(case TipFajla when '1' then StProdOsv when '3' then StProdOsvDL else 0 end) as TOTAL_COST_SELL_TAXFREE
  from v$asksvodzap
  group by ZIP
) dsd on dsd.declaration_version_id = d.declaration_version_id;
