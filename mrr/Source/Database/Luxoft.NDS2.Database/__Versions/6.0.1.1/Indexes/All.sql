﻿--selection
create index NDS2_MRR_USER.idx_sel_reqid on NDS2_MRR_USER.selection(REQUEST_ID);

--selection_declaration
create index NDS2_MRR_USER.idx_seldecl_reqid on NDS2_MRR_USER.selection_declaration(REQUEST_ID);
create index NDS2_MRR_USER.idx_seldecl_declid on NDS2_MRR_USER.selection_declaration(DECLARATION_ID);
create index NDS2_MRR_USER.idx_seldecl_reqdecl on NDS2_MRR_USER.selection_declaration(REQUEST_ID, DECLARATION_ID);
create index NDS2_MRR_USER.idx_seldecl_declinproc on NDS2_MRR_USER.selection_declaration(DECLARATION_ID, IS_IN_PROCESS);
create index NDS2_MRR_USER.idx_seldecl_inproc on NDS2_MRR_USER.selection_declaration(IS_IN_PROCESS);

--selection_discrepancy
create index NDS2_MRR_USER.idx_seldis_reqid on NDS2_MRR_USER.selection_discrepancy(REQUEST_ID);
create index NDS2_MRR_USER.idx_seldis_disid on NDS2_MRR_USER.selection_discrepancy(DISCREPANCY_ID);
create index NDS2_MRR_USER.idx_seldis_reqdis on NDS2_MRR_USER.selection_discrepancy(REQUEST_ID, DISCREPANCY_ID);
create index NDS2_MRR_USER.idx_seldis_disidinproc on NDS2_MRR_USER.selection_discrepancy(DISCREPANCY_ID, IS_IN_PROCESS);
create index NDS2_MRR_USER.idx_seldis_inproc on NDS2_MRR_USER.selection_discrepancy(IS_IN_PROCESS);

--seod_declaration
create index NDS2_MRR_USER.idx_seoddecl_nds2id on NDS2_MRR_USER.seod_declaration(nds2_id);
create index NDS2_MRR_USER.idx_seoddecl_nds2idcornum on NDS2_MRR_USER.seod_declaration(nds2_id, correction_number);

--sov_declaration_info
create index NDS2_MRR_USER.idx_sovdeclinfo_id on NDS2_MRR_USER.sov_declaration_info(id);
create index NDS2_MRR_USER.idx_sovdeclinfo_askdeclid on NDS2_MRR_USER.sov_declaration_info(ASK_DECL_ID);
create index NDS2_MRR_USER.idx_sovdeclinfo_declverid on NDS2_MRR_USER.sov_declaration_info(DECLARATION_VERSION_ID);

--sov_invoice
create index NDS2_MRR_USER.idx_sovinvoice_rid on NDS2_MRR_USER.sov_invoice(request_id);
create  index NDS2_MRR_USER.IDX$$_01DC0001 on NDS2_MRR_USER.SOV_INVOICE("DECLARATION_VERSION_ID","CHAPTER","ACTUAL_ROW_KEY");

create  index NDS2_MRR_USER.IDX$$_01DC0003 on NDS2_MRR_USER.V_EGRN_UL("KPP","INN");
create  index NDS2_MRR_USER.IDX$$_01DC0002 on NDS2_MRR_USER.V$ASK_DECLANDJRNL("ZIP");
create index nds2_mrr_user.idx_decl_hist_id on nds2_mrr_user.MV$Declaration_history(id);
create index nds2_mrr_user.idx_decl_hist_vid on nds2_mrr_user.MV$Declaration_history(declaration_version_id);
