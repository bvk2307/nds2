﻿create or replace function NDS2_MRR_USER.UTL_ADD_WORK_DAYS(p_date date, p_days_count number) return date
as
  cnt number := p_days_count;
  dt date := p_date;
  v_exist number;
begin
  while (cnt > 0) loop
    dt := dt + 1;
    cnt := cnt - 1;
    select count(1) into v_exist from redday where red_date = dt;
    while (v_exist > 0) loop
      dt := dt + 1;
      select count(1) into v_exist from redday where red_date = dt;
    end loop;
  end loop;
  return dt;
end;
/
