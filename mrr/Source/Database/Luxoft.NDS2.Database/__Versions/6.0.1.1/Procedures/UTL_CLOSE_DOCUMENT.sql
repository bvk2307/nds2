﻿create or replace procedure NDS2_MRR_USER.UTL_CLOSE_DOCUMENT(p_reg_num in number, p_date in date := sysdate)
as
begin
  update doc
  set status = 10, close_date = p_date
  where ref_entity_id = p_reg_num;

  insert into doc_status_history(doc_id, status, status_date)
  select doc.doc_id, 10, p_date
  from doc doc
  left join doc_status_history hist on hist.doc_id = doc.doc_id and hist.status = 10
  where doc.ref_entity_id = p_reg_num and hist.doc_id is null;

  commit;
end;
/