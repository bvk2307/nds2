﻿create or replace function NDS2_MRR_USER.UTL_GET_CONFIGURATION_NUMBER(p_key varchar2) return number
as
v_result number;
begin
  select nvl(value, default_value)
  into v_result
  from configuration
  where parameter = p_key;

  return v_result;
end;
/
