﻿create or replace force view NDS2_MRR_USER.V$REPORT_MONITOR_DECLARATION AS
SELECT
  r.TASK_ID
  ,r.ACTUAL_NUM
  ,r.FederalDistrict
  ,r.Region
  ,r.Inspection
  ,r.CORRECTION_NUMBER
  ,r.INN
  ,r.KPP
  ,r.NAME
  ,r.INSPECTOR
  ,r.r1totalCount
  ,r.r1totalSum
  ,r.r1lkError
  ,r.r1valuteCount
  ,r.r1valuteSum
  ,r.r1ndsCount
  ,r.r1ndsSum
  ,r.r1nsCount
  ,r.r1nsSum
  ,r.r1breakCount
  ,r.r1breakSum
  ,r.r2totalCount
  ,r.r2totalSum
  ,r.r2lkError
  ,r.r2valuteCount
  ,r.r2valuteSum
  ,r.r2ndsCount
  ,r.r2ndsSum
  ,r.r2nsCount
  ,r.r2nsSum
  ,r.r2breakCount
  ,r.r2breakSum
  ,r.r3totalCount
  ,r.r3totalSum
  ,r.r3valuteCount
  ,r.r3valuteSum
  ,r.r3ndsCount
  ,r.r3ndsSum
  ,r.r3nsCount
  ,r.r3nsSum
  ,r.r3breakCount
  ,r.r3breakSum
from NDS2_MRR_USER.REPORT_MONITOR_DECLARATION r
where r.ACTUAL_NUM = 1
order by r.FederalDistrict, r.Region, r.Inspection, r.Inspector, r.INN;