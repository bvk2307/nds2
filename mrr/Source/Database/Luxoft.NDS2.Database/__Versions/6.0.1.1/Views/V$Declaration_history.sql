﻿create or replace force view NDS2_MRR_USER.V$Declaration_history as
select
case when length(do.inspector_sid) > 1 then 1 else 0 end as Processed
,mdh.*
,do.inspector_name as INSPECTOR
,do.inspector_sid as INSPECTORSID
from
NDS2_MRR_USER.MV$Declaration_history mdh 
left join DECLARATION_OWNER do on do.declaration_id = mdh.id;
