﻿create or replace force view NDS2_MRR_USER.v$ssrf as
select
r.s_code,
r.s_name,
r.s_code || ' - ' || r.s_name as DESCRIPTION
from ext_ssrf r;
