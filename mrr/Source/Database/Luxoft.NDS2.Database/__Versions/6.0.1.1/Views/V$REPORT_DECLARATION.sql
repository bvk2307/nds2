﻿create or replace force view NDS2_MRR_USER.V$REPORT_DECLARATION AS
SELECT
    r.TASK_ID
    ,r.Name_Group as Name_Group
    ,r.Decl_Count_Total as Decl_Count_Total
    ,r.Decl_Count_Null as Decl_Count_Null
    ,r.Decl_Count_Compensation as Decl_Count_Compensation
    ,r.Decl_Count_Payment as Decl_Count_Payment
    ,r.Decl_Count_Not_Submit as Decl_Count_Not_Submit
    ,r.Decl_Count_Revised_Total as Decl_Count_Revised_Total 
    ,r.TaxBase_Sum_Compensation as TaxBase_Sum_Compensation
    ,r.TaxBase_Sum_Payment as TaxBase_Sum_Payment
    ,r.NdsCalculated_Sum_Compensation as NdsCalculated_Sum_Compensation
    ,r.NdsCalculated_Sum_Payment as NdsCalculated_Sum_Payment
    ,r.NdsDeduction_Sum_Compensation as NdsDeduction_Sum_Compensation
    ,r.NdsDeduction_Sum_Payment as NdsDeduction_Sum_Payment
    ,r.Nds_Sum_Compensation as Nds_Sum_Compensation
    ,r.Nds_Sum_Payment as Nds_Sum_Payment
from NDS2_MRR_USER.REPORT_DECLARATION r
where r.ACTUAL_NUM = 1
order by r.Name_Group;