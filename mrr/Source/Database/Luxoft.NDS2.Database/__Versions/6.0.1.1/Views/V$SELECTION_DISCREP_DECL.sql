﻿create or replace force view NDS2_MRR_USER.V$SELECTION_DISCREP_DECL
as
select
    sdi.id,
    sdi.declaration_version_id,
    sdi.correction_number,
    mc_decl.INNNP as inn,
    mc_decl.KPPNP as kpp,
    decode(mc_decl.NAIMORG, null, mc_decl.FAMILIYANP||' '||mc_decl.IMYANP||' '||mc_decl.OTCHETGOD,mc_decl.NAIMORG) as Tax_payer_name,
    mc_decl.PERIOD as tax_period,
    mc_decl.OTCHETGOD as FISCAL_YEAR,
    mc_decl.TYPE as DECL_TYPE_CODE,
    case substr(mc_decl.KPPNP, 5,2) when  '50' then 1 else 0 end as CATEGORY,
    DECODE(mc_decl.TYPE, 0, 'Декларация', 1, 'Журнал', '') as DECL_TYPE,
    soun.s_code as sono_code,
    soun.s_name as sono_name,
    soun.s_code||'-'||soun.s_name as sono_full_name,
    ssrf.s_code as region_code,
    ssrf.s_name as region_name,
    ssrf.s_code||'-'||ssrf.s_name as region_full_name,
    case when knp.completion_date is null then 'Открыта КНП' else 'Закрыта КНП' end as STATUS,
    case
          when mc_decl.SUMPU173_5 > 0 then 'К уплате'
          when mc_decl.SUMPU173_1 > 0 then 'К уплате'
          when mc_decl.SUMPU173_1 < 0 then 'К возмещению'
          else 'Не определено'
    end as DECL_SIGN,
    case
          when mc_decl.SUMPU173_5 > 0 then mc_decl.SUMPU173_5
          when mc_decl.SUMPU173_1 > 0 then mc_decl.SUMPU173_1
          when mc_decl.SUMPU173_1 < 0 then mc_decl.SUMPU173_1
          else null end as COMPENSATION_AMNT,
    case
          when mc_decl.SUMPU173_5 > 0 then REPLACE(TO_CHAR(mc_decl.SUMPU173_5, 'SFM999999999G999D009'),',',' ')
          when mc_decl.SUMPU173_1 > 0 then REPLACE(TO_CHAR(mc_decl.SUMPU173_1, 'SFM999999999G999D009'),',',' ')
          when mc_decl.SUMPU173_1 < 0 then REPLACE(TO_CHAR(mc_decl.SUMPU173_1, 'SFM999999999G999D009'),',',' ')
          else null end as COMPENSATION_AMNT_SIGN,
    decode((row_number() over (partition by sdi.id order by sd.CORRECTION_NUMBER desc)),1,1,0) as rank_num
    from 
    sov_declaration_info sdi
    inner join V$ASK_DECLANDJRNL mc_decl on mc_decl.ZIP = sdi.declaration_version_id
    inner join v$sono soun on soun.s_code = mc_decl.KODNO
    inner join v$ssrf ssrf on ssrf.s_code = substr(mc_decl.KODNO, 1,2)
    left join seod_declaration sd on sd.nds2_id = sdi.id and sd.correction_number = sdi.correction_number
    left join seod_knp knp on knp.declaration_reg_num = sd.decl_reg_num;
