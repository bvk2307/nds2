﻿create or replace force view NDS2_MRR_USER.v$doc as
select 
  decode(t.Seod_Accepted, 1, TO_CHAR(t.EXTERNAL_DOC_NUM), '*'||TO_CHAR(t.DOC_ID)) as NUM,
  decode(t.Seod_Accepted, 1, t.SEOD_ACCEPT_DATE, NULL) as DT,
  t.*
from DOC t;
