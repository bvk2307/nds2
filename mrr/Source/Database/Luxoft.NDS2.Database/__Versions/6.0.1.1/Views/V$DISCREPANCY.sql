﻿create or replace force view NDS2_MRR_USER.v$discrepancy as
select
  dis.id,
  dis.sov_id as SovId,
  dis.id as DiscrepancyId,/*ММ - избыточно, убрать после сборки*/
  dis.sov_id						 as DiscrepancySovId,
  dis.CREATE_DATE as FoundDate,
  dis.invoice_chapter,
  dis.INVOICE_RK as invoice_id,
  dis.invoice_contractor_chapter,
  dis.INVOICE_CONTRACTOR_RK as contractor_invoice_id,
  d1.id as declaration_id,
  d1.inn as TaxPayerInn,
  d1.kpp as TaxPayerKpp,
  d1.name as TaxPayerName,
  --ТНО
  d1.soun_code as TaxPayerSounCode,
  soun1.s_name as TaxPayerSoun,
  --Регион
  d1.region_code as TaxPayerRegionCode,
  ssrf1.s_name as TaxPayerRegion,
  --Период
  d1.tax_period as TaxPayerPeriodCode,
  d1.FISCAL_YEAR as TaxPayerYearCode,
  decode(d1.tax_period, '21', '1 кв.', '22', '2 кв.', '23', '3 кв.', '24', '4 кв.', '') || d1.FISCAL_YEAR || ' г.' as TaxPayerPeriod,
  d1.correction_number as CorrectionNumber,
  'Раздел ' || dis.Invoice_Chapter as TaxPayerSource,
  --Признак
  d1.decl_sign as TaxPayerDeclSign,
  --Категория НП
  d1.Category as TaxPayerCategoryCode,
  decode(d1.Category, 0, '', 1, 'Крупнейший', 'Неизвестно') TaxPayerCategory,
  --###Контрагент###
  d2.inn as ContractorInn,
  d2.kpp as ContractorKpp,
  d2.name as ContractorName,
  --ТНО
  d2.soun_code as ContractorSounCode,
  soun2.s_name as ContractorSoun,
  --Регион
  d2.region_code as ContractorRegionCode,
  ssrf1.s_name as ContractorRegion,
  --Период
  d2.tax_period as ContractorPeriodCode,
  d2.FISCAL_YEAR as ContractorYearCode,
  decode(d2.tax_period, '21', '1 кв. 2014', '22', '2 кв. 2014', '23', '3 кв. 2014', '24', '4 кв. 2014', 'Неизвестно') as ContractorPeriod,
  'Раздел ' || dis.Invoice_Contractor_Chapter as ContractorSource,
  d2.correction_number as ContractorCorrectionNumber,
  --Признак
  d2.decl_sign as ContractorDeclSign,
  --Категория НП
  d2.Category as ContractorCategoryCode,
  decode(d2.Category, 0, '', 1, 'Крупнейший', 'Неизвестно') ContractorCategory,
  dis.type as TypeCode,
  dscrType.s_name as Type,
  dis.Amount,
  dis.status as StatusCode,
  dStatus.name as Status,
  --документ
  dc.DOC_ID as CLAIM_ID,
  null as sync_date,
  dcs.description as Claim_status,
  dc.doc_type as Doc_Type,
  dc.Tax_Payer_Send_Date as SEND_DATE,
  dc.close_date as CLOSE_DATE,
  -------------------
  d1.INSPECTOR as INSPECTOR,
  d1.DECL_DATE as DECL_DATE,
  dis.deal_amnt as Deal_Amount,
  dis.AMOUNT_PVP as PVP_Amount,
  dis.COURSE,
  dis.COURSE_COST as CourseCost,
  --dis.SUR_CODE as DiscrepancySUR,
  2 as DiscrepancySUR,
  dStage.STAGE_CODE                 as STAGE_CODE,
  dStage.STAGE_NAME                 as STAGE_NAME,
  dStage.STAGE_STATUS_CODE          as STAGE_STATUS_CODE,
  dStage.STAGE_STATUS_NAME          as STAGE_STATUS_NAME,
  dis.User_Comment as UserComment,
  dis.SIDE_PRIMARY_PROCESSING,
  dis.RULE_GROUP, 
  --непонятно
  sysdate as CalculationDate
from v$discrepancy_union dis
inner join dict_discrepancy_status dStatus on dStatus.id = dis.status
inner join dict_discrepancy_type dscrType on dscrType.s_code = dis.type
left join v$discrepancy_stage dStage on dStage.Id = dis.id
--лицо проверяемой декларации
inner join V$Declaration d1 on d1.id = dis.decl_id
left join v$sono soun1 on soun1.s_code = d1.soun_code
left join v$ssrf ssrf1 on ssrf1.s_code = d1.region_code
--контрагент
left join V$Declaration d2 on d2.id = dis.decl_contractor_id
left join v$sono soun2 on soun2.s_code = d2.soun_code
left join v$ssrf ssrf2 on ssrf2.s_code = d2.region_code
--документ
left join doc_invoice dci
    inner join doc dc on dc.doc_id = dci.doc_id
    inner join doc_status dcs on dcs.id = dc.status
  on dci.invoice_row_key = dis.invoice_rk;