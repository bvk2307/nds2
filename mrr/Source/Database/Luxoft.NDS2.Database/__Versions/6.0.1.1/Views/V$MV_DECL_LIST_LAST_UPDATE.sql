﻿create or replace force view NDS2_MRR_USER.V$MV_DECL_LIST_LAST_UPDATE
as
select max(ldate) as data_date from
(
select max(insert_date) as ldate from seod_declaration
union
select max(d.DATAFAJLA) as ldate from v$askzipfajl d
) T;
