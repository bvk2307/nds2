﻿create or replace force view NDS2_MRR_USER.v$sono as
select
s.s_code,
s.s_name,
n.type as type,
n.s_parent_code as s_parent_code,
n.employers_count as employers_count,
n.region_code as region_code
from ext_sono s 
left outer join sono_ext n on n.s_code = s.s_code;
