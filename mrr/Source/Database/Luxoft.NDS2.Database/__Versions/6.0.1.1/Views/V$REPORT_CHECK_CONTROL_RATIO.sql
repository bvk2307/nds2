﻿create or replace force view NDS2_MRR_USER.V$REPORT_CHECK_CONTROL_RATIO AS
SELECT
    r.TASK_ID
    ,r.Num as Num
    ,r.description as Description
    ,r.countdiscrepancy as countdiscrepancy
from NDS2_MRR_USER.REPORT_CHECK_CONTROL_RATIO r
where r.ACTUAL_NUM = 1
order by r.AGG_ROW desc, r.Num;