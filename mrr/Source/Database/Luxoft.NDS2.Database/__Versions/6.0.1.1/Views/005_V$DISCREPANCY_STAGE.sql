﻿create or replace force view NDS2_MRR_USER.V$DISCREPANCY_STAGE AS
select
  dis_ref.discrepancy_id as ID,
  dis_ref.request_id as RQ_ID,
  dis.stage     as STAGE_CODE,
  dis_ref.is_in_process as IS_CHECKED,
  disstage.name as STAGE_NAME,
  case dis.stage
    when 1 then docStage1.status
    when 2 then docStage2.status
    when 3 then docStage3.status
    when 4 then docStage4.status
    when 5 then docStage5.status
    when 6 then null
    else null
  end as STAGE_STATUS_CODE,
  case dis.stage
    when 1 then replace(
                      replace(
                         replace(docStage1.description, 'Черновик', 'В работе'),
                      'На согласовании', 'В работе'),
                  'На доработке', 'В работе')
    when 2 then docstatStage2.description
    when 3 then docstatStage3.description
    when 4 then docstatStage4.description
    when 5 then docstatStage5.description
    when 6 then null
    else null
  end as STAGE_STATUS_NAME
from SELECTION_DISCREPANCY dis_ref
    inner join SOV_DISCREPANCY dis on dis.id = dis_ref.discrepancy_id
    inner join DICT_DISCREPANCY_STAGE disstage on disstage.id = NVL(dis.stage, 1)
    left join (
      select base.dis_id as dis_id, selstat.id as status, selstat.name as DESCRIPTION
      from(
        select dis_ref.discrepancy_id as dis_id, MAX(
                case dis_ref.is_in_process
                  when 1 then selstat.rank
                  else 50
                end) as max_rank
        from selection sel
               inner join SELECTION_DISCREPANCY dis_ref on sel.Request_Id = dis_ref.request_id
               inner join DICT_SELECTION_STATUS selstat on selstat.id = sel.status
		where selstat.id not in (5, 7)	-- В случае, если выборка удалена или отклонена, расхождение считается не включённым в данную выборку
        group by dis_ref.discrepancy_id
      ) base
        inner join DICT_SELECTION_STATUS selstat on selstat.rank = base.MAX_RANK
    ) docStage1 on docStage1.dis_id = dis.id
    left join DOC_INVOICE docinv on docinv.invoice_row_key = dis.invoice_rk
         left join DOC docStage2 on (docStage2.doc_id = docinv.doc_id) and (docStage2.stage = 2)
         left join DOC_STATUS docstatStage2 on (docstatStage2.id = docStage2.status) and (docstatStage2.doc_type = docStage2.doc_type)
         left join DOC docStage3 on (docStage3.doc_id = docinv.doc_id) and (docStage3.stage = 3)
         left join DOC_STATUS docstatStage3 on (docstatStage3.id = docStage3.status) and (docstatStage3.doc_type = docStage3.doc_type)
         left join DOC docStage4 on (docStage4.doc_id = docinv.doc_id) and (docStage4.stage = 4)
         left join DOC_STATUS docstatStage4 on (docstatStage4.id = docStage4.status) and (docstatStage4.doc_type = docStage4.doc_type)
         left join DOC docStage5 on (docStage5.doc_id = docinv.doc_id) and (docStage5.stage = 5)
         left join DOC_STATUS docstatStage5 on (docstatStage5.id = docStage5.status) and (docstatStage5.doc_type = docStage5.doc_type);
