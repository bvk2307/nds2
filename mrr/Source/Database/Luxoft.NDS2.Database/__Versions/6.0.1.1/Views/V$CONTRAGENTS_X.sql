﻿create or replace force view nds2_mrr_user.v$contragents_list as
select
  decl.DECLARATION_VERSION_ID as DECLARATION_VERSION_ID,
  decl.DECL_TYPE as DOC_TYPE,
  decl.DECL_TYPE_CODE as DOC_TYPE_CODE,
  cagnt.contractor_inn as CONTRACTOR_INN,
  vul.KPP as CONTRACTOR_KPP,
  case when vul.KPP is not null then vul.name_full
       else vip.last_name||' '||vip.first_name||' '||vip.patronymic
  end as CONTRACTOR_NAME,
  sur.sign_code as SUR_CODE,
  bb.BUYBOOK_DK_GAP_CNT,
  bb.BUYBOOK_DK_OTHR_CNT,
  bb.BUYBOOK_DK_GAP_AMNT,
  bb.BUYBOOK_DK_OTHR_AMNT,
  cagnt.AMOUNT_TOTAL,
  cagnt.AMOUNT_NDS,
  cagnt.NDS_WEIGHT,
  cagnt.OPERATIONS_COUNT
from NDS2_MRR_USER.V$DECLARATIONINSPECT decl
join NDS2_MRR_USER.SOV_CONTRAGENTS cagnt on decl.declaration_version_id = cagnt.declaration_version_id
left join NDS2_MRR_USER.V_EGRN_UL vul on vul.inn = cagnt.contractor_inn
left join NDS2_MRR_USER.V_EGRN_IP vip on vip.innfl = cagnt.contractor_inn
left join NDS2_MRR_USER.EXT_SUR sur on sur.inn = cagnt.contractor_inn and sur.fiscal_year = decl.FISCAL_YEAR and sur.fiscal_period = decl.TAX_PERIOD
left join (
select
  inv.seller_inn as INN,
  inv.declaration_version_id,
  SUM(case ds.type when 1 then 1 else 0 end) as BUYBOOK_DK_GAP_CNT,
  SUM(case when ds.type=1 then 0 when ds.type IS NULL then 0 else 1 end) as BUYBOOK_DK_OTHR_CNT,
  SUM(case ds.type when 1 then ds.amnt else 0 end) as BUYBOOK_DK_GAP_AMNT,
  SUM(case when ds.type=1 then 0 when ds.type IS NULL then 0 else ds.amnt end) as BUYBOOK_DK_OTHR_AMNT
from NDS2_MRR_USER.STAGE_INVOICE inv
left join NDS2_MRR_USER.SOV_DISCREPANCY ds on ds.invoice_rk = inv.row_key
where inv.chapter = 8
group by inv.seller_inn, inv.chapter, inv.declaration_version_id
) bb on bb.inn = cagnt.contractor_inn  and bb.declaration_version_id = decl.declaration_version_id
where nvl(cagnt.request_id, -1) in (select max(nvl(request_id, -1)) from NDS2_MRR_USER.SOV_CONTRAGENTS where decl.declaration_version_id = declaration_version_id);

create or replace force view nds2_mrr_user.v$contragent_params as
select
  decl.SUR_CODE
  ,decl.DECL_TYPE as DOC_TYPE
  ,decl.DECL_TYPE_CODE as DOC_TYPE_CODE
  ,inv.buyer_inn as INN
  ,decl.inn as CONTRACTOR_INN
  ,decl.kpp as CONTRACTOR_KPP
  ,decl.name as CONTRACTOR_NAME
  ,inv.invoice_num
  ,inv.invoice_date
  ,inv.price_buy_amount as AMOUNT_TOTAL
  ,inv.price_buy_nds_amount as AMOUNT_NDS
  ,cprm.nds_weight as NDS_WEIGHT
  ,dis.type as DIS_TYPE
  ,dis.id   as DIS_ID
  ,case when dis.type = 1 then dis.amnt else 0 end as DIS_GAP_AMOUNT
  ,case when dis.type = 2 then dis.amnt else 0 end as DIS_MISMATCH_AMOUNT
  ,case when dis.type = 4 then dis.amnt else 0 end as DIS_NDS_AMOUNT
from nds2_mrr_user.stage_invoice inv
join nds2_mrr_user.SOV_DISCREPANCY dis on dis.invoice_rk = inv.row_key
left join nds2_mrr_user.v$declaration decl on decl.INN = inv.seller_inn
left join nds2_mrr_user.SOV_CONTRAGENT_PARAMS cprm on cprm.row_key = inv.row_key
;

create or replace force view NDS2_MRR_USER.V$SOV_CONTR_DATA as
  select
    sd.zip as ZIP,
    SOV.CH8_NDS as CHAPTER_SUM,
    sd.INNNP as INN,
    sd.OTCHETGOD as FISCAL_YEAR,
    sd.PERIOD as PERIOD,
    decode((row_number() over (partition by sd.PERIOD, sd.OTCHETGOD, sd.INNNP order by sd.NOMKORR desc)), 1,1,0) as IS_ACTIVE
   from SOV_DECLARATION_INFO SOV
    inner join v$askdekl sd on sd.zip = SOV.DECLARATION_VERSION_ID;
