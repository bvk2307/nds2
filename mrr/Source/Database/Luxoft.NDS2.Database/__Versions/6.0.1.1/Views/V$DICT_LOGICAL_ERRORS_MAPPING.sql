﻿create or replace force view NDS2_MRR_USER.v$dict_logical_errors_mapping as
select m.ERROR_CODE, m.CHAPTER, m.NAME_CHECK, m.DESCREPTION, a.line_numbers
from
(
  select t.error_code
         ,min(t.chapter) as chapter
         ,min(t.name_check) as name_check
         ,min(t.descreption) as descreption
  from LOGICAL_ERRORS_MAPPING t
  group by t.error_code
) m
left join
(
  select
    error_code,
    substr(max(sys_connect_by_path(line_number, ', ' )), 3) line_numbers
  from
  (
    select
       lem.error_code
       ,lem.line_number
       ,row_number() over (partition by lem.error_code order by lem.line_number) rn
      from LOGICAL_ERRORS_MAPPING lem
      group by lem.error_code, lem.line_number
      order by lem.error_code, lem.line_number
  )
  start with rn = 1
  connect by prior rn = rn-1
  and prior error_code = error_code
  group by error_code
  order by error_code
) a on a.error_code = m.error_code;