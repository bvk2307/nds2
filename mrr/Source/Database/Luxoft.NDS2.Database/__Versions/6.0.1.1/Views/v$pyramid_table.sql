﻿create or replace force view NDS2_MRR_USER.v$pyramid_table as
select
      request_id,
    contractor_name || ' (' || contractor_inn || '/' || contractor_kpp || ')' as SellerName,
      contractor_name as Name,
      contractor_inn as Inn,
      contractor_kpp as Kpp,
      case
           when decl_provided = 'Y'
           then 'Предоставил'
           else 'Не предоставил'
      end as DECL_PROVIDED,
    ROUND(SYR, 0) as SYR,
    ROUND(DECL_NDS_8, 2) as DECL_NDS_8,
    ROUND(DECODE(SUM_NDS,0,0, SUM_NDS / (SUM(SUM_NDS) OVER(PARTITION BY REQUEST_ID))) * 100, 2) as DECL_NDS_8_WT,
    ROUND(DECL_NDS_9, 2) as DECL_NDS_9,
    ROUND((DECL_NDS_9 - DECL_NDS_8), 2) as DECL_NDS_9_FOR_PAY,
    ROUND(SUM_NDS, 2) as SUM_NDS,
    ROUND(SUM_RUB, 2) as SUM_RUB,
    ROUND(SUM_RUB_MATCHED, 2) as SUM_RUB_MATCHED,
    ROUND(SUM_RUB, 2) as SUM_RUB_MATCHED_2,
    ROUND(DISCR_TOTAL_SUM, 2) as DISCR_TOTAL_SUM,
    ROUND(DISCR_TOTAL_DEAL_COUNT, 2) as DISCR_TOTAL_DEAL_COUNT,
    ROUND(DISCR_TOTAL_DEAL_SUM, 2) as DISCR_TOTAL_DEAL_SUM,
    ROUND(DISCR_GAP_TOTAL_COUNT, 2) as DISCR_GAP_TOTAL_COUNT,
    ROUND(DISCR_GAP_TOTAL_SUM, 2) as DISCR_GAP_TOTAL_SUM,
    ROUND(DISCR_GAP_DEAL_COUNT, 2) as DISCR_GAP_DEAL_COUNT,
    ROUND(DISCR_GAP_DEAL_SUM, 2) as DISCR_GAP_DEAL_SUM,
    ROUND(DISCR_WEAK_TOTAL_COUNT, 2) as DISCR_WEAK_TOTAL_COUNT,
    ROUND(DISCR_WEAK_TOTAL_SUM, 2) as DISCR_WEAK_TOTAL_SUM,
    ROUND(DISCR_WEAK_DEAL_COUNT, 2) as DISCR_WEAK_DEAL_COUNT,
    ROUND(DISCR_WEAK_DEAL_SUM, 2) as DISCR_WEAK_DEAL_SUM,
    ROUND(DISCR_NDS_TOTAL_COUNT, 2) as DISCR_NDS_TOTAL_COUNT,
    ROUND(DISCR_NDS_TOTAL_SUM, 2) as DISCR_NDS_TOTAL_SUM,
    ROUND(DISCR_NDS_DEAL_COUNT, 2) as DISCR_NDS_DEAL_COUNT,
    ROUND(DISCR_NDS_DEAL_SUM, 2) as DISCR_NDS_DEAL_SUM
    from SOV_PYRAMID_INFO spi;
