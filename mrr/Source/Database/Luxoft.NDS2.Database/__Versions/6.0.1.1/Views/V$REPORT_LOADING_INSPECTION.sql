﻿create or replace force view NDS2_MRR_USER.V$REPORT_LOADING_INSPECTION AS
SELECT
    r.TASK_ID
    ,r.FederalDistrict as FederalDistrict
    ,r.Region as Region
    ,r.Inspection as Inspection
    ,r.InspectorCount as InspectorCount
    ,r.GeneralCount as GeneralCount
    ,r.GeneralSum as GeneralSum
    ,r.SentToWorkGeneralCount as SentToWorkGeneralCount
    ,r.SentToWorkGeneralSum as SentToWorkGeneralSum
    ,r.SentToWorkPercent as SentToWorkPercent
    ,r.SentToWorkPercentSum as SentToWorkPercentSum
    ,r.RepairedGeneralCount as RepairedGeneralCount
    ,r.RepairedGeneralSum as RepairedGeneralSum
    ,r.RepairedPercent as RepairedPercent
    ,r.RepairedPercentSum as RepairedPercentSum
    ,r.UnrepairedGeneralCount as UnrepairedGeneralCount
    ,r.UnrepairedGeneralSum as UnrepairedGeneralSum
from NDS2_MRR_USER.REPORT_LOADING_INSPECTION r
where r.ACTUAL_NUM = 1
order by r.FederalDistrict, r.Region, r.Inspection;