﻿create or replace force view NDS2_MRR_USER.V$METODOLOG_REGION as
with tRegion as
(
  select 1 as Context_Id, ssrf.s_code as Code
  from v$ssrf ssrf
  left join metodolog_region mr on mr.code = ssrf.s_code and mr.context_id = 1
  where mr.id is null
  union all
  select 2 as Context_Id, ssrf.s_code as Code
  from v$ssrf ssrf
  left join metodolog_region mr on mr.code = ssrf.s_code and mr.context_id = 2
  where mr.id is null and ssrf.s_code <> '00'
)
select reg.*, ssrf.s_Name as Name, ssrf.s_code||' - '||ssrf.s_name as regionfullname
from
(
  select mr.Id, mr.Context_Id, mr.Code, 0 as Is_New
  from metodolog_region mr
  union all
  select row_number() over (order by tReg.context_id, tReg.code) +
    (
      select case when max(mr.id) is null then 0 else max(mr.id) end
      from all_sequences seq
      left join metodolog_region mr on mr.id < seq.min_value
      where seq.sequence_name = 'SEQ_METODOLOG_REGION'
        and seq.sequence_owner = (select user from dual)
    ) as Id,
    tReg.Context_Id,
    tReg.Code,
    1 as Is_New
  from tRegion tReg
) reg
inner join v$ssrf ssrf on ssrf.s_code = reg.code
union all
select 1000000 as ID, 1 as Context_Id, cast('00' as VARCHAR2(2)) as Code, 1 as Is_New, cast('Вся страна' as VARCHAR2(50)) as name, 
      cast('Вся страна'as VARCHAR2(50)) as regionfullname
from dual;

create or replace force view NDS2_MRR_USER.v$cfg_region as
select
	cr.ID as cfg_id,
	mr.id as reg_id,
	mr.code as region_code,
	mr.name as region_name,
	mr.is_new as region_is_new,
	mr.context_id as region_context_id,
	cr.cfg_param_id as param_id,
	cr.value as param_value
from v$metodolog_region mr
left join CFG_REGION cr on mr.id = cr.id
where mr.context_id = 1
order by mr.code, mr.id;