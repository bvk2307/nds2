﻿create or replace force view NDS2_MRR_USER.v$declarationinspect as
select 
mdi.*
,do.inspector_name as INSPECTOR
,do.inspector_sid as INSPECTOR_SID
from MV$declarationinspect mdi 
left join nds2_mrr_user.DECLARATION_OWNER do on do.declaration_id = mdi.id;
