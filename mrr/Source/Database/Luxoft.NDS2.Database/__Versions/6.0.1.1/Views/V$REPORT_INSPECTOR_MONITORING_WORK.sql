﻿create or replace force view NDS2_MRR_USER.V$REPORT_INSPEC_MONITOR_WORK AS
SELECT
  r.ID
  ,r.TASK_ID
  ,r.ACTUAL_NUM
  ,r.FederalDistrict
  ,r.Region
  ,r.Inspection
  ,r.INSPECTOR
  ,r.NotEliminated_GeneralCount
  ,r.NotEliminated_GeneralSum
  ,r.NotElimin_CurrencyCount
  ,r.NotElimin_CurrencySum
  ,r.NotElimin_NDSCount
  ,r.NotElimin_NDSSum
  ,r.NotElimin_NotExactCount
  ,r.NotElimin_NotExactSum
  ,r.NotElimin_GapCount
  ,r.NotElimin_GapSum
  ,r.IdentNotElimin_GeneralCount
  ,r.IdentNotElimin_GeneralSum
  ,r.IdentNotElimin_CurrencyCount
  ,r.IdentNotElimin_CurrencySum
  ,r.IdentNotElimin_NDSCount
  ,r.IdentNotElimin_NDSSum
  ,r.IdentNotElimin_NotExactCount
  ,r.IdentNotElimin_NotExactSum
  ,r.IdentNotElimin_GapCount
  ,r.IdentNotElimin_GapSum
  ,r.IdentElimin_GeneralCount
  ,r.IdentElimin_GeneralSum
  ,r.IdentElimin_CurrencyCount
  ,r.IdentElimin_CurrencySum
  ,r.IdentElimin_NDSCount
  ,r.IdentElimin_NDSSum
  ,r.IdentElimin_NotExactCount
  ,r.IdentElimin_NotExactSum
  ,r.IdentElimin_GapCount
  ,r.IdentElimin_GapSum
from NDS2_MRR_USER.REPORT_INSPECTOR_MONITOR_WORK r
where r.ACTUAL_NUM = 1
order by r.Federaldistrict, r.region, r.Inspection, r.INSPECTOR;