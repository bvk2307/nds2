﻿create or replace force view NDS2_MRR_USER.v$doc_invoice as
SELECT 
    di.doc_id
    ,1 as row_rank
    ,(replace(replace(rtrim(TO_CHAR((case when chapter = 8 then PRICE_BUY_AMOUNT
        when chapter = 9 then (case when PRICE_SELL is not null then PRICE_SELL
                                    when PRICE_SELL_IN_CURR is not null then PRICE_SELL_IN_CURR else 0 end)
        when chapter = 10 then PRICE_TOTAL
        when chapter = 11 then PRICE_TOTAL
        when chapter = 12 then PRICE_TOTAL else 0 end), 'fm999999G999G990D009'), '.,'), ',', ' '),'.',',')/* || 
        (case when okv_code = 810 then ' руб' else '' end)*/) as CALC_PRICE_WITH_NDS
    ,(case when chapter = 8 then PRICE_NDS_TOTAL
        when chapter = 9 then nvl(PRICE_NDS_18, 0) + nvl(PRICE_NDS_10, 0)
        when chapter = 10 then PRICE_NDS_TOTAL
        when chapter = 11 then PRICE_NDS_TOTAL
        when chapter = 12 then PRICE_NDS_TOTAL else 0 end) as CALC_PRICE_NDS
    ,vd.breakSum as CALC_PVP_GAP
    ,vd.nsSum as CALC_PVP_NOT_EXACT
    ,vd.ndsSum as CALC_PVP_NDS
    ,vd.valuteSum as CALC_PVP_CURRENCY
    ,vw.*     
	,case when INSTR(nvl(vw.SELLER_INN, ''), ',', 1) > 0 then 'Сводный СФ' else vw.SELLER_INN end as SELLER_INN_RESOLVED
	,case when INSTR(nvl(vw.SELLER_KPP, ''), ',', 1) > 0 then '' else vw.SELLER_KPP end as SELLER_KPP_RESOLVED
FROM v$stage_invoice vw
inner join doc_invoice di on di.invoice_row_key = vw.ROW_KEY
LEFT OUTER JOIN
(
select
     distinct 
     INVOICE_ID
    ,sum(case when typecode=3 then amount else 0 end) over(partition by INVOICE_ID) as valuteSum
    ,sum(case when typecode=4 then amount else 0 end) over(partition by INVOICE_ID) as ndsSum
    ,sum(case when typecode=2 then amount else 0 end) over(partition by INVOICE_ID) as nsSum
    ,sum(case when typecode=1 then amount else 0 end) over(partition by INVOICE_ID) as breakSum
from V$DISCREPANCY 
) vd on vd.INVOICE_ID = vw.ROW_KEY
;
