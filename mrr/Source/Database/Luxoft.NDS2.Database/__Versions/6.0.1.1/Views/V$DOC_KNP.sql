﻿SET DEFINE OFF;
create or replace force view NDS2_MRR_USER.V$DOC_KNP as 
WITH mnk AS (
select
  SEOD_KNP_DECISION_TYPE.DESCRIPTION as NAME,
  SEOD_KNP_DECISION.DOC_NUM as NUM,
  SEOD_KNP_DECISION.DOC_DATE as DT,
  DECL.ID as DECL_ID,
  DECL.DECL_DATE as LEVEL1_DATE,
  DECL.CORRECTION_NUMBER as LEVEL1_NUM,
  SEOD_KNP_DECISION.DOC_DATE as LEVEL3_DATE,
  SEOD_KNP_DECISION.DOC_NUM as LEVEL3_NUM
from SEOD_KNP
  inner join SEOD_KNP_DECISION on SEOD_KNP_DECISION.KNP_ID = SEOD_KNP.KNP_ID
  inner join SEOD_KNP_DECISION_TYPE on SEOD_KNP_DECISION_TYPE.ID = SEOD_KNP_DECISION.TYPE_ID
  inner join SEOD_DECLARATION on SEOD_DECLARATION.DECL_REG_NUM = SEOD_KNP.DECLARATION_REG_NUM
  inner join v$declaration_history DECL on DECL.ID = SEOD_DECLARATION.NDS2_ID and DECL.CORRECTION_NUMBER = SEOD_DECLARATION.CORRECTION_NUMBER
)
select
  'Корректировка' as name,
  DECL.correction_number as num,
  DECL.decl_date as dt,
  case DECL.is_active when 0 then 'Не актуальная' else 'Актуальная' end as status,
  DECL.ID as DECL_ID,
  DECL.DECL_DATE as LEVEL1_DATE,
  DECL.CORRECTION_NUMBER as LEVEL1_NUM,
  TO_DATE ('01-JAN-1800', 'DD-MON-YYYY') as LEVEL2_DATE,
  '0' as LEVEL2_NUM,
  TO_DATE ('01-JAN-1800', 'DD-MON-YYYY') as LEVEL3_DATE,
  '0' as LEVEL3_NUM,
  null as ID,
  null as type_doc_knp,
  null as explain_can_open
from V$DECLARATION_HISTORY DECL
union
select
  '&nbsp;&nbsp;&nbsp;&nbsp;' ||
  DOC_TYPE.DESCRIPTION as name,
  DOC.NUM as num,
  DOC.DT as DT,
  DOC_STATUS.DESCRIPTION as STATUS,
  DECL.ID as DECL_ID,
  DECL.DECL_DATE as LEVEL1_DATE,
  DECL.CORRECTION_NUMBER as LEVEL1_NUM,
  nvl(DOC.DT, TO_DATE ('01-DEC-4712', 'DD-MON-YYYY'))  as LEVEL2_DATE,
  DOC.NUM as LEVEL2_NUM,
  TO_DATE ('01-JAN-1800', 'DD-MON-YYYY') as LEVEL3_DATE,
  '0' as LEVEL3_NUM,
  DOC.DOC_ID as ID,
  1 as type_doc_knp,
  null as explain_can_open
from V$DOC doc
     inner join seod_declaration sd on sd.decl_reg_num = doc.ref_entity_id
     inner join V$DECLARATION_HISTORY decl on sd.nds2_id = decl.ID and sd.correction_number = decl.CORRECTION_NUMBER
     inner join DOC_TYPE on DOC_TYPE.ID = DOC.Doc_Type
     left join DOC_STATUS on (DOC_STATUS.ID = DOC.STATUS) and (DOC_STATUS.DOC_TYPE = DOC.DOC_TYPE)
where DOC.DOC_TYPE <> 5
union
select
  '&nbsp;&nbsp;&nbsp;&nbsp;' ||
  DOC_TYPE.DESCRIPTION as name,
  DOC.NUM as num,
  DOC.DT as DT,
  DOC_STATUS.DESCRIPTION as STATUS,
  DECL.ID as DECL_ID,
  DECL.DECL_DATE as LEVEL1_DATE,
  DECL.CORRECTION_NUMBER as LEVEL1_NUM,
  nvl(DOC.DT, TO_DATE ('01-DEC-4712', 'DD-MON-YYYY'))  as LEVEL2_DATE,
  DOC.NUM as LEVEL2_NUM,
  TO_DATE ('01-JAN-1800', 'DD-MON-YYYY') as LEVEL3_DATE,
  '0' as LEVEL3_NUM,
  DOC.DOC_ID as ID,
  1 as type_doc_knp,
  null as explain_can_open
from V$DOC doc
  inner join seod_declaration sd on sd.decl_reg_num = doc.ref_entity_id
  inner join V$DECLARATION_HISTORY decl on sd.nds2_id = decl.ID and sd.correction_number = decl.CORRECTION_NUMBER
  inner join DOC_TYPE on DOC_TYPE.ID = DOC.Doc_Type
  inner join
  (
    select 
        d.doc_id
    from 
        doc d
        inner join control_ratio_doc crd on crd.doc_id = d.DOC_ID
        inner join v$askkontrsoontosh ks on ks.ID = crd.cr_id
    group by d.doc_id
          having sum(case when ks.Vypoln = 0 then 1 else 0 end) > 0
  ) T1 on T1.doc_id = doc.DOC_ID
    left join DOC_STATUS on (DOC_STATUS.ID = DOC.STATUS) and (DOC_STATUS.DOC_TYPE = DOC.DOC_TYPE)
where DOC.DOC_TYPE = 5
union	 
select
  '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' ||
  case
    when (REPL.RECEIVE_BY_TKS = 0) and (REPL.STATUS_ID = 1)
      then '<span style="color:Red; font-family:Segoe UI Symbol;">&#x2691;</span>'
    else '<span style="font-family:Segoe UI Symbol;">&nbsp;</span>'
  end || 
  nvl(dert.short_name, '') 
  as NAME,
  nvl(REPL.INCOMING_NUM, '') as NUM,
  REPL.INCOMING_DATE as DT,
  ders.NAME as STATUS,
  DECL.ID as DECL_ID,
  DECL.DECL_DATE as LEVEL1_DATE,
  DECL.CORRECTION_NUMBER as LEVEL1_NUM,
  nvl(DOC.DT, TO_DATE ('01-DEC-4712', 'DD-MON-YYYY')) as LEVEL2_DATE,
  DOC.NUM as LEVEL2_NUM,
  REPL.INCOMING_DATE as LEVEL3_DATE,
  REPL.INCOMING_NUM as LEVEL3_NUM,
  REPL.EXPLAIN_ID as ID,
  2 as type_doc_knp,
  (case when (REPL.Status_Id = 2 OR REPL.Status_Id = 3) then 1
       when (SELECT count(explain_id) FROM SEOD_EXPLAIN_REPLY se
             WHERE se.doc_id = REPL.DOC_ID AND se.explain_id <> REPL.EXPLAIN_ID
             AND se.incoming_date < REPL.INCOMING_DATE) = 0 then 1 
       when (SELECT count(explain_id) FROM SEOD_EXPLAIN_REPLY se
             WHERE se.doc_id = REPL.DOC_ID AND se.explain_id <> REPL.EXPLAIN_ID AND se.status_id = 1
                  AND se.incoming_date < REPL.INCOMING_DATE) = 0 then 1 
      else 0 end) as explain_can_open 
from SEOD_EXPLAIN_REPLY REPL
  inner join V$DOC doc on DOC.DOC_ID = REPL.DOC_ID
  inner join seod_declaration sd on sd.decl_reg_num = DOC.REF_ENTITY_ID
  inner join v$declaration_history DECL on DECL.ID = sd.nds2_id and sd.correction_number = decl.CORRECTION_NUMBER
  inner join DICT_EXPLAIN_REPLY_TYPE dert on dert.id = REPL.TYPE_ID
  inner join DICT_EXPLAIN_REPLY_STATUS ders on ders.id = REPL.status_id
union
select
  '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' ||
  NAME as NAME,
  NUM,
  DT,
  null as STATUS,
  DECL_ID,
  LEVEL1_DATE,
  LEVEL1_NUM,
  TO_DATE ('01-DEC-4712', 'DD-MON-YYYY') as LEVEL2_DATE,
  '100000000' as LEVEL2_NUM,
  LEVEL3_DATE,
  LEVEL3_NUM,
  null as ID,
  null as type_doc_knp,
  null as explain_can_open
from mnk
union
select
  '&nbsp;&nbsp;&nbsp;&nbsp;' ||
  'Прочие МНК' as NAME,
  null as NUM,
  null as DT,
  null as STATUS,
  DECL_ID,
  LEVEL1_DATE,
  LEVEL1_NUM,
  TO_DATE ('01-DEC-4712', 'DD-MON-YYYY') as LEVEL2_DATE,
  '100000000' as LEVEL2_NUM,
  TO_DATE ('01-JAN-1800', 'DD-MON-YYYY') as LEVEL3_DATE,
  '0' as LEVEL3_NUM,
  null as ID,
  null as type_doc_knp,
  null as explain_can_open
from mnk
group by DECL_ID, LEVEL1_DATE, LEVEL1_NUM
order by LEVEL1_DATE, LEVEL1_NUM, LEVEL2_DATE, LEVEL2_NUM, LEVEL3_DATE, LEVEL3_NUM
;
SET DEFINE ON;

