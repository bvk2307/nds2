﻿	create or replace force view NDS2_MRR_USER.V$SOV_PYRAMID_DATA 
	AS 
	select
	SOV.DECLARATION_VERSION_ID as ZIP,
	SOV.CH8_NDS as CH8,
	SOV.CH9_NDS as CH9,
	(SOV.DISCREP_CURRENCY_COUNT+SOV.GAP_DISCREP_COUNT+SOV.WEAK_DISCREP_COUNT+SOV.NDS_INCREASE_DISCREP_COUNT) as DISCR_TOTAL_CNT,
	(SOV.DISCREP_CURRENCY_AMNT+SOV.GAP_DISCREP_AMNT+SOV.WEAK_DISCREP_AMNT+SOV.NDS_INCREASE_DISCREP_AMNT) as DISCR_TOTAL_SUM,               
	SOV.GAP_DISCREP_COUNT as DISCR_GAP_CNT,
	SOV.WEAK_DISCREP_COUNT as DISCR_WEAK_CNT,
	SOV.NDS_INCREASE_DISCREP_COUNT as DISCR_NDS_CNT,                
	SOV.GAP_DISCREP_AMNT as DISCR_GAP_SUM,
	SOV.WEAK_DISCREP_AMNT as DISCR_WEAK_SUM,
	SOV.NDS_INCREASE_DISCREP_AMNT as DISCR_NDS_SUM,                
	sd.INNNP as INN,
	sd.KPPNP as KPP,
	sd.NAIMORG as NAME,
	sd.KODNO as KODNO,
	sd.OTCHETGOD as FISCAL_YEAR,
	sd.PERIOD as PERIOD,
	reg.s_name as REGION,
	fd.description as FEDERAL_DISTRICT,
	decode((row_number() over (partition by sd.PERIOD, sd.OTCHETGOD, sd.INNNP order by sd.NOMKORR desc)), 1,1,0) as IS_ACTIVE
	from SOV_DECLARATION_INFO SOV
	inner join V$ASK_DECLANDJRNL sd on sd.zip = SOV.DECLARATION_VERSION_ID
	left join v$ssrf reg on reg.s_code = SUBSTR(sd.KODNO,1,2)
	left join federal_district_region regToFed on reg.s_code=regToFed.Region_Code
	left join federal_district fd on fd.district_id=regToFed.District_Id;
	