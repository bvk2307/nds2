﻿create or replace force view NDS2_MRR_USER.v$invoice as
select
  i.REQUEST_ID,
  i.DECLARATION_VERSION_ID,
  i.CHAPTER,
  i.CREATE_DATE,
  i.RECEIVE_DATE,
  i.OPERATION_CODE,
  i.INVOICE_NUM,
  i.INVOICE_DATE,
  i.CHANGE_NUM,
  i.CHANGE_DATE,
  i.CORRECTION_NUM,
  i.CORRECTION_DATE,
  i.CHANGE_CORRECTION_NUM,
  i.CHANGE_CORRECTION_DATE,
  i.RECEIPT_DOC_NUM,
  i.RECEIPT_DOC_DATE,
  i.BUY_ACCEPT_DATE,
  i.BUYER_INN,
  i.BUYER_KPP,
  case 
    when buyer_jp.id is null 
    then (case when buyer_ip.id is null then '' else trim(nvl(buyer_ip.last_name,'')||' '||nvl(buyer_ip.first_name,'')||' '||nvl(buyer_ip.patronymic,'')) end) 
    else buyer_jp.name_full end 
  as BUYER_NAME,
  i.SELLER_INN,
  i.SELLER_KPP,
  case when INSTR(nvl(i.SELLER_INN, ''), ',', 1) > 0 then 'Сводный СФ' else i.SELLER_INN end as SELLER_INN_RESOLVED,
  case when INSTR(nvl(i.SELLER_KPP, ''), ',', 1) > 0 then '' else i.SELLER_KPP end as SELLER_KPP_RESOLVED,
  case 
    when seller_jp.id is null 
    then (case when seller_ip.id is null then '' else trim(nvl(seller_ip.last_name,'')||' '||nvl(seller_ip.first_name,'')||' '||nvl(seller_ip.patronymic,'')) end) 
    else seller_jp.name_full end
  as SELLER_NAME,
  i.SELLER_INVOICE_NUM,
  i.SELLER_INVOICE_DATE,
  i.BROKER_INN,
  i.BROKER_KPP,
  case 
    when broker_jp.id is null 
    then (case when broker_ip.id is null then '' else trim(nvl(broker_ip.last_name,'')||' '||nvl(broker_ip.first_name,'')||' '||nvl(broker_ip.patronymic,'')) end) 
    else broker_jp.name_full end
  as BROKER_NAME,
  i.DEAL_KIND_CODE,
  i.CUSTOMS_DECLARATION_NUM,
  i.OKV_CODE,
  i.PRICE_BUY_AMOUNT,
  i.PRICE_BUY_NDS_AMOUNT,
  i.PRICE_SELL,
  i.PRICE_SELL_IN_CURR,
  i.PRICE_SELL_18,
  i.PRICE_SELL_10,
  i.PRICE_SELL_0,
  i.PRICE_NDS_18,
  i.PRICE_NDS_10,
  i.PRICE_TAX_FREE,
  i.PRICE_TOTAL,
  i.PRICE_NDS_TOTAL,
  i.DIFF_CORRECT_DECREASE,
  i.DIFF_CORRECT_INCREASE,
  i.DIFF_CORRECT_NDS_DECREASE,
  i.DIFF_CORRECT_NDS_INCREASE,
  i.PRICE_NDS_BUYER,
  i.ROW_KEY,
  i.ACTUAL_ROW_KEY,
  i.COMPARE_ROW_KEY,
  i.COMPARE_ALGO_ID,
  i.FORMAT_ERRORS,
  i.LOGICAL_ERRORS,
  d.CORRECTION_NUMBER as DECL_CORRECTION_NUM,
  d.FULL_TAX_PERIOD,
  d.TAX_PERIOD,
  d.FISCAL_YEAR,
  i.IS_DOP_LIST as PRIZNAK_DOP_LIST,
  i.ORDINAL_NUMBER,
  i.SELLER_AGENCY_INFO_INN as SELLER_AGENCY_INFO_INN,
  i.SELLER_AGENCY_INFO_KPP as SELLER_AGENCY_INFO_KPP,
  i.SELLER_AGENCY_INFO_NAME as SELLER_AGENCY_INFO_NAME,
  i.SELLER_AGENCY_INFO_NUM as SELLER_AGENCY_INFO_NUM,
  trunc(sysdate) as SELLER_AGENCY_INFO_DATE,
  case
    when exists(select * from STAGE_INVOICE_CORRECTED sic WHERE i.row_key = sic.invoice_original_id) then 1
    else null end
  as IS_CHANGED
from sov_invoice i
  inner join V$Declaration_history d on i.DECLARATION_VERSION_ID = d.DECLARATION_VERSION_ID
  left join V_EGRN_IP buyer_ip on buyer_ip.innfl = i.buyer_inn
  left join V_EGRN_IP seller_ip on seller_ip.innfl = i.seller_inn
  left join V_EGRN_IP broker_ip on broker_ip.innfl = i.broker_inn
  left join V_EGRN_UL buyer_jp on buyer_jp.inn = i.buyer_inn AND buyer_jp.kpp = i.buyer_kpp
  left join V_EGRN_UL seller_jp on seller_jp.inn = i.seller_inn AND seller_jp.kpp = i.seller_kpp
  left join V_EGRN_UL broker_jp on broker_jp.inn = i.broker_inn AND broker_jp.kpp = i.broker_kpp
;