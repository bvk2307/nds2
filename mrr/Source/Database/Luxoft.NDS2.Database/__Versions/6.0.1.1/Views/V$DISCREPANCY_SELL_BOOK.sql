﻿create or replace force view NDS2_MRR_USER.v$discrepancy_sell_book
as
select
  d.id,
  d.sov_id as discrepancy_sov_id,
  i.row_key,
  i.actual_row_key,
  i.request_id,
  case when dc.row_key is null then 0 else 1 end as commented,
  /*break*/
  decl.tax_period,
  i.operation_code,
  i.invoice_num,
  i.invoice_date,
  i.change_num,
  i.change_date,
  i.correction_num,
  i.correction_date,
  i.change_correction_num,
  i.change_correction_date,
  i.okv_code,
  decl.correction_number,
  /*break*/
  i.buyer_inn,
  i.buyer_kpp,
  tBuyer.name as buyer_name,
  i.broker_inn,
  i.broker_kpp,
  tBroker.name as broker_name,
  i.receipt_doc_num,
  i.receipt_doc_date,
  i.price_sell_18,
  i.price_sell_10,
  i.price_sell_0,
  i.price_nds_18,
  i.price_nds_10,
  i.price_tax_free,
  i.price_sell_in_curr,
  i.price_sell,
  i.logical_errors,
  dStage.STAGE_CODE                 as STAGE_CODE,
  dStage.STAGE_NAME                 as STAGE_NAME,
  dStage.STAGE_STATUS_CODE          as STAGE_STATUS_CODE,
  dStage.STAGE_STATUS_NAME          as STAGE_STATUS_NAME
from v$discrepancy_union d
inner join V$Declaration decl on decl.id = d.decl_id
inner join sov_invoice i on i.DECLARATION_VERSION_ID = decl.DECLARATION_VERSION_ID
left join V$TAXPAYER tBroker on tBroker.inn = i.broker_inn and tBroker.kpp = i.broker_kpp
left join V$TAXPAYER tBuyer on tBuyer.inn = i.buyer_inn and tBuyer.kpp = i.buyer_kpp
left join discrepancy_comment dc
     on dc.discrepancy_id = d.id
     and dc.chapter = i.chapter
     and dc.row_key = i.row_key
left join v$discrepancy_stage dStage on dStage.Id = d.id
where i.chapter = 9;