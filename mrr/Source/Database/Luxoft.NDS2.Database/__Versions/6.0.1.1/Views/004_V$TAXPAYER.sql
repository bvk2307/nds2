﻿create or replace force view NDS2_MRR_USER.V$EGRN_IP
as
select
  ip.id,
  ip.innfl as inn,
  ip.ogrnip,
  ip.last_name,
  ip.first_name,
  ip.patronymic,
  ip.date_birth,
  ip.doc_code,
  ip.doc_num,
  ip.doc_date,
  ip.doc_org,
  ip.doc_org_code,
  ip.adr,
  ip.code_no,
  ip.date_on,
  ip.reason_on,
  ip.date_off,
  ip.reason_off,
  soun.S_Name as SOUN_NAME,
  ssrf.s_code as  Region_Code,
  ssrf.s_name as Region_Name
from V_EGRN_IP ip
  left join V$SONO soun on soun.s_code = ip.code_no
  left join V$SSRF ssrf on ssrf.s_code = (case when length(ip.code_no) > 1 then substr(ip.code_no, 1, 2) else '' end)
;

create or replace force view NDS2_MRR_USER.V$EGRN_UL
as
select
  ul.id,
  ul.inn,
  ul.kpp,
  ul.ogrn,
  ul.name_full,
  ul.date_reg,
  ul.code_no,
  ul.date_on,
  ul.date_off,
  ul.reason_off,
  ul.adr,
  ul.ust_capital,
  soun.S_Name as SOUN_NAME,
  ssrf.s_code as  Region_Code,
  ssrf.s_name as Region_Name
from V_EGRN_UL ul
  left join V$SONO soun on soun.s_code = ul.code_no
  left join V$SSRF ssrf on ssrf.s_code = (case when length(ul.code_no) > 1 then substr(ul.code_no, 1, 2) else '' end)
;

create or replace force view NDS2_MRR_USER.V$BSSCHET_UL
as
select
 INN,
 KPP,
 BIK,
 NAMEKO,
 NOMSCH,
 PRVAL,
 case PRVAL when  '0' then 'рубли' else 'не рубли' end as PRVALTEXT,
 DATEOPENSCH
from BSSCHET_UL
;

create or replace force view NDS2_MRR_USER.V$BSSCHET_IP
as
select
 INN,
 BIK,
 NAMEKO,
 NOMSCH,
 PRVAL,
 case PRVAL when  '0' then 'рубли' else 'не рубли' end as PRVALTEXT,
 DATEOPENSCH
from BSSCHET_IP
;


create or replace force view NDS2_MRR_USER.V$TAXPAYER
as
select
  ul.Name_Full as NAME,
  ul.inn,
  ul.kpp,
  ul.REGION_CODE,
  null as LEGAL_ADDRESS,
  ul.adr as ADDRESS,
  ul.code_no as SOUN_CODE,
  case substr(ul.kpp, 5, 2) when '01' then 1 else 0 end as CATEGORY,
  ul.date_reg as DATE_RECORDED,
  null as TAXATION,
  null as OKVED_CODE,
  ul.ust_capital as REGULATION_FUND,
  ul.SOUN_NAME,
  ul.region_name
from V$EGRN_UL ul
union
select
  ip.LAST_NAME || ' ' || ip.first_name || ' ' || ip.patronymic NAME,
  ip.inn,
  null as kpp,
  ip.REGION_CODE,
  null as LEGAL_ADDRESS,
  ip.adr as ADDRESS,
  ip.code_no as SOUN_CODE,
  null as CATEGORY,
  ip.date_on as DATE_RECORDED,
  null as TAXATION,
  null as OKVED_CODE,
  null as REGULATION_FUND,
  ip.SOUN_NAME,
  ip.region_name
from V$EGRN_IP ip;
