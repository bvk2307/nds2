﻿create or replace force view NDS2_MRR_USER.V$CONTROL_RATIO as
select
  cr.Id                as Id,
  cr.IdDekl            as ask_decl_Id,
  dh.DECLARATION_VERSION_ID as Decl_Version_Id,
  case when cr.Vypoln = 0 then 1 else 0 end as HasDiscrepancy,
  case when cr.Vypoln = 0 then 'Да' else null end as HasDiscrepancyString,
  cr.LevyaChast        as DisparityLeft,
  cr.PravyaChast       as DisparityRight,
  crc.User_Comment     as UserComment,
  crt.Code             as TypeCode,
  crt.Formulation      as TypeFormulation,
  crt.Description      as TypeDescription,
  crt.Calculation_Condition   as TypeCalculationCondition,
  crt.Calculation_Operator    as TypeCalculationOperator,
  crt.Calculation_Left_Side   as TypeCalculationLeftSide,
  crt.Calculation_Right_Side  as TypeCalculationRightSide
from V$ASKKontrSoontosh cr
inner join v$declaration_history dh on dh.ASK_DECL_ID = cr.IdDekl
left join control_ratio_comment crc on crc.id = cr.id
left join control_ratio_type crt on crt.code = cr.KodKs;
