﻿create or replace force view NDS2_MRR_USER.v$egrn_ip_alias
as
select 
row_number() over (partition by INNFL order by DATE_ON desc) as ActualSign,
ID, 
INNFL, 
OGRNIP, 
LAST_NAME, 
FIRST_NAME, 
PATRONYMIC, 
DATE_BIRTH, 
DOC_CODE, 
DOC_NUM, 
DOC_DATE, 
DOC_ORG, 
DOC_ORG_CODE, 
ADR,
CODE_NO,
DATE_ON, 
REASON_ON,
DATE_OFF,
REASON_OFF
from V_EGRN_IP@NDS2_FCOD;

create or replace force view NDS2_MRR_USER.v$egrn_ul_alias
as
select
row_number() over (partition by INN order by DATE_ON desc) as ActualSign,
ID,
INN,
KPP,
OGRN,
NAME_FULL,
DATE_REG,
CODE_NO,
DATE_ON,
DATE_OFF,
REASON_OFF,
ADR,
UST_CAPITAL,
IS_KNP
from V_EGRN_UL@NDS2_FCOD;

create or replace force view NDS2_MRR_USER.v$sur_alias
as
select
row_number() over (partition by INN,NPERIOD,PYEAR  order by DPR desc) as ActualSign,
PYEAR, 
NPERIOD, 
INN, 
PR, 
DPR
from ASKNDS1PR@NDS2_FCOD;

create or replace force view NDS2_MRR_USER.v$sono_alias
as
select
row_number() over (partition by s.kod order by s.date_from desc) as ActualSign,
s.kod as s_code,
s.naim as s_name
from thes_sono@NDS2_AIS3_MDM s 
where s.trs_cancel_id is null 
      and s.date_from <= sysdate 
      and s.date_to > sysdate 
      and s.is_annuled = 0 
      and s.is_closed = 0;

create or replace force view NDS2_MRR_USER.v$ssrf_alias
as
select
row_number() over (partition by s.kod order by s.date_from desc) as ActualSign,
s.kod as s_code,
s.naim as s_name
from thes_ssrf@NDS2_AIS3_MDM s 
where s.trs_cancel_id is null 
      and s.date_from <= sysdate 
      and s.date_to > sysdate 
      and s.is_annuled = 0 
      and s.is_closed = 0;
