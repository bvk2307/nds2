﻿create or replace force view NDS2_MRR_USER.V$REPORT_CHECK_LOGIC_CONTROL AS
SELECT
    r.TASK_ID
	,r.NUM
	,r.iNUM
	,r.Name
	,r.ErrorCount
	,r.ErrorCountPercentage
	,r.ErrorFrequencyInvoiceNotExact
	,r.ErrorFrequencyInvoiceGap
	,r.ErrorFrequencyInvoiceExact
	,r.ErrorFrequencyInnerInvoice
	,r.ErrorFrequencyOuterInvoice
from NDS2_MRR_USER.REPORT_CHECK_LOGIC_CONTROL r
where r.ACTUAL_NUM = 1
order by r.AGG_ROW desc, r.iNUM;