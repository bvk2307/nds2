﻿create or replace force view NDS2_MRR_USER.V$REPORT_MATCHING_RULE AS
SELECT
    r.TASK_ID
    ,r.GroupNumNotExact
    ,r.RuleNum
    ,r.DiscrepancyCount
    ,r.DiscrepancyAmount
    ,r.PercentByCount
    ,r.PercentByAmount
    ,r.PercentInnerInvoiceErrorLC
    ,r.PercentOuterInvoiceErrorLC
    ,r.PercentDiscrepRequirement
    ,r.PercentDiscrepReclaimDoc
    ,r.PercentDiscrepOtherMNK
    ,r.PercentDiscrepRemoveTotal
    ,r.PercentDiscrepNotRemoveTotal
from NDS2_MRR_USER.REPORT_MATCHING_RULE r
where r.ACTUAL_NUM = 1
order by r.AGG_ROW desc, r.GroupNumNotExact, r.RuleNum;