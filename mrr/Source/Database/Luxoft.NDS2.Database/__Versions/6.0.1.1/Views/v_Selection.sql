﻿create or replace force view NDS2_MRR_USER.v_Selection as
SELECT
  s.ID as Id,
  ST.s_Code as TypeCode,
  ST.s_Name as Type,
  s.Name as Name,
  s.Creation_Date as CreationDate,
  s.Modification_Date as ModificationDate,
  s.Status_Date as StatusDate,
  SS.ID as Status,
  ss.Name as StatusName,
  nvl(declAgr.decl_count, 0) as DeclarationCount,
  nvl(declAgr.decl_pvp_total, 0) as decl_pvp_total,
  nvl(discrAgr.discr_count, 0) as DiscrepanciesCount,
  nvl(discrAgr.discr_amount, 0) as TotalAmount,
  nvl(discrAgr.discr_pvp_amount, 0) as SelectedDiscrepanciesPVPAmount,
  s.Analytic_SID as Analytic_SID,
  s.Analytic as Analytic,
  s.Chief_SID as Manager_SID,
  s.Chief as Manager,
  null as RegionCode,
  /*regionAgr.RegionFirst*/ null as RegionName,
  /*regionAgr.Regions*/ null as RegionNames,
  s.Request_id,
  s.filter,
  ss.RANK
from selection s
inner join DICT_SELECTION_STATUS SS ON s.STATUS = SS.ID
inner join DICT_SELECTION_TYPE ST ON s.TYPE = ST.S_CODE
left join 
(
  select
    rf.request_id,
    count(d.id) as decl_count,
  sum(d.PVP_TOTAL_AMNT) as decl_pvp_total
  from SELECTION_DECLARATION rf
       inner join SOV_DECLARATION_INFO d on d.id = rf.declaration_id
  where rf.is_in_process = 1
  group by rf.request_id
)  declAgr on declAgr.request_id = s.request_id
left join
(
  select 
    dr.request_id,
    count(dr.discrepancy_id) as discr_count,
    sum(ds1.amnt) as discr_amount,
    sum(ds1.amount_pvp) as discr_pvp_amount
  from SELECTION_DISCREPANCY dr
       inner join sov_discrepancy ds1 on ds1.id = dr.discrepancy_id
  where dr.is_in_process = 1
  group by dr.request_id
)  discrAgr on discrAgr.request_id = s.request_id
/*
left join
(
  select
    request_id,
    substr(max(sys_connect_by_path(regionInfo, ', ' )), 3) regions,
    substr(min(sys_connect_by_path(regionInfo, ', ' )), 3) || (case when count(*) > 1 then ' ...' else '' end) as RegionFirst
  from
  (
    select
       rf.request_id
       ,d.regionInfo
       ,min(d.id) as id
       ,row_number() over (partition by rf.request_id order by d.regionInfo) rn
      from SELECTION_DECLARATION rf
      inner join
      (
        select v.*,
        (v."REGION_CODE" || ' - ' || v."REGION_NAME") as regionInfo
         from V$Declaration v
      ) d on d.id = rf.declaration_id
      group by rf.request_id, d.regionInfo
      order by rf.request_id, d.regionInfo
  )
  start with rn = 1
  connect by prior rn = rn-1
  and prior request_id = request_id
  group by request_id
  order by request_id
) regionAgr on regionAgr.request_id = s.request_id
*/
;
