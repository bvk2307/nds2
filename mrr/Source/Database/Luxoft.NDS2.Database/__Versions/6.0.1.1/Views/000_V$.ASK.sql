﻿create or replace force view NDS2_MRR_USER.V$ASKKontrSoontosh
as
select 
"Ид" as ID, 
	"ИдДекл" as IdDekl, 
	"КодКС" as KodKs, 
	"ЛеваяЧасть" as LevyaChast, 
	"ПраваяЧасть" as PravyaChast,
    "Выполн" as Vypoln
from NDS2_MC."ASKКонтрСоотн";

create or replace force view NDS2_MRR_USER.V$ASKPoyasnenie
as
select
   mc."Ид" as Id
  ,mc."ZIP" as ZIP
  ,mc."ИдФайл" as IdFayl
  ,mc."ВерсПрог" as VersProg
  ,mc."ВерсФорм" as VersForm
  ,mc."КНД" as KND
  ,mc."ДатаДок" as DataDok
  ,mc."ПризнНал8" as PriznNal8
  ,mc."ПризнНал81" as PriznNal81
  ,mc."ПризнНал9" as PriznNal9
  ,mc."ПризнНал91" as PriznNal91
  ,mc."ПризнНал10" as PriznNal10
  ,mc."ПризнНал11" as PriznNal11
  ,mc."ПризнНал12" as PriznNal12
  ,mc."КолФайлОтв" as KolFaylOtv
  ,mc."ПризОтпрУП" as PrizOtprUP
  ,mc."НаимОргОтпр" as NaimOrgOtpr
  ,mc."ИННОтпр" as INNOtpr
  ,mc."КППОтпр" as KPPOtpr
  ,mc."ФамилияОтпр" as FamiliyaOtpr
  ,mc."ИмяОтпр" as ImyaOtpr
  ,mc."ОтчествоОтпр" as OtchestvoOtpr
  ,mc."КодНО" as KodNO
  ,mc."НаимОргНП" as NaimOrgNP
  ,mc."ИНННП" as INNNP
  ,mc."КППНП" as KPPNP
  ,mc."ФамилияНП" as FamiliyaNP
  ,mc."ИмяНП" as ImyaNP
  ,mc."ОтчествоНП" as OtchestvoNP
  ,mc."ПрПодп" as PrPodp
  ,mc."Должн" as Dolzhn
  ,mc."Тлф" as Tlf
  ,mc."E-mail" as Email
  ,mc."ФамилияПодп" as FamiliyaPodp
  ,mc."ИмяПодп" as ImyaPodp
  ,mc."ОтчествоПодп" as OtchestvoPodp
  ,mc."НаимДок" as NaimDok
  ,mc."НомТреб" as NomTreb
  ,mc."ДатаТреб" as DataTreb
  ,mc."Период" as Period
  ,mc."ОтчетГод" as OtchetGod
  ,mc."НомКорр" as NomKorr
  ,mc."ИмяФайлТреб" as ImyaFaylTreb
  ,mc."ИдЗагрузка" as IdZagruzka
  ,mc."ИдДекл" as IdDekl
  ,mc."ОбработанМС" as ObrabotanMS
  ,mc."КолЗапВсего" as KolZapVsego
  ,mc."КолЗапПодтв" as KolZapPodtv
  ,mc."КолЗапИзм" as KolZapIzm
  ,mc."КолЗапНесовп" as KolZapNesovp
from NDS2_MC."ASKПояснение" mc;

/*ASKZIPFAJL (ASKZIPФайл)*/
create or replace force view NDS2_MRR_USER.V$ASKZIPFAJL
as
select
"Ид" as ID
,"ИмяФайла" as IMYAFAJLA
,"ИдФайлОпис" as IDFAJLOPIS
,"ИдГП3" as IDGP3
,"ИсхПуть" as ISXPUT
,"Путь" as PUT
,"ИдДокОбр" as IDDOKOBR
,"КодНО" as KODNO
,"ДатаФайла" as DATAFAJLA
,"Статус" as STATUS
,"ДатаСтатуса" as DATASTATUSA
,"Ошибка" as OSHIBKA
from NDS2_MC."ASKZIPФайл";
 
/*ASKDEKL (ASKДекл)*/
create or replace force view NDS2_MRR_USER.V$ASKDEKL
as
select
"Ид" as ID
,"ZIP" as ZIP
,"ИдФайл" as IDFAJL
,"ВерсПрог" as VERSPROG
,"ВерсФорм" as VERSFORM
,"ПризнНал8-12" as PRIZNNAL8_12
,"ПризнНал8" as PRIZNNAL8
,"ПризнНал81" as PRIZNNAL81
,"ПризнНал9" as PRIZNNAL9
,"ПризнНал91" as PRIZNNAL91
,"ПризнНал10" as PRIZNNAL10
,"ПризнНал11" as PRIZNNAL11
,"ПризнНал12" as PRIZNNAL12
,"КНД" as KND
,"ДатаДок" as DATADOK
,"Период" as PERIOD
,"ОтчетГод" as OTCHETGOD
,"КодНО" as KODNO
,"НомКорр" as NOMKORR
,"ПоМесту" as POMESTU
,"ОКВЭД" as OKVED
,"Тлф" as TLF
,"НаимОрг" as NAIMORG
,"ИНННП" as INNNP
,"КППНП" as KPPNP
,"ФормРеорг" as FORMREORG
,"ИННРеорг" as INNREORG
,"КППРеорг" as KPPREORG
,"ФамилияНП" as FAMILIYANP
,"ИмяНП" as IMYANP
,"ОтчествоНП" as OTCHESTVONP
,"ПрПодп" as PRPODP
,"ФамилияПодп" as FAMILIYAPODP
,"ИмяПодп" as IMYAPODP
,"ОтчествоПодп" as OTCHESTVOPODP
,"НаимДок" as NAIMDOK
,"НаимОргПред" as NAIMORGPRED
,"ОКТМО" as OKTMO
,"КБК" as KBK
,"СумПУ173.5" as SUMPU173_5
,"СумПУ173.1" as SUMPU173_1
,"НомДогИТ" as NOMDOGIT
,"ДатаДогИТ" as DATADOGIT
,"ДатаНачДогИТ" as DATANACHDOGIT
,"ДатаКонДогИТ" as DATAKONDOGIT
,"НалПУ164" as NALPU164
,"НалВосстОбщ" as NALVOSSTOBSHH
,"РлТв18НалБаз" as REALTOV18NALBAZA
,"РеалТов18СумНал" as REALTOV18SUMNAL
,"РлТв10НалБаз" as REALTOV10NALBAZA
,"РеалТов10СумНал" as REALTOV10SUMNAL
,"РлТв118НалБаз" as REALTOV118NALBAZA
,"РлТв118СумНал" as REALTOV118SUMNAL
,"РлТв110НалБаз" as REALTOV110NALBAZA
,"РлТв110СумНал" as REALTOV110SUMNAL
,"РлПрдИКНалБаз" as REALPREDIKNALBAZA
,"РлПрдИКСумНал" as REALPREDIKSUMNAL
,"ВыпСМРСобНалБаз" as VYPSMRSOBNALBAZA
,"ВыпСМРСобСумНал" as VYPSMRSOBSUMNAL
,"ОпПрдПстНлБаз" as OPLPREDPOSTNALBAZA
,"ОплПрдПстСумНал" as OPLPREDPOSTSUMNAL
,"СумНалВс" as SUMNALVS
,"СумНал170.3.5" as SUMNAL170_3_5
,"СумНал170.3.3" as SUMNAL170_3_3
,"КорРлТв18НалБаз" as KORREALTOV18NALBAZA
,"КорРлТв18СумНал" as KORREALTOV18SUMNAL
,"КорРлТв10НалБаз" as KORREALTOV10NALBAZA
,"КорРлТв10СумНал" as KORREALTOV10SUMNAL
,"КорРлТв118НлБз" as KORREALTOV118NALBAZA
,"КорРлТв118СмНл" as KORREALTOV118SUMNAL
,"КорРлТв110НлБз" as KORREALTOV110NALBAZA
,"КорРлТв110СмНл" as KORREALTOV110SUMNAL
,"КорРлПрдИКНлБз" as KORREALPREDIKNALBAZA
,"КорРлПрдИКСмНл" as KORREALPREDIKSUMNAL
,"НалПредНППриоб" as NALPREDNPPRIOB
,"НалПредНППок" as NALPREDNPPOK
,"НалИсчСМР" as NALISCHSMR
,"НалУплТамож" as NALUPLTAMOZH
,"НалУплНОТовТС" as NALUPLNOTOVTS
,"НалИсчПрод" as NALISCHPROD
,"НалУплПокНА" as NALUPLPOKNA
,"НалВычОбщ" as NALVYCHOBSHH
,"СумИсчислИтог" as SUMISCHISLITOG
,"СумВозмПдтв" as SUMVOZMPDTV
,"СумВозмНеПдтв" as SUMVOZMNEPDTV
,"СумНал164Ит" as SUMNAL164IT
,"НалВычНеПодИт" as NALVYCHNEPODIT
,"НалИсчислИт" as NALISCHISLIT
,"СмОп1010449КдОп" as SUMOPER1010449KODOPER
,"СмОп1010449НлБз" as SUMOPER1010449NALBAZA
,"СмОп1010449КрИсч" as SUMOPER1010449KORISCH
,"СмОп1010449НлВст" as SUMOPER1010449NALVOSST
,"ОплПостСв6Мес" as OPLPOSTSV6MES
,"НаимКнПок" as NAIMKNPOK
,"НаимКнПокДЛ" as NAIMKNPOKDL
,"НаимКнПрод" as NAIMKNPROD
,"НаимКнПродДЛ" as NAIMKNPRODDL
,"НаимЖУчВыстСчФ" as NAIMZHUCHVYSTSCHF
,"НаимЖУчПолучСчФ" as NAIMZHUCHPOLUCHSCHF
,"НмВстСчФ173_5" as NAIMVYSTSCHF173_5
,"ИдЗагрузка" as	IdZagruzka
,"КодОпер47" as	KodOper47
,"НалБаза47" as	NalBaza47
,"НалВосст47" as NalVosst47
,"КодОпер48"	as KodOper48
,"КорНалБазаУв48" as	KorNalBazaUv48
,"КорНалБазаУм48" as	KorNalBazaUm48
,"КодОпер50" as	KodOper50
,"КорНалБазаУв50" as KorNalBazaUv50
,"КорИсчУв50" as	KorIschUv50
,"КорНалБазаУм50" as	KorNalBazaUm50
,"КорИсчУм50" as	KorIschUm50
,"КлючДекл" as KlyuchDekl
from NDS2_MC."ASKДекл";
 
/*ASKSVEDNALGOD (ASKСведНалГод)*/
create or replace force view NDS2_MRR_USER.V$ASKSVEDNALGOD
as
select
"Ид" as ID
,"ИдСумВосУпл" as IDSUMVOSUPL
,"ГодОтч" as GODOTCH
,"ДатаИсп170" as DATAISP170
,"ДоляНеОбл" as DOLYANEOBL
,"НалГод" as NALGOD
from NDS2_MC."ASKСведНалГод";
 
/*ASKSVEDNALGODI (ASKСведНалГодИ)*/
create or replace force view NDS2_MRR_USER.V$ASKSVEDNALGODI
as
select
"Ид" as ID
,"ИдДекл" as IDDEKL
,"КППИнУч" as KPPINUCH
,"СумНалИсч" as SUMNALISCH
,"СумНалВыч" as SUMNALVYCH
from NDS2_MC."ASKСведНалГодИ";
 
/*ASKSVODZAP (ASKСводЗап)*/
create or replace force view NDS2_MRR_USER.V$ASKSVODZAP
as
select
   mc."Ид" as ID
  ,mc."ZIP" as ZIP
  ,mc."Индекс" as INDEKS
  ,mc."НомКорр" as NOMKORR
  ,mc."ПризнакАкт" as PRIZNAKAKT
  ,mc."ТипФайла" as TIPFAJLA
  ,mc."СумНДСПок" as SUMNDSPOK
  ,mc."СумНДСПокДЛ" as SUMNDSPOKDL
  ,mc."СтПрод18" as STPROD18
  ,mc."СтПрод10" as STPROD10
  ,mc."СтПрод0" as STPROD0
  ,mc."СумНДСПрод18" as SUMNDSPROD18
  ,mc."СумНДСПрод10" as SUMNDSPROD10
  ,mc."СтПрод" as STPROD
  ,mc."СтПродОсв" as STPRODOSV
  ,mc."СтПрод18ДЛ" as STPROD18DL
  ,mc."СтПрод10ДЛ" as STPROD10DL
  ,mc."СтПрод0ДЛ" as STPROD0DL
  ,mc."СумНДС18ДЛ" as SUMNDS18DL
  ,mc."СумНДС10ДЛ" as SUMNDS10DL
  ,mc."СтПродОсвДЛ" as STPRODOSVDL
  ,mc."ИдЗагрузка" as IdZagruzka
  ,mc."ИдЗамена" as IdZamena
  ,mc."ОшибкиЛК" as OshibkiLK
  ,mc."ОшибкиФК" as OshibkiFK
  ,mc."ИдДекл" as IdDekl
  ,mc."КолЗаписей" as KolZapisey
  ,mc."ПрНеобр0" as PrNeobr0
  ,mc."ПрНеобр1" as PrNeobr1
from NDS2_MC."ASKСводЗап" mc;
 
/*ASKSUMVOSUPL (ASKСумВосУпл)*/
create or replace force view NDS2_MRR_USER.V$ASKSUMVOSUPL
as
select
"Ид" as ID
,"ИдДекл" as IDDEKL
,"НаимНедв" as NAIMNEDV
,"КодОпНедв" as KODOPNEDV
,"ДатаВводОН" as DATAVVODON
,"ДатаНачАмОтч" as DATANACHAMOTCH
,"СтВводОН" as STVVODON
,"НалВычОН" as NALVYCHON
,"Индекс" as INDEKS
,"КодРегион" as KODREGION
,"Район" as RAJON
,"Город" as GOROD
,"НаселПункт" as NASELPUNKT
,"Улица" as ULICA
,"Дом" as DOM
,"Корпус" as KORPUS
,"Кварт" as KVART
from NDS2_MC."ASKСумВосУпл";
 
/*ASKSUMOPER1010447 (ASKСумОпер1010447)*/
--create or replace force view V$ASKSUMOPER1010447
--as
--select
--"Ид" as ID
--,"ИдДекл" as IDDEKL
--,"КодОпер" as KODOPER
--,"НалБаза" as NALBAZA
--,"НалВосст" as NALVOSST
--from NDS2_MC."ASKСумОпер1010447";
 
/*ASKSUMOPER1010448 (ASKСумОпер1010448)*/
--create or replace force view V$ASKSUMOPER1010448
--as
--select
--"Ид" as ID
--,"ИдДекл" as IDDEKL
--,"КодОпер" as KODOPER
--,"КорНалБазаУв" as KORNALBAZAUV
--,"КорНалБазаУм" as KORNALBAZAUM
--from NDS2_MC."ASKСумОпер1010448";
 
/*ASKSUMOPER1010450 (ASKСумОпер1010450)*/
--create or replace force view V$ASKSUMOPER1010450
--as
--select
--"Ид" as ID
--,"ИдДекл" as IDDEKL
--,"КодОпер" as KODOPER
--,"КорНалБазаУв" as KORNALBAZAUV
--,"КорИсч.164.23Ув" as KORISCH_164_23UV
--,"КорНалБазаУм" as KORNALBAZAUM
--,"КорИсч.164.23Ум" as KORISCH_164_23UM
--from NDS2_MC."ASKСумОпер1010450";
 
/*ASKSUMOPER4 (ASKСумОпер4)*/
create or replace force view NDS2_MRR_USER.V$ASKSUMOPER4
as
select
"Ид" as ID
,"ИдДекл" as IDDEKL
,"КодОпер" as KODOPER
,"НалБаза" as NALBAZA
,"НалВычПод" as NALVYCHPOD
,"НалНеПод" as NALNEPOD
,"НалВосст" as NALVOSST
from NDS2_MC."ASKСумОпер4";
 
/*ASKSUMOPER5 (ASKСумОпер5)*/
create or replace force view NDS2_MRR_USER.V$ASKSUMOPER5
as
select
"Ид" as ID
,"ИдСумПер" as IDSUMPER
,"КодОпер" as KODOPER
,"НалБазаПод" as NALBAZAPOD
,"НалВычПод" as NALVYCHPOD
,"НалБазаНеПод" as NALBAZANEPOD
,"НалВычНеПод" as NALVYCHNEPOD
from NDS2_MC."ASKСумОпер5";
 
/*ASKSUMOPER6 (ASKСумОпер6)*/
create or replace force view NDS2_MRR_USER.V$ASKSUMOPER6
as
select
"Ид" as ID
,"ИдДекл" as IDDEKL
,"КодОпер" as KODOPER
,"НалБаза" as NALBAZA
,"СумНал164" as SUMNAL164
,"НалВычНеПод" as NALVYCHNEPOD
from NDS2_MC."ASKСумОпер6";
 
/*ASKSUMOPER7 (ASKСумОпер7)*/
create or replace force view NDS2_MRR_USER.V$ASKSUMOPER7
as
select
"Ид" as ID
,"ИдДекл" as IDDEKL
,"КодОпер" as KODOPER
,"СтРеалТов" as STREALTOV
,"СтПриобТов" as STPRIOBTOV
,"НалНеВыч" as NALNEVYCH
from NDS2_MC."ASKСумОпер7";
 
/*ASKSUMPER (ASKСумПер)*/
create or replace force view NDS2_MRR_USER.V$ASKSUMPER
as
select
"Ид" as ID
,"ИдДекл" as IDDEKL
,"ОтчетГод" as OTCHETGOD
,"Период" as PERIOD
from NDS2_MC."ASKСумПер";
 
/*ASKSUMUPLNA (ASKСумУплНА)*/
create or replace force view NDS2_MRR_USER.V$ASKSUMUPLNA
as
select
"Ид" as ID
,"ИдДекл" as IDDEKL
,"КППИно" as KPPINO
,"КБК" as KBK
,"ОКТМО" as OKTMO
,"СумИсчисл" as SUMISCHISL
,"КодОпер" as KODOPER
,"СумИсчислОтгр" as SUMISCHISLOTGR
,"СумИсчислОпл" as SUMISCHISLOPL
,"СумИсчислНА" as SUMISCHISLNA
,"НаимПрод" as NAIMPROD
,"ИННПрод" as INNPROD
,"Фамилия" as FAMILIYA
,"Имя" as IMYA
,"Отчество" as OTCHESTVO
from NDS2_MC."ASKСумУплНА";
 
/*ASKFAJLOPIS (ASKФайлОпис)*/
create or replace force view NDS2_MRR_USER.V$ASKFAJLOPIS
as
select
"Ид" as ID
,"Дата" as DATA
,"ИдОпис" as IDOPIS
,"Состояние" as SOSTOYANIE
,"ДатаСост" as DATASOST
from NDS2_MC."ASKФайлОпис";

create or replace force view NDS2_MRR_USER.V$ASKZhurnal
as
select
   mc."Ид" as Id
  ,mc."ИдОпер" as IdOper
  ,mc."НаимПроцесс" as NaimProtsess
  ,mc."Итерация" as Iteratsiya
  ,mc."ДатаНач" as DataNach
  ,mc."ДатаОконч" as DataOkonch
  ,mc."ПризнРезульт" as PriznRezult
  ,mc."КолвоВхЗап" as KolvoVhZap
  ,mc."КолвоВыхЗап" as KolvoVyihZap
from NDS2_MC."ASKЖурнал" mc;


/*JOURNALUCH (ASKЖурналУч)*/
create or replace force view NDS2_MRR_USER.V$ASKJOURNALUCH
as
select
"Ид" as ID,
"ZIPЧ1" as ZIP,
"ИдФайл" as IDFAJL,
"ВерсПрог" as VERSPROG,
"ВерсФорм" as VERSFORM,
"ИдФайлИсх" as IDFAJLISH,
"ИдФайлПерв" as IDFAJLPERV,
"КолФайл" as KOLFAJL,
' ' as NOMFAJL,
"КНД" as KND,
"Период" as PERIOD,
"ОтчетГод" as OTCHETGOD,
"НаимОрг" as NAIMORG,
"ИНН" as INN,
"КПП" as KPP,
"СвГосРегИП" as SVGOSREGIP,
"Фамилия" as FAMILIJA,
"Имя" as IMJA,
"Отчество" as OTCHESTVO,
"ПрПодп" as PRPODP,
"ФамилияПодп"as FAMILIYAPODP,
"ИмяПодп" as IMYAPODP,
"ОтчествоПодп" as OTCHESTVOPODP,
"НаимДок" as NAIMDOK,
' ' as IDZAGRUZKA,
' ' as OSHIBKIFK
from NDS2_MC."ASKЖурналУч";

create or replace force view NDS2_MRR_USER.V$ASKSchetchiki
as
select
   mc."Ид" as Id
  ,mc."ИдЖурнал" as IdZhurnal
  ,mc."Счетчик" as Schetchik
  ,mc."Значение" as Znachenie
from NDS2_MC."ASKСчетчики" mc;

create or replace force view NDS2_MRR_USER.V$ASKOperatsiya
as 
select
   mc."Ид" as Id
  ,mc."НомерОп" as NomerOp
  ,mc."ВидОп" as VidOp
  ,mc."ДатаНач" as DataNach
  ,mc."ДатаОконч" as DataOkonch
  ,mc."ДатаОшибки" as DataOshibki
from NDS2_MC."ASKОперация" mc;

