﻿create or replace force view NDS2_MRR_USER.v$discrepancy_side as
select
  d.id as discrepancy_id,
  d.sov_id as discrepancy_sov_id,
  si.row_key as invoice_row_key,
  si.request_id as invoice_request_id,
  (case when (si.chapter = 8) then 'Раздел 8'
       when (si.chapter = 9) then 'Раздел 9'
       when (si.chapter = 10) then 'Раздел 10'
       when (si.chapter = 11) then 'Раздел 11'
       when (si.chapter = 12) then 'Раздел 12'
       else '' end) as Side,
  si.chapter,
  sd.DECLARATION_VERSION_ID as declaration_id,
  sd.inn as Side1Inn,
  sd.kpp as Side1Kpp,
  nvl(sd.name,taxpayer.name) as Side1Name,
  case si.chapter
    when 8 then si.seller_inn
    when 9 then si.buyer_inn
    when 10 then si.buyer_inn
    when 11 then si.seller_inn
    when 12 then si.buyer_inn
  end as Side2Inn,
  case si.chapter
    when 8 then si.seller_kpp
    when 9 then si.buyer_kpp
    when 10 then si.buyer_kpp
    when 11 then si.seller_kpp
    when 12 then si.buyer_kpp
  end as Side2Kpp,
  case si.chapter
    when 8 then seller.name
    when 9 then buyer.name
    when 10 then buyer.name
    when 11 then seller.name
    when 12 then buyer.name
  end as Side2Name,
  si.invoice_num,
  si.invoice_date,
  case si.chapter
    when 8 then si.PRICE_BUY_AMOUNT
    when 9 then si.PRICE_SELL
    when 10 then si.PRICE_TOTAL
    when 11 then si.PRICE_TOTAL
  end as price_total,
  case si.chapter
    when 8 then si.PRICE_BUY_NDS_AMOUNT
    when 9 then nvl(si.PRICE_SELL_18, 0) + nvl(si.PRICE_SELL_10, 0)
    when 10 then si.PRICE_NDS_TOTAL
    when 11 then si.PRICE_NDS_TOTAL
  end as price_nds_total,
  sd.region_code,
  ssrf.s_name as region,
  sd.soun_code,
  soun.s_name as soun,
  si.FORMAT_ERRORS as FormatErrors,
  si.LOGICAL_ERRORS as LogicalErrors
from v$discrepancy_union d
inner join sov_invoice si on si.row_key = d.invoice_rk
inner join V$Declaration_history sd on sd.DECLARATION_VERSION_ID = si.DECLARATION_VERSION_ID
left join v$sono soun on soun.s_code = sd.soun_code
left join v$ssrf ssrf on ssrf.s_code = sd.region_code
left join v$taxpayer buyer on buyer.inn=si.buyer_inn and (si.buyer_kpp is null or buyer.kpp=si.buyer_kpp)
left join v$taxpayer seller on seller.inn=si.seller_inn and (si.seller_kpp is null or seller.kpp=si.seller_kpp)
left join v$taxpayer taxpayer on taxpayer.inn=sd.inn and (sd.kpp is null or taxpayer.kpp=sd.kpp)
union all
select
  d.id as discrepancy_id,
  d.sov_id as discrepancy_sov_id,
  si.row_key as invoice_row_key,
  si.request_id as invoice_request_id,
  (case when (si.chapter = 8) then 'Раздел 8'
        when (si.chapter = 9) then 'Раздел 9'
        when (si.chapter = 10) then 'Раздел 10'
        when (si.chapter = 11) then 'Раздел 11'
        when (si.chapter = 12) then 'Раздел 12'
        else '' end) as Side,
  si.chapter,
  sd.DECLARATION_VERSION_ID as declaration_id,
  case si.chapter
    when 8 then si.seller_inn
    when 9 then si.buyer_inn
    when 10 then si.buyer_inn
    when 11 then si.seller_inn
    when 12 then si.buyer_inn      
  end as Side1Inn,
  case si.chapter
    when 8 then si.seller_kpp
    when 9 then si.buyer_kpp
    when 10 then si.buyer_kpp
    when 11 then si.seller_kpp
    when 12 then si.buyer_kpp      
  end as Side1Kpp,
  case si.chapter
    when 8 then seller.name
    when 9 then buyer.name
    when 10 then buyer.name
    when 11 then seller.name
    when 12 then buyer.name      
  end as Side1Name,
  sd.inn as Side2Inn,
  sd.kpp as Side2Kpp,
  nvl(sd.name,taxpayer.name) as Side2Name,
  si.invoice_num,
  si.invoice_date,
  case si.chapter
    when 8 then si.PRICE_BUY_AMOUNT
    when 9 then si.PRICE_SELL
    when 10 then si.PRICE_TOTAL
    when 11 then si.PRICE_TOTAL
  end as price_total,
  case si.chapter
    when 8 then si.PRICE_BUY_NDS_AMOUNT
    when 9 then nvl(si.PRICE_SELL_18, 0) + nvl(si.PRICE_SELL_10, 0)
    when 10 then si.PRICE_NDS_TOTAL
    when 11 then si.PRICE_NDS_TOTAL
  end as price_nds_total,
  sd.region_code,
  ssrf.s_name as region,
  sd.soun_code,
  soun.s_name as soun,
  si.FORMAT_ERRORS as FormatErrors,
  si.LOGICAL_ERRORS as LogicalErrors
from v$discrepancy_union d
inner join sov_invoice si on si.row_key = d.invoice_contractor_rk
left join V$Declaration_history sd on sd.DECLARATION_VERSION_ID = si.DECLARATION_VERSION_ID
left join v$sono soun on soun.s_code = sd.soun_code
left join v$ssrf ssrf on ssrf.s_code = sd.region_code
left join v$taxpayer buyer on buyer.inn=si.buyer_inn and (si.buyer_kpp is null or buyer.kpp=si.buyer_kpp)
left join v$taxpayer seller on seller.inn=si.seller_inn and (si.seller_kpp is null or seller.kpp=si.seller_kpp)
left join v$taxpayer taxpayer on taxpayer.inn=sd.inn and (sd.kpp is null or taxpayer.kpp=sd.kpp);