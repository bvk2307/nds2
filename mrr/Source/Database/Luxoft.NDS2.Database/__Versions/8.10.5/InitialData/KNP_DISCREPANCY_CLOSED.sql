﻿insert into NDS2_MRR_USER.KNP_DISCREPANCY_CLOSED (discrepancy_id,closed_at,reason_id)
select t.discrepancy_id,min(nvl(t.date_exclude,sysdate)),1 from NDS2_MRR_USER.DOC_DISCREPANCY_EXCLUDE t 
where t.DISCREPANCY_ID not in (select discrepancy_id from NDS2_MRR_USER.KNP_DISCREPANCY_CLOSED) 
group by t.discrepancy_id;
commit;

begin
 EXECUTE IMMEDIATE 'CREATE TABLE "NDS2_MRR_USER"."CR_KNP_EXCLUDE_03052017" ("DISCREPANCY_ID" NUMBER)';
 exception when others then null;
end;
/

insert into NDS2_MRR_USER.KNP_DISCREPANCY_CLOSED (discrepancy_id,closed_at,reason_id)
select t.DISCREPANCY_ID, sysdate, 1 from NDS2_MRR_USER.CR_KNP_EXCLUDE_03052017 t 
where t.DISCREPANCY_ID not in (select discrepancy_id from NDS2_MRR_USER.KNP_DISCREPANCY_CLOSED) 
group by t.discrepancy_id;
commit;
