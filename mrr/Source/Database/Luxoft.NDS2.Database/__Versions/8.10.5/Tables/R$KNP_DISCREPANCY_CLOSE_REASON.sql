﻿-- Create table
create table NDS2_MRR_USER.R$KNP_DISCREPANCY_CLOSE_REASON
(
  id          number not null,
  description varchar2(50) not null
)
;
-- Add comments to the table 
comment on table NDS2_MRR_USER.R$KNP_DISCREPANCY_CLOSE_REASON
  is 'Справочник причин закрытия расхождений для КНП в МРР';
-- Add comments to the columns 
comment on column NDS2_MRR_USER.R$KNP_DISCREPANCY_CLOSE_REASON.id
  is 'Идентификатор причины закрытия';
comment on column NDS2_MRR_USER.R$KNP_DISCREPANCY_CLOSE_REASON.description
  is 'Описание причины закрытия';
-- Create/Recreate primary, unique and foreign key constraints 
alter table NDS2_MRR_USER.R$KNP_DISCREPANCY_CLOSE_REASON
  add constraint PK$KNP_CLOSE_REASON primary key (ID);
