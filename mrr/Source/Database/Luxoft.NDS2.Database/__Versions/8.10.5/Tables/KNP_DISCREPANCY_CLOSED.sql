﻿-- Create table
create table NDS2_MRR_USER.KNP_DISCREPANCY_CLOSED
(
  discrepancy_id number not null,
  closed_at      date not null,
  reason_id      number not null
);
-- Add comments to the table 
comment on table NDS2_MRR_USER.KNP_DISCREPANCY_CLOSED
  is 'Идентификаторы расхождений для КНП, закрытых в МРР';
comment on column NDS2_MRR_USER.KNP_DISCREPANCY_CLOSED.discrepancy_id is 'Идентификатор расхождения (sov_discrepancy.id)';
comment on column NDS2_MRR_USER.KNP_DISCREPANCY_CLOSED.closed_at is 'Дата закрытия';
comment on column NDS2_MRR_USER.KNP_DISCREPANCY_CLOSED.reason_id is 'Идентификатор причины закрытия (knp_discrepancy_close_reason.id)';
-- Create/Recreate indexes 
create index NDS2_MRR_USER.I$KNP_DIS_CLOSED_AT on NDS2_MRR_USER.KNP_DISCREPANCY_CLOSED (closed_at);
-- Create/Recreate primary, unique and foreign key constraints 
alter table NDS2_MRR_USER.KNP_DISCREPANCY_CLOSED
  add constraint UK$KNP_CLOSED_DISCREPANCY_ID primary key (DISCREPANCY_ID);
