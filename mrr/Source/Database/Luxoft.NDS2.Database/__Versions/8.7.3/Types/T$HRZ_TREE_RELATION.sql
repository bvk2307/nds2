alter type NDS2_MRR_USER.T$HRZ_TREE_RELATION add attribute (njsa NUMBER) cascade;
alter type NDS2_MRR_USER.T$HRZ_TREE_RELATION add attribute (njba NUMBER) cascade;
alter type NDS2_MRR_USER.T$HRZ_TREE_RELATION add attribute (before2015_amount NUMBER) cascade;
alter type NDS2_MRR_USER.T$HRZ_TREE_RELATION add attribute (after2015_amount NUMBER) cascade;
alter type NDS2_MRR_USER.T$HRZ_TREE_RELATION add attribute (nds_amount NUMBER) cascade;
alter type NDS2_MRR_USER.T$HRZ_TREE_RELATION add attribute (nds_discrep_amnt NUMBER) cascade;
