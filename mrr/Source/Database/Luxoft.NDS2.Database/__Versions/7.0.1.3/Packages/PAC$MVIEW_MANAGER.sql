create or replace package NDS2_MRR_USER.PAC$MVIEW_MANAGER
as
/*������������� ������ ���������� �����������*/
procedure REFRESH_DECLARATONS;
end;
/

create or replace package body NDS2_MRR_USER.PAC$MVIEW_MANAGER
as
STATUS_IN_PROCESS constant number(1) := 1;
STATUS_READY constant number(1) := 2;

DECL_SCOPE_NAME constant varchar2(32) := 'DECLARATIONS';
DECL_INSPECT_NAME constant varchar2(32) := 'MV$INSPECTOR_DECLARATION';
DECL_PARAMS_INSPECT_NAME constant varchar2(32) := 'MV$INSPECTOR_DECL_PARAMS';
DECL_ANALYTIC_NAME constant varchar2(32) := 'MV$DECLARATION_HISTORY';
DECL_JRNL_NAME constant varchar2(32) := 'V$ASK_DECLANDJRNL';

procedure log(p_msg varchar2, p_type number)
as
pragma autonomous_transaction;
begin

  dbms_output.put_line(p_type||'-'||p_msg);

  insert into NDS2_MRR_USER.SYSTEM_LOG(ID, TYPE, SITE, ENTITY_ID, MESSAGE_CODE, MESSAGE, LOG_DATE)
  values(Seq_Sys_Log_Id.Nextval, p_type, 'PAC$MVIEW_MANAGER', null, 1, p_msg, sysdate);
  commit;
end;

procedure log_notification(p_msg varchar2)
as
begin
     log(p_msg, 0);
end;

procedure log_error(p_msg varchar2)
as
begin
     log(p_msg, 3);
end;

function GET_STATUS(v_name varchar2) return number
as
v_res number(1);
begin
  select max(STATUS) into v_res from MVIEW_UPDATE_STATUS where SCOPE_NAME = v_name;

  return v_res;
end;

--��������� � ������ "� ������"
procedure SET_IN_PROCESS(v_name varchar2)
as
pragma autonomous_transaction;
begin
  update MVIEW_UPDATE_STATUS set STATUS = STATUS_IN_PROCESS where SCOPE_NAME = v_name;
  commit;
end;

--��������� � ������ "������"
procedure SET_READY(v_name varchar2)
as
pragma autonomous_transaction;
begin
  update MVIEW_UPDATE_STATUS set STATUS = STATUS_READY where SCOPE_NAME = v_name;
  commit;
end;



procedure REFRESH_DECLARATONS
as
v_oldValue number;
v_newValue number;
v_hasDiff boolean;
v_execStatus number;
begin
  v_execStatus := GET_STATUS(DECL_SCOPE_NAME);
  
  if v_execStatus = STATUS_READY then
  begin
    SET_IN_PROCESS(DECL_SCOPE_NAME);

    /*select nvl(t.value, 0) into v_oldValue from MVIEW_CONDITIONS t where t.id = 2;
    select nvl(max(sd.id), 0) into v_newValue from sov_discrepancy sd;
    v_hasDiff := v_newValue > v_oldValue;
    update MVIEW_CONDITIONS t set t.value = v_newValue, t.timestamp = sysdate where t.id = 2;
    commit;

    if v_hasDiff then
    begin
      log_notification('REFRESH_DECLARATONS: UPDATE PARAMS');
  	  PAC$REMARKABLE_CHANGES.CALC();
      dbms_mview.refresh(DECL_PARAMS_INSPECT_NAME, 'C');
    end;
    end if;*/

    select nvl(t.value, 0) into v_oldValue from MVIEW_CONDITIONS t where t.id = 1;
    select nvl(max(sd.id), 0) into v_newValue from seod_declaration sd;
    v_hasDiff := v_newValue > v_oldValue;
    update MVIEW_CONDITIONS t set t.value = v_newValue, t.timestamp = sysdate where t.id = 1;

    if not v_hasDiff then
    begin
      select nvl(t.value, 0) into v_oldValue from MVIEW_CONDITIONS t where t.id = 3;
      select nvl(max(id), 0) into v_newValue from v$askzipfajl where status = 1;
      v_hasDiff := v_newValue > v_oldValue;
      update MVIEW_CONDITIONS t set t.value = v_newValue, t.timestamp = sysdate where t.id = 3;
    end;
    end if;
    
    commit;

    if v_hasDiff then
    begin
      log_notification('REFRESH_DECLARATONS: UPDATE');
      dbms_mview.refresh(DECL_JRNL_NAME, 'C');
      dbms_mview.refresh(DECL_ANALYTIC_NAME, 'C');
      dbms_mview.refresh(DECL_INSPECT_NAME, 'C');
    end;
    end if;
    SET_READY(DECL_SCOPE_NAME);
    exception when others then
      log_error('REFRESH_DECLARATONS:('||sqlcode||'):'||sqlerrm);
      SET_READY(DECL_SCOPE_NAME);
  end;
  end if;

end;
end;
/
