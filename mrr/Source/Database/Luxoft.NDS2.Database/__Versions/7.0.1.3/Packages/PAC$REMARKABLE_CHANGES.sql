create or replace package NDS2_MRR_USER.PAC$REMARKABLE_CHANGES is
  procedure CALC;
  procedure RESET_MARK (P_ID NDS2_MRR_USER.SEOD_DECLARATION.NDS2_ID%TYPE);
end PAC$REMARKABLE_CHANGES;
/
create or replace package body NDS2_MRR_USER.PAC$REMARKABLE_CHANGES is
  procedure CALC
  as
  begin
	  merge into NDS2_MRR_USER.REMARKABLE_CHANGES trg
	  using ( select
		  sd.id as discrepancy_id
		  ,inv1.row_key as side1_rk
		  ,inv2.row_key as side2_rk
		  ,sd.stage as stage
		  ,sd.status as status
		  ,doc1.status as side1_doc_status 
		  ,doc2.status as side2_doc_status 
		  ,case
			 when sd.status = 2 and sd.stage in (2,3,4,5,6) then 11                                                    -- �
			 when sd.stage = 2 and sd.status = 1 and doc1.status = 1 then 1                                            -- �1
			 when sd.stage = 2 and sd.status = 1 and doc1.status = 4 then 2                                            -- ��1
			 when sd.stage = 2 and sd.status = 1 and doc1.status = 5 and repl1.status_id = 3 then 3                    -- ��2
			 when sd.stage = 2 and sd.status = 1 and doc1.status = 6 and doc1.status_date >= doc1.deadline_date then 4 -- ��2
			 when sd.stage = 2 and sd.status = 1 and doc1.status = 6 /*���������� ��������*/ then 5                    -- ��4
			 when sd.stage = 4 and sd.status = 1 and doc1.status = 1 then 6                                            -- ��1
			 when sd.stage = 4 and sd.status = 1 and doc1.status = 4 then 7                                            -- �2
			 when sd.stage = 4 and sd.status = 1 and doc1.status = 5 and repl1.status_id = 3 then 8                    -- ��2
			 when sd.stage = 4 and sd.status = 1 and doc1.status = 10 and
			   (doc1.status_date >= doc1.deadline_date or doc1.status_date >= doc1.tax_payer_reply_date) then 9        -- ��3
			 when sd.stage = 6 and sd.status = 1 then 10                                                               -- �3
			 else null
		   end as side1_d_mark -- ������� 1, ���������� ���
		  ,case
			 when sd.status = 2 and sd.stage in (3, 4, 5, 6) then 11                                                   -- �
			 when sd.stage = 3 and sd.status = 1 and doc2.status = 1 then 2                                            -- �1
			 when sd.stage = 3 and sd.status = 1 and doc2.status = 4 then 3                                            -- ��1
			 when sd.stage = 3 and sd.status = 1 and doc2.status = 4 and repl2.status_id = 3 then 3                    -- ��2
			 when sd.stage = 3 and sd.status = 1 and doc2.status = 6 and doc2.status_date >= doc2.deadline_date then 4 -- ��3
			 when sd.stage = 3 and sd.status = 1 and doc2.status = 6 /*���������� ��������*/ then 5                    -- ��4
			 when sd.stage = 5 and sd.status = 1 and doc2.status = 1 then 6                                            -- ��1
			 when sd.stage = 5 and sd.status = 1 and doc2.status = 4 then 7                                            -- �2
			 when sd.stage = 5 and sd.status = 1 and doc2.status = 5 and repl2.status_id = 3 then 8                    -- ��2
			 when sd.stage = 5 and sd.status = 1 and doc2.status = 10 and
			   (doc2.status_date >= doc2.deadline_date or doc2.status_date >= doc2.tax_payer_reply_date) then 9        -- ��3
			 when sd.stage = 6 and sd.status = 1 then 10                                                               -- �3
			 else null
		   end as side2_d_mark -- ������� 2, ���������� ���
		  ,case
			 when sd.status = 2 then 11                                      -- �
			 when sd.status = 1 and sd.stage = 6 then 10                     -- �3
			 when sd.status = 1 and sd.stage = 4 and doc1.status = 4 then 7  -- �2
			 else null
		   end as side1_j_mark
		  ,case
			 when sd.status = 2 and sd.stage in (4, 6) then 11                                     -- �
			 when sd.status = 1 and sd.stage = 6 then 10                                           -- �3
			 when sd.status = 1 and sd.stage = 4 and doc1.doc_type = 3 and doc1.status <> 1 then 7 -- �2
			 else null
		   end as side2_j_mark
		from sov_discrepancy sd
		inner join stage_invoice inv1 on inv1.row_key = sd.invoice_rk
		left  join stage_invoice inv2 on inv2.row_key = sd.invoice_contractor_rk
		left  join v$doc doc1 on doc1.REF_ENTITY_ID = inv1.declaration_version_id
		left  join v$doc doc2 on doc2.REF_ENTITY_ID = inv2.declaration_version_id
		left join seod_explain_reply repl1 on repl1.doc_id = doc1.doc_id
		left join seod_explain_reply repl2 on repl2.doc_id = doc2.doc_id
	  ) src
	  on (trg.discrepancy_id = src.discrepancy_id)
	  when matched then update set
		trg.stage = src.stage,
		trg.status = src.status,
		trg.side1_doc_status = src.side1_doc_status,
		trg.side2_doc_status = src.side2_doc_status,
		trg.side1_d_mark = src.side1_d_mark,
		trg.side2_d_mark = src.side2_d_mark,
		trg.side1_j_mark = src.side1_j_mark,
		trg.side2_j_mark = src.side2_j_mark
	  when not matched then insert (
		trg.discrepancy_id,
		trg.side1_rk,
		trg.side2_rk,
		trg.stage,
		trg.status,
		trg.side1_doc_status,
		trg.side2_doc_status,
		trg.side1_d_mark,
		trg.side2_d_mark,
		trg.side1_j_mark,
		trg.side2_j_mark
		) values (
		src.discrepancy_id,
		src.side1_rk,
		src.side2_rk,
		src.stage,
		src.status,
		src.side1_doc_status,
		src.side2_doc_status,
		src.side1_d_mark,
		src.side2_d_mark,
		src.side1_j_mark,
		src.side2_j_mark
		);
	  commit;
  exception when others then
      rollback;
      Nds2$sys.LOG_ERROR('PAC$REMARKABLE_CHANGES.CALC', null, sqlcode, sqlerrm);
  end;
  
  procedure RESET_MARK (P_ID NDS2_MRR_USER.SEOD_DECLARATION.NDS2_ID%TYPE)
  as
  begin
    merge into NDS2_MRR_USER.DROPPED_MARKS trg
	  using (select P_ID as ID from dual) src
	  on (trg.seod_decl_id = src.id)
	  when not matched then
    insert (trg.seod_decl_id, trg.dropped, trg.drop_date)
    values (p_id, 1, sysdate);
    commit;
  exception when others then
    rollback;
    Nds2$sys.LOG_ERROR('PAC$REMARKABLE_CHANGES.RESET_MARK', null, sqlcode, sqlerrm);
  end;
end PAC$REMARKABLE_CHANGES;
/
