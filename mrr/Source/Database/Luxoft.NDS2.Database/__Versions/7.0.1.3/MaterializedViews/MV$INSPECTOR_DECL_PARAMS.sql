create materialized view NDS2_MRR_USER.MV$INSPECTOR_DECL_PARAMS
build DEFERRED 
refresh force on demand
as
select
  t.DECLARATION_VERSION_ID,
  t.CHAPTER,
  COUNT(1) AS CONTRAGENT_CNT,
  case when SUM(case when t.mark is not null then 1 else 0 end) > 1 then 22 else MAX(t.mark) end as MARK,
  SUM(case when t.GAPS = 0 then 0 else 1 end) AS GAP_CONTRAGENT_CNT,
  SUM(t.GAPS) as GAP_CNT,
  SUM(t.GAPS_AMT) as GAP_AMNT,
  SUM(case when t.OTHRS = 0 then 0 else 1 end) AS OTHER_CONTRAGENT_CNT,
  SUM(t.OTHRS) as OTHER_CNT,
  SUM(t.OTHRS_AMT) as OTHER_AMNT
from (
  select
    inv.DECLARATION_VERSION_ID,
    inv.chapter,
    inv.seller_inn,
    inv.seller_kpp,
    case
      when vdecl.type = 0 then case when SUM(case when vrc.side2_d_mark is not null then 1 else 0 end) > 1 then 22 else MAX(vrc.side2_d_mark) end
      when vdecl.type = 1 then case when SUM(case when vrc.side2_j_mark is not null then 1 else 0 end) > 1 then 22 else MAX(vrc.side2_j_mark) end
    end as MARK,
    SUM(case ds.type when 1 then 1 else 0 end) as GAPS,
    SUM(case when ds.type=1 then 0 when ds.type IS NULL then 0 else 1 end) as OTHRS,
    SUM(case ds.type when 1 then ds.amnt else 0 end) as GAPS_AMT,
    SUM(case when ds.type=1 then 0 when ds.type IS NULL then 0 else ds.amnt end) as OTHRS_AMT
  from nds2_mrr_user.stage_invoice inv
  left join nds2_mrr_user.sov_discrepancy ds on ds.invoice_rk = inv.row_key
  left join nds2_mrr_user.remarkable_changes vrc on vrc.side2_rk = inv.row_key
  left join nds2_mrr_user.v$ask_declandjrnl vdecl on vdecl.zip = inv.declaration_version_id
  group by inv.DECLARATION_VERSION_ID, inv.seller_inn, inv.seller_kpp, inv.chapter, vdecl.type
) t
group by t.DECLARATION_VERSION_ID, t.CHAPTER;
