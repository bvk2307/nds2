drop view NDS2_MRR_USER.V$DECLARATIONINSPECT;
create or replace force view NDS2_MRR_USER.V$INSPECTOR_DECLARATION as
select
       mdi.ID,
	     mdi.nds2_decl_corr_id,
       mdi.DECLARATION_VERSION_ID
       ,null as RECORD_MARK,
       mdi.DECL_TYPE_CODE,
       mdi.DECL_TYPE,
       mdi.PROCESSINGSTAGE,
       case when mdi.Processingstage > 0 then 2
            when mdi.processingstage = 0 and mdi.max_processing_stage > 0 then 1
            else 0 end as LOAD_MARK,
       mdi.STATUS_KNP,
       case when sovd.discrep_total_count > 0 then '����' else '���' end as STATUS,
       mdi.IS_ACTIVE,
       mdi.SUR_CODE,
       mdi.INN,
       mdi.KPP,
       mdi.NAME,
       mdi.SEOD_DECL_ID,
       mdi.TAX_PERIOD,
       mdi.FISCAL_YEAR,
       mdi.FULL_TAX_PERIOD,
       mdi.DECL_DATE,
       mdi.CORRECTION_NUMBER,
       mdi.COMPENSATION_AMNT_SIGN,
       mdi.NDS_AMOUNT,
       null as CHAPTER8_MARK,
       mdi.ch8_deals_cnt_total as CHAPTER8_CONTRAGENT_CNT,
       null as CHAPTER8_GAP_CONTRAGENT_CNT,
       null as CHAPTER8_GAP_CNT,
       null as CHAPTER8_GAP_AMNT,
       null as CHAPTER8_OTHER_CONTRAGENT_CNT,
       null as CHAPTER8_OTHER_CNT,
       null as CHAPTER8_OTHER_AMNT,
       null as CHAPTER9_MARK,
       mdi.ch9_deals_cnt_total as CHAPTER9_CONTRAGENT_CNT,
       null as CHAPTER9_GAP_CONTRAGENT_CNT,
       null as CHAPTER9_GAP_CNT,
       null as CHAPTER9_GAP_AMNT,
       null as CHAPTER9_OTHER_CONTRAGENT_CNT,
       null as CHAPTER9_OTHER_CNT,
       null as CHAPTER9_OTHER_AMNT,
       null as CHAPTER10_MARK,
       mdi.ch10_deals_cnt_total as CHAPTER10_CONTRAGENT_CNT,
       null as CHAPTER10_GAP_CONTRAGENT_CNT,
       null as CHAPTER10_GAP_CNT,
       null as CHAPTER10_GAP_AMNT,
       null as CHAPTER10_OTHER_CONTRAGENT_CNT,
       null as CHAPTER10_OTHER_CNT,
       null as CHAPTER10_OTHER_AMNT,
       null as CHAPTER11_MARK,
       mdi.ch11_deals_cnt_total as CHAPTER11_CONTRAGENT_CNT,
       null as CHAPTER11_GAP_CONTRAGENT_CNT,
       null as CHAPTER11_GAP_CNT,
       null as CHAPTER11_GAP_AMNT,
       null as CHAPTER11_OTHER_CONTRAGENT_CNT,
       null as CHAPTER11_OTHER_CNT,
       null as CHAPTER11_OTHER_AMNT,
       null as CHAPTER12_MARK,
       mdi.ch12_deals_cnt_total as CHAPTER12_CONTRAGENT_CNT,
       null as CHAPTER12_GAP_CONTRAGENT_CNT,
       null as CHAPTER12_GAP_CNT,
       null as CHAPTER12_GAP_AMNT,
       null as CHAPTER12_OTHER_CONTRAGENT_CNT,
       null as CHAPTER12_OTHER_CNT,
       null as CHAPTER12_OTHER_AMNT,
       mdi.SOUN_CODE,
       mdi.REGION_CODE
       ,do.inspector_name as INSPECTOR
       ,do.inspector_sid as INSPECTOR_SID
from MV$INSPECTOR_DECLARATION mdi
left join nds2_mrr_user.DECLARATION_OWNER do on do.declaration_id = mdi.id
left join sov_declaration_info sovd on sovd.declaration_version_id = mdi.declaration_version_id
/*left join mv$inspector_decl_params chapter8 on chapter8.declaration_version_id = mdi.declaration_version_id and chapter8.chapter=8
left join mv$inspector_decl_params chapter9 on chapter9.declaration_version_id = mdi.declaration_version_id and chapter9.chapter=9
left join mv$inspector_decl_params chapter10 on chapter10.declaration_version_id = mdi.declaration_version_id and chapter10.chapter=10
left join mv$inspector_decl_params chapter11 on chapter11.declaration_version_id = mdi.declaration_version_id and chapter11.chapter=11
left join mv$inspector_decl_params chapter12 on chapter12.declaration_version_id = mdi.declaration_version_id and chapter12.chapter=12
left join dict_remarkable_changes mark8 on mark8.id = chapter8.mark
left join dict_remarkable_changes mark9 on mark9.id = chapter9.mark
left join dict_remarkable_changes mark10 on mark10.id = chapter10.mark
left join dict_remarkable_changes mark11 on mark11.id = chapter11.mark
left join dict_remarkable_changes mark12 on mark12.id = chapter12.mark*/
left join dropped_marks dm on dm.seod_decl_id = mdi.id;

