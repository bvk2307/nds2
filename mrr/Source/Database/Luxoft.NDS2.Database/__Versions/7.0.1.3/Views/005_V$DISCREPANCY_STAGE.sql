﻿create or replace force view NDS2_MRR_USER.V$DISCREPANCY_STAGE AS
select 
  dis_ref.discrepancy_id as ID,
  dis_ref.request_id as RQ_ID,
  dis.stage     as STAGE_CODE,
  dis_ref.is_in_process as IS_CHECKED,
  disstage.name as STAGE_NAME,
  case
    when dis.stage = 1 then docStage1.status
    else queue.status_id
  end as STAGE_STATUS_CODE,
  case
    when dis.stage = 1 then
      replace(
        replace(
          replace(docStage1.description, 'Черновик', 'В работе'),
          'На согласовании', 'В работе'),
        'На доработке', 'В работе')
    else queue.status_name
  end as STAGE_STATUS_NAME
from SELECTION_DISCREPANCY dis_ref
inner join SOV_DISCREPANCY dis on dis.id = dis_ref.discrepancy_id
inner join DICT_DISCREPANCY_STAGE disstage on disstage.id = NVL(dis.stage, 1)
left join
(
  select base.dis_id as dis_id, selstat.id as status, selstat.name as DESCRIPTION
  from
  (
    select
	  dis_ref.discrepancy_id as dis_id,
	  max(case dis_ref.is_in_process when 1 then selstat.rank else 50 end) as max_rank
    from selection sel
    inner join SELECTION_DISCREPANCY dis_ref on sel.Request_Id = dis_ref.request_id
    inner join DICT_SELECTION_STATUS selstat on selstat.id = sel.status
    where selstat.id not in (5, 7)  -- В случае, если выборка удалена или отклонена, расхождение считается не включённым в данную выборку
    group by dis_ref.discrepancy_id
  ) base
  inner join DICT_SELECTION_STATUS selstat on selstat.rank = base.MAX_RANK
) docStage1 on docStage1.dis_id = dis.id
left join
(
  select distinct queue.discrepancy_id, queue.for_stage, doc_status.id as status_id, doc_status.description as status_name
  from seod_data_queue queue
  inner join doc on doc.doc_id = queue.ref_doc_id
  inner join doc_status on doc_status.id = doc.status and doc_status.doc_type = doc.doc_type
  inner join
  (
    select q.declaration_reg_num, q.discrepancy_id, max(q.for_stage) stage
    from seod_data_queue q
    group by q.declaration_reg_num, q.discrepancy_id
  ) max_stage on max_stage.declaration_reg_num = queue.declaration_reg_num
      and max_stage.discrepancy_id = queue.discrepancy_id
      and max_stage.stage = queue.for_stage  
) queue on queue.discrepancy_id = dis.id and queue.for_stage = dis.stage;