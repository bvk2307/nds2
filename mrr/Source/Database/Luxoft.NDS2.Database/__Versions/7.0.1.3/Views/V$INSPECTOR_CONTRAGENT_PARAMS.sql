drop view NDS2_MRR_USER.V$CONTRAGENT_PARAMS;
-- ��������� ������������
create or replace force view NDS2_MRR_USER.V$INSPECTOR_CONTRAGENT_PARAMS as
select
  mark_name.name as MARK
  ,params.*
from (
select
  dis.status as DIS_STATUS
  ,decl.DECLARATION_VERSION_ID
  ,decl.SUR_CODE
  ,case
     when decl.DECL_TYPE_CODE = 0 and mark.side1_rk = inv.row_key then mark.side1_d_mark
     when decl.DECL_TYPE_CODE = 0 and mark.side2_rk = inv.row_key then mark.side2_d_mark
     when decl.DECL_TYPE_CODE = 1 and mark.side1_rk = inv.row_key then mark.side1_j_mark
     when decl.DECL_TYPE_CODE = 1 and mark.side2_rk = inv.row_key then mark.side2_j_mark
     else null
   end as MARK_CODE
  ,decl.DECL_TYPE as DOC_TYPE
  ,decl.DECL_TYPE_CODE as DOC_TYPE_CODE
  ,inv.row_key
  ,inv.chapter
  ,case 
     when inv.chapter = 8 then inv.buyer_inn
     when inv.chapter = 9 then inv.seller_inn
     when inv.chapter = 10 then inv.buyer_inn
     when inv.chapter = 11 then inv.seller_inn
     when inv.chapter = 12 then inv.seller_inn
   end as inn
  ,decl.inn as CONTRACTOR_INN
  ,decl.kpp as CONTRACTOR_KPP
  ,decl.name as CONTRACTOR_NAME
  ,inv.invoice_num
  ,inv.invoice_date
  ,inv.price_buy_amount as AMOUNT_TOTAL
  ,inv.price_buy_nds_amount as AMOUNT_NDS
  ,dis.type as DIS_TYPE
  ,dis_type.s_name as DIS_TYPE_NAME
  ,dis.id   as DIS_ID
  ,dis.amnt as DIS_AMOUNT
  ,dis_stage.name as DIS_STAGE_NAME
  ,dis_status.name as DIS_STATUS_NAME
from stage_invoice inv
join SOV_DISCREPANCY dis on dis.invoice_rk = inv.row_key and dis.status = 1
left join MV$INSPECTOR_DECLARATION decl on decl.id = dis.decl_contractor_id
join dict_discrepancy_type dis_type on dis_type.s_code = dis.type
join dict_discrepancy_status dis_status on dis_status.id = dis.status
join dict_discrepancy_stage dis_stage on dis_stage.id = dis.stage
left join remarkable_changes mark on mark.side1_rk = inv.row_key or mark.side2_rk = inv.row_key
) params
left join dict_remarkable_changes mark_name on mark_name.id = params.mark_code;
