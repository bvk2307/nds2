﻿create or replace force view NDS2_MRR_USER.V$BSSCHET_IP
as
select
 INN,
 BIK,
 NAMEKO,
 NOMSCH,
 PRVAL,
 case PRVAL when  '0' then 'рубли' else 'валюта' end as PRVALTEXT,
 DATEOPENSCH
from BSSCHET_IP
;
