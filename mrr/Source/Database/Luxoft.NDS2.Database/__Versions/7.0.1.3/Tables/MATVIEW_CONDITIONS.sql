begin
  NDS2_MRR_USER.NDS2$SYS.DROP_IF_EXISTS('NDS2_MRR_USER', 'MVIEW_CONDITIONS');
end;
/

create table NDS2_MRR_USER.MVIEW_CONDITIONS
(
  ID number,
  DESCRIPTION varchar2(50),
  VALUE number,
  TIMESTAMP date,
  constraint MVIEW_CONDITIONS_PK primary key (ID)
);
insert into NDS2_MRR_USER.MVIEW_CONDITIONS (ID, DESCRIPTION, VALUE, TIMESTAMP) values (1, 'seod_declaration max id', 0, sysdate);
insert into NDS2_MRR_USER.MVIEW_CONDITIONS (ID, DESCRIPTION, VALUE, TIMESTAMP) values (2, 'sov_discrepancy max id', 0, sysdate);
insert into NDS2_MRR_USER.MVIEW_CONDITIONS (ID, DESCRIPTION, VALUE, TIMESTAMP) values (3, 'v$ask_declandjrnl max zip', 0, sysdate);
commit;
