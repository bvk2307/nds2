CREATE TABLE "NDS2_MRR_USER"."SOV_DISCREPANCY_ALL" 
(
-- 4.5.4.3	Выгружаемые данные:
				"SOV_ID" VARCHAR2(2051 CHAR) NOT NULL ENABLE, -- Идентификатор расхождения
				"STATUS" NUMBER(2,0), -- Статус расхождения
				"TAX_PERIOD" VARCHAR2(2 CHAR), -- Налоговый период
				"TAX_YEAR" VARCHAR2(4 CHAR), -- Отчётный год
				"TYPE" NUMBER(1,0), -- Тип расхождения
				"CREATE_DATE" DATE, -- Дата создания расхождения
				"RULE_GROUP" NUMBER(5,0), -- Группа правила, по которому произошло сопоставление
				"DEAL_AMNT" NUMBER(20,2), -- Сумма сделки
				"AMNT" NUMBER(20,2), -- Сумма расхождения
				-- Код валюты
				-- Курс валюты к рублю минимальный
				-- Курс валюты к рублю максимальный
				"BUYER_INN" VARCHAR2(12 CHAR), -- ИНН стороны, получившей СФ
				"SELLER_INN" VARCHAR2(12 CHAR), -- ИНН стороны, выставившей СФ
				"SIDE_PRIMARY_PROCESSING" NUMBER(1,0), -- Сторона, которой предъявлено расхождение
				-- Номер СФ 
				-- Дата СФ
				"INVOICE_CHAPTER" NUMBER(2,0), -- Номер раздела, в котором содержится сопоставляемая запись о СФ
				"INVOICE_RK" VARCHAR2(128 BYTE), -- Идентификатор сопоставляемой записи о СФ
				"INVOICE_CONTRACTOR_RK" VARCHAR2(128 BYTE), -- Идентификатор сопоставленной записи о СФ
				"INVOICE_CONTRACTOR_CHAPTER" NUMBER(2,0), 
-- 4.5.4.2.1	ПАРАМЕТРЫ ФИЛЬТРА
				"REGION" VARCHAR2(2 CHAR), -- Код региона
				"IFNS" NUMBER(4,0), -- Код ИФНС
				"INN" VARCHAR2(12 CHAR), -- ИНН первичной стороны обработки
				"NP_NAME" VARCHAR2(1000 CHAR), -- Наименование НП (стороны первичной отработки)
				"ND_PRIS" NUMBER(1,0), -- Признак НД
				"MISMATCH_ND_SUM" NUMBER(20,2), -- Общая сумма расхождений в НД, в которую входит расхождение
				"MISMATCH_MIN_SUM" NUMBER(20,2), -- Минимальная сумма расхождения в НД
				"MISMATCH_MAX_SUM" NUMBER(20,2), -- Максимальная сумма расхождения в НД
				"MISMATCH_AVG_SUM" NUMBER(20,2), -- Средняя сумма расхождения в НД
-- 4.4.9.2.1	ПАРАМЕТРЫ ФИЛЬТРА
				"SUR_CODE" VARCHAR2(1 CHAR), -- Показатель СУР НП
				"ND_SUBMISSION_DATE" DATE, -- Дата подачи НД
				"MISMATCH_COUNT" NUMBER(10), -- Общее количество расхождений в НД
				-- Общая сумма ПВП расхождений в НД
				-- Минимальное значение ПВП расхождения в НД
				-- Максимальное значение ПВП расхождения в НД
				-- Сумма расхождений по книге покупок 
				-- Сумма расхождений по книге продаж
				-- Сумма ПВП расхождений по книге покупок
				-- Сумма ПВП расхождений по книге продаж
				"AMOUNT_PVP" NUMBER(20,2), -- ПВП расхождения
-- 4.2.7.2	Параметры фильтра
				"RULE_NUM" NUMBER(3,0), -- Номер правила сопоставления
				-- Показатель СУР второй стороны
-- 4.4.2.1.1	ФИЛЬТРЫ ДЛЯ ФОРМИРОВАНИЯ АВТОВЫБОРОК
				"BUYER_KPP" VARCHAR2(9 CHAR), -- КПП покупателя
				-- КПП продавца
				-- Код вида операции
"COMPARE_KIND" NUMBER(1,0),  -- Вид расхождения
-- not in use "COURSE" NUMBER(20,2), 
-- not in use "COURSE_COST" NUMBER(20,2), 
"DECL_ID" NUMBER, 
"DECL_CONTRACTOR_ID" NUMBER, 
"STAGE" NUMBER DEFAULT 1,
CONSTRAINT "PK_SOV_DISCREPANCY_ALL" PRIMARY KEY ("SOV_ID")
);
