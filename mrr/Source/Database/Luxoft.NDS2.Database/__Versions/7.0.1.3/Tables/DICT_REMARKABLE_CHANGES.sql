 CREATE TABLE NDS2_MRR_USER.DICT_REMARKABLE_CHANGES 
   (
   	ID NUMBER, 
	NAME VARCHAR2(5),
	DESCRIPTION VARCHAR2(200),
	 CONSTRAINT "DICT_REMARKABLE_CHANGES" PRIMARY KEY ("ID")
    ) ;

INSERT INTO NDS2_MRR_USER.DICT_REMARKABLE_CHANGES (ID, NAME, DESCRIPTION) VALUES (1,  '�1',  '����� ����������� ��� ���');
INSERT INTO NDS2_MRR_USER.DICT_REMARKABLE_CHANGES (ID, NAME, DESCRIPTION) VALUES (2,  '��1', '��������������. ���������� ��');
INSERT INTO NDS2_MRR_USER.DICT_REMARKABLE_CHANGES (ID, NAME, DESCRIPTION) VALUES (3,  '��2', '��������������. ��������� ��������. ����������� �� ���������');
INSERT INTO NDS2_MRR_USER.DICT_REMARKABLE_CHANGES (ID, NAME, DESCRIPTION) VALUES (4,  '��3', '��������������. ��������� �� ��������. ����������� �� ���������');
INSERT INTO NDS2_MRR_USER.DICT_REMARKABLE_CHANGES (ID, NAME, DESCRIPTION) VALUES (5,  '��4', '��������������. �������� ���������� ��. ����������� �� ���������');
INSERT INTO NDS2_MRR_USER.DICT_REMARKABLE_CHANGES (ID, NAME, DESCRIPTION) VALUES (6,  '��1', '����������������. �������');
INSERT INTO NDS2_MRR_USER.DICT_REMARKABLE_CHANGES (ID, NAME, DESCRIPTION) VALUES (7,  '�2',  '����������������. ���������� ��');
INSERT INTO NDS2_MRR_USER.DICT_REMARKABLE_CHANGES (ID, NAME, DESCRIPTION) VALUES (8,  '��2', '����������������. ������� �����. ����������� �� ���������');
INSERT INTO NDS2_MRR_USER.DICT_REMARKABLE_CHANGES (ID, NAME, DESCRIPTION) VALUES (9,  '��3', '����������������. ����� �� �������');
INSERT INTO NDS2_MRR_USER.DICT_REMARKABLE_CHANGES (ID, NAME, DESCRIPTION) VALUES (10, '�3',  '������ ���. ����������� �� ���������');
INSERT INTO NDS2_MRR_USER.DICT_REMARKABLE_CHANGES (ID, NAME, DESCRIPTION) VALUES (11, '�',   '����������� ��� ��� ���������');
INSERT INTO NDS2_MRR_USER.DICT_REMARKABLE_CHANGES (ID, NAME, DESCRIPTION) VALUES (21, '�',   '���� ���������. ���������� ���������� �����������');
COMMIT;
