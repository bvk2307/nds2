﻿-- Create table
create table NDS2_MRR_USER.CFG_EXPORT_INVOICE
(
  ID number not null,
  INVOICE_MAX_COUNT number not null,
  PAGE_SIZE number not null,
  DELAY_BETWEEN_PAGE number not null
)
;
-- Create/Recreate primary, unique and foreign key constraints 
alter table NDS2_MRR_USER.CFG_EXPORT_INVOICE
  add constraint PK_CFG_EXPORT_INVOICE primary key (ID);