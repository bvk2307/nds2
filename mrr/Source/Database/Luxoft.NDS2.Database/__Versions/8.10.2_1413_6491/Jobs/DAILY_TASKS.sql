﻿begin
  sys.dbms_scheduler.set_attribute(name => 'NDS2_MRR_USER.P$DAILY_TASK_FINISH', attribute => 'program_action', value => 'BEGIN
  	  PAC$LOGICAL_LOCK.P$RELEASE_LOCK(1, ''DAILY_JOB'');
      nds2$sys.recompile_invalid_objects;
      NDS2$SYS.LOG_INFO(''BACKGROUND_DAILY_TASKS'', 0, 0, ''P$DAILY_TASK_FINISH'');
  END;');
end;
/
