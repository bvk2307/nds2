begin
  for line in (select 1 from all_tables where owner = 'NDS2_MRR_USER' and table_name = 'QUERY_ROWCOUNT_CACHE') loop
    begin
    execute immediate 'drop table NDS2_MRR_USER.QUERY_ROWCOUNT_CACHE purge';
    exception when others then null;
    end;
  end loop;
end;
/

create table NDS2_MRR_USER.QUERY_ROWCOUNT_CACHE
(
 scope_id number,
 query_hash_code number,
 rows_count number,
 elasped_ms number,
 arguments clob
)
compress
partition by list (scope_id)
(
PARTITION qrc_decl_analyst VALUES (1), 
PARTITION qrc_decl_inspect VALUES (2)
);
