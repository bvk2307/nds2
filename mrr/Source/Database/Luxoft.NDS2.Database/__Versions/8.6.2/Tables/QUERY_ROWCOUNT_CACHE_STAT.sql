
begin
  for line in (select 1 from all_tables where owner = 'NDS2_MRR_USER' and table_name = 'QUERY_ROWCOUNT_CACHE_STAT') loop
    begin
    execute immediate 'drop table NDS2_MRR_USER.QUERY_ROWCOUNT_CACHE_STAT purge';
    exception when others then null;
    end;
  end loop;
end;
/


create table NDS2_MRR_USER.QUERY_ROWCOUNT_CACHE_STAT
(
 scope_id number,
 query_hash_code number,
 dt date
);
