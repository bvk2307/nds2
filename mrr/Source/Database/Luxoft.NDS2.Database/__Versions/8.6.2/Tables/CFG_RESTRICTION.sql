﻿alter table NDS2_MRR_USER.CFG_RESTRICTION RENAME TO CFG_RESTRICTION_CHAINS;

create table NDS2_MRR_USER.CFG_RESTRICTION_EXPORTEXCEL
(
  ROLE_ID number not null,
  GROUP_ID number,
  NOT_RESTRICTED number not null,
  ALLOW_PURCHASE number not null,
  DESCRIPTION varchar(1024) null
);
