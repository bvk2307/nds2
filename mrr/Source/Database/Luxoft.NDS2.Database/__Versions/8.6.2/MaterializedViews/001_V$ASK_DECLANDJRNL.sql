﻿drop materialized view NDS2_MRR_USER.V$ASK_DECLANDJRNL;
create materialized view NDS2_MRR_USER.V$ASK_DECLANDJRNL
refresh force on demand
as
select
"Ид" as ID,									"ZIP" as ZIP,								"ИдФайл" as IDFAJL,								"ВерсПрог" as VERSPROG,						"ВерсФорм" as VERSFORM,
0 as TYPE,									"ПризнНал8-12" as PRIZNNAL8_12,				"ПризнНал8" as PRIZNNAL8,						"ПризнНал81" as PRIZNNAL81,					"ПризнНал9" as PRIZNNAL9,
"ПризнНал91" as PRIZNNAL91,					"ПризнНал10" as PRIZNNAL10,					"ПризнНал11" as PRIZNNAL11,						"ПризнНал12" as PRIZNNAL12,					"КНД" as KND,
"ДатаДок" as DATADOK,						"Период" as PERIOD,							"ОтчетГод" as OTCHETGOD,						"КодНО" as KODNO,							to_number("НомКорр") as NOMKORR,
"ПоМесту" as POMESTU,						"ОКВЭД" as OKVED,							"Тлф" as TLF,									"НаимОрг" as NAIMORG,						"ИНННП" as INNNP,
"КППНП" as KPPNP,							"ФормРеорг" as FORMREORG,					"ИННРеорг" as INNREORG,							"КППРеорг" as KPPREORG,						"ФамилияНП" as FAMILIYANP,
"ИмяНП" as IMYANP,							"ОтчествоНП" as OTCHESTVONP,				"ПрПодп" as PRPODP,								"ФамилияПодп" as FAMILIYAPODP,				"ИмяПодп" as IMYAPODP,
"ОтчествоПодп" as OTCHESTVOPODP,			"НаимДок" as NAIMDOK,						"НаимОргПред" as NAIMORGPRED,					"ОКТМО" as OKTMO,							"КБК" as KBK,
"СумПУ173.5" as SUMPU173_5,					"СумПУ173.1" as SUMPU173_1,					"НомДогИТ" as NOMDOGIT,							"ДатаДогИТ" as DATADOGIT,					"ДатаНачДогИТ" as DATANACHDOGIT,
"ДатаКонДогИТ" as DATAKONDOGIT,				"НалПУ164" as NALPU164,						"НалВосстОбщ" as NALVOSSTOBSHH,					"РлТв18НалБаз" as REALTOV18NALBAZA,			"РеалТов18СумНал" as REALTOV18SUMNAL,
"РлТв10НалБаз" as REALTOV10NALBAZA,			"РеалТов10СумНал" as REALTOV10SUMNAL,		"РлТв118НалБаз" as REALTOV118NALBAZA,			"РлТв118СумНал" as REALTOV118SUMNAL,		"РлТв110НалБаз" as REALTOV110NALBAZA,
"РлТв110СумНал" as REALTOV110SUMNAL,		"РлПрдИКНалБаз" as REALPREDIKNALBAZA,		"РлПрдИКСумНал" as REALPREDIKSUMNAL,			"ВыпСМРСобНалБаз" as VYPSMRSOBNALBAZA,		"ВыпСМРСобСумНал" as VYPSMRSOBSUMNAL,
"ОпПрдПстНлБаз" as OPLPREDPOSTNALBAZA,		"ОплПрдПстСумНал" as OPLPREDPOSTSUMNAL,		"СумНалВс" as SUMNALVS,							"СумНал170.3.5" as SUMNAL170_3_5,			"СумНал170.3.3" as SUMNAL170_3_3,
"КорРлТв18НалБаз" as KORREALTOV18NALBAZA,	"КорРлТв18СумНал" as KORREALTOV18SUMNAL,	"КорРлТв10НалБаз" as KORREALTOV10NALBAZA,		"КорРлТв10СумНал" as KORREALTOV10SUMNAL,	"КорРлТв118НлБз" as KORREALTOV118NALBAZA,
"КорРлТв118СмНл" as KORREALTOV118SUMNAL,	"КорРлТв110НлБз" as KORREALTOV110NALBAZA,	"КорРлТв110СмНл" as KORREALTOV110SUMNAL,		"КорРлПрдИКНлБз" as KORREALPREDIKNALBAZA,	"КорРлПрдИКСмНл" as KORREALPREDIKSUMNAL,
"НалПредНППриоб" as NALPREDNPPRIOB,			"НалПредНППок" as NALPREDNPPOK,				"НалИсчСМР" as NALISCHSMR,						"НалУплТамож" as NALUPLTAMOZH,				"НалУплНОТовТС" as NALUPLNOTOVTS,
"НалИсчПрод" as NALISCHPROD,				"НалУплПокНА" as NALUPLPOKNA,				"НалВычОбщ" as NALVYCHOBSHH,					"СумИсчислИтог" as SUMISCHISLITOG,			"СумВозмПдтв" as SUMVOZMPDTV,
"СумВозмНеПдтв" as SUMVOZMNEPDTV,			"СумНал164Ит" as SUMNAL164IT,				"НалВычНеПодИт" as NALVYCHNEPODIT,				"НалИсчислИт" as NALISCHISLIT,				"СмОп1010449КдОп" as SUMOPER1010449KODOPER,
"СмОп1010449НлБз" as SUMOPER1010449NALBAZA,	"СмОп1010449КрИсч" as SUMOPER1010449KORISCH,"СмОп1010449НлВст" as SUMOPER1010449NALVOSST,	"ОплПостСв6Мес" as OPLPOSTSV6MES,			"НаимКнПок" as NAIMKNPOK,
"НаимКнПокДЛ" as NAIMKNPOKDL,				"НаимКнПрод" as NAIMKNPROD,					"НаимКнПродДЛ" as NAIMKNPRODDL,					"НаимЖУчВыстСчФ" as NAIMZHUCHVYSTSCHF,		"НаимЖУчПолучСчФ" as NAIMZHUCHPOLUCHSCHF,
"НмВстСчФ173_5" as NAIMVYSTSCHF173_5,		"ИдЗагрузка" as	IdZagruzka,					"КодОпер47" as	KodOper47,						"НалБаза47" as	NalBaza47,					"НалВосст47" as NalVosst47,
"КодОпер48"	as KodOper48,					"КорНалБазаУв48" as	KorNalBazaUv48,			"КорНалБазаУм48" as	KorNalBazaUm48,				"КодОпер50" as	KodOper50,					"КорНалБазаУв50" as KorNalBazaUv50,
"КорИсчУв50" as	KorIschUv50,				"КорНалБазаУм50" as	KorNalBazaUm50,			"КорИсчУм50" as	KorIschUm50,					"ПризнакНД" as ND_PRIZN,					"СуммаНДС" as SUMMANDS,
(row_number() over (partition by "Период", "ОтчетГод", "ИНННП", to_number("НомКорр") order by "Ид")) as NOMKORR_RANK,
case when nvl("СуммаНалИсчисл", 0) = 0 then null else round(nvl("СуммаВыч", 0) * 100 / "СуммаНалИсчисл") end as NDS_WEIGHT
from NDS2_MC."ASKДекл"
union
select
jr."Ид" as ID,						jr."ZIPЧ1" as ZIP,				jr."ИдФайл" as IDFAJL,			jr."ВерсПрог" as VERSPROG,		jr."ВерсФорм" as VERSFORM,
1 as TYPE,							null as PRIZNNAL8_12,			null as PRIZNNAL8,				null as PRIZNNAL81,				null as PRIZNNAL9,
null as PRIZNNAL91,					null as PRIZNNAL10,				null as PRIZNNAL11,				null as PRIZNNAL12,				jr."КНД" as KND,
null as DATADOK,					jr."Период" as PERIOD,			jr."ОтчетГод" as OTCHETGOD,		'5252' as KODNO,				decode((row_number() over (partition by jr."Период", jr."ОтчетГод", jr."ИНН" order by zFile."ДатаФайла" asc) - 1),1,1,0) as NOMKORR,
'116' as POMESTU,					null as OKVED,					null as TLF,					jr."НаимОрг" as NAIMORG,		jr."ИНН" as INNNP,
jr."КПП" as KPPNP,					null as FORMREORG,				null as INNREORG,				null as KPPREORG,				null as FAMILIYANP,
null as IMYANP,						null as OTCHESTVONP,			jr."ПрПодп" as PRPODP,			jr."ФамилияПодп"as FAMILIYAPODP,jr."ИмяПодп" as IMYAPODP,
jr."ОтчествоПодп" as OTCHESTVOPODP,	jr."НаимДок" as NAIMDOK,		null as NAIMORGPRED,			null as OKTMO,					null as KBK,
null as SUMPU173_5,					null as SUMPU173_1,				null as NOMDOGIT,				null as DATADOGIT,				null as DATANACHDOGIT,
null as DATAKONDOGIT,				null as NALPU164,				null as NALVOSSTOBSHH,			null as REALTOV18NALBAZA,		null as REALTOV18SUMNAL,
null as REALTOV10NALBAZA,			null as REALTOV10SUMNAL,		null as REALTOV118NALBAZA,		null as REALTOV118SUMNAL,		null as REALTOV110NALBAZA,
null as REALTOV110SUMNAL,			null as REALPREDIKNALBAZA,		null as REALPREDIKSUMNAL,		null as VYPSMRSOBNALBAZA,		null as VYPSMRSOBSUMNAL,
null as OPLPREDPOSTNALBAZA,			null as OPLPREDPOSTSUMNAL,		null as SUMNALVS,				null as SUMNAL170_3_5,			null as SUMNAL170_3_3,
null as KORREALTOV18NALBAZA,		null as KORREALTOV18SUMNAL,		null as KORREALTOV10NALBAZA,	null as KORREALTOV10SUMNAL,		null as KORREALTOV118NALBAZA,
null as KORREALTOV118SUMNAL,		null as KORREALTOV110NALBAZA,	null as KORREALTOV110SUMNAL,	null as KORREALPREDIKNALBAZA,	null as KORREALPREDIKSUMNAL,
null as NALPREDNPPRIOB,				null as NALPREDNPPOK,			null as NALISCHSMR,				null as NALUPLTAMOZH,			null as NALUPLNOTOVTS,
null as NALISCHPROD,				null as NALUPLPOKNA,			null as NALVYCHOBSHH,			null as SUMISCHISLITOG,			null as SUMVOZMPDTV,
null as SUMVOZMNEPDTV,				null as SUMNAL164IT,			null as NALVYCHNEPODIT,			null as NALISCHISLIT,			null as SUMOPER1010449KODOPER,
null as SUMOPER1010449NALBAZA,		null as SUMOPER1010449KORISCH,  null as SUMOPER1010449NALVOSST, null as OPLPOSTSV6MES,			null as NAIMKNPOK,
null as NAIMKNPOKDL,				null as NAIMKNPROD,				null as NAIMKNPRODDL,			null as NAIMZHUCHVYSTSCHF,		null as NAIMZHUCHPOLUCHSCHF,
null as NAIMVYSTSCHF173_5,			null as  IdZagruzka,			null as  KodOper47,				null as  NalBaza47,				null as NalVosst47,
null as KodOper48,					null as  KorNalBazaUv48,		null as  KorNalBazaUm48,		null as  KodOper50,				null as KorNalBazaUv50,
null as KorIschUv50,				null as  KorNalBazaUm50,		null as  KorIschUm50,			'0' as ND_PRIZN,				null as SummaNDS,
(row_number() over (partition by jr."Период", jr."ОтчетГод", jr."ИНН", zFile."ДатаФайла" order by jr."Ид")) as NOMKORR_RANK,
null as NDS_WEIGHT
from NDS2_MC."ASKЖурналУч" jr inner join NDS2_MC."ASKZIPФайл" zFile on zFile."Ид" = jr."ZIPЧ1";
