﻿/* Ежедневные задачи */

begin
  DBMS_SCHEDULER.DROP_JOB(job_name => 'NDS2_MRR_USER.J$SYNCDATA_DAILY');
  exception when others then 
    dbms_output.put_line('ERROR drop J$SYNCDATA_DAILY: '||sqlerrm);
end;
/
begin
  DBMS_SCHEDULER.DROP_JOB(job_name => 'NDS2_MRR_USER.J$REPORT_DECL_BACKGROUND');  
  exception when others then 
    dbms_output.put_line('ERROR drop J$REPORT_DECL_BACKGROUND: '||sqlerrm);
end;
/
begin
  DBMS_SCHEDULER.DROP_JOB(job_name => 'NDS2_MRR_USER.J$REFRESH_KNP_AGGREGATES');
  exception when others then 
    dbms_output.put_line('ERROR drop J$REFRESH_KNP_AGGREGATES: '||sqlerrm);
end;
/
begin
  DBMS_SCHEDULER.DROP_JOB(job_name => 'NDS2_MRR_USER.J$REFRESH_MV_DECL');
  exception when others then 
    dbms_output.put_line('ERROR drop J$REFRESH_MV_DECL: '||sqlerrm);
end;
/
begin
  DBMS_SCHEDULER.create_program(
       program_name => 'NDS2_MRR_USER.P$DICTIONARY'
      ,program_type => 'PLSQL_BLOCK'
      ,program_action => 'BEGIN
      NDS2$SYS.LOG_INFO(''BACKGROUND_DAILY_TASKS'', 0, 0, ''P$DICTIONARY'');
      PAC$SYNC.START_WORK;
      END;'
      ,number_of_arguments => 0
      ,enabled => true
      ,comments => 'Обновление справочников ФЦОД');
  DBMS_SCHEDULER.create_program(
       program_name => 'NDS2_MRR_USER.P$DECLARATION_SUMMARY_REPORT'
      ,program_type => 'PLSQL_BLOCK'
      ,program_action => 'BEGIN
      NDS2$SYS.LOG_INFO(''BACKGROUND_DAILY_TASKS'', 0, 0, ''P$DECLARATION_SUMMARY_REPORT'');
      NDS2$REPORTS.START_DECLARATION_CALCULATE;
      END;'
      ,number_of_arguments => 0
      ,enabled => true
      ,comments => 'Построение отчета фоновые показатели');
  DBMS_SCHEDULER.create_program(
       program_name => 'NDS2_MRR_USER.P$KNP_AGGREGATES'
      ,program_type => 'PLSQL_BLOCK'
      ,program_action => 'BEGIN
      NDS2$SYS.LOG_INFO(''BACKGROUND_DAILY_TASKS'', 0, 0, ''P$KNP_AGGREGATES'');
      PAC$NDS2_FULL_CYCLE.P$UPDATE_DICREPANCY_DAILY;
      END;'
      ,number_of_arguments => 0
      ,enabled => true
      ,comments => 'Пересоздание агрегатов с данными открытых расхождений для КНП');  
  DBMS_SCHEDULER.create_program(
       program_name => 'NDS2_MRR_USER.P$DECLARATION_AGGREGATES'
      ,program_type => 'PLSQL_BLOCK'
      ,program_action => 'BEGIN
      NDS2$SYS.LOG_INFO(''BACKGROUND_DAILY_TASKS'', 0, 0, ''P$DECLARATION_AGGREGATES'');
      PAC$MVIEW_MANAGER.REFRESH_DECLARATONS;
      END;'
      ,number_of_arguments => 0
      ,enabled => true
      ,comments => 'Пересоздание агрегатов с данными деклараций');
  DBMS_SCHEDULER.create_program(
       program_name => 'NDS2_MRR_USER.P$DAILY_TASK_FINISH'
      ,program_type => 'PLSQL_BLOCK'
      ,program_action => 'BEGIN
      NDS2$SYS.LOG_INFO(''BACKGROUND_DAILY_TASKS'', 0, 0, ''P$DAILY_TASK_FINISH'');
      END;'
      ,number_of_arguments => 0
      ,enabled => true
      ,comments => 'Пересоздание агрегатов с данными деклараций');
	  
  DBMS_SCHEDULER.CREATE_CHAIN(chain_name => 'NDS2_MRR_USER.BACKGROUND_DAILY_TASKS');
  DBMS_SCHEDULER.DEFINE_CHAIN_STEP(
       chain_name => 'NDS2_MRR_USER.BACKGROUND_DAILY_TASKS'
      ,step_name => 'STEP1'
      ,program_name => 'P$DICTIONARY');
  DBMS_SCHEDULER.DEFINE_CHAIN_STEP(
       chain_name => 'NDS2_MRR_USER.BACKGROUND_DAILY_TASKS'
      ,step_name => 'STEP2'
      ,program_name => 'P$DECLARATION_AGGREGATES');
  DBMS_SCHEDULER.DEFINE_CHAIN_STEP(
       chain_name => 'NDS2_MRR_USER.BACKGROUND_DAILY_TASKS'
      ,step_name => 'STEP3'
      ,program_name => 'P$KNP_AGGREGATES');
  DBMS_SCHEDULER.DEFINE_CHAIN_STEP(
       chain_name => 'NDS2_MRR_USER.BACKGROUND_DAILY_TASKS'
      ,step_name => 'STEP4'
      ,program_name => 'P$DECLARATION_SUMMARY_REPORT');
  DBMS_SCHEDULER.DEFINE_CHAIN_STEP(
       chain_name => 'NDS2_MRR_USER.BACKGROUND_DAILY_TASKS'
      ,step_name => 'STEPEND'
      ,program_name => 'P$DAILY_TASK_FINISH');
  
  DBMS_SCHEDULER.DEFINE_CHAIN_RULE(
       chain_name => 'NDS2_MRR_USER.BACKGROUND_DAILY_TASKS'
	  ,condition => 'true' 
	  ,rule_name => 'rule1'
	  ,action => 'START "STEP1"'
  );
  DBMS_SCHEDULER.DEFINE_CHAIN_RULE(
       chain_name => 'NDS2_MRR_USER.BACKGROUND_DAILY_TASKS'
	  ,condition => 'STEP1 COMPLETED' 
	  ,rule_name => 'rule2'
	  ,action => 'START "STEP2"'
  );  
  DBMS_SCHEDULER.DEFINE_CHAIN_RULE(
       chain_name => 'NDS2_MRR_USER.BACKGROUND_DAILY_TASKS'
	  ,condition => 'STEP2 COMPLETED' 
	  ,rule_name => 'rule3'
	  ,action => 'START "STEP3"'
  );  
  DBMS_SCHEDULER.DEFINE_CHAIN_RULE(
       chain_name => 'NDS2_MRR_USER.BACKGROUND_DAILY_TASKS'
	  ,condition => 'STEP3 COMPLETED' 
	  ,rule_name => 'rule4'
	  ,action => 'START "STEP4"'
  );  
  DBMS_SCHEDULER.DEFINE_CHAIN_RULE(
       chain_name => 'NDS2_MRR_USER.BACKGROUND_DAILY_TASKS'
	  ,condition => 'STEP4 COMPLETED'
	  ,rule_name => 'ruleend1'
	  ,action => 'START "STEPEND"'
  );  
  DBMS_SCHEDULER.DEFINE_CHAIN_RULE(
       chain_name => 'NDS2_MRR_USER.BACKGROUND_DAILY_TASKS'
	  ,condition => 'STEPEND COMPLETED'
	  ,rule_name => 'ruleend2'
	  ,action => 'END'
  );  
  DBMS_SCHEDULER.ENABLE('NDS2_MRR_USER.BACKGROUND_DAILY_TASKS');
end;
/
begin  
  DBMS_SCHEDULER.create_job(
       job_name => 'NDS2_MRR_USER.J$DAILY_JOB'
      ,job_type => 'CHAIN'
      ,job_action => 'NDS2_MRR_USER.BACKGROUND_DAILY_TASKS'
      ,start_date => TO_DATE(TO_CHAR(SYSDATE, 'dd/mm/yyyy') || ' 04:00:00', 'dd/mm/yyyy HH24:mi:ss') + INTERVAL '1' MINUTE
      ,repeat_interval => 'freq=Daily;Interval=1'
      ,end_date => to_date('01-01-2100 00:00:00', 'dd-mm-yyyy hh24:mi:ss')
      ,job_class => 'DEFAULT_JOB_CLASS'
      ,enabled => false
      ,auto_drop => false 
      ,comments => 'Цепь ежедневных задач');
end;
/
  
