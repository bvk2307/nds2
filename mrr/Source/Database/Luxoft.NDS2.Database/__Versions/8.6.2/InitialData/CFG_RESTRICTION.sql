truncate table NDS2_MRR_USER.CFG_RESTRICTION_EXPORTEXCEL;
insert into NDS2_MRR_USER.CFG_RESTRICTION_EXPORTEXCEL (ROLE_ID, GROUP_ID, NOT_RESTRICTED, ALLOW_PURCHASE, DESCRIPTION)
       values (1, 0, 1, 1, '�������� �� - ��� �����������');
insert into NDS2_MRR_USER.CFG_RESTRICTION_EXPORTEXCEL (ROLE_ID, GROUP_ID, NOT_RESTRICTED, ALLOW_PURCHASE, DESCRIPTION)
       values (1, 1, 1, 1, '�������� �� �� �� - ��� �����������');
insert into NDS2_MRR_USER.CFG_RESTRICTION_EXPORTEXCEL (ROLE_ID, GROUP_ID, NOT_RESTRICTED, ALLOW_PURCHASE, DESCRIPTION)
       values (2, 0, 1, 0, '����������� �� - ��������� ��� �����������, ��� ������ � ����������');
insert into NDS2_MRR_USER.CFG_RESTRICTION_EXPORTEXCEL (ROLE_ID, GROUP_ID, NOT_RESTRICTED, ALLOW_PURCHASE, DESCRIPTION)
       values (2, 1, 1, 0, '����������� �� �� �� - ��������� ��� �����������, ��� ������ � ����������');
insert into NDS2_MRR_USER.CFG_RESTRICTION_EXPORTEXCEL (ROLE_ID, GROUP_ID, NOT_RESTRICTED, ALLOW_PURCHASE, DESCRIPTION)
       values (3, 0, 1, 1, '���������� �� - ��� �����������');
insert into NDS2_MRR_USER.CFG_RESTRICTION_EXPORTEXCEL (ROLE_ID, GROUP_ID, NOT_RESTRICTED, ALLOW_PURCHASE, DESCRIPTION)
       values (4, 0, 1, 1, '��������� �� - ��� �����������');
insert into NDS2_MRR_USER.CFG_RESTRICTION_EXPORTEXCEL (ROLE_ID, GROUP_ID, NOT_RESTRICTED, ALLOW_PURCHASE, DESCRIPTION)
       values (4, 2, 0, 1, '��������� �� �� ���������� - ������ ���� ���������');
insert into NDS2_MRR_USER.CFG_RESTRICTION_EXPORTEXCEL (ROLE_ID, GROUP_ID, NOT_RESTRICTED, ALLOW_PURCHASE, DESCRIPTION)
       values (4, 3, 0, 1, '��������� ���� - ������ ���� ���������');
insert into NDS2_MRR_USER.CFG_RESTRICTION_EXPORTEXCEL (ROLE_ID, GROUP_ID, NOT_RESTRICTED, ALLOW_PURCHASE, DESCRIPTION)
       values (4, null, 0, 0, '��������� �� ��������� - ������ ���� ���������, ��� ������ � ����������');
commit;

update NDS2_MRR_USER.CFG_INSPECTION_GROUP set name='�� �� ������������ ��������' where id=1;
update NDS2_MRR_USER.CFG_INSPECTION_GROUP set name='�� �� ����������' where id=2;
commit;
