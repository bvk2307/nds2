/* ���������� � ������� ������ �� ������ �� */
insert /*+ APPEND */ into NDS2_MRR_USER.doc_discrepancy
select
distinct 
d.doc_id,
sd.discrepancy_id,
sd.invoice_rk
from NDS2_MRR_USER.selection_discrepancy sd 
inner join NDS2_MRR_USER.selection s on s.id = sd.selection_id
inner join NDS2_MRR_USER.ask_invoice_decl_queue q on q.decl_id = sd.decl_id
inner join NDS2_MRR_USER.doc d on d.sono_code = q.soun_code and d.ref_entity_id = q.seod_decl_id
left join NDS2_MRR_USER.doc_discrepancy dd on dd.discrepancy_id = sd.discrepancy_id
where 
s.status = 10
and sd.is_in_process = 1
and d.doc_type = 1
and d.doc_kind = 1
and dd.doc_id is null;
commit;
