begin
	execute immediate 'alter table NDS2_MRR_USER.ext_sono add full_name varchar2(512 char)';
	exception when others then null;
end;
/

update NDS2_MRR_USER.ext_sono s set
  s.full_name = s.s_name,
  s.s_name = replace(replace(replace(replace(replace(s.s_name,
          '����������� ��������� ����������� ��������� ������', '��� ��� ������'),
          '��������� ����������� ��������� ������', '���� ������'),
          '���������� ����������� ��������� ������', '���� ������'),
          '��������������� ��������� ����������� ��������� ������ �� ���������� ������������������', '�� ��� ������ �� ��'),
          '��������������� ��������� ����������� ��������� ������', '�� ��� ������')
where s.full_name is null or s.full_name = s.s_name;
commit;
