create table NDS2_MRR_USER.EFF_MON_DECL_STG
(
  fiscal_year             VARCHAR2(4) not null,
  quarter                  NUMBER not null,
  calc_date               DATE not null,
  sono_code               VARCHAR2(4) not null,
  inn					  VARCHAR2(12) not null,
  period				  VARCHAR2(2) not null,
  deduction_amnt          NUMBER not null,
  calculation_amnt        NUMBER not null,
  decl_nds_sum            NUMBER not null,
  operation_decl_cnt      NUMBER not null,
  discrepancy_decl_cnt    NUMBER not null,
  pay_decl_cnt            NUMBER not null,
  compensate_decl_cnt     NUMBER not null,
  zero_decl_cnt           NUMBER not null,
  pay_decl_nds_sum        NUMBER not null,
  compensate_decl_nds_sum NUMBER not null
);
