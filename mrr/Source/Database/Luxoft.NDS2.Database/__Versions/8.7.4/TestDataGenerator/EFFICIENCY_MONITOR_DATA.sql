insert into efficiency_monitor_aggregate
select
  REGION_CODE
, FISCAL_YEAR
, PERIOD
, to_date('15.01.2016', 'dd.mm.yyyy') as CALC_DATE
, SONO_CODE
, round( dbms_random.value(round(EFF_IDX_1/10, 0),EFF_IDX_1) , 2) as EFF_IDX_1
, round( dbms_random.value(round(EFF_IDX_2/10, 0),EFF_IDX_2) , 2) as EFF_IDX_2
, round( dbms_random.value(round(EFF_IDX_3/10, 0),EFF_IDX_3) , 2) as EFF_IDX_3
, round( dbms_random.value(round(EFF_IDX_4/10, 0),EFF_IDX_4) , 2) as EFF_IDX_4
, round( dbms_random.value(round(EFF_IDX_2_1/10, 0),EFF_IDX_2_1) , 2) as EFF_IDX_2_1
, round( dbms_random.value(round(EFF_IDX_2_2/10, 0),EFF_IDX_2_2) , 2) as EFF_IDX_2_2
, round( dbms_random.value(round(EFF_IDX_2_3/10, 0),EFF_IDX_2_3) , 2) as EFF_IDX_2_3
, round( dbms_random.value(round(EFF_IDX_2_4_KNP/10, 0),EFF_IDX_2_4_KNP) , 2) as EFF_IDX_2_4_KNP
, round( dbms_random.value(round(EFF_IDX_2_4_ALL/10, 0),EFF_IDX_2_4_ALL) , 2) as EFF_IDX_2_4_ALL
, round( dbms_random.value(round(BUYER_DIS_OPN_AMNT/10, 0),BUYER_DIS_OPN_AMNT) , 2) as BUYER_DIS_OPN_AMNT
, round( dbms_random.value(round(BUYER_DIS_OPN_KNP_AMNT/10, 0),BUYER_DIS_OPN_KNP_AMNT) , 2) as BUYER_DIS_OPN_KNP_AMNT
, round( dbms_random.value(round(BUYER_DIS_OPN_GAP_AMNT/10, 0),BUYER_DIS_OPN_GAP_AMNT) , 2) as BUYER_DIS_OPN_GAP_AMNT
, round( dbms_random.value(round(BUYER_DIS_OPN_GAP_KNP_AMNT/10, 0),BUYER_DIS_OPN_GAP_KNP_AMNT) , 2) as BUYER_DIS_OPN_GAP_KNP_AMNT
, round( dbms_random.value(round(BUYER_DIS_OPN_NDS_AMNT/10, 0),BUYER_DIS_OPN_NDS_AMNT) , 2) as BUYER_DIS_OPN_NDS_AMNT
, round( dbms_random.value(round(BUYER_DIS_OPN_NDS_KNP_AMNT/10, 0),BUYER_DIS_OPN_NDS_KNP_AMNT) , 2) as BUYER_DIS_OPN_NDS_KNP_AMNT
, round( dbms_random.value(round(BUYER_DIS_ALL_KNP_AMNT/10, 0),BUYER_DIS_ALL_KNP_AMNT) , 2) as BUYER_DIS_ALL_KNP_AMNT
, round( dbms_random.value(round(BUYER_DIS_CLS_AMNT/10, 0),BUYER_DIS_CLS_AMNT) , 2) as BUYER_DIS_CLS_AMNT
, round( dbms_random.value(round(BUYER_DIS_CLS_KNP_AMNT/10, 0),BUYER_DIS_CLS_KNP_AMNT) , 2) as BUYER_DIS_CLS_KNP_AMNT
, round( dbms_random.value(round(BUYER_DIS_CLS_GAP_AMNT/10, 0),BUYER_DIS_CLS_GAP_AMNT) , 2) as BUYER_DIS_CLS_GAP_AMNT
, round( dbms_random.value(round(BUYER_DIS_CLS_GAP_KNP_AMNT/10, 0),BUYER_DIS_CLS_GAP_KNP_AMNT) , 2) as BUYER_DIS_CLS_GAP_KNP_AMNT
, round( dbms_random.value(round(BUYER_DIS_CLS_NDS_AMNT/10, 0),BUYER_DIS_CLS_NDS_AMNT) , 2) as BUYER_DIS_CLS_NDS_AMNT
, round( dbms_random.value(round(BUYER_DIS_CLS_NDS_KNP_AMNT/10, 0),BUYER_DIS_CLS_NDS_KNP_AMNT) , 2) as BUYER_DIS_CLS_NDS_KNP_AMNT
, round( dbms_random.value(round(BUYER_DIS_OPN_KNP_CNT/10, 0),BUYER_DIS_OPN_KNP_CNT) , 2) as BUYER_DIS_OPN_KNP_CNT
, round( dbms_random.value(round(BUYER_DIS_OPN_GAP_KNP_CNT/10, 0),BUYER_DIS_OPN_GAP_KNP_CNT) , 2) as BUYER_DIS_OPN_GAP_KNP_CNT
, round( dbms_random.value(round(BUYER_DIS_OPN_NDS_KNP_CNT/10, 0),BUYER_DIS_OPN_NDS_KNP_CNT) , 2) as BUYER_DIS_OPN_NDS_KNP_CNT
, round( dbms_random.value(round(BUYER_DIS_CLS_KNP_CNT/10, 0),BUYER_DIS_CLS_KNP_CNT) , 2) as BUYER_DIS_CLS_KNP_CNT
, round( dbms_random.value(round(BUYER_DIS_CLS_GAP_KNP_CNT/10, 0),BUYER_DIS_CLS_GAP_KNP_CNT) , 2) as BUYER_DIS_CLS_GAP_KNP_CNT
, round( dbms_random.value(round(BUYER_DIS_CLS_NDS_CNT/10, 0),BUYER_DIS_CLS_NDS_CNT) , 2) as BUYER_DIS_CLS_NDS_CNT
, round( dbms_random.value(round(SELLER_DIS_OPN_AMNT/10, 0),SELLER_DIS_OPN_AMNT) , 2) as SELLER_DIS_OPN_AMNT
, round( dbms_random.value(round(SELLER_DIS_OPN_KNP_AMNT/10, 0),SELLER_DIS_OPN_KNP_AMNT) , 2) as SELLER_DIS_OPN_KNP_AMNT
, round( dbms_random.value(round(SELLER_DIS_OPN_GAP_AMNT/10, 0),SELLER_DIS_OPN_GAP_AMNT) , 2) as SELLER_DIS_OPN_GAP_AMNT
, round( dbms_random.value(round(SELLER_DIS_OPN_GAP_KNP_AMNT/10, 0),SELLER_DIS_OPN_GAP_KNP_AMNT) , 2) as SELLER_DIS_OPN_GAP_KNP_AMNT
, round( dbms_random.value(round(SELLER_DIS_OPN_NDS_AMNT/10, 0),SELLER_DIS_OPN_NDS_AMNT) , 2) as SELLER_DIS_OPN_NDS_AMNT
, round( dbms_random.value(round(SELLER_DIS_OPN_NDS_KNP_AMNT/10, 0),SELLER_DIS_OPN_NDS_KNP_AMNT) , 2) as SELLER_DIS_OPN_NDS_KNP_AMNT
, round( dbms_random.value(round(SELLER_DIS_ALL_KNP_AMNT/10, 0),SELLER_DIS_ALL_KNP_AMNT) , 2) as SELLER_DIS_ALL_KNP_AMNT
, round( dbms_random.value(round(SELLER_DIS_CLS_AMNT/10, 0),SELLER_DIS_CLS_AMNT) , 2) as SELLER_DIS_CLS_AMNT
, round( dbms_random.value(round(SELLER_DIS_CLS_KNP_AMNT/10, 0),SELLER_DIS_CLS_KNP_AMNT) , 2) as SELLER_DIS_CLS_KNP_AMNT
, round( dbms_random.value(round(SELLER_DIS_CLS_GAP_AMNT/10, 0),SELLER_DIS_CLS_GAP_AMNT) , 2) as SELLER_DIS_CLS_GAP_AMNT
, round( dbms_random.value(round(SELLER_DIS_CLS_GAP_KNP_AMNT/10, 0),SELLER_DIS_CLS_GAP_KNP_AMNT) , 2) as SELLER_DIS_CLS_GAP_KNP_AMNT
, round( dbms_random.value(round(SELLER_DIS_CLS_NDS_AMNT/10, 0),SELLER_DIS_CLS_NDS_AMNT) , 2) as SELLER_DIS_CLS_NDS_AMNT
, round( dbms_random.value(round(SELLER_DIS_CLS_NDS_KNP_AMNT/10, 0),SELLER_DIS_CLS_NDS_KNP_AMNT) , 2) as SELLER_DIS_CLS_NDS_KNP_AMNT
, round( dbms_random.value(round(SELLER_DIS_OPN_KNP_CNT/10, 0),SELLER_DIS_OPN_KNP_CNT) , 2) as SELLER_DIS_OPN_KNP_CNT
, round( dbms_random.value(round(SELLER_DIS_OPN_GAP_KNP_CNT/10, 0),SELLER_DIS_OPN_GAP_KNP_CNT) , 2) as SELLER_DIS_OPN_GAP_KNP_CNT
, round( dbms_random.value(round(SELLER_DIS_OPN_NDS_KNP_CNT/10, 0),SELLER_DIS_OPN_NDS_KNP_CNT) , 2) as SELLER_DIS_OPN_NDS_KNP_CNT
, round( dbms_random.value(round(SELLER_DIS_CLS_KNP_CNT/10, 0),SELLER_DIS_CLS_KNP_CNT) , 2) as SELLER_DIS_CLS_KNP_CNT
, round( dbms_random.value(round(SELLER_DIS_CLS_GAP_KNP_CNT/10, 0),SELLER_DIS_CLS_GAP_KNP_CNT) , 2) as SELLER_DIS_CLS_GAP_KNP_CNT
, round( dbms_random.value(round(SELLER_DIS_CLS_NDS_KNP_CNT/10, 0),SELLER_DIS_CLS_NDS_KNP_CNT) , 2) as SELLER_DIS_CLS_NDS_KNP_CNT
, round( dbms_random.value(round(DEDUCTION_AMNT/10, 0),DEDUCTION_AMNT) , 2) as DEDUCTION_AMNT
, round( dbms_random.value(round(CALCULATION_AMNT/10, 0),CALCULATION_AMNT) , 2) as CALCULATION_AMNT
, round( dbms_random.value(round(DECL_ALL_CNT/10, 0),DECL_ALL_CNT) , 2) as DECL_ALL_CNT
, round( dbms_random.value(round(DECL_ALL_NDS_SUM/10, 0),DECL_ALL_NDS_SUM) , 2) as DECL_ALL_NDS_SUM
, round( dbms_random.value(round(DECL_OPERATION_CNT/10, 0),DECL_OPERATION_CNT) , 2) as DECL_OPERATION_CNT
, round( dbms_random.value(round(DECL_DISCREPANCY_CNT/10, 0),DECL_DISCREPANCY_CNT) , 2) as DECL_DISCREPANCY_CNT
, round( dbms_random.value(round(DECL_PAY_CNT/10, 0),DECL_PAY_CNT) , 2) as DECL_PAY_CNT
, round( dbms_random.value(round(DECL_PAY_NDS_SUM/10, 0),DECL_PAY_NDS_SUM) , 2) as DECL_PAY_NDS_SUM
, round( dbms_random.value(round(DECL_COMPENSATE_CNT/10, 0),DECL_COMPENSATE_CNT) , 2) as DECL_COMPENSATE_CNT
, round( dbms_random.value(round(DECL_COMPENSATE_NDS_SUM/10, 0),DECL_COMPENSATE_NDS_SUM) , 2) as DECL_COMPENSATE_NDS_SUM
, round( dbms_random.value(round(DECL_ZERO_CNT/10, 0),DECL_ZERO_CNT) , 2) as DECL_ZERO_CNT
from efficiency_monitor_aggregate
where calc_date = to_date('01.01.2016', 'dd.mm.yyyy')