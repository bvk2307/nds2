BEGIN
DBMS_SCHEDULER.DROP_JOB (
job_name => 'NDS2_MRR_USER.J$EFFECTIVENESS_MONITOR_CALC');
exception when others then null;
END;
/
begin 
  sys.dbms_scheduler.create_job(job_name => 'NDS2_MRR_USER.J$EFFECTIVENESS_MONITOR_CALC', 
  job_type => 'STORED_PROCEDURE', 
  job_action => 'PAC$EFFICIENCY_MONITOR.P$UPDATE_AGGREGATE', 
  start_date => trunc(next_day(sysdate,'SUN'))+18/24, 
  repeat_interval => 'trunc(next_day(sysdate,''SUN''))+18/24', 
  end_date => to_date('01-01-2100 00:00:00', 'dd-mm-yyyy hh24:mi:ss'), 
  job_class => 'DEFAULT_JOB_CLASS', 
  enabled => false, 
  auto_drop => false, 
  comments => '������ ����� ������ ����������� �������������'); 
end; 
/
