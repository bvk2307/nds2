create table NDS2_MRR_USER.IFNS_AGGREGATE
(
  ifns           VARCHAR2(4),
  inn            VARCHAR2(12),
  tax_name       VARCHAR2(1000),
  quarter        CHAR(1),
  tax_year       NUMBER(4),
  gap_count      NUMBER,
  gap_sum        NUMBER(19,2),
  nds_calculated NUMBER(19,2),
  nds_deduction  NUMBER(19,2)
) 
tablespace NDS2_DATA;

-- Add comments to the columns 
comment on column NDS2_MRR_USER.IFNS_AGGREGATE.nds_calculated  is '��� �����������';
comment on column NDS2_MRR_USER.IFNS_AGGREGATE.nds_deduction	 is '��� � ������';

-- Add indexes
-- Unique. We don't need double records...
create unique index NDS2_MRR_USER.IDX_IFNS_AGGREGATE_PK on NDS2_MRR_USER.IFNS_AGGREGATE (inn, tax_year, quarter) TableSpace NDS2_IDX;
