﻿-- Создание таблицы-агрегата при помощи запроса
create table NDS2_MRR_USER.KNP_DECLARATION_SUMMARY TableSpace NDS2_DATA 
as 
select  
	SD.DECL_ID 
	,SUM(Case When SD.INVOICE_CHAPTER=8 and SD.Type = 1 Then SD.Amount END) As gap_amount_8 
	,SUM(Case When SD.INVOICE_CHAPTER=9 and SD.Type = 1 Then SD.Amount END) As gap_amount_9  
	,SUM(Case When SD.INVOICE_CHAPTER=10 and SD.Type = 1 Then SD.Amount END) As gap_amount_10 
	,SUM(Case When SD.INVOICE_CHAPTER=11 and SD.Type = 1 Then SD.Amount END) As gap_amount_11 
	,SUM(Case When SD.INVOICE_CHAPTER=12 and SD.Type = 1 Then SD.Amount END) As gap_amount_12  
	,SUM(Case When SD.INVOICE_CHAPTER=8 and SD.Type <> 1 Then SD.Amount END) As other_amount_8 
	,SUM(Case When SD.INVOICE_CHAPTER=9 and SD.Type <> 1 Then SD.Amount END) As other_amount_9  
	,SUM(Case When SD.INVOICE_CHAPTER=10 and SD.Type <> 1 Then SD.Amount END) As other_amount_10 
	,SUM(Case When SD.INVOICE_CHAPTER=11 and SD.Type <> 1 Then SD.Amount END) As other_amount_11 
	,SUM(Case When SD.INVOICE_CHAPTER=12 and SD.Type <> 1 Then SD.Amount END) As other_amount_12  
	,COUNT(Case When SD.INVOICE_CHAPTER=8 and SD.Type = 1 Then SD.Discrepancy_ID END) As gap_count_8 
	,COUNT(Case When SD.INVOICE_CHAPTER=9 and SD.Type = 1 Then SD.Discrepancy_ID END) As gap_count_9  
	,COUNT(Case When SD.INVOICE_CHAPTER=10 and SD.Type = 1 Then SD.Discrepancy_ID END) As gap_count_10 
	,COUNT(Case When SD.INVOICE_CHAPTER=11 and SD.Type = 1 Then SD.Discrepancy_ID END) As gap_count_11 
	,COUNT(Case When SD.INVOICE_CHAPTER=12 and SD.Type = 1 Then SD.Discrepancy_ID END) As gap_count_12  
	,COUNT(Case When SD.INVOICE_CHAPTER=8 and SD.Type <> 1 Then SD.Discrepancy_ID END) As other_count_8 
	,COUNT(Case When SD.INVOICE_CHAPTER=9 and SD.Type <> 1 Then SD.Discrepancy_ID END) As other_count_9  
	,COUNT(Case When SD.INVOICE_CHAPTER=10 and SD.Type <> 1 Then SD.Discrepancy_ID END) As other_count_10 
	,COUNT(Case When SD.INVOICE_CHAPTER=11 and SD.Type <> 1 Then SD.Discrepancy_ID END) As other_count_11 
	,COUNT(Case When SD.INVOICE_CHAPTER=12 and SD.Type <> 1 Then SD.Discrepancy_ID END) As other_count_12 
	,COUNT(Case When SD.INVOICE_CHAPTER between 8 and 12 Then SD.DISCREPANCY_ID END) As total_discrepancy_count 
	From 
		NDS2_MRR_USER.SELECTION_DISCREPANCY SD 
		Join NDS2_MRR_USER.SELECTION S ON SD.SELECTION_ID = S.ID and S.Status = 10 
	Where 
		SD.status<>2
	Group By SD.DECL_ID;

-- Уникальный индекс по полю DECL_ID
create UNIQUE index NDS2_MRR_USER.IDX_KNP_DECL_SUM_PK on NDS2_MRR_USER.KNP_DECLARATION_SUMMARY (DECL_ID) tablespace NDS2_IDX;

