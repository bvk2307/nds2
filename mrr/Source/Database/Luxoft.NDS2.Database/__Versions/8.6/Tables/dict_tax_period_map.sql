create table NDS2_MRR_USER.dict_tax_period_map
(
  code varchar2(2 char),
  period number
);
/*1-� �������*/
insert into NDS2_MRR_USER.dict_tax_period_map values('01', 1);
insert into NDS2_MRR_USER.dict_tax_period_map values('02', 1);
insert into NDS2_MRR_USER.dict_tax_period_map values('03', 1);
insert into NDS2_MRR_USER.dict_tax_period_map values('21', 1);
insert into NDS2_MRR_USER.dict_tax_period_map values('51', 1);
insert into NDS2_MRR_USER.dict_tax_period_map values('71', 1);
insert into NDS2_MRR_USER.dict_tax_period_map values('72', 1);
insert into NDS2_MRR_USER.dict_tax_period_map values('73', 1);
/*2-� �������*/
insert into NDS2_MRR_USER.dict_tax_period_map values('04', 2);
insert into NDS2_MRR_USER.dict_tax_period_map values('05', 2);
insert into NDS2_MRR_USER.dict_tax_period_map values('06', 2);
insert into NDS2_MRR_USER.dict_tax_period_map values('22', 2);
insert into NDS2_MRR_USER.dict_tax_period_map values('54', 2);
insert into NDS2_MRR_USER.dict_tax_period_map values('74', 2);
insert into NDS2_MRR_USER.dict_tax_period_map values('75', 2);
insert into NDS2_MRR_USER.dict_tax_period_map values('76', 2);
/*3-� �������*/
insert into NDS2_MRR_USER.dict_tax_period_map values('07', 3);
insert into NDS2_MRR_USER.dict_tax_period_map values('08', 3);
insert into NDS2_MRR_USER.dict_tax_period_map values('09', 3);
insert into NDS2_MRR_USER.dict_tax_period_map values('23', 3);
insert into NDS2_MRR_USER.dict_tax_period_map values('55', 3);
insert into NDS2_MRR_USER.dict_tax_period_map values('77', 3);
insert into NDS2_MRR_USER.dict_tax_period_map values('78', 3);
insert into NDS2_MRR_USER.dict_tax_period_map values('79', 3);
/*4-� �������*/
insert into NDS2_MRR_USER.dict_tax_period_map values('10', 4);
insert into NDS2_MRR_USER.dict_tax_period_map values('11', 4);
insert into NDS2_MRR_USER.dict_tax_period_map values('12', 4);
insert into NDS2_MRR_USER.dict_tax_period_map values('24', 4);
insert into NDS2_MRR_USER.dict_tax_period_map values('56', 4);
insert into NDS2_MRR_USER.dict_tax_period_map values('80', 4);
insert into NDS2_MRR_USER.dict_tax_period_map values('81', 4);
insert into NDS2_MRR_USER.dict_tax_period_map values('82', 4);

create index NDS2_MRR_USER.idx_tax_per_map_per on NDS2_MRR_USER.dict_tax_period_map(period) tablespace NDS2_IDX compress;
