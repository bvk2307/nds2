begin
	execute immediate 'drop table NDS2_MRR_USER.v$inspector_contragent';
exception
	when others then
		if SQLCODE != -942 then
			RAISE;
		end if;
end;
/