﻿create table NDS2_MRR_USER.KNP_DISCREPANCY_INVOICE 
as 
select 
	KD.Discrepancy_Id	as DISCREPANCY_ID
	,SD.Invoice_Rk		as INVOICE_ROW_KEY 
	From 
		NDS2_MRR_USER.KNP_DISCREPANCY KD 
		JOIN NDS2_MRR_USER.SOV_DISCREPANCY SD ON KD.DISCREPANCY_ID = SD.ID 
		JOIN NDS2_MRR_USER.SOV_INVOICE_ALL I  
			ON (
					(SD.INVOICE_RK		like '%A%' and I.ACTUAL_ROW_KEY	= SD.INVOICE_RK)    
			  OR	(SD.INVOICE_RK not	like '%A%' and I.ROW_KEY			= SD.INVOICE_RK) ); 

create index NDS2_MRR_USER.IDX_KNP_DISCREP_INV on NDS2_MRR_USER.KNP_DISCREPANCY_INVOICE (DISCREPANCY_ID) TableSpace NDS2_IDX;
  