﻿create table NDS2_MRR_USER.DECLARATION_CHANGES
(
  decl_id     NUMBER not null,
  has_changes NUMBER
)
TableSpace NDS2_DATA
  pctfree 10
  initrans 1
  maxtrans 255
nologging;
-- Create/Recreate indexes 
create unique index NDS2_MRR_USER.IDX_DECL_CHANGES_ID on NDS2_MRR_USER.DECLARATION_CHANGES (DECL_ID)
  TableSpace NDS2_IDX
  pctfree 10
  initrans 2
  maxtrans 255;