﻿create table NDS2_MRR_USER.DECLARATION_REQUEST_COUNTER TableSpace NDS2_DATA 
as 
select  
	dc.ref_entity_id   as REF_ENTITY_ID  
	,dc.sono_code      as SONO_CODE   
	,count(*)          as AT_COUNTER  
	From 
		NDS2_MRR_USER.DOC dc  
	Where 
		dc.close_date is null  
	Group By 
		dc.ref_entity_id, 
		dc.sono_code;

-- Индекс
create index NDS2_MRR_USER.IDX_DECL_REQ_COUNTER on NDS2_MRR_USER.DECLARATION_REQUEST_COUNTER (REF_ENTITY_ID, SONO_CODE) TableSpace NDS2_IDX;