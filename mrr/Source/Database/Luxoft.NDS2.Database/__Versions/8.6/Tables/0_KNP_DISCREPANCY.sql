﻿create table NDS2_MRR_USER.KNP_DISCREPANCY TableSpace NDS2_DATA
as 
select 
	sd.discrepancy_id		as DISCREPANCY_ID
	,sd.invoice_chapter		as CHAPTER 
	,sd.decl_id				as DECL_ID
	,sd.taxpayerinn			as INN_1 
	,sd.inn_contractor		as CONTRACTOR_INN 
	from 
		NDS2_MRR_USER.selection_discrepancy sd 
		inner join NDS2_MRR_USER.selection s on s.id = sd.selection_id and s.status=10 
	where  
		sd.is_in_process = 1 and sd.status<>2
	group by 
		sd.discrepancy_id, 
		sd.invoice_chapter, 
		sd.decl_id, 
		sd.taxpayerinn, 
		sd.inn_contractor;

-- Индексы
create UNIQUE index NDS2_MRR_USER.IDX_KNP_DISCREP_PK		on NDS2_MRR_USER.KNP_DISCREPANCY (DISCREPANCY_ID)						TableSpace NDS2_IDX;
create UNIQUE index NDS2_MRR_USER.IDX_KNP_DISCREP_GROUP	on NDS2_MRR_USER.KNP_DISCREPANCY (CHAPTER, DECL_ID, CONTRACTOR_INN)	TableSpace NDS2_IDX;
