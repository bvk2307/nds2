﻿create table NDS2_MRR_USER.DECLARATION_EXPLAIN_SUMMARY TableSpace NDS2_DATA 
as 
select  
	DH.ID											as DECL_ID  
	,MAX(Case When ER.Type_Id=2 Then 1 Else 0 End)	as HAS_TKS  
	,COUNT(*)										as NOT_PROCESSED_EXPLAIN_COUNT  
	From 
		NDS2_MRR_USER.DECLARATION_HISTORY DH  
				Join NDS2_MRR_USER.DOC D ON D.Ref_Entity_Id = DH.SEOD_DECL_ID AND D.Sono_Code = DH.SOUN_CODE  
				Join NDS2_MRR_USER.SEOD_EXPLAIN_REPLY ER ON D.Doc_Id = ER.Doc_Id  
		Left	Join NDS2_MRR_USER.V$ASKPOYASNENIE P ON P.ImyaFaylTreb = ER.FILENAMEOUTPUT  
	Where 
		P.ID is not null  
	Group By 
		DH.ID ; 

-- Индекс
create index NDS2_MRR_USER.IDX_DECL_EXPL_SUMM_ID on NDS2_MRR_USER.DECLARATION_EXPLAIN_SUMMARY (DECL_ID) TableSpace NDS2_IDX;