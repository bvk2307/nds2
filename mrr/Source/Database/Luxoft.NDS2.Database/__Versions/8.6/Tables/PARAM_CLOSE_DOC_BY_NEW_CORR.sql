create table NDS2_MRR_USER.PARAM_CLOSE_DOC_BY_NEW_CORR
(
   job_id number,
   inn varchar2(12 char),
   period varchar2(2 char),
   year varchar2(4 char),
   status number
);
