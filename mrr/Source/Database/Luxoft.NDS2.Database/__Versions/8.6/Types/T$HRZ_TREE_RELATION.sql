CREATE OR REPLACE TYPE NDS2_MRR_USER.T$HRZ_TREE_RELATION as object (
  ID                            NUMBER,
  PARENT_INN                    VARCHAR2(12 CHAR),
  PARENT_VAT_DEDUCTION_TOTAL    NUMBER(19,2),
  PARENT_VAT_CALCULATION_TOTAL  NUMBER(19,2),
  INN                           VARCHAR2(12 CHAR),
  LVL                           NUMBER(2),
  DECLARATION_TYPE              VARCHAR2(24 CHAR),
  VAT_TOTAL                     NUMBER(19,2),
  VAT_DEDUCTION_TOTAL           NUMBER(19,2),
  VAT_CALCULATION_TOTAL         NUMBER(19,2),
  GAP_DISCREPANCY_TOTAL         NUMBER(19,2),
  GAP_DISCREPANCY               NUMBER(19,2),
  VAT_DEDUCTION                 NUMBER(19,2),
  VAT_CALCULATION               NUMBER(19,2)
);
/

