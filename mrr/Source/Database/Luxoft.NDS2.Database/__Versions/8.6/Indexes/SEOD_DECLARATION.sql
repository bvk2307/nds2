begin
  execute immediate 'drop index NDS2_MRR_USER.IDX$$_067F0001';
  exception when others then
    null;
end;
/
create index NDS2_MRR_USER.IDX_DECL_RNUM_SONO 
       on NDS2_MRR_USER.SEOD_DECLARATION (SONO_CODE,DECL_REG_NUM,TYPE)
       nologging
       tablespace NDS2_IDX;
