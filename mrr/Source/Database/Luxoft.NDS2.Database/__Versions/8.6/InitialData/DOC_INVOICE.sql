alter table NDS2_MRR_USER.DOC_INVOICE rename to DOC_INVOICE_BK;
create table NDS2_MRR_USER.DOC_INVOICE
nologging
pctfree 0
as
select
   dt.declaration_version_id
  ,pk.invoice_chapter
  ,pk.invoice_row_key
  ,pk.doc_id
  ,dt.ordinal_number
  ,dt.okv_code
  ,dt.create_date
  ,dt.receive_date
  ,dt.invoice_num
  ,dt.invoice_date
  ,dt.change_num
  ,dt.change_date
  ,dt.correction_num
  ,dt.correction_date
  ,dt.change_correction_num
  ,dt.change_correction_date
  ,dt.seller_invoice_num
  ,dt.seller_invoice_date
  ,dt.broker_inn
  ,dt.broker_kpp
  ,dt.deal_kind_code
  ,dt.customs_declaration_num
  ,dt.price_buy_amount
  ,dt.price_buy_nds_amount
  ,dt.price_sell
  ,dt.price_sell_in_curr
  ,dt.price_sell_18
  ,dt.price_sell_10
  ,dt.price_sell_0
  ,dt.price_nds_18
  ,dt.price_nds_10
  ,dt.price_tax_free
  ,dt.price_total
  ,dt.price_nds_total
  ,dt.diff_correct_decrease
  ,dt.diff_correct_increase
  ,dt.diff_correct_nds_decrease
  ,dt.diff_correct_nds_increase
  ,dt.price_nds_buyer
  ,dt.actual_row_key
  ,dt.compare_row_key
  ,dt.compare_algo_id
  ,dt.format_errors
  ,dt.logical_errors
  ,dt.seller_agency_info_inn
  ,dt.seller_agency_info_kpp
  ,dt.seller_agency_info_name
  ,dt.seller_agency_info_num
  ,dt.seller_agency_info_date
  ,dt.is_import
  ,dt.clarification_key
  ,dt.contragent_key
  ,dt.is_dop_list
from
  NDS2_MRR_USER.DOC_INVOICE_BK pk
  left join NDS2_MRR_USER.SOV_INVOICE_ALL dt on dt.row_key = pk.invoice_row_key;

begin
  execute immediate 'drop table DOC_INVOICE_BK';
  exception when others then
    null;
end;
/
create index NDS2_MRR_USER.IDX_DOC_INVOICE_DOC_ID on NDS2_MRR_USER.DOC_INVOICE (DOC_ID, INVOICE_CHAPTER) nologging tablespace NDS2_IDX;
create index NDS2_MRR_USER.IDX_DOC_INVOICE_ROW_KEY on NDS2_MRR_USER.DOC_INVOICE (INVOICE_ROW_KEY) nologging tablespace NDS2_IDX;

create table NDS2_MRR_USER.DOC_INVOICE_BUYER
as
select dt.*       
from
  NDS2_MRR_USER.DOC_INVOICE pk
  join NDS2_MRR_USER.SOV_INVOICE_BUYER_ALL dt on pk.INVOICE_ROW_KEY = dt.row_key_id;
  
create index NDS2_MRR_USER.IDX_DI_BUYER 
       on NDS2_MRR_USER.DOC_INVOICE_BUYER (ROW_KEY_ID)
       nologging
       tablespace NDS2_IDX;

create table NDS2_MRR_USER.DOC_INVOICE_SELLER
as
select dt.*
from 
  NDS2_MRR_USER.DOC_INVOICE pk
  join NDS2_MRR_USER.SOV_INVOICE_SELLER_ALL dt on pk.INVOICE_ROW_KEY = dt.row_key_id;

create index NDS2_MRR_USER.IDX_DI_SELLER 
       on NDS2_MRR_USER.DOC_INVOICE_SELLER (ROW_KEY_ID)
       nologging
       tablespace NDS2_IDX;
       
create table NDS2_MRR_USER.DOC_INVOICE_BUY_ACCEPT
as
select dt.*
from
  NDS2_MRR_USER.SOV_INVOICE_BUY_ACCEPT_All dt
  join NDS2_MRR_USER.DOC_INVOICE pk on pk.INVOICE_ROW_KEY = dt.row_key_id;
  
create index NDS2_MRR_USER.IDX_DI_BUY_ACCEPT
    on NDS2_MRR_USER.DOC_INVOICE_BUY_ACCEPT (ROW_KEY_ID)
    nologging
    tablespace NDS2_IDX;
    
create table NDS2_MRR_USER.DOC_INVOICE_OPERATION
as
select dt.*
from
  NDS2_MRR_USER.SOV_INVOICE_OPERATION_ALL dt
  join NDS2_MRR_USER.DOC_INVOICE pk on pk.INVOICE_ROW_KEY = dt.row_key_id;
  
create index NDS2_MRR_USER.IDX_DI_OPERATION
    on NDS2_MRR_USER.DOC_INVOICE_OPERATION (ROW_KEY_ID)
    nologging
    tablespace NDS2_IDX;
    
create table NDS2_MRR_USER.DOC_INVOICE_PAYMENT_DOC
as
select dt.*
from
  NDS2_MRR_USER.SOV_INVOICE_PAYMENT_DOC_ALL dt
  join NDS2_MRR_USER.DOC_INVOICE pk on pk.INVOICE_ROW_KEY = dt.row_key_id;
  
create index NDS2_MRR_USER.IDX_DI_PAYMENT_DOC
    on NDS2_MRR_USER.DOC_INVOICE_PAYMENT_DOC (ROW_KEY_ID)
    nologging
    tablespace NDS2_IDX;
