/* ������������ 2-� ������� */
update doc set close_date = sysdate where doc_kind = 1;
delete from doc_invoice where doc_id in (select doc_id from doc where doc_kind = 2);
delete from doc_discrepancy where doc_id in (select doc_id from doc where doc_kind = 2);
delete from doc where doc_kind = 2;
delete from ask_invoice_decl_queue where for_stage = 3;
update selection set status = 4 where discrepancy_stage = 2 and status = 10;
commit;
truncate table system_log;
begin
  -- Call the procedure
  pac$document.COLLECT_QUEUE;
  pac$document.process_queue;
end;
/
select * from system_log;
