﻿create table NDS2_MRR_USER.EFFICIENCY_MONITOR_DETAIL
(
  id                         NUMBER not null,
  inn                        VARCHAR2(12) not null,
  period                     VARCHAR2(2) not null,
  quarter                    NUMBER not null,
  fiscal_year                NUMBER not null,
  sono_code                  VARCHAR2(4) not null,
  calc_date                  DATE not null,
  opn_amnt         NUMBER not null,
  opn_knp_amnt     NUMBER not null,
  opn_knp_cnt      NUMBER not null,
  opn_gap_amnt     NUMBER not null,
  opn_gap_knp_amnt NUMBER not null,
  opn_gap_knp_cnt  NUMBER not null,
  opn_nds_amnt     NUMBER not null,
  opn_nds_knp_amnt NUMBER not null,
  opn_nds_knp_cnt  NUMBER not null,
  all_knp_amnt     NUMBER not null,
  cls_amnt         NUMBER not null,
  cls_knp_amnt     NUMBER not null,
  cls_knp_cnt      NUMBER not null,
  cls_gap_amnt     NUMBER not null,
  cls_gap_knp_amnt NUMBER not null,
  cls_gap_knp_cnt  NUMBER not null,
  cls_nds_amnt     NUMBER not null,
  cls_nds_knp_cnt  NUMBER not null,
  cls_nds_knp_amnt NUMBER not null
)
partition by list (calc_date)
(
 PARTITION p_effmon_detail_default  VALUES (to_date('01.01.1900', 'dd.mm.yyyy')) 
);

create index NDS2_MRR_USER.idx_eff_mon_det_ufns on NDS2_MRR_USER.efficiency_monitor_detail(fiscal_year,quarter, substr(sono_code, 1,2)||'00', calc_date) local tablespace NDS2_IDX;
create index NDS2_MRR_USER.idx_eff_mon_det_ifns on NDS2_MRR_USER.efficiency_monitor_detail(fiscal_year,quarter, sono_code, calc_date) local tablespace NDS2_IDX;
