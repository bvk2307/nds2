﻿BEGIN
  DBMS_SCHEDULER.DROP_JOB (job_name => 'NDS2_MRR_USER.J$CLOSE_CLAIM_BY_TIMEOUT');
END;
/
begin 
  sys.dbms_scheduler.create_job(job_name => 'NDS2_MRR_USER.J$CLOSE_CLAIM', 
  job_type => 'STORED_PROCEDURE', 
  job_action => 'PAC$DOCUMENT.CLOSE_DOC_PROCESSING', 
  start_date => TO_DATE(TO_CHAR(SYSDATE, 'dd/mm/yyyy') || ' 04:00:00', 'dd/mm/yyyy HH24:mi:ss') + INTERVAL '1' MINUTE, 
  repeat_interval => 'freq=Hourly;Interval=7', 
  end_date => to_date('01-01-2100 00:00:00', 'dd-mm-yyyy hh24:mi:ss'), 
  job_class => 'DEFAULT_JOB_CLASS', 
  enabled => false, 
  auto_drop => false, 
  comments => 'закрытие АТ по СФ'); 
end; 
/
