﻿create table NDS2_MRR_USER.DOC_CLOSE_REASON
(
  id NUMBER(3) not null CONSTRAINT PK_DOC_CLOSE_REASON PRIMARY KEY,
  description VARCHAR2(128) not null
);

