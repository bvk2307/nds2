﻿alter type NDS2_MRR_USER.T$HRZ_TREE_RELATION add attribute (DECL_SIGN NUMBER(1)) cascade;
alter type NDS2_MRR_USER.T$HRZ_TREE_RELATION add attribute (TRANSIT NUMBER(1)) cascade;
alter type NDS2_MRR_USER.T$HRZ_TREE_RELATION add attribute (YEAR_QUARTER_VAT_LIST VARCHAR2(4000 char)) cascade;