﻿create table NDS2_MRR_USER.EXPLAIN_FOR_RESEND
as
select sed.zip -- декларация
      ,sed.explain_id -- пояснение
  from 
    ( 
      select dh.zip
            ,sers.explain_id
            ,nvl(va.ObrabotanMS, -1) as FlagMS
        from 
          ( 
            select * 
              from NDS2_MRR_USER.seod_explain_reply 
              where type_id = 1 
                and receive_by_tks = 0 -- неформализованные пояснения
                and status_id = 2      -- со статусом "Отправлено в МС"
          ) sers 
        join NDS2_MRR_USER.doc ds 
          on sers.doc_id = ds.doc_id 
          and ds.doc_type = 1          -- только для АТ по СФ
          and ds.doc_kind in (1, 2) 
        join NDS2_MRR_USER.declaration_history dh 
          on ds.ref_entity_id = dh.reg_number 
          and ds.sono_code = dh.sono_code_submited 
        join NDS2_MRR_USER.v$askzipfajl vaz 
          on sers.zip = vaz.id 
          and vaz.status = '1'         -- только успешно полученные МСом, чтобы гарантировать, что это пояснение уже обрабатывалось, а не находится в очереди
        left outer join NDS2_MRR_USER.v$askpoyasnenie va 
          on sers.zip = va.zip 
        group by dh.zip, sers.explain_id, nvl(va.ObrabotanMS, -1)
    ) sed 
  join -- отбираем только те пояснения, для котрых есть дубли во множественных атрибутах
    ( 
      select distinct eis.explain_id 
        from 
          ( 
            select ser.explain_id 
                  ,di.invoice_row_key 
              from 
                ( 
                  select explain_id 
                        ,doc_id 
                    from NDS2_MRR_USER.seod_explain_reply 
                    where type_id = 1 
                      and receive_by_tks = 0 -- неформализованные пояснения
                      and status_id = 2      -- со статусом "Отправлено в МС"
                ) ser 
              inner join NDS2_MRR_USER.doc_invoice di 
                on ser.doc_id = di.doc_id 
              left outer join NDS2_MRR_USER.doc_invoice_seller dis 
                on di.invoice_row_key = dis.row_key_id 
              left outer join NDS2_MRR_USER.doc_invoice_buyer dib 
                on di.invoice_row_key = dib.row_key_id 
              left outer join NDS2_MRR_USER.doc_invoice_operation dio 
                on di.invoice_row_key = dio.row_key_id 
              left outer join NDS2_MRR_USER.doc_invoice_buy_accept diba 
                on di.invoice_row_key = diba.row_key_id 
              left outer join NDS2_MRR_USER.doc_invoice_payment_doc dipd 
                on di.invoice_row_key = dipd.row_key_id 
              group by ser.explain_id, di.invoice_row_key 
              having count(di.invoice_row_key) > 1 
          ) eis
    ) ei 
    on sed.explain_id = ei.explain_id
  where sed.FlagMS <> 0 -- т.е. пояснения, которые обработаны МСом
  order by 1, 2;

create table NDS2_MRR_USER.doc_invoice_buy_accept_temp
PCTFREE 0
nologging as 
(select s.row_key_id,s.buy_accept_date, d.doc_id, s.row_count
from (select row_key_id,buy_accept_date, count(*) as row_count from NDS2_MRR_USER.doc_invoice_buy_accept group by row_key_id,buy_accept_date) s
join NDS2_MRR_USER.doc_invoice d on s.row_key_id = d.invoice_row_key);

drop table NDS2_MRR_USER.doc_invoice_buy_accept;

alter table NDS2_MRR_USER.doc_invoice_buy_accept_temp rename to doc_invoice_buy_accept;

create table NDS2_MRR_USER.doc_invoice_buyer_temp
PCTFREE 0
nologging as 
(
select s.row_key_id, s.buyer_inn, s.buyer_kpp, d.doc_id, s.row_count
from (select row_key_id, buyer_inn, buyer_kpp, count(*) as row_count from NDS2_MRR_USER.doc_invoice_buyer group by row_key_id, buyer_inn, buyer_kpp) s
join NDS2_MRR_USER.doc_invoice d on s.ROW_KEY_ID = d.INVOICE_ROW_KEY
);

drop table NDS2_MRR_USER.doc_invoice_buyer;

alter table NDS2_MRR_USER.doc_invoice_buyer_temp rename to doc_invoice_buyer;

create table NDS2_MRR_USER.doc_invoice_operation_temp
PCTFREE 0
nologging as
( 
	select s.row_key_id, s.operation_code, d.doc_id, s.row_count
	from (select row_key_id, operation_code, count(*) as row_count from NDS2_MRR_USER.doc_invoice_operation group by row_key_id, operation_code) s
	join NDS2_MRR_USER.doc_invoice d on s.ROW_KEY_ID = d.INVOICE_ROW_KEY
);

drop table NDS2_MRR_USER.doc_invoice_operation;

alter table NDS2_MRR_USER.doc_invoice_operation_temp rename to doc_invoice_operation;

create table NDS2_MRR_USER.doc_invoice_payment_doc_temp
PCTFREE 0
nologging as
( 
	select s.row_key_id, s.doc_num, s.doc_date, d.doc_id, s.row_count
	from (select row_key_id, doc_num, doc_date, count(*) as row_count from NDS2_MRR_USER.doc_invoice_payment_doc group by row_key_id, doc_num, doc_date) s
	join NDS2_MRR_USER.doc_invoice d on s.ROW_KEY_ID = d.INVOICE_ROW_KEY
);

drop table NDS2_MRR_USER.doc_invoice_payment_doc;

alter table NDS2_MRR_USER.doc_invoice_payment_doc_temp rename to doc_invoice_payment_doc;

create table NDS2_MRR_USER.doc_invoice_seller_temp
PCTFREE 0
nologging as 
(
	select s.ROW_KEY_ID, s.INVOICE_NUM, s.SELLER_INN, s.SELLER_KPP, d.DOC_ID, s.row_count
	from (select ROW_KEY_ID, INVOICE_NUM, SELLER_INN, SELLER_KPP, count(*) as row_count from NDS2_MRR_USER.doc_invoice_seller group by ROW_KEY_ID, INVOICE_NUM, SELLER_INN, SELLER_KPP) s
	join NDS2_MRR_USER.doc_invoice d on s.ROW_KEY_ID = d.INVOICE_ROW_KEY
);

drop table NDS2_MRR_USER.doc_invoice_seller;

alter table NDS2_MRR_USER.doc_invoice_seller_temp rename to doc_invoice_seller;

create index NDS2_MRR_USER.IDX_DI_BUY_ACCEPT on NDS2_MRR_USER.DOC_INVOICE_BUY_ACCEPT(DOC_ID, ROW_KEY_ID);

create index NDS2_MRR_USER.IDX_DI_BUYER on NDS2_MRR_USER.DOC_INVOICE_BUYER(DOC_ID, ROW_KEY_ID);

create index NDS2_MRR_USER.IDX_DI_OPERATION on NDS2_MRR_USER.DOC_INVOICE_OPERATION(DOC_ID, ROW_KEY_ID);

create index NDS2_MRR_USER.IDX_DI_PAYMENT_DOC on NDS2_MRR_USER.DOC_INVOICE_PAYMENT_DOC(DOC_ID, ROW_KEY_ID);

create index NDS2_MRR_USER.IDX_DI_SELLER on NDS2_MRR_USER.DOC_INVOICE_SELLER(DOC_ID, ROW_KEY_ID);

commit;

/


