﻿--скрипт миграции старых записей в selection_discrepancy (брать из sov_discrepancy)

create or replace force view NDS2_MRR_USER.V$SELECTION_DISCREPANCY_BUILD as 
select 
   discrepancy_id
  ,sd.is_in_process
  ,sd.selection_id
  ,sd.zip
  ,sd.seller_zip
  ,sd.buyer_zip
  ,sd.amount
  ,sd.amountpvp
  ,sd.sov_id
  ,sd.founddate
  ,sd.type
  ,sd.rule
  ,sd.stage
  ,sd.status
  ,sd.invoice_rk
  ,sd.subtype_code
  ,sd.side_primary_processing
  ,sd.buyer_region_code
  ,sd.buyer_sono_code
  ,sd.buyer_inn
  ,sd.buyer_inn_declarant
  ,sd.buyer_kpp
  ,sd.buyer_kpp_declarant
  ,sd.buyer_ks_r8r3_quality
  ,sd.buyer_decl_sign
  ,sd.buyer_decl_submit_date
  ,sd.buyer_decl_nds_amount
  ,sd.buyer_decl_period
  ,sd.buyer_operation_type
  ,sd.buyer_source
  ,sd.buyer_sur_code
  ,sd.seller_inn
  ,sd.seller_inn_declarant
  ,sd.seller_kpp
  ,sd.seller_kpp_declarant
  ,sd.seller_ks_1_27
  ,sd.seller_decl_sign
  ,sd.seller_decl_submit_date
  ,sd.seller_decl_nds_amount
  ,sd.seller_decl_period
  ,sd.seller_source
  ,sd.seller_sur_code
  ,sd.seller_region_code
  ,sd.seller_sono_code
  ,case 
    when sovd.created_by_invoice_no is not null then sovd.created_by_invoice_no 
    else sd.created_by_invoice_no 
    end as created_by_invoice_no
  ,case 
    when sovd.created_by_invoice_date is not null then sovd.created_by_invoice_date 
    else sd.created_by_invoice_date 
    end as created_by_invoice_date
from nds2_mrr_user.selection_discrepancy sd
    left join nds2_mrr_user.sov_discrepancy sovd on sd.discrepancy_id = sovd.id;
/

begin
  -- Call the procedure
  nds2_mrr_user.pac$mview_manager.p$update_table(
   p_temp_table_name => 'selection_discrepancy_tmp'
  ,p_backup_table_name => 'selection_discrepancy_bak'
  ,p_original_table_name => 'selection_discrepancy'
  ,p_sorce_view => 'v$selection_discrepancy_build'
  ,p_index_key => 'selection_discrepancy');
end;
/
drop view NDS2_MRR_USER.V$SELECTION_DISCREPANCY_BUILD;
/

commit;

/
