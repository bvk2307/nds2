﻿create table NDS2_MRR_USER.ASK_INVOICE_DECL_QUEUE_STATUS
(
  status_id NUMBER primary key,
  description varchar2(128 char)
);


comment on table NDS2_MRR_USER.ASK_INVOICE_DECL_QUEUE_STATUS is 'Перечень статусов обработки очереди';
truncate table NDS2_MRR_USER.ASK_INVOICE_DECL_QUEUE_STATUS;
insert into NDS2_MRR_USER.ASK_INVOICE_DECL_QUEUE_STATUS(STATUS_ID, DESCRIPTION)  values(1, 'Обработано успешно');
insert into NDS2_MRR_USER.ASK_INVOICE_DECL_QUEUE_STATUS(STATUS_ID, DESCRIPTION)  values(3, 'Ошибка обработки');
insert into NDS2_MRR_USER.ASK_INVOICE_DECL_QUEUE_STATUS(STATUS_ID, DESCRIPTION)  values(4, 'Пропущено в связи с закрытием КНП');
commit;
