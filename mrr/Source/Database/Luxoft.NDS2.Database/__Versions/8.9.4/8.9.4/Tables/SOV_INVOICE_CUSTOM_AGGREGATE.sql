﻿begin
  execute immediate 'drop table NDS2_MRR_USER.SOV_INVOICE_CUSTOM_AGGREGATE purge';
  exception when others then null;
end;
/

create table NDS2_MRR_USER.SOV_INVOICE_CUSTOM_AGGREGATE
(
  INVOICE_ROW_KEY varchar2(128 char),
  CUSTOMS_DECLARATION_NUM varchar2(128 char)
) nologging;
