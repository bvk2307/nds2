﻿-- Create table
create table NDS2_MRR_USER.SELECTION_EXCLUDE_LOG
(
  selection_id NUMBER,
  dt_procesed  DATE
);
-- Create/Recreate indexes 
create index NDS2_MRR_USER.IDX_SELECTION_EXCLUDE_LOG_ID on NDS2_MRR_USER.SELECTION_EXCLUDE_LOG (SELECTION_ID) tablespace NDS2_IDX;
comment on table NDS2_MRR_USER.SELECTION_EXCLUDE_LOG is 'Перечень обработанных выборок при исключении НД';
