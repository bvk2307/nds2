begin
	for dh in   (
		select distinct h.*
		from declaration_history h
		inner join selection_discrepancy sd on sd.buyer_inn = h.inn_declarant
		where rownum <= 130)
	loop
		insert into doc(
			doc_id,
			ref_entity_id,
			doc_type,
			doc_kind,
			sono_code,
			create_date,
			status_date,
			status,
			inn,
			kpp_effective,
			processed,
			close_date,
			user_comment)
		values (
			seq_doc.nextval, 
			dh.reg_number, 
			1, 
			1, 
			dh.sono_code_submited, 
			sysdate, sysdate, 
			1, 
			dh.inn_declarant, 
			dh.kpp_effective,
			0,
			sysdate - 1,
			'CREATE_SELECTION_2nd_SIDE');
	end loop; 

	for sd in   (
		select distinct
			d.doc_id
			,sd.discrepancy_id
			,sd.invoice_rk
		from selection_discrepancy sd 
		inner join doc d on sd.buyer_inn = d.inn
		where user_comment = 'CREATE_SELECTION_2nd_SIDE'
		)
	loop
		insert  into doc_discrepancy(
			doc_id,
			discrepancy_id,
			row_key	)
		values (
			sd.doc_id, 
			sd.discrepancy_id, 
			sd.invoice_rk);
			
		update sov_discrepancy set
			created_by_invoice_no = 'CREATE_SELECTION_2nd_SIDE test',
			created_by_invoice_date = sysdate - 1
    where id = sd.discrepancy_id;
  end loop; 
end;
/
commit;

--delete doc where user_comment = 'CREATE_SELECTION_2nd_SIDE';
--truncate table doc_discrepancy;

