
begin
for j in (select * from all_scheduler_jobs where owner = 'NDS2_MRR_USER' and job_name = 'J$DAILY_JOB')
  loop
    execute immediate 'begin dbms_scheduler.drop_job(job_name => ''NDS2_MRR_USER.'||j.job_name||''', force => true); end;';
  end loop;
  
for pr in (select p.PROGRAM_NAME from all_scheduler_programs p 
                  inner join all_scheduler_chain_steps s on s.OWNER = p.OWNER 
                  and s.PROGRAM_NAME = p.PROGRAM_NAME 
                  where p.owner = 'NDS2_MRR_USER' and s.CHAIN_NAME = 'BACKGROUND_DAILY_TASKS')
  loop
    execute immediate 'begin dbms_scheduler.drop_program(program_name => ''NDS2_MRR_USER.'||pr.PROGRAM_NAME||''', force => true); end;';
  end loop;

for st in (select * from all_scheduler_chain_steps where owner = 'NDS2_MRR_USER' and chain_name = 'BACKGROUND_DAILY_TASKS')
  loop
    execute immediate 'begin  dbms_scheduler.drop_chain_step(chain_name => ''NDS2_MRR_USER.BACKGROUND_DAILY_TASKS'' , step_name =>'''||st.step_name||''' , force => true); end;';
  end loop;


for ch in (select * from all_scheduler_chains where owner = 'NDS2_MRR_USER' and chain_name = 'BACKGROUND_DAILY_TASKS')
  loop
    execute immediate 'begin  dbms_scheduler.drop_chain(chain_name => ''NDS2_MRR_USER.BACKGROUND_DAILY_TASKS''); end;';
  end loop;
  
for pr in (select * from all_scheduler_programs where owner = 'NDS2_MRR_USER' and PROGRAM_NAME in ('P$RECALCULATE_SLIDE_AT', 'P$UPDATE_WHITE_LIST','P$DAILY_TASK_FINISH','P$DECLARATION_AGGREGATES','P$KNP_AGGREGATES','P$DECLARATION_SUMMARY_REPORT','P$DICTIONARY'))
  loop
    execute immediate 'begin dbms_scheduler.drop_program(program_name => ''NDS2_MRR_USER.'||pr.PROGRAM_NAME||''', force => true); end;';
  end loop;
end;
/

begin
  DBMS_SCHEDULER.create_program(
       program_name => 'NDS2_MRR_USER.P$DICTIONARY'
      ,program_type => 'PLSQL_BLOCK'
      ,program_action => 'BEGIN
      NDS2$SYS.LOG_INFO(''BACKGROUND_DAILY_TASKS'', 0, 0, ''P$DICTIONARY'');
      PAC$SYNC.START_WORK;
      END;'
      ,number_of_arguments => 0
      ,enabled => true
      ,comments => '���������� ������������ ����');
end;
/

begin
  DBMS_SCHEDULER.create_program(
       program_name => 'NDS2_MRR_USER.P$USER_TASK_DAILY_PROCESS'
      ,program_type => 'PLSQL_BLOCK'
      ,program_action => 'BEGIN
      NDS2$SYS.LOG_INFO(''BACKGROUND_DAILY_TASKS'', 0, 0, ''P$USER_TASK_DAILY_PROCESS'');
      pac$user_task.p$run_daily_process;
      END;'
      ,number_of_arguments => 0
      ,enabled => true
      ,comments => '������������ ��');
end;
/

begin
  DBMS_SCHEDULER.create_program(
       program_name => 'NDS2_MRR_USER.P$DECLARATION_SUMMARY_REPORT'
      ,program_type => 'PLSQL_BLOCK'
      ,program_action => 'BEGIN
      NDS2$SYS.LOG_INFO(''BACKGROUND_DAILY_TASKS'', 0, 0, ''P$DECLARATION_SUMMARY_REPORT'');
      NDS2$REPORTS.START_DECLARATION_CALCULATE;
      END;'
      ,number_of_arguments => 0
      ,enabled => true
      ,comments => '���������� ������ ������� ����������');
end;
/

begin
  DBMS_SCHEDULER.create_program(
       program_name => 'NDS2_MRR_USER.P$KNP_AGGREGATES'
      ,program_type => 'PLSQL_BLOCK'
      ,program_action => 'BEGIN
      NDS2$SYS.LOG_INFO(''BACKGROUND_DAILY_TASKS'', 0, 0, ''P$KNP_AGGREGATES'');
      PAC$NDS2_FULL_CYCLE.P$UPDATE_DICREPANCY_DAILY;
      END;'
      ,number_of_arguments => 0
      ,enabled => true
      ,comments => '������������ ��������� � ������� �������� ����������� ��� ���');  
end;
/

begin
  DBMS_SCHEDULER.create_program(
       program_name => 'NDS2_MRR_USER.P$DECLARATION_AGGREGATES'
      ,program_type => 'PLSQL_BLOCK'
      ,program_action => 'BEGIN
      NDS2$SYS.LOG_INFO(''BACKGROUND_DAILY_TASKS'', 0, 0, ''P$DECLARATION_AGGREGATES'');
      PAC$MVIEW_MANAGER.REFRESH_DECLARATONS;
      END;'
      ,number_of_arguments => 0
      ,enabled => true
      ,comments => '������������ ��������� � ������� ����������');
end;
/

begin
  DBMS_SCHEDULER.create_program(
       program_name => 'NDS2_MRR_USER.P$DAILY_TASK_FINISH'
      ,program_type => 'PLSQL_BLOCK'
      ,program_action => 'BEGIN
      NDS2$SYS.LOG_INFO(''BACKGROUND_DAILY_TASKS'', 0, 0, ''P$DAILY_TASK_FINISH'');
      END;'
      ,number_of_arguments => 0
      ,enabled => true
      ,comments => '������������ ��������� � ������� ����������');
end;
/

begin
  DBMS_SCHEDULER.create_program(
       program_name => 'NDS2_MRR_USER.P$UPDATE_WHITE_LIST'
      ,program_type => 'PLSQL_BLOCK'
      ,program_action => 'BEGIN
      NDS2$SYS.LOG_INFO(''BACKGROUND_DAILY_TASKS'', 0, 0, ''P$UPDATE_WHITE_LIST'');
      PAC$TAX_PAYER_SETTINGS.UPDATE_WHITE_LIST;
      END;'
      ,number_of_arguments => 0
      ,enabled => true
      ,comments => '���������� �������� IDA_TAX_PAYER_WHITE_LIST');
end;
/

begin
  DBMS_SCHEDULER.create_program(
       program_name => 'NDS2_MRR_USER.P$RECALCULATE_SLIDE_AT'
      ,program_type => 'PLSQL_BLOCK'
      ,program_action => 'BEGIN
      NDS2$SYS.LOG_INFO(''BACKGROUND_DAILY_TASKS'', 0, 0, ''P$RECALCULATE_SLIDE_AT'');
      PAC$REMARKABLE_CHANGES.RECALC_SLIDE_AT;
      END;'
      ,number_of_arguments => 0
      ,enabled => true
      ,comments => '���������� ���������� ������������ �������� ��'
  );
end;
/

begin    
  DBMS_SCHEDULER.CREATE_CHAIN(chain_name => 'NDS2_MRR_USER.BACKGROUND_DAILY_TASKS');
end;
/

begin
  DBMS_SCHEDULER.DEFINE_CHAIN_STEP(
       chain_name => 'NDS2_MRR_USER.BACKGROUND_DAILY_TASKS'
      ,step_name => 'STEP1'
      ,program_name => 'P$DICTIONARY');
end;
/

begin
  DBMS_SCHEDULER.DEFINE_CHAIN_STEP(
       chain_name => 'NDS2_MRR_USER.BACKGROUND_DAILY_TASKS'
      ,step_name => 'STEP2'
      ,program_name => 'P$KNP_AGGREGATES');
end;
/

begin
  DBMS_SCHEDULER.DEFINE_CHAIN_STEP(
       chain_name => 'NDS2_MRR_USER.BACKGROUND_DAILY_TASKS'
      ,step_name => 'STEP3'
      ,program_name => 'P$DECLARATION_AGGREGATES');
end;
/

begin
  DBMS_SCHEDULER.DEFINE_CHAIN_STEP(
       chain_name => 'NDS2_MRR_USER.BACKGROUND_DAILY_TASKS'
      ,step_name => 'STEP4'
      ,program_name => 'P$DECLARATION_SUMMARY_REPORT');
end;
/

begin
  DBMS_SCHEDULER.DEFINE_CHAIN_STEP(
       chain_name => 'NDS2_MRR_USER.BACKGROUND_DAILY_TASKS'
      ,step_name => 'STEP5'
      ,program_name => 'P$UPDATE_WHITE_LIST');
end;
/

begin
  DBMS_SCHEDULER.DEFINE_CHAIN_STEP(
       chain_name => 'NDS2_MRR_USER.BACKGROUND_DAILY_TASKS'
      ,step_name => 'STEP6'
      ,program_name => 'P$RECALCULATE_SLIDE_AT'
  );
end;
/

begin
  DBMS_SCHEDULER.DEFINE_CHAIN_STEP(
       chain_name => 'NDS2_MRR_USER.BACKGROUND_DAILY_TASKS'
      ,step_name => 'STEP7'
      ,program_name => 'P$USER_TASK_DAILY_PROCESS'
  );
end;
/


begin
  DBMS_SCHEDULER.DEFINE_CHAIN_STEP(
       chain_name => 'NDS2_MRR_USER.BACKGROUND_DAILY_TASKS'
      ,step_name => 'STEPEND'
      ,program_name => 'P$DAILY_TASK_FINISH');
end;
/

begin
  DBMS_SCHEDULER.DEFINE_CHAIN_RULE(
       chain_name => 'NDS2_MRR_USER.BACKGROUND_DAILY_TASKS'
    ,condition => 'true' 
    ,rule_name => 'rule1�'
    ,action => 'START "STEP1"'
  );
end;
/

begin
  DBMS_SCHEDULER.DEFINE_CHAIN_RULE(
       chain_name => 'NDS2_MRR_USER.BACKGROUND_DAILY_TASKS'
    ,condition => 'STEP1 COMPLETED' 
    ,rule_name => 'rule2x'
    ,action => 'START "STEP2"'
  ); 
end;
/

begin
  DBMS_SCHEDULER.DEFINE_CHAIN_RULE(
       chain_name => 'NDS2_MRR_USER.BACKGROUND_DAILY_TASKS'
    ,condition => 'STEP2 COMPLETED' 
    ,rule_name => 'rule3x'
    ,action => 'START "STEP3"'
  ); 
end;
/

begin  
  DBMS_SCHEDULER.DEFINE_CHAIN_RULE(
       chain_name => 'NDS2_MRR_USER.BACKGROUND_DAILY_TASKS'
    ,condition => 'STEP3 COMPLETED' 
    ,rule_name => 'rule4x'
    ,action => 'START "STEP4"'
  );  
end;
/

begin
  DBMS_SCHEDULER.DEFINE_CHAIN_RULE(
       chain_name => 'NDS2_MRR_USER.BACKGROUND_DAILY_TASKS'
    ,condition => 'STEP4 COMPLETED' 
    ,rule_name => 'rule5x'
    ,action => 'START "STEP5"'
  );    

end;
/

begin
  DBMS_SCHEDULER.DEFINE_CHAIN_RULE(
       chain_name => 'NDS2_MRR_USER.BACKGROUND_DAILY_TASKS'
	  ,condition => 'STEP5 COMPLETED' 
	  ,rule_name => 'rule6x'
	  ,action => 'START "STEP6"'
  );
end;
/

begin
  DBMS_SCHEDULER.DEFINE_CHAIN_RULE(
       chain_name => 'NDS2_MRR_USER.BACKGROUND_DAILY_TASKS'
	  ,condition => 'STEP6 COMPLETED'
	  ,rule_name => 'rule7x'
	  ,action => 'START "STEP7"'
  ); 
end;
/

begin
  DBMS_SCHEDULER.DEFINE_CHAIN_RULE(
       chain_name => 'NDS2_MRR_USER.BACKGROUND_DAILY_TASKS'
	  ,condition => 'STEP7 COMPLETED'
	  ,rule_name => 'rule8x'
	  ,action => 'START "STEPEND"'
  ); 
end;
/

begin  
  DBMS_SCHEDULER.DEFINE_CHAIN_RULE(
       chain_name => 'NDS2_MRR_USER.BACKGROUND_DAILY_TASKS'
	  ,condition => 'STEPEND COMPLETED'
	  ,rule_name => 'ruleend2x'
	  ,action => 'END'
  ); 
end;
/

begin  
  DBMS_SCHEDULER.ENABLE('NDS2_MRR_USER.BACKGROUND_DAILY_TASKS');
end;
/

begin  
  DBMS_SCHEDULER.create_job(
       job_name => 'NDS2_MRR_USER.J$DAILY_JOB'
      ,job_type => 'CHAIN'
      ,job_action => 'NDS2_MRR_USER.BACKGROUND_DAILY_TASKS'
      ,start_date => TO_DATE(TO_CHAR(SYSDATE, 'dd/mm/yyyy') || ' 00:10:00', 'dd/mm/yyyy HH24:mi:ss') + INTERVAL '1' MINUTE
      ,repeat_interval => 'freq=Daily;Interval=1'
      ,end_date => to_date('01-01-2100 00:00:00', 'dd-mm-yyyy hh24:mi:ss')
      ,job_class => 'DEFAULT_JOB_CLASS'
      ,enabled => false
      ,auto_drop => false 
      ,comments => '���� ���������� �����');
end;
/
  
