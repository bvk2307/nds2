﻿create table NDS2_MRR_USER.SOV_INVOICE_PARTITION_DETALS
(
  partition_id number,
  zip number
);

create index NDS2_MRR_USER.idx_si_part_det_id on NDS2_MRR_USER.SOV_INVOICE_PARTITION_DETALS(partition_id);
