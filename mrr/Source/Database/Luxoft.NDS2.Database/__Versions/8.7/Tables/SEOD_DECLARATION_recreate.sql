alter session set current_schema = NDS2_MRR_USER;
-- Create table
create table NDS2_MRR_USER.SEOD_DECLARATION_N
(
  id                NUMBER,
  nds2_id           NUMBER not null,
  type              NUMBER(1) not null,
  sono_code         VARCHAR2(4 CHAR) not null,
  tax_period        VARCHAR2(2 CHAR) not null,
  fiscal_year       VARCHAR2(4 CHAR) not null,
  correction_number NUMBER(5) not null,
  decl_reg_num      NUMBER(20) not null,
  decl_fid          VARCHAR2(512 CHAR) not null,
  tax_payer_type    NUMBER(1) not null,
  inn               VARCHAR2(12 CHAR) not null,
  kpp               VARCHAR2(9 CHAR),
  insert_date       DATE not null,
  eod_date          DATE,
  date_receipt      DATE,
  delivery_type     NUMBER(2) default 0,
  id_file           VARCHAR2(1024 CHAR) default 0,
  is_imported       NUMBER(1)
);

insert /*+ APPEND */ into NDS2_MRR_USER.SEOD_DECLARATION_N select * from NDS2_MRR_USER.SEOD_DECLARATION;
commit;
-- Create/Recreate indexes 
create index IDX_SEODDECL_REGNUM on NDS2_MRR_USER.SEOD_DECLARATION_N (DECL_REG_NUM)  tablespace NDS2_IDX compress;
create index IDX_SEODDECL_NDS2_ID on NDS2_MRR_USER.SEOD_DECLARATION_N (NDS2_ID)  tablespace NDS2_DATA;
create index IDX_SEODDECL_NDS2_ID_CORNUM on NDS2_MRR_USER.SEOD_DECLARATION_N (NDS2_ID, CORRECTION_NUMBER) tablespace NDS2_DATA;
create index IDX_SEODDECL_MERGE on NDS2_MRR_USER.SEOD_DECLARATION_N (DECL_REG_NUM, SONO_CODE, INN, NVL(KPP,'0'))  tablespace NDS2_DATA;

alter table NDS2_MRR_USER.SEOD_DECLARATION rename  to SEOD_DECLARATION_86;
alter table NDS2_MRR_USER.SEOD_DECLARATION_N rename to SEOD_DECLARATION;

