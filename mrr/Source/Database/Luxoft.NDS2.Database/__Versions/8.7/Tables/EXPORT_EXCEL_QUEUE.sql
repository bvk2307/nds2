create table NDS2_MRR_USER.EXPORT_EXCEL_QUEUE
(
  QUEUE_ITEM_ID NUMBER PRIMARY KEY,
  USER_SID VARCHAR2(50 char) NOT NULL,
  ENQUEUED DATE NOT NULL,
  STARTED DATE,
  STARTED_BY_ID VARCHAR2(36),
  COMPLETED DATE
);
