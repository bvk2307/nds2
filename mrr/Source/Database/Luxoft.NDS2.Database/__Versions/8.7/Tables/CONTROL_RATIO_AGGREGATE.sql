﻿create table NDS2_MRR_USER.CONTROL_RATIO_AGGREGATE
(
  zip       NUMBER not null,
  r8r3_equality	NUMBER,
  left_1_28 NUMBER,
  left_1_32 NUMBER,
  left_1_33 NUMBER,
  hasdiscrepancy_1_27 NUMBER,
  vypoln_1_27 NUMBER,
  CONSTRAINT PK$CONTROLRATIO_AGGREGATE PRIMARY KEY (zip) ENABLE
);
