ALTER TABLE NDS2_MRR_USER.DECLARATION_ACTIVE_SEARCH ADD GAP_COUNT_8    		NUMBER;
ALTER TABLE NDS2_MRR_USER.DECLARATION_ACTIVE_SEARCH ADD GAP_AMOUNT_8   		NUMBER;
ALTER TABLE NDS2_MRR_USER.DECLARATION_ACTIVE_SEARCH ADD OTHER_COUNT_8  		NUMBER;
ALTER TABLE NDS2_MRR_USER.DECLARATION_ACTIVE_SEARCH ADD OTHER_AMOUNT_8 		NUMBER;
ALTER TABLE NDS2_MRR_USER.DECLARATION_ACTIVE_SEARCH ADD GAP_COUNT_9    		NUMBER;
ALTER TABLE NDS2_MRR_USER.DECLARATION_ACTIVE_SEARCH ADD GAP_AMOUNT_9   		NUMBER;
ALTER TABLE NDS2_MRR_USER.DECLARATION_ACTIVE_SEARCH ADD OTHER_COUNT_9  		NUMBER;
ALTER TABLE NDS2_MRR_USER.DECLARATION_ACTIVE_SEARCH ADD OTHER_AMOUNT_9 		NUMBER;
ALTER TABLE NDS2_MRR_USER.DECLARATION_ACTIVE_SEARCH ADD GAP_COUNT_10   		NUMBER;
ALTER TABLE NDS2_MRR_USER.DECLARATION_ACTIVE_SEARCH ADD GAP_AMOUNT_10  		NUMBER;
ALTER TABLE NDS2_MRR_USER.DECLARATION_ACTIVE_SEARCH ADD OTHER_COUNT_10 		NUMBER;
ALTER TABLE NDS2_MRR_USER.DECLARATION_ACTIVE_SEARCH ADD OTHER_AMOUNT_10		NUMBER;
ALTER TABLE NDS2_MRR_USER.DECLARATION_ACTIVE_SEARCH ADD GAP_COUNT_11   		NUMBER;
ALTER TABLE NDS2_MRR_USER.DECLARATION_ACTIVE_SEARCH ADD GAP_AMOUNT_11  		NUMBER;
ALTER TABLE NDS2_MRR_USER.DECLARATION_ACTIVE_SEARCH ADD OTHER_COUNT_11 		NUMBER;
ALTER TABLE NDS2_MRR_USER.DECLARATION_ACTIVE_SEARCH ADD OTHER_AMOUNT_11		NUMBER;
ALTER TABLE NDS2_MRR_USER.DECLARATION_ACTIVE_SEARCH ADD GAP_COUNT_12   		NUMBER;
ALTER TABLE NDS2_MRR_USER.DECLARATION_ACTIVE_SEARCH ADD GAP_AMOUNT_12  		NUMBER;
ALTER TABLE NDS2_MRR_USER.DECLARATION_ACTIVE_SEARCH ADD OTHER_COUNT_12 		NUMBER;
ALTER TABLE NDS2_MRR_USER.DECLARATION_ACTIVE_SEARCH ADD OTHER_AMOUNT_12		NUMBER;
ALTER TABLE NDS2_MRR_USER.DECLARATION_ACTIVE_SEARCH ADD HAS_CHANGES			NUMBER;
ALTER TABLE NDS2_MRR_USER.DECLARATION_ACTIVE_SEARCH ADD HAS_AT				NUMBER;
ALTER TABLE NDS2_MRR_USER.DECLARATION_ACTIVE_SEARCH ADD SLIDE_AT_COUNT		NUMBER;
ALTER TABLE NDS2_MRR_USER.DECLARATION_ACTIVE_SEARCH ADD NO_TKS_COUNT		NUMBER;
ALTER TABLE NDS2_MRR_USER.DECLARATION_ACTIVE_SEARCH ADD total_knp_count		NUMBER;

ALTER TABLE NDS2_MRR_USER.DECLARATION_ACTIVE_SEARCH ADD Ch8_Present_Description	VARCHAR2(16);
ALTER TABLE NDS2_MRR_USER.DECLARATION_ACTIVE_SEARCH ADD Ch9_Present_Description	VARCHAR2(16);


ALTER TABLE NDS2_MRR_USER.DECLARATION_ACTIVE_SEARCH MODIFY PRPODP VARCHAR2(80);

Update NDS2_MRR_USER.DECLARATION_ACTIVE_SEARCH 
	Set PRPODP = '�������������� �������������'
	Where PRPODP = '��������������';
Update NDS2_MRR_USER.DECLARATION_ACTIVE_SEARCH 
	Set PRPODP = '�������� �������������'
	Where PRPODP = '��������';
commit;
