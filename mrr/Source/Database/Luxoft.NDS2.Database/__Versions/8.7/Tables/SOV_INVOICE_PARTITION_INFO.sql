﻿create table NDS2_MRR_USER.SOV_INVOICE_PARTITION_INFO
(
  id number,
  name varchar2(128 char),
  rows_count number,
  processed number(1)
);
alter table NDS2_MRR_USER.SOV_INVOICE_PARTITION_INFO add constraint pk_si_part_id primary key(id);
