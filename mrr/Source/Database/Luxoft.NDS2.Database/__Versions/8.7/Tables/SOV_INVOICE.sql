﻿-- Create table
truncate table NDS2_MRR_USER.SOV_INVOICE;

drop table NDS2_MRR_USER.SOV_INVOICE purge;

create table NDS2_MRR_USER.SOV_INVOICE
(
  request_id                NUMBER not null,
  declaration_version_id    NUMBER,
  chapter                   NUMBER(2),
  ordinal_number            NUMBER(19),
  okv_code                  VARCHAR2(3 CHAR),
  create_date               DATE,
  receive_date              DATE,
  operation_code            VARCHAR2(64 CHAR),
  invoice_num               VARCHAR2(1024 CHAR),
  invoice_date              DATE,
  change_num                VARCHAR2(256 CHAR),
  change_date               DATE,
  correction_num            VARCHAR2(256 CHAR),
  correction_date           DATE,
  change_correction_num     VARCHAR2(256 CHAR),
  change_correction_date    DATE,
  receipt_doc_num           VARCHAR2(64 CHAR),
  receipt_doc_date          VARCHAR2(64 CHAR),
  buy_accept_date           VARCHAR2(64 CHAR),
  buyer_inn                 VARCHAR2(64 CHAR),
  buyer_kpp                 VARCHAR2(64 CHAR),
  seller_inn                VARCHAR2(64 CHAR),
  seller_kpp                VARCHAR2(64 CHAR),
  seller_invoice_num        VARCHAR2(4000 CHAR),
  seller_invoice_date       DATE,
  broker_inn                VARCHAR2(12 CHAR),
  broker_kpp                VARCHAR2(9 CHAR),
  deal_kind_code            NUMBER,
  customs_declaration_num   VARCHAR2(1000 CHAR),
  price_buy_amount          NUMBER(19,2),
  price_buy_nds_amount      NUMBER(19,2),
  price_sell                NUMBER(19,2),
  price_sell_in_curr        NUMBER(19,2),
  price_sell_18             NUMBER(19,2),
  price_sell_10             NUMBER(19,2),
  price_sell_0              NUMBER(19,2),
  price_nds_18              NUMBER(19,2),
  price_nds_10              NUMBER(19,2),
  price_tax_free            NUMBER(19,2),
  price_total               NUMBER(19,2),
  price_nds_total           NUMBER(19,2),
  diff_correct_decrease     NUMBER(19,2),
  diff_correct_increase     NUMBER(19,2),
  diff_correct_nds_decrease NUMBER(19,2),
  diff_correct_nds_increase NUMBER(19,2),
  price_nds_buyer           NUMBER(19,2),
  row_key                   VARCHAR2(128 CHAR) not null,
  actual_row_key            VARCHAR2(128 CHAR),
  compare_row_key           VARCHAR2(2048 CHAR),
  compare_algo_id           NUMBER(2),
  format_errors             VARCHAR2(128 CHAR),
  logical_errors            VARCHAR2(128 CHAR),
  seller_agency_info_inn    VARCHAR2(12 CHAR),
  seller_agency_info_kpp    VARCHAR2(9 CHAR),
  seller_agency_info_name   VARCHAR2(512 CHAR),
  seller_agency_info_num    VARCHAR2(128 CHAR),
  seller_agency_info_date   DATE,
  is_dop_list               NUMBER(1),
  is_import                 NUMBER(1),
  clarification_key         VARCHAR2(128 CHAR),
  contragent_key            NUMBER default 0,
  is_clarified              NUMBER(1)
)
partition by list (declaration_version_id)
(
  partition SI_DEF values (default) pctfree 10 initrans 10 compress
);
-- Create/Recreate indexes 

create index NDS2_MRR_USER.IDX_SI_ZIP_CHAP_RK on NDS2_MRR_USER.SOV_INVOICE (declaration_version_id, chapter, row_key)
local  tablespace NDS2_IDX pctfree 10 initrans 2 maxtrans 255 compress;

create index NDS2_MRR_USER.IDX_SI_ZIP_CHAPTER on NDS2_MRR_USER.SOV_INVOICE (declaration_version_id, chapter)
local  tablespace NDS2_IDX pctfree 10 initrans 2 maxtrans 255 compress;
