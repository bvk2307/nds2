﻿create sequence NDS2_MRR_USER.seq_sov_invoice_partition
start with 1
increment by 1
minvalue 1
maxvalue 9999999999
nocycle
nocache;
