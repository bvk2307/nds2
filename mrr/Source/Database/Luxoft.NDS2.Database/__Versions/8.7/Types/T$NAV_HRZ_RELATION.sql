create or replace 
TYPE NDS2_MRR_USER.T$NAV_HRZ_RELATION as object (
  INN_1                        VARCHAR2(12 CHAR),
  INN_2                        VARCHAR2(12 CHAR),
  LVL                          NUMBER(2),
  CHAIN_ID                     NUMBER,
  PATH                         VARCHAR2(1000 CHAR),
  HEAD                         VARCHAR2(12 CHAR),
  TAIL                         VARCHAR2(12 CHAR),  
  NDS_SALES                    NUMBER,
  NDS_PURCHASE                 NUMBER,
  NOT_MAPPED_NDS               NUMBER,
  MAPPED_NDS                   NUMBER,
  NDS_DEDUCTION                NUMBER,
  NDS_CALCULATED               NUMBER,  
  MAPPED_NDS_REV               NUMBER,
  NDS_DEDUCTION_REV            NUMBER,
  NDS_CALCULATED_REV           NUMBER,
  SUR_CODE                     NUMBER(1)
);
/
