﻿begin
  dbms_scheduler.drop_job(job_name => 'NDS2_MRR_USER.J$KILL_EXPIRED_SESSIONS');
  exception when others then null;
end;
/
begin 
  sys.dbms_scheduler.create_job(
    job_name => 'NDS2_MRR_USER.J$KILL_EXPIRED_SESSIONS', 
    job_type => 'STORED_PROCEDURE', 
    job_action => 'PAC$KNP_RESULT_DOCUMENTS.P$KILL_EXPIRED_SESSIONS', 
    start_date => SYSDATE + INTERVAL '600' SECOND, 
    repeat_interval => 'freq=hourly;Interval=1',
    end_date => to_date('01-01-2100 00:00:00', 'dd-mm-yyyy hh24:mi:ss'), 
    job_class => 'DEFAULT_JOB_CLASS', 
    enabled => false, 
    auto_drop => false, 
    comments => 'Очищение сессиий отбора расхождений в акты\решения, время жизни которых истекло'); 
end; 
/
