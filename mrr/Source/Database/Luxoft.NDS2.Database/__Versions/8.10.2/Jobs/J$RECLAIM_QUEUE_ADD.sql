﻿begin 
  sys.dbms_scheduler.create_job(job_name => 'NDS2_MRR_USER.J$RECLAIM_QUEUE_ADD', 
  job_type => 'PLSQL_BLOCK', 
  job_action => 'begin PAC$RECLAIM.P$FILL_QUEUE; end;', 
  start_date => TO_DATE(TO_CHAR(SYSDATE, 'dd/mm/yyyy') || ' 04:00:00', 'dd/mm/yyyy HH24:mi:ss') + INTERVAL '1' MINUTE, 
  repeat_interval => 'freq=Hourly;Interval=1', 
  end_date => to_date('01-01-2100 00:00:00', 'dd-mm-yyyy hh24:mi:ss'), 
  job_class => 'DEFAULT_JOB_CLASS', 
  enabled => false, 
  auto_drop => false, 
  comments => 'Формирование очереди рег. номеров для создания АИ'); 
end; 
/
