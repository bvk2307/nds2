﻿
/*ТОЛЬКО ДЛЯ ТЕСТОВОГО ОКРУЖЕНИЯ */
update NDS2_MRR_USER.doc  d
set 
       zip = (
             select 
              d.ZIP
             from 
             doc t1 
             inner join seod_declaration sd on sd.decl_reg_num = t1.ref_entity_id
             and sd.sono_code = t1.sono_code
             inner join v$askdekl d on d.KODNO = sd.sono_code and d.INNNP = sd.inn and d.IDFAJL = sd.id_file
             where t1.doc_id = d.doc_id and rownum  = 1),
        quarter = (
             select 
              f$tax_period_to_quarter(d.PERIOD)
             from 
             doc t1 
             inner join seod_declaration sd on sd.decl_reg_num = t1.ref_entity_id
             and sd.sono_code = t1.sono_code
             inner join v$askdekl d on d.KODNO = sd.sono_code and d.INNNP = sd.inn and d.IDFAJL = sd.id_file
             where t1.doc_id = d.doc_id and rownum  = 1),
        fiscal_year = (
             select 
              f$tax_period_to_quarter(d.PERIOD)
             from 
             doc t1 
             inner join seod_declaration sd on sd.decl_reg_num = t1.ref_entity_id
             and sd.sono_code = t1.sono_code
             inner join v$askdekl d on d.KODNO = sd.sono_code and d.INNNP = sd.inn and d.IDFAJL = sd.id_file
             where t1.doc_id = d.doc_id and rownum  = 1);

commit;
