﻿CREATE OR REPLACE TYPE NDS2_MRR_USER.T$RECLAIM_DOC
  AS
  OBJECT 
  (
    doc_id                   NUMBER,
    doc_type                 NUMBER,
    doc_kind                 NUMBER,
    close_reason             NUMBER
  );
/
  
CREATE OR REPLACE TYPE NDS2_MRR_USER.T$RECLAIM_DOCS AS
TABLE OF NDS2_MRR_USER.T$RECLAIM_DOC;
/