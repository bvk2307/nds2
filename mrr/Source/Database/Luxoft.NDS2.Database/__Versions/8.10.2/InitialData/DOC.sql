﻿-- миграция close_reason
--
-- причина закрытия: по таймауту
update NDS2_MRR_USER.doc t set t.close_reason = 1
where t.doc_id in
(
  select d.doc_id 
  from NDS2_MRR_USER.doc d
  left join NDS2_MRR_USER.DOC_KNP_CLOSED k on k.doc_id = d.doc_id
  join NDS2_MRR_USER.declaration_history dh on dh.sono_code_submited = d.sono_code and dh.reg_number = d.ref_entity_id
  where k.doc_id is null and d.doc_type = 1 and d.status = 10 and dh.is_active = 1 
);
commit;

-- причина закрытия: новая корректировка
update NDS2_MRR_USER.doc t set t.close_reason = 2
where t.doc_id in
(
  select d.doc_id 
  from NDS2_MRR_USER.doc d
  left join NDS2_MRR_USER.DOC_KNP_CLOSED k on k.doc_id = d.doc_id
  join NDS2_MRR_USER.declaration_history dh on dh.sono_code_submited = d.sono_code and dh.reg_number = d.ref_entity_id
  where k.doc_id is null and d.doc_type = 1 and d.status = 10 and dh.is_active = 0 
);
commit;