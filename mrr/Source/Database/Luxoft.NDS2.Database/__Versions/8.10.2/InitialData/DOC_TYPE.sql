﻿truncate table NDS2_MRR_USER.DOC_TYPE;

insert into NDS2_MRR_USER.DOC_TYPE (ID, DESCRIPTION, FLOW_NUMBER)
values (1, 'Автотребование по СФ', 1);

insert into NDS2_MRR_USER.DOC_TYPE (ID, DESCRIPTION, FLOW_NUMBER)
values (2, 'Автоистребование по ст. 93', 2);

insert into NDS2_MRR_USER.DOC_TYPE (ID, DESCRIPTION, FLOW_NUMBER)
values (3, 'Автоистребование по ст. 93.1', 3);

insert into NDS2_MRR_USER.DOC_TYPE (ID, DESCRIPTION, FLOW_NUMBER)
values (4, 'Информация о наличии расхождений в декларации', 4);

insert into NDS2_MRR_USER.DOC_TYPE (ID, DESCRIPTION, FLOW_NUMBER)
values (5, 'Автотребование по КС', 1);

insert into NDS2_MRR_USER.DOC_TYPE (ID, DESCRIPTION, FLOW_NUMBER)
values (6, 'Запрос о предоставлении выписки по операциям на счетах', 5);

commit;
