﻿merge into NDS2_MRR_USER.CONFIGURATION cfg
using (select 'BankAccountRequestDate' as parameter, TO_CHAR(trunc(sysdate - 1), 'DD-MM-YYYY HH24:MI:SS') as value from dual) src on (src.parameter = cfg.parameter)
when not matched then
  insert (parameter, value, default_value, description)
  values (src.parameter, src.value, src.value, 'Дата последнего запроса по банковским счетам');

merge into NDS2_MRR_USER.CONFIGURATION cfg
using (select 'BankAccountPullLimit' as parameter, 5 as value from dual) src on (src.parameter = cfg.parameter)
when not matched then
  insert (parameter, value, default_value, description)
  values (src.parameter, src.value, src.value, 'Количество запросов, обрабатываемых за один раз при формировании XML');
  
merge into NDS2_MRR_USER.CONFIGURATION c
    using (select 'reclaim_job_thread_count_max' as parameter from dual) par
      on (par.parameter = c.parameter)
    when not matched then
      insert (parameter, value, default_value, description)
      values ('reclaim_job_thread_count_max', 5, 5, 'Сервис формирования истребований: максимальное количество потоков');

merge into NDS2_MRR_USER.CONFIGURATION c
    using (select 'reclaim_log_info_message_use' as parameter from dual) par
      on (par.parameter = c.parameter)
    when not matched then
      insert (parameter, value, default_value, description)
      values ('reclaim_log_info_message_use', 'Y', 'Y', 'Сервис формирования истребований: логировать информационные сообщения');

merge into NDS2_MRR_USER.CONFIGURATION c
    using (select 'reclaim_manual_check_doc_use' as parameter from dual) par
      on (par.parameter = c.parameter)
    when not matched then
      insert (parameter, value, default_value, description)
	  values ('reclaim_manual_check_doc_use', 'Y', 'Y', 'Сервис формирования истребований: выставлять статус у документа - ручная проверка');
    
insert into NDS2_MRR_USER.CONFIGURATION (PARAMETER,VALUE,DEFAULT_VALUE,DESCRIPTION)
  values ('act_decision_session_duration', '7200', '7200', 'Длительность сессии работы с Актами и Решениями в секундах');
  
insert into NDS2_MRR_USER.CONFIGURATION (PARAMETER,VALUE,DEFAULT_VALUE,DESCRIPTION)
  values ('decision_close_enabled','1','1','Блокировка функции закрытия решения');

update NDS2_MRR_USER.CONFIGURATION c set c.value = '6', c.default_value = '6'
where c.parameter = 'timeout_claim_delivery';

update NDS2_MRR_USER.CONFIGURATION c set c.value = '8', c.default_value = '8'
where c.parameter = 'timeout_answer_for_autoclaim';

commit;

