﻿update nds2_mrr_user.QUEUE_SEOD_STREAM_PROCESS t set t.is_processed = 1
where t.is_processed is null and t.processed is not null;

update nds2_mrr_user.QUEUE_SEOD_STREAM_PROCESS t set t.is_processed = 0
where t.is_processed is null and t.processed is null;

commit;