﻿begin
  execute immediate 'drop table NDS2_MRR_USER.RM_CSUD_OPERATION';
  exception when others then null;
end;
/

create table NDS2_MRR_USER.RM_CSUD_OPERATION 
(
   ID              NUMBER not null,
   NAME            VARCHAR2(1000 CHAR),
   constraint PK_CSUD_OPERATION primary key (ID)
);

comment on column NDS2_MRR_USER.RM_CSUD_OPERATION.ID is
'Идентификатор операции';

comment on column NDS2_MRR_USER.RM_CSUD_OPERATION.NAME is
'Название операции';
