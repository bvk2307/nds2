﻿--create table
create table NDS2_MRR_USER.CFG_TABLE_STAT
(
  TABLE_NAME      varchar(50) not null,
  METHOD          varchar(50) not null
);
-- Add comments to the columns 
comment on column NDS2_MRR_USER.CFG_TABLE_STAT.TABLE_NAME
  is 'Имя таблицы';
comment on column NDS2_MRR_USER.CFG_TABLE_STAT.METHOD
  is 'Идентификатор стратегии сбора статистики';
-- Add Primary key
alter table NDS2_MRR_USER.CFG_TABLE_STAT
  add constraint PK_CFG_TABLE_STAT primary key (TABLE_NAME);
