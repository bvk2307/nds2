﻿BEGIN
  NDS2_MRR_USER.NDS2$SYS.DROP_IF_EXISTS('NDS2_MRR_USER', 'T$KNP_RESULT_REPORT_CURR');
END;
/

-- Create table
create global temporary table NDS2_MRR_USER.T$KNP_RESULT_REPORT_CURR
(
  region_code       VARCHAR2(2) not null,
  sono_code         VARCHAR2(4) not null,
  fiscal_year       VARCHAR2(4) not null,
  quarter           NUMBER not null,
  act_qty           NUMBER not null,
  act_nds_amt		NUMBER not null,
  act_nds_qty       NUMBER not null,
  act_gap_amt		NUMBER not null,
  act_gap_qty       NUMBER not null,
  decision_qty      NUMBER not null,
  decision_nds_amt	NUMBER not null,
  decision_nds_qty  NUMBER not null,
  decision_gap_amt	NUMBER not null,
  decision_gap_qty  NUMBER not null
) on commit delete rows;
-- Add comments to the table 
comment on table NDS2_MRR_USER.T$KNP_RESULT_REPORT_CURR
  is 'Временная таблица для ежедневных отчетов по актам и решениям (обобщенная статистика за текущий день)';