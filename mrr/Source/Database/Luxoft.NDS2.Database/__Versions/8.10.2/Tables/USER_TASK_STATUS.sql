﻿-- Create table
create table NDS2_MRR_USER.USER_TASK_STATUS
(
  id          NUMBER(2) not null,
  description VARCHAR2(128 CHAR)
);

truncate table NDS2_MRR_USER.USER_TASK_STATUS;

insert into NDS2_MRR_USER.USER_TASK_STATUS values(1, 'Создано');
insert into NDS2_MRR_USER.USER_TASK_STATUS values(2, 'Просрочено');
insert into NDS2_MRR_USER.USER_TASK_STATUS values(3, 'Выполнено');
insert into NDS2_MRR_USER.USER_TASK_STATUS values(4, 'Закрыто');
commit;
