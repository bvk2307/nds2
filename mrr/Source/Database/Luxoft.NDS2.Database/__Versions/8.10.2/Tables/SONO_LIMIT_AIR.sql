﻿-- Create table
create table NDS2_MRR_USER.SONO_LIMIT_AIR 
(
  sono_code	varchar2(4 BYTE) not null
);
-- Add comments to the table 
comment on table NDS2_MRR_USER.SONO_LIMIT_AIR
  is 'Ограничение на доступ к актам и решениям только для указанных здесь инспекций на период опытной эксплуатации (если пустая, то ограничений нет)';
-- Add comments to the columns 
comment on column NDS2_MRR_USER.SONO_LIMIT_AIR.sono_code
  is 'Код инспеции';
