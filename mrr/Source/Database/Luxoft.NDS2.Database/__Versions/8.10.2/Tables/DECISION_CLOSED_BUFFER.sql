﻿-- Create table
create table NDS2_MRR_USER.DECISION_CLOSED_BUFFER
(
  decision_id  NUMBER not null,
  is_processed NUMBER
);
-- Add comments to the table 
comment on table NDS2_MRR_USER.DECISION_CLOSED_BUFFER
  is 'Буфер идентификаторов решений, которые были закрыты, для закрытия расхождений, которые в них вошли в рамках DAILY_JOB';
-- Add comments to the columns 
comment on column NDS2_MRR_USER.DECISION_CLOSED_BUFFER.decision_id
  is 'Идентификатор решения';
comment on column NDS2_MRR_USER.DECISION_CLOSED_BUFFER.is_processed
  is 'Признак обработки DAILY_JOB';
