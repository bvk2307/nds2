﻿/*==============================================================*/
/* Table: USER_TASK_DOC_REPLY                                   */
/*==============================================================*/
create table NDS2_MRR_USER.USER_TASK_DOC_REPLY 
(
   TASK_ID              NUMBER,
   REPLY_ID             NUMBER
);

comment on column NDS2_MRR_USER.USER_TASK_DOC_REPLY.TASK_ID is
'Идентификатор задания';

comment on column NDS2_MRR_USER.USER_TASK_DOC_REPLY.REPLY_ID is
'Идентификатор ответа на истребование/требование';
