﻿begin
  execute immediate 'drop table NDS2_MRR_USER.RM_MRR_OPERATION';
  exception when others then null;
end;
/

create table NDS2_MRR_USER.RM_MRR_OPERATION 
(
   ID                  NUMBER not null,
   CSUD_OPERATION_ID   NUMBER,
   NAME                VARCHAR2(1000 CHAR),
   DESCRIPTION         VARCHAR2(1000 CHAR),
   constraint PK_MRR_OPERATION primary key (ID)
);

comment on column NDS2_MRR_USER.RM_MRR_OPERATION.ID is
'Идентификатор операции МРР';

comment on column NDS2_MRR_USER.RM_MRR_OPERATION.CSUD_OPERATION_ID is
'Идентификатор операции CSUD';

comment on column NDS2_MRR_USER.RM_MRR_OPERATION.NAME is
'Название операции';

comment on column NDS2_MRR_USER.RM_MRR_OPERATION.DESCRIPTION is
'Описание операции';
