﻿-- Create table
create table NDS2_MRR_USER.ACT
(
  id                   number not null,
  inn_declarant        varchar2(12) not null,
  inn                  varchar2(12) not null,  
  kpp			       varchar2(9),
  kpp_effective        varchar2(9) not null,
  period_code          varchar2(2) not null,
  fiscal_year          varchar2(4) not null,
  close_date           date null
)
;
-- Add comments to the columns 
comment on column NDS2_MRR_USER.ACT.id
  is 'Идентификатор Акта';
  comment on column NDS2_MRR_USER.ACT.inn_declarant
  is 'ИНН Декларанта';
comment on column NDS2_MRR_USER.ACT.inn
  is 'ИНН Реорганизованного лица/Декларанта';
comment on column NDS2_MRR_USER.ACT.kpp
  is 'КПП Декларанта';
comment on column NDS2_MRR_USER.ACT.kpp_effective
  is 'КПП эффективный декларанта';
comment on column NDS2_MRR_USER.ACT.period_code
  is 'Код отчетного периода';
comment on column NDS2_MRR_USER.ACT.fiscal_year
  is 'Год отчетного периода';
comment on column NDS2_MRR_USER.ACT.close_date
  is 'Дата отбора расхождений в акт';
-- Create/Recreate indexes 
create unique index NDS2_MRR_USER.IX_ACT_DECL on NDS2_MRR_USER.ACT (inn_declarant, inn, kpp_effective, period_code, fiscal_year)
  tablespace NDS2_IDX
  compress;
create index NDS2_MRR_USER.IX_ACT_DATA on NDS2_MRR_USER.ACT (trunc(close_date))
  tablespace NDS2_IDX
  compress;
-- Create/Recreate primary, unique and foreign key constraints 
alter table NDS2_MRR_USER.ACT
  add constraint PK_ACT primary key (ID);
  
