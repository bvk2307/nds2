﻿begin
  execute immediate 'drop table NDS2_MRR_USER.USER_TASK_REPORT_USER';
  exception when others then null;
end;
/

create table NDS2_MRR_USER.USER_TASK_REPORT_USER
(
  REPORT_ID              NUMBER,
  SONO_CODE              VARCHAR2(4 CHAR),
  USER_SID               VARCHAR2(1024 CHAR),
  TASK_TYPE_ID           NUMBER,
  COMPLETED_ONSCHEDULE   NUMBER,
  COMPLETED_OVERDUE      NUMBER,
  INPROGRESS_ONSCHEDULE  NUMBER,
  INPROGRESS_OVERDUE     NUMBER
);
