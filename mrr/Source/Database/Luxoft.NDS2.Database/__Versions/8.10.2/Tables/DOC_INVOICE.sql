﻿create index NDS2_MRR_USER.FIDX_DOC_INVOICE_NVLRK on NDS2_MRR_USER.DOC_INVOICE(DOC_ID, nvl(ACTUAL_ROW_KEY, INVOICE_ROW_KEY));

begin
  dbms_stats.gather_table_stats(ownname => 'NDS2_MRR_USER', tabname => 'DOC_INVOICE', cascade => true);
  dbms_stats.gather_table_stats(ownname => 'NDS2_MRR_USER', tabname => 'DOC_DISCREPANCY', cascade => true);
  dbms_stats.gather_table_stats(ownname => 'NDS2_MRR_USER', tabname => 'SOV_DISCREPANCY', cascade => true);
end;
/

create table NDS2_MRR_USER.DOC_INVOICE_TMP as
select d.*
  ,CALC_PVP_CURRENCY
  ,CALC_PVP_NDS
  ,CALC_PVP_NOT_EXACT
  ,CALC_PVP_GAP
  ,PVP_CURRENCY_STATUS
  ,PVP_NDS_STATUS
  ,PVP_NOT_EXACT_STATUS
  ,PVP_GAP_STATUS
from NDS2_MRR_USER.DOC_INVOICE d
left join
 (select dd.doc_id
      ,dd.row_key
      ,cast (sum(case when sd.TYPE = 3 then sd.AMNT else 0 end) as number(20,2)) as CALC_PVP_CURRENCY
      ,cast (sum(case when sd.TYPE = 4 then sd.AMNT else 0 end) as number(20,2)) as CALC_PVP_NDS
      ,cast (sum(case when sd.TYPE = 2 then sd.AMNT else 0 end) as number(20,2)) as CALC_PVP_NOT_EXACT
      ,cast (sum(case when sd.TYPE = 1 then sd.AMNT else 0 end) as number(20,2)) as CALC_PVP_GAP
      ,cast (max(case when sd.TYPE = 3 then NVL(sd.STATUS, 1) end) as number) as PVP_CURRENCY_STATUS
      ,cast (max(case when sd.TYPE = 4 then NVL(sd.STATUS, 1) end) as number) as PVP_NDS_STATUS
      ,cast (max(case when sd.TYPE = 2 then NVL(sd.STATUS, 1) end) as number) as PVP_NOT_EXACT_STATUS
      ,cast (max(case when sd.TYPE = 1 then NVL(sd.STATUS, 1) end) as number) as PVP_GAP_STATUS
  from NDS2_MRR_USER.DOC_DISCREPANCY dd
  join NDS2_MRR_USER.SOV_DISCREPANCY sd on dd.discrepancy_id = sd.id
  group by dd.doc_id, dd.row_key) t on t.doc_id = d.doc_id and t.row_key = nvl(d.actual_row_key, d.invoice_row_key);
  
drop index NDS2_MRR_USER.FIDX_DOC_INVOICE_NVLRK;

drop table NDS2_MRR_USER.DOC_INVOICE;

alter table NDS2_MRR_USER.DOC_INVOICE_TMP rename to DOC_INVOICE;

create index NDS2_MRR_USER.IDX_DOC_INVOICE_DOC_ID on NDS2_MRR_USER.DOC_INVOICE (DOC_ID, INVOICE_CHAPTER) nologging tablespace NDS2_IDX;
create index NDS2_MRR_USER.IDX_DOC_INVOICE_ROW_KEY on NDS2_MRR_USER.DOC_INVOICE (INVOICE_ROW_KEY) nologging tablespace NDS2_IDX;

