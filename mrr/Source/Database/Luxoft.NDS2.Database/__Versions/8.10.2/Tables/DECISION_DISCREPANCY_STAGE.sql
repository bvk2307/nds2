﻿-- Create table
create table NDS2_MRR_USER.DECISION_DISCREPANCY_STAGE
(
  decision_id          number not null,
  included             number not null,
  id                   number not null,
  type_name            varchar2(30 char),
  initial_amt          number(21,2) not null,
  status               number,
  invoice_number       varchar2(1024 char),
  invoice_date         date,
  invoice_chapter      number,
  invoice_amt          number(21,2),
  invoice_nds_amt      number(21,2),
  contractor_inn       varchar2(12),
  contractor_kpp       varchar2(9),
  contractor_inn_reorg varchar2(12),
  act_amount           number(21,2) not null
)
;
-- Add comments to the columns 
comment on column NDS2_MRR_USER.DECISION_DISCREPANCY_STAGE.decision_id
  is 'Идентификатор Решения';
comment on column NDS2_MRR_USER.DECISION_DISCREPANCY_STAGE.included
  is '1 - Расхождение включено в документ, 0 - Не включено';
comment on column NDS2_MRR_USER.DECISION_DISCREPANCY_STAGE.id
  is 'Идентификатор расхождения';
comment on column NDS2_MRR_USER.DECISION_DISCREPANCY_STAGE.type_name
  is 'Вид расхождения';
comment on column NDS2_MRR_USER.DECISION_DISCREPANCY_STAGE.initial_amt
  is 'Сумма расхождения (по результатам сопоставления)';
comment on column NDS2_MRR_USER.DECISION_DISCREPANCY_STAGE.status
  is 'Статус расхождения';
comment on column NDS2_MRR_USER.DECISION_DISCREPANCY_STAGE.invoice_number
  is 'Номер счета-фактуры';
comment on column NDS2_MRR_USER.DECISION_DISCREPANCY_STAGE.invoice_date
  is 'Дата счета-фактуры';
comment on column NDS2_MRR_USER.DECISION_DISCREPANCY_STAGE.invoice_chapter
  is 'Номер раздела (от 8 до 12)';
comment on column NDS2_MRR_USER.DECISION_DISCREPANCY_STAGE.invoice_amt
  is 'Сумма по счету-фактуре';
comment on column NDS2_MRR_USER.DECISION_DISCREPANCY_STAGE.invoice_nds_amt
  is 'Сумма НДС по счету-фактуре';
comment on column NDS2_MRR_USER.DECISION_DISCREPANCY_STAGE.contractor_inn
  is 'ИНН котрагента';
comment on column NDS2_MRR_USER.DECISION_DISCREPANCY_STAGE.contractor_kpp
  is 'КПП контрагента';
comment on column NDS2_MRR_USER.DECISION_DISCREPANCY_STAGE.contractor_inn_reorg
  is 'ИНН Реорганизованного лица контрагента';
comment on column NDS2_MRR_USER.DECISION_DISCREPANCY_STAGE.act_amount
  is 'Сумма расхождения по акту';
-- Create/Recreate indexes 
create unique index NDS2_MRR_USER.IX_DECDIS_STAGE_ID on NDS2_MRR_USER.DECISION_DISCREPANCY_STAGE (id) tablespace NDS2_IDX;
create index NDS2_MRR_USER.IX_DECDIS_STAGE_DOC on NDS2_MRR_USER.DECISION_DISCREPANCY_STAGE (decision_id) tablespace NDS2_IDX;
create index NDS2_MRR_USER.IX_DECDIS_STAGE_CTR_INN on NDS2_MRR_USER.DECISION_DISCREPANCY_STAGE (decision_id,contractor_inn) tablespace NDS2_IDX;
create index NDS2_MRR_USER.IX_DECDIS_STAGE_INV_NUM on NDS2_MRR_USER.DECISION_DISCREPANCY_STAGE (decision_id,invoice_number) tablespace NDS2_IDX;
create index NDS2_MRR_USER.IX_DECDIS_STAGE_INV_DATE on NDS2_MRR_USER.DECISION_DISCREPANCY_STAGE (decision_id,invoice_date) tablespace NDS2_IDX;
create index NDS2_MRR_USER.IX_DECDIS_STAGE_INIT_AMT on NDS2_MRR_USER.DECISION_DISCREPANCY_STAGE (decision_id,initial_amt) tablespace NDS2_IDX;
create index NDS2_MRR_USER.IX_DECDIS_STAGE_INV_NDS on NDS2_MRR_USER.DECISION_DISCREPANCY_STAGE (decision_id,invoice_nds_amt) tablespace NDS2_IDX;
create index NDS2_MRR_USER.IX_DECDIS_STAGE_ACT_AMT on NDS2_MRR_USER.DECISION_DISCREPANCY_STAGE (decision_id,act_amount) tablespace NDS2_IDX;
