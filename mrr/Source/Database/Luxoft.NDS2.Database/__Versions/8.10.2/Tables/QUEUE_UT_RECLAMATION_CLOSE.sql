﻿create table NDS2_MRR_USER.QUEUE_UT_RECLAMATION_CLOSE
(
  job_id     NUMBER not null,
  doc_id     NUMBER not null,
  by_status_id     NUMBER not null
);

create index NDS2_MRR_USER.idx_q_ut_recl_cls_by_eod_jid on NDS2_MRR_USER.QUEUE_UT_RECLAMATION_CLOSE(JOB_ID) tablespace NDS2_IDX;
