﻿alter table NDS2_MRR_USER.DOC_TYPE add FLOW_NUMBER NUMBER(3);

comment on column NDS2_MRR_USER.DOC_TYPE.ID  is 'ИД типа документа МРР';
comment on column NDS2_MRR_USER.DOC_TYPE.DESCRIPTION  is 'Описание';
comment on column NDS2_MRR_USER.DOC_TYPE.FLOW_NUMBER  is 'Номер потока АСК';