﻿CREATE TABLE NDS2_MRR_USER.USER_TASK_TYPE
(
 ID number(2) primary key,
 DESCRIPTION varchar2(128 char)
);

insert into NDS2_MRR_USER.USER_TASK_TYPE values(1, 'Истребование документов');
insert into NDS2_MRR_USER.USER_TASK_TYPE values(2, 'Ввод ответа на истребование');
insert into NDS2_MRR_USER.USER_TASK_TYPE values(3, 'Анализ налогоплательщика');

commit;
