﻿begin
  execute immediate 'drop table NDS2_MRR_USER.BANK_ACCOUNT_LOG';
  exception when others then null;
end;
/
  
create table NDS2_MRR_USER.BANK_ACCOUNT_LOG
(
  id                        NUMBER,
  log_time                  DATE,
  msg_type                  NUMBER,
  code_link                 VARCHAR2(1024 char),
  message                   VARCHAR2(2048 char),
  sql_code                  VARCHAR2(10 char),
  sql_error                 VARCHAR2(512 CHAR)
) nologging;
