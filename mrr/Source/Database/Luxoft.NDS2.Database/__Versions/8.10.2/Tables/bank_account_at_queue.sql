﻿begin
  execute immediate 'drop table NDS2_MRR_USER.BANK_ACCOUNT_AT_QUEUE';
  exception when others then null;
end;
/
  
create table NDS2_MRR_USER.BANK_ACCOUNT_AT_QUEUE
(
  doc_id                        NUMBER,
  inn                           VARCHAR2(12 char),
  kpp_effective                 VARCHAR2(9 char),
  type_code                     VARCHAR2(1 char),
  fiscal_year                   VARCHAR2(4 CHAR),
  period_code                   VARCHAR2(2 CHAR),
  sono_code                     VARCHAR2(4 CHAR),
  sequence_id                   NUMBER,
  is_processed                  NUMBER(1)
) nologging;
