﻿-- Create table
create table NDS2_MRR_USER.knp_result_report_log  
(
  id			number not null,
  log_date		date not null,
  message_type	number not null,
  message_text	varchar2(2048 char) not null
);
-- Add comments to the table 
comment on table NDS2_MRR_USER.knp_result_report_log
  is 'Лог формирования ежедневных отчетов по актам и решениям';
-- Add comments to the columns 
comment on column NDS2_MRR_USER.knp_result_report_log.id
  is 'Цикличный счетсчик записей';
comment on column NDS2_MRR_USER.knp_result_report_log.log_date
  is 'Дата и время записи';
comment on column NDS2_MRR_USER.knp_result_report_log.message_type
  is 'Тип сообщения';
comment on column NDS2_MRR_USER.knp_result_report_log.message_text
  is 'Текст сообщения';
