﻿
/*==============================================================*/
/* Table: USER_TASK_DECLARATION_REL                        */
/*==============================================================*/
create table NDS2_MRR_USER.USER_TASK_DECLARATION_REL 
(
   TASK_ID              number,
   DECLARATION_CORRECTION_ID number               not null
);

comment on column NDS2_MRR_USER.USER_TASK_DECLARATION_REL.TASK_ID is
'Идентификатор задания';

comment on column NDS2_MRR_USER.USER_TASK_DECLARATION_REL.DECLARATION_CORRECTION_ID is
'Идентификатор корректировки декларации';

