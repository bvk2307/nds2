﻿/*==============================================================*/
/* Table: USER_TASK                                             */
/*==============================================================*/
begin NDS2_MRR_USER.Nds2$Sys.Drop_object_if_exist('USER_TASK'); end;
/

create table NDS2_MRR_USER.USER_TASK 
(
   TASK_ID              NUMBER               not null,
   TYPE_ID              NUMBER(2)            not null,
   INN					VARCHAR2(12 char)	not null,
   INN_CONTRACTOR		VARCHAR2(12 char)	not null,
   KPP_EFFECTIVE		VARCHAR2(9 char)	not null,
   QUARTER              NUMBER(1) not null,
   PERIOD				varchar2(2 char) not null,
   FISCAL_YEAR          NUMBER(4) not null,
   ASSIGNEE_SID         VARCHAR2(128 CHAR),
   ASSIGNEE_NAME		varchar2(256 char),
   ASSIGN_DATE          date,
   CREATE_DATE          date            not null,
   END_DATE_PLANNING    date            not null,
   CLOSE_DATE           date,
   STATUS               NUMBER(2)            not null,
   constraint PK_USER_TASK primary key (TASK_ID)
);

comment on column NDS2_MRR_USER.USER_TASK.TASK_ID is
'Идентификатор задания';

comment on column NDS2_MRR_USER.USER_TASK.TYPE_ID is
'Тип задания';

comment on column NDS2_MRR_USER.USER_TASK.ASSIGNEE_SID is
'СИД закрепленного пользователя';

comment on column NDS2_MRR_USER.USER_TASK.ASSIGN_DATE is
'Дата закрепления пользователя';

comment on column NDS2_MRR_USER.USER_TASK.END_DATE_PLANNING is
'Планируемая дата завершения задания';

comment on column NDS2_MRR_USER.USER_TASK.CLOSE_DATE is
'Фактическая дата завершения';

comment on column NDS2_MRR_USER.USER_TASK.STATUS is
'Статус задания';
