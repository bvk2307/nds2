﻿begin NDS2_MRR_USER.Nds2$Sys.Drop_object_if_exist('SEOD_INCOMING'); end;
/

-- Create table
create table NDS2_MRR_USER.SEOD_INCOMING
(
  session_id             NUMBER(38) not null,
  eod_id                 NUMBER(38) not null,
  info_type              VARCHAR2(11),
  info_type_name         VARCHAR2(100 CHAR),
  date_eod               DATE,
  date_receipt           DATE,
  code_nsi_sono          VARCHAR2(4 CHAR),
  reg_number             NUMBER(38) not null,
  xml_data               CLOB,
  processing_result      NUMBER,
  processing_result_text VARCHAR2(1024 char)
)
tablespace NDS2_DATA
  pctfree 10
  initrans 1
  maxtrans 255;
-- Add comments to the table 
comment on table NDS2_MRR_USER.SEOD_INCOMING
  is 'Таблица для хранения данных поступающих из СЭОД.';
-- Add comments to the columns 
comment on column NDS2_MRR_USER.SEOD_INCOMING.session_id
  is 'Идентификатор сессии загрузки.';
comment on column NDS2_MRR_USER.SEOD_INCOMING.eod_id
  is 'Идентификатор записи ЭОД.';
comment on column NDS2_MRR_USER.SEOD_INCOMING.info_type
  is 'Тип входящего потока. Определяет формат файла обмена данными.';
comment on column NDS2_MRR_USER.SEOD_INCOMING.code_nsi_sono
  is 'Код ТНО из справочника налоговых органов';
comment on column NDS2_MRR_USER.SEOD_INCOMING.xml_data
  is 'Содержимое Xml файла обмена данными.';
comment on column NDS2_MRR_USER.SEOD_INCOMING.processing_result
  is '1 - запись успешно обработана. 0 - при обработке записи произошла ошибка. NULL запись еще не обработана';
comment on column NDS2_MRR_USER.SEOD_INCOMING.processing_result_text
  is 'Идентификатор поступившей записи. Значения определяются последовательностью "NDS2_MRR_USER"."SEOD_INCOME_SEQ"';

create index NDS2_MRR_USER.idx_seod_income_sess_id on NDS2_MRR_USER.SEOD_INCOMING(SESSION_ID, processing_result) tablespace NDS2_IDX;
create index NDS2_MRR_USER.idx_seod_income_eod_id on NDS2_MRR_USER.SEOD_INCOMING(eod_id) tablespace NDS2_IDX;

