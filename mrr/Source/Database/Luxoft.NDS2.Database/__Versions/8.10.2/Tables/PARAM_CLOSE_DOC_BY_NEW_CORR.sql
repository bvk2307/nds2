﻿begin NDS2_MRR_USER.Nds2$Sys.Drop_object_if_exist('PARAM_CLOSE_DOC_BY_NEW_CORR'); end;
/

create table NDS2_MRR_USER.PARAM_CLOSE_DOC_BY_NEW_CORR
(
  job_id NUMBER,
  inn    VARCHAR2(12 CHAR),
  period VARCHAR2(2 CHAR),
  year   VARCHAR2(4 CHAR),
  status NUMBER
);

create index NDS2_MRR_USER.IDX_JB_ID on NDS2_MRR_USER.PARAM_CLOSE_DOC_BY_NEW_CORR (JOB_ID) tablespace NDS2_IDX;
