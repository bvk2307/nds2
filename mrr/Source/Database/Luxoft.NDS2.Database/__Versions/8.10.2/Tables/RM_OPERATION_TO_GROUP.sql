﻿begin
  execute immediate 'drop table NDS2_MRR_USER.RM_OPERATION_TO_GROUP';
  exception when others then null;
end;
/

create table NDS2_MRR_USER.RM_OPERATION_TO_GROUP 
(
   MRR_OPERATION_ID    NUMBER,
   SONO_GROUP_ID       NUMBER
);
