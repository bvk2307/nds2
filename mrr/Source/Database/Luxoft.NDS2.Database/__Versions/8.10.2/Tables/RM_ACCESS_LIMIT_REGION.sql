﻿begin
  execute immediate 'drop table NDS2_MRR_USER.RM_ACCESS_LIMIT_REGION';
  exception when others then null;
end;
/

create table NDS2_MRR_USER.RM_ACCESS_LIMIT_REGION 
(
   GROUP_ID              NUMBER,
   SONO_CODE             VARCHAR2(4 CHAR),
   REGION_CODE           VARCHAR2(2 CHAR)
);

