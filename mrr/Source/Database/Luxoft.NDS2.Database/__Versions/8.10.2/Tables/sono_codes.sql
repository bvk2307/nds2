﻿create table nds2_mrr_user.sono_codes (
  sono_code varchar2(4) not null,
  type_id number not null);
  
create index nds2_mrr_user.ix_sono_code on nds2_mrr_user.sono_codes (sono_code) tablespace nds2_idx;
create index nds2_mrr_user.ix_sono_code_type on nds2_mrr_user.sono_codes (type_id) tablespace nds2_idx;
