﻿-- Create table
create table NDS2_MRR_USER.ACT_DISCREPANCY
(
  act_id               number not null,
  id                   number not null,
  type_name            varchar2(30 char),
  initial_amt          number(21,2) not null,
  status               number,
  invoice_number       varchar2(1024 char),
  invoice_date         date,
  invoice_chapter      number,
  invoice_amt          number(21,2),
  invoice_nds_amt      number(21,2),
  contractor_inn       varchar2(12),
  contractor_kpp       varchar2(9),
  contractor_inn_reorg varchar2(12),
  amount               number(21,2) not null);
-- Add comments to the columns 
comment on column NDS2_MRR_USER.ACT_DISCREPANCY.act_id
  is 'Идентификатор Акта';
comment on column NDS2_MRR_USER.ACT_DISCREPANCY.id
  is 'Идентификатор расхождения';
comment on column NDS2_MRR_USER.ACT_DISCREPANCY.type_name
  is 'Вид расхождения';
comment on column NDS2_MRR_USER.ACT_DISCREPANCY.initial_amt
  is 'Сумма расхождения (по результатам сопоставления)';
comment on column NDS2_MRR_USER.ACT_DISCREPANCY.status
  is 'Статус расхождения';
comment on column NDS2_MRR_USER.ACT_DISCREPANCY.invoice_number
  is 'Номер счета-фактуры';
comment on column NDS2_MRR_USER.ACT_DISCREPANCY.invoice_date
  is 'Дата счета-фактуры';
comment on column NDS2_MRR_USER.ACT_DISCREPANCY.invoice_chapter
  is 'Номер раздела (от 8 до 12)';
comment on column NDS2_MRR_USER.ACT_DISCREPANCY.invoice_amt
  is 'Сумма по счету-фактуре';
comment on column NDS2_MRR_USER.ACT_DISCREPANCY.invoice_nds_amt
  is 'Сумма НДС по счету-фактуре';
comment on column NDS2_MRR_USER.ACT_DISCREPANCY.contractor_inn
  is 'ИНН котрагента';
comment on column NDS2_MRR_USER.ACT_DISCREPANCY.contractor_kpp
  is 'КПП контрагента';
comment on column NDS2_MRR_USER.ACT_DISCREPANCY.contractor_inn_reorg
  is 'ИНН Реорганизованного лица контрагента';
comment on column NDS2_MRR_USER.ACT_DISCREPANCY.amount
  is 'Сумма расхождения по акту';
-- Create/Recreate indexes 
create unique index NDS2_MRR_USER.IX_ACT_DIS_ID on NDS2_MRR_USER.ACT_DISCREPANCY (id) tablespace NDS2_IDX;
create index NDS2_MRR_USER.IX_ACT_DIS_ACT on NDS2_MRR_USER.ACT_DISCREPANCY (act_id) tablespace NDS2_IDX;
create index NDS2_MRR_USER.IX_ACT_DIS_STATUS on NDS2_MRR_USER.ACT_DISCREPANCY (act_id, status) tablespace NDS2_IDX;
create index NDS2_MRR_USER.IX_ACT_DIS_CTR_INN on NDS2_MRR_USER.ACT_DISCREPANCY (act_id, contractor_inn) tablespace NDS2_IDX;
create index NDS2_MRR_USER.IX_ACT_DIS_INV_NUM on NDS2_MRR_USER.ACT_DISCREPANCY (act_id, invoice_number) tablespace NDS2_IDX;
create index NDS2_MRR_USER.IX_ACT_DIS_INV_DATE on NDS2_MRR_USER.ACT_DISCREPANCY (act_id, invoice_date) tablespace NDS2_IDX;
create index NDS2_MRR_USER.IX_ACT_DIS_AMT on NDS2_MRR_USER.ACT_DISCREPANCY (act_id, initial_amt) tablespace NDS2_IDX;
create index NDS2_MRR_USER.IX_ACT_DIS_INV_NDS on NDS2_MRR_USER.ACT_DISCREPANCY (act_id, invoice_nds_amt) tablespace NDS2_IDX;
create index NDS2_MRR_USER.IX_ACT_DIS_ACT_AMT on NDS2_MRR_USER.ACT_DISCREPANCY (act_id, status, amount) tablespace NDS2_IDX;
create index NDS2_MRR_USER.IX_ACT_DIS_ID_STATUS on NDS2_MRR_USER.ACT_DISCREPANCY (id, status) tablespace NDS2_IDX;
