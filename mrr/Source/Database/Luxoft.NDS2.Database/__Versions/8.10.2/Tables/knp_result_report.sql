﻿-- Create table
create table NDS2_MRR_USER.knp_result_report 
(
  id				number not null,
  report_date		date not null,
  fiscal_year		varchar2(4 byte) not null,
  quarter			number not null,
  actual_report_id	number not null
);
-- Add comments to the table 
comment on table NDS2_MRR_USER.knp_result_report
  is 'Ежедневные отчеты по актам и решениям';
-- Add comments to the columns 
comment on column NDS2_MRR_USER.knp_result_report.id
  is 'Идентификатор отчета';
comment on column NDS2_MRR_USER.knp_result_report.report_date
  is 'Дата на которую сформирован отчет';
comment on column NDS2_MRR_USER.knp_result_report.fiscal_year
  is 'Отчетный год';
comment on column NDS2_MRR_USER.knp_result_report.quarter
  is 'Отчетный квартал';
comment on column NDS2_MRR_USER.knp_result_report.actual_report_id
  is 'Действительный id отчета (предыдущий)';
-- Create/Recreate primary, unique and foreign key constraints 
alter table NDS2_MRR_USER.knp_result_report
  add constraint pk_knp_result_report primary key (id);
-- Create/Recreate indexes 
create index NDS2_MRR_USER.ix_knp_result_report_dt on NDS2_MRR_USER.knp_result_report(report_date) 
tablespace NDS2_IDX;
create index NDS2_MRR_USER.ix_knp_result_report_key on NDS2_MRR_USER.knp_result_report(
	fiscal_year, 
	quarter, 
	report_date) 
tablespace NDS2_IDX;
