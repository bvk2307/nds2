﻿/*==============================================================*/
/* Table: USER_TASK_DOC_REL                                     */
/*==============================================================*/
create table NDS2_MRR_USER.USER_TASK_DOC_REL 
(
   TASK_ID              NUMBER               not null,
   DOC_ID               NUMBER               not null
);

comment on column NDS2_MRR_USER.USER_TASK_DOC_REL.TASK_ID is
'Идентификатор задания';

comment on column NDS2_MRR_USER.USER_TASK_DOC_REL.DOC_ID is
'Идентификатор требования/истребования';
