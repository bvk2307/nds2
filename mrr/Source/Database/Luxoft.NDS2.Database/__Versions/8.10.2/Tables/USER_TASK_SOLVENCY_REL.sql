﻿create table NDS2_MRR_USER.USER_TASK_SOLVENCY_REL
(
   TASK_ID              NUMBER               not null,
   SOLVENCY_ID			NUMBER(2)            not null,
   DECISION_DATE        TIMESTAMP            not null
);

comment on column NDS2_MRR_USER.USER_TASK_SOLVENCY_REL.TASK_ID is
'Идентификатор задания';

comment on column NDS2_MRR_USER.USER_TASK_SOLVENCY_REL.SOLVENCY_ID is
'Ссылка на статус платежеспособности';

comment on column NDS2_MRR_USER.USER_TASK_SOLVENCY_REL.DECISION_DATE is
'Дата принятия решения(доначисления)';
