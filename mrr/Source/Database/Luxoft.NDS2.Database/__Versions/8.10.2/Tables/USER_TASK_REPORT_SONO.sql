﻿begin
  execute immediate 'drop table NDS2_MRR_USER.USER_TASK_REPORT_SONO';
  exception when others then null;
end;
/

create table NDS2_MRR_USER.USER_TASK_REPORT_SONO
(
REPORT_ID              NUMBER not null,
REGION_CODE            VARCHAR2(2 CHAR) not null,
SONO_CODE              VARCHAR2(4 CHAR),
TASK_TYPE_ID           NUMBER,
COMPLETED_ONSCHEDULE   NUMBER,
COMPLETED_OVERDUE      NUMBER,
INPROGRESS_ONSCHEDULE  NUMBER,
INPROGRESS_OVERDUE     NUMBER,
UNASSIGNED             NUMBER
);
