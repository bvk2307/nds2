﻿-- Create table
create table NDS2_MRR_USER.USER_TASK_SYSTEM_LOG
(
  site_id     VARCHAR2(128 CHAR) not null,
  msg_code    VARCHAR2(32 CHAR) not null,
  msg_text    VARCHAR2(2048 CHAR) not null,
  entity_id   NUMBER not null,
  create_date date not null
);
-- Add comments to the columns 
comment on column NDS2_MRR_USER.USER_TASK_SYSTEM_LOG.site_id
  is 'контекст ошибки';
comment on column NDS2_MRR_USER.USER_TASK_SYSTEM_LOG.msg_code
  is 'Код сообщения';
comment on column NDS2_MRR_USER.USER_TASK_SYSTEM_LOG.msg_text
  is 'Текст сообщения';
comment on column NDS2_MRR_USER.USER_TASK_SYSTEM_LOG.entity_id
  is 'Идентификатор связанной сущности';
comment on column NDS2_MRR_USER.USER_TASK_SYSTEM_LOG.create_date
  is 'Дата создания записи';
