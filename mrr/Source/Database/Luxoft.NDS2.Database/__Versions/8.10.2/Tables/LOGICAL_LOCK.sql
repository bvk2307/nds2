﻿create table NDS2_MRR_USER.LOGICAL_LOCK 
(
   LOCK_ID          number, 
   WHEN_IS_LOCKED   timestamp, 
   LOCKED_BY        varchar2(128 char), 
   WHEN_IS_RELEASED timestamp,
   IS_LOCKED        number(1)
);