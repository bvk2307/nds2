﻿-- Create table
create table NDS2_MRR_USER.QUEUE_UT_RECLAMATION
(
  job_id NUMBER not null,
  doc_id NUMBER not null
) tablespace NDS2_DATA;

-- Create/Recreate indexes 
create index NDS2_MRR_USER.IDX_QUEUE_UT_RECLAMATION_JID on NDS2_MRR_USER.QUEUE_UT_RECLAMATION (JOB_ID) tablespace NDS2_IDX;
