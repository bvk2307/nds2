﻿-- Create table
create table NDS2_MRR_USER.knp_result_report_period  
(
  fiscal_year		varchar2(4 byte) not null,
  quarter			number not null
);
-- Add comments to the table 
comment on table NDS2_MRR_USER.knp_result_report_period
  is 'Перечень налоговых периодов по которым сформированы агрегаты';
-- Add comments to the columns 
comment on column NDS2_MRR_USER.knp_result_report_period.fiscal_year
  is 'Отчетный год';
comment on column NDS2_MRR_USER.knp_result_report_period.quarter
  is 'Отчетный квартал';
 -- Create/Recreate primary, unique and foreign key constraints 
alter table NDS2_MRR_USER.knp_result_report_period
  add constraint pk_knp_result_report_period primary key (fiscal_year, quarter);