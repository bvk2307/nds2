﻿begin
  execute immediate 'drop table NDS2_MRR_USER.RM_ACCESS_LIMIT_TYPE';
  exception when others then null;
end;
/

create table NDS2_MRR_USER.RM_ACCESS_LIMIT_TYPE 
(
   ID              NUMBER not null,
   DESCRIPTION     VARCHAR2(1000 CHAR),
   constraint PK_ACCESS_LIMIT_TYPE primary key (ID)
);

comment on column NDS2_MRR_USER.RM_ACCESS_LIMIT_TYPE.ID is
'Идентификатор ограничения';

comment on column NDS2_MRR_USER.RM_ACCESS_LIMIT_TYPE.DESCRIPTION is
'Текст ограничения';
