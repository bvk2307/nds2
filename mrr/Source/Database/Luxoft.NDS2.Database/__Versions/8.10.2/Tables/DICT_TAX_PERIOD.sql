﻿drop table NDS2_MRR_USER.DICT_TAX_PERIOD;
create table NDS2_MRR_USER.DICT_TAX_PERIOD
(
  code               VARCHAR2(2 CHAR) not null,
  period             VARCHAR2(2 CHAR) not null,
  description        VARCHAR2(512 CHAR) not null,
  quarter            NUMBER(1) not null,
  is_main_in_quarter NUMBER(1) not null,
  sort_order         NUMBER default 0 not null,
  month			     NUMBER(2) default(0) not null
);

alter table NDS2_MRR_USER.DICT_TAX_PERIOD add constraint PK_DICT_TAX_PERIOD_CODE primary key (CODE) using index;