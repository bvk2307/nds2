﻿create table NDS2_MRR_USER.RECLAIM_LOG
(
  id           NUMBER not null,
  type         NUMBER(2) not null,
  title        VARCHAR2(256 char),
  message      VARCHAR2(2048 char),
  zip          NUMBER,
  sono_code    VARCHAR2(4 char),
  reg_number   NUMBER,
  queue_id     NUMBER,
  log_date     DATE
);