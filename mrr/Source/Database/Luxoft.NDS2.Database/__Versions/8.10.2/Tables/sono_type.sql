﻿-- Create table
create table nds2_mrr_user.sono_type
(
  id          number not null,
  description varchar2(50) not null
)
;
-- Create/Recreate primary, unique and foreign key constraints 
alter table nds2_mrr_user.sono_type
  add constraint pk_sono_type primary key (ID);
