﻿begin
  execute immediate 'drop table NDS2_MRR_USER.RM_GROUP_TO_SONO';
  exception when others then null;
end;
/

create table NDS2_MRR_USER.RM_GROUP_TO_SONO 
(
   SONO_CODE             VARCHAR2(4 CHAR) not null,
   GROUP_ID              NUMBER,
   ACCESS_LIMIT_TYPE_ID  NUMBER
);