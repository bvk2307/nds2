﻿create table NDS2_MRR_USER.TAXPAYER_SOLVENCY 
(
   ID number,
   INN					VARCHAR2(12 char)           not null,
   KPP_EFFECTIVE	    VARCHAR2(9 char),
   FISCAL_YEAR			number not null,
   PERIOD				varchar2(2 char)	 not null,
   SOLVENCY_STATUS_ID   number not null,
   DECISION_DATE        date            not null
);

create table NDS2_MRR_USER.TAXPAYER_SOLVENCY_COMMENT
(
   SOLVENCY_ID number,
   USER_COMMENT CLOB
);
