﻿-- Create table
create table NDS2_MRR_USER.knp_result_report_aggregate 
(
  report_id						number not null,
  region_code					varchar2(2 BYTE) not null, 
  sono_code						varchar2(4 BYTE),
  acts_qty						number not null,
  act_discrepancy_nds_amt		number(21,2) not null,
  act_discrepancy_nds_qty		number not null,
  act_discrepancy_gap_amt		number(21,2) not null,
  act_discrepancy_gap_qty		number not null,
  decisions_qty					number not null,
  decision_discrepancy_nds_amt	number(21,2) not null,
  decision_discrepancy_nds_qty	number not null,
  decision_discrepancy_gap_amt	number(21,2) not null,
  decision_discrepancy_gap_qty	number not null
);
-- Add comments to the table 
comment on table NDS2_MRR_USER.knp_result_report_aggregate
  is 'Агрегат отчета по актам и решениям';
-- Add comments to the columns 
comment on column NDS2_MRR_USER.knp_result_report_aggregate.report_id
  is 'Идентификатор отчета';
comment on column NDS2_MRR_USER.knp_result_report_aggregate.region_code
  is 'Код региона';
comment on column NDS2_MRR_USER.knp_result_report_aggregate.sono_code
  is 'Код инспекции';
comment on column NDS2_MRR_USER.knp_result_report_aggregate.acts_qty
  is 'Кол-во НП для которых расхождения для КНП были включены в акты';
comment on column NDS2_MRR_USER.knp_result_report_aggregate.act_discrepancy_nds_amt
  is '∑ сумм по актам, по расхождениям вида «НДС», включенных в акты';
comment on column NDS2_MRR_USER.knp_result_report_aggregate.act_discrepancy_nds_qty
  is 'Кол-во расхождений для КНП вида «НДС» включенных в акты';
comment on column NDS2_MRR_USER.knp_result_report_aggregate.act_discrepancy_gap_amt
  is '∑ сумм по актам, по расхождениям вида «Разрыв», включенных в акты';
comment on column NDS2_MRR_USER.knp_result_report_aggregate.act_discrepancy_gap_qty
  is 'Кол-во расхождений для КНП вида «Разрыв» включенных в акты';
comment on column NDS2_MRR_USER.knp_result_report_aggregate.decisions_qty
  is 'Кол-во НП для которых расхождения для КНП были включены в решения';
comment on column NDS2_MRR_USER.knp_result_report_aggregate.decision_discrepancy_nds_amt
  is '∑ сумм по решениям, по расхождениям вида «НДС», включенных в решения';
comment on column NDS2_MRR_USER.knp_result_report_aggregate.decision_discrepancy_nds_qty
  is 'Кол-во расхождений для КНП вида «НДС» включенных в решения';
comment on column NDS2_MRR_USER.knp_result_report_aggregate.decision_discrepancy_gap_amt
  is '∑ сумм по решениям, по расхождениям вида «Разрыв», включенных в решения';
comment on column NDS2_MRR_USER.knp_result_report_aggregate.decision_discrepancy_gap_qty
  is 'Кол-во расхождений для КНП вида «Разрыв» включенных в решения';
-- Create/Recreate indexes 
create index NDS2_MRR_USER.ix_knp_result_report_aggregate on NDS2_MRR_USER.knp_result_report_aggregate 
(
	report_id,
	region_code,
	sono_code
) tablespace NDS2_IDX;

