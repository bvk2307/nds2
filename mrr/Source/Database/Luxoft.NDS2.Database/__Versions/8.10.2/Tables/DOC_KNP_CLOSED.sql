﻿create table NDS2_MRR_USER.DOC_KNP_CLOSED
(
  doc_id not null
) 
as
select d.doc_id from NDS2_MRR_USER.doc d
join NDS2_MRR_USER.SEOD_KNP k on k.declaration_reg_num = d.ref_entity_id 
              and k.ifns_code = d.sono_code
where k.completion_date is not null
group by d.doc_id;