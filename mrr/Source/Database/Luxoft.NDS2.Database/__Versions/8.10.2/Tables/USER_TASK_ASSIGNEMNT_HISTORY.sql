﻿/*==============================================================*/
/* Table: USER_TASK_ASSIGNMENT_HISTORY                          */
/*==============================================================*/
create table NDS2_MRR_USER.USER_TASK_ASSIGNMENT_HISTORY 
(
   TASK_ID              NUMBER,
   USER_SID             VARCHAR2(128 CHAR)   not null,
   USER_NAME		varchar2(256 char)   not null,
   ASSIGN_DATE          date            not null
);

comment on column NDS2_MRR_USER.USER_TASK_ASSIGNMENT_HISTORY.TASK_ID is
'Идентификатор задания';

comment on column NDS2_MRR_USER.USER_TASK_ASSIGNMENT_HISTORY.USER_SID is
'СИД закрепленного за заданием пользователя';

comment on column NDS2_MRR_USER.USER_TASK_ASSIGNMENT_HISTORY.ASSIGN_DATE is
'Дата закрепления';
