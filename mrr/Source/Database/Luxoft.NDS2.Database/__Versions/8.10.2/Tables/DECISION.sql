﻿--create table
create table NDS2_MRR_USER.DECISION
(
  id              number not null,
  inn_declarant   varchar2(12) not null,
  inn             varchar2(12) not null,
  kpp			  varchar2(9),
  kpp_effective   varchar2(9) not null,
  period_code     varchar2(2) not null,
  fiscal_year     varchar2(4) not null,
  close_date   date
)
;
-- Add comments to the columns 
comment on column NDS2_MRR_USER.DECISION.id
  is 'Идентификатор решения КНП';
  comment on column NDS2_MRR_USER.DECISION.inn_declarant
  is 'ИНН Декларанта';
comment on column NDS2_MRR_USER.DECISION.inn
  is 'ИНН Декларанта / Реорганизованного лица';
comment on column NDS2_MRR_USER.DECISION.kpp
  is 'КПП Декларанта';
comment on column NDS2_MRR_USER.DECISION.kpp_effective
  is 'Эффективный КПП декларанта';
comment on column NDS2_MRR_USER.DECISION.period_code
  is 'Код отчетного периода';
comment on column NDS2_MRR_USER.DECISION.fiscal_year
  is 'Год отчетного периода';
comment on column NDS2_MRR_USER.DECISION.close_date
  is 'Дата Отбора расхождений в Решение';
-- Create/Recreate indexes 
create unique index NDS2_MRR_USER.IX_DECISION_DECL on NDS2_MRR_USER.DECISION (inn_declarant, inn, kpp_effective, period_code, fiscal_year)
  tablespace NDS2_IDX
  compress;
create index NDS2_MRR_USER.IX_DECISION_DATA on NDS2_MRR_USER.DECISION (trunc(close_date))
  tablespace NDS2_IDX
  compress;
-- Create/Recreate primary, unique and foreign key constraints 
alter table NDS2_MRR_USER.DECISION
  add constraint PK_DECISION primary key (ID);
