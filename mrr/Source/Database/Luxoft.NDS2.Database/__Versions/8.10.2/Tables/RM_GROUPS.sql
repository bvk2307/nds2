﻿begin
  execute immediate 'drop table NDS2_MRR_USER.RM_GROUPS';
  exception when others then null;
end;
/

create table NDS2_MRR_USER.RM_GROUPS 
(
   ID                  NUMBER not null,
   DESCRIPTION         VARCHAR2(1000 CHAR)
);
