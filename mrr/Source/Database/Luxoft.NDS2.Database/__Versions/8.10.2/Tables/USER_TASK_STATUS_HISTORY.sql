﻿create table NDS2_MRR_USER.USER_TASK_STATUS_HISTORY
(
  task_id       NUMBER,
  status_id     NUMBER not null,
  change_reason number not null,
  changed_by    VARCHAR2(128 CHAR) not null,
  changed_date  date not null
);
-- Add comments to the columns 
comment on column NDS2_MRR_USER.USER_TASK_STATUS_HISTORY.task_id
  is 'Идентификатор задания';
comment on column NDS2_MRR_USER.USER_TASK_STATUS_HISTORY.status_id
  is 'Идентификатор статуса';
comment on column NDS2_MRR_USER.USER_TASK_STATUS_HISTORY.change_reason
  is 'Причина изменения';
comment on column NDS2_MRR_USER.USER_TASK_STATUS_HISTORY.changed_by
  is 'Кем изменено';
