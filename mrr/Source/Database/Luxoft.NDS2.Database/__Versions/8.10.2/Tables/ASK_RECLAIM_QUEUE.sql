﻿create table NDS2_MRR_USER.ASK_RECLAIM_QUEUE
(
  id          NUMBER not null,
  doc_id      NUMBER not null,
  case_num    NUMBER(1) not null,
  side        NUMBER(1) not null,
  state       NUMBER(2) not null,
  is_locked   NUMBER(1) not null,
  create_date DATE not null,
  update_date DATE
);