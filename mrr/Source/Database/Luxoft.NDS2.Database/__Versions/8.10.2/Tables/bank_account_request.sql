﻿begin
  execute immediate 'drop table NDS2_MRR_USER.BANK_ACCOUNT_REQUEST';
  exception when others then null;
end;
/

create table NDS2_MRR_USER.BANK_ACCOUNT_REQUEST
(
  id            INTEGER,
  inn           VARCHAR2(12 CHAR),
  kpp_effective VARCHAR2(9 CHAR),
  type_code     VARCHAR2(1 CHAR),
  fiscal_year   VARCHAR2(4 CHAR),
  period_code   VARCHAR2(2 CHAR),
  sono_code     VARCHAR2(4 CHAR),
  status_code   NUMBER,
  period_begin  DATE,
  period_end    DATE,
  request_actor NUMBER,
  request_type  NUMBER,
  request_date  DATE,
  innbank       VARCHAR2(12 CHAR),
  kppbank       VARCHAR2(9 CHAR),
  namebank      VARCHAR2(1000 CHAR),
  bikbank       VARCHAR2(9 CHAR),
  filialnumber  VARCHAR2(4 CHAR),
  banknumber    VARCHAR2(4 CHAR),
  user_sid                      VARCHAR2(1024 char),
  user_name                     VARCHAR2(1024 char),
  accounts_list                 VARCHAR2(1024 char),
  accounts                      CLOB,
  doc_body                      CLOB,
  doc_id                        NUMBER,
  reg_number                    NUMBER,
  seod_date                     DATE
);
