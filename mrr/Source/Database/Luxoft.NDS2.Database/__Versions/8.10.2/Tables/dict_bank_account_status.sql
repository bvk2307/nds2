﻿begin
  execute immediate 'drop table NDS2_MRR_USER.DICT_BANK_ACCOUNT_STATUS';
  exception when others then null;
end;
/

create table NDS2_MRR_USER.DICT_BANK_ACCOUNT_STATUS
(
  status_code number,
  inner_name  varchar2(30 char),
  ui_name     varchar2(30 char)
) compress nologging;
