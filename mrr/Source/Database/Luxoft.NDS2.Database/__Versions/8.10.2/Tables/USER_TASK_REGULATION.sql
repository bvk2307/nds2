﻿-- Create table
create table NDS2_MRR_USER.USER_TASK_REGULATION
(
  task_type_id    NUMBER(2) not null,
  days_to_process NUMBER(3) not null,
  days_to_notify  NUMBER(1) not null
);
-- Add comments to the table 
comment on table NDS2_MRR_USER.USER_TASK_REGULATION
  is 'Регламент выполнения ПЗ';
-- Add comments to the columns 
comment on column NDS2_MRR_USER.USER_TASK_REGULATION.task_type_id
  is 'Тип ПЗ';
comment on column NDS2_MRR_USER.USER_TASK_REGULATION.days_to_process
  is 'Кол-во дней, отведенное на обработку';
comment on column NDS2_MRR_USER.USER_TASK_REGULATION.days_to_notify
  is 'Кол-во дней до начала уведомления';

insert into NDS2_MRR_USER.USER_TASK_REGULATION(TASK_TYPE_ID, DAYS_TO_PROCESS, DAYS_TO_NOTIFY) values (1, 3, 1);
insert into NDS2_MRR_USER.USER_TASK_REGULATION(TASK_TYPE_ID, DAYS_TO_PROCESS, DAYS_TO_NOTIFY) values (2, 3, 1);
insert into NDS2_MRR_USER.USER_TASK_REGULATION(TASK_TYPE_ID, DAYS_TO_PROCESS, DAYS_TO_NOTIFY) values (3, 28, 3);
commit;
