﻿-- Create table
create table NDS2_MRR_USER.ACT_DECISION_APPEND_SESSION
(
  user_id     varchar2(100) not null,
  user_name   varchar2(100),
  document_id number not null,
  expired_at  timestamp not null
)
;
-- Add comments to the columns 
comment on column NDS2_MRR_USER.ACT_DECISION_APPEND_SESSION.user_id
  is 'СИД пользователя, стартовавшего сессию';
comment on column NDS2_MRR_USER.ACT_DECISION_APPEND_SESSION.user_name
  is 'Имя пользователя';
comment on column NDS2_MRR_USER.ACT_DECISION_APPEND_SESSION.document_id
  is 'Идентификатор Акта\Решения';
comment on column NDS2_MRR_USER.ACT_DECISION_APPEND_SESSION.expired_at
  is 'Дата и время завершения сессии';
-- Create/Recreate indexes 
create index NDS2_MRR_USER.IX_KNPD_SES_USR on NDS2_MRR_USER.ACT_DECISION_APPEND_SESSION (USER_ID) tablespace NDS2_IDX;
create unique index NDS2_MRR_USER.IX_KNPD_SES_DOC on NDS2_MRR_USER.ACT_DECISION_APPEND_SESSION (DOCUMENT_ID)tablespace NDS2_IDX;
