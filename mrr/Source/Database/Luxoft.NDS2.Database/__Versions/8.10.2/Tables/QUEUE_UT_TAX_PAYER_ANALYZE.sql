﻿create table NDS2_MRR_USER.QUEUE_UT_TAX_PAYER_ANALYZE
(
  job_id     NUMBER not null,
  doc_id     NUMBER not null
);

create index NDS2_MRR_USER.idx_queue_ut_tp_jid on NDS2_MRR_USER.QUEUE_UT_TAX_PAYER_ANALYZE(JOB_ID) tablespace NDS2_IDX;
