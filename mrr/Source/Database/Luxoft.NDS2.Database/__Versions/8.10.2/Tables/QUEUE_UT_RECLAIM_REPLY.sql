﻿-- Create table
create table NDS2_MRR_USER.QUEUE_UT_RECLAIM_REPLY
(
  job_id NUMBER not null,
  doc_id NUMBER not null,
  explain_id NUMBER not null
);

-- Create/Recreate indexes 
create index NDS2_MRR_USER.IDX_QUEUE_UT_RECLAIM_REPLY_JID on NDS2_MRR_USER.QUEUE_UT_RECLAIM_REPLY (JOB_ID) tablespace NDS2_IDX;
