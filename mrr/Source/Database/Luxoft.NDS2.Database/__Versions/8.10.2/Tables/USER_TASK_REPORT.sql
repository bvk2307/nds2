﻿begin
  execute immediate 'drop table NDS2_MRR_USER.USER_TASK_REPORT';
  exception when others then null;
end;
/

create table NDS2_MRR_USER.USER_TASK_REPORT
(
  ID              NUMBER PRIMARY KEY,
  REPORT_DATE     DATE,
  AGGREGATE_DATE  TIMESTAMP
);
