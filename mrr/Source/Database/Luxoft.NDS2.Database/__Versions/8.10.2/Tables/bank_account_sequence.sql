﻿begin
  execute immediate 'drop table NDS2_MRR_USER.BANK_ACCOUNT_SEQUENCE';
  exception when others then null;
end;
/

create table NDS2_MRR_USER.BANK_ACCOUNT_SEQUENCE
(
  sequence_id number,
  change_date date
) nologging;
