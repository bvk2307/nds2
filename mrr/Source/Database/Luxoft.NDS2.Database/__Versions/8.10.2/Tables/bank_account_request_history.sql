﻿begin
  execute immediate 'drop table NDS2_MRR_USER.BANK_ACCOUNT_REQUEST_HISTORY';
  exception when others then null;
end;
/

create table NDS2_MRR_USER.BANK_ACCOUNT_REQUEST_HISTORY
(
  request_id NUMBER,
  status_date DATE,
  status_code NUMBER
);
