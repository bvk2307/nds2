﻿drop table NDS2_MRR_USER.KNP_DISCREPANCY;
create table NDS2_MRR_USER.KNP_DISCREPANCY pctfree 0 nologging as
select dis.id, dis.status   
from (
     select knp.discrepancy_id
     from NDS2_MRR_USER.DOC_DISCREPANCY knp 
     join NDS2_MRR_USER.DOC doc on doc.doc_id = knp.doc_id and doc.status not in (-1,1,3,11)
     group by knp.discrepancy_id) knp_dis
     join NDS2_MRR_USER.SOV_DISCREPANCY dis on dis.id = knp_dis.discrepancy_id;

create unique index NDS2_MRR_USER.IX_KNP_DIS_ID on NDS2_MRR_USER.KNP_DISCREPANCY (ID) tablespace NDS2_IDX;
create unique index NDS2_MRR_USER.IX_KNP_DIS on NDS2_MRR_USER.KNP_DISCREPANCY (ID, STATUS) tablespace NDS2_IDX;

create table NDS2_MRR_USER.KNP_DISCREPANCY_DECLARATION pctfree 0 nologging as
  select
     knp.id as discrepancy_id
    ,b_decl.innnp as inn_declarant
    ,dis.buyer_inn as inn
    ,b_decl.innreorg as inn_reorganized
    ,b_decl.kpp_effective
    ,b_decl.period as period_code
    ,b_decl.otchetgod as fiscal_year
    ,b_decl.type as type_code
    ,nvl(s_decl.innnp,dis.seller_inn) as contractor_inn_declarant
    ,dis.seller_inn as contractor_inn
    ,s_decl.innreorg as contractor_inn_reorganized
    ,s_decl.kppnp as contractor_kpp
    ,nvl(s_decl.kpp_effective,'111111111') as contractor_kpp_effective
    ,s_decl.period as contractor_period_code
    ,s_decl.otchetgod as contractor_fiscal_year 
    ,s_decl.type as contractor_type_code    
    ,dis.type as discrepancy_type
    ,dis.amnt as discrepancy_amt
    ,knp.status as discrepancy_status
    ,dis.invoice_rk as invoice_row_key
    ,inv.invoice_num as invoice_number
    ,inv.invoice_date
    ,dis.invoice_chapter
    ,inv.price_buy_amount as invoice_amt
    ,inv.price_buy_nds_amount as invoice_nds_amt
  from 
    NDS2_MRR_USER.KNP_DISCREPANCY knp
    join NDS2_MRR_USER.SOV_DISCREPANCY dis on dis.id = knp.id
    join NDS2_MRR_USER.ASK_DECLANDJRNL b_decl on b_decl.zip = dis.buyer_zip
    left join NDS2_MRR_USER.ASK_DECLANDJRNL s_decl on s_decl.zip = dis.seller_zip
    left join NDS2_MRR_USER.SOV_INVOICE_ALL inv on inv.row_key = dis.invoice_rk
  union all
  select
     knp.id
    ,s_decl.innnp 
    ,dis.seller_inn 
    ,s_decl.innreorg 
    ,s_decl.kpp_effective
    ,s_decl.period 
    ,s_decl.otchetgod
    ,s_decl.type as type_code
    ,b_decl.innnp 
    ,dis.buyer_inn 
    ,b_decl.innreorg 
    ,b_decl.kppnp
    ,b_decl.kpp_effective 
    ,b_decl.period 
    ,b_decl.otchetgod
    ,b_decl.type as contractor_type_code
    ,dis.type 
    ,dis.amnt 
    ,knp.status
    ,dis.invoice_contractor_rk
    ,inv.invoice_num 
    ,inv.invoice_date
    ,dis.invoice_chapter
    ,inv.price_sell
    ,nvl(inv.price_sell_18,0) + nvl(inv.price_sell_10,0)
  from 
    NDS2_MRR_USER.KNP_DISCREPANCY knp
    join NDS2_MRR_USER.SOV_DISCREPANCY dis on dis.id = knp.id
    join NDS2_MRR_USER.ASK_DECLANDJRNL b_decl on b_decl.zip = dis.buyer_zip    
    join NDS2_MRR_USER.ASK_DECLANDJRNL s_decl on s_decl.zip = dis.seller_zip
    left join NDS2_MRR_USER.SOV_INVOICE_ALL inv on inv.row_key = dis.invoice_contractor_rk
  where dis.buyer_inn <> dis.seller_inn 
     or b_decl.kpp_effective <> s_decl.kpp_effective
     or b_decl.period <> s_decl.period
     or b_decl.otchetgod <> s_decl.otchetgod;

create index NDS2_MRR_USER.IX_KNP_DIS_DECL_DECLID on NDS2_MRR_USER.KNP_DISCREPANCY_DECLARATION (
       INN_DECLARANT,
       INN,
       KPP_EFFECTIVE,
       FISCAL_YEAR,
       PERIOD_CODE,
       TYPE_CODE) tablespace NDS2_IDX;   
create index NDS2_MRR_USER.IX_KNP_DIS_DECL_DISID on NDS2_MRR_USER.KNP_DISCREPANCY_DECLARATION (DISCREPANCY_ID) tablespace NDS2_IDX;
create index NDS2_MRR_USER.IX_KNP_DIS_DECL_SUM on NDS2_MRR_USER.KNP_DISCREPANCY_DECLARATION (
        DISCREPANCY_ID
       ,INN
       ,KPP_EFFECTIVE
       ,TYPE_CODE
       ,PERIOD_CODE
       ,FISCAL_YEAR
       ,CONTRACTOR_INN
       ,CONTRACTOR_KPP_EFFECTIVE) tablespace NDS2_IDX;
create index NDS2_MRR_USER.IX_KNP_DIS_DECL_CTR on NDS2_MRR_USER.KNP_DISCREPANCY_DECLARATION (INVOICE_CHAPTER, INN, KPP_EFFECTIVE, TYPE_CODE, PERIOD_CODE, FISCAL_YEAR, CONTRACTOR_INN, CONTRACTOR_KPP_EFFECTIVE) tablespace NDS2_IDX;
create index NDS2_MRR_USER.IX_KNP_DIS_DECL on NDS2_MRR_USER.KNP_DISCREPANCY_DECLARATION (DISCREPANCY_ID,INN_DECLARANT, INN, KPP_EFFECTIVE, TYPE_CODE, PERIOD_CODE, FISCAL_YEAR) tablespace NDS2_IDX;
create index NDS2_MRR_USER.IX_KNP_DIS_DECL_GROUP on NDS2_MRR_USER.KNP_DISCREPANCY_DECLARATION (
       INVOICE_CHAPTER
      ,INN
      ,KPP_EFFECTIVE
      ,TYPE_CODE
      ,PERIOD_CODE
      ,FISCAL_YEAR
      ,CONTRACTOR_INN
      ,CONTRACTOR_KPP_EFFECTIVE
      ,INVOICE_ROW_KEY) tablespace NDS2_IDX;
