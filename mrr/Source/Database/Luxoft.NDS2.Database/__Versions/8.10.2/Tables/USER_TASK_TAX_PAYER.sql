﻿/*==============================================================*/
/* Table: USER_TASK_TAX_PAYER                                   */
/*==============================================================*/
create table NDS2_MRR_USER.USER_TASK_TAX_PAYER 
(
   TASK_ID              NUMBER               not null,
   INN                  VARCHAR(12 CHAR)     not null,
   EFFECTIVE_KPP        VARCHAR(9 CHAR)
);

comment on column NDS2_MRR_USER.USER_TASK_TAX_PAYER.TASK_ID is
'Идентификатор задания';

comment on column NDS2_MRR_USER.USER_TASK_TAX_PAYER.INN is
'ИНН налогоплательзика';

comment on column NDS2_MRR_USER.USER_TASK_TAX_PAYER.EFFECTIVE_KPP is
'Эффективный КПП налогоплательщика';
