﻿CREATE SEQUENCE  NDS2_MRR_USER.SEQ_USER_TASK_ID
MINVALUE 1 
MAXVALUE 999999999999999
INCREMENT BY 1 
START WITH 1 
NOCACHE  
NOCYCLE;

CREATE SEQUENCE  NDS2_MRR_USER.SEQ_TP_SOLVENCY_ID
MINVALUE 1 
MAXVALUE 999999999999999
INCREMENT BY 1 
START WITH 1 
NOCACHE  
NOCYCLE;

CREATE SEQUENCE  NDS2_MRR_USER.SEQ_USER_TASK_REPORT_ID
MINVALUE 1 
MAXVALUE 999999999999999
INCREMENT BY 1 
START WITH 1 
NOCACHE  
NOCYCLE;
