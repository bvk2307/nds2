﻿BEGIN
	DBMS_SCHEDULER.SET_ATTRIBUTE (
	  name      => 'NDS2_MRR_USER.J$DAILY_JOB' 
	, attribute => 'start_date' 
	, value     => TO_DATE(TO_CHAR(SYSDATE, 'dd/mm/yyyy') || ' 21:00:00', 'dd/mm/yyyy HH24:mi:ss') 
	);
END;
/

begin
	dbms_scheduler.enable('NDS2_MRR_USER.J$DAILY_JOB'); 
end;
/ 
