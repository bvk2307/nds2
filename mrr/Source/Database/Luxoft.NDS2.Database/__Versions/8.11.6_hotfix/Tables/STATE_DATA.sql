﻿begin
  execute immediate 'drop table NDS2_MRR_USER.STATE_DATA';
  exception when others then null;
end;
/

begin
  execute immediate 'drop table NDS2_MRR_USER.STATE';
  exception when others then null;
end;
/

CREATE TABLE NDS2_MRR_USER.STATE_DATA
(	
	PARAM_NAME VARCHAR2(30 CHAR) NOT NULL ENABLE, 
	PARAM_VALUE VARCHAR2(30 CHAR), 
	TS TIMESTAMP (6), 
	CONSTRAINT STATE_DATA_PK PRIMARY KEY (PARAM_NAME)
); 
