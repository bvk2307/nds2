﻿declare pMinValue number;
begin
  begin
    execute immediate 'drop sequence NDS2_MRR_USER.SEQ$KNP_DISCR_CLOSE_REASON';
    exception when others then null;
  end;
  
  select nvl(MAX(ID), 0) + 1 into pMinValue from dual, NDS2_MRR_USER.R$KNP_DISCREPANCY_CLOSE_REASON;

  execute immediate 'create sequence NDS2_MRR_USER.SEQ$KNP_DISCR_CLOSE_REASON start with '||pMinValue||
  ' increment by 1 maxvalue 99999999999999 minvalue '||pMinValue||' nocache cycle';

end;
/