﻿delete from NDS2_MRR_USER.CFG_INDEX t where t.id in (235, 236, 237);

-- DECLARATION_KNP_STATUS
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type, op_statistics, op_parallel_count, op_noparallel_postbuild)
values(235, 'DECLARATION_KNP_STATUS', 'IX_DECLKNPST_ZIP', 'ZIP', 'NONUNIQUE', 1, 4, 1);
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type, op_statistics, op_parallel_count, op_noparallel_postbuild)
values(236, 'DECLARATION_KNP_STATUS', 'IX_DECLKNPST_DECL_DATE', 'DECL_INSERT_DATE', 'NONUNIQUE', 1, 4, 1);
insert into NDS2_MRR_USER.CFG_INDEX (id, table_name, index_name, column_list, index_type, op_statistics, op_parallel_count, op_noparallel_postbuild)
values(237, 'DECLARATION_KNP_STATUS', 'IX_DECLKNPST_SEOD_DATE', 'SEOD_CREATION_DATE', 'NONUNIQUE', 1, 4, 1);

commit;
