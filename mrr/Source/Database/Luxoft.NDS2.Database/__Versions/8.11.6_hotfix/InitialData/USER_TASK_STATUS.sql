﻿truncate table NDS2_MRR_USER.USER_TASK_STATUS;

insert into NDS2_MRR_USER.USER_TASK_STATUS values(1, 'Выполнено');
insert into NDS2_MRR_USER.USER_TASK_STATUS values(2, 'Закрыто');
insert into NDS2_MRR_USER.USER_TASK_STATUS values(3, 'Создано');

commit;