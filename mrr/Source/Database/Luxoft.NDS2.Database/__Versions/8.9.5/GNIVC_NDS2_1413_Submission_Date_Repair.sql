﻿merge into NDS2_MRR_USER.seod_declaration sd 
  using (
    select distinct code_nsi_sono as sono_cd 
        ,to_number(xmltype(xml_data).extract('/Файл/Документ/@РегНомДек').getStringVal()) as seod_reg_num 
        ,case xmltype(xml_data).existsNode('/Файл/Документ/СвНП/НПЮЛ') 
          when 1 then xmltype(xml_data).extract('/Файл/Документ/СвНП/НПЮЛ/@ИННЮЛ').getStringVal()
          else xmltype(xml_data).extract('/Файл/Документ/СвНП/НПФЛ/@ИННФЛ').getStringVal()
         end as inn 
        ,case xmltype(xml_data).existsNode('/Файл/Документ/СвНП/НПЮЛ') 
          when 1 then xmltype(xml_data).extract('/Файл/Документ/СвНП/НПЮЛ/@КПП').getStringVal()
          else null
         end as kpp 
        ,case 
          when xmltype(xml_data).extract('/Файл/Документ/@ДатаПредНД') is not null then to_date(xmltype(xml_data).extract('/Файл/Документ/@ДатаПредНД').getStringVal(), 'DD.MM.YYYY') 
          else NULL 
         end as submission_date
    from NDS2_MRR_USER.seod_incoming 
    where info_type = 'CAM_NDS2_01' 
      and date_receipt >= to_date('14.04.2017', 'dd.mm.yyyy')
  ) tmp 
    on (tmp.seod_reg_num = sd.decl_reg_num and tmp.sono_cd = sd.sono_code and tmp.inn = sd.inn and nvl(tmp.kpp, '0') = nvl(sd.kpp, '0'))
  when matched then
    update set sd.submission_date = tmp.submission_date;