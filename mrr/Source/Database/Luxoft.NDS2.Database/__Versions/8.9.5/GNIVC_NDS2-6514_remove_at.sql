﻿MERGE INTO NDS2_MRR_USER.SELECTION_DISCREPANCY seldiscr
USING (
        SELECT distinct sd.ZIP,
          sd.SELECTION_ID   AS SELECTION_ID,
          sd.DISCREPANCY_ID AS DISCREPANCY_ID
        FROM NDS2_MRR_USER.SELECTION s
          JOIN NDS2_MRR_USER.SELECTION_DECLARATION sdecl
            ON s.ID = sdecl.SELECTION_ID
          JOIN NDS2_MRR_USER.SELECTION_DISCREPANCY sd
            ON sdecl.SELECTION_ID = sd.SELECTION_ID
               AND sdecl.ZIP = sd.ZIP
          LEFT JOIN NDS2_MRR_USER.DOC_DISCREPANCY dd
            ON sd.DISCREPANCY_ID = dd.DISCREPANCY_ID
        WHERE
          s.STATUS = 10 -- В отправленных выборках ищем различные декларации,
          AND sd.IS_IN_PROCESS = 1 -- расхождения в которых помечены обработанными,
          AND sd.STATUS = 1 -- но являются открытыми
          AND dd.DISCREPANCY_ID IS NULL -- но для них нет документа
      ) for_update
ON (seldiscr.ZIP = for_update.ZIP AND seldiscr.SELECTION_ID = for_update.SELECTION_ID AND seldiscr.DISCREPANCY_ID = for_update.DISCREPANCY_ID)
WHEN MATCHED THEN UPDATE
SET seldiscr.IS_IN_PROCESS = 0;
COMMIT;

