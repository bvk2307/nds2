﻿declare
v_exist number(1);
begin
  
  select count(1) into v_exist
  from all_tables 
  where 
  table_name = 'SOV_DISCREPANCY_INVOICE_QTR' 
  and owner = 'NDS2_MRR_USER';
  
  if v_exist = 0 then
    execute immediate '
create table NDS2_MRR_USER.SOV_DISCREPANCY_INVOICE_QTR
(
   discrepancy_id     number               not null,
   fiscal_year        number               not null,
   quarter            number               not null,
   has_invoice_duplicate number(1)            not null
)';

	execute immediate 'create index NDS2_MRR_USER.idx_sov_dis_inv_qtr on NDS2_MRR_USER.SOV_DISCREPANCY_INVOICE_QTR(DISCREPANCY_ID, FISCAL_YEAR, QUARTER) tablespace NDS2_IDX';
  end if;
end;
/

comment on table NDS2_MRR_USER.SOV_DISCREPANCY_INVOICE_QTR is 'перечень кварталов отчетных периодов счетов-фактур, которые входят в расхождение по стороне покупателя';
