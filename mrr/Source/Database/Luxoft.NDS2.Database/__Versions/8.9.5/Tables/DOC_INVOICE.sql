﻿alter table NDS2_MRR_USER.DOC_INVOICE 
modify
(
  price_buy_amount          NUMBER(22,2),
  price_buy_nds_amount      NUMBER(22,2),
  price_sell                NUMBER(22,2),
  price_sell_in_curr        NUMBER(22,2),
  price_sell_18             NUMBER(22,2),
  price_sell_10             NUMBER(22,2),
  price_sell_0              NUMBER(22,2),
  price_nds_18              NUMBER(22,2),
  price_nds_10              NUMBER(22,2),
  price_tax_free            NUMBER(22,2),
  price_total               NUMBER(22,2),
  price_nds_total           NUMBER(22,2),
  diff_correct_decrease     NUMBER(22,2),
  diff_correct_increase     NUMBER(22,2),
  diff_correct_nds_decrease NUMBER(22,2),
  diff_correct_nds_increase NUMBER(22,2),
  price_nds_buyer           NUMBER(22,2)
);