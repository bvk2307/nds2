﻿create table nds2_mrr_user.explain_tks_receipt_document (
  explain_zip NUMBER not null,
  invoice_row_key VARCHAR2(128 CHAR) not null,
  state NUMBER(1),
  doc_num VARCHAR2(64 CHAR),
  doc_date DATE
);
