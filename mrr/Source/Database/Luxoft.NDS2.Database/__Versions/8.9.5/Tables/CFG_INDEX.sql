﻿alter table NDS2_MRR_USER.CFG_INDEX add op_statistics number(1) default 1;
alter table NDS2_MRR_USER.CFG_INDEX add op_parallel_count number(2) default 4;
alter table NDS2_MRR_USER.CFG_INDEX add op_noparallel_postbuild number(1) default 1;