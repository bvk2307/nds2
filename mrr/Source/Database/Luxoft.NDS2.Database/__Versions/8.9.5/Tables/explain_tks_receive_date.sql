﻿create table nds2_mrr_user.explain_tks_receive_date (
  explain_zip NUMBER not null,
  invoice_row_key VARCHAR2(128 CHAR) not null,
  state NUMBER(1),
  receive_date DATE 
);
