﻿begin
 NDS2_MRR_USER.nds2$sys.DROP_IF_EXISTS_EX(P_OBJ_NAME => 'DECLARATION_OWNER2', P_SCHEME => 'NDS2_MRR_USER', P_OBJ_TYPE => 'TABLE', P_CHILD_OBJ => null);
end;
/

create table NDS2_MRR_USER.DECLARATION_OWNER2 as 
select type_code, inn_declarant, kpp_effective, fiscal_year, period_code, inspector_sid, inspector_name  from 
(
select 
row_number() over (partition by type_code, inn_declarant, kpp_effective, fiscal_year, period_code order by inspector_name desc) as rn,
type_code, inn_declarant, kpp_effective, fiscal_year, period_code, inspector_sid, inspector_name 
from NDS2_MRR_USER.declaration_owner
) T where rn = 1;

declare 
v_num_errors number;
begin
  DBMS_REDEFINITION.abort_redef_table(
  uname => 'NDS2_MRR_USER', 
  int_table => 'DECLARATION_OWNER', 
  orig_table => 'DECLARATION_OWNER2');
    

  dbms_redefinition.can_redef_table(
  uname => 'NDS2_MRR_USER', 
  tname => 'DECLARATION_OWNER',
  options_flag => DBMS_REDEFINITION.CONS_USE_ROWID);
  
  DBMS_REDEFINITION.start_redef_table(
  uname => 'NDS2_MRR_USER', 
  int_table => 'DECLARATION_OWNER', 
  orig_table => 'DECLARATION_OWNER2',
  options_flag => DBMS_REDEFINITION.CONS_USE_ROWID); 
  
  DBMS_REDEFINITION.COPY_TABLE_DEPENDENTS(
  uname => 'NDS2_MRR_USER', 
  int_table => 'DECLARATION_OWNER', 
  orig_table => 'DECLARATION_OWNER2', 
  copy_indexes => 0, 
  copy_statistics => true, 
  copy_constraints => false, num_errors => v_num_errors );
  
  dbms_output.put_line(v_num_errors);
  
  DBMS_REDEFINITION.finish_redef_table(
  uname => 'NDS2_MRR_USER', 
  int_table => 'DECLARATION_OWNER', 
  orig_table => 'DECLARATION_OWNER2');
  
end;
/

begin
 execute immediate 'drop index NDS2_MRR_USER.IDX$$_46100003'; 
 exception when others then null;
end;
/
begin
 execute immediate 'drop index NDS2_MRR_USER.idx_decl_owner'; 
 exception when others then null;
end;
/

create unique index NDS2_MRR_USER.idx_decl_owner on NDS2_MRR_USER.DECLARATION_OWNER (PERIOD_CODE, FISCAL_YEAR, KPP_EFFECTIVE, INN_DECLARANT, TYPE_CODE) tablespace NDS2_IDX compute statistics;

