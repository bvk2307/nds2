﻿create table nds2_mrr_user.explain_tks_contractor (
  explain_zip NUMBER not null,
  invoice_row_key VARCHAR2(128 CHAR) not null,
  state NUMBER(1),
  inn VARCHAR2(12 CHAR),
  kpp VARCHAR2(9 CHAR)
);
