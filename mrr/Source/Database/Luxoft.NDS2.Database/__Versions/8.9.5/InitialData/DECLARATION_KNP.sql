﻿drop table NDS2_MRR_USER.DECLARATION_KNP;
create table nds2_mrr_user.declaration_knp as
select * from nds2_mrr_user.v$declaration_knp_build;

create index NDS2_MRR_USER.IDX_DECLARATION_KNP_ZIP on NDS2_MRR_USER.DECLARATION_KNP (ZIP);