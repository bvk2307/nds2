﻿-- сбрасываем флаг результата работы у записей с данными потока ЭОД11, обработанными с ошибкой

update NDS2_MRR_USER.seod_incoming 
  set processing_result = 0 
  where processing_result = 3 
    and info_type = 'CAM_NDS2_11';
commit;

-- в цикле проходим по всем процессам обработки потоков, у которых сброшен флаг результата работы и заново их обрабатываем
begin 
  for jLine in 
    ( 
      select distinct q.SESSION_ID 
        from NDS2_MRR_USER.seod_incoming t 
        inner join NDS2_MRR_USER.queue_seod_stream_process q 
          on q.session_id = t.session_id 
        where t.processing_result = 0 
          and q.processed is not null
    ) 
  loop
    update NDS2_MRR_USER.queue_seod_stream_process set processed = null where SESSION_ID = jLine.SESSION_ID;
    commit;
    NDS2_MRR_USER.PAC$SEOD_EXCHANGE.P$PROCESS_SEOD_INCOME;
  end loop;
end;
/
