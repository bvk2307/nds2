﻿create table NDS2_MRR_USER.TMP_GNIVC_NDS2_5180 nologging as
select distinct
  seod.id,
  seod.decl_reg_num,
  seod.sono_code,
  seod.type,
  ask.idfajl as id_file_correct
from NDS2_MRR_USER.ASK_DECLANDJRNL ask
  inner join NDS2_MRR_USER.SEOD_DECLARATION seod on
        seod.tax_period = ask.period
   and seod.fiscal_year = ask.otchetgod
    and seod.inn = ask.innnp
    and seod.correction_number = ask.nomkorr
    and seod.id_file = substr(ask.idfajl, 1, 100)
    and seod.type = ask.type
where length(seod.id_file) = 100
  and length(ask.idfajl) > 100;
    
update NDS2_MRR_USER.SEOD_DECLARATION seod
set id_file = (
  select tmp.id_file_correct
  from NDS2_MRR_USER.TMP_GNIVC_NDS2_5180 tmp
  where tmp.id = seod.id
    and tmp.decl_reg_num = seod.decl_reg_num
    and tmp.sono_code = seod.sono_code
    and tmp.type = seod.type)
where exists (
  select 1
  from NDS2_MRR_USER.TMP_GNIVC_NDS2_5180 tmp
  where tmp.id = seod.id
    and tmp.decl_reg_num = seod.decl_reg_num
    and tmp.sono_code = seod.sono_code
    and tmp.type = seod.type);

drop table NDS2_MRR_USER.TMP_GNIVC_NDS2_5180;

commit;
