﻿-- сбрасываем флаг результата работы у записей с данными потока ЭОД11, обработанными с ошибкой
update NDS2_MRR_USER.seod_incoming 
  set processing_result = 0 
  where processing_result = 3 
    and info_type = 'CAM_NDS2_11';
commit;

-- в цикле проходим по всем процессам обработки потоков, у которых сброшен флаг результата работы и заново их обрабатываем
begin 
  for jLine in 
    ( 
      select distinct q.job_id
        from NDS2_MRR_USER.seod_incoming t 
        inner join NDS2_MRR_USER.queue_seod_stream_process q 
          on q.session_id = t.session_id 
        where t.processing_result = 0 
          and q.processed is not null
    ) 
  loop
    NDS2_MRR_USER.PAC$SEOD_EXCHANGE.P$PROCESS_NEW_SEOD_DATA(v_job_id => jLine.job_id);
  end loop;
end;
/
