﻿update NDS2_MRR_USER.DECLARATION_ACTIVE da 
set da.status_knp_code = case 
    when da.status_knp = 'Открыта' then 1 
    when da.status_knp = 'Закрыта' then 2
    else 0 end;
commit;