﻿create table NDS2_MRR_USER.DOC_DISCREPANCY_TEMP
PCTFREE 0
nologging as 
(
select d.DOC_ID, d.DISCREPANCY_ID, d.ROW_KEY, s.AMNT as AMOUNT, s.AMOUNT_PVP
from NDS2_MRR_USER.DOC_DISCREPANCY d
join NDS2_MRR_USER.SOV_DISCREPANCY s on s.ID = d.DISCREPANCY_ID
);

DROP TABLE NDS2_MRR_USER.DOC_DISCREPANCY;

ALTER TABLE NDS2_MRR_USER.DOC_DISCREPANCY_TEMP RENAME TO DOC_DISCREPANCY;

CREATE INDEX NDS2_MRR_USER.IDX_DOC_DIS_DISID ON NDS2_MRR_USER.DOC_DISCREPANCY (DISCREPANCY_ID); 

CREATE INDEX NDS2_MRR_USER.IDX_DOC_DIS_ID ON NDS2_MRR_USER.DOC_DISCREPANCY (DOC_ID, DISCREPANCY_ID); 

merge into NDS2_MRR_USER.DOC dest
using
(
      select d.DOC_ID, dh.FISCAL_YEAR, dtp.QUARTER, dh.zip, data.amount, data.amount_pvp, data.discrepancy_count 
        from 
          ( 
            select dc.doc_id 
                  ,dc.ref_entity_id 
                  ,dc.sono_code 
                  ,case dc.doc_id 
                     when 12555297 then 2399011226510277246 
                     when 12565953 then 9163699341797862763 
                     when 12767420 then 6297158178976412605 
                     when 12776416 then 7420243336052049841 
                     when 12805162 then 4066187513567056036   
                     when 12805569 then 7153968008083776612 
                     when 12860923 then 3738269165699578510 
                     when 12866811 then 5243034391195071012 
                     when 12870812 then 8811011195979213511 
                     when 12871804 then 7979534114775930109 
                     when 12877759 then 7524107602457908850 
                     when 12880472 then 2116973299846587580 
                     when 12895473 then 4186377328622942683 
                     when 12921220 then 2849371189247713549 
                     when 19332573 then 8230328319026711370 
                     when 19405075 then 6714867044417345873 
                     when 19695155 then 2285576810898400645 
                     when 19695635 then 2713418775498660181 
                     when 23395250 then 8365717782832000416 
                     when 23452354 then 2724677774573710891 
                     when 23491536 then 6169087064581827434 
                     when 23491685 then 7395473538110336568 
                     when 23708270 then 2111062325344533456 
                     else null 
                   end as zip                       
              from NDS2_MRR_USER.DOC dc 
              where dc.doc_id in (19332573, 12860923, 23708270, 23452354, 12767420, 23395250, 12895473, 19695635, 12805569, 12877759, 12866811, 12805162, 12921220, 12555297, 23491685, 12871804, 12776416, 12565953, 23491536, 12880472, 19695155, 12870812, 19405075) 
          ) d 
        join NDS2_MRR_USER.declaration_history dh on d.REF_ENTITY_ID = dh.reg_number and d.SONO_CODE = dh.SONO_CODE_SUBMITED and d.zip = dh.zip
        join NDS2_MRR_USER.dict_tax_period dtp on dtp.code = dh.period_code
        join (
          select dd.doc_id, sum(sd.amnt) as amount, sum(sd.amount_pvp) as amount_pvp, count(*) as discrepancy_count
          from 
          NDS2_MRR_USER.doc_discrepancy dd
          join NDS2_MRR_USER.sov_discrepancy sd on dd.discrepancy_id = sd.id
          group by dd.doc_id
        ) data on data.doc_id = d.doc_id 
) src
  on (dest.doc_id = src.doc_id)
  when matched then UPDATE
  set dest.fiscal_year = src.fiscal_year
      ,dest.quarter = src.quarter
      ,dest.zip = src.zip
      ,dest.discrepancy_amount = src.amount
      ,dest.discrepancy_amount_pvp = src.amount_pvp
      ,dest.discrepancy_count = src.discrepancy_count;
   commit;
   /
   
declare 
  i number;
  max_id number;
begin
  i := 0;
  
  select max(doc_id) into max_id from NDS2_MRR_USER.DOC; 
  while i < max_id 
  LOOP
  begin
    merge into NDS2_MRR_USER.DOC dest
    using
    (
      select d.DOC_ID, dh.FISCAL_YEAR, dtp.QUARTER, dh.zip, data.amount, data.amount_pvp, data.discrepancy_count  
        from 
          ( 
          select dc.doc_id 
              ,dc.ref_entity_id 
              ,dc.sono_code 
            from NDS2_MRR_USER.DOC dc
            where doc_id not in (19332573, 12860923, 23708270, 23452354, 12767420, 23395250, 12895473, 19695635, 12805569, 12877759, 12866811, 12805162, 12921220, 12555297, 23491685, 12871804, 12776416, 12565953, 23491536, 12880472, 19695155, 12870812, 19405075) 
          ) d 
        join NDS2_MRR_USER.declaration_history dh on d.REF_ENTITY_ID = dh.reg_number and d.SONO_CODE = dh.SONO_CODE_SUBMITED
        join NDS2_MRR_USER.dict_tax_period dtp on dtp.code = dh.period_code
        join (
          select dd.doc_id, sum(sd.amnt) as amount, sum(sd.amount_pvp) as amount_pvp, count(*) as discrepancy_count
          from 
          NDS2_MRR_USER.doc_discrepancy dd
          join NDS2_MRR_USER.sov_discrepancy sd on dd.discrepancy_id = sd.id
          group by dd.doc_id
        ) data on data.doc_id = d.doc_id 
        where d.doc_id between i + 1 and i + 5000  
    ) src
      on (dest.doc_id = src.doc_id)
      when matched then UPDATE
      set dest.fiscal_year = src.fiscal_year
        ,dest.quarter = src.quarter
        ,dest.zip = src.zip
        ,dest.discrepancy_amount = src.amount
        ,dest.discrepancy_amount_pvp = src.amount_pvp
        ,dest.discrepancy_count = src.discrepancy_count;
    commit;
    
    i := i + 5000;
  end;
  END LOOP;
end;
/
