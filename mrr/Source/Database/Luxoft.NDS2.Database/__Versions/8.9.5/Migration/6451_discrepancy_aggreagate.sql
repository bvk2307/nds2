﻿begin
  execute immediate 'drop table NDS2_MRR_USER.DISCREPANCY_AGGREGATE_MIGR';
  exception when others then null;
end;
/

create table NDS2_MRR_USER.DISCREPANCY_AGGREGATE_MIGR 
  tablespace NDS2_DATA
  pctfree 10
partition by list (REGION_CODE)
subpartition by list (SONO_CODE)  
(
  partition REGION_V_01 values ('01') 
  (
    subpartition SONO_V_0100 values ('0100'), 
    subpartition SONO_V_0101 values ('0101'),
    subpartition SONO_V_0105 values ('0105'),
    subpartition SONO_V_0107 values ('0107'),
    subpartition SONO_V_01_DEAFULT values (default)
  ),
  partition REGION_V_02 values ('02')
  (
    subpartition SONO_V_0200 values ('0200'),
    subpartition SONO_V_0252 values ('0252'),
    subpartition SONO_V_0256 values ('0256'),
    subpartition SONO_V_0261 values ('0261'),
    subpartition SONO_V_0264 values ('0264'),
    subpartition SONO_V_0267 values ('0267'),
    subpartition SONO_V_0268 values ('0268'),
    subpartition SONO_V_0269 values ('0269'),
    subpartition SONO_V_0272 values ('0272'),
    subpartition SONO_V_0273 values ('0273'),
    subpartition SONO_V_0274 values ('0274'),
    subpartition SONO_V_0276 values ('0276'),
    subpartition SONO_V_0277 values ('0277'),
    subpartition SONO_V_0278 values ('0278'),
    subpartition SONO_V_0280 values ('0280'),
    subpartition SONO_V_02_DEAFULT values (default)
  ),
  partition REGION_V_03 values ('03')
  (
    subpartition SONO_V_0300 values ('0300'),
    subpartition SONO_V_0309 values ('0309'),
    subpartition SONO_V_0317 values ('0317'),
    subpartition SONO_V_0318 values ('0318'),
    subpartition SONO_V_0323 values ('0323'),
    subpartition SONO_V_0326 values ('0326'),
    subpartition SONO_V_0327 values ('0327'),
    subpartition SONO_V_03_DEAFULT values (default)
  ),
  partition REGION_V_04 values ('04')
  (
    subpartition SONO_V_0400 values ('0400'),
    subpartition SONO_V_0404 values ('0404'),
    subpartition SONO_V_0411 values ('0411'),
    subpartition SONO_V_04_DEAFULT values (default)
  ),
  partition REGION_V_05 values ('05')
  (
    subpartition SONO_V_0500 values ('0500'),
    subpartition SONO_V_0506 values ('0506'),
    subpartition SONO_V_0507 values ('0507'),
    subpartition SONO_V_0521 values ('0521'),
    subpartition SONO_V_0522 values ('0522'),
    subpartition SONO_V_0523 values ('0523'),
    subpartition SONO_V_0529 values ('0529'),
    subpartition SONO_V_0531 values ('0531'),
    subpartition SONO_V_0533 values ('0533'),
    subpartition SONO_V_0536 values ('0536'),
    subpartition SONO_V_0541 values ('0541'),
    subpartition SONO_V_0542 values ('0542'),
    subpartition SONO_V_0544 values ('0544'),
    subpartition SONO_V_0545 values ('0545'),
    subpartition SONO_V_0546 values ('0546'),
    subpartition SONO_V_0547 values ('0547'),
    subpartition SONO_V_0548 values ('0548'),
    subpartition SONO_V_0550 values ('0550'),
    subpartition SONO_V_0571 values ('0571'),
    subpartition SONO_V_0572 values ('0572'),
    subpartition SONO_V_0573 values ('0573'),
    subpartition SONO_V_05_DEAFULT values (default)
  ),
  partition REGION_V_06 values ('06')
  (
    subpartition SONO_V_0600 values ('0600'),
    subpartition SONO_V_0601 values ('0601'),
    subpartition SONO_V_0603 values ('0603'),
    subpartition SONO_V_0608 values ('0608'),
    subpartition SONO_V_06_DEAFULT values (default)
  ),
  partition REGION_V_07 values ('07')
  (
    subpartition SONO_V_0700 values ('0700'),
    subpartition SONO_V_0716 values ('0716'),
    subpartition SONO_V_0718 values ('0718'),
    subpartition SONO_V_0720 values ('0720'),
    subpartition SONO_V_0724 values ('0724'),
    subpartition SONO_V_0725 values ('0725'),
    subpartition SONO_V_0726 values ('0726'),
    subpartition SONO_V_07_DEAFULT values (default)
  ),
  partition REGION_V_08 values ('08')
  (
    subpartition SONO_V_0800 values ('0800'),
    subpartition SONO_V_0801 values ('0801'),
    subpartition SONO_V_0813 values ('0813'),
    subpartition SONO_V_0816 values ('0816'),
    subpartition SONO_V_0817 values ('0817'),
    subpartition SONO_V_08_DEAFULT values (default)
  ),
  partition REGION_V_09 values ('09')
  (
    subpartition SONO_V_0900 values ('0900'),
    subpartition SONO_V_0912 values ('0912'),
    subpartition SONO_V_0916 values ('0916'),
    subpartition SONO_V_0917 values ('0917'),
    subpartition SONO_V_0918 values ('0918'),
    subpartition SONO_V_0919 values ('0919'),
    subpartition SONO_V_0920 values ('0920'),
    subpartition SONO_V_09_DEAFULT values (default)
  ),
  partition REGION_V_10 values ('10')
  (
    subpartition SONO_V_1000 values ('1000'),
    subpartition SONO_V_1001 values ('1001'),
    subpartition SONO_V_1031 values ('1031'),
    subpartition SONO_V_1032 values ('1032'),
    subpartition SONO_V_1035 values ('1035'),
    subpartition SONO_V_1039 values ('1039'),
    subpartition SONO_V_1040 values ('1040'),
    subpartition SONO_V_10_DEAFULT values (default)
  ),
  partition REGION_V_11 values ('11')
  (
    subpartition SONO_V_1100 values ('1100'),
    subpartition SONO_V_1101 values ('1101'),
    subpartition SONO_V_1102 values ('1102'),
    subpartition SONO_V_1103 values ('1103'),
    subpartition SONO_V_1104 values ('1104'),
    subpartition SONO_V_1105 values ('1105'),
    subpartition SONO_V_1106 values ('1106'),
    subpartition SONO_V_1108 values ('1108'),
    subpartition SONO_V_1109 values ('1109'),
    subpartition SONO_V_1121 values ('1121'),
    subpartition SONO_V_1122 values ('1122'),
    subpartition SONO_V_11_DEAFULT values (default)
  ),
  partition REGION_V_12 values ('12')
  (
    subpartition SONO_V_1200 values ('1200'),
    subpartition SONO_V_1215 values ('1215'),
    subpartition SONO_V_1218 values ('1218'),
    subpartition SONO_V_1223 values ('1223'),
    subpartition SONO_V_1224 values ('1224'),
    subpartition SONO_V_1225 values ('1225'),
    subpartition SONO_V_1226 values ('1226'),
    subpartition SONO_V_12_DEAFULT values (default)
  ),
  partition REGION_V_13 values ('13')
  (
    subpartition SONO_V_1300 values ('1300'),
    subpartition SONO_V_1308 values ('1308'),
    subpartition SONO_V_1310 values ('1310'),
    subpartition SONO_V_1314 values ('1314'),
    subpartition SONO_V_1322 values ('1322'),
    subpartition SONO_V_1323 values ('1323'),
    subpartition SONO_V_1324 values ('1324'),
    subpartition SONO_V_1326 values ('1326'),
    subpartition SONO_V_1327 values ('1327'),
    subpartition SONO_V_1328 values ('1328'),
    subpartition SONO_V_13_DEAFULT values (default)
  ),
  partition REGION_V_14 values ('14')
  (
    subpartition SONO_V_1400 values ('1400'),
    subpartition SONO_V_1402 values ('1402'),
    subpartition SONO_V_1426 values ('1426'),
    subpartition SONO_V_1434 values ('1434'),
    subpartition SONO_V_1436 values ('1436'),
    subpartition SONO_V_1445 values ('1445'),
    subpartition SONO_V_1446 values ('1446'),
    subpartition SONO_V_1447 values ('1447'),
    subpartition SONO_V_1448 values ('1448'),
    subpartition SONO_V_1449 values ('1449'),
    subpartition SONO_V_1450 values ('1450'),
    subpartition SONO_V_14_DEAFULT values (default)
  ),
  partition REGION_V_15 values ('15')
  (
    subpartition SONO_V_1500 values ('1500'),
    subpartition SONO_V_1510 values ('1510'),
    subpartition SONO_V_1511 values ('1511'),
    subpartition SONO_V_1512 values ('1512'),
    subpartition SONO_V_1513 values ('1513'),
    subpartition SONO_V_1514 values ('1514'),
    subpartition SONO_V_15_DEAFULT values (default)
  ),
  partition REGION_V_16 values ('16')
  (
    subpartition SONO_V_1600 values ('1600'),
    subpartition SONO_V_1644 values ('1644'),
    subpartition SONO_V_1650 values ('1650'),
    subpartition SONO_V_1651 values ('1651'),
    subpartition SONO_V_1655 values ('1655'),
    subpartition SONO_V_1656 values ('1656'),
    subpartition SONO_V_1658 values ('1658'),
    subpartition SONO_V_1673 values ('1673'),
    subpartition SONO_V_1674 values ('1674'),
    subpartition SONO_V_1675 values ('1675'),
    subpartition SONO_V_1677 values ('1677'),
    subpartition SONO_V_1681 values ('1681'),
    subpartition SONO_V_1683 values ('1683'),
    subpartition SONO_V_1684 values ('1684'),
    subpartition SONO_V_1685 values ('1685'),
    subpartition SONO_V_1686 values ('1686'),
    subpartition SONO_V_1689 values ('1689'),
    subpartition SONO_V_1690 values ('1690'),
    subpartition SONO_V_16_DEAFULT values (default)
  ),
  partition REGION_V_17 values ('17')
  (
    subpartition SONO_V_1700 values ('1700'),
    subpartition SONO_V_1719 values ('1719'),
    subpartition SONO_V_1720 values ('1720'),
    subpartition SONO_V_1721 values ('1721'),
    subpartition SONO_V_1722 values ('1722'),
    subpartition SONO_V_17_DEAFULT values (default)
  ),
  partition REGION_V_18 values ('18')
  (
    subpartition SONO_V_1800 values ('1800'),
    subpartition SONO_V_1821 values ('1821'),
    subpartition SONO_V_1828 values ('1828'),
    subpartition SONO_V_1831 values ('1831'),
    subpartition SONO_V_1832 values ('1832'),
    subpartition SONO_V_1836 values ('1836'),
    subpartition SONO_V_1837 values ('1837'),
    subpartition SONO_V_1838 values ('1838'),
    subpartition SONO_V_1839 values ('1839'),
    subpartition SONO_V_1840 values ('1840'),
    subpartition SONO_V_1841 values ('1841'),
    subpartition SONO_V_18_DEAFULT values (default)
  ),
  partition REGION_V_19 values ('19')
  (
    subpartition SONO_V_1900 values ('1900'),
    subpartition SONO_V_1901 values ('1901'),
    subpartition SONO_V_1902 values ('1902'),
    subpartition SONO_V_1903 values ('1903'),
    subpartition SONO_V_19_DEAFULT values (default)
  ),
  partition REGION_V_20 values ('20')
  (
    subpartition SONO_V_2000 values ('2000'),
    subpartition SONO_V_2031 values ('2031'),
    subpartition SONO_V_2032 values ('2032'),
    subpartition SONO_V_2033 values ('2033'),
    subpartition SONO_V_2034 values ('2034'),
    subpartition SONO_V_2035 values ('2035'),
    subpartition SONO_V_2036 values ('2036'),
    subpartition SONO_V_20_DEAFULT values (default)
  ),
  partition REGION_V_21 values ('21')
  (
    subpartition SONO_V_2100 values ('2100'),
    subpartition SONO_V_2124 values ('2124'),
    subpartition SONO_V_2130 values ('2130'),
    subpartition SONO_V_2131 values ('2131'),
    subpartition SONO_V_2132 values ('2132'),
    subpartition SONO_V_2133 values ('2133'),
    subpartition SONO_V_2134 values ('2134'),
    subpartition SONO_V_2135 values ('2135'),
    subpartition SONO_V_2137 values ('2137'),
    subpartition SONO_V_2138 values ('2138'),
    subpartition SONO_V_21_DEAFULT values (default)
  ),
  partition REGION_V_22 values ('22')
  (
    subpartition SONO_V_2200 values ('2200'),
    subpartition SONO_V_2201 values ('2201'),
    subpartition SONO_V_2202 values ('2202'),
    subpartition SONO_V_2204 values ('2204'),
    subpartition SONO_V_2207 values ('2207'),
    subpartition SONO_V_2208 values ('2208'),
    subpartition SONO_V_2209 values ('2209'),
    subpartition SONO_V_2210 values ('2210'),
    subpartition SONO_V_2223 values ('2223'),
    subpartition SONO_V_2224 values ('2224'),
    subpartition SONO_V_2225 values ('2225'),
    subpartition SONO_V_2235 values ('2235'),
    subpartition SONO_V_2261 values ('2261'),
    subpartition SONO_V_22_DEAFULT values (default)
  ),
  partition REGION_V_23 values ('23')
  (
    subpartition SONO_V_2300 values ('2300'),
    subpartition SONO_V_2301 values ('2301'),
    subpartition SONO_V_2304 values ('2304'),
    subpartition SONO_V_2307 values ('2307'),
    subpartition SONO_V_2308 values ('2308'),
    subpartition SONO_V_2309 values ('2309'),
    subpartition SONO_V_2310 values ('2310'),
    subpartition SONO_V_2311 values ('2311'),
    subpartition SONO_V_2312 values ('2312'),
    subpartition SONO_V_2315 values ('2315'),
    subpartition SONO_V_2323 values ('2323'),
    subpartition SONO_V_2337 values ('2337'),
    subpartition SONO_V_2339 values ('2339'),
    subpartition SONO_V_2348 values ('2348'),
    subpartition SONO_V_2352 values ('2352'),
    subpartition SONO_V_2360 values ('2360'),
    subpartition SONO_V_2361 values ('2361'),
    subpartition SONO_V_2362 values ('2362'),
    subpartition SONO_V_2363 values ('2363'),
    subpartition SONO_V_2364 values ('2364'),
    subpartition SONO_V_2365 values ('2365'),
    subpartition SONO_V_2366 values ('2366'),
    subpartition SONO_V_2367 values ('2367'),
    subpartition SONO_V_2368 values ('2368'),
    subpartition SONO_V_2369 values ('2369'),
    subpartition SONO_V_2370 values ('2370'),
    subpartition SONO_V_2371 values ('2371'),
    subpartition SONO_V_2372 values ('2372'),
    subpartition SONO_V_2373 values ('2373'),
    subpartition SONO_V_2374 values ('2374'),
    subpartition SONO_V_23_DEAFULT values (default)
  ),
  partition REGION_V_24 values ('24')
  (
    subpartition SONO_V_2400 values ('2400'),
    subpartition SONO_V_2411 values ('2411'),
    subpartition SONO_V_2420 values ('2420'),
    subpartition SONO_V_2443 values ('2443'),
    subpartition SONO_V_2448 values ('2448'),
    subpartition SONO_V_2450 values ('2450'),
    subpartition SONO_V_2452 values ('2452'),
    subpartition SONO_V_2454 values ('2454'),
    subpartition SONO_V_2455 values ('2455'),
    subpartition SONO_V_2457 values ('2457'),
    subpartition SONO_V_2459 values ('2459'),
    subpartition SONO_V_2460 values ('2460'),
    subpartition SONO_V_2461 values ('2461'),
    subpartition SONO_V_2463 values ('2463'),
    subpartition SONO_V_2464 values ('2464'),
    subpartition SONO_V_2465 values ('2465'),
    subpartition SONO_V_2466 values ('2466'),
    subpartition SONO_V_2467 values ('2467'),
    subpartition SONO_V_2468 values ('2468'),
    subpartition SONO_V_24_DEAFULT values (default)
  ),
  partition REGION_V_25 values ('25')
  (
    subpartition SONO_V_2500 values ('2500'),
    subpartition SONO_V_2501 values ('2501'),
    subpartition SONO_V_2502 values ('2502'),
    subpartition SONO_V_2503 values ('2503'),
    subpartition SONO_V_2505 values ('2505'),
    subpartition SONO_V_2506 values ('2506'),
    subpartition SONO_V_2507 values ('2507'),
    subpartition SONO_V_2508 values ('2508'),
    subpartition SONO_V_2509 values ('2509'),
    subpartition SONO_V_2510 values ('2510'),
    subpartition SONO_V_2511 values ('2511'),
    subpartition SONO_V_2515 values ('2515'),
    subpartition SONO_V_2533 values ('2533'),
    subpartition SONO_V_2536 values ('2536'),
    subpartition SONO_V_2537 values ('2537'),
    subpartition SONO_V_2540 values ('2540'),
    subpartition SONO_V_2542 values ('2542'),
    subpartition SONO_V_2543 values ('2543'),
    subpartition SONO_V_25_DEAFULT values (default)
  ),
  partition REGION_V_26 values ('26')
  (
    subpartition SONO_V_2600 values ('2600'),
    subpartition SONO_V_2625 values ('2625'),
    subpartition SONO_V_2628 values ('2628'),
    subpartition SONO_V_2632 values ('2632'),
    subpartition SONO_V_2634 values ('2634'),
    subpartition SONO_V_2635 values ('2635'),
    subpartition SONO_V_2641 values ('2641'),
    subpartition SONO_V_2643 values ('2643'),
    subpartition SONO_V_2644 values ('2644'),
    subpartition SONO_V_2645 values ('2645'),
    subpartition SONO_V_2646 values ('2646'),
    subpartition SONO_V_2648 values ('2648'),
    subpartition SONO_V_2649 values ('2649'),
    subpartition SONO_V_2650 values ('2650'),
    subpartition SONO_V_2651 values ('2651'),
    subpartition SONO_V_26_DEAFULT values (default)
  ),
  partition REGION_V_27 values ('27')
  (
    subpartition SONO_V_2700 values ('2700'),
    subpartition SONO_V_2703 values ('2703'),
    subpartition SONO_V_2705 values ('2705'),
    subpartition SONO_V_2709 values ('2709'),
    subpartition SONO_V_2720 values ('2720'),
    subpartition SONO_V_2721 values ('2721'),
    subpartition SONO_V_2722 values ('2722'),
    subpartition SONO_V_2723 values ('2723'),
    subpartition SONO_V_2724 values ('2724'),
    subpartition SONO_V_2728 values ('2728'),
    subpartition SONO_V_27_DEAFULT values (default)
  ),
  partition REGION_V_28 values ('28')
  (
    subpartition SONO_V_2800 values ('2800'),
    subpartition SONO_V_2801 values ('2801'),
    subpartition SONO_V_2804 values ('2804'),
    subpartition SONO_V_2807 values ('2807'),
    subpartition SONO_V_2808 values ('2808'),
    subpartition SONO_V_2813 values ('2813'),
    subpartition SONO_V_2815 values ('2815'),
    subpartition SONO_V_2827 values ('2827'),
    subpartition SONO_V_28_DEAFULT values (default)
  ),
  partition REGION_V_29 values ('29')
  (
    subpartition SONO_V_2900 values ('2900'),
    subpartition SONO_V_2901 values ('2901'),
    subpartition SONO_V_2903 values ('2903'),
    subpartition SONO_V_2904 values ('2904'),
    subpartition SONO_V_2907 values ('2907'),
    subpartition SONO_V_2918 values ('2918'),
    subpartition SONO_V_2920 values ('2920'),
    subpartition SONO_V_2931 values ('2931'),
    subpartition SONO_V_2932 values ('2932'),
    subpartition SONO_V_2983 values ('2983'),
    subpartition SONO_V_29_DEAFULT values (default)
  ),
  partition REGION_V_30 values ('30')
  (
    subpartition SONO_V_3000 values ('3000'),
    subpartition SONO_V_3015 values ('3015'),
    subpartition SONO_V_3019 values ('3019'),
    subpartition SONO_V_3022 values ('3022'),
    subpartition SONO_V_3023 values ('3023'),
    subpartition SONO_V_3025 values ('3025'),
    subpartition SONO_V_30_DEAFULT values (default)
  ),
  partition REGION_V_31 values ('31')
  (
    subpartition SONO_V_3100 values ('3100'),
    subpartition SONO_V_3114 values ('3114'),
    subpartition SONO_V_3116 values ('3116'),
    subpartition SONO_V_3120 values ('3120'),
    subpartition SONO_V_3122 values ('3122'),
    subpartition SONO_V_3123 values ('3123'),
    subpartition SONO_V_3126 values ('3126'),
    subpartition SONO_V_3127 values ('3127'),
    subpartition SONO_V_3128 values ('3128'),
    subpartition SONO_V_3130 values ('3130'),
    subpartition SONO_V_31_DEAFULT values (default)
  ),
  partition REGION_V_32 values ('32')
  (
    subpartition SONO_V_3200 values ('3200'),
    subpartition SONO_V_3241 values ('3241'),
    subpartition SONO_V_3245 values ('3245'),
    subpartition SONO_V_3252 values ('3252'),
    subpartition SONO_V_3253 values ('3253'),
    subpartition SONO_V_3256 values ('3256'),
    subpartition SONO_V_3257 values ('3257'),
    subpartition SONO_V_32_DEAFULT values (default)
  ),
  partition REGION_V_33 values ('33')
  (
    subpartition SONO_V_3300 values ('3300'),
    subpartition SONO_V_3302 values ('3302'),
    subpartition SONO_V_3304 values ('3304'),
    subpartition SONO_V_3316 values ('3316'),
    subpartition SONO_V_3326 values ('3326'),
    subpartition SONO_V_3327 values ('3327'),
    subpartition SONO_V_3328 values ('3328'),
    subpartition SONO_V_3332 values ('3332'),
    subpartition SONO_V_3334 values ('3334'),
    subpartition SONO_V_3339 values ('3339'),
    subpartition SONO_V_3340 values ('3340'),
    subpartition SONO_V_33_DEAFULT values (default)
  ),
  partition REGION_V_34 values ('34')
  (
    subpartition SONO_V_3400 values ('3400'),
    subpartition SONO_V_3435 values ('3435'),
    subpartition SONO_V_3443 values ('3443'),
    subpartition SONO_V_3444 values ('3444'),
    subpartition SONO_V_3452 values ('3452'),
    subpartition SONO_V_3453 values ('3453'),
    subpartition SONO_V_3454 values ('3454'),
    subpartition SONO_V_3455 values ('3455'),
    subpartition SONO_V_3456 values ('3456'),
    subpartition SONO_V_3457 values ('3457'),
    subpartition SONO_V_3458 values ('3458'),
    subpartition SONO_V_3459 values ('3459'),
    subpartition SONO_V_3460 values ('3460'),
    subpartition SONO_V_3461 values ('3461'),
    subpartition SONO_V_34_DEAFULT values (default)
  ),
  partition REGION_V_35 values ('35')
  (
    subpartition SONO_V_3500 values ('3500'),
    subpartition SONO_V_3525 values ('3525'),
    subpartition SONO_V_3528 values ('3528'),
    subpartition SONO_V_3529 values ('3529'),
    subpartition SONO_V_3532 values ('3532'),
    subpartition SONO_V_3533 values ('3533'),
    subpartition SONO_V_3535 values ('3535'),
    subpartition SONO_V_3536 values ('3536'),
    subpartition SONO_V_3537 values ('3537'),
    subpartition SONO_V_3538 values ('3538'),
    subpartition SONO_V_3539 values ('3539'),
    subpartition SONO_V_35_DEAFULT values (default)
  ),
  partition REGION_V_36 values ('36')
  (
    subpartition SONO_V_3600 values ('3600'),
    subpartition SONO_V_3601 values ('3601'),
    subpartition SONO_V_3604 values ('3604'),
    subpartition SONO_V_3610 values ('3610'),
    subpartition SONO_V_3620 values ('3620'),
    subpartition SONO_V_3627 values ('3627'),
    subpartition SONO_V_3628 values ('3628'),
    subpartition SONO_V_3629 values ('3629'),
    subpartition SONO_V_3652 values ('3652'),
    subpartition SONO_V_3661 values ('3661'),
    subpartition SONO_V_3662 values ('3662'),
    subpartition SONO_V_3663 values ('3663'),
    subpartition SONO_V_3664 values ('3664'),
    subpartition SONO_V_3665 values ('3665'),
    subpartition SONO_V_3666 values ('3666'),
    subpartition SONO_V_3667 values ('3667'),
    subpartition SONO_V_3668 values ('3668'),
    subpartition SONO_V_36_DEAFULT values (default)
  ),
  partition REGION_V_37 values ('37')
  (
    subpartition SONO_V_3700 values ('3700'),
    subpartition SONO_V_3701 values ('3701'),
    subpartition SONO_V_3702 values ('3702'),
    subpartition SONO_V_3703 values ('3703'),
    subpartition SONO_V_3704 values ('3704'),
    subpartition SONO_V_3705 values ('3705'),
    subpartition SONO_V_3706 values ('3706'),
    subpartition SONO_V_3711 values ('3711'),
    subpartition SONO_V_3720 values ('3720'),
    subpartition SONO_V_37_DEAFULT values (default)
  ),
  partition REGION_V_38 values ('38')
  (
    subpartition SONO_V_3800 values ('3800'),
    subpartition SONO_V_3801 values ('3801'),
    subpartition SONO_V_3802 values ('3802'),
    subpartition SONO_V_3804 values ('3804'),
    subpartition SONO_V_3805 values ('3805'),
    subpartition SONO_V_3808 values ('3808'),
    subpartition SONO_V_3810 values ('3810'),
    subpartition SONO_V_3811 values ('3811'),
    subpartition SONO_V_3812 values ('3812'),
    subpartition SONO_V_3814 values ('3814'),
    subpartition SONO_V_3816 values ('3816'),
    subpartition SONO_V_3817 values ('3817'),
    subpartition SONO_V_3818 values ('3818'),
    subpartition SONO_V_3827 values ('3827'),
    subpartition SONO_V_3849 values ('3849'),
    subpartition SONO_V_3850 values ('3850'),
    subpartition SONO_V_3851 values ('3851'),
    subpartition SONO_V_38_DEAFULT values (default)
  ),
  partition REGION_V_39 values ('39')
  (
    subpartition SONO_V_3900 values ('3900'),
    subpartition SONO_V_3905 values ('3905'),
    subpartition SONO_V_3906 values ('3906'),
    subpartition SONO_V_3914 values ('3914'),
    subpartition SONO_V_3917 values ('3917'),
    subpartition SONO_V_3925 values ('3925'),
    subpartition SONO_V_3926 values ('3926'),
    subpartition SONO_V_39_DEAFULT values (default)
  ),
  partition REGION_V_40 values ('40')
  (
    subpartition SONO_V_4000 values ('4000'),
    subpartition SONO_V_4001 values ('4001'),
    subpartition SONO_V_4004 values ('4004'),
    subpartition SONO_V_4011 values ('4011'),
    subpartition SONO_V_4023 values ('4023'),
    subpartition SONO_V_4024 values ('4024'),
    subpartition SONO_V_4025 values ('4025'),
    subpartition SONO_V_4027 values ('4027'),
    subpartition SONO_V_4028 values ('4028'),
    subpartition SONO_V_4029 values ('4029'),
    subpartition SONO_V_40_DEAFULT values (default)
  ),
  partition REGION_V_41 values ('41')
  (
    subpartition SONO_V_4100 values ('4100'),
    subpartition SONO_V_4101 values ('4101'),
    subpartition SONO_V_4177 values ('4177'),
    subpartition SONO_V_41_DEAFULT values (default)
  ),
  partition REGION_V_42 values ('42')
  (
    subpartition SONO_V_4200 values ('4200'),
    subpartition SONO_V_4202 values ('4202'),
    subpartition SONO_V_4205 values ('4205'),
    subpartition SONO_V_4212 values ('4212'),
    subpartition SONO_V_4213 values ('4213'),
    subpartition SONO_V_4214 values ('4214'),
    subpartition SONO_V_4216 values ('4216'),
    subpartition SONO_V_4217 values ('4217'),
    subpartition SONO_V_4222 values ('4222'),
    subpartition SONO_V_4223 values ('4223'),
    subpartition SONO_V_4230 values ('4230'),
    subpartition SONO_V_4246 values ('4246'),
    subpartition SONO_V_4249 values ('4249'),
    subpartition SONO_V_4250 values ('4250'),
    subpartition SONO_V_4252 values ('4252'),
    subpartition SONO_V_4253 values ('4253'),
    subpartition SONO_V_42_DEAFULT values (default)
  ),
  partition REGION_V_43 values ('43')
  (
    subpartition SONO_V_4300 values ('4300'),
    subpartition SONO_V_4303 values ('4303'),
    subpartition SONO_V_4307 values ('4307'),
    subpartition SONO_V_4312 values ('4312'),
    subpartition SONO_V_4313 values ('4313'),
    subpartition SONO_V_4316 values ('4316'),
    subpartition SONO_V_4321 values ('4321'),
    subpartition SONO_V_4322 values ('4322'),
    subpartition SONO_V_4329 values ('4329'),
    subpartition SONO_V_4330 values ('4330'),
    subpartition SONO_V_4334 values ('4334'),
    subpartition SONO_V_4339 values ('4339'),
    subpartition SONO_V_4345 values ('4345'),
    subpartition SONO_V_43_DEAFULT values (default)
  ),
  partition REGION_V_44 values ('44')
  (
    subpartition SONO_V_4400 values ('4400'),
    subpartition SONO_V_4401 values ('4401'),
    subpartition SONO_V_4433 values ('4433'),
    subpartition SONO_V_4434 values ('4434'),
    subpartition SONO_V_4436 values ('4436'),
    subpartition SONO_V_4437 values ('4437'),
    subpartition SONO_V_44_DEAFULT values (default)
  ),
  partition REGION_V_45 values ('45')
  (
    subpartition SONO_V_4500 values ('4500'),
    subpartition SONO_V_4501 values ('4501'),
    subpartition SONO_V_4502 values ('4502'),
    subpartition SONO_V_4506 values ('4506'),
    subpartition SONO_V_4508 values ('4508'),
    subpartition SONO_V_4510 values ('4510'),
    subpartition SONO_V_4512 values ('4512'),
    subpartition SONO_V_4524 values ('4524'),
    subpartition SONO_V_4526 values ('4526'),
    subpartition SONO_V_45_DEAFULT values (default)
  ),
  partition REGION_V_46 values ('46')
  (
    subpartition SONO_V_4600 values ('4600'),
    subpartition SONO_V_4611 values ('4611'),
    subpartition SONO_V_4613 values ('4613'),
    subpartition SONO_V_4614 values ('4614'),
    subpartition SONO_V_4619 values ('4619'),
    subpartition SONO_V_4620 values ('4620'),
    subpartition SONO_V_4623 values ('4623'),
    subpartition SONO_V_4628 values ('4628'),
    subpartition SONO_V_4632 values ('4632'),
    subpartition SONO_V_4633 values ('4633'),
    subpartition SONO_V_46_DEAFULT values (default)
  ),
  partition REGION_V_47 values ('47')
  (
    subpartition SONO_V_4700 values ('4700'),
    subpartition SONO_V_4702 values ('4702'),
    subpartition SONO_V_4703 values ('4703'),
    subpartition SONO_V_4704 values ('4704'),
    subpartition SONO_V_4705 values ('4705'),
    subpartition SONO_V_4706 values ('4706'),
    subpartition SONO_V_4707 values ('4707'),
    subpartition SONO_V_4710 values ('4710'),
    subpartition SONO_V_4711 values ('4711'),
    subpartition SONO_V_4712 values ('4712'),
    subpartition SONO_V_4715 values ('4715'),
    subpartition SONO_V_4716 values ('4716'),
    subpartition SONO_V_4725 values ('4725'),
    subpartition SONO_V_4726 values ('4726'),
    subpartition SONO_V_4727 values ('4727'),
    subpartition SONO_V_47_DEAFULT values (default)
  ),
  partition REGION_V_48 values ('48')
  (
    subpartition SONO_V_4800 values ('4800'),
    subpartition SONO_V_4802 values ('4802'),
    subpartition SONO_V_4811 values ('4811'),
    subpartition SONO_V_4813 values ('4813'),
    subpartition SONO_V_4816 values ('4816'),
    subpartition SONO_V_4822 values ('4822'),
    subpartition SONO_V_4824 values ('4824'),
    subpartition SONO_V_4825 values ('4825'),
    subpartition SONO_V_4827 values ('4827'),
    subpartition SONO_V_4828 values ('4828'),
    subpartition SONO_V_48_DEAFULT values (default)
  ),
  partition REGION_V_49 values ('49')
  (
    subpartition SONO_V_4900 values ('4900'),
    subpartition SONO_V_4910 values ('4910'),
    subpartition SONO_V_4911 values ('4911'),
    subpartition SONO_V_4912 values ('4912'),
    subpartition SONO_V_49_DEAFULT values (default)
  ),
  partition REGION_V_50 values ('50')
  (
    subpartition SONO_V_5000 values ('5000'),
    subpartition SONO_V_5001 values ('5001'),
    subpartition SONO_V_5003 values ('5003'),
    subpartition SONO_V_5004 values ('5004'),
    subpartition SONO_V_5005 values ('5005'),
    subpartition SONO_V_5007 values ('5007'),
    subpartition SONO_V_5009 values ('5009'),
    subpartition SONO_V_5010 values ('5010'),
    subpartition SONO_V_5011 values ('5011'),
    subpartition SONO_V_5012 values ('5012'),
    subpartition SONO_V_5017 values ('5017'),
    subpartition SONO_V_5018 values ('5018'),
    subpartition SONO_V_5019 values ('5019'),
    subpartition SONO_V_5020 values ('5020'),
    subpartition SONO_V_5022 values ('5022'),
    subpartition SONO_V_5024 values ('5024'),
    subpartition SONO_V_5027 values ('5027'),
    subpartition SONO_V_5029 values ('5029'),
    subpartition SONO_V_5030 values ('5030'),
    subpartition SONO_V_5031 values ('5031'),
    subpartition SONO_V_5032 values ('5032'),
    subpartition SONO_V_5034 values ('5034'),
    subpartition SONO_V_5035 values ('5035'),
    subpartition SONO_V_5038 values ('5038'),
    subpartition SONO_V_5040 values ('5040'),
    subpartition SONO_V_5042 values ('5042'),
    subpartition SONO_V_5043 values ('5043'),
    subpartition SONO_V_5044 values ('5044'),
    subpartition SONO_V_5045 values ('5045'),
    subpartition SONO_V_5047 values ('5047'),
    subpartition SONO_V_5048 values ('5048'),
    subpartition SONO_V_5049 values ('5049'),
    subpartition SONO_V_5050 values ('5050'),
    subpartition SONO_V_5053 values ('5053'),
    subpartition SONO_V_5072 values ('5072'),
    subpartition SONO_V_5074 values ('5074'),
    subpartition SONO_V_5075 values ('5075'),
    subpartition SONO_V_5099 values ('5099'),
    subpartition SONO_V_50_DEAFULT values (default)
  ),
  partition REGION_V_51 values ('51')
  (
    subpartition SONO_V_5100 values ('5100'),
    subpartition SONO_V_5102 values ('5102'),
    subpartition SONO_V_5105 values ('5105'),
    subpartition SONO_V_5107 values ('5107'),
    subpartition SONO_V_5108 values ('5108'),
    subpartition SONO_V_5110 values ('5110'),
    subpartition SONO_V_5118 values ('5118'),
    subpartition SONO_V_5190 values ('5190'),
    subpartition SONO_V_5199 values ('5199'),
    subpartition SONO_V_51_DEAFULT values (default)
  ),
  partition REGION_V_52 values ('52')
  (
    subpartition SONO_V_5200 values ('5200'),
    subpartition SONO_V_5222 values ('5222'),
    subpartition SONO_V_5228 values ('5228'),
    subpartition SONO_V_5229 values ('5229'),
    subpartition SONO_V_5235 values ('5235'),
    subpartition SONO_V_5243 values ('5243'),
    subpartition SONO_V_5246 values ('5246'),
    subpartition SONO_V_5247 values ('5247'),
    subpartition SONO_V_5248 values ('5248'),
    subpartition SONO_V_5249 values ('5249'),
    subpartition SONO_V_5250 values ('5250'),
    subpartition SONO_V_5252 values ('5252'),
    subpartition SONO_V_5253 values ('5253'),
    subpartition SONO_V_5254 values ('5254'),
    subpartition SONO_V_5256 values ('5256'),
    subpartition SONO_V_5257 values ('5257'),
    subpartition SONO_V_5258 values ('5258'),
    subpartition SONO_V_5259 values ('5259'),
    subpartition SONO_V_5260 values ('5260'),
    subpartition SONO_V_5261 values ('5261'),
    subpartition SONO_V_5262 values ('5262'),
    subpartition SONO_V_5263 values ('5263'),
    subpartition SONO_V_5270 values ('5270'),
    subpartition SONO_V_52_DEAFULT values (default)
  ),
  partition REGION_V_53 values ('53')
  (
    subpartition SONO_V_5300 values ('5300'),
    subpartition SONO_V_5321 values ('5321'),
    subpartition SONO_V_5331 values ('5331'),
    subpartition SONO_V_5332 values ('5332'),
    subpartition SONO_V_5336 values ('5336'),
    subpartition SONO_V_53_DEAFULT values (default)
  ),
  partition REGION_V_54 values ('54')
  (
    subpartition SONO_V_5400 values ('5400'),
    subpartition SONO_V_5401 values ('5401'),
    subpartition SONO_V_5402 values ('5402'),
    subpartition SONO_V_5403 values ('5403'),
    subpartition SONO_V_5404 values ('5404'),
    subpartition SONO_V_5405 values ('5405'),
    subpartition SONO_V_5406 values ('5406'),
    subpartition SONO_V_5407 values ('5407'),
    subpartition SONO_V_5410 values ('5410'),
    subpartition SONO_V_5456 values ('5456'),
    subpartition SONO_V_5460 values ('5460'),
    subpartition SONO_V_5473 values ('5473'),
    subpartition SONO_V_5474 values ('5474'),
    subpartition SONO_V_5475 values ('5475'),
    subpartition SONO_V_5476 values ('5476'),
    subpartition SONO_V_5483 values ('5483'),
    subpartition SONO_V_5485 values ('5485'),
    subpartition SONO_V_54_DEAFULT values (default)
  ),
  partition REGION_V_55 values ('55')
  (
    subpartition SONO_V_5500 values ('5500'),
    subpartition SONO_V_5501 values ('5501'),
    subpartition SONO_V_5503 values ('5503'),
    subpartition SONO_V_5504 values ('5504'),
    subpartition SONO_V_5505 values ('5505'),
    subpartition SONO_V_5506 values ('5506'),
    subpartition SONO_V_5507 values ('5507'),
    subpartition SONO_V_5509 values ('5509'),
    subpartition SONO_V_5514 values ('5514'),
    subpartition SONO_V_5515 values ('5515'),
    subpartition SONO_V_5535 values ('5535'),
    subpartition SONO_V_5542 values ('5542'),
    subpartition SONO_V_5543 values ('5543'),
    subpartition SONO_V_55_DEAFULT values (default)
  ),
  partition REGION_V_56 values ('56')
  (
    subpartition SONO_V_5600 values ('5600'),
    subpartition SONO_V_5601 values ('5601'),
    subpartition SONO_V_5602 values ('5602'),
    subpartition SONO_V_5603 values ('5603'),
    subpartition SONO_V_5607 values ('5607'),
    subpartition SONO_V_5609 values ('5609'),
    subpartition SONO_V_5610 values ('5610'),
    subpartition SONO_V_5611 values ('5611'),
    subpartition SONO_V_5612 values ('5612'),
    subpartition SONO_V_5613 values ('5613'),
    subpartition SONO_V_5614 values ('5614'),
    subpartition SONO_V_5617 values ('5617'),
    subpartition SONO_V_5635 values ('5635'),
    subpartition SONO_V_5636 values ('5636'),
    subpartition SONO_V_5638 values ('5638'),
    subpartition SONO_V_5646 values ('5646'),
    subpartition SONO_V_5658 values ('5658'),
    subpartition SONO_V_56_DEAFULT values (default)
  ),
  partition REGION_V_57 values ('57')
  (
    subpartition SONO_V_5700 values ('5700'),
    subpartition SONO_V_5740 values ('5740'),
    subpartition SONO_V_5743 values ('5743'),
    subpartition SONO_V_5744 values ('5744'),
    subpartition SONO_V_5745 values ('5745'),
    subpartition SONO_V_5746 values ('5746'),
    subpartition SONO_V_5748 values ('5748'),
    subpartition SONO_V_5749 values ('5749'),
    subpartition SONO_V_57_DEAFULT values (default)
  ),
  partition REGION_V_58 values ('58')
  (
    subpartition SONO_V_5800 values ('5800'),
    subpartition SONO_V_5802 values ('5802'),
    subpartition SONO_V_5803 values ('5803'),
    subpartition SONO_V_5805 values ('5805'),
    subpartition SONO_V_5809 values ('5809'),
    subpartition SONO_V_5826 values ('5826'),
    subpartition SONO_V_5827 values ('5827'),
    subpartition SONO_V_5834 values ('5834'),
    subpartition SONO_V_5835 values ('5835'),
    subpartition SONO_V_5836 values ('5836'),
    subpartition SONO_V_5837 values ('5837'),
    subpartition SONO_V_5838 values ('5838'),
    subpartition SONO_V_58_DEAFULT values (default)
  ),
  partition REGION_V_59 values ('59')
  (
    subpartition SONO_V_5900 values ('5900'),
    subpartition SONO_V_5901 values ('5901'),
    subpartition SONO_V_5902 values ('5902'),
    subpartition SONO_V_5903 values ('5903'),
    subpartition SONO_V_5904 values ('5904'),
    subpartition SONO_V_5905 values ('5905'),
    subpartition SONO_V_5906 values ('5906'),
    subpartition SONO_V_5907 values ('5907'),
    subpartition SONO_V_5908 values ('5908'),
    subpartition SONO_V_5911 values ('5911'),
    subpartition SONO_V_5914 values ('5914'),
    subpartition SONO_V_5916 values ('5916'),
    subpartition SONO_V_5917 values ('5917'),
    subpartition SONO_V_5918 values ('5918'),
    subpartition SONO_V_5919 values ('5919'),
    subpartition SONO_V_5920 values ('5920'),
    subpartition SONO_V_5921 values ('5921'),
    subpartition SONO_V_5933 values ('5933'),
    subpartition SONO_V_5944 values ('5944'),
    subpartition SONO_V_5947 values ('5947'),
    subpartition SONO_V_5948 values ('5948'),
    subpartition SONO_V_5951 values ('5951'),
    subpartition SONO_V_5957 values ('5957'),
    subpartition SONO_V_5958 values ('5958'),
    subpartition SONO_V_5981 values ('5981'),
    subpartition SONO_V_59_DEAFULT values (default)
  ),
  partition REGION_V_60 values ('60')
  (
    subpartition SONO_V_6000 values ('6000'),
    subpartition SONO_V_6009 values ('6009'),
    subpartition SONO_V_6025 values ('6025'),
    subpartition SONO_V_6027 values ('6027'),
    subpartition SONO_V_6030 values ('6030'),
    subpartition SONO_V_6031 values ('6031'),
    subpartition SONO_V_6032 values ('6032'),
    subpartition SONO_V_60_DEAFULT values (default)
  ),
  partition REGION_V_61 values ('61')
  (
    subpartition SONO_V_6100 values ('6100'),
    subpartition SONO_V_6152 values ('6152'),
    subpartition SONO_V_6154 values ('6154'),
    subpartition SONO_V_6164 values ('6164'),
    subpartition SONO_V_6165 values ('6165'),
    subpartition SONO_V_6171 values ('6171'),
    subpartition SONO_V_6173 values ('6173'),
    subpartition SONO_V_6174 values ('6174'),
    subpartition SONO_V_6179 values ('6179'),
    subpartition SONO_V_6181 values ('6181'),
    subpartition SONO_V_6182 values ('6182'),
    subpartition SONO_V_6183 values ('6183'),
    subpartition SONO_V_6186 values ('6186'),
    subpartition SONO_V_6188 values ('6188'),
    subpartition SONO_V_6191 values ('6191'),
    subpartition SONO_V_6192 values ('6192'),
    subpartition SONO_V_6193 values ('6193'),
    subpartition SONO_V_6194 values ('6194'),
    subpartition SONO_V_6195 values ('6195'),
    subpartition SONO_V_6196 values ('6196'),
    subpartition SONO_V_61_DEAFULT values (default)
  ),
  partition REGION_V_62 values ('62')
  (
    subpartition SONO_V_6200 values ('6200'),
    subpartition SONO_V_6214 values ('6214'),
    subpartition SONO_V_6215 values ('6215'),
    subpartition SONO_V_6219 values ('6219'),
    subpartition SONO_V_6225 values ('6225'),
    subpartition SONO_V_6226 values ('6226'),
    subpartition SONO_V_6229 values ('6229'),
    subpartition SONO_V_6230 values ('6230'),
    subpartition SONO_V_6232 values ('6232'),
    subpartition SONO_V_6234 values ('6234'),
    subpartition SONO_V_62_DEAFULT values (default)
  ),
  partition REGION_V_63 values ('63')
  (
    subpartition SONO_V_6300 values ('6300'),
    subpartition SONO_V_6310 values ('6310'),
    subpartition SONO_V_6311 values ('6311'),
    subpartition SONO_V_6312 values ('6312'),
    subpartition SONO_V_6313 values ('6313'),
    subpartition SONO_V_6315 values ('6315'),
    subpartition SONO_V_6316 values ('6316'),
    subpartition SONO_V_6317 values ('6317'),
    subpartition SONO_V_6318 values ('6318'),
    subpartition SONO_V_6319 values ('6319'),
    subpartition SONO_V_6320 values ('6320'),
    subpartition SONO_V_6324 values ('6324'),
    subpartition SONO_V_6325 values ('6325'),
    subpartition SONO_V_6330 values ('6330'),
    subpartition SONO_V_6350 values ('6350'),
    subpartition SONO_V_6372 values ('6372'),
    subpartition SONO_V_6375 values ('6375'),
    subpartition SONO_V_6376 values ('6376'),
    subpartition SONO_V_6377 values ('6377'),
    subpartition SONO_V_6381 values ('6381'),
    subpartition SONO_V_6382 values ('6382'),
    subpartition SONO_V_63_DEAFULT values (default)
  ),
  partition REGION_V_64 values ('64')
  (
    subpartition SONO_V_6400 values ('6400'),
    subpartition SONO_V_6413 values ('6413'),
    subpartition SONO_V_6432 values ('6432'),
    subpartition SONO_V_6438 values ('6438'),
    subpartition SONO_V_6439 values ('6439'),
    subpartition SONO_V_6440 values ('6440'),
    subpartition SONO_V_6441 values ('6441'),
    subpartition SONO_V_6444 values ('6444'),
    subpartition SONO_V_6445 values ('6445'),
    subpartition SONO_V_6446 values ('6446'),
    subpartition SONO_V_6447 values ('6447'),
    subpartition SONO_V_6449 values ('6449'),
    subpartition SONO_V_6450 values ('6450'),
    subpartition SONO_V_6451 values ('6451'),
    subpartition SONO_V_6453 values ('6453'),
    subpartition SONO_V_6454 values ('6454'),
    subpartition SONO_V_6455 values ('6455'),
    subpartition SONO_V_64_DEAFULT values (default)
  ),
  partition REGION_V_65 values ('65')
  (
    subpartition SONO_V_6500 values ('6500'),
    subpartition SONO_V_6501 values ('6501'),
    subpartition SONO_V_6504 values ('6504'),
    subpartition SONO_V_6507 values ('6507'),
    subpartition SONO_V_6509 values ('6509'),
    subpartition SONO_V_6517 values ('6517'),
    subpartition SONO_V_65_DEAFULT values (default)
  ),
  partition REGION_V_66 values ('66')
  (
    subpartition SONO_V_6600 values ('6600'),
    subpartition SONO_V_6608 values ('6608'),
    subpartition SONO_V_6612 values ('6612'),
    subpartition SONO_V_6617 values ('6617'),
    subpartition SONO_V_6619 values ('6619'),
    subpartition SONO_V_6623 values ('6623'),
    subpartition SONO_V_6633 values ('6633'),
    subpartition SONO_V_6658 values ('6658'),
    subpartition SONO_V_6670 values ('6670'),
    subpartition SONO_V_6671 values ('6671'),
    subpartition SONO_V_6676 values ('6676'),
    subpartition SONO_V_6677 values ('6677'),
    subpartition SONO_V_6678 values ('6678'),
    subpartition SONO_V_6679 values ('6679'),
    subpartition SONO_V_6680 values ('6680'),
    subpartition SONO_V_6681 values ('6681'),
    subpartition SONO_V_6682 values ('6682'),
    subpartition SONO_V_6683 values ('6683'),
    subpartition SONO_V_6684 values ('6684'),
    subpartition SONO_V_6685 values ('6685'),
    subpartition SONO_V_6686 values ('6686'),
    subpartition SONO_V_66_DEAFULT values (default)
  ),
  partition REGION_V_67 values ('67')
  (
    subpartition SONO_V_6700 values ('6700'),
    subpartition SONO_V_6712 values ('6712'),
    subpartition SONO_V_6713 values ('6713'),
    subpartition SONO_V_6714 values ('6714'),
    subpartition SONO_V_6722 values ('6722'),
    subpartition SONO_V_6725 values ('6725'),
    subpartition SONO_V_6726 values ('6726'),
    subpartition SONO_V_6727 values ('6727'),
    subpartition SONO_V_6732 values ('6732'),
    subpartition SONO_V_6733 values ('6733'),
    subpartition SONO_V_67_DEAFULT values (default)
  ),
  partition REGION_V_68 values ('68')
  (
    subpartition SONO_V_6800 values ('6800'),
    subpartition SONO_V_6809 values ('6809'),
    subpartition SONO_V_6820 values ('6820'),
    subpartition SONO_V_6827 values ('6827'),
    subpartition SONO_V_6828 values ('6828'),
    subpartition SONO_V_6829 values ('6829'),
    subpartition SONO_V_68_DEAFULT values (default)
  ),
  partition REGION_V_69 values ('69')
  (
    subpartition SONO_V_6900 values ('6900'),
    subpartition SONO_V_6906 values ('6906'),
    subpartition SONO_V_6908 values ('6908'),
    subpartition SONO_V_6910 values ('6910'),
    subpartition SONO_V_6912 values ('6912'),
    subpartition SONO_V_6913 values ('6913'),
    subpartition SONO_V_6914 values ('6914'),
    subpartition SONO_V_6915 values ('6915'),
    subpartition SONO_V_6949 values ('6949'),
    subpartition SONO_V_6950 values ('6950'),
    subpartition SONO_V_6952 values ('6952'),
    subpartition SONO_V_69_DEAFULT values (default)
  ),
  partition REGION_V_70 values ('70')
  (
    subpartition SONO_V_7000 values ('7000'),
    subpartition SONO_V_7014 values ('7014'),
    subpartition SONO_V_7017 values ('7017'),
    subpartition SONO_V_7022 values ('7022'),
    subpartition SONO_V_7024 values ('7024'),
    subpartition SONO_V_7025 values ('7025'),
    subpartition SONO_V_7026 values ('7026'),
    subpartition SONO_V_7028 values ('7028'),
    subpartition SONO_V_7030 values ('7030'),
    subpartition SONO_V_70_DEAFULT values (default)
  ),
  partition REGION_V_71 values ('71')
  (
    subpartition SONO_V_7100 values ('7100'),
    subpartition SONO_V_7101 values ('7101'),
    subpartition SONO_V_7104 values ('7104'),
    subpartition SONO_V_7107 values ('7107'),
    subpartition SONO_V_7148 values ('7148'),
    subpartition SONO_V_7150 values ('7150'),
    subpartition SONO_V_7151 values ('7151'),
    subpartition SONO_V_7153 values ('7153'),
    subpartition SONO_V_7154 values ('7154'),
    subpartition SONO_V_7155 values ('7155'),
    subpartition SONO_V_71_DEAFULT values (default)
  ),
  partition REGION_V_72 values ('72')
  (
    subpartition SONO_V_7200 values ('7200'),
    subpartition SONO_V_7202 values ('7202'),
    subpartition SONO_V_7203 values ('7203'),
    subpartition SONO_V_7204 values ('7204'),
    subpartition SONO_V_7205 values ('7205'),
    subpartition SONO_V_7206 values ('7206'),
    subpartition SONO_V_7207 values ('7207'),
    subpartition SONO_V_7220 values ('7220'),
    subpartition SONO_V_7224 values ('7224'),
    subpartition SONO_V_7230 values ('7230'),
    subpartition SONO_V_7232 values ('7232'),
    subpartition SONO_V_72_DEAFULT values (default)
  ),
  partition REGION_V_73 values ('73')
  (
    subpartition SONO_V_7300 values ('7300'),
    subpartition SONO_V_7303 values ('7303'),
    subpartition SONO_V_7309 values ('7309'),
    subpartition SONO_V_7313 values ('7313'),
    subpartition SONO_V_7321 values ('7321'),
    subpartition SONO_V_7325 values ('7325'),
    subpartition SONO_V_7326 values ('7326'),
    subpartition SONO_V_7327 values ('7327'),
    subpartition SONO_V_7328 values ('7328'),
    subpartition SONO_V_7329 values ('7329'),
    subpartition SONO_V_73_DEAFULT values (default)
  ),
  partition REGION_V_74 values ('74')
  (
    subpartition SONO_V_7400 values ('7400'),
    subpartition SONO_V_7404 values ('7404'),
    subpartition SONO_V_7413 values ('7413'),
    subpartition SONO_V_7415 values ('7415'),
    subpartition SONO_V_7424 values ('7424'),
    subpartition SONO_V_7430 values ('7430'),
    subpartition SONO_V_7447 values ('7447'),
    subpartition SONO_V_7448 values ('7448'),
    subpartition SONO_V_7449 values ('7449'),
    subpartition SONO_V_7451 values ('7451'),
    subpartition SONO_V_7452 values ('7452'),
    subpartition SONO_V_7453 values ('7453'),
    subpartition SONO_V_7454 values ('7454'),
    subpartition SONO_V_7455 values ('7455'),
    subpartition SONO_V_7456 values ('7456'),
    subpartition SONO_V_7457 values ('7457'),
    subpartition SONO_V_7458 values ('7458'),
    subpartition SONO_V_7459 values ('7459'),
    subpartition SONO_V_7460 values ('7460'),
    subpartition SONO_V_74_DEAFULT values (default)
  ),
  partition REGION_V_75 values ('75')
  (
    subpartition SONO_V_7500 values ('7500'),
    subpartition SONO_V_7505 values ('7505'),
    subpartition SONO_V_7513 values ('7513'),
    subpartition SONO_V_7524 values ('7524'),
    subpartition SONO_V_7527 values ('7527'),
    subpartition SONO_V_7530 values ('7530'),
    subpartition SONO_V_7536 values ('7536'),
    subpartition SONO_V_7538 values ('7538'),
    subpartition SONO_V_7580 values ('7580'),
    subpartition SONO_V_75_DEAFULT values (default)
  ),
  partition REGION_V_76 values ('76')
  (
    subpartition SONO_V_7600 values ('7600'),
    subpartition SONO_V_7602 values ('7602'),
    subpartition SONO_V_7603 values ('7603'),
    subpartition SONO_V_7604 values ('7604'),
    subpartition SONO_V_7606 values ('7606'),
    subpartition SONO_V_7608 values ('7608'),
    subpartition SONO_V_7609 values ('7609'),
    subpartition SONO_V_7610 values ('7610'),
    subpartition SONO_V_7611 values ('7611'),
    subpartition SONO_V_7612 values ('7612'),
    subpartition SONO_V_7627 values ('7627'),
    subpartition SONO_V_76_DEAFULT values (default)
  ),
  partition REGION_V_77 values ('77')
  (
    subpartition SONO_V_7700 values ('7700'),
    subpartition SONO_V_7701 values ('7701'),
    subpartition SONO_V_7702 values ('7702'),
    subpartition SONO_V_7703 values ('7703'),
    subpartition SONO_V_7704 values ('7704'),
    subpartition SONO_V_7705 values ('7705'),
    subpartition SONO_V_7706 values ('7706'),
    subpartition SONO_V_7707 values ('7707'),
    subpartition SONO_V_7708 values ('7708'),
    subpartition SONO_V_7709 values ('7709'),
    subpartition SONO_V_7710 values ('7710'),
    subpartition SONO_V_7713 values ('7713'),
    subpartition SONO_V_7714 values ('7714'),
    subpartition SONO_V_7715 values ('7715'),
    subpartition SONO_V_7716 values ('7716'),
    subpartition SONO_V_7717 values ('7717'),
    subpartition SONO_V_7718 values ('7718'),
    subpartition SONO_V_7719 values ('7719'),
    subpartition SONO_V_7720 values ('7720'),
    subpartition SONO_V_7721 values ('7721'),
    subpartition SONO_V_7722 values ('7722'),
    subpartition SONO_V_7723 values ('7723'),
    subpartition SONO_V_7724 values ('7724'),
    subpartition SONO_V_7725 values ('7725'),
    subpartition SONO_V_7726 values ('7726'),
    subpartition SONO_V_7727 values ('7727'),
    subpartition SONO_V_7728 values ('7728'),
    subpartition SONO_V_7729 values ('7729'),
    subpartition SONO_V_7730 values ('7730'),
    subpartition SONO_V_7731 values ('7731'),
    subpartition SONO_V_7733 values ('7733'),
    subpartition SONO_V_7734 values ('7734'),
    subpartition SONO_V_7735 values ('7735'),
    subpartition SONO_V_7736 values ('7736'),
    subpartition SONO_V_7743 values ('7743'),
    subpartition SONO_V_7745 values ('7745'),
    subpartition SONO_V_7746 values ('7746'),
    subpartition SONO_V_7747 values ('7747'),
    subpartition SONO_V_7748 values ('7748'),
    subpartition SONO_V_7749 values ('7749'),
    subpartition SONO_V_7750 values ('7750'),
    subpartition SONO_V_7751 values ('7751'),
    subpartition SONO_V_77_DEAFULT values (default)
  ),
  partition REGION_V_78 values ('78')
  (
    subpartition SONO_V_7800 values ('7800'),
    subpartition SONO_V_7801 values ('7801'),
    subpartition SONO_V_7802 values ('7802'),
    subpartition SONO_V_7804 values ('7804'),
    subpartition SONO_V_7805 values ('7805'),
    subpartition SONO_V_7806 values ('7806'),
    subpartition SONO_V_7807 values ('7807'),
    subpartition SONO_V_7810 values ('7810'),
    subpartition SONO_V_7811 values ('7811'),
    subpartition SONO_V_7813 values ('7813'),
    subpartition SONO_V_7814 values ('7814'),
    subpartition SONO_V_7816 values ('7816'),
    subpartition SONO_V_7817 values ('7817'),
    subpartition SONO_V_7819 values ('7819'),
    subpartition SONO_V_7820 values ('7820'),
    subpartition SONO_V_7834 values ('7834'),
    subpartition SONO_V_7835 values ('7835'),
    subpartition SONO_V_7837 values ('7837'),
    subpartition SONO_V_7838 values ('7838'),
    subpartition SONO_V_7839 values ('7839'),
    subpartition SONO_V_7840 values ('7840'),
    subpartition SONO_V_7841 values ('7841'),
    subpartition SONO_V_7842 values ('7842'),
    subpartition SONO_V_7843 values ('7843'),
    subpartition SONO_V_7847 values ('7847'),
    subpartition SONO_V_7848 values ('7848'),
    subpartition SONO_V_7850 values ('7850'),
    subpartition SONO_V_78_DEAFULT values (default)
  ),
  partition REGION_V_79 values ('79')
  (
    subpartition SONO_V_7900 values ('7900'),
    subpartition SONO_V_7901 values ('7901'),
    subpartition SONO_V_7907 values ('7907'),
    subpartition SONO_V_79_DEAFULT values (default)
  ),
  partition REGION_V_83 values ('83')
  (
    subpartition SONO_V_83_DEAFULT values (default)
  ),
  partition REGION_V_86 values ('86')
  (
    subpartition SONO_V_8600 values ('8600'),
    subpartition SONO_V_8601 values ('8601'),
    subpartition SONO_V_8602 values ('8602'),
    subpartition SONO_V_8603 values ('8603'),
    subpartition SONO_V_8606 values ('8606'),
    subpartition SONO_V_8607 values ('8607'),
    subpartition SONO_V_8608 values ('8608'),
    subpartition SONO_V_8610 values ('8610'),
    subpartition SONO_V_8611 values ('8611'),
    subpartition SONO_V_8617 values ('8617'),
    subpartition SONO_V_8619 values ('8619'),
    subpartition SONO_V_8622 values ('8622'),
    subpartition SONO_V_8624 values ('8624'),
    subpartition SONO_V_86_DEAFULT values (default)
  ),
  partition REGION_V_87 values ('87')
  (
    subpartition SONO_V_8700 values ('8700'),
    subpartition SONO_V_8706 values ('8706'),
    subpartition SONO_V_8709 values ('8709'),
    subpartition SONO_V_87_DEAFULT values (default)
  ),
  partition REGION_V_89 values ('89')
  (
    subpartition SONO_V_8900 values ('8900'),
    subpartition SONO_V_8901 values ('8901'),
    subpartition SONO_V_8903 values ('8903'),
    subpartition SONO_V_8904 values ('8904'),
    subpartition SONO_V_8905 values ('8905'),
    subpartition SONO_V_8911 values ('8911'),
    subpartition SONO_V_8914 values ('8914'),
    subpartition SONO_V_89_DEAFULT values (default)
  ),
  partition REGION_V_91 values ('91')
  (
    subpartition SONO_V_9100 values ('9100'),
    subpartition SONO_V_9101 values ('9101'),
    subpartition SONO_V_9102 values ('9102'),
    subpartition SONO_V_9103 values ('9103'),
    subpartition SONO_V_9104 values ('9104'),
    subpartition SONO_V_9105 values ('9105'),
    subpartition SONO_V_9106 values ('9106'),
    subpartition SONO_V_9107 values ('9107'),
    subpartition SONO_V_9108 values ('9108'),
    subpartition SONO_V_9109 values ('9109'),
    subpartition SONO_V_9110 values ('9110'),
    subpartition SONO_V_9111 values ('9111'),
    subpartition SONO_V_91_DEAFULT values (default)
  ),
  partition REGION_V_92 values ('92')
  (
    subpartition SONO_V_9200 values ('9200'),
    subpartition SONO_V_9201 values ('9201'),
    subpartition SONO_V_9202 values ('9202'),
    subpartition SONO_V_9203 values ('9203'),
    subpartition SONO_V_9204 values ('9204'),
    subpartition SONO_V_92_DEAFULT values (default)
  ),
  partition REGION_V_99 values ('99')
  (
    subpartition SONO_V_9900 values ('9900'),
    subpartition SONO_V_9901 values ('9901'),
    subpartition SONO_V_9951 values ('9951'),
    subpartition SONO_V_9952 values ('9952'),
    subpartition SONO_V_9953 values ('9953'),
    subpartition SONO_V_9954 values ('9954'),
    subpartition SONO_V_9955 values ('9955'),
    subpartition SONO_V_9956 values ('9956'),
    subpartition SONO_V_9957 values ('9957'),
    subpartition SONO_V_9958 values ('9958'),
    subpartition SONO_V_9959 values ('9959'),
    subpartition SONO_V_9961 values ('9961'),
    subpartition SONO_V_9965 values ('9965'),
    subpartition SONO_V_9971 values ('9971'),
    subpartition SONO_V_9972 values ('9972'),
    subpartition SONO_V_9973 values ('9973'),
    subpartition SONO_V_9974 values ('9974'),
    subpartition SONO_V_9975 values ('9975'),
    subpartition SONO_V_9976 values ('9976'),
    subpartition SONO_V_9977 values ('9977'),
    subpartition SONO_V_9978 values ('9978'),
    subpartition SONO_V_9979 values ('9979'),
    subpartition SONO_V_9998 values ('9998'),
    subpartition SONO_V_99_DEAFULT values (default)
  ),
  partition REGION_V_00 values ('00')
  (
    subpartition SONO_V_0000 values ('0000'),
    subpartition SONO_V_00_DEAFULT values (default)
  ),
  partition REGION_V_DEFAULT values (default)
  (
    subpartition SONO_V_DEFAULT values (default)
  )
)
as select * from NDS2_MRR_USER.DISCREPANCY_AGGREGATE;

begin
  execute immediate 'drop table NDS2_MRR_USER.DISCREPANCY_AGGREGATE_BAK';
  exception when others then null;
end;
/

alter table NDS2_MRR_USER.DISCREPANCY_AGGREGATE rename to DISCREPANCY_AGGREGATE_BAK;
alter table NDS2_MRR_USER.DISCREPANCY_AGGREGATE_MIGR rename to DISCREPANCY_AGGREGATE;

drop table NDS2_MRR_USER.DISCREPANCY_AGGREGATE_BAK;