﻿create table NDS2_MRR_USER.TMP_GNIVC_NDS2_6168 nologging as
select
  calc.INN_CONTRACTOR,
  calc.KPP_EFFECTIVE,
  calc.TYPE_CODE,
  calc.PERIOD_CODE,
  calc.FISCAL_YEAR,
  calc.NO_TKS_COUNT
from (
select
  hstr.INN_CONTRACTOR,
  hstr.KPP_EFFECTIVE,
  hstr.TYPE_CODE,
  hstr.PERIOD_CODE,
  hstr.FISCAL_YEAR,
  count(repl.DOC_ID) AS NO_TKS_COUNT
from NDS2_MRR_USER.DECLARATION_HISTORY hstr
  left join NDS2_MRR_USER.V$DOC doc
    on doc.REF_ENTITY_ID = hstr.REG_NUMBER
      and doc.SONO_CODE = hstr.SONO_CODE_SUBMITED
      and doc.DOC_TYPE = 1
  left join NDS2_MRR_USER.SEOD_EXPLAIN_REPLY repl
    on repl.DOC_ID = doc.DOC_ID
      and repl.STATUS_ID = 1
group by
  hstr.INN_CONTRACTOR,
  hstr.KPP_EFFECTIVE,
  hstr.TYPE_CODE,
  hstr.PERIOD_CODE,
  hstr.FISCAL_YEAR
) calc
left join NDS2_MRR_USER.SIGNIFICANT_CHANGES sgn
  on sgn.INN_CONTRACTOR = calc.INN_CONTRACTOR
	  and sgn.KPP_EFFECTIVE = calc.KPP_EFFECTIVE
	  and sgn.TYPE_CODE = calc.TYPE_CODE
	  and sgn.PERIOD_CODE = calc.PERIOD_CODE
	  and sgn.FISCAL_YEAR = calc.FISCAL_YEAR
where (sgn.NO_TKS_COUNT is null and calc.NO_TKS_COUNT > 0)
  or sgn.NO_TKS_COUNT <> calc.NO_TKS_COUNT;
  
merge into NDS2_MRR_USER.SIGNIFICANT_CHANGES trg
using (select * from NDS2_MRR_USER.TMP_GNIVC_NDS2_6168) src
  on (trg.INN_CONTRACTOR = src.INN_CONTRACTOR
    and trg.KPP_EFFECTIVE = src.KPP_EFFECTIVE
    and trg.TYPE_CODE = src.TYPE_CODE
    and trg.PERIOD_CODE = src.PERIOD_CODE
    and trg.FISCAL_YEAR = src.FISCAL_YEAR)
when matched then
  update set
    HAS_CHANGES = case when (nvl(HAS_AT, 0) + nvl(SLIDE_AT_COUNT, 0) + src.NO_TKS_COUNT) > 0 then 1 else 0 end,
    NO_TKS_COUNT = src.NO_TKS_COUNT
when not matched then
  insert (INN_CONTRACTOR, KPP_EFFECTIVE, TYPE_CODE, PERIOD_CODE, FISCAL_YEAR, HAS_CHANGES, NO_TKS_COUNT)
      values (
        src.INN_CONTRACTOR,
        src.KPP_EFFECTIVE,
        src.TYPE_CODE,
        src.PERIOD_CODE,
        src.FISCAL_YEAR,
        case when src.NO_TKS_COUNT > 0 then 1 else 0 end,
        src.NO_TKS_COUNT
      );

drop table NDS2_MRR_USER.TMP_GNIVC_NDS2_6168;

commit;