-- Create table
create table NDS2_MRR_USER.AUTOSELECTION_REGION
(
  id           number not null,
  selection_id number not null,
  filter_id    number not null,
  region_code  varchar2(2 char) not null
);
