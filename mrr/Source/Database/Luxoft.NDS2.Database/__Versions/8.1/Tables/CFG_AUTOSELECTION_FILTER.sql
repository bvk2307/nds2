create table NDS2_MRR_USER.CFG_AUTOSELECTION_FILTER
(
  include_filter nclob,
  exclude_filter nclob,
  date_modified  date not null
);

insert into NDS2_MRR_USER.CFG_AUTOSELECTION_FILTER (INCLUDE_FILTER, EXCLUDE_FILTER, DATE_MODIFIED)
       select
            flt.include,
            flt.exclude,
            flt.dt
       from 
            NDS2_MRR_USER.AUTOSELECTION_FILTER flt
            join (
                 select MAX(x.dt) as dt
                 from NDS2_MRR_USER.AUTOSELECTION_FILTER x
            ) last_flt on last_flt.dt = flt.dt;

drop table NDS2_MRR_USER.AUTOSELECTION_FILTER;

create table NDS2_MRR_USER.AUTOSELECTION_FILTER
(
  id             number not null,
  include_filter nclob,
  exclude_filter nclob,
  date_created   date not null
);
