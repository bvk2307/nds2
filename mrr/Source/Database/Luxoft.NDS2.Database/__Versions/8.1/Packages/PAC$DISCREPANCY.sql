﻿create or replace package NDS2_MRR_USER.PAC$DISCREPANCY is

  -- Author  : Зыкин Д. (ООО "Люксофт Профешнл" Омск)
  -- Created : 6/10/2015 1:12:21 PM
  -- Purpose : Обработка расхождений в МРР

procedure POST_IMPORT; -- Вызывается СОВ после импорта всех актуальных расхождений из МС

end PAC$DISCREPANCY;
/
create or replace package body NDS2_MRR_USER.PAC$DISCREPANCY is

procedure POST_IMPORT as
  aggregateExists number;
begin

 -- Закрыть расхождения, которые более не найдены среди актуальных (status = 0 Закрыто)
 update SOV_DISCREPANCY t
 set STATUS = 0
 where NOT EXISTS(select 1 from SOV_DISCREPANCY_ALL x where x.SOV_ID = t.SOV_ID);

 -- Добавить новые расхождения (status = 1 Открыто)
 insert into SOV_DISCREPANCY (
        id,
        sov_id,
        create_date,
        type,
        compare_kind,
        rule_group,
        deal_amnt,
        amnt,
        amount_pvp,
        invoice_chapter,
        invoice_rk,
        decl_id,
        invoice_contractor_chapter,
        invoice_contractor_rk,
        decl_contractor_id,
        status,
        side_primary_processing,
        rule_num)
 select
        SEQ_DISCREPANCY_ID.nextval,
        all_t.sov_id,
        all_t.create_date,
        all_t.type,
        all_t.compare_kind,
        all_t.rule_group,
        all_t.deal_amnt,
        all_t.amnt,
        all_t.amount_pvp,
        all_t.invoice_chapter,
        all_t.invoice_rk,
        all_t.decl_id,
        all_t.invoice_contractor_chapter,
        all_t.invoice_contractor_rk,
        all_t.decl_contractor_id,
        1,
        all_t.side_primary_processing,
        all_t.rule_num
 from SOV_DISCREPANCY_ALL all_t
      left join SOV_DISCREPANCY t on t.sov_id = all_t.sov_id
 where t.id is null;

 -- Рассчитать аггрегаты для формирования выборок

 select count(1) into aggregateExists from USER_TABLES where table_name = 'DISCREPANCY_AGGREGATE';

 if aggregateExists > 0 then
    execute immediate 'drop table DISCREPANCY_AGGREGATE';
 end if;

 execute immediate
    'create table DISCREPANCY_AGGREGATE (
        id not null,
        sov_id not null,
        region_code not null,
        sono_code not null,
        inn null,
        name null,
        sur_code null,
        contractor_sur_code null,
        period not null,
        submit_date null,
        decl_sign not null,
        decl_total_amount not null,
        decl_total_count not null,
        decl_min_amount not null,
        decl_max_amount not null,
        decl_avg_amount not null,
        decl_purchase_count not null,
        decl_sales_count not null,
        decl_total_pvp not null,
        decl_max_pvp not null,
        decl_min_pvp not null,
        decl_sales_pvp not null,
        decl_purchase_pvp not null,
        amount  not null,
        pvp not null,
        type_code not null,
        compare_rule not null,
        create_date null) as
    select
        t.id,
        t_all.sov_id,
        substr(seod_decl.sono_code, 1, 2),
        seod_decl.sono_code,
        t_all.buyer_inn,
        t_all.np_name,
        nvl(buyer_sur.sign_code, sur.code),
        nvl(seller_sur.sign_code, sur.code),
        100 * to_number(t_all.tax_year) + to_number(t_all.tax_period),
        seod_decl.eod_date,
        1,
        nvl(sum_decl.discrep_currency_amnt,0),
        nvl(sum_decl.discrep_currency_count,0),
        nvl(sum_decl.discrep_min_amnt,0),
        nvl(sum_decl.discrep_max_amnt,0),
        nvl(sum_decl.discrep_avg_amnt,0),
        nvl(sum_decl.discrep_buy_book_amnt,0),
        nvl(sum_decl.discrep_sell_book_amnt,0),
        nvl(sum_decl.pvp_total_amnt,0),
        nvl(sum_decl.pvp_discrep_max_amnt,0),
        nvl(sum_decl.pvp_discrep_min_amnt,0),
        nvl(sum_decl.pvp_sell_book_amnt,0),
        nvl(sum_decl.pvp_buy_book_amnt,0),
        t_all.amnt,
        t_all.amount_pvp,
        t_all.type,
        t_all.rule_num,
        t_all.create_date
    from SOV_DISCREPANCY_ALL t_all
      join SOV_DISCREPANCY t on t.sov_id = t_all.sov_id
      join
      (
             select row_number() over (partition by a.nds2_id, a.correction_number order by a.correction_number desc) rn, 
                    a.* from SEOD_DECLARATION a
      ) seod_decl on seod_decl.nds2_id = t_all.decl_id and rn = 1
      join V$ASKDEKL ask_decl
           on     ask_decl.period = seod_decl.tax_period
              and ask_decl.otchetgod = seod_decl.fiscal_year
              and ask_decl.innnp = seod_decl.inn
              and ask_decl.nomkorr = seod_decl.correction_number
      join SOV_DECLARATION_INFO sum_decl on sum_decl.declaration_version_id = ask_decl.zip
      join DICT_SUR sur on sur.is_default = 1
      left join EXT_SUR buyer_sur
           on     buyer_sur.inn = t_all.buyer_inn
              and buyer_sur.fiscal_year = t_all.tax_year
              and buyer_sur.fiscal_period = t_all.tax_period
      left join EXT_SUR seller_sur
           on     seller_sur.inn = t_all.buyer_inn
              and seller_sur.fiscal_year = t_all.tax_year
              and seller_sur.fiscal_period = t_all.tax_period';



end;

end PAC$DISCREPANCY;
/
