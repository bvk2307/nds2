﻿create or replace package NDS2_MRR_USER.PAC$AUTOSELECTION is

  -- Author  : Д. Зыкин (Люксофт Омск)
  -- Created : 6/16/2015 5:42:08 PM
  -- Purpose : Создание автовыборок и работа с ними

procedure P$CREATE_EMPTY; -- Создает автовыборки без расхождений
  
procedure P$GET_INSPECTION_LIMITS ( -- Возвращает параметры отбора автовыборки
  pSelectionId in SELECTION.ID%TYPE,
  pCursor out SYS_REFCURSOR);

procedure P$GET_LIMITS_FOR_ONE_SELECTION ( -- Возвращает параметры отбора автовыборки
  pSelectionId in SELECTION.ID%TYPE,
  pCursor out SYS_REFCURSOR);

end PAC$AUTOSELECTION;
/
create or replace package body NDS2_MRR_USER.PAC$AUTOSELECTION is

procedure P$CREATE_EMPTY
  as
      selectionId number;
      regionId number;
      currentDate date;
      filterId number;
      filterSet number;
      allInspections number;
  begin
          
    currentDate := SYSDATE;   
    
    select SEQ_AUTOSELECTION_FILTER.NEXTVAL into filterId from DUAL;
    select COUNT(1) into filterSet from CFG_AUTOSELECTION_FILTER;
    
    if filterSet > 0 then
      begin   
          insert into AUTOSELECTION_FILTER (id, include_filter, exclude_filter, date_created)
                 select
                      filterId,
                      q.include_filter,
                      q.exclude_filter,
                      currentDate
                 from (
                      select include_filter, exclude_filter 
                      from CFG_AUTOSELECTION_FILTER 
                      order by date_modified desc
                 ) q
                 where ROWNUM = 1;
      end;
    else
      begin
          insert into AUTOSELECTION_FILTER (id, date_created)
                 values (filterId, currentDate); 
      end;
    end if;
    
    for autoselection in (
        select 
            region_group.code as region_code,
            region_group.id as region_group_id
        from METODOLOG_REGION region_group 
        where 
                 region_group.context_id = 2 
             and exists(select 1 from METODOLOG_CHECK_EMPLOYEE x where x.resp_id = region_group.id and x.type_id = 0))
    loop
      selectionId := SELECTIONS_SEQ.NEXTVAL;
      regionId := SEQ_AUTOSELECTION_REGION.NEXTVAL;
      
      insert into SELECTION (id, name, creation_date, modification_date, status_date, status, type)
             values (selectionId, 'Автовыборка '||selectionId, currentDate, currentDate, currentDate, 8, 0);
             
      insert into HIST_SELECTION_STATUS (id, dt, val) values (selectionId, currentDate, 8);      
      
      insert into AUTOSELECTION_REGION (id, selection_id, filter_id, region_code)
             values (regionId, selectionId, filterId, autoselection.region_code);
             
      select count(1) into allInspections from METODOLOG_REGION_INSPECTION t where t.region_id = autoselection.region_group_id;
      
      if allInspections = 0 then
        begin          
            insert into AUTOSELECTION_PVP_LIMIT (id, region_id, pvp_limit)
                   select 
                        SEQ_AUTOSELECTION_PVP_LIMIT.NEXTVAL, 
                        regionId, 
                        q.pvp
                   from (
                        select all_limits.pvp
                        from
                               V$METHODOLOGIST_PVP_LIMITS all_limits
                               left join (
                                    select insp.code                          
                                    from
                                           METODOLOG_REGION reg
                                           join METODOLOG_REGION_INSPECTION insp on insp.region_id = reg.id
                                     where reg.context_id = 2 and reg.code = autoselection.region_code
                               ) groups on groups.code = all_limits.sono_code
                        where substr(all_limits.sono_code,1,2) = autoselection.region_code
                              and groups.code is null
                   group by all_limits.pvp) q;    
            
            insert into AUTOSELECTION_INSPECTION (pvp_limit_id, inspection_code)
                   select 
                        lmt.id, all_limits.sono_code
                   from 
                        V$METHODOLOGIST_PVP_LIMITS all_limits
                        join AUTOSELECTION_PVP_LIMIT lmt 
                             on lmt.region_id = regionId
                                and lmt.pvp_limit = all_limits.pvp
                        left join (
                             select insp.code                          
                             from
                                    METODOLOG_REGION reg
                                    join METODOLOG_REGION_INSPECTION insp on insp.region_id = reg.id
                             where reg.context_id = 2 and reg.code = autoselection.region_code
                   ) groups on groups.code = all_limits.sono_code
                   where substr(all_limits.sono_code,1,2) = autoselection.region_code
                         and groups.code is null;
        end;
      else
        begin
            insert into AUTOSELECTION_PVP_LIMIT (id, region_id, pvp_limit)
                   select 
                        SEQ_AUTOSELECTION_PVP_LIMIT.NEXTVAL, 
                        regionId, 
                        q.pvp
                   from (
                        select all_limits.pvp
                        from 
                               V$METHODOLOGIST_PVP_LIMITS all_limits
                               join METODOLOG_REGION_INSPECTION insp on insp.code = all_limits.sono_code
                               join METODOLOG_REGION reg on reg.id = insp.region_id
                        where reg.id = autoselection.region_group_id
                        group by all_limits.pvp) q;    
            
            insert into AUTOSELECTION_INSPECTION (pvp_limit_id, inspection_code)
                   select 
                        lmt.id, all_limits.sono_code
                   from 
                        V$METHODOLOGIST_PVP_LIMITS all_limits
                        join AUTOSELECTION_PVP_LIMIT lmt 
                             on lmt.region_id = regionId
                                and lmt.pvp_limit = all_limits.pvp
                        join METODOLOG_REGION_INSPECTION insp on insp.code = all_limits.sono_code
                        join METODOLOG_REGION reg on reg.id = insp.region_id
                   where reg.id = autoselection.region_group_id;
        end;
      end if;
      
    end loop;
  
  end;

procedure P$GET_INSPECTION_LIMITS (
  pSelectionId in SELECTION.ID%TYPE,
  pCursor out SYS_REFCURSOR)
  as
  begin         
    open pCursor for
         select 
                 all_reg.s_code as region_code,
                 all_reg.s_name as region,
                 all_insp.s_code as inspection_code,
                 all_insp.s_name as inspection,
                 limit.pvp_limit as pvp
         from                
              AUTOSELECTION_INSPECTION insp
              join AUTOSELECTION_PVP_LIMIT limit on limit.id = insp.pvp_limit_id
              join AUTOSELECTION_REGION reg on reg.id = limit.region_id
              join AUTOSELECTION_REGION current_selection 
                   on current_selection.selection_id = pSelectionId 
                      and current_selection.filter_id = reg.filter_id
              join EXT_SONO all_insp on all_insp.s_code = insp.inspection_code
              join EXT_SSRF all_reg on all_reg.s_code = reg.region_code
         order by all_reg.s_code asc, limit.pvp_limit desc;         
  end;

procedure P$GET_LIMITS_FOR_ONE_SELECTION (
  pSelectionId in SELECTION.ID%TYPE,
  pCursor out SYS_REFCURSOR)
  as
  begin
    open pCursor for
         select
                 all_reg.s_code as region_code,
                 all_reg.s_name as region,
                 all_insp.s_code as inspection_code,
                 all_insp.s_name as inspection,
                 limit.pvp_limit as pvp
         from
              AUTOSELECTION_INSPECTION insp
              join AUTOSELECTION_PVP_LIMIT limit on limit.id = insp.pvp_limit_id
              join AUTOSELECTION_REGION reg on reg.id = limit.region_id
                      and reg.selection_id = pSelectionId
              join EXT_SONO all_insp on all_insp.s_code = insp.inspection_code
              join EXT_SSRF all_reg on all_reg.s_code = reg.region_code
         order by all_reg.s_code asc, limit.pvp_limit desc;
  end;

end PAC$AUTOSELECTION;
/
