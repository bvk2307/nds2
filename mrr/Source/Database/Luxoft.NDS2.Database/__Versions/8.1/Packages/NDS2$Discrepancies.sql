﻿create or replace package NDS2_MRR_USER.NDS2$Discrepancies is

  procedure SaveDiscrepancyFilter
    (
      p_Cd in Discrepancies_Filters.Cd%type,
      p_UserName in Discrepancies_Filters.User_Name%type,
      p_IsFavourite in Discrepancies_Filters.Is_Favourite%type,
      p_Name in Discrepancies_Filters.Name%type,
      p_Data in Discrepancies_Filters.Data%type,
      p_Id out Discrepancies_Filters.Id%type
    );

  procedure GetDiscrepancyFilterByUserName
    (
      UserName in Discrepancies_Filters.User_Name%type,
      FiltersCursor out sys_refcursor
    );

  procedure GetDiscrepancyFilterBySel
    (
      SelectionId in Discrepancies_Filters.Cd%type,
      FiltersCursor out sys_refcursor
    );

  procedure GetFilterCriterias
    (
      FiltersCursor out sys_refcursor
    );

  procedure GetActionsHistry
    (
      SelectionId in Selection.Id%type,
      ActionHistoryCursor out sys_refcursor
    );
    
  procedure SaveBuyBookComment
    (
      p_discrepancy_id in discrepancy_comment.discrepancy_id%type,
      p_chapter in discrepancy_comment.chapter%type,
      p_row_key in discrepancy_comment.row_key%type,
      p_buy_accept_date in discrepancy_comment.buy_accept_date%type,
      p_correction_date in discrepancy_comment.correction_date%type,
      p_correction_num in discrepancy_comment.correction_num%type,
      p_currency_code in discrepancy_comment.currency_code%type,
      p_change_correction_date in discrepancy_comment.change_correction_date%type,
      p_change_correction_num in discrepancy_comment.change_correction_num%type,
      p_change_date in discrepancy_comment.change_date%type,
      p_change_num in discrepancy_comment.change_num%type,
      p_broker_inn in discrepancy_comment.broker_inn%type,
      p_broker_kpp in discrepancy_comment.broker_kpp%type,
      p_invoice_date in discrepancy_comment.invoice_date%type,
      p_invoice_num in discrepancy_comment.invoice_num%type,
      p_customs_declaration_num in discrepancy_comment.customs_declaration_num%type,
      p_operation_code in discrepancy_comment.operation_code%type,
      p_seller_inn in discrepancy_comment.seller_inn%type,
      p_seller_kpp in discrepancy_comment.seller_kpp%type,
      p_price_nds_total in discrepancy_comment.price_nds_total%type,
      p_price_total in discrepancy_comment.price_total%type
    );

  procedure SaveSellBookComment
    (
      p_discrepancy_id in discrepancy_comment.discrepancy_id%type,
      p_chapter in discrepancy_comment.chapter%type,
      p_row_key in discrepancy_comment.row_key%type,
      p_buyer_inn in discrepancy_comment.buyer_inn%type,
      p_buyer_kpp in discrepancy_comment.buyer_kpp%type,
      p_receipt_doc_date in discrepancy_comment.receipt_doc_date%type,
      p_receipt_doc_num in discrepancy_comment.receipt_doc_num%type,
      p_correction_date in discrepancy_comment.correction_date%type,
      p_correction_num in discrepancy_comment.correction_num%type,
      p_currency_code in discrepancy_comment.currency_code%type,
      p_change_correction_date in discrepancy_comment.change_correction_date%type,
      p_change_correction_num in discrepancy_comment.change_correction_num%type,
      p_change_date in discrepancy_comment.change_date%type,
      p_change_num in discrepancy_comment.change_num%type,
      p_broker_inn in discrepancy_comment.broker_inn%type,
      p_broker_kpp in discrepancy_comment.broker_kpp%type,
      p_invoice_date in discrepancy_comment.invoice_date%type,
      p_invoice_num in discrepancy_comment.invoice_num%type,
      p_operation_code in discrepancy_comment.operation_code%type,
      p_price_sell_0 in discrepancy_comment.price_sell_0%type,
      p_price_sell_10 in discrepancy_comment.price_sell_10%type,
      p_price_sell_18 in discrepancy_comment.price_sell_18%type,
      p_price_nds_10 in discrepancy_comment.price_nds_10%type,
      p_price_nds_18 in discrepancy_comment.price_nds_18%type,
      p_price_tax_free in discrepancy_comment.price_tax_free%type,
      p_price_sell_in_curr in discrepancy_comment.price_sell_in_curr%type,
      p_price_sell in discrepancy_comment.price_sell%type
    );

  procedure SaveSentJournalComment
    (
      p_discrepancy_id in discrepancy_comment.discrepancy_id%type,
      p_chapter in discrepancy_comment.chapter%type,
      p_row_key in discrepancy_comment.row_key%type,
      p_price_total in discrepancy_comment.price_total%type,
      p_buyer_inn in discrepancy_comment.buyer_inn%type,
      p_buyer_kpp in discrepancy_comment.buyer_kpp%type,
      p_correction_date in discrepancy_comment.correction_date%type,
      p_correction_num in discrepancy_comment.correction_num%type,
      p_diff_correct_decrease in discrepancy_comment.diff_correct_decrease%type,
      p_diff_correct_increase in discrepancy_comment.diff_correct_increase%type,
      p_currency_code in discrepancy_comment.currency_code%type,
      p_change_correction_date in discrepancy_comment.change_correction_date%type,
      p_change_correction_num in discrepancy_comment.change_correction_num%type,
      p_change_date in discrepancy_comment.change_date%type,
      p_change_num in discrepancy_comment.change_num%type,
      p_invoice_date in discrepancy_comment.invoice_date%type,
      p_invoice_num in discrepancy_comment.invoice_num%type,
      p_operation_code in discrepancy_comment.operation_code%type,
      p_seller_inn in discrepancy_comment.seller_inn%type,
      p_seller_kpp in discrepancy_comment.seller_kpp%type,
      p_create_date in discrepancy_comment.create_date%type,
      p_price_nds_total in discrepancy_comment.price_nds_total%type,
      p_diff_correct_nds_decrease in discrepancy_comment.diff_correct_nds_decrease%type,
      p_diff_correct_nds_increase in discrepancy_comment.diff_correct_nds_increase%type
    );

  procedure SaveRecieveJournalComment
    (
      p_discrepancy_id in discrepancy_comment.discrepancy_id%type,
      p_chapter in discrepancy_comment.chapter%type,
      p_row_key in discrepancy_comment.row_key%type,
      p_price_total in discrepancy_comment.price_total%type,
      p_broker_inn in discrepancy_comment.broker_inn%type,
      p_broker_kpp in discrepancy_comment.broker_kpp%type,
    p_deal_kind_code in discrepancy_comment.deal_kind_code%type,
      p_correction_date in discrepancy_comment.correction_date%type,
      p_correction_num in discrepancy_comment.correction_num%type,
      p_diff_correct_decrease in discrepancy_comment.diff_correct_decrease%type,
      p_diff_correct_increase in discrepancy_comment.diff_correct_increase%type,
      p_currency_code in discrepancy_comment.currency_code%type,
      p_change_correction_date in discrepancy_comment.change_correction_date%type,
      p_change_correction_num in discrepancy_comment.change_correction_num%type,
      p_change_date in discrepancy_comment.change_date%type,
      p_change_num in discrepancy_comment.change_num%type,
      p_invoice_date in discrepancy_comment.invoice_date%type,
      p_invoice_num in discrepancy_comment.invoice_num%type,
      p_operation_code in discrepancy_comment.operation_code%type,
      p_seller_inn in discrepancy_comment.seller_inn%type,
      p_seller_kpp in discrepancy_comment.seller_kpp%type,
      p_receive_date in discrepancy_comment.receive_date%type,
      p_price_nds_total in discrepancy_comment.price_nds_total%type,
      p_diff_correct_nds_decrease in discrepancy_comment.diff_correct_nds_decrease%type,
      p_diff_correct_nds_increase in discrepancy_comment.diff_correct_nds_increase%type
    );

procedure P$MoveDiscrepancyFromTmpToWork
(
   p_discrepancy_sov_id in sov_discrepancy.sov_id%type,
   p_discrepancy_id out sov_discrepancy.id%type
);

function F$GET_STAGE_STATUS_NAME
(
   pStageId in dict_discrepancy_stage.id%type,
   pStageStatusId in hist_discrepancy_stage.status_id%type
)
return VARCHAR2;

  procedure Get_Discrepancy_Side
  (
    pDiscrepancyId in SOV_Discrepancy.Id%type,
    pCursor out sys_refcursor
  );

  procedure GetBuyBooks
  (
    pDiscrepancyId in SOV_Discrepancy.Id%type,
    pSideNumber in number,
    pCursor out sys_refcursor
  );

  procedure GetSellBooks
  (
    pDiscrepancyId in SOV_Discrepancy.Id%type,
    pSideNumber in number,
    pCursor out sys_refcursor
  );

  procedure GetSentJournal
  (
    pDiscrepancyId in SOV_Discrepancy.Id%type,
    pSideNumber in number,
    pCursor out sys_refcursor
  );

  procedure GetRecieveJournal
  (
    pDiscrepancyId in SOV_Discrepancy.Id%type,
    pSideNumber in number,
    pCursor out sys_refcursor
  );

end;
/
create or replace package body NDS2_MRR_USER.NDS2$Discrepancies is

  procedure SaveDiscrepancyFilter
    (
      p_Cd in Discrepancies_Filters.Cd%type,
      p_UserName in Discrepancies_Filters.User_Name%type,
      p_IsFavourite in Discrepancies_Filters.Is_Favourite%type,
      p_Name in Discrepancies_Filters.Name%type,
      p_Data in Discrepancies_Filters.Data%type,
      p_Id out Discrepancies_Filters.Id%type
    )
  is
  begin
    update
      Discrepancies_Filters
    set
      Cd = p_Cd,
      User_Name = p_UserName,
      Is_Favourite = p_IsFavourite,
      Name = p_Name,
      Data = p_Data
    where Id = p_Id;

    if SQL%ROWCOUNT = 0 then
    begin
        p_Id := SEQ_DISCREPANCIES_FILTER.NEXTVAL;
        insert into Discrepancies_Filters values(p_Id, p_Cd, p_UserName, p_IsFavourite, p_Name, p_Data);
    end;
    end if;
 end;

  procedure GetDiscrepancyFilterByUserName
    (
      UserName in Discrepancies_Filters.User_Name%type,
      FiltersCursor out sys_refcursor
    )
  is
  begin
    open FiltersCursor for
    select * from Discrepancies_Filters where (User_Name = UserName);
  end;

  procedure GetDiscrepancyFilterBySel
    (
      SelectionId in Discrepancies_Filters.Cd%type,
      FiltersCursor out sys_refcursor
    )
  is
  begin
    open FiltersCursor for
    select * from Discrepancies_Filters where (Cd = SelectionId);
  end;

  procedure GetFilterCriterias
    (
      FiltersCursor out sys_refcursor
    )
  is
  begin
    open FiltersCursor for
    select * from Dict_Filter_Types;
  end;

  procedure GetActionsHistry
    (
      SelectionId in Selection.Id%type,
      ActionHistoryCursor out sys_refcursor
    )
  is
  begin
    open ActionHistoryCursor for
    select ah.id, ah.cd, ah.status, ah.change_date, ah.action_comment, ah.user_name, ah.action,
       dss.id, dss.name, da.id, da.action_name
    from Action_History ah
           left join Dict_Selection_Status dss on (ah.Status = dss.Id)
           left join Dict_Actions da on (ah.Action = da.Id)
    where ah.Cd = SelectionId
    order by ah.change_date desc;
  end;

  procedure SaveComment
    (
      p_discrepancy_id in discrepancy_comment.discrepancy_id%type,
      p_chapter in discrepancy_comment.chapter%type,
      p_row_key in discrepancy_comment.row_key%type,
      p_create_date in discrepancy_comment.create_date%type,
      p_receive_date in discrepancy_comment.receive_date%type,
      p_operation_code in discrepancy_comment.operation_code%type,
      p_invoice_num in discrepancy_comment.invoice_num%type,
      p_invoice_date in discrepancy_comment.invoice_date%type,
      p_change_num in discrepancy_comment.change_num%type,
      p_change_date in discrepancy_comment.change_date%type,
      p_correction_num in discrepancy_comment.correction_num%type,
      p_correction_date in discrepancy_comment.correction_date%type,
      p_change_correction_num in discrepancy_comment.change_correction_num%type,
      p_change_correction_date in discrepancy_comment.change_correction_date%type,
      p_receipt_doc_num in discrepancy_comment.receipt_doc_num%type,
      p_receipt_doc_date in discrepancy_comment.receipt_doc_date%type,
      p_buy_accept_date in discrepancy_comment.buy_accept_date%type,
      p_buyer_inn in discrepancy_comment.buyer_inn%type,
      p_buyer_kpp in discrepancy_comment.buyer_kpp%type,
      p_seller_inn in discrepancy_comment.seller_inn%type,
      p_seller_kpp in discrepancy_comment.seller_kpp%type,
      p_seller_invoice_num in discrepancy_comment.seller_invoice_num%type,
      p_seller_invoice_date in discrepancy_comment.seller_invoice_date%type,
      p_broker_inn in discrepancy_comment.broker_inn%type,
      p_broker_kpp in discrepancy_comment.broker_kpp%type,
      p_deal_kind_code in discrepancy_comment.deal_kind_code%type,
      p_customs_declaration_num in discrepancy_comment.customs_declaration_num%type,
      p_currency_code in discrepancy_comment.currency_code%type,
      p_price_buy_amount in discrepancy_comment.price_buy_amount%type,
      p_price_buy_nds_amount in discrepancy_comment.price_buy_nds_amount%type,
      p_price_sell in discrepancy_comment.price_sell%type,
      p_price_sell_in_curr in discrepancy_comment.price_sell_in_curr%type,
      p_price_sell_18 in discrepancy_comment.price_sell_18%type,
      p_price_sell_10 in discrepancy_comment.price_sell_10%type,
      p_price_sell_0 in discrepancy_comment.price_sell_0%type,
      p_price_nds_18 in discrepancy_comment.price_nds_18%type,
      p_price_nds_10 in discrepancy_comment.price_nds_10%type,
      p_price_tax_free in discrepancy_comment.price_tax_free%type,
      p_price_total in discrepancy_comment.price_total%type,
      p_price_nds_total in discrepancy_comment.price_nds_total%type,
      p_diff_correct_decrease in discrepancy_comment.diff_correct_decrease%type,
      p_diff_correct_increase in discrepancy_comment.diff_correct_increase%type,
      p_diff_correct_nds_decrease in discrepancy_comment.diff_correct_nds_decrease%type,
      p_diff_correct_nds_increase in discrepancy_comment.diff_correct_nds_increase%type
    )
    is
    begin
      insert into discrepancy_comment
      values (p_discrepancy_id, p_chapter, p_row_key, p_create_date, p_receive_date, p_operation_code, p_invoice_num, p_invoice_date, p_change_num, p_change_date, p_correction_num, p_correction_date, p_change_correction_num, p_change_correction_date, p_receipt_doc_num, p_receipt_doc_date, p_buy_accept_date, p_buyer_inn, p_buyer_kpp, p_seller_inn, p_seller_kpp, p_seller_invoice_num, p_seller_invoice_date, p_broker_inn, p_broker_kpp, p_deal_kind_code, p_customs_declaration_num, p_currency_code, p_price_buy_amount, p_price_buy_nds_amount, p_price_sell, p_price_sell_in_curr, p_price_sell_18, p_price_sell_10, p_price_sell_0, p_price_nds_18, p_price_nds_10, p_price_tax_free, p_price_total, p_price_nds_total, p_diff_correct_decrease, p_diff_correct_increase, p_diff_correct_nds_decrease, p_diff_correct_nds_increase);
    end;

  procedure SaveBuyBookComment
    (
      p_discrepancy_id in discrepancy_comment.discrepancy_id%type,
      p_chapter in discrepancy_comment.chapter%type,
      p_row_key in discrepancy_comment.row_key%type,
      p_buy_accept_date in discrepancy_comment.buy_accept_date%type,
      p_correction_date in discrepancy_comment.correction_date%type,
      p_correction_num in discrepancy_comment.correction_num%type,
      p_currency_code in discrepancy_comment.currency_code%type,
      p_change_correction_date in discrepancy_comment.change_correction_date%type,
      p_change_correction_num in discrepancy_comment.change_correction_num%type,
      p_change_date in discrepancy_comment.change_date%type,
      p_change_num in discrepancy_comment.change_num%type,
      p_broker_inn in discrepancy_comment.broker_inn%type,
      p_broker_kpp in discrepancy_comment.broker_kpp%type,
      p_invoice_date in discrepancy_comment.invoice_date%type,
      p_invoice_num in discrepancy_comment.invoice_num%type,
      p_customs_declaration_num in discrepancy_comment.customs_declaration_num%type,
      p_operation_code in discrepancy_comment.operation_code%type,
      p_seller_inn in discrepancy_comment.seller_inn%type,
      p_seller_kpp in discrepancy_comment.seller_kpp%type,
      p_price_nds_total in discrepancy_comment.price_nds_total%type,
      p_price_total in discrepancy_comment.price_total%type
    )
    is
  begin
    SaveComment(p_discrepancy_id, p_chapter, p_row_key, null, null, p_operation_code, p_invoice_num, p_invoice_date, p_change_num, p_change_date, p_correction_num, p_correction_date, p_change_correction_num, p_change_correction_date, null, null, p_buy_accept_date, null, null, p_seller_inn, p_seller_kpp, null, null, p_broker_inn, p_broker_kpp, null, p_customs_declaration_num, p_currency_code, null, null, null, null, null, null, null, null, null, null, p_price_total, p_price_nds_total, null, null, null, null);
  end;

  procedure SaveSellBookComment
    (
      p_discrepancy_id in discrepancy_comment.discrepancy_id%type,
      p_chapter in discrepancy_comment.chapter%type,
      p_row_key in discrepancy_comment.row_key%type,
      p_buyer_inn in discrepancy_comment.buyer_inn%type,
      p_buyer_kpp in discrepancy_comment.buyer_kpp%type,
      p_receipt_doc_date in discrepancy_comment.receipt_doc_date%type,
      p_receipt_doc_num in discrepancy_comment.receipt_doc_num%type,
      p_correction_date in discrepancy_comment.correction_date%type,
      p_correction_num in discrepancy_comment.correction_num%type,
      p_currency_code in discrepancy_comment.currency_code%type,
      p_change_correction_date in discrepancy_comment.change_correction_date%type,
      p_change_correction_num in discrepancy_comment.change_correction_num%type,
      p_change_date in discrepancy_comment.change_date%type,
      p_change_num in discrepancy_comment.change_num%type,
      p_broker_inn in discrepancy_comment.broker_inn%type,
      p_broker_kpp in discrepancy_comment.broker_kpp%type,
      p_invoice_date in discrepancy_comment.invoice_date%type,
      p_invoice_num in discrepancy_comment.invoice_num%type,
      p_operation_code in discrepancy_comment.operation_code%type,
      p_price_sell_0 in discrepancy_comment.price_sell_0%type,
      p_price_sell_10 in discrepancy_comment.price_sell_10%type,
      p_price_sell_18 in discrepancy_comment.price_sell_18%type,
      p_price_nds_10 in discrepancy_comment.price_nds_10%type,
      p_price_nds_18 in discrepancy_comment.price_nds_18%type,
      p_price_tax_free in discrepancy_comment.price_tax_free%type,
      p_price_sell_in_curr in discrepancy_comment.price_sell_in_curr%type,
      p_price_sell in discrepancy_comment.price_sell%type
    )
    is
  begin
    SaveComment(p_discrepancy_id, p_chapter, p_row_key, null, null, p_operation_code, p_invoice_num, p_invoice_date, p_change_num, p_change_date, p_correction_num, p_correction_date, p_change_correction_num, p_change_correction_date, p_receipt_doc_num, p_receipt_doc_date, null, p_buyer_inn, p_buyer_kpp, null, null, null, null, p_broker_inn, p_broker_kpp, null, null, p_currency_code, null, null, p_price_sell, p_price_sell_in_curr, p_price_sell_18, p_price_sell_10, p_price_sell_0, p_price_nds_18, p_price_nds_10, p_price_tax_free, null, null, null, null, null, null);
  end;

  procedure SaveSentJournalComment
    (
      p_discrepancy_id in discrepancy_comment.discrepancy_id%type,
      p_chapter in discrepancy_comment.chapter%type,
      p_row_key in discrepancy_comment.row_key%type,
      p_price_total in discrepancy_comment.price_total%type,
      p_buyer_inn in discrepancy_comment.buyer_inn%type,
      p_buyer_kpp in discrepancy_comment.buyer_kpp%type,
      p_correction_date in discrepancy_comment.correction_date%type,
      p_correction_num in discrepancy_comment.correction_num%type,
      p_diff_correct_decrease in discrepancy_comment.diff_correct_decrease%type,
      p_diff_correct_increase in discrepancy_comment.diff_correct_increase%type,
      p_currency_code in discrepancy_comment.currency_code%type,
      p_change_correction_date in discrepancy_comment.change_correction_date%type,
      p_change_correction_num in discrepancy_comment.change_correction_num%type,
      p_change_date in discrepancy_comment.change_date%type,
      p_change_num in discrepancy_comment.change_num%type,
      p_invoice_date in discrepancy_comment.invoice_date%type,
      p_invoice_num in discrepancy_comment.invoice_num%type,
      p_operation_code in discrepancy_comment.operation_code%type,
      p_seller_inn in discrepancy_comment.seller_inn%type,
      p_seller_kpp in discrepancy_comment.seller_kpp%type,
      p_create_date in discrepancy_comment.create_date%type,
      p_price_nds_total in discrepancy_comment.price_nds_total%type,
      p_diff_correct_nds_decrease in discrepancy_comment.diff_correct_nds_decrease%type,
      p_diff_correct_nds_increase in discrepancy_comment.diff_correct_nds_increase%type
    )
    is
  begin
    SaveComment(p_discrepancy_id, p_chapter, p_row_key, p_create_date, null, p_operation_code, p_invoice_num, p_invoice_date, p_change_num, p_change_date, p_correction_num, p_correction_date, p_change_correction_num, p_change_correction_date, null, null, null, p_buyer_inn, p_buyer_kpp, p_seller_inn, p_seller_kpp, null, null, null, null, null, null, p_currency_code, null, null, null, null, null, null, null, null, null, null, p_price_total, p_price_nds_total, p_diff_correct_decrease, p_diff_correct_increase, p_diff_correct_nds_decrease, p_diff_correct_nds_increase);
  end;

  procedure SaveRecieveJournalComment
    (
      p_discrepancy_id in discrepancy_comment.discrepancy_id%type,
      p_chapter in discrepancy_comment.chapter%type,
      p_row_key in discrepancy_comment.row_key%type,
      p_price_total in discrepancy_comment.price_total%type,
      p_broker_inn in discrepancy_comment.broker_inn%type,
      p_broker_kpp in discrepancy_comment.broker_kpp%type,
    p_deal_kind_code in discrepancy_comment.deal_kind_code%type,
      p_correction_date in discrepancy_comment.correction_date%type,
      p_correction_num in discrepancy_comment.correction_num%type,
      p_diff_correct_decrease in discrepancy_comment.diff_correct_decrease%type,
      p_diff_correct_increase in discrepancy_comment.diff_correct_increase%type,
      p_currency_code in discrepancy_comment.currency_code%type,
      p_change_correction_date in discrepancy_comment.change_correction_date%type,
      p_change_correction_num in discrepancy_comment.change_correction_num%type,
      p_change_date in discrepancy_comment.change_date%type,
      p_change_num in discrepancy_comment.change_num%type,
      p_invoice_date in discrepancy_comment.invoice_date%type,
      p_invoice_num in discrepancy_comment.invoice_num%type,
      p_operation_code in discrepancy_comment.operation_code%type,
      p_seller_inn in discrepancy_comment.seller_inn%type,
      p_seller_kpp in discrepancy_comment.seller_kpp%type,
      p_receive_date in discrepancy_comment.receive_date%type,
      p_price_nds_total in discrepancy_comment.price_nds_total%type,
      p_diff_correct_nds_decrease in discrepancy_comment.diff_correct_nds_decrease%type,
      p_diff_correct_nds_increase in discrepancy_comment.diff_correct_nds_increase%type
    )
    is
  begin
    SaveComment(p_discrepancy_id, p_chapter, p_row_key, null, p_receive_date, p_operation_code, p_invoice_num, p_invoice_date, p_change_num, p_change_date, p_correction_num, p_correction_date, p_change_correction_num, p_change_correction_date, null, null, null, null, null, p_seller_inn, p_seller_kpp, null, null, p_broker_inn, p_broker_kpp, p_deal_kind_code, null, p_currency_code, null, null, null, null, null, null, null, null, null, null, p_price_total, p_price_nds_total, p_diff_correct_decrease, p_diff_correct_increase, p_diff_correct_nds_decrease, p_diff_correct_nds_increase);
  end;

procedure P$MoveDiscrepancyFromTmpToWork
(
   p_discrepancy_sov_id in sov_discrepancy.sov_id%type,
   p_discrepancy_id out sov_discrepancy.id%type
)
is
begin
  p_discrepancy_id := SEQ_DISCREPANCY_ID.NEXTVAL();
    insert into SOV_DISCREPANCY
    (
      id,
      sov_id,
      create_date,
      type,
      compare_kind,
      rule_group,
      deal_amnt,
      amnt,
      amount_pvp,
      course,
      course_cost,
      sur_code,
      invoice_chapter,
      invoice_rk,
      decl_id,
      invoice_contractor_chapter,
      invoice_contractor_rk,
      decl_contractor_id,
      status,
      side_primary_processing,
	  stage
    )
    select
      p_discrepancy_id,
      st.sov_id,
      st.create_date,
      st.type,
      st.compare_kind,
      st.rule_group,
      st.deal_amnt,
      st.amnt,
      st.amount_pvp,
      st.course,
      st.course_cost,
      st.sur_code,
      st.invoice_chapter,
      st.invoice_rk,
      st.decl_id,
      st.invoice_contractor_chapter,
      st.invoice_contractor_rk,
      st.decl_contractor_id,
      st.status,
      st.side_primary_processing,
	  NVL(st.stage, 1) as stage
    from SOV_DISCREPANCY_TMP  st
    left join SOV_DISCREPANCY sd on sd.sov_id = st.sov_id
    where st.sov_id = p_discrepancy_sov_id and sd.id is null;
end;


function F$GET_STAGE_STATUS_NAME
(
   pStageId in dict_discrepancy_stage.id%type,
   pStageStatusId in hist_discrepancy_stage.status_id%type
)
return VARCHAR2
is
Result VARCHAR2(128);
DocTypeId number;
begin

if (pStageId = 2 or pStageId = 3) then
   DocTypeId := 1;
else
  If (pStageId = 4 or pStageId = 5) then
   DocTypeId := 2;
  end If;
end If;

select
  case NVL(pStageId, 1)
      when 1 then replace(
                      replace(
                         replace(selstat.name, 'Черновик', 'В работе'),
                      'На согласовании', 'В работе'),
                  'На доработке', 'В работе')
      when 2 then docstatStage2.description
      when 3 then docstatStage3.description
      when 4 then docstatStage4.description
      when 5 then docstatStage5.description
      when 6 then null
  end as STAGE_STATUS_NAME into Result
from dual
     left join DICT_SELECTION_STATUS selstat on selstat.id = pStageStatusId
     left join DOC_STATUS docstatStage2 on (docstatStage2.id = pStageStatusId) and (docstatStage2.doc_type = DocTypeId)
     left join DOC_STATUS docstatStage3 on (docstatStage3.id = pStageStatusId) and (docstatStage3.doc_type = DocTypeId)
     left join DOC_STATUS docstatStage4 on (docstatStage4.id = pStageStatusId) and (docstatStage4.doc_type = DocTypeId)
     left join DOC_STATUS docstatStage5 on (docstatStage5.id = pStageStatusId) and (docstatStage5.doc_type = DocTypeId);

     Return Result;
end;

  procedure Get_Discrepancy_Side
  (
    pDiscrepancyId in SOV_Discrepancy.Id%type,
    pCursor out sys_refcursor
  )
  as
  v_side2_name varchar2(1024 char);
  v_side2_inn varchar2(128 char);
  v_side2_kpp varchar2(128 char);
  v_sid number(38);
  begin
  
  v_sid := seq_discrep_tmp_sid.nextval;
  
  for r in (select 
                sd.id as discrep_id,
                sd.sov_id,
                sd.invoice_rk,
                sd.invoice_chapter,
                case sd.invoice_chapter 
                  when 8 then sia.price_buy_amount
                  when 9 then sia.price_sell
                  when 10 then sia.price_total
                  when 11 then sia.price_total
                  when 12 then sia.price_total
                end as price,
                case sd.invoice_chapter 
                  when 8 then sia.price_buy_nds_amount
                  when 9 then nvl(sia.PRICE_SELL_18, 0) + nvl(sia.PRICE_SELL_10, 0)
                  when 10 then sia.price_nds_total
                  when 11 then sia.price_nds_total
                  when 12 then sia.price_nds_total
                end as priceNds,
                dh.inn,
                dh.kpp,
                dh.name,
                dh.declaration_version_id,
                dh.soun_code,
                dh.soun,
                dh.region_code,
                dh.region,
                sia.invoice_num,
                sia.invoice_date,
                sia.logical_errors,
                sia.format_errors,
                /*Contractor*/
                dhContr.Declaration_Version_Id as ctr_DeclVerId,
                sd.invoice_contractor_rk,
                sd.invoice_contractor_chapter,
                case sd.invoice_contractor_chapter 
                  when 8 then sia.price_buy_amount
                  when 9 then sia.price_sell
                  when 10 then sia.price_total
                  when 11 then sia.price_total
                  when 12 then sia.price_total
                end as priceContractor,
                case sd.invoice_contractor_chapter 
                  when 8 then sia.price_buy_nds_amount
                  when 9 then nvl(sia.PRICE_SELL_18, 0) + nvl(sia.PRICE_SELL_10, 0)
                  when 10 then sia.price_nds_total
                  when 11 then sia.price_nds_total
                  when 12 then sia.price_nds_total
                end as priceNdsContractor,
                sia2.invoice_num as ctr_Invoice_num,
                sia2.invoice_date as ctr_invoice_date,    
                dhContr.Inn as ctr_Inn,
                dhContr.Kpp as ctr_Kpp,
                dhContr.Name as ctr_Name,            
                dhContr.Soun_Code as ctr_Soun_Code,
                dhContr.Soun as ctr_Soun,
                dhContr.Region_Code as ctr_Region_Code,
                dhContr.Region as ctr_Region
                from sov_discrepancy sd
                inner join mv$declaration_history dh on dh.id = sd.decl_id and dh.is_active = 1
                left join sov_invoice_all sia on sia.row_key = sd.invoice_rk 
                left join sov_invoice_all sia2 on sia2.row_key = sd.invoice_contractor_rk 
                left join mv$declaration_history dhContr on dhContr.Declaration_Version_Id = sia2.declaration_version_id 
                where sd.id = pDiscrepancyId)
   loop
     begin
       if r.invoice_chapter in (8, 11) then
         select
          sisa.seller_inn,
          sisa.seller_kpp,
          tpName.Np_Name
          into v_side2_inn, v_side2_kpp, v_side2_name
         from sov_invoice_seller_all sisa 
         left join mv$tax_payer_name tpName on tpName.inn = sisa.seller_inn
         where sisa.row_key_id = r.invoice_contractor_rk and rownum = 1;
       else
         select
          siba.buyer_inn,
          siba.buyer_kpp,
          tpName.Np_Name
          into v_side2_inn, v_side2_kpp, v_side2_name
         from sov_invoice_buyer_all siba 
         left join mv$tax_payer_name tpName on tpName.inn = siba.buyer_inn
         where siba.row_key_id = r.invoice_contractor_rk and rownum = 1;         
       end if;   
       exception when no_data_found then null;
     end;
    
     insert into T$DISCREPANCY_SIDE values(v_sid, r.discrep_id, r.sov_id, r.invoice_rk, 'Раздел '||r.invoice_chapter,
     r.invoice_chapter, r.declaration_version_id, r.inn, r.kpp, r.name, v_side2_inn, v_side2_kpp, v_side2_name, r.invoice_num, r.invoice_date,
     r.price, r.priceNds, r.region_code, r.region, r.soun_code, r.soun, r.format_errors, r.logical_errors);

     begin
       if r.invoice_contractor_chapter in (8, 11) then
         select
          sisa.seller_inn,
          sisa.seller_kpp,
          tpName.Np_Name
          into v_side2_inn, v_side2_kpp, v_side2_name
         from sov_invoice_seller_all sisa 
         left join mv$tax_payer_name tpName on tpName.inn = sisa.seller_inn
         where sisa.row_key_id = r.invoice_rk and rownum = 1;
       else
         select
          siba.buyer_inn,
          siba.buyer_kpp,
          tpName.Np_Name
          into v_side2_inn, v_side2_kpp, v_side2_name
         from sov_invoice_buyer_all siba 
         left join mv$tax_payer_name tpName on tpName.inn = siba.buyer_inn
         where siba.row_key_id = r.invoice_rk and rownum = 1;         
       end if;   
       exception when no_data_found then null;
     end;

     insert into T$DISCREPANCY_SIDE values(v_sid, r.discrep_id, r.sov_id, r.invoice_contractor_rk, 'Раздел '||r.invoice_contractor_chapter,
     r.invoice_contractor_chapter, r.ctr_DeclVerId, r.ctr_Inn, r.ctr_Kpp, r.ctr_Name, v_side2_inn, v_side2_kpp, v_side2_name, r.ctr_invoice_num, r.ctr_invoice_date,
     r.priceContractor, r.priceNdsContractor, r.ctr_region_code, r.ctr_region, r.ctr_soun_code, r.ctr_soun, r.format_errors, r.logical_errors);
     
   end loop;      
  
  /*return result*/
    open pCursor for
    select
      discrepancy_id,
      discrepancy_sov_id,
      invoice_row_key as InvoiceId,
      chapter_descr as SideName,
      chapter,
      declaration_ver_id as DeclarationId,
      Side_1_Inn as Side1Inn,
      Side_1_Kpp as Side1Kpp,
      Side_1_Name as Side1Name,
      Side_2_Inn as Side2Inn,
      Side_2_Kpp as Side2Kpp,
      Side_2_Name as Side2Name,
      invoice_num as SideInfoInvoiceNumber,
      invoice_date as SideInfoInvoiceDate,
      price_total as SideInfoInvoiceAmount,
      price_nds_total as SideInfoTaxAmount,
      region_code as RegionCode,
      region as RegionName,
      soun_code as InspectionCode,
      soun as InspectionName,
      FormatErrors,
      LOGICAL_ERRORS as LogicalErrors
      from T$DISCREPANCY_SIDE where sid = v_sid;

  end;

  procedure GetBuyBooks
  (
    pDiscrepancyId in SOV_Discrepancy.Id%type,
    pSideNumber in number,
    pCursor out sys_refcursor
  ) 
  is
  v_rowKey varchar2(1024 char);
  v_declId number(38);
  begin
    
  select
  (case pSideNumber when 0 then d.DECL_ID when 1 then d.DECL_CONTRACTOR_ID else null end) as declId,
  (case pSideNumber when 0 then d.invoice_rk when 1 then d.invoice_contractor_rk else null end) as rowKey
  into v_declId, v_rowKey
  from sov_discrepancy d where d.id = pDiscrepancyId;
  
  
    open pCursor for
    select
      d.id,
      d.sov_id as discrepancy_sov_id,
      i.row_key as RowKey,
      i.actual_row_key,
      case when dc.row_key is null then 0 else 1 end as Commented,
      /*break*/
	    i.ordinal_number as OrdinalNumber,
      decl.full_tax_period as ReportPeriod,
      PayDoc.Doc_Date as PaymentDocDate,
      PayDoc.Doc_Num as PaymentDocNumber,
      OpCode.operation_code as OperationCode,
      i.invoice_num as InvoiceNumber,
      i.invoice_date as InvoiceDate,
      i.change_num as FixedInvoiceNumber,
      i.change_date as FixedInvoiceDate,
      i.correction_num as CorrectedInvoiceNumber,
      i.correction_date as CorrectedInvoiceDate,
      i.change_correction_num as FixedCorrectedInvoiceNumber,
      i.change_correction_date as FixedCorrectedInvoiceDate,
      i.okv_code as CurrencyCode,
      decl.correction_number as CorrectionNumber,
	    /*i.is_dop_list*/ 0 as PriznakDopList,
      /*break*/
      SellerInfo.Seller_Inn as SellerInfoINN,
      SellerInfo.Seller_Kpp as SellerInfoKPP,
      SellerInfo.Seller_Name as SellerInfoName,
      i.broker_inn as IntermediaryInfoINN,
      i.broker_kpp as IntermediaryInfoKPP,
      brokerName.Np_Name as IntermediaryInfoName,
      null as AccountingDate,
      --i.buy_accept_date         as AccountingDate,
      i.customs_declaration_num as NumberTD,
      i.price_total             as TotalAmount,
      i.price_nds_total         as TaxAmount,
      i.logical_errors          as ErrorCodesString,
      0 as STAGE_CODE,
      '-' as STAGE_NAME,
      ''  as STAGE_STATUS_CODE,
      '-' as STAGE_STATUS_NAME
    from sov_discrepancy d
    inner join V$Declaration decl on decl.id  = v_declId
    left join sov_invoice_all i on i.row_key = v_rowKey and i.chapter = 8
    /*Данные продавца*/
    left join (select sis.seller_inn, 
                      sis.seller_kpp,
                      sis.row_key_id,
                      tpName.Np_Name as seller_name
               from sov_invoice_seller_all sis 
               left join mv$tax_payer_name tpname on tpname.inn = sis.seller_inn
               where sis.row_key_id = v_rowKey 
               and rownum = 1) SellerInfo on 1=1
    /*Данные посредника*/          
    left join mv$tax_payer_name brokerName on brokerName.Inn = i.broker_inn
    /*Данные кодов операций*/ 
    left join (select sio.operation_code
               from sov_invoice_operation_all sio 
               where sio.row_key_id = v_rowKey 
               and rownum = 1) OpCode on 1=1
    /*Данные документа уплаты*/ 
    left join (select sipd.doc_num, sipd.doc_date
               from sov_invoice_payment_doc_all sipd 
               where sipd.row_key_id = v_rowKey 
               and rownum = 1) PayDoc on 1=1
    left join discrepancy_comment dc on dc.discrepancy_id = d.id and dc.chapter = i.chapter and dc.row_key = i.row_key
    where d.ID = pDiscrepancyId;
  end;

  procedure GetSellBooks
  (
    pDiscrepancyId in SOV_Discrepancy.Id%type,
    pSideNumber in number,
    pCursor out sys_refcursor
  ) is
  v_rowKey varchar2(1024 char);
  v_declId number(38);
  begin
    
  select
  (case pSideNumber when 0 then d.DECL_ID when 1 then d.DECL_CONTRACTOR_ID else null end) as declId,
  (case pSideNumber when 0 then d.invoice_rk when 1 then d.invoice_contractor_rk else null end) as rowKey
  into v_declId, v_rowKey
  from sov_discrepancy d where d.id = pDiscrepancyId;
  
    open pCursor for
    select distinct
      d.id,
      d.sov_id as discrepancy_sov_id,
      i.row_key as RowKey,
      i.actual_row_key,
      null as Commented,
      /*break*/
	    i.ordinal_number as OrdinalNumber,
      decl.full_tax_period   as ReportPeriod,
      OpCode.Operation_Code  as OperationCode,
      i.invoice_num     as InvoiceNumber,
      i.invoice_date    as InvoiceDate,
      i.change_num      as FixedInvoiceNumber,
      i.change_date     as FixedInvoiceDate,
      i.correction_num  as CorrectedInvoiceNumber,
      i.correction_date as CorrectedInvoiceDate,
      i.change_correction_num  as FixedCorrectedInvoiceNumber,
      i.change_correction_date as FixedCorrectedInvoiceDate,
      i.okv_code        as CurrencyCode,
      decl.correction_number as CorrectionNumber,
	    /*i.is_dop_list*/ 0 as PriznakDopList,
      /*break*/
      BuyerInfo.Buyer_Inn  as BuyerInfoINN,
      BuyerInfo.Buyer_Kpp  as BuyerInfoKPP,
      BuyerInfo.Buyer_Name  as BuyerInfoName,
      i.broker_inn as IntermediaryInfoINN,
      i.broker_kpp as IntermediaryInfoKPP,
      tBroker.Np_Name as IntermediaryInfoName,
      PayDoc.Doc_Num        as PaymentDocNumber,
      PayDoc.Doc_Date       as PaymentDocDate,
      i.price_sell_18          as TaxableAmount18,
      i.price_sell_10          as TaxableAmount10,
      i.price_sell_0           as TaxableAmount0,
      i.price_nds_18           as TaxAmount18,
      i.price_nds_10           as TaxAmount10,
      i.price_tax_free         as TaxFreeAmount,
      i.price_sell_in_curr     as TotalAmountCurrency,
      i.price_sell             as TotalAmountRouble,
      i.logical_errors         as ErrorCodesString,
      0 as STAGE_CODE,
      '-' as STAGE_NAME,
      ''  as STAGE_STATUS_CODE,
      '-' as STAGE_STATUS_NAME
    from sov_discrepancy d
    inner join V$Declaration decl on decl.id = v_declId
    left join sov_invoice_all i on i.row_key = v_rowKey
    /*Данные покупателя*/
    left join (select sib.buyer_inn, 
                      sib.buyer_kpp,
                      sib.row_key_id,
                      tpName.Np_Name as buyer_name
               from sov_invoice_buyer_all sib 
               left join mv$tax_payer_name tpname on tpname.inn = sib.buyer_inn
               where sib.row_key_id = v_rowKey 
               and rownum = 1) BuyerInfo on 1=1
    left join mv$tax_payer_name tBroker on tBroker.inn = i.broker_inn
    /*Данные кодов операций*/ 
    left join (select sio.operation_code
               from sov_invoice_operation_all sio 
               where sio.row_key_id = v_rowKey 
               and rownum = 1) OpCode on 1=1
    /*Данные документа уплаты*/ 
    left join (select sipd.doc_num, sipd.doc_date
               from sov_invoice_payment_doc_all sipd 
               where sipd.row_key_id = v_rowKey 
               and rownum = 1) PayDoc on 1=1
    where d.ID = pDiscrepancyId and i.chapter = 9;
  end;

  procedure GetSentJournal
  (
    pDiscrepancyId in SOV_Discrepancy.Id%type,
    pSideNumber in number,
    pCursor out sys_refcursor
  ) is
  begin
    open pCursor for
    select distinct
      d.id,
      d.sov_id as discrepancy_sov_id,
      i.row_key as RowKey,
      i.actual_row_key,
      null as Commented,
      /*break*/
	  i.ordinal_number as OrdinalNumber,
      decl.full_tax_period  as ReportPeriod,
      '01' as OperationCode,
      i.invoice_num  as InvoiceNumber,
      i.invoice_date as InvoiceDate,
      i.change_num  as FixedInvoiceNumber,
      i.change_date as FixedInvoiceDate,
      i.correction_num  as CorrectedInvoiceNumber,
      i.correction_date as CorrectedInvoiceDate,
      i.change_correction_num  as FixedCorrectedInvoiceNumber,
      i.change_correction_date as FixedCorrectedInvoiceDate,
      i.okv_code          as CurrencyCode,
      decl.correction_number   as CorrectionNumber,
      /*break*/
      null as BuyerInfoINN,
      null as BuyerInfoKPP,
      null as BuyerInfoName,
      null as SellerInfoINN,
      null as SellerInfoKPP,
      null as SellerInfoName,
      i.create_date as SendDate,
      i.price_total as Amount,
      i.price_nds_total as TaxAmount,
      i.diff_correct_decrease as CostDifferenceDecrease,
      i.diff_correct_increase as CostDifferenceIncrease,
      i.diff_correct_nds_decrease as TaxDifferenceDecrease,
      i.diff_correct_nds_increase as TaxDifferenceIncrease,
      i.logical_errors as ErrorCodesString,
      dStage.STAGE_CODE                 as STAGE_CODE,
      dStage.STAGE_NAME                 as STAGE_NAME,
      dStage.STAGE_STATUS_CODE          as STAGE_STATUS_CODE,
      dStage.STAGE_STATUS_NAME          as STAGE_STATUS_NAME
    from sov_discrepancy d
    inner join V$Declaration decl on decl.id
      = (case pSideNumber when 0 then d.DECL_ID when 1 then d.DECL_CONTRACTOR_ID else null end)
    left join sov_invoice_all i on i.row_key = (case pSideNumber when 0 then d.invoice_rk when 1 then d.invoice_contractor_rk else null end)
    left join v$discrepancy_stage dStage on dStage.Id = d.id
    where d.ID = pDiscrepancyId and i.chapter = 10;
  end;

  procedure GetRecieveJournal
  (
    pDiscrepancyId in SOV_Discrepancy.Id%type,
    pSideNumber in number,
    pCursor out sys_refcursor
  ) is
  begin
    open pCursor for
    select distinct
      d.id,
      d.sov_id as discrepancy_sov_id,
      i.row_key RowKey,
      i.actual_row_key,
      null Commented,
      /*break*/
	  i.ordinal_number as OrdinalNumber,
      decl.full_tax_period as ReportPeriod,
      '01' as OperationCode,
      i.invoice_num as InvoiceNumber,
      i.invoice_date as InvoiceDate,
      i.change_num as FixedInvoiceNumber,
      i.change_date as FixedInvoiceDate,
      i.correction_num as CorrectedInvoiceNumber,
      i.correction_date as CorrectedInvoiceDate,
      i.change_correction_num  as FixedCorrectedInvoiceNumber,
      i.change_correction_date as FixedCorrectedInvoiceDate,
      i.okv_code as CurrencyCode,
      decl.correction_number as CorrectionNumber,
      /*break*/
      i.broker_inn as SubcommissionAgentINN,
      i.broker_kpp as SubcommissionAgentKPP,
      null as SubcommissionAgentName,
      null as SellerInfoINN,
      null as SellerInfoKPP,
      null as SellerInfoName,
      i.deal_kind_code as DealTypeCode,
      i.receive_date as RecieveDate,
      i.price_total as Amount,
      i.price_nds_total as TaxAmount,
      i.diff_correct_decrease as CostDifferenceDecrease,
      i.diff_correct_increase as CostDifferenceIncrease,
      i.diff_correct_nds_decrease as TaxDifferenceDecrease,
      i.diff_correct_nds_increase as TaxDifferenceIncrease,
      i.logical_errors as ErrorCodesString,
      dStage.STAGE_CODE                 as STAGE_CODE,
      dStage.STAGE_NAME                 as STAGE_NAME,
      dStage.STAGE_STATUS_CODE          as STAGE_STATUS_CODE,
      dStage.STAGE_STATUS_NAME          as STAGE_STATUS_NAME
    from sov_discrepancy d
    inner join V$Declaration decl on decl.id
      = (case pSideNumber when 0 then d.DECL_ID when 1 then d.DECL_CONTRACTOR_ID else null end)
    left join sov_invoice_all i on i.row_key = (case pSideNumber when 0 then d.invoice_rk when 1 then d.invoice_contractor_rk else null end)
    left join v$discrepancy_stage dStage on dStage.Id = d.id
    where d.ID = pDiscrepancyId and i.chapter = 11 ;
  end;
end;
/
