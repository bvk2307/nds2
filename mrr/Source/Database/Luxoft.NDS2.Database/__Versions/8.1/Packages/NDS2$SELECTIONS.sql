﻿create or replace package NDS2_MRR_USER.Nds2$Selections as
TYPE T_ARR_NUM IS TABLE OF NUMBER(19) INDEX BY PLS_INTEGER;

function GenerateUniqueName
(
  p_user_sid varchar2
) return varchar2;

function VERIFY_SELECTION_NAME
(
  p_user_sid varchar2,
  p_selection_name varchar2,
  p_selection_id number
) return varchar2;

PROCEDURE SAVE_SELECTION
(
  p_selectionId IN OUT selection.id%type,
  p_name IN selection.name%type,
  p_analytic IN selection.analytic%type,
  p_analytic_sid IN selection.analytic_sid%type,
  p_chief IN selection.chief%type,
  p_chief_sid IN selection.chief_sid%type,
  p_sender IN selection.sender%type,
  p_status IN selection.status%type,
  p_status_date IN selection.status_date%type,
  p_creation_date in selection.creation_date%type,
  p_modification_date in selection.modification_date%type,
  p_comment in action_history.action_comment%type,
  p_save_reason in number,/*1 - create, 2 - remove, 3 - status change, 4 - update*/
  p_invoice_request_id in number
);

PROCEDURE                   REMOVE_SELECTION
(pId IN NUMBER, analytic IN VARCHAR2);

PROCEDURE          SELECTION_SET_STATE
(pId IN NUMBER, statusId IN NUMBER, userName in varchar2);

PROCEDURE          SELECT_SELECTION
(selectionCursor OUT SYS_REFCURSOR);

PROCEDURE          SELECT_SELECTION_BY_NAME
(selectionStartName VARCHAR2, selectionCursor OUT SYS_REFCURSOR);

PROCEDURE GET_SELECTION_STATUS
(selectionId IN NUMBER, statusCursor OUT SYS_REFCURSOR);

PROCEDURE GET_FAVORITE_FILTERS (pAnalyst IN FAVORITE_FILTERS.ANALYST%type, pFavCursor OUT SYS_REFCURSOR);

PROCEDURE SAVE_FAVORITE_FILTER (
  pId IN OUT FAVORITE_FILTERS.ID%type,
  pName IN FAVORITE_FILTERS.NAME%type,
  pAnalyst IN FAVORITE_FILTERS.ANALYST%type,
  pFilter IN FAVORITE_FILTERS.FILTER%type);

PROCEDURE DELETE_FAVORITE_FILTER (pId IN FAVORITE_FILTERS.ID%type);

PROCEDURE UPDATE_DECLARATION_CHECK_STATE (
  pSelectionId IN SELECTION.ID%type,
  pDeclarationId IN SOV_DECLARATION_INFO.ID%type,
  pCheckState IN SELECTION_DECLARATION.IS_IN_PROCESS%type);

PROCEDURE UPDATE_DISCREPANCY_CHECK_STATE (
  pSelectionId IN SELECTION.ID%type,
  pDiscrepancyId IN SOV_DISCREPANCY.ID%type,
  pCheckState IN SELECTION_DISCREPANCY.IS_IN_PROCESS%type,
  pStatus OUT NUMBER
  );

PROCEDURE ALL_DECLARATIONS_CHECK_STATE (
  pSelectionId IN SELECTION.ID%type,
  pCheckState IN SELECTION_DECLARATION.IS_IN_PROCESS%type
  );

PROCEDURE ALL_DISCREPANCIES_CHECK_STATE (
  pSelectionId IN SELECTION.ID%type,
  pCheckState IN SELECTION_DISCREPANCY.IS_IN_PROCESS%type,
  pStatus OUT NUMBER
  );

PROCEDURE SYNC_DECLARATIONS_CHECK_STATE (
  pSelectionId IN SELECTION.ID%type,
  pTargetCheckState IN SELECTION_DECLARATION.IS_IN_PROCESS%type
  );

PROCEDURE COUNT_CHECKED_DECLARATIONS (
  pSelectionId IN SELECTION.ID%type,
  pChecked OUT NUMBER,
  pAll OUT NUMBER
  );

PROCEDURE COUNT_CHECKED_DISCREPANCIES (
  pSelectionId IN SELECTION.ID%type,
  pDisabled OUT NUMBER,
  pChecked OUT NUMBER,
  pAll OUT NUMBER,
  pInWorked OUT NUMBER
  );

procedure MERGE_SENDED_SELECTIONS;
/*
ведение истории смены состояний выборки
*/
PROCEDURE UPD_HIST_SELECTION_STATUS
(
  p_id in selection.id%type,
  p_status in selection.status%type
);

/*
загрузка переходов состояний
*/
PROCEDURE GET_SELECTION_TRANSITIONS
(pFavCursor OUT SYS_REFCURSOR);

PROCEDURE INSERT_ACTION_HISTORY (
    pSelectionId IN ACTION_HISTORY.CD%type,
    pComment IN ACTION_HISTORY.ACTION_COMMENT%type,
    pUser IN ACTION_HISTORY.USER_NAME%type,
    pAction IN ACTION_HISTORY.ACTION%type
);

PROCEDURE GET_DISCREPANCY (
  pDiscrepancyId IN SELECTION_DISCREPANCY.DISCREPANCY_ID%type,
  pSelectionId IN SELECTION.ID%type,
  pCursor OUT SYS_REFCURSOR
  );

PROCEDURE P$GET_REGIONS (
  pUserSID IN METODOLOG_CHECK_EMPLOYEE.SID%type, -- Идентификатор пользователя
  pData OUT SYS_REFCURSOR -- Список регионов
  ); -- Возвращает список регионов, доступных пользователю (для фильтра выборки)

PROCEDURE P$SEARCH_REGIONS (
  pUserSID IN USER_TO_N_SSRF.USER_SID%type, -- Идентификатор пользователя
  pSearchKey IN VARCHAR2,
  pMaxQuantity IN NUMBER,
  pData OUT SYS_REFCURSOR -- Список регионов
  );

PROCEDURE P$GET_INSPECTIONS (
  pUserSID IN METODOLOG_CHECK_EMPLOYEE.SID%type, -- Идентификатор пользователя
  pData OUT SYS_REFCURSOR -- Список инспекций
  ); -- Возвращает список инспекций, доступных пользователю (для фильтра выборки)

PROCEDURE P$SEARCH_INSPECTIONS (
  pUserSID IN USER_TO_N_SSRF.USER_SID%type,
  pSearchKey IN VARCHAR2,
  pMaxQuantity IN NUMBER,
  pData OUT SYS_REFCURSOR
  );

PROCEDURE P$UPD_DISCREPANCY_SENT_STATUS; -- Помечает расхождения как отправленные, если существует хотя бы 1 расхождение в статусе "Отправлено" на ту же с/ф и исключает их из всех выборок

procedure P$START_CANCEL_SELECTION;
procedure P$DO_CANCEL_SELECTION;

procedure P$REMOVE_SELECTION_RESULTS
(
  p_selection_id IN SELECTION.ID%type
);
procedure P$LOAD_SELECTION ( -- Загружает детализированные данные выборки 
  pId IN SELECTION.ID%type,
  pCursor OUT SYS_REFCURSOR
  );

procedure P$FILL_SELECTION_DECLARATION
(
  p_selection_id IN SELECTION.ID%type
);


end Nds2$Selections;
/
create or replace package body NDS2_MRR_USER.Nds2$Selections as

function GenerateUniqueName
(
  p_user_sid varchar2
) return varchar2
as
v_name varchar2(128);
v_pattern varchar2(32) := 'Выборка ';
v_counter number(10) := 1;
begin
 v_name := v_pattern || v_counter;
 for sel in
   (SELECT distinct name, TO_NUMBER(REGEXP_SUBSTR(selection.name, '[[:digit:]]+')) x FROM selection
      WHERE lower(analytic_sid) = lower(p_user_sid) and regexp_like(selection.name, v_pattern) order by x asc) --(select name from selections where analytic = p_user_name order by name asc)
 loop
  if TO_CHAR(sel.name) = v_name then
    v_counter := v_counter + 1;
    v_name := v_pattern || v_counter;
  end if;
 end loop;
 return v_name;
 exception
   when no_data_found then
     return rawtohex(sys_guid());
end;

function VERIFY_SELECTION_NAME
(
  p_user_sid varchar2,
  p_selection_name varchar2,
  p_selection_id number
) return varchar2
as
v_idx number := 0;
v_exist number;
v_name varchar2(1024);
begin
v_name := trim(p_selection_name);
select count(1) into v_exist from selection where lower(analytic_sid) = lower(p_user_sid) and name = v_name and id <> p_selection_id;
dbms_output.put_line(v_exist);
while v_exist >= 1 and v_idx < 10000 loop
   v_idx := v_idx+1;
   v_name := trim(p_selection_name)||' ('||v_idx||')';
   select count(1) into v_exist from selection where lower(analytic_sid) = lower(p_user_sid) and name = v_name and id <> p_selection_id;
end loop;

return v_name;
end;

PROCEDURE SAVE_SELECTION
(
  p_selectionId IN OUT selection.id%type,
  p_name IN selection.name%type,
  p_analytic IN selection.analytic%type,
  p_analytic_sid IN selection.analytic_sid%type,
  p_chief IN selection.chief%type,
  p_chief_sid IN selection.chief_sid%type,
  p_sender IN selection.sender%type,
  p_status IN selection.status%type,
  p_status_date IN selection.status_date%type,
  p_creation_date in selection.creation_date%type,
  p_modification_date in selection.modification_date%type,
  p_comment in action_history.action_comment%type,
  p_save_reason in number,/*1 - create, 2 - remove, 3 - status change, 4 - update*/
  p_invoice_request_id in number
)
as
begin

update SELECTION set
   name = p_name,
   analytic = p_analytic,
   analytic_sid = p_analytic_sid,
   chief = p_chief,
   sender = p_sender,
   status = p_status,
   modification_date = p_modification_date,
   status_date = p_status_date,
   invoice_request_id = p_invoice_request_id
where id = p_selectionId;

if sql%rowcount = 0 then
  begin
    p_selectionId := SELECTIONS_SEQ.NEXTVAL;

    insert into selection(id, name, creation_date, modification_date, status_date, analytic, analytic_sid, chief, chief_sid, sender, status, invoice_request_id, type)
    values(p_selectionId, p_name, p_creation_date, null, null, p_analytic, p_analytic_sid, p_chief, p_chief_sid, p_sender, p_status,p_invoice_request_id, 0);

    insert into Action_History (id, cd, status, change_date, action_comment, user_name, action) values(SEQ_ACTION_HISTORY.nextval, p_selectionId, p_status, sysdate, p_comment, p_analytic, 1);
    
    insert into SELECTION_FILTER (id, selection_id) values (p_selectionId, p_selectionId);
  end;
else
  begin
    insert into Action_History (id, cd, status, change_date, action_comment, user_name, action) values(SEQ_ACTION_HISTORY.nextval, p_selectionId, p_status, sysdate, p_comment, p_analytic, 4);
  end;
end if;

if p_save_reason = 3 then
  UPD_HIST_SELECTION_STATUS(p_selectionId, p_status);
end if;

end;

PROCEDURE REMOVE_SELECTION
(pId IN NUMBER, analytic IN VARCHAR2)
AS
c1 number;
c2 number;
BEGIN
  select ID into c1 FROM DICT_SELECTION_STATUS WHERE NAME='Удалено';
  select ID into c2 from DICT_ACTIONS where ACTION_NAME='Удаление выборки';
  UPDATE SELECTION SET STATUS = c1 WHERE ID = pId;
  insert into Action_History values(SEQ_ACTION_HISTORY.nextval, pId, c1, sysdate, '', analytic, c2);
  UPD_HIST_SELECTION_STATUS(pId, c1);
END;


PROCEDURE SELECTION_SET_STATE
(pId IN NUMBER, statusId IN NUMBER, userName in varchar2)
AS
c1 number;
BEGIN
  UPDATE SELECTION SET STATUS = statusId WHERE ID = pId;
  select ID into c1 from DICT_ACTIONS where ACTION_NAME='Изменение статуса';
  insert into Action_History values(SEQ_ACTION_HISTORY.nextval, pId, statusId, sysdate, '', userName, c1);
  UPD_HIST_SELECTION_STATUS(pId, statusId);
END;




PROCEDURE SELECT_SELECTION
(selectionCursor OUT SYS_REFCURSOR) AS
BEGIN
  OPEN selectionCursor FOR
  SELECT
    SELECTION.ID,
    SELECTION.ANALYTIC,
    SELECTION.CHIEF,
    SELECTION.CREATION_DATE,
    SELECTION.NAME,
    SS.NAME
    FROM SELECTION
    LEFT JOIN DICT_SELECTION_STATUS SS ON SELECTION.STATUS = SS.ID;
END;

PROCEDURE SELECT_SELECTION_BY_NAME
(selectionStartName VARCHAR2, selectionCursor OUT SYS_REFCURSOR) AS
BEGIN
  OPEN selectionCursor FOR
  SELECT
    SELECTION.ID,
    SELECTION.ANALYTIC,
    SELECTION.CHIEF,
    SELECTION.CREATION_DATE,
    SELECTION.NAME,
    SS.NAME
    FROM SELECTION
    LEFT JOIN DICT_SELECTION_STATUS SS ON SELECTION.STATUS = SS.ID
    WHERE SELECTION.NAME LIKE selectionStartname;
END;

PROCEDURE GET_SELECTION_STATUS
(selectionId IN NUMBER, statusCursor OUT SYS_REFCURSOR)
AS
BEGIN
 OPEN statusCursor FOR
 SELECT STATUS FROM SELECTION WHERE ID = selectionId;
END;

PROCEDURE GET_FAVORITE_FILTERS (pAnalyst IN FAVORITE_FILTERS.ANALYST%type, pFavCursor OUT SYS_REFCURSOR)
AS
BEGIN
  OPEN pFavCursor FOR SELECT ID,NAME,ANALYST,FILTER FROM FAVORITE_FILTERS WHERE ANALYST = pAnalyst;
END;

PROCEDURE SAVE_FAVORITE_FILTER (
  pId IN OUT FAVORITE_FILTERS.ID%type,
  pName IN FAVORITE_FILTERS.NAME%type,
  pAnalyst IN FAVORITE_FILTERS.ANALYST%type,
  pFilter IN FAVORITE_FILTERS.FILTER%type)
AS
BEGIN
  IF pId IS NULL THEN
    BEGIN
      pId := SEQ_FAV_FILTER.NEXTVAL;
      INSERT INTO FAVORITE_FILTERS (ID,NAME,ANALYST,FILTER) VALUES (pId,pName,pAnalyst,pFilter);
    END;
  ELSE
    BEGIN
      UPDATE FAVORITE_FILTERS
      SET
             FILTER = pFilter
      WHERE
             ID = pId;
    END;
   END IF;
END;

PROCEDURE DELETE_FAVORITE_FILTER (pId IN FAVORITE_FILTERS.ID%type)
AS
BEGIN
  DELETE FROM FAVORITE_FILTERS WHERE ID = pId;
END;

PROCEDURE UPDATE_DECLARATION_CHECK_STATE (
  pSelectionId IN SELECTION.ID%type,
  pDeclarationId IN SOV_DECLARATION_INFO.ID%type,
  pCheckState IN SELECTION_DECLARATION.IS_IN_PROCESS%type)
AS
BEGIN
  UPDATE SELECTION_DISCREPANCY
  SET IS_IN_PROCESS = pCheckState
  WHERE
         SELECTION_ID = pSelectionId
     AND EXISTS (
         SELECT 1
         FROM SOV_DISCREPANCY x
     left join v$discrepancy_stage dStage on dStage.Id = x.id
         WHERE
                   x.DECL_ID = pDeclarationId
              AND (x.ID = SELECTION_DISCREPANCY.DISCREPANCY_ID)
              AND (dStage.STAGE_CODE = 1)
              AND (pCheckState = 0 OR dStage.STAGE_STATUS_CODE < 3) -- В случае если расхождение надо включить вместе с декларацией, проверяем, что оно еще не согласовано в другой выборке
          );

  UPDATE SELECTION_DECLARATION
  SET IS_IN_PROCESS = pCheckState
  WHERE
       SELECTION_ID = pSelectionId
   AND DECLARATION_ID = pDeclarationId;
END;

PROCEDURE UPDATE_DISCREPANCY_CHECK_STATE (
  pSelectionId IN SELECTION.ID%type,
  pDiscrepancyId IN SOV_DISCREPANCY.ID%type,
  pCheckState IN SELECTION_DISCREPANCY.IS_IN_PROCESS%type,
  pStatus OUT NUMBER
  )
AS
BEGIN

  SELECT
          CASE WHEN dStage.STAGE_STATUS_CODE < 3 THEN 0 ELSE 1 END
  INTO pStatus
  FROM SOV_DISCREPANCY t
  left join v$discrepancy_stage dStage on dStage.Id = t.id and dStage.selection_id = pSelectionId
  WHERE t.ID = pDiscrepancyId
    AND (dStage.STAGE_CODE = 1);

  IF pStatus = 0 THEN
  BEGIN

     IF pCheckState = 0 THEN
     BEGIN
        UPDATE SELECTION_DECLARATION t_decl_ref
        SET IS_IN_PROCESS = 0
        WHERE
                  SELECTION_ID = pSelectionId
                  AND NOT EXISTS(
                             SELECT 1
                                FROM
                                   SELECTION_DISCREPANCY xRef
                                   JOIN SOV_DISCREPANCY x
                                          ON
                                              x.ID = xRef.DISCREPANCY_ID
                                          AND xRef.IS_IN_PROCESS = 1
                                   WHERE
                                          x.DECL_ID = t_decl_ref.DECLARATION_ID
                                      AND xRef.SELECTION_ID = pSelectionId
                                      AND xRef.DISCREPANCY_ID <> pDiscrepancyId
                           );
     END;
     ELSE
     BEGIN
         UPDATE SELECTION_DECLARATION t_decl_ref
         SET IS_IN_PROCESS = 1
         WHERE
                 t_decl_ref.SELECTION_ID = pSelectionId
             AND EXISTS(
                        SELECT 1
                        FROM
                                SELECTION_DISCREPANCY xRef
                                JOIN SOV_DISCREPANCY x ON x.ID = xRef.DISCREPANCY_ID
                        WHERE     x.DECL_ID = t_decl_ref.DECLARATION_ID
                              AND xRef.DISCREPANCY_ID = pDiscrepancyId
                        );
    END;
    END IF;

    UPDATE SELECTION_DISCREPANCY
    SET IS_IN_PROCESS = pCheckState
    WHERE
            SELECTION_ID = pSelectionId
        AND DISCREPANCY_ID = pDiscrepancyId;
    END;
END IF;
END;

PROCEDURE ALL_DECLARATIONS_CHECK_STATE (
  pSelectionId IN SELECTION.ID%type,
  pCheckState IN SELECTION_DECLARATION.IS_IN_PROCESS%type)
AS
BEGIN

  UPDATE SELECTION_DISCREPANCY main
  SET IS_IN_PROCESS = pCheckState
  WHERE
            main.SELECTION_ID = pSelectionId
        AND EXISTS(
            SELECT 1
            FROM
                   SOV_DISCREPANCY xDisc
                   JOIN SELECTION_DECLARATION xDecl_ref
                        ON xDisc.DECL_ID = xDecl_ref.DECLARATION_ID
                       AND xDecl_ref.Selection_ID = pSelectionId
                       AND xDecl_ref.IS_IN_PROCESS != pCheckState
              left join v$discrepancy_stage dStage on dStage.Id = xDisc.id
                   WHERE
                       xDisc.ID = main.DISCREPANCY_ID
             AND (dStage.STAGE_CODE = 1)
             AND (pCheckState = 0 OR dStage.STAGE_STATUS_CODE < 3)
        );

   UPDATE SELECTION_DECLARATION SET IS_IN_PROCESS = pCheckState WHERE SELECTION_ID = pSelectionId;
END;

PROCEDURE ALL_DISCREPANCIES_CHECK_STATE (
  pSelectionId IN SELECTION.ID%type,
  pCheckState IN SELECTION_DISCREPANCY.IS_IN_PROCESS%type,
  pStatus OUT NUMBER
  )
AS
BEGIN

   SELECT SUM(CASE WHEN pCheckState=1 AND dStage.STAGE_STATUS_CODE>=3 THEN 1 ELSE 0 END)
   INTO pStatus
   FROM
             SOV_DISCREPANCY t
        JOIN SELECTION_DISCREPANCY t_ref ON t.ID=t_ref.DISCREPANCY_ID
    left join v$discrepancy_stage dStage on dStage.Id = t.id
   WHERE
            t_ref.SELECTION_ID = pSelectionId
      AND (dStage.STAGE_CODE = 1);

   UPDATE SELECTION_DECLARATION t_decl_ref
   SET IS_IN_PROCESS = pCheckState
   WHERE  t_decl_ref.SELECTION_ID=pSelectionId
      AND EXISTS(
          SELECT 1
          FROM
           SELECTION_DISCREPANCY t_disc_ref
           JOIN SOV_DISCREPANCY t_disc ON t_disc.ID=t_disc_ref.DISCREPANCY_ID
       left join v$discrepancy_stage dStage on dStage.Id = t_disc.id
          WHERE
                    t_disc.DECL_ID=t_decl_ref.DECLARATION_ID
                AND t_disc_ref.SELECTION_ID=pSelectionId
        AND (dStage.STAGE_CODE = 1)
        AND (pCheckState = 0 OR dStage.STAGE_STATUS_CODE < 3)
      );

   UPDATE SELECTION_DISCREPANCY t_ref
   SET IS_IN_PROCESS = pCheckState
   WHERE     t_ref.SELECTION_ID=pSelectionId
         AND EXISTS(
             SELECT 1
             FROM SOV_DISCREPANCY t
       left join v$discrepancy_stage dStage on dStage.Id = t.id
             WHERE  t.ID=t_ref.DISCREPANCY_ID
        AND (dStage.STAGE_CODE = 1)
        AND (pCheckState = 0 OR dStage.STAGE_STATUS_CODE < 3)
         );
END;

PROCEDURE SYNC_DECLARATIONS_CHECK_STATE (
  pSelectionId IN SELECTION.ID%type,
  pTargetCheckState IN SELECTION_DECLARATION.IS_IN_PROCESS%type
  )
AS
BEGIN
  IF pTargetCheckState = 0 THEN
     BEGIN
       UPDATE SELECTION_DECLARATION main
       SET IS_IN_PROCESS = 0
       WHERE main.SELECTION_ID = pSelectionId
             AND main.IS_IN_PROCESS = 1
             AND NOT EXISTS (
                 SELECT 1
                 FROM
                        SELECTION_DISCREPANCY xRef
                        JOIN SOV_DISCREPANCY x ON x.ID = xRef.DISCREPANCY_ID
                 WHERE xRef.SELECTION_ID = pSelectionId
                       AND x.DECL_ID = main.Declaration_Id
                       AND xRef.IS_IN_PROCESS = 1
             );
     END;
  ELSE
     BEGIN
       UPDATE SELECTION_DECLARATION main
       SET IS_IN_PROCESS = 1
       WHERE main.SELECTION_ID = pSelectionId
             AND main.IS_IN_PROCESS = 0
             AND EXISTS (
                 SELECT 1
                 FROM
                        SELECTION_DISCREPANCY xRef
                        JOIN SOV_DISCREPANCY x ON x.ID = xRef.DISCREPANCY_ID
                 WHERE xRef.SELECTION_ID = pSelectionId
                       AND x.DECL_ID = main.Declaration_Id
                       AND xRef.IS_IN_PROCESS = 1
             );
     END;
  END IF;
END;

PROCEDURE COUNT_CHECKED_DECLARATIONS (
  pSelectionId IN SELECTION.ID%type,
  pChecked OUT NUMBER,
  pAll OUT NUMBER
  )
AS
BEGIN
  SELECT
       SUM(ISCHECKED),
       COUNT(ID)
  INTO pChecked, pAll
  FROM V$SelectionDeclaration
  WHERE SELECTION_ID = pSelectionId;
END;

PROCEDURE COUNT_CHECKED_DISCREPANCIES (
  pSelectionId IN SELECTION.ID%type,
  pDisabled OUT NUMBER,
  pChecked OUT NUMBER,
  pAll OUT NUMBER,
  pInWorked OUT NUMBER
  )
AS
BEGIN
  SELECT
       SUM(CASE WHEN (STAGE_STATUS_CODE >= 3) and (STAGE_CODE = 1) THEN 1 ELSE 0 END),
       SUM(ISCHECKED),
       COUNT(DISCREPANCYID),
       SUM(CASE WHEN (STAGE_STATUS_CODE in (1,2,3)) and (ischecked = 1) THEN 1 ELSE 0 END)
  INTO pDisabled, pChecked, pAll, pInWorked
  FROM
  (
    select
      dis.id                            as DiscrepancyId,
      dis_ref.IS_IN_PROCESS             as ischecked,
          dis.stage     as STAGE_CODE,
      case dis.stage
      when 1 then docStage1.status
      when 2 then docStage2.status
      when 3 then docStage3.status
      when 4 then docStage4.status
      when 5 then docStage5.status
      when 6 then null
      else null
      end as STAGE_STATUS_CODE
    from SELECTION_DISCREPANCY dis_ref
    inner join SOV_DISCREPANCY dis on dis.id = dis_ref.discrepancy_id
        left join (
          select base.dis_id as dis_id, selstat.id as status, selstat.name as DESCRIPTION
          from(
            select dis_ref.discrepancy_id as dis_id, MAX(
                    case dis_ref.is_in_process
                      when 1 then selstat.rank
                      else 50
                    end) as max_rank
            from selection sel
                   inner join SELECTION_DISCREPANCY dis_ref on sel.Id = dis_ref.selection_id
                   inner join DICT_SELECTION_STATUS selstat on selstat.id = sel.status
            where dis_ref.discrepancy_id in (select discrepancy_id from selection_discrepancy where selection_id = pSelectionId)
                  and selstat.id not in (5, 7)
            group by dis_ref.discrepancy_id
          ) base
            inner join DICT_SELECTION_STATUS selstat on selstat.rank = base.MAX_RANK
        ) docStage1 on docStage1.dis_id = dis.id
        left join DOC_INVOICE docinv on docinv.invoice_row_key = dis.invoice_rk
             left join DOC docStage2 on (docStage2.doc_id = docinv.doc_id) and (docStage2.stage = 2)
             left join DOC_STATUS docstatStage2 on (docstatStage2.id = docStage2.status) and (docstatStage2.doc_type = docStage2.doc_type)
             left join DOC docStage3 on (docStage3.doc_id = docinv.doc_id) and (docStage3.stage = 3)
             left join DOC_STATUS docstatStage3 on (docstatStage3.id = docStage3.status) and (docstatStage3.doc_type = docStage3.doc_type)
             left join DOC docStage4 on (docStage4.doc_id = docinv.doc_id) and (docStage4.stage = 4)
             left join DOC_STATUS docstatStage4 on (docstatStage4.id = docStage4.status) and (docstatStage4.doc_type = docStage4.doc_type)
             left join DOC docStage5 on (docStage5.doc_id = docinv.doc_id) and (docStage5.stage = 5)
             left join DOC_STATUS docstatStage5 on (docstatStage5.id = docStage5.status) and (docstatStage5.doc_type = docStage5.doc_type)
    where dis_ref.selection_id = pSelectionId
  );

END;


/*
ведение истории смены состояний выборки
*/
PROCEDURE UPD_HIST_SELECTION_STATUS
(
  p_id in selection.id%type,
  p_status in selection.status%type
)
as
begin

  insert into HIST_SELECTION_STATUS (ID, DT, VAL)
         values(p_id, sysdate, p_status);

end;


/*
загрузка переходов состояний
*/
PROCEDURE GET_SELECTION_TRANSITIONS
(pFavCursor OUT SYS_REFCURSOR)
AS
BEGIN
  OPEN pFavCursor FOR
  SELECT STATE_FROM, STATE_TO, OPERATION FROM SELECTION_TRANSITION;
END;

PROCEDURE INSERT_ACTION_HISTORY (
    pSelectionId IN ACTION_HISTORY.CD%type,
    pComment IN ACTION_HISTORY.ACTION_COMMENT%type,
    pUser IN ACTION_HISTORY.USER_NAME%type,
    pAction IN ACTION_HISTORY.ACTION%type
)
AS
BEGIN

    INSERT INTO ACTION_HISTORY(ID,CD,STATUS,CHANGE_DATE,ACTION_COMMENT,USER_NAME,ACTION)
           SELECT SEQ_ACTION_HISTORY.nextval, t.ID, t.STATUS, Sysdate, pComment, pUser, pAction
           FROM SELECTION t
           WHERE t.ID = pSelectionId;

END;

PROCEDURE GET_DISCREPANCY (
  pDiscrepancyId IN SELECTION_DISCREPANCY.DISCREPANCY_ID%type,
  pSelectionId IN SELECTION.ID%type,
  pCursor OUT SYS_REFCURSOR
  )
AS
BEGIN
  OPEN pCursor FOR SELECT * FROM V$SELECTION_DISCREPANCY WHERE DISCREPANCYID = pDiscrepancyId AND selection_id = pSelectionId;
END;

PROCEDURE P$GET_REGIONS (
  pUserSID IN METODOLOG_CHECK_EMPLOYEE.SID%type, -- Идентификатор пользователя
  pData OUT SYS_REFCURSOR -- Список регионов
)
AS
BEGIN
  OPEN pData FOR
  SELECT distinct reg.S_CODE, reg.S_NAME, reg.DESCRIPTION
  FROM V$SSRF reg
    LEFT OUTER JOIN METODOLOG_CHECK_EMPLOYEE mce ON mce.Sid = pUserSID
    LEFT OUTER JOIN Metodolog_Region rr ON rr.id = mce.resp_id AND reg.s_code = rr.code
  WHERE (EXISTS(SELECT 1 FROM CONFIGURATION WHERE parameter='restrict_by_region' and value = 'N') OR rr.id IS NOT NULL);
END;

PROCEDURE P$SEARCH_REGIONS (
  pUserSID IN USER_TO_N_SSRF.USER_SID%type, -- Идентификатор пользователя
  pSearchKey IN VARCHAR2,
  pMaxQuantity IN NUMBER,
  pData OUT SYS_REFCURSOR -- Список регионов
)
AS
BEGIN
  OPEN pData FOR
  WITH q AS (
       SELECT ROWNUM as IDX, v.*
       FROM (
              SELECT DISTINCT reg.S_CODE, reg.S_NAME
              FROM
                     V$SSRF reg
                     LEFT OUTER JOIN METODOLOG_CHECK_EMPLOYEE mce ON mce.Sid = pUserSID
                     LEFT OUTER JOIN Metodolog_Region rr ON rr.id = mce.resp_id AND reg.s_code = rr.code
              WHERE
                     (EXISTS(SELECT 1 FROM CONFIGURATION WHERE parameter='restrict_by_region' and value = 'N') OR rr.id IS NOT NULL)
                     AND (reg.S_CODE LIKE pSearchKey OR reg.S_NAME LIKE pSearchKey)
              ORDER BY reg.s_name
            ) v)
    SELECT DISTINCT vw.S_CODE, vw.S_NAME, vw.DESCRIPTION
    FROM
            V$SSRF vw
            JOIN q ON q.S_CODE=vw.S_CODE
    WHERE q.IDX <= pMaxQuantity;
END;



PROCEDURE P$GET_INSPECTIONS (
  pUserSID IN METODOLOG_CHECK_EMPLOYEE.SID%type, -- Идентификатор пользователя
  pData OUT SYS_REFCURSOR -- Список инспекций
  ) -- Возвращает список инспекций, доступных пользователю (для фильтра выборки)
AS
BEGIN
  OPEN pData FOR
  SELECT distinct ins.s_code, ins.s_parent_code, ins.s_name
  FROM v$inspection ins
    LEFT OUTER JOIN METODOLOG_CHECK_EMPLOYEE mce ON mce.Sid = pUserSID
    LEFT OUTER JOIN Metodolog_Region_Inspection mci ON mci.Region_Id = mce.resp_id AND mci.code = ins.s_code
  WHERE (EXISTS(SELECT 1 FROM CONFIGURATION WHERE parameter='restrict_by_region' and value = 'N') OR mci.Region_Id IS NOT NULL);
END;

PROCEDURE P$SEARCH_INSPECTIONS (
  pUserSID IN USER_TO_N_SSRF.USER_SID%type, -- Идентификатор пользователя
  pSearchKey IN VARCHAR2,
  pMaxQuantity IN NUMBER,
  pData OUT SYS_REFCURSOR -- Список инспекций
  ) -- Возвращает список инспекций, доступных пользователю (для фильтра выборки)
AS
BEGIN
  OPEN pData FOR
  WITH q AS (
       SELECT ROWNUM as IDX, v.*
       FROM (
            SELECT distinct ins.s_code, ins.s_parent_code, ins.s_name
            FROM
                   v$inspection ins
                   LEFT OUTER JOIN METODOLOG_CHECK_EMPLOYEE mce ON mce.Sid = pUserSID
                   LEFT OUTER JOIN Metodolog_Region_Inspection mci ON mci.Region_Id = mce.resp_id AND mci.code = ins.s_code
            WHERE
                   (EXISTS(SELECT 1 FROM CONFIGURATION WHERE parameter='restrict_by_region' and value = 'N') OR mci.Region_Id IS NOT NULL) AND
                   (ins.s_code LIKE pSearchKey OR ins.s_name LIKE pSearchKey)
            ORDER BY ins.s_name) v)
   SELECT DISTINCT ins.s_code, ins.s_parent_code, ins.s_name
   FROM v$inspection ins JOIN q ON q.s_code=ins.s_code
   WHERE q.idx <= pMaxQuantity;
END;

PROCEDURE P$UPD_DISCREPANCY_SENT_STATUS -- Помечает расхождения как отправленные, если существует хотя бы 1 расхождение в статусе "Отправлено" на ту же с/ф и исключает их из всех выборок
AS
          InvoiceRequestId long;
BEGIN

  SELECT SEQ_SOV_REQUEST.NEXTVAL INTO InvoiceRequestId FROM DUAL;

  INSERT INTO INVOICE_REQUEST_DATA (REQUEST_ID, chapter, INVOICE_RK)
  WITH tInvoice as
  (
    SELECT
      InvoiceRequestId,
      d.invoice_chapter,
      d.invoice_rk,
      d.invoice_contractor_chapter,
      d.invoice_contractor_rk
    FROM SELECTION s
    INNER JOIN SELECTION_DISCREPANCY sd ON sd.selection_ID = s.id AND sd.Is_In_Process = 1
    INNER JOIN SOV_DISCREPANCY d ON d.id = sd.discrepancy_id
    WHERE nvl(s.Invoice_Request_Id, 0) = 0 AND s.STATUS = 4
  )
  select
    InvoiceRequestId,
    invoice_chapter,
    invoice_rk
  from tInvoice
  union
  select
    InvoiceRequestId,
    invoice_contractor_chapter,
    invoice_contractor_rk
  from tInvoice
  where invoice_contractor_rk is not null;

  UPDATE SELECTION
  SET
  INVOICE_REQUEST_ID = InvoiceRequestId
  WHERE
  STATUS = 4
  AND INVOICE_REQUEST_ID = 0;


  INSERT INTO INVOICE_REQUEST (id, status, REQUESTDATE, LOAD_ONLY_TARGETS, LOAD_MISMATCHES) VALUES (InvoiceRequestId, 4, sysdate, 1, 0);

END;

procedure P$START_CANCEL_SELECTION
  as
begin
  NDS2$SYS.LOG_INFO(
  P_SITE => 'NDS2$REPORTS.START_CANCEL_SELECTION',   P_ENTITY_ID => null,
  P_MESSAGE_CODE => SQLCODE,   P_MESSAGE => 'starting');
  begin
    P$DO_CANCEL_SELECTION;
  exception when others then null;
  end;
end;

procedure P$DO_CANCEL_SELECTION
as
  CURSOR selections IS
  SELECT id FROM SELECTION WHERE STATUS in (1, 2, 3);
  v_selections selections%ROWTYPE;
  pCountAll number(19);
  pCountClose number(19);
begin

  OPEN selections;
  FETCH selections INTO v_selections;
  LOOP

    pCountClose := 0;
    pCountAll := 0;
    select a.CountClose, a.CountAll into pCountClose, pCountAll from dual
    left outer join
    (
      select
         count(case when dis.status = 2 then dis.id end) as CountClose
        ,count(dis.id) as CountAll
      from sov_discrepancy dis
      where dis.id in
      (
        select sd.discrepancy_id from SELECTION_DISCREPANCY sd where sd.selection_id = v_selections.id
        and sd.IS_IN_PROCESS != 0
      )
    ) a on 1 = 1;

    if pCountAll > 0 and pCountClose = pCountAll then
    begin
       update SELECTION s set status = 12, status_date = SYS_EXTRACT_UTC(SYSTIMESTAMP)
       where s.id = v_selections.id and s.status in (1, 2, 3);
       commit;
    end;
    end if;

    FETCH selections INTO v_selections;
    EXIT WHEN selections%NOTFOUND;
  END LOOP;

  CLOSE selections;

end;

procedure MERGE_SENDED_SELECTIONS
as
  error_sc  number := 0;
begin
  for row in
  (
    select sel.invoice_request_id as request_id
    from selection sel
    inner join invoice_request req on req.id = sel.invoice_request_id
    where sel.status = 4
      and req.status = 2
  )
  loop

  -- проверка полноты отгрузки СФ по расхожнениям
  select count(*) into error_sc
  from 
  (
    select case when agg.invoice_row_key is not null then agg.invoice_row_key else rqd.invoice_rk end as key
    from INVOICE_REQUEST_DATA rqd
      left join SOV_INVOICE_AGGREGATE agg on agg.aggregate_row_key = rqd.invoice_rk
    where request_id = row.request_id
  ) ch
  left join SOV_INVOICE si on ch.key = si.row_key and si.request_id = row.request_id
  where si.row_key is null;


  if error_sc = 0 then

    insert into sov_discrepancy
    (
      id,
      sov_id,
      create_date,
      type,
      compare_kind,
      rule_group,
      deal_amnt,
      amnt,
      amount_pvp,
      course,
      course_cost,
      sur_code,
      invoice_chapter,
      invoice_rk,
      decl_id,
      invoice_contractor_chapter,
      invoice_contractor_rk,
      decl_contractor_id,
      status,
      stage,
      side_primary_processing
    )
    select
      seq_discrepancy_id.nextval,
      dis_tmp.sov_id,
      dis_tmp.create_date,
      dis_tmp.type,
      dis_tmp.compare_kind,
      dis_tmp.rule_group,
      dis_tmp.deal_amnt,
      dis_tmp.amnt,
      dis_tmp.amount_pvp,
      dis_tmp.course,
      dis_tmp.course_cost,
      dis_tmp.sur_code,
      dis_tmp.invoice_chapter,
      dis_tmp.invoice_rk,
      dis_tmp.decl_id,
      dis_tmp.invoice_contractor_chapter,
      dis_tmp.invoice_contractor_rk,
      dis_tmp.decl_contractor_id,
      dis_tmp.status,
      dis_tmp.stage,
      dis_tmp.side_primary_processing
    from sov_discrepancy_tmp dis_tmp
    left join sov_discrepancy dis on dis.sov_id = dis_tmp.sov_id
    where dis_tmp.request_id = row.request_id and dis.sov_id is null;

    insert into hist_discrepancy_stage(id, submit_date, stage_id, status_id)
    select dis.id, sysdate, 1, 1
    from sov_discrepancy dis
    left join hist_discrepancy_stage hist on hist.id = dis.id and hist.stage_id = 1 and hist.status_id = 1
    where hist.id is null;

    update selection
    set status = 13
    where invoice_request_id = row.request_id;

  else

    update selection
    set status = 11
    where invoice_request_id = row.request_id;

    for err in
    (
	    select ch.key as key
		from 
		(
		select case when agg.invoice_row_key is not null then agg.invoice_row_key else rqd.invoice_rk end as key
		from INVOICE_REQUEST_DATA rqd
			left join SOV_INVOICE_AGGREGATE agg on agg.aggregate_row_key = rqd.invoice_rk
		where request_id = row.request_id
		) ch
		left join SOV_INVOICE si on ch.key = si.row_key and si.request_id = row.request_id
		where si.row_key is null
    )
    loop
      NDS2$SYS.LOG_INFO(
      P_SITE => 'Nds2$Selections.MERGE_SENDED_SELECTIONS',   P_ENTITY_ID => null,
      P_MESSAGE_CODE => SQLCODE,   P_MESSAGE => TO_CHAR(err.key) || ' - error SOV loading invoice key for selection request: ' || TO_CHAR(row.request_id));
    end loop;

  end if;

  end loop;
  commit;
end;

procedure P$REMOVE_SELECTION_RESULTS
(
  p_selection_id IN SELECTION.ID%type
)
as
begin
  delete from SELECTION_DISCREPANCY where selection_id = p_selection_id;
  delete from SELECTION_DECLARATION where selection_id = p_selection_id;
end;

procedure P$FILL_SELECTION_DECLARATION
(
  p_selection_id IN SELECTION.ID%type
)
as
begin
  insert into SELECTION_DECLARATION (selection_id, declaration_id, is_in_process)
  select p_selection_id, decl_id, 1 from
  (
    select distinct dis.decl_id as decl_id from SELECTION_DISCREPANCY sd
    join sov_discrepancy dis on dis.id = sd.discrepancy_id
    where selection_id = p_selection_id
  );
end;

procedure P$LOAD_SELECTION ( -- Загружает детализированные данные выборки 
  pId IN SELECTION.ID%type,
  pCursor OUT SYS_REFCURSOR
)
as
begin
  
  open pCursor for
       select
               s.id,
               s.name,
               s.creation_date as CreationDate,
               s.modification_date as ModificationDate,
               s.status_date as StatusDate,
               s.analytic_sid as Analytic_SID,
               s.analytic as Analytic,
               s.chief_sid as Manager_SID,
               s.chief as Manager,
               flt.filter,
               sel_type.s_code as TypeCode,
               sel_type.s_name as Type,
               sel_status.id as Status,
               sel_status.name as StatusName,
               sel_status.rank,
               nvl(sel_decl.declarationcount,0) as declarationcount,
               nvl(sel_decl.decl_pvp_total,0) as decl_pvp_total,
               nvl(sel_discrep.discrepancycount,0) as discrepanciescount,
               nvl(sel_discrep.discrepancyamount,0) as totalamount,
               nvl(sel_discrep.discrepancypvp,0) as selecteddiscrepanciespvpamount,
               null as RegionCode,
               regionAgr.regionfirst as RegionName,
               regionAgr.regions as RegionNames
        from 
                  SELECTION s
             join DICT_SELECTION_STATUS sel_status on s.status = sel_status.id
             join DICT_SELECTION_TYPE sel_type on s.type = sel_type.s_code
             join SELECTION_FILTER flt on flt.selection_id = s.id
             left join (
                  select 
                       sd.selection_id,
                       count(1) as declarationcount,
                       sum(d.pvp_total_amnt) as decl_pvp_total
                  from 
                       SELECTION_DECLARATION sd
                       join SOV_DECLARATION_INFO d on d.id = sd.declaration_id
                  where sd.is_in_process = 1 and sd.selection_id = pId
                  group by sd.selection_id
             ) sel_decl on sel_decl.selection_id = s.id 
             left join (
                  select
                       sdis.selection_id,
                       count(1) as discrepancycount,
                       sum(dis.amnt) as discrepancyamount,
                       sum(dis.amount_pvp) as discrepancypvp
                  from
                       SELECTION_DISCREPANCY sdis
                       join SOV_DISCREPANCY dis on dis.id = sdis.discrepancy_id
                  where sdis.is_in_process = 1 and sdis.selection_id = pId
                  group by sdis.selection_id
             ) sel_discrep on sel_discrep.selection_id = s.id
             left join (
                  select
                       selection_id,
                       substr(max(sys_connect_by_path(regionInfo, ', ' )), 3) regions,
                       substr(min(sys_connect_by_path(regionInfo, ', ' )), 3) || (case when count(*) > 1 then ' ...' else '' end) as RegionFirst
                       from (
                            select
                                rf.selection_id,
                                d.regionInfo,
                                min(d.id) as id,
                                row_number() over (partition by rf.selection_id order by d.regionInfo) rn
                            from SELECTION_DECLARATION rf
                                 inner join (
                                     select 
                                       v.*,
                                       (v.REGION_CODE || ' - ' || v.REGION) as regionInfo
                                     from V$Declaration v
                                  ) d on d.id = rf.declaration_id
                             where rf.selection_id = pId                                 
                             group by rf.selection_id, d.regionInfo
                             order by rf.selection_id, d.regionInfo
                       )
                       start with rn = 1
                       connect by prior rn = rn-1 and prior selection_id = selection_id
                       group by selection_id
                       order by selection_id
               ) regionAgr on regionAgr.selection_id = s.id
              where s.id = pId;
end;

end Nds2$Selections;
/
