﻿DELETE FROM NDS2_MRR_USER.CONFIGURATION WHERE PARAMETER = 'service_selection_thread_count_max';
INSERT INTO NDS2_MRR_USER.CONFIGURATION (PARAMETER,VALUE,DEFAULT_VALUE,DESCRIPTION)
       VALUES ('service_selection_thread_count_max', '5', '5', 'Сервис выборок максимальное количество потоков');
commit;