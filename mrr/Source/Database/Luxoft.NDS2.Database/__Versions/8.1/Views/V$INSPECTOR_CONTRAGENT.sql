create or replace force view NDS2_MRR_USER.V$INSPECTOR_CONTRAGENT as
select
  bb.chapter,
  mark_name.name as MARK,
  decl.DECLARATION_VERSION_ID as DECLARATION_VERSION_ID,
  decl.DECL_TYPE as DOC_TYPE,
  decl.DECL_TYPE_CODE as DOC_TYPE_CODE,
  bb.inn2 as CONTRACTOR_INN,
  taxpayer.KPP as CONTRACTOR_KPP,
  taxpayer.NAME as CONTRACTOR_NAME,
  sur.sign_code as SUR_CODE,
  bb.GAP_CNT,
  bb.OTHR_CNT,
  bb.GAP_AMNT,
  bb.OTHR_AMNT,
  bb.AMOUNT_TOTAL,
  bb.AMOUNT_NDS,
  bb.NDS_WEIGHT,
  bb.OPERATIONS_COUNT
from NDS2_MRR_USER.MV$INSPECTOR_DECLARATION decl
join (
select
  inv.chapter,
  case
    when inv.chapter = 8 then inv.buyer_inn
    when inv.chapter = 9 then inv.seller_inn
    when inv.chapter = 10 then inv.buyer_inn
    when inv.chapter = 11 then inv.seller_inn
    when inv.chapter = 12 then inv.seller_inn
  end as inn1,
  case 
    when inv.chapter = 8 then inv.seller_inn
    when inv.chapter = 9 then inv.buyer_inn
    when inv.chapter = 10 then inv.seller_inn
    when inv.chapter = 11 then inv.buyer_inn
    when inv.chapter = 12 then inv.buyer_inn
  end as inn2,
  inv.declaration_version_id,
  COUNT(1) as OPERATIONS_COUNT,
  SUM(inv.price_total) as AMOUNT_TOTAL,
  SUM(inv.price_nds_total) as AMOUNT_NDS,
  ROUND(SUM(inv.price_nds_total)/
    (select SUM(tmp.price_nds_total) from NDS2_MRR_USER.STAGE_INVOICE tmp
     where tmp.declaration_version_id = inv.declaration_version_id), 2)
  as NDS_WEIGHT,
  SUM(case ds.type when 1 then 1 else 0 end) as GAP_CNT,
  SUM(case when ds.type=1 then 0 when ds.type IS NULL then 0 else 1 end) as OTHR_CNT,
  SUM(case ds.type when 1 then ds.amnt else 0 end) as GAP_AMNT,
  SUM(case when ds.type=1 then 0 when ds.type IS NULL then 0 else ds.amnt end) as OTHR_AMNT
from NDS2_MRR_USER.SOV_INVOICE inv
left join NDS2_MRR_USER.SOV_DISCREPANCY ds on ds.invoice_rk = inv.row_key and ds.status = 1
group by inv.chapter, inv.seller_inn, inv.buyer_inn, inv.declaration_version_id
) bb on bb.declaration_version_id = decl.declaration_version_id
left join NDS2_MRR_USER.EXT_SUR sur on sur.inn = bb.inn2 and sur.fiscal_year = decl.FISCAL_YEAR and sur.fiscal_period = decl.TAX_PERIOD
left join NDS2_MRR_USER.V$taxpayer taxpayer on taxpayer.inn = bb.inn2
left join
(
  select inn, case when SUM(case when MARK_CODE is not null then 1 else 0 end) > 1 then 22 else MAX(MARK_CODE) end as MARK
  from NDS2_MRR_USER.V$INSPECTOR_CONTRAGENT_PARAMS group by inn
) vcp on vcp.inn = bb.inn2
left join dict_remarkable_changes mark_name on mark_name.id = vcp.mark;
