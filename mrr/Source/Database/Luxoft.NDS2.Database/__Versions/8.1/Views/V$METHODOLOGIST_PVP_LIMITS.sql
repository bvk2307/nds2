create or replace force view NDS2_MRR_USER.V$METHODOLOGIST_PVP_LIMITS as
  select 
       all_inspections.s_code as sono_code,
       nvl(by_group.pvp, nvl(by_region.value, nvl(by_entire_country.value, 0))) as pvp
    from EXT_SONO all_inspections
    left join (
         select
              limited_inspect.code,
              limits.value as pvp
         from
                  METODOLOG_REGION limited_reg
             join METODOLOG_REGION_INSPECTION limited_inspect on limited_inspect.region_id = limited_reg.id
             join CFG_REGION limits on limits.id = limited_reg.id
         where limited_reg.context_id = 1
    ) by_group on by_group.code = all_inspections.s_code
    left join CFG_REGION by_region on by_region.region_code = substr(all_inspections.s_code, 1, 2)
    left join CFG_REGION by_entire_country on by_entire_country.region_code = '00';
   
