CREATE OR REPLACE FORCE VIEW NDS2_MRR_USER.V$SELECTION_LIST AS
select "ID","TYPECODE","TYPE","NAME","CREATIONDATE","MODIFICATIONDATE","STATUSDATE","STATUS","STATUSNAME","DECLARATIONCOUNT","DECL_PVP_TOTAL","DISCREPANCIESCOUNT","TOTALAMOUNT","SELECTEDDISCREPANCIESPVPAMOUNT","ANALYTIC_SID","ANALYTIC","MANAGER_SID","MANAGER","REGIONCODE","REGIONNAME","REGIONNAMES","RANK" from (
                    SELECT
                      s.ID as Id,
                      ST.s_Code as TypeCode,
                      ST.s_Name as Type,
                      s.Name as Name,
                      s.Creation_Date as CreationDate,
                      s.Modification_Date as ModificationDate,
                      s.Status_Date as StatusDate,
                      SS.ID as Status,
                      ss.Name as StatusName,
                      nvl(declAgr.decl_count, 0) as DeclarationCount,
                      nvl(declAgr.decl_pvp_total, 0) as decl_pvp_total,
                      nvl(discrAgr.discr_count, 0) as DiscrepanciesCount,
                      nvl(discrAgr.discr_amount, 0) as TotalAmount,
                      nvl(discrAgr.discr_pvp_amount, 0) as SelectedDiscrepanciesPVPAmount,
                      s.Analytic_SID as Analytic_SID,
                      s.Analytic as Analytic,
                      s.Chief_SID as Manager_SID,
                      s.Chief as Manager,
                      null as RegionCode,
                      /*regionAgr.RegionFirst*/ null as RegionName,
                      /*regionAgr.Regions*/ null as RegionNames,
                      ss.RANK
                    from selection s
                        inner join DICT_SELECTION_STATUS SS ON s.STATUS = SS.ID
                        inner join DICT_SELECTION_TYPE ST ON s.TYPE = ST.S_CODE
                        left join
                        (
                          select
                            rf.selection_id,
                            count(1) as decl_count,
                          sum(d.PVP_TOTAL_AMNT) as decl_pvp_total
                          from SELECTION_DECLARATION rf
                               inner join SOV_DECLARATION_INFO d on d.id = rf.declaration_id
                          where rf.is_in_process = 1
                          group by rf.selection_id
                        )  declAgr on declAgr.selection_id = s.id
                        left join
                        (
                          select
                            dr.selection_id,
                            count(dr.discrepancy_id) as discr_count,
                            sum(ds1.amnt) as discr_amount,
                            sum(ds1.amount_pvp) as discr_pvp_amount
                          from SELECTION_DISCREPANCY dr
                               inner join sov_discrepancy ds1 on ds1.id = dr.discrepancy_id
                          where dr.is_in_process = 1
                          group by dr.selection_id
                        )  discrAgr on discrAgr.selection_id = s.id
                    ) vw;
