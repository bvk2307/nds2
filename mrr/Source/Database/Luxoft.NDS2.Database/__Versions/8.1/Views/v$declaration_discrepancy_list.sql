﻿create or replace force view NDS2_MRR_USER.v$declaration_discrepancy_list as
select
  dis.id               as Id,
	dis.id               as DiscrepancyId,
  dis.sov_id                        as DiscrepancySovId,
  dis.CREATE_DATE                   as FoundDate,
  dis.INVOICE_RK                    as invoice_id,
  dis.INVOICE_CONTRACTOR_RK         as contractor_invoice_id,
  dis.type                          as typeCode,
  dType.s_Name                      as Type,
  d1.id                             as declaration_id,
  d1.declaration_version_id         as declaration_version_id,
  d2.declaration_version_id         as contr_declaration_version_id,
  d1.inn                            as TaxPayerInn,
  d1.kpp                            as TaxPayerKpp,
  d1.name                           as TaxPayerName,
  --ТНО
  d1.soun_code              as TaxPayerSounCode,
  d1.soun_code || ' - ' || soun1.s_name  as TaxPayerSoun,
  --Регион
  d1.region_code              as TaxPayerRegionCode,
  d1.region_code || ' - ' || ssrf1.s_name  as TaxPayerRegion,
  --Период
  d1.tax_period                     as TaxPayerPeriodCode,
  d1.FISCAL_YEAR                    as TaxPayerYearCode,
  decode(d1.tax_period, '21', '1 кв.', '22', '2 кв.', '23', '3 кв.', '24', '4 кв.', '') || d1.FISCAL_YEAR || ' г.' as TaxPayerPeriod,
  d1.correction_number              as CorrectionNumber,
  case when dis.Invoice_Chapter is null then null else
    'Раздел ' || dis.Invoice_Chapter ||
    case dis.Invoice_Chapter
      when 8 then ' (КнПок)'
      when 9 then ' (КнПрод)'
      when 10 then ' (ЖвСФ)'
      when 11 then ' (ЖпСФ)'
      when 12 then ' (КнПрод)'
      else ''
    end
  end as TaxPayerSource,
  --Признак
  d1.decl_sign                      as TaxPayerDeclSign,
  --Категория НП
  d1.Category                       as TaxPayerCategoryCode,
  decode(d1.Category, 0, '', 1, 'Крупнейший', 'Неизвестно') TaxPayerCategory,
  --###Контрагент###
  nvl(d2.inn,
      case si.chapter
        when 8 then si.seller_inn
        when 9 then si.buyer_inn
        else ''end) as ContractorInn,
  nvl(d2.kpp,
      case si.chapter
        when 8 then si.seller_kpp
        when 9 then si.buyer_kpp
        else '' end)                            as ContractorKpp,
    nvl(d2.name,
      case si.chapter
        when 8 then seller.name
        when 9 then buyer.name
        else '' end)                           as ContractorName,
  --ТНО
  d2.soun_code              as ContractorSounCode,
  d2.soun_code || ' - ' || soun2.s_name  as ContractorSoun,
  --Регион
  d2.region_code              as ContractorRegionCode,
  d2.region_code || ' - ' || ssrf2.s_name  as ContractorRegion,
  --Период
  d2.tax_period                     as ContractorPeriodCode,
  d2.FISCAL_YEAR                    as ContractorYearCode,
  decode(d2.tax_period, '21', '1 кв.', '22', '2 кв.', '23', '3 кв.', '24', '4 кв.', '') || d2.FISCAL_YEAR as ContractorPeriod,
  case when dis.Invoice_Contractor_Chapter is null then null else
    'Раздел ' || dis.Invoice_Contractor_Chapter ||
    case dis.Invoice_Contractor_Chapter
      when 8 then ' (КнПок)'
      when 9 then ' (КнПрод)'
      when 10 then ' (ЖвСФ)'
      when 11 then ' (ЖпСФ)'
      when 12 then ' (КнПрод)'
      else ''
    end
  end as ContractorSource,
  d2.correction_number              as ContractorCorrectionNumber,
  --Признак
  d2.decl_sign                      as ContractorDeclSign,
  --Категория НП
  d2.Category                       as ContractorCategoryCode,
  decode(d2.Category, 0, '', 1, 'Крупнейший', 'Неизвестно') ContractorCategory,
  dis.amnt                          as Amount,
  dis.amount_pvp                    as AmountPVP,
  dis.course,
  dis.course_cost                   as CourseCost,
  --dis.sur_code                      as SURCode,
  2                                 as SURCode,
  dis.status                        as StatusCode,
  null                              as ischecked,
  null                              as selected,
  dStatus.name                      as Status,
  null                              as InProcessByOtherSelection,
  null as CLAIM_ID,
  null as sync_date,
  null as Claim_status,
  d2.DECL_TYPE as ContractorDocType,
  decode(d2.DECL_TYPE_CODE, 0, d2.COMPENSATION_AMNT, null) as SUM_NDS,
  decode(d2.DECL_TYPE_CODE, 0, d2.STATUS, null) as STATUS_KNP,
  --непонятно
  sysdate                           as CalculationDate,
  null                 as Stage,
  DST.NAME                 as Stage_Name,
  null          as StageStatusCode,
  DDFS.NAME          as STAGE_STATUS_NAME
from SOV_DISCREPANCY dis
left join v$invoice si on si.row_key = dis.invoice_rk
left join v$taxpayer buyer on buyer.inn=si.buyer_inn and (si.buyer_kpp is null or buyer.kpp=si.buyer_kpp)
left join v$taxpayer seller on seller.inn=si.seller_inn and (si.seller_kpp is null or seller.kpp=si.seller_kpp)
--лицо проверяемой декларации
left join V$Declaration d1 on d1.id = dis.decl_id
left join v$sono soun1 on soun1.s_code = d1.soun_code
left join v$ssrf ssrf1 on ssrf1.s_code = d1.region_code
--контрагент
left join V$Declaration d2 on d2.id = dis.decl_contractor_id
left join v$sono soun2 on soun2.s_code = d2.soun_code
left join v$ssrf ssrf2 on ssrf2.s_code = d2.region_code
left join DICT_DISCREPANCY_TYPE dType on dType.s_Code = dis.type
--inner join dict_discrepancy_status disStatus on disStatus.id = dis.status
left join dict_discrepancy_status dStatus on dStatus.id = dis.status
--left join v$discrepancy_stage dStage on dStage.Id = dis.id
left join DICT_DISCREPANCY_STAGE DST ON DST.ID = DIS.STAGE
left join DICT_DISCREP_FILTER_STATUS DDFS ON DDFS.ID = DIS.STATUS;
