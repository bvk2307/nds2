﻿CREATE MATERIALIZED VIEW NDS2_MRR_USER.MV$TAX_PAYER_NAME
nologging
pctfree 0
build DEFERRED 
refresh force on demand
AS select
ip.innfl as INN,
nvl(ip.last_name, '')||' '||nvl(ip.first_name, '')||' '||nvl(ip.patronymic, '') as NP_NAME,
0 as TP_TYPE
from v_egrn_ip ip
union all
select
ul.inn,
ul.name_full as NP_NAME,
1 as TP_TYPE
from
v_egrn_ul ul
;
create index NDS2_MRR_USER.idx_mvtpname_inn on NDS2_MRR_USER.MV$TAX_PAYER_NAME(inn);
