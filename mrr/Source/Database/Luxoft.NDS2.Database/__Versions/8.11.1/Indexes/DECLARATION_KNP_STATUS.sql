﻿create index nds2_mrr_user.IX_DECLKNPST_ZIP
	on nds2_mrr_user.declaration_knp_status (zip) tablespace nds2_idx;

create index nds2_mrr_user.IX_DECLKNPST_DECL_DATE
	on nds2_mrr_user.declaration_knp_status (decl_insert_date) tablespace nds2_idx;

create index nds2_mrr_user.IX_DECLKNPST_SEOD_DATE
	on nds2_mrr_user.declaration_knp_status (seod_creation_date) tablespace nds2_idx;
