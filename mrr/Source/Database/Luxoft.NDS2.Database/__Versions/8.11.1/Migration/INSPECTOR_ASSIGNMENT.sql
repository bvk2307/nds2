﻿CREATE TABLE NDS2_MRR_USER.MRR_USER(ID PRIMARY KEY, NAME NOT NULL, SID NOT NULL) AS
SELECT 
    NDS2_MRR_USER.SEQ$MRR_USER.NEXTVAL,
    DO.INSPECTOR_NAME,
	DO.INSPECTOR_SID
FROM 
	(SELECT INSPECTOR_SID,
			INSPECTOR_NAME,
			ROW_NUMBER() 
				OVER (PARTITION BY INSPECTOR_SID ORDER BY NULL) 
					AS INSP_NAME_RANK
    FROM NDS2_MRR_USER.DECLARATION_OWNER
    WHERE INSPECTOR_SID IS NOT NULL 
          AND INSPECTOR_NAME IS NOT NULL
	)DO
WHERE DO.INSP_NAME_RANK = 1;
    
CREATE UNIQUE INDEX NDS2_MRR_USER.IX_MRR_USER_SID ON NDS2_MRR_USER.MRR_USER(SID) TABLESPACE NDS2_IDX;    

CREATE TABLE NDS2_MRR_USER.DECLARATION_ASSIGNMENT 
(
  INN_DECLARANT NOT NULL 
, KPP_EFFECTIVE NOT NULL 
, PERIOD_EFFECTIVE NOT NULL 
, FISCAL_YEAR NOT NULL 
, DECLARATION_TYPE_CODE NOT NULL 
, ASSIGNED_TO 
, ASSIGNED_AT NOT NULL 
) 
AS 
    SELECT 
        DO.INN_DECLARANT, 
        DO.KPP_EFFECTIVE, 
        DTP.EFFECTIVE_VALUE AS PERIOD_EFFECTIVE,
        DO.FISCAL_YEAR,
        DO.TYPE_CODE AS DECLARATION_TYPE_CODE,
        U.ID AS ASSIGNED_TO,
        CURRENT_TIMESTAMP AS ASSIGNED_AT
    FROM NDS2_MRR_USER.DECLARATION_OWNER DO
    JOIN NDS2_MRR_USER.MRR_USER U ON DO.INSPECTOR_SID = U.SID
    JOIN NDS2_MRR_USER.DICT_TAX_PERIOD DTP ON DO.PERIOD_CODE = DTP.CODE;
    
ALTER TABLE NDS2_MRR_USER.DECLARATION_ASSIGNMENT ADD ASSIGNED_BY NUMBER;
 
CREATE INDEX NDS2_MRR_USER.IX_DECL_ASSIGN_TO ON NDS2_MRR_USER.DECLARATION_ASSIGNMENT (INN_DECLARANT, KPP_EFFECTIVE, PERIOD_EFFECTIVE, FISCAL_YEAR, DECLARATION_TYPE_CODE)
TABLESPACE NDS2_IDX;

DROP TABLE NDS2_MRR_USER.DECLARATION_OWNER;

--Скрипт обновления полей имя/сид таблиц Declaration_Active и Declaration_Active_Search
DECLARE
    CURSOR update_rows_cur IS 
         SELECT usr.NAME, usr.SID, da.ROWID, dsa.ROWID
         FROM nds2_mrr_user.declaration_assignment das
         INNER JOIN nds2_mrr_user.mrr_user usr
            ON das.assigned_to = usr.ID
         INNER JOIN nds2_mrr_user.declaration_active da
            ON das.inn_declarant = da.inn_declarant
                AND das.kpp_effective = da.kpp_effective
                AND das.period_effective = da.period_effective
                AND das.fiscal_year = da.fiscal_year
                AND das.declaration_type_code = da.type_code
         LEFT JOIN nds2_mrr_user.declaration_active_search dsa
            ON da.zip = dsa.declaration_version_id
         FOR UPDATE OF usr.NAME, da.zip, dsa.declaration_version_id;

    TYPE update_info_rec IS RECORD  (   
            NAME nds2_mrr_user.declaration_active.inspector%TYPE,
            SID nds2_mrr_user.declaration_active.inspector_sid%TYPE,
            da_rowid ROWID,
            dsa_rowid ROWID);

    TYPE upd_info_list IS TABLE OF update_info_rec;
    upd_list upd_info_list;
    --кол-во обрабатываемых назначений за итерацию
    v_item_count NUMBER DEFAULT 1000;
BEGIN
   OPEN update_rows_cur;
   LOOP
        FETCH  update_rows_cur 
        BULK COLLECT INTO upd_list
        LIMIT v_item_count;

        FORALL I IN 1..upd_list.COUNT
            UPDATE nds2_mrr_user.declaration_active
            SET inspector = upd_list(I).NAME,
                inspector_sid = upd_list(I).SID
            WHERE ROWID = upd_list(I).da_rowid;     

         FORALL I IN 1..upd_list.COUNT
            UPDATE nds2_mrr_user.declaration_active_search
            SET assignee_name = upd_list(I).NAME,
                assignee_sid = upd_list(I).SID
            WHERE ROWID = upd_list(I).dsa_rowid;        

        EXIT WHEN update_rows_cur%notfound;
   END LOOP;

   CLOSE update_rows_cur;
   COMMIT;
   EXCEPTION WHEN OTHERS THEN
    ROLLBACK;
    raise_application_error(-20111,'Ошибка миграции текущих наначений инспекторов '||substr(sqlerrm,1,150));
END;
/