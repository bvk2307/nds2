﻿begin
NDS2_MRR_USER.NDS2$SYS.DROP_IF_EXISTS('NDS2_MRR_USER', 'TMP_USER_TASK');
end;
/
create table NDS2_MRR_USER.TMP_USER_TASK as select 
TASK_ID,
TYPE_ID,
INN,
INN_CONTRACTOR,
KPP_EFFECTIVE,
QUARTER,
PERIOD,
FISCAL_YEAR,
ASSIGNEE_SID,
ASSIGNEE_NAME,
ASSIGN_DATE,
CREATE_DATE,
END_DATE_PLANNING,
CLOSE_DATE,
case
  when status in (1, 2) then 3
  else
    case 
    when status = 3 then 1 else 2 
    end                                                  
  end as STATUS 
from NDS2_MRR_USER.USER_TASK; 
      
comment on column NDS2_MRR_USER.TMP_USER_TASK.TASK_ID is
'Идентификатор задания';

comment on column NDS2_MRR_USER.TMP_USER_TASK.TYPE_ID is
'Тип задания';

comment on column NDS2_MRR_USER.TMP_USER_TASK.ASSIGNEE_SID is
'СИД закрепленного пользователя';

comment on column NDS2_MRR_USER.TMP_USER_TASK.ASSIGN_DATE is
'Дата закрепления пользователя';

comment on column NDS2_MRR_USER.TMP_USER_TASK.END_DATE_PLANNING is
'Планируемая дата завершения задания';

comment on column NDS2_MRR_USER.TMP_USER_TASK.CLOSE_DATE is
'Фактическая дата завершения';

comment on column NDS2_MRR_USER.TMP_USER_TASK.STATUS is
'Статус задания';

  
 begin
  EXECUTE IMMEDIATE 'drop index NDS2_MRR_USER.IDX_USER_TASK_TYPEID'; 
  EXECUTE IMMEDIATE 'drop index NDS2_MRR_USER.IDX_USER_TASK_ASSIGNEESID'; 
  EXECUTE IMMEDIATE 'drop index NDS2_MRR_USER.IDX_USER_TASK_ASSIGNEENAME';  
  exception when others then null;
 end;
 /
begin      
NDS2_MRR_USER.NDS2$SYS.DROP_IF_EXISTS('NDS2_MRR_USER', 'USER_TASK');  
end;
/

alter table NDS2_MRR_USER.TMP_USER_TASK rename to USER_TASK;
alter table NDS2_MRR_USER.USER_TASK add constraint PK_USER_TASK primary key (TASK_ID);

create index NDS2_MRR_USER.IDX_USER_TASK_TYPEID on NDS2_MRR_USER.USER_TASK (TYPE_ID);
create index NDS2_MRR_USER.IDX_USER_TASK_ASSIGNEESID on NDS2_MRR_USER.USER_TASK (ASSIGNEE_SID);
create index NDS2_MRR_USER.IDX_USER_TASK_ASSIGNEENAME on NDS2_MRR_USER.USER_TASK (ASSIGNEE_NAME);