﻿declare
  v_length number;
  v_exist number;
  v_account varchar2(20 char);
begin
  for line in (
    select
      bar.inn,
      bar.kpp_effective,
      bar.bikbank,
      bar.period_begin,
      bar.period_end,
      bar.accounts_list
    from NDS2_MRR_USER.BANK_ACCOUNT_REQUEST bar
  ) loop
    if (line.accounts_list = 'Все счета') then
      
      for acc in (
        select bs.nomsch
        from NDS2_MRR_USER.BSSCHET_UL bs
        where bs.inn = line.inn and NDS2_MRR_USER.F$GET_EFFECTIVE_KPP(bs.inn, bs.kpp) = line.kpp_effective
          and bs.bik = line.bikbank
          and bs.dateopensch <= line.period_end and (bs.dateclosesch >= line.period_begin or bs.dateclosesch is null)
        union all  
        select bs.nomsch
        from NDS2_MRR_USER.BSSCHET_IP bs
        where bs.inn = line.inn
          and bs.bik = line.bikbank
          and bs.dateopensch <= line.period_end and (bs.dateclosesch >= line.period_begin or bs.dateclosesch is null)
      ) loop
          v_account := acc.nomsch;
          
          select case when ra.bik is null then 0 else 1 end into v_exist from dual
          left join NDS2_MRR_USER.REQUESTED_ACCOUNTS ra on ra.bik = line.bikbank and ra.account = v_account
                  and ra.periodbegin <= line.period_begin and ra.periodend >= line.period_end
          where rownum = 1;
                  
          if v_exist = 0 then
            insert into NDS2_MRR_USER.REQUESTED_ACCOUNTS (BIK, ACCOUNT, PERIODBEGIN, PERIODEND)
              values (line.bikbank, v_account, line.period_begin, line.period_end);
          end if;
      end loop;
      
    else begin
      
      v_length := length(line.accounts_list) - length(replace(line.accounts_list, ',', '')) + 1;
      for i in 1 .. v_length loop
        v_account := replace(regexp_substr(line.accounts_list,'[^,]+',1,i), ' ', '');
          
        select case when ra.bik is null then 0 else 1 end into v_exist from dual
        left join NDS2_MRR_USER.REQUESTED_ACCOUNTS ra on ra.bik = line.bikbank and ra.account = v_account
                and ra.periodbegin <= line.period_begin and ra.periodend >= line.period_end
        where rownum = 1;
                  
        if v_exist = 0 then
          insert into NDS2_MRR_USER.REQUESTED_ACCOUNTS (BIK, ACCOUNT, PERIODBEGIN, PERIODEND)
            values (line.bikbank, v_account, line.period_begin, line.period_end);
        end if;
      end loop;
    end;
    
    end if;
    
  end loop;
  
  commit;
  
end;
/
