﻿-- Create table
create table NDS2_MRR_USER.RM_ROLE
(
  id   number not null,
  name varchar2(40) not null
)
;
-- Create/Recreate primary, unique and foreign key constraints 
alter table NDS2_MRR_USER.RM_ROLE
  add constraint PK_RM_ROLE primary key (ID);
alter table NDS2_MRR_USER.RM_ROLE
  add constraint UK_RM_ROLE_NAME unique (NAME);
