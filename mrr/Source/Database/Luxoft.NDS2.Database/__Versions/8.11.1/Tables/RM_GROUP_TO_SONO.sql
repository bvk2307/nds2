﻿delete from NDS2_MRR_USER.RM_GROUP_TO_SONO;
commit;
-- Drop columns 
alter table NDS2_MRR_USER.RM_GROUP_TO_SONO drop column access_limit_type_id;
alter table NDS2_MRR_USER.RM_GROUP_TO_SONO modify group_id not null;
alter table NDS2_MRR_USER.RM_GROUP_TO_SONO modify sono_code varchar2(4);

-- Create/Recreate primary, unique and foreign key constraints 
alter table NDS2_MRR_USER.RM_GROUP_TO_SONO
  add constraint IDX_RM_GRP_SONO unique (GROUP_ID, SONO_CODE);
