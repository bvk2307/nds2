﻿create table nds2_mrr_user.declaration_knp_status (
	zip number,
	decl_insert_date date,
	seod_creation_date date,
	calc_is_open number,
	calc_status number,
	calc_status_date date
);
