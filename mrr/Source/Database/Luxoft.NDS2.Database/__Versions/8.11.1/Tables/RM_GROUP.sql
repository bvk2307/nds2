﻿-- Create table
create table NDS2_MRR_USER.RM_GROUP
(
  id          number not null,
  description varchar2(250)
)
;
-- Create/Recreate primary, unique and foreign key constraints 
alter table NDS2_MRR_USER.RM_GROUP
  add constraint PK_RM_GRP primary key (ID);
