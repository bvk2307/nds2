﻿CREATE TABLE NDS2_MRR_USER.STATE 
(	
	PARAM_NAME VARCHAR2(30 CHAR) NOT NULL ENABLE, 
	PARAM_VALUE VARCHAR2(30 CHAR), 
	TS TIMESTAMP (6), 
	CONSTRAINT STATE_PK PRIMARY KEY (PARAM_NAME)
); 

insert into NDS2_MRR_USER.STATE(param_name, param_value, ts)
	values ('decl_refresh', '0', current_timestamp); 
commit;