﻿begin
NDS2_MRR_USER.NDS2$SYS.DROP_IF_EXISTS('NDS2_MRR_USER', 'DICT_EXPLAIN_TYPE');
end;
/
create table NDS2_MRR_USER.DICT_EXPLAIN_TYPE
(
   id          NUMBER(2) not null,
   description VARCHAR2(128 CHAR)
);

