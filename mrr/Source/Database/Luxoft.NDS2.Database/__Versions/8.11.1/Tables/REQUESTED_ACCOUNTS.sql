﻿create table NDS2_MRR_USER.REQUESTED_ACCOUNTS
(
  Bik varchar2(9 char),
  Account varchar2(20 char),
  PeriodBegin date,
  PeriodEnd date
);
