﻿begin
NDS2_MRR_USER.NDS2$SYS.DROP_IF_EXISTS('NDS2_MRR_USER', 'SEOD_EXPLAIN_COMMENT');
end;
/
create table NDS2_MRR_USER.SEOD_EXPLAIN_COMMENT as select
	explain_id,
	1 as explain_type_id,
	user_comment
from NDS2_MRR_USER.SEOD_EXPLAIN_REPLY where type_id = 1;
