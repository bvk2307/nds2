﻿-- Create table
create table NDS2_MRR_USER.RM_OPERATION
(
  id   number not null,
  name varchar2(250) not null,
  description varchar2(250) null
)
;
-- Create/Recreate primary, unique and foreign key constraints 
alter table NDS2_MRR_USER.RM_OPERATION
  add constraint PK_RM_OPER primary key (ID);
alter table NDS2_MRR_USER.RM_OPERATION
  add constraint UK_RM_OPER_NAME unique (NAME);
