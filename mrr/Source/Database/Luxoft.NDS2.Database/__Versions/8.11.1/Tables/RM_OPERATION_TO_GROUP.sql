﻿delete from NDS2_MRR_USER.RM_OPERATION_TO_GROUP;
commit;
-- Add/modify columns 
alter table NDS2_MRR_USER.RM_OPERATION_TO_GROUP rename column mrr_operation_id to OPERATION_ID;
alter table NDS2_MRR_USER.RM_OPERATION_TO_GROUP modify operation_id not null;
alter table NDS2_MRR_USER.RM_OPERATION_TO_GROUP rename column sono_group_id to GROUP_ID;
alter table NDS2_MRR_USER.RM_OPERATION_TO_GROUP modify group_id not null;
alter table NDS2_MRR_USER.RM_OPERATION_TO_GROUP add role_id number;
alter table NDS2_MRR_USER.RM_OPERATION_TO_GROUP add limit_type_id number not null;
