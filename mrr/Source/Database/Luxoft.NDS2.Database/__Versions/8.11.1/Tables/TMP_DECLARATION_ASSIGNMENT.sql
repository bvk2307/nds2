﻿CREATE TABLE NDS2_MRR_USER.TMP_DECLARATION_ASSIGNMENT
( zip number not null
  , assigned_to_name varchar2(128) not null
  , assigned_to_sid  varchar2(128) not null);
