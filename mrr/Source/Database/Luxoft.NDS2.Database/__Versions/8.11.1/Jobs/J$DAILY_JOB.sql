﻿begin
for j in (select * from all_scheduler_jobs where owner = 'NDS2_MRR_USER' and job_name = 'J$DAILY_JOB')
  loop
    execute immediate 'begin dbms_scheduler.drop_job(job_name => ''NDS2_MRR_USER.'||j.job_name||''', force => true); end;';
  end loop;
  
for pr in (select p.PROGRAM_NAME from all_scheduler_programs p 
                  inner join all_scheduler_chain_steps s on s.OWNER = p.OWNER 
                  and s.PROGRAM_NAME = p.PROGRAM_NAME 
                  where p.owner = 'NDS2_MRR_USER' and s.CHAIN_NAME = 'BACKGROUND_DAILY_TASKS')
  loop
    execute immediate 'begin dbms_scheduler.drop_program(program_name => ''NDS2_MRR_USER.'||pr.PROGRAM_NAME||''', force => true); end;';
  end loop;

for st in (select * from all_scheduler_chain_steps where owner = 'NDS2_MRR_USER' and chain_name = 'BACKGROUND_DAILY_TASKS')
  loop
    execute immediate 'begin  dbms_scheduler.drop_chain_step(chain_name => ''NDS2_MRR_USER.BACKGROUND_DAILY_TASKS'' , step_name =>'''||st.step_name||''' , force => true); end;';
  end loop;


for ch in (select * from all_scheduler_chains where owner = 'NDS2_MRR_USER' and chain_name = 'BACKGROUND_DAILY_TASKS')
  loop
    execute immediate 'begin  dbms_scheduler.drop_chain(chain_name => ''NDS2_MRR_USER.BACKGROUND_DAILY_TASKS''); end;';
  end loop;
  
for pr in (select * from all_scheduler_programs where owner = 'NDS2_MRR_USER' and PROGRAM_NAME in ('P$DAILY_TASK_BEGIN', 'P$DICTIONARY', 'P$KNP_AGGREGATES', 'P$DECLARATION_AGGREGATES', 'P$DECLARATION_SUMMARY_REPORT', 'P$UPDATE_WHITE_LIST', 'P$RECALCULATE_SLIDE_AT', 'P$BANK_REQUEST', 'P$USER_TASK_DAILY_PROCESS', 'P$REFRESH_STAT', 'P$DAILY_TASK_FINISH'))
  loop
    execute immediate 'begin dbms_scheduler.drop_program(program_name => ''NDS2_MRR_USER.'||pr.PROGRAM_NAME||''', force => true); end;';
  end loop;
end;
/

begin  
  DBMS_SCHEDULER.create_job(
       job_name => 'NDS2_MRR_USER.J$DAILY_JOB'
      ,job_type => 'STORED_PROCEDURE'
      ,job_action => 'PAC$JOB.START_DAILY_TASKS'
      ,start_date => TO_DATE(TO_CHAR(SYSDATE, 'dd/mm/yyyy') || ' 00:10:00', 'dd/mm/yyyy HH24:mi:ss') + INTERVAL '1' MINUTE
      ,repeat_interval => 'freq=Daily;Interval=1'
      ,end_date => to_date('01-01-2100 00:00:00', 'dd-mm-yyyy hh24:mi:ss')
      ,job_class => 'DEFAULT_JOB_CLASS'
      ,enabled => false
      ,auto_drop => false 
      ,comments => 'Цепь ежедневных задач');
end;
/
 
