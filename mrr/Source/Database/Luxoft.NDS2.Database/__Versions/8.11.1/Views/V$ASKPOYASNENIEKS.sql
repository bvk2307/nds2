﻿create or replace force view nds2_mrr_user.v$askpoyasnenieks as
select
   mc."Ид" as Id
  ,mc."ZIP" as ZIP
  ,mc."Пояснение" as Poyasnenie
  ,mc."НомКС" as NomKs
  ,mc."ПоясненКС" as PoyasnKs
 from NDS2_MC."ASKПояснКС" mc;
