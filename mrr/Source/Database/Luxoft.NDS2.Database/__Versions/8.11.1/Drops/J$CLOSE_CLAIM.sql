﻿begin
    sys.dbms_scheduler.drop_job(  job_name=> 'NDS2_MRR_USER.J$CLOSE_CLAIM',
                                  force=> false,
                                  defer=> true);
end;
/