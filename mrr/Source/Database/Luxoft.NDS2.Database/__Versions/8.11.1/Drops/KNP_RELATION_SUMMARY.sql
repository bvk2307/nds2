﻿begin
	for ix_meta in (select i.INDEX_NAME 
					from all_indexes  i 
					where 
						i.table_owner = 'NDS2_MRR_USER' 
						and i.table_name = 'KNP_RELATION_SUMMARY')
	LOOP 
		execute immediate 'drop index NDS2_MRR_USER.'||ix_meta.INDEX_NAME;
	END LOOP;
end;
/
