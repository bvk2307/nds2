﻿insert into NDS2_MRR_USER.CONFIGURATION (PARAMETER,VALUE,DEFAULT_VALUE,DESCRIPTION)
  values ('drop_assign_sono_changed_exec_date',TO_CHAR(sysdate, 'DD-MM-YYYY HH24:MI:SS'),TO_CHAR(sysdate, 'DD-MM-YYYY HH24:MI:SS'),'Дата последнего запуска процедуры сброса назначений при перерегистрации НП');

insert into NDS2_MRR_USER.CONFIGURATION (PARAMETER,VALUE,DEFAULT_VALUE,DESCRIPTION)
  values ('at_close_by_new_corr_exec_date',TO_CHAR(sysdate, 'DD-MM-YYYY HH24:MI:SS'),TO_CHAR(sysdate, 'DD-MM-YYYY HH24:MI:SS'),'Дата последнего запуска процедуры автозакрытия АТ при поступлении нвоой корректировки');

insert into NDS2_MRR_USER.CONFIGURATION (PARAMETER,VALUE,DEFAULT_VALUE,DESCRIPTION)
  values ('ai_close_by_new_corr_exec_date',TO_CHAR(sysdate, 'DD-MM-YYYY HH24:MI:SS'),TO_CHAR(sysdate, 'DD-MM-YYYY HH24:MI:SS'),'Дата последнего запуска процедуры автозакрытия АИ при поступлении нвоой корректировки');

insert into NDS2_MRR_USER.CONFIGURATION (PARAMETER,VALUE,DEFAULT_VALUE,DESCRIPTION)
  values ('at_autoclose_items_limit','5000','5000','Ограничение количества АТ на итерацию автозакрытия');

insert into NDS2_MRR_USER.CONFIGURATION (PARAMETER,VALUE,DEFAULT_VALUE,DESCRIPTION)
  values ('ai_autoclose_items_limit','5000','5000','Ограничение количества АИ на итерацию автозакрытия');

commit;