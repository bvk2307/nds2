﻿insert into nds2_mrr_user.cfg_index (id, table_name, index_name, column_list, index_type, op_statistics, op_parallel_count, op_noparallel_postbuild)
values((select coalesce(max(id) + 1, 1) from nds2_mrr_user.cfg_index), 'DECLARATION_KNP_STATUS', 'IX_DECLKNPST_ZIP', 'ZIP', 'NONUNIQUE', 1, 4, 1);

insert into nds2_mrr_user.cfg_index (id, table_name, index_name, column_list, index_type, op_statistics, op_parallel_count, op_noparallel_postbuild)
values((select coalesce(max(id) + 1, 1) from nds2_mrr_user.cfg_index), 'DECLARATION_KNP_STATUS', 'IX_DECLKNPST_DECL_DATE', 'DECL_INSERT_DATE', 'NONUNIQUE', 1, 4, 1);

insert into nds2_mrr_user.cfg_index (id, table_name, index_name, column_list, index_type, op_statistics, op_parallel_count, op_noparallel_postbuild)
values((select coalesce(max(id) + 1, 1) from nds2_mrr_user.cfg_index), 'DECLARATION_KNP_STATUS', 'IX_DECLKNPST_SEOD_DATE', 'SEOD_CREATION_DATE', 'NONUNIQUE', 1, 4, 1);

commit;