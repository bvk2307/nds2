﻿begin
 execute immediate 'alter table NDS2_MRR_USER.MV$DISCREPANCY_DECL_LIST add decl_inn VARCHAR2(12 CHAR)';
 execute immediate 'alter table NDS2_MRR_USER.MV$DISCREPANCY_DECL_LIST add decl_kpp VARCHAR2(9 CHAR)';
 execute immediate 'alter table NDS2_MRR_USER.MV$DISCREPANCY_DECL_LIST add decl_name VARCHAR2(3000)';
 execute immediate 'alter table NDS2_MRR_USER.MV$DISCREPANCY_DECL_LIST add decl_doctype VARCHAR2(20)';
 exception when others then null;
end;
/
