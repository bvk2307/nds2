create table NDS2_MRR_USER.EFFICIENCY_MONITOR_BUYER
(
  fiscal_year                VARCHAR2(4 CHAR),
  period                     NUMBER,
  calc_date                  DATE,
  sono_code                  VARCHAR2(4 CHAR),
  buyer_dis_opn_amnt         NUMBER,
  buyer_dis_opn_knp_amnt     NUMBER,
  buyer_dis_opn_gap_amnt     NUMBER,
  buyer_dis_opn_gap_knp_amnt NUMBER,
  buyer_dis_opn_nds_amnt     NUMBER,
  buyer_dis_opn_nds_knp_amnt NUMBER,
  buyer_dis_all_knp_amnt     NUMBER,
  buyer_dis_cls_amnt         NUMBER,
  buyer_dis_cls_knp_amnt     NUMBER,
  buyer_dis_cls_gap_amnt     NUMBER,
  buyer_dis_cls_gap_knp_amnt NUMBER,
  buyer_dis_cls_nds_amnt     NUMBER,
  buyer_dis_cls_nds_knp_amnt NUMBER
) 
nologging;
