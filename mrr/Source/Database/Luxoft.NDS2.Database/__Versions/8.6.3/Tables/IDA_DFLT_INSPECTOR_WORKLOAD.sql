CREATE TABLE NDS2_MRR_USER.IDA_DFLT_INSPECTOR_WORKLOAD
(
  S_CODE VARCHAR2(4) NOT NULL PRIMARY KEY,
  PAYMENT_DECL_PERM NUMBER(1) NOT NULL CHECK (PAYMENT_DECL_PERM IN (0,1)),
  COMPENSATION_DECL_PERM NUMBER(1) NOT NULL CHECK (COMPENSATION_DECL_PERM IN (0,1)),
  MAX_PAYMENT_CAPACITY DECIMAL DEFAULT 0 NOT NULL,
  MAX_COMPENSATION_CAPACITY DECIMAL DEFAULT 0 NOT NULL,
  IS_ACTIVE NUMBER(1) NOT NULL CHECK (IS_ACTIVE IN (0,1)),
  INSERT_DATE DATE DEFAULT SYSDATE NOT NULL,
  INSERTED_BY VARCHAR2(4000) NOT NULL,
  UPDATE_DATE DATE DEFAULT SYSDATE NOT NULL,
  UPDATED_BY VARCHAR2(4000) NOT NULL
);
comment on table  NDS2_MRR_USER.IDA_DFLT_INSPECTOR_WORKLOAD IS '������� �������� �������� �� ��������� �� ��������� ��� �������� ����';
comment on column  NDS2_MRR_USER.IDA_DFLT_INSPECTOR_WORKLOAD.S_CODE IS '��� ���� �� ���� �� �������� ���������� ����������� ����������';
comment on column  NDS2_MRR_USER.IDA_DFLT_INSPECTOR_WORKLOAD.PAYMENT_DECL_PERM IS '�������, ������������ ������� � ���������� ���� �� ��������� ���������� ���� ''� ������''';
comment on column  NDS2_MRR_USER.IDA_DFLT_INSPECTOR_WORKLOAD.COMPENSATION_DECL_PERM IS '�������, ������������ ������� � ���������� ���� �� ��������� ���������� ���� ''� ����������''';
comment on column  NDS2_MRR_USER.IDA_DFLT_INSPECTOR_WORKLOAD.MAX_PAYMENT_CAPACITY IS '������������ ����� �� ����������� ���� ''� ������'', ������� ����� ���� ������������� ��������� �� ����������';
comment on column  NDS2_MRR_USER.IDA_DFLT_INSPECTOR_WORKLOAD.MAX_COMPENSATION_CAPACITY IS '������������ ����� �� ����������� ���� ''� ����������'', ������� ����� ���� ������������� ��������� �� ����������';
comment on column  NDS2_MRR_USER.IDA_DFLT_INSPECTOR_WORKLOAD.IS_ACTIVE IS '������� ���������� ������. ���������� ������ ��������� ������ ��� ����������� ����������� ������';
comment on column  NDS2_MRR_USER.IDA_DFLT_INSPECTOR_WORKLOAD.INSERT_DATE IS '���� ���������� ������';
comment on column  NDS2_MRR_USER.IDA_DFLT_INSPECTOR_WORKLOAD.INSERTED_BY IS '����� ������������, ������� ���� ������� ������';
comment on column  NDS2_MRR_USER.IDA_DFLT_INSPECTOR_WORKLOAD.UPDATE_DATE IS '���� ���������� �������������� ������';
comment on column  NDS2_MRR_USER.IDA_DFLT_INSPECTOR_WORKLOAD.UPDATED_BY IS '����� ������������,������� ��������� ������������ ������';
