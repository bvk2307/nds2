create table NDS2_MRR_USER.CONTROL_RATIO_QUEUE_LOG
(
  LOAD_ID NUMBER,
  LOAD_DECL_COUNT NUMBER,
  TIME_STAMP DATE
);

alter table NDS2_MRR_USER.CONTROL_RATIO_QUEUE_LOG add constraint pk_load_id primary key (load_id);

declare
v_last_load_id number;
begin
  select 
  nvl(max(d.IdZagruzka), 0) into v_last_load_id
  from
  NDS2_MRR_USER.control_ratio_queue q
  inner join NDS2_MRR_USER.v$askdekl d on d.ID = q.id;
  

  insert into NDS2_MRR_USER.control_ratio_queue_log(load_id, load_decl_count, time_stamp)
  values (v_last_load_id, 0, sysdate);
  commit;
end;
/
