CREATE TABLE NDS2_MRR_USER.IDA_TAX_PAYER_INDIVIDUAL_ASSG
(
  S_CODE VARCHAR2(4) NOT NULL,
  INN VARCHAR2(128) NOT NULL,
  SID VARCHAR2(128),
  IS_ACTIVE NUMBER(1) NOT NULL CHECK (IS_ACTIVE IN (0,1)),
  INSERT_DATE DATE DEFAULT SYSDATE NOT NULL,
  INSERTED_BY VARCHAR2(4000) NOT NULL,
  UPDATE_DATE DATE DEFAULT SYSDATE NOT NULL,
  UPDATED_BY VARCHAR2(4000) NOT NULL,
  PRIMARY KEY(S_CODE,INN)
);
comment on table  NDS2_MRR_USER.IDA_TAX_PAYER_INDIVIDUAL_ASSG IS '������ �� ��� ��������������� ����������� �� ������������';
comment on column  NDS2_MRR_USER.IDA_TAX_PAYER_INDIVIDUAL_ASSG.S_CODE IS 'SID ��������� ������������ � ����� ���������';
comment on column  NDS2_MRR_USER.IDA_TAX_PAYER_INDIVIDUAL_ASSG.INN IS '��� �� ��� ��������������� �����������';
comment on column  NDS2_MRR_USER.IDA_TAX_PAYER_INDIVIDUAL_ASSG.IS_ACTIVE IS '������� ���������� ������. ���������� ������ ��������� ������ ��� ����������� ����������� ������';
comment on column  NDS2_MRR_USER.IDA_TAX_PAYER_INDIVIDUAL_ASSG.INSERT_DATE IS '���� ���������� ������';
comment on column  NDS2_MRR_USER.IDA_TAX_PAYER_INDIVIDUAL_ASSG.INSERTED_BY IS '����� ������������, ������� ���� ������� ������';
comment on column  NDS2_MRR_USER.IDA_TAX_PAYER_INDIVIDUAL_ASSG.UPDATE_DATE IS '���� ���������� �������������� ������';
comment on column  NDS2_MRR_USER.IDA_TAX_PAYER_INDIVIDUAL_ASSG.UPDATED_BY IS '����� ������������,������� ��������� ������������ ������';


