CREATE TABLE NDS2_MRR_USER.IDA_TAX_PAYER_WHITE_LIST TableSpace NDS2_DATA
as
Select 
	INN, 
	KPP, 
	SONO_CODE, 
	NAME 
	From ( select
			row_number() over (partition by d.INNNP order by d.NOMKORR desc, z.DATAFAJLA asc) as rn,
			d.INNNP as INN,
			d.KPPNP AS KPP,
			d.KODNO as SONO_CODE,
			(case when length(d.INNNP) = 10 then d.NAIMORG else d.FAMILIYANP||' '||d.IMYANP||' '||d.OTCHESTVONP end) as NAME
			from NDS2_MRR_USER.v$askdekl d
				inner join NDS2_MRR_USER.v$askzipfajl z on z.ID = d.ZIP
			where d.PERIOD in ('23', '07', '08', '09', '77','78','79')) T 
	where T.rn = 1;
create index NDS2_MRR_USER.IDX_IDA_TP_INN on NDS2_MRR_USER.IDA_TAX_PAYER_WHITE_LIST(INN) TableSpace NDS2_IDX;

