create table NDS2_MRR_USER.EFFICIENCY_MONITOR_DECLARATION
(
  fiscal_year      VARCHAR2(4 CHAR),
  period           NUMBER,
  calc_date        DATE,
  sono_code        VARCHAR2(4 CHAR),
  deduction_amnt   NUMBER,
  calculation_amnt NUMBER
)
nologging;
