CREATE TABLE NDS2_MRR_USER.IDA_INSPECTOR_SONO_ASSG
(
  SID VARCHAR2(128) NOT NULL,
  S_CODE VARCHAR2(4) NOT NULL,
  IS_ACTIVE NUMBER(1) NOT NULL CHECK (IS_ACTIVE IN (0,1)),
  INSERT_DATE DATE DEFAULT SYSDATE NOT NULL,
  UPDATE_DATE DATE DEFAULT SYSDATE NOT NULL,
  INSERTED_BY VARCHAR2(4000),
  UPDATED_BY VARCHAR2(4000),
  PRIMARY KEY(SID,S_CODE)
);
comment on table  NDS2_MRR_USER.IDA_INSPECTOR_SONO_ASSG IS '������� �������� ����������� � ����';
comment on column  NDS2_MRR_USER.IDA_INSPECTOR_SONO_ASSG.SID IS '������������� ��������� ������������ � ����� ���������';
comment on column  NDS2_MRR_USER.IDA_INSPECTOR_SONO_ASSG.S_CODE IS '��� ���� �� ���� �� �������� ���������� ����������� ����������';
comment on column  NDS2_MRR_USER.IDA_INSPECTOR_SONO_ASSG.IS_ACTIVE IS '������� ���������� ������. ���������� ������ ��������� ������ ��� ����������� ����������� ������';
comment on column  NDS2_MRR_USER.IDA_INSPECTOR_SONO_ASSG.INSERT_DATE IS '���� ���������� ������';
comment on column  NDS2_MRR_USER.IDA_INSPECTOR_SONO_ASSG.UPDATE_DATE IS '���� ���������� �������������� ������';
comment on column  NDS2_MRR_USER.IDA_INSPECTOR_SONO_ASSG.INSERTED_BY IS '������������ ���������� ������';
comment on column  NDS2_MRR_USER.IDA_INSPECTOR_SONO_ASSG.UPDATED_BY IS '��������� ������������ ����������������� ������';
