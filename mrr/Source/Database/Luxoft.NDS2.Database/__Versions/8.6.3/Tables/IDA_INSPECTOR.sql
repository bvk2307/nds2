CREATE TABLE NDS2_MRR_USER.IDA_INSPECTOR
(
  SID VARCHAR2(128) NOT NULL PRIMARY KEY,
  NAME VARCHAR2(128) NOT NULL,
  EMPLOYEE_NUM VARCHAR2(128) NOT NULL,
  IS_ACTIVE NUMBER(1) NOT NULL CHECK (IS_ACTIVE IN (0,1)),
  INSERT_DATE DATE DEFAULT SYSDATE NOT NULL,
  INSERTED_BY VARCHAR2(4000) NOT NULL,
  UPDATE_DATE DATE DEFAULT SYSDATE NOT NULL,
  UPDATED_BY VARCHAR2(4000) NOT NULL
);
comment on table  NDS2_MRR_USER.IDA_INSPECTOR IS '������ ����������� ����';
comment on column  NDS2_MRR_USER.IDA_INSPECTOR.SID IS 'SID ��������� ������������ � ����� ���������';
comment on column  NDS2_MRR_USER.IDA_INSPECTOR.NAME IS '��� ����������';
comment on column  NDS2_MRR_USER.IDA_INSPECTOR.EMPLOYEE_NUM IS '��������� ����� ����������';
comment on column  NDS2_MRR_USER.IDA_INSPECTOR.IS_ACTIVE IS '������� ���������� ������. ���������� ������ ��������� ������ ��� ����������� ����������� ������';
comment on column  NDS2_MRR_USER.IDA_INSPECTOR.INSERT_DATE IS '���� ���������� ������';
comment on column  NDS2_MRR_USER.IDA_INSPECTOR.INSERTED_BY IS '����� ������������, ������� ���� ������� ������';
comment on column  NDS2_MRR_USER.IDA_INSPECTOR.UPDATE_DATE IS '���� ���������� �������������� ������';
comment on column  NDS2_MRR_USER.IDA_INSPECTOR.UPDATED_BY IS '����� ������������,������� ��������� ������������ ������';
