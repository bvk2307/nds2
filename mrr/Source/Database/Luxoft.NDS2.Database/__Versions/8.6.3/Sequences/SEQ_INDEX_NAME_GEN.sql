declare
v_exist number;
begin
  select count(1) into v_exist from all_objects where object_name='SEQ_INDEX_NAME_GEN' and owner = 'NDS2_MRR_USER';
  if v_exist = 0 then
    execute immediate 'create sequence NDS2_MRR_USER.SEQ_INDEX_NAME_GEN
start with 0
increment by 1
minvalue 0
maxvalue 999
nocache
cycle';
    end if;
end;

/
