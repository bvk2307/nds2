﻿declare
v_exists number;
begin
 select count(1) into v_exists from all_indexes where index_name = 'idx_Декл_ИдЗагр' and owner = 'NDS2_MC';
 if v_exists = 0 then
   execute immediate 'create index NDS2_MC."idx_Декл_ИдЗагр" on NDS2_MC."ASKДекл"("ИдЗагрузка") tablespace NDS2_IDX';
 end if;
end;
/
