﻿--Таблица для открытых КНП расхождений не прошедших шаг отбора расхождений;
begin
	execute immediate '
        create table NDS2_MRR_USER.EM_CHECK_LOST_DIS as 
			select * from NDS2_MRR_USER.sov_discrepancy where 1=0';
    exception when others then null;            
end;
/