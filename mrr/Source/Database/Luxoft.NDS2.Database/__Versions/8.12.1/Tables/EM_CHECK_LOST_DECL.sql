﻿-- Таблица для корерктировок отобранных на шаге агрегации данных, но не вошедших в V$EFFICIENCY_MONITOR_DETAIL
begin
	execute immediate '
        create table NDS2_MRR_USER.EM_CHECK_LOST_DECL as 
			select * from NDS2_MRR_USER.declaration_history where 1=0';
    exception when others then null;            
end;
/