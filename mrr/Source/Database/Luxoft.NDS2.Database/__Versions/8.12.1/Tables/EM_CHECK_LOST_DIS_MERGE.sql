﻿-- Таблица для открытых КНП расхождений отобранных на шаге отбора расхождений, но не прошедших шаг слияния с корректировками;
begin
	execute immediate '
		create table NDS2_MRR_USER.EM_CHECK_LOST_DIS_MERGE as 
			select * from NDS2_MRR_USER.sov_discrepancy where 1=0';
    exception when others then null;            
end;
/