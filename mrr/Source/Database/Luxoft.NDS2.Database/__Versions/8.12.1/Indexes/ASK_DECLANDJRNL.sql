﻿create index nds2_mrr_user.IDX_ASKDJ_LAST_DATE
	on nds2_mrr_user.ask_declandjrnl (innnp, kpp_effective, insert_date desc) tablespace nds2_idx;

insert into nds2_mrr_user.cfg_index (id, table_name, index_name, column_list, index_type, op_statistics, op_parallel_count, op_noparallel_postbuild)
values((select coalesce(max(id) + 1, 1) from nds2_mrr_user.cfg_index), 'V$ASK_DECLANDJRNL', 'IDX_ASKDJ_LAST_DATE', 'INNNP, KPP_EFFECTIVE, INSERT_DATE DESC', 'NONUNIQUE', 1, 4, 1);
