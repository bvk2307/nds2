﻿merge into NDS2_MRR_USER.CFG_TABLE_STAT trg
using (select 'KNP_DISCREPANCY_DECLARATION' as tabname, 'BASE_STARTEGY' as method from dual) src
   on (src.tabname = trg.TABLE_NAME)
when not matched then
insert values (src.tabname, src.method);

commit;