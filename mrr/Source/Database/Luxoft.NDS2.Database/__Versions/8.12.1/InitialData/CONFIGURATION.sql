﻿delete from NDS2_MRR_USER.CONFIGURATION where PARAMETER in ('restrict_by_region','auto_approve_selections', 'auto_send_selections', 'period_forming_autoselection',
 'period_sending_claim', 'send_autoclame_onepart', 'send_autoclame_twopart', 'send_autoreclame_onepart');
commit;

INSERT INTO NDS2_MRR_USER.CONFIGURATION (PARAMETER, VALUE, DEFAULT_VALUE, DESCRIPTION) values ('repeat_sending_claim_attempts', '3', '3', 'Количество повторных отправок');
INSERT INTO NDS2_MRR_USER.CONFIGURATION (PARAMETER, VALUE, DEFAULT_VALUE, DESCRIPTION) values ('repeat_sending_claim_timeout', '3', '3', 'Время ожидания ответа от СЭОД');
commit;

update NDS2_MRR_USER.CONFIGURATION set DEFAULT_VALUE = 6 where PARAMETER = 'timeout_claim_delivery';
update NDS2_MRR_USER.CONFIGURATION set DEFAULT_VALUE = 8 where PARAMETER = 'timeout_answer_for_autoclaim';
update NDS2_MRR_USER.CONFIGURATION set DEFAULT_VALUE = 8 where PARAMETER = 'timeout_answer_for_autooverclaim';
update NDS2_MRR_USER.CONFIGURATION set DEFAULT_VALUE = 3 where PARAMETER = 'timeout_input_answer_for_autooverclaim';
commit;