﻿create or replace package NDS2_MRR_USER.PAC$NAVIGATOR is

procedure P$GET_PAIRS (
  pRequestId in SOV_NAVIGATOR_REQUEST.REQUEST_ID%type,
  pResult out SYS_REFCURSOR
  );
  
procedure P$GET_STATUS (
  pRequestId in SOV_NAVIGATOR_REQUEST.REQUEST_ID%type,
  pStatus out SOV_NAVIGATOR_REQUEST.STATUS_ID%type
  );
  
procedure P$GET_DATA (
  pPairId in SOV_NAVIGATOR_PAIR.ID%type,
  pChainId in SOV_NAVIGATOR_CHAIN.ID%type,
  pResult out SYS_REFCURSOR);

end PAC$NAVIGATOR;
/
create or replace package body NDS2_MRR_USER.PAC$NAVIGATOR is

procedure P$GET_PAIRS (
  pRequestId in SOV_NAVIGATOR_REQUEST.REQUEST_ID%type,
  pResult out SYS_REFCURSOR
  )
as
begin
  
  open pResult for 
       select
               pair.id as pair_id,
               chain.id as chain_id,
               head.inn as head_taxpayer_inn,
               head.name as head_taxpayer_name,
               tail.inn as tail_taxpayer_inn,
               tail.name as tail_taxpayer_name,
               chain.chainnumber as chain_number,
               chain_summary.targets_quantity as chain_targets_quantity,
               chain_summary.total_quantity - chain_summary.targets_quantity as chain_links            
       from 
               SOV_NAVIGATOR_PAIR pair
               join SOV_NAVIGATOR_CHAIN chain on chain.pair_id = pair.id
               join SOV_NAVIGATOR_CONTRACTOR head on head.id = pair.head_contractor_id
               join SOV_NAVIGATOR_CONTRACTOR tail on tail.id = pair.tail_contractor_id
               join (
                    select 
                        chain_tp.chain_id, 
                        sum(1) as total_quantity,
                        sum(case when tp.is_target = 1 then 1 else 0 end) as targets_quantity
                    from 
                        SOV_NAVIGATOR_CHAIN_CONTRACTOR chain_tp
                        join SOV_NAVIGATOR_CONTRACTOR tp on tp.id = chain_tp.contractor_id
                    group by chain_tp.chain_id
               ) chain_summary on chain_summary.chain_id = chain.id               
       where
               pair.request_id = pRequestId;

end;

procedure P$GET_STATUS (
  pRequestId in SOV_NAVIGATOR_REQUEST.REQUEST_ID%type,
  pStatus out SOV_NAVIGATOR_REQUEST.STATUS_ID%type
  )
as
begin
  select status_id into pStatus from SOV_NAVIGATOR_REQUEST where request_id = pRequestId;
end;

procedure P$GET_DATA (
  pPairId in SOV_NAVIGATOR_PAIR.ID%type,
  pChainId in SOV_NAVIGATOR_CHAIN.ID%type,
  pResult out SYS_REFCURSOR)
as
begin

  open pResult for
  select
          tp.*,
          chain.id as chain_id,
          chain.chainnumber as chain_number,
          chain.min_mapped_amountt as min_mapped_amount,
          chain.min_not_mapped_amount,
          chain.max_mapped_amount,
          chain.max_not_mapped_amount,
          case when chain_tp.order_number = 0 then 1 else 0 end as head,
          case when next_tp.id is null then 1 else 0 end as tail,
          chain_tp.order_number,
          deal_buyer.mapped_amount as mapped_amount_as_buyer,
          deal_buyer.amount_per_buyer as amount_per_buyer_as_buyer,
          deal_buyer.amount_per_seller as amount_per_seller_as_buyer,
          deal_seller.mapped_amount as mapped_amount_as_seller,
          deal_seller.amount_per_buyer as amount_per_buyer_as_seller,
          deal_seller.amount_per_seller as amount_per_seller_as_seller
  from
          SOV_NAVIGATOR_CHAIN chain
          join SOV_NAVIGATOR_CHAIN_CONTRACTOR chain_tp on chain_tp.chain_id = chain.id
          join SOV_NAVIGATOR_CONTRACTOR tp on tp.id = chain_tp.contractor_id
          left join SOV_NAVIGATOR_CHAIN_CONTRACTOR next_chain_tp 
               on next_chain_tp.chain_id = chain.id and next_chain_tp.order_number = chain_tp.order_number + 1
          left join SOV_NAVIGATOR_CONTRACTOR next_tp on next_tp.id = next_chain_tp.contractor_id
          left join SOV_NAVIGATOR_DEAL deal_buyer 
            on deal_buyer.buyer_id = chain_tp.contractor_id 
               and deal_buyer.seller_id = next_chain_tp.contractor_id
          left join SOV_NAVIGATOR_DEAL deal_seller
            on deal_seller.buyer_id = next_chain_tp.contractor_id
               and deal_seller.seller_id = chain_tp.contractor_id
   where
               (pChainId is null or chain.id = pChainId) 
           and (pPairId is null or chain.pair_id = pPairId)
   order by
         tp.name, chain.chainnumber; 

end;

end PAC$NAVIGATOR;
/
