drop  materialized view NDS2_MRR_USER.MV$DECLARATIONINSPECT;
create materialized view NDS2_MRR_USER.MV$DECLARATIONINSPECT
build DEFERRED 
refresh force on demand
as
select
	seod.IS_ACTIVE
    ,seod.nds2_id as ID
    ,sd.zip as declaration_version_id
    ,seod.TYPE as DECL_TYPE_CODE
    ,DECODE(seod.TYPE, 0, '����������', 1, '������', '') as DECL_TYPE
    ,case when sovd.DECLARATION_VERSION_ID is not null then 2 else (case when  sd.ID is not null then 1 else 0 end) end as ProcessingStage
    ,case
       when sovd.declaration_version_id is null then null
       else case when seod.TYPE=0 and (knp.knp_id IS NULL or knp.completion_date is null) then '�������'
                 when seod.TYPE=0 and (knp.knp_id IS NOT NULL and knp.completion_date is not null) then '�������'
                 else NULL end
       end as STATUS_KNP
    ,case
       when sovd.declaration_version_id is null then null
       else case when cnt.BUYBOOK_DK_OTHR_CNT + cnt.BUYBOOK_DK_OTHR_CNT > 0 then '����'
       else '���' end end as STATUS
    ,nvl(esur.sign_code, 4) as SUR_CODE
    ,seod.inn as INN
    ,seod.kpp as KPP
    ,vtp.NAME
    ,case
       when seod.TYPE=0 then seod.decl_reg_num
       else null end as SEOD_DECL_ID
    ,seod.TAX_PERIOD
    ,seod.FISCAL_YEAR
    ,dtp.description||' '||seod.FISCAL_YEAR as FULL_TAX_PERIOD
    ,case when seod.TYPE=0 then nvl(sd.DATADOK, seod.EOD_DATE) else null end as DECL_DATE
    ,case
       when seod.TYPE=0 then seod.CORRECTION_NUMBER
       else null end as CORRECTION_NUMBER
    ,case
      when sd.SUMPU173_5 > 0 then sd.SUMPU173_5
      when sd.SUMPU173_1 > 0 then sd.SUMPU173_1
      when sd.SUMPU173_1 < 0 then sd.SUMPU173_1
      else null end as COMPENSATION_AMNT_SIGN
    ,dsd.NDS_AMOUNT
    ,cnt.BUYBOOK_CONTRAGENT_CNT
    ,cnt.BUYBOOK_DK_GAP_CAGNT_CNT
    ,cnt.BUYBOOK_DK_GAP_CNT
    ,cnt.BUYBOOK_DK_GAP_AMNT
    ,cnt.BUYBOOK_DK_OTHR_CAGNT_CNT
    ,cnt.BUYBOOK_DK_OTHR_CNT
    ,cnt.BUYBOOK_DK_OTHR_AMNT
    ,seod.SONO_CODE as SOUN_CODE
    ,substr(sd.KODNO,1,2) as REGION_CODE
from
(
   select 
    decode((row_number() over (partition by seod.TAX_PERIOD, seod.FISCAL_YEAR, seod.INN order by seod.CORRECTION_NUMBER desc)),1,1,0) as IS_ACTIVE,
    seod.*
   from seod_declaration seod
) seod
inner join DICT_TAX_PERIOD dtp on dtp.code = seod.TAX_PERIOD
left join V$ASK_DECLANDJRNL sd on SD.PERIOD = seod.TAX_PERIOD and SD.OTCHETGOD = seod.FISCAL_YEAR and SD.INNNP = seod.INN and SD.NOMKORR = seod.CORRECTION_NUMBER
left join nds2_mrr_user.seod_knp knp on knp.declaration_reg_num = seod.decl_reg_num
left join sov_declaration_info sovd on SOVD.DECLARATION_VERSION_ID = SD.ZIP
left join v$taxpayer vtp on vtp.inn = seod.inn
left join EXT_SUR esur on esur.inn = seod.INN and seod.TAX_PERIOD = esur.fiscal_period and seod.FISCAL_YEAR = esur.fiscal_year
left join 
(
  select
    ZIP,
    sum(case TipFajla when '0' then SumNDSPok when '2' then SumNDSPokDL else 0 end) as NDS_AMOUNT
  from nds2_mrr_user.v$asksvodzap
  group by ZIP
) dsd on dsd.zip = sd.zip
left join 
(
    select
        t.DECLARATION_VERSION_ID,
        COUNT(*) AS BUYBOOK_CONTRAGENT_CNT,
        SUM(case when t.GAPS = 0 then 0 else 1 end) AS BUYBOOK_DK_GAP_CAGNT_CNT,
        SUM(t.GAPS) as BUYBOOK_DK_GAP_CNT,
        SUM(t.GAPS_AMT) as BUYBOOK_DK_GAP_AMNT,
        SUM(case when t.OTHRS = 0 then 0 else 1 end) AS BUYBOOK_DK_OTHR_CAGNT_CNT,
        SUM(t.OTHRS) as BUYBOOK_DK_OTHR_CNT,
        SUM(t.OTHRS_AMT) as BUYBOOK_DK_OTHR_AMNT
    from (
         select
             decl.DECLARATION_VERSION_ID,
             inv.seller_inn,
             inv.seller_kpp,
             SUM(case ds.type when 1 then 1 else 0 end) as GAPS,
             SUM(case when ds.type=1 then 0 when ds.type IS NULL then 0 else 1 end) as OTHRS,
             SUM(case ds.type when 1 then ds.amnt else 0 end) as GAPS_AMT,
             SUM(case when ds.type=1 then 0 when ds.type IS NULL then 0 else ds.amnt end) as OTHRS_AMT
         from nds2_mrr_user.sov_declaration_info decl
         join nds2_mrr_user.stage_invoice inv on inv.DECLARATION_VERSION_ID = decl.DECLARATION_VERSION_ID
                                              and inv.chapter = 8
         left join nds2_mrr_user.sov_discrepancy ds on ds.invoice_rk = inv.row_key
        group by decl.DECLARATION_VERSION_ID, inv.seller_inn, inv.seller_kpp
         ) t
    group by t.DECLARATION_VERSION_ID
) cnt on cnt.declaration_version_id = sd.zip
where seod.IS_ACTIVE = 1;
