﻿drop materialized view NDS2_MRR_USER.V$ASK_DECLANDJRNL;
create materialized view NDS2_MRR_USER.V$ASK_DECLANDJRNL
as
select
ID,								ZIP,							IDFAJL,							VERSPROG,						VERSFORM, 
0 as TYPE,						PRIZNNAL8_12,					PRIZNNAL8,						PRIZNNAL81,						PRIZNNAL9,				
PRIZNNAL91,						PRIZNNAL10,						PRIZNNAL11,						PRIZNNAL12,						KND,					
DATADOK,						PERIOD,							OTCHETGOD,						KODNO,							to_number(NOMKORR) as NOMKORR,				
POMESTU,						OKVED,							TLF,							NAIMORG,						INNNP,					
KPPNP,							FORMREORG,						INNREORG,						KPPREORG,						FAMILIYANP,				
IMYANP,							OTCHESTVONP,					PRPODP,							FAMILIYAPODP,					IMYAPODP,				
OTCHESTVOPODP,					NAIMDOK,						NAIMORGPRED,					OKTMO,							KBK,				
SUMPU173_5,						SUMPU173_1,						NOMDOGIT,						DATADOGIT,						DATANACHDOGIT,			
DATAKONDOGIT,					NALPU164,						NALVOSSTOBSHH,					REALTOV18NALBAZA,				REALTOV18SUMNAL,		
REALTOV10NALBAZA,				REALTOV10SUMNAL,				REALTOV118NALBAZA,				REALTOV118SUMNAL,				REALTOV110NALBAZA,		
REALTOV110SUMNAL,				REALPREDIKNALBAZA,				REALPREDIKSUMNAL,				VYPSMRSOBNALBAZA,				VYPSMRSOBSUMNAL,		
OPLPREDPOSTNALBAZA,				OPLPREDPOSTSUMNAL,				SUMNALVS,						SUMNAL170_3_5,					SUMNAL170_3_3,			
KORREALTOV18NALBAZA,			KORREALTOV18SUMNAL,				KORREALTOV10NALBAZA,			KORREALTOV10SUMNAL,				KORREALTOV118NALBAZA,	
KORREALTOV118SUMNAL,			KORREALTOV110NALBAZA,			KORREALTOV110SUMNAL,			KORREALPREDIKNALBAZA,			KORREALPREDIKSUMNAL,	
NALPREDNPPRIOB,					NALPREDNPPOK,					NALISCHSMR,						NALUPLTAMOZH,					NALUPLNOTOVTS,			
NALISCHPROD,					NALUPLPOKNA,					NALVYCHOBSHH,					SUMISCHISLITOG,					SUMVOZMPDTV,			
SUMVOZMNEPDTV,					SUMNAL164IT,					NALVYCHNEPODIT,					NALISCHISLIT,					SUMOPER1010449KODOPER,	
SUMOPER1010449NALBAZA,			SUMOPER1010449KORISCH,			SUMOPER1010449NALVOSST,			OPLPOSTSV6MES,					NAIMKNPOK,				
NAIMKNPOKDL,					NAIMKNPROD,						NAIMKNPRODDL,					NAIMZHUCHVYSTSCHF,				NAIMZHUCHPOLUCHSCHF,	
NAIMVYSTSCHF173_5,				IdZagruzka,						KodOper47,						NalBaza47,						NalVosst47,				
KodOper48,						KorNalBazaUv48,					KorNalBazaUm48,					KodOper50,						KorNalBazaUv50,			
KorIschUv50,					KorNalBazaUm50,					KorIschUm50
from NDS2_MRR_USER.V$ASKDEKL
union
select
jr.ID,								jr.ZIP,						  jr.IDFAJL,              jr.VERSPROG,            jr.VERSFORM, 
1 as TYPE,            null as PRIZNNAL8_12,      null as PRIZNNAL8,        null as PRIZNNAL81,        null as PRIZNNAL9,
null as PRIZNNAL91,        null as PRIZNNAL10,        null as PRIZNNAL11,        null as PRIZNNAL12,        KND,
null as DATADOK,        PERIOD,              OTCHETGOD,            '5252' as KODNO,        decode((row_number() over (partition by PERIOD, OTCHETGOD, INN order by zFile.DATAFAJLA asc) - 1),1,1,0) as NOMKORR,
'116' as POMESTU,        null as OKVED,          null as TLF,          jr.NAIMORG,            jr.INN as INNNP,
KPP as KPPNP,          null as FORMREORG,        null as INNREORG,        null as KPPREORG,        null as FAMILIYANP,
null as IMYANP,          null as OTCHESTVONP,      jr.PRPODP,              jr.FAMILIYAPODP,          jr.IMYAPODP,
jr.OTCHESTVOPODP,          jr.NAIMDOK,            null as NAIMORGPRED,      null as OKTMO,          null as KBK,
null as SUMPU173_5,        null as SUMPU173_1,        null as NOMDOGIT,        null as DATADOGIT,        null as DATANACHDOGIT,
null as DATAKONDOGIT,      null as NALPU164,        null as NALVOSSTOBSHH,      null as REALTOV18NALBAZA,    null as REALTOV18SUMNAL,
null as REALTOV10NALBAZA,    null as REALTOV10SUMNAL,    null as REALTOV118NALBAZA,    null as REALTOV118SUMNAL,    null as REALTOV110NALBAZA,
null as REALTOV110SUMNAL,    null as REALPREDIKNALBAZA,    null as REALPREDIKSUMNAL,    null as VYPSMRSOBNALBAZA,    null as VYPSMRSOBSUMNAL,
null as OPLPREDPOSTNALBAZA,    null as OPLPREDPOSTSUMNAL,    null as SUMNALVS,        null as SUMNAL170_3_5,      null as SUMNAL170_3_3,
null as KORREALTOV18NALBAZA,  null as KORREALTOV18SUMNAL,    null as KORREALTOV10NALBAZA,  null as KORREALTOV10SUMNAL,    null as KORREALTOV118NALBAZA,
null as KORREALTOV118SUMNAL,  null as KORREALTOV110NALBAZA,  null as KORREALTOV110SUMNAL,  null as KORREALPREDIKNALBAZA,  null as KORREALPREDIKSUMNAL,
null as NALPREDNPPRIOB,      null as NALPREDNPPOK,      null as NALISCHSMR,        null as NALUPLTAMOZH,      null as NALUPLNOTOVTS,
null as NALISCHPROD,      null as NALUPLPOKNA,      null as NALVYCHOBSHH,      null as SUMISCHISLITOG,      null as SUMVOZMPDTV,
null as SUMVOZMNEPDTV,      null as SUMNAL164IT,      null as NALVYCHNEPODIT,      null as NALISCHISLIT,      null as SUMOPER1010449KODOPER,
null as SUMOPER1010449NALBAZA,  null as SUMOPER1010449KORISCH,  null as SUMOPER1010449NALVOSST,  null as OPLPOSTSV6MES,      null as NAIMKNPOK,
null as NAIMKNPOKDL,      null as NAIMKNPROD,        null as NAIMKNPRODDL,      null as NAIMZHUCHVYSTSCHF,    null as NAIMZHUCHPOLUCHSCHF,
null as NAIMVYSTSCHF173_5,    null as  IdZagruzka,        null as  KodOper47,        null as  NalBaza47,        null as NalVosst47,
null as KodOper48,        null as  KorNalBazaUv48,      null as  KorNalBazaUm48,      null as  KodOper50,        null as KorNalBazaUv50,
null as  KorIschUv50,      null as  KorNalBazaUm50,      null as  KorIschUm50
from NDS2_MRR_USER.V$ASKJOURNALUCH jr inner join NDS2_MRR_USER.V$askzipfajl zFile on zFile.ID = jr.ZIP;
