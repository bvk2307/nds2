create sequence NDS2_MRR_USER.SEQ_SOV_NAVIGATOR_REQUEST
minvalue 0
maxvalue 9999999999
start with 1
increment by 1
nocache;

create sequence NDS2_MRR_USER.SEQ_REPORT_DYNAMIC_PARAMETERS
minvalue 1
maxvalue 9999999999
start with 1
increment by 1
nocache;

create sequence NDS2_MRR_USER.SEQ_REPORT_DYNAMIC_PERIOD
minvalue 1
maxvalue 9999999999
start with 1
increment by 1
nocache;

 CREATE SEQUENCE  "NDS2_MRR_USER"."SEL_AUTO_FILTER_PAR_SEQ"  
 MINVALUE 0 
 MAXVALUE 9999999999 
 INCREMENT BY 1 
 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;
 
 
CREATE SEQUENCE  "NDS2_MRR_USER"."PVP_STATE_ID_SEQ"  
MINVALUE 1 
MAXVALUE 9999999999 
INCREMENT BY 1 
START WITH 1 NOCACHE  NOORDER  NOCYCLE;

create sequence NDS2_MRR_USER.SEQ_REPORT_DYNAMIC_DECLARATION
minvalue 1
maxvalue 9999999999
start with 1
increment by 1
nocache;

 create sequence NDS2_MRR_USER.SEQ_SOV_NAVIGATOR_CONTRACTOR
minvalue 0
maxvalue 9999999999
start with 1
increment by 1
nocache;


create sequence NDS2_MRR_USER.SEQ_SOV_NAVIGATOR_DEAL
minvalue 0
maxvalue 9999999999
start with 1
increment by 1
nocache;

create sequence NDS2_MRR_USER.SEQ_SOV_NAVIGATOR_CHAIN
minvalue 0
maxvalue 9999999999
start with 1
increment by 1
nocache;

create sequence NDS2_MRR_USER.SEQ_SOV_NAVIGATOR_PAIR
minvalue 0
maxvalue 9999999999
start with 1
increment by 1
nocache;

create sequence NDS2_MRR_USER.SEQ_REPORT_INFO_RES_MATCHING
minvalue 1
maxvalue 9999999999
start with 1
increment by 1
nocache;

create sequence NDS2_MRR_USER.SEQ_SOV_MATCH_RESULT_SUMMARY
minvalue 0
maxvalue 9999999999
start with 1
increment by 1
nocache;
