﻿create or replace force view NDS2_MRR_USER.V$TECHNO_REPORT_PARAMETER as
select id, name, data_type 
from TECHNO_REPORT_PARAMETER
union select 995, 'Среднее количество записей в декларации', 'NUMBER_SUMMARY' from dual
union select 996, 'Количестово успешно сопоставленных записей', 'NUMBER' from dual
union select 997, 'Время сопоставления', 'DATE' from dual;