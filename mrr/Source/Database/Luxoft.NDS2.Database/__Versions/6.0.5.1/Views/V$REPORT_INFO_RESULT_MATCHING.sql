﻿create or replace force view NDS2_MRR_USER.V$REPORT_INFO_RESULT_MATCHING AS
SELECT
	r.TASK_ID
	,r.aggregate_name						
	,r.primary_declaration_count     
	,r.correction_declaration_count  
	,r.decl_with_mediator_ops_count  
	,r.journal_count                 
	,r.invoice_record_count          
	,r.invoice_record_count_chapter8 
	,r.invoice_record_count_chapter9  
	,r.invoice_record_count_chapter10 
	,r.invoice_record_count_chapter11 
	,r.invoice_record_count_chapter12 
	,r.exact_match_count              
	,r.prim_decl_with_mismatch_count  
	,r.corr_decl_with_mismatch_count  
	,r.decl_with_kc_mismatch_count    
	,r.mismatch_total_count           
	,r.mismatch_total_sum             
	,r.weak_and_nds_count             
	,r.weak_and_nds_sum               
	,r.weak_and_no_nds_count          
	,r.exact_and_nds_count            
	,r.exact_and_nds_sum              
	,r.gap_count                      
	,r.gap_amount                     
	,r.currency_count                 
	,r.currency_amount                
	,r.mismatch_tno_count			 
	,r.mismatch_tno_sum	
	,r.weight_mismatch_in_total_oper			 
from NDS2_MRR_USER.REPORT_INFO_RESULT_MATCHING r
where r.ACTUAL_NUM = 1
order by r.federal_district, r.i_summary, r.region_code, r.soun_code;