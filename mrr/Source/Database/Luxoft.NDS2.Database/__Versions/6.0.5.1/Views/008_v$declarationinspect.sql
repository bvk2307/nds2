﻿create or replace force view NDS2_MRR_USER.v$declarationinspect as
select
       mdi."ID",
       mdi."DECLARATION_VERSION_ID",
       case when mdi.ProcessingStage != 2 then case when do.inspector_sid is not null then null else 'Н' end else rm.record_mark end as RECORD_MARK,
       mdi."DECL_TYPE_CODE",
       mdi."DECL_TYPE",
       mdi."PROCESSINGSTAGE",
       mdi."STATUS_KNP",
       mdi."STATUS",
       mdi."IS_ACTIVE",
       mdi."SUR_CODE",
       mdi."INN",
       mdi."KPP",
       mdi."NAME",
       mdi."SEOD_DECL_ID",
       mdi."TAX_PERIOD",
       mdi."FISCAL_YEAR",
       mdi."FULL_TAX_PERIOD",
       mdi."DECL_DATE",
       mdi."CORRECTION_NUMBER",
       mdi."COMPENSATION_AMNT_SIGN",
       mdi."NDS_AMOUNT",
       mdi."BUYBOOK_CONTRAGENT_CNT",
       mdi."BUYBOOK_DK_GAP_CAGNT_CNT",
       mdi."BUYBOOK_DK_GAP_CNT",
       mdi."BUYBOOK_DK_GAP_AMNT",
       mdi."BUYBOOK_DK_OTHR_CAGNT_CNT",
       mdi."BUYBOOK_DK_OTHR_CNT",
       mdi."BUYBOOK_DK_OTHR_AMNT",
       mdi."SOUN_CODE",
       mdi."REGION_CODE"
       ,do.inspector_name as INSPECTOR
       ,do.inspector_sid as INSPECTOR_SID
from MV$declarationinspect mdi
left join nds2_mrr_user.DECLARATION_OWNER do on do.declaration_id = mdi.id
left join v$declaration_record_mark rm on rm.declaration_version_id = mdi.declaration_version_id;

