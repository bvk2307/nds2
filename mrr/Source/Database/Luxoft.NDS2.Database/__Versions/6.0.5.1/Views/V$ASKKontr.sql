﻿create or replace force view NDS2_MRR_USER.V$ASKKontrSoontosh
as
select 
"Ид" as ID, 
	"ИдДекл" as IdDekl, 
	"КодКС" as KodKs, 
	"ДатаРасчета" as DataRascheta,
	"ЛеваяЧасть" as LevyaChast, 
	"ПраваяЧасть" as PravyaChast,
    "Выполн" as Vypoln
from NDS2_MC."ASKКонтрСоотн";
