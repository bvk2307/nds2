﻿CREATE OR REPLACE FORCE VIEW NDS2_MRR_USER.V$SELECTION_DISCREPANCY AS
select
  dis.id,
  dis_ref.request_id                as RequestId,
  dis.id                            as DiscrepancyId, /*ММ - избыточно, убрать после сборки*/
  dis.sov_id						as DiscrepancySovId,
  dis.CREATE_DATE                   as FoundDate,
  dis.INVOICE_RK                    as invoice_id,
  dis.INVOICE_CONTRACTOR_RK         as contractor_invoice_id,
  dis.type                          as typeCode,
  dType.s_Name                      as Type,
  d1.id                             as declaration_id,
  d1.declaration_version_id         as declaration_version_id,
  d2.declaration_version_id         as contr_declaration_version_id,
  d1.inn                            as TaxPayerInn,
  d1.kpp                            as TaxPayerKpp,
  d1.name                           as TaxPayerName,
  --ТНО
  d1.soun_code								as TaxPayerSounCode,
  d1.soun_code || ' - ' || d1.soun_name		as TaxPayerSoun,
  --Регион
  d1.region_code								as TaxPayerRegionCode,
  d1.region_code || ' - ' || d1.region_name		as TaxPayerRegion,
  --Период
  d1.tax_period												as TaxPayerPeriodCode,
  d1.FISCAL_YEAR											as TaxPayerYearCode,
  dtaxper1.description || ' ' || d1.FISCAL_YEAR || ' г.'	as TaxPayerPeriod,
  d1.correction_number										as CorrectionNumber,
  case when dis.Invoice_Chapter is null then null else
  'Раздел ' || dis.Invoice_Chapter  ||
  case dis.Invoice_Chapter
    when 8 then ' (КнПок)'
    when 9 then ' (КнПрод)'
    when 10 then ' (ЖвСФ)'
    when 11 then ' (ЖпСФ)'
    when 12 then ' (КнПрод)'
    else ''
  end end													as TaxPayerSource,
  --Признак
  d1.decl_sign												as TaxPayerDeclSign,
  --Категория НП
  d1.Category												as TaxPayerCategoryCode,
  decode(d1.Category, 0, '', 1, 'Крупнейший', 'Неизвестно') as TaxPayerCategory,
  --###Контрагент###
  d2.sur_code                       as ContractorSur,
  d2.inn                            as ContractorInn,
  d2.kpp                            as ContractorKpp,
  d2.name                           as ContractorName,
  --ТНО
  d2.soun_code              as ContractorSounCode,
  d2.soun_code || ' - ' || d2.soun_name  as ContractorSoun,
  --Регион
  d2.region_code              as ContractorRegionCode,
  d2.region_code || ' - ' || d2.region_name  as ContractorRegion,
  --Период
  d2.tax_period                     as ContractorPeriodCode,
  d2.FISCAL_YEAR                    as ContractorYearCode,
  dtaxper2.description || ' ' || d2.FISCAL_YEAR as ContractorPeriod,
  case when dis.type = 1 or dis.Invoice_Contractor_Chapter is null then null else
    'Раздел ' || dis.Invoice_Contractor_Chapter ||
    case dis.Invoice_Contractor_Chapter
    when 8 then ' (КнПок)'
    when 9 then ' (КнПрод)'
    when 10 then ' (ЖвСФ)'
    when 11 then ' (ЖпСФ)'
    when 12 then ' (КнПрод)'
    else ''
    end end as ContractorSource,
  d2.correction_number              as ContractorCorrectionNumber,
  --Признак
  d2.decl_sign                      as ContractorDeclSign,
  --Категория НП
  d2.Category                       as ContractorCategoryCode,
  decode(d2.Category, 0, '', 1, 'Крупнейший', 'Неизвестно') ContractorCategory,
  dis.amnt                          as Amount,
  dis.amount_pvp                    as AmountPVP,
  dis.course,
  dis.course_cost					as CourseCost,
  dis.sur_code						as SURCode,
  dis.status                        as StatusCode,
  dis_ref.IS_IN_PROCESS             as ischecked,
  dis_ref.IS_IN_PROCESS             as selected,
  dStatus.name                      as Status,
  null as CLAIM_ID,
  null as sync_date,
  null as Claim_status,
  dStage.STAGE_CODE                 as STAGE_CODE,
  dStage.STAGE_NAME                 as STAGE_NAME,
  dStage.STAGE_STATUS_CODE          as STAGE_STATUS_CODE,
  dStage.STAGE_STATUS_NAME          as STAGE_STATUS_NAME,
  --непонятно
  sysdate                           as CalculationDate,
  d2.DECL_TYPE						as ContractorDocType,
  decode(d2.DECL_TYPE_CODE, 0, d2.COMPENSATION_AMNT_SIGN, null) as SUM_NDS,
  decode(d2.DECL_TYPE_CODE, 0, d2.STATUS, null)					as STATUS_KNP,
  dcomprule.code												as COMPARE_RULE_CODE,
  dcomprule.description											as COMPARE_RULE
  from SELECTION_DISCREPANCY dis_ref
  inner join SOV_DISCREPANCY dis on dis.id = dis_ref.discrepancy_id
  inner join dict_discrepancy_status dStatus on dStatus.id = dis.status
  inner join V$Declaration_History d1 on d1.id = dis.decl_id and d1.is_active = 1
  left join V$Declaration_History d2 on d2.id = dis.decl_contractor_id and d2.is_active = 1
  inner join DICT_DISCREPANCY_TYPE dType on dType.s_Code = dis.type
  inner join v$discrepancy_stage dStage on (dStage.Id = dis_ref.discrepancy_id) and (dStage.rq_id = dis_ref.request_id)
  left join DICT_TAX_PERIOD dtaxper1 on dtaxper1.code = d1.tax_period
  left join DICT_TAX_PERIOD dtaxper2 on dtaxper2.code = d2.tax_period
  left join DICT_COMPARE_RULES dcomprule on dcomprule.code = dis.RULE_NUM;