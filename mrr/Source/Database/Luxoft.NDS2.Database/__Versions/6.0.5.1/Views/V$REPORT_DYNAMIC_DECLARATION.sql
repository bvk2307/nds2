﻿create or replace force view NDS2_MRR_USER.V$REPORT_DYNAMIC_DECLARATION AS
SELECT
    r.TASK_ID
    ,r.Period as Period
    ,r.P01 as P01
    ,r.P02 as P02
    ,r.P03 as P03
    ,r.P04 as P04
    ,r.P05 as P05
    ,r.P06 as P06
    ,r.P07 as P07
    ,r.P08 as P08
    ,r.P09 as P09
    ,r.P10 as P10
    ,r.P11 as P11
    ,r.P12 as P12
    ,r.P13 as P13
    ,r.P14 as P14
from NDS2_MRR_USER.REPORT_DYNAMIC_DECLARATION r
where r.ACTUAL_NUM = 1
order by r.Period_Rank;