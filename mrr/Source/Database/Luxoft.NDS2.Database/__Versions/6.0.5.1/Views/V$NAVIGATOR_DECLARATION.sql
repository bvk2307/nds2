create or replace force view NDS2_MRR_USER.V$NAVIGATOR_DECLARATION
as
select
       ask.innnp,
       ask.kppnp,
       sov.sur_code,
       sov.ch8_deals_amnt_total,
       sov.ch9_deals_amnt_total,
       sov.ch8_nds,
       sov.ch9_nds,
       100 * ask.otchetgod + ask.period as Period,
       decode((row_number() over (partition by ask.PERIOD, ask.OTCHETGOD, ask.INNNP order by ask.zip desc)),1,1,0) as is_active
from
       SOV_DECLARATION_INFO sov
       join V$ASK_DECLANDJRNL ask on ask.zip = sov.declaration_version_id;
