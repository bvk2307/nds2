﻿create or replace force view NDS2_MRR_USER.V$REPORT_DYNAMIC_PARAMETERS AS
SELECT
    r.TASK_ID
    ,r.Period as Period
    ,r.P01 as P01
    ,r.P02 as P02
    ,r.P03 as P03
    ,r.P04 as P04
    ,r.P05 as P05
    ,r.P06 as P06
    ,r.P07 as P07
    ,r.P08 as P08
    ,r.P09 as P09
    ,r.P10 as P10
    ,r.P11 as P11
    ,r.P12 as P12
    ,r.P13 as P13
    ,r.P14 as P14
    ,r.P15 as P15
    ,r.P16 as P16
    ,r.P17 as P17
    ,r.P18 as P18
    ,r.P19 as P19
    ,r.P20 as P20
    ,r.P21 as P21
    ,r.P22 as P22
    ,r.P23 as P23
    ,r.P24 as P24
    ,r.P25 as P25
    ,r.P26 as P26
    ,r.P27 as P27
    ,r.P28 as P28
    ,r.P29 as P29
    ,r.P30 as P30
from NDS2_MRR_USER.REPORT_DYNAMIC_PARAMETERS r
where r.ACTUAL_NUM = 1
order by r.Period_Rank;