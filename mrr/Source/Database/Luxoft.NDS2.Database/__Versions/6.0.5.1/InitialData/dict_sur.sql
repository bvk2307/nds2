﻿truncate table NDS2_MRR_USER.dict_sur;
insert into NDS2_MRR_USER.dict_sur (CODE,COLOR_ARGB,IS_DEFAULT,DESCRIPTION) values (1, -65536, 0, 'Высокий риск');
insert into NDS2_MRR_USER.dict_sur (CODE,COLOR_ARGB,IS_DEFAULT,DESCRIPTION) values (2, -256, 0, 'Средний риск');
insert into NDS2_MRR_USER.dict_sur (CODE,COLOR_ARGB,IS_DEFAULT,DESCRIPTION) values (3, -6632142, 0, 'Низкий риск');
insert into NDS2_MRR_USER.dict_sur (CODE,COLOR_ARGB,IS_DEFAULT,DESCRIPTION) values (4, -8943463, 1, 'Неопределенный');
commit;
