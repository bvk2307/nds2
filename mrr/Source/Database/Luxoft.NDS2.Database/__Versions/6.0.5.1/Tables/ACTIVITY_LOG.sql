BEGIN
  NDS2_MRR_USER.NDS2$SYS.DROP_IF_EXISTS('NDS2_MRR_USER', 'ACTIVITY_LOG');
  NDS2_MRR_USER.NDS2$SYS.DROP_IF_EXISTS('NDS2_MRR_USER', 'ACTIVITY_LOG_REPORT');
  NDS2_MRR_USER.NDS2$SYS.DROP_IF_EXISTS('NDS2_MRR_USER', 'DICT_ACTIVITY_LOG_TYPE');
END;
/

CREATE TABLE NDS2_MRR_USER.ACTIVITY_LOG --��� �������
(
  ACTIVITY_ID NUMBER(3) not null,
  TIMESTAMP DATE not null, --������ � ���� ������� ����� ������������� ������ � ��, ����� ��������� �������� �� ������� �������� 
  ELAPSED_MILLISEC NUMBER(19),
  COUNT NUMBER(19)
);

CREATE TABLE NDS2_MRR_USER.DICT_ACTIVITY_LOG_TYPE --��� �������
(
  ID number(3) not null,
  NAME VARCHAR2(256 CHAR)
);
alter table NDS2_MRR_USER.DICT_ACTIVITY_LOG_TYPE add constraint PK_DICT_ACTLOG_TYPE_ID primary key (ID);

--drop table NDS2_MRR_USER.ACTIVITY_LOG_REPORT;
create table NDS2_MRR_USER.ACTIVITY_LOG_REPORT
(
  begin date,
  end date,
  logons number,
  decl_open number,
  decl_size number,
  decl_ext_open number,
  man_sel_call number,
  reports number,
  decl_time number,
  decl_sov_time number,
  decl_mrr_time number,
  reports_time number,
  decl_time_min number,
  decl_time_max number,
  decl_time_avg number,
  decl_size_min number,
  decl_size_max number,
  decl_size_avg number,
  decl_sov_time_min number,
  decl_sov_time_max number,
  decl_sov_time_avg number,
  decl_mrr_time_min number,
  decl_mrr_time_max number,
  decl_mrr_time_avg number,
  reports_time_min number,
  reports_time_max number,
  reports_time_avg number,
  man_sel_time_min number,
  man_sel_time_max number,
  man_sel_time_avg number
);

INSERT INTO NDS2_MRR_USER.DICT_ACTIVITY_LOG_TYPE VALUES ( 1, '��������������������');
INSERT INTO NDS2_MRR_USER.DICT_ACTIVITY_LOG_TYPE VALUES ( 2, '����� �������� � �������');
INSERT INTO NDS2_MRR_USER.DICT_ACTIVITY_LOG_TYPE VALUES ( 3, '��������� ������� 1-7 ��');
INSERT INTO NDS2_MRR_USER.DICT_ACTIVITY_LOG_TYPE VALUES ( 4, '��������� ������� 8-12 ��');
INSERT INTO NDS2_MRR_USER.DICT_ACTIVITY_LOG_TYPE VALUES ( 5, '������������� ������ �������');
INSERT INTO NDS2_MRR_USER.DICT_ACTIVITY_LOG_TYPE VALUES ( 6, '����������� ���������� �������');
INSERT INTO NDS2_MRR_USER.DICT_ACTIVITY_LOG_TYPE VALUES ( 7, '����� �������� ������� 1-7 ��');
INSERT INTO NDS2_MRR_USER.DICT_ACTIVITY_LOG_TYPE VALUES ( 8, '����� �������� ������� 8-12 �� (���)');
INSERT INTO NDS2_MRR_USER.DICT_ACTIVITY_LOG_TYPE VALUES ( 9, '����� �������� ������� 8-12 �� (���)');
INSERT INTO NDS2_MRR_USER.DICT_ACTIVITY_LOG_TYPE VALUES (10, '����� ������������ ������ �������');
INSERT INTO NDS2_MRR_USER.DICT_ACTIVITY_LOG_TYPE VALUES (11, '����� ������������ �������');
COMMIT;

