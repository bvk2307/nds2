-- Create table
create table NDS2_MRR_USER.SOV_NAVIGATOR_PAIR
(
  id                 number not null,
  request_id         number not null,
  head_contractor_id number not null,
  tail_contractor_id number not null
)
;
-- Create/Recreate primary, unique and foreign key constraints 
alter table NDS2_MRR_USER.SOV_NAVIGATOR_PAIR
  add constraint PK_SOV_NAVIGATOR_PAIR primary key (ID);

COMMENT ON COLUMN NDS2_MRR_USER.SOV_NAVIGATOR_PAIR.request_id 
IS 'Идентификатор запроса';
COMMENT ON COLUMN NDS2_MRR_USER.SOV_NAVIGATOR_PAIR.head_contractor_id 
IS 'Первый НП в цепочке';
COMMENT ON COLUMN NDS2_MRR_USER.SOV_NAVIGATOR_PAIR.tail_contractor_id 
IS 'Последний НП в цепочке';