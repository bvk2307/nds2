-- Create table
create table NDS2_MRR_USER.SOV_NAVIGATOR_CONTRACTOR
(
  id             number not null,
  inn            varchar2(12) not null,
  kpp            varchar2(9),
  name           varchar2(1000),
  nds_calculated number,
  nds_deduction  number,
  nds_sales      number,
  nds_purchase   number,
  is_target      number,
  sur_code       number
)
;
-- Create/Recreate primary, unique and foreign key constraints 
alter table NDS2_MRR_USER.SOV_NAVIGATOR_CONTRACTOR
  add constraint PK_SOV_NAVIGATOR_CONTRACTOR primary key (ID);

COMMENT ON COLUMN NDS2_MRR_USER.SOV_NAVIGATOR_CONTRACTOR.id 
 IS 'идентификатор';
COMMENT ON COLUMN NDS2_MRR_USER.SOV_NAVIGATOR_CONTRACTOR.inn 
 IS 'инн НП';
COMMENT ON COLUMN NDS2_MRR_USER.SOV_NAVIGATOR_CONTRACTOR.inn 
 IS 'кпп НП';
 COMMENT ON COLUMN NDS2_MRR_USER.SOV_NAVIGATOR_CONTRACTOR.inn 
 IS 'имя НП';
COMMENT ON COLUMN NDS2_MRR_USER.SOV_NAVIGATOR_CONTRACTOR.inn 
 IS 'имя НП';
 COMMENT ON COLUMN NDS2_MRR_USER.SOV_NAVIGATOR_CONTRACTOR.nds_calculated 
 IS 'Исчисленный НДС по разделам 9, 9.1 и 12';
COMMENT ON COLUMN NDS2_MRR_USER.SOV_NAVIGATOR_CONTRACTOR.nds_deduction 
 IS 'Сумма НДС к вычету по разделу 8 и 8.1';
 COMMENT ON COLUMN NDS2_MRR_USER.SOV_NAVIGATOR_CONTRACTOR.nds_sales 
 IS 'НДС по журналу выставленных СФ (10 раздел)';
 COMMENT ON COLUMN NDS2_MRR_USER.SOV_NAVIGATOR_CONTRACTOR.nds_purchase 
 IS 'НДС по журналу полученных СФ (11 раздел)';
