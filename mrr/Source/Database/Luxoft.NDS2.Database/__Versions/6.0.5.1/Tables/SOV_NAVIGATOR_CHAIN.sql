-- Create table
create table NDS2_MRR_USER.SOV_NAVIGATOR_CHAIN
(
  id                    number not null,
  pair_id               number not null,
  chainnumber                number not null,
  min_mapped_amountt    number,
  min_not_mapped_amount number,
  max_mapped_amount     number,
  max_not_mapped_amount number
)
;
-- Create/Recreate primary, unique and foreign key constraints 
alter table NDS2_MRR_USER.SOV_NAVIGATOR_CHAIN
  add constraint PK_SOV_NAVIGATOR_CHAIN primary key (ID);

 COMMENT ON COLUMN NDS2_MRR_USER.SOV_NAVIGATOR_CHAIN.pair_id 
 IS 'Идентификатор в таблице SOV_NAVIGATOR_PAIR';
 
 COMMENT ON COLUMN NDS2_MRR_USER.SOV_NAVIGATOR_CHAIN.chainnumber 
 IS 'Номер цепочки';
 COMMENT ON COLUMN NDS2_MRR_USER.SOV_NAVIGATOR_CHAIN.min_mapped_amountt 
 IS 'Минимальная сопоставленная сумма потока';
 COMMENT ON COLUMN NDS2_MRR_USER.SOV_NAVIGATOR_CHAIN.min_not_mapped_amount 
 IS 'Минимальная несопоставленная сумма потока';
 COMMENT ON COLUMN NDS2_MRR_USER.SOV_NAVIGATOR_CHAIN.max_mapped_amount 
 IS 'Максимальная сопоставленная сумма потока';
 COMMENT ON COLUMN NDS2_MRR_USER.SOV_NAVIGATOR_CHAIN.max_not_mapped_amount 
 IS 'Максимальная несопоставленная сумма потока';