﻿alter table NDS2_MRR_USER.SOV_INVOICE add CLARIFICATION_KEY    VARCHAR2(128 CHAR);

alter table NDS2_MRR_USER.SOV_INVOICE modify  BUYER_INN varchar2(2048 CHAR);
alter table NDS2_MRR_USER.SOV_INVOICE modify  BUYER_KPP varchar2(2048 CHAR);
alter table NDS2_MRR_USER.SOV_INVOICE modify  BUY_ACCEPT_DATE varchar2(2048 CHAR);
alter table NDS2_MRR_USER.SOV_INVOICE modify  OPERATION_CODE varchar2(1024 CHAR);
alter table NDS2_MRR_USER.SOV_INVOICE modify  RECEIPT_DOC_NUM varchar2(2048 CHAR);
alter table NDS2_MRR_USER.SOV_INVOICE modify  RECEIPT_DOC_DATE varchar2(2048 CHAR);
alter table NDS2_MRR_USER.SOV_INVOICE modify  customs_declaration_num varchar2(1000 CHAR);

alter table NDS2_MRR_USER.SOV_INVOICE modify  COMPARE_ROW_KEY varchar2(2048 CHAR);
alter table NDS2_MRR_USER.SOV_INVOICE modify  INVOICE_NUM varchar2(2048 CHAR);
alter table NDS2_MRR_USER.SOV_INVOICE modify  COMPARE_ALGO_ID number(2);
