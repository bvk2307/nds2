﻿drop table NDS2_MRR_USER.V_EGRN_UL;
create table NDS2_MRR_USER.V_EGRN_UL
(
  id          NUMBER not null,
  inn         VARCHAR2(10),
  kpp         VARCHAR2(9),
  ogrn        VARCHAR2(13),
  name_full   VARCHAR2(1000),
  date_reg    DATE,
  code_no     CHAR(4),
  date_on     DATE,
  date_off    DATE,
  reason_off  CHAR(2),
  adr         VARCHAR2(254),
  ust_capital NUMBER(20,4),
  is_knp CHAR(1)
)
;
