﻿create table NDS2_MRR_USER.REPORT_DECLARATION_PARAMETER
(
  Id number not null,
  Name varchar2(1024 char),
  Data_Type varchar2(32 char),
  constraint REPORT_DECLARATION_PARAM_PK primary key (Id)
);