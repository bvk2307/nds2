﻿create table NDS2_MRR_USER.BOOK_DATA_REQUEST_PRIORITY
(
  id          number not null,
  priority	  number(2) not null,
  min_count   number not null,
  max_count   number null,
  CONSTRAINT BOOK_DATA_REQUEST_PRIORITY_PK PRIMARY KEY (ID) ENABLE
);