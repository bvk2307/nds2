﻿-- Create table
create table NDS2_MRR_USER.SOV_MATCH_RESULT_SUMMARY
(
  id                             number not null,
  fiscal_year                    number not null,
  fiscal_period                  number not null,
  inspection_code                varchar2(4) not null, 
  process_date                   date not null,
  primary_declaration_count      number not null,
  correction_declaration_count   number not null,
  decl_with_mediator_ops_count   number not null,
  journal_count                  number not null,
  invoice_record_count           number not null,
  invoice_record_count_chapter8  number not null,
  invoice_record_count_chapter9  number not null,
  invoice_record_count_chapter10 number not null,
  invoice_record_count_chapter11 number not null,
  invoice_record_count_chapter12 number not null,
  exact_match_count              number not null,
  prim_decl_with_mismatch_count  number not null,
  corr_decl_with_mismatch_count  number not null,
  decl_with_kc_mismatch_count    number not null,
  mismatch_total_count           number not null,
  mismatch_total_sum             number not null,
  weak_and_nds_count             number not null,
  weak_and_nds_sum               number not null,
  weak_and_no_nds_count          number not null,
  exact_and_nds_count            number not null,
  exact_and_nds_sum              number not null,
  gap_count                      number not null,
  gap_sum                        number not null,
  currency_count                 number not null,
  currency_sum                   number not null,
  mismatch_tno_count		 number not null,
  mismatch_tno_sum		 number not null
)
;
-- Create/Recreate primary, unique and foreign key constraints 
alter table NDS2_MRR_USER.SOV_MATCH_RESULT_SUMMARY
  add constraint PK_SOV_MATCH_RESULT_SUMMARY primary key (id);
