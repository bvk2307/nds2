-- Create table
create table NDS2_MRR_USER.SOV_NAVIGATOR_DEAL
(
  id                number not null,
  buyer_id          number not null,
  seller_id         number not null,
  mapped_amount     number,
  amount_per_buyer  number,
  amount_per_seller number
)
;
-- Create/Recreate primary, unique and foreign key constraints 
alter table NDS2_MRR_USER.SOV_NAVIGATOR_DEAL
  add constraint PK_SOV_NAVIGATOR_DEAL primary key (id);

COMMENT ON COLUMN NDS2_MRR_USER.SOV_NAVIGATOR_DEAL.mapped_amount 
IS 'Сопоставленный поток между покупателем и продавцом';
COMMENT ON COLUMN NDS2_MRR_USER.SOV_NAVIGATOR_DEAL.amount_per_buyer 
IS 'Сумма операций по стороне покупателя по разделам 8 и 11';
COMMENT ON COLUMN NDS2_MRR_USER.SOV_NAVIGATOR_DEAL.amount_per_seller 
IS 'Сумма операций по стороне продавца по разделам 9, 10 и 12';