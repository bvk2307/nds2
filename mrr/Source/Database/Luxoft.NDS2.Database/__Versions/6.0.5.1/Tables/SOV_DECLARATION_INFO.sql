﻿alter table NDS2_MRR_USER.SOV_DECLARATION_INFO add (
DISCREP_TOTAL_COUNT     number default 0 not null,
ch8_deals_cnt_total     number default 0 not null,
ch9_deals_cnt_total     number default 0 not null, 
ch10_deals_cnt_total    number default 0 not null, 
ch11_deals_cnt_total    number default 0 not null, 
ch12_deals_cnt_total    number default 0 not null,
exact_match_count       number default 0 not null,
weak_and_nds_mis_count  number default 0 not null,
weak_and_nds_mis_amount number(19,2) default 0 not null
);
