-- Create table
create table NDS2_MRR_USER.SOV_NAVIGATOR_CHAIN_CONTRACTOR
(
  chain_id      number not null,
  contractor_id number not null,
  order_number  number not null
)
;
-- Create/Recreate primary, unique and foreign key constraints 
alter table NDS2_MRR_USER.SOV_NAVIGATOR_CHAIN_CONTRACTOR
  add constraint PK_SOV_NVG_CHAIN_CONTRACTOR primary key (CHAIN_ID, ORDER_NUMBER);
  
 COMMENT ON COLUMN NDS2_MRR_USER.SOV_NAVIGATOR_CHAIN_CONTRACTOR.chain_id 
 IS 'Идентификатор в таблице SOV_NAVIGATOR_CHAIN';
 COMMENT ON COLUMN NDS2_MRR_USER.SOV_NAVIGATOR_CHAIN_CONTRACTOR.chain_id 
 IS 'Идентификатор в таблице SOV_NAVIGATOR_CONTRACTOR';
 COMMENT ON COLUMN NDS2_MRR_USER.SOV_NAVIGATOR_CHAIN_CONTRACTOR.chain_id 
 IS 'Номер НП в цепочке';

