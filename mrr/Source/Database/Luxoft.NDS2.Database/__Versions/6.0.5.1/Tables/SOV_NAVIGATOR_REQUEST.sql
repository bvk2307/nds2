-- Create table
create table NDS2_MRR_USER.SOV_NAVIGATOR_REQUEST
(
  request_id               number not null,
  month_from               number not null,
  month_to                 number not null,
  year_from                number not null,
  year_to                  number not null,
  depth                    number not null,
  min_mapped_amount        number not null,
  max_contractors_quantity number not null,
  min_buyer_amount         number not null,
  min_seller_amount        number not null,
  status_id                number not null,
  request_date             date not null,
  process_date             date
)
;
-- Create/Recreate primary, unique and foreign key constraints 
alter table NDS2_MRR_USER.SOV_NAVIGATOR_REQUEST
  add constraint PK_SOV_NAVIGATOR_REQUEST primary key (REQUEST_ID);

COMMENT ON COLUMN NDS2_MRR_USER.SOV_NAVIGATOR_REQUEST.request_id 
IS 'Идентификатор запроса';
COMMENT ON COLUMN NDS2_MRR_USER.SOV_NAVIGATOR_REQUEST.month_from 
IS 'Начало периода формирования (месяц)';
COMMENT ON COLUMN NDS2_MRR_USER.SOV_NAVIGATOR_REQUEST.month_to 
IS 'Конец периода формирования (месяц)';
COMMENT ON COLUMN NDS2_MRR_USER.SOV_NAVIGATOR_REQUEST.year_from 
IS 'Начало периода формирования (год)';
COMMENT ON COLUMN NDS2_MRR_USER.SOV_NAVIGATOR_REQUEST.year_to 
IS 'Конец периода формирования (год)';
COMMENT ON COLUMN NDS2_MRR_USER.SOV_NAVIGATOR_REQUEST.depth 
IS 'Глубина поиска (целое число от 1 до 7)';
COMMENT ON COLUMN NDS2_MRR_USER.SOV_NAVIGATOR_REQUEST.min_mapped_amount 
IS 'Минимальная сумма сопоставленного потока в рублях (целое число, необязательный параметр)';
COMMENT ON COLUMN NDS2_MRR_USER.SOV_NAVIGATOR_REQUEST.max_contractors_quantity 
IS 'Максимальное количество контрагентов (целое число)';
COMMENT ON COLUMN NDS2_MRR_USER.SOV_NAVIGATOR_REQUEST.min_buyer_amount 
IS 'Минимальная сумма операций между парой контрагентов по данным продавца в рублях (целое число)';
COMMENT ON COLUMN NDS2_MRR_USER.SOV_NAVIGATOR_REQUEST.min_seller_amount 
IS 'Минимальная сумма операций между парой контрагентов по данным покупателя в рублях (целое число).';