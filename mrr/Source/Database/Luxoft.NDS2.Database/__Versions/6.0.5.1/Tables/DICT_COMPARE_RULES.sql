BEGIN
  NDS2_MRR_USER.NDS2$SYS.DROP_IF_EXISTS('NDS2_MRR_USER', 'DICT_COMPARE_RULES');
END;
/
create table NDS2_MRR_USER.DICT_COMPARE_RULES
(
    code varchar2(2 CHAR) not null,
    group_code varchar2(2 CHAR) not null,
    description varchar2(512 CHAR) not null,
    CONSTRAINT PK_DICT_COMPARE_RULES PRIMARY KEY (code) ENABLE
);
