-- Create table
create table NDS2_MRR_USER.SOV_NAVIGATOR_REQUEST_TAXPAYER
(
  request_id number not null,
  inn        nvarchar2(10) not null
);
COMMENT ON COLUMN NDS2_MRR_USER.SOV_NAVIGATOR_REQUEST_TAXPAYER.request_id 
IS 'Идентификатор запроса';
COMMENT ON COLUMN NDS2_MRR_USER.SOV_NAVIGATOR_REQUEST_TAXPAYER.inn 
IS 'Инн НП';