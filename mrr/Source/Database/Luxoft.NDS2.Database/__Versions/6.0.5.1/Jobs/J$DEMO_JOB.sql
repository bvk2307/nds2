﻿BEGIN
DBMS_SCHEDULER.DROP_JOB (
job_name => 'NDS2_MRR_USER.J$CLAIM_KS');
exception when others then null;
END;
/
begin 
  sys.dbms_scheduler.create_job(job_name => 'NDS2_MRR_USER.J$CLAIM_KS', 
  job_type => 'PLSQL_BLOCK', 
  job_action => 'declare v_runjob number := 0; v_jobName varchar2(32 CHAR) := ''J$CLAIM_KS''; v_jobOwner varchar2(32 CHAR) := ''NDS2_MRR_USER'';
begin
  select count(1) into v_runjob from all_scheduler_running_jobs where job_name = v_jobName and owner = v_jobOwner;
  if v_runjob = 0 then
    nds2_mrr_user.pac$document.SEND_CLAIM_KS;
  end if;
end;', 
  start_date => SYSDATE + INTERVAL '120' SECOND, 
  repeat_interval => 'freq=minutely;Interval=30',
  end_date => to_date('01-01-2100 00:00:00', 'dd-mm-yyyy hh24:mi:ss'), 
  job_class => 'DEFAULT_JOB_CLASS', 
  enabled => false, 
  auto_drop => false, 
  comments => 'фоновые задания МРР'); 
end; 
/

BEGIN
DBMS_SCHEDULER.DROP_JOB (
job_name => 'NDS2_MRR_USER.J$DEMO_JOB');
exception when others then null;
END;
/
begin 
  sys.dbms_scheduler.create_job(job_name => 'NDS2_MRR_USER.J$DEMO_JOB', 
  job_type => 'PLSQL_BLOCK', 
  job_action => 'declare v_runjob number := 0; v_jobName varchar2(32 CHAR) := ''J$DEMO_JOB''; v_jobOwner varchar2(32 CHAR) := ''NDS2_MRR_USER'';
begin
  select count(1) into v_runjob from all_scheduler_running_jobs where job_name = v_jobName and owner = v_jobOwner;
  if v_runjob = 0 then
    nds2_mrr_user.PAC$DOCUMENT.DEMO;
  end if;
end;', 
  start_date => SYSDATE + INTERVAL '600' SECOND, 
  repeat_interval => 'freq=hourly;Interval=6',
  end_date => to_date('01-01-2100 00:00:00', 'dd-mm-yyyy hh24:mi:ss'), 
  job_class => 'DEFAULT_JOB_CLASS', 
  enabled => false, 
  auto_drop => false, 
  comments => 'фоновые задания МРР'); 
end; 
/

