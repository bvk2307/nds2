﻿create bitmap index I$CAM.idx_tmp_01 on I$CAM.EOD_DUMP(info_type);

create table I$CAM.EOD_DUMP_01 as select t.*, 1 as STATUS from I$CAM.EOD_DUMP t where t.info_type = 'CAM_NDS2_01';
create table I$CAM.EOD_DUMP_02 as select t.*, 1 as STATUS from I$CAM.EOD_DUMP t where t.info_type = 'CAM_NDS2_02';
create table I$CAM.EOD_DUMP_03 as select t.*, 1 as STATUS from I$CAM.EOD_DUMP t where t.info_type = 'CAM_NDS2_03';
create table I$CAM.EOD_DUMP_04 as select t.*, 1 as STATUS from I$CAM.EOD_DUMP t where t.info_type = 'CAM_NDS2_04';
create table I$CAM.EOD_DUMP_05 as select t.*, 1 as STATUS from I$CAM.EOD_DUMP t where t.info_type = 'CAM_NDS2_05';
create table I$CAM.EOD_DUMP_06 as select t.*, 1 as STATUS from I$CAM.EOD_DUMP t where t.info_type = 'CAM_NDS2_06';
create table I$CAM.EOD_DUMP_07 as select t.*, 1 as STATUS from I$CAM.EOD_DUMP t where t.info_type = 'CAM_NDS2_07';
create table I$CAM.EOD_DUMP_08 as select t.*, 1 as STATUS from I$CAM.EOD_DUMP t where t.info_type = 'CAM_NDS2_08';
create table I$CAM.EOD_DUMP_09 as select t.*, 1 as STATUS from I$CAM.EOD_DUMP t where t.info_type = 'CAM_NDS2_09';
create table I$CAM.EOD_DUMP_10 as select t.*, 1 as STATUS from I$CAM.EOD_DUMP t where t.info_type = 'CAM_NDS2_10';
create table I$CAM.EOD_DUMP_11 as select t.*, 1 as STATUS from I$CAM.EOD_DUMP t where t.info_type = 'CAM_NDS2_11';

drop table I$CAM.EOD_DUMP purge;

create index I$CAM.idx_dm_01_id on I$CAM.EOD_DUMP_01(id);
create index I$CAM.idx_dm_02_id on I$CAM.EOD_DUMP_02(id);
create index I$CAM.idx_dm_03_id on I$CAM.EOD_DUMP_03(id);
create index I$CAM.idx_dm_04_id on I$CAM.EOD_DUMP_04(id);
create index I$CAM.idx_dm_05_id on I$CAM.EOD_DUMP_05(id);
create index I$CAM.idx_dm_06_id on I$CAM.EOD_DUMP_06(id);
create index I$CAM.idx_dm_07_id on I$CAM.EOD_DUMP_07(id);
create index I$CAM.idx_dm_08_id on I$CAM.EOD_DUMP_08(id);
create index I$CAM.idx_dm_09_id on I$CAM.EOD_DUMP_09(id);
create index I$CAM.idx_dm_10_id on I$CAM.EOD_DUMP_10(id);
create index I$CAM.idx_dm_11_id on I$CAM.EOD_DUMP_11(id);

create bitmap index I$CAM.idxbm_dm_01_id on I$CAM.EOD_DUMP_01(status);
create bitmap index I$CAM.idxbm_dm_02_id on I$CAM.EOD_DUMP_02(status);
create bitmap index I$CAM.idxbm_dm_03_id on I$CAM.EOD_DUMP_03(status);
create bitmap index I$CAM.idxbm_dm_04_id on I$CAM.EOD_DUMP_04(status);
create bitmap index I$CAM.idxbm_dm_05_id on I$CAM.EOD_DUMP_05(status);
create bitmap index I$CAM.idxbm_dm_06_id on I$CAM.EOD_DUMP_06(status);
create bitmap index I$CAM.idxbm_dm_07_id on I$CAM.EOD_DUMP_07(status);
create bitmap index I$CAM.idxbm_dm_08_id on I$CAM.EOD_DUMP_08(status);
create bitmap index I$CAM.idxbm_dm_09_id on I$CAM.EOD_DUMP_09(status);
create bitmap index I$CAM.idxbm_dm_10_id on I$CAM.EOD_DUMP_10(status);
create bitmap index I$CAM.idxbm_dm_11_id on I$CAM.EOD_DUMP_11(status);
