﻿create table NDS2_MRR_USER.CFG_RESTRICTION
(
  ROLE_ID number not null,
  GROUP_ID number,
  NOT_RESTRICTED number not null,
  MAX_LEVEL_SALES number not null,
  MAX_LEVEL_PURCHASE number not null,
  MAX_NAVIGATOR_CHAINS number not null,
  ALLOW_PURCHASE number not null,
  DESCRIPTION varchar(1024) null
);
