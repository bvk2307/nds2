create table NDS2_MRR_USER.DOC_DISCREPANCY
(
  doc_id         NUMBER,
  discrepancy_id NUMBER,
  row_key        VARCHAR2(512 CHAR)
);

create index NDS2_MRR_USER.idx_doc_dis_id on NDS2_MRR_USER.DOC_DISCREPANCY(doc_id, discrepancy_id);
