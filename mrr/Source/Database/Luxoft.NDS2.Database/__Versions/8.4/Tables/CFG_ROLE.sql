﻿create table NDS2_MRR_USER.CFG_ROLE
(
  ID					NUMBER,
  NAME					VARCHAR2(128),
  Constraint PK$CFG_ROLE primary key (ID) using index
);
