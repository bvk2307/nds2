-- Create table
create global temporary table NDS2_MRR_USER.T$TMP_AUTO_CLAIM
(
  doc_id     NUMBER,
  invoice_rk VARCHAR2(512 CHAR)
)
on commit delete rows;
