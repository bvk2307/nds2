﻿create table NDS2_MRR_USER.CFG_TAX_PERIOD
(
  ID					NUMBER,
  FISCAL_YEAR_BEGIN		NUMBER(4) not null,
  TAX_PERIOD_BEGIN		VARCHAR2(2) not null,
  Constraint PK$CFG_TAX_PERIOD primary key (ID) using index
);
