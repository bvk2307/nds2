﻿create table NDS2_MRR_USER.CFG_INSPECTION_GROUP
(
  ID					NUMBER,
  NAME					VARCHAR2(128),
  Constraint PK$CFG_INSPECTION_GROUP primary key (ID) using index
);
