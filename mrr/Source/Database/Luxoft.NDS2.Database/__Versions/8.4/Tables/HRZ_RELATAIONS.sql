﻿DECLARE
  v_exist number(1);
BEGIN
  select nvl(count(1), 0) into v_exist from all_tables where owner = 'NDS2_MRR_USER' and table_name = 'HRZ_RELATIONS';
  if v_exist > 0 then
     NDS2_MRR_USER.NDS2$SYS.DROP_IF_EXISTS('NDS2_MRR_USER', 'HRZ_RELATIONS_BACKUP');
     EXECUTE IMMEDIATE 'alter table NDS2_MRR_USER.HRZ_RELATIONS rename to HRZ_RELATIONS_BACKUP';
  end if;
END;
/

create table NDS2_MRR_USER.HRZ_RELATIONS
(
  inn_1               VARCHAR2(12 CHAR),
  inn_2               VARCHAR2(12 CHAR),
  year                NUMBER(4),
  period              NUMBER,
  chapter8_amount     NUMBER,
  chapter9_amount     NUMBER,
  chapter10_amount    NUMBER,
  chapter11_amount    NUMBER,
  chapter12_amount    NUMBER,
  chapter8_count      NUMBER,
  chapter9_count      NUMBER,
  chapter10_count     NUMBER,
  chapter11_count     NUMBER,
  chapter12_count     NUMBER,
  nds_chapter9        NUMBER,
  nds_chapter12       NUMBER,
  sa                  NUMBER,
  ba                  NUMBER,
  nba                 NUMBER,
  nsa                 NUMBER,
  njsa                NUMBER,
  njba                NUMBER,
  gap_amount          NUMBER,
  total_purchase_amnt NUMBER(20,2),
  total_sales_amnt    NUMBER(20,2),
  deduction_nds       NUMBER,
  calc_nds            NUMBER,
  declaration_type    VARCHAR2(23),
  total_nds           NUMBER,
  gap_discrep_amnt    NUMBER(20,2),
  share_nds_amnt      NUMBER,
  nba_order           NUMBER,
  nsa_order           NUMBER
);