﻿DECLARE
  v_exist number(1);
BEGIN
  select nvl(count(1), 0) into v_exist from all_tables where owner = 'NDS2_MRR_USER' and table_name = 'NAVIGATOR_AGGREGATE';
  if v_exist > 0 then
    NDS2_MRR_USER.NDS2$SYS.DROP_IF_EXISTS('NDS2_MRR_USER', 'NAVIGATOR_AGGREGATE_BACKUP');
    EXECUTE IMMEDIATE 'alter table NDS2_MRR_USER.NAVIGATOR_AGGREGATE rename to NAVIGATOR_AGGREGATE_BACKUP';
  end if;
END;
/


--BA заменена на CHAPTER8_AMOUNT и CHAPTER11_AMOUNT
--SA заменена на CHAPTER9_AMOUNT, CHAPTER10_AMOUNT и CHAPTER12_AMOUNT
--NSA заменена на NDS_CHAPTER9 и NDS_CHAPTER12
--NBA переименована в NDS_BUYER
--ID убран
--MA пока остается, но не заполняется

--Скрипт создания обновленной таблицы:
CREATE TABLE "NDS2_MRR_USER"."NAVIGATOR_AGGREGATE" 
( 
	"INN_1" VARCHAR2(12 CHAR), --ИНН НП
	"INN_2" VARCHAR2(12 CHAR), -- ИНН контрагента
	"YEAR" NUMBER(4,0), -- год
	"QTR" NUMBER(2,0), -- квартал
	"MONTH" NUMBER(2,0), -- месяц
	"MA" NUMBER, -- Сопоставленный поток между налогоплательщиком и контрагентом
	"CHAPTER8_AMOUNT" NUMBER, --Сумма операций по стороне покупателя 8 раздел (в рублях) 
	"CHAPTER9_AMOUNT" NUMBER, --Сумма операций по стороне продавца раздел 9 (в рублях) 
	"CHAPTER10_AMOUNT" NUMBER, --Сумма операций по стороне продавца раздел 10 (в рублях)
	"CHAPTER11_AMOUNT" NUMBER, --Сумма операций по стороне покупателя 11 раздел (в рублях)
	"CHAPTER12_AMOUNT" NUMBER, --Сумма операций по стороне продавца раздел 12 (в рублях)
	"CHAPTER8_COUNT" NUMBER, --Кол-во операций по стороне покупателя 8 раздел
	"CHAPTER9_COUNT" NUMBER, --Кол-во операций по стороне продавца раздел 9
	"CHAPTER10_COUNT" NUMBER, --Кол-во операций по стороне продавца раздел 10
	"CHAPTER11_COUNT" NUMBER, --Кол-во операций по стороне покупателя 11 раздел
	"CHAPTER12_COUNT" NUMBER, --Кол-во операций по стороне продавца раздел 12
	"NDS_CHAPTER9" NUMBER, --Исчисленный НДС 9 раздела
	"NDS_CHAPTER12" NUMBER, --Исчисленный НДС 9 раздела
	"NDS_BUYER" NUMBER, -- Сумма НДС к вычету
	"NJSA" NUMBER, -- НДС по журналу учёта выставленных СФ
	"NJBA" NUMBER, -- НДС по журналу учёта полученных СФ
	"MA_NOAGENT" NUMBER, -- Сумма сопоставленных операций между покупателем и продавцом без учета посреднеческих операций(в рублях)
	"SA_NOAGENT" NUMBER, -- Сумма операций по стороне покупателя по разделу 8 без посреднеческих операций (в рублях)
	"BA_NOAGENT" NUMBER, -- Сумма операций по стороне продавца по разделам 9 и 12 без посреднеческих операций (в рублях)
	"GAP_COUNT" NUMBER, -- Количество расхождений вида “разрыв” у Налогоплательщика с контрагентом
	"GAP_AMOUNT" NUMBER, -- Сумма расхождений вида “разрыв” у Налогоплательщика с контрагентом
	"GAP_COUNT_NOAGENT" NUMBER, -- Количество расхождений вида “разрыв” у покупателя с продавцом без посреднеческих операций
	"GAP_AMOUNT_NOAGENT" NUMBER -- Сумма расхождений вида “разрыв” у покупателя с продавцом без посреднеческих операций
);
