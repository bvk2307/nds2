﻿drop table NDS2_MRR_USER.DICT_TAX_PERIOD;
create table NDS2_MRR_USER.DICT_TAX_PERIOD
(
    code					varchar2(2 CHAR) not null,
    description				varchar2(512 CHAR) not null,
	QUARTER					NUMBER(1) NOT NULL,
	IS_MAIN_IN_QUARTER		NUMBER(1) NOT NULL,
    CONSTRAINT PK_DICT_TAX_PERIOD_CODE PRIMARY KEY (code) ENABLE
);
