﻿begin
  execute immediate 'drop sequence nds2_mrr_user.seq_bakup_num';
  exception when others then null;
end;
/

create sequence nds2_mrr_user.seq_bakup_num
start with 0
increment by 1
minvalue 0
maxvalue 999999
nocache
cycle;
