﻿declare
v_name varchar2(32);
begin
  FOR i IN ( select object_name from all_objects where object_type = 'SEQUENCE' and owner = 'FIR' )
  LOOP
    execute immediate 'drop sequence FIR.'||i.object_name;
  END LOOP;
end;
/

create sequence FIR.MRR_REQ_SEQ
start with 1
maxvalue 999999999
nocache
increment by 1;
