﻿truncate table FIR.V_EGRN_UL;
insert into FIR.V_EGRN_UL (id, inn, kpp, name_full, code_no, adr, date_reg, date_on, ust_capital)
select id, innnp, kppnp, naimorg, kodno,
'643,194291,САНКТ-ПЕТЕРБУРГ Г,,,,САНТЬЯГО-ДЕ-КУБА УЛ,8,2,249', sysdate-31, sysdate, 10000
from "NDS2_MRR_USER"."V$ASKDEKL" b where naimorg is not null
and not exists(select inn from fir.v_egrn_ul a where a.inn = b.innnp and a.kpp = b.kppnp)
union
select 1000000 + id, inn, kpp, naimorg, '5252',
'643,194291,САНКТ-ПЕТЕРБУРГ Г,,,,САНТЬЯГО-ДЕ-КУБА УЛ,8,2,249', sysdate-31, sysdate, 10000
from "NDS2_MRR_USER"."V$ASKJOURNALUCH" b where naimorg is not null
and not exists(select inn from fir.v_egrn_ul a where a.inn = b.inn and a.kpp = b.kpp);
commit;
/

truncate table FIR.V_EGRN_IP;
insert into FIR.V_EGRN_IP (id, innfl, last_name, first_name, patronymic, code_no, adr, date_on)
select id, innnp, familiyanp, imyanp, otchestvonp, kodno, 'Санкт-Петербург', sysdate
from "NDS2_MRR_USER"."V$ASKDEKL" b where familiyanp is not null
and not exists(select inn from fir.v_egrn_ul a where a.inn = b.innnp)
union
select 1000000 + id, inn, FAMILIJA, IMJA, OTCHESTVO, '5252', 'Санкт-Петербург', sysdate
from "NDS2_MRR_USER"."V$ASKJOURNALUCH" b where FAMILIJA is not null
and not exists(select inn from fir.v_egrn_ul a where a.inn = b.inn);
commit;
/