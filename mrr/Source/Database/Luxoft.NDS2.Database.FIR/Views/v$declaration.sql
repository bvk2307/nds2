﻿create or replace force view fir.v$declaration
as
select
SDI.ID,
substr(D.KODNO, 1,2) as region_code,
D.KODNO as Soun_code,
D.INNNP as inn,
D.NAIMORG as org_name,
D.PERIOD as Period_code
from NDS2_MRR_USER.SOV_DECLARATION_INFO sdi
inner join NDS2_MRR_USER.V$ASKDEKL d on SDI.DECLARATION_VERSION_ID = d.zip 
;
