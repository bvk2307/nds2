﻿create table FIR.V_EGRN_IP
(
  id           NUMBER not null,
  innfl        VARCHAR2(12 CHAR),
  ogrnip       CHAR(15),
  last_name    VARCHAR2(60),
  first_name   VARCHAR2(60),
  patronymic   VARCHAR2(60),
  date_birth   DATE,
  doc_code     CHAR(2),
  doc_num      VARCHAR2(25),
  doc_date     DATE,
  doc_org      VARCHAR2(254),
  doc_org_code VARCHAR2(7),
  adr          VARCHAR2(254),
  code_no      CHAR(4),
  date_on      DATE,
  reason_on    CHAR(3),
  date_off     DATE,
  reason_off   CHAR(2)
);

comment on table FIR.V_EGRN_IP
  is 'Сведения о индивидуальных предпринимателях';
comment on column FIR.V_EGRN_IP.id
  is 'Уникальный идентификатор';
comment on column FIR.V_EGRN_IP.innfl
  is 'ИННФЛ';
comment on column FIR.V_EGRN_IP.ogrnip
  is 'ОГРНИП';
comment on column FIR.V_EGRN_IP.last_name
  is 'Фамилия';
comment on column FIR.V_EGRN_IP.first_name
  is 'Имя';
comment on column FIR.V_EGRN_IP.patronymic
  is 'Отчество';
comment on column FIR.V_EGRN_IP.date_birth
  is 'Дата рождения';
comment on column FIR.V_EGRN_IP.doc_code
  is 'Код документа, удостоверяющего личность, по СПДУЛ';
comment on column FIR.V_EGRN_IP.doc_num
  is 'Номер документа, удостоверяющего личность';
comment on column FIR.V_EGRN_IP.doc_date
  is 'Дата выдачи документа, удостоверяющего личность';
comment on column FIR.V_EGRN_IP.doc_org
  is 'Орган, выдавший документ, удостоверяющий личность';
comment on column FIR.V_EGRN_IP.doc_org_code
  is 'Код органа, выдавшего документ, удостоверяющий личность';
comment on column FIR.V_EGRN_IP.adr
  is 'Адрес места жительства';
comment on column FIR.V_EGRN_IP.code_no
  is 'Код налогового органа по месту постановки на учет в качестве ИП (по СОУН)';
comment on column FIR.V_EGRN_IP.date_on
  is 'Дата постановки на учет в качестве ИП';
comment on column FIR.V_EGRN_IP.reason_on
  is 'Код причины постановки на учет в качестве ИП (по СППУФЛ)';
comment on column FIR.V_EGRN_IP.date_off
  is 'Дата снятия с учета в качестве ИН';
comment on column FIR.V_EGRN_IP.reason_off
  is 'Код причины снятия с учета в качестве ИП (по СПСУФЛ)';
alter table FIR.V_EGRN_IP add constraint PK_SRC_IP_STAGE_ID primary key (ID);

create table FIR.V_EGRN_UL
(
  id          NUMBER not null,
  inn         VARCHAR2(10),
  kpp         VARCHAR2(9),
  ogrn        VARCHAR2(13),
  name_full   VARCHAR2(1000),
  date_reg    DATE,
  code_no     CHAR(4),
  date_on     DATE,
  date_off    DATE,
  reason_off  CHAR(2),
  adr         VARCHAR2(254),
  ust_capital NUMBER(20,4),
  is_knp      VARCHAR2(1)
);

comment on table FIR.V_EGRN_UL
  is 'Сведения о юридических лицах';
comment on column FIR.V_EGRN_UL.id
  is 'Уникальный идентификатор';
comment on column FIR.V_EGRN_UL.inn
  is 'ИНН';
comment on column FIR.V_EGRN_UL.kpp
  is 'КПП по месту нахождения';
comment on column FIR.V_EGRN_UL.ogrn
  is 'ОГРН';
comment on column FIR.V_EGRN_UL.name_full
  is 'Полное наименование';
comment on column FIR.V_EGRN_UL.date_reg
  is 'Дата регистрации';
comment on column FIR.V_EGRN_UL.code_no
  is 'Код НО по месту нахождения (по СОУН)';
comment on column FIR.V_EGRN_UL.date_on
  is 'Дата постановки на учет по месту нахождения';
comment on column FIR.V_EGRN_UL.date_off
  is 'Дата снятия с учета по месту нахождения';
comment on column FIR.V_EGRN_UL.reason_off
  is 'Код причины снятия с учета по месту нахождения (по СПСУНО)';
comment on column FIR.V_EGRN_UL.adr
  is 'Юридический адрес';
comment on column FIR.V_EGRN_UL.ust_capital
  is 'Размер уставного капитала';
comment on column FIR.V_EGRN_UL.is_knp
  is 'Признак крупнейшего НП (1-КНП;2-нет)';
alter table FIR.V_EGRN_UL add constraint PK_SRC_UL_STAGE_ID primary key (ID);

create table FIR.ASKNDS1PR
(
 PYEAR     VARCHAR2(4),
 NPERIOD   VARCHAR2(2),
 INN       VARCHAR2(12),
 PR        VARCHAR2(1),
 DPR       DATE
);

create table  FIR.BSSCHET_UL
(INN         VARCHAR2(10), --ИНН НП
 KPP         VARCHAR2(9), --КПП НП
 BIK         VARCHAR2(9), -- БИК КО
 NAMEKO      VARCHAR2(1000), -- Наименеование КО
 NOMSCH      VARCHAR2(20), --Номер счета
 PRVAL       VARCHAR2(1), --0-рублевый, 1-валютный
 DATEOPENSCH DATE --дата открытия счета
 );

create table  FIR.BSSCHET_IP
(INN         VARCHAR2(12), --ИНН НП
 BIK         VARCHAR2(9), -- БИК КО
 NAMEKO      VARCHAR2(1000), -- Наименеование КО
 NOMSCH      VARCHAR2(20), --Номер счета
 PRVAL       VARCHAR2(1), --0-рублевый, 1-валютный
 DATEOPENSCH DATE --дата открытия счета
 );

