﻿create table FIR.DISCREPANCY_RAW
(
   ID                   NUMBER               not null,
   CREATE_DATE          DATE,/*дата выявления*/
   TYPE                 NUMBER(1),
   RULE_GROUP           NUMBER(5),
   DEAL_AMNT            NUMBER(19,2),
   AMNT                 NUMBER(19,2),
   AMOUNT_PVP      NUMBER(19,2),
   COURSE        NUMBER(19,2),
   COURSE_COST      NUMBER(19,2),
   SUR_CODE        NUMBER(1),
   INVOICE_CHAPTER    NUMBER(2),
   INVOICE_RK           VARCHAR2(128),
   DECL_ID              NUMBER,
   INVOICE_CONTRACTOR_CHAPTER NUMBER(2),
   INVOICE_CONTRACTOR_RK VARCHAR2(128),
   DECL_CONTRACTOR_ID   NUMBER,
   STATUS               NUMBER(2)
);