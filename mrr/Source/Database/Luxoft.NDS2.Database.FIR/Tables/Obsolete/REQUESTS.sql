﻿create table FIR.REQUEST
(
   request_id number(19),
   gid nvarchar2(128),
   status number(1),
   message varchar2(2048)
);
