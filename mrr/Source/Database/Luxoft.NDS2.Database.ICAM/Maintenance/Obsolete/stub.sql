﻿drop sequence I$CAM.SEQ_TMP_ID;
create sequence I$CAM.SEQ_TMP_ID
minvalue 0
maxvalue 9999999999
start with 1
increment by 1
nocache;

drop table I$CAM.NDS2_TEST_DATA;
create table I$CAM.NDS2_TEST_DATA
(
  id             NUMBER(38) not null,
  info_type      VARCHAR2(11),
  info_type_name VARCHAR2(100 CHAR) not null,
  date_eod       DATE,
  date_receipt   DATE,
  code_nsi_sono  VARCHAR2(4 CHAR),
  reg_number     NUMBER(38) not null,
  xml_data       CLOB,
  status number(1) default 1
);

create or replace force view I$CAM.V$NDS2_OUTGOING AS
SELECT 
* from I$CAM.NDS2_TEST_DATA where status = 1;

create or replace force view I$CAM.V$NDS2_SENT AS
SELECT * from I$CAM.NDS2_TEST_DATA where status <> 1;


CREATE OR REPLACE PACKAGE I$CAM.PAC$NDS2 AS

  -- Подтверждение приёма
	PROCEDURE P$ACCEPT
	(
		VP_ID IN NUMBER
	);

	-- Ошибка при приеме
	PROCEDURE P$REFUSE
	(
		VP_ID IN NUMBER
		, VP_ERR_DESC VARCHAR2
	);
END PAC$NDS2;
/
CREATE OR REPLACE PACKAGE BODY I$CAM.PAC$NDS2 AS

  PROCEDURE P$ACCEPT
	(
		VP_ID IN NUMBER
	) AS
pragma autonomous_transaction;  
  begin
    update NDS2_TEST_DATA t set t.status = 2 where t.reg_number = VP_ID;
    commit;
  exception when others then rollback; dbms_output.put_line(SQLCODE||'-'||substr(SQLERRM, 128));
  END P$ACCEPT;

	PROCEDURE P$REFUSE
	(
		VP_ID IN NUMBER
		, VP_ERR_DESC VARCHAR2
) AS
pragma autonomous_transaction;
  begin
    update I$CAM.NDS2_TEST_DATA t set t.status = 3 where t.reg_number = VP_ID;
    commit;
  exception when others then rollback; dbms_output.put_line(SQLCODE||'-'||substr(SQLERRM, 128));    
  END P$REFUSE;
	
END PAC$NDS2;
/




insert into I$CAM.NDS2_TEST_DATA
(id ,info_type ,info_type_name ,date_eod ,date_receipt ,code_nsi_sono ,reg_number ,xml_data)
values(1,'CAM_NDS2_02','АСК НДС-2: Сведения по статусам исполнения автотребования (ЭОД-2)',sysdate,sysdate,sysdate,'5248',
42138104,
'<?xml version="1.0" encoding="windows-1251"?>
<Файл xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="TAX3EXCH_CAM_NDS2_02_01.xsd" ТипИнф="CAM_NDS2_02" ВерсПрог="АИС Налог (5248) 2.5.160.021">
	<Документ КодНО="5248" РегНомДек="42138104">
		<ТребПоясн УчНомТреб="50" НомДок="1" ДатаДок="02.12.2014"/>
	</Документ>
</Файл>');


insert into I$CAM.NDS2_TEST_DATA
(id ,info_type ,info_type_name ,date_eod ,date_receipt ,code_nsi_sono ,reg_number ,xml_data)
values(9707493,'CAM_NDS2_05','АСК НДС-2: Сведения о получении данных автотребования/автоистребования (ЭОД-5)',sysdate,sysdate,sysdate,'5248'
,42138120
,'<?xml version="1.0" encoding="windows-1251"?>
<Файл xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="TAX3EXCH_CAM_NDS2_05_01.xsd" ТипИнф="CAM_NDS2_05" ВерсПрог="АИС Налог (5248) 2.5.160.031">
	<Документ КодНО="5248" РегНомДек="42138120" ДатаПолучДан="03.12.2014">
		<Требование>
			<АвтоТреб УчНомТреб="7"/>
		</Требование>
	</Документ>
</Файл>');

