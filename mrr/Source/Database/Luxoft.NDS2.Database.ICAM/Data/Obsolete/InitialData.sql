﻿truncate table CLAIM_STATUS;
insert into CLAIM_STATUS values(51, 'Создано');
insert into CLAIM_STATUS values(52, 'Передано в СЭОД');
insert into CLAIM_STATUS values(53, 'Ошибка отправки');
insert into CLAIM_STATUS values(54, 'Отправлено налогоплательщику');
insert into CLAIM_STATUS values(55, 'Получено налогоплательщиком');
commit;
