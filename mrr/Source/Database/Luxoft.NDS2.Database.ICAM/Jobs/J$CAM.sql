﻿BEGIN
DBMS_SCHEDULER.DROP_JOB (
job_name => 'I$CAM.J$CAM');
exception when others then null;
END;
/
begin 
  sys.dbms_scheduler.create_job(job_name => 'I$CAM.J$CAM', 
  job_type => 'STORED_PROCEDURE', 
  job_action => 'I$CAM.pac$nds2.p$process_new_data', 
  start_date => SYSDATE + INTERVAL '5' SECOND, 
  repeat_interval => 'freq=Secondly;Interval=5', 
  end_date => to_date('01-01-2100 00:00:00', 'dd-mm-yyyy hh24:mi:ss'), 
  job_class => 'DEFAULT_JOB_CLASS', 
  enabled => false, 
  auto_drop => false, 
  comments => 'фоновые задания CAM'); 
end; 
/
