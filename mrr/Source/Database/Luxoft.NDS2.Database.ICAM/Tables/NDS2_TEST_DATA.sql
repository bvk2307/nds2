﻿create table I$CAM.NDS2_TEST_DATA
(
  id             NUMBER(38) not null,
  info_type      VARCHAR2(11),
  info_type_name VARCHAR2(100 CHAR) not null,
  date_eod       DATE,
  date_receipt   DATE,
  code_nsi_sono  VARCHAR2(4 CHAR),
  reg_number     NUMBER(38) not null,
  object_reg_num     NUMBER(38),
  xml_data       CLOB,
  status number(1) default 1
);
