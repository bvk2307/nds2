﻿create table I$STUB_CAM.KNP_DOC
(
  doc_id number,
  doc_num varchar(32),
  doc_type number(4),
  doc_date date,
  additional_amnt number(19,2),
  update_date date,
  sync_date date
);
