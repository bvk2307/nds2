﻿create table I$STUB_CAM.CLAIM_STATUS
(
       CLAIM_ID NUMBER not null,
       SYNC_GROUP_ID NUMBER not null,
       SOUN_CODE CHAR(4) not null,
       STATUS NUMBER(2) not null,
       ERROR NVARCHAR2(1024),
       UPDATE_DATE DATE,
       SYNC_DATE DATE
);
