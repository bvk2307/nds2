﻿create table I$STUB_CAM.STG_CLAIM
(
  claim_id         NUMBER,
  process_id       NUMBER not null,
  create_date      DATE not null,
  soun_code        CHAR(4) not null,
  status           NUMBER(2) not null,
  discrepancy_data SYS.XMLTYPE,
  ready_to_sync	   number(1)
);
