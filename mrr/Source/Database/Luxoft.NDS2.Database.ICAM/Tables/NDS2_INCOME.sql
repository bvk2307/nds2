﻿create table I$CAM.NDS2_INCOME
(
  sono_code          VARCHAR2(4 CHAR),
  declartion_reg_num NUMBER(38),
  object_reg_num     NUMBER(38),
  object_type_id     NUMBER(2) not null,
  object_type_name   VARCHAR2(128 CHAR),
  create_date        DATE,
  object_status_id   NUMBER(2) not null,
  object_status_name VARCHAR2(128) not null,
  xml_data           CLOB
);
