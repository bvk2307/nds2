﻿CREATE OR REPLACE PACKAGE I$CAM.PAC$NDS2 AS

  -- Подтверждение приёма
	PROCEDURE P$ACCEPT
	(
		VP_ID IN NUMBER
	);

	-- Ошибка при приеме
	PROCEDURE P$REFUSE
	(
		VP_ID IN NUMBER
		, VP_ERR_DESC VARCHAR2
	);

  procedure p$process_new_data;

END PAC$NDS2;
/
CREATE OR REPLACE PACKAGE BODY I$CAM.PAC$NDS2 AS

  PROCEDURE P$ACCEPT
	(
		VP_ID IN NUMBER
	) AS
pragma autonomous_transaction;
  begin
    update NDS2_TEST_DATA t set t.status = 2 where t.id = VP_ID;
    commit;
  exception when others then rollback; dbms_output.put_line(SQLCODE||'-'||substr(SQLERRM, 128));
  END P$ACCEPT;

	PROCEDURE P$REFUSE
	(
		VP_ID IN NUMBER
		, VP_ERR_DESC VARCHAR2
) AS
pragma autonomous_transaction;
  begin
    update I$CAM.NDS2_TEST_DATA t set t.status = 3 where t.id = VP_ID;
    commit;
  exception when others then rollback; dbms_output.put_line(SQLCODE||'-'||substr(SQLERRM, 128));
  END P$REFUSE;

procedure p$process_new_data
  as
    pragma autonomous_transaction;
  begin
    for line in (select * from i$nds2.v$cam_outgoing) loop
	MERGE INTO nds2_income t
    USING (
	select line.sono_code as sono_code,
	 line.declartion_reg_num as declartion_reg_num,
	 line.object_reg_num as object_reg_num,
	 line.object_type_id as object_type_id,
	 line.object_type_name as object_type_name,
	 line.create_date as create_date,
	 line.object_status_id as object_status_id,
	 line.object_status_name as object_status_name,
	 line.xml_data as xml_data from dual) s ON 
	 (s.sono_code = t.sono_code and
	 s.declartion_reg_num = t.declartion_reg_num and
	 s.object_reg_num = t.object_reg_num and
	 s.object_type_id = t.object_type_id and
	 s.create_date = t.create_date and
	 s.object_status_id = t.object_status_id)
	 WHEN NOT MATCHED 
     THEN insert (sono_code, declartion_reg_num, object_reg_num, object_type_id, object_type_name, create_date, object_status_id, object_status_name, xml_data)
     values (s.sono_code, s.declartion_reg_num, s.object_reg_num, s.object_type_id, s.object_type_name, s.create_date, s.object_status_id, s.object_status_name, s.xml_data);
     i$nds2.pac$cam_sync.accept(line.object_reg_num, line.object_type_id);
    end loop;
    commit;
    exception when others then rollback;
  end;

END PAC$NDS2;
/
