﻿create sequence I$CAM.SEQ_TMP_ID
minvalue 0
maxvalue 9999999999
start with 1
increment by 1
nocache;

create sequence I$CAM.SEQ_NDS2_SENT_ID
minvalue 0
maxvalue 9999999999
start with 1
increment by 1
nocache;