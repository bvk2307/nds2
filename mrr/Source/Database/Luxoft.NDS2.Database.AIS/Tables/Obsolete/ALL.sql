﻿CREATE TABLE "serialgen" 
   (	"DummyID" NUMBER NOT NULL ENABLE, 
	"CurValue" NUMBER, 
	"TableName" VARCHAR2(40 BYTE)
   ) ;
   

--------------------------------------------------------
--  DDL for Table ASKZIPФайл
--------------------------------------------------------

  CREATE TABLE "ASKZIPФайл" 
   (	"Ид" NUMBER, 
	"ИмяФайла" VARCHAR2(300 BYTE), 
	"ИдФайлОпис" NUMBER, 
	"ИдГП3" VARCHAR2(100 BYTE), 
	"ИсхПуть" VARCHAR2(300 BYTE), 
	"Путь" VARCHAR2(300 BYTE), 
	"ИдДокОбр" VARCHAR2(100 BYTE), 
	"КодНО" VARCHAR2(4 BYTE), 
	"ДатаФайла" DATE DEFAULT SYSDATE, 
	"Статус" VARCHAR2(1 BYTE) DEFAULT '0', 
	"ДатаСтатуса" DATE DEFAULT SYSDATE, 
	"Ошибка" VARCHAR2(2000 BYTE)
   ) ;
--------------------------------------------------------
--  DDL for Table ASKАгрегация
--------------------------------------------------------

  CREATE TABLE "ASKАгрегация" 
   (	"Ид" NUMBER, 
	"ДатаНач" DATE DEFAULT SYSDATE, 
	"ДатаОконч" DATE, 
	"ДатаОшибки" DATE
   ) ;
--------------------------------------------------------
--  DDL for Table ASKДекл
--------------------------------------------------------

  CREATE TABLE "ASKДекл" 
   (	"Ид" NUMBER, 
	"ZIP" NUMBER, 
	"ИдФайл" VARCHAR2(200 BYTE), 
	"ВерсПрог" VARCHAR2(80 BYTE), 
	"ВерсФорм" VARCHAR2(10 BYTE), 
	"ПризнНал8-12" VARCHAR2(2 BYTE), 
	"ПризнНал8" VARCHAR2(2 BYTE), 
	"ПризнНал81" VARCHAR2(2 BYTE), 
	"ПризнНал9" VARCHAR2(2 BYTE), 
	"ПризнНал91" VARCHAR2(2 BYTE), 
	"ПризнНал10" VARCHAR2(2 BYTE), 
	"ПризнНал11" VARCHAR2(2 BYTE), 
	"ПризнНал12" VARCHAR2(2 BYTE), 
	"КНД" VARCHAR2(14 BYTE), 
	"ДатаДок" DATE, 
	"Период" VARCHAR2(4 BYTE), 
	"ОтчетГод" VARCHAR2(10 BYTE), 
	"КодНО" VARCHAR2(8 BYTE), 
	"НомКорр" VARCHAR2(6 BYTE), 
	"ПоМесту" VARCHAR2(6 BYTE), 
	"ОКВЭД" VARCHAR2(16 BYTE), 
	"Тлф" VARCHAR2(40 BYTE), 
	"НаимОрг" VARCHAR2(2000 BYTE), 
	"ИНННП" VARCHAR2(24 BYTE), 
	"КППНП" VARCHAR2(18 BYTE), 
	"ФормРеорг" VARCHAR2(2 BYTE), 
	"ИННРеорг" VARCHAR2(20 BYTE), 
	"КППРеорг" VARCHAR2(18 BYTE), 
	"ФамилияНП" VARCHAR2(120 BYTE), 
	"ИмяНП" VARCHAR2(120 BYTE), 
	"ОтчествоНП" VARCHAR2(120 BYTE), 
	"ПрПодп" VARCHAR2(2 BYTE), 
	"ФамилияПодп" VARCHAR2(120 BYTE), 
	"ИмяПодп" VARCHAR2(120 BYTE), 
	"ОтчествоПодп" VARCHAR2(120 BYTE), 
	"НаимДок" VARCHAR2(240 BYTE), 
	"НаимОргПред" VARCHAR2(2000 BYTE), 
	"ОКТМО" VARCHAR2(22 BYTE), 
	"КБК" VARCHAR2(40 BYTE), 
	"СумПУ173.5" NUMBER(14,0), 
	"СумПУ173.1" NUMBER(14,0), 
	"НомДогИТ" VARCHAR2(20 BYTE), 
	"ДатаДогИТ" DATE, 
	"ДатаНачДогИТ" DATE, 
	"ДатаКонДогИТ" DATE, 
	"НалПУ164" NUMBER(14,0), 
	"НалВосстОбщ" NUMBER(14,0), 
	"РлТв18НалБаз" NUMBER(14,0), 
	"РеалТов18СумНал" NUMBER(14,0), 
	"РлТв10НалБаз" NUMBER(14,0), 
	"РеалТов10СумНал" NUMBER(14,0), 
	"РлТв118НалБаз" NUMBER(14,0), 
	"РлТв118СумНал" NUMBER(14,0), 
	"РлТв110НалБаз" NUMBER(14,0), 
	"РлТв110СумНал" NUMBER(14,0), 
	"РлПрдИКНалБаз" NUMBER(14,0), 
	"РлПрдИКСумНал" NUMBER(14,0), 
	"ВыпСМРСобНалБаз" NUMBER(14,0), 
	"ВыпСМРСобСумНал" NUMBER(14,0), 
	"ОпПрдПстНлБаз" NUMBER(14,0), 
	"ОплПрдПстСумНал" NUMBER(14,0), 
	"СумНалВс" NUMBER(14,0), 
	"СумНал170.3.5" NUMBER(14,0), 
	"СумНал170.3.3" NUMBER(14,0), 
	"КорРлТв18НалБаз" NUMBER(14,0), 
	"КорРлТв18СумНал" NUMBER(14,0), 
	"КорРлТв10НалБаз" NUMBER(14,0), 
	"КорРлТв10СумНал" NUMBER(14,0), 
	"КорРлТв118НлБз" NUMBER(14,0), 
	"КорРлТв118СмНл" NUMBER(14,0), 
	"КорРлТв110НлБз" NUMBER(14,0), 
	"КорРлТв110СмНл" NUMBER(14,0), 
	"КорРлПрдИКНлБз" NUMBER(14,0), 
	"КорРлПрдИКСмНл" NUMBER(14,0), 
	"НалПредНППриоб" NUMBER(14,0), 
	"НалПредНППок" NUMBER(14,0), 
	"НалИсчСМР" NUMBER(14,0), 
	"НалУплТамож" NUMBER(14,0), 
	"НалУплНОТовТС" NUMBER(14,0), 
	"НалИсчПрод" NUMBER(14,0), 
	"НалУплПокНА" NUMBER(14,0), 
	"НалВычОбщ" NUMBER(14,0), 
	"СумИсчислИтог" NUMBER(14,0), 
	"СумВозмПдтв" NUMBER(14,0), 
	"СумВозмНеПдтв" NUMBER(14,0), 
	"СумНал164Ит" NUMBER(14,0), 
	"НалВычНеПодИт" NUMBER(14,0), 
	"НалИсчислИт" NUMBER(14,0), 
	"СмОп1010449КдОп" VARCHAR2(14 BYTE), 
	"СмОп1010449НлБз" NUMBER(14,0), 
	"СмОп1010449КрИсч" NUMBER(14,0), 
	"СмОп1010449НлВст" NUMBER(14,0), 
	"ОплПостСв6Мес" NUMBER(14,0), 
	"НаимКнПок" VARCHAR2(200 BYTE), 
	"НаимКнПокДЛ" VARCHAR2(200 BYTE), 
	"НаимКнПрод" VARCHAR2(200 BYTE), 
	"НаимКнПродДЛ" VARCHAR2(200 BYTE), 
	"НаимЖУчВыстСчФ" VARCHAR2(200 BYTE), 
	"НаимЖУчПолучСчФ" VARCHAR2(200 BYTE), 
	"НмВстСчФ173_5" VARCHAR2(200 BYTE), 
	"Публ" VARCHAR2(1 BYTE)
   ) ;
--------------------------------------------------------
--  DDL for Table ASKСведНалГод
--------------------------------------------------------

  CREATE TABLE "ASKСведНалГод" 
   (	"Ид" NUMBER, 
	"ИдСумВосУпл" NUMBER, 
	"ГодОтч" VARCHAR2(10 BYTE), 
	"ДатаИсп170" DATE, 
	"ДоляНеОбл" NUMBER(4,1), 
	"НалГод" NUMBER(14,0)
   ) ;
--------------------------------------------------------
--  DDL for Table ASKСведНалГодИ
--------------------------------------------------------

  CREATE TABLE "ASKСведНалГодИ" 
   (	"Ид" NUMBER, 
	"ИдДекл" NUMBER, 
	"КППИнУч" VARCHAR2(18 BYTE), 
	"СумНалИсч" NUMBER(14,0), 
	"СумНалВыч" NUMBER(14,0)
   ) ;
--------------------------------------------------------
--  DDL for Table ASKСверка
--------------------------------------------------------

  CREATE TABLE "ASKСверка" 
   (	"Ид" NUMBER, 
	"ДатаНач" DATE DEFAULT SYSDATE, 
	"ДатаОконч" DATE, 
	"ДатаОшибки" DATE
   ) ;
--------------------------------------------------------
--  DDL for Table ASKСводЗап
--------------------------------------------------------

  CREATE TABLE "ASKСводЗап" 
   (	"Ид" NUMBER, 
	"ZIP" NUMBER, 
	"Индекс" VARCHAR2(7 BYTE), 
	"НомКорр" NUMBER, 
	"ПризнакАкт" VARCHAR2(1 BYTE), 
	"ТипФайла" VARCHAR2(1 BYTE), 
	"СумНДСПок" VARCHAR2(20 BYTE), 
	"СумНДСПокДЛ" VARCHAR2(20 BYTE), 
	"СтПрод18" NUMBER, 
	"СтПрод10" NUMBER, 
	"СтПрод0" NUMBER, 
	"СумНДСПрод18" VARCHAR2(20 BYTE), 
	"СумНДСПрод10" VARCHAR2(20 BYTE), 
	"СтПрод" NUMBER, 
	"СтПродОсв" NUMBER, 
	"СтПрод18ДЛ" NUMBER, 
	"СтПрод10ДЛ" NUMBER, 
	"СтПрод0ДЛ" NUMBER, 
	"СумНДС18ДЛ" VARCHAR2(20 BYTE), 
	"СумНДС10ДЛ" VARCHAR2(20 BYTE), 
	"СтПродОсвДЛ" NUMBER
   ) ;
--------------------------------------------------------
--  DDL for Table ASKСумВосУпл
--------------------------------------------------------

  CREATE TABLE "ASKСумВосУпл" 
   (	"Ид" NUMBER, 
	"ИдДекл" NUMBER, 
	"НаимНедв" VARCHAR2(200 BYTE), 
	"КодОпНедв" VARCHAR2(14 BYTE), 
	"ДатаВводОН" DATE, 
	"ДатаНачАмОтч" DATE, 
	"СтВводОН" NUMBER(14,0), 
	"НалВычОН" NUMBER(14,0), 
	"Индекс" VARCHAR2(12 BYTE), 
	"КодРегион" VARCHAR2(4 BYTE), 
	"Район" VARCHAR2(100 BYTE), 
	"Город" VARCHAR2(100 BYTE), 
	"НаселПункт" VARCHAR2(100 BYTE), 
	"Улица" VARCHAR2(100 BYTE), 
	"Дом" VARCHAR2(40 BYTE), 
	"Корпус" VARCHAR2(40 BYTE), 
	"Кварт" VARCHAR2(40 BYTE)
   ) ;
--------------------------------------------------------
--  DDL for Table ASKСумОпер1010447
--------------------------------------------------------

  CREATE TABLE "ASKСумОпер1010447" 
   (	"Ид" NUMBER, 
	"ИдДекл" NUMBER, 
	"КодОпер" VARCHAR2(14 BYTE), 
	"НалБаза" NUMBER(14,0), 
	"НалВосст" NUMBER(14,0)
   ) ;
--------------------------------------------------------
--  DDL for Table ASKСумОпер1010448
--------------------------------------------------------

  CREATE TABLE "ASKСумОпер1010448" 
   (	"Ид" NUMBER, 
	"ИдДекл" NUMBER, 
	"КодОпер" VARCHAR2(14 BYTE), 
	"КорНалБазаУв" NUMBER(14,0), 
	"КорНалБазаУм" NUMBER(14,0)
   ) ;
--------------------------------------------------------
--  DDL for Table ASKСумОпер1010450
--------------------------------------------------------

  CREATE TABLE "ASKСумОпер1010450" 
   (	"Ид" NUMBER, 
	"ИдДекл" NUMBER, 
	"КодОпер" VARCHAR2(14 BYTE), 
	"КорНалБазаУв" NUMBER(14,0), 
	"КорИсч.164.23Ув" NUMBER(14,0), 
	"КорНалБазаУм" NUMBER(14,0), 
	"КорИсч.164.23Ум" NUMBER(14,0)
   ) ;
--------------------------------------------------------
--  DDL for Table ASKСумОпер4
--------------------------------------------------------

  CREATE TABLE "ASKСумОпер4" 
   (	"Ид" NUMBER, 
	"ИдДекл" NUMBER, 
	"КодОпер" VARCHAR2(14 BYTE), 
	"НалБаза" NUMBER(14,0), 
	"НалВычПод" NUMBER(14,0), 
	"НалНеПод" NUMBER(14,0), 
	"НалВосст" NUMBER(14,0)
   ) ;
--------------------------------------------------------
--  DDL for Table ASKСумОпер5
--------------------------------------------------------

  CREATE TABLE "ASKСумОпер5" 
   (	"Ид" NUMBER, 
	"ИдСумПер" NUMBER, 
	"КодОпер" VARCHAR2(14 BYTE), 
	"НалБазаПод" NUMBER(14,0), 
	"НалВычПод" NUMBER(14,0), 
	"НалБазаНеПод" NUMBER(14,0), 
	"НалВычНеПод" NUMBER(14,0)
   ) ;
--------------------------------------------------------
--  DDL for Table ASKСумОпер6
--------------------------------------------------------

  CREATE TABLE "ASKСумОпер6" 
   (	"Ид" NUMBER, 
	"ИдДекл" NUMBER, 
	"КодОпер" VARCHAR2(14 BYTE), 
	"НалБаза" NUMBER(14,0), 
	"СумНал164" NUMBER(14,0), 
	"НалВычНеПод" NUMBER(14,0)
   ) ;
--------------------------------------------------------
--  DDL for Table ASKСумОпер7
--------------------------------------------------------

  CREATE TABLE "ASKСумОпер7" 
   (	"Ид" NUMBER, 
	"ИдДекл" NUMBER, 
	"КодОпер" VARCHAR2(14 BYTE), 
	"СтРеалТов" NUMBER(14,0), 
	"СтПриобТов" NUMBER(14,0), 
	"НалНеВыч" NUMBER(14,0)
   ) ;
--------------------------------------------------------
--  DDL for Table ASKСумПер
--------------------------------------------------------

  CREATE TABLE "ASKСумПер" 
   (	"Ид" NUMBER, 
	"ИдДекл" NUMBER, 
	"ОтчетГод" VARCHAR2(10 BYTE), 
	"Период" VARCHAR2(4 BYTE)
   ) ;
--------------------------------------------------------
--  DDL for Table ASKСумУплНА
--------------------------------------------------------

  CREATE TABLE "ASKСумУплНА" 
   (	"Ид" NUMBER, 
	"ИдДекл" NUMBER, 
	"КППИно" VARCHAR2(18 BYTE), 
	"КБК" VARCHAR2(40 BYTE), 
	"ОКТМО" VARCHAR2(22 BYTE), 
	"СумИсчисл" NUMBER(14,0), 
	"КодОпер" VARCHAR2(14 BYTE), 
	"СумИсчислОтгр" NUMBER(14,0), 
	"СумИсчислОпл" NUMBER(14,0), 
	"СумИсчислНА" NUMBER(14,0), 
	"НаимПрод" VARCHAR2(2000 BYTE), 
	"ИННПрод" VARCHAR2(24 BYTE), 
	"Фамилия" VARCHAR2(120 BYTE), 
	"Имя" VARCHAR2(120 BYTE), 
	"Отчество" VARCHAR2(120 BYTE)
   ) ;
--------------------------------------------------------
--  DDL for Table ASKФайлОпис
--------------------------------------------------------

  CREATE TABLE "ASKФайлОпис" 
   (	"Ид" NUMBER, 
	"Дата" DATE DEFAULT SYSDATE, 
	"ИдОпис" VARCHAR2(100 BYTE), 
	"Состояние" NUMBER, 
	"ДатаСост" DATE
   ) ;
--------------------------------------------------------
--  DDL for Table ASKGROUP
--------------------------------------------------------

  CREATE TABLE "ASKGROUP" 
   (	"ID" NUMBER, 
	"NUMALG" NUMBER, 
	"NUMGROUP" NUMBER, 
	"ENABLED" VARCHAR2(1 BYTE)
   ) ;
--------------------------------------------------------
--  DDL for Table ASKJOBLOG
--------------------------------------------------------

  CREATE TABLE "ASKJOBLOG" 
   (	"ID" NUMBER, 
	"JOBNAME" VARCHAR2(255 BYTE), 
	"NUMALG" NUMBER, 
	"STARTDATE" DATE, 
	"ENDDATE" DATE, 
	"SUCCEED" VARCHAR2(1 BYTE), 
	"INRECORDS" NUMBER, 
	"OUTRECORDS" NUMBER
   ) ;
--------------------------------------------------------
--  DDL for Index PK_ASKZIPФАЙЛ
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_ASKZIPФАЙЛ" ON "ASKZIPФайл" ("Ид") 
  ;
--------------------------------------------------------
--  DDL for Index PK_ASKАгрегация
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_ASKАгрегация" ON "ASKАгрегация" ("Ид") 
  ;
--------------------------------------------------------
--  DDL for Index PK_ASKСВЕДНАЛГОД
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_ASKСВЕДНАЛГОД" ON "ASKСведНалГод" ("Ид") 
  ;
--------------------------------------------------------
--  DDL for Index PK_ASKСВЕДНАЛГОДИ
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_ASKСВЕДНАЛГОДИ" ON "ASKСведНалГодИ" ("Ид") 
  ;
--------------------------------------------------------
--  DDL for Index PK_ASKСверка
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_ASKСверка" ON "ASKСверка" ("Ид") 
  ;
--------------------------------------------------------
--  DDL for Index PK_ASKСВОДЗАП
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_ASKСВОДЗАП" ON "ASKСводЗап" ("Ид") 
  ;
--------------------------------------------------------
--  DDL for Index PK_ASKСУМВОСУПЛ
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_ASKСУМВОСУПЛ" ON "ASKСумВосУпл" ("Ид") 
  ;
--------------------------------------------------------
--  DDL for Index PK_ASKСУМОПЕР1010447
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_ASKСУМОПЕР1010447" ON "ASKСумОпер1010447" ("Ид") 
  ;
--------------------------------------------------------
--  DDL for Index PK_ASKСУМОПЕР1010448
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_ASKСУМОПЕР1010448" ON "ASKСумОпер1010448" ("Ид") 
  ;
--------------------------------------------------------
--  DDL for Index PK_ASKСУМОПЕР1010450
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_ASKСУМОПЕР1010450" ON "ASKСумОпер1010450" ("Ид") 
  ;
--------------------------------------------------------
--  DDL for Index PK_ASKСУМОПЕР4
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_ASKСУМОПЕР4" ON "ASKСумОпер4" ("Ид") 
  ;
--------------------------------------------------------
--  DDL for Index PK_ASKСУМОПЕР5
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_ASKСУМОПЕР5" ON "ASKСумОпер5" ("Ид") 
  ;
--------------------------------------------------------
--  DDL for Index PK_ASKСУМОПЕР6
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_ASKСУМОПЕР6" ON "ASKСумОпер6" ("Ид") 
  ;
--------------------------------------------------------
--  DDL for Index PK_ASKСУМОПЕР7
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_ASKСУМОПЕР7" ON "ASKСумОпер7" ("Ид") 
  ;
--------------------------------------------------------
--  DDL for Index PK_ASKСУМПЕР
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_ASKСУМПЕР" ON "ASKСумПер" ("Ид") 
  ;
--------------------------------------------------------
--  DDL for Index PK_ASKСУМУПЛНА
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_ASKСУМУПЛНА" ON "ASKСумУплНА" ("Ид") 
  ;
--------------------------------------------------------
--  DDL for Index PK_ASKФАЙЛОПИС
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_ASKФАЙЛОПИС" ON "ASKФайлОпис" ("Ид") 
  ;
--------------------------------------------------------
--  DDL for Index PK_ASKALGGROUP
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_ASKALGGROUP" ON "ASKGROUP" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index PK_ASKJOB
--------------------------------------------------------

  CREATE UNIQUE INDEX "PK_ASKJOB" ON "ASKJOBLOG" ("ID") 
  ;
--------------------------------------------------------
--  Constraints for Table ASKZIPФайл
--------------------------------------------------------

  ALTER TABLE "ASKZIPФайл" ADD CONSTRAINT "PK_ASKZIPФАЙЛ" PRIMARY KEY ("Ид") ENABLE;
 
  ALTER TABLE "ASKZIPФайл" MODIFY ("Ид" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table ASKАгрегация
--------------------------------------------------------

  ALTER TABLE "ASKАгрегация" ADD CONSTRAINT "PK_ASKАгрегация" PRIMARY KEY ("Ид") ENABLE;
 
  ALTER TABLE "ASKАгрегация" MODIFY ("Ид" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table ASKДекл
--------------------------------------------------------

  ALTER TABLE "ASKДекл" MODIFY ("Ид" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table ASKСведНалГод
--------------------------------------------------------

  ALTER TABLE "ASKСведНалГод" ADD CONSTRAINT "PK_ASKСВЕДНАЛГОД" PRIMARY KEY ("Ид") ENABLE;
 
  ALTER TABLE "ASKСведНалГод" MODIFY ("Ид" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table ASKСведНалГодИ
--------------------------------------------------------

  ALTER TABLE "ASKСведНалГодИ" ADD CONSTRAINT "PK_ASKСВЕДНАЛГОДИ" PRIMARY KEY ("Ид") ENABLE;
 
  ALTER TABLE "ASKСведНалГодИ" MODIFY ("Ид" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table ASKСверка
--------------------------------------------------------

  ALTER TABLE "ASKСверка" ADD CONSTRAINT "PK_ASKСверка" PRIMARY KEY ("Ид") ENABLE;
 
  ALTER TABLE "ASKСверка" MODIFY ("Ид" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table ASKСводЗап
--------------------------------------------------------

  ALTER TABLE "ASKСводЗап" ADD CONSTRAINT "PK_ASKСВОДЗАП" PRIMARY KEY ("Ид") ENABLE;
 
  ALTER TABLE "ASKСводЗап" MODIFY ("Ид" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table ASKСумВосУпл
--------------------------------------------------------

  ALTER TABLE "ASKСумВосУпл" ADD CONSTRAINT "PK_ASKСУМВОСУПЛ" PRIMARY KEY ("Ид") ENABLE;
 
  ALTER TABLE "ASKСумВосУпл" MODIFY ("Ид" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table ASKСумОпер1010447
--------------------------------------------------------

  ALTER TABLE "ASKСумОпер1010447" ADD CONSTRAINT "PK_ASKСУМОПЕР1010447" PRIMARY KEY ("Ид") ENABLE;
 
  ALTER TABLE "ASKСумОпер1010447" MODIFY ("Ид" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table ASKСумОпер1010448
--------------------------------------------------------

  ALTER TABLE "ASKСумОпер1010448" ADD CONSTRAINT "PK_ASKСУМОПЕР1010448" PRIMARY KEY ("Ид") ENABLE;
 
  ALTER TABLE "ASKСумОпер1010448" MODIFY ("Ид" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table ASKСумОпер1010450
--------------------------------------------------------

  ALTER TABLE "ASKСумОпер1010450" ADD CONSTRAINT "PK_ASKСУМОПЕР1010450" PRIMARY KEY ("Ид") ENABLE;
 
  ALTER TABLE "ASKСумОпер1010450" MODIFY ("Ид" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table ASKСумОпер4
--------------------------------------------------------

  ALTER TABLE "ASKСумОпер4" ADD CONSTRAINT "PK_ASKСУМОПЕР4" PRIMARY KEY ("Ид") ENABLE;
 
  ALTER TABLE "ASKСумОпер4" MODIFY ("Ид" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table ASKСумОпер5
--------------------------------------------------------

  ALTER TABLE "ASKСумОпер5" ADD CONSTRAINT "PK_ASKСУМОПЕР5" PRIMARY KEY ("Ид") ENABLE;
 
  ALTER TABLE "ASKСумОпер5" MODIFY ("Ид" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table ASKСумОпер6
--------------------------------------------------------

  ALTER TABLE "ASKСумОпер6" ADD CONSTRAINT "PK_ASKСУМОПЕР6" PRIMARY KEY ("Ид") ENABLE;
 
  ALTER TABLE "ASKСумОпер6" MODIFY ("Ид" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table ASKСумОпер7
--------------------------------------------------------

  ALTER TABLE "ASKСумОпер7" ADD CONSTRAINT "PK_ASKСУМОПЕР7" PRIMARY KEY ("Ид") ENABLE;
 
  ALTER TABLE "ASKСумОпер7" MODIFY ("Ид" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table ASKСумПер
--------------------------------------------------------

  ALTER TABLE "ASKСумПер" ADD CONSTRAINT "PK_ASKСУМПЕР" PRIMARY KEY ("Ид") ENABLE;
 
  ALTER TABLE "ASKСумПер" MODIFY ("Ид" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table ASKСумУплНА
--------------------------------------------------------

  ALTER TABLE "ASKСумУплНА" ADD CONSTRAINT "PK_ASKСУМУПЛНА" PRIMARY KEY ("Ид") ENABLE;
 
  ALTER TABLE "ASKСумУплНА" MODIFY ("Ид" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table ASKФайлОпис
--------------------------------------------------------

  ALTER TABLE "ASKФайлОпис" ADD CONSTRAINT "PK_ASKФАЙЛОПИС" PRIMARY KEY ("Ид") ENABLE;
 
  ALTER TABLE "ASKФайлОпис" MODIFY ("Ид" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table ASKGROUP
--------------------------------------------------------

  ALTER TABLE "ASKGROUP" ADD CONSTRAINT "PK_ASKALGGROUP" PRIMARY KEY ("ID") ENABLE;
 
  ALTER TABLE "ASKGROUP" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table ASKJOBLOG
--------------------------------------------------------

  ALTER TABLE "ASKJOBLOG" ADD CONSTRAINT "PK_ASKJOB" PRIMARY KEY ("ID") ENABLE;
 
  ALTER TABLE "ASKJOBLOG" MODIFY ("ID" NOT NULL ENABLE);
