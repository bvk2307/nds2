﻿ALTER session SET nls_length_semantics=CHAR
/
create table NDS2_MC.ASKGROUP 
(
   ID                   NUMBER               not null,
   NUMALG               NUMBER,
   NUMGROUP             NUMBER,
   ENABLED              VARCHAR2(1),
   constraint PK_ASKGROUP1 primary key (ID)
)
/
create table NDS2_MC."ASKZIPФайл" 
(
   "Ид"                 NUMBER               not null,
   "ИмяФайла"           VARCHAR2(300),
   "ИдФайлОпис"         NUMBER,
   "ИдГП3"              VARCHAR2(100),
   "ИсхПуть"            VARCHAR2(300),
   "Путь"               VARCHAR2(300),
   "ИдДокОбр"           VARCHAR2(100),
   "КодНО"              VARCHAR2(4),
   "ДатаФайла"          DATE                 default SYSDATE,
   "Статус"             VARCHAR2(1)          default '0',
   "ДатаСтатуса"        DATE                 default SYSDATE,
   "Ошибка"             VARCHAR2(2000),
   "ИдЗагрузка"         NUMBER,
   constraint PK_ASKZIPФАЙЛ primary key ("Ид")
)
/
create table NDS2_MC."ASKZIPФайл_Вр" 
(
   ROW_NUM              NUMBER               not null,
   "Ид"                 NUMBER,
   "ИсхПуть"            VARCHAR2(300),
   "ИмяФайла"           VARCHAR2(300),
   constraint PK_ASKZIP_ВР primary key (ROW_NUM)
)
/
create table NDS2_MC."ASKДекл" 
(
   "Ид"                 NUMBER               not null,
   ZIP                  NUMBER               not null,
   "ИдФайл"             VARCHAR2(100),
   "ВерсПрог"           VARCHAR2(40),
   "ВерсФорм"           VARCHAR2(5),
   "ПризнНал8-12"       VARCHAR2(1),
   "ПризнНал8"          VARCHAR2(1),
   "ПризнНал81"         VARCHAR2(1),
   "ПризнНал9"          VARCHAR2(1),
   "ПризнНал91"         VARCHAR2(1),
   "ПризнНал10"         VARCHAR2(1),
   "ПризнНал11"         VARCHAR2(1),
   "ПризнНал12"         VARCHAR2(1),
   КНД                  VARCHAR2(7),
   "ДатаДок"            DATE,
   "Период"             VARCHAR2(2),
   "ОтчетГод"           VARCHAR2(4),
   "КодНО"              VARCHAR2(4),
   "НомКорр"            VARCHAR2(3),
   "ПоМесту"            VARCHAR2(3),
   ОКВЭД                VARCHAR2(8),
   "Тлф"                VARCHAR2(20),
   "НаимОрг"            VARCHAR2(1000),
   ИНННП                VARCHAR2(12),
   КППНП                VARCHAR2(9),
   "ФормРеорг"          VARCHAR2(1),
   "ИННРеорг"           VARCHAR2(10),
   "КППРеорг"           VARCHAR2(9),
   "ФамилияНП"          VARCHAR2(60),
   "ИмяНП"              VARCHAR2(60),
   "ОтчествоНП"         VARCHAR2(60),
   "ПрПодп"             VARCHAR2(1),
   "ФамилияПодп"        VARCHAR2(60),
   "ИмяПодп"            VARCHAR2(60),
   "ОтчествоПодп"       VARCHAR2(60),
   "НаимДок"            VARCHAR2(120),
   "НаимОргПред"        VARCHAR2(1000),
   ОКТМО                VARCHAR2(11),
   КБК                  VARCHAR2(20),
   "СумПУ173.5"         NUMBER(14),
   "СумПУ173.1"         NUMBER(14),
   "НомДогИТ"           VARCHAR2(10),
   "ДатаДогИТ"          DATE,
   "ДатаНачДогИТ"       DATE,
   "ДатаКонДогИТ"       DATE,
   "НалПУ164"           NUMBER(14),
   "НалВосстОбщ"        NUMBER(14),
   "РлТв18НалБаз"       NUMBER(14),
   "РеалТов18СумНал"    NUMBER(14),
   "РлТв10НалБаз"       NUMBER(14),
   "РеалТов10СумНал"    NUMBER(14),
   "РлТв118НалБаз"      NUMBER(14),
   "РлТв118СумНал"      NUMBER(14),
   "РлТв110НалБаз"      NUMBER(14),
   "РлТв110СумНал"      NUMBER(14),
   "РлПрдИКНалБаз"      NUMBER(14),
   "РлПрдИКСумНал"      NUMBER(14),
   "ВыпСМРСобНалБаз"    NUMBER(14),
   "ВыпСМРСобСумНал"    NUMBER(14),
   "ОпПрдПстНлБаз"      NUMBER(14),
   "ОплПрдПстСумНал"    NUMBER(14),
   "СумНалВс"           NUMBER(14),
   "СумНал170.3.5"      NUMBER(14),
   "СумНал170.3.3"      NUMBER(14),
   "КорРлТв18НалБаз"    NUMBER(14),
   "КорРлТв18СумНал"    NUMBER(14),
   "КорРлТв10НалБаз"    NUMBER(14),
   "КорРлТв10СумНал"    NUMBER(14),
   "КорРлТв118НлБз"     NUMBER(14),
   "КорРлТв118СмНл"     NUMBER(14),
   "КорРлТв110НлБз"     NUMBER(14),
   "КорРлТв110СмНл"     NUMBER(14),
   "КорРлПрдИКНлБз"     NUMBER(14),
   "КорРлПрдИКСмНл"     NUMBER(14),
   "НалПредНППриоб"     NUMBER(14),
   "НалПредНППок"       NUMBER(14),
   "НалИсчСМР"          NUMBER(14),
   "НалУплТамож"        NUMBER(14),
   "НалУплНОТовТС"      NUMBER(14),
   "НалИсчПрод"         NUMBER(14),
   "НалУплПокНА"        NUMBER(14),
   "НалВычОбщ"          NUMBER(14),
   "СумИсчислИтог"      NUMBER(14),
   "СумВозмПдтв"        NUMBER(14),
   "СумВозмНеПдтв"      NUMBER(14),
   "СумНал164Ит"        NUMBER(14),
   "НалВычНеПодИт"      NUMBER(14),
   "НалИсчислИт"        NUMBER(14),
   "СмОп1010449КдОп"    VARCHAR2(7),
   "СмОп1010449НлБз"    NUMBER(14),
   "СмОп1010449КрИсч"   NUMBER(14),
   "СмОп1010449НлВст"   NUMBER(14),
   "ОплПостСв6Мес"      NUMBER(14),
   "НаимКнПок"          VARCHAR2(100),
   "НаимКнПокДЛ"        VARCHAR2(100),
   "НаимКнПрод"         VARCHAR2(100),
   "НаимКнПродДЛ"       VARCHAR2(100),
   "НаимЖУчВыстСчФ"     VARCHAR2(100),
   "НаимЖУчПолучСчФ"    VARCHAR2(100),
   "НмВстСчФ173_5"      VARCHAR2(100),
   "Публ"               VARCHAR2(1),
   "ИдЗагрузка"         NUMBER,
   "КодОпер47"          VARCHAR2(7),
   "НалБаза47"          NUMBER,
   "НалВосст47"         NUMBER,
   "КодОпер48"          VARCHAR2(7),
   "КорНалБазаУв48"     NUMBER,
   "КорНалБазаУм48"     NUMBER,
   "КодОпер50"          VARCHAR2(7),
   "КорНалБазаУв50"     NUMBER,
   "КорИсчУв50"         NUMBER,
   "КорНалБазаУм50"     NUMBER,
   "КорИсчУм50"         NUMBER,
   "КлючДекл"           VARCHAR2(32),
   constraint "PK_ASKДекл" primary key ("Ид")
)
/
create table NDS2_MC."ASKДеклПериод"
(
  "Ид"      NUMBER not null,
  "ИдДекл"  NUMBER not null,
  "Год"     NUMBER(4),
  "Квартал" NUMBER(1)
)
/
create table NDS2_MC."ASKДекл_Вр" 
(
   ROW_NUM              NUMBER               not null,
   "Ид"                 NUMBER,
   ZIP                  NUMBER,
   constraint PK_ASKДЕКЛВР primary key (ROW_NUM)
)
/
create table NDS2_MC."ASKЖурнал" 
(
   "Ид"                 NUMBER               not null,
   "ИдОпер"             NUMBER               not null,
   "НаимПроцесс"        VARCHAR2(255),
   "Итерация"           NUMBER,
   "ДатаНач"            DATE,
   "ДатаОконч"          DATE,
   "ПризнРезульт"       VARCHAR2(1),
   "КолвоВхЗап"         NUMBER,
   "КолвоВыхЗап"        NUMBER,
   constraint "PK_ASKЖурнал" primary key ("Ид")
)
/
create table NDS2_MC."ASKЖурналУч" 
(
   "Ид"                 NUMBER               not null,
   ZIPЧ1                NUMBER               not null,
   "ИдФайл"             VARCHAR2(200),
   "ВерсПрог"           VARCHAR2(40),
   "ВерсФорм"           VARCHAR2(5),
   "ИдФайлИсх"          VARCHAR2(100),
   "ИдФайлПерв"         VARCHAR2(100),
   "КолФайл"            NUMBER,
   КНД                  VARCHAR2(7),
   "Период"             VARCHAR2(2),
   "ОтчетГод"           VARCHAR2(4),
   "НаимОрг"            VARCHAR2(1000),
   ИНН                  VARCHAR2(12),
   КПП                  VARCHAR2(9),
   "СвГосРегИП"         VARCHAR2(100),
   "Фамилия"            VARCHAR2(60),
   "Имя"                VARCHAR2(60),
   "Отчество"           VARCHAR2(60),
   "ПрПодп"             VARCHAR2(1),
   "ФамилияПодп"        VARCHAR2(60),
   "ИмяПодп"            VARCHAR2(60),
   "ОтчествоПодп"       VARCHAR2(60),
   "НаимДок"            VARCHAR2(120),
   "ИдЗамена"           NUMBER,
   "КлючЖурн"           VARCHAR2(32),
   "ПрНеобр"            VARCHAR2(1)          default '1',
   constraint "PK_ЖурналУч" primary key ("Ид")
)
/
create table NDS2_MC."ASKЖурналУч_Вр" 
(
   ROW_NUM              NUMBER               not null,
   "ИдЧ1"               NUMBER,
   ZIP                  CLOB,
   constraint PK_ASKЖУРНУЧВР primary key (ROW_NUM)
)
/
create table NDS2_MC."ASKЖурналЧасть" 
(
   "Ид"                 NUMBER               not null,
   "ИдЖурн"             NUMBER,
   ZIP                  NUMBER               not null,
   "ИдФайл"             VARCHAR2(200),
   "ВерсПрог"           VARCHAR2(40),
   "ВерсФорм"           VARCHAR2(5),
   "ИдФайлИсх"          VARCHAR2(100),
   "ИдФайлПерв"         VARCHAR2(100),
   "КолФайл"            NUMBER,
   "НомФайл"            NUMBER,
   КНД                  VARCHAR2(7),
   "Период"             VARCHAR2(2),
   "ОтчетГод"           VARCHAR2(4),
   "НаимОрг"            VARCHAR2(1000),
   ИНН                  VARCHAR2(12),
   КПП                  VARCHAR2(9),
   "СвГосРегИП"         VARCHAR2(100),
   "Фамилия"            VARCHAR2(60),
   "Имя"                VARCHAR2(60),
   "Отчество"           VARCHAR2(60),
   "ПрПодп"             VARCHAR2(1),
   "ФамилияПодп"        VARCHAR2(60),
   "ИмяПодп"            VARCHAR2(60),
   "ОтчествоПодп"       VARCHAR2(60),
   "НаимДок"            VARCHAR2(120),
   "ИдЗагрузка"         NUMBER,
   "ОшибкиФК"           VARCHAR2(100),
   "КолЗапЧ1"           NUMBER,
   "КолЗапЧ2"           NUMBER,
   "КлючКоррЖурн"       VARCHAR2(32),
   "ПрНеобр"            VARCHAR2(1)          default '1',
   constraint PK_ЖУРЧ primary key ("Ид")
)
/
create table NDS2_MC."ASKКонтрСоотн" 
(
   "Ид"                 NUMBER               not null,
   "ИдДекл"             NUMBER               not null,
   "КодКС"              VARCHAR2(10),
   "ЛеваяЧасть"         NUMBER,
   "ПраваяЧасть"        NUMBER,
   "Выполн"             VARCHAR2(1),
   constraint PK_КС primary key ("Ид")
)
/
create table NDS2_MC."ASKОперация" 
(
   "Ид"                 NUMBER               not null,
   "НомерОп"            NUMBER               not null,
   "ВидОп"              VARCHAR2(1),
   "ДатаНач"            DATE                 default SYSDATE,
   "ДатаОконч"          DATE,
   "ДатаОшибки"         DATE,
   constraint "PK_ASKОперация" primary key ("Ид")
)
/
create table NDS2_MC."ASKПоясн_Вр" 
(
   ROW_NUM              NUMBER               not null,
   "ИдДеклар"           NUMBER,
   "ОтчетГод"           VARCHAR2(4),
   "Период"             VARCHAR2(2),
   ZIP                  NUMBER,
   "Пояснения"          CLOB,
   constraint PK_ASKПОЯСНВР primary key (ROW_NUM)
)
/
create table NDS2_MC."ASKПояснение" 
(
   "Ид"                 NUMBER               not null,
   ZIP                  NUMBER               not null,
   "ИдФайл"             VARCHAR2(150),
   "ВерсПрог"           VARCHAR2(40),
   "ВерсФорм"           VARCHAR2(5),
   КНД                  VARCHAR2(7),
   "ДатаДок"            DATE,
   "ПризнНал8"          VARCHAR2(1),
   "ПризнНал81"         VARCHAR2(1),
   "ПризнНал9"          VARCHAR2(1),
   "ПризнНал91"         VARCHAR2(1),
   "ПризнНал10"         VARCHAR2(1),
   "ПризнНал11"         VARCHAR2(1),
   "ПризнНал12"         VARCHAR2(1),
   "КолФайлОтв"         NUMBER,
   "ПризОтпрУП"         VARCHAR2(1),
   "НаимОргОтпр"        VARCHAR2(1000),
   "ИННОтпр"            VARCHAR2(12),
   "КППОтпр"            VARCHAR2(9),
   "ФамилияОтпр"        VARCHAR2(60),
   "ИмяОтпр"            VARCHAR2(60),
   "ОтчествоОтпр"       VARCHAR2(60),
   "КодНО"              VARCHAR2(4),
   "НаимОргНП"          VARCHAR2(1000),
   ИНННП                VARCHAR2(12),
   КППНП                VARCHAR2(9),
   "ФамилияНП"          VARCHAR2(60),
   "ИмяНП"              VARCHAR2(60),
   "ОтчествоНП"         VARCHAR2(60),
   "ПрПодп"             VARCHAR2(1),
   "Должн"              VARCHAR2(128),
   "Тлф"                VARCHAR2(20),
   "E-mail"             VARCHAR2(45),
   "ФамилияПодп"        VARCHAR2(60),
   "ИмяПодп"            VARCHAR2(60),
   "ОтчествоПодп"       VARCHAR2(60),
   "НаимДок"            VARCHAR2(120),
   "НомТреб"            NUMBER,
   "ДатаТреб"           DATE,
   "Период"             VARCHAR2(2),
   "ОтчетГод"           VARCHAR2(4),
   "НомКорр"            NUMBER,
   "ИмяФайлТреб"        VARCHAR2(200),
   "НаимКнПок"          VARCHAR2(200),
   "НаимКнПокДЛ"        VARCHAR2(200),
   "НаимКнПрод"         VARCHAR2(200),
   "НаимКнПродДЛ"       VARCHAR2(200),
   "НаимЖУчВыстСчФ"     VARCHAR2(200),
   "НаимЖУчПолучСчФ"    VARCHAR2(200),
   "НаимВыст_173.5"     VARCHAR2(200),
   "ДатаВремяЗакр"      DATE,
   "ИдЗагрузка"         NUMBER,
   "ИдДекл"             NUMBER,
   "ОбработанМС"        VARCHAR2(1)          default '0',
   "КолЗапВсего"        NUMBER,
   "КолЗапПодтв"        NUMBER,
   "КолЗапИзм"          NUMBER,
   "КолЗапНесовп"       NUMBER,
   "ИмяФайла"			VARCHAR2(200),
   constraint "PK_Пояснение" primary key ("Ид")
)
/
create table NDS2_MC."ASKСведНалГод" 
(
   "Ид"                 NUMBER               not null,
   "ИдСумВосУпл"        NUMBER               not null,
   "ГодОтч"             VARCHAR2(4),
   "ДатаИсп170"         DATE,
   "ДоляНеОбл"          NUMBER(4,1),
   "НалГод"             NUMBER(14),
   constraint PK_СВЕДНАЛГ primary key ("Ид")
)
/
create table NDS2_MC."ASKСведНалГодИ" 
(
   "Ид"                 NUMBER               not null,
   "ИдДекл"             NUMBER               not null,
   "КППИнУч"            VARCHAR2(9),
   "СумНалИсч"          NUMBER(14),
   "СумНалВыч"          NUMBER(14),
   constraint PK_СВЕДНАЛГИ primary key ("Ид")
)
/
create table NDS2_MC."ASKСводЗап" 
(
  "Ид"           NUMBER not null,
  zip            NUMBER not null,
  "Индекс"       VARCHAR2(7 CHAR),
  "НомКорр"      NUMBER,
  "ПризнакАкт"   VARCHAR2(1 CHAR),
  "ТипФайла"     VARCHAR2(1 CHAR),
  "СумНДСПок"    NUMBER,
  "СумНДСПокДЛ"  NUMBER,
  "СтПрод18"     NUMBER,
  "СтПрод10"     NUMBER,
  "СтПрод0"      NUMBER,
  "СумНДСПрод18" NUMBER,
  "СумНДСПрод10" NUMBER,
  "СтПрод"       NUMBER,
  "СтПродОсв"    NUMBER,
  "СтПрод18ДЛ"   NUMBER,
  "СтПрод10ДЛ"   NUMBER,
  "СтПрод0ДЛ"    NUMBER,
  "СумНДС18ДЛ"   NUMBER,
  "СумНДС10ДЛ"   NUMBER,
  "СтПродОсвДЛ"  NUMBER,
  "ИдЗагрузка"   NUMBER,
  "ИдЗамена"     NUMBER,
  "ОшибкиЛК"     VARCHAR2(200 CHAR),
  "ОшибкиФК"     VARCHAR2(200 CHAR),
  "ИдДекл"       NUMBER not null,
  "КолЗаписей"   NUMBER,
  "ПрНеобр0"     VARCHAR2(1 CHAR) default '1',
  "ПрНеобр1"     VARCHAR2(1 CHAR) default '1',
  "ИдАкт"        NUMBER,
   constraint PK_ASKСВОДЗАП primary key ("Ид")
)
/
create table NDS2_MC."ASKСводЗап_Вр" 
(
   ROW_NUM              NUMBER               not null,
   "Ид"                 NUMBER,
   ZIP                  NUMBER,
   "ИдЗам"              NUMBER,
   "ZIPЗам"             NUMBER,
   "ТипФайла"           VARCHAR2(1),
   constraint PK_ASKСВОД_ВР primary key (ROW_NUM)
)
/
create table NDS2_MC."ASKСумВосУпл" 
(
   "Ид"                 NUMBER               not null,
   "ИдДекл"             NUMBER               not null,
   "НаимНедв"           VARCHAR2(100),
   "КодОпНедв"          VARCHAR2(7),
   "ДатаВводОН"         DATE,
   "ДатаНачАмОтч"       DATE,
   "СтВводОН"           NUMBER(14),
   "НалВычОН"           NUMBER(14),
   "Индекс"             VARCHAR2(6),
   "КодРегион"          VARCHAR2(2),
   "Район"              VARCHAR2(50),
   "Город"              VARCHAR2(50),
   "НаселПункт"         VARCHAR2(50),
   "Улица"              VARCHAR2(50),
   "Дом"                VARCHAR2(20),
   "Корпус"             VARCHAR2(20),
   "Кварт"              VARCHAR2(20),
   constraint PK_ASKСУМВОСУПЛ primary key ("Ид")
)
/
create table NDS2_MC."ASKСумОпер4" 
(
   "Ид"                 NUMBER               not null,
   "ИдДекл"             NUMBER               not null,
   "КодОпер"            VARCHAR2(7),
   "НалБаза"            NUMBER(14),
   "НалВычПод"          NUMBER(14),
   "НалНеПод"           NUMBER(14),
   "НалВосст"           NUMBER(14),
   constraint PK_ASKСУМОПЕР4 primary key ("Ид")
)
/
create table NDS2_MC."ASKСумОпер5" 
(
   "Ид"                 NUMBER               not null,
   "ИдСумПер"           NUMBER               not null,
   "КодОпер"            VARCHAR2(7),
   "НалБазаПод"         NUMBER(14),
   "НалВычПод"          NUMBER(14),
   "НалБазаНеПод"       NUMBER(14),
   "НалВычНеПод"        NUMBER(14),
   constraint PK_ASKСУМОПЕР5 primary key ("Ид")
)
/
create table NDS2_MC."ASKСумОпер6" 
(
   "Ид"                 NUMBER               not null,
   "ИдДекл"             NUMBER               not null,
   "КодОпер"            VARCHAR2(7),
   "НалБаза"            NUMBER(14),
   "СумНал164"          NUMBER(14),
   "НалВычНеПод"        NUMBER(14),
   constraint PK_ASKСУМОПЕР6 primary key ("Ид")
)
/
create table NDS2_MC."ASKСумОпер7" 
(
   "Ид"                 NUMBER               not null,
   "ИдДекл"             NUMBER               not null,
   "КодОпер"            VARCHAR2(7),
   "СтРеалТов"          NUMBER(14),
   "СтПриобТов"         NUMBER(14),
   "НалНеВыч"           NUMBER(14),
   constraint PK_ASKСУМОПЕР7 primary key ("Ид")
)
/
create table NDS2_MC."ASKСумПер" 
(
   "Ид"                 NUMBER               not null,
   "ИдДекл"             NUMBER               not null,
   "ОтчетГод"           VARCHAR2(4),
   "Период"             VARCHAR2(2),
   constraint PK_ASKСУМПЕР primary key ("Ид")
)
/
create table NDS2_MC."ASKСумУплНА" 
(
   "Ид"                 NUMBER               not null,
   "ИдДекл"             NUMBER               not null,
   "КППИно"             VARCHAR2(9),
   КБК                  VARCHAR2(20),
   ОКТМО                VARCHAR2(11),
   "СумИсчисл"          NUMBER(14),
   "КодОпер"            VARCHAR2(7),
   "СумИсчислОтгр"      NUMBER(14),
   "СумИсчислОпл"       NUMBER(14),
   "СумИсчислНА"        NUMBER(14),
   "НаимПрод"           VARCHAR2(1000),
   "ИННПрод"            VARCHAR2(12),
   "Фамилия"            VARCHAR2(60),
   "Имя"                VARCHAR2(60),
   "Отчество"           VARCHAR2(60),
   constraint PK_ASKСУМУПЛНА primary key ("Ид")
)
/
create table NDS2_MC."ASKСчетчики" 
(
   "Ид"                 NUMBER               not null,
   "ИдЖурнал"           NUMBER,
   "Счетчик"            VARCHAR2(255),
   "Значение"           NUMBER,
   constraint "PK_ASKСчетчики" primary key ("Ид")
)
/
create table NDS2_MC."ASKФайлОпис" 
(
   "Ид"                 NUMBER               not null,
   "Дата"               DATE                 default SYSDATE,
   "ИдОпис"             VARCHAR2(100),
   "Состояние"          NUMBER,
   "ДатаСост"           DATE,
   constraint PK_ASKФАЙЛОПИС primary key ("Ид")
)
/
alter table NDS2_MC."ASKДекл"
   add constraint "FK_ASKДекл_ZIP" foreign key (ZIP)
      references NDS2_MC."ASKZIPФайл" ("Ид")
      on delete cascade
/
alter table NDS2_MC."ASKЖурналУч"
   add constraint FK_ЖУРУЧ_ZIP foreign key (ZIPЧ1)
      references NDS2_MC."ASKZIPФайл" ("Ид")
      on delete cascade
/
alter table NDS2_MC."ASKЖурналЧасть"
   add constraint FK_ASKЖУРЧ_ZIP foreign key (ZIP)
      references NDS2_MC."ASKZIPФайл" ("Ид")
      on delete cascade
/
alter table NDS2_MC."ASKЖурналЧасть"
   add constraint FK_ASKЖУРЧ_ЖУР foreign key ("ИдЖурн")
      references NDS2_MC."ASKЖурналУч" ("Ид")
/
alter table NDS2_MC."ASKКонтрСоотн"
   add constraint FK_КС_ДЕКЛ foreign key ("ИдДекл")
      references NDS2_MC."ASKДекл" ("Ид")
      on delete cascade
/
alter table NDS2_MC."ASKПояснение"
   add constraint FK_ПОЯСН_ZIP foreign key (ZIP)
      references NDS2_MC."ASKZIPФайл" ("Ид")
      on delete cascade
/
alter table NDS2_MC."ASKПояснение"
   add constraint FK_ПОЯСН_Д foreign key ("ИдДекл")
      references NDS2_MC."ASKДекл" ("Ид")
      on delete cascade
/
alter table NDS2_MC."ASKСведНалГод"
   add constraint "FK_ASKСвГод_Д" foreign key ("ИдСумВосУпл")
      references NDS2_MC."ASKСумВосУпл" ("Ид")
      on delete cascade
/
alter table NDS2_MC."ASKСведНалГодИ"
   add constraint "FK_ASKНалГодИ_Д" foreign key ("ИдДекл")
      references NDS2_MC."ASKДекл" ("Ид")
      on delete cascade
/
alter table NDS2_MC."ASKСводЗап"
   add constraint FK_ASKСВЗАП_Д foreign key ("ИдДекл")
      references NDS2_MC."ASKДекл" ("Ид")
/
alter table NDS2_MC."ASKСумВосУпл"
   add constraint "FK_ASKCумВос_Д" foreign key ("ИдДекл")
      references NDS2_MC."ASKДекл" ("Ид")
      on delete cascade
/
alter table NDS2_MC."ASKСумОпер4"
   add constraint "FK_ASKCумОп4_Д" foreign key ("ИдДекл")
      references NDS2_MC."ASKДекл" ("Ид")
      on delete cascade
/
alter table NDS2_MC."ASKСумОпер5"
   add constraint "FK_ASKСумОп5_Д" foreign key ("ИдСумПер")
      references NDS2_MC."ASKСумПер" ("Ид")
      on delete cascade
/
alter table NDS2_MC."ASKСумОпер6"
   add constraint "FK_ASKCумОп6_Д" foreign key ("ИдДекл")
      references NDS2_MC."ASKДекл" ("Ид")
      on delete cascade
/
alter table NDS2_MC."ASKСумОпер7"
   add constraint "FK_ASKCумОп7_Д" foreign key ("ИдДекл")
      references NDS2_MC."ASKДекл" ("Ид")
      on delete cascade
/
alter table NDS2_MC."ASKСумПер"
   add constraint "FK_ASKCумПер_Д" foreign key ("ИдДекл")
      references NDS2_MC."ASKДекл" ("Ид")
      on delete cascade
/
alter table NDS2_MC."ASKСумУплНА"
   add constraint "FK_ASKCумУаНа_Д" foreign key ("ИдДекл")
      references NDS2_MC."ASKДекл" ("Ид")
      on delete cascade
/
alter table NDS2_MC."ASKСчетчики"
   add constraint "FK_ASKСчет_Журн" foreign key ("ИдЖурнал")
      references NDS2_MC."ASKЖурнал" ("Ид")
      on delete cascade
/
INSERT INTO NDS2_MC.ASKGROUP (ID, NUMALG, NUMGROUP, ENABLED) VALUES (1, 2121222, 0, '1')
/
INSERT INTO NDS2_MC.ASKGROUP (ID, NUMALG, NUMGROUP, ENABLED) VALUES (2, 2121322, 1, '1')
/
INSERT INTO NDS2_MC.ASKGROUP (ID, NUMALG, NUMGROUP, ENABLED) VALUES (4, 2121392, 2, '1')
/
INSERT INTO NDS2_MC.ASKGROUP (ID, NUMALG, NUMGROUP, ENABLED) VALUES (5, 2121922, 2, '1')
/
INSERT INTO NDS2_MC.ASKGROUP (ID, NUMALG, NUMGROUP, ENABLED) VALUES (6, 2121329, 2, '1')
/
INSERT INTO NDS2_MC.ASKGROUP (ID, NUMALG, NUMGROUP, ENABLED) VALUES (7, 2121992, 3, '1')
/
INSERT INTO NDS2_MC.ASKGROUP (ID, NUMALG, NUMGROUP, ENABLED) VALUES (8, 2121399, 3, '1')
/
INSERT INTO NDS2_MC.ASKGROUP (ID, NUMALG, NUMGROUP, ENABLED) VALUES (9, 2121929, 3, '1')
/
INSERT INTO NDS2_MC.ASKGROUP (ID, NUMALG, NUMGROUP, ENABLED) VALUES (10, 9121322, 4, '1')
/
INSERT INTO NDS2_MC.ASKGROUP (ID, NUMALG, NUMGROUP, ENABLED) VALUES (11, 9121392, 5, '1')
/
INSERT INTO NDS2_MC.ASKGROUP (ID, NUMALG, NUMGROUP, ENABLED) VALUES (12, 9121922, 5, '1')
/
INSERT INTO NDS2_MC.ASKGROUP (ID, NUMALG, NUMGROUP, ENABLED) VALUES (13, 9121329, 5, '1')
/
INSERT INTO NDS2_MC.ASKGROUP (ID, NUMALG, NUMGROUP, ENABLED) VALUES (14, 2191322, 6, '1')
/
INSERT INTO NDS2_MC.ASKGROUP (ID, NUMALG, NUMGROUP, ENABLED) VALUES (15, 2191392, 7, '1')
/
INSERT INTO NDS2_MC.ASKGROUP (ID, NUMALG, NUMGROUP, ENABLED) VALUES (16, 2191922, 7, '1')
/
INSERT INTO NDS2_MC.ASKGROUP (ID, NUMALG, NUMGROUP, ENABLED) VALUES (17, 2191329, 7, '1')
/
commit
/

ALTER TABLE NDS2_MC."ASKКонтрСоотн" ADD "ДатаРасчета" DATE DEFAULT sysdate;
