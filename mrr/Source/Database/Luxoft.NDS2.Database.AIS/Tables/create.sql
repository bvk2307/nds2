﻿ALTER session SET nls_length_semantics=CHAR;
/*==============================================================*/
/* Table: "serialgen"                                           */
/*==============================================================*/
CREATE TABLE NDS2_MC."serialgen" 
   (	"DummyID" NUMBER NOT NULL ENABLE, 
	"CurValue" NUMBER, 
	"TableName" VARCHAR2(40)
   ) ;  
/*==============================================================*/
/* Table: "SПараметры"                                          */
/*==============================================================*/
CREATE TABLE NDS2_MC."SПараметры" 
(
   "Ид"                 NUMBER               not null,
   "НаименПараметра"    VARCHAR2(60),
   "КодПараметра"       VARCHAR2(20),
   "Значение"           VARCHAR2(254),
   "ИдГруппы"           NUMBER,
   constraint PK_SПАРАМЕТРЫ primary key ("Ид"),
   constraint AUNQ_KOD_SПАРАМЕТРЫ unique ("КодПараметра")
);

/*==============================================================*/
/* Table: WEBCACHE                                              */
/*==============================================================*/
CREATE TABLE NDS2_MC.WEBCACHE 
(
   ID                   NUMBER               not null,
   SQLTEXT              CLOB,
   SQLMD5               VARCHAR2(32),
   CREATETIME           DATE,
   CREATEUSER           VARCHAR2(32),
   ACCESSTIME           DATE,
   ACCESSUSER           VARCHAR2(32),
   ERRSTR               VARCHAR2(1000),
   PAGECOUNT            NUMBER,
   ROWCOUNT             NUMBER,
   ACCESSCOUNT          NUMBER,
   GRID                 VARCHAR2(100),
   constraint PK_WEBCACHE primary key (ID)
);

/*==============================================================*/
/* Table: WEBCACHEPAGES                                         */
/*==============================================================*/
CREATE TABLE NDS2_MC.WEBCACHEPAGES 
(
   ID                   NUMBER               not null,
   WEBCACHE             NUMBER               not null,
   PAGENUM              NUMBER,
   PAGEDATA             CLOB,
   ROWCOUNT             NUMBER,
   constraint PK_WEBCACHEPAGES primary key (ID)
);

/*==============================================================*/
/* Index: WEBCAHCEPAGES_I0                                      */
/*==============================================================*/
create index NDS2_MC.WEBCAHCEPAGES_I0 on NDS2_MC.WEBCACHEPAGES (
   WEBCACHE ASC
);

/*==============================================================*/
/* Table: WEBERRDICT                                            */
/*==============================================================*/
CREATE TABLE NDS2_MC.WEBERRDICT 
(
   ID                   NUMBER               not null,
   ERRCODE              VARCHAR2(255),
   ERRTEXT              VARCHAR2(4000),
   constraint UK_WEBERRDICT_ERRCODE unique (ERRCODE)
);

/*==============================================================*/
/* Table: WEBERRLOG                                             */
/*==============================================================*/
CREATE TABLE NDS2_MC.WEBERRLOG 
(
   ID                   NUMBER               not null,
   DTERR                DATE                 default SYSDATE,
   LOGIN                VARCHAR2(32),
   ERR                  VARCHAR2(2000),
   SQL                  VARCHAR2(2000),
   ID_WEBLOG            NUMBER,
   URL                  VARCHAR2(2000),
   TOMCAT_IP            VARCHAR2(50),
   constraint SYS_C0027897 primary key (ID)
);

/*==============================================================*/
/* Table: WEBLOG                                                */
/*==============================================================*/
CREATE TABLE NDS2_MC.WEBLOG 
(
   ID                   NUMBER               not null,
   GNI                  VARCHAR2(4)          default '????',
   NETADDR              VARCHAR2(35),
   OPDATE               DATE,
   SYSTEM               VARCHAR2(50),
   OBJECT               VARCHAR2(32),
   OPERATION            VARCHAR2(32),
   QUERY                VARCHAR2(2000),
   LOGIN                VARCHAR2(32),
   OPFINISHDATE         DATE,
   INSDATE              DATE                 default SYSDATE,
   TOMCAT_IP            VARCHAR2(50),
   USER_AGENT           VARCHAR2(500),
   GRID_ID              VARCHAR2(100),
   constraint PK_WEBLOG primary key (ID)
);

/*==============================================================*/
/* Table: WEBPROFSTATUS                                         */
/*==============================================================*/
CREATE TABLE NDS2_MC.WEBPROFSTATUS 
(
   ID                   NUMBER               not null,
   PROF_ID              NUMBER,
   STATUS               VARCHAR2(1),
   DOCUMENT             VARCHAR2(500),
   constraint WEBPROFSTATUS_PK primary key (ID),
   constraint WEBPROFSTATUS_UK1 unique (PROF_ID)
);

/*==============================================================*/
/* Table: WEBPROJECTS                                           */
/*==============================================================*/
CREATE TABLE NDS2_MC.WEBPROJECTS 
(
   PNAME                VARCHAR2(32)         not null,
   DESCRIPTION          VARCHAR2(512),
   ENABLED              VARCHAR2(1),
   ORDERBY              NUMBER,
   WEBTYPE              VARCHAR2(1),
   ACTDATE              DATE,
   VERSION              VARCHAR2(32),
   XML                  CLOB,
   CAPTION              VARCHAR2(128),
   ID                   NUMBER               not null,
   COMPNAME             VARCHAR2(50),
   USERNAME             VARCHAR2(50),
   constraint PK_WEBPROJECTS primary key (PNAME)
);

/*==============================================================*/
/* Table: WEBQASYNCDATA                                         */
/*==============================================================*/
CREATE TABLE NDS2_MC.WEBQASYNCDATA 
(
   ID                   NUMBER               not null,
   SQLSTR               VARCHAR2(4000),
   BEGDATE              DATE                 default SYSDATE,
   ENDDATE              DATE,
   ERR                  VARCHAR2(2000),
   JOBID                NUMBER,
   RESDATA              BLOB,
   REMARK               VARCHAR2(2000),
   LOGIN                VARCHAR2(32),
   PROJECT              VARCHAR2(255),
   GRIDNAME             VARCHAR2(512),
   WHERESTR             VARCHAR2(2000),
   RESTYPE              VARCHAR2(40),
   SQLTEXT              CLOB,
   VISIBLECOLUMNS       VARCHAR2(2048),
   COLUMNSORDER         VARCHAR2(2048),
   TOPQ                 NUMBER,
   ISTOP                NUMBER,
   DSNAME               VARCHAR2(100),
   constraint PK_WEBQASYNCDATA primary key (ID)
);

/*==============================================================*/
/* Table: WEBQDATA                                              */
/*==============================================================*/
CREATE TABLE NDS2_MC.WEBQDATA 
(
   ID                   NUMBER               not null,
   FKWEBQUERIES         NUMBER,
   SQLSTR               VARCHAR2(4000),
   BEGDATE              DATE                 default SYSDATE,
   ENDDATE              DATE,
   ERR                  VARCHAR2(2000),
   JOBID                NUMBER,
   RESDATA              BLOB,
   REMARK               VARCHAR2(2000),
   LOGIN                VARCHAR2(32),
   RESTYPE              VARCHAR2(40),
   SQLTEXT              CLOB,
   constraint PK_WEBQDATA primary key (ID)
);

/*==============================================================*/
/* Table: WEBQDATAFILE                                          */
/*==============================================================*/
CREATE TABLE NDS2_MC.WEBQDATAFILE 
(
   ID                   NUMBER               not null,
   WEBQDATA             NUMBER,
   WEBQASYNCDATA        NUMBER,
   FILENAME             VARCHAR2(255),
   FILEDATA             BLOB,
   constraint PK_WEBQDATAFILE primary key (ID)
);

/*==============================================================*/
/* Index: WEBQDATAFILE_I1                                       */
/*==============================================================*/
create index NDS2_MC.WEBQDATAFILE_I1 on NDS2_MC.WEBQDATAFILE (
   WEBQDATA ASC
);

/*==============================================================*/
/* Index: WEBQDATAFILE_I2                                       */
/*==============================================================*/
create index NDS2_MC.WEBQDATAFILE_I2 on NDS2_MC.WEBQDATAFILE (
   WEBQASYNCDATA ASC
);

/*==============================================================*/
/* Table: WEBQFIELDS                                            */
/*==============================================================*/
CREATE TABLE NDS2_MC.WEBQFIELDS 
(
   ID                   NUMBER               not null,
   FKWEBQUERIES         NUMBER,
   IDGRID               NUMBER,
   IDCOLUMN             VARCHAR2(64),
   VISIBLE              NUMBER,
   ORDERBY              NUMBER,
   GROUPBY              NUMBER,
   QBE                  VARCHAR2(1000),
   FUNCTION             VARCHAR2(1000),
   CAPTION              VARCHAR2(1000),
   EXPR                 VARCHAR2(1000),
   ISMASTER             VARCHAR2(1),
   constraint PK_WEBQFIELDS primary key (ID)
);

/*==============================================================*/
/* Table: WEBQFILE                                              */
/*==============================================================*/
CREATE TABLE NDS2_MC.WEBQFILE 
(
   ID                   NUMBER               not null,
   PROJECT              VARCHAR2(35),
   LOGIN                VARCHAR2(32),
   FILENAME             VARCHAR2(255),
   DESCRIPTION          VARCHAR2(255),
   CRDATE               DATE                 default SYSDATE,
   constraint SYS_C0027905 primary key (ID)
);

/*==============================================================*/
/* Table: WEBQFILEDATA                                          */
/*==============================================================*/
CREATE TABLE NDS2_MC.WEBQFILEDATA 
(
   ID                   NUMBER               not null,
   FKWEBQFILE           NUMBER               not null,
   FIELD1               VARCHAR2(4000),
   FIELD2               VARCHAR2(4000),
   FIELD3               VARCHAR2(4000),
   constraint SYS_C0027906 primary key (ID)
);

/*==============================================================*/
/* Index: WEBQFILEDATA_I1                                       */
/*==============================================================*/
create index NDS2_MC.WEBQFILEDATA_I1 on NDS2_MC.WEBQFILEDATA (
   FKWEBQFILE ASC
);

/*==============================================================*/
/* Table: WEBQUERIES                                            */
/*==============================================================*/
CREATE TABLE NDS2_MC.WEBQUERIES 
(
   ID                   NUMBER               not null,
   QNAME                VARCHAR2(60),
   PROJECT              VARCHAR2(35),
   LOGIN                VARCHAR2(32),
   SQLSTR               VARCHAR2(4000),
   CRDATE               DATE                 default SYSDATE,
   IDMASTER             NUMBER,
   IDDETAIL             NUMBER,
   JOINSTR              VARCHAR2(256),
   QTYPE                NUMBER,
   constraint PK_WEBQUERIES primary key (ID)
);

/*==============================================================*/
/* Table: WEBREMARKS                                            */
/*==============================================================*/
CREATE TABLE NDS2_MC.WEBREMARKS 
(
   ID                   NUMBER               not null,
   LOGIN                VARCHAR2(32),
   OPDATE               DATE                 default SYSDATE,
   SUBJECT              VARCHAR2(255),
   REMARK               VARCHAR2(4000),
   constraint PK_WEBREMARKS primary key (ID)
);

/*==============================================================*/
/* Table: WEBROLES                                              */
/*==============================================================*/
CREATE TABLE NDS2_MC.WEBROLES 
(
   ID                   NUMBER               not null,
   NAME                 VARCHAR2(32),
   DESCRIPTION          VARCHAR2(128),
   PRIORITY             NUMBER(1)            not null,
   ADMGROUP             NUMBER(1),
   DESCRIBE             VARCHAR2(1000),
   constraint PK_WEBROLES primary key (ID),
   constraint AUK_WEBROLES_NAME_WEBROLES unique (NAME)
);

/*==============================================================*/
/* Table: WEBROLESPROF                                          */
/*==============================================================*/
CREATE TABLE NDS2_MC.WEBROLESPROF 
(
   ID                   NUMBER               not null,
   ROLE1                NUMBER               not null,
   ROLE2                NUMBER               not null,
   constraint PK_WEBROLESPROF primary key (ID)
);

/*==============================================================*/
/* Index: WEBROLESPROF_ROLE1                                    */
/*==============================================================*/
create index NDS2_MC.WEBROLESPROF_ROLE1 on NDS2_MC.WEBROLESPROF (
   ROLE1 ASC
);

/*==============================================================*/
/* Index: WEBROLESPROF_ROLE2                                    */
/*==============================================================*/
create index NDS2_MC.WEBROLESPROF_ROLE2 on NDS2_MC.WEBROLESPROF (
   ROLE2 ASC
);

/*==============================================================*/
/* Table: WEBSETTINGS                                           */
/*==============================================================*/
CREATE TABLE NDS2_MC.WEBSETTINGS 
(
   ID                   NUMBER               not null,
   USER_ID              NUMBER               not null,
   NAME                 VARCHAR2(64)         not null,
   VALUE                VARCHAR2(2048),
   constraint PK_WEBSETTINGS primary key (ID)
);

/*==============================================================*/
/* Table: WEBUSERINFO                                           */
/*==============================================================*/
CREATE TABLE NDS2_MC.WEBUSERINFO 
(
   LOGIN                VARCHAR2(64)         not null,
   DECISION_DATE        DATE,
   DECISION_NUM         VARCHAR2(64),
   DECISION_BY          VARCHAR2(64),
   constraint PK_WEBUSERINFO primary key (LOGIN)
);

/*==============================================================*/
/* Table: WEBUSERROLES                                          */
/*==============================================================*/
CREATE TABLE NDS2_MC.WEBUSERROLES 
(
   ID                   NUMBER               not null,
   LOGIN                VARCHAR2(32)         not null,
   ROLENAME             VARCHAR2(32)         not null,
   constraint PK_WEBUSERROLES primary key (ID)
);

/*==============================================================*/
/* Index: IDX_WEBUSERROLES_LOGIN                                */
/*==============================================================*/
create index NDS2_MC.IDX_WEBUSERROLES_LOGIN on NDS2_MC.WEBUSERROLES (
   LOGIN ASC
);

/*==============================================================*/
/* Table: WEBUSERS                                              */
/*==============================================================*/
CREATE TABLE NDS2_MC.WEBUSERS 
(
   ID                   NUMBER               not null,
   LOGIN                VARCHAR2(32)         not null,
   PASSWORD             VARCHAR2(32)         not null,
   NAME                 VARCHAR2(128)        not null,
   GNI                  VARCHAR2(4),
   PASSDATE             DATE,
   PASSPERIOD           NUMBER(3),
   POSITION             VARCHAR2(80),
   PHONE                VARCHAR2(35),
   EMAIL                VARCHAR2(48),
   ENABLED              VARCHAR2(1),
   CRDATE               DATE,
   DISDATE              DATE,
   INN                  VARCHAR2(12),
   SUBDIVISION          VARCHAR2(128),
   QDATE                DATE                 default SYSDATE+5000,
   LASTDATE             DATE,
   CSVCNT               NUMBER(10),
   CERTDATE             DATE,
   ENDCERTDATE          DATE,
   PRZPAY               NUMBER(1),
   USERTYPE             VARCHAR2(255),
   SUMPAY               NUMBER(9),
   constraint PK_WEBUSERS primary key (ID),
   constraint AKEY_WEBUSERS_LOGIN_WEBUSERS unique (LOGIN)
);

/*==============================================================*/
/* View: WEBALLUSERROLES_VIEW                                   */
/*==============================================================*/
create or replace force view NDS2_MC.WEBALLUSERROLES_VIEW as
select login,rolename from webuserroles
union
select ur.login, r2.name
  from webroles r, webuserroles ur, webrolesprof p, webroles r2
 where ur.rolename = r.name
   and p.role1 = r.id
   and p.role2 = r2.id
union select login,'tomcat' from webusers;

/*==============================================================*/
/* View: WEBPROFILES_VIEW                                       */
/*==============================================================*/
create or replace force view NDS2_MC.WEBPROFILES_VIEW as
SELECT r.id p_id,
  r.name p_name,
  r.description p_description,
  r.priority p_priority,
  NVL(r.admgroup,0) p_admgroup,
  R.DESCRIBE P_DESCRIBE,
  S.ID p_STATUS_ID,
  NVL(S.STATUS,'0') P_STATUS,
  s.document p_document
FROM webroles r
LEFT OUTER JOIN webprofstatus s
ON s.prof_id=r.id
WHERE r.id IN (SELECT role1 FROM webrolesprof);

comment on column NDS2_MC.WEBPROFILES_VIEW.P_DOCUMENT is
'Реквизиты регламентирующего документа';

alter table NDS2_MC.WEBCACHEPAGES
   add constraint WEBCACHEPAGES_F0 foreign key (WEBCACHE)
      references NDS2_MC.WEBCACHE (ID)
      on delete cascade
      not deferrable;

alter table NDS2_MC.WEBPROFSTATUS
   add constraint FK_WEBPROFSTATUS_WEBROLES foreign key (PROF_ID)
      references NDS2_MC.WEBROLES (ID)
      on delete cascade
      not deferrable;

alter table NDS2_MC.WEBQASYNCDATA
   add constraint WEBQASYNCDATA_F0 foreign key (TOPQ)
      references NDS2_MC.WEBQASYNCDATA (ID)
      on delete cascade
      not deferrable;

alter table NDS2_MC.WEBQDATA
   add constraint FK_WEBQUERIES foreign key (FKWEBQUERIES)
      references NDS2_MC.WEBQUERIES (ID)
      on delete cascade
      not deferrable;

alter table NDS2_MC.WEBQDATAFILE
   add constraint WEBQDATAFILE_F1 foreign key (WEBQDATA)
      references NDS2_MC.WEBQDATA (ID)
      on delete cascade
      not deferrable;

alter table NDS2_MC.WEBQDATAFILE
   add constraint WEBQDATAFILE_F2 foreign key (WEBQASYNCDATA)
      references NDS2_MC.WEBQASYNCDATA (ID)
      on delete cascade
      not deferrable;

alter table NDS2_MC.WEBQFIELDS
   add constraint FK_WEBQUERIES_1 foreign key (FKWEBQUERIES)
      references NDS2_MC.WEBQUERIES (ID)
      on delete cascade
      not deferrable;

alter table NDS2_MC.WEBQFILEDATA
   add constraint FK_WEBQFILE foreign key (FKWEBQFILE)
      references NDS2_MC.WEBQFILE (ID)
      on delete cascade
      not deferrable;

alter table NDS2_MC.WEBROLESPROF
   add constraint WEBROLESPROF_R1 foreign key (ROLE1)
      references NDS2_MC.WEBROLES (ID)
      on delete cascade
      not deferrable;

alter table NDS2_MC.WEBROLESPROF
   add constraint WEBROLESPROF_R2 foreign key (ROLE2)
      references NDS2_MC.WEBROLES (ID)
      on delete cascade
      not deferrable;

alter table NDS2_MC.WEBUSERINFO
   add constraint FK_WEBUSERINFO_WEBUSERS foreign key (LOGIN)
      references NDS2_MC.WEBUSERS (LOGIN)
      on delete cascade
      not deferrable;

alter table NDS2_MC.WEBUSERROLES
   add constraint WEBUSERROLES_WEBUSERS foreign key (LOGIN)
      references NDS2_MC.WEBUSERS (LOGIN)
      on delete cascade
      not deferrable;

