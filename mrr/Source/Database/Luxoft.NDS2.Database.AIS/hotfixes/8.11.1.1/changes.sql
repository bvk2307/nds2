﻿create table NDS2_MC."ASKДеклПериод"
(
  "Ид"      NUMBER not null,
  "ИдДекл"  NUMBER not null,
  "Год"     NUMBER(4),
  "Квартал" NUMBER(1)
);

create index NDS2_MC."IDX_ASKДеклПер" on NDS2_MC."ASKДеклПериод" ("ИдДекл");