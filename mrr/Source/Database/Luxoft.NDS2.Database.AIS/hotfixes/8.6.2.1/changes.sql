﻿CREATE OR REPLACE PROCEDURE NDS2_MC.P$POJASNENIE_ADD_ASKZIPFAJL
(
  pFileName IN VARCHAR2,
  pPathDirectory IN VARCHAR2,
  pSounCode IN VARCHAR2,
  pZip OUT NUMBER
)
as
  pSerialGenCurrent NUMBER;
  pSerialGenNext NUMBER;
  pOlderPartId NUMBER;
  pId NUMBER;
begin
  pSerialGenCurrent := 0;
  
  select nvl("CurValue", 0) into pSerialGenCurrent 
  from NDS2_MC."serialgen"
  where "TableName" = 'ASKZipФайл'
  for update;

  pSerialGenNext := pSerialGenCurrent + 1;

  update NDS2_MC."serialgen" set "CurValue" = pSerialGenNext
  where "TableName" = 'ASKZipФайл';

  pOlderPartId := round(dbms_random.value(0, 32767));
  pId := pOlderPartId * POWER(2, 48) + pSerialGenCurrent;

  insert into NDS2_MC."ASKZIPФайл"
  (
     "Ид","ИмяФайла","ИдФайлОпис","ИдГП3",
     "ИсхПуть","Путь","ИдДокОбр","КодНО",
     "ДатаФайла","Статус","ДатаСтатуса",
     "Ошибка"
  )
  values(pId, pFileName, null, null,
         pPathDirectory, null, null, pSounCode,
         sysdate, 0, null, null);

  commit;

  pZip := pId;

end;
/
