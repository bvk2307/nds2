﻿
CREATE OR REPLACE PACKAGE "NDS2_MC"."AIS_UTILS" as
  -- Author  : FIRAT
  -- Created : 17.02.2004 15:09:49
  -- Purpose : AIS_UTILS

      /*
  Пакет предначначен для общесистемных функций и процедур
  */

  -- VIT 02.03.2004 ошибки
  err_custom_error exception;
  pragma exception_init(err_custom_error, -20001);
  ern_custom_error constant number := -20001;

  err_insert_error exception;
  pragma exception_init(err_insert_error, -20002);
  ern_insert_error constant number := -20002;

  err_link_error exception;
  pragma exception_init(err_link_error, -20003);
  ern_link_error constant number := -20003;

  err_length_exceeded exception;
  pragma exception_init(err_length_exceeded, -20004);
  ern_length_exceeded constant number := -20004;

  err_wrong_delimiters exception;
  pragma exception_init(err_wrong_delimiters, -20005);
  ern_wrong_delimiters constant number := -20005;

  err_check_error exception;
  pragma exception_init(err_check_error, -20006);
  ern_check_error constant number := -20006;

  type serial_numbers_t is table of number;
  type serial_tables_t is table of binary_integer index by varchar2(40);

  -- VIT 30.11.2006
  FUNCTION ERR_WHENCE RETURN VARCHAR2;

      /*
  Функция возвращает параметр из таблицы SПараметры
  Передается код параметра -строка
  */
  function Get_PARAMETR(AIS_PAR in varchar2) return varchar2;

      /*
  Функция возвращает значение счетчика
  Передается наименование счетчика - строка
  CntRange - шаг значения, чтобы за раз можно было получить
  несколько значений
  Если ReadOnly = 1, то просто возвращает очередное значение, не правя его в базе.
  Возвращает значение счетчика - число
  */
  function Get_NextValue(AIS_CntName in varchar2, CntRange in number := 1,
     ReadOnly in number := 0) return number;

  -- VIT 02.03.2004 стандартный обработчик ошибок
  procedure RaiseError(error_source  varchar2,
                       error_message varchar2 default ERR_WHENCE || sqlerrm, -- VIT 30.11.2006
                       error_code    number default ern_custom_error);

  -- VIT 02.03.2004 serialgen
  function GetSerialNumber(table_name varchar2) return number;

  -- VIT 24.06.2004 serialgen without cache
  function GetSerialNumberNoCache(table_name varchar2) return number;

  -- VIT 09.02.2005
  function GetSerialNumberNoCacheEx(
    table_name varchar2,
    request_count number
  ) return number;

      /*
  "SБлокировка" (
  "Ид" INTEGER NOT NULL,
  "Класс" VARCHAR2(120) NOT NULL,
  "Объект" VARCHAR2(120) NOT NULL,
  "Владелец" VARCHAR2(120) NULL,
  "ДатаУстановки" DATE NULL,
  */
      /*
  Процедура установки блокировки пишет в таблицу SБлокировка
  если такая уже существует ("Класс"=ObjectName,"Объект"=ObjectID),
  то возвращает в LockID=null
  если еще нет, то  в LockID=Ид уникальный номер блокировки.
  "Ид"=serialnumber
  "Класс"=ObjectName
  "Объект"=ObjectID
  "Владелец"=UserName
  "ДатаУстановки"=sysdate
  */
  procedure RequestLock(ObjectName varchar2,
                        ObjectID   varchar2,
                        UserName   varchar2,
                        LockID     in out number);

      /*
  Процедура снятия блокировки
  delete "SБлокировка" where "Ид"=LockID
  commit
  если все нормально, вернуть LockID=0, иначе LockID=1
  */
  procedure ReleaseLock(LockID in out number);

      /*
  Процедура создания задания
  НАЧАО=sysdate - 1 час (вдруг на сервере время меньше)
  АКТИВНЫЙ=Нет
  СЕРВЕР - по IDProcess из таблицы SЗАДАНИЕ.СЕРВЕР
  на каком текущий процесс там и создаем
  ПОЛЬЗОВАТЕЛЬ - тоже так же берем из текущего задания
  IDProcess - Ид SЗАДАНИЕ
  TaskName - КРАТНАИМЕН SКЛПРОЦЕСС
  TaskParams - параметры задания
  */
  procedure TaskMaker(IDProcess  number,
                      TaskName   varchar2,
                      TaskParams varchar2);
  -- VIT 29.07.2004
  -- Процедура, выполняющая чистку clob-ов
  procedure Clean;

  -- VIT 29.07.2004
  -- Процедура, выполняющая создиние задания (JOB), которое будет переодически
  -- выполняться (раз в сутки в определенный час).
  -- Параметр - час, в который будет стартовать задание.
  procedure CreateCleanJob(Hour in number);

  -- VIT 28.03.2005
  -- Функция для выполнения асинхронного запроса
  -- Параметр: QueryID - WEBQDATA.ID
  -- Результат: JOBID, либо NULL если создание JOB-а сломалось
  function AsyncQuery(QueryID in number) return number;

  -- VIT 28.03.2005
  -- Собственно функционал выполнения запроса и сохранение результата в WEBQDATA.RESDATA тут
  procedure AsyncQueryProcess(QueryID in number, JobID in number);

  -- VIT 28.03.2005
  -- Функция для прибивания сессиии запроса и соотв. джоба
  -- Параметр: QueryID - WEBQDATA.ID
  function KillQuery(QueryID in number) return number;

  -- VIT 14.10.2005 то же что предыдущие функции но работают с таблицей WEBQASYNCDATA
  function AsyncQueryNew(QueryID in number) return number;
  procedure AsyncQueryProcessNew(QueryID in number, JobID in number);
  function KillQueryNew(QueryID in number) return number;

  -- VIT 11.09.2007
  function RequestLockEx(ObjectName in varchar2, ObjectID in varchar2, UserName in varchar2) return number;
  function ReleaseLockEx(ObjectName in varchar2, ObjectID in varchar2, UserName in varchar2) return number;

  -- VIT 14.11.2007
  procedure AsyncQueryCompleteNew(QueryID in number);

end AIS_UTILS;
/

CREATE OR REPLACE PACKAGE BODY "NDS2_MC"."AIS_UTILS" 
as
  -- VIT 25.05.2009 Mantis 0004741
  USE_SCHEDULER constant boolean := true;

  l_arrSerialCurrNumbers serial_numbers_t := serial_numbers_t();
  l_arrSerialNextNumbers serial_numbers_t := serial_numbers_t();
  l_arrSerialTables      serial_tables_t;

  -- VIT 30.11.2006
  FUNCTION ERR_WHENCE RETURN VARCHAR2
  AS
    l_back_trace  VARCHAR2(4096) DEFAULT DBMS_UTILITY.FORMAT_ERROR_BACKTRACE;
    l_pos integer;
  BEGIN
    if substr(l_back_trace, length(l_back_trace), 1) = CHR(10)
    then
      l_back_trace := substr(l_back_trace, 1, length(l_back_trace)-1);
    end if;
    l_pos := instr(l_back_trace, CHR(10), -1);
    if l_pos > 0 then
      l_back_trace := substr(l_back_trace, l_pos+1);
    end if;
    return l_back_trace || CHR(10);
  END ERR_WHENCE;

--------------------------------------------------------------------------------------
  function Get_PARAMETR(AIS_PAR in  varchar2) return varchar2 is
    CURSOR zn_cur IS SELECT "Значение" FROM "SПараметры" WHERE "КодПараметра" = AIS_PAR;
    zn_rec zn_cur%ROWTYPE;
  begin
   --Rus
    OPEN zn_cur;
    FETCH zn_cur INTO zn_rec;
    IF zn_cur%FOUND THEN
      CLOSE zn_cur;
      return zn_rec."Значение";
    ELSE
      return null;
    END IF;
  exception
    when others then
     CLOSE zn_cur;
     raise_application_error(
        -20001, 'ошибка в Get_Parametr'|| chr(13) ||
        ERR_WHENCE || sqlerrm); -- VIT 30.11.2006
  end;
----------------------------------------------------------------------------------------

-- VIT 29.07.2009 fix
 function Get_NextValue(AIS_CntName in  varchar2,CntRange in number :=1,
   ReadOnly in number := 0)return number is

   pragma autonomous_transaction;
   --FOR UPDATE - блокирует строки таблицы
   CURSOR Counter_cur IS SELECT * FROM "SСчетчик" WHERE "Наименование"=trim(AIS_CntName) FOR UPDATE;
   Counter_rec Counter_cur%ROWTYPE;
   id number;
 begin

-- проверить пустой ли курсор
-- если пустой курсор, то нужно создать новую запись счетчика
-- КАК-ТО НУЖНО ЗАБЛОКИРОВАТЬ ЗАПИСЬ, ЧТОБЫ ДРУГИЕ ПРОЦЕССЫ НЕ ВЗЯЛИ ТАКОЕ ЖЕ ЗНАЧЕНИЕ
-- INSERT INTO ""SСчетчик""(""Ид"",""Наименование"",""Значение"") VALUES( , , )
-- если уже существует счетчик, то наращиваем значение на 1
-- Значение=Значение+1 и обновляем запись
--"UPDATE ""SСчетчик"" SET ""Значение"" =   WHERE ""Ид""=   "

   OPEN Counter_cur;                  --открыли курсор
   FETCH Counter_cur INTO Counter_rec;--получили строку

   IF Counter_cur%NOTFOUND THEN          --проверяем на пустоту
     --если нет
     Counter_rec."Значение" := 1;
     --получить ид, вставить новое наименование и вернуть 1
     if ReadOnly = 0 then
       id:=GetSerialNumber('SСчетчик');
       begin
         INSERT INTO "SСчетчик" ("Ид","Наименование","Значение") VALUES(id,trim(AIS_CntName),CntRange);
       exception
         when others then
           declare
             l_errmsg varchar2(512) := sqlerrm;
           begin
             if (instr(l_errmsg, 'ORA-00001') > 0) and
                (instr(l_errmsg, 'SСчетчик_Наименование') > 0)
             then
               close Counter_cur;
               open Counter_cur;
               fetch Counter_cur into counter_rec;
               if Counter_cur%notfound then
                 RaiseError('AIS_UTILS.Get_NextValue', 'Counter_cur not found');
               end if;
             else
               raise;
             end if;
           end;
       end;
     end if;
   END IF;

   IF Counter_cur%FOUND THEN
     --если есть
     --изменить значение на CntRange, и вернуть следующее значение
     id:=Counter_rec."Значение"+CntRange;
     if ReadOnly = 0 then
       UPDATE "SСчетчик" SET "Значение" =id  WHERE "Ид"=Counter_rec."Ид";
     end if;
     Counter_rec."Значение" := Counter_rec."Значение" + 1;
   END IF;

   if ReadOnly = 0 then
     COMMIT;
   end if;
   CLOSE Counter_cur;
   RETURN Counter_rec."Значение";

 exception
  when others then
    IF Counter_cur%ISOPEN THEN
     ROLLBACK;
     CLOSE Counter_cur;
    END IF;
    raise_application_error(
        -20001, 'ошибка в Get_NextValue'|| chr(13) ||
        ERR_WHENCE || sqlerrm); -- VIT 30.11.2006
 end;

-------------------------------------------------------------------------------------------
-- стандартный обработчик ошибок
  procedure RaiseError(
    error_source varchar2,
    error_message varchar2 default ERR_WHENCE || sqlerrm, -- VIT 30.11.2006
    error_code number default ern_custom_error
  )
  is
  begin
    if not error_source is null then
      raise_application_error(
        error_code, 'ошибка в ' || error_source || chr(13) || error_message);
    else
      raise_application_error(
        error_code, error_message);
    end if;
  end;

-------------------------------------------------------------------------------------------
-- возвращет Serial для primary key таблицы table_name
-- допускает параллельное использование с штормовым SerialGen-ом
  function GetSerialNumber(
    table_name varchar2
  ) return number
  is
    pragma autonomous_transaction; -- без этого объявления будет вылетать
                                   -- ORA-14551: cannot perform a DML operation inside a query
    cursor serialgen_cur is
      select * from "serialgen"
      where "TableName"=table_name
      for update;

    l_currNumber   number;
    l_nextNumber   number;
    l_index        binary_integer;
    l_serialGenRec serialgen_cur%rowtype;
    -- VIT 11.12.2007
    l_table_found boolean;
  begin
    -- VIT 11.12.2007 dont use exists() anymore
    -- if l_arrSerialTables.exists(table_name) then
    l_table_found := false;
    begin
      l_index := l_arrSerialTables(table_name);
      -- ok, we are here - table_name found
      l_table_found := true;
    exception
      when NO_DATA_FOUND then
        null; -- nothing to do - just continue
    end;
    if l_table_found then
-- если для запрашиваемой таблицы уже зарезервирован диапозон номеров
-- и этот диапозон не исчерпан, то возвращаем l_currNumber
-- и пишем l_currNumber+1 в l_arrSerialCurrNumbers
      -- VIT 11.12.2007 was set before
      -- l_index := l_arrSerialTables(table_name);
      l_currNumber := l_arrSerialCurrNumbers(l_index);
      l_nextNumber := l_arrSerialNextNumbers(l_index);
      if l_currNumber <= l_NextNumber then
-- если диапозон номеров не исчерпан, то возвращаем результат
-- граница диапозона входит в диапозон
        l_arrSerialCurrNumbers(l_index) := l_currNumber+1;
        return l_currNumber;
      end if;
    else
-- если таблицы в коллекции нету, то добавляем
      l_arrSerialCurrNumbers.extend;
      l_arrSerialNextNumbers.extend;
      l_index := l_arrSerialCurrNumbers.count;
      l_arrSerialTables(table_name) := l_index;
    end if;
-- иначе запрашиваем текущее значение из таблицы serialgen
    open serialgen_cur;
    fetch serialgen_cur into l_serialGenRec;
    if serialgen_cur%found then
-- если запись выбрана, то вычисляем текущее и следующее значения
      close serialgen_cur;
      if l_serialGenRec."CurValue" is null then
        l_currNumber := 0;
      else
        l_currNumber := l_serialGenRec."CurValue";
      end if;
      l_nextNumber := l_currNumber + 50; -- порциями по 50 записей
      update "serialgen" set "CurValue"=l_nextNumber
        where "TableName"=table_name;
    else
-- если же не нашлось записи в serialgen для таблицы table_name,
-- то добавляем ее
      close serialgen_cur;
      l_currNumber := 0;
      l_nextNumber := l_currNumber + 50;
      insert into "serialgen" ("DummyID", "CurValue", "TableName")
        select nvl(max("DummyID"), 0)+1, l_nextNumber, table_name from "serialgen";
    end if;
-- фиксируем изменения
    commit;
-- следующее значение
    l_currNumber := l_currNumber+1;
-- теперь записываем l_currValue в l_arrSerialCurrNumbers
-- и l_nextValue - в l_arrSerialNextNumbers
    l_arrSerialCurrNumbers(l_index) := l_currNumber+1;
    l_arrSerialNextNumbers(l_index) := l_nextNumber;
-- возвращаем результат
    return l_currNumber;
  exception
    when others then
      if serialgen_cur%isopen then
        close serialgen_cur;
      end if;
      RaiseError('GetSerialNumber');
  end;

-------------------------------------------------------------------------------------------
-- The same as GetSerialNumber except do not use cache for serial numbers
  function GetSerialNumberNoCache(
    table_name varchar2
  ) return number
  is
    pragma autonomous_transaction; -- без этого объявления будет вылетать
                                   -- ORA-14551: cannot perform a DML operation inside a query
    cursor serialgen_cur is
      select * from "serialgen"
      where "TableName"=table_name
      for update;

    l_nextNumber   number;
    l_serialGenRec serialgen_cur%rowtype;
  begin
    open serialgen_cur;
    fetch serialgen_cur into l_serialGenRec;
    if serialgen_cur%found then
-- если запись выбрана, то вычисляем текущее и следующее значения
      close serialgen_cur;
      if l_serialGenRec."CurValue" is null then
        l_nextNumber := 1;
      else
        l_nextNumber := l_serialGenRec."CurValue"+1;
      end if;
      update "serialgen" set "CurValue"=l_nextNumber
        where "TableName"=table_name;
    else
-- если же не нашлось записи в serialgen для таблицы table_name,
-- то добавляем ее
      close serialgen_cur;
      l_nextNumber := 1;
      insert into "serialgen" ("DummyID", "CurValue", "TableName")
        select nvl(max("DummyID"), 0)+1, l_nextNumber, table_name from "serialgen";
    end if;
-- фиксируем изменения
    commit;
-- возвращаем результат
    return l_nextNumber;

  exception
    when others then
      if serialgen_cur%isopen then
        close serialgen_cur;
      end if;
      RaiseError('GetSerialNumberNoCache');
  end;

  -- VIT 09.02.2005 like the GetSerialNumberNoCache function. additionally it allows you to request more
  -- than one serial number at a time
  function GetSerialNumberNoCacheEx(
    table_name varchar2,
    request_count number
  ) return number
  is
    pragma autonomous_transaction; -- без этого объявления будет вылетать
                                   -- ORA-14551: cannot perform a DML operation inside a query
    cursor serialgen_cur is
      select * from "serialgen"
      where "TableName"=table_name
      for update;

    l_nextNumber   number;
    l_serialGenRec serialgen_cur%rowtype;
  begin
    open serialgen_cur;
    fetch serialgen_cur into l_serialGenRec;
    if serialgen_cur%found then
-- если запись выбрана, то вычисляем текущее и следующее значения
      close serialgen_cur;
      if l_serialGenRec."CurValue" is null then
        l_nextNumber := 1;
      else
        l_nextNumber := l_serialGenRec."CurValue"+1;
      end if;
      update "serialgen" set "CurValue"=(l_nextNumber+request_count-1)
        where "TableName"=table_name;
    else
-- если же не нашлось записи в serialgen для таблицы table_name,
-- то добавляем ее
      close serialgen_cur;
      l_nextNumber := 1;
      insert into "serialgen" ("DummyID", "CurValue", "TableName")
        select nvl(max("DummyID"), 0)+1, (l_nextNumber+request_count-1), table_name from "serialgen";
    end if;
-- фиксируем изменения
    commit;
-- возвращаем результат
    return l_nextNumber;

  exception
    when others then
      if serialgen_cur%isopen then
        close serialgen_cur;
      end if;
      RaiseError('GetSerialNumberNoCacheEx');
  end;

  -- VIT 11.09.2007
  -- Возвращаемые значения:
  -- 0 - есть чужая блокировка
  -- 1 - установлена своя блокировка
  -- 2 - ранее уже была установлена своя блокировка
  function RequestLockEx
         (ObjectName in varchar2,
          ObjectID in varchar2,
          UserName in varchar2)
         return number is
    pragma autonomous_transaction;
    n number;
  begin
    merge into "SБлокировка" t1
      using (select ObjectName "Класс", ObjectID "Объект", UserName "Владелец" from dual) t2
      on (t1."Класс"=t2."Класс" and t2."Объект"=t1."Объект" and t2."Владелец"=t1."Владелец")
      when not matched then
        insert values (AIS_UTILS.GetSerialNumber('SБлокировка'),
          t2."Класс", t2."Объект", t2."Владелец", sysdate);
    n := 2-sql%rowcount;
    commit;
    return n;
  exception
    when others then
      declare
        errmsg varchar2(512) := sqlerrm;
      begin
        if instr(upper(errmsg), 'UQ_КЛАССОБЪЕКТ') > 0 then
          return 0;
        end if;
        RaiseError('AIS_UTILS.RequestLockEx', ERR_WHENCE || errmsg);
      end;
  end RequestLockEx;

  -- VIT 11.09.2007
  function ReleaseLockEx
         (ObjectName in varchar2,
          ObjectID in varchar2,
          UserName in varchar2)
         return number is
    pragma autonomous_transaction;
    n number;
  begin
    delete from "SБлокировка"
     where "Класс"=ObjectName
       and "Объект"=ObjectID
       and "Владелец"=UserName;
    n := sql%rowcount;
    commit;
    return n;
  exception
    when others then
      RaiseError('AIS_UTILS.ReleaseLockEx');
  end ReleaseLockEx;

---------------------------------------------------------
procedure RequestLock
         (ObjectName varchar2,
          ObjectID varchar2,
          UserName varchar2,
          LockID in out number) is

  pragma autonomous_transaction;

  cursor cur_Lock is
      select "Ид" from "SБлокировка" WHERE "Класс"=ObjectName AND "Объект"=ObjectID AND "Владелец"=UserName;
  rec_Lock cur_Lock%rowtype;

  ID NUMBER;

  begin
    ID:=NULL;
    LockID:=NULL;
    OPEN cur_Lock;
    FETCH cur_Lock into rec_Lock;
    IF cur_Lock%notfound then
       ID := AIS_UTILS.GetSerialNumber('SБлокировка');
       INSERT INTO "SБлокировка" ("Ид","Класс","Объект","Владелец","ДатаУстановки")
       VALUES(ID,ObjectName,ObjectID,UserName,SYSDATE);
       LockID:=ID;
       COMMIT;
      IF cur_Lock%isopen THEN
       CLOSE cur_Lock;
      END IF;
    END IF;
    exception
    when others then
      IF cur_Lock%isopen THEN
       CLOSE cur_Lock;
      END IF;
      RaiseError('RequestLock');
  end;
-----------------------------------------------------------

procedure ReleaseLock(LockID in out number) is

 pragma autonomous_transaction;

   begin
     DELETE "SБлокировка" WHERE "Ид"=LockID;
     COMMIT;
     exception
    when others then
      RaiseError('ReleaseLock');
   end;

----------------------TaskMaker----------------------------------
    /*
Процедура создания задания
НАЧАО=sysdate - 1 час (вдруг на сервере время меньше)
АКТИВНЫЙ=Нет
СЕРВЕР - по IDProcess из таблицы SЗАДАНИЕ.СЕРВЕР
на каком текущий процесс там и создаем
ПОЛЬЗОВАТЕЛЬ - тоже так же берем из текущего задания
IDProcess - Ид SЗАДАНИЕ
TaskName - КРАТНАИМЕН SКЛПРОЦЕСС
TaskParams - параметры задания
*/
procedure TaskMaker(IDProcess number, TaskName varchar2, TaskParams varchar2)
is
  /*
  l_userId number;
  l_server varchar2(120);
  --
  l_pos number;
  l_str varchar2(50);
  l_procName varchar2(30);
  l_procId number;
  l_found boolean;
  --
  cursor cur_proc(l_procName in varchar2) is
    select * from "SКЛПРОЦЕСС" where "КРАТНАИМЕН" = l_procName;
  rec_proc cur_proc%rowtype;
  --
*/
  nNewProcess number;
begin
  nNewProcess := AIS_NOTIFYER.AddNewTask(IDProcess, TaskName, TaskParams);
  /*
  --
  select "ПОЛЬЗОВАТЕЛЬ", "СЕРВЕР"
      into l_userId, l_server
      from "SЗАДАНИЕ"
      where "Ид" = IDProcess;
  --
  l_pos := INSTR(TaskName, '\');
  l_procName := SUBSTR(TaskName, l_pos + 1);
  l_str := SUBSTR(TaskName, 1, l_pos - 1);
  --
  l_found := false;
  dbms_output.put_line(l_str);
  open cur_proc(l_str);
  loop
    fetch cur_proc into rec_proc;
    exit when cur_proc%notfound;
    l_procId := rec_proc."Ид";
    -- проверим совпадает ли имя комп и то что передали
    if rec_proc."КОМПОНЕНТА" = l_procName then
      l_found := true;
      exit;
    end if;

  end loop;
  --
  if cur_proc%rowcount = 0 then l_found := false; end if;
  if cur_proc%rowcount = 1 then l_found := true; end if;
  --
  if cur_proc%isopen then close cur_proc; end if;
  --
  if not l_found then
    RaiseError('TaskMaker', 'Задание "' || TaskName || '" не найдено в классификаторе заданий');
  end if;
  --
  insert into "SЗАДАНИЕ"
      ("Ид", "ОПИСАТЕЛЬ", "ПОЛЬЗОВАТЕЛЬ", "НАЧАЛО", "РОДИТЕЛЬ",
       "АКТИВНЫЙ", "ПАРАМЕТРЫ", "СЕРВЕР", "ПРИОРИТЕТ")
      values (AIS_UTILS.GetSerialNumber('SЗАДАНИЕ'), l_procId, l_userId, sysdate, null,
              'Нет', TaskParams, l_server, 1);
  --
  commit;
  --
*/
exception when others then
  --rollback;
  --if cur_proc%isopen then close cur_proc; end if;
  RaiseError('TaskMaker');
end;

---------------------------------------------------------------------------------------
-- VIT 29.07.2004
procedure Clean is
begin

  -- 1. Удалить SЗАДАНИЕИСТ, у которых ОКОНЧАНИЕ < trunc(sysdate)-5.
  --    Удалить свызанные с SЗАДАНИЕИСТ записи из SNSLOG.
  --    Удалить свызанные с SЗАДАНИЕИСТ записи из SNSLOG2.

  for rec_hist in (
    select *
      from "SЗАДАНИЕИСТ"
     where "ОКОНЧАНИЕ" < trunc(sysdate) - 5)
  loop
    --delete from "SNSLOG" where "ЗАДАНИЕИСТ" = rec_hist."Ид";
    delete from "SNSLOG2" where task_id = rec_hist."ИДЗАДАНИЯ";
    delete from "SЗАДАНИЕИСТ" where "Ид" = rec_hist."Ид";
    commit;
  end loop;

  -- 2. Удалить SПротФайла, где SФайл.ДатаОконч < trunc(sysdate)-5.
  --    Почистить SФайл.Файл, где SФайл.ДатаОконч < trunc(sysdate)-5.

  delete
   from "SПротФайла"
   where "Файл" in
    (select "Ид"
     from "SФайл"
     where "ДатаОконч" < trunc(sysdate)-5);

  commit;

  update "SФайл"
   set "Файл"=null
   where "ДатаОконч" < trunc(sysdate)-5;

  commit;

  -- 3. Удалить SИсхФайл, где SСообщение.ДатаОтправки < trunc(sysdate)-10.

  delete
   from "SИсхФайл"
   where "Сообщение" in
    (select "Ид"
      from "SСообщение"
      where "ДатаОтправки" < trunc(sysdate)-10)
      and "Файл" is not null;

  commit;

exception
  when others then
    rollback;
    -- тут бы надо чего-нить в лог писать...
    raise;
end;

-- VIT 29.07.2004
procedure CreateCleanJob(
  Hour in number
)
is
  pragma autonomous_transaction;
  l_job number;
begin
  dbms_job.submit(job       => l_job,
                  what      => 'AIS_UTILS.Clean;',
                  next_date => trunc(sysdate)+1+Hour/24,
                  interval  => 'trunc(sysdate)+1+'||to_char(Hour)||'/24');
  commit;
end;

-- VIT 28.03.2005
function KillQuery(QueryID in number) return number
is
  pragma autonomous_transaction;
  type cur_terminate is ref cursor;
  l_cur_terminate cur_terminate;
  l_job integer;
  l_sid_serial varchar2(100);
begin

/*  execute immediate
    'grant select any dictionary to '||sys_context('userenv', 'current_user');
  execute immediate
    'grant alter system to '||sys_context('userenv', 'current_user');
*/
  -- VIT 25.05.2009
  if USE_SCHEDULER then
    open l_cur_terminate for
      'select t1.JOBID, t3.SID||'',''||t3.SERIAL# SID_SERIAL '||
         'from WEBQDATA t1, user_scheduler_running_jobs t2, v$session t3 '||
        'where t1.ID='||QueryID||
         ' and t2.job_name=''AIS_JOB_''||t1.JOBID'||
         ' and t3.SID=t2.session_id';
  else
    open l_cur_terminate for
      'select t4.JOB, t4.SID_SERIAL '||
        'from WEBQDATA t1, '||
          '(select     /*+ ORDERED */ v.id2 JOB, v.SID||'',''||s.SERIAL# SID_SERIAL '||
             'from v$lock v, v$session s '||
            'where v.type = ''JQ'' '||
              'and v.SID=s.SID ) t4 '||
       'where t1.ID='||QueryID||' '||
         'and t4.JOB=t1.JOBID';
  end if;
  fetch l_cur_terminate into l_job, l_sid_serial;

  if l_cur_terminate%found then
    update WEBQDATA
       set JOBID=null
     where ID=QueryID;
    commit;
    -- VIT 25.05.2009
    if USE_SCHEDULER then
      execute immediate
        'alter system kill session '''||
          l_sid_serial||'''';
    else
      DBMS_JOB.remove(l_job);
      commit;
      execute immediate
        'alter system kill session '''||
          l_sid_serial||'''';
    end if;
  end if;

  close l_cur_terminate;

  return 1;

exception
  when others then
    if l_cur_terminate%isopen then
      close l_cur_terminate;
    end if;
    RaiseError('KillQuery');
end;

-- VIT 28.03.2005
function AsyncQuery(QueryID in number) return number
is
  pragma autonomous_transaction;
  l_query_id number;
  l_restype varchar2(40); -- VIT 22.10.2007
  l_job number;
  -- VIT 25.05.2009
  l_job_sql varchar2(32000);
  l_job_name varchar2(255);
begin

  select ID, RESTYPE into l_query_id, l_restype
    from WEBQDATA
   where ID=QueryID
     and JOBID is null; -- вот тут сломается если запроса не найдено
                        -- либо параллельно выполняется другой запрос с тем же ID
  -- VIT 25.05.2009
  if USE_SCHEDULER then
    l_job := GetSerialNumber('AIS_JOB');
    if not l_restype is null
    then
      l_job_sql :=
        'begin '||
          'AsyncQueryProcess3(''WEBQDATA'','||QueryID||','||l_job||'); '||
        'exception '||
          'when others then '||
            'declare '||
              'err_msg varchar2(512) := AIS_UTILS.ERR_WHENCE || sqlerrm; '||
            'begin '||
              'update WEBQDATA set ENDDATE=sysdate, ERR=replace(err_msg, '''', '''''''') where ID='||QueryID||';'||
            'end; '||
        'end;';
    else
      l_job_sql :=
        'begin '||
          'AsyncQueryProcess2New(''WEBQDATA'','||QueryID||','||l_job||'); '||
        'exception '||
          'when others then '||
            'declare '||
              'err_msg varchar2(512) := AIS_UTILS.ERR_WHENCE || sqlerrm; '||
            'begin '||
              'update WEBQDATA set ENDDATE=sysdate, ERR=replace(err_msg, '''', '''''''') where ID='||QueryID||';'||
            'end; '||
        'end;';
    end if;
    update WEBQDATA
       set BEGDATE=sysdate, ENDDATE=null, ERR=null, JOBID=l_job, RESDATA=EMPTY_BLOB()
     where ID=QueryID;
    commit;
    l_job_name := 'AIS_JOB_'||l_job;
    -- required privilegies: grant create any job to ais
    dbms_scheduler.create_job(
      job_name => l_job_name,
      job_type => 'PLSQL_BLOCK',
      job_action => l_job_sql,
      comments => 'AIS ASYNCQUERY Job (query_id=>'||QueryID||')',
--      repeat_interval => 'FREQ=DAILY',
      enabled => false, -- VIT 27.08.2009
      job_class => 'AIS_JOB_CLASS', -- VIT 31.03.2009
      auto_drop => true);
    -- Необходимо, чтобы задание не прилипало к текущему инстансу
    dbms_scheduler.set_attribute (
      name      => l_job_name,
      attribute => 'instance_stickiness',
      value     => false);
    dbms_scheduler.set_attribute (
      name      => l_job_name,
      attribute => 'restartable',
      value     => true);
    -- VIT 27.08.2009
    -- may be required privilegie: grant execute on sys.ais_job_class to ais
    dbms_scheduler.enable(l_job_name);
  else
    -- VIT 22.10.2007
    if not l_restype is null
    then
      l_job_sql :=
        'begin '||
          'AsyncQueryProcess3(''WEBQDATA'','||QueryID||',job); '||
        'exception '||
          'when others then '||
            'declare '||
              'err_msg varchar2(512) := AIS_UTILS.ERR_WHENCE || sqlerrm; '||
            'begin '||
              'next_date := sysdate+1; '||
              'update WEBQDATA set ENDDATE=sysdate, ERR=replace(err_msg, '''', '''''''') where ID='||QueryID||';'||
              'broken := true; '||
            'end; '||
        'end;';
    else
      l_job_sql :=
        'begin '||
          -- 'AIS_Q.ASYNCQUERYPROCESS('||
          -- VIT 31.03.2007, 21.01.2008
          'AsyncQueryProcess2New(''WEBQDATA'','||QueryID||',job); '||
        'exception '||
          'when others then '||
            -- VIT 21.11.2006
            'declare '||
              'err_msg varchar2(512) := AIS_UTILS.ERR_WHENCE || sqlerrm; '|| -- VIT 30.11.2006
            'begin '||
              'next_date := sysdate+1; '||
              'update WEBQDATA set ENDDATE=sysdate, ERR=replace(err_msg, '''', '''''''') where ID='||QueryID||';'||
              'broken := true; '||
            'end; '||
        'end;';
    end if;
    dbms_job.submit(l_job, l_job_sql, sysdate);
    update WEBQDATA
       set BEGDATE=sysdate, ENDDATE=null, ERR=null, JOBID=l_job, RESDATA=EMPTY_BLOB()
     where ID=QueryID;
    commit;
  end if;

  return l_job;

exception
  when others then
    rollback;
    RaiseError('AsyncQuery');
end;

-- VIT 28.03.2005
-- VIT 08.12.2006 ограничение на макс выборку - из WEBUSERS.CSVCNT если заполнено, иначе - 5000
procedure AsyncQueryProcess(QueryID in number, JobID in number)
is
  cursor cur_WEBQDATA is
    select t1.SQLSTR, t1.RESDATA, t2.CSVCNT
      from WEBQDATA t1 left outer join WEBUSERS t2
        on t1.LOGIN = t2.LOGIN
     where t1.ID=QueryID
     for update;
  l_sql         WEBQDATA.SQLSTR%type;
  l_blob        BLOB;
  l_csvcnt      WEBUSERS.CSVCNT%type;
  l_data        varchar2(32000);
  l_columnValue varchar2(4000);
  l_value       varchar2(4000);
  l_status      integer;
  l_colCnt      number default 0;
  l_cnt         number default 0;
  l_max_cnt     number default 5000;
  l_line        long;
  l_descTbl     dbms_sql.desc_tab;
  l_theCursor   integer default null;
  l_tmpCursor   integer;
  l_sql_start   varchar2(7);
begin

  execute immediate
    'alter session set nls_numeric_characters=''. ''';
  execute immediate
    'alter session set nls_date_format=''DD.MM.YYYY HH24:MI:SS''';

  open cur_WEBQDATA;

  fetch cur_WEBQDATA into l_sql, l_blob, l_csvcnt;

  if not l_csvcnt is null
  then
    l_max_cnt := l_csvcnt;
  end if;

  if l_sql is null
  then
    AIS_UTILS.RaiseError('AsyncQueryProcess', 'Отсутствует SQL-выражение');
  end if;

  l_sql_start := lower(substr(ltrim(l_sql),1,7));
  if (l_sql_start <> 'select ') and
     (l_sql_start <> 'select'||CHR(9)) and
     (l_sql_start <> 'select'||CHR(10)) and
     (l_sql_start <> 'select'||CHR(13))
  then
    AIS_UTILS.RaiseError('AsyncQueryProcess', 'Неверное SQL-выражение');
  end if;

  l_theCursor := dbms_sql.open_cursor;

  dbms_sql.parse(l_theCursor,
    l_sql, dbms_sql.native);

  dbms_sql.describe_columns(l_theCursor, l_colCnt, l_descTbl);

  l_line := '<HTML xmlns:o="urn:schemas-microsoft-com:office:office" '||
            'xmlns:x="urn:schemas-microsoft-com:office:excel" '||
            'xmlns="http://www.w3.org/TR/REC-html40">'||CHR(13)||'<HEAD>'||CHR(13)||
            '<META HTTP-EQUIV="Content-Type" Content="text/html; charset=UTF-8">'||CHR(13)||'</HEAD>'||CHR(13)||
            '<BODY>'||CHR(13)||'<TABLE BORDER="1">'||CHR(13);
  -- dbms_output.put_line(l_line);
  l_data := convert(l_line, 'UTF8');

  l_line := '<TR>';
  for i in 1..l_colCnt
  loop
    l_line := l_line||'<TH>'||l_descTbl(i).col_name||'</TH>';
  end loop;
  l_line := l_line||'</TR>'||CHR(13);
  -- dbms_output.put_line(substr(l_line, 1, 255));
  l_data := l_data||convert(l_line, 'UTF8');

  l_cnt := 1;

  for i in 1..l_colCnt
  loop
    dbms_sql.define_column(l_theCursor, i, l_ColumnValue, 4000);
  end loop;

  l_status := dbms_sql.execute(l_theCursor);

  while (dbms_sql.fetch_rows(l_theCursor)>0)
  loop
    if l_cnt >= l_max_cnt then
      exit;
    end if;
    l_line := '<TR>';
    for i in 1..l_colCnt
    loop
      dbms_sql.column_value(l_theCursor, i, l_columnValue);
      l_value := trim(l_columnValue);
      if not l_value is null
      then
        l_line := l_line||'<TD x:str>'||
          replace(replace(replace(replace(l_value,
            CHR(38),CHR(38)||'amp;'),'<',CHR(38)||'lt;'),'>',CHR(38)||'gt;'),'"',CHR(38)||'quot;')||
          '</TD>';
      else
        l_line := l_line||'<TD>'||CHR(38)||'nbsp;</TD>';
      end if;
    end loop;
    l_line := l_line||'</TR>'||CHR(13);
    -- dbms_output.put_line(substr(l_line, 1, 255));
    l_line := convert(l_line, 'UTF8');
    if length(l_data)+length(l_line) > 32000
    then
      dbms_lob.writeappend(l_blob, length(l_data), utl_raw.cast_to_raw(l_data));
      l_data := l_line;
    else
      l_data := l_data||l_line;
    end if;
    l_cnt := l_cnt+1;
  end loop;

  l_line := '</TABLE>'||CHR(13)||'</BODY>'||CHR(13)||'</HTML>'||CHR(13);
  -- dbms_output.put_line(l_line);
  l_line := convert(l_line, 'UTF8');
  if length(l_data)+length(to_nchar(l_line)) > 32000
  then
    dbms_lob.writeappend(l_blob, length(l_data), utl_raw.cast_to_raw(l_data));
    dbms_lob.writeappend(l_blob, length(l_line), utl_raw.cast_to_raw(l_line));
  else
    l_data := l_data||l_line;
    dbms_lob.writeappend(l_blob, length(l_data), utl_raw.cast_to_raw(l_data));
  end if;

  close cur_WEBQDATA;

  l_tmpCursor := l_theCursor;
  l_theCursor := null;
  dbms_sql.close_cursor(l_tmpCursor);

  update WEBQDATA
     set ENDDATE=sysdate, JOBID=null
   where ID=QueryID;
  commit;

exception
  when others then
    declare
      err_msg varchar2(512) := ERR_WHENCE || sqlerrm; -- VIT 30.11.2006
    begin
      if cur_WEBQDATA%isopen then
        close cur_WEBQDATA;
      end if;
      if not l_theCursor is null then
        dbms_sql.close_cursor(l_theCursor);
      end if;
      update WEBQDATA
         set ENDDATE=sysdate, ERR=err_msg, JOBID=null
       where ID=QueryID;
      commit;
      raise;
    end;
end;

-- VIT 14.10.2005
function KillQueryNew(QueryID in number) return number
is
  pragma autonomous_transaction;
  type cur_terminate is ref cursor;
  l_cur_terminate cur_terminate;
  l_job integer;
  l_sid_serial varchar2(100);
begin

/*  execute immediate
    'grant select any dictionary to '||sys_context('userenv', 'current_user');
  execute immediate
    'grant alter system to '||sys_context('userenv', 'current_user');
*/
  -- VIT 25.05.2009
  if USE_SCHEDULER then
    open l_cur_terminate for
      'select t1.JOBID, t3.SID||'',''||t3.SERIAL# SID_SERIAL '||
         'from WEBQASYNCDATA t1, user_scheduler_running_jobs t2, v$session t3 '||
        'where t1.ID='||QueryID||
         ' and t2.job_name=''AIS_JOB_''||t1.JOBID'||
         ' and t3.SID=t2.session_id';
  else
    open l_cur_terminate for
      'select t4.JOB, t4.SID_SERIAL '||
        'from WEBQASYNCDATA t1, '||
          '(select     /*+ ORDERED */ v.id2 JOB, v.SID||'',''||s.SERIAL# SID_SERIAL '||
             'from v$lock v, v$session s '||
            'where v.type = ''JQ'' '||
              'and v.SID=s.SID ) t4 '||
       'where t1.ID='||QueryID||' '||
         'and t4.JOB=t1.JOBID';
  end if;
  fetch l_cur_terminate into l_job, l_sid_serial;

  if l_cur_terminate%found then
    update WEBQASYNCDATA
       set JOBID=null
     where ID=QueryID;
    commit;
    -- VIT 25.05.2009
    if USE_SCHEDULER then
      execute immediate
        'alter system kill session '''||
          l_sid_serial||'''';
    else
      DBMS_JOB.remove(l_job);
      commit;
      execute immediate
        'alter system kill session '''||
          l_sid_serial||'''';
    end if;
  end if;

  close l_cur_terminate;

  return 1;

exception
  when others then
    if l_cur_terminate%isopen then
      close l_cur_terminate;
    end if;
    RaiseError('KillQueryNew');
end;

-- VIT 14.10.2005
function AsyncQueryNew(QueryID in number) return number
is
  pragma autonomous_transaction;
  l_query_id number;
  l_restype varchar2(40); -- VIT 22.10.2007
  l_job number;
  -- VIT 25.05.2009
  l_job_sql varchar2(32000);
  l_job_name varchar2(255);
begin

  select ID, RESTYPE into l_query_id, l_restype
    from WEBQASYNCDATA
   where ID=QueryID
     and JOBID is null; -- вот тут сломается если запроса не найдено
                        -- либо параллельно выполняется другой запрос с тем же ID

  -- VIT 25.05.2009
  if USE_SCHEDULER then
    l_job := GetSerialNumber('AIS_JOB');
    if not l_restype is null
    then
      l_job_sql :=
        'begin '||
          'AsyncQueryProcess3(''WEBQASYNCDATA'','||QueryID||','||l_job||'); '||
        'exception '||
          'when others then '||
            'declare '||
              'err_msg varchar2(512) := AIS_UTILS.ERR_WHENCE || sqlerrm; '||
            'begin '||
              'update WEBQASYNCDATA set ENDDATE=sysdate, ERR=replace(err_msg, '''', '''''''') where ID='||QueryID||';'||
              'AIS_UTILS.AsyncQueryCompleteNew('||QueryID||');'||
            'end; '||
        'end;';
    else
      l_job_sql :=
        'begin '||
          'AsyncQueryProcess2New(''WEBQASYNCDATA'','||QueryID||','||l_job||'); '||
        'exception '||
          'when others then '||
            'declare '||
              'err_msg varchar2(512) := AIS_UTILS.ERR_WHENCE || sqlerrm; '||
            'begin '||
              'update WEBQASYNCDATA set ENDDATE=sysdate, ERR=replace(err_msg, '''', '''''''') where ID='||QueryID||';'||
              'AIS_UTILS.AsyncQueryCompleteNew('||QueryID||');'||
            'end; '||
        'end;';
    end if;
    update WEBQASYNCDATA
       set BEGDATE=sysdate, ENDDATE=null, ERR=null, JOBID=l_job, RESDATA=EMPTY_BLOB()
     where ID=QueryID;
    commit;
    l_job_name := 'AIS_JOB_'||l_job;
    -- required privilegies: grant create any job to ais
    dbms_scheduler.create_job(
      job_name => l_job_name,
      job_type => 'PLSQL_BLOCK',
      job_action => l_job_sql,
      comments => 'AIS ASYNCQUERYNEW Job (query_id=>'||QueryID||')',
--      repeat_interval => 'FREQ=DAILY',
      enabled => false, -- VIT 27.08.2009
      job_class => 'AIS_JOB_CLASS', -- VIT 31.03.2009
      auto_drop => true);
    -- Необходимо, чтобы задание не прилипало к текущему инстансу
    dbms_scheduler.set_attribute (
      name      => l_job_name,
      attribute => 'instance_stickiness',
      value     => false);
    dbms_scheduler.set_attribute (
      name      => l_job_name,
      attribute => 'restartable',
      value     => true);
    -- VIT 27.08.2009
    -- may be required privilegie: grant execute on sys.ais_job_class to ais
    dbms_scheduler.enable(l_job_name);
  else
    -- VIT 22.10.2007
    if not l_restype is null
    then
      l_job_sql :=
        'begin '||
          'AsyncQueryProcess3(''WEBQASYNCDATA'','||QueryID||',job); '||
        'exception '||
          'when others then '||
            'declare '||
              'err_msg varchar2(512) := AIS_UTILS.ERR_WHENCE || sqlerrm; '||
            'begin '||
              'next_date := sysdate+1; '||
              'update WEBQASYNCDATA set ENDDATE=sysdate, ERR=replace(err_msg, '''', '''''''') where ID='||QueryID||';'||
              'AIS_UTILS.AsyncQueryCompleteNew('||QueryID||');'|| -- VIT 14.11.2007
              'broken := true; '||
            'end; '||
        'end;';
    else
      -- dbms_job.submit(l_job,
      l_job_sql :=
        'begin '||
          -- 'AIS_Q.AsyncQueryProcessNew('||
          -- VIT 31.03.2007, 21.01.2008
          'AsyncQueryProcess2New(''WEBQASYNCDATA'','||QueryID||',job); '||
        'exception '||
          'when others then '||
            -- VIT 21.11.2006
            'declare '||
              'err_msg varchar2(512) := AIS_UTILS.ERR_WHENCE || sqlerrm; '|| -- VIT 30.11.2006
            'begin '||
              'next_date := sysdate+1; '||
              'update WEBQASYNCDATA set ENDDATE=sysdate, ERR=replace(err_msg, '''', '''''''') where ID='||QueryID||';'||
              'AIS_UTILS.AsyncQueryCompleteNew('||QueryID||');'|| -- VIT 14.11.2007
              'broken := true; '||
            'end; '||
        'end;';
    end if;
    dbms_job.submit(l_job, l_job_sql, sysdate);
    update WEBQASYNCDATA
       set BEGDATE=sysdate, ENDDATE=null, ERR=null, JOBID=l_job, RESDATA=EMPTY_BLOB()
     where ID=QueryID;
    commit;
  end if;

  return l_job;

exception
  when others then
    rollback;
    RaiseError('AsyncQueryNew');
end;

-- VIT 14.10.2005
-- VIT 08.12.2006 ограничение на макс выборку - из WEBUSERS.CSVCNT если заполнено, иначе - 5000
procedure AsyncQueryProcessNew(QueryID in number, JobID in number)
is
  cursor cur_WEBQASYNCDATA is
    select t1.SQLSTR, t1.RESDATA, t2.CSVCNT
      from WEBQASYNCDATA t1 left outer join WEBUSERS t2
        on t1.LOGIN = t2.LOGIN
     where t1.ID=QueryID
     for update;
  l_sql         WEBQASYNCDATA.SQLSTR%type;
  l_blob        BLOB;
  l_csvcnt      WEBUSERS.CSVCNT%type;
  l_data        varchar2(32000);
  l_columnValue varchar2(4000);
  l_value       varchar2(4000);
  l_status      integer;
  l_colCnt      number default 0;
  l_cnt         number default 0;
  l_max_cnt     number default 5000;
  l_line        long;
  l_descTbl     dbms_sql.desc_tab;
  l_theCursor   integer default null;
  l_tmpCursor   integer;
  l_sql_start   varchar2(7);
begin

  execute immediate
    'alter session set nls_numeric_characters=''. ''';
  execute immediate
    'alter session set nls_date_format=''DD.MM.YYYY HH24:MI:SS''';

  open cur_WEBQASYNCDATA;

  fetch cur_WEBQASYNCDATA into l_sql, l_blob, l_csvcnt;

  if not l_csvcnt is null
  then
    l_max_cnt := l_csvcnt;
  end if;

  if l_sql is null
  then
    AIS_UTILS.RaiseError('AsyncQueryProcessNew', 'Отсутствует SQL-выражение');
  end if;

  l_sql_start := lower(substr(ltrim(l_sql),1,7));
  if (l_sql_start <> 'select ') and
     (l_sql_start <> 'select'||CHR(9)) and
     (l_sql_start <> 'select'||CHR(10)) and
     (l_sql_start <> 'select'||CHR(13))
  then
    AIS_UTILS.RaiseError('AsyncQueryProcessNew', 'Неверное SQL-выражение');
  end if;

  l_theCursor := dbms_sql.open_cursor;

  dbms_sql.parse(l_theCursor,
    l_sql, dbms_sql.native);

  dbms_sql.describe_columns(l_theCursor, l_colCnt, l_descTbl);

  l_line := '<HTML xmlns:o="urn:schemas-microsoft-com:office:office" '||
            'xmlns:x="urn:schemas-microsoft-com:office:excel" '||
            'xmlns="http://www.w3.org/TR/REC-html40">'||CHR(13)||'<HEAD>'||CHR(13)||
            '<META HTTP-EQUIV="Content-Type" Content="text/html; charset=UTF-8">'||CHR(13)||'</HEAD>'||CHR(13)||
            '<BODY>'||CHR(13)||'<TABLE BORDER="1">'||CHR(13);
  -- dbms_output.put_line(l_line);
  l_data := convert(l_line, 'UTF8');

  l_line := '<TR>';
  for i in 1..l_colCnt
  loop
    l_line := l_line||'<TH>'||l_descTbl(i).col_name||'</TH>';
  end loop;
  l_line := l_line||'</TR>'||CHR(13);
  -- dbms_output.put_line(substr(l_line, 1, 255));
  l_data := l_data||convert(l_line, 'UTF8');

  l_cnt := 1;

  for i in 1..l_colCnt
  loop
    dbms_sql.define_column(l_theCursor, i, l_ColumnValue, 4000);
  end loop;

  l_status := dbms_sql.execute(l_theCursor);

  while (dbms_sql.fetch_rows(l_theCursor)>0)
  loop
    if l_cnt >= l_max_cnt then
      exit;
    end if;
    l_line := '<TR>';
    for i in 1..l_colCnt
    loop
      dbms_sql.column_value(l_theCursor, i, l_columnValue);
      l_value := trim(l_columnValue);
      if not l_value is null
      then
        l_line := l_line||'<TD x:str>'||
          replace(replace(replace(replace(l_value,
            CHR(38),CHR(38)||'amp;'),'<',CHR(38)||'lt;'),'>',CHR(38)||'gt;'),'"',CHR(38)||'quot;')||
          '</TD>';
      else
        l_line := l_line||'<TD>'||CHR(38)||'nbsp;</TD>';
      end if;
    end loop;
    l_line := l_line||'</TR>'||CHR(13);
    -- dbms_output.put_line(substr(l_line, 1, 255));
    l_line := convert(l_line, 'UTF8');
    if length(l_data)+length(l_line) > 32000
    then
      dbms_lob.writeappend(l_blob, length(l_data), utl_raw.cast_to_raw(l_data));
      l_data := l_line;
    else
      l_data := l_data||l_line;
    end if;
    l_cnt := l_cnt+1;
  end loop;

  l_line := '</TABLE>'||CHR(13)||'</BODY>'||CHR(13)||'</HTML>'||CHR(13);
  -- dbms_output.put_line(l_line);
  l_line := convert(l_line, 'UTF8');
  if length(l_data)+length(to_nchar(l_line)) > 32000
  then
    dbms_lob.writeappend(l_blob, length(l_data), utl_raw.cast_to_raw(l_data));
    dbms_lob.writeappend(l_blob, length(l_line), utl_raw.cast_to_raw(l_line));
  else
    l_data := l_data||l_line;
    dbms_lob.writeappend(l_blob, length(l_data), utl_raw.cast_to_raw(l_data));
  end if;

  close cur_WEBQASYNCDATA;

  l_tmpCursor := l_theCursor;
  l_theCursor := null;
  dbms_sql.close_cursor(l_tmpCursor);

  update WEBQASYNCDATA
     set ENDDATE=sysdate, JOBID=null
   where ID=QueryID;
  commit;

exception
  when others then
    declare
      err_msg varchar2(512) := ERR_WHENCE || sqlerrm; -- VIT 30.11.2006
    begin
      if cur_WEBQASYNCDATA%isopen then
        close cur_WEBQASYNCDATA;
      end if;
      if not l_theCursor is null then
        dbms_sql.close_cursor(l_theCursor);
      end if;
      update WEBQASYNCDATA
         set ENDDATE=sysdate, ERR=err_msg, JOBID=null
       where ID=QueryID;
      commit;
      raise;
    end;
end;

-- VIT 14.11.2007
procedure AsyncQueryCompleteNew(QueryID in number) is
cursor ThisQ is
select ENDDATE, ERR, TOPQ from webqasyncdata
where TOPQ is not null
and id = QueryID;
fdate date;
begin
  commit;
  for Q in ThisQ loop
      fdate := sysdate;
         for SQ in (
            select ID from webqasyncdata
            where TOPQ = Q.TOPQ
            and ENDDATE is null) loop
            fdate := null;
         end loop;
          update webqasyncdata set ERR = Q.ERR, ENDDATE = fdate
          where ID = Q.TOPQ;
  end loop;
end;


---------------------------------------------------------------------------------------

end AIS_UTILS;
/
