﻿ALTER TABLE "NDS2_MC"."ASKZIPФайл" ADD ("КНД" VARCHAR2(2048));
ALTER TABLE "NDS2_MC"."ASKZIPФайл" ADD ("КодТипаДок" VARCHAR2(2));
ALTER TABLE "NDS2_MC"."ASKZIPФайл" ADD ("ОперИд" VARCHAR2(3));
ALTER TABLE "NDS2_MC"."ASKZIPФайл" ADD ("ОперТип" VARCHAR2(50));
ALTER TABLE "NDS2_MC"."ASKZIPФайл" ADD ("ОтпрИд" VARCHAR2(46));
ALTER TABLE "NDS2_MC"."ASKZIPФайл" ADD ("ОтпрТип" VARCHAR2(50));
ALTER TABLE "NDS2_MC"."ASKZIPФайл" ADD ("ПолучИд" VARCHAR2(46));
ALTER TABLE "NDS2_MC"."ASKZIPФайл" ADD ("ПолучТип" VARCHAR2(50));

create table "NDS2_MC"."ASKПериодКС"
(
  "ОтчетныйПериод" DATE,
  "ИНН"            VARCHAR2(12),
  "МаксСумИсчисл"  NUMBER,
  "ПрКС1.1"        VARCHAR2(1),
  "ПрКС1.2"        VARCHAR2(1),
  "ПрКС1.3"        VARCHAR2(1)           
);

ALTER TABLE "NDS2_MC"."ASKДекл" MODIFY ("НомКорр" NUMBER);

  CREATE TABLE "NDS2_MC"."DATABASECHANGELOG" 
   (	"ID" VARCHAR2(255) NOT NULL ENABLE,
	"AUTHOR" VARCHAR2(255) NOT NULL ENABLE,
	"FILENAME" VARCHAR2(255) NOT NULL ENABLE,
	"DATEEXECUTED" TIMESTAMP(6) NOT NULL ENABLE,
	"ORDEREXECUTED" NUMBER(10,0) NOT NULL ENABLE,
	"EXECTYPE" VARCHAR2(10) NOT NULL ENABLE,
	"MD5SUM" VARCHAR2(35),
	"DESCRIPTION" VARCHAR2(255),
	"COMMENTS" VARCHAR2(255),
	"TAG" VARCHAR2(255),
	"LIQUIBASE" VARCHAR2(20)
   );

     CREATE TABLE "NDS2_MC"."DATABASECHANGELOGLOCK" 
   (	"ID" NUMBER(10,0) NOT NULL ENABLE,
	"LOCKED" NUMBER(1,0) NOT NULL ENABLE,
	"LOCKGRANTED" TIMESTAMP(6),
	"LOCKEDBY" VARCHAR2(255)
   );

     CREATE TABLE "NDS2_MC"."TPГруппы" 
   (	"Ид" NUMBER NOT NULL ENABLE,
	"Описание" VARCHAR2(800),
	"ТипСервера" NUMBER
   );

     CREATE TABLE "NDS2_MC"."TPЛог" 
   (	"Дата" TIMESTAMP(6),
	"Продолж" NUMBER,
	"Сервер" NUMBER,
	"Источник" VARCHAR2(400),
	"ИдПотока" NUMBER,
	"ИдЭлемента" VARCHAR2(200),
	"Операция" VARCHAR2(200),
	"Ошибка" VARCHAR2(1),
	"Сообщение" VARCHAR2(4000),
	"ДатаВставки" DATE DEFAULT SYSDATE
   );

     CREATE TABLE "NDS2_MC"."TPПараметры" 
   (	"Ид" NUMBER NOT NULL ENABLE,
	"Описание" VARCHAR2(800),
	"Тип" NUMBER,
	"Принадлежность" NUMBER,
	"Имя" VARCHAR2(100),
	"Значение" VARCHAR2(1000)
   );

     CREATE TABLE "NDS2_MC"."TPПроцессы" 
   (	"Ид" NUMBER NOT NULL ENABLE,
	"Группа" NUMBER,
	"Описание" VARCHAR2(800),
	"ИмяКласса" VARCHAR2(100),
	"ИсточникДанных" VARCHAR2(100),
	"ЛимитПотоков" NUMBER,
	"Логирование" VARCHAR2(1) DEFAULT '0',
	"ТипОчереди" NUMBER,
	"ИнтервалОчереди" NUMBER,
	"Расписание" NUMBER,
	"Запрос" CLOB,
	"ДатаСтарта" DATE,
	"ДатаКонтроля" DATE,
	"ДатаОчереди" DATE,
	"Сервер" NUMBER,
	"Запрещен" VARCHAR2(1) DEFAULT '0'
   );

     CREATE TABLE "NDS2_MC"."TPРасписания" 
   (	"Ид" NUMBER NOT NULL ENABLE,
	"Имя" VARCHAR2(100)
   );

     CREATE TABLE "NDS2_MC"."TPРаспПериод" 
   (	"Ид" NUMBER NOT NULL ENABLE,
	"Расписание" NUMBER,
	"ВремяНачала" NUMBER,
	"ВремяОкончания" NUMBER,
	"ДеньНедели" NUMBER,
	"ЧислоМесяца" NUMBER
   );

     CREATE TABLE "NDS2_MC"."TPСервера" 
   (	"Ид" NUMBER NOT NULL ENABLE,
	"Тип" NUMBER,
	"Имя" VARCHAR2(100),
	"IP" VARCHAR2(100),
	"Доступен" VARCHAR2(1)
   );

     CREATE TABLE "NDS2_MC"."TR_QUEUE_7203" 
   (	"TBL_NAME" VARCHAR2(30),
	"TRANS_NO" NUMBER(*,0),
	"TS" TIMESTAMP(6) DEFAULT systimestamp,
	"OP_KIND" CHAR(1),
	"KEYS" VARCHAR2(2000),
	"REC_PTR" ROWID
   );

     CREATE OR REPLACE TRIGGER "NDS2_MC"."AIU$TRDT_17734"
  AFTER INSERT OR DELETE OR UPDATE ON "NDS2_MC"."ASKДекл"
  REFERENCING FOR EACH ROW
  begin
 if deleting or updating('Ид') then
 insert into NDS2_MC.TR_QUEUE_7203 (TBL_NAME,TRANS_NO,OP_KIND,REC_PTR,KEYS) values
 ('ASKДекл',0,'D', :old.ROWID,:old."Ид"||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127)||chr(127));
 end if;
 if not deleting then
 insert into NDS2_MC.TR_QUEUE_7203 (TBL_NAME,TRANS_NO,OP_KIND,REC_PTR) values
 ('ASKДекл',0,'M', :new.ROWID);
 end if;
 exception when others then
 null;
 end;
/

CREATE OR REPLACE PROCEDURE "NDS2_MC"."KSPERIOD" AS
  cur_date  VARCHAR(2);
  otch_date DATE;
  kol       NUMBER;
  BEGIN

    cur_date := to_char(SYSDATE, 'MM');
    CASE cur_date
      WHEN '01'
      THEN otch_date := to_date('01.01.' || to_char(SYSDATE, 'YYYY'), 'DD.MM.YYYY');
      WHEN '02'
      THEN otch_date := to_date('01.01.' || to_char(SYSDATE, 'YYYY'), 'DD.MM.YYYY');
      WHEN '03'
      THEN otch_date := to_date('01.01.' || to_char(SYSDATE, 'YYYY'), 'DD.MM.YYYY');
      WHEN '04'
      THEN otch_date := to_date('01.04.' || to_char(SYSDATE, 'YYYY'), 'DD.MM.YYYY');
      WHEN '05'
      THEN otch_date := to_date('01.04.' || to_char(SYSDATE, 'YYYY'), 'DD.MM.YYYY');
      WHEN '06'
      THEN otch_date := to_date('01.04.' || to_char(SYSDATE, 'YYYY'), 'DD.MM.YYYY');
      WHEN '07'
      THEN otch_date := to_date('01.07.' || to_char(SYSDATE, 'YYYY'), 'DD.MM.YYYY');
      WHEN '08'
      THEN otch_date := to_date('01.07.' || to_char(SYSDATE, 'YYYY'), 'DD.MM.YYYY');
      WHEN '09'
      THEN otch_date := to_date('01.07.' || to_char(SYSDATE, 'YYYY'), 'DD.MM.YYYY');
      WHEN '10'
      THEN otch_date := to_date('01.10.' || to_char(SYSDATE, 'YYYY'), 'DD.MM.YYYY');
      WHEN '11'
      THEN otch_date := to_date('01.10.' || to_char(SYSDATE, 'YYYY'), 'DD.MM.YYYY');
      WHEN '12'
      THEN otch_date := to_date('01.10.' || to_char(SYSDATE, 'YYYY'), 'DD.MM.YYYY');
    END CASE;

    LOOP

      SELECT count(*)
      INTO kol
      FROM "ASKПериодКС"
      WHERE "ОтчетныйПериод" = to_date(otch_date, 'DD.MM.YYYY')
            AND rownum < 2;

      IF kol = 0
      THEN
        INSERT INTO "ASKПериодКС" ("ИНН", "ОтчетныйПериод", "МаксСумИсчисл", "ПрКС1.1", "ПрКС1.2", "ПрКС1.3")
          SELECT
            "ИНН",
            otch_date,
            nvl(maxSumIsch, 0) "МаксСумИсчисл",
            CASE WHEN cntper = 2
              THEN 1
            ELSE 0 END "ПрКС1.1",
            pr12 "ПрКС1.2",
            CASE WHEN pr132 > 0
              THEN 1
            ELSE 0 END "ПрКС1.3"
          FROM (
            SELECT
              "ИНН",
              maxSumIsch,
              count(DISTINCT numper) cntper,
              min(KodOper) pr12,
              max(prznk3) pr132
            FROM (
              SELECT
                "ИНН",
                maxSumIsch,
                numper,
                KodOper,
                max(SumOpl)
                OVER (PARTITION BY "Ид") prznk3
              FROM
                (
                  SELECT DISTINCT
                    t1."Ид" "Ид",
                    t1."ИНННП" "ИНН",
                    max(t2."СумИсчисл")
                    OVER (PARTITION BY t1."ИНННП") maxSumIsch,
                    CASE WHEN t1."ДатаНачПер" BETWEEN otch_date - interval '3' month AND otch_date
                      THEN 1
                    WHEN t1."ДатаНачПер" BETWEEN otch_date - interval '6' month AND otch_date - interval '3' month
                      THEN 2
                    ELSE NULL END numper,
                    CASE WHEN t2."КодОпер" IN ('1011705', '1011707')
                      THEN 1
                    ELSE 0 END KodOper,
                    CASE WHEN t1."ДатаНачПер" BETWEEN otch_date - interval '6' month AND otch_date - interval '1' day
                      THEN t2."СумИсчислОпл"
                    ELSE 0 END SumOpl
                  FROM (
                         SELECT
                           min("Ид") iddekl,
                           "ОтчетГод" || "Период" period
                         FROM (
                           SELECT
                             tab1."Ид",
                             tab1."ИНННП",
                             max(to_number(tab1."НомКорр"))
                             OVER (PARTITION BY tab1."ИНННП", tab1."ОтчетГод", tab1."Период") "НомКоррМакс",
                             to_number(tab1."НомКорр") "НомКорр",
                             tab1."ОтчетГод",
                             tab1."Период"
                           FROM "ASKДекл" tab1 INNER JOIN "ASKДекл" tab2
                               ON tab1."ИНННП" = tab2."ИНННП" AND nvl(tab1."КППНП", '0') = nvl(tab2."КППНП", '0'))
                         WHERE nvl("НомКоррМакс", -1) = nvl("НомКорр", -1)
                         GROUP BY "ИНННП", "ОтчетГод", "Период") t3 JOIN "ASKДекл" t1 ON t3.iddekl = t1."Ид"
                    LEFT JOIN "ASKСумУплНА" t2 ON t1."Ид" = t2."ИдДекл"
                  WHERE t1."ДатаНачПер" BETWEEN otch_date - interval '3' year AND otch_date + interval '3' month
                ))
            GROUP BY "ИНН", maxSumIsch);
        COMMIT;

      END IF;

      otch_date := otch_date - interval '3' month;

      EXIT WHEN otch_date = to_date('01.04.2015', 'DD.MM.YYYY');
    END LOOP;

  END;
/
