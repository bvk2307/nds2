﻿ALTER TABLE NDS2_MC."ASKДекл" ADD "ДатаНачПер" DATE;

alter table NDS2_MC."ASKДекл" add "ПризнакНД" varchar2(1) default '0';
alter table NDS2_MC."ASKДекл" add "СуммаНДС" number;
CREATE INDEX NDS2_MC.IDX_ASKДЕКЛ_PRIZN ON NDS2_MC."ASKДекл" ("ПризнакНД" ASC);

create table NDS2_MC."ASKКритСУР"
(
  "ZIP"            NUMBER not null,
  "ИНН"            VARCHAR2(12 CHAR),
  "ОтчетныйПериод" DATE,
  "Период"         VARCHAR2(2 CHAR),
  "ОтчетГод"       VARCHAR2(4 CHAR),
  "К07001"         NUMBER,
  "К07002"         NUMBER,
  "К07003"         NUMBER,
  "К07003Кол"      NUMBER,
  "К07004"         NUMBER,
  "К07004Кол"      NUMBER,
  "К07005"         NUMBER,
  "К07006"         NUMBER,
  "К07007"         NUMBER,  
  "К07010"         NUMBER,
  "К07011"         NUMBER,
  "К07012"         NUMBER,
  "К07013"         NUMBER,
  "КПокСум"        NUMBER,
  "КПродСум"       NUMBER,
  "КПокСумПред"    NUMBER,
  "КолЗап"         NUMBER,
  "КолЗапПред"     NUMBER,
  "ДатаЗаписи"     DATE DEFAULT SYSDATE
)
;
alter table NDS2_MC."ASKКритСУР"
  add constraint PK_ASKКРИТСУР primary key ("ZIP");
;
alter table NDS2_MC."ASKКритСУР"
  add constraint FK_ASKКРИТСУР foreign key ("ZIP") references NDS2_MC."ASKZIPФайл"("Ид") on delete cascade
;