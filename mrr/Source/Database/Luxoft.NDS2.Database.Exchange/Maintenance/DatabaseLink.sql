﻿begin
for line in (select * from all_db_links where db_link like  'NDS2DBEXT%' or db_link like  'NDS2DBSEOD%') loop
	execute immediate 'drop public database link '||line.db_link;
end loop;
end;
/

create public database link NDS2DBEXT
CONNECT TO NDS2_MRR_USER IDENTIFIED BY NDS2_MRR_USER
using 'NDS2';

create public database link NDS2DBSEOD
CONNECT TO NDS2_SEOD IDENTIFIED BY NDS2_SEOD
using 'NDS2';
