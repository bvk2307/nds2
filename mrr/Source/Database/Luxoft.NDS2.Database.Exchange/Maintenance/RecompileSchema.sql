﻿begin
	DBMS_UTILITY.compile_schema(schema => 'NDS2_MC');
end;
/
begin
	DBMS_UTILITY.compile_schema(schema => 'FIR');
end;
/
begin
	DBMS_UTILITY.compile_schema(schema => 'NDS2_MRR_USER');
end;
/
begin
	DBMS_UTILITY.compile_schema(schema => 'I$CAM');
end;
/
begin
	DBMS_UTILITY.compile_schema(schema => 'I$NDS2');
end;
/
