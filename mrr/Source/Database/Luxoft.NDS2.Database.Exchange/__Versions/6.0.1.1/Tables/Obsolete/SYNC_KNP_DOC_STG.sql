﻿create table I$NDS2.SYNC_KNP_DOC_STG
(
  batch_id number,
  batch_date date,
  doc_id number,
  doc_num varchar(32),
  doc_type number(4),
  doc_date date,
  additional_amnt number(19,2),
  update_date date,
  sync_date date
);
