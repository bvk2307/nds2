﻿CREATE TABLE SYNC_OBJECT_TYPE
(
	ID NUMBER,
	DESCRIPTION VARCHAR2(512) NOT NULL,
	CONSTRAINT PK_SYNC_OBJ_TYPE_ID PRIMARY KEY (ID)
);
