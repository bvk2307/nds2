﻿create table I$NDS2.TEST_LOG_TYPE
(
       TYPE_ID NUMBER,
       DESCR VARCHAR2(32) NOT NULL,
       PRIMARY KEY (TYPE_ID)
);

truncate table I$NDS2.TEST_LOG_TYPE;
insert into I$NDS2.TEST_LOG_TYPE values(1, 'Сообщение');
insert into I$NDS2.TEST_LOG_TYPE values(2, 'Предупреждение');
insert into I$NDS2.TEST_LOG_TYPE values(3, 'Ошибка');
commit;
