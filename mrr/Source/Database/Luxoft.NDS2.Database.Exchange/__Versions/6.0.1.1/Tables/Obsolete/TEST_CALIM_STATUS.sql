﻿create table I$NDS2.TEST_CLAIM_STATUS
(
       STATUS_ID NUMBER(2),
       DESCR VARCHAR2(32) NOT NULL,
       PRIMARY KEY (STATUS_ID)
);

truncate table I$NDS2.TEST_CLAIM_STATUS;
insert into I$NDS2.TEST_CLAIM_STATUS values(51, 'Создано');
insert into I$NDS2.TEST_CLAIM_STATUS values(52, 'Передано в ЭОД');
insert into I$NDS2.TEST_CLAIM_STATUS values(53, 'Ошибка отправки');
insert into I$NDS2.TEST_CLAIM_STATUS values(54, 'Статус ЭОД 1');
insert into I$NDS2.TEST_CLAIM_STATUS values(55, 'Статус ЭОД 2');
insert into I$NDS2.TEST_CLAIM_STATUS values(56, 'Статус ЭОД N');
commit;
