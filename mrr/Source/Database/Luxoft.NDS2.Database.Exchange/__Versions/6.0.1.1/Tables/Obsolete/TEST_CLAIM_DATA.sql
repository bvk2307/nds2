﻿create table I$NDS2.TEST_CLAIM_DATA
(
       CLAIM_ID NUMBER,
       PROCESS_ID NUMBER not null,
       CREATE_DATE DATE not null,
       SOUN_CODE CHAR(4) not null,
       SEND_DATE DATE,
       STATUS NUMBER(2) not null,
       DISCREPANCY_DATA XMLType not null,
       EOD_ERROR VARCHAR2(1024),
       EOD_UPDATE_DATE DATE,
       SYNC_DATE DATE,   
       PRIMARY KEY (CLAIM_ID)
);
