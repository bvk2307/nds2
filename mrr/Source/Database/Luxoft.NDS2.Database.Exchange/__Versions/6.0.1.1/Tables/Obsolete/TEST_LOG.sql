﻿create table I$NDS2.TEST_LOG
(
       LOG_ID NUMBER,
       LOG_TYPE_ID NUMBER(1) not null,
       INSERT_DATE DATE not null,
       MESSAGE VARCHAR2(1024),
       PRIMARY KEY (LOG_ID)
);
