﻿create table I$NDS2.SYNC_CLAIM_STATUS_STG
( 
       BATCH_ID NUMBER not null,
       BATCH_DATE DATE not null,
       CLAIM_ID NUMBER not null,
       SYNC_GROUP_ID NUMBER not null,
       SOUN_CODE CHAR(4) not null,
       STATUS NUMBER(1) not null,
       ERROR VARCHAR2(1024),
       UPDATE_DATE DATE,
       SYNC_DATE DATE
);
