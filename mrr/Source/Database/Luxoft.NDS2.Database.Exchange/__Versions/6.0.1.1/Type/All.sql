﻿
CREATE TYPE I$NDS2.nds2_doc_row_type AS OBJECT 
(
  doc_id number(38),
  external_doc_num varchar2(128),
  external_doc_date date,
  ref_entity_id number(38),
  doc_type number(2),
  doc_kind number(2),
  sono_code varchar2(4 char),
  discrepancy_count number(19),
  discrepancy_amount number(19,2),
  create_date date,
  seod_accepted number(1),
  seod_accept_date date,
  tax_payer_send_date date,
  tax_payer_delivery_date date,
  tax_payer_delivery_method varchar2(1),
  tax_payer_reply_date date,
  close_date date,
  status_date date,
  status number(2),
  stage number,
  document_body clob,
  user_comment varchar2(2000 char),
  deadline_date date,
  prolong_answer_date date
)
/

CREATE TYPE I$NDS2.nds2_doc_table_type AS TABLE OF I$NDS2.nds2_doc_row_type
/



