﻿create or replace package I$NDS2.PAC$CAM_SYNC
  as

  procedure ACCEPT
    (
      P_OBJ_ID IN NUMBER,
      P_OBJ_TYPE_ID IN NUMBER
    );

  procedure DECLINE
    (
      P_OBJ_ID IN NUMBER,
      P_OBJ_TYPE_ID IN NUMBER
    );

  procedure PROCESS_NEW_DATA;

  end;
/
create or replace package body I$NDS2.PAC$CAM_SYNC
as

procedure UTL_LOG
  (
    v_source varchar2,
    v_code varchar2 := 0,
    v_msg varchar2 := null,
    v_entity_id varchar2 := null
  )
as
pragma autonomous_transaction;
begin
 insert into system_log(id, type, site, entity_id, message_code, message)
 values(seq_sys_log_id.nextval, 0, v_source, v_entity_id, v_code, v_msg);
 commit;
exception when others then
  rollback;
  dbms_output.put_line('UTL_LOG:'||sqlcode||'-'||substr(sqlerrm, 0, 256));
end;

procedure CHANGE_DOC_STATUS
(
  p_doc_id number,
  p_status number
)
as
begin
  update doc
  set status = p_status
  where doc_id = p_doc_id;

  insert into doc_status_history(doc_id, status, status_date)
  values(p_doc_id, p_status, sysdate);

  commit;
end;

procedure ACCEPT
(
  P_OBJ_ID IN NUMBER,
  P_OBJ_TYPE_ID IN NUMBER
)
as
pragma autonomous_transaction;
begin
 CHANGE_DOC_STATUS(P_OBJ_ID, 2);
commit;
end;

procedure DECLINE
(
  P_OBJ_ID IN NUMBER,
  P_OBJ_TYPE_ID IN NUMBER
)
as
pragma autonomous_transaction;
begin
 CHANGE_DOC_STATUS(P_OBJ_ID, 3);
end;

procedure PROCESS_NEW_DATA
as
v_result boolean;
v_err_msg varchar2(1024);
pragma autonomous_transaction;
begin
UTL_LOG('PROCESS_NEW_DATA - start;');
for line in (select * from I$CAM.V$nds2_Outgoing)
loop
  v_result := false;
  v_err_msg := '';

  PAC$EXCHANGE.P$PROCESS_NEW_SEOD_DATA
  (
    p_info_type => line.info_type,
    p_info_type_name => line.info_type_name,
    p_date_eod => line.date_eod,
    p_date_receipt => line.date_receipt,
    p_code_nsi_sono => line.code_nsi_sono,
    p_reg_number => line.reg_number,
    p_xml_data => line.xml_data,
    p_out_result => v_result,
    p_out_error_text => v_err_msg
  );


  if v_result then
      dbms_output.put_line('Ok');
      I$CAM.PAC$NDS2.P$ACCEPT(VP_ID => line.id);
  else
      dbms_output.put_line(line.id||':'||v_err_msg);
      I$CAM.PAC$NDS2.P$REFUSE(VP_ID => line.id, VP_ERR_DESC => v_err_msg);
  end if;

end loop;
commit;
 UTL_LOG('PROCESS_NEW_DATA - end;');
exception when others then
  rollback;
   UTL_LOG('PROCESS_NEW_DATA:', sqlcode,substr(sqlerrm, 256));
end;
end;
/
