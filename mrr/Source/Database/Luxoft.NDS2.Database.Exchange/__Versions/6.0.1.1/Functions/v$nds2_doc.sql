﻿CREATE OR REPLACE FUNCTION I$NDS2.v$nds2_doc RETURN nds2_doc_table_type PIPELINED IS
PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
 insert into NDS2_DOC_TMP select * from doc@NDS2DBEXT;
 commit;
 FOR cur IN (SELECT   doc_id
  ,external_doc_num
  ,external_doc_date
  ,ref_entity_id
  ,doc_type
  ,doc_kind
  ,sono_code
  ,discrepancy_count
  ,discrepancy_amount
  ,create_date
  ,seod_accepted
  ,seod_accept_date
  ,tax_payer_send_date
  ,tax_payer_delivery_date
  ,tax_payer_delivery_method
  ,tax_payer_reply_date
  ,close_date
  ,status_date
  ,status
  ,stage
  ,document_body
  ,user_comment
  ,deadline_date
  ,prolong_answer_date from NDS2_DOC_TMP)
 LOOP
   PIPE ROW(nds2_doc_row_type(cur.doc_id
  ,cur.external_doc_num
  ,cur.external_doc_date
  ,cur.ref_entity_id
  ,cur.doc_type
  ,cur.doc_kind
  ,cur.sono_code
  ,cur.discrepancy_count
  ,cur.discrepancy_amount
  ,cur.create_date
  ,cur.seod_accepted
  ,cur.seod_accept_date
  ,cur.tax_payer_send_date
  ,cur.tax_payer_delivery_date
  ,cur.tax_payer_delivery_method
  ,cur.tax_payer_reply_date
  ,cur.close_date
  ,cur.status_date
  ,cur.status
  ,cur.stage
  ,cur.document_body
  ,cur.user_comment
  ,cur.deadline_date
  ,cur.prolong_answer_date));
 END LOOP;
 DELETE FROM NDS2_DOC_TMP;
 COMMIT;
 RETURN;
END;
/
