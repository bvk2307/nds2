﻿create sequence I$NDS2.SEQ$CLAIM_TEST_ID
start with 1
increment by 1
maxvalue 999999999999999999
minvalue 1
nocache;

create sequence I$NDS2.SEQ$REQUEST_TEST_ID
start with 1
increment by 1
maxvalue 999999999999999999
minvalue 1
nocache;

create sequence I$NDS2.SEQ$TEST_LOG_ID
start with 1
increment by 1
maxvalue 999999999999999999
minvalue 1
nocache;

create sequence I$NDS2.SEQ$EOD_CLAIM_SYNC
start with 1
increment by 1
maxvalue 999999999999999999
minvalue 1
nocache;

create sequence I$NDS2.SEQ$CLAIM_STATUS_BATCH_ID
start with 1
increment by 1
maxvalue 999999999999999999
minvalue 1
nocache;

create sequence I$NDS2.SEQ$SYNC_KNP_DOC
start with 1
increment by 1
maxvalue 999999999999999999
minvalue 1
nocache;
