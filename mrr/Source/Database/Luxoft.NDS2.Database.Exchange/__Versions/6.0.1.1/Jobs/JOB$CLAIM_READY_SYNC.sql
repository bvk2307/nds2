﻿BEGIN
DBMS_SCHEDULER.DROP_JOB (
job_name => 'I$NDS2.J$CLAIM_READY_TO_SYNC');
exception when others then null;
END;
/
begin 
	sys.dbms_scheduler.create_job(job_name => 'I$NDS2.J$CLAIM_READY_TO_SYNC', 
	job_type => 'STORED_PROCEDURE', 
  job_action => 'PAC$CAM_SYNC.PROCESS_NEW_DATA', 
  start_date => SYSDATE + INTERVAL '1' SECOND, 
  repeat_interval => 'freq=Secondly;Interval=10', 
	end_date => to_date('01-01-2100 00:00:00', 'dd-mm-yyyy hh24:mi:ss'), 
	job_class => 'DEFAULT_JOB_CLASS', 
	enabled => false, 
	auto_drop => false, 
	comments => 'Запрос на синхронизацияю новых требований со стороны НДС2(МРР)'); 
end; 
/
