﻿create or replace force view I$NDS2.V$CAM_OUTGOING
(
/* Код НО */
SONO_CODE,
/* Идентификатор декларации */
DECLARTION_REG_NUM,
/* Идентификатор объекта синхронизации */
OBJECT_REG_NUM,
/* Идентификатор типа объекта синхронизации */
OBJECT_TYPE_ID,
/* Наименование типа объекта синхронизации */
OBJECT_TYPE_NAME,
/* Дата создания объекта синхронизации */
CREATE_DATE,
/* Статус синхронизации(идентификатор) */
OBJECT_STATUS_ID,
/* Статус синхронизации(наименование) */
OBJECT_STATUS_NAME,
/* XML данные синхронизации */
XML_DATA
)
AS
SELECT
d.sono_code                as SONO_CODE,
d.ref_entity_id           as DECLARATION_REG_NUM,
d.doc_id                    as OBJECT_REG_NUM,
d.doc_kind                           as OBJECT_TYPE_ID,
dt.description            as OBJECT_TYPE_NAME,
d.create_date              as CREATE_DATE,
d.status                   as OBJECT_STATUS_ID,
cls.description             as OBJECT_STATUS_NAME,
d.document_body         as XML_DATA
FROM table(v$nds2_doc) d
INNER JOIN Doc_Status cls on cls.id = d.status and cls.doc_type = d.doc_type
INNER JOIN Doc_Type dt on dt.id = decode(d.doc_type, 5, 1, d.doc_type)
WHERE d.status = 1;

