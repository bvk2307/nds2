﻿begin
execute immediate 'drop FUNCTION I$NDS2.v$nds2_doc';
exception when others then null;
end;

/


CREATE OR REPLACE FUNCTION I$NDS2.V$DOC RETURN doc_table_type PIPELINED IS
PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
 insert into DOC_TMP(
 SONO_CODE, 
 DECLARTION_REG_NUM, 
 OBJECT_REG_NUM, 
 OBJECT_TYPE_ID, 
 OBJECT_TYPE_NAME, 
 OBJECT_STATUS_NAME, 
 OBJECT_STATUS_ID, 
 CREATE_DATE, 
 XML_DATA) 
 select 
   SONO_CODE, 
   DECLARTION_REG_NUM, 
   OBJECT_REG_NUM, 
   OBJECT_TYPE_ID, 
   OBJECT_TYPE_NAME, 
   OBJECT_STATUS_NAME, 
   OBJECT_STATUS_ID, 
   CREATE_DATE, 
   XML_DATA
 from V$ASK_OUTGOING@NDS2DBEXT;
 commit;
 
 FOR cur IN (SELECT
   SONO_CODE,
  DECLARTION_REG_NUM,
  OBJECT_REG_NUM,
  OBJECT_TYPE_ID,
  OBJECT_TYPE_NAME,
  OBJECT_STATUS_NAME,
  OBJECT_STATUS_ID,
  CREATE_DATE,
  XML_DATA
  from DOC_TMP)
 LOOP
   PIPE ROW(doc_row_type(
   cur.SONO_CODE,
   cur.DECLARTION_REG_NUM,
   cur.OBJECT_REG_NUM,
   cur.OBJECT_TYPE_ID,
   cur.OBJECT_TYPE_NAME,
   cur.OBJECT_STATUS_NAME,
   cur.OBJECT_STATUS_ID,
   cur.CREATE_DATE,
   cur.XML_DATA));
 END LOOP;
 DELETE FROM DOC_TMP;
 COMMIT;
 RETURN;
END;

/
