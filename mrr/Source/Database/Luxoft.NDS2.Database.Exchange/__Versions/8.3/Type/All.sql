﻿
begin
execute immediate 'drop type I$NDS2.nds2_doc_table_type';
exception when others then null;
end;
/

begin
execute immediate 'drop type I$NDS2.nds2_doc_row_type';
exception when others then null;
end;
/

begin
execute immediate 'drop type I$NDS2.DOC_TABLE_TYPE';
exception when others then null;
end;
/

begin
execute immediate 'drop type I$NDS2.DOC_ROW_TYPE';
exception when others then null;
end;
/


CREATE OR REPLACE TYPE I$NDS2.DOC_ROW_TYPE AS OBJECT
(
  SONO_CODE varchar2(4 char),
  DECLARTION_REG_NUM number(38),
  OBJECT_REG_NUM number(38),
  OBJECT_TYPE_ID number(2),
  OBJECT_TYPE_NAME varchar2(64 CHAR),
  OBJECT_STATUS_NAME varchar2(64 CHAR),
  OBJECT_STATUS_ID number(2),
  CREATE_DATE DATE,
  XML_DATA clob
)

/

CREATE OR REPLACE TYPE I$NDS2.DOC_TABLE_TYPE AS TABLE OF I$NDS2.DOC_ROW_TYPE

/
