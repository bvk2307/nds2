﻿begin
execute immediate 'drop table I$NDS2.NDS2_DOC_TMP';
exception when others then null;
end;

/

begin
execute immediate 'drop table I$NDS2.DOC_TMP';
exception when others then null;
end;

/

CREATE GLOBAL TEMPORARY TABLE I$NDS2.DOC_TMP
(	
	SONO_CODE VARCHAR2(4 CHAR), 
	DECLARTION_REG_NUM NUMBER(38,0), 
	OBJECT_REG_NUM NUMBER(38,0), 
	OBJECT_TYPE_ID NUMBER(2,0), 
	OBJECT_TYPE_NAME VARCHAR2(64 CHAR), 
	OBJECT_STATUS_NAME VARCHAR2(64 CHAR), 
	OBJECT_STATUS_ID NUMBER(2,0), 
	CREATE_DATE DATE, 
	XML_DATA CLOB
) 
ON COMMIT PRESERVE ROWS;
