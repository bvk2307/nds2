﻿create or replace package i$nds2.PAC$EXCHANGE
as
  procedure P$PROCESS_NEW_SEOD_DATA
  (
     p_info_type varchar2,
     p_info_type_name varchar2,
     p_date_eod date,
     p_date_receipt date,
     p_code_nsi_sono varchar2,
     p_reg_number number,
     p_xml_data clob,
     p_out_result out boolean,
     p_out_error_text out varchar2
  );
end;
/
create or replace package body i$nds2.PAC$EXCHANGE
as
procedure UTL_LOG_CONSOLE(v_msg varchar2)
as
pragma autonomous_transaction;
begin
  dbms_output.put_line(v_msg);
end;

procedure UTL_LOG
  (
    v_source varchar2,
    v_code varchar2 := 0,
    v_msg varchar2 := null,
    v_entity_id varchar2 := null
  )
as
pragma autonomous_transaction;
begin
 insert into system_log(id, type, site, entity_id, message_code, message, log_date)
 values(seq_sys_log_id.nextval, 0, v_source, v_entity_id, v_code, v_msg, sysdate);
 commit;
null;
exception when others then
  rollback;
  dbms_output.put_line('UTL_LOG:'||sqlcode||'-'||substr(sqlerrm, 0, 256));
end;

function UTL_EXTRACT_ATTRIBUTE_STRING
(
  p_xml xmltype,
  p_xpath varchar2
)
return varchar2
is
begin
  if p_xml.extract(p_xpath) is not null then
    return p_xml.extract(p_xpath).getStringVal();
  else
    return null;
  end if;
  exception when others then return null;
end;

function UTL_EXTRACT_ATTRIBUTE_NUM
(
  p_xml xmltype,
  p_xpath varchar2
)
return number
is
begin
  if p_xml.extract(p_xpath) is not null then
    return p_xml.extract(p_xpath).getNumberVal();
  else
    return null;
  end if;
  exception when others then return null;
end;

function UTL_EXTRACT_ATTRIBUTE_DATE
(
  p_xml xmltype,
  p_xpath varchar2
)
return date
is
v_strVal varchar2(10 CHAR);
v_pattern varchar2(10 CHAR) := 'dd.mm.yyyy';
begin
  if p_xml.extract(p_xpath) is not null then
    v_strVal := p_xml.extract(p_xpath).getStringVal();
    if length(v_strVal) = 10 then
      return to_date(v_strVal, v_pattern);
    else
      return null;
    end if;
  else
    return null;
  end if;
  exception when others then
    dbms_output.put_line('UTL_EXTRACT_ATTRIBUTE_DATE:'||sqlcode||'-'||substr(sqlerrm, 256));
    return null;
end;

procedure CREATE_EXPLAIN
(
  p_doc_id  number,
  p_type_id number,
  p_incoming_num  varchar2,
  p_incoming_date date,
  p_file_id varchar2,
  p_executor_receive_date  date,
  p_send_date_to_iniciator date
)
as
  v_explain_id number;
  v_hist_explain_id number;
begin
  v_explain_id := SEQ_EXPLAIN_REPLY.NEXTVAL();
  v_hist_explain_id := SEQ_HIST_EXPLAIN_REPLY.NEXTVAL();

  insert into seod_explain_reply (explain_id, doc_id, type_id, status_id, incoming_num, incoming_date, executor_receive_date, send_date_to_iniciator, file_id)
  values (v_explain_id, p_doc_id, p_type_id, 1, p_incoming_num, p_incoming_date, p_executor_receive_date, p_send_date_to_iniciator, p_file_id);

  insert into hist_explain_reply_status(id, explain_id, submit_date, status_id)
  values(v_hist_explain_id, v_explain_id, sysdate, 1);
end;

/*########################################################################################################################*/
/*#идентификационные данные по налоговой декларации по налогу на добавленную стоимость, передаваемые из СЭОД в АСК НДС-2.#*/
/*########################################################################################################################*/
 function CAM_01_01_WRITE
   (
      p_data in clob,
      p_id in number,
      p_sono_code in varchar2,
      p_eod_date in date,
      p_receipt_date in date,
      p_refuse_reason out varchar2
   )
  return boolean
  is
  pragma autonomous_transaction;
  v_data xmltype;
  v_nds2_id number;
  v_tax_period varchar2(2);
  v_fiscal_year varchar2(4);
  v_correction_number varchar2(3);
  v_decl_reg_num number;
  v_decl_fid varchar2(128);
  v_tax_payer_type number(1);
  v_inn varchar2(12);
  v_kpp varchar2(9);
  v_idFile varchar2(512);
  v_deliveryType number(1) := 1;
  begin
    v_data                := xmltype(p_data);
    v_tax_period          := v_data.extract('/Файл/Документ/@Период').getStringVal();
    v_fiscal_year         := v_data.extract('/Файл/Документ/@ОтчетГод').getStringVal();
    v_correction_number   := v_data.extract('/Файл/Документ/@НомКорр').getStringVal();
    v_decl_reg_num        := to_number(v_data.extract('/Файл/Документ/@РегНомДек').getStringVal());
    v_decl_fid            := v_data.extract('/Файл/Документ/@ФидДек').getStringVal();
    v_tax_payer_type      := v_data.existsNode('/Файл/Документ/СвНП/НПЮЛ');
    v_idFile              := UTL_EXTRACT_ATTRIBUTE_STRING(v_data, '/Файл/Документ/@ИдФайл');

    if length(v_idFile) > 0 then
       v_deliveryType := 0;
    end if;
    
    if v_tax_payer_type = 1 then
       v_inn := v_data.extract('/Файл/Документ/СвНП/НПЮЛ/@ИННЮЛ').getStringVal();
       v_kpp := v_data.extract('/Файл/Документ/СвНП/НПЮЛ/@КПП').getStringVal();
    else
       v_inn := v_data.extract('/Файл/Документ/СвНП/НПФЛ/@ИННФЛ').getStringVal();
    end if;

    v_nds2_id := to_number(v_tax_period||v_fiscal_year||v_inn);

    merge into seod_declaration sd
    using (select v_decl_reg_num as seod_reg_num from dual) tmp
    on (tmp.seod_reg_num = sd.decl_reg_num)
    when not matched then
    insert (id, nds2_id, type, sono_code, tax_period, fiscal_year, correction_number, decl_reg_num, decl_fid, tax_payer_type, inn, kpp, insert_date, eod_date, date_receipt, delivery_type, Id_file)
    values(p_id, v_nds2_id, 0, p_sono_code, v_tax_period, v_fiscal_year, v_correction_number, v_decl_reg_num, v_decl_fid, v_tax_payer_type, v_inn, v_kpp, sysdate, p_eod_date, p_receipt_date, v_deliveryType, v_idFile);

    begin
      for decl_row in (select * from seod_declaration where nds2_id = v_nds2_id and decl_reg_num <> v_decl_reg_num)
      loop
        UTL_CLOSE_DOCUMENT(decl_row.decl_reg_num);
      end loop;
    exception when others then
       UTL_LOG('CAM_01_01_WRITE(UTL_CLOSE_DOCUMENT)', sqlcode, substr(sqlerrm, 0, 256));
    end;

    commit;
    UTL_LOG('CAM_01_01_WRITE', 0, 'success', v_nds2_id);
    return true;
  exception when others then
    rollback;
    UTL_LOG('CAM_01_01_WRITE', sqlcode, substr(sqlerrm, 0, 256));
    return false;
  end;

/*################################################################################*/
/*#данные по статусам исполнения автотребования, передаваемые из СЭОД в АСК НДС-2#*/
/*################################################################################*/
   function CAM_02_01_WRITE
   (
      p_data in clob,
      p_id in number,
      p_sono_code in varchar2,
      p_eod_date in date,
      p_receipt_date in date,
      p_refuse_reason out varchar2
   )
  return boolean
  as
  v_xml xmltype;
  v_doc_id number(19);
  v_seod_doc_num varchar2(128);
  v_seod_doc_date date;
  v_send_date date;
  v_delivery_date date;
  v_delivery_method varchar(2);
  v_status number(1);
  v_deadline_date date;
  pragma autonomous_transaction;
  begin
    v_xml             := xmltype(p_data);
    v_doc_id          := UTL_EXTRACT_ATTRIBUTE_NUM(v_xml, '/Файл/Документ/ТребПоясн/@УчНомТреб');
    v_seod_doc_num    := UTL_EXTRACT_ATTRIBUTE_STRING(v_xml, '/Файл/Документ/ТребПоясн/@НомДок');
    v_seod_doc_date   := UTL_EXTRACT_ATTRIBUTE_DATE(v_xml, '/Файл/Документ/ТребПоясн/@ДатаДок');
    v_send_date       := UTL_EXTRACT_ATTRIBUTE_DATE(v_xml, '/Файл/Документ/ТребПоясн/@ДатаОтправ');
    v_delivery_date   := UTL_EXTRACT_ATTRIBUTE_DATE(v_xml, '/Файл/Документ/ТребПоясн/@ДатаВруч');
    v_delivery_method := UTL_EXTRACT_ATTRIBUTE_STRING(v_xml, '/Файл/Документ/ТребПоясн/@СпВруч');

    /*отправлено налогоплательщику*/
    if v_send_date is not null then
      v_status := 4;
      insert into doc_status_history(doc_id, status, status_date)
      values(v_doc_id, v_status, sysdate);

      v_deadline_date := utl_add_work_days(v_send_date,
        utl_get_configuration_number('timeout_answer_for_autoclaim') +
        utl_get_configuration_number('timeout_claim_delivery'));
    end if;
    /*вручено налогоплательщику*/
    if v_delivery_date is not null then
      v_status := 5;
      insert into doc_status_history(doc_id, status, status_date)
      values(v_doc_id, v_status, sysdate);

      v_deadline_date := utl_add_work_days(v_delivery_date,
        utl_get_configuration_number('timeout_answer_for_autoclaim'));
    end if;

    update doc d
    set
     d.tax_payer_send_date = nvl(v_send_date, d.tax_payer_send_date),
     d.external_doc_date = v_seod_doc_date,
     d.tax_payer_delivery_date = nvl(v_delivery_date, d.tax_payer_delivery_date),
     d.tax_payer_delivery_method = nvl(v_delivery_method, d.tax_payer_delivery_method),
     d.external_doc_num = v_seod_doc_num,
     d.status = nvl(v_status, d.status),
     d.status_date = sysdate,
     d.deadline_date = nvl(v_deadline_date, d.deadline_date)
    where d.doc_id = v_doc_id;

  commit;
  UTL_LOG('CAM_02_01_WRITE', 0, 'success', v_doc_id);

  return true;

  exception when others then
    rollback;
    UTL_LOG('CAM_02_01_WRITE', sqlcode, substr(sqlerrm, 0, 256));
    return false;
  end;
/*#########################################################################################*/
/*#данные об ответе налогоплательщика на автотребование, передаваемые из СЭОД в АСК НДС-3.#*/
/*#########################################################################################*/
   function CAM_03_01_WRITE
   (
      p_data in clob,
      p_id in number,
      p_sono_code in varchar2,
      p_eod_date in date,
      p_receipt_date in date,
      p_refuse_reason out varchar2
   )
  return boolean
  as
  v_xml xmltype;
  pragma autonomous_transaction;
  begin
    v_xml := xmltype(p_data);

    for line in
    (
      select reply.doc_id, reply.incoming_num, reply.incoming_date, reply.file_id
      from
      (
        select
          cast(req_answer."req_id" as number) as doc_id,
          case when length(req_answer."answer_date") = 10 then to_date(req_answer."answer_date", 'dd.mm.yyyy') else null end as incoming_date,
          req_answer."answer_number" as incoming_num,
      req_answer."file_id" as file_id
        from xmltable
        (
          '/Файл/Документ/ТребПоясн'
          passing v_xml
          columns
            "answer_number" varchar2(100) PATH '/ТребПоясн/@НомОтв',
            "answer_date" varchar2(100) PATH '/ТребПоясн/@ДатаОтв',
      "file_id" varchar2(100) PATH '/ТребПоясн/@ИмяФайлОтв',
            "req_id" varchar2(100) PATH '/ТребПоясн/@УчНомТреб'
        ) req_answer
      ) reply
      left join seod_explain_reply stored_reply
        on stored_reply.doc_id = reply.doc_id and stored_reply.incoming_num = reply.incoming_num and stored_reply.type_id = 1
      where stored_reply.doc_id is null
    )
    loop
      CREATE_EXPLAIN(line.doc_id, 1, line.incoming_num, line.incoming_date, line.file_id, null, null);
    UTL_LOG('CAM_03_01_WRITE', 0, 'success', line.doc_id);
    end loop;
    commit;

    return true;
    exception when others then
    rollback;
    UTL_LOG('CAM_03_01_WRITE', sqlcode, substr(sqlerrm, 0, 256));
    return false;
  end;


  function CAM_04_01_WRITE
   (
      p_data in clob,
      p_id in number,
      p_sono_code in varchar2,
      p_eod_date in date,
      p_receipt_date in date,
      p_refuse_reason out varchar2
   )
  return boolean
  as
    vDeclarationRegNum number(13);
    vKnpId number;
    vDate date;
    pRawData xmltype;
  pragma autonomous_transaction;
  begin
    pRawData := xmltype(p_data);
    select extractValue(value(t),'Документ/@РегНомДек')
    into vDeclarationRegNum
    from table(XMLSequence(pRawData.Extract('Файл/Документ'))) t;

    begin
         select KNP_ID
         into vKnpId
         from SEOD_KNP
         where DECLARATION_REG_NUM = vDeclarationRegNum;
         exception when NO_DATA_FOUND then vKnpId := null;
    end;

    if vKnpId is NULL then
      begin
       select SEQ_SEOD_KNP.NEXTVAL into vKnpId from DUAL;
       for line in (select extractValue(value(d), 'Документ/@КодНО') as sono from table(XMLSequence(pRawData.Extract('Файл/Документ'))) d)
        loop
               insert into SEOD_KNP (knp_id, declaration_reg_num, creation_date, ifns_code)
               values(vKnpId, vDeclarationRegNum, sysdate, line.sono);
        end loop;
      end;
    end if;

    vDate := UTL_EXTRACT_ATTRIBUTE_DATE(pRawData,'/Файл/Документ/КНП/СвЗавКНП/@ДатаЗавКНП');

    update SEOD_KNP
           set completion_date = nvl(vDate, completion_date)
    where knp_id = vKnpId;

     UTL_CLOSE_DOCUMENT(vDeclarationRegNum);

for line in (select
                vKnpId,
                to_date(extractValue(value(act), 'АктКНП/@ДатаДок'),'DD.MM.YYYY') as docDate,
                extractValue(value(act), 'АктКНП/@НомДок') as docNum,
                to_number(extractValue(value(act), 'АктКНП/@СумНеупл')) as SumNeup,
                to_number(extractValue(value(act), 'АктКНП/@СумПени')) as sumPeni,
                to_number(extractValue(value(act), 'АктКНП/@СумЗавВозм')) as SumVozm
           from
                table(XMLSequence(pRawData.Extract('Файл/Документ/КНП/АктКНП'))) act)
loop
    insert into SEOD_KNP_ACT (knp_id,doc_data,doc_num,sum_unpaid,sum_penalty,sum_overestimated)
    values (line.vKnpId, line.docDate, line.docNum, line.SumNeup, line.sumPeni, line.SumVozm);
end loop;

--РешПриост
for line in (select
                vKnpId,
                (select x.id from SEOD_KNP_DECISION_TYPE x where x.name = 'РешПриост') as typeId,
                to_date(extractValue(value(t),'РешПриост/@ДатаДок'),'DD.MM.YYYY') as docDate,
                extractValue(value(t), 'РешПриост/@НомДок') as docNum
           from table(XMLSequence(pRawData.Extract('Файл/Документ/КНП/РешПриост'))) t)
loop
    insert into SEOD_KNP_DECISION (knp_id,type_id,doc_date,doc_num)
    values (line.vKnpId, line.typeId, line.docDate, line.docNum);
end loop;

--РешПривлОтв
for line in (select
                vKnpId,
                (select x.id from SEOD_KNP_DECISION_TYPE x where x.name = 'РешПривлОтв') as typeId,
                to_date(extractValue(value(t),'РешПривлОтв/@ДатаДок'),'DD.MM.YYYY') as docDate,
                extractValue(value(t), 'РешПривлОтв/@НомДок') as docNum,
                to_number(extractValue(value(t), 'РешПривлОтв/@СумНеупл')) as Amnt,
                to_number(extractValue(value(t), 'РешПривлОтв/@СумПени')) as Amnt2,
                to_number(extractValue(value(t), 'РешПривлОтв/@СумШтраф')) as Amnt3
           from table(XMLSequence(pRawData.Extract('Файл/Документ/КНП/РешПривлОтв'))) t)
loop
    insert into SEOD_KNP_DECISION (knp_id,type_id,doc_date,doc_num, amount, amount2, amount3)
    values (line.vKnpId, line.typeId, line.docDate, line.docNum, line.amnt, line.amnt2, line.amnt3);
end loop;

--РешОтказПривл
for line in (select
                vKnpId,
                (select x.id from SEOD_KNP_DECISION_TYPE x where x.name = 'РешОтказПривл') as typeId,
                to_date(extractValue(value(t),'РешОтказПривл/@ДатаДок'),'DD.MM.YYYY') as docDate,
                extractValue(value(t), 'РешОтказПривл/@НомДок') as docNum,
                to_number(extractValue(value(t), 'РешОтказПривл/@СумНеупл')) as Amnt,
                to_number(extractValue(value(t), 'РешОтказПривл/@СумПени')) as Amnt2
           from table(XMLSequence(pRawData.Extract('Файл/Документ/КНП/РешОтказПривл'))) t)
loop
    insert into SEOD_KNP_DECISION (knp_id,type_id,doc_date,doc_num, amount, amount2)
    values (line.vKnpId, line.typeId, line.docDate, line.docNum, line.amnt, line.amnt2);
end loop;


--РешВозмНДС
for line in (select
                vKnpId,
                (select x.id from SEOD_KNP_DECISION_TYPE x where x.name = 'РешВозмНДС') as typeId,
                to_date(extractValue(value(t),'РешВозмНДС/@ДатаДок'),'DD.MM.YYYY') as docDate,
                extractValue(value(t), 'РешВозмНДС/@НомДок') as docNum,
                to_number(extractValue(value(t), 'РешВозмНДС/@СумНДСВозм')) as Amnt
           from table(XMLSequence(pRawData.Extract('Файл/Документ/КНП/РешВозмНДС'))) t)
loop
    insert into SEOD_KNP_DECISION (knp_id,type_id,doc_date,doc_num, amount)
    values (line.vKnpId, line.typeId, line.docDate, line.docNum, line.amnt);
end loop;


--РешОтказВозмНДС
for line in (select
                vKnpId,
                (select x.id from SEOD_KNP_DECISION_TYPE x where x.name = 'РешОтказВозмНДС') as typeId,
                to_date(extractValue(value(t),'РешОтказВозмНДС/@ДатаДок'),'DD.MM.YYYY') as docDate,
                extractValue(value(t), 'РешОтказВозмНДС/@НомДок') as docNum,
                to_number(extractValue(value(t), 'РешОтказВозмНДС/@СумОтказВозм')) as Amnt
           from table(XMLSequence(pRawData.Extract('Файл/Документ/КНП/РешОтказВозмНДС'))) t)
loop
    insert into SEOD_KNP_DECISION (knp_id,type_id,doc_date,doc_num, amount)
    values (line.vKnpId, line.typeId, line.docDate, line.docNum, line.amnt);
end loop;


--РешВозмНДСЗаявит
for line in (select
                vKnpId,
                (select x.id from SEOD_KNP_DECISION_TYPE x where x.name = 'РешВозмНДСЗаявит') as typeId,
                to_date(extractValue(value(t),'РешВозмНДСЗаявит/@ДатаДок'),'DD.MM.YYYY') as docDate,
                extractValue(value(t), 'РешВозмНДСЗаявит/@НомДок') as docNum,
                to_number(extractValue(value(t), 'РешВозмНДСЗаявит/@СумНДСВозм')) as Amnt
           from table(XMLSequence(pRawData.Extract('Файл/Документ/КНП/РешВозмНДСЗаявит'))) t)
loop
    insert into SEOD_KNP_DECISION (knp_id,type_id,doc_date,doc_num, amount)
    values (line.vKnpId, line.typeId, line.docDate, line.docNum, line.amnt);
end loop;


--РешОтказВозмНДСЗаявит
for line in (select
                vKnpId,
                (select x.id from SEOD_KNP_DECISION_TYPE x where x.name = 'РешОтказВозмНДСЗаявит') as typeId,
                to_date(extractValue(value(t),'РешОтказВозмНДСЗаявит/@ДатаДок'),'DD.MM.YYYY') as docDate,
                extractValue(value(t), 'РешОтказВозмНДСЗаявит/@НомДок') as docNum,
                to_number(extractValue(value(t), 'РешОтказВозмНДСЗаявит/@СумОтказВозм')) as Amnt
           from table(XMLSequence(pRawData.Extract('Файл/Документ/КНП/РешОтказВозмНДСЗаявит'))) t)
loop
    insert into SEOD_KNP_DECISION (knp_id,type_id,doc_date,doc_num, amount)
    values (line.vKnpId, line.typeId, line.docDate, line.docNum, line.amnt);
end loop;

--РешОтказВозмНДСЗаявит
for line in (select
                vKnpId,
                (select x.id from SEOD_KNP_DECISION_TYPE x where x.name = 'РешОтменВозмНДСЗаявит') as typeId,
                to_date(extractValue(value(t),'РешОтменВозмНДСЗаявит/@ДатаДок'),'DD.MM.YYYY') as docDate,
                extractValue(value(t), 'РешОтменВозмНДСЗаявит/@НомДок') as docNum,
                to_number(extractValue(value(t), 'РешОтменВозмНДСЗаявит/@СумНДСВозмОтмен')) as Amnt
           from table(XMLSequence(pRawData.Extract('Файл/Документ/КНП/РешОтменВозмНДСЗаявит'))) t)
loop
    insert into SEOD_KNP_DECISION (knp_id,type_id,doc_date,doc_num, amount)
    values (line.vKnpId, line.typeId, line.docDate, line.docNum, line.amnt);
end loop;

--РешОтменВозмНДСУточн
for line in (select
                vKnpId,
                (select x.id from SEOD_KNP_DECISION_TYPE x where x.name = 'РешОтменВозмНДСУточн') as typeId,
                to_date(extractValue(value(t),'РешОтменВозмНДСУточн/@ДатаДок'),'DD.MM.YYYY') as docDate,
                extractValue(value(t), 'РешОтменВозмНДСУточн/@НомДок') as docNum,
                to_number(extractValue(value(t), 'РешОтменВозмНДСУточн/@СумНДСВозмОтмен')) as Amnt
           from table(XMLSequence(pRawData.Extract('Файл/Документ/КНП/РешОтменВозмНДСУточн'))) t)
loop
    insert into SEOD_KNP_DECISION (knp_id,type_id,doc_date,doc_num, amount)
    values (line.vKnpId, line.typeId, line.docDate, line.docNum, line.amnt);
end loop;

  commit;
  UTL_LOG('CAM_04_01_WRITE', 0, 'success', vDeclarationRegNum);
  return true;
  exception when others then
        rollback;
    UTL_LOG('CAM_04_01_WRITE', sqlcode, substr(sqlerrm, 0, 256));
        return false;
end;

  function CAM_05_01_WRITE
   (
      p_data in clob,
      p_id in number,
      p_sono_code in varchar2,
      p_eod_date in date,
      p_receipt_date in date,
      p_refuse_reason out varchar2
   )
  return boolean
  as
    pragma autonomous_transaction;
    v_xml xmltype;
    v_doc_num number(20);
    v_doc_accept_date date;
  begin
    v_xml := xmltype(p_data);

    v_doc_accept_date := UTL_EXTRACT_ATTRIBUTE_DATE(v_xml, '/Файл/Документ/@ДатаПолучДан');

    if v_xml.existsNode('/Файл/Документ/Требование/АвтоТреб') = 1 then
      v_doc_num := UTL_EXTRACT_ATTRIBUTE_NUM(v_xml, '/Файл/Документ/Требование/АвтоТреб/@УчНомТреб');
    end if;

    if v_xml.existsNode('/Файл/Документ/Требование/АвтоИстреб') = 1 then
      v_doc_num := UTL_EXTRACT_ATTRIBUTE_NUM(v_xml, '/Файл/Документ/Требование/АвтоИстреб/@УчНомИстреб');
    end if;

    update doc d set d.seod_accepted = 1, d.seod_accept_date = v_doc_accept_date where d.doc_id = v_doc_num;

    if SQL%ROWCOUNT = 0 then
      UTL_LOG('CAM_05_01_WRITE', sqlcode,  'не найдено требование с номером '||v_doc_num );
      p_refuse_reason := 'не найдено требование с номером '||v_doc_num;
      rollback;
      return false;
    else
      commit;
      UTL_LOG('CAM_03_01_WRITE', 0, 'success', v_doc_num);
      return true;
    end if;

    exception when others then
    rollback;
    UTL_LOG('CAM_05_01_WRITE', sqlcode, substr(sqlerrm, 0, 256));
    return false;
  end;

  function CAM_06_01_WRITE
  (
    p_data in clob, p_id in number,
    p_sono_code in varchar2,
    p_eod_date in date,
    p_receipt_date in date,
    p_refuse_reason out varchar2
  )
  return boolean
  as
  v_xml xmltype;
  v_doc_id number(19);
  v_seod_doc_num varchar2(128);
  v_seod_doc_date date;
  v_send_date date;
  v_delivery_date date;
  v_delivery_method varchar(2);
  v_status number(1);
  v_deadline_date date;
  pragma autonomous_transaction;
  begin
    v_xml             := xmltype(p_data);
    v_doc_id          := UTL_EXTRACT_ATTRIBUTE_NUM(v_xml, '/Файл/Документ/@УчНомИстреб');
    v_seod_doc_num    := UTL_EXTRACT_ATTRIBUTE_STRING(v_xml, '/Файл/Документ/ИсТреб/@НомДок');
    v_seod_doc_date   := UTL_EXTRACT_ATTRIBUTE_DATE(v_xml, '/Файл/Документ/ИсТреб/@ДатаДок');
    v_send_date       := UTL_EXTRACT_ATTRIBUTE_DATE(v_xml, '/Файл/Документ/ИсТреб/@ДатаОтправ');
    v_delivery_date   := UTL_EXTRACT_ATTRIBUTE_DATE(v_xml, '/Файл/Документ/ИсТреб/@ДатаВруч');
    v_delivery_method := UTL_EXTRACT_ATTRIBUTE_STRING(v_xml, '/Файл/Документ/ИсТреб/@СпВруч');

    /*отправлено налогоплательщику*/
    if v_send_date is not null then
      v_status := 4;
      insert into doc_status_history(doc_id, status, status_date)
      values(v_doc_id, v_status, v_send_date);

      v_deadline_date := utl_add_work_days(v_send_date,
        utl_get_configuration_number('timeout_answer_for_autooverclaim') +
        utl_get_configuration_number('timeout_claim_delivery'));
    end if;
    /*вручено налогоплательщику*/
    if v_delivery_date is not null then
      v_status := 5;
      insert into doc_status_history(doc_id, status, status_date)
      values(v_doc_id, v_status, v_delivery_date);

    v_deadline_date := utl_add_work_days(v_delivery_date,
        utl_get_configuration_number('timeout_answer_for_autooverclaim'));
    end if;

    update doc d
    set
      d.tax_payer_send_date = nvl(v_send_date, d.tax_payer_send_date),
      d.external_doc_date = nvl(v_seod_doc_date, d.external_doc_date),
      d.tax_payer_delivery_date = nvl(v_delivery_date, d.tax_payer_delivery_date),
      d.tax_payer_delivery_method = nvl(v_delivery_method, d.tax_payer_delivery_method),
      d.external_doc_num = nvl(v_seod_doc_num, d.external_doc_num),
      d.status = nvl(v_status, d.status),
      d.status_date = sysdate,
      d.deadline_date = nvl(v_deadline_date, d.deadline_date)
    where d.doc_id = v_doc_id;

    commit;
    UTL_LOG_CONSOLE('updated ' || sql%rowcount || ' rows');
    UTL_LOG('CAM_06_01_WRITE', 0, 'success', v_doc_id);
    return true;

    exception when others then
    rollback;
    UTL_LOG_CONSOLE('CAM_06_01_WRITE ' || sqlcode || substr(sqlerrm, 0, 256));
    UTL_LOG('CAM_06_01_WRITE', sqlcode, substr(sqlerrm, 0, 256));
    return false;
  end;

  function CAM_07_01_WRITE
  (
    p_data in clob,
    p_id in number,
    p_sono_code in varchar2,
    p_eod_date in date,
    p_receipt_date in date,
    p_refuse_reason out varchar2
  )
  return boolean
  as
  v_xml xmltype;
  v_doc_id number(20);
  v_doc_num varchar2(100);
  v_doc_date date;
  v_prolong_date date;
  v_deadline_date date;
  pragma autonomous_transaction;
  begin
    v_xml          := xmltype(p_data);
    v_doc_id       := UTL_EXTRACT_ATTRIBUTE_NUM(v_xml, '/Файл/Документ/@УчНомИстреб');
    v_doc_num      := UTL_EXTRACT_ATTRIBUTE_STRING(v_xml, '/Файл/Документ/РешПродлСрок/@НомДок');
    v_doc_date     := UTL_EXTRACT_ATTRIBUTE_DATE(v_xml, '/Файл/Документ/РешПродлСрок/@ДатаДок');
    v_prolong_date := UTL_EXTRACT_ATTRIBUTE_DATE(v_xml, '/Файл/Документ/РешПродлСрок/@ДатаПродлСрок');

    merge into seod_prolong_decision pd
    using
    (
      select
        v_doc_id as doc_id,
        v_doc_num as doc_num,
        v_doc_date as doc_date,
        v_prolong_date as prolong_date
      from dual
    ) d on (d.doc_id = pd.doc_id)
    when matched then update set
      pd.doc_num = nvl(d.doc_num, pd.doc_num),
      pd.doc_date = nvl(d.doc_date, pd.doc_date),
      pd.prolong_date = nvl(d.prolong_date, pd.prolong_date)
    when not matched then insert (pd.doc_id, pd.doc_num, pd.doc_date, pd.prolong_date)
      values (d.doc_id, d.doc_num, d.doc_date, d.prolong_date);

    if v_doc_id is not null and v_prolong_date is not null then
      update doc
      set deadline_date = v_prolong_date + utl_get_configuration_number('timeout_answer_for_autooverclaim')
      where doc_id = v_doc_id;
    end if;

    commit;
    UTL_LOG_CONSOLE('merged ' || sql%rowcount || ' rows');
    UTL_LOG('CAM_07_01_WRITE', 0, 'success', v_doc_id);
    return true;

    exception when others then
    rollback;
    UTL_LOG_CONSOLE('CAM_07_01_WRITE ' || sqlcode || substr(sqlerrm, 0, 256));
    UTL_LOG('CAM_07_01_WRITE', sqlcode, substr(sqlerrm, 0, 256));
    return false;
  end;

  function CAM_08_01_WRITE
  (
    p_data in clob,
    p_id in number,
    p_sono_code in varchar2,
    p_eod_date in date,
    p_receipt_date in date,
    p_refuse_reason out varchar2
  )
  return boolean
  as
  v_xml xmltype;
  v_doc_id number(20);
  v_reply_date date;
  pragma autonomous_transaction;
  begin
    v_xml        := xmltype(p_data);
    v_doc_id     := UTL_EXTRACT_ATTRIBUTE_NUM(v_xml, '/Файл/Документ/@УчНомИстреб');
    v_reply_date := UTL_EXTRACT_ATTRIBUTE_DATE(v_xml, '/Файл/Документ/ОтвТреб93/@ДатаОтв');
    CREATE_EXPLAIN(v_doc_id, 2, null, v_reply_date, null, null, null);

    commit;
    UTL_LOG_CONSOLE('inserted ' || sql%rowcount || ' rows');
    UTL_LOG('CAM_08_01_WRITE', 0, 'success', v_doc_id);
    return true;
    exception when others then
    rollback;
    UTL_LOG_CONSOLE('CAM_08_01_WRITE ' || sqlcode || substr(sqlerrm, 0, 256));
    UTL_LOG('CAM_08_01_WRITE', sqlcode, substr(sqlerrm, 0, 256));
    return false;
  end;

  function CAM_09_01_WRITE
  (
    p_data in clob,
    p_id in number,
    p_sono_code in varchar2,
    p_eod_date in date,
    p_receipt_date in date,
    p_refuse_reason out varchar2
  )
  return boolean
  as
  v_xml xmltype;
  v_doc_id number(20);
  v_doc_num varchar2(100);
  v_doc_date date;
  v_executive_soun_send_date date;
  v_executive_soun_recieve_date date;
  v_reject_reason varchar2(2);
  v_status number(1);
  pragma autonomous_transaction;
  begin
    v_xml                         := xmltype(p_data);
    v_doc_id                      := UTL_EXTRACT_ATTRIBUTE_NUM(v_xml, '/Файл/Документ/@УчНомИстреб');
    v_doc_num                     := UTL_EXTRACT_ATTRIBUTE_STRING(v_xml, '/Файл/Документ/Поруч/@НомПоруч');
    v_reject_reason               := UTL_EXTRACT_ATTRIBUTE_STRING(v_xml, '/Файл/Документ/Поруч/@ПричОтказ');
    v_doc_date                    := UTL_EXTRACT_ATTRIBUTE_DATE(v_xml, '/Файл/Документ/Поруч/@ДатаПоруч');
    v_executive_soun_send_date    := UTL_EXTRACT_ATTRIBUTE_DATE(v_xml, '/Файл/Документ/Поруч/@ДатаНапрПоруч');
    v_executive_soun_recieve_date := UTL_EXTRACT_ATTRIBUTE_DATE(v_xml, '/Файл/Документ/Поруч/@ДатаПолучПоруч');
    v_status := 0;

    /*Дата направления Поручения в НО-исполнитель*/
    if v_executive_soun_send_date is not null then
      v_status := 6;
      insert into doc_status_history(doc_id, status, status_date)
      values(v_doc_id, v_status, v_executive_soun_send_date);
    end if;
    /*Дата получения Поручения НО-исполнителем*/
    if v_executive_soun_recieve_date is not null then
      v_status := 7;
      insert into doc_status_history(doc_id, status, status_date)
      values(v_doc_id, v_status, v_executive_soun_recieve_date);
    end if;

    merge into seod_reclaim_order ro
    using
    (
      select
        v_doc_id as doc_id,
        v_doc_num as doc_num,
        v_reject_reason as reject_reason,
        v_doc_date as doc_date,
        v_executive_soun_send_date as executive_soun_send_date,
        v_executive_soun_recieve_date as executive_soun_recieve_date
      from dual
    ) d on (d.doc_id = ro.doc_id)
    when matched then update set
      ro.doc_num = nvl(d.doc_num, ro.doc_num),
      ro.reject_reason = nvl(d.reject_reason, ro.reject_reason),
      ro.doc_date = nvl(d.doc_date, ro.doc_date),
      ro.executive_soun_send_date = nvl(d.executive_soun_send_date, ro.executive_soun_send_date),
      ro.executive_soun_recieve_date = nvl(d.executive_soun_recieve_date, ro.executive_soun_recieve_date)
    when not matched then insert (ro.doc_id, ro.doc_num, ro.doc_date, ro.executive_soun_send_date, ro.executive_soun_recieve_date, ro.reject_reason)
      values (d.doc_id, d.doc_num, d.doc_date, d.executive_soun_send_date, d.executive_soun_recieve_date, d.reject_reason);

    update doc
    set status = v_status
    where doc_id = v_doc_id;

    commit;
    UTL_LOG_CONSOLE('merged ' || sql%rowcount || ' rows');
    UTL_LOG('CAM_09_01_WRITE', 0, 'success', v_doc_id);
    return true;
    exception when others then
    rollback;
    UTL_LOG_CONSOLE('CAM_09_01_WRITE ' || sqlcode || substr(sqlerrm, 0, 256));
    UTL_LOG('CAM_09_01_WRITE', sqlcode, substr(sqlerrm, 0, 256));
    return false;
  end;

  function CAM_10_01_WRITE
  (
    p_data in clob,
    p_id in number,
    p_sono_code in varchar2,
    p_eod_date in date,
    p_receipt_date in date,
    p_refuse_reason out varchar2
  )
  return boolean
  as
  v_xml xmltype;
  v_doc_id number(20);
  v_reply_date date;
  v_executor_recive_date date;
  v_initiator_send_date date;
  pragma autonomous_transaction;
  begin
    v_xml                  := xmltype(p_data);
    v_doc_id               := UTL_EXTRACT_ATTRIBUTE_NUM(v_xml, '/Файл/Документ/@УчНомИстреб');
    v_reply_date           := UTL_EXTRACT_ATTRIBUTE_DATE(v_xml, '/Файл/Документ/ОтвНП93.1/@ДатаОтв');
    v_executor_recive_date := UTL_EXTRACT_ATTRIBUTE_DATE(v_xml, '/Файл/Документ/ОтвНП93.1/@ВхДатаОтвИсп');
    v_initiator_send_date  := UTL_EXTRACT_ATTRIBUTE_DATE(v_xml, '/Файл/Документ/ОтвНП93.1/@ИсхДатаОтвИсп');

    CREATE_EXPLAIN(v_doc_id, 3, null, v_reply_date, null, v_executor_recive_date, v_initiator_send_date);
    commit;

    UTL_LOG_CONSOLE('inserted ' || sql%rowcount || ' rows');
    UTL_LOG('CAM_10_01_WRITE', 0, 'success', v_doc_id);
    return true;
    exception when others then
    rollback;
    UTL_LOG_CONSOLE('CAM_08_01_WRITE ' || sqlcode || substr(sqlerrm, 0, 256));
    UTL_LOG('CAM_10_01_WRITE', sqlcode, substr(sqlerrm, 0, 256));
    return false;
  end;

  function CAM_11_01_WRITE
  (
    p_data in clob,
    p_id in number,
    p_sono_code in varchar2,
    p_eod_date in date,
    p_receipt_date in date,
    p_refuse_reason out varchar2
  )
  return boolean
  as
  v_xml xmltype;
  v_inn varchar2(12 char);
  v_kpp varchar2(9 char);
  v_soun_code varchar2(4 char);
  v_tax_payer_type number(1);
  v_tax_payer_id number;

  v_tax_period varchar2(2 char);
  v_fiscal_year varchar2(4 char);
  v_reg_num number;
  v_fid_journal varchar2(36 char);
  v_file_id varchar2(100 char);
  v_nds2_id number;
  pragma autonomous_transaction;
  begin
    v_xml            := xmltype(p_data);
    v_file_id        := UTL_EXTRACT_ATTRIBUTE_STRING(v_xml, '/Файл/@ИдФайл');
    v_soun_code      := UTL_EXTRACT_ATTRIBUTE_STRING(v_xml, '/Файл/Документ/@КодНО');
    v_fid_journal    := UTL_EXTRACT_ATTRIBUTE_STRING(v_xml, '/Файл/Документ/@ФидЖурнал');
    v_reg_num        := UTL_EXTRACT_ATTRIBUTE_NUM(v_xml, '/Файл/Документ/@РегНомЖурнал');
    v_tax_period     := UTL_EXTRACT_ATTRIBUTE_STRING(v_xml, '/Файл/Документ/@Период');
    v_fiscal_year    := UTL_EXTRACT_ATTRIBUTE_STRING(v_xml, '/Файл/Документ/@ОтчетГод');
    v_tax_payer_type := v_xml.existsNode('/Файл/Документ/СвНП/НПЮЛ');

    v_nds2_id := to_number(v_tax_period || v_fiscal_year || v_inn);

    v_tax_payer_type      := v_xml.existsNode('/Файл/Документ/СвНП/НПЮЛ');

    if v_tax_payer_type = 1 then
       v_inn := v_xml.extract('/Файл/Документ/СвНП/НПЮЛ/@ИННЮЛ').getStringVal();
       v_kpp := v_xml.extract('/Файл/Документ/СвНП/НПЮЛ/@КПП').getStringVal();
    else
       v_inn := v_xml.extract('/Файл/Документ/СвНП/НПФЛ/@ИННФЛ').getStringVal();
    end if;

    merge into seod_declaration seod_decl
    using (select v_inn as inn, v_kpp as kpp, v_soun_code as soun_code, v_fiscal_year as fiscal_year, v_tax_period as tax_period, v_reg_num as reg_num, v_fid_journal as fid from dual) jrnl
      on
      (
        jrnl.inn = seod_decl.inn
        and jrnl.kpp = seod_decl.kpp
        and jrnl.soun_code = seod_decl.sono_code
        and jrnl.fiscal_year = seod_decl.fiscal_year
        and jrnl.tax_period = seod_decl.tax_period
      )
  when matched then
    update set
      seod_decl.decl_reg_num = v_reg_num, seod_decl.decl_fid = v_fid_journal
    when not matched then
      insert (id, nds2_id, type, sono_code, tax_period, fiscal_year, correction_number, decl_reg_num, decl_fid, tax_payer_type, inn, kpp, insert_date, eod_date, date_receipt)
      values(p_id, v_nds2_id, 1, v_soun_code, v_tax_period, v_fiscal_year, 0, v_reg_num, v_fid_journal, v_tax_payer_type, v_inn, v_kpp, sysdate, p_eod_date, p_receipt_date);


    commit;

    UTL_LOG_CONSOLE('inserted ' || sql%rowcount || ' rows');
    UTL_LOG('CAM_11_01_WRITE', 0, 'success', v_nds2_id);
    return true;
    exception when others then
    rollback;
    UTL_LOG_CONSOLE('CAM_08_01_WRITE ' || sqlcode || substr(sqlerrm, 0, 256));
    UTL_LOG('CAM_11_01_WRITE', sqlcode, substr(sqlerrm, 0, 256));
    return false;

    return false;
  end;

procedure P$PROCESS_NEW_SEOD_DATA
(
     p_info_type varchar2,
     p_info_type_name varchar2,
     p_date_eod date,
     p_date_receipt date,
     p_code_nsi_sono varchar2,
     p_reg_number number,
     p_xml_data clob,
     p_out_result out boolean,
     p_out_error_text out varchar2
)
as
begin

      if p_info_type = 'CAM_NDS2_01' then
        p_out_result := CAM_01_01_WRITE(
          p_data => p_xml_data,
          p_id => p_reg_number,
          p_sono_code => p_code_nsi_sono,
          p_eod_date => p_date_eod,
          p_receipt_date => p_date_receipt,
          p_refuse_reason => p_out_error_text);
      end if;

      if p_info_type = 'CAM_NDS2_02' then
        p_out_result := CAM_02_01_WRITE(
          p_data => p_xml_data,
          p_id => p_reg_number,
          p_sono_code => p_code_nsi_sono,
          p_eod_date => p_date_eod,
          p_receipt_date => p_date_receipt,
          p_refuse_reason => p_out_error_text);
      end if;

      if p_info_type = 'CAM_NDS2_03' then
        p_out_result := CAM_03_01_WRITE(
          p_data => p_xml_data,
          p_id => p_reg_number,
          p_sono_code => p_code_nsi_sono,
          p_eod_date => p_date_eod,
          p_receipt_date => p_date_receipt,
          p_refuse_reason => p_out_error_text);
      end if;

      if p_info_type = 'CAM_NDS2_04' then
        p_out_result := CAM_04_01_WRITE(
          p_data => p_xml_data,
          p_id => p_reg_number,
          p_sono_code => p_code_nsi_sono,
          p_eod_date => p_date_eod,
          p_receipt_date => p_date_receipt,
          p_refuse_reason => p_out_error_text);
      end if;

      if p_info_type = 'CAM_NDS2_05' then
        p_out_result := CAM_05_01_WRITE(
          p_data => p_xml_data,
          p_id => p_reg_number,
          p_sono_code => p_code_nsi_sono,
          p_eod_date => p_date_eod,
          p_receipt_date => p_date_receipt,
          p_refuse_reason => p_out_error_text);
      end if;

      if p_info_type = 'CAM_NDS2_06' then
        p_out_result := CAM_06_01_WRITE(
          p_data => p_xml_data,
          p_id => p_reg_number,
          p_sono_code => p_code_nsi_sono,
          p_eod_date => p_date_eod,
          p_receipt_date => p_date_receipt,
          p_refuse_reason => p_out_error_text);
      end if;

      if p_info_type = 'CAM_NDS2_07' then
        p_out_result := CAM_07_01_WRITE(
          p_data => p_xml_data,
          p_id => p_reg_number,
          p_sono_code => p_code_nsi_sono,
          p_eod_date => p_date_eod,
          p_receipt_date => p_date_receipt,
          p_refuse_reason => p_out_error_text);
      end if;

      if p_info_type = 'CAM_NDS2_08' then
        p_out_result := CAM_08_01_WRITE(
          p_data => p_xml_data,
          p_id => p_reg_number,
          p_sono_code => p_code_nsi_sono,
          p_eod_date => p_date_eod,
          p_receipt_date => p_date_receipt,
          p_refuse_reason => p_out_error_text);
      end if;

      if p_info_type = 'CAM_NDS2_09' then
        p_out_result := CAM_09_01_WRITE(
          p_data => p_xml_data,
          p_id => p_reg_number,
          p_sono_code => p_code_nsi_sono,
          p_eod_date => p_date_eod,
          p_receipt_date => p_date_receipt,
          p_refuse_reason => p_out_error_text);
      end if;

      if p_info_type = 'CAM_NDS2_10' then
        p_out_result := CAM_10_01_WRITE(
          p_data => p_xml_data,
          p_id => p_reg_number,
          p_sono_code => p_code_nsi_sono,
          p_eod_date => p_date_eod,
          p_receipt_date => p_date_receipt,
          p_refuse_reason => p_out_error_text);
      end if;

      if p_info_type = 'CAM_NDS2_11' then
        p_out_result := CAM_11_01_WRITE(
          p_data => p_xml_data,
          p_id => p_reg_number,
          p_sono_code => p_code_nsi_sono,
          p_eod_date => p_date_eod,
          p_receipt_date => p_date_receipt,
          p_refuse_reason => p_out_error_text);
      end if;
end;
end;
/
