﻿begin
  for line in (select max(1) from all_objects where owner='I$NDS2' and object_name = 'SEOD_INCOMING')
  loop
    execute immediate 'DROP TABLE I$NDS2.SEOD_INCOMING';
  end loop;
  exception when others then null;
end;
/

CREATE GLOBAL TEMPORARY TABLE I$NDS2.SEOD_INCOMING 
   (
       EOD_ID NUMBER,
       INFO_TYPE VARCHAR2(11 CHAR), 
       INFO_TYPE_NAME VARCHAR2(100 CHAR), 
       DATE_EOD DATE, 
       DATE_RECEIPT DATE, 
       CODE_NSI_SONO VARCHAR2(4 CHAR), 
       REG_NUMBER NUMBER, 
       XML_DATA CLOB
   ) ON COMMIT DELETE ROWS;
 
COMMENT ON TABLE I$NDS2.SEOD_INCOMING  IS 'Временная таблица для хранения данных поступающих из СЭОД. Используется из-за необходимости передачи данных типа CLOB в схему NDS2_MRR_USER';
COMMENT ON COLUMN I$NDS2.SEOD_INCOMING.INFO_TYPE IS 'Тип входящего потока. Определяет формат файла обмена данными.';
COMMENT ON COLUMN I$NDS2.SEOD_INCOMING.CODE_NSI_SONO IS 'Код ТНО из справочника налоговых органов';
COMMENT ON COLUMN I$NDS2.SEOD_INCOMING.XML_DATA IS 'Содержимое Xml файла обмена данными.';
COMMENT ON COLUMN I$NDS2.SEOD_INCOMING.EOD_ID IS 'Идентификатор поступившей записи.';
