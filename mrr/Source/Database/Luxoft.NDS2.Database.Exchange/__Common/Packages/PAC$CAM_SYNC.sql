﻿create or replace package I$NDS2.PAC$CAM_SYNC
  as

  procedure ACCEPT
    (
      P_OBJ_ID IN NUMBER,
      P_OBJ_TYPE_ID IN NUMBER
    );

  procedure DECLINE
    (
      P_OBJ_ID IN NUMBER,
      P_OBJ_TYPE_ID IN NUMBER,
    P_MSG IN VARCHAR2 := NULL
    );

  procedure PROCESS_NEW_DATA;

end;
/
create or replace package body I$NDS2.PAC$CAM_SYNC
as
submitFaultStatus constant number:=9;

procedure UTL_LOG
  (
    v_source varchar2,
    v_code varchar2 := 0,
    v_msg varchar2 := null,
    v_entity_id varchar2 := null
  )
as
pragma autonomous_transaction;
begin
 insert into system_log@nds2dbext(id, type, site, entity_id, message_code, message, log_date)
 values(seq_sys_log_id.nextval@nds2dbext, 0, v_source, v_entity_id, v_code, v_msg, sysdate);
 commit;
exception when others then
  rollback;
end;

procedure CHANGE_DOC_STATUS
(
  p_doc_id number,
  p_status number
)
as
begin

  if(p_status = submitFaultStatus) then
	  update doc@nds2dbext
	  set status = submitFaultStatus 
		 ,status_date = trunc(sysdate) 
		 ,seod_send_date = trunc(sysdate) 
		 ,fault_send_date = trunc(sysdate) 
	  where doc_id = p_doc_id;
  else
	  update doc@nds2dbext
	  set status = p_status 
		 ,status_date = trunc(sysdate) 
		 ,seod_send_date = trunc(sysdate) 
	  where doc_id = p_doc_id;
  end if;	
  
  update bank_account_request@nds2dbext
  set status_code = 4
  where doc_id = p_doc_id;

  insert into doc_status_history@nds2dbext(doc_id, status, status_date)
  values(p_doc_id, p_status, sysdate);

  commit;
end;

procedure CHANGE_DECL_ANNULMENT_STATUS
(
	p_annulment_id number,
	p_status_id number
)
as 
begin
	update declaration_annulment@nds2dbext
	set send_status_id = p_status_id
	where id = p_annulment_id;
	
	commit;
end;

procedure ACCEPT
(
  P_OBJ_ID IN NUMBER,
  P_OBJ_TYPE_ID IN NUMBER
)
as
pragma autonomous_transaction;
begin
  if (p_obj_type_id = 6) then
	CHANGE_DECL_ANNULMENT_STATUS(p_obj_id, 2);
  else
    CHANGE_DOC_STATUS(P_OBJ_ID, 2);
    UTL_REFRESH_DOC_DEADLINE_DATE@nds2dbext(P_OBJ_ID);
    commit;
  end if;

  exception when others then
	UTL_LOG('I$NDS2.PAC$CAM_SYNC::ACCEPT:', P_OBJ_ID, substr(sqlerrm, 1, 256));
commit;
end;

procedure DECLINE
(
  P_OBJ_ID IN NUMBER,
  P_OBJ_TYPE_ID IN NUMBER,
  P_MSG IN VARCHAR2 := NULL
)
as
pragma autonomous_transaction;
begin
  if (p_obj_type_id = 6) then
	CHANGE_DECL_ANNULMENT_STATUS(p_obj_id, 3);
  else
	CHANGE_DOC_STATUS(P_OBJ_ID, 9);
	UTL_LOG('I$NDS2.PAC$CAM_SYNC::DECLINE:', P_OBJ_ID, P_MSG);
  end if;
end;

procedure PROCESS_NEW_DATA
as
v_session_id number;
begin
  insert into I$NDS2.SEOD_INCOMING
    (
        EOD_ID,
        INFO_TYPE,
        INFO_TYPE_NAME,
        DATE_EOD,
        DATE_RECEIPT,
        CODE_NSI_SONO,
        REG_NUMBER,
        XML_DATA
    )
    select
       t.id,
       t.info_type,
       t.info_type_name,
       t.date_eod,
       t.date_receipt,
       t.code_nsi_sono,
       t.reg_number,
       t.xml_data
    from I$CAM.V$nds2_Outgoing t;

  if sql%rowcount > 0 then
    v_session_id := SEQ_SYNC_SESSION_ID.NEXTVAL;

    insert into SEOD_INCOMING@NDS2DBSEOD
    (
      session_id,
      eod_id,
      info_type,
      info_type_name,
      date_eod,
      date_receipt,
      code_nsi_sono,
      reg_number,
      xml_data,
      processing_result
    )
    select
        v_session_id,
        EOD_ID,
        INFO_TYPE,
        INFO_TYPE_NAME,
        DATE_EOD,
        DATE_RECEIPT,
        CODE_NSI_SONO,
        REG_NUMBER,
        XML_DATA,
        0
    from I$NDS2.SEOD_INCOMING;

    for line in (select EOD_ID from SEOD_INCOMING) loop
      I$CAM.PAC$NDS2.P$ACCEPT(VP_ID => line.eod_id);
    end loop;
    
    execute immediate 'truncate table I$NDS2.SEOD_INCOMING';

    PAC$ASYNC_SCHEDULER.P$SUBMIT_PROCESS_SEOD_STREAM@NDS2DBEXT(v_session_id);
  end if;
commit;
exception when others then
  rollback;
   UTL_LOG('PROCESS_NEW_DATA:', sqlcode,substr(sqlerrm, 1, 256));
end;
end;
/
