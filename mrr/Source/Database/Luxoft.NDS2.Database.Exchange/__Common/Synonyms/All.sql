﻿begin
  for line in (select max(1) from all_objects where owner='I$NDS2' and object_name = 'SEQ_HIST_EXPLAIN_REPLY' and object_type = 'SYNONYM')
  loop
    execute immediate 'DROP SYNONYM I$NDS2.SEQ_HIST_EXPLAIN_REPLY';
  end loop;
  exception when others then null;
end;
/
begin
  for line in (select max(1) from all_objects where owner='I$NDS2' and object_name = 'SEQ_SEOD_KNP' and object_type = 'SYNONYM')
  loop
    execute immediate 'DROP SYNONYM I$NDS2.SEQ_SEOD_KNP';
  end loop;
  exception when others then null;
end;
/
begin
  for line in (select max(1) from all_objects where owner='I$NDS2' and object_name = 'SEQ_EXPLAIN_REPLY' and object_type = 'SYNONYM')
  loop
    execute immediate 'DROP SYNONYM I$NDS2.SEQ_EXPLAIN_REPLY';
  end loop;
  exception when others then null;
end;
/
begin
  for line in (select max(1) from all_objects where owner='I$NDS2' and object_name = 'SEQ_SYS_LOG_ID' and object_type = 'SYNONYM')
  loop
    execute immediate 'DROP SYNONYM I$NDS2.SEQ_SYS_LOG_ID';
  end loop;
  exception when others then null;
end;
/
begin
  for line in (select max(1) from all_objects where owner='I$NDS2' and object_name = 'SEOD_DATA_QUEUE' and object_type = 'SYNONYM')
  loop
    execute immediate 'DROP SYNONYM I$NDS2.SEOD_DATA_QUEUE';
  end loop;
  exception when others then null;
end;
/
begin
  for line in (select max(1) from all_objects where owner='I$NDS2' and object_name = 'SEOD_DECLARATION' and object_type = 'SYNONYM')
  loop
    execute immediate 'DROP SYNONYM I$NDS2.SEOD_DECLARATION';
  end loop;
  exception when others then null;
end;
/
begin
  for line in (select max(1) from all_objects where owner='I$NDS2' and object_name = 'SEOD_EXPLAIN_REPLY' and object_type = 'SYNONYM')
  loop
    execute immediate 'DROP SYNONYM I$NDS2.SEOD_EXPLAIN_REPLY';
  end loop;
  exception when others then null;
end;
/
begin
  for line in (select max(1) from all_objects where owner='I$NDS2' and object_name = 'SEOD_KNP' and object_type = 'SYNONYM')
  loop
    execute immediate 'DROP SYNONYM I$NDS2.SEOD_KNP';
  end loop;
  exception when others then null;
end;
/
begin
  for line in (select max(1) from all_objects where owner='I$NDS2' and object_name = 'SEOD_KNP_ACT' and object_type = 'SYNONYM')
  loop
    execute immediate 'DROP SYNONYM I$NDS2.SEOD_KNP_ACT';
  end loop;
  exception when others then null;
end;
/
begin
  for line in (select max(1) from all_objects where owner='I$NDS2' and object_name = 'SEOD_KNP_DECISION' and object_type = 'SYNONYM')
  loop
    execute immediate 'DROP SYNONYM I$NDS2.SEOD_KNP_DECISION';
  end loop;
  exception when others then null;
end;
/
begin
  for line in (select max(1) from all_objects where owner='I$NDS2' and object_name = 'SEOD_KNP_DECISION_TYPE' and object_type = 'SYNONYM')
  loop
    execute immediate 'DROP SYNONYM I$NDS2.SEOD_KNP_DECISION_TYPE';
  end loop;
  exception when others then null;
end;
/
begin
  for line in (select max(1) from all_objects where owner='I$NDS2' and object_name = 'SEOD_PROLONG_DECISION' and object_type = 'SYNONYM')
  loop
    execute immediate 'DROP SYNONYM I$NDS2.SEOD_PROLONG_DECISION';
  end loop;
  exception when others then null;
end;
/
begin
  for line in (select max(1) from all_objects where owner='I$NDS2' and object_name = 'SEOD_RECLAIM_ORDER' and object_type = 'SYNONYM')
  loop
    execute immediate 'DROP SYNONYM I$NDS2.SEOD_RECLAIM_ORDER';
  end loop;
  exception when others then null;
end;
/
begin
  for line in (select max(1) from all_objects where owner='I$NDS2' and object_name = 'SYSTEM_LOG' and object_type = 'SYNONYM')
  loop
    execute immediate 'DROP SYNONYM I$NDS2.SYSTEM_LOG';
  end loop;
  exception when others then null;
end;
/
begin
  for line in (select max(1) from all_objects where owner='I$NDS2' and object_name = 'HIST_EXPLAIN_REPLY_STATUS' and object_type = 'SYNONYM')
  loop
    execute immediate 'DROP SYNONYM I$NDS2.HIST_EXPLAIN_REPLY_STATUS';
  end loop;
  exception when others then null;
end;
/
begin
  for line in (select max(1) from all_objects where owner='I$NDS2' and object_name = 'DOC_STATUS_HISTORY' and object_type = 'SYNONYM')
  loop
    execute immediate 'DROP SYNONYM I$NDS2.DOC_STATUS_HISTORY';
  end loop;
  exception when others then null;
end;
/
begin
  for line in (select max(1) from all_objects where owner='I$NDS2' and object_name = 'DOC_STATUS' and object_type = 'SYNONYM')
  loop
    execute immediate 'DROP SYNONYM I$NDS2.DOC_STATUS';
  end loop;
  exception when others then null;
end;
/
begin
  for line in (select max(1) from all_objects where owner='I$NDS2' and object_name = 'DOC_TYPE' and object_type = 'SYNONYM')
  loop
    execute immediate 'DROP SYNONYM I$NDS2.DOC_TYPE';
  end loop;
  exception when others then null;
end;
/
begin
  for line in (select max(1) from all_objects where owner='I$NDS2' and object_name = 'DOC' and object_type = 'SYNONYM')
  loop
    execute immediate 'DROP SYNONYM I$NDS2.DOC';
  end loop;
  exception when others then null;
end;
/
begin
  for line in (select max(1) from all_objects where owner='I$NDS2' and object_name = 'UTL_CLOSE_DOCUMENT' and object_type = 'SYNONYM')
  loop
    execute immediate 'DROP SYNONYM I$NDS2.UTL_CLOSE_DOCUMENT';
  end loop;
  exception when others then null;
end;
/
begin
  for line in (select max(1) from all_objects where owner='I$NDS2' and object_name = 'UTL_ADD_WORK_DAYS' and object_type = 'SYNONYM')
  loop
    execute immediate 'DROP SYNONYM I$NDS2.UTL_ADD_WORK_DAYS';
  end loop;
  exception when others then null;
end;
/
begin
  for line in (select max(1) from all_objects where owner='I$NDS2' and object_name = 'UTL_GET_CONFIGURATION_NUMBER' and object_type = 'SYNONYM')
  loop
    execute immediate 'DROP SYNONYM I$NDS2.UTL_GET_CONFIGURATION_NUMBER';
  end loop;
  exception when others then null;
end;
/
begin
  for line in (select max(1) from all_objects where owner='I$NDS2' and object_name = 'V_EGRN_IP' and object_type = 'SYNONYM')
  loop
    execute immediate 'DROP SYNONYM I$NDS2.V_EGRN_IP';
  end loop;
  exception when others then null;
end;
/
begin
  for line in (select max(1) from all_objects where owner='I$NDS2' and object_name = 'V_EGRN_UL' and object_type = 'SYNONYM')
  loop
    execute immediate 'DROP SYNONYM I$NDS2.V_EGRN_UL';
  end loop;
  exception when others then null;
end;
/
begin
  for line in (select max(1) from all_objects where owner='I$NDS2' and object_name = 'V_REPORT_EOD' and object_type = 'SYNONYM')
  loop
    execute immediate 'DROP SYNONYM I$NDS2.V_REPORT_EOD';
  end loop;
  exception when others then null;
end;
/
begin
  for line in (select max(1) from all_objects where owner='I$NDS2' and object_name = 'UTL_REFRESH_DOC_DEADLINE_DATE' and object_type = 'SYNONYM')
  loop
    execute immediate 'DROP SYNONYM I$NDS2.UTL_REFRESH_DOC_DEADLINE_DATE';
  end loop;
  exception when others then null;
end;
/
