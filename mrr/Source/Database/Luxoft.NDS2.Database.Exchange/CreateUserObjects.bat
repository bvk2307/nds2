﻿@echo OFF
set userName=NDS2_INSTALL
set userPas=NDS2_INSTALL
set SidName=NDS2

sqlplus %userName%/%userPas%@%SidName% @2.I$NDS2.SchemaObjects.Rel_6.0.1.1.sql
sqlplus %userName%/%userPas%@%SidName% @3.I$NDS2.SchemaObjects.Rel_6.0.7.1.sql
sqlplus %userName%/%userPas%@%SidName% @4.I$NDS2.SchemaObjects.Rel_8.3.sql

sqlplus %userName%/%userPas%@%SidName% @9.I$NDS2.SchemaObjects.Common.sql

pause
