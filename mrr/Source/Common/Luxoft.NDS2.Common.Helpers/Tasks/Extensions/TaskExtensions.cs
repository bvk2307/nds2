﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using FLS.CommonComponents.Lib.Exceptions.Extensions;
using Luxoft.NDS2.Common.Helpers.Dataflow.Extensions;

namespace Luxoft.NDS2.Common.Helpers.Tasks.Extensions
{
    /// <summary> Extension methods for <see cref="Task"/>. </summary>
    public static class TaskExtensions
    {
        public static bool IsRanToCompletion( this Task task )
        {
            return task.Status == TaskStatus.RanToCompletion;
        }

        /// <summary> Propagates task completion like as <see cref="DataflowLinkOptions.PropagateCompletion"/>. </summary>
        /// <param name="sourceTask"> A source task to link from. </param>
        /// <param name="linkedToBlock"> A target block to link to. </param>
        public static void PropagateCompletionTo( this Task sourceTask, IDataflowBlock linkedToBlock )
        {
            sourceTask.ContinueWith( sourceTaskPar => linkedToBlock.CompleteOrFault( sourceTaskPar.Exception ),
                                     CancellationToken.None, TaskContinuationOptions.ExecuteSynchronously, TaskScheduler.Default );
        }

        [Conditional( "DEBUG" )]
        public static void DebugWriteLineAfterCompletion( this Task sourceTask, string linePrefix, string linePostfix = null )
        {
            sourceTask.ContinueWith(
                taskPrev =>
                {
                    string text = string.Join( " ", linePrefix, taskPrev.GetCompletedText() );
                    if ( !String.IsNullOrWhiteSpace( linePostfix ) )
                        text = String.Join( " ", text, linePostfix );

                    Debug.WriteLine( text );
                },
                TaskContinuationOptions.ExecuteSynchronously );
        }

        /// <summary> </summary>
        /// <param name="taskPrev"></param>
        /// <returns></returns>
        public static string GetCompletedText( this Task taskPrev )
        {
            string text = null;

            if ( taskPrev.IsFaulted )
            {
                text = "is faulted";
                Exception exception = null;
                if ( taskPrev.Exception != null )
                    exception = taskPrev.Exception.Uncover();
                if ( exception != null )
                    text = String.Join( " ", text, "with", exception.ToStringEx( noStack: true ) );
            }
            else if ( taskPrev.IsCanceled )
            {
                text = "is canceled";
            }
            else if ( taskPrev.IsCompleted )
            {
                text = "is completed";
            }
            else
            {
                text = "is not completed";
            }
            return text;
        }
    }
}