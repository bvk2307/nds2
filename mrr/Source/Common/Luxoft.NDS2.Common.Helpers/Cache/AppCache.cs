﻿using System;
using System.Runtime.Caching;

namespace Luxoft.NDS2.Common.Helpers.Cache
{
    /// <summary> The process memory cache. </summary>
    public sealed class AppCache
    {
        private static MemoryCache singletonCach = null;
        private static readonly Lazy<AppCache> lzSingletonAppCache = new Lazy<AppCache>( () => new AppCache() );

        private AppCache() { }

        /// <summary> The signleton instance of application cache. </summary>
        public static AppCache Current
        {
            get { return AppCache.lzSingletonAppCache.Value; }
        }

        private static MemoryCache SingletonCache
        {
            get
            {
                if ( AppCache.singletonCach == null )
                {
                    AppCache.singletonCach = MemoryCache.Default;
                }
                return AppCache.singletonCach;
            }
        }

        public long Trim( int percent )
        {
            return SingletonCache.Trim( percent );
        }

        /// <summary> Existence check for a single item </summary>
        /// <param name="key"></param>
        /// <param name="regionName"></param>
        /// <returns></returns>
        public bool Contains<T>( string key, string regionName = null ) where T : class
        {
            return ( GetInternal<T>( key, regionName ) != null );
        }

        public bool Add<T>( AppCacheItem<T> item, CacheItemPolicy policy ) where T : class
        {
            AppCacheItem<T> existingEntry = AddOrGetExisting( item, policy );
            return ( existingEntry == null || existingEntry.Value == null );
        }

        public T AddOrGetExisting<T>( string key, T value, DateTimeOffset absoluteExpiration, string regionName = null ) where T : class
        {
            if ( regionName != null )
                throw new NotSupportedException( string.Format( "Cache regions are not supported by {0}", this.GetType().Name ) );

            CacheItemPolicy policy = new CacheItemPolicy();
            policy.AbsoluteExpiration = absoluteExpiration;
            return AddOrGetExistingInternal( key, value, policy );
        }

        public AppCacheItem<T> AddOrGetExisting<T>( AppCacheItem<T> item, CacheItemPolicy policy ) where T : class
        {
            if ( item == null )
                throw new ArgumentNullException( "item" );

            return new AppCacheItem<T>( item.Key, AddOrGetExistingInternal( item.Key, item.Value, policy ) );
        }

        public T AddOrGetExisting<T>( string key, T value, CacheItemPolicy policy, string regionName = null ) where T : class
        {
            if ( regionName != null )
                throw new NotSupportedException( string.Format( "Cache regions are not supported by {0}", this.GetType().Name ) );

            return AddOrGetExistingInternal( key, value, policy );
        }

        public T Get<T>( string key, string regionName = null ) where T : class
        {
            return GetInternal<T>( key, regionName );
        }

        public AppCacheItem<T> GetCacheItem<T>( string key, string regionName = null ) where T : class
        {
            T value = GetInternal<T>( key, regionName );
            return ( value != null ) ? new AppCacheItem<T>( key, value ) : null;
        }

        public void Set<T>( string key, T value, DateTimeOffset absoluteExpiration, string regionName = null ) where T : class
        {
            if ( regionName != null )
                throw new NotSupportedException( string.Format( "Cache regions are not supported by {0}", this.GetType().Name ) );

            CacheItemPolicy policy = new CacheItemPolicy();
            policy.AbsoluteExpiration = absoluteExpiration;
            Set( key, value, policy );
        }

        public void Set<T>( AppCacheItem<T> item, CacheItemPolicy policy ) where T : class
        {
            if ( item == null )
                throw new ArgumentNullException( "item" );

            Set( item.Key, item.Value, policy );
        }

        public void Set<T>( string key, T value, CacheItemPolicy policy, string regionName = null ) where T : class
        {
            SingletonCache.Set( key, value, policy, regionName );
        }

        public T Remove<T>( string key, string regionName = null )
        {
            return (T)SingletonCache.Remove( key, regionName );
        }

        private T AddOrGetExistingInternal<T>( string key, T value, CacheItemPolicy policy ) where T : class
        {
            return (T)SingletonCache.AddOrGetExisting( key, value, policy );
        }

        private T GetInternal<T>( string key, string regionName ) where T : class
        {
            return (T)SingletonCache.Get( key, regionName );
        }
    }
}