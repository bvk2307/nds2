namespace Luxoft.NDS2.Common.Helpers.Cache
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Text;

    public class CacheKeyBuilder
    {
        public const string UniqueMark = @"<FLS{";
        private StringBuilder sbuilder;

        private CacheKeyBuilder()
        {  
        }

        public static CacheKeyBuilder Create()
        {
            return new CacheKeyBuilder();
        }
             
        /// <summary> </summary>
        /// <param name="operation"> An operation identifier. </param> 
        /// <returns></returns>
        public string WithOperation( Enum operation )
        {
            if (this.sbuilder == null)
                this.sbuilder = new StringBuilder(32);

            this.sbuilder.Append(operation).Append(UniqueMark);
            string key = this.sbuilder.ToString();

            this.sbuilder.Length = 0;
            key = this.sbuilder.Append(key.GetHashCode()).Append("!").Append(key).ToString();
            return key;
        }

        /// <summary> </summary>
        /// <param name="parts"> Cache key parts. </param>
        /// <returns></returns>
        public CacheKeyBuilder WithCacheKeyFragment(IEnumerable<string> parts)
        {
            if (this.sbuilder == null)
                this.sbuilder = new StringBuilder(128);

            foreach (var element in parts)
            {
                if (!string.IsNullOrEmpty(element))
                {
                    this.sbuilder.Append(element);
                }
            }

            return this;
        }

        /// <summary> </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="param"> Cache key part. </param>
        /// <returns></returns>
        public CacheKeyBuilder WithCacheKeyPart<T>(T param)
        {
            if (this.sbuilder == null)
                this.sbuilder = new StringBuilder(128);

            this.sbuilder.Append(this.ConvertToCacheKeyPart(param));

            return this;
        }
              
        private string ConvertToCacheKeyPart<T>(T param)
        {
            string value = null;
            var typeCode = Type.GetTypeCode(typeof(T));

            switch (typeCode)
            {
                case TypeCode.String:
                    value = param as string;
                    break;
                case TypeCode.Char:
                    value = ((char)(dynamic)param).ToString(CultureInfo.InvariantCulture);
                    break;
                case TypeCode.Byte:
                    value = ((byte)(dynamic)param).ToString("X", CultureInfo.InvariantCulture);
                    break;
                case TypeCode.Int16:
                    value = ((short)(dynamic)param).ToString("X", CultureInfo.InvariantCulture);
                    break;
                case TypeCode.Int32:
                    value = ((int)(dynamic)param).ToString("X", CultureInfo.InvariantCulture);
                    break;
                case TypeCode.Int64:
                    value = ((long)(dynamic)param).ToString("X", CultureInfo.InvariantCulture);
                    break;
                case TypeCode.UInt16:
                    value = ((ushort)(dynamic)param).ToString("X", CultureInfo.InvariantCulture);
                    break;
                case TypeCode.UInt32:
                    value = ((uint)(dynamic)param).ToString("X", CultureInfo.InvariantCulture);
                    break;
                case TypeCode.UInt64:
                    value = ((ulong)(dynamic)param).ToString("X", CultureInfo.InvariantCulture);
                    break;
                case TypeCode.Boolean:
                    value = object.Equals(param, default(T)) ? "N" : "Y";
                    break;
                case TypeCode.Decimal:
                    int[] intParts = Decimal.GetBits((decimal)(dynamic)param);
                    string[] parts = new string[intParts.Length];
                    for (int nI = 0; nI < intParts.Length; nI++)
                    {
                        parts[nI] = this.ConvertToCacheKeyPart(intParts[nI]);
                    }
                    value = string.Concat(parts);
                    break;

                    //case TypeCode.Double:
                default:
                    throw new NotSupportedException(string.Format("Type: {0} is not supported yet by AppCacheExtensions.ConvertToCacheKeyPart()", typeof(T).Name));
            }

            return value;
        }  
    }
}