﻿using System.Runtime.Caching;

namespace Luxoft.NDS2.Common.Helpers.Cache
{
    /// <summary> A cache item. </summary>
    /// <typeparam name="T"> A type of a cache value. </typeparam>
    public class AppCacheItem<T> : CacheItem where T : class
    {
        /// <summary> Initializes a new <see cref="T:System.Runtime.Caching.CacheItem"/> instance using the specified key of a cache entry. </summary>
        /// <param name="key">A unique identifier for a <see cref="T:System.Runtime.Caching.CacheItem"/> entry. </param>
        public AppCacheItem( string key ) : base( key ) { }

        /// <summary> Initializes a new <see cref="T:System.Runtime.Caching.CacheItem"/> instance using the specified key and a value of the cache entry. </summary>
        /// <param name="key">A unique identifier for a <see cref="T:System.Runtime.Caching.CacheItem"/> entry.</param><param name="value">The data for a <see cref="T:System.Runtime.Caching.CacheItem"/> entry.</param>
        public AppCacheItem( string key, T value ) : base( key, value ) { }

        /// <summary> Initializes a new <see cref="T:System.Runtime.Caching.CacheItem"/> instance using the specified key, value, and region of the cache entry. </summary>
        /// <param name="key">A unique identifier for a <see cref="T:System.Runtime.Caching.CacheItem"/> entry.</param><param name="value">The data for a <see cref="T:System.Runtime.Caching.CacheItem"/> entry.</param><param name="regionName">The name of a region in the cache that will contain the <see cref="T:System.Runtime.Caching.CacheItem"/> entry.</param>
        public AppCacheItem( string key, T value, string regionName ) : base( key, value, regionName ) { }

        /// <summary> A typed value hidding <see cref="CacheItem.Value"/> property. </summary>
        public new T Value {
            get { return (T)base.Value; }
            set { base.Value = value; } }
    }
}