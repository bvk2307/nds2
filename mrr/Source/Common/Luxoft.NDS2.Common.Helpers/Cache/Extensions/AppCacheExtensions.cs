using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Globalization;
using System.Runtime.Caching;
using System.Text;

namespace Luxoft.NDS2.Common.Helpers.Cache.Extensions
{  
    public static class AppCacheExtensions
    {
        public const string UniqueMark = @"<FLS{";

        /// <summary> </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="storage"></param>
        /// <param name="creator"></param>
        /// <param name="operation"> An operation identifier. </param>
        /// <param name="absoluteExpiration"> Doesn't expire a cache entry if it's <see cref="TimeSpan.Zero"/>. </param>
        /// <returns></returns>
        public static T AddOrGetExisting<T>( this AppCache storage, Func<T> creator, Enum operation, TimeSpan absoluteExpiration )
            where T : class
        {
            string key = BuildCacheKey( operation );

            return storage.AddOrGetExisting( key, creator, absoluteExpiration );
        }

        /// <summary> </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="storage"></param>
        /// <param name="creator"></param>
        /// <param name="operation"> An operation identifier. </param>
        /// <param name="policy"> Doesn't expire a cache entry if it's 'null'. </param>
        /// <returns></returns>
        public static T AddOrGetExisting<T>( this AppCache storage, Func<T> creator, Enum operation, CacheItemPolicy policy = null )
            where T : class
        {
            string key = BuildCacheKey( operation );

            return storage.AddOrGetExisting<T>( key, creator, policy );
        }

        /// <summary> </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="storage"></param>
        /// <param name="key"></param>
        /// <param name="creator"></param>
        /// <param name="absoluteExpiration"> Doesn't expire a cache entry if it's <see cref="TimeSpan.Zero"/>. </param>
        /// <returns></returns>
        public static T AddOrGetExisting<T>( this AppCache storage, string key, Func<T> creator, TimeSpan absoluteExpiration )
            where T : class
        {
            Contract.Ensures( Contract.Result<T>() != null );

            T value = storage.Get<T>( key );
            if ( value == null )
            {
                T valueNew = creator();
                if ( valueNew == null )
                    throw new InvalidOperationException( string.Format( "Invalid try to cache 'null' value with key: {0}", key ) );

                DateTimeOffset expiration = absoluteExpiration == TimeSpan.Zero 
                                          ? ObjectCache.InfiniteAbsoluteExpiration : DateTimeOffset.Now + absoluteExpiration;
                value = storage.AddOrGetExisting( key, valueNew, expiration ) ?? valueNew;
            }

            return value;
        }

        /// <summary> </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="storage"></param>
        /// <param name="key"></param>
        /// <param name="creator"></param>
        /// <param name="policy"> Doesn't expire a cache entry if it's 'null'. </param>
        /// <returns></returns>
        public static T AddOrGetExisting<T>( this AppCache storage, string key, Func<T> creator, CacheItemPolicy policy = null )
            where T : class
        {
            Contract.Ensures( Contract.Result<T>() != null );

            T value = storage.Get<T>( key );
            if ( value == null )
            {
                T valueNew = creator();
                if ( valueNew == null )
                    throw new InvalidOperationException( string.Format( "Invalid try to cache 'null' value with key: {0}", key ) );

                value = storage.AddOrGetExisting( key, valueNew, policy ) ?? valueNew;
            }
            return value;
        }

        /// <summary> </summary>
        /// <typeparam name="TParam1"></typeparam>
        /// <typeparam name="T"></typeparam>
        /// <param name="storage"></param>
        /// <param name="key"></param>
        /// <param name="creator"></param>
        /// <param name="absoluteExpiration"> Doesn't expire a cache entry if it's <see cref="TimeSpan.Zero"/>. </param>
        /// <param name="param1"></param>
        /// <returns></returns>
        public static T AddOrGetExisting<TParam1, T>( 
            this AppCache storage, string key, Func<TParam1, T> creator, TimeSpan absoluteExpiration, TParam1 param1 )
            where T : class
        {
            Contract.Ensures( Contract.Result<T>() != null );

            T value = storage.Get<T>( key );
            if ( value == null )
            {
                T valueNew = creator( param1 );
                if ( valueNew == null )
                    throw new InvalidOperationException( string.Format( "Invalid try to cache 'null' value with key: {0}", key ) );

                DateTimeOffset expiration = absoluteExpiration == TimeSpan.Zero 
                                          ? ObjectCache.InfiniteAbsoluteExpiration : DateTimeOffset.Now + absoluteExpiration;
                value = storage.AddOrGetExisting( key, valueNew, expiration ) ?? valueNew;
            }

            return value;
        }

        /// <summary> </summary>
        /// <typeparam name="TParam1"></typeparam>
        /// <typeparam name="T"></typeparam>
        /// <param name="storage"></param>
        /// <param name="key"></param>
        /// <param name="creator"></param>
        /// <param name="param1"></param>
        /// <param name="policy"> Doesn't expire a cache entry if it's 'null'. </param>
        /// <returns></returns>
        public static T AddOrGetExisting<TParam1, T>( 
            this AppCache storage, string key, Func<TParam1, T> creator, TParam1 param1, CacheItemPolicy policy = null )
            where T : class
        {
            Contract.Ensures( Contract.Result<T>() != null );

            T value = storage.Get<T>( key );
            if ( value == null )
            {
                T valueNew = creator( param1 );
                if ( valueNew == null )
                    throw new InvalidOperationException( string.Format( "Invalid try to cache 'null' value with key: {0}", key ) );

                value = storage.AddOrGetExisting( key, valueNew, policy ) ?? valueNew;
            }
            return value;
        }

        /// <summary> </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="storage"></param>
        /// <param name="operation"> An operation identifier. </param>
        public static void Remove<T>( this AppCache storage, Enum operation ) where T : class
        {
            string key = BuildCacheKey( operation );
            storage.Remove<T>( key );
        }

        #region Cache key building

        public static string BuildCacheKey(Enum operation)
        {
            return CacheKeyBuilder
                .Create()
                .WithOperation(operation);
        }
          
        #endregion Cache key building
   }
}