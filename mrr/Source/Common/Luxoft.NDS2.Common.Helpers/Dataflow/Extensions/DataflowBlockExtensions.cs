﻿using System;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using FLS.CommonComponents.Lib.Exceptions.Extensions;
using Luxoft.NDS2.Common.Helpers.Tasks.Extensions;

namespace Luxoft.NDS2.Common.Helpers.Dataflow.Extensions
{
    /// <summary> Extension methods for <see cref="IDataflowBlock"/> and its descendants. </summary>
    public static class DataflowBlockExtensions
    {
        /// <summary> Provides a <see cref="T:System.Threading.Tasks.Task`1" /> that asynchronously monitors the source for available output. </summary>
        /// <typeparam name="TOutput">Specifies the type of data contained in the source.</typeparam>
        /// <param name="source">The source to monitor.</param>
        /// <returns>
        /// A <see cref="T:System.Threading.Tasks.Task`1" /> that informs of whether and when
        /// more output is available.  When the task completes, if its <see cref="P:System.Threading.Tasks.Task`1.Result" /> is true, more output
        /// is available in the source (though another consumer of the source may retrieve the data).
        /// If it returns false, more output is not and will never be available, due to the source
        /// completing prior to output being available.
        /// </returns>
        public static Task<bool> OutputBeforeAvailableAsync<TOutput>( this ISourceBlock<TOutput> source )
        {
            return source.OutputBeforeAvailableAsync<TOutput>( CancellationToken.None );
        }

        /// <summary> Provides a <see cref="T:System.Threading.Tasks.Task`1" /> that asynchronously monitors the source for available output. </summary>
        /// <typeparam name="TOutput">Specifies the type of data contained in the source.</typeparam>
        /// <param name="source">The source to monitor.</param>
        /// <param name="cancellationToken">The cancellation token with which to cancel the asynchronous operation.</param>
        /// <returns>
        /// A <see cref="T:System.Threading.Tasks.Task`1" /> that informs of whether and when
        /// more output is available.  When the task completes, if its <see cref="P:System.Threading.Tasks.Task`1.Result" /> is true, more output
        /// is available in the source (though another consumer of the source may retrieve the data).
        /// If it returns false, more output is not and will never be available, due to the source
        /// completing prior to output being available.
        /// </returns>
        public static Task<bool> OutputBeforeAvailableAsync<TOutput>( this ISourceBlock<TOutput> source, CancellationToken cancellationToken )
        {
            if ( source == null )
                throw new ArgumentNullException( "source" );

            if ( cancellationToken.IsCancellationRequested )
                return DataflowBlockExtensions.CreateTaskFromCancellation<bool>( cancellationToken );
            DataflowBlockExtensions.OutputAvailableAsyncTarget<TOutput> target = new DataflowBlockExtensions.OutputAvailableAsyncTarget<TOutput>();
            try
            {
                target.m_unlinker = source.LinkTo( (ITargetBlock<TOutput>)target, DataflowBlockExtensions.PrependedUnlinkAfterOneAndPropagateCompletion );
                if ( target.Task.IsCompleted )
                    return target.Task;
                if ( cancellationToken.CanBeCanceled )
                    target.m_ctr = cancellationToken.Register( DataflowBlockExtensions.OutputAvailableAsyncTarget<TOutput>.s_cancelAndUnlink, (object)target );
                return target.Task.ContinueWith<bool>( 
                    (Func<Task<bool>, bool>)( antecedent => DataflowBlockExtensions.OutputAvailableAsyncTarget<TOutput>.s_handleCompletion( antecedent, (object)target ) ), 
                    CancellationToken.None, DataflowBlockExtensions.GetContinuationOptions( TaskContinuationOptions.None ) | TaskContinuationOptions.NotOnCanceled, TaskScheduler.Default );
            }
            catch ( Exception ex )
            {
                target.TrySetException( ex );
                target.AttemptThreadSafeUnlink();
                return target.Task;
            }
        }

        [Pure]
        public static bool IsCompleted( this IDataflowBlock dataflowBlock )
        {
            return dataflowBlock.Completion.IsCompleted;
        }

        public static void CompleteOrFaultFrom( this IDataflowBlock linkedBlock, IDataflowBlock sourceBlock )
        {
            linkedBlock.CompleteOrFaultFrom( sourceBlock.Completion );
        }

        public static void CompleteOrFaultFrom( this IDataflowBlock linkedBlock, Task sourceTask )
        {
            Contract.Requires( sourceTask.IsCompleted );

            Exception exception = sourceTask.Exception;

            linkedBlock.CompleteOrFault( exception );
        }

        public static void CompleteOrFault( this IDataflowBlock dataflowBlock, Exception exception )
        {
            //dataflowBlock.DebugWriteLineOnCompleting( ">>>--", exception );

            if ( exception == null )
                dataflowBlock.Complete();   //call of Complete() is ignored if a block has been completed already
            else
                dataflowBlock.Fault( exception );
        }

        /// <summary> Propagates task fault like as <see cref="DataflowLinkOptions.PropagateCompletion"/> propagates task completion. </summary>
        /// <param name="linkedBlock"> A target block to link from. </param>
        /// <param name="sourceBlock"> A source block to link to. </param>
        public static void PropagateFaultToIfIsNotCompletedYet( this IDataflowBlock linkedBlock, IDataflowBlock sourceBlock )
        {
            linkedBlock.Completion.ContinueWith( 
                sourceTaskPar =>
                {
                    if ( !sourceBlock.IsCompleted() )	//apopov 26.7.2016	//TODO!!!   //replace by a specialized abort exception
                        sourceBlock.Fault( (Exception)sourceTaskPar.Exception ?? new ApplicationException( string.Format( "Completion of following block: {0} is earlier than completion of preceding block: {1}", linkedBlock, sourceBlock ) ) );
                },
                CancellationToken.None, TaskContinuationOptions.ExecuteSynchronously, TaskScheduler.Default );
        }

        [Conditional( "DEBUG" )]
        public static void DebugWriteLineAfterCompletion( this IDataflowBlock dataflowBlock, string linePrefix, string linePostfix = null )
        {
            dataflowBlock.Completion.ContinueWith(
                taskPrev =>
                {
                    string text = string.Join( " ", linePrefix, dataflowBlock, taskPrev.GetCompletedText() );
                    if ( !string.IsNullOrWhiteSpace( linePostfix ) )
                        text = string.Join( " ", text, linePostfix );

                    Debug.WriteLine( text );
                },
                TaskContinuationOptions.ExecuteSynchronously );
        }

        [Conditional( "DEBUG" )]
        public static void DebugWriteLineOnCompleting( this IDataflowBlock dataflowBlock, string linePrefix, Exception exception = null )
        {
            string text = exception == null ? "is completing or canceling" : exception.Uncover().ToStringEx( "is faulting with:", noStack: true );
            text = string.Join( " ", linePrefix, dataflowBlock, text );

            Debug.WriteLine( text );
        }

        /// <summary>Creates a task canceled with the specified cancellation token.</summary>
        /// <typeparam name="TResult">Specifies the type of the result for this task.</typeparam>
        /// <returns>The canceled task.</returns>
        internal static Task<TResult> CreateTaskFromCancellation<TResult>(CancellationToken cancellationToken)
        {
          return new Task<TResult>(DataflowBlockExtensions.CachedGenericDelegates<TResult>.DefaultTResultFunc, cancellationToken);
        }

        /// <summary>Gets the options to use for tasks.</summary>
        /// <param name="isReplacementReplica">If this task is being created to replace another.</param>
        /// <remarks>
        /// These options should be used for all tasks that have the potential to run user code or
        /// that are repeatedly spawned and thus need a modicum of fair treatment.
        /// </remarks>
        /// <returns>The options to use.</returns>
        internal static TaskCreationOptions GetCreationOptionsForTask( bool isReplacementReplica = false )
        {
            TaskCreationOptions taskCreationOptions = TaskCreationOptions.None;
            if ( isReplacementReplica )
                taskCreationOptions |= TaskCreationOptions.PreferFairness;
            return taskCreationOptions;
        }

        /// <summary> Disposes of the provided CancellationTokenRegistration in a manner that's safe from ObjectDisposedException. </summary>
        /// <param name="registration">The token registraiton to dispose.</param>
        internal static void SafeDisposeTokenRegistration( CancellationTokenRegistration registration )
        {
            try
            {
                registration.Dispose();
            }
            catch ( ObjectDisposedException )
            {
            }
        }

        /// <summary>Gets the options to use for continuation tasks.</summary>
        /// <param name="toInclude">Any options to include in the result.</param>
        /// <returns>The options to use.</returns>
        internal static TaskContinuationOptions GetContinuationOptions( TaskContinuationOptions toInclude = TaskContinuationOptions.None )
        {
            return toInclude;
        }

        /// <summary>Gets the completion task of a block, and protects against common cases of the completion task not being implemented or supported.</summary>
        /// <param name="block">The block.</param>
        /// <returns>The completion task, or null if the block's completion task is not implemented or supported.</returns>
        internal static Task GetPotentiallyNotSupportedCompletionTask( IDataflowBlock block )
        {
            try
            {
                return block.Completion;
            }
            catch ( NotImplementedException )
            {
            }
            catch ( NotSupportedException )
            {
            }
            return (Task)null;
        }

        /// <summary>Gets an ID for the dataflow block.</summary>
        /// <param name="block">The dataflow block.</param>
        /// <returns>An ID for the dataflow block.</returns>
        internal static int GetBlockId(IDataflowBlock block)
        {
          Task supportedCompletionTask = DataflowBlockExtensions.GetPotentiallyNotSupportedCompletionTask(block);
          if (supportedCompletionTask == null)
            return 0;
          return supportedCompletionTask.Id;
        }

        /// <summary>Gets the name for the specified block, suitable to be rendered in a debugger window.</summary>
        /// <param name="block">The block for which a name is needed.</param>
        /// <param name="options">
        /// The options to use when rendering the name. If no options are provided, the block's name is used directly.
        /// </param>
        /// <returns>The name of the object.</returns>
        /// <remarks>This is used from DebuggerDisplay attributes.</remarks>
        internal static string GetNameForDebugger( IDataflowBlock block, DataflowBlockOptions options = null )
        {
            if ( block == null )
                return string.Empty;
            string name = block.GetType().Name;
            if ( options == null )
                return name;
            int blockId = DataflowBlockExtensions.GetBlockId(block);
            try
            {
                return string.Format( options.NameFormat, (object)name, (object)blockId );
            }
            catch ( Exception ex )
            {
                return ex.Message;
            }
        }

        /// <summary>Static class used to cache generic delegates the C# compiler doesn't cache by default.</summary>
        /// <remarks>Without this, we end up allocating the generic delegate each time the operation is used.</remarks>
        private static class CachedGenericDelegates<T>
        {
            /// <summary>A function that returns the default value of T.</summary>
            internal static readonly Func<T> DefaultTResultFunc = (Func<T>) (() => default (T));
        }

        /// <summary>Provides a target used in OutputAvailableAsync operations.</summary>
        /// <typeparam name="T">Specifies the type of data in the data source being checked.</typeparam>
        //[DebuggerDisplay( "{DebuggerDisplayContent,nq}" )]
        private sealed class OutputAvailableAsyncTarget<T> : TaskCompletionSource<bool>, ITargetBlock<T>    //, IDebuggerDisplay
        {
            /// <summary>
            /// Cached continuation delegate that unregisters from cancellation and
            /// marshals the antecedent's result to the return value.
            /// </summary>
            internal static readonly Func<Task<bool>, object, bool> s_handleCompletion = (Func<Task<bool>, object, bool>) ((antecedent, state) =>
                {
                    DataflowBlockExtensions.SafeDisposeTokenRegistration((state as DataflowBlockExtensions.OutputAvailableAsyncTarget<T>).m_ctr);
                    return antecedent.Result;
                });
            /// <summary>
            /// Cached delegate that cancels the target and unlinks the target from the source.
            /// Expects an OutputAvailableAsyncTarget as the state argument.
            /// </summary>
            internal static readonly Action<object> s_cancelAndUnlink = new Action<object>(DataflowBlockExtensions.OutputAvailableAsyncTarget<T>.CancelAndUnlink);
            /// <summary>The IDisposable used to unlink this target from its source.</summary>
            internal IDisposable m_unlinker;
            /// <summary>The registration used to unregister this target from the cancellation token.</summary>
            internal CancellationTokenRegistration m_ctr;

            Task IDataflowBlock.Completion
            {
                get
                {
                    throw new NotSupportedException( "This member is not supported on this dataflow block. The block is intended for a specific purpose that does not warrant the implementation of this member." );
                }
            }

            /// <summary>The data to display in the debugger display attribute.</summary>
            private object DebuggerDisplayContent
            {
                get
                {
                    return (object)string.Format( "{0} IsCompleted={1}", (object)DataflowBlockExtensions.GetNameForDebugger( (IDataflowBlock)this, (DataflowBlockOptions)null ), (object)this.Task.IsCompleted );
                }
            }

            //object IDebuggerDisplay.Content
            //{
            //    get
            //    {
            //        return this.DebuggerDisplayContent;
            //    }
            //}

            /// <summary>Cancels the target and unlinks the target from the source.</summary>
            /// <param name="state">An OutputAvailableAsyncTarget.</param>
            private static void CancelAndUnlink( object state )
            {
                System.Threading.Tasks.Task.Factory.StartNew(
                    (Action<object>)( tgt =>
                    {
                        DataflowBlockExtensions.OutputAvailableAsyncTarget<T> availableAsyncTarget = (DataflowBlockExtensions.OutputAvailableAsyncTarget<T>) tgt;
                        availableAsyncTarget.TrySetCanceled();
                        availableAsyncTarget.AttemptThreadSafeUnlink();
                    } ),
                    (object)( state as DataflowBlockExtensions.OutputAvailableAsyncTarget<T> ), CancellationToken.None, DataflowBlockExtensions.GetCreationOptionsForTask( false ), TaskScheduler.Default );
            }

            /// <summary>Disposes of m_unlinker if the target has been linked.</summary>
            internal void AttemptThreadSafeUnlink()
            {
                IDisposable comparand = this.m_unlinker;
                if ( comparand == null || Interlocked.CompareExchange<IDisposable>( ref this.m_unlinker, (IDisposable)null, comparand ) != comparand )
                    return;
                comparand.Dispose();
            }

            DataflowMessageStatus ITargetBlock<T>.OfferMessage( DataflowMessageHeader messageHeader, T messageValue, ISourceBlock<T> source, bool consumeToAccept )
            {
                if ( !messageHeader.IsValid )
                    throw new ArgumentException( "The DataflowMessageHeader instance does not represent a valid message header.", "messageHeader" );
                if ( source == null )
                    throw new ArgumentNullException( "source" );
                this.TrySetResult( true );
                return DataflowMessageStatus.DecliningPermanently;
            }

            void IDataflowBlock.Complete()
            {
                this.TrySetResult( false );
            }

            void IDataflowBlock.Fault( Exception exception )
            {
                if ( exception == null )
                    throw new ArgumentNullException( "exception" );
                this.TrySetResult( false );
            }
        }

        /// <summary>A cached instance of <see cref="T:System.Threading.Tasks.Dataflow.DataflowLinkOptions" />.</summary>
        /// <remarks>
        /// Do not change the values of this instance.  It is shared by all of our blocks that need to unlink after one message has been consumed.
        /// </remarks>
        internal static readonly DataflowLinkOptions UnlinkAfterOneAndPropagateCompletion = new DataflowLinkOptions()
        {
            MaxMessages = 1,
            PropagateCompletion = true
        };

        /// <summary>A cached instance of <see cref="T:System.Threading.Tasks.Dataflow.DataflowLinkOptions" />.</summary>
        /// <remarks>
        /// Do not change the values of this instance.  It is shared by all of our blocks that need to unlink after one message has been consumed.
        /// </remarks>
        internal static readonly DataflowLinkOptions PrependedUnlinkAfterOneAndPropagateCompletion = new DataflowLinkOptions()
        {
            MaxMessages = 1,
            PropagateCompletion = true,
            Append = false
        };
    }
}