﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;

namespace Luxoft.NDS2.Common.Helpers.Serialization
{
    public class SequentStringParser
    {
        public SequentStringParser( string[] recordDelimiters, string[] recordItemDelimiters )
        {
            RecordDelimiters        = recordDelimiters;
            RecordItemDelimiters    = recordItemDelimiters;
        }

        public string[] RecordDelimiters { get; private set; }

        public string[] RecordItemDelimiters { get; private set; }

        public IEnumerable<string[]> CreateRecordsIterator( string specification )
        {
            Contract.Requires( specification != null && ( specification == string.Empty || !string.IsNullOrWhiteSpace( specification ) ) );
            if ( specification == null || ( specification != string.Empty && string.IsNullOrWhiteSpace( specification ) ) )
                throw new ArgumentOutOfRangeException( "specification", "The parameter can not be a blank non emtpy string" );
            Contract.EndContractBlock();

            string[] records = specification == string.Empty 
                                ? new string[0]
                                : specification.Split( RecordDelimiters, StringSplitOptions.None );

            IEnumerable<string[]> recordsFields = 
                from rec in records select rec.Split( RecordItemDelimiters, StringSplitOptions.None );

            return recordsFields;
        }
    }
}