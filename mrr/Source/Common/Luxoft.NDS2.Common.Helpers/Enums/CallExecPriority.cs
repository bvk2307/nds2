﻿namespace Luxoft.NDS2.Common.Helpers.Enums
{
    /// <summary> Service call execution priority (see 'CallExecContext{TParam, TInstanceKey, TContextKey, TResult}.Priority'. </summary>
    public enum CallExecPriority
    {
        /// <summary> A normal default priority for user called operations. </summary>
        Normal = (int)default(CallExecPriority),
        /// <summary> A low priority for background operations. </summary>
        Background
    }
}