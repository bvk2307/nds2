﻿using System.Threading;

namespace Luxoft.NDS2.Common.Helpers.Events
{
    /// <summary> An abstract event raised by GC finalizer in its thread. </summary>
    public abstract class FinalizingActionWCount
    {
        private int _count = 0;

        protected FinalizingActionWCount()
        {
            Interlocked.Increment( ref _count );
        }

        ~FinalizingActionWCount()
        {
            if ( 0 == Interlocked.Decrement( ref _count ) )
                OnActionCountReset();
        }

        /// <summary> ATTENTION! Called in the own GC finalizer's thread. Do not do anything long here! </summary>
        protected abstract void OnActionCountReset();
    }
}