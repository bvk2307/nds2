﻿using System;
using System.Diagnostics.Contracts;
using System.Threading;
using System.Threading.Tasks;

namespace Luxoft.NDS2.Common.Helpers.Events
{
    /// <summary> An abstract event delayed by GC finalizer and raised in a pool thread. </summary>
    public abstract class DelayedFinalizingActionWCount : FinalizingActionWCount
    {
        private readonly int _actionDelayInMs;
        private readonly CancellationToken _cancelToken;

        protected DelayedFinalizingActionWCount( int actionDelayInMs = 0, CancellationToken cancelToken = default(CancellationToken) )
        {
            Contract.Requires( actionDelayInMs >= 0 );

            _actionDelayInMs = actionDelayInMs;
            _cancelToken = cancelToken;
        }

        /// <summary> ATTENTION! Called in the own GC finalizer's thread. Do not do anything long here! </summary>
        protected sealed override void OnActionCountReset()
        {
            if ( _actionDelayInMs > 0 )
                Task.Delay( _actionDelayInMs, _cancelToken ).ContinueWith(
                    taskPrev => OnDelayedActionCountReset(), _cancelToken, TaskContinuationOptions.ExecuteSynchronously, TaskScheduler.Default );
            else
                Task.Run( (Action)OnDelayedActionCountReset, _cancelToken );
        }

        protected abstract void OnDelayedActionCountReset();
    }
}