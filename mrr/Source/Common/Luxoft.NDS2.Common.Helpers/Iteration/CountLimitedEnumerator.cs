﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;

namespace Luxoft.NDS2.Common.Helpers.Iteration
{
    public class CountLimitedEnumerator<T> : EnumeratorWCount<T>
    {
        private long _countLimit = 0;
        private int _blockSize = 0;

        [ContractInvariantMethod]
        private void ObjectInvariant()
        {
            Contract.Invariant( Count <= CountLimit );
        }

        public CountLimitedEnumerator( IEnumerable<T> source, long startCountValue = 0, long startCountLimit = 0, int startBlockSize = 0 ) 
            : base( source, startCountValue )
        {
            Contract.Requires( startCountValue <= startCountLimit );

            CountLimit = startCountLimit;
            BlockSize = startBlockSize;
        }

        public long CountLimit
        {
            get { return _countLimit; }
            set { _countLimit = value; }
        }

        public bool IsCountLimitReached
        {
            get { return 0 < CountLimit && Count >= CountLimit; }
        }

        /// <summary> An external auxiliary property with optional value of count values block size. It is not used by the class inside. </summary>
        public int BlockSize
        {
            get { return _blockSize; }
            set { _blockSize = value; }
        }

        /// <summary> Gets the current item if it is exist and the count limit is not reached. </summary>
        /// <param name="currentItem"></param>
        /// <returns> 'null' if the count limit is reached, 'true' if an item found otherwise 'false'. </returns>
        public bool? GetNextLimited( out T currentItem )
        {
            currentItem = default(T);

            bool? success = IsCountLimitReached ? (bool?)null : GetNext( out currentItem );

            return success;
        }
    }
}
