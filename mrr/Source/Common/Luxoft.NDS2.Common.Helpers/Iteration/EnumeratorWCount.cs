﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;

namespace Luxoft.NDS2.Common.Helpers.Iteration
{
    /// <summary> A counter for <see cref="IEnumerator{T}"/>. </summary>
    /// <typeparam name="T"></typeparam>
    public class EnumeratorWCount<T>
    {
        private readonly IEnumerable<T> _source;

        private IEnumerator<T> _enumerator = null;
        private long _count = 0;

        public EnumeratorWCount( IEnumerable<T> source, long startCountValue = 0 )
        {
            Contract.Requires( source != null );

            _source = source;
            _count  = startCountValue;
        }

        public long Count
        {
            get { return _count; }
        }

        private IEnumerator<T> Enumerator
        {
            get
            {
                if ( _enumerator == null )
                    _enumerator = _source.GetEnumerator();

                return _enumerator;
            }
        }

        public bool GetNext( out T currentItem )
        {
            currentItem = default(T);

            bool success = Enumerator.MoveNext();
            if ( success )
            {
                ++_count;
                currentItem = Enumerator.Current;
            }
            return success;
        }
    }
}