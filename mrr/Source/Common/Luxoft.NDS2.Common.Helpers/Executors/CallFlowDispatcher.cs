﻿#define CLEARONCOMPLETION

using System;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using Luxoft.NDS2.Common.Helpers.Dataflow.Extensions;
using Luxoft.NDS2.Common.Helpers.Enums;
using Luxoft.NDS2.Common.Helpers.Tasks.Extensions;

namespace Luxoft.NDS2.Common.Helpers.Executors
{
    /// <summary> A dispatcher of flow of calls. </summary>
    /// <typeparam name="TInput"></typeparam>
    /// <typeparam name="TOutput"></typeparam>
    public abstract class CallFlowDispatcher<TInput, TOutput> : ICallFlowDispatcher<TInput, TOutput>, I1ParamsExecutor<TInput, TOutput>
        where TInput : PrioritizedExecContext
    {
        #region Private members

        private readonly BufferBlock<TInput> _inBuffer;
        private readonly BufferBlock<TInput> _inLowBuffer;
        private readonly BufferBlock<TOutput> _outBuffer;
        private readonly Task _taskCompletion;

        private int _callProcessorItemsCount = 0;

        #endregion Private members

        protected CallFlowDispatcher( ICallExecutorManager сallExecutorManager, int inputBoundCapacity = 0, 
                                      int lowInputBoundCapacity = 0, CancellationToken cancelToken = default(CancellationToken) )
        {
            int maxExecutorNumber = сallExecutorManager.MaxExecutorNumber;
            //apopov 28.12.2015	//DEBUG!!!
            int maxDegreeOfParallelism = Environment.ProcessorCount;
            int maxParallCapacity = maxDegreeOfParallelism > maxExecutorNumber ? maxDegreeOfParallelism : maxExecutorNumber;
            int lowLimitOfParallelism = maxDegreeOfParallelism < maxExecutorNumber ? maxDegreeOfParallelism : maxExecutorNumber;
            if ( inputBoundCapacity == 0 )
                inputBoundCapacity = DataflowBlockOptions.Unbounded;
            if ( lowInputBoundCapacity == 0 )
                lowInputBoundCapacity = DataflowBlockOptions.Unbounded;

            _inBuffer = new BufferBlock<TInput>(
                new DataflowBlockOptions { BoundedCapacity = inputBoundCapacity, CancellationToken = cancelToken, NameFormat = "In: {0}, ID: {1} ServiceCallDispatcher" } );
            _inLowBuffer = new BufferBlock<TInput>(
                new DataflowBlockOptions { BoundedCapacity = lowInputBoundCapacity,	CancellationToken = cancelToken, NameFormat = "Low Prior In: {0}, ID: {1} ServiceCallDispatcher" } );
            var callThrottler = new JoinBlock<ICallExecutor, TInput>(
                new GroupingDataflowBlockOptions { Greedy = false, BoundedCapacity = maxParallCapacity, NameFormat = "Throttler: {0}, ID: {1} ServiceCallDispatcher" } );    //doesn't cancel on cancelation to return all executors to 'сallExecutorManager'
            var callLowThrottler = new JoinBlock<ICallExecutor, TInput>(
                new GroupingDataflowBlockOptions { Greedy = false, BoundedCapacity = maxParallCapacity, NameFormat = "Low Priority Throttler: {0}, ID: {1} ServiceCallDispatcher" } );    //doesn't cancel on cancelation by design
            _outBuffer = new BufferBlock<TOutput>(
                new DataflowBlockOptions { CancellationToken = cancelToken,	//apopov 1.7.2016	//DEBUG!!!
                                           NameFormat = "Out: {0}, ID: {1} ServiceCallDispatcher" } );
            var dataTransformer = new TransformBlock<Task<TInput>, TOutput>(
                taskExecutionCall =>
                {
                    TInput context = taskExecutionCall.Result;   //rethrows AggregateException from ExecuteCallAsync() if it has been raised

                    return Process( context );
                },
                new ExecutionDataflowBlockOptions { MaxDegreeOfParallelism = 1, //works with the buffered items in the single thread at the same time
                                                    BoundedCapacity = DataflowBlockOptions.Unbounded,   //the buffer with no size limit after the execution of service calls so 'dataTransformer.Post()' always returns 'true' until is not faulted or canceled
                                                    CancellationToken = cancelToken,
                                                    NameFormat = "Data Transformer: {0}, ID: {1} ServiceCallDispatcher" } );

            Func<bool> propogateToDataTransformerIfNeeded = null;

            var callProcessor =
                new TransformBlock<Tuple<ICallExecutor, TInput>, ICallExecutor>(
                pair =>
                {
                    ICallExecutor executor = pair.Item1;

                    Task<ICallExecutor> taskProcessing = ExecuteCallAsync( executor, context: pair.Item2 )
                        .ContinueWith(
                            taskPrev =>
                            {
                                bool success = dataTransformer.Post( taskPrev );
#if DEBUG
                                if ( !success && !dataTransformer.IsCompleted() )
                                    Debug.WriteLine( "{0} declined a message because block is failed", dataTransformer );
#endif //DEBUG
                                if ( DecrementCallProcessorCount() )
                                {
                                    // ReSharper disable once PossibleNullReferenceException
                                    // ReSharper disable once AccessToModifiedClosure
                                    bool propagated = propogateToDataTransformerIfNeeded();
                                    //if ( propagated )
                                    //    Debug.WriteLine( ">>> Completion of {0} is proparaged by {1}", dataTransformer, "Call Processor" );
                                }
                                return executor;
                            },
                            TaskContinuationOptions.ExecuteSynchronously );
                        //, cancelToken );  //ATTENTION! Does not cancel execution from the throttler to the call proceccor to always return back an executor to the executor mananger

                    return taskProcessing;
                },
                new ExecutionDataflowBlockOptions { BoundedCapacity = maxParallCapacity, MaxDegreeOfParallelism = lowLimitOfParallelism, NameFormat = "Execute: {0}, ID: {1} ServiceCallDispatcher" } );

            сallExecutorManager.LinkTo( callThrottler.Target1 );
            сallExecutorManager.OutputLow.LinkTo( callLowThrottler.Target1 );

            _inBuffer.LinkTo( callThrottler.Target2, context => ( context.Priority = CallExecPriority.Normal ) == CallExecPriority.Normal ); //the predicate allways return 'true' and is used only to assign priority
            _inLowBuffer.LinkTo( callLowThrottler.Target2, context => ( context.Priority = CallExecPriority.Background ) == CallExecPriority.Background ); //the predicate allways return 'true' and is used only to assign priority

            callThrottler.LinkTo( callProcessor, IncrementCallProcessorCount ); //accepts all items
            callLowThrottler.LinkTo( callProcessor, IncrementCallProcessorCount ); //accepts all items
            callProcessor.LinkTo( сallExecutorManager );

            Task inputCompletion = Task.WhenAll( _inBuffer.Completion, _inLowBuffer.Completion );

            propogateToDataTransformerIfNeeded =
                () =>
                {   //can be called twice at the same time from 2 different threads
                    bool propagated = CallProcessorCount() <= 0 && inputCompletion.IsCompleted
                                      && callLowThrottler.OutputCount <= 0 && callThrottler.OutputCount <= 0;
                    if ( propagated )
                        dataTransformer.CompleteOrFaultFrom( inputCompletion );

                    return propagated;
                };

            inputCompletion.ContinueWith(
                taskInputCompletion =>
                {
                    bool propagated = propogateToDataTransformerIfNeeded();
                    //if ( propagated )
                    //    Debug.WriteLine( ">>> Completion of {0} is proparaged by {1}", dataTransformer, "input blocks" );
                },
                TaskContinuationOptions.ExecuteSynchronously );

            dataTransformer.LinkTo( _outBuffer, new DataflowLinkOptions { PropagateCompletion = true } );

            _outBuffer.PropagateFaultToIfIsNotCompletedYet( _inLowBuffer ); //cycles completion propagation to fault all blocks if completion is started by error in the pipeline end (for example in '_outBuffer'). The propagation is ignored if '_inLowBuffer' has been completed already
            _outBuffer.PropagateFaultToIfIsNotCompletedYet( _inBuffer );    //cycles completion propagation to fault all blocks if completion is started by error in the pipeline end (for example in '_outBuffer'). The propagation is ignored if '_inBuffer' has been completed already

            _outBuffer.PropagateFaultToIfIsNotCompletedYet( dataTransformer );   //to abort 'dataTransformer' if 'dataHandler' is canceled or faulted becuase because 'dataTransformer.Complete()' can will be hanged up otherwise (if a linked block has been completed already before all message are sent from a source 'TranformBlock')

            //apopov 1.4.2016	//DEBUG!!!
//#if DEBUG
//            cancelToken.Register( () => Debug.WriteLine( ">>> Cancellation 'cancelToken' is requested" ) );

//            сallExecutorManager.DebugWriteLineAfterCompletion( ">>>" ); //should not appear

//            _inBuffer.DebugWriteLineAfterCompletion( ">>>" );
//            _inLowBuffer.DebugWriteLineAfterCompletion( ">>>" );
//            callThrottler.DebugWriteLineAfterCompletion( ">>>" );
//            callLowThrottler.DebugWriteLineAfterCompletion( ">>>" );
//            callProcessor.DebugWriteLineAfterCompletion( ">>>" );
//            dataTransformer.DebugWriteLineAfterCompletion( ">>>" );
//            _outBuffer.DebugWriteLineAfterCompletion( ">>>" );
//#endif //DEBUG

            _taskCompletion = Task.WhenAll( 
                _inBuffer.Completion, _inLowBuffer.Completion, dataTransformer.Completion, _outBuffer.Completion );

#if CLEARONCOMPLETION

            Completion.ContinueWith(    //unlinks the global Executor Manager 'сallExecutorManager' so it will not reference the mesh and this Call Dispatcher
                taskMeshCompletion => 
                {
                    callLowThrottler.Complete();
                    callThrottler.Complete();
                },
                TaskContinuationOptions.ExecuteSynchronously );

#endif //CLEARONCOMPLETION

            Completion.DebugWriteLineAfterCompletion( "--->>> " + this.GetType().Name );

            Contract.Assume( _outBuffer != null );
            Contract.Assume( _inLowBuffer != null );
            Contract.Assume( _inBuffer != null );
        }

        protected abstract Task<TInput> ExecuteCallAsync( ICallExecutor executor, TInput context );

        public ITargetBlock<TInput> Input
        {
            get { return _inBuffer; }
        }

        public ITargetBlock<TInput> InputLow
        {
            get { return _inLowBuffer; }
        }

        public IReceivableSourceBlock<TOutput> Output
        {
            get { return _outBuffer; }
        }

        /// <summary> Gets a <see cref="T:System.Threading.Tasks.Task">Task</see> that represents the asynchronous operation and completion of the dataflow block. </summary>
        public Task Completion
        {
            get
            {
                Contract.Assume( _taskCompletion != null );

                return _taskCompletion;
            }
        }

        private int CallProcessorCount()
        {
            int currentCount = Thread.VolatileRead( ref _callProcessorItemsCount );
            //if ( currentCount <= 0 )
            //    Debug.WriteLine( ">>> Call Processor count: {0}", currentCount );

            return currentCount;
        }

        /// <summary> </summary>
        /// <returns> 'true' if the counter is not more than 0. </returns>
        private bool DecrementCallProcessorCount()
        {
            int currentCount = Interlocked.Decrement( ref _callProcessorItemsCount );

            return currentCount <= 0;
        }

        /// <summary> Always returns 'true'. </summary>
        /// <param name="pairExecutorAndInput"></param>
        /// <returns> Always returns 'true'. </returns>
        private bool IncrementCallProcessorCount( Tuple<ICallExecutor, TInput> pairExecutorAndInput )
        {
            int currentCount = Interlocked.Increment( ref _callProcessorItemsCount );

            return true;
        }

        #region Implementation of I1ParamsExecutor<in TInput, out TOutput>

        public abstract TOutput Process( TInput context );

        #endregion Implementation of I1ParamsExecutor<in TInput, out TOutput>
    }
}