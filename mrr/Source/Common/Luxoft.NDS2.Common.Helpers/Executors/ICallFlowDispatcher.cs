﻿using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace Luxoft.NDS2.Common.Helpers.Executors
{
    /// <summary> A dispatcher of flow of calls. </summary>
    /// <typeparam name="TInput"></typeparam>
    /// <typeparam name="TOutput"></typeparam>
    public interface ICallFlowDispatcher<in TInput, TOutput>
    {
        ITargetBlock<TInput> Input { get; }

        ITargetBlock<TInput> InputLow { get; }

        IReceivableSourceBlock<TOutput> Output { get; }

        /// <summary> Gets a <see cref="T:System.Threading.Tasks.Task">Task</see> that represents completion of the dataflow mesh. </summary>
        Task Completion { get; }
    }
}