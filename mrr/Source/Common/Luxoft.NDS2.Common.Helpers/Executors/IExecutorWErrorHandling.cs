﻿namespace Luxoft.NDS2.Common.Helpers.Executors
{
    /// <summary> An abstract executor interface that handles exceptions in calls of <see cref="IDirectExecutor"/>. </summary>
    public interface IExecutorWErrorHandling : IDirectExecutor { }
}