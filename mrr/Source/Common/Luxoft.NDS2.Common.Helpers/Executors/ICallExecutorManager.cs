﻿using System.Threading.Tasks.Dataflow;

namespace Luxoft.NDS2.Common.Helpers.Executors
{
    /// <summary> A factory of <see cref="ICallExecutor"/> and a dispatcher of calls. </summary>
    public interface ICallExecutorManager : IPropagatorBlock<ICallExecutor, ICallExecutor>, IReceivableSourceBlock<ICallExecutor>
    {
        /// <summary> The low priority output. </summary>
        IReceivableSourceBlock<ICallExecutor> OutputLow { get; }

        /// <summary> The maximum number of call executors running at the same time. </summary>
        int MaxExecutorNumber { get; }

        /// <summary> The number of free call executors at the moment. </summary>
        int ExecutorCountFree { get; }
    }
}