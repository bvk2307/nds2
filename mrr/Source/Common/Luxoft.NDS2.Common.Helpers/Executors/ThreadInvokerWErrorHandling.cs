﻿using System;
using System.Diagnostics.Contracts;
using System.Threading;
using System.Windows.Threading;
using FLS.CommonComponents.Lib.Execution;

namespace Luxoft.NDS2.Common.Helpers.Executors
{
    /// <summary> A caller by 'BeginInvoke()' in the main UI thread with built-in error handling. </summary>
    public sealed class ThreadInvokerWErrorHandling : IThreadInvokerWErrorHandling
    {
        private readonly IThreadInvoker _threadInvokerDecorated;
        private readonly IExecutorWErrorHandling _executorWErrorHandling;

        public ThreadInvokerWErrorHandling( IThreadInvoker threadInvoker, IExecutorWErrorHandling executorWErrorHandling )
        {
            Contract.Requires( threadInvoker != null );
            Contract.Requires( executorWErrorHandling != null );

            _threadInvokerDecorated = threadInvoker;
            _executorWErrorHandling = executorWErrorHandling;
        }

        #region Implementation of IThreadInvoker

        /// <summary> Executes the specified delegate asynchronously with the specified arguments, on the thread that the Dispatcher was created on. </summary>
        /// <param name="callDelegate"> A delegate to execute in the main UI thread. </param>
        /// <param name="priority"> The priority that determines in what order the specified method is invoked relative to the other pending methods in the Dispatcher. </param>
        /// <param name="cancelToken"></param>
        /// <returns> An operation object that represents the result of the BeginInvoke operation or 'null' if the operation has been already canceled with <paramref name="cancelToken"/>. </returns>
        public DispatcherOperation BeginExecute( Action<CancellationToken> callDelegate, DispatcherPriority priority, 
            CancellationToken cancelToken = new CancellationToken() )
        {
            return _threadInvokerDecorated.BeginExecute( _executorWErrorHandling.Execute, priority, callDelegate, cancelToken );
        }

        /// <summary> Executes the specified delegate asynchronously with the specified arguments, on the thread that the Dispatcher was created on. </summary>
        /// <typeparam name="TParam"></typeparam>
        /// <param name="callDelegate"> A delegate to execute in the main UI thread. </param>
        /// <param name="priority"> The priority that determines in what order the specified method is invoked relative to the other pending methods in the Dispatcher. </param>
        /// <param name="param"> A parameter of <paramref name="callDelegate"/>. </param>
        /// <param name="cancelToken"></param>
        /// <returns> An operation object that represents the result of the BeginInvoke operation or 'null' if the operation has been already canceled with <paramref name="cancelToken"/>. </returns>
        public DispatcherOperation BeginExecute<TParam>( Action<TParam, CancellationToken> callDelegate, 
            DispatcherPriority priority, TParam param, CancellationToken cancelToken = new CancellationToken() )
        {
            return _threadInvokerDecorated.BeginExecute( _executorWErrorHandling.Execute, priority, callDelegate, param, cancelToken );
        }

        /// <summary> Executes the specified delegate asynchronously with the specified arguments, on the thread that the Dispatcher was created on. </summary>
        /// <typeparam name="TParam1"></typeparam>
        /// <typeparam name="TParam2"></typeparam>
        /// <param name="callDelegate"> A delegate to execute in the main UI thread. </param>
        /// <param name="priority"> The priority that determines in what order the specified method is invoked relative to the other pending methods in the Dispatcher. </param>
        /// <param name="param1"> The first parameter of <paramref name="callDelegate"/>. </param>
        /// <param name="param2"> The second parameter of <paramref name="callDelegate"/>. </param>
        /// <param name="cancelToken"></param>
        /// <returns> An operation object that represents the result of the BeginInvoke operation or 'null' if the operation has been already canceled with <paramref name="cancelToken"/>. </returns>
        public DispatcherOperation BeginExecute<TParam1, TParam2>( Action<TParam1, TParam2, CancellationToken> callDelegate, 
            DispatcherPriority priority, TParam1 param1, TParam2 param2, CancellationToken cancelToken = default(CancellationToken) )
        {
            return _threadInvokerDecorated.BeginExecute( 
                _executorWErrorHandling.Execute, priority, callDelegate, param1, param2, cancelToken );
        }

        /// <summary> Executes the specified delegate asynchronously with the specified arguments, on the thread that the Dispatcher was created on. </summary>
        /// <typeparam name="TParam1"></typeparam>
        /// <typeparam name="TParam2"></typeparam>
        /// <typeparam name="TParam3"></typeparam>
        /// <param name="callDelegate"> A delegate to execute in the main UI thread. </param>
        /// <param name="priority"> The priority that determines in what order the specified method is invoked relative to the other pending methods in the Dispatcher. </param>
        /// <param name="param1"> The first parameter of <paramref name="callDelegate"/>. </param>
        /// <param name="param2"> The second parameter of <paramref name="callDelegate"/>. </param>
        /// <param name="param3"></param>
        /// <param name="cancelToken"></param>
        /// <returns> An operation object that represents the result of the BeginInvoke operation or 'null' if the operation has been already canceled with <paramref name="cancelToken"/>. </returns>
        public DispatcherOperation BeginExecute<TParam1, TParam2, TParam3>( 
            Action<TParam1, TParam2, TParam3, CancellationToken> callDelegate, 
            DispatcherPriority priority, TParam1 param1, TParam2 param2, TParam3 param3, 
            CancellationToken cancelToken = default(CancellationToken) )
        {
            return _threadInvokerDecorated.BeginExecute(
                _executorWErrorHandling.Execute, priority, callDelegate, param1, param2, param3, cancelToken );
        }

        /// <summary> Executes the specified delegate asynchronously with the specified arguments, on the thread that the Dispatcher was created on. </summary>
        /// <typeparam name="TParam1"></typeparam>
        /// <typeparam name="TParam2"></typeparam>
        /// <typeparam name="TParam3"></typeparam>
        /// <typeparam name="TParam4"></typeparam>
        /// <param name="callDelegate"> A delegate to execute in the main UI thread. </param>
        /// <param name="priority"> The priority that determines in what order the specified method is invoked relative to the other pending methods in the Dispatcher. </param>
        /// <param name="param1"> The first parameter of <paramref name="callDelegate"/>. </param>
        /// <param name="param2"> The second parameter of <paramref name="callDelegate"/>. </param>
        /// <param name="param3"> The third parameter of <paramref name="callDelegate"/>. </param>
        /// <param name="param4"> The forth parameter of <paramref name="callDelegate"/>. </param>
        /// <param name="cancelToken"></param>
        /// <returns> An operation object that represents the result of the BeginInvoke operation or 'null' if the operation has been already canceled with <paramref name="cancelToken"/>. </returns>
        public DispatcherOperation BeginExecute<TParam1, TParam2, TParam3, TParam4>( 
            Action<TParam1, TParam2, TParam3, TParam4, CancellationToken> callDelegate, 
            DispatcherPriority priority, TParam1 param1, TParam2 param2, TParam3 param3, TParam4 param4, 
            CancellationToken cancelToken = default(CancellationToken) )
        {
            return _threadInvokerDecorated.BeginExecute(
                _executorWErrorHandling.Execute, priority, callDelegate, param1, param2, param3, param4, cancelToken );
        }

        #endregion
    }
}