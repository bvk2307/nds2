﻿using Luxoft.NDS2.Common.Helpers.Enums;

namespace Luxoft.NDS2.Common.Helpers.Executors
{
    /// <summary> A context of a service call execution. </summary>
    public class CallExecContext<TParam, TInstanceKey, TContextKey, TResult> : PrioritizedExecContext
    {
        public CallExecContext( TParam parameters, TInstanceKey instanceKey = default(TInstanceKey), 
            TContextKey contextKey = default(TContextKey), TResult result = default(TResult), CallExecPriority? priority = null )
            : base( priority )
        {
            Parameters      = parameters;
            Result          = result;
            InstanceKey     = instanceKey;
            ContextKey      = contextKey;
        }

        public TParam Parameters { get; protected set; }

        public TResult Result { get; set; }

        public TInstanceKey InstanceKey { get; private set; }

        public TContextKey ContextKey { get; private set; }
    }
}