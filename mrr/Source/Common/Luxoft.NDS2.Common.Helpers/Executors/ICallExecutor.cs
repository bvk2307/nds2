using System;
using System.Threading;
using System.Threading.Tasks;

namespace Luxoft.NDS2.Common.Helpers.Executors
{
    /// <summary> An executor of some calls. </summary>
    public interface ICallExecutor
    {
        /// <summary> Starts a delegate execution. </summary>
        /// <param name="executeHandler"></param>
        /// <param name="parameters"></param>
        /// <param name="cancelToken"></param>
        /// <returns></returns>
        Task<object> Process( Func<object, CancellationToken, object> executeHandler, object parameters, 
                              CancellationToken cancelToken = default(CancellationToken) );
    }
}