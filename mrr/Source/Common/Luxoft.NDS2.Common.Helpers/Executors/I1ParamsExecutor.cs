﻿namespace Luxoft.NDS2.Common.Helpers.Executors
{
    /// <summary> A simple handler with one parameter. </summary>
    /// <typeparam name="TParam1"></typeparam>
    /// <typeparam name="TResult"></typeparam>
    public interface I1ParamsExecutor<in TParam1, out TResult>
    {
        TResult Process( TParam1 param1 );
    }
}