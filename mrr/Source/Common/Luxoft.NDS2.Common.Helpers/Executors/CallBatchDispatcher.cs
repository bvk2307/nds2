﻿#define CLEARONCOMPLETION
//#define USE_EXCEPTIONRETHROWER

using System;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using Luxoft.NDS2.Common.Helpers.Dataflow.Extensions;
using Luxoft.NDS2.Common.Helpers.Tasks.Extensions;

namespace Luxoft.NDS2.Common.Helpers.Executors
{
    /// <summary> A dispatcher for a batch of calls. </summary>
    /// <typeparam name="TInput"></typeparam>
    /// <typeparam name="TOutput"></typeparam>
    public abstract class CallBatchDispatcher<TInput, TOutput> : I1ParamsExecutor<TInput, TOutput>
    {
        #region Private members

        private readonly BufferBlock<TInput> _inBuffer;
        private readonly Task _taskCompletion;

        private int _callProcessorItemsCount = 0;

#if USE_EXCEPTIONRETHROWER
        private int _dataHandlerItemsCount = 0;
#endif //USE_EXCEPTIONRETHROWER

        #endregion Private members

        public CallBatchDispatcher( 
            Action<TOutput> dataLoadedHandler, ICallExecutorManager сallExecutorManager, int inputBoundCapacity = 0, 
            CancellationToken cancelToken = default(CancellationToken) )
        {
            Contract.Assume( сallExecutorManager != null );
            Contract.Assume( inputBoundCapacity >= 0 );
            Contract.Assume( dataLoadedHandler != null );

            if ( inputBoundCapacity == 0 )
                inputBoundCapacity = DataflowBlockOptions.Unbounded;
            int maxExecutorNumber = сallExecutorManager.MaxExecutorNumber;
	//apopov 28.12.2015	//DEBUG!!!
            int maxDegreeOfParallelism = Environment.ProcessorCount;
            int maxParallCapacity = maxDegreeOfParallelism > maxExecutorNumber ? maxDegreeOfParallelism : maxExecutorNumber;
            int lowLimitOfParallelism = maxDegreeOfParallelism < maxExecutorNumber ? maxDegreeOfParallelism : maxExecutorNumber;

            _inBuffer = new BufferBlock<TInput>( 
                new DataflowBlockOptions { BoundedCapacity = inputBoundCapacity, CancellationToken = cancelToken, NameFormat = "In: {0}, ID: {1} CallBatchDispatcher" } );
            var callThrottler = new JoinBlock<ICallExecutor, TInput>(
                new GroupingDataflowBlockOptions { Greedy = false, BoundedCapacity = maxParallCapacity, NameFormat = "Throttler: {0}, ID: {1} CallBatchDispatcher" } );    //doesn't cancel on cancelation by design

#if USE_EXCEPTIONRETHROWER

            Func<bool> propogateToExceptionRethrowerIfNeeded = null;

            var exceptionRethrower = new TransformBlock<Task<TInput>, TInput>(
                task =>
                {
                    TInput context = task.Result;   //rethrows AggregateException from ExecuteCallAsync() if it has been raised

                    return context;
                },
                new ExecutionDataflowBlockOptions {
                    BoundedCapacity = DataflowBlockOptions.Unbounded,   //the buffer with no size limit between the execution of service calls and batch data processing
                    MaxDegreeOfParallelism = lowLimitOfParallelism, NameFormat = "Exception Rethrower: {0}, ID: {1} CallBatchDispatcher" } );

            var callProcessor = new TransformBlock<Tuple<ICallExecutor, TInput>, ICallExecutor>(
                pair =>
                {
                    ICallExecutor executor = pair.Item1;

                    Task<ICallExecutor> taskProcessing = ExecuteCallAsync( executor, context: pair.Item2 )
                        .ContinueWith(
                            taskPrev =>
                            {
                                bool success = exceptionRethrower.Post( taskPrev );

#if DEBUG
                                if ( !success && !_inBuffer.IsCompleted() && !exceptionRethrower.IsCompleted() )
                                    Debug.WriteLine( "{0} declined a message because block is failed", exceptionRethrower );
#endif //DEBUG

                                if ( DecrementCallProcessorCount() )
                                {
	            //apopov 18.5.2016	//DEBUG!!!
                                    // ReSharper disable once AccessToModifiedClosure
                                    // ReSharper disable once PossibleNullReferenceException
                                    bool propagated = propogateToExceptionRethrowerIfNeeded();
                                    //if ( propagated )
                                    //    Debug.WriteLine( ">>> Completion of {0} is proparaged by {1}", exceptionRethrower, "Call Processor" );
                                }
                                return executor;
                            },
                            TaskContinuationOptions.ExecuteSynchronously );
                            //, cancelToken );  //ATTENTION! Does not cancel execution from the throttler to the call proceccor to always return back an executor to the executor mananger

                    return taskProcessing;
                }, 
                new ExecutionDataflowBlockOptions { BoundedCapacity = maxParallCapacity, MaxDegreeOfParallelism = lowLimitOfParallelism,
                                                    NameFormat = "Call Processor: {0}, ID: {1} CallBatchDispatcher" } );

            var batchDataProcessor = new TransformBlock<TInput, TOutput>( 
                (Func<TInput, TOutput>)Process,
                new ExecutionDataflowBlockOptions { MaxDegreeOfParallelism = 1, //works with the buffered items in the single thread at the same time
                                                    NameFormat = "Batch Data Processor: {0}, ID: {1} CallBatchDispatcher" } );

            Func<bool> propagateToDataHandlerIfNeeded = null;

            var dataHandler = new ActionBlock<TOutput>(
                outItem =>
                {
                    bool propagated = false;
                    dataLoadedHandler( outItem );

                    propagated = DecrementDataHandlerCount();

                    if ( propagated )
                    {
                        //apopov 18.5.2016	//DEBUG!!!
                        // ReSharper disable once AccessToModifiedClosure
                        // ReSharper disable once PossibleNullReferenceException
                        propagated = propagateToDataHandlerIfNeeded();
                        //if ( propagated )
                        //    Debug.WriteLine( ">>> Completion of {0} is proparaged by {0}", (object)"Data Handler" );
                    }
                },
                new ExecutionDataflowBlockOptions { //CancellationToken = cancelToken,	//apopov 20.5.2016	//DEBUG!!!
                    NameFormat = "Data Handler: {0}, ID: {1} CallBatchDispatcher" } );

            //apopov 1.4.2016	//DEBUG!!!
//#if DEBUG
//            cancelToken.Register( () => Debug.WriteLine( ">>> Cancellation 'cancelToken' is requested" ) );

//            сallExecutorManager.DebugWriteLineAfterCompletion( ">>>" );
//            _inBuffer.DebugWriteLineAfterCompletion( ">>>" );
//            callThrottler.DebugWriteLineAfterCompletion( ">>>" );
//            callProcessor.DebugWriteLineAfterCompletion( ">>>" );
//            exceptionRethrower.DebugWriteLineAfterCompletion( ">>>" );
//            batchDataProcessor.DebugWriteLineAfterCompletion( ">>>" );
//            dataHandler.DebugWriteLineAfterCompletion( ">>>" );
//#endif //DEBUG

            propogateToExceptionRethrowerIfNeeded =
                () =>
                {   //can be called twice at the same time from 2 different threads
	//apopov 20.5.2016	//DEBUG!!!
                    bool propagated = CallProcessorCount() <= 0 && _inBuffer.IsCompleted()
                                      && callThrottler.OutputCount <= 0;
                    if ( propagated )
                        exceptionRethrower.CompleteOrFaultFrom( _inBuffer );

                    return propagated;
                };

            propagateToDataHandlerIfNeeded = 
                () =>
                {   //can be called twice at the same time from 2 different threads
                    bool propagated = DataHandlerCount() <= 0 && exceptionRethrower.IsCompleted();
                    if ( propagated )
                        dataHandler.CompleteOrFaultFrom( exceptionRethrower );

                    return propagated;
                };

            сallExecutorManager.LinkTo( callThrottler.Target1 );

            _inBuffer.LinkTo( callThrottler.Target2 );
            callThrottler.LinkTo( callProcessor, IncrementCallProcessorCount ); //accepts all items
            callProcessor.LinkTo( сallExecutorManager );
            exceptionRethrower.LinkTo( batchDataProcessor, IncrementDataHandlerCount ); //accepts all items

            _inBuffer.Completion.ContinueWith(
                taskInputCompletion =>
                {
	//apopov 22.5.2016	//DEBUG!!!
                    bool propagated = propogateToExceptionRethrowerIfNeeded();
                    //if ( propagated )
                    //    Debug.WriteLine( ">>> Completion of {0} is proparaged by {1}", exceptionRethrower, _inBuffer );
                },
                TaskContinuationOptions.ExecuteSynchronously );

            batchDataProcessor.LinkTo( dataHandler, new DataflowLinkOptions { PropagateCompletion = true } );  //propagates only on an exception inside 'batchDataProcessor' i.e. in Process(). It propagates completion to 'dataHandler' to 'dataHandler' can fail. 'dataHandler' can not complete when 'batchDataProcessor' will not offer messages to 'dataHandler' (propagateToDataHandlerIfNeeded() will not be called in this case)

            exceptionRethrower.Completion.ContinueWith(
                taskRethrower =>
                {
	//apopov 18.5.2016	//DEBUG!!!
                    bool propagated = propagateToDataHandlerIfNeeded();
                    //if ( propagated )
                    //    Debug.WriteLine( ">>> Completion of {0} is proparaged by {1}", dataHandler, exceptionRethrower );
                },
                TaskContinuationOptions.ExecuteSynchronously );

            dataHandler.PropagateFaultToIfIsNotCompletedYet( _inBuffer );   //cycles completion propagation to fault all blocks if completion is started by error in the pipeline end (for example in 'dataHandler'). The propagation is ignored if '_inBuffer' has been completed already

            //'batchDataProcessor' faults on mesh completion because it does not have completion way in normal
            batchDataProcessor.PropagateFaultToIfIsNotCompletedYet( exceptionRethrower );   //to abort 'exceptionRethrower' if 'exceptionRethrower' is faulted becuase because 'exceptionRethrower.Complete()' can will be hanged up otherwise (if a linked block has been completed already before all message are sent from a source 'TranformBlock')
            dataHandler.PropagateFaultToIfIsNotCompletedYet( batchDataProcessor );   //to abort 'batchDataProcessor' if 'dataHandler' is canceled (?) or faulted becuase because 'batchDataProcessor.Complete()' can will be hanged up otherwise (if a linked block has been completed already before all message are sent from a source 'TranformBlock')

            _taskCompletion = Task.WhenAll( 
                _inBuffer.Completion, exceptionRethrower.Completion, 
                //batchDataProcessor.Completion, 	//apopov 21.5.2016	//DEBUG!!!
                dataHandler.Completion );

#else //!USE_EXCEPTIONRETHROWER

            var batchDataProcessor = new TransformBlock<Task<TInput>, TOutput>(
                taskExecutionCall =>
                {
                    TInput context = taskExecutionCall.Result;   //rethrows AggregateException from ExecuteCallAsync() if it has been raised

                    return Process( context );
                },
                new ExecutionDataflowBlockOptions { MaxDegreeOfParallelism = 1, //works with the buffered items in the single thread at the same time
                                                    BoundedCapacity = DataflowBlockOptions.Unbounded,   //the buffer with no size limit after the execution of service calls so 'batchDataProcessor.Post()' always returns 'true' until is not faulted or canceled
                                                    CancellationToken = cancelToken,
                                                    NameFormat = "Batch Data Processor: {0}, ID: {1} CallBatchDispatcher" } );

            Func<bool> propogateToBatchDataProcessorIfNeeded = null;

            var callProcessor = new TransformBlock<Tuple<ICallExecutor, TInput>, ICallExecutor>(
                pair =>
                {
                    ICallExecutor executor = pair.Item1;

                    Task<ICallExecutor> taskProcessing = ExecuteCallAsync( executor, context: pair.Item2 )
                        .ContinueWith(
                            taskPrev =>
                            {
                                bool success = batchDataProcessor.Post( taskPrev ); //processes executor calls and post results in some thread at the same time without ordering

#if DEBUG
                                if ( !success && !_inBuffer.IsCompleted() && !batchDataProcessor.IsCompleted() )
                                    Debug.WriteLine( "{0} declined a message because block is failed", batchDataProcessor );
#endif //DEBUG

                                if ( DecrementCallProcessorCount() )
                                {
                                    // ReSharper disable once AccessToModifiedClosure
                                    // ReSharper disable once PossibleNullReferenceException
                                    bool propagated = propogateToBatchDataProcessorIfNeeded();
                                    //if ( propagated )
                                    //    Debug.WriteLine( ">>> Completion of {0} is proparaged by {1}", batchDataProcessor, "Call Processor" );
                                }
                                return executor;
                            },
                            TaskContinuationOptions.ExecuteSynchronously );
                            //, cancelToken );  //ATTENTION! Does not cancel execution from the throttler to the call proceccor to always return back an executor to the executor mananger

                    return taskProcessing;
                }, 
                new ExecutionDataflowBlockOptions { BoundedCapacity = maxParallCapacity, MaxDegreeOfParallelism = lowLimitOfParallelism,
                                                    NameFormat = "Call Processor: {0}, ID: {1} CallBatchDispatcher" } );

            var dataHandler = new ActionBlock<TOutput>( dataLoadedHandler,
                new ExecutionDataflowBlockOptions { MaxDegreeOfParallelism = 1, //processes output data in the single thread at the same time
                                                    CancellationToken = cancelToken,
                                                    NameFormat = "Data Handler: {0}, ID: {1} CallBatchDispatcher" } );

            //apopov 1.4.2016	//DEBUG!!!
//#if DEBUG
//            cancelToken.Register( () => Debug.WriteLine( ">>> Cancellation 'cancelToken' is requested" ) );

//            сallExecutorManager.DebugWriteLineAfterCompletion( ">>>" ); //should not appear

//            _inBuffer.DebugWriteLineAfterCompletion( ">>>" );
//            callThrottler.DebugWriteLineAfterCompletion( ">>>" );
//            callProcessor.DebugWriteLineAfterCompletion( ">>>" );
//            batchDataProcessor.DebugWriteLineAfterCompletion( ">>>" );
//            dataHandler.DebugWriteLineAfterCompletion( ">>>" );
//#endif //DEBUG

            propogateToBatchDataProcessorIfNeeded =
                () =>
                {   //can be called twice at the same time from 2 different threads
                    bool propagated = CallProcessorCount() <= 0 && _inBuffer.IsCompleted()
                                      && callThrottler.OutputCount <= 0;
                    if ( propagated )
                        batchDataProcessor.CompleteOrFaultFrom( _inBuffer );

                    return propagated;
                };

            сallExecutorManager.LinkTo( callThrottler.Target1 );

            _inBuffer.LinkTo( callThrottler.Target2 );
            callThrottler.LinkTo( callProcessor, IncrementCallProcessorCount ); //accepts all items
            callProcessor.LinkTo( сallExecutorManager );

            _inBuffer.Completion.ContinueWith(
                taskInputCompletion =>
                {
                    bool propagated = propogateToBatchDataProcessorIfNeeded();
                    //if ( propagated )
                    //    Debug.WriteLine( ">>> Completion of {0} is proparaged by {1}", batchDataProcessor, _inBuffer );
                },
                TaskContinuationOptions.ExecuteSynchronously );

            batchDataProcessor.LinkTo( dataHandler, new DataflowLinkOptions { PropagateCompletion = true } );  //propagates only on an exception inside 'batchDataProcessor' i.e. in Process(). It propagates completion to 'dataHandler' to 'dataHandler' can fail. 'dataHandler' can not complete when 'batchDataProcessor' will not offer messages to 'dataHandler' (propagateToDataHandlerIfNeeded() will not be called in this case)

            dataHandler.PropagateFaultToIfIsNotCompletedYet( _inBuffer );   //cycles completion propagation to fault all blocks if completion is started by error in the pipeline end (for example in 'dataHandler'). The propagation is ignored if '_inBuffer' has been completed already

            dataHandler.PropagateFaultToIfIsNotCompletedYet( batchDataProcessor );   //to abort 'batchDataProcessor' if 'dataHandler' is canceled or faulted becuase because 'batchDataProcessor.Complete()' can will be hanged up otherwise (if a linked block has been completed already before all message are sent from a source 'TranformBlock')

            _taskCompletion = Task.WhenAll( 
                _inBuffer.Completion, batchDataProcessor.Completion, dataHandler.Completion );

#endif //USE_EXCEPTIONRETHROWER

#if CLEARONCOMPLETION

            Completion.ContinueWith(    //unlinks the global Executor Manager 'сallExecutorManager' so it will not reference the mesh and this Call Dispatcher
                taskMeshCompletion => callThrottler.Complete(),
                TaskContinuationOptions.ExecuteSynchronously );

#endif //CLEARONCOMPLETION

            Completion.DebugWriteLineAfterCompletion( "--->>> " + this.GetType().Name );

            Contract.Assume( _inBuffer != null );
        }

        protected abstract Task<TInput> ExecuteCallAsync( ICallExecutor executor, TInput context );

        /// <summary> Gets a <see cref="T:System.Threading.Tasks.Task">Task</see> that represents completion of the dataflow mesh. </summary>
        public Task Completion
        {
            get
            {
                Contract.Assume( _taskCompletion != null );

                return _taskCompletion;
            }
        }

        protected int InputCount
        {
            get { return _inBuffer.Count; }
        }

        protected bool Post( TInput context )
        {
            bool success = _inBuffer.Post( context );
#if DEBUG
            if ( !success && !_inBuffer.IsCompleted() )
                Debug.WriteLine( "{0} declined a message because block is failed or canceled", _inBuffer );
#endif //DEBUG

            return success;
        }

        protected void Complete()
        {
            _inBuffer.Complete();
        }

        private int CallProcessorCount()
        {
            int currentCount = Thread.VolatileRead( ref _callProcessorItemsCount );
            //if ( currentCount <= 0 )
            //    Debug.WriteLine( ">>> Call Processor count: {0}", currentCount );

            return currentCount;
        }

        /// <summary> </summary>
        /// <returns> 'true' if the counter is not more than 0. </returns>
        private bool DecrementCallProcessorCount()
        {
            int currentCount = Interlocked.Decrement( ref _callProcessorItemsCount );

            return currentCount <= 0;
        }

        /// <summary> Always returns 'true'. </summary>
        /// <param name="pairExecutorAndInput"></param>
        /// <returns> Always returns 'true'. </returns>
        private bool IncrementCallProcessorCount( Tuple<ICallExecutor, TInput> pairExecutorAndInput )
        {
            int currentCount = Interlocked.Increment( ref _callProcessorItemsCount );

            return true;
        }

#if USE_EXCEPTIONRETHROWER

        private int DataHandlerCount()
        {
            return Thread.VolatileRead( ref _dataHandlerItemsCount );
        }

        /// <summary> Always returns 'true'. </summary>
        /// <param name="outputItem"></param>
        /// <returns> Always returns 'true'. </returns>
        private bool IncrementDataHandlerCount( TInput outputItem )
        {
            Interlocked.Increment( ref _dataHandlerItemsCount );

            return true;
        }

        /// <summary> </summary>
        /// <returns> 'true' if the counter is not more than 0. </returns>
        private bool DecrementDataHandlerCount()
        {
            return 0 >= Interlocked.Decrement( ref _dataHandlerItemsCount );
        }

#endif //USE_EXCEPTIONRETHROWER

        #region Implementation of I1ParamsExecutor<in TInput, out TOutput>

        public abstract TOutput Process( TInput param1 );

        #endregion Implementation of I1ParamsExecutor<in TInput, out TOutput>
    }
}