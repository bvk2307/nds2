﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace Luxoft.NDS2.Common.Helpers.Executors
{
    /// <summary> A factory of <see cref="ICallExecutor"/> and a dispatcher of calls. </summary>
    public partial class CallExecutorManager : ICallExecutorManager
    {
        private readonly int _maxExecutorNumber;

        private readonly BufferBlock<ICallExecutor> _inAndLowOutBuffer;
        private readonly BufferBlock<ICallExecutor> _outBuffer;

        public CallExecutorManager( int maxExecutorNumber )
        {
            Contract.Requires( maxExecutorNumber > 1 );
            Contract.Ensures( _outBuffer.Count == 1 );
            Contract.Ensures( _inAndLowOutBuffer.Count == MaxExecutorNumber - 1 );

            _maxExecutorNumber = maxExecutorNumber;

            _inAndLowOutBuffer = new BufferBlock<ICallExecutor>( 
                new DataflowBlockOptions { BoundedCapacity = _maxExecutorNumber - 1, NameFormat = "In & Low Out: {0}, ID: {1} CallExecutorManager" } );
            _outBuffer = new BufferBlock<ICallExecutor>(
                new DataflowBlockOptions { BoundedCapacity = 1, NameFormat = "Out: {0}, ID: {1} CallExecutorManager" } );

            _inAndLowOutBuffer.LinkTo( _outBuffer, new DataflowLinkOptions { PropagateCompletion = true } );

            foreach ( ICallExecutor executor in from nI in Enumerable.Range( 0, _maxExecutorNumber )
                      select new CallExecutor() )
            {
                while ( !this.Post( executor ) ) { }
            }
            //while ( this.Post( new ServiceCallExecutor() ) ) { }  //does not work stable
        }

        /// <summary> The low priority output. </summary>
        public IReceivableSourceBlock<ICallExecutor> OutputLow
        {
            get { return _inAndLowOutBuffer; }
        }

        /// <summary> The maximum number of call executors running at the same time. </summary>
        public int MaxExecutorNumber { get { return _maxExecutorNumber; } }

        /// <summary> The number of free call executors at the moment. </summary>
        public int ExecutorCountFree { get { return _inAndLowOutBuffer.Count + _outBuffer.Count; } }

        #region Implementation of IDataflowBlock

        /// <summary> Offers a message to the block. </summary>
        /// <param name="messageHeader"></param>
        /// <param name="messageValue"></param>
        /// <param name="source"></param>
        /// <param name="consumeToAccept"></param>
        /// <returns></returns>
        DataflowMessageStatus ITargetBlock<ICallExecutor>.OfferMessage( 
            DataflowMessageHeader messageHeader, ICallExecutor messageValue, ISourceBlock<ICallExecutor> source, bool consumeToAccept )
        {
            return ( (ITargetBlock<ICallExecutor>)_inAndLowOutBuffer ).OfferMessage( messageHeader, messageValue, source, consumeToAccept );
        }

        /// <summary> Signals to the <see cref="T:System.Threading.Tasks.Dataflow.IDataflowBlock"/> that it should not accept nor produce any more messages nor consume any more postponed messages. </summary>
        /// <remarks>
        /// After Complete has been called on a dataflow block, that block will complete
        ///         (such that its <see cref="M:Completion"/> task will enter a final state) after it has processed all previously
        ///         available data. Complete will not block waiting for completion to occur, but rather will initiate
        ///         the request; to wait for completion to occur, the <see cref="M:Completion"/> task may be used.
        /// </remarks>
        void IDataflowBlock.Complete()
        {
            _inAndLowOutBuffer.Complete();
        }

        /// <summary> Causes the <see cref="T:System.Threading.Tasks.Dataflow.IDataflowBlock"/> to complete in a <see cref="F:System.Threading.Tasks.TaskStatus.Faulted"/> state. </summary>
        /// <param name="exception">The <see cref="T:System.Exception"/> that caused the faulting.</param><exception cref="T:System.ArgumentNullException">The <paramref name="exception"/> is null (Nothing in Visual Basic).
        ///       </exception>
        /// <remarks>
        /// After Fault has been called on a dataflow block, that block will complete
        ///         (such that its <see cref="M:Completion"/> task will enter a final state). Faulting a block,
        ///         as with canceling a block, causes buffered messages (unprocessed input messages as well as 
        ///         unoffered output messages) to be lost. 
        /// </remarks>
        void IDataflowBlock.Fault( Exception exception )
        {
            ( (IDataflowBlock)_inAndLowOutBuffer ).Fault( exception );
        }

        /// <summary> Gets a <see cref="T:System.Threading.Tasks.Task">Task</see> that represents the asynchronous operation and completion of the dataflow block. </summary>
        /// <remarks>
        /// A dataflow block is considered completed when it is not currently processing a message and when it has guaranteed that it will not process
        ///         any more messages. The returned <see cref="T:System.Threading.Tasks.Task">Task</see> will transition to a completed state when the
        ///         associated block has completed. It will transition to the <see cref="T:System.Threading.Tasks.TaskStatus">RanToCompletion</see> state
        ///         when the block completes its processing successfully according to the dataflow block’s defined semantics, it will transition to
        ///         the <see cref="T:System.Threading.Tasks.TaskStatus">Faulted</see> state when the dataflow block has completed processing prematurely due to an unhandled exception,
        ///         and it will transition to the <see cref="T:System.Threading.Tasks.TaskStatus">Canceled</see> state when the dataflow  block has completed processing
        ///         prematurely due to receiving a cancellation request.  If the task completes in the Faulted state, its Exception property will return
        ///         an <see cref="T:System.AggregateException"/> containing the one or more exceptions that caused the block to fail.
        /// </remarks>
        Task IDataflowBlock.Completion { get { return _outBuffer.Completion; } }

        #endregion Implementation of IDataflowBlock

        #region Implementation of ISourceBlock<out IServiceCallExecutor>

        /// <summary> Links the <see cref="T:System.Threading.Tasks.Dataflow.ISourceBlock`1"/> to the specified <see cref="T:System.Threading.Tasks.Dataflow.ITargetBlock`1"/>. </summary>
        /// <param name="target">The <see cref="T:System.Threading.Tasks.Dataflow.ITargetBlock`1"/> to which to connect this source.
        ///       </param><param name="linkOptions">A <see cref="T:System.Threading.Tasks.Dataflow.DataflowLinkOptions"/> instance that configures the link.
        ///       </param>
        /// <returns>
        /// An IDisposable that, upon calling Dispose, will unlink the source from the target.
        /// </returns>
        /// <exception cref="T:System.ArgumentNullException"><paramref name="target"/> is null (Nothing in Visual Basic) or <paramref name="linkOptions"/> is null (Nothing in Visual Basic).
        ///       </exception>
        public IDisposable LinkTo( ITargetBlock<ICallExecutor> target, DataflowLinkOptions linkOptions )
        {
            return _outBuffer.LinkTo( target, linkOptions );
        }

        /// <summary> Passes the ownership of the message identified by the <see cref="T:System.Threading.Tasks.Dataflow.DataflowMessageHeader"/> from this 
        ///         <see cref="T:System.Threading.Tasks.Dataflow.ISourceBlock`1"/> instance to the <see cref="T:System.Threading.Tasks.Dataflow.ITargetBlock`1"/>.  
        /// </summary>
        /// <param name="messageHeader">The <see cref="T:System.Threading.Tasks.Dataflow.DataflowMessageHeader"/> of the message that is to be consumed.</param><param name="target">The <see cref="T:System.Threading.Tasks.Dataflow.ITargetBlock`1"/> for which the message is to be consumed.
        ///       </param><param name="messageConsumed">True if the message was successfully consumed. False otherwise.
        ///       </param>
        /// <returns>
        /// <para>
        /// The value of the consumed message. This may correspond to a different <see cref="T:System.Threading.Tasks.Dataflow.DataflowMessageHeader"/> instance than was previously reserved and
        ///           passed as the <paramref name="messageHeader"/> to <see cref="M:ConsumeMessage"/>. The consuming <see cref="T:System.Threading.Tasks.Dataflow.ITargetBlock`1"/> must 
        ///           use the returned value instead of the value passed as messageValue through <see cref="M:OfferMessage"/>.
        /// </para>
        /// <para>
        /// If the message requested is not available, the return value will be null (Nothing in Visual Basic).
        /// </para>
        /// </returns>
        /// <exception cref="T:System.ArgumentException">The <paramref name="messageHeader"/> is not valid.
        ///       </exception><exception cref="T:System.ArgumentNullException">The <paramref name="target"/> is null (Nothing in Visual Basic).
        ///       </exception>
        /// <remarks>
        /// <para>
        /// The <see cref="T:System.Threading.Tasks.Dataflow.ITargetBlock`1"/> for which the message is to be consumed need not be linked from this 
        ///           <see cref="T:System.Threading.Tasks.Dataflow.ISourceBlock`1"/> instance. Moreover, this <see cref="T:System.Threading.Tasks.Dataflow.ISourceBlock`1"/> 
        ///           instance may have never offered the message directly to the <see cref="T:System.Threading.Tasks.Dataflow.ITargetBlock`1"/>. 
        /// </para>
        /// </remarks>
        ICallExecutor ISourceBlock<ICallExecutor>.ConsumeMessage( DataflowMessageHeader messageHeader, ITargetBlock<ICallExecutor> target, out bool messageConsumed )
        {
            return ( (ISourceBlock<ICallExecutor>)_outBuffer ).ConsumeMessage( messageHeader, target, out messageConsumed );
        }

        /// <summary> Reserves the right to pass the ownership of the message identified by the <see cref="T:System.Threading.Tasks.Dataflow.DataflowMessageHeader"/> 
        ///         from this <see cref="T:System.Threading.Tasks.Dataflow.ISourceBlock`1"/> to the <see cref="T:System.Threading.Tasks.Dataflow.ITargetBlock`1"/>.
        /// </summary>
        /// <param name="messageHeader">The <see cref="T:System.Threading.Tasks.Dataflow.DataflowMessageHeader"/> of the message that is to be reserved.
        ///       </param><param name="target">The <see cref="T:System.Threading.Tasks.Dataflow.ITargetBlock`1"/> for which the message is to be reserved.
        ///       </param>
        /// <returns>
        /// true if the message was successfully reserved; otherwise, false.
        /// </returns>
        /// <exception cref="T:System.ArgumentException">The <paramref name="messageHeader"/> is not valid.
        ///       </exception><exception cref="T:System.ArgumentNullException">The <paramref name="target"/> is null (Nothing in Visual Basic).
        ///       </exception>
        /// <remarks>
        /// <para>
        /// The <see cref="T:System.Threading.Tasks.Dataflow.ITargetBlock`1"/> for which the message is to be reserved need not be linked from this
        ///           <see cref="T:System.Threading.Tasks.Dataflow.ISourceBlock`1"/> instance. Moreover, this <see cref="T:System.Threading.Tasks.Dataflow.ISourceBlock`1"/>
        ///           instance may have never offered the message directly to the <see cref="T:System.Threading.Tasks.Dataflow.ITargetBlock`1"/>.
        /// </para>
        /// <para>
        /// If true is returned, either <see cref="M:ConsumeMessage"/> or <see cref="M:ReleaseReservation"/> for this message must be subsequently called 
        ///           with the same <see cref="T:System.Threading.Tasks.Dataflow.DataflowMessageHeader"/> and <see cref="T:System.Threading.Tasks.Dataflow.ITargetBlock`1"/>.
        ///           Failure to do so may result in the source being unable to propagate any further messages to any target.
        /// </para>
        /// <para>
        /// <see cref="M:ReserveMessage"/> must not be called while the target is holding any internal locks.  Doing so will violate the lock hierarchy
        ///           necessary to avoid deadlocks in a dataflow network.
        /// </para>
        /// </remarks>
        bool ISourceBlock<ICallExecutor>.ReserveMessage( DataflowMessageHeader messageHeader, ITargetBlock<ICallExecutor> target )
        {
            return ( (ISourceBlock<ICallExecutor>)_outBuffer ).ReserveMessage( messageHeader, target );
        }

        /// <summary> Releases the right to pass the ownership of the message identified by the <see cref="T:System.Threading.Tasks.Dataflow.DataflowMessageHeader"/> 
        ///         from this <see cref="T:System.Threading.Tasks.Dataflow.ISourceBlock`1"/> to the <see cref="T:System.Threading.Tasks.Dataflow.ITargetBlock`1"/>.
        /// </summary>
        /// <param name="messageHeader">The <see cref="T:System.Threading.Tasks.Dataflow.DataflowMessageHeader"/> of the reserved message.
        ///       </param><param name="target">The <see cref="T:System.Threading.Tasks.Dataflow.ITargetBlock`1"/> that currently holds the reservation.
        ///       </param><exception cref="T:System.ArgumentException">The <paramref name="messageHeader"/> is not valid.
        ///       </exception><exception cref="T:System.ArgumentNullException">The <paramref name="target"/> is null (Nothing in Visual Basic).
        ///       </exception><exception cref="T:System.InvalidOperationException">The <paramref name="target"/> did not have the message reserved.
        ///       </exception>
        /// <remarks>
        /// <para>
        /// The <see cref="T:System.Threading.Tasks.Dataflow.ITargetBlock`1"/> that holds the reservation need not be linked from this
        ///           <see cref="T:System.Threading.Tasks.Dataflow.ISourceBlock`1"/> instance. Moreover, this <see cref="T:System.Threading.Tasks.Dataflow.ISourceBlock`1"/>
        ///           instance may have never offered the message directly to the <see cref="T:System.Threading.Tasks.Dataflow.ITargetBlock`1"/>.
        /// </para>
        /// <para>
        /// It is required that this message has been previously reserved for the same <see cref="T:System.Threading.Tasks.Dataflow.ITargetBlock`1"/>.
        /// </para>
        /// </remarks>
        void ISourceBlock<ICallExecutor>.ReleaseReservation( DataflowMessageHeader messageHeader, ITargetBlock<ICallExecutor> target )
        {
            ( (ISourceBlock<ICallExecutor>)_outBuffer ).ReleaseReservation( messageHeader, target );
        }

        #endregion Implementation of ISourceBlock<out IServiceCallExecutor>

        #region Implementation of IReceivableSourceBlock<IServiceCallExecutor>

        /// <summary> Attempts to synchronously receive an available output item from the <see cref="T:System.Threading.Tasks.Dataflow.IReceivableSourceBlock`1"/>. </summary>
        /// <param name="filter">The predicate a value must successfully pass in order for it to be received. 
        ///         <paramref name="filter"/> may be null (Nothing in Visual Basic), in which case all items will pass.
        ///       </param><param name="item">The item received from the source.</param>
        /// <returns>
        /// true if an item could be received; otherwise, false.
        /// </returns>
        /// <remarks>
        /// This method does not block waiting for the source to provide an item.
        ///         It will return after checking for an element, whether or not an element was available.
        /// </remarks>
        public bool TryReceive( Predicate<ICallExecutor> filter, out ICallExecutor item )
        {
            return _outBuffer.TryReceive( filter, out item );
        }

        /// <summary> Attempts to synchronously receive all available items from the <see cref="T:System.Threading.Tasks.Dataflow.IReceivableSourceBlock`1"/>. </summary>
        /// <param name="items">The items received from the source.</param>
        /// <returns>
        /// true if one or more items could be received; otherwise, false.
        /// </returns>
        /// <remarks>
        /// This method does not block waiting for the source to provide an item.
        ///         It will return after checking for elements, whether or not an element was available.
        /// </remarks>
        public bool TryReceiveAll( out IList<ICallExecutor> items )
        {
            return _outBuffer.TryReceiveAll( out items );
        }

        #endregion IReceivableSourceBlock<IServiceCallExecutor>
    }
}