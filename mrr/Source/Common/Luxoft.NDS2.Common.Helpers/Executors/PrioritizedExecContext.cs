﻿using System;
using Luxoft.NDS2.Common.Helpers.Enums;

namespace Luxoft.NDS2.Common.Helpers.Executors
{
    /// <summary> A prioritized context of an execution. </summary>
    public class PrioritizedExecContext
    {
        private CallExecPriority? _priority = null;

        public PrioritizedExecContext( CallExecPriority? priority = null )
        {
            if ( priority.HasValue )
                Priority    = priority.Value;
        }

        public CallExecPriority Priority
        {
            get { return _priority ?? default(CallExecPriority); }
            set
            {
                if ( _priority.HasValue )
                    throw new InvalidOperationException( string.Format( "Reassigning of PrioritizedExecContext.Priority is not allowed, old value: {0}, new value: {1}", _priority.Value, value ) );

                _priority = value;
            }
        }
    }
}