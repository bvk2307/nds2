﻿using System;
using System.Threading;

namespace Luxoft.NDS2.Common.Helpers.Executors
{
    /// <summary> An abstract executor interface. </summary>
    public interface IDirectExecutor
    {
        /// <summary> Executes a delegate defined. </summary>
        /// <param name="callDelegate"> A deletegate to execute. </param>
        /// <param name="cancelToken"> A cancellation token. Optional. Is not cancellable if this parameter is not defined. </param>
        void Execute( Action<CancellationToken> callDelegate, CancellationToken cancelToken = default(CancellationToken) );

        /// <summary> Вызов с обрабаткой исключения общего характера. Внимание! Перехватывает все exception-ы. </summary>
        /// <typeparam name="TResult">Тип сущности передаваемой между делегатами</typeparam>
        /// <param name="callDelegate">Делегат серверного вызова</param>
        /// <param name="cancelToken">Токен для прерывания операции. Опционально, по умолчанию операция не прерывается</param>
        /// <returns>Результат выполнения операции</returns>
        TResult Execute<TResult>( Func<CancellationToken, TResult> callDelegate, CancellationToken cancelToken = default(CancellationToken) );

        /// <summary> Executes a delegate defined. </summary>
        /// <typeparam name="TParam1"> A type of the first parameter <paramref name="param1"/> of the delegate <paramref name="callDelegate"/>. </typeparam>
        /// <param name="callDelegate"> A deletegate to execute. </param>
        /// <param name="param1"> The first delegate parameter. </param>
        /// <param name="cancelToken"> A cancellation token. Optional. Is not cancellable if this parameter is not defined. </param>
        void Execute<TParam1>( 
            Action<TParam1, CancellationToken> callDelegate, TParam1 param1, CancellationToken cancelToken = default(CancellationToken) );

        /// <summary> Executes a delegate defined. </summary>
        /// <typeparam name="TParam1"> A type of the first parameter <paramref name="param1"/> of the delegate <paramref name="callDelegate"/>. </typeparam>
        /// <typeparam name="TResult"> A type of execution result. </typeparam>
        /// <param name="callDelegate"> A deletegate to execute. </param>
        /// <param name="param1"> The first delegate parameter. </param>
        /// <param name="cancelToken"> A cancellation token. Optional. Is not cancellable if this parameter is not defined. </param>
        /// <returns> An execution result. </returns>
        TResult Execute<TParam1, TResult>( Func<TParam1, CancellationToken, TResult> callDelegate, TParam1 param1, 
            CancellationToken cancelToken = default(CancellationToken) );

        /// <summary> Executes a delegate defined. </summary>
        /// <typeparam name="TParam1"> A type of the first parameter <paramref name="param1"/> of the delegate <paramref name="callDelegate"/>. </typeparam>
        /// <typeparam name="TParam2"> A type of the second parameter <paramref name="param2"/> of the delegate <paramref name="callDelegate"/>. </typeparam>
        /// <param name="callDelegate"> A deletegate to execute. </param>
        /// <param name="param1"> The first delegate parameter. </param>
        /// <param name="param2"> The second delegate parameter. </param>
        /// <param name="cancelToken"> A cancellation token. Optional. Is not cancellable if this parameter is not defined. </param>
        void Execute<TParam1, TParam2>( Action<TParam1, TParam2, CancellationToken> callDelegate, TParam1 param1, TParam2 param2, 
            CancellationToken cancelToken = default(CancellationToken) );

        /// <summary> Executes a delegate defined. </summary>
        /// <typeparam name="TParam1"> A type of the first parameter <paramref name="param1"/> of the delegate <paramref name="callDelegate"/>. </typeparam>
        /// <typeparam name="TParam2"> A type of the second parameter <paramref name="param2"/> of the delegate <paramref name="callDelegate"/>. </typeparam>
        /// <typeparam name="TResult"> A type of execution result. </typeparam>
        /// <param name="callDelegate"> A deletegate to execute. </param>
        /// <param name="param1"> The first delegate parameter. </param>
        /// <param name="param2"> The second delegate parameter. </param>
        /// <param name="cancelToken"> A cancellation token. Optional. Is not cancellable if this parameter is not defined. </param>
        /// <returns> An execution result. </returns>
        TResult Execute<TParam1, TParam2, TResult>( Func<TParam1, TParam2, CancellationToken, TResult> callDelegate, 
            TParam1 param1, TParam2 param2, CancellationToken cancelToken = default(CancellationToken) );

        /// <summary> Executes a delegate defined. </summary>
        /// <typeparam name="TParam1"> A type of the first parameter <paramref name="param1"/> of the delegate <paramref name="callDelegate"/>. </typeparam>
        /// <typeparam name="TParam2"> A type of the second parameter <paramref name="param2"/> of the delegate <paramref name="callDelegate"/>. </typeparam>
        /// <typeparam name="TParam3"></typeparam>
        /// <param name="callDelegate"> A deletegate to execute. </param>
        /// <param name="param1"> The first delegate parameter. </param>
        /// <param name="param2"> The second delegate parameter. </param>
        /// <param name="param3">Третий параметр вызова</param>
        /// <param name="cancelToken"> A cancellation token. Optional. Is not cancellable if this parameter is not defined. </param>
        void Execute<TParam1, TParam2, TParam3>( Action<TParam1, TParam2, TParam3, CancellationToken> callDelegate,
            TParam1 param1, TParam2 param2, TParam3 param3,
            CancellationToken cancelToken = default( CancellationToken ) );

        /// <summary> Вызов с обрабаткой исключения общего характера. Внимание! Перехватывает все exception-ы. </summary>
        /// <typeparam name="TParam1"></typeparam>
        /// <typeparam name="TParam2"></typeparam>
        /// <typeparam name="TParam3"></typeparam>
        /// <typeparam name="TResult">Тип сущности передаваемой между делегатами</typeparam>
        /// <param name="callDelegate">Делегат серверного вызова</param>
        /// <param name="param1">Первый параметр вызова</param>
        /// <param name="param2">Второй параметр вызова</param>
        /// <param name="param3">Третий параметр вызова</param>
        /// <param name="cancelToken">Токен для прерывания операции. Опционально, по умолчанию операция не прерывается</param>
        /// <returns>Результат выполнения операции</returns>
        TResult Execute<TParam1, TParam2, TParam3, TResult>( Func<TParam1, TParam2, TParam3, CancellationToken, TResult> callDelegate, 
            TParam1 param1, TParam2 param2, TParam3 param3, CancellationToken cancelToken = default(CancellationToken) );

        /// <summary> Executes a delegate defined. </summary>
        /// <typeparam name="TParam1"> A type of the first parameter <paramref name="param1"/> of the delegate <paramref name="callDelegate"/>. </typeparam>
        /// <typeparam name="TParam2"> A type of the second parameter <paramref name="param2"/> of the delegate <paramref name="callDelegate"/>. </typeparam>
        /// <typeparam name="TParam3"></typeparam>
        /// <typeparam name="TParam4"></typeparam>
        /// <param name="callDelegate"> A deletegate to execute. </param>
        /// <param name="param1"> The first delegate parameter. </param>
        /// <param name="param2"> The second delegate parameter. </param>
        /// <param name="param3">Третий параметр вызова</param>
        /// <param name="param4">Четвертый параметр вызова</param>
        /// <param name="cancelToken"> A cancellation token. Optional. Is not cancellable if this parameter is not defined. </param>
        void Execute<TParam1, TParam2, TParam3, TParam4>( Action<TParam1, TParam2, TParam3, TParam4, CancellationToken> callDelegate, 
            TParam1 param1, TParam2 param2, TParam3 param3, TParam4 param4,
            CancellationToken cancelToken = default(CancellationToken) );

        /// <summary> Вызов с обрабаткой исключения общего характера. Внимание! Перехватывает все exception-ы. </summary>
        /// <typeparam name="TParam1"></typeparam>
        /// <typeparam name="TParam2"></typeparam>
        /// <typeparam name="TParam3"></typeparam>
        /// <typeparam name="TParam4"></typeparam>
        /// <typeparam name="TResult">Тип сущности передаваемой между делегатами</typeparam>
        /// <param name="callDelegate">Делегат серверного вызова</param>
        /// <param name="param1">Первый араметр вызова</param>
        /// <param name="param2">Второй параметр вызова</param>
        /// <param name="param3">Третий параметр вызова</param>
        /// <param name="param4">Четвертый параметр вызова</param>
        /// <param name="cancelToken">Токен для прерывания операции. Опционально, по умолчанию операция не прерывается</param>
        /// <returns>Результат выполнения операции</returns>
        TResult Execute<TParam1, TParam2, TParam3, TParam4, TResult>( 
            Func<TParam1, TParam2, TParam3, TParam4, CancellationToken, TResult> callDelegate, 
            TParam1 param1, TParam2 param2, TParam3 param3, TParam4 param4,
            CancellationToken cancelToken = default(CancellationToken) );
    }
}