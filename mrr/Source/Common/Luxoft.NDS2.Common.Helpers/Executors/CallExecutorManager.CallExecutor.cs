﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Luxoft.NDS2.Common.Helpers.Executors
{
    /// <summary> A factory of <see cref="ICallExecutor"/> and a dispatcher of calls. </summary>
    public partial class CallExecutorManager
    {
        /// <summary> An executor of calls. </summary>
        private sealed class CallExecutor : ICallExecutor
        {
            private static long s_lastId = 0;

            private readonly long _id;

            public CallExecutor()
            {
                _id = Interlocked.Increment( ref s_lastId );
            }

            public long Id { get { return _id; } }

            /// <summary> Starts a delegate execution. </summary>
            /// <param name="executeHandler"></param>
            /// <param name="parameters"></param>
            /// <param name="cancelToken"></param>
            /// <returns></returns>
            public Task<object> Process( 
                Func<object, CancellationToken, object> executeHandler, object parameters, CancellationToken cancelToken = default(CancellationToken) )
            {
                return Task.Run( () => executeHandler( parameters, cancelToken ), cancelToken );
            }
        }
    }
}