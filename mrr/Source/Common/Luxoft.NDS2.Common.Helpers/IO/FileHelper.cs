﻿using System;
using System.IO;
using System.Text.RegularExpressions;

namespace Luxoft.NDS2.Common.Helpers.IO
{
    public static class FileHelper
    {
        /// <summary> Deletes a file by specified path if it exists. </summary>
        /// <param name="filePath"></param>
        public static void FileDeleteIfExists( string filePath )
        {
            if ( File.Exists( filePath ) )
                File.Delete( filePath );
        }

        /// <summary>
        /// Remove invalid characters from file name and cut it to 250 symbols length if longer
        /// </summary>
        /// <param name="filename">Name of file</param>
        /// <returns>Edited filename</returns>
        public static string CleanFileName(string filename)
        {
            string file = filename;

            if (file.Length > 250)
            {
                file = file.Substring(0, 250);
            }

            string regexSearch = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
            var r = new Regex(string.Format("[{0}]", Regex.Escape(regexSearch)));
            file = r.Replace(file, string.Empty);

            return file;
        }
    }
}
