﻿using System.Windows.Media;

namespace FLS.Common.Controls.Wpf.Colors
{
    public static class ColorHelper
    {
        public static Color FromColor( System.Drawing.Color drawingColor )
        {
            Color resultColor = Color.FromArgb( drawingColor.A, drawingColor.R, drawingColor.G, drawingColor.B );

            return resultColor;
        }

        public static Color FromArgb( int colorArgb )
        {
            Color resultColor = FromColor( System.Drawing.Color.FromArgb( colorArgb ) );

            return resultColor;
        }
    }
}