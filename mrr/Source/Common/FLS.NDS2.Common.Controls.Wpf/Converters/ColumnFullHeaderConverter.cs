﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Data;

using Telerik.Windows.Controls;

namespace FLS.Common.Controls.Wpf.Converters
{
    public class ColumnFullHeaderConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var column = value as GridViewDataColumn;

            if (column != null)
            {
                var columnGroup = column.DataControl.ColumnGroups.Where(g => g.Name == column.ColumnGroupName);

                return columnGroup.Any() ? string.Format("{0}/{1}", columnGroup.First().Header.ToString(),column.Header.ToString()) : column.Header.ToString();
            }

            return string.Empty;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
