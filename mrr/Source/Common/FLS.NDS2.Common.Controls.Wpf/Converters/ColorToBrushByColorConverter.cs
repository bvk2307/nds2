﻿using System;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using FLS.Common.Controls.Wpf.Brushes;

namespace FLS.Common.Controls.Wpf.Converters
{
    public class ColorToBrushByColorConverter : IValueConverter
    {
        public object Convert( object value, Type targetType, object parameter, System.Globalization.CultureInfo culture )
        {
            object brush = value == DependencyProperty.UnsetValue ? value : SolidColorBrushHelper.GetColorBrush( (Color)value );

            return brush;
        }

        public object ConvertBack( object value, Type targetType, object parameter, System.Globalization.CultureInfo culture )
        {
            return value is SolidColorBrush ? ( (SolidColorBrush)value ).Color : value;
        }
    }
}