﻿using System.Windows;
using Telerik.Windows.Controls.Data.DataPager;

namespace FLS.Common.Controls.Wpf.DataPager
{
    public class DataPagerPresenterEx : DataPagerPresenter
    {
        static DataPagerPresenterEx()
        {
            DefaultStyleKeyProperty.OverrideMetadata( typeof(DataPagerPresenterEx), new FrameworkPropertyMetadata( typeof(DataPagerPresenterEx) ) );
        }
    }
}