﻿using System.Windows;
using Telerik.Windows.Controls;

namespace FLS.Common.Controls.Wpf.DataPager
{
    public class RadDataPagerEx : RadDataPager
    {
        static RadDataPagerEx()
        {
            DefaultStyleKeyProperty.OverrideMetadata( typeof(RadDataPagerEx), new FrameworkPropertyMetadata( typeof(RadDataPagerEx) ) );
        }
    }
}