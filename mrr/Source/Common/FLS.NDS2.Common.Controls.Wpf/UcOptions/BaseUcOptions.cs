﻿using System;

namespace FLS.Common.Controls.Wpf.UcOptions
{
    /// <summary> ATTENTION! Edit the state of this class carefully because it is permanently storing on client side! </summary>
    /// <remarks> ATTENTION! Do not use third party types in the state of these options. </remarks>
    [Serializable]
    public abstract class BaseUcOptions
    {
    }
}