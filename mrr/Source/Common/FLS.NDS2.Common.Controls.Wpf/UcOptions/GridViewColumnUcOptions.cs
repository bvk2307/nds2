﻿using System;
using System.Diagnostics.Contracts;
using FLS.Common.Controls.Wpf.UcOptions.Extensions;
using FLS.Common.Controls.Wpf.UcOptions.Telerik;
using FLS.Common.Controls.Wpf.UcOptions.Telerik.Extensions;
using Telerik.Windows.Controls;

namespace FLS.Common.Controls.Wpf.UcOptions
{
    /// <summary> ATTENTION! Edit the state of this class carefully because it is permanently storing on client side! </summary>
    /// <remarks> ATTENTION! Do not use third party types in the state of these options. </remarks>
    [Serializable]
    public class GridViewColumnUcOptions : BaseUcOptions
    {
        private readonly string _uniqueName;

        public GridViewColumnUcOptions( string uniqueName )
        {
            Contract.Requires( !String.IsNullOrEmpty( uniqueName ) );

            _uniqueName = uniqueName;
        }

        public string UniqueName
        {
            get { return _uniqueName; }
        }

        public int DisplayIndex { get; set; }

        public bool IsVisible { get; set; }

        //public TextWrapping HeaderTextWrapping { get; set; }

        //public TextAlignment HeaderTextAlignment { get; set; }

        public double WidthValue { get; set; }

        public UcGridViewLengthUnitType WidthUnitType { get; set; }

        public double WidthDesiredValue { get; set; }

        public double WidthDisplayValue { get; set; }

        public virtual void ApplyOptionsToColumn( GridViewColumn column )
        {
            Contract.Requires( column != null );

            //column.DisplayIndex        = DisplayIndex;    //does not assign display index because it is made in GridViewColumnOptionsCollectionExtensions.ApplyOptionsToColumns() and a new index value is not equal to saved one in general case

            column.IsVisible           = IsVisible;
            column.Width               = new GridViewLength( WidthValue, WidthUnitType.ToGridViewLengthUnitType(), WidthDesiredValue, WidthDisplayValue );
            //column.HeaderTextAlignment = HeaderTextAlignment;
            //column.HeaderTextWrapping  = HeaderTextWrapping;
        }

        public static GridViewColumnUcOptions CreateOptionsByColumn( GridViewColumn column )
        {
            Contract.Requires( column != null );
            Contract.Requires( !String.IsNullOrEmpty( column.UniqueName ) );
            Contract.Ensures( Contract.Result<GridViewColumnUcOptions>() != null );

            GridViewDataColumn gridViewDataColumn = column as GridViewDataColumn;
            GridViewBoundColumnBase gridViewBoundColumnBase = null;
            GridViewBoundColumnBaseUcOptions gridViewBoundColumnBaseUcOptions = null;
            GridViewColumnUcOptions options;
            if ( gridViewDataColumn != null )
            {
                options = new GridViewDataColumnUcOptions( gridViewDataColumn.UniqueName );
            }
            else
            {
                gridViewBoundColumnBase = column as GridViewBoundColumnBase;
                if ( gridViewBoundColumnBase != null )
                {
                    gridViewBoundColumnBaseUcOptions = new GridViewBoundColumnBaseUcOptions( gridViewBoundColumnBase.UniqueName );
                    options = gridViewBoundColumnBaseUcOptions;
                }
                else
                {
                    options = new GridViewColumnUcOptions( column.UniqueName );
                }
            }
            if ( gridViewDataColumn != null )
            {
                gridViewBoundColumnBase = gridViewDataColumn;
                gridViewBoundColumnBaseUcOptions = (GridViewBoundColumnBaseUcOptions)options;
            }
            if ( gridViewBoundColumnBase != null )
            {   //'gridViewBoundColumnBaseUcOptions' is the same one as 'options' here
                //gridViewBoundColumnBaseUcOptions.DataFormatString = gridViewBoundColumnBase.DataFormatString;

                //string header = column.Header as string;
                //if ( header != null )
                //    gridViewBoundColumnBaseUcOptions.Header = header;
            }

            options.DisplayIndex            = column.DisplayIndex;
            options.IsVisible               = column.IsVisible;
            options.WidthValue              = column.Width.Value;
            options.WidthUnitType           = column.Width.UnitType.ToUcGridViewLengthUnitType();
            options.WidthDesiredValue       = column.Width.DesiredValue;
            options.WidthDisplayValue       = column.Width.DisplayValue;
            //options.HeaderTextAlignment     = column.HeaderTextAlignment;
            //options.HeaderTextWrapping      = column.HeaderTextWrapping;

            return options;
        }
    }
}