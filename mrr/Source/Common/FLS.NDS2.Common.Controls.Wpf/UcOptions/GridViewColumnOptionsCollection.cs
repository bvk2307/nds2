﻿using System;
using System.Collections.ObjectModel;

namespace FLS.Common.Controls.Wpf.UcOptions
{
    /// <summary> ATTENTION! Edit the state of this class carefully because it is permanently storing on client side! </summary>
    [Serializable]
    public class GridViewColumnOptionsCollection<TOptions> : KeyedCollection<string, TOptions> where TOptions : GridViewColumnUcOptions
    {
        /// <summary> Gets the key. </summary>
        /// <param name="item"> The element from which to extract the key. </param>
        /// <returns> The key for the specified element. </returns>
        protected override string GetKeyForItem( TOptions item )
        {
            return item.UniqueName;
        }
    }
}