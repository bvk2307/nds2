﻿using System;
using Telerik.Windows.Controls;

namespace FLS.Common.Controls.Wpf.UcOptions
{
    /// <summary> ATTENTION! Edit the state of this class carefully because it is permanently storing on client side! </summary>
    [Serializable]
    public class GridViewDataColumnUcOptions : GridViewBoundColumnBaseUcOptions
    {
        public GridViewDataColumnUcOptions( string uniqueName ) : base( uniqueName ) { }

        public override void ApplyOptionsToColumn( GridViewColumn columnBase )
        {
            GridViewBoundColumnBase column = (GridViewDataColumn)columnBase;

            base.ApplyOptionsToColumn( column );
        }
    }
}