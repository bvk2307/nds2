﻿using System;
using FLS.Common.Controls.Wpf.UcOptions.Telerik;
using Telerik.Windows.Controls;

namespace FLS.Common.Controls.Wpf.UcOptions.Extensions
{
    /// <summary> Extension methods for <see cref="GridViewLengthUnitType"/>. </summary>
    public static class GridViewLengthUnitTypeExtensions
    {
        public static UcGridViewLengthUnitType ToUcGridViewLengthUnitType( this GridViewLengthUnitType ucLengthUnitType )
        {
            UcGridViewLengthUnitType lengthUnitType;
            switch ( ucLengthUnitType )
            {case GridViewLengthUnitType.Auto:
                lengthUnitType = UcGridViewLengthUnitType.Auto;
                break;
            case GridViewLengthUnitType.Pixel:
                lengthUnitType = UcGridViewLengthUnitType.Pixel;
                break;
            case GridViewLengthUnitType.SizeToCells:
                lengthUnitType = UcGridViewLengthUnitType.SizeToCells;
                break;
            case GridViewLengthUnitType.SizeToHeader:
                lengthUnitType = UcGridViewLengthUnitType.SizeToHeader;
                break;
            case GridViewLengthUnitType.Star:
                lengthUnitType = UcGridViewLengthUnitType.Star;
                break;
            default:
                throw new ArgumentOutOfRangeException( "ucLengthUnitType", string.Format( "Unsupported value: {0} of {1}", ucLengthUnitType, ucLengthUnitType.GetType().FullName ));
            }

            return lengthUnitType;
        }
    }
}