﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using Telerik.Windows.Controls;

namespace FLS.Common.Controls.Wpf.UcOptions.Extensions
{
    /// <summary> Extension methods for <see cref="GridViewColumnOptionsCollection{TOptions}"/>. </summary>
    public static class GridViewColumnOptionsCollectionExtensions
    {
        public static void ApplyOptionsToColumns<TOptions>( 
            this GridViewColumnOptionsCollection<TOptions> columnUcOptions, GridViewColumnCollection columns )
            where TOptions : GridViewColumnUcOptions
        {
            Contract.Requires( columnUcOptions != null );
            Contract.Requires( columns != null );

            Tuple<TOptions, GridViewColumn>[] optionsToApply = new Tuple<TOptions, GridViewColumn>[ columns.Count ];
            for ( int nI = columns.Count - 1; nI >= 0 ; nI-- )
            {
                GridViewColumn gridViewColumn = columns[nI];
                gridViewColumn.DisplayIndex = -1;   //resets the display index to update it below

                TOptions options = null;
                if ( columnUcOptions.Contains( gridViewColumn.UniqueName ) )
                {
                    options = columnUcOptions[ gridViewColumn.UniqueName ];

                    Contract.Assert( options != null );
                }
                optionsToApply[nI] = Tuple.Create( options, gridViewColumn );   //'options' can be 'null' here
            }
            int nJ = -1;
            foreach ( Tuple<TOptions, GridViewColumn> pair in 
                optionsToApply.OrderBy( pairPar => pairPar.Item1 == null ? int.MaxValue : pairPar.Item1.DisplayIndex ) )
            {
                pair.Item2.DisplayIndex = ++nJ; //assigns a new display index in order of saved options' display indexes
                if ( pair.Item1 != null )
                    pair.Item1.ApplyOptionsToColumn( pair.Item2 );
            }
        }

        public static void ApplyOptionsToColumns<TOptions>( 
            this GridViewColumnOptionsCollection<TOptions> columnUcOptions, GridViewColumnCollection columns, bool notRestoreOrder = false )
            where TOptions : GridViewColumnUcOptions
        {
            Contract.Requires( columnUcOptions != null );
            Contract.Requires( columns != null );

            bool needReorder = false;
            Func<bool> isNotSameOrder = () => notRestoreOrder || needReorder;

            for ( int nI = 0; nI < columns.Count; nI++ )
            {
                if ( !isNotSameOrder() && nI >= columnUcOptions.Count )
                    break;

                GridViewColumn gridViewColumn = columns[nI];
                TOptions options = null;
                if ( !isNotSameOrder() ) //column reordering is allowed and the columns order is the same as the column options order
                {
                    options = columnUcOptions[nI];
                    if ( gridViewColumn.UniqueName != options.UniqueName )  //the columns order is not the same as the column options order now so goto the control branch below
                        needReorder = true;
                }
                if ( isNotSameOrder() )   //column reordering is not allowed or the columns order is not the same as the column options order
                {
                    if ( columnUcOptions.Contains( gridViewColumn.UniqueName ) )
                    {
                        options = columnUcOptions[ gridViewColumn.UniqueName ];

                        Contract.Assert( options != null );
                    }
                }
                if ( options != null )
                    options.ApplyOptionsToColumn( gridViewColumn );
                //else //options have not found for the current column 'gridViewColumn'
            }
            if ( !notRestoreOrder && needReorder )
            {
                int colunnsCount = columns.Count;
                var newColumns = new List<GridViewColumn>( colunnsCount );

                foreach ( TOptions options in columnUcOptions )
                {
                    if ( columns.Count <= 0 )
                        break;

                    GridViewColumn column = columns[ options.UniqueName ];
                    if ( column != null )
                    {
                        columns.Remove( column );
                        newColumns.Add( column );   //adds columns in the order of their options
                    }
                }
                foreach ( GridViewColumn column in columns )
                {
                    newColumns.Add( column );   //adds rest coluns in the original column order into the end
                }
                columns.Clear();

                Contract.Assert( newColumns.Count == colunnsCount );

                columns.AddRange( newColumns );
            }
        }

        public static void AddOptions<TOptions>( this GridViewColumnOptionsCollection<TOptions> columnUcOptions, IEnumerable<GridViewColumn> columns )
            where TOptions : GridViewColumnUcOptions
        {
            Contract.Requires( columnUcOptions != null );
            Contract.Requires( columns != null );

            foreach ( GridViewColumn gridViewColumn in columns )
            //foreach ( GridViewColumn gridViewColumn in columns.OrderBy( column => column.DisplayIndex ) )
            {
                columnUcOptions.Add( (TOptions)GridViewColumnUcOptions.CreateOptionsByColumn( gridViewColumn ) );
            }
        }
    }
}