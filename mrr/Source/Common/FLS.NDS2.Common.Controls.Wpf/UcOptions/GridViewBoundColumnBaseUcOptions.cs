﻿using System;
using Telerik.Windows.Controls;

namespace FLS.Common.Controls.Wpf.UcOptions
{
    /// <summary> ATTENTION! Edit the state of this class carefully because it is permanently storing on client side! </summary>
    [Serializable]
    public class GridViewBoundColumnBaseUcOptions : GridViewColumnUcOptions
    {
        public GridViewBoundColumnBaseUcOptions( string uniqueName ) : base( uniqueName ) { }

        //public string DataFormatString { get; set; }

        //public string Header { get; set; }

        public override void ApplyOptionsToColumn( GridViewColumn columnBase )
        {
            GridViewBoundColumnBase column = (GridViewBoundColumnBase)columnBase;

            base.ApplyOptionsToColumn( column );

            //string header = Header as string;
            //if ( header != null )
            //    column.Header          = Header;
            //column.DataFormatString    = DataFormatString;
        }
    }
}