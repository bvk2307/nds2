﻿using System;
using System.Diagnostics.Contracts;
using FLS.CommonComponents.Lib.Interfaces;

namespace FLS.Common.Controls.Wpf.UcOptions
{
    /// <summary> ATTENTION! Edit the state of this class carefully because it is permanently storing on client side! </summary>
    /// <remarks> ATTENTION! Do not use third party types in the state of these options. </remarks>
    [Serializable]
    public abstract class BaseRootUcOptions : BaseUcOptions, IHaveVersion
    {
        private Version _optionsVersion = null;

        [NonSerialized]
        private bool _isNeededToSave;

        /// <summary> The required default constructor. </summary>
        protected BaseRootUcOptions() : base() { }

        /// <summary> The signal to save when it will be usefull. The flag internally used by 'Ais3.Abd.Analysis.Ui.UcOptions.UcOptionsProvider'. </summary>
        public bool IsNeededToSave
        {
            get { return _isNeededToSave; }
            set { _isNeededToSave = value; }
        }

        /// <summary> The options version requered for successful deserialization otherwise options are reset to default ones. ATTENTION! Do not change this value while you do not want to reset all saved client options of all users. </summary>
        public Version Version
        {
            get { return _optionsVersion; }
            set
            {
                if ( _optionsVersion != null && value != null )
                    throw new InvalidOperationException( string.Format( "{0}.Version could not be changed since it has been assigned", GetType().FullName ) );

                _optionsVersion = value;
            }
        }
    }
}