﻿using Telerik.Windows.Controls;

namespace FLS.Common.Controls.Wpf.UcOptions.Telerik
{
    /// <summary> ATTENTION! Do not edit items' values because they are equal to items' values of <see cref="GridViewLengthUnitType"/> and are permanently storing on client side! </summary>
    public enum UcGridViewLengthUnitType
    {
        Auto            = 0,
        Pixel           = 1,
        SizeToCells     = 2,
        SizeToHeader    = 3,
        Star            = 4,

        Unknown         = int.MinValue
    }
}