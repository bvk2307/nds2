﻿using System;
using Telerik.Windows.Controls;

namespace FLS.Common.Controls.Wpf.UcOptions.Telerik.Extensions
{
    /// <summary> Extension methods for <see cref="UcGridViewLengthUnitType"/>. </summary>
    public static class UcGridViewLengthUnitTypeExtensions
    {
        public static GridViewLengthUnitType ToGridViewLengthUnitType( this UcGridViewLengthUnitType ucLengthUnitType )
        {
            GridViewLengthUnitType lengthUnitType;
            switch ( ucLengthUnitType )
            {case UcGridViewLengthUnitType.Auto:
            case UcGridViewLengthUnitType.Unknown:
                lengthUnitType = GridViewLengthUnitType.Auto;
                break;
            case UcGridViewLengthUnitType.Pixel:
                lengthUnitType = GridViewLengthUnitType.Pixel;
                break;
            case UcGridViewLengthUnitType.SizeToCells:
                lengthUnitType = GridViewLengthUnitType.SizeToCells;
                break;
            case UcGridViewLengthUnitType.SizeToHeader:
                lengthUnitType = GridViewLengthUnitType.SizeToHeader;
                break;
            case UcGridViewLengthUnitType.Star:
                lengthUnitType = GridViewLengthUnitType.Star;
                break;
            default:
                throw new ArgumentOutOfRangeException( "ucLengthUnitType", string.Format( "Unsupported value: {0} of {1}", ucLengthUnitType, ucLengthUnitType.GetType().FullName ));
            }

            return lengthUnitType;
        }
    }
}