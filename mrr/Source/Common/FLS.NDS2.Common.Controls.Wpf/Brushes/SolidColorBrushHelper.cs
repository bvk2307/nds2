﻿using System.Collections.Generic;
using System.Windows.Media;

namespace FLS.Common.Controls.Wpf.Brushes
{
    /// <summary> Caches brushes returned by <see cref="GetColorBrush"/> in static state. </summary>
    public static class SolidColorBrushHelper
    {
        private static Dictionary<Color, SolidColorBrush> s_brushes = new Dictionary<Color, SolidColorBrush>();

        public static SolidColorBrush GetColorBrush( Color color )
        {
            SolidColorBrush resultBrush;
            if ( !SolidColorBrushHelper.s_brushes.TryGetValue( color, out resultBrush ) )
            {
                resultBrush = new SolidColorBrush( color );
                resultBrush.Freeze();
                SolidColorBrushHelper.s_brushes[ color ] = resultBrush;
            }
            return resultBrush;
        }
    }
}