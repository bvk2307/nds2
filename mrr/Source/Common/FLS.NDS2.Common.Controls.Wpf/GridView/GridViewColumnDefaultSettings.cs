﻿using Telerik.Windows.Controls;

namespace FLS.Common.Controls.Wpf.GridView
{
    /// <summary> Default settings for properties of <see cref="GridViewColumn"/>. </summary>
    public abstract class GridViewColumnDefaultSettings
    {
        private static readonly PagedGridViewColumnDefaultSettings s_pagedSettings = new PagedGridViewColumnDefaultSettings();
        private static readonly NonPagedGridViewColumnDefaultSettings s_nonPagedSettings = new NonPagedGridViewColumnDefaultSettings();

        public static PagedGridViewColumnDefaultSettings PagedSettings
        {
            get { return s_pagedSettings; }
        }

        public static NonPagedGridViewColumnDefaultSettings NonPagedSettings
        {
            get { return s_nonPagedSettings; }
        }

        public bool IsFilteringDeferred { get { return true; } }

        public abstract bool ShowDistinctFilters { get; }
    }
}