﻿namespace FLS.Common.Controls.Wpf.GridView
{
    public class NonPagedGridViewColumnDefaultSettings : GridViewColumnDefaultSettings
    {
        public override bool ShowDistinctFilters
        {
            get { return true; }
        }
    }
}