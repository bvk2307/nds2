﻿using System.Diagnostics.Contracts;
using FLS.Common.Controls.Wpf.UcOptions;
using FLS.Common.Controls.Wpf.UcOptions.Extensions;
using Telerik.Windows.Controls;

namespace FLS.Common.Controls.Wpf.GridView.Extensions
{
    /// <summary> Extension methods for <see cref="GridViewColumnCollection"/>. </summary>
    public static class GridViewColumnCollectionExtensions
    {
        public static void ApplyDefaultColumnProperties( this GridViewColumnCollection columnCollection, GridViewColumnDefaultSettings columnDefaultSettings )
        {
            Contract.Requires( columnCollection != null );
            Contract.Requires( columnDefaultSettings != null );

            foreach ( GridViewColumn gridViewColumn in columnCollection )
            {
                if ( gridViewColumn.IsFilterable )
                {
                    gridViewColumn.IsFilteringDeferred = columnDefaultSettings.IsFilteringDeferred;
                    gridViewColumn.ShowDistinctFilters = columnDefaultSettings.ShowDistinctFilters;
                }
            }
        }

        /// <summary> Applies options to columns in <paramref name="columnCollection"/> if <paramref name="columnsOptions"/> is not 'null'. Otherwise it reads and returns options from <paramref name="columnCollection"/>. </summary>
        /// <param name="columnCollection"></param>
        /// <param name="columnsOptions"></param>
        /// <returns></returns>
        public static GridViewColumnOptionsCollection<GridViewColumnUcOptions> 
            ApplyOrReadOptions( this GridViewColumnCollection columnCollection, GridViewColumnOptionsCollection<GridViewColumnUcOptions> columnsOptions )
        {
            Contract.Requires( columnCollection != null );
            Contract.Ensures( columnsOptions != null || Contract.Result<GridViewColumnOptionsCollection<GridViewColumnUcOptions>>() != null );

            GridViewColumnOptionsCollection<GridViewColumnUcOptions> readColumnsOptions = null;
            if ( columnsOptions == null )
            {
                readColumnsOptions = new GridViewColumnOptionsCollection<GridViewColumnUcOptions>();
                readColumnsOptions.AddOptions( columnCollection );
            }
            else
            {
                columnsOptions.ApplyOptionsToColumns( columnCollection );
            }
            return readColumnsOptions;
        }
    }
}