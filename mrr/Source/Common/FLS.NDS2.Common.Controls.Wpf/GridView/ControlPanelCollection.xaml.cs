﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;

using FLS.Common.Controls.Wpf.Helpers;

using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace FLS.Common.Controls.Wpf.GridView
{
    /// <summary>
    /// Interaction logic for ControlPanelCollection.xaml
    /// </summary>
    public partial class ControlPanelCollection : ObservableCollection<IControlPanelItem>
    {
        public ControlPanelCollection()
        {
            InitializeComponent();
        }

        /// <summary> Occurs on dimension сhanged. </summary>
        public event EventHandler<RoutedEventArgs> DimensionChanged;

        /// <summary> Occurs on click of button 'Clear filters'. </summary>
        public event EventHandler<RoutedEventArgs> ClickClearFilters;

        /// <summary> Occurs on click of button 'Export to Ms Excel'. </summary>
        public event EventHandler<RoutedEventArgs> ClickExportToExcel;

        private void OnClickExportToExcel( object sender, RoutedEventArgs eventArgs )
        {
            EventHandler<RoutedEventArgs> handlers = ClickExportToExcel;
            if ( handlers != null )
                handlers( sender, eventArgs );
        }

        private void OnClickClearFilters(object sender, RoutedEventArgs eventArgs)
        {
            EventHandler<RoutedEventArgs> handlers = ClickClearFilters;
            if (handlers != null)
                handlers(sender, eventArgs);
        }

        private void OnDimensionChanged(object sender, RoutedEventArgs e)
        {
            var radioButton = sender as RadioButton;

            if (radioButton == null)
            {
                return;
            }

            if (radioButton.Content != null)
            {
                DimensionValue.Text = radioButton.Content.ToString();
            }

            var button = WpfControlHelper.FindParent<RadDropDownButton>(DimensionValue);

            if (button != null)
            {
                button.IsOpen = false;
            }

            if (DimensionChanged != null)
            {
                DimensionChanged(this, new RoutedEventArgs());
            }
        }
    }
}
