﻿namespace FLS.Common.Controls.Wpf.GridView
{
    public class PagedGridViewColumnDefaultSettings : GridViewColumnDefaultSettings
    {
        public override bool ShowDistinctFilters
        {
            get { return false; }
        }
    }
}