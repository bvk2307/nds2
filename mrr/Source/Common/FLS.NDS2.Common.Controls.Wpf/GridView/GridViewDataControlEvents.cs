﻿using System;
using System.Windows;
using Telerik.Windows;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace FLS.Common.Controls.Wpf.GridView
{
    /// <summary> Routed events for <see cref="GridViewDataControl"/> and descendants. </summary>
    public static class GridViewDataControlEvents
    {
        /// <summary> Occurs on the exporting to MS Excel has finished. </summary>
        public static readonly RoutedEvent ColumnsUcOptionsChangedEvent = EventManager.RegisterRoutedEvent( "ColumnsUcOptionsChanged", RoutingStrategy.Bubble, typeof(EventHandler<RoutedEventArgs>), typeof(GridViewDataControl) );

        /// <summary> Occurs on exporting to MS Excel. </summary>
        public static readonly RoutedEvent ExportingToExcelEvent = EventManager.RegisterRoutedEvent( "ExportingToExcel", RoutingStrategy.Bubble, typeof(EventHandler<CancelRoutedEventArgs>), typeof(GridViewDataControl) );

        /// <summary> Occurs on the exporting to MS Excel has finished. </summary>
        public static readonly RoutedEvent ExportedToExcelEvent = EventManager.RegisterRoutedEvent( "ExportedToExcel", RoutingStrategy.Bubble, typeof(EventHandler<RadRoutedEventArgs>), typeof(GridViewDataControl) );
    }
}