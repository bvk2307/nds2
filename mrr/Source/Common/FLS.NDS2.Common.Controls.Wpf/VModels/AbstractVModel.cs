﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;

//using Ais3.Abd.Analysis.Ui.Annotations;

namespace FLS.Common.Controls.Wpf.VModels
{
    public abstract class AbstractVModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

	//apopov 20.9.2016	//DEBUG!!!
        //[NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged( 
            [CallerMemberName]
            string publicPropertyName = null )
        {
            ThrowIfPropertyNameNotVerified( publicPropertyName );

            PropertyChangedEventHandler handlers = PropertyChanged;
            if ( handlers != null )
                handlers.Invoke( this, new PropertyChangedEventArgs( publicPropertyName ) );
        }

        [Conditional( "DEBUG" )]
        public void ThrowIfPropertyNameNotVerified( string publicPropertyName )
        {
            if ( TypeDescriptor.GetProperties( this )[publicPropertyName] == null )
                throw new InvalidOperationException( string.Format( "Invalid public property name: {0} for class: {1}", publicPropertyName, GetType().FullName ) );        
        }
    }
}