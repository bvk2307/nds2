﻿using System.Diagnostics.Contracts;
using System.Windows.Input;
using FLS.Common.Controls.Wpf.Commands.Extensions;
using FLS.Common.Controls.Wpf.Data;

namespace FLS.Common.Controls.Wpf.VModels
{
    public class DataPagerStandardVModel : AbstractVModel
    {
        private readonly PagedRequest _pagedRequest;
        private readonly ICommand _dataPageRequestCommand;

        public DataPagerStandardVModel( PagedRequest pagedRequest, ICommand dataPageRequestCommand )
        {
            Contract.Requires( pagedRequest != null );
            Contract.Requires( dataPageRequestCommand != null );

            _pagedRequest = pagedRequest;
            _dataPageRequestCommand = dataPageRequestCommand;
        }

        public int PageSize
        {
            get { return (int)_pagedRequest.PageSize; }
            set
            {
                uint prevPageIndex = _pagedRequest.PageIndex;
                _pagedRequest.PageSize = (uint)value;

                OnPropertyChanged();

                if ( prevPageIndex == _pagedRequest.PageIndex ) //event 'PageIndexChanged' has not been raised in this case so sends the request directly
                    _dataPageRequestCommand.ExecuteIfCan();
            }
        }

        public bool IsPagingDefined { get { return _pagedRequest.IsPagingDefined; } }

        public ICommand DataPageRequestCommand
        {
            get { return _dataPageRequestCommand; }
        }

        public void ReloadCurrentDataPage()
        {
            PageSize = PageSize; //sends the initial data request
        }
    }
}