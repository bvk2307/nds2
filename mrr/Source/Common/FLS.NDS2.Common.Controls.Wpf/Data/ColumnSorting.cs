﻿using System.ComponentModel;

namespace FLS.Common.Controls.Wpf.Data
{
    public sealed class ColumnSorting
    {
        public string UnuqueName { get; set; }
        
        public ListSortDirection SortDirection { get; set; }
    }
}