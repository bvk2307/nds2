﻿using System;
using System.Collections.Generic;
using FLS.Common.Controls.Wpf.Data.Extensions;
using FLS.Common.Lib.Collections.Extensions;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;

namespace FLS.Common.Controls.Wpf.Data
{
    public class PagedFilteredRequest : PagedSortedRequest
    {
        public IReadOnlyCollection<FilterSetting> Filters { get;  set; }

        /// <summary> Applies event arguments to this instance if they are appropriate. </summary>
        /// <param name="eventArgs"></param>
        /// <returns> 'true' if event arguments are appropriate to get the row number otherwise 'false'. </returns>
        public override bool ApplyRequestEventArgs( EventArgs eventArgs )
        {
            base.ApplyRequestEventArgs( eventArgs );

            GridViewFilteredEventArgs gridViewFilteredEventArgs = eventArgs as GridViewFilteredEventArgs;
            if ( gridViewFilteredEventArgs != null )
            {
                PageIndex = 0;  //resets to the first page index because filtered row number of a new filter can be different from value of old one

                CompositeFilterDescriptorCollection filterDescriptors = ( (RadGridView)gridViewFilteredEventArgs.Source ).FilterDescriptors;
                var filterSettings = new List<FilterSetting>( filterDescriptors.Count );
                foreach ( IFilterDescriptor filter in filterDescriptors )
                {
                    FilterSetting filterSetting = filter.ToFilterSettingOrNull();
                    if ( filterSetting != null )
                        filterSettings.Add( filterSetting );
                }
                Filters = filterSettings.Count > 0 ? filterSettings.ToReadOnly() : null;
            }
            return true;
        }
    }
}