﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using FLS.Common.Controls.Wpf.Data.Extensions;
using FLS.Common.Lib.Collections.Extensions;
using Telerik.Windows.Controls;

namespace FLS.Common.Controls.Wpf.Data
{
    public class PagedSortedRequest : PagedRequest
    {
        public IReadOnlyCollection<ColumnSorting> Sorting { get; set; }

        /// <summary> Applies event arguments to this instance if they are appropriate. </summary>
        /// <param name="eventArgs"></param>
        /// <returns> 'true' if event arguments are appropriate to get the row number otherwise 'false'. </returns>
        public override bool ApplyRequestEventArgs( EventArgs eventArgs )
        {
            bool isRowNumberRequested = base.ApplyRequestEventArgs( eventArgs );

            GridViewSortingEventArgs gridViewSortingEventArgs = eventArgs as GridViewSortingEventArgs;
            if ( gridViewSortingEventArgs != null )
            {
                isRowNumberRequested = false;

                SortingState sortingState = gridViewSortingEventArgs.NewSortingState;
                ListSortDirection? sortDirection = sortingState.ToListSortDirection();
                if ( sortDirection.HasValue )
                    Sorting = new[]
                    {
                        new ColumnSorting { UnuqueName = gridViewSortingEventArgs.Column.UniqueName, SortDirection = sortDirection.Value }
                    }.ToReadOnly();
                else
                    Sorting = null; //is it real case of reseting sort by user?
            }
            return isRowNumberRequested;
        }
    }
}