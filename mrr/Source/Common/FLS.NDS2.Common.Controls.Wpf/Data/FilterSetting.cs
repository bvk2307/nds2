﻿using Telerik.Windows.Data;

namespace FLS.Common.Controls.Wpf.Data
{
    public class FilterSetting
    {
        public string ColumnName { get; set; }

        public FilterDescriptor Filter1 { get; set; }
        
        public FilterCompositionLogicalOperator FieldFilterLogicalOperator { get; set; }
        
        public FilterDescriptor Filter2 { get; set; }

        //public IReadOnlyCollection<object> SelectedDistinctValues { get; }
    }
}