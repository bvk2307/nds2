﻿using System;
using Telerik.Windows.Controls;

namespace FLS.Common.Controls.Wpf.Data
{
    /// <summary> Data page request parameters. </summary>
    public class PagedRequest
    {
        public uint PageIndex { get; set; }

        public uint PageSize { get; set; }

        public bool IsPagingDefined { get { return PageSize > 0; } }

        /// <summary> Applies event arguments to this instance if they are appropriate. </summary>
        /// <param name="eventArgs"></param>
        /// <returns> 'true' if event arguments are appropriate to get the row number otherwise 'false'. </returns>
        public virtual bool ApplyRequestEventArgs( EventArgs eventArgs )
        {
            bool isRowNumberRequested = true;
            PageIndexChangedEventArgs pageIndexChangedEventArgs = eventArgs as PageIndexChangedEventArgs;
            if ( pageIndexChangedEventArgs != null )
            {
                isRowNumberRequested = false;
                
                PageIndex = (uint)pageIndexChangedEventArgs.NewPageIndex;
            }
            return isRowNumberRequested;
        }
    }
}