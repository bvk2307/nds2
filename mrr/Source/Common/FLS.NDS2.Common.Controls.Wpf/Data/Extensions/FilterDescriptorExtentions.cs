﻿using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;

namespace FLS.Common.Controls.Wpf.Data.Extensions
{
    /// <summary> Extension methods for <see cref="IFilterDescriptor"/>. </summary>
    public static class FilterDescriptorExtentions
    {
        public static FilterSetting ToFilterSettingOrNull( this IFilterDescriptor filter )
        {
            FilterSetting filterSetting = null;
            Telerik.Windows.Data.FilterDescriptor filterDescriptor = filter as Telerik.Windows.Data.FilterDescriptor;
            if ( filterDescriptor != null )
            {
                filterSetting = new FilterSetting
                {
                    ColumnName = filterDescriptor.Member,
                    Filter1 = new FilterDescriptor
                        { Operator = filterDescriptor.Operator, Value = filterDescriptor.Value }
                };
            }
            else
            {
                IColumnFilterDescriptor columnFilter = (IColumnFilterDescriptor)filter;
                filterSetting = new FilterSetting
                {
                    ColumnName = columnFilter.Column.UniqueName,
                    FieldFilterLogicalOperator = columnFilter.FieldFilter.LogicalOperator
                };
                OperatorValueFilterDescriptorBase operatorFilter = columnFilter.FieldFilter.Filter1;
                if ( operatorFilter.IsActive )
                    filterSetting.Filter1 = new FilterDescriptor
                        { Operator = operatorFilter.Operator, Value = operatorFilter.Value, IsCaseSensitive = operatorFilter.IsCaseSensitive };

                operatorFilter = columnFilter.FieldFilter.Filter2;
                if ( operatorFilter.IsActive )
                    filterSetting.Filter2 = new FilterDescriptor
                        { Operator = operatorFilter.Operator, Value = operatorFilter.Value, IsCaseSensitive = operatorFilter.IsCaseSensitive };
            }
            //'FilterSetting.SelectedDistinctValues' is not supported yet
            if ( filterSetting.Filter1 == null && filterSetting.Filter2 == null )
                filterSetting = null;

            return filterSetting;
        }
    }
}