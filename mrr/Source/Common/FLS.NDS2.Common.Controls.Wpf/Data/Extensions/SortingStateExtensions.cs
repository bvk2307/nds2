﻿using System.ComponentModel;
using Telerik.Windows.Controls;

namespace FLS.Common.Controls.Wpf.Data.Extensions
{
    /// <summary> Extension methods for <see cref="SortingState"/>. </summary>
    public static class SortingStateExtensions
    {
        public static ListSortDirection? ToListSortDirection( this SortingState sortingState )
        {
            ListSortDirection? direction = sortingState == SortingState.Descending 
                ? ListSortDirection.Descending : sortingState == SortingState.Ascending ? ListSortDirection.Ascending : (ListSortDirection?)null;

            return direction;
        }
    }
}