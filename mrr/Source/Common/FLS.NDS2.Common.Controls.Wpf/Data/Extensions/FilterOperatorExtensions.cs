﻿using FLS.Common.Lib.Data;
using Telerik.Windows.Data;

namespace FLS.Common.Controls.Wpf.Data.Extensions
{
    /// <summary> Extension methods for <see cref="FilterOperator"/>. </summary>
    public static class FilterOperatorExtensions
    {
        public static FilterComparisionOperator ToFilterComparisonOperator( this FilterOperator filterOperator )
        {
            FilterComparisionOperator filterComparisionOperator = FilterComparisionOperator.NotDefinedOperator;
            switch ( filterOperator )
            {
                case FilterOperator.Contains:
                    filterComparisionOperator = FilterComparisionOperator.Contains;
                    break;
                case FilterOperator.DoesNotContain:
                    filterComparisionOperator = FilterComparisionOperator.DoesNotContain;
                    break;
                case FilterOperator.EndsWith:
                    filterComparisionOperator = FilterComparisionOperator.EndsWith;
                    break;
                case FilterOperator.IsContainedIn:
                    filterComparisionOperator = FilterComparisionOperator.NotDefinedOperator;
                    break;
                case FilterOperator.IsEmpty:
                    filterComparisionOperator = FilterComparisionOperator.Equals;
                    break;
                case FilterOperator.IsEqualTo:
                    filterComparisionOperator = FilterComparisionOperator.Equals;
                    break;
                case FilterOperator.IsGreaterThan:
                    filterComparisionOperator = FilterComparisionOperator.GreaterThan;
                    break;
                case FilterOperator.IsGreaterThanOrEqualTo:
                    filterComparisionOperator = FilterComparisionOperator.GreaterThanOrEqualTo;
                    break;
                case FilterOperator.IsLessThan:
                    filterComparisionOperator = FilterComparisionOperator.LessThan;
                    break;
                case FilterOperator.IsLessThanOrEqualTo:
                    filterComparisionOperator = FilterComparisionOperator.LessThanOrEqualTo;
                    break;
                case FilterOperator.IsNotContainedIn:
                    filterComparisionOperator = FilterComparisionOperator.DoesNotMatch;
                    break;
                case FilterOperator.IsNotEmpty:
                    filterComparisionOperator = FilterComparisionOperator.NotEquals;
                    break;
                case FilterOperator.IsNotEqualTo:
                    filterComparisionOperator = FilterComparisionOperator.NotEquals;
                    break;
                case FilterOperator.IsNotNull:
                    filterComparisionOperator = FilterComparisionOperator.NotEquals;
                    break;
                case FilterOperator.IsNull:
                    filterComparisionOperator = FilterComparisionOperator.Equals;
                    break;
                case FilterOperator.StartsWith:
                    filterComparisionOperator = FilterComparisionOperator.StartsWith;
                    break;
            }
            return filterComparisionOperator;
        }
    }
}