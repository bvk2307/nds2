﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using FLS.Common.Lib.Collections.Extensions;
using FLS.Common.Lib.Data;
using Telerik.Windows.Data;

namespace FLS.Common.Controls.Wpf.Data.Extensions
{
    /// <summary> Extension methods for <see cref="PagedRequest"/>. </summary>
    public static class PagedRequestExtensions
    {
        public static QueryConditions ToQueryConditions<TRequest>( this TRequest request ) where TRequest : PagedRequest
        {
            Contract.Requires( request != null );
            Contract.Ensures( Contract.Result<QueryConditions>() != null );

            var queryConditions = new QueryConditions();
            if ( request.IsPagingDefined )
            {
                queryConditions.PaginationDetails.RowsToSkip = request.PageIndex * request.PageSize;
                queryConditions.PaginationDetails.RowsToTake = request.PageSize;
            }
            PagedSortedRequest pagedSortedRequest = request as PagedSortedRequest;
            if ( pagedSortedRequest != null )
            {
                if ( pagedSortedRequest.Sorting != null )
                {
                    ColumnSort[] columnSorts = ( from columnSorting in pagedSortedRequest.Sorting select 
                        new ColumnSort { ColumnKey = columnSorting.UnuqueName,
                                         Order = columnSorting.SortDirection } ).ToArray();
                    if ( columnSorts.Length > 0 )
                        queryConditions.Sorting = columnSorts.ToReadOnly();
                }
                PagedFilteredRequest pagedFilteredRequest = pagedSortedRequest as PagedFilteredRequest;
                if ( pagedFilteredRequest != null && pagedFilteredRequest.Filters != null )
                {
                    List<QueryDataFilter> filters = null;
                    foreach ( FilterSetting filterSetting in pagedFilteredRequest.Filters )
                    {
                        if ( filters == null )
                            filters = new List<QueryDataFilter>();

                        var filterQuery = new QueryDataFilter
                        {
                            UserDefined = true, ColumnName = filterSetting.ColumnName,
                            FilterOperator = filterSetting.FieldFilterLogicalOperator == FilterCompositionLogicalOperator.And 
                                                ? QueryDataFilter.FilterLogicalOperator.And : QueryDataFilter.FilterLogicalOperator.Or
                        };
                        List<ColumnFilter> columnFilters = null;
                        if ( filterSetting.Filter1 != null )
                        {
                            columnFilters = new List<ColumnFilter>( 2 );
                            columnFilters.Add( new ColumnFilter { ComparisonOperator = filterSetting.Filter1.Operator.ToFilterComparisonOperator(),
                                                                Value = filterSetting.Filter1.Value } );
                        }
                        if ( filterSetting.Filter2 != null )
                        {
                            if ( columnFilters == null )
                                columnFilters = new List<ColumnFilter>( 1 );
                            columnFilters.Add( new ColumnFilter { ComparisonOperator = filterSetting.Filter2.Operator.ToFilterComparisonOperator(),
                                                                Value = filterSetting.Filter2.Value } );
                        }
                        if ( columnFilters != null )
                            filterQuery.Filtering = columnFilters.ToReadOnly();

                        filters.Add( filterQuery );
                    }
                    if ( filters != null )
                        queryConditions.Filter = filters.ToReadOnly();
                }
            }

            return queryConditions;
        }
    }
}