﻿using Telerik.Windows.Data;

namespace FLS.Common.Controls.Wpf.Data
{
    public class FilterDescriptor
    {
        public FilterOperator Operator { get; set; }
        
        public object Value { get; set; }
        
        public bool IsCaseSensitive { get; set; }
    }
}