﻿using System.Windows.Input;

namespace FLS.Common.Controls.Wpf.Commands.Extensions
{
    /// <summary> Extension methods for <see cref="System.Windows.Input.ICommand"/>. </summary>
    public static class CommandExtentions
    {
        public static bool ExecuteIfCan( this ICommand command, object parameter = null )
        {
            bool executed = command.CanExecute( parameter );
            if ( executed )
                command.Execute( parameter );

            return executed;
        }
    }
}