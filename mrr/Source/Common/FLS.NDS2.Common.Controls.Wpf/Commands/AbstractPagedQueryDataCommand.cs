﻿using System;
using System.Diagnostics.Contracts;
using System.Windows.Input;
using FLS.Common.Controls.Wpf.Data;
using FLS.Common.Controls.Wpf.Data.Extensions;
using FLS.Common.Lib.Data;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace FLS.Common.Controls.Wpf.Commands
{
    [ContractClass( typeof(AbstractPagedQueryDataCommandContract) )]
    public abstract class AbstractPagedQueryDataCommand : ICommand
    {
        private readonly PagedRequest _pagedRequest;

        protected AbstractPagedQueryDataCommand( PagedRequest pagedRequest )
        {
            _pagedRequest = pagedRequest;
        }

        public void Execute( PagedRequest pagedRequest, bool isRowNumberRequested = false )
        {
            Contract.Requires( pagedRequest != null );

            QueryConditions queryConditions = pagedRequest.ToQueryConditions();

            SheduleCommandExecution( isRowNumberRequested, queryConditions );
        }

        protected abstract void SheduleCommandExecution( bool isRowNumberRequested, QueryConditions queryConditions );
 
        #region Implementation of ICommand

        /// <summary>Defines the method to be called when the command is invoked.</summary>
        /// <param name="parameter">Data used by the command.  If the command does not require data to be passed, this object can be set to null.</param>
        public void Execute( object parameter = null )
        {
            Contract.Requires( parameter == null || parameter is PageIndexChangedEventArgs || parameter is GridViewSortingEventArgs || parameter is GridViewFilteredEventArgs );

            bool isRowNumberRequested = true;
            if ( parameter != null )
                isRowNumberRequested = _pagedRequest.ApplyRequestEventArgs( (System.EventArgs)parameter );

            Execute( _pagedRequest, isRowNumberRequested );
        }

        /// <summary>Defines the method that determines whether the command can execute in its current state.</summary>
        /// <returns>true if this command can be executed; otherwise, false.</returns>
        /// <param name="parameter">Data used by the command.  If the command does not require data to be passed, this object can be set to null.</param>
        public bool CanExecute( object parameter = null ) { return true; }

        public event EventHandler CanExecuteChanged;

        #endregion Implementation of ICommand
    }

    [ContractClassFor( typeof(AbstractPagedQueryDataCommand) )]
    internal abstract class AbstractPagedQueryDataCommandContract : AbstractPagedQueryDataCommand
    {
        public AbstractPagedQueryDataCommandContract( PagedRequest pagedRequest ) : base( pagedRequest )
        {
            Contract.Requires( pagedRequest != null );

            throw new NotImplementedException();
        }

        protected override void SheduleCommandExecution( bool isRowNumberRequested, QueryConditions queryConditions )
        {
            Contract.Requires( queryConditions != null );

            throw new NotImplementedException();
        }
    }
}