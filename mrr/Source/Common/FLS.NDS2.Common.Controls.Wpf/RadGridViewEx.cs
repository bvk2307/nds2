﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows;

using FLS.Common.Controls.Wpf.Enums;
using FLS.Common.Controls.Wpf.GridView;
using Telerik.Windows;
using Telerik.Windows.Controls;

namespace FLS.Common.Controls.Wpf
{
    public class RadGridViewEx : RadGridView
    {
        private NumericalDimensions _numericalDimension = NumericalDimensions.Units;

        public event EventHandler<RoutedEventArgs> ColumnsUcOptionsChanged
        {
            add
            {
                AddHandler( GridViewDataControlEvents.ColumnsUcOptionsChangedEvent, value );
            }
            remove
            {
                RemoveHandler( GridViewDataControlEvents.ColumnsUcOptionsChangedEvent, value );
            }
        }

        /// <summary> Occurs on exporting to MS Excel. </summary>
        public event EventHandler<CancelRoutedEventArgs> ExportingToExcel
        {
            add
            {
                AddHandler( GridViewDataControlEvents.ExportingToExcelEvent, value );
            }
            remove
            {
                RemoveHandler( GridViewDataControlEvents.ExportingToExcelEvent, value );
            }
        }

        public event EventHandler<RadRoutedEventArgs> ExportedToExcel
        {
            add
            {
                this.AddHandler( GridViewDataControlEvents.ExportedToExcelEvent, value );
            }
            remove
            {
                this.RemoveHandler( GridViewDataControlEvents.ExportedToExcelEvent, value );
            }
        }

        public NumericalDimensions NumericalDimension
        {
            get
            {
                return _numericalDimension;
            }
            set
            {
                _numericalDimension = value;
                OnPropertyChanged("NumericalDimension");
            }
        }

        protected override void OnInitialized( EventArgs e )
        {
            ClipboardCopyMode = GridViewClipboardCopyMode.Cells;

            var controlPanelCollection = new ControlPanelCollection();
            ControlPanelItems = controlPanelCollection;
            
            NumericalDimension = NumericalDimensions.Units;

            base.OnInitialized( e );

            SubscribeOnColumnPropertyChanged( Columns );
            Columns.CollectionChanged       += OnColumnsCollectionChanged;

            ColumnWidthChanged              += OnColumnWidthChanged;
            CopyingCellClipboardContent     += CopyingCellContent;

            controlPanelCollection.ClickExportToExcel   += OnClickExportToExcel;
            controlPanelCollection.ClickClearFilters    += OnClickClearFilters;
            controlPanelCollection.DimensionChanged     += DimensionChanged;
        }

        private void SubscribeOnColumnPropertyChanged( IEnumerable<GridViewColumn> columns )
        {
            foreach ( GridViewColumn gridViewColumn in columns )
            {
                gridViewColumn.PropertyChanged += OnGridViewColumnPropertyChanged;
            }
        }

        private void UnsubscribeOnColumnPropertyChanged( IEnumerable<GridViewColumn> columns )
        {
            foreach ( GridViewColumn gridViewColumn in columns )
            {
                gridViewColumn.PropertyChanged -= OnGridViewColumnPropertyChanged;
            }
        }

        private void OnColumnsCollectionChanged( object sender, NotifyCollectionChangedEventArgs notifyCollectionChangedEventArgs )
        {
            if ( notifyCollectionChangedEventArgs.Action == NotifyCollectionChangedAction.Reset )
            {
                UnsubscribeOnColumnPropertyChanged( Columns );
            }
            else if ( notifyCollectionChangedEventArgs.Action != NotifyCollectionChangedAction.Move )
            {
                if ( notifyCollectionChangedEventArgs.NewItems != null )
                    SubscribeOnColumnPropertyChanged( notifyCollectionChangedEventArgs.NewItems.Cast<GridViewColumn>() );
                if ( notifyCollectionChangedEventArgs.OldItems != null )
                    UnsubscribeOnColumnPropertyChanged( notifyCollectionChangedEventArgs.OldItems.Cast<GridViewColumn>() );
            }
        }

        private void OnGridViewColumnPropertyChanged( object sender, PropertyChangedEventArgs propertyChangedEventArgs )
        {
            if ( propertyChangedEventArgs.PropertyName == GridViewDataControlColumnProperties.IsVisible )
                RaiseColumnsUcOptionsChanged( this );
        }

        /// <summary>Called when the column is reordered.</summary>
        /// <param name="e">GridViewColumnEventArgs containing the reordered column.</param>
        protected override void OnColumnReordered( GridViewColumnEventArgs e )
        {
            base.OnColumnReordered( e );

            RaiseColumnsUcOptionsChanged( this );
        }

        private void OnColumnWidthChanged( object sender, ColumnWidthChangedEventArgs columnWidthChangedEventArgs )
        {
            RaiseColumnsUcOptionsChanged( sender );
        }

        private void RaiseColumnsUcOptionsChanged( object sender )
        {
            RaiseEvent( new RadRoutedEventArgs( GridViewDataControlEvents.ColumnsUcOptionsChangedEvent, sender ) );
        }

        private void CopyingCellContent(object sender, GridViewCellClipboardEventArgs e)
        {
            if (e.Cell != CurrentCellInfo)
            {
                e.Cancel = true;
            }
        }
        
        private void DimensionChanged(object sender, RoutedEventArgs e)
        {
            foreach (var column in this.Columns.OfType<GridViewDataColumn>())
            {
                if (((GridViewDataColumn)column).DataType == typeof(decimal))
                {
                    if (NumericalDimension == NumericalDimensions.Millions)
                    {
                        ((GridViewDataColumn)column).DataFormatString = " {0:#,###,,.00,} ";
                    }

                    if (NumericalDimension == NumericalDimensions.Thousands)
                    {
                        ((GridViewDataColumn)column).DataFormatString = " {0:#,###,.00,} ";
                    }

                    if (NumericalDimension == NumericalDimensions.Units)
                    {
                        ((GridViewDataColumn)column).DataFormatString = " {0:N2} ";
                    }
                }
            }
        }

        private void OnClickClearFilters(object sender, RoutedEventArgs e)
        {
            this.FilterDescriptors.SuspendNotifications();
            this.SortDescriptors.SuspendNotifications();

            foreach (GridViewColumn column in this.Columns)
            {
                column.SortingState = SortingState.None;
                column.ClearFilters();
            }

            this.FilterDescriptors.Clear();
            this.SortDescriptors.Clear();

            this.SortDescriptors.ResumeNotifications();
            this.FilterDescriptors.ResumeNotifications();
        }
        
        private void OnClickExportToExcel( object sender, RoutedEventArgs routedEventArgs )
        {
            var cancelRoutedEventArgs = new CancelRoutedEventArgs( GridViewDataControlEvents.ExportingToExcelEvent, sender );
            RaiseEvent( cancelRoutedEventArgs );

            if ( !cancelRoutedEventArgs.Cancel )
                RaiseEvent( new RadRoutedEventArgs( GridViewDataControlEvents.ExportedToExcelEvent, sender ) );
        }
    }
}