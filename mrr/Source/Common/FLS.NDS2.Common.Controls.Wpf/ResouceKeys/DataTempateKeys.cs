﻿using System.Windows;

namespace FLS.Common.Controls.Wpf.ResouceKeys
{
    public class RadColorPickerForegroundBrushKey
    {
        public static ComponentResourceKey Key { get { return new ComponentResourceKey( typeof(RadColorPickerForegroundBrushKey), "TelerikRadColorPickerForegroundBrush" ); } }
    }

    public class RadColorPickerBackgroundBrushKey
    {
        public static ComponentResourceKey Key { get { return new ComponentResourceKey( typeof(RadColorPickerBackgroundBrushKey), "TelerikRadColorPickerBackgroundBrush" ); } }
    }

    public class RadColorPickerBorderBrushKey
    {
        public static ComponentResourceKey Key { get { return new ComponentResourceKey( typeof(RadColorPickerBorderBrushKey), "TelerikRadColorPickerBorderBrush" ); } }
    }
}