﻿using System.Windows;

namespace FLS.Common.Controls.Wpf.ResouceKeys
{
    public class ColorToBrushByColorConverterKey
    {
        public static ComponentResourceKey Key { get { return new ComponentResourceKey( typeof(ColorToBrushByColorConverterKey), "ColorToBrushByColorConverterKey" ); } }
    }
}