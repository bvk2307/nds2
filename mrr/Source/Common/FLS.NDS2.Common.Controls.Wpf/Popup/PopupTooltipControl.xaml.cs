﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media.Animation;

using Telerik.Windows.Controls;

namespace FLS.Common.Controls.Wpf.Popup
{
    /// <summary>
    /// Interaction logic for PopupTooltipControl.xaml
    /// </summary>
    public partial class PopupTooltipControl : System.Windows.Controls.Primitives.Popup
    {
        private bool showCloseButton = true;

        public event PropertyChangedEventHandler PropertyChanged;

        #region DependencyProperty Content

        /// <summary>
        /// Registers a dependency property as backing store for the Content property
        /// </summary>
        public static readonly DependencyProperty ContentProperty =
            DependencyProperty.Register("Content", typeof(object), typeof(PopupTooltipControl),
            new FrameworkPropertyMetadata(null,
                  FrameworkPropertyMetadataOptions.AffectsRender |
                  FrameworkPropertyMetadataOptions.AffectsParentMeasure));

        /// <summary>
        /// Gets or sets the Content.
        /// </summary>
        /// <value>The Content.</value>
        public object Content
        {
            get { return (object)GetValue(ContentProperty); }
            set { SetValue(ContentProperty, value); }
        }

        #endregion

        public bool ShowCloseButton
        {
            get { return showCloseButton; }
            set
            {
                showCloseButton = value;
                OnPropertyChanged("ShowCloseButton");
            }
        }

        public bool MouseOverPopup { get; set; }
        
        public PopupTooltipControl()
        {
            InitializeComponent();
        }
        
        private void MouseOver_OnCompleted(object sender, EventArgs e)
        {
            MouseOverPopup = true;
            IsOpen = true;
        }

        private void MouseLeave_OnCompleted(object sender, EventArgs e)
        {
            MouseOverPopup = false;
            IsOpen = false;
        }
        
        private void CloseOnClick(object sender, RoutedEventArgs e)
        {
            MouseOverPopup = false;
            IsOpen = false;
        }

        // Create the OnPropertyChanged method to raise the event
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
