﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FLS.Common.Controls.Wpf.Enums
{
    public enum NumericalDimensions
    {
        Units,
        Thousands,
        Millions
    }
}
