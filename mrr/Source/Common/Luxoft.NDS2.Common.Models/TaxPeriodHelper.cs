﻿using Luxoft.NDS2.Common.Contracts.DTO.TaxPeriod;
using System;

namespace Luxoft.NDS2.Common.Models
{
    public enum PeriodToDateComparisonResult { Before, After, Contains }

    public static class TaxPeriodHelper
    {
        public static int MonthsPerPeriod = 3;

        public static DateTime PeriodStart(this EffectiveTaxPeriod taxPeriod)
        {
            return new DateTime(taxPeriod.Year, ((int)taxPeriod.Quarter - 1) * MonthsPerPeriod + 1, 1);
        }

        public static DateTime PeriodEnd(this EffectiveTaxPeriod taxPeriod)
        {
            return taxPeriod.PeriodStart().AddMonths(MonthsPerPeriod).AddDays(-1);
        }

        public static PeriodToDateComparisonResult Compare(this EffectiveTaxPeriod taxPeriod, DateTime date)
        {
            if (date < taxPeriod.PeriodStart())
            {
                return PeriodToDateComparisonResult.After;
            }

            if (date <= taxPeriod.PeriodEnd())
            {
                return PeriodToDateComparisonResult.Contains;
            }

            return PeriodToDateComparisonResult.Before;
        }
    }
}
