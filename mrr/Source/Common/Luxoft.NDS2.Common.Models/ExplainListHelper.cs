﻿using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using Luxoft.NDS2.Common.Models.KnpDocuments;

namespace Luxoft.NDS2.Common.Models
{
    public static class ExplainListHelper
    {
        public static List<ExplainListItemViewModel> CreateExplainListModel(
            this ClaimExplain item, 
            bool allowEdit,
            bool allowView, 
            ClaimExplain[] allManualExplains,
            bool isCorrectionAnnulment)
        {
            List<ExplainListItemViewModel> models = new List<ExplainListItemViewModel>();
            if(item.HasControlRatioExplain)
                models.Add(new ControlRatioExplainModel(item, allowEdit, allowView));
            if(item.HasInvoicesExplain)
                models.Add(new TksInvoiceExplainModel(item, allowEdit, allowView));
            if(!item.ReceivedByTks)
                models.Add(new ManualExplainModel(item, allowEdit, allowView, allManualExplains, isCorrectionAnnulment));
            
            return models;
        }
    }
}
