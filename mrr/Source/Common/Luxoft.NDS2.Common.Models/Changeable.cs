﻿using System;

namespace Luxoft.NDS2.Common.Models
{
    public class Changeable<T>
    {
        private T _value;

        public Changeable(T initialValue)
        {
            _value = initialValue;
        }

        public Changeable()
            : this(default(T))
        {
        }

        public T Value
        {
            get
            {
                return _value;
            }
            set
            {
                if (!_value.Equals(value))
                {
                    _value = value;
                    RaisePropertyChanged();
                }
            }
        }

        public event EventHandler PropertyChanged;

        private void RaisePropertyChanged()
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new EventArgs());
            }
        }
    }
}
