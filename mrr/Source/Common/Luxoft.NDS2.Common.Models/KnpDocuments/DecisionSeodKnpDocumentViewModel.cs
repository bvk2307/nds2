﻿using Luxoft.NDS2.Common.Contracts.DTO.KnpDocuments;

namespace Luxoft.NDS2.Common.Models.KnpDocuments
{
    public class DecisionSeodKnpDocumentViewModel : SeodKnpDocumentViewModel
    {
        private readonly string _name;
        public DecisionSeodKnpDocumentViewModel(SeodKnpDocument data) : base(data)
        {
            _name = data.Name;
        }

        public override string Name
        {
            get { return _name; }
        }
    }
}