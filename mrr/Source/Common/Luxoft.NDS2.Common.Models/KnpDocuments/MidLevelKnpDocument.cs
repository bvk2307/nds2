﻿using System.ComponentModel;

namespace Luxoft.NDS2.Common.Models.KnpDocuments
{
    public abstract class MidLevelKnpDocument : KnpDocument, IGridViewDataSource<KnpDocument>
    {
        protected MidLevelKnpDocument()
        {
            ListItems = new BindingList<KnpDocument>();
        }

        public BindingList<KnpDocument> ListItems
        {
            get;
            private set;
        }
    }
}
