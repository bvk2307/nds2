﻿using Luxoft.NDS2.Common.Contracts.AccessRestriction;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using Luxoft.NDS2.Common.Contracts.DTO.KnpDocuments;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Models.AccessContext;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Common.Models.KnpDocuments
{
    public abstract class ClaimViewModel : MidLevelKnpDocument, IListViewModel<KnpDocument, ClaimExplain>
    {
        private readonly string _sonoCode;

        private readonly OperationModel[] _operations;

        private readonly KnpDocumentClaim _data;

        # region .ctor

        protected ClaimViewModel(string sonoCode, KnpDocumentClaim data, Contracts.DTO.AccessRestriction.AccessContext accessContext)
        {
            _sonoCode = sonoCode;
            _data = data;
            _operations = accessContext.ToModel();
        }

        # endregion

        # region Пояснения

        public void UpdateList(IEnumerable<ClaimExplain> dataItems)
        {
            var allowEdit = _operations.Any(x => x.Name == MrrOperations.ExplainReply.ExplainEdit && x.Allow(_sonoCode));
            var allowView = _operations.Any(x => x.Name == MrrOperations.ExplainReply.ExplainView && x.Allow(_sonoCode));

            var dataItemsArray = dataItems.ToArray(); 

            ListItems.Clear();
            foreach (var item in dataItemsArray)
            {
                var models = item.CreateExplainListModel(allowEdit, 
                                                         allowView, 
                                                         dataItemsArray,
                                                         Utils.IsKnpClosedByAnnulment(_data.CloseReason));
                models.ForEach(x => ListItems.Add(x));
            }
        }

        # endregion

        # region Атрибуты Автотребования

        public override bool IsHighlighted
        {
            get { return false; }
        }

        public override string Number
        {
            get
            {
                return _data == null
                    ? string.Empty
                    : (!string.IsNullOrEmpty(_data.Number)
                        ? _data.Number
                        : string.Format("*{0}", _data.Id));
            }
        }

        public override DateTime? Date
        {
            get
            {
                return _data == null 
                    ? null 
                    : (!string.IsNullOrEmpty(_data.Number) ? _data.Date : null);
            }
        }

        public override string Status
        {
            get { return  _data == null ? string.Empty : _data.Status; }
        }

        public override long GetId()
        {
            return _data == null ? default(long) : _data.Id;
        }

        public override DateTime? StatusDate
        {
            get {return _data == null ? null : _data.SeodDate; }
        }

        # endregion

        # region Разграничение прав на доступ

        public override bool AllowView()
        {
            return true;
        }

        public override bool AllowEdit()
        {
            return false;
        }

        # endregion
    }
}
