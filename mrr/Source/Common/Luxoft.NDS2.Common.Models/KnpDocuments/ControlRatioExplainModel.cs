﻿using Luxoft.NDS2.Common.Contracts.DTO.Explain;

namespace Luxoft.NDS2.Common.Models.KnpDocuments
{
    public class ControlRatioExplainModel : ExplainListItemViewModel
    {
        public ControlRatioExplainModel(ClaimExplain data, bool allowEdit, bool allowView)
            : base(data, allowEdit, allowView)
        {
        }

        public override string Name
        {
            get { return "Пояснение по КС"; }
        }

        protected override bool CanBeViewed()
        {
            return true;
        }

        public override bool IsHighlighted
        {
            get { return false; }
        }
    }
}
