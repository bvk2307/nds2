﻿using System;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;

namespace Luxoft.NDS2.Common.Models.KnpDocuments
{
    public class OtherReasonExplainModel : MidLevelKnpDocument
    {
        private readonly bool _allowView;
        private readonly bool _allowEdit;
        private readonly ClaimExplain _data;

        public OtherReasonExplainModel(ClaimExplain data, bool allowEdit, bool allowView)
        {
            _data = data;
            _allowEdit = allowEdit;
            _allowView = allowView;
        }

        public override string Name
        {
            get { return "Пояснение по иным основаниям"; }
        }


        public override long GetId()
        {
            return _data.Id;
        }

        public override bool AllowView()
        {
            return _allowView;
        }

        public override bool AllowEdit()
        {
            return _allowEdit;
        }

        public override bool IsHighlighted
        {
            get { return false; }
        }

        public override string Number
        {
            get
            {
                return _data.Number;
            }
        }

        public override DateTime? Date
        {
            get
            {
                return _data.Date;
            }
        }

        public override string Status
        {
            get
            {
                return _data.StatusName;
            }
        }

        public override DateTime? StatusDate
        {
            get
            {
                return _data.StatusDate;
            }
        }
    }
}
