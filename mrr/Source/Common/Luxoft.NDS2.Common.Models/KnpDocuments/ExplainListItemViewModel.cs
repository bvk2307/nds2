﻿using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using System;

namespace Luxoft.NDS2.Common.Models.KnpDocuments
{
    public abstract class ExplainListItemViewModel : KnpDocument
    {
        # region .ctor

        protected readonly ClaimExplain _data;

        protected ExplainListItemViewModel(ClaimExplain data, bool allowEdit, bool allowView)
        {
            _data = data;
            _allowEdit = allowEdit;
            _allowView = allowView;
        }

        # endregion

        # region Данные

        public override string Number
        {
            get { return _data.Number; }
        }

        public override DateTime? Date
        {
            get { return _data.Date; }
        }

        public override string Status
        {
            get { return _data.StatusName; }
        }

        public override DateTime? StatusDate
        {
            get { return _data.StatusDate; }
        }
        # endregion

        # region Редактирование\Просмотр

        private readonly bool _allowEdit;

        public override bool AllowEdit()
        {
            return _allowEdit && CanBeEdited();
        }

        protected virtual bool CanBeEdited()
        {
            return false;
        }

        private readonly bool _allowView;

        public override bool AllowView()
        {
            return _allowView && CanBeViewed();
        }

        protected abstract bool CanBeViewed();

        public override long GetId()
        {
            return _data.Id;
        }

        public string TypeName
        {
            get { return _data.TypeName; }
        }
        # endregion
    }
}
