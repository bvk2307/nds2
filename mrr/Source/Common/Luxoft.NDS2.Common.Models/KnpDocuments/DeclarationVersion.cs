﻿using Luxoft.NDS2.Common.Contracts.DTO.KnpDocuments;
using System.ComponentModel;

namespace Luxoft.NDS2.Common.Models.KnpDocuments
{
    public class DeclarationVersion : KnpDocument, IGridViewDataSource<MidLevelKnpDocument>
    {

        private readonly KnpDocumentDeclaration _data;

        public DeclarationVersion(KnpDocumentDeclaration data)
        {
            _data = data;
            ListItems = new BindingList<MidLevelKnpDocument>();
        }

        public BindingList<MidLevelKnpDocument> ListItems
        {
            get;
            protected set;
        }

        public void UpdateData(KnpDocumentClaim[] claims)
        {
        }

        public override bool IsHighlighted
        {
            get { return false; }
        }

        public override string Name
        {
            get { return "Корректировка"; }
        }

        public override string Number
        {
            get
            {
                return 
                    !string.IsNullOrEmpty(_data.InnReorganized) 
                    ? string.Format("{0}({1})", _data.CorrectionNumber, _data.InnReorganized)
                    : _data.CorrectionNumber.ToString();
            }
        }

        public override System.DateTime? Date
        {
            get {return _data.SubmittedAt; }
        }

        public override string Status
        {
            get { return _data.Status == 0 ? "Неактуальная" : "Актуальная"; }
        }

        public override System.DateTime? StatusDate
        {
            get { return _data.SubmittedAt; }
        }

        public override long GetId()
        {
            return default(long);
        }

        public override bool AllowView()
        {
            return true;
        }

        public override bool AllowEdit()
        {
            return false;
        }
    }
}
