﻿using Luxoft.NDS2.Common.Contracts.DTO.KnpDocuments;

namespace Luxoft.NDS2.Common.Models.KnpDocuments
{
    public class ActSeodKnpDocumentViewModel : SeodKnpDocumentViewModel
    {
        public ActSeodKnpDocumentViewModel(SeodKnpDocument data) : base(data)
        {
        }

        public override string Name
        {
            get { return "Акт КНП"; }
        }
    }
}
