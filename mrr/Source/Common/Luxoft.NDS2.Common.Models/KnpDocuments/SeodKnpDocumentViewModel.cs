﻿using System;
using Luxoft.NDS2.Common.Contracts.DTO.KnpDocuments;

namespace Luxoft.NDS2.Common.Models.KnpDocuments
{
    public abstract class SeodKnpDocumentViewModel : KnpDocument
    {
        private readonly SeodKnpDocument _data;
        public SeodKnpDocumentViewModel(SeodKnpDocument data)
        {
            _data = data;
        }

        public override string Number
        {
            get { return _data.Number; }
        }

        public override DateTime? Date
        {
            get { return _data.Date; }
        }

        public override string Status
        {
            get { return string.Empty; }
        }

        public override DateTime? StatusDate
        {
            get { return null; }
        }

        public override long GetId()
        {
            return _data.Id;
        }

        public override bool AllowView()
        {
            return false;
        }

        public override bool AllowEdit()
        {
            return false;
        }

        protected bool CanBeViewed()
        {
            return false;
        }

        public override bool IsHighlighted
        {
            get { return false; }
        }
    }
}
