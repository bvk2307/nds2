﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using Luxoft.NDS2.Common.Contracts.DTO.KnpDocuments;

namespace Luxoft.NDS2.Common.Models.KnpDocuments
{
    public class ReclaimInvoiceViewModel : ClaimViewModel
    {
        public ReclaimInvoiceViewModel(string sonoCode, KnpDocumentClaim data, Contracts.DTO.AccessRestriction.AccessContext accessContext)
            : base(sonoCode, data, accessContext)
        {
        }

        public override string Name
        {
            get { return "Автоистребование по ст. 93"; }
        }
    }
}
