﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using System.Linq;

namespace Luxoft.NDS2.Common.Models.KnpDocuments
{
    public class ManualExplainModel : ExplainListItemViewModel
    {
        private readonly bool _requiresInput;
        private readonly bool _previousNotProcessedExists;
        private readonly bool _isCorrectionAnnulment;
        
        
        public ManualExplainModel(ClaimExplain data, bool allowEdit, bool allowView, ClaimExplain[] allManualExplainsData, bool isCorrectionAnnulment)
            : base(data, allowEdit, allowView)
        {
            _requiresInput = !isCorrectionAnnulment && (data.Status == ExplainReplyStatus.NotProcessed);

            _previousNotProcessedExists =
                allManualExplainsData.Any(
                    x => x.Status == ExplainReplyStatus.NotProcessed
                        && x.Id < data.Id);

            _isCorrectionAnnulment = isCorrectionAnnulment;
        }

        public override string Name
        {
            get { return TypeName == "Пояснение" ? "Неформализованное пояснение" : TypeName; }
        }

        protected override bool CanBeViewed()
        {
            return !_requiresInput && !_isCorrectionAnnulment;
        }

        protected override bool CanBeEdited()
        {
            return _requiresInput && !_previousNotProcessedExists && !_isCorrectionAnnulment;
        }

        public override bool IsHighlighted
        {
            get
            {
                return _requiresInput && !_isCorrectionAnnulment;
            }
        }
    }
}
