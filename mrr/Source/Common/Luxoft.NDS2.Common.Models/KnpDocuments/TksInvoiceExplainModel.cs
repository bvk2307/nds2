﻿using Luxoft.NDS2.Common.Contracts.DTO.Explain;

namespace Luxoft.NDS2.Common.Models.KnpDocuments
{
    public class TksInvoiceExplainModel : ExplainListItemViewModel
    {
        public TksInvoiceExplainModel(ClaimExplain data, bool allowEdit, bool allowView)
            : base(data, allowEdit, allowView)
        {
        }

        public override string Name
        {
            get { return "Формализованное пояснение"; }
        }

        protected override bool CanBeViewed()
        {
            return true;
        }

        public override bool IsHighlighted
        {
            get { return false; }
        }
    }
}
