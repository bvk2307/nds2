﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Models.KnpDocuments
{
    class OtherMnkViewModel : MidLevelKnpDocument
    {
        public override bool IsHighlighted
        {
            get { return false; }
        }

        public override string Name
        {
            get { return "Прочие МНК"; }
        }

        public override string Number
        {
            get { return string.Empty; }
        }

        public override DateTime? Date
        {
            get { return null; }
        }

        public override string Status
        {
            get { return string.Empty; }
        }

        public override DateTime? StatusDate
        {
            get { return null; }
        }

        public override long GetId()
        {
            return default(long);
        }

        public override bool AllowView()
        {
            return false;
        }

        public override bool AllowEdit()
        {
            return false;
        }
    }
}
