﻿using Luxoft.NDS2.Common.Contracts.DTO.KnpDocuments;

namespace Luxoft.NDS2.Common.Models.KnpDocuments
{
    public class ClaimInvoiceViewModel : ClaimViewModel
    {
        public ClaimInvoiceViewModel(string sonoCode, KnpDocumentClaim data, Contracts.DTO.AccessRestriction.AccessContext accessContext)
            : base(sonoCode, data, accessContext)
        {
        }

        public override string Name
        {
            get { return "Автотребование по СФ"; }
        }
    }
}
