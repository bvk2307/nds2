﻿using Luxoft.NDS2.Common.Contracts.AccessRestriction;
using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using Luxoft.NDS2.Common.Contracts.DTO.KnpDocuments;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Models.AccessContext;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Luxoft.NDS2.Common.Models.KnpDocuments
{
    public class DeclarationKnpDocumentsModel : IGridViewDataSource<DeclarationVersion>
    {
        private readonly string _inn;

        private readonly string _kppEffective;

        private readonly int _fiscalYear;

        private readonly int _periodEffective;

        private readonly Common.Contracts.DTO.AccessRestriction.AccessContext _explainAccessContext;

        public DeclarationKnpDocumentsModel(
            Common.Contracts.DTO.AccessRestriction.AccessContext explainAccessContext,
            string inn,
            string kppEffective,
            int fisclaYear,
            int periodEffective)
        {
            _explainAccessContext = explainAccessContext;
            _inn = inn;
            _kppEffective = kppEffective;
            _fiscalYear = fisclaYear;
            _periodEffective = periodEffective;

            ListItems = new BindingList<DeclarationVersion>();
        }

        public BindingList<DeclarationVersion> ListItems
        {
            get;
            private set;
        }

        public string GetInn()
        {
            return _inn;
        }

        public string InnContractor { get; set; }

        public string GetKppEffective()
        {
            return _kppEffective;
        }

        public int GetFiscalYear()
        {
            return _fiscalYear;
        }

        public int GetPeriodEffective()
        {
            return _periodEffective;
        }

        public void BuildTree(
            List<KnpDocumentDeclaration> declarationVersions,
            List<KnpDocumentClaim> declarationClaims,
            List<ClaimExplain> claimExplains,
            List<SeodKnpDocument> acts,
            List<SeodKnpDocument> decisions)
        {
            ListItems.Clear();

            foreach (var declarationVersion in declarationVersions)
            {
                var operations = _explainAccessContext.ToModel();
                var allowEdit = operations.Any(x => x.Name == MrrOperations.ExplainReply.ExplainEdit && x.Allow(declarationVersion.SonoCodeSubmitted)) && !declarationVersion.IsAnnulmentSeod;
                var allowView = operations.Any(x => x.Name == MrrOperations.ExplainReply.ExplainView && x.Allow(declarationVersion.SonoCodeSubmitted)) && !declarationVersion.IsAnnulmentSeod;

                var declarationModel = new DeclarationVersion(declarationVersion);
                var claims =
                    declarationClaims.Where(x => x.DeclarationRegistrationNumber == declarationVersion.RegistrationNumber);
                foreach (var claim in claims)
                {
                    ClaimViewModel claimModel = null;
                    if (!TryCreateClaimModel(declarationVersion, claim, out claimModel))
                        continue;

                    var explains = claimExplains.Where(x => x.ClaimId == claim.Id);
                    foreach (var explain in explains)
                    {
                        var explainModels = explain.CreateExplainListModel(
                            allowEdit,
                            allowView,
                            explains.Where(x => !x.ReceivedByTks).ToArray(),
                            Utils.IsKnpClosedByAnnulment(declarationVersion.KnpCloseReasonId));
                        explainModels.ForEach(x => claimModel.ListItems.Add(x));
                    }

                    declarationModel.ListItems.Add(claimModel);
                }

                var explainOtherReasons = claimExplains.Where(x => x.HasOtherReasonExplain);
                foreach (var explainOtherReason in explainOtherReasons)
                {
                    var explainOtherReasonModel = new OtherReasonExplainModel(explainOtherReason, allowEdit, allowView);

                    declarationModel.ListItems.Add(explainOtherReasonModel);
                }

                var otherMnk = new List<SeodKnpDocumentViewModel>();
                var declarationActs =
                    acts.Where(
                        x =>
                            x.DeclarationRegistrationNumber ==
                            declarationVersion.RegistrationNumber.GetValueOrDefault());

                foreach (var act in declarationActs)
                {
                    var actModel = new ActSeodKnpDocumentViewModel(act);
                    otherMnk.Add(actModel);
                }

                var declarationDecisions =
                    decisions.Where(
                        x =>
                            x.DeclarationRegistrationNumber ==
                            declarationVersion.RegistrationNumber.GetValueOrDefault());

                foreach (var decision in declarationDecisions)
                {
                    var decisionModel = new DecisionSeodKnpDocumentViewModel(decision);
                    otherMnk.Add(decisionModel);
                }
                if (otherMnk.Any())
                {
                    var otherMnkModel = new OtherMnkViewModel();
                    otherMnk.ForEach(x => otherMnkModel.ListItems.Add(x));
                    declarationModel.ListItems.Add(otherMnkModel);
                }
                ListItems.Add(declarationModel);
            }
        }

        private bool TryCreateClaimModel(KnpDocumentDeclaration declarationVersion, KnpDocumentClaim claim, out ClaimViewModel claimModel)
        {
            claimModel = null;
            switch (claim.Type)
            {
                case (int)DocumentType.ClaimControlRatio:
                    if (claim.HasDiscrepancies)
                        claimModel = new ClaimControlRatioViewModel(declarationVersion.SonoCodeSubmitted, claim, _explainAccessContext);
                    break;
                case (int)DocumentType.ClaimInvoice:
                    claimModel = new ClaimInvoiceViewModel(declarationVersion.SonoCodeSubmitted, claim, _explainAccessContext);
                    break;
                case (int)DocumentType.Reclaim:
                    claimModel = new ReclaimInvoiceViewModel(declarationVersion.SonoCodeSubmitted, claim, _explainAccessContext);
                    break;
            }
            return claimModel == null ? false : true;
        }
    }
}
