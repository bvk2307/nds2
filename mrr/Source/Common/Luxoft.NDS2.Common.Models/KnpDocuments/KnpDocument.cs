﻿using System;

namespace Luxoft.NDS2.Common.Models.KnpDocuments
{
    public abstract class KnpDocument
    {
        /// <summary>
        /// Возвращает признак того, что элемент должен быть выделен средди прочих документов КНП
        /// </summary>
        public abstract bool IsHighlighted
        {
            get;
        }

        /// <summary>
        /// Возвращает наименование документа
        /// </summary>
        public abstract string Name
        {
            get;
        }

        /// <summary>
        /// Возвращает номер документа
        /// </summary>
        public abstract string Number
        {
            get;
        }

        /// <summary>
        /// Возвращает дату документа
        /// </summary>
        public abstract DateTime? Date
        {
            get;
        }

        /// <summary>
        /// Возвращает статус документа
        /// </summary>
        public abstract string Status
        {
            get;
        }

        /// <summary>
        /// Возвращает дату установки статуса документа
        /// </summary>
        public abstract DateTime? StatusDate
        {
            get;
        }

        public abstract long GetId();

        public abstract bool AllowView();

        public abstract bool AllowEdit();
    }
}
