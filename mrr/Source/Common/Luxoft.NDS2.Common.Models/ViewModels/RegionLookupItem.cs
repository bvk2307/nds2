﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;
using org = Luxoft.NDS2.Common.Contracts.DTO.OrganizationStructure;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Common.Models.ViewModels
{
    /// <summary>
    /// Этот класс реализует модель элемента справочника регионов
    /// </summary>
    public class RegionLookupItem : LookupItem
    {
        private RegionLookupItem(
            string code, 
            string name, 
            IEnumerable<InspectionLookupItem> inspections,
            int federalDistrictId = 0)
            : base(code, name)
        {
            Inspections = inspections;
            FederalDistrictId = federalDistrictId;
        }

        /// <summary>
        /// Создает новый экземпляр класса RegionLookupItem
        /// </summary>
        /// <param name="regionData">Сведения о регионе</param>
        public RegionLookupItem(Region regionData)
            : this(
                regionData.Code, 
                regionData.Name, 
                (regionData.Inspections ?? new List<Inspection>())
                    .Select(dto => new InspectionLookupItem(dto))
                    .ToArray())
        {
        }

        /// <summary>
        /// Создает новый экземпляр класса RegionLookupItem
        /// </summary>
        /// <param name="code">Код инспекции</param>
        /// <param name="name">Наименование инспекции</param>
        public RegionLookupItem(string code, string name)
            : this(code, name, Enumerable.Empty<InspectionLookupItem>())
        {
        }

        public RegionLookupItem(org.Region region, org.Sono[] allSono)
            : this(
                region.Code,
                region.Name,
                allSono.Where(
                    x => x.RegionCode == region.Code 
                        && (x.SonoType == org.SonoType.Ifns
                            || x.SonoType == org.SonoType.MILarge))
                    .Select(x => new InspectionLookupItem(x)).ToArray(),
                region.FederalDistrictId)
        {
        }

        /// <summary>
        /// Возвращает коллекцию инспекций региона
        /// </summary>
        public IEnumerable<InspectionLookupItem> Inspections
        {
            get;
            private set;
        }

        public int FederalDistrictId
        {
            get;
            private set;
        }
    }
}
