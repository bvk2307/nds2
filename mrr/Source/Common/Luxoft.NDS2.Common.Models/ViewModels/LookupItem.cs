﻿using System;

namespace Luxoft.NDS2.Common.Models.ViewModels
{
    public class LookupItem : ISelectableLookupItem
    {
        public LookupItem(string title, string description)
        {
            if (string.IsNullOrWhiteSpace(title))
            {
                throw new ArgumentNullException("title");
            }

            Title = title;
            Description = description;
        }

        public event EventHandler CheckedChanged;

        private bool isChecked;

        public bool IsChecked
        {
            get
            {
                return isChecked;
            }
            set
            {
                if (isChecked != value)
                {
                    isChecked = value;
                    RaiseCheckedChanged();
                }
            }
        }

        private void RaiseCheckedChanged()
        {
            if (CheckedChanged != null)
            {
                CheckedChanged(this, new EventArgs());
            }
        }

        public string Title
        {
            get;
            private set;
        }

        public string Description
        {
            get;
            private set;
        }        
    }
}
