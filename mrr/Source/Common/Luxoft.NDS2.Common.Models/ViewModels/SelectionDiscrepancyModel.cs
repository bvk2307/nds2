﻿using System;
using System.ComponentModel;
using System.Globalization;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace.Enums;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy;

namespace Luxoft.NDS2.Common.Models.ViewModels
{
    public class SelectionDiscrepancyModel
    {
        private readonly SelectionDiscrepancy _rawData;

        public SelectionDiscrepancyModel(SelectionDiscrepancy discrepancy)
        {
            _rawData = discrepancy;
        }

        # region Свойства

        [DisplayName("")]
        [Description("Флаг выбора записи")]
        public bool Included
        {
            get
            {
                return _rawData.Included == 1;
            }
            set
            {
                _rawData.Included = value ? 1 : 0;
            }
        }

        [DisplayName("Дата выявления")]
        [Description("Дата выявления расхождения")]
        public DateTime? FoundDate
        {
            get
            {
                return _rawData.FoundDate;
            }
        }

        [DisplayName("Номер СФ")]
        [Description("Номер СФ")]
        public string CreatedByInvoiceNumber
        {
            get
            {
                return _rawData.CreatedByInvoiceNumber;
            }
        }

        [DisplayName("Дата СФ")]
        [Description("Дата СФ")]
        public DateTime? CreatedByInvoiceDate
        {
            get
            {
                return _rawData.CreatedByInvoiceDate;
            }
        }

        [DisplayName("Вид расхождения")]
        [Description("Вид расхождения")]
        public DiscrepancyType DiscrepancyType
        {
            get
            {
                return (DiscrepancyType)_rawData.DiscrepancyType;
            }
        }

        [DisplayName("Подвид расхождения")]
        [Description("Подвид расхождения")]
        public DiscrepancySubtype Subtype
        {
            get 
            {
                return (DiscrepancySubtype)_rawData.Subtype;
            }
        }
        
        [DisplayName("Правило сопоставления")]
        [Description("Номер правила сопоставления")]
        public string CompareRule
        {
            get
            {
                return _rawData.CompareRule;
            }
        }

        [DisplayName("Сумма расхождения (руб.)")]
        [Description("Сумма расхождения в руб.")]
        public decimal? Amount
        {
            get
            {
                return _rawData.Amount;
            }
        }

        [DisplayName("Идентификатор")]
        [Description("Идентификатор расхождения")]
        public long DiscrepancyId
        {
            get
            {
                return _rawData.DiscrepancyId;
            }
        }

        [DisplayName("Сторона отработки")]
        [Description("Сторона первичной отработки")]
        public SidePrimaryProcessing SidePrimaryProcessing
        {
            get
            {
                return _rawData.SidePrimaryProcessing == 0 || _rawData.SidePrimaryProcessing == 1 ? SidePrimaryProcessing.Buyer : SidePrimaryProcessing.Seller;
            }
        }

        [DisplayName("Код")]
        [Description("Код региона покупателя")]
        public string BuyerRegionCode
        {
            get
            {
                return _rawData.BuyerRegionCode;
            }
        }

        [DisplayName("Наименование")]
        [Description("Наименование региона покупателя")]
        public string BuyerRegionName
        {
            get
            {
                return _rawData.BuyerRegionName;
            }
        }

        [DisplayName("Код")]
        [Description("Код инспекции покупателя")]
        public string BuyerSonoCode
        {
            get
            {
                return _rawData.BuyerSonoCode;
            }
        }

        [DisplayName("Наименование")]
        [Description("Наименование инспекции покупателя")]
        public string BuyerSonoName
        {
            get
            {
                return _rawData.BuyerSonoName;
            }
        }

        [DisplayName("ИНН")]
        [Description("ИНН покупателя")]
        public string BuyerInn
        {
            get
            {
                return _rawData.BuyerInn;
            }
        }

        [DisplayName("ИНН")]
        [Description("ИНН покупателя")]
        public string BuyerInnDeclarant
        {
            get
            {
                return _rawData.BuyerInnDeclarant;
            }
        }

        [DisplayName("р.8 = р.3")]
        [Description("Левая часть КС 1.28 должна равняться левой части КС 1.33 (или 1.32)")]
        public BuyerControlRatio BuyerControlRatio
        {
            get
            {
                return _rawData.BuyerControlRatio <= 1 ? (BuyerControlRatio)_rawData.BuyerControlRatio : BuyerControlRatio.NotCalculated;
            }
        }

        [DisplayName("Признак НД")]
        [Description("Признак декларации покупателя")]
        public Contracts.DTO.SelectionFilterNamespace.Enums.DeclarationType BuyerDeclarationSign
        {
            get
            {
                return (Contracts.DTO.SelectionFilterNamespace.Enums.DeclarationType)_rawData.BuyerDeclarationSign.GetValueOrDefault(0);
            }
        }

        [DisplayName("Дата представления")]
        [Description("Дата представления покупателем декларации/журнала в НО")]
        public DateTime? BuyerDeclarationSubmitDate
        {
            get
            {
                return _rawData.BuyerDeclarationSubmitDate;
            }
        }

        [DisplayName("Сумма НДС")]
        [Description("Сумма НДС по НД покупателя")]
        public decimal? BuyerDeclarationNdsAmount
        {
            get
            {
                return _rawData.BuyerDeclarationNdsAmount;
            }
        }

        [DisplayName("Отчетный период")]
        [Description("Период, за который подается декларация/журнал покупателя")]
        public string BuyerDeclarationPeriod
        {
            get
            {
                return _rawData.BuyerDeclarationPeriod;
            }
        }

        [DisplayName("Вид операции")]
        [Description("Код вида операции покупателя")]
        public string BuyerOperation
        {
            get
            {
                    return _rawData.OperationCodesBit.HasValue
                        ? _rawData.OperationCodesBit.Value.ToOperationCodes()
                        : string.Empty;
            }
        }

        [DisplayName("Источник")]
        [Description("Источник записи о СФ покупателя")]
        public BuyerChapter BuyerChapter
        {
            get
            {
                return (BuyerChapter)_rawData.BuyerChapter ;
            }
        }

        [DisplayName("СУР")]
        [Description("Показатель СУР покупателя")]
        public SurCode BuyerSur
        {
            get
            {
                return (SurCode)_rawData.BuyerSur;
            }
        }

        [DisplayName("Код")]
        [Description("Код региона продавца")]
        public string SellerRegionCode
        {
            get
            {
                return _rawData.SellerRegionCode;
            }
        }

        [DisplayName("Наименование")]
        [Description("Наименование региона продавца")]
        public string SellerRegionName
        {
            get
            {
                return _rawData.SellerRegionName;
            }
        }

        [DisplayName("Код")]
        [Description("Код инспекции продавца")]
        public string SellerSonoCode
        {
            get
            {
                return _rawData.SellerSonoCode;
            }
        }

        [DisplayName("Наименование")]
        [Description("Наименование инспекции продавца")]
        public string SellerSonoName
        {
            get
            {
                return _rawData.SellerSonoName;
            }
        }

        [DisplayName("ИНН")]
        [Description("ИНН продавца")]
        public string SellerInn
        {
            get
            {
                return _rawData.SellerInn;
            }
        }

        [DisplayName("ИНН")]
        [Description("ИНН продавца")]
        public string SellerInnDeclarant
        {
            get
            {
                return _rawData.SellerInnDeclarant;
            }
        }


        [DisplayName("Расхождение по КС 1.27")]
        [Description("Признак наличия расхождения по КС 1.27 на стороне продавца")]
        public SellerControlRatio SellerControlRatio
        {
            get
            {
                return _rawData.SellerControlRatio > 1 ? SellerControlRatio.NotCalculated : (SellerControlRatio)_rawData.SellerControlRatio;
            }
        }

        [DisplayName("Признак НД")]
        [Description("Признак декларации продавца")]
        public Contracts.DTO.SelectionFilterNamespace.Enums.DeclarationType SellerDeclarationSign
        {
            get
            {
                return (Contracts.DTO.SelectionFilterNamespace.Enums.DeclarationType)_rawData.SellerDeclarationSign.GetValueOrDefault(0);
            }
        }

        [DisplayName("Дата представления")]
        [Description("Дата представления продавцом декларации/журнала в НО")]
        public DateTime? SellerDeclarationSubmitDate
        {
            get
            {
                return _rawData.SellerDeclarationSubmitDate;
            }
        }

        [DisplayName("Сумма НДС")]
        [Description("Сумма НДС по НД продавца")]
        public decimal? SellerDeclarationNdsAmount
        {
            get
            {
                return _rawData.SellerDeclarationNdsAmount;
            }
        }

        [DisplayName("Отчетный период")]
        [Description("Период, за который подается декларация/журнал продавца")]
        public string SellerDeclarationPeriod
        {
            get
            {
                return _rawData.SellerDeclarationPeriod;
            }
        }

        [DisplayName("Источник")]
        [Description("Источник записи о СФ продавца")]
        public SellerChapter SellerChapter
        {
            get
            {
                return (SellerChapter)_rawData.SellerChapter;
            }
        }

        [DisplayName("СУР")]
        [Description("Показатель СУР продавца")]
        public SurCode SellerSur
        {
            get
            {
                return (SurCode)_rawData.SellerSur;
            }
        }

        [DisplayName("КПП")]
        [Description("КПП покупателя")]
        public string BuyerKpp { get { return _rawData.BuyerKpp; } }

        [DisplayName("КПП")]
        [Description("КПП покупателя")]
        public string BuyerKppDeclarant { get { return _rawData.BuyerKppDeclarant; } }


        [DisplayName("КПП")]
        [Description("КПП продавца")]
        public string SellerKpp { get { return _rawData.SellerKpp; } }

        [DisplayName("КПП")]
        [Description("КПП продавца")]
        public string SellerKppDeclarant { get { return _rawData.SellerKppDeclarant; } }


        /// <summary>
        /// Исключено из АТ
        /// </summary>
        public ExcludeFromClaimType ExcludeFromClaimType
        {
            get { return (ExcludeFromClaimType)_rawData.ExcludeFromClaimType; }
        }

        # endregion

        # region Методы

        public string GetBuyerDeclarationYear()
        {
            return _rawData.BuyerDeclarationPeriod.Substring(0,4);
        }

        public string GetBuyerPeriodCode()
        {
            return _rawData.BuyerDeclarationPeriod.Substring(4);
        }

        public string GetSellerDeclarationYear()
        {
            return _rawData.SellerDeclarationPeriod.Substring(0, 4);
        }

        public string GetSellerPeriodCode()
        {
            return _rawData.SellerDeclarationPeriod.Substring(4);
        }

        # endregion

        # region Идентификация

        public override int GetHashCode()
        {
            return DiscrepancyId == 0 ? base.GetHashCode() : DiscrepancyId.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var anotherDiscrepancy = obj as SelectionDiscrepancyModel;

            if (anotherDiscrepancy == null)
            {
                return false;
            }

            if (anotherDiscrepancy.DiscrepancyId == 0 && DiscrepancyId == 0)
            {
                return base.Equals(anotherDiscrepancy);
            }

            return anotherDiscrepancy.DiscrepancyId == DiscrepancyId;
        }

        # endregion

        # region Вспомогательные методы визуализации данных

        private string ParseOperationCodes(string value)
        {
            ulong mask;

            if (ulong.TryParse(value, out mask))
                return mask.ToOperationCodes();

            return string.Empty;
        }

        # endregion
    }
}
