﻿using System;

namespace Luxoft.NDS2.Common.Models.ViewModels
{
    /// <summary>
    /// Этот интерфейс описывает элемент справочника
    /// </summary>
    public interface ISelectableLookupItem
    {
        /// <summary>
        /// Возвращает ключ элемента справочника
        /// </summary>
        string Title { get; }

        /// <summary>
        /// Возвращает текстовое представление элемента справочника
        /// </summary>
        string Description { get; }

        /// <summary>
        /// Возвращает или задает признак того, что элемент справочника выбран
        /// </summary>
        bool IsChecked { get; set; }

        /// <summary>
        /// Сигнализирует об изменении значения свойства IsChecked
        /// </summary>
        event EventHandler CheckedChanged;
    }
}
