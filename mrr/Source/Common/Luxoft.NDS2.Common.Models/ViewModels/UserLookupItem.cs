﻿
namespace Luxoft.NDS2.Common.Models.ViewModels
{
    public class UserLookupItem : LookupItem
    {
        public UserLookupItem(string title, string name)
            : base(title, name)
        {
        }
    }
}
