﻿using Luxoft.NDS2.Common.Contracts.CustomAttributes;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using System;
using System.ComponentModel;

namespace Luxoft.NDS2.Common.Models.ViewModels
{
    public class SelectionDiscrepancy
    {
        private readonly Discrepancy _rawData;

        public SelectionDiscrepancy(Discrepancy discrepancy)
        {
            _rawData = discrepancy;
        }

        #region Поля
        // ReSharper disable InconsistentNaming

        public bool IsChecked
        {
            get
            {
                return _rawData.IsChecked;
            }
            set
            {
                _rawData.IsChecked = value;
            }
        }

        [DisplayName("Номер")]
        [Description("Номер расхождения")]
        public long DiscrepancyId
        {
            get
            {
                return _rawData.DiscrepancyId;
            }
            set
            {
                _rawData.DiscrepancyId = value;
            }
        }

        [DisplayName("Дата выявления")]
        [Description("Дата выявления расхождения")]
        public DateTime? FoundDate
        {
            get
            {
                return _rawData.FoundDate;
            }
            set
            {
                _rawData.FoundDate = value;
            }
        }

        #region лицо проверяемой декларации

        [DisplayName("ИНН")]
        [Description("ИНН НП первичной отработки расхождения")]
        public string TaxPayerInn
        {
            get
            {
                return _rawData.TaxPayerInn;
            }
            set
            {
                _rawData.TaxPayerInn = value;
            }
        }

        [DisplayName("КПП")]
        [Description("КПП НП первичной отработки расхождения")]
        public string TaxPayerKpp
        {
            get
            {
                return _rawData.TaxPayerKpp;
            }
            set
            {
                _rawData.TaxPayerKpp = value;
            }
        }

        [DisplayName("Наименование")]
        [Description("Наименование НП первичной отработки расхождения")]
        public string TaxPayerName
        {
            get
            {
                return _rawData.TaxPayerName;
            }
            set
            {
                _rawData.TaxPayerName = value;
            }
        }

        [DisplayName("Инспекция")]
        [Description("Код и наименование инспекции")]
        public string TaxPayerSoun
        {
            get
            {
                return _rawData.TaxPayerSoun;
            }
            set
            {
                _rawData.TaxPayerSoun = value;
            }
        }

        [DisplayName("Регион")]
        [Description("Код и наименование региона")]
        public string TaxPayerRegion
        {
            get
            {
                return _rawData.TaxPayerRegion;
            }
            set
            {
                _rawData.TaxPayerRegion = value;
            }
        }

        [DisplayName("Отчетный период")]
        [Description("Период, за который подается  декларация")]
        public string TaxPayerPeriod
        {
            get
            {
                return _rawData.TaxPayerPeriod;
            }
            set
            {
                _rawData.TaxPayerPeriod = value;
            }
        }

        [DisplayName("Номер корректировки")]
        [Description("Номер корректировки декларации")]
        public string CorrectionNumber
        {
            get
            {
                return _rawData.CorrectionNumber;
            }
            set
            {
                _rawData.CorrectionNumber = value;
            }
        }

        [DisplayName("Источник")]
        [Description("Источник записи о СФ")]
        public string TaxPayerSource
        {
            get
            {
                return _rawData.TaxPayerSource;
            }
            set
            {
                _rawData.TaxPayerSource = value;
            }
        }

        [DisplayName("Признак НД")]
        [Description("Признак декларации")]
        public string TaxPayerDeclSign
        {
            get
            {
                return _rawData.TaxPayerDeclSign;
            }
            set
            {
                _rawData.TaxPayerDeclSign = value;
            }
        }

        [DisplayName("Крупнейший")]
        [Description("НП является крупнейшим")]
        public bool TaxPayerCategory
        {
            get
            {
                return _rawData.TaxPayerCategory;
            }
            set
            {
                _rawData.TaxPayerCategory = value;
            }
        }

        #endregion лицо проверяемой декларации

        [DisplayName("Вид расхождения")]
        [Description("Вид расхождения")]
        public string Type
        {
            get
            {
                return _rawData.Type;
            }
            set
            {
                _rawData.Type = value;
            }
        }

        [DisplayName("Сумма расхождения (руб.)")]
        [Description("Сумма расхождения в руб.")]
        [CurrencyFormat]
        public decimal Amount
        {
            get
            {
                return _rawData.Amount;
            }
            set
            {
                _rawData.Amount = value;
            }
        }

        [DisplayName("ПВП расхождения (руб.)")]
        [Description("ПВП расхождения в руб.")]
        [CurrencyFormat]
        public decimal AmountPVP
        {
            get
            {
                return _rawData.AmountPVP;
            }
            set
            {
                _rawData.AmountPVP = value;
            }
        }

        [DisplayName("Статус")]
        [Description("Статус расхождения")]
        public string Status
        {
            get
            {
                return _rawData.Status;
            }
            set
            {
                _rawData.Status = value;
            }
        }

        [DisplayName("Правило сопоставления")]
        [Description("Правило сопоставления")]
        public string COMPARE_RULE
        {
            get
            {
                return _rawData.COMPARE_RULE;
            }
            set
            {
                _rawData.COMPARE_RULE = value;
            }
        }

        public bool CanBeSelected
        {
            get;
            set;
        }

        // ReSharper restore InconsistentNaming
        #endregion

        public Discrepancy ToDTO()
        {
            return _rawData;
        }

        public override int GetHashCode()
        {
            return DiscrepancyId == 0 ? base.GetHashCode() : DiscrepancyId.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var anotherDiscrepancy = obj as SelectionDiscrepancy;

            if (anotherDiscrepancy == null)
            {
                return false;
            }

            if (anotherDiscrepancy.DiscrepancyId == 0 && DiscrepancyId == 0)
            {
                return base.Equals(anotherDiscrepancy);
            }

            return anotherDiscrepancy.DiscrepancyId == DiscrepancyId;
        }

    }
}
