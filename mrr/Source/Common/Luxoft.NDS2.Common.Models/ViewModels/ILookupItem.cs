﻿namespace Luxoft.NDS2.Common.Models.ViewModels
{
    public interface ILookupItem
    {
        /// <summary>
        /// Возвращает ключ элемента справочника
        /// </summary>
        string Title { get; }

        /// <summary>
        /// Возвращает текстовое представление элемента справочника
        /// </summary>
        string Description { get; }
    }
}
