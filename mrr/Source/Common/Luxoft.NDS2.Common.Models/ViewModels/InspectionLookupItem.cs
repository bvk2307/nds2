﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;
using Luxoft.NDS2.Common.Contracts.DTO.OrganizationStructure;
using System;

namespace Luxoft.NDS2.Common.Models.ViewModels
{
    /// <summary>
    /// Этот класс реализует элемент справочника инспекций (налоговых органов)
    /// </summary>
    public class InspectionLookupItem : LookupItem
    {
        /// <summary>
        /// Создает новый экземпляр класса InspectionLookupItem
        /// </summary>
        /// <param name="inspectionData">Сведения об инспекции</param>
        public InspectionLookupItem(Inspection inspectionData)
            : base(inspectionData.Code, inspectionData.Name)
        {
        }

        public InspectionLookupItem(Sono sonoData)
            : base(sonoData.Code, sonoData.Name)
        {
        }
    }
}
