﻿using Luxoft.NDS2.Common.Contracts.DTO.TaxPeriod;
using System;

namespace Luxoft.NDS2.Common.Models
{
    public class EffectiveTaxPeriodModel
    {
        private const string Pattern = "{0} кв. {1} г.";

        private readonly EffectiveTaxPeriod _data;

        public EffectiveTaxPeriodModel(EffectiveTaxPeriod data)
        {
            if (data == null)
            {
                throw new ArgumentNullException("data");
            }

            _data = data;
        }

        public Quarter Quarter
        {
            get
            {
                return _data.Quarter;
            }
        }

        public int Year
        {
            get
            {
                return _data.Year;
            }
        }

        public override string ToString()
        {
            return string.Format(Pattern, (int)Quarter, Year);
        }
    }
}
