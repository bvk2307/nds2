﻿using System.ComponentModel;

namespace Luxoft.NDS2.Common.Models
{
    public interface IGridViewDataSource<T>
    {
        BindingList<T> ListItems
        {
            get;
        }
    }
}
