﻿using dto = Luxoft.NDS2.Common.Contracts.DTO.AccessRestriction;
using System.Linq;

namespace Luxoft.NDS2.Common.Models.AccessContext
{
    public static class OperationModelCreator 
    {
        public static OperationModel[] ToModel(this dto.AccessContext accessData)
        {
            return accessData.Operations.Select(ToModel).ToArray();
        }

        public static OperationModel ToModel(this dto.OperationAccessContext accessData)
        {
            if (accessData.IsRestricted)
            {
                return (accessData.AvailableRegions != null && accessData.AvailableRegions.Any())
                    ? (OperationModel)new RegionRestrictedOperationModel(
                        accessData.Name, 
                        accessData.Description, 
                        accessData.AvailableRegions)
                    : new SonoRestrictedOperationModel(
                        accessData.Name, 
                        accessData.Description, 
                        accessData.AvailableInspections);
            }

            return new FullAccessOperationModel(accessData.Name, accessData.Description);
        }
    }
}
