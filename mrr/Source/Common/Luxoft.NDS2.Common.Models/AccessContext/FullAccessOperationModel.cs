﻿using Luxoft.NDS2.Common.Contracts.DTO.OrganizationStructure;
using System.Collections.Generic;
namespace Luxoft.NDS2.Common.Models.AccessContext
{
    public class FullAccessOperationModel : OperationModel
    {
        public FullAccessOperationModel(string name, string description)
            : base(name, description)
        {
        }

        public override string[] GetRegions()
        {
            return new string[0];
        }

        public override IEnumerable<Region> Intersect(IEnumerable<Region> availableRegions)
        {
            return availableRegions;
        }

        public override string[] GetSonoCodes()
        {
            return new string[0];
        }

        public override bool Allow(string sonoCode)
        {
            return true;
        }
    }
}
