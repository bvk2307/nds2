﻿using Luxoft.NDS2.Common.Contracts.DTO.OrganizationStructure;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Common.Models.AccessContext
{
    public class RegionRestrictedOperationModel : OperationModel
    {
        private readonly string[] _regionCodes;

        public RegionRestrictedOperationModel(
            string name,
            string description,
            string[] regionCodes)
            : base(name, description)
        {
            _regionCodes = regionCodes;
        }
    
        public override string[] GetRegions()
        {
 	        return _regionCodes;
        }

        public override IEnumerable<Region> Intersect(IEnumerable<Region> availableRegions)
        {
            return availableRegions.Where(region => _regionCodes.Contains(region.Code));
        }

        public override string[] GetSonoCodes()
        {
 	        return new string[0];
        }

        public override bool Allow(string sonoCode)
        {
            throw new System.NotSupportedException();
        }
    }
}
