﻿using Luxoft.NDS2.Common.Contracts.DTO.OrganizationStructure;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Common.Models.AccessContext
{
    public class SonoRestrictedOperationModel : OperationModel
    {
        private readonly string[] _sonoCodes;

        public SonoRestrictedOperationModel(
            string name,
            string description,
            string[] sonoCodes)
            : base(name, description)
        {
            _sonoCodes = sonoCodes;
        }

        private IEnumerable<string> CalculateRegions()
        {
            return _sonoCodes.Select(x => x.Substring(0, 2));
        }

        public override string[] GetRegions()
        {
            return CalculateRegions().Distinct().ToArray();
        }

        public override IEnumerable<Region> Intersect(IEnumerable<Region> availableRegions)
        {
            var allowedRegions = CalculateRegions().ToArray();

            return availableRegions.Where(region => allowedRegions.Contains(region.Code));
        }

        public override string[] GetSonoCodes()
        {
            return _sonoCodes;
        }

        public override bool Allow(string sonoCode)
        {
            return _sonoCodes.Contains(sonoCode);
        }
    }
}
