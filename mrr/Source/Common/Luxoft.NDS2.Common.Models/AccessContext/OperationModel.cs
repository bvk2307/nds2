﻿using Luxoft.NDS2.Common.Contracts.DTO.OrganizationStructure;
using System.Collections.Generic;
namespace Luxoft.NDS2.Common.Models.AccessContext
{
    public abstract class OperationModel
    {
        protected OperationModel(string name, string description)
        {
            Name = name;
            Description = description;
        }

        public string Name
        {
            get;
            private set;
        }

        public string Description
        {
            get;
            private set;
        }

        public abstract bool Allow(string sonoCode);

        public abstract string[] GetRegions();

        public abstract IEnumerable<Region> Intersect(IEnumerable<Region> availableRegions);

        public abstract string[] GetSonoCodes();

        public override string ToString()
        {
            return Description;
        }

        public override bool Equals(object obj)
        {
            var anotherModel = obj as OperationModel;

            return anotherModel != null && anotherModel.Name == Name;
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }
    }
}
