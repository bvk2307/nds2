﻿using System.Collections.Generic;

namespace Luxoft.NDS2.Common.Models
{
    public interface IListViewModel<TModel, TDto> : IGridViewDataSource<TModel>
    {
        void UpdateList(IEnumerable<TDto> dataItems);
    }
}
