﻿using System.Globalization;

namespace Luxoft.NDS2.Common.Models
{
    public static class OperationCodesParser
    {
        private static  int OperationCodeMaxBitsDimension = 32;

        private static string CodeSeparator = "; ";

        public static string ToOperationCodes(this ulong bitValue)
        {
            var result = string.Empty;
            for (var c = 0; c < OperationCodeMaxBitsDimension; ++c)
            {
                if (((bitValue >> c) & 1) == 0)
                    continue;

                if (result.Length > 0)
                    result += CodeSeparator;
                result += (c + 1).ToString().PadLeft(2, '0');
            }

            return result;
        }
    }
}
