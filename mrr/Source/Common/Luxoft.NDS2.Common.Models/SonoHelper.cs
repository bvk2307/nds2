﻿using Luxoft.NDS2.Common.Contracts.DTO.OrganizationStructure;

namespace Luxoft.NDS2.Common.Models
{
    public static class SonoHelper
    {
        public static string GetTitle(this Sono sono)
        {
            return string.Format("{0} - {1}", sono.Code, sono.Name);
        }
    }
}
