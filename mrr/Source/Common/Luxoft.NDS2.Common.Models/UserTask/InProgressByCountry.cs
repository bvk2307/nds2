﻿
namespace Luxoft.NDS2.Common.Models.UserTask
{
    public sealed class InProgressByCountry : InProgressSummaryModel
    {
        public InProgressByCountry(string taskTypeName)
            : base(taskTypeName)
        {
        }
    }
}
