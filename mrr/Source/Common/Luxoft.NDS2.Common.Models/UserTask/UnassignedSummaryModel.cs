﻿namespace Luxoft.NDS2.Common.Models.UserTask
{
    public abstract class UnassignedSummaryModel : TaskSummaryModel
    {
        protected UnassignedSummaryModel(string taskTypeName)
            : base(taskTypeName)
        {
        }

        /// <summary>
        /// Заданий не назначено к началу периода
        /// </summary>
        public int UnassignedStart 
        { 
            get; 
            private set; 
        }

        /// <summary>
        /// Заданий не назначено к концу периода
        /// </summary>
        public int UnassignedEnd 
        { 
            get; 
            private set; 
        }

        public override void ApplyTo(int onSchedule, int overdue)
        {
            UnassignedEnd += onSchedule + overdue;
        }

        public override void ApplyFrom(int onSchedule, int overdue)
        {
            UnassignedStart += onSchedule + overdue;
        }
    }
}
