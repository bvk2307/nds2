﻿namespace Luxoft.NDS2.Common.Models.UserTask
{
    public abstract class InProgressSummaryModel : TaskSummaryModel
    {
        protected InProgressSummaryModel(string taskTypeName)
            : base(taskTypeName)
        {
        }

        /// <summary>
        /// Заданий выполнено в срок на начало периода
        /// </summary>
        public int OnSchedulePeriodStart { get; private set; }

        /// <summary>
        /// Заданий выполнено в срок на конец периода
        /// </summary>
        public int OnSchedulePeriodEnd { get; private set; }

        /// <summary>
        /// Заданий выполнено и просрочено на начало периода
        /// </summary>
        public int OverdueStart { get; private set; }

        /// <summary>
        /// Заданий выполнено и просрочено на конец периода
        /// </summary>
        public int OverdueEnd { get; private set; }

        /// <summary>
        /// Всего выполнено заданий на начало периода
        /// </summary>
        public int TotalStart { get { return OnSchedulePeriodStart + OverdueStart; } }

        /// <summary>
        /// Всего выполнено заданий на конец периода
        /// </summary>
        public int TotalEnd { get { return OnSchedulePeriodEnd + OverdueEnd; } }

        public override void ApplyFrom(int onSchedule, int overdue)
        {
            OnSchedulePeriodStart += onSchedule;
            OverdueStart += overdue;
        }

        public override void ApplyTo(int onSchedule, int overdue)
        {
            OnSchedulePeriodEnd += onSchedule;
            OverdueEnd += overdue;
        }
    }
}
