﻿using Luxoft.NDS2.Common.Contracts.DTO;

namespace Luxoft.NDS2.Common.Models.UserTask
{
    public sealed class CompletedByAssignee : CompletedSummaryModel
    {
        public CompletedByAssignee(string taskType, string assigneeName)
            : base(taskType)
        {
            InspectorName = assigneeName;
        }

        /// <summary>
        /// Ф.И.О. инспектора
        /// </summary>
        public string InspectorName 
        { 
            get; 
            private set; 
        }
    }
}
