﻿namespace Luxoft.NDS2.Common.Models.UserTask
{
    public abstract class CompletedSummaryModel : TaskSummaryModel
    {
        protected CompletedSummaryModel(string taskTypeName)
            : base(taskTypeName)
        {
        }

        /// <summary>
        /// Заданий выполнено в срок
        /// </summary>
        public int OnSchedule 
        { 
            get; 
            private set; 
        }

        /// <summary>
        /// Заданий выполнено и просрочено
        /// </summary>
        public int Overdue 
        { 
            get; 
            private set; 
        }

        /// <summary>
        /// Всего выполнено заданий
        /// </summary>
        public int Total
        {
            get
            {
                return OnSchedule + Overdue;
            }
        }

        public override void ApplyFrom(int onSchedule, int overdue)
        {
            OnSchedule -= onSchedule;
            Overdue -= overdue;
        }

        public override void ApplyTo(int onSchedule, int overdue)
        {
            OnSchedule += onSchedule;
            Overdue += overdue;
        }
    }
}
