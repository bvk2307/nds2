﻿using Luxoft.NDS2.Common.Contracts.DTO.OrganizationStructure;

namespace Luxoft.NDS2.Common.Models.UserTask
{
    public sealed class CompletedBySono : CompletedSummaryModel
    {
        public CompletedBySono(string taskType, Region region, Sono sono)
            : base(taskType)
        {
            RegionName = region.ToString();
            SonoName = sono.ToString();
        }

        /// <summary>
        /// Регион
        /// </summary>
        public string RegionName { get; private set; }

        /// <summary>
        /// Инспекция
        /// </summary>
        public string SonoName { get; private set; }

    }
}
