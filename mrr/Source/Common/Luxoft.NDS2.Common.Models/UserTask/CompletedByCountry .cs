﻿
namespace Luxoft.NDS2.Common.Models.UserTask
{
    public sealed class CompletedByCountry : CompletedSummaryModel
    {
        public CompletedByCountry(string taskTypeName)
            : base(taskTypeName)
        {
        }
    }
}
