﻿
using Luxoft.NDS2.Common.Contracts.DTO.OrganizationStructure;
namespace Luxoft.NDS2.Common.Models.UserTask
{
    public sealed class InProgressByRegion : InProgressSummaryModel
    {
        public InProgressByRegion(string taskType, Region region)
            : base(taskType)
        {
            RegionName = region.ToString();
        }

        /// <summary>
        /// Регион
        /// </summary>
        public string RegionName { get; private set; }
    }
}
