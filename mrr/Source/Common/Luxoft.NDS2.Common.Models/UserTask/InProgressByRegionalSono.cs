﻿using Luxoft.NDS2.Common.Contracts.DTO.OrganizationStructure;

namespace Luxoft.NDS2.Common.Models.UserTask
{
    public sealed class InProgressByRegionalSono : InProgressSummaryModel
    {
        public InProgressByRegionalSono(string taskType, Sono sono)
            : base(taskType)
        {
            SonoName = sono.ToString();
        }

        /// <summary>
        /// Инспекция
        /// </summary>
        public string SonoName 
        { 
            get; 
            private set; 
        }

    }
}
