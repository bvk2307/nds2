﻿using Luxoft.NDS2.Common.Contracts.DTO.OrganizationStructure;

namespace Luxoft.NDS2.Common.Models.UserTask
{
    public sealed class UnassignedByRegion : UnassignedSummaryModel
    {
        public UnassignedByRegion(string taskTypeName, Region region)
            : base(taskTypeName)
        {
            RegionName = region.ToString();
        }

        /// <summary>
        /// Регион
        /// </summary>
        public string RegionName { get; private set; }

    }
}
