﻿namespace Luxoft.NDS2.Common.Models.UserTask
{
    public abstract class TaskSummaryModel
    {
        protected TaskSummaryModel(string taskTypeName)
        {
            TaskTypeName = taskTypeName;
        }

        public string TaskTypeName
        {
            get;
            private set;
        }

        public abstract void ApplyTo(int onSchedule, int overdue);

        public abstract void ApplyFrom(int onSchedule, int overdue);
    }
}
