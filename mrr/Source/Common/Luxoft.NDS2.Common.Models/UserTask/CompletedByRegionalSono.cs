﻿using Luxoft.NDS2.Common.Contracts.DTO.OrganizationStructure;

namespace Luxoft.NDS2.Common.Models.UserTask
{
    /// <summary>
    /// Этот класс описывает модель строки данных отчета о выполненных заданий в разрезе инспекций определенного региона
    /// </summary>
    public sealed class CompletedByRegionalSono : CompletedSummaryModel
    {
        public CompletedByRegionalSono(string taskType, Sono sono)
            : base(taskType)
        {
            SonoName = sono.ToString();
        }

        /// <summary>
        /// Инспекция
        /// </summary>
        public string SonoName 
        { 
            get; 
            private set; 
        }
    }
}
