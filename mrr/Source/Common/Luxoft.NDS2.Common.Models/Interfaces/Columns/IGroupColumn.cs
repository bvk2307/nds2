﻿using System.Collections.Generic;

namespace Luxoft.NDS2.Common.Models.Interfaces.Columns
{
    public interface IGroupColumn : ITableColumn
    {
        List<ITableColumn> Columns { get; set; }
    }
}
