﻿namespace Luxoft.NDS2.Common.Models.Interfaces.Columns
{
    public enum HorizontalTextAlignment
    {
        Left = 0,
        Center,
        Right
    }
    
    public interface ITableColumn
    {
        string Key { get; set; }

        string Title { get; set; }

        string Description { get; set; }

        int Width { get; set; }

        int RowSpan { get; set; }

        bool IsVisible { get; set; }

        HorizontalTextAlignment Align { get; set; }
    }
}
