﻿using System;
using System.Linq.Expressions;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;

namespace Luxoft.NDS2.Common.Models.Interfaces.Columns
{
    public interface IColumnFactory<TModel>
    {
        ITableColumn CreateCheckColumn<TField>(Expression<Func<TModel, TField>> key, int rowSpan = 1);

        ITableColumn CreateBoolColumn<TField>(Expression<Func<TModel, TField>> key, int rowSpan = 1);

        ITableColumn CreateTextColumn<TField>(Expression<Func<TModel, TField>> key, int rowSpan = 1);

        ITableColumn CreateNumberColumn<TField>(Expression<Func<TModel, TField>> key, int rowSpan = 1);

        ITableColumn CreateMoneyColumn<TField>(Expression<Func<TModel, TField>> key, int rowSpan = 1);

        ITableColumn CreateDateColumn<TField>(Expression<Func<TModel, TField>> key, int rowSpan = 1);

        ITableColumn CreateIntColumn<TField>(Expression<Func<TModel, TField>> key, int rowSpan = 1);

        ITableColumn CreateSurColumn<TField>(Expression<Func<TModel, TField>> key, SurCode[] surCodes, int rowSpan = 1);

        IGroupColumn CreateGroup(string key, string title, string description, int rowSpan = 1);
    }
}
