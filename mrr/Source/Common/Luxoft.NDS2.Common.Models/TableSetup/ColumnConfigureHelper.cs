﻿using System;
using Luxoft.NDS2.Common.Models.Interfaces.Columns;

namespace Luxoft.NDS2.Common.Models.TableSetup
{

    public static class ColumnConfigureHelper
    {
        public static ITableColumn Configure(this ITableColumn column, Action<ITableColumn> configure)
        {
            configure(column);
            return column;
        }

        public static ITableColumn MakeHidden(this ITableColumn column)
        {
            column.IsVisible = false;
            return column;
        }

        public static IGroupColumn WithColumn(this IGroupColumn group, ITableColumn column)
        {
            group.Columns.Add(column);
            return group;
        }
    }
}
