﻿using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Models.Interfaces.Columns;
using Luxoft.NDS2.Common.Models.ViewModels;
using System.Collections.Generic;

namespace Luxoft.NDS2.Common.Models.TableSetup
{
    public class SelectionDiscrepancyTableConfigurator
    {
        private readonly IColumnFactory<SelectionDiscrepancyModel> _factory;

        public SelectionDiscrepancyTableConfigurator(IColumnFactory<SelectionDiscrepancyModel> factory)
        {
            _factory = factory;
        }

        public List<ITableColumn> GetColumns(SurCode[] surCodes)
        {
               var buyerGroup = _factory.CreateGroup("BuyerGroup", "Покупатель", "Покупатель")
                            .WithColumn(_factory.CreateTextColumn(m => m.BuyerRegionCode))
                            .WithColumn(_factory.CreateTextColumn(m => m.BuyerRegionName))
                            .WithColumn(_factory.CreateTextColumn(m => m.BuyerSonoCode))
                            .WithColumn(_factory.CreateTextColumn(m => m.BuyerSonoName))
                            .WithColumn(_factory.CreateTextColumn(m => m.BuyerInn))
                            .WithColumn(_factory.CreateTextColumn(m => m.BuyerKpp))
                            .WithColumn(_factory.CreateTextColumn(m => m.BuyerControlRatio))
                            .WithColumn(_factory.CreateTextColumn(m => m.BuyerDeclarationSign))
                            .WithColumn(_factory.CreateDateColumn(m => m.BuyerDeclarationSubmitDate))
                            .WithColumn(_factory.CreateMoneyColumn(m => m.BuyerDeclarationNdsAmount))
                            .WithColumn(_factory.CreateTextColumn(m => m.BuyerDeclarationPeriod))
                            .WithColumn(_factory.CreateTextColumn(m => m.BuyerOperation))
                            .WithColumn(_factory.CreateTextColumn(m => m.BuyerChapter))
                            .WithColumn(_factory.CreateSurColumn(m => m.BuyerSur, surCodes));
               var sellerGroup = _factory.CreateGroup("SellerGroup", "Продавец", "Продавец")
                            .WithColumn(_factory.CreateTextColumn(m => m.SellerInn))
                            .WithColumn(_factory.CreateTextColumn(m => m.SellerKpp))
                            .WithColumn(_factory.CreateTextColumn(m => m.SellerControlRatio))
                            .WithColumn(_factory.CreateTextColumn(m => m.SellerDeclarationSign))
                            .WithColumn(_factory.CreateDateColumn(m => m.SellerDeclarationSubmitDate))
                            .WithColumn(_factory.CreateMoneyColumn(m => m.SellerDeclarationNdsAmount))
                            .WithColumn(_factory.CreateTextColumn(m => m.SellerDeclarationPeriod))
                            .WithColumn(_factory.CreateTextColumn(m => m.SellerChapter))
                            .WithColumn(_factory.CreateSurColumn(m => m.SellerSur, surCodes))
                            .WithColumn(_factory.CreateTextColumn(m => m.SellerRegionCode))
                            .WithColumn(_factory.CreateTextColumn(m => m.SellerRegionName))
                            .WithColumn(_factory.CreateTextColumn(m => m.SellerSonoCode))
                            .WithColumn(_factory.CreateTextColumn(m => m.SellerSonoName));

            return new List<ITableColumn>
            {
                _factory.CreateCheckColumn(m => m.Included, 3),
                _factory.CreateGroup("DiscrepancyGroup", "Расхождение", "Расхождение")
                    .WithColumn(_factory.CreateDateColumn(m => m.FoundDate, 2))
                    .WithColumn(_factory.CreateTextColumn(m => m.DiscrepancyType, 2))
                    .WithColumn(_factory.CreateTextColumn(m => m.Subtype, 2))
                    .WithColumn(_factory.CreateTextColumn(m => m.CompareRule, 2))
                    .WithColumn(_factory.CreateMoneyColumn(m => m.Amount, 2))
                    .WithColumn(_factory.CreateIntColumn(m => m.DiscrepancyId, 2))
            };
        }
    }

}
