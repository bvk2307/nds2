﻿namespace FLS.Common.Lib.Data
{
    public enum SortStyle
    {
        AlphabeticOrder,
        ElementsInGroupOrder,
        AlphabeticGroupsOrder
    }
}