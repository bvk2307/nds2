﻿using System;

namespace FLS.Common.Lib.Data
{
    /// <summary>
    /// Этот класс описывает набор параметров для выборки данных на клиент, связанных с постраничным выводом
    /// </summary>
    [Serializable]
    public class QueryDataPage : IEquatable<QueryDataPage>
    {
        /// <summary>
        /// Возвращает или задает максимальное кол-во строк данных, которые необходимо вернуть по запросу.
        /// null - означает отсутствие ограничений
        /// </summary>
        public uint? RowsToTake { get; set; }

        /// <summary>
        /// Возвращает или задает кол-во строк данных, которые необходимо пропустить при выборке
        /// </summary>
        public uint RowsToSkip { get; set; }

        public QueryDataPage Clone()
        {
            return new QueryDataPage { RowsToTake = RowsToTake, RowsToSkip = RowsToSkip };
        }

        /// <summary>Indicates whether the current object is equal to another object of the same type.</summary>
        /// <returns>true if the current object is equal to the <paramref name="other" /> parameter; otherwise, false.</returns>
        /// <param name="other">An object to compare with this object.</param>
        public bool Equals( QueryDataPage other )
        {
            if ( ReferenceEquals( null, other ) ) return false;
            if ( ReferenceEquals( this, other ) ) return true;
            return RowsToTake == other.RowsToTake && RowsToSkip == other.RowsToSkip;
        }

        /// <summary>Determines whether the specified <see cref="T:System.Object" /> is equal to the current <see cref="T:System.Object" />.</summary>
        /// <returns>true if the specified <see cref="T:System.Object" /> is equal to the current <see cref="T:System.Object" />; otherwise, false.</returns>
        /// <param name="obj">The object to compare with the current object. </param>
        public override bool Equals( object obj )
        {
            if ( ReferenceEquals( null, obj ) ) return false;
            if ( ReferenceEquals( this, obj ) ) return true;
            if ( obj.GetType() != this.GetType() ) return false;
            return Equals( (QueryDataPage)obj );
        }

        /// <summary>Serves as a hash function for a particular type. </summary>
        /// <returns>A hash code for the current <see cref="T:System.Object" />.</returns>
        public override int GetHashCode()
        {
            unchecked
            {
                return ( RowsToTake.GetHashCode() * 397 ) ^ (int)RowsToSkip;
            }
        }

        public static bool operator ==( QueryDataPage left, QueryDataPage right )
        {
            return Equals( left, right );
        }

        public static bool operator !=( QueryDataPage left, QueryDataPage right )
        {
            return !Equals( left, right );
        }
    }
}
