﻿namespace FLS.Common.Lib.Data
{
    public enum FilterComparisionOperator
    {
        NotDefinedOperator,
        Equals,
        NotEquals,
        LessThan,
        LessThanOrEqualTo,
        GreaterThan,
        GreaterThanOrEqualTo,
        Like,
        Match,
        NotLike,
        DoesNotMatch,
        StartsWith,
        DoesNotStartWith,
        EndsWith,
        DoesNotEndWith,
        Contains,
        DoesNotContain,
        OneOf
    }
}