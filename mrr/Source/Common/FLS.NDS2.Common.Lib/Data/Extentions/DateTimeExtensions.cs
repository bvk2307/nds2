﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FLS.Common.Lib.Data.Extentions
{
    public static class DateTimeExtensions
    {
        public static double? ToUnixFormat(this DateTime? data)
        {
            if (default(DateTime?) == data)
            {
                return default(double?);
            }

            return data.Value.ToUnixFormat();
        }

        public static double ToUnixFormat(this DateTime data)
        {
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            return data.Subtract(dtDateTime).TotalSeconds;
        }

        public static int? ToShortIntFormat(this DateTime? data)
        {
            if (default(DateTime?) == data)
            {
                return default(int?);
            }

            return data.Value.ToShortIntFormat();
        }

        public static int ToShortIntFormat(this DateTime data)
        {
            return 10000 * data.Year + 100 * data.Month + data.Day;
        }

        public static DateTime? FromShortIntFormat(this int? data)
        {
            if (default(int?) == data)
            {
                return default(DateTime?);
            }

            return data.Value.FromShortIntFormat();
        }

        public static DateTime FromShortIntFormat(this int data)
        {
            if (default(int) == data)
            {
                return default(DateTime);
            }

            int year = data / 10000;
            int month = data % 10000 / 100;
            int day = data % 10000 % 100;

            return new DateTime(year, month, day);
        }

        public static DateTime? FromUnixFormat(this double? data)
        {
            if (default(double?) == data)
            {
                return default(DateTime?);
            }

            return data.Value.FromUnixFormat();
        }

        public static DateTime FromUnixFormat(this double data)
        {
            if (default(double) == data)
            {
                return default(DateTime);
            }

            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(data).ToLocalTime();
            return dtDateTime;
        }

        /// <summary>
        /// Convert DateTime to timestamp
        /// </summary>
        /// <param name="date">Converted date</param>
        /// <returns></returns>
        public static int DateToTimestamp(this DateTime date)
        {
            return Convert.ToInt32(date.Subtract(new DateTime(1970, 1, 1)).TotalSeconds);
        }
    }
}
