﻿using System;
using FLS.CommonComponents.Lib.Serialization;

namespace FLS.Common.Lib.Data
{
    [Serializable]
    public class QueryParameters : ICloneable
    {
        /// <summary> Returnes a deep clone created. </summary>
        /// <returns></returns>
        QueryParameters Clone()
        {
            return SerializationDeepCloner.Clone( this );
        }

        #region Implementation of ICloneable

        object ICloneable.Clone()
        {
            return Clone();
        }

        #endregion
    }
}