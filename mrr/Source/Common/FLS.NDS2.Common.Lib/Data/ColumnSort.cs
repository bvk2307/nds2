﻿using System;
using System.ComponentModel;

namespace FLS.Common.Lib.Data
{
    [Serializable]
    public class ColumnSort : ICloneable
    {
        private ListSortDirection? _order;

        public static ListSortDirection? InvertOrder( ListSortDirection? order )
        {
            ListSortDirection? direction = null;
            switch ( order )
            {case ListSortDirection.Ascending:
                direction = ListSortDirection.Descending;
                break;
            case ListSortDirection.Descending:
                direction = ListSortDirection.Ascending;
                break;
            }
            return direction;
        }

        public string ColumnKey { get; set; }

        public bool NullAsZero { get; set; }

        public ListSortDirection? Order
        {
            get
            {
                if ( Inverted )
                    return InvertOrder( _order );
                return _order;
            }
            set { _order = Inverted ? InvertOrder( value ) : value; }
        }
        
        private SortStyle _sortKind = SortStyle.AlphabeticOrder;
        
        public SortStyle SortKind
        {
            get
            {
                return _sortKind;
            }
            set
            {
                if ( _sortKind != value )
                {
                    _sortKind = value;
                }
            }
        }

        public bool Inverted { get; set; }

        public object Clone()
        {
            return
                new ColumnSort()
                {
                    ColumnKey = ColumnKey,
                    Order = _order,
                    SortKind = SortKind,
                    Inverted = Inverted,
                    NullAsZero = NullAsZero
                };
        }
    }
}
