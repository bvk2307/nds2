﻿using System;
using System.Collections.Generic;

namespace FLS.Common.Lib.Data
{
    [Serializable]
    public class QueryDataPageResult<TData>
    {
        public QueryDataPageResult( IReadOnlyCollection<TData> data, int totalMatches )
        {
            Rows = data;
            TotalMatches = totalMatches;
        }

        /// <summary> The number value meeans that the row number value is not gotten. </summary>
        public static readonly int NumberNotGotten = int.MinValue;

        /// <summary> Записи на странице </summary>
        public IReadOnlyCollection<TData> Rows { get; set; }

        /// <summary> Количество записей удовлетворяющих выборке с учетом поиска </summary>
        public int TotalMatches { get; set; }

        ///// <summary> Количество записей удовлетворяющих выборке БЕЗ учета поиска </summary>
        //public int TotalAvailable { get; set; }
    }
}