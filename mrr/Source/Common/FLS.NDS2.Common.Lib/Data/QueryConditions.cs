﻿using System;
using System.Collections.Generic;
using FLS.Common.Lib.Collections.Extensions;
using FLS.CommonComponents.Lib.Serialization;

namespace FLS.Common.Lib.Data
{
    [Serializable]
    public class QueryConditions : ICloneable, IEquatable<QueryConditions>
    {
        public QueryConditions()
        {
            PaginationDetails = new QueryDataPage();
        }

        public QueryDataPage PaginationDetails
        {
            get;
            private set;
        }

        private IReadOnlyCollection<ColumnSort> sorting = new List<ColumnSort>().ToReadOnly();

        public IReadOnlyCollection<ColumnSort> Sorting 
        {
            get { return sorting; }

            set 
            {
                sorting = value ?? new List<ColumnSort>().ToReadOnly();
            }
        }

        private IReadOnlyCollection<QueryDataFilter> filter = new List<QueryDataFilter>().ToReadOnly();


        public IReadOnlyCollection<QueryDataFilter> Filter
        {
            get { return filter; }

            set { filter = value ?? new List<QueryDataFilter>().ToReadOnly(); }
        }

        public bool IsPagingDefined { get { return PaginationDetails.RowsToTake > 0; } }

        /// <summary> Returnes a deep clone created. </summary>
        /// <returns></returns>
        QueryConditions Clone()
        {
            return SerializationDeepCloner.Clone( this );
        }

        #region Implementation of ICloneable

        object ICloneable.Clone()
        {
            return Clone();
        }

        #endregion
        #region Equality members

        /// <summary>Determines whether the specified <see cref="T:System.Object" /> is equal to the current <see cref="T:System.Object" />.</summary>
        /// <returns>true if the specified <see cref="T:System.Object" /> is equal to the current <see cref="T:System.Object" />; otherwise, false.</returns>
        /// <param name="obj">The object to compare with the current object. </param>
        public override bool Equals( object obj )
        {
            if ( ReferenceEquals( null, obj ) ) return false;
            if ( ReferenceEquals( this, obj ) ) return true;
            if ( obj.GetType() != this.GetType() ) return false;
            return Equals( (QueryConditions)obj );
        }

        /// <summary>Serves as a hash function for a particular type. </summary>
        /// <returns>A hash code for the current <see cref="T:System.Object" />.</returns>
        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = ( Sorting != null ? Sorting.GetHashCode() : 0 );
                hashCode = ( hashCode * 397 ) ^ ( Filter != null ? Filter.GetHashCode() : 0 );
                hashCode = ( hashCode * 397 ) ^ ( PaginationDetails != null ? PaginationDetails.GetHashCode() : 0 );
                return hashCode;
            }
        }

        public static bool operator ==( QueryConditions left, QueryConditions right )
        {
            return Equals( left, right );
        }

        public static bool operator !=( QueryConditions left, QueryConditions right )
        {
            return !Equals( left, right );
        }

        #endregion

        public bool Equals( QueryConditions other )
        {
            return EqualsBase( other ) && Equals( Sorting, other.Sorting );
        }

        public bool EqualsBase( QueryConditions other )
        {
            if ( ReferenceEquals( null, other ) ) return false;
            if ( ReferenceEquals( this, other ) ) return true;
            return Equals( Filter, other.Filter ) && Equals( PaginationDetails, other.PaginationDetails );
        }

        public static bool EqualsSorting( QueryConditions qcOne, QueryConditions qcTwo )
        {
            if ( ReferenceEquals( qcOne, qcTwo ) ) return true;
            if ( ReferenceEquals( null, qcOne ) ) return false;
            if ( ReferenceEquals( null, qcTwo ) ) return false;
            return Equals( qcOne.Sorting, qcTwo.Sorting );
        }

        public QueryConditions Replace( string oldName, string newName )
        {
            if ( Filter != null )
                foreach ( QueryDataFilter x in Filter )
                    if ( x.ColumnName == oldName )
                        x.ColumnName = newName;

            if ( Sorting != null )
                foreach ( ColumnSort x in Sorting )
                    if ( x.ColumnKey == oldName )
                        x.ColumnKey = newName;

            return this;
        }
    }
}
