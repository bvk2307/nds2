﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using FLS.Common.Lib.Collections.Extensions;

namespace FLS.Common.Lib.Data
{
    [Serializable]
    public class QueryDataFilter : ICloneable
    {
        public QueryDataFilter()
        {
            UserDefined = false;
            ColumnType = typeof(string);
        }

        public QueryDataFilter(bool userDefined)
        {
            UserDefined = userDefined;
            ColumnType = typeof(string);
        }

        public enum FilterLogicalOperator
        {
            NotDefinedOperator,
            And,
            Or
        }

        public string ColumnName { get; set; }

        private bool _isComplex;
        public bool IsComplex
        {
            get { return _isComplex; }
            set { _isComplex = value; }
        }


        private FilterLogicalOperator _groupOperator = FilterLogicalOperator.And;
        public FilterLogicalOperator GroupOperator
        {
            get { return _groupOperator; }
            set { _groupOperator = value; }
        }


        public FilterLogicalOperator FilterOperator { get; set; }

        public IReadOnlyCollection<ColumnFilter> Filtering { get; set; }

        /// <summary>
        /// Возвращает или задает признак того, что фильтр создан пользователем, а не добавлен программно
        /// </summary>
        public bool UserDefined
        {
            get;
            set;
        }

        public object Clone()
        {
            return new QueryDataFilter
            {
                ColumnName = ColumnName,
                Filtering = Filtering.Select(x => (ColumnFilter)x.Clone()).ToReadOnly(),
                FilterOperator = FilterOperator,
                IsComplex = IsComplex,
                UserDefined = UserDefined
            };
        }

        [XmlIgnore]
        public Type ColumnType
        {
            get;
            set;
        }
    }
}
