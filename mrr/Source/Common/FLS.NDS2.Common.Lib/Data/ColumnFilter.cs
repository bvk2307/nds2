﻿using System;
using System.Xml.Serialization;

namespace FLS.Common.Lib.Data
{
    [XmlInclude( typeof( DBNull ) )]
    [Serializable]
    public class ColumnFilter : ICloneable, IEquatable<ColumnFilter>
    {
        public FilterComparisionOperator ComparisonOperator { get; set; }
        public object Value { get; set; }

        public object Clone()
        {
            return new ColumnFilter
            {
                ComparisonOperator = ComparisonOperator,
                Value = Value
            };
        }

        /// <summary>Indicates whether the current object is equal to another object of the same type.</summary>
        /// <returns>true if the current object is equal to the <paramref name="other" /> parameter; otherwise, false.</returns>
        /// <param name="other">An object to compare with this object.</param>
        public bool Equals( ColumnFilter other )
        {
            if ( ReferenceEquals( null, other ) ) return false;
            if ( ReferenceEquals( this, other ) ) return true;
            return ComparisonOperator == other.ComparisonOperator && Equals( Value, other.Value );
        }

        /// <summary>Determines whether the specified <see cref="T:System.Object" /> is equal to the current <see cref="T:System.Object" />.</summary>
        /// <returns>true if the specified <see cref="T:System.Object" /> is equal to the current <see cref="T:System.Object" />; otherwise, false.</returns>
        /// <param name="obj">The object to compare with the current object. </param>
        public override bool Equals( object obj )
        {
            if ( ReferenceEquals( null, obj ) ) return false;
            if ( ReferenceEquals( this, obj ) ) return true;
            if ( obj.GetType() != this.GetType() ) return false;
            return Equals( (ColumnFilter)obj );
        }

        /// <summary>Serves as a hash function for a particular type. </summary>
        /// <returns>A hash code for the current <see cref="T:System.Object" />.</returns>
        public override int GetHashCode()
        {
            unchecked
            {
                return ( (int)ComparisonOperator * 397 ) ^ ( Value != null ? Value.GetHashCode() : 0 );
            }
        }

        public static bool operator ==( ColumnFilter left, ColumnFilter right )
        {
            return Equals( left, right );
        }

        public static bool operator !=( ColumnFilter left, ColumnFilter right )
        {
            return !Equals( left, right );
        }

        /// <summary>Returns a string that represents the current object.</summary>
        /// <returns>A string that represents the current object.</returns>
        public override string ToString()
        {
            return string.Join( " ", "Operator:", ComparisonOperator, "Value:", Value );
        }
    }
}