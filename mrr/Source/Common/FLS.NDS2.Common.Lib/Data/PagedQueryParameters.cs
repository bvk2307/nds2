﻿using System;

namespace FLS.Common.Lib.Data
{
    [Serializable]
    public class PagedQueryParameters : QueryParameters
    {
        private readonly bool _isRowNumberRequested;

        public PagedQueryParameters( bool isRowNumberRequested = false )
        {
            _isRowNumberRequested = isRowNumberRequested;
        }

        public bool IsRowNumberRequested
        {
            get { return _isRowNumberRequested; }
        }
    }
}