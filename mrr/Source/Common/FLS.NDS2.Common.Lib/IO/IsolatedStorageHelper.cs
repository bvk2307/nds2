﻿using System;
using System.Diagnostics.Contracts;
using System.IO;
using System.IO.IsolatedStorage;

namespace FLS.Common.Lib.IO
{
    /// <summary> A helper around of <see cref="IsolatedStorageFile"/>. </summary>
    public static class IsolatedStorageHelper
    {
        public static IsolatedStorageFile GetStore_RoamingUser_Domain_Assembly( Type domainEvidenceType = null, Type assemblyEvidenceType = null )
        {
            IsolatedStorageFile isolatedStorage = IsolatedStorageFile.GetStore(
                IsolatedStorageScope.Roaming | IsolatedStorageScope.User | IsolatedStorageScope.Domain | IsolatedStorageScope.Assembly,
                domainEvidenceType, assemblyEvidenceType );

            return isolatedStorage;
        }

        public static IsolatedStorageFileStream OpenFile( IsolatedStorageFile isolatedStorage, string fileFullSpec )
        {
            Contract.Requires( isolatedStorage != null );
            Contract.Requires( !string.IsNullOrWhiteSpace( fileFullSpec ) );

            string filePath = Path.GetDirectoryName( fileFullSpec );
            if ( !string.IsNullOrEmpty( filePath ) )
            {
                string[] pathFolders = filePath.Split( new[] { Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar } );
                foreach ( string subfolderName in pathFolders )
                {
                    if ( !isolatedStorage.DirectoryExists( subfolderName ) )
                        isolatedStorage.CreateDirectory( subfolderName );
                }
            }
            IsolatedStorageFileStream fileStream = isolatedStorage.OpenFile( fileFullSpec, FileMode.OpenOrCreate );

            return fileStream;
        }
    }
}