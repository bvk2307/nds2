﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Reflection;

namespace FLS.Common.Lib.Attributes
{
    /// <summary> Auxilaries for <see cref="Attribute"/>. </summary>
    public static class AttributeReadHelper
    {
        /// <summary> Gets a value of <see cref="System.ComponentModel.DataAnnotations.DisplayAttribute"/> for an enumeration item. </summary>
        /// <param name="enumValue"></param>
        /// <returns> The value of <see cref="System.ComponentModel.DataAnnotations.DisplayAttribute"/> found if success otherwise <see cref="Enum.ToString()"/>. </returns>
        public static string GetDisplayName( Enum enumValue )
        {
            Contract.Requires( enumValue != null );

            GuardValueIsEnum( enumValue, "enumType" );

            string sDisplayName = null;
            DisplayAttribute displayName = GetAttribute<DisplayAttribute>( enumValue );
            sDisplayName = displayName == null ? enumValue.ToString() : displayName.Name;

            return sDisplayName;
        }

        /// <summary> Gets values of <see cref="System.ComponentModel.DataAnnotations.DisplayAttribute"/> for all enumeration items. </summary>
        /// <typeparam name="TEnum"></typeparam>
        /// <returns></returns>
        public static Dictionary<TEnum, string> GetDisplayNames<TEnum>()
        {
            Type enumType = GuardTypeIsEnum<TEnum>( "TEnum" );

            var displayNames = new Dictionary<TEnum, string>();
            foreach ( var enumItem in Enum.GetValues( enumType ) )
            {
                displayNames.Add( (TEnum)enumItem, AttributeReadHelper.GetDisplayName( (Enum)enumItem ) );
            }
            return displayNames;
        }

        /// <summary> Gets an <see cref="Attribute"/> of type <typeparamref name="TAtt"/> for an enumeration item. </summary>
        /// <typeparam name="TAtt"> A type of <see cref="Attribute"/>. </typeparam>
        /// <param name="enumValue"></param>
        /// <returns> The <see cref="Attribute"/> found if success otherwise 'null'. </returns>
        public static TAtt GetAttribute<TAtt>( Enum enumValue ) where TAtt : Attribute 
        {
            Contract.Requires( enumValue != null );

            GuardValueIsEnum( enumValue, "enumType" );

            TAtt attribute = null;
            FieldInfo fieldInfo = enumValue.GetType().GetField( enumValue.ToString() );
            if ( fieldInfo != null )
                attribute = (TAtt)fieldInfo.GetCustomAttributes( typeof(TAtt), inherit: false ).FirstOrDefault();

            return attribute;
        }

        private static void GuardValueIsEnum( Enum enumValue, string parameterName )
        {
            Type enumType = enumValue.GetType();
            if ( !enumType.IsEnum )
                throw new ArgumentOutOfRangeException( parameterName, "Enum types are supported only" );
        }

        private static Type GuardTypeIsEnum<TEnum>( string parameterName )
        {
            Type enumType = typeof(TEnum);
            if ( !enumType.IsEnum )
                throw new ArgumentOutOfRangeException( parameterName, "Enum types are supported only" );
            return enumType;
        }
    }
}