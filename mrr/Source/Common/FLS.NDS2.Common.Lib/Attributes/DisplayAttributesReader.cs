﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;

namespace FLS.Common.Lib.Attributes
{
    public sealed class DisplayAttributesReader<TModel, TField> : MemberAttributesReader<TModel, TField>
    {
        public DisplayAttributesReader( Expression<Func<TModel, TField>> lambda, IEnumerable<Attribute> attributesToSearch ) : base( lambda, attributesToSearch ) { }

        public string Title
        {
            get
            {
                string title = null;
                Attribute attrubute;
                if ( Attributes.TryGetValue( typeof(DisplayAttribute), out attrubute ) )
                    title = ( (DisplayAttribute)attrubute ).Name;
                if ( title == null && Attributes.TryGetValue( typeof(DisplayNameAttribute), out attrubute ) )
                    title = ( (DisplayNameAttribute)attrubute ).DisplayName;

                return title;
            }
        }

        public string Description
        {
            get
            {
                string description = null;
                Attribute attrubute;
                if ( Attributes.TryGetValue( typeof(DisplayAttribute), out attrubute ) )
                    description = ( (DisplayAttribute)attrubute ).Description;
                if ( description == null && Attributes.TryGetValue( typeof(DescriptionAttribute), out attrubute ) )
                    description = ( (DescriptionAttribute)attrubute ).Description;

                return description;
            }
        }
    }
}