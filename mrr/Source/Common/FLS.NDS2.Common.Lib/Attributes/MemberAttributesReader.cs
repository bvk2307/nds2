﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Linq.Expressions;
using FLS.Common.Lib.Comparers;

namespace FLS.Common.Lib.Attributes
{
    public class MemberAttributesReader<TModel, TField>
    {
        private readonly Dictionary<Type, Attribute> _foundAttributes;

        public MemberAttributesReader( Expression<Func<TModel, TField>> lambda, IEnumerable<Attribute> attributesToSearch )
        {
            Contract.Requires( lambda != null );
            Contract.Requires( lambda.Body is MemberExpression );
            Contract.Requires( attributesToSearch != null );
            Contract.Ensures( !string.IsNullOrEmpty( Name ) );
            Contract.Ensures( Attributes != null );

            MemberExpression body = (MemberExpression)lambda.Body;
            Name = body.Member.Name;
            IEnumerable<Attribute> attrs = body.Member.GetCustomAttributes( false ).Cast<Attribute>();
            var foundAttributes = 
                attrs.Join( attributesToSearch, attr => attr.GetType(), attrToSearch => attrToSearch.GetType(), 
                            ( attr, attrToSearch ) => new { Attriubte = attr, FoundAttribute = attrToSearch }, new IsAssignableFromTypeComparer() );

            _foundAttributes = foundAttributes.ToDictionary( attrPair => attrPair.FoundAttribute.GetType(), attrPair => attrPair.Attriubte );
        }

        public string Name { get; private set; }

        public Dictionary<Type, Attribute> Attributes { get { return _foundAttributes; } }
    }
}