﻿using System;
using System.Diagnostics.Contracts;
using FLS.Common.Lib.Primitives;

namespace FLS.Common.Lib.Events
{
    public sealed class CounterSimpleSemaphore : AbstractSemaphore
    {
        private readonly Int32Counter _counter;

        public CounterSimpleSemaphore( Int32Counter counter = null )
        {
            Contract.Ensures( _counter != null );

            _counter = counter ?? new Int32Counter();
        }

        /// <summary> Returns A value of the defined counter. </summary>
        public int CounterValue { get { return (int)_counter; } }

        /// <summary> Is the counter not turned off? </summary>
        public bool IsLive { get { return _counter != TurnedOffValue; } }

        /// <summary> Returns incremented with <paramref name="toSum"/> value of the internal counter. </summary>
        /// <param name="toSum"></param>
        /// <returns></returns>
        public int IncrementCounter(int toSum = 1 )
        {
            Contract.Requires( toSum > 0 );

            int incrementedValue = _counter.Increment( toSum );
            SetIsBusy();

            return incrementedValue;
        }

        /// <summary> Returns decremented with <paramref name="toSubtract"/> value of the internal counter. </summary>
        /// <param name="toSubtract"></param>
        /// <returns></returns>
        public int DecrementCounter( int toSubtract = 1 )
        {
            Contract.Requires( toSubtract > 0 );

            int decrementedValue = _counter.Decrement( toSubtract );
            SetIsBusy();

            return decrementedValue;
        }

        /// <summary> Turns off the counter assigning it to a negative value. </summary>
        public void TurnOffCounter()
        {
            _counter.Assign( TurnedOffValue );
            SetIsBusy();
        }

        protected override void SetIsBusy()
        {
            IsBusy = _counter > 0;
        }
    }
}