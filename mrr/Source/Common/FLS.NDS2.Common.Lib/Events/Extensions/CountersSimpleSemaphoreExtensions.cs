﻿using System.Diagnostics.Contracts;

namespace FLS.Common.Lib.Events.Extensions
{
    /// <summary> Extension methods for <see cref="CountersSimpleSemaphore{TIndexEnum}"/>. </summary>
    /// <remarks> It has the same algorithm that <see cref="CounterSimpleSemaphoreExtensions"/> has too. </remarks>
    public static class CountersSimpleSemaphoreExtensions
    {
        /// <summary> Returns 'true' if the counter has been incremented with value of <paramref name="toSum"/>. </summary>
        /// <typeparam name="TIndexEnum"></typeparam>
        /// <param name="counterSemaphore"></param>
        /// <param name="enumCounterIndex"></param>
        /// <param name="toSum"></param>
        /// <returns></returns>
        public static bool IncrementCounterIfEqual0<TIndexEnum>(
            this CountersSimpleSemaphore<TIndexEnum> counterSemaphore, TIndexEnum enumCounterIndex, int toSum = 1 )
        {
            Contract.Requires( counterSemaphore != null );
            Contract.Requires( toSum > 0 );

            int currentValue = counterSemaphore[ enumCounterIndex ];

            Contract.Assert( currentValue >= 0 );

            bool incremented = false;
            if ( currentValue == 0 )
            {
                currentValue = counterSemaphore.IncrementCounter( enumCounterIndex, toSum );

                Contract.Assert( currentValue > 0 );

                incremented = true;
            }
            return incremented;
        }

        /// <summary> Returns 'true' if the counter has been decremented with some value that equals or less than <paramref name="toSubtract"/>. </summary>
        /// <typeparam name="TIndexEnum"></typeparam>
        /// <param name="counterSemaphore"></param>
        /// <param name="enumCounterIndex"></param>
        /// <param name="toSubtract"></param>
        /// <returns></returns>
        public static bool DecrementCounterIfMoreThan0<TIndexEnum>( 
            this CountersSimpleSemaphore<TIndexEnum> counterSemaphore, TIndexEnum enumCounterIndex, int toSubtract = 1 )
        {
            Contract.Requires( counterSemaphore != null );
            Contract.Requires( toSubtract > 0 );

            int currentValue = counterSemaphore[ enumCounterIndex ];

            Contract.Assert( currentValue >= 0 );

            int? subtractDifference = null;
            if ( currentValue > 0 )
            {
                subtractDifference = currentValue - toSubtract;
                currentValue = counterSemaphore.DecrementCounter( enumCounterIndex, subtractDifference.Value >= 0 ? toSubtract : -subtractDifference.Value );

                Contract.Assert( currentValue >= 0 );
            }
            return subtractDifference.HasValue;
        }

        /// <summary> Turns off the counter assigning it to a negative value if the current value equals to '0'. </summary>
        /// <typeparam name="TIndexEnum"></typeparam>
        /// <param name="counterSemaphore"></param>
        /// <param name="enumCounterIndex"></param>
        /// <returns>  Returns 'true' if the counter has been assigned with a negative value. </returns>
        public static bool SwitchOffCounterIfEqual0<TIndexEnum>( 
            this CountersSimpleSemaphore<TIndexEnum> counterSemaphore, TIndexEnum enumCounterIndex )
        {
            Contract.Requires( counterSemaphore != null );

            int currentValue = counterSemaphore[ enumCounterIndex ];

            Contract.Assert( currentValue >= 0 );

            bool turnedOff = false;
            if ( currentValue == 0 )
            {
                counterSemaphore.SwitchOffCounter( enumCounterIndex );

                Contract.Assume( counterSemaphore[ enumCounterIndex ] < 0 );

                turnedOff = true;
            }
            return turnedOff;
        }

        /// <summary> Turns on the counter assigning it to <paramref name="initialValue"/> if the counter is turned off. </summary>
        /// <param name="counterSemaphore"></param>
        /// <param name="enumCounterIndex"></param>
        /// <param name="initialValue"></param>
        /// <returns>  Returns 'true' if the counter has been assigned with <paramref name="initialValue"/> value. </returns>
        public static bool SwitchOnCounterIfSwitchedOff<TIndexEnum>( 
            this CountersSimpleSemaphore<TIndexEnum> counterSemaphore, TIndexEnum enumCounterIndex, int initialValue = 0 )
        {
            Contract.Requires( counterSemaphore != null );

            int currentValue = counterSemaphore[ enumCounterIndex ];

            Contract.Assert( currentValue == AbstractSemaphore.TurnedOffValue || currentValue == initialValue );

            bool turnedOn = false;
            if ( currentValue == AbstractSemaphore.TurnedOffValue )
            {
                counterSemaphore.SwitchOnCounter( enumCounterIndex, initialValue );

                Contract.Assume( counterSemaphore[ enumCounterIndex ] == 0 );

                turnedOn = true;
            }
            return turnedOn;
        }
    }
}