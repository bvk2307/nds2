﻿using System.Diagnostics.Contracts;

namespace FLS.Common.Lib.Events.Extensions
{
    /// <summary> Extension methods for <see cref="CounterSimpleSemaphore"/>. </summary>
    /// <remarks> It has the same algorithm that <see cref="CountersSimpleSemaphoreExtensions"/> has too. </remarks>
    public static class CounterSimpleSemaphoreExtensions
    {
        /// <summary> Returns 'true' if the counter has been incremented with value of <paramref name="toSum"/>. </summary>
        /// <param name="counterSemaphore"></param>
        /// <param name="toSum"></param>
        /// <returns></returns>
        public static bool IncrementCounterIfEqual0( this CounterSimpleSemaphore counterSemaphore, int toSum = 1 )
        {
            Contract.Requires( counterSemaphore != null && counterSemaphore.CounterValue >= 0 );
            Contract.Requires( toSum > 0 );
            Contract.Ensures( counterSemaphore.CounterValue > 0 );

            int currentValue = counterSemaphore.CounterValue;

            bool incremented = false;
            if ( currentValue == 0 )
            {
                currentValue = counterSemaphore.IncrementCounter( toSum );

                Contract.Assert( currentValue > 0 );

                incremented = true;
            }
            return incremented;
        }
 
        /// <summary> Returns 'true' if the counter has been decremented with some value that equals or less than <paramref name="toSubtract"/>. </summary>
        /// <param name="counterSemaphore"></param>
        /// <param name="toSubtract"></param>
        /// <returns></returns>
        public static bool DecrementCounterIfMoreThan0( this CounterSimpleSemaphore counterSemaphore, int toSubtract = 1 )
        {
            Contract.Requires( counterSemaphore != null && counterSemaphore.CounterValue >= 0 );
            Contract.Requires( toSubtract > 0 );
            Contract.Ensures( counterSemaphore.CounterValue >= 0 );

            int currentValue = counterSemaphore.CounterValue;

            int? subtractDifference = null;
            if ( currentValue > 0 )
            {
                subtractDifference = currentValue - toSubtract;
                counterSemaphore.DecrementCounter( subtractDifference.Value >= 0 ? toSubtract : -subtractDifference.Value );
            }
            return subtractDifference.HasValue;
        }

        /// <summary> Turns off the counter assigning it to a negative value if the current value equals to '0'. </summary>
        /// <param name="counterSemaphore"></param>
        /// <returns>  Returns 'true' if the counter has been assigned with a negative value. </returns>
        public static bool TurnOffCounterIfEqual0( this CounterSimpleSemaphore counterSemaphore )
        {
            Contract.Requires( counterSemaphore != null && counterSemaphore.CounterValue >= 0 );

            int currentValue = counterSemaphore.CounterValue;

            bool turnedOff = false;
            if ( currentValue == 0 )
            {
                counterSemaphore.TurnOffCounter();

                Contract.Assume( counterSemaphore.CounterValue < 0 );

                turnedOff = true;
            }
            return turnedOff;
        }
    }
}