﻿using System.ComponentModel;
using System.Diagnostics.Contracts;
using System.Runtime.CompilerServices;
using FLS.Common.Lib.Interfaces;

namespace FLS.Common.Lib.Events
{
    public abstract class AbstractSemaphore : IBusy, INotifyPropertyChanged
    {
        internal protected const int TurnedOffValue = int.MinValue;

        private bool _isBusy = false;

        public virtual event PropertyChangedEventHandler PropertyChanged;

        /// <summary> 'true' if some counter value is more than 0. </summary>
        public bool IsBusy
        {
            get{ return _isBusy; }
            protected set
            {
                if ( _isBusy != value )
                {
                    _isBusy = value;

                    OnPropertyChanged();                    
                }
            }
        }

        protected abstract void SetIsBusy();

        private void OnPropertyChanged(
            [CallerMemberName]
            string publicPropertyName = null )
        {
            Contract.Requires( !string.IsNullOrEmpty( publicPropertyName ) );

            PropertyChangedEventHandler handlers = PropertyChanged;
            if ( handlers != null )
                handlers.Invoke( this, new PropertyChangedEventArgs( publicPropertyName ) );
        }
    }
}