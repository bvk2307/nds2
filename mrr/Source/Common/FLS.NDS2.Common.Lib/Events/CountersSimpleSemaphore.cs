﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.Contracts;
using System.Linq;
using FLS.Common.Lib.Interfaces;
using FLS.Common.Lib.Primitives;

namespace FLS.Common.Lib.Events
{
    /// <summary> Signals by <see cref="AbstractSemaphore.IsBusy"/> if some counter value is more than 0. </summary>
    public sealed class CountersSimpleSemaphore<TIndexEnum> : AbstractSemaphore
    {
        private readonly Int32Counter[] _counters;

        static CountersSimpleSemaphore()
        {
            Contract.Requires( typeof(TIndexEnum).IsEnum );
            Contract.Requires( (int)Enum.GetValues( typeof(TIndexEnum) ).GetValue( 0 ) == 0 );
        }

        public CountersSimpleSemaphore( IEnumerable<Int32Counter> counters )
        {
            Contract.Requires( counters != null );
            Contract.Requires( 0 < Enum.GetValues( typeof(TIndexEnum) ).Length );
            Contract.Ensures( Enum.GetValues( typeof(TIndexEnum) ).Length == _counters.Length );

            _counters = counters.ToArray();
        }

        /// <summary> Returns A value of the defined counter. </summary>
        /// <param name="enumCounterIndex"></param>
        /// <returns></returns>
        public int this[ TIndexEnum enumCounterIndex ]
        {
            get
            {
                Contract.Requires( Enum.IsDefined( typeof(TIndexEnum), enumCounterIndex ) );

                return (int)Counter( enumCounterIndex );
            }
        }

        /// <summary> Is the counter not turned off? </summary>
        /// <param name="enumCounterIndex"></param>
        /// <returns></returns>
        public bool IsLive( TIndexEnum enumCounterIndex )
        {
            Contract.Requires( Enum.IsDefined( typeof(TIndexEnum), enumCounterIndex ) );

            return Counter( enumCounterIndex ) != TurnedOffValue;
        }

        /// <summary> Returns incremented with <paramref name="toSum"/> value of the internal counter. </summary>
        /// <param name="enumCounterIndex"></param>
        /// <param name="toSum"></param>
        /// <returns></returns>
        public int IncrementCounter( TIndexEnum enumCounterIndex, int toSum = 1 )
        {
            Contract.Requires( Enum.IsDefined( typeof(TIndexEnum), enumCounterIndex ) );
            Contract.Requires( toSum > 0 );

            int incrementedValue = Counter( enumCounterIndex ).Increment( toSum );
            SetIsBusy();

            return incrementedValue;
        }

        /// <summary> Returns decremented with <paramref name="toSubtract"/> value of the internal counter. </summary>
        /// <param name="enumCounterIndex"></param>
        /// <param name="toSubtract"></param>
        /// <returns></returns>
        public int DecrementCounter( TIndexEnum enumCounterIndex, int toSubtract = 1 )
        {
            Contract.Requires( Enum.IsDefined( typeof(TIndexEnum), enumCounterIndex ) );
            Contract.Requires( toSubtract > 0 );

            int decrementedValue = Counter( enumCounterIndex ).Decrement( toSubtract );
            SetIsBusy();

            return decrementedValue;
        }

        /// <summary> Turns off the counter assigning it to a negative value. </summary>
        /// <param name="enumCounterIndex"></param>
        public void SwitchOffCounter( TIndexEnum enumCounterIndex )
        {
            Contract.Requires( Enum.IsDefined( typeof(TIndexEnum), enumCounterIndex ) );

            Counter( enumCounterIndex ).Assign( TurnedOffValue );
            SetIsBusy();
        }

        /// <summary> Turns on the counter assigning it to <paramref name="initialValue"/>. </summary>
        /// <param name="enumCounterIndex"></param>
        /// <param name="initialValue"></param>
        public void SwitchOnCounter( TIndexEnum enumCounterIndex, int initialValue = 0 )
        {
            Contract.Requires( Enum.IsDefined( typeof(TIndexEnum), enumCounterIndex ) );

            Counter( enumCounterIndex ).Assign( initialValue );
            SetIsBusy();
        }

        protected override void SetIsBusy()
        {
            bool isBusy = false;
            foreach ( Int32Counter counter in _counters )
            {
                if ( counter > 0 )
                {
                    isBusy = true;
                    break;
                }
            }
            IsBusy = isBusy;
        }

        private Int32Counter Counter( TIndexEnum enumCounterIndex )
        {
            int nI = (int)Enum.ToObject( typeof(TIndexEnum), enumCounterIndex );

            Contract.Assert( 0 <= nI && nI < _counters.Length );

            return _counters[ nI ];
        }
    }
}