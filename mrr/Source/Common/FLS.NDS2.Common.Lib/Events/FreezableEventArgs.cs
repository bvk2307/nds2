﻿using System;
using System.Diagnostics.Contracts;
using System.Windows;

namespace FLS.Common.Lib.Events
{
    /// <summary> <see cref="EventArgs"/> that can be frozen. </summary>
    public abstract class FreezableEventArgs : EventArgs
    {
        private readonly FreezableEventArgsProperties _propertyContainer;

        protected FreezableEventArgs()
        {
            _propertyContainer = new FreezableEventArgsProperties();
        }

        public bool IsFrozen { get { return _propertyContainer.IsFrozen; } }

        public FreezableEventArgs Freeze()
        {
            _propertyContainer.Freeze();

            return this;
        }

        protected void SetValue( DependencyProperty dependencyProperty, object value )
        {
            Contract.Requires( dependencyProperty != null );

            _propertyContainer.SetValue( dependencyProperty, value );   
        }

        protected object GetValue( DependencyProperty dependencyProperty )
        {
            Contract.Requires( dependencyProperty != null );

            return _propertyContainer.GetValue( dependencyProperty );
        }

        public class FreezableEventArgsProperties : Freezable
        {
            /// <summary> Can be used by generic type constraint. </summary>
            public FreezableEventArgsProperties() { }

            /// <summary>When implemented in a derived class, creates a new instance of the <see cref="T:System.Windows.Freezable" /> derived class. </summary>
            /// <returns>The new instance.</returns>
            protected override Freezable CreateInstanceCore()
            {
                return new FreezableEventArgsProperties();
            }
        }
    }
}