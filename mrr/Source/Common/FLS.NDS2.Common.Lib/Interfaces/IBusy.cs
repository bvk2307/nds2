﻿namespace FLS.Common.Lib.Interfaces
{
    public interface IBusy
    {
        bool IsBusy { get; }
    }
}