﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.Contracts;
using System.Linq;

namespace FLS.Common.Lib.Collections.Extensions
{
    /// <summary> Extension methods for <see cref="IEnumerable{T}"/>. </summary>
    public static class EnumerableExtensions
    {
        public static uint MaxWDefault<T>( this IEnumerable<T> enumeration, Func<T, uint> selector, uint defaultValue = 0 )
        {
            Contract.Requires( enumeration != null );

            uint result = defaultValue;
            foreach ( T item in enumeration )
            {
                uint current = selector( item );
                if ( result < current )
                    result = current;
            }
            return result;
        }

        public static IReadOnlyCollection<T> ToReadOnly<T>( this IEnumerable<T> enumeration )
        {
            IList<T> ilist = null;
            List<T> list = null;
            T[] array = enumeration as T[];
            if ( array == null )
            {
                ilist = enumeration as IList<T>;
                if ( ilist != null )
                    list = ilist as List<T>;
            }
            ReadOnlyCollection<T> collection = 
                list?.AsReadOnly() 
                    ?? ( array != null ? Array.AsReadOnly( array )
                                       : ilist == null ? Array.AsReadOnly( enumeration.ToArray() ) 
                                                       : new ReadOnlyCollection<T>( ilist ) );
            return collection;
        }
    }
}