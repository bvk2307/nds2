﻿using System;
using System.Diagnostics;
using System.Diagnostics.Contracts;

namespace FLS.Common.Lib.Primitives
{
    /// <summary> A simple wrapper around <see cref="int"/> value. </summary>
    [Serializable]
    [DebuggerDisplay( "Counter: {_counter}" )]
    public class Int32Counter
    {
        private int _counter = 0;

        public Int32Counter( int initValue = 0 )
        {
            _counter = initValue;
        }

        /// <summary> Increments the internal counter with <paramref name="toSum"/> value. </summary>
        /// <param name="toSum"> A value to add. Optional. By default is '1'. </param>
        /// <returns> The result counter value. </returns>
        public int Increment( int toSum = 1 )
        {
            return _counter += toSum;
        }

        /// <summary> Decrements the internal counter with <paramref name="toSubtract"/> value. </summary>
        /// <param name="toSubtract"> A value to subtract. Optional. By default is '1'. </param>
        /// <returns> The result counter value. </returns>
        public int Decrement( int toSubtract = 1 )
        {
            return _counter -= toSubtract;
        }

        /// <summary> Assigns a new value to the internal counter without replacement with a new instance of <see cref="Int32Counter"/> as it is in operator '=' and type casting case. </summary>
        /// <param name="newValue"></param>
        /// <returns></returns>
        public int Assign( int newValue )
        {
            int prevValue = _counter;
            _counter = newValue;

            return prevValue;
        }

        /// <summary> Implicit conversion from <see cref="int"/> to <see cref="Int32Counter"/>. </summary>
        /// <param name="value"></param>
        public static implicit operator Int32Counter( int value )
        {
            return new Int32Counter( value );
        }

        /// <summary> Explicit conversion from <see cref="Int32Counter"/> to <see cref="int"/>. </summary>
        /// <param name="counter"></param>
        public static explicit operator int( Int32Counter counter )
        {
            Contract.Requires( counter != null );

            return counter._counter;
        }

        /// <summary> ATTENTION! DON'T use as post increment only as preincrement. Post increment semanti does not work on reference types at all. </summary>
        /// <param name="counter"></param>
        /// <returns></returns>
        public static Int32Counter operator ++( Int32Counter counter )
        {
            Contract.Requires( counter != null );

            counter.Increment();

            return counter;
        }

        /// <summary> ATTENTION! DON'T use as post decrement only as predecrement. Post decrement semanti does not work on reference types at all. </summary>
        /// <param name="counter"></param>
        /// <returns></returns>
        public static Int32Counter operator --( Int32Counter counter )
        {
            Contract.Requires( counter != null );

            counter.Decrement();

            return counter;
        }

        public static bool operator >( Int32Counter counter1, int value2 )
        {
            if ( object.Equals( (object)counter1, null ) )
            {
                if ( object.Equals( (object)value2, null ) )
                    return false;
                return 0 > value2;
            }
            if ( object.Equals( (object)value2, null ) )
                return counter1._counter > 0;
            return counter1._counter > value2;
        }

        public static bool operator <( Int32Counter counter1, int value2 )
        {
            if ( object.Equals( (object)counter1, null ) )
            {
                if ( object.Equals( (object)value2, null ) )
                    return false;
                return 0 < value2;
            }
            if ( object.Equals( (object)value2, null ) )
                return counter1._counter < 0;
            return counter1._counter < value2;
        }

        public static bool operator >=( Int32Counter counter1, int value2 )
        {
            if ( object.Equals( (object)counter1, null ) )
            {
                if ( object.Equals( (object)value2, null ) )
                    return false;
                return 0 >= value2;
            }
            if ( object.Equals( (object)value2, null ) )
                return counter1._counter >= 0;
            return counter1._counter >= value2;
        }

        public static bool operator <=( Int32Counter counter1, int value2 )
        {
            if ( object.Equals( (object)counter1, null ) )
            {
                if ( object.Equals( (object)value2, null ) )
                    return false;
                return 0 <= value2;
            }
            if ( object.Equals( (object)value2, null ) )
                return counter1._counter <= 0;
            return counter1._counter <= value2;
        }

        public static bool operator >( int value1, Int32Counter counter2 )
        {
            if ( object.Equals( (object)value1, null ) )
            {
                if ( object.Equals( (object)counter2, null ) )
                    return false;
                return 0 > counter2._counter;
            }
            if ( object.Equals( (object)counter2, null ) )
                return value1 > 0;
            return value1 > counter2._counter;
        }

        public static bool operator <( int value1, Int32Counter counter2 )
        {
            if ( object.Equals( (object)value1, null ) )
            {
                if ( object.Equals( (object)counter2, null ) )
                    return false;
                return 0 < counter2._counter;
            }
            if ( object.Equals( (object)counter2, null ) )
                return value1 < 0;
            return value1 < counter2._counter;
        }

        public static bool operator >=( int value1, Int32Counter counter2 )
        {
            if ( object.Equals( (object)value1, null ) )
            {
                if ( object.Equals( (object)counter2, null ) )
                    return false;
                return 0 >= counter2._counter;
            }
            if ( object.Equals( (object)counter2, null ) )
                return value1 >= 0;
            return value1 >= counter2._counter;
        }

        public static bool operator <=( int value1, Int32Counter counter2 )
        {
            if ( object.Equals( (object)value1, null ) )
            {
                if ( object.Equals( (object)counter2, null ) )
                    return false;
                return 0 <= counter2._counter;
            }
            if ( object.Equals( (object)counter2, null ) )
                return value1 <= 0;
            return value1 <= counter2._counter;
        }

        public static bool operator >( Int32Counter counter1, Int32Counter counter2 )
        {
            if ( object.Equals( (object)counter1, null ) )
            {
                if ( object.Equals( (object)counter2, null ) )
                    return false;
                return 0 > counter2._counter;
            }
            if ( object.Equals( (object)counter2, null ) )
                return counter1._counter > 0;
            return counter1._counter > counter2._counter;
        }

        public static bool operator <( Int32Counter counter1, Int32Counter counter2 )
        {
            if ( object.Equals( (object)counter1, null ) )
            {
                if ( object.Equals( (object)counter2, null ) )
                    return false;
                return 0 < counter2._counter;
            }
            if ( object.Equals( (object)counter2, null ) )
                return counter1._counter < 0;
            return counter1._counter < counter2._counter;
        }

        public static bool operator >=( Int32Counter counter1, Int32Counter counter2 )
        {
            if ( object.Equals( (object)counter1, null ) )
            {
                if ( object.Equals( (object)counter2, null ) )
                    return false;
                return 0 >= counter2._counter;
            }
            if ( object.Equals( (object)counter2, null ) )
                return counter1._counter >= 0;
            return counter1._counter >= counter2._counter;
        }

        public static bool operator <=( Int32Counter counter1, Int32Counter counter2 )
        {
            if ( object.Equals( (object)counter1, null ) )
            {
                if ( object.Equals( (object)counter2, null ) )
                    return false;
                return 0 <= counter2._counter;
            }
            if ( object.Equals( (object)counter2, null ) )
                return counter1._counter <= 0;
            return counter1._counter <= counter2._counter;
        }

        public static bool operator ==( Int32Counter counter1, int value2 )
        {
            if ( object.Equals( (object)counter1, null ) )
                return object.Equals( (object)value2, null );
            if ( object.Equals( (object)value2, null ) )
                return false;
            return counter1._counter == value2;
        }

        public static bool operator !=( Int32Counter counter1, int value2 )
        {
            if ( object.Equals( (object)counter1, null ) )
                return object.Equals( (object)value2, null );
            if ( object.Equals( (object)value2, null ) )
                return true;
            return counter1._counter != value2;
        }

        public static bool operator ==( int value1, Int32Counter counter2 )
        {
            if ( object.Equals( (object)value1, null ) )
                return object.Equals( (object)counter2, null );
            if ( object.Equals( (object)counter2, null ) )
                return false;
            return value1 == counter2._counter;
        }

        public static bool operator !=( int value1, Int32Counter counter2 )
        {
            if ( object.Equals( (object)value1, null ) )
                return object.Equals( (object)counter2, null );
            if ( object.Equals( (object)counter2, null ) )
                return true;
            return value1 != counter2._counter;
        }

        public static bool operator ==( Int32Counter counter1, Int32Counter counter2 )
        {
            if ( object.Equals( (object)counter1, null ) )
                return object.Equals( (object)counter2, null );
            if ( object.Equals( (object)counter2, null ) )
                return false;
            return counter1._counter == counter2._counter;
        }

        public static bool operator !=( Int32Counter counter1, Int32Counter counter2 )
        {
            if ( object.Equals( (object)counter1, null ) )
                return !object.Equals( (object)counter2, null );
            if ( object.Equals( (object)counter2, null ) )
                return true;
            return counter1._counter != counter2._counter;
        }

        public override bool Equals( object oCounter )
        {
            if ( oCounter is int )
                return this == (int)oCounter;
            Int32Counter counter = oCounter as Int32Counter;
            if ( counter == null )
                return false;
            return this == counter;
        }

        public override int GetHashCode()
        {
            return _counter;
        }

        public override string ToString()
        {
            return _counter.ToString();
        }
    }
}