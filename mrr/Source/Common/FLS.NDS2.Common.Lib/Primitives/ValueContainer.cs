﻿using System;
using System.Diagnostics;

namespace FLS.Common.Lib.Primitives
{
    /// <summary> A simple container of some chnagable value. </summary>
    [Serializable]
    [DebuggerDisplay( "Value: {Value}" )]
    public class ValueContainer<TValue>
    {
        public TValue Value { get; set; }    

        public override string ToString()
        {
            return object.Equals( Value, null ) ? string.Empty : Value.ToString();
        }
    }
}