﻿using System;
using System.Collections.Generic;

namespace FLS.Common.Lib.Comparers
{
    /// <summary> A type comparer using <see cref="Type.IsAssignableFrom(Type)"/> to comparer types. ATTENTION! The order of comparing operands is important! </summary>
    public class IsAssignableFromTypeComparer : IEqualityComparer<Type>
    {
        #region Implementation of IEqualityComparer<in Type>

        /// <summary>Determines whether the specified objects are equal.</summary>
        /// <returns>true if the specified objects are equal; otherwise, false.</returns>
        /// <param name="x">The first object of type <see cref="Type"/> to compare.</param>
        /// <param name="y">The second object of type <see cref="Type"/> to compare.</param>
        public bool Equals( Type x, Type y )
        {
            return x.IsAssignableFrom( y );
        }

        /// <summary>Returns a hash code for the specified object.</summary>
        /// <returns>A hash code for the specified object.</returns>
        /// <param name="obj">The <see cref="T:System.Object" /> for which a hash code is to be returned.</param>
        /// <exception cref="T:System.ArgumentNullException">The type of <paramref name="obj" /> is a reference type and <paramref name="obj" /> is null.</exception>
        public int GetHashCode( Type obj )
        {
            return obj.GetHashCode();   //ATTENTION! It is different for different types X and Y for which 'IsAssignableFromTypeComparer.Equals( X, Y ) == true'
        }

        #endregion Implementation of IEqualityComparer<in Type>
    }
}