﻿using System.Windows;
using FLS.CommonComponents.Themes.Wpf.Styles;

namespace FLS.CommonComponents.Themes.Wpf.ResourceKeys
{
    /// <summary> The same style of 'RadDataPagerEx' like 'x:Key="RadDataPagerStyle"'. </summary>
    public static class TelerikRadDataPagerStyleKey
    {
        public static ComponentResourceKey Key { get { return new ComponentResourceKey( typeof(TelerikRadDataPagerStyleKey), SharedResourceConsts.UnusedResourceKeyId ); } }
    }
    /// <summary> The moved into style of 'RadRadioButton' that had the key 'x:Key="RadPageStyle"' in Styles\Telerik\Themes\Telerik.Windows.Controls.Data.xaml .</summary>
    public static class TelerikRadPageStyleKey
    {
        public static ComponentResourceKey Key { get { return new ComponentResourceKey( typeof(TelerikRadPageStyleKey), SharedResourceConsts.UnusedResourceKeyId ); } }
    }
}