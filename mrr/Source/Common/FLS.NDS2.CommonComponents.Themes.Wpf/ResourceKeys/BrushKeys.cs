﻿using System.Windows;
using FLS.CommonComponents.Themes.Wpf.Styles;

namespace FLS.CommonComponents.Themes.Wpf.ResourceKeys
{
    /// <summary> The moved into brush that had the key 'x:Key="RadPager_Separator1"' in Styles\Telerik\Themes\Telerik.Windows.Controls.Data.xaml .</summary>
    public static class RadPager_Separator1_BrushKey
    {
        public static ComponentResourceKey Key { get { return new ComponentResourceKey( typeof(RadPager_Separator1_BrushKey), SharedResourceConsts.UnusedResourceKeyId ); } }
    }
    /// <summary> The moved into brush that had the key 'x:Key="RadPager_Separator2"' in Styles\Telerik\Themes\Telerik.Windows.Controls.Data.xaml .</summary>
    public static class RadPager_Separator2_BrushKey
    {
        public static ComponentResourceKey Key { get { return new ComponentResourceKey( typeof(RadPager_Separator2_BrushKey), SharedResourceConsts.UnusedResourceKeyId ); } }
    }
}