﻿using System.Diagnostics.Contracts;
using System.Windows;
using FLS.CommonComponents.Themes.Wpf.Styles.Telerik;

namespace FLS.CommonComponents.Themes.Wpf.Styles
{
    /// <summary> Added functionality to sharing styles. </summary>
    public static class SharedResources
    {
        public static readonly DependencyProperty MergedDictionariesProperty = DependencyProperty.RegisterAttached( "MergedDictionaries", typeof(string), typeof(SharedResources), 
            new FrameworkPropertyMetadata( (string)null, new PropertyChangedCallback( OnMergedDictionariesChanged ) ) );

        public static string GetMergedDictionaries( DependencyObject depObject )
        {
            Contract.Requires( depObject != null );

            return (string)depObject.GetValue( MergedDictionariesProperty );
        }

        public static void SetMergedDictionaries( DependencyObject depObject, string value )
        {
            Contract.Requires( depObject != null );

            depObject.SetValue( MergedDictionariesProperty, value );
        }

        private static void OnMergedDictionariesChanged( DependencyObject depObject, DependencyPropertyChangedEventArgs eventArgs )
        {
            Contract.Requires( depObject != null );

            string newValue = eventArgs.NewValue as string;
            if ( !string.IsNullOrEmpty( newValue ) && ( depObject is FrameworkElement || depObject is FrameworkContentElement ) )
            {
                foreach ( string styleFilename in newValue.Split( ';' ) )
                {
                    ResourceDictionary resourceDictionary = TelerikLoader.LoadTelerikStyle( styleFilename );
                    FrameworkElement element = depObject as FrameworkElement;
                    if ( element == null )
                    {
                        FrameworkContentElement contentElement = (FrameworkContentElement)depObject;
                        contentElement.Resources.MergedDictionaries.Add( resourceDictionary );
                    }
                    else
                    {
                        element.Resources.MergedDictionaries.Add( resourceDictionary );
                    }
                }
            }
        }
    }
}