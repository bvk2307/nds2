﻿using System;
using System.Collections.Generic;
using System.Windows.Data;

namespace FLS.CommonComponents.Themes.Wpf.Styles.Telerik.Converters
{
    public class CountConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var result = new List<int>();
            result.AddRange(new []{50, 100, 150, 200});
            //if (value != DependencyProperty.UnsetValue)
            //{
            //    var itemCount = (int)value;
            //    for (int i = 1; i <= itemCount; i++)
            //    {
            //        if (i % 50 == 0)
            //        {
            //            result.Add(i);
            //        }
            //    }
            //    var lastCount = result.LastOrDefault();
            //    if (itemCount != 0 && lastCount < itemCount)
            //    {
            //        result.Add(itemCount);
            //    }    
            //}
            return result;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
