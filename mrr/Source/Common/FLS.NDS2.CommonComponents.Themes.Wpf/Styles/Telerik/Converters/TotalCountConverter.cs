﻿using System;
using System.Windows.Data;

namespace FLS.CommonComponents.Themes.Wpf.Styles.Telerik.Converters
{
    public class TotalCountConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return string.Format("(из {0})", value);
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value;
        }
    }
}
