﻿using System.Diagnostics.Contracts;
using Telerik.Windows.Controls;

namespace FLS.CommonComponents.Themes.Wpf.Styles.Telerik
{
    public class TelerikCustomLocalizationManager : LocalizationManager
    {
        public override string GetStringOverride(string key)
        {
            Contract.Assert( !string.IsNullOrEmpty( key ) );

            switch (key)
            {
                case "GridViewGroupPanelText":
                    return "Перетащите колонку для группировки";
                case "GridViewClearFilter":
                    return "Очистить";
                case "GridViewFilterShowRowsWithValueThat":
                    return "Отфильтровать по правилу:";
                case "GridViewFilterSelectAll":
                    return "Выделить все";
                case "GridViewFilterContains":
                    return "Содержит";
                case "GridViewFilterEndsWith":
                    return "Оканчивается на";
                case "GridViewFilterIsContainedIn":
                    return "Содержится в";
                case "GridViewFilterIsNotContainedIn":
                    return "Не содержится в";
                case "GridViewFilterDoesNotContain":
                    return "Не содержит";
                case "GridViewFilterIsNull":
                    return "Нулевое значение";
                case "GridViewFilterIsNotNull":
                    return "Не нулевое значение";
                case "GridViewFilterDistinctValueNull":
                    return "";
                case "GridViewFilterDistinctValueStringEmpty":
                    return "[пусто]";
                case "GridViewFilterIsEmpty":
                    return "Пустое значение";
                case "GridViewFilterIsNotEmpty":
                    return "Не пустое значение";
                case "GridViewAlwaysVisibleNewRow":
                    return "Выберите для добавления";
                case "GridViewGroupPanelTopText":
                    return "Заголовок группы";
                case "GridViewGroupPanelTopTextGrouped":
                    return "Сгруппированно по";
                case "GridViewColumnsSelectionButtonTooltip":
                    return "Выбрать колонки";
                case "GridViewFilterIsEqualTo":
                    return "Равно";
                case "GridViewFilterIsGreaterThan":
                    return "Больше";
                case "GridViewFilterIsGreaterThanOrEqualTo":
                    return "Больше или равно";
                case "GridViewFilterIsLessThan":
                    return "Меньше";
                case "GridViewFilterIsLessThanOrEqualTo":
                    return "Меньше или равно";
                case "GridViewFilterIsNotEqualTo":
                    return "Не равно";
                case "GridViewFilterStartsWith":
                    return "Начинается с";
                case "GridViewFilterAnd":
                    return "И";
                case "GridViewFilterOr":
                    return "Или";
                case "GridViewFilter":
                    return "Применить";
                case "GridViewFilterMatchCase":
                    return "Учитывать регистр";
                case "GridViewSearchPanelTopText":
                    return "Поиск:";
                case "RadDataPagerOf":
                    return "из";
                case "RadDataPagerPage":
                    return "Стр.";
                case "RadDataOnPage":
                    return "На стр.";
                case "RadDataTotal":
                    return "Всего";
                case "GidViewFilterIsTrue":
                    return "Да";
                case "GridViewFilterIsFalse":
                    return "Нет";

                case "Close":
                    return "Закрыть";
                case "EnterDate":
                    return "Введите дату";
                case "Error":
                    return "Ошибка";
                case "Today":
                    return "Сегодня";
            }
            return base.GetStringOverride(key);
        }
    }
}
