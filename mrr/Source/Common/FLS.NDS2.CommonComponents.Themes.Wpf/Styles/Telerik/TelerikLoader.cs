﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.Contracts;
using System.Reflection;
using System.Windows;
using Telerik.Windows.Controls;

namespace FLS.CommonComponents.Themes.Wpf.Styles.Telerik
{
    public static class TelerikLoader
    {
        private static readonly Dictionary<string, ResourceDictionary> s_styles = new Dictionary<string, ResourceDictionary>(8);

        private static string s_assemblyShortName = null;

        public static void Init()
        {
            //var path = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Application.ExecutablePath), ClientConstants.UserSettingsKeys.PATH);
            //var a = Assembly.LoadFrom(System.IO.Path.Combine(path, "Telerik.Windows.Themes.Office2013.dll"));

            //Application.Current.Resources.MergedDictionaries.Add(
            //    Application.LoadComponent(new Uri("/Telerik.Windows.Themes.Office2013;component/Themes/System.Windows.xaml", UriKind.Relative)) as ResourceDictionary);
            //Application.Current.Resources.MergedDictionaries.Add(
            //    Application.LoadComponent(new Uri("/Telerik.Windows.Themes.Office2013;component/Themes/Telerik.Windows.Controls.xaml", UriKind.Relative)) as ResourceDictionary);
            //Application.Current.Resources.MergedDictionaries.Add(
            //    Application.LoadComponent(new Uri("/Telerik.Windows.Themes.Office2013;component/Themes/Telerik.Windows.Controls.Navigation.xaml", UriKind.Relative)) as ResourceDictionary);
            //Application.Current.Resources.MergedDictionaries.Add(
            //    Application.LoadComponent(new Uri("/Telerik.Windows.Themes.Office2013;component/Themes/Telerik.Windows.Controls.Input.xaml", UriKind.Relative)) as ResourceDictionary);
            //Application.Current.Resources.MergedDictionaries.Add(
            //    Application.LoadComponent(new Uri("/Telerik.Windows.Themes.Office2013;component/Themes/Telerik.Windows.Controls.Chart.xaml", UriKind.Relative)) as ResourceDictionary);
            //Application.Current.Resources.MergedDictionaries.Add(
            //    Application.LoadComponent(new Uri("/Telerik.Windows.Themes.Office2013;component/Themes/Telerik.Windows.Controls.GridView.xaml", UriKind.Relative)) as ResourceDictionary);
            //Application.Current.Resources.MergedDictionaries.Add(
            //    Application.LoadComponent(new Uri("/Telerik.Windows.Themes.Office2013;component/Themes/Telerik.Windows.Controls.RibbonView.xaml", UriKind.Relative)) as ResourceDictionary);

            //Office2013Palette.Palette.FontSizeXXS = 8;
            //Office2013Palette.Palette.FontSizeXS = 10;
            //Office2013Palette.Palette.FontSizeS = 12;
            //Office2013Palette.Palette.FontSize = 13;
            //Office2013Palette.Palette.FontSizeL = 14;
            //Office2013Palette.Palette.FontSizeXL = 15;

            Collection<ResourceDictionary> resourceDictionaries = Application.Current.Resources.MergedDictionaries;

            ResourceDictionary resourceDictionary = LoadTelerikStyle_SystemWindows();
            resourceDictionaries.Add( resourceDictionary );

            resourceDictionary = LoadTelerikStyle_Controls();
            resourceDictionaries.Add( resourceDictionary );

	//apopov 12.1.2017	//DEBUG!!!
            //resourceDictionary = LoadTelerikStyle_ControlsData();
            //resourceDictionaries.Add( resourceDictionary );

            resourceDictionary = LoadTelerikStyle_ControlsNavigation();
            resourceDictionaries.Add( resourceDictionary );

            resourceDictionary = LoadTelerikStyle_ControlsInput();
            resourceDictionaries.Add( resourceDictionary );

            resourceDictionary = LoadTelerikStyle_ControlChart();
            resourceDictionaries.Add( resourceDictionary );

            resourceDictionary = LoadTelerikStyle_ControlGridView();
            resourceDictionaries.Add( resourceDictionary );

            resourceDictionary = LoadTelerikStyle_ControlRibbonView();
            resourceDictionaries.Add( resourceDictionary );

            LocalizationManager.Manager = new TelerikCustomLocalizationManager();
        }

        public static ResourceDictionary LoadTelerikStyle_SystemWindows()
        {
            return LoadTelerikStyle( "System.Windows" );
        }

        public static ResourceDictionary LoadTelerikStyle_Controls()
        {
            return LoadTelerikStyle( "Telerik.Windows.Controls" );
        }

        public static ResourceDictionary LoadTelerikStyle_ControlsData()
        {
            return LoadTelerikStyle( "Telerik.Windows.Controls.Data" );
        }

        public static ResourceDictionary LoadTelerikStyle_ControlsNavigation()
        {
            return LoadTelerikStyle( "Telerik.Windows.Controls.Navigation" );
        }

        public static ResourceDictionary LoadTelerikStyle_ControlsInput()
        {
            return LoadTelerikStyle( "Telerik.Windows.Controls.Input" );
        }

        public static ResourceDictionary LoadTelerikStyle_ControlChart()
        {
            return LoadTelerikStyle( "Telerik.Windows.Controls.Chart" );
        }

        public static ResourceDictionary LoadTelerikStyle_ControlGridView()
        {
            return LoadTelerikStyle( "Telerik.Windows.Controls.GridView" );
        }

        public static ResourceDictionary LoadTelerikStyle_ControlRibbonView()
        {
            return LoadTelerikStyle( "Telerik.Windows.Controls.RibbonView" );
        }

        public static ResourceDictionary LoadTelerikStyle( string telerikStyleFilenameWOPathAndExt )
        {
            Contract.Requires( !string.IsNullOrEmpty( telerikStyleFilenameWOPathAndExt ) );
            Contract.Ensures( Contract.Result<ResourceDictionary>() != null );

            ResourceDictionary resourceDictionary;
            if ( !s_styles.TryGetValue( telerikStyleFilenameWOPathAndExt, out resourceDictionary ) )
            {
                string uri = string.Join( ".", string.Join( "/", string.Join( ";", AssemblyShortName, "component" ), "Styles", "Telerik", "Themes", telerikStyleFilenameWOPathAndExt ), "xaml" );

                resourceDictionary = (ResourceDictionary)Application.LoadComponent( new Uri( uri, UriKind.Relative ) );
                s_styles.Add( telerikStyleFilenameWOPathAndExt, resourceDictionary );
            }
            return resourceDictionary;
        }

        public static string AssemblyShortName
        {
            get
            {
                Contract.Ensures( Contract.Result<string>() != null );

                if ( s_assemblyShortName == null )
                    s_assemblyShortName = Assembly.GetExecutingAssembly().GetName().Name;

                return s_assemblyShortName;
            }
        }
    }
}
