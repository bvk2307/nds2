﻿using System.Globalization;

namespace Luxoft.NDS2.Common.Excel.Excel.ExcelColumns
{
    public class IntColumn : BoundedColumn
    {
        public override object InterpretValue(object value)
        {
            double d;
            return double.TryParse(value.ToString(), out d) ? ((int)d).ToString(CultureInfo.InvariantCulture) : value;
        }
    }
}
