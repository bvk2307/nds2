﻿using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;

namespace Luxoft.NDS2.Common.Excel.Excel.ExcelColumns
{
    public class SurColumn : BoundedColumn
    {
        private readonly SurCode[] _surCodes;

        public SurColumn(SurCode[] surCodes)
        {
            _surCodes = surCodes;
        }

        public override object InterpretValue(object value)
        {
            var code = _surCodes.FirstOrDefault(c => c.Code == value as int?);
            return  code != null ? code.Description : value;
        }
    }
}
