﻿using Luxoft.NDS2.Common.Models.Interfaces.Columns;

namespace Luxoft.NDS2.Common.Excel.Excel.ExcelColumns
{
    public class BoundedColumn : IBoundedColumn
    {
        public string Key { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int Width { get; set; }
        public int RowSpan { get; set; }
        public bool IsVisible { get; set; }
        public HorizontalTextAlignment Align { get; set; }

        public virtual object InterpretValue(object value)
        {
            return value;
        }
    }
}