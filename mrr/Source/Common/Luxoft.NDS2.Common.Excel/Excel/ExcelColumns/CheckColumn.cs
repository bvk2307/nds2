﻿namespace Luxoft.NDS2.Common.Excel.Excel.ExcelColumns
{
    public class CheckColumn : BoundedColumn
    {
        public override object InterpretValue(object value)
        {
            bool b;
            return bool.TryParse(value.ToString(), out b) && b ? "Да" : "Нет";
        }
    }
}