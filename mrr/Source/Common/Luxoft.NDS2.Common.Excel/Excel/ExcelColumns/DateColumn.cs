﻿using System;

namespace Luxoft.NDS2.Common.Excel.Excel.ExcelColumns
{
    public class DateColumn : BoundedColumn
    {
        public override object InterpretValue(object value)
        {
            var dt = value as DateTime?;
            return dt.HasValue ? dt.Value.ToString("dd.MM.yyyy") : string.Empty;
        }
    }
}