﻿using System.Collections.Generic;
using Luxoft.NDS2.Common.Models.Interfaces.Columns;

namespace Luxoft.NDS2.Common.Excel.Excel.ExcelColumns
{
    internal class ColumnMapper
    {
        private readonly Dictionary<string, ITableColumn> _map = new Dictionary<string, ITableColumn>();

        public ColumnMapper(IEnumerable<ITableColumn> columns)
        {
            MapColumns(columns);
        }

        private void MapColumns(IEnumerable<ITableColumn> columns)
        {
            foreach (var column in columns)
            {
                _map[column.Key] = column;
                var group = column as IGroupColumn;
                if (group != null)
                {
                    MapColumns(group.Columns);
                }
            }
        }

        public ITableColumn GetColumn(string key)
        {
            ITableColumn column;
            return _map.TryGetValue(key, out column) ? column : null;
        }
    }
}