﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;
using Luxoft.NDS2.Common.Contracts.CustomAttributes;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Models.Interfaces.Columns;
using Luxoft.NDS2.Common.Excel.Excel.ExcelColumns;
using Luxoft.NDS2.Common.Models.Interfaces.Columns;

namespace Luxoft.NDS2.Common.Excel.Excel
{
    public class ExpressionHelper<TModel, TField>
    {
        /// <summary> //apopov 4.10.2016	//suggestion to consider usage of 'FLS.Common.Lib.Attributes.DisplayAttributesReader{TModel, TField}' instead of <see cref="ExpressionHelper{TModel, TField}"/>. </summary>
        /// <param name="lambda"></param>
        public ExpressionHelper(Expression<Func<TModel, TField>> lambda)
        {
            var body = (MemberExpression) lambda.Body;
            Name = body.Member.Name;
            var attrs = body.Member.GetCustomAttributes(false);
            foreach (var attr in attrs)
            {
                if (attr.GetType().IsAssignableFrom(typeof (DisplayNameAttribute)))
                {
                    Title = ((DisplayNameAttribute) attr).DisplayName;
                    continue;
                }

                if (attr.GetType().IsAssignableFrom(typeof (DescriptionAttribute)))
                {
                    Description = ((DescriptionAttribute) attr).Description;
                    continue;
                }

                if (attr.GetType().IsAssignableFrom(typeof (CurrencyFormatAttribute)))
                {
                    Format = ((FormatAttribute) attr).Format;
                }
            }
        }

        public string Name { get; private set; }
        public string Title { get; private set; }
        public string Description { get; private set; }
        public string Format { get; private set; }
    }

    public class ColumnFactory<TModel> : IColumnFactory<TModel>
    {
        public ITableColumn CreateCheckColumn<TField>(Expression<Func<TModel, TField>> key, int rowSpan = 1)
        {
            var helper = new ExpressionHelper<TModel, TField>(key);
            return new CheckColumn
            {
                Key = helper.Name,
                Title = helper.Title,
                Description = helper.Description,
                RowSpan = rowSpan,
                Align = HorizontalTextAlignment.Center,
                Width = 30
            };
        }

        public ITableColumn CreateBoolColumn<TField>(Expression<Func<TModel, TField>> key, int rowSpan = 1)
        {
            var helper = new ExpressionHelper<TModel, TField>(key);
            return new CheckColumn
            {
                Key = helper.Name,
                Title = helper.Title,
                Description = helper.Description,
                RowSpan = rowSpan,
                Align = HorizontalTextAlignment.Center,
                Width = 120
            };
        }

        public ITableColumn CreateTextColumn<TField>(Expression<Func<TModel, TField>> key, int rowSpan = 1)
        {
            var helper = new ExpressionHelper<TModel, TField>(key);
            return new BoundedColumn
            {
                Key = helper.Name,
                Title = helper.Title,
                Description = helper.Description,
                RowSpan = rowSpan,
                Align = HorizontalTextAlignment.Left,
                Width = 120
            };
        }

        public ITableColumn CreateNumberColumn<TField>(Expression<Func<TModel, TField>> key, int rowSpan = 1)
        {
            var helper = new ExpressionHelper<TModel, TField>(key);
            return new BoundedColumn
            {
                Key = helper.Name,
                Title = helper.Title,
                Description = helper.Description,
                RowSpan = rowSpan,
                Align = HorizontalTextAlignment.Right,
                Width = 100
            };
        }

        public ITableColumn CreateMoneyColumn<TField>(Expression<Func<TModel, TField>> key, int rowSpan = 1)
        {
            var helper = new ExpressionHelper<TModel, TField>(key);
            return new BoundedColumn
            {
                Key = helper.Name,
                Title = helper.Title,
                Description = helper.Description,
                RowSpan = rowSpan,
                Align = HorizontalTextAlignment.Right,
                Width = 100
            };
        }

        public ITableColumn CreateDateColumn<TField>(Expression<Func<TModel, TField>> key, int rowSpan = 1)
        {
            var helper = new ExpressionHelper<TModel, TField>(key);
            return new DateColumn
            {
                Key = helper.Name,
                Title = helper.Title,
                Description = helper.Description,
                RowSpan = rowSpan,
                Align = HorizontalTextAlignment.Left,
                Width = 80
            };
        }

        public ITableColumn CreateIntColumn<TField>(Expression<Func<TModel, TField>> key, int rowSpan = 1)
        {
            var helper = new ExpressionHelper<TModel, TField>(key);
            return new IntColumn
            {
                Key = helper.Name,
                Title = helper.Title,
                Description = helper.Description,
                RowSpan = rowSpan,
                Align = HorizontalTextAlignment.Right,
                Width = 100
            };
        }

        public ITableColumn CreateSurColumn<TField>(Expression<Func<TModel, TField>> key, SurCode[] surCodes, int rowSpan = 1)
        {
            var helper = new ExpressionHelper<TModel, TField>(key);
            return new SurColumn(surCodes)
            {
                Key = helper.Name,
                Title = helper.Title,
                Description = helper.Description,
                RowSpan = rowSpan,
                Align = HorizontalTextAlignment.Left,
                Width = 100
            };
        }
        
        public IGroupColumn CreateGroup(string key, string title, string description, int rowSpan = 1)
        {
            return new GroupColumn
            {
                Key = key,
                Title = title,
                Description = description,
                RowSpan = rowSpan
            };
        }
    }
}