﻿using System;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Excel.Excel.ExcelColumns;
using Luxoft.NDS2.Common.Excel.ExcelExport;
using Luxoft.NDS2.Common.Excel.ExcelExport.Implementation.ExcelColumns;
using Luxoft.NDS2.Common.Models.Interfaces.Columns;

namespace Luxoft.NDS2.Common.Excel.Excel
{
    public static class ExcelColumnsConverter
    {
        private static readonly Dictionary<HorizontalTextAlignment, CellHorizontalAlignment> _alignMap =
            new Dictionary<HorizontalTextAlignment, CellHorizontalAlignment>
            {
                {HorizontalTextAlignment.Left, CellHorizontalAlignment.Left},
                {HorizontalTextAlignment.Center, CellHorizontalAlignment.Center},
                {HorizontalTextAlignment.Right, CellHorizontalAlignment.Right}
            };

        private static uint ConvertWidth(int width)
        {
            return (uint) width/5;
        }

        private static readonly Dictionary<HorizontalTextAlignment, CellHorizontalAlignment> AlignMap =
            new Dictionary<HorizontalTextAlignment, CellHorizontalAlignment>
        {
            {HorizontalTextAlignment.Left, CellHorizontalAlignment.Left},
            {HorizontalTextAlignment.Center, CellHorizontalAlignment.Center},
            {HorizontalTextAlignment.Right, CellHorizontalAlignment.Right}
        };

        private static CellHorizontalAlignment ConvertAlignment(HorizontalTextAlignment align)
        {
            CellHorizontalAlignment result;
            return AlignMap.TryGetValue(align, out result) ? result : CellHorizontalAlignment.None;
        }

        public static List<IExcelColumn> ConvertToExcelColumns(this IEnumerable<ITableColumn> source)
        {
            var list = new List<IExcelColumn>();
            foreach (var column in source)
            {
                var bc = column as BoundedColumn;
                var interpret = bc != null ? bc.InterpretValue : (Func<object, object>) null;
                var ec = new ExcelColumn(
                    column.Key,
                    ConvertWidth(column.Width),
                    column.Title,
                    (uint) column.RowSpan,
                    ConvertAlignment(column.Align),
                    CellVerticalAlignment.Center,
                    interpret
                    );
                var group = column as IGroupColumn;
                if (group != null)
                {
                    foreach (var child in group.Columns.ConvertToExcelColumns())
                        ec.AddChild(child);
                }
                list.Add(ec);
            }
            return list;
        }
    }
}