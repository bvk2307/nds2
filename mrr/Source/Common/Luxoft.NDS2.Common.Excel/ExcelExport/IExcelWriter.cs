﻿using Luxoft.NDS2.Common.Excel.ExcelExport.Implementation;
using System.Collections.Generic;

namespace Luxoft.NDS2.Common.Excel.ExcelExport
{
    public delegate void ExportExcelChangeRowsDelegate(ExportRowsParameters exportRowsParams);

    public interface IExcelWriter
    {
        void Write<T>(T dataObject);
        void WriteAll<T>(IEnumerable<T> dataObject);
        void WriteHeader();
        void Close();
        event ExportExcelChangeRowsDelegate ExportedRowsQuantityChanged;
    }
}
