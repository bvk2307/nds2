﻿using Luxoft.NDS2.Common.Excel.ExcelExport.Implementation.ExcelColumns;
using System.Collections.Generic;

namespace Luxoft.NDS2.Common.Excel.ExcelExport
{
    public interface IExcelColumn
    {
        string DataPropertyName { get; }
        string HeaderText { get; }
        uint Width { get; }
        uint RowSpan { get; }
        CellHorizontalAlignment CellHorizontalAlignment { get; }
        CellVerticalAlignment CellVerticalAlignment { get; }
        IEnumerable<IExcelColumn> Childs { get; }
        void WriteHeaderCell(IHeaderCell cell);
        void WriteDataCell(IDataCell cell);
        object InterpretValue(object value);
    }
}
