﻿using Luxoft.NDS2.Common.Excel.ExcelExport.Implementation.ExcelColumns;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Luxoft.NDS2.Common.Excel.ExcelExport
{
    public interface IHeaderCell : IDataCell
    {
        uint Width { get; }
        string CellReference { get; }
        CellHorizontalAlignment HorizontalAlignment { get; }
        CellVerticalAlignment VerticalAlignment { get; }
    }
}
