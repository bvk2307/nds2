﻿using Luxoft.NDS2.Common.Excel.ExcelExport.Implementation.ColumnHelpers;
using Luxoft.NDS2.Common.Excel.ExcelExport.Implementation.ExcelColumns;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Luxoft.NDS2.Common.Excel.ExcelExport.Implementation
{
    public class ExcelFileWriter : IExcelWriter, IDisposable
    {
        #region Переменные

        private IEnumerable<IExcelColumn> _columns;
        private string _filePath;
        private ExcelAdapter _excelAdapter;
        private List<MergeParameter> _mergeParameters;
        private Dictionary<int, SheetParameter> _sheets;
        private int _numberCurrentSheet = 0;
        private bool _isWriteHeader = false;
        private string _sheetNamePattern = String.Empty;
        private long _totalRowCount = 0;
        private const long SHEET_SIZE = 1000000;

        #endregion

        public ExcelFileWriter(string filePath, IEnumerable<IExcelColumn> columns, string pattern, long totalRowCount = 1)
        {
            _filePath = filePath;
            _columns = columns;
            _sheetNamePattern = pattern;
            _totalRowCount = totalRowCount;

            SetupExcelColumn(_columns);
            var columnsWidth = GetEndPointColumn(_columns).Select(itemColumn => itemColumn.Width).Select(dummy => (double)dummy).ToList();

            _excelAdapter = new ExcelAdapter(columnsWidth, pattern);
            _mergeParameters = new List<MergeParameter>();
            _sheets = new Dictionary<int, SheetParameter>();

            _excelAdapter.ExportedRowsQuantityChanged += ExcelWriter_ExportedRowsQuantityChanged;

            CreateExcel();
        }

        #region Реализация интерфейсов

        public void Write<T>(T dataObject)
        {
            var listDataObject = new List<T>();
            listDataObject.Add(dataObject);
            WriteAll(listDataObject);

            if (dataObject.GetType() == typeof(string))
            {
                var mergeParam = new MergeParameter();
                mergeParam.IsNeedMerging = true;
                mergeParam.IsMerge = false;
                mergeParam.BeginSheetIndex = _excelAdapter.RowState.SheetRowIndex;
                mergeParam.NameSheet = _sheets[_numberCurrentSheet].NameSheet;
                mergeParam.NumberSheet = _numberCurrentSheet;
                mergeParam.TypeMerge = TypeMerge.OneRow;
                _mergeParameters.Add(mergeParam);
            }
        }

        public void WriteAll<T>(IEnumerable<T> dataObject)
        {
            var endPointColumns = GetEndPointColumn(_columns);

            var data = ConvertToDictionary(dataObject);

            long sheetMaxCount = SHEET_SIZE;
            long canCountOnSheet = 0;

            _excelAdapter.Open(_filePath, _numberCurrentSheet);
            _excelAdapter.RowState.SheetRowIndex = _sheets[_numberCurrentSheet].LastSheetIndex;
            while (data.Count() > 0)
            {
                canCountOnSheet = sheetMaxCount - _excelAdapter.RowState.SheetRowIndex;

                if (canCountOnSheet <= 0)
                {
                    _excelAdapter.Close();
                    _numberCurrentSheet++;
                    AddSheet();
                    _excelAdapter.Open(_filePath, _numberCurrentSheet);
                    _excelAdapter.RowState.SheetRowIndex = _sheets[_numberCurrentSheet].LastSheetIndex;
                    canCountOnSheet = sheetMaxCount - _excelAdapter.RowState.SheetRowIndex;
                }

                if (canCountOnSheet < int.MaxValue)
                {
                    int takeCount = (int)canCountOnSheet;
                    var dataCanBeOnSheet = data.Take(takeCount).ToList();
                    data = data.Skip(takeCount).ToList();

                    _excelAdapter.WriteRowList(dataCanBeOnSheet, endPointColumns);
                    _sheets[_numberCurrentSheet].LastSheetIndex = _excelAdapter.RowState.SheetRowIndex;
                }
            }
        }

        private void AddSheet()
        {
            int numberSheet = _sheets.Count();

            var sheetParam = new SheetParameter();
            sheetParam.NameSheet = string.Format(_sheetNamePattern, 1);
            sheetParam.NumberSheet = numberSheet + 1;
            _sheets.Add(numberSheet, sheetParam);

            _sheets[numberSheet].BeginSheetIndex = 0;
            _sheets[numberSheet].IsHeaderWritten = false;
            _sheets[numberSheet].LastSheetIndex = _sheets[numberSheet].BeginSheetIndex;
            _sheets[numberSheet].HeaderData = new List<Dictionary<string, ColumnData>>();
        }

        public void WriteHeader()
        {
            _isWriteHeader = true;

            Close();
            for (int numberSheet = _numberCurrentSheet - 1; ++numberSheet < _sheets.Count; )
            {
                if (numberSheet == _numberCurrentSheet)
                    _sheets[numberSheet].BeginSheetIndex = _excelAdapter.RowState.SheetRowIndex;
                else
                    _sheets[numberSheet].BeginSheetIndex = 0;
                _sheets[numberSheet].IsHeaderWritten = false;
                _sheets[numberSheet].LastSheetIndex = _sheets[numberSheet].BeginSheetIndex;
                _sheets[numberSheet].HeaderData = CreateHeader();

                var mergeParam = _mergeParameters.FirstOrDefault(p => p.NumberSheet == numberSheet);
                if (mergeParam != null)
                {
                    mergeParam.BeginSheetIndex = _sheets[numberSheet].BeginSheetIndex;
                    mergeParam.IsNeedMerging = true;
                }
            }

            WriteHeaderOnAllSheet();
            _excelAdapter.Close();
            MergeCells();
        }

        private void WriteHeaderOnAllSheet()
        {
            _excelAdapter.Open(_filePath, _numberCurrentSheet);
            for (int numberSheet = _numberCurrentSheet - 1; ++numberSheet < _sheets.Count; )
            {
                if (_isWriteHeader)
                {
                    _excelAdapter.Close();
                    _excelAdapter.RowState.SheetRowIndex = _sheets[numberSheet].BeginSheetIndex;
                    _excelAdapter.Open(_filePath, numberSheet);
                    WriteHeader(numberSheet);
                    _sheets[numberSheet].LastSheetIndex = _excelAdapter.RowState.SheetRowIndex;
                }
            }
            Close();
            _excelAdapter.Open(_filePath, _numberCurrentSheet);
            _excelAdapter.RowState.SheetRowIndex = _sheets[_numberCurrentSheet].LastSheetIndex;
        }

        private void WriteHeader(int numberSheet)
        {
            foreach (var itemHeaderRow in _sheets[numberSheet].HeaderData)
            {
                _excelAdapter.WriteStartRow();
                foreach (var itemHeaderElem in itemHeaderRow)
                {
                    ColumnData columnData = itemHeaderElem.Value;
                    columnData.ExcelColumn.WriteHeaderCell(columnData.HeaderCell);
                }
                _excelAdapter.WriteEndRow();
            }
            _sheets[numberSheet].IsHeaderWritten = true;
        }

        public void Close()
        {
            _excelAdapter.Close();
        }

        public void Dispose()
        {
            Close();
        }

        public event ExportExcelChangeRowsDelegate ExportedRowsQuantityChanged;

        #endregion

        #region Публичные Методы

        public void WriteStartRow()
        {
            _excelAdapter.WriteStartRow();
        }

        public void WriteEndRow()
        {
            _excelAdapter.WriteEndRow();
        }

        public void WriteHeaderCell(object value, CellHorizontalAlignment сellHorizontalAlignment,
            CellVerticalAlignment cellVerticalAlignment, string excelColumnName)
        {
            _excelAdapter.WriteHeaderCell(value, сellHorizontalAlignment, cellVerticalAlignment, excelColumnName);
        }

        public void WriteDataCell(object value, string excelColumnName)
        {
            _excelAdapter.WriteDataCell(value, excelColumnName);
        }

        #endregion

        #region Вспомогательные методы

        private void SetupExcelColumn(IEnumerable<IExcelColumn> columns)
        {
            foreach (ExcelColumn itemColumn in columns)
            {
                itemColumn.ExcelFileWriter = this;
                SetupExcelColumn(itemColumn.Childs);
            }
        }

        private void CreateExcel()
        {
            long sheetCount = CalculateSheetCount();

            _sheets.Clear();
            var sheetParam = new SheetParameter();
            sheetParam.NameSheet = string.Format(_sheetNamePattern, 1);
            sheetParam.NumberSheet = 1;
            _sheets.Add(0, sheetParam);

            _mergeParameters.Clear();
            var mergeParam = new MergeParameter();
            mergeParam.NameSheet = string.Format(_sheetNamePattern, 1);
            mergeParam.IsNeedMerging = false;
            mergeParam.IsMerge = false;
            mergeParam.TypeMerge = TypeMerge.Header;
            _mergeParameters.Add(mergeParam);
            for (int i = 0; ++i < sheetCount; )
            {
                mergeParam = new MergeParameter();
                mergeParam.IsNeedMerging = false;
                mergeParam.IsMerge = false;
                mergeParam.TypeMerge = TypeMerge.Header;
                mergeParam.NameSheet = string.Format(_sheetNamePattern, i + 1);
                mergeParam.NumberSheet = i;
                _mergeParameters.Add(mergeParam);

                sheetParam = new SheetParameter();
                sheetParam.NameSheet = string.Format(_sheetNamePattern, i + 1);
                sheetParam.NumberSheet = i;
                _sheets.Add(i, sheetParam);
            }

            //List<string> sheetNames = _sheets.Select(p => p.Value.NameSheet).ToList();

            _excelAdapter.CreateExcel(_filePath, sheetCount);
        }

        private long CalculateSheetCount()
        {
            long sheetSize = SHEET_SIZE;
            long sheetCount = _totalRowCount / sheetSize;
            long totalRowCountCalculate = sheetCount * sheetSize;
            if (totalRowCountCalculate < _totalRowCount)
                sheetCount++;

            var headerData = CreateHeader();
            long headerRowCount = headerData.Count();
            long totalRowCountHeaderCalculate = sheetCount * headerRowCount;

            sheetCount = (_totalRowCount + totalRowCountHeaderCalculate) / sheetSize;
            totalRowCountCalculate = sheetCount * sheetSize;
            if (totalRowCountCalculate < (_totalRowCount + totalRowCountHeaderCalculate))
                sheetCount++;

            return sheetCount;
        }

        private List<Dictionary<string, object>> ConvertToDictionary<T>(IEnumerable<T> entities)
        {
            List<Dictionary<string, object>> data = null;
            if (typeof(T) == typeof(string))
            {
                data = ConvertToDictionaryForString(entities.Select(p=>p.ToString()));
            }
            else
                data = ConvertToDictionaryBase(entities);
            return data;
        }

        private List<Dictionary<string, object>> ConvertToDictionaryBase<T>(IEnumerable<T> entities)
        {
            List<Dictionary<string, object>> rowsDict = new List<Dictionary<string, object>>();
            foreach (object itemTest in entities)
            {
                Dictionary<string, object> row = new Dictionary<string, object>();
                Type typeClass = itemTest.GetType();
                PropertyInfo[] properties = typeClass.GetProperties();
                foreach (PropertyInfo itemProp in properties)
                {
                    string name = itemProp.Name;
                    object value = itemProp.GetValue(itemTest, null);
                    row.Add(name, value);
                }
                rowsDict.Add(row);
            }
            return rowsDict;
        }

        private List<Dictionary<string, object>> ConvertToDictionaryForString(IEnumerable<string> entities)
        {
            var endPointColumns = GetEndPointColumn(_columns);

            var data = new List<Dictionary<string, object>>();
            var columnFirst = endPointColumns.FirstOrDefault();
            if (columnFirst != null)
            {
                foreach (var item in entities)
                {
                    var dictValues = new Dictionary<string, object>();
                    dictValues.Add(columnFirst.DataPropertyName, item);
                    data.Add(dictValues);
                }
            }
            return data;
        }

        private void GenerateExportedRowsQuantityChanged(ExportRowsParameters exportRowsParams)
        {
            if (ExportedRowsQuantityChanged != null)
            {
                ExportedRowsQuantityChanged(exportRowsParams);
            }
        }

        private void ExcelWriter_ExportedRowsQuantityChanged(long totalRowCount, long headerRowCount)
        {
            var exportRowsParams = new ExportRowsParameters();
            exportRowsParams.TotalRowsCount = _totalRowCount;
            exportRowsParams.ExportedRowsQuantity = totalRowCount - headerRowCount;

            GenerateExportedRowsQuantityChanged(exportRowsParams);
        }

        #endregion

        #region Мердж колонок

        private void MergeCells()
        {
            var cellMerging = new List<CellMerge>();
            long rowCurrentNumber = 0;
            foreach (var item in _mergeParameters)
            {
                if (item.IsNeedMerging &&
                    !item.IsMerge)
                {
                    if (item.TypeMerge == TypeMerge.Header)
                    {
                        rowCurrentNumber = item.BeginSheetIndex;
                        MergeCellOfColumns(item.NumberSheet, rowCurrentNumber, 0, _columns, cellMerging);
                        item.IsMerge = true;

                    }
                    else if (item.TypeMerge == TypeMerge.OneRow)
                    {
                        MergeCellOfTitle(_numberCurrentSheet, item, cellMerging);
                        item.IsMerge = true;
                    }
                }
            }
            foreach (var itemGroup in cellMerging.GroupBy(p => p.NameSheet))
            {
                _excelAdapter.MergeTwoCells(_filePath, itemGroup.Key, itemGroup.ToList());
            }
        }

        private void MergeCellOfTitle(int numberSheet, MergeParameter mergeParam, List<CellMerge> cellMerging)
        {
            string mergeBeginCell = string.Format("{0}{1}", "A", mergeParam.BeginSheetIndex);
            string mergeEndCell = string.Format("{0}{1}", "P", mergeParam.BeginSheetIndex);
            cellMerging.Add(new CellMerge() 
            { 
                NameSheet = mergeParam.NameSheet, CellOne = mergeBeginCell, CellSecond = mergeEndCell 
            });
        }

        private void MergeCellOfColumns(int numberSheet, long rowCurrentNumber, int cellCurrentNumber, IEnumerable<IExcelColumn> columns,
            List<CellMerge> cellMerging)
        {
            var excelColumnNames = ExcelColumnHelper.GetExcelColumnNames();
            rowCurrentNumber++;
            foreach (var itemColumn in columns)
            {
                var childEndPointColumns = GetEndPointColumn(itemColumn);
                if (childEndPointColumns.Count() > 1)
                {
                    string mergeBeginCell = string.Format("{0}{1}", excelColumnNames[cellCurrentNumber], rowCurrentNumber);
                    string mergeEndCell = string.Format("{0}{1}", excelColumnNames[cellCurrentNumber + childEndPointColumns.Count() - 1], rowCurrentNumber);
                    var mergeParam = _mergeParameters.Where(p => p.NumberSheet == numberSheet && p.TypeMerge == TypeMerge.Header).FirstOrDefault();
                    if (mergeParam != null)
                        cellMerging.Add(new CellMerge()
                        {
                            NameSheet = mergeParam.NameSheet,
                            CellOne = mergeBeginCell,
                            CellSecond = mergeEndCell
                        });
                    MergeCellOfColumns(numberSheet, rowCurrentNumber, cellCurrentNumber, itemColumn.Childs, cellMerging);
                    cellCurrentNumber = cellCurrentNumber + childEndPointColumns.Count();
                }
                else
                {
                    if (itemColumn.RowSpan > 1)
                    {
                        string mergeBeginCell = string.Format("{0}{1}", excelColumnNames[cellCurrentNumber], rowCurrentNumber);
                        string mergeEndCell = string.Format("{0}{1}", excelColumnNames[cellCurrentNumber], rowCurrentNumber + itemColumn.RowSpan - 1);
                        var mergeParam = _mergeParameters.Where(p => p.NumberSheet == numberSheet && p.TypeMerge == TypeMerge.Header).FirstOrDefault();
                        if (mergeParam != null)
                            cellMerging.Add(new CellMerge()
                            {
                                NameSheet = mergeParam.NameSheet,
                                CellOne = mergeBeginCell,
                                CellSecond = mergeEndCell
                            });
                    }
                    cellCurrentNumber++;
                }
            }
        }

        #endregion

        #region Формирования заголовка

        private List<Dictionary<string, ColumnData>> CreateHeader()
        {
            var dataDictionary = new List<Dictionary<string, ColumnData>>();

            var result = new ResultColumnLevel();

            CreateResultHeader(_columns, result);

            dataDictionary.AddRange(result.GetLevelColumns());

            var columnNames = ExcelColumnHelper.GetExcelColumnNames();
            foreach (var itemDict in dataDictionary)
            {
                int indexColumnName = 0;
                foreach (var item in itemDict)
                {
                    item.Value.CellReference = columnNames[indexColumnName];
                    indexColumnName++;
                }
            }

            return dataDictionary;
        }

        private void CreateResultHeader(IEnumerable<IExcelColumn> columns, ResultColumnLevel result)
        {
            result.IncreaseCurrentLevelData();
            var columnLevelData = result.GetColumnLevelData();

            foreach (IExcelColumn itemColumn in columns)
            {
                if (itemColumn.Childs.Count() == 0)
                {
                    if (!columnLevelData.Colummns.ContainsKey(itemColumn.DataPropertyName))
                    {
                        columnLevelData.Colummns.Add(itemColumn.DataPropertyName, new ColumnData() 
                        { 
                            ExcelColumn = itemColumn, Value = itemColumn.HeaderText
                        });
                        columnLevelData.Colummns.Last().Value.CreateHeaderCell();

                        AddColumnRowSpan(itemColumn, result);
                    }
                }
                else
                {
                    columnLevelData.Colummns.Add(itemColumn.DataPropertyName, new ColumnData() { ExcelColumn = itemColumn, Value = itemColumn.HeaderText });
                    columnLevelData.Colummns.Last().Value.CreateHeaderCell();

                    AddColumnEmptyData(itemColumn.Childs, columnLevelData);
                    RemoveLastColumnEmptyData(columnLevelData);

                    AddColumnRowSpan(itemColumn, result);

                    IncreaseLevelDataWithRowSpan(itemColumn, result);
                    CreateResultHeader(itemColumn.Childs, result);
                    DecreaseLevelDataWithRowSpan(itemColumn, result);
                }
            }
            result.DecreaseCurrentLevelData();
        }

        private void AddColumnEmptyData(IEnumerable<IExcelColumn> columns, ColumnLevelData columnLevelData)
        {
            var childFirst = columns.FirstOrDefault();
            if (childFirst != null)
            {
                foreach (var itemColumnChild in columns)
                {
                    if (itemColumnChild.Childs.Count() == 0)
                    {
                        columnLevelData.Colummns.Add(itemColumnChild.DataPropertyName, new ColumnData()
                        {
                            ExcelColumn = itemColumnChild,Value = String.Empty, IsEmpty = true
                        });
                        columnLevelData.Colummns.Last().Value.CreateHeaderCell();
                    }
                    else
                    {
                        AddColumnEmptyData(itemColumnChild.Childs, columnLevelData);
                    }
                }
            }
        }

        private void RemoveLastColumnEmptyData(ColumnLevelData columnLevelData)
        {
            var columnData = columnLevelData.Colummns.LastOrDefault();
            if (columnData.Value.IsEmpty)
            {
                columnLevelData.Colummns.Remove(columnData.Key);
            }
        }

        private void AddColumnRowSpan(IExcelColumn itemColumn, ResultColumnLevel result)
        {
            for (int i = 0; ++i < itemColumn.RowSpan; )
            {
                result.IncreaseCurrentLevelData();
                var columnLevelDataNext = result.GetColumnLevelData();
                columnLevelDataNext.Colummns.Add(itemColumn.DataPropertyName, new ColumnData()
                {
                    ExcelColumn = itemColumn,
                    Value = String.Empty
                });
                columnLevelDataNext.Colummns.Last().Value.CreateHeaderCell();
            }
            for (int i = 0; ++i < itemColumn.RowSpan; )
                result.DecreaseCurrentLevelData();
        }

        private void IncreaseLevelDataWithRowSpan(IExcelColumn itemColumn, ResultColumnLevel result)
        {
            for (int i = 0; ++i < itemColumn.RowSpan; )
                result.IncreaseCurrentLevelData();
        }

        private void DecreaseLevelDataWithRowSpan(IExcelColumn itemColumn, ResultColumnLevel result)
        {
            for (int i = 0; ++i < itemColumn.RowSpan; )
                result.DecreaseCurrentLevelData();
        }

        private IEnumerable<IExcelColumn> GetEndPointColumn(IExcelColumn column)
        {
            var columns = new List<IExcelColumn>();
            columns.Add(column);
            return GetEndPointColumn(columns);
        }

        private IEnumerable<IExcelColumn> GetEndPointColumn(IEnumerable<IExcelColumn> columns)
        {
            var endedColumns = new List<IExcelColumn>();
            foreach (IExcelColumn itemColumn in columns)
            {
                if (!itemColumn.Childs.Any())
                    endedColumns.Add(itemColumn);
                else
                {
                        endedColumns.AddRange(GetEndPointColumn(itemColumn.Childs));
                }
            }
            return endedColumns;
        }

        #endregion
    }
}
