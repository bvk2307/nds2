﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Luxoft.NDS2.Common.Excel.ExcelExport.Implementation
{
    public class ExportRowsParameters
    {
        public long TotalRowsCount { get; set; }
        public long ExportedRowsQuantity { get; set; }
    }
}
