﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Luxoft.NDS2.Common.Excel.ExcelExport.Implementation.ColumnHelpers
{
    public class ColumnLevelData
    {
        public int Level { get; set; }
        public Dictionary<string, ColumnData> Colummns { get; set; }

        public ColumnLevelData()
        {
            Colummns = new Dictionary<string, ColumnData>();
        }
    }
}
