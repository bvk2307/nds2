﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Luxoft.NDS2.Common.Excel.ExcelExport.Implementation.ColumnHelpers
{
    public class ResultColumnLevel
    {
        private List<Dictionary<string, ColumnData>> HeaderLevels { get; set; }

        public List<ColumnLevelData> ColumnLevelDatas { get; set; }

        private int CurrentLevelData { get; set; }

        public ResultColumnLevel()
        {
            HeaderLevels = new List<Dictionary<string, ColumnData>>();

            ColumnLevelDatas = new List<ColumnLevelData>();
            CurrentLevelData = 0;
        }

        public ColumnLevelData GetColumnLevelData()
        {
            var columnLevelDataFind = ColumnLevelDatas.Where(p => p.Level == CurrentLevelData).SingleOrDefault();
            if (columnLevelDataFind == null)
            {
                columnLevelDataFind = new ColumnLevelData();
                columnLevelDataFind.Level = CurrentLevelData;
                ColumnLevelDatas.Add(columnLevelDataFind);
            }
            return columnLevelDataFind;
        }

        public void IncreaseCurrentLevelData()
        {
            CurrentLevelData++;
        }

        public void DecreaseCurrentLevelData()
        {
            CurrentLevelData--;
        }

        public List<Dictionary<string, ColumnData>> GetLevelColumns()
        {
            HeaderLevels.Clear();

            foreach (var itemGroup in ColumnLevelDatas.OrderBy(p => p.Level).GroupBy(p => p.Level))
            {
                foreach(var itemValue in itemGroup)
                {
                    HeaderLevels.Add(itemValue.Colummns);
                }
            }

            return HeaderLevels;
        }
    }
}
