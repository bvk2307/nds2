﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Luxoft.NDS2.Common.Excel.ExcelExport.Implementation.ColumnHelpers
{
    public class ExcelColumnHelper
    {
        public static List<string> GetExcelColumnNames()
        {
            return Enumerable.Range(0, 100).Select(x => GetExcelColumnOneName(x)).ToList();
        }

        public static string GetExcelColumnOneName(int index)
        {
            if (index < 26)
            {
                return ((char)('A' + index)).ToString();
            }
            return GetExcelColumnOneName(index / 26 - 1) + GetExcelColumnOneName(index % 26);
        }
    }
}
