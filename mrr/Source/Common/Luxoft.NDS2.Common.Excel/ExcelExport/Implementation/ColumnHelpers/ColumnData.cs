﻿using Luxoft.NDS2.Common.Excel.ExcelExport.Implementation.Cells;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Luxoft.NDS2.Common.Excel.ExcelExport.Implementation.ColumnHelpers
{
    public class ColumnData
    {
        public IExcelColumn ExcelColumn { get; set; }
        public object Value { get; set; }
        public IHeaderCell HeaderCell { get; set; }
        public bool IsEmpty { get; set; }
        public string CellReference { get; set; }

        public void CreateHeaderCell()
        {
            HeaderCell = new HeaderCell(Value, ExcelColumn.Width, CellReference);
        }
    }
}
