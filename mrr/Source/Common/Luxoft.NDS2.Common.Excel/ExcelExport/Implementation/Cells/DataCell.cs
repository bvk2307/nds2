﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Luxoft.NDS2.Common.Excel.ExcelExport.Implementation.Cells
{
    public class DataCell : IDataCell
    {
        private object _value;

        public DataCell(object value)
        {
            _value = value;
        }

        public object value
        {
            get { return _value; }
        }
    }
}
