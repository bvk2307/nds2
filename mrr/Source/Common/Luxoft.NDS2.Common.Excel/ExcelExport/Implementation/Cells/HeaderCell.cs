﻿using Luxoft.NDS2.Common.Excel.ExcelExport.Implementation.ExcelColumns;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Luxoft.NDS2.Common.Excel.ExcelExport.Implementation.Cells
{
    public class HeaderCell : DataCell, IHeaderCell
    {
        private uint _width;
        private string _cellReference;

        public HeaderCell(object value, uint width, string cellReference)
            : base(value)
        {
            _width = width;
            _cellReference = cellReference;
        }

        public uint Width
        {
            get { return _width; }
        }

        public CellHorizontalAlignment HorizontalAlignment
        {
            get { throw new NotImplementedException(); }
        }

        public CellVerticalAlignment VerticalAlignment
        {
            get { throw new NotImplementedException(); }
        }

        public string CellReference
        {
            get { return _cellReference; }
        }
    }
}
