﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Luxoft.NDS2.Common.Excel.ExcelExport.Implementation.ColumnHelpers;
using Luxoft.NDS2.Common.Excel.ExcelExport.Implementation.ExcelColumns;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using X14 = DocumentFormat.OpenXml.Office2010.Excel;

namespace Luxoft.NDS2.Common.Excel.ExcelExport.Implementation
{
    public delegate void ChangeRowsCountDelegate(long totalRowCount, long headerRowCount);

    public class ExcelAdapter
    {
        #region Переменные

        private Dictionary<long, double> _rowsHeight = new Dictionary<long, double>();
        private double _rowHeightDefault = 0;
        private Dictionary<int, UInt32> sheetIds = new Dictionary<int, uint>();
        private SpreadsheetDocument _document = null;
        private OpenXmlReader reader = null;
        private OpenXmlWriter writer = null;
        private WorkbookPart workbookPart = null;
        private string replacementPartId = null;
        private string origninalSheetId = null;
        private WorksheetPart worksheetPart = null;
        private bool _isOpen = false;
        private bool _isWritingDataRow = false;
        public ExcelRowState RowState { get; set; }
        public event ChangeRowsCountDelegate ExportedRowsQuantityChanged;

        #endregion

        public ExcelAdapter(List<double> columnsWidth, string sheetNamePattern)
        {
            _columnsWidth = columnsWidth;
            _sheetNamePattern = sheetNamePattern;

            RowState = new ExcelRowState();
            RowState.AbsoluteRowIndex = 0;
            RowState.SheetRowIndex = 0;
        }

        #region Публичные методы

        public void SetupRowHeight(Dictionary<long, double> rowsHeight, double rowHeightDefault)
        {
            _rowsHeight = rowsHeight;
            _rowHeightDefault = rowHeightDefault;
        }

        private readonly List<double> _columnsWidth;
        private readonly string _sheetNamePattern;

        public void CreateExcel(string filepath, long sheetCount = 1)
        {
            using (SpreadsheetDocument document = SpreadsheetDocument.Create(filepath, SpreadsheetDocumentType.Workbook))
            {
                WorkbookPart workbookPart = document.AddWorkbookPart();
                WorksheetPart worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
                Workbook workbook = new Workbook();
                FileVersion fileVersion = new FileVersion();
                fileVersion.ApplicationName = "Microsoft Office Excel";
                Worksheet worksheet = new Worksheet();
                SheetData sheetData = new SheetData();

                WorkbookStylesPart workbookStylesPart = workbookPart.AddNewPart<WorkbookStylesPart>();
                workbookStylesPart.Stylesheet = CreateStylesheet();
                workbookStylesPart.Stylesheet.Save();

                Columns columns = new Columns();
                uint columnIndex = 1;
                foreach (double itemLength in _columnsWidth)
                {
                    columns.Append(CreateColumnData(columnIndex, columnIndex, itemLength));
                    columnIndex++;
                }
                worksheet.Append(columns);

                string sheetNameFirst = String.Empty;
                    sheetNameFirst = string.Format(_sheetNamePattern, 1);

                worksheet.Append(sheetData);
                worksheetPart.Worksheet = worksheet;
                worksheetPart.Worksheet.Save();
                Sheets sheets = new Sheets();
                Sheet sheet = new Sheet();
                sheet.SheetId = 1;
                sheet.Name = sheetNameFirst;
                SetSheetId(0, sheet.SheetId);
                sheet.Id = workbookPart.GetIdOfPart(worksheetPart);
                sheets.Append(sheet);
                workbook.Append(fileVersion);
                workbook.Append(sheets);

                document.WorkbookPart.Workbook = workbook;
                document.WorkbookPart.Workbook.Save();

                //MergeTwoCells(document, sheetNameFirst, "A1", "B1");

                for (int i = 0; ++i < sheetCount; )
                {
                    InsertWorksheet(document, i, string.Format(_sheetNamePattern, i + 1), _columnsWidth);
                }

                document.Close();
            }
        }

        public void MergeTwoCells(string filePath, string sheetName, List<CellMerge> cellMerging)
        {
            using (SpreadsheetDocument document = SpreadsheetDocument.Open(filePath, true))
            {
                MergeTwoCells(document, sheetName, cellMerging);
            }
        }

        public void MergeTwoCells(SpreadsheetDocument document, string sheetName, List<CellMerge> cellMerging)
        {
            Worksheet worksheet = GetWorksheet(document, sheetName);

            if (worksheet == null)
                return;

            foreach (var item in cellMerging)
            {
                if (string.IsNullOrEmpty(item.CellOne) ||
                    string.IsNullOrEmpty(item.CellSecond))
                    return;

                CreateSpreadsheetCellIfNotExist(worksheet, item.CellOne);
                CreateSpreadsheetCellIfNotExist(worksheet, item.CellSecond);

                MergeCells mergeCells;
                if (worksheet.Elements<MergeCells>().Count() > 0)
                {
                    mergeCells = worksheet.Elements<MergeCells>().First();
                }
                else
                {
                    mergeCells = new MergeCells();

                    if (worksheet.Elements<CustomSheetView>().Count() > 0)
                        worksheet.InsertAfter(mergeCells, worksheet.Elements<CustomSheetView>().First());
                    else if (worksheet.Elements<DataConsolidate>().Count() > 0)
                        worksheet.InsertAfter(mergeCells, worksheet.Elements<DataConsolidate>().First());
                    else if (worksheet.Elements<SortState>().Count() > 0)
                        worksheet.InsertAfter(mergeCells, worksheet.Elements<SortState>().First());
                    else if (worksheet.Elements<AutoFilter>().Count() > 0)
                        worksheet.InsertAfter(mergeCells, worksheet.Elements<AutoFilter>().First());
                    else if (worksheet.Elements<Scenarios>().Count() > 0)
                        worksheet.InsertAfter(mergeCells, worksheet.Elements<Scenarios>().First());
                    else if (worksheet.Elements<ProtectedRanges>().Count() > 0)
                        worksheet.InsertAfter(mergeCells, worksheet.Elements<ProtectedRanges>().First());
                    else if (worksheet.Elements<SheetProtection>().Count() > 0)
                        worksheet.InsertAfter(mergeCells, worksheet.Elements<SheetProtection>().First());
                    else if (worksheet.Elements<SheetCalculationProperties>().Count() > 0)
                        worksheet.InsertAfter(mergeCells, worksheet.Elements<SheetCalculationProperties>().First());
                    else
                        worksheet.InsertAfter(mergeCells, worksheet.Elements<SheetData>().First());
                }

                MergeCell mergeCell = new MergeCell() { Reference = new StringValue(item.CellOne + ":" + item.CellSecond) };
                mergeCells.Append(mergeCell);
            }

            worksheet.Save();
        }

        public bool CheckIsOpen()
        {
            return _isOpen;
        }

        public void Open(string filePath, int numSheet)
        {
            if (!CheckIsOpen())
                OpenInternal(filePath, numSheet);
        }

        private void OpenInternal(string filePath, int numSheet)
        {
            _document = SpreadsheetDocument.Open(filePath, true);

            workbookPart = _document.WorkbookPart;
            List<Sheet> sheetsAll = _document.WorkbookPart.Workbook.Descendants<Sheet>().ToList();
            Sheet sheetOne = sheetsAll.FirstOrDefault(p => p.SheetId == GetSheetId(numSheet));
            if (sheetOne == null)
            {
                sheetOne = InsertWorksheet(_document, numSheet, string.Format(_sheetNamePattern, numSheet + 1), _columnsWidth);
                _document.WorkbookPart.WorksheetParts.Last().Worksheet.Save();
            }
            worksheetPart = (WorksheetPart)workbookPart.GetPartById(sheetOne.Id);
            origninalSheetId = workbookPart.GetIdOfPart(worksheetPart);

            var replacementPart = workbookPart.AddNewPart<WorksheetPart>();
            replacementPartId = workbookPart.GetIdOfPart(replacementPart);

            reader = OpenXmlReader.Create(worksheetPart);
            writer = OpenXmlWriter.Create(replacementPart);

            while (reader.Read())
            {
                if (reader.ElementType == typeof(SheetData))
                {
                    if (reader.IsStartElement)
                    {
                    }
                    else if (reader.IsEndElement)
                    {
                        writer.WriteStartElement(new SheetData());
                        foreach (var item in worksheetPart.Worksheet)
                        {
                            if (item.GetType() == typeof(SheetData))
                            {
                                SheetData sheetData = (SheetData)item;
                                foreach (var itemChild in sheetData)
                                {
                                    object itemChildClone = itemChild.Clone();
                                    writer.WriteElement((OpenXmlElement)itemChildClone);
                                }
                            }
                            else
                            {
                                if (item.GetType() == typeof(Columns))
                                {
                                    object itemClone = item.Clone();
                                    writer.WriteElement((OpenXmlElement)itemClone);
                                }
                            }
                        }
                        break;
                    }
                }
                else
                {
                    if (reader.IsStartElement)
                    {
                        writer.WriteStartElement(reader);
                    }
                    else if (reader.IsEndElement)
                    {
                        writer.WriteEndElement();
                    }
                }
            }

            _isOpen = true;
        }

        public void WriteStartRow()
        {
            Row row = new Row();
            row.RowIndex = new UInt32Value((uint)(RowState.SheetRowIndex + 1));
            writer.WriteStartElement(row);
            RowState.AbsoluteRowIndex++;
            RowState.SheetRowIndex++;
            RowState.HeaderRowIndex++;
            GenerateExportedRowsQuantityChanged(RowState.AbsoluteRowIndex, RowState.HeaderRowIndex);
        }

        public void WriteEndRow()
        {
            writer.WriteEndElement();
        }

        public void WriteHeaderCell(object entityValue, CellHorizontalAlignment сellHorizontalAlignment,
            CellVerticalAlignment cellVerticalAlignment, string excelColumnName)
        {
            if (entityValue == null)
                entityValue = string.Empty;
            Type typeValue = entityValue.GetType();
            Cell cell = CreateHeaderCell(typeValue, сellHorizontalAlignment, cellVerticalAlignment);
            WriteCell(cell, entityValue, excelColumnName);
            _isWritingDataRow = true;
        }

        public void WriteDataCell(object entityValue, string excelColumnName)
        {
            if (entityValue == null)
                entityValue = string.Empty;

            Type typeValue = entityValue.GetType();
            Cell cell = CreateContentCell(typeValue);
            WriteCell(cell, entityValue, excelColumnName);
        }

        private void WriteCell(Cell cell, object entityValue, string excelColumnName)
        {
            Type typeValue = entityValue.GetType();
            if (IsNumberType(typeValue))
            {
                cell.DataType = CellValues.Number;
                cell.CellValue = new CellValue(GenerateExcelNumberValue(entityValue, typeValue));
            }
            else
            {
                cell.DataType = CellValues.InlineString;
                InlineString inlineString = new InlineString();
                Text text = new Text();
                text.Text = entityValue.ToString();
                inlineString.AppendChild(text);
                cell.AppendChild(inlineString);
            }
            writer.WriteElement(cell);
        }

        public void WriteRowList(List<Dictionary<string, object>> dataRows, IEnumerable<IExcelColumn> columns)
        {
            Dictionary<string, object> rowData;
            object entityValue = null;
            Row excelRow = new Row();

            for (int rowIndex = 0; rowIndex < dataRows.Count; rowIndex++)
            {
                rowData = dataRows[rowIndex];
                SetRowHeight(excelRow, RowState.AbsoluteRowIndex);

                excelRow.RowIndex = new UInt32Value((uint)(RowState.SheetRowIndex + 1));
                writer.WriteStartElement(excelRow);

                _isWritingDataRow = true;

                var columnNames = ExcelColumnHelper.GetExcelColumnNames();
                int columnNameIndex = 0;
                foreach (IExcelColumn itemColumn in columns)
                {
                    entityValue = String.Empty;
                    if (rowData.ContainsKey(itemColumn.DataPropertyName))
                        entityValue = itemColumn.InterpretValue(rowData[itemColumn.DataPropertyName]);

                    WriteDataCell(entityValue, columnNames[columnNameIndex]);
                    columnNameIndex++;
                }
                writer.WriteEndElement();

                RowState.AbsoluteRowIndex++;
                RowState.SheetRowIndex++;
                GenerateExportedRowsQuantityChanged(RowState.AbsoluteRowIndex, RowState.HeaderRowIndex);
            }
        }

        public void Close()
        {
            if (_isOpen)
            {
                CloseInternal();
            }
        }

        private void CloseInternal()
        {
            if (_isWritingDataRow)
            {
                _isWritingDataRow = false;
                WriteEndElement();
            }

            while (reader.Read())
            {
                if (reader.ElementType == typeof(SheetData))
                {
                    if (reader.IsEndElement)
                        continue;
                }
                else
                {
                    if (reader.IsStartElement)
                    {
                        writer.WriteStartElement(reader);
                    }
                    else if (reader.IsEndElement)
                    {
                        writer.WriteEndElement();
                    }
                }
            }

            reader.Close();
            writer.Close();

            Sheet sheet = workbookPart.Workbook.Descendants<Sheet>().Where(s => s.Id.Value.Equals(origninalSheetId)).First();
            sheet.Id.Value = replacementPartId;
            workbookPart.DeletePart(worksheetPart);

            _document.Close();
            _document.Dispose();

            _isOpen = false;
        }

        private void GenerateExportedRowsQuantityChanged(long totalRowCount, long headerRowCount)
        {
            if (ExportedRowsQuantityChanged != null)
            {
                ExportedRowsQuantityChanged(totalRowCount, headerRowCount);
            }
        }

        #endregion

        #region Вспомогательные методы

        private Sheet InsertWorksheet(SpreadsheetDocument spreadSheet, int numSheet, string sheetName, List<double> columnsWidth)
        {
            WorksheetPart newWorksheetPart = spreadSheet.WorkbookPart.AddNewPart<WorksheetPart>();
            newWorksheetPart.Worksheet = new Worksheet(new SheetData());

            Sheets sheets = spreadSheet.WorkbookPart.Workbook.GetFirstChild<Sheets>();
            string relationshipId = spreadSheet.WorkbookPart.GetIdOfPart(newWorksheetPart);

            uint sheetId = 1;
            if (sheets.Elements<Sheet>().Count() > 0)
            {
                sheetId = sheets.Elements<Sheet>().Select(s => s.SheetId.Value).Max() + 1;
            }

            Sheet sheet = new Sheet() { Id = relationshipId, SheetId = sheetId, Name = sheetName };
            SetSheetId(numSheet, sheet.SheetId);
            sheets.Append(sheet);

            //-------------- создание колонок во этой вкладке
            Worksheet workSheetNew = ((WorksheetPart)spreadSheet.WorkbookPart.GetPartById(sheet.Id)).Worksheet;
            Columns columns = workSheetNew.Elements<Columns>().FirstOrDefault();
            if ((columns == null))
            {
                SheetData sheetData = workSheetNew.Elements<SheetData>().FirstOrDefault();
                if ((sheetData != null))
                {
                    columns = workSheetNew.InsertBefore(new Columns(), sheetData);
                }
                else
                {
                    columns = new Columns();
                    workSheetNew.Append(columns);
                }
            }
            uint columnIndex = 1;
            foreach (double itemLength in columnsWidth)
            {
                columns.Append(CreateColumnData(columnIndex, columnIndex, itemLength));
                columnIndex++;
            }
            return sheet;
        }

        private Stylesheet CreateStylesheet()
        {
            Stylesheet stylesheet1 = new Stylesheet() { MCAttributes = new MarkupCompatibilityAttributes() { Ignorable = "x14ac" } };
            stylesheet1.AddNamespaceDeclaration("mc", "http://schemas.openxmlformats.org/markup-compatibility/2006");
            stylesheet1.AddNamespaceDeclaration("x14ac", "http://schemas.microsoft.com/office/spreadsheetml/2009/9/ac");

            Fonts fonts1 = new Fonts() { Count = (UInt32Value)2U, KnownFonts = true };

            DocumentFormat.OpenXml.Spreadsheet.Font font1 = new DocumentFormat.OpenXml.Spreadsheet.Font();
            FontSize fontSize1 = new FontSize() { Val = 11D };
            DocumentFormat.OpenXml.Spreadsheet.Color colorFont1 = new DocumentFormat.OpenXml.Spreadsheet.Color() { Theme = (UInt32Value)1U };
            FontName fontName1 = new FontName() { Val = "Calibri" };
            FontFamilyNumbering fontFamilyNumbering1 = new FontFamilyNumbering() { Val = 2 };
            FontScheme fontScheme1 = new FontScheme() { Val = FontSchemeValues.Minor };

            font1.Append(fontSize1);
            font1.Append(colorFont1);
            font1.Append(fontName1);
            font1.Append(fontFamilyNumbering1);
            font1.Append(fontScheme1);

            fonts1.Append(font1);

            //======================================
            DocumentFormat.OpenXml.Spreadsheet.Font font2 = new DocumentFormat.OpenXml.Spreadsheet.Font();
            Bold fontBold2 = new Bold();
            FontSize fontSize2 = new FontSize() { Val = 11D };
            DocumentFormat.OpenXml.Spreadsheet.Color colorFont2 = new DocumentFormat.OpenXml.Spreadsheet.Color() { Theme = (UInt32Value)1U };
            FontName fontName2 = new FontName() { Val = "Calibri" };
            FontFamilyNumbering fontFamilyNumbering2 = new FontFamilyNumbering() { Val = 2 };
            FontScheme fontScheme2 = new FontScheme() { Val = FontSchemeValues.Minor };

            font2.Append(fontSize2);
            font2.Append(fontBold2);
            font2.Append(colorFont2);
            font2.Append(fontName2);
            font2.Append(fontFamilyNumbering2);
            font2.Append(fontScheme2);

            fonts1.Append(font2);
            //======================================

            Fills fills1 = new Fills() { Count = (UInt32Value)5U };

            // FillId = 0
            Fill fill1 = new Fill();
            PatternFill patternFill1 = new PatternFill() { PatternType = PatternValues.None };
            fill1.Append(patternFill1);

            // FillId = 1
            Fill fill2 = new Fill();
            PatternFill patternFill2 = new PatternFill() { PatternType = PatternValues.Gray125 };
            fill2.Append(patternFill2);

            // FillId = 2,RED
            Fill fill3 = new Fill();
            PatternFill patternFill3 = new PatternFill() { PatternType = PatternValues.Solid };
            ForegroundColor foregroundColor1 = new ForegroundColor() { Rgb = "FFFF0000" };
            BackgroundColor backgroundColor1 = new BackgroundColor() { Indexed = (UInt32Value)64U };
            patternFill3.Append(foregroundColor1);
            patternFill3.Append(backgroundColor1);
            fill3.Append(patternFill3);

            // FillId = 3, Very Light Gray
            Fill fill4 = new Fill();
            PatternFill patternFill4 = new PatternFill() { PatternType = PatternValues.Solid };
            ForegroundColor foregroundColor2 = new ForegroundColor() { Rgb = "FFDCDCDC" };
            BackgroundColor backgroundColor2 = new BackgroundColor() { Indexed = (UInt32Value)64U };
            patternFill4.Append(foregroundColor2);
            patternFill4.Append(backgroundColor2);
            fill4.Append(patternFill4);

            // FillId = 4,Light Gray
            Fill fill5 = new Fill();
            PatternFill patternFill5 = new PatternFill() { PatternType = PatternValues.Solid };
            ForegroundColor foregroundColor3 = new ForegroundColor() { Rgb = "FFC0C0C0" };
            BackgroundColor backgroundColor3 = new BackgroundColor() { Indexed = (UInt32Value)64U };
            patternFill5.Append(foregroundColor3);
            patternFill5.Append(backgroundColor3);
            fill5.Append(patternFill5);

            fills1.Append(fill1);
            fills1.Append(fill2);
            fills1.Append(fill3);
            fills1.Append(fill4);
            fills1.Append(fill5);

            Borders borders1 = new Borders() { Count = (UInt32Value)2U };

            Border border1 = new Border();
            LeftBorder leftBorder1 = new LeftBorder();
            RightBorder rightBorder1 = new RightBorder();
            TopBorder topBorder1 = new TopBorder();
            BottomBorder bottomBorder1 = new BottomBorder();
            DiagonalBorder diagonalBorder1 = new DiagonalBorder();

            border1.Append(leftBorder1);
            border1.Append(rightBorder1);
            border1.Append(topBorder1);
            border1.Append(bottomBorder1);
            border1.Append(diagonalBorder1);

            borders1.Append(border1);

            //=====================================
            Border border2 = new Border();
            LeftBorder leftBorder2 = new LeftBorder() { Style = BorderStyleValues.Thin };
            DocumentFormat.OpenXml.Spreadsheet.Color colorBor1 = new DocumentFormat.OpenXml.Spreadsheet.Color() { Rgb = "FF000000", Indexed = (UInt32Value)64U };
            leftBorder2.Append(colorBor1);

            RightBorder rightBorder2 = new RightBorder() { Style = BorderStyleValues.Thin };
            DocumentFormat.OpenXml.Spreadsheet.Color color2 = new DocumentFormat.OpenXml.Spreadsheet.Color() { Rgb = "FF000000", Indexed = (UInt32Value)64U };
            rightBorder2.Append(color2);

            TopBorder topBorder2 = new TopBorder() { Style = BorderStyleValues.Thin };
            DocumentFormat.OpenXml.Spreadsheet.Color color3 = new DocumentFormat.OpenXml.Spreadsheet.Color() { Rgb = "FF000000", Indexed = (UInt32Value)64U };
            topBorder2.Append(color3);

            BottomBorder bottomBorder2 = new BottomBorder() { Style = BorderStyleValues.Thin };
            DocumentFormat.OpenXml.Spreadsheet.Color color4 = new DocumentFormat.OpenXml.Spreadsheet.Color() { Rgb = "FF000000", Indexed = (UInt32Value)64U };
            bottomBorder2.Append(color4);
            DiagonalBorder diagonalBorder2 = new DiagonalBorder();

            border2.Append(leftBorder2);
            border2.Append(rightBorder2);
            border2.Append(topBorder2);
            border2.Append(bottomBorder2);
            border2.Append(diagonalBorder2);
            borders1.Append(border2);
            //=====================================

            CellStyleFormats cellStyleFormats1 = new CellStyleFormats() { Count = (UInt32Value)1U };
            CellFormat cellFormat1 = new CellFormat() { NumberFormatId = (UInt32Value)0U, FontId = (UInt32Value)0U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U };

            cellStyleFormats1.Append(cellFormat1);

            CellFormats cellFormats1 = new CellFormats() { Count = (UInt32Value)4U };
            CellFormat cellFormat2_1 = new CellFormat() { NumberFormatId = (UInt32Value)0U, FontId = (UInt32Value)0U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U };
            CellFormat cellFormat3 = new CellFormat() { NumberFormatId = (UInt32Value)0U, FontId = (UInt32Value)1U, FillId = (UInt32Value)3U, BorderId = (UInt32Value)1U, FormatId = (UInt32Value)0U, ApplyFill = true };
            CellFormat cellFormat4 = new CellFormat() { NumberFormatId = (UInt32Value)0U, FontId = (UInt32Value)1U, FillId = (UInt32Value)3U, BorderId = (UInt32Value)1U, FormatId = (UInt32Value)0U, ApplyFill = true };
            CellFormat cellFormat5 = new CellFormat() { NumberFormatId = (UInt32Value)0U, FontId = (UInt32Value)1U, FillId = (UInt32Value)3U, BorderId = (UInt32Value)1U, FormatId = (UInt32Value)0U, ApplyFill = true };
            CellFormat cellFormat6 = new CellFormat() { NumberFormatId = (UInt32Value)0U, FontId = (UInt32Value)1U, FillId = (UInt32Value)3U, BorderId = (UInt32Value)1U, FormatId = (UInt32Value)0U, ApplyFill = true };
            CellFormat cellFormat2_2 = new CellFormat() { NumberFormatId = (UInt32Value)4U, FontId = (UInt32Value)0U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U };

            //=================================================
            CellformatSetup(cellFormat2_1, false, false);
            CellformatSetup(cellFormat3, false, true);
            CellformatSetup(cellFormat4, false, false);
            CellformatSetup(cellFormat5, true, false);
            CellformatSetup(cellFormat6, true, true);
            CellformatSetup(cellFormat2_2, false, false);
            //=================================================

            cellFormats1.Append(cellFormat2_1);
            cellFormats1.Append(cellFormat3);
            cellFormats1.Append(cellFormat4);
            cellFormats1.Append(cellFormat5);
            cellFormats1.Append(cellFormat6);
            cellFormats1.Append(cellFormat2_2);


            CellStyles cellStyles1 = new CellStyles() { Count = (UInt32Value)1U };
            CellStyle cellStyle1 = new CellStyle() { Name = "Normal", FormatId = (UInt32Value)0U, BuiltinId = (UInt32Value)0U };

            cellStyles1.Append(cellStyle1);
            DifferentialFormats differentialFormats1 = new DifferentialFormats() { Count = (UInt32Value)0U };
            TableStyles tableStyles1 = new TableStyles() { Count = (UInt32Value)0U, DefaultTableStyle = "TableStyleMedium2", DefaultPivotStyle = "PivotStyleMedium9" };

            StylesheetExtensionList stylesheetExtensionList1 = new StylesheetExtensionList();

            StylesheetExtension stylesheetExtension1 = new StylesheetExtension() { Uri = "{EB79DEF2-80B8-43e5-95BD-54CBDDF9020C}" };
            stylesheetExtension1.AddNamespaceDeclaration("x14", "http://schemas.microsoft.com/office/spreadsheetml/2009/9/main");
            X14.SlicerStyles slicerStyles1 = new X14.SlicerStyles() { DefaultSlicerStyle = "SlicerStyleLight1" };

            stylesheetExtension1.Append(slicerStyles1);

            stylesheetExtensionList1.Append(stylesheetExtension1);

            stylesheet1.Append(fonts1);
            stylesheet1.Append(fills1);
            stylesheet1.Append(borders1);
            stylesheet1.Append(cellStyleFormats1);
            stylesheet1.Append(cellFormats1);
            stylesheet1.Append(cellStyles1);
            stylesheet1.Append(differentialFormats1);
            stylesheet1.Append(tableStyles1);
            stylesheet1.Append(stylesheetExtensionList1);
            return stylesheet1;
        }

        private void CellformatSetup(CellFormat cellFormat, bool IsHorizontalAlignmentCenter, bool IsVerticalAlignmentCenter)
        {
            cellFormat.ApplyAlignment = true;
            if (cellFormat.Alignment == null)
                cellFormat.Alignment = new Alignment() { WrapText = true };
            Alignment alignment = cellFormat.Alignment;
            if (alignment.WrapText == null || alignment.WrapText.Value == false)
                alignment.WrapText = new BooleanValue(true);
            if (IsHorizontalAlignmentCenter)
            {
                alignment.Horizontal = HorizontalAlignmentValues.Center;
            }
            if (IsVerticalAlignmentCenter)
            {
                alignment.Vertical = VerticalAlignmentValues.Center;
            }
        }

        private Column CreateColumnData(UInt32 StartColumnIndex, UInt32 EndColumnIndex, double ColumnWidth)
        {
            Column column;
            column = new Column();
            column.Min = StartColumnIndex;
            column.Max = EndColumnIndex;
            column.Width = ColumnWidth;
            column.CustomWidth = true;
            return column;
        }

        private string GenerateExcelNumberValue(object value, Type typeValue)
        {
            string strValue = value.ToString();
            if (typeValue == typeof(decimal) || typeValue == typeof(decimal?))
            {
                strValue = (decimal.Round((decimal)value, 2)).ToString();
            }
            else if (typeValue == typeof(double) || typeValue == typeof(double?))
            {
                strValue = (Math.Round((double)value, 2)).ToString();
            }
            strValue = strValue.Replace(",", ".");
            return strValue;
        }

        private void SetRowHeight(Row row, long rowAbsolute)
        {
            if (_rowsHeight.ContainsKey(rowAbsolute))
            {
                row.CustomHeight = true;
                row.Height = _rowsHeight[rowAbsolute];
            }
            else if (_rowHeightDefault > 0)
            {
                row.CustomHeight = true;
                row.Height = _rowHeightDefault;
            }
            else
            {
                row.CustomHeight = false;
            }
        }

        private Cell CreateContentCell(Type valueDataType)
        {
            Cell cell = new Cell();
            if (IsNumberType(valueDataType))
            {
                cell = new Cell() { StyleIndex = (UInt32Value)5U };
            }
            else
            {
                cell = new Cell() { StyleIndex = (UInt32Value)0U };
            }
            return cell;
        }

        private Cell CreateHeaderCell(Type valueDataType, CellHorizontalAlignment сellHorizontalAlignment,
            CellVerticalAlignment cellVerticalAlignment)
        {
            Cell cell = new Cell();
            if (сellHorizontalAlignment == CellHorizontalAlignment.None &&
                cellVerticalAlignment == CellVerticalAlignment.None)
            {
                cell = new Cell() { StyleIndex = (UInt32Value)2U };
            }
            else
            {
                if (сellHorizontalAlignment == CellHorizontalAlignment.Center)
                {
                    if (cellVerticalAlignment == CellVerticalAlignment.Center)
                    {
                        cell = new Cell() { StyleIndex = (UInt32Value)4U };
                    }
                    else
                    {
                        cell = new Cell() { StyleIndex = (UInt32Value)3U };
                    }
                }
                else
                {
                    if (cellVerticalAlignment == CellVerticalAlignment.Center)
                    {
                        cell = new Cell() { StyleIndex = (UInt32Value)1U };
                    }
                    else
                    {
                        cell = new Cell() { StyleIndex = (UInt32Value)2U };
                    }
                }
            }
            return cell;
        }

        private Worksheet GetWorksheet(SpreadsheetDocument document, string worksheetName)
        {
            IEnumerable<Sheet> sheets = document.WorkbookPart.Workbook
                .Descendants<Sheet>().Where(s => s.Name == worksheetName);
            WorksheetPart worksheetPart = (WorksheetPart)document.WorkbookPart
                .GetPartById(sheets.First().Id);
            return worksheetPart.Worksheet;
        }

        private void CreateSpreadsheetCellIfNotExist(Worksheet worksheet, string cellName)
        {
            string columnName = GetColumnName(cellName);
            uint rowIndex = GetRowIndex(cellName);

            IEnumerable<Row> rows = worksheet.Descendants<Row>().Where(r => r.RowIndex != null && r.RowIndex.Value == rowIndex);

            if (rows.Count() == 0)
            {
                Row row = new Row() { RowIndex = new UInt32Value(rowIndex) };
                Cell cell = new Cell() { CellReference = new StringValue(cellName) };
                row.Append(cell);
                worksheet.Descendants<SheetData>().First().Append(row);
                worksheet.Save();
            }
            else
            {
                Row row = rows.First();
                IEnumerable<Cell> allCells = row.Elements<Cell>();
                var columnNames = ExcelColumnHelper.GetExcelColumnNames();
                string cellNameCurrent = String.Empty;
                int columnNameIndex = 0;
                List<Cell> cells = new List<Cell>();
                foreach (var itemCell in allCells)
                {
                    cellNameCurrent = string.Format("{0}{1}", columnNames[columnNameIndex], row.RowIndex);
                    if (cellNameCurrent == cellName)
                    {
                        cells.Add(itemCell);
                    }
                    columnNameIndex++;
                }

                //IEnumerable<Cell> cells = row.Elements<Cell>().Where(c => c.CellReference != null && c.CellReference.Value == cellName);
                if (cells.Count() == 0)
                {
                    Cell cell = new Cell() { CellReference = new StringValue(cellName) };
                    row.Append(cell);
                    worksheet.Save();
                }
            }
        }

        private uint GetRowIndex(string cellName)
        {
            Regex regex = new Regex(@"\d+");
            Match match = regex.Match(cellName);

            return uint.Parse(match.Value);
        }

        private string GetColumnName(string cellName)
        {
            Regex regex = new Regex("[A-Za-z]+");
            Match match = regex.Match(cellName);
            return match.Value;
        }

        private void SetSheetId(int num, UInt32 sheetId)
        {
            if (sheetIds.ContainsKey(num))
            {
                sheetIds[num] = sheetId;
            }
            else
            {
                sheetIds.Add(num, sheetId);
            }
        }

        private UInt32 GetSheetId(int num)
        {
            UInt32 sheetId = 0;
            if (sheetIds.ContainsKey(num))
            {
                sheetId = sheetIds[num];
            }
            return sheetId;
        }

        private bool IsNumberType(object value)
        {
            bool ret = false;
            if (value != null)
            {
                ret = IsNumberType(value.GetType());
            }
            return ret;
        }

        private bool IsNumberType(Type type)
        {
            bool ret = false;
            if (type == typeof(decimal) || type == typeof(double) || type == typeof(long) || type == typeof(int) ||
                type == typeof(decimal?) || type == typeof(double?) || type == typeof(long?) || type == typeof(int?))
            {
                ret = true;
            }
            return ret;
        }

        private void WriteEndElement()
        {
            writer.WriteEndElement();
        }

        #endregion
    }
}
