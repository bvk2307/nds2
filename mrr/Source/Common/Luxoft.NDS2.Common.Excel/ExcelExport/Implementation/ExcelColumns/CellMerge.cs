﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Excel.ExcelExport.Implementation.ExcelColumns
{
    public class CellMerge
    {
        public string NameSheet { get; set; }
        public string CellOne { get; set; }
        public string CellSecond { get; set; }
    }
}
