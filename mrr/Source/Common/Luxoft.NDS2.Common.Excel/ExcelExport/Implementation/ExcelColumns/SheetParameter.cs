﻿using Luxoft.NDS2.Common.Excel.ExcelExport.Implementation.ColumnHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Excel.ExcelExport.Implementation.ExcelColumns
{
    public class SheetParameter
    {
        public string NameSheet { get; set; }
        public int NumberSheet { get; set; }

        public bool IsHeaderWritten { get; set; }
        public long BeginSheetIndex { get; set; }
        public long LastSheetIndex { get; set; }
        public List<Dictionary<string, ColumnData>> HeaderData { get; set; }
    }
}
