﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Luxoft.NDS2.Common.Excel.ExcelExport.Implementation.ExcelColumns
{
    public class MergeParameter
    {
        public bool IsMerge { get; set; }
        public long BeginSheetIndex { get; set; }
        public bool IsNeedMerging { get; set; }
        public TypeMerge TypeMerge { get; set; }
        public string NameSheet { get; set; }
        public int NumberSheet { get; set; }

        public MergeParameter()
        {
        }
    }

    public enum TypeMerge
    {
        Header = 1,
        OneRow = 2
    }
}
