﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Luxoft.NDS2.Common.Excel.ExcelExport.Implementation.ExcelColumns
{
    public enum CellVerticalAlignment
    {
        None = 0,
        Top = 1,
        Bottom = 2,
        Center = 3
    }
}
