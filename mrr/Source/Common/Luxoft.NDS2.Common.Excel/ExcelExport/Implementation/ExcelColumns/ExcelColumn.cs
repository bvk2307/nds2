﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Luxoft.NDS2.Common.Excel.ExcelExport.Implementation.ExcelColumns
{
    public class ExcelColumn : IExcelColumn
    {
        private string _dataPropertyName;
        private uint _width;
        private string _headerText;
        private List<IExcelColumn> _childs = new List<IExcelColumn>();
        private ExcelFileWriter _excelFileWriter = null;
        private uint _rowSpan;
        private CellHorizontalAlignment _cellHorizontalAlignment = CellHorizontalAlignment.None;
        private CellVerticalAlignment _cellVerticalAlignment = CellVerticalAlignment.None;
        private readonly Func<object, object> _interpretValue;

        public ExcelColumn(string dataPropertyName, uint width, string headerText,
            uint rowSpan = 1, CellHorizontalAlignment cellHorizontalAlignment = CellHorizontalAlignment.None,
            CellVerticalAlignment cellVerticalAlignment = CellVerticalAlignment.None, Func<object, object> interpretValue = null)
        {
            _dataPropertyName = dataPropertyName;
            _width = width;
            _headerText = headerText;
            _rowSpan = rowSpan;
            _cellHorizontalAlignment = cellHorizontalAlignment;
            _cellVerticalAlignment = cellVerticalAlignment;
            _interpretValue = interpretValue;
        }

        public ExcelFileWriter ExcelFileWriter
        {
            get
            {
                return _excelFileWriter;
            }
            set
            {
                _excelFileWriter = value;
            }
        }

        public void AddChild(IExcelColumn child)
        {
            _childs.Add(child);
        }

        public string DataPropertyName
        {
            get { return _dataPropertyName; }
        }

        public uint Width
        {
            get { return _width; }
        }

        public string HeaderText
        {
            get { return _headerText; }
        }

        public IEnumerable<IExcelColumn> Childs
        {
            get { return _childs; }
        }

        public void WriteHeaderCell(IHeaderCell cell)
        {
            _excelFileWriter.WriteHeaderCell(cell.value, CellHorizontalAlignment.Center, _cellVerticalAlignment, cell.CellReference);
        }

        public void WriteDataCell(IDataCell cell)
        {
            var value = _interpretValue != null ? _interpretValue(cell.value) : cell.value;
            _excelFileWriter.WriteDataCell(value, String.Empty);
        }

        public object InterpretValue(object value)
        {
            return _interpretValue != null ? _interpretValue(value) : value;
        }

        public uint RowSpan
        {
            get { return _rowSpan; }
        }

        public CellHorizontalAlignment CellHorizontalAlignment
        {
            get { return _cellHorizontalAlignment; }
        }

        public CellVerticalAlignment CellVerticalAlignment
        {
            get { return _cellVerticalAlignment; }
        }
    }
}
