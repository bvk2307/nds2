﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Luxoft.NDS2.Common.Excel.ExcelExport.Implementation.ExcelColumns
{
    public enum CellHorizontalAlignment
    {
        None = 0,
        Left = 1,
        Right = 2,
        Center = 3
    }
}
