﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Luxoft.NDS2.Common.Excel.ExcelExport.Implementation
{
    public class ExcelRowState
    {
        public long SheetRowIndex { get; set; }
        public long AbsoluteRowIndex { get; set; }
        public long HeaderRowIndex { get; set; }
    }
}
