﻿using System.Collections.Generic;
using Luxoft.NDS2.Common.Excel.ExcelPattern;

namespace Luxoft.NDS2.Common.Excel.ReportExport
{
    public interface IExporterDataContainer
    {
        int Datarownumber { get; }
        object Items { get; }
        IList<FieldDefinition> Conformity { get; set; }
        IDictionary<string, object> NameValues { get; set; }
        bool IsVisible { get; set; }
    }
}
