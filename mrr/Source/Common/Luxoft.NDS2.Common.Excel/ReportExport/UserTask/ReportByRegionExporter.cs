﻿using System;
using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Common.Models.UserTask;
using CommonComponents.Catalog;
using Luxoft.NDS2.Common.Excel.ExcelPattern;

namespace Luxoft.NDS2.Common.Excel.ReportExport.UserTask
{
    /// <summary>Класс для выгрузки в Excel отчета 'ПЗ ЦА по региону'"</summary>
    /// <remarks>Шаблон NDS2-260-09-1-2-(Форма выгрузки-ЦА-по региону).xslx</remarks>
    public sealed class ReportByRegionExporter : CustomExporter, IReportExporter<CompletedByRegion, InProgressByRegion, UnassignedByRegion>
    {
        private static readonly string Patternname;

        public int Worksheetcount { get; private set; }

        static ReportByRegionExporter()
        {
            Patternname = "Bin/Templates/NDS22600912CAregion.xlsx";
        }

        private readonly ICatalogService _catalogService;

        /// <summary>Конструктор класса для выгрузки отчета 'NDS2-260-09-1-2-(Форма выгрузки-ЦА-по региону)'"</summary>
        /// <param name="datestart">Дата начала отчета</param>
        /// <seealso cref="DateTime"></seealso>
        /// <param name="dateend">Дата окончания отчета</param>
        /// <seealso cref="DateTime"></seealso>
        /// <param name="catalogService">Сервис доступа к каталогу для чтения Excel-файла паттерна отчета</param>
        /// <seealso cref="ICatalogService"></seealso>
        public ReportByRegionExporter(DateTime datestart, DateTime dateend, ICatalogService catalogService)
        {
            _catalogService = catalogService;
            Datestart = datestart;
            Dateend = dateend;
            Datacontainer = new List<IExporterDataContainer>(Worksheetcount)
            {
                new ExporterDataContainer<CompletedByRegion>() { Datarownumber = 5 }
               ,new ExporterDataContainer<InProgressByRegion>() { Datarownumber = 4 }
               ,new ExporterDataContainer<UnassignedByRegion>() { Datarownumber = 3 }
            };
        }

        /// <summary>Первый лист - Выполнено</summary>
        /// <param name="items">Коллекция объектов с данными</param>
        /// <seealso cref="IList{CompletedByRegion}"></seealso>
        /// <param name="hiddenColumns">Список колонок, которые необходимо скрыть, null если ничего скрывать не надо</param>
        /// <seealso cref="IList{Strign}"></seealso>
        /// <exception cref="ArgumentNullException"></exception>
        public void AddWorksheetData(IList<CompletedByRegion> items, IList<string> hiddenColumns = null)
        {
            if (items == null)
                throw new ArgumentNullException("items");

            Datacontainer[0].IsVisible = true;
            ((ExporterDataContainer<CompletedByRegion>)Datacontainer[0]).Items = items;
            Datacontainer[0].Conformity =
                new List<FieldDefinition>
                {
                    Helper<CompletedByRegion>.Create(x => x.RegionName, hiddenColumns),
                    Helper<CompletedSummaryModel>.Create(x => x.TaskTypeName, hiddenColumns),
                    Helper<CompletedSummaryModel>.Create(x => x.OnSchedule, hiddenColumns),
                    Helper<CompletedSummaryModel>.Create(x => x.Overdue, hiddenColumns),
                    Helper<CompletedSummaryModel>.Create(x => x.Total, hiddenColumns)
                };
            Datacontainer[0].NameValues = new Dictionary<string, object>()
            {
                { "DatePeriod", string.Format("Период формирования: {0:d} - {1:d}", Datestart, Dateend) }
               ,{ "OnShedule", items.Sum(n => n.OnSchedule) }
               ,{ "Overdue", items.Sum(n => n.Overdue) }
               ,{ "Total", items.Sum(n => n.Total) }
            };

            Worksheetcount++;
        }

        /// <summary>Второй лист - На исполнении</summary>
        /// <param name="items">Коллекция объектов с данными</param>
        /// <seealso cref="IList{InProgressByRegion}"></seealso>
        /// <param name="hiddenColumns">Список колонок, которые необходимо скрыть, null если ничего скрывать не надо</param>
        /// <seealso cref="IList{Strign}"></seealso>
        /// <exception cref="ArgumentNullException"></exception>
        public void AddWorksheetData(IList<InProgressByRegion> items, IList<string> hiddenColumns = null)
        {
            if (items == null)
                throw new ArgumentNullException("items");

            Datacontainer[1].IsVisible = true;
            ((ExporterDataContainer<InProgressByRegion>)Datacontainer[1]).Items = items;
            Datacontainer[1].Conformity =
                new List<FieldDefinition>
                {
                    Helper<InProgressByRegion>.Create(x => x.RegionName, hiddenColumns),
                    Helper<InProgressSummaryModel>.Create(x => x.TaskTypeName, hiddenColumns),
                    Helper<InProgressSummaryModel>.Create(x => x.OnSchedulePeriodStart, hiddenColumns),
                    Helper<InProgressSummaryModel>.Create(x => x.OnSchedulePeriodEnd, hiddenColumns),
                    Helper<InProgressSummaryModel>.Create(x => x.OverdueStart, hiddenColumns),
                    Helper<InProgressSummaryModel>.Create(x => x.OverdueEnd, hiddenColumns),
                    Helper<InProgressSummaryModel>.Create(x => x.TotalStart, hiddenColumns),
                    Helper<InProgressSummaryModel>.Create(x => x.TotalEnd, hiddenColumns)
                };
            Datacontainer[1].NameValues = new Dictionary<string, object>()
            {
                {"OnSchedulePeriodStart", items.Sum(n => n.OnSchedulePeriodStart)},
                {"OnSchedulePeriodEnd", items.Sum(n => n.OnSchedulePeriodEnd)},
                {"OverdueStart", items.Sum(n => n.OverdueStart)},
                {"OverdueEnd", items.Sum(n => n.OverdueEnd)},
                {"TotalStart", items.Sum(n => n.TotalStart)},
                {"TotalEnd", items.Sum(n => n.TotalEnd)},

                {"OnSheduleTimeStart", GetDateFormat(Datestart)},
                {"OnSheduleTimeEnd", GetDateFormat(Dateend)},
                {"OverdueTimeStart", GetDateFormat(Datestart)},
                {"OverdueTimeEnd", GetDateFormat(Dateend)},
                {"TotalTimeStart", GetDateFormat(Datestart)},
                {"TotalTimeEnd", GetDateFormat(Dateend)}
            };

            Worksheetcount++;
        }

        /// <summary>Третий лист - Не назначено</summary>
        /// <param name="items">Коллекция объектов с данными</param>
        /// <seealso cref="IList{UnAssignedByRegion}"></seealso>
        /// <param name="hiddenColumns">Список колонок, которые необходимо скрыть, null если ничего скрывать не надо</param>
        /// <seealso cref="IList{Strign}"></seealso>
        /// <exception cref="ArgumentNullException"></exception>
        public void AddWorksheetData(IList<UnassignedByRegion> items, IList<string> hiddenColumns = null)
        {
            if (items == null)
                throw new ArgumentNullException("items");

            Datacontainer[2].IsVisible = true;
            ((ExporterDataContainer<UnassignedByRegion>)Datacontainer[2]).Items = items;
            Datacontainer[2].Conformity =
                new List<FieldDefinition>
                {
                    Helper<UnassignedByRegion>.Create(x => x.RegionName, hiddenColumns),
                    Helper<UnassignedSummaryModel>.Create(x => x.TaskTypeName, hiddenColumns),
                    Helper<UnassignedSummaryModel>.Create(x => x.UnassignedStart, hiddenColumns),
                    Helper<UnassignedSummaryModel>.Create(x => x.UnassignedEnd, hiddenColumns)
                };
            Datacontainer[2].NameValues = new Dictionary<string, object>()
            {
                {"UnAssignedStart", items.Sum(n => n.UnassignedStart)},
                {"UnAssignedEnd", items.Sum(n => n.UnassignedEnd)},
                {"UnAssignedTimeStart", GetDateFormat(Datestart)},
                {"UnAssignedTimeEnd", GetDateFormat(Dateend)}
            };

            Worksheetcount++;
        }

        /// <summary>Выгрузка в Excel</summary>
        /// <param name="filename">Путь и имя файла отчета, который будет создан этим классом</param>
        /// <seealso cref="String"></seealso>
        public void Export(string filename)
        {
            ExportExcel(filename, Patternname, _catalogService
           , (dc, exporter) =>
           {
               if (dc.Items != null)
                   exporter.Export(((ExporterDataContainer<CompletedByRegion>)dc).Items, dc.Conformity, dc.Datarownumber, 1);
               if (dc.NameValues != null)
                   exporter.PushNameValues(dc.NameValues, 1);
           }
           , (dc, exporter) =>
           {
               if (dc.Items != null)
                   exporter.Export(((ExporterDataContainer<InProgressByRegion>)dc).Items, dc.Conformity, dc.Datarownumber, 2);
               if (dc.NameValues != null)
                   exporter.PushNameValues(dc.NameValues, 2);
           }
           , (dc, exporter) =>
           {
               if (dc.Items != null)
                   exporter.Export(((ExporterDataContainer<UnassignedByRegion>)dc).Items, dc.Conformity, dc.Datarownumber, 3);
               if (dc.NameValues != null)
                   exporter.PushNameValues(dc.NameValues, 3);
           });
        }
    }
}
