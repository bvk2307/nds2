﻿using System.Collections.Generic;
using Luxoft.NDS2.Common.Models.UserTask;

namespace Luxoft.NDS2.Common.Excel.ReportExport.UserTask
{
    public interface IReportExporter<TCompleted, TInProgress, TUnassigned>
        where TCompleted : CompletedSummaryModel
        where TInProgress : InProgressSummaryModel
        where TUnassigned : UnassignedSummaryModel
    {
        void Export(string filename);

        void AddWorksheetData(IList<TCompleted> items, IList<string> hiddenColumns);

        void AddWorksheetData(IList<TInProgress> items, IList<string> hiddenColumns);

        void AddWorksheetData(IList<TUnassigned> items, IList<string> hiddenColumns);
    }
}
