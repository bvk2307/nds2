﻿using System.Collections.Generic;
using System.Reflection;
using Luxoft.NDS2.Common.Excel.ExcelPattern;

namespace Luxoft.NDS2.Common.Excel.ReportExport
{
    public sealed class ExporterDataContainer<T> : IExporterDataContainer 
        where T : class
    {
        public int Datarownumber { get; set; }

        public IList<T> Items { get; set; }

        public IList<FieldDefinition> Conformity { get; set; }

        public IDictionary<string, object> NameValues { get; set; }

        object IExporterDataContainer.Items
        {
            get { return Items; }
        }

        public bool IsVisible { get; set; }

        public static IList<FieldDefinition> GetDefaultConformity(IList<string> hidecolumns)
        {
            var list = new List<FieldDefinition>();
            var propItems = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            for (var i = 0; i < propItems.Length; i++)
                list.Add(new FieldDefinition()
                {
                    Name = propItems[i].Name,
                    IsVisible = hidecolumns == null || !hidecolumns.Contains(propItems[i].Name)
                });

            return list;
        }
    }
}
