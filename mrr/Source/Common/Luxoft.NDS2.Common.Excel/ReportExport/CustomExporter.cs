﻿using System;
using System.Collections.Generic;
using System.IO;
using CommonComponents.Catalog;
using Luxoft.NDS2.Common.Excel.ExcelPattern;
using OfficeOpenXml;

namespace Luxoft.NDS2.Common.Excel.ReportExport
{
    public abstract class CustomExporter
    {
        protected List<IExporterDataContainer> Datacontainer { get; set; }
        protected DateTime Datestart;
        protected DateTime Dateend;

        protected void ExportExcel(string filename, string pattername, ICatalogService catalogService, params Action<IExporterDataContainer, IExcelExporter>[] actions)
        {
            var address = new CatalogAddress(CatalogAddressSchemas.LogicalCatalog, "RoleCatalog", "NDS2Subsystem", "FileSystems", pattername);
            using (var exporter = new ExcelExporter(filename, catalogService.CreateInstance<Stream>(address)))
                for (var i = 0; i < Datacontainer.Count; i++)
                {
                    exporter.ExcelPack.Workbook.Worksheets[i + 1].Hidden = Datacontainer[i].IsVisible
                        ? eWorkSheetHidden.Visible
                        : eWorkSheetHidden.VeryHidden;

                    if (Datacontainer[i].Items != null)
                    {
                        actions[i].Invoke(Datacontainer[i], exporter);
                    }
                }
        }

        protected static string GetDateFormat(DateTime date)
        {
            return string.Format("На {0:d}", date);
        }

    }
}
