﻿using Luxoft.NDS2.Common.Contracts.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Luxoft.NDS2.Common.Excel.ExcelPattern
{
    public class FieldDefinition
    {
        public string Name { get; set; }

        public bool IsVisible { get; set; }
    }

    public static class Helper<T> where T : class
    {
        public static FieldDefinition Create<V>(Expression<Func<T, V>> action, IEnumerable<string> hiddenColumns = null)
        {
            var fieldName = TypeHelper<T>.GetMemberName(action);
            return new FieldDefinition 
            { 
                IsVisible = (hiddenColumns == null || !hiddenColumns.Contains(fieldName)), 
                Name = fieldName 
            };
        }
    }
}
