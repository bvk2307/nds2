﻿using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Common.Excel.ExcelPattern
{
    /// <summary>Interface for export data collections to Excel and push extra information in it</summary>
    public interface IExcelExporter : IDisposable
    {
        /// <summary>Export data to Excel file with use columns conformity</summary>
        /// <param name="items">The list of classes that represent the data collection</param>
        /// <seealso cref="IList{T}"></seealso>
        /// <param name="conformity">The list of FieldDefinition that represent public property T classes</param>
        /// <seealso cref="IList{FieldDefinition}"></seealso>
        /// <param name="rowdataIndex">Start row number for items</param>
        /// <seealso cref="int"></seealso>
        /// <param name="worksheetId">Excel's worksheet number</param>
        /// <seealso cref="int"></seealso>
        void Export<T>(IList<T> items, IList<FieldDefinition> conformity, int rowdataIndex, int worksheetId) 
            where T : class;

        /// <summary>Push extra information to workbook regions with names</summary>
        /// <param name="namevalues">Dictionary with extra information. The keys are names in the workbook</param>
        /// <seealso cref="IDictionary{String,Object}"></seealso>
        void PushNameValues(IDictionary<string, object> namevalues);

        /// <summary>Push extra information to worksheet regions with names</summary>
        /// <param name="namevalues">Dictionary with extra information. The keys are names in a worksheet</param>
        /// <seealso cref="IDictionary{String,Object}"></seealso>
        /// <param name="worksheetId">Excel's worksheet number</param>
        /// <seealso cref="int"></seealso>
        void PushNameValues(IDictionary<string, object> namevalues, int worksheetId);

        /// <summary>Excel interface access point</summary>
        object ExcelPack { get; }
    }
}
