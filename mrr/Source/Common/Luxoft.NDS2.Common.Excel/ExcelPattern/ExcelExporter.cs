﻿using System;
using System.Collections.Generic;
using OfficeOpenXml;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Luxoft.NDS2.Common.Excel.ExcelPattern
{
    /// <summary>Helper class for export to Excel (OOXML file) with use template file</summary>
    /// <remarks>Wrap over EPPlus</remarks>
    public sealed class ExcelExporter : IExcelExporter
    {
        #region Private variables

        private readonly string _filename;
        private readonly Stream _pattern;
        private bool _disposed;
        private Lazy<ExcelPackage> _lazy;
        private readonly string PatternIsNull = @"Pattern's stream is null";

        #endregion

        // ReSharper disable once MemberCanBePrivate.Global
        public ExcelPackage ExcelPack
        {
            get
            {
                if (_lazy == null)
                    _lazy = _pattern == null
                        ? new Lazy<ExcelPackage>(() => new ExcelPackage(new FileInfo(_filename)))
                        : new Lazy<ExcelPackage>(() => new ExcelPackage(File.Create(_filename), _pattern));
                return _lazy.Value;
            }
        }

        object IExcelExporter.ExcelPack
        {
            get { return ExcelPack; }
        }

        public ExcelExporter(string fileName, Stream pattern)
        {
            _filename = fileName;
            _pattern = pattern;
        }
        public ExcelExporter(string fileName)
        {
            _filename = fileName;
        }

        #region IExcelExporter

        /// <summary>Export data to Excel file with use columns conformity</summary>
        /// <param name="items">The list of classes that represent the data collection</param>
        /// <seealso cref="IList{T}"></seealso>
        /// <param name="conformity">The list of FieldDefinition that represent public property T classes</param>
        /// <seealso cref="IList{FieldDefinition}"></seealso>
        /// <param name="rowdataIndex">Start row number for items</param>
        /// <seealso cref="int"></seealso>
        /// <param name="worksheetId">Excel's worksheet number</param>
        /// <seealso cref="int"></seealso>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        public void Export<T>(IList<T> items, IList<FieldDefinition> conformity, int rowdataIndex, int worksheetId) 
            where T : class
        {
            if (items == null)
                throw new ArgumentNullException("items");
            if (conformity == null)
                throw new ArgumentNullException("conformity");
            if (conformity.Count == 0)
                throw new ArgumentOutOfRangeException("conformity");
            if ((worksheetId < 1) || (worksheetId > ExcelPack.Workbook.Worksheets.Count))
                throw new ArgumentOutOfRangeException("worksheetId");

            var worksheet = ExcelPack.Workbook.Worksheets[worksheetId];
            worksheet.InsertRow(rowdataIndex + 1, items.Count - 1, rowdataIndex);
            var rowdataHeight = worksheet.Row(rowdataIndex).Height;

            for (var i = rowdataIndex + 1; i < items.Count + rowdataIndex; i++)
                worksheet.Row(i).Height = rowdataHeight;

            var data = PrepareData<T>(items, conformity.Select<FieldDefinition, string>(n => n.Name).ToList());

            var top = rowdataIndex;
            var height = data.GetLength(0);
            var width = data.GetLength(1);
            var bottom = top + height - 1;
            var right = width;

            worksheet.Cells[top, 1, bottom, right].Value = data;

            for (var i = 1; i <= conformity.Count; i++)
                if (!conformity[i-1].IsVisible)
                    worksheet.Column(i).Hidden = true;
        }

        /// <summary>Push extra information to workbook regions with names</summary>
        /// <param name="namevalues">Dictionary with extra information. The keys are names in the workbook</param>
        /// <seealso cref="IDictionary{String,Object}"></seealso>
        /// <exception cref="InvalidOperationException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        public void PushNameValues(IDictionary<string, object> namevalues)
        {
            if (_pattern == null)
                throw new InvalidOperationException(PatternIsNull);
            if (namevalues == null)
                throw new ArgumentNullException("namevalues");
            if (namevalues.Count == 0) return;

            foreach (var item in namevalues.Where(n => ExcelPack.Workbook.Names.ContainsKey(n.Key)))
                ExcelPack.Workbook.Names[item.Key].Value = namevalues[item.Key];
        }

        /// <summary>Push extra information to worksheet regions with names</summary>
        /// <param name="namevalues">Dictionary with extra information. The keys are names in a worksheet</param>
        /// <seealso cref="IDictionary{String,Object}"></seealso>
        /// <param name="worksheetId">Excel's worksheet number</param>
        /// <seealso cref="int"></seealso>
        /// <exception cref="InvalidOperationException"></exception>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        public void PushNameValues(IDictionary<string, object> namevalues, int worksheetId)
        {
            if (_pattern == null)
                throw new InvalidOperationException(PatternIsNull);
            if (namevalues == null)
                throw new ArgumentNullException("namevalues");
            if (namevalues.Count == 0) return;
            if ((worksheetId < 1) || (worksheetId > ExcelPack.Workbook.Worksheets.Count))
                throw new ArgumentOutOfRangeException("worksheetId");

            var worksheet = ExcelPack.Workbook.Worksheets[worksheetId];
            foreach (var item in namevalues.Where(n => worksheet.Names.ContainsKey(n.Key)))
                worksheet.Names[item.Key].Value = namevalues[item.Key];
        }

        #endregion

        private static object[,] PrepareData<T>(IList<T> items, IList<string> conformity) 
            where T : class
        {
            var data = new object[items.Count, conformity.Count];
            var cFields = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            var row = new object[conformity.Count];

            Parallel.For(0, conformity.Count, (columnIndex) =>
            {
                for (var rowIndex = 0; rowIndex < items.Count; rowIndex++)
                {
                    var source = items[rowIndex];

                    foreach (var t in cFields)
                    {
                        if (t.Name != conformity[columnIndex])
                            continue;
                        row[columnIndex] = t.GetValue(source, null);
                    }

                    data[rowIndex, columnIndex] = row[columnIndex];
                }
            });

            return data;
        }

        #region Dispose

        private void Close()
        {
            if ((_lazy == null) || (!_lazy.IsValueCreated)) return;
            ExcelPack.Save();
            ExcelPack.Dispose();
        }

        public void Dispose()
        {
            Dispose(true);
        }

        private void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
                Close();

            _disposed = true;
        }

        #endregion

    }
}
