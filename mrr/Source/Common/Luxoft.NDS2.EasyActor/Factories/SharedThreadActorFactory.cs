﻿#define NET40

using System;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using EasyActor.Factories;
using EasyActor.Queue;
using System.Threading;

namespace EasyActor
{
    public class SharedThreadActorFactory : ActorFactoryBase, IActorFactory, IActorCompleteLifeCycle
    {
        private readonly IAbortableTaskQueue _Queue;
        private readonly ConcurrentQueue<IAsyncDisposable> _Disposable;

        public SharedThreadActorFactory(Action<Thread> onCreated = null)
        {
#if NET40
            if ( this != null )	//apopov 3.4.2016	//TODO!!!
                throw new NotImplementedException( "Test with success SharedThreadActorFactory.GetEndTask()'s implementation before use" );
#endif

            _Queue = new MonoThreadedQueue(onCreated);
            _Disposable = new ConcurrentQueue<IAsyncDisposable>();
        }

        public override ActorFactorType Type
        {
            get { return ActorFactorType.Shared; }
        }

        public T Build<T>(T concrete) where T : class
        {
            var res = Create(concrete, _Queue);

            var disp = concrete as IAsyncDisposable;
            if (disp != null)
                _Disposable.Enqueue(disp);

            return res;
        }

        public Task<T> BuildAsync<T>(Func<T> concrete) where T : class
        {
            return _Queue.Enqueue(() => Build<T>(concrete()));
        }

        private 
#if !NET40
            async 
#endif //!NET40
            Task GetEndTask()
        {
            IAsyncDisposable actordisp = null;

            if ( actordisp == null )	//apopov 2.4.2016	//TODO!!!   //test implementation below
                throw new NotImplementedException( "SharedThreadActorFactory.GetEndTask() must been tested before using!" );

#if NET40
            if ( _Disposable.TryDequeue( out actordisp ) )
            {
                TaskScheduler scheduler = SynchronizationContext.Current == null 
                                          ? TaskScheduler.Default : TaskScheduler.FromCurrentSynchronizationContext();

                return actordisp.DisposeAsync()
                    .ContinueWith(
                        taskDispose =>
                        {
                            taskDispose.Wait(); //to rethrow an exception if it is caused

                            return GetEndTask();
                        },
                        scheduler )
                    .ContinueWith(
                        taskEnd =>
                        {
                            taskEnd.Wait(); //to rethrow an exception if it is caused
                        },
                        scheduler );
            }
            return TaskEx.CompletedTask;

#else //!NET40

            while (_Disposable.TryDequeue(out actordisp))
            {
                await actordisp.DisposeAsync();
            }

#endif //NET40
        }

        public Task Abort()
        {
            return _Queue.Abort(GetEndTask);
        }

        public Task Stop()
        {
            return _Queue.Stop(GetEndTask);
        }
    }
}
