﻿#define NET40

using System;
using System.Threading.Tasks;
using EasyActor.Queue;
using EasyActor.Factories;
using System.Threading;
using EasyActor.Helper;

namespace EasyActor
{
    public class ActorFactory : ActorFactoryBase, IActorFactory
    {
        private readonly Action<Thread> _OnCreate;

        public ActorFactory(Action<Thread> onCreate = null )
        {
            _OnCreate = onCreate;
        }

        public override ActorFactorType Type
        {
            get { return ActorFactorType.Standard; }
        }

        private T Build<T>(T concrete, MonoThreadedQueue queue) where T : class
        {
            var asyncDisposable =  concrete as IAsyncDisposable;
            return CreateIActorLifeCycle(concrete, queue, TypeHelper.IActorCompleteLifeCycleType,
                        new ActorCompleteLifeCycleInterceptor(queue,asyncDisposable),  
                        new ActorLifeCycleInterceptor(queue, asyncDisposable));
        }

        public T Build<T>(T concrete) where T : class
        {
            return Build<T>(concrete, new MonoThreadedQueue(_OnCreate));
        }

        public Task<T> BuildAsync<T>(Func<T> concrete) where T : class
        {
            var queue = new MonoThreadedQueue(_OnCreate);
            return queue.Enqueue( ()=> Build<T>(concrete(),queue) );
        }

        internal 
#if !NET40
            async 
#endif //!NET40
            Task<Tuple<T, MonoThreadedQueue>> InternalBuildAsync<T>(Func<T> concrete) where T : class
        {
            var queue = new MonoThreadedQueue(_OnCreate);

#if NET40

            return queue.Enqueue( () => Build( concrete(), queue ) )
                .ContinueWith( taskPrev => new Tuple<T, MonoThreadedQueue>( taskPrev.Result, queue ),
                               TaskContinuationOptions.ExecuteSynchronously );          

#else //!NET40

            var actor = await queue.Enqueue(() => Build<T>(concrete(), queue));
            return new Tuple<T, MonoThreadedQueue>(actor, queue);

#endif //NET40
        }
    }
}
