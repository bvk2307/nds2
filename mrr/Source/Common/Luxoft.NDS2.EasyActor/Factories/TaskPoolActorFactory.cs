﻿#define NET40

using System;
using System.Threading.Tasks;
using EasyActor.Factories;
using EasyActor.Helper;

namespace EasyActor
{
    public class TaskPoolActorFactory : ActorFactoryBase, IActorFactory
    {
        public TaskPoolActorFactory()
        {
#if NET40
            if ( this != null )	//apopov 3.4.2016	//TODO!!!
                throw new NotImplementedException( "Test with success tests Actor_Should_Return_Cancelled_Task_On_Any_Method_AfterCalling_IActorLifeCycle_Stop() and Actor_IActorLifeCycle_Stop_Should_Not_Cancel_Enqueued_Task() in TaskPoolActorFactoryTest before use" );
#endif
        }

        public override ActorFactorType Type
        {
            get { return ActorFactorType.TaskPool; }
        }

        private T Build<T>(T concrete, IStopableTaskQueue queue) where T : class
        {
            var asyncDisposable =  concrete as IAsyncDisposable;
            return CreateIActorLifeCycle(concrete, queue, TypeHelper.IActorLifeCycleType,
                        new ActorLifeCycleInterceptor(queue, asyncDisposable));
        }

        public T Build<T>(T concrete) where T : class
        {
            return Build<T>(concrete, GetQueue());
        }

        public Task<T> BuildAsync<T>(Func<T> concrete) where T : class
        {
            var queue = GetQueue();
            return queue.Enqueue( ()=> Build<T>(concrete(),queue) );
        }        
        
        private IStopableTaskQueue GetQueue()
        {
            return new TaskSchedulerQueue();
        }
    }
}
