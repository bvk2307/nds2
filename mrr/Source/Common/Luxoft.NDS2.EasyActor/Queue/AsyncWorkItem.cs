﻿#define NET40

using System;
using System.Threading.Tasks;

namespace EasyActor.Queue
{
    internal class AsyncWorkItem<T> : IWorkItem
    {
        private readonly TaskCompletionSource<T> _Source;
        private readonly Func<Task<T>> _Do;

        public AsyncWorkItem(Func<Task<T>> iDo)
        {
            _Do = iDo;
            _Source = new TaskCompletionSource<T>();
        }

        public Task<T> Task
        {
            get { return _Source.Task; }
        }

        public void Cancel()
        {
            _Source.TrySetCanceled();
        }

        public 
#if !NET40
            async 
#endif //!NET40
            void Do()
        {
            try
            {

#if NET40
            _Do().ContinueWith(
                doTask =>
                {   //this is not important if the original thread has SynchronizationContext and from which thread TrySetResult()|TrySetException() will be called
                    AggregateException exception = doTask.Exception;
                    if ( exception == null )
                         _Source.TrySetResult( doTask.Result );
                    else _Source.TrySetException( exception );
                    } );    //, TaskContinuationOptions.ExecuteSynchronously );	//apopov 4.4.2016	//DEBUG!!!

#else //!NET40

                _Source.TrySetResult(await _Do());

#endif //NET40

            }
            catch(Exception e)
            {
                _Source.TrySetException(e);
            }
        }
    }

    internal class AsyncActionWorkItem : AsyncWorkItem<object>, IWorkItem
    {
        public AsyncActionWorkItem( Func<Task> iDo )
            : base(
#if !NET40
                async 
#endif //!NET40
                () =>
                {
#if NET40
                    return iDo().ContinueWith(
                        doTask =>
                        {
                            doTask.Wait();  //to rethrow an exception if it is caused

                            return (object)null;
                        } );    //, TaskContinuationOptions.ExecuteSynchronously );	//apopov 4.4.2016	//DEBUG!!!
#else //!NET40
                    await iDo(); return null;
#endif //NET40
                } )
        {
        }
    }
}
