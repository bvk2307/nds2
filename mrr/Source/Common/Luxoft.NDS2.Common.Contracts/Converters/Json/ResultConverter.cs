﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Newtonsoft.Json;

namespace Luxoft.NDS2.Common.Contracts.Converters.Json
{
    public class ResultConverter
    {
        public static string Convert(Stream str)
        {
            string result = null;
            using (StreamReader reader = new StreamReader(str))
            {
                using (JsonTextReader jsonReader = new JsonTextReader(reader))
                {
                    while (jsonReader.Read())
                    {
                        if(jsonReader.TokenType == JsonToken.PropertyName)
                        {
                            if (jsonReader.Value.Equals("Result"))
                            {
                                jsonReader.Read();
                                result = jsonReader.Value.ToString();
                            }                            
                        }
                    }
                }
            }

            return result;
        }
    }
}
