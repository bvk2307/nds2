﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;

namespace Luxoft.NDS2.Common.Contracts.Converters.Json
{
//    public class BuyBookJsonConverter : JsonBookConverter
//    {
//        private Action<BuyBookLineDemo> _callBack;
//
//        public BuyBookJsonConverter(Action<BuyBookLineDemo> callback)
//        {
//            _callBack = callback;
//        }
//
//        #region Overrides of JsonBookConverter
//
//        public override int ConvertFromStream(Stream stream, CancellationToken cancelToken)
//        {
//            return ConvertInternal(stream, _callBack, cancelToken);
//        }
//
//
//        protected override void FillObjectProperty(JsonTextReader reader, object obj)
//        {
//            var buyBook = (BuyBookLineDemo)obj;
//            CultureInfo ciENG = new CultureInfo("en-US", false);
//            CultureInfo ciRUS = new CultureInfo("ru-RU", false);
//            string propertyName = reader.Value.ToString();
//            reader.Read();
//            object propertyValue = reader.Value;
//            switch (propertyName)
//            {
//                case "k":
//                    buyBook.InvoiceId = propertyValue as string;
//                    break;
//                case "si":
//                    buyBook.SellerInn = propertyValue as string;
//                    break;
//                case "sk":
//                    buyBook.SellerKpp = propertyValue as string;
//                    break;
//                case "n":
//                    buyBook.SellerInvoiceNum = propertyValue as string;
//                    break;
//                case "d":
//                    buyBook.SellerInvoiceDate = Convert.ToDateTime(propertyValue, ciRUS.DateTimeFormat);
//                    break;
//                case "o":
//                    buyBook.OperationCode = propertyValue as string;
//                    break;
//                case "v":
//                    buyBook.NdsAmountRub = Convert.ToDecimal(propertyValue, ciENG);
//                    break;
//                case "s":
//                    buyBook.TotalAmountByInvoice = Convert.ToDecimal(propertyValue, ciENG);
//                    break;
//            }
//        }
//
//        #endregion
//    }
}
