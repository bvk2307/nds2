﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Newtonsoft.Json;

namespace Luxoft.NDS2.Common.Contracts.Converters.Json
{
    public class SellBookJsonConverter : JsonBookConverter
    {
        private Action<SellBookLineDemo> _callBack;

        public SellBookJsonConverter(Action<SellBookLineDemo> callback)
        {
            _callBack = callback;
        }

        #region Overrides of JsonBookConverter

        public override int ConvertFromStream(Stream stream, CancellationToken cancelToken)
        {
            return ConvertInternal(stream, _callBack, cancelToken);
        }

        protected override void FillObjectProperty(JsonTextReader reader, object obj)
        {
            var sellBook = (SellBookLineDemo)obj;
            CultureInfo ciENG = new CultureInfo("en-US", false);
            CultureInfo ciRUS = new CultureInfo("ru-RU", false);
            string propertyName = reader.Value.ToString();
            reader.Read();
            object propertyValue = reader.Value;
            switch (propertyName)
            {
                case "k":
                    sellBook.InvoiceId = propertyValue as string;
                    break;
                case "o":
                    sellBook.OperationCode = propertyValue as string;
                    break;
                case "n":
                    sellBook.SellerInvoiceNum = propertyValue as string;
                    break;
                case "d":
                    sellBook.SellerInvoiceDate = Convert.ToDateTime(propertyValue, ciRUS.DateTimeFormat);
                    break;
                case "bi":
                    sellBook.BuyerInn = propertyValue as string;
                    break;
                case "bk":
                    sellBook.BuyerInn = propertyValue as string;
                    break;
                case "s":
                    sellBook.InvoiceTotalNdsInclude = Convert.ToDecimal(propertyValue, ciENG);
                    break;
                case "s18":
                    sellBook.AmountTaxed18Rub = Convert.ToDecimal(propertyValue, ciENG);
                    break;
                case "s10":
                    sellBook.AmountTaxed10Rub = Convert.ToDecimal(propertyValue, ciENG);
                    break;
                case "s0":
                    sellBook.AmountTaxed0 = Convert.ToDecimal(propertyValue, ciENG);
                    break;
                case "v18":
                    sellBook.AmountTaxedNds18Rub = Convert.ToDecimal(propertyValue, ciENG);
                    break;
                case "v10":
                    sellBook.AmountTaxedNds10Rub = Convert.ToDecimal(propertyValue, ciENG);
                    break;
                case "sn":
                    sellBook.TaxFreeAmountRub = Convert.ToDecimal(propertyValue, ciENG);
                    break;
            }
        }

        #endregion
    }
}
