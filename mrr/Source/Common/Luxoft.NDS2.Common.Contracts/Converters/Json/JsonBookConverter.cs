﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using Luxoft.NDS2.Common.Contracts.DTO.Business;

namespace Luxoft.NDS2.Common.Contracts.Converters.Json
{
    public enum ResponseResult
    {
        Ok,
        NoContent,
        ServerError,
        BadRequest,
        Unknown
    }

//    public abstract class JsonBookConverter
//    {
//        private int _convertedObjects;
//        public int ConverterObjects{get { return _convertedObjects; }}
//
//        public ResponseResult ResponseStatus { get; private set;}
//
//        protected int ConvertInternal<T>(Stream data, Action<T> callback, CancellationToken cancelToken) where T : BusinessObject, new()
//        {
//            bool enteredToArrayOfLines = false;
//            bool enteredToObject = false;
//            T currentBookLine = null;
//            int _convertedObjects = 0;
//            using (var textReader = new StreamReader(data))
//            {
//                using (var reader = new JsonTextReader(textReader))
//                {
//                    long lineNum = 1;
//                    while (reader.Read())
//                    {
//                        if (cancelToken.IsCancellationRequested)
//                        {
//                            Trace.WriteLine("JsonBookConverter: cancellation requested");
//                            return _convertedObjects;
//                        }
//
//                        switch (reader.TokenType)
//                        {
//                            case JsonToken.StartArray:
//                                enteredToArrayOfLines = true;
//                                break;
//                            case JsonToken.StartObject:
//                                if (enteredToArrayOfLines)
//                                {
//                                    currentBookLine = new T();
//                                    //FillDefaults(currentBookLine);
//                                    enteredToObject = true;
//                                    //currentBookLine.Id = lineNum;
//                                    lineNum++;
//                                }
//                                break;
//                            case JsonToken.PropertyName:
//                                if(reader.Value.Equals("status"))
//                                {
//                                    reader.Read();
//                                    ResponseStatus = ReadResult(reader.Value);
//                                }
//                                if (enteredToArrayOfLines & enteredToObject)
//                                {
//                                    FillObjectProperty(reader, currentBookLine);
//                                }
//                                break;
//                            case JsonToken.EndObject:
//                                if (enteredToArrayOfLines & enteredToObject)
//                                {
//                                    callback(currentBookLine);
//                                    _convertedObjects++;
//                                    enteredToObject = false;
//                                }
//                                break;
//                        }
//                    }
//                }
//            }
//
//            return _convertedObjects;
//        }
//
//        public abstract int ConvertFromStream(Stream stream, CancellationToken cancelToken);
//
//
//        protected abstract void FillObjectProperty(JsonTextReader reader, object obj);
//
//        private ResponseResult ReadResult(object value)
//        {
//            if (value == null) return ResponseResult.Unknown;
//            
//            switch (value.ToString().ToLowerInvariant())
//            {
//                case "Ok":
//                    return ResponseResult.Ok;
//                case "No Content":
//                    return ResponseResult.NoContent;
//                case "Internal Server Error":
//                    return ResponseResult.ServerError;
//                case "Bad Request":
//                    return ResponseResult.BadRequest;
//                default:
//                    return ResponseResult.Unknown;
//            }
//        }
//
//    }
}
