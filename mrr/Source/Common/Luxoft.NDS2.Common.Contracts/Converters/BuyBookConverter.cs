﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Xml;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;

namespace Luxoft.NDS2.Common.Contracts.Converters
{
    public class BuyBookConverter : BaseBookConverter
    {
        public static List<BuyBookLineDemo> ToObject(Stream xmlcontentStream)
        {
            List<BuyBookLineDemo> result = new List<BuyBookLineDemo>();
            int lineNumnber = 1;
            using (XmlReader reader = XmlReader.Create(xmlcontentStream))
            {
                reader.MoveToContent();
                while (reader.Read())
                {
                    if(reader.NodeType == XmlNodeType.Element)
                    {
                        switch (reader.Name)
                        {
                            case "Документ":
                                if (reader.AttributeCount > 0)
                                {
                                    reader.MoveToAttribute(0);
                                    //sellBook.Knd = Convert.ToInt32(reader.Value);                                    
                                }
                                break;
                            case "КнПокСтр":
                                BuyBookLineDemo line = new BuyBookLineDemo();
                                line.Id = lineNumnber;
                                result.Add(line);
                                InitDefaults(line);
                                ReadBookLineAttributes(line, reader);
                                ++lineNumnber;
                                break;
                            case "ДатаОплСчФПрод":
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            return result;
        }

        private static void ReadBookLineAttributes(BuyBookLineDemo b, XmlReader reader)
        {
            var formatProvider = new CultureInfo("en-US");
            for (int i = 0; i < reader.AttributeCount; i++)
            {
                reader.MoveToAttribute(i);
                switch (reader.Name)
                {
                    case "НомСчФПрод":
                        b.SellerInvoiceNum = reader.Value;
                        break;
                    case "ДатаСчФПрод":
                        b.SellerInvoiceDate = Convert.ToDateTime(reader.Value);
                        break;
                    case "НомИспрСчФ":
                        b.SellerInvoiceNumChanged = reader.Value;
                        break;
                    case "ДатаИспрСчФ":
                        b.SellerInvoiceDateChanged = Convert.ToDateTime(reader.Value);
                        break;
                    case "НомИспрКСчФ":
                        b.SellerInvoiceNumChangedAndCorrected = reader.Value;
                        break;
                    case "ДатаИспрКСчФ":
                        b.SellerInvoiceDateChangedAndCorrected = Convert.ToDateTime(reader.Value);
                        break;
                    case "НомКСчФПрод":
                        b.SellerInvoiceNumCorrected = reader.Value;
                        break;
                    case "ДатаКСчФПрод":
                        b.SellerInvoiceDateCorrected = Convert.ToDateTime(reader.Value);
                        break;
                    case "НомДокПдтвОпл":
                        b.PaymentDocNumber = reader.Value;
                        break;
                    case "ДатаДокПдтвОпл":
                        b.PaymentDocDate = Convert.ToDateTime(reader.Value);
                        break;
                    case "НомТД":
                        b.CustomsDeclarationNumber = reader.Value;
                        break;
                    case "НомерПор":
                        b.CustomsDeclarationNumber = reader.Value;
                        break;
                    case "СтоимПокупВ":
                        b.TotalAmountByInvoice = Convert.ToDecimal(reader.Value, formatProvider);
                        break;
                    case "СумНДСВыч":
                        b.NdsAmountRub = Convert.ToDecimal(reader.Value, formatProvider);
                        break;
                    case "ДатаУчТов":
                        break;
                    default:
                        break;
                }
            }

            reader.MoveToElement();
        }

        private static void InitDefaults(BuyBookLineDemo book)
        {
            book.SellerInn = GetRandomINN();
            book.SellerName = GetRandomTaxPayerName();
            book.SellerKpp = GetRandomKPP();
            book.BrokerInn = GetRandomINN();
            book.BrokerName = GetRandomTaxPayerName();
            book.BrokerKpp = GetRandomKPP();
            book.RegisterDate = GetRandomDateString();
            book.Currency = "Руб.";
        }
    }
}
