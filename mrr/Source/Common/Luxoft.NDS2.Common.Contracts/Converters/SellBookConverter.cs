﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Xml;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;

namespace Luxoft.NDS2.Common.Contracts.Converters
{
    public class SellBookConverter : BaseBookConverter
    {
        public static List<SellBookLineDemo> ToObject(Stream xmlcontentStream)
        {
            List<SellBookLineDemo> result = new List<SellBookLineDemo>();
            int lineNumber = 1;
            using (XmlReader reader = XmlReader.Create(xmlcontentStream))
            {
                reader.MoveToContent();
                while (reader.Read())
                {
                    if(reader.NodeType == XmlNodeType.Element)
                    {
                        switch (reader.Name)
                        {
                            case "Документ":
                                if (reader.AttributeCount > 0)
                                {
                                    reader.MoveToAttribute(0);
                                    //sellBook.Knd = Convert.ToInt32(reader.Value);                                    
                                }
                                break;

                            case "СвПродав":
                                break;

                            case "СведЮЛ":
                                //ReadSellerInfo(sellBook, reader);
                                break;

                            case "СвКнПрод":
                                //ReadBookHeader(sellBook, reader);
                                break;

                            case "Всего":
//                                if (reader.AttributeCount > 0)
//                                {
//                                    reader.MoveToAttribute(0);
//                                    sellBook.TotalSum = Convert.ToDecimal(reader.Value, formatProvider);
//                                    reader.MoveToElement();
//                                }

                                //ReadBookTaxSummary(sellBook, reader.ReadSubtree());
                                break;

                            case "СвПродаж":
                                SellBookLineDemo line = new SellBookLineDemo();
                                line.Id = lineNumber;
                                result.Add(line);
                                InitDefaults(line);
                                ReadBookLineAttributes(line, reader);
                                ReadBookTaxLine(line, reader.ReadSubtree());
                                lineNumber++;
                                break;
                            case "ДатаОплСчФПрод":
                                break;
                            default:
                                break;
                        }
                    }
                }
            }


            return result;
        }

        private static void InitDefaults(SellBookLineDemo book)
        {
            book.OperationCode = GetRandomOperationCode();
            book.SellerInvoiceNum = GetRandomInvoiceNumber();
            book.SellerInvoiceDate = GetRandomDateString();
            book.SellerInvoiceNumChanged = GetRandomInvoiceNumber();
            book.SellerInvoiceDateChanged = GetRandomDateString();
            book.SellerInvoiceNumCorrected = GetRandomInvoiceNumber();
            book.SellerInvoiceDateCorrected = GetRandomDateString();

            book.BrokerName = GetRandomTaxPayerName();
            book.BrokerInn = GetRandomINN();
            book.BrokerKpp = GetRandomKPP();

            book.PaymentDocNumber = GetRandomInvoiceNumber();
            book.PaymentDocDate = GetRandomDateString();

            book.Currency = "Руб.";

        }

        private static void ReadBookLineAttributes(SellBookLineDemo sl, XmlReader reader)
        {
            var formatProvider = new CultureInfo("en-US");
            for (int i = 0; i < reader.AttributeCount; i++)
            {
                reader.MoveToAttribute(i);
                switch (reader.Name)
                {
                    case "ДатаСчФ":
                        //sl.BuyerSellerInvoicePayDay = Convert.ToDateTime(reader.Value);
                        break;
                    case "НомерСчФ":
                        //sl.InvoiceNumber = reader.Value;
                        break;
                    case "НаимПок":
                        sl.BuyerName = reader.Value;
                        break;
                    case "ИННЮЛ":
                        sl.BuyerInn = reader.Value;
                        break;
                    case "КПП":
                        sl.BuyerKpp = reader.Value;
                        break;
                    case "СтТовУчНалВсего":
                        sl.InvoiceTotalNdsInclude = Convert.ToDecimal(reader.Value, formatProvider);
                        break;
                    default:
                        break;
                }
            }

            reader.MoveToElement();
        }

        private static void ReadBookTaxLine(SellBookLineDemo sl, XmlReader reader)
        {
            var formatProvider = new CultureInfo("en-US");
            Action<decimal> ndsValueSetter = v => { };
            decimal nonNdsValue = 0;

            using (reader)
            {
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element)
                    {
                        switch (reader.Name)
                        {
                            case "ДатаОплСчФПрод":
                                reader.Read();
                                //sl.BuyerSellerInvoicePayDay = Convert.ToDateTime(reader.Value);
                                break;
                            case "ВтЧисле":
                                if (reader.AttributeCount > 0)
                                {
                                    reader.MoveToAttribute(0);
                                    nonNdsValue = Convert.ToDecimal(reader.Value, formatProvider);
                                }
                                else
                                {
                                    nonNdsValue = 0;
                                }
                                break;
                            case "НалСт":
                                reader.MoveToAttribute(0);//НалСтВел
                                switch (Convert.ToInt32(reader.Value))
                                {
                                    case 0:
                                        ndsValueSetter = v => { sl.AmountTaxed0 = v; };
                                        break;
                                    case 10:
                                        sl.AmountTaxed10Rub = nonNdsValue;
                                        ndsValueSetter = v => { sl.AmountTaxedNds10Rub = v; };
                                        break;
                                    case 18:
                                        sl.AmountTaxed18Rub = nonNdsValue;
                                        ndsValueSetter = v => { sl.AmountTaxedNds18Rub = v; };
                                        break;
                                    case 20:
//                                        sl.Amount20Tax = nonNdsValue;
//                                        ndsValueSetter = v => { sl.Amount20Nds = v; };
                                        break;
                                }

                                break;
                            case "СумНал":
                                reader.MoveToAttribute(0);
                                ndsValueSetter(Convert.ToDecimal(reader.Value, formatProvider));
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
