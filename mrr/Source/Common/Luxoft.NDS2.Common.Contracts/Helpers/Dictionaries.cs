﻿using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Common.Contracts.Helpers
{
    public class Dictionaries
    {
        public static Dictionary<string, string> GetDictChapter()
        {
            return new Dictionary<string, string>
            {
                {"8", "8"},
                {"9", "9"},
                {"10", "10"},
                {"11", "11"},
                {"12", "12"}
            };
        }

        public static Dictionary<string, string> GetDictMark()
        {
            return new Dictionary<string, string>
            {
                {"1", "К уплате"},
                {"2", "К возмещению"},
                {"3", "Нулевая"}
            };
        }

        public static Dictionary<string, string> GetPeriodDictionary(int availablePeriodCount = 12)
        {
            var codes = new Dictionary<int, string>
            {
                {1, "21"},
                {2, "22"},
                {3, "23"},
                {4, "24"}
            };
            var result = new Dictionary<string, string>(0);
            var q = (int)Math.Round((DateTime.Today.Month + 2) / 3f); // номер квартала
            var year = DateTime.Today.Year;
            for (var i = 0; i < availablePeriodCount; ++i)
            {
                --q;
                if (q == 0)
                {
                    q = 4;
                    --year;
                }
                result[string.Format("{0}{1}", year, codes[q])] = string.Format("{0} кв. {1} г.", q, year);
            }
            return result;
        }
    }
}
