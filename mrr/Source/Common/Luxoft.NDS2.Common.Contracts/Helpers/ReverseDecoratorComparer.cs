﻿using System.Collections.Generic;

namespace Luxoft.NDS2.Common.Contracts.Helpers
{
    public class ReverseDecoratorComparer<T> : IComparer<T>
    {
        private readonly IComparer<T> _decoratedComparer;

        public ReverseDecoratorComparer( IComparer<T> decoratedComparer )
        {
            System.Diagnostics.Contracts.Contract.Requires( decoratedComparer != null );

            _decoratedComparer = decoratedComparer;
        }

        /// <summary>Compares two objects and returns a value indicating whether one is less than, equal to, or greater than the other.</summary>
        /// <returns>A signed integer that indicates the relative values of <paramref name="x" /> and <paramref name="y" />, as shown in the following table.Value Meaning Less than zero<paramref name="x" /> is less than <paramref name="y" />.Zero<paramref name="x" /> equals <paramref name="y" />.Greater than zero<paramref name="x" /> is greater than <paramref name="y" />.</returns>
        /// <param name="x">The first object to compare.</param>
        /// <param name="y">The second object to compare.</param>
        public int Compare( T x, T y )
        {
            return -1 * _decoratedComparer.Compare( x, y );
        }
    }
}