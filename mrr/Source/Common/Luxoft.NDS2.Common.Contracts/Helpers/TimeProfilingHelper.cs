﻿using System.Diagnostics;
using CommonComponents.Instrumentation;

namespace Luxoft.NDS2.Common.Contracts.Helpers
{
    /// <summary> Helps to trace time profiling info into <see cref="IInstrumentationService"/>. </summary>
    public sealed class TimeProfilingHelper
    {
        private readonly IInstrumentationService _instrumentationService;

        public TimeProfilingHelper( IInstrumentationService instrumentationService )
        {
            _instrumentationService = instrumentationService;
        }

        public IProfilingEntry InitTracing( 
            string tracingName, string profilingMessageTypeName, InstrumentationLevel instrumentationLevel = InstrumentationLevel.Debug, 
            TraceEventType traceEventType = TraceEventType.Verbose, int messagePriority = 0 )
        {
            System.Diagnostics.Contracts.Contract.Requires( !string.IsNullOrWhiteSpace( tracingName ) );
            System.Diagnostics.Contracts.Contract.Ensures( System.Diagnostics.Contracts.Contract.Result<IProfilingEntry>() != null );

            IProfilingEntry profilingEntry = _instrumentationService.CreateProfilingEntry( 
                                    traceEventType, (int)instrumentationLevel, profilingMessageTypeName, messagePriority );
            profilingEntry.MessageText = tracingName;

            return profilingEntry;
        }

        public void TracePoint( IProfilingEntry profilingEntry, string pointName = null, object pointDetails = null )
        {
            System.Diagnostics.Contracts.Contract.Requires( profilingEntry != null );
            System.Diagnostics.Contracts.Contract.Requires( !string.IsNullOrWhiteSpace( pointName ) || pointDetails != null );

            profilingEntry.CreateCheckpoint( 
                pointName, profilingEntry.EventType, description: null, monitoringItem: pointDetails );
        }

        public void WriteTracing( IProfilingEntry profilingEntry )
        {
            System.Diagnostics.Contracts.Contract.Requires( profilingEntry != null );

            _instrumentationService.Write( profilingEntry );
        }
    }
}