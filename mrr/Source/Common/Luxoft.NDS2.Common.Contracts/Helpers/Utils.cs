﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.ActiveDirectory;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.Helpers
{
    public class Utils
    {
        public static bool IsKnpClosedByAnnulment(int? closeReason)
        {
            var closeResonByAnnulment = 5;

            return closeReason.HasValue
                        ? closeReason.Value == closeResonByAnnulment
                        : false;
        }       
        
        public static double GetUnixDate()
        {
            return DateTime.UtcNow.Subtract(new DateTime(2000, 1, 1)).TotalMilliseconds;
        }

        public static byte[] Zip(string str)
        {
            var bytes = Encoding.UTF8.GetBytes(str);

            using (var msi = new MemoryStream(bytes))
            using (var mso = new MemoryStream())
            {
                using (var gs = new GZipStream(mso, CompressionMode.Compress))
                {
                    //msi.CopyTo(gs);
                    CopyTo(msi, gs);
                }

                return mso.ToArray();
            }
        }

        public static string Unzip(byte[] bytes)
        {
            using (var msi = new MemoryStream(bytes))
            using (var mso = new MemoryStream())
            {
                using (var gs = new GZipStream(msi, CompressionMode.Decompress))
                {
                    //gs.CopyTo(mso);
                    CopyTo(gs, mso);
                }

                return Encoding.UTF8.GetString(mso.ToArray());
            }
        }

#if DEBUG
        public static bool IsUserInDomain()
        {

            try
            {
                var d = System.DirectoryServices.ActiveDirectory.Domain.GetComputerDomain();
                return true;
            }
            catch (ActiveDirectoryObjectNotFoundException)
            {
                return false;
            }

        }
#else
        public static bool IsUserInDomain()
        {
            return true;
        }
#endif

        private static void CopyTo(Stream src, Stream dest)
        {
            byte[] bytes = new byte[4096];

            int cnt;

            while ((cnt = src.Read(bytes, 0, bytes.Length)) != 0)
            {
                dest.Write(bytes, 0, cnt);
            }
        }

    }
        
    
}
