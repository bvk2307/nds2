﻿using System;
using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.Services;

//Пример использования: Client\Luxoft.NDS2.Client\UI\SettingsMRR\Calendar\CalendarPresenter.cs TestCalendarHelper()
namespace Luxoft.NDS2.Common.Contracts.Helpers
{
    /// <summary>
    /// Помощник по производственному календарю
    /// </summary>
    public class CalendarHelper
    {
        private readonly ICalendarDataService _service;
        private List<DateTime> _redDays;

        /// <summary>
        /// Помощник по производственному календарю
        /// </summary>
        /// <param name="service">Сервис для доступа к БД</param>
        public CalendarHelper(ICalendarDataService service)
        {
            _service = service;
        }

        /// <summary>
        /// Помощник по производственному календарю, инициализиорванный для периода begin - end
        /// </summary>
        /// <param name="service">Сервис для доступа к БД</param>
        /// <param name="begin">Начало периода</param>
        /// <param name="end">Конец периода</param>
        public CalendarHelper(ICalendarDataService service, DateTime begin, DateTime end) : this(service)
        {
            Init(begin, end);
        }

        /// <summary>
        /// Инициализация данными для периода begin-end
        /// </summary>
        /// <param name="begin">Начало периода</param>
        /// <param name="end">Конец периода</param>
        public void Init(DateTime begin, DateTime end)
        {
            _redDays = _service.GetRedDays(begin, end).Result;
        }

        /// <summary>
        /// Проверка является ли день выходным
        /// </summary>
        /// <param name="date">Дата (время отрезать НЕ НУЖНО)</param>
        /// <returns></returns>
        public bool IsRedDay(DateTime date)
        {
            if (_redDays == null)
                throw new Exception("CalendarHelper не инициализирован.");
            return _redDays.Any(d => d.Date == date.Date);
        }

        /// <summary>
        /// Дата со сдвигом на days рабочих дней
        /// </summary>
        /// <param name="date">Начальная дата</param>
        /// <param name="days">Количество дней</param>
        /// <returns>Новая дата</returns>
        public DateTime AddWorkingDays(DateTime date, int days)
        {
            if (days == 0)
                return date;
            var shift = days > 0 ? 1 : -1;
            while (days != 0)
            {
                date = date.AddDays(shift);
                while (IsRedDay(date))
                    date = date.AddDays(shift);
                days -= shift;
            }
            return date;
        }
    }
}
