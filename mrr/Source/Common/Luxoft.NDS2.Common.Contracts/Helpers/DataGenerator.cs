﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.Helpers
{
    public static class DataGenerator
    {
        static Random _random = new Random();

        public static DateTime GetRandomDateString()
        {
            DateTime start = new DateTime(2012, 1, 1);
            int range = (DateTime.Today - start).Days;
            return start.AddDays(_random.Next(range));
        }

        public static string GetRandomInvoiceChangeNum()
        {
            return _random.Next(1, 10).ToString();
        }

        public static string GetRandomInvoiceNumber()
        {
            return _random.Next(1000, 10000000).ToString();
        }

        public static string GetRandomOperationCode()
        {
            return _random.Next(10, 20).ToString();
        }

        public static string GetRandomINN()
        {
            var l = _random.Next(100000, 999999);
            var r = _random.Next(100000, 999999);
            return string.Format("{0}{1}", l, r);
        }

        public static string GetRandomINN_10digit()
        {
            var l = _random.Next(10000, 99999);
            var r = _random.Next(10000, 99999);
            return string.Format("{0}{1}", l, r);
        }

        public static string GetRandomKPP()
        {
            var l = _random.Next(100000, 999999);
            var r = _random.Next(100, 999);
            return string.Format("{0}{1}", l, r);
        }

        public static string GetRandomDecimalString()
        {
            return string.Format("{0}.{1}", _random.Next(10, 1000000), _random.Next(10, 99));
        }

        public static string GetRandomLongString()
        {
            return string.Format("{0}", _random.Next(10, 1000000));
        }

        public static decimal? GetRandom_COMPENSATION_AMNT()
        {
            decimal ret = (decimal)_random.Next(-200000, 200000) + (decimal)(((decimal)(_random.Next(10, 99))) / 100);
            return (decimal ?)ret;
        }

        public static string GetRandomIndexChapterString(int ind)
        {
            Dictionary<int, string> dictChapters = new Dictionary<int, string>();
            dictChapters.Add(0, "000080");
            dictChapters.Add(1, "000090");
            dictChapters.Add(2, "000100");
            dictChapters.Add(3, "000110");
            dictChapters.Add(4, "000120");

            string ret = "000080";
            if (dictChapters.ContainsKey(ind))
            {
                ret = dictChapters[ind];
            }
            return ret;
        }

        public static int GetChapterNum(int ind)
        {
            Dictionary<int, int> dictChapters = new Dictionary<int, int>();
            dictChapters.Add(0, 0); // 8 раздел
            dictChapters.Add(1, 1); // 9 раздел
            dictChapters.Add(2, 4); // 10 раздел
            dictChapters.Add(3, 5); // 11 раздел
            dictChapters.Add(4, 6); // 12 раздел
            dictChapters.Add(5, 2); // 8.1 раздел
            dictChapters.Add(6, 3); // 9.1 раздел

            int ret = 0;
            if (dictChapters.ContainsKey(ind))
            {
                ret = dictChapters[ind];
            }
            return ret;
        }

        public static decimal GetRandomDecimal()
        {
            return Convert.ToDecimal(_random.NextDouble());
        }

        public static decimal GetRandomDecimalBig()
        {
            decimal ret = Convert.ToDecimal(_random.Next(10, 10000000));
            decimal dop = Convert.ToDecimal(_random.NextDouble());
            ret += dop;
            return ret;
        }

        public static decimal? GetRandomNullableDecimal(double nullSeed = 0.2)
        {
            var d = _random.NextDouble();
            return d > nullSeed ? Convert.ToDecimal(d) : (decimal?)null;
        }

        public static string GetRandomTaxPayerName()
        {
            return Guid.NewGuid().ToString();
        }

        public static string GetRandomSource()
        {
            var sources = new List<string>()
            {
                "Книга покупок",
                "Книга продаж"
            };

            int pos = _random.Next(0, sources.Count);

            return sources[pos];
        }

        public static string GetRandomStatus()
        {
            var selectionStatuses = new List<string>()
            {
                "Черновик",
                "На доработке",
                "На утверждении",
                "Отправлено",
                "Откланено",
                "Утверждено",
                "Удалено"
            };

            int pos = _random.Next(0, selectionStatuses.Count);

            return selectionStatuses[pos];
        }

        public static decimal GetRandomPrice()
        {
            return Convert.ToDecimal(_random.Next(1000, 10000000));
        }

    }
}
