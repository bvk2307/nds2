﻿using System.Globalization;

namespace Luxoft.NDS2.Common.Contracts.Helpers
{
    /// <summary> Auxiliary method to convert a numeric type value to format string. </summary>
    public static class NumericFormatHelper
    {
        public static string ToTwoDigitAfterDot( decimal val )
        {
            var nfi = (NumberFormatInfo)CultureInfo.InvariantCulture.NumberFormat.Clone();
            nfi.NumberGroupSeparator = " ";
            return val == 0 ? "0" : val.ToString( "#,#.00", nfi );
        }

        public static string ToTwoDigitAfterDot( long val )
        {
            var nfi = (NumberFormatInfo)CultureInfo.InvariantCulture.NumberFormat.Clone();
            nfi.NumberGroupSeparator = " ";
            return val == 0 ? "0" : val.ToString( "#,#.00", nfi );
        }

        public static string ToString( int val )
        {
            string stringValue = val.ToString( CultureInfo.InvariantCulture );

            return stringValue;
        }
    }
}