﻿namespace Luxoft.NDS2.Common.Contracts.Helpers
{
    /// <summary>
    /// Класс описывает ключ к декларации контрагента, состоящий из
    /// Года подачи декларации (первые 4 цифры)
    /// Месяца подачи декларации (последующие 2 цифры)
    /// ИНН (оставшиеся 12 цифр)
    /// </summary>
    public class ContractorDeclarationKey
    {
        private const int INN_STARTING_INDEX = 6;
        private const int MONTH_STARTING_INDEX = 4;
        private const int MONTH_DIGIT_COUNT = 2;
        private const int YEAR_DIGIT_COUNT = 4;
        private readonly string _key;

        public ContractorDeclarationKey(long key)
        {
            _key = key.ToString(System.Globalization.CultureInfo.InvariantCulture);
        }

        public long GetInn()
        {
            return long.Parse(_key.Substring(INN_STARTING_INDEX));
        }

        public int GetMonth()
        {
            return int.Parse(_key.Substring(MONTH_STARTING_INDEX, MONTH_DIGIT_COUNT));
        }

        public int GetYear()
        {
            return int.Parse(_key.Substring(0, YEAR_DIGIT_COUNT));
        }
    }
}
