﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.Helpers
{
    public static class TypeHelper<T> where T : class
    {
        public static string GetMemberName<TProperty>(Expression<Func<T, TProperty>> propertyExpresion)
        {
            MemberExpression memberBody = (MemberExpression)propertyExpresion.Body;
            return memberBody.Member.Name;
        }
    }
}
