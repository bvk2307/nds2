﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.Caching
{
    public interface ICachingProvider
    {
        bool ContainsCache(string key);

        T GetItem<T>(string key);
        List<T> GetItems<T>(string key);
    }
}
