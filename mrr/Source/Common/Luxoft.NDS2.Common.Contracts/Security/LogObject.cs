﻿using System;

namespace Luxoft.NDS2.Common.Contracts.Security
{
    [Serializable]
    public sealed class LogObject
    {
        public const int SUBSYSTEM_ID = 99;
        public const string SUBSYSTEM_TITLE = "НДС-2";
        public const string SUBSYSTEM_CODE = "NDS2";

        public long? ObjectId { get; set; }

        public string UserName { get; set; }

        public DateTime IssueDate { get; set; }

        public LogOperation Operation { get; set; }
    }

    public class LogOperationInfo
    {
        public LogOperation Operation { get; set; }

        public string Message { get; set; }

        public string Description { get; set; }
    }

    public enum LogOperation
    {
        /// <summary>Создание выборки</summary>
        SelectionCreate = 99001,
        /// <summary>Изменение выборки</summary>
        SelectionChange = 99002,
        /// <summary>Удаление выборки</summary>
        SelectionRemove = 99003,
        /// <summary>Отправка выборки на согласование</summary>
        SelectionToApprove = 99004,
        /// <summary>Согласование выборки</summary>
        SelectionApproved = 99005,
        /// <summary>Возврат выборки на доработку</summary>
        SelectionToCorrect = 99006,
        /// <summary>Отклонение выборки</summary>
        SelectionReject = 99007,
        /// <summary>Отправка выборки</summary>
        SelectionSendToSeod = 99008,
        /// <summary>Внесение уточнения по расхождению</summary>
        DiscrepancyAddCorrection = 99009,
        /// <summary>Создание автотребования</summary>
        DocumentCreate = 99010,
        /// <summary>Изменение статуса автотребования</summary>
        DocumentStatusChange = 99011,
        /// <summary>Выгрузка в Excel. Отчет 2 МЭ</summary>
        EfficeincyExcelMonitoringReport_2ME_UFNS = 99012,
        /// <summary>Выгрузка в Excel. МЭ Рейтинг ИФНС</summary>
        EfficeincyExcelMonitoringReportIFNSRating = 99013,
        /// <summary>Выгрузка в Excel. МЭ ДНР</summary>
        EfficeincyExcelMonitoringUnresilvedDiscrepancyReport = 99014,
        /// <summary>Выгрузка в Excel. МЭ Еженедельный отчет</summary>
        EfficeincyExcelMonitoringReportWeekly = 99015

    }
}