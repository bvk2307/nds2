﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using CommonComponents.Instrumentation;
using CommonComponents.Security.SecurityLogger;

namespace Luxoft.NDS2.Common.Contracts.Security
{
    public static class LogExtention
    {
        private static readonly ReadOnlyCollection<LogOperationInfo> _operations;

        private static readonly ReadOnlyCollection<SecurityLogEntryTemplate> _securityTemplates;

        static LogExtention()
        {
            var operations = new List<LogOperationInfo>
            {
                new LogOperationInfo
                {
                    Operation = LogOperation.SelectionCreate,
                    Message = "NDS2: Selection - Create",
                    Description = "Создание выборки"
                },
                new LogOperationInfo
                {
                    Operation = LogOperation.SelectionChange,
                    Message = "NDS2: Selection - Edit",
                    Description = "Изменение выборки"
                },
                new LogOperationInfo
                {
                    Operation = LogOperation.SelectionRemove,
                    Message = "NDS2: Selection - Delete ",
                    Description = "Удаление выборки"
                },
                new LogOperationInfo
                {
                    Operation = LogOperation.SelectionToApprove,
                    Message = "NDS2: Selection - Send to approve",
                    Description = "Отправка выборки на согласование"
                },
                new LogOperationInfo
                {
                    Operation = LogOperation.SelectionApproved,
                    Message = "NDS2: Selection - Approved",
                    Description = "Согласование выборки"
                },
                new LogOperationInfo
                {
                    Operation = LogOperation.SelectionToCorrect,
                    Message = "NDS2: Selection - Send to corrent",
                    Description = "Возврат выборки на доработку"
                },
                new LogOperationInfo
                {
                    Operation = LogOperation.SelectionReject,
                    Message = "NDS2: Selection - Reject",
                    Description = "Отклонение выборки"
                },
                new LogOperationInfo
                {
                    Operation = LogOperation.SelectionSendToSeod,
                    Message = "NDS2: Selection - Send to SEOD",
                    Description = "Отправка выборки"
                },
                new LogOperationInfo
                {
                    Operation = LogOperation.DiscrepancyAddCorrection,
                    Message = "NDS2: Discrepancy - Add correction",
                    Description = "Внесение уточнения по расхождению"
                },
                new LogOperationInfo
                {
                    Operation = LogOperation.DocumentCreate,
                    Message = "NDS2: Document - Create",
                    Description = "Создание автотребования"
                },
                new LogOperationInfo
                {
                    Operation = LogOperation.DocumentStatusChange,
                    Message = "NDS2: Document - Change status",
                    Description = "Изменение статуса автотребования"
                },
                new LogOperationInfo
                {
                    Operation = LogOperation.EfficeincyExcelMonitoringReport_2ME_UFNS,
                    Message = "NDS2: EfficeincyReport 2ME",
                    Description = "МЭ:Выгрузка отчета Форма №2"
                },
                new LogOperationInfo
                {
                    Operation = LogOperation.EfficeincyExcelMonitoringReportIFNSRating,
                    Message = "NDS2: EfficeincyReport IFNS",
                    Description = "МЭ:Выгрузка отчета рейтинга ИФНС"
                },
                new LogOperationInfo
                {
                    Operation = LogOperation.EfficeincyExcelMonitoringUnresilvedDiscrepancyReport,
                    Message = "NDS2: EfficeincyReport unresolved discrepancies",
                    Description = "МЭ:Выгрузка отчета ДНР"
                },
                new LogOperationInfo
                {
                    Operation = LogOperation.EfficeincyExcelMonitoringReportWeekly,
                    Message = "NDS2: EfficeincyReport weekly",
                    Description = "МЭ:Выгрузка отчета еженедельный"
                }
            };
            _operations = operations.AsReadOnly();

            var securityTemplates = new List<SecurityLogEntryTemplate>
            {
                new SecurityLogEntryTemplate(LogObject.SUBSYSTEM_ID, (int)LogOperation.SelectionCreate, "Создание выборки"),
                new SecurityLogEntryTemplate(LogObject.SUBSYSTEM_ID, (int)LogOperation.SelectionChange, "Изменение выборки"),
                new SecurityLogEntryTemplate(LogObject.SUBSYSTEM_ID, (int)LogOperation.SelectionRemove, "Удаление выборки"),
                new SecurityLogEntryTemplate(LogObject.SUBSYSTEM_ID, (int)LogOperation.SelectionToApprove, "Отправка выборки на согласование"),
                new SecurityLogEntryTemplate(LogObject.SUBSYSTEM_ID, (int)LogOperation.SelectionApproved, "Согласование выборки"),
                new SecurityLogEntryTemplate(LogObject.SUBSYSTEM_ID, (int)LogOperation.SelectionToCorrect, "Возврат выборки на доработку"),
                new SecurityLogEntryTemplate(LogObject.SUBSYSTEM_ID, (int)LogOperation.SelectionReject, "Отклонение выборки"),
                new SecurityLogEntryTemplate(LogObject.SUBSYSTEM_ID, (int)LogOperation.SelectionSendToSeod, "Отправка выборки"),
                new SecurityLogEntryTemplate(LogObject.SUBSYSTEM_ID, (int)LogOperation.DiscrepancyAddCorrection, "Внесение уточнения по расхождению"),
                new SecurityLogEntryTemplate(LogObject.SUBSYSTEM_ID, (int)LogOperation.DocumentCreate, "Создание автотребования"),
                new SecurityLogEntryTemplate(LogObject.SUBSYSTEM_ID, (int)LogOperation.DocumentStatusChange, "Изменение статуса автотребования"),
                new SecurityLogEntryTemplate(LogObject.SUBSYSTEM_ID, (int)LogOperation.EfficeincyExcelMonitoringReport_2ME_UFNS, "МЭ:Выгрузка отчета Форма №2"),
                new SecurityLogEntryTemplate(LogObject.SUBSYSTEM_ID, (int)LogOperation.EfficeincyExcelMonitoringReportIFNSRating, "МЭ:Выгрузка отчета рейтинга ИФНС"),
                new SecurityLogEntryTemplate(LogObject.SUBSYSTEM_ID, (int)LogOperation.EfficeincyExcelMonitoringUnresilvedDiscrepancyReport, "МЭ:Выгрузка отчета ДНР"),
                new SecurityLogEntryTemplate(LogObject.SUBSYSTEM_ID, (int)LogOperation.EfficeincyExcelMonitoringReportWeekly, "МЭ:Выгрузка отчета еженедельный"),
            };
            _securityTemplates = securityTemplates.AsReadOnly();
        }

        private static LogOperationInfo GetLogOperationInfo(this LogOperation index)
        {
            return _operations.Single(operation => operation.Operation == index);
        }

        private static SecurityLogEntryTemplate GetSecurityLogEntryTemplate(this LogOperation index)
        {
            return _securityTemplates.Single(template => template.EventID == (int)index);
        }

        public static void Write(this ISecurityLoggerService logger, LogObject obj, bool success, params KeyValuePair<string, string>[] parameters)
        {
            var entry = new SecurityLogEntry(obj.Operation.GetSecurityLogEntryTemplate()) { Success = success };
            if (obj.ObjectId.HasValue && obj.ObjectId.Value > 0)
            {
                entry.ObjectInfo = new ObjectInfo(entry.EventID, obj.ObjectId.Value.ToString());
            }
            if (parameters != null)
            {
                foreach (var parameter in parameters)
                {
                    entry.EventData.Add(parameter.Key, parameter.Value);
                }
            }
            logger.Write(entry);
        }

        public static void Write(this IInstrumentationService logger, LogObject obj, params KeyValuePair<string, object>[] parameters)
        {
            var operationInfo = obj.Operation.GetLogOperationInfo();
            var entry = logger.CreateProfilingEntry(TraceEventType.Information, (int)InstrumentationLevel.Normal, "NDS2:Profile-Security", 100);
            entry.MessageText = operationInfo.Message;
            entry.AddMonitoringItem("UserName", obj.UserName);
            entry.AddMonitoringItem("IssueDate", obj.IssueDate);
            entry.AddMonitoringItem("Operation", operationInfo.Description);
            if (obj.ObjectId.HasValue && obj.ObjectId > 0)
            {
                entry.AddMonitoringItem("ObjectId", obj.ObjectId.Value);
            }
            if (parameters != null)
            {
                foreach (var parameter in parameters)
                {
                    entry.AddMonitoringItem(parameter.Key, parameter.Value);
                }
            }
            logger.Write(entry);
        }

    }
}