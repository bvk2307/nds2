﻿using System;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO;

namespace Luxoft.NDS2.Common.Contracts.Services
{
    public interface ILoadTestHelpService
    {
        OperationResult<List<long>> GetDeclarationForBookLoad(string sonoCode, int chapterToLoad, int countOfData);

        OperationResult<List<string>> GetQueryText(string scope, string qName);
    }
}