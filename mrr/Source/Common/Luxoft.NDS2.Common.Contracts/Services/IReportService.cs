﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Report;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;

namespace Luxoft.NDS2.Common.Contracts.Services
{
    public interface IReportService
    {
        ReportDocumentResult CreateReport(string templateName, DataSet data);

        OperationResult<PageResult<InspectorMonitoringWork>> SearchInspectorMonitoringWork(QueryConditions criteria, long taskId);
        OperationResult<PageResult<LoadingInspection>> SearchLoadingInspection(QueryConditions criteria, long taskId);
        OperationResult<PageResult<MonitoringProcessingDeclaration>> SearchMonitoringProcessingDeclaration(QueryConditions criteria, long taskId);
        OperationResult<PageResult<CheckControlRatio>> SearchCheckControlRatio(QueryConditions criteria, long taskId);
        OperationResult<PageResult<MatchingRule>> SearchMatchingRule(QueryConditions criteria, long taskId);
        OperationResult<PageResult<CheckLogicControl>> SearchCheckLogicControl(QueryConditions criteria, long taskId);
        OperationResult<PageResult<DeclarationStatistic>> SearchDeclarationStatistic(QueryConditions criteria, long taskId);
        OperationResult<PageResult<DynamicTechnoParameter>> SearchDynamicTechnoParameter(QueryConditions criteria, long taskId);
        OperationResult<List<DynamicParameter>> SearchDictionaryDynamicParameters(DynamicModuleType moduleType);
        OperationResult<List<DateTime>> GetDatesTechnoReport();
        OperationResult<PageResult<DynamicDeclarationStatistic>> SearchDynamicDeclarationStatistic(QueryConditions criteria, long taskId);
        OperationResult<PageResult<InfoResultsOfMatching>> SearchInfoResultsOfMatching(QueryConditions criteria, long taskId);

        OperationResult<long> SaveTask(QueryConditions criteria, long requestId, int reportNum);
        OperationResult<TaskStatus> GetTaskStatus(long taskId);
        OperationResult RunCreateReport(int reportNum, long taskId, long requestId);
        OperationResult<long> StartBuildReport(QueryConditions criteria, int reportNum);
        OperationResult<DataRequestStatusWitnInitialize> CheckBuildReport(long requestId);
    }
}
