﻿using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Map;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Common.Contracts.Services
{
    public interface IMacroReportDataService
    {
        OperationResult<List<MapResponseDataNds>> GetMapDataNds(MapRequestData model);
        OperationResult<List<MapResponseDataDiscrepancy>> GetMapDataDiscrepancy(MapRequestData model);
        OperationResult<PageResult<NpResponseData>> GetNpData(QueryConditions queryConditions, bool withTotal);
    }
}
