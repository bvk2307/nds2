﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using System.Collections.Generic;

namespace Luxoft.NDS2.Common.Contracts.Services
{
    /// <summary>
    /// Сервис работы с данными счетов-фактур
    /// </summary>
    public interface IInvoiceDataService
    {
        /// <summary>
        /// Получение данных счетов-фактур из раздела декларации
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        OperationResult<DeclarationChapterData> SearchInDeclarationChapter(
            DeclarationChapterDataRequest request);

        /// <summary>
        /// Возвращает набор номеров таможенных деклараций по ключу СФ
        /// </summary>
        /// <param name="invoiceRowKey">Ключ СФ</param>
        /// <returns>Набор номеров</returns>
        OperationResult<List<string>> SearchCustomDeclarationNumbers(string invoiceRowKey);
    }
}
