﻿using System;
using Luxoft.NDS2.Common.Contracts.DTO;

using Luxoft.NDS2.Common.Contracts.DTO.Business.ControlRatio;
using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;

namespace Luxoft.NDS2.Common.Contracts.Services
{
    public interface IDiscrepancyDocumentService
    {
        OperationResult<PageResult<DiscrepancyDocumentInfo>> GetDocumentInfo(QueryConditions qc);

        OperationResult<PageResult<ControlRatio>> GetClaimControlRatio(long claimId, QueryConditions conditions);

        OperationResult<DiscrepancyDocumentInfo> GetDocument(long id);
        OperationResult<PageResult<DiscrepancyDocumentInvoice>> GetInvoices(long docId, QueryConditions conditions);
        OperationResult<PageResult<InvoiceRelated>> GetInvoicesRelated(long docId, QueryConditions conditions);

        /// <summary>
        /// Запрашивает информацию по кол-ву и сумме расхождений для АТ по КС
        /// </summary>
        /// <param name="docId">Идентификатор АТ</param>
        /// <returns>объект с кол-вом и суммами расхождения для АТ по КС</returns>
        OperationResult<DocumentCalculateInfo> GetDocumentCalculateInfo(long docId);

        /// <summary>
        /// Запрашивает информацию по кол-ву и сумме расхождений для АТ по СФ
        /// </summary>
        /// <param name="docId">Идентификатор АТ</param>
        /// <returns>объект с кол-вом и суммами расхождения для АТ по СФ</returns>
        OperationResult<DocumentCalculateInfo> GetDocumentCalculateInfoClaimSF(long docId);

        /// <summary>
        /// Запрашивает информацию по кол-ву и сумме расхождений для АИ
        /// </summary>
        /// <param name="docId">Идентификатор АИ</param>
        /// <returns>объект с кол-вом и суммами расхождения для АИ</returns>
        OperationResult<DocumentCalculateInfo> GetDocumentCalculateInfoReclaim(long docId);

        OperationResult<List<DocumentStatusHistory>> GetStatusHistories(long docId);
        OperationResult<long> GetDiscrepancyId(string invoiceId, int typeCode, long docId);
        OperationResult UpdateUserComment(long docId, string userComment);
        OperationResult<PageResult<NotReflectedInvoice>> GetNotReflectedInvoices(long docId, QueryConditions qc);
        OperationResult<DeclarationSummary> GetDeclaration(long id);
    }
}