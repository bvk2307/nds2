﻿using System;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.BankAccounts;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Contragents;
using Luxoft.NDS2.Common.Contracts.DTO.Business.ControlRatio;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.TaxPayer;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration.Results;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting.TaxPayer;
using Luxoft.NDS2.Common.Contracts.DTO.UserTask;
using Luxoft.NDS2.Common.Contracts.DTO.KnpDocuments;

namespace Luxoft.NDS2.Common.Contracts.Services
{
    public interface IDeclarationsDataService
    {
        # region Назначение НД на инспектора

        /// <summary>
        /// Назначает декларацию на пользователя (инспектор)
        /// </summary>
        /// <param name="declarationKey">Ключ группы корректировок деклараций\журналов</param>
        /// <param name="assignToSid">SID инспектора, на которого следует назначить декларацию</param>
        /// <returns>Статус выполнения операции</returns>
        OperationResult AssignDeclaration(DeclarationSummaryKey declarationKey, string assignToSid);

        # endregion


        // общие операции для декларации
        OperationResult<Object> GetUserDisplayName();        

        
        // список НД
        OperationResult<PageResult<DeclarationSummary>> SelectDeclarations(QueryConditions conditions);

        OperationResult<PageResult<TaxPayerDeclarationVersion>> SelectTaxPayerDeclarations(QueryConditions query);

        // список НД для арм инспектора
        OperationResult<PageResult<DeclarationBrief>> SelectDeclarationsForInspector(QueryConditions conditions);
        OperationResult<Dictionary<string, string>> GetTipsForMarks();
        
        // карточка НД
        OperationResult<DeclarationSummary> GetDeclaration(long id);
        OperationResult<DeclarationSummary> GetDeclaration(string inn, string kppEffective, string year, string period, int typeCode);
        OperationResult<DeclarationSummary> GetDeclaration(DeclarationKey key);
        /// <summary>
        /// Возвращает данные актуальной корректировки НД по ключу любой корректировки
        /// </summary>
        /// <param name="key">ключ любой корректировки НД</param>
        /// <returns></returns>
        OperationResult<DeclarationSummary> GetActualDeclaration(DeclarationKey key);

        /// <summary>
        /// Возвращает zip последней корректировки, наследующей указанный раздел
        /// </summary>
        /// <param name="zip">zip корректировки где был подан указанный раздел</param>
        /// <param name="chapter">номер раздела НД</param>
        /// <returns>zip последней корректировки, наследующей указанный раздел</returns>
        OperationResult<long> GetDeclarationLastZipInheritChapter(long zip, InvoiceRowKeyPartitionNumber chapter);

        OperationResult<PageResult<ControlRatio>> GetDeclarationControlRatio(long declarationVersionId, QueryConditions conditions);

        OperationResult<DeclarationRankedZip> GetContractorDeclarationZip(long inn, int year, int month);

        OperationResult<PageResult<ContragentSummary>> GetContragentsList(
            string inn, 
            string innReorganized, 
            string kppEffective, 
            string year, 
            string period, 
            int typeCode, 
            int chapter, 
            QueryConditions conditions, 
            bool needCount);
        OperationResult<PageResult<ContragentParamsSummary>> GetContragentParams(
            string inn, 
            string innReorganized, 
            string kppEffective, 
            string year, 
            string period, 
            int typeCode,
            string contragentInn, 
            int chapter, 
            QueryConditions conditions);
        OperationResult<ContragentsSovOperation> GetContragentsDataSov(ContragentsSovOperation param);
        OperationResult<ContragentsSovOperation> GetContragentParamsDataSov(ContragentsSovOperation arg);
        OperationResult<DeclarationSummary> SearchLastFullLoadedCorrection(string innContractor, string kppEffective, string period, string year, int typeCode);

        OperationResult<Dictionary<int, int>> GetChaptersSellers(long declId, string Inn);
        OperationResult<Dictionary<int, int>> GetChaptersBuyers(long declId, string Inn);
        OperationResult<ConfigExportInvoice> GetConfigExportInvoice();
        
        // Карточка НП
        OperationResult<TaxPayer> GetTaxPayerByKppEffective(string inn, string kpp);
        OperationResult<TaxPayer> GetTaxPayerByKppOriginal(string inn, string kpp);
        OperationResult<TaxPayer> GetTaxPayerByInn(string inn);
        OperationResult<DeclarationSummary> SearchLastDeclaration(string inn, string kpp);
        OperationResult<Object> GetTaxPayerSUR(string inn, string kpp);
        OperationResult<PageResult<BankAccount>> SelectULBankAccounts(QueryConditions criteriaSearch);
        OperationResult<PageResult<BankAccount>> SelectFLBankAccounts(QueryConditions criteriaSearch);
        OperationResult<ResetMarkResult> ResetMark(string innContractor, string kppEffective, int type, string period, string year);

        OperationResult<PageResult<BankAccountRequestSummary>> SelectBankAccountRequests(QueryConditions criteriaSearch, string inn, string kpp);

        OperationResult<PageResult<BankAccountBrief>> SelectFLBankAccountBriefs(string inn, DateTime begin, DateTime end, QueryConditions criteria);

        OperationResult<PageResult<BankAccountBrief>> SelectULBankAccountBriefs(string inn, string kpp, DateTime begin, DateTime end, QueryConditions criteria);

        OperationResult<List<DeclarationPeriod>> GetAvailablePeriods(string inn, string kpp);

        OperationResult<bool> PushBankAccountRequest(BankAccountManualRequest request);
        OperationResult<List<long>> FindSentBankAccountRequest(string inn, string kppEffective, string periodCode, string fiscalYear);
        OperationResult<List<string>> GetRequestedAccounts(string bik, DateTime begin, DateTime end);

        OperationResult<List<string>> GetUserStructContexts(string context);
        OperationResult<TaxPayerStatus> GetTaxPayerSolvencyStatus(string inn, string kppEffective);

        /// <summary>
        /// Проверка актуальности версии данных сопоставления
        /// </summary>
        /// <param name="count">номер текущей версии сопоставления хранящийся у клиента</param>
        /// <returns>
        /// true - данные сопоставления актуальны
        /// false - данные сопоставления устарели
        /// </returns>
        OperationResult<bool> VerifyComparasionDataVersionActuality(long count);

        /// <summary>
        /// Проверка проведения регламентных работ в системе
        /// (например идет выгрузка результатов сопоставления данных)
        /// </summary>
        /// <returns>
        /// true - регламентные работы не проводяться
        /// false - проводятся регламентные работы
        /// </returns>
        OperationResult<bool> VerifySystemSupportInProcess();

        /// <summary>
        /// Загружает сведения о корректировках деклараци, поданных НП за себя либо реорганизованное лицо в определенный налоговый период
        /// </summary>
        /// <param name="innDeclarant">ИНН декларанта</param>
        /// <param name="innReorganized">ИНН реорганизованного лица</param>
        /// <param name="kppEffective">КПП эффективный</param>
        /// <param name="fiscalYear">Год налогового периода</param>
        /// <param name="periodEffective">Код налогового периода</param>
        /// <returns>Данные о корректировках</returns>
        OperationResult<KnpDocumentsData> KnpDocuments(
            string innDeclarant, 
            string innReorganized, 
            string kppEffective, 
            int fiscalYear, 
            int periodEffective);
    }
}