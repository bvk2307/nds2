﻿using System;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Common.Contracts.Services
{
    /// <summary> Этот интерфейс описывает методы сервиса отчета "Пирамида" </summary>
    public interface IPyramidDataService
    {
        /// <summary> Запрашивает данные для узла дерева </summary>
        /// <param name="nodesKeyParameters"></param>
        /// <returns>Возвращает иерархию НП</returns>
        OperationResult<IReadOnlyCollection<GraphData>> LoadGraphNodes( GraphNodesKeyParameters nodesKeyParameters );

        /// <summary> Запрашивает данные для пакета узлов дерева для отчёта </summary>
        /// <param name="nodesKeyParameters"></param>
        /// <returns> Данные для пакета узлов дерева для отчёта </returns>
        OperationResult<IReadOnlyCollection<GraphData>> LoadReportGraphNodes( GraphNodesKeyParameters nodesKeyParameters );

        OperationResult<PageResult<GraphData>> LoadGraphNodeChildrenByPage( GraphNodesKeyParameters nodesKeyParameters );

        /// <summary> Запрашивает данные по суммам вычетов контрагента за предыдущие периоды. </summary>
        /// <param name="deductionKeyParameters"></param>
        /// <returns></returns>
        OperationResult<IReadOnlyCollection<DeductionDetail>> LoadDeductionDetails( DeductionDetailsKeyParameters deductionKeyParameters );

        /// <summary> Запрашивает КПП для НП по ИНН. </summary>
        /// <param name="inn"></param>
        /// <returns></returns>
        OperationResult<IReadOnlyCollection<string>> LoadTaxpayersKpp( string inn );
    }

    //apopov 16.6.2016	//TODO!!!   //remove with UI.Pyramid.Presenter
    [Obsolete( "There is not support for old Pyramid data service. Use IPyramidDataService instead of", error: false )]
    public interface IPyramidDataServiceOld
    {
        OperationResult<long> SubmitRequest( Request request );

        OperationResult<RequestStatusType> RequestStatus( long requestId );

        OperationResult<Contractor> LoadRoot( long declarationId );

        OperationResult<PageResult<Contractor>> LoadContractorsPage(
            long requestId,
            int offset,
            int pageSize,
            int minSum,
            int minNdsPercent );

        OperationResult<PageResult<ContractorRelationshipsDetail>> LoadRelationships(
            long requestId,
            QueryConditions criteria );

        OperationResult<List<Period>> AllPeriods();

        /// <summary> Запрашивает данные для узла дерева </summary>
        /// <param name="nodesKeyParameters"></param>
        /// <returns>Возвращает иерархию НП</returns>
        OperationResult<IReadOnlyCollection<GraphData>> LoadGraphNodes( GraphNodesKeyParameters nodesKeyParameters );

        /// <summary> Запрашивает данные для пакета узлов дерева для отчёта </summary>
        /// <param name="nodesKeyParameters"></param>
        /// <returns> Данные для пакета узлов дерева для отчёта </returns>
        OperationResult<IReadOnlyCollection<GraphData>> LoadReportGraphNodes( GraphNodesKeyParameters nodesKeyParameters );

        OperationResult<PageResult<GraphData>> LoadGraphNodeChildrenByPage( GraphNodesKeyParameters nodesKeyParameters );

        /// <summary> Used only by GraphTreeViewer utility. ATTENTION! Uses procedure "PAC$HRZ_REPORT_EXCEL.TAXPAYER_RELATION" and does not use "PAC$HRZ_REPORT.TAXPAYER_RELATION" which is different from other cases. </summary>
        /// <param name="nodesKeyParameters"></param>
        /// <returns></returns>
        OperationResult<PageResult<GraphData>> LoadGraphNodeChildren( GraphNodesKeyParameters nodesKeyParameters );

        /// <summary> Запрашивает данные по суммам вычетов контрагента за предыдущие периоды. </summary>
        /// <param name="deductionKeyParameters"></param>
        /// <returns></returns>
        OperationResult<IReadOnlyCollection<DeductionDetail>> LoadDeductionDetails( DeductionDetailsKeyParameters deductionKeyParameters );

        /// <summary> Запрашивает КПП для НП по ИНН. </summary>
        /// <param name="inn"></param>
        /// <returns></returns>
        OperationResult<IReadOnlyCollection<string>> LoadTaxpayersKpp( string inn );
    }
}
