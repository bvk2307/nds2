﻿using Luxoft.NDS2.Common.Contracts.DTO;

namespace Luxoft.NDS2.Common.Contracts.Services.KnpResultDocuments
{
    public interface IEditDocumentDiscrepancyService : IDocumentDiscrepancyService
    {
        /// <summary>
        /// Завершает отбор расхождений в документ
        /// </summary>
        /// <param name="documentId">Идентификатор документа</param>
        /// <returns></returns>
        OperationResult CloseDocument(long documentId);

        /// <summary>
        /// Изменяет сумму расхождения по документу
        /// </summary>
        /// <param name="id">Идентификатор расхождения</param>
        /// <param name="newAmount">Новое значение суммы</param>
        /// <returns></returns>
        OperationResult Update(long id, decimal newAmount);

        /// <summary>
        /// Вычисляет к-во открытых расхождений с некорректной суммой
        /// </summary>
        /// <param name="documentId">Идентификатор документа</param>
        /// <returns>количество расхождений с некорректной суммой</returns>
        OperationResult<int> CountAllInvalid(long documentId);

    }
}
