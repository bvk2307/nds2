﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.KnpResult;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Common.Contracts.Services.KnpResultDocuments
{
    public interface IDocumentDiscrepancyService
    {
        /// <summary>
        /// Создает документ по итогам КНП
        /// </summary>
        /// <param name="innDeclarant">ИНН декларанта</param>
        /// <param name="inn">ИНН реорганизованного лица при подаче НД за реорганизованное лицо иначе ИНН декларанта</param>
        /// <param name="kppEffective">Эффективный КПП декларанта</param>
        /// <param name="year">Год налогового периода</param>
        /// <param name="periodCode">Код налогового периода</param>
        /// <returns>Идентификатор документа</returns>
        OperationResult<long> CreateDocument(
            string innDeclarant,
            string inn,
            string kpp,
            string kppEffective,
            int year,
            string periodCode);

        /// <summary>
        /// Возвращает данные для отображения 1 страницы с данными расхождений
        /// </summary>
        /// <param name="documentId">Идентификатор документа</param>
        /// <param name="queryConditions">Параметры выборки данных</param>
        /// <returns>Данные о расхождениях в составе документа</returns>
        OperationResult<KnpResultDiscrepancyPage> Search(long documentId, QueryConditions queryConditions);

        /// <summary>
        /// Возвращает данные для отображения 1 страницы с несохраненными данными расхождений
        /// </summary>
        /// <param name="documentId">Идентификатор документа</param>
        /// <param name="documentType">Тип документа</param>
        /// <param name="queryConditions">Параметры выборки данных</param>
        /// <returns>Данные о расхождениях в составе документа</returns>
        OperationResult<KnpResultDiscrepancyPage> SearchUncommited(
            long documentId,
            QueryConditions queryConditions);

        /// <summary>
        /// Включает 1 расхождение в состав документа
        /// </summary>
        /// <param name="documentId">Идентификатор документа</param>
        /// <param name="documentType">Тип документа</param>
        /// <param name="id">Идентификатор расхождения</param>
        /// <returns></returns>
        OperationResult Include(long documentId, long id);

        /// <summary>
        /// Исключает 1 расхождение из состава документа
        /// </summary>
        /// <param name="documentId">Идентификатор документа</param>
        /// <param name="documentType">Тип документа</param>
        /// <param name="id">Идентификатор расхождения</param>
        /// <returns></returns>
        OperationResult Exclude(long documentId, long id);

        /// <summary>
        /// Включает все открытые расхождения в состав документа, удовлетворяющие критерию
        /// </summary>
        /// <param name="documentId">Идентификатор документа</param>
        /// <param name="criteria">Критерий отбора расхождений</param>
        /// <returns>Суммарные данные по расхождениям в документе после выполнения операции</returns>
        OperationResult<KnpResultDiscrepancySummary> IncludeAll(long documentId, FilterQuery[] criteria);

        /// <summary>
        ///Исключает все открытые расхождения из состава документа, удовлетворяющие критерию
        /// </summary>
        /// <param name="documentId">Идентификатор документа</param>
        /// <param name="criteria">Критерий отбора расхождений</param>
        /// <returns>Суммарные данные по расхождениям в документе после выполнения операции</returns>
        OperationResult<KnpResultDiscrepancySummary> ExcludeAll(long documentId, FilterQuery[] criteria);
    }
}
