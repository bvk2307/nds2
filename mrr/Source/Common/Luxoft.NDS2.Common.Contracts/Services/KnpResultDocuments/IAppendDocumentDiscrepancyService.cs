﻿using Luxoft.NDS2.Common.Contracts.DTO;

namespace Luxoft.NDS2.Common.Contracts.Services.KnpResultDocuments
{
    public interface IAppendDocumentDiscrepancyService : IDocumentDiscrepancyService
    {
        /// <summary>
        /// Стартует сеанс отбора расхождений в документ (либо возобновляет ранее стартованный)
        /// </summary>
        /// <param name="documentId">Идентификатор документа</param>
        /// <returns>Дата и время окончания жизни сессии</returns>
        OperationResult<int> StartSession(long documentId);

        /// <summary>
        /// Продливает время жизни сессии отбора расхождений в документ
        /// </summary>
        /// <param name="documentId">Идентификатор документа</param>
        /// <returns></returns>
        OperationResult<int> ExtendSession(long documentId);

        /// <summary>
        /// Сохранение изменений, сделанных в документе
        /// </summary>
        /// <param name="documentId">Идентификатор документа</param>
        /// <returns></returns>
        OperationResult Commit(long documentId);

        /// <summary>
        /// Откат изменений, сделанных в документе
        /// </summary>
        /// <param name="documentId">Идентификатор документа</param>
        /// <returns></returns>
        OperationResult Rollback(long documentId);

    }
}
