﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.AccessRestriction;
using Luxoft.NDS2.Common.Contracts.DTO.KnpResult;
using Luxoft.NDS2.Common.Contracts.DTO.TaxPeriod;
using System;

namespace Luxoft.NDS2.Common.Contracts.Services.KnpResultDocuments
{
    /// <summary>
    /// Этот интерфейс описывает сервис данных отчета по Актам и Решениям
    /// </summary>
    public interface IActDecisionReportService
    {
        /// <summary>
        /// Возвращает идентификатор агрегата с данными отчета об актах и решениях за определенную дату и налоговый период
        /// </summary>
        /// <param name="reportDate">Дата отчета</param>
        /// <param name="fiscalYear">Год налогового периода</param>
        /// <param name="quarter">Квартал налогового периода</param>
        /// <returns>Идентификатор агрегата</returns>
        OperationResult<long> ReportId(DateTime reportDate, int fiscalYear, int quarter);

        /// <summary>
        /// Возвращает данные отчета об Актах и Решениях с детализацией по стране / федеральным округам / регионам
        /// </summary>
        /// <param name="reportId">Идентификатор отчета</param>
        /// <returns>Массив с данными отчета с детализацией по региону</returns>
        OperationResult<KnpResultReportSummary[]> DataByRegion(long reportId);

        /// <summary>
        /// Возвращает данные отчета об Актах и Решениях с детализацией по инспекции
        /// </summary>
        /// <param name="reportId">Идентификатор отчета</param>
        /// <param name="regionCodes">Перечень кодов регионов, по которым необходимо построить отчет (игнорируется если задан перечень инспекций)</param>
        /// <param name="sonoCodes">Перечень кодов инспекций, по которым необходимо построить отчет</param>
        /// <returns>Массив с данными отчета с детализацией по инспекции</returns>
        OperationResult<KnpResultReportSummary[]> DataBySono(long reportId, string[] regionCodes, string[] sonoCodes);

        /// <summary>
        /// Возвращает набор ограничений текущего пользователя для данного отчета
        /// </summary>
        OperationResult<AccessContext> GetAccesContext();

        /// <summary>
        /// Возвращает перечень кварталов (налоговых периодов), для которых построены агрегаты отчета
        /// </summary>
        /// <returns>Массив налоговых периодов (кварталов)</returns>
        OperationResult<EffectiveTaxPeriod[]> GetAvailableTaxPeriods();
    }
}
