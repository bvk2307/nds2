﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.SystemSettings;

namespace Luxoft.NDS2.Common.Contracts.Services
{
    /// <summary>
    /// Этот интрефейс описывает сервис работы с параметрами АСК НДС-2
    /// </summary>
    public interface ISystemSettingsService
    {
        /// <summary>
        /// Вычитывает данные настроек АСК НДС-2
        /// </summary>
        /// <returns>Объект с данными</returns>
        OperationResult<SystemSettings> Load();

        /// <summary>
        /// Сохраняет данные настроек АСК НДС-2
        /// </summary>
        /// <param name="settingsData">Данные с измененными значениями</param>
        /// <returns>Результат выполнения операции</returns>
        OperationResult Save(SystemSettings settingsData);
    }
}
