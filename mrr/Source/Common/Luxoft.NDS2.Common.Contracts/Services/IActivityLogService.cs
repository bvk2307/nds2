﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting;

namespace Luxoft.NDS2.Common.Contracts.Services
{
    public interface IActivityLogService
    {
        OperationResult Write(string sid, ActivityLogEntry entry);
    }
}