﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.AccessRestriction;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.Services
{
    public interface IExplainReplyDataService
    {
        OperationResult<ExplainReplyInfo> GetExplainReplyInfo(long id);

        [Obsolete]
        OperationResult<List<ExplainReplyInfo>> GetExplainReplyList(long docId, QueryConditions conditions);

        OperationResult<List<ExplainReplyInfo>> GetListExplainsOfDocument(long docId);

        OperationResult<bool> IsPreviousExplainsOfDocumentInWorked(ExplainReplyInfo explainReplyInfo);

        [Obsolete]
        OperationResult UpdateUserComment(long explainId, string userComment);

        OperationResult<List<ExplainReplyHistoryStatus>> GetExplainReplyHistoryStatues(long explain_id);

        OperationResult<DeclarationSummary> GetDeclaration(long id);

        OperationResult UpdateInvoiceCorrect(long explainId, ExplainInvoiceCorrect correct, InvoiceCorrectTypeOperation typeOperation);

        OperationResult UpdateExplainStatus(long explainId, int explainStatusId);

        OperationResult UpdateInvoiceCorrectState(long explainId, string invoiceId, ExplainInvoiceStateInThis state);

        OperationResult DeleteInvoiceCorrectState(long explainId, string invoiceId);

        OperationResult DeleteAllInvoiceCorrectState(long explainId);

        OperationResult<bool> ExistsInvoiceChangeOfExplain(long explainId);

        OperationResult SendExplainXMLtoMC(long explainId, long declId);

        OperationResult<List<ExplainControlRatio>> GetExplainControlRatio(long zip);

        OperationResult<AccessContext> GetAccessContext();

        /// <summary>
        /// Запрашивает данные пояснений переданных в ответ на требование (АТ)
        /// </summary>
        /// <param name="claimId">Идентификатор АТ</param>
        /// <returns>Массим данных пояснений</returns>
        OperationResult<List<ClaimExplain>> SearchByClaim(long claimId);

        /// <summary>
        /// Запрашивает данные пояснения
        /// </summary>
        /// <param name="id">Идентификатор пояснения</param>
        /// <param name="explainType">Тип пояснения (в зависимости, от которого загружается комментарий)</param>
        /// <returns>Данные пояснения</returns>
        OperationResult<ExplainDetailsData> GetExplainDetails(long id, ExplainType explainType);

        /// <summary>
        /// Изменяет текст комментария пользователя к пояснению НП
        /// </summary>
        /// <param name="explainId">Идентификатор пояснения</param>
        /// <param name="type">Тип пояснения</param>
        /// <param name="text">Новый текст комментария</param>
        /// <returns>Статус выполнения операции</returns>
        OperationResult UpdateComment(long explainId, ExplainType type, string text);

    }
}
