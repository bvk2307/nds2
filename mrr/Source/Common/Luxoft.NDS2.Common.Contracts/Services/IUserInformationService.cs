﻿using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO;

namespace Luxoft.NDS2.Common.Contracts.Services
{
    public interface IUserInformationService
    {
        OperationResult<List<UserInformation>> GetInspectorListBySono(string sonoCode);
    }
}
