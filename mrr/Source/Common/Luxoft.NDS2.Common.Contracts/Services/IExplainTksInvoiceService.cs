﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Common.Contracts.Services
{
    public interface IExplainTksInvoiceService
    {
        OperationResult<PageResult<ExplainTksInvoiceChapter8>> FindInChapter8(
            long explainId,
            QueryConditions queryConditions);

        OperationResult<PageResult<ExplainTksInvoiceChapter9>> FindInChapter9(
            long explainId,
            QueryConditions queryConditions);

        OperationResult<PageResult<ExplainTksInvoiceChapter10>> FindInChapter10(
            long explainId,
            QueryConditions queryConditions);

        OperationResult<PageResult<ExplainTksInvoiceChapter11>> FindInChapter11(
            long explainId,
            QueryConditions queryConditions);

        OperationResult<PageResult<ExplainTksInvoiceChapter12>> FindInChapter12(
            long explainId,
            QueryConditions queryConditions);

        OperationResult<PageResult<ExplainTksInvoiceConfirmed>> FindConfirmedInvoices(
            long explainZip,
            QueryConditions queryConditions);

        OperationResult<PageResult<ExplainTksInvoiceUnconfirmed>> FindUnconfirmedInvoices(
            long explainZip,
            QueryConditions queryConditions);
    }
}
