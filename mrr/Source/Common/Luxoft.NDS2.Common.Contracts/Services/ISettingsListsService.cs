﻿using System;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.DTO.SystemSettings;

namespace Luxoft.NDS2.Common.Contracts.Services
{
    /// <summary>
    /// Этот интрефейс описывает сервис работы с параметрами АСК НДС-2
    /// </summary>
    public interface ISettingsListsService
    {
        /// <summary>
        /// Возвращает список БС
        /// </summary>
        OperationResult<List<SystemSettingsList>> All(QueryConditions qc);

        /// <summary>
        /// Возвращает список НМ
        /// </summary>
        OperationResult<SystemSettingsList> LoadTaxMonitorList();


        /// <summary>
        /// Возвращает список ИВ
        /// </summary>
        OperationResult<SystemSettingsList> LoadExcludeList();

        /// <summary>
        /// Возвращает список INN
        /// </summary>
        OperationResult<string[]> LoadInnList(long id);

        /// <summary>
        /// Сохраняет список НМ
        /// </summary>
        /// <param name="list">список</param>
        OperationResult SaveTaxMonitorList(SystemSettingsList list);

        /// <summary>
        /// Сохраняет список ИВ
        /// </summary>
        /// <param name="list">список</param>
        OperationResult SaveExcludeList(SystemSettingsList list);

        /// <summary>
        /// Сохраняет список БС
        /// </summary>
        OperationResult SaveWhiteList(SystemSettingsList list);

        /// <summary>
        /// Активирует БС
        /// </summary>
        /// <param name="id">идентификатор БС</param>
        /// <returns></returns>
        OperationResult Activate(long id);

        /// <summary>
        /// Деакстивирует БС
        /// </summary>
        /// <param name="id">идентификатор БС</param>
        /// <returns></returns>
        OperationResult Deactivate(long id);

        /// <summary>
        /// Возвращает имя текущего юзера
        /// </summary>
        /// <returns></returns>
        OperationResult<Object> GetUserDisplayName();

        /// <summary>
        /// Синхронизирует БС В Hive и Oracle
        /// </summary>
        /// <returns></returns>
        OperationResult HiveSyncWhiteList();
    }
}
