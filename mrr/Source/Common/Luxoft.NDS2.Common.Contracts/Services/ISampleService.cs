﻿namespace Luxoft.NDS2.Common.Contracts.Services
{
    public interface ISampleService
    {
        string PingAdmin(string input);
        string PingInspector(string input);
        string PingManager(string input);
        void RaiseExceptionOnServer();

        int PingDatabase();
    }
}
