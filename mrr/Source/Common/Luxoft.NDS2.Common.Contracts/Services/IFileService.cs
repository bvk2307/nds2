﻿namespace Luxoft.NDS2.Common.Contracts.Services
{
    /// <summary>
    /// Сервис для загрузки файлов с сервера
    /// </summary>
    public interface IFileService
    {
        /// <summary>
        /// Получить размер файла
        /// </summary>
        /// <param name="path">Полный путь к файлу на сервере</param>
        /// <returns>Размер файла, -1 - если файл не найден</returns>
        long GetSize(string path);

        /// <summary>
        /// Получение блока (части) файла
        /// </summary>
        /// <param name="path">Полный путь к файлу на сервер</param>
        /// <param name="offset">Сдвиг от начала файла (количество уже принятых байт)</param>
        /// <param name="bufferSize">Размер блока</param>
        /// <returns>Масиив байт</returns>
        byte[] GetChunk(string path, int offset, int bufferSize);
    }
}
