﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;

namespace Luxoft.NDS2.Common.Contracts.Services
{
    public interface IEmployeeDialogDataService
    {
        OperationResult<EmployeeData> Load(long objectId, EmployeeType type);

        OperationResult Save(Region region, EmployeeData data);

        OperationResult<List<string>> GetSIDsByCode(EmployeeType type, string code);
    }
}