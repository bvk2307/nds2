﻿using Luxoft.NDS2.Common.Contract.DTO.SearchEntity;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.OrganizationStructure;
using Luxoft.NDS2.Common.Contracts.DTO.TaxPeriod;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.Services
{
    public interface IDictionaryDataService
    {
        # region Территориальная и орг. структура

        OperationResult<Sono[]> GetSonoDictionary();

        OperationResult<Region[]> GetRegionDictionary();

        OperationResult<Luxoft.NDS2.Common.Contracts.DTO.OrganizationStructure.FederalDistrict[]> GetFederalDistrictDictionary();

        # endregion

        /// <summary>
        /// Получить описание справочника
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        OperationResult<NsiMetadataEntry> GetLookupMetadata(string name);

        /// <summary>
        /// Получить список значение найденных совпадений по запросу из справочника
        /// </summary>
        /// <param name="request">запрос поиска в справочнике</param>
        /// <returns>список найденных значений из справочника</returns>
        OperationResult<List<LookupResponse>> GetLookupData(LookupRequest request);

        /// <summary>
        /// Получить список колонок для справочников (имя таблицы, имя колонки, можно ли редактировать и т.д.)
        /// </summary>
        /// <returns>список колонок для справочников</returns>
        OperationResult<List<TableDictionaryColumnInfo>> GetTablesDictionaryColumnInfo();

        /// <summary>
        /// Получить список справочников (название таблицы, название для пользователя)
        /// </summary>
        /// <returns>список справочников</returns>
        OperationResult<List<TableDictionaryInfo>> GetTablesDictionaryInfo();

        /// <summary>
        /// Получить все строки справочника
        /// </summary>
        /// <param name="tableName">название таблицы справочника</param>
        /// <param name="columns">список необходимых колонок</param>
        /// <returns>список строк (в виде словаря колонка - значение)</returns>
        OperationResult<List<Dictionary<string, object>>> GetTableRows(string tableName, List<string> columns);

        /// <summary>
        /// Обновить строку справочника
        /// </summary>
        /// <param name="tableName">название таблицы справочника</param>
        /// <param name="key">название колонки-ключа таблицы (update по данному ключу)</param>
        /// <param name="value">значение ключа</param>
        /// <param name="row">список значений строки</param>
        /// <returns></returns>
        OperationResult UpdateTableRow(string tableName, string key, string value, Dictionary<string, string> row);

        /// <summary>
        /// Добавить строку справочника
        /// </summary>
        /// <param name="tableName">название таблицы справочника</param>
        /// <param name="row">список значений строки</param>
        /// <returns></returns>
        OperationResult AddTableRow(string tableName, Dictionary<string, string> row);

        /// <summary>
        /// Удвлить строку справочника (пометить на удаление)
        /// </summary>
        /// <param name="tableName">название таблицы справочника</param>
        /// <param name="row">название ключа</param>
        /// <param name="value">значение ключа</param>
        /// <returns></returns>
        OperationResult DeleteTableRow(string tableName, string key, string value);

        /// <summary>
        /// Загружает справочник СУР
        /// </summary>
        /// <returns>Результат выполнения загрузки справочника СУР</returns>
        OperationResult<List<SurCode>> GetSurDictionary();

        OperationResult<List<OperCode>> GetOperDictionary();
        OperationResult<List<OperCode>> GetCurrencyDictionary();
        OperationResult<List<OperCode>> GetBargainDictionary();

        /// <summary>
        /// Загружает справочник Причины аннулирования
        /// </summary>
        /// <returns>Результат выполнения загрузки справочника Причины аннулирования</returns>
        OperationResult<List<AnnulmentReason>> GetAnnulmentReasonDictionary();

        /// <summary>
        /// Загружает справочник налоговых периодов
        /// </summary>
        /// <returns>Результат выполнения загрузки справочника налоговых периодов</returns>
        OperationResult<List<DictTaxPeriod>> GetTaxPeriodDictionary();
    }
}
