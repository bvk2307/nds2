﻿using Luxoft.NDS2.Common.Contracts.DTO;
using System.Collections.Generic;

namespace Luxoft.NDS2.Common.Contracts.Services
{
    public interface IVersionInfoService
    {
        OperationResult<VersionInfo> GetLatest();

        OperationResult<IEnumerable<VersionInfo>> GetVersionsWithReleaseNotes();

        OperationResult<UpdateInfo> GetUpdateInfo();

        byte[] GetClientChunk(string path, int offset, int bufferSize);
    }
}
