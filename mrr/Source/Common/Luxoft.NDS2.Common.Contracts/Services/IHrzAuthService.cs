﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO.Horizon;
using Luxoft.NDS2.Common.Contracts.DTO.Security;

namespace Luxoft.NDS2.Common.Contracts.Services
{
    [ServiceContract]
    public interface  IHrzAuthService
    {
        [OperationContract]
        HorizonAuthorizationResult GetUserRights(string domain, string login, string pass);
    }
}
