﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Navigator;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System.Collections.Generic;

namespace Luxoft.NDS2.Common.Contracts.Services
{
    public interface INavigatorReportDataService
    {
        OperationResult<List<TaxPayer>> GetTaxPayers(LoadTaxPayersContract contract);

        OperationResult<int> CountTaxPayers(CountTaxPayersContract contract);

        OperationResult<long> SubmitRequest(Request request);

        OperationResult<RequestStatus> GetRequestStatus(long requestId);

        OperationResult<List<ChainSummary>> GetPairs(long requestId);

        OperationResult<List<ChainContractorData>> GetChainContractors(long pairId, long requestId);

        OperationResult<List<ChainContractorData>> GetChain(long chainId, long requestId);

        OperationResult<Dictionary<string, bool>> GetTaxPayersPurchaseStatus(string[] taxPayersInn, PeriodRange range);
    }
}
