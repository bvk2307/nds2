﻿using System;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO;

namespace Luxoft.NDS2.Common.Contracts.Services
{
    public interface ICalendarDataService
    {
        OperationResult<List<DateTime>> GetRedDays(DateTime begin, DateTime end);
        void AddRedDay(DateTime date);
        void RemoveRedDay(DateTime date);
        OperationResult<DateTime> AddWorkingDays(DateTime date, int daysToShift);
    }
}
