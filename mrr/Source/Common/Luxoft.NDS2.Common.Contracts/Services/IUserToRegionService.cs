﻿using System;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Common.Contracts.Services
{
    public interface IUserToRegionService
    {
        #region Old
        OperationResult<List<string>> GetUsers(string regionCode);
        #endregion

        #region New
        OperationResult<SounDataContainer> GetSounDialogData(Region region, QueryConditions conditions);
        OperationResult SaveSounDialogData(Region region);

        OperationResult<List<ResponsibilityRegion>> GetResponsibilityRegionData(int contextId, QueryConditions conditions);
        #endregion
    }
}