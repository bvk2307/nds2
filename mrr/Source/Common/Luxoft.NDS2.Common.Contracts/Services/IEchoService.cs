﻿using System;
using System.Collections.Generic;
using CommonComponents.Communication;
using Luxoft.NDS2.Common.Contracts.DTO;

namespace Luxoft.NDS2.Common.Contracts.Services
{
    public interface IEchoService
    {
        string PingDatabase();
        string PingAuthService();
        string PingSOV();
    }
}
