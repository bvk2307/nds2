﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Claims;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Common.Contracts.Services.Claims
{
    public interface IInvoiceClaimService
    {
        /// <summary>
        /// Возвращает список дат, в которые создавались АТ по СФ
        /// </summary>
        /// <returns>Список дат, упорядоченный по убыванию</returns>
        OperationResult<List<DateTime>> GetCreateDates();

        /// <summary>
        /// Возвращает страницу списка Ат по СФ, сформированных на дату с ограничением по статусу
        /// </summary>
        /// <param name="qc">Условия фильтрации и сортировки</param>
        /// <param name="date"></param>
        /// <param name="status"></param>
        /// <returns>Обертка операционного результата</returns>
        OperationResult<PageResult<InvoiceClaim>> GetClaims(QueryConditions qc, DateTime date, int []status);

        /// <summary>
        /// Возвращает страницу списка Ат по СФ, сформированных на дату с ограничением по статусу
        /// </summary>
        /// <param name="qc">Условия фильтрации и сортировки</param>
        /// <param name="date"></param>
        /// <returns>Обертка операционного результата</returns>
        OperationResult<PageResult<InvoiceClaim>> GetClaimsToSend(QueryConditions qc, DateTime date);

        /// <summary>
        /// Возвращает количество АТ, гтовых к отправке
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        OperationResult<int> GetCountClaimsToSend(DateTime date);

        /// <summary>
        /// Возвращает справочник статусов
        /// </summary>
        /// <returns>Справочник</returns>
        OperationResult<List<SimpleStatusDto>> GetStatusList();

        /// <summary>
        /// Получение информации об отправке на дату
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        OperationResult<DocSendStatus> GetSendStatus(DateTime date);

        /// <summary>
        /// Включает АТ в отправку
        /// </summary>
        /// <param name="docId"></param>
        /// <param name="username"></param>
        /// <returns></returns>
        OperationResult Include(long docId, string username);

        /// <summary>
        /// Исключает АТ из отправки
        /// </summary>
        /// <param name="docId"></param>
        /// <param name="username"></param>
        /// <returns></returns>
        OperationResult Exclude(long docId, string username);

        OperationResult Send(DateTime date, string username);
        
        OperationResult<DateTime> GetBaseDate();
    }
}
