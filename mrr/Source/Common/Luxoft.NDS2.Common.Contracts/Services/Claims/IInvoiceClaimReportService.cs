﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Claims;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Common.Contracts.Services.Claims
{
    public interface IInvoiceClaimReportService
    {
        /// <summary>
        /// Возвращает набор данных для отчета "Статистика по выборкам"
        /// </summary>
        /// <param name="date">Дата, на котороую строится отчет</param>
        /// <returns></returns>
        OperationResult<List<SelectionClaimCompare>> GetSelectionStats(DateTime date);

        /// <summary>
        /// Возвращает набор данных для отчета "Список АТ" к отправке
        /// </summary>
        /// <param name="date">Дата, на котороую строится отчет</param>
        /// <param name="filter">Фильтр списка АТ</param>
        /// <returns></returns>
        OperationResult<List<InvoiceClaimReportItem>> GetInvoiceClaimList(DateTime date, List<FilterQuery> filter);

        /// <summary>
        /// Возвращает набор данных для отчета "Список АТ" с фильтром по статусам
        /// </summary>
        /// <param name="date">Дата, на котороую строится отчет</param>
        /// <param name="statuses">Массив значений статусов</param>
        /// <param name="filter">Фильтр списка АТ</param>
        /// <returns></returns>
        OperationResult<List<InvoiceClaimReportItem>> GetInvoiceClaimListByStatus(DateTime date, int[] statuses,
            List<FilterQuery> filter);

        OperationResult<List<TaxMonitoringReportItem>> GetReportForChapters(long docId, int chapter);

        OperationResult<List<TaxMonitoringReportItem>> GetReportUnconfirmed(long docId);
    }
}
