﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Common.Contracts.Services
{
    public interface IExplainInvoiceService
    {
        OperationResult<PageResult<ExplainInvoice>> GetAllClaimInvoices(
            long explainId,
            int chapter,
            QueryConditions query);

        OperationResult<PageResult<ExplainInvoice>> GetModifiedInvoices(
            long explainId,
            int chapter,
            QueryConditions query);

        OperationResult<ExplainInvoice> GetOneExplainInvoice(
            ExplainReplyInfo explainInfo,
            ExplainRegim regim,
            string invoiceId);
    }
}
