﻿using System;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO;

using Luxoft.NDS2.Common.Contracts.DTO.Business.ControlRatio;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;

namespace Luxoft.NDS2.Common.Contracts.Services
{
    public interface IControlRatioService
    {
        OperationResult<DiscrepancyDocumentInfo> GetDocument(long ratioId);

        OperationResult<DeclarationSummary> GetDeclarationVersion(long? declarationId);

        OperationResult UpdateComment(long id, string comment);

        OperationResult<ControlRatio> GetControlRatio(long id);
    }
}