﻿using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Common.Contracts.Services
{
    /// <summary>
    /// Этот интерфейс описывает операции логирования
    /// </summary>
    public interface ILogProvider
    {
        /// <summary>
        /// Пишет уведомление в лог
        /// </summary>
        /// <param name="message">Текст уведомления</param>
        /// <param name="methodName">Имя метода-источника</param>
        /// <param name="additionalProperties"></param>
        void LogDebug(string message, string methodName = null, IList<KeyValuePair<string, object>> additionalProperties = null);

        /// <summary>
        /// Пишет уведомление в лог
        /// </summary>
        /// <param name="message">Текст уведомления</param>
        /// <param name="methodName">Имя метода-источника</param>
        /// <param name="additionalProperties"></param>
        void LogNotification(string message, string methodName = null, IList<KeyValuePair<string, object>> additionalProperties = null);

        /// <summary>
        /// Пишет ошибку в лог
        /// </summary>
        /// <param name="message">Текст ошибки</param>
        /// <param name="methodName">Имя метода-источника</param>
        /// <param name="ex">Исключение</param>
        /// <param name="additionalProperties"></param>
        void LogError(string message, string methodName = null, Exception ex = null, IList<KeyValuePair<string, object>> additionalProperties = null);

        /// <summary>
        /// Пишет предупреждение в лог
        /// </summary>
        /// <param name="message">Текст предупреждения</param>
        /// <param name="methodName">Имя метода-источника</param>
        /// <param name="additionalProperties"></param>
        void LogWarning(string message, string methodName = null, IList<KeyValuePair<string, object>> additionalProperties = null);
    }
}
