﻿using System;
using Luxoft.NDS2.Common.Contracts.DTO;

namespace Luxoft.NDS2.Common.Contracts.Services
{
    public interface IObjectLocker
    {
        OperationResult<Object> Lock(long objectId, long objectType, string hostName);
        OperationResult Unlock(string lockKey, string hostName);
        OperationResult<Object> CheckLock(long objectId, long objectType);
    }
}