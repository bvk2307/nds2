﻿using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting.TaxPayer;
using Luxoft.NDS2.Common.Contracts.DTO.Security;

namespace Luxoft.NDS2.Common.Contracts.Services
{
    /// <summary> Requests for user access permissions by user operations. </summary>
    public interface IUserPermissionRequestService
    {
        /// <summary> Is operation eligible for "soun" code <paramref name="sounCode"/>? </summary>
        /// <param name="operationIdentifier"></param>
        /// <param name="sounCode"></param>
        /// <returns></returns>
        OperationResult<UserPermissionRequestResult> 
            IsOperationEligibleByUserAccess(string operationIdentifier, string sounCode);

        /// <summary> Is operation eligible for some "soun" code from defined <paramref name="sounCodes"/>? </summary>
        /// <param name="operationIdentifier"></param>
        /// <param name="sounCodes"></param>
        /// <returns></returns>
        OperationResult<UserPermissionRequestResult> 
            IsOperationEligibleByUserAccess(string operationIdentifier, IEnumerable<string> sounCodes);

        /// <summary> Is operation eligible for a taxpayer? </summary>
        /// <param name="operationIdentifier"></param>
        /// <param name="taxpayerId"></param>
        /// <param name="period"></param>
        /// <returns></returns>
        OperationResult<UserPermissionRequestResult> 
            IsOperationEligibleByUserAccess(string operationIdentifier, TaxPayerId taxpayerId, Period period = null);
    }
}