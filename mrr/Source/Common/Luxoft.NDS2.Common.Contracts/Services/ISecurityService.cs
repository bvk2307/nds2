﻿using System;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Security;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.AccessRestriction;

namespace Luxoft.NDS2.Common.Contracts.Services
{
    public interface ISecurityService : IUserPermissionRequestService
    {
        [Obsolete("Переходим на GetAccesContext")]
        /// <summary>
        /// Возвращает список всех прав пользователя(операции, задачи, роли)
        /// </summary>
        /// <returns></returns>
        OperationResult<List<AccessRight>> GetUserPermissions();

        [Obsolete]
        /// <summary>
        /// Позволяет изменить роль пользователя в системе(режим отладки)
        /// </summary>
        /// <param name="roleName"></param>
        /// <returns></returns>
        OperationResult ChangeUserRole(string roleName);

        [Obsolete("Переходим на GetAccesContext")]
        /// <summary>
        /// возвращает список всех ролей в системе(режим отладки)
        /// </summary>
        /// <returns></returns>
        OperationResult<List<string>> GetSubsystemRoles();

        /// <summary>
        /// возвращает список пользователей в системе
        /// </summary>
        /// <returns></returns>
        [Obsolete("Переходим на GetAccesContext")]
        OperationResult<List<string>> GetSubsystemUsers();

        [Obsolete("Переходим на GetAccesContext")]
        OperationResult<UserAccessPolicy> GetUserAccess(string operationIdentifier);

        [Obsolete("Переходим на GetAccesContext")]
        OperationResult<ContractorDataPolicy> GetUserRestrictionConfiguration();

        OperationResult<string[]> ValidateAccess(string[] operations);

        /// <summary>
        /// возвращает список операций
        /// </summary>
        /// <returns></returns>
        OperationResult<string[]> ValidateAccessCsud(string[] operations);

        OperationResult<AccessContext> GetAccessContext(string[] operations);
    }
}
