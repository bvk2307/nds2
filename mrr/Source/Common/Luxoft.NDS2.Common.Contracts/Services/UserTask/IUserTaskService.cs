﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.DTO.UserTask;
using System;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;

namespace Luxoft.NDS2.Common.Contracts.Services.UserTask
{
    public interface IUserTaskService
    {
        /// <summary>
        /// права текущего user
        /// </summary>
        OperationResult<UserRights> GetUserRights();

        /// <summary>
        /// статистика по пз текущего user
        /// </summary>
        OperationResult<List<Summary>> GetSummaryList();

        /// <summary>
        /// поиск пз по заданным условиям
        /// </summary>
        /// <param name="queryContext">условия поиска</param>
        OperationResult<PageResult<UserTaskInList>> Search(QueryConditions queryContext, bool fullData);

        OperationResult<UserTaskInDetails> GetCardData(
            long userTaskId,
            int taskTypeId,
            long declarationZip
            );

        /// <summary>
        ///     Выставить статус платежеспособности
        /// </summary>
        OperationResult SetSolvency(long taskId, long solvencyStatusId, string comment);

        /// <summary>
        ///     Возвращает список всех возможных типов для ПЗ
        /// </summary>
        OperationResult<List<DictionaryItem>> GetUserTaskTypeDictionary();

        /// <summary>
        ///     Возвращает список всех возможных статусов для ПЗ
        /// </summary>
        OperationResult<List<DictionaryItem>> GetUserTaskStatusDictionary();

        /// <summary>
        ///     Устанавливает статус "Выполнено" ПЗ Ввод ответа на истребование
        /// </summary>
        OperationResult CompleteReclaimReplyTask(long explainId);
    }
}
