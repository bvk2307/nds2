﻿using System;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting;
using Luxoft.NDS2.Common.Contracts.DTO.UserTask;
using Luxoft.NDS2.Common.Contracts.DTO.AccessRestriction;

namespace Luxoft.NDS2.Common.Contracts.Services.UserTask
{
    public interface IUserTaskReportService
    {
        OperationResult<AccessContext> GetAccesContext();

        /// <summary>
        /// Ищет наличие рассчитанного агрегата отчета о ходе выполнения пользовательских заданий на дату
        /// </summary>
        /// <param name="reportDate">Дата построения отчета</param>
        /// <returns>ИД агрегата</returns>
        OperationResult<ReportIdSearchResult> GetReportId(DateTime reportDate);

        /// <summary>
        /// Возвращает данные агрегата отчета о выполнении пользовательских заданий с детализацией по пользователю (инспектору)
        /// </summary>
        /// <param name="reportId">ИД отчета</param>
        /// <param name="userSids">Массив идентификаторов пользователей</param>
        /// <param name="taskTypes">Массив идентификаторов типов пользвательских заданий (пустой массив трактуется как ВСЕ)</param>
        /// <returns>Данные агрегата отчета о выполнении пользовательских заданий, удовлетворяющие заданным критериям</returns>
        OperationResult<UserTaskInspectorSummary[]> ReportByUsers(
            long reportId, 
            string[] userSids,
            int[] taskTypes);

        /// <summary>
        /// Возвращает данные агрегата отчета о выполнении пользовательских заданий с детализацией по налоговому органу
        /// </summary>
        /// <param name="reportId">ИД отчета</param>
        /// <param name="regionCodes">Массив кодов регионов (пустой массив трактуется как ВСЕ)</param>
        /// <param name="taskTypes">Массив идентификаторов типов пользвательских заданий (пустой массив трактуется как ВСЕ)</param>
        /// <returns>Данные агрегата отчета о выполнении пользовательских заданий, удовлетворяющие заданным критериям</returns>
        OperationResult<UserTaskRegionSummary[]> ReportByRegion(
            long reportId, 
            string[] regionCodes,
            int[] taskTypes);

        /// <summary>
        /// Возвращает данные агрегата отчета о выполнении пользовательских заданий с детализацией по региону
        /// </summary>
        /// <param name="reportId">ИД отчета</param>
        /// <param name="regionCodes">Массив кодов регионов (пустой массив трактуется как ВСЕ)</param>
        /// <param name="taskTypes">Массив идентификаторов типов пользвательских заданий (пустой массив трактуется как ВСЕ)</param>
        /// <returns>Данные агрегата отчета о выполнении пользовательских заданий, удовлетворяющие заданным критериям</returns>
        OperationResult<UserTaskSonoSummary[]> ReportBySono(
            long reportId, 
            string[] regionCodes,
            int[] taskTypes);
    }
}
