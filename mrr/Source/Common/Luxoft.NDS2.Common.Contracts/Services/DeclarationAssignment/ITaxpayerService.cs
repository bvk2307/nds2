﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Common.Contracts.Services.DeclarationAssignment
{
    public interface ITaxpayerService
    {
        OperationResult<PageResult<Taxpayer>> GeTaxpayersList(QueryConditions qc);
    }
}
