﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Common.Contracts.Services.DeclarationAssignment
{
    public interface IInspectorDeclarationService
    {
        OperationResult<PageResult<InspetorDeclarationDistribution>> GetInspectorDeclationDistribution(QueryConditions qc);

        OperationResult<PageResult<InspectorDeclarationNotDistribution>> GetInspectorDeclationNotDistribution(QueryConditions searchCriteria);

        OperationResult AssignInspector(string sid, long declarationId);
    }
}
