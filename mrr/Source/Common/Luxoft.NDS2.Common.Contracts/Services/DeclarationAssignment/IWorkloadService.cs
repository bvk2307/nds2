﻿using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment;

namespace Luxoft.NDS2.Common.Contracts.Services.DeclarationAssignment
{
    public interface IWorkloadService
    {
        OperationResult<List<InspectorWorkload>> GetActiveWorkLoads();

        OperationResult<SetInspectorWorkLoadResult> SetWorkloadForInspectors(List<InspectorWorkload> inspectorWorkLoads);

        OperationResult UpdateWorkLoad(InspectorWorkload inspectorWorkload);

        OperationResult<ClearInspectorsWorkloadResult> ClearInspectorsWorkload(List<InspectorWorkload> workloads);
    }
}
