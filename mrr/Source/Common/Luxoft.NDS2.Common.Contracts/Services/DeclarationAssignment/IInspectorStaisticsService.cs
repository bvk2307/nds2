﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Common.Contracts.Services.DeclarationAssignment
{
    public interface IInspectorStatisticsService
    {
        OperationResult<List<InspectorStatistics>> GetActiveInspectorsStatistics();

        OperationResult<PageResult<InspectorStatistics>> GetFullInspectorsStatistics(QueryConditions qc);
    }
}
