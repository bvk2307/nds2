﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System.Collections.Generic;

namespace Luxoft.NDS2.Common.Contracts.Services.DeclarationAssignment
{
    public interface ITaxpayerAssignmentService
    {
        OperationResult<PageResult<TaxpayerAssignment>> GetTaxpayersAssigmentsList(QueryConditions qc);

        OperationResult<TaxpayersResult> AddTaxpayerAssignment(IEnumerable<Taxpayer> tList);

        OperationResult<TaxpayersAssignmentsResult> RemoveTaxpayerAssignment(IEnumerable<TaxpayerAssignment> taList);

        OperationResult<bool> CancelInspectorAssignment(TaxpayerAssignment ta);

        OperationResult<bool> AddInspectorAssignment(TaxpayerAssignment ta);
    }
}
