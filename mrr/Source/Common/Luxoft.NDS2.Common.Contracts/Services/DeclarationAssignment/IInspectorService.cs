using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment;

namespace Luxoft.NDS2.Common.Contracts.Services.DeclarationAssignment
{
    public interface IInspectorService
    {
        OperationResult<List<InspectorSonoAssignment>> GetFreeInspectors();
        OperationResult<InspectorSonoAssignment> GetInspector(string sid);
    }
}