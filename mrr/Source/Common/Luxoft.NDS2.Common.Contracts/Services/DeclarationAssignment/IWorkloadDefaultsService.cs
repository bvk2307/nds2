using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment;

namespace Luxoft.NDS2.Common.Contracts.Services.DeclarationAssignment
{
    public interface IWorkloadDefaultsService
    {
        OperationResult<DefaultInspectorWorkLoad> GetDefaultWorkLoad();

        OperationResult SetDefaultWorkLoad(DefaultInspectorWorkLoad defaultInspectorWorkLoad);
    }
}