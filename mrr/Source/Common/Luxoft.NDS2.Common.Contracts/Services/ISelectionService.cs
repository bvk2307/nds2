﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using Luxoft.NDS2.Common.Contracts.DTO.Selections;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;

namespace Luxoft.NDS2.Common.Contracts.Services
{
    /// <summary>
    ///     Этот интрефейс описывает сервис работы с выборками
    /// </summary>
    public interface ISelectionService
    {
        /// <summary>
        ///     Возвращает список выборок
        /// </summary>
        OperationResult<PageResult<SelectionListItemData>> Search(QueryConditions qc);

        /// <summary>
        ///     Возвращает список выборок для шаблона
        /// </summary>
        OperationResult<List<SelectionListItemData>> SearchByTemplate(long? templateId, QueryConditions qc);

        /// <summary>
        /// Список всех возможных статусов Выборки
        /// </summary>
        OperationResult<List<DictionaryItem>> StatusDictionary();

        /// <summary>
        /// Список всех возможных Причин исключения расхождений
        /// </summary>
        OperationResult<List<DictionaryItem>> ExcludeReasonDictionary();

        /// <summary>
        /// Строит список деклараций в рамках одной выборки, которые удовлетворяют критериям выборки
        /// </summary>
        /// <param name="selectionId">Идентификатор выборки</param>
        /// <param name="criteria">Критерии фильтрации деклараций внутри выборки</param>
        /// <returns>Список деклараций</returns>
        OperationResult<PageResult<SelectionDeclaration>> SearchDeclarations(long selectionId, QueryConditions criteria);


        /// <summary>
        /// Возвращает список расхождений по идентификатору запроса
        /// </summary>
        /// <param name="selectionId">Идентификатор выборки</param>
        /// <param name="condition">Критерии фильтрации расхождений внутри выборки</param>
        /// <returns>Список расхождений</returns>
        OperationResult<PageResult<SelectionDiscrepancy>> SearchDiscrepancies(
            long selectionId,
            bool firstSide,
            QueryConditions condition);

        /// <summary>
        /// Возвращает полный спиок расхождений выборки
        /// </summary>
        /// <param name="selectionId"></param>
        /// <param name="firstSide"></param>
        /// <param name="condition"></param>
        /// <returns></returns>
        OperationResult<List<SelectionDiscrepancy>> AllDiscrepancies(
            long selectionId,
            bool firstSide,
            QueryConditions condition);

        /// <summary>
        /// Список записий истории
        /// </summary>
        /// <param name="selectionId">ID выборки</param>
        /// <returns></returns>
        OperationResult<List<ActionHistory>> GetActionsHistory(long selectionId);

        /// <summary>
        /// Создает запрос на получение данных для выборки из Hive
        /// </summary>
        /// <param name="selection"></param>
        /// <param name="filter"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        OperationResult<SelectionRequestResult> RequestSelection(
            Selection selection, 
            SelectionType type,
            Filter filter,
            ActionType action );

        OperationResult RequestSelections(
            long templateId,
            string templateName,
            Filter filter,
            List<SelectionRegionalBoundsDataItem> regionalBounds,
            ActionType action);

        /// <summary>
        /// Возвращает список границ отбора по регионам для шаблона
        /// </summary>
        /// <param name="selectionTemplateId"></param>
        /// <returns></returns>
        OperationResult<List<SelectionRegionalBoundsDataItem>> GetRegionalBounds(long selectionTemplateId);

        /// <summary>
        /// Возвращает границы отбора по региону для выборки по шаблону
        /// </summary>
        /// <param name="selectionTemplateId"></param>
        /// <param name="regionalCode"></param>
        /// <returns></returns>
        OperationResult<SelectionRegionalBoundsDataItem> GetRegionalBounds(long selectionTemplateId, string regionalCode);

                    /// <summary>
        /// Сохраняет границы отбора в базу
        /// </summary>
        /// <param name="templateId"></param>
        /// <param name="items"></param>
        /// <returns></returns>
        OperationResult UpdateRegionalBounds(long templateId, List<SelectionRegionalBoundsDataItem> items);

        /// <summary>
        /// Сохранение выборки
        /// </summary>
        /// <param name="selection">выборка</param>
        /// <param name="action">действие</param>
        /// <returns>результат выполнения</returns>
        OperationResult<Selection> Save(Selection selection, ActionType action);

        #region Жизненный цикл выборки

        /// <summary>
        /// 
        /// </summary>
        /// <param name="selection"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        OperationResult<Selection> Delete(Selection selection, SelectionCommandParam param);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="selection"></param>
        /// <returns></returns>
        OperationResult<Selection> SendToAproval(Selection selection);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="selection">выборка или шаблон</param>
        /// <param name="selIds">Id выборок в шаблоне</param>
        /// <returns></returns>
        OperationResult Approve(Selection selection, List<long> selIds);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="selection">выборка или шаблон</param>
        /// <param name="selIds">Id выборок в шаблоне</param>
        /// <returns></returns>
        OperationResult SendBackForCorrections(Selection selection, List<long> selIds);

        /// <summary>
        /// Разрешить создание АТ по выборке
        /// </summary>
        /// <param name="selection"></param>
        /// <returns></returns>
        OperationResult<Selection> CreateClaim(Selection selection);

        /// <summary>
        /// Апдейтит статус выборки после того, как пришел запрос
        /// </summary>
        /// <param name="selectionId"></param>
        /// <param name="type"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        OperationResult UpdatetSelectionStatusAfterHiveLoad(long selectionId, SelectionType type, ActionType action);

        #endregion

        #region Активности с выборками

        /// <summary>
        /// Запрос на формирование выборки в МС
        /// </summary>
        /// <param name="selectionId">идентификатор выборки</param>
        /// <returns>результат выполнения</returns>
        OperationResult<long> RequestDiscrepancies(long selectionId);

        /// <summary>
        /// Возвращает уникальное имя выборки в контексе пользователя И типа выборки
        /// </summary>
        /// <returns></returns>
        string GenerateSelectionName(SelectionType type);


        OperationResult<List<SelectionRequest>> SyncWithRequestStatus(long selectionId, SelectionType type);

        OperationResult<PageResult<Selection>> SelectSelections(QueryConditions condition);

        OperationResult<Selection> GetSelection(long id);

        #endregion

        # region Работа с избранными фильтрами выборки

        /// <summary>
        /// Возвращает все избранные фильтры текущего пользователя
        /// </summary>
        /// <returns>Перечисление пар идентификаторов и названий избранных фильтров</returns>
        OperationResult<List<KeyValuePair<long, string>>> GetFavoriteFilters();

        /// <summary>
        /// Возвращает настройки избранного фильтра по идентификатору
        /// </summary>
        /// <param name="favoriteId">Идентификатор фильтра в списке избранных</param>
        /// <returns>Результат поиска фильтра</returns>
        OperationResult<Filter> GetFavoriteSelectionFilterVersionTwo(long favoriteId);

        /// <summary>
        /// Добавляет фильтр в избранное
        /// </summary>
        /// <param name="name">Имя фильтра</param>
        /// <param name="filter">Настройки фильтра</param>
        /// <returns>Результат выполнения операции</returns>
        OperationResult<KeyValuePair<long, string>> CreateFavorite(string name, Filter filter);

        /// <summary>
        /// Перезаписывает фильтр в избранном
        /// </summary>
        /// <param name="name">Имя фильтра</param>
        /// <param name="filter">Настройки фильтра</param>
        /// <returns>Результат выпонения операции</returns>
        OperationResult OverwriteFavorite(string name, Filter filter);

        /// <summary>
        /// Удаляет фильтр из избранного
        /// </summary>
        /// <param name="id">Идентификатор фильтра в избранном</param>
        /// <returns>Результат выполнения операции</returns>
        OperationResult DeleteFavorite(long id);

        # endregion

        # region Ручное включение/исключение деклараций и расхождений в/из выборки

        /// <summary>
        /// Включает декларацию обратно в выборку.
        /// </summary>
        /// <param name="selectionId">Идентификатор запроса выборки</param>
        /// <param name="declarationId">Идентификатор декларации</param>
        OperationResult IncludeDeclaration(long selectionId, long declarationId);

        /// <summary>
        /// Исключает декларацию из выборки
        /// </summary>
        /// <param name="selectionId">Идентификатор запроса выборки</param>
        /// <param name="declarationId">Идентификатор декларации</param>
        OperationResult ExcludeDeclaration(long selectionId, long declarationId);

        /// <summary>
        /// Вычисляет обобщенное состояние включения в выборку группы деклараций
        /// </summary>
        /// <param name="selectionId">Идентификатор запроса выборки</param>
        /// <param name="filter">Критерии фильтрации деклараций внутри выборки (определяют группу)</param>
        /// <returns>Результат выполнения операции</returns>
        OperationResult<SelectionGroupState> GetDeclarationGroupState(
            long selectionId,
            QueryConditions filter = null);

        /// <summary>
        /// Вычисляет обощенное состояние включения в выборку группы расхождений
        /// </summary>
        /// <param name="selectionId">Идентификатор выборки</param>
        /// <returns>Результат выполнения операции</returns>
        OperationResult<List<SelectionDiscrepancyStatistic>> GetDiscrepancyStatistic(
            long selectionId,
            SelectionType type
            );

        /// <summary>
        /// Включает группу деклараций обратно в выборку
        /// </summary>
        /// <param name="selectionId">Идентификатор запроса выборки</param>
        /// <param name="zipList">Список зипов деклараций, которые надо апдейтить</param>
        /// <returns>Результат выполнения операции</returns>
        OperationResult IncludeDeclarations(long selectionId, long[] zipList);

        /// <summary>
        /// Исключает группу деклараций из выборки
        /// </summary>
        /// <param name="selectionId">Идентификатор запроса выборки</param>
        /// <param name="zipList">Список зипов деклараций, которые надо апдейтить</param>
        /// <returns>Результат выполнения операции</returns>
        OperationResult ExcludeDeclarations(long selectionId, long[] zipList);

        # endregion

        # region Работа с переходами между состояними

        OperationResult<List<SelectionTransition>> GetStateTransitions();

        #endregion Работа с переходами между состояними

        #region проверка на определенную роль

        OperationResult<bool> CheckUserInRole(string userSid, string role);

        #endregion


        OperationResult<bool> CanBeDeleted(Selection selection);

        /// <summary>
        /// Определяет возможность удаления шаблона в зависимости от статусов выборок в нем
        /// </summary>
        /// <param name="templateId"></param>
        /// <returns></returns>
        OperationResult<bool> CanDeleteTemplate(long templateId);

        OperationResult DeleteSelectionsByTemplate(long templateId);

        # region Загрузка данных выборки

        OperationResult<SelectionDetails> Load(long? id, string lckKey, SelectionType type);

        OperationResult<SelectionDetails> NewFromTemplate(long id);

        bool IsNameUnique(string name, long id, SelectionType type);


        #endregion

        #region Фильтр выборки (НОВЫЙ)

        OperationResult<List<SelectionParameter>> GetFilterParameters();

        OperationResult<List<Region>> GetRegions();

        OperationResult<List<EditorParameter>> GetFilterEditorParameters(SelectionType type);

        OperationResult<List<KeyValuePair<string, string>>> GetFilterParameterOptions(int parameterId);

        OperationResult SaveFilter(long selectionId, Filter filter);

        OperationResult<List<KeyValuePair<string, string>>> GetWhiteLists();

        OperationResult<List<string>> GetInnFromWhiteList(long whiteListId);

        # endregion

        #region User Info

        /// <summary>
        /// Возаращает имя текущего пользователя
        /// </summary>
        /// <returns></returns>
        string GetCurrentUserDisplayName();

        /// <summary>
        /// Возвращает идентификатор текущего пользователя
        /// </summary>
        /// <returns></returns>
        string GetCurrentUserSid();

        #endregion

        OperationResult UpdateSelectionAggregate(long selectionId, SelectionType type);

        /// <summary>
        /// Обноялвяет поля Регионы в выборке, если оно изменилось после применеия фильтра
        /// </summary>
        /// <param name="selectionId"></param>
        /// <param name="regions"></param>
        OperationResult UpdateSelectionRegions(long selectionId, string regions);

        #region Creating Claim Functionality

        OperationResult<bool> ExistApprovedSelections();

        OperationResult<SelectionByStatusesCount> CountApprovedSelections();

        OperationResult SetClaimCreatingStatus();

        #endregion
    }
}