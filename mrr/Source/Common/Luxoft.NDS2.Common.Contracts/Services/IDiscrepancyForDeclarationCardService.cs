﻿using System;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.HiveRequest;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.DTO.Security;

namespace Luxoft.NDS2.Common.Contracts.Services
{
    public interface IDiscrepancyForDeclarationCardService
    {
        /// <summary>
        /// Возвращает список всех расхождений для отображения пользователю с ролью "Аналитик" в карточке НД
        /// </summary>
        /// <param name="qc">Условия фильтрации и сортировки</param>
        /// <param name="hiveRequest">Данные для запроса информации в Hive</param>
        /// <returns>Расхождения по СФ НД</returns>
        OperationResult<PageResult<DeclarationDiscrepancy>> GetAllDiscrepancyList(QueryConditions qc, HiveRequestDiscrepanciesList hiveRequest);

        /// <summary>
        /// Возвращает список расхождений КНП для отображения пользователю с ролью "Инспектор" в карточке НД
        /// </summary>
        /// <param name="qc">Условия фильтрации и сортировки</param>
        /// <param name="inn">ИНН налогоплательщика НД</param>
        /// <param name="kppEffective">Эффективный КПП налогоплательщика НД</param>
        /// <param name="fiscalYear">Год отчетного периода НД</param>
        /// <param name="periodEffective">Эффективный код отчетного периода НД</param>
        /// <returns>Расхождения по СФ НД</returns>
        OperationResult<PageResult<DeclarationDiscrepancy>> GetKnpDiscrepancyList(QueryConditions qc, string inn, string kppEffective, string fiscalYear, int periodEffective);

        /// <summary>
        /// Возвращает список наименовний операций, доступных пользователю
        /// </summary>
        /// <returns>Список имен операций NDS2</returns>
        OperationResult<List<string>> GetUserOperations();
    }
}
