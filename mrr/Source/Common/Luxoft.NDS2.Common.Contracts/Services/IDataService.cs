﻿using System.Collections.Generic;
using System.ServiceModel;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Business.SellBook;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Results;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Report;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;
using Luxoft.NDS2.Common.Contracts.DTO.TaxPeriod;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;


namespace Luxoft.NDS2.Common.Contracts.Services
{
    public interface IDataService
    {

        OperationResult<List<RegionDetails>> GetRegions();
        OperationResult<List<DictionaryNalogOrgan>> GetNalogOrgans();
        OperationResult<List<DictionaryNalogPeriods>> GetNalogPeriods();
        OperationResult<List<FederalDistrict>> GetFederalDistricts();

        OperationResult<Dictionary<string, string>> GetDictionaryData(string nameDictionary, DictionaryConditions criteria);

        OperationResult<Dictionary<string, string>> SearchRegions(string searchKey, int maxQuantity);
        OperationResult<Dictionary<string, string>> SearchInspections(string searchKey, int maxQuantity);

        OperationResult<Configuration> ReadConfiguration();
        OperationResult SaveConfiguration(ConfigurationParamName param, string value);

        OperationResult<List<FilterCriteria>> GetAutoselectionFilterCriteria();
        OperationResult SaveAutoselectionFilter(List<GroupFilter> IncludeFilter, List<GroupFilter> ExcludeFilter);
        OperationResult SaveAutoselectionFilter(Filter includeFilter, Filter excludeFilter);
        OperationResult LoadAutoselectionFilter(out List<GroupFilter> IncludeFilter, out List<GroupFilter> ExcludeFilter);
        OperationResult LoadAutoselectionFilter(out Filter includeFilter, out Filter excludeFilter);

        OperationResult<List<CFGRegion>> GetLoadingInspection();
        OperationResult<CFGRegion> LoadingInspectionSetValue(CFGRegion param);
        OperationResult<TaxPeriodConfiguration> GetTaxPeriodConfiguration();

        OperationResult<List<ShortClarification>> GetShortClarifications();
    }
}
