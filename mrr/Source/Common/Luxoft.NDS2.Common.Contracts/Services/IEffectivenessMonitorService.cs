﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Common.Contracts.Services
{
    public interface IEfficiencyMonitoringService
    {
        //OperationResult<SonoInfo> GetSonoCodeByCode(string code);

        /// <summary>
        /// Показатели доли суммы расхождений в сумме вычетов
        /// </summary>
        /// <param name="year">Отчетный гож</param>
        /// <param name="qtr">Квартал</param>
        /// <param name="calcDate">Дата расчета</param>
        /// <returns></returns>
        OperationResult<List<IfnsRatingByDiscrepancyShareInDeductionData>> GetIfnsRatingByDiscrepancyShareInDeduction(int year, int qtr, DateTime calcDate);

        /// <summary>
        /// Список детализации по открытым расхождениям КНП
        /// </summary>
        /// <param name="queryConditions">условия поиска</param>
        /// <param name="withTotal">возвращать тотал записей</param>
        /// <returns></returns>
        OperationResult<PageResult<DiscrepancyDetail>> GetDiscrepancyDetails(QueryConditions queryConditions, bool withTotal);


            /// <summary>
        /// Расчетные даты сгруппированные по году и кварталу
        /// </summary>
        /// <returns></returns>
        OperationResult<Dictionary<QuarterInfo, List<ReportCalculationInfo>>> GetQuarterCalculationDates();

        /// <summary>
        /// Показатели рейтингов по кварталам
        /// </summary>
        /// <param name="tnoName"></param>
        /// <returns></returns>
        OperationResult<Dictionary<string, List<TnoRatingInfo>>> GetAllQuartersRatings(ChartData request);

        /// <summary>
        /// Данные по Общим показателям
        /// </summary>
        OperationResult<List<TnoData>> GetFederalCollection(CalculationData request);

        /// <summary>
        /// Данные по Регионам
        /// </summary>
        /// <returns></returns>
        OperationResult<List<RegionData>> GetEfficiencyRegionCollection(RaitingRequestData requestData);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestData"></param>
        /// <returns></returns>
        OperationResult<List<RegionData>> GetEfficiencyRegionForComparison( RaitingRequestData requestData );

        /// <summary>
        /// Данные по Регионам
        /// </summary>
        /// <returns></returns>
        OperationResult<List<RegionData>> GetDispatchRaitingRegionCollection(RaitingRequestData requestData);

        /// <summary>
        /// Список доступных дат в квартале
        /// </summary>
        /// <param name="fiscalYear">Год</param>
        /// <param name="quarterNumber">Номер квартала</param>
        /// <returns></returns>
        OperationResult<List<DateTime>> GetEnabledDates(int fiscalYear, int quarterNumber);

        /// <summary>
        /// Список доступных дат в квартале для данного региона
        /// </summary>
        /// <param name="regionId">Ид-р региона</param>
        /// <param name="fiscalYear">Год</param>
        /// <param name="quarterNumber">Номер квартала</param>
        /// <returns></returns>
        OperationResult<List<DateTime>> GetEnabledRegionDates(string regionId, int fiscalYear, int quarterNumber);

        // new

        /// <summary>
        /// 
        /// </summary>
        /// <param name="regionId">Ид-р региона</param>
        /// <param name="fiscalYear">Год</param>
        /// <param name="quarterNumber">Номер квартала</param>
        /// <returns></returns>
        OperationResult<RegionData> GetLastRaitingForRegion(string regionId, int fiscalYear, int quarterNumber);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="regionId">Ид-р региона</param>
        /// <param name="fiscalYear">Год</param>
        /// <param name="quarterNumber">Номер квартала</param>
        /// <param name="date"></param>
        /// <returns></returns>
        OperationResult<RegionData> GetRegionData(string regionId, int fiscalYear, int quarterNumber, DateTime date);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="regionId">Ид-р региона</param>
        /// <returns></returns>
        OperationResult<List<RegionData>> GetAllRegionData(string regionId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestData"></param>
        /// <param name="regionId">Ид-р региона</param>
        /// <returns></returns>
        OperationResult<List<IfnsData>> GetEfficiencyIfnsCollection( RaitingRequestData requestData, string regionId );

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestData"></param>
        /// <param name="regionId">Ид-р региона</param>
        /// <param name="codes"></param>
        /// <returns></returns>
        OperationResult<List<IfnsData>> GetEfficiencyIfnsCollection( RaitingRequestData requestData, string regionId, List<string> codes );

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fiscalYear">Год</param>
        /// <param name="quarterNumber">Номер квартала</param>
        /// <param name="regionId">Ид-р региона</param>
        /// <returns></returns>
        OperationResult<List<DateTime>> GetIfnsEnabledDates(int fiscalYear, int quarterNumber, string regionId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sonoCode">Ид-р инспекции</param>
        /// <param name="fiscalYear">Год</param>
        /// <param name="quarterNumber">Номер квартала</param>
        /// <returns></returns>
        OperationResult<IfnsData> GetLastRatingForIfns(string sonoCode, int fiscalYear, int quarterNumber);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sonoCode">Ид-р инспекции</param>
        /// <param name="fiscalYear">Год</param>
        /// <param name="quarterNumber">Номер квартала</param>
        /// <param name="date"></param>
        /// <returns></returns>
        OperationResult<IfnsData> GetIfnsData(string sonoCode, int fiscalYear, int quarterNumber, DateTime date);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sonoCode">Ид-р инспекции</param>
        /// <returns></returns>
        OperationResult<List<IfnsData>> GetAllIfnsData(string sonoCode);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="districtId"></param>
        /// <returns></returns>
        OperationResult<List<RegionData>> GetRegions(string districtId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="regionId">Ид-р региона</param>
        /// <returns></returns>
        OperationResult<List<IfnsData>> GetIfnsForComparison(RaitingRequestData request, string regionId);
    }
}
