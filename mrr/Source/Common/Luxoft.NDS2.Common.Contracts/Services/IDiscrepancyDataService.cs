﻿using System;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy.DiscrepancyTable;
using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.HiveRequest;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Common.Contracts.Services
{
    public interface IDiscrepancyDataService
    {
        OperationResult<bool> DiscrepancyIsClosed(long discrepancyId);

        OperationResult<List<DiscrepancySide>> GetDiscrepancySides(HiveRequestDiscrepancyCard hiveRequest);

        OperationResult<DiscrepancySummaryInfo> GetSummaryInfo(long discrepancyId);

        OperationResult SaveComment(long discrepancyId, DetailsBase details);

        OperationResult SaveDiscrepancyUserComment(long discrepancyId, string comment);

        OperationResult<DiscrepancyDocumentInfo> SelectDocumentInfo(long id);

        OperationResult<DiscrepancyDocumentInfo> GetDiscrepancyDocumentInfo(long discrepancyId, int stageId);

    }
}
