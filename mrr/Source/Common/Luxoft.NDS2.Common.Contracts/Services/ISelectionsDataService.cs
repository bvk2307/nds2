﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Common.Contracts.Services
{
    public interface ISelectionsDataService
    {
        /// <summary>
        ///     Строит список деклараций в рамках одной выборки, которые удовлетворяют критериям выборки
        /// </summary>
        /// <param name="selectionId">Идентификатор выборки</param>
        /// <param name="criteria">Критерии фильтрации деклараций внутри выборки</param>
        /// <returns>Список деклараций</returns>
        OperationResult<DeclarationPageResult> SearchDeclarations(long selectionId, QueryConditions criteria);
    }
}