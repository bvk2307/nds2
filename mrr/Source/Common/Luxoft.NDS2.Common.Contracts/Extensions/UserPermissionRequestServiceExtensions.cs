﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Contracts.DTO.Security;
using Luxoft.NDS2.Common.Contracts.Services;
using DiagContracts = System.Diagnostics.Contracts;


namespace Luxoft.NDS2.Common.Contracts.Extensions
{
    /// <summary> Extension mehtods for <see cref="IUserPermissionRequestService"/>. </summary>
    public static class UserPermissionRequestServiceExtensions
    {
        /// <summary> Is operation eligible for "soun" code <paramref name="declarationBrief"/>? </summary>
        /// <param name="userPermissionRequestService"></param>
        /// <param name="operationIdentifier"></param>
        /// <param name="declarationBrief"></param>
        /// <returns></returns>
        public static OperationResult<UserPermissionRequestResult> 
            IsOperationEligibleByUserAccess( this IUserPermissionRequestService userPermissionRequestService, string operationIdentifier, DeclarationBrief declarationBrief )
        {
            DiagContracts.Contract.Requires( declarationBrief != null );

            return userPermissionRequestService.IsOperationEligibleByUserAccess( operationIdentifier, declarationBrief.SOUN_CODE );
        }

        /// <summary> Is operation eligible for "soun" code <paramref name="declarationSummary"/>? </summary>
        /// <param name="userPermissionRequestService"></param>
        /// <param name="operationIdentifier"></param>
        /// <param name="declarationSummary"></param>
        /// <returns></returns>
        public static OperationResult<UserPermissionRequestResult> 
            IsOperationEligibleByUserAccess( this IUserPermissionRequestService userPermissionRequestService, string operationIdentifier, DeclarationSummary declarationSummary )
        {
            DiagContracts.Contract.Requires( declarationSummary != null );

            return userPermissionRequestService.IsOperationEligibleByUserAccess( operationIdentifier, declarationSummary.SOUN_CODE );
        }
    }
}