﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System
{
    public static class StringExtension
    {
        //ZSON - сериализатор разработанный Михаилом Жавжаровым(ФЛС)
        public static string FromZsonToCommaSeparated(this string zson)
        {
            if (string.IsNullOrEmpty(zson))
            {
                return string.Empty;
            }

            return zson.Replace("{", string.Empty)
                .Replace("}", string.Empty)
                .Replace("[", string.Empty)
                .Replace("]", string.Empty);
        }

        public static string[] FromJsonToStringArray(this string json)
        {
            return json.TrimStart('[').TrimEnd(']').Split(',')
                .Select(s => s.Trim().TrimStart('{').TrimEnd('}'))
                .ToArray();
        }
    }
}
