﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.TaxPeriod
{
    public abstract class PeriodBase
    {
        protected int _index;

        protected abstract int GetMonth(int year);

        protected abstract int GetMonthToAdd(int year);

        public PeriodBase(int index)
        {
            _index = index;
        }

        private DateTime YearBegins(int year)
        {
            return new DateTime(year, 1, 1);
        }

        public DateTime BeginDate(int year)
        {
            return YearBegins(year)
                .AddMonths(GetMonth(year));
        }

        public DateTime EndDate(int year)
        {
            return YearBegins(year)
                .AddMonths(GetMonthToAdd(year))
                .AddDays(-1);
        }
    }
}
