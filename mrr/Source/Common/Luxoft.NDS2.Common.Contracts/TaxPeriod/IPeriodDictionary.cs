﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.TaxPeriod
{
    public interface IPeriodDictionary
    {
        PeriodBase Get(string code);
    }
}
