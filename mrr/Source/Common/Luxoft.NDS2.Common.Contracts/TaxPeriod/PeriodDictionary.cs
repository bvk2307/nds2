﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Concurrent;
using Luxoft.NDS2.Common.Contracts.DTO.TaxPeriod;

namespace Luxoft.NDS2.Common.Contracts.TaxPeriod
{
    public class PeriodDictionary : IPeriodDictionary
    {
        private List<DictTaxPeriod> _taxPeriods = new List<DictTaxPeriod>();

        private ConcurrentDictionary<string, PeriodBase> _periods = new ConcurrentDictionary<string, PeriodBase>();

        private object _threadLock = new object();

        public PeriodDictionary(List<DictTaxPeriod> taxPeriods)
        {
            _taxPeriods.AddRange(taxPeriods);
        }

        public PeriodBase Get(string code)
        {
            return _periods.GetOrAdd(code, CreatePeriod(code));
        }

        private PeriodBase CreatePeriod(string periodCode)
        {
            PeriodBase period = null;

            var dictTaxPeriod = _taxPeriods.Where(p => p.Code == periodCode).Single();

            if (dictTaxPeriod.Month > 0)
                period = new PeriodMonth(dictTaxPeriod.Month);
            else
                period = new PeriodQuarter(dictTaxPeriod.Quarter);

            return period;
        }
    }
}
