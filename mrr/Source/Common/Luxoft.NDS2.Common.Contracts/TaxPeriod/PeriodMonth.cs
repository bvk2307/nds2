﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.TaxPeriod
{
    public class PeriodMonth : PeriodBase
    {
        public PeriodMonth(int index)
            : base(index)
        {
        }

        protected override int GetMonth(int year)
        {
            return _index - 1;
        }

        protected override int GetMonthToAdd(int year)
        {
            return _index;
        }
    }
}
