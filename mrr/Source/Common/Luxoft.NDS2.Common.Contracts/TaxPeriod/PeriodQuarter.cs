﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.TaxPeriod
{
    public class PeriodQuarter : PeriodBase
    {
        private const int MONTHS_IN_QUARTER = 3;

        public PeriodQuarter(int index)
            : base(index)
        {
        }

        protected override int GetMonth(int year)
        {
            return (_index - 1) * MONTHS_IN_QUARTER;
        }

        protected override int GetMonthToAdd(int year)
        {
            return (_index) * MONTHS_IN_QUARTER;
        }
    }
}
