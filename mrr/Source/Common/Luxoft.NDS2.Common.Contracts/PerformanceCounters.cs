﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts
{
    public class PerformanceCounters
    {
        public class Server
        {
             public const string CategoryName = "NDS2.Server";
             public PerformanceCounterCategoryType CategoryType = PerformanceCounterCategoryType.SingleInstance;
             public class Security
             {
                 public const string RequestsTotalName = "Auth.LDAP.RequestsTotalCount";
                 public const PerformanceCounterType RequestTotalType = PerformanceCounterType.NumberOfItems64;

                 public const string RequestsCacheTotalName = "Auth.LDAP.Cache.RequestsTotalCount";
                 public const PerformanceCounterType RequestCacheTotalType = PerformanceCounterType.NumberOfItems64;

                 public const string LdapRequestTimeName = "Auth.LDAP.RequestTime";
                 public const PerformanceCounterType LdapRequestTimeType = PerformanceCounterType.NumberOfItems64;
             }
        }
    }
}
