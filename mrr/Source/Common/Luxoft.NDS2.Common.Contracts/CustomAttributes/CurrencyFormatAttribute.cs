﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.CustomAttributes
{
    public class CurrencyFormatAttribute : FormatAttribute
    {
        private const string DEFAULT_FORMAT = "### ### ### ### ##0.00";
        public CurrencyFormatAttribute(string format)
            : base(format)
        {
        }

        public CurrencyFormatAttribute()
            : this(DEFAULT_FORMAT)
        {
        }
    }
}
