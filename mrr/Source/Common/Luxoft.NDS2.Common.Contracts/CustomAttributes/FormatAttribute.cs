﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.CustomAttributes
{
    public class FormatAttribute : Attribute
    {
        public string Format { get; protected set; }

        public FormatAttribute(string format)
        {
            Format = format;
        }
    }
}
