﻿using System;

namespace Luxoft.NDS2.Common.Contracts
{
    public sealed class Constants
    {
        public class CommonMessages
        {
            public const string TRACE_DB_CMD_EXECUTED = "Запрос к БД";
            public const string TRACE_DB_CMD_ERROR = "Ошибка выполнения запроса к БД";
            public const string TRACE_DB_CMD_TEXT = "Запрос";
            public const string TRACE_DB_CONN_OPEN_TIME = "open connection time";
            public const string TRACE_DB_EXEC_TIME = "execution time";
        }

        public class Instrumentation
        {
            public const string SERVER_PROFILE_TYPE_NAME = "NDS2:Profile-Server";
            public const string DEPENDENCYTREEREPORT_PROFILING_PROFILE_TYPE_NAME = "NDS2:Profiling-DependencyTreeReport";
            //public const string SERVICECALL_PROFILING_PROFILE_TYPE_NAME = "NDS2:Profiling-ServiceCall";
            public const string CLIENT_PROFILE_TYPE_NAME = "NDS2:Profile-Client";
        }

        public class SystemPermissions
        {
            public const string CentralDepartmentCode = "0000";
            public const string MIKKCode = "9962";

            public const string UFMSKeyPostfix = "00";

            public class Operations
            {
                public const string RoleDeveloper = "Роль.Developer";
                public const string RoleAnalyst = "Роль.Аналитик";
                public const string RoleApprover = "Роль.Согласующий";
                public const string RoleManager = "Роль.Руководитель";
                public const string RoleSender = "Роль.Отправитель";
                public const string RoleMedodologist = "Роль.Методолог";
                public const string RoleInspector = "Роль.Инспектор";
                public const string RoleAdministrator = "Роль.Администратор";
                public const string RoleObserver = "Роль.Наблюдатель";

                public const string SystemLogon = "Система.Вход";

                public const string SelectionCreate = "Выборка.Создать";
                public const string SelectionTemplateCreate = "Выборка.Создать_шаблон";
                public const string SelectionChange = "Выборка.Изменить";
                public const string SelectionTemplateChange = "Выборка.Изменить_шаблон";
                public const string SelectionRemove = "Выборка.Удалить";
                public const string SelectionTemplateRemove = "Выборка.Удалить_шаблон";
                public const string SelectionTemplateCopy = "Выборка.Копировать_шаблон";
                public const string SelectionView = "Выборка.Просмотр";
                public const string SelectionTemplateView = "Выборка.Просмотр_шаблона";
                public const string SelectionUpdate = "Выборка.Обновить";
                public const string SelectionFilterApply = "Выборка.Фильтр.Применить";
                public const string SelectionFilterReset = "Выборка.Фильтр.Сбросить";
                public const string SelectionFilterFavorites = "Выборка.Фильтр.Избранное";
                public const string SelectionFilterFavoritesAdd = "Выборка.Фильтр.Добавить_в_избранное";
                public const string SelectionFilterReturnParams = "Выборка.Фильтр.Вернуться_к_примененному";
                public const string SelectionTakeInWork = "Выборка.Взять_в_работу";

                public const string SelectionToCorrect = "Выборка.Отправить_на_доработку";
                public const string SelectionApproved = "Выборка.Согласовать";
                public const string SelectionToApprove = "Выборка.На_Согласование";

                public const string DeclarationTreeRelation = "Декларация.Дерево_связей";
                public const string DeclarationViewList = "Декларация.Просмотр_списка";
                public const string DeclarationViewCard = "Декларация.Просмотр_карточки";
                public const string DeclarationCardViewClaims = "Декларация.Карточка.Список_автотребований";

                public const string TaxPayerViewCard = "Налогоплательщик.Просмотр_карточки";

                public const string TaxPayerRequestBankAccounts = "Налогоплательщик.Запросы_в_банк";

                public const string NavigatorView = "Навигатор.Просмотр";

                public const string OOREntrу = "ООР.Вход";
                public const string OORResetMark = "ООР.Сбросить_признак";
                public const string OORTakeInWork = "ООР.Взять_в_работу";

                public const string NDSView = "НД_Журнал.Просмотр";
                public const string ExplainCardEdit = "Пояснение_Ответ.Редактирование";
                public const string ExplainCardView = "Пояснение_Ответ.Просмотр";

				public const string UserTaskEnter = "ПЗ.Вход";
				public const string UserTaskAdmin = "ПЗ.Администрирование";

                public const string SettingsAsk = "ПараметрыАСК";

                public const string DiscrepancyAllView = "Расхождения.Все";
            }

            //            public class Roles
            //            {
            ////                public const string Developer = "Developer";
            ////                public const string Analyst = "Аналитик";
            ////                public const string Approver = "Согласующий";
            ////                public const string Manager = "Руководитель";
            ////                public const string Sender = "Отправитель";
            ////                public const string Medodologist = "Методолог";
            ////                public const string Inspector = "Инспектор";
            //            }
        }

        public const string EndpointName = "NDS2";
        public const short SubsystemId = 20;
        public const string SubsystemName = "NDS2";

        public const string DB_CONFIG_KEY = "NDS2_DB";
        public const string DB_MIRROR_CONFIG_KEY = "NDS2_DB_MIRROR";
        public const string SERVER_SOV_CACHE_ENABLED_KEY = "NDS2.Server.SovCacheEnabled";

        public const string SERVER_CLASSIFIERS_CACHE_DISABLED_KEY = "NDS2.Server.ClassifierCache.Disabled";

        public const string MC_SERVICE_HOST = "MC_ServiceHost";
        public const string MC_SERVICE_PORT = "MC_ServicePort";

        public const string THES_ACESS_CONFIG_PROFILE_NAME_KEY = "ThesAccessConfigProfileName";

        public const string DeclarationNotFoundErrorMessage = "Декларация с ID={0} не найдена";

        public const int DeclarationDiscrepancyPartNum = 100;

        public const StringComparison KeysComparison            = StringComparison.Ordinal;
        public const StringComparison KeysComparisonIgnoreCase  = StringComparison.OrdinalIgnoreCase;

        public const string HiveRequestDiscrepanciesListLoadFailed = "Выгрузка данных из Hive завершилась с ошибкой для НД: ИНН={0}, КПП={1}, год={2}, период={3}, корректировка={4}";
        public const string HiveRequestDiscrepanciesListTimeout = "Выгрузка данных из Hive превысила таймаут для НД: ИНН={0}, КПП={1}, год={2}, период={3}, корректировка={4}";

        public const string HiveRequestDiscrepancyCardLoadFailed = "Выгрузка данных из Hive завершилась с ошибкой для расхождения: ИД={0}, СФ1={1}, СФ2={2}";
        public const string HiveRequestDiscrepancyCardTimeout = "Выгрузка данных из Hive превысила таймаут для расхождения: ИД={0}, СФ1={1}, СФ2={2}";
    }
}