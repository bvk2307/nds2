﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Common.Contracts.DTO.Navigator
{
    [Serializable]
    public class LoadTaxPayersContract : CountTaxPayersContract
    {
        public List<ColumnSort> Sort
        {
            get;
            set;
        }

        public ushort PageSize
        {
            get;
            set;
        }

        public uint PageIndex
        {
            get;
            set;
        }
    }
}
