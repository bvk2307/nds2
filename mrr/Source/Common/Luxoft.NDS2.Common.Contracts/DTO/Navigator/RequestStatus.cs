﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Navigator
{
    public enum RequestStatus 
    { 
        Submited = 0, 
        InProcess = 1, 
        Completed = 2,
        NotSuccessInNightMode = 8,
        Failed = 9
    }
}
