﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Navigator
{
    [Serializable]
    public class ChainSummary
    {
        public long PairId
        {
            get;
            set;
        }

        public long ChainId
        {
            get;
            set;
        }

        public string HeadTaxPayerInn
        {
            get;
            set;
        }

        public string HeadTaxPayerName
        {
            get;
            set;
        }

        public string TailTaxPayerName
        {
            get;
            set;
        }

        public string TailTaxPayerInn
        {
            get;
            set;
        }

        public int Number
        {
            get;
            set;
        }

        public int TargetTaxPayersQuantity
        {
            get;
            set;
        }

        public int LinksQuantity
        {
            get;
            set;
        }
    }
}
