﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Navigator
{
    [Serializable]
    public class Request
    {
        public int MonthFrom
        {
            get;
            set;
        }

        public int YearFrom
        {
            get;
            set;
        }

        public int MonthTo
        {
            get;
            set;
        }

        public int YearTo
        {
            get;
            set;
        }

        public int Depth
        {
            get;
            set;
        }

        public int MinMappedAmount
        {
            get;
            set;
        }

        public int MaxContractorsQuantity
        {
            get;
            set;
        }

        public int MinSellerAmount
        {
            get;
            set;
        }

        public int MinBuyerAmount
        {
            get;
            set;
        }

        public string[] Contractors
        {
            get;
            set;
        }
    }
}
