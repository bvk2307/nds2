﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Navigator
{
    [Serializable]
    public class TaxPayerKey
    {
        public string Inn
        {
            get;
            set;
        }

        public string Kpp
        {
            get;
            set;
        }
    }
}
