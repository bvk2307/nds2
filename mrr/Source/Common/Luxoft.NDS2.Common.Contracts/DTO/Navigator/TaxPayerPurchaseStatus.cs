﻿namespace Luxoft.NDS2.Common.Contracts.DTO.Navigator
{
    public class TaxPayerPurchaseStatus
    {
        public string Inn
        {
            get;
            set;
        }

        public int NdsCompensationQuantity
        {
            get;
            set;
        }
    }
}
