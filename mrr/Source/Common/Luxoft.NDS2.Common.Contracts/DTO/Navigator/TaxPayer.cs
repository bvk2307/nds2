﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Navigator
{
    [Serializable]
    public class TaxPayer
    {
        public string Inn
        {
            get;
            set;
        }

        public string Kpp
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public string Inspection
        {
            get;
            set;
        }

        public string Region
        {
            get;
            set;
        }

        public string FederalDistrict
        {
            get;
            set;
        }

        public int SurCode
        {
            get;
            set;
        }

        public decimal? SalesAmount
        {
            get;
            set;
        }

        public decimal? PurchaseAmount
        {
            get;
            set;
        }

        public decimal? NdsCalculated
        {
            get;
            set;
        }

        public decimal? NdsDeduction
        {
            get;
            set;
        }

        public string RegionCode
        {
            get;
            set;
        }

        public string InspectionCode
        {
            get;
            set;
        }
    }
}
