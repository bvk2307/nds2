﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Navigator
{
    [Serializable]
    public class ChainContractorData
    {
        public ChainContractorData()
        {
        }

        public long ChainId
        {
            get;
            set;
        }

        public int SurCode
        {
            get;
            set;
        }

        public int ChainNumber
        {
            get;
            set;
        }

        public long Id
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public string Inn
        {
            get;
            set;
        }

        public string Kpp
        {
            get;
            set;
        }

        public bool Head
        {
            get;
            set;
        }

        public bool Tail
        {
            get;
            set;
        }

        public decimal? NdsCalculated
        {
            get;
            set;
        }

        public decimal? NdsDeduction
        {
            get;
            set;
        }

        public decimal? NdsSales
        {
            get;
            set;
        }

        public decimal? NdsPurchase
        {
            get;
            set;
        }

        public int Index
        {
            get;
            set;
        }

        public decimal? ChainMinMappedAmount
        {
            get;
            set;
        }

        public decimal? ChainMaxMappedAmount
        {
            get;
            set;
        }

        public decimal? ChainMinNotMappedAmount
        {
            get;
            set;
        }

        public decimal? ChainMaxNotMappedAmount
        {
            get;
            set;
        }

        public decimal MappedAmountAsBuyer
        {
            get;
            set;
        }

        public decimal BuyerAmountAsBuyer
        {
            get;
            set;
        }

        public decimal SellerAmountAsBuyer
        {
            get;
            set;
        }

        public decimal MappedAmountAsSeller
        {
            get;
            set;
        }

        public decimal BuyerAmountAsSeller
        {
            get;
            set;
        }

        public decimal SellerAmountAsSeller
        {
            get;
            set;
        }
    }
}
