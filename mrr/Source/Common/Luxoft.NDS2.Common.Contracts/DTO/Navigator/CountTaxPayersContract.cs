﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Common.Contracts.DTO.Navigator
{
    [Serializable]
    public class CountTaxPayersContract
    {
        public List<FilterQuery> Filter
        {
            get;
            set;
        }

        public PeriodRange Range
        {
            get;
            set;
        }
    }
}
