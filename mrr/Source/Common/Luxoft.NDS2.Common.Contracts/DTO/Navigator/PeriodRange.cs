﻿using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Navigator
{
    [Serializable]
    public class PeriodRange
    {
        public Period From
        {
            get;
            set;
        }

        public Period To
        {
            get;
            set;
        }
    }
}
