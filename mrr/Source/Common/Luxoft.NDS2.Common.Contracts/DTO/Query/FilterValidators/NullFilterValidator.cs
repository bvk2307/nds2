﻿namespace Luxoft.NDS2.Common.Contracts.DTO.Query.FilterValidators
{
    public class NullFilterValidator : IFilterValidator
    {
        public bool IsValid(
            ColumnFilter.FilterComparisionOperator comparisonOperator, 
            object filterValue)
        {
            return true;
        }
    }
}
