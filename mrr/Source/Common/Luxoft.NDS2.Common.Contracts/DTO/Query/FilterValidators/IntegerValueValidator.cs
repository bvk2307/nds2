﻿namespace Luxoft.NDS2.Common.Contracts.DTO.Query.FilterValidators
{
    public class IntegerValueValidator : IValueValidator
    {
        public bool IsValid(object value)
        {
            if (value == null
                || string.IsNullOrEmpty(value.ToString()))
            {
                return true;
            }

            int temp;

            return int.TryParse(value.ToString(), out temp);
        }
    }
}
