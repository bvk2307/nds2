﻿namespace Luxoft.NDS2.Common.Contracts.DTO.Query.FilterValidators
{
    public interface IFilterValidator
    {
        bool IsValid(
            ColumnFilter.FilterComparisionOperator comparisonOperator, 
            object filterValue);
    }
}
