﻿using System.Linq;

namespace Luxoft.NDS2.Common.Contracts.DTO.Query.FilterValidators
{
    public class NumericFilterValidator : IFilterValidator
    {
        private readonly ColumnFilter.FilterComparisionOperator[] _allowedOperators =
            new []
            {
                ColumnFilter.FilterComparisionOperator.DoesNotMatch,
                ColumnFilter.FilterComparisionOperator.Equals,
                ColumnFilter.FilterComparisionOperator.GreaterThan,
                ColumnFilter.FilterComparisionOperator.GreaterThanOrEqualTo,
                ColumnFilter.FilterComparisionOperator.LessThan,
                ColumnFilter.FilterComparisionOperator.LessThanOrEqualTo,
                ColumnFilter.FilterComparisionOperator.Match,
                ColumnFilter.FilterComparisionOperator.NotDefinedOperator,
                ColumnFilter.FilterComparisionOperator.NotEquals
            };

        private readonly IValueValidator _numberValidator;

        public NumericFilterValidator(IValueValidator numberValidator)
        {
            _numberValidator = numberValidator;
        }

        public bool IsValid(
            ColumnFilter.FilterComparisionOperator comparisonOperator, 
            object filterValue)
        {
            if (_allowedOperators.Contains(comparisonOperator))
            {
                return _numberValidator.IsValid(filterValue);
            }

            return false;
        }
    }
}
