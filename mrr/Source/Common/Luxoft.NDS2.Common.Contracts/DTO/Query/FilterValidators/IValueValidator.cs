﻿namespace Luxoft.NDS2.Common.Contracts.DTO.Query.FilterValidators
{
    public interface IValueValidator
    {
        bool IsValid(object value);
    }
}
