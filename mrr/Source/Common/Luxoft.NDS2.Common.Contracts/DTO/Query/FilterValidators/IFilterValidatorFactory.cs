﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Query.FilterValidators
{
    public interface IFilterValidatorFactory
    {
        IFilterValidator Create(string fieldName, Type fieldType);
    }
}
