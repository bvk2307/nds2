﻿namespace Luxoft.NDS2.Common.Contracts.DTO.Query.FilterValidators
{
    public class LongValueValidator : IValueValidator
    {
        public bool IsValid(object value)
        {
            if (value == null
                || string.IsNullOrEmpty(value.ToString()))
            {
                return true;
            }

            long temp;

            return long.TryParse(value.ToString(), out temp);
        }
    }
}
