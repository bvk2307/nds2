﻿using System;
using System.Globalization;

namespace Luxoft.NDS2.Common.Contracts.DTO.Query.FilterValidators
{
    public class DecimalValueValidator : IValueValidator
    {
        private readonly IFormatProvider _formatProvider;

        public DecimalValueValidator(IFormatProvider formatProvider)
        {
            _formatProvider = formatProvider;
        }

        public DecimalValueValidator()
            : this(CultureInfo.CurrentCulture)
        {
        }

        public bool IsValid(object value)
        {
            if (value == null || string.IsNullOrEmpty(value.ToString()))
            {
                return true;
            }

            decimal temp;

            return decimal.TryParse(
                value.ToString(), 
                NumberStyles.Number, 
                _formatProvider, 
                out temp);
        }
    }
}
