﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Query.FilterValidators
{
    public class DefaultFilterValidatorFactory : IFilterValidatorFactory
    {
        public IFilterValidator Create(string fieldName, Type fieldType)
        {
            if (fieldType == typeof(decimal))
            {
                return new NumericFilterValidator(new DecimalValueValidator());
            }

            if (fieldType == typeof(int) || fieldType == typeof(int?))
            {
                return new NumericFilterValidator(new IntegerValueValidator());
            }

            if (fieldType == typeof(long) || fieldType == typeof(long?))
            {
                return new NumericFilterValidator(new LongValueValidator());
            }

            return new NullFilterValidator();
        }
    }
}
