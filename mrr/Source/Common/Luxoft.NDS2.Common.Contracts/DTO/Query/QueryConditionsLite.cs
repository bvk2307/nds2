﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Query
{
    [Serializable]
    public class QueryConditionsLite
    {
        public QueryConditionsLite()
        {
        }

        public QueryConditionsLite(QueryConditions source)
        {
            Sorting = source.Sorting;
            Filter = source.Filter;
        }
        
        public List<ColumnSort> Sorting { get; set; }

        public List<FilterQuery> Filter { get; set; }

        public QueryConditions ToQueryConditions()
        {
            return new QueryConditions
            {
                Sorting = Sorting,
                Filter = Filter
            };
        }
    }
}
