﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Query
{
    [Serializable]
    public class DictionaryConditions : ICloneable
    {
        private List<FilterQuery> _filters = new List<FilterQuery>();
        public List<FilterQuery> Filter
        {
            get
            {
                return _filters;
            }
            set
            {
                if (value == null)
                {
                    _filters.Clear();
                }
                else
                {
                    _filters = value;
                }
            }
        }

        public object Clone()
        {
            return new QueryConditions()
            {
                Filter = Filter.Select(x => (FilterQuery)x.Clone()).ToList(),
            };
        }
    }
}
