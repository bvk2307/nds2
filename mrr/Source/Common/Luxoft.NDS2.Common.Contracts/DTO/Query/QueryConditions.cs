﻿using Luxoft.NDS2.Common.Contracts.DTO.Query.FilterValidators;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Common.Contracts.DTO.Query
{
    [Serializable]
    public class QueryConditions : EventArgs, ICloneable
    {
        public const ushort DEFAULT_PAGE_SIZE = 200;

        public QueryConditions()
        {            
            PaginationDetails = new DataPagination();
            PageIndex = 1;
            PageSize = DEFAULT_PAGE_SIZE;            
        }

        public DataPagination PaginationDetails
        {
            get;
            private set;
        }

        private uint _pageIndex;

        [Obsolete("Необходимо использовать PaginationDetails")]
        public uint PageIndex
        {
            get 
            { 
                return _pageIndex; 
            }
            set 
            {
                _pageIndex = value;
                PaginationDetails.RowsToSkip = (_pageIndex - 1) * _pageSize;
            }
        }

        private ushort _pageSize;

        [Obsolete("Необходимо использовать PaginationDetails")]
        public ushort PageSize 
        { 
            get
            {
                return _pageSize;
            }
            set
            {
                _pageSize = value;
                PaginationDetails.RowsToTake = _pageSize;
            }
        }
        
        private List<ColumnSort> _sorting = new List<ColumnSort>();
        public List<ColumnSort> Sorting
        {
            get
            {
                return _sorting;
            }
            set
            {
                if (value == null)
                {
                    _sorting.Clear();
                }
                else
                {
                    _sorting = value;
                }                
            }
        }

        private List<FilterQuery> _filters = new List<FilterQuery>();
        public List<FilterQuery> Filter
        {
            get
            {
                return _filters;
            }
            set
            {
                if (value == null)
                {
                    _filters.Clear();
                    //_filters.FilterOperator = FilterQuery.FilterLogicalOperator.NotDefinedOperator;
                }
                else
                {
                    _filters = value;
                }
            }
        }

        public bool FilterValid()
        {
            return FilterValid(new DefaultFilterValidatorFactory());
        }

        public bool FilterValid(IFilterValidatorFactory factory)
        {
            return Filter.All(flt => flt.IsValid(factory.Create(flt.ColumnName, flt.ColumnType)));
        }

        /// <summary>
        /// Возвращает или задает SID текущего пользователя
        /// </summary>
        public string UserSid
        {
            get;
            set;
        }

        public static QueryConditions Empty
        {
            get
            {
                return new QueryConditions();
            }
        }

        public object Clone()
        {
            return new QueryConditions()
            {
                Filter = Filter.Select(x => (FilterQuery)x.Clone()).ToList(),
                PageIndex = PageIndex,
                PageSize = PageSize,
                Sorting = Sorting.Select(x => (ColumnSort)x.Clone()).ToList(),
                UserSid = UserSid,
                PaginationDetails = PaginationDetails.Clone()
            };
        }

        public bool Equals(QueryConditions other)
        {
            if (!EqualsBase(other))
            {
                return false;
            }

            return QueryConditions.EqualsSorting(this, other);
        }

        public bool EqualsBase(QueryConditions other)
        {
            if (other == null)
                return false;

            if (this.GetType() != other.GetType())
                return false;

            var isEquals = this.PageIndex == other.PageIndex && this.PageSize == other.PageSize && this.UserSid == other.UserSid;
            if (isEquals)
            {
                #region Check filtering

                if (this.Filter.Count() == other.Filter.Count())
                {
                    foreach (FilterQuery thisFQ in this.Filter)
                    {
                        var isFilterQuery = false;
                        foreach (FilterQuery otherFQ in other.Filter)
                        {
                            if (thisFQ.ColumnName == otherFQ.ColumnName &&
                                thisFQ.FilterOperator == otherFQ.FilterOperator &&
                                thisFQ.GroupOperator == otherFQ.GroupOperator &&
                                thisFQ.IsComplex == otherFQ.IsComplex &&
                                thisFQ.Filtering.Count() == otherFQ.Filtering.Count())
                            {
                                var isColumnFilter = false;
                                foreach (ColumnFilter thisCF in thisFQ.Filtering)
                                {
                                    isColumnFilter = false;
                                    foreach (ColumnFilter otherCF in otherFQ.Filtering)
                                    {
                                        if (thisCF.ComparisonOperator == otherCF.ComparisonOperator &&
                                            thisCF.Value == otherCF.Value)
                                        {
                                            isColumnFilter = true;
                                            break;
                                        }
                                    }
                                    if (!isColumnFilter)
                                    {
                                        isEquals = false;
                                        break;
                                    }
                                }
                                if (isColumnFilter)
                                {
                                    isFilterQuery = true;
                                    break;
                                }
                            }
                        }
                        if (!isFilterQuery)
                        {
                            isEquals = false;
                            break;
                        }
                    }

                }
                else { isEquals = false; }

                #endregion
            }

            if (isEquals)
            {
                if (this.Sorting.Count == other.Sorting.Count)
                {
                    if (this.Sorting.Any(sort => other.Sorting.Any(otrSort => sort.ColumnKey == otrSort.ColumnKey &&
                                                                              sort.Order != otrSort.Order)))
                    {
                        isEquals = false;
                    }
                }
                else
                {
                    isEquals = false;
                }
            }

            return isEquals;
        }

        public static bool EqualsSorting(QueryConditions qcOne, QueryConditions qcTwo)
        {
            bool ret = false;
            if (qcOne != null && qcTwo != null)
            {
                if (qcOne.Sorting != null && qcTwo.Sorting != null)
                {
                    ret = true;
                    if (qcOne.Sorting.Count() == qcTwo.Sorting.Count())
                    {
                        foreach (ColumnSort colOne in qcOne.Sorting)
                        {
                            bool isColumnSort = false;
                            foreach (ColumnSort colTwo in qcTwo.Sorting)
                            {
                                if (colOne.ColumnKey == colTwo.ColumnKey && colOne.Order == colTwo.Order)
                                {
                                    isColumnSort = true;
                                    break;
                                }
                            }
                            if (!isColumnSort)
                            {
                                ret = false;
                                break;
                            }
                        }
                    }
                    else
                    {
                        ret = false;
                    }
                }
                else if (qcOne.Sorting == null && qcTwo.Sorting == null)
                {
                    ret = true;
                }
            }
            return ret;
        }

        public static bool IsEqualsConditions(QueryConditions left, QueryConditions right)
        {
            if (left == null && right == null)
                return true;

            return left != null && left.EqualsBase(right) && EqualsSorting(left, right);
        }
        
        public QueryConditions Remove(string criteriaName)
        {
            if (_filters != null)
            {
                for (int i = 0; i < _filters.Count; i++)
                {
                    var item = _filters[i];

                    if (item.ColumnName == criteriaName)
                        _filters.Remove(item);
                }
            }

            if (_sorting != null)
            {
                for (int i = 0; i < _sorting.Count; i++)
                {
                    var item = _sorting[i];

                    if (item.ColumnKey == criteriaName)
                        _sorting.Remove(item);
                }
            }

            return this;
        }

        public QueryConditions Relace(string oldName, string newName)
        {
            if (_filters != null)
                foreach (var x in _filters)
                    if (x.ColumnName == oldName)
                        x.ColumnName = newName;

            if (_sorting != null)
                foreach (var x in _sorting)
                    if (x.ColumnKey == oldName)
                        x.ColumnKey = newName;

            return this;
        }
    }
}
