﻿using System;
using System.Xml.Serialization;

namespace Luxoft.NDS2.Common.Contracts.DTO.Query
{
    [Serializable]
    public class ColumnSort : ICloneable
    {
        public enum SortOrder
        {
            None,
            Asc,
            Desc
        }

        public enum SortStyle
        {
            AlphabeticOrder,
            ElementsInGroupOrder,
            AlphabeticGroupsOrder
        }

        public static SortOrder InvertOrder(SortOrder order)
        {
            switch (order)
            {
                case SortOrder.Asc:
                    return SortOrder.Desc;
                case SortOrder.Desc:
                    return SortOrder.Asc;
                default:
                    return SortOrder.None;
            }
        }

        //[XmlIgnore]
        public string ColumnKey { get; set; }

        public bool NullAsZero { get; set; }

        private SortOrder _order;
        public SortOrder Order {
            get
            {
                if (Inverted)
                {
                    return InvertOrder(_order);
                }
                return _order;
            }
            set
            {                
                _order = Inverted ? InvertOrder(value) : value;
            }
        }

        private SortStyle _SortKind = SortStyle.AlphabeticOrder;
        public SortStyle SortKind
        {
            get
            {
                return _SortKind;
            }
            set
            {
                if (_SortKind != value)
                {
                    _SortKind = value;
                }
            }
        }

        public bool Inverted;

        public object Clone()
        {
            return 
                new ColumnSort()
                {
                     ColumnKey = ColumnKey,
                     Order = _order,
                     SortKind = SortKind,
                     Inverted = Inverted,
                     NullAsZero = NullAsZero
                };
        }
    }
}
