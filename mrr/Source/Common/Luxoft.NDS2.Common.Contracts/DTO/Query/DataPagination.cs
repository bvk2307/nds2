﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Query
{
    /// <summary>
    /// Этот класс описывает набор параметров для выборки данных на клиент МРР, связанных с постраничным выводом
    /// </summary>
    [Serializable]
    public class DataPagination
    {
        /// <summary>
        /// Возвращает или задает максимальное кол-во строк данных, которые необходимо вернуть по запросу.
        /// null - означает отсутствие ограничений
        /// </summary>
        public uint? RowsToTake
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает кол-во строк данных, которые необходимо пропустить при выборке
        /// </summary>
        public uint RowsToSkip
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает признак того, что общее число записей, удовлетворяющих условию клиентского фильтра, 
        /// берется из кэша клиента и не требует рассчета на стороне сервера
        /// </summary>
        public bool SkipTotalMatches
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает признак того, что общее число записей, доступных клиенту, 
        /// берется из кэша клиента и не требует рассчета на стороне сервера
        /// </summary>
        public bool SkipTotalAvailable
        {
            get;
            set;
        }

        public DataPagination Clone()
        {
            return new DataPagination
            {
                RowsToTake = RowsToTake,
                RowsToSkip = RowsToSkip,
                SkipTotalAvailable = SkipTotalAvailable,
                SkipTotalMatches = SkipTotalMatches
            };
        }
    }
}
