﻿using System.Xml.Serialization;
using Luxoft.NDS2.Common.Contracts.DTO.Query.FilterValidators;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Common.Contracts.DTO.Query
{
    [Serializable]
    public class FilterQuery : ICloneable
    {
        public FilterQuery()
        {
            UserDefined = false;
            ColumnType = typeof(string);
        }
        
        public FilterQuery(bool userDefined)
        {
            UserDefined = userDefined;
            ColumnType = typeof(string);
        }

        public enum FilterLogicalOperator
        {
            NotDefinedOperator,
            And,
            Or
        }

        public string ColumnName { get; set; }

        private bool _isComplex;
        public bool IsComplex
        {
            get { return _isComplex; }
            set { _isComplex = value; }
        }


        private FilterLogicalOperator _groupOperator = FilterLogicalOperator.And;       
        public FilterLogicalOperator GroupOperator
        {
            get { return _groupOperator; }
            set { _groupOperator = value; }
        }


        public FilterLogicalOperator FilterOperator { get; set; }

        private List<ColumnFilter> _filtering = new List<ColumnFilter>();
        public List<ColumnFilter> Filtering
        {
            get
            {
                return _filtering;
            }
            set
            {
                if (value != _filtering)
                {
                    if (value != null)
                    {
                        _filtering = value;
                    }
                    else
                    {
                        _filtering.Clear();
                    }
                }
            }
        }

        /// <summary>
        /// Возвращает или задает признак того, что фильтр создан пользователем, а не добавлен программно
        /// </summary>
        public bool UserDefined
        {
            get;
            set;
        }

        public object Clone()
        {
            return new FilterQuery
            {
                ColumnName = ColumnName,
                Filtering = Filtering.Select(x => (ColumnFilter)x.Clone()).ToList(),
                FilterOperator = FilterOperator,
                IsComplex = IsComplex,
                UserDefined = UserDefined
            };
        }

        # region Валидация

        [XmlIgnore]
        public Type ColumnType
        {
            get;
            set;
        }

        public bool IsValid(IFilterValidator validator)
        {
            return Filtering.All(flt => validator.IsValid(flt.ComparisonOperator, flt.Value));
        }

        #endregion

        public static bool operator == (FilterQuery a, FilterQuery b)
        {
            // If both are null, or both are same instance, return true.
            if (System.Object.ReferenceEquals(a, b))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)a == null) || ((object)b == null))
            {
                return false;
            }

            // Return true if the fields match:
            return a.ColumnName == b.ColumnName && a.FilterOperator == b.FilterOperator && a.Filtering.All(x => b.Filtering.Contains(x))
                && b.Filtering.All(x => a.Filtering.Contains(x));
        }

        public static bool operator !=(FilterQuery a, FilterQuery b)
        {
            return !(a == b);
        }
    }

    [XmlInclude(typeof(DBNull))]
    [Serializable]
    public class ColumnFilter : ICloneable
    {
        public enum FilterComparisionOperator
        {
            NotDefinedOperator,
            Equals,
            NotEquals,
            LessThan,
            LessThanOrEqualTo,
            GreaterThan,
            GreaterThanOrEqualTo,
            Like,
            Match,
            NotLike,
            DoesNotMatch,
            StartsWith,
            DoesNotStartWith,
            EndsWith,
            DoesNotEndWith,
            Contains,
            DoesNotContain,
            OneOf
        }
        
        public FilterComparisionOperator ComparisonOperator { get; set; }
        public object Value { get; set; }

        private bool _isCaseSensitive = true;
        public bool IsCaseSensitive
        {
            get { return _isCaseSensitive; }
            set { _isCaseSensitive = value; }
        }

        public object Clone()
        {
            return new ColumnFilter
            {
                ComparisonOperator = ComparisonOperator,
                Value = Value
            };
        }

        public override bool Equals(object obj)
        {
            bool ret = false;
            var item = obj as ColumnFilter;

            if (item != null)
                ret = (Value == item.Value) && (ComparisonOperator == item.ComparisonOperator);

            return ret;
        }
      
        public bool HasRoundFactor { get; set; }
        public int RoundFactor { get; set; }

        public override int GetHashCode()
        {
            return Tuple.Create(Value, ComparisonOperator).GetHashCode();
        }

        public override string ToString()
        {
            return Tuple.Create(Value, ComparisonOperator).ToString();
        }

    }
}
