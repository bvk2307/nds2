﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.HiveRequest
{
    public enum HiveRequestType
    {
        DiscrepanciesListForDeclarationCard = 1,
        InvoicesForDiscrepancyCard = 2
    }
}
