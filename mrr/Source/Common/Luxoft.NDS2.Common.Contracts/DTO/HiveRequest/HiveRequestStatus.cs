﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO
{
    public enum HiveRequestStatus
    {
        NewRequest = 1,
        MarkedForLoading = 2,
        StartLoading = 3,
        FinishLoading = 4,
        Error = 5
    }
}
