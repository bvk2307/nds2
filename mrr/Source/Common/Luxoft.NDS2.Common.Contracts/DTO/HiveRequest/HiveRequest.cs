﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Xml.Serialization;
using System.Web.Script.Serialization;

namespace Luxoft.NDS2.Common.Contracts.DTO.HiveRequest
{
    [Serializable]
    public abstract class HiveRequestBase 
    {
        public string GetJsonString()
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string json = serializer.Serialize(this);
            return json; 
        }

        public abstract int GetRequestType();
    }
    
    [Serializable]
    public class HiveRequestDiscrepanciesList : HiveRequestBase
    {
        public string Inn { get; set; }

        public string KppEffective { get; set; }

        public string InnReorganized { get; set; }

        public int PeriodEffective { get; set; }

        public string FiscalYear { get; set; }

        public string CorrectionNumber { get; set; }

        public HiveRequestDiscrepanciesList(string inn,
                                            string kppEffective,
                                            string innReorganized,
                                            int periodEffective,
                                            string fiscalYear,
                                            string correctionNumber)
            : base() 
        {
            Inn = inn;
            KppEffective = kppEffective;
            InnReorganized = innReorganized;
            PeriodEffective = periodEffective;
            FiscalYear = fiscalYear;
            CorrectionNumber = correctionNumber;
        }

        public override int GetRequestType()
        {
            return (int)HiveRequestType.DiscrepanciesListForDeclarationCard;
        }
    }

    [Serializable]
    public class HiveRequestDiscrepancyCard : HiveRequestBase
    {
        public long DiscrepancyId { get; set; }
        public string InvoiceRowKey  { get; set; }
        public string ContractorInvoiceRowKey { get; set; }

        public HiveRequestDiscrepancyCard(long discrepancyId)
            : base()
        {
            DiscrepancyId = discrepancyId;
            InvoiceRowKey = String.Empty;
            ContractorInvoiceRowKey = String.Empty;
        }

        public HiveRequestDiscrepancyCard(long discrepancyId, 
                                            string invoiceRowKey,
                                            string contractorInvoiceRowKey)
            : base()
        {
            DiscrepancyId = discrepancyId;
            InvoiceRowKey = invoiceRowKey;
            ContractorInvoiceRowKey = contractorInvoiceRowKey;
        }
        
        public override int GetRequestType()
        {
            return (int)HiveRequestType.InvoicesForDiscrepancyCard;
        }
    }
}
