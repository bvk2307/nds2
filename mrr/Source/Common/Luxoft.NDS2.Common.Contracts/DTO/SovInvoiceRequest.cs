﻿namespace Luxoft.NDS2.Common.Contracts.DTO
{
    public class SovInvoiceRequest
    {
        public long Id
        {
            get;
            set;
        }

        public RequestStatus Status
        {
            get;
            set;
        }

        #region Итоговые суммы

        public decimal PriceBuyAmountSum { get; set; }

        public decimal PriceBuyNdsAmountSum { get; set; }

        public decimal PriceSellSum { get; set; }

        public decimal PriceSell18Sum { get; set; }

        public decimal PriceSell10Sum { get; set; }

        public decimal PriceSell0Sum { get; set; }

        public decimal PriceNds18Sum { get; set; }

        public decimal PriceNds10Sum { get; set; }

        public decimal PriceTaxFreeSum { get; set; }

        public decimal PriceTotalSum { get; set; }

        public decimal PriceNdsTotalSum { get; set; }

        #endregion
    }
}
