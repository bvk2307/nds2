﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Luxoft.NDS2.Common.Contracts.DTO
{
    [Serializable]
    public class PageResult<TData> : IEnumerable<TData>
    {
        public PageResult()
            : this(new List<TData>(), 0, 0)
        {
        }

        public PageResult(List<TData> data, int totalMatches)
            : this(data, totalMatches, totalMatches)
        {
        }

        public PageResult(List<TData> data, int totalMatches, int totalAvailable)
        {
            Rows = data;
            TotalMatches = totalMatches;
            TotalAvailable = totalAvailable;
        }

        /// <summary>
        /// Записи на странице
        /// </summary>
        public List<TData> Rows { get; set; }

        /// <summary>
        /// Количество записей удовлетворяющих выборке с учетом поиска
        /// </summary>
        public int TotalMatches { get; set; }

        /// <summary>
        /// Количество записей удовлетворяющих выборке БЕЗ учета поиска
        /// </summary>
        public int TotalAvailable { get; set; }

        public IEnumerator<TData> GetEnumerator()
        {
            return Rows.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return Rows.GetEnumerator();
        }
    }
}
