﻿namespace Luxoft.NDS2.Common.Contracts.DTO.OrganizationStructure
{
    public enum SonoType
    {
        CA = 1, 
        MILarge,
        MIPrices,
        MIFederalDistrict,
        MIData,
        Ufns,
        Ifns,
        Other
    }
}
