﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.OrganizationStructure
{
    [Serializable]
    public class Region
    {
        public int FederalDistrictId
        {
            get;
            set;
        }

        public string Code
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public override string ToString()
        {
            return string.Format("{0} - {1}", Code, Name);
        }
    }
}
