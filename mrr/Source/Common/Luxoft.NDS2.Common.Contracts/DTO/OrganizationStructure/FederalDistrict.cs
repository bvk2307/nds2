﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.OrganizationStructure
{
    [Serializable]
    public class FederalDistrict
    {
        public int Id
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }
    }
}
