﻿using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Common.Contracts.DTO
{
    /// <summary> Request results including request parameters. </summary>
    /// <typeparam name="TParams"> Parameters' set type. </typeparam>
    /// <typeparam name="TData"> Result data type. </typeparam>
    [Serializable]
    public class PageRequestResult<TParams, TData>
    {
        public PageRequestResult( TParams parameters, ICollection<TData> data = null )
        {
            Params = parameters;
            Rows = data;
        }

        public TParams Params { get; set; }

        public ICollection<TData> Rows { get; set; }
    }
}