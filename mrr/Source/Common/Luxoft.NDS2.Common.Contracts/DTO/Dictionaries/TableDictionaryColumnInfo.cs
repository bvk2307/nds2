﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Dictionaries
{
    [Serializable]
    public class TableDictionaryColumnInfo
    {
        public string TableName { get; set; }
        public string TableNameEdit { get; set; }
        public string ColumnName { get; set; }
        public string ColumnDateType { get; set; }
        public int Index { get; set; }
        public string Caption { get; set; }
        public bool IsReadOnly { get; set; }
        public bool Visible { get; set; }
        public int Width { get; set; }
        public bool IsKey { get; set; }
        public string TypeSort { get; set; }

        public TableDictionaryColumnInfo()
        {
            SetDefaultValue();
        }

        public void SetDefaultValue()
        {
            this.Caption = String.Empty;
            this.IsReadOnly = true;
            this.Visible = true;
            this.Width = 100;
            this.IsKey = false;
            this.TypeSort = String.Empty;
        }
    }
}
