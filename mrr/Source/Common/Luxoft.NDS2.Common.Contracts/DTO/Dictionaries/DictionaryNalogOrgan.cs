﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Dictionaries
{
    [DataContract]
    [Serializable]
    public class DictionaryNalogOrgan
    {
        public string S_CODE { get; set; }
        public string S_PARENT_CODE { get; set; }
        public string S_NAME { get; set; }
        public string FullName { get { return string.Format("{0}-{1}", S_CODE, S_NAME); } }
        public bool Check { get; set; }
    }
}
