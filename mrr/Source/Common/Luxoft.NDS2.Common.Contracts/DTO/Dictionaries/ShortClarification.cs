﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Dictionaries
{
    [DataContract]
    [Serializable]
    public class ShortClarification
    {
        public string EXPLAIN_ID { get; set; }

        public string ZIP { get; set; }
    }
}
