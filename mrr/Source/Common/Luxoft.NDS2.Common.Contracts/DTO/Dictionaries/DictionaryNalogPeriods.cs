﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Dictionaries
{
    [Serializable]
    public class DictionaryNalogPeriods
    {
        public string CODE { get; set; }
        public string NAME { get; set; }
        public string FullName { get { return string.Format("{0} {1}", CODE, NAME); } }
        public DateTime DateBegin { get; set; }
        public DateTime DateEnd { get; set; }

        public DictionaryNalogPeriods()
        {
            DateBegin = DateTime.Now;
            DateEnd = DateTime.Now;
        }

        public void GenerateBoundary(int year)
        {
            if (CODE == "21")
            {
                DateBegin = new DateTime(year, 01, 01);
                DateEnd = new DateTime(year, 03, 31);
            }
            else if (CODE == "22")
            {
                DateBegin = new DateTime(year, 04, 01);
                DateEnd = new DateTime(year, 06, 30);
            }
            else if (CODE == "23")
            {
                DateBegin = new DateTime(year, 07, 01);
                DateEnd = new DateTime(year, 09, 30);
            }
            else if (CODE == "24")
            {
                DateBegin = new DateTime(year, 10, 01);
                DateEnd = new DateTime(year, 12, 31);
            }
        }
    }
}
