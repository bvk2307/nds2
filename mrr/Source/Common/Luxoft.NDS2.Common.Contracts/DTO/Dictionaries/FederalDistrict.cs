﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Dictionaries
{
    [Serializable]
    [Obsolete]
    public class FederalDistrict
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public List<string> RegionCodes { get; set; }
        public bool Check { get; set; }
        public string FullName { get { return string.Format("{0}", Description); } }
    }
}
