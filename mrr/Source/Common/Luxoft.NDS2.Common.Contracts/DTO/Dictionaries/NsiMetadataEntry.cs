﻿using Luxoft.NDS2.Common.Contract.DTO.SearchEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Luxoft.NDS2.Common.Contracts.DTO.Dictionaries
{
    public enum NsiEntryDataLocationType
    {
        Remote,
        Embedded
    }

    [Serializable]
    public class NsiMetadataEntry
    {
        [XmlAttribute("source")]
        public NsiEntryDataLocationType LocationType { get; set; }
        [XmlAttribute("source")]
        public string Name { get; set; }
        public string TableName { get; set; }
        public string KeyField { get; set; }
        public string CodeField { get; set; }
        public string DescriptionField { get; set; }
        public string CodeFieldComment { get; set; }
        public string DescriptionFieldComment { get; set; }
        public string SourceTarget { get; set; }
        public List<LookupResponse> EmbeddedData { get; set; }
    }
}