﻿using System;
namespace Luxoft.NDS2.Common.Contracts.DTO.Dictionaries
{
    public enum RequestStatusType
    {
        NotRegistered = -1,
        Ok = 0,
        InProcess = 1,
        Completed = 2,
        PartialCompleted = 3,
        NotSuccessInNightMode = 8,
        Error = 9
    }

    /// <summary>
    /// Этот класс представляет статус запроса данных в МС.
    /// </summary>
    public class DataRequestStatus
    {
        /// <summary>
        /// Возвращает или задает тип статуса
        /// </summary>
        public RequestStatusType Type
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает текст статуса
        /// </summary>
        public string Name
        {
            get;
            set;
        }
    }


    /// <summary>
    /// Этот класс представляет статус запроса данных в МС с учетом процесса инициализации.
    /// </summary>
    [Serializable]
    public class DataRequestStatusWitnInitialize
    {
        /// <summary>
        /// Возвращает или задает тип статуса
        /// </summary>
        public RequestStatusType Type
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает признак инициализации
        /// </summary>
        public bool IsIninializing
        {
            get;
            set;
        }
    }
}
