﻿using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Common.Contracts.DTO.Dictionaries
{
    [Serializable]
    public class RegionDetails
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public List<string> UserSIDs { get; set; }
        public bool Check { get; set; }
        public string FullName { get { return string.Format("{0}-{1}", Code, Name); } }
        public string FederalCode { get; set; }

        public void ClearUsers()
        {
            if (UserSIDs == null)
            {
                UserSIDs = new List<string>();
            }
            else
            {
                UserSIDs.Clear();
            }
        }
    }
}
