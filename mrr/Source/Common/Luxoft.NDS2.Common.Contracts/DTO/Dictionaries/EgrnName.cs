﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Dictionaries
{
    [Serializable]
    public class EgrnName
    {
        public string Inn
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }
    }
}
