﻿using System;
using System.Runtime.Serialization;

namespace Luxoft.NDS2.Common.Contracts.DTO.Dictionaries
{
    [Serializable]
    public enum SelectionType
    {
        Hand = 0,
        Template = 1,
        SelectionByTemplate = 2
    }
}
