﻿using System;
namespace Luxoft.NDS2.Common.Contracts.DTO.Dictionaries
{
    /// <summary>
    /// Состояния включения группы элементов в выборке 
    /// </summary>
    public enum SelectionGroupState
    {
        AllIncluded = 1,
        PartiallyIncluded = 2,
        AllExcluded = 0,
        AllExcludedAndDisabled = 3
    }
}
