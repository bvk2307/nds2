﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Dictionaries
{
    [Serializable]
    public class TableDictionaryInfo
    {
        public string TableName { get; set; }
        public string TableCaption { get; set; }
    }
}
