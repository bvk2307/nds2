﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO
{
    [Serializable]
    public enum KnpSyncStatus
    {
        NotForSync = 0,
        ReadyForSync,
        Synchronized
    }
}
