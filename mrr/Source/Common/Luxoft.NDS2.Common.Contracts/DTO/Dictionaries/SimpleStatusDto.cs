﻿using System;
using System.Runtime.Serialization;

namespace Luxoft.NDS2.Common.Contracts.DTO.Dictionaries
{
    [DataContract]
    [Serializable]
    public class SimpleStatusDto
    {
        public int Id { get; set; }

        public string Description { get; set; }
    }
}
