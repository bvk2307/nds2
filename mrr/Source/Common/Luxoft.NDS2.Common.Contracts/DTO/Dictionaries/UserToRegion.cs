﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Dictionaries
{
    [DataContract]
    [Serializable]
    public class UserToRegion
    {
        public string USER_SID { get; set; }
        public string N_SSRF_CODE { get; set; }
    }
}
