﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Dictionaries
{
    [Serializable]
    public class FederalDistrictToRegion
    {
        public int DistrictId { get; set; }
        public string RegionCode { get; set; }
    }
}
