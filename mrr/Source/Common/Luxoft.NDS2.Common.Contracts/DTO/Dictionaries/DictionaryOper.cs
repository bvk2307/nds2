﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Dictionaries
{ 
    [Serializable]
    public sealed class OperCode
    {
        public string ID
        {
            get;
            set;
        }

        public string NAME
        {
            get;
            set;
        }  
    }
}
