﻿using System;
using System.Runtime.Serialization;

namespace Luxoft.NDS2.Common.Contracts.DTO.Dictionaries
{
    [DataContract]
    [Serializable]
    public class DictionaryCommon
    {
        public string S_CODE { get; set; }
        public string S_PARENT { get; set; }
        public string S_NAME { get; set; }

        public decimal ID { get; set; }
        public string NAME { get; set; }

        public string CODE { get; set; }
        public string DESCRIPTION { get; set; }

        public string BIT_CODE { get; set; }
        public string NUM_CODE { get; set; }
    }
}
