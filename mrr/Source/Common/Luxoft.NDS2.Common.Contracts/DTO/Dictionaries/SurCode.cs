﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Dictionaries
{
    [Serializable]
    public sealed class SurCode
    {
        public int Code 
        { 
            get; 
            set; 
        }

        public string Description 
        { 
            get; 
            set; 
        }

        public int ColorARGB 
        { 
            get; 
            set; 
        }

        public bool IsDefault
        {
            get;
            set;
        }
    }
}