﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Dictionaries
{
    [Serializable]
    public class AnnulmentReason
    {
        public int Id
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }
    }
}
