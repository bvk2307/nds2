﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO
{
    /// <summary>
    /// Этот класс описывает объекты передачи данных о версии системы
    /// </summary>
    [Serializable]    
    public class VersionInfo
    {
        /// <summary>
        /// Возвращает или задает уникальный номер версии в формате nn.nn.nnn.nnnn, где n - целое число от 0 до 9
        /// </summary>
        public string VersionNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает дату выхода версии в КПЭ
        /// </summary>
        public DateTime ReleaseDate
        {
            get;
            set;
        }

        /// <summary>
        /// Имя файла с описанием версии
        /// </summary>

        public string ReleaseFileName
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает признак, указывающий на необходимость реиндексации каталога МРР после обновления клиента
        /// </summary>
        public bool CatalogReindexRequired
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает признак того, что текущая версия не известна
        /// </summary>
        public bool Undefined
        {
            get;
            set;
        }
    }
}
