﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO.KnpResult;

namespace Luxoft.NDS2.Common.Contracts.DTO
{
    public enum ResultStatus
    {
        [Display( Name = "Сервер вернул пустой результат" )]
        Undefined,
        [Display( Name = "Успешный результат" )]
        Success,
        [Display( Name = "Данные не найдены" )]
        NoDataFound,
        [Display( Name = "Ошибка выполнения на сервере" )]
        Error,
        [Display( Name = "Нарушение целостности данных" )]
        ConstraintViolation,
        [Display( Name = "Отказано в доступе" )]
        Denied
    }

    public interface IOperationResult
    {
        ResultStatus Status { get; set; }

        string Message { get; set; }
    }

    [Serializable]
    public class OperationResult : IOperationResult
    {
        public OperationResult()
        {
            Status = ResultStatus.Success;
        }

        public ResultStatus Status { get; set; }

        public string Message { get; set; }

        public static OperationResult Failure(string errorMessage)
        {
            return new OperationResult
            {
                Status = ResultStatus.Error,
                Message = errorMessage
            };
        }
    }

    [Serializable]
    public class OperationResult<T> : IOperationResult
    {
        public OperationResult()
        {
            Status = ResultStatus.Success;
        }

        public OperationResult(T defaultResult) 
            : this()
        {
            Result = defaultResult;    
        }

        public T Result { get; set; }

        public ResultStatus Status { get; set; }

        public string Message { get; set; }
    }

    [Serializable]
    public class OperationResult<T1, T2> : IOperationResult where T1 : class, new()
    {
        public OperationResult()
        {
            Status = ResultStatus.Undefined;
        }

        public OperationResult(bool initDefaults)
            : this()
        {
            if (initDefaults)
            {
                Result = new T1();
                Result2 = default(T2);
            }
        }

        public T1 Result { get; set; }

        public T2 Result2 { get; set; }

        public ResultStatus Status { get; set; }

        public string Message { get; set; }
    }
}
