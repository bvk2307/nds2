﻿namespace Luxoft.NDS2.Common.Contracts.DTO.BankAccounts
{
    public enum RequestActor
    {
        Auto = 0,
        User = 1
    }
}
