﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting.TaxPayer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace Luxoft.NDS2.Common.Contracts.DTO.BankAccounts
{
    [Serializable]
    public class BankAccountManualRequest : DeclarationKey
    {
        public string KppEffective { get; set; }

        public bool IsReorginized { get; set; }

        public string UserSid { get; set; }

        public string UserName { get; set; }

        public BankAccountBrief[] Accounts { get; set; }
        
        public string SonoCode { get; set; }

        public static string GetAccountsXml(BankAccountBrief[] accounts)
        {
            var xmlSerializer =
                new XmlSerializer(typeof(BankAccountBrief[]));

            using (var textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, accounts);
                return textWriter.ToString();
            }
        }

        public BankAccountManualRequest() {}

        public BankAccountManualRequest(string inn, string kpp, string kppEffective, bool isReorganized, int typeCode, string periodCode, string fiscalYear,
                                        string sonoCode,
                                        BankAccountBrief[] accounts,
                                        string userSid, string userName)
        {
            Inn = inn;
            Kpp = kpp;
            KppEffective = kppEffective;
            IsReorginized = isReorganized;
            TypeCode = typeCode;
            Period = periodCode;
            Year = fiscalYear;
            SonoCode = sonoCode;
            Accounts = accounts;
            UserSid = userSid;
            UserName = userName;
        }

        public BankAccountRequest ToBankAccountRequest()
        {
            var bank = Accounts.First();
            var end = PeriodHelper.GetEndDate(Period, Year);

            end = DateTime.Compare(end, DateTime.Today) > 0 ? DateTime.Today : end;

            return new BankAccountRequest
            {
                Accounts = Accounts,
                BankNumber = bank.BankNumber,
                Bik = bank.Bik,
                FilialNumber = bank.FilialNumber,
                FiscalYear = Year,
                Inn = Inn,
                Kpp = Kpp,
                KppEffective = KppEffective,
                InnBank = bank.InnBank,
                KppBank = bank.KppBank,
                NameBank = bank.NameBank,
                PeriodBegin = PeriodHelper.GetBeginDate(Period, Year),
                PeriodEnd = end,
                PeriodCode = Period,
                RequestActor = RequestActor.User,
                RequestType = "3",
                SonoCode = SonoCode,
                TypeCode = TypeCode
            };
        }
    }

    public static class PeriodHelper
    {
        #region maps

        static readonly Dictionary<string, int> Begins = new Dictionary<string, int>
            {
                {"1", 1},
                {"2", 4},
                {"3", 7},
                {"4", 10},

                {"21", 1},
                {"22", 4},
                {"23", 7},
                {"24", 10},

                {"51", 1},
                {"54", 4},
                {"55", 7},
                {"56", 10},

                {"01", 1},
                {"02", 2},
                {"03", 3},
                {"04", 4},
                {"05", 5},
                {"06", 6},
                {"07", 7},
                {"08", 8},
                {"09", 9},
                {"10", 10},
                {"11", 11},
                {"12", 12},

                {"71", 1},
                {"72", 2},
                {"73", 3},
                {"74", 4},
                {"75", 5},
                {"76", 6},
                {"77", 7},
                {"78", 8},
                {"79", 9},
                {"80", 10},
                {"81", 11},
                {"82", 12},
            };

        static readonly Dictionary<string, int> Ends = new Dictionary<string, int>
            {
                {"1", 3},
                {"2", 6},
                {"3", 9},
                {"4", 12},

                {"21", 3},
                {"22", 6},
                {"23", 9},
                {"24", 12},

                {"51", 3},
                {"54", 6},
                {"55", 9},
                {"56", 12},

                {"01", 1},
                {"02", 2},
                {"03", 3},
                {"04", 4},
                {"05", 5},
                {"06", 6},
                {"07", 7},
                {"08", 8},
                {"09", 9},
                {"10", 10},
                {"11", 11},
                {"12", 12},

                {"71", 1},
                {"72", 2},
                {"73", 3},
                {"74", 4},
                {"75", 5},
                {"76", 6},
                {"77", 7},
                {"78", 8},
                {"79", 9},
                {"80", 10},
                {"81", 11},
                {"82", 12},
            };

        #endregion

        public static DateTime GetBeginDate(string periodCode, string year)
        {
            return new DateTime(int.Parse(year), Begins[periodCode], 1);
        }

        public static DateTime GetEndDate(string periodCode, string year)
        {
            return new DateTime(int.Parse(year), Ends[periodCode], 1).AddMonths(1).AddDays(-1);
        }
    }
}
