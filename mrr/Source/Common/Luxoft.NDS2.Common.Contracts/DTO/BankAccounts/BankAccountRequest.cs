﻿using Luxoft.NDS2.Common.Contracts.DTO.Reporting.TaxPayer;
using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.BankAccounts
{
    public enum BankAccountRequestStatus
    {
        Created = 1,
        Pulled = 2,
        XmlGenerated = 3,
        AnswerReceived = 4,
        XmlGeneratingError = 5,
        SeodError = 6,
        XmlValidationError = 8
    }
    
    [Serializable]
    public class BankAccountRequest
    {
        public long Id { get; set; }

        public string Inn { get; set; }

        public string Kpp { get; set; }

        public string KppEffective { get; set; }

        public string FiscalYear { get; set; }

        public string PeriodCode { get; set; }

        public string SonoCode { get; set; }

        public DateTime PeriodBegin { get; set; }

        public DateTime PeriodEnd { get; set; }

        public int TypeCode { get; set; }

        public RequestActor RequestActor { get; set; }

        public string RequestType { get; set; }

        public string Bik { get; set; }

        public string InnBank { get; set; }

        public string KppBank { get; set; }

        public string NameBank { get; set; }

        public string BankNumber { get; set; }

        public string FilialNumber { get; set; }

        public BankAccountBrief[] Accounts { get; set; }
    }
}
