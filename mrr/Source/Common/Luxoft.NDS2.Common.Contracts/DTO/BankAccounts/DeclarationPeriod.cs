﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.BankAccounts
{
    [Serializable]
    public class DeclarationPeriod
    {
        public string FiscalYear { get; set; }

        public string PeriodCode { get; set; }

        public string Description { get; set; }

        public int Quarter { get; set; }

        public int Month { get; set; }
    }
}
