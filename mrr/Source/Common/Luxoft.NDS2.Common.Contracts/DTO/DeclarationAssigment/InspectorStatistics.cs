﻿using System;
using System.ComponentModel;

namespace Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment
{
    [Serializable]
    public class InspectorStatistics
    {
        [DisplayName("Идентификатор пользователя")]
        [Description("Идентификатор пользователя")]
        public string SID { get; set; }

        [DisplayName("Имя пользователя")]
        [Description("Имя пользователя")]
        public string NAME { get; set; }

        [DisplayName("Табельный номер")]
        [Description("Табельный номер")]
        public string EMPLOYEE_NUM { get; set; }

        [DisplayName("НД к возмещению: кол-во")]
        public decimal COMPENSATION_CNT { get; set; }
        [DisplayName("НД к возмещению: ндс к возмещению")]
        public decimal NDS_COMPENSATION_AMOUNT { get; set; }
        [DisplayName("НД к уплате: кол-во")]
        public decimal PAY_CNT { get; set; }
        [DisplayName("НД к уплате: Сумма расхожедний по АТ")]
        public decimal CLAIM_AMOUNT { get; set; }

        public bool IS_ACTIVE { get; set; }
    }
}
