﻿using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment
{
    [Serializable]
    public sealed class SetInspectorWorkLoadResult
    {
        public SetInspectorWorkLoadResult()
        {
            SuccesfullRecords = new List<InspectorWorkload>();
            FailedRecords = new List<InspectorWorkload>();
        }

        public List<InspectorWorkload> SuccesfullRecords { get; set; }

        public List<InspectorWorkload> FailedRecords { get; set; }
    }
}
