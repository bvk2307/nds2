﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment
{
    [Serializable]
    public class TaxpayersResult
    {
        public TaxpayersResult()
        {
            SuccesfullRecords = new List<Taxpayer>();
            FailedRecords = new List<Taxpayer>();
        }

        public List<Taxpayer> SuccesfullRecords { get; set; }

        public List<Taxpayer> FailedRecords { get; set; }
    }
}
