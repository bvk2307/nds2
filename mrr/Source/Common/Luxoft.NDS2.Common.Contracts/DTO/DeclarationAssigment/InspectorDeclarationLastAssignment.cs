﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment
{
    [Serializable]
    public class InspectorDeclarationLastAssignment
    {
        [DisplayName("Идентификатор пользователя")]
        [Description("Идентификатор пользователя")]
        public string SID { get; set; }

        [DisplayName("Имя пользователя")]
        [Description("Имя пользователя")]
        public string Name { get; set; }

        [DisplayName("Табельный номер")]
        [Description("Табельный номер")]
        public string EmployeeNum
        {
            get;
            set;
        }
    }
}
