using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment
{
    [Serializable]
    public sealed class ClearInspectorsWorkloadResult
    {
        public ClearInspectorsWorkloadResult()
        {
            SuccesfullRecords = new List<InspectorSonoAssignment>();
            FailedRecords = new List<InspectorSonoAssignment>();
        }

        public List<InspectorSonoAssignment> SuccesfullRecords { get; set; }

        public List<InspectorSonoAssignment> FailedRecords { get; set; }
    }
}