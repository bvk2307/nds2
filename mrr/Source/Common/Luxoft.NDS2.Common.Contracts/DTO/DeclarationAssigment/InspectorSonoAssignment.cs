﻿using System;
using System.ComponentModel;

namespace Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment
{
    [Serializable]
    public class InspectorSonoAssignment
    {
        [DisplayName("Идентификатор пользователя")]
        [Description("Идентификатор пользователя")]
        public string SID { get; set; }

        [DisplayName("Имя пользователя")]
        [Description("Имя пользователя")]
        public string Name { get; set; }

        [DisplayName("Табельный номер")]
        [Description("Табельный номер")]
        public string EmployeeNum
        {
            get;
            set;
        }

        [DisplayName("Код СОНО")]
        [Description("Код СОНО")]
        public string SonoCode { get; set; }


        [DisplayName("Дата добавления")]
        [Description("Дата добавления")]
        public DateTime InsertDate
        {
            get;
            set;
        }

        [DisplayName("Кем добавлен")]
        [Description("Кем добавлен")]
        public string InsertedBy
        {
            get;
            set;
        }

        [DisplayName("Кем отредактирован")]
        [Description("Кем отредактирован")]
        public string UpdatedBy
        {
            get;
            set;
        }

        [DisplayName("Дата редактирования")]
        [Description("Дата редактирования")]
        public DateTime UpdatedDate
        {
            get;
            set;
        }

        [DisplayName("Признак активности")]
        [Description("Признак активности")]
        public bool IsActive
        {
            get;
            set;
        }

    }
}
