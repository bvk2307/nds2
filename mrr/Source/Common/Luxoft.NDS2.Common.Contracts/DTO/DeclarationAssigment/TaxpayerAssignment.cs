﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment
{
    [Serializable]
    public class TaxpayerAssignment
    {
        public string SONO_CODE { get; set; }

        public string INN { get; set; }

        public string KPP { get; set; }

        public string TAX_PAYER_NAME { get; set; }

        public string SID { get; set; }

        public string EMPLOYEE_NUM { get; set; }

        public string INSPECTOR_NAME { get; set; }

        public bool IS_ACTIVE;

        public DateTime UPDATE_DATE { get; set; }

        public bool IS_INSPECTOR_ACTIVE { get; set; }
    }
}
