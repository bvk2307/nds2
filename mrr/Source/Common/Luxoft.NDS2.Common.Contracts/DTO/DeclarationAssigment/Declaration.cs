﻿using System;
using System.ComponentModel;

namespace Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment
{
    [Serializable]
    public class Declaration
    {
        [DisplayName("ИНН")]
        [Description("ИНН")]
        public string Inn { get; set; }

        [DisplayName("КПП")]
        [Description("КПП")]
        public string Kpp { get; set; }

        [DisplayName("Наименование")]
        [Description("Наименование")]
        public string Name { get; set; }

        [DisplayName("Отчетный период")]
        [Description("Отчетный период")]
        public string Period { get; set; }

        [DisplayName("Признак НД")]
        [Description("Признак НД")]
        public string DeclSign { get; set; }

        [DisplayName("Сумма НДС")]
        [Description("Сумма НДС")]
        public decimal CompensationAmount { get; set; }

        [DisplayName("Статус КНП")]
        [Description("Статус КНП")]
        public string KNPStatus { get; set; }

        [DisplayName("")]
        [Description("")]
        public bool IsTaxPayerAttached { get; set; }

        [DisplayName("Сумма расхождений по АТ")]
        [Description("Сумма расхождений по АТ")]
        public decimal? ClaimAmount { get; set; }

        public long? DeclarationVersionId { get; set; }

        public long Id { get; set; }
    }

    [Serializable]
    public class InspetorDeclarationDistribution
    {
        public Declaration Declaration { get; set; }
        public InspectorSonoAssignment Inspector { get; set; }
    }

    [Serializable]
    public class InspectorDeclarationNotDistribution
    {
        public Declaration Declaration { get; set; }
        public InspectorDeclarationLastAssignment InspectorLastAssignment { get; set; }
    }
}
