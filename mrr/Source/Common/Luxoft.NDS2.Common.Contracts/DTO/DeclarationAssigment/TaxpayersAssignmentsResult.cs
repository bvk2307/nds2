﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment
{
    [Serializable]
    public sealed class TaxpayersAssignmentsResult
    {
        public TaxpayersAssignmentsResult()
        {
            SuccesfullRecords = new List<TaxpayerAssignment>();
            FailedRecords = new List<TaxpayerAssignment>();
        }

        public List<TaxpayerAssignment> SuccesfullRecords { get; set; }

        public List<TaxpayerAssignment> FailedRecords { get; set; }

    }
}
