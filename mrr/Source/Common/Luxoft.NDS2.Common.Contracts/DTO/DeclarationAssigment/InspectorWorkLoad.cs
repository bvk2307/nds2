﻿using System;
using System.ComponentModel;

namespace Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment
{
    [Serializable]
    public class InspectorWorkload
    {
        public InspectorSonoAssignment Inspector { get; set; }

        [DisplayName("Код из СОНО")]
        [Description("Код из СОНО")]
        public string SonoCode { get; set; }

        [DisplayName("Разрешение на обрабокту НДС к возмещению")]
        [Description("Разрешение на обрабокту НДС к возмещению")]
        public bool HasPaymentPermission { get; set; }

        [DisplayName("Разрешение на обрабокту НДС к уплате")]
        [Description("Разрешение на обрабокту НДС к уплате")]
        public bool HasCompensationPermission { get; set; }

        [DisplayName("Сумма расхождений по АТ")]
        [Description("Сумма расхождений по АТ")]
        public decimal MaxPaymentCapacity { get; set; }

        [DisplayName("НДС к возмещению")]
        [Description("НДС к возмещению")]
        public decimal MaxCompenationCapacity { get; set; }


        [DisplayName("Дата добавления")]
        [Description("Дата добавления")]
        public DateTime InsertDate
        {
            get;
            set;
        }

        [DisplayName("Кем добавлен")]
        [Description("Кем добавлен")]
        public string InsertedBy
        {
            get;
            set;
        }

        [DisplayName("Кем отредактирован")]
        [Description("Кем отредактирован")]
        public string UpdatedBy
        {
            get;
            set;
        }

        [DisplayName("Дата редактирования")]
        [Description("Дата редактирования")]
        public DateTime UpdatedDate
        {
            get;
            set;
        }

        [DisplayName("Распределение нагрузки приостановлено")]
        [Description("Распределение нагрузки приостановлено")]
        public bool IsPaused
        {
            get;
            set;
        }

        [DisplayName("Признак активности")]
        [Description("Признак активности")]
        public bool IsActive
        {
            get;
            set;
        }
    }
}
