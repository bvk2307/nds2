﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment
{
    public class InspectorDeclaration
    {
        public string Inn { get; set; }

        public string Kpp { get; set; }

        public string Name { get; set; }

        public string Period { get; set; }

        public string DeclSign { get; set; }

        public decimal VATAmount { get; set; }

        public decimal DifferenceAmount { get; set; }

        public bool IsAttachedToTaxPayer { get; set; }
    }
}
