﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment
{
    [Serializable]
    public class Taxpayer
    {
        public string SONO_CODE { get; set; }

        [DisplayName("Флаг выбора записи")]
        [Description("Флаг выбора записи")]
        public bool IsChecked { get; set; }

        [DisplayName("ИНН")]
        [Description("ИНН налогоплательщика")]
        public string INN { get; set; }

        [DisplayName("КПП")]
        [Description("КПП налогоплательщика")]
        public string KPP { get; set; }

        [DisplayName("Наименование")]
        [Description("Наименование налогоплательщика")]
        public string NAME { get; set; }
    }
}
