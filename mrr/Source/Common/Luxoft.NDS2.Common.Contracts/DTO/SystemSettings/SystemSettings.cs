﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.SystemSettings
{
    /// <summary>
    /// Этот класс реализует объект передачи данных системных настроек интеграции СЭОД и АСК НДС-2
    /// </summary>
    [Serializable]
    public class SystemSettings
    {
        # region Повторная отправка автотребования

        /// <summary>
        /// Возвращает или задает количество секунд ожидания ЭОД-5 в ответ на АСК-1 и АСК-2 
        /// или время, через которое повторяется попытка отправки потоков АСК-1 и АСК-2
        /// </summary>
        public int ClaimResendingTimeout
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает количество попыток повторной отправки потоков АСК-1 и АСК-2, 
        /// в ответ на которые не был получен ЭОД-5
        /// </summary>
        public int ResendingAttempts
        {
            get;
            set;
        }

        # endregion

        # region Время ожидания

        /// <summary>
        /// Возвращает или задает время вручения требвоания\истребования НП в секундах
        /// </summary>
        public int ClaimDeliveryTimeout
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает время ответа на требование в секундах
        /// </summary>
        public int ClaimExplainTimeout
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает время ответа на истребование в секундах
        /// </summary>
        public int ReclaimReplyTimeout
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает время ввода ответа на истребование в секундах
        /// </summary>
        public int ReplyEntryTimeout
        {
            get;
            set;
        }

        # endregion

        # region Клонирование и сравнение

        /// <summary>
        /// Клонирует исходный объект
        /// </summary>
        /// <returns></returns>
        public SystemSettings Clone()
        {
            return new SystemSettings
            {
                ClaimDeliveryTimeout = ClaimDeliveryTimeout,
                ClaimResendingTimeout = ClaimResendingTimeout,
                ClaimExplainTimeout = ClaimExplainTimeout,
                ReclaimReplyTimeout = ReclaimReplyTimeout,
                ReplyEntryTimeout = ReplyEntryTimeout,
                ResendingAttempts = ResendingAttempts
            };
        }

        /// <summary>
        /// Сравнивает два объекта
        /// </summary>
        /// <param name="compareTo"></param>
        /// <returns></returns>
        public bool EqualsTo(SystemSettings compareTo)
        {
            return ClaimDeliveryTimeout == compareTo.ClaimDeliveryTimeout
                && ClaimExplainTimeout == compareTo.ClaimExplainTimeout
                && ClaimResendingTimeout == compareTo.ClaimResendingTimeout
                && ReclaimReplyTimeout == compareTo.ReclaimReplyTimeout
                && ReplyEntryTimeout == compareTo.ReplyEntryTimeout
                && ResendingAttempts == compareTo.ResendingAttempts;
        }

        # endregion
    }
}
