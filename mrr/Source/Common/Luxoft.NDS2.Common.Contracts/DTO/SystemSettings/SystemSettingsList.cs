﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.SystemSettings
{
    /// <summary>
    /// Этот класс реализует объект передачи данных системных настроек списков
    /// </summary>
    [Serializable]
    public class SystemSettingsList
    {
   
        /// <summary>
        /// Возвращает или задает идентификатор списка
        /// </summary>
        public long Id
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает название списка
        /// </summary>
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает тип списка
        /// </summary>
        public int ListType
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает кем был создан список
        /// </summary>
        public string CreatedBy
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает дату создания списка
        /// </summary>
        public DateTime CreatedDate
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает активен ли список
        /// </summary>
        public bool IsActive
        {
            get;
            set;
        }

        public string[] InnList
        {
            get;
            set;
        }
    }
}
