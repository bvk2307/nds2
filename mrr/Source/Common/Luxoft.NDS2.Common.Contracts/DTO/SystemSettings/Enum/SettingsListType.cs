﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.SystemSettings.Enum
{
    public enum SettingsListType
    {
        /// <summary>
        ///     Белый список
        /// </summary>
        WhiteList = 1,

        /// <summary>
        ///     Список исключений из выборок
        /// </summary>
        ExcludeList = 2,

        /// <summary>
        ///     Список налогового мониторинга
        /// </summary>
        TaxMonitorList = 3
    }
}
