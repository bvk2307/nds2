﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Security
{
    public enum UserRole
    {
        Analyst = 1, 
        Approver, 
        Methodologist,   
        Inspector,    
        Sender,  
        Manager,
        Observer 
    };
}
