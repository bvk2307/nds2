﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Common.Contracts.DTO.Security.Helpers
{
   /// <summary> Auxilaries around of <see cref="InspectionGroup"/>. </summary>
   public static class InspectionGroupHelper
   {
      private static readonly Lazy<Dictionary<InspectionGroup, string[]>> s_lzTopSpecialGroups = 
         new Lazy<Dictionary<InspectionGroup, string[]>>( CreateTopSpecialGroups );

      /// <summary> Defines an inspection group by an organization code. Returns 'null' for a common inspection ('ИФНС'). </summary>
      /// <param name="organizationCode"></param>
      /// <returns></returns>
      public static InspectionGroup? GetInspectionGroupByOrganizationCode( string organizationCode )
      {
         if ( string.IsNullOrEmpty( organizationCode ) )
            throw new ArgumentNullException( "organizationCode" );
            if ( organizationCode.Length <= Constants.SystemPermissions.UFMSKeyPostfix.Length )
                throw new ArgumentOutOfRangeException( "organizationCode", String.Format( "The parameter value: '{0}' must be longer then UFMS' postfix: '{1}'", organizationCode, Constants.SystemPermissions.UFMSKeyPostfix ) );

            InspectionGroup? inspectionGroup = null;
         var specialGroup = 
            ( from inspecGroup in TopSpecialGroups from groupCode in inspecGroup.Value
              select new { Group = inspecGroup.Key, Code = groupCode } 
            ).FirstOrDefault( group => string.Equals( group.Code, organizationCode, Constants.KeysComparison ) );

         if ( specialGroup != null )
            inspectionGroup = specialGroup.Group;
         else if ( organizationCode.EndsWith( Constants.SystemPermissions.UFMSKeyPostfix, Constants.KeysComparison ) )
            inspectionGroup = InspectionGroup.UFNS;

         return inspectionGroup;
      }

      /// <summary> Does an inspection group include an organization defined by code <paramref name="organizationCode"/>? </summary>
      /// <remarks> Doesn't support yet <paramref name="inspectionGroup"/> with values <see cref="InspectionGroup.MILarge"/> and <see cref="InspectionGroup.MIKK"/>. </remarks>
      /// <param name="organizationCode"></param>
      /// <param name="inspectionGroup"></param>
      /// <param name="inspectionGroupCode"></param>
      /// <returns></returns>
      public static bool IsInspectionGroupIncludingOrganizationCode( 
         string organizationCode, InspectionGroup inspectionGroup, string inspectionGroupCode )

      {
         if ( string.IsNullOrEmpty( organizationCode ) )
            throw new ArgumentNullException( "organizationCode" );
         if ( inspectionGroup != InspectionGroup.CA && inspectionGroup != InspectionGroup.UFNS )
            throw new ArgumentOutOfRangeException( "inspectionGroup", string.Format( "The parameter must have value: {0} or {1}", InspectionGroup.CA, InspectionGroup.UFNS ) );
         if ( string.IsNullOrEmpty( inspectionGroupCode ) )
            throw new ArgumentNullException( "inspectionGroupCode" );
         if ( inspectionGroupCode.Length <= Constants.SystemPermissions.UFMSKeyPostfix.Length )
            throw new ArgumentOutOfRangeException( "inspectionGroupCode", String.Format( "The parameter value: '{0}' must be longer then UFMS' postfix: '{1}'", inspectionGroupCode, Constants.SystemPermissions.UFMSKeyPostfix ) );

         bool isIncluding = false;
         switch ( inspectionGroup )
         {case InspectionGroup.CA:
            isIncluding = true;
            break;
         case InspectionGroup.UFNS:
            isIncluding = organizationCode.StartsWith( 
               inspectionGroupCode.Substring( 0, inspectionGroupCode.Length - Constants.SystemPermissions.UFMSKeyPostfix.Length ), 
               Constants.KeysComparison );
            break;
         }

         return isIncluding;
      }

      /// <summary> Special tax organization code groups including 'Центральный аппарат', 'Межрегиональные инспекции по камеральному контролю' (МИ по КК), 'Межрегиональные инспекции по крупнейшим налогоплательщикам' (МИ по КН) but non including 'Управления ФНС'. </summary>
      public static IDictionary<InspectionGroup, string[]> TopSpecialGroups
      {
         get { return s_lzTopSpecialGroups.Value; }
      }

      private static Dictionary<InspectionGroup, string[]> CreateTopSpecialGroups()
      {
         Dictionary<InspectionGroup, string[]> topSpecialGroups = new Dictionary<InspectionGroup, string[]>(3);
         topSpecialGroups.Add( InspectionGroup.CA, new[] { Constants.SystemPermissions.CentralDepartmentCode } );
         topSpecialGroups.Add( InspectionGroup.MIKK, new[] { Constants.SystemPermissions.MIKKCode } );
         topSpecialGroups.Add( InspectionGroup.MILarge, new[] { "9971", "9972", "9973", "9974", "9975", "9976", "9977", "9978", "9979" } );

         return topSpecialGroups;
      }

   }
}