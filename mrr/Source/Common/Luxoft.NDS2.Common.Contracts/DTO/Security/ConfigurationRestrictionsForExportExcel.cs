﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Security
{
    /// <summary>
    /// Данные об ограничениях прав на экспорт в Excel
    /// </summary>
    [Serializable]    
    public class ConfigurationRestrictionsForExportExcel
    {
        /// <summary>
        /// Ограничение выгрузка в эксель списком инспекций
        /// </summary>
        public bool InspectionNotRestricted
        {
            get;
            set;
        }

        /// <summary>
        /// Доступнось деклараций к уплате для выгрузки в эксель
        /// </summary>
        public bool AllowPurchase
        {
            get;
            set;
        }
    }
}
