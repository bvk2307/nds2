﻿using System;
using CommonComponents.Security.Authorization;

namespace Luxoft.NDS2.Common.Contracts.DTO.Security
{
    [Serializable]
    public class AccessRight
    {
        public string Name { get; set; }
        public PermissionType PermType { get; set; }
        public string StructContext { get; set; }

        public override int GetHashCode()
        {
            return string.Format("{0}{1}{2}", Name, PermType, StructContext).GetHashCode();
        }

        public override string ToString()
        {
            return string.Format("{0}:{1}", PermType, Name);
        }
    }
}
