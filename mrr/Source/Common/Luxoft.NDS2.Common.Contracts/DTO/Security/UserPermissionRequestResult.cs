﻿namespace Luxoft.NDS2.Common.Contracts.DTO.Security
{
    /// <summary> Result values for a user permission request. </summary>
    public enum UserPermissionRequestResult
    {
        Denied = (int)default(UserPermissionRequestResult),
        Granted,
        NoData
    }
}