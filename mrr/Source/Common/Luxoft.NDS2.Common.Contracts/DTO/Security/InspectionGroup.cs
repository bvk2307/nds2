﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Security
{
    public enum InspectionGroup
    {
        CA      = 0,    //Центральный аппарат
        MIKK    = 1,    //МИ по камеральному контролю  
        MILarge = 2,    //МИ по крупнейшим
        UFNS    = 3     //УФНС
    };
}
