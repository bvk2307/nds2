﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Security
{
    [Serializable]
    public class UserAccessPolicy
    {
        public bool FullAccess { get; set; }
        public List<string> AllowedInspections { get; private set; }

        public UserAccessPolicy()
        {
            AllowedInspections = new List<string>();
        }

        public override string ToString()
        {
            string ret = FullAccess ? "FULL ACCESS" : "RESTRICT ACCESS";

            ret = String.Concat(ret, " - ", String.Join(", ", AllowedInspections));

            return ret;
        }
    }
}
