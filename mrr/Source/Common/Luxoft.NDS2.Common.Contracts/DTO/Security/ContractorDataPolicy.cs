﻿using System;
using System.Collections;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;

namespace Luxoft.NDS2.Common.Contracts.DTO.Security
{
    /// <summary>
    /// Этот класс описывает данные об ограничениях пользователей
    /// </summary>
    [Serializable]    
    public class ContractorDataPolicy
    {
        #region Ограничения по цепочкам переходов сопоставленных записей
        
        /// <summary>
        /// Возвращает или задает признак того, что навигация по цепочкам сделок никак не ограничена по глубине
        /// </summary>
        public bool NotRestricted
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает макс. глубину просмотра цепочки от декларации к уплате НДС
        /// </summary>
        public int MaxLevelSales
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает макс. глубину просмотра цепочки от декларации к возмешению НДС
        /// </summary>
        public int MaxLevelPurchase
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает признак того, что начиная движение от декларации к возмещению от продавца можно перейти на цепочку покупателя
        /// </summary>
        public bool AllowPurchase
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает макс. кол-во звеньев доступное в отчете Навигатор
        /// </summary>
        public int MaxNavigatorChains
        {
            get;
            set;
        }

        #endregion
    }
}
