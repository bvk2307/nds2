﻿using System;
using System.Collections;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;

namespace Luxoft.NDS2.Common.Contracts.DTO.Security
{
    /// <summary>
    /// Этот класс описывает ключ по которому производится привязка ограничений
    /// для текущего пользователя
    /// </summary>
    [Serializable]    
    public class UserRestrictionKey
    {
        private readonly Tuple<UserRole, InspectionGroup?> dtoTuple;

        public UserRestrictionKey(UserRole role, InspectionGroup? group)
        {
            dtoTuple = Tuple.Create(role, group);
        }

        public UserRole Role
        {
            get { return dtoTuple.Item1; }
        }

        public InspectionGroup? Group
        {
            get { return dtoTuple.Item2; }
        }


        public override bool Equals(object obj)
        {
            bool ret = false;
            var item = obj as UserRestrictionKey;

            if (item != null)
                ret = (this.Role == item.Role) && (this.Group == item.Group);

            return ret;
        }

        public override int GetHashCode()
        {
            return dtoTuple.GetHashCode();
        }

        public override string ToString()
        {
            return dtoTuple.ToString();
        }
    }
}
