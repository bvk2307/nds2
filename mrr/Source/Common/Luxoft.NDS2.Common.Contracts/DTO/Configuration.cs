﻿using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Common.Contracts.DTO
{
    /// <summary>
    /// Контейнер для текущего и дефолтного значения парметра-идикатора
    /// </summary>
    [Serializable]
    public class BoolParam
    {
        public bool Value { get; set; }
        public bool DefaultValue { get; set; }
    }

    /// <summary>
    /// Контейнер для текущего и дефолтного значения парметра-целого
    /// </summary>
    [Serializable]
    public class IntParam
    {
        public int Value { get; set; }
        public int DefaultValue { get; set; }
    }

    /// <summary>
    /// Контейнер для текущего и дефолтного значения парметра-времени
    /// </summary>
    [Serializable]
    public class TimeParam
    {
        public TimeSpan Value { get; set; }
        public TimeSpan DefaultValue { get; set; }
    }

    /// <summary>
    /// Контейнер для текущего и дефолтного значения парметра-строки
    /// </summary>
    [Serializable]
    public class StringParam
    {
        public string Value { get; set; }
        public string DefaultValue { get; set; }
    }

    ///<summary>
    /// Этот класс реализует промежуточный контейнер для значений параметра
    /// (текущего и по умолчанию)
    /// </summary>
    public class ParamValueContainer
    {
        public string Value { get; set; }
        public string DefaultValue { get; set; }

        public static ParamValueContainer Default<T>()
        {
            return new ParamValueContainer {Value = default(T).ToString(), DefaultValue = default(T).ToString()};
        }
    }

    /// <summary>
    /// Этот класс описывает DTO системных настроек
    /// </summary>
    [Serializable]
    public class Configuration
    {
        public IntParam  TR_TimerPeriodSec { get; set; }

        public TimeParam TR_ReportBuildTimeBegin { get; set; }
        public TimeParam TR_ReportBuildTimeEnd { get; set; }

        public StringParam TR_GP3_InputFileNameFormat { get; set; }
        public StringParam TR_GP3_InputFilesLocation { get; set; }
        public StringParam TR_ProcessMachineName { get; set; }
        public StringParam TR_ReportFileNameFormat { get; set; }
        public StringParam TR_ReportFilesLocation { get; set; }
        public StringParam Number_invoice_warning { get; set; }
        public StringParam ClientPath { get; set; }

        public IntParam ServiceSelectionThreadCountMax { get; set; }
        public IntParam ReclaimJobThreadCountMax { get; set; }
        public BoolParam ReclaimLogInfoMessageUse { get; set; }
        public BoolParam ReclaimManualCheckDocUse { get; set; }
    
    }

    /// <summary>
    /// Перечисление параметров системных настроек
    /// </summary>
    public enum ConfigurationParamName
    {
        TR_GP3_InputFileNameFormat,
        TR_GP3_InputFilesLocation,
        TR_ProcessMachineName,
        TR_ReportFileNameFormat,
        TR_ReportFilesLocation,
        TR_TimerPeriodSec,
        TR_ReportBuildTimeBegin,
        TR_ReportBuildTimeEnd,
        Number_invoice_warning,
        ClientPath,
        ServiceSelectionThreadCountMax,
        ReclaimJobThreadCountMax,
        ReclaimLogInfoMessageUse,
        ReclaimManualCheckDocUse
    }
}