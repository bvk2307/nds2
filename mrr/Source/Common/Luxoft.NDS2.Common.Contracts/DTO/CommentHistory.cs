﻿using System;
using System.ComponentModel;

namespace Luxoft.NDS2.Common.Contracts.DTO
{
    [Serializable]
    public sealed class CommentHistory
    {
        public long Object_Id { get; set; }

        [DisplayName("Дата")]
        [Description("Дата и время добавления комментария")]
        public DateTime? Issue_Date { get; set; }

        [DisplayName("Автор")]
        [Description("ФИО автора комментария")]
        public string Author { get; set; }

        [DisplayName("Комментарий")]
        [Description("Комментарий")]
        public string Comment_Text { get; set; }

        public ObjectType Object_Type_Id { get; set; }
    }
}