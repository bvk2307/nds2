﻿using System;
using System.Linq;

namespace Luxoft.NDS2.Common.Contracts.DTO.AccessRestriction
{
    /// <summary>
    /// Контекст доступа
    /// </summary>
    [Serializable]
    public class AccessContext
    {
        /// <summary>
        /// Массив операционных контекстов
        /// </summary>
        public OperationAccessContext[] Operations { get; set; }

        public OperationAccessContext this[string name]
        {
            get
            {
                return Operations.First(x => x.Name == name);
            }
        }
    } 
}
