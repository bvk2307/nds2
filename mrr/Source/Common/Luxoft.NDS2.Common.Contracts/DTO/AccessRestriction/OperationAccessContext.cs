﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.AccessRestriction
{
    /// <summary>
    /// Контекст доступа к операции
    /// </summary>
    [Serializable]
    public class OperationAccessContext
    {
        /// <summary>
        /// Название операции
        /// </summary>
        public string Name 
        { 
            get; 
            set; 
        }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description 
        { 
            get; 
            set; 
        }

        /// <summary>
        /// Наличие ограничения
        /// </summary>
        public bool IsRestricted 
        { 
            get; 
            set; 
        }

        /// <summary>
        /// Доступные регионы
        /// </summary>
        public string[] AvailableRegions 
        { 
            get; 
            set; 
        }

        /// <summary>
        /// Доступные инспекции
        /// </summary>
        public string[] AvailableInspections 
        { 
            get; 
            set; 
        }
    }
}
