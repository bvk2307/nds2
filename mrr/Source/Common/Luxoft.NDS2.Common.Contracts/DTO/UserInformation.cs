﻿using System;
using System.ComponentModel;

namespace Luxoft.NDS2.Common.Contracts.DTO
{
    /// <summary>
    /// Этот класс описывает объекты передачи данных о пользовательской информации
    /// </summary>
    [Serializable]    
    public class UserInformation
    {
        [DisplayName("Идентификатор пользователя")]
        [Description("Идентификатор пользователя")]
        public string Sid
        {
            get;
            set;
        }

        [DisplayName("Имя пользователя")]
        [Description("Имя пользователя")]
        public string Name
        {
            get;
            set;
        }

        [DisplayName("Табельный номер")]
        [Description("Табельный номер")]
        public string EmployeeNum
        {
            get;
            set;
        }

        public override string ToString()
        {
            return Name;
        }

    }
}
