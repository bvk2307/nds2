﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contract.DTO.SearchEntity
{
    /// <summary>контейнер для передачи коллекции фильтров между клиентом и серовером</summary>
    [Serializable]
    public class FilterNode : IFilterTreeItem
    {
        public bool IsAggregate { get; set; }
        public List<LinkNode> Child { get; set; }
        public FilterNode()
        {
            Child = new List<LinkNode>();
            IsAggregate = false;
        }

        public IFilterTreeItem Clone(bool withAggregate = true)
        {
            if ((!withAggregate) && (this.IsAggregate))
                return null;
            
            FilterNode res = new FilterNode();
            res.IsAggregate = this.IsAggregate;
            Child.ForEach((c) => 
                {
                    IFilterTreeItem item = c.Item.Clone(withAggregate);
                    if (item != null)
                        res.Child.Add(new LinkNode(c.Link, item)); 
                });
            
            if (res.Child.Count == 0)
                return null;
            return res;
        }

        public LinkNode FindFilterValue(string criteria)
        {
            foreach (LinkNode item in this.Child)
            {
                if ((item.Item is FilterValue) && (item.Item as FilterValue).Criteria == criteria)
                    return item;
                if (item.Item is FilterNode)
                {
                    LinkNode res = (item.Item as FilterNode).FindFilterValue(criteria);
                    if (res != null)
                        return res;
                }
            }
            return null;
        }

    }
}
