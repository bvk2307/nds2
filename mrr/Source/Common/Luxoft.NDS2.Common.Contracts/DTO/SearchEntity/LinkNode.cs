﻿using Luxoft.NDS2.Common.Contract.DTO.SearchEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contract.DTO.SearchEntity
{
    [Serializable]
    public class LinkNode
    {
        public BoolOperator Link { get; set; }
        public IFilterTreeItem Item { get; set; }
        public LinkNode()
        {
        }
        public LinkNode(BoolOperator link, IFilterTreeItem item)
        {
            Link = link;
            Item = item;
        }
        public FilterValue GetFilterValue()
        {
            return Item as FilterValue;
        }
    }
}
