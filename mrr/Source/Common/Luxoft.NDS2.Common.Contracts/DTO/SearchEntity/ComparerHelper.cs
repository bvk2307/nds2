﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using Luxoft.NDS2.Common.Contract.DTO.SearchEntity;

namespace Luxoft.NDS2.Common.Contract.DTO.SearchEntity
{
    /// <summary>дополнительные методы для работы с оператором</summary>
    public static class ComparerHelper
    {
        public static CompareOperator DefaultComparer(ParameterType pType)
        {
            switch (pType)
            {
                case ParameterType.DateTime:
                case ParameterType.Int64:
                case ParameterType.Boolean:
                    return CompareOperator.Equals;
                case ParameterType.String:
                    return CompareOperator.Like;
                default:
                    return CompareOperator.Equals;
            }
        }

        /// <summary>для указанного типа данных вернем список допустимых операторов</summary>
        public static List<CompareOperator> GetCompareresList(ParameterType pType)
        {
            switch (pType)
            {
                case ParameterType.DateTime:
                case ParameterType.Int64:
                case ParameterType.Decimal:
                case ParameterType.Money:
                    return new List<CompareOperator> { CompareOperator.Equals, CompareOperator.NotEquals, CompareOperator.More, CompareOperator.MoreOrEquals,
                                                       CompareOperator.Less, CompareOperator.LessOrEquals, CompareOperator.Between, 
                                                       CompareOperator.Empty, CompareOperator.NotEmpty};
                case ParameterType.Boolean:
                    return new List<CompareOperator> { CompareOperator.Equals };
                case ParameterType.String:
                    return new List<CompareOperator> { CompareOperator.Like, CompareOperator.NotLike, CompareOperator.Empty, CompareOperator.NotEmpty };
                default:
                    return new List<CompareOperator>();
            }
        }
        /// <summary>по UI имени оператора получим сам оператор</summary>
        public static CompareOperator ParseDisplayName(string value)
        {
            FieldInfo[] values = typeof(CompareOperator).GetFields();
            foreach (FieldInfo item in values)
            {
                DisplayNameAttribute[] attrs = (DisplayNameAttribute[])item.GetCustomAttributes(typeof(DisplayNameAttribute), false);
                DisplayNameAttribute attr = attrs.Where(a => a.Description == value).FirstOrDefault();
                if (attr != null)
                    return (CompareOperator)Enum.Parse(typeof(CompareOperator), item.Name);
            }
            return CompareOperator.Unknown;
        }
        public static ValueKind GetValueKind(ParameterType pType, CompareOperator comparer)
        {
            switch (pType)
            {
                case ParameterType.DateTime:
                case ParameterType.Int64:
                case ParameterType.Decimal:
                case ParameterType.Money:
                    if (comparer == CompareOperator.Equals || comparer == CompareOperator.NotEquals)
                        return ValueKind.SingleCollection;
                    if (comparer == CompareOperator.Between)
                        return ValueKind.DoubleCollection;
                    if (comparer == CompareOperator.More || comparer == CompareOperator.MoreOrEquals ||
                        comparer == CompareOperator.Less || comparer == CompareOperator.LessOrEquals)
                        return ValueKind.SingleValue;
                    return ValueKind.EmptyValue;
                case ParameterType.Boolean:
                    return ValueKind.SingleValue;
                case ParameterType.Lookup:
                    return ValueKind.SingleValue;
                case ParameterType.String:
                    if (comparer == CompareOperator.Like || comparer == CompareOperator.NotLike)
                        return ValueKind.SingleCollection;
                    return ValueKind.EmptyValue;
                default:
                    return ValueKind.EmptyValue;
            }
        }
    }
}
