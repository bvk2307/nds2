﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Luxoft.NDS2.Common.Contract.DTO.SearchEntity
{
    /// <summary>отображаемое на UI имя оператора</summary>
    public class DisplayNameAttribute : DescriptionAttribute
    {
        public DisplayNameAttribute(string syntax)
            : base(syntax)
        { }
    }
}
