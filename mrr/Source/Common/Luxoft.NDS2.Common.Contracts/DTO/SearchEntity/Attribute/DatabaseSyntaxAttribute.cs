﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Luxoft.NDS2.Common.Contract.DTO.SearchEntity
{
    /// <summary>написание оператора в синтаксисе базы</summary>
    public class DatabaseSyntaxAttribute : DescriptionAttribute
    {
        public DatabaseSyntaxAttribute(string syntax)
            : base(syntax)
        { }
    }
}
