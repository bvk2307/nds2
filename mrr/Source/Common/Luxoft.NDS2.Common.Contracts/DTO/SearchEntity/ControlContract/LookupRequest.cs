﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contract.DTO.SearchEntity
{
    [Serializable]
    public class LookupRequest
    {
        public string SourceName { get; set; }
        public string IdentityFieldName { get; set; }
        public string CodeFieldName { get; set; }
        public string ValueFieldName { get; set; }
        public string TargetSchema { get; set; }
        public string FilterValue { get; set; }
        public string DataSourceKey { get; set; }
    }
}
