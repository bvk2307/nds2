﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Luxoft.NDS2.Common.Contract.DTO.SearchEntity
{
    [Serializable]
    public class LookupResponse
    {
        public string Code { get; set; }
        /// <summary>
        /// Selected
        /// </summary>
        public bool IsSelected { get; set; }
        /// <summary>
        /// Code
        /// </summary>
        public string Value1 { get; set; }
        /// <summary>
        /// Name or description
        /// </summary>
        public string Value2 { get; set; }

        public string DisplayValue
        {
            get
            {
                if (string.IsNullOrEmpty(Value1))
                {
                    return Value2 ?? string.Empty;
                }

                if (string.IsNullOrEmpty(Value2))
                {
                    return Value1 ?? string.Empty;
                }

                return String.Format("{0} - {1}", Value1, Value2);
            }
        }

        public string ConvertToXmlString()
        {
            StringBuilder data = new StringBuilder();
            XmlSerializer serializer = new XmlSerializer(this.GetType());
            using (TextWriter wr = new StringWriter(data))
            {
                serializer.Serialize(wr, this);
            }

            return data.ToString();
        }

        public static LookupResponse ConvertFromXmlString(string xmlString)
        {
            LookupResponse result = null;
            StringBuilder data = new StringBuilder();
            XmlSerializer serializer = new XmlSerializer(typeof(LookupResponse));
            using (TextReader reader = new StringReader(xmlString))
            {
                result =  serializer.Deserialize(reader) as LookupResponse;
            }

            return result;
        }

        public override string ToString()
        {
            return DisplayValue;
        }
    }
}
