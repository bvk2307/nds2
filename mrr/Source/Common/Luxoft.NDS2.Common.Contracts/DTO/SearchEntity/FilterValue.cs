﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contract.DTO.SearchEntity
{
    /// <summary>контейнер для передачи фильтров поиска между клиентом и сервером</summary>
    [Serializable]
    public class FilterValue : IFilterTreeItem
    {
        public string Criteria { get; set; }
        public CompareOperator Comparer { get; set; }
        public ParameterType ValueType { get; set; }
        public object ValueOne { get; set; }
        public object ValueTwo { get; set; }

        public FilterValue(string criteria, CompareOperator comparer, ParameterType valueType, object valueOne, object valueTwo = null)
        {
            Criteria = criteria;
            Comparer = comparer;
            ValueType = valueType;
            ValueOne = valueOne;
            ValueTwo = valueTwo;
        }
        public FilterValue Clone()
        {
            return new FilterValue(this.Criteria, this.Comparer, this.ValueType, this.ValueOne, this.ValueTwo);
        }

        IFilterTreeItem IFilterTreeItem.Clone(bool withAggregate)
        {
            return this.Clone();
        }

        public bool IsNull
        {
            get
            {
                //return (ValueOne == null || string.IsNullOrEmpty(ValueOne.ToString()) && (ValueTwo == null || string.IsNullOrEmpty(ValueTwo.ToString())));
                return ValueOne == null && ValueTwo == null;
            }
        }

        public static object GetDefault(ParameterType valueType)
        {
            switch (valueType)
            {
                case ParameterType.Boolean:
                    return (int)1;
                case ParameterType.Int64:
                    return 0L;
                case ParameterType.DateTime:
                    //return DateTime.Now.Date;
                    return null;
                case ParameterType.String:
                    return string.Empty;
                case ParameterType.Decimal:
                case ParameterType.Money:
                    return 0m;
                case ParameterType.Lookup:
                    return new LookupResponse { Code = null, Value1 = "" };
                default:
                    return null;
            }
        }
    }
}
