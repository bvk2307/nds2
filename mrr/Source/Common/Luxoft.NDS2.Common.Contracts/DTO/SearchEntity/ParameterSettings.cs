﻿using Luxoft.NDS2.Common.Contract.DTO.SearchEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Luxoft.NDS2.Common.Contract.DTO.SearchEntity
{
    [Serializable]
    public class ParameterSettings
    {
        [XmlAttribute]
        public string Name { get; set; }
        
        [XmlAttribute]
        public ParameterType LeafType { get; set; }
        
        [XmlAttribute]
        public string CriteriaName { get; set; }

        [XmlAttribute]
        public string CriteriaRegion { get; set; }

        [XmlAttribute]
        public bool Ignore { get; set; }

        [XmlElement("ChildParameters")]
        public List<ParameterSettings> ChildParameters { get; set; }

        private bool isAggregate = false;
        [XmlAttribute]
        public bool IsAggregate { get { return isAggregate; } set { isAggregate = value; } }

        [XmlAttribute]
        public string Scope { get; set; }

        [XmlAttribute]
        public string LookupSource { get; set; }

        public string GetFullName(string criteriaName)
        {
            string result = string.Empty;
            foreach (var param in ChildParameters)
            {
                if (param.ChildParameters != null && param.ChildParameters.Count > 0)
                {
                    result += param.GetFullName(criteriaName);
                    if (!string.IsNullOrEmpty(result))
                    {
                        result = String.Format("{0}.{1}", param.Name, result);
                        return result;
                    }
                }
                if (!string.IsNullOrEmpty(param.CriteriaName) && param.CriteriaName.Equals(criteriaName))
                {
                    return param.Name;
                }
            }
            return result;
        }

        
    }
}
