﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contract.DTO.SearchEntity
{
    public enum BoolOperator
    {
        NONE,
        AND,
        OR
    }
}
