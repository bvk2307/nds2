﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Reflection;

namespace Luxoft.NDS2.Common.Contract.DTO.SearchEntity
{

    /// <summary>виды операторов</summary>
    public enum CompareOperator
    {
        [DatabaseSyntaxAttribute("=")]
        [DisplayNameAttribute("=")]
        Equals,
        [DatabaseSyntaxAttribute("<>")]
        [DisplayNameAttribute("<>")]
        NotEquals,
        [DatabaseSyntaxAttribute(">")]
        [DisplayNameAttribute(">")]
        More,
        [DatabaseSyntaxAttribute(">=")]
        [DisplayNameAttribute(">=")]
        MoreOrEquals,
        [DatabaseSyntaxAttribute("<")]
        [DisplayNameAttribute("<")]
        Less,
        [DatabaseSyntaxAttribute("<=")]
        [DisplayNameAttribute("<=")]
        LessOrEquals,
        [DatabaseSyntaxAttribute("like")]
        [DisplayNameAttribute("=")]
        Like,
        [DatabaseSyntaxAttribute("not like")]
        [DisplayNameAttribute("<>")]
        NotLike,
        [DatabaseSyntaxAttribute("between")]
        [DisplayNameAttribute("интервал")]
        Between,
        [DatabaseSyntaxAttribute("is null")]
        [DisplayNameAttribute("пусто")]
        Empty,
        [DatabaseSyntaxAttribute("is not null")]
        [DisplayNameAttribute("не пусто")]
        NotEmpty,
        Unknown
    }

    public static class CompareOperatorEx
    {
        /// <summary>получим написание оператора по синтаксису базы</summary>
        public static string DatabaseSyntaxString(this CompareOperator value)
        {
            DatabaseSyntaxAttribute[] attrs = (DatabaseSyntaxAttribute[])typeof(CompareOperator).GetField(value.ToString()).GetCustomAttributes(typeof(DatabaseSyntaxAttribute), false);
            if (attrs.Length > 0)
                return attrs[0].Description;
            else return value.ToString();
        }
        /// <summary>отобращаемое в UI имя оператора</summary>
        public static string DisplayNameString(this CompareOperator value)
        {
            DisplayNameAttribute[] attrs = (DisplayNameAttribute[])typeof(CompareOperator).GetField(value.ToString()).GetCustomAttributes(typeof(DisplayNameAttribute), false);
            if (attrs.Length > 0)
                return attrs[0].Description;
            else return value.ToString();
        }
    }
}
