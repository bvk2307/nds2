﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contract.DTO.SearchEntity
{
    public enum ParameterType
    {
        None,
        Node,
        String,
        DateTime,
        Int64,
        Boolean,
        Money,
        Decimal,
        Lookup
    }
}
