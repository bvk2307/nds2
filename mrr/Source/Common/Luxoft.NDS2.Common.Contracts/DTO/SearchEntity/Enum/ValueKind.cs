﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contract.DTO.SearchEntity
{
    /// <summary>вид данных: простое значение, список, список интервалов</summary>
    public enum ValueKind
    {
        /// <summary>отсутсвие данных для пусто и не пусто</summary>
        EmptyValue,
        /// <summary>простой тип</summary>
        SingleValue,
        /// <summary>коллекция простых типов</summary>
        SingleCollection,
        /// <summary>коллекция, элементом которого являются два простых типа (для интервалов)</summary>
        DoubleCollection
    }
}
