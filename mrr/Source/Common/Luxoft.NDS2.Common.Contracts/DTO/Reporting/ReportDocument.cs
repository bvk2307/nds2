﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Reporting
{
    [Serializable]
    public class ReportDocumentResult
    {
        public Boolean Result;
        
        public String FileType;
        
        public byte[] FileData;
        
        public string ErrorMessage;
    }
}
