﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Reporting
{
    public enum SearchStatus
    {
        None = 0,
        Found = 1,
        NoAggregate = 2,
        NoDataBefore = 3
    }

    [Serializable]
    public class ReportIdSearchResult
    {
        public long Id { get; set; }

        public SearchStatus Status { get; set; }
    }
}
