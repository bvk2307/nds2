﻿using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using System;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration.Helpers;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.TaxPayer
{
    /// <summary>
    /// Этот класс представляет DTO - Налогоплательщик
    /// </summary>
    [Serializable]
    public class TaxPayer
    {
        /// <summary>
        /// Возвращает или задает идентификатор налогоплательщика
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Возвращает или задает наименование налогоплательщика
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Возвращает или задает ИНН
        /// </summary>
        public string Inn { get; set; }

        /// <summary>
        /// Возвращает или задает ИНН правоприемника
        /// </summary>
        public string InnSuccessor { get; set; }
        
        /// <summary>
        /// Возвращает или задает КПП налогоплательщика
        /// </summary>
        public string Kpp { get; set; }

        /// <summary>
        /// Возвращает или задает КПП налогоплательщика
        /// </summary>
        public string KppEffective { get; set; }

        /// <summary>
        /// Возвращает или задает КПП правоприемника
        /// </summary>
        public string KppSuccessor { get; set; }
        
        /// <summary>
        /// Возвращает или задает КПП правоприемника
        /// </summary>
        public string KppEffectiveSuccessor { get; set; }

        /// <summary>
        /// Возвращает или задает наименование налогоплательщика
        /// </summary>
        public string NameSuccessor { get; set; }
        
        /// <summary>
        /// Возвращает или задает адрес налогоплательщика
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Возвращает или задает налоговый орган(код), к которому относится налогоплательщик
        /// </summary>
        public string Soun { get; set; }

        /// <summary>
        /// Возвращает или задает налоговый орган(название), к которому относится налогоплательщик
        /// </summary>
        public string SounName { get; set; }

        /// <summary>
        /// Возвращает или задает дату регистрации налогоплательщика
        /// </summary>
        public DateTime? RecordedAt { get; set; }

        /// <summary>
        /// Возвращает или задает дату постановки на учет налогоплательщика
        /// </summary>
        public DateTime? RegistryAt { get; set; }

        /// <summary>
        /// Возвращает или задает дату снятия с учет налогоплательщика
        /// </summary>
        public DateTime? UnregistryAt { get; set; }

        /// <summary>
        /// Возвращает или задает ОГРН
        /// </summary>
        public string OGRN { get; set; }

        /// <summary>
        /// Возвращает или задает размер уставного капитала налогоплательщика
        /// </summary>
        public decimal RegulationFund { get; set; }

        public int? SurCode { get; set; }

        /// <summary>
        /// Возвращает или задает код ОКВЭД
        /// </summary>
        public string OkvedCode { get; set; }

        /// <summary>
        /// Возвращает или задает наименование ОКВЭД
        /// </summary>
        public string OkvedName { get; set; }

        /// <summary>
        /// Возвращает признак налогоплателщика (ЮЛ/ФЛ)
        /// </summary>
        public bool IsUL 
        {
            get { return !string.IsNullOrEmpty(this.Kpp); }
        }

        /// <summary> Возвращает признак крупнейшего налогоплателщика (5 и 6 символ в кпп должен быть 01) |</summary>
        public bool IsLArge { get { return KppCodeHelper.IsLargeTaxPayerByKpp( Kpp ); }  }
    }
}
