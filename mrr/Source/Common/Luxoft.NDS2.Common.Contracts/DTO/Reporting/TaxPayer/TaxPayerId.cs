﻿using System;
using System.Text;
using System.Diagnostics;

namespace Luxoft.NDS2.Common.Contracts.DTO.Reporting.TaxPayer
{ 
    /// <summary> Неизменяемая строковая пользовательская идентификация налогоплательщика. </summary>
    [DebuggerDisplay( "INN: {_inn} KPP: {_kpp}" )]
    [Serializable]
    public sealed class TaxPayerId : IEquatable<TaxPayerId>
    {
        private readonly string _inn;
        private readonly string _kpp;

        public TaxPayerId( string innCode, string kppCode )
        {
            if ( string.IsNullOrWhiteSpace( innCode ) )
                throw new ArgumentNullException( "innCode", "Parameter can not be 'null', empty or blank" );
            System.Diagnostics.Contracts.Contract.EndContractBlock();

            _inn = innCode;
            _kpp = kppCode;
        }

        /// <summary> Возвращает или задает ИНН </summary>
        public string Inn
        {
            get { return _inn; }
        }

        /// <summary> Возвращает или задает КПП налогоплательщика </summary>
        public string Kpp
        {
            get { return _kpp; }
        }

        #region Equality members

        public static bool operator ==( TaxPayerId left, TaxPayerId right )
        {
            return Equals( left, right );
        }

        public static bool operator !=( TaxPayerId left, TaxPayerId right )
        {
            return !Equals( left, right );
        }

        /// <summary>Indicates whether the current object is equal to another object of the same type.</summary>
        /// <returns>true if the current object is equal to the <paramref name="other" /> parameter; otherwise, false.</returns>
        /// <param name="other">An object to compare with this object.</param>
        public bool Equals( TaxPayerId other )
        {
            if ( ReferenceEquals( null, other ) ) return false;
            if ( ReferenceEquals( this, other ) ) return true;
            return string.Equals( _inn, other._inn ) && string.Equals( _kpp, other._kpp );
        }

        /// <summary>Determines whether the specified <see cref="T:System.Object" /> is equal to the current <see cref="T:System.Object" />.</summary>
        /// <returns>true if the specified <see cref="T:System.Object" /> is equal to the current <see cref="T:System.Object" />; otherwise, false.</returns>
        /// <param name="obj">The object to compare with the current object. </param>
        public override bool Equals( object obj )
        {
            if ( ReferenceEquals( null, obj ) ) return false;
            if ( ReferenceEquals( this, obj ) ) return true;
            return obj is TaxPayerId && Equals( (TaxPayerId)obj );
        }

        /// <summary>Serves as a hash function for a particular type. </summary>
        /// <returns>A hash code for the current <see cref="T:System.Object" />.</returns>
        public override int GetHashCode()
        {
            unchecked
            {
                return ( _inn.GetHashCode() * 397 ) ^ ( _kpp != null ? _kpp.GetHashCode() : 0 );
            }
        }

        #endregion

        /// <summary>Returns a string that represents the current object.</summary>
        /// <returns>A string that represents the current object.</returns>
        public override string ToString()
        {
            return string.Join( " ", "INN:", Inn, "KPP:", Kpp );
        }
    }
}