﻿namespace Luxoft.NDS2.Common.Contracts.DTO.Business.TaxPayer
{
    public enum Taxation
    {
        Simple,
        OSNO,
        EVNO
    }
}
