﻿using System;
using System.ComponentModel;

namespace Luxoft.NDS2.Common.Contracts.DTO.Reporting.TaxPayer
{
    /// <summary>
    /// Банковский счет
    /// </summary>
    [Serializable]
    public class BankAccount
    {
        [DisplayName("БИК банка")]
        [Description("БИК банка")]
        public string BIK { get; set; }

        [DisplayName("ИНН банка")]
        [Description("ИНН банка")]
        public string BankInn { get; set; }

        [DisplayName("КПП банка")]
        [Description("КПП банка")]
        public string BankKpp { get; set; }

        [DisplayName("Регистрационный номер банка")]
        [Description("Регистрационный номер банка")]
        public string RegistrationBankNumber { get; set; }

        [DisplayName("Номер филиала")]
        [Description("Номер филиала")]
        public string BankBrunhNumber { get; set; }

        [DisplayName("Наименование банка")]
        [Description("Наименование банка")]
        public string BankName { get; set; }

        [DisplayName("Номер счета")]
        [Description("Номер счета")]
        public string AccountNumber { get; set; }

        [DisplayName("Валюта счета")]
        [Description("Валюта счета")]
        public string AccountValute { get; set; }

        [DisplayName("Дата открытия")]
        [Description("Дата открытия счета")]
        public DateTime? OpenDate { get; set; }

        [DisplayName("Дата закрытия")]
        [Description("Дата закрытия счета")]
        public DateTime? CloseDate { get; set; }

        [DisplayName("Вид счета")]
        [Description("Вид счета")]
        public string AccountType { get; set; }

        [DisplayName("ИНН НП")]
        [Description("ИНН НП")]
        public string Inn { get; set; }

        [DisplayName("КПП НП")]
        [Description("КПП НП")]
        public string Kpp { get; set; }
    }

    [Serializable]
    public class BankAccountBrief
    {
        public string Inn { get; set; }

        public string Kpp { get; set; }

        public string Bik { get; set; }

        public string NameBank { get; set; }

        public string InnBank { get; set; }

        public string KppBank { get; set; }

        public string BankNumber { get; set; }

        public string FilialNumber { get; set; }

        public string Account { get; set; }

        public string Currency { get; set; }

        public string AccountType { get; set; }

        public DateTime? CloseDate { get; set; }

        public DateTime? OpenDate { get; set; }
    }

}
