﻿using System;
using System.ComponentModel;

namespace Luxoft.NDS2.Common.Contracts.DTO.Reporting.TaxPayer
{
    [Serializable]
    public class BankAccountRequestSummary
    {
        public string Inn { get; set; }

        public string KppEffective { get; set; }

        [DisplayName("Дата формирования")]
        [Description("Дата формирования запроса в АСК")]
        public DateTime? CreateDate { get; set; }

        [DisplayName("ИНН банка")]
        [Description("ИНН банка")]
        public string BankInn { get; set; }

        [DisplayName("КПП банка")]
        [Description("КПП банка")]
        public string BankKpp { get; set; }

        [DisplayName("БИК банка")]
        [Description("БИК банка")]
        public string Bik { get; set; }

        [DisplayName("Регистрационный номер")]
        [Description("Регистрационный номер банка")]
        public string BankNumber { get; set; }

        [DisplayName("Номер филиала")]
        [Description("Номер филиала")]
        public string BankFilial { get; set; }

        [DisplayName("Наименование банка")]
        [Description("Наименование банка")]
        public string BankName { get; set; }

        [DisplayName("Номер счета")]
        [Description("Номер счета")]
        public string AccountsList { get; set; }

        [DisplayName("Запрашиваемый период")]
        [Description("Запрашиваемый отчетный период")]
        public string FullTaxPeriod { get; set; }

        [DisplayName("Дата начала запроса")]
        [Description("Дата начала запроса")]
        public DateTime RequestBeginDate { get; set; }

        [DisplayName("Дата окончания запроса")]
        [Description("Дата окончания запроса")]
        public DateTime RequestEndDate { get; set; }

        [DisplayName("Номер запроса")]
        [Description("Номер запроса налогового органа")]
        public long? RequestNumber { get; set; }

        [DisplayName("Статус запроса")]
        [Description("Статус запроса")]
        public string RequestStatus { get; set; }

        [DisplayName("ИНН НП")]
        [Description("ИНН НП")]
        public string InnContractor { get; set; }

        [DisplayName("КПП НП")]
        [Description("КПП НП")]
        public string KppContractor { get; set; }
        
        [DisplayName("Дата запроса")]
        [Description("Дата запроса налогового органа")]
        public DateTime? RequestDate { get; set; }

        [DisplayName("Инициатор запроса")]
        [Description("Инициатор запроса")]
        public string Requester { get; set; }

        public int FiscalYear { get; set; }

        public int SortOrderPeriod { get; set; }
    }
}
