﻿using System.Collections.Generic;

namespace Luxoft.NDS2.Common.Contracts.DTO.Reporting.TaxPayer
{ 
    public class TaxPayerIdHelper
    { 
        public static IEnumerable<string> GetInns(IEnumerable<TaxPayerId> taxPayerIds)
        {
            foreach (var taxPayerId in taxPayerIds)
            {
                yield return taxPayerId.Inn;
            }
        }

        public static IEnumerable<string> GetKpps(IEnumerable<TaxPayerId> taxPayerIds)
        {
            foreach (var taxPayerId in taxPayerIds)
            {
                yield return taxPayerId.Kpp;
            }
        }
    }
}
