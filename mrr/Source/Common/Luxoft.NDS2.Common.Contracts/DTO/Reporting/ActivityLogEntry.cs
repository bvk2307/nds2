﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Reporting
{
    // ReSharper disable InconsistentNaming
    #region Description

    // -----------------------Количество пользователей-----------------------
    // Всего вошедших в систему  - такого показателя у нас нет, но для нас это имеется ввиду когда сработал
    //   "public SubsystemNavigationNodesProvider(WorkItem subsystemWorkItem)". поскольку презентера там нет,
    //   то логику LogActivity(ActivityLogEntry entry) придется тут продублировать
    // Открывших разделы 1-7 НД - это событие открытия карточки декларации
    // Открывших разделы 8-12 НД - есть событие которое симафорит что закладка в карточке декларации с
    //   разделом открывается впервый раз(подробности у Павла)
    // Формировавших ручные выборки - это событие кнопки применить на выборке
    // Запустивших выполнение отчётов - Спросить у Андрея

    // -----------------------Запросы к НД-----------------------
    // ----Открытие разделов 1-7 
    // Время открытия - замеряем через Stopwatch сколько метод выполняется и логируем
    // ----Открытие разделов 8-12 
    // Время открытия - замеряем через Stopwatch сколько метод выполняется и логируем. Поскольку тут 2 метода
    //   (1-СОВ, 2-МРР), то и события надо 2: начальное открытие и работа с готовыми данными
    // Размер НД (записей о СФ) - Андрей уже делает доступность данной информации в карточке декларации. так
    //   что при логировании открытия раздела 1-7 в поле count кидаем ссумарное кол-во записей СФ по разделам

    // -----------------------Запросы на формирование ручных выборок, количество-----------------------
    // Время формирования - замеряем через Stopwatch сколько метод выполняется и логируем

    // -----------------------Запросы на формирование отчётов -----------------------
    // Время формирования - замеряем через Stopwatch сколько метод выполняется и логируем

    #endregion

    public enum ActivityLogType
    {
        Unclassified = 1,
        LogonsCount = 2,
        Chapter17OpenCount = 3,
        Chapter812OpenCount = 4,
        ManualSelectionCount = 5,
        ReportCount = 6,
        Chapter17OpenTime = 7,
        Chaprer812SovOpenTime = 8,
        Chaprer812MrrOpenTime = 9,
        ManualSelectionTime = 10,
        ReportTime = 11
    }

    [Serializable]
    public class ActivityLogEntry
    {
        public ActivityLogType LogType { get; set; }
        public int ElapsedMilliseconds { get; set; }
        public int Count { get; set; }

        public static ActivityLogEntry New(ActivityLogType logType, long elapsedMilliseconds = 0, int count = 1)
        {
            return new ActivityLogEntry { LogType = logType, ElapsedMilliseconds = (int)elapsedMilliseconds, Count = count };
        }
    }

    [Serializable]
    public class SystemLogEntry
    {
        public int Kind { get; set; }
        public DateTime LOG_DATE { get; set; }
        public string SITE { get; set; }
        public string Sql { get; set; }
        public string Message { get; set; }
    }

    [Serializable]
    public class ActivityReportSummary
    {
        public DateTime Begin { get; set; }
        public DateTime End { get; set; }

        public int Logons { get; set; }
        public int Decl_open { get; set; }
        public int Decl_size { get; set; }
        public int Decl_ext_open { get; set; }
        public int Man_sel_call { get; set; }
        public int Reports { get; set; }

        public int Decl_time { get; set; }
        public int Decl_sov_time { get; set; }
        public int Decl_mrr_time { get; set; }
        public int Reports_time { get; set; }
        public int Decl_time_min { get; set; }
        public int Decl_time_max { get; set; }
        public int Decl_time_avg { get; set; }
        public int Decl_size_min { get; set; }
        public int Decl_size_max { get; set; }
        public int Decl_size_avg { get; set; }
        public int Decl_sov_time_min { get; set; }
        public int Decl_sov_time_max { get; set; }
        public int Decl_sov_time_avg { get; set; }
        public int Decl_mrr_time_min { get; set; }
        public int Decl_mrr_time_max { get; set; }
        public int Decl_mrr_time_avg { get; set; }
        public int Reports_time_min { get; set; }
        public int Reports_time_max { get; set; }
        public int Reports_time_avg { get; set; }
        public int Man_sel_time_min { get; set; }
        public int Man_sel_time_max { get; set; }
        public int Man_sel_time_avg { get; set; }
        public int Total_users { get; set; }

        public int Decl_open_uniq { get; set; }
        public int Decl_ext_open_uniq { get; set; }
        public int Man_sel_call_uniq { get; set; }
        public int Reports_uniq { get; set; }
    // ReSharper restore InconsistentNaming

        public int MaxUserForHour { get; set; }

    }
}
