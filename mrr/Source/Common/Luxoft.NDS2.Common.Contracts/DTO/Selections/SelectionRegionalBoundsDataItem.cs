﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Selections
{
    [Serializable]
    public class SelectionRegionalBoundsDataItem
    {
        public long? Id { get; set; }

        public long SelectionTemplateId { get; set; }

        public int? Include { get; set; }

        public string RegionCode { get; set; }

        public string RegionName { get; set; }

        public uint DiscrTotalAmt { get; set; }

        public uint DiscrMinAmt { get; set; }

        public uint DiscrGapTotalAmt { get; set; }

        public uint DiscrGapMinAmt { get; set; }

        public SelectionRegionalBoundsDataItem Clone( )
        {
            return new SelectionRegionalBoundsDataItem()
            {
                Id = this.Id,
                SelectionTemplateId = this.SelectionTemplateId,
                RegionCode = this.RegionCode,
                RegionName = this.RegionName,
                Include = this.Include,
                DiscrTotalAmt = this.DiscrTotalAmt,
                DiscrMinAmt = this.DiscrMinAmt,
                DiscrGapTotalAmt = this.DiscrGapTotalAmt,
                DiscrGapMinAmt = this.DiscrGapMinAmt
            };
        }
    }
}