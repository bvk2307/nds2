﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Selections
{
    [Serializable]
    public class SelectionByStatusesCount
    {
        public int RequestApproveCount { get; set; }
        public int ApprovedCount { get; set; }

        public SelectionByStatusesCount(int requestApproveCount, int approvedCount)
        {
            RequestApproveCount = requestApproveCount;
            ApprovedCount = approvedCount;
        }
    }
}
