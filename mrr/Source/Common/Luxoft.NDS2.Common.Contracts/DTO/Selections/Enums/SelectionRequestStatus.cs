﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums
{
    public enum SelectionRequestStatus
    {
        Undefined = -1,
        /// <summary>
        /// Создана
        /// </summary>
        New = 0,
        
        /// <summary>
        /// Отправлен в очередь
        /// </summary>
        InQueue = 1,

        /// <summary>
        /// Принят в очереди
        /// </summary>
        AcceptedInQueue = 2,

        /// <summary>
        /// Получен план запроса
        /// </summary>
        PlanReceived = 3,

        /// <summary>
        /// Выборка сформирована
        /// </summary>
        SelectionCreated = 4,

        /// <summary>
        /// Выборка записана в оракл
        /// </summary>
        SelectionRecorded = 5,

        /// <summary>
        /// НД записаны в оракл
        /// </summary>
        DeclarationsRecorded = 6,

        /// <summary>
        /// Ошибка при формировании плана запроса
        /// </summary>
        PlanError = 13,

        /// <summary>
        /// Ошибка при выполнении выборки
        /// </summary>
        SelectionError = 14,

        /// <summary>
        /// Ошибка при загрузке выборки в оракл
        /// </summary>
        SelectionRecordError = 15,

        /// <summary>
        /// Ошибка при записи списка НД
        /// </summary>
        DeclarationsRecordError = 16

    }
}
