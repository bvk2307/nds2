﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums
{
    [Serializable]
    public enum ExcludeFromClaimType
    {
        Include = 0,

        /// <summary>
        /// Закрыта КНП
        /// </summary>
        CloseKnp = 2,

        /// <summary>
        /// Корр. неактуальна
        /// </summary>
        NotActualDeclVersion = 1,

        /// <summary>
        /// Помечено как КНП
        /// </summary>
        MarkedAsKnp = 4,

        /// <summary>
        /// Отсутствует рег. номер СЭОД
        /// </summary>
        MissingRegNum = 3,

        /// <summary>
        /// Ручное исключение
        /// </summary>
        Exclude = 5,

        /// <summary>
        /// Искл. по второй стороне 
        /// </summary>
        ExcludeBySecondSide = 6
    }
}
