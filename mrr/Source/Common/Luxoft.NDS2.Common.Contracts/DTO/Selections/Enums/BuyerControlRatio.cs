﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums
{
    [Serializable]
    public enum BuyerControlRatio
    {
        /// <summary>
        /// Не равно
        /// </summary>
        NotEqual = 0,

        /// <summary>
        /// Равно
        /// </summary>
        Equal = 1,

        /// <summary>
        /// КС не рассчитан
        /// </summary>
        NotCalculated = 2
    }
}
