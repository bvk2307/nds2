﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums
{
    [Serializable]
    public enum SelectionActionType
    {
        /// <summary>
        /// Создание
        /// </summary>
        Create = 1,

        /// <summary>
        /// Редактирование
        /// </summary>
        Edit = 2,

        /// <summary>
        /// Повторное применение фильтра
        /// </summary>
        SecondaryApply = 3,

        /// <summary>
        /// Отправка на согласование
        /// </summary>
        RequestForApprove = 4,

        /// <summary>
        /// Согласование
        /// </summary>
        Approve = 5,

        /// <summary>
        /// Возврат на доработку
        /// </summary>
        ReturnForRework = 6,

        /// <summary>
        /// Формирование АТ
        /// </summary>
        CreateClaim = 7

    }
}
