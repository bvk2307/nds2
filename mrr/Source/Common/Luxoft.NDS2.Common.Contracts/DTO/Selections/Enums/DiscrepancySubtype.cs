﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums
{
    [Serializable]
    public enum DiscrepancySubtype
    {
        /// <summary>
        /// Не множественный
        /// </summary>
        NoneMultiple = 0,

        /// <summary>
        /// Множественный
        /// </summary>
        Multiple = 1
    }
}
