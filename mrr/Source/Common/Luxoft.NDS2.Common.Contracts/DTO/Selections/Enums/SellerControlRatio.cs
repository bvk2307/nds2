﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums
{
    [Serializable]
    public enum SellerControlRatio
    {
        /// <summary>
        /// Нет
        /// </summary>
        NotExist = 0,

        /// <summary>
        /// Есть
        /// </summary>
        Exist = 1,

        /// <summary>
        /// КС не рассчитан
        /// </summary>
        NotCalculated = 2
    }
}
