﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums
{
    [Serializable]
    public enum DeclarationType
    {
        /// <summary>
        /// Декларация
        /// </summary>
        Declaration = 0,

        /// <summary>
        /// Журнал
        /// </summary>
        Journal = 1
    }
}
