﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums
{
    [Serializable]
    public enum SidePrimaryProcessing
    {
        /// <summary>
        /// Покупатель
        /// </summary>
        Buyer = 0,

        /// <summary>
        /// Продавец
        /// </summary>
        Seller = 1
    }
}
