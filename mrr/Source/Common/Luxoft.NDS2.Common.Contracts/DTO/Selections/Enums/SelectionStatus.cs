﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums
{
    [Serializable]
    public enum SelectionStatus
    {
        /// <summary>
        /// загружается
        /// </summary>
        Loading = 1,

        /// <summary>
        /// на согласовании
        /// </summary>
        RequestForApproval = 2,

        /// <summary>
        /// Ошибка загрузки
        /// </summary>
        LoadingError = 3,

        /// <summary>
        /// согласована
        /// </summary>
        Approved = 4,

        /// <summary>
        /// Сформировано АТ
        /// </summary>
        ClaimCreated = 5,
        
        /// <summary>
        /// удалена
        /// </summary>
        Deleted = 6,

        /// <summary>
        /// формирование АТ
        /// </summary>
        ClaimCreating = 7,

        /// <summary>
        /// черновик
        /// </summary>
        Draft = 8
    }
}
