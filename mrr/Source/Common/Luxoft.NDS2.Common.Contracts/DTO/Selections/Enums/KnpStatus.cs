﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums
{
    [Serializable]
    public enum KnpStatus
    {
        /// <summary>
        /// Пусто
        /// </summary>
        Empty = -1,

        /// <summary>
        /// Закрыта
        /// </summary>
        Closed = 0,

        /// <summary>
        /// Открыта
        /// </summary>
        Opened = 1
    }
}
