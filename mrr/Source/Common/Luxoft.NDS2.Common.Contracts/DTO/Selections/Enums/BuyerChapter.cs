﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums
{
    [Serializable]
    public enum BuyerChapter
    {
        /// <summary>
        /// Книга покупок
        /// </summary>
        PurchaseBook = 8,

        /// <summary>
        /// Журнал выставленных СФ
        /// </summary>
        InvoiceJournalOut = 10,

        /// <summary>
        /// Журнал полученных СФ
        /// </summary>
        InvoiceJournalIn = 11,
        
        /// <summary>
        /// Неизвестен
        /// </summary>
        Unknown = 99
    }
}
