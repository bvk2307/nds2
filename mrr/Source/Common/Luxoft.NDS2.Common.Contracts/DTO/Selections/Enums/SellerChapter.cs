﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums
{
    [Serializable]
    public enum SellerChapter
    {
        /// <summary>
        /// Книга продаж
        /// </summary>
        SellBook = 9,

        /// <summary>
        /// Журнал выставленных СФ
        /// </summary>
        InvoiceJournalOut = 10,

        /// <summary>
        /// Журнал полученных СФ
        /// </summary>
        InvoiceJournalIn = 11,

        /// <summary>
        /// Раздел 12
        /// </summary>
        Chapter12 = 12,

        /// <summary>
        /// Неизвестен
        /// </summary>
        Unknown = 99
    }
}
