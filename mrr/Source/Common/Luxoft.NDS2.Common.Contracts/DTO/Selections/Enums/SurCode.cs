﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums
{
    [Serializable]
    public enum SurCode
    {
        /// <summary>
        /// Высокий риск
        /// </summary>
        High = 1,

        /// <summary>
        /// Средний риск
        /// </summary>
        Medium = 2,

        /// <summary>
        /// Низкий риск
        /// </summary>
        Low = 3,

        /// <summary>
        /// Неопределенный
        /// </summary>
        Undefined = 4
    }
}
