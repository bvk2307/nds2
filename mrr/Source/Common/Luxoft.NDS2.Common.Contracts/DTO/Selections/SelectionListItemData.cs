﻿using System;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;

namespace Luxoft.NDS2.Common.Contracts.DTO.Selections
{
    [Serializable]
    public class SelectionListItemData
    {
        /// <summary>
        /// Номер выборки
        /// </summary>
        public long Id { set; get; }

        /// <summary>
        /// Номер шаблона выборки
        /// </summary>
        public long? TemplateId { set; get; }

        /// <summary>
        /// Название выборки
        /// </summary>
        public string Name { set; get; }

        /// <summary>
        /// Тип выборки
        /// </summary>
        public string TypeName { get; set; }

        /// <summary>
        /// Код Типа выборки
        /// </summary>
        public SelectionType TypeCode { get; set; }

        /// <summary>
        /// Дата создания выборки
        /// </summary>
        public DateTime CreatedAt { set; get; }

        /// <summary>
        /// Дата изменения статуса
        /// </summary>
        public DateTime? LastStatusChangedAt { set; get; }

        /// <summary>
        /// Статус выборки
        /// </summary>
        public SelectionStatus? StatusId { get; set; }

        /// <summary>
        /// К-во НД первоначально отобранных в выборку
        /// </summary>
        public int? InitialDeclarationQuantity { get; set; }

        /// <summary>
        /// К-во расхождений по СФ первоначально отобранных в выборку
        /// </summary>
        public int? InitialDiscrepancyQuantity { get; set; }

        /// <summary>
        /// Общая сумма расхождений
        /// </summary>
        public decimal? InitialDiscrepancyAmount { get; set; }

        /// <summary>
        /// Аналитик
        /// </summary>
        public string LastEditedBy { get; set; }

        /// <summary>
        /// Аналитик
        /// </summary>
        public string LastEditedBySid { get; set; }

        /// <summary>
        /// Согласующий
        /// </summary>
        public string ApprovedBy { get; set; }

        /// <summary>
        /// Согласующий
        /// </summary>
        public string ApprovedBySid { get; set; }
    }
}
