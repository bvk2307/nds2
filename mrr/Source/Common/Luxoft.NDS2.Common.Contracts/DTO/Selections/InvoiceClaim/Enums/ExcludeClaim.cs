﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Selections.InvoiveClaim.Enums
{
    [Serializable]
    public enum ExcludeClaim
    {
        Include = 0,

        Exclude = 1
    }
}
