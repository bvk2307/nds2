﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Selections.InvoiveClaim.Enums
{
    [Serializable]
    public enum TaxMonitoring
    {
        Include = 1,

        Exclude = 0
    }
}
