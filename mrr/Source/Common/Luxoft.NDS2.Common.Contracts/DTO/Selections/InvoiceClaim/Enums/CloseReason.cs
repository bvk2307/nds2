﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Selections.InvoiveClaim.Enums
{
    [Serializable]
    public enum CloseReason
    {
        Empty = 0,

        /// <summary>
        /// Истек срок отработки
        /// </summary>
        ExpiredWork = 1,

        /// <summary>
        /// Новая корректировка
        /// </summary>
        NewCorrection = 2,

        /// <summary>
        /// Закрылась КНП
        /// </summary>
        ClosedKnp = 3,

        /// <summary>
        /// Закрылись все расхождения
        /// </summary>
        ClosedAllDiscrepancies = 4,

        /// <summary>
        /// Корректировка аннулирована
        /// </summary>
        Annulable = 5

    }
}
