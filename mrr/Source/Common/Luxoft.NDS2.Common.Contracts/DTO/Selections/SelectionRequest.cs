﻿using System;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;

namespace Luxoft.NDS2.Common.Contracts.DTO.Selections
{
    [Serializable]
    public class SelectionRequest
    {
        public long SelectionId { get; set; }

        public long RequestId { get; set; }

        public SelectionRequestStatus RequestStatus { get; set; }
    }
}
