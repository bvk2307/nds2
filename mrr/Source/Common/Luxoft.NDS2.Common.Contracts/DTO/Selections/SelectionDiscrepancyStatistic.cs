﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Selections
{
    [Serializable]
    public class SelectionDiscrepancyStatistic
    {
        public long SelectionId { get; set; }

        public string SelectionName { get; set; }

        public long TotalQty { get; set; }

        public long InProcessQty { get; set; }
    }
}
