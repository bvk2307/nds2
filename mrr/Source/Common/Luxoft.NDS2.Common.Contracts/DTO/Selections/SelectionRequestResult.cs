﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;

namespace Luxoft.NDS2.Common.Contracts.DTO.Selections
{
    [Serializable]
    public class SelectionRequestResult
    {
        public SelectionRequestStatus RequestStatus { get; set; }

        public long SelectionRequestId { get; set; }


        /// <summary>
        /// DEBUG ONLY
        /// </summary>
        public string QueryText { get; set; }
    }
}
