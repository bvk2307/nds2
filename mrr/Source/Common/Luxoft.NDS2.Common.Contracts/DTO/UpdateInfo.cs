﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO
{
    [Serializable]
    public class UpdateInfo
    {
        public VersionInfo Version
        {
            get;
            set;
        }

        public string Path
        {
            get;
            set;
        }

        public long Length
        {
            get;
            set;
        }
    }
}
