﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.ExportExcel
{
    [Serializable]
    public class ExportExcelItem
    {
        // ReSharper disable InconsistentNaming

        public long QUEUE_ITEM_ID { get; set; }

        public string USER_SID { get; set; }

        public DateTime? ENQUEUED { get; set; }

        public DateTime? STARTED { get; set; }

        public string STARTED_BY_ID { get; set; }

        public DateTime? COMPLETED { get; set; }

        public long SELECTION_ID { get; set; }

        public string ROW_FILTER { get; set; }

        public string FILENAME { get; set; }

        public string ERROR_DESCRIPTION { get; set; }

        public long ROWS_QUANTITY { get; set; }

        // ReSharper restore InconsistentNaming
    }


}
