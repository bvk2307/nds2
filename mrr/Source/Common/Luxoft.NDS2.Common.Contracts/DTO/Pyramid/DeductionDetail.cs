﻿using System; 
using System.ComponentModel; 
using Luxoft.NDS2.Common.Contracts.CustomAttributes;

namespace Luxoft.NDS2.Common.Contracts.DTO.Pyramid
{ 
    [Serializable]
    public class DeductionDetail
    { 
        [DisplayName("Сумма НДС к вычету)")]
        [Description("Сумма НДС к вычету)")]
        [CurrencyFormat]
        public decimal DecuctionAmount { get; set; }

        [DisplayName("Налоговый период)")]
        [Description("Налоговый период)")] 
        public PeriodOrYear PeriodOrYear { get; set; }
    }
}