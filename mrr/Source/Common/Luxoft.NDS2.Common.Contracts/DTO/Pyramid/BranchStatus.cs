﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Pyramid
{
    [Flags]
    public enum BranchStatus
    {
        ChildrenRequested           = 0x01,
        ChildrenLoaded              = 0x02,
        AllChildrenLoaded           = 0x04,
        SentInReport                = 0x08,
        AllChildrenSentInReport     = 0x10      
    }
}
