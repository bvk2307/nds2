﻿namespace Luxoft.NDS2.Common.Contracts.DTO.Pyramid
{
    public enum ReportType
    {
        Sellers,
        Buyers
    }
}
