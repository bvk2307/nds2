﻿using System;
using System.ComponentModel;
using Luxoft.NDS2.Common.Contracts.CustomAttributes;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration.Enums;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid.Extensions;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting.TaxPayer;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace.Enums;

namespace Luxoft.NDS2.Common.Contracts.DTO.Pyramid
{
    [Serializable]
    public class GraphData
    {
        private TaxPayerId _taxPayerId = null;

        /// <summary> Tax payer string identifier. </summary>
        public TaxPayerId TaxPayerId
        {
            get { return _taxPayerId; }
            set
            {
                if ( _taxPayerId != null )
                    throw new InvalidOperationException( string.Format( "Try to reassign INN|KPP code from INN|KPP: {0}|{1} to INN|KPP {2}|{3}", _taxPayerId.Inn, _taxPayerId.Kpp, value.Inn, value.Kpp ) );
                if ( value == null )
                    throw new InvalidOperationException( "TaxPayerId could not be assigned as 'null'" );

                _taxPayerId = value;
            }
        }

        [DisplayName( "ИНН контрагента" )]
        [Description( "ИНН контрагента" )]
        public string Inn { get { return _taxPayerId == null ? null : _taxPayerId.Inn; } }

        /// <summary> KPP code of a tax payer. </summary>
        public string Kpp { get { return _taxPayerId == null ? null : _taxPayerId.Kpp; } }

        /// <summary> A node request level. </summary>
        public long Level { get; set; }

        /// <summary> An order number on the level <see cref="Level"/> according the current sort order. It is not assigned from DB. </summary>
        public long LevelOrderNumber { get; set; }

        public long Id { get; set; }

        public string ParentInn { get; set; }

        public string ParentKpp { get; set; }

        public TaxPayerId ParentTaxPayerId
        {
            get 
            {
                if (ParentInn == null)
                {
                    return null;
                }
                return new TaxPayerId(ParentInn, ParentKpp);
            }
        }

        [DisplayName("НДС исчисленный продавцом по операциям с покупателем")]
        [Description("НДС исчисленный продавцом по операциям с покупателем")]
        [CurrencyFormat]
        public decimal MappedAmount { get; set; }

        [DisplayName("Сумма НДС, подлежащая вычету у покупателя по операциям с продавцом")]
        [Description("Сумма НДС, подлежащая вычету у покупателя по операциям с продавцом")]
        [CurrencyFormat]
        public decimal NotMappedAmnt { get; set; }

        public string Name { get; set; }

        [DisplayName("Признак НД")]
        [Description("Признак НД")]
        public string DeclarationType
        {
            get
            {
                return this.SignTypeString();
            }
        }

        /// <summary> Document type code: a declaration or a journal. </summary>
        public DeclarationTypeCode TypeCode { get; set; }

        /// <summary> A declaration inspection code and name. </summary>
        public CodeValueDictionaryEntry Inspection { get; set; }

        /// <summary> Processing stage in 'МС'. </summary>
        public DeclarationProcessignStage ProcessingStage { get; set; }

        public long ClientNumber { get; set; }

        public long SellerNumber { get; set; }

        [DisplayName("Сумма НДС, подлежащая вычету у контрагента, ВСЕГО")]
        [Description("Сумма НДС, подлежащая вычету у контрагента, ВСЕГО")]
        [CurrencyFormat]
        public decimal DeductionNds { get; set; }

        [DisplayName("НДС исчисленный у контрагента, ВСЕГО")]
        [Description("НДС исчисленный у контрагента, ВСЕГО")]
        [CurrencyFormat]
        public decimal CalcNds { get; set; }

        [DisplayName("Сумма разрывов у покупателя с продавцом")]
        [Description("Сумма разрывов у покупателя с продавцом")]
        [CurrencyFormat]
        public decimal DiscrepancyAmnt { get; set; }

        public bool IsZombie { get; set; }

        public bool IsTransitional { get; set; }

        public decimal VatShare { get; set; }

        public decimal VatTotal { get; set; }

        public DeclarationType SignType { get; set; }

        [DisplayName("Сумма разрывов по контрагенту (по стороне контрагента), ВСЕГО")]
        [Description("Сумма разрывов по контрагенту (по стороне контрагента), ВСЕГО")]
        [CurrencyFormat]
        public decimal GapDiscrepancyTotal { get; set; }

        [DisplayName("Доля вычетов у покупателя по операциям с продавцом")]
        [Description("Доля вычетов у покупателя по операциям с продавцом")]
        public decimal ClientNdsPercentage { get; set; }

        [DisplayName("Доля исчисленного НДС продавцом по операциям с покупателем")]
        [Description("Доля исчисленного НДС продавцом по операциям с покупателем")]
        public decimal SellerNdsPercentage { get; set; }

        [DisplayName("Доля вычетов в исчисленном НДС у контрагента")]
        [Description("Доля вычетов в исчисленном НДС у контрагента")]
        public decimal NdsPercentage { get; set; }

        private decimal CalculatePercentage(decimal sum1, decimal sum2)
        {
            return sum2 == 0 ? 0 : Math.Round(sum1 / sum2 * 100, 2);
        } 

        [DisplayName("СУР")]
        [Description("СУР")]
        public int? Sur { get; set; }

        public int? SurColorArgb { get; set; }

        [DisplayName("СУР")]
        [Description("СУР")]
        public string SurDescription { get; set; }

        public bool? SurIsDefault { get; set; }

        [DisplayName("Регион")]
        [Description("Регион")]
        public string Region { get; set; }

        [DisplayName("Код региона")]
        [Description("Код региона")]
        public string RegionCode { get; set; }

        [DisplayName("Название региона")]
        [Description("Название региона")]
        public string RegionName { get; set; }

        /// <summary> Contractor specification (ИНН^~КПП^~НазваниеКонтрагента) list with delimiter '^|'. </summary>
        public string ContractorSpecificationChain { get; set; }

        [DisplayName("Сумма расхождений вида проверка НДС")]
        [Description("Сумма расхождений вида проверка НДС")]
        [CurrencyFormat]
        public decimal DiscrepancyNdsAmnt { get; set; }

        [DisplayName("НДС по журналу учета выставленных СФ продавца")]
        [Description("НДС по журналу учета выставленных СФ продавца")]
        [CurrencyFormat]
        public decimal CreatedInvoiceAmnt { get; set; }

        [DisplayName("НДС по журналу учета полученных СФ покупателя")]
        [Description("НДС по журналу учета полученных СФ покупателя")]
        [CurrencyFormat]
        public decimal ReceivedInvoiceAmnt { get; set; }

        [DisplayName("Данные о вычетах покупателя до 01.01.2015")]
        [Description("Данные о вычетах покупателя до 01.01.2015")]
        [CurrencyFormat]
        public decimal DeductionBefore20150101Amnt { get; set; }

        [DisplayName("Данные о вычетах покупателя после 01.01.2015")]
        [Description("Данные о вычетах покупателя после 01.01.2015")]
        [CurrencyFormat]
        public decimal DeductionAfter20150101Amnt { get; set; }
        
        /// <summary>
        /// Связь построена не по данным родительского узла, а по данным дочернего
        /// </summary>
        public bool IsReverted { get; set; }
       
        public bool IsLarge { get; set; }

        [DisplayName("Сумма расхождений вида «разрыв» и «проверка НДС» у покупателя с продавцом")]
        [Description("Сумма расхождений вида «разрыв» и «проверка НДС» у покупателя с продавцом")]
        [CurrencyFormat]
        public decimal DiscrepancySumAmnt
        {
            get { return DiscrepancyAmnt + DiscrepancyNdsAmnt; }
        }

        [DisplayName("Сумма расхождений вида проверка НДС, ВСЕГО")]
        [Description("Сумма расхождений вида проверка НДС, ВСЕГО")]
        [CurrencyFormat]
        public decimal NdsDiscrepancyTotal { get; set; }

        [DisplayName("Сумма расхождений вида «разрыв» и «проверка НДС» по стороне продавца, ВСЕГО")]
        [Description("Сумма расхождений вида «разрыв» и «проверка НДС» по стороне продавца, ВСЕГО")]
        [CurrencyFormat]
        public decimal DiscrepancySumAmntTotal
        {
            get { return GapDiscrepancyTotal + NdsDiscrepancyTotal; }
        }

        /// <summary> Specification of 'Вычеты контрагента за предыдущие периоды' (deduction details for the previous period) as the list of value pairs: period - sum with delimiters. </summary>
        public string PreviousDeductionsSpecification { get; set; }
    }
}
