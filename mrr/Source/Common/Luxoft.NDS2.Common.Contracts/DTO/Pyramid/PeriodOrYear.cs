using System;
using System.Globalization;

namespace Luxoft.NDS2.Common.Contracts.DTO.Pyramid
{
    [Serializable]
    public sealed class PeriodOrYear : Period
    {
        public PeriodOrYear( int quarterNumber, int year ) : base( quarterNumber, year ) { }

        public PeriodOrYear( int year ) : base()
        {
            System.Diagnostics.Contracts.Contract.Requires( year > 0 );

            Year = year;
        }

        public override string ToString()
        {
            string text = Code == 0 ? Year.ToString( CultureInfo.InvariantCulture ) : base.ToString();

            return text;
        }
    }
}