﻿using System.Collections.Generic;

namespace Luxoft.NDS2.Common.Contracts.DTO.Pyramid.Helpers
{
    public sealed class DeductionDetailComparer : IComparer<DeductionDetail>, IEqualityComparer<DeductionDetail>
    {
        /// <summary>Compares two objects and returns a value indicating whether one is less than, equal to, or greater than the other.</summary>
        /// <returns>A signed integer that indicates the relative values of <paramref name="x" /> and <paramref name="y" />, as shown in the following table.Value Meaning Less than zero<paramref name="x" /> is less than <paramref name="y" />.Zero<paramref name="x" /> equals <paramref name="y" />.Greater than zero<paramref name="x" /> is greater than <paramref name="y" />.</returns>
        /// <param name="x">The first object to compare.</param>
        /// <param name="y">The second object to compare.</param>
        public int Compare( DeductionDetail x, DeductionDetail y )
        {
            if ( object.ReferenceEquals( x, y ) ) return 0;
            if ( object.ReferenceEquals( x, null ) ) return int.MinValue;
            if ( object.ReferenceEquals( null, y ) ) return int.MaxValue;
            int result = x.PeriodOrYear.CompareTo( y.PeriodOrYear );

            return result;
        }

        /// <summary>Determines whether the specified objects are equal.</summary>
        /// <returns>true if the specified objects are equal; otherwise, false.</returns>
        /// <param name="x">The first object of type <paramref name="T" /> to compare.</param>
        /// <param name="y">The second object of type <paramref name="T" /> to compare.</param>
        public bool Equals( DeductionDetail x, DeductionDetail y )
        {
            return 0 == Compare( x, y );
        }

        /// <summary>Returns a hash code for the specified object.</summary>
        /// <returns>A hash code for the specified object.</returns>
        /// <param name="obj">The <see cref="T:System.Object" /> for which a hash code is to be returned.</param>
        /// <exception cref="T:System.ArgumentNullException">The type of <paramref name="obj" /> is a reference type and <paramref name="obj" /> is null.</exception>
        public int GetHashCode( DeductionDetail obj )
        {
            return obj.PeriodOrYear.GetHashCode();
        }
    }
}