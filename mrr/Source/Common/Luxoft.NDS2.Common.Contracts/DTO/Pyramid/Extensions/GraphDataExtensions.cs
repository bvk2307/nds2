﻿using System;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration.Converters;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace.Enums;
using Luxoft.NDS2.Common.Contracts.Helpers;

namespace Luxoft.NDS2.Common.Contracts.DTO.Pyramid.Extensions
{
    /// <summary> Auxilaries for <see cref="GraphData"/>. </summary>
    public static class GraphDataExtensions
    {
	//apopov 27.9.2016	//analysis is moved onto the database side
        ///// <summary> A sign type of declaration ('Признак декларации') (see <see cref="DeclarationType"/>). </summary>
        ///// <param name="graphData"></param>
        ///// <exception cref="FormatException"> if <see cref="DeclarationType"/> contains an invalid value or 'null'. </exception>
        ///// <returns></returns>
        //public static DeclarationType SignType( this GraphData graphData )
        //{
        //    return graphData.VatTotal >= 0 ? DeclarationType.ToPay : DeclarationType.ToCharge;
        //}

        /// <summary> A sign type of declaration ('Признак декларации') (see <see cref="DeclarationType"/>). </summary>
        /// <param name="graphData"></param>
        /// <exception cref="FormatException"> if <see cref="DeclarationType"/> contains an invalid value or 'null'. </exception>
        /// <returns></returns>
        public static string SignTypeString(this GraphData graphData)
        {
            return SignTypeConverter.ConvertTo(graphData.SignType);
        }
        
        private static DeclarationSignTypeConverter SignTypeConverter
        {
            get { return DeclarationSignTypeConverter.Current; }
        }

        /// <summary> Исчисленный НДС, руб., в строковом представлении </summary>
        public static string ClacNdsString( this GraphData graphData )
        {
            return NumericFormatHelper.ToTwoDigitAfterDot( graphData.CalcNds );
        }

        /// <summary> Вычет по НДС, руб., в строковом представлении </summary>
        public static string DeductionNdsString( this GraphData graphData )
        {
            return NumericFormatHelper.ToTwoDigitAfterDot( graphData.DeductionNds );
        }

        /// <summary> Доля вычетов в исчисленном НДС у контрагента, %%, в строковом представлении </summary>
        public static string NdsPercentageString( this GraphData graphData )
        {
            return NumericFormatHelper.ToTwoDigitAfterDot( graphData.NdsPercentage );
        }

        /// <summary> Iterates through nodes from a child to parent (by default) or virce versa. </summary>
        /// <param name="graphData"></param>
        /// <param name="beginFromParent"> To use direction from a parent to child one. Optional. By default is 'false'. </param>
        /// <param name="excludeThis"> Does not include <paramref name="graphData"/> into iteration if it is 'true'. Optional. By default is 'false'. </param>
        /// <returns></returns>
        public static IEnumerable<GraphDataNode> IterateParents(this GraphDataNode graphData, bool beginFromParent = false, bool excludeThis = false)
        {
            if ( beginFromParent )
            {
                List<GraphDataNode> datas = new List<GraphDataNode>(6 + 1); //6 is common reports' level depth
                if ( !excludeThis )
                    datas.Add( graphData );
                GraphDataNode graphDataCur = graphData;
                while ( null != ( graphDataCur = graphDataCur.Parent ) )
                {
                    datas.Add( graphDataCur );
                }
                datas.Reverse();

                foreach (GraphDataNode data in datas)
                {
                    yield return data;
                }
            }
            else
            {
                if ( !excludeThis )
                    yield return graphData;
                GraphDataNode graphDataCur = graphData;
                while ( null != ( graphDataCur = graphDataCur.Parent ) )
                {
                    yield return graphDataCur;
                }
            }
        }
    }
}