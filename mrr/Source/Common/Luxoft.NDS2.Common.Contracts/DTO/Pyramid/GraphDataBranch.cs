using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Common.Contracts.DTO.Pyramid
{
    [Serializable]
    public class GraphDataBranch : GraphDataNode
    {
        public GraphDataBranch ParentOfBranch
        {
            get { return ((GraphDataBranch)Parent); }
        }

        public BranchStatus Status { get; set; }

        public IList<GraphDataBranch> ChildrenList
        {
            get;
            private set;
        }

        public int IndexInParent { get; set; }

        public int TreeLevel { get; set; }

        public GraphDataBranch(GraphData data)
            : base(data)
        { ChildrenList = new List<GraphDataBranch>(); }

        public void CheckAllChildrenLoaded()
        {
            //���������, �� ���� �� �������� ��������� ����������
            foreach (var branch in ChildrenList)
            {
                if (branch != null && !branch.Status.HasFlag(BranchStatus.AllChildrenLoaded))
                {
                    return;
                }
            }
            //���� ��, ���������� ������ � ��������� ��������
            Status |= BranchStatus.AllChildrenLoaded;
            if (ParentOfBranch != null)
            {
                ParentOfBranch.CheckAllChildrenLoaded();
            }
        }

        public void SetChildren(List<GraphDataBranch> children)
        {
            ChildrenList = children;
        }

        public void CheckAllSentInReport()
        {
            if (Status.HasFlag(BranchStatus.AllChildrenLoaded))
            {
                //���������, �� ���� �� �������� ��������� ����������
                //������ ��������� ������ ��������� ��� ��� � ����� ����� �� ������� � ������
                int i = ChildrenList.Count - 1;
                if (i >= 0 && ChildrenList[i] != null && !ChildrenList[i].Status.HasFlag(BranchStatus.AllChildrenSentInReport))
                {
                    return;
                }
                //���� ��, ���������� ������ � ��������� ��������
                Status |= BranchStatus.AllChildrenSentInReport;
                if (ParentOfBranch != null)
                {
                    ParentOfBranch.CheckAllSentInReport();
                }
            }
        }
    }
}