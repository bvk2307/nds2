using System;
using System.ComponentModel;
using Luxoft.NDS2.Common.Contracts.CustomAttributes;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration.Enums;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid.Extensions;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting.TaxPayer;
using Luxoft.NDS2.Common.Contracts.Helpers;

namespace Luxoft.NDS2.Common.Contracts.DTO.Pyramid
{
    [Serializable]
    public class GraphDataNode
    {
        private GraphDataNode _parent;
        private readonly GraphData _graphData;

        public GraphDataNode()
        { 
        }

        public GraphDataNode(GraphData graphData)
        {
            _graphData = graphData;
        }

        public GraphData GraphData
        {
            get { return _graphData; }  
        }

        public GraphDataNode Parent
        {
            get { return _parent; }
            set
            {
                if (_parent != null)
                    throw new InvalidOperationException(string.Format("Try to reassign the node parent from INN: {0} to INN {1}", _parent.Inn, value == null ? null : value.Inn));

                _parent = value;
            }
        }
         
        /// <summary> Tax payer string identifier. </summary>
        public TaxPayerId TaxPayerId
        {
            get
            {
                return _graphData.TaxPayerId;
            }
        }

        public long Level
        { 
            get
            {
                return _graphData.Level;
            }
        }

        public long LevelOrderNumber
        {
            get
            {
                return _graphData.LevelOrderNumber;
            }
        }

        public long Id
        {
            get
            {
                return _graphData.Id;
            }
        }

        public string ParentInn
        {
            get
            {
                return _graphData.ParentInn;
            }
        }

        [DisplayName( "��� �����������" )]
        [Description( "��� �����������" )]
        public string Inn
        { 
            get
            {
                return _graphData.Inn;
            }
        }

        [DisplayName("��� ����������� ��������� �� ��������� � �����������")]
        [Description("��� ����������� ��������� �� ��������� � �����������")]
        [CurrencyFormat]
        public decimal MappedAmount
        {
            get
            {
                return _graphData.MappedAmount;
            }
        }

        [DisplayName("����� ���, ���������� ������ � ���������� �� ��������� � ���������")]
        [Description("����� ���, ���������� ������ � ���������� �� ��������� � ���������")]
        [CurrencyFormat]
        public decimal NotMappedAmnt
        {
            get
            {
                return _graphData.NotMappedAmnt;
            }
        }

        public string Kpp
        {
            get
            {
                return _graphData.Kpp;
            }
        }

        public string Name
        { 
            get
            {
                return _graphData.Name;
            }
        }

        [DisplayName("������� ��")]
        [Description("������� ��")]
        public string DeclarationType
        {
            get
            {
                return _graphData.SignTypeString();
            }
        }

        /// <summary> Document type code: a declaration or a journal. </summary>
        public DeclarationTypeCode TypeCode
        {
            get
            {
                return _graphData.TypeCode;
            }
        }

        /// <summary> A declaration inspection code and name. </summary>
        public CodeValueDictionaryEntry Inspection
        {
            get
            {
                return _graphData.Inspection;
            }
        }

        /// <summary> Processing stage in '��'. </summary>
        public DeclarationProcessignStage ProcessingStage
        {
            get
            {
                return _graphData.ProcessingStage;
            }
        }

        public long ClientNumber
        {
            get
            {
                return _graphData.ClientNumber;
            }
        }

        public long SellerNumber
        {
            get
            {
                return _graphData.SellerNumber;
            }
        }

        [DisplayName("����� ���, ���������� ������ � �����������, �����")]
        [Description("����� ���, ���������� ������ � �����������, �����")]
        [CurrencyFormat]
        public decimal DeductionNds
        {
            get
            {
                return _graphData.DeductionNds;
            }
        }

        [DisplayName("��� ����������� � �����������, �����")]
        [Description("��� ����������� � �����������, �����")]
        [CurrencyFormat]
        public decimal CalcNds
        {
            get
            {
                return _graphData.CalcNds;
            }
        }

        [DisplayName( "����� �������� � ���������� � ���������" )]
        [Description( "����� �������� � ���������� � ���������" )]
        [CurrencyFormat]
        public decimal DiscrepancyAmnt
        {
            get
            {
                return _graphData.DiscrepancyAmnt;
            }
            set
            {
                _graphData.DiscrepancyAmnt = value;
            }
        }

        public bool IsZombie
        {
            get
            {
                return _graphData.IsZombie;
            }
        }

        public bool IsTransitional
        {
            get
            {
                return _graphData.IsTransitional;
            }
        }

        public decimal VatShare
        {
            get
            {
                return _graphData.VatShare;
            }
        }

        public decimal VatTotal
        {
            get
            {
                return _graphData.VatTotal;
            }
        }


        [DisplayName( "����� �������� �� ����������� (�� ������� �����������), �����" )]
        [Description( "����� �������� �� ����������� (�� ������� �����������), �����" )]
        [CurrencyFormat]
        public decimal GapDiscrepancyTotal
        {
            get
            {
                return _graphData.GapDiscrepancyTotal;
            }
        }

        [DisplayName("���� ������� � ���������� �� ��������� � ���������")]
        [Description("���� ������� � ���������� �� ��������� � ���������")]
        public decimal ClientNdsPercentage
        {
            get { return _graphData.ClientNdsPercentage; }
        }

        [DisplayName("���� ������������ ��� ��������� �� ��������� � �����������")]
        [Description("���� ������������ ��� ��������� �� ��������� � �����������")]
        public decimal SellerNdsPercentage
        {
            get { return _graphData.SellerNdsPercentage; }
        }

        [DisplayName("���� ������� � ����������� ��� � �����������")]
        [Description("���� ������� � ����������� ��� � �����������")]
        public decimal NdsPercentage
        {
            get { return _graphData.NdsPercentage; }
        }

        public string NdsPercentageText
        {
            get { return CalcNds != 0 ? NumericFormatHelper.ToTwoDigitAfterDot(NdsPercentage) : "��� �� ��������"; }
        }

        private decimal CalculatePercentage(decimal sum1, decimal sum2)
        {
            return sum2 == 0 ? 0 : Math.Round(sum1 / sum2 * 100, 2);
        }

        [DisplayName("��� ����������� ��������� �� ��������� � �����������")]
        [Description("��� ����������� ��������� �� ��������� � �����������")]
        [CurrencyFormat]
        public decimal ParentMappedAmnt
        {
            get { return Parent == null ? 0 : Parent.MappedAmount; }
        }

        [DisplayName("����� ���, ���������� ������ � ���������� �� ��������� � ���������")]
        [Description("����� ���, ���������� ������ � ���������� �� ��������� � ���������")]
        [CurrencyFormat]
        public decimal ParentNotMappedAmnt
        {
            get { return Parent == null ? 0 : Parent.NotMappedAmnt; }
        }

        [DisplayName("���� ������� � ���������� �� ��������� � ���������")]
        [Description("���� ������� � ���������� �� ��������� � ���������")]
        public decimal ParentClientNdsPercentage
        {
            get { return Parent == null ? 0 : Parent.ClientNdsPercentage; }
        }

        [DisplayName("���� ������������ ��� ��������� �� ��������� � �����������")]
        [Description("���� ������������ ��� ��������� �� ��������� � �����������")]
        public decimal ParentSellerNdsPercentage
        {
            get { return Parent == null ? 0 : Parent.SellerNdsPercentage; }
        }

        [DisplayName( "���" )]
        [Description( "���" )]
        public int? Sur
        {
            get
            {
                return _graphData.Sur;
            }
        }

        public int? SurColorArgb
        {
            get
            {
                return _graphData.SurColorArgb;
            }
        }

        [DisplayName( "���" )]
        [Description( "���" )]
        public string SurDescription
        {
            get
            {
                return _graphData.SurDescription;
            }
        }

        public bool? SurIsDefault
        {
            get
            {
                return _graphData.SurIsDefault;
            }
        }

        [DisplayName( "������" )]
        [Description( "������" )]
        public string Region
        {
            get
            {
                return _graphData.Region;
            }
        }

        [DisplayName( "��� �������" )]
        [Description( "��� �������" )]
        public string RegionCode
        {
            get
            {
                return _graphData.RegionCode;
            }
        }

        [DisplayName( "�������� �������" )]
        [Description( "�������� �������" )]
        public string RegionName
        {
            get
            {
                return _graphData.RegionName;
            }
        }
         
        public string ContractorSpecificationChain
        {
            get
            {
                return _graphData.ContractorSpecificationChain;
            }
        }

        [DisplayName("����� ����������� ���� �������� ���")]
        [Description("����� ����������� ���� �������� ���")]
        [CurrencyFormat]
        public decimal DiscrepancyNdsAmnt
        {
            get
            {
                return _graphData.DiscrepancyNdsAmnt;
            }
        }

        [DisplayName("��� �� ������� ����� ������������ �� ��������")]
        [Description("��� �� ������� ����� ������������ �� ��������")]
        [CurrencyFormat]
        public decimal CreatedInvoiceAmnt
        {
            get
            {
                return _graphData.CreatedInvoiceAmnt;
            }
        }

        [DisplayName("��� �� ������� ����� ���������� �� ����������")]
        [Description("��� �� ������� ����� ���������� �� ����������")]
        [CurrencyFormat]
        public decimal ReceivedInvoiceAmnt
        {
            get
            {
                return _graphData.ReceivedInvoiceAmnt;
            }
        }

        [DisplayName("������ � ������� ���������� �� 01.01.2015")]
        [Description("������ � ������� ���������� �� 01.01.2015")]
        [CurrencyFormat]
        public decimal DeductionBefore20150101Amnt
        {
            get
            {
                return _graphData.DeductionBefore20150101Amnt;
            }
        }

        [DisplayName("������ � ������� ���������� ����� 01.01.2015")]
        [Description("������ � ������� ���������� ����� 01.01.2015")]
        [CurrencyFormat]
        public decimal DeductionAfter20150101Amnt
        {
            get
            {
                return _graphData.DeductionAfter20150101Amnt;
            }
        }

        [DisplayName("����� ����������� ���� ������� � ��������� ��ѻ � ���������� � ���������")]
        [Description("����� ����������� ���� ������� � ��������� ��ѻ � ���������� � ���������")]
        [CurrencyFormat]
        public decimal DiscrepancySumAmnt
        {
            get
            {
                return _graphData.DiscrepancySumAmnt;
            }
        }

        [DisplayName("����� ����������� ���� �������� ���, �����")]
        [Description("����� ����������� ���� �������� ���, �����")]
        [CurrencyFormat]
        public decimal NdsDiscrepancyTotal
        {
            get
            {
                return _graphData.NdsDiscrepancyTotal;
            }
            set
            {
                _graphData.NdsDiscrepancyTotal = value;
            }
        }

        [DisplayName("����� ����������� ���� ������� � ��������� ��ѻ �� ������� ��������, �����")]
        [Description("����� ����������� ���� ������� � ��������� ��ѻ �� ������� ��������, �����")]
        [CurrencyFormat]
        public decimal DiscrepancySumAmntTotal
        {
            get
            {
                return _graphData.DiscrepancySumAmntTotal;
            }
        }

        /// <summary>
        /// ����� ��������� �� �� ������ ������������� ����, � �� ������ ���������
        /// </summary>
        public bool IsReverted { get { return _graphData.IsReverted; } }

        /// <summary> Specification of '������ ����������� �� ���������� �������' (deduction details for the previous period) as the list of value pairs: period - sum with delimiters. </summary>
        public string PreviousDeductionsSpecification { get { return _graphData.PreviousDeductionsSpecification; } }

        public string IsLarge
        {
            get { return GraphData.IsLarge ? "��" : "���"; }
        }
    }
}