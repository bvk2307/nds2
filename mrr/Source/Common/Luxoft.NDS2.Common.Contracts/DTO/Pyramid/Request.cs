﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Pyramid
{
    /// <summary>
    /// Этот класс описывает структуру объектов передачи данных о запросе связей контрагента в СОВ
    /// </summary>
    [Serializable]
    public class Request
    {
        public string Inn { get; set; }

        public string Kpp { get; set; }

        public string Year { get; set; }

        public string Period { get; set; }

        public int RequestType { get; set; }
    }
}
