﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using FLS.Common.Lib.Collections.Extensions;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting.TaxPayer;
using Luxoft.NDS2.Common.Contracts.Services;

namespace Luxoft.NDS2.Common.Contracts.DTO.Pyramid
{
    /// <summary> Parameters of graph tree nodes loading (see <see cref="IPyramidDataService.LoadGraphNodes"/>. </summary> 
    [Serializable]
    public class GraphNodesKeyParameters : ICloneable
    {
        private Period _period = null;

        public GraphNodesKeyParameters(bool isByPurchase, int quarter, int year, IEnumerable<TaxPayerId> ids)
        { 
            System.Diagnostics.Contracts.Contract.Requires(0 < quarter && quarter <= 4);
            System.Diagnostics.Contracts.Contract.Requires(year > 2000);
            System.Diagnostics.Contracts.Contract.Requires(ids != null);
            System.Diagnostics.Contracts.Contract.Ensures(TaxPayerIds.Count > 0 
                && TaxPayerIds.All( taxPayerId => !string.IsNullOrEmpty( taxPayerId.Inn ) ) );
            
            IsPurchase = isByPurchase;
            TaxYear = year;
            TaxQuarter = quarter;

            MostImportantContractors = 250;
            LayersLimit = 2;
            SharedNdsPercent = 1;
            MinReturnedContractors = 0;
            IsRoot = false;            
            TaxPayerIds = ids.ToReadOnly(); 
        }

        /// <summary> Tax payer string identifier. </summary>
        public TaxPayerId TaxPayerId
        {
            get
            {
               return TaxPayerIds.FirstOrDefault();
            }
        }

        public IReadOnlyCollection<TaxPayerId> TaxPayerIds { get; protected set; }

        public string Inn { get { return TaxPayerId == null ? null : TaxPayerId.Inn; } }

        public string Kpp { get { return TaxPayerId == null ? null : TaxPayerId.Kpp; } }

        public int TaxQuarter { get; protected set; }

        public int TaxYear { get; protected set; }

        public Period Period
        {
            get
            {
                if ( _period == null )
                    _period = new Period( TaxQuarter, TaxYear );

                return _period;
            }
        }

        public bool IsPurchase { get; protected set; }

        public bool IsRoot { get; protected set; }

        public int MostImportantContractors { get; protected set; }

        public decimal SharedNdsPercent { get; protected set; }

        /// <summary> A limitation on requested node layer number. </summary>
        public int LayersLimit { get; protected set; }

        /// <summary> A min filtered count of the node children, wich returned from stored procedure. </summary>
        public int MinReturnedContractors { get; protected set; }

        public bool ShowReverted { get; protected set; }
        
        public static GraphNodesKeyParameters New(GraphNodesKeyParameters treeParameters,
            bool? isByPurchase = null,
            int? year = null,
            int? quarter = null,
            decimal? sharedNdsPercent = null,
            int? mostImportantContractors = null,
            int? layersLimit = null,
            int? minReturnedContractors = null,
            bool? isRoot = null,
            bool? showReverted = null,
            IEnumerable<TaxPayerId> ids = null)
        {
            GraphNodesKeyParameters treeParametersNew = treeParameters.Clone();

            if (ids != null)
                treeParametersNew.TaxPayerIds = ids.ToReadOnly();

            if (isByPurchase.HasValue)
                treeParametersNew.IsPurchase = isByPurchase.Value;

            if (year.HasValue)
                treeParametersNew.TaxYear = year.Value;

            if (quarter.HasValue)
                treeParametersNew.TaxQuarter = quarter.Value;

            if (layersLimit.HasValue)
                treeParametersNew.LayersLimit = layersLimit.Value;

            if (sharedNdsPercent.HasValue)
                treeParametersNew.SharedNdsPercent = sharedNdsPercent.Value;

            if (mostImportantContractors.HasValue)
                treeParametersNew.MostImportantContractors = mostImportantContractors.Value;

            if (minReturnedContractors.HasValue)
                treeParametersNew.MinReturnedContractors = minReturnedContractors.Value;

            if (isRoot.HasValue)
                treeParametersNew.IsRoot = isRoot.Value;

            if ( showReverted.HasValue ) 
                treeParametersNew.ShowReverted = showReverted.Value;

            return treeParametersNew;
        }

        /// <summary> Returnes a deep clone created. </summary>
        /// <returns></returns>
        GraphNodesKeyParameters Clone()
        {
            return SerializationDeepCloner.Clone(this);
        }

        #region ICloneable implementation

        /// <summary> Returnes a deep clone created. </summary>
        /// <returns></returns>
        object ICloneable.Clone()
        {
            return Clone();
        }

        #endregion ICloneable implementation

        /// <summary> Simple cloning by serialization of a serializable object. </summary>
        [Obsolete( "Use FLS.CommonComponents.Lib.Serialization.SerializationDeepCloner istead of this one", error: false )]
        private static class SerializationDeepCloner
        {
            /// <summary> Clones a plane type structure of type <typeparamref name="T"/>. </summary>
            /// <typeparam name="T"></typeparam>
            /// <param name="tdata"></param>
            /// <returns></returns>
            public static T Clone<T>( T tdata )
            {
                System.Diagnostics.Contracts.Contract.Requires( tdata != null );
                if ( tdata == null )
                    throw new ArgumentOutOfRangeException( "tdata", String.Format( "The parameter value: '{0}' is empty", tdata ) );
                System.Diagnostics.Contracts.Contract.EndContractBlock();

                T tclone;
                using ( MemoryStream stream = CreateStream() )
                {
                    BinaryFormatter formatter = CreateFormatter();
                    formatter.Serialize( stream, tdata );
                    stream.Position = 0;
                    object oclone = formatter.Deserialize( stream );
                    tclone = (T)oclone;
                }
                return tclone;
            }

            private static MemoryStream CreateStream( byte[] bytes = null )
            {
                System.Diagnostics.Contracts.Contract.Ensures( System.Diagnostics.Contracts.Contract.Result<MemoryStream>() != null );

                MemoryStream stream;
                if ( bytes == null )
                    stream = new MemoryStream();
                else
                    stream = new MemoryStream( bytes );

                return stream;
            }

            private static BinaryFormatter CreateFormatter()
            {
                System.Diagnostics.Contracts.Contract.Ensures( System.Diagnostics.Contracts.Contract.Result<BinaryFormatter>() != null );

                return new BinaryFormatter();
            }
        }
    }
}