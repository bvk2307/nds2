﻿using System;
namespace Luxoft.NDS2.Common.Contracts.DTO.Pyramid
{
    /// <summary>
    /// Этот класс определяет структуру объектов передачи данных контрагентов
    /// </summary>
    [Serializable]
    public class Contractor
    {
        /// <summary>
        /// Возвращает или задает идентификатор данных конрагента в контексте отчета "Пирамида"
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Возвращает или задает идентификатор запроса, по которому данные контрагента были получены.
        /// null только для корневого элемента деерва связей
        /// </summary>
        public long? RequestId { get; set; }

        /// <summary>
        /// Возвращает или задает идентификатор запроса связей данного конрагента
        /// </summary>
        public long? SelfRequestId { get; set; }

        public string Name { get; set; }

        public string Inn { get; set; }

        public string Kpp { get; set; }
    }
}
