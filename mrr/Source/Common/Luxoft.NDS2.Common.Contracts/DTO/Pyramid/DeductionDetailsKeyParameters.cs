﻿using System;
using Luxoft.NDS2.Common.Contracts.DTO.Business.TaxPayer;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting.TaxPayer;
using System.Collections.Generic;

namespace Luxoft.NDS2.Common.Contracts.DTO.Pyramid
{
    /// <summary> Request key parameters for request of taxpayer deduction sums for the previous tax periods (see 'IPyramidReportAdapter.LoadDeductionDetails()'. </summary>
    [Serializable]
    public class DeductionDetailsKeyParameters : GraphNodesKeyParameters	//apopov 5.4.2016	//DEBUG!!!
    {
        public DeductionDetailsKeyParameters(TaxPayerId taxPayerId, bool isByPurchase, int quarter, int year)
            : base(isByPurchase, quarter, year, new[] { taxPayerId }){}

        public DeductionDetailsKeyParameters(GraphNodesKeyParameters graphNodesKeyParameters)
            : base(
            graphNodesKeyParameters.IsPurchase, 
            graphNodesKeyParameters.TaxQuarter, 
            graphNodesKeyParameters.TaxYear,
            graphNodesKeyParameters.TaxPayerIds) { }


    }
}