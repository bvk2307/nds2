﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Common.Contracts.DTO.Pyramid
{
    [Serializable]
    public class Period : IComparable<Period>, IEquatable<Period>
    {
        public const string QuarterShortText = "кв";
        public const string QuarterShortTextWDot = QuarterShortText + ".";

        public static readonly Dictionary<int, int> Quaters =
            new Dictionary<int, int>()
            {
                { 21, 1 },
                { 22, 2 },
                { 23, 3 },
                { 24, 4 },
            };

        public static Dictionary<string, string[]> AllCodes =
            new Dictionary<string, string[]>
            {
                { "21", new []{"21", "01", "02", "03", "51", "71", "72", "73"} },
                { "22", new []{"22", "04", "05", "06", "54", "74", "75", "76"} },
                { "23", new []{"23", "07", "08", "09", "55", "77", "78", "79"} },
                { "24", new []{"24", "10", "11", "12", "56", "80", "81", "82"} }
            };

        public static int GetQuarterByCode(string code)
        {
            return Quaters[int.Parse(AllCodes.First(x => x.Value.Contains(code)).Key)];
        }

        public static readonly Dictionary<int, int> QuatersByNumber = Quaters.ToDictionary( kvpair => kvpair.Value, kvpair => kvpair.Key );

        [NonSerialized]
        private PeriodComparer _comparer = null;

        public Period() { }

        public Period( int quarterNumber, int year )
        {
            System.Diagnostics.Contracts.Contract.Requires( 1 <= quarterNumber && quarterNumber <= 4 );
            System.Diagnostics.Contracts.Contract.Requires( year > 0 );

            Code = QuatersByNumber[quarterNumber];
            Year = year;
        }

        public int Code { get; set; }

        public int Year { get; set; }

        /// <summary> The quarter number with value from 1 to 4. </summary>
        public int QuarterNumber
        {
            get { return Quaters[Code]; }
        }

        private PeriodComparer Comparer
        {
            get
            {
                if ( _comparer == null )
                    _comparer = new PeriodComparer();

                return _comparer;
            }
        }

        public static bool operator ==( Period period1, Period period2 )
        {
            if ( object.Equals( (object)period1, null ) )
                return object.Equals( (object)period2, null );
            if ( object.Equals( (object)period2, null ) )
                return false;
            return period1.Comparer.Equals( period1, period2 );
        }

        public static bool operator !=( Period period1, Period period2 )
        {
            if ( object.Equals( (object)period1, null ) )
                return !object.Equals( (object)period2, null );
            if ( object.Equals( (object)period2, null ) )
                return true;
            return !period1.Comparer.Equals( period1, period2 );
        }

        public static bool operator >( Period period1, Period period2 )
        {
            if ( object.Equals( (object)period1, null ) )
            {
                if ( object.Equals( (object)period2, null ) )
                    return false;
                return 0 < period2.Comparer.Compare( period1, period2 );
            }
            return 0 < period1.Comparer.Compare( period1, period2 );
        }

        public static bool operator <( Period period1, Period period2 )
        {
            if ( object.Equals( (object)period1, null ) )
            {
                if ( object.Equals( (object)period2, null ) )
                    return false;
                return 0 > period2.Comparer.Compare( period1, period2 );
            }
            return 0 > period1.Comparer.Compare( period1, period2 );
        }

        public static bool operator >=( Period period1, Period period2 )
        {
            if ( object.Equals( (object)period1, null ) )
            {
                if ( object.Equals( (object)period2, null ) )
                    return false;
                return 0 <= period2.Comparer.Compare( period1, period2 );
            }
            return 0 <= period1.Comparer.Compare( period1, period2 );
        }

        public static bool operator <=( Period period1, Period period2 )
        {
            if ( object.Equals( (object)period1, null ) )
            {
                if ( object.Equals( (object)period2, null ) )
                    return false;
                return 0 >= period2.Comparer.Compare( period1, period2 );
            }
            return 0 >= period1.Comparer.Compare( period1, period2 );
        }

        /// <summary>Indicates whether the current object is equal to another object of the same type.</summary>
        /// <returns>true if the current object is equal to the <paramref name="other" /> parameter; otherwise, false.</returns>
        /// <param name="other">An object to compare with this object.</param>
        public bool Equals( Period other )
        {
            return Comparer.Equals( this, other );
        }

        /// <summary>Determines whether the specified <see cref="T:System.Object" /> is equal to the current <see cref="T:System.Object" />.</summary>
        /// <returns>true if the specified <see cref="T:System.Object" /> is equal to the current <see cref="T:System.Object" />; otherwise, false.</returns>
        /// <param name="other">The object to compare with the current object. </param>
        public override bool Equals( object other )
        {
            if ( object.ReferenceEquals( null, other ) ) return false;
            if ( object.ReferenceEquals( this, other ) ) return true;
            return other is Period && Equals( (Period)other );
        }

        /// <summary>Serves as a hash function for a particular type. </summary>
        /// <returns>A hash code for the current <see cref="T:System.Object" />.</returns>
        public override int GetHashCode()
        {
            return Comparer.GetHashCode( this );
        }

        public override string ToString()
        {
            return string.Join( " ", QuarterNumber, QuarterShortTextWDot, Year );
        }

        public string ToDelimiterFreeString()
        {
            return string.Concat( QuarterNumber, QuarterShortText, Year );
        }

        #region Implementation of IComparable<Period>

        /// <summary>Compares the current object with another object of the same type.</summary>
        /// <returns>A value that indicates the relative order of the objects being compared. The return value has the following meanings: Value Meaning Less than zero This object is less than the <paramref name="other" /> parameter.Zero This object is equal to <paramref name="other" />. Greater than zero This object is greater than <paramref name="other" />. </returns>
        /// <param name="other">An object to compare with this object.</param>
        public int CompareTo( Period other )
        {
            return Comparer.Compare( this, other );
        }

        public class PeriodComparer : IComparer<Period>, IEqualityComparer<Period>
        {
            /// <summary>Compares two objects and returns a value indicating whether one is less than, equal to, or greater than the other.</summary>
            /// <returns>A signed integer that indicates the relative values of <paramref name="x" /> and <paramref name="y" />, as shown in the following table.Value Meaning Less than zero<paramref name="x" /> is less than <paramref name="y" />.Zero<paramref name="x" /> equals <paramref name="y" />.Greater than zero<paramref name="x" /> is greater than <paramref name="y" />.</returns>
            /// <param name="x">The first object to compare.</param>
            /// <param name="y">The second object to compare.</param>
            public int Compare( Period x, Period y )
            {
                if ( object.ReferenceEquals( x, y ) ) return 0;
                if ( object.ReferenceEquals( x, null ) ) return int.MinValue;
                if ( object.ReferenceEquals( null, y ) ) return int.MaxValue;
                int result = x.Year.CompareTo( y.Year );
                if ( result == 0 )
                    result = ( x.Code == 0 ? 0 : x.QuarterNumber ).CompareTo( y.Code == 0 ? 0 : y.QuarterNumber );

                return result;
            }

            /// <summary>Determines whether the specified objects are equal.</summary>
            /// <returns>true if the specified objects are equal; otherwise, false.</returns>
            /// <param name="x">The first object of type <paramref name="T" /> to compare.</param>
            /// <param name="y">The second object of type <paramref name="T" /> to compare.</param>
            public bool Equals( Period x, Period y )
            {
                return 0 == Compare( x, y );
            }

            /// <summary>Returns a hash code for the specified object.</summary>
            /// <returns>A hash code for the specified object.</returns>
            /// <param name="obj">The <see cref="T:System.Object" /> for which a hash code is to be returned.</param>
            /// <exception cref="T:System.ArgumentNullException">The type of <paramref name="obj" /> is a reference type and <paramref name="obj" /> is null.</exception>
            public int GetHashCode( Period obj )
            {
                unchecked
                {
                    return ( obj.Year * 397 ) ^ ( obj.Code == 0 ? 0 : obj.QuarterNumber );
                }
            }
        }

        #endregion Implementation of IComparable<Period>
    }
}
