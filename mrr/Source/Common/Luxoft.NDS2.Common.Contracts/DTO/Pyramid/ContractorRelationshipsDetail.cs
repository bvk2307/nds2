﻿using System;
using System.ComponentModel;

namespace Luxoft.NDS2.Common.Contracts.DTO.Pyramid
{
    /// <summary>
    /// Этот класс описывает структуру объектов передачи данных таблицы связей контрагентов
    /// </summary>
    [Serializable]
    public class ContractorRelationshipsDetail
    {
        //Сведения о контрагентах
        [DisplayName("Сведения о покупателе")]
        public string BuyerName { get; set; }
        [DisplayName("Сведения о продавце")]
        public string SellerName { get; set; }
        [DisplayName("Уровень")]
        public string Level { get; set; }

        //По данным покупателя
        [DisplayName("Сумма НДС, подлежащая вычету у покупателя, в руб. ")]
        public decimal? DECL_NDS_8 { get; set; }
        [DisplayName("Удельный вес суммы НДС, подлежащей вычету у покупателя, в общей сумме НДС, подлежащей вычету")]
        public decimal? DECL_NDS_8_WT { get; set; }

        //Сведения о продавце
        [DisplayName("СУР")]
        public int SYR { get; set; }
        [DisplayName("Факт представления продавцом налоговой декларации по НДС")]
        public string DECL_PROVIDED { get; set; }

        //Сведения о расхождениях (по сумме НДС)
        [DisplayName("Общая сумма расхождений у продавца")]
        public decimal? DISCR_TOTAL_SUM { get; set; }
        [DisplayName("Количество расхождений у продавца с покупателем")]
        public decimal? DISCR_TOTAL_DEAL_COUNT { get; set; }
        [DisplayName("Сумма расхождений у продавца с покупателем")]
        public decimal? DISCR_TOTAL_DEAL_SUM { get; set; }

        //В том числе сведения о разрывах
        [DisplayName("Общее количество расхождений у продавца")]
        public decimal? DISCR_GAP_TOTAL_COUNT { get; set; }
        [DisplayName("Общая сумма расхождений у продавца")]
        public decimal? DISCR_GAP_TOTAL_SUM { get; set; }
        [DisplayName("Количество расхождений у продавца с покупателем")]
        public decimal? DISCR_GAP_DEAL_COUNT { get; set; }
        [DisplayName("Сумма расхождений у продавца с покупателем")]
        public decimal? DISCR_GAP_DEAL_SUM { get; set; }

        //В том числе сведения о неточных сопоставлениях
        [DisplayName("Общее количество расхождений у продавца")]
        public decimal? DISCR_WEAK_TOTAL_COUNT { get; set; }
        [DisplayName("Общая сумма расхождений у продавца")]
        public decimal? DISCR_WEAK_TOTAL_SUM { get; set; }
        [DisplayName("Количество расхождений у продавца с покупателем")]
        public decimal? DISCR_WEAK_DEAL_COUNT { get; set; }
        [DisplayName("Сумма расхождений у продавца с покупателем")]
        public decimal? DISCR_WEAK_DEAL_SUM { get; set; }

        //В том числе сведения о нарушениях НДС
        [DisplayName("Общее количество расхождений у продавца")]
        public decimal? DISCR_NDS_TOTAL_COUNT { get; set; }
        [DisplayName("Общая сумма расхождений у продавца")]
        public decimal? DISCR_NDS_TOTAL_SUM { get; set; }
        [DisplayName("Количество расхождений у продавца с покупателем")]
        public decimal? DISCR_NDS_DEAL_COUNT { get; set; }
        [DisplayName("Сумма расхождений у продавца с покупателем")]
        public decimal? DISCR_NDS_DEAL_SUM { get; set; }

        //Сведения о декларации продавца
        [DisplayName("Общая сумма НДС, подлежащая вычету, в руб.")]
        public decimal? DECL_NDS_AMOUNT { get; set; }
        [DisplayName("Сумма НДС, исчисленная к уплате (+) или заявленная к возмещению (-), в руб.")]
        public decimal? DECL_NDS_9_FOR_PAY { get; set; }

        //Сумма операций (включая НДС), руб.
        [DisplayName("Сумма сопоставленных операций")]
        public decimal? SUM_RUB_MATCHED { get; set; }
        [DisplayName("Сумма операций с Продавцом по сведениям Покупателя")]
        public decimal? SUM_RUB { get; set; }
        [DisplayName("Сумма операций с Покупателем по сведениям Продавца")]
        public decimal? SUM_RUB_MATCHED_2 { get; set; }
        
        
        
        public string Name { get; set; }
        public string Inn { get; set; }
        public string Kpp { get; set; }
    }
}
