﻿namespace Luxoft.NDS2.Common.Contracts.DTO.Pyramid
{
    public enum RequestStatus 
    { 
        Created,
        InProcess,
        Completed,
        Error
    }
}
