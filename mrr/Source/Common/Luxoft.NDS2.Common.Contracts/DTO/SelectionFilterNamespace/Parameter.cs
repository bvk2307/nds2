﻿using System;
using System.Runtime.Serialization;

namespace Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace
{
    [DataContract]
    [Serializable]
    public class Parameter
    {
        public int AttributeId { get; set; }

        public ComparisonOperator Operator { get; set; }

        public ParameterValue[] Values { get; set; }

        public bool IsEnabled { get; set; }
    }
}
