﻿namespace Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace
{
    public enum SelectionSide
    {
        //dn mock
        Buyer = 0,
        Seller
    }

    public enum EditorArea
    {
        //dn mock
        BuyerSide = 1,
        SellerSide,
        DiscrepancyParameters
    }

    public enum EditorType
    {
        //dn mock
        RegionEditor = 1,
        TaxAuthorityEditor = 2,
        SurEditor = 3,
        PeriodEditor = 4,
        OperationCodeEditor = 5,
        RangeEditor = 6,
        InnEditor = 7,
        RangeOfDateEditor = 8,
        CheckOneEditor = 9,
        TextEditor = 10,
        CheckBoxEditor = 11,
        SourceRecordEditor = 12,
        UnEditor = 13
    }

    public enum ParameterType
    {
        //dn mock
        RangeOfIntModel = 1,
        RangeOfDateModel = 2,
        MultiRangeItemOfIntModel = 3,
        MultiRangeOfIntModel = 4,
        RegionsModel = 5,
        TaxAuthorityModel = 6,
        ListOfStringModel = 7,
        ListModel = 8,
        MultiStringModel = 9,
        MiltiCheckModel = 10,
        StringModel = 11,
        CheckModel = 12,
        LabelModel = 13,
    }

    public enum RawDataType
    {
        //dn mock
        String = 0,
        Int32
    }

    public enum ComparisonOperator
    {
        //dn mock
        NotDefinedOperator = -1,
        IsNull = 0,
        IsNotNull,
        Equals,
        NotEquals,
        InList,
        NotInList,
        Beetween,
        GT,
        LT,
        GE,
        LE,
        StartWith,
        EndWith,
        Contains,
        NotContains
    }

    public enum SelectionFilterVersion
    {
        VersionOne = 1,
        VersionTwo = 2
    }
}
