﻿using System;
using System.Runtime.Serialization;

namespace Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace
{
    [DataContract]
    [Serializable]
    public class EditorParameter
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string AggregateName { get; set; }

        public EditorArea Area { get; set; }

        public EditorType ViewType { get; set; }

        public ParameterType ModelType { get; set; }

        public int AreaId { get; set; }

        public int TypeId { get; set; }

        public int ModelId { get; set; }
    }
}
