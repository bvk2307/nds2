﻿using System;
using System.Runtime.Serialization;

namespace Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace
{
    [DataContract]
    [Serializable]
    public class ParameterGroup
    {
        public string Name { get; set; }

        public Parameter[] Parameters { get; set; }

        public bool IsEnabled { get; set; }
    }
}
