﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace
{
    public class FilterHelper
    {
        public static string Serialize(Filter filter)
        {
            XmlSerializer ser = new XmlSerializer(typeof(Filter));
            StringBuilder sb = new StringBuilder();
            using (TextWriter w = new StringWriter(sb))
            {
                ser.Serialize(w, filter);
            }

            return sb.ToString();
        }

        public static FilterResult Deserialize(string content)
        {
            var filterResult = new FilterResult();

            if (!string.IsNullOrWhiteSpace(content))
            {
                XmlSerializer ser = new XmlSerializer(typeof(Filter));

                try
                {
                    using (TextReader reader = new StringReader(content))
                    {
                        filterResult.Filter = ser.Deserialize(reader) as Filter;
                        filterResult.IsSuccess = true;
                    }
                }
                catch (Exception)
                {
                    filterResult.Filter = new Filter();
                    filterResult.IsSuccess = false;
                }
            }
            else
            {
                filterResult.Filter = new Filter();
                filterResult.IsSuccess = false;
            }

            return filterResult;
        }
    }

    public class FilterResult
    {
        public Filter Filter { get; set; }
        public bool IsSuccess { get; set; }
    }
}
