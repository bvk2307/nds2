﻿using System;
using System.Runtime.Serialization;

namespace Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace
{
    [DataContract]
    [Serializable]
    public class ParameterValue
    {
        public string Value { get; set; } // старое Val1

        public string ValueFrom { get; set; } // старое Val1

        public string ValueTo { get; set; } // старое Val2

        public string ValueDescription { get; set; } // старое Val2
    }
}
