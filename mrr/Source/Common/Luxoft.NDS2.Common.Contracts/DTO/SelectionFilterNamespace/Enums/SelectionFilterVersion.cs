﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace.Enums
{
    public enum SelectionFilterVersion
    {
        Unknow = 0,
        VersionOne = 1,
        VersionTwo = 2
    }
}
