namespace Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace
{
    public enum EditorType
    {
        RegionEditor = 1,
        TaxAuthorityEditor = 2,
        SurEditor = 3,
        PeriodEditor = 4,
        OperationCodeEditor = 5,
        InnEditor = 6,
        CheckBoxEditor = 7,
        ListMultiEditor = 8,
        UntitledCheckBoxEditor = 9,
        IntRangeEditor = 10,
        SimpleListEditor = 11,
        DataRangeEditor = 12,
        UnEditor = 13,
        NdsRangeEditor = 14,
        WhiteListEditor = 15,
        MultiDiscrepancyAmountControl = 16
    }
}