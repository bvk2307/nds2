namespace Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace
{
    public enum ParameterType
    {
        RangeOfIntModel = 1,
        RangeOfDateModel = 2,
        MultiRangeItemOfIntModel = 3,
        MultiRangeOfIntModel = 4,
        RegionsModel = 5,
        TaxAuthorityModel = 6,
        ListOfStringModel = 7,
        ListModel = 8,
        LabelModel = 9,
        MultiRangeOfNDSModel = 10,
        DiscrepancyTypeModel = 11,
        CompareRuleModel = 12,
        OperationCodes = 13,
        PeriodsModel = 14,
        InnFilterModel = 15,
        WhiteListModel = 16,
        MultiDiscrepancyTypeParameterModel = 17
    }
}