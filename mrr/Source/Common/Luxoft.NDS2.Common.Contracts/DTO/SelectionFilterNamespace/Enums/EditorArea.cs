namespace Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace
{
    public enum EditorArea
    {
        BuyerSide = 1,
        SellerSide = 2,
        DiscrepancyParameters = 3
    }
}