﻿using System.ComponentModel.DataAnnotations;

namespace Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace.Enums
{
    public enum DeclarationType
    {
        [Display( Name = "" )]
        Empty = 0,
        [Display( Name = "К уплате" )]
        ToPay = 1,
        [Display( Name = "К возмещению" )]
        ToCharge = 2,
        [Display( Name = "Нулевая" )]
        Zero = 3,
    }

}
