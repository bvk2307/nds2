﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace.Enums
{
    public class EditorParameterConst
    {
        public const int PERIOD_FULL_BUYER = 5;
        public const int PERIOD_FULL_SELLER = 105;
    }
}
