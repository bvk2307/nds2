namespace Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace
{
    public enum ComparisonOperator
    {
        //dn mock
        NotDefinedOperator = -1,
        IsNull = 0,
        IsNotNull,
        Equals,
        NotEquals,
        InList,
        NotInList,
        Beetween,
        GT,
        LT,
        GE,
        LE,
        StartWith,
        EndWith,
        Contains,
        NotContains,
        OneOf
    }
}