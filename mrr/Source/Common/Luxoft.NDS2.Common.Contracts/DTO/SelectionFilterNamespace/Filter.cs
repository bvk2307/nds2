﻿using System;
using System.Linq;
using System.Runtime.Serialization;

namespace Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace
{
    [DataContract]
    [Serializable]
    public class Filter
    {
        public ParameterGroup[] Groups { get; set; }

        public Filter()
        {
            Groups = new ParameterGroup[] {};
        }
    }

    public static class FilterExtension
    {
        public static Filter Copy(this Filter filter)
        {
            return new Filter
            {
                Groups = filter.Groups != null && filter.Groups.Any()
                    ? filter.Groups.Select(
                        group => new ParameterGroup
                        {
                            Name = group.Name,
                            IsEnabled = group.IsEnabled,
                            Parameters = group.Parameters != null && group.Parameters.Any() 
                                ? group.Parameters.Select(param => new Parameter
                                {
                                    AttributeId = param.AttributeId,
                                    IsEnabled = param.IsEnabled,
                                    Operator = param.Operator,
                                    Values = param.Values != null && param.Values.Any()
                                        ? param.Values.Select(value => new ParameterValue
                                        {
                                            Value = value.Value,
                                            ValueFrom = value.ValueFrom,
                                            ValueTo = value.ValueTo,
                                            ValueDescription = value.ValueDescription
                                        }).ToArray()
                                        : new ParameterValue[]{}
                                }).ToArray()
                                : new Parameter[] {}
                        }).ToArray()
                    : new []
                    {
                        new ParameterGroup
                        {
                            IsEnabled = true,
                            Parameters = new Parameter[] {}
                        }
                    }
            };
        }

        public static bool IsEquals(this Filter a, Filter b)
        {
            if (a == null && b == null)
                return true;

            if (a == null || b == null)
                return false;

            return a.Groups.Length == b.Groups.Length &&
                   a.Groups.All(ag => b.Groups.Any(bg => bg.IsEquals(ag)));
        }

        public static bool IsEquals(this ParameterGroup a, ParameterGroup b)
        {
            if (a == null && b == null)
                return true;

            if (a == null || b == null)
                return false;

            return a.Name == b.Name &&
                   a.IsEnabled == b.IsEnabled &&
                   a.Parameters.Length == b.Parameters.Length &&
                   a.Parameters.All(ap => b.Parameters.Any(bp => bp.IsEquals(ap)));
        }

        public static bool IsEquals(this Parameter a, Parameter b)
        {
            if (a == null && b == null)
                return true;

            if (a == null || b == null)
                return false;

            return a.AttributeId == b.AttributeId &&
                   a.IsEnabled == b.IsEnabled &&
                   a.Operator == b.Operator &&
                   a.Values.Length == b.Values.Length &&
                   a.Values.All(av => b.Values.Any(bv => bv.IsEquals(av)));
        }

        public static bool IsEquals(this ParameterValue a, ParameterValue b)
        {
            if (a == null && b == null)
                return true;

            if (a == null || b == null)
                return false;
            
            return a.Value == b.Value &&
                   a.ValueFrom == b.ValueFrom &&
                   a.ValueTo == b.ValueTo &&
                   a.ValueDescription == b.ValueDescription;
        }
    }
}
