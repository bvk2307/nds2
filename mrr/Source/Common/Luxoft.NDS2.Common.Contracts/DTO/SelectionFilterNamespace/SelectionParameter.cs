﻿using System;
using System.Runtime.Serialization;

namespace Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace
{
    [DataContract]
    [Serializable]
    public class SelectionParameter
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
