﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Selection
{
    public enum SelectionSaveReason : int
    {
        Create = 1,
        Remove = 2,
        StatusChange =3,
        Update =4
    }
}
