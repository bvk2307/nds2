﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Selection
{
    [Serializable]
    public class SelectionAutoParameter
    {
        [DisplayName("Регион")]
        [Description("Код и наименование региона")]
        public string Region { get; set; }

        [DisplayName("Инспекция")]
        [Description("Код и наименование инспекции")]
        public string Inspection { get; set; }

        [DisplayName("Минимальная сумма ПВП (руб.)")]
        [Description("Минимальная сумма ПВП (руб.)")]
        public decimal PVPMinSum { get; set; }
    }
}
