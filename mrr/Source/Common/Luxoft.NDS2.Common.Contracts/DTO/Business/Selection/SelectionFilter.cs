﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Luxoft.NDS2.Common.Contracts.DTO
{
    [Serializable]
    public class FilterCriteria : ICloneable
    {
        public enum ValueTypes
        {
            Edit = 0,
            List = 1,
            EditWithComparisonNum = 2,
            EditWithComparisonStr = 3,
            EditIntervalPrice = 4,
            EditIntervalDates = 5,
            ListMultpleChoice = 6,
            EditIntervalInt = 7,
            RegionsMultiSelect = 8,
            TaxAuthoritiesMultiSelect = 9,
            TextMultipleInput = 10,
            ListMultipleCompirison = 11,
            ListMultipleChoiceOperator = 12
        };

        public string Name { get; set; }
        public ValueTypes ValueTypeCode { get; set; }
        public string LookupTableName { get; set; }
        public string InternalName { get; set; }
        public string FirInternalName { get; set; }
        public bool IsRequired { get; set; }
        public bool IsActive { get; set; }
        public List<FilterElement> Values { get; set; }
        public bool IsEmpty { get { return Values == null || !Values.Any(); } }

        public object Clone()
        {
            return new FilterCriteria
            {
                Name = Name,
                ValueTypeCode = ValueTypeCode,
                LookupTableName = LookupTableName,
                InternalName = InternalName,
                FirInternalName = FirInternalName,
                IsRequired = IsRequired,
                IsActive = IsActive,
                Values = null
            };
        }

        public FilterCriteria CloneAsActive()
        {
            var retval = (FilterCriteria)Clone();
            retval.IsActive = true;

            return retval;
        }

        public override bool Equals(object obj)
        {
            var anotherCriteria = obj as FilterCriteria;

            return anotherCriteria != null
                && anotherCriteria.IsActive == IsActive
                && anotherCriteria.Name == Name
                && anotherCriteria.Values.ArrayEquals(Values);
        }
    }

    [Serializable]
    public class FilterElement
    {
        public enum ComparisonOperations
        {
            Equals,
            NotEquals,
            GT,
            LT,
            GE,
            LE,
            Between,
            StartWith,
            EndWith,
            Contains,
            InSet,
            NotContains,
            OneOf
        }

        public ComparisonOperations Operator { get; set; }
        public string ValueOne { get; set; }
        public string ValueTwo { get; set; }

        public override bool Equals(object obj)
        {
            var anotherElement = obj as FilterElement;

            return anotherElement != null
                && anotherElement.Operator == Operator
                && anotherElement.ValueOne == ValueOne
                && anotherElement.ValueTwo == ValueTwo;
        }
    }

    [Serializable]
    public class GroupFilter
    {
        public string Name { get; set; }        
        public List<FilterCriteria> Filters { get; set; }
        public bool IsActive { get; set; }

        public override bool Equals(object obj)
        {
            var anotherGroup = obj as GroupFilter;

            return anotherGroup != null
                && anotherGroup.IsActive == IsActive
                && anotherGroup.Filters.ArrayEquals(Filters);
        }
    }

    [Serializable]
    [Obsolete("Нужно использовать Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace.Filter")]
    public class SelectionFilter
    {
        public SelectionFilter()
        {
            GroupsFilters = new List<GroupFilter>();
        }

        public long Id { get; set; }

        public long? SelectionId { get; set; }

        public bool IsActive { get; set; }

        public string FilterName { get; set; }
        public string UserName { get; set; }
        public bool IsFavourite { get; set; }

        public List<GroupFilter> GroupsFilters { get; set; }

        public string Serialize()
        {
            XmlSerializer ser = new XmlSerializer(typeof(SelectionFilter));
            StringBuilder sb = new StringBuilder();
            using(TextWriter w = new StringWriter(sb))
            {
                ser.Serialize(w, this);
            }

            return sb.ToString();
        }

        public static SelectionFilterResult Deserialize(string content)
        {
            var filterResult = new SelectionFilterResult();

            if (!string.IsNullOrWhiteSpace(content))
            {
                XmlSerializer ser = new XmlSerializer(typeof(SelectionFilter));

                try
                {
                    using (TextReader reader = new StringReader(content))
                    {
                        filterResult.SelectionFilter = ser.Deserialize(reader) as SelectionFilter;
                        filterResult.IsSuccess = true;
                    }
                }
                catch (Exception ex)
                {
                    filterResult.SelectionFilter = new SelectionFilter();
                    filterResult.IsSuccess = false;
                    filterResult.Message = ex.Message;
                }
            }
            else
            {
                filterResult.SelectionFilter = new SelectionFilter();
                filterResult.IsSuccess = false;
                filterResult.Message = String.Empty;
            }

            return filterResult;
        }

        public override bool Equals(object obj)
        {
            var anotherFilter = obj as SelectionFilter;

            return anotherFilter != null
                && anotherFilter.IsActive == IsActive
                && anotherFilter.SelectionId == SelectionId
                && anotherFilter.GroupsFilters.ArrayEquals(GroupsFilters);
        }
    }

    public static class IEnumerableComparisonHelper
    {
        public static bool ArrayEquals<T>(this IEnumerable<T> thisCollection, IEnumerable<T> collectionToCompare)
        {
            return thisCollection != null
                && collectionToCompare != null
                && thisCollection.Count() == collectionToCompare.Count()
                && thisCollection.All(
                    thisItem => collectionToCompare.Any(thatItem => thatItem.Equals(thisItem)));
        }
    }

    public class SelectionFilterResult
    {
        public SelectionFilter SelectionFilter { get; set; }
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}
