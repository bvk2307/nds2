﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Selection
{
    [Serializable]
    public sealed class SelectionLock
    {
        public string Name { get; set; }

        public LockState State { get; set; }
    }
}