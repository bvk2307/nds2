﻿using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace.Enums;
using System;
using System.ComponentModel;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Selection
{
    /// <summary>
    /// Этот класс представляет избранный фильтр выборки
    /// </summary>
    [Serializable]
    public class FavoriteFilter : BusinessObject
    {
        # region Свойства

        /// <summary>
        /// Возвращает или задает идентификатор фильтра выборки
        /// </summary>
        [DisplayName("Номер")]
        [Description("Номер фильтра")]  
        public long Id { get; set; }

        /// <summary>
        /// Возвращает или задает владельца фильтра
        /// </summary>
        [DisplayName("Аналитик")]
        [Description("ФИО Аналитика")] 
        public string Analyst { get; set; }

        /// <summary>
        /// Возвращает или задает название фильтра
        /// </summary>
        [DisplayName("Название")]
        [Description("Название фильтра")] 
        public string Name { get; set; }

        /// <summary>
        /// Возвращает или задает фильтр
        /// </summary>
        public SelectionFilter Filter { get; set; }

        /// <summary>
        /// Возвращает или задает фильтр второй версии
        /// </summary>
        public Filter FilterVersionTwo { get; set; }

        /// <summary>
        /// Возвращает или задает версию фильтра
        /// </summary>
        public SelectionFilterVersion FilterVersion { get; set; }

        # endregion

        # region Методы

        public bool IsNew()
        {
            return Id == default(long);
        }

        # endregion
    }
}
