﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Selection
{
    [Serializable]
    public class AutoselectionInspectionPvpLimit
    {
        public string RegionCode
        {
            get;
            set;
        }

        public string RegionName
        {
            get;
            set;
        }

        public string InspectionCode
        {
            get;
            set;
        }

        public string InspectionName
        {
            get;
            set;
        }

        public decimal Pvp
        {
            get;
            set;
        }
    }
}
