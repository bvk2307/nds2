﻿namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Selection
{
    public enum DiscrepancyIncludeStatus
    {
        Success,
        Restricted
    }
}
