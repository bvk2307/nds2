﻿//using System;
//using System.ComponentModel;
//using Luxoft.NDS2.Common.Contracts.CustomAttributes;
//
//namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Selection
//{
//    [Serializable]
//    public class Declaration
//    {
//        public long Id { get; set; }
//
//        public bool IS_IN_PROCESS { get; set; }
//
//        [DisplayName("Налогоплательщик: ИНН")]
//        [Description("ИНН налогоплательщика первичной отработки")]
//        public string TaxPayerInn { get; set; }
//
//        [DisplayName("КПП НП")]
//        [Description("КПП НП")]
//        public string TaxPayerKpp { get; set; }
//
//        [DisplayName("Налогоплательщик: Наименование")]
//        [Description("Наименование налогоплательщика первичной отработки")]
//        public string TaxPayerName { get; set; }
//
//        [DisplayName("Регион")]
//        [Description("Код и наименование региона")]
//        public string Region { get; set; }
//
//        [DisplayName("Регион НП(Код)")]
//        [Description("Регион НП(Код)")]
//        public string RegionCode { get; set; }
//
//        [DisplayName("Инспекция")]
//        [Description("Код и наименование инспекции")]
//        public string TaxOrgan { get; set; }
//
//        [DisplayName("Инспекция НП(Код)")]
//        [Description("Инспекция НП(Код)")]
//        public string TaxOrganCode { get; set; }
//
//        [DisplayName("Крупнейший")]
//        [Description("Признак крупнейшего налогоплательщика")]
//        public string TaxPayerCategory { get; set; }
//
//        [DisplayName("Категория НП(код)")]
//        [Description("Категория НП(код)")]
//        public string TaxPayerCategoryCode { get; set; }
//
//        [DisplayName("Отчетный период")]
//        [Description("Период за который подается  декларация")]
//        public string TaxPeriod { get; set; }
//
//        [DisplayName("Отчетный период(код)")]
//        [Description("Отчетный период(код)")]
//        public string TaxPeriodCode { get; set; }
//
//        [DisplayName("Признак НД")]
//        [Description("Признак декларации")]
//        public string Sign { get; set; }
//
//        [DisplayName("Признак НД(Код)")]
//        [Description("Признак НД(Код)")]
//        public string SignCode { get; set; }
//
//        [DisplayName("Номер корректировки")]
//        [Description("Номер корректировки декларации")]
//        public string CorrentionNumber { get; set; }
//
//        [DisplayName("Количество расхождений")]
//        [Description("Количество расхождений")]
//        public int TotalMismatchesCount { get; set; }
//
//        [DisplayName("Сумма расхождений: Общая")]
//        [CurrencyFormat]
//        [Description("Общая сумма расхождений по декларации")]
//        public decimal TotalMismatchesAmount { get; set; }
//
//        [DisplayName("Сумма расхождения: Минимальная")]
//        [CurrencyFormat]
//        [Description("Минимальная сумма расхождения по декларации")]
//        public decimal TotalMismatchesAmountMin { get; set; }
//
//        [DisplayName("Сумма расхождения:  Максимальная")]
//        [CurrencyFormat]
//        [Description("Максимальная сумма расхождения по декларации")]
//        public decimal TotalMismatchesAmountMax { get; set; }
//
//        [DisplayName("Сумма расхождения: Средняя")]
//        [CurrencyFormat]
//        [Description("Средняя сумма расхождения = Общая сумма / кол-во расхождений")]
//        public decimal TotalMismatchesAmountAvg { get; set; }
//    }
//}
