﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Selection
{
    [Serializable]
    public class SelectionCommandParam
    {
        public string LockKey { get; set; }
        public string Comment { get; set; }
        public string Analytic { get; set; }
        public string AnalyticSid { get; set; }

        public static SelectionCommandParam Empty
        {
            get
            {
                return new SelectionCommandParam
                {
                    Comment = string.Empty,
                    Analytic = string.Empty,
                    AnalyticSid = string.Empty,
                    LockKey = string.Empty
                };
            }
        }
    }
}
