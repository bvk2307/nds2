﻿using System;
using System.ComponentModel;
using Luxoft.NDS2.Common.Contracts.CustomAttributes;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Selection
{
    [Serializable]
    public class Selection : BusinessObject
    {
        public bool IsNew()
        {
            return Id < 1;
        }

        [DisplayName("Номер")]
        [Description("Номер выборки")]   
        public long Id { set; get; }

        public long? TemplateId { get; set; }

        public long InvoiceRequestId { get; set; }

        [DisplayName("Тип")]
        [Description("Тип выборки")]
        public string Type { get; set; }

        public SelectionType TypeCode { get; set; }

        [DisplayName("Название")]
        [Description("Название выборки")]   
        public string Name { set; get; }

        [DisplayName("Дата создания")]
        [Description("Дата создания выборки")]
        public DateTime? CreationDate { set; get; }

        [DisplayName("Дата изменения")]
        [Description("Дата изменения выборки")]
        public DateTime? ModificationDate { set; get; }


        [DisplayName("Дата статуса")]
        [Description("Дата изменения статуса")]
        public DateTime? StatusDate { set; get; }

        [DisplayName("Статус")]
        [Description("Статус выборки")]
        public SelectionStatus? Status { get; set; }

        [DisplayName("Общая сумма расхождений")]
        [Description("Сумма расхождений по всем записям в выборке")]
        [CurrencyFormat]
        public decimal? TotalAmount { set; get; }

        [DisplayName("Количество НД")]
        [Description("Количество НД в выборке")]
        public int DeclarationCount { set; get; }

        [DisplayName("Количество расхождений")]
        [Description("Количество отобранных расхождений")]
        public int DiscrepanciesCount { set; get; }

        [DisplayName("Аналитик")]
        [Description("ФИО аналитика")]
        public string Analytic { set; get; }

        [DisplayName("Аналитик SID")]
        [Description("SID аналитика")]
        public string AnalyticSid { set; get; }

        [DisplayName("Согласующий")]
        [Description("ФИО согласующего")]
        public string Manager { set; get; }

        [DisplayName("Согласующий SID")]
        [Description("SID согласующего")]
        public string ManagerSid { set; get; }
        

        public string Regions { get; set; }
        /// <summary>
        /// Возвращает или задает фильтр
        /// </summary>
        public Filter Filter { get; set; }

        /// <summary>
        /// Возвращает или задает фильтр второй версии
        /// </summary>
        public Filter FilterVersionTwo { get; set; }

        public static Selection New()
        {
            return new Selection() { Filter = new Filter()};
        }

        public static Selection New(string name, string analystName, string analystSid, SelectionType type)
        {
            return new Selection
            {
                Filter = new Filter(),
                TypeCode = type,
                Type = type == SelectionType.Hand ? "Ручная" :
                (type == SelectionType.Template ? "Шаблон" : "Выборка по шаблону"),
                Analytic = type != SelectionType.Template ? analystName : String.Empty,
                AnalyticSid = type != SelectionType.Template ? analystSid : string.Empty,
                Manager = type == SelectionType.Template ? analystName : string.Empty,
                ManagerSid = type == SelectionType.Template ? analystSid :string.Empty,
                Name = name,
                CreationDate = DateTime.Now
            };
        }
        
    }
}
