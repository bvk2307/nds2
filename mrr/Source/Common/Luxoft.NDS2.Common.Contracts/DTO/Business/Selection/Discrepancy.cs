﻿using System;
using System.ComponentModel;
using Luxoft.NDS2.Common.Contracts.CustomAttributes;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Selection
{
    [Serializable]
    public class Discrepancy
    {
        public bool IsChecked { get; set; }

        [DisplayName("Номер")]
        [Description("Номер расхождения")]
        public long DiscrepancyId { get; set; }

        public long Declaration_Version_Id { get; set; }

        public long Contr_Declaration_Version_Id { get; set; }

        [DisplayName("Дата выявления")]
        [Description("Дата выявления расхождения")]
        public DateTime? FoundDate { get; set; }

        #region лицо проверяемой декларации

        [DisplayName("ИНН")]
        [Description("ИНН НП первичной отработки расхождения")]
        public string TaxPayerInn { get; set; }

        [DisplayName("КПП")]
        [Description("КПП НП первичной отработки расхождения")]
        public string TaxPayerKpp { get; set; }

        [DisplayName("Наименование")]
        [Description("Наименование НП первичной отработки расхождения")]
        public string TaxPayerName { get; set; }

        [DisplayName("Инспекция")]
        [Description("Код и наименование инспекции")]
        public string TaxPayerSoun { get; set; }

        [DisplayName("Регион")]
        [Description("Код и наименование региона")]
        public string TaxPayerRegion { get; set; }

        [DisplayName("Отчетный период")]
        [Description("Период, за который подается  декларация")]
        public string TaxPayerPeriod { get; set; }

        [DisplayName("Номер корректировки")]
        [Description("Номер корректировки декларации")]
        public string CorrectionNumber { get; set; }

        [DisplayName("Источник")]
        [Description("Источник записи о СФ")]
        public string TaxPayerSource { get; set; }

        [DisplayName("Признак НД")]
        [Description("Признак декларации")]
        public string TaxPayerDeclSign { get; set; }

        [DisplayName("Крупнейший")]
        [Description("НП является крупнейшим")]
        public bool TaxPayerCategory { get; set; }

        public string TaxPayerPeriodCode { get; set; }
        public string TaxPayerYearCode { get; set; }

        #endregion лицо проверяемой декларации

        #region контрагент
        [DisplayName("СФ контрагента")]
        [Description("СФ контрагента")]
        public string Contractor_Invoice_Id { get; set; }
        
        [DisplayName("ИНН")]
        [Description("ИНН НП с которым связано расхождение первичной отработки")]
        public string ContractorInn { get; set; }

        [DisplayName("КПП")]
        [Description("КПП НП с которым связано расхождение первичной отработки")]
        public string ContractorKpp { get; set; }

        [DisplayName("Наименование")]
        [Description("Наименование НП с которым связано расхождение первичной отработки")]
        public string ContractorName { get; set; }

        [DisplayName("Инспекция")]
        [Description("Код и наименование инспекции")]
        public string ContractorSoun { get; set; }

        [DisplayName("Регион")]
        [Description("Код и наименование региона")]
        public string ContractorRegion { get; set; }

        [DisplayName("Отчетный период")]
        [Description("Период, за который подается  декларация")]
        public string ContractorPeriod { get; set; }

        [DisplayName("Номер корректировки")]
        [Description("Номер корректировки декларации")]
        public string ContractorCorrectionNumber { get; set; }

        [DisplayName("Источник")]
        [Description("Источник записи о СФ")]
        public string ContractorSource { get; set; }

        [DisplayName("Признак НД")]
        [Description("Признак декларации")]
        public string ContractorDeclSign { get; set; }

        [DisplayName("Крупнейший")]
        [Description("Признак крупнейшего налогоплательщика")]
        public bool ContractorCategory { get; set; }

        public string ContractorPeriodCode { get; set; }
        public string ContractorYearCode { get; set; }

        [DisplayName("Тип документа")]
        [Description("НД по НДС или Журнал учета")]
        public string ContractorDocType { get; set; }

        #endregion контрагент

        [DisplayName("Вид расхождения")]
        [Description("Вид расхождения")]
        public DictionaryEntry TypeDict 
        { 
            get
            {
                if (null == _type)
                {
                    _type = new DictionaryEntry() { EntryId = TypeCode, EntryValue = Type };
                }
                return _type;
            }
            set
            {
                _type = value;
            }
        }
        private DictionaryEntry _type;
        public int TypeCode { get; set; }
        [DisplayName("Вид расхождения")]
        [Description("Вид расхождения")]
        public string Type { get; set; }


        [DisplayName("Сумма расхождения (руб.)")]
        [Description("Сумма расхождения в руб.")]
        [CurrencyFormat]
        public decimal Amount { get; set; }

        [DisplayName("Статус")]
        [Description("Статус расхождения")]
        public string Status { get; set; }

        [DisplayName("СУР")]
        [Description("СУР НП первичной отработки расхождения")]
        public int SURCode { get; set; }

        [DisplayName("СУР")]
        [Description("СУР НП первичной отработки расхождения")]
        public string SUR_DESCRIPTION { get; set; }

        [DisplayName("СУР")]
        [Description("СУР НП второй стороны отработки расхождения")]
        public int ContractorSur { get; set; }

        [DisplayName("СУР")]
        [Description("СУР НП второй стороны отработки расхождения")]
        public string ContractorSurDescription { get; set; }

        public DiscrepancyStatus StatusCode { get; set; }

        [DisplayName("Наименование этапа")]
        [Description("Этап отработки расхождения")]
        public string STAGE_NAME { get; set; }

        public int StageStatusCode { get; set; }

        [DisplayName("Статус этапа")]
        [Description("Статус этапа отработки расхождения")]
        public string STAGE_STATUS_NAME { get; set; }


        [DisplayName("Сумма НДС")]
        [Description("Сумма налога, исчисленная к «+» уплате / «-» возмещению в бюджет")]
        public decimal SUM_NDS { get; set; }

        [DisplayName("Статус КНП")]
        [Description("Статус декларации")]
        public string STATUS_KNP { get; set; }

        [DisplayName("Код правила сопоставления")]
        [Description("Код правила сопоставления")]
        public int? COMPARE_RULE_CODE { get; set; }

        [DisplayName("Правило сопоставления")]
        [Description("Правило сопоставления")]
        public string COMPARE_RULE { get; set; }

        public bool CanBeSelected
        {
            get; set;
        }

        public override int GetHashCode()
        {
            return DiscrepancyId == 0 ? base.GetHashCode() : DiscrepancyId.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var anotherDiscrepancy = obj as Discrepancy;

            if (anotherDiscrepancy == null)
            {
                return false;
            }

            if (anotherDiscrepancy.DiscrepancyId == 0 && DiscrepancyId == 0)
            {
                return base.Equals(anotherDiscrepancy);
            }

            return anotherDiscrepancy.DiscrepancyId == DiscrepancyId;
        }
    }
}
