﻿namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Selection
{
    public enum DiscrepancyStage
    {
        Selection = 1,
        AutoClaim1 = 2,
        AutoClaim2 = 3,
        AutoOverClaim1 = 4,
        AutoOverClaim2 = 5,
        OtherMNK = 6
    }
}
