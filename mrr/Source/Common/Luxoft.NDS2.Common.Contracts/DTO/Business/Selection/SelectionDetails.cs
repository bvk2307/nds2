﻿using Luxoft.NDS2.Common.Contracts.AccessRestriction.Selections;
using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Selection
{
    [Serializable]
    public class SelectionDetails
    {
        public Selection Data
        {
            get;
            set;
        }

        public string[] Lockers
        {
            get;
            set;
        }

        public SelectionOperation[] ApplicableOperations
        {
            get;
            set;
        }

        public FilterCriteria[] FilterCriterias
        {
            get;
            set;
        }
    }
}
