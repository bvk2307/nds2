﻿//using Luxoft.NDS2.Common.Contracts.DTO.Business;
//using System;
//using System.Collections.Generic;
//using System.ComponentModel;
//using System.Linq;
//using System.Text;
//
//namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Results
//{
//    [Serializable]
//    public class ResultDataLine : BusinessObject
//    {
//        [DisplayName("№ п/п")]
//        [Description("№ п/п")]
//        [DefaultValue(1)]
//        public int Id { get; set; }
//
//        [DisplayName("Наименование")]
//        [Description("Наименование выборки")]
//        public string Name { get; set; }
//
//        [DisplayName("Дата создания")]
//        [Description("Дата создания выборки")]
//        [DefaultValue("01.01.2000")]
//        public DateTime DateStatusChanged { get; set; }
//
//        [DisplayName("Руководитель")]
//        [Description("Руководитель")]
//        public string Manager { get; set; }
//
//        [DisplayName("Отправитель")]
//        [Description("Отправитель")]
//        public string Sender { get; set; }
//
//        [DisplayName("Статус")]
//        [Description("Статус выборки")]
//        public string Status { get; set; }
//
//        [DisplayName("Кол-во записей в выборке")]
//        [Description("Количество записей с установленным флагом выбора")]
//        public uint DifferenceCount { get; set; }
//
//        [DisplayName("Кол-во несоответствий")]
//        [Description("Количество несоответствий по всем записям с установленным флагом выбора")]
//        public uint DiscrepencyCount { get; set; }
//
//        [DisplayName("НП")]
//        [Description("Количество налогоплательщиков")]
//        public uint TaxPayersNumber { get; set; }
//
//        [DisplayName("Количество инспекций")]
//        [Description("Количество инспекций")]
//        public uint InspectionsNumber { get; set; }
//    }
//}
