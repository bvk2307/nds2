﻿using System;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace.Enums;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Selection
{
    /// <summary>
    /// Контракт данных расхождений в выборке
    /// </summary>
    [Serializable]
    public class SelectionDiscrepancy
    {
        /// <summary>
        /// Идентификатор выборки
        /// </summary>
        public long SelectionId { get; set; }

        /// <summary>
        /// Идентификатор корректировки декларации
        /// </summary>
        public long Zip { get; set; }

        /// <summary>
        /// Признак включения расхождения в выборку
        /// </summary>
        public int Included { get; set; }

        /// <summary>
        /// Дата выявления расхождения
        /// </summary>
        public DateTime? FoundDate { get; set; }

        /// <summary>
        /// Номер СФ
        /// </summary>
        public string CreatedByInvoiceNumber { get; set; }

        /// <summary>
        /// Дата СФ
        /// </summary>
        public DateTime? CreatedByInvoiceDate { get; set; }

        /// <summary>
        /// Вид расхождения
        /// </summary>
        public int DiscrepancyType { get; set; }

        /// <summary>
        /// Код подвида расхождения
        /// </summary>
        public int Subtype { get; set; }

        /// <summary>
        /// Правило сопоставления
        /// </summary>
        public string CompareRule { get; set; }

        /// <summary>
        /// Сумма расхождения, руб.
        /// </summary>
        public decimal? Amount { get; set; }

        /// <summary>
        /// Идентификатор расхождения
        /// </summary>
        public long DiscrepancyId { get; set; }

        /// <summary>
        /// Признак стороны первичной отработки
        /// </summary>
        public int SidePrimaryProcessing { get; set; }

        /// <summary>
        /// Код региона покупателя
        /// </summary>
        public string BuyerRegionCode { get; set; }

        /// <summary>
        /// Наименование региона покупателя
        /// </summary>
        public string BuyerRegionName { get; set; }

        /// <summary>
        /// Код налогового органа покупателя
        /// </summary>
        public string BuyerSonoCode { get; set; }

        /// <summary>
        /// Наименование налогового органа покупателя
        /// </summary>
        public string BuyerSonoName { get; set; }

        /// <summary>
        /// ИНН покупателя
        /// </summary>
        public string BuyerInn { get; set; }
        public string BuyerInnDeclarant { get; set; }

        /// <summary>
        /// КПП покупателя
        /// </summary>
        public string BuyerKpp { get; set; }
        public string BuyerKppDeclarant { get; set; }

        /// <summary>
        /// Признак равенства левых частей КС 1.28 и 1.33 в НД покупателя
        /// </summary>
        public int BuyerControlRatio { get; set; }

        /// <summary>
        /// Признак НД продавца
        /// </summary>
        public int? BuyerDeclarationSign { get; set; }

        /// <summary>
        /// Дата подачи декларации продавцом
        /// </summary>
        public DateTime? BuyerDeclarationSubmitDate { get; set; }

        /// <summary>
        /// Сумма НДС по декларации продавца
        /// </summary>
        public decimal? BuyerDeclarationNdsAmount { get; set; }

        /// <summary>
        /// Период подачи НД покупателем 
        /// </summary>
        public string BuyerDeclarationPeriod { get; set; }

        public string BuyerDeclarationPeriodId { get; set; }
        /// <summary>
        /// Код операции покупателя
        /// </summary>
        public string BuyerOperation { get; set; }

        /// <summary>
        /// Источник записи о сф по стороне покупателя (раздел НД 8-12)
        /// </summary>
        public int BuyerChapter { get; set; }

        /// <summary>
        /// СУР покупателя
        /// </summary>
        public int? BuyerSur { get; set; }

        /// <summary>
        /// ИНН продавец
        /// </summary>
        public string SellerInn { get; set; }
        public string SellerInnDeclarant { get; set; }

        /// <summary>
        /// КПП продавца
        /// </summary>
        public string SellerKpp { get; set; }
        public string SellerKppDeclarant { get; set; }

        /// <summary>
        /// Признак наличия по КС 1.27 у продавца
        /// </summary>
        public int SellerControlRatio { get; set; }

        /// <summary>
        /// Признак НД декларации продавца
        /// </summary>
        public int? SellerDeclarationSign { get; set; }

        /// <summary>
        /// Дата подачи декларации продавцом
        /// </summary>
        public DateTime? SellerDeclarationSubmitDate { get; set; }

        /// <summary>
        /// Сумма НДС по декларации продавца
        /// </summary>
        public decimal? SellerDeclarationNdsAmount { get; set; }

        /// <summary>
        /// Период декларации продавца
        /// </summary>
        public string SellerDeclarationPeriod { get; set; }

        public string SellerDeclarationPeriodId { get; set; }
        /// <summary>
        /// Источник записи о сф продавца
        /// </summary>
        public int SellerChapter { get; set; }

        /// <summary>
        /// СУР код продавца
        /// </summary>
        public int? SellerSur { get; set; }

        /// <summary>
        /// Код региона продавца
        /// </summary>
        public string SellerRegionCode { get; set; }
 
        /// <summary>
        /// Наименование региона покупателя
        /// </summary>
        public string SellerRegionName { get; set; }
 
        /// <summary>
        /// Код налогового органа продавца
        /// </summary>
        public string SellerSonoCode { get; set; }
 
        /// <summary>
        /// Наименование налогового органа продавца
        /// </summary>
        public string SellerSonoName { get; set; }

        public int ExcludeFromClaimType { get; set; }

        public ulong? OperationCodesBit { get; set; }

    }
}
