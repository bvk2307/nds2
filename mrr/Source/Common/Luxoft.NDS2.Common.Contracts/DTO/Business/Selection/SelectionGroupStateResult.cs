﻿using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Selection
{
    [Serializable]
    public class SelectionGroupStateResult
    {
        public SelectionGroupState SelectionGroupState { get; set; }
        public long DiscrepancyInWorkedCount { get; set; }
    }

    [Serializable]
    public class SelectionStatusExtended
    {
        public SelectionStatus SelectionStatus { get; set; }
        public RequestStatusType RequestStatusType { get; set; }
    }
}
