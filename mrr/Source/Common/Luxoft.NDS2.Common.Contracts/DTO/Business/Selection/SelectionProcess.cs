﻿using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace.Enums;
using System;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Selection
{
    [Serializable]
    public class SelectionProcess
    {
        public long Id { set; get; }
        public DateTime? StatusDate { set; get; }
        [Obsolete("Необходимо использовать FilterManual")]
        public SelectionFilter FilterManualVersionOne { get; set; }
        public Filter FilterManual { get; set; }
        public SelectionFilterVersion FilterManualVersion { get; set; }
        public SelectionFilterVersion FilterAutoIncludeVersion { get; set; }
        public SelectionFilterVersion FilterAutoExcludeVersion { get; set; }
        public List<GroupFilter> IncludeFilterAutoVersionOne { get; set; }
        public List<GroupFilter> ExcludeFilterAutoVersionOne { get; set; }
        public Filter IncludeFilterAuto { get; set; }
        public Filter ExcludeFilterAuto { get; set; }
        public bool IsManual { get; set; }
        public SelectionStatus Status { get; set; }
        public string FilterManualContent { get; set; }
    }
}
