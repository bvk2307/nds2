﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Selection
{
    [Serializable]
    public class DictionaryEntry
    {
        public int EntryId { get; set; }
        public string EntryValue { get; set; }

        public override string ToString()
        {
            return EntryValue;
        }
    }
}
