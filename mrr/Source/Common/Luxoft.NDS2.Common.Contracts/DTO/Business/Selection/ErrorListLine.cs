﻿using Luxoft.NDS2.Common.Contracts.DTO.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Results
{
    [Serializable]
    public class ErrorListLine : BusinessObject
    {
        public static ErrorListLine Create(string grp, string src, string text, int c)
        {
            return new ErrorListLine() { Group = grp, Source = src, ErrorText = text, code = c };
        }

        [DisplayName("Группа контроля")]
        public string Group { get; set; }

        [DisplayName("Источник")]
        public string Source { get; set; }

        [DisplayName("Ошибка")]
        public string ErrorText { get; set; }

        private int code;
        public int GetCode() { return code; }
    }
}
