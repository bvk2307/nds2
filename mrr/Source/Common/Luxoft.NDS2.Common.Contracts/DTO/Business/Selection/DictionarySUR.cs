﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Selection
{
    [Serializable]
    public sealed class DictionarySUR
    {
        private static readonly Dictionary<int, Image> _icons;

        public int Code { get; set; }

        public string Description { get; set; }

        public int ColorARGB { get; set; }

        static DictionarySUR()
        {
            _icons = new Dictionary<int, Image>();
        }

        public Image GetIcon(Control owner)
        {
            if (!_icons.ContainsKey(ColorARGB))
            {
                lock (_icons)
                {
                    if (!_icons.ContainsKey(ColorARGB))
                    {
                        _icons[ColorARGB] = DrawIcon(owner);
                    }
                }
            }
            return _icons[ColorARGB];
        }

        private Image DrawIcon(Control owner)
        {
            using (var stream = new MemoryStream())
            using (var hdc = owner.CreateGraphics())
            {
                var rectangle = new Rectangle(0, 0, 16, 16);
                var emf = new Metafile(stream, hdc.GetHdc(), rectangle, MetafileFrameUnit.Pixel, EmfType.EmfPlusOnly);
                using (var graphics = Graphics.FromImage(emf))
                using (var brush = new SolidBrush(Color.FromArgb(ColorARGB)))
                {
                    graphics.FillRectangle(Brushes.Transparent, rectangle);
                    rectangle = new Rectangle(0, 0, 15, 15);
                    graphics.FillEllipse(brush, rectangle);
                    graphics.DrawEllipse(Pens.DarkSlateGray, rectangle);
                }
                return emf;
            }
        }
    }
}