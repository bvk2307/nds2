﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Selection
{
    [Serializable]
    public class SelectionTransition
    {
        public long ID { get; set; }
        public string OPERATION { get; set; }
        public SelectionStatus STATE_FROM { get; set; }
        public SelectionStatus STATE_TO { get; set; }
    }
}
