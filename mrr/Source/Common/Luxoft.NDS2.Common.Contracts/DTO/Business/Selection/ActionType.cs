﻿namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Selection
{
    public enum ActionType
    {
        /// <summary>
        /// Создание
        /// </summary>
        Create = 1,

        /// <summary>
        /// Редактирование
        /// </summary>
        Edit = 2,

        /// <summary>
        /// Повторное примение филтра
        /// </summary>
        SecondaryApply = 3,

        /// <summary>
        /// Отправка на согласование
        /// </summary>
        RequestForApproval = 4,

        /// <summary>
        /// Согласование
        /// </summary>
        Approval = 5,

        /// <summary>
        /// Возврат на доработку
        /// </summary>
        ReturnedForCorrection = 6,

        /// <summary>
        /// формирование АТ
        /// </summary>
        ClaimCreate = 7
    }
}
