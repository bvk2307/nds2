﻿using System;
using Luxoft.NDS2.Common.Contracts.CustomAttributes;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy
{
    public enum DiscrepancyInvoiceMultipleDataKind
    {
        Seller = 1,
        Buuyer = 2,
        OperationCode = 3,
        BuyAcceptDate = 4,
        PaymentDocuments = 5,
    }

    [Serializable]
    public sealed class DiscrepancyInvoiceMultipleData
    {
        public int Kind { get; set; }
        public string RowKey { get; set; }
        public string Value1 { get; set; }
        public string Value2 { get; set; }
        public string Value3 { get; set; }
    }
}