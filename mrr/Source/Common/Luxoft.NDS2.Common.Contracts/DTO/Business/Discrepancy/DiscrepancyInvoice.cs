﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy
{
    [Serializable]
    public class DiscrepancyInvoice
    {
        /// <summary>Код валюты</summary>
        public string CodeCurrency { get; set; }

        /// <summary>Стоимость продаж в валюте</summary>
        public decimal? SalesCostInCurrency { get; set; }

        /// <summary>Стоимость продаж в рублях</summary>
        public decimal? SalesCostInRuble { get; set; }
    }
}
