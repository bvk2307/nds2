﻿using System;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy.DiscrepancyTable
{
    [Serializable]
    public sealed class AggregateRecievedInvoiceJournal : DetailsRecievedInvoiceJournal, ICloneable<DetailsRecievedInvoiceJournal>
    {
        public List<DetailsRecievedInvoiceJournal> RecievedInvoiceJournalDetails { get; set; }

        public bool Commented { get; set; }

        public AggregateRecievedInvoiceJournal()
        {
            RecievedInvoiceJournalDetails = new List<DetailsRecievedInvoiceJournal>();
        }

        public DetailsRecievedInvoiceJournal ToDetails()
        {
            return new DetailsRecievedInvoiceJournal
            {
                RowKey = this.RowKey,
                OrdinalNumber = this.OrdinalNumber,
                CorrectedInvoiceDate = this.CorrectedInvoiceDate,
                CorrectedInvoiceNumber = this.CorrectedInvoiceNumber,
                CorrectionNumber = this.CorrectionNumber,
                CurrencyCode = this.CurrencyCode,
                ErrorCodesString = this.ErrorCodesString,
                FixedCorrectedInvoiceDate = this.FixedCorrectedInvoiceDate,
                FixedCorrectedInvoiceNumber = this.FixedCorrectedInvoiceNumber,
                FixedInvoiceDate = this.FixedInvoiceDate,
                FixedInvoiceNumber = this.FixedInvoiceNumber,
                InvoiceDate = this.InvoiceDate,
                InvoiceNumber = this.InvoiceNumber,
                OperationCode = this.OperationCode,
                ReportPeriod = this.ReportPeriod,
                RecieveDate = this.RecieveDate,
                Amount = this.Amount,
                CostDifferenceDecrease = this.CostDifferenceDecrease,
                CostDifferenceIncrease = this.CostDifferenceIncrease,
                TaxAmount = this.TaxAmount,
                TaxDifferenceDecrease = this.TaxDifferenceDecrease,
                TaxDifferenceIncrease = this.TaxDifferenceIncrease,
                SellerInfoINN = this.SellerInfoINN,
                SellerInfoKPP = this.SellerInfoKPP,
                SellerInfoName = this.SellerInfoName,
                SubcommissionAgentINN = this.SubcommissionAgentINN,
                SubcommissionAgentKPP = this.SubcommissionAgentKPP,
                SubcommissionAgentName = this.SubcommissionAgentName
            };
        }

        DetailsBase IAggreagate.ToDetails()
        {
            return ToDetails();
        }
    }
}