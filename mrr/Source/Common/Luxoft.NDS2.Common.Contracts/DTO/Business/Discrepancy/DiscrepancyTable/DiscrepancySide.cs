﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Common.Contracts.CustomAttributes;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy.DiscrepancyTable
{
    [Serializable]
    public sealed class DiscrepancySide
    {
        /// <summary>
        /// Ссылка на декларацию
        /// </summary>
        public long DeclarationId { get; set; }        

        /// <summary>
        /// Сторона расхождения
        /// </summary>
        public string SideName { get; set; }

        /// <summary>
        /// Сторона 1: ИНН
        /// [ИНН НП]
        /// </summary>
        public string Side1INN { get; set; }

        /// <summary>
        /// Сторона 1: КПП
        /// [КПП НП]
        /// </summary>
        public string Side1KPP { get; set; }

        /// <summary>
        /// Сторона 1: Наименование
        /// [Наименование НП]
        /// </summary>
        public string Side1Name { get; set; }

        /// <summary>
        /// Сторона 2: ИНН
        /// [ИНН НП]
        /// </summary>
        public string Side2INN { get; set; }

        /// <summary>
        /// Сторона 2: КПП
        /// [КПП НП]
        /// </summary>
        public string Side2KPP { get; set; }

        /// <summary>
        /// Сторона 2: Наименование
        /// [Наименование НП]
        /// </summary>
        public string Side2Name { get; set; }

        /// <summary>
        /// Сведения стороны расхождения: Номер СФ
        /// [Номер СФ декларации НП, содержащее расхождение]
        /// </summary>
        public string SideInfoInvoiceNumber { get; set; }

        /// <summary>
        /// Сведения стороны расхождения: Дата СФ
        /// [Дата СФ декларации НП, содержащее расхождение]
        /// </summary>
        public DateTime? SideInfoInvoiceDate { get; set; }

        /// <summary>
        /// Сведения стороны расхождения: Сумма по СФ
        /// [Сумма покупки/продажи по СФ с учетом НДС]
        /// </summary>
        [CurrencyFormat]
        public decimal? SideInfoInvoiceAmount { get; set; }

        /// <summary>
        /// Сведения стороны расхождения: Сумма НДС
        /// [Сумма НДС по СФ]
        /// </summary>
        [CurrencyFormat]
        public decimal? SideInfoTaxAmount { get; set; }

        /// <summary>
        /// Сведения стороны расхождения: Инспекция
        /// [Инспекция НП подавшего декларацию]
        /// </summary>
        [IgnoreLoad]
        public CodeValueDictionaryEntry SideInfoInspection
        {
            get
            {
                if (null == _sideInfoInspection)
                {
                    _sideInfoInspection = new CodeValueDictionaryEntry(InspectionCode, InspectionName);
                }
                return _sideInfoInspection;
            }
            set
            {
                _sideInfoInspection = value;
            }
        }
        private CodeValueDictionaryEntry _sideInfoInspection;
        public string InspectionCode { get; set; }
        public string InspectionName { get; set; }

        /// <summary>
        /// Сведения стороны расхождения: Регион
        /// [Регион НП подавшего декларацию]
        /// </summary>
        [IgnoreLoad]
        public CodeValueDictionaryEntry SideInfoRegion
        {
            get
            {
                if (null == _sideInfoRegion)
                {
                    _sideInfoRegion = new CodeValueDictionaryEntry(RegionCode, RegionName);
                }
                return _sideInfoRegion;
            }
            set
            {
                _sideInfoRegion = value;
            }
        }
        private CodeValueDictionaryEntry _sideInfoRegion;
        public string RegionCode { get; set; }
        public string RegionName { get; set; }

        public string InvoiceId { get; set; }
        public int chapter { get; set; }

        /// <summary>
        /// Тип файла по версии Модуля сопоставления
        /// </summary>
        public int MsFileType { get; set; }

        /// <summary>Агрегированные сведения из Книги покупок</summary>
        [IgnoreLoad]
        public List<AggregateBuyBook> BuyBooks { get; set; }

        /// <summary>Агрегированные сведения из Книги продаж</summary>
        [IgnoreLoad]
        public List<AggregateSellBook> SellBooks { get; set; }

        /// <summary>Агрегированные сведения из Журнала выставленных СФ</summary>
        [IgnoreLoad]
        public List<AggregateSentInvoiceJournal> SentInvoiceJournals { get; set; }

        /// <summary>Агрегированные сведения из Журнала полученных СФ</summary>
        [IgnoreLoad]
        public List<AggregateRecievedInvoiceJournal> RecievedInvoiceJournals { get; set; }

        public DiscrepancySide()
        {
            BuyBooks = new List<AggregateBuyBook>();
            SellBooks = new List<AggregateSellBook>();
            SentInvoiceJournals = new List<AggregateSentInvoiceJournal>();
            RecievedInvoiceJournals = new List<AggregateRecievedInvoiceJournal>();
        }
    }
}