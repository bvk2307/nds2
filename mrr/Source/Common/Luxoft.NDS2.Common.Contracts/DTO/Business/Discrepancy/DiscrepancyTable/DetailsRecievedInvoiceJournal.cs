﻿using System;
using Luxoft.NDS2.Common.Contracts.CustomAttributes;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy.DiscrepancyTable
{
    [Serializable]
    public class DetailsRecievedInvoiceJournal : DetailsBase
    {
        public long request_id { get; set; }

        public override int Chapter { get { return 11; } }

        /// <summary>Порядковый номер</summary>
        public long OrdinalNumber { get; set; }

        /// <summary>Дата получения</summary>
        public DateTime? RecieveDate { get; set; }

        /// <summary>Сведения о продавце: ИНН</summary>
        public string SellerInfoINN { get; set; }

        /// <summary>Сведения о продавце: КПП</summary>
        public string SellerInfoKPP { get; set; }

        /// <summary>Сведения о продавце: Наименование</summary>
        public string SellerInfoName { get; set; }

        /// <summary>Сведения о субкомиссионере: ИНН</summary>
        public string SubcommissionAgentINN { get; set; }

        /// <summary>Сведения о субкомиссионере: КПП</summary>
        public string SubcommissionAgentKPP { get; set; }

        /// <summary>Сведения о субкомиссионере: Наименование</summary>
        public string SubcommissionAgentName { get; set; }

        /// <summary>Код вида сделки</summary>
        public int? DealTypeCode { get; set; }
        
        /// <summary>Стоимость товаров</summary>
        [CurrencyFormat]
        public decimal? Amount { get; set; }

        /// <summary>Сумма НДС</summary>
        [CurrencyFormat]
        public decimal? TaxAmount { get; set; }

        /// <summary>Разница стоимости с НДС по КСФ: уменьшение</summary>
        [CurrencyFormat]
        public decimal? CostDifferenceDecrease { get; set; }

        /// <summary>Разница стоимости с НДС по КСФ: увеличение</summary>
        [CurrencyFormat]
        public decimal? CostDifferenceIncrease { get; set; }

        /// <summary>Разница НДС по КСФ: уменьшение</summary>
        [CurrencyFormat]
        public decimal? TaxDifferenceDecrease { get; set; }

        /// <summary>Разница НДС по КСФ: увеличение</summary>
        [CurrencyFormat]
        public decimal? TaxDifferenceIncrease { get; set; }
    }
}