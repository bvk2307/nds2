﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy.DiscrepancyTable
{
    public interface IAggreagate
    {
        bool Commented { get; set; }

        DetailsBase ToDetails();
    }

    public interface ICloneable<out T> : IAggreagate
        where T : DetailsBase
    {
        T ToDetails();
    }
}