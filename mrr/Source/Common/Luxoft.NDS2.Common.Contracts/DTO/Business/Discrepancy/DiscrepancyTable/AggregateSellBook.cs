﻿using System;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.CustomAttributes;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy.DiscrepancyTable
{
    [Serializable]
    public sealed class AggregateSellBook : DetailsSellBook, ICloneable<DetailsSellBook>
    {
        public string NumberTD { get; set; }

        [IgnoreLoad]
        public List<DetailsSellBook> SellBookDetails { get; set; }

        public bool Commented { get; set; }

        public AggregateSellBook()
        {
            SellBookDetails = new List<DetailsSellBook>();
        }

        public DetailsSellBook ToDetails()
        {
            return new DetailsSellBook
            {
                RowKey = this.RowKey,
                OrdinalNumber = this.OrdinalNumber,
                CorrectedInvoiceDate = this.CorrectedInvoiceDate,
                CorrectedInvoiceNumber = this.CorrectedInvoiceNumber,
                CorrectionNumber = this.CorrectionNumber,
                CurrencyCode = this.CurrencyCode,
                ErrorCodesString = this.ErrorCodesString,
                FixedCorrectedInvoiceDate = this.FixedCorrectedInvoiceDate,
                FixedCorrectedInvoiceNumber = this.FixedCorrectedInvoiceNumber,
                FixedInvoiceDate = this.FixedInvoiceDate,
                FixedInvoiceNumber = this.FixedInvoiceNumber,
                InvoiceDate = this.InvoiceDate,
                InvoiceNumber = this.InvoiceNumber,
                OperationCode = this.OperationCode,
                ReportPeriod = this.ReportPeriod,
                PaymentDocDate = this.PaymentDocDate,
                TaxableAmount0 = this.TaxableAmount0,
                TaxableAmount10 = this.TaxableAmount10,
                TaxableAmount18 = this.TaxableAmount18,
                TaxAmount10 = this.TaxAmount10,
                TaxAmount18 = this.TaxAmount18,
                TaxFreeAmount = this.TaxFreeAmount,
                TotalAmountCurrency = this.TotalAmountCurrency,
                TotalAmountRouble = this.TotalAmountRouble,
                NumberTD = this.NumberTD,
                BuyerInfoINN = this.BuyerInfoINN,
                BuyerInfoKPP = this.BuyerInfoKPP,
                BuyerInfoName = this.BuyerInfoName,
                PaymentDocNumber = this.PaymentDocNumber,
                IntermediaryInfoINN = this.IntermediaryInfoINN,
                IntermediaryInfoKPP = this.IntermediaryInfoKPP,
                IntermediaryInfoName = this.IntermediaryInfoName,
                PriznakDopList = this.PriznakDopList
            };
        }

        DetailsBase IAggreagate.ToDetails()
        {
            return ToDetails();
        }
    }
}