﻿using System;
using Luxoft.NDS2.Common.Contracts.CustomAttributes;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy.DiscrepancyTable
{
    [Serializable]
    public class DetailsSellBook : DetailsBase
    {
        public long request_id { get; set; }

        /// <summary>номер раздела</summary>
        public override int Chapter { get { return 9; } }

        /// <summary>Порядковый номер</summary>
        public long OrdinalNumber { get; set; }

        /// <summary>Сведения о покупателе: ИНН</summary>
        public string BuyerInfoINN { get; set; }

        /// <summary>Сведения о покупателе: КПП</summary>
        public string BuyerInfoKPP { get; set; }

        /// <summary>Сведения о покупателе: Наименование</summary>
        public string BuyerInfoName { get; set; }

        /// <summary>Сведения о посреднике: ИНН</summary>
        public string IntermediaryInfoINN { get; set; }

        /// <summary>Сведения о посреднике: КПП</summary>
        public string IntermediaryInfoKPP { get; set; }

        /// <summary>Сведения о посреднике: Наименование</summary>
        public string IntermediaryInfoName { get; set; }

        /// <summary>Документ подтверждающий оплату: Номер</summary>
        public string PaymentDocNumber { get; set; }

        /// <summary>Документ подтверждающий оплату: Дата</summary>
        public DateTime? PaymentDocDate { get; set; }

        /// <summary>Стоимость продаж с НДС: в валюте</summary>
        [CurrencyFormat]
        public decimal? TotalAmountCurrency { get; set; }

        /// <summary>Стоимость продаж с НДС: в руб. и коп.</summary>
        [CurrencyFormat]
        public decimal? TotalAmountRouble { get; set; }

        /// <summary>Стоимость продаж облагаемых налогом (в руб.) без НДС: 18%</summary>
        [CurrencyFormat]
        public decimal? TaxableAmount18 { get; set; }

        /// <summary>Стоимость продаж облагаемых налогом (в руб.) без НДС: 10%</summary>
        [CurrencyFormat]
        public decimal? TaxableAmount10 { get; set; }

        /// <summary>Стоимость продаж облагаемых налогом (в руб.) без НДС: 0%</summary>
        [CurrencyFormat]
        public decimal? TaxableAmount0 { get; set; }

        /// <summary>Сумма НДС: 18%</summary>
        [CurrencyFormat]
        public decimal? TaxAmount18 { get; set; }

        /// <summary>Сумма НДС: 10%</summary>
        [CurrencyFormat]
        public decimal? TaxAmount10 { get; set; }

        /// <summary>Стоимость продаж, освобождаемых он налога</summary>
        [CurrencyFormat]
        public decimal? TaxFreeAmount { get; set; }

        /// <summary>Сведения из дополнительного листа</summary>
        public bool PriznakDopList { get; set; }

        public string NumberTD { get; set; }
    }
}