﻿using System;
using Luxoft.NDS2.Common.Contracts.CustomAttributes;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy.DiscrepancyTable
{
    [Serializable]
    public class DetailsBuyBook : DetailsBase
    {
        public long request_id { get; set; }

        /// <summary>номер раздела</summary>
        public override int Chapter { get { return 8; } }

        /// <summary>Порядковый номер</summary>
        public long OrdinalNumber { get; set; }

        /// <summary>Документ, подтверждающий оплату: Номер</summary>
        public string PaymentDocNumber { get; set; }

        /// <summary>Документ, подтверждающий оплату: Дата</summary>
        public DateTime? PaymentDocDate { get; set; }

        /// <summary>Дата принятия на учет</summary>
        public DateTime? AccountingDate { get; set; }

        /// <summary>Сведения о продавце: ИНН</summary>
        public string SellerInfoINN { get; set; }

        /// <summary>Сведения о продавце: КПП</summary>
        public string SellerInfoKPP { get; set; }

        /// <summary>Сведения о продавце: Наименование</summary>
        public string SellerInfoName { get; set; }

        /// <summary>Сведения о посреднике: ИНН</summary>
        public string IntermediaryInfoINN { get; set; }

        /// <summary>Сведения о посреднике: КПП</summary>
        public string IntermediaryInfoKPP { get; set; }

        /// <summary>Сведения о посреднике: Наименование</summary>
        public string IntermediaryInfoName { get; set; }

        /// <summary>№ ТД</summary>
        public string NumberTD { get; set; }

        /// <summary>Стоимость покупок с НДС</summary>
        [CurrencyFormat]
        public decimal? TotalAmount { get; set; }

        /// <summary>Сумма НДС</summary>
        [CurrencyFormat]
        public decimal? TaxAmount { get; set; }

        /// <summary>Сведения из дополнительного листа</summary>
        public bool PriznakDopList { get; set; }
    }
}