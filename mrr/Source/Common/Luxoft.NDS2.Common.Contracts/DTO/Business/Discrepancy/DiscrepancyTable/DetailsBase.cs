﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Common.Contracts.CustomAttributes;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy.DiscrepancyTable
{
    [Serializable]
    public abstract class DetailsBase
    {
        /// <summary>Идентификатор стета-фактуры</summary>
        public string RowKey { get; set; }

        [IgnoreLoad]
        public abstract int Chapter { get; }

        /// <summary>Отчетный период</summary>
        public string ReportPeriod { get; set; }

        /// <summary>Код вида операции</summary>
        public string OperationCode { get; set; }

        /// <summary>СФ: №</summary>
        public string InvoiceNumber { get; set; }

        /// <summary>СФ: Дата</summary>
        public DateTime? InvoiceDate { get; set; }

        /// <summary>ИСФ: №</summary>
        public string FixedInvoiceNumber { get; set; }

        /// <summary>ИСФ: Дата</summary>
        public DateTime? FixedInvoiceDate { get; set; }

        /// <summary>КСФ: №</summary>
        public string CorrectedInvoiceNumber { get; set; }

        /// <summary>КСФ: Дата</summary>
        public DateTime? CorrectedInvoiceDate { get; set; }

        /// <summary>ИКСФ: №</summary>
        public string FixedCorrectedInvoiceNumber { get; set; }

        /// <summary>ИКСФ: Дата</summary>
        public DateTime? FixedCorrectedInvoiceDate { get; set; }

        /// <summary>Код валюты</summary>
        public string CurrencyCode { get; set; }

        /// <summary>№ коррект.</summary>
        public string CorrectionNumber { get; set; }

        public string ErrorCodesString { get; set; }

        [IgnoreLoad]
        /// <summary>Коды ошибки ЛК</summary>
        public List<int> ErrorCodes
        {
            get
            {
                Func<string, int> parse = s =>
                {
                    int i;
                    return int.TryParse(s, out i) ? i : 0;
                };
                return (ErrorCodesString ?? string.Empty).Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(parse).ToList();
            }
        }

        protected List<T> CopyList<T>(List<T> list)
        {
            return list == null ? null : new List<T>(list);
        }
    }
}