﻿using System;
using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy.DiscrepancyTable
{
    public sealed class InvoiceDiscrepancy : Invoice
    {
        private const string ERROR_CODES_SEPARATOR = ",";

        public long? DESCREPANCY_ID { get; set; }

        public string DECLARAIION_TAX_PERIOD { get; set; }

        public string DECLARAIION_CORRECTION_NUMBER { get; set; }

        public bool COMMENTED { get; set; }

        #region General
        public static InvoiceDiscrepancy FromBaseClass(DetailsBase input)
        {
            return new InvoiceDiscrepancy
            {
                ROW_KEY = input.RowKey,
                CHAPTER = input.Chapter,
                DECLARAIION_TAX_PERIOD = input.ReportPeriod,
                OPERATION_CODE = input.OperationCode,
                INVOICE_NUM = input.InvoiceNumber,
                INVOICE_DATE = input.InvoiceDate,
                CHANGE_NUM = input.FixedInvoiceNumber,
                CHANGE_DATE = input.FixedInvoiceDate,
                CORRECTION_NUM = input.CorrectedInvoiceNumber,
                CORRECTION_DATE = input.CorrectedInvoiceDate,
                CHANGE_CORRECTION_NUM = input.FixedCorrectedInvoiceNumber,
                CHANGE_CORRECTION_DATE = input.FixedCorrectedInvoiceDate,
                OKV_CODE = input.CurrencyCode,
                DECLARAIION_CORRECTION_NUMBER = input.CorrectionNumber,
                LOGICAL_ERRORS = input.ErrorCodesString
            };
        }

        private static void FillBaseClass(InvoiceDiscrepancy source, DetailsBase target)
        {
            if (source.CHAPTER != target.Chapter)
            {
                throw new InvalidCastException();
            }
            target.RowKey = source.ROW_KEY;
            target.ReportPeriod = source.DECLARAIION_TAX_PERIOD;
            target.OperationCode = source.OPERATION_CODE;
            target.InvoiceNumber = source.INVOICE_NUM;
            target.InvoiceDate = source.INVOICE_DATE;
            target.FixedInvoiceNumber = source.CHANGE_NUM;
            target.FixedInvoiceDate = source.CHANGE_DATE;
            target.CorrectedInvoiceNumber = source.CORRECTION_NUM;
            target.CorrectedInvoiceDate = source.CORRECTION_DATE;
            target.FixedCorrectedInvoiceNumber = source.CHANGE_CORRECTION_NUM;
            target.FixedCorrectedInvoiceDate = source.CHANGE_CORRECTION_DATE;
            target.CurrencyCode = source.OKV_CODE;
            target.CorrectionNumber = source.DECLARAIION_CORRECTION_NUMBER;
            target.ErrorCodesString = source.LOGICAL_ERRORS;
        }
        #endregion
        #region BuyBook
        public static explicit operator DetailsBuyBook(InvoiceDiscrepancy input)
        {
            return ((AggregateBuyBook)input).ToDetails();
        }

        public static explicit operator AggregateBuyBook(InvoiceDiscrepancy input)
        {
            var result = new AggregateBuyBook();
            FillBaseClass(input, result);
            result.AccountingDate = input.BUY_ACCEPT_DATE.FirstDate();
            result.SellerInfoINN = input.SELLER_INN;
            result.SellerInfoKPP = input.SELLER_KPP;
            result.SellerInfoName = input.SELLER_NAME;
            result.IntermediaryInfoINN = input.BROKER_INN;
            result.IntermediaryInfoKPP = input.BROKER_KPP;
            result.IntermediaryInfoName = input.BROKER_NAME;
            result.NumberTD = input.CUSTOMS_DECLARATION_NUM;
            result.TotalAmount = input.PRICE_TOTAL;
            result.TaxAmount = input.PRICE_NDS_TOTAL;
            result.Commented = input.COMMENTED;
            return result;
        }

        public static implicit operator InvoiceDiscrepancy(AggregateBuyBook input)
        {
            var result = FillFromBuyBook(input);
            result.COMMENTED = input.Commented;
            return result;
        }

        public static implicit operator InvoiceDiscrepancy(DetailsBuyBook input)
        {
            return FillFromBuyBook(input);
        }

        private static InvoiceDiscrepancy FillFromBuyBook(DetailsBuyBook input)
        {
            var result = FromBaseClass(input);
            result.BUY_ACCEPT_DATE = new DocDates(input.AccountingDate);
            result.SELLER_INN = input.SellerInfoINN;
            result.SELLER_KPP = input.SellerInfoKPP;
            result.SELLER_NAME = input.SellerInfoName;
            result.BROKER_INN = input.IntermediaryInfoINN;
            result.BROKER_KPP = input.IntermediaryInfoKPP;
            result.BROKER_NAME = input.IntermediaryInfoName;
            result.CUSTOMS_DECLARATION_NUM = input.NumberTD;
            result.PRICE_TOTAL = input.TotalAmount.GetValueOrDefault();
            result.PRICE_NDS_TOTAL = input.TaxAmount.GetValueOrDefault();
            return result;
        }
        #endregion
        #region SellBook
        public static explicit operator DetailsSellBook(InvoiceDiscrepancy input)
        {
            return ((AggregateSellBook)input).ToDetails();
        }

        public static explicit operator AggregateSellBook(InvoiceDiscrepancy input)
        {
            var result = new AggregateSellBook();
            FillBaseClass(input, result);
            result.BuyerInfoINN = input.BUYER_INN;
            result.BuyerInfoKPP = input.BUYER_KPP;
            result.BuyerInfoName = input.BUYER_NAME;
            result.IntermediaryInfoINN = input.BROKER_INN;
            result.IntermediaryInfoKPP = input.BROKER_KPP;
            result.IntermediaryInfoName = input.BROKER_NAME;
            result.PaymentDocNumber = input.RECEIPT_DOC_NUM;
            result.PaymentDocDate = input.RECEIPT_DOC_DATE;
            result.TotalAmountCurrency = input.PRICE_SELL_IN_CURR;
            result.TotalAmountRouble = input.PRICE_SELL;
            result.TaxableAmount18 = input.PRICE_SELL_18;
            result.TaxableAmount10 = input.PRICE_SELL_10;
            result.TaxableAmount0 = input.PRICE_SELL_0;
            result.TaxAmount18 = input.PRICE_NDS_18;
            result.TaxAmount10 = input.PRICE_NDS_10;
            result.TaxFreeAmount = input.PRICE_TAX_FREE;
            result.NumberTD = input.CUSTOMS_DECLARATION_NUM;
            return result;
        }

        public static implicit operator InvoiceDiscrepancy(AggregateSellBook input)
        {
            var result = FillFromSellBook(input);
            result.COMMENTED = input.Commented;
            return result;
        }

        public static implicit operator InvoiceDiscrepancy(DetailsSellBook input)
        {
            return FillFromSellBook(input);
        }

        private static InvoiceDiscrepancy FillFromSellBook(DetailsSellBook input)
        {
            var result = FromBaseClass(input);
            result.BUYER_INN = input.BuyerInfoINN;
            result.BUYER_KPP = input.BuyerInfoKPP;
            result.BUYER_NAME = input.BuyerInfoName;
            result.BROKER_INN = input.IntermediaryInfoINN;
            result.BROKER_KPP = input.IntermediaryInfoKPP;
            result.BROKER_NAME = input.IntermediaryInfoName;
            result.RECEIPT_DOC_NUM = input.PaymentDocNumber;
            result.RECEIPT_DOC_DATE = input.PaymentDocDate;
            result.PRICE_SELL_IN_CURR = input.TotalAmountCurrency.GetValueOrDefault();
            result.PRICE_SELL = input.TotalAmountRouble.GetValueOrDefault();
            result.PRICE_SELL_18 = input.TaxableAmount18.GetValueOrDefault();
            result.PRICE_SELL_10 = input.TaxableAmount10.GetValueOrDefault();
            result.PRICE_SELL_0 = input.TaxableAmount0.GetValueOrDefault();
            result.PRICE_NDS_18 = input.TaxAmount18.GetValueOrDefault();
            result.PRICE_NDS_10 = input.TaxAmount10.GetValueOrDefault();
            result.PRICE_TAX_FREE = input.TaxFreeAmount.GetValueOrDefault();
            result.CUSTOMS_DECLARATION_NUM = input.NumberTD;
            return result;
        }
        #endregion
        #region RecievedJournal
        public static explicit operator DetailsRecievedInvoiceJournal(InvoiceDiscrepancy input)
        {
            return ((AggregateRecievedInvoiceJournal)input).ToDetails();
        }

        public static explicit operator AggregateRecievedInvoiceJournal(InvoiceDiscrepancy input)
        {
            throw new NotImplementedException();
        }

        public static implicit operator InvoiceDiscrepancy(AggregateRecievedInvoiceJournal input)
        {
            var result = FillFromRecievedJournal(input);
            result.COMMENTED = input.Commented;
            return result;
        }

        public static implicit operator InvoiceDiscrepancy(DetailsRecievedInvoiceJournal input)
        {
            return FillFromRecievedJournal(input);
        }

        private static InvoiceDiscrepancy FillFromRecievedJournal(DetailsRecievedInvoiceJournal input)
        {
            throw new NotImplementedException();
        }
        #endregion
        #region SentJournal
        public static explicit operator DetailsSentInvoiceJournal(InvoiceDiscrepancy input)
        {
            return ((AggregateSentInvoiceJournal)input).ToDetails();
        }

        public static explicit operator AggregateSentInvoiceJournal(InvoiceDiscrepancy input)
        {
            throw new NotImplementedException();
        }

        public static implicit operator InvoiceDiscrepancy(AggregateSentInvoiceJournal input)
        {
            var result = FillFromSentJournal(input);
            result.COMMENTED = input.Commented;
            return result;
        }

        public static implicit operator InvoiceDiscrepancy(DetailsSentInvoiceJournal input)
        {
            return FillFromSentJournal(input);
        }

        private static InvoiceDiscrepancy FillFromSentJournal(DetailsSentInvoiceJournal input)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}