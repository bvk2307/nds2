﻿using System;
using Luxoft.NDS2.Common.Contracts.CustomAttributes;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy.DiscrepancyTable
{
    [Serializable]
    public class DetailsSentInvoiceJournal : DetailsBase
    {
        public long request_id { get; set; }

        public override int Chapter { get { return 10; } }

        /// <summary>Порядковый номер</summary>
        public long OrdinalNumber { get; set; }

        /// <summary>Дата выставления</summary>
        public DateTime? SendDate { get; set; }

        /// <summary>Сведения о покупателе: ИНН</summary>
        public string BuyerInfoINN { get; set; }

        /// <summary>Сведения о покупателе: КПП</summary>
        public string BuyerInfoKPP { get; set; }

        /// <summary>Сведения о покупателе: Наименование</summary>
        public string BuyerInfoName { get; set; }

        /// <summary>Сведения о продавце: ИНН</summary>
        public string SellerInfoINN { get; set; }

        /// <summary>Сведения о продавце: КПП</summary>
        public string SellerInfoKPP { get; set; }

        /// <summary>Сведения о продавце: Наименование</summary>
        public string SellerInfoName { get; set; }

        /// <summary>Стоимость товаров</summary>
        [CurrencyFormat]
        public decimal? Amount { get; set; }

        /// <summary>Сумма НДС</summary>
        [CurrencyFormat]
        public decimal? TaxAmount { get; set; }

        /// <summary>Разница стоимости с НДС по КСФ: уменьшение</summary>
        [CurrencyFormat]
        public decimal? CostDifferenceDecrease { get; set; }

        /// <summary>Разница стоимости с НДС по КСФ: увеличение</summary>
        [CurrencyFormat]
        public decimal? CostDifferenceIncrease { get; set; }

        /// <summary>Разница НДС по КСФ: уменьшение</summary>
        [CurrencyFormat]
        public decimal? TaxDifferenceDecrease { get; set; }

        /// <summary>Разница НДС по КСФ: увеличение</summary>
        [CurrencyFormat]
        public decimal? TaxDifferenceIncrease { get; set; }
    }
}