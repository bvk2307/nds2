﻿using System;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy.DiscrepancyTable
{
    [Serializable]
    public sealed class AggregateBuyBook : DetailsBuyBook, ICloneable<DetailsBuyBook>
    {
        public List<DetailsBuyBook> BuyBookDetails { get; set; }

        public bool Commented { get; set; }

        public AggregateBuyBook()
        {
            BuyBookDetails = new List<DetailsBuyBook>();
        }

        public DetailsBuyBook ToDetails()
        {
            return new DetailsBuyBook
            {
                RowKey = this.RowKey,
                OrdinalNumber = this.OrdinalNumber,
                CorrectedInvoiceDate = this.CorrectedInvoiceDate,
                CorrectedInvoiceNumber = this.CorrectedInvoiceNumber,
                CorrectionNumber = this.CorrectionNumber,
                CurrencyCode = this.CurrencyCode,
                ErrorCodesString = this.ErrorCodesString,
                FixedCorrectedInvoiceDate = this.FixedCorrectedInvoiceDate,
                FixedCorrectedInvoiceNumber = this.FixedCorrectedInvoiceNumber,
                FixedInvoiceDate = this.FixedInvoiceDate,
                FixedInvoiceNumber = this.FixedInvoiceNumber,
                InvoiceDate = this.InvoiceDate,
                InvoiceNumber = this.InvoiceNumber,
                OperationCode = this.OperationCode,
                ReportPeriod = this.ReportPeriod,
                AccountingDate = this.AccountingDate,
                TaxAmount = this.TaxAmount,
                TotalAmount = this.TotalAmount,
                IntermediaryInfoINN = this.IntermediaryInfoINN,
                IntermediaryInfoKPP = this.IntermediaryInfoKPP,
                IntermediaryInfoName = this.IntermediaryInfoName,
                NumberTD = this.NumberTD,
                SellerInfoINN = this.SellerInfoINN,
                SellerInfoKPP = this.SellerInfoKPP,
                SellerInfoName = this.SellerInfoName,
                PriznakDopList = this.PriznakDopList
            };
        }

        DetailsBase IAggreagate.ToDetails()
        {
            return ToDetails();
        }
    }
}