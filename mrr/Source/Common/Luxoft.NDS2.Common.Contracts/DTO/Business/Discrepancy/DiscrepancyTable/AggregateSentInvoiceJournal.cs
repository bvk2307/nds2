﻿using System;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy.DiscrepancyTable
{
    [Serializable]
    public sealed class AggregateSentInvoiceJournal : DetailsSentInvoiceJournal, ICloneable<DetailsSentInvoiceJournal>
    {
        public List<DetailsSentInvoiceJournal> SentInvoiceJournalDetails { get; set; }

        public bool Commented { get; set; }

        public AggregateSentInvoiceJournal()
        {
            SentInvoiceJournalDetails = new List<DetailsSentInvoiceJournal>();
        }

        public DetailsSentInvoiceJournal ToDetails()
        {
            return new DetailsSentInvoiceJournal
            {
                RowKey = this.RowKey,
                OrdinalNumber = this.OrdinalNumber,
                CorrectedInvoiceDate = this.CorrectedInvoiceDate,
                CorrectedInvoiceNumber = this.CorrectedInvoiceNumber,
                CorrectionNumber = this.CorrectionNumber,
                CurrencyCode = this.CurrencyCode,
                ErrorCodesString = this.ErrorCodesString,
                FixedCorrectedInvoiceDate = this.FixedCorrectedInvoiceDate,
                FixedCorrectedInvoiceNumber = this.FixedCorrectedInvoiceNumber,
                FixedInvoiceDate = this.FixedInvoiceDate,
                FixedInvoiceNumber = this.FixedInvoiceNumber,
                InvoiceDate = this.InvoiceDate,
                InvoiceNumber = this.InvoiceNumber,
                OperationCode = this.OperationCode,
                ReportPeriod = this.ReportPeriod,
                SendDate = this.SendDate,
                Amount = this.Amount,
                CostDifferenceDecrease = this.CostDifferenceDecrease,
                CostDifferenceIncrease = this.CostDifferenceIncrease,
                TaxAmount = this.TaxAmount,
                TaxDifferenceDecrease = this.TaxDifferenceDecrease,
                TaxDifferenceIncrease = this.TaxDifferenceIncrease,
                BuyerInfoINN = this.BuyerInfoINN,
                BuyerInfoKPP = this.BuyerInfoKPP,
                BuyerInfoName = this.BuyerInfoName,
                SellerInfoINN = this.SellerInfoINN,
                SellerInfoKPP = this.SellerInfoKPP,
                SellerInfoName = this.SellerInfoName
            };
        }

        DetailsBase IAggreagate.ToDetails()
        {
            return ToDetails();
        }
    }
}