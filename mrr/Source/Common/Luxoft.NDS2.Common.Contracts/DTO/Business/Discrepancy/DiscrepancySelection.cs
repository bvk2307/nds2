﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy
{
    /// <summary>Выборки</summary>
    [Serializable]
    public sealed class DiscrepancySelection
    {
        /// <summary>Номер [Номер выборки]</summary>
        public string Number { get; set; }

        /// <summary>Название [Название выборки]</summary>
        public string Name { get; set; }

        /// <summary>Тип [Тип выборки]</summary>
        public string Type { get; set; }

        /// <summary>Дата создания [Дата создания выборки]</summary>
        public DateTime? CreationDate { get; set; }

        /// <summary>Дата статуса [Дата изменения статуса]</summary>
        public DateTime? StatusChangeDate { get; set; }

        /// <summary>Статус [Статус выборки]</summary>
        public string Status { get; set; }

        /// <summary>Аналитик [ФИО аналитика]</summary>
        public string Analyst { get; set; }

        /// <summary>Согласующий [ФИО согласующего]</summary>
        public string Approver { get; set; }
    }
}