﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy
{
    /// <summary>История статуса расхождения</summary>
    [Serializable]
    public sealed class DiscrepancyStatusChange
    {
        /// <summary>Статус</summary>
        public string Status { get; set; }

        /// <summary>Дата присвоения статуса</summary>
        public DateTime? StatusChangeDate { get; set; }
    }
}