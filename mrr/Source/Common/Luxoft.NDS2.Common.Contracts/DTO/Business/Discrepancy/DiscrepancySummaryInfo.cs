﻿using System;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.CustomAttributes;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy.DiscrepancyTable;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy
{
    [Serializable]
    public enum DiscrepancyType
    {
        Break = 1,
        InaccurateComparison = 2,
        Currency = 3,
        NDS = 4
    }

    [Serializable]
    public sealed class DiscrepancySummaryInfo
    {
        [IgnoreLoad]
        public long InvoiceRequestId { get; set; }

        public long Id { get; set; }

        public int TypeCode { get; set; }
        
        public string TypeName { get; set; }

        public string TypeNameForCaption { get; set; }

        public string StatusName { get; set; }

        public string WorkSideINN { get; set; }

        public string WorkSideReorgINN { get; set; }

        public string WorkSideKpp { get; set; }

        public int? WorkSideCodeSUR { get; set; }

        public string OtherSideINN { get; set; }

        public DateTime? IssueDate { get; set; }

        public decimal? Amount { get; set; }

        public decimal? AmountPVP { get; set; }

        public DateTime? CloseDate { get; set; }

        public string UserComment { get; set; }

        public decimal? Course { get; set; }

        public decimal? CourseCost { get; set; }

        /// <summary>Номер раздела СФ</summary>
        public int? InvoiceChapter { get; set; }

        /// <summary>Ссылка на счет фактуру</summary>
        public string InvoiceId { get; set; }

        /// <summary>Номер раздела СФ контрагента</summary>
        public int? ContractorInvoiceChapter { get; set; }

        /// <summary>Ссылка на счет фактуру контрагента</summary>
        public string ContractorInvoiceId { get; set; }

        /// <summary>Аттрибуты из СФ</summary>
        [IgnoreLoad]
        public DiscrepancyInvoice DiscrepancyInvoice { get; set; }

        /// <summary>Номер автотребования</summary>
        [IgnoreLoad]
        public long? ClaimId { get; set; }

        /// <summary>Дата формирования автотребования</summary>
        [IgnoreLoad]
        public DateTime? ClaimStatusDate { get; set; }

        /// <summary>Статус автотребования</summary>
        [IgnoreLoad]
        public string ClaimStatus { get; set; }

        /// <summary>Дата расхождения</summary>
        [IgnoreLoad]
        public DateTime? FoundDate { get; set; }

        /// <summary>Дата расчета</summary>
        [IgnoreLoad]
        public DateTime? CalculationDate { get; set; }

        [IgnoreLoad]
        public DiscrepancyType Type
        {
            get { return (DiscrepancyType)TypeCode; }
        }

        [IgnoreLoad]
        public List<DiscrepancySide> Sides { get; set; }

        [IgnoreLoad]
        public List<ErrorMapping> ErrorsDictionary { get; set; }

        //История этапов

        public DiscrepancySummaryInfo()
        {
            ErrorsDictionary = new List<ErrorMapping>();
            Sides = new List<DiscrepancySide>();
        }

        public DiscrepancySummaryInfo(
            DeclarationDiscrepancy declarationDiscrepancy) : this()
        {
            Amount = declarationDiscrepancy.Amount;
            AmountPVP = declarationDiscrepancy.AmountPvp;
            CloseDate = declarationDiscrepancy.CloseDate;
            ContractorInvoiceChapter = declarationDiscrepancy.ContractorInvoiceChapter;
            ContractorInvoiceId = declarationDiscrepancy.ContractorInvoiceKey;
            Id = declarationDiscrepancy.DiscrepancyId;
            InvoiceChapter = declarationDiscrepancy.WorkSideInvoiceChapter;
            InvoiceId = declarationDiscrepancy.TaxPayerInvoiceKey;
            IssueDate = declarationDiscrepancy.FoundDate;
            OtherSideINN = declarationDiscrepancy.OtherSideINN;
            StatusName = declarationDiscrepancy.StatusName;
            TypeCode = declarationDiscrepancy.TypeCode;
            TypeName = declarationDiscrepancy.TypeName;
            TypeNameForCaption = declarationDiscrepancy.TypeNameForCaption;
            UserComment = declarationDiscrepancy.UserComment;
            WorkSideINN = declarationDiscrepancy.TaxPayerInn;
            WorkSideKpp = declarationDiscrepancy.TaxPayerKpp;
            WorkSideReorgINN = declarationDiscrepancy.TaxPayerReorgInn;
        }
    }
}