﻿using System;
using Luxoft.NDS2.Common.Contracts.CustomAttributes;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy
{
    [Serializable]
    public sealed class ErrorMapping
    {
        public int ErrorCode { get; set; }

        public int Chapter { get; set; }

        public bool IsDopList { get; set; }

        public string LineNumber { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string DataMembers { get; set; }

        [IgnoreLoad]
        public string[] DataMembersArray { get; set; }
    }
}