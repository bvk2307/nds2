﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument
{
    public enum KnpDecisionType
    {
        /// <summary>
        /// РешПриост - Решение о приостановлении операций по счетам
        /// </summary>
        SuspendOperationsByAccount = 1,
        /// <summary>
        /// РешПривлОтв - Решение о привлечении к ответственности
        /// </summary>
        BringToJustice = 2,
        /// <summary>
        /// РешОтказПривл - Решение об отказе в привлечении к ответственности за совершение налогового правонарушения
        /// </summary>
        RefuseToJutice = 3,
        /// <summary>
        /// РешВозмНДС - Решение о возмещении (полностью или частично) суммы НДС, заявленной к возмещению
        /// </summary>
        ApproveCompensationAmountNds = 4,
        /// <summary>
        /// РешОтказВозмНДС - Решение об отказе в возмещении (полностью или частично) суммы НДС, заявленной к возмещению
        /// </summary>
        RefuseCompensationAmountNds = 5,
        /// <summary>
        /// РешВозмНДСЗаявит - Решение о возмещении (полностью или частично) суммы НДС, заявленной к возмещению в заявительносм порядке
        /// </summary>
        ApproveCompensationAmountNdsDeclarer = 6,
        /// <summary>
        /// РешОтказВозмНДСЗаявит - Решение об отказе в возмещении (полностью или частично) суммы НДС, заявленной к возмещению в заявительном порядке
        /// </summary>
        RefuseCompensationAmountNdsDeclarer = 7,
        /// <summary>
        /// РешОтменВозмНДСЗаявит - Решение об отмене решения о возмещении (полностью или частично) суммы НДС, заявленной к возмещению в заявительном порядке
        /// </summary>
        CancelCompensationAmountNdsDeclarer = 8,
        /// <summary>
        /// РешОтменВозмНДСУточн - Решение об отмене решения о возмещении (полностью или частично) суммы НДС, заявленной к возмещению в заявительном порядке, в связи с представлением уточненной декларации
        /// </summary>
        CancelCompensationAmountNdsDeclarerByReasonOfRectification = 9
    }
}
