﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument
{
    public enum DocumentType
    {
        ClaimInvoice = 1,
        Reclaim = 2,
        ClaimControlRatio = 5
    }
}
