﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument
{
    public enum DocumentKind
    {
        FirstSide = 1,
        SecondSide = 2
    }
}
