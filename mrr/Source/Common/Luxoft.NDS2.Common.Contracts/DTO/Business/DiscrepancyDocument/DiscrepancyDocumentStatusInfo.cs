﻿namespace Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument
{
    public class DiscrepancyStatusInfo
    {
        public long DocumentId { get; set; }
        public string RowKey { get; set; }
        public int? DiscrepancyType { get; set; }
        public int? DiscrepancyStatus { get; set; }
        public int Count { get; set; }
    }
}
