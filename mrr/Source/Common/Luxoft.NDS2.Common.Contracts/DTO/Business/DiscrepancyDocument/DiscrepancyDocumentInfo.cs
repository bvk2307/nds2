﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument
{
    /// <summary>
    /// Документ, сформированный для расхождения(требование, истребование, прочее) в рамках взаимодействия с СЭОД
    /// </summary>
    [Serializable]
    public class DiscrepancyDocumentInfo : BusinessObject
    {
        [DisplayName("Номер")]
        [Description("Номер")]
        public long Id { get; set; }

        public long DeclarationId { get; set; }

        public long DeclarationVersionId { get; set; }

        public long ProcessId { get; set; }

        [DisplayName("Дата создания")]
        [Description("Дата создания требования")]
        public DateTime? CreateDate { get; set; }

        [DisplayName("Дата направления")]
        [Description("Дата направления налогоплательщику")]
        public DateTime? SendDate { get; set; }

        [DisplayName("Дата вручения НП")]
        [Description("Дата вручения требования налогоплательщику")]
        public DateTime? DeliveryDate { get; set; }

        [DisplayName("Дата ответа")]
        [Description("Дата ответа")]
        public DateTime? ReplyDate { get; set; }

        [DisplayName("Дата изменения статуса")]
        [Description("Дата изменения статуса")]
        public DateTime? StatusChangeDate { get; set; }

        [DisplayName("Статус")]
        [Description("Статус")]
        public DictionaryEntry Status { get; set; }

        [DisplayName("Номер в ЭОД")]
        [Description("Номер требования в ЭОД")]
        public string EodId { get; set; }

        [DisplayName("Дата в ЭОД")]
        [Description("Дата требования в ЭОД")]
        public DateTime? EodDate { get; set; }

        [DisplayName("Количество расхождений")]
        [Description("Количество расхождений")]
        public int DiscrepancyCount { get; set; }

        [DisplayName("Сумма расхождений")]
        [Description("Сумма расхождений")]
        public decimal? DiscrepancyAmount { get; set; }

        [DisplayName("Наименование НП")]
        [Description("Наименование налогоплательщика")]
        public string TaxPayerName { get; set; }

        [DisplayName("ИНН НП")]
        [Description("ИНН налогоплательщика")]
        public string TaxPayerInn { get; set; }

        [DisplayName("Налоговый орган")]
        [Description("Налоговый орган")]
        public CodeValueDictionaryEntry Soun { get; set; }

        /// <summary>Принято в СЭОД</summary>
        public int SeodAccepted { get; set; }

        /// <summary>Дата отправки в СЭОД</summary>
        public DateTime? SeodSentDate { get; set; }

        /// <summary>Дата принятия в СЭОД</summary>
        public DateTime? SeodAcceptDate { get; set; }

        /// <summary>Дата закрытия</summary>
        public DateTime? CloseDate { get; set; }

        /// <summary>Тип документа</summary>
        public DictionaryEntry DocType { get; set; }

        /// <summary>Комментарий пользователя</summary>
        public string UserComment { get; set; }

        /// <summary>Дата продления ответа (временно здесь, должна быть в сущности "Решение о продлении сроков")</summary>
        public DateTime? ProlongAnswerDate { get; set; }

        public string DeclarationOwner { get; set; }

        /// <summary>Причина закрытия</summary>
        public int CloseReason { get; set; }

        /// <summary>Статус обработки Hive КНП-расхождений документа</summary>
        public int KnpSyncStatus { get; set; }
    }
}