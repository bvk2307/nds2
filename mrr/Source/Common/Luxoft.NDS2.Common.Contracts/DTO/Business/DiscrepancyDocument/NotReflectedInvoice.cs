﻿using System;
using System.ComponentModel;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument
{
    [Serializable]
    public class NotReflectedInvoice
    {
        [DisplayName("№ НД")]
        [Description("Номер декларации")]
        public string DECLARATION_VERSION_ID { get; set; }

        [DisplayName("ИНН")]
        [Description("ИНН контрагента")]
        public string INN { get; set; }

        [DisplayName("КПП")]
        [Description("КПП контрагента")]
        public string KPP { get; set; }

        [DisplayName("Наименование")]
        [Description("Наименование контрагента")]
        public string NAME { get; set; }

        [DisplayName("№")]
        [Description("Номер счета-фактуры")]
        public string INVOICE_NUM { get; set; }

        [DisplayName("Дата")]
        [Description("Дата счета-фактуры")]
        public DateTime? INVOICE_DATE { get; set; }

        [DisplayName("Статус расхождения")]
        [Description("Статус расхождения по текущей записи о СФ")]
        public int DISCREPANCY_STATUS { get; set; }

        public string COMPARE_ROW_KEY { get; set; }

        public int CHAPTER { get; set; }

    }
}
