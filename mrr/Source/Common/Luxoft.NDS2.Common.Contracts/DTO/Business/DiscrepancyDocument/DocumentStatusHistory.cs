﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument
{
    [Serializable]
    public class DocumentStatusHistory
    {
        public int StatusCode { get; set; }
        public DateTime StatusDate { get; set; }
    }
}
