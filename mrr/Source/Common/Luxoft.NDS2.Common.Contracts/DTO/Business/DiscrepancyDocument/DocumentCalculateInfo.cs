﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument
{
    [Serializable]
    public class DocumentCalculateInfo
    {
        public long CountDiscrepancy { get; set; }
        public long CountDiscrepancyControlRatio { get; set; }
        public long CountInvoice { get; set; }
        public decimal SumAmountDiscrepancy { get; set; }
        public decimal SumPVPAmountDiscrepancy { get; set; }
    }
}
