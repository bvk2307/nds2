﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument
{
    [Serializable]
    public class DiscrepancyDocumentInvoice : Invoice
    {
        public decimal? CALC_PRICE_WITH_NDS { get; set; }
        public decimal? CALC_PRICE_NDS { get; set; }
        public decimal? GAP_AMOUNT { get; set; }
        public decimal? NDS_AMOUNT { get; set; }
        public int? GAP_STATUS { get; set; }
        public int? NDS_STATUS { get; set; }
        public string DISPLAY_FULL_TAX_PERIOD { get; set; }
    }
}
