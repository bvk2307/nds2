﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.TaxPayer
{
    /// <summary>
    /// Банковский счет
    /// </summary>
    [Serializable]
    public class BankAccount
    {
        [DisplayName("БИК банка")]
        [Description("БИК банка")]
        public string BIK { get; set; }

        [DisplayName("Наименование банка")]
        [Description("Наименование банка")]
        public string BankName { get; set; }

        [DisplayName("Номер счета")]
        [Description("Номер счета")]
        public string AccountNumber { get; set; }

        [DisplayName("Валюта счета")]
        [Description("Валюта счета")]
        public string AccountValute { get; set; }

        public string INN { get; set; }
        public string KPP { get; set; }
    }
}
