﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Security
{
    [Serializable]
    public class UserStructContextRight
    {
        public string StructContext { get; set; }

        public string UserSid { get; set; }

        public string Role { get; set; }
    }
}
