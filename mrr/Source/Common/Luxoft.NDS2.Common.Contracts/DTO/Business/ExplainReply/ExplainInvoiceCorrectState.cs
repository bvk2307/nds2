﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply
{
    [Serializable]
    public class ExplainInvoiceCorrectState
    {
        public long ExplainId { get; set; }
        public string InvoiceId { get; set; }
        public ExplainInvoiceStateInThis State { get; set; }
        public DateTime ExplainIncomingDate { get; set; }
        public bool RECEIVE_BY_TKS { get; set; }
    }
}
