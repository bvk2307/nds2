﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply
{
    [Serializable]
    public class ExplainReplyInfo
    {
        public long EXPLAIN_ID { get; set; }

        public long ZIP { get; set; }

        public long DOC_ID { get; set; }

        public long DECLARATION_VERSION_ID { get; set; }

        public bool RECEIVE_BY_TKS { get; set; }

        public int TYPE_ID { get; set; }

        [DisplayName("Тип")]
        [Description("Тип документа")]
        public string TYPE_NAME { get; set; }

        [DisplayName("Входящий номер")]
        [Description("Входящий номер")]
        public string INCOMING_NUM { get; set; }

        [DisplayName("Входящая дата")]
        [Description("Входящая дата")]
        public DateTime INCOMING_DATE { get; set; }

        [DisplayName("Дата получения ответа НО")]
        [Description("Дата получения ответа НО")]
        public DateTime? executor_receive_date { get; set; }

        [DisplayName("Дата отправки ответа из НО-исполнителя в НО-инициатор")]
        [Description("Дата отправки ответа из НО-исполнителя в НО-инициатор")]
        public DateTime? send_date_to_iniciator { get; set; }

        [DisplayName("Дата и время установки статуса")]
        [Description("Дата и время установки статуса")]
        public DateTime? status_set_date { get; set; }

        [DisplayName("Наименование статуса")]
        [Description("Наименование статуса")]
        public string status_name { get; set; }

        public int status_id { get; set; }

        [DisplayName("Комментарий")]
        [Description("Комментарий")]
        public string user_comment { get; set; }

        public string FILE_ID { get; set; }

        public string doc_num { get; set; }
        public DateTime? doc_date { get; set; }

        public string ExplainOtherReason { get; set; }
    }
}
