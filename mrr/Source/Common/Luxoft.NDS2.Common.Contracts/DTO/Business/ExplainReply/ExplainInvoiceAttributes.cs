﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply
{
    [Serializable]
    public class ExplainInvoiceAttributes
    {
        public List<InvoiceContractor> Buyers { get; set; }

        public List<InvoiceContractor> Sellers { get; set; }

        public List<InvoiceBuyAccept> BuyAccepts { get; set; }

        public List<InvoiceOperation> Operations { get; set; }

        public List<InvoicePaymentDocument> PaymentDocuments { get; set; }
    }
}
