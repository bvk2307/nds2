﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply
{
    [Serializable]
    public enum ExplainInvoiceType
    {
        Original = 1,
        Correct = 2
    }
}
