﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply
{
    [Serializable]
    public class ExplainInvoice : ICloneable
    {
        public ExplainInvoiceType Type { get; set; }

        public ExplainInvoiceState State { get; set; }

        public ExplainStates EStates { get; private set; }

        public List<ExplainInvoice> RelatedInvoices { get; set; }

        public ExplainInvoice Parent { get; set; }

        public ExplainInvoice PreviousInvoiceChange { get; set; }

        public ExplainInvoice LastInvoiceChange { get; set; }

        # region Множественные атрибуты

        public ExplainInvoiceAttributes Attributes { get; private set; }

        # endregion

        public ExplainInvoice()
        {
            EStates = new ExplainStates { States = new List<ExplainInvoiceState>() };
            State = ExplainInvoiceState.NotChanging;
            Attributes = new ExplainInvoiceAttributes
            {
                Buyers = new List<InvoiceContractor>(),
                Sellers = new List<InvoiceContractor>(),
                Operations = new List<InvoiceOperation>(),
                PaymentDocuments = new List<InvoicePaymentDocument>(),
                BuyAccepts = new List<InvoiceBuyAccept>()
            };
        }

        public void ClearState()
        {
            EStates.States.Clear();
        }

        public void SetState(ExplainInvoiceState state)
        {
            EStates.States.Clear();
            EStates.States.Add(state);
            SetCurrentState();
        }

        public void AddState(ExplainInvoiceState state)
        {
            if (!EStates.States.Contains(state))
            {
                EStates.States.Add(state);
            }
            SetCurrentState();
        }

        public void RemoveState(ExplainInvoiceState state)
        {
            EStates.States.Remove(state);
            SetCurrentState();
        }

        private void SetCurrentState()
        {
            if (EStates.States.Count > 0)
            {
                State = InvoiceStateRank.GetMax(EStates.States);
            }
            else
            {
                State = ExplainInvoiceState.NotChanging;
                EStates.States.Add(ExplainInvoiceState.NotChanging);
            }
        }

        public object Clone()
        {
            ExplainInvoice obj = new ExplainInvoice();

            obj.ACTUAL_ROW_KEY = this.ACTUAL_ROW_KEY;
            obj.BROKER_INN = this.BROKER_INN;
            obj.BROKER_KPP = this.BROKER_KPP;
            obj.BROKER_NAME = this.BROKER_NAME;
            obj.BUY_ACCEPT_DATE = this.BUY_ACCEPT_DATE;
            obj.BUYER_INN = this.BUYER_INN;
            obj.BUYER_KPP = this.BUYER_KPP;
            obj.BUYER_NAME = this.BUYER_NAME;
            obj.CHANGE_CORRECTION_DATE = this.CHANGE_CORRECTION_DATE;
            obj.CHANGE_CORRECTION_NUM = this.CHANGE_CORRECTION_NUM;
            obj.CHANGE_DATE = this.CHANGE_DATE;
            obj.CHANGE_NUM = this.CHANGE_NUM;
            obj.CHAPTER = this.CHAPTER;
            obj.COMPARE_ALGO_ID = this.COMPARE_ALGO_ID;
            obj.COMPARE_ROW_KEY = this.COMPARE_ROW_KEY;
            obj.CORRECTION_DATE = this.CORRECTION_DATE;
            obj.CORRECTION_NUM = this.CORRECTION_NUM;
            obj.CREATE_DATE = this.CREATE_DATE;
            obj.CUSTOMS_DECLARATION_NUM = this.CUSTOMS_DECLARATION_NUM;
            obj.DEAL_KIND_CODE = this.DEAL_KIND_CODE;
            obj.DECL_CORRECTION_NUM = this.DECL_CORRECTION_NUM;
            obj.DeclarationId = this.DeclarationId;
            obj.DIFF_CORRECT_DECREASE = this.DIFF_CORRECT_DECREASE;
            obj.DIFF_CORRECT_INCREASE = this.DIFF_CORRECT_INCREASE;
            obj.DIFF_CORRECT_NDS_DECREASE = this.DIFF_CORRECT_NDS_DECREASE;
            obj.DIFF_CORRECT_NDS_INCREASE = this.DIFF_CORRECT_NDS_INCREASE;
            obj.FISCAL_YEAR = this.FISCAL_YEAR;
            obj.FORMAT_ERRORS = this.FORMAT_ERRORS;
            obj.DISPLAY_FULL_TAX_PERIOD = this.DISPLAY_FULL_TAX_PERIOD;
            obj.INVOICE_DATE = this.INVOICE_DATE;
            obj.INVOICE_NUM = this.INVOICE_NUM;
            obj.LOGICAL_ERRORS = this.LOGICAL_ERRORS;
            obj.OKV_CODE = this.OKV_CODE;
            obj.OPERATION_CODE = this.OPERATION_CODE;
            obj.ORDINAL_NUMBER = this.ORDINAL_NUMBER;
            obj.PRICE_BUY_AMOUNT = this.PRICE_BUY_AMOUNT;
            obj.PRICE_BUY_NDS_AMOUNT = this.PRICE_BUY_NDS_AMOUNT;
            obj.PRICE_NDS_10 = this.PRICE_NDS_10;
            obj.PRICE_NDS_18 = this.PRICE_NDS_18;
            obj.PRICE_NDS_BUYER = this.PRICE_NDS_BUYER;
            obj.PRICE_NDS_TOTAL = this.PRICE_NDS_TOTAL;
            obj.PRICE_SELL = this.PRICE_SELL;
            obj.PRICE_SELL_0 = this.PRICE_SELL_0;
            obj.PRICE_SELL_10 = this.PRICE_SELL_10;
            obj.PRICE_SELL_18 = this.PRICE_SELL_18;
            obj.PRICE_SELL_IN_CURR = this.PRICE_SELL_IN_CURR;
            obj.PRICE_TAX_FREE = this.PRICE_TAX_FREE;
            obj.PRICE_TOTAL = this.PRICE_TOTAL;
            obj.PRIZNAK_DOP_LIST = this.PRIZNAK_DOP_LIST;
            obj.RECEIPT_DOC_DATE = this.RECEIPT_DOC_DATE;
            obj.RECEIPT_DOC_NUM = this.RECEIPT_DOC_NUM;
            obj.RECEIVE_DATE = this.RECEIVE_DATE;
            obj.ROW_KEY = this.ROW_KEY;
            obj.SELLER_AGENCY_INFO_DATE = this.SELLER_AGENCY_INFO_DATE;
            obj.SELLER_AGENCY_INFO_INN = this.SELLER_AGENCY_INFO_INN;
            obj.SELLER_AGENCY_INFO_KPP = this.SELLER_AGENCY_INFO_KPP;
            obj.SELLER_AGENCY_INFO_NAME = this.SELLER_AGENCY_INFO_NAME;
            obj.SELLER_AGENCY_INFO_NUM = this.SELLER_AGENCY_INFO_NUM;
            obj.SELLER_INN = this.SELLER_INN;
            obj.SELLER_INVOICE_DATE = this.SELLER_INVOICE_DATE;
            obj.SELLER_INVOICE_NUM = this.SELLER_INVOICE_NUM;
            obj.SELLER_KPP = this.SELLER_KPP;
            obj.SELLER_NAME = this.SELLER_NAME;
            obj.TAX_PERIOD = this.TAX_PERIOD;
            obj.Type = this.Type;
            obj.State = this.State;

            obj.INVOICE_DATE_DT = this.INVOICE_DATE_DT;
            obj.CHANGE_DATE_DT = this.CHANGE_DATE_DT;
            obj.CORRECTION_DATE_DT = this.CORRECTION_DATE_DT;
            obj.CHANGE_CORRECTION_DATE_DT = this.CHANGE_CORRECTION_DATE_DT;
            obj.BUY_ACCEPT_DATE_DT = this.BUY_ACCEPT_DATE_DT;
            obj.CREATE_DATE_DT = this.CREATE_DATE_DT;
            obj.SELLER_AGENCY_INFO_DATE_DT = this.SELLER_AGENCY_INFO_DATE_DT;
            obj.RECEIVE_DATE_DT = this.RECEIVE_DATE_DT;
            obj.SELLER_INVOICE_DATE_DT = this.SELLER_INVOICE_DATE_DT;
            obj.CHANGE_NUM_I = this.CHANGE_NUM_I;
            obj.CHANGE_CORRECTION_NUM_I = this.CHANGE_CORRECTION_NUM_I;
            obj.PRICE_BUY_AMOUNT_DC = this.PRICE_BUY_AMOUNT_DC;
            obj.PRICE_SELL_IN_CURR_DC = this.PRICE_SELL_IN_CURR_DC;
            obj.PRICE_SELL_DC = this.PRICE_SELL_DC;
            obj.PRICE_SELL_18_DC = this.PRICE_SELL_18_DC;
            obj.PRICE_SELL_10_DC = this.PRICE_SELL_10_DC;
            obj.PRICE_SELL_0_DC = this.PRICE_SELL_0_DC;
            obj.PRICE_TAX_FREE_DC = this.PRICE_TAX_FREE_DC;
            obj.PRICE_TOTAL_DC = this.PRICE_TOTAL_DC;
            obj.DIFF_CORRECT_NDS_DECREASE_DC = this.DIFF_CORRECT_NDS_DECREASE_DC;
            obj.DIFF_CORRECT_NDS_INCREASE_DC = this.DIFF_CORRECT_NDS_INCREASE_DC;
            obj.DEAL_KIND_CODE_I = this.DEAL_KIND_CODE_I;

            foreach (var item in this.Attributes.Buyers)
                obj.Attributes.Buyers.Add((InvoiceContractor)item.Clone());

            foreach (var item in this.Attributes.Sellers)
                obj.Attributes.Sellers.Add((InvoiceContractor)item.Clone());

            foreach (var item in this.Attributes.BuyAccepts)
                obj.Attributes.BuyAccepts.Add((InvoiceBuyAccept)item.Clone());

            foreach (var item in this.Attributes.Operations)
                obj.Attributes.Operations.Add((InvoiceOperation)item.Clone());

            foreach (var item in this.Attributes.PaymentDocuments)
                obj.Attributes.PaymentDocuments.Add((InvoicePaymentDocument)item.Clone());

            return obj;
        }

        [DisplayName("Дата")]
        [Description("Дата счета-фактуры")]
        public string INVOICE_DATE { get; set; }

        [DisplayName("Дата")]
        [Description("Дата исправления счета-фактуры продавца")]
        public string CHANGE_DATE { get; set; }

        [DisplayName("Дата")]
        [Description("Дата корректировочного счета-фактуры продавца")]
        public string CORRECTION_DATE { get; set; }

        [DisplayName("Дата")]
        [Description("Дата исправления корректировочного счета-фактуры продавца")]
        public string CHANGE_CORRECTION_DATE { get; set; }

        [DisplayName("Дата")]
        [Description("Дата документа, подтверждающего уплату налога")]
        public string RECEIPT_DOC_DATE { get; set; }

        [DisplayName("Дата принятия на учет")]
        [Description("Дата принятия на учет товаров (работ, услуг), имущественных прав")]
        public string BUY_ACCEPT_DATE { get; set; }

        [DisplayName("Дата выставления")]
        [Description("Дата выставления")]
        public string CREATE_DATE { get; set; }

        /// <summary>
        /// Сведения о посреднической деятельности продавца: дата СФ
        /// </summary>
        public string SELLER_AGENCY_INFO_DATE { get; set; }

        [DisplayName("Дата получения")]
        [Description("Дата получения")]
        public string RECEIVE_DATE { get; set; }

        [DisplayName("Дата")]
        [Description("Дата счета-фактуры продавца")]
        public string SELLER_INVOICE_DATE { get; set; }

        [DisplayName("№")]
        [Description("Номер исправления счета-фактуры продавца")]
        public string CHANGE_NUM { get; set; }

        [DisplayName("№")]
        [Description("Номер корректировочного счета-фактуры продавца")]
        public string CORRECTION_NUM { get; set; }

        [DisplayName("№")]
        [Description("Номер исправления корректировочного счета-фактуры продавца")]
        public string CHANGE_CORRECTION_NUM { get; set; }

        [DisplayName("Стоимость покупок с НДС")]
        [Description("Стоимость покупок по счету-фактуре, разница стоимости по корректировочному счету-фактуре (включая налог), в валюте счета-фактуры")]
        public string PRICE_BUY_AMOUNT { get; set; }

        [DisplayName("в валюте")]
        [Description("Стоимость продаж по счету-фактуре, разница стоимости по корректировочному счету-фактуре (включая налог), в валюте счета-фактуры")]
        public string PRICE_SELL_IN_CURR { get; set; }

        [DisplayName("в руб. и коп.")]
        [Description("Стоимость продаж по счету-фактуре, разница стоимости по корректировочному счету-фактуре (включая налог) в рублях и копейках ")]
        public string PRICE_SELL { get; set; }

        [DisplayName("18%")]
        [Description("Стоимость продаж, облагаемых налогом, по счету-фактуре, разница стоимости по корректировочному счету-фактуре (без налога) в рублях и копейках, по ставке 18 %")]
        public string PRICE_SELL_18 { get; set; }

        [DisplayName("10%")]
        [Description("Стоимость продаж, облагаемых налогом, по счету-фактуре, разница стоимости по корректировочному счету-фактуре (без налога) в рублях и копейках, по ставке 10 %")]
        public string PRICE_SELL_10 { get; set; }

        [DisplayName("0%")]
        [Description("Стоимость продаж, облагаемых налогом, по счету-фактуре, разница стоимости по корректировочному счету-фактуре (без налога) в рублях и копейках, по ставке 0 %")]
        public string PRICE_SELL_0 { get; set; }

        [DisplayName("Стоимость продаж, освобождаемых он налога")]
        [Description("Стоимость продаж, освобождаемых от налога, по счету-фактуре, разница стоимости по корректировочному счету-фактуре, в рублях и копейках")]
        public string PRICE_TAX_FREE { get; set; }

        [DisplayName("Стоимость товаров")]
        [Description("Стоимость товаров (работ, услуг), имущественных прав по счету-фактуре - всего")]
        public string PRICE_TOTAL { get; set; }

        [DisplayName("уменьшение")]
        [Description("Разница стоимости с учетом налога по корректировочному счету-фактуре - уменьшение")]
        public string DIFF_CORRECT_NDS_DECREASE { get; set; }

        [DisplayName("увеличение")]
        [Description("Разница стоимости с учетом налога по корректировочному счету-фактуре - увеличение")]
        public string DIFF_CORRECT_NDS_INCREASE { get; set; }

        [DisplayName("Код вида сделки")]
        [Description("Код вида сделки")]
        public string DEAL_KIND_CODE { get; set; }

        public long DeclarationId { get; set; }

        public long DocId { get; set; }

        public int CHAPTER { get; set; }

        [DisplayName("№")]
        [Description("Порядковый номер")]
        public long? ORDINAL_NUMBER { get; set; }

        [DisplayName("Дата выставления")]
        [Description("Дата выставления")]
        public DateTime? CREATE_DATE_DT { get; set; }

        [DisplayName("Дата получения")]
        [Description("Дата получения")]
        public DateTime? RECEIVE_DATE_DT { get; set; }

        [DisplayName("Код вида операции")]
        [Description("Код вида операции")]   
        public string OPERATION_CODE { get; set; }

        [DisplayName("№")]
        [Description("Номер счета-фактуры")]   
        public string INVOICE_NUM { get; set; }

        [DisplayName("Дата")]
        [Description("Дата счета-фактуры")]
        public DateTime? INVOICE_DATE_DT { get; set; }

        [DisplayName("№")]
        [Description("Номер исправления счета-фактуры продавца")]
        public int? CHANGE_NUM_I { get; set; }

        [DisplayName("Дата")]
        [Description("Дата исправления счета-фактуры продавца")]
        public DateTime? CHANGE_DATE_DT { get; set; }

        [DisplayName("Дата")]
        [Description("Дата корректировочного счета-фактуры продавца")]
        public DateTime? CORRECTION_DATE_DT { get; set; }

        [DisplayName("№")]
        [Description("Номер исправления корректировочного счета-фактуры продавца")]
        public int? CHANGE_CORRECTION_NUM_I { get; set; }

        [DisplayName("Дата")]
        [Description("Дата исправления корректировочного счета-фактуры продавца")]
        public DateTime? CHANGE_CORRECTION_DATE_DT { get; set; }

        [DisplayName("Номер")]
        [Description("Номер документа, подтверждающего уплату налога")]
        public string RECEIPT_DOC_NUM { get; set; }

        [DisplayName("Дата принятия на учет")]
        [Description("Дата принятия на учет товаров (работ, услуг), имущественных прав")]
        public DateTime? BUY_ACCEPT_DATE_DT { get; set; }

        [DisplayName("ИНН")]
        [Description("ИНН покупателя")]
        public string BUYER_INN { get; set; }

        [DisplayName("КПП")]
        [Description("КПП покупателя")]
        public string BUYER_KPP { get; set; }

        [DisplayName("Наименование")]
        [Description("Наименование покупателя")]
        public string BUYER_NAME { get; set; }

        [DisplayName("ИНН")]
        [Description("ИНН продавца")]
        public string SELLER_INN { get; set; }

        [DisplayName("КПП")]
        [Description("КПП продавца")]
        public string SELLER_KPP { get; set; }

        [DisplayName("Наименование")]
        [Description("Наименование продавца")]
        public string SELLER_NAME { get; set; }

        [DisplayName("№")]
        [Description("Номер счета-фактуры продавца")]
        public string SELLER_INVOICE_NUM { get; set; }

        [DisplayName("Дата")]
        [Description("Дата счета-фактуры продавца")]
        public DateTime? SELLER_INVOICE_DATE_DT { get; set; }

        [DisplayName("ИНН")]
        [Description("ИНН посредника(комиссионера, агента, экспедитора или застройщика)")]
        public string BROKER_INN { get; set; }

        [DisplayName("КПП")]
        [Description("КПП посредника (комиссионера, агента, экспедитора или застройщика)")]       
        public string BROKER_KPP { get; set; }

        [DisplayName("Наименование")]
        [Description("Наименование посредника(комиссионера, агента, экспедитора или застройщика)")]
        public string BROKER_NAME { get; set; }

        [DisplayName("Код валюты")]
        [Description("Код валюты по ОКВ")]
        public string OKV_CODE { get; set; }

        [DisplayName("Код вида сделки")]
        [Description("Код вида сделки")]
        public int? DEAL_KIND_CODE_I { get; set; }

        [DisplayName("Регистрационный № ТД")]
        [Description("Регистрационный номер таможенной декларации")]
        public string CUSTOMS_DECLARATION_NUM { get; set; }

        [DisplayName("Стоимость покупок с НДС")]
        [Description("Стоимость покупок по счету-фактуре, разница стоимости по корректировочному счету-фактуре (включая налог), в валюте счета-фактуры")]
        public decimal? PRICE_BUY_AMOUNT_DC { get; set; }

        [DisplayName("Сумма НДС")]
        [Description("Сумма налога по счету-фактуре, разница суммы налога по корректировочному счету-фактуре принимаемая к вычету в рублях и копейках")]
        public decimal? PRICE_BUY_NDS_AMOUNT { get; set; }

        [DisplayName("в руб. и коп.")]
        [Description("Стоимость продаж по счету-фактуре, разница стоимости по корректировочному счету-фактуре (включая налог) в рублях и копейках ")]
        public decimal? PRICE_SELL_DC { get; set; }

        [DisplayName("в валюте")]
        [Description("Стоимость продаж по счету-фактуре, разница стоимости по корректировочному счету-фактуре (включая налог), в валюте счета-фактуры")]
        public decimal? PRICE_SELL_IN_CURR_DC { get; set; }

        [DisplayName("18%")]
        [Description("Стоимость продаж, облагаемых налогом, по счету-фактуре, разница стоимости по корректировочному счету-фактуре (без налога) в рублях и копейках, по ставке 18 %")]
        public decimal? PRICE_SELL_18_DC { get; set; }

        [DisplayName("10%")]
        [Description("Стоимость продаж, облагаемых налогом, по счету-фактуре, разница стоимости по корректировочному счету-фактуре (без налога) в рублях и копейках, по ставке 10 %")]
        public decimal? PRICE_SELL_10_DC { get; set; }

        [DisplayName("0%")]
        [Description("Стоимость продаж, облагаемых налогом, по счету-фактуре, разница стоимости по корректировочному счету-фактуре (без налога) в рублях и копейках, по ставке 0 %")]
        public decimal? PRICE_SELL_0_DC { get; set; }

        [DisplayName("18%")]
        [Description("Сумма налога по счету-фактуре, разница суммы налога по корректировочному счету-фактуре, в рублях и копейках, по ставке 18 %")]
        public decimal? PRICE_NDS_18 { get; set; }

        [DisplayName("10%")]
        [Description("Сумма налога по счету-фактуре, разница суммы налога по корректировочному счету-фактуре, в рублях и копейках, по ставке 10 %")]
        public decimal? PRICE_NDS_10 { get; set; }

        [DisplayName("Стоимость продаж, освобождаемых он налога")]
        [Description("Стоимость продаж, освобождаемых от налога, по счету-фактуре, разница стоимости по корректировочному счету-фактуре, в рублях и копейках")]
        public decimal? PRICE_TAX_FREE_DC { get; set; }

        [DisplayName("Стоимость товаров")]
        [Description("Стоимость товаров (работ, услуг), имущественных прав по счету-фактуре - всего")]
        public decimal? PRICE_TOTAL_DC { get; set; }

        [DisplayName("Сумма НДС")]
        [Description("В том числе сумма НДС по счету-фактуре")]       
        public decimal? PRICE_NDS_TOTAL { get; set; }

        [DisplayName("уменьшение")]
        [Description("Разница налога по корректировочному счету-фактуре – уменьшение")]
        public decimal? DIFF_CORRECT_DECREASE { get; set; }

        [DisplayName("увеличение")]
        [Description("Разница налога по корректировочному счету-фактуре - увеличение")]
        public decimal? DIFF_CORRECT_INCREASE { get; set; }

        [DisplayName("уменьшение")]
        [Description("Разница стоимости с учетом налога по корректировочному счету-фактуре - уменьшение")]
        public decimal? DIFF_CORRECT_NDS_DECREASE_DC { get; set; }

        [DisplayName("увеличение")]
        [Description("Разница стоимости с учетом налога по корректировочному счету-фактуре - увеличение")]
        public decimal? DIFF_CORRECT_NDS_INCREASE_DC { get; set; }

        [DisplayName("№ коррект.")]
        [Description("Номер корректировки декларации")]
        public string DECL_CORRECTION_NUM  { get; set; }

        [DisplayName("Сумма НДС")]
        [Description("Сумма налога, предъявляемая покупателю, в руб. и коп.")]
        public decimal? PRICE_NDS_BUYER { get; set; }

        [DisplayName("Период отображения записи")]
        [Description("Отчетный период НД, в которой данная запись была отражена")]
        public string DISPLAY_FULL_TAX_PERIOD { get; set; }

        public string TAX_PERIOD { get; set; }
        public string FISCAL_YEAR { get; set; }

        [DisplayName("Доп.лист")]
        [Description("Сведения из дополнительного листа")]
        public bool PRIZNAK_DOP_LIST { get; set; }

        /// <summary>
        /// Сведения о посреднической деятельности продавца: ИНН
        /// </summary>
        public string SELLER_AGENCY_INFO_INN { get; set; }
        /// <summary>
        /// Сведения о посреднической деятельности продавца: КПП
        /// </summary>
        public string SELLER_AGENCY_INFO_KPP { get; set; }
        /// <summary>
        /// Сведения о посреднической деятельности продавца: Наименование
        /// </summary>
        public string SELLER_AGENCY_INFO_NAME { get; set; }
        /// <summary>
        /// Сведения о посреднической деятельности продавца: № СФ
        /// </summary>
        public string SELLER_AGENCY_INFO_NUM { get; set; }
        /// <summary>
        /// Сведения о посреднической деятельности продавца: дата СФ
        /// </summary>
        public DateTime? SELLER_AGENCY_INFO_DATE_DT { get; set; }

        /// <summary>
        /// Идентификатор записи в Hbase
        /// </summary>
        public string ROW_KEY { get; set; }
        /// <summary>
        /// Идентификатор актуальной записи в Hbase
        /// </summary>
        public string ACTUAL_ROW_KEY { get; set; }
        /// <summary>
        /// Идентификатор записи в Hbase с которой было произведено сопоставление
        /// </summary>
        public string COMPARE_ROW_KEY { get; set; }
        /// <summary>
        /// Алгоритм по которому было сопоставление
        /// </summary>
        public int? COMPARE_ALGO_ID { get; set; }
        /// <summary>
        /// Форматные ошибки
        /// </summary>
        public string FORMAT_ERRORS { get; set; }
        /// <summary>
        /// Логические ошибки
        /// </summary>
        public string LOGICAL_ERRORS { get; set; }

        public static string FormatDateTime(DateTime? dt)
        {
            string ret = String.Empty;
            if (dt != null)
            {
                ret = dt.Value.ToShortDateString();
            }
            return ret;
        }

        public static string FormatDecimal(decimal? value)
        {            
            string ret = String.Empty;
            if (value != null)
            {
                ret = value.ToString();
            }
            return ret;
        }

        public void CopyToStringProperties()
        {
            INVOICE_DATE = FormatDateTime(INVOICE_DATE_DT);
            CHANGE_DATE = FormatDateTime(CHANGE_DATE_DT);
            CORRECTION_DATE = FormatDateTime(CORRECTION_DATE_DT);
            CHANGE_CORRECTION_DATE = FormatDateTime(CHANGE_CORRECTION_DATE_DT);
            CREATE_DATE = FormatDateTime(CREATE_DATE_DT);
            SELLER_AGENCY_INFO_DATE = FormatDateTime(SELLER_AGENCY_INFO_DATE_DT);
            RECEIVE_DATE = FormatDateTime(RECEIVE_DATE_DT);
            SELLER_INVOICE_DATE = FormatDateTime(SELLER_INVOICE_DATE_DT);
            CHANGE_NUM = CHANGE_NUM_I.ToString();
            CHANGE_CORRECTION_NUM = CHANGE_CORRECTION_NUM_I.ToString();
            PRICE_BUY_AMOUNT = FormatDecimal(PRICE_BUY_AMOUNT_DC);
            PRICE_SELL_IN_CURR = FormatDecimal(PRICE_SELL_IN_CURR_DC);
            PRICE_SELL = FormatDecimal(PRICE_SELL_DC);
            PRICE_SELL_18 = FormatDecimal(PRICE_SELL_18_DC);
            PRICE_SELL_10 = FormatDecimal(PRICE_SELL_10_DC);
            PRICE_SELL_0 = FormatDecimal(PRICE_SELL_0_DC);
            PRICE_TAX_FREE = FormatDecimal(PRICE_TAX_FREE_DC);
            PRICE_TOTAL = FormatDecimal(PRICE_TOTAL_DC);
            DIFF_CORRECT_NDS_DECREASE = FormatDecimal(DIFF_CORRECT_NDS_DECREASE_DC);
            DIFF_CORRECT_NDS_INCREASE = FormatDecimal(DIFF_CORRECT_NDS_INCREASE_DC);
            DEAL_KIND_CODE = DEAL_KIND_CODE_I.ToString();
        }

        public void SetMultipleAttributes()
        {
            BUYER_INN = string.Join(",", Attributes.Buyers.Select(x => x.Inn).ToArray());
            BUYER_KPP = string.Join(",", Attributes.Buyers.Select(x => x.Kpp).ToArray());
            SELLER_INN = string.Join(",", Attributes.Sellers.Select(x => x.Inn).ToArray());
            SELLER_KPP = string.Join(",", Attributes.Sellers.Select(x => x.Kpp).ToArray());
            BUY_ACCEPT_DATE = 
                string.Join(
                    ",",
                    Attributes.BuyAccepts
                        .Where(x => x.AcceptedAt.HasValue)
                        .Select(x => x.AcceptedAt.Value.ToShortDateString())
                        .ToArray());
            OPERATION_CODE = string.Join(",", Attributes.Operations.Select(x => x.Code).ToArray());
            RECEIPT_DOC_NUM = string.Join(",", Attributes.PaymentDocuments.Select(x => x.Number).ToArray());
            RECEIPT_DOC_DATE = 
                string.Join(
                ",", 
                Attributes.PaymentDocuments
                .Where(x => x.Date.HasValue)
                .Select(x => x.Date.Value.ToShortDateString()).ToArray());
        }

        public void RefreshProperties()
        {
            CopyToStringProperties();
            SetMultipleAttributes();
        }
    }
}
