﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply
{
    [Serializable]
    public class ExplainInvoiceCorrect
    {
        public long ExplainId { get; set; }
        public string InvoiceId { get; set; }
        public DateTime ExplainIncomingDate { get; set; }
        public string FieldName { get; set; }
        public string FieldValue { get; set; }
    }
}
