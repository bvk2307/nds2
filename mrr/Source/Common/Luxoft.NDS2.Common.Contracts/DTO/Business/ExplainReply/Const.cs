﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply
{
    public class ExplainReplyType
    {
        public static readonly int Explain = 1;
        public static readonly int Reply_93 = 2;
        public static readonly int Reply_93_1 = 3;
    }

    public class ExplainReplyStatus
    {
        public static readonly int NotProcessed = 1;
        public static readonly int SendToMC = 2;
        public static readonly int ProcessedInMC = 3;
        public static readonly int SendToIniciator = 4;
        public static readonly int RecieveIniciator = 5;
    }

    [Serializable]
    public enum InvoiceCorrectTypeOperation
    {
        AddOrUpdate = 1,
        Remove = 2
    }
}