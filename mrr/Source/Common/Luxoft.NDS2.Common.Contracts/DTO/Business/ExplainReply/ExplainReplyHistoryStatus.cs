﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply
{
    [Serializable]
    public class ExplainReplyHistoryStatus
    {
        public int Status_id { get; set; }
        public DateTime Submit_date { get; set; }
    }
}
