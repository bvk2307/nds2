﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply
{
    [Serializable]
    public enum ExplainInvoiceState
    {
        NotUsing = 0,
        NotChanging = 1,
        ChangingInThisExplain = 2,
        ChangingInPreviousExplain = 3,
        ProcessedInThisExplain = 4,
        ProcessedInPreviousExplain = 5
    }

    [Serializable]
    public enum ExplainInvoiceStateInThis
    {
        Changing = 1,
        Processed = 2
    }

    public class InvoiceStateRank
    {
        private static Dictionary<ExplainInvoiceState, int> prioritets = new Dictionary<ExplainInvoiceState, int>();

        private static void Init()
        {
            prioritets.Add(ExplainInvoiceState.NotUsing, 1);
            prioritets.Add(ExplainInvoiceState.NotChanging, 2);
            prioritets.Add(ExplainInvoiceState.ChangingInPreviousExplain, 3);
            prioritets.Add(ExplainInvoiceState.ProcessedInPreviousExplain, 4);
            prioritets.Add(ExplainInvoiceState.ChangingInThisExplain, 5);
            prioritets.Add(ExplainInvoiceState.ProcessedInThisExplain, 6);
        }

        public static ExplainInvoiceState GetMax(List<ExplainInvoiceState> states)
        {
            ExplainInvoiceState ret = ExplainInvoiceState.NotChanging;

            if (prioritets.Count() == 0)
            {
                Init();
            }

            int maxRank = 0;
            int currentRank = 0;
            foreach(ExplainInvoiceState item in states)
            {
                if (prioritets.ContainsKey(item))
                {
                    currentRank = prioritets[item];
                    if (currentRank > maxRank)
                    {
                        currentRank = maxRank;
                        ret = item;
                    }
                }
            }

            return ret;
        }
    }
}
