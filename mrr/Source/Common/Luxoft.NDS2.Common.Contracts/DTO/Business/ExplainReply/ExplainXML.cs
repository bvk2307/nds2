﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply
{
    [Serializable]
    public class ExplainXML
    {
        public string XmlSerilizedOfExplain { get; set; }
        public string NameFileOfExplain { get; set; }
        public string XmlSerilizedOfInventory { get; set; }
        public string NameFileOfInventory { get; set; }
    }
}
