﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Common
{
    [Serializable]
    public class DictionaryEntryBase<T>
    {
        public T EntryId { get; set; }
        public string EntryValue { get; set; }

        public override string ToString()
        {
            return EntryValue;
        }
    }

    [Serializable]
    public class DictionaryEntry : DictionaryEntryBase<int>
    {

    }

    [Serializable]
    public class CodeValueDictionaryEntry : DictionaryEntryBase<string>
    {
        public CodeValueDictionaryEntry(string code, string value)
        {
            EntryId = code;
            EntryValue = value;
        }

        public override string ToString()
        {
            string ret = String.Empty;
            if (!string.IsNullOrWhiteSpace(EntryId) || !string.IsNullOrWhiteSpace(EntryValue))
            {
                ret = string.Format("{0}-{1}", EntryId, EntryValue);
            }
            return ret; 
        }

        public override int GetHashCode()
        {
            return EntryId.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(obj, this))
            {
                return true;
            }
            var value = obj as CodeValueDictionaryEntry;
            if (ReferenceEquals(value, null))
            {
                return false;
            }
            return EntryId.Equals(value.EntryId) && EntryValue.Equals(value.EntryValue);
        }
    }
}
