﻿using System;
using System.Runtime.Serialization;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Common
{
    [DataContract]
    [Serializable]
    public class QuarterInfo
    {
        
        /// <summary>
        /// Год
        /// </summary>
        [DataMember(Name = "year")]
        public int Year { get; set; }

        
        /// <summary>
        /// Номер квартала
        /// </summary>
        [DataMember(Name = "quarter")]
        public int Quarter { get; set; }

        [IgnoreDataMember]
        public int Index { get { return int.Parse(string.Concat(Year, Quarter)); } }

        public override int GetHashCode()
        {
            return this.Index;
        }

        public override bool Equals(object obj)
        {
            var refData = obj as QuarterInfo;
            if (refData == null) return false;
            
            return refData.Quarter ==
            this.Quarter && refData.Year == this.Year;
        }

        public bool Equals(int year, int quarter)
        {
            return this.Year == year && this.Quarter == quarter;
        }

        public override string ToString()
        {
            return string.Format("{0} кв. {1} г.", Quarter, Year);
        }

        public string QuarterToString()
        {
            return string.Format("{0} кв.", Quarter);
        }

        public static string ToString(int year, int quarter)
        {
            return string.Format("{0} кв. {1} г.", quarter, year);
        }

        public DateTime ToReportDate()
        {
            int month = 0;
            int year = Year;
            if (Quarter == 3 || Quarter == 4)
            {
                year++;
            }
            switch (Quarter)
            {
                case 1:
                    month = 8;
                    break;
                case 2:
                    month = 11;
                    break;
                case 3:
                    month = 2;
                    break;
                case 4:
                    month = 5;
                    break;
            }
            return new DateTime(year, month, 1);
        }
    }
}
