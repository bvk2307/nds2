﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Report
{
    public enum ReportNum
    {
        MatchingRule = 12,
        CheckLogicControl = 13,
        CheckControlRatio = 14,
        InspectorMonitoringWork = 16,
        MonitorProcessDeclaration = 17,
        LoadingInspection = 21,
        DeclarationStatistic = 51,
        DynamicTechnoParameter = 52,
        DynamicDeclarationStatistic = 53,
        InfoResultsOfMatching = 54
    }
}
