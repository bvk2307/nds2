﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Report
{
    [Serializable]
    public class StatisticDeclarationAndBook
    {
        public bool IsNew()
        {
            return Id < 1;
        }

        [DisplayName("Номер")]
        [Description("Номер документа")]
        public long Id { set; get; }

        [DisplayName("Федеральный округ")]
        [Description("Федеральный округ")]
        public string FederalDistrict { get; set; }

        [DisplayName("Регион")]
        [Description("Регион")]
        public string Region { get; set; }

        [DisplayName("Инспекция/МРИ")]
        [Description("Инспекция/МРИ")]
        public string Inspection { get; set; }

        [DisplayName("Кол-во лиц обязанных пред. НД")]
        [Description("Кол-во лиц обязанных пред. НД")]
        public long ObligedPersonCount { get; set; }

        [DisplayName("Кол-во лиц обязанных пред. НД Доля представивших")]
        [Description("Кол-во лиц обязанных пред. НД Доля представивших")]
        public long ObligedPersonSubmittedCount { get; set; }

        [DisplayName("Кол-во лиц обязанных пред. представивших:нулевых")]
        [Description("Кол-во лиц обязанных пред. представивших:нулевых")]
        public long ObligedPersonSubmittedNullCount { get; set; }

        [DisplayName("Кол-во лиц обязанных пред. представивших:к возмещению")]
        [Description("Кол-во лиц обязанных пред. представивших:к возмещению")]
        public long ObligedPersonSubmittedСompensationCount { get; set; }

        [DisplayName("Кол-во лиц обязанных пред. представивших:к уплате")]
        [Description("Кол-во лиц обязанных пред. представивших:к уплате")]
        public long ObligedPersonSubmittedPaymentCount { get; set; }

        [DisplayName("Кол-во лиц обязанных пред. представивших журнал")]
        [Description("Кол-во лиц обязанных пред. представивших журнал")]
        public long ObligedPersonSubmittedBookCount { get; set; }

        [DisplayName("Кол-во лиц обязанных пред. не представивших ни один из документов")]
        [Description("Кол-во лиц обязанных пред. не представивших ни один из документов")]
        public long ObligedPersonSubmittedNotDocumentCount { get; set; }

        [DisplayName("Кол-во лиц обязанных пред. представивших документ на бумаге")]
        [Description("Кол-во лиц обязанных пред. представивших документ на бумаге")]
        public long ObligedPersonSubmittedPaperDocumentCount { get; set; }

        [DisplayName("Кол-во лиц обязанных пред. представивших документ посредством ТКС")]
        [Description("Кол-во лиц обязанных пред. представивших документ посредством ТКС")]
        public long ObligedPersonSubmittedDocumentTKSCount { get; set; }

        [DisplayName("Кол-во лиц обязанных пред. получивших возмещение ст. 176.1")]
        [Description("Кол-во лиц обязанных пред. получивших возмещение ст. 176.1")]
        public long ObligedPersonSubmittedСompensation176p1Сount { get; set; }



        [DisplayName("Кол-во лиц не обязанных пред. представивших:нулевых")]
        [Description("Кол-во лиц не обязанных пред. представивших:нулевых")]
        public long ObligedPersonNotSubmittedNullCount { get; set; }

        [DisplayName("Кол-во лиц не обязанных пред. представивших:к возмещению")]
        [Description("Кол-во лиц не обязанных пред. представивших:к возмещению")]
        public long ObligedPersonNotSubmittedСompensationCount { get; set; }

        [DisplayName("Кол-во лиц не обязанных пред. представивших:к уплате")]
        [Description("Кол-во лиц не обязанных пред. представивших:к уплате")]
        public long ObligedPersonNotSubmittedPaymentCount { get; set; }

        [DisplayName("Кол-во лиц не обязанных пред. представивших журнал")]
        [Description("Кол-во лиц не обязанных пред. представивших журнал")]
        public long ObligedPersonNotSubmittedBookCount { get; set; }

        [DisplayName("Кол-во лиц не обязанных пред. не представивших ни один из документов")]
        [Description("Кол-во лиц не обязанных пред. не представивших ни один из документов")]
        public long ObligedPersonNotSubmittedNotDocumentCount { get; set; }

        [DisplayName("Кол-во лиц не обязанных пред. представивших документ на бумаге")]
        [Description("Кол-во лиц не обязанных пред. представивших документ на бумаге")]
        public long ObligedPersonNotSubmittedPaperDocumentCount { get; set; }

        [DisplayName("Кол-во лиц не обязанных пред. представивших документ посредством ТКС")]
        [Description("Кол-во лиц не обязанных пред. представивших документ посредством ТКС")]
        public long ObligedPersonNotSubmittedDocumentTKSCount { get; set; }

        [DisplayName("Кол-во лиц не обязанных пред. получивших возмещение ст. 176.1")]
        [Description("Кол-во лиц не обязанных пред. получивших возмещение ст. 176.1")]
        public long ObligedPersonNotSubmittedСompensation176p1Сount { get; set; }



        [DisplayName("Кол-во лиц удовл. условие п.5.2 ст.174 представившие журнал")]
        [Description("Кол-во лиц удовл. условие п.5.2 ст.174 представившие журнал")]
        public long ObligedPersonSatisfyingConditionArticle174BookСount { get; set; }

        [DisplayName("Кол-во лиц удовл. условие п.5.2 ст.174 не представившие журнал")]
        [Description("Кол-во лиц удовл. условие п.5.2 ст.174 не представившие журнал")]
        public long ObligedPersonSatisfyingConditionArticle174NotBookСount { get; set; }

        [DisplayName("Кол-во лиц удовл. условие п.5.2 ст.174 представивших документ на бумаге")]
        [Description("Кол-во лиц удовл. условие п.5.2 ст.174 представивших документ на бумаге")]
        public long ObligedPersonSatisfyingConditionArticle174PaperDocumentСount { get; set; }

        [DisplayName("Кол-во лиц удовл. условие п.5.2 ст.174 представивших документ посредством ТКС")]
        [Description("Кол-во лиц удовл. условие п.5.2 ст.174 представивших документ посредством ТКС")]
        public long ObligedPersonSatisfyingConditionArticle174DocumentTKSСount { get; set; }


        [DisplayName("Кол-во лиц не удовл. условие п.5.2 ст.174 представившие журнал")]
        [Description("Кол-во лиц не удовл. условие п.5.2 ст.174 представившие журнал")]
        public long ObligedPersonNotSatisfyingConditionArticle174BookСount { get; set; }

        [DisplayName("Кол-во лиц не удовл. условие п.5.2 ст.174 не представившие журнал")]
        [Description("Кол-во лиц не удовл. условие п.5.2 ст.174 не представившие журнал")]
        public long ObligedPersonNotSatisfyingConditionArticle174NotBookСount { get; set; }

        [DisplayName("Кол-во лиц не удовл. условие п.5.2 ст.174 представивших документ на бумаге")]
        [Description("Кол-во лиц не удовл. условие п.5.2 ст.174 представивших документ на бумаге")]
        public long ObligedPersonNotSatisfyingConditionArticle174PaperDocumentСount { get; set; }

        [DisplayName("Кол-во лиц не удовл. условие п.5.2 ст.174 представивших документ посредством ТКС")]
        [Description("Кол-во лиц не удовл. условие п.5.2 ст.174 представивших документ посредством ТКС")]
        public long ObligedPersonNotSatisfyingConditionArticle174DocumentTKSСount { get; set; }


        public static StatisticDeclarationAndBook New()
        {
            return new StatisticDeclarationAndBook();
        }
    }
}
