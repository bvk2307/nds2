﻿
namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Report
{
    public enum TaskStatus
    {
        None = 9,
        WaitProcessing = 0,
        Complete = 1
    }

    public class TaskParamCriteria
    {
        public const string DynamicParameters = "DynamicParameters";
        public const string DynamicAggregateLevel = "DynamicAggregateLevel";
        public const string DynamicTimeCounting = "DynamicTimeCounting";
        public const string DynamicDateBegin = "DynamicDateBegin";

        public const string DDS_AggregateByNalogOrgan = "AggregateByNalogOrgan";
        public const string DDS_AggregateByTime = "AggregateByTime";
        public const string DDS_TypeReport = "TypeReport";
        public const string DDS_NalogPeriod = "NalogPeriod";
        public const string DDS_FiscalYear = "FiscalYear";
        public const string DDS_DateBeginDay = "DateBeginDay";
        public const string DDS_DateEndDay = "DateEndDay";
        public const string DDS_DateBeginWeek = "DateBeginWeek";
        public const string DDS_DateEndWeek = "DateEndWeek";
        public const string DDS_YearBegin = "YearBegin";
        public const string DDS_MonthBegin = "MonthBegin";
        public const string DDS_YearEnd = "YearEnd";
        public const string DDS_MonthEnd = "MonthEnd";
        public const string DDS_DynamicParameters = "DynamicParameters";
        public const string DDS_FederalDistrict = "FederalDistrict";
        public const string DDS_TaxPayerRegionCode = "TaxPayerRegionCode";
        public const string DDS_NalogOrganCode = "NalogOrganCode";
        public const string SonoCode = "SonoCode";

        public const string IRM_AggregateByNalogOrgan = "AggregateByNalogOrgan";
        public const string IRM_FederalDistrict = "FederalDistrict";
        public const string IRM_TaxPayerRegionCode = "TaxPayerRegionCode";
        public const string IRM_NalogOrganCode = "NalogOrganCode";
        public const string IRM_TimePeriod = "TimePeriod";
        public const string IRM_NalogOrganCheck = "NalogOrganCheck";
        public const string IRM_DateOneDay = "DateOneDay";
        public const string IRM_DateBegin = "DateBegin";
        public const string IRM_DateEnd = "DateEnd";

        public const string MR_AggregateByNalogOrgan = "AggregateByNalogOrgan";
        public const string MR_FederalDistrict = "FederalDistrict";
        public const string MR_TaxPayerRegionCode = "TaxPayerRegionCode";
        public const string MR_NalogOrganCode = "NalogOrganCode";
        public const string MR_DateOneDay = "DateOneDay";
    }
}
