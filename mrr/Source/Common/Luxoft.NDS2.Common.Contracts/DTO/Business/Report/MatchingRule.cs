﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Report
{
    [Serializable]
    public class MatchingRule
    {
        [DisplayName("Группа правил сопоставления")]
        [Description("Группа правил сопоставления")]
        public string RuleGroupCaption { get; set; }

        [DisplayName("Правило сопоставления")]
        [Description("Правило сопоставления")]
        public string RuleNumberCaption { get; set; }

        [DisplayName("Комментарий к правилу")]
        [Description("Комментарий к правилу")]
        public string RuleNumberComment { get; set; }

        [DisplayName("Количество сопоставлений")]
        [Description("Количество сопоставлений")]
        public long MatchCount { get; set; }

        [DisplayName("Сумма НДС по сопоставленным записям")]
        [Description("Сумма НДС по сопоставленным записям")]
        public decimal MatchAmount { get; set; }

        [DisplayName("Доля в общем числе сопоставлений по количеству")]
        [Description("Доля в общем числе сопоставлений по количеству")]
        public decimal? PercentByCount { get; set; }

        [DisplayName("Доля в общем числе сопоставлений по сумме")]
        [Description("Доля в общем числе сопоставлений по сумме")]
        public decimal? PercentByAmount { get; set; }

        [DisplayName("Доля СФ с ошибкой ЛК среди входящих СФ")]
        [Description("Доля СФ с ошибкой ЛК среди входящих СФ")]
        public decimal? PercentInnerInvoiceErrorLC { get; set; }

        [DisplayName("Доля СФ с ошибкой ЛК среди исходящих СФ")]
        [Description("Доля СФ с ошибкой ЛК среди исходящих СФ")]
        public decimal? PercentOuterInvoiceErrorLC { get; set; }

        [DisplayName("Доля СФ с ошибкой ЛК и среди входящих СФ и среди исходящих")]
        [Description("Доля СФ с ошибкой ЛК и среди входящих СФ и среди исходящих")]
        public decimal? PercentInnerOuterInvoiceErrorLC { get; set; }
    }
}
