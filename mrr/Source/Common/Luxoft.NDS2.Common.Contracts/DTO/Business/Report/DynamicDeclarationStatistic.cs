﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Report
{
    [Serializable]
    public class DynamicDeclarationStatistic
    {
        public string Period { get; set; }

        public string P01 { get; set; }
        public string P02 { get; set; }
        public string P03 { get; set; }
        public string P04 { get; set; }
        public string P05 { get; set; }
        public string P06 { get; set; }
        public string P07 { get; set; }
        public string P08 { get; set; }
        public string P09 { get; set; }
        public string P10 { get; set; }
        public string P11 { get; set; }
        public string P12 { get; set; }
        public string P13 { get; set; }
        public string P14 { get; set; }
    }

    [Serializable]
    public class DynamicParamDeclStat
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Check { get; set; }
        public string FullName { get { return Name; } }
    }
}
