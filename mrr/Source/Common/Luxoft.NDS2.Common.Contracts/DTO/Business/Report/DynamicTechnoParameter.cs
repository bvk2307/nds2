﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Report
{
    [Serializable]
    public class DynamicTechnoParameter
    {
        public string Period { get; set; }

        public string P01 { get; set; }
        public string P02 { get; set; }
        public string P03 { get; set; }
        public string P04 { get; set; }
        public string P05 { get; set; }
        public string P06 { get; set; }
        public string P07 { get; set; }
        public string P08 { get; set; }
        public string P09 { get; set; }
        public string P10 { get; set; }
        public string P11 { get; set; }
        public string P12 { get; set; }
        public string P13 { get; set; }
        public string P14 { get; set; }
        public string P15 { get; set; }
        public string P16 { get; set; }
        public string P17 { get; set; }
        public string P18 { get; set; }
        public string P19 { get; set; }
        public string P20 { get; set; }
        public string P21 { get; set; }
        public string P22 { get; set; }
        public string P23 { get; set; }
        public string P24 { get; set; }
        public string P25 { get; set; }
        public string P26 { get; set; }
        public string P27 { get; set; }
        public string P28 { get; set; }
        public string P29 { get; set; }
        public string P30 { get; set; }
    }
}
