﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Report
{
    [Serializable]
    public class InspectorMonitoringWork
    {
        #region Общие параметры

        [DisplayName("Федеральный округ")]
        [Description("Федеральный округ")]
        public string FederalDistrict { get; set; }

        [DisplayName("Регион")]
        [Description("Регион")]
        public string Region { get; set; }

        [DisplayName("Инспекция/МРИ")]
        [Description("Инспекция/МРИ")]
        public string Inspection { get; set; }

        [DisplayName("Ответственный инспектор")]
        [Description("Ответственный инспектор")]
        public string Inspector { get; set; }

        #endregion

        #region Не устраненные расхождения, направленные НП на [с]

        [DisplayName("Общее количество расхождений")]
        [Description("Общее количество расхождений")]
        public long UnrepairedDiscrepancyGeneralCount { get; set; }

        [DisplayName("Общая сумма расхождений")]
        [Description("Общая сумма расхождений")]
        public decimal UnrepairedDiscrepancyGeneralSum { get; set; }

        [DisplayName("Количество расхождений по валюте")]
        [Description("Количество расхождений по валюте")]
        public long UnrepairedDiscrepancyCurrencyCount { get; set; }

        [DisplayName("Сумма расхождений по валюте")]
        [Description("Сумма расхождений по валюте")]
        public decimal UnrepairedDiscrepancyCurrencySum { get; set; }

        [DisplayName("Количество расхождений по проверке НДС")]
        [Description("Количество расхождений по проверке НДС")]
        public long UnrepairedDiscrepancyNDSCount { get; set; }

        [DisplayName("Сумма расхождений по проверке НДС")]
        [Description("Сумма расхождений по проверке НДС")]
        public decimal UnrepairedDiscrepancyNDSSum { get; set; }

        [DisplayName("Количество неточных сопоставлений")]
        [Description("Количество неточных сопоставлений")]
        public long UnrepairedDiscrepancyInexactComparisonCount { get; set; }

        [DisplayName("Сумма расхождений по неточным сопоставлениям")]
        [Description("Сумма расхождений по неточным сопоставлениям")]
        public decimal UnrepairedDiscrepancyInexactComparisonSum { get; set; }

        [DisplayName("Количество разрывов")]
        [Description("Количество разрывов")]
        public long UnrepairedDiscrepancyBreakCount { get; set; }

        [DisplayName("Сумма расхождений по разрывам")]
        [Description("Сумма расхождений по разрывам")]
        public decimal UnrepairedDiscrepancyBreakSum { get; set; }

        #endregion

        #region Расхождения выявленные на [c] и НЕ УСТРАНЕННЫЕ на [по]

        [DisplayName("Общее количество расхождений")]
        [Description("Общее количество расхождений")]
        public long IdentifiedAndUnrepairedDiscrepancyGeneralCount { get; set; }

        [DisplayName("Общая сумма расхождений")]
        [Description("Общая сумма расхождений")]
        public decimal IdentifiedAndUnrepairedDiscrepancyGeneralSum { get; set; }

        [DisplayName("Количество расхождений по валюте")]
        [Description("Количество расхождений по валюте")]
        public long IdentifiedAndUnrepairedDiscrepancyCurrencyCount { get; set; }

        [DisplayName("Сумма расхождений по валюте")]
        [Description("Сумма расхождений по валюте")]
        public decimal IdentifiedAndUnrepairedDiscrepancyCurrencySum { get; set; }

        [DisplayName("Количество расхождений по проверке НДС")]
        [Description("Количество расхождений по проверке НДС")]
        public long IdentifiedAndUnrepairedDiscrepancyNDSCount { get; set; }

        [DisplayName("Сумма расхождений по проверке НДС")]
        [Description("Сумма расхождений по проверке НДС")]
        public decimal IdentifiedAndUnrepairedDiscrepancyNDSSum { get; set; }

        [DisplayName("Количество неточных сопоставлений")]
        [Description("Количество неточных сопоставлений")]
        public long IdentifiedAndUnrepairedDiscrepancyInexactComparisonCount { get; set; }

        [DisplayName("Сумма расхождений по неточным сопоставлениям")]
        [Description("Сумма расхождений по неточным сопоставлениям")]
        public decimal IdentifiedAndUnrepairedDiscrepancyInexactComparisonSum { get; set; }

        [DisplayName("Количество разрывов")]
        [Description("Количество разрывов")]
        public long IdentifiedAndUnrepairedDiscrepancyBreakCount { get; set; }

        [DisplayName("Сумма расхождений по разрывам")]
        [Description("Сумма расхождений по разрывам")]
        public decimal IdentifiedAndUnrepairedDiscrepancyBreakSum { get; set; }

        #endregion

        #region Расхождения выявленные на [c] и УСТРАНЕННЫЕ на [по]

        [DisplayName("Общее количество расхождений")]
        [Description("Общее количество расхождений")]
        public long IdentifiedAndRepairedDiscrepancyGeneralCount { get; set; }

        [DisplayName("Общая сумма расхождений")]
        [Description("Общая сумма расхождений")]
        public decimal IdentifiedAndRepairedDiscrepancyGeneralSum { get; set; }

        [DisplayName("Количество расхождений по валюте")]
        [Description("Количество расхождений по валюте")]
        public long IdentifiedAndRepairedDiscrepancyCurrencyCount { get; set; }

        [DisplayName("Сумма расхождений по валюте")]
        [Description("Сумма расхождений по валюте")]
        public decimal IdentifiedAndRepairedDiscrepancyCurrencySum { get; set; }

        [DisplayName("Количество расхождений по проверке НДС")]
        [Description("Количество расхождений по проверке НДС")]
        public long IdentifiedAndRepairedDiscrepancyNDSCount { get; set; }

        [DisplayName("Сумма расхождений по проверке НДС")]
        [Description("Сумма расхождений по проверке НДС")]
        public decimal IdentifiedAndRepairedDiscrepancyNDSSum { get; set; }

        [DisplayName("Количество неточных сопоставлений")]
        [Description("Количество неточных сопоставлений")]
        public long IdentifiedAndRepairedDiscrepancyInexactComparisonCount { get; set; }

        [DisplayName("Сумма расхождений по неточным сопоставлениям")]
        [Description("Сумма расхождений по неточным сопоставлениям")]
        public decimal IdentifiedAndRepairedDiscrepancyInexactComparisonSum { get; set; }

        [DisplayName("Количество разрывов")]
        [Description("Количество разрывов")]
        public long IdentifiedAndRepairedDiscrepancyBreakCount { get; set; }

        [DisplayName("Сумма расхождений по разрывам")]
        [Description("Сумма расхождений по разрывам")]
        public decimal IdentifiedAndRepairedDiscrepancyBreakSum { get; set; }

        #endregion
    }
}
