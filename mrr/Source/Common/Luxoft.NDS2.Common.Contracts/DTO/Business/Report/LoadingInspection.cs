﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Report
{
    [Serializable]
    public class LoadingInspection
    {
        #region Общие параметры

        [DisplayName("Федеральный округ")]
        [Description("Федеральный округ")]
        public string FederalDistrict { get; set; }

        [DisplayName("Регион")]
        [Description("Регион")]
        public string Region { get; set; }

        [DisplayName("Инспекция/МРИ")]
        [Description("Инспекция/МРИ")]
        public string Inspection { get; set; }

        [DisplayName("Количество инспекторов")]
        [Description("Количество инспекторов")]
        public string InspectorCount { get; set; }

        #endregion

        #region Всего расхождений

        [DisplayName("Количество")]
        [Description("Количество")]
        public long DiscrepancyGeneralCount { get; set; }

        [DisplayName("Общая сумма")]
        [Description("Общая сумма")]
        public decimal DiscrepancyGeneralSum { get; set; }

        #endregion

        #region Расхождения, отправленные на отработку

        [DisplayName("Количество")]
        [Description("Количество")]
        public long SentToWorkDiscrepancyCount { get; set; }

        [DisplayName("Общая сумма")]
        [Description("Общая сумма")]
        public decimal SentToWorkDiscrepancySum { get; set; }

        [DisplayName("Доля по количеству от всех расхождений")]
        [Description("Доля по количеству от всех расхождений")]
        public decimal? SentToWorkDiscrepancyPercentage { get; set; }

        [DisplayName("Доля по сумме от всех расхождений")]
        [Description("Доля по сумме от всех расхождений")]
        public decimal? SentToWorkDiscrepancyPercentageSum { get; set; }

        #endregion

        #region Устраненные расхождения

        [DisplayName("Количество")]
        [Description("Количество")]
        public long RepairedDiscrepancyCount { get; set; }

        [DisplayName("Общая сумма")]
        [Description("Общая сумма")]
        public decimal RepairedDiscrepancySum { get; set; }

        [DisplayName("Доля по количеству от расхождений, отправленных на отработку")]
        [Description("Доля по количеству от расхождений, отправленных на отработку")]
        public decimal? RepairedDiscrepancyPercentage { get; set; }

        [DisplayName("Доля по сумме от расхождений, отправленных на отработку")]
        [Description("Доля по сумме от расхождений, отправленных на отработку")]
        public decimal? RepairedDiscrepancyPercentageSum { get; set; }

        #endregion

        #region неустраненные расхождения (из отправленных)

        [DisplayName("Количество")]
        [Description("Количество")]
        public long UnrepairedDiscrepancyCount { get; set; }

        [DisplayName("Общая сумма")]
        [Description("Общая сумма")]
        public decimal UnrepairedDiscrepancySum { get; set; }

        #endregion
    }
}
