﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Report
{
    [Serializable]
    public class MonitoringProcessingDeclaration
    {
        #region Общие параметры

        [DisplayName("Федеральный округ")]
        [Description("Федеральный округ")]
        public string FederalDistrict { get; set; }

        [DisplayName("Регион")]
        [Description("Регион")]
        public string Region { get; set; }

        [DisplayName("Инспекция/МРИ")]
        [Description("Инспекция/МРИ")]
        public string Inspection { get; set; }

        [DisplayName("Количество поданных уточнений")]
        [Description("Количество поданных уточнений")]
        public int TakeUpdatesCount { get; set; }

        #endregion

        #region Сведения о налогоплательщике

        [DisplayName("ИНН")]
        [Description("ИНН")]
        public string TaxpayerINN { get; set; }

        [DisplayName("КПП")]
        [Description("КПП")]
        public string TaxpayerKPP { get; set; }

        [DisplayName("Наименование")]
        [Description("Наименование")]
        public string TaxpayerName { get; set; }
        
        #endregion

        [DisplayName("Ответственный инспектор")]
        [Description("Ответственный инспектор")]
        public string Inspector { get; set; }


        #region Не устраненные расхождения на [с]

        [DisplayName("Общее количество расхождений")]
        [Description("Общее количество расхождений")]
        public long UnrepairedDiscrepancyFromGeneralCount { get; set; }

        [DisplayName("Общая сумма расхождений")]
        [Description("Общая сумма расхождений")]
        public decimal UnrepairedDiscrepancyFromGeneralSum { get; set; }

        [DisplayName("Количество ошибок по ЛК")]
        [Description("Количество ошибок по ЛК")]
        public int UnrepairedDiscrepancyFromErrorLKCount { get; set; }

        [DisplayName("Количество расхождений по валюте")]
        [Description("Количество расхождений по валюте")]
        public long UnrepairedDiscrepancyFromCurrencyCount { get; set; }

        [DisplayName("Сумма расхождений по валюте")]
        [Description("Сумма расхождений по валюте")]
        public decimal UnrepairedDiscrepancyFromCurrencySum { get; set; }

        [DisplayName("Количество расхождений при проверке НДС")]
        [Description("Количество расхождений при проверке НДС")]
        public long UnrepairedDiscrepancyFromNDSCount { get; set; }

        [DisplayName("Сумма расхождений по проверке НДС")]
        [Description("Сумма расхождений по проверке НДС")]
        public decimal UnrepairedDiscrepancyFromNDSSum { get; set; }

        [DisplayName("Количество неточных сопоставлений")]
        [Description("Количество неточных сопоставлений")]
        public long UnrepairedDiscrepancyFromInexactComparisonCount { get; set; }

        [DisplayName("Сумма расхождений по неточным сопоставлениям")]
        [Description("Сумма расхождений по неточным сопоставлениям")]
        public decimal UnrepairedDiscrepancyFromInexactComparisonSum { get; set; }

        [DisplayName("Количество разрывов")]
        [Description("Количество разрывов")]
        public long UnrepairedDiscrepancyFromBreakCount { get; set; }

        [DisplayName("Сумма расхождений по разрывам")]
        [Description("Сумма расхождений по разрывам")]
        public decimal UnrepairedDiscrepancyFromBreakSum { get; set; }        

        #endregion

        #region Не устраненные расхождения на [по]

        [DisplayName("Общее количество расхождений")]
        [Description("Общее количество расхождений")]
        public long UnrepairedDiscrepancyToGeneralCount { get; set; }

        [DisplayName("Общая сумма расхождений")]
        [Description("Общая сумма расхождений")]
        public decimal UnrepairedDiscrepancyToGeneralSum { get; set; }

        [DisplayName("Количество ошибок по ЛК")]
        [Description("Количество ошибок по ЛК")]
        public int UnrepairedDiscrepancyToErrorLKCount { get; set; }

        [DisplayName("Количество расхождений по валюте")]
        [Description("Количество расхождений по валюте")]
        public long UnrepairedDiscrepancyToCurrencyCount { get; set; }

        [DisplayName("Сумма расхождений по валюте")]
        [Description("Сумма расхождений по валюте")]
        public decimal UnrepairedDiscrepancyToCurrencySum { get; set; }

        [DisplayName("Количество расхождений при проверке НДС")]
        [Description("Количество расхождений при проверке НДС")]
        public long UnrepairedDiscrepancyToNDSCount { get; set; }

        [DisplayName("Сумма расхождений по проверке НДС")]
        [Description("Сумма расхождений по проверке НДС")]
        public decimal UnrepairedDiscrepancyToNDSSum { get; set; }

        [DisplayName("Количество неточных сопоставлений")]
        [Description("Количество неточных сопоставлений")]
        public long UnrepairedDiscrepancyToInexactComparisonCount { get; set; }

        [DisplayName("Сумма расхождений по неточным сопоставлениям")]
        [Description("Сумма расхождений по неточным сопоставлениям")]
        public decimal UnrepairedDiscrepancyToInexactComparisonSum { get; set; }

        [DisplayName("Количество разрывов")]
        [Description("Количество разрывов")]
        public long UnrepairedDiscrepancyToBreakCount { get; set; }

        [DisplayName("Сумма расхождений по разрывам")]
        [Description("Сумма расхождений по разрывам")]
        public decimal UnrepairedDiscrepancyToBreakSum { get; set; }

        #endregion

        #region Устраненные расхождения с [c] по [по]

        [DisplayName("Общее количество расхождений")]
        [Description("Общее количество расхождений")]
        public long RepairedDiscrepancyGeneralCount { get; set; }

        [DisplayName("Общая сумма расхождений")]
        [Description("Общая сумма расхождений")]
        public decimal RepairedDiscrepancyGeneralSum { get; set; }

        [DisplayName("Количество расхождений по валюте")]
        [Description("Количество расхождений по валюте")]
        public long RepairedDiscrepancyCurrencyCount { get; set; }

        [DisplayName("Сумма расхождений по валюте")]
        [Description("Сумма расхождений по валюте")]
        public decimal RepairedDiscrepancyCurrencySum { get; set; }

        [DisplayName("Количество расхождений при проверке НДС")]
        [Description("Количество расхождений при проверке НДС")]
        public long RepairedDiscrepancyNDSCount { get; set; }

        [DisplayName("Сумма расхождений по проверке НДС")]
        [Description("Сумма расхождений по проверке НДС")]
        public decimal RepairedDiscrepancyNDSSum { get; set; }

        [DisplayName("Количество неточных сопоставлений")]
        [Description("Количество неточных сопоставлений")]
        public long RepairedDiscrepancyInexactComparisonCount { get; set; }

        [DisplayName("Сумма расхождений по неточным сопоставлениям")]
        [Description("Сумма расхождений по неточным сопоставлениям")]
        public decimal RepairedDiscrepancyInexactComparisonSum { get; set; }

        [DisplayName("Количество разрывов")]
        [Description("Количество разрывов")]
        public long RepairedDiscrepancyBreakCount { get; set; }

        [DisplayName("Сумма расхождений по разрывам")]
        [Description("Сумма расхождений по разрывам")]
        public decimal RepairedDiscrepancyBreakSum { get; set; }

        #endregion
    }
}
