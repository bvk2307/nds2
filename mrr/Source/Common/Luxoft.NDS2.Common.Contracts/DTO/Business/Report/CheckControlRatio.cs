﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Report
{
    [Serializable]
    public class CheckControlRatio
    {
        [DisplayName("Номер КС")]
        [Description("Номер КС")]
        public string Num { get; set; }

        [DisplayName("Описание соотношения")]
        [Description("Описание соотношения")]
        public string Description { get; set; }

        [DisplayName("Количество расхождений")]
        [Description("Количество расхождений")]
        public long CountDiscrepancy { get; set; }
    }
}
