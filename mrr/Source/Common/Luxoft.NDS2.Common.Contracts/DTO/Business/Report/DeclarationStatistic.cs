﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Report
{
    [Serializable]
    public class DeclarationStatistic
    {
        [DisplayName("Федеральный округ")]
        [Description("Федеральный округ")]
        [Display(Name = "NAME_GROUP")]
        public string NameGroup { get; set; }

        //--- Количество деклараций

        [DisplayName("Всего")]
        [Description("Всего")]
        [Display(Name = "DECL_COUNT_TOTAL")]
        public long? DeclCountTotal { get; set; }

        [DisplayName("Нулевых")]
        [Description("Нулевых")]
        [Display(Name = "DECL_COUNT_NULL")]
        public long? DeclCountNull { get; set; }

        [DisplayName("К возмещению")]
        [Description("К возмещению")]
        [Display(Name = "DECL_COUNT_COMPENSATION")]
        public long? DeclCountCompensation { get; set; }

        [DisplayName("К уплате")]
        [Description("К уплате")]
        [Display(Name = "DECL_COUNT_PAYMENT")]
        public long? DeclCountPayment { get; set; }

        [DisplayName("Не представленные декларации(но имеется ссылка в декларации другого налогопалательщика)")]
        [Description("Не представленные декларации(но имеется ссылка в декларации другого налогопалательщика)")]
        [Display(Name = "DECL_COUNT_NOT_SUBMIT")]
        public long? DeclCountNotSubmit { get; set; }

        [DisplayName("Подано уточненных деклараций всего")]
        [Description("Подано уточненных деклараций всего")]
        [Display(Name = "DECL_COUNT_REVISED_TOTAL")]
        public long? DeclCountRevisedTotal { get; set; }

        //----- По данным деклараций

        //----- Налоговая база 
        [DisplayName("В декларациях к возмещению")]
        [Description("В декларациях к возмещению")]
        [Display(Name = "TAXBASE_SUM_COMPENSATION")]
        public decimal? TaxBaseSumCompensation { get; set; }

        [DisplayName("В декларациях к уплате")]
        [Description("В декларациях к уплате")]
        [Display(Name = "TAXBASE_SUM_PAYMENT")]
        public decimal? TaxBaseSumPayment { get; set; }

        //----- Сумма исчисленного НДС с налоговой бызы 
        [DisplayName("В декларациях к возмещению")]
        [Description("В декларациях к возмещению")]
        [Display(Name = "NDSCALCULATED_SUM_COMPENSATION")]
        public decimal? NdsCalculatedSumCompensation { get; set; }

        [DisplayName("В декларациях к уплате")]
        [Description("В декларациях к уплате")]
        [Display(Name = "NDSCALCULATED_SUM_PAYMENT")]
        public decimal? NdsCalculatedSumPayment { get; set; }

        //----- Сумма вычетов по НДС 
        [DisplayName("В декларациях к возмещению")]
        [Description("В декларациях к возмещению")]
        [Display(Name = "NDSDEDUCTION_SUM_COMPENSATION")]
        public decimal? NdsDeductionSumCompensation { get; set; }

        [DisplayName("В декларациях к уплате")]
        [Description("В декларациях к уплате")]
        [Display(Name = "NDSDEDUCTION_SUM_PAYMENT")]
        public decimal? NdsDeductionSumPayment { get; set; }


        [DisplayName("Сумма НДС к возмещению")]
        [Description("Сумма НДС к возмещению")]
        [Display(Name = "NDS_SUM_COMPENSATION")]
        public decimal? NdsSumCompensation { get; set; }

        [DisplayName("Сумма НДС к уплате")]
        [Description("Сумма НДС к уплате")]
        [Display(Name = "NDS_SUM_PAYMENT")]
        public decimal? NdsSumPayment { get; set; }

        public static Dictionary<string, string> GetDisplayNameKeyDictionary()
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            PropertyInfo[] props = typeof(DeclarationStatistic).GetProperties();
            foreach (var prop in props)
            {
                object[] attrs = prop.GetCustomAttributes(true);
                foreach (var attr in attrs)
                {
                    DisplayAttribute displayAttr = attr as DisplayAttribute;
                    if (displayAttr != null)
                    {
                        dict.Add(prop.Name, displayAttr.Name);
                    }
                }
            }
            return dict;
        }
    }
}
