﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Report
{
    [Serializable]
    public enum DynamicModuleType
    {
        All = 0,
        GP_3 = 1,
        MC = 2,
        SOV = 3,
        MRR = 4
    }

    [Serializable]
    public class DynamicModule
    {
        public DynamicModuleType Type { get; set; }
        public string Name { get; set; }
    }
}
