﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Report
{
    [Serializable]
    public class DynamicParameter
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Check { get; set; }
        public string FullName { get { return Name; } }
        public DynamicModuleType ModuleType { get; set; }
        public string ModuleName { get; set; }
    }
}
