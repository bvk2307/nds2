﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Report
{
    [Serializable]
    public class ReportInvoice
    {
        public bool IsNew()
        {
            return Id < 1;
        }

        [DisplayName("Номер")]
        [Description("Номер счета фактуры")]
        public long Id { set; get; }

        [DisplayName("Код")]
        [Description("Код вида операции")]
        public string OperationCode { get; set; }

        [DisplayName("Название")]
        [Description("Название вида операции")]
        public string OperationName { get; set; }

        [DisplayName("Кол-во записей о СФ")]
        [Description("Количество записей о СФ")]
        public long RecordSFCount { get; set; }

        [DisplayName("Кол-во выявленных расхождений")]
        [Description("Количество выявленных расхождений")]
        public long IdentifiedDifferencesCount { get; set; }

        [DisplayName("Кол-во устраненных расхождений")]
        [Description("Количество устраненных расхождений")]
        public long ResolvedDifferencesCount { get; set; }

        [DisplayName("ЛК: Кол-во ошибок")]
        [Description("Количество ошибок логического контроля")]
        public long ErrorLogicalControlCount { get; set; }

        [DisplayName("ЛК: Кол-во устраненных ошибок")]
        [Description("Количество устраненных ошибок логического контроля")]
        public long ResolvedErrorLogicalControlCount { get; set; }

        [DisplayName("Валюта: Кол-во расхождений")]
        [Description("Количество расхождений по валюте")]
        public long CurrencyDifferencesCount { get; set; }

        [DisplayName("Валюта: Кол-во устраненных расхождений")]
        [Description("Количество устраненных расхождений по валюте")]
        public long ResolvedCurrencyDifferencesCount { get; set; }

        [DisplayName("Валюта: Сумма устраненых расхождений")]
        [Description("Сумма устраненных расхождений по валюте")]
        public decimal SummaResolvedCurrencyDifferencesCount { get; set; }

        [DisplayName("Проверка НДС: Кол-во расхождений")]
        [Description("Количество расхождений при провекри НДС")]
        public long CheckNDSDifferencesCount { get; set; }

        [DisplayName("Проверка НДС: Сумма расхождений")]
        [Description("Сумма расхождений при проверки НДС")]
        public decimal SummaCheckNDSDifferencesCount { get; set; }

        public static ReportInvoice New()
        {
            return new ReportInvoice();
        }
    }
}
