﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Report
{
    [Serializable]
    public class InfoResultsOfMatching
    {
        public string AggregateName { get; set; }

        //---- Количество представленных деклараций
        [DisplayName("Первичные декларации")]
        [Description("Первичные декларации")]
        public long SubmittingDeclarationPrimaryCount { get; set; }

        [DisplayName("Уточнённые декларации")]
        [Description("Уточнённые декларации")]
        public long SubmittingDeclarationCorrectCount { get; set; }

        [DisplayName("В т.ч. представленных по посредническим операциям")]
        [Description("В т.ч. представленных по посредническим операциям")]
        public long SubmittingDeclarationBrokeringCount { get; set; }
        //------------------------------------------------------------

        [DisplayName("Количество представленных журналов счет-фактур")]
        [Description("Количество представленных журналов счет-фактур")]
        public long SubmittingJournalCount { get; set; }

        //---- Количество операций в декларациях
        [DisplayName("Всего")]
        [Description("Всего")]
        public long CountOperationInDeclarationTotal { get; set; }

        [DisplayName("в т.ч. по данным раздела 8")]
        [Description("в т.ч. по данным раздела 8")]
        public long CountOperationInDeclarationR08 { get; set; }

        [DisplayName("в т.ч. по данным раздела 9")]
        [Description("в т.ч. по данным раздела 9")]
        public long CountOperationInDeclarationR09 { get; set; }

        [DisplayName("в т.ч. по данным раздела 10")]
        [Description("в т.ч. по данным раздела 10")]
        public long CountOperationInDeclarationR10 { get; set; }

        [DisplayName("в т.ч. по данным раздела 11")]
        [Description("в т.ч. по данным раздела 11")]
        public long CountOperationInDeclarationR11 { get; set; }

        [DisplayName("в т.ч. по данным раздела 12")]
        [Description("в т.ч. по данным раздела 12")]
        public long CountOperationInDeclarationR12 { get; set; }
        //------------------------------------------------------------

        [DisplayName("Количество точных сопоставлений")]
        [Description("Количество точных сопоставлений")]
        public long CountExactDiscrepancy { get; set; }

        //----- Всего деклараций с расхождениями  
        [DisplayName("первичные декларации")]
        [Description("первичные декларации")]
        public long TotalDeclarationDiscrepancyPrimary { get; set; }

        [DisplayName("уточнённые декларации")]
        [Description("уточнённые декларации")]
        public long TotalDeclarationDiscrepancyCorrection { get; set; }
        //------------------------------------------------------------

        [DisplayName("Количество деклараций с расхождениями по КС")]
        [Description("Количество деклараций с расхождениями по КС")]
        public long TotalDeclarationDiscrepancyControlRation { get; set; }

        //----- Выявлено расхождений
        [DisplayName("Всего – Кол-во")]
        [Description("Всего – Кол-во")]
        public long IdentDiscrepancyTotalCount { get; set; }

        [DisplayName("Всего – Сумма")]
        [Description("Всего – Сумма")]
        public decimal IdentDiscrepancyTotalAmount { get; set; }

        [DisplayName("Неточное сопоставление – При завышении вычета над исчисленным НДС с НБ НДС - Кол-во")]
        [Description("Неточное сопоставление – При завышении вычета над исчисленным НДС с НБ НДС - Кол-во")]
        public long IdentNotExactDiscrepancyOverflowCount { get; set; }

        [DisplayName("Неточное сопоставление – При завышении вычета над исчисленным НДС с НБ НДС – Сумма")]
        [Description("Неточное сопоставление – При завышении вычета над исчисленным НДС с НБ НДС – Сумма")]
        public decimal IdentNotExactDiscrepancyOverflowAmount { get; set; }

        [DisplayName("Неточное сопоставление – Без завышения вычета над исчисленным НДС с НБ НДС - Кол-во")]
        [Description("Неточное сопоставление – Без завышения вычета над исчисленным НДС с НБ НДС - Кол-во")]
        public long IdentNotExactDiscrepancyNotOverflowCount { get; set; }

        [DisplayName("Точное сопоставление – При завышении вычета над исчисленным НДС с НБ НДС - Кол-во")]
        [Description("Точное сопоставление – При завышении вычета над исчисленным НДС с НБ НДС - Кол-во")]
        public long IdentExactDiscrepancyOverflowCount { get; set; }

        [DisplayName("Точное сопоставление – При завышении вычета над исчисленным НДС с НБ НДС – Сумма")]
        [Description("Точное сопоставление – При завышении вычета над исчисленным НДС с НБ НДС – Сумма")]
        public decimal IdentExactDiscrepancyOverflowAmount { get; set; }

        [DisplayName("Разрыв – Кол-во")]
        [Description("Разрыв – Кол-во")]
        public long IdentExactDiscrepancyBreakCount { get; set; }

        [DisplayName("Разрыв – Сумма")]
        [Description("Разрыв – Сумма")]
        public decimal IdentExactDiscrepancyBreakAmount { get; set; }

        [DisplayName("Валюта – Кол-во")]
        [Description("Валюта – Кол-во")]
        public long IdentExactDiscrepancyCurrencyCount { get; set; }

        [DisplayName("Валюта – Сумма")]
        [Description("Валюта – Сумма")]
        public decimal IdentExactDiscrepancyCurrencyAmount { get; set; }
        //------------------------------------------------------------

        [DisplayName("Удельный вес расхождений в общем кол-ве операций")]
        [Description("Удельный вес расхождений в общем кол-ве операций")]
        public decimal SpecificWeightDiscrepancyInTotalOperation { get; set; }

        //----- Расхождения, отправленные в ТНО 
        [DisplayName("Кол-во")]
        [Description("Кол-во")]
        public long DiscrepancySendToTNOCount { get; set; }

        [DisplayName("Сумма")]
        [Description("Сумма")]
        public decimal DiscrepancySendToTNOAmount { get; set; }
        //------------------------------------------------------------
    }
}
