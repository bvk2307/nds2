﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Report
{
    [Serializable]
    public class CheckLogicControl
    {
        [DisplayName("Номер проверки")]
        [Description("Номер проверки")]
        public string Num { get; set; }

        [DisplayName("Наименование проверки")]
        [Description("Наименование проверки")]
        public string Name { get; set; }

        [DisplayName("Количество ошибок")]
        [Description("Количество ошибок")]
        public long ErrorCount { get; set; }

        [DisplayName("Доля в общем числе ошибок по ЛК")]
        [Description("Доля в общем числе ошибок по ЛК")]
        public decimal? ErrorCountPercentage { get; set; }

        [DisplayName("Частота ошибки в неточно сопоставленных счетах-фактурах")]
        [Description("Частота ошибки в неточно сопоставленных счетах-фактурах")]
        public decimal? ErrorFrequencyInvoiceNotExact { get; set; }

        [DisplayName("Частота ошибки в счетах-фактурах с разрывами")]
        [Description("Частота ошибки в счетах-фактурах с разрывами")]
        public decimal? ErrorFrequencyInvoiceGap { get; set; }

        [DisplayName("Частота ошибки в счетах-фактурах с точным сопоставлением")]
        [Description("Частота ошибки в счетах-фактурах с точным сопоставлением")]
        public decimal? ErrorFrequencyInvoiceExact { get; set; }

        [DisplayName("Частота ошибки во входящих счетах-фактурах")]
        [Description("Частота ошибки во входящих счетах-фактурах")]
        public decimal? ErrorFrequencyInnerInvoice { get; set; }

        [DisplayName("Частота ошибки в исходящих счетах-фактурах")]
        [Description("Частота ошибки в исходящих счетах-фактурах")]
        public decimal? ErrorFrequencyOuterInvoice { get; set; }
    }
}
