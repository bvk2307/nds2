﻿using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Map
{
    [Serializable]
    public class MapRequestData
    {
        public string RegionId { get; set; }
		public string KppCode { get; set; }
        public MapType MapType { get; set; }
        public NdsType NdsType { get; set; }
        public MacroReportType ReportType { get; set; }
    }
    
}
