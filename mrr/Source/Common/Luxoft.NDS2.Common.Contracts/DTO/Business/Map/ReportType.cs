namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Map
{
    public enum MacroReportType
    {
        Nds = 0,
        Discrepancy = 1
    }
}