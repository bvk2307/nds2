﻿namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Map
{
    public enum NdsType
    {
        ShareDeductionsInComputingNds = 0,//Доля вычетов в исчислительном ндс
        AmountsOfComputingNds = 1,//Сумма исчислительного Ндс
        AmountOfDeductionsNds = 2,//Сумма вычетов по Ндс
        AmountNdsToBeRecovered = 3,//Сумма Ндс к возмещеию
        AmountNdsToPayment = 4,//Сумма Ндс к уплате
        NdsSaldo = 5,//НДС Сальдо
        SubmitDeclarationsForCompensation = 6,//Подано деклараций к возмещению
        SubmitDeclarationsToPayment = 7,//Подано деклараций к уплате

        SumOfAllDiscrepancy = 8,
        SumOfDiscrepancyInTears = 9,
        SumOfDiscrepancyInTearsAtInflatedDeduction = 10,
        ShareOfTransactionsWithDiscrepancy = 11,
        Tno = 12
    }
}
