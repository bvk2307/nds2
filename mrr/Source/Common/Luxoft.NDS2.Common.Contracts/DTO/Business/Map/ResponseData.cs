﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using Luxoft.NDS2.Common.Contracts.Annotations;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Map
{
    [Serializable]
    public class MapResponseDataNds
    {
        public string AGGREGATE_CODE { get; set; }
        public string AGGREGATE_NAME { get; set; }
        public int FISCAL_YEAR { get; set; }
        public int QUARTER { get; set; }
        public int DECL_COUNT_COMPENSATION { get; set; }
        public int DECL_COUNT_PAYMENT { get; set; }
        public decimal NDS_CALCULATED_SUM { get; set; }
        public decimal NDS_DEDUCTION_SUM { get; set; }
        public decimal NDS_COMPENSATION_SUM { get; set; }
        public decimal NDS_PAYMENT_SUM { get; set; }
        public decimal WEIGHT_DEDUCT_TO_CALC { get; set; }
    }

    [Serializable]
    public class MapResponseDataDiscrepancy
    {
        public string AGGREGATE_CODE { get; set; }
        public string AGGREGATE_NAME { get; set; }
        public int FISCAL_YEAR { get; set; }
        public int QUARTER { get; set; }
        public decimal MISMATCH_TOTAL_SUM { get; set; }
        public decimal MISMATCH_NDS { get; set; }
        public decimal GAP_AMOUNT { get; set; }
        public decimal MISMATCH_TNO_SUM { get; set; }
        public decimal WEIGHT_MISMATCH_IN_TOTAL_OPER { get; set; }
    }

    [DataContract]
    [Serializable]
    public class NpResponseData : INotifyPropertyChanged
    {
		public string INN { get; set; }
		public string KPP { get; set; }
		public string TAX_NAME { get; set; }
        public int TAX_YEAR { get; set; }
        public int QUARTER { get; set; }
        public string SONO_CODE{ get; set; }
        public long GAP_COUNT { get; set; }
        public decimal GAP_SUM { get; set; }
        public decimal NDS_CALCULATED { get; set; }
		public decimal NDS_DEDUCTION { get; set; }
        public decimal SHARE_DEDUCTIONS { get; set; }
		public decimal NDS_TOTAL { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
