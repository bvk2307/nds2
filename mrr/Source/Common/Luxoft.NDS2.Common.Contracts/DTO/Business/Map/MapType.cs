﻿namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Map
{
    public enum MapType
    {
        /// <summary>
        /// Страна
        /// </summary>
        Districts = 1,
        
        /// <summary>
        /// Округ
        /// </summary>
        Regions = 2,
        
        /// <summary>
        /// Область
        /// </summary>
        Sono = 3,
        
        /// <summary>
        /// Налоговая в области
        /// </summary>
        Np = 4,

        /// <summary>
        /// Граф связей (в другом окне)
        /// </summary>
        Graph = 5
    }
}
