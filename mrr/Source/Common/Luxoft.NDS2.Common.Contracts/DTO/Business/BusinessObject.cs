﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business
{
    [Serializable]
    public class BusinessObject 
    {

    }

    public static class BusinessObjectExtension
    {
        public static string GetPropertyName<T, TP>(this T obj, Expression<Func<T, TP>> expression) where T : class
        {
            return ((MemberExpression)expression.Body).Member.Name;
        }
    }

}
