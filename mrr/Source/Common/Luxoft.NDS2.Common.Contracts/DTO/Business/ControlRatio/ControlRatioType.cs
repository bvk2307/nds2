﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.ControlRatio
{
    [Serializable]
    public sealed class ControlRatioType
    {
        /// <summary>Код КС</summary>
        public string Code { get; set; }

        /// <summary>Формулировка нарушения</summary>
        public string Formulation { get; set; }

        /// <summary>Формула расчета – Условие</summary>
        public string CalculationCondition { get; set; }

        /// <summary>Формула расчета – Левая часть</summary>
        public string CalculationLeftSide { get; set; }

        /// <summary>Формула расчета – Правая часть</summary>
        public string CalculationRightSide { get; set; }

        /// <summary>Формула расчета – Знак неравенства</summary>
        public string CalculationOperator { get; set; }

        /// <summary>Описание соотношения</summary>
        public string Description { get; set; }
    }
}