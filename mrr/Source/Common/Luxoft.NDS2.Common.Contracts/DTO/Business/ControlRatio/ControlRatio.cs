﻿using System;
using System.ComponentModel;
using System.Data;
using Luxoft.NDS2.Common.Contracts.CustomAttributes;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.ControlRatio
{
    [Serializable]
    public sealed class ControlRatio
    {
        [DisplayName("Номер КС")]
        [Description("Номер КС")]
        public long Id { get; set; }

        [DisplayName("Код КС")]
        [Description("Код КС")]
        public string TypeCode { get { return GetTypePropertySafe(() => Type.Code); } }

        [DisplayName("Выявлено расхождение")]
        [Description("Выявлено расхождение")]
        public string HasDiscrepancyString { get; set; }


        [DisplayName("Формулировка нарушения")]
        [Description("Формулировка нарушения")]
        public string TypeFormulation { get { return GetTypePropertySafe(() => Type.Formulation); } }

        [DisplayName("Формула расчета")]
        [Description("Формула расчета")]
        public string TypeCalculationCondition { get { return GetTypePropertySafe(() => Type.CalculationCondition); } }

        [DisplayName("Дата расчета")]
        [Description("Дата расчета")]
        public DateTime? CalculationDate { get; set; }

        [DisplayName("Левая")]
        [Description("Рассчитанная левая часть (рубли)")]
        [CurrencyFormat]
        public decimal DisparityLeft { get; set; }

        [DisplayName("Правая")]
        [Description("Рассчитанная правая часть (рубли)")]
        [CurrencyFormat]
        public decimal DisparityRight { get; set; }

        public ControlRatioType Type { get; set; }

        public bool HasDiscrepancy { get; set; }

        /// <summary>Ссылка на декларацию</summary>
        public long? Decl_Version_Id { get; set; }

        /// <summary>Комментарий пользователя</summary>
        public string UserComment { get; set; }

        private T GetTypePropertySafe<T>(Func<T> get)
        {
            return Type == null ? default(T) : get();
        }
    }
}