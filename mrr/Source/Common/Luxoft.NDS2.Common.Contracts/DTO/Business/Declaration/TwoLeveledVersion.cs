﻿using System.Linq;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration
{
    /// <summary>
    /// Двухуровневая версия поданной декларации, например "5.04"
    /// </summary>
    public class TwoLeveledVersion
    {
        public int Major
        {
            get;
            set;
        }

        public int Minor
        {
            get;
            set;
        }

        public TwoLeveledVersion(string version)
        {
            var v = version.Split('.').Select(s =>
            {
                int i;
                return int.TryParse(s, out i) ? i : 0;
            }).ToArray();

            Major = v.Length > 0 ? v[0] : 0;
            Minor = v.Length > 1 ? v[1] : 0;
        }

        public bool OlderThan(TwoLeveledVersion version)
        {
            if (version == null)
                return false;

            return (Major < version.Major) || (Major == version.Major && Minor < version.Minor);
        }

        public bool NewerThan(TwoLeveledVersion version)
        {
            if (version == null)
                return false;

            return (Major > version.Major) || (Major == version.Major && Minor > version.Minor);
        }
    }

    public static class DeclarationFormatVersionConst
    {
        public const string FIVE_POINT_ZERO_FOUR = "5.04";
        public const string FIVE_POINT_ZERO_FIVE = "5.05";
    }
}
