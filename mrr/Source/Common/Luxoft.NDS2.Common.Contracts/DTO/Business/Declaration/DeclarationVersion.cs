﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration
{
    [Serializable]
    public class DeclarationVersion
    {
        public long DeclarationVersionId { get; set; }
        public string CorrectionNumber { get; set; }
        public string CorrectionNumberEffective { get; set; }
        public string CorrectionNumberCaption { get; set; }
        public int CorrectionNumberRank { get; set; }
        public string SounCode { get; set; }
        public string Kpp { get; set; }
        public DateTime? DeclDate { get; set; }
        public DateTime? SubmissionDate { get; set; }
        public bool IsAnnulmentSeod { get; set; }
        public bool IsAnnulment { get; set; }

        public void BuildCaption()
        {
            CorrectionNumberCaption = string.Format("{0}", CorrectionNumberEffective);
        }
    }
}
