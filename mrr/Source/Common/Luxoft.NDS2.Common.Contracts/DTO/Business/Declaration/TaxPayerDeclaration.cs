﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration
{
    /// <summary>
    /// Этот класс представляет объект передачи данных корректировки НД\Журнала для карточки НП
    /// </summary>
    [Serializable]
    public class TaxPayerDeclaration
    {
        public long Zip
        {
            get;
            set;
        }

        public string InnDeclarant
        {
            get;
            set;
        }

        public string KppEffectiveDeclarant
        {
            get;
            set;
        }

        public int? SurCode
        {
            get;
            set;
        }

        public string RegNumber
        {
            get;
            set;
        }

        public string TaxPeriod
        {
            get;
            set;
        }

        public int TypeCode
        {
            get;
            set;
        }

        public string TypeDescription
        {
            get;
            set;
        }

        public string KnpStatus
        {
            get;
            set;
        }

        public string CorrectionNumber
        {
            get;
            set;
        }

        public string Sign
        {
            get;
            set;
        }

        public decimal? NdsTotal
        {
            get;
            set;
        }

        public int? KnpDiscrepancyQuantity
        {
            get;
            set;
        }

        public int? DiscrepancyQuantity
        {
            get;
            set;
        }

        public bool? IsLarge
        {
            get;
            set;
        }

        public int? ControlRatioQuantity
        {
            get;
            set;
        }

        public string FiscalYear { get; set; }

        public string PeriodCode { get; set; }
    }
}
