﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration
{
    [Serializable]
    public class SelectionDeclaration
    {
        public long SelectionId { get; set; }
        /// <summary>
        /// Включена в выборку или нет
        /// </summary>
        public bool Included { get; set; }

        /// <summary>
        /// Zip выборки
        /// </summary>
        public long Zip { get; set; }

        /// <summary>
        /// Признак актуальности декларации
        /// </summary>
        public bool IsActual { get; set; }

        /// <summary>
        /// Тип : 1 - Журнал или 0 - Декларация
        /// </summary>
        public int TypeId { get; set; }

        public string Type { get; set; }

        /// <summary>
        /// СУР Налогоплательщика
        /// </summary>
        public int? Sur { get; set; }

        /// <summary>
        /// ИНН налогоплательщика
        /// </summary>
        public string Inn { get; set; }
        
        /// <summary>
        /// КПП налогоплательщика
        /// </summary>
        public string Kpp { get; set; }

        /// <summary>
        /// Название выборки
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// ИНН реорганизованный
        /// </summary>
        public string InnReorganized { get; set; }

        /// <summary>
        ///Регистрационный номер
        /// </summary>
        public int? RegNumber { get; set; }

        /// <summary>
        /// Признак НД
        /// </summary>
        public string DeclarationSign { get; set; }

        /// <summary>
        /// Сумма НДС
        /// </summary>
        public decimal? NdsAmount { get; set; }

        /// <summary>
        /// К-во расхождений
        /// </summary>
        public long? DiscrepancyCount { get; set; }

        /// <summary>
        /// Сумма расхождений по книгам покупок раздел 8
        /// </summary>
        public decimal? DiscrepancyAmountChapter8 { get; set; }

        /// <summary>
        /// Сумма расхождений по книгам покупок раздел 9 и 12
        /// </summary>
        public decimal? DiscrepancyAmountChapter9 { get; set; }

        /// <summary>
        /// Общая сумма расхождений 
        /// </summary>
        public decimal? DiscrepancyAmountTotal { get; set; }

        /// <summary>
        /// Минимальная сумма расхождений 
        /// </summary>
        public decimal? DiscrepancyAmountMin { get; set; }

        /// <summary>
        /// Максимальная сумма расхождений 
        /// </summary>
        public decimal? DiscrepancyAmountMax { get; set; }

        /// <summary>
        /// Средняя сумма расхождений 
        /// </summary>
        public decimal? DiscrepancyAmountAvg { get; set; }

        /// <summary>
        /// Отчетный период
        /// </summary>
        public string TaxPeriod { get; set; }

        public string TaxPeriodId { get; set; }

        /// <summary>
        /// Номер корректировки
        /// </summary>
        public string DeclarationVersion { get; set; }
        
        /// <summary>
        /// Дата подачи
        /// </summary>
        public DateTime? SubmitDate { get; set; }

       
        public string SonoCode { get; set; }
        
        public string SonoName { get; set; }
        
        public string RegionCode { get; set; }
        
        public string RegionName { get; set; }

        public string Inspector { get; set; }

        public int? StatusCode { get; set; }

        public DateTime? StatusDate { get; set; }

        public int? CalcStatus { get; set; }

        public bool Category { get; set; }

        public int ExcludeFromClaimType { get; set; }

        public string TaxPayer { get; set; }
    }

}
