﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration
{
    [Serializable]
    public class DeclarationRankedZip
    {
        /// <summary>
        /// ZIP декларации
        /// </summary>
        public long ZIP { get; set; }

        /// <summary>
        /// ранк актуальности
        /// у актуальной самый высокий
        /// </summary>
        public int RANK { get; set; }
    }
}
