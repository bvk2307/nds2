using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration
{
    public partial class ASKSumOper6
    {
        public decimal Id { get; set; }
        public Nullable<decimal> IdDekl { get; set; }
        public string KodOper { get; set; }
        public Nullable<long> NalBaza { get; set; }
        public Nullable<long> SumNal164 { get; set; }
        public Nullable<long> NalVychNePod { get; set; }
    }
}
