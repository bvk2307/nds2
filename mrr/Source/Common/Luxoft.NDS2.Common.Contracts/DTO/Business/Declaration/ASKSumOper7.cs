using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration
{
    public partial class ASKSumOper7
    {
        public decimal Id { get; set; }
        public Nullable<decimal> IdDekl { get; set; }
        public string KodOper { get; set; }
        public Nullable<long> StRealTov { get; set; }
        public Nullable<long> StPriobTov { get; set; }
        public Nullable<long> NalNeVych { get; set; }
    }
}
