﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration
{
    public enum ImpalaInvoiceChapterNumber
    {
        Chapter8 = 0,
        Chapter9 = 1,
        Chapter81 = 2,
        Chapter91 = 3,
        Chapter10 = 4,
        Chapter11 = 5,
        Chapter12 = 6,
        JournalChapter1 = 7,
        JournalChapter2 = 8,
    }

    public enum AskInvoiceChapterNumber
    {
        Chapter8 = 8,
        Chapter9 = 9,
        Chapter81 = 81,
        Chapter91 = 91,
        Chapter10 = 10,
        Chapter11 = 11,
        Chapter12 = 12
    }

    public enum DeclarationInvoiceChapterNumber
    {
        Chapter8 = 8,
        Chapter9,
        Chapter10,
        Chapter11,
        Chapter12,
        JournalChapter1,
        JournalChapter2
    }

    public enum DeclarationInvoicePartitionNumber
    {
        Chapter8 = 8,
        Chapter9,
        Chapter10,
        Chapter11,
        Chapter12,
        JournalChapter1,
        JournalChapter2,
        Chapter81 = 81,
        Chapter91 = 91
    }

    public enum InvoiceRowKeyPartitionNumber
    {
        Chapter8 = 0,
        Chapter81 = 2,
        Chapter9 = 1,
        Chapter91 = 3,
        Chapter10 = 4,
        Chapter11 = 5,
        Chapter12 = 6
    }

    public static class DeclarationInvoiceChapterNumberConverter
    {
        public static Dictionary<AskInvoiceChapterNumber, DeclarationInvoiceChapterNumber> AskToChapterMapping =
            new Dictionary<AskInvoiceChapterNumber, DeclarationInvoiceChapterNumber>
            {
                { AskInvoiceChapterNumber.Chapter10, DeclarationInvoiceChapterNumber.Chapter10 },
                { AskInvoiceChapterNumber.Chapter11, DeclarationInvoiceChapterNumber.Chapter11 },
                { AskInvoiceChapterNumber.Chapter12, DeclarationInvoiceChapterNumber.Chapter12 },
                { AskInvoiceChapterNumber.Chapter8, DeclarationInvoiceChapterNumber.Chapter8 },
                { AskInvoiceChapterNumber.Chapter81, DeclarationInvoiceChapterNumber.Chapter8 },
                { AskInvoiceChapterNumber.Chapter9, DeclarationInvoiceChapterNumber.Chapter9 },
                { AskInvoiceChapterNumber.Chapter91, DeclarationInvoiceChapterNumber.Chapter9 }
            };

        public static Dictionary<AskInvoiceChapterNumber, DeclarationInvoicePartitionNumber> AskToPartitionMapping =
            new Dictionary<AskInvoiceChapterNumber, DeclarationInvoicePartitionNumber>
            {
                { AskInvoiceChapterNumber.Chapter10, DeclarationInvoicePartitionNumber.Chapter10 },
                { AskInvoiceChapterNumber.Chapter11, DeclarationInvoicePartitionNumber.Chapter11 },
                { AskInvoiceChapterNumber.Chapter12, DeclarationInvoicePartitionNumber.Chapter12},
                { AskInvoiceChapterNumber.Chapter8, DeclarationInvoicePartitionNumber.Chapter8 },
                { AskInvoiceChapterNumber.Chapter81, DeclarationInvoicePartitionNumber.Chapter81 },
                { AskInvoiceChapterNumber.Chapter9, DeclarationInvoicePartitionNumber.Chapter9 },
                { AskInvoiceChapterNumber.Chapter91, DeclarationInvoicePartitionNumber.Chapter91 }
            };

        public static Dictionary<ImpalaInvoiceChapterNumber, DeclarationInvoicePartitionNumber> ImpalaToPartitionMapping =
            new Dictionary<ImpalaInvoiceChapterNumber, DeclarationInvoicePartitionNumber>
            {
                { ImpalaInvoiceChapterNumber.Chapter8, DeclarationInvoicePartitionNumber.Chapter8 },
                { ImpalaInvoiceChapterNumber.Chapter81, DeclarationInvoicePartitionNumber.Chapter81 },
                { ImpalaInvoiceChapterNumber.Chapter9, DeclarationInvoicePartitionNumber.Chapter9 },
                { ImpalaInvoiceChapterNumber.Chapter91, DeclarationInvoicePartitionNumber.Chapter91 },
                { ImpalaInvoiceChapterNumber.Chapter10, DeclarationInvoicePartitionNumber.Chapter10 },
                { ImpalaInvoiceChapterNumber.Chapter11, DeclarationInvoicePartitionNumber.Chapter11 },
                { ImpalaInvoiceChapterNumber.Chapter12, DeclarationInvoicePartitionNumber.Chapter12},
                { ImpalaInvoiceChapterNumber.JournalChapter1, DeclarationInvoicePartitionNumber.JournalChapter1},
                { ImpalaInvoiceChapterNumber.JournalChapter2, DeclarationInvoicePartitionNumber.JournalChapter2}
            };

        public static Dictionary<DeclarationInvoicePartitionNumber, ImpalaInvoiceChapterNumber> PartitionToImpalaMapping =
            new Dictionary<DeclarationInvoicePartitionNumber, ImpalaInvoiceChapterNumber>
            {
                { DeclarationInvoicePartitionNumber.Chapter8, ImpalaInvoiceChapterNumber.Chapter8 },
                { DeclarationInvoicePartitionNumber.Chapter81, ImpalaInvoiceChapterNumber.Chapter81 },
                { DeclarationInvoicePartitionNumber.Chapter9, ImpalaInvoiceChapterNumber.Chapter9 },
                { DeclarationInvoicePartitionNumber.Chapter91, ImpalaInvoiceChapterNumber.Chapter91 },
                { DeclarationInvoicePartitionNumber.Chapter10, ImpalaInvoiceChapterNumber.Chapter10 },
                { DeclarationInvoicePartitionNumber.Chapter11, ImpalaInvoiceChapterNumber.Chapter11 },
                { DeclarationInvoicePartitionNumber.Chapter12, ImpalaInvoiceChapterNumber.Chapter12},
                { DeclarationInvoicePartitionNumber.JournalChapter1, ImpalaInvoiceChapterNumber.JournalChapter1},
                { DeclarationInvoicePartitionNumber.JournalChapter2, ImpalaInvoiceChapterNumber.JournalChapter2}
            };

        public static AskInvoiceChapterNumber ToAskNumber(this DeclarationInvoicePartitionNumber partition)
        {
            return AskToPartitionMapping.First(pair => pair.Value == partition).Key;
        }

        public static DeclarationInvoicePartitionNumber ToBusinessNumber(this AskInvoiceChapterNumber askNumber)
        {
            return AskToPartitionMapping[askNumber];
        }

        public static DeclarationInvoicePartitionNumber ToBusinessNumber(this ImpalaInvoiceChapterNumber implalaNumber)
        {
            return ImpalaToPartitionMapping[implalaNumber];
        }

        public static ImpalaInvoiceChapterNumber ToImpalaNumber(this DeclarationInvoicePartitionNumber partition)
        {
            return PartitionToImpalaMapping[partition];
        }
    }    
}
