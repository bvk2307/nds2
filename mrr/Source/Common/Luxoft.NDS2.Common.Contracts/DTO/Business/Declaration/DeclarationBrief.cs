﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration.Enums;
using Luxoft.NDS2.Common.Contracts.DTO.Business.TaxPeriod;
using System;
using System.ComponentModel;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration
{
    // ReSharper disable InconsistentNaming
    [Serializable]
    public class DeclarationBrief : IDeclarationIdentity
    {
        /// <summary>
        /// идентификатор декларации (год+отчетный период+ИНН)
        /// </summary>
        public long ID { get; set; }

        /// <summary>
        /// Идентификатор версии дакларации
        /// </summary>
        public long DECLARATION_VERSION_ID { get; set; }

        public bool SELECTED { get; set; }
        public bool PROCESSED { get; set; }
    
        [DisplayName("Признак")]
        [Description("")]
        public string RECORD_MARK { get; set; }
        /*
        Т     – если есть автотребование/автоистребование по которому не получена квитанция о вручении НП в отведенный
              срок (т.е. срок ожидания вручения > «Отправлено налогоплательщику» + «Время ожидания вручения
              автотребования/автоистребования». Если методолог не указал время ожидания, то данный признак не устанаволивается)
        П     – если получено  на бумаге пояснение/ответ, но результат отработки инспектором не отправлен в МС
        Т-П   – если не получена квитанция по автотребованию/автоистребованию и есть не обработанные пояснения/ответы
        Н     – если получена новая декларация/журнал и/или новая версия декларации/журнала
        МНК   – если в декларации есть расхождение, которое перешло на этап «Прочие МНК»
        Пусто – если декларация/журнал не требуют вышеописанного внимания инспектора
        */

        [DisplayName("Признак значимых изменений")]
        [Description("Признак значимых изменений")]
        public DeclarationHasChangesType HAS_CHANGES { get; set; }

        [DisplayName("Р")]
        [Description("Сформировано автотребование по расхождениям")]
        public bool HAS_AT { get; set; }
        [DisplayName("АТ")]
        [Description("Кол-во ненаправленных требований о представлении пояснений")]
        public int SLIDE_AT_COUNT { get; set; }
        [DisplayName("П")]
        [Description("Кол-во пояснений для ввода")]
        public int NO_TKS_COUNT { get; set; }

        /// <summary>
        /// Стадия обработки декларации
        /// </summary>
        public DeclarationProcessignStage ProcessingStage { get; set; }

        [DisplayName("")]
        [Description("Статус загрузки")]
        public DeclarationProcessignStage LOAD_MARK { get; set; }

        [DisplayName("Тип")]
        [Description("Тип документа")]
        public string DECL_TYPE { get; set; }

        public bool CANCELLED { get; set; }

        [DisplayName("Аннул.")]
        [Description("Аннулировано")]
        public bool HAS_CANCELLED_CORRECTION { get; set; }
        
        public int DECL_TYPE_CODE { get; set; }

        [DisplayName("Расхождения")]
        [Description("Наличие расхождений для КНП")]
        public string STATUS { get; set; }
        [DisplayName("КНП")]
        [Description("Статус декларации")]
        public string STATUS_KNP { get; set; }

        [DisplayName("СУР")]
        [Description("Показатель СУР налогоплательщика")]
        public int? SUR_CODE { get; set; }

        [DisplayName("ИНН")]
        [Description("ИНН налогоплательщика")]
        public string INN { get; set; }

        [DisplayName("ИНН")]
        [Description("ИНН налогоплательщика как контрагента")]
        public string INN_CONTRACTOR { get; set; }

        [DisplayName("КПП")]
        [Description("КПП налогоплательщика")]
        public string KPP { get; set; }

        [DisplayName("КПП")]
        [Description("КПП налогоплательщика")]
        public string KPP_EFFECTIVE { get; set; }

        [DisplayName("КПП")]
        [Description("КПП налогоплательщика")]
        public string KPP_EFFECTIVE_CONTRACTOR { get; set; }

        [DisplayName("Наименование")]
        [Description("Наименование налогоплательщика")]
        public string NAME { get; set; }

        [DisplayName("Регистрационный №")]
        [Description("Регистрационный № СЭОД")]
        public long? SEOD_DECL_ID { get; set; }

        [DisplayName("Отчетный период")]
        [Description("Период за который подается декларация/журнал")]
        public string FULL_TAX_PERIOD { get; set; }
        public string TAX_PERIOD { get; set; }

        public int PERIOD_EFFECTIVE { get; set; }

        [DisplayName("Налоговый год")]
        [Description("Налоговый год")]
        public string FISCAL_YEAR { get; set; }

        [DisplayName("Отчетный период")]
        [Description("Период за который подается  декларация/журнал")]
        public FullTaxPeriodInfo FullTaxPeriod { get; set; }

        [DisplayName("Дата подачи")]
        [Description("Дата подачи в НО декларации/журнала")]
        public DateTime? DECL_DATE { get; set; }

        [DisplayName("Дата представления")]
        [Description("Дата представления в НО декларации/журнала")]
        public DateTime? SUBMISSION_DATE { get; set; }
        
        [DisplayName("Номер корректировки")]
        [Description("Номер корректировки декларации/журнала")]
        public string CORRECTION_NUMBER { get; set; }

        [DisplayName("Обработана")]
        [Description("Обработана")]
        public string CORRECTION_PROCESSED { get; set; }

        [DisplayName("ФИО")]
        [Description("ФИО лица, подписавшего декларацию")]
        public string SUBSCRIBER_NAME { get; set; }

        [DisplayName("Представитель")]
        [Description("Представитель лица, подписавшего декларацию")]
        public string PRPODP { get; set; }

        [DisplayName("НД по НДС")]
        [Description("Сумма налога, исчисленная к «+» уплате / «-» возмещению в бюджет")]
        public decimal? COMPENSATION_AMNT { get; set; }

        [DisplayName("Признак НД")]
        [Description("Признак декларации")]
        public string DECL_SIGN { get; set; }

        [DisplayName("Книга покупок")]
        [Description("Сумма налога, исчисленная к уплате по книге покупок")]
        public decimal? NDS_CHAPTER8 { get; set; }
        
        [DisplayName("Книга продаж")]
        [Description("Сумма налога, исчисленная к уплате по книге продаж")]
        public decimal? NDS_CHAPTER9 { get; set; }

        public decimal? NDS_AMOUNT { get; set; }

        [DisplayName("Удельный вес")]
        [Description("Удельный вес вычетов в исчисленной сумме налога, %")]
        public long? NDS_WEIGHT { get; set; }


        #region buy book

        [DisplayName("Признак")]
        [Description("Признак значимых изменений")]
        public string CHAPTER8_MARK { get; set; }

        [DisplayName("Кол-во контрагентов")]
        [Description("Количество контрагентов-продавцов")]
        public long CHAPTER8_CONTRAGENT_CNT { get; set; }

        [DisplayName("Кол-во разрывов")]
        [Description("Количество расхождений вида «Разрыв» с контрагентами-продавцами")]
        public long CHAPTER8_GAP_CNT { get; set; }

        [DisplayName("Кол-во контрагентов")]
        [Description("Количество контрагентов-продавцов с которыми есть расхождение вида «Разрыв»")]
        public long CHAPTER8_GAP_CONTRAGENT_CNT { get; set; }

        [DisplayName("Сумма разрывов, руб.")]
        [Description("Сумма расхождений с видом «Разрыв» по контрагентам-продавцам")]
        public decimal? CHAPTER8_GAP_AMNT { get; set; }

        [DisplayName("Кол-во расхождений")]
        [Description("Количество расхождений отличныx от расхождений вида «Разрыв» с контрагентами-продавцами")]
        public long CHAPTER8_OTHER_CNT { get; set; }

        [DisplayName("Кол-во контрагентов")]
        [Description("Количество контрагентов-продавцов с которыми есть расхождения отличные от расхождений вида «Разрыв»")]
        public long CHAPTER8_OTHER_CONTRAGENT_CNT { get; set; }

        [DisplayName("Сумма расхождений, руб.")]
        [Description("Сумма расхождений отличныx от расхождений вида «Разрыв» по контрагентам-продавцам")]
        public decimal? CHAPTER8_OTHER_AMNT { get; set; }

        #endregion

        #region sell book

        [DisplayName("Признак")]
        [Description("Признак значимых изменений")]
        public string CHAPTER9_MARK { get; set; }

        [DisplayName("Кол-во контрагентов")]
        [Description("Количество контрагентов-продавцов")]
        public long CHAPTER9_CONTRAGENT_CNT { get; set; }

        [DisplayName("Кол-во разрывов")]
        [Description("Количество расхождений вида «Разрыв» с контрагентами-продавцами")]
        public long CHAPTER9_GAP_CNT { get; set; }

        [DisplayName("Кол-во контрагентов")]
        [Description("Количество контрагентов-продавцов с которыми есть расхождение вида «Разрыв»")]
        public long CHAPTER9_GAP_CONTRAGENT_CNT { get; set; }

        [DisplayName("Сумма разрывов, руб.")]
        [Description("Сумма расхождений с видом «Разрыв» по контрагентам-продавцам")]
        public decimal? CHAPTER9_GAP_AMNT { get; set; }

        [DisplayName("Кол-во расхождений")]
        [Description("Количество расхождений отличныx от расхождений вида «Разрыв» с контрагентами-продавцами")]
        public long CHAPTER9_OTHER_CNT { get; set; }

        [DisplayName("Кол-во контрагентов")]
        [Description("Количество контрагентов-продавцов с которыми есть расхождения отличные от расхождений вида «Разрыв»")]
        public long CHAPTER9_OTHER_CONTRAGENT_CNT { get; set; }

        [DisplayName("Сумма расхождений, руб.")]
        [Description("Сумма расхождений отличныx от расхождений вида «Разрыв» по контрагентам-продавцам")]
        public decimal? CHAPTER9_OTHER_AMNT { get; set; }

        #endregion

        #region out invoice

        [DisplayName("Признак")]
        [Description("Признак значимых изменений")]
        public string CHAPTER10_MARK { get; set; }

        [DisplayName("Кол-во контрагентов")]
        [Description("Количество контрагентов-продавцов")]
        public long CHAPTER10_CONTRAGENT_CNT { get; set; }

        [DisplayName("Кол-во разрывов")]
        [Description("Количество расхождений вида «Разрыв» с контрагентами-продавцами")]
        public long CHAPTER10_GAP_CNT { get; set; }

        [DisplayName("Кол-во контрагентов")]
        [Description("Количество контрагентов-продавцов с которыми есть расхождение вида «Разрыв»")]
        public long CHAPTER10_GAP_CONTRAGENT_CNT { get; set; }

        [DisplayName("Сумма разрывов, руб.")]
        [Description("Сумма расхождений с видом «Разрыв» по контрагентам-продавцам")]
        public decimal? CHAPTER10_GAP_AMNT { get; set; }

        [DisplayName("Кол-во расхождений")]
        [Description("Количество расхождений отличныx от расхождений вида «Разрыв» с контрагентами-продавцами")]
        public long CHAPTER10_OTHER_CNT { get; set; }

        [DisplayName("Кол-во контрагентов")]
        [Description("Количество контрагентов-продавцов с которыми есть расхождения отличные от расхождений вида «Разрыв»")]
        public long CHAPTER10_OTHER_CONTRAGENT_CNT { get; set; }

        [DisplayName("Сумма расхождений, руб.")]
        [Description("Сумма расхождений отличныx от расхождений вида «Разрыв» по контрагентам-продавцам")]
        public decimal? CHAPTER10_OTHER_AMNT { get; set; }

        #endregion

        #region in invoice

        [DisplayName("Признак")]
        [Description("Признак значимых изменений")]
        public string CHAPTER11_MARK { get; set; }

        [DisplayName("Кол-во контрагентов")]
        [Description("Количество контрагентов-продавцов")]
        public long CHAPTER11_CONTRAGENT_CNT { get; set; }

        [DisplayName("Кол-во разрывов")]
        [Description("Количество расхождений вида «Разрыв» с контрагентами-продавцами")]
        public long CHAPTER11_GAP_CNT { get; set; }

        [DisplayName("Кол-во контрагентов")]
        [Description("Количество контрагентов-продавцов с которыми есть расхождение вида «Разрыв»")]
        public long CHAPTER11_GAP_CONTRAGENT_CNT { get; set; }

        [DisplayName("Сумма разрывов, руб.")]
        [Description("Сумма расхождений с видом «Разрыв» по контрагентам-продавцам")]
        public decimal? CHAPTER11_GAP_AMNT { get; set; }

        [DisplayName("Кол-во расхождений")]
        [Description("Количество расхождений отличныx от расхождений вида «Разрыв» с контрагентами-продавцами")]
        public long CHAPTER11_OTHER_CNT { get; set; }

        [DisplayName("Кол-во контрагентов")]
        [Description("Количество контрагентов-продавцов с которыми есть расхождения отличные от расхождений вида «Разрыв»")]
        public long CHAPTER11_OTHER_CONTRAGENT_CNT { get; set; }

        [DisplayName("Сумма расхождений, руб.")]
        [Description("Сумма расхождений отличныx от расхождений вида «Разрыв» по контрагентам-продавцам")]
        public decimal? CHAPTER11_OTHER_AMNT { get; set; }

        #endregion

        #region invoice data

        [DisplayName("Признак")]
        [Description("Признак значимых изменений")]
        public string CHAPTER12_MARK { get; set; }

        [DisplayName("Кол-во контрагентов")]
        [Description("Количество контрагентов-продавцов")]
        public long CHAPTER12_CONTRAGENT_CNT { get; set; }

        [DisplayName("Кол-во разрывов")]
        [Description("Количество расхождений вида «Разрыв» с контрагентами-продавцами")]
        public long CHAPTER12_GAP_CNT { get; set; }

        [DisplayName("Кол-во контрагентов")]
        [Description("Количество контрагентов-продавцов с которыми есть расхождение вида «Разрыв»")]
        public long CHAPTER12_GAP_CONTRAGENT_CNT { get; set; }

        [DisplayName("Сумма разрывов, руб.")]
        [Description("Сумма расхождений с видом «Разрыв» по контрагентам-продавцам")]
        public decimal? CHAPTER12_GAP_AMNT { get; set; }

        [DisplayName("Кол-во расхождений")]
        [Description("Количество расхождений отличныx от расхождений вида «Разрыв» с контрагентами-продавцами")]
        public long CHAPTER12_OTHER_CNT { get; set; }

        [DisplayName("Кол-во контрагентов")]
        [Description("Количество контрагентов-продавцов с которыми есть расхождения отличные от расхождений вида «Разрыв»")]
        public long CHAPTER12_OTHER_CONTRAGENT_CNT { get; set; }

        [DisplayName("Сумма расхождений, руб.")]
        [Description("Сумма расхождений отличныx от расхождений вида «Разрыв» по контрагентам-продавцам")]
        public decimal? CHAPTER12_OTHER_AMNT { get; set; }

        #endregion

        [DisplayName("Инспектор")]
        [Description("ФИО инспектора")]
        public string INSPECTOR { get; set; }

        [DisplayName("Инспектор SID")]
        [Description("SID инспектора")]
        public string INSPECTOR_SID { get; set; }

        [DisplayName("")]
        [Description("Признак крупнейшего налогоплательщика")]
        public int? CATEGORY { get; set; }

        [DisplayName("Код инспекции")]
        [Description("Код инспекции")]
        public string SOUN_CODE { get; set; }

        [DisplayName("Наименование инспекции")]
        [Description("Наименование инспекции")]
        public string SOUN_NAME { get; set; }

        [DisplayName("Код региона")]
        [Description("Код региона")]
        public string REGION_CODE { get; set; }

        [DisplayName("Наименование региона")]
        [Description("Наименование региона")]
        public string REGION_NAME { get; set; }
    }
}
