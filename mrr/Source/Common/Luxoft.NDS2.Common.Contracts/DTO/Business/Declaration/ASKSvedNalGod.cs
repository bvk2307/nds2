using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration
{
    public partial class ASKSvedNalGod
    {
        public decimal Id { get; set; }
        public Nullable<decimal> IdSumVosUpl { get; set; }
        public string GodOtch { get; set; }
        public Nullable<System.DateTime> DataIsp170 { get; set; }
        public Nullable<decimal> DolyaNeObl { get; set; }
        public Nullable<long> NalGod { get; set; }
    }
}
