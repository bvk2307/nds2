﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration
{
    [Serializable]
    public enum DeclarationPart
    {
        R8 = 8,
        R9 = 9,
        R10 = 10,
        R11 = 11,
        R12 = 12,
        J1 = 13,
        J2 = 14,
        Discrepancy = 100,
        KC = 200,
        DOCKNP = 300
    }

    public static class PartConverter
    {
        public static DeclarationInvoiceChapterNumber ToDeclarationInvoiceChapter(this DeclarationPart src)
        {
            switch (src)
            {
                case DeclarationPart.R8:
                    return DeclarationInvoiceChapterNumber.Chapter8;
                case DeclarationPart.R9:
                    return DeclarationInvoiceChapterNumber.Chapter9;
                case DeclarationPart.R10:
                    return DeclarationInvoiceChapterNumber.Chapter10;
                case DeclarationPart.R11:
                    return DeclarationInvoiceChapterNumber.Chapter11;
                case DeclarationPart.R12:
                    return DeclarationInvoiceChapterNumber.Chapter12;
                case DeclarationPart.Discrepancy:
                    return DeclarationInvoiceChapterNumber.Chapter10;
                case DeclarationPart.KC:
                    return DeclarationInvoiceChapterNumber.Chapter11;


                default:
                    throw new ArgumentException("Значение раздела может быть только в диапазоне 8-12");
            }
        }

        public static DeclarationPart ToDeclarationPart(this DeclarationInvoiceChapterNumber src)
        {
            switch (src)
            {
                case DeclarationInvoiceChapterNumber.Chapter8:
                    return DeclarationPart.R8;
                case DeclarationInvoiceChapterNumber.Chapter9:
                    return DeclarationPart.R9;
                case DeclarationInvoiceChapterNumber.Chapter10:
                    return DeclarationPart.R10;
                case DeclarationInvoiceChapterNumber.Chapter11:
                    return DeclarationPart.R11;
                case DeclarationInvoiceChapterNumber.Chapter12:
                    return DeclarationPart.R12;
            }
            return DeclarationPart.R8;
        }
    }
}
