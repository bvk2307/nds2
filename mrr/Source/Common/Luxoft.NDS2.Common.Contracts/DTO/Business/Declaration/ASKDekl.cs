using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration
{
    [Serializable]
    public partial class ASKDekl
    {
        public decimal Id { get; set; }
        public Nullable<decimal> ZIP { get; set; }
        public string IdFajl { get; set; }
        public string VersProg { get; set; }
        public string VersForm { get; set; }
        public string PriznNal8_12 { get; set; }
        public string PriznNal8 { get; set; }
        public string PriznNal81 { get; set; }
        public string PriznNal9 { get; set; }
        public string PriznNal91 { get; set; }
        public string PriznNal10 { get; set; }
        public string PriznNal11 { get; set; }
        public string PriznNal12 { get; set; }
        public string KND { get; set; }
        public Nullable<System.DateTime> DataDok { get; set; }
        public string Period { get; set; }
        public string OtchetGod { get; set; }
        public string KodNO { get; set; }
        public string NomKorr { get; set; }
        public string PoMestu { get; set; }
        public string OKVED { get; set; }
        public string Tlf { get; set; }
        public string NaimOrg { get; set; }
        public string INNNP { get; set; }
        public string KPPNP { get; set; }
        public string FormReorg { get; set; }
        public string INNReorg { get; set; }
        public string KPPReorg { get; set; }
        public string FamiliyaNP { get; set; }
        public string ImyaNP { get; set; }
        public string OtchestvoNP { get; set; }
        public string PrPodp { get; set; }
        public string FamiliyaPodp { get; set; }
        public string ImyaPodp { get; set; }
        public string OtchestvoPodp { get; set; }
        public string NaimDok { get; set; }
        public string NaimOrgPred { get; set; }
        public string OKTMO { get; set; }
        public string KBK { get; set; }
        public Nullable<long> SumPU173_5 { get; set; }
        public Nullable<long> SumPU173_1 { get; set; }
        public string NomDogIT { get; set; }
        public Nullable<System.DateTime> DataDogIT { get; set; }
        public Nullable<System.DateTime> DataNachDogIT { get; set; }
        public Nullable<System.DateTime> DataKonDogIT { get; set; }
        public Nullable<long> NalPU164 { get; set; }
        public Nullable<long> NalVosstObshh { get; set; }
        public Nullable<long> RealTov18NalBaza { get; set; }
        public Nullable<long> RealTov18SumNal { get; set; }
        public Nullable<long> RealTov10NalBaza { get; set; }
        public Nullable<long> RealTov10SumNal { get; set; }
        public Nullable<long> RealTov118NalBaza { get; set; }
        public Nullable<long> RealTov118SumNal { get; set; }
        public Nullable<long> RealTov110NalBaza { get; set; }
        public Nullable<long> RealTov110SumNal { get; set; }
        public Nullable<long> RealPredIKNalBaza { get; set; }
        public Nullable<long> RealPredIKSumNal { get; set; }
        public Nullable<long> VypSMRSobNalBaza { get; set; }
        public Nullable<long> VypSMRSobSumNal { get; set; }
        public Nullable<long> OplPredPostNalBaza { get; set; }
        public Nullable<long> OplPredPostSumNal { get; set; }
        public Nullable<long> SumNalVs { get; set; }
        public Nullable<long> SumNal170_3_5 { get; set; }
        public Nullable<long> SumNal170_3_3 { get; set; }
        public Nullable<long> KorRealTov18NalBaza { get; set; }
        public Nullable<long> KorRealTov18SumNal { get; set; }
        public Nullable<long> KorRealTov10NalBaza { get; set; }
        public Nullable<long> KorRealTov10SumNal { get; set; }
        public Nullable<long> KorRealTov118NalBaza { get; set; }
        public Nullable<long> KorRealTov118SumNal { get; set; }
        public Nullable<long> KorRealTov110NalBaza { get; set; }
        public Nullable<long> KorRealTov110SumNal { get; set; }
        public Nullable<long> KorRealPredIKNalBaza { get; set; }
        public Nullable<long> KorRealPredIKSumNal { get; set; }
        public Nullable<long> NalPredNPPriob { get; set; }
        public Nullable<long> NalPredNPPok { get; set; }
        public Nullable<long> NalIschSMR { get; set; }
        public Nullable<long> NalUplTamozh { get; set; }
        public Nullable<long> NalUplNOTovTS { get; set; }
        public Nullable<long> NalIschProd { get; set; }
        public Nullable<long> NalUplPokNA { get; set; }
        public Nullable<long> NalVychObshh { get; set; }
        public Nullable<long> SumIschislItog { get; set; }
        public Nullable<long> SumIschislItogMinus { get; set; }
        public Nullable<long> SumVozmPdtv { get; set; }
        public Nullable<long> SumVozmNePdtv { get; set; }
        public Nullable<long> SumNal164It { get; set; }
        public Nullable<long> NalVychNePodIt { get; set; }
        public Nullable<long> NalIschislIt { get; set; }
        public Nullable<long> NalIschislItMinus { get; set; }
        public string SumOper1010449KodOper { get; set; }
        public Nullable<long> SumOper1010449NalBaza { get; set; }
        public Nullable<long> SumOper1010449KorIsch { get; set; }
        public Nullable<long> SumOper1010449NalVosst { get; set; }
        public Nullable<long> OplPostSv6Mes { get; set; }
        public string NaimKnPok { get; set; }
        public string NaimKnPokDL { get; set; }
        public string NaimKnProd { get; set; }
        public string NaimKnProdDL { get; set; }
        public string NaimZhUchVystSchF { get; set; }
        public string NaimZhUchPoluchSchF { get; set; }
        public string NaimVystSchF173_5 { get; set; }

        public string IdZagruzka{ get; set; }

        public string KodOper47{ get; set; }
        public Nullable<long> NalBaza47 { get; set; }
        public Nullable<long> NalVosst47 { get; set; }

        public string KodOper48{ get; set; }
        public Nullable<long> KorNalBazaUv48 { get; set; }
        public Nullable<long> KorNalBazaUm48 { get; set; }

        public string KodOper50{ get; set; }
        public Nullable<long> KorNalBazaUv50 { get; set; }
        public Nullable<long> KorIschUv50 { get; set; }
        public Nullable<long> KorNalBazaUm50 { get; set; }
        public Nullable<long> KorIschUm50 { get; set; }

        public List<ASKSumOper4> SumOper4 { get; set; }
        public List<ASKSumOper6> SumOper6 { get; set; }
        public List<ASKSumPer> SumPer { get; set; }
        public List<ASKSumOper7> SumOper7 { get; set; }
        public List<ASKSumUplNA> SumUplNA { get; set; }
        public List<ASKSumVosUpl> SumVosUpl { get; set; }
        public List<ASKSvedNalGodI> SvedNalGodI { get; set; }

        public long? RlSr118SumNal { get; set; }
        public long? RlSr118NalBaza { get; set; }
        public long? RlSr110SumNal { get; set; }
        public long? RlSr110NalBaza { get; set; }
        public long? UplD151SumNal { get; set; }
        public long? UplD151NalBaza { get; set; }
        public long? UplD173SumNal { get; set; }
        public long? UplD173NalBaza { get; set; }
        public long? NalPredNPKapStr { get; set; }
        public long? NalVych171_14 { get; set; }
    }
}
