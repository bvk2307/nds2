﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration
{
    [Serializable]
    public class ActKNP
    {
        /// <summary>
        /// идентификатор документа
        /// </summary>
        public long KNP_ID { get; set; }

        /// <summary>
        /// дата документа
        /// </summary>
        public DateTime? Date { get; set; }

        /// <summary>
        /// номер документа
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// Сумма налога 1 (денежное значение)
        /// </summary>
        public decimal? SumUnpaid { get; set; }

        /// <summary>
        /// Сумма налога 2 (денежное значение)
        /// </summary>
        public decimal? SumOverestimated { get; set; }

        /// <summary>
        /// Сумма налога 3 (денежное значение)
        /// </summary>
        public decimal? SumCompensation { get; set; }

        public decimal? Penalty { get; set; }
    }
}
