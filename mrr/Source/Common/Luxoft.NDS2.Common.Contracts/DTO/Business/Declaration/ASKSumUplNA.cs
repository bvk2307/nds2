using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration
{
    public partial class ASKSumUplNA
    {
        public decimal Id { get; set; }
        public Nullable<decimal> IdDekl { get; set; }
        public string KPPIno { get; set; }
        public string KBK { get; set; }
        public string OKTMO { get; set; }
        public Nullable<long> SumIschisl { get; set; }
        public string KodOper { get; set; }
        public Nullable<long> SumIschislOtgr { get; set; }
        public Nullable<long> SumIschislOpl { get; set; }
        public Nullable<long> SumIschislNA { get; set; }
        public string NaimProd { get; set; }
        public string INNProd { get; set; }
        public string Familiya { get; set; }
        public string Imya { get; set; }
        public string Otchestvo { get; set; }
    }
}
