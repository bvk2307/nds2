﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using FLS.Common.Lib.Attributes;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace.Enums;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration.Converters
{
    /// <summary> A enum type converter for <see cref="DeclarationType"/>. </summary>
    public sealed class DeclarationSignTypeConverter : EnumConverter
    {
        private static DeclarationSignTypeConverter s_signTypeConverter = null;
        private static Dictionary<DeclarationType, string> s_Values = null;

        public static DeclarationSignTypeConverter Current
        {
            get
            {
                if ( s_signTypeConverter == null )
                    s_signTypeConverter = new DeclarationSignTypeConverter();

                return s_signTypeConverter;
            }
        }

        public DeclarationSignTypeConverter() : base( typeof(DeclarationType) ) { }

        public DeclarationType ConvertFrom( string declarationTypeText )
        {
            return (DeclarationType)ConvertFrom( (object)declarationTypeText );
        }

        public string ConvertTo( DeclarationType declarationType )
        {
            return (string)ConvertTo( declarationType, typeof(string) );
        }

        public override object ConvertFrom( ITypeDescriptorContext context, CultureInfo culture, object value )
        {
            if ( value is string )
            {
                try
                {
                    string strValue = (string)value;
                    if ( strValue.IndexOf( ',' ) != -1 )
                    {
                        long convertedValue = 0;
                        string[] values = strValue.Split( new[] {','} );
                        foreach ( string stringValue in values )
                        {
                            convertedValue |= Convert.ToInt64( ConvertStringValueToEnum( EnumType, stringValue, culture ), culture );
                        }

                        return Enum.ToObject( EnumType, convertedValue );
                    }

                    return ConvertStringValueToEnum( EnumType, strValue, culture );
                }
                catch ( Exception e )
                {
                    if ( e is FormatException )
                        throw;
                    throw new FormatException( string.Format( "Convertion of string: '{0}' falied for type: {1}", (string)value, EnumType.Name ), e );
                }
            }
            return base.ConvertFrom( context, culture, value );
        }

        public override object ConvertTo( ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType )
        {
            if ( destinationType == typeof( string ) && value != null )
            {   // Raise an argument exception if the value isn't defined and if the enum isn't a flags style.
                Type underlyingType = Enum.GetUnderlyingType( EnumType );
                if ( value is IConvertible && value.GetType() != underlyingType )
                    value = ( (IConvertible)value ).ToType( underlyingType, culture );
                if ( !IsEnumTypeFlagged( EnumType ) && !Enum.IsDefined( EnumType, value ) )
                    throw new ArgumentException( string.Format( "Convertion of enum value: '{0}' failed for type: {1}", value, EnumType.Name ) );

                return ConvertEnumValueToString( (DeclarationType)value );
            }
            return base.ConvertTo( context, culture, value, destinationType );
        }

        private static DeclarationType ConvertStringValueToEnum( Type type, string stringValue, CultureInfo culture )
        {
            KeyValuePair<DeclarationType, string> valuePair = EnumValues.FirstOrDefault( 
                kvpair => 0 == string.Compare( stringValue.Trim(), kvpair.Value, culture, CompareOptions.IgnoreCase ) );

            if ( default(KeyValuePair<DeclarationType, string>).Equals( valuePair ) )
                throw new FormatException( string.Format( "Convertion of string: '{0}' falied for type: {1}", stringValue, typeof(DeclarationType).Name ) );

            return valuePair.Key;
        }

        private string ConvertEnumValueToString( DeclarationType value )
        {
            string sValue = IsEnumTypeFlagged( EnumType ) ? ConvertEnumValueToString( (Enum)value ) : EnumValues[ value ];

            return sValue;
        }

        private static string ConvertEnumValueToString( Enum value )
        {
            Type enumType = value.GetType();
            string sValue = null;
            if ( IsEnumTypeFlagged( enumType ) )
            {
                List<string> values = null;
                foreach ( DeclarationType declarationType in Enum.GetValues( enumType ) )
                {
                    if ( value.HasFlag( declarationType ) )
                    {
                        if ( values == null )
                            values = new List<string>();
                        values.Add( EnumValues[declarationType] );
                    }
                }
                sValue = string.Join( ", ", values );
            }
            else
            {
                sValue = AttributeReadHelper.GetDisplayName(value );
            }
            return sValue;
        }

        private static bool IsEnumTypeFlagged( Type enumType )
        {
            if ( !enumType.IsEnum )
                throw new ArgumentOutOfRangeException( "enumType", "Enum types are supported only" );

            return enumType.IsDefined( typeof( FlagsAttribute ), inherit: false );
        }
 
        private static Dictionary<DeclarationType, string> EnumValues
        {
            get
            {
                if ( s_Values == null )
                    s_Values = AttributeReadHelper.GetDisplayNames<DeclarationType>();

                return s_Values;
            }
        }
    }
}