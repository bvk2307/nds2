using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration
{
    public partial class ASKSumOper5
    {
        public decimal Id { get; set; }
        public Nullable<decimal> IdSumPer { get; set; }
        public string KodOper { get; set; }
        public Nullable<long> NalBazaPod { get; set; }
        public Nullable<long> NalVychPod { get; set; }
        public Nullable<long> NalBazaNePod { get; set; }
        public Nullable<long> NalVychNePod { get; set; }
    }
}
