﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration
{
    [Serializable]
    public class DocumentKNP
    {
        /// <summary>
        /// идентификатор документа
        /// </summary>
        public long? Id { get; set; }

        /// <summary>
        /// тип документа
        /// </summary>
        public long? Type { get; set; }

        /// <summary>
        /// Состояние документа Пояснение/Ответ (разрешено открывать документ или нет)
        /// </summary>
        public int? StateExplainCanOpen { get; set; }

        [DisplayName("Тип документа")]
        [Description("Тип документа")]
        public string TypeName { get; set; }

        [DisplayName("№ документа")]
        [Description("№ документа")]
        public string DocNum { get; set; }

        [DisplayName("Дата документа")]
        [Description("Дата документа")]
        public DateTime? Date { get; set; }

        [DisplayName("Статус")]
        [Description("Статус документа")]
        public string DocStatus { get; set; }

    }
}
