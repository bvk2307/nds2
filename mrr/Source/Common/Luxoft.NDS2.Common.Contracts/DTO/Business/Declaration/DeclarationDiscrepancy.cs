﻿using Luxoft.NDS2.Common.Contracts.CustomAttributes;
using System;
using System.ComponentModel;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration
{
    /// <summary>
    /// Этот класс представляет данные расхождения в списке расхождений карточки НД/Журнала
    /// </summary>
    [Serializable]
    public class DeclarationDiscrepancy
    {
        # region Атрибуты Расхождения

        [DisplayName("Номер")]
        [Description("Номер расхождения")]
        public long DiscrepancyId
        {
            get;
            set;
        }

        [DisplayName("Сумма расхождения (руб.)")]
        [Description("Сумма расхождения в руб.")]
        [CurrencyFormat]
        public decimal Amount 
        { 
            get; 
            set; 
        }

        [DisplayName("ПВП расхождения (руб.)")]
        [Description("ПВП расхождения в руб.")]
        [CurrencyFormat]
        public decimal? AmountPvp 
        { 
            get; 
            set; 
        }

        public string StatusCode
        {
            get;
            set;
        }

        [DisplayName("Статус")]
        [Description("Статус расхождения")]
        public string StatusName 
        { 
            get; 
            set; 
        }

        public int TypeCode
        {
            get;
            set;
        }

        [DisplayName("Вид расхождения")]
        [Description("Вид расхождения")]
        public string TypeName
        {
            get;
            set;
        }

        [DisplayName("Вид расхождения для подсказки")]
        [Description("Вид расхождения для всплывающей подсказки")]
        public string TypeNameForCaption
        {
            get;
            set;
        }

        [DisplayName("Дата выявления")]
        [Description("Дата выявления расхождения")]
        public DateTime? FoundDate
        {
            get;
            set;
        }

        [DisplayName("Статус КНП")]
        [Description("Статус декларации")]
        public string KnpStatus
        { 
            get; 
            set; 
        }
        
        [DisplayName("Дата закрытия расхождения")]
        [Description("Дата закрытия расхождения")]
        public DateTime? CloseDate
        { 
            get; 
            set; 
        }
        
        [DisplayName("Комментарий пользователя")]
        [Description("Комментарий пользователя  ")]
        public string UserComment
        { 
            get; 
            set; 
        }
        

        # endregion

        # region Атрибуты НД\Журнала НП

        public long TaxPayerZip
        {
            get;
            set;
        }
        
        [DisplayName("ИНН реорганизованного налогоплательщика")]
        [Description("ИНН реорганизованного налогоплательщика")]
        public string TaxPayerReorgInn 
        { 
            get; 
            set; 
        }

        [DisplayName("ИНН налогоплательщика")]
        [Description("ИНН налогоплательщика")]
        public string TaxPayerInn
        {
            get;
            set;
        }

        [DisplayName("КПП налогоплательщика")]
        [Description("КПП налогоплательщика")]
        public string TaxPayerKpp 
        { 
            get; 
            set; 
        }
        
        [DisplayName("Признак НД")]
        [Description("Признак декларации")]
        public string TaxPayerDeclSign 
        { 
            get; 
            set; 
        }

        # endregion

        # region Атрибут счета-фактуры НП

        public string TaxPayerInvoiceKey
        {
            get;
            set;
        }

        [DisplayName("Источник")]
        [Description("Номер раздела/части")]
        public string TaxPayerInvoiceSource
        {
            get;
            set;
        }

        [DisplayName("Источник расхождения")]
        [Description("Номер раздела НД с расхождением")]
        public int? WorkSideInvoiceChapter
        {
            get;
            set;
        }
        # endregion

        # region Атрибуты контрагента

        [DisplayName("ИНН")]
        [Description("ИНН НП с которым связано расхождение первичной отработки")]
        public string ContractorInn 
        { 
            get; 
            set; 
        }

        [DisplayName("КПП")]
        [Description("КПП НП с которым связано расхождение первичной отработки")]
        public string ContractorKpp 
        { 
            get; 
            set; 
        }

        [DisplayName("Реорганизованный")]
        [Description("ИНН реорганизованного НП")]
        public string ContractorInnReorganized
        {
            get;
            set;
        }

        [DisplayName("ИНН НП, подавшего НД, с которой связано расхождение")]
        [Description("ИНН НП, подавшего НД, с которой связано расхождение")]
        public string OtherSideINN 
        {
            get;
            set;
        }

        [DisplayName("Наименование")]
        [Description("Наименование НП с которым связано расхождение первичной отработки")]
        public string ContractorName 
        { 
            get; 
            set; 
        }

        # endregion

        # region Атрибуты НД\Журнала контрагента 

        public long? ContractorZip
        {
            get;
            set;
        }

        [DisplayName("Тип документа")]
        [Description("НД по НДС или Журнал учета")]
        public string ContractorDocType 
        { 
            get; 
            set; 
        }

        [DisplayName("Признак НД")]
        [Description("Признак декларации")]
        public string ContractorDeclSign 
        { 
            get; 
            set; 
        }

        [DisplayName("Сумма НДС")]
        [Description("Сумма налога, исчисленная к «+» уплате / «-» возмещению в бюджет")]
        public decimal? ContractorNdsAmount 
        { 
            get; 
            set; 
        }

        # endregion

        # region Атрибуты счета-фактуры контрагента

        public string ContractorInvoiceKey
        {
            get;
            set;
        }

        [DisplayName("Источник")]
        [Description("Источник записи о СФ")]
        public string ContractorInvoiceSource 
        { 
            get; 
            set; 
        }
        /// <summary>
        /// Номер СФ
        /// </summary>
        public string CreatedByInvoiceNumber { get; set; }

        /// <summary>
        /// Дата СФ
        /// </summary>
        public DateTime? CreatedByInvoiceDate { get; set; }
        
        [DisplayName("Источник")]
        [Description("Номер раздела НД с записью о СФ")]
        public int? ContractorInvoiceChapter
        {
            get;
            set;
        }
        

        # endregion

        # region Атрибуты Актов/Решений

        /// <summary>
        /// Налогоплательщик, в акт которого отобрано расхождение (ИНН) 
        /// </summary>
        public string ActInn { get; set; }

        /// <summary>
        /// Налогоплательщик, в акт которого отобрано расхождение (КПП) 
        /// </summary>
        public string ActKpp { get; set; }

        /// <summary>
        /// Тип документа, в которое отобрано расхождение
        /// </summary>
        public string KnpDocType { get; set; }

        /// <summary>
        /// Сумма расхождения по документу, в которое оно отобрано
        /// </summary>
        public decimal? KnpDocAmount { get; set; }

        /// <summary>
        /// Сумма неустраненного расхождения по решению
        /// </summary>
        public decimal? DecisionAmountDifference { get; set; }

        # endregion

    }
}
