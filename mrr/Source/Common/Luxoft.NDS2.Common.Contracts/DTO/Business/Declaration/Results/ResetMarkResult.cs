﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration.Results
{
    [Serializable]
    public class ResetMarkResult
    {
        public bool Success { get; set; }
        public DeclarationOwnershipType DeclarationOwnerType { get; set; }

        public ResetMarkResult()
        {
            Success = false;
            DeclarationOwnerType = Enums.DeclarationOwnershipType.None;
        }
    }
}
