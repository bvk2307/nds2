﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration
{
    [Serializable]
    public class DeclarationKey
    {
        public long? Id
        {
            get;
            set;
        }

        public string Inn
        {
            get;
            set;
        }

        public string Kpp
        {
            get;
            set;
        }

        public string Year
        {
            get;
            set;
        }

        public string Period
        {
            get;
            set;
        }

        public int TypeCode
        {
            get;
            set;
        }
    }

    [Serializable]
    [Obsolete]
    public class DeclarantionDeclarantKey
    {
        public string InnDeclarant { get; set; }

        public string KppEffective { get; set; }

        public string Period { get; set; }

        public string Year { get; set; }

        public int TypeCode { get; set; }

        public DeclarantionDeclarantKey(DeclarationBrief decl)
        {
            InnDeclarant = decl.INN;
            KppEffective = decl.KPP_EFFECTIVE;
            Period = decl.TAX_PERIOD;
            Year = decl.FISCAL_YEAR;
            TypeCode = decl.DECL_TYPE_CODE;
        }

        public DeclarantionDeclarantKey(DeclarationSummary decl)
        {
            InnDeclarant = decl.INN;
            KppEffective = decl.KPP_EFFECTIVE;
            Period = decl.TAX_PERIOD;
            Year = decl.FISCAL_YEAR;
            TypeCode = decl.DECL_TYPE_CODE;
        }
    }
}
