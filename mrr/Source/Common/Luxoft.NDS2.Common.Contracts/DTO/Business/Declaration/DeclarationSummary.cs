﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.CustomAttributes;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using Luxoft.NDS2.Common.Contracts.DTO.Business.TaxPeriod;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration
{
    [Serializable]
    public class DeclarationSummary : IDeclarationIdentity
    {
        /// <summary>
        /// идентификатор декларации (год+отчетный период+ИНН)
        /// </summary>
        public long ID { get; set; }

        #region SEOD_DECL_ID, ASK_DECL_ID, DECLARATION_VERSION_ID (==ZIP)

        [DisplayName("Регистрационный №")]
        [Description("Регистрационный № СЭОД")]
        public long? SEOD_DECL_ID { get; set; }

        public long ASK_DECL_ID { get; set; }

        public string XmlVersion { get; set; }

        /// <summary>
        /// Идентификатор версии дакларации
        /// </summary>
        public long DECLARATION_VERSION_ID { get; set; }

        #endregion

        #region IS_ACTUAL, ProcessingStage, LOAD_MARK

        public bool IS_ACTUAL { get; set; }

        public DeclarationProcessignStage ProcessingStage { get; set; }

        public DeclarationSummaryState SummaryState { get; set; }

        [DisplayName("")]
        [Description("Статус загрузки")]
        public DeclarationProcessignStage LOAD_MARK { get; set; }

        #endregion

        #region INN, KPP, NAME, ADDRESS1, ADDRESS2, REGION*, SOUN*, CATEGORY*

        [DisplayName("ИНН")]
        [Description("ИНН налогоплательщика первичной отработки")]
        public string INN { get; set; }

        [DisplayName("ИНН")]
        [Description("ИНН налогоплательщика первичной отработки")]
        public string INN_CONTRACTOR { get; set; }

        [DisplayName("КПП")]
        [Description("КПП налогоплательщика первичной отработки")]
        public string KPP { get; set; }

        [DisplayName("КПП эффективный")]
        [Description("КПП эффективный налогоплательщика первичной отработки")]
        public string KPP_EFFECTIVE { get; set; }

        [DisplayName("Наименование")]
        [Description("Наименование налогоплательщика первичной отработки")]
        public string NAME { get; set; }

        [DisplayName("Юридический адрес")]
        [Description("Юридический адрес")]
        public string ADDRESS1 { get; set; }

        [DisplayName("Фактический адрес")]
        [Description("Фактический адрес")]
        public string ADDRESS2 { get; set; }

        [DisplayName("Регион")]
        [Description("Код и наименование региона")]
        public string REGION_NAME { get; set; }

        [DisplayName("Регион")]
        [Description("Код и наименование региона")]
        public CodeValueDictionaryEntry REGION_ENTRY { get; set; }

        public string REGION_CODE { get; set; }

        [DisplayName("Инспекция")]
        [Description("Код и наименование инспекции")]
        public string SOUN_NAME { get; set; }

        [DisplayName("Инспекция")]
        [Description("Код и наименование инспекции")]
        public CodeValueDictionaryEntry SOUN_ENTRY { get; set; }

        public string SOUN_CODE { get; set; }

        [DisplayName("Наименование инспекции")]
        [Description("Наименование инспекции")]
        public string SOUN { get; set; }

        [DisplayName("Крупнейший")]
        [Description("Признак крупнейшего налогоплательщика")]
        public int? CATEGORY { get; set; }

        [DisplayName("Крупнейший")]
        [Description("Признак крупнейшего налогоплательщика")]
        public string CATEGORY_RU { get; set; }

        #endregion

        #region REG_DATE, TAX_MODE, OKVED_CODE, CAPITAL, TAX_PERIOD, FISCAL_YEAR, DECL*, CORRECTION*

        [DisplayName("Дата создания")]
        [Description("Дата создания (регистрации)")]
        public DateTime? REG_DATE { get; set; }

        [DisplayName("Применяемый режим налогообложения")]
        [Description("Применяемый режим налогообложения")]
        public string TAX_MODE { get; set; }

        [DisplayName("ОКВЭД")]
        [Description("ОКВЭД")]
        public string OKVED_CODE { get; set; }

        [DisplayName("Размер уставного капитала")]
        [Description("Размер уставного капитала")]
        public decimal CAPITAL { get; set; }

        [DisplayName("Отчетный период")]
        [Description("Период за который подается декларация")]
        public string TAX_PERIOD { get; set; }

        public int PeriodEffective { get; set; }

        [DisplayName("Налоговый год")]
        [Description("Налоговый год")]
        public string FISCAL_YEAR { get; set; }

        [DisplayName("Дата подачи декларации")]
        [Description("Дата подачи декларации")]
        public DateTime? DECL_DATE { get; set; }

        [DisplayName("Дата представления")]
        [Description("Дата представления в НО")]
        public DateTime? SUBMISSION_DATE { get; set; }

        [DisplayName("Признак НД")]
        [Description("Признак декларации")]
        public string DECL_SIGN { get; set; }

        [DisplayName("Тип НД")]
        [Description("Тип декларации")]
        public string DECL_TYPE { get; set; }

        [DisplayName("Аннул.")]
        [Description("Аннулировано")]
        public bool HAS_CANCELLED_CORRECTION { get; set; }

        public int DECL_TYPE_CODE { get; set; }

        [DisplayName("Номер корректировки")]
        [Description("Номер корректировки декларации")]
        public string CORRECTION_NUMBER { get; set; }

        [DisplayName("ФИО")]
        [Description("ФИО подписанта")]
        public string SUBSCRIBER_NAME { get; set; }

        [DisplayName("Представитель")]
        [Description("Представитель")]
        public string PRPODP { get; set; }

        public int CORRECTION_NUMBER_RANK { get; set; }
        public string CORRECTION_NUMBER_EFFECTIVE { get; set; }

        [DisplayName("Сумма к уплате/возмещению")]
        [Description("Сумма к уплате/возмещению")]
        public decimal? COMPENSATION_AMNT { get; set; }

        [DisplayName("Сумма НДС")]
        [Description("Сумма налога, исчисленная к «+» уплате / «-» возмещению в бюджет")]
        public string COMPENSATION_AMNT_SIGN { get; set; }

        [DisplayName("Удельный вес")]
        [Description("Удельный вес вычетов в исчисленной сумме налога, %")]
        public long? NDS_WEIGHT { get; set; }

        #endregion

        #region

        public long LK_ERRORS_COUNT { get; set; }

        #endregion

        #region Сводная информация по разделу 8 и 9

        [CurrencyFormat]
        public decimal CH8_DEALS_AMNT_TOTAL { get; set; }

        [CurrencyFormat]
        public decimal CH9_DEALS_AMNT_TOTAL { get; set; }

        [CurrencyFormat]
        public decimal CH8_NDS { get; set; }

        [CurrencyFormat]
        public decimal CH9_NDS { get; set; }

        public string CH8_PRESENT_DESCRIPTION { get; set; }
        public string CH9_PRESENT_DESCRIPTION { get; set; }

        #region Сводные данные по разделу 8 и разделу 8.1

        /// <summary>Сумма налога всего по книге покупок (КнигаПокуп.СумНДСВсКПк)</summary>
        [CurrencyFormat]
        public decimal? SumNDSPok_8 { get; set; }

        /// <summary>Итоговая сумма налога по книге покупок (КнигаПокупДЛ.СумНДСИтКПк)</summary>
        [CurrencyFormat]
        public decimal? SumNDSPok_81 { get; set; }

        /// <summary>Сумма налога всего по приложению 1 к разделу 8 (КнигаПокупДЛ.СумНДСИтП1Р8)</summary>
        [CurrencyFormat]
        public decimal? SumNDSPokDL_81 { get; set; }

        #endregion

        #region Сводные данные по разделу 9 и разделу 9.1

        //----- Всего по книге продаж (из раздела 9)
        /// <summary>Стоимость (без налога) по ставке 18%. (КнигаПрод.СтПродБезНДС18)</summary>
        [CurrencyFormat]
        public decimal? StProd18_9 { get; set; }
        /// <summary>Стоимость (без налога) по ставке 10%. (КнигаПрод.СтПродБезНДС10)</summary>
        [CurrencyFormat]
        public decimal? StProd10_9 { get; set; }
        /// <summary>Стоимость (без налога) по ставке 0%. (КнигаПрод.СтПродБезНДС0)</summary>
        [CurrencyFormat]
        public decimal? StProd0_9 { get; set; }
        /// <summary>Сумма налога, по ставке 18%. (КнигаПрод.СумНДСВсКПр18)</summary>
        [CurrencyFormat]
        public decimal? SumNDSProd18_9 { get; set; }
        /// <summary>Сумма налога, по ставке 10%. (КнигаПрод.СумНДСВсКПр10)</summary>
        [CurrencyFormat]
        public decimal? SumNDSProd10_9 { get; set; }
        /// <summary>Стоимость продаж, освобождаемых от налога. (КнигаПрод.СтПродОсвВсКПр)</summary>
        [CurrencyFormat]
        public decimal? StProdOsv_9 { get; set; }

        //----- Итого по книге продаж (из раздела 9.1)
        /// <summary>Стоимость (без налога) по ставке 18%. (КнигаПродДЛ.ИтСтПродКПр18)</summary>
        [CurrencyFormat]
        public decimal? StProd18_91 { get; set; }
        /// <summary>Стоимость (без налога) по ставке 10%. (КнигаПродДЛ.ИтСтПродКПр10)</summary>
        [CurrencyFormat]
        public decimal? StProd10_91 { get; set; }
        /// <summary>>Стоимость (без налога) по ставке 0%. (КнигаПродДЛ.ИтСтПродКПр0)</summary>
        [CurrencyFormat]
        public decimal? StProd0_91 { get; set; }
        /// <summary>Сумма налога, по ставке 18%. (КнигаПродДЛ.СумНДСИтКПр18)</summary>
        [CurrencyFormat]
        public decimal? SumNDSProd18_91 { get; set; }
        /// <summary>Сумма налога, по ставке 10%. (КнигаПродДЛ.СумНДСИтКПр10)</summary>
        [CurrencyFormat]
        public decimal? SumNDSProd10_91 { get; set; }
        /// <summary>Стоимость продаж, освобождаемых от налога. (КнигаПродДЛ.ИтСтПродОсвКПр)</summary>
        [CurrencyFormat]
        public decimal? StProdOsv_91 { get; set; }

        //----- Всего по Приложению 1 к разделу 9 (из раздела 9.1)
        /// <summary>Стоимость (без налога) по ставке 18%. (КнигаПродДЛ.СтПродВсП1Р9_18)</summary>
        [CurrencyFormat]
        public decimal? StProd18DL_91 { get; set; }
        /// <summary>Стоимость (без налога) по ставке 10%. (КнигаПродДЛ.СтПродВсП1Р9_10)</summary>
        [CurrencyFormat]
        public decimal? StProd10DL_91 { get; set; }
        /// <summary>Стоимость (без налога) по ставке 0%. (КнигаПродДЛ.СтПродВсП1Р9_0)</summary>
        [CurrencyFormat]
        public decimal? StProd0DL_91 { get; set; }
        /// <summary>Сумма налога, по ставке 18%. (КнигаПродДЛ.СумНДСВсП1Р9_18)</summary>
        [CurrencyFormat]
        public decimal? SumNDSProd18DL_91 { get; set; }
        /// <summary>Сумма налога, по ставке 10%. (КнигаПродДЛ.СумНДСВсП1Р9_10)</summary>
        [CurrencyFormat]
        public decimal? SumNDSProd10DL_91 { get; set; }
        /// <summary>Стоимость продаж, освобождаемых от налога. (КнигаПродДЛ.СтПродОсвП1Р9Вс)</summary>
        [CurrencyFormat]
        public decimal? StProdOsvDL_91 { get; set; }

        #endregion

        ///// <summary>Итоговая стоимость продаж по книге продаж (без налога), по ставке 10%</summary>
        //[CurrencyFormat]
        //public decimal? StProd18 { get; set; }
        ///// <summary>Итоговая стоимость продаж по книге продаж (без налога), по ставке 0%</summary>
        //[CurrencyFormat]
        //public decimal? StProd10 { get; set; }
        ///// <summary>Итоговая сумма налога по книге продаж, по ставке 18%</summary>
        //[CurrencyFormat]
        //public decimal? StProd0 { get; set; }
        ///// <summary>Итоговая сумма налога по книге продаж, по ставке 10%</summary>
        //[CurrencyFormat]
        //public decimal? SumNDSProd18 { get; set; }

        //[CurrencyFormat]
        //public decimal? SumNDSProd10 { get; set; }

        [CurrencyFormat]
        public decimal? StProd { get; set; }

        //[CurrencyFormat]
        //public decimal? StProdOsv { get; set; }

        //[CurrencyFormat]
        //public decimal? StProd18DL { get; set; }

        //[CurrencyFormat]
        //public decimal? StProd10DL { get; set; }

        //[CurrencyFormat]
        //public decimal? StProd0DL { get; set; }

        //[CurrencyFormat]
        //public decimal? SumNDSProd18DL { get; set; }

        //[CurrencyFormat]
        //public decimal? SumNDSProd10DL { get; set; }

        //[CurrencyFormat]
        //public decimal? StProdOsvDL { get; set; }

        #endregion

        #region Сводная информация по разделу 8, 9, 10, 11, 12

        /// <summary>Акутальный номер корректировки раздела 8</summary>
        public int? AKT_NOMKORR_8 { get; set; }
        /// <summary>Акутальный номер корректировки раздела 8.1</summary>
        public int? AKT_NOMKORR_81 { get; set; }
        /// <summary>Акутальный номер корректировки раздела 9</summary>
        public int? AKT_NOMKORR_9 { get; set; }
        /// <summary>Акутальный номер корректировки раздела 9.1</summary>
        public int? AKT_NOMKORR_91 { get; set; }
        /// <summary>Акутальный номер корректировки раздела 10</summary>
        public int? AKT_NOMKORR_10 { get; set; }
        /// <summary>Акутальный номер корректировки раздела 11</summary>
        public int? AKT_NOMKORR_11 { get; set; }
        /// <summary>Акутальный номер корректировки раздела 12</summary>
        public int? AKT_NOMKORR_12 { get; set; }

        #endregion

        #region Расхождения по валюте

        [CurrencyFormat]
        public decimal DISCREP_CURRENCY_AMNT { get; set; }

        public long DISCREP_CURRENCY_COUNT { get; set; }

        #endregion

        #region Расхождения по не точному соотношению

        public long WEAK_DISCREP_COUNT { get; set; }

        [CurrencyFormat]
        public decimal WEAK_DISCREP_AMNT { get; set; }

        #endregion

        #region Расхождения по типу разрыв

        public long GAP_DISCREP_COUNT { get; set; }

        [CurrencyFormat]
        public decimal GAP_DISCREP_AMNT { get; set; }

        #endregion

        #region Расхождения по завышенному НДС

        public long NDS_INCREASE_DISCREP_COUNT { get; set; }

        [CurrencyFormat]
        public decimal NDS_INCREASE_DISCREP_AMNT { get; set; }

        #endregion

        #region Расхождения по типу разрыв по множественному сопоставлению

        public long GAP_MULTIPLE_DISCREP_COUNT { get; set; }

        [CurrencyFormat]
        public decimal GAP_MULTIPLE_DISCREP_AMNT { get; set; }

        #endregion

        [DisplayName("Кол-во расхождений по КС")]
        [Description("Количество расхождений по КС в НД")]
        public long? CONTROL_RATIO_COUNT { get; set; }

        [DisplayName("Кол-во расхождений по СФ")]
        [Description("Количество расхождений по СФ в НД")]
        public long? TOTAL_DISCREP_COUNT { get; set; }

        [DisplayName("Кол-во расхождений по СФ")]
        [Description("Количество расхождений по СФ в НД")]
        public long? KNP_DISCREP_COUNT { get; set; }
        
        [DisplayName("Кол-во расхождений по КС")]
        [Description("Кол-во расхождений по КС в НД  ")]
        public long CONTROL_RATIO_DISCREP_COUNT { get; set; }


        #region Агрегированная информация по расхождениям

        [DisplayName("Минимальная")]
        [Description("Минимальная сумма расхождения по декларации")]
        [CurrencyFormat]
        public decimal? DISCREP_MIN_AMNT { get; set; }

        [DisplayName("Максимальная")]
        [Description("Максимальная сумма расхождения по декларации")]
        [CurrencyFormat]
        public decimal? DISCREP_MAX_AMNT { get; set; }

        [DisplayName("Средняя")]
        [Description("Средняя сумма расхождения = Общая сумма / кол-во расхождений")]
        [CurrencyFormat]
        public decimal? DISCREP_AVG_AMNT { get; set; }

        [DisplayName("Общая")]
        [Description("Общая сумма расхождений по декларации")]
        [CurrencyFormat]
        public decimal? DISCREP_TOTAL_AMNT { get; set; }


        [DisplayName("Общая по НД")]
        [Description("Общая сумма ПВП по НД")]
        [CurrencyFormat]
        public decimal? PVP_TOTAL_AMNT { get; set; }

        [DisplayName("Минимальная по расхождению")]
        [Description("Минимальная сумма ПВП расхождения в НД")]
        [CurrencyFormat]
        public decimal? PVP_DISCREP_MIN_AMNT { get; set; }

        [DisplayName("Максимальная по расхождению")]
        [Description("Максимальная сумма ПВП расхождения в НД")]
        [CurrencyFormat]
        public decimal? PVP_DISCREP_MAX_AMNT { get; set; }

        [DisplayName("Средняя")]
        [Description("Средняя сумма ПВП расхождения в НД")]
        [CurrencyFormat]
        public decimal? PVP_DISCREP_AVG_AMNT { get; set; }


        [DisplayName("Покупок")]
        [Description("Сумма ПВП расхождения по книге покупок")]
        [CurrencyFormat]
        public decimal? PVP_BUY_BOOK_AMNT { get; set; }

        [DisplayName("Продаж")]
        [Description("Сумма ПВП расхождения по книге продаж")]
        [CurrencyFormat]
        public decimal? PVP_SELL_BOOK_AMNT { get; set; }

        public decimal PVP_RECIEVE_JOURNAL_AMNT { get; set; }

        public decimal PVP_SENT_JOURNAL_AMNT { get; set; }


        [DisplayName("Покупок")]
        [Description("Сумма расхождений по книге покупок")]
        public decimal? DISCREP_BUY_BOOK_AMNT { get; set; }

        [DisplayName("Продаж")]
        [Description("Сумма расхождений по книге продаж")]
        public decimal? DISCREP_SELL_BOOK_AMNT { get; set; }

        #endregion

        [DisplayName("Дата публикации")]
        [Description("Дата публикации агрегированных данных в хранилище")]
        public DateTime? UPDATE_DATE { get; set; }

        [DisplayName("Инспектор")]
        [Description("ФИО инспектора")]
        public string INSPECTOR { get; set; }

        [DisplayName("Инспектор SID")]
        [Description("SID инспектора")]
        public string INSPECTORSID { get; set; }

        [DisplayName("СУР")]
        [Description("СУР налогоплательщика первичной отработки")]
        public int? SUR_CODE { get; set; }

        /// <summary>Дата и время проверки КС</summary>
        public DateTime? CONTROL_RATIO_DATE { get; set; }

        /// <summary>КС переданы в СЭОД</summary>
        public bool CONTROL_RATIO_SEND_TO_SEOD { get; set; }

        [DisplayName("Статус")]
        [Description("Статус декларации")]
        public string STATUS { get; set; }

        public bool Selected { get; set; }

        public bool Processed { get; set; }

        [DisplayName("Отчетный период")]
        [Description("Период за который подается  декларация/журнал")]
        public string FULL_TAX_PERIOD { get; set; }

        [DisplayName("Отчетный период")]
        [Description("Период за который подается  декларация/журнал")]
        public FullTaxPeriodInfo FullTaxPeriod { get; set; }

        /// <summary>
        /// список ревизий декларации
        /// </summary>
        public List<DeclarationVersion> Revisions { get; set; }

        /// <summary>
        /// Разделы 1-7 декларации сереализованные в XML
        /// </summary>
        public string DetailData { get; set; }

        #region Акты и решения

        /// <summary>
        /// Признак наличия открытых расхождений для КНП
        /// </summary>
        public bool HasKnpDiscrepancy
        {
            get;
            set;
        }

        /// <summary>
        /// Акт камеральной налоговой проверки связанный с данной декларацией (по ЭОД)
        /// </summary>
        public ActKNP ACTKNP { get; set; }

        /// <summary>
        /// Идентификатор акта с расхождениями КНП
        /// </summary>
        public long? ActId { get; set; }

        /// <summary>
        /// Признак того, что акт закрыт
        /// </summary>
        public bool ActIsClosed { get; set; }

        /// <summary>
        /// Идентификатор решения с расхождениями КНП
        /// </summary>
        public long? DecisionId { get; set; }

        /// <summary>
        /// Признак того, что решение закрыто
        /// </summary>
        public bool DecisionIsClosed { get; set; }

        public bool HasActDiscrepancy { get; set; }

        public bool HasDecisionDiscrepancy { get; set; }

        public bool HasSeodDecision { get; set; }

        /// <summary>
        /// Список документов - решений полученных в ЭОД-4 по данной декларации
        /// </summary>
        public KnpDecisionType[] SeodDecisionDocuments { get; set; }

        // TODO: кандидат на зачистку
        public DateTime? DateCloseKNP { get; set; }

        #endregion

        #region Информация о СФ

        public int? INVOICE_COUNT8 { get; set; }

        public int? INVOICE_COUNT81 { get; set; }

        public int? INVOICE_COUNT9 { get; set; }

        public int? INVOICE_COUNT91 { get; set; }

        public int? INVOICE_COUNT10 { get; set; }

        public int? INVOICE_COUNT11 { get; set; }

        public int? INVOICE_COUNT12 { get; set; }

        public int PRIORITY_08 { get; set; }

        public int PRIORITY_81 { get; set; }

        public int PRIORITY_09 { get; set; }

        public int PRIORITY_91 { get; set; }

        public int PRIORITY_10 { get; set; }

        public int PRIORITY_11 { get; set; }

        public int PRIORITY_12 { get; set; }

        public long? REQUEST_KEY8 { get; set; }

        public long? REQUEST_KEY81 { get; set; }

        public long? REQUEST_KEY9 { get; set; }

        public long? REQUEST_KEY91 { get; set; }

        public long? REQUEST_KEY10 { get; set; }

        public long? REQUEST_KEY11 { get; set; }

        public long? REQUEST_KEY12 { get; set; }

        public long? ACTUAL_ZIP8 { get; set; }

        public long? ACTUAL_ZIP81 { get; set; }

        public long? ACTUAL_ZIP9 { get; set; }

        public long? ACTUAL_ZIP91 { get; set; }

        public long? ACTUAL_ZIP10 { get; set; }

        public long? ACTUAL_ZIP11 { get; set; }

        public long? ACTUAL_ZIP12 { get; set; }
        #endregion

        #region DN 2015-01-13 рм инспектора

        [DisplayName("Признак записи")]
        [Description("Признак записи")]
        public string RECORD_MARK { get; set; }
        /*
        Т     – если есть автотребование/автоистребование по которому не получена квитанция о вручении НП в отведенный
              срок (т.е. срок ожидания вручения > «Отправлено налогоплательщику» + «Время ожидания вручения
              автотребования/автоистребования». Если методолог не указал время ожидания, то данный признак не устанаволивается)
        П     – если получено  на бумаге пояснение/ответ, но результат отработки инспектором не отправлен в МС
        Т-П   – если не получена квитанция по автотребованию/автоистребованию и есть не обработанные пояснения/ответы
        Н     – если получена новая декларация/журнал и/или новая версия декларации/журнала
        МНК   – если в декларации есть расхождение, которое перешло на этап «Прочие МНК»
        Пусто – если декларация/журнал не требуют вышеописанного внимания инспектора
        */

        #endregion

        #region Файлы с данными по СФ

        public string Chapter8xml { get; set; }
        public string Chapter81xml { get; set; }
        public string Chapter9xml { get; set; }
        public string Chapter91xml { get; set; }
        public string Chapter10xml { get; set; }
        public string Chapter11xml { get; set; }
        public string Chapter12xml { get; set; }

        #endregion

        /// <summary>
        /// Номер версии НД
        /// </summary>
        public string FormatVersion { get; set; }

        #region Сведения о текущем ПЦ

        /// <summary>
        /// счетчик ПЦ
        /// </summary>
        public long ComparisonDataVersion { get; set; }

        #endregion


        /// <summary>
        /// Причина закрытия КНП по корректировке
        /// </summary>
        public int? KnpClosedReasonId { get; set; }
    }
}