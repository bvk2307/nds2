﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration
{
    [Serializable]
    public class DeclarationRequestData
    {
        public long ID { get; private set; }
        public long DECLARATION_VERSION_ID { get; private set; }

        public string INN { get; private set; }
        public string CORRECTION_NUMBER { get; private set; }
        public string TAX_PERIOD { get; private set; }
        public string FISCAL_YEAR { get; private set; }
        
        public DeclarationRequestData(DeclarationSummary decl)
        {
            this.ID = decl.ID;
            this.DECLARATION_VERSION_ID = decl.DECLARATION_VERSION_ID;
            this.INN = decl.INN;
            this.CORRECTION_NUMBER = decl.CORRECTION_NUMBER;
            this.TAX_PERIOD = decl.TAX_PERIOD;
            this.FISCAL_YEAR = decl.FISCAL_YEAR;
        }
    }
}
