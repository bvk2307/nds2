using System;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration.Converters;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration.Enums;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace.Enums;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration.Extensions
{
    /// <summary> Auxilaries for <see cref="DeclarationBrief"/>. </summary>
    public static class DeclarationBriefExtensions
    {
        /// <summary> A type of declaration. </summary>
        /// <param name="declarationBrief"></param>
        /// <returns></returns>
        public static DeclarationTypeCode TypeCode( this DeclarationBrief declarationBrief )
        {
            return (DeclarationTypeCode)declarationBrief.DECL_TYPE_CODE;
        }

        public static bool HasDiscrepancies(this DeclarationBrief declaration)
        {
            return declaration != null && declaration.CHAPTER8_GAP_CNT +
                   declaration.CHAPTER8_OTHER_CONTRAGENT_CNT > 0;
        }

        /// <summary> A sign type of declaration ('Признак декларации') (see <see cref="DeclarationBrief.DECL_SIGN"/>). </summary>
        /// <param name="declarationBrief"></param>
        /// <exception cref="FormatException"> if <see cref="DeclarationBrief.DECL_SIGN"/> contains an invalid value or 'null'. </exception>
        /// <returns></returns>
        public static DeclarationType? SignType( this DeclarationBrief declarationBrief )
        {
            DeclarationType? signType = null;
            if ( declarationBrief.DECL_SIGN != null )
                signType = SignTypeConverter.ConvertFrom( declarationBrief.DECL_SIGN );

            return signType;
        }

        private static DeclarationSignTypeConverter SignTypeConverter
        {
            get { return DeclarationSignTypeConverter.Current; }
        }
   }
}