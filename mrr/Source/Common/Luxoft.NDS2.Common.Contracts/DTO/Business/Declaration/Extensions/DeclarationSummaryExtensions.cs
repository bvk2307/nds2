﻿using System;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration.Converters;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration.Enums;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace.Enums;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration.Extensions
{
    /// <summary> Auxilaries for <see cref="DeclarationSummary"/>. </summary>
    public static class DeclarationSummaryExtensions
    {
        /// <summary> A type of declaration. (see <see cref="declarationSummary.DECL_TYPE_CODE"/>). </summary>
        /// <param name="declarationSummary"></param>
        /// <returns></returns>
        public static DeclarationTypeCode TypeCode( this DeclarationSummary declarationSummary )
        {
            return (DeclarationTypeCode)declarationSummary.DECL_TYPE_CODE;
        }

        /// <summary> A sign type of declaration ('Признак декларации') (see <see cref="DeclarationSummary.DECL_SIGN"/>). </summary>
        /// <param name="declarationSummary"></param>
        /// <exception cref="FormatException"> if <see cref="DeclarationSummary.DECL_SIGN"/> contains an invalid value or 'null'. </exception>
        /// <returns></returns>
        public static DeclarationType? SignType( this DeclarationSummary declarationSummary )
        {
            DeclarationType? signType = null;
            if ( declarationSummary.DECL_SIGN != null )
                signType = SignTypeConverter.ConvertFrom( declarationSummary.DECL_SIGN );

            return signType;
        }

        private static DeclarationSignTypeConverter SignTypeConverter
        {
            get { return DeclarationSignTypeConverter.Current; }
        }
    }
}