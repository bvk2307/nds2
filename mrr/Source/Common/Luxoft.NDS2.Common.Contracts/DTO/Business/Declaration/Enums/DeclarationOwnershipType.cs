﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration.Enums
{
    public enum DeclarationOwnershipType
    {
        None = 0,
        AssignedToMe = 1,
        AssignedToOther = 2
    }
}
