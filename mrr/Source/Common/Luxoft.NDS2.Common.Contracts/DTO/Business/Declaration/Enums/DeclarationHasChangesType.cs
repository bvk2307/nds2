﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration.Enums
{

    public enum DeclarationHasChangesType
    {
        /// <summary>
        /// Нет изменений
        /// </summary>
        NoChanges = 0,

        /// <summary>
        /// Изменение есть
        /// </summary>
        Changes = 1
        ///// <summary>
        ///// Сформировано автотребование по СФ
        ///// </summary>
        //AtFormed = 1,
        ///// <summary>
        ///// НП не направлено автотребование по СФ
        ///// </summary>
        //NpNotDirectedAt = 2,
        ///// <summary>
        ///// Сформировано автотребование по СФ/Сформировано автотребование по СФ
        ///// </summary>
        //AtFormedAndNpNotDirectedAt = 3,
        ///// <summary>
        ///// Необходимо ввести пояснение
        ///// </summary>
        //NeedInputComment = 4,
        ///// <summary>
        ///// Сформировано автотребование по СФ/Необходимо ввести пояснение
        ///// </summary>
        //AtFormedAndNeedInputComment = 5,
        ///// <summary>
        ///// НП не направлено автотребование по СФ/Необходимо ввести пояснение
        ///// </summary>
        //NpNotDirectedAtAndNeedInputComment = 6,
        ///// <summary>
        ///// Сформировано автотребование по СФ/НП не направлено автотребование по СФ/Необходимо ввести пояснение
        ///// </summary>
        //All = 7
    }
}
