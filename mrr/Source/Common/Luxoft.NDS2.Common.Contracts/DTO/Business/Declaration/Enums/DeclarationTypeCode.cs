﻿namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration.Enums
{
    /// <summary> Full mapping to value of <see cref="DeclarationSummary.DECL_TYPE_CODE"/>. </summary>
    public enum DeclarationTypeCode
    {
        /// <summary> Declaration </summary>
        Declaration     = 0,
        /// <summary> Journal </summary>
        Journal         = 1
    }
}