using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration
{
    public partial class ASKSumOper1010450
    {
        public decimal Id { get; set; }
        public Nullable<decimal> IdDekl { get; set; }
        public string KodOper { get; set; }
        public Nullable<long> KorNalBazaUv { get; set; }
        public Nullable<long> KorIsch_164_23Uv { get; set; }
        public Nullable<long> KorNalBazaUm { get; set; }
        public Nullable<long> KorIsch_164_23Um { get; set; }
    }
}
