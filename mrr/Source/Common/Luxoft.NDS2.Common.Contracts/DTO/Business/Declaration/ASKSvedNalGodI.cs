using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration
{
    public partial class ASKSvedNalGodI
    {
        public decimal Id { get; set; }
        public Nullable<decimal> IdDekl { get; set; }
        public string KPPInUch { get; set; }
        public Nullable<long> SumNalIsch { get; set; }
        public Nullable<long> SumNalVych { get; set; }
    }
}
