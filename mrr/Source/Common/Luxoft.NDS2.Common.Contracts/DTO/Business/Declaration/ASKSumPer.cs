using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration
{
    public partial class ASKSumPer
    {
        public decimal Id { get; set; }
        public Nullable<decimal> IdDekl { get; set; }
        public string OtchetGod { get; set; }
        public string Period { get; set; }

        public List<ASKSumOper5> SumOper5 { get; set; }
    }
}
