﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration.Helpers
{
    /// <summary> Auxilaries around KPP code ('КПП') (see <see cref="DeclarationBrief.KPP"/>, <see cref="DeclarationSummary.KPP"/>, <see cref="TaxPayer.Kpp"/>. </summary>
    public static class KppCodeHelper
    {
        public const string LargeTaxPayerrCode = "01";

        /// <summary> Возвращает признак крупнейшего налогоплателщика (5 и 6 символ в кпп должен быть 01) |</summary>
        public static bool IsLargeTaxPayerByKpp( string kppCode )
        {
            bool isLarge = false;
            if ( kppCode != null && kppCode.Length >= 4 + 2 )
                isLarge = string.Equals( kppCode.Substring( 4, 2 ), LargeTaxPayerrCode, Constants.KeysComparison );

            return isLarge;
        }
    }
}