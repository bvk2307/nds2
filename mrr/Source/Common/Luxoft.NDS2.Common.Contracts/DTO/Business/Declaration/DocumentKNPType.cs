﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration
{
    [Serializable]
    public enum DocumentKNPType
    {
        Doc = 1,
        Explain = 2
    }
}
