﻿namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration
{
    //0 - пояснение загружено, не обработано
    //1 - по имени файла определены реквизиты поясняемой декларации (по NDS2_MRR_USER.V$I_MC_AT_REPLY)
    //2 - пояснение применено
    //3 - пояснение не применено, т.к. имя файла не уникально в ASKПояснение
    //4 - пояснение не применено, т.к. имя файла не уникально в NDS2_MRR_USER.V$I_MC_AT_REPLY
    //5 - пояснение не применено, т.к. имя файла не соответствует формату

    /// <summary>
    /// Стадии отработки декларации в АСК-НДС2
    /// </summary>
    public enum DeclarationProcessignStage
    {
        /// <summary>
        /// получено из СЭОД(ЭОД-1)
        /// </summary>
        Seod = 0,
        /// <summary>
        /// Получено в МС
        /// </summary>
        LoadedInMs = 1,
        /// <summary>
        /// Обработано МС
        /// </summary>
        ProcessedByMs = 2
    }
}
