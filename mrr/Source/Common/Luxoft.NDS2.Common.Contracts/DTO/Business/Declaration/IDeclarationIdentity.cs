﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration
{
    public interface IDeclarationIdentity
    {
        long ID { get; set; }
        long? SEOD_DECL_ID { get; set; }
        long DECLARATION_VERSION_ID { get; set; }
        string CORRECTION_NUMBER { get; set; }
        int DECL_TYPE_CODE { get; set; }
        DeclarationProcessignStage ProcessingStage { get; set; }
        string TAX_PERIOD { get; set; }
        string FISCAL_YEAR { get; set; }
    }
}
