﻿namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration
{
    public enum DeclarationSummaryState
    {
        Unavailable = 0,
        Nonactual = 1,
        Actual = 2
    }
}
