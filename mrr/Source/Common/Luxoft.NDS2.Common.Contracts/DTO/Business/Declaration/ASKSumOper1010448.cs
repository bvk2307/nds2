using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration
{
    public partial class ASKSumOper1010448
    {
        public decimal Id { get; set; }
        public Nullable<decimal> IdDekl { get; set; }
        public string KodOper { get; set; }
        public Nullable<long> KorNalBazaUv { get; set; }
        public Nullable<long> KorNalBazaUm { get; set; }
    }
}
