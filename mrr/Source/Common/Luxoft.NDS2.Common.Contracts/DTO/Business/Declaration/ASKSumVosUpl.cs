using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration
{
    public partial class ASKSumVosUpl
    {
        public decimal Id { get; set; }
        public Nullable<decimal> IdDekl { get; set; }
        public string NaimNedv { get; set; }
        public string KodOpNedv { get; set; }
        public Nullable<System.DateTime> DataVvodON { get; set; }
        public Nullable<System.DateTime> DataNachAmOtch { get; set; }
        public Nullable<long> StVvodON { get; set; }
        public Nullable<long> NalVychON { get; set; }
        public string Indeks { get; set; }
        public string KodRegion { get; set; }
        public string Rajon { get; set; }
        public string Gorod { get; set; }
        public string NaselPunkt { get; set; }
        public string Ulica { get; set; }
        public string Dom { get; set; }
        public string Korpus { get; set; }
        public string Kvart { get; set; }

        public List<ASKSvedNalGod> SvedNalGod { get; set; }
        
        public string NaimOOS { get; set; }
        public string KodOpOOS { get; set; }
        public DateTime? DataVvodOOC { get; set; }
        public int? StVvodOOS { get; set; }
        public int? NalVychOOS { get; set; }
    }
}
