﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Book
{
    [Serializable]
    public class DocumentNumDate
    {
        public string Num { get; set; }
        public DateTime? Date { get; set; }
        public string DateString { get; set; }

        public override string ToString()
        {
            if (!string.IsNullOrEmpty(Num))
            {
                if (Date != null)
                    return string.Format("{0} от {1}", Num, Date.Value.ToShortDateString());
                else if (!string.IsNullOrWhiteSpace(DateString))
                    return string.Format("{0} от {1}", Num, DateString);
                else
                    return Num;
            }
            else
            {
                if (Date != null)
                    return string.Format("от {0}", Date.Value.ToShortDateString());
                else if (!string.IsNullOrWhiteSpace(DateString))
                    return string.Format("от {0}", DateString);
                else
                    return String.Empty;
            }
        }
    }

    [Serializable]
    public class DocumentNumDates
    {
        public string RawString { get; set; }

        public List<DocumentNumDate> PaymentDocs { get; set; }

        public DocumentNumDates()
        {
            PaymentDocs = new List<DocumentNumDate>();
        }

        public override string ToString()
        {
            var stringBuilder = new StringBuilder();
            bool isFirstItem = true;
            foreach (var item in PaymentDocs)
            {
                string oneDocument = item.ToString();
                if (oneDocument.Length > 0)
                {
                    if (isFirstItem)
                        isFirstItem = false;
                    else
                        stringBuilder.Append(", ");

                    stringBuilder.AppendFormat("'{0}'", item.ToString());
                }
            }
            return stringBuilder.ToString();
        }
    }

    [Serializable]
    public class DocDates
    {
        public string RawString { get; set; }
        public List<DateTime> Dates { get; set; }
        public List<string> DateStrings { get; set; }
        private string Values { get; set; }

        public DocDates(DateTime? dt)
        {
            Values = String.Empty;
            RawString = String.Empty;
            Dates = new List<DateTime>();
            DateStrings = new List<string>();

            if (dt != null)
            {
                Dates.Add((DateTime)dt);
                Values = dt.Value.ToShortDateString();
                DateStrings.Add(Values);
                RawString = Values;
            }
        }

        public DocDates(string values)
        {
            Values = String.Empty;
            RawString = values;
            Dates = new List<DateTime>();
            DateStrings = new List<string>();

            if (!String.IsNullOrEmpty(values))
            {
                StringBuilder sb = new StringBuilder();
                bool isFirst = true;
                List<string> words = values.Split(',').ToList();
                foreach (string itemValue in words)
                {
                    if (!string.IsNullOrWhiteSpace(itemValue))
                    {
                        string val = itemValue.Trim();
                        DateStrings.Add(val);
                        DateTime dtParse = DateTime.Now;
                        if (TryParseDataTime(val, out dtParse))
                        {
                            Dates.Add(dtParse);
                            if (!isFirst)
                                sb.Append(", ");
                            else
                                isFirst = false;
                            sb.Append(dtParse.ToShortDateString());
                        }
                    }
                }
                Values = sb.ToString();
            }
        }

        private bool TryParseDataTime(string value, out DateTime resultValue)
        {
            bool ret = false;
            resultValue = DateTime.MinValue;

            DateTime dtParse = DateTime.Now;
            if (DateTime.TryParse(value, out dtParse))
            {
                resultValue = dtParse;
                ret = true;
            }
            else
            {
                string[] formats = { "yyyyMMdd" };

                if (DateTime.TryParseExact(
                    value,
                    formats,
                    System.Globalization.CultureInfo.InvariantCulture,
                    System.Globalization.DateTimeStyles.None,
                    out dtParse))
                {
                    resultValue = dtParse;
                    ret = true;
                }
            }

            return ret;
        }

        public DateTime? FirstDate()
        {
            return Dates.FirstOrDefault();
        }

        public override string ToString()
        {
            return Values;
        }
    }
}
