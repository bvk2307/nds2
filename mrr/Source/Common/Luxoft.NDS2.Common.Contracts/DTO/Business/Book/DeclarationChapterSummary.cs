﻿using System;
using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Book
{
    [Serializable]
    public class DeclarationChapterSummary
    {
        public AskInvoiceChapterNumber Part
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает признак того, что данные счетов-фактур загружены
        /// </summary>
        public bool DataReady
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает общее количество счетов-фактур в разделе
        /// </summary>
        public int AvailableQuantity
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает количество счетов-фактур в разделе, удовлетворяющих условию выборки
        /// </summary>
        public int MatchesQuantity
        {
            get;
            set;
        }
    }

    [Serializable]
    public class DeclarationChapterData
    {
        public List<DeclarationChapterSummary> PartDataList
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает данные счетов-фактур, удовлетворяющих условию выборки
        /// </summary>
        public List<Invoice> Invoices
        {
            get;
            set;
        }

        public int MatchesQuantity
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает справочников итоговых сумм (ключ - название поля, значение - сумма)
        /// </summary>
        public Dictionary<string, decimal> Summaries
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает доступность данных разделов в БД
        /// </summary>
        public bool UsableData
        {
            get;
            set;
        }

        public DeclarationChapterData(DeclarationChapterDataRequest request)
        {
            PartDataList = new List<DeclarationChapterSummary>();
            foreach (var cache in request.ClientCache)
            {
                var data = new DeclarationChapterSummary
                {
                    Part = cache.Part,
                    DataReady = cache.DataReady
                };
                PartDataList.Add(data);
            }
        }

        public DeclarationChapterData()
        {
            PartDataList = new List<DeclarationChapterSummary>();
        }
    }

    public static class DeclarationChapterSummaryListHelper
    {
        public static DeclarationChapterSummary Find(
            this IEnumerable<DeclarationChapterSummary> list, 
            AskInvoiceChapterNumber chapterNumber)
        {
            return list.FirstOrDefault(summary => summary.Part == chapterNumber);
        }

        public static DeclarationChapterSummary Find(
            this IEnumerable<DeclarationChapterSummary> list,
            DeclarationInvoicePartitionNumber partition)
        {
            return list.Find(partition.ToAskNumber());
        }
    }
}
