﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Book
{
    // ReSharper disable InconsistentNaming
    [Serializable]
    public class Invoice
    {
        [DisplayName("Признак замены записи")]
        [Description("Признак редактирования записи в ходе полученного пояснения/ответа от НП")]
        public int? IS_CHANGED { get; set; }

        /// <summary>
        /// Номер пояснения/ответа от НП
        /// </summary>
        public string ExplainNum { get; set; }

        /// <summary>
        /// Дата пояснения/ответа от НП
        /// </summary>
        public DateTime? ExplainDate { get; set; }

        /// <summary>
        /// Тип пояснения/ответа от НП
        /// </summary>
        public int ExplainType { get; set; }

        public bool IS_MATCHING_ROW { get; set; }

        public long? CONTRACTOR_KEY { get; set; }
        public bool CONTRACTOR_DECL_IN_MC { get; set; }

        [DisplayName("№\n\r(стр. 005)")]
        [Description("Порядковый номер")]
        public long? ORDINAL_NUMBER { get; set; }


        [DisplayName("Код вида операции\n\r(стр. 010)")]
        [Description("Код вида операции")]
        public string OPERATION_CODE { get; set; }

        [DisplayName("№\n\r(стр. 020)")]
        [Description("Номер счета-фактуры")]
        public string INVOICE_NUM { get; set; }

        [DisplayName("Дата\n\r(стр. 030)")]
        [Description("Дата счета-фактуры")]
        public DateTime? INVOICE_DATE { get; set; }

        [DisplayName("№\n\r(стр. 040)")]
        [Description("Номер исправления счета-фактуры продавца")]
        public string CHANGE_NUM { get; set; }

        [DisplayName("Дата\n\r(стр. 050)")]
        [Description("Дата исправления счета-фактуры продавца")]
        public DateTime? CHANGE_DATE { get; set; }

        [DisplayName("№\n\r(стр. 060)")]
        [Description("Номер корректировочного счета-фактуры продавца")]
        public string CORRECTION_NUM { get; set; }

        [DisplayName("Дата\n\r(стр. 070)")]
        [Description("Дата корректировочного счета-фактуры продавца")]
        public DateTime? CORRECTION_DATE { get; set; }

        [DisplayName("№\n\r(стр. 080)")]
        [Description("Номер исправления корректировочного счета-фактуры продавца")]
        public string CHANGE_CORRECTION_NUM { get; set; }

        [DisplayName("Дата\n\r(стр. 090)")]
        [Description("Дата исправления корректировочного счета-фактуры продавца")]
        public DateTime? CHANGE_CORRECTION_DATE { get; set; }

        [DisplayName("Документ, подтверждающий уплату налога\n\r(стр. 100)")]
        [Description("Номер документа, подтверждающего уплату налога")]
        public string RECEIPT_DOC_NUM { get; set; }

        [DisplayName("Дата документа\n\r(стр. 110)")]
        [Description("Дата документа, подтверждающего уплату налога")]
        public DateTime? RECEIPT_DOC_DATE { get; set; }

        //--- Номер и дата документа (множественное значение) 
        public DocumentNumDates RECEIPT_DOC_DATE_STR { get; set; }

        [DisplayName("Дата принятия на учет\n\r(стр. 120)")]
        [Description("Дата принятия на учет товаров (работ, услуг), имущественных прав")]
        public DocDates BUY_ACCEPT_DATE { get; set; }

        [DisplayName("ИНН\n\r(стр. 130)")]
        [Description("ИНН продавца")]
        public string SELLER_INN { get; set; }

        [DisplayName("КПП\n\r(стр. 130)")]
        [Description("КПП продавца")]
        public string SELLER_KPP { get; set; }

        [DisplayName("ИНН\n\r(стр. 130)")]
        [Description("ИНН продавца")]
        public string SELLER_INN_RESOLVED { get; set; }

        [DisplayName("КПП\n\r(стр. 130)")]
        [Description("КПП продавца")]
        public string SELLER_KPP_RESOLVED { get; set; }

        [DisplayName("Наименование")]
        [Description("Наименование продавца")]
        public string SELLER_NAME { get; set; }

        [DisplayName("ИНН\n\r(стр. 140)")]
        [Description("ИНН посредника(комиссионера, агента, экспедитора или застройщика)")]
        public string BROKER_INN { get; set; }

        [DisplayName("КПП\n\r(стр. 140)")]
        [Description("КПП посредника (комиссионера, агента, экспедитора или застройщика)")]
        public string BROKER_KPP { get; set; }

        [DisplayName("Наименование")]
        [Description("Наименование посредника(комиссионера, агента, экспедитора или застройщика)")]
        public string BROKER_NAME { get; set; }

        [DisplayName("Дата выставления")]
        [Description("Дата выставления")]
        public DateTime? CREATE_DATE { get; set; }

        [DisplayName("Дата получения")]
        [Description("Дата получения")]
        public DateTime? RECEIVE_DATE { get; set; }

        public long DeclarationId { get; set; }

        public int CHAPTER { get; set; }

        [DisplayName("ИНН\n\r(стр. 100)")]
        [Description("ИНН покупателя")]
        public string BUYER_INN { get; set; }

        [DisplayName("КПП\n\r(стр. 100)")]
        [Description("КПП покупателя")]
        public string BUYER_KPP { get; set; }

        [DisplayName("Наименование")]
        [Description("Наименование покупателя")]
        public string BUYER_NAME { get; set; }

        [DisplayName("№ ТД\n\r(стр. 150)")]
        [Description("Номер таможенной декларации")]
        public string CUSTOMS_DECLARATION_NUM { get; set; }

        /// <summary>
        /// Полный список номеров таможенных деклараций
        /// </summary>
        public string[] CUSTOMS_DECLARATION_NUMBERS { get; set; }

        [DisplayName("Код валюты\n\r(стр. 160)")]
        [Description("Код валюты по ОКВ")]
        public string OKV_CODE { get; set; }

        [DisplayName("Стоимость покупок с НДС\n\r(стр. 170)")]
        [Description("Стоимость покупок по счету-фактуре, разница стоимости по корректировочному счету-фактуре (включая налог), в валюте счета-фактуры")]
        public decimal? PRICE_BUY_AMOUNT { get; set; }

        [DisplayName("Сумма НДС\n\r(стр. 180)")]
        [Description("Сумма налога по счету-фактуре, разница суммы налога по корректировочному счету-фактуре принимаемая к вычету в рублях и копейках")]
        public decimal? PRICE_BUY_NDS_AMOUNT { get; set; }

        [DisplayName("№")]
        [Description("Номер счета-фактуры продавца")]
        public string SELLER_INVOICE_NUM { get; set; }

        [DisplayName("Дата")]
        [Description("Дата счета-фактуры продавца")]
        public DateTime? SELLER_INVOICE_DATE { get; set; }

        [DisplayName("Код вида сделки")]
        [Description("Код вида сделки")]
        public int? DEAL_KIND_CODE { get; set; }

        [DisplayName("в руб. и коп.")]
        [Description("Стоимость продаж по счету-фактуре, разница стоимости  по корректировочному счету-фактуре (включая налог) в рублях и копейках ")]
        public decimal? PRICE_SELL { get; set; }

        [DisplayName("в валюте")]
        [Description("Стоимость продаж по счету-фактуре, разница стоимости  по корректировочному счету-фактуре (включая налог), в валюте счета-фактуры")]
        public decimal? PRICE_SELL_IN_CURR { get; set; }

        [DisplayName("18%\n\r(стр. 170)")]
        [Description("Стоимость продаж, облагаемых налогом, по счету-фактуре, разница стоимости по корректировочному счету-фактуре (без налога) в рублях и копейках, по ставке 18 %")]
        public decimal? PRICE_SELL_18 { get; set; }

        [DisplayName("10%\n\r(стр. 180)")]
        [Description("Стоимость продаж, облагаемых налогом, по счету-фактуре, разница стоимости по корректировочному счету-фактуре (без налога) в рублях и копейках, по ставке 10 %")]
        public decimal? PRICE_SELL_10 { get; set; }

        [DisplayName("0%\n\r(стр. 190)")]
        [Description("Стоимость продаж, облагаемых налогом, по счету-фактуре, разница стоимости по корректировочному счету-фактуре (без налога) в рублях и копейках, по ставке 0 %")]
        public decimal? PRICE_SELL_0 { get; set; }

        [DisplayName("18%\n\r(стр. 200)")]
        [Description("Сумма налога по счету-фактуре, разница суммы налога по корректировочному счету-фактуре, в рублях и копейках, по ставке 18 %")]
        public decimal? PRICE_NDS_18 { get; set; }

        [DisplayName("10%\n\r(стр. 210)")]
        [Description("Сумма налога по счету-фактуре, разница суммы налога по корректировочному счету-фактуре, в рублях и копейках, по ставке 10 %")]
        public decimal? PRICE_NDS_10 { get; set; }

        [DisplayName("Стоимость продаж, освобождаемых он налога\n\r(стр. 220)")]
        [Description("Стоимость продаж, освобождаемых от налога, по счету-фактуре, разница стоимости по корректировочному счету-фактуре, в рублях и копейках")]
        public decimal? PRICE_TAX_FREE { get; set; }

        [DisplayName("Стоимость товаров")]
        [Description("Стоимость товаров (работ, услуг), имущественных прав по счету-фактуре - всего")]
        public decimal? PRICE_TOTAL { get; set; }

        [DisplayName("Сумма НДС")]
        [Description("В том числе сумма НДС по счету-фактуре")]
        public decimal? PRICE_NDS_TOTAL { get; set; }

        [DisplayName("уменьшение")]
        [Description("Разница налога по корректировочному счету-фактуре – уменьшение")]
        public decimal? DIFF_CORRECT_DECREASE { get; set; }

        [DisplayName("увеличение")]
        [Description("Разница налога по корректировочному счету-фактуре - увеличение")]
        public decimal? DIFF_CORRECT_INCREASE { get; set; }

        [DisplayName("уменьшение")]
        [Description("Разница стоимости с учетом налога по корректировочному счету-фактуре - уменьшение")]
        public decimal? DIFF_CORRECT_NDS_DECREASE { get; set; }

        [DisplayName("увеличение")]
        [Description("Разница стоимости с учетом налога по корректировочному счету-фактуре - увеличение")]
        public decimal? DIFF_CORRECT_NDS_INCREASE { get; set; }

        [DisplayName("№ коррект.")]
        [Description("Номер корректировки декларации")]
        public string DECL_CORRECTION_NUM { get; set; }

        [DisplayName("Сумма НДС")]
        [Description("Сумма налога, предъявляемая покупателю, в руб. и коп.")]
        public decimal? PRICE_NDS_BUYER { get; set; }

        [DisplayName("Отчетный период")]
        [Description("Отчетный период")]
        public string FULL_TAX_PERIOD { get; set; }

        public string TAX_PERIOD { get; set; }
        public string FISCAL_YEAR { get; set; }

        [DisplayName("Доп.лист")]
        [Description("Сведения из дополнительного листа")]
        public bool IsAdditionalSheet { get; set; }

        /// <summary>
        /// Сведения о посреднической деятельности продавца: ИНН
        /// </summary>
        public string SELLER_AGENCY_INFO_INN { get; set; }
        /// <summary>
        /// Сведения о посреднической деятельности продавца: КПП
        /// </summary>
        public string SELLER_AGENCY_INFO_KPP { get; set; }
        /// <summary>
        /// Сведения о посреднической деятельности продавца: Наименование
        /// </summary>
        public string SELLER_AGENCY_INFO_NAME { get; set; }
        /// <summary>
        /// Сведения о посреднической деятельности продавца: № СФ
        /// </summary>
        public string SELLER_AGENCY_INFO_NUM { get; set; }
        /// <summary>
        /// Сведения о посреднической деятельности продавца: дата СФ
        /// </summary>
        public DateTime? SELLER_AGENCY_INFO_DATE { get; set; }

        /// <summary>
        /// Идентификатор записи в Hbase
        /// </summary>
        public string ROW_KEY { get; set; }
        /// <summary>
        /// Идентификатор актуальной записи в Hbase
        /// </summary>
        public string ACTUAL_ROW_KEY { get; set; }
        /// <summary>
        /// Идентификатор записи в Hbase с которой было произведено сопоставление
        /// </summary>
        public string COMPARE_ROW_KEY { get; set; }
        /// <summary>
        /// Алгоритм по которому было сопоставление
        /// </summary>
        public int COMPARE_ALGO_ID { get; set; }
        /// <summary>
        /// Форматные ошибки
        /// </summary>
        public string FORMAT_ERRORS { get; set; }
        /// <summary>
        /// Логические ошибки
        /// </summary>
        public string LOGICAL_ERRORS { get; set; }

        /// <summary>
        /// Id запроса в СОВ
        /// </summary>
        public long REQUEST_ID { get; set; }

        public Invoice()
        {
        }
    }

    [Serializable]
    public class InvoiceRelated : Invoice
    {
        public InvoiceRelated Parent { get; set; }
        public List<InvoiceRelated> Children { get; set; }
        public bool IsChildren { get; private set; }

        public long DocId { get; set; }

        # region Множественные атрибуты

        public ExplainInvoiceAttributes Attributes { get; private set; }

        # endregion

        [DisplayName("Дата принятия на учет\n\r(стр. 120)")]
        [Description("Дата принятия на учет товаров (работ, услуг), имущественных прав")]
        public string BUY_ACCEPT_DATE_VIEW { get; set; }

        [DisplayName("Документ, подтверждающий уплату налога\n\r(стр. 100)")]
        [Description("Номер документа, подтверждающего уплату налога")]
        public string RECEIPT_DOC_NUM_VIEW { get; set; }

        [DisplayName("Дата документа\n\r(стр. 110)")]
        [Description("Дата документа, подтверждающего уплату налога")]
        public string RECEIPT_DOC_DATE_VIEW { get; set; }

        [DisplayName("Период отображения записи")]
        [Description("Отчетный период НД, в которой данная запись была отражена")]
        public string DISPLAY_FULL_TAX_PERIOD { get; set; }

        public InvoiceRelated()
        {
            Attributes = new ExplainInvoiceAttributes
            {
                Buyers = new List<InvoiceContractor>(),
                Sellers = new List<InvoiceContractor>(),
                Operations = new List<InvoiceOperation>(),
                PaymentDocuments = new List<InvoicePaymentDocument>(),
                BuyAccepts = new List<InvoiceBuyAccept>()
            };
        }

        public void Correct(IEnumerable<InvoiceCorrection> corrections)
        {
            var typeClass = this.GetType();
            var properties = typeClass.GetProperties();
            foreach (var itemCorr in corrections)
            {
                var itemProp = properties.SingleOrDefault(p => p.Name == itemCorr.FIELD_NAME);
                if (itemProp == null)
                    continue;
                try
                {
                    var t = Nullable.GetUnderlyingType(itemProp.PropertyType) ?? itemProp.PropertyType;
                    var value = (itemCorr.FIELD_VALUE == null) ? null : Convert.ChangeType(itemCorr.FIELD_VALUE, t);
                    itemProp.SetValue(this, value, null);
                }
                catch {}
            }
        }

        public InvoiceRelated BornChild()
        {
            var obj = new InvoiceRelated
            {
                ACTUAL_ROW_KEY = this.ACTUAL_ROW_KEY,
                BROKER_INN = this.BROKER_INN,
                BROKER_KPP = this.BROKER_KPP,
                BROKER_NAME = this.BROKER_NAME,
                BUY_ACCEPT_DATE = this.BUY_ACCEPT_DATE,
                BUYER_INN = this.BUYER_INN,
                BUYER_KPP = this.BUYER_KPP,
                BUYER_NAME = this.BUYER_NAME,
                CHANGE_CORRECTION_DATE = this.CHANGE_CORRECTION_DATE,
                CHANGE_CORRECTION_NUM = this.CHANGE_CORRECTION_NUM,
                CHANGE_DATE = this.CHANGE_DATE,
                CHANGE_NUM = this.CHANGE_NUM,
                CHAPTER = this.CHAPTER,
                COMPARE_ALGO_ID = this.COMPARE_ALGO_ID,
                COMPARE_ROW_KEY = this.COMPARE_ROW_KEY,
                CORRECTION_DATE = this.CORRECTION_DATE,
                CORRECTION_NUM = this.CORRECTION_NUM,
                CREATE_DATE = this.CREATE_DATE,
                CUSTOMS_DECLARATION_NUM = this.CUSTOMS_DECLARATION_NUM,
                DEAL_KIND_CODE = this.DEAL_KIND_CODE,
                DECL_CORRECTION_NUM = this.DECL_CORRECTION_NUM,
                DeclarationId = this.DeclarationId,
                DIFF_CORRECT_DECREASE = this.DIFF_CORRECT_DECREASE,
                DIFF_CORRECT_INCREASE = this.DIFF_CORRECT_INCREASE,
                DIFF_CORRECT_NDS_DECREASE = this.DIFF_CORRECT_NDS_DECREASE,
                DIFF_CORRECT_NDS_INCREASE = this.DIFF_CORRECT_NDS_INCREASE,
                FISCAL_YEAR = this.FISCAL_YEAR,
                FORMAT_ERRORS = this.FORMAT_ERRORS,
                FULL_TAX_PERIOD = this.FULL_TAX_PERIOD,
                INVOICE_DATE = this.INVOICE_DATE,
                INVOICE_NUM = this.INVOICE_NUM,
                LOGICAL_ERRORS = this.LOGICAL_ERRORS,
                OKV_CODE = this.OKV_CODE,
                OPERATION_CODE = this.OPERATION_CODE,
                ORDINAL_NUMBER = this.ORDINAL_NUMBER,
                PRICE_BUY_AMOUNT = this.PRICE_BUY_AMOUNT,
                PRICE_BUY_NDS_AMOUNT = this.PRICE_BUY_NDS_AMOUNT,
                PRICE_NDS_10 = this.PRICE_NDS_10,
                PRICE_NDS_18 = this.PRICE_NDS_18,
                PRICE_NDS_BUYER = this.PRICE_NDS_BUYER,
                PRICE_NDS_TOTAL = this.PRICE_NDS_TOTAL,
                PRICE_SELL = this.PRICE_SELL,
                PRICE_SELL_0 = this.PRICE_SELL_0,
                PRICE_SELL_10 = this.PRICE_SELL_10,
                PRICE_SELL_18 = this.PRICE_SELL_18,
                PRICE_SELL_IN_CURR = this.PRICE_SELL_IN_CURR,
                PRICE_TAX_FREE = this.PRICE_TAX_FREE,
                PRICE_TOTAL = this.PRICE_TOTAL,
                IsAdditionalSheet = this.IsAdditionalSheet,
                RECEIPT_DOC_DATE = this.RECEIPT_DOC_DATE,
                RECEIPT_DOC_NUM = this.RECEIPT_DOC_NUM,
                RECEIVE_DATE = this.RECEIVE_DATE,
                ROW_KEY = this.ROW_KEY,
                SELLER_AGENCY_INFO_DATE = this.SELLER_AGENCY_INFO_DATE,
                SELLER_AGENCY_INFO_INN = this.SELLER_AGENCY_INFO_INN,
                SELLER_AGENCY_INFO_KPP = this.SELLER_AGENCY_INFO_KPP,
                SELLER_AGENCY_INFO_NAME = this.SELLER_AGENCY_INFO_NAME,
                SELLER_AGENCY_INFO_NUM = this.SELLER_AGENCY_INFO_NUM,
                SELLER_INN = this.SELLER_INN,
                SELLER_KPP = this.SELLER_KPP,
                SELLER_INN_RESOLVED = this.SELLER_INN_RESOLVED,
                SELLER_KPP_RESOLVED = this.SELLER_KPP_RESOLVED,
                SELLER_INVOICE_DATE = this.SELLER_INVOICE_DATE,
                SELLER_INVOICE_NUM = this.SELLER_INVOICE_NUM,
                SELLER_NAME = this.SELLER_NAME,
                TAX_PERIOD = this.TAX_PERIOD,
                BUY_ACCEPT_DATE_VIEW = this.BUY_ACCEPT_DATE_VIEW,
                RECEIPT_DOC_DATE_VIEW = this.RECEIPT_DOC_DATE_VIEW,
                RECEIPT_DOC_NUM_VIEW = this.RECEIPT_DOC_NUM_VIEW,
                DISPLAY_FULL_TAX_PERIOD = this.DISPLAY_FULL_TAX_PERIOD,
                IsChildren = true,
                Parent = this
            };

            foreach (var item in this.Attributes.Buyers)
                obj.Attributes.Buyers.Add((InvoiceContractor)item.Clone());

            foreach (var item in this.Attributes.Sellers)
                obj.Attributes.Sellers.Add((InvoiceContractor)item.Clone());

            foreach (var item in this.Attributes.BuyAccepts)
                obj.Attributes.BuyAccepts.Add((InvoiceBuyAccept)item.Clone());

            foreach (var item in this.Attributes.Operations)
                obj.Attributes.Operations.Add((InvoiceOperation)item.Clone());

            foreach (var item in this.Attributes.PaymentDocuments)
                obj.Attributes.PaymentDocuments.Add((InvoicePaymentDocument)item.Clone());

            obj.SetMultipleAttributes();

            if (Children == null)
                Children = new List<InvoiceRelated> { obj };
            else
                Children.Add(obj);

            return obj;
        }

        public void SetMultipleAttributes()
        {
            BUYER_INN = string.Join(",", Attributes.Buyers.Select(x => x.Inn).ToArray());
            BUYER_KPP = string.Join(",", Attributes.Buyers.Select(x => x.Kpp).ToArray());
            SELLER_INN = string.Join(",", Attributes.Sellers.Select(x => x.Inn).ToArray());
            SELLER_KPP = string.Join(",", Attributes.Sellers.Select(x => x.Kpp).ToArray());
            BUY_ACCEPT_DATE_VIEW =
                string.Join(
                    ",",
                    Attributes.BuyAccepts
                        .Where(x => x.AcceptedAt.HasValue)
                        .Select(x => x.AcceptedAt.Value.ToShortDateString())
                        .ToArray());
            OPERATION_CODE = string.Join(",", Attributes.Operations.Select(x => x.Code).ToArray());
            RECEIPT_DOC_NUM_VIEW = string.Join(",", Attributes.PaymentDocuments.Select(x => x.Number).ToArray());
            RECEIPT_DOC_DATE_VIEW =
                string.Join(
                ",",
                Attributes.PaymentDocuments
                .Where(x => x.Date.HasValue)
                .Select(x => x.Date.Value.ToShortDateString()).ToArray());
        }
    }

    [Serializable]
    public class InvoiceCorrection
    {
        public long EXPLAIN_ID;
        public string INVOICE_ORIGINAL_ID;
        public string FIELD_NAME;
        public string FIELD_VALUE;
    }

    public static class InvoiceRelatedExtensions
    {
        public static string GetCorrection(this IEnumerable<InvoiceCorrection> corrections, string field)
        {
            if (corrections == null)
                return null;
            var c = corrections.FirstOrDefault(x => x.FIELD_NAME == field);
            return c == null ? null : c.FIELD_VALUE;
        }
    }

    // ReSharper restore InconsistentNaming
}
