﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Book
{
    [Serializable]
    public class InvoiceOperation : ICloneable
    {
        public string InvoiceKey { get; set; }

        public string Code { get; set; }

        public object Clone()
        {
            var obj = new InvoiceOperation();

            obj.InvoiceKey = this.InvoiceKey;
            obj.Code = this.Code;

            return obj;
        }
    }
}
