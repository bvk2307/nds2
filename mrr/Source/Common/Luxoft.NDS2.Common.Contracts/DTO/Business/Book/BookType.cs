﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.SellBook
{
    public enum BookType : int
    {
        Sell = 0,
        Buy = 1
    }
}
