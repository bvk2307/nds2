﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Book
{
    /// <summary>
    /// Этот класс описывает набор данных, кэшируемых клиентом для повторного использования при обработки запроса счетов-фактур раздела декларации
    /// </summary>
    [Serializable]
    public class DeclarationInvoiceChapterInfo
    {
        /// <summary>
        /// Номер раздела 8, 8.1 и т.д.
        /// </summary>
        public AskInvoiceChapterNumber Part
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает ZIP актуальной корректировки для раздела
        /// </summary>
        public long Zip
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает признак готовности данных
        /// </summary>
        public bool DataReady
        {
            get;
            set;
        }

        /// <summary>
        /// Приоритет запроса СОВ (зависит от числа записей)
        /// </summary>
        public int Priority { get; set; }

        /// <summary>
        /// Число записей
        /// </summary>
        public int TotalQuantity { get; set; }

        #region Поля для сумарной строки
        public decimal SUM_PRICE_BUY_AMOUNT { get; set; }

        public decimal SUM_PRICE_BUY_NDS_AMOUNT { get; set; }

        public decimal SUM_PRICE_SELL { get; set; }

        public decimal SUM_PRICE_SELL_18 { get; set; }

        public decimal SUM_PRICE_SELL_10 { get; set; }

        public decimal SUM_PRICE_SELL_0 { get; set; }

        public decimal SUM_PRICE_NDS_18 { get; set; }

        public decimal SUM_PRICE_NDS_10 { get; set; }

        public decimal SUM_PRICE_TAX_FREE { get; set; }

        public decimal SUM_PRICE_TOTAL { get; set; }

        public decimal SUM_PRICE_NDS_TOTAL { get; set; }
        #endregion
    }
}
