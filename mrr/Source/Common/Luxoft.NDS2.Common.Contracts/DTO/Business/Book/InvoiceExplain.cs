﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Book
{
    [Serializable]
    public class InvoiceExplain
    {
        public string InvoiceKey { get; set; }

        public int DocumentType { get; set; }

        public string DocumentNumber { get; set; }

        public DateTime? DocumentDate { get; set; }

        public int Rank { get; set; }
    }

    [Serializable]
    public enum InvoiceExplainState
    {
        Processed = 0,
        Changing = 1
    }
}
