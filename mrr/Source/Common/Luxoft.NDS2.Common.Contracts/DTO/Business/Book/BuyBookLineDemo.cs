﻿using System;
using System.ComponentModel;
using Luxoft.NDS2.Common.Contracts.CustomAttributes;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Book
{
    [Serializable]
    public class BuyBookLineDemo : BusinessObject
    {
        [DisplayName("1")]
        [Description("Номер в книге покупок")]
        public long Id { get; set; }

        [DisplayName("2")]
        [Description("Код вида операции ")]
        public string OperationCode { get; set; }
        
        [DisplayName("3")]
        [Description("Номер счета-фактуры продавца")]
        public string SellerInvoiceNum { get; set; }

        [DisplayName("3а")]
        [Description("Дата счета-фактуры продавца")]
        public DateTime? SellerInvoiceDate { get; set; }

        [DisplayName("4")]
        [Description("Номер исправления счета-фактуры продавца")]
        public string SellerInvoiceNumChanged { get; set; }

        [DisplayName("4а")]
        [Description("Дата исправления счета-фактуры продавца")]
        public DateTime? SellerInvoiceDateChanged { get; set; }

        [DisplayName("5")]
        [Description("Номер корректировочного счета-фактуры продавца")]
        public string SellerInvoiceNumCorrected { get; set; }

        [DisplayName("5а")]
        [Description("Дата корректировочного счета-фактуры продавца")]
        public DateTime? SellerInvoiceDateCorrected { get; set; }

        [DisplayName("6")]
        [Description("Номер исправления корректировочного счета-фактуры продавца")]
        public string SellerInvoiceNumChangedAndCorrected { get; set; }

        [DisplayName("6а")]
        [Description("Дата исправления корректировочного счета-фактуры продавца")]
        public DateTime? SellerInvoiceDateChangedAndCorrected { get; set; }

        [DisplayName("7")]
        [Description("Номер документа подтверждающего оплату")]
        public string PaymentDocNumber { get; set; }

        [DisplayName("7а")]
        [Description("Дата документа подтверждающего оплату")]
        public DateTime? PaymentDocDate { get; set; }

        [DisplayName("8")]
        [Description("Дата принятия на учёт товаров (работ, услуг), имущественных прав")]
        public DateTime? RegisterDate { get; set; }

        [DisplayName("9")]
        [Description("Наименование продавца")]
        public string SellerName { get; set; }

        [DisplayName("9а")]
        [Description("ИНН продавца")]
        public string SellerInn { get; set; }

        [DisplayName("9б")]
        [Description("КПП продавца")]
        public string SellerKpp { get; set; }

        [DisplayName("10")]
        [Description("Наименование  посредника (комиссионенера, агента, застройщика или технического заказчика, экспедитора)")]
        public string BrokerName { get; set; }

        [DisplayName("10а")]
        [Description("ИНН посредника (комиссионенера, агента, застройщика или технического заказчика, экспедитора)")]
        public string BrokerInn { get; set; }

        [DisplayName("10б")]
        [Description("КПП посредника (комиссионенера, агента, застройщика или технического заказчика, экспедитора)")]
        public string BrokerKpp { get; set; }

        [DisplayName("11")]
        [Description("Номер таможенной декларации")]
        public string CustomsDeclarationNumber { get; set; }

        [DisplayName("12")]
        [Description("Краткое наименование валюты")]
        public string Currency { get; set; }

        [DisplayName("13")]
        [Description("Стоимость покупок по счёту-фактуре, разница стоимости по корректировочному счету-фактуре (включая НДС), в валюте счета-фактуры")]
        [CurrencyFormat]
        public decimal TotalAmountByInvoice { get; set; }

        [DisplayName("14")]
        [Description("Сумма НДС по счёту-фактуре, разница суммы НДС по корректировочному счёту-фактуре принимаемая к вычету, в руб. и коп.")]
        [CurrencyFormat]
        public decimal NdsAmountRub { get; set; }


        public string InvoiceId { get; set; }

        public override string ToString()
        {
            return String.Format("Книга покупок: Ссылка-{0}; ИННПрод: {1}", InvoiceId, SellerInn);
        }
    }
}
