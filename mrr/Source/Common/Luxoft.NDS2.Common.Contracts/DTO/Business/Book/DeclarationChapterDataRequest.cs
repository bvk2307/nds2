﻿using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Book
{
    /// <summary>
    /// Этот класс описывает параметры запроса счетов-фактур раздела декларации
    /// </summary>
    [Serializable]
    public class DeclarationChapterDataRequest
    {
        /// <summary>
        /// Возвращает или задает идентификатор корректировки декларации
        /// </summary>
        public long DeclarationId
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает номер раздела
        /// </summary>
        public int Chapter
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает параметры фильтрации и сортировки данных
        /// </summary>
        public QueryConditions SearchContext
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает данные обработки запроса, сохраненные в клиентском кэше
        /// </summary>
        public List<DeclarationInvoiceChapterInfo> ClientCache
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает справочников итоговых сумм (ключ - название поля, значение - сумма)
        /// </summary>
        public Dictionary<string, decimal> ClientCacheSummaries
        {
            get;
            set;
        }
    }    
}
