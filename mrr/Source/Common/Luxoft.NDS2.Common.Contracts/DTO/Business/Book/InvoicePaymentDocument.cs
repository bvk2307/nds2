﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Book
{
    [Serializable]
    public class InvoicePaymentDocument : ICloneable
    {
        public string InvoiceKey { get; set; }

        public string Number { get; set; }

        public DateTime? Date { get; set; }

        public object Clone()
        {
            var obj = new InvoicePaymentDocument();

            obj.InvoiceKey = this.InvoiceKey;
            obj.Number = this.Number;
            obj.Date = this.Date;

            return obj;
        }
    }
}
