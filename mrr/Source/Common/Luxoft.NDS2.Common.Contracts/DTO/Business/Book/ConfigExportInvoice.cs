﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Book
{
    [Serializable]
    public class ConfigExportInvoice
    {
        public long InvoiceMaxCount { get; set; }
        public int PageSize { get; set; }
        public int DelayBetweenPage { get; set; }
    }
}
