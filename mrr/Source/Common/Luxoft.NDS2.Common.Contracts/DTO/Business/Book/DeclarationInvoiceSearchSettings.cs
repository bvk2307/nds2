﻿using System.Collections.Generic;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Book
{
    public class DeclarationInvoiceSearchSettings
    {
        public Dictionary<int, int> ChapterPartitionValues { get; private set; }

        public DeclarationInvoiceSearchSettings()
        {
            ChapterPartitionValues = new Dictionary<int, int>();
        }
    }
}
