﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Book
{
    [Serializable]
    public class InvoiceContractor : ICloneable
    {
        public string InvoiceKey { get; set; }

        public string Inn { get; set; }

        public string Kpp { get; set; }

        public object Clone()
        {
            var obj = new InvoiceContractor();

            obj.InvoiceKey = this.InvoiceKey;
            obj.Inn = this.Inn;
            obj.Kpp = this.Kpp;

            return obj;
        }
    }
}
