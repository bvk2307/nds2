﻿using System;
using System.ComponentModel;
using Luxoft.NDS2.Common.Contracts.CustomAttributes;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Book
{
    [Serializable]
    public class SellBookLineDemo : BusinessObject
    {
        [DisplayName("1")]
        [Description("Номер в книге продаж")]
        public long Id { get; set; }

        [DisplayName("2")]
        [Description("Код вида операции ")]
        public string OperationCode { get; set; }

        [DisplayName("3")]
        [Description("Номер счета-фактуры продавца")]
        public string SellerInvoiceNum { get; set; }

        [DisplayName("3а")]
        [Description("Дата счета-фактуры продавца")]
        public DateTime? SellerInvoiceDate { get; set; }

        [DisplayName("4")]
        [Description("Номер исправления счета-фактуры продавца")]
        public string SellerInvoiceNumChanged { get; set; }

        [DisplayName("4а")]
        [Description("Дата исправления счета-фактуры продавца")]
        public DateTime? SellerInvoiceDateChanged { get; set; }

        [DisplayName("5")]
        [Description("Номер корректировочного счета-фактуры продавца")]
        public string SellerInvoiceNumCorrected { get; set; }

        [DisplayName("5а")]
        [Description("Дата корректировочного счета-фактуры продавца")]
        public DateTime? SellerInvoiceDateCorrected { get; set; }

        [DisplayName("6")]
        [Description("Номер исправления корректировочного счета-фактуры продавца")]
        public string SellerInvoiceNumChangedAndCorrected { get; set; }

        [DisplayName("6а")]
        [Description("Дата исправления корректировочного счета-фактуры продавца")]
        public DateTime? SellerInvoiceDateChangedAndCorrected { get; set; }

        [DisplayName("7")]
        [Description("Номер документа подтверждающего оплату")]
        public string PaymentDocNumber { get; set; }

        [DisplayName("7а")]
        [Description("Дата документа подтверждающего оплату")]
        public DateTime? PaymentDocDate { get; set; }

        [DisplayName("8")]
        [Description("Наименование покупателя")]
        public string BuyerName { get; set; }

        [DisplayName("8а")]
        [Description("ИНН покупателя")]
        public string BuyerInn { get; set; }

        [DisplayName("8б")]
        [Description("КПП покупателя")]
        public string BuyerKpp { get; set; }

        [DisplayName("9")]
        [Description("Наименование  посредника (комиссионенера, агента, застройщика или технического заказчика, экспедитора)")]
        public string BrokerName { get; set; }

        [DisplayName("9а")]
        [Description("ИНН посредника (комиссионенера, агента, застройщика или технического заказчика, экспедитора)")]
        public string BrokerInn { get; set; }

        [DisplayName("9б")]
        [Description("КПП посредника (комиссионенера, агента, застройщика или технического заказчика, экспедитора)")]
        public string BrokerKpp { get; set; }

        [DisplayName("10")]
        [Description("Краткое наименование валюты")]
        public string Currency { get; set; }

        [DisplayName("11")]
        [Description("Стоимость продаж по счету-фактуре, разница стоимости по корректировочному счету-фактуре (включая НДС), в валюте счета-фактуры")]
        [CurrencyFormat]
        public decimal InvoiceTotalNdsInclude { get; set; }

        [DisplayName("11а")]
        [Description("Стоимость продаж по счету-фактуре, разница стоимости по корректировочному счету-фактуре (включая НДС), в валюте счета-фактуры")]
        [CurrencyFormat]
        public decimal InvoiceTotalRubNdsInclude { get; set; }

        [DisplayName("12")]
        [Description("Стоимость продаж, облагаемых налогом, по счёту-фактуре, разница стоимости по корректировочному счету-фактуре (без НДС), в руб. и коп., по ставке 18%")]
        [CurrencyFormat]
        public decimal AmountTaxed18Rub { get; set; }

        [DisplayName("12а")]
        [Description("Сумма НДС по счету-фактуре, разница суммы НДС по корректировочному счету-фактуре, в руб. и коп., по ставке 18%")]
        [CurrencyFormat]
        public decimal AmountTaxedNds18Rub { get; set; }

        [DisplayName("13")]
        [Description("Стоимость продаж, облагаемых налогом, по счёту-фактуре, разница стоимости по корректировочному счету-фактуре (без НДС), в руб. и коп., по ставке 10%")]
        [CurrencyFormat]
        public decimal AmountTaxed10Rub { get; set; }

        [DisplayName("13а")]
        [Description("Сумма НДС по счету-фактуре, разница суммы НДС по корректировочному счету-фактуре, в руб. и коп., по ставке 10% ")]
        [CurrencyFormat]
        public decimal AmountTaxedNds10Rub { get; set; }

        [DisplayName("14")]
        [Description("Стоимость продаж, облагаемых налогом, по счёту-фактуре, разница стоимости по корректировочному счету-фактуре (без НДС), в руб. и коп., по ставке 0%")]
        [CurrencyFormat]
        public decimal AmountTaxed0 { get; set; }

        [DisplayName("15")]
        [Description("Стоимость продаж, освобождаемых от налога, по счету-фактуре, разница стоимости по корректировочному счету-фактуре, в руб. и коп. ")]
        [CurrencyFormat]
        public decimal TaxFreeAmountRub { get; set; }

        public string InvoiceId { get; set; }

        public override string ToString()
        {
            return string.Format("Книга продаж: Ссылка:{0}; ИННПок:{1};", InvoiceId, BuyerInn);
        }
    }


}
