﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Book
{
    [Serializable]
    public class InvoiceExplainData
    {
        public string RowKey { get; set; }

        public int DocType { get; set; }

        public string ExplainNum { get; set; }

        public DateTime? ExplainDate { get; set; }

        public int Rank { get; set; }
    }
}
