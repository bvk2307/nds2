﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

// ReSharper disable InconsistentNaming
namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Contragents
{
    [Serializable]
    public class ContragentParamsSummary
    {
        public long DECLARATION_VERSION_ID { get; set; }

        public string INN { get; set; }

        [DisplayName("Тип")]
        [Description("Тип документа контрагента")]
        public string DOC_TYPE { get; set; }

        public int CHAPTER { get; set; }

        [DisplayName("Признак")]
        [Description("")]
        public string MARK { get; set; }

        [DisplayName("Статус")]
        [Description("Статус расхождения")]
        public int? DIS_STATUS { get; set; }

        [DisplayName("СУР")]
        [Description("Показатель СУР контрагента")]
        public int SUR_CODE { get; set; }

        [DisplayName("ИНН")]
        [Description("ИНН контрагента")]
        public string CONTRACTOR_INN { get; set; }

        [DisplayName("КПП")]
        [Description("КПП контрагента")]
        public string CONTRACTOR_KPP { get; set; }

        [DisplayName("Наименование")]
        [Description("Наименование контрагента")]
        public string CONTRACTOR_NAME { get; set; }

        [DisplayName("Номер")]
        [Description("Номер СФ НП")]
        public string INVOICE_NUM { get; set; }

        [DisplayName("Дата")]
        [Description("Дата СФ НП")]
        public DateTime? INVOICE_DATE { get; set; }

        [DisplayName("Стоимость, включая НДС, руб.")]
        [Description("Стоимость по счету-фактуре, включая НДС")]
        public decimal? AMOUNT_TOTAL { get; set; }

        [DisplayName("Сумма НДС, руб.")]
        [Description("Сумма НДС, по счету-фактуре")]
        public decimal? AMOUNT_NDS { get; set; }

        [DisplayName("Вид")]
        [Description("Вид расхождения")]
        public string DIS_TYPE_NAME { get; set; }

        [DisplayName("Сумма, руб.")]
        [Description("Сумма расхождения")]
        public decimal? DIS_AMOUNT { get; set; }

        [DisplayName("Статус")]
        [Description("Статус этапа отработки")]
        public string DIS_STATUS_NAME { get; set; }

        public string INN_DECLARANT { get; set; }
        public string INN_REORGANIZED { get; set; }
        public string KPP_EFFECTIVE { get; set; }
        public string FISCAL_YEAR { get; set; }
        public string PERIOD_CODE { get; set; }

        public int DOC_TYPE_CODE { get; set; }
        public long? DIS_ID { get; set; }
        public int? DIS_TYPE { get; set; }
    }
}
// ReSharper restore InconsistentNaming
