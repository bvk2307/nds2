﻿using System;
using System.ComponentModel;

// ReSharper disable InconsistentNaming
namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Contragents
{
    [Serializable]
    public class ContragentSummary
    {
        /// <summary>
        /// идентификатор связи с декларацией
        /// </summary>
        public long DECLARATION_VERSION_ID { get; set; }

        public string INN_DECLARANT { get; set; }
        public string INN_REORGANIZED{ get; set; }
        public string KPP_EFFECTIVE{ get; set; }
        public string PERIOD_CODE { get; set; }
        public string FISCAL_YEAR { get; set; }

        [DisplayName("Признак")]
        [Description("Признак")]
        public string MARK { get; set; }

        [DisplayName("Тип")]
        [Description("Тип документа контрагента")]
        public string DOC_TYPE { get; set; }

        [DisplayName("Код типа")]
        [Description("Код типа документа контрагента")]
        public string DOC_TYPE_CODE { get; set; }

        [DisplayName("СУР")]
        [Description("Показатель СУР контаргента")]
        public int? SUR_CODE { get; set; }

        [DisplayName("ИНН")]
        [Description("ИНН контрагента, с которым произошло сопоставление")]
        public string CONTRACTOR_INN { get; set; }

        [DisplayName("КПП")]
        [Description("КПП контрагента")]
        public string CONTRACTOR_KPP { get; set; }

        [DisplayName("Наименование")]
        [Description("Наименование контрагента")]
        public string CONTRACTOR_NAME { get; set; }

        /// <summary>
        /// Признак имортной сделки
        /// </summary>
        public bool? IS_IMPORT { get; set; }

        [DisplayName("Стоимость покупок, включая НДС, руб.")]
        [Description("Стоимость покупок у контрагента, включая НДС, руб.")]
        public decimal? CHAPTER8_AMOUNT { get; set; }

        [DisplayName("Стоимость продаж, включая НДС, руб.")]
        [Description("Стоимость продаж  контрагенту, включая НДС")]
        public decimal? CHAPTER9_AMOUNT { get; set; }

        [DisplayName("Стоимость по выставленным СФ, руб.")]
        [Description("Стоимость товаров (работ, услуг), имущественных прав по выставленным СФ, руб.")]
        public decimal? CHAPTER10_AMOUNT { get; set; }

        [DisplayName("Стоимость по полученным СФ, руб.")]
        [Description("Стоимость товаров (работ, услуг), имущественных прав по полученным СФ, руб.")]
        public decimal? CHAPTER11_AMOUNT { get; set; }

        [DisplayName("Стоимость продаж, включая НДС, руб.")]
        [Description("Стоимость продаж  контрагенту, включая НДС")]
        public decimal? CHAPTER12_AMOUNT { get; set; }

        [DisplayName("Заявленный к вычету НДС, руб.")]
        [Description("Сумма НДС, заявленная налогоплательщиком к вычету по операциям с контрагентом")]
        public decimal? DECLARED_TAX_AMNT { get; set; }

        [DisplayName("Исчисленная сумма НДС, руб.")]
        [Description("Сумма НДС, исчисленная налогоплательщиком по операциям с контрагентом")]
        public decimal? CALCULATED_TAX_AMNT_R9 { get; set; }

        [DisplayName("Исчисленная сумма НДС, руб.")]
        [Description("Сумма НДС, исчисленная налогоплательщиком по операциям с контрагентом")]
        public decimal? CALCULATED_TAX_AMNT_R12 { get; set; }

        [DisplayName("Сумма НДС, руб.")]
        [Description("Сумма НДС, руб.")]
        public decimal? CHAPTER10_AMOUNT_NDS { get; set; }

        [DisplayName("Сумма НДС, руб.")]
        [Description("Сумма НДС, руб.")]
        public decimal? CHAPTER11_AMOUNT_NDS { get; set; }

        /// <summary>
        /// Заявленная к вычету НДС по всему разделу
        /// </summary>
        public decimal? DECLARED_TAX_AMNT_TOTAL { get; set; }

        //количество операций эффективное - для фильтров
        public int CHAPTER9_COUNT_EFFECTIVE { get; set; }

        [DisplayName("Кол-во операций")]
        [Description("Количество операций с контрагентом-продавцом")]
        public int CHAPTER8_COUNT { get; set; }
        [DisplayName("Кол-во операций")]
        [Description("Количество операций с контрагентом-продавцом")]
        public int CHAPTER9_COUNT { get; set; }
        [DisplayName("Кол-во операций")]
        [Description("Количество операций с контрагентом-продавцом")]
        public int CHAPTER10_COUNT { get; set; }
        [DisplayName("Кол-во операций")]
        [Description("Количество операций с контрагентом-продавцом")]
        public int CHAPTER11_COUNT { get; set; }
        [DisplayName("Кол-во операций")]
        [Description("Количество операций с контрагентом-продавцом")]
        public int CHAPTER12_COUNT { get; set; }


        [DisplayName("Кол-во разрывов")]
        [Description("Количество расхождений вида «Разрыв»")]
        public decimal? GAP_QUANTITY_8 { get; set; }
        [DisplayName("Кол-во разрывов")]
        [Description("Количество расхождений вида «Разрыв»")]
        public decimal? GAP_QUANTITY_9 { get; set; }
        [DisplayName("Кол-во разрывов")]
        [Description("Количество расхождений вида «Разрыв»")]
        public decimal? GAP_QUANTITY_10 { get; set; }
        [DisplayName("Кол-во разрывов")]
        [Description("Количество расхождений вида «Разрыв»")]
        public decimal? GAP_QUANTITY_11 { get; set; }
        [DisplayName("Кол-во разрывов")]
        [Description("Количество расхождений вида «Разрыв»")]
        public decimal? GAP_QUANTITY_12 { get; set; }


        [DisplayName("Сумма разрывов, руб.")]
        [Description("Сумма расхождений с видом «Разрыв»")]
        public decimal? GAP_AMOUNT_8 { get; set; }
        [DisplayName("Сумма разрывов, руб.")]
        [Description("Сумма расхождений с видом «Разрыв»")]
        public decimal? GAP_AMOUNT_9 { get; set; }
        [DisplayName("Сумма разрывов, руб.")]
        [Description("Сумма расхождений с видом «Разрыв»")]
        public decimal? GAP_AMOUNT_10 { get; set; }
        [DisplayName("Сумма разрывов, руб.")]
        [Description("Сумма расхождений с видом «Разрыв»")]
        public decimal? GAP_AMOUNT_11 { get; set; }
        [DisplayName("Сумма разрывов, руб.")]
        [Description("Сумма расхождений с видом «Разрыв»")]
        public decimal? GAP_AMOUNT_12 { get; set; }


        [DisplayName("Кол-во расхождений")]
        [Description("Количество расхождений отличных от расхождений вида «Разрыв»")]
        public decimal? OTHER_QUANTITY_8 { get; set; }
        [DisplayName("Кол-во расхождений")]
        [Description("Количество расхождений отличных от расхождений вида «Разрыв»")]
        public decimal? OTHER_QUANTITY_9 { get; set; }
        [DisplayName("Кол-во расхождений")]
        [Description("Количество расхождений отличных от расхождений вида «Разрыв»")]
        public decimal? OTHER_QUANTITY_10 { get; set; }
        [DisplayName("Кол-во расхождений")]
        [Description("Количество расхождений отличных от расхождений вида «Разрыв»")]
        public decimal? OTHER_QUANTITY_11 { get; set; }
        [DisplayName("Кол-во расхождений")]
        [Description("Количество расхождений отличных от расхождений вида «Разрыв»")]
        public decimal? OTHER_QUANTITY_12 { get; set; }


        [DisplayName("Сумма расхождений, руб.")]
        [Description("Сумма расхождений отличных от расхождений вида «Разрыв»")]
        public decimal? OTHER_AMOUNT_8 { get; set; }
        [DisplayName("Сумма расхождений, руб.")]
        [Description("Сумма расхождений отличных от расхождений вида «Разрыв»")]
        public decimal? OTHER_AMOUNT_9 { get; set; }
        [DisplayName("Сумма расхождений, руб.")]
        [Description("Сумма расхождений отличных от расхождений вида «Разрыв»")]
        public decimal? OTHER_AMOUNT_10 { get; set; }
        [DisplayName("Сумма расхождений, руб.")]
        [Description("Сумма расхождений отличных от расхождений вида «Разрыв»")]
        public decimal? OTHER_AMOUNT_11 { get; set; }
        [DisplayName("Сумма расхождений, руб.")]
        [Description("Сумма расхождений отличных от расхождений вида «Разрыв»")]
        public decimal? OTHER_AMOUNT_12 { get; set; }
    }
}
// ReSharper restore InconsistentNaming
