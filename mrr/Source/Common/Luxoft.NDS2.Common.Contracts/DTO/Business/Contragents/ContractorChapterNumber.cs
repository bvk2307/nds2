﻿
namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Contragents
{
    public enum ContractorChapterNumber
    {
        Chapter8 = 8,
        Chapter9 = 9,
        Chapter10 = 10,
        Chapter11 = 11,
        Chapter12 =12
    }
}
