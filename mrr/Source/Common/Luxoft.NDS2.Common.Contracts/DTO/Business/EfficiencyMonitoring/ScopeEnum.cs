﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring
{
    [Serializable]
    public enum ScopeEnum
    {
        RF,
        RF_UFNS,
        MIKN,
        RF_DISTRICTS,
        DISTRICT,
        DISTRICT_UFNS,
        UFNS,
        IFNS,
        UNDIFINED
    }
}
