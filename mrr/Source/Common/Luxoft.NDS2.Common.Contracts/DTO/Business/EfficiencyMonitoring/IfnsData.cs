﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring
{
    [Serializable]
    public class IfnsData
    {
        public string REGION_CODE { get; set; }  

        public string REGION_NAME { get; set; }

        public string SONO_NAME { get; set; }  

        public DateTime? CALCULATION_DATE { get; set; }  

        public int FISCAL_YEAR { get; set; }  
        
        public int QTR { get; set; }  

        public string SONO_CODE { get; set; }  

        public decimal DEDUCTION_AMNT { get; set; }  

        public decimal CALCULATION_AMNT { get; set; }  

        public decimal EFF_IDX_1 { get; set; }  

        public decimal EFF_IDX_2 { get; set; }  

        public decimal EFF_IDX_3 { get; set; }  

        public decimal EFF_IDX_4 { get; set; }  

        public decimal EFF_IDX_AVG { get; set; }  

        public int RNK_REGION_EFF_IDX_1 { get; set; }  

        public int RNK_REGION_EFF_IDX_2 { get; set; }  

        public int RNK_REGION_EFF_IDX_3 { get; set; }  

        public int RNK_REGION_EFF_IDX_4 { get; set; }  

        public int RNK_REGION_EFF_IDX_AVG { get; set; }  

        public int RNK_FEDERAL_EFF_IDX_1 { get; set; }  

        public int RNK_FEDERAL_EFF_IDX_2 { get; set; }  

        public int RNK_FEDERAL_EFF_IDX_3 { get; set; }  

        public int RNK_FEDERAL_EFF_IDX_4 { get; set; }  

        public int RNK_FEDERAL_EFF_IDX_AVG { get; set; }  

        public decimal BUYER_DIS_OPN_AMNT { get; set; } 
        public decimal BUYER_DIS_CLS_AMNT { get; set; } 

        public decimal BUYER_DIS_OPN_KNP_AMNT { get; set; }  
        public decimal BUYER_DIS_CLS_KNP_AMNT { get; set; }

        public decimal BUYER_DIS_OPN_GAP_AMNT { get; set; }  
        public decimal BUYER_DIS_CLS_GAP_AMNT { get; set; }

        public decimal BUYER_DIS_OPN_GAP_KNP_AMNT { get; set; }
        public decimal BUYER_DIS_CLS_GAP_KNP_AMNT { get; set; }  

        public decimal BUYER_DIS_OPN_NDS_AMNT { get; set; }
        public decimal BUYER_DIS_CLS_NDS_AMNT { get; set; } 

        public decimal BUYER_DIS_OPN_NDS_KNP_AMNT { get; set; }
        public decimal BUYER_DIS_CLS_NDS_KNP_AMNT { get; set; }

        public decimal BUYER_DIS_ALL_KNP_AMNT { get; set; }  

        public decimal SELLER_DIS_OPN_AMNT { get; set; }
        public decimal SELLER_DIS_CLS_AMNT { get; set; }

        public decimal SELLER_DIS_OPN_KNP_AMNT { get; set; }
        public decimal SELLER_DIS_CLS_KNP_AMNT { get; set; }

        public decimal SELLER_DIS_OPN_GAP_AMNT { get; set; }
        public decimal SELLER_DIS_CLS_GAP_AMNT { get; set; }

        public decimal SELLER_DIS_OPN_GAP_KNP_AMNT { get; set; }
        public decimal SELLER_DIS_CLS_GAP_KNP_AMNT { get; set; }

        public decimal SELLER_DIS_OPN_NDS_AMNT { get; set; }
        public decimal SELLER_DIS_CLS_NDS_AMNT { get; set; }

        public decimal SELLER_DIS_OPN_NDS_KNP_AMNT { get; set; }
        public decimal SELLER_DIS_CLS_NDS_KNP_AMNT { get; set; }

        public decimal SELLER_DIS_ALL_KNP_AMNT { get; set; }  

        // Пока не реализованные поля
        public decimal SENT_IDX { get; set; }
        public decimal SENT_IDX_NDS { get; set; }
        public decimal SENT_IDX_GAP { get; set; }

        public decimal CLOSED_IDX { get; set; }
        public decimal CLOSED_IDX_GAP { get; set; }
        public decimal CLOSED_IDX_NDS { get; set; }
    }
}
