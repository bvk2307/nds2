﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring
{
    [Serializable]
    [DataContract]
    public enum IndexEnum
    {
        /// <summary>
        /// Доля закрытых расхождений в расхождениях  всего по сумме (2.1)
        /// </summary>
        ClosedDiscrepancyShareByAmount = 1,

        /// <summary>
        /// Доля закрытых расхождений в расхождениях  всего по количеству (2.2)
        /// </summary>
        ClosedDiscrepancyShareByQuantity = 2,

        /// <summary>
        /// Доля деклараций с расхождениями в декларациях с операциями (2.3)
        /// </summary>
        DeclarationWithDiscrepancyShareByQuantity = 3,

        /// <summary>
        /// Доля расхождений в сумме вычетов по НДС (2.4)
        /// </summary>
        DiscrepancyShareInDeduction = 0
    }
}
