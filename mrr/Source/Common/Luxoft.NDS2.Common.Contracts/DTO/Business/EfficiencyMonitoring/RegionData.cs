﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring
{
    [Serializable]
    public class RegionData
    {
        public int DISTRICT_CODE { get; set; }

        public string UFNS_CODE { get; set; }

        public string UFNS_NAME { get; set; }

        public DateTime? CALCULATION_DATE { get; set; }

        public int FISCAL_YEAR { get; set; }

        public int QTR { get; set; }

        public decimal DEDUCTION_AMNT { get; set; }

        public decimal CALCULATION_AMNT { get; set; }

        public decimal EFF_IDX_1 { get; set; }

        public decimal EFF_IDX_2 { get; set; }

        public decimal EFF_IDX_3 { get; set; }

        public decimal EFF_IDX_4 { get; set; }

        public decimal EFF_IDX_AVG { get; set; }

        public int RNK_FEDERAL_EFF_IDX_1 { get; set; }

        public int RNK_FEDERAL_EFF_IDX_2 { get; set; }

        public int RNK_FEDERAL_EFF_IDX_3 { get; set; }

        public int RNK_FEDERAL_EFF_IDX_4 { get; set; }

        public int RNK_FEDERAL_EFF_IDX_AVG { get; set; }

        #region Сводные данные по расхождениям

        #region Все

        public decimal DIS_TOTAL_AMNT { get; set; }

        public int DIS_TOTAL_CNT { get; set; }

        public decimal DIS_GAP_TOTAL_AMNT { get; set; }

        public int DIS_GAP_TOTAL_CNT { get; set; }

        public decimal DIS_NDS_TOTAL_AMNT { get; set; }

        public int DIS_NDS_TOTAL_CNT { get; set; }

        #endregion Все

        #region Закрытые

        public decimal DIS_CLS_TOTAL_AMNT { get; set; }

        public int DIS_CLS_TOTAL_CNT { get; set; }

        public decimal DIS_CLS_GAP_TOTAL_AMNT { get; set; }

        public int DIS_CLS_GAP_TOTAL_CNT { get; set; }

        public decimal DIS_CLS_NDS_TOTAL_AMNT { get; set; }

        public int DIS_CLS_NDS_TOTAL_CNT { get; set; }

        #endregion Закрытые

        #region Открытые

        public decimal DIS_OPN_TOTAL_AMNT { get; set; }

        public int DIS_OPN_TOTAL_CNT { get; set; }

        public decimal DIS_OPN_GAP_TOTAL_AMNT { get; set; }

        public int DIS_OPN_GAP_TOTAL_CNT { get; set; }

        public decimal DIS_OPN_NDS_TOTAL_AMNT { get; set; }

        public int DIS_OPN_NDS_TOTAL_CNT { get; set; }

        #endregion Открытые

        #endregion Сводные данные по расхождениям

        #region Покупатель

        public decimal BUYER_DIS_OPN_AMNT { get; set; }

        public decimal BUYER_DIS_CLS_AMNT { get; set; }

        public decimal BUYER_DIS_OPN_KNP_AMNT { get; set; }

        public decimal BUYER_DIS_CLS_KNP_AMNT { get; set; }

        public decimal BUYER_DIS_OPN_GAP_AMNT { get; set; }

        public decimal BUYER_DIS_CLS_GAP_AMNT { get; set; }

        public decimal BUYER_DIS_OPN_GAP_KNP_AMNT { get; set; }

        public decimal BUYER_DIS_CLS_GAP_KNP_AMNT { get; set; }

        public decimal BUYER_DIS_OPN_NDS_AMNT { get; set; }

        public decimal BUYER_DIS_CLS_NDS_AMNT { get; set; }

        public decimal BUYER_DIS_OPN_NDS_KNP_AMNT { get; set; }

        public decimal BUYER_DIS_CLS_NDS_KNP_AMNT { get; set; }

        public decimal BUYER_DIS_ALL_KNP_AMNT { get; set; }

        public int BUYER_DIS_OPN_KNP_CNT { get; set; }

        public int BUYER_DIS_OPN_GAP_KNP_CNT { get; set; }

        public int BUYER_DIS_OPN_NDS_KNP_CNT { get; set; }

        public int BUYER_DIS_CLS_KNP_CNT { get; set; }

        public int BUYER_DIS_CLS_GAP_KNP_CNT { get; set; }

        public int BUYER_DIS_CLS_NDS_CNT { get; set; }

        #endregion

        #region Продавец

        public decimal SELLER_DIS_OPN_AMNT { get; set; }
        public decimal SELLER_DIS_CLS_AMNT { get; set; }

        public decimal SELLER_DIS_OPN_KNP_AMNT { get; set; }
        public decimal SELLER_DIS_CLS_KNP_AMNT { get; set; }

        public decimal SELLER_DIS_OPN_GAP_AMNT { get; set; }
        public decimal SELLER_DIS_CLS_GAP_AMNT { get; set; }

        public decimal SELLER_DIS_OPN_GAP_KNP_AMNT { get; set; }
        public decimal SELLER_DIS_CLS_GAP_KNP_AMNT { get; set; }

        public decimal SELLER_DIS_OPN_NDS_AMNT { get; set; }
        public decimal SELLER_DIS_CLS_NDS_AMNT { get; set; }

        public decimal SELLER_DIS_OPN_NDS_KNP_AMNT { get; set; }
        public decimal SELLER_DIS_CLS_NDS_KNP_AMNT { get; set; }

        public decimal SELLER_DIS_ALL_KNP_AMNT { get; set; }


        public int SELLER_DIS_OPN_KNP_CNT { get; set; }

        public int SELLER_DIS_OPN_GAP_KNP_CNT { get; set; }

        public int SELLER_DIS_OPN_NDS_KNP_CNT { get; set; }

        public int SELLER_DIS_CLS_KNP_CNT { get; set; }

        public int SELLER_DIS_CLS_GAP_KNP_CNT { get; set; }

        public int SELLER_DIS_CLS_NDS_KNP_CNT { get; set; }

        #endregion Продавец

        #region  Данные декларации

        public int DECL_ALL_CNT { get; set; }

        public decimal DECL_ALL_NDS_SUM{ get; set; }

        public int DECL_OPERATION_CNT{ get; set; }

        public int DECL_DISCREPANCY_CNT{ get; set; }

        public int DECL_PAY_CNT{ get; set; }

        public decimal DECL_PAY_NDS_SUM{ get; set; }

        public int DECL_COMPENSATE_CNT{ get; set; }

        public decimal DECL_COMPENSATE_NDS_SUM{ get; set; }

        public int DECL_ZERO_CNT { get; set; }

        #endregion  Данные декларации

        public decimal SENT_IDX { get; set; }

        public decimal SENT_IDX_NDS { get; set; }

        public decimal SENT_IDX_GAP { get; set; }

        public decimal CLOSED_IDX { get; set; }

        public decimal CLOSED_IDX_GAP { get; set; }

        public decimal CLOSED_IDX_NDS { get; set; }
    }
}
