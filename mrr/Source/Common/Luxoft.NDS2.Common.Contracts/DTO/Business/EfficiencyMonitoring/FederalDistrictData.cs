﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring
{
    [Serializable]
    public class FederalDistrictData
    {
        [DisplayName("Год")]
        public int FISCAL_YEAR { get; set; }

        [DisplayName("Квартал")]
        public int QTR { get; set; }

        [DisplayName("Дата")]
        public DateTime? CALCULATION_DATE { get; set; }

        public String DISTRICT_CODE { get; set; }

        #region Данные декларации
        public decimal DEDUCTION_AMNT { get; set; }

        public decimal CALCULATION_AMNT { get; set; }
        /// <summary>
        /// Деклараций всего
        /// </summary>
        public int DECL_ALL_CNT { get; set; }
        /// <summary>
        /// НДС всех НД
        /// </summary>
        public decimal DECL_ALL_NDS_SUM { get; set; }
        /// <summary>
        /// Кол-во НД с операциями
        /// </summary>
        public int DECL_OPERATION_CNT { get; set; }
        /// <summary>
        /// Кол-во НД с расхождениями
        /// </summary>
        public int DECL_DISCREPANCY_CNT { get; set; }
        /// <summary>
        /// Кол-во НД к уплате
        /// </summary>
        public int DECL_PAY_CNT { get; set; }
        /// <summary>
        /// НДС НД к уплате
        /// </summary>
        public decimal DECL_PAY_NDS_SUM { get; set; }
        /// <summary>
        /// Кол-во НД к возмещению
        /// </summary>
        public int DECL_COMPENSATE_CNT { get; set; }
        /// <summary>
        /// Сумма НДС деклараций к возмещению
        /// </summary>
        public decimal DECL_COMPENSATE_NDS_SUM { get; set; }
        /// <summary>
        /// Кол-во нулевок
        /// </summary>
        public int DECL_ZERO_CNT { get; set; }


        #endregion Данные декларации

        #region Сводные данные по расхождениям

        #region Все

        public decimal DIS_TOTAL_AMNT { get; set; }

        public int DIS_TOTAL_CNT { get; set; }

        public decimal DIS_GAP_TOTAL_AMNT { get; set; }

        public int DIS_GAP_TOTAL_CNT { get; set; }

        public decimal DIS_NDS_TOTAL_AMNT { get; set; }

        public int DIS_NDS_TOTAL_CNT { get; set; }

        #endregion Все

        #region Закрытые

        public decimal DIS_CLS_TOTAL_AMNT { get; set; }

        public int DIS_CLS_TOTAL_CNT { get; set; }

        public decimal DIS_CLS_GAP_TOTAL_AMNT { get; set; }

        public int DIS_CLS_GAP_TOTAL_CNT { get; set; }

        public decimal DIS_CLS_NDS_TOTAL_AMNT { get; set; }

        public int DIS_CLS_NDS_TOTAL_CNT { get; set; }

        #endregion Закрытые

        #region Открытые

        public decimal DIS_OPN_TOTAL_AMNT { get; set; }

        public int DIS_OPN_TOTAL_CNT { get; set; }

        public decimal DIS_OPN_GAP_TOTAL_AMNT { get; set; }

        public int DIS_OPN_GAP_TOTAL_CNT { get; set; }

        public decimal DIS_OPN_NDS_TOTAL_AMNT { get; set; }

        public int DIS_OPN_NDS_TOTAL_CNT { get; set; }

        #endregion Открытые

        #endregion Сводные данные по расхождениям


    }
}
