﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring
{
    [DataContract]
    [Serializable]
    public class TnoRatingInfo
    {
        [DataMember(Name = "tnoCode")]
        public string TnoCode { get; set; }
        [DataMember(Name = "fiscalYear")]
        public int FiscalYear { get; set; }
        [DataMember(Name = "quarter")]
        public int Quarter { get; set; }
        [DataMember(Name = "rating_2_1_Value")]
        public decimal Rating_2_1_Value { get; set; }
        [DataMember(Name = "rating_2_2_Value")]
        public decimal Rating_2_2_Value { get; set; }
        [DataMember(Name = "rating_2_3_Value")]
        public decimal Rating_2_3_Value { get; set; }
        [DataMember(Name = "rating_2_4_Value")]
        public decimal Rating_2_4_Value { get; set; }

        public TnoRatingInfo Clone()
        {
            return new TnoRatingInfo()
            {
                Quarter = this.Quarter,
                FiscalYear = this.FiscalYear,
                Rating_2_1_Value = this.Rating_2_1_Value,
                Rating_2_2_Value = this.Rating_2_2_Value,
                Rating_2_3_Value = this.Rating_2_3_Value,
                Rating_2_4_Value = this.Rating_2_4_Value,
            };
        }

    }


    public class TnoRatingInfoEqualityComparerByQuarter : IEqualityComparer<TnoRatingInfo>
    {
        public bool Equals(TnoRatingInfo x, TnoRatingInfo y)
        {
            return x.Quarter.Equals(y.Quarter) && x.FiscalYear.Equals(y.FiscalYear);
        }

        public int GetHashCode(TnoRatingInfo obj)
        {
            return obj.Quarter + obj.FiscalYear*10;
        }
    }
}
