﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Luxoft.NDS2.Common.Contracts.Annotations;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring
{
    [DataContract]
    [Serializable]
    public class DiscrepancyDetail : INotifyPropertyChanged
    {

        public long IdDekl { get; set; }

        public string Inn { get; set; }

        public string Period { get; set; }

        public string Quarter { get; set; }

        public string FiscalYear { get; set; }

        public string SonoCode { get; set; }

        public string SonoName { get; set; }

        public string RegionCode { get; set; }

        public string RegionName { get; set; }

        public DateTime CalculationDate { get; set; }

        /// <summary>
        /// Необработано расхождений кол-во
        /// </summary>
        public int OpenKnpDiscrepancyCount { get; set; }

        /// <summary>
        /// Необработано расхождений сумма
        /// </summary>
        public decimal OpenKnpDiscrepancyAmount { get; set; }

        /// <summary>
        /// Необработано расхождений разрыв кол-во
        /// </summary>
        public int OpenKnpGapDiscrepancyCount { get; set; }

        /// <summary>
        /// Необработано расхождений разрыв сумма
        /// </summary>
        public decimal OpenKnpGapDiscrepancyAmount { get; set; }

        /// <summary>
        /// Необработано расхождений ндс кол-во
        /// </summary>
        public int OpenKnpNdsDiscrepancyCount { get; set; }

        /// <summary>
        /// Необработано расхождений ндс сумма
        /// </summary>
        public decimal OpenKnpNdsDiscrepancyAmount { get; set; }

        public decimal NdsSum { get; set; }

        public string TaxPayerName { get; set; }

        public string AssignedInspectorName { get; set; }

        public bool IsActual { get; set; }

        public string SurCode { get; set; }


        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
