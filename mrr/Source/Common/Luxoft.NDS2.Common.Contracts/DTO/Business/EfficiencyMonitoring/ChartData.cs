﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring
{
    [Serializable]
    public class ChartData
    {
        public int FiscalYear { get; set; }

        public int Quarter { get; set; }

        public DateTime CalculateDate { get; set; }

        public ScopeEnum ParentScope { get; set; }
        public string ParentScopeCode { get; set; }

        public string ScopeCode { get; set; }
        public ScopeEnum Scope { get; set; }
    }
}
