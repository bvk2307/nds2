﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring
{
    [Serializable]
    public class RaitingRequestData
    {
        public string District { get; set; }
        public int FiscalYear { get; set; }
        public int Quarter { get; set; }
        public DateTime Day { get; set; }
        public bool IsTopBest { get; set; }
        public string RankName { get; set; }
        public List<string> IDs { get; set; }
    }    
}
