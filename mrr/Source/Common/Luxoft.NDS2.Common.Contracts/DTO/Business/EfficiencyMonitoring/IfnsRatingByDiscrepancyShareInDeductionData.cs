﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring
{

    [Serializable]
    public class IfnsRatingByDiscrepancyShareInDeductionData
    {
        /// <summary>
        /// Рейтинг
        /// </summary>
        public int Rating { get; set; }
        /// <summary>
        /// Рейтинг предыдущего расчета по этому же кварталу
        /// </summary>
        public int? RatingPrev { get; set; }
        /// <summary>
        /// Код инспекции
        /// </summary>
        public string SonoCode { get; set; }
        /// <summary>
        /// Наименование инспекции
        /// </summary>
        public string SonoName { get; set; }
        /// <summary>
        /// Код региона
        /// </summary>
        public string RegionCode { get; set; }
        /// <summary>
        /// Наименование региона
        /// </summary>
        public string RegionName { get; set; }
        /// <summary>
        /// Сумма открытых расхождений  КНП
        /// </summary>
        public decimal DiscrepancyAmount { get; set; }
        /// <summary>
        /// Сумма вычетов
        /// </summary>
        public decimal DeductionAmount { get; set; }
        /// <summary>
        /// Доля суммы расхождений КНП в сумме вычетов
        /// </summary>
        public decimal ShareValue { get; set; }

    }
}
