﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring
{
    /// <summary>
    /// Данные о расчете данных Мониторинга эффективности
    /// </summary>
    [Serializable]
    public class ReportCalculationInfo
    {
        /// <summary>
        /// Дата расчета 
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Признак выполнения расчета на основании данных агрегата мониторинга эффективности
        /// </summary>
        public int IsBasedOnEfficiencyAgregate { get; set; }
    }
}
