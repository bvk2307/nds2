﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring
{
    [Serializable]
    public class CalculationData
    {
        public string SelectedTnoCode { get; set; }

        public ScopeEnum DetailLevel { get; set; }

        public QuarterInfo Quarter { get; set; }

        public DateTime CalculationDate { get; set; }
    }
}
