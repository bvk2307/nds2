﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring
{
    [DataContract]
    [Serializable]
    public class TnoData
    {

		[DataMember]
		public String RATING_TYPE { get; set; }

        [DataMember]
        public String TNO_CODE { get; set; }

        [DataMember]
        public String TNO_NAME { get; set; }

        #region Показатели и рейтинг

        [DataMember]
        public int RNK_EFF_IDX_2_1 { get; set; }
        [DataMember]
        public int RNK_EFF_IDX_2_2 { get; set; }
        [DataMember]
        public int RNK_EFF_IDX_2_3 { get; set; }
        [DataMember]
        public int RNK_EFF_IDX_2_4 { get; set; }
        

        [DataMember]
        public int RNK_FEDERAL_EFF_IDX_2_1 { get; set; }
        [DataMember]
        public int RNK_FEDERAL_EFF_IDX_2_2 { get; set; }
        [DataMember]
        public int RNK_FEDERAL_EFF_IDX_2_3 { get; set; }
        [DataMember]
        public int RNK_FEDERAL_EFF_IDX_2_4 { get; set; }


        [DataMember]
        public decimal EFF_IDX_2_1 { get; set; }
        [DataMember]
        public decimal EFF_IDX_2_2 { get; set; }
        [DataMember]
        public decimal EFF_IDX_2_3 { get; set; }
        [DataMember]
        public decimal EFF_IDX_2_4 { get; set; }


        [DataMember]
        public int? RNK_EFF_IDX_2_1_PREV { get; set; }
        [DataMember]
        public int? RNK_EFF_IDX_2_2_PREV { get; set; }
        [DataMember]
        public int? RNK_EFF_IDX_2_3_PREV { get; set; }
        [DataMember]
        public int? RNK_EFF_IDX_2_4_PREV { get; set; }

        [DataMember]
        public int? RNK_FEDERAL_EFF_IDX_2_1_PREV { get; set; }
        [DataMember]
        public int? RNK_FEDERAL_EFF_IDX_2_2_PREV { get; set; }
        [DataMember]
        public int? RNK_FEDERAL_EFF_IDX_2_3_PREV { get; set; }
        [DataMember]
        public int? RNK_FEDERAL_EFF_IDX_2_4_PREV { get; set; }


        [DataMember]
        public decimal EFF_IDX_2_1_PREV { get; set; }
        [DataMember]
        public decimal EFF_IDX_2_2_PREV { get; set; }
        [DataMember]
        public decimal EFF_IDX_2_3_PREV { get; set; }
        [DataMember]
        public decimal EFF_IDX_2_4_PREV { get; set; }
        

        #endregion

        #region Данные декларации
        [DataMember]
        public decimal DEDUCTION_AMNT { get; set; }

        [DataMember]
        public decimal CALCULATION_AMNT { get; set; }
        /// <summary>
        /// Деклараций всего
        /// </summary>
        [DataMember]
        public int DECL_ALL_CNT { get; set; }
        /// <summary>
        /// НДС всех НД
        /// </summary>
        [DataMember]
        public decimal DECL_ALL_NDS_SUM { get; set; }
        /// <summary>
        /// Кол-во НД с операциями
        /// </summary>
        [DataMember]
        public int DECL_OPERATION_CNT { get; set; }
        /// <summary>
        /// Кол-во НД с расхождениями
        /// </summary>
        [DataMember]
        public int DECL_DISCREPANCY_CNT { get; set; }
        /// <summary>
        /// Кол-во НД к уплате
        /// </summary>
        [DataMember]
        public int DECL_PAY_CNT { get; set; }
        /// <summary>
        /// НДС НД к уплате
        /// </summary>
        [DataMember]
        public decimal DECL_PAY_NDS_SUM { get; set; }
        /// <summary>
        /// Кол-во НД к возмещению
        /// </summary>
        [DataMember]
        public int DECL_COMPENSATE_CNT { get; set; }
        /// <summary>
        /// Сумма НДС деклараций к возмещению
        /// </summary>
        [DataMember]
        public decimal DECL_COMPENSATE_NDS_SUM { get; set; }
        /// <summary>
        /// Кол-во нулевок
        /// </summary>
        [DataMember]
        public int DECL_ZERO_CNT { get; set; }


        #endregion Данные декларации

        #region Сводные данные по расхождениям

        #region Все

        [DataMember]
        public decimal DIS_TOTAL_AMNT { get; set; }

        [DataMember]
        public int DIS_TOTAL_CNT { get; set; }

        [DataMember]
        public decimal DIS_GAP_TOTAL_AMNT { get; set; }

        [DataMember]
        public int DIS_GAP_TOTAL_CNT { get; set; }

        [DataMember]
        public decimal DIS_NDS_TOTAL_AMNT { get; set; }

        [DataMember]
        public int DIS_NDS_TOTAL_CNT { get; set; }

        #endregion Все

        #region Закрытые

        [DataMember]
        public decimal DIS_CLS_TOTAL_AMNT { get; set; }

        [DataMember]
        public int DIS_CLS_TOTAL_CNT { get; set; }
        
        [DataMember]
        public decimal DIS_CLS_GAP_TOTAL_AMNT { get; set; }

        [DataMember]
        public int DIS_CLS_GAP_TOTAL_CNT { get; set; }

        [DataMember]
        public decimal DIS_CLS_NDS_TOTAL_AMNT { get; set; }

        [DataMember]
        public int DIS_CLS_NDS_TOTAL_CNT { get; set; }

        #endregion Закрытые

        #region Открытые

        [DataMember]
        public decimal DIS_OPN_TOTAL_AMNT { get; set; }

        [DataMember]
        public int DIS_OPN_TOTAL_CNT { get; set; }

        [DataMember]
        public decimal DIS_OPN_GAP_TOTAL_AMNT { get; set; }

        [DataMember]
        public int DIS_OPN_GAP_TOTAL_CNT { get; set; }

        [DataMember]
        public decimal DIS_OPN_NDS_TOTAL_AMNT { get; set; }

        [DataMember]
        public int DIS_OPN_NDS_TOTAL_CNT { get; set; }

        #endregion Открытые

        #endregion Сводные данные по расхождениям

    }
}
