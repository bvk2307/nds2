﻿using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog
{
    [Serializable]
    public class SounDataContainer
    {
        public int TotalInspectionCount { get; set; }
        public List<Inspection> Inspections { get; set; }
    }
}
