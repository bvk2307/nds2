﻿using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog
{
    [Serializable]
    public sealed class EmployeeData
    {
        public EmployeeType Type { get; set; }

        public List<string> Sids { get; set; }

        public EmployeeData()
        {
            Sids = new List<string>();
        }
    }
}