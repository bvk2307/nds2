﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog
{
    [Serializable]
    public class AutoSelectionFilterConfiguration
    {
        public string IncludeFilter
        {
            get;
            set;
        }

        public string ExcludeFilter
        {
            get;
            set;
        }

        public DateTime LastChanged
        {
            get;
            set;
        }
    }
}
