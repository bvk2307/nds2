﻿using System;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Security;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog
{
    [Serializable]
    public enum EmployeeType : long
    {
        Other = -1,
        Analyst = 0,
        Approver = 1,
        All = 2,
    }
}