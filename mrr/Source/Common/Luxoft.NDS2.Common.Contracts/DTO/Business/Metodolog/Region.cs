﻿using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog
{
    public enum RegionContext
    {
        AmountPVP = 1,
        Employee = 2
    }

    [Serializable]
    public sealed class Region
    {
        public long Id { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public string RegionFullName { get; set; }

        public RegionContext Context { get; set; }

        public bool IsNew { get; set; }

        public List<Inspection> Inspections { get; set; }
    }
}