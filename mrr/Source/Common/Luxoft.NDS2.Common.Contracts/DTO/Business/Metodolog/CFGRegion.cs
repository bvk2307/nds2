﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog
{
    [Serializable]
    public class CFGRegion
    {
        public long? Id { get; set; }

        public Region Region { get; set; }

        public CFGParam Param { get; set; }

        public string ParamValue { get; set; }

        [DisplayName("Регион")]
        [Description("Регион")]
        public string RegionName  { get; set; }

        [DisplayName("Инспекции региона")]
        [Description("Инспекции региона")]
        public string RegionInspections  { get; set; }
    }
}