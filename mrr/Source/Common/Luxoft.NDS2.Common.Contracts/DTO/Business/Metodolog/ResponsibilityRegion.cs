﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog
{
    [Serializable]
    public sealed class ResponsibilityRegion
    {
        public Region Region { get; set; }

        public EmployeeData Analitics { get; set; }

        public EmployeeData Approvers { get; set; }

        [DisplayName("Регионы")]
        [Description("Регионы")]
        public string RegionFullName { get ; set; }

        [DisplayName("Инспекции региона")]
        [Description("Инспекции региона")]
        public string InspectionsList { get; set; }

        [DisplayName("Аналитики")]
        [Description("Аналитики")]
        public string AnalystsList { get; set; }

        [DisplayName("Согласующие")]
        [Description("Согласующие")]
        public string ApproversList { get; set; }

        public ResponsibilityRegion()
        {
            Analitics = new EmployeeData { Type = EmployeeType.Analyst };
            Approvers = new EmployeeData { Type = EmployeeType.Approver };
        }
    }
}