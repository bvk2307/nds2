﻿using System;
using System.ComponentModel;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog
{
    [Serializable]
    public sealed class Inspection
    {
        public bool Selected { get; set; }

        [DisplayName("Код")]
        [Description("Код")]
        public string Code { get; set; }

        [DisplayName("Инспекции региона")]
        [Description("Инспекции региона")]
        public string Name { get; set; }
    }
}