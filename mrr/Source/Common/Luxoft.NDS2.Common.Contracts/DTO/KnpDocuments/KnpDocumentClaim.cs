﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.KnpDocuments
{
    [Serializable]
    public class KnpDocumentClaim
    {
        public long DeclarationRegistrationNumber
        {
            get;
            set;
        }

        public string SonoCode
        {
            get;
            set;
        }

        public long Id
        {
            get;
            set;
        }

        public int Type
        {
            get;
            set;
        }

        public string Number
        {
            get;
            set;
        }

        public DateTime CreatedAt
        {
            get;
            set;
        }

        public DateTime? SeodDate
        {
            get;
            set;
        }

        public DateTime? Date
        {
            get;
            set;
        }

        public string Status
        {
            get;
            set;
        }

        public bool SeodAccepted
        {
            get;
            set;
        }

        public int? CloseReason
        {
            get;
            set;
        }

        public bool HasDiscrepancies
        {
            get;
            set;
        }
    }
}
