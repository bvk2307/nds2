﻿using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Common.Contracts.DTO.KnpDocuments
{
    [Serializable]
    public class KnpDocumentsData
    {
        public List<KnpDocumentDeclaration> Declarations
        {
            get;
            set;
        }

        public List<KnpDocumentClaim> Claims
        {
            get;
            set;
        }

        public List<ClaimExplain> Explains
        {
            get;
            set;
        }

        public List<SeodKnpDocument> Acts
        {
            get;
            set;
        }

        public List<SeodKnpDocument> Decisions
        {
            get;
            set;
        }
    }
}
