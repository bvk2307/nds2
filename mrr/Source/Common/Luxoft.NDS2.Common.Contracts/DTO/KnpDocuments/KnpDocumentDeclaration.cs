﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.KnpDocuments
{
    [Serializable]
    public class KnpDocumentDeclaration
    {
        public long? RegistrationNumber
        {
            get;
            set;
        }

        public string SonoCodeSubmitted
        {
            get;
            set;
        }

        public int? CorrectionNumber
        {
            get;
            set;
        }

        public DateTime? SubmittedAt
        {
            get;
            set;
        }

        public int Status
        {
            get;
            set;
        }

        public string InnReorganized
        {
            get;
            set;
        }

        public int? KnpCloseReasonId
        {
            get;
            set;
        }


        public bool IsAnnulmentSeod
        {
            get;
            set;
        }

        public bool IsAnnulment
        {
            get;
            set;
        }
    }
}
