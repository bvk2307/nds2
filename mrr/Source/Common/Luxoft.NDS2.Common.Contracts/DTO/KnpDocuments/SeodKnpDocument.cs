﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.KnpDocuments
{
    [Serializable]
    public class SeodKnpDocument
    {
        public long DeclarationRegistrationNumber
        {
            get;
            set;
        }

        public string SonoCode
        {
            get;
            set;
        }

        public long Id
        {
            get;
            set;
        }

        public string Number
        {
            get;
            set;
        }
        public string Name
        {
            get;
            set;
        }

        public DateTime CreatedAt
        {
            get;
            set;
        }

        public DateTime? Date
        {
            get;
            set;
        }
    }
}
