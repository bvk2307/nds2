﻿namespace Luxoft.NDS2.Common.Contracts.DTO
{
    public enum DiscrepancyStatus
    {
        Opened = 1,
        Closed
    }
}
