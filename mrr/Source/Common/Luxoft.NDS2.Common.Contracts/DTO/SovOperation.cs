﻿using System;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;

namespace Luxoft.NDS2.Common.Contracts.DTO
{
    [Serializable]
    public class SovOperation
    {
        public long? RequestId { get; set; }

        public RequestStatusType ExecutionStatus { get; set; }
    }

    [Serializable]
    public class DeclarationSovOperation<T> : SovOperation
    {
        public DeclarationRequestData decl { get; set; }

        public QueryConditions Conditions { get; set; }
        public long CommonCount { get; set; }
        public List<T> Result { get; set; }
    }

    [Serializable]
    public class InvoiceSovOperation : DeclarationSovOperation<Invoice>
    {
        public int Chapter { get; set; }
        public int Priority { get; set; }
    }

    [Serializable]
    public class DiscrepancySovOperation : DeclarationSovOperation<DeclarationDiscrepancy>
    {
    }

    [Serializable]
    public class ContragentsSovOperation : SovOperation
    {
        public DeclarationRequestData Declaration;
    }
}
