﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Common.Contracts.DTO
{
    [Serializable]
    public class DeclarationPageResult
    {
        public int TotalMatches
        {
            get;
            set;
        }

        public List<SelectionDeclaration> Rows
        {
            get;
            set;
        }

        public SelectionGroupState State
        {
            get;
            set;
        }
    }

    [Serializable]
    public class DiscrepancyPageResult
    {
        public int TotalMatches
        {
            get;
            set;
        }

        public List<SelectionDiscrepancy> Rows
        {
            get;
            set;
        }

        public SelectionGroupState State
        {
            get;
            set;
        }
    }
}
