﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO
{
    [Serializable]
    public class DictionaryItem
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// описание
        /// </summary>
        public string Description { get; set; }

        public override string ToString()
        {
            return Description;
        }
    }
}