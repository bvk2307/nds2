﻿using selection = Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using System;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Luxoft.NDS2.Common.Contracts.DTO
{
    [DataContract]
    [Serializable]
    public class ActionHistory
    {
        public long Id { get; set; }

        public long SelectionId { get; set; }
 
        [DisplayName("Дата")]
        [Description("Дата сохранения изменений")]
        public DateTime ChangeAt { get; set; }

        [DisplayName("Автор")]
        [Description("Инициатор изменения")]
        public string ChangedBy { get; set; }

        [DisplayName("Статус")]
        [Description("Статус выборки")]
        public string Status { get; set; }

        [DisplayName("Автор")]
        [Description("Инициатор изменения")]
        public string ChangedBySid { get; set; }

        public string Action { get; set; }
       
    }
}
