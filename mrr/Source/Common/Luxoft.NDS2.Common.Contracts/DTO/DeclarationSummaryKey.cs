﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO
{
    /// <summary>
    /// Этот класс описывает ключ группы корректировок НД либо Журнала, поданных одним НП
    /// 1. За один налоговый период (квартал либо месяц)
    /// 2. За себя и всех лиц, правоприемником которых он является
    /// </summary>
    [Serializable]
    public class DeclarationSummaryKey
    {
        /// <summary>
        /// Возвращает или задает ИНН НП, подавшего декларацию (декларант)
        /// </summary>
        public string InnDeclarant
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает эффективный КПП декларанта
        /// </summary>
        public string KppEffective
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает идентификатор налогового периода (квартал, либо месяц)
        /// </summary>
        public int PeriodEffective
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает год налогового периода
        /// </summary>
        public int FiscalYear
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает признак того, что ключ объединяет корректировки журналов, а не НД
        /// </summary>
        public bool IsJournal
        {
            get;
            set;
        }
    }
}
