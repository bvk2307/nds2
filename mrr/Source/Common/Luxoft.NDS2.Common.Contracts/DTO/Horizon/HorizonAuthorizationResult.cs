﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Luxoft.NDS2.Common.Contracts.DTO.Horizon
{
    [DataContract]
    [Serializable]
    public enum HorizonAuthorizationStatus
    {
        Success = 1,
        Unauthorized = 2,
        Error = 3,
        Unkonown = 0
    }

    [DataContract]
    [Serializable]
    public class HorizonAuthorizationResult
    {
        public HorizonAuthorizationResult()
        {
            AccessRigths = new Dictionary<string, List<string>>();
            Status = HorizonAuthorizationStatus.Unkonown;
        }
        [DataMember]
        public HorizonAuthorizationStatus Status { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public Dictionary<string, List<string>> AccessRigths { get; set; }
    }
}
