﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.TaxPeriod
{
    [Serializable]  
    public class TaxPeriodConfiguration
    {
        public int FiscalYearBegin { get; set; }
        public string TaxPeriodBegin { get; set; }
    }
}
