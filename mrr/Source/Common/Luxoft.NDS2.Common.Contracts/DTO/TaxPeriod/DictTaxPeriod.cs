﻿using System;
using System.Collections.Generic;
using CommonComponents.ThesAccess;

namespace Luxoft.NDS2.Common.Contracts.DTO.TaxPeriod
{
    [Serializable]   
    public class DictTaxPeriod
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public int Quarter { get; set; }
        public bool IsMainInQuarter { get; set; }
        public int Month { get; set; }
    }

    [Serializable]   
    public class DictTaxPeriodAccessor : DictTaxPeriod, IAccessorInitializer
    {
        /// <summary>Инициализировать объект.</summary>
        /// <param name="row">
        /// Объект, содержащий нетипизированный элемент классификатора.
        /// </param>
        public void Init( IDataItem row )
        {
            System.Diagnostics.Contracts.Contract.Ensures( !string.IsNullOrEmpty( Code ) );
            System.Diagnostics.Contracts.Contract.Ensures( !string.IsNullOrEmpty( Description ) );
            System.Diagnostics.Contracts.Contract.Ensures( 0 < Quarter && Quarter <= 4 );

            if ( row != null )
            {
                foreach ( KeyValuePair<string, IProperty> keyValuePair in row.PropertyLists )
                {
                    string propertyName = keyValuePair.Value.Name;
                    switch ( propertyName )
                    {case "Code":
                        Code = row.GetValue<string>( propertyName );
                        break;
                    case "Description":
                        Description = row.GetValue<string>( propertyName );
                        break;
                    case "Quarter":
                        int? quarter = row.GetValue<int?>( propertyName );
                        if ( quarter.HasValue )
                            Quarter = quarter.Value;
                        break;
                    case "IsMainInQuarter":
                        bool? isMainInQuarter = row.GetValue<bool?>( propertyName );
                        if ( isMainInQuarter.HasValue )
                            IsMainInQuarter = isMainInQuarter.Value;
                        break;
                    case "Month":
                        Month = row.GetValue<int>(propertyName);
                        break;
                    default:
                        throw new NotSupportedException( string.Format( "Unknown property name: '{0}' for type: {1}", propertyName, GetType().FullName ) );
                    }
                }
            }
        }
    }
}
