﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Business.TaxPeriod
{
    [Serializable]
    public class FullTaxPeriodInfo
    {
        public string TaxPeriod { get; set; }
        public string FiscalYear { get; set; }
        public string Description { get; set; }

        public override string ToString()
        {
            return Description;
        }
    }
}
