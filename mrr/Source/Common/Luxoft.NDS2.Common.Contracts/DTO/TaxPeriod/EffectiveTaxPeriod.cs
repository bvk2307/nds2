﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.TaxPeriod
{
    public enum Quarter { First = 1, Second, Third, Fourth }

    /// <summary>
    /// Этот класс описывает объекты передачи данных о налоговом периоде
    /// </summary>
    [Serializable]
    public class EffectiveTaxPeriod
    {
        /// <summary>
        /// Возвращает или задает квартал года
        /// </summary>
        public Quarter Quarter
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает год
        /// </summary>
        public int Year
        {
            get;
            set;
        }

        /// <summary>
        /// Отображение в UI
        /// </summary>
        public override string ToString()
        {
            return String.Concat((int)Quarter, " кв. ", Year, " г.");
        }
    }
}
