﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Claims
{
    [Serializable]
    public class InvoiceClaimReportItem
    {
        public string RegionCode { get; set; }

        public string SonoCode { get; set; }

        public long DocId { get; set; }

        public DateTime CreateDate { get; set; }

        public int Side { get; set; }

        public DateTime? ReceivedBySeodDate { get; set; }

        public long? SeodRegNumber { get; set; }

        public DateTime? SeodDocDate { get; set; }

        public int SeodAccepted { get; set; }

        public DateTime? SendToTaxpayerDate { get; set; }

        public DateTime? DeliveryDate { get; set; }

        public string DeliveryType { get; set; }

        public DateTime? CloseDate { get; set; }

        public string Status { get; set; }

        public DateTime? StatusDate { get; set; }

        public long? DeclRegNumber { get; set; }

        public long? AskNumber { get; set; }

        public string PeriodCode { get; set; }

        public string FiscalYear { get; set; }

        public string Inn { get; set; }

        public int DeclarationType { get; set; }

        public decimal? Nds1735 { get; set; }

        public decimal? Nds1731 { get; set; }

        public string TaxBaseAmnt { get; set; }

        public decimal? NdsCalculated { get; set; }

        public decimal? DeductionSum { get; set; }

        public decimal? NdsSum { get; set; }

        public decimal? DeductionPercent { get; set; }

        public decimal? TaxBurdenPercent { get; set; }

        public string Subscriber { get; set; }

        public int? SubscriberType { get; set; }

        public decimal? BuyerDiscrepancyAmount { get; set; }

        public int? BuyerDiscrepancyCount { get; set; }

        public decimal? BuyerGapDiscrepancyAmt { get; set; }

        public int? BuyerGapDiscrepancyQty { get; set; }

        public decimal? BuyerNdsDiscrepancyAmt { get; set; }

        public int? BuyerNdsDiscrepancyQty { get; set; }

        public decimal? SellerDiscrepancyAmount { get; set; }

        public int? SellerDiscrepancyCount { get; set; }

        public decimal? SellerGapDiscrepancyAmt { get; set; }

        public int? SellerGapDiscrepancyQty { get; set; }

        public decimal? SellerNdsDiscrepancyAmt { get; set; }

        public int? SellerNdsDiscrepancyQty { get; set; }

        public string Sur { get; set; }

        public int DiscrepancySign { get; set; }

        public int HasActiveAccount { get; set; }

    }
}
