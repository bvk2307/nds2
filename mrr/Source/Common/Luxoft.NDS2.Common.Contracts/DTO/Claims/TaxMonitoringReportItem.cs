﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Claims
{
    [Serializable]
    public class TaxMonitoringReportItem
    {
        public long LineNumber { get; set; }

        public long DocId { get; set; }

        public string RowKey { get; set; }

        public int IsUnconfirmed { get; set; }
        
        public int Chapter { get; set; }
        
        public string OperationCode { get; set; }
        
        public string InvoiceNum { get; set; }
        
        public DateTime? InvoiceDate { get; set; }
        
        public string ChangeNum { get; set; }

        public DateTime? ChangeDate { get; set; }
        
        public string CorrectionInvoiceNum { get; set; }

        public DateTime? CorrectionInvoiceDate { get; set; }
        
        public string CorrectionChangeNum { get; set; }

        public DateTime? CorrectionChangeDate { get; set; }
        
        public string ConfirmDocNum { get; set; }

        public DateTime? ConfirmDocDate { get; set; }

        public string AcceptDate { get; set; }
        
        public string BuyerInn { get; set; }
        
        public string SellerInn { get; set; }

        public string ContractorInn { get; set; }

        public string ContractorKpp { get; set; }

        public string ContractorName { get; set; }
        
        public string SellerInvoiceNum { get; set; }

        public DateTime? SellerInvoiceDate { get; set; }
        
        public string SellerCurrencyOkvCode { get; set; }
        
        public decimal? SellerPriceTotal { get; set; }
        
        public decimal? SellerNdsTotal { get; set; }
        
        public string BrokerInn { get; set; }
        
        public string CustomDeclarationNum { get; set; }
        
        public string CurrencyOkvCode { get; set; }
        
        public decimal? PriceAmount { get; set; }

        public decimal? NdsAmount { get; set; }
        
        public string ErrorCode { get; set; }

        public decimal? Price18 { get; set; }

        public decimal? Price10 { get; set; }

        public decimal? Price0 { get; set; }

        public decimal? Nds18 { get; set; }

        public decimal? Nds10 { get; set; }

        public decimal? PriceTaxFree { get; set; }

        public decimal? TaxDecrease { get; set; }

        public decimal? TaxIncrease { get; set; }

        public decimal? NdsDecrease { get; set; }

        public decimal? NdsIncrease { get; set; }

        public string TaxPeriod { get; set; }
    }
}
