﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Claims
{
    [Serializable]
    public class SelectionClaimCompare
    {
        public int LineNumber { get; set; }

        public DateTime? CreateDate { get; set; }

        public long? DocId { get; set; }

        public string TaxPeriod { get; set; }

        public string RegionCode { get; set; }

        public string InspectionCode { get; set; }

        public string Inn { get; set; }

        public string Kpp { get; set; }

        public decimal? Side1TotalAmount { get; set; }

        public decimal? Side1TotalQuantity { get; set; }

        public decimal? Side1GapAmount { get; set; }

        public int? Side1GapQuantity { get; set; }

        public decimal? Side1NdsAmount { get; set; }

        public int? Side1NdsQuantity { get; set; }

        public decimal? Side2TotalAmount { get; set; }

        public int? Side2TotalQuantity { get; set; }

        public decimal? Side2GapAmount { get; set; }

        public int? Side2GapQuantity { get; set; }

        public decimal? Side2NdsAmount { get; set; }

        public int? Side2NdsQuantity { get; set; }

        public decimal? TotalAmount { get; set; }

        public int? TotalQuantity { get; set; }

        public decimal? GapAmount { get; set; }

        public int? GapQuantity { get; set; }

        public decimal? NdsAmount { get; set; }

        public int? NdsQuantity { get; set; }

        public decimal? ClaimTotalAmount { get; set; }

        public int? ClaimTotalQuantity { get; set; }

        public decimal? ClaimGapAmount { get; set; }

        public int? ClaimGapQuantity { get; set; }

        public decimal? ClaimNdsAmount { get; set; }

        public int? ClaimNdsQuantity { get; set; }

    }
}
