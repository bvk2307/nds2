﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Claims
{
    [Serializable]
    public class InvoiceClaim
    {
        public int? Excluded { get; set; }

        public int? TaxMonitoring { get; set; }

        public string LastUser { get; set; }

        public long DocId { get; set; }

        public string RegionCode { get; set; }

        public string RegionName { get; set; }

        public string SonoCode { get; set; }

        public string SonoName { get; set; }

        public string Inn { get; set; }

        public string Kpp { get; set; }

        public string Name { get; set; }

        public int? BuyerDiscrepancyCount { get; set; }

        public decimal? BuyerDiscrepancyAmount { get; set; }

        public int? SellerDiscrepancyCount { get; set; }

        public decimal? SellerDiscrepancyAmount { get; set; }

        public string InnReorg { get; set; }
    
        public string TaxPeriod { get; set; }

        public string TaxPeriodCode { get; set; }

        public string CorrectionNumber { get; set; }

        public DateTime? CorrectionDate { get; set; }

        public string DeclarationSign { get; set; }

        public int DeclSignCode { get; set; }

        public decimal? NdsAmount { get; set; }

        public DateTime? FaultFormingDate { get; set; }

        public DateTime? ReadyToSendDate { get; set; }

        public DateTime? SentToSeodDate { get; set; }

        public DateTime? FaultSendToSeodDate { get; set; }

        public DateTime? ReceivedBySeodDate { get; set; }

        public long? SeodRegNumber { get; set; }

        public DateTime? SeodDocDate { get; set; }

        public DateTime? SendToTaxpayerDate { get; set; }

        public string DeliveryType { get; set; }

        public DateTime? TaxpayerReceiveDate { get; set; }

        public DateTime? CloseDate { get; set; }

        public string CloseType { get; set; }

        public int? CloseTypeId { get; set; }

        public int StatusCode { get; set; }
    }

}
