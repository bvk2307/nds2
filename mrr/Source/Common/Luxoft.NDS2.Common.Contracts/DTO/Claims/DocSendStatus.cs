﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Claims
{
    [Serializable]
    public class DocSendStatus
    {
        public int DocType { get; set; }

        public DateTime? SendDate { get; set; }

        public DateTime? SendTime { get; set; }

        public string Username { get; set; }

        public bool WasSent { get; set; }
    }
}
