﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.UserTask
{
    [Serializable]
    public class UserTaskInspectorSummary : UserTaskSummary
    {
        public string UserSid { get; set; }

        public string SonoCode { get; set; }
    }
}