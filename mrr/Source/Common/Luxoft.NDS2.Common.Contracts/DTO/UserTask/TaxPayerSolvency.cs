﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.UserTask
{
    /// <summary>
    /// Статус налогоплательщика
    /// </summary>
    [Serializable]
    public class TaxPayerSolvency
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }
         
    }
}