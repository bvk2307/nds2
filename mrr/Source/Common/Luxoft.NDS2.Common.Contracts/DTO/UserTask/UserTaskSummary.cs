﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.UserTask
{
    [Serializable]
    public class UserTaskSummary
    {
        public long ReportId { get; set; }

        /// <summary>
        /// Тип ПЗ
        /// </summary>
        public int TaskType { get; set; }

        /// <summary>
        /// К-во ПЗ завершенных в срок
        /// </summary>
        public int CompletedOnSchedule { get; set; }

        /// <summary>
        /// К-во ПЗ завершенных просроченных
        /// </summary>
        public int CompletedOverdue { get;set;}

        /// <summary>
        /// К-во ПЗ в работе
        /// </summary>
        public int InProgressOnSchedule { get; set; }

        /// <summary>
        /// Кол-во ПЗ в работе (просроченных)  
        /// </summary>
        public int InProgressOverdue { get; set; }
    }
}