﻿namespace Luxoft.NDS2.Common.Contracts.DTO.UserTask
{
    /// <summary>
    /// Статус налогоплательщика для карточки НП
    /// </summary>
    [System.Serializable]
    public class TaxPayerStatus
    {
        /// <summary>
        /// Статус НП (Платёжеспособный / Неплатёжеспособный / Наличие основных средств)
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Дата установки статуса
        /// </summary>
        public System.DateTime? StatusDate { get; set; }

        /// <summary>
        /// Комментарий к статусу
        /// </summary>
        public string StatusComment { get; set; }
    }
}
