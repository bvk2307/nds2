﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.UserTask
{
    /// <summary>
    /// Список пользовательских заданий - Общие сведения 
    /// </summary>
    [Serializable]
    public class Summary
    {
        /// <summary>
        /// Тип задания
        /// </summary>
        public string TaskTypeName { get; set; }

        /// <summary>
        /// На исполнении
        /// </summary>
        public int UnprocessedQty { get; set; }

        /// <summary>
        /// Из них просроченных
        /// </summary>
        public int ExpiredQty { get; set; }


    }
}