﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.UserTask
{
    /// <summary>
    /// Пользовательское задание - для списка
    /// </summary>
    [Serializable]
    public class UserTaskInList : UserTask
    {
        /// <summary>
        /// Текущее состояние отработки ПЗ
        /// 1 - Просрочено
        /// 2 - Истекает
        /// 3 - Создано
        /// 4 - Выполнено
        /// 5 - Закрыто
        /// </summary>
        public int CurrentProcessState { get; set; }

        /// <summary>
        /// Отчетный год
        /// </summary>
        public int FiscalYear { get; set; }

        /// <summary>
        /// Назаначенный на задание пользователь
        /// </summary>
        public string AssignedSid { get; set; }

        /// <summary>
        /// Назаначенный на задание пользователь(ФИО)
        /// </summary>
        public string AssignedName { get; set; }

        /// <summary>
        /// Тип задания - Code
        /// </summary>
        public int TaskType { get; set; }

        /// <summary>
        /// Тип задания
        /// </summary>
        public string TaskTypeName{get;set;}

        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime? CreateDate { get; set; }

        /// <summary>
        /// Налогоплательщик: ИНН
        /// </summary>
        public string TaxPayerInn { get; set; }

        /// <summary>
        /// Налогоплательщик: КПП
        /// </summary>
        public string TaxPayerKpp { get; set; }

        /// <summary>
        /// Налогоплательщик: Наименование
        /// </summary>
        public string TaxPayerName { get; set; }

        /// <summary>
        ///  Связанный документ: Тип Code
        /// </summary>
        public int DocumentTypeCode { get; set; }

        /// <summary>
        /// Регистрационный №
        /// </summary>
        public long DocumentNumber { get; set; }

        /// <summary>
        /// № корр./ версии
        /// </summary>
        public string DocumentCorrectionNumber { get; set; }


        /// <summary>
        /// Отчетный период - Код
        /// </summary>
        public string DocumentPeriodCode { get; set; }

        /// <summary>
        /// Отчетный период - Наименование
        /// </summary>
        public string DocumentPeriodName { get; set; }

        /// <summary>
        /// Отчетный период - поле для поиска и сортировки
        /// </summary>
        public string DocumentPeriodId { get; set; }


        /// <summary>
        /// Инспекция: Код
        /// </summary>
        public string SonoCode { get; set; }

        /// <summary>
        /// Инспекция: Наименование
        /// </summary>
        public string SonoName { get; set; }

        /// <summary>
        /// Регион: Код
        /// </summary>
        public string RegionCode { get; set; }

        /// <summary>
        /// Регион: Наименование
        /// </summary>
        public string RegionName { get; set; }

        /// <summary>
        /// идентификатор документа
        /// </summary>
        public long DocumentId { get; set; }

    }
}