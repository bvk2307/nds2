﻿using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Common.Contracts.DTO.UserTask
{
    /// <summary>
    /// Пользовательское задание - карточка
    /// </summary>
    [Serializable]
    public class UserTaskInDetails : UserTask
    {

        /// <summary>
        /// Срок выполнения задания (в днях)
        /// </summary>
        public int DuePeriod { get; set; }

        //общие сведения

        /// <summary>
        /// СУР НП
        /// </summary>
        public int? SurCode { get; set; }

        /// <summary>
        /// Статус  КНП
        /// </summary>
        public string KnpStatus { get; set; }
        /// <summary>
        /// Признак НД
        /// </summary>
        public string Sign { get; set; }
        /// <summary>
        /// Сумма НДС
        /// </summary>
        public decimal? NdsTotal { get; set; }



        //Сведения о выполнении пользовательского задания

        //Истребование документов/Ввод ответа на истребование

        ///// <summary>
        ///// Номер регистрации в СЭОД
        ///// </summary>
        //public int? SeodDocNumber { get; set; }

        ///// <summary>
        ///// дата связанного документа
        ///// </summary>
        //public DateTime? SeodDocDate { get; set; }

        /// <summary>
        /// Номер связанного документа СЭОД
        /// </summary>
		  public string SeodNumber { get; set; }


        /// <summary>
        /// Дата связанного документа СЭОД
        /// </summary>
        public DateTime? ClaimdocDate { get; set; }

        /// <summary>
        /// «Номер» данный при создании в АСК НДС-2
        /// </summary>
        public int? ClaimdocCreateNumber { get; set; }

        /// <summary>
        /// id связанного ответа 
        /// </summary>
        public long? ExplainId { get; set; }

        /// <summary>
        /// дата из связанного ответа 
        /// </summary>
        public DateTime? AnswerDate { get; set; }

        /// <summary>
        /// статус из связанного  ответа 
        /// </summary>
        public string AnswerStatus { get; set; }

        /// <summary>
        /// статус из связанного  ответа - код
        /// </summary>
        public int? AnswerStatusCode { get; set; }


        //Анализ налогоплательщика

        /// <summary>
        /// тип связанного документа СЭОД - Анализ налогоплательщика
        /// </summary>
        public string ClaimdocTypeName { get; set; }


        /// <summary>
        /// Статус  налогоплательщика
        /// </summary>
        public int? TaxPayerSolvency { get; set; }

        /// <summary>
        /// Комментарий
        /// </summary>
        public string Comment { get; set; }


        /// <summary>
        /// Список исполнителей -источник данных для grid
        /// </summary>
        public List<Actor> ActorList { get; set; }

        /// <summary>
        /// Статус налогоплательщика -источник данных для combo
        /// </summary>
        public List<TaxPayerSolvency> TaxPayerSolvencyList { get; set; }

        /// <summary>
        /// Ввести ответ - Доступна
        /// </summary>
        public int OtherAnswerNotExists { get; set; }

        /// <summary>
        /// Код причины закрытия КНП корректировки
        /// </summary>
        public int? KnpCloseReasonId { get; set; }
    }
}