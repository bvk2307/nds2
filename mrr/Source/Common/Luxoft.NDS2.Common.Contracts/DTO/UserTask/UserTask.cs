﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.UserTask
{
    [Serializable]
    public class UserTask
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Статус - идентификатор
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// Статус
        /// </summary>
        public string StatusName{get;set;}

        /// <summary>
        /// Дата выполнения: Фактическая
        /// </summary>
        public DateTime? ActualCompleteDate{ get; set; }

        /// <summary>
        /// Дата выполнения: Плановая
        /// </summary>
        public DateTime? PlannedCompleteDate { get; set; }

        /// <summary>
        /// Кол-во просроченных дней 
        /// </summary>
        public int DaysAfterExpire { get; set; }

    }
}