﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.UserTask
{
    [Serializable]
    public class UserTaskRegionSummary : UserTaskSummary
    {
        public string RegionCode { get; set; }

        public int Unassigned { get; set; }
    }
}