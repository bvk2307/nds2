﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.UserTask
{
    [Serializable]
    public class UserTaskSonoSummary : UserTaskRegionSummary
    {
        public string SonoCode { get; set; }     
    }
}