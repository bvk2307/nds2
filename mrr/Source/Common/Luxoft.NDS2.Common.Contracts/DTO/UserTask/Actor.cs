﻿using System;
using System.ComponentModel;

namespace Luxoft.NDS2.Common.Contracts.DTO.UserTask
{
    /// <summary>
    /// Пользовательское задание - запись об исполнителе
    /// </summary>
    [Serializable]
    public class Actor
    {
        /// <summary>
        /// Назаначенный на задание пользователь(ФИО)
        /// </summary>
        [DisplayName("Инспектор")]
        [Description("ФИО инспектора")]
        public string AssignedName { get; set; }
        /// <summary>
        /// Назаначенный на задание пользователь(ФИО)
        /// </summary>
        [DisplayName("Дата назначения")]
        [Description("Дата назначения")]
        public DateTime? AssignDate { get; set; }
         
    }
}