﻿using System;
using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.Security;

namespace Luxoft.NDS2.Common.Contracts.DTO.UserTask
{
    /// <summary>
    /// права текущего польз на форме
    /// </summary>
    [Serializable]
    public class UserRights
    {
        /// <summary>
        /// ЦА SonoCode
        /// </summary>
        private const string _centralSonoCode = "0000";


        public UserRights(List<AccessRight> _userPermissions)
        {
            UserTaskRole = UserTaskRole.None;

            var administratorRight = GetRoleRight(
                _userPermissions,
					 Constants.SystemPermissions.Operations.UserTaskAdmin);
            if (administratorRight != null)
            {
                UserTaskRole = UserTaskRole.Administrator;
                SonoCode = administratorRight.StructContext;
                return;
            }

				var inspectorRight = GetRoleRight(_userPermissions, Constants.SystemPermissions.Operations.UserTaskEnter);
            if (inspectorRight != null)
            {
                UserTaskRole = UserTaskRole.Inspector;
                SonoCode = inspectorRight.StructContext;
            }
        }

        private AccessRight GetRoleRight(List<AccessRight> _userPermissions, string roleName)
        {
            return _userPermissions.FirstOrDefault(o => o.Name.Equals(roleName));
        }

        /// <summary>
        /// роль в польз заданиях
        /// </summary>
        public UserTaskRole UserTaskRole { get; set; }

        /// <summary>
        /// ЦА
        /// </summary>
        public bool IsCentral {get { return SonoCode == _centralSonoCode; }}

        /// <summary>
        /// УФНС
        /// </summary>
        public bool IsManagement {get { return SonoCode.EndsWith("00") && !IsCentral; }}

        /// <summary>
        /// Регион-Код
        /// </summary>
        public string RegionCode { get { return SonoCode.Substring(0, 2); } }


        /// <summary>
        /// Инспекция: Код
        /// </summary>
        public string SonoCode { get; private set; }

        /// <summary>
        /// null - .tor 
        /// </summary>
        public UserRights()
        {
            UserTaskRole = UserTaskRole.None;
            SonoCode = "errr";
        }
    }

    /// <summary>
    /// роль в польз заданиях
    /// </summary>
    public enum UserTaskRole
    {
        /// <summary>
        /// исполнитель
        /// </summary>
        Inspector,

        /// <summary>
        /// админ
        /// </summary>
        Administrator,

        None
    }
}