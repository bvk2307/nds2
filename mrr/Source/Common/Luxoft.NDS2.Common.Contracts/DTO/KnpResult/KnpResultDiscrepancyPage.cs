﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.KnpResult
{
    /// <summary>
    /// Класс для работы с расхождениями КНП
    /// включаемыми в акт/решение
    /// </summary>
    [Serializable]
    public class KnpResultDiscrepancyPage
    {
        /// <summary>
        /// общая информация о расхождениях отобранных по фильтру 
        /// если фильтр не задан то NULL
        /// </summary>
        public KnpResultDiscrepancySummary MatchesSummary
        {
            get;
            set;
        }

        /// <summary>
        /// общее количество расхождений без учета фильтра
        /// не NULL
        /// </summary>
        public KnpResultDiscrepancySummary TotalSummary
        {
            get;
            set;
        }

        /// <summary>
        /// собственно расхождения кнп
        /// </summary>
        public KnpResultDiscrepancy[] Discrepancies
        {
            get;
            set;
        }
    }
}
