﻿using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.KnpResult
{
    [Serializable]
    public class KnpResultDiscrepancySummary
    {
        public int Quantity
        {
            get;
            set;
        }

        public decimal Amount
        {
            get;
            set;
        }

        public decimal ActAmount
        {
            get;
            set;
        }

        public decimal DecisionAmount
        {
            get;
            set;
        }

        public int IncludedQuantity
        {
            get;
            set;
        }

        public int ClosedQuantity
        {
            get;
            set;
        }
    }
}
