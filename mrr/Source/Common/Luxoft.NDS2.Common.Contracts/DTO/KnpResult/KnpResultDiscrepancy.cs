﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.KnpResult
{
    [Serializable]
    public class KnpResultDiscrepancy
    {
        # region Включение в Акт \ Решение

        /// <summary>
        /// Возвращает или задает признак включения расхождения в Акт \ Решение
        /// </summary>
        public bool Included
        {
            get;
            set;
        }

        public long DocumentId
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает SID пользователя, захватившего работу с расхождением
        /// (если не равен SID текущего пользователя, то включение в Акт \ Решение не допустимо
        /// </summary>
        public string LockedByUserId
        {
            get;
            set;
        }
        
        /// <summary>
        /// Возвращает или задает сумму расхождения по акту, руб.
        /// </summary>
        public decimal? AmountByAct
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает сумму расхождения по решению, руб.
        /// </summary>
        public decimal? AmountByDecision
        {
            get;
            set;
        }

        # endregion

        # region Атрибуты расхождения

        /// <summary>
        /// Возвращает или задает Идентификатор расхождения
        /// </summary>
        public long Id
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает тип расхождения
        /// </summary>
        public string Type
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает сумму расхождения по результатам сопоставления
        /// </summary>
        public decimal Amount
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает статус расхождения
        /// </summary>
        public DiscrepancyStatus Status
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает признак того, что расхождение не имеет статус Закрыто
        /// </summary>
        public bool Opened
        {
            get;
            set;
        }

        # endregion

        # region Аттрибуты счета-фактуры НП

        /// <summary>
        /// Возвращает или задает номер раздела счета-фактуры НП 
        /// </summary>
        public AskInvoiceChapterNumber? InvoiceChapter
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает номер счета-фактуры по данным НП
        /// </summary>
        public string InvoiceNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает дату счета-фактуры по данным НП
        /// </summary>
        public DateTime? InvoiceDate
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает сумму счета-фактуры по данным НП
        /// </summary>
        public decimal? InvoiceAmount
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает сумму НДС счета-фактуры по данным НП
        /// </summary>
        public decimal? InvoiceAmountNds
        {
            get;
            set;
        }

        # endregion

        # region Аттрибуты контрагента

        /// <summary>
        /// Возвращает или задает ИНН контрагента (декларант)
        /// </summary>
        public string ContractorInn
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает КПП контрагента (декларант)
        /// </summary>
        public string ContractorKpp
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает наименование контрагента (декларант)
        /// </summary>
        public string ContractorName
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает ИНН контрагента (реорганизованное лицо)
        /// </summary>
        public string ContractorReorganizedInn
        {
            get;
            set;
        }

        # endregion
    }
}
