﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.KnpResult
{
    /// <summary>
    /// Этот класс реализует объект передачи данных строки отчета об Актах и Решениях
    /// </summary>
    [Serializable]
    public class KnpResultReportSummary 
    {
        /// <summary>
        /// Идентификатор агрегата
        /// </summary>
        public long ReportId
        {
            get;
            set;
        }

        /// <summary>
        /// Код региона
        /// </summary>
        public string RegionCode
        {
            get;
            set;
        }

        /// <summary>
        /// Код инспекции
        /// </summary>
        public string SonoCode
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает к-во Актов
        /// </summary>
        public int ActQuantity
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает к-во расхождений вида НДС, отобранных в акты
        /// </summary>
        public int ActNdsDiscrepancyQuantity
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает к-во расхождений вида разрыв, отобранных в акты
        /// </summary>
        public int ActGapQuantity
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает сумму расхождений вида НДС, отобранных в акты
        /// </summary>
        public decimal ActNdsDiscrepancyAmount
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает сумму расхождений вида разрыв, отобранных в акты
        /// </summary>
        public decimal ActGapAmount
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает к-во решений
        /// </summary>
        public int DecisionQuantity
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает к-во расхождений вида НДС, отобранных в решения
        /// </summary>
        public int DecisionNdsDiscrepancyQuantity
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает к-во расхождений вида разрыв, отобранных в решения
        /// </summary>
        public int DecisionGapQuantity
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает сумму расхождений вида НДС, отобранных в решения
        /// </summary>
        public decimal DecisionNdsDiscrepancyAmount
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает сумму расхождений вида разрыв, отобранных в решения
        /// </summary>
        public decimal DecisionGapAmount
        {
            get;
            set;
        }
    }
}
