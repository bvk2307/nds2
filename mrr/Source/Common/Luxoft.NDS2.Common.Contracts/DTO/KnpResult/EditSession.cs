﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.KnpResult
{
    [Serializable]
    public class EditSession
    {
        public long DocumentId
        {
            get;
            set;
        }

        public string UserSid
        {
            get;
            set;
        }

        public string UserName
        {
            get;
            set;
        }

        public DateTime ExpiredAt
        {
            get;
            set;
        }
    }
}
