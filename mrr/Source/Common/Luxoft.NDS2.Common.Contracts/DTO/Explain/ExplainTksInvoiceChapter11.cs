﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Explain
{
    [Serializable]
    public class ExplainTksInvoiceChapter11 : IExplainTksInvoice
    {
        public long ExplainZip { get; set; }

        public string RowKey { get; set; }

        public AskInvoiceChapterNumber Chapter { get; set; }

        public ExplainTksInvoiceState Status { get; set; }

        public string OrdinalNumber { get; set; }

        public ulong? OperationCodesBit { get; set; }

        public ulong? OperationCodesBitOrder { get; set; }

        public ulong? NewOperationCodesBit { get; set; } 

        public string InvoiceNumber { get; set; }

        public string NewInvoiceNumber { get; set; }

        public DateTime? InvoiceDate { get; set; }

        public DateTime? NewInvoiceDate { get; set; }

        public string ChangeNumber { get; set; }

        public string NewChangeInvoiceNumber { get; set; }

        public DateTime? ChangeDate { get; set; }

        public DateTime? NewChangeDate { get; set; }

        public string CorrectionNumber { get; set; }

        public string NewCorrectionNumber { get; set; }

        public DateTime? CorrectionDate { get; set; }

        public DateTime? NewCorrectionDate { get; set; }

        public string ChangeCorrectionNumber { get; set; }

        public string NewChangeCorrectionNumber { get; set; }

        public DateTime? ChangeCorrectionDate { get; set; }

        public DateTime? NewChangeCorrectionDate { get; set; }

        public string OkvCode { get; set; }

        public string NewOkvCode { get; set; }

        public string BrokerInn { get; set; }

        public string NewBrokerInn { get; set; }

        public string BrokerKpp { get; set; }

        public string NewBrokerKpp { get; set; }

        public string SellerInn { get; set; }

        public string NewSellerInn { get; set; }

        public string SellerKpp { get; set; }

        public string NewSellerKpp { get; set; }

        public string DealCode { get; set; }

        public string NewDealCode { get; set; }

        public decimal? Amount { get; set; }

        public decimal? NewAmount { get; set; }

        public string NewChangeNumber
        {
            get;
            set;
        }
    }
}
