﻿using System;
using System.ComponentModel;

namespace Luxoft.NDS2.Common.Contracts.DTO.Explain
{
    [Serializable]
    public class ExplainTksInvoiceUnconfirmed
    {
        public long ExplainZip { get; set; }

        [DisplayName("Номер СФ")]
        [Description("Номер счета-фактуры продавца")]
        public string InvoiceNumber { get; set; }

        [DisplayName("Дата СФ")]
        [Description("Дата счета-фактуры продавца")]
        public DateTime? InvoiceDate { get; set; }

        [DisplayName("ИНН")]
        [Description("ИНН контрагента")]
        public string ContractorInn { get; set; }

        [DisplayName("КПП")]
        [Description("КПП контрагента")]
        public string ContractorKpp { get; set; }
    }
}
