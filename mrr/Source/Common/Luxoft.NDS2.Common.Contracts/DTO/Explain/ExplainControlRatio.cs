﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Explain
{
    /// <summary>
    /// Этот класс реализует объект передачи данных об ответе НП по КС
    /// </summary>
    [Serializable]
    public class ExplainControlRatio
    {
        /// <summary>
        /// Возвращает или задает код КС
        /// </summary>
        public string ControlRatioCode
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает текст пояснения НП по КС
        /// </summary>
        public string ExplainText
        {
            get;
            set;
        }
    }
}
