﻿namespace Luxoft.NDS2.Common.Contracts.DTO.Explain
{
    public enum InvoiceAttributeState
    {
        Deleted,
        Added,
        NotChanged
    }
}
