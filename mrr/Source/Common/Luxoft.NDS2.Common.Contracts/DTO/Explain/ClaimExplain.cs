﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply;
using System;
using FLS.Common.Lib.Attributes;

namespace Luxoft.NDS2.Common.Contracts.DTO.Explain
{
    /// <summary>
    /// Этот класс представляет объект передачи данных пояснения\ответа для отображения в списке пояснений автотребования
    /// </summary>
    [Serializable]    
    public class ClaimExplain
    {
       /// <summary>
        /// Возвращает или задает идентификатор пояснения (МРР)
        /// </summary>
        public long Id
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает номер пояснения (СЭОД)
        /// </summary>
        public string Number
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает дату пояснения (СЭОД)
        /// </summary>
        public DateTime Date
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает статус пояснения / ответа
        /// </summary>
        public int Status
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает дату статуса пояснения / ответа
        /// </summary>
        public DateTime? StatusDate
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает название статуса пояснения / ответа
        /// </summary>
        public string StatusName
        {
            get;
            set;
        }
        /// <summary>
        /// Возвращает или задает признак получения пояснения по ТКС
        /// </summary>
        public bool ReceivedByTks
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает признак наличия в контейнере пояснения по СФ 
        /// </summary>
        public bool HasInvoicesExplain
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает признак наличия в контейнере пояснения по КС
        /// </summary>
        public bool HasControlRatioExplain
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает признак наличия в контейнере пояснения по иным основаниям
        /// </summary>
        public bool HasOtherReasonExplain
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает название типа пояснения
        /// </summary>
        public string TypeName
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает идентификатор АТ
        /// </summary>
        public long ClaimId
        {
            get;
            set;
        }
    }
}
