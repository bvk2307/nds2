﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Explain
{
    public interface IExplainTksInvoice : IExplainTksInvoiceBase
    {
        string OrdinalNumber { get; set; }

        ulong? OperationCodesBit { get; set; }

        ulong? NewOperationCodesBit { get; set; }

        string ChangeNumber { get; set; }

        string NewChangeNumber { get; set; }

        DateTime? ChangeDate { get; set; }

        DateTime? NewChangeDate { get; set; }

        string CorrectionNumber { get; set; }

        string NewCorrectionNumber { get; set; }

        DateTime? CorrectionDate { get; set; }

        DateTime? NewCorrectionDate { get; set; }

        string ChangeCorrectionNumber { get; set; }

        string NewChangeCorrectionNumber { get; set; }

        DateTime? ChangeCorrectionDate { get; set; }

        DateTime? NewChangeCorrectionDate { get; set; }

        string OkvCode { get; set; }

        string NewOkvCode { get; set; }
    }
}
