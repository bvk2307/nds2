﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Explain
{
    public interface IExplainTksBookInvoice : IExplainTksInvoice
    {
        InvoiceReceiveDate[] ReceiveDates { get; set; }

        InvoiceReceiptDocument[] ReceiptDocuments { get; set; }

        InvoiceContractor[] Contractors { get; set; }

        string BrokerInn { get; set; }

        string NewBrokerInn { get; set; }

        string BrokerKpp { get; set; }

        string NewBrokerKpp { get; set; }
    }
}
