﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Explain
{
    [Serializable]
    public class InvoiceReceiveDate : InvoiceAttribute
    {
        public DateTime? ReceiveDate { get; set; }
    }
}
