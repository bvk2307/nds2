﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Explain
{
    [Serializable]
    public class InvoiceAttribute
    {
        public long ExplainZip { get; set; }

        public string InvoiceRowKey { get; set; }

        public InvoiceAttributeState State { get; set; }
    }
}
