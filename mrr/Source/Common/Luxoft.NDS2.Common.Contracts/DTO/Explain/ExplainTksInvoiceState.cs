﻿namespace Luxoft.NDS2.Common.Contracts.DTO.Explain
{
    public enum ExplainTksInvoiceState
    {
        Confirmed = 1,
        Changed,
        Ignored
    }
}
