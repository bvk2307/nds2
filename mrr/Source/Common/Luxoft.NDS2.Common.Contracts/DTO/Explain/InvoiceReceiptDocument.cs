﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Explain
{
    [Serializable]
    public class InvoiceReceiptDocument : InvoiceAttribute
    {
        public string DocumentNumber { get; set; }

        public DateTime? DocumentDate { get; set; }
    }
}
