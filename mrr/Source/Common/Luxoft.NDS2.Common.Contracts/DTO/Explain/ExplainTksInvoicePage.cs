﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Explain
{
    [Serializable]
    public class ExplainTksInvoicePage<TInvoice> : PageResult<TInvoice>
        where TInvoice : IExplainTksInvoiceBase
    {
        public ExplainTksInvoicePage(PageResult<TInvoice> page)
        {
            TotalAvailable = page.TotalAvailable;
            TotalMatches = page.TotalMatches;
            Rows = page.Rows;
        }

        public long RequestId { get; set; }

        public ExplainInvoiceRequestState Status { get; set; }
    }
}
