﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Explain
{
    public interface IExplainTksInvoiceBase
    {
        long ExplainZip { get; }

        string RowKey { get; }

        AskInvoiceChapterNumber Chapter { get; }

        ExplainTksInvoiceState Status { get; }

        string OkvCode { get; }

        string NewOkvCode { get; }

        string InvoiceNumber { get; }

        string NewInvoiceNumber { get; }

        DateTime? InvoiceDate { get; }

        DateTime? NewInvoiceDate { get; }
    }
}
