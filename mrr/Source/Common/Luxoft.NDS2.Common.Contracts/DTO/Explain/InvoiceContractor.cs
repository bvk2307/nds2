﻿using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Explain
{
    [Serializable]
    public class InvoiceContractor : InvoiceAttribute
    {
        public string Inn { get; set; }

        public string Kpp { get; set; }
    }
}
