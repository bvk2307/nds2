﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Explain
{
    [Serializable]
    public class ExplainTksInvoiceRequestCache
    {
        public long? RequestId { get; set; }

        public ExplainInvoiceRequestState? Status { get; set; }
    }
}
