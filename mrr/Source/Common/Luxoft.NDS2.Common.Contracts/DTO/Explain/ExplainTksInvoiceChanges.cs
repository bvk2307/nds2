﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Explain
{
    [Serializable]
    public class ExplainTksInvoiceChanges
    {
        /// <summary>
        /// id пояснения
        /// </summary>
        public long ExplainId { get; set; }

        /// <summary>
        /// входящая дата пояснения
        /// </summary>
        public DateTime ExplainIncomingDate { get; set; }

        /// <summary>
        /// zip пояснения
        /// </summary>
        public long ExplainZip { get; set; }

        /// <summary>
        /// идентификатор СФ 
        /// </summary>
        public string RowKey { get; set; }

        /// <summary>
        /// Признак пояснения записи
        /// </summary>
        public ExplainTksInvoiceState Status { get; set; }

        /// <summary>
        /// Раздел, в котором отражена запись о счете-фактуре
        /// </summary>
        public int Chapter { get; set; }

        /// <summary>
        /// Битовое представление списка КВО
        /// </summary>
        public ulong? NewOperationCodesBit { get; set; }

        /// <summary>
        /// Номер счета-фактуры продавца
        /// </summary>
        public string NewInvoiceNumber { get; set; }

        /// <summary>
        /// Дата счета-фактуры продавца
        /// </summary>
        public DateTime? NewInvoiceDate { get; set; }

        /// <summary>
        /// Номер исправления счета-фактуры продавца
        /// </summary>
        public string NewChangeNumber { get; set; }

        /// <summary>
        /// Дата исправления счета-фактуры продавца
        /// </summary>
        public DateTime? NewChangeDate { get; set; }

        /// <summary>
        /// Номер корректировочного счета-фактуры продавца
        /// </summary>
        public string NewCorrectionNumber { get; set; }

        /// <summary>
        /// Дата корректировочного счета-фактуры продавца
        /// </summary>
        public DateTime? NewCorrectionDate { get; set; }

        /// <summary>
        /// Номер исправления корректировочного счета-фактуры продавца
        /// </summary>
        public string NewChangeCorrectionNumber { get; set; }

        /// <summary>
        /// Дата исправления корректировочного счета-фактуры продавца
        /// </summary>
        public DateTime? NewChangeCorrectionDate { get; set; }

        /// <summary>
        /// Код валюты по ОКВ
        /// </summary>
        public string NewOkvCode { get; set; }

        /// <summary>
        /// Сведения о посреднической деятельности продавца: № СФ
        /// </summary>
        public string NewAgencyInvoiceNumber { get; set; }

        /// <summary>
        /// Сведения о посреднической деятельности продавца: дата СФ
        /// </summary>
        public DateTime? NewAgencyInvoiceDate { get; set; }

        /// <summary>
        /// Код вида сделки
        /// </summary>
        public string NewDealCode { get; set; }

        /// <summary>
        /// ИНН посредника
        /// </summary>
        public string NewBrokerInn { get; set; }

        /// <summary>
        /// КПП посредника
        /// </summary>
        public string NewBrokerKpp { get; set; }

        /// <summary>
        /// ИНН покупателя/продавца
        /// </summary>
        public string NewContractorInn { get; set; }

        /// <summary>
        /// КПП покупателя/продавца
        /// </summary>
        public string NewContractorKpp { get; set; }

        /// <summary>
        /// Стоимость 1
        /// </summary>
        public decimal? NewAmount { get; set; }

        /// <summary>
        /// Стоимость 2
        /// </summary>
        public decimal? NewAmount2 { get; set; }

        public InvoiceReceiveDate[] ReceiveDates { get; set; }

        public InvoiceReceiptDocument[] ReceiptDocuments { get; set; }

        public InvoiceContractor[] Contractors { get; set; }
    }
}
