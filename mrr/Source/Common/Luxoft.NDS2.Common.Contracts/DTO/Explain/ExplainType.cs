﻿namespace Luxoft.NDS2.Common.Contracts.DTO.Explain
{
    public enum ExplainType
    {
        Invoice = 1,
        ControlRatio,
        OtherReason
    }
}
