﻿using System.ComponentModel;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Explain
{
    [Serializable]
    public class ExplainTksInvoiceConfirmed
    {
        public long ExplainZip { get; set; }

        [DisplayName("Признак записи")]
        [Description("Признак пояснения записи")]
        public ExplainTksInvoiceState Status { get; set; }

        [DisplayName("Раздел")]
        [Description("Раздел, в котором отражена запись о счете-фактуре")]
        public AskInvoiceChapterNumber Chapter { get; set; }
        
        public ulong? OperationCodesBit { get; set; }

        public ulong? NewOperationCodesBit { get; set; } 

        [DisplayName("Номер СФ")]
        [Description("Номер счета-фактуры продав")]
        public string InvoiceNumber { get; set; }

        public string NewInvoiceNumber { get; set; }

        [DisplayName("Дата СФ")]
        [Description("Дата счета-фактуры продав")]
        public DateTime? InvoiceDate { get; set; }

        public DateTime? NewInvoiceDate { get; set; }

        [DisplayName("ИНН")]
        [Description("ИНН покупателя")]
        public string BuyerInn { get; set; }

        public string NewBuyerInn { get; set; }

        [DisplayName("КПП")]
        [Description("КПП покупателя")]
        public string BuyerKpp { get; set; }

        public string NewBuyerKpp { get; set; }

        [DisplayName("ИНН")]
        [Description("ИНН посредника")]
        public string BrokerInn { get; set; }

        public string NewBrokerInn { get; set; }

        [DisplayName("КПП")]
        [Description("КПП посредника")]
        public string BrokerKpp { get; set; }

        public string NewBrokerKpp { get; set; }

        [DisplayName("в валюте")]
        [Description("Стоимость продаж по счету-фактуре, разница стоимости по корректировочному счету-фактуре (включая налог), в валюте счета-фактуры")]
        public decimal? Amount { get; set; }

        public decimal? NewAmount { get; set; }

        [DisplayName("в руб. и коп.")]
        [Description("Стоимость продаж по счету-фактуре, разница стоимости по корректировочному счету-фактуре (включая налог) в рублях и копейках")]
        public decimal? AmountRur { get; set; }

        public decimal? NewAmountRur { get; set; }
    }
}
