﻿using System;
using Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply;

namespace Luxoft.NDS2.Common.Contracts.DTO.Explain
{
    /// <summary>
    /// Этот класс описывает объект данных о пояснении / ответе НП на требование или истребование
    /// </summary>
    [Serializable]
    public class ExplainDetailsData
    {
        /// <summary>
        /// Возвращает или задает идентификатор пояснения в МРР
        /// </summary>
        public long Id
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает идентификатор пояснения в МС (ЗИП)
        /// </summary>
        public long Zip
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает номер пояснения в СЭОД
        /// </summary>
        public string Number
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает дату пояснения в СЭОД
        /// </summary>
        public DateTime Date
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает статус пояснения
        /// </summary>
        public string Status
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает идентификатор требования или инстребования
        /// </summary>
        public long ClaimId
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает номер требования или истребования
        /// </summary>
        public string ClaimNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает дату требования или истребования
        /// </summary>
        public DateTime? ClaimDate
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает комментарий инспектора к пояснению
        /// </summary>
        public string Comment
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает текст ответа НП к пояснению по иным основаниям
        /// </summary>
        public string Reply
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает зип декларации
        /// </summary>
        public long DeclarationZip
        {
            get;
            set;
        }
    }
}
