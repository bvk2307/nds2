﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using System;

namespace Luxoft.NDS2.Common.Contracts.DTO.Explain
{
    [Serializable]
    public class ExplainTksInvoiceChapter12 : IExplainTksInvoiceBase
    {
        public long ExplainZip { get; set; }

        public string RowKey { get; set; }

        public AskInvoiceChapterNumber Chapter { get; set; }

        public ExplainTksInvoiceState Status { get; set; }

        public string InvoiceNumber { get; set; }

        public string NewInvoiceNumber { get; set; }

        public DateTime? InvoiceDate { get; set; }

        public DateTime? NewInvoiceDate { get; set; }

        public string OkvCode { get; set; }

        public string NewOkvCode { get; set; }

        public string BuyerInn { get; set; }

        public string NewBuyerInn { get; set; }

        public string BuyerKpp { get; set; }

        public string NewBuyerKpp { get; set; }

        public decimal? Amount { get; set; }

        public decimal? NewAmount { get; set; }

        public decimal? AmountNoNds { get; set; }

        public decimal? NewAmountNoNds { get; set; }
    }
}
