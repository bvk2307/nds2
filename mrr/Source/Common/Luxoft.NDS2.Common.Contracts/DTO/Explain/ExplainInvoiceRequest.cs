﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;

namespace Luxoft.NDS2.Common.Contracts.DTO.Explain
{

    public enum ExplainInvoiceRequestState
    {
        Created = 1,
        InProcess,
        Completed,
        Error
    };

    public class ExplainInvoiceRequest
    {
        public long Id { get; set; }

        public long ExplainId { get; set; }

        public DeclarationInvoiceChapterNumber Chapter { get; set; }

        public ExplainInvoiceRequestState Status { get; set; }

        public int InvoiceQuantity { get; set; }
    }
}
