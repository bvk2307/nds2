﻿
namespace Luxoft.NDS2.Common.Contracts.AccessRestriction
{
    public static class MrrOperations
    {
        public static class UserTask
        {
            public const string UserTaskReportByCountry = "ПЗ.Отчет.Страна";

            public const string UserTaskReportByRegion = "ПЗ.Отчет.Регион";

            public const string UserTaskReportByInspections = "ПЗ.Отчет.Инспекция";

            public const string UserTaskReportByInspector = "ПЗ.Отчет.Инспектор";

            public const string UserTaskReportByRegionInspection = "ПЗ.Отчет.Инспекции_региона";
        }

        public static class ActAndDecision
        {
            public const string ActDecisionReportByCountry = "Акты.Отчет по стране";

            public const string ActDecisionReportByFederalDistrict = "Акты.Отчет по федеральному округу";

            public const string ActDecisionReportByRegion = "Акты.Отчет по региону";

            public const string ActDecisionReportBySono = "Акты.Отчет по инспекции";

            public const string ActDiscrepanciesEdit = "Акты.Расхождения.Изменить";

            public const string ActDiscrepanciesView = "Акты.Расхождения.Просмотр";

            public const string DecisionDiscrepanciesEdit = "Решения.Расхождения.Изменить";

            public const string DecisionDiscrepanciesView = "Решения.Расхождения.Просмотр";
        }

        public static class Macroreports
        {
            public const string ReportBackgroundMetricsCountry = "Макроотчеты.Фоновые показатели.Страна";

            public const string ReportBackgroundMetricsFedRegions = "Макроотчеты.Фоновые показатели.Федеральные округа";

            public const string ReportBackgroundMetricsRegions = "Макроотчеты.Фоновые показатели.Регионы";

            public const string ReportBackgroundMetricsInspections = "Макроотчеты.Фоновые показатели.Инспекции";
        }

        public static class ExplainReply
        {
            public const string ExplainEdit = @"Пояснение_Ответ.Редактирование";

            public const string ExplainView = @"Пояснение_Ответ.Просмотр";
        }

        public static class Declaration
        {
            public const string DeclarationAssignment = "НД.Назначить";

            public const string DeclarationView = "НД.Открыть";

            public const string DeclarationContractor = "НД.Контрагент";

            public const string DeclarationReport = "НД.Отчёт";

            public const string CommonInfo = "НД.Общие сведения";

            public const string AllDiscrepancies = "Расхождения.Все";

            public const string SignificantChagesReset = "НД.Сброс ПЗИ";

            public const string ClaimReclaimView = "НД.КНП Документы.Открыть АТ_АИ";
        }

        public static class Selection
        {
            public const string CreateTemplate = "Выборки.Создать шаблон";

            public const string Create = "Выборки.Создать выборку";

            public const string EditTemplate = "Выборки.Изменить шаблон";

            public const string Edit = "Выборки.Изменить выборку";

            public const string EditTemplateBasedSelection = "Выборки.Изменить шаблонную выборку";

            public const string DeleteTemplate = "Выборки.Удалить шаблон";

            public const string DeleteSelectionDraft = "Выборки.Удалить выборку.Черновик";

            public const string DeleteSelectionApproved = "Выборки.Удалить выборку.Согласована";

            public const string CopyTemplate = "Выборки.Копировать шаблон";

            public const string ViewTemplate = "Выборки.Просмотреть шаблон";

            public const string ViewSelection = "Выборки.Просмотреть выборку";

            public const string SelectionToRework = "Выборки.На доработку";

            public const string SelectionToApprove = "Выборки.На согласование";

            public const string SelectionApprove = "Выборки.Согласовать";

            public const string SelectionCreateClaim = "Выборки.Сформировать АТ";

            public const string SelectionIncludeClaim = "Выборки.Включить АТ";

            public const string SelectionSendClaim = "Выборки.Отправить АТ";

        }

        public static class Claim
        {
            public const string ClaimView = "Список_АТ.Открыть АТ";
        }

        public const string TaxPayerDiscrepanciesAll = "НП.Кол-во расхождений.Все";

        public const string TaxPayerDiscrepanciesKnp = "НП.Кол-во расхождений.КНП";

        public const string BankAccountRequestCreate = "Налогоплательщик.Запросы_в_банк";

        public const string InspectorWorkPlace = "ООР";

        public const string VersionChages = "Изменения в версии";

        public const string DeclarationList = "Декларации";

        public const string SelectionList = "Список выборок";

        public const string InvoiceClaimList = "Список АТ";

        public const string ReportNdsAndDiscrepancies = "Макроотчёты.НДС и расхождения";

        public const string ReportEffectiveness = "Макроотчёты.Эффективность";

        public const string AskParams = "Параметры АСК";
        
        public const string ListSettings = "Настройка списков";

        public const string Calendar = "Календарь";

        public const string Dictionaries = "Справочники";
    }
}
