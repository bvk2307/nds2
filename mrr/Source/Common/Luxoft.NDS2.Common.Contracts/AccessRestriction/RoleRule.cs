﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Common.Contracts.AccessRestriction
{
    public class RoleRule<T> : Rule<T>
    {
        private readonly string _role;

        public RoleRule(string role)
        {
            _role = role;
        }

        public override bool IsAvailable(IUser user, T model)
        {
            return user.IsInRole(_role);
        }
    }
}
