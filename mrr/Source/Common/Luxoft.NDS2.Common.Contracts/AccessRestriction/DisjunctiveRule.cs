﻿using System.Linq;

namespace Luxoft.NDS2.Common.Contracts.AccessRestriction
{
    public class DisjunctiveRule<TModel> : Rule<TModel>
    {
        private readonly Rule<TModel>[] _rules;

        public DisjunctiveRule(Rule<TModel>[] rules)
        {
            _rules = rules;
        }

        public override bool IsAvailable(IUser user, TModel model)
        {
            return _rules.Any(rule => rule.IsAvailable(user, model));
        }
    }
}
