﻿namespace Luxoft.NDS2.Common.Contracts.AccessRestriction
{
    public class DenyRule<TModel> : Rule<TModel>
    {
        public override bool IsAvailable(IUser user, TModel model)
        {
            return false;
        }
    }
}
