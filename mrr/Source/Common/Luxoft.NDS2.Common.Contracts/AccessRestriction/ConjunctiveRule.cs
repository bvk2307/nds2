﻿using System.Linq;

namespace Luxoft.NDS2.Common.Contracts.AccessRestriction
{
    public class ConjunctiveRule<TModel> : Rule<TModel>
    {
        private readonly Rule<TModel>[] _rules;

        public ConjunctiveRule(Rule<TModel>[] rules)
        {
            _rules = rules;
        }

        public override bool IsAvailable(IUser user, TModel model)
        {
            return _rules.All(rule => rule.IsAvailable(user, model));
        }
    }
}
