﻿namespace Luxoft.NDS2.Common.Contracts.AccessRestriction
{
    public interface IRuleFactory<TModel>
    {
        Rule<TModel> Rule(string operation);
    }
}
