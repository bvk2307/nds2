﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using System.Collections.Generic;

namespace Luxoft.NDS2.Common.Contracts.AccessRestriction.Selections
{
    public static class OperationRuleHelper
    {
        private static Dictionary<string, Rule<SelectionDetails>> Rules()
        {
            return new Dictionary<string, Rule<SelectionDetails>>
            {
                { Constants.SystemPermissions.Operations.SelectionChange, new AllowEditRule() },
                { Constants.SystemPermissions.Operations.SelectionTemplateChange, new AllowEditRule() },
                { Constants.SystemPermissions.Operations.SelectionFilterApply, new FilterOperationRule(Constants.SystemPermissions.Operations.SelectionFilterApply) },
                { Constants.SystemPermissions.Operations.SelectionFilterFavorites, new FavoritesOperationRule(Constants.SystemPermissions.Operations.SelectionFilterFavorites) },
                { Constants.SystemPermissions.Operations.SelectionFilterFavoritesAdd, new FavoritesOperationRule(Constants.SystemPermissions.Operations.SelectionFilterFavoritesAdd) },
                { Constants.SystemPermissions.Operations.SelectionFilterReset, new FilterOperationRule(Constants.SystemPermissions.Operations.SelectionFilterReset) },
                { Constants.SystemPermissions.Operations.SelectionFilterReturnParams, new FavoritesOperationRule( Constants.SystemPermissions.Operations.SelectionFilterReturnParams) },
                { Constants.SystemPermissions.Operations.SelectionRemove, new RemoveRule() },
                { Constants.SystemPermissions.Operations.SelectionTemplateRemove, new TemplateRemoveRule() },
                { Constants.SystemPermissions.Operations.SelectionToApprove, new ToApproveRule() },
                { Constants.SystemPermissions.Operations.SelectionToCorrect, new ToCorrectionRule() },
                { Constants.SystemPermissions.Operations.SelectionApproved, new ApproveRule() },
                { Constants.SystemPermissions.Operations.SelectionUpdate, new RefreshRule() },
                { Constants.SystemPermissions.Operations.SelectionView, new FormalRule<SelectionDetails>() },
                { Constants.SystemPermissions.Operations.SelectionTemplateView, new FormalRule<SelectionDetails>()},
                { Constants.SystemPermissions.Operations.SelectionTakeInWork, new TakeInWorkRule()}
            };
        }

        public static Rule<SelectionDetails> AccessRule(this SelectionOperation operation)
        {
            var rules = Rules();

            return rules.ContainsKey(operation.Id)
                ? rules[operation.Id] 
                : new DenyRule<SelectionDetails>();
        }
    }
}
