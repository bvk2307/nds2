﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;

namespace Luxoft.NDS2.Common.Contracts.AccessRestriction.Selections
{
    public class FilterOperationRule : ConjunctiveRule<SelectionDetails>
    {
        public FilterOperationRule(string operation)
            : base(
                new Rule<SelectionDetails>[]
                {
                    new AllowEditRule(),
                    new DisjunctiveRule<SelectionDetails>(
                        new Rule<SelectionDetails>[]{
                            new ManualSelectionRule(),
                            new TemplateSelectionRule()}), 
                    new OperationRule(operation)
                })
        {
        }
    }
}
