﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;

namespace Luxoft.NDS2.Common.Contracts.AccessRestriction.Selections
{
    public class RefreshRule : ConjunctiveRule<SelectionDetails>
    {
        public RefreshRule()
            : base(
                new Rule<SelectionDetails>[]
                {
                    new OperationRule(Constants.SystemPermissions.Operations.SelectionView),
                    new NegateRule<SelectionDetails>(new NewSelectionRule())
                })
        {
        }
    }
}
