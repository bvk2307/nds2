﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;

namespace Luxoft.NDS2.Common.Contracts.AccessRestriction.Selections
{
    public class AllowEditRule : ConjunctiveRule<SelectionDetails>
    {
        /// <summary>
        /// 1. Выборка не заблокирована
        /// 2. Выборка - Черновик или На дорабоке
        /// 3. Пользователю разрешена операция создания выборки и выборка новая либо выборка не новая и пользователю разрешена операция редактирования выборки (и он аналитик для этой выборки)
        /// </summary>
        public AllowEditRule()
            : base(
                new Rule<SelectionDetails>[]
                {
                    new CheckLockRule(),
                    new DisjunctiveRule<SelectionDetails>(
                        new Rule<SelectionDetails>[] 
                        {
                            new StatusRule(SelectionStatus.Draft), 
                            new StatusRule(SelectionStatus.LoadingError),
                            new StatusRule(null)
                        }),
                    new DisjunctiveRule<SelectionDetails>(
                        new Rule<SelectionDetails>[]
                        {
                            new ConjunctiveRule<SelectionDetails>(
                                new Rule<SelectionDetails>[]
                                {
                                    new NewSelectionRule(),
                                    new OperationRule(Constants.SystemPermissions.Operations.SelectionCreate)
                                }),
                            new ConjunctiveRule<SelectionDetails>(
                                new Rule<SelectionDetails>[]
                                {
                                    new NewSelectionRule(),
                                    new OperationRule(Constants.SystemPermissions.Operations.SelectionTemplateCreate)
                                }),
                            new ConjunctiveRule<SelectionDetails>(
                                new Rule<SelectionDetails>[]
                                {
                                    new NegateRule<SelectionDetails>(new NewSelectionRule()),
                                    new ManualSelectionRule(), 
                                    new OperationRule(Constants.SystemPermissions.Operations.SelectionChange),
                                    new AnalystOwnedRule()
                                }),
                            new ConjunctiveRule<SelectionDetails>(
                                new Rule<SelectionDetails>[]
                                {
                                    new NegateRule<SelectionDetails>(new NewSelectionRule()),
                                    new TemplateSelectionRule(), 
                                    new OperationRule(Constants.SystemPermissions.Operations.SelectionTemplateChange),
                                    new AnalystOwnedRule()
                                }),
                           new ConjunctiveRule<SelectionDetails>(
                                new Rule<SelectionDetails>[]
                                {
                                    new NegateRule<SelectionDetails>(new NewSelectionRule()),
                                    new SelectionByTemplateSelectionRule(), 
                                    new OperationRule(Constants.SystemPermissions.Operations.SelectionChange),
                                    new DisjunctiveRule<SelectionDetails>(
                                        new Rule<SelectionDetails>[]
                                        {
                                            new AnalystOwnedRule(),
                                            new AnalystNotSetRule()
                                        })
                                })
                        })
                })
        {
        }
    }
}
