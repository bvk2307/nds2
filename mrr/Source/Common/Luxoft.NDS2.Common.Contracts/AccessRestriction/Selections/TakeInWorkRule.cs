﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;

namespace Luxoft.NDS2.Common.Contracts.AccessRestriction.Selections
{
    public class TakeInWorkRule : ConjunctiveRule<SelectionDetails>
    {
        public TakeInWorkRule()
            : base(
                new Rule<SelectionDetails>[]
                {
                    new OperationRule(Constants.SystemPermissions.Operations.SelectionTakeInWork), 
                    new AnalystNotSetRule(), 
                    new SelectionByTemplateSelectionRule()
                })
        {
        }
    }
}
