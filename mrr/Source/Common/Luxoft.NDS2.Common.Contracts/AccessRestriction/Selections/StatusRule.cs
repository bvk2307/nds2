﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;

namespace Luxoft.NDS2.Common.Contracts.AccessRestriction.Selections
{
    public class StatusRule : Rule<SelectionDetails>
    {
        private readonly SelectionStatus? _status;

        public StatusRule(SelectionStatus? status)
        {
            _status = status;
        }

        public override bool IsAvailable(IUser user, SelectionDetails model)
        {
            return _status == model.Data.Status;
        }
    }
}
