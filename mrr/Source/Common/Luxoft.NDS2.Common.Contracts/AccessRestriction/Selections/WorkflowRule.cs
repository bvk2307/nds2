﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;

namespace Luxoft.NDS2.Common.Contracts.AccessRestriction.Selections
{
    public class WorkflowRule : ConjunctiveRule<SelectionDetails>
    {
        public WorkflowRule(string operation, SelectionStatus?[] statuses)
            : base(
                new Rule<SelectionDetails>[]
                {
                    new OperationRule(operation),
                    new DisjunctiveRule<SelectionDetails>(
                            statuses.Select(status => new StatusRule(status)).ToArray())
                })
        {
        }

        public WorkflowRule(string operation, SelectionStatus status)
            : this(operation, new SelectionStatus?[] { status })
        {
        }
    }

    public class RemoveRule : DisjunctiveRule<SelectionDetails>
    {
        public RemoveRule()
            : base(
                new[]
                {
                    new ConjunctiveRule<SelectionDetails>(
                        new Rule<SelectionDetails>[]
                        {
                            new AnalystOwnedRule(), 
                            new ManualSelectionRule(),
                            new WorkflowRule(
                                MrrOperations.Selection.DeleteSelectionDraft,
                                new SelectionStatus?[] {SelectionStatus.LoadingError, SelectionStatus.Draft})
                        }),
                    new ConjunctiveRule<SelectionDetails>(
                        new Rule<SelectionDetails>[]
                        {
                            new ManualSelectionRule(),
                            new WorkflowRule(
                                MrrOperations.Selection.DeleteSelectionApproved,
                                new SelectionStatus?[] {SelectionStatus.RequestForApproval, SelectionStatus.Approved})
                        })
                })
        {
        }
    }

    public class TemplateRemoveRule : DisjunctiveRule<SelectionDetails>
    {
        public TemplateRemoveRule()
            : base(
                new Rule<SelectionDetails>[]
                {
                    new ConjunctiveRule<SelectionDetails>(
                        new Rule<SelectionDetails>[]
                        {
                            new AnalystOwnedRule(),
                            new TemplateSelectionRule(),
                            new OperationRule(Constants.SystemPermissions.Operations.SelectionTemplateRemove)
                        })
                })
        {
        }
    }

    public class ToApproveWorkflowRule : WorkflowRule
    {
        public ToApproveWorkflowRule()
            : base(
                Constants.SystemPermissions.Operations.SelectionToApprove,
                new SelectionStatus?[]
                {
                    SelectionStatus.Draft
                })
        {
        }
    }

    public class ToCorrectionRule : WorkflowRule
    {
        public ToCorrectionRule()
            : base(
                Constants.SystemPermissions.Operations.SelectionToCorrect,
            new SelectionStatus?[]{
                SelectionStatus.RequestForApproval,
                null})
        {
        }
    }

    public class ApproveRule : WorkflowRule
    {
        public ApproveRule()
            : base(
                Constants.SystemPermissions.Operations.SelectionApproved,
                new SelectionStatus?[]{
                SelectionStatus.RequestForApproval,
                null})
        {
        }
    }
}
