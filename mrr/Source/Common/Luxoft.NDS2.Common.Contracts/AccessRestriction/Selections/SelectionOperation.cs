﻿using System;

namespace Luxoft.NDS2.Common.Contracts.AccessRestriction.Selections
{
    [Serializable]
    public class SelectionOperation
    {
        public string Id
        {
            get;
            set;
        }

        public bool IsAvailable
        {
            get;
            set;
        }
    }
}
