﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;

namespace Luxoft.NDS2.Common.Contracts.AccessRestriction.Selections
{
    public class ToApproveRule : ConjunctiveRule<SelectionDetails>
    {
        public ToApproveRule()
            : base(
                new Rule<SelectionDetails>[]
                {
                    new ToApproveWorkflowRule(),
                    new AnalystOwnedRule()
                })
        {
        }
    }
}
