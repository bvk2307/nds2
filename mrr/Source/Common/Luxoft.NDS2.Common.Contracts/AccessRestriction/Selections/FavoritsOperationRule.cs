﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;

namespace Luxoft.NDS2.Common.Contracts.AccessRestriction.Selections
{
    public class FavoritesOperationRule : ConjunctiveRule<SelectionDetails>
    {
        public FavoritesOperationRule(string operation)
            : base(
                new Rule<SelectionDetails>[]
                {
                    new AllowEditRule(),
                    new ManualSelectionRule(),
                    new OperationRule(operation)
                })
        {
        }
    }
}
