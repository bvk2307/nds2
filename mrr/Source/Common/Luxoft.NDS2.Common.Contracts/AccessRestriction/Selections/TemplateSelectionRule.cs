﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;

namespace Luxoft.NDS2.Common.Contracts.AccessRestriction.Selections
{
    public class TemplateSelectionRule : Rule<SelectionDetails>
    {
        public override bool IsAvailable(IUser user, SelectionDetails model)
        {
            return model.Data.TypeCode == SelectionType.Template;
        }
    }
}
