﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;

namespace Luxoft.NDS2.Common.Contracts.AccessRestriction.Selections
{
    public class OperationRule : Rule<SelectionDetails>
    {
        private readonly string _operation;

        public OperationRule(string operation)
        {
            _operation = operation;
        }

        public override bool IsAvailable(IUser user, SelectionDetails model)
        {
            return user.HasOperation(_operation);
        }
    }
}
