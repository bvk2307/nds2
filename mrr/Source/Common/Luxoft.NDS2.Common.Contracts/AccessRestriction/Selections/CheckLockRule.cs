﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using System.Linq;

namespace Luxoft.NDS2.Common.Contracts.AccessRestriction.Selections
{
    public class CheckLockRule : Rule<SelectionDetails>
    {
        public override bool IsAvailable(IUser user, SelectionDetails model)
        {
            return !model.Lockers.Any();
        }
    }
}
