﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;

namespace Luxoft.NDS2.Common.Contracts.AccessRestriction.Selections
{
    public class NewSelectionRule : Rule<SelectionDetails>
    {
        public override bool IsAvailable(IUser user, SelectionDetails model)
        {
            return model.Data.IsNew();
        }
    }
}
