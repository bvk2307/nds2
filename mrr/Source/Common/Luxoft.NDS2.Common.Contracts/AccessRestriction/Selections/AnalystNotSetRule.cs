﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;

namespace Luxoft.NDS2.Common.Contracts.AccessRestriction.Selections
{
    public class AnalystNotSetRule : Rule<SelectionDetails>
    {
        public override bool IsAvailable(IUser user, SelectionDetails model)
        {
            return string.IsNullOrEmpty(model.Data.AnalyticSid);
        }
    }
}
