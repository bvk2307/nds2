﻿namespace Luxoft.NDS2.Common.Contracts.AccessRestriction
{
    public class NegateRule<T> : Rule<T>
    {
        private readonly Rule<T> _rule;

        public NegateRule(Rule<T> rule)
        {
            _rule = rule;
        }

        public override bool IsAvailable(IUser user, T model)
        {
            return !_rule.IsAvailable(user, model);
        }
    }
}
