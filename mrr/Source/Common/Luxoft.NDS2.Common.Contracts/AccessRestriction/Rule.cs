﻿namespace Luxoft.NDS2.Common.Contracts.AccessRestriction
{
    public abstract class Rule<TModel>
    {
        public abstract bool IsAvailable(IUser user, TModel model);
    }
}
