﻿namespace Luxoft.NDS2.Common.Contracts.AccessRestriction
{
    public interface IUser
    {
        string Sid
        {
            get;
        }

        bool IsInRole(string role);

        bool HasOperation(string operation);
    }
}
