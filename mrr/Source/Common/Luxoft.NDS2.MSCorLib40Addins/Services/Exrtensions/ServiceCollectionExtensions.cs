﻿using FLS.Common.Lib.Interfaces;
using Microsoft.Practices.CompositeUI.Collections;

namespace FLS.CommonComponents.CorLibAddins.Services.Exrtensions
{
    /// <summary> Extension methods for <see cref="ServiceCollection"/>. </summary>
    public static class ServiceCollectionExtensions
    {
        public static IServiceProviderEx ToServiceProviderEx( this ServiceCollection serviceCollection )
        {
            return new ServiceProviderWrapper( serviceCollection );
        }

        private class ServiceProviderWrapper : IServiceProviderEx
        {
            private readonly ServiceCollection _serviceCollection;

            public ServiceProviderWrapper( ServiceCollection serviceCollection )
            {
                _serviceCollection = serviceCollection;
            }

            #region Implementation of IServiceProviderEx

            public TService Get<TService>()
            {
                return _serviceCollection.Get<TService>();
            }

            public TService Get<TService>( bool ensureExists )
            {
                return _serviceCollection.Get<TService>( ensureExists );
            }

            #endregion
        }
    }
}