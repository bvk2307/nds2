#define NET40
#if NET40

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;

namespace System.Threading.Tasks
{
    public static class TaskEx
    {
        private static Lazy<Task> lztaskCompleted = new Lazy<Task>( () => FromResult(false), isThreadSafe: true );

        public static Task CompletedTask
        {
            get { return lztaskCompleted.Value; }

        }

        /// <summary>
        /// Creates a task that runs the specified action.
        /// </summary>
        /// <param name="action">The action to execute asynchronously.</param>
        /// <returns>
        /// A task that represents the completion of the action.
        /// </returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="action"/> argument is null.</exception>
        public static Task Run( Action action )
        {
            return TaskEx.Run( action, CancellationToken.None );
        }

        /// <summary>
        /// Creates a task that runs the specified action.
        /// </summary>
        /// <param name="action">The action to execute.</param><param name="cancellationToken">The CancellationToken to use to request cancellation of this task.</param>
        /// <returns>
        /// A task that represents the completion of the action.
        /// </returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="action"/> argument is null.</exception>
        public static Task Run( Action action, CancellationToken cancellationToken )
        {
            return Task.Factory.StartNew( action, cancellationToken, TaskCreationOptions.None, TaskScheduler.Default );
        }

        /// <summary>
        /// Creates a task that runs the specified function.
        /// </summary>
        /// <param name="function">The function to execute asynchronously.</param>
        /// <returns>
        /// A task that represents the completion of the action.
        /// </returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="function"/> argument is null.</exception>
        public static Task<TResult> Run<TResult>( Func<TResult> function )
        {
            return TaskEx.Run<TResult>( function, CancellationToken.None );
        }

        /// <summary>
        /// Creates a task that runs the specified function.
        /// </summary>
        /// <param name="function">The action to execute.</param><param name="cancellationToken">The CancellationToken to use to cancel the task.</param>
        /// <returns>
        /// A task that represents the completion of the action.
        /// </returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="function"/> argument is null.</exception>
        public static Task<TResult> Run<TResult>( Func<TResult> function, CancellationToken cancellationToken )
        {
            return Task.Factory.StartNew<TResult>( function, cancellationToken, TaskCreationOptions.None, TaskScheduler.Default );
        }

        /// <summary>
        /// Creates a task that runs the specified function.
        /// </summary>
        /// <param name="function">The action to execute asynchronously.</param>
        /// <returns>
        /// A task that represents the completion of the action.
        /// </returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="function"/> argument is null.</exception>
        public static Task Run( Func<Task> function )
        {
            return TaskEx.Run( function, CancellationToken.None );
        }

        /// <summary>
        /// Creates a task that runs the specified function.
        /// </summary>
        /// <param name="function">The function to execute.</param><param name="cancellationToken">The CancellationToken to use to request cancellation of this task.</param>
        /// <returns>
        /// A task that represents the completion of the function.
        /// </returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="function"/> argument is null.</exception>
        public static Task Run( Func<Task> function, CancellationToken cancellationToken )
        {
            return TaskExtensions.Unwrap( TaskEx.Run<Task>( function, cancellationToken ) );
        }

        /// <summary> Creates a task that runs the specified function. ATTENTION! It uses 'UnwrapPromise{TResult}(outerTask, lookForOce: false)' when 
        /// Task.Run{TResult}( Func{Task{TResult}}, CancellationToken ) in .NET 4.5+ uses 'UnwrapPromise{TResult}(outerTask, lookForOce: true)'!
        /// </summary>
        /// <param name="function">The function to execute asynchronously.</param>
        /// <returns>
        /// A task that represents the completion of the action.
        /// </returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="function"/> argument is null.</exception>
        public static Task<TResult> Run<TResult>( Func<Task<TResult>> function )
        {
            return TaskEx.Run<TResult>( function, CancellationToken.None );
        }

        /// <summary> Creates a task that runs the specified function. ATTENTION! It uses 'UnwrapPromise{TResult}(outerTask, lookForOce: false)' when 
        /// Task.Run{TResult}( Func{Task{TResult}}, CancellationToken ) in .NET 4.5+ uses 'UnwrapPromise{TResult}(outerTask, lookForOce: true)'!
        /// </summary>
        /// <param name="function">The action to execute.</param><param name="cancellationToken">The CancellationToken to use to cancel the task.</param>
        /// <returns>
        /// A task that represents the completion of the action.
        /// </returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="function"/> argument is null.</exception>
        public static Task<TResult> Run<TResult>( Func<Task<TResult>> function, CancellationToken cancellationToken )
        {
            return TaskExtensions.Unwrap<TResult>( TaskEx.Run<Task<TResult>>( function, cancellationToken ) );
        }

        /// <summary>
        /// Starts a Task that will complete after the specified due time.
        /// </summary>
        /// <param name="dueTime">The delay in milliseconds before the returned task completes.</param>
        /// <returns>
        /// The timed Task.
        /// </returns>
        /// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="dueTime"/> argument must be non-negative or -1 and less than or equal to Int32.MaxValue.
        ///             </exception>
        public static Task Delay( int dueTime )
        {
            return TaskEx.Delay( dueTime, CancellationToken.None );
        }

        /// <summary>
        /// Starts a Task that will complete after the specified due time.
        /// </summary>
        /// <param name="dueTime">The delay before the returned task completes.</param>
        /// <returns>
        /// The timed Task.
        /// </returns>
        /// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="dueTime"/> argument must be non-negative or -1 and less than or equal to Int32.MaxValue.
        ///             </exception>
        public static Task Delay( TimeSpan dueTime )
        {
            return TaskEx.Delay( dueTime, CancellationToken.None );
        }

        /// <summary>
        /// Starts a Task that will complete after the specified due time.
        /// </summary>
        /// <param name="dueTime">The delay before the returned task completes.</param><param name="cancellationToken">A CancellationToken that may be used to cancel the task before the due time occurs.</param>
        /// <returns>
        /// The timed Task.
        /// </returns>
        /// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="dueTime"/> argument must be non-negative or -1 and less than or equal to Int32.MaxValue.
        ///             </exception>
        public static Task Delay( TimeSpan dueTime, CancellationToken cancellationToken )
        {
            if ( dueTime.TotalMilliseconds < -1L || dueTime.TotalMilliseconds > (long)int.MaxValue )
                throw new ArgumentOutOfRangeException( "dueTime", "The timeout must be non-negative or -1, and it must be less than or equal to Int32.MaxValue." );
            Contract.EndContractBlock();

            return TaskEx.Delay( (int)dueTime.TotalMilliseconds, cancellationToken );
        }

        /// <summary>
        /// Starts a Task that will complete after the specified due time.
        /// </summary>
        /// <param name="dueTime">The delay in milliseconds before the returned task completes.</param><param name="cancellationToken">A CancellationToken that may be used to cancel the task before the due time occurs.</param>
        /// <returns>
        /// The timed Task.
        /// </returns>
        /// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="dueTime"/> argument must be non-negative or -1 and less than or equal to Int32.MaxValue.
        ///             </exception>
        public static Task Delay( int dueTime, CancellationToken cancellationToken )
        {
            if ( dueTime < -1 )
                throw new ArgumentOutOfRangeException( "dueTime", "The timeout must be non-negative or -1, and it must be less than or equal to Int32.MaxValue." );
            Contract.EndContractBlock();
            if ( cancellationToken.IsCancellationRequested )
                return new Task( (Action)( () => { } ), cancellationToken );
            if ( dueTime == 0 )
                return TaskEx.CompletedTask;
            TaskCompletionSource<bool> tcs = new TaskCompletionSource<bool>();
            CancellationTokenRegistration ctr = new CancellationTokenRegistration();
            Timer timer = (Timer) null;
            timer = new Timer( (TimerCallback)( state =>
             {
                 ctr.Dispose();
                 timer.Dispose();
                 tcs.TrySetResult( true );
                 TimerManager.Remove( timer );
             } ), (object)null, -1, -1 );
            TimerManager.Add( timer );
            if ( cancellationToken.CanBeCanceled )
                ctr = cancellationToken.Register( (Action)( () =>
                 {
                     timer.Dispose();
                     tcs.TrySetCanceled();
                     TimerManager.Remove( timer );
                 } ) );
            timer.Change( dueTime, -1 );
            return (Task)tcs.Task;
        }

        /// <summary>
        /// Creates a Task that will complete only when all of the provided collection of Tasks has completed.
        /// </summary>
        /// <param name="tasks">The Tasks to monitor for completion.</param>
        /// <returns>
        /// A Task that represents the completion of all of the provided tasks.
        /// </returns>
        /// 
        /// <remarks>
        /// If any of the provided Tasks faults, the returned Task will also fault, and its Exception will contain information
        ///             about all of the faulted tasks.  If no Tasks fault but one or more Tasks is canceled, the returned
        ///             Task will also be canceled.
        /// 
        /// </remarks>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="tasks"/> argument is null.</exception><exception cref="T:System.ArgumentException">The <paramref name="tasks"/> argument contains a null reference.</exception>
        public static Task WhenAll( params Task[] tasks )
        {
            return WhenAll( (IEnumerable<Task>)tasks );
        }

        /// <summary>
        /// Creates a Task that will complete only when all of the provided collection of Tasks has completed.
        /// </summary>
        /// <param name="tasks">The Tasks to monitor for completion.</param>
        /// <returns>
        /// A Task that represents the completion of all of the provided tasks.
        /// </returns>
        /// 
        /// <remarks>
        /// If any of the provided Tasks faults, the returned Task will also fault, and its Exception will contain information
        ///             about all of the faulted tasks.  If no Tasks fault but one or more Tasks is canceled, the returned
        ///             Task will also be canceled.
        /// 
        /// </remarks>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="tasks"/> argument is null.</exception><exception cref="T:System.ArgumentException">The <paramref name="tasks"/> argument contains a null reference.</exception>
        public static Task<TResult[]> WhenAll<TResult>( params Task<TResult>[] tasks )
        {
            return WhenAll<TResult>( (IEnumerable<Task<TResult>>)tasks );
        }

        /// <summary>
        /// Creates a Task that will complete only when all of the provided collection of Tasks has completed.
        /// </summary>
        /// <param name="tasks">The Tasks to monitor for completion.</param>
        /// <returns>
        /// A Task that represents the completion of all of the provided tasks.
        /// </returns>
        /// 
        /// <remarks>
        /// If any of the provided Tasks faults, the returned Task will also fault, and its Exception will contain information
        ///             about all of the faulted tasks.  If no Tasks fault but one or more Tasks is canceled, the returned
        ///             Task will also be canceled.
        /// 
        /// </remarks>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="tasks"/> argument is null.</exception><exception cref="T:System.ArgumentException">The <paramref name="tasks"/> argument contains a null reference.</exception>
        public static Task WhenAll( IEnumerable<Task> tasks )
        {
            return (Task)WhenAllCore<object>( tasks, (Action<Task[], TaskCompletionSource<object>>)( ( completedTasks, tcs ) => tcs.TrySetResult( (object)null ) ) );
        }

        /// <summary>
        /// Creates a Task that will complete only when all of the provided collection of Tasks has completed.
        /// </summary>
        /// <param name="tasks">The Tasks to monitor for completion.</param>
        /// <returns>
        /// A Task that represents the completion of all of the provided tasks.
        /// </returns>
        /// 
        /// <remarks>
        /// If any of the provided Tasks faults, the returned Task will also fault, and its Exception will contain information
        ///             about all of the faulted tasks.  If no Tasks fault but one or more Tasks is canceled, the returned
        ///             Task will also be canceled.
        /// 
        /// </remarks>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="tasks"/> argument is null.</exception><exception cref="T:System.ArgumentException">The <paramref name="tasks"/> argument contains a null reference.</exception>
        public static Task<TResult[]> WhenAll<TResult>( IEnumerable<Task<TResult>> tasks )
        {
            return WhenAllCore<TResult[]>( Enumerable.Cast<Task>( (IEnumerable)tasks ), (Action<Task[], TaskCompletionSource<TResult[]>>)( ( completedTasks, tcs ) => tcs.TrySetResult( Enumerable.ToArray<TResult>( Enumerable.Select<Task, TResult>( (IEnumerable<Task>)completedTasks, (Func<Task, TResult>)( t => ( (Task<TResult>)t ).Result ) ) ) ) ) );
        }

        /// <summary>
        /// Creates a Task that will complete only when all of the provided collection of Tasks has completed.
        /// </summary>
        /// <param name="tasks">The Tasks to monitor for completion.</param><param name="setResultAction">A callback invoked when all of the tasks complete successfully in the RanToCompletion state.
        ///             This callback is responsible for storing the results into the TaskCompletionSource.
        ///             </param>
        /// <returns>
        /// A Task that represents the completion of all of the provided tasks.
        /// </returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="tasks"/> argument is null.</exception><exception cref="T:System.ArgumentException">The <paramref name="tasks"/> argument contains a null reference.</exception>
        private static Task<TResult> WhenAllCore<TResult>( IEnumerable<Task> tasks, Action<Task[], TaskCompletionSource<TResult>> setResultAction )
        {
            if ( tasks == null )
                throw new ArgumentNullException( "tasks" );
            Contract.EndContractBlock();
            Contract.Assert( setResultAction != null, (string)null );
            TaskCompletionSource<TResult> tcs = new TaskCompletionSource<TResult>();
            Task[] taskArray = tasks as Task[] ?? Enumerable.ToArray<Task>(tasks);
            if ( taskArray.Length == 0 )
                setResultAction( taskArray, tcs );
            else
                Task.Factory.ContinueWhenAll( taskArray, (Action<Task[]>)( completedTasks =>
                 {
                     List<Exception> targetList = (List<Exception>) null;
                     bool flag = false;
                     foreach ( Task task in completedTasks )
                     {
                         if ( task.IsFaulted )
                             AddPotentiallyUnwrappedExceptions( ref targetList, (Exception)task.Exception );
                         else if ( task.IsCanceled )
                             flag = true;
                     }
                     if ( targetList != null && targetList.Count > 0 )
                         tcs.TrySetException( (IEnumerable<Exception>)targetList );
                     else if ( flag )
                         tcs.TrySetCanceled();
                     else
                         setResultAction( completedTasks, tcs );
                 } ), CancellationToken.None, TaskContinuationOptions.ExecuteSynchronously, TaskScheduler.Default );
            return tcs.Task;
        }

        /// <summary>
        /// Creates a Task that will complete when any of the tasks in the provided collection completes.
        /// </summary>
        /// <param name="tasks">The Tasks to be monitored.</param>
        /// <returns>
        /// A Task that represents the completion of any of the provided Tasks.  The completed Task is this Task's result.
        /// 
        /// </returns>
        /// 
        /// <remarks>
        /// Any Tasks that fault will need to have their exceptions observed elsewhere.
        /// </remarks>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="tasks"/> argument is null.</exception><exception cref="T:System.ArgumentException">The <paramref name="tasks"/> argument contains a null reference.</exception>
        public static Task<Task> WhenAny( params Task[] tasks )
        {
            return WhenAny( (IEnumerable<Task>)tasks );
        }

        /// <summary>
        /// Creates a Task that will complete when any of the tasks in the provided collection completes.
        /// </summary>
        /// <param name="tasks">The Tasks to be monitored.</param>
        /// <returns>
        /// A Task that represents the completion of any of the provided Tasks.  The completed Task is this Task's result.
        /// 
        /// </returns>
        /// 
        /// <remarks>
        /// Any Tasks that fault will need to have their exceptions observed elsewhere.
        /// </remarks>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="tasks"/> argument is null.</exception><exception cref="T:System.ArgumentException">The <paramref name="tasks"/> argument contains a null reference.</exception>
        public static Task<Task> WhenAny( IEnumerable<Task> tasks )
        {
            if ( tasks == null )
                throw new ArgumentNullException( "tasks" );
            Contract.EndContractBlock();
            TaskCompletionSource<Task> tcs = new TaskCompletionSource<Task>();
            Task.Factory.ContinueWhenAny<bool>( tasks as Task[] ?? Enumerable.ToArray<Task>( tasks ),
                completed => tcs.TrySetResult( completed ),
                CancellationToken.None, TaskContinuationOptions.ExecuteSynchronously, TaskScheduler.Default );
            return tcs.Task;
        }

        /// <summary>
        /// Creates a Task that will complete when any of the tasks in the provided collection completes.
        /// </summary>
        /// <param name="tasks">The Tasks to be monitored.</param>
        /// <returns>
        /// A Task that represents the completion of any of the provided Tasks.  The completed Task is this Task's result.
        /// 
        /// </returns>
        /// 
        /// <remarks>
        /// Any Tasks that fault will need to have their exceptions observed elsewhere.
        /// </remarks>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="tasks"/> argument is null.</exception><exception cref="T:System.ArgumentException">The <paramref name="tasks"/> argument contains a null reference.</exception>
        public static Task<Task<TResult>> WhenAny<TResult>( params Task<TResult>[] tasks )
        {
            return WhenAny<TResult>( (IEnumerable<Task<TResult>>)tasks );
        }

        /// <summary>
        /// Creates a Task that will complete when any of the tasks in the provided collection completes.
        /// </summary>
        /// <param name="tasks">The Tasks to be monitored.</param>
        /// <returns>
        /// A Task that represents the completion of any of the provided Tasks.  The completed Task is this Task's result.
        /// 
        /// </returns>
        /// 
        /// <remarks>
        /// Any Tasks that fault will need to have their exceptions observed elsewhere.
        /// </remarks>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="tasks"/> argument is null.</exception><exception cref="T:System.ArgumentException">The <paramref name="tasks"/> argument contains a null reference.</exception>
        public static Task<Task<TResult>> WhenAny<TResult>( IEnumerable<Task<TResult>> tasks )
        {
            if ( tasks == null )
                throw new ArgumentNullException( "tasks" );
            Contract.EndContractBlock();
            TaskCompletionSource<Task<TResult>> tcs = new TaskCompletionSource<Task<TResult>>();
            Task.Factory.ContinueWhenAny<TResult, bool>( tasks as Task<TResult>[] ?? Enumerable.ToArray<Task<TResult>>( tasks ),
                completed => tcs.TrySetResult( completed ),
                CancellationToken.None, TaskContinuationOptions.ExecuteSynchronously, TaskScheduler.Default );
            return tcs.Task;
        }

        /// <summary>
        /// Creates an already completed <see cref="T:System.Threading.Tasks.Task`1"/> from the specified result.
        /// </summary>
        /// <param name="result">The result from which to create the completed task.</param>
        /// <returns>
        /// The completed task.
        /// </returns>
        public static Task<TResult> FromResult<TResult>( TResult result )
        {
            TaskCompletionSource<TResult> completionSource = new TaskCompletionSource<TResult>((object) result);
            completionSource.TrySetResult( result );
            return completionSource.Task;
        }

        /// <summary>
        /// Adds the target exception to the list, initializing the list if it's null.
        /// </summary>
        /// <param name="targetList">The list to which to add the exception and initialize if the list is null.</param><param name="exception">The exception to add, and unwrap if it's an aggregate.</param>
        private static void AddPotentiallyUnwrappedExceptions( ref List<Exception> targetList, Exception exception )
        {
            AggregateException aggregateException = exception as AggregateException;
            Contract.Assert( exception != null, (string)null );
            Contract.Assert( aggregateException == null || aggregateException.InnerExceptions.Count > 0, (string)null );
            if ( targetList == null )
                targetList = new List<Exception>();
            if ( aggregateException != null )
                targetList.Add( aggregateException.InnerExceptions.Count == 1 ? exception.InnerException : exception );
            else
                targetList.Add( exception );
        }
    }
}
#endif