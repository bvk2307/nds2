﻿using CommonComponents.Instrumentation;
using FLS.CommonComponents.Lib.Interfaces;

namespace FLS.CommonComponents.CorLibAddins.Execution
{
    public interface IInsrumentationExecutionWrapper : IExecutionWrapper, IInstanceProvider<IInstrumentationService> { }
}