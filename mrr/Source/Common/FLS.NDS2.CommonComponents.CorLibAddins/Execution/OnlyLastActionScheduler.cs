﻿using System;

namespace FLS.CommonComponents.CorLibAddins.Execution
{
    public sealed class OnlyLastActionScheduler<TArgs> : AbstractActionScheduler<TArgs> where TArgs : class
    {
        /// <summary> </summary>
        /// <param name="actionToSchedule"> An action that can be scheduled many time. It must handle all exception inside itself because <see cref="AbstractActionScheduler{TArgs}"/> will be ignored any exception inside it. </param>
        /// <param name="executor"></param>
        /// <param name="actionEventName"></param>
        /// <param name="userFriendlyErrorMessage"></param>
        public OnlyLastActionScheduler( Action<TArgs> actionToSchedule, IInsrumentationExecutionWrapper executor, 
                                        string actionEventName, string userFriendlyErrorMessage ) 
            : base( actionToSchedule, executor, actionEventName, userFriendlyErrorMessage ) { }

        protected override TArgs DequeToExecute()
        {
            TArgs args, lastArgs = null;

            while ( ActionQueue.TryDequeue( out args ) ) { lastArgs = args; }   //gets only the last action arguments for the single call of the action

            return lastArgs;
        }
    }
}