﻿using System;
using System.Diagnostics;

namespace FLS.CommonComponents.CorLibAddins.Execution
{
    /// <summary> A simple execution call wrapper. </summary>
    public interface IExecutionWrapper
    {
        void WrapExecution<TArgs>( Action<TArgs> actionToExecute, TArgs arguments = default(TArgs), string eventName = null, 
                                   string userFriendlyErrorMessage = null, TraceEventType traceEventType = default(TraceEventType) );
    }
}