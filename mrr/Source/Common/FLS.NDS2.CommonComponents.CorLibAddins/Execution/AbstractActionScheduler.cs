﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Threading;
using System.Threading.Tasks;
using CommonComponents.Instrumentation;
using FLS.CommonComponents.CorLibAddins.Properties;

namespace FLS.CommonComponents.CorLibAddins.Execution
{
    /// <summary> An abstract scheduler of some actions. </summary>
    public abstract class AbstractActionScheduler<TArgs> where TArgs : class
    {
        private const int FlagDown = 0;
        private const int FlagUp = 1;

        private readonly Action<TArgs> _actionSchedule;
        private readonly IInsrumentationExecutionWrapper _executor;
        private readonly string _actionEventName;
        private readonly string _userFriendlyErrorMessage;

        private ConcurrentQueue<TArgs> _actionQueue = null;
        private TArgs _argsCurrent = null;
        private TaskFactory _taskFactory = null;

        private int _startExecutionCycleFlag = 0;

        /// <summary> </summary>
        /// <param name="actionToSchedule"> An action that can be scheduled many time. It must handle all exception inside itself because <see cref="AbstractActionScheduler{TArgs}"/> will be ignored any exception inside it. </param>
        /// <param name="executor"></param>
        /// <param name="actionEventName"></param>
        /// <param name="userFriendlyErrorMessage"></param>
        public AbstractActionScheduler( Action<TArgs> actionToSchedule, IInsrumentationExecutionWrapper executor, 
                                        string actionEventName, string userFriendlyErrorMessage )
        {
            Contract.Requires( actionToSchedule != null );
            Contract.Requires( executor != null );
            Contract.Requires( !string.IsNullOrEmpty( userFriendlyErrorMessage ) );
            Contract.Requires( !string.IsNullOrEmpty( actionEventName ) );

            _actionSchedule             = actionToSchedule;
            _executor                   = executor;
            _userFriendlyErrorMessage   = userFriendlyErrorMessage;
            _actionEventName            = actionEventName;
        }

        public TArgs CurrentArgs { get { return _argsCurrent; } }    //does not volatile read by performance reasons

        public void ScheduleAction( TArgs args )
        {
            Contract.Requires( args != null );
            if ( args == null )
                throw new ArgumentNullException( "args" );
            Contract.EndContractBlock();

            if ( OnActionScheduling( args ) )
            {
                Interlocked.Exchange( ref _argsCurrent, args );

                ActionQueue.Enqueue( args );

                StartExecutionCycle();
            }
        }

        private void StartExecutionCycle()
        {
            Contract.Requires( _executor != null );

            if ( TryNotEmptyAndTakeFlagExclusivelly() )
            {
                //IProfilingEntry profilingEntry = InstrumentationService.CreateProfilingEntry( TraceEventType.Error, (int)InstrumentationLevel.Normal, 
                //                                            messageType: InstrumentationMessageTypes.FLSCommonComponentsTypes, messagePriority: 100 );

	//apopov 11.10.2016	//TODO!!! //support cancellation
                ActionFactory.StartNew( cancelToken => _executor.WrapExecution( ActionsExecutionCycle, cancelToken, _actionEventName, _userFriendlyErrorMessage, TraceEventType.Error ), 
                                        CancellationToken.None, CancellationToken.None );
            }
        }

        private bool TryNotEmptyAndTakeFlagExclusivelly()
        {
            return !ActionQueue.IsEmpty && FlagUp != Interlocked.CompareExchange( ref _startExecutionCycleFlag, FlagUp, FlagDown );
        }

        /// <summary> Any excepiton caused will be handled by the wrapping <see cref="IExecutionWrapper"/> because it is executing in background pool work item. </summary>
        /// <param name="oCancelToken"></param>
        private void ActionsExecutionCycle( object oCancelToken )
        {
            try
            {
                CancellationToken cancelToken = (CancellationToken)oCancelToken;
                if ( !cancelToken.IsCancellationRequested )
                {
                    do
                    {
                        DequeAndExecuteCycle( cancelToken );

                        int prevFlagValue = Interlocked.CompareExchange( ref _startExecutionCycleFlag, FlagDown, FlagUp );

                        Contract.Assert( prevFlagValue == FlagUp, "An unexpected flag value when code has exclusive access to it" );

                    }while ( TryNotEmptyAndTakeFlagExclusivelly() && cancelToken.IsCancellationRequested );
                }
            }
            finally
            { 
                Interlocked.CompareExchange( ref _startExecutionCycleFlag, FlagDown, FlagUp );  //resets the flag only if an exception caused, for a example inside DequeAndExecuteCycle()
            }
        }

        private void DequeAndExecuteCycle( CancellationToken cancelToken )
        {
            TArgs args = null;
            while ( ( args = DequeToExecute() ) != null )
            {
                if ( !cancelToken.IsCancellationRequested )
                    _actionSchedule( args );
            }
        }

        protected abstract TArgs DequeToExecute();

        protected virtual bool OnActionScheduling( TArgs args ) { return true; }

        protected ConcurrentQueue<TArgs> ActionQueue
        {
            get { return LazyInitializer.EnsureInitialized( ref _actionQueue, () => new ConcurrentQueue<TArgs>() ); }
        }

        protected IInstrumentationService InstrumentationService
        {
            get { return _executor.Instance; }
        }

        private TaskFactory ActionFactory
        {
            get { return LazyInitializer.EnsureInitialized( ref _taskFactory, () => new TaskFactory() ); }
        }
    }
}