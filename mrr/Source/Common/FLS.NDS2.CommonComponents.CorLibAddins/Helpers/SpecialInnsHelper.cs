﻿using System;
using System.Collections.Generic;

namespace FLS.CommonComponents.CorLibAddins.Helpers
{
    public static class SpecialInnsHelper
    {
        private static Dictionary<string, Tuple<string, string>> s_specialInns =
            new Dictionary<string, Tuple<string, string>>
            {
                {"", new Tuple<string, string>("", "Images/circle.png")},
                {"0000000001", new Tuple<string, string>("Не определенный", "Images/circle.png")},
                {"0000000026", new Tuple<string, string>("Физическое лицо", "Images/person.png")}, 
                {"0000000020", new Tuple<string, string>("Импортер", "Images/import.png")}, 
                {"0000000019", new Tuple<string, string>("Импортер таможенного союза", "Images/importTC.png")}
            };

        public static bool IsSpecialInn(string inn)
        {
            return s_specialInns.ContainsKey(inn);
        }

        public static string GetSpecialInnImgPath(string inn)
        {
            Tuple<string, string> innProperties;
            if (s_specialInns.TryGetValue(inn, out innProperties))
            {
                return innProperties.Item2;
            }

            return string.Empty;
        }

        public static string GetSpecialInnName(string inn)
        {
            Tuple<string, string> innProperties;
            if (s_specialInns.TryGetValue(inn, out innProperties))
            {
                return innProperties.Item1;
            }

            return string.Empty;
        }
    }
}
