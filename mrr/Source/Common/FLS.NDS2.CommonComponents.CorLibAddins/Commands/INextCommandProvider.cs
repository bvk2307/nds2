﻿using System.Collections.Generic;
using System.Threading;

namespace FLS.CommonComponents.CorLibAddins.Commands
{
    /// <summary> A command provider. </summary>
    public interface INextCommandProvider
    {
        bool TakeOrWaitNextCommand( int waitMs, out KeyValuePair<string, CommandEventArgs> command, CancellationToken cancelToken = default(CancellationToken) );
    }
}