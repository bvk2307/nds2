﻿using FLS.Common.Lib.Events;

namespace FLS.CommonComponents.CorLibAddins.Commands
{
    public class CommandEventArgs : FreezableEventArgs
    {
        private static CommandEventArgs s_emptySingleton = null;

        static CommandEventArgs()
        {
            s_emptySingleton = (CommandEventArgs)new CommandEventArgs().Freeze();
        }

        public static new CommandEventArgs Empty { get { return s_emptySingleton; } }
    }
}