﻿using System.Diagnostics.Contracts;

namespace FLS.CommonComponents.CorLibAddins.Commands
{
    /// <summary> A command scheduler. </summary>
    [ContractClass( typeof(ICommandSchedulerContract) )]
    public interface ICommandScheduler
    {
        void ScheduleCommand( string commandNameId, CommandEventArgs eventArgs );
    }

    [ContractClassFor( typeof(ICommandScheduler) )]
    internal abstract class ICommandSchedulerContract : ICommandScheduler
    {
        public void ScheduleCommand( string commandNameId, CommandEventArgs eventArgs )
        {
            Contract.Requires( !string.IsNullOrEmpty( commandNameId ), "Parameter 'commandNameId' can not be empty" );

            throw new System.NotImplementedException();
        }
    }
}