﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;

namespace FLS.CommonComponents.CorLibAddins.Commands
{
    /// <summary> A command scheduler that store delayed commands and give their by request. </summary>
    public sealed class CommandScheduler : ICommandScheduler, INextCommandProvider
    {
        private ConcurrentQueue<KeyValuePair<string, CommandEventArgs>> _commandQueue = null;

        /// <summary> Contains ManualResetEventSlim returned by <see cref="CommandScheduledEvent"/>. </summary>
        private object _commandScheduledEvent = null;   

        /// <summary> The default constructor that can be using by IoC container. </summary>
        public CommandScheduler() { }

        private ConcurrentQueue<KeyValuePair<string, CommandEventArgs>> CommandQueue
        {
            get { return LazyInitializer.EnsureInitialized( ref _commandQueue, InitalizeAtomically ); }
        }

        private ConcurrentQueue<KeyValuePair<string, CommandEventArgs>> InitalizeAtomically()
        {
            Thread.VolatileWrite( ref _commandScheduledEvent, new ManualResetEventSlim( initialState: false ) );

            return new ConcurrentQueue<KeyValuePair<string, CommandEventArgs>>();
        }

        private ManualResetEventSlim CommandScheduledEvent
        {
            get { return (ManualResetEventSlim)_commandScheduledEvent; }
        }

        public void ScheduleCommand( string commandNameId, CommandEventArgs eventArgs )
        {
            if ( string.IsNullOrEmpty( commandNameId ) )
                throw new ArgumentOutOfRangeException( "commandNameId", "Parameter 'commandNameId' can not be empty" );

            if ( !eventArgs.IsFrozen )
                eventArgs.Freeze();

            CommandQueue.Enqueue( new KeyValuePair<string, CommandEventArgs>( commandNameId, eventArgs ) );

            SignalCommandScheduled();
        }

        public bool TakeOrWaitNextCommand( int waitMs, out KeyValuePair<string, CommandEventArgs> command, CancellationToken cancelToken = default(CancellationToken) )
        {
            bool result = CommandQueue.TryDequeue( out command );
            if ( !result )
            {
                if ( CommandScheduledEvent.Wait( waitMs, cancelToken ) )
                    result = CommandQueue.TryDequeue( out command );
            }       
            return result;
        }

        private void SignalCommandScheduled()
        {
            CommandScheduledEvent.Set();
            CommandScheduledEvent.Reset();
        }
    }
}