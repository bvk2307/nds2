﻿using FLS.Common.Lib.Data;
using FLS.CommonComponents.CorLibAddins.Commands;

namespace FLS.CommonComponents.CorLibAddins.Events
{
    public class QueryConditionsEventArgs : CommandEventArgs
    {
        private readonly QueryConditions _queryConditions;
        private readonly bool _isRowNumberRequested;


        /// <summary> </summary>
        /// <param name="queryConditions"> Query conditions. It can be euqal to 'null'. </param>
        /// <param name="isRowNumberRequested"> Is row count according filters requested? </param>
        public QueryConditionsEventArgs( QueryConditions queryConditions, bool isRowNumberRequested = false )
        {
            _queryConditions = queryConditions;
            _isRowNumberRequested = isRowNumberRequested;
        }

        /// <summary> Query conditions. It can be euqal to 'null'. </summary>
        public QueryConditions Conditions
        {
            get { return _queryConditions; }
        }

        /// <summary> Is row count according filters requested? </summary>
        public bool IsRowNumberRequested
        {
            get { return _isRowNumberRequested; }
        }
    }
}