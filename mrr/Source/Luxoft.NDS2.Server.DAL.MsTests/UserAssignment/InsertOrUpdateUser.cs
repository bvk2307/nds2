﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Luxoft.NDS2.Server.DAL.DeclarationAssignment;
using Luxoft.NDS2.Common.Contracts.DTO;
using Moq;
using System.Configuration;
using System.Collections.Generic;
using Oracle.DataAccess.Client;
using System.Data;
using Luxoft.NDS2.Server.Common.Adapters;

namespace Luxoft.NDS2.Server.DAL.MsTests.UserAssignment
{
    [TestClass]
    public class InsertOrUpdateUser
    {
        [TestMethod]
        public void InsertOrUpdateExistingUser()
        {
            var existingUser = DbHelper.CreateUser();
            var adapter = CreateUserAdapter();
            long retVal = 0;
            try
            {
                retVal = adapter.InsertOrUpdate(existingUser);
            }
            catch (Exception Ex)
            {
                Assert.Fail(Ex.Message);
            }
            Assert.AreEqual(retVal, Int64.Parse(existingUser.EmployeeNum));
        }

        [TestMethod]
        public void InsertOrUpdateNewUser()
        {
            var adapter = CreateUserAdapter();
            long retVal = 0;
            var uniqueStr = Guid.NewGuid().ToString();
            var newUser = new UserInformation() { Name = uniqueStr, Sid = uniqueStr };
            try
            {
                retVal = adapter.InsertOrUpdate(newUser);
            }
            catch (Exception Ex)
            {
                Assert.Fail(Ex.Message);
            }
            Assert.IsTrue(retVal > 0);
        }

        private IUserAdapter CreateUserAdapter()
        {
            var moq = new Mock<IServiceProvider>();

            moq.Setup(x => x.GetConfigurationValue(It.IsAny<string>()))
                .Returns(ConfigurationManager.AppSettings["NDS2_DB"]);

           return UserAdapterCreator.UserAdapter(moq.Object);
        }
    }
}
