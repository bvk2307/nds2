﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Luxoft.NDS2.Server.DAL.DeclarationAssignment;
using Luxoft.NDS2.Common.Contracts.DTO;
using Moq;
using System.Configuration;
using System.Collections.Generic;
using Oracle.DataAccess.Client;
using System.Data;
using Luxoft.NDS2.Server.Common.Adapters;
using Luxoft.NDS2.Server.Common.Adapters.Declaration;
using System.Linq;
using Oracle.DataAccess.Types;
using System.Transactions;

namespace Luxoft.NDS2.Server.DAL.MsTests.UserAssignment
{
    [TestClass]
    public class AssignDeclaration
    {
        class DeclarationInfo: DeclarationSummaryKey
        {
            public string InspectorName { get; set; }
            public string InspectorSid { get; set; }
        }

        [TestMethod]
        public void AssignDeclarationToInspector()
        {
            var unassignedDecl = GetUnAssignedDeclaration();
            var admin = DbHelper.CreateUser();
            var inspector = DbHelper.CreateUser();
            var adapter = CreateAssignmentAdapter();

            adapter.Insert(unassignedDecl, long.Parse(admin.EmployeeNum), long.Parse(inspector.EmployeeNum));

            var assignedInspector = GetDeclarationInspector(unassignedDecl);

            Assert.AreEqual(inspector.Name, assignedInspector.Name);
            Assert.AreEqual(inspector.Sid, assignedInspector.Sid);
        }

        private UserInformation GetDeclarationInspector(DeclarationSummaryKey unassignedDecl)
        {
            var cmdText = 
                "select " +
                    "inspector, " + 
                    "inspector_sid " + 
                    "from nds2_mrr_user.declaration_active " +
                "where " + 
                    "inn_declarant = :inn_declarant and " +
                    "kpp_effective = :kpp_effective and " +
                    "period_effective = :period_effective and " +
                    "fiscal_year = :fiscal_year and " +
                    "period_code = :period_code and " +
                    "type_code = :type_code and " +
                    "rownum = 1";

            var oraParams = new OracleParameter[]
            {
                new OracleParameter(":inn_declarant",unassignedDecl.InnDeclarant),
                new OracleParameter(":kpp_effective",unassignedDecl.KppEffective),
                new OracleParameter(":period_effective",unassignedDecl.PeriodEffective),
                new OracleParameter(":fiscal_year",unassignedDecl.FiscalYear),
                new OracleParameter(":period_code","02"),
                new OracleParameter(":type_code","0")
            };

            return DbHelper.ExecuteCommand<UserInformation>(
                cmdText, 
                oraParams, 
                cmd =>
                {
                    var da = new OracleDataAdapter(cmd);
                    var ds = new DataSet();
                    da.Fill(ds);
                    return new UserInformation()
                    {
                        Name = ds.Tables[0].Rows[0]["inspector"].ToString(),
                        Sid = ds.Tables[0].Rows[0]["inspector_sid"].ToString(),
                    };
                });
        }

        private DeclarationInfo GetUnAssignedDeclaration()
        {
            var retVal = new DeclarationInfo()
            {
                InnDeclarant = "000000000",
                KppEffective = "123456789",
                PeriodEffective = 1,
                FiscalYear = 2018
            };

            var cmdText = "insert into nds2_mrr_user.declaration_active(" +
                                    "zip, "+
                                    "inn_declarant, " +
                                    "kpp_effective, " +
                                    "period_effective," +
                                    "fiscal_year," +
                                    "period_code," +
                                    "type_code) " +
                        "values (" +
                                    ":zip, " +
                                    ":inn_declarant, " +
                                    ":kpp_effective, " +
                                    ":period_effective, " +
                                    ":fiscal_year, " +
                                    ":period_code,"+
                                    ":type_code)";

            var oraParams = new OracleParameter[]
            {
                new OracleParameter(":zip",1),
                new OracleParameter(":inn_declarant",retVal.InnDeclarant),
                new OracleParameter(":kpp_effective",retVal.KppEffective),
                new OracleParameter(":period_effective",retVal.PeriodEffective),
                new OracleParameter(":fiscal_year",retVal.FiscalYear),
                new OracleParameter(":period_code","02"),
                new OracleParameter(":type_code","0")
            };
            using (var tr = new TransactionScope())
            {
                DbHelper.ExecuteVoidCommand(cmdText, oraParams);
                tr.Complete();
            }
            return retVal;
        }



        private IDeclarationAssignmentAdapter CreateAssignmentAdapter()
        {
            var moq = new Mock<IServiceProvider>();

            moq.Setup(x => x.GetConfigurationValue(It.IsAny<string>()))
                .Returns(ConfigurationManager.AppSettings["NDS2_DB"]);

           return DeclarationAssignmentAdapterCreator.DeclarationAssignmentAdapter(moq.Object);
        }
    }
}
