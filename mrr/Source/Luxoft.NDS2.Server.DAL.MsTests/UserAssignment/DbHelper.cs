﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.MsTests.UserAssignment
{
    public static class DbHelper
    {
        public static object ExecuteCommand(string cmdText, OracleParameter[] oraParams, OracleParameter retParam)
        {
            using (var conn = GetConnection())
            {
                try
                {
                    conn.Open();
                    var cmd = new OracleCommand(cmdText, conn);
                    cmd.BindByName = true;
                    cmd.Parameters.Add(retParam);
                    cmd.Parameters.AddRange(oraParams);
                    cmd.ExecuteNonQuery();
                    return retParam.Value;
                }

                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }


        public static void ExecuteVoidCommand(string cmdText, OracleParameter[] oraParams = null)
        {
            using (var conn = GetConnection())
            {
                try
                {
                    conn.Open();
                    var cmd = new OracleCommand(cmdText, conn);
                    if (oraParams != null)
                    {
                        cmd.BindByName = true;
                        cmd.Parameters.AddRange(oraParams);
                    }
                    cmd.ExecuteNonQuery();
                }

                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        static OracleConnection GetConnection()
        {
            return new OracleConnection(ConfigurationManager.AppSettings["NDS2_DB"]);
        }


        public static UserInformation CreateUser()
        {
            var uniqStr = Guid.NewGuid().ToString();
            var retVal = new UserInformation()
            {
                Name = "Test" + uniqStr.Substring(0, 6),
                Sid = uniqStr.Substring(0, 20)
            };

            var cmdText = "INSERT INTO nds2_mrr_user.mrr_user(id,name,sid)" +
                            "VALUES(nds2_mrr_user.seq$mrr_user.nextval,:name,:sid)" +
                            "RETURNING id INTO :insId";

            var retParam = new OracleParameter(":insId", OracleDbType.Decimal, ParameterDirection.Output);
            var oraParams = new OracleParameter[]
            {
                new OracleParameter(":name",retVal.Name),
                new OracleParameter(":sid",retVal.Sid)
            };
            retVal.EmployeeNum = DbHelper.ExecuteCommand(cmdText, oraParams, retParam).ToString();
            return retVal;
        }


        public static T ExecuteCommand<T>(string cmdText, 
                OracleParameter[] oraParams, 
                Func<OracleCommand,T> execute)
        {
            using (var conn = GetConnection())
            {
                try
                {
                    conn.Open();
                    var cmd = new OracleCommand(cmdText, conn);
                    cmd.BindByName = true;
                    cmd.Parameters.AddRange(oraParams);

                    return execute(cmd);
                }

                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
    }
}
