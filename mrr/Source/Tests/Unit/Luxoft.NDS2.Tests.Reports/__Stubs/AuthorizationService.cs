﻿using CommonComponents.Security.Authorization;
using System;
using System.Collections.Generic;
using System.Security.Principal;

namespace Luxoft.NDS2.Tests.__Stubs
{
    public class AuthorizationService : IAuthorizationService
    {
        public bool Authorize(string userIdentifier, string context)
        {
            throw new NotImplementedException();
        }

        public bool IsValidContext(string context)
        {
            throw new NotImplementedException();
        }

        public bool[] Authorize(IList<string> contexts)
        {
            throw new NotImplementedException();
        }

        public bool[] Authorize(IIdentity identity, IList<string> contexts)
        {
            throw new NotImplementedException();
        }

        public bool[] Authorize(string userIdentifier, IList<string> contexts)
        {
            throw new NotImplementedException();
        }

        public IList<PermissionObject> GetUserPermissions(string subsystemName, string structContext, PermissionType permissionTypes)
        {
            throw new NotImplementedException();
        }

        public IList<PermissionObject> GetUserPermissions(IIdentity identity, string subsystemName, string structContext, PermissionType permissionTypes)
        {
            throw new NotImplementedException();
        }

        public IList<PermissionObject> GetUserPermissions(string userIdentifier, string subsystemName, string structContext, PermissionType permissionTypes)
        {
            throw new NotImplementedException();
        }

        public IList<PermissionObject> GetPermissions(string subsystemName, string structContext, PermissionType permissionTypes)
        {
            throw new NotImplementedException();
        }

        public IList<string> GetStructContexts(IIdentity identity, string context)
        {
            throw new NotImplementedException();
        }

        public IList<string> GetStructContexts(string userIdentifier, string context)
        {
            throw new NotImplementedException();
        }

        public IList<PermissionObject> GetSubsystemPermissions(IIdentity identity, string subsystemName)
        {
            throw new NotImplementedException();
        }

        public IList<string> GetAssignments(string context)
        {
            throw new NotImplementedException();
        }

        public IList<string> GetStructContexts(string context)
        {
            throw new NotImplementedException();
        }

        public IList<PermissionObject> GetSubsystemPermissions(string userIdentifier, string subsystemName)
        {
            throw new NotImplementedException();
        }
    }
}
