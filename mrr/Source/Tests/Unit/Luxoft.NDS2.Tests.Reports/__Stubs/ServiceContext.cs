﻿using System;
using System.Collections.Generic;
using CommonComponents.Configuration;
using CommonComponents.Instrumentation;
using CommonComponents.Security.Authorization;
using CommonComponents.Shared;
using Luxoft.NDS2.Server.TaskScheduler.Reports.Services;

namespace Luxoft.NDS2.Tests.__Stubs
{
    public class ServiceContext : IReadOnlyServiceCollection
    {
        Dictionary<Type, object> _services = new Dictionary<Type, object>();

        public ServiceContext()
        {
            _services.Add(typeof(IConfigurationDataService), new ConfiguraionService());
            _services.Add(typeof(IInstrumentationService), new InstrumentationService());
            _services.Add(typeof(IAuthorizationService), new AuthorizationService());
            _services.Add(typeof(ITemplateService), new CatalogServiceStub());
        }

        #region Implementation of IServiceProvider

        public object GetService(Type serviceType)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Implementation of IReadOnlyServiceCollection

        public bool Contains(Type serviceType)
        {
            throw new NotImplementedException();
        }

        public bool Contains<TService>()
        {
            throw new NotImplementedException();
        }

        public object Get(Type serviceType)
        {
            return _services[serviceType];
        }

        public TService Get<TService>() 
        {
            return (TService)_services[typeof(TService)];
        }

        public bool TryGet(Type serviceType, out object serviceInstance)
        {
            throw new NotImplementedException();
        }

        public bool TryGet<TService>(out TService serviceInstance)
        {
            throw new NotImplementedException();
        }

        public object Resolve(Type serviceType)
        {
            throw new NotImplementedException();
        }

        public TService Resolve<TService>()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}

