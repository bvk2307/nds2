﻿using System;
using System.IO;
using CommonComponents.Catalog;
using Luxoft.NDS2.Server.TaskScheduler.Reports.Services;

namespace Luxoft.NDS2.Tests.__Stubs
{
    public class CatalogServiceStub : ITemplateService
    {
        public Stream GetTemplateStream(CatalogAddress address)
        {
            if (address.Item.EndsWith("technological_report.xlsx"))
            {
                return
                    File.Open(
                        Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Templates", "technological_report.xlsx"),
                        FileMode.Open);
            }

            if (address.Item.EndsWith("decl_status_report.xlsx"))
            {
                return
                    File.Open(
                        Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Templates", "decl_status_report.xlsx"),
                        FileMode.Open);
            }

            return null;
        }
    }
}
