﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonComponents.Instrumentation;

namespace Luxoft.NDS2.Tests.__Stubs
{
    public class InstrumentationService : IInstrumentationService
    {
        #region Implementation of IInstrumentationService

        public LogEntry CreateLogEntry(LogEntryTemplate template)
        {
            return new LogEntry(template);
        }

        public LogEntry Write(LogEntryTemplate logEntry)
        {
            Console.WriteLine(logEntry.Message);
            return new LogEntry(logEntry);
        }

        public LogEntry Write(LogEntryTemplate logEntry, Action<LogEntry> prepareEntry)
        {
            return new LogEntry(logEntry);
        }

        public IProfilingEntry CreateProfilingEntry(ProfilingEntryTemplate template)
        {
            throw new NotImplementedException();
        }

        public IProfilingEntry CreateProfilingEntry(TraceEventType eventType, int messageLevel, string messageType, int messagePriority)
        {
            return new ProfilingEntryStub();
        }

        public IProfilingEntry CreateProfilingEntry(TraceEventType eventType, int messageLevel, string messageType, int messagePriority, string settingsName)
        {
            throw new NotImplementedException();
        }

        public IProfilingEntry CreateProfilingEntry(TraceEventType eventType, int messageLevel, string messageType, int messagePriority, string settingsName, SettingsRequirements settingsRequirement)
        {
            throw new NotImplementedException();
        }

        public void Write(IProfilingEntry profilingEntry)
        {
            Console.WriteLine(profilingEntry.MessageText);
        }

        #endregion
    }
}
