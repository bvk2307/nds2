﻿using System.Diagnostics;
using CommonComponents.Instrumentation;
using CommonComponents.Security.Authorization;
using System;
using System.Collections.Generic;
using System.Security.Principal;

namespace Luxoft.NDS2.Tests.__Stubs
{
    public class ProfilingEntryStub : IProfilingEntry 
    {
        public void ChangeEventType(TraceEventType eventType)
        {
        }

        public IRealtimeMonitoringPropertiesItem CreateCheckpoint(string name, string description)
        {
            throw new NotImplementedException();
        }

        public IRealtimeMonitoringPropertiesItem CreateCheckpoint(string name, TraceEventType eventType, string description)
        {
            throw new NotImplementedException();
        }

        public IRealtimeMonitoringCompareContainer CreateCompareContainer(string name, string description)
        {
            throw new NotImplementedException();
        }

        public void Add(IRealtimeMonitoringNamedItem snapshot)
        {
        }

        public bool HasSettings()
        {
            return false;
        }

        public bool HasConfiguredParameter(string name)
        {
            return false;
        }

        public TType GetConfiguredParameterOrDefault<TType>(string name)
        {
            throw new NotImplementedException();
        }

        public bool TryGetConfiguredParameter<TType>(string name, out TType value)
        {
            throw new NotImplementedException();
        }

        public bool HasSettings(string settingsName)
        {
            throw new NotImplementedException();
        }

        public bool HasConfiguredParameter(string settingsName, string name)
        {
            throw new NotImplementedException();
        }

        public TType GetConfiguredParameterOrDefault<TType>(string settingsName, string name)
        {
            throw new NotImplementedException();
        }

        public bool TryGetConfiguredParameter<TType>(string settingsName, string name, out TType value)
        {
            throw new NotImplementedException();
        }

        public ProfilingSensitiveDataLevel UpgradeSensitiveDataLevel(ProfilingSensitiveDataLevel level)
        {
            throw new NotImplementedException();
        }

        public bool IsEnabled { get; private set; }
        public int MessageLevel { get; private set; }
        public int MonitoringLevel { get; private set; }
        public ProfilingSensitiveDataLevel SensitiveDataLevel { get; private set; }
        public int MessagePriority { get; private set; }
        public long GroupId { get; set; }
        public long CorrelationId { get; set; }
        public Guid GlobalUId { get; set; }
        public Guid GlobalGroupUId { get; set; }
        public Guid GlobalCorrelationUId { get; set; }
        public long OrderId { get; private set; }
        public TraceEventType EventType { get; private set; }
        public string MessageType { get; set; }
        public DateTime EventTime { get; set; }
        public TimeSpan Duration { get; private set; }
        public string MessageText { get; set; }
        public bool IsRealMessage { get; private set; }
        public bool IsEnqueued { get; private set; }
    }
}
