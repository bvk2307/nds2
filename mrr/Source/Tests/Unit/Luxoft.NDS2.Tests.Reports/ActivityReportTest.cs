﻿using System;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Server.TaskScheduler.Reports.ActivityReport;
using Luxoft.NDS2.Tests.__Stubs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
//using NUnit.Framework;

namespace Luxoft.NDS2.Tests.Reports
{
    [TestClass]
    public class ActivityReportTest
    {
        [TestClass]
        public class ActivityReportLancher
        {
            //[TestMethod]
            public void Build()
            {
                Build(@"C:\Luxoft\ReportService\out", new DateTime(2015, 5, 19), DateTime.Today);
            }

            public void Build(string path, DateTime begin, DateTime end)
            {
                var ctx = new ServiceContext();
                var cfg = new Configuration
                {
                    TR_ReportFilesLocation = new StringParam {Value = path}
                };

                var imp = new ActivityReportProvider(ctx);
                var startDate = begin;
                var endDate = end;

                while (startDate < endDate)
                {
                    Console.WriteLine("Creating report on: {0} ", startDate);
                    imp.Build(startDate, cfg);
                    startDate = startDate.AddDays(1);
                }
            }
        }
    }
}
