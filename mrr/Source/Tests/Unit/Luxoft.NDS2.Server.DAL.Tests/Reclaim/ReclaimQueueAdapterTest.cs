﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Server.Common.DTO.Reclaim;
using Luxoft.NDS2.Server.DAL.Helpers;
using Luxoft.NDS2.Server.DAL.Reclaim;
using Luxoft.NDS2.Server.Services.Helpers;
using NUnit.Framework;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Luxoft.NDS2.Server.DAL.Tests.Reclaim
{
    [TestFixture]
    public class ReclaimQueueAdapterTest
    {
        private readonly long COUNT_QUEUE_MAX = 200000;

        /// <summary>
        /// Проверяем работоспособность получения одного элемента очереди для формирования автоистребования
        /// </summary>
        [Test]
        public void PullQueueTest()
        {
            var serviceHelpers = new ServiceHelpers(ReclaimMockHelper.CreateStubServiceContext());

            using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(serviceHelpers))
            {
                long queueId = 111222333444555;

                RemoveQueueDiapazon(serviceHelpers, queueId, COUNT_QUEUE_MAX);

                InsertQueue(serviceHelpers, queueId);

                ReclaimQueue queue = null;

                var result = ReclaimQueueAdapterCreator
                    .Create(serviceHelpers, connectionFactory)
                    .TryPull(out queue);

                Assert.IsTrue(result);

                RemoveQueue(serviceHelpers, queueId);

                Assert.IsNotNull(queue);
                Assert.AreEqual(queue.Id, queueId);
            }
        }

        private OperationResult ExecuteSqlQuery(Luxoft.NDS2.Server.DAL.IServiceProvider _helpers, string sqlQuery, List<OracleParameter> oracleParameters)
        {
            return ReclaimSqlHelper.ExecuteSqlQuery(_helpers, sqlQuery, oracleParameters);
        }

        private void InsertQueue(Luxoft.NDS2.Server.DAL.IServiceProvider _helpers, long queueId)
        {
            string sqlTemplate = @"
insert into ASK_RECLAIM_QUEUE
(
  id,
  zip,
  reg_number,
  sono_code,
  period_code,
  fiscal_year,
  inn,
  kpp,
  kpp_effective,
  contragent_inn,
  contragent_kpp_effective,
  type_discrepancy_select,
  side,
  state,
  is_locked,
  create_date,
  update_date
)
values 
(
  {0},
  333444555666777,
  999111222,
  '5252',
  '21',
  '2016',
  '1234567890',
  '123456789',
  '111111111',
  '-',
  '-',
  0,
  1,
  1,
  0,
  TO_DATE('01.01.2012', 'dd.mm.yyyy'),
  null
);
commit;";

            ExecuteSqlQuery(
                _helpers,
                string.Format(sqlTemplate, queueId),
                new List<OracleParameter>());
        }

        private void RemoveQueue(Luxoft.NDS2.Server.DAL.IServiceProvider _helpers, long queueId)
        {
            ExecuteSqlQuery(
                _helpers,
                string.Format("delete from NDS2_MRR_USER.ASK_RECLAIM_QUEUE q where q.id = '{0}'; commit;", queueId),
                new List<OracleParameter>());
        }

        /// <summary>
        /// Проверяем работоспособность получения одного элемента очереди в нескольких потоках
        /// </summary>
        [Test]
        [Explicit]
        public void PullQueueManyThreadTest()
        {
            var serviceHelpers = new ServiceHelpers(ReclaimMockHelper.CreateStubServiceContext());

            long queueIdFirst = 111222333444555;
            long countQueue = 5000;
            int countThread = 5;

            RemoveQueueDiapazon(serviceHelpers, queueIdFirst, COUNT_QUEUE_MAX);

            InsertQueueDiapazon(serviceHelpers, queueIdFirst, countQueue);

            IndexReclaimQueueIdRebuild(serviceHelpers);

            IndexReclaimQueueCaptureRebuild(serviceHelpers);

            var queueProcesing = new QueueProcessing();
            queueProcesing.Run(countThread);

            bool isCheckHasNotCaptureQueue = false;
            for (int i = -1; ++i < 30000; )
            {
                long count = CheckHasNotCaptureQueue(serviceHelpers, queueIdFirst, countQueue);
                if (count == 0)
                {
                    isCheckHasNotCaptureQueue = true;
                    Thread.Sleep(countThread * 1000 + 2000);
                    break;
                }

                Thread.Sleep(5000);
            }

            RemoveQueueDiapazon(serviceHelpers, queueIdFirst, countQueue);

            Assert.IsTrue(isCheckHasNotCaptureQueue);

            var queueIdProcesseds = queueProcesing.GetQueueProcessed().Select(p => p.Id).ToList();

            for (int i = -1; ++i < countQueue; )
            {
                var count = queueIdProcesseds.Where(p => p == (queueIdFirst + i)).Count();
                Assert.AreEqual(count, 1);
            }
        }

        private void InsertQueueDiapazon(Luxoft.NDS2.Server.DAL.IServiceProvider _helpers, long queueIdFirst, long countQueue)
        {
            string sqlTemplate = @"
  FOR I IN 1..{1}
  LOOP
    insert into ASK_RECLAIM_QUEUE
    (
      id,
      zip,
      reg_number,
      sono_code,
      period_code,
      fiscal_year,
      inn,
      kpp,
      kpp_effective,
      contragent_inn,
      contragent_kpp_effective,
      type_discrepancy_select,
      side,
      state,
      is_locked,
      create_date,
      update_date
    )
    values 
    (
      {0} + I - 1,
      333444555666777,
      999111222,
      '5252',
      '21',
      '2016',
      '1234567890',
      '123456789',
      '111111111',
      '-',
      '-',
      0,
      1,
      1,
      0,
      TO_DATE('01.01.2012', 'dd.mm.yyyy'),
      null
    );
  END LOOP;
  commit;";

            ExecuteSqlQuery(
                _helpers,
                string.Format(sqlTemplate, queueIdFirst, countQueue),
                new List<OracleParameter>());
        }

        private void IndexReclaimQueueIdRebuild(Luxoft.NDS2.Server.DAL.IServiceProvider _helpers)
        {
            ExecuteSqlQuery(
               _helpers,
               string.Format("EXECUTE IMMEDIATE ('alter index NDS2_MRR_USER.IDX_RECLAIM_QUEUE_ID rebuild tablespace NDS2_DATA');"),
               new List<OracleParameter>());
        }

        private void IndexReclaimQueueCaptureRebuild(Luxoft.NDS2.Server.DAL.IServiceProvider _helpers)
        {
            ExecuteSqlQuery(
               _helpers,
               string.Format("EXECUTE IMMEDIATE ('alter index NDS2_MRR_USER.IDX_RECLAIM_QUEUE_CAPTURE rebuild tablespace NDS2_DATA');"),
               new List<OracleParameter>());
        }

        private void RemoveQueueDiapazon(Luxoft.NDS2.Server.DAL.IServiceProvider _helpers, long queueIdFirst, long countQueue)
        {
             ExecuteSqlQuery(
                _helpers,
                string.Format("delete from NDS2_MRR_USER.ASK_RECLAIM_QUEUE q where q.id >= {0} and q.id <= {1}; commit;",
                queueIdFirst, queueIdFirst + countQueue),
                new List<OracleParameter>());
        }

        private long CheckHasNotCaptureQueue(Luxoft.NDS2.Server.DAL.IServiceProvider _helpers, long queueIdFirst, long countQueue)
        {
            var oracleParametersChecking = new List<OracleParameter>() { FormatCommandHelper.NumericOut("pCount") };

            ExecuteSqlQuery(
                _helpers,
                string.Format("select count(1) into :pCount from NDS2_MRR_USER.ASK_RECLAIM_QUEUE q where q.id >= {0} and q.id <= {1} and q.is_locked = 0;",
                    queueIdFirst, queueIdFirst + countQueue),
                oracleParametersChecking);

            var paramCount = oracleParametersChecking.Where(p => p.ParameterName == "pCount").FirstOrDefault();
            return Int32.Parse(paramCount.Value.ToString());
        }

        /// <summary>
        /// Проверяем работоспособность количества необработанных элементов очереди рег.номеров
        /// </summary>
        [Test]
        public void ExistsInQueueTest()
        {
            var serviceHelpers = new ServiceHelpers(ReclaimMockHelper.CreateStubServiceContext());

            long queueIdFirst = 111222333444555;
            long countQueue = 30;

            RemoveQueueDiapazon(serviceHelpers, queueIdFirst, COUNT_QUEUE_MAX);

            DropTableTempQueue(serviceHelpers);

            CreateTableTempQueue(serviceHelpers);

            SetAllQueueState(serviceHelpers, (int)ReclaimQueueState.Processed);

            InsertQueueDiapazon(serviceHelpers, queueIdFirst, countQueue);

            using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(serviceHelpers))
            {
                var exists = ReclaimQueueAdapterCreator
                    .Create(serviceHelpers, connectionFactory)
                    .ExistsInQueue();

                RemoveQueueDiapazon(serviceHelpers, queueIdFirst, countQueue);

                Assert.IsTrue(exists);
            }

            RestoreQueueIsLocked(serviceHelpers);

            DropTableTempQueue(serviceHelpers);
        }

        private void CreateTableTempQueue(Luxoft.NDS2.Server.DAL.IServiceProvider _helpers)
        {
            ExecuteSqlQuery(
               _helpers,
               string.Format("EXECUTE IMMEDIATE ('create table TEMP_ASK_RECLAIM_QUEUE as select * from ASK_RECLAIM_QUEUE t');"),
               new List<OracleParameter>());
        }

        private void DropTableTempQueue(Luxoft.NDS2.Server.DAL.IServiceProvider _helpers)
        {
            ExecuteSqlQuery(
               _helpers,
               string.Format("EXECUTE IMMEDIATE ('drop table TEMP_ASK_RECLAIM_QUEUE'); exception when others then null;"),
               new List<OracleParameter>());
        }

        private void SetAllQueueState(Luxoft.NDS2.Server.DAL.IServiceProvider _helpers, int state)
        {
            ExecuteSqlQuery(
               _helpers,
               string.Format("update NDS2_MRR_USER.ASK_RECLAIM_QUEUE q set q.state = {0}; commit;", state),
               new List<OracleParameter>());
        }

        private void RestoreQueueIsLocked(Luxoft.NDS2.Server.DAL.IServiceProvider _helpers)
        {
            ExecuteSqlQuery(
               _helpers,
               string.Format("update NDS2_MRR_USER.ASK_RECLAIM_QUEUE q set q.state = (select a.state from TEMP_ASK_RECLAIM_QUEUE a where a.id = q.id); commit;"),
               new List<OracleParameter>());
        }

        /// <summary>
        /// Проверяем работоспособность получения одного элемента очереди для формирования автоистребования
        /// </summary>
        [Test]
        public void UpdateQueueTest()
        {
            var serviceHelpers = new ServiceHelpers(ReclaimMockHelper.CreateStubServiceContext());

            using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(serviceHelpers))
            {
                long queueId = 111222333444555;
                ReclaimQueueState state = ReclaimQueueState.Processed;
                ReclaimLocked locked = ReclaimLocked.Locked;

                RemoveQueueDiapazon(serviceHelpers, queueId, COUNT_QUEUE_MAX); 

                InsertQueue(serviceHelpers, queueId);

                ReclaimQueueAdapterCreator
                    .Create(serviceHelpers, connectionFactory)
                    .UpdateQueue(queueId, state, locked);

                var countQueue = CheckStateForUpdatingQueue(serviceHelpers, queueId, (int)state, (int)locked);

                RemoveQueue(serviceHelpers, queueId);

                Assert.AreEqual(countQueue, (int)ReclaimLocked.Locked);
            }
        }

        private long CheckStateForUpdatingQueue(Luxoft.NDS2.Server.DAL.IServiceProvider _helpers, long queueId, int state, int isLocked)
        {
            var oracleParametersChecking = new List<OracleParameter>() { FormatCommandHelper.NumericOut("pCount") };

            ExecuteSqlQuery(
                _helpers,
                string.Format("select count(1) into :pCount from NDS2_MRR_USER.ASK_RECLAIM_QUEUE q where q.id = {0} and q.state = {1} and q.is_locked = {2};",
                    queueId, state, isLocked),
                oracleParametersChecking);

            var paramCount = oracleParametersChecking.Where(p => p.ParameterName == "pCount").FirstOrDefault();
            return Int32.Parse(paramCount.Value.ToString());
        }
    }
}
