﻿using CommonComponents.Configuration;
using CommonComponents.Instrumentation;
using CommonComponents.Security.Authorization;
using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.DAL.Helpers;
using Luxoft.NDS2.Server.DAL.Reclaim;
using Luxoft.NDS2.Server.DAL.Reclaim.Adapters;
using Luxoft.NDS2.Server.Services.Helpers;
using NUnit.Framework;
using Oracle.DataAccess.Client;
using Rhino.Mocks;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Luxoft.NDS2.Server.DAL.Tests.Reclaim
{
    [TestFixture]
    public class ReclaimLogAdapterTest
    {
        private IReadOnlyServiceCollection GetStubServiceContext()
        {
            //--- Mock IConfigurationDataService
            var mockConfiguraionService = MockRepository.GenerateStub<IConfigurationDataService>();

            var _appSettings = new AppSettingsSection();
            _appSettings.Settings.Add(new KeyValueConfigurationElement(Constants.DB_CONFIG_KEY, ConfigurationManager.AppSettings[Constants.DB_CONFIG_KEY]));

            mockConfiguraionService.Expect(p => p.TryGetSection<AppSettingsSection>(
                Arg<ProfileInfo>.Is.Anything, out Arg<AppSettingsSection>.Out(_appSettings).Dummy))
                .Return(true);

            //--- Mock IProfilingEntry
            var mockProfilingEntry = MockRepository.GenerateStub<IProfilingEntry>();

            //--- Mock IInstrumentationService
            var mockInstrumentationService = MockRepository.GenerateStub<IInstrumentationService>();
            mockInstrumentationService.Expect(p => p.Write(Arg<IProfilingEntry>.Is.Anything));
            mockInstrumentationService.Expect(p => p.CreateLogEntry(Arg<LogEntryTemplate>.Is.Anything)).Return(null)
                .WhenCalled(_ => 
                { 
                    var template = (LogEntryTemplate)_.Arguments[0]; 
                    var ret = new LogEntry(template); 
                    _.ReturnValue = ret; 
                });

            mockInstrumentationService
                .Expect(p => p.CreateProfilingEntry(Arg<TraceEventType>.Is.Anything, Arg<int>.Is.Anything, Arg<string>.Is.Anything, Arg<int>.Is.Anything))
                .Return(mockProfilingEntry);

            //--- Mock IAuthorizationService
            var mockAuthorizationService = MockRepository.GenerateStub<IAuthorizationService>();

            //--- Mock Luxoft.NDS2.Server.Services.Managers.Query.IQueryProvider
            var mockQueryProvider = MockRepository.GenerateStub<Luxoft.NDS2.Server.Services.Managers.Query.IQueryProvider>();

            //--- Mock IReadOnlyServiceCollection (ServiceContext)
            var mockServiceContext = MockRepository.GenerateStub<IReadOnlyServiceCollection>();

            mockServiceContext.Expect(p => p.Get(typeof(IConfigurationDataService))).Return(mockConfiguraionService);
            mockServiceContext.Expect(p => p.Get(typeof(IInstrumentationService))).Return(mockInstrumentationService);
            mockServiceContext.Expect(p => p.Get(typeof(IAuthorizationService))).Return(mockAuthorizationService);
            mockServiceContext.Expect(p => p.Get(typeof(IQueryProvider))).Return(mockQueryProvider);

            mockServiceContext.Expect(p => p.Get<IConfigurationDataService>()).Return(mockConfiguraionService);
            mockServiceContext.Expect(p => p.Get<IInstrumentationService>()).Return(mockInstrumentationService);
            mockServiceContext.Expect(p => p.Get<IAuthorizationService>()).Return(mockAuthorizationService);
            mockServiceContext.Expect(p => p.Get<Luxoft.NDS2.Server.Services.Managers.Query.IQueryProvider>()).Return(mockQueryProvider);

            return mockServiceContext;
        }

        /// <summary>
        /// Проверяем работоспособность добавления сообщения в лог (тип ошибка)
        /// </summary>
        [Test]
        public void LogErrorTest()
        {
            Luxoft.NDS2.Server.DAL.IServiceProvider _helpers = new ServiceHelpers(GetStubServiceContext());

            string title = "title_test_error";
            string message = "message_test_error";
            long? zip = 123456789;
            string sonoCode = "5010";
            long? regNumber = 523;
            long? queueId = 278;

            RemoveLogMessage(_helpers, title);

            using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(_helpers))
            {
                CreateReclaimLogAdapter(_helpers, connectionFactory)
                    .LogError(title, message, zip, sonoCode, regNumber, queueId);
            }

            CheckHasLogRecord(_helpers, title, 1);

            RemoveLogMessage(_helpers, title);
        }

        /// <summary>
        /// Проверяем работоспособность добавления сообщения в лог (тип предупреждение)
        /// </summary>
        [Test]
        public void LogWarningTest()
        {
            Luxoft.NDS2.Server.DAL.IServiceProvider _helpers = new ServiceHelpers(GetStubServiceContext());

            string title = "title_test_warning";
            string message = "message_test_warning";
            long? zip = 12345;
            string sonoCode = "5252";
            long? regNumber = 423;
            long? queueId = 239;

            RemoveLogMessage(_helpers, title);

            using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(_helpers))
            {
                CreateReclaimLogAdapter(_helpers, connectionFactory)
                    .LogWarning(title, message, zip, sonoCode, regNumber, queueId);
            }

            CheckHasLogRecord(_helpers, title, 1);

            RemoveLogMessage(_helpers, title);
        }

        /// <summary>
        /// Проверяем работоспособность добавления сообщения в лог (тип информативное)
        /// </summary>
        [Test]
        public void LogInformationTest()
        {
            Luxoft.NDS2.Server.DAL.IServiceProvider _helpers = new ServiceHelpers(GetStubServiceContext());

            string title = "title_test_information";
            string message = "message_test_information";
            long? zip = null;
            string sonoCode = String.Empty;
            long? regNumber = null;
            long? queueId = 456;
            string reclaimLogInfoMessageUseName = "reclaim_log_info_message_use";

            var reclaimLogInfoMessageUseCurrentValue = ConfigurationHelper.ReadConfigurationParameter(_helpers, reclaimLogInfoMessageUseName);
            bool hasChangeConfigurationValue = false;
            if (reclaimLogInfoMessageUseCurrentValue != "Y")
            {
                ConfigurationHelper.SetConfigurationParameter(_helpers, reclaimLogInfoMessageUseName, "Y");
                hasChangeConfigurationValue = true;
            }

            RemoveLogMessage(_helpers, title);

            using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(_helpers))
            {
                CreateReclaimLogAdapter(_helpers, connectionFactory)
                    .LogInformation(title, message, zip, sonoCode, regNumber, queueId);
            }

            if (hasChangeConfigurationValue)
            {
                ConfigurationHelper.SetConfigurationParameter(_helpers, reclaimLogInfoMessageUseName, reclaimLogInfoMessageUseCurrentValue);
            }

            CheckHasLogRecord(_helpers, title, 1);

            RemoveLogMessage(_helpers, title);
        }

        /// <summary>
        /// Проверяем работоспособность добавления сообщения в лог (тип информативное)
        /// </summary>
        [Test]
        public void LogInformationNotSaveTest()
        {
            Luxoft.NDS2.Server.DAL.IServiceProvider _helpers = new ServiceHelpers(GetStubServiceContext());

            string title = "title_test_information";
            string message = "message_test_information";
            long? zip = null;
            string sonoCode = String.Empty;
            long? regNumber = null;
            long? queueId = 456;
            string reclaimLogInfoMessageUseName = "reclaim_log_info_message_use";

            var reclaimLogInfoMessageUseCurrentValue = ConfigurationHelper.ReadConfigurationParameter(_helpers, reclaimLogInfoMessageUseName);
            bool hasChangeConfigurationValue = false;
            if (reclaimLogInfoMessageUseCurrentValue != "N")
            {
                ConfigurationHelper.SetConfigurationParameter(_helpers, reclaimLogInfoMessageUseName, "N");
                hasChangeConfigurationValue = true;
            }

            RemoveLogMessage(_helpers, title);

            using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(_helpers))
            {
                CreateReclaimLogAdapter(_helpers, connectionFactory)
                    .LogInformation(title, message, zip, sonoCode, regNumber, queueId);
            }

            if (hasChangeConfigurationValue)
            {
                ConfigurationHelper.SetConfigurationParameter(_helpers, reclaimLogInfoMessageUseName, reclaimLogInfoMessageUseCurrentValue);
            }

            CheckHasLogRecord(_helpers, title, 0);

            RemoveLogMessage(_helpers, title);
        }

        private IReclaimLogAdapter CreateReclaimLogAdapter(Luxoft.NDS2.Server.DAL.IServiceProvider _helpers, ConnectionFactoryBase connectionFactory)
        {
            return ReclaimLogAdapterCreator
                    .Create(_helpers, connectionFactory);
        }

        private OperationResult ExecuteSqlQuery(Luxoft.NDS2.Server.DAL.IServiceProvider _helpers, 
            string sqlQuery, List<OracleParameter> oracleParameters)
        {
            return ReclaimSqlHelper.ExecuteSqlQuery(_helpers, sqlQuery, oracleParameters);
        }

        private void CheckHasLogRecord(Luxoft.NDS2.Server.DAL.IServiceProvider _helpers, string title, int countRecordForChecking)
        {
            //--- Проверить кол-во добавленных записей
            var oracleParametersChecking = new List<OracleParameter>();
            oracleParametersChecking.Add(FormatCommandHelper.NumericOut("pCount"));

            string sqlCheckingQuery = string.Format("select count(1) into :pCount from NDS2_MRR_USER.RECLAIM_LOG where title = '{0}';", title);
            var resultChecking = ExecuteSqlQuery(_helpers, sqlCheckingQuery, oracleParametersChecking);

            Assert.AreEqual(resultChecking.Status, ResultStatus.Success);

            var paramCount = oracleParametersChecking.Where(p => p.ParameterName == "pCount").FirstOrDefault();
            Assert.IsNotNull(paramCount);
            Assert.GreaterOrEqual(Int32.Parse(paramCount.Value.ToString()), countRecordForChecking);
        }

        private void RemoveLogMessage(Luxoft.NDS2.Server.DAL.IServiceProvider _helpers, string title)
        {
            string sqlRemoveQuery = string.Format("delete from NDS2_MRR_USER.RECLAIM_LOG where title = '{0}'; commit;", title);
            var resultRemove = ExecuteSqlQuery(_helpers, sqlRemoveQuery, new List<OracleParameter>());

            Assert.AreEqual(resultRemove.Status, ResultStatus.Success);
        }
    }
}
