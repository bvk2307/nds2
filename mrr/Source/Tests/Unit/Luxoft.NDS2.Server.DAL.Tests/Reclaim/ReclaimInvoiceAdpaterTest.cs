﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Server.DAL.Helpers;
using Luxoft.NDS2.Server.DAL.Reclaim;
using Luxoft.NDS2.Server.Services.Helpers;
using NUnit.Framework;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Tests.Reclaim
{
    [TestFixture]
    public class ReclaimInvoiceAdpaterTest
    {
        /// <summary>
        /// Проверяем работоспособность добавления всех счет-факутр в автоистребование
        /// </summary>
        [Test]
        public void InsertInvoicesTest()
        {
            var serviceHelpers = new ServiceHelpers(ReclaimMockHelper.CreateStubServiceContext());

            long zip = 999888777666555;
            long docId = 333999001;

            var reclaimDataPreparer = new ReclaimDataPreparer(serviceHelpers, zip, docId);

            using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(serviceHelpers))
            {
                reclaimDataPreparer.RemoveDataForInsertInvoices();
                reclaimDataPreparer.PrepareDataForInsertInvoices();

                ReclaimInvoiceAdapterCreator
                    .Create(serviceHelpers, connectionFactory)
                    .InsertAll(docId);

                CheckHasDocInvoices(serviceHelpers, docId, zip);

                CheckHasDocInvoicesChildBase(serviceHelpers, docId, zip, "DOC_INVOICE_BUYER");

                CheckHasDocInvoicesChildBase(serviceHelpers, docId, zip, "DOC_INVOICE_SELLER");

                CheckHasDocInvoicesChildBase(serviceHelpers, docId, zip, "DOC_INVOICE_BUY_ACCEPT");

                CheckHasDocInvoicesChildBase(serviceHelpers, docId, zip, "DOC_INVOICE_OPERATION");

                CheckHasDocInvoicesChildBase(serviceHelpers, docId, zip, "DOC_INVOICE_PAYMENT_DOC");

                reclaimDataPreparer.RemoveDataForInsertInvoices();
            }
        }

        private OperationResult ExecuteSqlQuery(Luxoft.NDS2.Server.DAL.IServiceProvider _helpers, string sqlQuery, List<OracleParameter> oracleParameters)
        {
            return ReclaimSqlHelper.ExecuteSqlQuery(_helpers, sqlQuery, oracleParameters);
        }

        private void CheckHasDocInvoices(Luxoft.NDS2.Server.DAL.IServiceProvider _helpers, long docId, long zip)
        {
            var oracleParametersChecking = new List<OracleParameter>() { FormatCommandHelper.NumericOut("pCount") };

            ExecuteSqlQuery(
                _helpers,
                string.Format("select count(1) into :pCount from NDS2_MRR_USER.DOC_INVOICE di where di.doc_id = {0} and di.declaration_version_id = {1};",
                    docId, zip),
                oracleParametersChecking);

            var paramCount = oracleParametersChecking.Where(p => p.ParameterName == "pCount").FirstOrDefault();
            Assert.IsNotNull(paramCount);
            Assert.AreEqual(Int32.Parse(paramCount.Value.ToString()), 2);
        }

        private void CheckHasDocInvoicesChildBase(Luxoft.NDS2.Server.DAL.IServiceProvider _helpers, long docId, long zip, string table)
        {
            var oracleParametersChecking = new List<OracleParameter>() { FormatCommandHelper.NumericOut("pCount") };

            var sqlTemplate = @"
select count(1) into :pCount from {2} a where a.row_key_id in 
(select di.invoice_row_key from DOC_INVOICE di where di.doc_id = {0} and di.declaration_version_id = {1});";

            ExecuteSqlQuery(
                _helpers,
                string.Format(sqlTemplate, docId, zip, table),
                oracleParametersChecking);

            var paramCount = oracleParametersChecking.Where(p => p.ParameterName == "pCount").FirstOrDefault();
            Assert.IsNotNull(paramCount);
            Assert.AreEqual(Int32.Parse(paramCount.Value.ToString()), 2);
        }
    }
}
