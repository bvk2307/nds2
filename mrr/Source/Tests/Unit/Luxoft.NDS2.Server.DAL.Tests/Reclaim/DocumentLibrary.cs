﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Tests.Reclaim
{
    internal static class DocumentXmlLibrary
    {
        private const long SIZE_DOCUMENT_SMALL = 10000;
        private const long SIZE_DOCUMENT_BIG = 80000;

        public static string CreateDocumentSmall()
        {
            return CreateDocument(SIZE_DOCUMENT_SMALL);
        }

        public static string CreateDocumentBig()
        {
            return CreateDocument(SIZE_DOCUMENT_BIG);
        }

        private static string CreateDocument(long documentSize)
        {
            var stringBuilder = new StringBuilder();
            string ret = String.Empty;

            for (long i = -1; ++i < 10000; )
            {
                stringBuilder.AppendFormat("<КодВидОпер_{0}>{1}</<КодВидОпер_{0}>", i, (((double)i) * 0.56) + 2);
                if (stringBuilder.Length > documentSize)
                    break;
            }

            return stringBuilder.ToString();
        }
    }
}
