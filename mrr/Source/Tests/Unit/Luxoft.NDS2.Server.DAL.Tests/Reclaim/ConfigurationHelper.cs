﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Tests.Reclaim
{
    public class ConfigurationHelper
    {
        private static OperationResult ExecuteSqlQuery(Luxoft.NDS2.Server.DAL.IServiceProvider _helpers,
            string sqlQuery, List<OracleParameter> oracleParameters)
        {
            return ReclaimSqlHelper.ExecuteSqlQuery(_helpers, sqlQuery, oracleParameters);
        }

        public static string ReadConfigurationParameter(Luxoft.NDS2.Server.DAL.IServiceProvider _helpers, string configKey)
        {
            var parameter = new OracleParameter(
                    "pValue",
                    OracleDbType.Varchar2,
                    ParameterDirection.Output
                );
            parameter.Size = 128;

            var oracleParameters = new List<OracleParameter>() { parameter };

            ExecuteSqlQuery(
                _helpers,
                string.Format("select t.Value into :pValue from NDS2_MRR_USER.CONFIGURATION t where t.parameter = '{0}';", configKey),
                oracleParameters);

            var paramValue = oracleParameters.Where(p => p.ParameterName == "pValue").FirstOrDefault();

            return paramValue.Value.ToString();
        }

        public static void SetConfigurationParameter(Luxoft.NDS2.Server.DAL.IServiceProvider _helpers, string configKey, string value)
        {
            var oracleParameters = new List<OracleParameter>();

            ExecuteSqlQuery(
                _helpers,
                string.Format("update NDS2_MRR_USER.CONFIGURATION t set t.value = '{1}' where t.parameter = '{0}';", configKey, value),
                oracleParameters);
        }
    }
}
