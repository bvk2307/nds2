﻿using Luxoft.NDS2.Server.DAL.Reclaim.Adapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Reclaim
{
    public static class ReclaimTestingAdapterCreator
    {
        public static IReclaimTestingAdapter Create(this IServiceProvider service)
        {
            return new ReclaimTestingAdapter(service);
        }
    }
}
