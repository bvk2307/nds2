﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.DAL.Helpers;
using Luxoft.NDS2.Server.DAL.Reclaim;
using Luxoft.NDS2.Server.Services.Helpers;
using NUnit.Framework;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Tests.Reclaim
{
    [TestFixture]
    public class ReclaimAdapterTest
    {
        /// <summary>
        /// Проверяем работоспособность добавления автоистребования
        /// </summary>
        [Test]
        public void InsertDocTest()
        {
            var serviceHelpers = new ServiceHelpers(ReclaimMockHelper.CreateStubServiceContext());

            long regNumber = 444999001;
            int docType = 2;
            int docKind = 1; 
            string sonoCode = "5252";
            long discrepancyCount = 2;
            decimal discrepancyAmount = 2300;
            int proccessed = 0;
            string inn = "1234567890";
            string kppEffective = "111111111";
            long queueId = 555666111222333;
            long zip = 999888777666555;
            int quarter = 1;
            int fiscalYear = 2015;

            using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(serviceHelpers))
            {
                var docId = 
                    ReclaimAdapterCreator
                    .Create(serviceHelpers, connectionFactory)
                    .Insert(
                        regNumber, 
                        docType,
                        docKind, 
                        sonoCode,
                        discrepancyCount,
                        discrepancyAmount,
                        proccessed,
                        inn,
                        kppEffective,
                        queueId,
                        zip,
                        quarter,
                        fiscalYear);

                CheckHasDoc(serviceHelpers, docId);

                CheckEqualsInsertingDocFields(serviceHelpers,
                    docId,
                    regNumber,
                    docType,
                    docKind,
                    sonoCode,
                    discrepancyCount,
                    discrepancyAmount,
                    proccessed,
                    inn,
                    kppEffective,
                    queueId,
                    zip,
                    quarter,
                    fiscalYear);

                RemoveDoc(serviceHelpers, docId);
            }
        }

        private OperationResult ExecuteSqlQuery(Luxoft.NDS2.Server.DAL.IServiceProvider _helpers, string sqlQuery, List<OracleParameter> oracleParameters)
        {
            return ReclaimSqlHelper.ExecuteSqlQuery(_helpers, sqlQuery, oracleParameters);
        }

        private void CheckHasDoc(Luxoft.NDS2.Server.DAL.IServiceProvider _helpers, long docId)
        {
            var oracleParametersChecking = new List<OracleParameter>() { FormatCommandHelper.NumericOut("pCount") };

            ExecuteSqlQuery(
                _helpers, 
                string.Format("select count(1) into :pCount from NDS2_MRR_USER.DOC d where d.doc_id = '{0}';", docId), 
                oracleParametersChecking);

            var paramCount = oracleParametersChecking.Where(p => p.ParameterName == "pCount").FirstOrDefault();
            Assert.IsNotNull(paramCount);
            Assert.AreEqual(Int32.Parse(paramCount.Value.ToString()), 1);
        }

        private void CheckEqualsInsertingDocFields(Luxoft.NDS2.Server.DAL.IServiceProvider _helpers,
            long docId,
            long regNumber,
            int docType,
            int docKind,
            string sonoCode,
            long discrepancyCount,
            decimal discrepancyAmount,
            int proccessed,
            string inn,
            string kppEffective,
            long queueId,
            long zip,
            int quarter,
            int fiscalYear)
        {
            string sqlTemplate = @"
select count(1) into :pCount from doc d 
where d.doc_id = '{0}'
      and d.ref_entity_id = {1}
      and d.doc_type = {2}
      and d.doc_kind = {3}
      and d.sono_code = '{4}'
      and d.discrepancy_count = {5}
      and d.discrepancy_amount = {6}
      and d.processed = {7}
      and d.inn = '{8}'
      and d.kpp_effective = '{9}'
      and d.queue_item_id = '{10}'
      and d.zip = '{11}'
      and d.quarter = '{12}'
      and d.fiscal_year = {13};";

            var oracleParametersChecking = new List<OracleParameter>() { FormatCommandHelper.NumericOut("pCount") };

            ExecuteSqlQuery(
                _helpers,
                string.Format(sqlTemplate, docId, regNumber, docType, docKind, sonoCode, discrepancyCount, discrepancyAmount,
                    proccessed, inn, kppEffective, queueId, zip, quarter, fiscalYear),
                oracleParametersChecking);

            var paramCount = oracleParametersChecking.Where(p => p.ParameterName == "pCount").FirstOrDefault();
            Assert.IsNotNull(paramCount);
            Assert.AreEqual(Int32.Parse(paramCount.Value.ToString()), 1);
        }

        private void RemoveDoc(Luxoft.NDS2.Server.DAL.IServiceProvider _helpers, long docId)
        {
            ExecuteSqlQuery(
                _helpers, 
                string.Format("delete from NDS2_MRR_USER.DOC d where d.doc_id = '{0}'; commit;", docId),
                new List<OracleParameter>());
        }

        /// <summary>
        /// Проверяем работоспособность обновления автоистребования (маленький документ)
        /// </summary>
        [Test]
        public void UpdateDocWithDocumentSmallTest()
        {
            UpdateDocTest(DocumentXmlLibrary.CreateDocumentSmall());
        }

        /// <summary>
        /// Проверяем работоспособность обновления автоистребования (большой документ)
        /// </summary>
        [Test]
        public void UpdateDocWithDocumentBigTest()
        {
            UpdateDocTest(DocumentXmlLibrary.CreateDocumentBig());
        }

        public void UpdateDocTest(string documentBody)
        {
            int[] statuses = { 11 }; //--- ручная проверка

            UpdateDocTest(documentBody, "Y", statuses);
        }

        /// <summary>
        /// Проверяем работоспособность обновления автоистребования (большой документ)
        /// </summary>
        [Test]
        public void UpdateDocWithStatusCreated()
        {
            int[] statuses = { 1, 2 }; //--- создано, отправлено

            UpdateDocTest(DocumentXmlLibrary.CreateDocumentSmall(), "N", statuses);
        }

        private void UpdateDocTest(string documentBody, string reclaimManualCheckNeedValue, int[] statuses)
        {
            var serviceHelpers = new ServiceHelpers(ReclaimMockHelper.CreateStubServiceContext());

            long docId = 777333222111;
            long discrepancyCount = 2;
            decimal discrepancyAmount = 2300;
            decimal discrepancyAmountPvp = 1950;
            bool hasSchemaValidationErrors = false;
            bool hasDatabaseError = false;
            bool hasGeneralError = false;
            int proccessed = 1;
            string reclaimManualCheckDocUseName = "reclaim_manual_check_doc_use";

            var reclaimManualCheckDocUseValue = ConfigurationHelper.ReadConfigurationParameter(serviceHelpers, reclaimManualCheckDocUseName);
            bool hasChangeConfigurationValue = false;
            if (reclaimManualCheckDocUseValue != reclaimManualCheckNeedValue)
            {
                ConfigurationHelper.SetConfigurationParameter(serviceHelpers, reclaimManualCheckDocUseName, reclaimManualCheckNeedValue);
                hasChangeConfigurationValue = true;
            }

            using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(serviceHelpers))
            {
                RemoveDoc(serviceHelpers, docId);

                InsertDoc(serviceHelpers, docId);

                ReclaimAdapterCreator
                    .Create(serviceHelpers, connectionFactory)
                    .Update(
                        docId,
                        discrepancyCount,
                        discrepancyAmount,
                        discrepancyAmountPvp,
                        hasSchemaValidationErrors,
                        hasDatabaseError,
                        hasGeneralError,
                        documentBody,
                        proccessed);

                CheckHasDoc(serviceHelpers, docId);

                CheckEqualsUpdatingDocFields(serviceHelpers,
                        docId,
                        discrepancyCount,
                        discrepancyAmount,
                        statuses,
                        documentBody,
                        proccessed);

                RemoveDoc(serviceHelpers, docId);
            }

            if (hasChangeConfigurationValue)
                ConfigurationHelper.SetConfigurationParameter(
                    serviceHelpers, 
                    reclaimManualCheckDocUseName,
                    reclaimManualCheckDocUseValue);
        }

        private void InsertDoc(Luxoft.NDS2.Server.DAL.IServiceProvider _helpers, long docId)
        {
            string sqlTemplate = @"
  insert into doc
  (
    doc_id,
    ref_entity_id,
    doc_type,
    doc_kind,
    sono_code,
    discrepancy_count,
    discrepancy_amount,
    create_date,
    status_date,
    status,
    stage,
    user_comment,
    processed,
    inn,
    kpp_effective
  )
  values
  (
    {0},
    444999001,
    2,
    1,
    '5252',
    2,
    1570,
    sysdate,
    trunc(sysdate),
    11,
    1,
    'test_insert_comment',
    1,
    '1234567890',
    '111111111'
  ); commit;";

            ExecuteSqlQuery(
                _helpers,
                string.Format(sqlTemplate, docId),
                new List<OracleParameter>());
        }

        private void CheckEqualsUpdatingDocFields(Luxoft.NDS2.Server.DAL.IServiceProvider _helpers, 
            long docId, 
            long discrepancyCount,
            decimal discrepancyAmount,
            int[] statuses,
            string documentBody,
            int proccessed)
        {
            string sqlTemplate = @"
select count(1) into :pCount from doc d 
where d.doc_id = '{0}'
      and d.discrepancy_count = {1}
      and d.discrepancy_amount = {2}
      and d.status in ({3})
      and dbms_lob.compare(d.document_body, :pDocumentBody) = 0
      and d.processed = {4};";

            var oracleParametersChecking = new List<OracleParameter>() 
            { 
                FormatCommandHelper.ClobIn("pDocumentBody", documentBody),
                FormatCommandHelper.NumericOut("pCount")
            };

            string statuesString = String.Empty;
            bool isFirst = true;
            for (int i = -1; ++i < statuses.Count(); )
            {
                if (isFirst)
                    isFirst = false;
                else
                    statuesString += ",";
                statuesString += statuses[i];
            }

            ExecuteSqlQuery(
                    _helpers,
                    string.Format(sqlTemplate, docId, discrepancyCount, discrepancyAmount, statuesString, proccessed),
                    oracleParametersChecking);

            var paramCount = oracleParametersChecking.Where(p => p.ParameterName == "pCount").FirstOrDefault();
            Assert.IsNotNull(paramCount);
            Assert.AreEqual(Int32.Parse(paramCount.Value.ToString()), 1);
        }
    }
}
