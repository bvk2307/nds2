﻿using Luxoft.NDS2.Server.Common.DTO.Reclaim;
using Luxoft.NDS2.Server.DAL.Reclaim;
using Luxoft.NDS2.Server.Services.Helpers;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Luxoft.NDS2.Server.DAL.Tests.Reclaim
{
    public class QueueProcessing
    {
        private ServiceHelpers serviceHelpers = new ServiceHelpers(ReclaimMockHelper.CreateStubServiceContext());

        private ConcurrentBag<ReclaimQueue> queueProcessed = new ConcurrentBag<ReclaimQueue>();

        public QueueProcessing()
        {
        }

        public void Run(int countThread)
        {
            for (int i = -1; ++i < countThread; )
                ThreadPool.QueueUserWorkItem(DoTask);
        }

        public List<ReclaimQueue> GetQueueProcessed()
        {
            return queueProcessed.ToList();
        }

        private void DoTask(object instance)
        {
            using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(serviceHelpers))
            {
                var reclaimQueueAdapter = ReclaimQueueAdapterCreator
                    .Create(serviceHelpers, connectionFactory);

                for (int i = -1; ++i < 200000; )
                {
                    ReclaimQueue queue = null;
                    bool result = reclaimQueueAdapter
                        .TryPull(out queue);

                    if (result && queue != null)
                    {
                        queueProcessed.Add(queue);
                        Thread.Sleep(30);
                    }
                    else
                        Thread.Sleep(1000);
                }
            }
        }
    }
}
