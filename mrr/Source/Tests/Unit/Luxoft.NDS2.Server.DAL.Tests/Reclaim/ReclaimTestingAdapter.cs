﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Server.DAL.Implementation;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Reclaim.Adapters
{
    public class ReclaimTestingAdapter : BaseOracleTableAdapter, IReclaimTestingAdapter
    {
        # region Конструктор

        /// <summary>
        /// Создает экземпляр класса ReclaimTestingAdpater
        /// </summary>
        /// <param name="service"></param>
        public ReclaimTestingAdapter(IServiceProvider service)
            : base(service)
        {
        }

        # endregion

        /// <summary>
        /// Выполняет произвольный запрос
        /// </summary>
        /// <param name="queryText">текст запроса</param>
        /// <param name="parameters">список параметров</param>
        /// <returns>результат выполнения запроса</returns>
        public OperationResult ExecuteQuery(string queryText, List<OracleParameter> parameters)
        {
            var result = new OperationResult();

            try
            {
                Execute(queryText, CommandType.Text, parameters.ToArray());
                result.Status = ResultStatus.Success;
            }
            catch(Exception ex)
            {
                result.Status = ResultStatus.Error;
                result.Message = ex.Message;
            }

            return result;
        }
    }
}
