﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Server.DAL.Helpers;
using Luxoft.NDS2.Server.DAL.Reclaim;
using Luxoft.NDS2.Server.Services.Helpers;
using NUnit.Framework;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Tests.Reclaim
{
    [TestFixture]
    public class ReclaimDiscrepancyAdpaterTest
    {
        private const long TestZip = 999888777666555;
        private const long TestQueueId = 111222333444555;

        /// <summary>
        /// Проверяем работоспособность чтения расхождений для автоистребования
        /// </summary>
        [Test]
        public void SearchDiscrepanciesTest()
        {
            var serviceHelpers = new ServiceHelpers(ReclaimMockHelper.CreateStubServiceContext());

            var reclaimDataPreparer = new ReclaimDataPreparer(serviceHelpers, TestZip, TestQueueId);

            using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(serviceHelpers))
            {
                reclaimDataPreparer.RemoveDataForSearchDiscrepacies();
                reclaimDataPreparer.PrepareDataForSearchDiscrepacies();

                var disrepacies = ReclaimDiscrepancyAdapterCreator
                    .Create(serviceHelpers, connectionFactory)
                    .SelectSideFirstAll(TestQueueId);

                reclaimDataPreparer.RemoveDataForSearchDiscrepacies();

                Assert.IsNotNull(disrepacies);
                Assert.AreEqual(disrepacies.Count(), 2);
            }
        }

        /// <summary>
        /// Проверяем работоспособность добавления расхождения в автоистребование
        /// </summary>
        [Test]
        public void InsertDiscrepancyTest()
        {
            var serviceHelpers = new ServiceHelpers(ReclaimMockHelper.CreateStubServiceContext());

            long docId = 333999001;
            long discrepancyId = 555999105;
            string rowKey = string.Format("{0}/0/000000001", 999888777666555);
            decimal amount = 8734;
            decimal amountPvp = 2894;

            using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(serviceHelpers))
            {
                RemoveDocDiscrepancy(serviceHelpers, docId, discrepancyId);

                ReclaimDiscrepancyAdapterCreator
                    .Create(serviceHelpers, connectionFactory)
                    .Insert(docId, discrepancyId, rowKey, amount, amountPvp);

                CheckHasDocDiscrepancy(serviceHelpers, docId, discrepancyId, rowKey);

                RemoveDocDiscrepancy(serviceHelpers, docId, discrepancyId);
            }
        }

        private OperationResult ExecuteSqlQuery(Luxoft.NDS2.Server.DAL.IServiceProvider _helpers, string sqlQuery, List<OracleParameter> oracleParameters)
        {
            return ReclaimSqlHelper.ExecuteSqlQuery(_helpers, sqlQuery, oracleParameters);
        }

        private void CheckHasDocDiscrepancy(Luxoft.NDS2.Server.DAL.IServiceProvider _helpers, long docId, long discrepancyId, string rowKey)
        {
            var oracleParametersChecking = new List<OracleParameter>() { FormatCommandHelper.NumericOut("pCount") };

            ExecuteSqlQuery(
                _helpers,
                string.Format("select count(1) into :pCount from NDS2_MRR_USER.DOC_DISCREPANCY dd where dd.doc_id = '{0}' and dd.discrepancy_id = '{1}' and dd.row_key = '{2}' ;",
                    docId, discrepancyId, rowKey),
                oracleParametersChecking);

            var paramCount = oracleParametersChecking.Where(p => p.ParameterName == "pCount").FirstOrDefault();
            Assert.IsNotNull(paramCount);
            Assert.AreEqual(Int32.Parse(paramCount.Value.ToString()), 1);
        }

        private void RemoveDocDiscrepancy(Luxoft.NDS2.Server.DAL.IServiceProvider _helpers, long docId, long discrepancyId)
        {
            ExecuteSqlQuery(
                _helpers,
                string.Format("delete from NDS2_MRR_USER.DOC_DISCREPANCY dd where dd.doc_id = '{0}' and dd.discrepancy_id = '{1}'; commit;", docId, discrepancyId),
                new List<OracleParameter>());
        }
    }
}
