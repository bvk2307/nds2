﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Reclaim.Adapters
{
    public interface IReclaimTestingAdapter
    {
        /// <summary>
        /// Выполняет произвольный запрос
        /// </summary>
        /// <param name="queryText">текст запроса</param>
        /// <param name="parameters">список параметров</param>
        /// <returns>результат выполнения запроса</returns>
        OperationResult ExecuteQuery(string queryText, List<OracleParameter> parameters);
    }
}
