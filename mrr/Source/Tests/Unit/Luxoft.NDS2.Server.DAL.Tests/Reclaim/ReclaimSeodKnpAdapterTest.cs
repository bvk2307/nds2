﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Server.Common.DTO.Reclaim;
using Luxoft.NDS2.Server.DAL.Reclaim;
using Luxoft.NDS2.Server.Services.Helpers;
using NUnit.Framework;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Tests.Reclaim
{
    [TestFixture]
    public class ReclaimSeodKnpAdapterTest
    {
        /// <summary>
        /// Проверяем работоспособность получения состояния КНП корректировки, когда КНП открыта
        /// </summary>
        [Test]
        public void GetStateSeodKnpOpenTest()
        {
            var serviceHelpers = new ServiceHelpers(ReclaimMockHelper.CreateStubServiceContext());

            string sonoCode = "5252";
            long regNumber = 111222333444555;

            using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(serviceHelpers))
            {
                var stateKnp = ReclaimSeodKnpAdapterCreator
                    .Create(serviceHelpers, connectionFactory)
                    .GetStateSeodKnp(sonoCode, regNumber);

                Assert.AreEqual(stateKnp, ReclaimStateSeodKnp.Opened);
            }
        }

        /// <summary>
        /// Проверяем работоспособность получения состояния КНП корректировки, когда КНП закрыта
        /// </summary>
        [Test]
        public void GetStateSeodKnpCloseTest()
        {
            var serviceHelpers = new ServiceHelpers(ReclaimMockHelper.CreateStubServiceContext());

            long seodKnpId = 111333444555666;
            string sonoCode = "5252";
            long regNumber = 222333444555;

            using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(serviceHelpers))
            {
                RemoveSeodKnp(serviceHelpers, seodKnpId);

                AddSeodKnp(serviceHelpers, seodKnpId, regNumber, sonoCode);

                var stateKnp = ReclaimSeodKnpAdapterCreator
                    .Create(serviceHelpers, connectionFactory)
                    .GetStateSeodKnp(sonoCode, regNumber);

                RemoveSeodKnp(serviceHelpers, seodKnpId);

                Assert.AreEqual(stateKnp, ReclaimStateSeodKnp.Closed);
            }
        }

        private OperationResult ExecuteSqlQuery(Luxoft.NDS2.Server.DAL.IServiceProvider _helpers, string sqlQuery, List<OracleParameter> oracleParameters)
        {
            return ReclaimSqlHelper.ExecuteSqlQuery(_helpers, sqlQuery, oracleParameters);
        }

        private void RemoveSeodKnp(Luxoft.NDS2.Server.DAL.IServiceProvider _helpers, long seodKnpId)
        {
            ExecuteSqlQuery(
               _helpers,
               string.Format("delete from seod_knp a where a.knp_id = {0}; commit;", seodKnpId),
               new List<OracleParameter>());
        }

        private void AddSeodKnp(Luxoft.NDS2.Server.DAL.IServiceProvider _helpers, long seodKnpId, long regNumber, string sonoCode)
        {
            string sqlTemplate = @"
insert into seod_knp 
(
  knp_id,
  declaration_reg_num,
  creation_date,
  ifns_code,
  completion_date
)
values
(
  {0},
  {1},
  sysdate,
  '{2}',
  sysdate
);
commit;"; 

            ExecuteSqlQuery(
               _helpers,
               string.Format(sqlTemplate, seodKnpId, regNumber, sonoCode),
               new List<OracleParameter>());
        }
    }
}
