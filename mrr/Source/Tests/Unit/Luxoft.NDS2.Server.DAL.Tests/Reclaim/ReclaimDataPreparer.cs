﻿using Luxoft.NDS2.Common.Contracts.DTO;
using NUnit.Framework;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Tests.Reclaim
{
    public class ReclaimDataPreparer
    {
        private readonly IServiceProvider _serviceHelpers;
        private readonly long _zip;
        private readonly long _docId;
        private readonly string _sonoCode;
        private readonly long _regNumber;
        private readonly long _seodId;
        private readonly long _askId;
        private readonly long _queueId;
        private readonly string _idFile;
        private readonly long _discrepancyId_One;
        private readonly long _discrepancyId_Two;

        public ReclaimDataPreparer(IServiceProvider serviceHelpers, long zip, long queueId)
        {
            _serviceHelpers = serviceHelpers;
            _zip = zip;
            _queueId = queueId;
            _docId = 333999001;
            _sonoCode = "5252";
            _regNumber = 444999001;
            _seodId = 555777888;
            _askId = 777333444555111;
            _idFile = "id_file_999888777666555";
            _discrepancyId_One = 555999001;
            _discrepancyId_Two = 555999002;
        }

        public ReclaimDataPreparer(IServiceProvider serviceHelpers, long zip, long docId, long queueId = 0)
        {
            _serviceHelpers = serviceHelpers;
            _zip = zip;
            _docId = docId;
            _regNumber = 444999001;
            _discrepancyId_One = 555999001;
            _discrepancyId_Two = 555999002;
        }

        public void RemoveDataForSearchDiscrepacies()
        {
            RemoveClaim();
            RemoveClaimDiscrepancy();
            RemoveClaimInvoice();
            RemoveSovDiscrepancy();
            RemoveSovInvoiceAll();
            RemoveSeodDeclaration();
            RemoveAskAndJrnl();
            RemoveQueue();
        }

        public void PrepareDataForSearchDiscrepacies()
        {
            PrepareClaim();
            PrepareClaimDiscrepancy();
            PrepareClaimInvoice();
            PrepareSovDiscrepancy();
            PrepareSovInvoiceAll();
            PrepareSeodDeclaration();
            PrepareAskAndJrnl();
            PrepareQueue();
        }

        public void RemoveDataForInsertInvoices()
        {
            RemoveReclaim();
            RemoveClaimDiscrepancy();
            RemoveClaimInvoice();
            RemoveSovDiscrepancy();
            RemoveSovInvoiceAll();
            RemoveSeodDeclaration();
            RemoveAskAndJrnl();
        }

        public void PrepareDataForInsertInvoices()
        {
            PrepareReclaim();
            PrepareClaimDiscrepancy();
            PrepareSovDiscrepancy();
            PrepareSovInvoiceAll();
            PrepareSeodDeclaration();
            PrepareAskAndJrnl();
        }

        private void ExecuteSqlQuery(string sqlPrepareQuery)
        {
            var helpAdapter = TableAdapterCreator.DiscrepancySelectionAdpater(_serviceHelpers);

            string sqlTemplate = "begin {0} end;";

            var result = helpAdapter.ExecuteQuery(string.Format(sqlTemplate, sqlPrepareQuery), new List<OracleParameter>());

            Assert.AreEqual(result.Status, ResultStatus.Success);
        }

        private void RemoveClaim()
        {
            ExecuteSqlQuery(string.Format("delete from nds2_mrr_user.doc d where d.doc_id = {0}; commit;", _docId));
        }

        private void PrepareClaim()
        {
            string sqlPrepareClaimTemplate = @"
-- Автотребование
insert into nds2_mrr_user.doc
(
  doc_id, external_doc_num, external_doc_date, ref_entity_id, doc_type, doc_kind,
  sono_code, discrepancy_count, discrepancy_amount, create_date, seod_accepted,
  seod_accept_date, tax_payer_send_date, tax_payer_delivery_date, tax_payer_delivery_method,
  tax_payer_reply_date, close_date, status_date, status, stage, document_body,
  user_comment, deadline_date, prolong_answer_date, process_elapsed, processed,
  inn, kpp_effective, close_reason
) 
values({0}, null, null, {1}, 1, 1, '5252', 5, 5000, sysdate, 0, 
       null, null, null, null, null, null, trunc(sysdate), 10, 1, null, null, null,
       null, null, null, null, null, null);

commit;";

            ExecuteSqlQuery(string.Format(sqlPrepareClaimTemplate, _docId, _regNumber));
        }

        private void RemoveClaimDiscrepancy()
        {
            ExecuteSqlQuery(string.Format("delete from nds2_mrr_user.DOC_DISCREPANCY dis where dis.doc_id = {0}; commit;", _docId));
        }

        private void PrepareClaimDiscrepancy()
        {
            string sqlPrepareClaimDiscrepancyTemplate = @"       
insert into nds2_mrr_user.DOC_DISCREPANCY (doc_id, discrepancy_id, row_key)
values({0}, {1}, '{3}/0/001');
insert into nds2_mrr_user.DOC_DISCREPANCY (doc_id, discrepancy_id, row_key)
values({0}, {2}, '{3}/0/002');

commit;";

            ExecuteSqlQuery(string.Format(sqlPrepareClaimDiscrepancyTemplate, _docId, _discrepancyId_One, _discrepancyId_Two,  _zip));
        }

        private void RemoveClaimInvoice()
        {
            string sqlTemplate = @"
delete from DOC_INVOICE_BUYER a where a.row_key_id in
    (select di.invoice_row_key from DOC_INVOICE di where di.doc_id = {0});

delete from DOC_INVOICE_SELLER a where a.row_key_id in
    (select di.invoice_row_key from DOC_INVOICE di where di.doc_id = {0});

delete from DOC_INVOICE_BUY_ACCEPT a where a.row_key_id in
    (select di.invoice_row_key from DOC_INVOICE di where di.doc_id = {0});

delete from DOC_INVOICE_OPERATION a where a.row_key_id in
    (select di.invoice_row_key from DOC_INVOICE di where di.doc_id = {0});

delete from DOC_INVOICE_PAYMENT_DOC a where a.row_key_id in
    (select di.invoice_row_key from DOC_INVOICE di where di.doc_id = {0});

delete from DOC_INVOICE dis where dis.doc_id = {0}; 

commit;
";

            ExecuteSqlQuery(string.Format(sqlTemplate, _docId));
        }

        private void PrepareClaimInvoice()
        {
            string sqlPrepareClaimInvoiceTemplate = @" 
insert into nds2_mrr_user.DOC_INVOICE
( 
  declaration_version_id, invoice_chapter, invoice_row_key, doc_id, ordinal_number, okv_code, create_date,
  receive_date, invoice_num, invoice_date, change_num, change_date, correction_num, correction_date,
  change_correction_num, change_correction_date, seller_invoice_num, seller_invoice_date, broker_inn,
  broker_kpp, deal_kind_code, customs_declaration_num, price_buy_amount, price_buy_nds_amount,
  price_sell, price_sell_in_curr, price_sell_18, price_sell_10, price_sell_0, price_nds_18, price_nds_10,
  price_tax_free, price_total, price_nds_total, diff_correct_decrease, diff_correct_increase, diff_correct_nds_decrease,
  diff_correct_nds_increase, price_nds_buyer, actual_row_key, compare_row_key, compare_algo_id, format_errors,
  logical_errors, seller_agency_info_inn, seller_agency_info_kpp, seller_agency_info_name,
  seller_agency_info_num, seller_agency_info_date, is_import, clarification_key, contragent_key,
  is_dop_list, decl_tax_period, decl_fiscal_year
)
values
( 
  {0}, 8, '{0}/0/001', {1}, '001', null, TO_DATE('01.01.2016', 'dd.mm.yyyy'),
  null, 'number 001', TO_DATE('01.01.2016', 'dd.mm.yyyy'), null, null, null, null,
  null, null, 'number 001', TO_DATE('01.01.2016', 'dd.mm.yyyy'), null,
  null, null, null, 1234, 2345, 1, 2, 3, 4, 5, 6, 7,
  8, 9, 10, 11, 12, 13, 14, 15, null, null, null, null,
  null, null, null, null, null, null, null, null, null, null, '21', '2016'
);

insert into nds2_mrr_user.DOC_INVOICE
( 
  declaration_version_id, invoice_chapter, invoice_row_key, doc_id, ordinal_number, okv_code, create_date,
  receive_date, invoice_num, invoice_date, change_num, change_date, correction_num, correction_date,
  change_correction_num, change_correction_date, seller_invoice_num, seller_invoice_date, broker_inn,
  broker_kpp, deal_kind_code, customs_declaration_num, price_buy_amount, price_buy_nds_amount,
  price_sell, price_sell_in_curr, price_sell_18, price_sell_10, price_sell_0, price_nds_18, price_nds_10,
  price_tax_free, price_total, price_nds_total, diff_correct_decrease, diff_correct_increase, diff_correct_nds_decrease,
  diff_correct_nds_increase, price_nds_buyer, actual_row_key, compare_row_key, compare_algo_id, format_errors,
  logical_errors, seller_agency_info_inn, seller_agency_info_kpp, seller_agency_info_name,
  seller_agency_info_num, seller_agency_info_date, is_import, clarification_key, contragent_key,
  is_dop_list, decl_tax_period, decl_fiscal_year
)
values
( 
  {0}, 8, '{0}/0/002', {1}, '001', null, TO_DATE('02.01.2016', 'dd.mm.yyyy'),
  null, 'number 002', TO_DATE('02.01.2016', 'dd.mm.yyyy'), null, null, null, null,
  null, null, 'number 002', TO_DATE('02.01.2016', 'dd.mm.yyyy'), null,
  null, null, null, 2234, 3345, 21, 22, 23, 24, 25, 26, 27,
  28, 29, 30, 31, 32, 33, 34, 35, null, null, null, null,
  null, null, null, null, null, null, null, null, null, null, '21', '2016'
);

commit;";

            ExecuteSqlQuery(string.Format(sqlPrepareClaimInvoiceTemplate, _zip, _docId));
        }

        private void RemoveSovDiscrepancy()
        {
            ExecuteSqlQuery(string.Format("delete from nds2_mrr_user.SOV_DISCREPANCY sov where sov.id in ({0}, {1}); commit;", _discrepancyId_One, _discrepancyId_Two));
        }

        private void PrepareSovDiscrepancy()
        {
            string sqlPrepareSovDiscrepancyTemplate = @" 
-- СОВ расхождение
insert into nds2_mrr_user.SOV_DISCREPANCY
(
  id, ifns, create_date, type, rule_group, deal_amnt, amnt, amount_pvp, invoice_chapter,
  invoice_rk, invoice_contractor_chapter, invoice_contractor_rk, status, 
  rule_num, close_date, buyer_inn, seller_inn, mult_compare, buyer_operation_code, seller_operation_code,
  buyer_zip, seller_zip
)
values
(
  {1}, '5252', TO_DATE('22.01.2016', 'dd.mm.yyyy'), 1, 1, 21, 22, 23, 8,
  '{0}/0/001', null, null, 1, 
  1, null, '1234567890', '1234567891', 1, null, null,
  {0}, null
);

insert into nds2_mrr_user.SOV_DISCREPANCY
(
  id, ifns, create_date, type, rule_group, deal_amnt, amnt, amount_pvp, invoice_chapter,
  invoice_rk, invoice_contractor_chapter, invoice_contractor_rk, status,  
  rule_num, close_date, buyer_inn, seller_inn, mult_compare, buyer_operation_code, seller_operation_code,
  buyer_zip, seller_zip
)
values
(
  {2}, '5252', TO_DATE('23.01.2016', 'dd.mm.yyyy'), 1, 1, 31, 32, 33, 8,
  '{0}/0/002', null, null, 1, 
  1, null, '1234567890', '1234567891', 1, null, null,
  {0}, null
);

commit;";

            ExecuteSqlQuery(string.Format(sqlPrepareSovDiscrepancyTemplate, _zip, _discrepancyId_One, _discrepancyId_Two));
        }

        private void RemoveSovInvoiceAll()
        {
            string sqlRemoveSovInvoiceAllTemplate = @"
delete from SOV_INVOICE_BUYER_ALL a where a.row_key_id in
    (select row_key from SOV_INVOICE_ALL sia where sia.declaration_version_id = {0});

delete from SOV_INVOICE_SELLER_ALL a where a.row_key_id in
    (select row_key from SOV_INVOICE_ALL sia where sia.declaration_version_id = {0});

delete from SOV_INVOICE_BUY_ACCEPT_ALL a where a.row_key_id in
    (select row_key from SOV_INVOICE_ALL sia where sia.declaration_version_id = {0});

delete from SOV_INVOICE_OPERATION_ALL a where a.row_key_id in
    (select row_key from SOV_INVOICE_ALL sia where sia.declaration_version_id = {0});

delete from SOV_INVOICE_PAYMENT_DOC_ALL a where a.row_key_id in
    (select row_key from SOV_INVOICE_ALL sia where sia.declaration_version_id = {0});

delete from nds2_mrr_user.SOV_INVOICE_ALL sia where sia.declaration_version_id = {0}; 

commit;";

            ExecuteSqlQuery(string.Format(sqlRemoveSovInvoiceAllTemplate, _zip));
        }

        private void PrepareSovInvoiceAll()
        {
            string sqlPrepareSovInvoiceAllTemplate = @" 
insert into nds2_mrr_user.SOV_INVOICE_ALL
( 
  declaration_version_id, chapter, row_key, ordinal_number, okv_code, create_date,
  receive_date, invoice_num, invoice_date, change_num, change_date, correction_num, correction_date,
  change_correction_num, change_correction_date, seller_invoice_num, seller_invoice_date, broker_inn,
  broker_kpp, deal_kind_code, customs_declaration_num, price_buy_amount, price_buy_nds_amount,
  price_sell, price_sell_in_curr, price_sell_18, price_sell_10, price_sell_0, price_nds_18, price_nds_10,
  price_tax_free, price_total, price_nds_total, diff_correct_decrease, diff_correct_increase, diff_correct_nds_decrease,
  diff_correct_nds_increase, price_nds_buyer, actual_row_key, compare_row_key, compare_algo_id, format_errors,
  logical_errors, seller_agency_info_inn, seller_agency_info_kpp, seller_agency_info_name,
  seller_agency_info_num, seller_agency_info_date, is_import, clarification_key, contragent_key,
  is_dop_list
)
values
( 
  {0}, 8, '{0}/0/001', '001', null, TO_DATE('01.01.2016', 'dd.mm.yyyy'),
  null, 'number 001', TO_DATE('01.01.2016', 'dd.mm.yyyy'), null, null, null, null,
  null, null, 'number 001', TO_DATE('01.01.2016', 'dd.mm.yyyy'), null,
  null, null, null, 1234, 2345, 1, 2, 3, 4, 5, 6, 7,
  8, 9, 10, 11, 12, 13, 14, 15, null, null, null, null,
  null, null, null, null, null, null, null, null, null, null
);

insert into nds2_mrr_user.SOV_INVOICE_BUYER_ALL (row_key_id, buyer_inn, buyer_kpp)
values('{0}/0/001', '1234567890', '123456789');
       
insert into nds2_mrr_user.SOV_INVOICE_SELLER_ALL (row_key_id, invoice_num, seller_inn, seller_kpp)
values('{0}/0/001', '001', '5234567890', '523456789');

insert into nds2_mrr_user.SOV_INVOICE_BUY_ACCEPT_ALL (row_key_id, buy_accept_date)
values('{0}/0/001', TO_DATE('21.02.2016', 'dd.mm.yyyy'));

insert into nds2_mrr_user.SOV_INVOICE_OPERATION_ALL (row_key_id, operation_code)
values('{0}/0/001', '05');

insert into nds2_mrr_user.SOV_INVOICE_PAYMENT_DOC_ALL (row_key_id, doc_num, doc_date)
values('{0}/0/001', '089', TO_DATE('22.02.2016', 'dd.mm.yyyy'));

insert into nds2_mrr_user.SOV_INVOICE_ALL
( 
  declaration_version_id, chapter, row_key, ordinal_number, okv_code, create_date,
  receive_date, invoice_num, invoice_date, change_num, change_date, correction_num, correction_date,
  change_correction_num, change_correction_date, seller_invoice_num, seller_invoice_date, broker_inn,
  broker_kpp, deal_kind_code, customs_declaration_num, price_buy_amount, price_buy_nds_amount,
  price_sell, price_sell_in_curr, price_sell_18, price_sell_10, price_sell_0, price_nds_18, price_nds_10,
  price_tax_free, price_total, price_nds_total, diff_correct_decrease, diff_correct_increase, diff_correct_nds_decrease,
  diff_correct_nds_increase, price_nds_buyer, actual_row_key, compare_row_key, compare_algo_id, format_errors,
  logical_errors, seller_agency_info_inn, seller_agency_info_kpp, seller_agency_info_name,
  seller_agency_info_num, seller_agency_info_date, is_import, clarification_key, contragent_key,
  is_dop_list
)
values
( 
  {0}, 8, '{0}/0/002', '001', null, TO_DATE('02.01.2016', 'dd.mm.yyyy'),
  null, 'number 002', TO_DATE('02.01.2016', 'dd.mm.yyyy'), null, null, null, null,
  null, null, 'number 002', TO_DATE('02.01.2016', 'dd.mm.yyyy'), null,
  null, null, null, 2234, 3345, 21, 22, 23, 24, 25, 26, 27,
  28, 29, 30, 31, 32, 33, 34, 35, null, null, null, null,
  null, null, null, null, null, null, null, null, null, null
);

insert into nds2_mrr_user.SOV_INVOICE_BUYER_ALL (row_key_id, buyer_inn, buyer_kpp)
values('{0}/0/002', '3234567890', '323456789');
       
insert into nds2_mrr_user.SOV_INVOICE_SELLER_ALL (row_key_id, invoice_num, seller_inn, seller_kpp)
values('{0}/0/002', '001', '7234567890', '523456789');

insert into nds2_mrr_user.SOV_INVOICE_BUY_ACCEPT_ALL (row_key_id, buy_accept_date)
values('{0}/0/002', TO_DATE('26.02.2016', 'dd.mm.yyyy'));

insert into nds2_mrr_user.SOV_INVOICE_OPERATION_ALL (row_key_id, operation_code)
values('{0}/0/002', '05');

insert into nds2_mrr_user.SOV_INVOICE_PAYMENT_DOC_ALL (row_key_id, doc_num, doc_date)
values('{0}/0/002', '089', TO_DATE('27.02.2016', 'dd.mm.yyyy'));

commit;";

            ExecuteSqlQuery(string.Format(sqlPrepareSovInvoiceAllTemplate, _zip));
        }

        private void RemoveSeodDeclaration()
        {
            ExecuteSqlQuery(string.Format("delete from nds2_mrr_user.seod_declaration seod where seod.id = {0}; commit;", _seodId));

            ExecuteSqlQuery(string.Format("delete from nds2_mrr_user.seod_declaration seod where seod.sono_code = {0} and seod.decl_reg_num = {1}; commit;",
                _sonoCode, _regNumber));
        }

        private void PrepareSeodDeclaration()
        {
            string sqlPrepareDeclarationHistoryTemplate = @"
insert into nds2_mrr_user.seod_declaration
(
  id,
  nds2_id,
  type,
  sono_code,
  tax_period,
  fiscal_year,
  correction_number,
  decl_reg_num,
  decl_fid,
  tax_payer_type,
  inn,
  kpp,
  insert_date,
  eod_date,
  date_receipt,
  delivery_type,
  id_file,
  is_imported
)
values
(
  {0},
  666777999,
  0,
  '{2}',
  '21',
  '2016',
  1,
  {1},
  '1',
  1,
  '1234567890',
  '123456789',
  sysdate,
  null,
  null,
  0,
  'id_file_999888777666555',
  0
);

commit;";

            ExecuteSqlQuery(string.Format(sqlPrepareDeclarationHistoryTemplate, _seodId, _regNumber, _sonoCode));
        }

        private void RemoveAskAndJrnl()
        {
            ExecuteSqlQuery(string.Format("delete from ASK_DECLANDJRNL a where a.id = {0}; commit;", _askId));
        }

        private void PrepareAskAndJrnl()
        {
            string sqlPrepareDeclarationHistoryTemplate = @"
insert into nds2_mrr_user.ASK_DECLANDJRNL
(
  id,
  zip,
  idfajl,
  type,
  datadok,
  period,
  otchetgod,
  kodno,
  nomkorr,
  naimorg,
  innnp,
  kppnp,
  kpp_effective,
  innreorg,
  kppreorg,
  kpp_effective_reorg,
  inn_contractor,
  nomkorr_rank,
  insert_date
)
values
(
  {0},
  {1},
  '{2}',
  0,
  sysdate,
  '21',
  '2016',
  '5252',
  1,
  'ооо название',
  '1234567890',
  '123456789',
  '111111111',
  '',
  '',
  '',
  '1234567890',
  1,
  sysdate
);

commit;";

            ExecuteSqlQuery(string.Format(sqlPrepareDeclarationHistoryTemplate, _askId, _zip, _idFile));
        }

        private void RemoveQueue()
        {
            ExecuteSqlQuery(string.Format("delete from nds2_mrr_user.ASK_RECLAIM_QUEUE where id = {0}; commit;", _queueId));
        }

        private void PrepareQueue()
        {
            string sqlPrepareDeclarationHistoryTemplate = @"
insert into nds2_mrr_user.ASK_RECLAIM_QUEUE
(
  id,
  zip,
  reg_number,
  sono_code,
  period_code,
  fiscal_year,
  inn,
  kpp,
  kpp_effective,
  contragent_inn,
  contragent_kpp_effective,
  type_discrepancy_select,
  side,
  state,
  is_locked,
  create_date,
  update_date
)
values 
(
  {0},
  {1},
  {2},
  '5252',
  '21',
  '2016',
  '1234567890',
  '123456789',
  '111111111',
  '-',
  '-',
  0,  
  1,
  1,
  0,
  TO_DATE('01.01.2012', 'dd.mm.yyyy'),
  null
);

commit;";

            ExecuteSqlQuery(string.Format(sqlPrepareDeclarationHistoryTemplate, _queueId, _zip, _regNumber));
        }

        private void RemoveReclaim()
        {
            ExecuteSqlQuery(string.Format("delete from nds2_mrr_user.doc d where d.doc_id = {0}; commit;", _docId));
        }

        private void PrepareReclaim()
        {
            string sqlPrepareClaimTemplate = @"
insert into nds2_mrr_user.doc
(
  doc_id, external_doc_num, external_doc_date, ref_entity_id, doc_type, doc_kind,
  sono_code, discrepancy_count, discrepancy_amount, create_date, seod_accepted,
  seod_accept_date, tax_payer_send_date, tax_payer_delivery_date, tax_payer_delivery_method,
  tax_payer_reply_date, close_date, status_date, status, stage, document_body,
  user_comment, deadline_date, prolong_answer_date, process_elapsed, processed,
  inn, kpp_effective, close_reason
) 
values({0}, null, null, {1}, 2, 1, '5252', 5, 5000, sysdate, 0, 
       null, null, null, null, null, null, trunc(sysdate), 10, 1, null, null, null,
       null, null, null, null, null, null);

commit;";

            ExecuteSqlQuery(string.Format(sqlPrepareClaimTemplate, _docId, _regNumber));
        }
    }
}
