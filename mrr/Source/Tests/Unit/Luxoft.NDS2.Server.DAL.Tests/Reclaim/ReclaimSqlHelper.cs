﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Server.DAL.Reclaim;
using NUnit.Framework;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Tests.Reclaim
{
    internal static class ReclaimSqlHelper
    {
        public static OperationResult ExecuteSqlQuery(
            Luxoft.NDS2.Server.DAL.IServiceProvider _helpers, 
            string sqlQuery, 
            List<OracleParameter> oracleParameters)
        {
            var helpAdapter = ReclaimTestingAdapterCreator
                .Create(_helpers);

            string sqlTemplate = "begin {0} end;";

            var result = helpAdapter.ExecuteQuery(string.Format(sqlTemplate, sqlQuery), oracleParameters);

            Assert.AreEqual(result.Status, ResultStatus.Success);

            return result;
        }
    }
}
