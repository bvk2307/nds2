﻿using CommonComponents.Configuration;
using CommonComponents.Instrumentation;
using CommonComponents.Security.Authorization;
using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contracts;
using Rhino.Mocks;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.DAL.Tests.Reclaim
{
    public static class ReclaimMockHelper
    {
        public static IReadOnlyServiceCollection CreateStubServiceContext()
        {
            //--- Mock IConfigurationDataService
            var mockConfiguraionService = MockRepository.GenerateStub<IConfigurationDataService>();

            var _appSettings = new AppSettingsSection();
            _appSettings.Settings.Add(new KeyValueConfigurationElement(Constants.DB_CONFIG_KEY, ConfigurationManager.AppSettings[Constants.DB_CONFIG_KEY]));

            mockConfiguraionService.Expect(p => p.TryGetSection<AppSettingsSection>(
                Arg<ProfileInfo>.Is.Anything, out Arg<AppSettingsSection>.Out(_appSettings).Dummy))
                .Return(true);

            //--- Mock IProfilingEntry
            var mockProfilingEntry = MockRepository.GenerateStub<IProfilingEntry>();

            //--- Mock IInstrumentationService
            var mockInstrumentationService = MockRepository.GenerateStub<IInstrumentationService>();
            mockInstrumentationService.Expect(p => p.Write(Arg<IProfilingEntry>.Is.Anything));
            mockInstrumentationService.Expect(p => p.CreateLogEntry(Arg<LogEntryTemplate>.Is.Anything)).Return(null)
                .WhenCalled(_ =>
                {
                    var template = (LogEntryTemplate)_.Arguments[0];
                    var ret = new LogEntry(template);
                    _.ReturnValue = ret;
                });

            mockInstrumentationService
                .Expect(p => p.CreateProfilingEntry(Arg<TraceEventType>.Is.Anything, Arg<int>.Is.Anything, Arg<string>.Is.Anything, Arg<int>.Is.Anything))
                .Return(mockProfilingEntry);

            //--- Mock IAuthorizationService
            var mockAuthorizationService = MockRepository.GenerateStub<IAuthorizationService>();

            //--- Mock Luxoft.NDS2.Server.Services.Managers.Query.IQueryProvider
            var mockQueryProvider = MockRepository.GenerateStub<Luxoft.NDS2.Server.Services.Managers.Query.IQueryProvider>();

            //--- Mock IReadOnlyServiceCollection (ServiceContext)
            var mockServiceContext = MockRepository.GenerateStub<IReadOnlyServiceCollection>();

            mockServiceContext.Expect(p => p.Get(typeof(IConfigurationDataService))).Return(mockConfiguraionService);
            mockServiceContext.Expect(p => p.Get(typeof(IInstrumentationService))).Return(mockInstrumentationService);
            mockServiceContext.Expect(p => p.Get(typeof(IAuthorizationService))).Return(mockAuthorizationService);
            mockServiceContext.Expect(p => p.Get(typeof(IQueryProvider))).Return(mockQueryProvider);

            mockServiceContext.Expect(p => p.Get<IConfigurationDataService>()).Return(mockConfiguraionService);
            mockServiceContext.Expect(p => p.Get<IInstrumentationService>()).Return(mockInstrumentationService);
            mockServiceContext.Expect(p => p.Get<IAuthorizationService>()).Return(mockAuthorizationService);
            mockServiceContext.Expect(p => p.Get<Luxoft.NDS2.Server.Services.Managers.Query.IQueryProvider>()).Return(mockQueryProvider);

            return mockServiceContext;
        }
    }
}
