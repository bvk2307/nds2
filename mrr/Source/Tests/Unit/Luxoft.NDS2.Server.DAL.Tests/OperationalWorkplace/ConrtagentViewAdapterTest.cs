﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Contragents;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.DAL.Creators;
using Luxoft.NDS2.Server.DAL.OperationalWorkplace;
using Luxoft.NDS2.Server.Services.Helpers;
using NUnit.Framework;
using Oracle.DataAccess.Client;
using System.Collections.Generic;
using Luxoft.NDS2.Server.Common.DataQuery;
using System.Linq;
using Luxoft.NDS2.Server.DAL.Tests.Reclaim;

namespace Luxoft.NDS2.Tests.Dal
{
    [TestFixture]
    public class ConrtagentViewAdapterTest
    {
        /// <summary>
        /// Проверяем работоспособность списка контрагентов определенной декларации НП
        /// </summary>
        [Test]
        public void SelectContragentsListTest()
        {
            var serviceHelpers = new ServiceHelpers(ReclaimMockHelper.CreateStubServiceContext());

            string sqlRemoveQuery = @"delete from KNP_RELATION_SUMMARY t where t.zip = 9991485940166959; commit;";

            ExecuteSqlQuery(serviceHelpers, sqlRemoveQuery, new List<OracleParameter>());

            string sqlPrepareQuery = @"
insert into KNP_RELATION_SUMMARY (inn, kpp_effective, type_code, period_code, fiscal_year, inn_contractor, inn_contractor_declarant, kpp_effective_contractor,
gap_amount_8, gap_quantity_8, other_amount_8, other_quantity_8, gap_amount_9, gap_quantity_9, other_amount_9, other_quantity_9, gap_amount_10, gap_quantity_10,
other_amount_10, other_quantity_10, gap_amount_11, gap_quantity_11, other_amount_11, other_quantity_11, gap_amount_12, gap_quantity_12, other_amount_12,
other_quantity_12, contractor_kpp, contractor_name, declaration_quarter_id, zip, declared_tax_amnt, calculated_tax_amnt_r9, calculated_tax_amnt_r12,
chapter10_amount_nds, chapter11_amount_nds, declared_tax_amnt_total, chapter8_amount, chapter8_count, chapter9_amount, chapter9_count,
chapter9_count_effective, chapter10_amount, chapter10_count, chapter11_amount, chapter11_count, chapter12_amount, chapter12_count)
values ('7810346931', '111111111', 0, '76', '2015', '0810346930', '0810346930', '111111111',
100, 1, null, 0, null, 0, null, 0, null, 0,
null, 0, null, 0, null, 0, null, 0, null,
0, '7810AZ001', 'ООО Дрозд', '220157810346931', 9991485940166959, 100, 100, 0,
200, 0, 0, 1000, 1, 1000, 1,
1, 2000, 1, 2000, 1, 0, 0);
insert into KNP_RELATION_SUMMARY (inn, kpp_effective, type_code, period_code, fiscal_year, inn_contractor, inn_contractor_declarant, kpp_effective_contractor,
gap_amount_8, gap_quantity_8, other_amount_8, other_quantity_8, gap_amount_9, gap_quantity_9, other_amount_9, other_quantity_9, gap_amount_10, gap_quantity_10,
other_amount_10, other_quantity_10, gap_amount_11, gap_quantity_11, other_amount_11, other_quantity_11, gap_amount_12, gap_quantity_12, other_amount_12,
other_quantity_12, contractor_kpp, contractor_name, declaration_quarter_id, zip, declared_tax_amnt, calculated_tax_amnt_r9, calculated_tax_amnt_r12,
chapter10_amount_nds, chapter11_amount_nds, declared_tax_amnt_total, chapter8_amount, chapter8_count, chapter9_amount, chapter9_count,
chapter9_count_effective, chapter10_amount, chapter10_count, chapter11_amount, chapter11_count, chapter12_amount, chapter12_count)
values ('7810346931', '111111111', 0, '76', '2015', '1001270919', '1001270919', '111111111',
null, 0, null, 0, null, 0, null, 0, null, 0,
null, 0, 301.03, 3, null, 0, null, 0, null,
0, '100101001', 'ООО ТУКАН', '220157810346931', 9991485940166959, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0);
commit;";

            ExecuteSqlQuery(serviceHelpers, sqlPrepareQuery, new List<OracleParameter>());

            int chapter = 8;

            long zip = 9991485940166959;

            var conditions = new QueryConditions();
            conditions.Sorting.Add(new ColumnSort() { ColumnKey = TypeHelper<ContragentSummary>.GetMemberName(t => t.CHAPTER8_AMOUNT ) });

            using (var connectionFactory = new SingleConnectionFactory(serviceHelpers))
            {
                var searchCriteria = conditions.Filter.ToFilterGroup();

                var mappingQuantity = new Dictionary<int, string>()
                {
                    { 8, TypeHelper<ContragentSummary>.GetMemberName(x => x.CHAPTER8_COUNT) },
                    { 9, TypeHelper<ContragentSummary>.GetMemberName(x => x.CHAPTER9_COUNT) },
                    { 10, TypeHelper<ContragentSummary>.GetMemberName(x => x.CHAPTER10_COUNT) },
                    { 11, TypeHelper<ContragentSummary>.GetMemberName(x => x.CHAPTER11_COUNT) },
                    { 12, TypeHelper<ContragentSummary>.GetMemberName(x => x.CHAPTER12_COUNT) }
                };

                var quantityChapter = FilterExpressionCreator.Create(
                    mappingQuantity[chapter],
                    ColumnFilter.FilterComparisionOperator.GreaterThan,
                    0);

                searchCriteria
                    .WithExpression(quantityChapter);

                var zipExpression = FilterExpressionCreator.Create(
                    TypeHelper<ContragentSummary>.GetMemberName(x => x.DECLARATION_VERSION_ID),
                    ColumnFilter.FilterComparisionOperator.Equals,
                    zip);

                searchCriteria
                    .WithExpression(zipExpression);

                var operationalWorkplaceContractorAdapter = OperationalWorkplaceContractorAdapterCreator
                    .OperationalWorkplaceContractorAdapter(serviceHelpers, connectionFactory);

                int resultTotalMatches = operationalWorkplaceContractorAdapter.Count(searchCriteria);

                Assert.AreEqual(resultTotalMatches, 1);

                var resultRows = operationalWorkplaceContractorAdapter.Search(
                        searchCriteria,
                        conditions.Sorting,
                        conditions.PaginationDetails.RowsToSkip,
                        (uint)conditions.PaginationDetails.RowsToTake).ToList();

                Assert.AreEqual(resultRows.Count, 1);
            }

            sqlRemoveQuery = @"delete from KNP_RELATION_SUMMARY t where t.zip = 9991485940166959; commit;";

            ExecuteSqlQuery(serviceHelpers, sqlRemoveQuery, new List<OracleParameter>());
        }

        private OperationResult ExecuteSqlQuery(Luxoft.NDS2.Server.DAL.IServiceProvider _helpers, string sqlQuery, List<OracleParameter> oracleParameters)
        {
            return ReclaimSqlHelper.ExecuteSqlQuery(_helpers, sqlQuery, oracleParameters);
        }
    }
}
