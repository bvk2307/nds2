﻿using Luxoft.NDS2.Common.Contracts.DTO.ExportExcel;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Tests.__Stubs;
using NUnit.Framework;
using System;
using Luxoft.NDS2.Server.TaskScheduler.Excel;

namespace Luxoft.NDS2.Tests.Server
{
    [TestFixture]
    [Category("Manual")]
    class ExportExcelTest
    {
        readonly ServiceContext _services = new ServiceContext();

        class ExportCallBack : IExportSelectionDiscrepancyCallback
        {
            public void LogExportedRows(int count)
            {
                Console.WriteLine(count);
            }

            public bool CheckCanceled()
            {
                return false;
            }
        }

        [Test]
        public void Export()
        {
            var helper = new ServiceHelpers(_services);
            var item = new ExportExcelItem
            {
                QUEUE_ITEM_ID = 1,
                SELECTION_ID = 11
            };
            
            var export = new ExportSelectionDiscrepancy(helper, item);
            
            var packageAdapter = TableAdapterCreator.ExportExcelPackageAdapter(helper);

            var fileName = string.Format("{0}Export_{2:yyyy-MM-dd}_{1}.xlsx", packageAdapter.GetExportPath(), item.QUEUE_ITEM_ID, DateTime.Now);

            export.DoExport(fileName, new ExportCallBack());
        }
    }
}
