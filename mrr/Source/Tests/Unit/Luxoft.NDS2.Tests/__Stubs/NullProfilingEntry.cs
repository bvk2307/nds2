﻿using CommonComponents.Instrumentation;
using System;
using System.Diagnostics;

namespace Luxoft.NDS2.Tests.__Stubs
{
    public class NullProfilingEntry : IProfilingEntry
    {
        public NullProfilingEntry()
        {
            Duration = new TimeSpan(0);
            GlobalCorrelationUId = Guid.NewGuid();
            GlobalGroupUId = Guid.NewGuid();
        }

        public void Add(IRealtimeMonitoringNamedItem snapshot)
        {
            return;
        }

        public void ChangeEventType(System.Diagnostics.TraceEventType eventType)
        {
            return;
        }

        public long CorrelationId
        {
            get;
            set;
        }

        public IRealtimeMonitoringPropertiesItem CreateCheckpoint(string name, System.Diagnostics.TraceEventType eventType, string description)
        {
            throw new System.NotImplementedException();
        }

        public IRealtimeMonitoringPropertiesItem CreateCheckpoint(string name, string description)
        {
            throw new System.NotImplementedException();
        }

        public IRealtimeMonitoringCompareContainer CreateCompareContainer(string name, string description)
        {
            throw new System.NotImplementedException();
        }

        public TimeSpan Duration
        {
            get;
            set;
        }

        public System.DateTime EventTime
        {
            get;
            set;
        }

        public TraceEventType EventType
        {
            get 
            { 
                throw new System.NotImplementedException(); 
            }
        }

        public TType GetConfiguredParameterOrDefault<TType>(string settingsName, string name)
        {
            throw new System.NotImplementedException();
        }

        public TType GetConfiguredParameterOrDefault<TType>(string name)
        {
            throw new System.NotImplementedException();
        }

        public System.Guid GlobalCorrelationUId
        {
            get;
            set;
        }

        public System.Guid GlobalGroupUId
        {
            get;
            set;
        }

        public System.Guid GlobalUId
        {
            get
            {
                throw new System.NotImplementedException();
            }
            set
            {
                throw new System.NotImplementedException();
            }
        }

        public long GroupId
        {
            get
            {
                throw new System.NotImplementedException();
            }
            set
            {
                throw new System.NotImplementedException();
            }
        }

        public bool HasConfiguredParameter(string settingsName, string name)
        {
            throw new System.NotImplementedException();
        }

        public bool HasConfiguredParameter(string name)
        {
            throw new System.NotImplementedException();
        }

        public bool HasSettings(string settingsName)
        {
            throw new System.NotImplementedException();
        }

        public bool HasSettings()
        {
            throw new System.NotImplementedException();
        }

        public bool IsEnabled
        {
            get { throw new System.NotImplementedException(); }
        }

        public bool IsEnqueued
        {
            get { throw new System.NotImplementedException(); }
        }

        public bool IsRealMessage
        {
            get { throw new System.NotImplementedException(); }
        }

        public int MessageLevel
        {
            get { throw new System.NotImplementedException(); }
        }

        public int MessagePriority
        {
            get { throw new System.NotImplementedException(); }
        }

        public string MessageText
        {
            get;
            set;
        }

        public string MessageType
        {
            get
            {
                throw new System.NotImplementedException();
            }
            set
            {
                throw new System.NotImplementedException();
            }
        }

        public int MonitoringLevel
        {
            get { throw new System.NotImplementedException(); }
        }

        public long OrderId
        {
            get { throw new System.NotImplementedException(); }
        }

        public ProfilingSensitiveDataLevel SensitiveDataLevel
        {
            get { throw new System.NotImplementedException(); }
        }

        public bool TryGetConfiguredParameter<TType>(string settingsName, string name, out TType value)
        {
            throw new System.NotImplementedException();
        }

        public bool TryGetConfiguredParameter<TType>(string name, out TType value)
        {
            throw new System.NotImplementedException();
        }

        public ProfilingSensitiveDataLevel UpgradeSensitiveDataLevel(ProfilingSensitiveDataLevel level)
        {
            throw new System.NotImplementedException();
        }
    }
}
