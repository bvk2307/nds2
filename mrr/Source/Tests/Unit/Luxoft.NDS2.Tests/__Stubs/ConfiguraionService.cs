﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CommonComponents.Configuration;
using Luxoft.NDS2.Common.Contracts;

namespace Luxoft.NDS2.Tests.__Stubs
{
    public class ConfiguraionService : IConfigurationDataService
    {
        private AppSettingsSection _appSettings = new AppSettingsSection();


        public ConfiguraionService()
        {
            _appSettings.Settings.Add(new KeyValueConfigurationElement(Constants.DB_CONFIG_KEY, ConfigurationManager.AppSettings[Constants.DB_CONFIG_KEY]));
            _appSettings.Settings.Add(new KeyValueConfigurationElement("SovServiceUrl", ConfigurationManager.AppSettings["SovServiceUrl"]));

            
        }

        #region Implementation of IConfigurationDataService

        public bool HasProfile(ProfileInfo profileInfo)
        {
            throw new NotImplementedException();
        }

        public TSection GetSection<TSection>(ProfileInfo profileInfo) where TSection : ConfigurationSection
        {
            if (typeof(TSection) == typeof(AppSettingsSection))
            {
                return _appSettings as TSection;
                
            }
            return null;
        }

        public TSection GetSection<TSection>(ProfileInfo profileInfo, Predicate<TSection> predicate) where TSection : ConfigurationSection
        {
            throw new NotImplementedException();
        }

        public IList<TSection> GetSectionList<TSection>(ProfileInfo profileInfo) where TSection : ConfigurationSection
        {
            throw new NotImplementedException();
        }

        public IList<TSection> GetSectionList<TSection>(ProfileInfo profileInfo, Predicate<TSection> predicate) where TSection : ConfigurationSection
        {
            throw new NotImplementedException();
        }

        public bool TryGetSection<TSection>(ProfileInfo profileInfo, out TSection section) where TSection : ConfigurationSection
        {
            section = null;

            if (typeof(TSection) == typeof(AppSettingsSection))
            {
                section = _appSettings as TSection;
                return true;
            }

            return false;
        }

        public bool TryGetSection<TSection>(ProfileInfo profileInfo, Predicate<TSection> predicate, out TSection section) where TSection : ConfigurationSection
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
