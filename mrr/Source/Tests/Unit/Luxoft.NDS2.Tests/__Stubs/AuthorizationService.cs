﻿using CommonComponents.Security.Authorization;
using System;
using System.Collections.Generic;
using System.Security.Principal;

namespace Luxoft.NDS2.Tests.__Stubs
{
    public class AuthorizationService : IAuthorizationService
    {
        public bool Authorize(string userIdentifier, string context)
        {
            throw new NotImplementedException();
        }

        public bool[] Authorize(string userIdentifier, IList<string> contexts)
        {
            throw new NotImplementedException();
        }

        public IList<string> GetStructContexts(string userIdentifier, string context)
        {
            throw new NotImplementedException();
        }

        public IList<string> GetAssignments(string context)
        {
            throw new NotImplementedException();
        }

        public IList<PermissionObject> GetSubsystemPermissions(string userIdentifier, string subsystemName)
        {
            throw new NotImplementedException();
        }
    }
}
