﻿using System;
using Luxoft.NDS2.Server.Services.Managers.Query;

namespace Luxoft.NDS2.Tests.__Stubs
{
    class MockQueryProvider : IQueryProvider
    {
        public string GetQueryText(string scope, string queryName)
        {
            if (scope == "Selection.Discrepancy" && queryName == "All")
            {
                return @"select * from V$SELECTION_DISCREPANCY vw where selectionid = :pSelectionId and {0} {1}";
            }
            throw new NotSupportedException(string.Format("Шаблон не поддерживается MockQueryProvider: {0}\\{1}", scope, queryName));
        }
    }
}
