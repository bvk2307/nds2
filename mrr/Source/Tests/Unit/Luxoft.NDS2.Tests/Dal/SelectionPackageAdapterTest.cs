﻿using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.DAL.Selections;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Tests.__Stubs;
using NUnit.Framework;
using Oracle.DataAccess.Client;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Tests.Dal
{
    [TestFixture]
    class SelectionPackageAdapterTest
    {
        ServiceContext _services = new ServiceContext();

        /// <summary>
        /// Проверяем работоспособность получения списка параметров фильтра
        /// </summary>
        [Test]
        public void GetFilterParametersTest()
        {
            IServiceProvider _helpers = new ServiceHelpers(_services);

            var helpAdapter = TableAdapterCreator.DiscrepancySelectionAdpater(new ServiceHelpers(_services));

            string sqlTemplate = "begin {0} end;";

            string sqlRemoveQuery = @"
delete from NDS2_MRR_USER.SELECTION_FILTER_OPTION where parameter_id = 999001;
delete from NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY where parameter_id = 999001;
delete from NDS2_MRR_USER.SELECTION_FILTER_PARAMETER where id = 999001;
commit;";

            string sqlPrepareQuery = @"
insert into NDS2_MRR_USER.SELECTION_FILTER_PARAMETER (id, editor_name, aggregate_name, editor_area_id, editor_type_id, editor_model_id)
values (999001, 'editor_name_01', 'aggregate_name_01', 1, 1, 1);
commit;";

            var oracleParametersEmpty = new List<OracleParameter>();

            var resultHelpFirst = helpAdapter.ExecuteQuery(string.Format(sqlTemplate, sqlRemoveQuery), oracleParametersEmpty);
            Assert.AreEqual(resultHelpFirst.Status, Common.Contracts.DTO.ResultStatus.Success);

            var resultHelpSecond = helpAdapter.ExecuteQuery(string.Format(sqlTemplate, sqlPrepareQuery), oracleParametersEmpty);
            Assert.AreEqual(resultHelpSecond.Status, Common.Contracts.DTO.ResultStatus.Success);

            List<SelectionParameter> result = null;
            using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(_helpers))
            {
                result = SelectionsAdapterCreator
                    .SelectionFilterAdapter(_helpers, connectionFactory)
                    .GetFilterParameters().ToList();
            }

            helpAdapter.ExecuteQuery(string.Format(sqlTemplate, sqlRemoveQuery), oracleParametersEmpty);

            Assert.GreaterOrEqual(result.Count(), 0);

            var selectionParameter = result.Where(p => p.Id == 999001).FirstOrDefault();

            Assert.IsNotNull(selectionParameter);
            Assert.AreEqual(selectionParameter.Name, "aggregate_name_01");
        }

        /// <summary>
        /// Проверяем работоспособность получения списка параметров фильтра c метаданными
        /// </summary>
        [Test]
        public void GetFilterEditorParametersTest()
        {
            IServiceProvider _helper = new ServiceHelpers(_services);
            var helpAdapter = TableAdapterCreator.DiscrepancySelectionAdpater(_helper);

            string sqlTemplate = "begin {0} end;";

            string sqlRemoveQuery = @"
delete from NDS2_MRR_USER.SELECTION_FILTER_OPTION where parameter_id = 999001;
delete from NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY where parameter_id = 999001;
delete from NDS2_MRR_USER.SELECTION_FILTER_PARAMETER where id = 999001;
commit;";

            string sqlPrepareQuery = @"
insert into NDS2_MRR_USER.SELECTION_FILTER_PARAMETER (id, editor_name, aggregate_name, editor_area_id, editor_type_id, editor_model_id)
values (999001, 'фильтр 01', 'filter_01', 1, 1, 1);

insert into NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY(parameter_id, is_manual)
values(999001, 1);

commit;";

            var oracleParametersEmpty = new List<OracleParameter>();

            var resultHelpFirst = helpAdapter.ExecuteQuery(string.Format(sqlTemplate, sqlRemoveQuery), oracleParametersEmpty);
            Assert.AreEqual(resultHelpFirst.Status, Common.Contracts.DTO.ResultStatus.Success);

            var resultHelpSecond = helpAdapter.ExecuteQuery(string.Format(sqlTemplate, sqlPrepareQuery), oracleParametersEmpty);
            Assert.AreEqual(resultHelpSecond.Status, Common.Contracts.DTO.ResultStatus.Success);

            List<EditorParameter> result = new List<EditorParameter>();
            using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(_helper))
            {
                result = SelectionsAdapterCreator
                    .SelectionFilterAdapter(_helper, connectionFactory)
                    .GetFilterEditorParameters(SelectionType.Hand).ToList();
            }

            helpAdapter.ExecuteQuery(string.Format(sqlTemplate, sqlRemoveQuery), oracleParametersEmpty);

            Assert.GreaterOrEqual(result.Count(), 0);

            var editorParameter = result.Where(p => p.Id == 999001).FirstOrDefault();

            Assert.IsNotNull(editorParameter);
            Assert.AreEqual(editorParameter.Name, "фильтр 01");
            Assert.AreEqual(editorParameter.AggregateName, "filter_01");
            Assert.AreEqual(editorParameter.ViewType, EditorType.RegionEditor);
            Assert.AreEqual(editorParameter.Area, EditorArea.BuyerSide);
            Assert.AreEqual(editorParameter.ModelType, ParameterType.RangeOfIntModel);
        }

        /// <summary>
        /// Проверяем работоспособность получения списка опций параметров фильтра
        /// </summary>
        [Test]
        public void GetFilterParameterOptionsTest()
        {
            var _helper = new ServiceHelpers(_services);
            var helpAdapter = TableAdapterCreator.DiscrepancySelectionAdpater(_helper);

            string sqlTemplate = "begin {0} end;";

            string sqlRemoveQuery = @"
delete from NDS2_MRR_USER.SELECTION_FILTER_OPTION where parameter_id = 999001;
delete from NDS2_MRR_USER.SELECTION_FILTER_APPLICABILITY where parameter_id = 999001;
delete from NDS2_MRR_USER.SELECTION_FILTER_PARAMETER where id = 999001;
commit;";

            string sqlPrepareQuery = @"
insert into NDS2_MRR_USER.SELECTION_FILTER_PARAMETER (id, editor_name, aggregate_name, editor_area_id, editor_type_id, editor_model_id)
values (999001, 'фильтр 01', 'filter_01', 1, 1, 1);

insert into NDS2_MRR_USER.SELECTION_FILTER_OPTION(id, parameter_id, value, title)
values (999001, 999001, 'value_01', 'title_01');

commit;";

            var oracleParametersEmpty = new List<OracleParameter>();

            var resultHelpFirst = helpAdapter.ExecuteQuery(string.Format(sqlTemplate, sqlRemoveQuery), oracleParametersEmpty);
            Assert.AreEqual(resultHelpFirst.Status, Common.Contracts.DTO.ResultStatus.Success);

            var resultHelpSecond = helpAdapter.ExecuteQuery(string.Format(sqlTemplate, sqlPrepareQuery), oracleParametersEmpty);
            Assert.AreEqual(resultHelpSecond.Status, Common.Contracts.DTO.ResultStatus.Success);

            var result = new List<KeyValuePair<string, string>>();
            using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(_helper))
            {
                result = SelectionsAdapterCreator
                    .SelectionFilterAdapter(_helper, connectionFactory)
                    .GetFilterParameterOptions(999001).ToList();
            }

            helpAdapter.ExecuteQuery(string.Format(sqlTemplate, sqlRemoveQuery), oracleParametersEmpty);

            Assert.GreaterOrEqual(result.Count(), 0);

            var filterOptions = result.Where(p => p.Key == "value_01").FirstOrDefault();

            Assert.IsNotNull(filterOptions);
            Assert.AreEqual(filterOptions.Value, "title_01");
        }
       
    }
}
