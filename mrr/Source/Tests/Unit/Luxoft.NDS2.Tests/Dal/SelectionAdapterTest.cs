﻿using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.DAL.Selections;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Tests.__Stubs;
using NUnit.Framework;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Luxoft.NDS2.Tests.Dal
{
    [TestFixture]
    public class SelectionAdapterTest
    {
        ServiceContext _services = new ServiceContext();

        /// <summary>
        /// Проверяем работоспособность получения фильтра выборки
        /// </summary>
        [Test]
        public void GetSelectionFilterContentTest()
        {
            var _helper = new ServiceHelpers(_services);
            var helpAdapter = TableAdapterCreator.DiscrepancySelectionAdpater(new ServiceHelpers(_services));

            string sqlTemplate = "begin {0} end;";

            string sqlRemoveQuery = @"
delete from NDS2_MRR_USER.SELECTION_FILTER where id = 999001;
commit;";

            string sqlPrepareQuery = @"
insert into SELECTION_FILTER (id, selection_id, filter)
values(999001, 999001, '');
commit;";

            var oracleParametersEmpty = new List<OracleParameter>();

            helpAdapter.ExecuteQuery(string.Format(sqlTemplate, sqlRemoveQuery), oracleParametersEmpty);
            helpAdapter.ExecuteQuery(string.Format(sqlTemplate, sqlPrepareQuery), oracleParametersEmpty);

            Filter filter = null;
            using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(_helper))
            {
                filter = SelectionsAdapterCreator
                    .SelectionFilterAdapter(_helper, connectionFactory)
                    .GetSelectionFilterContent(999001);
            }

            helpAdapter.ExecuteQuery(string.Format(sqlTemplate, sqlRemoveQuery), oracleParametersEmpty);

            Assert.IsNotNull(filter);
        }
    }
}
