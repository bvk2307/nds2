﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Results;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.Services.DataAccess;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Tests.__Stubs;
using NUnit.Framework;
using Oracle.DataAccess.Client;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Tests.Dal
{
    [TestFixture]
    [Category("Manual")]
    public class DalManagerTest
    {
        ServiceContext _services = new ServiceContext();

        [Test]
        public void EchoTest()
        {
            DalManager mgr = new DalManager(new ServiceHelpers(_services));
            Console.Write(mgr.TestConnection());
        }

        [Test]
        public void GenerateUniqueNameTest()
        {
            var mgr = TableAdapterCreator.SelectionPackageAdapter(new ServiceHelpers(_services));
            string userName = @"non";
            var name = mgr.GenerateUniqueName(userName, SelectionType.Hand);

            Assert.AreEqual("Выборка 1", name);

        }

        private void CleanData(string tableName, string col, string mask)
        {
            using (OracleConnection conn = new OracleConnection(ConfigurationManager.AppSettings[Constants.DB_CONFIG_KEY]))
            {
                conn.Open();

                OracleCommand cmd = new OracleCommand(string.Format("delete from {0} where {1} like '{2}'", tableName, col, mask));
                cmd.Connection = conn;

                cmd.ExecuteNonQuery();
            }
        }

        #region История активности

        [Test]
        public void GetActionHistory()
        {
            var mgr = TableAdapterCreator.SelectionPackageAdapter(new ServiceHelpers(_services));
            var res1 = mgr.GetActionsHistory(1);
        }

        #endregion История активности
    }
}
