﻿using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Tests.__Stubs;
using Luxoft.NDS2.Tests.Dal.DataGeneration;
using NUnit.Framework;
using System.Transactions;

namespace Luxoft.NDS2.Tests.Dal
{
    [TestFixture]
    [Category("Manual")]
    public class InspectorDeclarationAssignmentAdapterTest
    {
        private const string FakeSonoCode = "AAAA";

        ///<summary>
        /// Проверяет то, что декларации инспектора поставленного на паузу, отправляются в 
        /// список не распределенных
        /// </summary>
        [Test]
        public void DeclarationsOfPausedInspectorMovedToNonAssignedTest()
        {
            using (new TransactionScope())
            {
                var services = new ServiceContext();
                var helpers = new ServiceHelpers(services);
                var sql = new SqlTestHelper(helpers);
                sql.RunInitialScript("InspectorDeclarationAssingmentAdapter", "DeclarationlAssinmentCommonTest");
                sql.RunInitialScript("InspectorDeclarationAssingmentAdapter", "DeclarationsOfPausedInspectorMovedToNonAssignedTest");
                var adapter = TableAdapterCreator.InspectorDeclarationAssingmentAdapter(helpers);
                adapter.DistributeIndividualDeclarations(FakeSonoCode);
                var nonAssigned = sql.GetData(
                    "select count(*) as cnt from NDS2_MRR_USER.IDA_NON_ASSIGNED_DECLS d where d.declaration_id = -1",
                    (r) => r.GetInt32("cnt"));
                Assert.AreEqual(1, nonAssigned[0]);
            }
        }

        ///<summary>
        /// Проверяет то, что декларации инспектора не поставленного на паузу, отправляются в 
        /// список распределенных
        /// </summary>
        [Test]
        public void DeclarationsOfNonPausedInspectorMovedToAssignedTest()
        {
            using (new TransactionScope())
            {
                var services = new ServiceContext();
                var helpers = new ServiceHelpers(services);
                var sql = new SqlTestHelper(helpers);
                sql.RunInitialScript("InspectorDeclarationAssingmentAdapter", "DeclarationlAssinmentCommonTest");
                sql.RunInitialScript("InspectorDeclarationAssingmentAdapter", "DeclarationsOfNonPausedInspectorMovedToAssignedTest");
                var adapter = TableAdapterCreator.InspectorDeclarationAssingmentAdapter(helpers);
                adapter.DistributeIndividualDeclarations(FakeSonoCode);
                var nonAssigned = sql.GetData("select count(*) as cnt from NDS2_MRR_USER.IDA_NON_ASSIGNED_DECLS d where d.declaration_id = -1",
    (r) => r.GetInt32("cnt"));
                var assigned = sql.GetData("select count(*) as cnt from NDS2_MRR_USER.DECLARATION_OWNER d where d.declaration_id = -1",
(r) => r.GetInt32("cnt"));
                Assert.AreEqual(1, assigned[0]);
                Assert.AreEqual(0, nonAssigned[0]);
            }
        }
    }
}
