﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Server.Common.Adapters;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.Common.DTO;
using System.Collections.Generic;

namespace Luxoft.NDS2.Tests.Dal.Stubs
{
    public class SovInvoiceAdapter : ISovInvoiceAdapter
    {
        public CountResult Count(int chapter, FilterExpressionBase searchBy, IEnumerable<string> summaryFields = null)
        {
            return new CountResult() { Count = 100 };
        }

        public IEnumerable<Invoice> Search(int chapter, DataQueryContext queryContext)
        {
            return new List<Invoice>()
            {
                new Invoice()
            };
        }

        public Dictionary<int, ImpalaOption> SearchOptions()
        {
            return new Dictionary<int, ImpalaOption>();
        }
    }
}
