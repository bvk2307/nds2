﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Server.Common.Adapters;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Tests.Dal.Stubs
{
    internal class SovInvoiceRequestAdapter : ISovInvoiceRequestAdapter, IClientProxy
    {
        private readonly List<Tuple<long, int, SovInvoiceRequest>> _testData;

        private long _idsequence;

        public SovInvoiceRequestAdapter(List<Tuple<long, int, SovInvoiceRequest>> testData)
        {
            _testData = testData;
            _idsequence = testData.Any() ? testData.Max(x => x.Item3.Id) : 0;
        }

        public SovInvoiceRequest SearchById(long requestId)
        {
            return _testData.First(x => x.Item3.Id == requestId).Item3;
        }

        public List<SovInvoiceRequest> SearchByDeclaration(long declarationId, int partition)
        {
            return _testData.Where(x => x.Item1 == declarationId && x.Item2 == partition).Select(x => x.Item3).ToList();
        }

        public long RequestInvoiceData(List<KeyValuePair<int, string>> invoices, bool loadMismatches = false, bool loadOnlyTargets = false)
        {
            throw new System.NotImplementedException();
        }

        public long RequestDiscrepancies(SelectionFilter filter, long selectionId)
        {
            throw new System.NotImplementedException();
        }

        public long RequestBookData(string inn, int period, int year, int partitionNumber, int correctionNumber = 0, int priority = 1)
        {
            throw new System.NotImplementedException();
        }

        public long RequestNdsDiscrepancies(Common.Contracts.DTO.Business.Declaration.DeclarationRequestData declaration, int discrepancyType, int discrepancyKind)
        {
            throw new System.NotImplementedException();
        }

        public long RequestPyramidData(Common.Contracts.DTO.Pyramid.Request request)
        {
            throw new System.NotImplementedException();
        }

        public long RequestReportLogicalChecks(int year, List<string> periods, string date, List<string> inspections)
        {
            throw new System.NotImplementedException();
        }

        public long RequestContragentData(string inn, int period, int year, int correctionNumber = 0)
        {
            throw new System.NotImplementedException();
        }

        public long RequestContragentParamsData(string inn, string contractorInn, int period, int year, int correctionNumber = 0)
        {
            throw new System.NotImplementedException();
        }

        public long RequestNavigatorData(Common.Contracts.DTO.Navigator.Request request)
        {
            throw new System.NotImplementedException();
        }

        public long RequestDeclarationChapter(long declarationId, int partition, int priority)
        {
            _idsequence++;
            return _idsequence;
        }
    }
}
