﻿using Luxoft.NDS2.Server.DAL.Helpers;
using Luxoft.NDS2.Server.DAL.Implementation;
using Luxoft.NDS2.Server.Services.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;

namespace Luxoft.NDS2.Tests.Dal.DataGeneration
{
    public class SqlTestHelper
    {
        private readonly ServiceHelpers _service;

        public SqlTestHelper(ServiceHelpers service)
        {
            _service = service;
        }

        public void RunInitialScript(string adapterName, string testName)
        {
            var path = Path.Combine("Dal", "DataGeneration", "InitialScripts", adapterName, testName + ".sql");
            var sql = File.ReadAllText(path);
            new CommandExecuter(_service).TryExecute(new CommandContext
                                                         {
                                                             Text = sql,
                                                             Type = CommandType.Text,
                                                             Parameters = new List<Oracle.DataAccess.Client.OracleParameter>()
                                                         });
        }

        public List<T> GetData<T>(string sql, Func<ResultCommandHelper, T> objectBuilder)
        {
            var list = new List<T>();
            new CommandExecuter(_service).TryRead(new CommandContext
                                                      {
                                                          Text = sql,
                                                          Type = CommandType.Text,
                                                          Parameters = new List<Oracle.DataAccess.Client.OracleParameter>()
                                                      }, r =>
                                                             {
                                                                 while (r.Read())
                                                                 {
                                                                     list.Add(objectBuilder(r));
                                                                 }
                                                                 return list;
                                                             });
            return list;
        }
    }
}