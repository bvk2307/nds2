﻿using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Server.Common.Adapters;
using Luxoft.NDS2.Server.Common.DataQuery;
using Luxoft.NDS2.Server.DAL.DeclarationsRequest;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Tests.__Stubs;
using NUnit.Framework;

namespace Luxoft.NDS2.Tests.Dal
{
    [TestFixture]
    class DeclarationAdapterTest
    {
        ServiceContext _services = new ServiceContext();

        /// <summary>
        /// Проверяем работоспособность списка деклараций
        /// </summary>
        [Test]
        public void DeclarationListTest()
        {
            var conditions = new QueryConditions();
            conditions.Sorting.Add(new ColumnSort { ColumnKey = TypeHelper<DeclarationSummary>.GetMemberName(t => t.INN) });

            ISearchAdapter<DeclarationSummary> actor = new ServiceHelpers(_services).GetDeclarationsRequestAdapter();

            IEnumerable<DeclarationSummary> result = actor.Search(conditions.Filter.ToFilterGroup(), conditions.Sorting, 0, 200);

            Assert.GreaterOrEqual(result.Count(), 0);
        }


    }
}
