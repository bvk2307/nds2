﻿using System;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.DAL.Packages;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Tests.__Stubs;
using NUnit.Framework;

namespace Luxoft.NDS2.Tests.Dal.Packages
{
    [TestFixture]
    [Category("Manual")]
    class ExportExcelPackageAdapterTest
    {
        readonly ServiceContext _services = new ServiceContext();

        [Test]
        public void Test()
        {
            var adapter = TableAdapterCreator.ExportExcelPackageAdapter(new ServiceHelpers(_services));
            var id = adapter.EnqueueSelectionDiscrepancy(42, "Test_user_sid", "A fairy xml");
            var item1 = adapter.FindById(id);
            var item2 = adapter.Dequeue("Test_process_id");
            adapter.Update(item2.QUEUE_ITEM_ID, ExportExcelState.Success, 5, "text.txt", "");
            var item3 = adapter.FindById(item2.QUEUE_ITEM_ID);

            Assert.AreEqual(id, item1.QUEUE_ITEM_ID);
            Assert.AreEqual(id, item2.QUEUE_ITEM_ID);
            Assert.AreEqual(item2.STARTED_BY_ID, "Test_process_id");
            Assert.AreNotEqual(item3.COMPLETED, null);
        }
    }
}
