﻿using System;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.DTO.UserTask;
using Luxoft.NDS2.Server.Services;
using Luxoft.NDS2.Server.Services.EfficiencyMonitoring;
using Luxoft.NDS2.Tests.__Stubs;
using NUnit.Framework;
using Luxoft.NDS2.Server.Services.UserTasks;

namespace Luxoft.NDS2.Tests.Services.UserTask
{

    [TestFixture]
    [Category("Manual")]
    class UserTaskServiceTest
    {
        [Test]
        public void GetUserTaskListSummaryTest()
        {
            ServiceContext serviceContext = new ServiceContext();

            var service = new UserTaskService(serviceContext);
            var data = service.GetSummaryList();

            Type t = typeof(Summary);

            Console.WriteLine(data.Status);


            foreach (var userTaskData in data.Result)
            {
                Console.WriteLine(new string('-', 10));
                foreach (var propertyInfo in t.GetProperties())
                {
                    Console.WriteLine("{0} - {1}", propertyInfo.Name, propertyInfo.GetValue(userTaskData));
                }
            }


            Assert.IsTrue(true);

        }

        [Test]
        public void GetUserTaskListTest()
        {
            ServiceContext serviceContext = new ServiceContext();

            var service = new UserTaskService(serviceContext);

            QueryConditions qc = new QueryConditions();

            /*Условия фильтрации*/
            qc.Filter.Add(
            new FilterQuery()
            {
                ColumnName = "AssignedSid",
                ColumnType = typeof(int),
                FilterOperator = FilterQuery.FilterLogicalOperator.And,
                Filtering = new List<ColumnFilter>() { new ColumnFilter() { ComparisonOperator = ColumnFilter.FilterComparisionOperator.NotEquals, Value = null } }
            });



            Type t = typeof(UserTaskInList);
            

            /*Условия сортировки*/
            qc.Sorting.Add(new ColumnSort() { ColumnKey = "PlannedCompleteDate" });

            /*Условия паджинации*/
            qc.PaginationDetails.RowsToSkip = 0;
            qc.PaginationDetails.RowsToTake = 10;

            var data = service.Search(qc, true);

            Console.WriteLine(data.Status);
            Console.WriteLine(data.Result.Rows);
            Console.WriteLine(data.Result.Rows.Count);
            Console.WriteLine(data.Result.TotalMatches);

            qc.PaginationDetails.SkipTotalMatches = true;
            data = service.Search(qc, true);

            foreach (var userTaskData in data.Result.Rows)
            {
                Console.WriteLine(new string('-', 10));
                foreach (var propertyInfo in t.GetProperties())
                {
                    Console.WriteLine("{0} - {1}", propertyInfo.Name, propertyInfo.GetValue(userTaskData));
                }
            }

            Console.WriteLine(data.Status);
            Console.WriteLine(data.Result.Rows);
            Console.WriteLine(data.Result.Rows.Count);
            Console.WriteLine(data.Result.TotalMatches);

            Assert.IsTrue(true);

        }
    }
}
