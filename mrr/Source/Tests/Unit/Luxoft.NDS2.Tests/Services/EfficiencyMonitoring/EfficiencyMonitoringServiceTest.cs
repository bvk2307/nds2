﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.Common;
using Luxoft.NDS2.Server.Services.InvoiceData;
using Luxoft.NDS2.Tests.Dal.Stubs;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;
using Luxoft.NDS2.Server.Services.EfficiencyMonitoring;
using Luxoft.NDS2.Tests.__Stubs;

namespace Luxoft.NDS2.Tests.Services
{
    [TestFixture]
    [Category("Manual")]
    public class EfficiencyMonitoringServiceTest
    {
        [Test]
        public void IfnsDataTest()
        {
            ServiceContext serviceContext = new ServiceContext();

            var service = new EfficiencyMonitoringService(serviceContext);

            string[] testDates = new string[] {"14.08.2016", "05.09.2016", "11.09.2016"};

            foreach (var testDate in testDates)
            {
                Console.WriteLine(new string('-', 30));
                var data = service.GetFederalCollection(new CalculationData()
                {
                    CalculationDate = DateTime.Parse(testDate),
                    DetailLevel = ScopeEnum.UFNS,
                    Quarter = new QuarterInfo() { Quarter = 2, Year = 2016 },
                    SelectedTnoCode = "11"
                });

                foreach (var tnoData in data.Result.Where(t => t.TNO_CODE.Equals("1103")).Take(10))
                {
                    Console.WriteLine("{0} - {1}, {2}",tnoData.TNO_CODE, tnoData.DECL_ALL_CNT, tnoData.DEDUCTION_AMNT);
                }
            }

        }

        [Test]
        public void GetCalculateDates()
        {
            ServiceContext serviceContext = new ServiceContext();

            var service = new EfficiencyMonitoringService(serviceContext);

            var data = service.GetQuarterCalculationDates();

            var res = data.Result;
            foreach (var key in res.Keys)
            {
                Console.WriteLine("{0} - {1}", key.Year, key.Quarter);
                foreach (var dateTime in res[key])
                {
                    Console.WriteLine("\t{0}", dateTime);
                }
            }

            Assert.IsTrue(true);            
        }

        [Test]
        public void GetDiscrepancyDetailsTest()
        {
            ServiceContext serviceContext = new ServiceContext();

            var service = new EfficiencyMonitoringService(serviceContext);

            QueryConditions qc = new QueryConditions();

            /*Условия фильтрации*/
            qc.Filter.Add(
            new FilterQuery()
            {
                ColumnName = "Quarter",
                ColumnType = typeof(int),
                FilterOperator = FilterQuery.FilterLogicalOperator.And,
                Filtering = new List<ColumnFilter>() { new ColumnFilter() { ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals, Value = 2 } }});

            qc.Filter.Add(
            new FilterQuery()
            {
                ColumnName = "FiscalYear",
                ColumnType = typeof(int),
                FilterOperator = FilterQuery.FilterLogicalOperator.And,
                Filtering = new List<ColumnFilter>() { new ColumnFilter()
            {
                ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals, Value = 2016
            }}});

            qc.Filter.Add(
            new FilterQuery()
            {
                ColumnName = "SonoCode",
                ColumnType = typeof(string),
                FilterOperator = FilterQuery.FilterLogicalOperator.And,
                Filtering = new List<ColumnFilter>() { new ColumnFilter() 
            { ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals, Value = "5252" }}});

            qc.Filter.Add(
            new FilterQuery()
            {
                ColumnName = "CalculationDate",
                ColumnType = typeof(DateTime),
                FilterOperator = FilterQuery.FilterLogicalOperator.And,
                Filtering = new List<ColumnFilter>() { new ColumnFilter()
            { ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals, Value = DateTime.Parse("11.09.2016") },
            new ColumnFilter()
            { ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals, Value = DateTime.Parse("12.09.2016") }
                
                }});

            /*Условия сортировки*/
            qc.Sorting.Add(new ColumnSort() { ColumnKey = "NdsSum" });

            /*Условия паджинации*/
            qc.PaginationDetails.RowsToSkip = 30;
            qc.PaginationDetails.RowsToTake = 10;

            var data = service.GetDiscrepancyDetails(qc, false);

            Console.WriteLine(data.Status);
            Console.WriteLine(data.Result.Rows);
            Console.WriteLine(data.Result.Rows.Count);
            Console.WriteLine(data.Result.TotalMatches);

            Assert.IsTrue(true);
        }
    }
}
