﻿using System;
using Luxoft.NDS2.Client.UI;
using Luxoft.NDS2.Client.UI.Controls.Selection.Filter;
using Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Mock;
using Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Model;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using NUnit.Framework;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;

namespace Luxoft.NDS2.Tests.Services.Selections
{
    class NotifierMock : INotifier
    {
        public void ShowError(string text)
        {
        }

        public void ShowWarning(string message)
        {
        }

        public void ShowNotification(string text)
        {
        }
    }
    
    
    [TestFixture]
    [Category("Manual")]
    class FilterModelTest
    {
        #region generic test data

        private Filter CreateEmptyFilter()
        {
            return new Filter
            {
                Groups = new[]
                {
                    new ParameterGroup
                    {
                        IsEnabled = true,
                        Name = "Test",
                    }
                }
            };
        }

        private Parameter CreateRegionParameter(string regions)
        {
            return new Parameter
            {
                AttributeId = 0,
                IsEnabled = true,
                Operator = ComparisonOperator.InList,
                Values = regions.Split(',').Select(r => new ParameterValue { Value = r }).ToArray()
            };
        }

        #endregion

        [Test]
        public void RegionTest()
        {
            var filter = CreateEmptyFilter();
            filter.Groups.First().Parameters = new[] { CreateRegionParameter("05,77") };
            var model = new FilterModel(filter, new FilterParameterCreator(new FilterDictionaryService(), new NotifierMock(), SelectionType.Hand), false);
        }
    }
}
