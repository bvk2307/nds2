﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Server.DAL.DiscrepancySelection.FilterConverterExpression;
using Luxoft.NDS2.Server.DAL.DiscrepancySelection.Job;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Tests.Services.Selections
{
    [TestFixture]
    [Category("Manual")]
    class FilterConverterTest
    {
        #region Create basic objects

        private SelectionFilter CreateSelectionFilter()
        {
            return new SelectionFilter
            {
                FilterName = "Test",
                UserName="User",
                SelectionId = 1,
                IsActive = true,GroupsFilters = new List<GroupFilter>
                {
                    new GroupFilter {IsActive = true}
                }
            };
        }

        private FilterCriteria CreateFilterCriteria(string firName, string name, string table, FilterCriteria.ValueTypes typeCode, List<FilterElement> values)
        {
            return new FilterCriteria
            {
                FirInternalName = firName,
                InternalName = name,
                IsActive = true,
                LookupTableName = table,
                ValueTypeCode = typeCode,
                Values = values
            };
        }

        private FilterElement CreateFilterElement(FilterElement.ComparisonOperations op, string valueOne, string valueTwo = "")
        {
            return new FilterElement
            {
                Operator = op,
                ValueOne = valueOne,
                ValueTwo = valueTwo
            };
        }

        private List<FilterCriteria> CreateDefaultCriteriaList()
        {
            return new List<FilterCriteria>
            {
                CreateFilterCriteria("region_code", "Region", "v$ssrf", FilterCriteria.ValueTypes.RegionsMultiSelect,
                    new List<FilterElement> {CreateFilterElement(FilterElement.ComparisonOperations.Equals, "05")})
            };
        }

        #endregion

        [Test]
        public void SimpleTest()
        {
            var selectionFilter = CreateSelectionFilter();
            var criteria = CreateDefaultCriteriaList();
            selectionFilter.GroupsFilters.First().Filters = criteria;

            var filter = new FilterConvertManual().CreateFilter(selectionFilter, criteria);

            var builder = SqlJobBodyBuilder.CreateManualSelectionBodySql(1, filter);
            Assert.AreEqual(builder.SqlQueryText.Contains("region_code = :param_1"), true);
        }

        #region Тесты КВО

        [Test]
        public void BitandTest()
        {
            var selectionFilter = CreateSelectionFilter();
            
            var criteria = CreateDefaultCriteriaList();
            criteria.Add(CreateFilterCriteria(
                "side1operationCodes",
                "Side1OperationCodes",
                "DICT_OPERATION_CODES",
                FilterCriteria.ValueTypes.ListMultipleCompirison,
                new List<FilterElement>
                {
                    CreateFilterElement(FilterElement.ComparisonOperations.Contains, "1"),
                    CreateFilterElement(FilterElement.ComparisonOperations.Contains, "2")
                }
                ));

            selectionFilter.GroupsFilters.First().Filters = criteria;

            var filter = new FilterConvertManual().CreateFilter(selectionFilter, criteria);

            var builder = SqlJobBodyBuilder.CreateManualSelectionBodySql(1, filter);
            Assert.AreEqual(builder.SqlQueryText.Contains("NVL(BITAND(dis.side1operationCodes, 3), 0) = 3"), true);
        }

        [Test]
        public void BitandNegativeTest()
        {
            var selectionFilter = CreateSelectionFilter();

            var criteria = CreateDefaultCriteriaList();
            criteria.Add(CreateFilterCriteria(
                "side1operationCodes",
                "Side1OperationCodes",
                "DICT_OPERATION_CODES",
                FilterCriteria.ValueTypes.ListMultipleCompirison,
                new List<FilterElement>
                {
                    CreateFilterElement(FilterElement.ComparisonOperations.NotContains, "1"),
                    CreateFilterElement(FilterElement.ComparisonOperations.NotContains, "2")
                }
                ));

            selectionFilter.GroupsFilters.First().Filters = criteria;

            var filter = new FilterConvertManual().CreateFilter(selectionFilter, criteria);

            var builder = SqlJobBodyBuilder.CreateManualSelectionBodySql(1, filter);
            Assert.AreEqual(builder.SqlQueryText.Contains("NVL(BITAND(dis.side1operationCodes, 3), 0) != 3"), true);
        }

        [Test]
        public void BitandComplexTest()
        {
            var selectionFilter = CreateSelectionFilter();

            var criteria = CreateDefaultCriteriaList();
            criteria.Add(CreateFilterCriteria(
                "side1operationCodes",
                "Side1OperationCodes",
                "DICT_OPERATION_CODES",
                FilterCriteria.ValueTypes.ListMultipleCompirison,
                new List<FilterElement>
                {
                    CreateFilterElement(FilterElement.ComparisonOperations.NotContains, "1"),
                    CreateFilterElement(FilterElement.ComparisonOperations.NotContains, "2")
                }
                )); 
            criteria.Add(CreateFilterCriteria(
                "side2operationCodes",
                "Side2OperationCodes",
                "DICT_OPERATION_CODES",
                FilterCriteria.ValueTypes.ListMultipleCompirison,
                new List<FilterElement>
                {
                    CreateFilterElement(FilterElement.ComparisonOperations.NotContains, "1"),
                    CreateFilterElement(FilterElement.ComparisonOperations.NotContains, "2")
                }
                ));

            selectionFilter.GroupsFilters.First().Filters = criteria;

            var filter = new FilterConvertManual().CreateFilter(selectionFilter, criteria);

            var builder = SqlJobBodyBuilder.CreateManualSelectionBodySql(1, filter);
            Assert.AreEqual(builder.SqlQueryText.Contains("NVL(BITAND(dis.side1operationCodes, 3), 0) != 3"), true);
            Assert.AreEqual(builder.SqlQueryText.Contains("NVL(BITAND(dis.side2operationCodes, 3), 0) != 3"), true);
        }

        #endregion

        #region Тесты налогового периода

        [Test]
        public void TaxPeriodSingle()
        {
            var selectionFilter = CreateSelectionFilter();
            var criteria = CreateDefaultCriteriaList();

            criteria.Add(CreateFilterCriteria(
                "period",
                "TaxPeriod",
                "DICT_NALOG_PERIODS",
                FilterCriteria.ValueTypes.ListMultpleChoice,
                new List<FilterElement>
                {
                    CreateFilterElement(FilterElement.ComparisonOperations.Equals, "201521"),
                }
                ));

            selectionFilter.GroupsFilters.First().Filters = criteria;

            var filter = new FilterConvertManual().CreateFilter(selectionFilter, criteria);

            var builder = SqlJobBodyBuilder.CreateManualSelectionBodySql(1, filter);
            Assert.AreEqual(builder.SqlQueryText.Contains("region_code = :param_1"), true);
            Assert.AreEqual(builder.SqlQueryText.Contains("period = :param_2))"), true);
        }

        [Test]
        public void TaxPeriodList()
        {
            var selectionFilter = CreateSelectionFilter();
            var criteria = CreateDefaultCriteriaList();

            criteria.Add(CreateFilterCriteria(
                "period",
                "TaxPeriod",
                "DICT_NALOG_PERIODS",
                FilterCriteria.ValueTypes.ListMultpleChoice,
                new List<FilterElement>
                {
                    CreateFilterElement(FilterElement.ComparisonOperations.Equals, "201521"),
                    CreateFilterElement(FilterElement.ComparisonOperations.Equals, "201522"),
                }
                ));

            selectionFilter.GroupsFilters.First().Filters = criteria;

            var filter = new FilterConvertManual().CreateFilter(selectionFilter, criteria);

            var builder = SqlJobBodyBuilder.CreateManualSelectionBodySql(1, filter);
            Assert.AreEqual(builder.SqlQueryText.Contains("region_code = :param_1"), true);
            Assert.AreEqual(builder.SqlQueryText.Contains("period in (:param_2,:param_3)"), true);

        }

        #endregion
    }
}
