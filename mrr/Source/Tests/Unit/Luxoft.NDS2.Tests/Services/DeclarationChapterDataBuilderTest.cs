﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Server.Common;
using Luxoft.NDS2.Server.Services.InvoiceData.Impala;
using Luxoft.NDS2.Tests.Dal.Stubs;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Tests.Services
{
    [TestFixture]
    [Category("Manual")]
    public class DeclarationChapterDataBuilderTest
    {
        # region Создание экземпляров тестируемых объектов

        private DeclarationChapterDataBuilder CreateBuilder(List<Tuple<long, int, SovInvoiceRequest>> requests = null)
        {
            var requestAdapter = new SovInvoiceRequestAdapter(requests ?? new List<Tuple<long, int, SovInvoiceRequest>>());

            return new DeclarationChapterDataBuilder(new SovInvoiceAdapter());
        }

        private DeclarationChapterDataRequest CreateRequest(long id, int chapter)
        {
            var cache = new List<DeclarationInvoiceChapterInfo> { CreateDeclarationChapterInfo(id, GetGenericPart(chapter)) };
            if (chapter == 8 || chapter == 9)
                cache.Add(CreateDeclarationChapterInfo(id, GetExtensionPart(chapter)));

            var result = new DeclarationChapterDataRequest
            {
                Chapter = chapter,
                ClientCache = cache,
                DeclarationId = id,
                SearchContext = new QueryConditions()
            };

            result.SearchContext.PaginationDetails.RowsToSkip = 0;
            result.SearchContext.PaginationDetails.RowsToTake = 200;

            return result;
        }

        private DeclarationInvoiceChapterInfo CreateDeclarationChapterInfo(long zip, AskInvoiceChapterNumber part)
        {
            return new DeclarationInvoiceChapterInfo
            {
                Zip = zip,
                Part = part,
            };
        }

        private AskInvoiceChapterNumber GetGenericPart(int chapter)
        {
            switch (chapter)
            {
                case 8: return AskInvoiceChapterNumber.Chapter8;
                case 9: return AskInvoiceChapterNumber.Chapter9;
                case 10: return AskInvoiceChapterNumber.Chapter10;
                case 11: return AskInvoiceChapterNumber.Chapter11;
                case 12: return AskInvoiceChapterNumber.Chapter12;
            }
            throw new ArgumentOutOfRangeException(string.Format("{0} некорректный номер раздела", chapter));
        }

        private AskInvoiceChapterNumber GetExtensionPart(int chapter)
        {
            switch (chapter)
            {
                case 8: return AskInvoiceChapterNumber.Chapter81;
                case 9: return AskInvoiceChapterNumber.Chapter91;
            }
            throw new ArgumentOutOfRangeException(string.Format("{0} некорректный номер раздела с доплистом", chapter));
        }

        # endregion

        # region Валидация контракта

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void NullRequest()
        {
            CreateBuilder().Build(null);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void NullClientCache()
        {
            CreateBuilder().Build(new DeclarationChapterDataRequest());
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void NullCachedRequestKeys()
        {
            CreateBuilder()
                .Build(
                    new DeclarationChapterDataRequest
                    {
                        ClientCache = new List<DeclarationInvoiceChapterInfo>()
                    });
        }

        # endregion

        # region Поиск актуальных корректировок для разделов

        [Test]
        public void RequestKeysChapter8()
        {
            var builder = CreateBuilder();
            var request = CreateRequest(1, 8);

            var response = builder.Build(request);

            Assert.AreEqual(response.PartDataList.Count, 2);
        }

        [Test]
        public void RequestKeysChapter9()
        {
            var builder = CreateBuilder();
            var request = CreateRequest(1, 9);

            var response = builder.Build(request);

            Assert.AreEqual(response.PartDataList.Count, 2);
        }

        [Test]
        public void RequestKeysChapter10()
        {
            var builder = CreateBuilder();
            var request = CreateRequest(1, 10);

            var response = builder.Build(request);

            Assert.AreEqual(response.PartDataList.Count, 1);
        }

        [Test]
        public void RequestKeysChapter11()
        {
            var builder = CreateBuilder();
            var request = CreateRequest(1, 11);

            var response = builder.Build(request);

            Assert.AreEqual(response.PartDataList.Count, 1);
        }

        [Test]
        public void RequestKeysChapter12()
        {
            var builder = CreateBuilder();
            var request = CreateRequest(1, 12);

            var response = builder.Build(request);

            Assert.AreEqual(response.PartDataList.Count, 1);
        }
        # endregion

        # region Первоначальная загрузка

        [Test]
        public void NoCacheLoad()
        {
            var response = CreateBuilder().Build(CreateRequest(2, 8));

            Assert.IsFalse(response.PartDataList.All(x => x.DataReady));
            Assert.AreEqual(response.MatchesQuantity, 0);
            Assert.IsNull(response.Invoices);
        }

        [Test]
        public void FailedCacheLoad()
        {
            var builder = CreateBuilder(
                new List<Tuple<long, int, SovInvoiceRequest>>
                {
                    new Tuple<long, int, SovInvoiceRequest>(2, 8, new SovInvoiceRequest { Id = 1, Status = RequestStatus.Failed }),
                    new Tuple<long, int, SovInvoiceRequest>(1, 81, new SovInvoiceRequest { Id = 2, Status = RequestStatus.Completed }),
                });
            var request = CreateRequest(2, 8);
            request.ClientCache[1].Zip = 1;
            var response = builder.Build(request);

            Assert.IsTrue(false); //--- была поверка на RequestKey
            Assert.IsFalse(response.PartDataList.All(x => x.DataReady));
            Assert.AreEqual(response.MatchesQuantity, 0);
            Assert.IsNull(response.Invoices);
        }

        [Test]
        public void NightModeCacheLoad()
        {
            var builder = CreateBuilder(
                new List<Tuple<long, int, SovInvoiceRequest>>
                {
                    new Tuple<long, int, SovInvoiceRequest>(2, 8, new SovInvoiceRequest { Id = 1, Status = RequestStatus.NotSuccessInNightMode }),
                    new Tuple<long, int, SovInvoiceRequest>(1, 81, new SovInvoiceRequest { Id = 2, Status = RequestStatus.Completed }),
                });
            var request = CreateRequest(2, 8);
            request.ClientCache[1].Zip = 1;
            var response = builder.Build(request);

            Assert.IsTrue(false); //--- была поверка на RequestKey
            Assert.IsFalse(response.PartDataList.All(x => x.DataReady));
            Assert.AreEqual(response.MatchesQuantity, 0);
            Assert.IsNull(response.Invoices);
        }

        [Test]
        [ExpectedException(typeof(SovRequestProcessingException))]
        public void FailedLoad()
        {
            var builder = CreateBuilder(
                new List<Tuple<long, int, SovInvoiceRequest>>
                {
                    new Tuple<long, int, SovInvoiceRequest>(2, 8, new SovInvoiceRequest { Id = 1, Status = RequestStatus.Failed }),
                    new Tuple<long, int, SovInvoiceRequest>(1, 81, new SovInvoiceRequest { Id = 2, Status = RequestStatus.Completed }),
                });
            var request = CreateRequest(2, 8);
            request.ClientCache[1].Zip = 1;
            var response = builder.Build(request);

            Assert.IsTrue(false); //--- была поверка на RequestKey
            Assert.IsFalse(response.PartDataList.All(x => x.DataReady));
            Assert.AreEqual(response.MatchesQuantity, 0);
            Assert.IsNull(response.Invoices);
        }

        [Test]
        [ExpectedException(typeof(SovRequestProcessingException))]
        public void FailedNightModeLoad()
        {
            var builder = CreateBuilder(
                new List<Tuple<long, int, SovInvoiceRequest>>
                {
                    new Tuple<long, int, SovInvoiceRequest>(2, 8, new SovInvoiceRequest { Id = 1, Status = RequestStatus.NotSuccessInNightMode }),
                    new Tuple<long, int, SovInvoiceRequest>(1, 81, new SovInvoiceRequest { Id = 2, Status = RequestStatus.Completed }),
                });
            var request = CreateRequest(2, 8);
            request.ClientCache[1].Zip = 1;
            var response = builder.Build(request);

            Assert.IsTrue(false); //--- была поверка на RequestKey
            Assert.IsFalse(response.PartDataList.All(x => x.DataReady));
            Assert.AreEqual(response.MatchesQuantity, 0);
            Assert.IsNull(response.Invoices);
        }
        # endregion

        # region Загрузка с кэшем

        [Test]
        public void CachedLoad()
        {
            var builder = CreateBuilder(
                new List<Tuple<long, int, SovInvoiceRequest>>
                {
                    new Tuple<long, int, SovInvoiceRequest>(2, 8, new SovInvoiceRequest { Id = 1, Status = RequestStatus.Completed }),
                    new Tuple<long, int, SovInvoiceRequest>(1, 81, new SovInvoiceRequest { Id = 2, Status = RequestStatus.Completed }),
                });
            var request = CreateRequest(2, 8);
            request.ClientCache[1].Zip = 1;
            var response = builder.Build(request);

            Assert.IsTrue(response.PartDataList.All(x => x.DataReady));
            Assert.Greater(response.MatchesQuantity, 0);
            Assert.Greater(response.Invoices.Count(), 0);
        }

        [Test]
        public void InProcessCachedLoadDataReady()
        {
            var builder = CreateBuilder(
                new List<Tuple<long, int, SovInvoiceRequest>>
                {
                    new Tuple<long, int, SovInvoiceRequest>(2, 8, new SovInvoiceRequest { Id = 1, Status = RequestStatus.Completed }),
                    new Tuple<long, int, SovInvoiceRequest>(1, 81, new SovInvoiceRequest { Id = 2, Status = RequestStatus.InProcess }),
                });
            var request = CreateRequest(2, 8);
            request.ClientCache[1].Zip = 1;
            var response = builder.Build(request);

            Assert.IsFalse(response.PartDataList.All(x => x.DataReady));
            Assert.AreEqual(response.MatchesQuantity, 0);
            Assert.IsNull(response.Invoices);
        }
        # endregion

        # region Клиентский кэш

        [Test]
        public void CompletedRequestKeysClientCache()
        {
            var request = CreateRequest(2, 8);

            var builder = CreateBuilder(
                new List<Tuple<long, int, SovInvoiceRequest>>
                {
                    new Tuple<long, int, SovInvoiceRequest>(20, 8, new SovInvoiceRequest { Id = 1, Status = RequestStatus.Completed }),
                    new Tuple<long, int, SovInvoiceRequest>(10, 81, new SovInvoiceRequest { Id = 2, Status = RequestStatus.Completed }),
                });

            var response = builder.Build(request);

            Assert.AreEqual(response.PartDataList.Count, 2);
            Assert.IsTrue(response.PartDataList.All(x => x.DataReady));
            Assert.Greater(response.MatchesQuantity, 0);
            Assert.Greater(response.Invoices.Count(), 0);
        }

        [Test]
        public void InProcessRequestKeysClientCache()
        {
            var request = CreateRequest(2, 8);

            var builder = CreateBuilder(
                new List<Tuple<long, int, SovInvoiceRequest>>
                {
                    new Tuple<long, int, SovInvoiceRequest>(20, 8, new SovInvoiceRequest { Id = 1, Status = RequestStatus.Completed }),
                    new Tuple<long, int, SovInvoiceRequest>(10, 81, new SovInvoiceRequest { Id = 2, Status = RequestStatus.InProcess }),
                });

            var response = builder.Build(request);

            Assert.AreEqual(response.PartDataList.Count, 2);
            Assert.IsFalse(response.PartDataList.All(x => x.DataReady));
            Assert.AreEqual(response.MatchesQuantity, 0);
            Assert.IsNull(response.Invoices);
        }

        [Test]
        public void CompletedDataReadyClientCache()
        {
            var request = CreateRequest(2, 8);
            request.ClientCache[0].DataReady = true;
            request.ClientCache[1].DataReady = true;

            var builder = CreateBuilder(
                new List<Tuple<long, int, SovInvoiceRequest>>
                {
                    new Tuple<long, int, SovInvoiceRequest>(20, 8, new SovInvoiceRequest { Id = 1, Status = RequestStatus.InProcess }),
                    new Tuple<long, int, SovInvoiceRequest>(10, 81, new SovInvoiceRequest { Id = 2, Status = RequestStatus.Failed }),
                });

            var response = builder.Build(request);

            Assert.AreEqual(response.PartDataList.Count, 2);
            Assert.IsTrue(response.PartDataList.All(x => x.DataReady));
            Assert.Greater(response.MatchesQuantity, 0);
            Assert.Greater(response.Invoices.Count(), 0);
        }

        # endregion
    }
}
