﻿using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Helpers.Executors;

namespace Luxoft.NDS2.Tests.Client.GraphTree.Data.Auxiliaries
{
    /// <summary> A test decorator for <see cref="IServiceCallDispatcher{GraphNodesKeyParameters, int, string, List{GraphData}, List{GraphData}}"/>. </summary>
    public sealed class ServiceCallDispatcherTestObj 
        : IServiceCallDispatcher<GraphNodesKeyParameters, int, string, List<GraphData>, List<GraphData>>, 
          IEnumerable<KeyValuePair<long, GraphNodesKeyParameters>>
    {
        private readonly IServiceCallDispatcher<GraphNodesKeyParameters, int, string, List<GraphData>, List<GraphData>> _dispatcherDecorated;
        private readonly ConcurrentDictionary<long, GraphNodesKeyParameters> _inputParameters = new ConcurrentDictionary<long, GraphNodesKeyParameters>();
        private readonly long _testId;

        private long _postponedRequestCount = 0;

        /// <summary> A decorator constructor. </summary>
        /// <param name="dispatcherDecorated"> A decorated testing service call dispatcher. </param>
        /// <param name="testId"> A connection identifier for test purposes only. It isn't used if a value equal or less than 0. By default it isn't used. </param>
        public ServiceCallDispatcherTestObj( 
            IServiceCallDispatcher<GraphNodesKeyParameters, int, string, List<GraphData>, List<GraphData>> dispatcherDecorated, long testId = 0 )
        {
            _dispatcherDecorated = dispatcherDecorated;
            if ( testId > 0 )
                _testId = testId;
        }

        public long TestId { get { return this._testId; } }

        public int RequestCount { get { return _inputParameters.Count; } }

        public long PostponedRequestCount { get { return _postponedRequestCount; } }

        public bool TryAddRequest( long requestKey, GraphNodesKeyParameters requestParameters )
        {
            return _inputParameters.TryAdd( requestKey, requestParameters );
        }

        public bool TryRemoveRequest( long requestKey, out GraphNodesKeyParameters paramsUseless )
        {
            return _inputParameters.TryRemove( requestKey, out paramsUseless );
        }

        public bool TryRemovePostponedRequest( long requestKey, out GraphNodesKeyParameters paramsUseless )
        {
            ++_postponedRequestCount;   //TryRemovePostponedRequest() is called from the single test init thread

            return _inputParameters.TryRemove( requestKey, out paramsUseless );
        }

        #region Implementation of IServiceCallDispatcher<GraphNodesKeyParameters,int,string,List<GraphData>,List<GraphData>>

        /// <summary> Executes all queued items and completes the mesh. </summary>
        public void Complete()
        {
            InputLow.Complete();
            Input.Complete();
        }

        #endregion

        #region Implementation of IServiceCallDispatcher<TParam,TInstanceKey,TContextKey,TResult>

        public ITargetBlock<CallExecContext<GraphNodesKeyParameters, int, string, List<GraphData>>> Input
        {
            get { return _dispatcherDecorated.Input; }
        }

        public ITargetBlock<CallExecContext<GraphNodesKeyParameters, int, string, List<GraphData>>> InputLow
        {
            get { return _dispatcherDecorated.InputLow; }
        }

        public IReceivableSourceBlock<CallExecContext<GraphNodesKeyParameters, int, string, List<GraphData>>> Output
        {
            get { return _dispatcherDecorated.Output; }
        }

        /// <summary> Gets a <see cref="T:System.Threading.Tasks.Task">Task</see> that represents completion of the dataflow mesh. </summary>
        public Task Completion
        {
            get { return _dispatcherDecorated.Completion; }
        }

        #endregion

        #region Implementation of IEnumerable

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// An enumerator that can be used to iterate through the collection.
        /// </returns>
        public IEnumerator<KeyValuePair<long, GraphNodesKeyParameters>> GetEnumerator()
        {
            return _inputParameters.GetEnumerator();
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.IEnumerator"/> object that can be used to iterate through the collection.
        /// </returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ( (IEnumerable)_inputParameters ).GetEnumerator();
        }

        #endregion
    }
}