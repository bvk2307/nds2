﻿using System;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting.TaxPayer;

namespace Luxoft.NDS2.Tests.Client.GraphTree.Data.Auxiliaries
{
    [Serializable]
    public sealed class GraphNodesKeyParametersTestObj : GraphNodesKeyParameters
    {
        private readonly long _testId;

        public GraphNodesKeyParametersTestObj( TaxPayerId taxPayerId, bool isByPurchase, int quarter, int year, long testId ) 
            : base(isByPurchase, quarter, year, new[] { taxPayerId })
        {
            _testId = testId;
        }

        /// <summary> A connection identifier for test purposes only. It isn't used if a value equal or less than 0. By default it isn't used. </summary>
        public long TestId { get { return this._testId; } }
    }
}