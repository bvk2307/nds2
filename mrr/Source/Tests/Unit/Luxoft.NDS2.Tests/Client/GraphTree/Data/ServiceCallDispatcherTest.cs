﻿#if MSTEST

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.UI.Base.ServiceAsync.Auxiliaries;
using Luxoft.NDS2.Client.UI.GraphTree;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting.TaxPayer;
using Luxoft.NDS2.Common.Helpers.Dataflow.Extensions;
using Luxoft.NDS2.Common.Helpers.Executors;
using Luxoft.NDS2.Common.Helpers.Tasks.Extensions;
using Luxoft.NDS2.Tests.Client.GraphTree.Data.Auxiliaries;
using Luxoft.NDS2.Tests.Client.GraphTree.Data.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Luxoft.NDS2.Tests.Client.GraphTree.Data
{
    /// <summary> Test class for <see cref="IServiceCallDispatcher{TParam,TInstanceKey,TContextKey,TResult}"/>. </summary>
    [TestClass]
    public class ServiceCallDispatcherTest
    {
        private const int CallDispatcherInputBoundCapacity = 1;
        private const int CallDispatcherLowInputBoundCapacity = 1000;
        private const int BatchCallDispatcherInputBoundCapacity = 250;

        private const int GraphDataTestID = 345;
        private const long RootTestID = 1;
        private const string RootInn = "1"; //equals to 'RootTestID.ToString( CultureInfo.InvariantCulture )'

        private static readonly int[] s_highPriorityModules = { 0, 1, 2, 3, 15, 35 };   //from 0 to 49
        private static ServiceCallDispatcherFactory _callDispatcherFactory;

        private int _dispatcherCount = 0;
        private long _allRequestCount = 0;
        private int _skipBeforeThrowCount = 0;

        [ClassInitialize]
        public static void ClassInit( TestContext context )
        {
            ServiceCallExecutorManagerFactory.InitIfNeeded();

            _callDispatcherFactory = new ServiceCallDispatcherFactory( СallExecutorManager );
        }

        private static IServiceCallExecutorManager СallExecutorManager
        {
            get { return ServiceCallExecutorManagerFactory.CurrentManager; }
        }

        [ClassCleanup]
        public static void ClassCleanup()
        {
        }

        [TestInitialize]
        public void TestInitialize()
        {
            _allRequestCount = 0;
            _skipBeforeThrowCount = 0;
        }

        /// <summary> Tests on batch service call dispatching concurrently. </summary>
        [Ignore]
        [TestMethod]
        [TestCategory( "GraphTree.Data" )]
        public void TransformSimpleTest()
        {
            //apopov 20.5.2016	//DEBUG!!!
            var transform = new TransformBlock<int, int>( n => n );
            transform.Completion.ContinueWith( taskPrev => Debug.WriteLine( ">>> 'transform' is {0}", taskPrev.Status ), TaskContinuationOptions.ExecuteSynchronously );
            transform.Post( 333 );

            transform.Complete();
            //( (IDataflowBlock)transform ).Fault( new ApplicationException( "	//apopov 21.5.2016	//DEBUG!!!" ) );

            Thread.Sleep( 1*1000 );

            Debug.WriteLine( ">>> 'transform' completion: before 'Receive()': {0}", transform.Completion.Status );

            int item = transform.Receive();
            Assert.IsTrue( item == 333 );

            Thread.Sleep( 1*1000 );

            Debug.WriteLine( ">>> 'transform' completion: after 'Receive()': {0}", transform.Completion.Status );
        }

        /// <summary> Tests on batch service call dispatching concurrently. </summary>
        [Ignore]
        [TestMethod]
        [TestCategory( "GraphTree.Data" )]
        public void JoinSimpleTest()
        {
            //apopov 20.5.2016	//DEBUG!!!
            var join = new JoinBlock<int, int>( new GroupingDataflowBlockOptions { Greedy = false } );
            join.Completion.ContinueWith( taskPrev => Debug.WriteLine( ">>> 'join' is {0}", taskPrev.Status ), TaskContinuationOptions.ExecuteSynchronously );
            join.Target1.Post( 333 ); join.Target2.Post( 777 );

            join.Complete();
            //( (IDataflowBlock)join ).Fault( new ApplicationException( "	//apopov 21.5.2016	//DEBUG!!!" ) );

            Thread.Sleep( 1*1000 );

            Debug.WriteLine( ">>> 'join' completion: before 'Receive()': {0}", join.Completion.Status );

            Tuple<int, int> pair;
            bool success = join.TryReceive( out pair );

            Assert.IsFalse( success );

            Thread.Sleep( 1*1000 );

            Debug.WriteLine( ">>> 'join' completion: after 'Receive()': {0}", join.Completion.Status );
        }

        private ServiceCallDispatcherTestObj CreateNewServiceCallDispatcherTestObj( 
            Func<CallExecContext<GraphNodesKeyParameters, int, string, List<GraphData>>, 
                 CallExecContext<GraphNodesKeyParameters, int, string, List<GraphData>>> transoformHandler, 
            CancellationToken? cancelTokenAllTest = null )
        {
            int tryNumber = Interlocked.Increment(ref _dispatcherCount);

            CancellationToken cancelToken = cancelTokenAllTest.HasValue ? cancelTokenAllTest.Value : CancellationToken.None;

            IServiceCallDispatcher<GraphNodesKeyParameters, int, string, List<GraphData>, List<GraphData>> serviceCallDispatcher =
                _callDispatcherFactory.Open<GraphNodesKeyParameters, int, string, List<GraphData>, List<GraphData>>(
                    transoformHandler, GetNode, cancelToken, CallDispatcherInputBoundCapacity, CallDispatcherLowInputBoundCapacity );

            var srvCallDispatcherTestObj = new ServiceCallDispatcherTestObj( serviceCallDispatcher, testId: tryNumber );

            Action<CallExecContext<GraphNodesKeyParameters, int, string, List<GraphData>>> onDataLoadedHandler =
                callResult => OnDataLoaded( callResult, srvCallDispatcherTestObj );

            var dataProcessor = new ActionBlock<CallExecContext<GraphNodesKeyParameters, int, string, List<GraphData>>>(
                onDataLoadedHandler,
                new ExecutionDataflowBlockOptions
                {
	//apopov 20.3.2016	//DEBUG!!!
                    CancellationToken = cancelToken, //TaskScheduler = TaskScheduler.FromCurrentSynchronizationContext(),
                    NameFormat = "OnDataLoaded {0}, ID {1} GraphTreePresenter"
                });

            serviceCallDispatcher.Output.LinkTo( dataProcessor, new DataflowLinkOptions { PropagateCompletion = true } ); //subscribes to receiving nodes

            return srvCallDispatcherTestObj;
        }

        private ServiceCallBatchDispatcherTestObj CreateNewServiceCallBatchDispatcherTestObj( CancellationToken? cancelTokenAllTest = null )
        {
            int tryNumber = Interlocked.Increment(ref _dispatcherCount);

            CancellationToken cancelToken = cancelTokenAllTest.HasValue ? cancelTokenAllTest.Value : CancellationToken.None;

            var reportBatchDataManager = new GTReportDummyBatchDataManager( callCountLimit: 20 );    //1 );     //by 1 request using GetNode()
            //var reportBatchDataManager = new GTReportDummyBatchDataManager( callCountLimit: 20, throwInside: true );     //ATTENTION! Throws an exception! By 1 request using GetNode()

            //var reportBatchDataManager = new GraphTreeReportBatchDataManager( new BatchDataLoadStrategyOptions( maxParallelRequestCount: 1, maxLevel: 4 ) );	//apopov 5.5.2016	//DEBUG!!!

            ServiceCallBatchDispatcherTestObj srvCallDispatcherTestObj = null;

            Action<CallExecContext<GraphNodesKeyParameters, int, string, IEnumerable<GraphDataNode>>> onDataLoadedHandler =
                // ReSharper disable once AccessToModifiedClosure
                ( callResult ) => OnDataLoaded( callResult, srvCallDispatcherTestObj );
                //( callResult ) => OnDataLoadedThrow( callResult, srvCallDispatcherTestObj );

            IServiceCallBatchDispatcher<GraphNodesKeyParameters, int, string> serviceCallDispatcher =
                _callDispatcherFactory.CreateBatch( //GetTestTree2,
                                                    //GetTestTree,
                                                    //GetTestTree_SpecInns,
                                                    Get100Nodes,            //by 100 requests
                                                    //Get100NodesThrow,       //by 100 requests with exception raising
                                                    //GetNode,                //by 1 request
                                                    reportBatchDataManager, onDataLoadedHandler, cancelToken, BatchCallDispatcherInputBoundCapacity );

            srvCallDispatcherTestObj = new ServiceCallBatchDispatcherTestObj( serviceCallDispatcher, testId: tryNumber );

            return srvCallDispatcherTestObj;
        }

        /// <summary> Tests on service call dispatching concurrently. </summary>
        [TestMethod]
        [TestCategory( "GraphTree.Data" )]
        public void ServiceCallDispatcherContinuously()
        {
            const string testName = "ServiceCallDispatcherContinuously";

            ServiceCallDispatcherConcurrently(
                createCallDispatcher: cancelTokenAllTest => CreateNewServiceCallDispatcherTestObj( OnDataTransform ),
                testResultCheckHandler: srvCallDispatcherTestObjPar => TestResultCheckHandler( srvCallDispatcherTestObjPar ), 
                testLengthMillisec: 1 * 1 * 30 * 1000, testName: testName );
        }

        /// <summary> Tests on cancellation of service call dispatching concurrently. </summary>
        [TestMethod]
        [TestCategory( "GraphTree.Data" )]
        public void ServiceCallDispatcherWCancel()
        {
            const string testName = "ServiceCallDispatcherWCancel";

            ServiceCallDispatcherConcurrently(
                createCallDispatcher: cancelTokenAllTest => CreateNewServiceCallDispatcherTestObj( OnDataTransformThrow, cancelTokenAllTest ),
                testResultCheckHandler: callDispatcher => TestResultCheckHandler( callDispatcher, canCancel: true, shouldFault: true ),
                testLengthMillisec: 1 * 1 * 10 * 1000, testName: testName, cancelWithAllTest: true );
        }

        /// <summary> Tests on batch service call dispatching concurrently. </summary>
        [TestMethod]
        [TestCategory( "GraphTree.Data" )]
        public void ServiceCallBatchDispatcher()
        {
            const string testName = "ServiceCallBatchDispatcher";

            ServiceCallBatchDispatcherConcurrently(
                createCallDispatcher: cancelTokenAllTest => CreateNewServiceCallBatchDispatcherTestObj(), 
                testResultCheckHandler: srvCallDispatcherTestObjPar => TestResultCheckBatchHandler( srvCallDispatcherTestObjPar ),
                testLengthMillisec: 1 * 1 * 20 * 1000, testName: testName, cancelWithAllTest: true );
        }

        /// <summary> Tests on cancellation of batch service call dispatching concurrently. </summary>
        [TestMethod]
        [TestCategory( "GraphTree.Data" )]
        public void ServiceCallBatchDispatcherWCancel()
        {
            const string testName = "ServiceCallBatchDispatcher";

            ServiceCallBatchDispatcherConcurrently(
                createCallDispatcher: CreateNewServiceCallBatchDispatcherTestObj,
                testResultCheckHandler: batchDispatcher => TestResultCheckBatchHandler( batchDispatcher, canCancel: true ),
                testLengthMillisec: 1 * 1 * 10 * 1000, testName: testName, cancelWithAllTest: true );
        }

        /// <summary> Tests on service call dispatching concurrently. </summary>
        /// <param name="createCallDispatcher"></param>
        /// <param name="testResultCheckHandler"></param>
        /// <param name="testLengthMillisec"></param>
        /// <param name="testName"></param>
        /// <param name="cancelWithAllTest"></param>
        public void ServiceCallDispatcherConcurrently(
            Func<CancellationToken?, IServiceCallDispatcher<GraphNodesKeyParameters, int, string, List<GraphData>, List<GraphData>>> createCallDispatcher,
            Action<ServiceCallDispatcherTestObj> testResultCheckHandler,
            int testLengthMillisec, string testName, bool cancelWithAllTest = false )
        {
            ServiceCallDispatcherTest.WriteLine( "======= TEST \t{0} starting... \t=======", testName );
            try
            {
                ServiceCallDispatcherTest.WriteLine( "Free executor count before testt: {0}", СallExecutorManager.ExecutorCountFree );

                ServiceCallDispatcherTestHelper.ConcurrentCallTest( createCallDispatcher,

                    testHandler: ( srvCallDispatcherTestObjPar, sleepMillisecPar, cancellTokenAllTestPar ) =>
                    {
                        ServiceCallDispatcherTestObj srvCallDispatcherWrapper = (ServiceCallDispatcherTestObj)srvCallDispatcherTestObjPar;
                        int instanceKey = (int)srvCallDispatcherWrapper.TestId;
                        int testCount = 0;

                        while ( ( !cancelWithAllTest || !cancellTokenAllTestPar.IsCancellationRequested ) && ++testCount <= 10000
                                && !srvCallDispatcherWrapper.InputLow.IsCompleted() && !srvCallDispatcherWrapper.Input.IsCompleted() )
                        {
                            long requestID = Interlocked.Increment( ref _allRequestCount );
                            var requestParameters = new GraphNodesKeyParametersTestObj( 
                                new TaxPayerId( requestID.ToString(), kppCode: null ), isByPurchase: false, quarter: 1, year: 2016, testId: requestID );

                            Assert.IsTrue( srvCallDispatcherWrapper.TryAddRequest( requestID, requestParameters ) );

                            ITargetBlock<CallExecContext<GraphNodesKeyParameters, int, string, List<GraphData>>>
                                input = IsLowPriority(requestID) ? srvCallDispatcherWrapper.InputLow : srvCallDispatcherWrapper.Input;

                            bool isPostponed = !input.Post(new CallExecContext<GraphNodesKeyParameters, int, string, List<GraphData>>(
                                requestParameters, instanceKey: instanceKey, contextKey: null));
                            if ( isPostponed )
                            {
                                ServiceCallDispatcherTest.WriteLine( "Request with ID: {0} is postponed by dispatcher with test ID: {1}", requestID, srvCallDispatcherWrapper.TestId );

                                GraphNodesKeyParameters paramsUseless;
                                Assert.IsTrue( srvCallDispatcherWrapper.TryRemovePostponedRequest( requestID, out paramsUseless ) );
                            }
                            else
                            {
                                //ServiceCallDispatcherTest.WriteLine( "Request with ID: {0} is posted to dispatcher with test ID: {1}", requestID, srvCallDispatcherTestObjPar.TestId );
                            }
                            Thread.Sleep( 3 );
                        }
                        //apopov 19.4.2016	//DEBUG!!!
                        //if ( cancelWithAllTest && cancellTokenAllTestPar.IsCancellationRequested )
                        //    Thread.Sleep( 500 );    //waits while a few high priority requests will be handled on cancelling but all low priority requests will not

                        return true;    //don't sleep inside ServiceCallDispatcherTestHelper.ServiceCallDispatcherConcurrently()
                        //return false;    //sleeps inside ServiceCallDispatcherTestHelper.ServiceCallDispatcherConcurrently()
                    },

                    testFinishHandler: srvCallDispatcherTestObjPar =>
                    {
                        ServiceCallDispatcherTestObj srvCallDispatcherWrapper = (ServiceCallDispatcherTestObj)srvCallDispatcherTestObjPar;
                        Task taskCompletion = srvCallDispatcherTestObjPar.Output.Completion;
                        Stopwatch sw = Stopwatch.StartNew();
	//apopov 5.7.2016	//DEBUG!!!
                        while ( !taskCompletion.IsCompleted && sw.ElapsedMilliseconds < 40 * 1000 )
                        {
                            Thread.Sleep( 100 );
                        }
                        sw.Stop();

                        testResultCheckHandler( srvCallDispatcherWrapper );
                    },

                    testLengthMillisec: testLengthMillisec, threadNumber: 1, sleepMillisec: 10 );

                int previousValue = СallExecutorManager.ExecutorCountFree;

                Thread.Sleep( 100 ); //waits while executors will be returned to Executor Manager
                ServiceCallDispatcherTest.WriteLine( "Free executor count: {0} before", previousValue );

                ServiceCallDispatcherTest.WriteLine( "Free executor count: {0}", СallExecutorManager.ExecutorCountFree );

                Assert.IsTrue( СallExecutorManager.ExecutorCountFree == СallExecutorManager.MaxExecutorNumber );

                ServiceCallDispatcherTest.WriteLine( "Dispatcher created count: {0}", _dispatcherCount );
                ServiceCallDispatcherTest.WriteLine( "======= TEST \t{0} finished \t=======", testName );
            }
            catch ( Exception exc )
            {
                ServiceCallDispatcherTest.WriteLine( "======= TEST \t{0} FAILED \t======= :\r\n{1}", testName, exc.ToString() );
                throw;
            }
        }

        private static void TestResultCheckHandler( 
            ServiceCallDispatcherTestObj srvCallDispatcherTestObjPar, bool canCancel = false, bool shouldFault = false )
        {
            if ( !canCancel && !shouldFault )
                srvCallDispatcherTestObjPar.Complete();

            Task taskCompletion = srvCallDispatcherTestObjPar.Output.Completion;
            if ( canCancel )
            {
                int nonHandledPriorityCount = 0;
                foreach ( KeyValuePair<long, GraphNodesKeyParameters> keyValuePair in srvCallDispatcherTestObjPar )
                {
                    if ( !IsLowPriority( keyValuePair.Key ) )
                    {
                        ++nonHandledPriorityCount;
                        ServiceCallDispatcherTest.WriteLine( "High priority request with test ID: {0} is not handled", keyValuePair.Key );
                    }
                }
                Assert.IsTrue( nonHandledPriorityCount <= СallExecutorManager.MaxExecutorNumber + CallDispatcherInputBoundCapacity );

                if ( taskCompletion.IsFaulted )
                {
                    if ( !shouldFault )
                        taskCompletion.Wait();  //to rethrow an exception only
                }
                if ( shouldFault )
                {
                    if ( !taskCompletion.IsFaulted )
                        Assert.IsTrue( taskCompletion.IsCanceled );
                }
                else
                {
                    Assert.IsTrue( taskCompletion.IsCanceled || taskCompletion.IsRanToCompletion() );
                }
            }
            else if ( shouldFault )
            {
                Assert.IsTrue( taskCompletion.IsFaulted );

                Assert.IsTrue( srvCallDispatcherTestObjPar.RequestCount <= 
                    CallDispatcherInputBoundCapacity + CallDispatcherLowInputBoundCapacity + 16 );  //some non handled requests (unknown number) can be inside the mesh
            }
            else
            {
                Assert.IsTrue( srvCallDispatcherTestObjPar.RequestCount == 0 );
            }
            Task fullCompletion = srvCallDispatcherTestObjPar.Completion;
            Exception exception = null;
            bool completed = false;
            try
            {
                completed = fullCompletion.Wait( 100 );  //to rethrow an exception only
            }
            catch ( Exception exc )
            {
                exception = exc;
                if ( !shouldFault )
                    throw;
            }
            Assert.IsTrue( completed || exception != null );

            if ( canCancel )
            {
                if ( shouldFault )
                {
                    if ( !fullCompletion.IsFaulted )
                        Assert.IsTrue( fullCompletion.IsCanceled );
                    else Assert.IsTrue( exception != null );
                }
                else
                {
                    Assert.IsTrue( fullCompletion.IsCanceled || fullCompletion.IsRanToCompletion() );
                }
            }
            else if ( shouldFault )
            {
                Assert.IsTrue( fullCompletion.IsFaulted && exception != null );
            }
            else
            {
                Assert.IsTrue( exception == null );
            }
            ServiceCallDispatcherTest.WriteLine( "Dispatcher with test ID: {0} postponed {1} requests", srvCallDispatcherTestObjPar.TestId, srvCallDispatcherTestObjPar.PostponedRequestCount );
        }

        private CallExecContext<GraphNodesKeyParameters, int, string, List<GraphData>> 
            OnDataTransform( CallExecContext<GraphNodesKeyParameters, int, string, List<GraphData>> context )
        {
            return context;
        }

        private CallExecContext<GraphNodesKeyParameters, int, string, List<GraphData>> 
            OnDataTransformThrow( CallExecContext<GraphNodesKeyParameters, int, string, List<GraphData>> context )
        {
            int tryNumber = Interlocked.Increment( ref _skipBeforeThrowCount );
            if ( tryNumber == 1100 )
                throw new ApplicationException( string.Format( "TEST EXCEPTION on try #: {0}", tryNumber ) );

            return OnDataTransform( context );
        }

        private void OnDataLoaded( CallExecContext<GraphNodesKeyParameters, int, string, List<GraphData>> callResult,
            ServiceCallDispatcherTestObj srvCallDispatcherTestObj )
        {
            Assert.IsTrue( callResult.Result[0].Id == GraphDataTestID );

            var graphNodesKeyParamsTestObj = (GraphNodesKeyParametersTestObj)callResult.Parameters;

            Assert.IsTrue( graphNodesKeyParamsTestObj.TestId.ToString() == graphNodesKeyParamsTestObj.Inn );
            GraphNodesKeyParameters graphNodesKeyParams;
            Assert.IsTrue( srvCallDispatcherTestObj.TryRemoveRequest( graphNodesKeyParamsTestObj.TestId, out graphNodesKeyParams ) );
            Assert.IsTrue( object.ReferenceEquals( graphNodesKeyParams, graphNodesKeyParamsTestObj ) );

            //ServiceCallDispatcherTest.WriteLine( "Request with ID: {0} is successful handled", graphNodesKeyParamsTestObj.TestId );
        }

        /// <summary> Tests on batch service call dispatching concurrently. </summary>
        /// <param name="createCallDispatcher"></param>
        /// <param name="testResultCheckHandler"></param>
        /// <param name="testLengthMillisec"></param>
        /// <param name="testName"></param>
        /// <param name="cancelWithAllTest"></param>
        public void ServiceCallBatchDispatcherConcurrently(

            Func<CancellationToken?, IServiceCallBatchDispatcher<GraphNodesKeyParameters, int, string>> createCallDispatcher,
            Action<ServiceCallBatchDispatcherTestObj> testResultCheckHandler,
            int testLengthMillisec, string testName, bool cancelWithAllTest = false )
        {
            ServiceCallDispatcherTest.WriteLine( "======= TEST \t{0} starting... \t=======", testName );
            try
            {
                ServiceCallDispatcherTest.WriteLine( "Free executor count before testt: {0}", СallExecutorManager.ExecutorCountFree );

                ServiceCallDispatcherTestHelper.ConcurrentCallTest( createCallDispatcher,

                    testHandler: ( srvCallDispatcherTestObjPar, sleepMillisecPar, cancellTokenAllTestPar ) =>
                    {
                        ServiceCallBatchDispatcherTestObj srvCallDispatcherWrapper = (ServiceCallBatchDispatcherTestObj)srvCallDispatcherTestObjPar;
                        int instanceKey = (int)srvCallDispatcherWrapper.TestId;
                        int testCount = 0;

                        //long requestID = Interlocked.Increment(ref _allRequestCount);

                        var requestParameters = new GraphNodesKeyParametersTestObj(new TaxPayerId(RootInn, kppCode: null), isByPurchase: false, quarter: 1, year: 2016, testId: RootTestID)
                      ;

                        srvCallDispatcherWrapper.Start( requestParameters, instanceKey, contextKey: null );

                        while ( ( !cancelWithAllTest || !cancellTokenAllTestPar.IsCancellationRequested ) && ++testCount <= 10000 )
                        {
                            Thread.Sleep( 3 );
                        }

                        return true;    //don't sleep inside ServiceCallDispatcherTestHelper.ServiceCallDispatcherConcurrently()
                        //return false;    //sleeps inside ServiceCallDispatcherTestHelper.ServiceCallDispatcherConcurrently()
                    },

                    testFinishHandler: srvCallDispatcherTestObjPar =>
                    {
                        ServiceCallBatchDispatcherTestObj srvCallDispatcherWrapper = (ServiceCallBatchDispatcherTestObj)srvCallDispatcherTestObjPar;
                        Task taskCompletion = srvCallDispatcherTestObjPar.Completion;
                        Stopwatch sw = Stopwatch.StartNew();
                        while ( !taskCompletion.IsCompleted && sw.ElapsedMilliseconds < 2 * 1000 )
                        {
                            Thread.Sleep( 100 );
                        }
                        sw.Stop();

                        testResultCheckHandler( srvCallDispatcherWrapper );
                    },

                    testLengthMillisec: testLengthMillisec, threadNumber: 1, sleepMillisec: 10 );

                var previousValue = СallExecutorManager.ExecutorCountFree;

                Thread.Sleep( 100 ); //waits while executors will be returned to  Executor Manager

                ServiceCallDispatcherTest.WriteLine( "Free executor count: {0} before", previousValue );
                ServiceCallDispatcherTest.WriteLine( "Free executor count: {0}", СallExecutorManager.ExecutorCountFree );

                Assert.IsTrue( СallExecutorManager.ExecutorCountFree == СallExecutorManager.MaxExecutorNumber );

                ServiceCallDispatcherTest.WriteLine( "Dispatcher created count: {0}", _dispatcherCount );
                ServiceCallDispatcherTest.WriteLine( "======= TEST \t{0} finished \t=======", testName );
            }
            catch ( Exception exc )
            {
                ServiceCallDispatcherTest.WriteLine( "======= TEST \t{0} FAILED \t======= :\r\n{1}", testName, exc.ToString() );
                throw;
            }
        }

        private void OnDataLoaded( CallExecContext<GraphNodesKeyParameters, int, string, IEnumerable<GraphDataNode>> callResult,
            ServiceCallBatchDispatcherTestObj srvCallDispatcherTestObj )
        {
            //Assert.IsTrue( callResult.Result.First().Id == GraphDataTestID );

            GraphNodesKeyParametersTestObj graphNodesKeyParamsTestObj = callResult.Parameters as GraphNodesKeyParametersTestObj;

            if ( graphNodesKeyParamsTestObj != null )
                Assert.IsTrue( graphNodesKeyParamsTestObj.TestId.ToString() == graphNodesKeyParamsTestObj.Inn );

            if ( graphNodesKeyParamsTestObj != null )
                ServiceCallDispatcherTest.WriteLine( "Request with ID: {0} is successful handled", graphNodesKeyParamsTestObj.TestId );
            else
                ServiceCallDispatcherTest.WriteLine( "Request for INN: {0} is successful handled", callResult.Parameters.TaxPayerIds.Aggregate( "", ( x, y ) => ( x + " " + y.Inn ) ) );
            ServiceCallDispatcherTest.WriteLine( "Output: {0}",
                callResult.Result.Aggregate( "", ( x, y ) => ( x + " " + y.Inn ) ) );
        }

        private void OnDataLoadedThrow( CallExecContext<GraphNodesKeyParameters, int, string, IEnumerable<GraphDataNode>> callResult,
            ServiceCallBatchDispatcherTestObj srvCallDispatcherTestObj )
        {
            int tryNumber = Interlocked.Increment( ref _skipBeforeThrowCount );
            if ( tryNumber == 1486 )    // 1000 )   //for debug
                throw new ApplicationException( string.Format( "TEST EXCEPTION on try #: {0}", tryNumber ) );

            OnDataLoaded( callResult, srvCallDispatcherTestObj );
        }

        private static void TestResultCheckBatchHandler( ServiceCallBatchDispatcherTestObj srvCallDispatcherTestObjPar, bool canCancel = false )
        {
            Task taskCompletion = srvCallDispatcherTestObjPar.Completion;
            Assert.IsTrue( taskCompletion.IsCompleted );
            if ( taskCompletion.IsFaulted )
                taskCompletion.Wait();  //to rethrow an exception only
            if ( canCancel )
            {
                Assert.IsTrue( taskCompletion.IsCanceled || taskCompletion.IsRanToCompletion() );
            }
            else
            {
                Assert.IsFalse( taskCompletion.IsCanceled );
                Assert.IsTrue( taskCompletion.IsRanToCompletion() );
            }
        }

        private static bool IsLowPriority( long requestID )
        {
            bool isLowPriority = 0 > Array.BinarySearch(s_highPriorityModules, (int)(requestID % 50));

            return isLowPriority;
        }

        private List<GraphData> GetNode( GraphNodesKeyParameters nodesKeyParameters, CancellationToken cancelToken = default( CancellationToken ) )
        {
            Contract.Requires( nodesKeyParameters != null );
            Contract.Ensures( Contract.Result<List<GraphData>>() != null );

            var nodes = new List<GraphData>(new[] { new GraphData { Id = GraphDataTestID, TaxPayerId = new TaxPayerId(innCode: "TEST", kppCode: null) } });

            Thread.Sleep( 25 );

            return nodes;
        }

        private List<GraphData> Get100Nodes( GraphNodesKeyParameters nodesKeyParameters, CancellationToken cancelToken = default( CancellationToken ) )
        {
            Contract.Requires( nodesKeyParameters != null );
            Contract.Ensures( Contract.Result<List<GraphData>>() != null );

            var nodes = new List<GraphData>(100);
            for ( int nI = 0; nI < 100; nI++ )
            {
                nodes.Add( new GraphData { Id = GraphDataTestID, TaxPayerId = new TaxPayerId( innCode: "TEST" + nI, kppCode: null ) } );
            }
            Thread.Sleep( new Random().Next( 20, 30 ) );

            return nodes;
        }

        private List<GraphData> Get100NodesThrow( GraphNodesKeyParameters nodesKeyParameters, CancellationToken cancelToken = default( CancellationToken ) )
        {
            Contract.Requires( nodesKeyParameters != null );
            Contract.Ensures( Contract.Result<List<GraphData>>() != null );

            int tryNumber = Interlocked.Increment( ref _skipBeforeThrowCount );
            if ( tryNumber == 1464 )    // 800 for debug )
                throw new ApplicationException( string.Format( "TEST EXCEPTION on try #: {0}", tryNumber ) );

            return Get100Nodes( nodesKeyParameters, cancelToken );
        }

        private IEnumerable<GraphData> GetTestTree( GraphNodesKeyParameters keyParameters, CancellationToken cancelToken )
        {
            Contract.Requires( keyParameters != null );
            Contract.Ensures( Contract.Result<IEnumerable<GraphData>>() != null );

            var nodes = new List<GraphData>(5);
            switch ( keyParameters.Inn )
            {
                case RootInn:
                    nodes.Add( new GraphData
                    {
                        Id = GraphDataTestID,
                        TaxPayerId = new TaxPayerId( innCode: RootInn, kppCode: null ),
                        ParentInn = null
                    } );
                    nodes.Add( new GraphData
                    {
                        Id = GraphDataTestID,
                        TaxPayerId = new TaxPayerId( innCode: "Inn2", kppCode: null ),
                        ParentInn = RootInn
                    } );
                    nodes.Add( new GraphData
                    {
                        Id = GraphDataTestID,
                        TaxPayerId = new TaxPayerId( innCode: "Inn3", kppCode: null ),
                        ParentInn = RootInn
                    } );
                    nodes.Add( new GraphData
                    {
                        Id = GraphDataTestID,
                        TaxPayerId = new TaxPayerId( innCode: "Inn4", kppCode: null ),
                        ParentInn = RootInn
                    } );
                    break;
                case "Inn2":

                    nodes.Add( new GraphData
                    {
                        Id = GraphDataTestID,
                        TaxPayerId = new TaxPayerId( innCode: "Inn5", kppCode: null ),
                        ParentInn = "Inn2"
                    } );
                    nodes.Add( new GraphData
                    {
                        Id = GraphDataTestID,
                        TaxPayerId = new TaxPayerId( innCode: "Inn6", kppCode: null ),
                        ParentInn = "Inn2"
                    } );

                    break;

                case "Inn3":

                    nodes.Add( new GraphData
                    {
                        Id = GraphDataTestID,
                        TaxPayerId = new TaxPayerId( innCode: "Inn7", kppCode: null ),
                        ParentInn = "Inn3"
                    } );
                    break;
                case "Inn4":

                    nodes.Add( new GraphData
                    {
                        Id = GraphDataTestID,
                        TaxPayerId = new TaxPayerId( innCode: "Inn8", kppCode: null ),
                        ParentInn = "Inn4"
                    } );
                    nodes.Add( new GraphData
                    {
                        Id = GraphDataTestID,
                        TaxPayerId = new TaxPayerId( innCode: "Inn9", kppCode: null ),
                        ParentInn = "Inn4"
                    } );

                    break;
                case "Inn8":

                    nodes.Add( new GraphData
                    {
                        Id = GraphDataTestID,
                        TaxPayerId = new TaxPayerId( innCode: "Inn10", kppCode: null ),
                        ParentInn = "Inn8"
                    } );
                    break;
                case "Inn9":

                    nodes.Add( new GraphData
                    {
                        Id = GraphDataTestID,
                        TaxPayerId = new TaxPayerId( innCode: "Inn11", kppCode: null ),
                        ParentInn = "Inn9"
                    } );
                    break;

            }

            return nodes;
        }

        private IEnumerable<GraphData> GetTestTree2( GraphNodesKeyParameters keyParameters, CancellationToken cancelToken )
        {
            Contract.Requires( keyParameters != null );
            Contract.Ensures( Contract.Result<IEnumerable<GraphData>>() != null );

            var nodes = new List<GraphData>(5);
            foreach ( var id in keyParameters.TaxPayerIds )
            {
                switch ( id.Inn )
                {
                    case RootInn:
                        nodes.Add( new GraphData
                        {
                            Id = GraphDataTestID,
                            TaxPayerId = new TaxPayerId( innCode: RootInn, kppCode: null ),
                            ParentInn = null
                        } );
                        nodes.Add( new GraphData
                        {
                            Id = GraphDataTestID,
                            TaxPayerId = new TaxPayerId( innCode: "Inn2", kppCode: null ),
                            ParentInn = RootInn
                        } );
                        nodes.Add( new GraphData
                        {
                            Id = GraphDataTestID,
                            TaxPayerId = new TaxPayerId( innCode: "Inn3", kppCode: null ),
                            ParentInn = RootInn
                        } );
                        nodes.Add( new GraphData
                        {
                            Id = GraphDataTestID,
                            TaxPayerId = new TaxPayerId( innCode: "Inn4", kppCode: null ),
                            ParentInn = RootInn
                        } );
                        break;
                    case "Inn2":

                        nodes.Add( new GraphData
                        {
                            Id = GraphDataTestID,
                            TaxPayerId = new TaxPayerId( innCode: "Inn5", kppCode: null ),
                            ParentInn = "Inn2"
                        } );
                        nodes.Add( new GraphData
                        {
                            Id = GraphDataTestID,
                            TaxPayerId = new TaxPayerId( innCode: "Inn6", kppCode: null ),
                            ParentInn = "Inn2"
                        } );
                        nodes.Add( new GraphData
                        {
                            Id = GraphDataTestID,
                            TaxPayerId = new TaxPayerId( innCode: "Inn5_1", kppCode: null ),
                            ParentInn = "Inn2"
                        } );
                        nodes.Add( new GraphData
                        {
                            Id = GraphDataTestID,
                            TaxPayerId = new TaxPayerId( innCode: "Inn6_2", kppCode: null ),
                            ParentInn = "Inn2"
                        } );

                        break;

                    case "Inn3":

                        nodes.Add( new GraphData
                        {
                            Id = GraphDataTestID,
                            TaxPayerId = new TaxPayerId( innCode: "Inn7", kppCode: null ),
                            ParentInn = "Inn3"
                        } );
                        nodes.Add( new GraphData
                        {
                            Id = GraphDataTestID,
                            TaxPayerId = new TaxPayerId( innCode: "Inn7_2", kppCode: null ),
                            ParentInn = "Inn3"
                        } );
                        nodes.Add( new GraphData
                        {
                            Id = GraphDataTestID,
                            TaxPayerId = new TaxPayerId( innCode: "Inn7_3", kppCode: null ),
                            ParentInn = "Inn3"
                        } );
                        nodes.Add( new GraphData
                        {
                            Id = GraphDataTestID,
                            TaxPayerId = new TaxPayerId( innCode: "Inn7_4", kppCode: null ),
                            ParentInn = "Inn3"
                        } );
                        nodes.Add( new GraphData
                        {
                            Id = GraphDataTestID,
                            TaxPayerId = new TaxPayerId( innCode: "Inn7_5", kppCode: null ),
                            ParentInn = "Inn3"
                        } );
                        break;
                    case "Inn4":

                        nodes.Add( new GraphData
                        {
                            Id = GraphDataTestID,
                            TaxPayerId = new TaxPayerId( innCode: "Inn8", kppCode: null ),
                            ParentInn = "Inn4"
                        } );
                        nodes.Add( new GraphData
                        {
                            Id = GraphDataTestID,
                            TaxPayerId = new TaxPayerId( innCode: "Inn9", kppCode: null ),
                            ParentInn = "Inn4"
                        } );
                        nodes.Add( new GraphData
                        {
                            Id = GraphDataTestID,
                            TaxPayerId = new TaxPayerId( innCode: "Inn8_1", kppCode: null ),
                            ParentInn = "Inn4"
                        } );
                        nodes.Add( new GraphData
                        {
                            Id = GraphDataTestID,
                            TaxPayerId = new TaxPayerId( innCode: "Inn9_1", kppCode: null ),
                            ParentInn = "Inn4"
                        } );
                        nodes.Add( new GraphData
                        {
                            Id = GraphDataTestID,
                            TaxPayerId = new TaxPayerId( innCode: "Inn8_2", kppCode: null ),
                            ParentInn = "Inn4"
                        } );
                        nodes.Add( new GraphData
                        {
                            Id = GraphDataTestID,
                            TaxPayerId = new TaxPayerId( innCode: "Inn9_2", kppCode: null ),
                            ParentInn = "Inn4"
                        } );
                        nodes.Add( new GraphData
                        {
                            Id = GraphDataTestID,
                            TaxPayerId = new TaxPayerId( innCode: "Inn8_3", kppCode: null ),
                            ParentInn = "Inn4"
                        } );
                        nodes.Add( new GraphData
                        {
                            Id = GraphDataTestID,
                            TaxPayerId = new TaxPayerId( innCode: "Inn9_4", kppCode: null ),
                            ParentInn = "Inn4"
                        } );
                        break;
                    case "Inn8":

                        nodes.Add( new GraphData
                        {
                            Id = GraphDataTestID,
                            TaxPayerId = new TaxPayerId( innCode: "Inn10", kppCode: null ),
                            ParentInn = "Inn8"
                        } );
                        break;
                    case "Inn9":

                        nodes.Add( new GraphData
                        {
                            Id = GraphDataTestID,
                            TaxPayerId = new TaxPayerId( innCode: "Inn11", kppCode: null ),
                            ParentInn = "Inn9"
                        } );
                        break;
                }
            }

            return nodes;
        }

        private IEnumerable<GraphData> GetTestTree_SpecInns( GraphNodesKeyParameters keyParameters, CancellationToken cancelToken )
        {
            Contract.Requires( keyParameters != null );
            Contract.Ensures( Contract.Result<IEnumerable<GraphData>>() != null );

            var nodes = new List<GraphData>(5);
            foreach ( var id in keyParameters.TaxPayerIds )
            {
                switch ( id.Inn )
                {case RootInn:
                    nodes.Add( new GraphData
                    {
                        Id = GraphDataTestID,
                        TaxPayerId = new TaxPayerId( innCode: RootInn, kppCode: null ),
                        ParentInn = null
                    } );
                    nodes.Add( new GraphData
                    {
                        Id = GraphDataTestID,
                        TaxPayerId = new TaxPayerId( innCode: "Inn2", kppCode: null ),
                        ParentInn = RootInn
                    } );
                    nodes.Add( new GraphData
                    {
                        Id = GraphDataTestID,
                        TaxPayerId = new TaxPayerId( innCode: "Inn3", kppCode: null ),
                        ParentInn = RootInn
                    } );
                    nodes.Add( new GraphData
                    {
                        Id = GraphDataTestID,
                        TaxPayerId = new TaxPayerId( innCode: "Inn4", kppCode: null ),
                        ParentInn = RootInn
                    } );
                    nodes.Add( new GraphData
                    {
                        Id = GraphDataTestID,
                        TaxPayerId = new TaxPayerId( innCode: "0000000026", kppCode: null ),
                        ParentInn = RootInn
                    } );
                    nodes.Add( new GraphData
                    {
                        Id = GraphDataTestID,
                        TaxPayerId = new TaxPayerId( innCode: "0000000020", kppCode: null ),
                        ParentInn = RootInn
                    } );
                    break;

                case "Inn2":

                    nodes.Add( new GraphData
                    {
                        Id = GraphDataTestID,
                        TaxPayerId = new TaxPayerId( innCode: "Inn5", kppCode: null ),
                        ParentInn = "Inn2"
                    } );
                    nodes.Add( new GraphData
                    {
                        Id = GraphDataTestID,
                        TaxPayerId = new TaxPayerId( innCode: "Inn6", kppCode: null ),
                        ParentInn = "Inn2"
                    } );
                    nodes.Add( new GraphData
                    {
                        Id = GraphDataTestID,
                        TaxPayerId = new TaxPayerId( innCode: "Inn5_1", kppCode: null ),
                        ParentInn = "Inn2"
                    } );
                    nodes.Add( new GraphData
                    {
                        Id = GraphDataTestID,
                        TaxPayerId = new TaxPayerId( innCode: "Inn6_2", kppCode: null ),
                        ParentInn = "Inn2"
                    } );

                    break;

                case "Inn3":

                    nodes.Add( new GraphData
                    {
                        Id = GraphDataTestID,
                        TaxPayerId = new TaxPayerId( innCode: "Inn7", kppCode: null ),
                        ParentInn = "Inn3"
                    } );
                    nodes.Add( new GraphData
                    {
                        Id = GraphDataTestID,
                        TaxPayerId = new TaxPayerId( innCode: "Inn7_2", kppCode: null ),
                        ParentInn = "Inn3"
                    } );
                    nodes.Add( new GraphData
                    {
                        Id = GraphDataTestID,
                        TaxPayerId = new TaxPayerId( innCode: "Inn7_3", kppCode: null ),
                        ParentInn = "Inn3"
                    } );
                    nodes.Add( new GraphData
                    {
                        Id = GraphDataTestID,
                        TaxPayerId = new TaxPayerId( innCode: "Inn7_4", kppCode: null ),
                        ParentInn = "Inn3"
                    } );
                    nodes.Add( new GraphData
                    {
                        Id = GraphDataTestID,
                        TaxPayerId = new TaxPayerId( innCode: "Inn7_5", kppCode: null ),
                        ParentInn = "Inn3"
                    } );
                    break;
                case "Inn4":

                    nodes.Add( new GraphData
                    {
                        Id = GraphDataTestID,
                        TaxPayerId = new TaxPayerId( innCode: "Inn8", kppCode: null ),
                        ParentInn = "Inn4"
                    } );
                    nodes.Add( new GraphData
                    {
                        Id = GraphDataTestID,
                        TaxPayerId = new TaxPayerId( innCode: "Inn9", kppCode: null ),
                        ParentInn = "Inn4"
                    } );
                    nodes.Add( new GraphData
                    {
                        Id = GraphDataTestID,
                        TaxPayerId = new TaxPayerId( innCode: "Inn8_1", kppCode: null ),
                        ParentInn = "Inn4"
                    } );
                    nodes.Add( new GraphData
                    {
                        Id = GraphDataTestID,
                        TaxPayerId = new TaxPayerId( innCode: "Inn9_1", kppCode: null ),
                        ParentInn = "Inn4"
                    } );
                    nodes.Add( new GraphData
                    {
                        Id = GraphDataTestID,
                        TaxPayerId = new TaxPayerId( innCode: "Inn8_2", kppCode: null ),
                        ParentInn = "Inn4"
                    } );
                    nodes.Add( new GraphData
                    {
                        Id = GraphDataTestID,
                        TaxPayerId = new TaxPayerId( innCode: "Inn9_2", kppCode: null ),
                        ParentInn = "Inn4"
                    } );
                    nodes.Add( new GraphData
                    {
                        Id = GraphDataTestID,
                        TaxPayerId = new TaxPayerId( innCode: "Inn8_3", kppCode: null ),
                        ParentInn = "Inn4"
                    } );
                    nodes.Add( new GraphData
                    {
                        Id = GraphDataTestID,
                        TaxPayerId = new TaxPayerId( innCode: "Inn9_4", kppCode: null ),
                        ParentInn = "Inn4"
                    } );
                    break;
                case "Inn8":

                    nodes.Add( new GraphData
                    {
                        Id = GraphDataTestID,
                        TaxPayerId = new TaxPayerId( innCode: "0000000020", kppCode: null ),
                        ParentInn = "Inn8"
                    } );
                    break;
                case "Inn9":

                    nodes.Add( new GraphData
                    {
                        Id = GraphDataTestID,
                        TaxPayerId = new TaxPayerId( innCode: "0000000001", kppCode: null ),
                        ParentInn = "Inn9"
                    } );
                    break;

                case "0000000001":
                case "0000000019":
                case "0000000020":
                case "0000000026":
                    //nodes of special INN do not have chilren
                    throw new InvalidOperationException( string.Format( "A node with special INN: '{0}' can not been requested for its children", id.Inn ) );
                }
            }

            return nodes;
        }

        private static void WriteLine( string message, params object[] formatArguments )
        {
            message = string.Format( message, formatArguments );

            ServiceCallDispatcherTestHelper.WriteLine( message );
        }
    }
}
#endif //MSTEST
