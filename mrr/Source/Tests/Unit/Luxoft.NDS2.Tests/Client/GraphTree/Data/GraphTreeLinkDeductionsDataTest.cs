﻿using System;
using System.Collections.Generic;
using System.Linq;
using FLS.CommonComponents.Lib.Exceptions;
using Luxoft.NDS2.Client.NodeTreePanel;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using NUnit.Framework;

namespace Luxoft.NDS2.Tests.Client.GraphTree.Data
{
    [TestFixture]
    public sealed class GraphTreeLinkDeductionsDataTest
    {
        [SetUp]
        public void SetUp()
        {
            
        }

        [Test]
        [TestCase( "2016~4~1234567890123456789.33^2015~1~333^-0~-0~-12345,0330^0~0~0" )]
        [TestCase( "2016~4~1234567890123456789.33^2015~1~333.^2014~2~1234567890123456789012,00^-2000~-3~12345.33" )]
        [TestCase( "2016~4~1234567890123456789.330000" )]
        [TestCase( "" )]
        [TestCase( null )]
        public void ConvertSpecificationToDeductionDetails( string specification )
        {
            IEnumerable<DeductionDetail> deductionDetails = 
                GraphTreeLinkDeductionsData.ConvertSpecificationToDeductionDetails( specification );
            int count = 0;
            foreach ( DeductionDetail deductionDetail in deductionDetails )
            {
                ++count;
                if ( count == 1 )
                {
                    Assert.IsTrue( deductionDetail.PeriodOrYear.Year == 2016 );
                    Assert.IsTrue( deductionDetail.PeriodOrYear.QuarterNumber == 4 );
                    Assert.IsTrue( deductionDetail.DecuctionAmount == 1234567890123456789.33m );
                }
                else if ( count == 2 )
                {
                    Assert.IsTrue( deductionDetail.PeriodOrYear.Year == 2015 );
                    Assert.IsTrue( deductionDetail.PeriodOrYear.QuarterNumber == 1 );
                    Assert.IsTrue( deductionDetail.DecuctionAmount == 333m );
                }
            }
            Assert.IsTrue( 0 < count && count < 4 || string.IsNullOrEmpty( specification ) );
        }

        [Test]
        [TestCase( "2016n~4_~O.33^g2015~1d~333m" )]
        [TestCase( "20 16~e 4~1234567890123456789*33^x2015~~333.XX^2014|~\"2~1234567890123456789a012.00" )]
        [TestCase( "ertg~ ~1234567890123456789 .330000" )]
        [TestCase( " " )]
        public void ConvertSpecificationToDeductionDetails_Throw( string specification )
        {
            if ( specification != string.Empty && string.IsNullOrWhiteSpace( specification ) )
                Assert.That( () => GraphTreeLinkDeductionsData.ConvertSpecificationToDeductionDetails( specification ).ToArray(), 
                             Throws.Exception.AssignableTo<Exception>() );
            else
                Assert.Throws<GenericException<ParsingExceptionArgs>>(
                    () => GraphTreeLinkDeductionsData.ConvertSpecificationToDeductionDetails( specification ).ToArray() );
        }
    }
}