﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Tests.Client.GraphTree.Data.Auxiliaries;
using NUnit.Framework;

namespace Luxoft.NDS2.Tests.Client.GraphTree.Data.Helpers
{
    /// <summary> A test helper for <see cref="IServiceCallDispatcher{TParam, TInstanceKey, TContextKey, TData, TResult}"/> </summary>
    public class ServiceCallDispatcherTestHelper
    {
        /// <summary> Tests on service call dispatching concurrently. </summary>
        public static void ConcurrentCallTest( 
            Func<CancellationToken?, IServiceCallDispatcher<GraphNodesKeyParameters, int, string, List<GraphData>, List<GraphData>>> createCallDispatcher, 
            Func<IServiceCallDispatcher<GraphNodesKeyParameters, int, string, List<GraphData>, List<GraphData>>, int, CancellationToken, bool> testHandler, 
            Action<IServiceCallDispatcher<GraphNodesKeyParameters, int, string, List<GraphData>, List<GraphData>>> testFinishHandler, 
            int testLengthMillisec, int threadNumber, int sleepMillisec )
        {
            var cancellSource = new CancellationTokenSource();
            CancellationToken cancelToken = cancellSource.Token;

            Task<IServiceCallDispatcher<GraphNodesKeyParameters, int, string, List<GraphData>, List<GraphData>>>[] concurrentTasks = 
                new Task<IServiceCallDispatcher<GraphNodesKeyParameters, int, string, List<GraphData>, List<GraphData>>>[ threadNumber ];
            for ( int nI = 0; nI < concurrentTasks.Length; nI++ )
            {
                concurrentTasks[nI] = new Task<IServiceCallDispatcher<GraphNodesKeyParameters, int, string, List<GraphData>, List<GraphData>>>(
                    () =>
                    {
                        IServiceCallDispatcher<GraphNodesKeyParameters, int, string, List<GraphData>, List<GraphData>> callDispatcher;
                        while ( true )
                        {
                            callDispatcher = createCallDispatcher( cancelToken );

                            Assert.IsNotNull( callDispatcher );

                            if ( !testHandler( callDispatcher, sleepMillisec, cancelToken ) )
                            {
                                if ( sleepMillisec > 0 ) Thread.Sleep( sleepMillisec );
                            }
                            testFinishHandler( callDispatcher );

                            ServiceCallDispatcherTestObj srvCallDispatcherTestObj = (ServiceCallDispatcherTestObj)callDispatcher;
                            ServiceCallDispatcherTestHelper.WriteLine( "ServiceCallDispatcher is released, Id: {0}", srvCallDispatcherTestObj.TestId );

                            if ( cancellSource.IsCancellationRequested )
                                break;
                        }

                        return callDispatcher;
                    }
                    //, cancelToken 
                    );
            }
            foreach ( Task<IServiceCallDispatcher<GraphNodesKeyParameters, int, string, List<GraphData>, List<GraphData>>> task in concurrentTasks )
            {
                task.Start();
            }

            int finishedTaskIndex = Task.WaitAny( concurrentTasks, testLengthMillisec );

            cancellSource.Cancel();

            try
            {
                foreach ( Task<IServiceCallDispatcher<GraphNodesKeyParameters, int, string, List<GraphData>, List<GraphData>>> task in concurrentTasks )
                {
                    Assert.IsNotNull( task.Result );    //can throw an exception if such one has been caused inside a task
                }
            }
            catch ( Exception )
            {
                Task.WaitAll( concurrentTasks );    //waits while task will be completed before test finish to don't share test files with sequent tests

                throw;
            }
            Assert.IsTrue( -1 == finishedTaskIndex );
        }

        /// <summary> Tests on batch service call dispatching concurrently. </summary>
        public static void ConcurrentCallTest( 
            Func<CancellationToken?, IServiceCallBatchDispatcher<GraphNodesKeyParameters, int, string>> createCallDispatcher, 
            Func<IServiceCallBatchDispatcher<GraphNodesKeyParameters, int, string>, int, CancellationToken, bool> testHandler, 
            Action<IServiceCallBatchDispatcher<GraphNodesKeyParameters, int, string>> testFinishHandler, 
            int testLengthMillisec, int threadNumber, int sleepMillisec )
        {
            var cancellSource = new CancellationTokenSource();
            CancellationToken cancelToken = cancellSource.Token;

            Task<IServiceCallBatchDispatcher<GraphNodesKeyParameters, int, string>>[] concurrentTasks = 
                new Task<IServiceCallBatchDispatcher<GraphNodesKeyParameters, int, string>>[ threadNumber ];
            for ( int nI = 0; nI < concurrentTasks.Length; nI++ )
            {
                concurrentTasks[nI] = new Task<IServiceCallBatchDispatcher<GraphNodesKeyParameters, int, string>>(
                    () =>
                    {
                        IServiceCallBatchDispatcher<GraphNodesKeyParameters, int, string> callDispatcher;
                        while ( true )
                        {
                            callDispatcher = createCallDispatcher( cancelToken );

                            Assert.IsNotNull( callDispatcher );

                            if ( !testHandler( callDispatcher, sleepMillisec, cancelToken ) )
                            {
                                if ( sleepMillisec > 0 ) Thread.Sleep( sleepMillisec );
                            }
                            testFinishHandler( callDispatcher );

                            ServiceCallBatchDispatcherTestObj srvCallDispatcherTestObj = (ServiceCallBatchDispatcherTestObj)callDispatcher;
                            ServiceCallDispatcherTestHelper.WriteLine( "ServiceCallDispatcher is released, Id: {0}", srvCallDispatcherTestObj.TestId );

                            if ( cancellSource.IsCancellationRequested )
                                break;
                        }

                        return callDispatcher;
                    }
                    //, cancelToken 
                    );
            }
            foreach ( Task<IServiceCallBatchDispatcher<GraphNodesKeyParameters, int, string>> task in concurrentTasks )
            {
                task.Start();
            }

            int finishedTaskIndex = Task.WaitAny( concurrentTasks, testLengthMillisec );

            Debug.WriteLine( "finishedTaskIndex == '{0}'", finishedTaskIndex );

            cancellSource.Cancel();

            try
            {
                foreach ( Task<IServiceCallBatchDispatcher<GraphNodesKeyParameters, int, string>> task in concurrentTasks )
                {
                    Assert.IsNotNull( task.Result );    //can throw an exception if such one has been caused inside a task
                }
            }
            catch ( Exception )
            {
                Task.WaitAll( concurrentTasks );    //waits while task will be completed before test finish to don't share test files with sequent tests

                throw;
            }
            Assert.IsTrue( -1 == finishedTaskIndex );
        }

        /// <summary> </summary>
        /// <param name="message"></param>
        /// <param name="formatArguments"></param>
        public static void WriteLine( string message, params object[] formatArguments )
        {
            message = string.Format( message, formatArguments );

            Debug.WriteLine( message );
        }
    }
}