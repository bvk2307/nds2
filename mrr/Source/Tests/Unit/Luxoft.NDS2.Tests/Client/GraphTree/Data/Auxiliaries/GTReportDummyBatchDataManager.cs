﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using FLS.Common.Lib.Collections.Extensions;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting.TaxPayer;
using Luxoft.NDS2.Common.Helpers.Executors;

namespace Luxoft.NDS2.Tests.Client.GraphTree.Data.Auxiliaries
{
    /// <summary> A dummy strategy controls batch data processing that generates the single request each time. </summary>
    /// <remarks> Does not support concurrent usage from the different threads at the same time. </remarks>
    public sealed class GTReportDummyBatchDataManager : IBatchDataLoadStrategy<GraphNodesKeyParameters, int, string, IEnumerable<GraphData>, IEnumerable<GraphDataNode>>
    {
        private readonly int _callCountLimit;
        private readonly bool _throwInside;

        private int _callCount = 0;
        private GraphNodesKeyParameters _rootKeyParameters = null;
        private CallExecContext<GraphNodesKeyParameters, int, string, IEnumerable<GraphData>> _callExecContext = null;
        private IEnumerable<GraphDataNode> _buffer;

        public GTReportDummyBatchDataManager( int callCountLimit, bool throwInside = false )
        {
            Contract.Requires( callCountLimit >= 0 );

            _callCountLimit = callCountLimit;
            _throwInside = throwInside;
        }

        public GraphNodesKeyParameters KeyParameters
        {
            get { return _rootKeyParameters; }
        }

        /// <summary> Initializes the strategy parameters. </summary>
        /// <param name="keyParameters"></param>
        public void Init( GraphNodesKeyParameters keyParameters )
        {
            _rootKeyParameters = keyParameters;
        }

        /// <summary> Buffers recieved datas in the internal buffer before post processing. </summary>
        /// <param name="callExecContext"></param>
        public void Buffer( CallExecContext<GraphNodesKeyParameters, int, string, IEnumerable<GraphData>> callExecContext )
        {
            _callExecContext = callExecContext;
            _buffer = callExecContext.Result.Select( graphData => new GraphDataNode( graphData ) );
        }

        /// <summary> Returns next data requests that should be requested at the bounds of the batch. </summary>
        /// <returns> The next data requests or 'null' if the completion is requested instead of. </returns>
        public IReadOnlyCollection<GraphNodesKeyParameters> 
            GetNextToPostOrFinish()
        {
            IEnumerable<GraphNodesKeyParameters> toPost = null;
            if ( ++_callCount <= _callCountLimit )
            {
                if ( _throwInside && _callCount == _callCountLimit - 4 )    //10 for debug )    //1 )
                    throw new ApplicationException( string.Format( "TEST EXCEPTION on try #: {0}", _callCount ) );

                toPost = from graphData in _buffer select new GraphNodesKeyParameters(
                    KeyParameters.IsPurchase, KeyParameters.TaxQuarter, KeyParameters.TaxYear, new[] { new TaxPayerId(_callCount + "_" + graphData.Inn, kppCode: null)});
            }
            return toPost == null ? null : toPost.ToReadOnly();
        }

        /// <summary> Takes next datas from the internal buffer to post process. </summary>
        /// <returns></returns>
        public CallExecContext<GraphNodesKeyParameters, int, string, IEnumerable<GraphDataNode>> 
            GetNextToOffer()
        {
            var toOffer = new CallExecContext<GraphNodesKeyParameters, int, string, IEnumerable<GraphDataNode>>( 
                _callExecContext.Parameters, _callExecContext.InstanceKey, _callExecContext.ContextKey, _buffer );

            return toOffer;
        }
    }
}