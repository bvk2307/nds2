﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using NUnit.Framework;

namespace Luxoft.NDS2.Tests.Client
{
    [TestFixture]
    public class DeclarationCardDataTest
    {
        [Test]
        public void KeyTest()
        {
            DeclarationCardData item = new DeclarationCardData("02656842305171881984/0/0000000001", string.Empty, 0);

            long id = item.Id;
            DeclarationPart part = item.KeyDeclarationPart;

            Assert.AreEqual(id, 02656842305171881984);
            Assert.AreEqual(part, DeclarationPart.R8);
        }

        [Test]
        [ExpectedException()]
        public void KeyTestException()
        {
            DeclarationCardData item = new DeclarationCardData("abracadabra", string.Empty, 0);

            long id = item.Id;
            DeclarationPart part = item.KeyDeclarationPart;
        }



    }
}
