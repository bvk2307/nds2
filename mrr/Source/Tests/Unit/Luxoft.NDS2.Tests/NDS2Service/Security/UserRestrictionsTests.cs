﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using CommonComponents.Security.Authorization;
using Luxoft.NDS2.Common.Contracts.DTO.Security;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.DAL.CFG;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Server.Services.Managers.Authorization;
using NUnit.Framework;

namespace Luxoft.NDS2.Tests.NDS2Service.Security
{
    [TestFixture]
    public class UserRestrictionsTest
    {
        [Test]
        public void KeyRestrictionEqualsTest()
        {
            var item1 = new UserRestrictionKey(UserRole.Analyst, null);
            var item2 = new UserRestrictionKey(UserRole.Analyst, null);
            var item3 = new UserRestrictionKey(UserRole.Analyst, InspectionGroup.CA);

            Assert.AreEqual(item1, item2);
            Assert.AreNotEqual(item1, item3);
        }

        [Test]
        public void KeyRestrictionDictionaryTest()
        {
            var item1 = new UserRestrictionKey(UserRole.Analyst, null);
            var item2 = new UserRestrictionKey(UserRole.Analyst, null);
            var item3 = new UserRestrictionKey(UserRole.Analyst, InspectionGroup.CA);
            var testDictionary = new Dictionary<UserRestrictionKey, int>();

            testDictionary.Add(item1, 0);

            Assert.Throws<ArgumentException>(() => testDictionary.Add(item2, 0));
            Assert.DoesNotThrow(() => testDictionary.Add(item3, 0));
        }

        [Test]
        public void UserRestrictionsCreatorTest()
        {
            var authProvider = new AuthProviderStub();
            var grpInspAdapter = new InspectionGroupAdapterStub();
            var restrictAdapter = new RestrictionChainsAdapterStub();

            var restrictionCreator = new ContractorDataPolicyProvider(authProvider, grpInspAdapter, restrictAdapter);

            ContractorDataPolicy cfgItem = restrictionCreator.GetPermissions();

            Assert.IsTrue(cfgItem.NotRestricted);
        }

        #region Stubs for UserRestrictionsCreatorTest

        private class AuthProviderStub : IAuthorizationProvider
        {

            public string CurrentUserName
            {
                get { throw new NotImplementedException(); }
            }

            public List<AccessRight> GetUserPermissions()
            {
                return new List<AccessRight>()
                {
                    new AccessRight() {Name = "Роль.Аналитик", PermType = PermissionType.Operation}
                };
            }

            public List<AccessRight> GetUserPermissions(string SID)
            {
                throw new NotImplementedException();
            }

            public void ChangeUserRole(string roleName)
            {
                throw new NotImplementedException();
            }

            public bool IsOperationEligible(string operationName)
            {
                throw new NotImplementedException();
            }

            public bool IsUserInRole(string roleName)
            {
                throw new NotImplementedException();
            }

            public bool IsUserInRole(string userSID, string roleName)
            {
                throw new NotImplementedException();
            }

            public string CurrentUserSID
            {
                get { throw new NotImplementedException(); }
            }

            public List<string> GetUserSIDs()
            {
                throw new NotImplementedException();
            }

            public List<Common.Contracts.DTO.Business.Security.UserStructContextRight> GetStructContextUserRigths(string ifns)
            {
                throw new NotImplementedException();
            }

            public IList<string> GetUserStructContexts(CommonComponents.Security.Authorization.PermissionType permType, string objectName)
            {
                return new List<string>()
                {
                    "0000"
                };
            }


            public List<Common.Contracts.DTO.Business.Security.UserStructContextRight> GetStructContextUsersByRole(string ifns, string roleOperation)
            {
                throw new NotImplementedException();
            }
        }

        private class InspectionGroupAdapterStub : IInspectionGroupAdapter
        {
            public List<InspectionGroup> Search(IList<string> list)
            {
                return new List<InspectionGroup>()
                {
                    InspectionGroup.CA
                };
            }
        }

        private class RestrictionChainsAdapterStub : IRestrictionChainsAdapter
        {
            public IDictionary<UserRestrictionKey, ConfigurationRestrictionsForChains> All()
            {
                return new Dictionary<UserRestrictionKey, ConfigurationRestrictionsForChains>()
                {
                    {new UserRestrictionKey(UserRole.Analyst, InspectionGroup.CA), new ConfigurationRestrictionsForChains() {NotRestricted = true, AllowPurchase = true}},
                    {new UserRestrictionKey(UserRole.Analyst, InspectionGroup.MIKK), new ConfigurationRestrictionsForChains() {NotRestricted = true, AllowPurchase = true}},
                    {new UserRestrictionKey(UserRole.Approver, InspectionGroup.CA), new ConfigurationRestrictionsForChains() {NotRestricted = true, AllowPurchase = false}},
                    {new UserRestrictionKey(UserRole.Approver, InspectionGroup.MIKK), new ConfigurationRestrictionsForChains() {NotRestricted = true, AllowPurchase = false}},
                    {new UserRestrictionKey(UserRole.Methodologist, InspectionGroup.CA), new ConfigurationRestrictionsForChains() {NotRestricted = true, AllowPurchase = true}},
                    {new UserRestrictionKey(UserRole.Inspector, InspectionGroup.CA), new ConfigurationRestrictionsForChains() {NotRestricted = true, AllowPurchase = true}},
                    {new UserRestrictionKey(UserRole.Inspector, InspectionGroup.MILarge), new ConfigurationRestrictionsForChains() {NotRestricted = false, AllowPurchase = true}},
                    {new UserRestrictionKey(UserRole.Inspector, InspectionGroup.UFNS), new ConfigurationRestrictionsForChains() {NotRestricted = false, AllowPurchase = true}},
                    {new UserRestrictionKey(UserRole.Inspector, null), new ConfigurationRestrictionsForChains() {NotRestricted = false, AllowPurchase = false}},
                };
            }
        }

        #endregion

    }
}
