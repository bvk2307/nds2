﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using Luxoft.NDS2.Server.Services.Managers.Authorization;
using NUnit.Framework;

namespace Luxoft.NDS2.Tests.NDS2Service.Authorization
{
    [TestFixture]
    [Category("Manual")]
    public class AuthorizationTest
    {
        [Test]
        public void ParseAzmanTest()
        {
            DevAuthorizationProvider provider = new DevAuthorizationProvider(null);
            var identity = WindowsIdentity.GetCurrent();

            foreach (var mapping in provider.UserMappings)
            {
                Console.WriteLine("Sid = {0}", mapping.Key);
                Console.WriteLine("\tEnum roles");

                if(mapping.Key == identity.User.Value) Console.WriteLine("####Current account####");

                foreach (AzRole azRole in mapping.Value)
                {
                    Console.WriteLine("\tRole {0}", azRole.Name);
                    foreach (var azTask in azRole.Tasks)
                    {
                        Console.WriteLine("\t\tTask {0}", azTask.Name);
                        foreach (var azOperation in azTask.Operations)
                        {
                            Console.WriteLine("\t\t\tOperation {0}", azOperation.Name);
                        }
                    }
                }
            }
        }

        [Test]
        public void CurrentUserPermissions()
        {
            DevAuthorizationProvider provider = new DevAuthorizationProvider(null);

            foreach (var userPermission in provider.GetUserPermissions())
            {
                Console.WriteLine("{0} - {1}", userPermission.PermType.ToString(), userPermission.Name);
            }
        }
    }
}
