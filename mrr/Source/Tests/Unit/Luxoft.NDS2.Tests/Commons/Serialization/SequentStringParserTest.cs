﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Luxoft.NDS2.Common.Helpers.Serialization;
using NUnit.Framework;

namespace Luxoft.NDS2.Tests.Commons.Serialization
{
    [TestFixture]
    public sealed class SequentStringParserTest
    {
        private static readonly SequentStringParser s_intAndDecimalParser = 
            new SequentStringParser( recordDelimiters: new[] { "^" }, recordItemDelimiters: new [] { "~" } );

        [SetUp]
        public void SetUp()
        {
            
        }

        [Test]
        [TestCase( "2016~4~1234567890123456789.33^2015~1~333^0~0~12345,0330" )]
        [TestCase( "2016~4~1234567890123456789.33^2015~1~333.^2014~2~1234567890123456789012,00^-2000~-3~12345.33" )]
        [TestCase( "2016~4~1234567890123456789.330000" )]
        [TestCase( "" )]
        public void Parser_IntAndDecimal_Parse_Source( string specification )
        {
            IEnumerable<string[]> recordsFields = s_intAndDecimalParser.CreateRecordsIterator( specification );
            int count = 0;
            foreach ( string[] fields in recordsFields )
            {
                Assert.IsTrue( fields != null && fields.Length == 3 );

                ++count;
                int firstInt = int.Parse( fields[0] );
                int secondInt = int.Parse( fields[1] );
                decimal thirdDecimal = decimal.Parse( fields[2], CultureInfo.InvariantCulture );

                if ( count == 1 )
                {
                    Assert.IsTrue( firstInt == 2016 );
                    Assert.IsTrue( secondInt == 4 );
                    Assert.IsTrue( thirdDecimal == 1234567890123456789.33m );
                }
                else if ( count == 2 )
                {
                    Assert.IsTrue( firstInt == 2015 );
                    Assert.IsTrue( secondInt == 1 );
                    Assert.IsTrue( thirdDecimal == 333m );
                }
            }
            Assert.IsTrue( count > 0 || specification == string.Empty );
        }

        [Test]
        [TestCase( "2016n~4_~O.33^g2015~1d~333m" )]
        [TestCase( "20 16~e 4~1234567890123456789*33^x2015~~333.XX^2014|~\"2~1234567890123456789a012.00" )]
        [TestCase( "ertg~ ~1234567890123456789 .330000" )]
        [TestCase( " " )]
        [TestCase( null )]
        public void Parser_IntAndDecimal_ThrowOnParse_Source( string specification )
        {
            IEnumerable<string[]> recordsFields = Enumerable.Empty<string[]>();

            if ( specification != string.Empty && string.IsNullOrWhiteSpace( specification ) )
                Assert.That( () => s_intAndDecimalParser.CreateRecordsIterator( specification ), 
                             Throws.Exception.AssignableTo<Exception>() );
            else 
                recordsFields = s_intAndDecimalParser.CreateRecordsIterator( specification );

            int count = 0;
            foreach ( string[] fields in recordsFields )
            {
                Assert.IsTrue( fields != null && 1 <= fields.Length && fields.Length <= 3 );

                ++count;
                Assert.Throws<FormatException>( () => int.Parse( fields[0] ) );
                Assert.Throws<FormatException>( () => int.Parse( fields[1] ) );
                Assert.Throws<FormatException>( () => decimal.Parse( fields[2], CultureInfo.InvariantCulture ) );
            }
        }
    }
}