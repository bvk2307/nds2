﻿//using System;
//using System.IO;
//using System.Net;
//using NUnit.Framework;
//using Newtonsoft.Json;
//
//namespace Luxoft.NDS2.Tests
//{
//    [TestFixture]
//    public class SovServiceTest
//    {
//        [Test]
//        [Category("Manual")]
//        public void GetSellBookSmokeTest()
//        {
//            GetBookSmokeTest("sellBook");
//        }
//
//        [Test]
//        [Category("Manual")]
//        public void GetBuyBookSmokeTest()
//        {
//            GetBookSmokeTest("buyBook");
//        }
//
//        private void GetBookSmokeTest(string bookType)
//        {
//            WebRequest request = WebRequest.Create(string.Format("http://localhost:8787/{0}/2014-01/1/1", bookType));
//
//            using (var response = request.GetResponse())
//            {
//                using (StreamReader textReader = new StreamReader(response.GetResponseStream()))
//                {
//                    using (JsonTextReader reader = new JsonTextReader(textReader))
//                    {
//                        while (reader.Read())
//                        {
//                            Console.WriteLine("{0} - {1}", reader.TokenType, reader.Value);
//                        }
//                    }
//                }
//            }
//        }
//    }
//}
