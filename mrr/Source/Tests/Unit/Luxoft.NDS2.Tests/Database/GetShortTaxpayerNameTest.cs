﻿using System.Linq;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Tests.__Stubs;
using Luxoft.NDS2.Tests.Dal.DataGeneration;
using NUnit.Framework;
using System.Transactions;

namespace Luxoft.NDS2.Tests.Dal
{
    [TestFixture]
    public class GetShortTaxpayerNameTest
    {
        ///<summary>
        /// Проверка сокращений ОПФ
        /// </summary>
        [Test]
        public void AbbreviationTest()
        {
            using (new TransactionScope())
            {
                var services = new ServiceContext();
                var helpers = new ServiceHelpers(services);
                var sql = new SqlTestHelper(helpers);

                AssertAbbreviation(sql, "  индивидуальный  предприниматель     иван",        "ИП ИВАН");
                AssertAbbreviation(sql, "Общество с ограниченной ответственностью Иттрий",   "ООО ИТТРИЙ");
                AssertAbbreviation(sql, "Публичное акционерное общество Стронций",           "ПАО СТРОНЦИЙ");
                AssertAbbreviation(sql, "открытое акционерное общество Скандий",             "ОАО СКАНДИЙ");
                AssertAbbreviation(sql, "Индивидуальный предприниматель Германий",           "ИП ГЕРМАНИЙ");
                AssertAbbreviation(sql, "Непубличное акционерное общество Рубидий",          "НАО РУБИДИЙ");
            }
        }

        private static void AssertAbbreviation(SqlTestHelper sql, string baseStr, string expectedStr)
        {
            string request = string.Concat("select NDS2_MRR_USER.F$GET_SHORT_TAXPAYER_NAME('", baseStr, "') as result from dual");

            string rez = sql.GetData(request, (r) => r.GetString("result")).First();

            Assert.AreEqual(expectedStr, rez);
        }
    }
}
