﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Tests.Dal.DataGeneration;
using Luxoft.NDS2.Tests.__Stubs;
using NUnit.Framework;

namespace Luxoft.NDS2.Tests.Dal
{
	[TestFixture]
	public class AddWorkDaysTest
	{
		private readonly List<TestParam> TestData = new ListTestParam
		{
			{"25.11.2016", 28, "16.01.2017"},
			{"24.11.2016", 2, "28.11.2016"},
			{"25.11.2016", 27, "13.01.2017"},
			{"25.11.2016", 1, "28.11.2016"}
		};

		[Test]
		[TestCaseSource("TestData")]
		public void ParameterizedTest(TestParam testParam)
		{
			using (new TransactionScope())
			{
				var services = new ServiceContext();
				var helpers = new ServiceHelpers(services);
				var sql = new SqlTestHelper(helpers);

				var request = string.Format(
					"select nds2_mrr_user.pac$calendar.f$add_work_days(to_date('{0}'),{1}) as result from dual"
					, testParam.InDate
					, testParam.InDays);

				var result = sql.GetData(request, r => r.GetDate("result")).First().ToString("dd.MM.yyyy");

				Assert.AreEqual(testParam.ExpectedResult, result);
			}
		}
	}

	public class TestParam
	{
		public TestParam(string inDate, int inDays, string expectedResult)
		{
			InDate = inDate;
			InDays = inDays;
			ExpectedResult = expectedResult;
		}

		public string InDate { get; set; }
		public int InDays { get; set; }
		public string ExpectedResult { get; set; }
	}
	class ListTestParam : List<TestParam>
	{
		public void Add(string inDate, int inDays, string expectedResult)
		{
			Add(new TestParam(inDate, inDays, expectedResult));
		}
	}
}
