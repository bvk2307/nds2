﻿using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using NUnit.Framework;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Tests.Others
{
    [TestFixture]
    [Category("Manual")]
    public class ImpalaTests
    {
        [Test]
        public void ValuesFormatInfo()
        {
            List<string> fields = new List<string>();
            List<string> targetTables = new List<string>()
            {
             "target_nds8",
             "target_nds9",
             "target_nds10",
             "target_nds11",
             "target_nds12",
             "target_nds13",
             "target_nds14",
            };

            using (OdbcConnection conn = new OdbcConnection("Driver={Cloudera ODBC Driver for Impala};Host=m9965-hdp013;Database=default;Port=21050"))
            {
                conn.Open();
                using (OdbcCommand cmd = new OdbcCommand("select * from target_nds8 limit 1", conn))
                {
                    using (OdbcDataReader reader = cmd.ExecuteReader())
                    {
                        reader.Read();
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            fields.Add(reader.GetName(i));
                        }
                    }
                }

                foreach (var targetTable in targetTables)
                {

                    using (StreamWriter wr = new StreamWriter(targetTable + ".txt"))
                    {
                        Console.WriteLine("Table - {0}", targetTable);
                        foreach (var field in fields)
                        {
                            Console.WriteLine("\t -------------- ");
                            Console.WriteLine("\t field - {0}", field);

                            wr.WriteLine("\t -------------- ");
                            wr.WriteLine("\t field - {0}", field);

                            using (OdbcCommand cmd = new OdbcCommand(string.Format("select * from {0} where {1} is not null limit 3", targetTable, field), conn))
                            {
                                using (OdbcDataReader reader = cmd.ExecuteReader())
                                {
                                    while (reader.Read())
                                    {
                                        wr.WriteLine("\t {0}", reader[field]);
                                        Console.WriteLine("\t {0}", reader[field]);
                                    }
                                }
                            }
                        }                        
                    }
                }
            }
        }
    }
}
