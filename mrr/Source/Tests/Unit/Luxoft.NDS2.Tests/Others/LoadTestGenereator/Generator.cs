﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Tests.Others.Common;
using Luxoft.NDS2.Tests.Others.Entity;
using Luxoft.NDS2.Tests.Others.TestDataGenerator;
using NUnit.Framework;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Tests.Others.LoadTestGenereator
{
    [TestFixture]
    public class Generator
    {
        //private string _outputFile = @"inspector1.sql";

        private string _ouputDirPath = null;

        private long declCounter = 1;

        private long selectionId = 1;
        private long requestId = 1;

        private long invoiceCounter = 1;
        private long svodZapId = 1;
        private long tpId = 1;
        private long misId = 1;

        private string taxYear = "2015";
        private string taxPeriod = "21";

        Random random = new Random();

        [TestFixtureSetUp]
        private void RampUp()
        {

            _ouputDirPath = @"D:\Temp";
            if (!Directory.Exists(_ouputDirPath))
            {
                Directory.CreateDirectory(_ouputDirPath);
            }

//            if (File.Exists(_outputFile))
//            {
//                File.Delete(_outputFile);
//            }
            //команды на очистку таблиц к которым будут сгенерированы данные

            WriteToOutput("default", "truncate table NDS2_MRR_USER.selection;");
            WriteToOutput("default", "truncate table NDS2_MRR_USER.sov_selection_request;");
            WriteToOutput("default", "truncate table NDS2_MRR_USER.selection_declaration;");
            WriteToOutput("default", "truncate table NDS2_MRR_USER.selection_discrepancy;");
            WriteToOutput("default", "truncate table NDS2_MRR_USER.book_data_request;");
            WriteToOutput("default", "truncate table NDS2_MRR_USER.sov_invoice;");
            WriteToOutput("default", "truncate table NDS2_MRR_USER.DECLARATION_OWNER;");
            WriteToOutput("default", "truncate table NDS2_MRR_USER.V_EGRN_UL;");
            WriteToOutput("default", "truncate table NDS2_MRR_USER.V_EGRN_IP;");
            WriteToOutput("default", "truncate table FIR.INVOICE_RAW;");
            WriteToOutput("default", "truncate table FIR.DISCREPANCY_RAW;");
            WriteToOutput("default", "truncate table NDS2_MRR_USER.SOV_DECLARATION_INFO;");
            WriteToOutput("default", "truncate table NDS2_MRR_USER.SEOD_DECLARATION;");
            WriteToOutput("default", "truncate table NDS2_MRR_USER.V_EGRN_UL;");
            WriteToOutput("default", "truncate table NDS2_MRR_USER.V_EGRN_IP;");
            WriteToOutput("default", "truncate table FIR.INVOICE_RAW;");
            WriteToOutput("default", "truncate table FIR.DISCREPANCY_RAW;");
            WriteToOutput("default", "truncate table NDS2_MRR_USER.SOV_DECLARATION_INFO;");
            WriteToOutput("default", "truncate table NDS2_MRR_USER.SEOD_DECLARATION;");

            WriteToOutput("default", "delete from NDS2_MC.\"ASKСумОпер7\" cascade;");
            WriteToOutput("default", "delete from NDS2_MC.\"ASKСумПер\" cascade;");
            WriteToOutput("default", "delete from NDS2_MC.\"ASKСумУплНА\" cascade;");
            WriteToOutput("default", "delete from NDS2_MC.\"ASKФайлОпис\" cascade;");
            WriteToOutput("default", "delete from NDS2_MC.\"ASKZIPФайл\" cascade;");
            WriteToOutput("default", "delete from NDS2_MC.\"ASKZIPФайл_Вр\" cascade;");
            WriteToOutput("default", "delete from NDS2_MC.\"ASKДекл\" cascade;");
            WriteToOutput("default", "delete from NDS2_MC.\"ASKЖурнал\" cascade;");
            WriteToOutput("default", "delete from NDS2_MC.\"ASKЖурналУч\" cascade;");
            WriteToOutput("default", "delete from NDS2_MC.\"ASKКонтрСоотн\" cascade;");
            WriteToOutput("default", "delete from NDS2_MC.\"ASKОперация\" cascade;");
            WriteToOutput("default", "delete from NDS2_MC.\"ASKПояснение\" cascade;");
            WriteToOutput("default", "delete from NDS2_MC.\"ASKСведНалГод\" cascade;");
            WriteToOutput("default", "delete from NDS2_MC.\"ASKСведНалГодИ\" cascade;");
            WriteToOutput("default", "delete from NDS2_MC.\"ASKСводЗап\" cascade;");
            WriteToOutput("default", "delete from NDS2_MC.\"ASKСводЗап_Вр\" cascade;");
            WriteToOutput("default", "delete from NDS2_MC.\"ASKСумВосУпл\" cascade;");
            WriteToOutput("default", "delete from NDS2_MC.\"ASKСумОпер4\" cascade;");
            WriteToOutput("default", "delete from NDS2_MC.\"ASKСумОпер5\" cascade;");
            WriteToOutput("default", "delete from NDS2_MC.\"ASKСумОпер6\" cascade;");
            WriteToOutput("default", "delete from NDS2_MC.\"ASKGROUP\" cascade;");
            WriteToOutput("default", "commit;");
        }

        ConcurrentQueue<Soun> queue = new ConcurrentQueue<Soun>();
            
            
       [Test]
        public void InspectorTestData()
        {

           List<Task> tasks = new List<Task>();
            
            foreach (var soun in GetSouns())
            {
                queue.Enqueue(soun);
                //WriteToOutput(soun.Code, "exit;");
            }


            for (int tid = 0; tid <= 2; tid++ )
            {
                Task t = Task.Factory.StartNew(()=>
                    {
                        while(queue.Count > 0)
                        {
                            Soun s;
                            if (queue.TryDequeue(out s))
                            {
                                ProcessRawData(s);
                                ProcessPrepairedSelectionData(s, 1);
                                ProcessPrepairedSelectionData(s, 2);
                                ProcessPrepairedSelectionData(s, 3);
                                ProcessPrepairedSelectionData(s, 6);
                                Console.WriteLine("-------------------------");
                            }                            
                        }
                });

                tasks.Add(t);
            }

           Task.WaitAll(tasks.ToArray());
        }

        private void ProcessPrepairedSelectionData(Soun soun,  int status)
        {
            var fOut = soun.Code;
            int numOfSelections = 5;
            int numberOfDeclarations = 3;
            int numberOfCounterDeclaration = 2;
            int numberOfInvoicesPerRelation = 2;
            int declVersionsCount = 2;
            int mismatchesPerInvoice = 2;

            Console.WriteLine("Processed {0}", soun.Code);
            while(numOfSelections-- >= 0)
            {
                var selecton = TestEntityFactory.CreateSelection(selectionId++, soun);
                selecton.Status = (SelectionStatus)status;
                WriteToOutput(fOut,selecton.ToSql());

                for (int declIdx = 0; declIdx <= numberOfDeclarations; declIdx++)
                {
                    //Создадим сторону обработки 1
                    TPInfo tp1 = TestEntityFactory.CreateTaxPayer();
                    WriteToOutput(fOut, tp1.ToSql(tpId++));

                    Declaration d1 = TestEntityFactory.CreateDeclaration(++declCounter, taxYear, taxPeriod, tp1, soun, random, 0);
                    WriteToOutput(fOut, d1.ToSql(random, ref svodZapId));
                    WriteToOutput(fOut, selecton.SqlAttachDeclaration(d1));

                    //Создадим контрагентов
                    List<Declaration> dCounter = new List<Declaration>();
                    for (int counterDeclIdx = 0; counterDeclIdx <= numberOfCounterDeclaration; counterDeclIdx++)
                    {
                        TPInfo tpCounter = TestEntityFactory.CreateTaxPayer();
                        WriteToOutput(fOut, tp1.ToSql(tpId++));

                        Declaration counterDecl = TestEntityFactory.CreateDeclaration(++declCounter, taxYear, taxPeriod,
                                                                                      tpCounter, soun, random, 0);
                        WriteToOutput(fOut, counterDecl.ToSql(random, ref svodZapId));
                        dCounter.Add(counterDecl);
                        WriteToOutput(fOut, selecton.SqlAttachDeclaration(counterDecl));
                    }

                    //для каждой версии декларации стороны 1 создадим между налогоплательщиками счета фактуры
                    for (int ver = 0; ver < declVersionsCount; ver++)
                    {
                        if (ver > 0) //первичку скинули в output выше
                        {
                            d1.ASK_DECL_ID = ++declCounter + 100000;
                            d1.DECLARATION_VERSION_ID = d1.ASK_DECL_ID + 100000;
                            d1.CORRECTION_NUMBER = ver.ToString();
                            WriteToOutput(fOut, d1.ToSql(random, ref svodZapId));
                        }

                        foreach (Declaration declaration in dCounter)
                        {
                            for (int invIdx = 0; invIdx <= numberOfInvoicesPerRelation; invIdx++)
                            {
                                var dealNumber = DataGenerator.GetRandomInvoiceNumber();
                                var dealDate = DateTime.Now;
                                var invoice = TestEntityFactory.CreateInvoice(++invoiceCounter, dealNumber, 8, dealDate, d1,
                                                                              declaration, random);
                                var counterInvoice = TestEntityFactory.CreateInvoice(++invoiceCounter, dealNumber, 9, dealDate,
                                                                                     declaration, d1, random);

                                var invoice2 = TestEntityFactory.CreateInvoice(++invoiceCounter, dealNumber, 8, dealDate,
                                                                               declaration, d1, random);
                                var counterInvoice2 = TestEntityFactory.CreateInvoice(++invoiceCounter, dealNumber, 9, dealDate,
                                                                                      d1, declaration, random);

                                WriteToOutput(fOut, invoice.ToSql());
                                WriteToOutput(fOut, counterInvoice.ToSql());
                                WriteToOutput(fOut, invoice2.ToSql());
                                WriteToOutput(fOut, counterInvoice2.ToSql());

                                for (int mIdx = 0; mIdx <= mismatchesPerInvoice; mIdx++)
                                {
                                    Mismatch m = TestEntityFactory.CreateMismatch(++misId, random, d1, invoice, declaration,
                                                                                  counterInvoice);
                                    WriteToOutput(fOut, m.ToSql());
                                    WriteToOutput(fOut, m.ToMrrSql());
                                    WriteToOutput(fOut, selecton.SqlAttachDiscrepancy(m));

                                    m = TestEntityFactory.CreateMismatch(++misId, random, declaration, counterInvoice, d1,
                                                                         invoice);
                                    WriteToOutput(fOut, m.ToSql());
                                    WriteToOutput(fOut, m.ToMrrSql());
                                    WriteToOutput(fOut, selecton.SqlAttachDiscrepancy(m));

                                    m = TestEntityFactory.CreateMismatch(++misId, random, declaration, counterInvoice2, d1,
                                                                         invoice2);
                                    WriteToOutput(fOut, m.ToSql());
                                    WriteToOutput(fOut, m.ToMrrSql());
                                    WriteToOutput(fOut, selecton.SqlAttachDiscrepancy(m));

                                    m = TestEntityFactory.CreateMismatch(++misId, random, d1, invoice2, declaration,
                                                                         counterInvoice2);
                                    WriteToOutput(fOut, m.ToSql());
                                    WriteToOutput(fOut, m.ToMrrSql());
                                    WriteToOutput(fOut, selecton.SqlAttachDiscrepancy(m));
                                }
                            }
                        }
                    }

                    //WriteToOutput(fOut, "commit;");
                }                
            }
        }

        private void ProcessRawData(Soun soun)
        {
            var fOut = soun.Code;
            int numberOfDeclarations = 2;
            int numberOfCounterDeclaration = 2;
            int numberOfInvoicesPerRelation = 2;
            int declVersionsCount = 2;
            int mismatchesPerInvoice = 2;

            Console.WriteLine("Processed {0}", soun.Code);

            for (int declIdx = 0; declIdx <= numberOfDeclarations; declIdx++)
            {
                //Создадим сторону обработки 1
                TPInfo tp1 = TestEntityFactory.CreateTaxPayer();
                WriteToOutput(fOut, tp1.ToSql(tpId++));

                Declaration d1 = TestEntityFactory.CreateDeclaration(++declCounter, taxYear, taxPeriod, tp1, soun, random, 0);
                WriteToOutput(fOut, d1.ToSql(random, ref svodZapId));


                //Создадим контрагентов
                List<Declaration> dCounter = new List<Declaration>();
                for (int counterDeclIdx = 0; counterDeclIdx <= numberOfCounterDeclaration; counterDeclIdx++)
                {
                    TPInfo tpCounter = TestEntityFactory.CreateTaxPayer();
                    WriteToOutput(fOut, tp1.ToSql(tpId++));

                    Declaration counterDecl = TestEntityFactory.CreateDeclaration(++declCounter, taxYear, taxPeriod,
                                                                                  tpCounter, soun, random, 0);
                    WriteToOutput(fOut, counterDecl.ToSql(random, ref svodZapId));
                    dCounter.Add(counterDecl);
                }

                //для каждой версии декларации стороны 1 создадим между налогоплательщиками счета фактуры
                for (int ver = 0; ver < declVersionsCount; ver++)
                {
                    if (ver > 0) //первичку скинули в output выше
                    {
                        d1.ASK_DECL_ID = ++declCounter + 100000;
                        d1.DECLARATION_VERSION_ID = d1.ASK_DECL_ID + 100000;
                        d1.CORRECTION_NUMBER = ver.ToString();
                        WriteToOutput(fOut, d1.ToSql(random, ref svodZapId));
                    }

                    foreach (Declaration declaration in dCounter)
                    {
                        for (int invIdx = 0; invIdx <= numberOfInvoicesPerRelation; invIdx++)
                        {
                            var dealNumber = DataGenerator.GetRandomInvoiceNumber();
                            var dealDate = DateTime.Now;
                            var invoice = TestEntityFactory.CreateInvoice(++invoiceCounter, dealNumber, 8, dealDate, d1,
                                                                          declaration, random);
                            var counterInvoice = TestEntityFactory.CreateInvoice(++invoiceCounter, dealNumber, 9, dealDate,
                                                                                 declaration, d1, random);

                            var invoice2 = TestEntityFactory.CreateInvoice(++invoiceCounter, dealNumber, 8, dealDate,
                                                                           declaration, d1, random);
                            var counterInvoice2 = TestEntityFactory.CreateInvoice(++invoiceCounter, dealNumber, 9, dealDate,
                                                                                  d1, declaration, random);

                            WriteToOutput(fOut, invoice.ToSql());
                            WriteToOutput(fOut, counterInvoice.ToSql());
                            WriteToOutput(fOut, invoice2.ToSql());
                            WriteToOutput(fOut, counterInvoice2.ToSql());

                            for (int mIdx = 0; mIdx <= mismatchesPerInvoice; mIdx++)
                            {
                                Mismatch m = TestEntityFactory.CreateMismatch(++misId, random, d1, invoice, declaration,
                                                                              counterInvoice);
                                WriteToOutput(fOut, m.ToSql());

                                m = TestEntityFactory.CreateMismatch(++misId, random, declaration, counterInvoice, d1,
                                                                     invoice);
                                WriteToOutput(fOut, m.ToSql());

                                m = TestEntityFactory.CreateMismatch(++misId, random, declaration, counterInvoice2, d1,
                                                                     invoice2);
                                WriteToOutput(fOut, m.ToSql());

                                m = TestEntityFactory.CreateMismatch(++misId, random, d1, invoice2, declaration,
                                                                     counterInvoice2);
                                WriteToOutput(fOut, m.ToSql());
                            }
                        }
                    }
                }

                WriteToOutput(fOut, "commit;");
            }
        }

        public List<Soun> GetSouns()
        {
            var result = new List<Soun>();
            using (var conn = new OracleConnection(ConfigurationManager.AppSettings["NDS2_DB"]))
            {
                conn.Open();
                using (OracleCommand cmd = new OracleCommand("select s_code from v$sono", conn))
                {
                    using (OracleDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var sCode = reader[0].ToString();
                            result.Add(new Soun() { Code = sCode, Region = sCode.Substring(1, 2) });
                        }
                    }
                }
            }

            return result;
        }

        public void WriteToOutput(string fName, string data)
        {
            //Console.WriteLine(data);

            File.AppendAllLines(Path.Combine(_ouputDirPath, fName + ".sql"), new string[] { data });
        }
    }
}
