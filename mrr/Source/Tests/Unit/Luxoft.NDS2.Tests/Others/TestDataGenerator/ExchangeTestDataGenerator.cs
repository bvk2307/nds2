﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.ControlRatio;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Tests.Others.Common;
using Luxoft.NDS2.Tests.Others.Entity;
using NUnit.Framework;

namespace Luxoft.NDS2.Tests.Others.TestDataGenerator
{
    [TestFixture]
    public class ExchangeTestDataGenerator
    {
        private const string FILE_NAME = "exchange_data.sql";

        [Test]
        public void GenerateDeclarationsAndMissmatches()
        {
            long idDecl = 1000000;      //стартовый идентификатор для деклараций
            long idInvoice = 1000000;   //стартовый идентификатор для СФ
            long idDiscr = 10000;       //стартовый идентификатор для расхождений
            //long idCr = 1;              //стартовый идентификатор для контрольных соотношений
            int objectsToGenerate = 3;  //кол-во деклараций генерируемых на один НО
            int invoicesToGenerate = 10; //кол-во счетов фактур на раздел декларации
            int subojbecstToGeneratePerInvoice = 0; //кол-во историчных записей СФ
            int incr = 0;
            long svodZapId = 0;
            string fullPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, FILE_NAME);

            if (File.Exists(fullPath))
            {
                File.Delete(fullPath);
            }

            Action<string> writeToFile = s =>
                {
                    if (string.IsNullOrEmpty(s)) return;

                    File.AppendAllText(fullPath, s + '\n');
                };

            //команды на очистку таблиц к которым будут сгенерированы данные
            writeToFile("truncate table FIR.INVOICE_RAW;");
            writeToFile("truncate table FIR.DISCREPANCY_RAW;");
            writeToFile("truncate table NDS2_MRR_USER.SOV_DECLARATION_INFO;");
            writeToFile("truncate table \"NDS2_MC\".\"ASKДекл\";");
            writeToFile("truncate table \"NDS2_MC\".\"ASKZIPФайл\";");
            writeToFile("truncate table \"NDS2_MC\".\"ASKСводЗап\";");

            var souns = GetSouns();//загрузим список НО

            //коллекция отчетных периодов
            List<string> periods = new List<string>()
            {
                "21",
                "22",
                "23",
                "24"
            };

            //для некоторых данных будем использовать рандомные числа
            Random r = new Random(100);
            //контейнер для деклараций
            //List<Declaration> decls = new List<Declaration>();

            var taxpayerInfo = GetTaxPayerTestData();//загрузим стэк налогоплательщиков
            //List<TPInfo> taxPayers = new List<TPInfo>();
            while (souns.Any())// понеслась
            {
                var soun = souns.Pop();//достаем НО для которого сгенерируем декларации
                Console.WriteLine("soun");
                //for (int i = 0; i++ < objectsToGenerate; )//цикл по декларациям стороны 1(к которому прилипнет расхождение)
                foreach (var declRegNum in GetDecls())
                {
                    //сгенерим отчетный период
                    var period1 = periods[r.Next(0, 3)];
                    //достанем для декларации налогоплательщика
                    var tp = taxpayerInfo.Pop();
                    //taxPayers.Add(tp);
                    //writeToFile(tp.ToSql());

                    var versionsCount = 0;// r.Next(1, 3);//сгенерим кол-во вресий декларации

                    for (int declVer = 0; declVer <= versionsCount; declVer++)//сгенерируем версии(0-самая первая)
                    {
                        var decl = GetDeclarationSummary(idDecl++, r, period1, tp, soun, declVer);//сгенерируем сам объект декларации

                        decl.DECLARATION_VERSION_ID = declRegNum;
                        decl.IS_ACTUAL = declVer == versionsCount;//актальная та, что с высшим номером коррекции
                        //decls.Add(decl);
                        writeToFile(decl.ToSql(r, ref svodZapId, decl.ID%2 != 0));
                        Console.WriteLine("  declaration");
                        //idCr = GetControlRatio(idCr, r, decl);
                        incr += 1;
                        for (int k = 0; k++ <= invoicesToGenerate + incr; )//сгенерируем счета фактуры для декларации
                        {
                            //по каждому разделу(8-12, 12-й будет в 4-м релизе)
                            for (int chIdx = 8; chIdx <= 13; chIdx++)
                            {
                                var inv = GetInvoiceLine(++idDiscr, r, chIdx);//сгенерируем саму запись о СФ
                                inv.DeclarationId = decl.DECLARATION_VERSION_ID;//замапим ее на версию декларации
                                writeToFile(inv.ToSql());//запишем инсерт команду на выход
                                //Console.WriteLine("    invoice");
                                var tpCounter = new TPInfo()
                                {
                                    Inn = DataGenerator.GetRandomINN(),
                                    Kpp = DataGenerator.GetRandomKPP(),
                                    Name = Guid.NewGuid().ToString()
                                };
                                //taxPayers.Add(tpCounter);
                                //writeToFile(tpCounter.ToSql());
                                var declContr = GetDeclarationSummary(idDecl++, r, period1, tpCounter, soun, 0); //создадим декларацию контрагента с фейковым налогоплательщиком
                                //writeToFile(declContr.ToSql());//добавили его в контейнер так же
                                var invContr = GetInvoiceLine(++idDiscr, r, 9);//создаем сопоставленную СФ 
                                invContr.DeclarationId = declContr.DECLARATION_VERSION_ID;//привязываем ее к декларации контрагента
                                //writeToFile(invContr.ToSql());//пишем интерт по СФ в аутпут


                                if (decl.IS_ACTUAL)
                                {
                                    for (int j = 0; j++ < subojbecstToGeneratePerInvoice; )
                                    //сгенерим расхождения по данной СФ
                                    {
                                        var mismatchType = r.Next(1, 4);
                                        Mismatch m = new Mismatch() //само расхождение
                                            {
                                                Id = idDiscr,
                                                Type = mismatchType,
                                                CompareKind = 1,
                                                RuleGroup = 1,
                                                DealAmount = DataGenerator.GetRandomDecimal(),
                                                MismatchesAmount = DataGenerator.GetRandomDecimal(),
                                                PVPAmount = DataGenerator.GetRandomDecimal(),
                                                Course = DataGenerator.GetRandomDecimal(),
                                                CourseCost = DataGenerator.GetRandomDecimal(),
                                                SurCode = 2,
                                                InvoiceId = inv.ROW_KEY, //ссылка на СФ стороны 1
                                                InvoiceChapter = inv.CHAPTER,
                                                DeclarationId = decl.ID,
                                                InvoiceContractorId = invContr.ROW_KEY,
                                                //ссылка на СФ стороны 2(контрагент)
                                                InvoiceContractorChapter = invContr.CHAPTER,
                                                DeclarationContractorId = declContr.ID,
                                                Status = 2
                                            };

                                        if (mismatchType != 1)
                                        {
                                            m.InvoiceContractorId = invContr.ROW_KEY;
                                            //ссылка на СФ стороны 2(контрагент)
                                            m.InvoiceContractorChapter = invContr.CHAPTER;
                                            m.DeclarationContractorId = declContr.ID;
                                        }
                                        //writeToFile(m.ToSql());//добавили расхождение в декларацию
                                        //decl.Mismatches.Add(m); 
                                        idInvoice++;
                                    }
                                }
                                //                                for (int k1 = 0; k1 < r.Next(1, 7); k1++)//добавим версии для агрегированной СФ
                                //                                {
                                //                                    var invInner = GetInvoiceLine(++idDiscr, r, chIdx);
                                //                                    invInner.DeclarationId = decl.DECLARATION_VERSION_ID;
                                //                                    invInner.ACTUAL_ROW_KEY = inv.ROW_KEY;
                                //                                    invInner.CHAPTER = inv.CHAPTER;
                                //                                    writeToFile(invInner.ToSql());
                                //                                }
                            }
                        }
                    }
                }
            }

            //   Console.WriteLine("truncate table FIR.DISCREPANCY_RAW;");

            //            foreach (var declaration in decls)//пишем SQL комманды в аутпут
            //            {
            //                //Console.WriteLine(declaration.ToSql());
            //                foreach (var mismatch in declaration.Mismatches)
            //                {
            //                    Console.WriteLine(mismatch.ToSql());
            //                }
            //            }

            //            foreach (var declarationSummary in decls)
            //            {
            //                Console.WriteLine(declarationSummary.ToSql());
            //            }
            //
            //            foreach (var taxPayer in taxPayers)
            //            {
            //                Console.WriteLine(taxPayer.ToSql());
            //            }

            Console.WriteLine("commit;");
        }

        private void WriteToFile(string command)
        {

        }

        private void MapInvoice(Declaration side1, Declaration side2)
        {

        }

        private int GetLKError(int chapter, Random r)
        {
            switch (chapter)
            {
                case 8:
                    return r.Next(1, 3);
                case 9:
                    return r.Next(4, 6);
                case 10:
                    return r.Next(7, 10);
                case 11:
                    return r.Next(11, 15);
                default:
                    return 0;
            }
        }

        [Test]
        public void GenerateInvoices()
        {
            Random r = new Random();
            Console.WriteLine("truncate table FIR.INVOICE_RAW;");
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(GetInvoiceLine(i, r, 8).ToSql());
            }
        }

        private Invoice GetInvoiceLine(long id, Random r, int chapter)
        {
            return new Invoice()
            {
                CHAPTER = chapter,
                ORDINAL_NUMBER = id + 1,
                OKV_CODE = "810",
                CREATE_DATE = DataGenerator.GetRandomDateString(),
                RECEIVE_DATE = DataGenerator.GetRandomDateString(),
                OPERATION_CODE = DataGenerator.GetRandomOperationCode(),
                INVOICE_NUM = DataGenerator.GetRandomInvoiceNumber(),
                INVOICE_DATE = DataGenerator.GetRandomDateString(),
                CHANGE_NUM = DataGenerator.GetRandomInvoiceNumber(),
                CHANGE_DATE = DataGenerator.GetRandomDateString(),
                CORRECTION_NUM = DataGenerator.GetRandomInvoiceNumber(),
                CORRECTION_DATE = DataGenerator.GetRandomDateString(),
                CHANGE_CORRECTION_NUM = DataGenerator.GetRandomInvoiceNumber(),
                CHANGE_CORRECTION_DATE = DataGenerator.GetRandomDateString(),
                RECEIPT_DOC_NUM = DataGenerator.GetRandomInvoiceNumber(),
                RECEIPT_DOC_DATE = DataGenerator.GetRandomDateString(),
                BUY_ACCEPT_DATE = new NDS2.Common.Contracts.DTO.Business.Book.DocDates(DataGenerator.GetRandomDateString()),
                BUYER_INN = DataGenerator.GetRandomINN(),
                BUYER_KPP = DataGenerator.GetRandomKPP(),
                BUYER_NAME = DataGenerator.GetRandomTaxPayerName(),
                SELLER_INN = DataGenerator.GetRandomINN(),
                SELLER_KPP = DataGenerator.GetRandomKPP(),
                SELLER_NAME = DataGenerator.GetRandomTaxPayerName(),
                SELLER_INVOICE_NUM = DataGenerator.GetRandomInvoiceNumber(),
                SELLER_INVOICE_DATE = DataGenerator.GetRandomDateString(),
                BROKER_INN = DataGenerator.GetRandomINN(),
                BROKER_KPP = DataGenerator.GetRandomKPP(),
                BROKER_NAME = DataGenerator.GetRandomTaxPayerName(),
                DEAL_KIND_CODE = r.Next(1, 4),
                CUSTOMS_DECLARATION_NUM = DataGenerator.GetRandomTaxPayerName(),
                PRICE_BUY_AMOUNT = DataGenerator.GetRandomNullableDecimal(),
                PRICE_BUY_NDS_AMOUNT = DataGenerator.GetRandomNullableDecimal(),
                PRICE_SELL = DataGenerator.GetRandomNullableDecimal(),
                PRICE_SELL_IN_CURR = DataGenerator.GetRandomNullableDecimal(),
                PRICE_SELL_18 = DataGenerator.GetRandomNullableDecimal(),
                PRICE_SELL_10 = DataGenerator.GetRandomNullableDecimal(),
                PRICE_SELL_0 = DataGenerator.GetRandomNullableDecimal(),
                PRICE_NDS_18 = DataGenerator.GetRandomNullableDecimal(),
                PRICE_NDS_10 = DataGenerator.GetRandomNullableDecimal(),
                PRICE_TAX_FREE = DataGenerator.GetRandomNullableDecimal(),
                PRICE_TOTAL = DataGenerator.GetRandomNullableDecimal(),
                PRICE_NDS_TOTAL = DataGenerator.GetRandomNullableDecimal(),
                DIFF_CORRECT_DECREASE = DataGenerator.GetRandomNullableDecimal(),
                DIFF_CORRECT_INCREASE = DataGenerator.GetRandomNullableDecimal(),
                DIFF_CORRECT_NDS_DECREASE = DataGenerator.GetRandomNullableDecimal(),
                DIFF_CORRECT_NDS_INCREASE = DataGenerator.GetRandomNullableDecimal(),
                PRICE_NDS_BUYER = DataGenerator.GetRandomNullableDecimal(),
                LOGICAL_ERRORS = GetLKError(chapter, r).ToString(),
                ROW_KEY = DataGenerator.GetRandomKPP(),
                ACTUAL_ROW_KEY = null
            };
        }


        public Declaration GetDeclarationSummary(long id, Random r, string period, TPInfo ti, Soun s, int version)
        {
            return new Declaration()
            {
                ID = long.Parse("2014" + period + ti.Inn),
                LK_ERRORS_COUNT = r.Next(1, 33),
                CH8_DEALS_AMNT_TOTAL = 0,
                CH9_DEALS_AMNT_TOTAL = 0,
                CH8_NDS = 0,
                CH9_NDS = 0,
                DISCREP_CURRENCY_AMNT = 0,
                DISCREP_CURRENCY_COUNT = 0,
                GAP_DISCREP_COUNT = r.Next(1, 1000),
                GAP_DISCREP_AMNT = DataGenerator.GetRandomDecimal(),
                NDS_INCREASE_DISCREP_COUNT = r.Next(1, 1000),
                NDS_INCREASE_DISCREP_AMNT = DataGenerator.GetRandomDecimal(),
                GAP_MULTIPLE_DISCREP_COUNT = r.Next(1, 1000),
                GAP_MULTIPLE_DISCREP_AMNT = DataGenerator.GetRandomDecimal(),
                CONTROL_RATIO_COUNT = r.Next(1, 1000),
                DISCREP_BUY_BOOK_AMNT = DataGenerator.GetRandomDecimal(),
                DISCREP_SELL_BOOK_AMNT = DataGenerator.GetRandomDecimal(),
                DISCREP_AVG_AMNT = r.Next(1, 1000),
                DISCREP_MAX_AMNT = r.Next(1, 1000),
                DISCREP_MIN_AMNT = r.Next(1, 1000),
                DISCREP_TOTAL_AMNT = r.Next(1, 1000),
                PVP_TOTAL_AMNT = r.Next(1, 1000),
                PVP_DISCREP_MIN_AMNT = r.Next(1, 1000),
                PVP_DISCREP_MAX_AMNT = r.Next(1, 1000),
                PVP_DISCREP_AVG_AMNT = r.Next(1, 1000),
                PVP_BUY_BOOK_AMNT = r.Next(1, 1000),
                PVP_SELL_BOOK_AMNT = r.Next(1, 1000),
                PVP_RECIEVE_JOURNAL_AMNT = r.Next(1, 1000),
                PVP_SENT_JOURNAL_AMNT = r.Next(1, 1000),
                SUR_CODE = r.Next(1, 4),
                CONTROL_RATIO_DATE = DateTime.Now,
                CONTROL_RATIO_SEND_TO_SEOD = r.Next(99) < 50,
                UPDATE_DATE = DataGenerator.GetRandomDateString(),
                /*ASK Decl*/
                ASK_DECL_ID = id + 100000,
                DECLARATION_VERSION_ID = id + 200000,
                DECL_DATE = DateTime.Now,
                TAX_PERIOD = period,
                FISCAL_YEAR = "2014",
                SOUN_ENTRY = new CodeValueDictionaryEntry(s.Code, ""),
                SOUN_NAME = s.Code + "-",
                CORRECTION_NUMBER = version.ToString(),// == 0 ? string.Empty : version.ToString(),
                OKVED_CODE = "12",
                NAME = ti.Name,
                INN = ti.Inn,
                KPP = ti.Kpp,

            };
        }

        [Test]
        public void ToSqlGeneration()
        {

            //Console.WriteLine(DateTime.Now.ToString("dd.MM.yyyy"));
            //return;
            Type t = typeof(DeclarationSummary);

            foreach (var propertyInfo in t.GetProperties())
            {
                //Console.WriteLine("sb.AppendFormat(\"{1}, \", this.{0});", GetPropertyRepresentation(propertyInfo) ,GetPropertyFormat(propertyInfo));
                //Console.WriteLine("," + propertyInfo.Name);
                Console.WriteLine("obj." + propertyInfo.Name + " = reader.ReadString(\"" + propertyInfo.Name + "\");");
            }

        }

        public List<long> GetDecls()
        {
            List<long> declList = new List<long>();

            declList.Add(19703803);
            declList.Add(19703804);
            declList.Add(19703808);
            declList.Add(19703814);
            declList.Add(19703816);
            declList.Add(19703817);
            declList.Add(19703818);
            declList.Add(19703819);
            declList.Add(19703820);
            declList.Add(19703821);
            declList.Add(19703822);
            declList.Add(19703823);
            declList.Add(19703824);
            declList.Add(19703825);
            declList.Add(19703829);
            declList.Add(19703831);
            declList.Add(19703833);
            declList.Add(19703837);
            declList.Add(19703841);
            declList.Add(19703843);
            declList.Add(19703844);
            declList.Add(19703845);
            declList.Add(19703847);
            declList.Add(19703848);
            declList.Add(19703849);
            declList.Add(19703850);
            declList.Add(19703875);
            declList.Add(19703877);
            declList.Add(19703878);
            declList.Add(19703879);
            declList.Add(19703880);
            declList.Add(19703881);
            declList.Add(19703882);
            declList.Add(19703883);

            return declList;
        }

        public Stack<Soun> GetSouns()
        {
            Stack<Soun> stack = new Stack<Soun>();
            //            stack.Push(new Soun() { Code = "", Region = "" });
            //            stack.Push(new Soun() { Code = "0513", Region = "05" });
            //            stack.Push(new Soun() { Code = "1432", Region = "14" });
            //            stack.Push(new Soun() { Code = "1434", Region = "14" });
            //            stack.Push(new Soun() { Code = "1435", Region = "14" });
            //            stack.Push(new Soun() { Code = "1646", Region = "16" });
            //            stack.Push(new Soun() { Code = "1648", Region = "16" });
            //stack.Push(new Soun() { Code = "2028", Region = "20" });
            //stack.Push(new Soun() { Code = "2455", Region = "24" });
            //stack.Push(new Soun() { Code = "2622", Region = "26" });
            //stack.Push(new Soun() { Code = "2826", Region = "28" });
            //stack.Push(new Soun() { Code = "3423", Region = "34" });
            //stack.Push(new Soun() { Code = "3901", Region = "39" });
            //stack.Push(new Soun() { Code = "4013", Region = "40" });
            stack.Push(new Soun() { Code = "5252", Region = "52" });
            //stack.Push(new Soun() { Code = "5441", Region = "54" });
            //stack.Push(new Soun() { Code = "5521", Region = "55" });
            //stack.Push(new Soun() { Code = "5609", Region = "56" });
            //stack.Push(new Soun() { Code = "5942", Region = "59" });
            //stack.Push(new Soun() { Code = "6438", Region = "64" });
            //stack.Push(new Soun() { Code = "7715", Region = "77" });
            return stack;
        }

        public Stack<TPInfo> GetTaxPayerTestData()
        {
            Stack<TPInfo> stack = new Stack<TPInfo>();

            stack.Push(new TPInfo() { Inn = "5101600300", Kpp = "511801001", Name = "ОАО \"КГИЛЦ\"" });
            stack.Push(new TPInfo() { Inn = "4407009340", Kpp = "440701001", Name = "ООО \"М-про\"" });
            stack.Push(new TPInfo() { Inn = "5821400720", Kpp = "582101001", Name = "ООО \"Лунинское АТП\"" });
            stack.Push(new TPInfo() { Inn = "5245020719", Kpp = "524501001", Name = "ООО \"Контур\"" });
            stack.Push(new TPInfo() { Inn = "5821402333", Kpp = "582101001", Name = "МБУ \"МЦ Лунинского района\"" });
            stack.Push(new TPInfo() { Inn = "5245009105", Kpp = "524501001", Name = "ООО \"ТЭК \"Трансинвест-НН\"" });
            stack.Push(new TPInfo() { Inn = "5208012345", Kpp = "520801001", Name = "ООО \"Стройсфера\"" });
            stack.Push(new TPInfo() { Inn = "5263028774", Kpp = "526301001", Name = "ООО \"Компания \"Рента-НН\"" });
            stack.Push(new TPInfo() { Inn = "5245001234", Kpp = "524501001", Name = "Банк \"Ростовщик\" (ООО)" });
            stack.Push(new TPInfo() { Inn = "3017043047", Kpp = "302301001", Name = "ООО \"Клондайк\"" });
            stack.Push(new TPInfo() { Inn = "5262268974", Kpp = "526201001", Name = "ООО  \"Компания ТАЛИОН-ПРО\"" });
            stack.Push(new TPInfo() { Inn = "5252031373", Kpp = "525201001", Name = "ООО\"Мастер-кузнец Волков\"" });
            stack.Push(new TPInfo() { Inn = "5252029889", Kpp = "525201001", Name = "ООО \"ЛесПромСтрой\"" });
            stack.Push(new TPInfo() { Inn = "5821004050", Kpp = "582101001", Name = "Агентство по развитию предпринимательства" });
            stack.Push(new TPInfo() { Inn = "5252024425", Kpp = "525201001", Name = "Местная религиозная организация Церковь Христиан-Адвентистов Седьмого Дня г. Павлово Нижегородской области" });
            stack.Push(new TPInfo() { Inn = "3127000014", Kpp = "312701001", Name = "ОАО \"Лебединский ГОК\"" });
            stack.Push(new TPInfo() { Inn = "1835087959", Kpp = "184101001", Name = "ООО \"РегионЗемКом\"" });
            stack.Push(new TPInfo() { Inn = "5256085482", Kpp = "525601001", Name = "ООО \"НАШ\"" });
            stack.Push(new TPInfo() { Inn = "5245008630", Kpp = "524601001", Name = "ООО \"ВТГСМ\"" });
            stack.Push(new TPInfo() { Inn = "1660151953", Kpp = "166001001", Name = "ООО УК \"ЖКХ Танкодром\"" });
            stack.Push(new TPInfo() { Inn = "7727004113", Kpp = "770101001", Name = "ЗАО \"ЛАНИТ\"" });
            stack.Push(new TPInfo() { Inn = "6321130852", Kpp = "631801001", Name = "ООО \"РСУ\"" });
            stack.Push(new TPInfo() { Inn = "5252030556", Kpp = "525201001", Name = "ООО \"Метторгснаб НН\"" });
            stack.Push(new TPInfo() { Inn = "7713056834", Kpp = "775001001", Name = "ОАО \"АльфаСтрахование\"" });
            stack.Push(new TPInfo() { Inn = "5252026574", Kpp = "525201001", Name = "ГК \"ВДОАМ - 2\"" });
            stack.Push(new TPInfo() { Inn = "5245995112", Kpp = "524501001", Name = "БФ \"Начало Жизни\"" });
            stack.Push(new TPInfo() { Inn = "7602038533", Kpp = "760301001", Name = "ООО \"Алекс плюс\"" });
            stack.Push(new TPInfo() { Inn = "5027105930", Kpp = "502701001", Name = "ООО \"БЛЮЗ\"" });
            stack.Push(new TPInfo() { Inn = "5245020797", Kpp = "524501001", Name = "ООО \"СГМ\"" });
            stack.Push(new TPInfo() { Inn = "5256106252", Kpp = "526201001", Name = "ООО \"Орион\"" });
            stack.Push(new TPInfo() { Inn = "5208001234", Kpp = "520801001", Name = "ТСЖ \"Чкаловский\"" });
            stack.Push(new TPInfo() { Inn = "5245012370", Kpp = "524501001", Name = "ООО \"Вираж\"" });
            stack.Push(new TPInfo() { Inn = "5836140881", Kpp = "583601001", Name = "Ассоциация \"СмоПо\"" });
            stack.Push(new TPInfo() { Inn = "7706724054", Kpp = "770601001", Name = "ЗАО \"ВК Комфорт\"" });
            stack.Push(new TPInfo() { Inn = "5252024425", Kpp = "525201001", Name = "Поместная Церковь Христиан-Адвентистов Седьмого Дня г. Павлово Нижегородской области" });
            stack.Push(new TPInfo() { Inn = "5260075303", Kpp = "525901001", Name = "ООО Фирма \"НикО\"" });
            stack.Push(new TPInfo() { Inn = "7733584350", Kpp = "773301001", Name = "ООО \"ПРОМНЕТ\"" });
            stack.Push(new TPInfo() { Inn = "2129040677", Kpp = "213001001", Name = "ООО \"Саяны-центр\"" });
            stack.Push(new TPInfo() { Inn = "7203181482", Kpp = "720301001", Name = "ООО \"Аптеки 36,6 \"Тюмень\"" });
            stack.Push(new TPInfo() { Inn = "7017268626", Kpp = "701701001", Name = "ООО \"Тк-Св\"" });
            stack.Push(new TPInfo() { Inn = "5252028606", Kpp = "525201001", Name = "ООО \"САДКО ПЛЮС\"" });
            stack.Push(new TPInfo() { Inn = "7719726789", Kpp = "771901001", Name = "ООО \"Полюс\"" });
            stack.Push(new TPInfo() { Inn = "5252015572", Kpp = "525201001", Name = "ООО \"КОМФОРТ +\"" });
            stack.Push(new TPInfo() { Inn = "5252014152", Kpp = "525201001", Name = "ООО \"Сервисцентр\"" });
            stack.Push(new TPInfo() { Inn = "1627005803", Kpp = "162701001", Name = "ООО  \"НЕПТУН\"" });
            stack.Push(new TPInfo() { Inn = "2309102185", Kpp = "230901001", Name = "ООО \"Кавинформресурс\"" });
            stack.Push(new TPInfo() { Inn = "5821003875", Kpp = "582101001", Name = "АДМИНИСТРАЦИЯ ЛУНИНСКОГО РАЙОНА" });
            stack.Push(new TPInfo() { Inn = "5253003900", Kpp = "526201001", Name = "ООО СК \"Надежда-НН\"" });
            stack.Push(new TPInfo() { Inn = "2330002810", Kpp = "233001001", Name = "Крестьянское хозяйство \"Остров Надежды\"" });
            stack.Push(new TPInfo() { Inn = "5245002345", Kpp = "524501001", Name = "ОАО \"Птицефабрика Симбирская\"" });
            stack.Push(new TPInfo() { Inn = "5252012099", Kpp = "525201001", Name = "ООО\"АгроТрейд\"" });
            stack.Push(new TPInfo() { Inn = "5245016208", Kpp = "524501001", Name = "ООО \"Сварочный Легион\"" });
            stack.Push(new TPInfo() { Inn = "5252023654", Kpp = "525201001", Name = "ООО \"МАИ\"" });
            stack.Push(new TPInfo() { Inn = "1327013641", Kpp = "132701001", Name = "ООО \"Тройка лек\"" });
            stack.Push(new TPInfo() { Inn = "5252022026", Kpp = "525201001", Name = "ООО \"Производственно-строительная компания\"" });
            stack.Push(new TPInfo() { Inn = "1627008723", Kpp = "162701001", Name = "ООО \"ЕРЦ Менделеевск\"" });
            stack.Push(new TPInfo() { Inn = "1646027961", Kpp = "164601001", Name = "УК ООО \"ЖИЛКОМФОРТСЕРВИС\"" });
            stack.Push(new TPInfo() { Inn = "5321119060", Kpp = "532101001", Name = "ООО \"СУ - 3\"" });
            stack.Push(new TPInfo() { Inn = "5236003330", Kpp = "523601001", Name = "СПК (колхоз) \"Прогресс\"" });
            stack.Push(new TPInfo() { Inn = "5252017562", Kpp = "525201001", Name = "ОО \"ФЛГ\"" });
            stack.Push(new TPInfo() { Inn = "1653018661", Kpp = "165801001", Name = "ООО КБЭР \"БАНК КАЗАНИ\"" });
            stack.Push(new TPInfo() { Inn = "7729655706", Kpp = "502701001", Name = "ООО \"Гламберри\"" });
            stack.Push(new TPInfo() { Inn = "7743737189", Kpp = "774301001", Name = "ООО \"Эра Красоты\"" });
            stack.Push(new TPInfo() { Inn = "7439009108", Kpp = "742401001", Name = "МБМУ \"Нижнесанарская УБ\"" });
            stack.Push(new TPInfo() { Inn = "5245012345", Kpp = "524501001", Name = "СНТ \"Радист\"" });
            stack.Push(new TPInfo() { Inn = "2627017494", Kpp = "263001001", Name = "ООО \"СЕРВИС-КМВ\"" });
            stack.Push(new TPInfo() { Inn = "2464104677", Kpp = "246401001", Name = "ООО \"Авер-Дент\"" });
            stack.Push(new TPInfo() { Inn = "6316126498", Kpp = "631601001", Name = "ООО \"АПОГЕЙ-С\"" });
            stack.Push(new TPInfo() { Inn = "6027111728", Kpp = "602701001", Name = "ООО\"МВ ДЛЯ ОФИСА\"" });
            stack.Push(new TPInfo() { Inn = "5260337252", Kpp = "525201001", Name = "ООО \"Капитал плюс\"" });
            stack.Push(new TPInfo() { Inn = "5245012926", Kpp = "524501001", Name = "ООО \"Монолит\"" });
            stack.Push(new TPInfo() { Inn = "5263023456", Kpp = "525701001", Name = "ЗАО \"Оптерон\"" });
            stack.Push(new TPInfo() { Inn = "5609066470", Kpp = "560901001", Name = "ООО \"Оренпромавтоматика\"" });
            stack.Push(new TPInfo() { Inn = "5410037050", Kpp = "541001001", Name = "ООО \"Будь здоров\"" });
            stack.Push(new TPInfo() { Inn = "3664088086", Kpp = "773401001", Name = "ООО \"ПАНОРАМА\"" });
            stack.Push(new TPInfo() { Inn = "8614002935", Kpp = "861401001", Name = "БУ \"ОКТЯБРЬСКАЯ РАЙОННАЯ БОЛЬНИЦА\"" });
            stack.Push(new TPInfo() { Inn = "5245000448", Kpp = "524501001", Name = "ООО предприятие \"АЛИДИ\"" });
            stack.Push(new TPInfo() { Inn = "5252031528", Kpp = "525201001", Name = "ООО \"Триумф\"" });
            stack.Push(new TPInfo() { Inn = "5821400255", Kpp = "582101001", Name = "МУП\"МТС плюс\"" });
            stack.Push(new TPInfo() { Inn = "5252032909", Kpp = "525201001", Name = "ООО \"ФЕМИДА\"" });
            stack.Push(new TPInfo() { Inn = "7722144195", Kpp = "312001001", Name = "ООО\"ПКФ Атлас\"" });
            return stack;
        }

        private string GetPropertyFormat(PropertyInfo pi)
        {
            if (pi.PropertyType == typeof(DateTime) || pi.PropertyType == typeof(DateTime?))
            {
                return "to_date('{0}', 'dd.mm.yyyy')";
            }

            if (pi.PropertyType == typeof(string))
            {
                return "'{0}'";
            }

            return "{0}";
        }

        private string GetPropertyRepresentation(PropertyInfo pi)
        {
            if (pi.PropertyType == typeof(DateTime))
            {
                return string.Format("{0}.ToString(\"dd.MM.yyyy\")", pi.Name);
            }

            if (pi.PropertyType == typeof(decimal) || pi.PropertyType == typeof(decimal?))
            {
                return string.Format("{0}.ToString(ci)", pi.Name);
            }

            return pi.Name;
        }
    }
}
