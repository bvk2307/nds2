﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Tests.Others.Entity;
using NUnit.Framework;

namespace Luxoft.NDS2.Tests.Others.TestDataGenerator
{
    public class Declaration : DeclarationSummary
    {
        public Declaration()
        {
            Mismatches = new List<Mismatch>();
        }

        public List<Mismatch> Mismatches { get; private set; }
    }
}

