﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management.Instrumentation;
using System.Reflection;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Tests.Others.Common;
using Luxoft.NDS2.Tests.Others.Entity;
using NUnit.Framework;
using ControlRatio = Luxoft.NDS2.Tests.Others.Entity.ControlRatio;

namespace Luxoft.NDS2.Tests.Others.TestDataGenerator
{
    [TestFixture]
    public class FirTestDataGenerator
    {
        [Test]
        public void GenerateDeclarationsAndMissmatches()
        {
            long idDecl = 1000000;      //стартовый идентификатор для деклараций
            long idInvoice = 1000000;   //стартовый идентификатор для СФ
            long idDiscr = 10000;       //стартовый идентификатор для расхождений
            long idCr = 0;//1;              //стартовый идентификатор для контрольных соотношений
            int objectsToGenerate = 3;  //кол-во деклараций генерируемых на один НО
            int invoicesToGenerate = 3; //кол-во счетов фактур на раздел декларации
            int subojbecstToGeneratePerInvoice = 1; //кол-во историчных записей СФ

            //команды на очистку таблиц к которым будут сгенерированы данные

            Console.WriteLine("truncate table FIR.V_EGRN_UL;");
            Console.WriteLine("truncate table FIR.V_EGRN_IP;");
            Console.WriteLine("truncate table FIR.INVOICE_RAW;");
            Console.WriteLine("truncate table FIR.DISCREPANCY_RAW;");
            Console.WriteLine("truncate table NDS2_MRR_USER.SOV_DECLARATION_INFO;");
            Console.WriteLine("truncate table NDS2_MRR_USER.SEOD_DECLARATION;");
            Console.WriteLine("delete from NDS2_MC.\"ASKСумОпер7\" cascade;");
            Console.WriteLine("delete from NDS2_MC.\"ASKСумПер\" cascade;");
            Console.WriteLine("delete from NDS2_MC.\"ASKСумУплНА\" cascade;");
            Console.WriteLine("delete from NDS2_MC.\"ASKФайлОпис\" cascade;");
            Console.WriteLine("delete from NDS2_MC.\"ASKZIPФайл\" cascade;");
            Console.WriteLine("delete from NDS2_MC.\"ASKZIPФайл_Вр\" cascade;");
            Console.WriteLine("delete from NDS2_MC.\"ASKДекл\" cascade;");
            Console.WriteLine("delete from NDS2_MC.\"ASKЖурнал\" cascade;");
            Console.WriteLine("delete from NDS2_MC.\"ASKЖурналУч\" cascade;");
            Console.WriteLine("delete from NDS2_MC.\"ASKКонтрСоотн\" cascade;");
            Console.WriteLine("delete from NDS2_MC.\"ASKОперация\" cascade;");
            Console.WriteLine("delete from NDS2_MC.\"ASKПояснение\" cascade;");
            Console.WriteLine("delete from NDS2_MC.\"ASKСведНалГод\" cascade;");
            Console.WriteLine("delete from NDS2_MC.\"ASKСведНалГодИ\" cascade;");
            Console.WriteLine("delete from NDS2_MC.\"ASKСводЗап\" cascade;");
            Console.WriteLine("delete from NDS2_MC.\"ASKСводЗап_Вр\" cascade;");
            Console.WriteLine("delete from NDS2_MC.\"ASKСумВосУпл\" cascade;");
            Console.WriteLine("delete from NDS2_MC.\"ASKСумОпер4\" cascade;");
            Console.WriteLine("delete from NDS2_MC.\"ASKСумОпер5\" cascade;");
            Console.WriteLine("delete from NDS2_MC.\"ASKСумОпер6\" cascade;");
            Console.WriteLine("delete from NDS2_MC.\"ASKGROUP\" cascade;");
            Console.WriteLine("commit;");

            Console.WriteLine("insert into NDS2_MC.\"serialgen\" (\"DummyID\", \"CurValue\", \"TableName\") values(17, 1, 'ASKZipФайл');");
            Console.WriteLine("commit;");

            long svodZapId = 0;
            
            long sc = 0;

            var souns = GetSouns();//загрузим список НО

            //коллекция отчетных периодов
            List<string> periods = new List<string>()
            {
                "21", "22", "23", "24", 
                "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", 
                "51", "54", "55", "56", 
                "71", "72", "73", "74", "75", "76", "77", "78", "79", "80", "81", "82"
            };

            //для некоторых данных будем использовать рандомные числа
            Random r = new Random(100);
            //контейнер для деклараций
            List<Declaration> decls = new List<Declaration>();

            var taxpayerInfo = GetTaxPayerTestData();//загрузим стэк налогоплательщиков
            scPull_INN = 0;
            pull_INN.Clear();
            int countInn = 0;
            foreach (TPInfo itemInfo in taxpayerInfo)
            {
                pull_INN.Add(new IdPair(itemInfo.Inn, itemInfo.Kpp));
                countInn++;
                if (countInn >= 10) { break; }
            }
            var taxPayers = new List<TPInfo>();
            var controlRatios = new List<ControlRatio>();
            while (souns.Any())// понеслась
            {
                var soun = souns.Pop();//достаем НО для которого сгенерируем декларации
                for (int i = 0; i++ < objectsToGenerate; )//цикл по декларациям стороны 1(к которому прилипнет расхождение)
                {
                    //сгенерим отчетный период
                    var period1 = periods[r.Next(0, 30)];
                    //достанем для декларации налогоплательщика
                    var tp = taxpayerInfo.Pop();
                    taxPayers.Add(tp);
                    if (!pull_INN.Any(x => x.INN == tp.Inn && x.KPP == tp.Kpp))
                    {
                        pull_INN.Add(new IdPair(tp.Inn, tp.Kpp));
                    }
                    //--- Для ООО предприятия "АЛИДИ" и для ООО "ФЕМИДА" 
                    if (tp.Inn == "5245000448" || tp.Inn == "5252032909")
                        period1 = "23";

                    CreateHalfEmptyDeclarations(r, ref idDecl, ref svodZapId, soun, period1, ref sc, ref idDiscr);

                    var versionsCount = r.Next(1, 3);//сгенерим кол-во вресий декларации

                    for (int declVer = 0; declVer <= versionsCount; declVer++)//сгенерируем версии(0-самая первая)
                    {
                        var decl = GetDeclarationSummary(idDecl++, r, period1, tp, soun, declVer);//сгенерируем сам объект декларации
                        decl.IS_ACTUAL = declVer == versionsCount;//актальная та, что с высшим номером коррекции
                        decls.Add(decl);

                        controlRatios.AddRange(GetControlRatio(idCr, r, decl));
                        idCr = controlRatios.Count + 1;

                        for (int k = 0; k++ <= invoicesToGenerate; )//сгенерируем счета фактуры для декларации
                        {
                            //по каждому разделу(8-12, 12-й будет в 4-м релизе)
                            for (int chIdx = 8; chIdx <= 12; chIdx++)
                            {
                                var tpCounter = new TPInfo()
                                {
                                    Inn = DataGenerator.GetRandomINN_10digit(),
                                    Kpp = DataGenerator.GetRandomKPP(),
                                    Name = Guid.NewGuid().ToString()
                                };
                                taxPayers.Add(tpCounter);

                                var id = GetIdFromPull();
                                var inns = new InvoiceINN(GetIdFromPull(), GetIdFromPull(), GetIdFromPull());
                                if (chIdx == 8 || chIdx == 11 || chIdx == 13)
                                    inns = new InvoiceINN(decl, tpCounter) { BROKER_INN = id.INN, BROKER_KPP = id.KPP};
                                else if (chIdx == 9 || chIdx == 10 || chIdx == 12 || chIdx == 14)
                                    inns = new InvoiceINN(decl, tpCounter, false) { BROKER_INN = id.INN, BROKER_KPP = id.KPP };

                                var inv = GetInvoiceLine(++idDiscr, r, chIdx, NalogPeriod.GetInnerPeriodDT(period1, decl.FISCAL_YEAR), inns, decl.DECLARATION_VERSION_ID, true);//сгенерируем саму запись о СФ
                                
                                inv.DeclarationId = decl.DECLARATION_VERSION_ID;//замапим ее на версию декларации
                                Console.WriteLine(inv.ToSql());//запишем инсерт команду на выход

                                var declContr = GetDeclarationSummary(idDecl++, r, period1, tpCounter, soun, 0); //создадим декларацию контрагента с фейковым налогоплательщиком
                                decls.Add(declContr);//добавили его в контейнер так же
                                id = GetIdFromPull();
                                var innsContr = new InvoiceINN(decl, declContr) { BROKER_INN = id.INN, BROKER_KPP = id.KPP };
                                var invContr = GetInvoiceLine(++idDiscr, r, 9, NalogPeriod.GetInnerPeriodDT(period1, declContr.FISCAL_YEAR), innsContr, declContr.DECLARATION_VERSION_ID, true);//создаем сопоставленную СФ 
                                invContr.DeclarationId = declContr.DECLARATION_VERSION_ID;//привязываем ее к декларации контрагента
                                Console.WriteLine(invContr.ToSql());//пишем интерт по СФ в аутпут

                                if (decl.IS_ACTUAL)
                                {
                                    for (int j = 0; j++ < subojbecstToGeneratePerInvoice; )
                                    //сгенерим расхождения по данной СФ
                                    {
                                        var mismatchType = r.Next(1, 4);
                                        Mismatch m = new Mismatch(); //само расхождение
                                        m.Id = idDiscr;
                                        m.Type = mismatchType;
                                        m.CompareKind = 1;
                                        m.RuleGroup = r.Next(3, 8);
                                        m.DealAmount = DataGenerator.GetRandomDecimal();
                                        m.MismatchesAmount = DataGenerator.GetRandomDecimalBig();
                                        m.PVPAmount = DataGenerator.GetRandomDecimal();
                                        m.Course = DataGenerator.GetRandomDecimal();
                                        m.CourseCost = DataGenerator.GetRandomDecimal();
                                        m.SurCode = 2;
                                        m.InvoiceId = inv.ROW_KEY; //ссылка на СФ стороны 1
                                        m.InvoiceChapter = inv.CHAPTER;
                                        m.DeclarationId = decl.ID;
                                        m.InvoiceContractorId = invContr.ROW_KEY;
                                        //ссылка на СФ стороны 2(контрагент)
                                        m.InvoiceContractorChapter = invContr.CHAPTER;
                                        m.DeclarationContractorId = declContr.ID;
                                        m.Status = 1;

                                        if (mismatchType != 1)
                                        {
                                            m.InvoiceContractorId = invContr.ROW_KEY;
                                            //ссылка на СФ стороны 2(контрагент)
                                            m.InvoiceContractorChapter = invContr.CHAPTER;
                                            m.DeclarationContractorId = declContr.ID;
                                        }

                                        decl.Mismatches.Add(m); //добавили расхождение в декларацию
                                        idInvoice++;
                                    }

                                }
                                for (int k1 = 0; k1 < r.Next(1, 7); k1++)//добавим версии для агрегированной СФ
                                {
                                    InvoiceINN innsAggr = new InvoiceINN(GetIdFromPull(), GetIdFromPull(), GetIdFromPull());
                                    var invInner = GetInvoiceLine(++idDiscr, r, chIdx, NalogPeriod.GetInnerPeriodDT(period1, decl.FISCAL_YEAR), innsAggr, decl.DECLARATION_VERSION_ID, false);
                                    invInner.DeclarationId = decl.DECLARATION_VERSION_ID;
                                    invInner.ACTUAL_ROW_KEY = inv.ROW_KEY;
                                    invInner.CHAPTER = inv.CHAPTER;
                                    Console.WriteLine(invInner.ToSql());
                                }
                            }
                        }
                    }
                }
            }

            Console.WriteLine("truncate table FIR.DISCREPANCY_RAW;");

            foreach (var declaration in decls)//пишем SQL комманды в аутпут
            {
                //Console.WriteLine(declaration.ToSql());
                foreach (var mismatch in declaration.Mismatches)
                {
                    Console.WriteLine(mismatch.ToSql());
                }
            }

            foreach (var declarationSummary in decls)
            {
                Console.WriteLine(declarationSummary.ToSql(r, ref svodZapId, (Convert.ToInt64(declarationSummary.INN) % 2) == 0));
            }

            foreach (var taxPayer in taxPayers)
            {
                Console.WriteLine(taxPayer.ToSqlFIR(sc++));
            }

            foreach (var taxPayer in taxPayers)
            {
                Console.WriteLine(taxPayer.ToSqlBankAccounts());
            }

            foreach (var cr in controlRatios)
            {
                Console.WriteLine(cr.ToSql());
            }

            Console.WriteLine("commit;");
        }

        private int ConvertChapterToChapterIntoRowKey(int chapter)
        {
            int ret = 0;
            if (chapter == 8)
                ret = 0;
            else if (chapter == 9)
                ret = 1;
            else if (chapter == 10)
                ret = 4;
            else if (chapter == 11)
                ret = 5;
            else if (chapter == 12)
                ret = 6;
            return ret;
        }

        private string CreateRowKey(long declVerId, int chapter, long id, bool isAgregate = false)
        {
            return string.Format("{0}/{1}/{2}{3}", declVerId, ConvertChapterToChapterIntoRowKey(chapter), isAgregate ? "A" : string.Empty, id);
        }

        private void CreateHalfEmptyDeclarations(Random r, ref long idDecl, ref long svodZp, Soun soun, string period, ref long tpId, ref long idDiscr)
        {
            # region Декларации, по которым сведения имеются только из СЭОД и СЭОД + МС
            var tpSeod = new TPInfo()
            {
                Inn = DataGenerator.GetRandomINN_10digit(),
                Name = Guid.NewGuid().ToString()
            };

            var tpMsSeod = new TPInfo()
            {
                Inn = DataGenerator.GetRandomINN_10digit(),
                Name = Guid.NewGuid().ToString()
            };
            var declSeod = GetDeclarationSummary(idDecl++, r, period, tpSeod, soun, 0);
            Console.WriteLine(declSeod.ToSql(r, ref svodZp, true, DeclarationPartsToGenerate.SeodOnly));
            Console.WriteLine(tpSeod.ToSqlFIR(tpId++));

            var declSeodMs = GetDeclarationSummary(idDecl++, r, period, tpMsSeod, soun, 0);
            Console.WriteLine(declSeodMs.ToSql(r, ref svodZp, true, DeclarationPartsToGenerate.SeodAndMs));
            Console.WriteLine(tpSeod.ToSqlFIR(tpId++));


            for (int chIdx = 8; chIdx <= 14; chIdx++)
            {
                var tpCounter = new TPInfo()
                    {
                        Inn = DataGenerator.GetRandomINN_10digit(),
                        Name = Guid.NewGuid().ToString()
                    };


                tpCounter.ToSqlFIR(tpId++);

                var id = GetIdFromPull();
                var inns = new InvoiceINN(GetIdFromPull(), GetIdFromPull(), GetIdFromPull());
                if (chIdx == 8 || chIdx == 11 || chIdx == 13)
                    inns = new InvoiceINN(declSeodMs, tpCounter) { BROKER_INN = id.INN, BROKER_KPP = id.KPP };
                else if (chIdx == 9 || chIdx == 10 || chIdx == 12 || chIdx == 14)
                    inns = new InvoiceINN(declSeodMs, tpCounter, false) { BROKER_INN = id.INN, BROKER_KPP = id.KPP };

                var inv = GetInvoiceLine(++idDiscr, r, chIdx, NalogPeriod.GetInnerPeriodDT(period, declSeodMs.FISCAL_YEAR), inns, declSeodMs.DECLARATION_VERSION_ID, true); //сгенерируем саму запись о СФ
                inv.DeclarationId = declSeodMs.DECLARATION_VERSION_ID; //замапим ее на версию декларации
                Console.WriteLine(inv.ToSql()); //запишем инсерт команду на выход

                var declContr = GetDeclarationSummary(idDecl++, r, period, tpCounter, soun, 0);
                    //создадим декларацию контрагента с фейковым налогоплательщиком

                Console.WriteLine(declContr.ToSql(r, ref svodZp, true, DeclarationPartsToGenerate.SeodOnly));
                
                id = GetIdFromPull();
                var innsContr = new InvoiceINN(declSeodMs, declContr) { BROKER_INN = id.INN, BROKER_KPP = id.KPP };
                var invContr = GetInvoiceLine(++idDiscr, r, 9,
                                              NalogPeriod.GetInnerPeriodDT(period, declContr.FISCAL_YEAR), innsContr, declContr.DECLARATION_VERSION_ID,true);
                    //создаем сопоставленную СФ 
                invContr.DeclarationId = declContr.DECLARATION_VERSION_ID; //привязываем ее к декларации контрагента
                Console.WriteLine(invContr.ToSql()); //пишем интерт по СФ в аутпут
            }


            #endregion
        }


        private static int scPull_INN = 0;
        private static List<IdPair> pull_INN = new List<IdPair>();

        private static IdPair GetInnForDeclaration()
        {
            var item = new IdPair();
            pull_INN.Add(item);
            return item;
        }

        private static IdPair GetIdFromPull()
        {
            if (pull_INN.Count == 0)
            {
                return new IdPair();
            }
            
            // what?
            if (scPull_INN < 0 || scPull_INN >= pull_INN.Count)
                return new IdPair("", "");

            var id = pull_INN[scPull_INN];
            ++scPull_INN;
            if (scPull_INN >= pull_INN.Count)
            {
                scPull_INN = 0;
            }
            return id;
        }

        private static IEnumerable<ControlRatio> GetControlRatio(long startId, Random random, Declaration decl)
        {
            var codes = new[]
            {
               "1.1", "1.2", "1.3.1", "1.3.2", "1.3.3", "1.3.4", "1.4", "1.5", "1.6", "1.7", "1.8", "1.9.1", "1.9.2", "1.10",
               "1.11", "1.12", "1.13", "1.14", "1.15", "1.16", "1.17", "1.18", "1.19", "1.20",
               "1.21", "1.22", "1.23", "1.24", "1.25.1", "1.25.2", "1.25.3", "1.25.4", "1.25.5", "1.25.6", "1.26", "1.27", "1.28", "1.29.1", "1.29.2", "1.30",
               "1.31", "1.32", "1.33", "1.34", "1.35", "1.36", "1.37", "1.38", "1.39", "1.40",
               "1.41", "1.42", "1.43", "1.44", "1.45", "1.46", "1.47"
            };

            var id = startId;
            var crCount = random.Next(1, 20);
            for (int j = 0; j < crCount; j++)
            {
                yield return new ControlRatio
                {
                    Id = id++,
                    Type_Code = codes[random.Next(0, codes.Length - 1)],
                    AskDeklId = decl.ASK_DECL_ID,
                    Disparity_Right = DataGenerator.GetRandomDecimalString(),
                    Disparity_Left = DataGenerator.GetRandomDecimalString(),
                    Complete = random.Next(99) < 50
                };
            }
        }

        private void MapInvoice(Declaration side1, Declaration side2)
        {

        }

        private int GetLKError(int chapter, Random r)
        {
            switch (chapter)
            {
                case 8:
                    return r.Next(8001, 8004);
                case 9:
                    return r.Next(9020, 9026);
                case 10:
                    return r.Next(1001, 1015);
                case 11:
                    return r.Next(1101, 1115);
                default:
                    return 0;
            }
        }

//        [Test]
//        public void GenerateInvoices()
//        {
//            Random r = new Random();
//            Console.WriteLine("truncate table FIR.INVOICE_RAW;");
//            for (int i = 0; i < 10; i++)
//            {
//                var inns = new InvoiceINN(GetIdFromPull(), GetIdFromPull(), GetIdFromPull());
//                Invoice invoice = GetInvoiceLine(i, r, 8, DataGenerator.GetRandomDateString(), inns);
//                Console.WriteLine(invoice.ToSql());
//            }
//        }

        private Invoice GetInvoiceLine(long id, Random r, int chapter, DateTime? invoceDate, InvoiceINN inns, long declId, bool isAggregate = false)
        {
            var ident = GetIdFromPull();
            return new Invoice
            {
                CHAPTER = chapter,
                ORDINAL_NUMBER = id + 1,
                OKV_CODE = "810",
                CREATE_DATE = DataGenerator.GetRandomDateString(),
                RECEIVE_DATE = DataGenerator.GetRandomDateString(),
                OPERATION_CODE = DataGenerator.GetRandomOperationCode(),
                INVOICE_NUM = DataGenerator.GetRandomInvoiceNumber(),
                INVOICE_DATE = invoceDate,
                CHANGE_NUM = DataGenerator.GetRandomInvoiceChangeNum(),
                CHANGE_DATE = DataGenerator.GetRandomDateString(),
                CORRECTION_NUM = DataGenerator.GetRandomInvoiceChangeNum(),
                CORRECTION_DATE = DataGenerator.GetRandomDateString(),
                CHANGE_CORRECTION_NUM = DataGenerator.GetRandomInvoiceChangeNum(),
                CHANGE_CORRECTION_DATE = DataGenerator.GetRandomDateString(),
                RECEIPT_DOC_NUM = DataGenerator.GetRandomInvoiceNumber(),
                RECEIPT_DOC_DATE = DataGenerator.GetRandomDateString(),
                BUY_ACCEPT_DATE = new NDS2.Common.Contracts.DTO.Business.Book.DocDates(DataGenerator.GetRandomDateString()),
                BUYER_INN = inns.BUYER_INN,
                BUYER_KPP = inns.BUYER_KPP,
                BUYER_NAME = DataGenerator.GetRandomTaxPayerName(),
                SELLER_INN = inns.SELLER_INN,
                SELLER_KPP = inns.SELLER_KPP,
                SELLER_NAME = DataGenerator.GetRandomTaxPayerName(),
                SELLER_INVOICE_NUM = DataGenerator.GetRandomInvoiceNumber(),
                SELLER_INVOICE_DATE = DataGenerator.GetRandomDateString(),
                BROKER_INN = ident.INN,
                BROKER_KPP = ident.KPP,
                BROKER_NAME = DataGenerator.GetRandomTaxPayerName(),
                DEAL_KIND_CODE = r.Next(1, 4),
                CUSTOMS_DECLARATION_NUM = DataGenerator.GetRandomTaxPayerName(),
                PRICE_BUY_AMOUNT = DataGenerator.GetRandomDecimal(),
                PRICE_BUY_NDS_AMOUNT = DataGenerator.GetRandomDecimal(),
                PRICE_SELL = DataGenerator.GetRandomNullableDecimal(),
                PRICE_SELL_IN_CURR = DataGenerator.GetRandomDecimal(),
                PRICE_SELL_18 = DataGenerator.GetRandomDecimal(),
                PRICE_SELL_10 = DataGenerator.GetRandomDecimal(),
                PRICE_SELL_0 = DataGenerator.GetRandomDecimal(),
                PRICE_NDS_18 = DataGenerator.GetRandomDecimal(),
                PRICE_NDS_10 = DataGenerator.GetRandomDecimal(),
                PRICE_TAX_FREE = DataGenerator.GetRandomDecimal(),
                PRICE_TOTAL = DataGenerator.GetRandomDecimal(),
                PRICE_NDS_TOTAL = DataGenerator.GetRandomDecimal(),
                DIFF_CORRECT_DECREASE = DataGenerator.GetRandomDecimal(),
                DIFF_CORRECT_INCREASE = DataGenerator.GetRandomDecimal(),
                DIFF_CORRECT_NDS_DECREASE = DataGenerator.GetRandomDecimal(),
                DIFF_CORRECT_NDS_INCREASE = DataGenerator.GetRandomDecimal(),
                PRICE_NDS_BUYER = DataGenerator.GetRandomDecimal(),
                LOGICAL_ERRORS = GetLKError(chapter, r).ToString(),
                ROW_KEY = CreateRowKey(declId, chapter, id, isAggregate),
                ACTUAL_ROW_KEY = null
            };
        }


        public Declaration GetDeclarationSummary(long id, Random r, string period, TPInfo ti, Soun s, int version)
        {
            string year = "2014";

            Declaration obj = new Declaration();
            obj.ID = long.Parse(period + year + ti.Inn);
            obj.LK_ERRORS_COUNT = r.Next(1, 33);
            obj.CH8_DEALS_AMNT_TOTAL = 0;
            obj.CH9_DEALS_AMNT_TOTAL = 0;
            obj.CH8_NDS = DataGenerator.GetRandomPrice();
            obj.CH9_NDS = DataGenerator.GetRandomPrice();
            obj.DISCREP_CURRENCY_AMNT = 0;
            obj.GAP_DISCREP_COUNT = r.Next(1, 1000);
            obj.DISCREP_CURRENCY_COUNT = 0;
            obj.GAP_DISCREP_COUNT = r.Next(1, 1000);
            obj.GAP_DISCREP_AMNT = DataGenerator.GetRandomDecimal();
            obj.NDS_INCREASE_DISCREP_COUNT = r.Next(1, 1000);
            obj.NDS_INCREASE_DISCREP_AMNT = DataGenerator.GetRandomDecimal();
            obj.GAP_MULTIPLE_DISCREP_COUNT = r.Next(1, 1000);
            obj.GAP_MULTIPLE_DISCREP_AMNT = DataGenerator.GetRandomDecimal();
            obj.CONTROL_RATIO_COUNT = r.Next(1, 1000);
            obj.DISCREP_BUY_BOOK_AMNT = DataGenerator.GetRandomDecimal();
            obj.DISCREP_SELL_BOOK_AMNT = DataGenerator.GetRandomDecimal();
            obj.DISCREP_AVG_AMNT = r.Next(1, 1000);
            obj.DISCREP_MAX_AMNT = r.Next(1, 1000);
            obj.DISCREP_MIN_AMNT = r.Next(1, 1000);
            obj.DISCREP_TOTAL_AMNT = r.Next(1, 1000);
            obj.PVP_TOTAL_AMNT = r.Next(1, 1000);
            obj.PVP_DISCREP_MIN_AMNT = r.Next(1, 1000);
            obj.PVP_DISCREP_MAX_AMNT = r.Next(1, 1000);
            obj.PVP_DISCREP_AVG_AMNT = r.Next(1, 1000);
            obj.PVP_BUY_BOOK_AMNT = r.Next(1, 1000);
            obj.PVP_SELL_BOOK_AMNT = r.Next(1, 1000);
            obj.PVP_RECIEVE_JOURNAL_AMNT = r.Next(1, 1000);
            obj.PVP_SENT_JOURNAL_AMNT = r.Next(1, 1000);
            obj.SUR_CODE = 2;
            obj.CONTROL_RATIO_DATE = DateTime.Now;
            obj.CONTROL_RATIO_SEND_TO_SEOD = r.Next(99) < 50;
            obj.UPDATE_DATE = DataGenerator.GetRandomDateString();
            /*ASK Decl*/
            obj.ASK_DECL_ID = id + 100000;
            obj.DECLARATION_VERSION_ID = id + 200000;
            DictionaryNalogPeriods np = NalogPeriod.GetNalogPeriod(period, year);
            obj.DECL_DATE = np.DateEnd.AddDays(version * 10 + r.Next(0, 10));
            obj.TAX_PERIOD = period;
            obj.FISCAL_YEAR = year;
            obj.SOUN_ENTRY = new CodeValueDictionaryEntry(s.Code, "");
            obj.SOUN_NAME = s.Code + "-";
            obj.CORRECTION_NUMBER = version.ToString();// == 0 ? string.Empty : version.ToString(),
            long rest = 0;
            Math.DivRem(id, 2, out rest);
            obj.OKVED_CODE = (rest == 0) ? "12" : "13";
            obj.NAME = ti.IsUl ?  ti.Name : null;
            obj.INN = ti.Inn;
            obj.KPP = ti.Kpp;
            if (obj.INN == "5252031528")
                obj.COMPENSATION_AMNT = 0;
            else
                obj.COMPENSATION_AMNT = DataGenerator.GetRandom_COMPENSATION_AMNT();
            if (obj.COMPENSATION_AMNT > 0)
                obj.DECL_SIGN = "К уплате";
            else if (obj.COMPENSATION_AMNT < 0)
                obj.DECL_SIGN = "К возмещению";
            else
                obj.DECL_SIGN = "Нулевая";

            return obj;
        }

        [Test]
        public void ToSqlGeneration()
        {

            //Console.WriteLine(DateTime.Now.ToString("dd.MM.yyyy"));
            //return;
            Type t = typeof(DeclarationSummary);

            foreach (var propertyInfo in t.GetProperties())
            {
                //Console.WriteLine("sb.AppendFormat(\"{1}, \", this.{0});", GetPropertyRepresentation(propertyInfo) ,GetPropertyFormat(propertyInfo));
                //Console.WriteLine("," + propertyInfo.Name);
                Console.WriteLine("obj." + propertyInfo.Name + " = reader.ReadString(\"" + propertyInfo.Name + "\");");
            }

        }

        public Stack<Soun> GetSouns()
        {
            Stack<Soun> stack = new Stack<Soun>();
            //stack.Push(new Soun() { Code = "", Region = "" });
            
            stack.Push(new Soun() { Code = "0523", Region = "05" });
            stack.Push(new Soun() { Code = "1426", Region = "14" });
            stack.Push(new Soun() { Code = "1434", Region = "14" });
            stack.Push(new Soun() { Code = "1436", Region = "14" });
            stack.Push(new Soun() { Code = "1656", Region = "16" });
            stack.Push(new Soun() { Code = "1644", Region = "16" });

            //stack.Push(new Soun() { Code = "2028", Region = "20" });
            //stack.Push(new Soun() { Code = "2455", Region = "24" });
            //stack.Push(new Soun() { Code = "2622", Region = "26" });
            //stack.Push(new Soun() { Code = "2826", Region = "28" });
            //stack.Push(new Soun() { Code = "3423", Region = "34" });
            //stack.Push(new Soun() { Code = "3901", Region = "39" });
            //stack.Push(new Soun() { Code = "4013", Region = "40" });
            stack.Push(new Soun() { Code = "5252", Region = "52" });
            //stack.Push(new Soun() { Code = "5441", Region = "54" });
            //stack.Push(new Soun() { Code = "5521", Region = "55" });
            //stack.Push(new Soun() { Code = "5609", Region = "56" });
            //stack.Push(new Soun() { Code = "5942", Region = "59" });
            //stack.Push(new Soun() { Code = "6438", Region = "64" });
            //stack.Push(new Soun() { Code = "7715", Region = "77" });
            return stack;
        }

        public Stack<TPInfo> GetTaxPayerTestData()
        {
            Stack<TPInfo> stack = new Stack<TPInfo>();

            stack.Push(new TPInfo() { Inn = "5101600300", Kpp = string.Empty, Name = "Коломойский" });
            stack.Push(new TPInfo() { Inn = "4407009340", Kpp = "440701001", Name = "ООО \"М-про\"" });
            stack.Push(new TPInfo() { Inn = "5821400720", Kpp = "582101001", Name = "ООО \"Лунинское АТП\"" });
            stack.Push(new TPInfo() { Inn = "5245020719", Kpp = "524501001", Name = "ООО \"Контур\"" });
            stack.Push(new TPInfo() { Inn = "5821402333", Kpp = "582101001", Name = "МБУ \"МЦ Лунинского района\"" });
            stack.Push(new TPInfo() { Inn = "5245009105", Kpp = "524501001", Name = "ООО \"ТЭК \"Трансинвест-НН\"" });
            stack.Push(new TPInfo() { Inn = "5208012345", Kpp = "520801001", Name = "ООО \"Стройсфера\"" });
            stack.Push(new TPInfo() { Inn = "5263028774", Kpp = "526301001", Name = "ООО \"Компания \"Рента-НН\"" });
            stack.Push(new TPInfo() { Inn = "5245001234", Kpp = "524501001", Name = "Банк \"Ростовщик\" (ООО)" });
            stack.Push(new TPInfo() { Inn = "3017043047", Kpp = "302301001", Name = "ООО \"Клондайк\"" });
            stack.Push(new TPInfo() { Inn = "5262268974", Kpp = "526201001", Name = "ООО  \"Компания ТАЛИОН-ПРО\"" });
            stack.Push(new TPInfo() { Inn = "5252031373", Kpp = "525201001", Name = "ООО\"Мастер-кузнец Волков\"" });
            stack.Push(new TPInfo() { Inn = "5252029889", Kpp = "525201001", Name = "ООО \"ЛесПромСтрой\"" });
            stack.Push(new TPInfo() { Inn = "5821004050", Kpp = "582101001", Name = "Агентство по развитию предпринимательства" });
            stack.Push(new TPInfo() { Inn = "5252024425", Kpp = "525201001", Name = "Местная религиозная организация Церковь Христиан-Адвентистов Седьмого Дня г. Павлово Нижегородской области" });
            stack.Push(new TPInfo() { Inn = "3127000014", Kpp = "312701001", Name = "ОАО \"Лебединский ГОК\"" });
            stack.Push(new TPInfo() { Inn = "1835087959", Kpp = "184101001", Name = "ООО \"РегионЗемКом\"" });
            stack.Push(new TPInfo() { Inn = "5256085482", Kpp = "525601001", Name = "ООО \"НАШ\"" });
            stack.Push(new TPInfo() { Inn = "5245008630", Kpp = "524601001", Name = "ООО \"ВТГСМ\"" });
            stack.Push(new TPInfo() { Inn = "1660151953", Kpp = "166001001", Name = "ООО УК \"ЖКХ Танкодром\"" });
            stack.Push(new TPInfo() { Inn = "7727004113", Kpp = "770101001", Name = "ЗАО \"ЛАНИТ\"" });
            stack.Push(new TPInfo() { Inn = "6321130852", Kpp = "631801001", Name = "ООО \"РСУ\"" });
            stack.Push(new TPInfo() { Inn = "5252030556", Kpp = "525201001", Name = "ООО \"Метторгснаб НН\"" });
            stack.Push(new TPInfo() { Inn = "7713056834", Kpp = "775001001", Name = "ОАО \"АльфаСтрахование\"" });
            stack.Push(new TPInfo() { Inn = "5252026574", Kpp = "525201001", Name = "ГК \"ВДОАМ - 2\"" });
            stack.Push(new TPInfo() { Inn = "5245995112", Kpp = "524501001", Name = "БФ \"Начало Жизни\"" });
            stack.Push(new TPInfo() { Inn = "7602038533", Kpp = "760301001", Name = "ООО \"Алекс плюс\"" });
            stack.Push(new TPInfo() { Inn = "5027105930", Kpp = "502701001", Name = "ООО \"БЛЮЗ\"" });
            stack.Push(new TPInfo() { Inn = "5245020797", Kpp = "524501001", Name = "ООО \"СГМ\"" });
            stack.Push(new TPInfo() { Inn = "5256106252", Kpp = "526201001", Name = "ООО \"Орион\"" });
            stack.Push(new TPInfo() { Inn = "5208001234", Kpp = "520801001", Name = "ТСЖ \"Чкаловский\"" });
            stack.Push(new TPInfo() { Inn = "5245012370", Kpp = "524501001", Name = "ООО \"Вираж\"" });
            stack.Push(new TPInfo() { Inn = "5836140881", Kpp = "583601001", Name = "Ассоциация \"СмоПо\"" });
            stack.Push(new TPInfo() { Inn = "7706724054", Kpp = "770601001", Name = "ЗАО \"ВК Комфорт\"" });
            stack.Push(new TPInfo() { Inn = "5252024425", Kpp = "525201001", Name = "Поместная Церковь Христиан-Адвентистов Седьмого Дня г. Павлово Нижегородской области" });
            stack.Push(new TPInfo() { Inn = "5260075303", Kpp = "525901001", Name = "ООО Фирма \"НикО\"" });
            stack.Push(new TPInfo() { Inn = "7733584350", Kpp = "773301001", Name = "ООО \"ПРОМНЕТ\"" });
            stack.Push(new TPInfo() { Inn = "2129040677", Kpp = "213001001", Name = "ООО \"Саяны-центр\"" });
            stack.Push(new TPInfo() { Inn = "7203181482", Kpp = "720301001", Name = "ООО \"Аптеки 36,6 \"Тюмень\"" });
            stack.Push(new TPInfo() { Inn = "7017268626", Kpp = "701701001", Name = "ООО \"Тк-Св\"" });
            stack.Push(new TPInfo() { Inn = "5252028606", Kpp = "525201001", Name = "ООО \"САДКО ПЛЮС\"" });
            stack.Push(new TPInfo() { Inn = "7719726789", Kpp = "771901001", Name = "ООО \"Полюс\"" });
            stack.Push(new TPInfo() { Inn = "5252015572", Kpp = "525201001", Name = "ООО \"КОМФОРТ +\"" });
            stack.Push(new TPInfo() { Inn = "5252014152", Kpp = "525201001", Name = "ООО \"Сервисцентр\"" });
            stack.Push(new TPInfo() { Inn = "1627005803", Kpp = "162701001", Name = "ООО  \"НЕПТУН\"" });
            stack.Push(new TPInfo() { Inn = "2309102185", Kpp = "230901001", Name = "ООО \"Кавинформресурс\"" });
            stack.Push(new TPInfo() { Inn = "5821003875", Kpp = "582101001", Name = "АДМИНИСТРАЦИЯ ЛУНИНСКОГО РАЙОНА" });
            stack.Push(new TPInfo() { Inn = "5253003900", Kpp = "526201001", Name = "ООО СК \"Надежда-НН\"" });
            stack.Push(new TPInfo() { Inn = "2330002810", Kpp = "233001001", Name = "Крестьянское хозяйство \"Остров Надежды\"" });
            stack.Push(new TPInfo() { Inn = "5245002345", Kpp = "524501001", Name = "ОАО \"Птицефабрика Симбирская\"" });
            stack.Push(new TPInfo() { Inn = "5252012099", Kpp = "525201001", Name = "ООО\"АгроТрейд\"" });
            stack.Push(new TPInfo() { Inn = "5245016208", Kpp = "524501001", Name = "ООО \"Сварочный Легион\"" });
            stack.Push(new TPInfo() { Inn = "5252023654", Kpp = "525201001", Name = "ООО \"МАИ\"" });
            stack.Push(new TPInfo() { Inn = "1327013641", Kpp = "132701001", Name = "ООО \"Тройка лек\"" });
            stack.Push(new TPInfo() { Inn = "5252022026", Kpp = "525201001", Name = "ООО \"Производственно-строительная компания\"" });
            stack.Push(new TPInfo() { Inn = "1627008723", Kpp = "162701001", Name = "ООО \"ЕРЦ Менделеевск\"" });
            stack.Push(new TPInfo() { Inn = "1646027961", Kpp = "164601001", Name = "УК ООО \"ЖИЛКОМФОРТСЕРВИС\"" });
            stack.Push(new TPInfo() { Inn = "5321119060", Kpp = "532101001", Name = "ООО \"СУ - 3\"" });
            stack.Push(new TPInfo() { Inn = "5236003330", Kpp = "523601001", Name = "СПК (колхоз) \"Прогресс\"" });
            stack.Push(new TPInfo() { Inn = "5252017562", Kpp = "525201001", Name = "ОО \"ФЛГ\"" });
            stack.Push(new TPInfo() { Inn = "1653018661", Kpp = "165801001", Name = "ООО КБЭР \"БАНК КАЗАНИ\"" });
            stack.Push(new TPInfo() { Inn = "7729655706", Kpp = "502701001", Name = "ООО \"Гламберри\"" });
            stack.Push(new TPInfo() { Inn = "7743737189", Kpp = "774301001", Name = "ООО \"Эра Красоты\"" });
            stack.Push(new TPInfo() { Inn = "7439009108", Kpp = "742401001", Name = "МБМУ \"Нижнесанарская УБ\"" });
            stack.Push(new TPInfo() { Inn = "5245012345", Kpp = "524501001", Name = "СНТ \"Радист\"" });
            stack.Push(new TPInfo() { Inn = "2627017494", Kpp = "263001001", Name = "ООО \"СЕРВИС-КМВ\"" });
            stack.Push(new TPInfo() { Inn = "2464104677", Kpp = "246401001", Name = "ООО \"Авер-Дент\"" });
            stack.Push(new TPInfo() { Inn = "6316126498", Kpp = "631601001", Name = "ООО \"АПОГЕЙ-С\"" });
            stack.Push(new TPInfo() { Inn = "6027111728", Kpp = "602701001", Name = "ООО\"МВ ДЛЯ ОФИСА\"" });
            //stack.Push(new TPInfo() { Inn = "5260337252", Kpp = "525201001", Name = "ООО \"Капитал плюс\"" });
            stack.Push(new TPInfo() { Inn = "5260337252", Kpp = "", Name = "ООО \"Капитал плюс\"" });
            stack.Push(new TPInfo() { Inn = "5245012926", Kpp = "524501001", Name = "ООО \"Монолит\"" });
            stack.Push(new TPInfo() { Inn = "5263023456", Kpp = "525701001", Name = "ЗАО \"Оптерон\"" });
            stack.Push(new TPInfo() { Inn = "5609066470", Kpp = "560901001", Name = "ООО \"Оренпромавтоматика\"" });
            stack.Push(new TPInfo() { Inn = "5410037050", Kpp = "541001001", Name = "ООО \"Будь здоров\"" });
            stack.Push(new TPInfo() { Inn = "3664088086", Kpp = "773401001", Name = "ООО \"ПАНОРАМА\"" });
            stack.Push(new TPInfo() { Inn = "8614002935", Kpp = "861401001", Name = "БУ \"ОКТЯБРЬСКАЯ РАЙОННАЯ БОЛЬНИЦА\"" });
            stack.Push(new TPInfo() { Inn = "5245000448", Kpp = "524501001", Name = "ООО предприятие \"АЛИДИ\"" });
            stack.Push(new TPInfo() { Inn = "5252031528", Kpp = "525201001", Name = "ООО \"Триумф\"" });
            stack.Push(new TPInfo() { Inn = "5821400255", Kpp = "582101001", Name = "МУП\"МТС плюс\"" });
            stack.Push(new TPInfo() { Inn = "5252032909", Kpp = "525201001", Name = "ООО \"ФЕМИДА\"" });
            stack.Push(new TPInfo() { Inn = "7722144195", Kpp = "312001001", Name = "ООО\"ПКФ Атлас\"" });
            return stack;
        }

        private string GetPropertyFormat(PropertyInfo pi)
        {
            if (pi.PropertyType == typeof(DateTime) || pi.PropertyType == typeof(DateTime?))
            {
                return "to_date('{0}', 'dd.mm.yyyy')";
            }

            if (pi.PropertyType == typeof(string))
            {
                return "'{0}'";
            }

            return "{0}";
        }

        private string GetPropertyRepresentation(PropertyInfo pi)
        {
            if (pi.PropertyType == typeof(DateTime))
            {
                return string.Format("{0}.ToString(\"dd.MM.yyyy\")", pi.Name);
            }

            if (pi.PropertyType == typeof(decimal) || pi.PropertyType == typeof(decimal?))
            {
                return string.Format("{0}.ToString(ci)", pi.Name);
            }

            return pi.Name;
        }
    }


}
