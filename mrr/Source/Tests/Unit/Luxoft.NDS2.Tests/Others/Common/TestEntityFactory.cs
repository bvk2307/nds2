﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Tests.Others.Entity;
using Luxoft.NDS2.Tests.Others.TestDataGenerator;

namespace Luxoft.NDS2.Tests.Others.Common
{
    public class TestEntityFactory
    {
        public static SelectionTestEntity CreateSelection(
            long selectionId,
            Soun s)
        {

            var filter = new SelectionFilter();
            filter.GroupsFilters.Add(new GroupFilter()
            {
                IsActive = true,
                Filters = new List<FilterCriteria>()
                        {new FilterCriteria(){Name = "Регион", LookupTableName = "v$ssrf", FirInternalName = "REGION", InternalName = "Region", IsActive = true, IsRequired = true, Values = new List<FilterElement>(){ new FilterElement(){Operator = FilterElement.ComparisonOperations.Equals, ValueOne = s.Code.Substring(0,2)}}}
                        , new FilterCriteria(){Name = "Инспекция", LookupTableName = "v$sono", FirInternalName = "IFNS_NUMBER", InternalName = "TaxOrgan", IsActive = true, IsRequired = false, Values = new List<FilterElement>(){ new FilterElement(){Operator = FilterElement.ComparisonOperations.Equals, ValueOne = s.Code}}}}
            });

            return new SelectionTestEntity()
                {
                    Analytic = "LoadTest",
                    CreationDate = DateTime.Now,
                    Id = selectionId,
                    Name = s.Region + "_" + Guid.NewGuid().ToString(),
                    Status = SelectionStatus.Loading,
                    //Filter = filter,
                    TestCode = int.Parse(s.Code)
                };
        }

        public static TPInfo CreateTaxPayer()
        {
            return new TPInfo()
                {
                    Inn = DataGenerator.GetRandomINN_10digit(),
                    Kpp = DataGenerator.GetRandomKPP(),
                    Name = DataGenerator.GetRandomTaxPayerName()
                };
        }

        public static Declaration CreateDeclaration(
            long id, 
            string taxYear, 
            string period, 
            TPInfo ti, 
            Soun s,
            Random r,
            int version)
        {
            Declaration obj = new Declaration();
            obj.ID = long.Parse(period + taxYear + ti.Inn);
            obj.LK_ERRORS_COUNT = r.Next(1, 33);
            obj.CH8_DEALS_AMNT_TOTAL = 0;
            obj.CH9_DEALS_AMNT_TOTAL = 0;
            obj.CH8_NDS = 0;
            obj.CH9_NDS = 0;
            obj.DISCREP_CURRENCY_AMNT = 0;
            obj.GAP_DISCREP_COUNT = r.Next(1, 1000);
            obj.DISCREP_CURRENCY_COUNT = 0;
            obj.GAP_DISCREP_COUNT = r.Next(1, 1000);
            obj.GAP_DISCREP_AMNT = DataGenerator.GetRandomDecimal();
            obj.NDS_INCREASE_DISCREP_COUNT = r.Next(1, 1000);
            obj.NDS_INCREASE_DISCREP_AMNT = DataGenerator.GetRandomDecimal();
            obj.GAP_MULTIPLE_DISCREP_COUNT = r.Next(1, 1000);
            obj.GAP_MULTIPLE_DISCREP_AMNT = DataGenerator.GetRandomDecimal();
            obj.CONTROL_RATIO_COUNT = r.Next(1, 1000);
            obj.DISCREP_BUY_BOOK_AMNT = DataGenerator.GetRandomDecimal();
            obj.DISCREP_SELL_BOOK_AMNT = DataGenerator.GetRandomDecimal();
            obj.DISCREP_AVG_AMNT = r.Next(1, 1000);
            obj.DISCREP_MAX_AMNT = r.Next(1, 1000);
            obj.DISCREP_MIN_AMNT = r.Next(1, 1000);
            obj.DISCREP_TOTAL_AMNT = r.Next(1, 1000);
            obj.PVP_TOTAL_AMNT = r.Next(1, 1000);
            obj.PVP_DISCREP_MIN_AMNT = r.Next(1, 1000);
            obj.PVP_DISCREP_MAX_AMNT = r.Next(1, 1000);
            obj.PVP_DISCREP_AVG_AMNT = r.Next(1, 1000);
            obj.PVP_BUY_BOOK_AMNT = r.Next(1, 1000);
            obj.PVP_SELL_BOOK_AMNT = r.Next(1, 1000);
            obj.PVP_RECIEVE_JOURNAL_AMNT = r.Next(1, 1000);
            obj.PVP_SENT_JOURNAL_AMNT = r.Next(1, 1000);
            obj.SUR_CODE = 2;
            obj.CONTROL_RATIO_DATE = DateTime.Now;
            obj.CONTROL_RATIO_SEND_TO_SEOD = r.Next(99) < 50;
            obj.UPDATE_DATE = DataGenerator.GetRandomDateString();
            /*ASK Decl*/
            obj.ASK_DECL_ID = id + 100000;
            obj.DECLARATION_VERSION_ID = id + 200000;
            DictionaryNalogPeriods np = NalogPeriod.GetNalogPeriod(period, taxYear);
            obj.DECL_DATE = np.DateEnd.AddDays(version * 10 + r.Next(0, 10));
            obj.TAX_PERIOD = period;
            obj.FISCAL_YEAR = taxYear;
            obj.SOUN_ENTRY = new CodeValueDictionaryEntry(s.Code, "");
            obj.SOUN_NAME = s.Code + "-";
            obj.CORRECTION_NUMBER = version.ToString();// == 0 ? string.Empty : version.ToString(),
            obj.OKVED_CODE = "12";
            obj.NAME = ti.Name;
            obj.INN = ti.Inn;
            obj.KPP = ti.Kpp;

            return obj;
        }

        public static Invoice CreateInvoice(long id, string invoiceNumber, int chapter, DateTime? invoceDate, Declaration buyer, Declaration seller, Random r)
        {
            return new Invoice
            {
                DeclarationId = buyer.DECLARATION_VERSION_ID,
                CHAPTER = chapter,
                ORDINAL_NUMBER = id + 1,
                OKV_CODE = "810",
                CREATE_DATE = DataGenerator.GetRandomDateString(),
                RECEIVE_DATE = DataGenerator.GetRandomDateString(),
                OPERATION_CODE = DataGenerator.GetRandomOperationCode(),
                INVOICE_NUM = invoiceNumber,
                INVOICE_DATE = invoceDate,
                CHANGE_NUM = DataGenerator.GetRandomInvoiceChangeNum(),
                CHANGE_DATE = DataGenerator.GetRandomDateString(),
                CORRECTION_NUM = DataGenerator.GetRandomInvoiceChangeNum(),
                CORRECTION_DATE = DataGenerator.GetRandomDateString(),
                CHANGE_CORRECTION_NUM = DataGenerator.GetRandomInvoiceChangeNum(),
                CHANGE_CORRECTION_DATE = DataGenerator.GetRandomDateString(),
                RECEIPT_DOC_NUM = DataGenerator.GetRandomInvoiceNumber(),
                RECEIPT_DOC_DATE = DataGenerator.GetRandomDateString(),
                BUY_ACCEPT_DATE = new NDS2.Common.Contracts.DTO.Business.Book.DocDates(DataGenerator.GetRandomDateString()),
                BUYER_INN = buyer.INN,
                BUYER_KPP = buyer.KPP,
                BUYER_NAME = DataGenerator.GetRandomTaxPayerName(),
                SELLER_INN = seller.INN,
                SELLER_KPP = seller.KPP,
                SELLER_NAME = DataGenerator.GetRandomTaxPayerName(),
                SELLER_INVOICE_NUM = DataGenerator.GetRandomInvoiceNumber(),
                SELLER_INVOICE_DATE = DataGenerator.GetRandomDateString(),
                BROKER_INN = string.Empty,
                BROKER_KPP = string.Empty,
                BROKER_NAME = DataGenerator.GetRandomTaxPayerName(),
                DEAL_KIND_CODE = r.Next(1, 4),
                CUSTOMS_DECLARATION_NUM = DataGenerator.GetRandomTaxPayerName(),
                PRICE_BUY_AMOUNT = DataGenerator.GetRandomDecimal(),
                PRICE_BUY_NDS_AMOUNT = DataGenerator.GetRandomDecimal(),
                PRICE_SELL = DataGenerator.GetRandomNullableDecimal(),
                PRICE_SELL_IN_CURR = DataGenerator.GetRandomDecimal(),
                PRICE_SELL_18 = DataGenerator.GetRandomDecimal(),
                PRICE_SELL_10 = DataGenerator.GetRandomDecimal(),
                PRICE_SELL_0 = DataGenerator.GetRandomDecimal(),
                PRICE_NDS_18 = DataGenerator.GetRandomDecimal(),
                PRICE_NDS_10 = DataGenerator.GetRandomDecimal(),
                PRICE_TAX_FREE = DataGenerator.GetRandomDecimal(),
                PRICE_TOTAL = DataGenerator.GetRandomDecimal(),
                PRICE_NDS_TOTAL = DataGenerator.GetRandomDecimal(),
                DIFF_CORRECT_DECREASE = DataGenerator.GetRandomDecimal(),
                DIFF_CORRECT_INCREASE = DataGenerator.GetRandomDecimal(),
                DIFF_CORRECT_NDS_DECREASE = DataGenerator.GetRandomDecimal(),
                DIFF_CORRECT_NDS_INCREASE = DataGenerator.GetRandomDecimal(),
                PRICE_NDS_BUYER = DataGenerator.GetRandomDecimal(),
                LOGICAL_ERRORS = string.Empty,
                ROW_KEY = DataGenerator.GetRandomKPP(),
                ACTUAL_ROW_KEY = null
            };
        }

        public static Mismatch CreateMismatch(long id, Random rand, Declaration side1Decl, Invoice side1Invoice, Declaration side2Decl, Invoice side2Invoice)
        {
            return new Mismatch()
                {
                    Id = id,
                    Type = rand.Next(1,4),
                    CompareKind = 1,
                    RuleGroup = 1,
                    DealAmount = DataGenerator.GetRandomDecimal(),
                    MismatchesAmount = DataGenerator.GetRandomDecimal(),
                    PVPAmount = DataGenerator.GetRandomDecimal(),
                    Course = DataGenerator.GetRandomDecimal(),
                    CourseCost = DataGenerator.GetRandomDecimal(),
                    SurCode = 2,
                    InvoiceId = side1Invoice.ROW_KEY, //ссылка на СФ стороны 1
                    InvoiceChapter = side1Invoice.CHAPTER,
                    DeclarationId = side1Decl.ID,
                    InvoiceContractorId = side2Invoice.ROW_KEY,
                    //ссылка на СФ стороны 2(контрагент)
                    InvoiceContractorChapter = side2Invoice.CHAPTER,
                    DeclarationContractorId = side2Decl.ID,
                    Status = 1
                };
        }
    }
}
