﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Tests.Others.TestDataGenerator;

namespace Luxoft.NDS2.Tests.Others.Common
{
    public enum DeclarationPartsToGenerate
    {
        All,
        SeodOnly,
        SeodAndMs
    }

    public static class Extensions
    {
        public static string ToSql(this Declaration declaration, Random rnd, ref long SvodZapId, bool isNotJournal = true, DeclarationPartsToGenerate partsToGenerate = DeclarationPartsToGenerate.All)
        {
            StringBuilder sb = new StringBuilder();
            CultureInfo ci = new CultureInfo("en-GB");
            string namePrefix = partsToGenerate == DeclarationPartsToGenerate.All
                                    ? string.Empty
                                    : partsToGenerate == DeclarationPartsToGenerate.SeodAndMs ? "МС_СЭОД_И_" : "СЭОД_";

            if (partsToGenerate == DeclarationPartsToGenerate.All)
            {
                sb.Append("insert into NDS2_MRR_USER.SOV_DECLARATION_INFO values(");

                sb.AppendFormat("{0}, ", declaration.ID);
                sb.AppendFormat("{0}, ", declaration.ASK_DECL_ID);
                sb.AppendFormat("{0}, ", declaration.DECLARATION_VERSION_ID);
                sb.AppendFormat("{0}, ", declaration.LK_ERRORS_COUNT);
                sb.AppendFormat("{0}, ", declaration.CH8_DEALS_AMNT_TOTAL.ToSqlString());
                sb.AppendFormat("{0}, ", declaration.CH9_DEALS_AMNT_TOTAL.ToSqlString());
                sb.AppendFormat("{0}, ", declaration.CH8_NDS.ToSqlString());
                sb.AppendFormat("{0}, ", declaration.CH9_NDS.ToSqlString());
                sb.AppendFormat("{0}, ", declaration.DISCREP_CURRENCY_AMNT.ToSqlString());
                sb.AppendFormat("{0}, ", declaration.DISCREP_CURRENCY_COUNT);
                sb.AppendFormat("{0}, ", declaration.GAP_DISCREP_COUNT);
                sb.AppendFormat("{0}, ", declaration.GAP_DISCREP_AMNT.ToSqlString());
                sb.AppendFormat("{0}, ", declaration.WEAK_DISCREP_COUNT);
                sb.AppendFormat("{0}, ", declaration.WEAK_DISCREP_AMNT.ToSqlString());
                sb.AppendFormat("{0}, ", declaration.NDS_INCREASE_DISCREP_COUNT);
                sb.AppendFormat("{0}, ", declaration.NDS_INCREASE_DISCREP_AMNT.ToSqlString());
                sb.AppendFormat("{0}, ", declaration.GAP_MULTIPLE_DISCREP_COUNT);
                sb.AppendFormat("{0}, ", declaration.GAP_MULTIPLE_DISCREP_AMNT.ToSqlString());
                sb.AppendFormat("{0}, ", declaration.DISCREP_BUY_BOOK_AMNT.ToSqlString());
                sb.AppendFormat("{0}, ", declaration.DISCREP_SELL_BOOK_AMNT.ToSqlString());
                sb.AppendFormat("{0}, ", declaration.DISCREP_MIN_AMNT.ToSqlString());
                sb.AppendFormat("{0}, ", declaration.DISCREP_MAX_AMNT.ToSqlString());
                sb.AppendFormat("{0}, ", declaration.DISCREP_AVG_AMNT.ToSqlString());
                sb.AppendFormat("{0}, ", declaration.DISCREP_TOTAL_AMNT.ToSqlString());

                sb.AppendFormat("{0}, ", declaration.PVP_TOTAL_AMNT.ToSqlString());
                sb.AppendFormat("{0}, ", declaration.PVP_DISCREP_MIN_AMNT.ToSqlString());
                sb.AppendFormat("{0}, ", declaration.PVP_DISCREP_MAX_AMNT.ToSqlString());
                sb.AppendFormat("{0}, ", declaration.PVP_DISCREP_AVG_AMNT.ToSqlString());
                sb.AppendFormat("{0}, ", declaration.PVP_BUY_BOOK_AMNT.ToSqlString());
                sb.AppendFormat("{0}, ", declaration.PVP_SELL_BOOK_AMNT.ToSqlString());
                sb.AppendFormat("{0}, ", declaration.PVP_RECIEVE_JOURNAL_AMNT.ToSqlString());
                sb.AppendFormat("{0}, ", declaration.PVP_SENT_JOURNAL_AMNT.ToSqlString());
                if (declaration.SUR_CODE != null)
                {
                    int cr = (int)declaration.SUR_CODE;
                    sb.AppendFormat("{0}, ", cr.ToString(CultureInfo.InvariantCulture));
                }
                if (declaration.CONTROL_RATIO_COUNT != null)
                {
                    long cr = (long)declaration.CONTROL_RATIO_COUNT;
                    sb.AppendFormat("{0}, ", cr.ToString(CultureInfo.InvariantCulture));
                }
                sb.AppendFormat("to_date('{0}', 'dd.mm.yyyy'), ", declaration.CONTROL_RATIO_DATE.Value.ToString("dd.MM.yyyy"));
                sb.AppendFormat("{0}, ", declaration.CONTROL_RATIO_SEND_TO_SEOD ? "1" : "0");
                sb.AppendFormat("to_date('{0}', 'dd.mm.yyyy'), ", declaration.UPDATE_DATE.Value.ToString("dd.MM.yyyy"));
                sb.AppendFormat("'{0}', ", declaration.CORRECTION_NUMBER);
                sb.Append("0, "); // DISCREP_TOTAL_COUNT     number default 0 not null,
                sb.Append("0, "); // ch8_deals_cnt_total     number default 0 not null,
                sb.Append("0, "); // ch9_deals_cnt_total     number default 0 not null, 
                sb.Append("0, "); // ch10_deals_cnt_total    number default 0 not null, 
                sb.Append("0, "); // ch11_deals_cnt_total    number default 0 not null, 
                sb.Append("0, "); // ch12_deals_cnt_total    number default 0 not null,
                sb.Append("0, "); // exact_match_count       number default 0 not null,
                sb.Append("0, "); // weak_and_nds_mis_count  number default 0 not null,
                sb.Append("0, "); // weak_and_nds_mis_amount number(19,2) default 0 not null
                sb.Append("0, "); //ch8_contractor_cnt     number default 0,
                sb.Append("0, "); // ch9_contractor_cnt     number default 0,
                sb.Append("0, "); // ch10_contractor_cnt    number default 0,
                sb.Append("0"); // ch11_contractor_cnt    number default 0
                sb.Append(");");

                sb.AppendLine();
            }

            sb.AppendFormat("insert into NDS2_MRR_USER.SEOD_DECLARATION (id, nds2_id, type, sono_code, tax_period, fiscal_year, correction_number, decl_reg_num, decl_fid, tax_payer_type, " +
                            "inn, kpp, insert_date, eod_date, date_receipt, id_file) VALUES (");
            sb.AppendFormat("{0}, ", declaration.ASK_DECL_ID + 100);//ID
            sb.AppendFormat("{0}, ", declaration.ID);//NDS2_ID
            sb.AppendFormat("{0}, ", isNotJournal ? 0 : 1);
            sb.AppendFormat("'{0}', ", string.IsNullOrEmpty(declaration.SOUN_ENTRY.EntryId) ? "0000" : declaration.SOUN_ENTRY.EntryId);//SONO_CODE
            sb.AppendFormat("'{0}', ", declaration.TAX_PERIOD);//TAX_PERIOD
            sb.AppendFormat("'{0}', ", declaration.FISCAL_YEAR);//FISCAL_YEAR
            sb.AppendFormat("'{0}', ", declaration.CORRECTION_NUMBER);//CORR_NUM
            sb.AppendFormat("{0}, ", declaration.ASK_DECL_ID + 100);//DECL_REGNUM
            sb.AppendFormat("'{0}', ", declaration.ID);//FID
            sb.AppendFormat("{0}, ", 1);
            sb.AppendFormat("'{0}', ", declaration.INN);
            sb.AppendFormat("'{0}', ", declaration.KPP);
            sb.AppendFormat("to_date('{0}', 'dd.mm.yyyy'), ", DateTime.Now.ToString("dd.MM.yyyy"));
            sb.AppendFormat("to_date('{0}', 'dd.mm.yyyy'), ", DateTime.Now.ToString("dd.MM.yyyy"));
            sb.AppendFormat("to_date('{0}', 'dd.mm.yyyy'), ", DateTime.Now.ToString("dd.MM.yyyy"));
            sb.AppendFormat("{0}", declaration.DECLARATION_VERSION_ID + 100000);//ID_FAIL
            sb.AppendFormat(");");

            sb.AppendLine();


            if (partsToGenerate == DeclarationPartsToGenerate.All || partsToGenerate == DeclarationPartsToGenerate.SeodAndMs)
            {
                sb.Append("insert into \"NDS2_MC\".\"ASKZIPФайл\"");
                sb.Append("(\"Ид\")");
                sb.Append(" values (");
                sb.AppendFormat("{0}", declaration.DECLARATION_VERSION_ID);
                sb.Append(");");

                sb.AppendLine();

                if (true /*isNotJournal*/)
                {
                    sb.Append("insert into \"NDS2_MC\".\"ASKДекл\"");
                    sb.Append(
                        "(\"Ид\", \"ZIP\", \"ДатаДок\", \"Период\", \"ОтчетГод\", \"КодНО\", \"НомКорр\", \"ОКВЭД\", \"НаимОрг\", \"ФамилияНП\", \"ОтчествоНП\", \"ИмяНП\", " +
                        "\"ИНННП\", \"КППНП\", \"КНД\", \"ПоМесту\", \"СумПУ173.5\", \"СумПУ173.1\", \"ПрПодп\", \"ПризнакНД\", \"СуммаНДС\", \"СуммаНалИсчисл\", \"СуммаВыч\", \"ИдФайл\")");
                    sb.Append(" values (");
                    sb.AppendFormat("{0}, ", declaration.ASK_DECL_ID);
                    sb.AppendFormat("{0}, ", declaration.DECLARATION_VERSION_ID);
                    sb.AppendFormat("to_date('{0}', 'dd.mm.yyyy'), ", declaration.DECL_DATE.Value.ToString("dd.MM.yyyy"));
                    sb.AppendFormat("'{0}', ", declaration.TAX_PERIOD);
                    sb.AppendFormat("'{0}', ", declaration.FISCAL_YEAR);
                    sb.AppendFormat("'{0}', ", declaration.SOUN_ENTRY.EntryId);
                    sb.AppendFormat("'{0}', ", declaration.CORRECTION_NUMBER);
                    sb.AppendFormat("'{0}', ", declaration.OKVED_CODE);
                    sb.AppendFormat("'{0}', ", string.IsNullOrEmpty(declaration.KPP) ? string.Empty : string.Format("{0}{1}", namePrefix, declaration.NAME));
                    sb.AppendFormat("'{0}', ", string.Format("{0}{1}", namePrefix, Guid.NewGuid()));
                    sb.AppendFormat("'{0}', ", Guid.NewGuid());
                    sb.AppendFormat("'{0}', ", Guid.NewGuid());
                    sb.AppendFormat("'{0}', ", declaration.INN);
                    sb.AppendFormat("'{0}', ", declaration.KPP);
                    sb.Append("'1151001',");
                    sb.Append("'116',");
                    sb.AppendFormat("{0}, ", (DateTime.Now.Millisecond % 2 == 0) ? "null" : rnd.Next(1000, 1000000).ToString());
                    sb.AppendFormat("{0}, ", (DateTime.Now.Millisecond % 2 == 0) ? rnd.Next(-10000, 10000).ToString() : "null");
                    sb.AppendFormat("1, "); // HACKHACKHACK "ПрПодп"
                    int ND_Priz = 0;
                    if (declaration.DECL_SIGN != null)
                    {
                        if (declaration.DECL_SIGN.ToLower() == "К уплате".ToLower())
                            ND_Priz = 1;
                        else if (declaration.DECL_SIGN.ToLower() == "К возмещению".ToLower())
                            ND_Priz = 2;
                        else if (declaration.DECL_SIGN.ToLower() == "Нулевая".ToLower())
                            ND_Priz = 3;
                    }
                    sb.AppendFormat("'{0}', ", ND_Priz);
                    string summNDS = "null";
                    if (declaration.COMPENSATION_AMNT != null)
                        summNDS = (string.Format("'{0}'", declaration.COMPENSATION_AMNT)).Replace(",", ".");
                    sb.AppendFormat("{0},", summNDS);
                    sb.AppendFormat("{0},", rnd.Next(0, 1000000).ToString());
                    sb.AppendFormat("{0},", rnd.Next(0, 1000000).ToString());
                    sb.AppendFormat("{0}", declaration.DECLARATION_VERSION_ID + 100000);
                    sb.Append(");");

                    sb.AppendLine();

                    for (int i = 0; i < 7; i++)
                    {
                        sb.Append("insert into \"NDS2_MC\".\"ASKСводЗап\"");
                        sb.Append("(\"Ид\", \"ИдДекл\", \"ZIP\", \"Индекс\", \"НомКорр\", \"ПризнакАкт\", \"ТипФайла\", \"СумНДСПок\", \"СумНДСПокДЛ\", \"СтПрод18\", \"СтПрод10\", \"СтПрод0\", \"СумНДСПрод18\", \"СумНДСПрод10\", \"СтПродОсв\", \"СтПрод18ДЛ\", \"СтПрод10ДЛ\", \"СтПрод0ДЛ\", \"СумНДС18ДЛ\", \"СумНДС10ДЛ\", \"СтПродОсвДЛ\", \"КолЗаписей\")");
                        sb.Append(" values (");
                        sb.AppendFormat("{0}, ", SvodZapId++);
                        sb.AppendFormat("{0}, ", declaration.ASK_DECL_ID);
                        sb.AppendFormat("{0}, ", declaration.DECLARATION_VERSION_ID);
                        sb.AppendFormat("'{0}', ", DataGenerator.GetRandomIndexChapterString(i));
                        sb.AppendFormat("'{0}', ", declaration.CORRECTION_NUMBER);
                        sb.AppendFormat("'{0}', ", declaration.IS_ACTUAL ? "1" : "0");
                        sb.AppendFormat("{0}, ", DataGenerator.GetChapterNum(i));
                        sb.AppendFormat("{0}, ", DataGenerator.GetRandomDecimalString());
                        sb.AppendFormat("{0}, ", DataGenerator.GetRandomDecimalString());
                        sb.AppendFormat("{0}, ", DataGenerator.GetRandomDecimalString());
                        sb.AppendFormat("{0}, ", DataGenerator.GetRandomDecimalString());
                        sb.AppendFormat("{0}, ", DataGenerator.GetRandomDecimalString());
                        sb.AppendFormat("{0}, ", DataGenerator.GetRandomDecimalString());
                        sb.AppendFormat("{0}, ", DataGenerator.GetRandomDecimalString());
                        sb.AppendFormat("{0}, ", DataGenerator.GetRandomDecimalString());
                        sb.AppendFormat("{0}, ", DataGenerator.GetRandomDecimalString());
                        sb.AppendFormat("{0}, ", DataGenerator.GetRandomDecimalString());
                        sb.AppendFormat("{0}, ", DataGenerator.GetRandomDecimalString());
                        sb.AppendFormat("{0}, ", DataGenerator.GetRandomDecimalString());
                        sb.AppendFormat("{0}, ", DataGenerator.GetRandomDecimalString());
                        sb.AppendFormat("{0}, ", DataGenerator.GetRandomDecimalString());
                        sb.AppendFormat("{0}", DataGenerator.GetRandomLongString());
                        sb.Append(");");
                        sb.AppendLine();
                    }
                }
                else
                {
                    sb.Append("insert into \"NDS2_MC\".\"ASKЖурналУч\"");
                    sb.Append(
                        "(\"Ид\", \"ZIPЧ1\", \"КНД\", \"Период\", \"ОтчетГод\", \"НаимОрг\",\"ИНН\", \"КПП\", \"ПрПодп\")");
                    sb.Append(" values (");
                    sb.AppendFormat("{0}, ", declaration.ASK_DECL_ID);
                    sb.AppendFormat("{0}, ", declaration.DECLARATION_VERSION_ID);
                    sb.Append("'1151001',");
                    sb.AppendFormat("'{0}', ", declaration.TAX_PERIOD);
                    sb.AppendFormat("'{0}', ", declaration.FISCAL_YEAR);
                    sb.AppendFormat("'{0}', ", string.Format("{0}{1}", namePrefix, declaration.NAME));
                    sb.AppendFormat("'{0}', ", declaration.INN);
                    sb.AppendFormat("'{0}', ", declaration.KPP);
                    sb.AppendFormat("1"); // HACKHACKHACK "ПрПодп"
                    sb.Append(");");
                }
            }

            return sb.ToString();
        }

        public static string ToSqlString(this decimal? value)
        {
            return value == null ? "null" : value.Value.ToString("F2", CultureInfo.GetCultureInfo("en-GB"));
        }

        public static string ToSqlLoaderString(this decimal? value)
        {
            return value == null ? "" : value.Value.ToString("F2", CultureInfo.GetCultureInfo("en-GB"));
        }

        public static string ToSqlString(this decimal value)
        {
            return value.ToString("F2", CultureInfo.GetCultureInfo("en-GB"));
        }
    }
}
