﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;
using NUnit.Framework;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Tests.Others
{
    [TestFixture]
    [Category("Manual")]
    public class XslValidator
    {
        private const string BASE_PATH = @"d:\exp\xsd";


        //[Test]
        public void ValidateSeod01()
        {
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.ValidationType = ValidationType.Schema;
            settings.ValidationFlags |= XmlSchemaValidationFlags.ProcessInlineSchema;
            settings.ValidationFlags |= XmlSchemaValidationFlags.ProcessSchemaLocation;
            settings.ValidationFlags |= XmlSchemaValidationFlags.ReportValidationWarnings;
            settings.ValidationEventHandler += new ValidationEventHandler(ValidationCallBack);

            DirectoryInfo di = new DirectoryInfo(BASE_PATH);

            foreach (var fileInfo in di.GetFiles("*.xml"))
            {
                Console.WriteLine("###################################Validating file:{0}", fileInfo.Name);

                XmlReader reader = XmlReader.Create(fileInfo.FullName, settings);
                // Parse the file. 
                while (reader.Read()) ;
            }
        }

        [Test]
        public void ValidateSeod01fromDb()
        {
            DirectoryInfo di = new DirectoryInfo(@"C:\Projects\Luxoft\GNIVC_NDS2\DEV\trunk\Release_1\Source\Database");

            string data;
            Encoding utf8 = new UTF8Encoding(false);
            foreach (var fileInfo in di.GetFiles("*.sql", SearchOption.AllDirectories))
            {
                using (StreamReader sr = new StreamReader(fileInfo.FullName, Encoding.Default ,true))
                {
                    data = sr.ReadToEnd();
                }

                using (StreamWriter sw = new StreamWriter(fileInfo.FullName, false, utf8))
                {
                    sw.Write(data);
                }
            }
        }

       // [Test]
        public void ExtractSeodDataTest()
        {
            StringBuilder sb = new StringBuilder();
            OracleConnection conn = new OracleConnection("Data Source=AIS3_KOE;User Id=NDS2_MRR_USER;Password=NDS2_MRR_USER;");
            using (conn)
            {
                conn.Open();
                OracleCommand cmd = new OracleCommand("select * from I$cam.V$nds2_Sent");
                cmd.Connection = conn;


                using (OracleDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        sb.AppendFormat("inser into T123(");
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            sb.AppendFormat(",'{0}'", reader[i].ToString());
                        }
                        sb.AppendFormat(");");
                        sb.AppendLine();
                    }

                }
            }

            using (StreamWriter sw = new StreamWriter("exp.sql"))
            {
                sw.Write(sb.ToString());
            };
        }

   //     [Test]
        public void ValidateSeod011fromDb()
        {
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.ValidationType = ValidationType.Schema;
            settings.ValidationFlags |= XmlSchemaValidationFlags.ProcessInlineSchema;
            settings.ValidationFlags |= XmlSchemaValidationFlags.ProcessSchemaLocation;
            settings.ValidationFlags |= XmlSchemaValidationFlags.ReportValidationWarnings;
            settings.ValidationEventHandler += new ValidationEventHandler(ValidationCallBack);

            //DirectoryInfo di = new DirectoryInfo(BASE_PATH);
            OracleConnection conn = new OracleConnection("Data Source=AIS3_KOE;User Id=NDS2_MRR_USER;Password=NDS2_MRR_USER;");
            using (conn)
            {
                conn.Open();
                OracleCommand cmd = new OracleCommand("select * from I$CAM.V$nds2_Sent");
                cmd.Connection = conn;
                using (OracleDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var TypeID = reader["reg_number"].ToString();
                        var xmlData = reader["xml_data"].ToString();
                        using (StreamWriter w = new StreamWriter(string.Format("{0}.xml", TypeID)))
                        {
                            w.Write(xmlData);
                        }

                    }

                }
            }
        }

        class ColInfo
        {
            public string colName { get; set; }
            public string colType { get; set; }
            public override string ToString()
            {
                return colName + "-" + colType;
            }
        }

        [Test]
        public void ExtractTableData()
        {
            string owner = "I$CAM";
            string tableName = "V$NDS2_SENT";

            List<ColInfo> ci = new List<ColInfo>();


            using (OracleConnection conn = new OracleConnection("Data Source=AIS3_KOE;User Id=NDS2_MRR_USER;Password=NDS2_MRR_USER;"))
            {
                conn.Open();
                string cmd1 = string.Format("select * from all_tab_cols where owner ='{0}' and table_name = '{1}' order by internal_column_id", owner, tableName);
                using (OracleCommand cmd = new OracleCommand(cmd1, conn))
                {
                    using(OracleDataReader r =  cmd.ExecuteReader())
                    {
                        while(r.Read())
                        {
                            ci.Add(new ColInfo(){colName = r["Column_name"].ToString(), colType = r["data_type"].ToString()});
                        }
                    }
                }

                string cmd2 = string.Format("select * from {0}.{1}", owner, tableName);
                using(OracleCommand cmd = new OracleCommand(cmd2, conn))
                {
                    using (OracleDataReader r = cmd.ExecuteReader())
                    {

                        while (r.Read())
                        {
                            StringBuilder sb = new StringBuilder();
                            sb.AppendFormat("insert into {0}.{1} values(", owner, tableName);
                            foreach (var colInfo in ci)
                            {
                                switch (colInfo.colType)
                                {
                                    case "NUMBER":
                                        sb.AppendFormat(",{0}", r[colInfo.colName].ToString());
                                        break;
                                    default:
                                        sb.AppendFormat(",'{0}'", r[colInfo.colName].ToString());
                                        break;
                                }
                            }
                            sb.AppendFormat(");");
                            sb.AppendLine();
                            Console.WriteLine(sb.ToString());
                        }

                        
                    }   
                }
               }

            //ci.ForEach(Console.WriteLine);
        }

        private static void ValidationCallBack(object sender, ValidationEventArgs args)
        {
            if (args.Severity == XmlSeverityType.Warning)
                Console.WriteLine("\tWarning: Matching schema not found.  No validation occurred." + args.Message);
            else
                Console.WriteLine("\tValidation error: " + args.Message);

        }
    }
}
