﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;
using NUnit.Framework;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Tests.Others
{
    [TestFixture]
    public class SEODHelper
    {
        private readonly string connectionString;

        public SEODHelper()
        {
            connectionString = ConfigurationManager.AppSettings["NDS2_DB"];
        }

        [Test]
        public void GenerateSeod01()
        {
            string fileName = "seod1Stream.txt";
            using (StreamWriter wr = new StreamWriter(fileName))
            {
                using (OracleConnection conn = new OracleConnection(connectionString))
                {
                    conn.Open();

                    using (OracleCommand cmd = new OracleCommand("select * From v$declaration d inner join v$askdekl fd on fd.ID = d.ASK_DECL_ID", conn))
                    {
                        using (OracleDataReader r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                wr.WriteLine(@"insert into I$CAM.NDS2_TEST_DATA values(SEQ_TMP_ID.nextval,'CAM_NDS2_01','АСК НДС-2: Идентификационные данные декларации по НДС (ЭОД-1)', sysdate, sysdate, '{1}',{0},'<?xml version=""1.0"" encoding=""windows-1251""?>
<Файл xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xsi:noNamespaceSchemaLocation=""TAX3EXCH_CAM_NDS2_01_01.xsd"" ТипИнф=""CAM_NDS2_01"" ИдФайл=""NO_NDS_7710_7710_7710698167771001001_20140917_00"" ВерсПрог=""АИС Налог [7710] 2.5.160.021"">
	<Документ КодНО=""{1}"" Период=""{2}"" ОтчетГод=""{3}"" НомКорр=""0"" РегНомДек=""{0}"" ФидДек=""0741786CE5944444AC6821DA7D8784E6"">
		<СвНП ФидНП=""5476627"">
			<НПЮЛ ИННЮЛ=""7710698167"" КПП=""771001001""/>
		</СвНП>
	</Документ>
</Файл>
');",
    "111"+r["ask_decl_id"].ToString(), /*id декларации*/
    r["soun_code"].ToString(), /*soun*/
    r["tax_period"].ToString(), /*tax period*/
    r["FISCAL_YEAR"].ToString());
                            }
                        }
                    }
                }                
            }
        }
    }
}
