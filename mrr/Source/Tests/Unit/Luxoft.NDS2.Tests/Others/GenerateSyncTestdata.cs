﻿using System;
using System.Collections.Generic;
using System.Text;
using Luxoft.NDS2.Common.Contracts.Helpers;
using NUnit.Framework;

namespace Luxoft.NDS2.Tests.Others
{

    [TestFixture]
    public class GenerateSyncTestData
    {
        public class Document
        {
            public long ID { get; set; }
            public long RequestId { get; set; }
            public DateTime Date { get; set; }
            public string SounCode { get; set; }
            public int Status { get; set; }
            public string DiscrepancyData { get; set; }

            public string ToSql()
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("insert into I$NDS2.TEST_CLAIM_DATA values(");
                sb.AppendFormat("{0}, ", ID);
                sb.AppendFormat("{0}, ", RequestId);
                sb.AppendFormat("to_date('{0}', 'dd.mm.yyyy'), ", Date.ToString("dd.MM.yyyy"));
                sb.AppendFormat("'{0}', ", SounCode);
                sb.AppendFormat("null, ");
                sb.AppendFormat("{0}, ", Status);
                sb.AppendFormat("'{0}', ", DiscrepancyData);
                sb.Append("null, null, null ");
                sb.Append(");");

                return sb.ToString();
            }
        }

        public class ClaimStatusSync
        {
            public long id { get; set; }
            public long sync_group_id { get; set; }
            public string sounCode { get; set; }
            public int status { get; set; }
            public string error { get; set; }

            public DateTime update_date { get; set; }

            public DateTime syncDate { get; set; }

            public string ToSql()
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("insert into I$NDS2.STUB_CAM_CLAIM_STATUS values(");
                sb.AppendFormat("{0}, ", id);
                sb.AppendFormat("{0}, ", sync_group_id);
                sb.AppendFormat("'{0}', ", sounCode);
                sb.AppendFormat("{0}, ", status);
                sb.AppendFormat("'{0}', ", error);
                sb.AppendFormat("to_date('{0}', 'dd.mm.yyyy') , ", update_date.ToString("dd.MM.yyyy"));
                sb.AppendFormat("to_date('{0}', 'dd.mm.yyyy') ", syncDate.ToString("dd.MM.yyyy"));

                sb.Append(");");

                return sb.ToString();
            }

        }

        [Test]
        public void Generate3()
        {
            Console.WriteLine("truncate table I$NDS2.STUB_CAM_KNP_DOC;");
            for(int i=1; i++<=100;)
            {
                Console.WriteLine("insert into I$NDS2.STUB_CAM_KNP_DOC values({0}, {1},{2},to_date('{3}', 'dd.mm.yyyy'),{4},to_date('{5}', 'dd.mm.yyyy'),to_date('{6}', 'dd.mm.yyyy'));", 
                    i, 
                    DataGenerator.GetRandomKPP(), 
                    DataGenerator.GetRandomOperationCode(),
                    DataGenerator.GetRandomDateString().ToString("dd.MM.yyyy"),
                    DataGenerator.GetRandomDecimalString(),
                    DataGenerator.GetRandomDateString().ToString("dd.MM.yyyy"),
                    DataGenerator.GetRandomDateString().ToString("dd.MM.yyyy"));
            }
            Console.WriteLine("commit;");
        }


        [Test]
        public void Generate2()
        {
            long claim_id = 1000;
            long sync_group_id = 12;
            List<string> Souns = new List<string>() { "1111", "2222", "3333", "5555", "7777" };
            Random r = new Random(20);

            Console.WriteLine("truncate table I$NDS2.STUB_CAM_CLAIM_STATUS;");
            for (int i = 1; i++ <= 10; )
            {
                for (int k = 1; k++ <= 10; )
                {
                    Console.WriteLine(new ClaimStatusSync()
                    {
                        id = claim_id++,
                        sync_group_id = sync_group_id,
                        sounCode = Souns[r.Next(0,3)],
                        status = 5,
                        error = string.Empty,
                        syncDate = DataGenerator.GetRandomDateString(),
                        update_date = DataGenerator.GetRandomDateString()
                    }.ToSql());
                }
                sync_group_id++;
            }

            Console.WriteLine("commit;");
        }

        [Test]
        public void Generate()
        {
            long claimId = 1;
            long requestId = 1;

            List<string> Souns = new List<string>() { "1111", "2222", "3333", "5555", "7777" };
            Random r = new Random(20);
            Console.WriteLine("truncate table I$NDS2.TEST_CLAIM_DATA;");

            for (int i = 1; i++ <= 10; )
            {
                var soun = Souns[r.Next(0, 3)];


                for (int j = 1; j++ <= 10; )
                {
                    Console.WriteLine(new Document()
                    {
                        ID = claimId,
                        RequestId = requestId,
                        Date = DataGenerator.GetRandomDateString(),
                        SounCode = soun,
                        DiscrepancyData = "<NULL />",
                        Status = 1
                    }.ToSql());

                    claimId++;
                }

                requestId++;
            }

            Console.WriteLine("commit;");
        }
    }
}
