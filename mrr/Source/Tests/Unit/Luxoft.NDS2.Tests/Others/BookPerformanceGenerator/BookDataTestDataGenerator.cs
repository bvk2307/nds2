﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Tests.Helpers;
using Luxoft.NDS2.Tests.Others.Entity;
using NUnit.Framework;

namespace Luxoft.NDS2.Tests.Others.TestDataGenerator
{
    [TestFixture]
    public class BookDataTestDataGenerator
    {
        [Test]
        public void TestGen()
        {
            TPInfo seller = new TPInfo();
            TPInfo buyer = new TPInfo();
            TPInfo broker = new TPInfo();

            Random rnd = RandomProvider.GetThreadRandom();
            StreamWriter ulwr = new StreamWriter("egrn_ul.sql");
            CreateSqlLoaderControlInfoEgrnUl(ulwr);
            StreamWriter ipwr = new StreamWriter("egrn_ip.sql");
            CreateSqlLoaderControlInfoEgrnIp(ipwr);
            StreamWriter invwr = new StreamWriter("invoices.sql");
            CreateSqlLoaderControlInfoInvoice(invwr);
            StreamWriter reqwr = new StreamWriter("requests.sql");
            CreateSqlLoaderControlInfoRequest(reqwr);
            StreamWriter sellerInvWriter = new StreamWriter("invSeller.sql");
            CreateSqlLoaderControlInfoInvoiceSeller(sellerInvWriter);
            StreamWriter buyerInvWriter = new StreamWriter("invBuyer.sql");
            CreateSqlLoaderControlInfoInvoiceBuyer(buyerInvWriter);


            int lineTpCounts = 0;

            long requests = 10;
            long requestId = 1;

            int linesPerRequest = 0;
            int linesMin = 1;
            int linesMax = 100;
            long tpIdentity = 1;
             

            Action<TPInfo> tpWriteAction = (tp) =>
            {
                if (tp.IsUl)
                {
                    ulwr.WriteLine(tp.ToSqlLoader(tpIdentity++));
                }
                else
                {
                    ipwr.WriteLine(tp.ToSqlLoader(tpIdentity++));
                }
            };

            for (int ch = 12; ch >= 8; ch--)
            {
                for (int request = 0; request <= requests; request++)
                {
                    linesPerRequest = rnd.Next(linesMin, linesMax);
                    WriteRequest(requestId++, ch, reqwr);

                    while (--linesPerRequest >= 0)
                    {
                        var inv = GetInvoiceLine(requestId, rnd, ch, DateTime.Now, seller, buyer, broker);

                        lineTpCounts = rnd.Next(1, 10);

                        SetRandomTP(broker, linesPerRequest);
                        tpWriteAction(broker);
                        inv.BROKER_INN = broker.Inn;
                        inv.BROKER_KPP = broker.Kpp;

                        switch (ch)
                        {
                            case 8: 
                                while (--lineTpCounts >= 0)
                                {
                                    SetRandomTP(seller, lineTpCounts);
                                    WriteInvoiceTaxPayerData(requestId, inv.INVOICE_NUM, seller, sellerInvWriter);
                                    tpWriteAction(seller);
                                }
                                break;
                            case 9:
                                while (--lineTpCounts >= 0)
                                {
                                    SetRandomTP(buyer, lineTpCounts);
                                    WriteInvoiceTaxPayerData(requestId, inv.INVOICE_NUM, buyer, buyerInvWriter);
                                    tpWriteAction(buyer);
                                }
                                break;
                            case 10:
                            case 11:
                            case 12:
                                while (--lineTpCounts >= 0)
                                {
                                    SetRandomTP(seller, lineTpCounts);
                                    WriteInvoiceTaxPayerData(requestId, inv.INVOICE_NUM, seller, sellerInvWriter);
                                    tpWriteAction(seller);

                                    SetRandomTP(buyer, lineTpCounts+1);
                                    WriteInvoiceTaxPayerData(requestId, inv.INVOICE_NUM, buyer, buyerInvWriter);
                                    tpWriteAction(buyer);
                                }
                                break;
                        }

                        invwr.WriteLine(inv.ToSqlLoader(requestId));
                    }

                }

                linesMin = linesMax;
                linesMax += 10;
            }
               

            ulwr.Close();
            ipwr.Close();
            invwr.Close();
            reqwr.Close();
            sellerInvWriter.Close();
            buyerInvWriter.Close();
        }

        private void SetRandomTP(TPInfo tp, long stuff)
        {
            if (stuff % 2 == 0)
            {
                tp.Inn = DataGenerator.GetRandomINN_10digit();
                tp.Kpp = DataGenerator.GetRandomKPP();
            }
            else
            {
                tp.Inn = DataGenerator.GetRandomINN();
                tp.Kpp = null;
            }
            
            tp.Name = DataGenerator.GetRandomTaxPayerName();
            tp.SonoCode = tp.Inn.Substring(0, 4);
        }

        private void WriteInvoiceTaxPayerData(long rid, string invNo, TPInfo info, StreamWriter writer)
        {
            writer.WriteLine("{0}|{1}|{2}|{3}|", rid, invNo, info.Inn, info.Kpp);
        }

        private void WriteRequest(long reqId, int chapter, StreamWriter writer)
        {
            writer.WriteLine("{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}|{9}", 
                reqId, 
                DataGenerator.GetRandomINN(),
                0,
                chapter,
                "21",
                "2015",
                2,
                "01.01.2015",
                "01.01.2015",
                1
                );
        }

        private Invoice GetInvoiceLine(long id, Random r, int chapter, DateTime? invoceDate, TPInfo seller, TPInfo buyer, TPInfo broker)
        {
            return new Invoice
            {
                CHAPTER = chapter,
                ORDINAL_NUMBER = id + 1,
                OKV_CODE = "810",
                CREATE_DATE = DataGenerator.GetRandomDateString(),
                RECEIVE_DATE = DataGenerator.GetRandomDateString(),
                OPERATION_CODE = DataGenerator.GetRandomOperationCode(),
                INVOICE_NUM = DataGenerator.GetRandomInvoiceNumber(),
                INVOICE_DATE = invoceDate,
                CHANGE_NUM = DataGenerator.GetRandomInvoiceChangeNum(),
                CHANGE_DATE = DataGenerator.GetRandomDateString(),
                CORRECTION_NUM = DataGenerator.GetRandomInvoiceChangeNum(),
                CORRECTION_DATE = DataGenerator.GetRandomDateString(),
                CHANGE_CORRECTION_NUM = DataGenerator.GetRandomInvoiceChangeNum(),
                CHANGE_CORRECTION_DATE = DataGenerator.GetRandomDateString(),
                RECEIPT_DOC_NUM = DataGenerator.GetRandomInvoiceNumber(),
                RECEIPT_DOC_DATE = DataGenerator.GetRandomDateString(),
                BUY_ACCEPT_DATE = new NDS2.Common.Contracts.DTO.Business.Book.DocDates(DataGenerator.GetRandomDateString()),
                BUYER_INN = buyer.Inn,
                BUYER_KPP = buyer.Kpp,
                BUYER_NAME = DataGenerator.GetRandomTaxPayerName(),
                SELLER_INN = seller.Inn,
                SELLER_KPP = seller.Kpp,
                SELLER_NAME = DataGenerator.GetRandomTaxPayerName(),
                SELLER_INVOICE_NUM = DataGenerator.GetRandomInvoiceNumber(),
                SELLER_INVOICE_DATE = DataGenerator.GetRandomDateString(),
                BROKER_INN = broker.Inn,
                BROKER_KPP = broker.Kpp,
                BROKER_NAME = DataGenerator.GetRandomTaxPayerName(),
                DEAL_KIND_CODE = r.Next(1, 4),
                CUSTOMS_DECLARATION_NUM = DataGenerator.GetRandomTaxPayerName(),
                PRICE_BUY_AMOUNT = DataGenerator.GetRandomDecimal(),
                PRICE_BUY_NDS_AMOUNT = DataGenerator.GetRandomDecimal(),
                PRICE_SELL = DataGenerator.GetRandomNullableDecimal(),
                PRICE_SELL_IN_CURR = DataGenerator.GetRandomDecimal(),
                PRICE_SELL_18 = DataGenerator.GetRandomDecimal(),
                PRICE_SELL_10 = DataGenerator.GetRandomDecimal(),
                PRICE_SELL_0 = DataGenerator.GetRandomDecimal(),
                PRICE_NDS_18 = DataGenerator.GetRandomDecimal(),
                PRICE_NDS_10 = DataGenerator.GetRandomDecimal(),
                PRICE_TAX_FREE = DataGenerator.GetRandomDecimal(),
                PRICE_TOTAL = DataGenerator.GetRandomDecimal(),
                PRICE_NDS_TOTAL = DataGenerator.GetRandomDecimal(),
                DIFF_CORRECT_DECREASE = DataGenerator.GetRandomDecimal(),
                DIFF_CORRECT_INCREASE = DataGenerator.GetRandomDecimal(),
                DIFF_CORRECT_NDS_DECREASE = DataGenerator.GetRandomDecimal(),
                DIFF_CORRECT_NDS_INCREASE = DataGenerator.GetRandomDecimal(),
                PRICE_NDS_BUYER = DataGenerator.GetRandomDecimal(),
                LOGICAL_ERRORS = "",
                ROW_KEY = DataGenerator.GetRandomKPP(),
                ACTUAL_ROW_KEY = null
            };
        }

        private void CreateSqlLoaderControlInfoEgrnUl(StreamWriter wr)
        {
            wr.WriteLine("LOAD DATA");
            wr.WriteLine("INFILE *");
            wr.WriteLine("INTO TABLE V_EGRN_UL");
            wr.WriteLine("FIELDS TERMINATED BY \"|\"");
            wr.WriteLine("(");
            wr.WriteLine("ID");
            wr.WriteLine(",INN");
            wr.WriteLine(",KPP");
            wr.WriteLine(",NAME_FULL");
            wr.WriteLine(",CODE_NO");
            wr.WriteLine(",ADR");
            wr.WriteLine(",DATE_REG DATE \"dd.mm.yyyy\"");
            wr.WriteLine(",DATE_ON DATE \"dd.mm.yyyy\"");
            wr.WriteLine(",DATE_OFF DATE \"dd.mm.yyyy\"");
            wr.WriteLine(",UST_CAPITAL");
            wr.WriteLine(")");
            wr.WriteLine("BEGINDATA");
        }

        private void CreateSqlLoaderControlInfoEgrnIp(StreamWriter wr)
        {
            wr.WriteLine("LOAD DATA");
            wr.WriteLine("INFILE *");
            wr.WriteLine("INTO TABLE V_EGRN_IP");
            wr.WriteLine("FIELDS TERMINATED BY \"|\"");
            wr.WriteLine("(");
            wr.WriteLine("id,");
            wr.WriteLine("innfl,");
            wr.WriteLine("last_name,");
            wr.WriteLine("first_name,");
            wr.WriteLine("patronymic,");
            wr.WriteLine("code_no, ");
            wr.WriteLine("adr,");
            wr.WriteLine("date_on DATE \"dd.mm.yyyy\",");
            wr.WriteLine("date_off DATE \"dd.mm.yyyy\"");
            wr.WriteLine(")");
            wr.WriteLine("BEGINDATA");
        }

        private void CreateSqlLoaderControlInfoInvoice(StreamWriter wr)
        {
            wr.WriteLine("LOAD DATA");
            wr.WriteLine("INFILE *");
            wr.WriteLine("INTO TABLE SOV_INVOICE");
            wr.WriteLine("FIELDS TERMINATED BY \"|\"");
            wr.WriteLine("(");
            wr.WriteLine("REQUEST_ID");
            wr.WriteLine(",DECLARATION_VERSION_ID");
            wr.WriteLine(",CHAPTER");
            wr.WriteLine(",ORDINAL_NUMBER");
            wr.WriteLine(",OKV_CODE");
            wr.WriteLine(",CREATE_DATE DATE \"dd.mm.yyyy\"");
            wr.WriteLine(",RECEIVE_DATE DATE \"dd.mm.yyyy\"");
            wr.WriteLine(",OPERATION_CODE");
            wr.WriteLine(",INVOICE_NUM");
            wr.WriteLine(",INVOICE_DATE DATE \"dd.mm.yyyy\"");
            wr.WriteLine(",CHANGE_NUM");
            wr.WriteLine(",CHANGE_DATE DATE \"dd.mm.yyyy\"");
            wr.WriteLine(",CORRECTION_NUM");
            wr.WriteLine(",CORRECTION_DATE DATE \"dd.mm.yyyy\"");
            wr.WriteLine(",CHANGE_CORRECTION_NUM");
            wr.WriteLine(",CHANGE_CORRECTION_DATE DATE \"dd.mm.yyyy\"");
            wr.WriteLine(",RECEIPT_DOC_NUM");
            wr.WriteLine(",RECEIPT_DOC_DATE");
            wr.WriteLine(",BUY_ACCEPT_DATE");
            wr.WriteLine(",BUYER_INN");
            wr.WriteLine(",BUYER_KPP");
            wr.WriteLine(",SELLER_INN");
            wr.WriteLine(",SELLER_KPP");
            wr.WriteLine(",SELLER_INVOICE_NUM");
            wr.WriteLine(",SELLER_INVOICE_DATE DATE \"dd.mm.yyyy\"");
            wr.WriteLine(",BROKER_INN");
            wr.WriteLine(",BROKER_KPP");
            wr.WriteLine(",DEAL_KIND_CODE");
            wr.WriteLine(",CUSTOMS_DECLARATION_NUM");
            wr.WriteLine(",PRICE_BUY_AMOUNT");
            wr.WriteLine(",PRICE_BUY_NDS_AMOUNT");
            wr.WriteLine(",PRICE_SELL");
            wr.WriteLine(",PRICE_SELL_IN_CURR");
            wr.WriteLine(",PRICE_SELL_18");
            wr.WriteLine(",PRICE_SELL_10");
            wr.WriteLine(",PRICE_SELL_0");
            wr.WriteLine(",PRICE_NDS_18");
            wr.WriteLine(",PRICE_NDS_10");
            wr.WriteLine(",PRICE_TAX_FREE");
            wr.WriteLine(",PRICE_TOTAL");
            wr.WriteLine(",PRICE_NDS_TOTAL");
            wr.WriteLine(",DIFF_CORRECT_DECREASE");
            wr.WriteLine(",DIFF_CORRECT_INCREASE");
            wr.WriteLine(",DIFF_CORRECT_NDS_DECREASE");
            wr.WriteLine(",DIFF_CORRECT_NDS_INCREASE");
            wr.WriteLine(",PRICE_NDS_BUYER");
            wr.WriteLine(",ROW_KEY");
            wr.WriteLine(",ACTUAL_ROW_KEY");
            wr.WriteLine(",COMPARE_ROW_KEY");
            wr.WriteLine(",COMPARE_ALGO_ID");
            wr.WriteLine(",FORMAT_ERRORS");
            wr.WriteLine(",LOGICAL_ERRORS");
            wr.WriteLine(",SELLER_AGENCY_INFO_INN");
            wr.WriteLine(",SELLER_AGENCY_INFO_KPP");
            wr.WriteLine(",SELLER_AGENCY_INFO_NAME");
            wr.WriteLine(",SELLER_AGENCY_INFO_NUM");
            wr.WriteLine(",SELLER_AGENCY_INFO_DATE DATE \"dd.mm.yyyy\"");
            wr.WriteLine(",IS_DOP_LIST");
            wr.WriteLine(",IS_IMPORT");
            wr.WriteLine(",CLARIFICATION_KEY");
            wr.WriteLine(")");
            wr.WriteLine("BEGINDATA");
        }

        private void CreateSqlLoaderControlInfoRequest(StreamWriter wr)
        {
            wr.WriteLine("LOAD DATA");
            wr.WriteLine("INFILE *");
            wr.WriteLine("INTO TABLE BOOK_DATA_REQUEST");
            wr.WriteLine("FIELDS TERMINATED BY \"|\"");
            wr.WriteLine("(");
            wr.WriteLine("ID");
            wr.WriteLine(",INN");
            wr.WriteLine(",CORRECTIONNUMBER");
            wr.WriteLine(",PARTITIONNUMBER");
            wr.WriteLine(",PERIOD");
            wr.WriteLine(",YEAR");
            wr.WriteLine(",STATUS");
            wr.WriteLine(",REQUESTDATE DATE \"dd.mm.yyyy\"");
            wr.WriteLine(",PROCESS_DATE DATE \"dd.mm.yyyy\"");
            wr.WriteLine(",PRIORITY");
            wr.WriteLine(")");
            wr.WriteLine("BEGINDATA");
        }

        private void CreateSqlLoaderControlInfoInvoiceSeller(StreamWriter wr)
        {
            wr.WriteLine("LOAD DATA");
            wr.WriteLine("INFILE *");
            wr.WriteLine("INTO TABLE SOV_INVOICE_SELLER");
            wr.WriteLine("FIELDS TERMINATED BY \"|\"");
            wr.WriteLine("(");
            wr.WriteLine("REQUEST_ID");
            wr.WriteLine(",INVOICE_NUM");
            wr.WriteLine(",SELLER_INN");
            wr.WriteLine(",SELLER_KPP");
            wr.WriteLine(")");
            wr.WriteLine("BEGINDATA");
        }

        private void CreateSqlLoaderControlInfoInvoiceBuyer(StreamWriter wr)
        {
            wr.WriteLine("LOAD DATA");
            wr.WriteLine("INFILE *");
            wr.WriteLine("INTO TABLE SOV_INVOICE_BUYER");
            wr.WriteLine("FIELDS TERMINATED BY \"|\"");
            wr.WriteLine("(");
            wr.WriteLine("REQUEST_ID");
            wr.WriteLine(",INVOICE_NUM");
            wr.WriteLine(",BUYER_INN");
            wr.WriteLine(",BUYER_KPP");
            wr.WriteLine(")");
            wr.WriteLine("BEGINDATA");
        }
    }


}
