﻿//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using System.Xml;
//using GotDotNet.XPath;
//using Luxoft.NDS2.Common.Contracts.DTO.Business.SellBook;
//using NUnit.Framework;
//
//namespace Luxoft.NDS2.Tests.Others
//{
//    [TestFixture]
//    public class BookIndexier
//    {
//        [Test]
//        public void LinqForDummies()
//        {
//          List<string> l1 = new List<string>(){"one", "two", "four"}; 
//          List<string> l2 = new List<string>(){"one", "two", "three"};
//
//            foreach (string i in l2.Except(l1))
//            {
//                Console.WriteLine(i);
//            } 
//        }
//
//        [Test]
//        public void CreateTestData()
//        {
//
//            IEnumerable<string> test = new List<string>();
//            var result = test.Where(s => s.Contains("1")).Select(t => t[1]);
//
//            Assert.IsNotNull(result);
//            Assert.IsEmpty(result);
//
//            return;
//            int idx = 0;
//            string file = @"D:\01_Distr\test.txt";
//            using (StreamWriter sw = new StreamWriter(@"D:\01_Distr\test.out"))
//            {
//                using (StreamReader reader = new StreamReader(file))
//                {
//                    string line = reader.ReadLine();
//                    while (line != null)
//                    {
//                        sw.Write("insert into TestData values({0}, '{1}', '08', '2014') \n", idx, line);
//                        line = reader.ReadLine();
//                        idx++;
//                    }
//                }
//            }
//        }
//
//        [Test]
//        public void IndexIt()
//        {
//            IndexPrivate(BookType.Sell, @"C:\Projects\Luxoft\FNS\DEV\NDS2\TestData\RZDBooks");
//        }
//
//
//
//        private void IndexPrivate(BookType type, string storagePath)
//        {
//            DirectoryInfo di = new DirectoryInfo(storagePath);
//            if (!di.Exists) return;
//
//            var idxFilePath = Path.Combine(storagePath, @"\index.txt");
//
//            if(File.Exists(idxFilePath))
//            {
//                File.Delete(idxFilePath);
//            }
//
//            using (StreamWriter writer = new StreamWriter(idxFilePath))
//            {
//                XPathCollection paths = new XPathCollection();
//
//                paths.Add("//Документ/СвПродав/СведЮЛ");
//                paths.Add("//Документ/СвКнПрод");
//
//                foreach (var fileInfo in di.GetFiles("*.xml", SearchOption.TopDirectoryOnly))
//                {
//                    using (XPathReader reader = new XPathReader(fileInfo.FullName, paths))
//                    {
//                        //UL
//                        if (reader.ReadUntilMatch())
//                        {
//                            reader.MoveToAttribute("ИННЮЛ");
//                            writer.Write(reader.Value);
//                            writer.Write("-");
//                            reader.MoveToAttribute("КПП");
//                            writer.Write(reader.Value);
//                            //Book info
//                            if(reader.ReadUntilMatch())
//                            {
//                                writer.Write("-");
//                                reader.MoveToAttribute("Период");
//                                writer.Write(reader.Value);
//                                writer.Write("-");
//                                reader.MoveToAttribute("ОтчетГод");
//                                writer.Write(reader.Value);
//                            }
//                            writer.Write(';');
//                            writer.Write(fileInfo.Name);
//                            writer.Write('\n');
//                        }                        
//                    }
//                }                
//            }
//        }
//    }
//}
