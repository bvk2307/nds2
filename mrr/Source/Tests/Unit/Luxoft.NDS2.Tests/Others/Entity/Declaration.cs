﻿using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;

namespace Luxoft.NDS2.Tests.Others.Entity
{
    public class ASKDeclarations : DeclarationSummary
    {
        public string ToSql()
        {
            StringBuilder sb = new StringBuilder();

            //sb.AppendFormat("'{0}'", this.REGION.EntryId);

            return sb.ToString();
        }
    }

    public class ASKZip
    {
        public long ZipId { get; set; }

        public string ToSql()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("insert into \"NDS2_MC\".\"ASKZIPФайл\"");
            sb.Append("(\"ДатаДок\", \"Период\", \"ОтчетГод\", \"КодНО\", \"НомКорр\", \"ОКВЭД\", \"НаимОрг\",\"ИНННП\", \"КППНП\")");
            sb.Append(" values (");
            sb.AppendFormat("{0},", this.ZipId);
            sb.Append(");");
            //sb.AppendFormat("'{0}'", this.REGION.EntryId);

            return sb.ToString();
        }
    }
}

