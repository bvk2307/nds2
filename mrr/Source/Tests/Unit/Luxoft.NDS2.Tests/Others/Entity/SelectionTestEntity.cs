﻿using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;

namespace Luxoft.NDS2.Tests.Others.Entity
{
    public sealed class SelectionTestEntity : Selection 
    {
        public int TestCode { get; set; }

        private const string SQL_INSERT_SELECTION = @"insert into NDS2_MRR_USER.SELECTION values({0}/*ID*/,'{1}'/*NAME*/,{2}/*CREATION_DATE*/,{3}/*MODIFICATION_DATE*/,{4}/*STATUS_DATE*/,'{5}'/*ANALYTIC*/,'{6}'/*ANALYTIC_SID*/,'{7}'/*CHIEF*/,'{8}'/*CHIEF_SID*/,'{9}'/*SENDER*/,{10}/*STATUS*/,{11}/*TYPE*/,{12}/*REQUEST_ID*/,'{13}'/*FILTER*/,{14}/*INVOICE_REQUEST_ID*/,{15}/*LT_SONO_CODE*/);";

        private const string SQL_INSERT_SELECTION_REQUEST = @"insert into NDS2_MRR_USER.SOV_SELECTION_REQUEST values({0}/*REQUEST_ID*/,{1}/*STATUS_ID*/,'{2}'/*REQUEST_BODY*/,{3}/*REQUEST_DATE*/,{4}/*PROCESS_DATE*/);";

        public string ToSql()
        {
            var sysdateString = "sysdate";
            var sb = new StringBuilder();

            sb.AppendLine(string.Format(SQL_INSERT_SELECTION_REQUEST, this.Id, 2, "<xml1>test</xml1>", sysdateString, sysdateString));

            sb.AppendLine(string.Format(SQL_INSERT_SELECTION,
                this.Id,
                this.Name,
                sysdateString,
                sysdateString,
                sysdateString,
                this.Analytic,
                this.AnalyticSid,
                this.Manager,
                this.ManagerSid,
                "test",
                this.Status,
                this.TypeCode,
                //this.Filter.Serialize(),
                "",
                this.InvoiceRequestId,
                this.TestCode
                ));

            return sb.ToString();
        }

        private const string SQL_INSERT_SELECTION_DECLARATION = @"insert into NDS2_MRR_USER.SELECTION_DECLARATION values({0}/*SELECTION_ID*/,{1}/*DECLARATION_ID*/,{2}/*IS_IN_PROCESS*/);";

        public string SqlAttachDeclaration(DeclarationSummary d)
        {
            return string.Format(SQL_INSERT_SELECTION_DECLARATION, this.Id, d.ID, 1);
        }



        private const string SQL_INSERT_SELECTION_DISCREPANCY = @"insert into NDS2_MRR_USER.SELECTION_DISCREPANCY values({0}/*SELECTION_ID*/,{1}/*DISCREPANCY_ID*/,{2}/*IS_IN_PROCESS*/);";

        public string SqlAttachDiscrepancy(Mismatch d)
        {
            return string.Format(SQL_INSERT_SELECTION_DISCREPANCY, this.Id, d.Id, 1);
        }
    }
}