﻿using System.Globalization;
using System.Text;
using Luxoft.NDS2.Tests.Others.Common;

namespace Luxoft.NDS2.Tests.Others.Entity
{
    public class Mismatch
    {
        public long Id { get; set; }
        public int CreateDate { get; set; }
        public int Type { get; set; }
        public int CompareKind { get; set; }
        public int RuleGroup { get; set; }
        public decimal? DealAmount { get; set; }
        public decimal? MismatchesAmount { get; set; }
        public decimal? PVPAmount { get; set; }
        public decimal? Course { get; set; }
        public decimal? CourseCost { get; set; }
        public int SurCode { get; set; }
        public int InvoiceChapter { get; set; }
        public string InvoiceId { get; set; }
        public int InvoiceContractorChapter { get; set; }
        public string InvoiceContractorId { get; set; }
        public long DeclarationId { get; set; }
        public long DeclarationContractorId { get; set; }
        public int Status { get; set; }

        public string ToSql()
        {
            CultureInfo ci = new CultureInfo("en-GB");
            StringBuilder sb = new StringBuilder();
            sb.Append("insert into FIR.DISCREPANCY_RAW values(");
            sb.AppendFormat("{0}, ", this.Id);
            sb.AppendFormat("{0}, ", this.Id.ToString(ci));
            sb.Append(" sysdate, ");
            sb.AppendFormat("{0}, ", this.Type);
            sb.AppendFormat("'{0}', ", this.CompareKind);
            sb.AppendFormat("{0}, ", this.RuleGroup);
            sb.AppendFormat("{0}, ", this.DealAmount.ToSqlString());
            sb.AppendFormat("{0}, ", this.MismatchesAmount.ToSqlString());
            sb.AppendFormat("{0}, ", this.PVPAmount.ToSqlString());
            sb.AppendFormat("{0}, ", this.Course.ToSqlString());
            sb.AppendFormat("{0}, ", this.CourseCost.ToSqlString());
            sb.AppendFormat("{0}, ", this.SurCode);
            sb.AppendFormat("{0}, ", this.InvoiceChapter);
            sb.AppendFormat("'{0}', ", this.InvoiceId);
            sb.AppendFormat("{0}, ", this.DeclarationId);
            sb.AppendFormat("{0}, ", this.InvoiceContractorChapter);
            sb.AppendFormat("'{0}', ", this.InvoiceContractorId);
            sb.AppendFormat("'{0}', ", this.DeclarationContractorId);
            sb.AppendFormat("'{0}',", this.Status);
            sb.Append(");");

            return sb.ToString();
        }

        public string ToMrrSql()
        {
            CultureInfo ci = new CultureInfo("en-GB");
            StringBuilder sb = new StringBuilder();
            sb.Append("insert into NDS2_MRR_USER.SOV_DISCREPANCY values(");
            sb.AppendFormat("{0}, ", this.Id);
            sb.AppendFormat("{0}, ", this.Id.ToString(ci));
            sb.Append(" sysdate, ");
            sb.AppendFormat("{0}, ", this.Type);
            sb.AppendFormat("'{0}', ", this.CompareKind);
            sb.AppendFormat("{0}, ", this.RuleGroup);
            sb.AppendFormat("{0}, ", this.DealAmount.ToSqlString());
            sb.AppendFormat("{0}, ", this.MismatchesAmount.ToSqlString());
            sb.AppendFormat("{0}, ", this.PVPAmount.ToSqlString());
            sb.AppendFormat("{0}, ", this.Course.ToSqlString());
            sb.AppendFormat("{0}, ", this.CourseCost.ToSqlString());
            sb.AppendFormat("{0}, ", this.SurCode);
            sb.AppendFormat("{0}, ", this.InvoiceChapter);
            sb.AppendFormat("'{0}', ", this.InvoiceId);
            sb.AppendFormat("{0}, ", this.DeclarationId);
            sb.AppendFormat("{0}, ", this.InvoiceContractorChapter);
            sb.AppendFormat("'{0}', ", this.InvoiceContractorId);
            sb.AppendFormat("'{0}', ", this.DeclarationContractorId);
            sb.AppendFormat("'{0}',", this.Status);
            sb.AppendFormat("{0},", 1);
            sb.AppendFormat("'{0}',", "1");
            sb.Append(");");

            return sb.ToString();
        }
    }
}