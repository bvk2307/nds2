﻿namespace Luxoft.NDS2.Tests.Others.Entity
{
    internal sealed class ControlRatio
    {
        private const string SQL_FORMAT = @"insert into NDS2_MC.""ASKКонтрСоотн"" values ({0}, {1}, '{2}', {3}, {4}, {5}, sysdate);";
        //private const string SQL_FORMAT = @"insert into NDS2_MC.""ASKКонтрСоотн"" values ({0}, '{1}', {2}, {3}, {4});";

        public long Id { get; set; }

        public long AskDeklId { get; set; }

        public string Type_Code { get; set; }

        public string Disparity_Right { get; set; }

        public string Disparity_Left { get; set; }

        public bool Complete { get; set; }

        public string ToSql()
        {
            return string.Format(SQL_FORMAT,
                Id,
                AskDeklId,
                Type_Code,
                Disparity_Right,
                Disparity_Left,
                Complete ? 1 : 0);
        }
    }
}