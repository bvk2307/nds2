﻿using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Tests.Others.TestDataGenerator;

namespace Luxoft.NDS2.Tests.Others.Entity
{
    public class IdPair
    {
        public string INN { get; set; }
        public string KPP { get; set; }

        public IdPair()
        {
            INN = DataGenerator.GetRandomINN_10digit();
            KPP = DataGenerator.GetRandomKPP();
        }

        public IdPair(string inn, string kpp)
        {
            INN = inn;
            KPP = kpp;
        }

        public IdPair(IdPair src)
        {
            INN = src.INN;
            KPP = src.KPP;
        }
    }


    public class InvoiceINN
    {
        public string BUYER_INN { get; set; }
        public string SELLER_INN { get; set; }
        public string BROKER_INN { get; set; }

        public string BUYER_KPP { get; set; }
        public string SELLER_KPP { get; set; }
        public string BROKER_KPP { get; set; }

        public InvoiceINN ()
        {}

        public InvoiceINN(IdPair buyer, IdPair seller, IdPair broker)
        {
            BUYER_INN = buyer.INN;
            BUYER_KPP = buyer.KPP;
            SELLER_INN = seller.INN;
            SELLER_KPP = seller.KPP;
            BROKER_INN = broker.INN;
            BROKER_KPP = broker.KPP;
        }

        public InvoiceINN(Declaration d, TPInfo tp, bool buy = true)
        {
            if (buy)
            {
                BUYER_INN = d.INN;
                BUYER_KPP = d.KPP;
                SELLER_INN = tp.Inn;
                SELLER_KPP = tp.Kpp;
            }
            else
            {
                BUYER_INN = tp.Inn;
                BUYER_KPP = tp.Kpp;
                SELLER_INN = d.INN;
                SELLER_KPP = d.KPP;
            }
        }

        public InvoiceINN(Declaration d, Declaration tp, bool buy = true)
        {
            if (buy)
            {
                BUYER_INN = d.INN;
                BUYER_KPP = d.KPP;
                SELLER_INN = tp.INN;
                SELLER_KPP = tp.KPP;
            }
            else
            {
                BUYER_INN = tp.INN;
                BUYER_KPP = tp.KPP;
                SELLER_INN = d.INN;
                SELLER_KPP = d.KPP;
            }
        }
    }
}
