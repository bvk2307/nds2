﻿using System.Globalization;
using System.Text;
using Luxoft.NDS2.Tests.Others.Common;
using System.Linq;

namespace Luxoft.NDS2.Tests.Others.Entity
{
    public class Invoice : Luxoft.NDS2.Common.Contracts.DTO.Business.Book.Invoice
    {
        public string ToSql()
        {
            CultureInfo ci = new CultureInfo("en-GB");

            StringBuilder sb = new StringBuilder();
            sb.Append("insert into FIR.INVOICE_RAW values(");
            sb.AppendFormat("{0}, ", this.DeclarationId);
            sb.AppendFormat("{0}, ", this.CHAPTER);
            sb.AppendFormat("{0}, ", this.ORDINAL_NUMBER);
            sb.AppendFormat("'{0}', ", this.OKV_CODE);
            sb.AppendFormat("to_date('{0}', 'dd.mm.yyyy'), ", this.CREATE_DATE.GetValueOrDefault().ToString("dd.MM.yyyy"));
            sb.AppendFormat("to_date('{0}', 'dd.mm.yyyy'), ", this.RECEIVE_DATE.GetValueOrDefault().ToString("dd.MM.yyyy"));
            sb.AppendFormat("'{0}', ", this.OPERATION_CODE);
            sb.AppendFormat("'{0}', ", this.INVOICE_NUM);
            sb.AppendFormat("to_date('{0}', 'dd.mm.yyyy'), ", this.INVOICE_DATE.GetValueOrDefault().ToString("dd.MM.yyyy"));
            sb.AppendFormat("{0}, ", this.CHANGE_NUM);
            sb.AppendFormat("to_date('{0}', 'dd.mm.yyyy'), ", this.CHANGE_DATE.GetValueOrDefault().ToString("dd.MM.yyyy"));
            sb.AppendFormat("'{0}', ", this.CORRECTION_NUM);
            sb.AppendFormat("to_date('{0}', 'dd.mm.yyyy'), ", this.CORRECTION_DATE.GetValueOrDefault().ToString("dd.MM.yyyy"));
            sb.AppendFormat("{0}, ", this.CHANGE_CORRECTION_NUM);
            sb.AppendFormat("to_date('{0}', 'dd.mm.yyyy'), ", this.CHANGE_CORRECTION_DATE.GetValueOrDefault().ToString("dd.MM.yyyy"));
            sb.AppendFormat("'{0}', ", this.RECEIPT_DOC_NUM);
            sb.AppendFormat("to_date('{0}', 'dd.mm.yyyy'), ", this.RECEIPT_DOC_DATE.GetValueOrDefault().ToString("dd.MM.yyyy"));
            sb.AppendFormat("to_date('{0}', 'dd.mm.yyyy'), ", this.BUY_ACCEPT_DATE.FirstDate().GetValueOrDefault().ToString("dd.MM.yyyy"));
            sb.AppendFormat("'{0}', ", this.BUYER_INN);
            sb.AppendFormat("'{0}', ", this.BUYER_KPP);
            sb.AppendFormat("'{0}', ", this.SELLER_INN);
            sb.AppendFormat("'{0}', ", this.SELLER_KPP);
            sb.AppendFormat("'{0}', ", this.SELLER_INVOICE_NUM);
            sb.AppendFormat("to_date('{0}', 'dd.mm.yyyy'), ", this.SELLER_INVOICE_DATE.GetValueOrDefault().ToString("dd.MM.yyyy"));
            sb.AppendFormat("'{0}', ", this.BROKER_INN);
            sb.AppendFormat("'{0}', ", this.BROKER_KPP);
            sb.AppendFormat("{0}, ", this.DEAL_KIND_CODE);
            sb.AppendFormat("'{0}', ", this.CUSTOMS_DECLARATION_NUM);
            sb.AppendFormat("{0}, ", this.PRICE_BUY_AMOUNT.ToSqlString());
            sb.AppendFormat("{0}, ", this.PRICE_BUY_NDS_AMOUNT.ToSqlString());
            sb.AppendFormat("{0}, ", this.PRICE_SELL.ToSqlString());
            sb.AppendFormat("{0}, ", this.PRICE_SELL_IN_CURR.ToSqlString());
            sb.AppendFormat("{0}, ", this.PRICE_SELL_18.ToSqlString());
            sb.AppendFormat("{0}, ", this.PRICE_SELL_10.ToSqlString());
            sb.AppendFormat("{0}, ", this.PRICE_SELL_0.ToSqlString());
            sb.AppendFormat("{0}, ", this.PRICE_NDS_18.ToSqlString());
            sb.AppendFormat("{0}, ", this.PRICE_NDS_10.ToSqlString());
            sb.AppendFormat("{0}, ", this.PRICE_TAX_FREE.ToSqlString());
            sb.AppendFormat("{0}, ", this.PRICE_TOTAL.ToSqlString());
            sb.AppendFormat("{0}, ", this.PRICE_NDS_TOTAL.ToSqlString());
            sb.AppendFormat("{0}, ", this.DIFF_CORRECT_DECREASE.ToSqlString());
            sb.AppendFormat("{0}, ", this.DIFF_CORRECT_INCREASE.ToSqlString());
            sb.AppendFormat("{0}, ", this.DIFF_CORRECT_NDS_DECREASE.ToSqlString());
            sb.AppendFormat("{0}, ", this.DIFF_CORRECT_NDS_INCREASE.ToSqlString());
            sb.AppendFormat("{0}, ", this.PRICE_NDS_BUYER.ToSqlString());
            sb.AppendFormat("'{0}', ", this.ROW_KEY);
            sb.AppendFormat("'{0}', ", this.ACTUAL_ROW_KEY);
            sb.AppendFormat("'{0}', ", this.COMPARE_ROW_KEY);
            sb.AppendFormat("{0}, ", this.COMPARE_ALGO_ID);
            sb.AppendFormat("'{0}', ", this.FORMAT_ERRORS);
            sb.AppendFormat("'{0}', ", this.LOGICAL_ERRORS);
            sb.AppendFormat("'{0}', ", this.SELLER_AGENCY_INFO_INN);
            sb.AppendFormat("'{0}', ", this.SELLER_AGENCY_INFO_KPP);
            sb.AppendFormat("'{0}', ", this.SELLER_AGENCY_INFO_NAME);
            sb.AppendFormat("'{0}', ", this.SELLER_AGENCY_INFO_NUM);
            sb.AppendFormat("to_date('{0}', 'dd.mm.yyyy') ", this.SELLER_AGENCY_INFO_DATE.GetValueOrDefault().ToString("dd.MM.yyyy"));
            sb.Append(");");

            return sb.ToString();
        }

        public string ToSqlLoader(long reqId)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("{0}|", reqId);
            sb.AppendFormat("{0}|", this.DeclarationId);
            sb.AppendFormat("{0}|", this.CHAPTER);
            sb.AppendFormat("{0}|", this.ORDINAL_NUMBER);
            sb.AppendFormat("{0}|", this.OKV_CODE);
            sb.AppendFormat("{0}|", this.CREATE_DATE.GetValueOrDefault().ToString("dd.MM.yyyy"));
            sb.AppendFormat("{0}|", this.RECEIVE_DATE.GetValueOrDefault().ToString("dd.MM.yyyy"));
            sb.AppendFormat("{0}|", this.OPERATION_CODE);
            sb.AppendFormat("{0}|", this.INVOICE_NUM);
            sb.AppendFormat("{0}|", this.INVOICE_DATE.GetValueOrDefault().ToString("dd.MM.yyyy"));
            sb.AppendFormat("{0}|", this.CHANGE_NUM);
            sb.AppendFormat("{0}|", this.CHANGE_DATE.GetValueOrDefault().ToString("dd.MM.yyyy"));
            sb.AppendFormat("{0}|", this.CORRECTION_NUM);
            sb.AppendFormat("{0}|", this.CORRECTION_DATE.GetValueOrDefault().ToString("dd.MM.yyyy"));
            sb.AppendFormat("{0}|", this.CHANGE_CORRECTION_NUM);
            sb.AppendFormat("{0}|", this.CHANGE_CORRECTION_DATE.GetValueOrDefault().ToString("dd.MM.yyyy"));
            sb.AppendFormat("{0}|", this.RECEIPT_DOC_NUM);
            sb.AppendFormat("{0}|", this.RECEIPT_DOC_DATE.GetValueOrDefault().ToString("dd.MM.yyyy"));
            sb.AppendFormat("{0}|", this.BUY_ACCEPT_DATE.FirstDate().GetValueOrDefault().ToString("dd.MM.yyyy"));
            sb.AppendFormat("{0}|", this.BUYER_INN);
            sb.AppendFormat("{0}|", this.BUYER_KPP);
            sb.AppendFormat("{0}|", this.SELLER_INN);
            sb.AppendFormat("{0}|", this.SELLER_KPP);
            sb.AppendFormat("{0}|", this.SELLER_INVOICE_NUM);
            sb.AppendFormat("{0}|", this.SELLER_INVOICE_DATE.GetValueOrDefault().ToString("dd.MM.yyyy"));
            sb.AppendFormat("{0}|", this.BROKER_INN);
            sb.AppendFormat("{0}|", this.BROKER_KPP);
            sb.AppendFormat("{0}|", this.DEAL_KIND_CODE);
            sb.AppendFormat("{0}|", this.CUSTOMS_DECLARATION_NUM);
            sb.AppendFormat("{0}|", this.PRICE_BUY_AMOUNT.ToSqlLoaderString());
            sb.AppendFormat("{0}|", this.PRICE_BUY_NDS_AMOUNT.ToSqlLoaderString());
            sb.AppendFormat("{0}|", this.PRICE_SELL.ToSqlLoaderString());
            sb.AppendFormat("{0}|", this.PRICE_SELL_IN_CURR.ToSqlLoaderString());
            sb.AppendFormat("{0}|", this.PRICE_SELL_18.ToSqlLoaderString());
            sb.AppendFormat("{0}|", this.PRICE_SELL_10.ToSqlLoaderString());
            sb.AppendFormat("{0}|", this.PRICE_SELL_0.ToSqlLoaderString());
            sb.AppendFormat("{0}|", this.PRICE_NDS_18.ToSqlLoaderString());
            sb.AppendFormat("{0}|", this.PRICE_NDS_10.ToSqlLoaderString());
            sb.AppendFormat("{0}|", this.PRICE_TAX_FREE.ToSqlLoaderString());
            sb.AppendFormat("{0}|", this.PRICE_TOTAL.ToSqlLoaderString());
            sb.AppendFormat("{0}|", this.PRICE_NDS_TOTAL.ToSqlLoaderString());
            sb.AppendFormat("{0}|", this.DIFF_CORRECT_DECREASE.ToSqlLoaderString());
            sb.AppendFormat("{0}|", this.DIFF_CORRECT_INCREASE.ToSqlLoaderString());
            sb.AppendFormat("{0}|", this.DIFF_CORRECT_NDS_DECREASE.ToSqlLoaderString());
            sb.AppendFormat("{0}|", this.DIFF_CORRECT_NDS_INCREASE.ToSqlLoaderString());
            sb.AppendFormat("{0}|", this.PRICE_NDS_BUYER.ToSqlLoaderString());
            sb.AppendFormat("{0}|", this.ROW_KEY);
            sb.AppendFormat("{0}|", this.ACTUAL_ROW_KEY);
            sb.AppendFormat("{0}|", this.COMPARE_ROW_KEY);
            sb.AppendFormat("{0}|", this.COMPARE_ALGO_ID);
            sb.AppendFormat("{0}|", this.FORMAT_ERRORS);
            sb.AppendFormat("{0}|", this.LOGICAL_ERRORS);
            sb.AppendFormat("{0}|", this.SELLER_AGENCY_INFO_INN);
            sb.AppendFormat("{0}|", this.SELLER_AGENCY_INFO_KPP);
            sb.AppendFormat("{0}|", this.SELLER_AGENCY_INFO_NAME);
            sb.AppendFormat("{0}|", this.SELLER_AGENCY_INFO_NUM);
            sb.AppendFormat("{0}|0|0||", this.SELLER_AGENCY_INFO_DATE.GetValueOrDefault().ToString("dd.MM.yyyy"));

            return sb.ToString();
        }
    }

}

