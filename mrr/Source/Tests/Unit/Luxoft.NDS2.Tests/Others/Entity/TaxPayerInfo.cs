﻿using System;

namespace Luxoft.NDS2.Tests.Others.Entity
{
    public class TPInfo
    {
        public string Inn { get; set; }
        public string Kpp { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string SurName { get; set; }
        public string SonoCode { get; set; }

        public bool IsUl
        {
            get { return !string.IsNullOrEmpty(Kpp); }
        }

        public string ToSqlFIR(long sc)
        {
            string ret;

            if (string.IsNullOrEmpty(Kpp))
                ret = string.Format(@"insert into FIR.V_EGRN_IP (id, innfl, last_name, first_name, patronymic, code_no, adr, date_on, date_off) values({0}, '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', {7}, {8});", sc.ToString(), Inn, LastName, Name, SurName, "1436", "643,194291,САНКТ-ПЕТЕРБУРГ Г,,,,САНТЬЯГО-ДЕ-КУБА УЛ,8,2,249", "sysdate", "sysdate");
            else
                ret = string.Format(@"insert into FIR.V_EGRN_UL (id, inn, kpp, name_full, code_no, adr, date_reg, date_on, date_off, ust_capital) values({0}, '{1}', '{2}', '{3}', '{4}', '{5}', {6}, {7}, {8}, {9});", sc.ToString(), Inn, Kpp, Name, "5252", "643,194291,САНКТ-ПЕТЕРБУРГ Г,,,,САНТЬЯГО-ДЕ-КУБА УЛ,8,2,249", "sysdate", "sysdate", "sysdate", "10000");

            return ret;                
        }

        public string ToSql(long sc)
        {
            string ret;

            if (string.IsNullOrEmpty(Kpp))
                ret = string.Format(@"insert into NDS2_MRR_USER.V_EGRN_IP (id, innfl, last_name, first_name, patronymic, code_no, adr, date_on, date_off) values({0}, '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', {7}, {8});", sc.ToString(), Inn, LastName, Name, SurName, "1436", "643,194291,САНКТ-ПЕТЕРБУРГ Г,,,,САНТЬЯГО-ДЕ-КУБА УЛ,8,2,249", "sysdate", "sysdate");
            else
                ret = string.Format(@"insert into NDS2_MRR_USER.V_EGRN_UL (id, inn, kpp, name_full, code_no, adr, date_reg, date_on, date_off, ust_capital) values({0}, '{1}', '{2}', '{3}', '{4}', '{5}', {6}, {7}, {8}, {9});",sc.ToString(), Inn, Kpp, Name, "5252", "643,194291,САНКТ-ПЕТЕРБУРГ Г,,,,САНТЬЯГО-ДЕ-КУБА УЛ,8,2,249","sysdate", "sysdate", "sysdate", "10000");

            return ret;
        }

        public string ToSqlLoader(long identity)
        {
            string ret;

            if (string.IsNullOrEmpty(Kpp))
                ret = string.Format(@"{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}", identity.ToString(), Inn, LastName, Name, SurName, SonoCode, "643,194291,САНКТ-ПЕТЕРБУРГ Г,,,,САНТЬЯГО-ДЕ-КУБА УЛ,8,2,249", "01.01.2001", "12.12.2020");
            else
                ret = string.Format(@"{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}|{9}", identity.ToString(), Inn, Kpp, Name, SonoCode, "643,194291,САНКТ-ПЕТЕРБУРГ Г,,,,САНТЬЯГО-ДЕ-КУБА УЛ,8,2,249", "01.01.2007", "12.12.2001", "12.12.2020", "10000");

            return ret;
        }

        public string ToSqlBankAccounts()
        {
            string ret = string.Empty;
            Random rnd = new Random();

            int maxCount = rnd.Next(1, 3);

            for (int i = 0; i <= maxCount; i++)
            {
                ret += "\r\n";

                if (string.IsNullOrEmpty(Kpp))
                    ret += string.Format(@"insert into NDS2_MRR_USER.BSSCHET_IP (inn, bik, nameko, nomsch, prval, dateopensch) values('{0}', '{1}', '{2}', '{3}', '{4}', {5});",Inn, rnd.Next(1, 1000000).ToString("D9"), "РОССЕЛЬХОЗБАНК", rnd.Next(1, 1000000).ToString("D10"), rnd.Next(0, 1).ToString("D1"), "sysdate");
                else
                    ret += string.Format(@"insert into NDS2_MRR_USER.BSSCHET_UL (inn, kpp, bik, nameko, nomsch, prval, dateopensch) values('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', {6});",Inn, Kpp, rnd.Next(1, 1000000).ToString("D9"), "РОССЕЛЬХОЗБАНК", rnd.Next(1, 1000000).ToString("D10"), rnd.Next(0, 1).ToString("D1"), "sysdate");
            }

            return ret;
        }
    }
}


