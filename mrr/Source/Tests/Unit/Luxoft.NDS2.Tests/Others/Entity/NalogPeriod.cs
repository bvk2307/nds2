﻿using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using System;

namespace Luxoft.NDS2.Tests.Others.Entity
{
    public class NalogPeriod
    {
        public static DictionaryNalogPeriods GetNalogPeriod(string period, string sYear)
        {
            int year = Convert.ToInt16(sYear);

            DictionaryNalogPeriods item = new DictionaryNalogPeriods();
            item.CODE = period;
            if (item.CODE == "21")
            {
                item.DateBegin = new DateTime(year, 01, 01);
                item.DateEnd = new DateTime(year, 03, 31);
            }
            else if (item.CODE == "22")
            {
                item.DateBegin = new DateTime(year, 04, 01);
                item.DateEnd = new DateTime(year, 06, 30);
            }
            else if (item.CODE == "23")
            {
                item.DateBegin = new DateTime(year, 07, 01);
                item.DateEnd = new DateTime(year, 09, 30);
            }
            else if (item.CODE == "24")
            {
                item.DateBegin = new DateTime(year, 10, 01);
                item.DateEnd = new DateTime(year, 12, 31);
            }
            else
            {
                item.DateBegin = new DateTime(year, 01, 01);
                item.DateEnd = new DateTime(year, 03, 31);
            }
            return item;
        }

        static Random _random = new Random();

        public static DateTime? GetInnerPeriodDT(string period, string year)
        {
            DictionaryNalogPeriods np = GetNalogPeriod(period, year);
            DateTime start = np.DateBegin.AddDays(2);
            DateTime end = np.DateEnd.AddDays(-2);
            int range = (end - start).Days;
            return start.AddDays(_random.Next(range));
        }
    }
}
