﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using NUnit.Framework;

namespace Luxoft.NDS2.Tests.Others
{
    [TestFixture]
    [Category("Manual")]
    public class BuyBookGenerator
    {
        Random _random = new Random(int.MaxValue);

        [Test]
        public void SplitHadoopTestData()
        {
            //int linesToRead = 750000;
            //int linesToRead = 1500000;
            int linesToRead = 2250000;

            using (StreamWriter writer = new StreamWriter(@"C:\Projects\Luxoft\GNIVC_NDS2\Personal\Out.csv", true))
            {
                using (StreamReader reader = new StreamReader(@"C:\Projects\Luxoft\GNIVC_NDS2\Personal\tins_2500000_201423_23.csv"))
                {
                    do
                    {
                        var line = reader.ReadLine().Split(';')[0].Replace("\"", string.Empty);
                        writer.Write("{0};{1};{2}\n", line, "23", "2004");
                    }
                    while (--linesToRead != 0);
                }                
            }
        }

        [Test]
        public void FormatTestData()
        {
            using (StreamWriter writer = new StreamWriter(@"C:\Projects\Luxoft\GNIVC_NDS2\DEV\trunk\Release_1\TestData\BuyBook.txt", true))
            {
                using (StreamReader reader = new StreamReader(@"C:\Projects\Luxoft\GNIVC_NDS2\DEV\trunk\Release_1\TestData\index.txt"))
                {
                    var line = reader.ReadLine();
                    while (!string.IsNullOrEmpty(line))
                    {
                        
                        var parameters = line.Split(';')[0].Split('-');
                        if (parameters[1].Equals("buyBooksPath"))
                        {
                            writer.Write("----------------------\n");
                            writer.Write(string.Format("ИНН:{0}\n", parameters[0]));
                            writer.Write(string.Format("Период:{0}\n", parameters[2]));
                            writer.Write(string.Format("Год:{0}\n", parameters[3]));                            
                        }


                        line = reader.ReadLine();
                    }
                }
            }
        }

        [Test]
        public void HttpWebRequestProxyCheck()
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://94.125.90.50:6336/NdsRestWS/errors?inn=7715805253&year=2014&period=03&isSalesBook=true&errorType=3&errorType=4&attrCode=2&attrCode=11");

            request.Proxy = WebRequest.GetSystemWebProxy();
            request.Proxy.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;

            var resp = request.GetResponse();


            Console.WriteLine((new StreamReader(resp.GetResponseStream()).ReadToEnd()));
        }

        [Test]
        public void Generate()
        {
            uint numberOfLines = 100;

            using (StreamWriter writer = new StreamWriter("test.xml"))
            {
                using (XmlWriter w = new XmlTextWriter(writer))
                {
                    w.WriteStartElement("Файл");
                    {
                        w.WriteStartElement("Документ");
                        {
                            w.WriteStartElement("СвНП");
                            {
                                w.WriteStartElement("НПЮЛ");
                                {
                                    w.WriteAttributeString("ИННЮЛ", GetRandomINN());
                                    w.WriteAttributeString("КПП", GetRandomKPP());
                                    w.WriteAttributeString("НаимОрг", GetRandomTaxPayerName());
                                }
                                w.WriteEndElement();
                            }
                            w.WriteEndElement();

                            for (int i = 0; i++ < numberOfLines; )
                            {
                                w.WriteStartElement("КнигаПокуп");
                                {
                                    w.WriteAttributeString("СумНДСВсКПк", GetRandomDecimalString());

                                    w.WriteStartElement("КнПокСтр");
                                    {
                                        w.WriteAttributeString("ДатаИспрКСчФ", GetRandomDateString());
                                        w.WriteAttributeString("ДатаСчФПрод", GetRandomDateString());
                                        w.WriteAttributeString("НомСчФПрод", GetRandomInvoiceNumber());
                                        w.WriteAttributeString("НомИспрКСчФ", GetRandomInvoiceNumber());
                                        w.WriteAttributeString("НомДокПдтвОпл", GetRandomInvoiceNumber());
                                        w.WriteAttributeString("ДатаИспрСчФ", GetRandomDateString());
                                        w.WriteAttributeString("ДатаДокПдтвОпл", GetRandomDateString());
                                        w.WriteAttributeString("НомКСчФПрод", GetRandomInvoiceNumber());
                                        w.WriteAttributeString("ОКВ", GetRandomInvoiceNumber());
                                        w.WriteAttributeString("ДатаКСчФПрод", GetRandomDateString());
                                        w.WriteAttributeString("НомТД", GetRandomInvoiceNumber());
                                        w.WriteAttributeString("НомерПор", GetRandomInvoiceNumber());
                                        w.WriteAttributeString("СтоимПокупВ", GetRandomDecimalString());
                                        w.WriteAttributeString("ДатаУчТов", GetRandomDateString());
                                        w.WriteAttributeString("НомИспрСчФ", GetRandomInvoiceNumber());
                                        w.WriteAttributeString("СумНДСВыч", GetRandomDecimalString());


                                        w.WriteStartElement("КодВидОпер");
                                        {
                                            w.WriteRaw(GetRandomOperationCode());
                                        }
                                        w.WriteEndElement();


                                        w.WriteStartElement("СвПрод");
                                        {
                                            w.WriteStartElement("СведЮЛ");
                                            {
                                                w.WriteAttributeString("ИННЮЛ", GetRandomINN());
                                                w.WriteAttributeString("КПП", GetRandomKPP());
                                            }
                                            w.WriteEndElement();
                                        }
                                        w.WriteEndElement();

                                        w.WriteStartElement("СвПос");
                                        {
                                            w.WriteStartElement("СведЮЛ");
                                            {
                                                w.WriteAttributeString("ИННЮЛ", GetRandomINN());
                                                w.WriteAttributeString("КПП", GetRandomKPP());
                                            }
                                            w.WriteEndElement();
                                        }
                                        w.WriteEndElement();
                                    }
                                    w.WriteEndElement();
                                }
                                w.WriteEndElement();
                            }


                        }
                        w.WriteEndElement();
                    }
                    w.WriteEndElement();
                }
            };
        }

        string GetRandomDateString()
        {
            DateTime start = new DateTime(2012, 1, 1);
            int range = (DateTime.Today - start).Days;
            return start.AddDays(_random.Next(range)).ToString("dd.MM.yyyy");
        }

        string GetRandomInvoiceNumber()
        {
            return _random.Next(1000, 10000000).ToString();
        }

        string GetRandomOperationCode()
        {
            return _random.Next(0, 99).ToString();
        }

        string GetRandomINN()
        {
            var l = _random.Next(100000, 999999);
            var r = _random.Next(1000000, 9999999);
            return string.Format("{0}{1}", l, r);
        }

        string GetRandomKPP()
        {
            var l = _random.Next(100000, 999999);
            var r = _random.Next(1000, 9999);
            return string.Format("{0}{1}", l, r);
        }

        string GetRandomDecimalString()
        {
            return string.Format("{0}.{1}", _random.Next(10, 1000000), _random.Next(10, 99));
        }

        string GetRandomTaxPayerName()
        {
            return Guid.NewGuid().ToString();
        }
    }
}

