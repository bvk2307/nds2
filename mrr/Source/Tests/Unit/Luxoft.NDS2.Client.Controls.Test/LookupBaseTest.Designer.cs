﻿namespace Luxoft.NDS2.Client.Controls.Test
{
    partial class LookupBaseTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._shortLookup = new Luxoft.NDS2.Client.UI.Controls.LookupEditor.LookupBase();
            this._fullLookup = new Luxoft.NDS2.Client.UI.Controls.LookupEditor.LookupBase();
            this.SuspendLayout();
            // 
            // _shortLookup
            // 
            this._shortLookup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._shortLookup.Location = new System.Drawing.Point(0, 25);
            this._shortLookup.Name = "_shortLookup";
            this._shortLookup.Size = new System.Drawing.Size(450, 25);
            this._shortLookup.TabIndex = 1;
            // 
            // _fullLookup
            // 
            this._fullLookup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._fullLookup.Location = new System.Drawing.Point(0, 0);
            this._fullLookup.Name = "_fullLookup";
            this._fullLookup.Size = new System.Drawing.Size(450, 25);
            this._fullLookup.TabIndex = 0;
            // 
            // LookupBaseTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(450, 164);
            this.Controls.Add(this._fullLookup);
            this.Controls.Add(this._shortLookup);
            this.Name = "LookupBaseTest";
            this.Text = "Luxoft.NDS2.Client.UI.Controls.LookupEditor";
            this.ResumeLayout(false);

        }

        #endregion

        private UI.Controls.LookupEditor.LookupBase _fullLookup;
        private UI.Controls.LookupEditor.LookupBase _shortLookup;
    }
}