﻿using Luxoft.NDS2.Client.UI.Controls.LookupEditor;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.Controls.Test
{
    public partial class LookupBaseTest : Form
    {
        public LookupBaseTest()
        {
            InitializeComponent();

            _fullLookup.WithEditor(new UI.Controls.LookupEditor.LookupFull.Editor());
            var src = new DataMock(100);
            new LookupPresenter(_fullLookup, new LookupModel(src.Items));

            _shortLookup.WithEditor(new UI.Controls.LookupEditor.LookupShort.Editor());
            src = new DataMock(8);
            new LookupPresenter(_shortLookup, new LookupModel(src.Items));
        }
    }
}
