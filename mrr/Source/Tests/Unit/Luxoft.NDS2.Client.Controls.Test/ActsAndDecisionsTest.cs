﻿using Luxoft.NDS2.Client.UI;
using Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Model;
using Luxoft.NDS2.Client.UI.Reports.ActsAndDecisions;
using Luxoft.NDS2.Client.UI.TaxPayer.BankAccountsRequestDialog;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;
using Luxoft.NDS2.Common.Contracts.DTO.KnpResult;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Luxoft.NDS2.Common.Contracts.DTO.TaxPeriod;
using Presenter = Luxoft.NDS2.Client.UI.Reports.ActsAndDecisions.ReportPresenter;

namespace Luxoft.NDS2.Client.Controls.Test
{
    public class DataService : IActDecisionReportDataLoader
    {
        private void FillData(UI.Reports.ActsAndDecisions.ReportViewModel model)
        {
            /*model.Data = new[] {new ReportItem(new KnpResultReportSummary()) {District = "СФО"}};*/
        }

        #region Implementation of IActDecisionReportDataLoader

        public void FillDataByRegionForDate(DateTime date, int year, Quarter quarter, INotifier notifier, UI.Reports.ActsAndDecisions.ReportViewModel model)
        {
            FillData(model);
        }

        public void FillDataBySonoForDate(DateTime date, int year, Quarter quarter, INotifier notifier, UI.Reports.ActsAndDecisions.ReportViewModel model)
        {
            FillData(model);
        }

        public void FillDataByRegionDelta(DateTime dateFrom, DateTime dateTo, int year, Quarter quarter, INotifier notifier, UI.Reports.ActsAndDecisions.ReportViewModel model)
        {
            FillData(model);
        }

        public void FillDataBySonoDelta(DateTime dateFrom, DateTime dateTo, int year, Quarter quarter, INotifier notifier, UI.Reports.ActsAndDecisions.ReportViewModel model)
        {
            FillData(model);
        }

        #endregion
    }
    
    public partial class ActsAndDecisionsTest : Form
    {
        public ActsAndDecisionsTest()
        {
            InitializeComponent();

            var presenter = new Presenter(baseView1, null, new DataService());
        }
    }
}
