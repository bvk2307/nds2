﻿using System;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.Controls.Test
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new LookupBaseTest());
            Application.Run(new ActsAndDecisionsTest());
        }
    }
}
