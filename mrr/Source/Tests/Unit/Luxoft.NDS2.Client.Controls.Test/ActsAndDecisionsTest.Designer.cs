﻿using Luxoft.NDS2.Client.UI.Reports.ActsAndDecisions;

namespace Luxoft.NDS2.Client.Controls.Test
{
    partial class ActsAndDecisionsTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.baseView1 = new Luxoft.NDS2.Client.UI.Reports.ActsAndDecisions.View();
            this.SuspendLayout();
            // 
            // baseView1
            // 
            this.baseView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.baseView1.Location = new System.Drawing.Point(0, 0);
            this.baseView1.Name = "baseView1";
            this.baseView1.Size = new System.Drawing.Size(813, 460);
            this.baseView1.TabIndex = 0;
            // 
            // ActsAndDecisionsTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(813, 460);
            this.Controls.Add(this.baseView1);
            this.Name = "ActsAndDecisionsTest";
            this.Text = "ActsAndDecisionsTest";
            this.ResumeLayout(false);

        }

        #endregion

        private Luxoft.NDS2.Client.UI.Reports.ActsAndDecisions.View baseView1;
    }
}