﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Models.ViewModels;
using NUnit.Framework;
using System.Collections.Generic;

namespace Luxoft.NDS2.Tests.Client
{
    [TestFixture]
    public class SelectionDiscrepancyModelTest
    {
        private static Dictionary<string, string> TestValues =
            new Dictionary<string, string>
            {
                { "1", "1" },
                { "2", "2" },
                { "3", "1; 2" },
                { "4", "3" },
                { "543", "1; 2; 3; 4; 5; 10" },
                { "hello", "hello"},
                { "9999999999999999999999999999999999999999999999999", "9999999999999999999999999999999999999999999999999"},
                { "-1", "-1"}
            };

        private static SelectionDiscrepancy TestData(string buyerOperation)
        {
            return new SelectionDiscrepancy { BuyerOperation = buyerOperation };
        }

        [Test]
        public void ConvertBuyerOperationCodes()
        {
            foreach (var testItem in TestValues)
            {
                var actual = new SelectionDiscrepancyModel(TestData(testItem.Key)).BuyerOperation;
                var expected = testItem.Value;

                Assert.AreEqual(expected, actual);
            }
        }
    }
}
