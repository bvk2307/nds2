﻿using System;

namespace Luxoft.NDS2.Client.Test.Sample
{
    public interface IView
    {
        string Id
        {
            get;
        }

        string Data
        {
            get;
            set;
        }

        event EventHandler OnSubmit;

        void ShowError(string message);
    }
}
