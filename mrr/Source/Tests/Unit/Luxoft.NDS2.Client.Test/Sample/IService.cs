﻿namespace Luxoft.NDS2.Client.Test.Sample
{
    public interface IService
    {
        string GetData(int id);
    }
}
