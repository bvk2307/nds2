﻿using NUnit.Framework;
using Rhino.Mocks;
using System;

namespace Luxoft.NDS2.Client.Test.Sample
{
    [TestFixture]
    public class PresenterTest
    {
        # region Создание объекта

        private static Presenter CreatePresenter(IView view, IService service)
        {
            return new Presenter(view, service);
        }

        # endregion

        # region Тесты Submit

        /// <summary>
        /// Базовый сценарий. Пользователь ввел нормальный тестовый ИД, нажал кнопку сабмит и увидел на экране тестовые данные
        /// </summary>
        [Test]
        public void BasicFlow()
        {
            var view = MockRepository.GenerateStub<IView>(); // заглушка формы
            var service = MockRepository.GenerateStub<IService>(); // заглушка сервиса
            
            var testId = 123; // тестовый ИД (входные данные)
            var testResult = "result"; // тестовый результат (выходные данные)

            var presenter = CreatePresenter(view, service);
            
            // Указываем, что стаб сервиса должен возвращать тестовый результат 
            // если в качестве параметра передан тестовый ИД
            service.Stub(x => x.GetData(testId)).Return(testResult);
            
            //Пользователь ввел тестовый ИД
            view.Stub(x => x.Id).Return(testId.ToString());

            //И нажал сабмит
            view.Raise(x => x.OnSubmit += null, view, EventArgs.Empty);

            Assert.AreEqual(testResult, view.Data);
        }

        /// <summary>
        /// Альтернативный сценарий. Пользователь ввел некорректный ИД и должен получить сообщение об ошибке "model error"
        /// </summary>
        [Test]
        public void InvalidIdFlow()
        {
            var view = MockRepository.GenerateStub<IView>(); // заглушка формы
            var service = MockRepository.GenerateStub<IService>(); // заглушка сервиса

            var testId = "s1564"; // тестовый ИД (входные данные)

            var presenter = CreatePresenter(view, service);

            //Пользователь ввел тестовый ИД
            view.Stub(x => x.Id).Return(testId);

            //Пользователь жмет сабмит
            view.Raise(x => x.OnSubmit += null, view, EventArgs.Empty);

            // Ожидаем что будет вызван ShowError с параметром "model error"
            view.AssertWasCalled(x => x.ShowError("model error"));
        }

        /// <summary>
        /// Альтернативный сценарий. Пользователь ввел корректный ИД, но сервис падает с ошибкой.
        /// Пользователь должен получить сообщение об ошибке "service error"
        /// </summary>
        [Test]
        public void ServiceUnavailableFlow()
        {
            var view = MockRepository.GenerateStub<IView>(); // заглушка формы
            var service = MockRepository.GenerateStub<IService>(); // заглушка сервиса

            var testId = "1564"; // тестовый ИД (входные данные)

            var presenter = CreatePresenter(view, service);

            // Мокаем сервис
            service
                .Stub(x => x.GetData(Arg<int>.Is.Anything))
                .Throw(new Exception());

            //Пользователь ввел тестовый ИД
            view.Stub(x => x.Id).Return(testId);

            //Пользователь жмет сабмит
            view.Raise(x => x.OnSubmit += null, view, EventArgs.Empty);

            // Ожидаем что будет вызван ShowError с параметром "service error"
            view.AssertWasCalled(x => x.ShowError("service error"));
        }

        # endregion
    }
}
