﻿using System;

namespace Luxoft.NDS2.Client.Test.Sample
{
    public class Presenter
    {
        private readonly IView _view;

        private readonly IService _service;

        private readonly DataModel _model = new DataModel();

        public Presenter(IView view, IService service)
        {
            _view = view;
            _service = service;
            _view.OnSubmit += Submit;
        }

        private void Submit(object sender, EventArgs e)
        {
            if (_model.TrySetId(_view.Id))
            {
                try
                {
                    _view.Data = _service.GetData(_model.Id);
                }
                catch
                {
                    _view.ShowError("service error");
                }
            }
            else
            {
                _view.ShowError("model error");
            }
        }
    }
}
