﻿using NUnit.Framework;
using System;

namespace Luxoft.NDS2.Client.Test.Sample
{
    [TestFixture]
    public class DataModelTest
    {
        # region Создание объека

        private static DataModel CreateModel()
        {
            return new DataModel();
        }

        # endregion

        # region Тесты TrySetId

        [Test]
        public void MinId()
        {
            Assert.IsTrue(CreateModel().TrySetId(int.MinValue.ToString()));
        }

        [Test]
        public void MaxId()
        {
            Assert.IsTrue(CreateModel().TrySetId(int.MaxValue.ToString()));
        }

        [Test]
        public void Zero()
        {
            Assert.IsTrue(CreateModel().TrySetId("0"));
        }

        [Test]
        public void OverflowMax()
        {
            Assert.IsFalse(CreateModel().TrySetId(((long)int.MaxValue + 1).ToString()));
        }

        [Test]
        public void OverflowMin()
        {
            Assert.IsFalse(CreateModel().TrySetId(((long)int.MinValue - 1).ToString()));
        }

        [Test]
        public void EmptyString()
        {
            Assert.IsFalse(CreateModel().TrySetId(string.Empty));
        }

        [Test]
        public void InvalidFormat()
        {
            Assert.IsFalse(CreateModel().TrySetId("325a"));
        }

        # endregion
    }
}
