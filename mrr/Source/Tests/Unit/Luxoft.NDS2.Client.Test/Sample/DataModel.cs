﻿namespace Luxoft.NDS2.Client.Test.Sample
{
    public class DataModel
    {
        public int Id
        {
            get;
            private set;
        }

        public bool TrySetId(string value)
        {
            int tempId;

            if (int.TryParse(value, out tempId))
            {
                Id = tempId;
                return true;
            }

            return false;
        }
    }
}
