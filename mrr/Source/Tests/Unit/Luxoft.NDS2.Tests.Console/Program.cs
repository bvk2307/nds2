﻿using Luxoft.NDS2.Tests.Others.TestDataGenerator;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Tests.ConsoleGenerator
{
    class Program
    {
        private sealed class Settings
        {
            public bool ShowSqlOnly { get; set; }
            public string OutputFile { get; set; }
            public bool ProcessingFromCI { get; set; }
        }

        static void Main(string[] args)
        {
            const string batFileName = "exec.bat";
            const string tempFileName = "Generator.txt";

            var settings = ReadSettings(args);
            using (var stream = new FileStream(tempFileName, FileMode.Create))
            using (var writer = new StreamWriter(stream))
            {
                var oldOut = Console.Out;
                Console.SetOut(writer);
                WriteBody(settings);
                writer.Close();
                Console.SetOut(oldOut);
            }

            if (!settings.ShowSqlOnly)
            {
                using (var writer = new StreamWriter(batFileName, false, Encoding.UTF8))
                {
                    writer.WriteLine("@echo OFF");
                    writer.WriteLine("set userName=NDS2_INSTALL");
                    writer.WriteLine("set userPas=NDS2_INSTALL");
                    writer.WriteLine("set SidName=NDS2");
                    writer.WriteLine("sqlplus %userName%/%userPas%@%SidName% @{0}", tempFileName);
                    
                    if (!settings.ProcessingFromCI)
                        writer.WriteLine("pause");
                }
            }

            var StartInfo = new ProcessStartInfo
            {
                FileName = !settings.ShowSqlOnly ? batFileName : tempFileName,
                UseShellExecute = false,
                RedirectStandardOutput = true
            };

            Process proc = Process.Start(StartInfo);

            while (!proc.StandardOutput.EndOfStream)
            {
                string line = proc.StandardOutput.ReadLine();
                Console.WriteLine(line);
            }

            if (!settings.ShowSqlOnly)
                File.Delete(batFileName);

            File.Delete(tempFileName);

            Environment.Exit(proc.ExitCode);
        }

        private static void WriteBody(Settings settings)
        {
            if (!settings.ShowSqlOnly)
            {
                Console.WriteLine("WHENEVER SQLERROR EXIT SQL.SQLCODE");
            }
            new FirTestDataGenerator().GenerateDeclarationsAndMissmatches();

            Console.WriteLine(Constants.COPY_INVOICE);
            //Console.WriteLine(Constants.REFRESH_MVIEWS);
            //Console.WriteLine(Constants.COPY_DECLARATIONS);

            if (!settings.ShowSqlOnly)
            {
                Console.WriteLine("exit;");
            }
        }

        private static Settings ReadSettings(string[] args)
        {
            if (args == null)
            {
                return new Settings();
            }

            return new Settings
            {
                ShowSqlOnly = args.Contains("-sql", StringComparer.OrdinalIgnoreCase) || args.Contains("/sql", StringComparer.OrdinalIgnoreCase),
                ProcessingFromCI = args.Contains("-CI", StringComparer.OrdinalIgnoreCase) || args.Contains("/CI", StringComparer.OrdinalIgnoreCase)
            };
        }
    }
}