﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Luxoft.NDS2.Tests.ConsoleGenerator.DataGenerators
{
    class CalendarGenerator
    {
        private readonly Lazy<List<DateTime>> _basicRedDays = new Lazy<List<DateTime>>(FillBasicRedDays);

        private static List<DateTime> FillBasicRedDays()
        {
            var y = DateTime.Today.Year;
            return new List<DateTime>
            {
                new DateTime(y, 1, 1),  // новогодние праздники
                new DateTime(y, 1, 2),  // новогодние праздники
                new DateTime(y, 1, 3),  // новогодние праздники
                new DateTime(y, 1, 4),  // новогодние праздники
                new DateTime(y, 1, 5),  // новогодние праздники
                new DateTime(y, 1, 6),  // новогодние праздники
                new DateTime(y, 1, 7),  // Рождество
                new DateTime(y, 1, 8),  // новогодние праздники
                new DateTime(y, 2, 23), // день защитника Отечества
                new DateTime(y, 3, 8),  // международный женский день
                new DateTime(y, 5, 1),  // праздник весны и труда
                new DateTime(y, 5, 9),  // день Победы
                new DateTime(y, 6, 12), // день России
                new DateTime(y, 11, 4), // день народного единства
            };
        }

        private readonly List<DayOfWeek> _weekEnds = new List<DayOfWeek> { DayOfWeek.Saturday, DayOfWeek.Sunday };
        public List<DayOfWeek> WeekEnds { get { return _weekEnds; } }

        private bool DayAndMonthEquals(DateTime left, DateTime right)
        {
            return left.Day == right.Day && left.Month == right.Month;
        }

        public List<DateTime> GenerateRedDays(DateTime begin, DateTime end)
        {
            if (begin > end) { var tmp = begin; begin = end; end = tmp; }

            var result = new List<DateTime>();
            while (begin < end)
            {
                if (_basicRedDays.Value.Any(d => DayAndMonthEquals(d, begin)))
                {
                    var date = begin;
                    while (_weekEnds.Contains(date.DayOfWeek) || result.Any(d => d.Date == date.Date))
                        date = date.AddDays(1);
                    if (date >= end)
                        return result;
                    result.Add(date.Date);
                }
                begin = begin.AddDays(1);
            }
            return result;
        }

        public void GenerateScript(DateTime begin, DateTime end)
        {
            Console.WriteLine("TRUNCATE TABLE NDS2_MRR_USER.REDDAY;");
            var list = GenerateRedDays(begin, end);
            while (begin < end)
            {
                if (_weekEnds.Contains(begin.DayOfWeek) || list.Any(d => d.Date == begin.Date))
                    Console.WriteLine("INSERT INTO NDS2_MRR_USER.REDDAY (RED_DATE) VALUES (TO_DATE('{0}', 'yyyymmdd'));", begin.ToString("yyyyMMdd"));
                begin = begin.AddDays(1);
            }
            Console.WriteLine("COMMIT;");
        }
    }
}
