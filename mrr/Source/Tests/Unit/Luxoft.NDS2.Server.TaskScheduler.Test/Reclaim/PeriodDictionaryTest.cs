﻿using Luxoft.NDS2.Common.Contracts.TaxPeriod;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.Services.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.TaskScheduler.Test.Reclaim
{
    [TestClass]
    public class PeriodDictionaryTest
    {
        [TestMethod]
        public void PeriodDictionaryGeneralTest()
        {
            var serviceHelpers = new ServiceHelpers(ReclaimMockHelper.CreateStubServiceContext());

            var periods = TableAdapterCreator
                    .DictionaryTaxPeriodAdapter(serviceHelpers)
                    .GetList();

            var periodDictionary = new PeriodDictionary(periods);

            //---------
            var period = periodDictionary.Get("01");

            var beginDate = period.BeginDate(2015);
            var endDate = period.EndDate(2015);

            Assert.AreEqual(beginDate, new DateTime(2015, 1, 1));
            Assert.AreEqual(endDate, new DateTime(2015, 1, 31));

            //---------
            period = periodDictionary.Get("06");

            beginDate = period.BeginDate(2015);
            endDate = period.EndDate(2015);

            Assert.AreEqual(beginDate, new DateTime(2015, 6, 1));
            Assert.AreEqual(endDate, new DateTime(2015, 6, 30));

            //---------
            period = periodDictionary.Get("12");

            beginDate = period.BeginDate(2015);
            endDate = period.EndDate(2015);

            Assert.AreEqual(beginDate, new DateTime(2015, 12, 1));
            Assert.AreEqual(endDate, new DateTime(2015, 12, 31));

            //---------
            period = periodDictionary.Get("82");

            beginDate = period.BeginDate(2015);
            endDate = period.EndDate(2015);

            Assert.AreEqual(beginDate, new DateTime(2015, 12, 1));
            Assert.AreEqual(endDate, new DateTime(2015, 12, 31));

            //---------
            period = periodDictionary.Get("21");

            beginDate = period.BeginDate(2015);
            endDate = period.EndDate(2015);

            Assert.AreEqual(beginDate, new DateTime(2015, 1, 1));
            Assert.AreEqual(endDate, new DateTime(2015, 3, 31));

            //---------
            period = periodDictionary.Get("22");

            beginDate = period.BeginDate(2015);
            endDate = period.EndDate(2015);

            Assert.AreEqual(beginDate, new DateTime(2015, 4, 1));
            Assert.AreEqual(endDate, new DateTime(2015, 6, 30));

            //---------
            period = periodDictionary.Get("23");

            beginDate = period.BeginDate(2015);
            endDate = period.EndDate(2015);

            Assert.AreEqual(beginDate, new DateTime(2015, 7, 1));
            Assert.AreEqual(endDate, new DateTime(2015, 9, 30));

            //---------
            period = periodDictionary.Get("24");

            beginDate = period.BeginDate(2015);
            endDate = period.EndDate(2015);

            Assert.AreEqual(beginDate, new DateTime(2015, 10, 1));
            Assert.AreEqual(endDate, new DateTime(2015, 12, 31));

            //---------
            period = periodDictionary.Get("56");

            beginDate = period.BeginDate(2015);
            endDate = period.EndDate(2015);

            Assert.AreEqual(beginDate, new DateTime(2015, 10, 1));
            Assert.AreEqual(endDate, new DateTime(2015, 12, 31));
        }
    }
}
