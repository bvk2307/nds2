﻿using CommonComponents.Catalog;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy;
using Luxoft.NDS2.Server.Common.DTO.Reclaim;
using Luxoft.NDS2.Server.Services.Helpers;
using Luxoft.NDS2.Server.TaskScheduler.Reclaim.ReclaimBuilders;
using Luxoft.NDS2.Server.TaskScheduler.Reclaim.ReclaimXml;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace Luxoft.NDS2.Server.TaskScheduler.Test.Reclaim
{
    [TestClass]
    public class ReclaimOpenKNPBuilderTest
    {
        [TestMethod]
        public void ReclaimOpeningKNPBuildTest()
        {
            long docId = 111222333444555;

            string xsdSchemaName = "TAX3EXCH_NDS2_CAM_02_01.xsd";

            var reclaimQueue = new ReclaimQueue()
            {
                Id = 1,
                DocId = 12345,
                Side = 1,
                State = 1,
                IsLocked = 1,
                CreateDate = DateTime.Now

            };

            var reclaimDeclaration = new ReclaimDeclaration()
            {
                Zip = 2,
                RegNumber = 3,
                SonoCode = "5252",
                PeriodCode = "21",
                FiscalYear = "2016",
                Inn = "1234567890",
                Kpp = "123123123",
                KppEffective = "111111111",
            };

            var reclaimDiscrepacy = new ReclaimDiscrepancy()
            {
                DiscrepancyId = 111222333,
                Type = DiscrepancyType.Break,
                RuleNum = 1,
                BuyerInn = "1234567890",
                SellerInn = "2234567890",
                PeriodCode = "21",
                FiscalYear = "2016",
                BuyerKppEffective = "111111111",
                SellerKppEffective = "111111111",
                InvoiceNum = "invoice_num_01",
                InvoiceDate = new DateTime(2016, 2, 10),
                CorrectionNum = "correction_num_04",
                CorrectionDate = new DateTime(2016, 2, 15)
            };

            var xmlBuilder = ReclaimOpenKnpXmlBuilderCreator
                .Create(docId, reclaimQueue, reclaimDeclaration, ReclaimMockHelper.CreatePeriodDictionary());

            xmlBuilder.Append(reclaimDiscrepacy);

            var xmlBody = xmlBuilder.Build();

            var serviceHelpers = new ServiceHelpers(ReclaimMockHelper.CreateStubServiceContext());

            IXmlValidator xmlValidator = ReclaimMockHelper.CreateMockXmlValidator(xsdSchemaName);

            xmlValidator.Validate(xmlBody, ReclaimMockHelper.CreateCatalogAddress(xsdSchemaName));
        }
    }
}