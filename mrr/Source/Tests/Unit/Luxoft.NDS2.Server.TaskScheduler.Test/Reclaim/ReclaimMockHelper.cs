﻿using CommonComponents.Catalog;
using CommonComponents.Configuration;
using CommonComponents.Instrumentation;
using CommonComponents.Security.Authorization;
using CommonComponents.Shared;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO.TaxPeriod;
using Luxoft.NDS2.Common.Contracts.TaxPeriod;
using Luxoft.NDS2.Server.TaskScheduler.Reclaim.ReclaimBuilders;
using Luxoft.NDS2.Server.TaskScheduler.Reclaim.ReclaimXml;
using Rhino.Mocks;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace Luxoft.NDS2.Server.TaskScheduler.Test.Reclaim
{
    public static class ReclaimMockHelper
    {
        public static IReadOnlyServiceCollection CreateStubServiceContext()
        {
            //--- Mock IConfigurationDataService
            var mockConfiguraionService = MockRepository.GenerateStub<IConfigurationDataService>();

            var _appSettings = new AppSettingsSection();
            _appSettings.Settings.Add(new KeyValueConfigurationElement(Constants.DB_CONFIG_KEY, ConfigurationManager.AppSettings[Constants.DB_CONFIG_KEY]));

            mockConfiguraionService.Expect(p => p.TryGetSection<AppSettingsSection>(
                Arg<ProfileInfo>.Is.Anything, out Arg<AppSettingsSection>.Out(_appSettings).Dummy))
                .Return(true);

            //--- Mock IProfilingEntry
            var mockProfilingEntry = MockRepository.GenerateStub<IProfilingEntry>();

            //--- Mock IInstrumentationService
            var mockInstrumentationService = MockRepository.GenerateStub<IInstrumentationService>();
            mockInstrumentationService.Expect(p => p.Write(Arg<IProfilingEntry>.Is.Anything));
            mockInstrumentationService.Expect(p => p.CreateLogEntry(Arg<LogEntryTemplate>.Is.Anything)).Return(null)
                .WhenCalled(_ =>
                {
                    var template = (LogEntryTemplate)_.Arguments[0];
                    var ret = new LogEntry(template);
                    _.ReturnValue = ret;
                });

            mockInstrumentationService
                .Expect(p => p.CreateProfilingEntry(Arg<TraceEventType>.Is.Anything, Arg<int>.Is.Anything, Arg<string>.Is.Anything, Arg<int>.Is.Anything))
                .Return(mockProfilingEntry);

            //--- Mock IAuthorizationService
            var mockAuthorizationService = MockRepository.GenerateStub<IAuthorizationService>();

            //--- Mock Luxoft.NDS2.Server.Services.Managers.Query.IQueryProvider
            var mockQueryProvider = MockRepository.GenerateStub<Luxoft.NDS2.Server.Services.Managers.Query.IQueryProvider>();

            //--- Mock IReadOnlyServiceCollection (ServiceContext)
            var mockServiceContext = MockRepository.GenerateStub<IReadOnlyServiceCollection>();

            //--- Mock CatalogService
            var mockCatalogService = MockRepository.GenerateStub<ICatalogService>();

            mockServiceContext.Expect(p => p.Get(typeof(IConfigurationDataService))).Return(mockConfiguraionService);
            mockServiceContext.Expect(p => p.Get(typeof(IInstrumentationService))).Return(mockInstrumentationService);
            mockServiceContext.Expect(p => p.Get(typeof(IAuthorizationService))).Return(mockAuthorizationService);
            mockServiceContext.Expect(p => p.Get(typeof(IQueryProvider))).Return(mockQueryProvider);

            mockServiceContext.Expect(p => p.Get<IConfigurationDataService>()).Return(mockConfiguraionService);
            mockServiceContext.Expect(p => p.Get<IInstrumentationService>()).Return(mockInstrumentationService);
            mockServiceContext.Expect(p => p.Get<IAuthorizationService>()).Return(mockAuthorizationService);
            mockServiceContext.Expect(p => p.Get<Luxoft.NDS2.Server.Services.Managers.Query.IQueryProvider>()).Return(mockQueryProvider);

            return mockServiceContext;
        }

        public static CatalogAddress CreateCatalogAddress(string xsdShemaName)
        {
            CatalogAddress xsdSchemaAddres = new CatalogAddress(
                CatalogAddressSchemas.LogicalCatalog,
                "RoleCatalog",
                "NDS2Subsystem",
                "FileSystems",
                string.Format("XMLSchemas/{0}", xsdShemaName));

            return xsdSchemaAddres;
        }

        public static IXmlValidator CreateMockXmlValidator(string xsdShemaName)
        {
            var mockXmlValidator = MockRepository.GenerateStub<IXmlValidator>();

            mockXmlValidator.Expect(p => p.Validate(Arg<string>.Is.Anything, Arg<CatalogAddress>.Is.Equal(CreateCatalogAddress(xsdShemaName))))
                .WhenCalled(_ =>
                {
                    var xmlBody = (string)_.Arguments[0];
                    var catalogAddress = (CatalogAddress)_.Arguments[1];

                    Stream streamXsd = new FileStream(string.Format("..\\UnitTests\\Reclaim\\{0}", xsdShemaName), FileMode.Open);

                    XmlReader reader = XmlReader.Create(streamXsd);

                    var xmlDocument = new XmlDocument();
                    xmlDocument.LoadXml(xmlBody);

                    xmlDocument.Schemas.Add(null, reader);

                    xmlDocument.Validate(null);
                });

            return mockXmlValidator;
        }

        public static IPeriodDictionary CreatePeriodDictionary()
        {
            var taxPeriods = new List<DictTaxPeriod>()
            {
                new DictTaxPeriod() { Code = "01", Description = "январь", Quarter = 1, IsMainInQuarter = false, Month = 1 },
                new DictTaxPeriod() { Code = "02", Description = "февраль", Quarter = 1, IsMainInQuarter = false, Month = 2 },
                new DictTaxPeriod() { Code = "03", Description = "март", Quarter = 1, IsMainInQuarter = false, Month = 3 },
                new DictTaxPeriod() { Code = "04", Description = "апрель", Quarter = 2, IsMainInQuarter = false, Month = 4 },
                new DictTaxPeriod() { Code = "05", Description = "май", Quarter = 2, IsMainInQuarter = false, Month = 5 },
                new DictTaxPeriod() { Code = "06", Description = "июнь", Quarter = 2, IsMainInQuarter = false, Month = 6 },
                new DictTaxPeriod() { Code = "07", Description = "июль", Quarter = 3, IsMainInQuarter = false, Month = 7 },
                new DictTaxPeriod() { Code = "08", Description = "август", Quarter = 3, IsMainInQuarter = false, Month = 8 },
                new DictTaxPeriod() { Code = "09", Description = "сентябрь", Quarter = 3, IsMainInQuarter = false, Month = 9 },
                new DictTaxPeriod() { Code = "10", Description = "октябрь", Quarter = 4, IsMainInQuarter = false, Month = 10 },
                new DictTaxPeriod() { Code = "11", Description = "ноябрь", Quarter = 4, IsMainInQuarter = false, Month = 11 },
                new DictTaxPeriod() { Code = "12", Description = "декабрь", Quarter = 4, IsMainInQuarter = false, Month = 12 },
                new DictTaxPeriod() { Code = "21", Description = "1 кв.", Quarter = 1, IsMainInQuarter = true, Month = 0 },
                new DictTaxPeriod() { Code = "22", Description = "2 кв.", Quarter = 2, IsMainInQuarter = true, Month = 0 },
                new DictTaxPeriod() { Code = "23", Description = "3 кв.", Quarter = 3, IsMainInQuarter = true, Month = 0 },
                new DictTaxPeriod() { Code = "24", Description = "4 кв.", Quarter = 4, IsMainInQuarter = true, Month = 0 },
                new DictTaxPeriod() { Code = "51", Description = "1 кв.", Quarter = 1, IsMainInQuarter = true, Month = 0 },
                new DictTaxPeriod() { Code = "54", Description = "2 кв.", Quarter = 2, IsMainInQuarter = true, Month = 0 },
                new DictTaxPeriod() { Code = "55", Description = "3 кв.", Quarter = 3, IsMainInQuarter = true, Month = 0 },
                new DictTaxPeriod() { Code = "56", Description = "4 кв.", Quarter = 4, IsMainInQuarter = true, Month = 0 },
                new DictTaxPeriod() { Code = "71", Description = "январь", Quarter = 1, IsMainInQuarter = false, Month = 1 },
                new DictTaxPeriod() { Code = "72", Description = "февраль", Quarter = 1, IsMainInQuarter = false, Month = 2 },
                new DictTaxPeriod() { Code = "73", Description = "март", Quarter = 1, IsMainInQuarter = false, Month = 3 },
                new DictTaxPeriod() { Code = "74", Description = "апрель", Quarter = 2, IsMainInQuarter = false, Month = 4 },
                new DictTaxPeriod() { Code = "75", Description = "май", Quarter = 2, IsMainInQuarter = false, Month = 5 },
                new DictTaxPeriod() { Code = "76", Description = "июнь", Quarter = 2, IsMainInQuarter = false, Month = 6 },
                new DictTaxPeriod() { Code = "77", Description = "июль", Quarter = 3, IsMainInQuarter = false, Month = 7 },
                new DictTaxPeriod() { Code = "78", Description = "август", Quarter = 3, IsMainInQuarter = false, Month = 8 },
                new DictTaxPeriod() { Code = "79", Description = "сентябрь", Quarter = 3, IsMainInQuarter = false, Month = 9 },
                new DictTaxPeriod() { Code = "80", Description = "октябрь", Quarter = 4, IsMainInQuarter = false, Month = 10 },
                new DictTaxPeriod() { Code = "81", Description = "ноябрь", Quarter = 4, IsMainInQuarter = false, Month = 11 },
                new DictTaxPeriod() { Code = "82", Description = "декабрь", Quarter = 4, IsMainInQuarter = false, Month = 12 }
            };

            ConcurrentDictionary<string, PeriodBase> periods = new ConcurrentDictionary<string, PeriodBase>();

            foreach (var item in taxPeriods)
            {
                if (item.Month > 0)
                    periods.TryAdd(item.Code, new PeriodMonth(item.Month));
                else
                    periods.TryAdd(item.Code, new PeriodQuarter(item.Quarter));
            }

            var mockPeriodDictionary = MockRepository.GenerateStub<IPeriodDictionary>();

            mockPeriodDictionary.Expect(p => p.Get(Arg<string>.Is.Anything)).Return(null)
                .WhenCalled(_ =>
                {
                    var periodCode = (string)_.Arguments[0];

                    PeriodBase periodBase = null;

                    periods.TryGetValue(periodCode, out periodBase);

                    _.ReturnValue = periodBase;
                });

            return mockPeriodDictionary;
        }
    }
}
