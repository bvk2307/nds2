﻿using Luxoft.NDS2.Server.TaskScheduler.Reclaim.Tasks.TaskParts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Server.TaskScheduler.Test.Reclaim
{
    [TestClass]
    public class KppEffectiveComparatorTest
    {
        [TestMethod]
        public void KppEffectiveComparatorGeneralTest()
        {
            Assert.IsTrue(KppEffectiveComparator.Compare("111111111", "111111111"));

            Assert.IsTrue(KppEffectiveComparator.Compare(String.Empty, "111111111"));

            Assert.IsTrue(KppEffectiveComparator.Compare(null, "111111111"));

            Assert.IsTrue(KppEffectiveComparator.Compare("111111111", String.Empty));

            Assert.IsTrue(KppEffectiveComparator.Compare("111111111", null));

            Assert.IsTrue(KppEffectiveComparator.Compare(null, null));

            Assert.IsTrue(KppEffectiveComparator.Compare(String.Empty, String.Empty));

            Assert.IsTrue(KppEffectiveComparator.Compare(null, String.Empty));

            Assert.IsTrue(KppEffectiveComparator.Compare(String.Empty, null));

            Assert.IsFalse(KppEffectiveComparator.Compare("111111112", "111111111"));
        }
    }
}
