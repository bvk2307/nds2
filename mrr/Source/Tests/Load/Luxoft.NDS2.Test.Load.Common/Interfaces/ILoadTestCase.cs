﻿using System.Collections;
using System.Collections.Generic;

namespace Luxoft.NDS2.Test.Load.Common.Interfaces
{
    /// <summary>
    /// Контракт тестового задания
    /// </summary>
    public interface ILoadTestCase
    {
        /// <summary>
        /// запуск теста
        /// </summary>
        /// <param name="context">контест окружения</param>
        void Execute(TestExecutionContext context);

        /// <summary>
        /// Возвращает информацию теста
        /// </summary>
        /// <returns></returns>
        string GetInfo();
    }
}
