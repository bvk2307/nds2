﻿using System.Collections.Generic;

namespace Luxoft.NDS2.Test.Load.Common.Base
{
    /// <summary>
    /// абстрактный класс аргумента теста
    /// </summary>
    public abstract class TestCaseArgument
    {
        protected readonly string _argName = null;

        public string ArgName { get { return _argName; } }

        protected TestCaseArgument(string argName)
        {
            _argName = argName;
        }

        /// <summary>
        /// Имя аргумента
        /// </summary>
        public  string ArgumentName { get { return _argName; } }

        /// <summary>
        /// признак обязательности аргумнта
        /// </summary>
        public abstract bool Required { get; set; }

        /// <summary>
        /// Чтение параметров арумента
        /// </summary>
        /// <param name="args"></param>
        public abstract void ReadArgumentValues(Queue<string> args);


        /// <summary>
        /// Получение информации об аргументе (список параметров, формат, последовательность)
        /// </summary>
        /// <returns></returns>
        public abstract string GetArgumentInfo();

    }
}
