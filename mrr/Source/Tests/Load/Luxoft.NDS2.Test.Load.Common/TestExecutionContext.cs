﻿using System.Collections.Generic;

namespace Luxoft.NDS2.Test.Load.Common
{
    public class TestExecutionContext
    {
        public Queue<string> Arguments { get; set; }
    }
}
