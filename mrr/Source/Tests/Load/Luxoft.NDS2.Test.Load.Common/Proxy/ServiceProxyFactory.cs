﻿namespace Luxoft.NDS2.Test.Load.Common.Proxy
{
    public class ServiceProxyFactory
    {
        public static T CreateServiceProxy<T>() where T : class
        {
            return (T)(new ServiceProxy(typeof (T))).GetTransparentProxy();
        }
    }
}
