﻿using System;
using System.IO;
using System.Runtime.Remoting.Messaging;
using System.Runtime.Remoting.Proxies;
using System.Runtime.Serialization.Formatters.Binary;
using System.ServiceModel;
using System.ServiceModel.Channels;
using CommonComponents.Infra.Communication;
using CommonComponents.Infra.Communication.Strategies;
using CommonComponents.Infra.Communication.Transport.WcfServer;
using Luxoft.NDS2.Test.Load.Common.Providers;

namespace Luxoft.NDS2.Test.Load.Common.Proxy
{
    public sealed class ServiceProxy : RealProxy 
    {
        private Type ProxiedType { get; set; }

        public ServiceProxy(Type pType)
            : base(pType)
        {
            ProxiedType = pType;
        }

        #region Overrides of RealProxy

        /// <summary>
        /// Обработка прокси вызова
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        public override IMessage Invoke(IMessage msg)
        {
            var remotingCallMessage = msg as IMethodCallMessage;
            var callMessage = new CallMethodMessage(ProxiedType, remotingCallMessage);
            var requestBytes = SerializeMessage(callMessage);
            var response = SubmitRequest(requestBytes);
            var returnMessage = DeserializeMessage(response);
            var returnArgs = new object[remotingCallMessage.ArgCount];

            var parameters = remotingCallMessage.MethodBase.GetParameters();
            for (int i = 0, j = 0; i < parameters.Length; i++)
            {
                if (parameters[i].ParameterType.IsByRef)
                    returnArgs[i] = returnMessage.OutArgumentList[j++];
            }

            return new ReturnMessage(
                        returnMessage.ReturnValue,
                        returnArgs,
                        returnArgs.Length,
                        remotingCallMessage.LogicalCallContext,
                        remotingCallMessage);
        }

        /// <summary>
        /// Передача сообщения на сервис
        /// </summary>
        /// <param name="requestBytes"></param>
        /// <returns></returns>
        private byte[] SubmitRequest(byte[] requestBytes)
        {
            var cb = new CustomBinding("ServiceBinding");

            var endpoint = new EndpointAddress(LocalConfigurationProvider.ServiceEndpoint);

            var cf = new ChannelFactory<IWcfTransportService>(cb, endpoint);
            cf.Endpoint.Behaviors.Add(new MessageCompressionEndpointBehavior());
            var c = cf.CreateChannel();
            long sd;

            var result = c.SubmitMessage(requestBytes, out sd);

            cf.Close();

            return result;
        }

        /// <summary>
        /// Сериализация сообщения
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        private byte[] SerializeMessage(CallMethodMessage message)
        {
            var serializer = new BinaryFormatter();
            byte[] data;

            using (var ms = new MemoryStream())
            {
                serializer.Serialize(ms, message);
                data = ms.ToArray();
            }

            return data;
        }

        /// <summary>
        /// Десереализация сообщения
        /// </summary>
        /// <param name="data">байты данных</param>
        /// <returns></returns>
        private MethodCallReturnMessage DeserializeMessage(byte[] data)
        {
            var serializer2 = new BinaryFormatter();
            using (var ms2 = new MemoryStream(data, false))
            {
                return (MethodCallReturnMessage)serializer2.Deserialize(ms2);
            }
        }

        #endregion
    }
}
