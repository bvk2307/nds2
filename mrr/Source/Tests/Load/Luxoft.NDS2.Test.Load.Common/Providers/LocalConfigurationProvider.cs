﻿using System.Configuration;

namespace Luxoft.NDS2.Test.Load.Common.Providers
{
    /// <summary>
    /// Провайдер конфигурации
    /// </summary>
    public sealed class LocalConfigurationProvider
    {
        private static string _serviceEndpoint;
        /// <summary>
        /// Полный адрес сервиса
        /// </summary>
        public static string ServiceEndpoint
        {
            get
            {
                if (string.IsNullOrEmpty(_serviceEndpoint))
                {
                    _serviceEndpoint = ConfigurationManager.AppSettings["serviceEndpoint"];
                }

                return _serviceEndpoint;
            }
        }
    }
}
