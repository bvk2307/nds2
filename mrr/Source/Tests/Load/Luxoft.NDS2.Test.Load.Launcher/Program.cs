﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Luxoft.NDS2.Test.Load.Common;
using Luxoft.NDS2.Test.Load.Common.Interfaces;

namespace Luxoft.NDS2.Test.Load.Launcher
{
    class Program
    {
        static Dictionary<string, ILoadTestCase> _testCases = new Dictionary<string, ILoadTestCase>();
 
        static void Main(string[] args)
        {

            #region стаб коллекции тест кейсов, в дальнейшем надо бы загружать сборки и регистрировать тесты в рантайме

            _testCases.Add(Luxoft.NDS2.Test.Load.TestCaseLibrary.TestCases.Analyst.Case1.Name, new Luxoft.NDS2.Test.Load.TestCaseLibrary.TestCases.Analyst.Case1());
            _testCases.Add(Luxoft.NDS2.Test.Load.TestCaseLibrary.TestCases.Analyst.Case2.Name, new Luxoft.NDS2.Test.Load.TestCaseLibrary.TestCases.Analyst.Case2());
            _testCases.Add(Luxoft.NDS2.Test.Load.TestCaseLibrary.TestCases.Analyst.DeclarationCardTest.Name, new Luxoft.NDS2.Test.Load.TestCaseLibrary.TestCases.Analyst.DeclarationCardTest());

            _testCases.Add(Luxoft.NDS2.Test.Load.TestCaseLibrary.TestCases.Approver.Case1.Name, new Luxoft.NDS2.Test.Load.TestCaseLibrary.TestCases.Approver.Case1());
            _testCases.Add(Luxoft.NDS2.Test.Load.TestCaseLibrary.TestCases.Approver.Case2.Name, new Luxoft.NDS2.Test.Load.TestCaseLibrary.TestCases.Approver.Case2());

            _testCases.Add(Luxoft.NDS2.Test.Load.TestCaseLibrary.TestCases.Inspector.Case_1.Name, new Luxoft.NDS2.Test.Load.TestCaseLibrary.TestCases.Inspector.Case_1());
            _testCases.Add(Luxoft.NDS2.Test.Load.TestCaseLibrary.TestCases.Inspector.Case_2.Name, new Luxoft.NDS2.Test.Load.TestCaseLibrary.TestCases.Inspector.Case_2());
            _testCases.Add(Luxoft.NDS2.Test.Load.TestCaseLibrary.TestCases.Inspector.Case_3.Name, new Luxoft.NDS2.Test.Load.TestCaseLibrary.TestCases.Inspector.Case_3());

            _testCases.Add(Luxoft.NDS2.Test.Load.TestCaseLibrary.TestCases.Samples.Sample1.Name, new Luxoft.NDS2.Test.Load.TestCaseLibrary.TestCases.Samples.Sample1());
            _testCases.Add(Luxoft.NDS2.Test.Load.TestCaseLibrary.TestCases.PerformanceImprovement.DeclarationListImprovement.Name, new Luxoft.NDS2.Test.Load.TestCaseLibrary.TestCases.PerformanceImprovement.DeclarationListImprovement());
            _testCases.Add(Luxoft.NDS2.Test.Load.TestCaseLibrary.TestCases.Analyst.InvoiceCall.Name, new Luxoft.NDS2.Test.Load.TestCaseLibrary.TestCases.Analyst.InvoiceCall());
            
            #endregion  

            if (args == null || args.Length == 0)
            {
                Console.WriteLine("Luxoft.NDS2.Test.Load.Launcher.exe test_name args");
                Console.WriteLine("Luxoft.NDS2.Test.Load.Launcher.exe ? test_name");
                foreach (var key in _testCases.Keys)
                {
                    Console.WriteLine("Luxoft.NDS2.Test.Load.Launcher.exe {0} -sono 1234", key);
                }
                return;
            }

            ILoadTestCase tCase;
            var argsInternal = new Queue<string>();

            foreach (var s in args)
            {
                argsInternal.Enqueue(s);
            }

            var firstArg = argsInternal.Dequeue();

            if(firstArg.Equals("/?"))
            {
                var testName = argsInternal.Dequeue();
                
                if(_testCases.TryGetValue(testName, out tCase))
                {
                    Console.WriteLine("Information for test '{0}'", testName);
                    Console.WriteLine(tCase.GetInfo());
                    return;
                }

                Console.WriteLine("Test '{0}' not registered", testName);

                return;
            }

            Stopwatch sw = new Stopwatch();
            if (_testCases.ContainsKey(firstArg))
            {
                tCase = _testCases[firstArg];
                Console.WriteLine("Starting '{0}' test at {1}.", firstArg, DateTime.Now);

                try
                {
                    tCase.Execute(new TestExecutionContext() { Arguments = argsInternal });

                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error occurs during test execution: {0}", ex);

                    Environment.Exit(1);
                }

                Console.WriteLine("Test ended at: {0}", DateTime.Now);
            }
            else
            {
                Console.WriteLine("Test '{0}' not registered.", firstArg);
            }

        }
    }
}
