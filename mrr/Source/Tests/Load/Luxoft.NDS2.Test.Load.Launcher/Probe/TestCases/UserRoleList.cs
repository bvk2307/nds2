﻿using System;
using System.Threading;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Test.Load.Common.Proxy;

namespace Luxoft.NDS2.Test.Load.Launcher.Probe.TestCases
{
    public class UserRoleList : ILoadTestCase
    {
        public void Execute(ExecutionContext context)
        {
            var service = ServiceProxyFactory.CreateServiceProxy<ISecurityService>();
            var rolesList = service.GetSubsystemRoles();
            Console.WriteLine("Received {0} roles for {1}", rolesList.Result.Count, Thread.CurrentPrincipal.Identity.Name);

            foreach (var role in rolesList.Result)
            {
                Console.WriteLine(role);
            }
        }
    }
}
