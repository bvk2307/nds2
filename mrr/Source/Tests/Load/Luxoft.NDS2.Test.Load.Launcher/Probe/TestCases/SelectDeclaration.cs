﻿using System;
using System.DirectoryServices.AccountManagement;
using System.Security.Principal;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Test.Load.Common.Proxy;

namespace Luxoft.NDS2.Test.Load.Launcher.Probe.TestCases
{
    public class SelectDeclaration : ILoadTestCase
    {
        private SelectDeclarationConfiguration _configuration;

        public void Execute(ExecutionContext context)
        {
            _configuration = new SelectDeclarationConfiguration(context);
            if (_configuration.ImpersonateUser)
            {

                using (var imp = new Impersonation(_configuration.DomainName,_configuration.UserName, _configuration.UserPass))
                {
                    ExecuteInternal(context);
                }
            }
        }

        private void ExecuteInternal(ExecutionContext context)
        {
            var service = ServiceProxyFactory.CreateServiceProxy<IDeclarationsDataService>();

            for (int i = 0; i < _configuration.CyclesCount; i++)
            {
                var r = service.SelectDeclarations(QueryConditions.Empty);

                if (r.Status == ResultStatus.Success)
                {
                    if (r.Result == null)
                    {
                        Console.WriteLine("Result is empty");
                    }
                    else
                    {
                        if (r.Result.Rows == null)
                        {
                            Console.WriteLine("Result.Rows is empty");
                        }
                        else
                        {
                            Console.WriteLine("Received {0} declarations", r.Result.Rows.Count);
                        }
                    }
                }
                else
                {
                    Console.WriteLine("NDS2 Server error: {0}", r.Message);
                }

            }
        }

        private class SelectDeclarationConfiguration
        {

            public int DelaysInMilisec { get; set; }

            public int CyclesCount { get; set; }

            public bool ImpersonateUser { get; set; }

            public string UserName { get; set; }

            public string UserPass { get; set; }

            public string DomainName { get; set; }


            public SelectDeclarationConfiguration(ExecutionContext context)
            {
                //Defaults
                CyclesCount = 1;
                DelaysInMilisec = 100;

                while (context.Arguments.Count > 0)
                {
                    var param = context.Arguments.Dequeue();
                    switch (param)
                    {
                        case "-cycles":
                            CyclesCount = GetSafeValue(context.Arguments.Dequeue(), 1);
                            break;

                        case "-delay":
                            DelaysInMilisec = GetSafeValue(context.Arguments.Dequeue(), 100);
                            break;

                        case "-impersonate":
                            this.ImpersonateUser = true;
                            this.DomainName = context.Arguments.Dequeue();
                            this.UserName = context.Arguments.Dequeue();
                            this.UserPass = context.Arguments.Dequeue();
                            break;

                        default:
                            break;
                    }
                }
            }

            private static int GetSafeValue(string input, int defaultValue = 1)
            {
                int val;
                if (!int.TryParse(input, out val))
                {
                    val = defaultValue;
                }

                return val;
            }
        }
    }
}
