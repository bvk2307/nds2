﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Luxoft.NDS2.Test.Load.Launcher.Probe
{
    public class ExecutionContext
    {
        public Queue<string> Arguments { get; set; }

        public List<object> ServiceCollection { get; set; }
    }
}
