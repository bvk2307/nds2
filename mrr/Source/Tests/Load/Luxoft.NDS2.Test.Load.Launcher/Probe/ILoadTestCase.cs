﻿namespace Luxoft.NDS2.Test.Load.Launcher.Probe
{
    public interface ILoadTestCase
    {
        void Execute(ExecutionContext context);
    }
}
