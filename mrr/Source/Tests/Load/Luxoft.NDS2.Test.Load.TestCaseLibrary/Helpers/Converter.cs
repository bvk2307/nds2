﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Luxoft.NDS2.Test.Load.TestCaseLibrary.Helpers
{
    public class Converter
    {
        public static int ToInt(string input, int defaultValue = 0)
        {
            int i;

            if(!int.TryParse(input, out i))
            {
                i = defaultValue;
            }

            return i;
        }

    }
}
