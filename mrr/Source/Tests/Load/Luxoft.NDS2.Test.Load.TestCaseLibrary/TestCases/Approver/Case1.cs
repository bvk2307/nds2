﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Test.Load.Common;
using Luxoft.NDS2.Test.Load.Common.Proxy;
using Luxoft.NDS2.Test.Load.Launcher.Probe;
using Luxoft.NDS2.Test.Load.TestCaseLibrary.Base.Arguments;

namespace Luxoft.NDS2.Test.Load.TestCaseLibrary.TestCases.Approver
{
    [Description(@"Кейс 1. Работа с автовыборкой. Согласующий получил 5 автовыборок для согласования, каждая автовыборка содержит до 1 млн расхождений

•	один раз обновляет список выборок 
•	с задержкой 1 мин открывает выборку
•	с задержкой 30 сек перелистывает список расхождений на две страницы вперед
•	с задержкой 5 мин нажимает кнопку отправляет выборку на доработку
")]
    public class Case1 : TestCaseBase
    {
        public const string Name = "approverCase1";

        #region Constants

        public const string ARG_SONO_CODE = "-sono";
        public const string ARG_IMPERSONATE = "-impersonate";

        public const string ARG_ITERATION_TIME = "-iterationTime";
        public const string ARG_ITERATIONS_COUNT = "-iterationsCount";

        public const string ARG_DELAY_OPEN_SELECTION = "-delayOpenSelection";
        public const string ARG_DELAY_LIST_DISCREPANCIES = "-delayListDiscrepancies";
        public const string ARG_DELAY_SEND_FOR_CORRECTION = "-delaySendForCorrection";
        //public const string ARG_NUM_OF_DISCREP_TO_CHECK = "-numCheckDiscrep";

        private const int SELECTION_STATUS_FOR_APPROVAL = 3;

        #endregion

        #region Fields

        private string _sono = string.Empty;
        private int _iterationTime = 0;
        private int _iterationsCount = 0;
        private int _delayOpenSelection = 0;
        private int _delayListDiscrepancies = 0;
        private int _delaySendForCorrection = 0;



        #endregion

        public Case1()
        {
            _arguments.Add(ARG_SONO_CODE, new SingleStringArgument(ARG_SONO_CODE) { Description = "{0} sono_code \t\t\tномер инспекции по которой будет проиходить поиск деклараций", Required = true });

            _arguments.Add(ARG_ITERATION_TIME, new SingleNumericArgument(ARG_ITERATION_TIME) { DefaultValue = 500 });
            _arguments.Add(ARG_ITERATIONS_COUNT, new SingleNumericArgument(ARG_ITERATIONS_COUNT) { DefaultValue = 5 });

            _arguments.Add(ARG_DELAY_OPEN_SELECTION, new SingleNumericArgument(ARG_DELAY_OPEN_SELECTION) { DefaultValue = 500 });
            _arguments.Add(ARG_DELAY_LIST_DISCREPANCIES, new SingleNumericArgument(ARG_DELAY_LIST_DISCREPANCIES) { DefaultValue = 600 });
            _arguments.Add(ARG_DELAY_SEND_FOR_CORRECTION, new SingleNumericArgument(ARG_DELAY_SEND_FOR_CORRECTION) { DefaultValue = 700 });

            _arguments.Add(ARG_IMPERSONATE, new ImpersonateArgument(ARG_IMPERSONATE));            
        }

        #region Implementation of ILoadTestCase

        public override void Execute(TestExecutionContext context)
        {
            base.LoadArguments(context.Arguments);

            _sono = GetStringArgValue(ARG_SONO_CODE);
            _iterationTime = GetIntArgValue(ARG_ITERATION_TIME);
            _iterationsCount = GetIntArgValue(ARG_ITERATIONS_COUNT);
            _delayOpenSelection = GetIntArgValue(ARG_DELAY_OPEN_SELECTION);
            _delayListDiscrepancies = GetIntArgValue(ARG_DELAY_LIST_DISCREPANCIES);
            _delaySendForCorrection = GetIntArgValue(ARG_DELAY_SEND_FOR_CORRECTION);



            var impersonateArg = (ImpersonateArgument)_arguments[ARG_IMPERSONATE];

            if (impersonateArg.IsInitialized)
            {
                using (new Impersonation(impersonateArg.DomainName, impersonateArg.UserName, impersonateArg.Password))
                {
                    ExecuteInternal();
                }
            }
            else
            {
                ExecuteInternal();
            }

            WriteLog("Конец");
        }

        public override string GetInfo()
        {
            return string.Empty;
        }

        #endregion

        private void ExecuteInternal()
        {
            foreach (var sono in GetSonos())
            {
                WriteLog("Processing sono {0}", ConsoleColor.White, sono);
                ProcessSono(sono);
            }
        }

        private void ProcessSono(string sono)
        {
            try
            {
                int stepId = 1;
                
                WriteLog("Step {0}: Refreshing selection list", ConsoleColor.Green, stepId++);
                RefreshSelectionsList();

                Sleep(_delayOpenSelection, "waiting before open selection {0} ", _delayOpenSelection);
                WriteLog("Step {0}: Getting selection", ConsoleColor.Green, stepId++);
                WriteLog("Gettin selection object");
                var sel = GetSelection(sono);
                
                if (sel == null)
                {
                    return;
                }

                var discrepQc = QueryConditions.Empty;
                var declQc = QueryConditions.Empty;

                WriteLog("Loading declarations");
                var declarations = LoadDeclarations(sel.Id, declQc);
                WriteLog("declarations - {0}", declarations.Rows.Count);

                WriteLog("Loading discrepancies");
                var discrepancies = LoadDiscrepancies(sel.Id, discrepQc);
                WriteLog("discrepancies - {0}", discrepancies.Rows.Count);

                Sleep(_delayListDiscrepancies, "waiting before skip 2 discrepancies page {0} ", _delayListDiscrepancies);
                WriteLog("Step {0}: skippin' 2 pages in deiscrepancies", ConsoleColor.Green, stepId++);
                WriteLog("Loading discrepancies");
                discrepQc.PageIndex = 2;
                discrepancies = LoadDiscrepancies(sel.Id, discrepQc);
                discrepQc.PageIndex = 3;
                discrepancies = LoadDiscrepancies(sel.Id, discrepQc);

                Sleep(_delaySendForCorrection, "waiting before send for correction {0} ", _delaySendForCorrection);
                WriteLog("Step {0}: sending for correction", ConsoleColor.Green, stepId++);
                SendSelectionToCorrection(sel);
                WriteLog("Done");
            }
            catch (Exception ex)
            {
                Guid errId = Guid.NewGuid();
                WriteLog("Error - {0}", errId);
                WriteError("{0} Error occured during sono processing: {1}", errId, ex);
            }
        }

        #region helpers

        private void SendSelectionToCorrection(Selection s)
        {
            var serv = ServiceProxyFactory.CreateServiceProxy<ISelectionService>();
            serv.SendBackForCorrections(s, new List<long>());
        }

        private DeclarationPageResult LoadDeclarations(long reqId, QueryConditions c)
        {
            DeclarationPageResult res = null;

            var serv = ServiceProxyFactory.CreateServiceProxy<ISelectionsDataService>();
            var response = serv.SearchDeclarations(reqId, c);

            res = ExtractValueOrDie(response);

            return res;
        }


        private PageResult<SelectionDiscrepancy> LoadDiscrepancies(long reqId, QueryConditions c)
        {
            PageResult<SelectionDiscrepancy> res = null;

            var serv = ServiceProxyFactory.CreateServiceProxy<ISelectionService>();
            var response = serv.SearchDiscrepancies(reqId, true, c);

            res = ExtractValueOrDie(response);

            return res;
        }

        private void RefreshSelectionsList()
        {
            var serv = ServiceProxyFactory.CreateServiceProxy<ISelectionService>();
            QueryConditions qc = QueryConditions.Empty;

            var data = ExtractValueOrDie(serv.SelectSelections(qc));
        }

        private Selection GetSelection(string sono)
        {
            var region = sono.Substring(0, 2);
            var serv = ServiceProxyFactory.CreateServiceProxy<ISelectionService>();
            QueryConditions qc = QueryConditions.Empty;
            /*
            qc.Filter.Add(FilterColumn(
                TypeHelper<Selection>.GetMemberName(t=>t.RegionNames), 
                region, ColumnFilter.FilterComparisionOperator.Like));
            */
            qc.Filter.Add(FilterColumn(
                TypeHelper<Selection>.GetMemberName(t=>t.Status), 
                SELECTION_STATUS_FOR_APPROVAL));

            var data = ExtractValueOrDie(serv.SelectSelections(qc)); 
           
            if(data.Rows == null || data.Rows.Count == 0)
            {
                WriteLog("empty selection list for {0}", region);
                WriteError("empty selection list for {0}", region);
                //throw new ApplicationException(string.Format("empty selection list for {0}", region));
                return null;
            }


            return data.Rows.First();
        }

        private void GetFirstSelectionsList(string sono)
        {
            var serv = ServiceProxyFactory.CreateServiceProxy<ISelectionService>();
            QueryConditions qc = QueryConditions.Empty;

            serv.SelectSelections(qc);
        }

        private IEnumerable<string> GetSonos()
        {
            if (_sono.Contains(","))
            {
                return _sono.Split(',');

            }

            return new[] { _sono };
        }

        #endregion
    }
}
