﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Business.TaxPayer;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Test.Load.Common;
using Luxoft.NDS2.Test.Load.Common.Proxy;
using Luxoft.NDS2.Test.Load.Launcher.Probe;
using Luxoft.NDS2.Test.Load.TestCaseLibrary.Base.Arguments;

namespace Luxoft.NDS2.Test.Load.TestCaseLibrary.TestCases.Approver
{
    [Description(@"Кейс 1. Работа с автовыборкой. Согласующий получил 5 автовыборок для согласования, каждая автовыборка содержит до 1 млн расхождений
•	один раз обновляет список выборок 
•	с задержкой 1 мин открывает выборку
•	с задержкой 30 сек перелистывает список расхождений на две страницы вперед
•	с задержкой 5 мин нажимает кнопку отправляет выборку на доработку
")]
    public class Case2 : TestCaseBase
    {
        public const string Name = "approverCase2";

        #region Constants

        public const string ARG_SONO_CODE = "-sono";
        public const string ARG_IMPERSONATE = "-impersonate";

        public const string ARG_ITERATION_TIME = "-iterationTime";
        public const string ARG_ITERATIONS_COUNT = "-iterationsCount";

        public const string ARG_DELAY_OPEN_SELECTION = "-delayOpenSelection";
        public const string ARG_DELAY_OPEN_TAXPAYER = "-delayOpenTaxPayer";
        public const string ARG_DELAY_SEND_FOR_CORRECTION = "-delaySendForApproval";

        private const int SELECTION_STATUS_FOR_APPROVAL = 3;

        #endregion

        #region Fields

        private string _sono = string.Empty;
        private int _iterationTime = 0;
        private int _iterationsCount = 0;
        private int _delayOpenSelection = 0;
        private int _delayOpenTaxPayer = 0;
        private int _delaySendForCorrection = 0;



        #endregion

        public Case2()
        {
            _arguments.Add(ARG_SONO_CODE, new SingleStringArgument(ARG_SONO_CODE) { Description = "{0} sono_code \t\t\tномер инспекции по которой будет проиходить поиск деклараций", Required = true });

            _arguments.Add(ARG_ITERATION_TIME, new SingleNumericArgument(ARG_ITERATION_TIME) { DefaultValue = 500 });
            _arguments.Add(ARG_ITERATIONS_COUNT, new SingleNumericArgument(ARG_ITERATIONS_COUNT) { DefaultValue = 5 });

            _arguments.Add(ARG_DELAY_OPEN_SELECTION, new SingleNumericArgument(ARG_DELAY_OPEN_SELECTION) { DefaultValue = 500 });
            _arguments.Add(ARG_DELAY_OPEN_TAXPAYER, new SingleNumericArgument(ARG_DELAY_OPEN_TAXPAYER) { DefaultValue = 600 });
            _arguments.Add(ARG_DELAY_SEND_FOR_CORRECTION, new SingleNumericArgument(ARG_DELAY_SEND_FOR_CORRECTION) { DefaultValue = 700 });

            _arguments.Add(ARG_IMPERSONATE, new ImpersonateArgument(ARG_IMPERSONATE));            
        }

        #region Implementation of ILoadTestCase

        public override void Execute(TestExecutionContext context)
        {
            base.LoadArguments(context.Arguments);

            _sono = GetStringArgValue(ARG_SONO_CODE);
            _iterationTime = GetIntArgValue(ARG_ITERATION_TIME);
            _iterationsCount = GetIntArgValue(ARG_ITERATIONS_COUNT);
            _delayOpenSelection = GetIntArgValue(ARG_DELAY_OPEN_SELECTION);
            _delayOpenTaxPayer = GetIntArgValue(ARG_DELAY_OPEN_TAXPAYER);
            _delaySendForCorrection = GetIntArgValue(ARG_DELAY_SEND_FOR_CORRECTION);



            var impersonateArg = (ImpersonateArgument)_arguments[ARG_IMPERSONATE];

            if (impersonateArg.IsInitialized)
            {
                using (new Impersonation(impersonateArg.DomainName, impersonateArg.UserName, impersonateArg.Password))
                {
                    ExecuteInternal();
                }
            }
            else
            {
                ExecuteInternal();
            }

            WriteLog("Конец");
        }

        public override string GetInfo()
        {
            return string.Empty;
        }

        #endregion

        private void ExecuteInternal()
        {
            foreach (var sono in GetSonos())
            {
                WriteLog("Processing sono {0}", ConsoleColor.White, sono);
                ProcessSono(sono);
            }
        }

        private void ProcessSono(string sono)
        {
            try
            {
                int stepId = 1;
                
                WriteLog("Step {0}: Refreshing selection list", ConsoleColor.Green, stepId++);
                RefreshSelectionsList();

                Sleep(_delayOpenSelection, "waiting before open selection {0} ", _delayOpenSelection);
                WriteLog("Step {0}: Refreshing selection list", ConsoleColor.Green, stepId++);
                WriteLog("Gettin selection object");
                var sel = GetSelection(sono);
                
                if(sel == null)
                {
                    return;
                }
                
                var discrepQc = QueryConditions.Empty;
                var declQc = QueryConditions.Empty;

                WriteLog("Loading declarations");
                var declarations = LoadDeclarations(sel.Id, declQc);
                WriteLog("Loading discrepancies");
                var discrepancies = LoadDiscrepancies(sel.Id, discrepQc);

                Sleep(_delayOpenTaxPayer, "waiting before opening tax payer card {0} ", _delayOpenTaxPayer);
                var declaration = declarations.Rows.FirstOrDefault();
                var tp = GetTaxPayer(declaration);
                WriteLog("received taxpayer = {0} {1} {2}", tp.Inn, tp.Kpp, tp.Name);


                Sleep(_delaySendForCorrection, "waiting before send for approval {0} ", _delaySendForCorrection);
                WriteLog("Step {0}: Approving...", ConsoleColor.Green, stepId++);
                Approve(sel);
                WriteLog("Done");
            }
            catch (Exception ex)
            {
                Guid errId = Guid.NewGuid();
                WriteLog("Error - {0}", errId);
                WriteError("{0} Error occured during sono processing: {1}", errId, ex);
            }
        }

        #region helpers

        private TaxPayer GetTaxPayer(SelectionDeclaration obj)
        {
            var serv = ServiceProxyFactory.CreateServiceProxy<IDeclarationsDataService>();
            var result = ExtractValueSafe(serv.GetTaxPayerByKppEffective(obj.Inn, obj.Kpp));

            return result;
        }

        private void Approve(Selection s)
        {
            var serv = ServiceProxyFactory.CreateServiceProxy<ISelectionService>();
            serv.Approve(s, new List<long>());
        }

        private DeclarationPageResult LoadDeclarations(long reqId, QueryConditions c)
        {
            DeclarationPageResult res = null;

            var serv = ServiceProxyFactory.CreateServiceProxy<ISelectionsDataService>();
            var response = serv.SearchDeclarations(reqId, c);

            res = ExtractValueOrDie(response);

            return res;
        }


        private PageResult<SelectionDiscrepancy> LoadDiscrepancies(long reqId, QueryConditions c)
        {
            PageResult<SelectionDiscrepancy> res = null;

            var serv = ServiceProxyFactory.CreateServiceProxy<ISelectionService>();
            var response = serv.SearchDiscrepancies(reqId, true, c);

            res = ExtractValueOrDie(response);

            return res;
        }

        private void RefreshSelectionsList()
        {
            var serv = ServiceProxyFactory.CreateServiceProxy<ISelectionService>();
            QueryConditions qc = QueryConditions.Empty;

            var data = ExtractValueOrDie(serv.SelectSelections(qc));
        }

        private Selection GetSelection(string sono)
        {
            var region = sono.Substring(0, 2);
            var serv = ServiceProxyFactory.CreateServiceProxy<ISelectionService>();
            QueryConditions qc = QueryConditions.Empty;
            /*
            qc.Filter.Add(FilterColumn(
                TypeHelper<Selection>.GetMemberName(t => t.RegionNames),
                region, ColumnFilter.FilterComparisionOperator.Like));
            */
            qc.Filter.Add(FilterColumn(
                TypeHelper<Selection>.GetMemberName(t=>t.Status), 
                SELECTION_STATUS_FOR_APPROVAL));

            var data = ExtractValueOrDie(serv.SelectSelections(qc)); 
           
            if(data.Rows == null || data.Rows.Count == 0)
            {
                WriteLog("empty selection list for {0}", region);
                WriteError("empty selection list for {0}", region);
//                throw new ApplicationException(string.Format("empty selection list for {0}", region));

                return null;
            }


            return data.Rows.First();
        }

        private void GetFirstSelectionsList(string sono)
        {
            var serv = ServiceProxyFactory.CreateServiceProxy<ISelectionService>();
            QueryConditions qc = QueryConditions.Empty;

            serv.SelectSelections(qc);
        }

        private IEnumerable<string> GetSonos()
        {
            if (_sono.Contains(","))
            {
                return _sono.Split(',');

            }

            return new[] { _sono };
        }

        #endregion
    }
}
