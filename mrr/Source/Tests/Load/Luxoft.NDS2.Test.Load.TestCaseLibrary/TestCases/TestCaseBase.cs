﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Test.Load.Common;
using Luxoft.NDS2.Test.Load.Common.Base;
using Luxoft.NDS2.Test.Load.Common.Interfaces;
using Luxoft.NDS2.Test.Load.TestCaseLibrary.Base.Arguments;

namespace Luxoft.NDS2.Test.Load.TestCaseLibrary.TestCases
{

    public class TestCaseBase : ILoadTestCase
    {
        protected Dictionary<string, TestCaseArgument> _arguments = new Dictionary<string, TestCaseArgument>();

        Stopwatch _sw = new Stopwatch();

        #region Implementation of ILoadTestCase

        public virtual void Execute(TestExecutionContext context)
        {

        }

        public virtual string GetInfo()
        {
            return string.Empty;
        }

        #endregion
        
        #region Logger

        protected void WriteLog(string log, params object[] p)
        {
            Console.WriteLine("[{0}({2})]: {1}", DateTime.Now, string.Format(log, p), Environment.MachineName);
        }

        protected void WriteLog(string log, ConsoleColor color, params object[] p)
        {
            var prev = Console.ForegroundColor;
            Console.ForegroundColor = color;
            Console.WriteLine("[{0}({2})]: {1}", DateTime.Now, string.Format(log, p), Environment.MachineName);
            Console.ForegroundColor = prev;
        }


        protected void WriteError(string log, params object[] p)
        {
            Console.Error.WriteLine("[{0}]: {1}", DateTime.Now, string.Format(log, p));
        }


        #endregion

        protected void Sleep(int miliseconds, string reason, params object[] p)
        {
            WriteLog(reason, p);
            Thread.Sleep(miliseconds);
        }

        #region User info

        protected string UserSid
        {
            get
            {
                var wi = WindowsIdentity.GetCurrent();
                if (wi != null)
                {
                    return wi.User.Value;
                }

                return string.Empty;
            }
        }

        #endregion region User info

        #region Arguments

        protected string GetStringArgValue(string argName)
        {
            var arg = ((SingleStringArgument)_arguments[argName]);
            WriteLog("{0} = {1}", ConsoleColor.Green, arg.ArgName, arg.Value);

            return arg.Value;
        }

        protected int GetIntArgValue(string argName)
        {
            var arg = ((SingleNumericArgument)_arguments[argName]);
            WriteLog("{0} = {1}", ConsoleColor.Green, arg.ArgName, arg.Value);

            return arg.Value;
        }

        #endregion Arguments

        protected void LoadArguments(Queue<string> args)
        {
            while (args.Count > 0)
            {
                var argName = args.Dequeue();

                if (_arguments.ContainsKey(argName))
                {
                    _arguments[argName].ReadArgumentValues(args);
                }
            }
        }

        protected T ExtractValueOrDie<T>(OperationResult<T> opRes)
        {
            if(opRes == null)
            {
                throw new ArgumentNullException("opRes");
            }

            if(opRes.Status != ResultStatus.Success)
            {
                throw new ApplicationException(string.Format("Сервер вернул статус {0} ('{1}')", opRes.Status, opRes.Message));
            }
            
            if(opRes.Result == null)
            {
                throw new NullReferenceException("opRes.Result");
            }


            return opRes.Result;
        }

        protected T ExtractValueSafe<T>(OperationResult<T> opRes) where T : class , new()
        {
            if (opRes == null)
            {
                return new T();
            }

            if (opRes.Status != ResultStatus.Success)
            {
                WriteLog(string.Format("Сервер вернул статус {0} ('{1}')", opRes.Status, opRes.Message));
                return new T();
            }

            if (opRes.Result == null)
            {
                WriteLog("opRes.Result = null");
                return new T();
            }


            return opRes.Result;
        }

        protected Tuple<T1, T2> ExtractValueOrDie<T1, T2>(OperationResult<T1, T2> opRes) where T1 : class , new() where T2:class , new()
        {
            Tuple<T1, T2> res = null;

            if (opRes == null)
            {
                throw new ArgumentNullException("opRes");
            }

            if (opRes.Status != ResultStatus.Success)
            {
                throw new ApplicationException(string.Format("Сервер вернул статус {0} ('{1}')", opRes.Status, opRes.Message));
            }

            if (opRes.Result == null)
            {
                throw new NullReferenceException("opRes.Result");
            }

            res = new Tuple<T1, T2>(opRes.Result, opRes.Result2);

            return res;
        }

        protected FilterQuery FilterColumn(string columnName, object value, ColumnFilter.FilterComparisionOperator op = ColumnFilter.FilterComparisionOperator.Equals)
        {
            return new FilterQuery
            {
                ColumnName = columnName,
                FilterOperator = FilterQuery.FilterLogicalOperator.Or,
                Filtering = new List<ColumnFilter>
                        {
                            new ColumnFilter
                                {
                                    ComparisonOperator = op,
                                    Value = value
                                }
                        }
            };
        }

        protected void SafeExecute(Action action)
        {
            try
            {
                action();
            }
            catch (Exception ex)
            {
                Guid g = new Guid();
                WriteLog("error occured {0}", g);
                WriteError("Error {0}: {1}", g, ex);
            }
        }

        protected void StartWatch()
        {
            if(_sw.IsRunning)
            {
                _sw.Stop();
            }
            _sw.Reset();

            _sw.Start();
        }

        protected void StopWatch()
        {
            if (_sw.IsRunning)
            {
                _sw.Stop();
            }            

            WriteLog("Потрачено {0} мс", ConsoleColor.White, (double)_sw.ElapsedMilliseconds / 1000);
        }
    }

}
