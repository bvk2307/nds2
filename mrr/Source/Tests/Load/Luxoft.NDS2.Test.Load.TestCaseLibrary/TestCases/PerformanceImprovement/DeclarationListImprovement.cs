﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Test.Load.Common;
using Luxoft.NDS2.Test.Load.Common.Proxy;
using Luxoft.NDS2.Test.Load.TestCaseLibrary.Base.Arguments;

namespace Luxoft.NDS2.Test.Load.TestCaseLibrary.TestCases.PerformanceImprovement
{

    public class DeclarationListImprovement : TestCaseBase
    {
        public const string Name = "decl_list";

        public const string ARG_RANDOM_SORT = "-sort";
        public const string ARG_RANDOM_FILTER = "-filter";
        public const string ARG_THREADS = "-threads";

        Random _randomizer = new Random();

        public DeclarationListImprovement()
        {
            _arguments.Add(ARG_RANDOM_SORT, new SingleNumericArgument(ARG_RANDOM_SORT){ Required = true });
            _arguments.Add(ARG_RANDOM_FILTER, new SingleNumericArgument(ARG_RANDOM_FILTER) { Required = true });
            _arguments.Add(ARG_THREADS, new SingleNumericArgument(ARG_THREADS) { Required = false, DefaultValue=10 });
        }

        private TestExecutionContext _ctx;

        private ConcurrentDictionary<int, List<long>> _stats = new ConcurrentDictionary<int, List<long>>(); 

        public override void Execute(TestExecutionContext context)
        {
            ThreadPool.SetMinThreads(5, 5);
            ThreadPool.SetMaxThreads(200, 200);
            LoadArguments(context.Arguments);
            var tl = new List<Task>();
            var f= new TaskFactory();

            var threads = GetIntArgValue(ARG_THREADS);

            for (int i = 0; i++ < threads; )
            {
                tl.Add(f.StartNew(ExecuteInternal));
            }

            foreach (var task in tl)
            {
                task.Wait();
            }

            foreach (var statKey in _stats.Keys)
            {
                Console.WriteLine("{0} - Min:{1} Max:{2} Avg:{3}", statKey, _stats[statKey].Min(), _stats[statKey].Max(), _stats[statKey].Average());
            }
        }

        private void ExecuteInternal()
        {
            var serv = ServiceProxyFactory.CreateServiceProxy<IDeclarationsDataService>();
            var dtoProperties = GetDtoProperties();
            var rndSort = GetIntArgValue(ARG_RANDOM_SORT);
            var rndFilter = GetIntArgValue(ARG_RANDOM_FILTER);
            _stats.GetOrAdd(Thread.CurrentThread.ManagedThreadId, new List<long>());
            #region Дефолтные фильтры кварталов

            FilterQuery q1 = new FilterQuery()
            {
                ColumnName = "TAX_PERIOD",
                FilterOperator = FilterQuery.FilterLogicalOperator.Or,
                GroupOperator = FilterQuery.FilterLogicalOperator.And,
                Filtering = new List<ColumnFilter>()
                {
                    new ColumnFilter(){ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals, Value = "21"},
                    new ColumnFilter(){ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals, Value = "01"},
                    new ColumnFilter(){ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals, Value = "02"},
                    new ColumnFilter(){ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals, Value = "03"},
                    new ColumnFilter(){ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals, Value = "71"},
                    new ColumnFilter(){ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals, Value = "72"},
                    new ColumnFilter(){ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals, Value = "73"},
                    new ColumnFilter(){ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals, Value = "51"},
                }
            };

            FilterQuery q2 = new FilterQuery()
            {
                ColumnName = "TAX_PERIOD",
                FilterOperator = FilterQuery.FilterLogicalOperator.Or,
                GroupOperator = FilterQuery.FilterLogicalOperator.And,
                Filtering = new List<ColumnFilter>()
                {
                    new ColumnFilter(){ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals, Value = "22"},
                    new ColumnFilter(){ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals, Value = "04"},
                    new ColumnFilter(){ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals, Value = "05"},
                    new ColumnFilter(){ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals, Value = "06"},
                    new ColumnFilter(){ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals, Value = "54"},
                    new ColumnFilter(){ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals, Value = "74"},
                    new ColumnFilter(){ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals, Value = "75"},
                    new ColumnFilter(){ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals, Value = "76"},
                }
            };


            FilterQuery q3 = new FilterQuery()
            {
                ColumnName = "TAX_PERIOD",
                FilterOperator = FilterQuery.FilterLogicalOperator.Or,
                GroupOperator = FilterQuery.FilterLogicalOperator.And,
                Filtering = new List<ColumnFilter>()
                {
                    new ColumnFilter(){ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals, Value = "23"},
                    new ColumnFilter(){ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals, Value = "07"},
                    new ColumnFilter(){ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals, Value = "08"},
                    new ColumnFilter(){ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals, Value = "09"},
                    new ColumnFilter(){ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals, Value = "55"},
                    new ColumnFilter(){ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals, Value = "77"},
                    new ColumnFilter(){ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals, Value = "78"},
                    new ColumnFilter(){ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals, Value = "79"},
                }
            };

            FilterQuery q4 = new FilterQuery()
            {
                ColumnName = "TAX_PERIOD",
                FilterOperator = FilterQuery.FilterLogicalOperator.Or,
                GroupOperator = FilterQuery.FilterLogicalOperator.And,
                Filtering = new List<ColumnFilter>()
                {
                    new ColumnFilter(){ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals, Value = "24"},
                    new ColumnFilter(){ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals, Value = "10"},
                    new ColumnFilter(){ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals, Value = "11"},
                    new ColumnFilter(){ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals, Value = "12"},
                    new ColumnFilter(){ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals, Value = "56"},
                    new ColumnFilter(){ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals, Value = "80"},
                    new ColumnFilter(){ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals, Value = "81"},
                    new ColumnFilter(){ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals, Value = "82"},
                }
            };

            FilterQuery qDefault = new FilterQuery();

            #endregion Дефолтные фильтры кварталов

            List<FilterQuery> defaults = new List<FilterQuery>() { q1, q2, q3, q4, qDefault };

            int i = 5;
            while (i-- > 0)
            {
                foreach (var filterQuery in defaults)
                {
                    try
                    {
                        QueryConditions qc = new QueryConditions();

                        qc.Filter = new List<FilterQuery>()
                        {
                            filterQuery
                        };

                        #region Доп фильтры
                        if (rndFilter > 0)
                        {
                            var cntRndFilter = _randomizer.Next(0, 3);

                            while (cntRndFilter-- > 0)
                            {
                                qc.Filter.Add(GetRandomFilter(dtoProperties));
                            }
                        }
                        #endregion Доп фильтры

                        #region Сортировка
                        if (rndSort > 0) qc.Sorting.Add(GetSortCriteria(dtoProperties));
                        #endregion Сортировка

                        Console.WriteLine("-----------------------  {0} ------------------------------", Thread.CurrentThread.ManagedThreadId);
                        Console.Write("Filter:");
                        foreach (var queryFilter in qc.Filter)
                        {
                            Console.Write("\n\t{0}:\t", queryFilter.ColumnName);
                            foreach (var filter in queryFilter.Filtering)
                            {
                                Console.Write("{0}, ", filter.Value);
                            }
                        }

                        Console.Write("\nSort by:");
                        foreach (var querySort in qc.Sorting)
                        {
                            Console.WriteLine("\t{0}:", querySort.ColumnKey);
                        }

                        Stopwatch sw = new Stopwatch();
                        sw.Start();
                        var rslt = serv.SelectDeclarations(qc);
                        sw.Stop();

                        _stats[Thread.CurrentThread.ManagedThreadId].Add(sw.ElapsedMilliseconds);

                        var status = rslt.Status;
                        var message = rslt.Status == ResultStatus.Success ? string.Empty : rslt.Message;
                        var receivedRows = rslt.Status == ResultStatus.Success ? 0 : rslt.Result.TotalMatches;

                        Console.WriteLine("({0} ms) \t - {1}, rows = {2}, msg = {3}", sw.ElapsedMilliseconds, status, receivedRows, message);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
        }

        private bool IsNumeric(Type t)
        {
            List<Type> numTypes = new List<Type>()
            {
                              typeof (Int16),
                              typeof (Int32),
                              typeof (Int64),
                              typeof (Single),
                              typeof (Double),
                              typeof (Decimal),
                              typeof (Int16?),
                              typeof (Int32?),
                              typeof (Int64?),
                              typeof (Single?),
                              typeof (Double?),
                              typeof (Decimal?),
            };

            return numTypes.Any(n => t.IsAssignableFrom(n));
        }

        private FilterQuery GetRandomFilter(List<PropertyInfo> props)
        {
            var rnd = _randomizer.Next(0, props.Count);
            var prop = props[rnd];

            FilterQuery q = new FilterQuery(){ColumnName = prop.Name};

            q.FilterOperator = FilterQuery.FilterLogicalOperator.And;

            if (IsNumeric(prop.PropertyType))
            {
                q.Filtering = new List<ColumnFilter>(){ new ColumnFilter(){ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals, Value = 1234}};
            }

            if (prop.PropertyType.IsAssignableFrom(typeof(string)))
            {
                q.Filtering = new List<ColumnFilter>() { new ColumnFilter() { ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals, Value = "1234" } };
            }

            if (prop.PropertyType.IsAssignableFrom(typeof(DateTime)))
            {
                q.Filtering = new List<ColumnFilter>() { new ColumnFilter() { ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals, Value = DateTime.Now } };
            }

            return q;
        }

        private ColumnSort GetSortCriteria(List<PropertyInfo> props)
        {
            return new ColumnSort(){ColumnKey = props[_randomizer.Next(1, props.Count)].Name};
        }

        private List<PropertyInfo> GetDtoProperties()
        {
            Type t = typeof (DeclarationSummary);

            return
                t.GetProperties()
                    .Where(
                        p =>
                            p.GetCustomAttributes(false)
                                .Any(a => a.GetType().IsAssignableFrom(typeof (DisplayNameAttribute))))
                    .ToList();
        }
    }
}
