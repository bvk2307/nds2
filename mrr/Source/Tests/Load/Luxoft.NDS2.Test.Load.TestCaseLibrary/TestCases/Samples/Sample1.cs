﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Luxoft.NDS2.Client;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Test.Load.Common;
using Luxoft.NDS2.Test.Load.Common.Base;
using Luxoft.NDS2.Test.Load.Common.Interfaces;
using Luxoft.NDS2.Test.Load.Common.Proxy;
using Luxoft.NDS2.Test.Load.Launcher.Probe;
using Luxoft.NDS2.Test.Load.TestCaseLibrary.Base.Arguments;

namespace Luxoft.NDS2.Test.Load.TestCaseLibrary.TestCases.Samples
{
    public class Sample1 : TestCaseBase
    {
        public const string ARG_SONO_CODE = "-sono";
        public const string ARG_IMPERSONATE = "-impersonate";


        public const string Name = "sample1";

        private Dictionary<string, TestCaseArgument> _arguments = new Dictionary<string, TestCaseArgument>();

        public Sample1()
        {
            _arguments.Add(ARG_SONO_CODE, new SingleStringArgument(ARG_SONO_CODE) { Description = "{0} sono_code1,sono_code2,sono_code3,... \t\t\tномер инспекции по которой будет проиcходить поиск деклараций", Required = true });
            _arguments.Add(ARG_IMPERSONATE, new ImpersonateArgument(ARG_IMPERSONATE));
        }


        public override void Execute(TestExecutionContext context)
        {
            var sono = "3000";
//
//            var blService = ServiceProxyFactory.CreateServiceProxy<ILoadTestHelpService>();
//            var response = blService.GetQueryText("Declaration", "test");
//
//            var value = ExtractValueOrDie(response);
//
//            WriteLog(value.FirstOrDefault(), ConsoleColor.Yellow);


            var serv = ServiceProxyFactory.CreateServiceProxy<ISelectionService>();
            SelectionDetails resultOut = null;

            WriteLog("Saving selection");
            var filter = new SelectionFilter();
            filter.GroupsFilters.Add(new GroupFilter()
            {
                IsActive = true,
                Filters = new List<FilterCriteria>()
                        {new FilterCriteria(){Name = "Регион", LookupTableName = "v$ssrf", FirInternalName = "REGION", InternalName = "Region", IsActive = true, IsRequired = true, Values = new List<FilterElement>(){ new FilterElement(){Operator = FilterElement.ComparisonOperations.Equals, ValueOne = sono.Substring(0,2)}}}
                        , new FilterCriteria(){Name = "Инспекция", LookupTableName = "v$sono", FirInternalName = "IFNS_NUMBER", InternalName = "TaxOrgan", IsActive = true, IsRequired = false, Values = new List<FilterElement>(){ new FilterElement(){Operator = FilterElement.ComparisonOperations.Equals, ValueOne = sono}}}}
            });

            var s = new Selection()
            {
                Analytic = "LoadTest",
                AnalyticSid = UserSid,
                Name = Guid.NewGuid().ToString(),
                //Filter = filter,
                Status = SelectionStatus.Loading
            };

            var result = serv.Save(s, ActionType.Create);

            s = ExtractValueOrDie(result);

            WriteLog("Do request");
            var result2 = serv.RequestDiscrepancies(s.Id);

            var requestId = ExtractValueOrDie(result2);

            bool keepWaiting = true;

            while (keepWaiting)
            {
                var statusRes = ExtractValueOrDie(serv.SyncWithRequestStatus(s.Id, s.TypeCode));

                foreach (var item in statusRes)
                  if (s.TypeCode == SelectionType.Hand)
                    {
                        if (statusRes.Count != 1)
                            throw new ApplicationException(string.Format(ResourceManagerNDS2.SelectionRequestNotFound, s.Id));
                        else
                        {
                            switch (statusRes.First().RequestStatus)
                            {
                                case SelectionRequestStatus.New:
                                case SelectionRequestStatus.InQueue:
                                case SelectionRequestStatus.AcceptedInQueue:
                                case SelectionRequestStatus.PlanReceived:
                                case SelectionRequestStatus.SelectionCreated:
                                case SelectionRequestStatus.SelectionRecorded:
                                    Thread.Sleep(2000);
                                    break;
                                case SelectionRequestStatus.DeclarationsRecorded:
                                    keepWaiting = false;
                                    break;
                                case SelectionRequestStatus.PlanError:
                                case SelectionRequestStatus.SelectionError:
                                case SelectionRequestStatus.SelectionRecordError:
                                case SelectionRequestStatus.DeclarationsRecordError:
                                    throw new ApplicationException(string.Format(ResourceManagerNDS2.LoadError, statusRes.First().RequestStatus));
                                default:
                                    throw new ApplicationException(string.Format(ResourceManagerNDS2.SelectionUnknownSatus, statusRes.First().RequestStatus));
                            }
                        }
                    }
                }
            

            resultOut = ExtractValueOrDie(serv.Load(s.Id, String.Empty, SelectionType.Hand));

            Console.WriteLine(resultOut.Data.Id);
        }

    }
}
