﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Test.Load.Common;
using Luxoft.NDS2.Test.Load.Common.Base;
using Luxoft.NDS2.Test.Load.Common.Interfaces;
using Luxoft.NDS2.Test.Load.Common.Proxy;
using Luxoft.NDS2.Test.Load.Launcher.Probe;
using Luxoft.NDS2.Test.Load.TestCaseLibrary.Base.Arguments;

namespace Luxoft.NDS2.Test.Load.TestCaseLibrary.TestCases.Samples
{
    public class GetFirst10DeclarationsTest : ILoadTestCase
    {
        public const string ARG_SONO_CODE = "-sono";
        public const string ARG_IMPERSONATE = "-impersonate";


        public const string Name = "getFirst10Declarations";

        private Dictionary<string, TestCaseArgument> _arguments = new Dictionary<string, TestCaseArgument>();

        public GetFirst10DeclarationsTest()
        {
            _arguments.Add(ARG_SONO_CODE, new SingleStringArgument(ARG_SONO_CODE) { Description = "{0} sono_code1,sono_code2,sono_code3,... \t\t\tномер инспекции по которой будет проиcходить поиск деклараций", Required = true });
            _arguments.Add(ARG_IMPERSONATE, new ImpersonateArgument(ARG_IMPERSONATE));
        }

        #region Implementation of ILoadTestCase


        public void Execute(TestExecutionContext context)
        {
            LoadArguments(context.Arguments);
            var impersonate = GetArgument<ImpersonateArgument>(ARG_IMPERSONATE);
            if(impersonate.IsInitialized)
            {
                using(new Impersonation(impersonate.DomainName, impersonate.UserName, impersonate.Password))
                {
                    ExecuteInternal();
                }
            }
            else
            {
                ExecuteInternal();
            }

        }

        private void ExecuteInternal()
        {
            var blService = ServiceProxyFactory.CreateServiceProxy<IDeclarationsDataService>();
            var sonoCodeArg = GetArgument<SingleStringArgument>(ARG_SONO_CODE);
            var multipleSouns = sonoCodeArg.Value.Split(',').ToList();

            if(multipleSouns.Count == 0){multipleSouns.Add(sonoCodeArg.Value);}


            foreach(var soun in multipleSouns)
            {
                Console.WriteLine("Gettin declarations for {0}", soun);
                QueryConditions c = QueryConditions.Empty;
                c.Filter.Add(new FilterQuery(true)
                {
                    ColumnName = "SOUN_CODE",
                    FilterOperator = FilterQuery.FilterLogicalOperator.And,
                    Filtering =
                {
                    new ColumnFilter()
                    {
                        ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                        Value = soun
                    }
                }
                });

                var result = blService.SelectDeclarations(c);
                LogResponse(result);
            }
        }

        private void LogResponse(OperationResult<PageResult<DeclarationSummary>> response)
        {
            Console.WriteLine("response status: {0}", response.Status);
            Console.WriteLine("declarations count: {0}", response.Result == null ? 0 : response.Result.TotalMatches);
        }

        public string GetInfo()
        {
            var sb = new StringBuilder();
            sb.AppendLine("Тест инициирующий загрузку декларации в МРР для дальнейшего отображения в МРР");
            sb.AppendLine("Параметры");
            foreach (var testCaseArgument in _arguments)
            {
                sb.AppendLine(testCaseArgument.Value.GetArgumentInfo());
            }

            return sb.ToString();
        }

        private T GetArgument<T>(string key) where T : TestCaseArgument
        {
            return (T)_arguments[key];
        }

        public void LoadArguments(Queue<string> args)
        {
            while (args.Count > 0)
            {
                var argName = args.Dequeue();

                if (_arguments.ContainsKey(argName))
                {
                    _arguments[argName].ReadArgumentValues(args);
                }
            }
        }

        #endregion
    }
}
