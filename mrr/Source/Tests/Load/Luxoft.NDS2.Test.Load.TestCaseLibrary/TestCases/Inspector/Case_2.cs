﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Windows.Forms.VisualStyles;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Test.Load.Common;
using Luxoft.NDS2.Test.Load.Common.Base;
using Luxoft.NDS2.Test.Load.Common.Interfaces;
using Luxoft.NDS2.Test.Load.Common.Proxy;
using Luxoft.NDS2.Test.Load.Launcher.Probe;
using Luxoft.NDS2.Test.Load.TestCaseLibrary.Base.Arguments;

namespace Luxoft.NDS2.Test.Load.TestCaseLibrary.TestCases.Inspector
{
    [Description(@"Действия инспектора
•	раз в 30 мин обновляет список деклараций
•	с задержкой в 30 сек отмечает флагом 100 деклараций 
•	с задержкой в 10 сек нажимает кнопку “Взять в работу”
•	с задержкой в 10 сек нажимает “Только мои”
•	с задержкой в 1 мин открывает декларацию
•	с задержкой в 5 мин открывает другую корректировку декларации
•	с задержкой в 10 мин переходит на другую декларацию по номеру счет-фактуры
•	с задержкой в 5 мин открывает карточку автотребования
•	с задержкой в 5 мин открывает карточку автоистребования
")]

    /*
     Luxoft.NDS2.Test.Load.Launcher.exe inspectorTest1 -sono 5252,7746 -impersonate vortex maxim pass -declarationToSelect 5 -refreshDeclListInterval 1000 -delaySelectDecl 1500 -delayTakeWork 1600 -delayShowMine 1700 -delayOpenDecl 1800 -delayChangeCorrection 1900 -delayOpenRefDecl 2000
     */

    public class Case_2 : TestCaseBase
    {
        #region Constants

        public const string ARG_CYCLES_COUNT = "-cycles";
        public const string ARG_SONO_CODE = "-sono";
        public const string ARG_IMPERSONATE = "-impersonate";
        public const string ARG_DECL_COUNT_TO_SELECT = "-declarationToSelect";
        public const string ARG_DELAY_DECL_REFRESH = "-refreshDeclListInterval";
        public const string ARG_DELAY_SELECT_DECL = "-delaySelectDecl";
        public const string ARG_DELAY_TAKE_WORK = "-delayTakeWork";
        public const string ARG_DELAY_SHOW_MINE = "-delayShowMine";
        public const string ARG_DELAY_OPEN_DECL = "-delayOpenDecl";
        public const string ARG_DELAY_CHANGE_CORRECTION = "-delayChangeCorrection";
        public const string ARG_DELAY_OPEN_REF_DECL = "-delayOpenRefDecl";

        public const string Name = "inspectorTest2";

        #endregion

        #region Fields

        private string _sono = string.Empty;
        private int _declarationsToSelect = 0;
        private int _delayDeclRefresh = 0;
        private int _delaySelectDecl = 0;
        private int _delayTakeWork = 0;
        private int _delayShowMine = 0;
        private int _delayOpenDecl = 0;
        private int _delayChangeCorrection = 0;
        private int _delayOpenRefDecl = 0;

        #endregion

        public Case_2()
        {
            _arguments.Add(ARG_CYCLES_COUNT, new SingleNumericArgument(ARG_CYCLES_COUNT) { DefaultValue = 10, Description = "{0} number кол-во деклараций для обработки", Required = true });
            _arguments.Add(ARG_SONO_CODE, new SingleStringArgument(ARG_SONO_CODE) { Description = "{0} sono_code \t\t\tномер инспекции по которой будет проиходить поиск деклараций", Required = true });

            _arguments.Add(ARG_DECL_COUNT_TO_SELECT, new SingleNumericArgument(ARG_DECL_COUNT_TO_SELECT) { DefaultValue = 5 });

            _arguments.Add(ARG_DELAY_DECL_REFRESH, new SingleNumericArgument(ARG_DELAY_DECL_REFRESH) { DefaultValue = 1800000 });
            _arguments.Add(ARG_DELAY_SELECT_DECL, new SingleNumericArgument(ARG_DELAY_SELECT_DECL) { DefaultValue = 500 });
            _arguments.Add(ARG_DELAY_TAKE_WORK, new SingleNumericArgument(ARG_DELAY_TAKE_WORK) { DefaultValue = 500 });
            _arguments.Add(ARG_DELAY_SHOW_MINE, new SingleNumericArgument(ARG_DELAY_SHOW_MINE) { DefaultValue = 500 });
            _arguments.Add(ARG_DELAY_OPEN_DECL, new SingleNumericArgument(ARG_DELAY_OPEN_DECL) { DefaultValue = 500 });
            _arguments.Add(ARG_DELAY_CHANGE_CORRECTION, new SingleNumericArgument(ARG_DELAY_CHANGE_CORRECTION) { DefaultValue = 500 });
            _arguments.Add(ARG_DELAY_OPEN_REF_DECL, new SingleNumericArgument(ARG_DELAY_OPEN_REF_DECL) { DefaultValue = 500 });

            _arguments.Add(ARG_IMPERSONATE, new ImpersonateArgument(ARG_IMPERSONATE));
        }

        #region Implementation of ILoadTestCase

        public override void Execute(TestExecutionContext context)
        {
            LoadArguments(context.Arguments);
            _declarationsToSelect = GetIntArgValue(ARG_DECL_COUNT_TO_SELECT);
            _delayDeclRefresh   = GetIntArgValue(ARG_DELAY_DECL_REFRESH);
            _delaySelectDecl    = GetIntArgValue(ARG_DELAY_SELECT_DECL);
            _delayTakeWork      = GetIntArgValue(ARG_DELAY_TAKE_WORK);
            _delayShowMine      = GetIntArgValue(ARG_DELAY_SHOW_MINE);
            _delayOpenDecl      = GetIntArgValue(ARG_DELAY_OPEN_DECL);
            _delayChangeCorrection = GetIntArgValue(ARG_DELAY_CHANGE_CORRECTION);
            _delayOpenRefDecl   = GetIntArgValue(ARG_DELAY_OPEN_REF_DECL);
            _sono = GetStringArgValue(ARG_SONO_CODE);

            var impersonateArg = (ImpersonateArgument)_arguments[ARG_IMPERSONATE];

            if(impersonateArg.IsInitialized)
            {
                using(new Impersonation(impersonateArg.DomainName, impersonateArg.UserName, impersonateArg.Password))
                {
                    ExecuteInternal();
                }
            }
            else
            {
                ExecuteInternal();
            }

            WriteLog("Конец");
        }

        public void ExecuteInternal()
        {

            var sonoCodes = _sono.Split(',');
            if(!sonoCodes.Any())
            {
                sonoCodes = new string[]{_sono};
            }

            foreach (string sonoCode in sonoCodes)
            {
                WriteLog("СОНО {0}.", ConsoleColor.Yellow, sonoCode);

                List<long> processedDeclarations = new List<long>();

                var unassignedDecls = GetUnassignedDeclarations(sonoCode);
                if (unassignedDecls.Count == 0)
                {
                    WriteLog("Список свободных деклараций пуст.");
                    return;
                }
                else
                {
                    WriteLog("Получено {0} деклараций.", unassignedDecls.Count);
                }
            }
            WriteLog("Окончание сценария.");
        }

        public virtual string GetInfo()
        {
            var sb = new StringBuilder();
            sb.AppendLine("Тест инициирующий загрузку декларации в МРР для дальнейшего отображения в МРР");
            sb.AppendLine("Параметры");
            foreach (var testCaseArgument in _arguments)
            {
                sb.AppendLine(testCaseArgument.Value.GetArgumentInfo());
            }

            return sb.ToString();
        }



        #endregion

        private List<DeclarationBrief> GetUnassignedDeclarations(string sono)
        {
            var qc = new QueryConditions();
            var propName = TypeHelper<DeclarationBrief>.GetMemberName(t => t.INSPECTOR_SID);
            var sonoPropName = TypeHelper<DeclarationBrief>.GetMemberName(t => t.SOUN_CODE);
            qc.PageSize = 10;
            qc.Filter.Add(FilterColumn(propName, null));
            qc.Filter.Add(FilterColumn(sonoPropName, sono));

            return GetDeclarations(qc);
        }

        private List<DeclarationBrief> GetMyDeclarations(string sono)
        {
            var qc = new QueryConditions();
            var propName = TypeHelper<DeclarationBrief>.GetMemberName(t => t.INSPECTOR_SID);
            var sonoPropName = TypeHelper<DeclarationBrief>.GetMemberName(t => t.SOUN_CODE);
            qc.PageSize = 10;
            qc.Filter.Add(FilterColumn(propName, UserSid));
            qc.Filter.Add(FilterColumn(sonoPropName, sono));

            return GetDeclarations(qc);
        }

        private List<DeclarationBrief> GetDeclarations(QueryConditions conditions)
        {
            var declService = ServiceProxyFactory.CreateServiceProxy<IDeclarationsDataService>();
            var response = declService.SelectDeclarationsForInspector(conditions);

            return response.Status == ResultStatus.Success ? response.Result.Rows : new List<DeclarationBrief>();
        }

        private DeclarationSummary OpenDeclaration(long declId)
        {
            var declService = ServiceProxyFactory.CreateServiceProxy<IDeclarationsDataService>();
            var response = declService.GetDeclaration(declId);

            if (response.Status != ResultStatus.Success)
            {
                WriteLog("Ошибка при открытии декларации {0}: {1}", declId, response.Message);
                return null;
            }

            return response.Result;
        }


        private List<Invoice> LoadChapterData(DeclarationSummary decl, int chapter)
        {
            var resOut = new List<Invoice>();
            /*var declService = ServiceProxyFactory.CreateServiceProxy<IDeclarationsDataService>();
            var dr = new DeclarationRequestData(decl);
            var argument = new InvoiceSovOperation {decl = dr, Chapter = chapter, Conditions = QueryConditions.Empty};

            var result = declService.GetSovInvoices(argument);
            WriteLog("Идентификатор СОВ запроса - {0}", result.Result.RequestId);
            while (result.Result.ExecutionStatus == RequestStatusType.InProcess)
            {
                Sleep(1500, "Ожидание выгрузки раздела {0}", 1500);
                result = declService.GetSovInvoices(result.Result);
            }

            if (result.Result.ExecutionStatus == RequestStatusType.Completed)
            {
                resOut = result.Result.Result;
            }
            else
            {
                WriteLog("Запрос получения СФ вернул статус: {0}", result.Result.ExecutionStatus);
            }*/

            return resOut;
        }
    }
}
