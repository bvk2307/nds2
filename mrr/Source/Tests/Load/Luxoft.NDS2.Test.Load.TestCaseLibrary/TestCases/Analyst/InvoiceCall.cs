﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CommonComponents.Instrumentation;
using CommonComponents.Security.Authorization;
using Luxoft.NDS2.Client;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Test.Load.Common;
using Luxoft.NDS2.Test.Load.Common.Proxy;
using Luxoft.NDS2.Test.Load.Launcher.Probe;
using Luxoft.NDS2.Test.Load.TestCaseLibrary.Base.Arguments;

namespace Luxoft.NDS2.Test.Load.TestCaseLibrary.TestCases.Analyst
{
    [Description(@"")]
    public class InvoceCallData
    {
        long Zip { get; set; }
        int Chapter { get; set; }
        int Rows { get; set; }
    }

    public class InvoiceCall : TestCaseBase
    {

        public const string Name = "analyticTestInvoiceCall";

        #region Constants

        public const string ARG_PRL = "-parallel";
        public const string ARG_CYCLES = "-cycles";
        public const string ARG_DATA = "-data";
        public const string ARG_LOG = "-log";
        public object _syncLock = new object();

        #endregion

        #region Fields

        private int _parallelCnt = 0;
        private List<string> _logs = new List<string>();
        private string _logFileName = "log.txt";

        #endregion

        public InvoiceCall()
        {
            _arguments.Add(ARG_PRL, new SingleNumericArgument(ARG_PRL) { DefaultValue = 5 });
            _arguments.Add(ARG_CYCLES, new SingleNumericArgument(ARG_CYCLES) { DefaultValue = 2 });
            _arguments.Add(ARG_DATA, new SingleStringArgument(ARG_DATA));
            _arguments.Add(ARG_LOG, new SingleStringArgument(ARG_LOG));
        }

        #region Implementation of ILoadTestCase

        public override void Execute(TestExecutionContext context)
        {
            //ThreadPool.SetMinThreads(50, 50);
            //ThreadPool.SetMaxThreads(70, 70);

            base.LoadArguments(context.Arguments);

            var cycles = GetIntArgValue(ARG_CYCLES);
            _logFileName = GetStringArgValue(ARG_LOG);
            var inputData = GetStringArgValue(ARG_DATA);

            Execute(inputData);
        }

        private  void Execute(string inputData)
        {
            int quarter;
            int year;
            long zip;
            int chapter;
            int mode;
            int rows;

            try
            {
                var inputs = inputData.Split('/');

                mode = int.Parse(inputs[0]);

                if (mode == 1)
                {
                    year = int.Parse(inputs[1]);
                    quarter = int.Parse(inputs[2]);
                    chapter = int.Parse(inputs[3]);
                    zip = long.Parse(inputs[4]);
                    rows = int.Parse(inputs[5]);
                    //Console.WriteLine("{0}/{1}/{2}", chapter, zip, rows);
                    GetChapterData(zip, TipFajlaToChapterNum(chapter), rows);
                }
                else
                {
                                
                }

                            
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public override string GetInfo()
        {
            return string.Empty;
        }

        #endregion

        private void GetChapterData(long zip, int chapter, int rows)
        {
            DeclarationChapterDataRequest request = new DeclarationChapterDataRequest()
            {
                Chapter = chapter, ClientCache = new List<DeclarationInvoiceChapterInfo>(), DeclarationId = zip, SearchContext = new QueryConditions()
            };
            request.SearchContext.PaginationDetails.RowsToTake = 5000;
            request.SearchContext.PaginationDetails.RowsToSkip = 0;
            Console.WriteLine((AskInvoiceChapterNumber)Enum.Parse(typeof(AskInvoiceChapterNumber), chapter.ToString()));
            request.ClientCache.Add(new DeclarationInvoiceChapterInfo(){Zip = zip, Part = (AskInvoiceChapterNumber)Enum.Parse(typeof(AskInvoiceChapterNumber), chapter.ToString())});
            var serv = ServiceProxyFactory.CreateServiceProxy<IInvoiceDataService>();
            Stopwatch sw = new Stopwatch();
            sw.Start();
            var response = serv.SearchInDeclarationChapter(request);
            sw.Stop();


            AppendLog(string.Format("{0}/{1}/{2}/{3}/{4}", zip, chapter, rows, sw.ElapsedMilliseconds, response.Status == ResultStatus.Success ? 1 : 3));
            
            
            Console.WriteLine("#{0}[{1}] - {2} {3}", Thread.CurrentThread.ManagedThreadId, rows, response.Status, response.Message);
            Console.WriteLine();

        }


        private int TipFajlaToChapterNum(int tipFajla)
        {
            switch (tipFajla)
            {
                case 0:
                    return 8;
                case 1:
                    return 9;
                case 2:
                    return 81;
                case 3:
                    return 91;
                case 4:
                    return 10;
                case 5:
                    return 11;
                case 6:
                    return 12;
                case 7:
                    return 13;
                case 8:
                    return 14;
                default:
                    return -1;
            }
        }

        #region Helpers

        private void AppendLog(string log)
        {
            if (_logs.Count > 10)
            {
                lock (_syncLock)
                {
                    if (_logs.Count > 10)
                    {
                        File.AppendAllLines(_logFileName, _logs);
                        _logs.Clear();
                    }
                }
            }

            _logs.Add(log);
        }

        #endregion Helpers
    }
}
