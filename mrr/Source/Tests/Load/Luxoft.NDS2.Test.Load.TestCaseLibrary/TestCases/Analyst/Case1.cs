﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Test.Load.Common;
using Luxoft.NDS2.Test.Load.Common.Base;
using Luxoft.NDS2.Test.Load.Common.Interfaces;
using Luxoft.NDS2.Test.Load.Common.Proxy;
using Luxoft.NDS2.Test.Load.Launcher.Probe;
using Luxoft.NDS2.Test.Load.TestCaseLibrary.Base.Arguments;

namespace Luxoft.NDS2.Test.Load.TestCaseLibrary.TestCases.Analyst
{
    [Description(@"Кейс 2. Аналитик работает с ручными выборками. Последовательно выполняет следующие действия:
•	раз в 30 мин. создает выборку, в которую должно попасть 1000 расхождений
•	с задержкой 10 сек. перелистывает список расхождений на следующую страницу
•	с задержкой 15 сек. применяет фильтр на списке расхождений 
•	с задержкой 15 сек. сортирует записи по сумме расхождения
•	отмечает флагом 500 расхождений
•	нажимает кнопку “На согласование”
")]
    public class Case1 : TestCaseBase
    {
        public const string ARG_CYCLES_COUNT = "-cycles";
        public const string ARG_SONO_CODE = "-sono";
        public const string ARG_IMPERSONATE = "-impersonate";

        public const string Name = "analyticTest1";
        private string _sono = string.Empty;
        public Case1()
        {
            _arguments.Add(ARG_CYCLES_COUNT, new SingleNumericArgument(ARG_CYCLES_COUNT) { DefaultValue = 10, Description = "{0} number кол-во деклараций для обработки", Required = true });
            _arguments.Add(ARG_SONO_CODE, new SingleStringArgument(ARG_SONO_CODE) { Description = "{0} sono_code \t\t\tномер инспекции по которой будет проиходить поиск деклараций", Required = true });
            _arguments.Add(ARG_IMPERSONATE, new ImpersonateArgument(ARG_IMPERSONATE));
        }

        #region Implementation of ILoadTestCase


        public override void Execute(TestExecutionContext context)
        {
            LoadArguments(context.Arguments);

            var impersonateArg = (ImpersonateArgument)_arguments[ARG_IMPERSONATE];

            if(impersonateArg.IsInitialized)
            {
                using(new Impersonation(impersonateArg.DomainName, impersonateArg.UserName, impersonateArg.Password))
                {
                    ExecuteInternal();
                }
            }
            else
            {
                ExecuteInternal();
            }
        }

        public void ExecuteInternal()
        {
            _sono = GetStringArgValue(ARG_SONO_CODE);

            foreach(var sono in GetSonos())
            {
                var selectionService = ServiceProxyFactory.CreateServiceProxy<ISelectionService>();

                List<Discrepancy> selDiscrepancies = null;
                List<DeclarationPageResult> selDeclarations = null;

                //получим список черновиков
                WriteLog("Getting drafts");
                QueryConditions qc = QueryConditions.Empty;
                qc.Filter = new List<FilterQuery>();
                //qc.Filter.Add(new FilterQuery()
                //{
                //    ColumnName = TypeHelper<Selection>.GetMemberName(s => s.RegionName),
                //    Filtering =
                //    {
                //        new ColumnFilter()
                //        {
                //            ComparisonOperator = ColumnFilter.FilterComparisionOperator.StartsWith,
                //            Value = sono.Substring(0,2)
                //        }
                //    }
                //});

                var response = selectionService.SelectSelections(qc);
                var selectionList = ExtractValueOrDie(response);
                WriteLog("Received {0} ", selectionList.Rows.Count);
                var selection = selectionList.Rows.FirstOrDefault();
                if (selection != null)
                {

                    QueryConditions qcInternalLists = QueryConditions.Empty;
                    qcInternalLists.PageSize = 50;
                    //загрузим расхождения
                    WriteLog("Getting discrepancies");
                    var discrepancyListResponse = selectionService.SearchDiscrepancies(selection.Id, true,
                        qcInternalLists);
                    //загрузим декларации
                    WriteLog("Getting declarations");
                    var declarationsResponse = selectionService.SearchDeclarations(selection.Id, qcInternalLists);


                    qcInternalLists.PageSize = 200;
                    //загрузим расхождения
                    WriteLog("Getting 200 declarations");
                    discrepancyListResponse = selectionService.SearchDiscrepancies(selection.Id, true, qcInternalLists);
                    //загрузим декларации
                    WriteLog("Getting 200 declarations");
                    declarationsResponse = selectionService.SearchDeclarations(selection.Id, qcInternalLists);

                    WriteLog("Sending for approval");
                    selectionService.SendToAproval(selection);
                }
                else
                {
                    WriteLog("No selections found");
                }
            }

        }



        public string GetInfo()
        {
            var sb = new StringBuilder();
            sb.AppendLine("Тест инициирующий загрузку декларации в МРР для дальнейшего отображения в МРР");
            sb.AppendLine("Параметры");
            foreach (var testCaseArgument in _arguments)
            {
                sb.AppendLine(testCaseArgument.Value.GetArgumentInfo());
            }

            return sb.ToString();
        }

        private IEnumerable<string> GetSonos()
        {
            if (_sono.Contains(","))
            {
                return _sono.Split(',');

            }

            return new[] { _sono };
        }
//        public void LoadArguments(Queue<string> args)
//        {
//            while (args.Count > 0)
//            {
//                var argName = args.Dequeue();
//
//                if (_arguments.ContainsKey(argName))
//                {
//                    _arguments[argName].ReadArgumentValues(args);
//                }
//            }
//        }

        #endregion
    }
}
