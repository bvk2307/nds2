﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Luxoft.NDS2.Client;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Test.Load.Common;
using Luxoft.NDS2.Test.Load.Common.Proxy;
using Luxoft.NDS2.Test.Load.Launcher.Probe;
using Luxoft.NDS2.Test.Load.TestCaseLibrary.Base.Arguments;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;

namespace Luxoft.NDS2.Test.Load.TestCaseLibrary.TestCases.Analyst
{
    public class Case3 : TestCaseBase
    {
        #region Constants

        public const string ARG_SONO_CODE = "-sono";
        public const string ARG_IMPERSONATE = "-impersonate";

        public const string ARG_ITERATION_TIME = "-iterationTime";

        public const string ARG_DELAY_NEXT_PAGE_DISCREP = "-delayNextPageDiscrep";
        public const string ARG_DELAY_FILTER_APPLY = "-delayFilterApply";
        public const string ARG_DELAY_SORT_APPLY = "-delaySortApply";
        public const string ARG_NUM_OF_DISCREP_TO_CHECK = "-numCheckDiscrep";

        public const string Name = "analyticTest2";

        #endregion

        #region Fields

        private string _sono = string.Empty;
        private int _iterationTime = 0;
        private int _delayNextPageDiscrep = 0;
        private int _delayFilterApply = 0;
        private int _delaySortApply = 0;
        private int _numCheckDiscrep = 0;


        #endregion

        public Case3()
        {
            _arguments.Add(ARG_SONO_CODE, new SingleStringArgument(ARG_SONO_CODE) { Description = "{0} sono_code \t\t\tномер инспекции по которой будет проиходить поиск деклараций", Required = true });

            _arguments.Add(ARG_ITERATION_TIME, new SingleNumericArgument(ARG_ITERATION_TIME){DefaultValue = 500});
            _arguments.Add(ARG_DELAY_NEXT_PAGE_DISCREP, new SingleNumericArgument(ARG_DELAY_NEXT_PAGE_DISCREP) { DefaultValue = 500 });
            _arguments.Add(ARG_DELAY_FILTER_APPLY, new SingleNumericArgument(ARG_DELAY_FILTER_APPLY) { DefaultValue = 600 });
            _arguments.Add(ARG_DELAY_SORT_APPLY, new SingleNumericArgument(ARG_DELAY_SORT_APPLY) { DefaultValue = 700 });
            _arguments.Add(ARG_NUM_OF_DISCREP_TO_CHECK, new SingleNumericArgument(ARG_NUM_OF_DISCREP_TO_CHECK) { DefaultValue = 50 });
            
            _arguments.Add(ARG_IMPERSONATE, new ImpersonateArgument(ARG_IMPERSONATE));
        }

        #region Implementation of ILoadTestCase

        public override void Execute(TestExecutionContext context)
        {
            base.LoadArguments(context.Arguments);

            _sono = GetStringArgValue(ARG_SONO_CODE);
            _iterationTime = GetIntArgValue(ARG_ITERATION_TIME);
            _delayNextPageDiscrep = GetIntArgValue(ARG_DELAY_NEXT_PAGE_DISCREP);
            _delayFilterApply = GetIntArgValue(ARG_DELAY_FILTER_APPLY);
            _delaySortApply = GetIntArgValue(ARG_DELAY_SORT_APPLY);
            _numCheckDiscrep = GetIntArgValue(ARG_NUM_OF_DISCREP_TO_CHECK);


            var impersonateArg = (ImpersonateArgument)_arguments[ARG_IMPERSONATE];

            if (impersonateArg.IsInitialized)
            {
                using (new Impersonation(impersonateArg.DomainName, impersonateArg.UserName, impersonateArg.Password))
                {
                    ExecuteInternal();
                }
            }
            else
            {
                ExecuteInternal();
            }

            WriteLog("Конец");
        }

        public override string GetInfo()
        {
            return string.Empty;
        }

        #endregion

        private void ExecuteInternal()
        {
            foreach (var sono in GetSonos())
            {

            }
        }


        private PageResult<Contractor> ProcessLoadLevel()
        {
            PageResult<Contractor> result = new PageResult<Contractor>();

            var serv = ServiceProxyFactory.CreateServiceProxy<IPyramidDataServiceOld>();
            Request r = new Request()
                {
                    Inn = string.Empty
                };
            var rid = ExtractValueOrDie(serv.SubmitRequest(r));

            RequestStatusType stat = ExtractValueOrDie(serv.RequestStatus(rid));
            while (stat == RequestStatusType.InProcess)
            {
                Sleep(1000, "waiting for load data {0}", rid);
                stat = ExtractValueOrDie(serv.RequestStatus(rid));
            }

            if (stat == RequestStatusType.Ok)
            {
                result = ExtractValueOrDie(serv.LoadContractorsPage(rid, 2, 100, 0, 0));
            }
            else
            {
                throw new ApplicationException(String.Format("RequestStatus returns unsupported status: {0}", stat));
            }

            return result;
        }


        #region Helpers

        private IEnumerable<string> GetSonos()
        {
            if(_sono.Contains(","))
            {
                 return _sono.Split(',');

            }

            return new []{_sono};
        }


        #endregion Helpers
    }
}
