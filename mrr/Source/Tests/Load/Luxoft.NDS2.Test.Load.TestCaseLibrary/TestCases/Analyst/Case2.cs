﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using Luxoft.NDS2.Client;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Test.Load.Common;
using Luxoft.NDS2.Test.Load.Common.Proxy;
using Luxoft.NDS2.Test.Load.Launcher.Probe;
using Luxoft.NDS2.Test.Load.TestCaseLibrary.Base.Arguments;

namespace Luxoft.NDS2.Test.Load.TestCaseLibrary.TestCases.Analyst
{
    [Description(@"Кейс 2. Аналитик работает с ручными выборками. Последовательно выполняет следующие действия:
•	раз в 30 мин. создает выборку, в которую должно попасть 1000 расхождений
•	с задержкой 10 сек. перелистывает список расхождений на следующую страницу
•	с задержкой 15 сек. применяет фильтр на списке расхождений 
•	с задержкой 15 сек. сортирует записи по сумме расхождения
•	отмечает флагом 500 расхождений
•	нажимает кнопку “На согласование”
")]
    public class Case2 : TestCaseBase
    {

        public const string Name = "analyticTest2";

        #region Constants

        public const string ARG_SONO_CODE = "-sono";
        public const string ARG_IMPERSONATE = "-impersonate";

        public const string ARG_ITERATION_TIME = "-iterationTime";

        public const string ARG_DELAY_NEXT_PAGE_DISCREP = "-delayNextPageDiscrep";
        public const string ARG_DELAY_FILTER_APPLY = "-delayFilterApply";
        public const string ARG_DELAY_SORT_APPLY = "-delaySortApply";
        public const string ARG_NUM_OF_DISCREP_TO_CHECK = "-numCheckDiscrep";


        #endregion

        #region Fields

        private string _sono = string.Empty;
        private int _iterationTime = 0;
        private int _delayNextPageDiscrep = 0;
        private int _delayFilterApply = 0;
        private int _delaySortApply = 0;
        private int _numCheckDiscrep = 0;


        #endregion

        public Case2()
        {
            _arguments.Add(ARG_SONO_CODE, new SingleStringArgument(ARG_SONO_CODE) { Description = "{0} sono_code \t\t\tномер инспекции по которой будет проиходить поиск деклараций", Required = true });

            _arguments.Add(ARG_ITERATION_TIME, new SingleNumericArgument(ARG_ITERATION_TIME){DefaultValue = 500});
            _arguments.Add(ARG_DELAY_NEXT_PAGE_DISCREP, new SingleNumericArgument(ARG_DELAY_NEXT_PAGE_DISCREP) { DefaultValue = 500 });
            _arguments.Add(ARG_DELAY_FILTER_APPLY, new SingleNumericArgument(ARG_DELAY_FILTER_APPLY) { DefaultValue = 600 });
            _arguments.Add(ARG_DELAY_SORT_APPLY, new SingleNumericArgument(ARG_DELAY_SORT_APPLY) { DefaultValue = 700 });
            _arguments.Add(ARG_NUM_OF_DISCREP_TO_CHECK, new SingleNumericArgument(ARG_NUM_OF_DISCREP_TO_CHECK) { DefaultValue = 50 });
            
            _arguments.Add(ARG_IMPERSONATE, new ImpersonateArgument(ARG_IMPERSONATE));
        }

        #region Implementation of ILoadTestCase

        public override void Execute(TestExecutionContext context)
        {
            base.LoadArguments(context.Arguments);

            _sono = GetStringArgValue(ARG_SONO_CODE);
            _iterationTime = GetIntArgValue(ARG_ITERATION_TIME);
            _delayNextPageDiscrep = GetIntArgValue(ARG_DELAY_NEXT_PAGE_DISCREP);
            _delayFilterApply = GetIntArgValue(ARG_DELAY_FILTER_APPLY);
            _delaySortApply = GetIntArgValue(ARG_DELAY_SORT_APPLY);
            _numCheckDiscrep = GetIntArgValue(ARG_NUM_OF_DISCREP_TO_CHECK);


            var impersonateArg = (ImpersonateArgument)_arguments[ARG_IMPERSONATE];

            if (impersonateArg.IsInitialized)
            {
                using (new Impersonation(impersonateArg.DomainName, impersonateArg.UserName, impersonateArg.Password))
                {
                    ExecuteInternal();
                }
            }
            else
            {
                ExecuteInternal();
            }

            WriteLog("Конец");
        }

        public override string GetInfo()
        {
            return string.Empty;
        }

        #endregion

        private void ExecuteInternal()
        {
            foreach (var sono in GetSonos())
            {
                StartWatch();
                WriteLog("Creating selection");
                var s = CreateSelection(sono);
                StopWatch();

                var discrepQc = QueryConditions.Empty;
                var declQc = QueryConditions.Empty;

                StartWatch();
                WriteLog("Gettin' declarations");
                var d1 = LoadDeclarations(s.Data.Id, declQc);
                StopWatch();

                StartWatch();
                WriteLog("Gettin' discrepancies");
                var d2 = LoadDiscrepancies(s.Data.Id, discrepQc);
                StopWatch();


                Sleep(_delayNextPageDiscrep, "Pause before moving on next discrepancies page {0}", _delayNextPageDiscrep);
                StartWatch();
                WriteLog("going to the next page...", ConsoleColor.Gray);
                discrepQc.PageSize = 100;
                discrepQc.PageIndex = 2;
                d2 = LoadDiscrepancies(s.Data.Id, discrepQc);
                StopWatch();

                Sleep(_delayFilterApply, "Pause before filter apply at discrepancy list {0}", _delayFilterApply);
                StartWatch();
                WriteLog("filtering...", ConsoleColor.Gray);
                discrepQc.PageSize = 100;
                discrepQc.PageIndex = 1;
                discrepQc.Filter.Add(FilterColumn(TypeHelper<Discrepancy>.GetMemberName(t => t.TaxPayerInn), "123", ColumnFilter.FilterComparisionOperator.NotLike));
                d2 = LoadDiscrepancies(s.Data.Id, discrepQc);
                StopWatch();

                Sleep(_delaySortApply, "Pause before sort apply at discrepancy list {0}", _delaySortApply);
                StartWatch();
                WriteLog("sorting...", ConsoleColor.Gray);
                discrepQc.Sorting.Add(new ColumnSort() { ColumnKey = TypeHelper<Discrepancy>.GetMemberName(t => t.Amount), Order = ColumnSort.SortOrder.Asc});
                discrepQc.PageSize = 100;
                d2 = LoadDiscrepancies(s.Data.Id, discrepQc);
                StopWatch();

                WriteLog("sending for approval ...", ConsoleColor.White);

                SendForApproval(s.Data);

                WriteLog("Promoted ");
            }
        }

        private void SendForApproval(Selection s)
        {
            var serv = ServiceProxyFactory.CreateServiceProxy<ISelectionService>();
            var response = serv.SendToAproval(s);

            s = ExtractValueOrDie(response);
        }

        private SelectionDetails CreateSelection(string sono)
        {
            var serv = ServiceProxyFactory.CreateServiceProxy<ISelectionService>();
            SelectionDetails resultOut = null;


            var filter = new Filter();
            filter.Groups = new []
            {
                new ParameterGroup
                {
                    IsEnabled = true,
                    Parameters = new []
                    {
                        new Parameter
                        {
                            AttributeId = 1, IsEnabled = true, Operator = ComparisonOperator.InList, Values = new []{new ParameterValue{Value = sono.Substring(0,2)}}
                        },
                        new Parameter
                        {
                            AttributeId = 2, IsEnabled = true, Operator = ComparisonOperator.InList, Values = new []{new ParameterValue{Value = sono}}
                        }
                    }
                }
            };

            var s = new Selection()
                {
                    Analytic = "LoadTest",
                    AnalyticSid = UserSid,
                    Name = Guid.NewGuid().ToString(),
                    Filter = filter,
                    Status = SelectionStatus.Loading
                };

            var result = serv.Save(s, ActionType.Create);

            s = ExtractValueOrDie(result);


            var result2 = serv.RequestDiscrepancies(s.Id);

            var requestId = ExtractValueOrDie(result2);

            bool keepWaiting = true;

            while (keepWaiting)
            {
                var statusRes = ExtractValueOrDie(serv.SyncWithRequestStatus(s.Id, s.TypeCode));

                foreach (var item in statusRes)
                  if (s.TypeCode == SelectionType.Hand)
                    {
                        if (statusRes.Count != 1)
                            throw new ApplicationException(string.Format(ResourceManagerNDS2.SelectionRequestNotFound, s.Id));
                        else
                        {
                            switch (statusRes.First().RequestStatus)
                            {
                                case SelectionRequestStatus.New:
                                case SelectionRequestStatus.InQueue:
                                case SelectionRequestStatus.AcceptedInQueue:
                                case SelectionRequestStatus.PlanReceived:
                                case SelectionRequestStatus.SelectionCreated:
                                case SelectionRequestStatus.SelectionRecorded:
                                    Thread.Sleep(2000);
                                    break;
                                case SelectionRequestStatus.DeclarationsRecorded:
                                    keepWaiting = false;
                                    break;
                                case SelectionRequestStatus.PlanError:
                                case SelectionRequestStatus.SelectionError:
                                case SelectionRequestStatus.SelectionRecordError:
                                case SelectionRequestStatus.DeclarationsRecordError:
                                    throw new ApplicationException(string.Format(ResourceManagerNDS2.LoadError, statusRes.First().RequestStatus));
                                default:
                                    throw new ApplicationException(string.Format(ResourceManagerNDS2.SelectionUnknownSatus, statusRes.First().RequestStatus));
                            }
                        }
                    }
                }

            resultOut = ExtractValueOrDie(serv.Load(s.Id, String.Empty, SelectionType.Hand));

            return resultOut;
        }


        private DeclarationPageResult LoadDeclarations(long reqId, QueryConditions c)
        {
            DeclarationPageResult res = null;

            var serv = ServiceProxyFactory.CreateServiceProxy<ISelectionsDataService>();
            var response = serv.SearchDeclarations(reqId, c);

            res = ExtractValueOrDie(response);

            return res;
        }


        private PageResult<SelectionDiscrepancy> LoadDiscrepancies(long reqId, QueryConditions c)
        {
            PageResult<SelectionDiscrepancy> res = null;

            var serv = ServiceProxyFactory.CreateServiceProxy<ISelectionService>();
            var response = serv.SearchDiscrepancies(reqId, true, c);

            res = ExtractValueOrDie(response);

            return res;
        }

        #region Helpers

        private IEnumerable<string> GetSonos()
        {
            if(_sono.Contains(","))
            {
                 return _sono.Split(',');

            }

            return new []{_sono};
        }


        #endregion Helpers
    }
}
