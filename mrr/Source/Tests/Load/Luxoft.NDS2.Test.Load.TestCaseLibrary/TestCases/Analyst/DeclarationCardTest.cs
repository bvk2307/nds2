﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Test.Load.Common;
using Luxoft.NDS2.Test.Load.Common.Base;
using Luxoft.NDS2.Test.Load.Common.Interfaces;
using Luxoft.NDS2.Test.Load.Common.Proxy;
using Luxoft.NDS2.Test.Load.TestCaseLibrary.Base.Arguments;

namespace Luxoft.NDS2.Test.Load.TestCaseLibrary.TestCases.Analyst
{
    public class DeclarationCardTest : TestCaseBase
    {
        public const string ARG_CYCLES_COUNT = "-cycles";
        public const string ARG_SONO_CODE = "-sono";
        public const string ARG_CHAPTER = "-chapter";
        public const string ARG_IMPERSONATE = "-impersonate";


        public const string Name = "declarationCard";

        public DeclarationCardTest()
        {
            _arguments.Add(ARG_CYCLES_COUNT, new SingleNumericArgument(ARG_CYCLES_COUNT) { DefaultValue = 10, Description = "{0} number кол-во деклараций для обработки", Required = true });
            _arguments.Add(ARG_CHAPTER, new SingleNumericArgument(ARG_CHAPTER) { Description = "{0} number \t\t\tвыгружаемого раздела", DefaultValue = 8 });
            _arguments.Add(ARG_SONO_CODE, new SingleStringArgument(ARG_SONO_CODE) { Description = "{0} sono_code \t\t\tномер инспекции по которой будет проиходить поиск деклараций", Required = true });
            _arguments.Add(ARG_IMPERSONATE, new ImpersonateArgument(ARG_IMPERSONATE));
        }

        #region Implementation of ILoadTestCase


        public override void Execute(TestExecutionContext context)
        {
            /*LoadArguments(context.Arguments);
            
            var sono = GetStringArgValue(ARG_SONO_CODE);
            
            var testHelperService = ServiceProxyFactory.CreateServiceProxy<ILoadTestHelpService>();


            var freeDeclList = testHelperService.GetDeclarationForBookLoad(sono, 8, 10);
            var blService = ServiceProxyFactory.CreateServiceProxy<IDeclarationsDataService>();

            foreach (var declId in freeDeclList.Result)
            {
                Console.WriteLine("Processing declaration {0}", declId);
                var decl = blService.GetDeclaration(declId).Result;

                Console.WriteLine("Requesting chapter 8...");
                Console.WriteLine("Initial call");

                var request = new InvoiceSovOperation();
                request.Chapter = 8;
                request.decl = new DeclarationRequestData(decl);

                var requestChapterResult = blService.GetSovInvoices(request);
                LogChapterResponse(requestChapterResult);
                int iterationNum = 0;
                while (requestChapterResult.Result.ExecutionStatus == RequestStatusType.InProcess)
                {
                    Console.WriteLine("Iteration {0}", iterationNum++);
                    Thread.Sleep(500);

                    requestChapterResult = blService.GetSovInvoices(requestChapterResult.Result);
                    LogChapterResponse(requestChapterResult);
                }

                Console.WriteLine();
            }*/
        }

        private void LogChapterResponse(OperationResult<InvoiceSovOperation> response)
        {
            Console.WriteLine("response status: {0}", response.Status);
            Console.WriteLine("sov request id: {0}", response.Result.RequestId);
            Console.WriteLine("sov status: {0}", response.Result.ExecutionStatus);
            //Console.WriteLine("sov data rows: {0}", response.Result.Result.Count);
        }

        public string GetInfo()
        {
            var sb = new StringBuilder();
            sb.AppendLine("Тест инициирующий загрузку декларации в МРР для дальнейшего отображения в МРР");
            sb.AppendLine("Параметры");
            foreach (var testCaseArgument in _arguments)
            {
                sb.AppendLine(testCaseArgument.Value.GetArgumentInfo());
            }

            return sb.ToString();
        }

        public void LoadArguments(Queue<string> args)
        {
            while (args.Count > 0)
            {
                var argName = args.Dequeue();

                if (_arguments.ContainsKey(argName))
                {
                    _arguments[argName].ReadArgumentValues(args);
                }
            }
        }

        #endregion
    }
}
