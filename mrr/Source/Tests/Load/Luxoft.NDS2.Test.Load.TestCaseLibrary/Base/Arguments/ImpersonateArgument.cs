﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Luxoft.NDS2.Test.Load.Common.Base;
using Luxoft.NDS2.Test.Load.Common.Interfaces;
using Luxoft.NDS2.Test.Load.TestCaseLibrary.Helpers;

namespace Luxoft.NDS2.Test.Load.TestCaseLibrary.Base.Arguments
{
    public sealed class ImpersonateArgument : TestCaseArgument
    {
        private readonly string _argName;

        public string DomainName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        public bool IsInitialized { get; private set; }

        public ImpersonateArgument(string argName) : base(argName)
        {
        }

        #region Overrides of TestCaseArgument

        public override bool Required { get; set; }

        public override void ReadArgumentValues(Queue<string> args)
        {
            DomainName = args.Dequeue();
            UserName = args.Dequeue();
            Password = args.Dequeue();

            IsInitialized = true;
        }

        public override string GetArgumentInfo()
        {
            return string.Format("{0} domain_name user_name password", _argName);
        }

        #endregion
    }
}
