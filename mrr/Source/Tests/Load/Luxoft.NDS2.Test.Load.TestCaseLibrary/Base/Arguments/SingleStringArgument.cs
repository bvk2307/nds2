﻿using System.Collections.Generic;
using Luxoft.NDS2.Test.Load.Common.Base;

namespace Luxoft.NDS2.Test.Load.TestCaseLibrary.Base.Arguments
{
    public sealed class SingleStringArgument : TestCaseArgument
    {
        private readonly string _description;
        public override bool Required { get; set; }
        public string Description { get; set; }

        /// <summary>
        /// Значение Аргумента
        /// </summary>
        public string Value { get; private set; }

        public SingleStringArgument(string argName) : base(argName){}

        #region Overrides of TestCaseArgument
        
        public override void ReadArgumentValues(Queue<string> args)
        {
            Value = args.Dequeue();
        }

        public override string GetArgumentInfo()
        {
            return string.Format(_description, _argName);
        }

        #endregion
    }
}
