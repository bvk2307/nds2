﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Luxoft.NDS2.Test.Load.Common.Base;
using Luxoft.NDS2.Test.Load.Common.Interfaces;
using Luxoft.NDS2.Test.Load.TestCaseLibrary.Helpers;

namespace Luxoft.NDS2.Test.Load.TestCaseLibrary.Base.Arguments
{
    public sealed class DelayArgument : TestCaseArgument
    {
        private const string _argName = "-delay";

        /// <summary>
        /// задержка перед выполнением очередной итерации в миллисекундах
        /// </summary>
        public int Delay { get; private set; }

        public DelayArgument() : base(_argName){}

        #region Overrides of TestCaseArgument

        public override void ReadArgumentValues(Queue<string> args)
        {
            Delay = Converter.ToInt(args.Dequeue(), 0);
        }

        public override string GetArgumentInfo()
        {
            return string.Format("{0} milisec      Задержка в миллисекундах", _argName);
        }

        #endregion
    }
}
