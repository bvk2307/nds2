﻿using System.Collections.Generic;
using Luxoft.NDS2.Test.Load.Common.Base;
using Luxoft.NDS2.Test.Load.TestCaseLibrary.Helpers;

namespace Luxoft.NDS2.Test.Load.TestCaseLibrary.Base.Arguments
{
    public sealed class SingleNumericArgument : TestCaseArgument
    {
        /// <summary>
        /// Описание аргумента
        /// </summary>
        public string Description { get; set; }

        
        public override bool Required { get; set; }

        /// <summary>
        /// Значение по умолчанию
        /// </summary>
        public int DefaultValue 
        { 
            get { return Value; }
            set { Value = value; }
        }

        

        /// <summary>
        /// Значение Аргумента
        /// </summary>
        public int Value { get; private set; }

        public SingleNumericArgument(string argName): base(argName)
        {
        }

        #region Overrides of TestCaseArgument

        public override void ReadArgumentValues(Queue<string> args)
        {
            Value = Converter.ToInt(args.Dequeue(), DefaultValue);
        }

        public override string GetArgumentInfo()
        {
            return string.Format(Description, _argName);
        }

        #endregion
    }
}
