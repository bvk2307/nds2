﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Luxoft.NDS2.Test.Load.Common.Base;
using Luxoft.NDS2.Test.Load.Common.Interfaces;
using Luxoft.NDS2.Test.Load.TestCaseLibrary.Helpers;

namespace Luxoft.NDS2.Test.Load.TestCaseLibrary.Base.Arguments
{
    public sealed class CyclesCountArgument : TestCaseArgument
    {
        private const string _argName = "-cycles";

        /// <summary>
        /// Кол-воциклов запуска задачи
        /// </summary>
        public int Count { get; set; }

        public CyclesCountArgument() : base(_argName) { }

        #region Overrides of TestCaseArgument

        public override void ReadArgumentValues(Queue<string> args)
        {
            Count = Converter.ToInt(args.Dequeue(), 1);
        }

        public override string GetArgumentInfo()
        {
            return string.Format("{0} number", _argName);
        }

        #endregion
    }
}
