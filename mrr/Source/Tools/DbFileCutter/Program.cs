﻿using System;
using System.IO;

namespace DbFileCutter
{
    class Program
    {
        private static int _pocketSize = 1024;

        #region Log

        static void LogError(string msg)
        {
            Console.WriteLine("[ERROR]   " + msg);
        }
        static void LogWarning(string msg)
        {
            Console.WriteLine("[WARNING] " + msg);
        }
        static void LogInfo(string msg)
        {
            Console.WriteLine("[INFO]    " + msg);
        }

        static void LogData(string msg)
        {
            Console.WriteLine(msg);
        }
        #endregion Log

        static void Main(string[] args)
        {
            if (args == null || args.Length == 0)
            {
                LogError("No arguments");
                LogInfo("Usage:");
                LogData("\tDbFileCutter [-s #] <path>");
                LogData(String.Concat("\t-s - Size of block (default ", _pocketSize, " chars)"));
                LogData("\tpath - Path to file");
                return;
            }

            var i = 0;
            var path = string.Empty;
            while (i < args.Length)
            {
                var arg = args[i];
                switch (arg.ToLower())
                {
                    case "-s":
                    case "-size":
                    case "-blocksize":
                        int k;
                        ++i;
                        if (args.Length > i && int.TryParse(args[i], out k))
                        {
                            _pocketSize = k;
                        }
                        break;
                    default:
                        path = args[i];
                        break;
                }
                ++i;
            }

            if (!File.Exists(path))
            {
                LogError(string.Format("File \"{0}\" not found", path));
                return;
            }
            LogInfo(string.Format("Prepare script for \"{0}\"", path));
            WriteScript(path);
            LogInfo("Finished");
        }

        private static void WriteScript(string src)
        {
            var path = src + ".sql";
            using (var stream = new FileStream(path, FileMode.Create))
            using (var file = new StreamWriter(stream))
            {
                file.WriteLine("DECLARE\r\n\tC1 clob;\r\nBEGIN");

                var first = true;
                file.Write("c1 := '");

                using (var reader = new StreamReader(src))
                {
                    var k = 0;
                    while (!reader.EndOfStream)
                    {
                        var c = (char)reader.Read();
                        file.Write(c);
                        ++k;
                        if (k < _pocketSize)
                            continue;
                        file.WriteLine(first ? "';" : "');");
                        file.WriteLine("dbms_output.put_line(dbms_lob.getlength(c1));");
                        file.Write("dbms_lob.append(c1, '");
                        first = false;
                        k = 0;
                    }
                }
                file.WriteLine(first ? "';" : "');");
                file.WriteLine("dbms_output.put_line(dbms_lob.getlength(c1));");

                file.WriteLine();
                file.WriteLine("DBMS_XMLSCHEMA.registerSchema");
                file.WriteLine("(");
                file.WriteLine("\tSCHEMAURL => '{0}',", Path.GetFileName(src));
                file.WriteLine("\tSCHEMADOC => C1,");
                file.WriteLine("\tgenTypes => false,");
                file.WriteLine("\tgenTables => false,");
                file.WriteLine("\tlocal => true,");
                file.WriteLine("\towner => 'NDS2_MRR_USER'");
                file.WriteLine(");");
                file.WriteLine("exception when others then null;");
                file.WriteLine("END;");
                file.WriteLine("/");

                file.Close();
            }
        }
    }
}
