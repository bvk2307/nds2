﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Input;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using FLS.CommonComponents.App.Processes;
using Luxoft.NDS2.Client.GraphTreeViewer.Bases;
using Luxoft.NDS2.Client.GraphTreeViewer.Infrastructure;
using Luxoft.NDS2.Client.GraphTreeViewer.Presenters;
using Luxoft.NDS2.Client.GraphTreeViewer.Services;
using Luxoft.NDS2.Client.NodeTreePanel;
using Luxoft.NDS2.Client.UI.GraphTree;
using Luxoft.NDS2.Client.UI.Reports.Excel.GraphTree;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting.TaxPayer;
using Luxoft.NDS2.Common.Helpers.IO;
using Microsoft.Win32;

namespace Luxoft.NDS2.Client.GraphTreeViewer.Views
{
    /// <summary>
    /// Interaction logic for ReportersView.xaml
    /// </summary>
    public partial class ReportersView : BaseViewWindow
    {
        private const int _mostImportantContractors = 250;
        private const int _maxLevels = 4;

        private readonly ReportsPresenter _presenter;

        private IUcMessageService _ucMessageService = null;
        private SaveFileDialog _saveFileDialogReport = null;
        private FileProcessStarter _fileProcessStarter = null;

        public ReportersView()
        {
            ServiceContext services = AppMain.InitServices();

            RootWorkItem rootWorkItem = AppMain.InitRootWorkItem( services );

            base.InitWithWorkItem( rootWorkItem );

            _ucMessageService = rootWorkItem.Services.Get<IUcMessageService>( ensureExists: true );

            var featureContext = new FeatureContextBase( Guid.NewGuid() );
            var presentationContext = new PresentationContext( Guid.NewGuid(), null, featureContext );

            _presenter = new ReportsPresenter( presentationContext, rootWorkItem, this );
            PresenterTestable = _presenter;

            InitializeComponent();
            Init();
            excelReportButton.Click += ExcelReportButtonClick;
        }

        private void Init()
        {
            var years = new List<int>();

            for (int i = DateTime.Now.Year - 5; i <= DateTime.Now.Year; i++)
            {
                years.Add(i);
            }

            yearComboBox.ItemsSource = years;
            yearComboBox.SelectedIndex = years.Count - 2;
            qrtComboBox.ItemsSource = new List<int> { 1, 2, 3, 4 };
            qrtComboBox.SelectedIndex = 0;
            ndsPrcTextBox.Text = "1";
            countNodesTextBox.Text = "5";
        }

        public void ExcelReportButtonClick(object sender, RoutedEventArgs e)
        {
            //apopov 22.2.2016	//DEBUG!!!
            //_presenter.SwitchDataServiceType( useGerationStub: true );

            GraphNodesKeyParameters nodesKeyParameters = GetTESTDeclarationKeyParameters();
            nodesKeyParameters = ReadCurrentKeyParameters( nodesKeyParameters );

            ExportTreeExcel( nodesKeyParameters );
        }

        private GraphNodesKeyParameters ReadCurrentKeyParameters( GraphNodesKeyParameters templateParameters )
        {
            string inn = innTextBox.Text;

            bool? isPurchase = byPurchase.IsChecked;

            int year = Convert.ToInt32(yearComboBox.SelectedValue);
            int qrt = Convert.ToInt32(qrtComboBox.SelectedValue);

            int percent;
            int? bnPercent = int.TryParse( ndsPrcTextBox.Text, out percent ) ? (int?)percent : null;

            int minNodesCount;
            int? bnMinNodesCount = int.TryParse( countNodesTextBox.Text, out minNodesCount ) ? (int?)minNodesCount: null;

            return GraphNodesKeyParameters.New(
                templateParameters, isPurchase, year, qrt, bnPercent, _mostImportantContractors, layersLimit: _maxLevels, 
                minReturnedContractors: bnMinNodesCount, ids: new[] { new TaxPayerId(inn, kppCode: null) });
        }

        private void ExportTreeExcel( GraphNodesKeyParameters nodesKeyParameters )
        {
            GraphTreeNodeReportData reportData = InitExportTreeReport( nodesKeyParameters );

            ExportTreeExcel( reportData, _presenter.GenerateTreeExcelFileName( reportData ), nodesKeyParameters );
        }

        private GraphTreeNodeReportData InitExportTreeReport( GraphNodesKeyParameters nodesKeyParameters )
        {
            GraphData graphData = new GraphData { TaxPayerId = nodesKeyParameters.TaxPayerId };

            GraphTreeNodeData treeNodeData = new GraphTreeNodeData( 
                GraphTreeNodeData.NoNodeKey, graphData: graphData );
            GraphTreeNodeReportData reportData = new GraphTreeNodeReportData( treeNodeData, nodesKeyParameters );

            return reportData;
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            var regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private SaveFileDialog SaveFileDialogReport
        {
            get
            {
                if ( _saveFileDialogReport == null )
                {
                    var saveFileDialog = new SaveFileDialog();
                    string fileExtension = BaseNodeReporter<NodeReportManager>.ReportExcelFileExtensionName;
                    saveFileDialog.DefaultExt = string.Concat( ".", fileExtension );
                    saveFileDialog.Filter = string.Format( "Excel отчёты (.{0})|*.{0}", fileExtension );
                    _saveFileDialogReport = saveFileDialog;
                }
                return _saveFileDialogReport;
            }
        }

        private void ExportTreeExcel( GraphTreeNodeReportData reportData, string fileFullSpec, GraphNodesKeyParameters nodesKeyParameters )
        {
            var fileSpecWoExtension = Path.GetFileNameWithoutExtension( fileFullSpec );
            SaveFileDialogReport.FileName = FileHelper.CleanFileName(fileSpecWoExtension);
            // ReSharper disable once PossibleInvalidOperationException
            if (SaveFileDialogReport.ShowDialog().Value)
            {
                this.nodeTreeProgressBar.Visibility = Visibility.Visible;
                _presenter.StartGenerateTreeExcelAsync(SaveFileDialogReport.FileName, reportData, nodesKeyParameters);
            }
        }

        public void HideProgerssBar()
        {
            this.nodeTreeProgressBar.Visibility = Visibility.Hidden;
        }

        public void OpenTreeExcelFile( string reportExcelFilePath )
        {
            ProcessStarter.StartFileProcessAndShowErrorIfPresent(
                reportExcelFilePath, ResourceManagerNDS2.ExportExcelMessages.ExcelExportDialogTitle,
                string.Format( ResourceManagerNDS2.ExportExcelMessages.FileFormedIsItOpen, reportExcelFilePath ),
                string.Format( ResourceManagerNDS2.ExportExcelMessages.FileExcelOpenError, reportExcelFilePath ) );
        }

        protected IUcMessageService MessageService { get { return _ucMessageService; } }

        private FileProcessStarter ProcessStarter
        {
            get
            {
                return _fileProcessStarter ?? ( _fileProcessStarter = new FileProcessStarter( MessageService ) );
            }
        }
    }
}
