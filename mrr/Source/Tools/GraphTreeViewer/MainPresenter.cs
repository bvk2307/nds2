﻿//#define DONOTDISABLE

using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using System.Windows.Media;
using CommonComponents.Configuration;
using CommonComponents.Shared;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using FLS.Common.Lib.Collections.Extensions;
using FLS.CommonComponents.Lib.Interfaces;
using Luxoft.NDS2.Client.Config;
using Luxoft.NDS2.Client.GraphTreeViewer.Bases;
using Luxoft.NDS2.Client.GraphTreeViewer.DataServices;
using Luxoft.NDS2.Client.GraphTreeViewer.Declarations;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.NodeTreePanel;
using Luxoft.NDS2.Client.NodeTreePanel.Extensions;
using Luxoft.NDS2.Client.NodeTreePanel.Interfaces;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.GraphTree;
using Luxoft.NDS2.Client.UI.Reports.Excel.GraphTree;
using Luxoft.NDS2.Client.UI.Reports.Interfaces;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Common.Helpers.Enums;
using Luxoft.NDS2.Common.Helpers.Executors;
using Luxoft.NDS2.Server.Services;
using Microsoft.Practices.CompositeUI;

namespace Luxoft.NDS2.Client.GraphTreeViewer
{
    public sealed class MainPresenter : BasePresenter<MainWindow>, IInstanceProvider<ICommandTransactionManager>, ITestablePresenter
    {
        #region Private members

        private const int CallDispatcherInputBoundCapacity      = 1;
        private const int CallDispatcherLowInputBoundCapacity   = 1000;

        private readonly IDeclarationUserPermissionsPolicy _declarationUserPermissionsPolicy;
        private readonly IServiceCallDispatcherFactory _serviceCallDispatcherFactory;
        private readonly DependencyTreeSection _dependencyTreeSection;
        private readonly IExecutorWErrorHandling _executorWErrorHandling;

	//apopov 21.6.2016	//DEBUG!!!
        //private readonly InfoDeductionDetailSumsAction _infoDeductionDetailSumsAction;

        private CancellationTokenSource _cancelSource = null;
        private IGraphTreeDataManager _graphTreeDataManager = null;
        private IServiceCallDispatcher<DeductionDetailsKeyParameters, int, Guid, IReadOnlyCollection<DeductionDetail>, IReadOnlyCollection<DeductionDetail>> _deductionDetailsDispatcher = null;
        private IServiceCallDispatcher<GraphNodesKeyParameters, int, Guid, IReadOnlyCollection<GraphData>, IReadOnlyCollection<GraphData>> _serviceCallDispatcher = null;
        private ActionBlock<CallExecContext<GraphNodesKeyParameters, int, Guid, IReadOnlyCollection<GraphData>>> _dataProcessor = null;

        private ICommandTransactionManager _commandManager = null;
        private GraphTreeObjectTransformer _graphTreeObjectManager = null;
        /// <summary> The current build number of the graph tree. </summary>
        private int _graphBuildCounter = -1;

        private bool _useDataGenerationStub = false;

        /// <summary> Parameters of the sheet with tree graph. </summary>
        private GraphTreeParameters _treeTabTreeParameters = null;

        //<< Reports
        private CancellationTokenSource _cancelSourceTreeExport = null;

        private IPyramidDataService _pyramidDataService = null;
        private INodeAllTreeReporter _nodeTreeReporter = null;
        // Reports >>

        #endregion Private members

        public MainPresenter( PresentationContext presentationContext, WorkItem workItem, MainWindow view, IExecutorWErrorHandling executorWErrorHandling ) 
            : base( presentationContext, workItem, view )
        {
            Contract.Requires( presentationContext != null );
            Contract.Requires( workItem != null );
            Contract.Requires( view != null );
            Contract.Requires( executorWErrorHandling != null );

            _executorWErrorHandling             = executorWErrorHandling;

            _declarationUserPermissionsPolicy   = new DeclarationUserPermissionsDummyPolicy<MainWindow>( this );
            _serviceCallDispatcherFactory       = new ServiceCallDispatcherFactory( ServiceCallExecutorManagerFactory.CurrentManager );

            IConfigurationDataService configurationDataService = Services.Get<IConfigurationDataService>();

            Contract.Assume( configurationDataService != null );

            _dependencyTreeSection = configurationDataService.GetSection<DependencyTreeSection>( ProfileInfo.Default );

	//apopov 21.6.2016	//DEBUG!!!
            //_infoDeductionDetailSumsAction      = View.CreateDeductionDetailsAction( graphTreeDataManager, this );
        }

        public IDeclarationUserPermissionsPolicy UserPermissionsPolicy
        {
            get { return _declarationUserPermissionsPolicy; }
        }

        public void InitInfrastructure( GraphTreeParameters graphTreeParameters, IReadOnlyDictionary<int, Tuple<SolidColorBrush, string>> sur )
        {
            _graphTreeObjectManager = new GraphTreeObjectTransformer( graphTreeParameters, UserPermissionsPolicy, sur );
        }

        public void StartLoadTree()
        {
            ++_graphBuildCounter;

            CancelDataLoadingIfNeeded();

            if ( _commandManager != null ) //otherwise there is nothing to clear yet
            {
                using ( _commandManager )
                {
                    _commandManager.ExecuteInTransaction( "ClearTreeNodes", View.ClearTreeNodes );
                    _commandManager = null; //will be initiated again below (in StartLoadNodes())
                }
            }
            InitServiceCallDispatcher();

            _commandManager = View.StartLoadTree( out _treeTabTreeParameters );	//apopov 29.3.2016	//TODO!!!   remove when GraphTreeParameters will be passed through out the data loading pipeline instead of GraphNodesKeyParameters
        }

        private void InitServiceCallDispatcher()
        {
            _cancelSource = new CancellationTokenSource();
            CancellationToken cancelToken = _cancelSource.Token;

            _serviceCallDispatcher = 
                _serviceCallDispatcherFactory.Open<GraphNodesKeyParameters, int, Guid, IReadOnlyCollection<GraphData>, IReadOnlyCollection<GraphData>>( 
                    TransformNodeData, GetNodes, cancelToken, CallDispatcherInputBoundCapacity, CallDispatcherLowInputBoundCapacity );

            _dataProcessor = new ActionBlock<CallExecContext<GraphNodesKeyParameters, int, Guid, IReadOnlyCollection<GraphData>>>(
                (Action<CallExecContext<GraphNodesKeyParameters, int, Guid, IReadOnlyCollection<GraphData>>>)OnDataLoaded,
                new ExecutionDataflowBlockOptions
                {
                    CancellationToken = cancelToken, TaskScheduler = TaskScheduler.FromCurrentSynchronizationContext(),
                    NameFormat = "OnDataLoaded {0}, ID {1} GraphTreePresenter"
                } );

            _serviceCallDispatcher.Output.LinkTo( _dataProcessor, new DataflowLinkOptions { PropagateCompletion = true } ); //subscribes to receiving nodes

            _deductionDetailsDispatcher = GetGraphTreeDataManager().InitDeductionDetailsDataDispatcher(
                context => context, OnDeductionDetaislDataLoaded, _cancelSource, OnDataProcessorFaultOrCancel, "OnNodesDataLoaded {0}, ID {1} GraphTreePresenter");
        }

        private void CancelDataLoadingIfNeeded()
        {
            if ( _dataProcessor != null && !_dataProcessor.Completion.IsCompleted )
                _cancelSource.Cancel();
        }

        /// <summary> Starts tree node data loading. </summary>
        /// <param name="graphTreeDataRequestArgs"></param>
        public void StartLoadNodes( GraphTreeDataRequestArgs graphTreeDataRequestArgs )
        {
            Contract.Requires( graphTreeDataRequestArgs != null );
            Contract.Requires( graphTreeDataRequestArgs.Parameters != null );

            CallExecPriority priority = graphTreeDataRequestArgs.Priority;

            StartTreeDataRequest( priority );

            ITargetBlock<CallExecContext<GraphNodesKeyParameters, int, Guid, IReadOnlyCollection<GraphData>>> inputBlock = 
                priority.IsLoadingCountsOnly() ? _serviceCallDispatcher.InputLow : _serviceCallDispatcher.Input;

            bool success = inputBlock.Post( new CallExecContext<GraphNodesKeyParameters, int, Guid, IReadOnlyCollection<GraphData>>( 
                graphTreeDataRequestArgs.Parameters.KeyParameters, instanceKey: _graphBuildCounter, 
                contextKey: graphTreeDataRequestArgs.ParentKey ) );  //it is a signal to request a new tree data if it is GraphTreeNodeData.NoNodeKey

            if ( !success )
            {
                if ( !graphTreeDataRequestArgs.Priority.IsLoadingCountsOnly() )
                {
                    CommandManager.RollbackTransaction();
                    
                    IgnoreOfTreeDataRequest(); //ignores user calls those could not be serviced by the service call dispatcher
                }
            }
        }

        /// <summary> Starts deduction detail sums data loading. </summary>
        /// <param name="deductionDetailsDataRequestArgs"></param>
        public void StartDeductionDetails( DeductionDetailsDataRequestArgs deductionDetailsDataRequestArgs )
        {
            Contract.Requires( deductionDetailsDataRequestArgs != null );
            Contract.Requires( deductionDetailsDataRequestArgs.Parameters != null );

            DeductionDetailsKeyParameters keyParameters = deductionDetailsDataRequestArgs.DeductionKeyParameters;
            int requestCmdManagerInstanceKey = deductionDetailsDataRequestArgs.CommandManager.InstanceKey;

            StartDeductionDetailsRequest();

            bool success = this._graphTreeDataManager.PostTo( _deductionDetailsDispatcher, new DeductionDetailsKeyParameters( keyParameters ),
                instanceKey: requestCmdManagerInstanceKey, contextKey: deductionDetailsDataRequestArgs.ParentNodeId );

            if ( !success )
            {
                CommandManager.RollbackTransaction();
                
                IgnoreOfDeductionDetailsRequest( requestCmdManagerInstanceKey, keyParameters ); //ignores user calls those could not be serviced by the service call dispatcher
            }
        }

        private void StartTreeDataRequest( CallExecPriority priority )
        {
#if !( DEBUG && DONOTDISABLE )

            bool isLoadingCountsOnly = priority.IsLoadingCountsOnly();
            if ( !isLoadingCountsOnly )
                CommandManager.StartTransaction( "BuildTreeBranch" );

#endif //!DONOTDISABLE
        }

        private void StartDeductionDetailsRequest()
        {
#if !( DEBUG && DONOTDISABLE )

            CommandManager.StartTransaction( "InfoDeductionDetailSums" );

#endif //!DONOTDISABLE
        }

        private CallExecContext<GraphNodesKeyParameters, int, Guid, IReadOnlyCollection<GraphData>> 
            TransformNodeData( CallExecContext<GraphNodesKeyParameters, int, Guid, IReadOnlyCollection<GraphData>> context )
        {
            return context;
        }

        private void OnDataLoaded( CallExecContext<GraphNodesKeyParameters, int, Guid, IReadOnlyCollection<GraphData>> callResult )
        {
            _executorWErrorHandling.Execute( (Func<CallExecContext<GraphNodesKeyParameters, int, Guid, IReadOnlyCollection<GraphData>>, CancellationToken, bool>)
                     ProcessData, callResult );
        }

        private void OnDeductionDetaislDataLoaded( 
            CallExecContext<DeductionDetailsKeyParameters, int, Guid, IReadOnlyCollection<DeductionDetail>> callResult, CancellationToken cancelToken )
        {
            _executorWErrorHandling.Execute( (Func<CallExecContext<DeductionDetailsKeyParameters, int, Guid, IReadOnlyCollection<DeductionDetail>>, CancellationToken, bool>)
                     ProcessDeductionDetailsData, callResult, cancelToken );
        }

        private bool ProcessData( CallExecContext<GraphNodesKeyParameters, int, Guid, IReadOnlyCollection<GraphData>> callResult, 
                                  CancellationToken cancelToken = default(CancellationToken) )
        {
            IReadOnlyCollection<GraphData>  nodes           = callResult.Result;
            Guid parentNodeKey              = callResult.ContextKey;
            CallExecPriority priority       = callResult.Priority;
            int instanceKey                 = callResult.InstanceKey;

            bool isLoadingCountsOnly        = callResult.Priority.IsLoadingCountsOnly();
            bool success = false;
            if ( isLoadingCountsOnly )
            {
                success = ProcessDataWOTransaction( cancelToken, nodes, parentNodeKey, priority, instanceKey );
            }
            else
            {
                success = ProcessDataTransactional( cancelToken, nodes, parentNodeKey, priority, instanceKey );
                if ( !success )
                    IgnoreOfTreeDataRequest(); //ignores user calls those could not be serviced by the service call dispatcher
            }
            return success;
        }

        /// <summary> </summary>
        /// <param name="cancelToken"></param>
        /// <param name="nodes"></param>
        /// <param name="parentNodeKey"> A parent node key <see cref="GraphTreeNodeData.Key"/> or <see cref="GraphTreeNodeData.NoNodeKey"/> if the node is root one. </param>
        /// <param name="priority"></param>
        /// <param name="instanceKey"></param>
        /// <returns></returns>
        private bool ProcessDataWOTransaction( 
            CancellationToken cancelToken, IReadOnlyCollection<GraphData> nodes, Guid parentNodeKey, CallExecPriority priority, int instanceKey )
        {
            bool success = nodes != null && _graphBuildCounter == instanceKey;
            if ( success ) //ignores results from different graph tree builds
            {
                success = !cancelToken.IsCancellationRequested;
                if ( success )
                {
                    IEnumerable<GraphData> nodesToProcess = parentNodeKey == GraphTreeNodeData.NoNodeKey    //builds a new tree if 'parentNodeKey == GraphTreeNodeData.NoNodeKey'
                        ? nodes : nodes.Where( n => n.ParentInn != null );    //excludes the parent node. It does not use a value of the parent node identifier only flag 'does a node have a parent or not?'

                    if ( priority.IsLoadingCountsOnly() )
                    {
                        var graphDatas = nodesToProcess.ToArray();
                        var nodesToProcessCount = graphDatas.Count(x => !x.IsReverted);
                        var revertedNodesToProcessCount = graphDatas.Length - nodesToProcessCount;

                        success = View.SetChildCounter( parentNodeKey, nodesToProcessCount, revertedNodesToProcessCount );
                    }
                    else
                    {
                        IEnumerable<GraphTreeNodeData> graphNodes = 
                            _graphTreeObjectManager.TransformTreeData( nodesToProcess, parentNodeKey );

                        success = !cancelToken.IsCancellationRequested;
                        if ( success )
                            success = View.BuildTreeBranch( parentNodeKey, graphNodes );
                    }
                }
            }
            return success;
        }

        private bool IsRequestOfActualCommandManager( int instanceKey )
        {
            return _commandManager.InstanceKey == instanceKey;
        }

        /// <summary> </summary>
        /// <param name="cancelToken"></param>
        /// <param name="nodes"></param>
        /// <param name="parentNodeKey"> A parent node key <see cref="GraphTreeNodeData.Key"/> or <see cref="GraphTreeNodeData.NoNodeKey"/> if the node is root one. </param>
        /// <param name="priority"></param>
        /// <param name="instanceKey"></param>
        /// <returns></returns>
        private bool ProcessDataTransactional( CancellationToken cancelToken, IReadOnlyCollection<GraphData> nodes, Guid parentNodeKey, CallExecPriority priority, int instanceKey )
        {
            bool success = false;
            bool toRollback = false;

#if ( DEBUG && DONOTDISABLE )

            _commandManager.StartTransaction( "BuildTreeBranch" );

#endif //DONOTDISABLE
            try
            {
                success = ProcessDataWOTransaction( cancelToken, nodes, parentNodeKey, priority, instanceKey );
            }catch( Exception )
            {
                toRollback = true;

                throw;
            }finally
            {
                if ( !success || toRollback )
                     CommandManager.RollbackTransaction();
                else CommandManager.CommitTransaction( "BuildTreeBranch" );
            }
            return success;
        }

        private bool ProcessDeductionDetailsData( CallExecContext<DeductionDetailsKeyParameters, int, Guid, IReadOnlyCollection<DeductionDetail>> callResult, 
                                  CancellationToken cancelToken = default(CancellationToken) )
        {
            IReadOnlyCollection<DeductionDetail> deductionDetails = callResult.Result;
            Guid parentNodeId               = callResult.ContextKey;
            DeductionDetailsKeyParameters nodesKeyParameters = callResult.Parameters;
            int instanceKey                 = callResult.InstanceKey;

            bool success = ProcessDeductionDetailsDataTransactional( cancelToken, deductionDetails, parentNodeId, nodesKeyParameters, instanceKey );
            if ( !success )
                IgnoreOfDeductionDetailsRequest( instanceKey, nodesKeyParameters ); //ignores user calls those could not be serviced by the service call dispatcher

            return success;
        }

        private bool ProcessDeductionDetailsDataTransactional( 
            CancellationToken cancelToken, IReadOnlyCollection<DeductionDetail> deductionDetails, Guid parentNodeId, 
            DeductionDetailsKeyParameters nodesKeyParameters, int instanceKey )
        {
            bool success = false;
            bool toRollback = false;

#if ( DEBUG && DONOTDISABLE )

            CommandManager.StartTransaction( "InfoDeductionDetailSums" );

#endif //DONOTDISABLE
            try
            {
                success = ProcessDeductionDetailsDataWOTransaction( cancelToken, deductionDetails, parentNodeId, nodesKeyParameters, instanceKey );
            }catch( Exception )
            {
                toRollback = true;

                throw;
            }finally
            {
                if ( !success || toRollback )
                     CommandManager.RollbackTransaction();
                else CommandManager.CommitTransaction( "InfoDeductionDetailSums" );
            }
            return success;
        }

        private bool ProcessDeductionDetailsDataWOTransaction( 
            CancellationToken cancelToken, IReadOnlyCollection<DeductionDetail> deductionDetails, Guid parentNodeId, 
            DeductionDetailsKeyParameters nodesKeyParameters, int instanceKey )
        {
            bool success = IsRequestOfActualCommandManager( instanceKey ) && deductionDetails != null;
            if ( success ) //ignores results from different graph tree builds
            {
                success = !cancelToken.IsCancellationRequested;
                if ( success )
                    success = View.InfoDeductionDetailSums( parentNodeId, deductionDetails );
            }
            return success;
        }

        private void OnDataProcessorFaultOrCancel( Task dataProcessorCompletion )
        {
            //StopDataLoading();

            if ( !dataProcessorCompletion.IsCanceled )
                dataProcessorCompletion.Wait(); //to rethrow an exception only if it has been caused. 'dataProcessorCompletion' is already completed here
        }

	//apopov 13.1.2016	//TODO!!! remove if it will be useless in future
        /// <summary> Ignores a node data request. </summary>
        private void IgnoreOfTreeDataRequest()
        {
            //CommandManager.RollbackTransaction();            
        }

	//apopov 13.1.2016	//TODO!!! remove if it will be useless in future
        /// <summary> Ignores deduction detail sums data request. </summary>
        /// <param name="instanceKey"></param>
        /// <param name="keyParameters"></param>
        private void IgnoreOfDeductionDetailsRequest( int instanceKey, DeductionDetailsKeyParameters keyParameters )
        {
        }

        //apopov 29.12.2015	//TODO!!! remove this method when it wil be useless

        /// <summary> Gets nodes from the server. Executes in background thread. </summary>
        /// <param name="nodesKeyParameters"></param>
        /// <param name="cancelToken"></param>
        /// <returns></returns>
        public IReadOnlyCollection<GraphData> GetNodes( GraphNodesKeyParameters nodesKeyParameters, CancellationToken cancelToken = default(CancellationToken) )
        {
            return GetNodes( nodesKeyParameters, CancellationToken.None, cancelSource: null );
        }

        /// <summary> Gets nodes from the server. Executes in background thread. </summary>
        /// <param name="nodesKeyParameters"></param>
        /// <param name="cancelToken"></param>
        /// <param name="cancelSource"></param>
        /// <returns></returns>
        public IReadOnlyCollection<GraphData> GetNodes( GraphNodesKeyParameters nodesKeyParameters, 
            CancellationToken cancelToken = default(CancellationToken), CancellationTokenSource cancelSource = null )
        {
            Contract.Requires( nodesKeyParameters != null );
            Contract.Ensures( Contract.Result<IReadOnlyCollection<GraphData>>() != null );

            IReadOnlyCollection<GraphData> result = null;
            if ( !cancelToken.IsCancellationRequested )
            {
                IPyramidDataService dataService = GetDataService();
                OperationResult<IReadOnlyCollection<GraphData>> resultSet = 
                    ExecuteServiceCall( cancelTokenPar => dataService.LoadGraphNodes( nodesKeyParameters ) );

                if ( resultSet.Status == ResultStatus.Success )
                    result = resultSet.Result;
                else if ( cancelSource != null )   //here the error has been already logged and shown to user inside ExecuteServiceCallDirectly()
                    cancelSource.Cancel();
            }
            return result ?? new GraphData[0].ToReadOnly();
        }

        public ICollection<GraphDataNode> GetTableTabNodesByPage( 
            GraphNodesKeyParameters queryParameters, CancellationToken cancelToken = default(CancellationToken) )
        {
            IPyramidDataService dataService = GetDataService();

            OperationResult<PageResult<GraphData>> callResult = 
                ExecuteServiceCall( cancelTokenPar => dataService.LoadGraphNodeChildrenByPage( queryParameters ),   //dataService.LoadGraphNodeChildren( queryParameters ), 
                                    cancelToken );

            IEnumerable<GraphDataNode> result = null;
            if ( callResult.Status == ResultStatus.Success )
                result = callResult.Result.Rows.Select( graphData => new GraphDataNode( graphData ) );

            return result == null ? (ICollection<GraphDataNode>)Enumerable.Empty<GraphDataNode>() : result.ToArray();
        }

        #region Implementation of IInstanceProvider<ICommandTransactionManager>

        ICommandTransactionManager IInstanceProvider<ICommandTransactionManager>.Instance { get { return CommandManager; } }

        private ICommandTransactionManager CommandManager
        {
            get { return _commandManager; }
        }

        #endregion Implementation of IInstanceProvider<ICommandTransactionManager>

        public INodeAllTreeReporter NodeTreeReporter
        {
            get
            {
                if ( _nodeTreeReporter == null )
                    _nodeTreeReporter = new NodeTreePlainReporter( ClientLogger );

                return _nodeTreeReporter;
            }
        }

        public async void StartGenerateTreeExcelAsync( string filePath, GraphTreeNodeReportData treeTableData, GraphNodesKeyParameters nodesKeyParameters )
        {
            int layersDepthLimitAllTree = GetLayersDepthLimit();
            int layerMaxNodesLimit = GetLayerMaxNodesLimit();
            string fileFullSpec = null;
            bool isCanceled = false;
            _cancelSourceTreeExport = new CancellationTokenSource();
            try
            {
                GraphNodesKeyParameters reportKeyParameters = GraphNodesKeyParameters.New( 
                    nodesKeyParameters, layersLimit: layersDepthLimitAllTree, mostImportantContractors: layerMaxNodesLimit );

                fileFullSpec = await NodeTreeReporter.StartReportBuildAsync(
                    filePath, treeTableData, GetTableTabNodesByPage, reportKeyParameters, _cancelSourceTreeExport.Token );
            }
            finally
            {
                isCanceled = _cancelSourceTreeExport.IsCancellationRequested;
                _cancelSourceTreeExport = null;
            }
            OnTreeExcelExportFinish( fileFullSpec, isCanceled );
        }

        private void OnTreeExcelExportFinish( string fileDirectorySpec, bool isExportCanceled )
        {
            if ( !isExportCanceled && fileDirectorySpec != null )
                View.OpenTreeExcelFile( fileDirectorySpec );
        }

        public string GenerateTreeExcelFileName( GraphTreeNodeReportData treeTableData )
        {
            return NodeTreeReporter.GenerateExcelFileName( treeTableData );
        }

        public int GetLayersDepthLimit()
        {
            int layersDepthLimitAllTree = IsUseDataGenerationStub ? 2  //PyramidDataGenerator does not support yet limits different from '2'
                                                           : _dependencyTreeSection.ReportSettings.LayersDepthLimit + 1;
            return layersDepthLimitAllTree;
        }

        public int GetLayerMaxNodesLimit()
        {
            int layerMaxNodesLimit = _dependencyTreeSection.TreeSettings.LayerMaxNodesLimit;

            return layerMaxNodesLimit;
        }

        public void SwitchDataServiceType( bool useGerationStub )
        {
            if ( _useDataGenerationStub != useGerationStub )
            {
                _useDataGenerationStub = useGerationStub;
                _pyramidDataService = null;
            }
        }

        /// <summary> The root INN. It is usable only if the presenter uses a data generator as a data provider. </summary>
        public string RootInn
        {
            get
            {
                string rootInn = null;
                IPyramidDataService dataService = GetDataService();
                PyramidDataGenerator dataGenerator = dataService as PyramidDataGenerator;
                if ( dataGenerator != null )
                    rootInn = dataGenerator.RootInn;

                return rootInn;
            }
        }

        public bool IsUseDataGenerationStub { get { return _useDataGenerationStub; } }

        private IPyramidDataService GetDataService()
        {
            Contract.Ensures( Contract.Result<IPyramidDataService>() != null );

            if ( IsUseDataGenerationStub )
            {
                if ( _pyramidDataService == null )
                    _pyramidDataService = new PyramidDataGenerator();
            }
            else
            {
                if ( _pyramidDataService == null )
                {
                    var serviceContext = Services.Get<IReadOnlyServiceCollection>();

                    Contract.Assume( serviceContext != null );

                    _pyramidDataService = new PyramidDataService( serviceContext );
                }
            }
            return _pyramidDataService;
        }

        private IGraphTreeDataManager GetGraphTreeDataManager()
        {
            if ( _graphTreeDataManager == null )
                _graphTreeDataManager = new GraphTreeDataManager( 
                    _serviceCallDispatcherFactory, this, _executorWErrorHandling, GetDataService(), ClientLogger );

            return _graphTreeDataManager;
        }
    }
}