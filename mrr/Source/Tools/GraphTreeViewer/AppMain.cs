﻿using System;
using System.Security.Principal;
using System.Threading;
using System.Windows;
using CommonComponents.Configuration;
using CommonComponents.ExceptionHandling;
using CommonComponents.Instrumentation;
using CommonComponents.Shared;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using FLS.CommonComponents.App.ErrorHandling;
using FLS.CommonComponents.App.Execution;
using FLS.CommonComponents.App.Wpf.App;
using FLS.CommonComponents.Lib.Execution;
using FLS.GoWPFApp;
using Luxoft.NDS2.Client.GraphTreeViewer.Infrastructure;
using Luxoft.NDS2.Client.GraphTreeViewer.Services;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.Infrastructure.Executors;
using Luxoft.NDS2.Client.UI;
using Luxoft.NDS2.Client.UI.CommonFunctions;
using Luxoft.NDS2.Common.Helpers.Executors;
using Microsoft.Practices.CompositeUI.Collections;

namespace Luxoft.NDS2.Client.GraphTreeViewer
{
    internal sealed class AppMain
    {
        [STAThread]
        private static void Main()
        {
            bool isDoneInitialization;
            ApplicationWpfInitializer.InitAppIfNeeded<WpfApplication>( out isDoneInitialization );

            Application application = Application.Current;
            application.StartupUri = new Uri( "MainWindow.xaml", UriKind.Relative );

            application.Run();
        }

        internal static ServiceContext InitServices()
        {
            UIThreadExecutor.InitIfNeeded();

            AppDomain appDomain = Thread.GetDomain();
            appDomain.SetPrincipalPolicy( PrincipalPolicy.WindowsPrincipal );

            var services = new ServiceContext();

            IInstrumentationService instrumentationService = services.Get<IInstrumentationService>();
            IUcMessageService ucMessageService = services.Get<IUcMessageService>();

            UnhandledExceptionHandler.InitIfNeeded( instrumentationService, ucMessageService ); //assumes like of an application enter point. It supports multi time calls

            ServiceCallExecutorManagerFactory.InitIfNeeded();

            return services;
        }

        internal static RootWorkItem InitRootWorkItem( ServiceContext serviceContext )
        {
            //apopov 21.2.2016	//DEBUG!!!
            //var exceptionHandlingService = new ExceptionHandlingService( serviceContext );

            //var cabVisualizer = new RootVisualizer();
            //cabVisualizer.Initialize( null, null );
            //var rootWorkItem = new WorkItem();  //{ ID = Guid.NewGuid().ToString() };
            //rootWorkItem.InitializeWorkItem();

            var rootWorkItem = new RootWorkItem( serviceContext );
            rootWorkItem.Initialize();
            ServiceCollection services = rootWorkItem.Services;
            //already added inside of RootWorkItem.Initialize()
            //services.Add( serviceContext.Get<IExceptionHandlingService>() );
            //services.Add( instrumentationService );

            IInstrumentationService instrumentationService = serviceContext.Get<IInstrumentationService>();

            services.Add( serviceContext.Get<IExceptionPolicy>() );
#pragma warning disable 618
            services.Add( serviceContext.Get<ILoggerService>() );
#pragma warning restore 618
            services.Add( serviceContext.Get<IUcMessageService>() );
            services.Add( serviceContext.Get<IUcEventLogService>() );
            services.Add( serviceContext.Get<IConfigurationDataService>() );

            services.Add( serviceContext.Get<IViewBase>() );

            services.Add( typeof( IReadOnlyServiceCollection ), serviceContext );

            IThreadInvoker threadInvoker = UIThreadExecutor.CurrentExecutor;
            services.Add( threadInvoker );
            services.Add( ServiceCallExecutorManagerFactory.CurrentManager );

            INotifier notifier = new AlertNotifier(threadInvoker, instrumentationService );
            services.Add( notifier );
            IClientLogger logger = new ClientLogger( instrumentationService );
            services.Add( logger );
            IExecutorWErrorHandling executorWErrorHandling = new DirectExecutorWErrorHandling( notifier, logger );
            services.Add( executorWErrorHandling );
            IThreadInvokerWErrorHandling threadInvokerWErrorHandling = new ThreadInvokerWErrorHandling( threadInvoker, executorWErrorHandling );
            services.Add( threadInvokerWErrorHandling );

            return rootWorkItem;
        }
    }
}