﻿using Luxoft.NDS2.Client.NodeTreePanel.Interfaces;
using Luxoft.NDS2.Client.UI;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting.TaxPayer;

namespace Luxoft.NDS2.Client.GraphTreeViewer.Declarations
{
    public sealed class DeclarationUserPermissionsDummyPolicy<TView> : IDeclarationUserPermissionsPolicy where TView : IViewBase
    {
        private readonly BasePresenter<TView> _presenter;

        public DeclarationUserPermissionsDummyPolicy( BasePresenter<TView> presenter )
        {
            _presenter = presenter;
        }

        /// <summary> Is building of Dependency Tree allowable for the current user? </summary>
        /// <param name="declarationBrief"></param>
        /// <returns></returns>
        public bool IsTreeRelationEligible( DeclarationBrief declarationBrief )
        {
            return true;
        }

        /// <summary> Is building of Dependency Tree allowable for the current user? </summary>
        /// <param name="declarationSummary"></param>
        /// <returns></returns>
        public bool IsTreeRelationEligible( DeclarationSummary declarationSummary )
        {
            return true;
        }

        /// <summary> Is building of Dependency Tree allowable for the current user? </summary>
        /// <param name="sounCode"></param>
        /// <returns></returns>
        public bool IsTreeRelationEligible( string sounCode )
        {
            return true;
        }

        /// <summary> Is building of Dependency Tree allowable for the current user? </summary>
        /// <param name="taxPayerId"></param>
        /// <param name="period"> Optional. Checks for all periods if it is 'null' (by default). </param>
        /// <returns></returns>
        public bool IsTreeRelationEligible( TaxPayerId taxPayerId, Period period = null )
        {
            return true;
        }

        /// <summary> Is All tree node report of Dependency Tree allowable for the current user? </summary>
        /// <returns></returns>
        public bool IsTreeRelationAllTreeReportEligible()
        {
            return true;
        }

        public int? GetTreeRelationMaxLayersLimit( bool isByPurchase )
        {
            return null;
        }
    }
}