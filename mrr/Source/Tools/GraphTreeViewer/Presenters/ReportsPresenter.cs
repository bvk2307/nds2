﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Threading;
using CommonComponents.Configuration;
using CommonComponents.Shared;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using FLS.CommonComponents.Lib.Interfaces;
using Luxoft.NDS2.Client.Config;
using Luxoft.NDS2.Client.GraphTreeViewer.Bases;
using Luxoft.NDS2.Client.GraphTreeViewer.DataServices;
using Luxoft.NDS2.Client.GraphTreeViewer.Declarations;
using Luxoft.NDS2.Client.GraphTreeViewer.Views;
using Luxoft.NDS2.Client.NodeTreePanel.Interfaces;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.GraphTree;
using Luxoft.NDS2.Client.UI.Reports.Excel.GraphTree;
using Luxoft.NDS2.Client.UI.Reports.Interfaces;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Server.Services;
using Microsoft.Practices.CompositeUI;

namespace Luxoft.NDS2.Client.GraphTreeViewer.Presenters
{
    public sealed class ReportsPresenter : BasePresenter<ReportersView>, IInstanceProvider<ICommandTransactionManager>, ITestablePresenter
    {
        #region Private members

        private readonly IDeclarationUserPermissionsPolicy _declarationUserPermissionsPolicy;
        private readonly DependencyTreeSection _dependencyTreeSection;

        private ICommandTransactionManager _commandManager = null;

        private bool _useDataGenerationStub = false;

        //<< Reports
        private CancellationTokenSource _cancelSourceTreeExport = null;

        private IPyramidDataService _pyramidDataService = null;
        private INodeAllTreeReporter _nodeTreeReporter = null;
        // Reports >>

        #endregion Private members

        public ReportsPresenter( PresentationContext presentationContext, WorkItem workItem, ReportersView view ) 
            : base( presentationContext, workItem, view )
        {
            Contract.Requires( presentationContext != null );
            Contract.Requires( workItem != null );
            Contract.Requires( view != null );

            _declarationUserPermissionsPolicy = new DeclarationUserPermissionsDummyPolicy<ReportersView>( this );

            IConfigurationDataService configurationDataService = Services.Get<IConfigurationDataService>();

            Contract.Assume( configurationDataService != null );

            _dependencyTreeSection = configurationDataService.GetSection<DependencyTreeSection>( ProfileInfo.Default );
        }

        public IDeclarationUserPermissionsPolicy UserPermissionsPolicy
        {
            get { return _declarationUserPermissionsPolicy; }
        }

        public ICollection<GraphDataNode> GetTableTabNodesByPage( 
            GraphNodesKeyParameters queryParameters, CancellationToken cancelToken = default(CancellationToken) )
        {
            IPyramidDataService dataService = GetDataService();

            OperationResult<PageResult<GraphData>> callResult = 
                ExecuteServiceCall( cancelTokenPar => dataService.LoadGraphNodeChildrenByPage( queryParameters ),   //dataService.LoadGraphNodeChildren( queryParameters ), 
                                    cancelToken );

            IEnumerable<GraphDataNode> result = null;
            if ( callResult.Status == ResultStatus.Success )
                result = callResult.Result.Rows.Select( graphData => new GraphDataNode( graphData ) );

            return result == null ? (ICollection<GraphDataNode>)Enumerable.Empty<GraphDataNode>() : result.ToArray();
        }

        #region Implementation of IInstanceProvider<ICommandTransactionManager>

        ICommandTransactionManager IInstanceProvider<ICommandTransactionManager>.Instance { get { return CommandManager; } }

        private ICommandTransactionManager CommandManager
        {
            get { return _commandManager; }
        }

        #endregion Implementation of IInstanceProvider<ICommandTransactionManager>

        public INodeAllTreeReporter NodeTreeReporter
        {
            get
            {
                if ( _nodeTreeReporter == null )
                    _nodeTreeReporter = new NodeTreePlainReporter( ClientLogger );

                return _nodeTreeReporter;
            }
        }

        public async void StartGenerateTreeExcelAsync( string filePath, GraphTreeNodeReportData treeTableData, 
            GraphNodesKeyParameters nodesKeyParameters )
        {
            int layersDepthLimitAllTree = GetLayersDepthLimit();
            int layerMaxNodesLimit = GetLayerMaxExportNodesLimit();
            string fileFullSpec = null;
            bool isCanceled = false;
            _cancelSourceTreeExport = new CancellationTokenSource();
            try
            {
                GraphNodesKeyParameters reportKeyParameters = GraphNodesKeyParameters.New( 
                    nodesKeyParameters, layersLimit: layersDepthLimitAllTree, mostImportantContractors: layerMaxNodesLimit );

                fileFullSpec = await NodeTreeReporter.StartReportBuildAsync(
                    filePath, treeTableData, GetTableTabNodesByPage, reportKeyParameters, _cancelSourceTreeExport.Token );
            }
            finally
            {
                isCanceled = _cancelSourceTreeExport.IsCancellationRequested;
                _cancelSourceTreeExport = null;
            }
            OnTreeExcelExportFinish( fileFullSpec, isCanceled );
        }

        private void OnTreeExcelExportFinish( string fileDirectorySpec, bool isExportCanceled )
        {
            View.HideProgerssBar();
            if ( !isExportCanceled && fileDirectorySpec != null )
                View.OpenTreeExcelFile( fileDirectorySpec );
            else 
                View.ShowNotification( isExportCanceled ? "Построение отчёта прервано" : "Данные для отчёта не найдены" );
        }

        public string GenerateTreeExcelFileName( GraphTreeNodeReportData treeTableData )
        {
            return NodeTreeReporter.GenerateExcelFileName( treeTableData );
        }

        public int GetLayersDepthLimit()
        {
            int layersDepthLimitAllTree = IsUseDataGenerationStub ? 2  //PyramidDataGenerator does not support yet limits different from '2'
                                                           : _dependencyTreeSection.ReportSettings.LayersDepthLimit + 1;
            return layersDepthLimitAllTree;
        }

        public int GetLayerMaxExportNodesLimit()
        {
            int layerMaxNodesLimit = _dependencyTreeSection.ReportSettings.LayerMaxNodesExportLimit;

            return layerMaxNodesLimit;
        }

        public void SwitchDataServiceType( bool useGerationStub )
        {
            if ( _useDataGenerationStub != useGerationStub )
            {
                _useDataGenerationStub = useGerationStub;
                _pyramidDataService = null;
            }
        }

        /// <summary> The root INN. It is usable only if the presenter uses a data generator as a data provider. </summary>
        public string RootInn
        {
            get
            {
                string rootInn = null;
                IPyramidDataService dataService = GetDataService();
                PyramidDataGenerator dataGenerator = dataService as PyramidDataGenerator;
                if ( dataGenerator != null )
                    rootInn = dataGenerator.RootInn;

                return rootInn;
            }
        }

        public bool IsUseDataGenerationStub { get { return _useDataGenerationStub; } }

        private IPyramidDataService GetDataService( bool createNew = false )
        {
            Contract.Ensures( Contract.Result<IPyramidDataService>() != null );

            if ( IsUseDataGenerationStub )
            {
                if ( _pyramidDataService == null || createNew )
                    _pyramidDataService = new PyramidDataGenerator();
            }
            else
            {
                if ( _pyramidDataService == null )
                {
                    var serviceContext = Services.Get<IReadOnlyServiceCollection>();

                    Contract.Assume( serviceContext != null );

                    _pyramidDataService = new PyramidDataService( serviceContext );
                }
            }
            return _pyramidDataService;
        }
    }
}