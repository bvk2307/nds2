﻿using System;
using CommonComponents.GenericHost;

namespace Luxoft.NDS2.Client.GraphTreeViewer.Infrastructure
{
    public class HostInfoService : IHostInfoService
    {
        private readonly Lazy<HostInfo> lsHostInfo = new Lazy<HostInfo>( () => new HostInfo(), isThreadSafe: true );

        /// <summary>
        /// Метод возвращает информацию о среде.
        /// </summary>
        /// <returns>
        /// Информация о среде.
        /// </returns>
        public HostInfo GetInfo()
        {
            return lsHostInfo.Value;
        }

        /// <summary>
        /// Сигнализировать хосту о начеле исполнения 
        ///             прикладной роли.
        /// </summary>
        /// <param name="performCapacity"/>
        public void InitiatePerformCapacity( string performCapacity ) { }
    }
}