﻿using System.Diagnostics;
using CommonComponents.Instrumentation;

namespace Luxoft.NDS2.Client.GraphTreeViewer.Infrastructure
{
    public class PerformanceCounterStub : IPerformanceCounter
    {
        private static CounterSample emptySample;

        public string CategoryName
        {
            get
            {
            return string.Empty;
            }
        }

        public string CounterHelp
        {
            get
            {
            return string.Empty;
            }
        }

        public string CounterName
        {
            get
            {
            return string.Empty;
            }
        }

        public bool IsGenuineConter
        {
            get
            {
            return false;
            }
        }

        public PerformanceCounterType CounterType
        {
            get
            {
            return PerformanceCounterType.NumberOfItemsHEX32;
            }
        }

        public PerformanceCounterInstanceLifetime InstanceLifetime
        {
            get
            {
            return PerformanceCounterInstanceLifetime.Global;
            }
            set
            {
            }
        }

        public string InstanceName
        {
            get
            {
            return string.Empty;
            }
            set
            {
            }
        }

        public string MachineName
        {
            get
            {
            return string.Empty;
            }
        }

        public long RawValue
        {
            get
            {
            return 0;
            }
            set
            {
            }
        }

        public bool ReadOnly
        {
            get
            {
            return true;
            }
        }

        public bool Disposed
        {
            get
            {
            return true;
            }
        }

        public PerformanceCounterStub()
        {
            PerformanceCounterStub.emptySample = new CounterSample();
        }

        public long Decrement()
        {
            return 0;
        }

        public long Increment()
        {
            return 0;
        }

        public long IncrementBy(long value)
        {
            return 0;
        }

        public CounterSample NextSample()
        {
            return PerformanceCounterStub.emptySample;
        }

        public float NextValue()
        {
            return 0.0f;
        }

        public IInstrumentationSettings GetSettings()
        {
            return (IInstrumentationSettings) null;
        }

        public void Dispose()
        {
        }
        }
}