﻿using CommonComponents.ExceptionHandling;
using CommonComponents.Instrumentation;
using CommonComponents.Shared;
using Microsoft.Practices.CompositeUI;
using Microsoft.Practices.CompositeUI.BuilderStrategies;
using Microsoft.Practices.CompositeUI.Services;
using Microsoft.Practices.ObjectBuilder;

namespace Luxoft.NDS2.Client.GraphTreeViewer.Infrastructure
{
    public sealed class RootWorkItem : WorkItem
    {
        private readonly IExceptionHandlingService _exceptionHandlingService;
        private readonly IInstrumentationService _instrumentationService;

        public RootWorkItem( IReadOnlyServiceCollection readOnlyServiceCollection )
        {
            _exceptionHandlingService       = readOnlyServiceCollection.Get<IExceptionHandlingService>();
            _instrumentationService         = readOnlyServiceCollection.Get<IInstrumentationService>();
        }

        public void Initialize()
        {
            //Services.Add( _exceptionHandlingService );
            Builder builder = CreateBuilder();
            try
            {
                InitializeRootWorkItem( builder );
            }
            catch ( ServiceMissingException ) { }   //can not fully initialize the root WorkItem so only interrupts initialization

            Services.Add( _exceptionHandlingService );
            Services.Add( _instrumentationService );

            InitializeRootWorkItem( builder );  //repeats to complete initialization interrupted above

            //apopov 24.2.2016	//DEBUG!!!
            //ID = Guid.NewGuid().ToString(); //the copy of context of WorkItem.InitializeState()
            BuildUp();
        }

        private Builder CreateBuilder()
        {
          Builder builder = new Builder();
          builder.Strategies.AddNew<EventBrokerStrategy>(BuilderStage.Initialization);
          builder.Strategies.AddNew<CommandStrategy>(BuilderStage.Initialization);
          builder.Policies.SetDefault<ISingletonPolicy>((ISingletonPolicy) new SingletonPolicy(true));
          return builder;
        }
    }
}