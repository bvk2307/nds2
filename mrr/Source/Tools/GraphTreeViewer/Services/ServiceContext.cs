﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using CommonComponents.Configuration;
using CommonComponents.ExceptionHandling;
using CommonComponents.GenericHost;
//using CommonComponents.Infra.Dev.Security.Client.Authorization;
using CommonComponents.Instrumentation;
//using CommonComponents.Security.Authorization;
using CommonComponents.Shared;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using Luxoft.NDS2.Client.GraphTreeViewer.Infrastructure;
using Luxoft.NDS2.Client.UI;
using Luxoft.NDS2.Server.Services.Managers.Authorization;

namespace Luxoft.NDS2.Client.GraphTreeViewer.Services
{
    public class ServiceContext : IReadOnlyServiceCollection
    {
        Dictionary<Type, object> _services = new Dictionary<Type, object>();

        public ServiceContext()
        {
            var configuraionService = new ConfiguraionService();
            _services.Add( typeof( IConfigurationDataService ), configuraionService );

	        //apopov 2.3.2016	//ATTENTION! Uses reflection to access to the internal type in CommonComponents.CorLib.Bundle
            Type typeInstrumentationService = Type.GetType( 
                "CommonComponents.Instrumentation.InstrumentationService, CommonComponents.CorLib.Bundle", throwOnError: true );
            IInstrumentationService instrumentationService = (IInstrumentationService)Activator.CreateInstance( 
                typeInstrumentationService, args: new []{ new HostInfoService() } );
            //var instrumentationService = new InstrumentationService();

            _services.Add( typeof( IInstrumentationService ), instrumentationService );
            _services.Add( typeof( IPerformanceCountersService ), instrumentationService );

	        ////apopov 1.3.2016	//DEBUG!!!
            //var appConfigSection = configuraionService.GetSection<AppSettingsSection>( ProfileInfo.Default );
            //KeyValueConfigurationElement configurationElement = appConfigSection.Settings["CsudProvider.AzmanFile"];
            //string str = null;
            //if (configurationElement != null)
            //str = configurationElement.Value;
            //if (!string.IsNullOrWhiteSpace(str))
            //{
            //    if (!System.IO.Path.IsPathRooted(str))
            //    str = System.IO.Path.GetFullPath(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, str));
            //}

	        //apopov 1.3.2016	//DEBUG!!!
            //_services.Add( typeof( IAuthorizationService ), new DevClientAuthorizationService( this, appConfigSection ) );
            ////_services.Add( typeof(IAuthorizationService), new AuthorizationService() );

            _services.Add( typeof(IAuthorizationProvider), new DevAuthorizationProvider( this ) );    //is needed for PyramidDataService too
            //_services.Add( typeof(IAuthorizationProvider), new AzManAuthorizationProvider( this ) );    //is needed for PyramidDataService too
            ////_services.Add( typeof(IAuthorizationProvider), new AuthProviderStub() );    //is needed for PyramidDataService

            //apopov 24.2.2016	//DEBUG!!!
            //_services.Add(typeof(IAuthorizationService), new AuthorizationService() );
            //apopov 24.2.2016	//DEBUG!!!
            //var strategyChainService = new StrategyChainService( new PolicyContainer(), instrumentationService );
            //_services.Add( typeof(IStrategyChainService), strategyChainService );
            //apopov 24.2.2016	//DEBUG!!!
            //_services.Add( typeof(ICatalogFactoryService), new CatalogInteractionBusEmptyStub( this ) );

            var exceptionHandlingService = new ExceptionHandlingServiceStub();
            _services.Add( typeof( IExceptionHandlingService ), exceptionHandlingService );
            //_services.Add( typeof(IExceptionHandlingService), new ExceptionHandlingService( this ) );
            _services.Add( typeof( IExceptionPolicy ), exceptionHandlingService );

#pragma warning disable 618
            _services.Add( typeof( ILoggerService ), instrumentationService );
#pragma warning restore 618
            _services.Add( typeof( IUcEventLogService ), new UcEventLogServiceEmptyStub() );
            var usMessageService = new UcMessageService();
            _services.Add( typeof( IUcMessageService ), usMessageService );
            _services.Add( typeof( IViewBase ), usMessageService );
        }

        #region Implementation of IServiceProvider2

        public object GetService( Type serviceType )
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Implementation of IReadOnlyServiceCollection

        public bool Contains( Type serviceType )
        {
            throw new NotImplementedException();
        }

        public bool Contains<TService>()
        {
            throw new NotImplementedException();
        }

        public object Get( Type serviceType )
        {
            return _services[serviceType];
        }

        public TService Get<TService>()
        {
            return (TService)_services[typeof( TService )];
        }

        public bool TryGet( Type serviceType, out object serviceInstance )
        {
            serviceInstance = null;
            return _services.TryGetValue( serviceType, out serviceInstance );
        }

        public bool TryGet<TService>( out TService serviceInstance )
        {
            serviceInstance = default(TService);
            object service;
            if ( _services.TryGetValue( typeof(TService), out service ) )
            {
                serviceInstance = (TService)service;
                return true;
            }
            return false;
        }

        public object Resolve( Type serviceType )
        {
            throw new NotImplementedException();
        }

        public TService Resolve<TService>()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}