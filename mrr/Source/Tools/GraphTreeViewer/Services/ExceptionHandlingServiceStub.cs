﻿using System;
using CommonComponents.ExceptionHandling;
using CommonComponents.Instrumentation;

namespace Luxoft.NDS2.Client.GraphTreeViewer.Services
{
    public sealed class ExceptionHandlingServiceStub : IExceptionHandlingService, IExceptionPolicy
    {
        #region Implementation of IExceptionHandlingService

        /// <summary>
        /// Создать экземпляр истории.
        ///             Необходимо только для сценариев обработки с историей.
        /// </summary>
        /// <returns/>
        public IExceptionHandlingHistory CreateHandlingHistory()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Конструирует контекст обработки исключений.
        ///             Далее на одном контесте может быть применено много обработчиков.
        /// </summary>
        /// <param name="exceptionToHandle">Обрабатываемое исключение.</param><param name="serviceProvider">Сервисный провайдер окружения, где возникло исключение. 
        ///             может быть null, то используется окружение самого сервиса обработки исключений.</param><param name="history">Can be null</param><param name="eventName">Event assotion name</param>
        /// <returns/>
        public IExceptionHandlingContext CreateHandlingContext( Exception exceptionToHandle, IServiceProvider serviceProvider, IExceptionHandlingHistory history, string eventName )
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Конструирует контекст обработки исключений.
        ///             Далее на одном контесте может быть применено много обработчиков.
        /// </summary>
        /// <param name="exceptionToHandle">Обрабатываемое исключение.</param><param name="serviceProvider">Сервисный провайдер окружения, где возникло исключение. 
        ///             может быть null, то используется окружение самого сервиса обработки исключений.</param><param name="history">Can be null</param><param name="eventName">Event assotion name</param><param name="profilingEntry">событие профилирования, ассоциированное с обработчиком исключения или 
        /// <value>
        /// null
        /// </value>
        /// , в случае чего будет создано и записано событие профилирования по-умолчанию.</param>
        /// <returns/>
        public IExceptionHandlingContext CreateHandlingContext( Exception exceptionToHandle, IServiceProvider serviceProvider, IExceptionHandlingHistory history, string eventName, IProfilingEntry profilingEntry )
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Запускает цепочки политик обработки исключения представленного контекстом.
        ///             Цепочки политик применяются до тех пор пока исключение не будет обработано.
        ///             Исключение представленное контестом считается обработаннымм в 
        ///             случае если свойство <see cref="P:CommonComponents.ExceptionHandling.IExceptionHandlingContext.IsHandled"/> = 
        /// <value>
        /// true
        /// </value>
        /// .
        /// </summary>
        /// <param name="context">Исключение, которое требуется обработать.
        ///             </param><param name="policyNames">Адреса цепочек стратегий задаются через символ (';').
        ///             Обработка контеста будет осуществлятся цепочками в порядке их указания.
        ///             Также может быть не задано ни одной цепочки. В этом случае применяется цепочка сконфигурированная по умолчанию.
        ///             </param>
        public void HandleException( IExceptionHandlingContext context, string policyNames )
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Implementation of IExceptionPolicy

        /// <summary>
        /// Обрабатывает исключение. 
        ///             Применяя стандартную цепочку обработки стратегий.
        /// </summary>
        /// <param name="exceptionToHandle">Исключение, которое требуется обработать.
        ///             </param>
        /// <returns>
        /// Флаг описывающий необходимость продвигать
        ///             исходное исключение дальше.
        /// </returns>
        public bool HandleException( Exception exceptionToHandle )
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Обрабатывает исключение.
        ///             Применяя цепочку политик заданную <paramref name="policyName"/>.
        /// </summary>
        /// <param name="exceptionToHandle">Исключение, которое требуется обработать.
        ///             </param><param name="policyName">Адреса цепочек стратегий (через ';'), согласно которым требуется обработать исключение.
        ///             </param>
        /// <returns>
        /// Рекомендуется ли передавать исходное исключение дальше.
        /// </returns>
        public bool HandleException( Exception exceptionToHandle, string policyName )
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Обрабатывает исключение.
        /// </summary>
        /// <param name="exceptionToHandle">Исключение, которое требуется обработать.
        ///             </param><param name="policyName">Адреса цепочек стратегий (через ';'), согласно которым требуется обработать исключение.
        ///             </param><param name="exceptionToThrow">Возвращает исключение, которое произошло при обработке exceptionToHandle (если таковое было)
        ///             или null, если обработка прошла без ошибок.
        ///             </param>
        /// <returns>
        /// Рекомендуется ли передавать исходное исключение дальше?
        /// </returns>
        public bool HandleException( Exception exceptionToHandle, string policyName, out Exception exceptionToThrow )
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}