﻿using System.Collections.Generic;
using CommonComponents.Security.Authorization;

namespace Luxoft.NDS2.Client.GraphTreeViewer.Services
{
    public sealed class AuthorizationServiceStub : IAuthorizationService
    {
        #region Implementation of IAuthorizationService

        public bool Authorize( string userIdentifier, string context )
        {
            throw new System.NotImplementedException();
        }

        public bool[] Authorize( string userIdentifier, IList<string> contexts )
        {
            throw new System.NotImplementedException();
        }

        public IList<string> GetStructContexts( string userIdentifier, string context )
        {
            throw new System.NotImplementedException();
        }

        public IList<string> GetAssignments( string context )
        {
            throw new System.NotImplementedException();
        }

        public IList<PermissionObject> GetSubsystemPermissions( string userIdentifier, string subsystemName )
        {
            throw new System.NotImplementedException();
        }

        #endregion
    }
}