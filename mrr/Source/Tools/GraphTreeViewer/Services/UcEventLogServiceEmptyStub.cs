﻿using CommonComponents.Uc.Infrastructure.Interface.Services;

namespace Luxoft.NDS2.Client.GraphTreeViewer.Services
{
    public sealed class UcEventLogServiceEmptyStub : IUcEventLogService
    {
        #region Implementation of IUcEventLogService

        public void RegisterEntry( UcEventLogEntry logEntry )
        {
            throw new System.NotImplementedException();
        }

        #endregion
    }
}