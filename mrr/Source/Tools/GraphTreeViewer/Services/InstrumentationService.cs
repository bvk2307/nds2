﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using CommonComponents.Instrumentation;
using Luxoft.NDS2.Client.GraphTreeViewer.Infrastructure;

namespace Luxoft.NDS2.Client.GraphTreeViewer.Services
{
#pragma warning disable 618
    public class InstrumentationService : IInstrumentationService, ILoggerService, IPerformanceCountersService
#pragma warning restore 618
    {
        private readonly Lazy<IPerformanceCounter> lzEmptyPerformanceCounterStub = 
            new Lazy<IPerformanceCounter>( () => new PerformanceCounterStub(), isThreadSafe: true );

        #region Implementation of IInstrumentationService

        public LogEntry CreateLogEntry(LogEntryTemplate template)
        {
            return new LogEntry(template);
        }

        public LogEntry Write(LogEntryTemplate logEntry)
        {
            Console.WriteLine(logEntry.Message);
            return new LogEntry(logEntry);
        }

        public LogEntry Write(LogEntryTemplate logEntry, Action<LogEntry> prepareEntry)
        {
            return new LogEntry(logEntry);
        }

        public IProfilingEntry CreateProfilingEntry(ProfilingEntryTemplate template)
        {
            throw new NotImplementedException();
        }

        public IProfilingEntry CreateProfilingEntry(TraceEventType eventType, int messageLevel, string messageType, int messagePriority)
        {
            return new NullProfilingEntry();
        }

        public IProfilingEntry CreateProfilingEntry(TraceEventType eventType, int messageLevel, string messageType, int messagePriority, string settingsName)
        {
            throw new NotImplementedException();
        }

        public IProfilingEntry CreateProfilingEntry(TraceEventType eventType, int messageLevel, string messageType, int messagePriority, string settingsName, SettingsRequirements settingsRequirement)
        {
            throw new NotImplementedException();
        }

        public void Write(IProfilingEntry profilingEntry)
        {
            return;
        }

        #endregion

        #region Implementation of ILoggerService

        /// <summary>
        /// Проверяет, разрешено ли логировать событие с указанным уровнем. 
        /// </summary>
        /// <param name="logEntryLevel">Проверяемый уровень логирования.</param>
        /// <returns>
        /// True, если событие с указанным уровнем следует логировать.
        /// </returns>
        public bool ShouldLog( InstrumentationLevel logEntryLevel )
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Проверяет, разрешено ли логировать событие с указанным уровнем. 
        /// </summary>
        /// <param name="logEntry">Запись журнала событий.</param>
        /// <returns>
        /// True, если событие с указанным уровнем следует логировать.
        /// </returns>
        public bool ShouldLog( LogEntry logEntry )
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Операция записи события в лог.
        /// </summary>
        /// <param name="logEntry">Запись журнала событий.</param>
        public void Write( LogEntry logEntry )
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Операция записи события в лог.
        /// </summary>
        /// <param name="logEntry">Запись журнала событий.</param><param name="orderedID">Целочисленный идентификатор события, присвоенный записанному событию.
        ///             </param>
        public void Write( LogEntry logEntry, out long orderedID )
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Выполняет преобразование объектного значения параметра в пару: ключ, 
        ///             строковое значение.
        /// </summary>
        /// <param name="extendedPropertyName">Имя дополнительного параметра.</param><param name="value">Объектное значение дополнительного параметра.</param>
        /// <returns>
        /// Результат пара ключ значение.
        /// </returns>
        public KeyValuePair<string, string> Format( string extendedPropertyName, object value )
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Форматирует исключение. Для добовления его в запись журнала событий.
        /// </summary>
        /// <param name="ex">Возникшее исключение. Не null.</param><param name="wrappedEx">Исключение в которое было обернуто исходное исключение. Может быть null.
        ///             </param><param name="handled">Признак, что исключение обработано.</param>
        /// <returns>
        /// Объект, содержащий информацию об исключении.
        /// </returns>
        public LogExceptionInfo Format( Exception ex, Exception wrappedEx, bool handled )
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Признак того, что инструментирование включено.
        ///             При исполнении прикладного кода подсистем инструментирование всегда включено.
        /// </summary>
        public bool InstrumentationEnabled { get; private set; }

        /// <summary>
        /// Текущий уровень инструментирования.
        /// </summary>
        public InstrumentationLevel Level { get; private set; }

        #endregion

        #region Implementation of IPerformanceCountersService

        /// <summary>
        /// Возвращает неименованный экземпляр класса счетчика производительности
        /// </summary>
        /// <typeparam name="T">Тип объекта, реализующий счётчик производительности.</typeparam>
        /// <returns>
        /// Затребованный экземпляр класса счетчика производительности.
        /// </returns>
        public IPerformanceCounter Get<T>()
        {
            return lzEmptyPerformanceCounterStub.Value;
        }

        /// <summary>
        /// Возвращает именованный экземпляр класса счетчика производительности
        /// </summary>
        /// <typeparam name="T">Тип объекта, реализующий счётчик производительности.</typeparam><param name="instanceName">Наименование инстанса счётчика производительности.</param>
        /// <returns>
        /// Затребованный экземпляр класса счетчика производительности.
        /// </returns>
        public IPerformanceCounter Get<T>( string instanceName )
        {
            return lzEmptyPerformanceCounterStub.Value;
        }

        #endregion
    }
}