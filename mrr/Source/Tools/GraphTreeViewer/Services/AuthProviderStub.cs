﻿using System;
using System.Collections.Generic;
using CommonComponents.Security.Authorization;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO.Security;
using Luxoft.NDS2.Server.Services.Managers.Authorization;

namespace Luxoft.NDS2.Client.GraphTreeViewer.Services
{
        internal class AuthProviderStub : IAuthorizationProvider
        {

            public string CurrentUserName
            {
                get { throw new NotImplementedException(); }
            }

            public List<AccessRight> GetUserPermissions()
            {
                return new List<AccessRight>()
                {
                    new AccessRight() {Name = "Роль.Аналитик", PermType = PermissionType.Operation}
                };
            }

            public List<AccessRight> GetUserPermissions(string SID)
            {
                throw new NotImplementedException();
            }

            public void ChangeUserRole(string roleName)
            {
                throw new NotImplementedException();
            }

            public List<string> GetSubsystemRoles()
            {
                throw new NotImplementedException();
            }

            public bool IsOperationEligible(string operationName)
            {
                bool isOperationEligible = operationName == Constants.SystemPermissions.Operations.DeclarationTreeRelation;

                if ( !isOperationEligible )
                    throw new NotImplementedException();

                return isOperationEligible;
            }

            public bool IsUserInRole(string roleName)
            {
                throw new NotImplementedException();
            }

            public bool IsUserInRole(string userSID, string roleName)
            {
                throw new NotImplementedException();
            }

            public string CurrentUserSID
            {
                get { throw new NotImplementedException(); }
            }

            public List<string> GetUserSIDs()
            {
                throw new NotImplementedException();
            }

            public List<Common.Contracts.DTO.Business.Security.UserStructContextRight> GetStructContextUserRigths(string ifns)
            {
                throw new NotImplementedException();
            }

            public IList<string> GetUserStructContexts(CommonComponents.Security.Authorization.PermissionType permType, string objectName)
            {
                return new List<string>()
                {
                    "0000"
                };
            }


            public List<Common.Contracts.DTO.Business.Security.UserStructContextRight> GetStructContextUsersByRole(string ifns, string roleOperation)
            {
                throw new NotImplementedException();
            }
        }
}