﻿using System.Windows;
using System.Windows.Forms;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using Luxoft.NDS2.Client.UI;

namespace Luxoft.NDS2.Client.GraphTreeViewer.Services
{
    public class UcMessageService : IUcMessageService, IViewBase
    {
        public void Show( MessageInfoContext message )
        {
            MessageBoxButton messageBoxButton = MessageBoxButton.OK;
            MessageBoxImage  messageBoxImage = MessageBoxImage.None;
            MessageBoxResult messageBoxResult = MessageBoxResult.OK;
            switch ( message.Category )
            {
                case MessageCategory.Exclamation:
                    messageBoxImage  = MessageBoxImage.Warning;
                    break;
                case MessageCategory.Info:
                    messageBoxImage  = MessageBoxImage.Information;
                    break;
                case MessageCategory.Question:
                    messageBoxButton = MessageBoxButton.YesNo;
                    messageBoxImage  = MessageBoxImage.Question;
                    messageBoxResult = MessageBoxResult.Yes;
                    break;
                case MessageCategory.Error:
                    messageBoxImage  = MessageBoxImage.Error;
                    break;
            }

            MessageBoxResult result = System.Windows.MessageBox.Show( message.Message, message.Caption, messageBoxButton, messageBoxImage, messageBoxResult );

            DialogResult dialogResult = DialogResult.No;
            switch ( result )
            {
                case MessageBoxResult.OK:
                    dialogResult = DialogResult.OK;
                    break;
                case MessageBoxResult.Yes:
                    dialogResult = DialogResult.Yes;
                    break;
                case MessageBoxResult.No:
                    dialogResult = DialogResult.No;
                    break;
            }
            message.DialogResult = dialogResult;
        }

        public void ShowInfo( MessageInfoContext message )
        {
            message.Category = MessageCategory.Info;
            Show( message );
        }

        public void ShowExclamation( MessageInfoContext message )
        {
            message.Category = MessageCategory.Exclamation;
            Show( message );
        }

        public void ShowError( MessageInfoContext message )
        {
            message.Category = MessageCategory.Error;
            Show( message );
        }

        public DialogResult ShowQuestion( MessageInfoContext message )
        {
            message.Category = MessageCategory.Question;
            Show( message );

            return message.DialogResult;
        }

        #region Implementation of IViewBase

        public void ShowError( string message )
        {
            ShowError( new MessageInfoContext( "Ошибка", message, MessageCategory.Error ) );
        }

        public void ShowWarning( string message )
        {
            ShowExclamation( new MessageInfoContext( "Предупреждение", message, MessageCategory.Exclamation ) );
        }

        public void ShowNotification( string message )
        {
            ShowInfo( new MessageInfoContext( "Инофрмация", message, MessageCategory.Info ) );
        }

        #endregion Implementation of IViewBase

        #region Implementation of IMessageView

        public DialogResult ShowQuestion( string caption, string message )
        {
            return ShowQuestion( new MessageInfoContext( caption, message, MessageCategory.Question ) );
        }

        public void ShowInfo( string caption, string message )
        {
            ShowInfo( new MessageInfoContext( caption, message, MessageCategory.Info ) );
        }

        #endregion Implementation of IMessageView
    }
}