﻿namespace Luxoft.NDS2.Client.GraphTreeViewer.Bases
{
    public interface ITestablePresenter
    {
        bool IsUseDataGenerationStub { get; }

        /// <summary> The root INN. It is usable only if the presenter uses a data generator as a data provider. </summary>
        string RootInn { get; }

        int GetLayersDepthLimit();
    }
}