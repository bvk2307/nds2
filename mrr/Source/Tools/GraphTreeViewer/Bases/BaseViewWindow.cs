﻿using System.Diagnostics.Contracts;
using System.Windows;
using System.Windows.Forms;
using Luxoft.NDS2.Client.GraphTreeViewer.Infrastructure;
using Luxoft.NDS2.Client.NodeTreePanel;
using Luxoft.NDS2.Client.UI;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.TaxPayer;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting.TaxPayer;
using Microsoft.Practices.CompositeUI;
using MessageBox = System.Windows.MessageBox;

namespace Luxoft.NDS2.Client.GraphTreeViewer.Bases
{
    public class BaseViewWindow : Window, IViewBase
    {
        protected IViewBase _viewBase = null;

        private ITestablePresenter _presenterTestable;

        protected ITestablePresenter PresenterTestable
        {
            get { return _presenterTestable; }
            set { _presenterTestable = value; }
        }

        protected void InitWithWorkItem( WorkItem workItem )
        {
            Contract.Requires( workItem != null );
            Contract.Ensures( _viewBase != null );
            Contract.Ensures( _viewBase != null );

            _viewBase = workItem.Services.Get<IViewBase>();
        }


        #region Implementation of IViewBase

        public void ShowError( string message )
        {
            _viewBase.ShowError( message );
        }

        public void ShowNotification( string message )
        {
            _viewBase.ShowNotification( message );
        }

        public void ShowWarning( string message )
        {
            _viewBase.ShowWarning( message );
        }

        public void ShowInfo( string caption, string message )
        {
            _viewBase.ShowInfo( caption, message );
        }

        #endregion Implementation of IViewBase

        public DialogResult ShowQuestion( string caption, string message )
        {
            MessageBoxResult result = MessageBox.Show( message, caption, MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.Yes );

            return result == MessageBoxResult.Yes ? System.Windows.Forms.DialogResult.Yes : System.Windows.Forms.DialogResult.No;
        }

        protected GraphTreeParameters GetTESTDeclarationParameters()
        {
            DeclarationBrief declarationBrief;
            if ( PresenterTestable.IsUseDataGenerationStub )
            {
                declarationBrief = new DeclarationBrief
                {
                    //ID                  = ,
                    INN                 = PresenterTestable.RootInn,
                    //NAME =              = 
                    FULL_TAX_PERIOD     = "3 2015",
                    // COMPENSATION_AMNT   = -1000000  // is by purchase: 'COMPENSATION_AMNT.HasValue && COMPENSATION_AMNT.Value < 0'
                };
            }
            else
            {
                //ООО РОМА
                //declarationBrief = new DeclarationBrief
                //{
                //    //ID                  = ,
                //    INN                   = "1112223334",
                //    //NAME =              = 
                //    FULL_TAX_PERIOD = "1 2016",
                //    //COMPENSATION_AMNT   = -1000000  // is by purchase: 'COMPENSATION_AMNT.HasValue && COMPENSATION_AMNT.Value < 0'
                //};
                //ООО Василёк
                declarationBrief = new DeclarationBrief
                {
                    //ID                  = ,
                    INN                   = "5030049549",
                    //NAME =              = 
                    FULL_TAX_PERIOD = "1 2015",
                    //COMPENSATION_AMNT   = -1000000  // is by purchase: 'COMPENSATION_AMNT.HasValue && COMPENSATION_AMNT.Value < 0'
                };
            }
            GraphTreeParameters graphTreeParameters = new GraphTreeParameters( declarationBrief );
            graphTreeParameters = GraphTreeParameters.New( graphTreeParameters, showReverted: true );
            return graphTreeParameters;
        }

        protected GraphNodesKeyParameters GetTESTDeclarationKeyParameters()
        {
            GraphNodesKeyParameters nodesKeyParameters;
            if ( PresenterTestable.IsUseDataGenerationStub )
            {
                nodesKeyParameters = GraphNodesKeyParameters.New( new GraphNodesKeyParameters( 
                    isByPurchase: false, quarter: 3, year: 2015, ids: new[] { new TaxPayerId(PresenterTestable.RootInn, kppCode: null) }), sharedNdsPercent: 1);    //PyramidDataGenerator does not support yet layer limits different from '2'
            }
            else
            {
                nodesKeyParameters = GraphNodesKeyParameters.New(
                    new GraphNodesKeyParameters(isByPurchase: false, quarter: 1, year: 2015, ids: new[] { new TaxPayerId("5030049549", kppCode: null) }), sharedNdsPercent: 1, layersLimit: 4);
                //new GraphNodesKeyParameters( "333", isByPurchase: false, quarter: 1, year: 2015 ) );
            }
            return nodesKeyParameters;
        }
    }
}