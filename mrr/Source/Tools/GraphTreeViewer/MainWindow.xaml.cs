﻿using System;
using System.Collections.Generic;
using System.Windows.Media;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using FLS.Common.Lib.Primitives;
using FLS.CommonComponents.App.Processes;
using FLS.CommonComponents.Lib.Execution;
using FLS.CommonComponents.Lib.Interfaces;
using Luxoft.NDS2.Client.GraphTreeViewer.Bases;
using Luxoft.NDS2.Client.GraphTreeViewer.Infrastructure;
using Luxoft.NDS2.Client.GraphTreeViewer.Services;
using Luxoft.NDS2.Client.NodeTreePanel;
using Luxoft.NDS2.Client.NodeTreePanel.Interfaces;
using Luxoft.NDS2.Client.UI.GraphTree;
using Luxoft.NDS2.Client.UI.GraphTree.Actions;
using Luxoft.NDS2.Client.UI.Reports.Excel.GraphTree;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Helpers.Executors;
using Microsoft.Win32;

namespace Luxoft.NDS2.Client.GraphTreeViewer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : BaseViewWindow, IInstanceProvider<ICommandTransactionManager>
    {
        private readonly IThreadInvokerWErrorHandling _threadInvoker;
        private readonly MainPresenter _presenter;

        private IUcMessageService _ucMessageService = null;
        private SaveFileDialog _saveFileDialogReport = null;
        private FileProcessStarter _fileProcessStarter = null;

        public MainWindow()
        {
            ServiceContext services = AppMain.InitServices();

            RootWorkItem rootWorkItem = AppMain.InitRootWorkItem( services );

            base.InitWithWorkItem( rootWorkItem );

            _threadInvoker = rootWorkItem.Services.Get<IThreadInvokerWErrorHandling>( ensureExists: true );
            IExecutorWErrorHandling executorWErrorHandling = rootWorkItem.Services.Get<IExecutorWErrorHandling>( ensureExists: true );
            _ucMessageService = rootWorkItem.Services.Get<IUcMessageService>( ensureExists: true );

            var featureContext = new FeatureContextBase( Guid.NewGuid() );
            var presentationContext = new PresentationContext( Guid.NewGuid(), null, featureContext );

            _presenter = new MainPresenter( presentationContext, rootWorkItem, this, executorWErrorHandling );
            PresenterTestable = _presenter;

            InitializeComponent();
        }

        protected override void OnSourceInitialized( EventArgs e )
        {
            base.OnSourceInitialized( e );

            nodeTree.Init( _threadInvoker, this, new Int32Counter() );
            nodeTree.RequestTreeData                += OnRequestTreeData;
            nodeTree.RequestDeductionDetailsData    += OnRequestDeductionDetailsData;

	//apopov 24.6.2016	//DEBUG!!!
            _presenter.SwitchDataServiceType( useGerationStub: true );

            _presenter.StartLoadTree();
        }

        public InfoDeductionDetailSumsAction CreateDeductionDetailsAction( 
            IGraphTreeDataManager graphTreeDataManager, IDirectExecutor callExecutor )
        {
            return new InfoDeductionDetailSumsAction( nodeTree, graphTreeDataManager, callExecutor );
        }

        public ICommandTransactionManager StartLoadTree( out GraphTreeParameters graphTreeParameters )
        {
            var sur = new Dictionary<int, Tuple<SolidColorBrush, string>>( 0 );
            //foreach (var s in p.Sur.Items)
            //{
            //    sur.Add(s.Code, new Tuple<SolidColorBrush, string>(ArgbToColor(s.ColorARGB), s.Description));
            //}

            graphTreeParameters = GetTESTDeclarationParameters();

            IReadOnlyDictionary<int, Tuple<SolidColorBrush, string>> surs = sur;
            _presenter.InitInfrastructure( graphTreeParameters, surs );
            nodeTree.InitGraph( surs );

            return nodeTree.InitAndDelayLoadTree( graphTreeParameters, cancelSource: null );
        }

        public void ClearTreeNodes()
        {
            nodeTree.ClearTreeNodes();
        }

        private void OnRequestTreeData( object sender, GraphTreeDataRequestArgs graphTreeDataRequestArgs )
        {
            _presenter.StartLoadNodes( graphTreeDataRequestArgs );
        }

        private void OnRequestDeductionDetailsData( object sender, DeductionDetailsDataRequestArgs deductionDetailsDataRequestArgs )
        {
            _presenter.StartDeductionDetails( deductionDetailsDataRequestArgs );
        }

        /// <summary> Loads tree branch nodes into the tree. </summary>
        /// <param name="parentNodeKey"> A parent node key <see cref="GraphTreeNodeData.Key"/> or <see cref="GraphTreeNodeData.NoNodeKey"/> if the node is root one. </param>
        /// <param name="nodes"></param>
        /// <returns></returns>
        public bool BuildTreeBranch( Guid parentNodeKey, IEnumerable<GraphTreeNodeData> nodes )
        {
            GraphTreeNodeData parentNode = nodeTree.BuildTreeBranch( parentNodeKey, nodes );
            bool success = parentNode != null;

            return success;
        }

        /// <summary> Loads deduction detail sums into the tree details. </summary>
        /// <param name="parentNodeId"></param>
        /// <param name="deductionDetails"></param>
        /// <returns></returns>
        public bool InfoDeductionDetailSums( Guid parentNodeId, IEnumerable<DeductionDetail> deductionDetails )
        {
            bool success = nodeTree.InfoDeductionDetailSums( parentNodeId, deductionDetails );

            return success;
        }

        /// <summary> Sets the child counter for a node. </summary>
        /// <param name="parentNodeKey"> A parent node key <see cref="GraphTreeNodeData.Key"/>. </param>
        /// <param name="childCount"></param>
        /// <param name="revertedChildCount"></param>
        /// <returns></returns>
        public bool SetChildCounter( Guid parentNodeKey, int childCount, int revertedChildCount )
        {
            return nodeTree.SetChildCounter( parentNodeKey, childCount, revertedChildCount );
        }

        //apopov 11.1.2016	//DEBUG!!!
        //public void CancelDataLoadingIfNeeded()
        //{
        //    nodeTree.CancelDataLoadingIfNeeded();
        //}

        #region Implementation of IInstanceProvider<ICommandTransactionManager>

        ICommandTransactionManager IInstanceProvider<ICommandTransactionManager>.Instance
        {
            get { return ( (IInstanceProvider<ICommandTransactionManager>)_presenter ).Instance; }
        }

        #endregion Implementation of IInstanceProvider<ICommandTransactionManager>

        private SaveFileDialog SaveFileDialogReport
        {
            get
            {
                if ( _saveFileDialogReport == null )
                {
                    var saveFileDialog = new SaveFileDialog();
                    string fileExtension = BaseNodeReporter<NodeReportManager>.ReportExcelFileExtensionName;
                    saveFileDialog.DefaultExt = string.Concat( ".", fileExtension );
                    saveFileDialog.Filter = string.Format( "Excel отчёты (.{0})|*.{0}", fileExtension );
                    _saveFileDialogReport = saveFileDialog;
                }
                return _saveFileDialogReport;
            }
        }

        private void ExportTreeExcel( GraphTreeNodeReportData reportData, string fileFullSpec, GraphNodesKeyParameters nodesKeyParameters )
        {
            SaveFileDialogReport.FileName = fileFullSpec;
            // ReSharper disable once PossibleInvalidOperationException
            if ( SaveFileDialogReport.ShowDialog().Value )
                _presenter.StartGenerateTreeExcelAsync( SaveFileDialogReport.FileName, reportData, nodesKeyParameters );
        }

        public void OpenTreeExcelFile( string reportExcelFilePath )
        {
            ProcessStarter.StartFileProcessAndShowErrorIfPresent(
                reportExcelFilePath, ResourceManagerNDS2.ExportExcelMessages.ExcelExportDialogTitle,
                string.Format( ResourceManagerNDS2.ExportExcelMessages.FileFormedIsItOpen, reportExcelFilePath ),
                string.Format( ResourceManagerNDS2.ExportExcelMessages.FileExcelOpenError, reportExcelFilePath ) );
        }

        protected IUcMessageService MessageService { get { return _ucMessageService; } }

        private FileProcessStarter ProcessStarter
        {
            get
            {
                return _fileProcessStarter ?? ( _fileProcessStarter = new FileProcessStarter( MessageService ) );
            }
        }
    }
}