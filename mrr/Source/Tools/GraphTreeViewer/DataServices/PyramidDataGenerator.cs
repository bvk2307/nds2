using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using FLS.Common.Lib.Collections.Extensions;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration.Enums;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting.TaxPayer;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace.Enums;
using Luxoft.NDS2.Common.Contracts.Services;

namespace Luxoft.NDS2.Client.GraphTreeViewer.DataServices
{
    /// <summary> Data generation implementing <see cref="IPyramidDataService"/>. </summary>
    public class PyramidDataGenerator : IPyramidDataService
    {
        #region Private members

        private const long RootId = 1;

        private long _lastNodeId = RootId;
        private string _rootInn = null;
        private ConcurrentDictionary<string, ConcurrentBag<GraphData>> _nodeLevels = new ConcurrentDictionary<string, ConcurrentBag<GraphData>>();

        private readonly Random s_random = new Random();

        #endregion Private members

        public PyramidDataGenerator()
        {
            _rootInn = ConvertToTaxPayerId( RootId, RootId ).Inn;
        }

        public string RootInn { get { return _rootInn; } }

        /// <summary> Запрашивает данные для узла дерева </summary>
        /// <param name="nodesKeyParameters"></param>
        /// <returns>Возвращает иерархию НП</returns>
        public OperationResult<IReadOnlyCollection<GraphData>> LoadGraphNodes( GraphNodesKeyParameters nodesKeyParameters )
        {
            ConcurrentBag<GraphData> nodes = GenerateNodes( nodesKeyParameters );
            var operationResult = new OperationResult<IReadOnlyCollection<GraphData>>{ Result = nodes.ToReadOnly<GraphData>() };

	////apopov 7.4.2016	//DEBUG!!!
 //           Thread.Sleep( 3*1000 );

            return operationResult;
        }

        /// <summary> Запрашивает данные для пакета узлов дерева для отчёта </summary>
        /// <param name="nodesKeyParameters"></param>
        /// <returns> Данные для пакета узлов дерева для отчёта </returns>
        public OperationResult<IReadOnlyCollection<GraphData>> LoadReportGraphNodes( GraphNodesKeyParameters nodesKeyParameters )
        {
            ConcurrentBag<GraphData> nodes = GenerateNodes( nodesKeyParameters );
            var operationResult = new OperationResult<IReadOnlyCollection<GraphData>>{ Result = nodes.ToReadOnly<GraphData>() };

	////apopov 7.4.2016	//DEBUG!!!
 //           Thread.Sleep( 3*1000 );

            return operationResult;
        }

        public OperationResult<PageResult<GraphData>> LoadGraphNodeChildrenByPage( GraphNodesKeyParameters nodesKeyParameters )
        {
            ConcurrentBag<GraphData> nodes = GenerateNodes( nodesKeyParameters );
            var operationResult = new OperationResult<PageResult<GraphData>> { Result = new PageResult<GraphData> { Rows = nodes.ToList() } };

            return operationResult;
        }

        /// <summary> Запрашивает данные по суммам вычетов контрагента за предыдущие периоды. </summary>
        /// <param name="deductionKeyParameters"></param>
        /// <returns></returns>
        public OperationResult<IReadOnlyCollection<DeductionDetail>> LoadDeductionDetails( DeductionDetailsKeyParameters deductionKeyParameters )
        {
            ConcurrentBag<DeductionDetail> deductionDetails = GenerateDeductionDetails( deductionKeyParameters );
            var operationResult = new OperationResult<IReadOnlyCollection<DeductionDetail>>{ Result = deductionDetails.ToReadOnly<DeductionDetail>() };

	////apopov 7.4.2016	//DEBUG!!!
 //           Thread.Sleep( 3*1000 );

            return operationResult;
        }

        /// <summary> Запрашивает КПП для НП по ИНН. </summary>
        /// <param name="inn"></param>
        /// <returns></returns>
        public OperationResult<IReadOnlyCollection<string>> LoadTaxpayersKpp( string inn )
        {
            throw new NotImplementedException();
        }

        /// <summary> ATTENTION! Supports only children of one level for one parent. </summary>
        /// <param name="nodesKeyParameters"></param>
        /// <returns></returns>
        private ConcurrentBag<GraphData> GenerateNodes( GraphNodesKeyParameters nodesKeyParameters )
        {
            ////apopov 30.11.2015	//DEBUG!!!
            //Thread.Sleep( 3*1000 );

            if ( this == null )
                throw new ApplicationException( "	//apopov 22.12.2015	//DEBUG!!!" );

            ConcurrentBag<GraphData> nodes = null;
            string innParent = nodesKeyParameters.Inn;
            while ( !_nodeLevels.TryGetValue( innParent, out nodes ) )
            {
                long startId;
                int numberOfNodes;
                nodes = GenerateNodes( innParent, out startId, out numberOfNodes );

                if ( _nodeLevels.TryAdd( innParent, nodes ) )
                    break;
                Debug.WriteLine( "--->>> Node IDs from: {0} to: {1} are skipped", startId, startId + numberOfNodes );
            }
            return nodes;
        }

        private ConcurrentBag<GraphData> GenerateNodes( string innParent, out long startId, out int numberOfNodes )
        {
            bool isParentRoot = innParent == RootInn;
            var nodes = new ConcurrentBag<GraphData>();
            GraphData parentNode = FindParentNode( innParent );

            //apopov 11.1.2016	//DEBUG!!!
            //int minNumberOfNodes = isParentRoot ? 1 : 0;
            //int numberOfNodes = s_random.Next( minNumberOfNodes, 16 );

            numberOfNodes = s_random.Next( 46, 50 );

            //big data
            //int minNumberOfNodes = isParentRoot ? 1 : 100;
            //int numberOfNodes = s_random.Next( minNumberOfNodes, 500 );

            //int numberOfNodes = 8;  //6;

            startId = int.MinValue;
            for ( int i = 0; i <= numberOfNodes; i++ ) //#0 is the parent node
            {
                long id, level;
                string parentInn = null;
                if ( i == 0 ) //#0 is the parent node
                {
                    if ( isParentRoot )
                    {
                        id = RootId;
                        level = 1;
                    }
                    else
                    {
                        id = parentNode.Id;
                        level = parentNode.Level;
                        parentInn = parentNode.ParentInn;
                    }
                    startId = id;
                }
                else
                {
                    id = Interlocked.Increment( ref _lastNodeId );
                    level = isParentRoot ? 1 + 1 : parentNode.Level + 1;
                    parentInn = innParent;
                }

                DeclarationType declarationType = (DeclarationType)(int)( id % 4 );

                GraphData node = new GraphData();
                node.Id = 0; //allows define ID to NodeTree    //reader.ReadNullableInt64("ID") ?? 0;
                //node.Id = id;    //reader.ReadNullableInt64("ID") ?? 0;

                ////if ( id == 7 )
                //if ( id == 14 )
                //    id = 7; //generates a zomby node
                //else if ( id == 21 )
                //    id = 7;

                var kpp = id;

                //apopov 31.12.2015	//DEBUG!!!
                if ( id == 60 )
                { 
                    id = 7; //generates a zomby node
                    kpp = id;
                }
                else if ( id == 70 )
                {
                    id = 7;
                    kpp = id;
                }
                else if ( id == 76 )
                {
                    id = 7;
                    kpp = id;
                }

                if ( id == 3 )
                {
                    node.TaxPayerId = new TaxPayerId( "0000000026", kppCode: null ); //physical person
                }
                else
                {
                    node.TaxPayerId = ConvertToTaxPayerId( id, kpp ); //reader.ReadStringNullable("INN");
                    node.Name = "НП # " + id; //reader.ReadStringNullable("NAME");
                }
                node.Level = level; //reader.ReadNullableInt64("LVL") ?? 0;
                if ( parentInn != null )
                    node.ParentInn = parentInn; //reader.ReadStringNullable("PARENT_INN");

                #region Other fields

                //node.Kpp = reader.ReadStringNullable("KPP");
                node.TypeCode = DeclarationTypeCode.Declaration;
                node.Inspection = new CodeValueDictionaryEntry( "5000", "УФНС России по Московской области" ); //reader.ReadString( "SOUN_CODE" ), reader.ReadString( "SOUN_NAME" ) );
                node.ProcessingStage = DeclarationProcessignStage.ProcessedByMs;
                //node.ClientNumber = reader.ReadNullableInt64("BUYERS_CNT") ?? 0;
                //node.SellerNumber = reader.ReadNullableInt64("SELLERS_CNT") ?? 0;
                node.CalcNds = s_random.Next( 0, 10000000 ) + Convert.ToDecimal( s_random.NextDouble() ); //reader.ReadNullableDecimal("CALC_NDS") ?? 0;
                node.DeductionNds = s_random.Next( 0, 10000000 ) + Convert.ToDecimal( s_random.NextDouble() ); //reader.ReadNullableDecimal("DEDUCTION_NDS") ?? 0;
                //node.DiscrepancyAmnt = reader.ReadNullableInt64("DISCREPANCY_AMNT") ?? 0;
                //node.GapDiscrepancyTotal = reader.ReadNullableDecimal("GAP_DISCREPANCY_TOTAL") ?? 0;
                node.MappedAmount = s_random.Next( 0, 10000000 ) + Convert.ToDecimal( s_random.NextDouble() ); //reader.ReadNullableDecimal("MAPPED_AMOUNT") ?? 0;
                node.NotMappedAmnt = s_random.Next( 0, 10000000 ) + Convert.ToDecimal( s_random.NextDouble() ); //reader.ReadNullableDecimal("NOT_MAPPED_AMOUNT") ?? 0;
                //node.Sur = reader.ReadNullableInt("SUR");
                //node.TotalPurchaseAmnt = reader.ReadNullableDecimal("TOTAL_PURCHASE_AMNT") ?? 0;
                //node.TotalSalesAmnt = reader.ReadNullableDecimal("TOTAL_SALES_AMNT") ?? 0;
                //node.VatShare = reader.ReadDecimal("VAT_SHARE");
                node.VatTotal = node.CalcNds - node.DeductionNds; //reader.ReadDecimal("VAT_TOTAL");
                node.SignType = node.VatTotal < -100 ? DeclarationType.ToPay : node.VatTotal < -50 ? DeclarationType.Empty : node.VatTotal < 50 ? DeclarationType.Zero : node.VatTotal < 100 ? DeclarationType.Empty : DeclarationType.ToCharge;

                node.ContractorSpecificationChain = "7709771398^772601001^ООО 'АЛЛЕРГАН СНГ САРЛ'|7707089084^770701001^ДЕПАРТАМЕНТ ЗДРАВООХРАНЕНИЯ ГОРОДА МОСКВЫ|7708807891^773001001^ООО \"ФЭШН РИТЕЙЛ ГРУПП\"";

                #endregion Other fields

                nodes.Add( node );
            }
            //RandomizeNodes( nodes );
            return nodes;
        }

        private ConcurrentBag<DeductionDetail> GenerateDeductionDetails(
            DeductionDetailsKeyParameters deductionKeyParameters )
        {
            var r = new Random();
            var x = r.Next(100);

            if (x < 30)
            {
                return this.GenerateDeductionDetails(2015, 2);
            }

            if ( x < 60 )
            {
                return new ConcurrentBag<DeductionDetail>(); 
            }

            return this.GenerateDeductionDetails(2016, 3); 
        }

        private ConcurrentBag<DeductionDetail> GenerateDeductionDetails(int year, int quarter)
        {
            var r = new Random();
            IList<DeductionDetail> details = new List<DeductionDetail>();
            
            Func<int, int?, DeductionDetail> generateDetail = ( int y, int? q) =>
                {
                    PeriodOrYear periodOrYear;
                    if ( q == null )
                    {
                        periodOrYear = new PeriodOrYear( y );
                    }
                    else
                    {
                        periodOrYear = new PeriodOrYear( q.Value, y );
                    }

                    return new DeductionDetail
                    {
                        DecuctionAmount = r.Next(500, 10000) * 1000,
                        PeriodOrYear = periodOrYear
                    };
            };

            for ( int i = year; i > 2013; i-- )
            {
                if ( i == 2014 )
                {
                    details.Add(generateDetail(i, null)); 
                }
                else
                { 
                    for ( int j = quarter; j > 0; j-- )
                    {
                        details.Add(generateDetail(i, j));
                    }
                }
            }
            details = details.Reverse().ToList();

            return new ConcurrentBag<DeductionDetail>( details );
        }
 
        private GraphData FindParentNode( string parentInn )
        {
            GraphData foundNode = null;
            foreach ( KeyValuePair<string, ConcurrentBag<GraphData>> pair in _nodeLevels )
            {
                if ( null != ( foundNode = pair.Value.FirstOrDefault( nodePar => nodePar.Inn == parentInn ) ) )
                    break;
            }
            return foundNode;
        }

       private static TaxPayerId ConvertToTaxPayerId( long inn, long kpp)
       {
           var innStr = ( inn * 1000000000 ).ToString();
           var kppStr = ( kpp * 1000000000 ).ToString(); 
            
           return new TaxPayerId(innCode: innStr, kppCode: kppStr); //can cause overflow exception on string parsing
        }
    }
}