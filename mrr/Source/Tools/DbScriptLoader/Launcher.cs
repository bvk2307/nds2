﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Oracle.DataAccess.Client;

namespace DbScriptLoader
{
    class Launcher
    {
        private const string SQL_SEARCH_MASK = "*.sql";

        private string _targetCatalog;
        private int _numOfParallelOperations = 4;


        ConcurrentQueue<string> _filesToUpload = new ConcurrentQueue<string>();
        List<Task> _runners = new List<Task>();

        public void Launch(string[] args)
        {
            ReadSettings(args);
            Init();
            Console.WriteLine();

            if(!Directory.Exists("Processed"))
            {
                Directory.CreateDirectory("Processed");
            }

            for (int tidx = 0; tidx <= _numOfParallelOperations; tidx++)
            {
                _runners.Add(Task.Factory.StartNew(Job));
            }

            Console.WriteLine("waiting...");
            Task.WaitAll(_runners.ToArray());
            Console.WriteLine("Done");
            Console.ReadLine();
        }

        private void Init()
        {
            foreach (var file in Directory.GetFiles(_targetCatalog, SQL_SEARCH_MASK))
            {
                _filesToUpload.Enqueue(file);
            }
        }

        private void ReadSettings(string[] args)
        {
            Queue<string> result = new Queue<string>();
            foreach (var s in args)
            {
                result.Enqueue(s);
            }

            _targetCatalog = AppDomain.CurrentDomain.BaseDirectory;

            while (result.Any())
            {
                var cmd = result.Dequeue();
                switch (cmd)
                {
                    case "-count":
                        _numOfParallelOperations = ReadSafeInt(result.Dequeue(), 3);
                        break;
                    case "-target":
                        var newPathVal = result.Dequeue();
                        if (Directory.Exists(newPathVal))
                        {
                            _targetCatalog = newPathVal;
                        }
                        else
                        {
                            Console.WriteLine("некорректный каталог: {0}", newPathVal);
                        }
                        break;

                    default:
                        break;
                }
            }
        }

        private int ReadSafeInt(string val, int defaultVal = 0)
        {
            int res;

            if (int.TryParse(val, out res))
            {
                return res < 0 ? defaultVal : res;
            }

            return defaultVal;
        }

        private void Job()
        {
            Console.WriteLine("Running job {0}", Thread.CurrentThread.ManagedThreadId);
            while (_filesToUpload.Any())
            {
                string val;
                if (!_filesToUpload.TryDequeue(out val))
                {
                    break;
                }
                /*
                int exitCode;
                ProcessStartInfo processInfo;
                Process process;
                int timeout = Int32.MaxValue;

                processInfo = new ProcessStartInfo("sqlplus.exe", "NDS2_INSTALL@NDS2/NDS2_INSTALL @" + val);
                //processInfo = new ProcessStartInfo("sqlplus.exe");
                processInfo.CreateNoWindow = true;
                processInfo.UseShellExecute = true;
                process = Process.Start(processInfo);
                process.WaitForExit(timeout);
                exitCode = process.ExitCode;
                //process.Close();
                */
                var sb = new StringBuilder();
                int linesPerSent = 700;
                int currLineIndex = 0;
                int iterationTime = 1;
                using (var r = new StreamReader(val))
                {
                    while (!r.EndOfStream)
                    {
                        if (currLineIndex == 0)
                        {
                            sb.AppendLine("begin");
                        }

                        sb.AppendLine(r.ReadLine());
                        currLineIndex++;

                        if (currLineIndex == linesPerSent)
                        {
                            iterationTime++;
                            sb.AppendLine("end;");

                            WriteToDb(sb.ToString(), Thread.CurrentThread.ManagedThreadId.ToString(), iterationTime, val);

                            sb.Clear();

                            currLineIndex = 0;
                        }
                    }

                    if (sb.Length > 0)
                    {
                        WriteToDb(sb.ToString(), Thread.CurrentThread.ManagedThreadId.ToString().ToString(), iterationTime, val);
                        sb.Clear();
                    }
                }

                File.Move(val, Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Processed", val));
            }
        }

        private static void WriteToDb(string data, string tid, int cntr, string stuff)
        {

            using (var conn = new OracleConnection("Data Source=NDS2;User Id=NDS2_MRR_USER;Password=NDS2_MRR_USER;Connection Timeout=120;Max Pool Size=150;Min Pool Size=30;"))
            {
                conn.Open();

                using (var cmd = new OracleCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = data;
                    try
                    {
                        cmd.ExecuteNonQuery();
                        Console.WriteLine("{0}-{1} {2} executed", tid, cntr, stuff);
                    }
                    catch (Exception ex)
                    {
                        using (var sw = new StreamWriter(string.Format("err_{0}{1}.txt", tid, cntr)))
                        {
                            sw.WriteLine(ex.Message);
                            sw.WriteLine(data);
                        }
                        Console.WriteLine("error:{0}", ex.ToString());
                    }
                }
            }

        }
    }
}
