﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonComponents.Security.Authorization;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.Services;

namespace Luxoft.NDS2.PermissionChecker
{
    class Program
    {
        static void Main(string[] args)
        {
            int port = 0;
            List<string> hosts = new List<string>();
            foreach (var allKey in ConfigurationManager.AppSettings.AllKeys)
            {
                if (allKey == "port")
                {
                    port = int.Parse(ConfigurationManager.AppSettings[allKey]);
                }

                if (allKey.StartsWith("node_"))
                {
                    hosts.Add(ConfigurationManager.AppSettings[allKey]);
                }
            }


            foreach (var host in hosts)
            {
                Console.WriteLine("HOST: {0}", host);

                var rp = (ISecurityService)(new RemoteServiceProxy(typeof(ISecurityService), string.Format("http://{0}:{1}/nds2", host, port)).GetTransparentProxy());
                var result = rp.GetUserPermissions();
                if (result.Status == ResultStatus.Success)
                {
                    foreach (var accessRight in result.Result)
                    {
                        if (accessRight.PermType == PermissionType.Operation && accessRight.Name.StartsWith("Роль."))
                        {
                            Console.WriteLine("\t\t{0}",accessRight.Name);
                        }
                    }
                }
            }
            Console.WriteLine("Press enter to exit.");
            Console.ReadLine();
        }
    }
}
