﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Luxoft.NDS2.PerformanceCounters
{
    class Program
    {
        static void Main(string[] args)
        {
            if(args.Length > 0)
            {
                if(args[0] == "-u")
                {
                    if(PerformanceCounterCategory.Exists(Luxoft.NDS2.Common.Contracts.PerformanceCounters.Server.CategoryName))
                    {
                        PerformanceCounterCategory.Delete(Luxoft.NDS2.Common.Contracts.PerformanceCounters.Server.CategoryName);
                    }

                    return;
                }

                return;
            }

            if (!PerformanceCounterCategory.Exists(Luxoft.NDS2.Common.Contracts.PerformanceCounters.Server.CategoryName))
            {
                Console.WriteLine("Установка счетчиков производительности {0}:", Luxoft.NDS2.Common.Contracts.PerformanceCounters.Server.CategoryName);
                CounterCreationDataCollection collection = new CounterCreationDataCollection();
                
                Console.WriteLine(Luxoft.NDS2.Common.Contracts.PerformanceCounters.Server.Security.LdapRequestTimeName);
                collection.Add(new CounterCreationData(Luxoft.NDS2.Common.Contracts.PerformanceCounters.Server.Security.LdapRequestTimeName, "", Luxoft.NDS2.Common.Contracts.PerformanceCounters.Server.Security.LdapRequestTimeType));

                Console.WriteLine(Luxoft.NDS2.Common.Contracts.PerformanceCounters.Server.Security.RequestsCacheTotalName);
                collection.Add(new CounterCreationData(Luxoft.NDS2.Common.Contracts.PerformanceCounters.Server.Security.RequestsCacheTotalName, "", Luxoft.NDS2.Common.Contracts.PerformanceCounters.Server.Security.RequestCacheTotalType));

                Console.WriteLine(Luxoft.NDS2.Common.Contracts.PerformanceCounters.Server.Security.RequestsTotalName);
                collection.Add(new CounterCreationData(Luxoft.NDS2.Common.Contracts.PerformanceCounters.Server.Security.RequestsTotalName, "", Luxoft.NDS2.Common.Contracts.PerformanceCounters.Server.Security.RequestTotalType));

                System.Diagnostics.PerformanceCounterCategory.Create(Luxoft.NDS2.Common.Contracts.PerformanceCounters.Server.CategoryName, "", PerformanceCounterCategoryType.SingleInstance, collection);
            }

        }
    }
}
