﻿using System;

namespace XMLValidator
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length < 2)
            {
                Console.WriteLine("XMLValidator.exe XmlFilePath XsdFilePath");
                Environment.Exit(11);
            }

            IXmlValidator xmlValidator = new XmlValidator();

            var rez = xmlValidator.Validate(
                args[0], 
                args[1],
                (msg) => Console.WriteLine("\tError: " + msg),
                (msg) => Console.WriteLine("\tWarning: " + msg));

            if (!rez)
            {
                Console.WriteLine();
                Console.WriteLine("XML Validation Error");
                Environment.Exit(11);
            }
        }
    }
}
