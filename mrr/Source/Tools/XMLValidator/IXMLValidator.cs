using System;
using System.IO;

namespace XMLValidator
{
    /// <summary>
    /// ��������� XML �� XSD,
    /// ���� ������������ ��� �������� ��������� ������,
    /// �� ������ ����� ���������� ������������
    /// </summary>
    public interface IXmlValidator
    {
        /// <summary>
        /// ��������� ������
        /// </summary>
        /// <param name="xmlFilePath">���� � ����� XML</param>
        /// <param name="xsdFilePath">���� � ����� XSD</param>
        /// <param name="onError">������� ��������� ������</param>
        /// <param name="onWarning">������� ��������� ��������������</param>
        /// <returns></returns>
        Boolean Validate(String xmlFilePath,
                         String xsdFilePath,
                         Action<String> onError = null,
                         Action<String> onWarning = null);

        /// <summary>
        /// ��������� �������
        /// </summary>
        /// <param name="xmlStream">����� XML</param>
        /// <param name="xsdStream">����� XSD</param>
        /// <param name="onError">������� ��������� ������</param>
        /// <param name="onWarning">������� ��������� ��������������</param>
        /// <returns></returns>
        Boolean Validate(Stream xmlStream,
                         Stream xsdStream,
                         Action<String> onError = null,
                         Action<String> onWarning = null);
    }
}