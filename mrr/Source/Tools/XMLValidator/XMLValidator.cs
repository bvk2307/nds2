﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Schema;

namespace XMLValidator
{
    public class XmlValidator : IXmlValidator
    {
        public Boolean Validate(String xmlFilePath,
                                String xsdFilePath,
                                Action<String> onError = null,
                                Action<String> onWarning = null)
        {
            bool ret = false;

            try
            {
                using (var xml = new FileStream(xmlFilePath, FileMode.Open, FileAccess.Read))
                using (var xsd = new FileStream(xsdFilePath, FileMode.Open, FileAccess.Read))
                {
                    ret = Validate(xml, xsd, onError, onWarning);
                }
            }
            catch (Exception ex)
            {
                if (onError != null)
                    onError(ex.Message);
                else
                    throw;
            }

            return ret;
        }

        private bool _validationError = false;
        private Action<String> _onError = null;
        private Action<String> _onWarning = null;

        public Boolean Validate(Stream xmlStream,
                                Stream xsdStream,
                                Action<String> onError = null,
                                Action<String> onWarning = null)
        {
            bool ret = true;

            _validationError = false;
            _onError = onError;
            _onWarning = onWarning;

            try
            {
                var xml = new XmlDocument();
                xml.Load(xmlStream);

                var schemaReader = new XmlTextReader(xsdStream);
                XmlSchema schema;
                
                if (_onError != null)
                    schema = XmlSchema.Read(schemaReader, ValidationCallBack);
                else
                    schema = XmlSchema.Read(schemaReader, null);

                xml.Schemas.Add(schema);

                if (_onError != null)
                    xml.Validate(ValidationCallBack);
                else
                    xml.Validate(null);
            }
            catch (Exception ex)
            {
                ret = false;

                if (!_validationError)
                {
                    if (_onError != null)
                        _onError(ex.Message);
                    else
                        throw;
                }
            }

            if (ret)
                ret = !_validationError;

            return ret;

        }

        private void ValidationCallBack(object sender, ValidationEventArgs args)
        {
            if (args.Severity == XmlSeverityType.Error)
            {
                _validationError = true;

                if (_onError != null)
                    _onError(args.Message);
            }
            else
            {
                if (_onWarning != null)
                    _onWarning(args.Message);
            }
        }
    
    }
}
