﻿using System;
using System.Globalization;
using Luxoft.NDS2.Tests.Reports;

namespace ReportsConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            var begin = DateTime.Today.AddDays(-1);
            var end = DateTime.Today;
            var path =@"c:\luxoft\out";
            var gp3 = @"\\m9965-opz288\Share2\Storage2\GP3_ReportNDS";
            var i = 0;
            while (i < args.Length)
            {
                var s = args[i];
                switch (s)
                {
                    case "-begin":
                        ++i;
                        s = args[i];
                        DateTime.TryParseExact(s, "dd.MM.yyyy",
                            CultureInfo.InvariantCulture,
                            DateTimeStyles.None,
                            out begin);
                        break;
                    case "-end":
                        ++i;
                        s = args[i];
                        DateTime.TryParseExact(s, "dd.MM.yyyy",
                            CultureInfo.InvariantCulture,
                            DateTimeStyles.None,
                            out end);
                        break;
                    case "-out":
                        ++i;
                        path = args[i];
                        break;
                    case "-gp3":
                        ++i;
                        gp3 = args[i];
                        break;
                    case "-help":
                    case "/help":
                    case "-?":
                    case "/?":
                        Console.WriteLine("Использование\n" +
                                          "ReportsConsole.exe [-begin dd.MM.yyyy] [-end dd.MM.yyyy] [-out <Path>] [-gp3 <Path>]\n" +
                                          "-begin - дата, начиная с которой строятся отчеты, например 01.06.2015 для 1 июня 15 года\n" +
                                          "-end - дата, до которой строятся отчеты, например 05.06.2015 для 5 июня 15 года\n" +
                                          "-path  - директория, куда нужно поместить отчеты\n" +
                                          "-gp3   - директория, где лежат отчеты ГП-3");
                        return;
                }
                
                ++i;
            }
            Console.WriteLine(string.Format("Период {0} - {1}.\nПуть к отчетам: {2}\nПуть к отчетам ГП-3: {3}",
                begin.ToString("dd.MM.yyyy"), end.ToString("dd.MM.yyyy"),
                path,gp3));
            Console.WriteLine("Построение отчетов по активности пользователей...");
            var arl = new ActivityReportTest.ActivityReportLancher();
            arl.Build(path, begin, end);
            Console.Write("Построение окончено.\nНажмите любую клавишу...");
            Console.ReadKey();
        }
    }
}
