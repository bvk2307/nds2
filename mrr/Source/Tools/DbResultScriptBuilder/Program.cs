﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace DbResultScriptBuilder
{
    class Program
    {
        private static string _basePath;
        private static string _newPath;
        private static string _oldValue;
        private static string _newValue;
        private static string _buildConfig;

        private static readonly Encoding SrcFileEncoder = Encoding.UTF8;
        private static readonly Encoding DstFileEncoder = Encoding.GetEncoding(1251);


        static void Main(string[] args)
        {
            try
            {
                if (args.Length != 4)
                {
                    Console.WriteLine("programm.exe configuration basePath");
                    Environment.Exit(11);
                }

                _basePath = args[0];
                _newPath = args[1];
                var configurationFile = args[2];
                _buildConfig = args[3];

                var configContent = File.ReadLines(configurationFile).ToList();

                _oldValue = configContent[0];
                _newValue = configContent[1];

                if (!BuildScripts())
                {
                    Console.WriteLine("Errors in process file generation");
                    Environment.Exit(11);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Environment.Exit(11);
            }

        }

        
        private static bool BuildScripts()
        {
            var ret = true;

            var doc = XDocument.Load(Path.Combine(_basePath, _buildConfig));
            foreach (var section in doc.Descendants("section"))
            {
                var fout = Path.Combine(_newPath, section.Attribute("outputFile").Value);
                var baseDir = Path.Combine(_basePath, section.Attribute("path").Value);
                var skipErrors = section.Attribute("skipErrors") != null;

                using (var sw = new StreamWriter(fout, false, DstFileEncoder))
                {
                    if (!skipErrors)
                    {
                        sw.WriteLine("---SQL script generated");
                        sw.WriteLine("WHENEVER SQLERROR EXIT SQL.SQLCODE");
                    }
                    sw.WriteLine();

                    sw.WriteLine("prompt running script {0} ...", fout);
                    sw.WriteLine("set feedback on");
                    sw.WriteLine("set define off");

                    bool rez;
                    foreach (var subsection in section.Elements("subsection"))
                    {
                        var relPath = Path.Combine(baseDir, subsection.Attribute("path").Value);

                        rez = ProcessigSectionFiles(relPath, subsection, sw);

                        if (ret) 
                            ret = rez;
                    }

                    rez = ProcessigSectionFiles(baseDir, section, sw);

                    if (ret)
                        ret = rez;

                    sw.WriteLine("prompt Done");
                    sw.Write("exit;");
                    sw.Write("\n");
                }
            }

            return ret;
        }

        private static bool ProcessigSectionFiles(string relPath, XElement subsection, StreamWriter sw)
        {
            var ret = true;

            if (!Directory.Exists(relPath))
                return true;

            var di = new DirectoryInfo(relPath);
            foreach (var fileElm in subsection.Elements("file"))
            {
                foreach (var fi in di.GetFiles(fileElm.Value))
                {
                    var enc = GetEncoding(fi.FullName);

                    if (enc.Equals(SrcFileEncoder))
                    {
                        using (var reader = new StreamReader(fi.FullName, SrcFileEncoder, true))
                        {
                            var srcFileContent = reader.ReadToEnd().Replace(_oldValue, _newValue);

                            var originalByteString = SrcFileEncoder.GetBytes(srcFileContent);
                            var convertedByteString = Encoding.Convert(SrcFileEncoder, DstFileEncoder, originalByteString);

                            var dstFileContent = sw.Encoding.GetString(convertedByteString);

                            sw.Write("prompt [ {0} ]\r\n", Path.GetFileName(fi.FullName));
                            sw.Write(dstFileContent);
                            sw.Write("\n");
                        }
                    }
                    else
                    {
                        Console.WriteLine("File not in UTF-8: " + fi.FullName);
                        ret = false;
                    }
                }
            }

            return ret;
        }

        /// <summary>
        /// Determines a text file's encoding by analyzing its byte order mark (BOM).
        /// Defaults to ASCII when detection of the text file's endianness fails.
        /// </summary>
        /// <param name="filename">The text file to analyze.</param>
        /// <returns>The detected encoding.</returns>
        private static Encoding GetEncoding(string filename)
        {
            // Read the BOM
            var bom = new byte[4];
            using (var file = new FileStream(filename, FileMode.Open, FileAccess.Read))
            {
                file.Read(bom, 0, 4);
            }

            // Analyze the BOM
            if (bom[0] == 0x2b && bom[1] == 0x2f && bom[2] == 0x76) return Encoding.UTF7;
            if (bom[0] == 0xef && bom[1] == 0xbb && bom[2] == 0xbf) return Encoding.UTF8;
            if (bom[0] == 0xff && bom[1] == 0xfe) return Encoding.Unicode; //UTF-16LE
            if (bom[0] == 0xfe && bom[1] == 0xff) return Encoding.BigEndianUnicode; //UTF-16BE
            if (bom[0] == 0 && bom[1] == 0 && bom[2] == 0xfe && bom[3] == 0xff) return Encoding.UTF32;

            return Encoding.ASCII;
        }
    }
}
