﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;

namespace Luxoft.NDS2.SelectionFilterConverter
{
    public class Logger
    {
        private string _filePath = String.Empty;

        private List<EditorParameter> _metaData = new List<EditorParameter>();

        public Logger(string filePath)
        {
            _filePath = filePath;
        }

        public void SetMetaData(List<EditorParameter> metaData)
        {
            _metaData = metaData;
        }

        public void AddLog(string message)
        {
            DateTime dt = DateTime.Now;
            string prefixTime = dt.ToString("HH:mm:ss.fff");

            using (StreamWriter sw = new StreamWriter(_filePath, true))
            {
                sw.WriteLine(string.Format("{0} {1}", prefixTime, message));
                sw.Close();
            }
        }

        public void LogInfo(Filter filter)
        {
            using (var sw = new StreamWriter(_filePath, true))
            {
                try
                {
                    var sb = new StringBuilder("= NEW FILTER ===============================\nГрупп: ")
                        .Append(filter.Groups.Length)
                        .Append(", параметров: ")
                        .Append(filter.Groups.Sum(g => g.Parameters.Length));
                    sw.WriteLine(sb);
                    foreach (var g in filter.Groups)
                    {
                        sb = new StringBuilder("\t")
                            .Append(g.Name)
                            .Append(" ")
                            .Append(g.Parameters.Length)
                            .Append(g.IsEnabled ? " [ON] " : "[OFF] ");
                        sw.WriteLine(sb);
                        foreach (var param in g.Parameters)
                        {
                            var meta = _metaData.FirstOrDefault(x => x.Id == param.AttributeId);
                            sb = new StringBuilder("\t\t")
                                .Append(meta != null ? meta.Name : param.AttributeId.ToString())
                                .Append(param.IsEnabled ? " [ON] " : " [OFF] ")
                                .Append(param.Values.Length)
                                .Append(" ")
                                .Append(param.Operator);
                            sw.WriteLine(sb);
                            foreach (var value in param.Values)
                            {
                                sb = new StringBuilder().Append("\t\t\t")
                                    .Append(value.Value)
                                    .Append(": ")
                                    .Append(value.ValueDescription)
                                    .Append(" [")
                                    .Append(value.ValueFrom)
                                    .Append(" - ")
                                    .Append(value.ValueTo)
                                    .Append("]");
                                sw.WriteLine(sb);
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    sw.WriteLine(e.Message);
                }
                sw.WriteLine("============================================");
                sw.Close();
            }
            
        }

        public void LogInfo(SelectionFilter filter)
        {
            using (var sw = new StreamWriter(_filePath, true))
            {
                try
                {
                    var sb = new StringBuilder("= OLD FILTER ===============================\n")
                        .Append("Выборка ")
                        .Append(filter.SelectionId)
                        .Append(" групп: ")
                        .Append(filter.GroupsFilters.Count)
                        .Append(", параметров: ")
                        .Append(filter.GroupsFilters.Sum(g => g.Filters.Count));
                    sw.WriteLine(sb);
                    foreach (var g in filter.GroupsFilters)
                    {
                        sb = new StringBuilder("\t")
                            .Append(g.Name)
                            .Append(" ")
                            .Append(g.Filters.Count)
                            .Append(g.IsActive ? " [ON] " : "[OFF] ");
                        sw.WriteLine(sb);
                        foreach (var param in g.Filters)
                        {
                            sb = new StringBuilder("\t\t")
                                .Append(param.Name)
                                .Append(param.IsActive ? " [ON] " : " [OFF] ")
                                .Append(param.Values.Count)
                                .Append(" (")
                                .Append(param.FirInternalName)
                                .Append(" / ")
                                .Append(param.InternalName)
                                .Append(")");
                            sw.WriteLine(sb);
                            foreach (var value in param.Values)
                            {
                                sb = new StringBuilder().Append("\t\t\t")
                                    .Append("[")
                                    .Append(value.ValueOne)
                                    .Append(" - ")
                                    .Append(value.ValueTwo)
                                    .Append("] ")
                                    .Append(value.Operator);
                                sw.WriteLine(sb);
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    sw.WriteLine(e.Message);
                }
                sw.WriteLine("============================================");
                sw.Close();
            }
            
        }
    }
}
