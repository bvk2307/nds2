﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.SelectionFilterConverter
{
    public class AutoFilterItem
    {
        public List<GroupFilter> IncludeFilter { get; set; }
        public List<GroupFilter> ExcludeFilter { get; set; }
        public SelectionFilterVersion IncludeVersion { get; set; }
        public SelectionFilterVersion ExcludeVersion { get; set; }
    }
}
