﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace.Enums;
using Luxoft.NDS2.Server.DAL;
using Luxoft.NDS2.Server.DAL.DiscrepancySelection.FilterConverterVersion;
using Luxoft.NDS2.Server.DAL.Selections;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace Luxoft.NDS2.SelectionFilterConverter
{
    public class FilterConverterManager
    {
        private StubServiceHelper _helper;
        private Logger _logger;

        public FilterConverterManager()
        {
            CreateLogger();
            _helper = new StubServiceHelper();
            _logger.SetMetaData(GetAllEditorParameters());
        }

        private void CreateLogger()
        {
            DateTime dtBegin = DateTime.Now;
            _logger = new Logger(
                string.Format("log_{0}.txt",
                    dtBegin.ToString("yyyy-MM-dd_HH-mm-ss")));
        }

        public void VerboseResult(SelectionFilter oldFilter, Filter newFilter, bool forcePrint = false)
        {
            if (oldFilter == null || oldFilter.GroupsFilters == null)
                return;
            try
            {
                var good = newFilter != null &&
                           newFilter.Groups != null &&
                           oldFilter.GroupsFilters.Sum(g => g.Filters.Count) ==
                           newFilter.Groups.Sum(g => g.Parameters.Length);
                _logger.AddLog(good ? "[v] Отлично" : "[x] Есть расхождение");
                if (good && !forcePrint) return;

                _logger.LogInfo(oldFilter);
                _logger.LogInfo(newFilter);
            }
            catch (Exception e)
            {
                _logger.AddLog("[ERROR] VerboseResult: " + e.Message);
            }
        }

        public void Execute()
        {
            AddLog("Begin convert");

            int countSuccessManual = 0;
            int countSuccessAuto = 0;
            int countSuccessFavorite = 0;
            int countSuccessConfigAuto = 0;

            Console.WriteLine("Getting selections...");
            var selectionProcesses = GetAllSelections();

            int selectionManualCountWithVersionUnknow = selectionProcesses
                .Count(p => p.FilterManualVersion == SelectionFilterVersion.Unknow);
            int selectionManualCountWithVersionOne = selectionProcesses
                .Count(p => p.FilterManualVersion == SelectionFilterVersion.VersionOne);
            int selectionAutoCountWithVersionOne = selectionProcesses
                .Count(p => (p.FilterAutoIncludeVersion == SelectionFilterVersion.VersionOne ||
                             p.FilterAutoExcludeVersion == SelectionFilterVersion.VersionOne));

            AddLog(string.Format("COUNT Selections User Unknow Format = {0}", selectionManualCountWithVersionUnknow));
            AddLog(string.Format("COUNT Selections User = {0}", selectionManualCountWithVersionOne));
            AddLog(string.Format("COUNT Selections Auto = {0}", selectionAutoCountWithVersionOne));

            Console.WriteLine("Getting favorite filters...");
            var favoriteFilters = GetAllFavoriteFilters();
            int favoriteCountWithVersionOne = favoriteFilters.Count(p => p.FilterVersion == SelectionFilterVersion.VersionOne);
            AddLog(string.Format("COUNT Favorite filters = {0}", favoriteCountWithVersionOne));

           foreach (var item in selectionProcesses.Where(p => p.FilterManualVersion == SelectionFilterVersion.Unknow))
            { 
                int contentLength = !string.IsNullOrWhiteSpace(item.FilterManualContent) ? item.FilterManualContent.Length : 0;
                var selectionFilterResult = SelectionFilter.Deserialize(item.FilterManualContent);
                AddLog(string.Format("Unknow Format Selection Id = {0}, content length = {1}, errorMessage = \"{2}\"", 
                    item.Id, contentLength, selectionFilterResult.Message));
            }

            if (selectionManualCountWithVersionOne > 0 ||
                selectionAutoCountWithVersionOne > 0 ||
                favoriteCountWithVersionOne > 0)
            {
                var filterConverterManual =
                    FilterConverterVersionCreator.CreateFilterConverterManual(GetAllEditorParameters());
                var filterConverterAuto =
                    FilterConverterVersionCreator.CreateFilterConverterAuto(GetAllEditorParameters());

                foreach (var selection in selectionProcesses)
                {
                    OperationResult rez = _helper.Do(() =>
                    {
                        if (selection.IsManual && selection.FilterManualVersion == SelectionFilterVersion.VersionOne)
                        {
                            selection.FilterManual = filterConverterManual.Convert(selection.FilterManualVersionOne);
                            VerboseResult(selection.FilterManualVersionOne, selection.FilterManual);
                            if (SaveFilter(selection.Id, selection.FilterManual) == ResultStatus.Success)
                                countSuccessManual++;
                        }

                    });
                    PostOperationAction(rez, string.Format("selectionId = {0}", selection.Id));
                }

                foreach (var favoriteFilter in favoriteFilters)
                {
                    OperationResult rez = _helper.Do(() =>
                    {
                        if (favoriteFilter.FilterVersion == SelectionFilterVersion.VersionOne)
                        {
                            favoriteFilter.FilterVersionTwo = filterConverterManual.Convert(favoriteFilter.Filter);
                            VerboseResult(favoriteFilter.Filter, favoriteFilter.FilterVersionTwo);
                            if (UpdateFavoriteFilter(favoriteFilter) == ResultStatus.Success)
                                countSuccessFavorite++;
                        }
                    });
                    PostOperationAction(rez, string.Format("favoriteId = {0}", favoriteFilter.Id));
                }

            }
            AddLog("Report:");
            AddLog(string.Format("   FilterManual SUCCESS = {0}    (error = {1})", countSuccessManual, selectionManualCountWithVersionOne - countSuccessManual));
            AddLog(string.Format("   FilterAuto SUCCESS = {0}    (error = {1})", countSuccessAuto, selectionAutoCountWithVersionOne - countSuccessAuto));
            AddLog(string.Format("   FilterFavorite SUCCESS = {0}    (error = {1})", countSuccessFavorite, favoriteCountWithVersionOne - countSuccessFavorite));
            AddLog("End convert");
            Console.WriteLine("Press Any Key ...");
            Console.ReadKey();
        }

        private void PostOperationAction(OperationResult result, string message)
        {
            if (result.Status != ResultStatus.Success)
                AddLog(string.Format("ERROR, {0}, {1}", message, result.Message));
        }

        private ResultStatus UpdateFavoriteFilter(FavoriteFilter favoriteFilter)
        {
            OperationResult rez = _helper.Do(() =>
            {
                using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(_helper))
                {
                    SelectionsAdapterCreator
                        .SelectionFavoriteFilterAdapter(_helper, connectionFactory)
                        .Update(favoriteFilter);
                }
            });
            if (rez.Status == ResultStatus.Success)
                AddLog(string.Format("Favorite Convert Ok, favoriteFilterId = {0}", favoriteFilter.Id));
            else
                AddLog(string.Format("Favorite Error, Status != Success, favoriteFilterId = {0}, Status = {1}, Message = {2}",
                    favoriteFilter.Id, (int)rez.Status, rez.Message));
            return rez.Status;
        }

        private ResultStatus SaveFilter(long selectionId, Filter filter)
        {
            OperationResult rez = _helper.Do(() =>
            {
                using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(_helper))
                {
                    SelectionsAdapterCreator
                        .SelectionFilterAdapter(_helper, connectionFactory)
                        .SaveFilter(selectionId, filter);
                }
            });
            if (rez.Status == ResultStatus.Success)
                AddLog(string.Format("Selection Convert Ok, selectionId = {0}", selectionId));
            else
                AddLog(string.Format("Selection Convert Error, Status != Success, selectionId = {0}, Status = {1}, Message = {2}",
                    selectionId, (int)rez.Status, rez.Message));
            return rez.Status;
        }

       
        public List<AutoFilterItem> LoadAutoselectionFilter()
        {
            var filters = new List<AutoFilterItem>();
            var filtersRead = new List<AutoFilterItem>();

            OperationResult rez = _helper.Do(() =>
            {
                var data = TableAdapterCreator.ConfigurationAutoselectionFilter(_helper).All();
                foreach (var item in data)
                {
                    var autoFilterItem = DeserializeCfgAutofilter(item.IncludeFilter, item.ExcludeFilter);
                    filtersRead.Add(autoFilterItem);
                }
            });

            if (rez.Status == ResultStatus.Success)
                filters.AddRange(filtersRead);

            return filters;
        }

        private AutoFilterItem DeserializeCfgAutofilter(string incl, string excl)
        {
            var autoFilterItem = new AutoFilterItem();
            autoFilterItem.IncludeVersion = SelectionFilterVersion.Unknow;
            autoFilterItem.ExcludeVersion = SelectionFilterVersion.VersionOne;
            XmlSerializer ser = new XmlSerializer(typeof(List<GroupFilter>));
            try
            {
                using (TextReader reader = new StringReader(incl))
                {
                    autoFilterItem.IncludeFilter = ser.Deserialize(reader) as List<GroupFilter>;
                    autoFilterItem.IncludeVersion = SelectionFilterVersion.VersionOne;
                }
            }
            catch (Exception)
            {
                autoFilterItem.IncludeFilter = null;
                autoFilterItem.IncludeVersion = SelectionFilterVersion.Unknow;
            }

            try
            {
                using (TextReader reader = new StringReader(excl))
                {
                    autoFilterItem.ExcludeFilter = ser.Deserialize(reader) as List<GroupFilter>;
                    autoFilterItem.ExcludeVersion = SelectionFilterVersion.VersionOne;
                }
            }
            catch (Exception)
            {
                autoFilterItem.ExcludeFilter = null;
                autoFilterItem.ExcludeVersion = SelectionFilterVersion.Unknow;
            }

            return autoFilterItem;
        }

        private List<FavoriteFilter> GetAllFavoriteFilters()
        {
            OperationResult<List<FavoriteFilter>> rez = _helper.Do(() =>
            {
                using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(_helper))
                {
                    return SelectionsAdapterCreator
                        .SelectionFavoriteFilterAdapter(_helper, connectionFactory)
                        .All().ToList();
                }
            });
            if (rez.Status == ResultStatus.Success)
            {
                if (rez.Result == null)
                    return new List<FavoriteFilter>();
                return rez.Result;
            }
            else
            {
                AddLog(string.Format("GetAllFavoriteFilters Error, Status != Success, Status = {0}, Message = {1}", (int)rez.Status, rez.Message));
                return new List<FavoriteFilter>();
            }
        }

        private List<SelectionProcess> GetAllSelections()
        {
            OperationResult<List<SelectionProcess>> rez = _helper.Do(() =>
            {
                return TableAdapterCreator.DiscrepancySelectionAdpater(_helper).GetAllSelections();
            });
            if (rez.Status == ResultStatus.Success)
            {
                if (rez.Result == null)
                    return new List<SelectionProcess>();
                return rez.Result;
            }
            else
            {
                AddLog(string.Format("GetSelectionProcess Error, Status != Success, Status = {0}, Message = {1}", (int)rez.Status, rez.Message));
                return new List<SelectionProcess>();
            }
        }

        private List<EditorParameter> _metaData;

        public List<EditorParameter> GetAllEditorParameters()
        {
            if (_metaData != null && _metaData.Count > 0)
                return _metaData;

            Console.WriteLine("Getting meta data for filter parameters...");
            var rez =
                _helper.Do(()=>
                    {
                        List<EditorParameter> editorParameters = new List<EditorParameter>();
                        using (ConnectionFactoryBase connectionFactory = new SingleConnectionFactory(_helper))
                        {
                            editorParameters = SelectionsAdapterCreator
                                .SelectionFilterAdapter(_helper, connectionFactory)
                                .GetFilterEditorParameters(SelectionType.Hand).ToList();
                        }
                        return editorParameters;
                    }
                );
            if (rez.Status == ResultStatus.Success)
            {
                _metaData = rez.Result ?? new List<EditorParameter>();
            }
            else
            {
                AddLog(string.Format("GetAllEditorParameters Error, Status != Success, Status = {0}, Message = {1}", (int)rez.Status, rez.Message));
                _metaData = new List<EditorParameter>();
            }
            return _metaData;
        }

        private void AddLog(string message)
        {
            Console.WriteLine(message);
            _logger.AddLog(message);
        }
    }
}
