﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Server.Common;
using Luxoft.NDS2.Server.DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;

namespace Luxoft.NDS2.SelectionFilterConverter
{
    public class StubServiceHelper : Luxoft.NDS2.Server.DAL.IServiceProvider
    {
        public OperationResult<T> Do<T>(Func<T> work, IEnumerable<string> permissions = null) where T : class, new()
        {
            var result = new OperationResult<T>();

            try
            {
                result.Result = work();
                result.Status = ResultStatus.Success;
            }
            catch (ThriftDataTransferException thriftEx)
            {
                LogError(thriftEx.Message, "Do", thriftEx);
                result.Status = ResultStatus.Error;
                result.Message = "Ошибка обмена данными с сервисом взаимодействия";
            }
            catch (SovRequestProcessingException sovEx)
            {
                LogError(sovEx.Message, "Do", sovEx);
                result.Status = ResultStatus.Error;
                result.Message = "Ошибка загрузки данных сервиса взаимодействия";
            }
            catch (DatabaseException dbEx)
            {
                LogError(dbEx.InnerException.Message, "Do DB", dbEx.InnerException);
                result.Status = ResultStatus.Error;
                result.Message = dbEx.InnerException.Message;
            }
            catch (ObjectNotFoundException exception)
            {
                LogError(exception.Message, "Do", exception);
                result.Status = ResultStatus.NoDataFound;
                result.Message = exception.Message;
            }
            catch (AccessDeniedException exception)
            {
                LogError(exception.Message, "Do", exception);
                result.Status = ResultStatus.Denied;
                result.Message = exception.Message;
            }
            catch (Exception exception)
            {
                LogError(exception.Message, "Do", exception);
                result.Status = ResultStatus.Error;
                result.Message = exception.Message;
            }

            return result;
        }

        public OperationResult Do(Action work, IEnumerable<string> permissions = null)
        {
            var result = new OperationResult();

            try
            {
                work();
                result.Status = ResultStatus.Success;
            }
            catch (DatabaseException dbEx)
            {
                LogError(dbEx.InnerException.Message, "Do DB", dbEx.InnerException);
                result.Status = ResultStatus.Error;
                result.Message = dbEx.InnerException.Message;
            }
            catch (ObjectNotFoundException exception)
            {
                LogError(exception.Message, "Do", exception);
                result.Status = ResultStatus.NoDataFound;
                result.Message = exception.Message;
            }
            catch (Exception exception)
            {
                LogError(exception.Message, "Do", exception);
                result.Status = ResultStatus.Error;
                result.Message = exception.Message;
            }

            return result;
        }

        public string GetConfigurationValue(string key)
        {
            string value = String.Empty;
            try
            {
                value = ConfigurationManager.AppSettings[key];
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Ошибка чтения из AppSettings, {0}", ex.Message));
            }
            return value;
        }

        public string GetQueryText(string scope, string qName)
        {
            throw new NotImplementedException();
        }

        public string CurrentUserSID { get; private set; }

        public void LogDebug(string message, string methodName = null, IList<KeyValuePair<string, object>> additionalProperties = null)
        {
        }

        public void LogNotification(string message, string methodName = null, IList<KeyValuePair<string, object>> additionalProperties = null)
        {
        }

        public void LogError(string message, string methodName = null, Exception ex = null, IList<KeyValuePair<string, object>> additionalProperties = null)
        {
        }

        public void LogWarning(string message, string methodName = null, IList<KeyValuePair<string, object>> additionalProperties = null)
        {
        }
    }
}
