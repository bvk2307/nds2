﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Luxoft.NDS2.HealthMonitor.Configuration
{
    public class ConfigurationProvider
    {
        public int Port
        {
            get { return int.Parse(ConfigurationManager.AppSettings["port"]); }
        }

        public List<string> Hosts
        {
            get
            {
                List<string> result = new List<string>();
                foreach (var key in ConfigurationManager.AppSettings.AllKeys)
                {
                    if (key.StartsWith("host_"))
                    {
                        result.Add(ConfigurationManager.AppSettings[key]);
                    }
                }

                return result;
            }
        }
    }
}
