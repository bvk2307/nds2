﻿namespace Luxoft.NDS2.HealthMonitor
{
    partial class NodeStatus
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNodeName = new System.Windows.Forms.Label();
            this.lblDatabaseStatus = new System.Windows.Forms.Label();
            this.lblLdap = new System.Windows.Forms.Label();
            this.lblSOV = new System.Windows.Forms.Label();
            this.lblLoading = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblNodeName
            // 
            this.lblNodeName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lblNodeName.AutoSize = true;
            this.lblNodeName.Location = new System.Drawing.Point(3, 7);
            this.lblNodeName.Name = "lblNodeName";
            this.lblNodeName.Size = new System.Drawing.Size(77, 13);
            this.lblNodeName.TabIndex = 0;
            this.lblNodeName.Text = "m9965-opz327";
            // 
            // lblDatabaseStatus
            // 
            this.lblDatabaseStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lblDatabaseStatus.AutoSize = true;
            this.lblDatabaseStatus.ForeColor = System.Drawing.Color.Green;
            this.lblDatabaseStatus.Location = new System.Drawing.Point(100, 7);
            this.lblDatabaseStatus.Name = "lblDatabaseStatus";
            this.lblDatabaseStatus.Size = new System.Drawing.Size(23, 13);
            this.lblDatabaseStatus.TabIndex = 1;
            this.lblDatabaseStatus.Text = "БД";
            // 
            // lblLdap
            // 
            this.lblLdap.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lblLdap.AutoSize = true;
            this.lblLdap.ForeColor = System.Drawing.Color.Green;
            this.lblLdap.Location = new System.Drawing.Point(152, 7);
            this.lblLdap.Name = "lblLdap";
            this.lblLdap.Size = new System.Drawing.Size(35, 13);
            this.lblLdap.TabIndex = 2;
            this.lblLdap.Text = "LDAP";
            // 
            // lblSOV
            // 
            this.lblSOV.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lblSOV.AutoSize = true;
            this.lblSOV.ForeColor = System.Drawing.Color.Green;
            this.lblSOV.Location = new System.Drawing.Point(207, 7);
            this.lblSOV.Name = "lblSOV";
            this.lblSOV.Size = new System.Drawing.Size(29, 13);
            this.lblSOV.TabIndex = 3;
            this.lblSOV.Text = "СОВ";
            // 
            // lblLoading
            // 
            this.lblLoading.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lblLoading.AutoSize = true;
            this.lblLoading.Location = new System.Drawing.Point(242, 7);
            this.lblLoading.Name = "lblLoading";
            this.lblLoading.Size = new System.Drawing.Size(78, 13);
            this.lblLoading.TabIndex = 4;
            this.lblLoading.Text = "Обновление...";
            // 
            // NodeStatus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblLoading);
            this.Controls.Add(this.lblSOV);
            this.Controls.Add(this.lblLdap);
            this.Controls.Add(this.lblDatabaseStatus);
            this.Controls.Add(this.lblNodeName);
            this.Name = "NodeStatus";
            this.Size = new System.Drawing.Size(339, 28);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNodeName;
        private System.Windows.Forms.Label lblDatabaseStatus;
        private System.Windows.Forms.Label lblLdap;
        private System.Windows.Forms.Label lblSOV;
        private System.Windows.Forms.Label lblLoading;
    }
}
