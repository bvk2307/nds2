﻿using System;
using System.Windows.Forms;

namespace Luxoft.NDS2.HealthMonitor
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            flpMain.Controls.Add(new NodeStatus());
        }
    }
}
