﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Luxoft.NDS2.Common.Contracts.Services;

namespace Luxoft.NDS2.HealthMonitor
{
    public enum NodePoint
    {
        Database,
        MrrHost,
        SovService,
        LdapService,
    }

    public enum NodePointStatus
    {
        Pending,
        Failed,
        Ok
    }

    public class NodeMonitor
    {
        private IEchoService _srv;

        private NodePointStatus _dataBase;
        private NodePointStatus _mrrServiceHost;
        private NodePointStatus _sovService;
        private NodePointStatus _ldapService;

        private string _host;
        private int _port;

        //public event EventHandler<NodePointStatus> OnStateChanged;

        public NodeMonitor(IEchoService service, string host, int port)
        {
            _srv = service;
            _host = host;
            _port = port;

            _dataBase = NodePointStatus.Pending;
            _mrrServiceHost = NodePointStatus.Pending;
            _sovService = NodePointStatus.Pending;
            _ldapService = NodePointStatus.Pending;
        }


        public void Refresh()
        {
            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += (sender, args) =>
            {
                if (RefreshMrrHost())
                {
                    args.Result = new object();
                }
            };
            worker.RunWorkerCompleted += WorkerOnRunWorkerCompleted;

        }

        private void WorkerOnRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs runWorkerCompletedEventArgs)
        {
            if (runWorkerCompletedEventArgs.Result == null)
            {
                FireStateChanged(NodePoint.Database, NodePointStatus.Pending);
                FireStateChanged(NodePoint.SovService, NodePointStatus.Pending);
                FireStateChanged(NodePoint.LdapService, NodePointStatus.Pending);
            }
            else
            {
                
            }
        }

        private bool RefreshMrrHost()
        {
            var client = new TcpClient();
            bool result = false;
            NodePointStatus status = NodePointStatus.Failed;

            try
            {
                client.Connect(_host, _port);

                if (client.Connected)
                {
                    status = NodePointStatus.Ok;
                    result = true;
                }
                else
                {
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                if(client.Connected)
                    client.Close();
            }

            FireStateChanged(NodePoint.MrrHost, status);


            return result;
        }

         

        private bool RefreshDatabase()
        {
            try
            {
                _srv.PingDatabase();
                FireStateChanged(NodePoint.Database, NodePointStatus.Ok);

                return true;
            }
            catch (Exception ex)
            {
                FireStateChanged(NodePoint.Database, NodePointStatus.Failed);
            }

            return false;
        }

        private bool RefreshSov()
        {
            try
            {
                _srv.PingSOV();
                FireStateChanged(NodePoint.SovService, NodePointStatus.Ok);
                return true;
            }
            catch (Exception ex)
            {
                FireStateChanged(NodePoint.SovService, NodePointStatus.Failed);
            }

            return false;
        }

        private void FireStateChanged(NodePoint point, NodePointStatus status)
        {
//            var hndl = OnStateChanged;
//            if (hndl != null)
//            {
//                hndl.Invoke(point, status);
//            }
        }
    }
}
