﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Luxoft.NDS2.HealthMonitor.Properties;

namespace Luxoft.NDS2.HealthMonitor
{
    public class TrayApp : IDisposable
    {
        private NotifyIcon notifyIcon;
        private Timer _timer;
        private MessageBox mb;

        public TrayApp()
        {
            notifyIcon = new NotifyIcon();
            _timer = new Timer();
            _timer.Interval = 5000; 
            _timer.Tick += _timer_Tick;
            _timer.Start();
        }

        void _timer_Tick(object sender, EventArgs e)
        {
            notifyIcon.ShowBalloonTip(1000, DateTime.Now.ToString(), "test", ToolTipIcon.Info);
            notifyIcon.Icon = notifyIcon.Icon == Resources.link_delete ?  Resources.link : Resources.link_delete ;
        }

        public void AttachToTray()
        {
            notifyIcon.MouseClick += NotifyIconOnMouseClick;
            notifyIcon.Icon = Resources.link;
            notifyIcon.Text = "Мониторинг здоровья АСК-НДС2";
            notifyIcon.Visible = true;

            // Attach a context menu.
            //notifyIcon.ContextMenuStrip = new ContextMenus().Create();
        }

        private void NotifyIconOnMouseClick(object sender, MouseEventArgs mouseEventArgs)
        {
            if (mouseEventArgs.Button == MouseButtons.Left)
            {
                FormMain mnfrm = new FormMain();
                mnfrm.ShowDialog();
            }
            else
            {
                Application.Exit();                
            }
        }

        public void Dispose()
        {
            notifyIcon.Dispose();
            _timer.Stop();
            _timer.Dispose();
        }
    }
}
