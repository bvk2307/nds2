﻿namespace EmulatorSEOD
{
    partial class View
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.UltraWinToolTip.UltraToolTipInfo ultraToolTipInfo12 = new Infragistics.Win.UltraWinToolTip.UltraToolTipInfo("Сведения по статусам исполнения автотребования", Infragistics.Win.ToolTipImage.Default, null, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinToolTip.UltraToolTipInfo ultraToolTipInfo11 = new Infragistics.Win.UltraWinToolTip.UltraToolTipInfo("Сведения о получении данных автотребования/автоистребования", Infragistics.Win.ToolTipImage.Default, null, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinToolTip.UltraToolTipInfo ultraToolTipInfo10 = new Infragistics.Win.UltraWinToolTip.UltraToolTipInfo("Сведения по ответу на автотребование", Infragistics.Win.ToolTipImage.Default, null, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinToolTip.UltraToolTipInfo ultraToolTipInfo9 = new Infragistics.Win.UltraWinToolTip.UltraToolTipInfo("Сведения о ходе проведения КНП", Infragistics.Win.ToolTipImage.Default, null, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinToolTip.UltraToolTipInfo ultraToolTipInfo5 = new Infragistics.Win.UltraWinToolTip.UltraToolTipInfo("Сведения о поступлении документов от НП ст. 93", Infragistics.Win.ToolTipImage.Default, null, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinToolTip.UltraToolTipInfo ultraToolTipInfo6 = new Infragistics.Win.UltraWinToolTip.UltraToolTipInfo("Данные по статусам Решения о продлении сроков представления документов", Infragistics.Win.ToolTipImage.Default, null, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinToolTip.UltraToolTipInfo ultraToolTipInfo7 = new Infragistics.Win.UltraWinToolTip.UltraToolTipInfo("Данные по статусам Поручения об истребовании документов", Infragistics.Win.ToolTipImage.Default, null, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinToolTip.UltraToolTipInfo ultraToolTipInfo8 = new Infragistics.Win.UltraWinToolTip.UltraToolTipInfo("Данные по статусам исполнения автоистребования", Infragistics.Win.ToolTipImage.Default, null, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinToolTip.UltraToolTipInfo ultraToolTipInfo4 = new Infragistics.Win.UltraWinToolTip.UltraToolTipInfo("Данные по статусам Ответа НП на Требование о представление документов ст. 93.1", Infragistics.Win.ToolTipImage.Default, null, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinToolTip.UltraToolTipInfo ultraToolTipInfo3 = new Infragistics.Win.UltraWinToolTip.UltraToolTipInfo("Данные по журналу учёта полученных и выставленных счетов-фактур", Infragistics.Win.ToolTipImage.Default, null, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinToolTip.UltraToolTipInfo ultraToolTipInfo2 = new Infragistics.Win.UltraWinToolTip.UltraToolTipInfo("Ответ на запрос о предоставлении выписки по операциям на счетах", Infragistics.Win.ToolTipImage.Default, "", Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinToolTip.UltraToolTipInfo ultraToolTipInfo1 = new Infragistics.Win.UltraWinToolTip.UltraToolTipInfo("Ответ на запрос о предоставлении выписки по операциям на счетах", Infragistics.Win.ToolTipImage.Default, "", Infragistics.Win.DefaultableBoolean.Default);
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.gridResult = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.gridDebug = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.btnEOD02 = new Infragistics.Win.Misc.UltraButton();
            this.btnEOD05 = new Infragistics.Win.Misc.UltraButton();
            this.gridIncome = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.btnRefresh = new Infragistics.Win.Misc.UltraButton();
            this.btnEOD03 = new Infragistics.Win.Misc.UltraButton();
            this.btnEOD04 = new Infragistics.Win.Misc.UltraButton();
            this.ultraTabControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.btnEOD08 = new Infragistics.Win.Misc.UltraButton();
            this.btnEOD07 = new Infragistics.Win.Misc.UltraButton();
            this.btnEOD09 = new Infragistics.Win.Misc.UltraButton();
            this.btnEOD06 = new Infragistics.Win.Misc.UltraButton();
            this.btnEOD10 = new Infragistics.Win.Misc.UltraButton();
            this.ultraToolTipManager1 = new Infragistics.Win.UltraWinToolTip.UltraToolTipManager(this.components);
            this.btnEOD11 = new Infragistics.Win.Misc.UltraButton();
            this.btnEOD13 = new Infragistics.Win.Misc.UltraButton();
            this.btnEOD14 = new Infragistics.Win.Misc.UltraButton();
            this.btnEOD01 = new Infragistics.Win.Misc.UltraButton();
            this.comboTypeDocument = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.labelTypeDocument = new Infragistics.Win.Misc.UltraLabel();
            this.numericRegNumber = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.checkRegNumber = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.checkSendAll = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.checkTop1000 = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridResult)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridDebug)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridIncome)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).BeginInit();
            this.ultraTabControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboTypeDocument)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericRegNumber)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.gridResult);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(1136, 140);
            // 
            // gridResult
            // 
            this.gridResult.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.gridResult.DisplayLayout.Override.AllowColMoving = Infragistics.Win.UltraWinGrid.AllowColMoving.NotAllowed;
            this.gridResult.DisplayLayout.Override.AllowColSwapping = Infragistics.Win.UltraWinGrid.AllowColSwapping.NotAllowed;
            this.gridResult.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.gridResult.DisplayLayout.Override.AllowGroupBy = Infragistics.Win.DefaultableBoolean.False;
            this.gridResult.DisplayLayout.Override.AllowGroupMoving = Infragistics.Win.UltraWinGrid.AllowGroupMoving.NotAllowed;
            this.gridResult.DisplayLayout.Override.AllowGroupSwapping = Infragistics.Win.UltraWinGrid.AllowGroupSwapping.NotAllowed;
            this.gridResult.DisplayLayout.Override.AllowMultiCellOperations = Infragistics.Win.UltraWinGrid.AllowMultiCellOperation.None;
            this.gridResult.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            this.gridResult.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.gridResult.DisplayLayout.Override.CellMultiLine = Infragistics.Win.DefaultableBoolean.False;
            this.gridResult.DisplayLayout.Override.ColumnSizingArea = Infragistics.Win.UltraWinGrid.ColumnSizingArea.EntireColumn;
            this.gridResult.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.gridResult.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.Single;
            this.gridResult.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.gridResult.DisplayLayout.Override.SelectTypeGroupByRow = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.gridResult.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            this.gridResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridResult.Location = new System.Drawing.Point(0, 0);
            this.gridResult.Name = "gridResult";
            this.gridResult.Size = new System.Drawing.Size(1136, 140);
            this.gridResult.TabIndex = 0;
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.gridDebug);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(1136, 140);
            // 
            // gridDebug
            // 
            this.gridDebug.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.gridDebug.DisplayLayout.Override.AllowColMoving = Infragistics.Win.UltraWinGrid.AllowColMoving.NotAllowed;
            this.gridDebug.DisplayLayout.Override.AllowColSwapping = Infragistics.Win.UltraWinGrid.AllowColSwapping.NotAllowed;
            this.gridDebug.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.gridDebug.DisplayLayout.Override.AllowGroupBy = Infragistics.Win.DefaultableBoolean.False;
            this.gridDebug.DisplayLayout.Override.AllowGroupMoving = Infragistics.Win.UltraWinGrid.AllowGroupMoving.NotAllowed;
            this.gridDebug.DisplayLayout.Override.AllowGroupSwapping = Infragistics.Win.UltraWinGrid.AllowGroupSwapping.NotAllowed;
            this.gridDebug.DisplayLayout.Override.AllowMultiCellOperations = Infragistics.Win.UltraWinGrid.AllowMultiCellOperation.None;
            this.gridDebug.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            this.gridDebug.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.gridDebug.DisplayLayout.Override.CellMultiLine = Infragistics.Win.DefaultableBoolean.False;
            this.gridDebug.DisplayLayout.Override.ColumnSizingArea = Infragistics.Win.UltraWinGrid.ColumnSizingArea.EntireColumn;
            this.gridDebug.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.gridDebug.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.Single;
            this.gridDebug.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.gridDebug.DisplayLayout.Override.SelectTypeGroupByRow = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.gridDebug.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            this.gridDebug.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridDebug.Location = new System.Drawing.Point(0, 0);
            this.gridDebug.Name = "gridDebug";
            this.gridDebug.Size = new System.Drawing.Size(1136, 140);
            this.gridDebug.TabIndex = 0;
            // 
            // btnEOD02
            // 
            this.btnEOD02.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnEOD02.Location = new System.Drawing.Point(92, 470);
            this.btnEOD02.Name = "btnEOD02";
            this.btnEOD02.Size = new System.Drawing.Size(75, 23);
            this.btnEOD02.TabIndex = 3;
            this.btnEOD02.Text = "ЭОД-2";
            ultraToolTipInfo12.ToolTipText = "Сведения по статусам исполнения автотребования";
            this.ultraToolTipManager1.SetUltraToolTip(this.btnEOD02, ultraToolTipInfo12);
            // 
            // btnEOD05
            // 
            this.btnEOD05.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnEOD05.Location = new System.Drawing.Point(335, 470);
            this.btnEOD05.Name = "btnEOD05";
            this.btnEOD05.Size = new System.Drawing.Size(75, 23);
            this.btnEOD05.TabIndex = 6;
            this.btnEOD05.Text = "ЭОД-5";
            ultraToolTipInfo11.ToolTipText = "Сведения о получении данных автотребования/автоистребования";
            this.ultraToolTipManager1.SetUltraToolTip(this.btnEOD05, ultraToolTipInfo11);
            // 
            // gridIncome
            // 
            this.gridIncome.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridIncome.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.gridIncome.DisplayLayout.Override.AllowColMoving = Infragistics.Win.UltraWinGrid.AllowColMoving.NotAllowed;
            this.gridIncome.DisplayLayout.Override.AllowColSwapping = Infragistics.Win.UltraWinGrid.AllowColSwapping.NotAllowed;
            this.gridIncome.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.gridIncome.DisplayLayout.Override.AllowGroupBy = Infragistics.Win.DefaultableBoolean.False;
            this.gridIncome.DisplayLayout.Override.AllowGroupMoving = Infragistics.Win.UltraWinGrid.AllowGroupMoving.NotAllowed;
            this.gridIncome.DisplayLayout.Override.AllowGroupSwapping = Infragistics.Win.UltraWinGrid.AllowGroupSwapping.NotAllowed;
            this.gridIncome.DisplayLayout.Override.AllowMultiCellOperations = Infragistics.Win.UltraWinGrid.AllowMultiCellOperation.None;
            this.gridIncome.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            this.gridIncome.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.gridIncome.DisplayLayout.Override.CellMultiLine = Infragistics.Win.DefaultableBoolean.False;
            this.gridIncome.DisplayLayout.Override.ColumnSizingArea = Infragistics.Win.UltraWinGrid.ColumnSizingArea.EntireColumn;
            this.gridIncome.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.Single;
            this.gridIncome.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.gridIncome.DisplayLayout.Override.SelectTypeGroupByRow = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.gridIncome.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            this.gridIncome.Location = new System.Drawing.Point(12, 65);
            this.gridIncome.Name = "gridIncome";
            this.gridIncome.Size = new System.Drawing.Size(1140, 229);
            this.gridIncome.TabIndex = 0;
            this.gridIncome.Text = "Income";
            this.gridIncome.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.gridIncome_InitializeLayout);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefresh.Location = new System.Drawing.Point(1077, 470);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(75, 23);
            this.btnRefresh.TabIndex = 13;
            this.btnRefresh.Text = "Обновить";
            // 
            // btnEOD03
            // 
            this.btnEOD03.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnEOD03.Location = new System.Drawing.Point(173, 470);
            this.btnEOD03.Name = "btnEOD03";
            this.btnEOD03.Size = new System.Drawing.Size(75, 23);
            this.btnEOD03.TabIndex = 4;
            this.btnEOD03.Text = "ЭОД-3";
            ultraToolTipInfo10.ToolTipText = "Сведения по ответу на автотребование";
            this.ultraToolTipManager1.SetUltraToolTip(this.btnEOD03, ultraToolTipInfo10);
            // 
            // btnEOD04
            // 
            this.btnEOD04.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnEOD04.Location = new System.Drawing.Point(254, 470);
            this.btnEOD04.Name = "btnEOD04";
            this.btnEOD04.Size = new System.Drawing.Size(75, 23);
            this.btnEOD04.TabIndex = 5;
            this.btnEOD04.Text = "ЭОД-4";
            ultraToolTipInfo9.ToolTipText = "Сведения о ходе проведения КНП";
            this.ultraToolTipManager1.SetUltraToolTip(this.btnEOD04, ultraToolTipInfo9);
            // 
            // ultraTabControl1
            // 
            this.ultraTabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraTabControl1.Controls.Add(this.ultraTabSharedControlsPage1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl1);
            this.ultraTabControl1.Controls.Add(this.ultraTabPageControl2);
            this.ultraTabControl1.Location = new System.Drawing.Point(12, 298);
            this.ultraTabControl1.Name = "ultraTabControl1";
            this.ultraTabControl1.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.ultraTabControl1.Size = new System.Drawing.Size(1140, 166);
            this.ultraTabControl1.TabIndex = 1;
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "Result";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "Debug";
            this.ultraTabControl1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2});
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(1136, 140);
            // 
            // btnEOD08
            // 
            this.btnEOD08.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnEOD08.Location = new System.Drawing.Point(578, 470);
            this.btnEOD08.Name = "btnEOD08";
            this.btnEOD08.Size = new System.Drawing.Size(75, 23);
            this.btnEOD08.TabIndex = 9;
            this.btnEOD08.Text = "ЭОД-8";
            ultraToolTipInfo5.ToolTipText = "Сведения о поступлении документов от НП ст. 93";
            this.ultraToolTipManager1.SetUltraToolTip(this.btnEOD08, ultraToolTipInfo5);
            // 
            // btnEOD07
            // 
            this.btnEOD07.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnEOD07.Location = new System.Drawing.Point(497, 470);
            this.btnEOD07.Name = "btnEOD07";
            this.btnEOD07.Size = new System.Drawing.Size(75, 23);
            this.btnEOD07.TabIndex = 8;
            this.btnEOD07.Text = "ЭОД-7";
            ultraToolTipInfo6.ToolTipText = "Данные по статусам Решения о продлении сроков представления документов";
            this.ultraToolTipManager1.SetUltraToolTip(this.btnEOD07, ultraToolTipInfo6);
            // 
            // btnEOD09
            // 
            this.btnEOD09.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnEOD09.Location = new System.Drawing.Point(659, 470);
            this.btnEOD09.Name = "btnEOD09";
            this.btnEOD09.Size = new System.Drawing.Size(75, 23);
            this.btnEOD09.TabIndex = 10;
            this.btnEOD09.Text = "ЭОД-9";
            ultraToolTipInfo7.ToolTipText = "Данные по статусам Поручения об истребовании документов";
            this.ultraToolTipManager1.SetUltraToolTip(this.btnEOD09, ultraToolTipInfo7);
            // 
            // btnEOD06
            // 
            this.btnEOD06.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnEOD06.Location = new System.Drawing.Point(416, 470);
            this.btnEOD06.Name = "btnEOD06";
            this.btnEOD06.Size = new System.Drawing.Size(75, 23);
            this.btnEOD06.TabIndex = 7;
            this.btnEOD06.Text = "ЭОД-6";
            ultraToolTipInfo8.ToolTipText = "Данные по статусам исполнения автоистребования";
            this.ultraToolTipManager1.SetUltraToolTip(this.btnEOD06, ultraToolTipInfo8);
            // 
            // btnEOD10
            // 
            this.btnEOD10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnEOD10.Location = new System.Drawing.Point(740, 470);
            this.btnEOD10.Name = "btnEOD10";
            this.btnEOD10.Size = new System.Drawing.Size(75, 23);
            this.btnEOD10.TabIndex = 11;
            this.btnEOD10.Text = "ЭОД-10";
            ultraToolTipInfo4.ToolTipText = "Данные по статусам Ответа НП на Требование о представление документов ст. 93.1";
            this.ultraToolTipManager1.SetUltraToolTip(this.btnEOD10, ultraToolTipInfo4);
            // 
            // ultraToolTipManager1
            // 
            this.ultraToolTipManager1.ContainingControl = this;
            // 
            // btnEOD11
            // 
            this.btnEOD11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnEOD11.Location = new System.Drawing.Point(821, 470);
            this.btnEOD11.Name = "btnEOD11";
            this.btnEOD11.Size = new System.Drawing.Size(75, 23);
            this.btnEOD11.TabIndex = 12;
            this.btnEOD11.Text = "ЭОД-11";
            ultraToolTipInfo3.ToolTipText = "Данные по журналу учёта полученных и выставленных счетов-фактур";
            this.ultraToolTipManager1.SetUltraToolTip(this.btnEOD11, ultraToolTipInfo3);
            this.btnEOD11.Click += new System.EventHandler(this.btnEOD11_Click);
            // 
            // btnEOD13
            // 
            this.btnEOD13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnEOD13.Location = new System.Drawing.Point(902, 470);
            this.btnEOD13.Name = "btnEOD13";
            this.btnEOD13.Size = new System.Drawing.Size(75, 23);
            this.btnEOD13.TabIndex = 14;
            this.btnEOD13.Text = "ЭОД-13";
            ultraToolTipInfo2.ToolTipText = "Ответ на запрос о предоставлении выписки по операциям на счетах";
            this.ultraToolTipManager1.SetUltraToolTip(this.btnEOD13, ultraToolTipInfo2);
            this.btnEOD13.Click += new System.EventHandler(this.btnEOD13_Click);
            // 
            // btnEOD14
            // 
            this.btnEOD14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnEOD14.Location = new System.Drawing.Point(983, 470);
            this.btnEOD14.Name = "btnEOD14";
            this.btnEOD14.Size = new System.Drawing.Size(75, 23);
            this.btnEOD14.TabIndex = 47;
            this.btnEOD14.Text = "ЭОД-14";
            ultraToolTipInfo1.ToolTipText = "Ответ на запрос о предоставлении выписки по операциям на счетах";
            this.ultraToolTipManager1.SetUltraToolTip(this.btnEOD14, ultraToolTipInfo1);
            // 
            // btnEOD01
            // 
            this.btnEOD01.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnEOD01.Location = new System.Drawing.Point(11, 470);
            this.btnEOD01.Name = "btnEOD01";
            this.btnEOD01.Size = new System.Drawing.Size(75, 23);
            this.btnEOD01.TabIndex = 2;
            this.btnEOD01.Text = "ЭОД-1";
            // 
            // comboTypeDocument
            // 
            this.comboTypeDocument.DisplayMember = "NAME";
            this.comboTypeDocument.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.comboTypeDocument.Location = new System.Drawing.Point(349, 8);
            this.comboTypeDocument.Margin = new System.Windows.Forms.Padding(1);
            this.comboTypeDocument.Name = "comboTypeDocument";
            this.comboTypeDocument.Size = new System.Drawing.Size(211, 21);
            this.comboTypeDocument.TabIndex = 42;
            // 
            // labelTypeDocument
            // 
            this.labelTypeDocument.Location = new System.Drawing.Point(258, 10);
            this.labelTypeDocument.Name = "labelTypeDocument";
            this.labelTypeDocument.Size = new System.Drawing.Size(92, 18);
            this.labelTypeDocument.TabIndex = 43;
            this.labelTypeDocument.Text = "Тип документа:";
            // 
            // numericRegNumber
            // 
            this.numericRegNumber.Location = new System.Drawing.Point(121, 8);
            this.numericRegNumber.MaskInput = "nnnnnnnnnnnn";
            this.numericRegNumber.MaxValue = 999999999999D;
            this.numericRegNumber.MinValue = 1;
            this.numericRegNumber.Name = "numericRegNumber";
            this.numericRegNumber.Nullable = true;
            this.numericRegNumber.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Double;
            this.numericRegNumber.PromptChar = ' ';
            this.numericRegNumber.Size = new System.Drawing.Size(129, 21);
            this.numericRegNumber.TabIndex = 44;
            // 
            // checkRegNumber
            // 
            this.checkRegNumber.Location = new System.Drawing.Point(14, 8);
            this.checkRegNumber.Name = "checkRegNumber";
            this.checkRegNumber.Size = new System.Drawing.Size(107, 20);
            this.checkRegNumber.TabIndex = 46;
            this.checkRegNumber.Text = "Рег. номер НД";
            // 
            // checkSendAll
            // 
            this.checkSendAll.Location = new System.Drawing.Point(222, 34);
            this.checkSendAll.Name = "checkSendAll";
            this.checkSendAll.Size = new System.Drawing.Size(269, 20);
            this.checkSendAll.TabIndex = 48;
            this.checkSendAll.Text = "Отправить всем (только для ЭОД-2, ЭОД-5)";
            // 
            // checkTop1000
            // 
            this.checkTop1000.Location = new System.Drawing.Point(14, 34);
            this.checkTop1000.Name = "checkTop1000";
            this.checkTop1000.Size = new System.Drawing.Size(183, 20);
            this.checkTop1000.TabIndex = 49;
            this.checkTop1000.Text = "Только первые 1000 записей";
            this.checkTop1000.ThreeState = true;
            // 
            // View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1164, 505);
            this.Controls.Add(this.checkTop1000);
            this.Controls.Add(this.checkSendAll);
            this.Controls.Add(this.btnEOD14);
            this.Controls.Add(this.checkRegNumber);
            this.Controls.Add(this.numericRegNumber);
            this.Controls.Add(this.comboTypeDocument);
            this.Controls.Add(this.labelTypeDocument);
            this.Controls.Add(this.btnEOD13);
            this.Controls.Add(this.btnEOD11);
            this.Controls.Add(this.btnEOD10);
            this.Controls.Add(this.btnEOD08);
            this.Controls.Add(this.btnEOD07);
            this.Controls.Add(this.btnEOD09);
            this.Controls.Add(this.btnEOD06);
            this.Controls.Add(this.ultraTabControl1);
            this.Controls.Add(this.btnEOD04);
            this.Controls.Add(this.btnEOD03);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.gridIncome);
            this.Controls.Add(this.btnEOD05);
            this.Controls.Add(this.btnEOD01);
            this.Controls.Add(this.btnEOD02);
            this.Name = "View";
            this.Text = "Эмулятор СЭОД";
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridResult)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridDebug)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridIncome)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl1)).EndInit();
            this.ultraTabControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboTypeDocument)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericRegNumber)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.Misc.UltraButton btnEOD02;
        private Infragistics.Win.Misc.UltraButton btnEOD05;
        private Infragistics.Win.UltraWinGrid.UltraGrid gridIncome;
        private Infragistics.Win.Misc.UltraButton btnRefresh;
        private Infragistics.Win.Misc.UltraButton btnEOD03;
        private Infragistics.Win.Misc.UltraButton btnEOD04;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinGrid.UltraGrid gridDebug;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinGrid.UltraGrid gridResult;
        private Infragistics.Win.Misc.UltraButton btnEOD08;
        private Infragistics.Win.Misc.UltraButton btnEOD07;
        private Infragistics.Win.Misc.UltraButton btnEOD09;
        private Infragistics.Win.Misc.UltraButton btnEOD06;
        private Infragistics.Win.Misc.UltraButton btnEOD10;
        private Infragistics.Win.UltraWinToolTip.UltraToolTipManager ultraToolTipManager1;
        private Infragistics.Win.Misc.UltraButton btnEOD11;
        private Infragistics.Win.Misc.UltraButton btnEOD01;
        private Infragistics.Win.Misc.UltraButton btnEOD13;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor comboTypeDocument;
        private Infragistics.Win.Misc.UltraLabel labelTypeDocument;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor numericRegNumber;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor checkRegNumber;
        private Infragistics.Win.Misc.UltraButton btnEOD14;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor checkSendAll;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor checkTop1000;
    }
}

