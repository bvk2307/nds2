﻿using System;
using System.Text;

namespace EmulatorSEOD
{
    internal static class Queries
    {
        public const string INCOME_SELECT = @"
            select 
                ni.*
            from nds2_income ni
            left join nds2_mrr_user.doc d
                on ni.object_reg_num  = d.doc_id
            where 1 = 1 {0} 
            order by ni.object_reg_num";
        public const string RESULT_SELECT = "select * from nds2_test_data where object_reg_num = :pId order by date_eod";

        public const string NDS2_SEND = @"
declare
  p_id number := SEQ_NDS2_SENT_ID.NEXTVAL();
begin  
  insert into I$CAM.nds2_test_data (id, info_type, info_type_name, date_eod, date_receipt, code_nsi_sono, reg_number, object_reg_num, xml_data, status)
  values
  (
    p_id
    ,'{3}'
    ,'{4}'
    ,sysdate
    ,sysdate
    ,'{0}' --sono_code
    ,{1}  -- ref_entity_id
    ,{5}  -- doc_id
    ,'{2}' --xml
    ,1
  );
end;";

        #region Response
        public const string EOD_STUB = "select * from doc where doc_id = :pId";

        public const string EOD2_RESPONSE = @"
select
    doc_id,
    external_doc_num,
    external_doc_date,
    tax_payer_send_date,
    tax_payer_delivery_date,
    tax_payer_delivery_method
from doc
where doc_id = :pId";
        public const string EOD3_RESPONSE = @"
select
    doc_id,
    incoming_num,
    incoming_date
from nds2_seod.seod_explain_reply
where doc_id = :pId and type_id = 1";

        public const string EOD4_RESPONSE = @"
select
    d.doc_id,
    k.knp_id,
    k.declaration_reg_num,
    k.completion_date,
    ka.knp_id, ka.doc_data,
    ka.doc_num,
    ka.sum_unpaid,
    ka.sum_overestimated,
    ka.sum_compensation
from doc d
left join NDS2_SEOD.SEOD_KNP k 
    on k.declaration_reg_num = d.ref_entity_id
left join NDS2_SEOD.SEOD_KNP_ACT ka 
    on ka.knp_id = k.knp_id 
    and k.ifns_code = d.sono_code
where d.doc_id = :pId";

        public const string EOD5_RESPONSE = @"
select
    doc_id,
    seod_accept_date
from doc
where doc_id = :pId";
        #endregion


        #region stuff

        public const string GetInnKppByDeclartionRegNum = "select decl.INN_CONTRACTOR, decl.KPP_EFFECTIVE FROM NDS2_MRR_USER.DECLARATION_HISTORY decl WHERE decl.REG_NUMBER = :pDeclartionRegNum";


        public const string GetDocData = "Select doc_id, external_doc_num, external_doc_date from NDS2_MRR_USER.DOC where doc_id = :doc_id";
        #endregion
    }
}