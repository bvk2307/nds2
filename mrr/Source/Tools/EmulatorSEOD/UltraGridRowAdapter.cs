﻿using System.Collections.Generic;
using System.Data;
using Infragistics.Win.UltraWinGrid;
using System.Collections;

namespace EmulatorSEOD
{
    /// <summary>
    /// Преобразует объекты типов UltraGridRow и SelectedRowsCollection в объект IEnumerable<DataRow>
    /// </summary>
    class UltraGridRowAdapter : IEnumerable<DataRow>
    {
        private readonly SelectedRowsCollection _selectedRowsCollection;
        private readonly UltraGridRow _row;
        IEnumerable<DataRow> _enumerableDataRows;

        private IEnumerable<DataRow> EnumerableDataRows
        {
            get
            {
                if (_enumerableDataRows == null)
                    _enumerableDataRows = CreateEnumerableDataRows();
                return _enumerableDataRows;
            }
        }

        public UltraGridRowAdapter(SelectedRowsCollection selectedRowsCollection)
        {
            _selectedRowsCollection = selectedRowsCollection;
        }

        public UltraGridRowAdapter(UltraGridRow row)
        {
            _row = row;
        }

        public IEnumerator<DataRow> GetEnumerator()
        {
            return EnumerableDataRows.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return EnumerableDataRows.GetEnumerator();
        }

        private IEnumerable<DataRow> CreateEnumerableDataRows()
        {
            if (_selectedRowsCollection != null)
            {
                for (int i = 0; i < _selectedRowsCollection.Count; i++)
                {
                    yield return ToDataRow(_selectedRowsCollection[i]);
                }
            }
            if (_row != null)
                yield return ToDataRow(_row);
        }

        private DataRow ToDataRow(UltraGridRow ultraGridRow)
        {
            return (ultraGridRow.ListObject as DataRowView).Row;
        }
    }
}
