﻿using EmulatorSEOD.Filters;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Infragistics.Win.UltraWinGrid;

namespace EmulatorSEOD
{
    public partial class View : Form
    {
        public event EventHandler OnSEOD01;
        public event EventHandler OnSEOD02;
        public event EventHandler OnSEOD03;
        public event EventHandler OnSEOD04;
        public event EventHandler OnSEOD05;
        public event EventHandler OnSEOD06;
        public event EventHandler OnSEOD07;
        public event EventHandler OnSEOD08;
        public event EventHandler OnSEOD09;
        public event EventHandler OnSEOD10;
        public event EventHandler OnSEOD11;
        public event EventHandler OnSEOD13;
        public event EventHandler OnSEOD14;

        public event EventHandler OnRefresh;
        public event EventHandler OnRowActivate;

        public Func<DataTable> GetDebugTable { get; set; }

        private List<TypeDocument> _typeDocuments = new List<TypeDocument>();

        private List<DataRow> _selectedRows = new List<DataRow>();

        public DataRow ActiveRow
        {
            get
            {
                var rowView = gridIncome.ActiveRow == null ? null : gridIncome.ActiveRow.ListObject as DataRowView;
                return rowView == null ? null : rowView.Row;
            }
        }

        public IEnumerable<DataRow> SelectedRows
        {
            get
            {
                return _selectedRows;
            }
        }

        public View()
        {
            InitializeComponent();

            gridIncome.KeyDown += GridIncome_KeyDown;
            gridIncome.AfterSelectChange += GridIncome_AfterSelectChange;
            gridIncome.AfterRowActivate += GridIncome_AfterRowActivate;
            gridIncome.AfterRowActivate += (sender, e) => OnEvent(OnRowActivate, sender, e);

            btnRefresh.Click += (sender, e) => OnEvent(OnRefresh, sender, e);
            btnEOD01.Click += (sender, e) => OnEvent(OnSEOD01, sender, e);
            btnEOD02.Click += (sender, e) => OnEvent(OnSEOD02, sender, e);
            btnEOD03.Click += (sender, e) => OnEvent(OnSEOD03, sender, e);
            btnEOD04.Click += (sender, e) => OnEvent(OnSEOD04, sender, e);
            btnEOD05.Click += (sender, e) => OnEvent(OnSEOD05, sender, e);
            btnEOD06.Click += (sender, e) => OnEvent(OnSEOD06, sender, e);
            btnEOD07.Click += (sender, e) => OnEvent(OnSEOD07, sender, e);
            btnEOD08.Click += (sender, e) => OnEvent(OnSEOD08, sender, e);
            btnEOD09.Click += (sender, e) => OnEvent(OnSEOD09, sender, e);
            btnEOD10.Click += (sender, e) => OnEvent(OnSEOD10, sender, e);
            btnEOD11.Click += (sender, e) => OnEvent(OnSEOD11, sender, e);
            btnEOD13.Click += (sender, e) => OnEvent(OnSEOD13, sender, e);
            btnEOD14.Click += (sender, e) => OnEvent(OnSEOD14, sender, e);

            new Presenter { View = this };
        }

        public void ShowTable(DataTable income, DataTable result)
        {
            var doc_id_column_name = "object_reg_num";
            long id = 0;
            if (ActiveRow != null)
            {
                id = (long)(decimal)ActiveRow[doc_id_column_name];
            }
            gridIncome.DataSource = income;
            gridIncome.Refresh();

            gridIncome.DisplayLayout.Bands[0].Columns["object_type_name"].Width = 180;

            gridDebug.DataSource = GetDebugTable == null ? null : GetDebugTable();

            if (id > 0)
            {
                foreach (var row in gridIncome.DisplayLayout.Rows)
                {
                    if ((long)(decimal)row.Cells[doc_id_column_name].Value == id)
                    {
                        gridIncome.ActiveRow = row;
                        break;
                    }
                }
            }
            else if (gridIncome.DisplayLayout.Rows.Count > 0)
            {
                gridIncome.ActiveRow = gridIncome.DisplayLayout.Rows[0];
            }
        }

        public void ShowResult(DataTable result)
        {
            gridResult.DataSource = result;
            gridResult.Refresh();
        }

        private static void OnEvent(EventHandler handler, object sender, EventArgs e)
        {
            if (handler != null)
            {
                handler(sender, e);
            }
        }

        public void SetDictionaryTypeDocuments(List<TypeDocument> typeDocuments)
        {
            _typeDocuments.Clear();
            _typeDocuments.AddRange(typeDocuments);
        }

        public void InitFilters()
        {
            comboTypeDocument.DataSource = _typeDocuments;
            comboTypeDocument.Refresh();

            if (comboTypeDocument.Items.Count > 0)
                comboTypeDocument.SelectedIndex = 0;
            else
                comboTypeDocument.SelectedIndex = -1;

            checkRegNumber.Checked = false;
            numericRegNumber.Value = null;
            numericRegNumber.Enabled = checkRegNumber.Checked;

            checkTop1000.Checked = true;

            checkRegNumber.CheckedChanged += CheckRegNumberCheckedChanged;

            numericRegNumber.ValueChanged += NumericRegNumberValueChanged;

            comboTypeDocument.ValueChanged += new EventHandler(ComboTypeDocumentValueChanged);
        }

        private void RaiseOnRefresh(object sender, EventArgs e)
        {
            if (OnRefresh != null)
                OnRefresh(sender, e);
        }

        private void CheckRegNumberCheckedChanged(object sender, EventArgs e)
        {
            numericRegNumber.Enabled = checkRegNumber.Checked;

            if (checkRegNumber.Checked)
            {
                numericRegNumber.Value = null;
                numericRegNumber.Select();
                numericRegNumber.SelectedText = null;
                numericRegNumber.SelectionStart = 0;
                numericRegNumber.SelectionLength = 0;
            }

            RaiseOnRefresh(sender, e);
        }

        private void NumericRegNumberValueChanged(object sender, EventArgs e)
        {
            RaiseOnRefresh(sender, e);
        }

        private void ComboTypeDocumentValueChanged(object sender, EventArgs e)
        {
            RaiseOnRefresh(sender, e);
        }

        public TypeDocument GetFilterTypeDocumentSelected()
        {
            TypeDocument typeDocumentSelected = null;
            if (comboTypeDocument.SelectedIndex > -1)
            {
                if (comboTypeDocument.SelectedIndex < _typeDocuments.Count)
                {
                    typeDocumentSelected = _typeDocuments[comboTypeDocument.SelectedIndex];
                }
            }
            return typeDocumentSelected;
        }

        public double? GetFilterRegNumber()
        {
            double? retValue = null;

            if (checkRegNumber.Checked && numericRegNumber.Value != null)
            {
                if (numericRegNumber.Value is double)
                    retValue = (double)numericRegNumber.Value;
            }

            return retValue;
        }

        public bool GetIsSendAll()
        {
            return checkSendAll.Checked;
        }

        private void gridIncome_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            e.Layout.Override.SelectTypeRow = SelectType.Extended;
            e.Layout.Override.CellClickAction = CellClickAction.EditAndSelectText;
        }

        private void GridIncome_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.A)
                gridIncome.Selected.Rows.AddRange(gridIncome.Rows.ToArray());
        }

        private void GridIncome_AfterRowActivate(object sender, EventArgs e)
        {
            if (gridIncome.Selected == null
                || gridIncome.Selected.Rows.Count == 0)
            {
                _selectedRows.Clear();
                _selectedRows.AddRange(new UltraGridRowAdapter(gridIncome.ActiveRow));
            }
        }

        private void GridIncome_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            if (e.Type == typeof(UltraGridRow))
            {
                if (gridIncome.Selected != null
                    && gridIncome.Selected.Rows.Count > 0)
                {
                    _selectedRows.Clear();
                    _selectedRows.AddRange(new UltraGridRowAdapter(gridIncome.Selected.Rows));
                }
            }
        }


        private void btnEOD13_Click(object sender, EventArgs e)
        {

        }

        private void btnEOD11_Click(object sender, EventArgs e)
        {

        }
    }
}