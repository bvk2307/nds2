﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using EmulatorSEOD.Dialogs;

namespace EmulatorSEOD
{
    public static class XmlBuilder
    {
        private static void RemoveEmptyAttributes(XElement element)
        {
            var empty =
                from attribute in element.Attributes()
                where string.IsNullOrEmpty(attribute.Value)
                select attribute;

            foreach (var item in empty)
            {
                item.Remove();
            }

            foreach (var item in element.Elements())
            {
                RemoveEmptyAttributes(item);
            }
        }

        public static string GetEOD01Xml(DialogEOD01.Model model)
        {
            var xml = XDocument.Parse("<?xml version='1.0' encoding='UTF-8'?><Файл xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:noNamespaceSchemaLocation='TAX3EXCH_CAM_NDS2_01_01.xsd' ТипИнф='CAM_NDS2_01' ВерсПрог='2.5.000' />");
            var xRoot = xml.Root;
            xRoot.SetAttributeValue("ИдФайл", model.FileId);

            var xDoc = new XElement("Документ");
            xDoc.SetAttributeValue("КодНО", model.SounCode);
            xDoc.SetAttributeValue("Период", model.TaxPeriod);
            xDoc.SetAttributeValue("ОтчетГод", model.FiscalYear);
            xDoc.SetAttributeValue("НомКорр", model.CorrectionNum);
            xDoc.SetAttributeValue("РегНомДек", model.RegNum);
            xDoc.SetAttributeValue("ФидДек", model.FID);
            xDoc.SetAttributeValue("ДатаПредНД", model.SubmissionDate);
            xRoot.Add(xDoc);

            var xTaxpayerInfo = new XElement("СвНП");
            xTaxpayerInfo.SetAttributeValue("ФидНП", model.TaxpayerFID);
            xDoc.Add(xTaxpayerInfo);

            if (model.IsUL)
            {
                var xUL = new XElement("НПЮЛ");
                xUL.SetAttributeValue("ИННЮЛ", model.INN);
                xUL.SetAttributeValue("КПП", model.KPP);
                xTaxpayerInfo.Add(xUL);
            }
            else
            {
                var xIP = new XElement("НПФЛ");
                xIP.SetAttributeValue("ИННФЛ", model.INN);
                xTaxpayerInfo.Add(xIP);
            }

            RemoveEmptyAttributes(xRoot);
            return xml.Declaration.ToString() + xml.ToString(SaveOptions.DisableFormatting);
        }

        public static string GetEOD02Xml(DataRow row, DialogEodDoc.Model model)
        {
            var xml = XDocument.Parse("<?xml version='1.0' encoding='UTF-8'?><Файл xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:noNamespaceSchemaLocation='TAX3EXCH_CAM_NDS2_02_01.xsd' ТипИнф='CAM_NDS2_02' ВерсПрог='2.5.000' />");
            var xRoot = xml.Root;
            var xDoc = new XElement("Документ");
            xDoc.SetAttributeValue("КодНО", row["sono_code"].ToString());
            xDoc.SetAttributeValue("РегНомДек", row["declartion_reg_num"].ToString());
            xRoot.Add(xDoc);

            var xReq = new XElement("ТребПоясн");
            xReq.SetAttributeValue("ДатаВруч", model.RecieveDate.AsDateString());
            xReq.SetAttributeValue("УчНомТреб", row["object_reg_num"].ToString());
            xReq.SetAttributeValue("СпВруч", model.RecieveMethod.ToString());
            xReq.SetAttributeValue("ДатаДок", model.DateDoc.AsDateString());
            xReq.SetAttributeValue("ДатаОтправ", model.SendDate.AsDateString());
            xReq.SetAttributeValue("НомДок", model.NumDoc.ToString());
            xDoc.Add(xReq);

            RemoveEmptyAttributes(xRoot);
            return xml.Declaration.ToString() + xml.ToString(SaveOptions.DisableFormatting);
        }

        public static string GetEOD03Xml(DataRow row, List<DialogOD03.Model> models)
        {
            var xml = XDocument.Parse("<?xml version='1.0' encoding='UTF-8'?><Файл xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:noNamespaceSchemaLocation='TAX3EXCH_CAM_NDS2_03_01.xsd' ТипИнф='CAM_NDS2_03' ВерсПрог='2.5.000' />");
            var xRoot = xml.Root;
            var xDoc = new XElement("Документ");
            xDoc.SetAttributeValue("КодНО", row["sono_code"].ToString());
            xDoc.SetAttributeValue("РегНомДек", row["declartion_reg_num"].ToString());
            xRoot.Add(xDoc);

            foreach (var item in models)
            {
                var xReq = new XElement("ТребПоясн");
                xReq.SetAttributeValue("УчНомТреб", item.DocId.ToString());
                xReq.SetAttributeValue("ДатаОтв", item.ReplyDate.AsDateString());
                xReq.SetAttributeValue("НомОтв", item.ReplyNumber);
                xReq.SetAttributeValue("ИмяФайлОтв", item.ReplyFileName);

                xDoc.Add(xReq);
            }
            RemoveEmptyAttributes(xRoot);
            return xml.Declaration.ToString() + xml.ToString(SaveOptions.DisableFormatting);
        }

        public static string GetEOD04Xml(DialogEOD04.Model model)
        {
            var xml = XDocument.Parse("<?xml version='1.0' encoding='UTF-8'?><Файл xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:noNamespaceSchemaLocation='TAX3EXCH_CAM_NDS2_04_01.xsd' ТипИнф='CAM_NDS2_04' ВерсПрог='2.5.000' />");
            var xRoot = xml.Root;
            var xDoc = new XElement("Документ");
            xDoc.SetAttributeValue("КодНО", model.Declaration.SonoCode);
            xDoc.SetAttributeValue("РегНомДек", model.Declaration.RegNumber);
            xRoot.Add(xDoc);

            var xKNP = new XElement("КНП");
            xDoc.Add(xKNP);
            if (model.Suspend != null)
            {
                var xSuspend = new XElement("РешПриост");
                xSuspend.SetAttributeValue("ДатаДок", model.Suspend.DateDoc.AsDateString());
                xSuspend.SetAttributeValue("НомДок", model.Suspend.NumDoc);
                xKNP.Add(xSuspend);
            }
            if (model.Close != null)
            {
                var xClose = new XElement("СвЗавКНП");
                xClose.SetAttributeValue("ДатаЗавКНП", model.Close.CloseDate.AsDateString());
                xKNP.Add(xClose);
            }
            if (model.Act != null)
            {
                var xAct = new XElement("АктКНП");
                xAct.SetAttributeValue("НомДок", model.Act.NumDoc);
                xAct.SetAttributeValue("ДатаДок", model.Act.DateDoc.AsDateString());
                xAct.SetAttributeValue("СумНеупл", model.Act.UnpaidTax);
                xAct.SetAttributeValue("СумЗавВозм", model.Act.ReturnTax);
                xAct.SetAttributeValue("СумПени", model.Act.Penalty);
                xKNP.Add(xAct);
            }
            if (model.Prosecute != null)
            {
                var xProsecute = new XElement("РешПривлОтв");
                xProsecute.SetAttributeValue("ДатаДок", model.Prosecute.DateDoc.AsDateString());
                xProsecute.SetAttributeValue("НомДок", model.Prosecute.NumDoc);
                xProsecute.SetAttributeValue("СумНеупл", model.Prosecute.UnpaidAmount);
                xProsecute.SetAttributeValue("СумПени", model.Prosecute.PenaltyAmount);
                xProsecute.SetAttributeValue("СумШтраф", model.Prosecute.FineAmount);
                xKNP.Add(xProsecute);
            }
            if (model.ProsecuteReject != null)
            {
                var xProsecuteReject = new XElement("РешОтказПривл");
                xProsecuteReject.SetAttributeValue("ДатаДок", model.ProsecuteReject.DateDoc.AsDateString());
                xProsecuteReject.SetAttributeValue("НомДок", model.ProsecuteReject.NumDoc);
                xProsecuteReject.SetAttributeValue("СумНеупл", model.ProsecuteReject.UnpaidAmount);
                xProsecuteReject.SetAttributeValue("СумПени", model.ProsecuteReject.PenaltyAmount);
                xKNP.Add(xProsecuteReject);
            }
            if (model.Return != null)
            {
                var xDecision = new XElement("РешВозмНДС");
                xDecision.SetAttributeValue("ДатаДок", model.Return.DateDoc.AsDateString());
                xDecision.SetAttributeValue("НомДок", model.Return.NumDoc);
                xDecision.SetAttributeValue("СумНДСВозм", model.Return.Amount);
                xKNP.Add(xDecision);
            }
            if (model.ReturnReject != null)
            {
                var xDecision = new XElement("РешОтказВозмНДС");
                xDecision.SetAttributeValue("ДатаДок", model.ReturnReject.DateDoc.AsDateString());
                xDecision.SetAttributeValue("НомДок", model.ReturnReject.NumDoc);
                xDecision.SetAttributeValue("СумОтказВозм", model.ReturnReject.Amount);
                xKNP.Add(xDecision);
            }
            if (model.ReturnDeclaration != null)
            {
                var xDecision = new XElement("РешВозмНДСЗаявит");
                xDecision.SetAttributeValue("ДатаДок", model.ReturnDeclaration.DateDoc.AsDateString());
                xDecision.SetAttributeValue("НомДок", model.ReturnDeclaration.NumDoc);
                xDecision.SetAttributeValue("СумНДСВозм", model.ReturnDeclaration.Amount);
                xKNP.Add(xDecision);
            }
            if (model.ReturnDeclarationReject != null)
            {
                var xDecision = new XElement("РешОтказВозмНДСЗаявит");
                xDecision.SetAttributeValue("ДатаДок", model.ReturnDeclarationReject.DateDoc.AsDateString());
                xDecision.SetAttributeValue("НомДок", model.ReturnDeclarationReject.NumDoc);
                xDecision.SetAttributeValue("СумОтказВозм", model.ReturnDeclarationReject.Amount);
                xKNP.Add(xDecision);
            }
            if (model.ReturnDeclarationCancel != null)
            {
                var xDecision = new XElement("РешОтменВозмНДСЗаявит");
                xDecision.SetAttributeValue("ДатаДок", model.ReturnDeclarationCancel.DateDoc.AsDateString());
                xDecision.SetAttributeValue("НомДок", model.ReturnDeclarationCancel.NumDoc);
                xDecision.SetAttributeValue("СумНДСВозмОтмен", model.ReturnDeclarationCancel.Amount);
                xKNP.Add(xDecision);
            }
            if (model.ReturnDeclarationCancelCorrection != null)
            {
                var xDecision = new XElement("РешОтменВозмНДСУточн");
                xDecision.SetAttributeValue("ДатаДок", model.ReturnDeclarationCancelCorrection.DateDoc.AsDateString());
                xDecision.SetAttributeValue("НомДок", model.ReturnDeclarationCancelCorrection.NumDoc);
                xDecision.SetAttributeValue("СумНДСВозмОтмен", model.ReturnDeclarationCancelCorrection.Amount);
                xKNP.Add(xDecision);
            }
            RemoveEmptyAttributes(xRoot);
            return xml.Declaration.ToString() + xml.ToString(SaveOptions.DisableFormatting);
        }

        public static string GetEOD05Xml(DataRow row)
        {
            var xml = XDocument.Parse("<?xml version='1.0' encoding='UTF-8'?><Файл xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:noNamespaceSchemaLocation='TAX3EXCH_CAM_NDS2_05_01.xsd' ТипИнф='CAM_NDS2_05' ВерсПрог='2.5.000' />");
            var xRoot = xml.Root;
            var xDoc = new XElement("Документ");
            xDoc.SetAttributeValue("КодНО", row["sono_code"].ToString());
            xDoc.SetAttributeValue("РегНомДек", row["declartion_reg_num"].ToString());
            xDoc.SetAttributeValue("ДатаПолучДан", row["create_date"].AsDateString());
            xDoc.Add(new XElement("Требование", new XElement("АвтоТреб", new XAttribute("УчНомТреб", row["object_reg_num"].ToString()))));
            xRoot.Add(xDoc);

            RemoveEmptyAttributes(xRoot);
            return xml.Declaration.ToString() + xml.ToString(SaveOptions.DisableFormatting);
        }

        public static string GetEOD06Xml(DataRow row, DialogEodDoc.Model model)
        {
            var xml = XDocument.Parse("<?xml version='1.0' encoding='UTF-8'?><Файл xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:noNamespaceSchemaLocation='TAX3EXCH_CAM_NDS2_06_01.xsd' ТипИнф='CAM_NDS2_06' ВерсПрог='2.5.000' />");
            var xRoot = xml.Root;
            var xDoc = new XElement("Документ");
            xDoc.SetAttributeValue("КодНО", row["sono_code"].ToString());
            xDoc.SetAttributeValue("РегНомДек", row["declartion_reg_num"].ToString());
            xDoc.SetAttributeValue("УчНомИстреб", row["object_reg_num"].ToString());
            xRoot.Add(xDoc);

            var xReclaim = new XElement("ИсТреб");
            xReclaim.SetAttributeValue("НомДок", model.NumDoc);
            xReclaim.SetAttributeValue("ДатаДок", model.DateDoc.AsDateString());
            xReclaim.SetAttributeValue("ДатаОтправ", model.SendDate.AsDateString());
            xReclaim.SetAttributeValue("ДатаВруч", model.RecieveDate.AsDateString());
            xReclaim.SetAttributeValue("СпВруч", model.RecieveMethod);
            xDoc.Add(xReclaim);

            RemoveEmptyAttributes(xRoot);
            return xml.Declaration.ToString() + xml.ToString(SaveOptions.DisableFormatting);
        }

        public static string GetEOD07Xml(DataRow row, DialogEOD07.Model model)
        {
            var xml = XDocument.Parse("<?xml version='1.0' encoding='UTF-8'?><Файл xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:noNamespaceSchemaLocation='TAX3EXCH_CAM_NDS2_07_01.xsd' ТипИнф='CAM_NDS2_07' ВерсПрог='2.5.000' />");
            var xRoot = xml.Root;
            var xDoc = new XElement("Документ");
            xDoc.SetAttributeValue("КодНО", row["sono_code"].ToString());
            xDoc.SetAttributeValue("РегНомДек", row["declartion_reg_num"].ToString());
            xDoc.SetAttributeValue("УчНомИстреб", row["object_reg_num"].ToString());
            xRoot.Add(xDoc);

            var xDecision = new XElement("РешПродлСрок");
            xDecision.SetAttributeValue("НомДок", model.NumDoc);
            xDecision.SetAttributeValue("ДатаДок", model.DateDoc.AsDateString());
            xDecision.SetAttributeValue("ДатаПродлСрок", model.ProlongDate.AsDateString());
            xDoc.Add(xDecision);

            RemoveEmptyAttributes(xRoot);
            return xml.Declaration.ToString() + xml.ToString(SaveOptions.DisableFormatting);
        }

        public static string GetEOD08Xml(DataRow row, DialogEOD08.Model model)
        {
            var xml = XDocument.Parse("<?xml version='1.0' encoding='UTF-8'?><Файл xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:noNamespaceSchemaLocation='TAX3EXCH_CAM_NDS2_08_01.xsd' ТипИнф='CAM_NDS2_08' ВерсПрог='2.5.000' />");
            var xRoot = xml.Root;
            var xDoc = new XElement("Документ");
            xDoc.SetAttributeValue("КодНО", row["sono_code"].ToString());
            xDoc.SetAttributeValue("РегНомДек", row["declartion_reg_num"].ToString());
            xDoc.SetAttributeValue("УчНомИстреб", row["object_reg_num"].ToString());
            xRoot.Add(xDoc);

            var xDecision = new XElement("ОтвТреб93");
            xDecision.SetAttributeValue("ДатаОтв", model.ReplyDate.AsDateString());
            xDoc.Add(xDecision);

            RemoveEmptyAttributes(xRoot);
            return xml.Declaration.ToString() + xml.ToString(SaveOptions.DisableFormatting);
        }

        public static string GetEOD09Xml(DataRow row, DialogEOD09.Model model)
        {
            var xml = XDocument.Parse("<?xml version='1.0' encoding='UTF-8'?><Файл xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:noNamespaceSchemaLocation='TAX3EXCH_CAM_NDS2_09_01.xsd' ТипИнф='CAM_NDS2_09' ВерсПрог='2.5.000' />");
            var xRoot = xml.Root;
            var xDoc = new XElement("Документ");
            xDoc.SetAttributeValue("КодНО", row["sono_code"].ToString());
            xDoc.SetAttributeValue("РегНомДек", row["declartion_reg_num"].ToString());
            xDoc.SetAttributeValue("УчНомИстреб", row["object_reg_num"].ToString());
            xRoot.Add(xDoc);

            var xDecision = new XElement("Поруч");
            xDecision.SetAttributeValue("НомПоруч", model.NumDoc);
            xDecision.SetAttributeValue("ДатаПоруч", model.RegisterDate.AsDateString());
            xDecision.SetAttributeValue("ДатаНапрПоруч", model.ExecutorSendDate.AsDateString());
            xDecision.SetAttributeValue("ДатаПолучПоруч", model.ExecutorRecieveDate.AsDateString());
            xDecision.SetAttributeValue("ПричОтказ", model.RejectReason);
            xDoc.Add(xDecision);

            RemoveEmptyAttributes(xRoot);
            return xml.Declaration.ToString() + xml.ToString(SaveOptions.DisableFormatting);
        }

        public static string GetEOD10Xml(DataRow row, DialogEOD10.Model model)
        {
            var xml = XDocument.Parse("<?xml version='1.0' encoding='UTF-8'?><Файл xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:noNamespaceSchemaLocation='TAX3EXCH_CAM_NDS2_10_01.xsd' ТипИнф='CAM_NDS2_10' ВерсПрог='2.5.000' />");
            var xRoot = xml.Root;
            var xDoc = new XElement("Документ");
            xDoc.SetAttributeValue("КодНО", row["sono_code"].ToString());
            xDoc.SetAttributeValue("РегНомДек", row["declartion_reg_num"].ToString());
            xDoc.SetAttributeValue("УчНомИстреб", row["object_reg_num"].ToString());
            xRoot.Add(xDoc);

            var xDecision = new XElement("ОтвНП93.1");
            xDecision.SetAttributeValue("ВхДатаОтвИсп", model.ExecutorRecieveDate.AsDateString());
            xDecision.SetAttributeValue("ИсхДатаОтвИсп", model.InitiatorSendDate.AsDateString());
            xDecision.SetAttributeValue("ДатаОтв", model.TaxpayerReplyDate.AsDateString());
            xDoc.Add(xDecision);

            RemoveEmptyAttributes(xRoot);
            return xml.Declaration.ToString() + xml.ToString(SaveOptions.DisableFormatting);
        }

        public static string GetEOD11Xml(DialogEOD11.Model model)
        {
            var xml = XDocument.Parse("<?xml version='1.0' encoding='UTF-8'?><Файл xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:noNamespaceSchemaLocation='TAX3EXCH_CAM_NDS2_11_01.xsd' ТипИнф='CAM_NDS2_11' ВерсПрог='2.5.000' />");
            var xRoot = xml.Root;
            xRoot.SetAttributeValue("ИдФайл", model.FileId);

            var xDoc = new XElement("Документ");
            xDoc.SetAttributeValue("КодНО", model.SounCode);
            xDoc.SetAttributeValue("Период", model.TaxPeriod);
            xDoc.SetAttributeValue("ОтчетГод", model.FiscalYear);
            xDoc.SetAttributeValue("РегНомЖурнал", model.RegNum);
            xDoc.SetAttributeValue("ФидЖурнал", model.FID);
            xRoot.Add(xDoc);

            var xTaxpayerInfo = new XElement("СвНП");
            xTaxpayerInfo.SetAttributeValue("ФидНП", model.TaxpayerFID);
            xDoc.Add(xTaxpayerInfo);

            if (model.IsUL)
            {
                var xUL = new XElement("НПЮЛ");
                xUL.SetAttributeValue("ИННЮЛ", model.INN);
                xUL.SetAttributeValue("КПП", model.KPP);
                xTaxpayerInfo.Add(xUL);
            }
            else
            {
                var xIP = new XElement("НПФЛ");
                xIP.SetAttributeValue("ИННФЛ", model.INN);
                xTaxpayerInfo.Add(xIP);
            }

            RemoveEmptyAttributes(xRoot);
            return xml.Declaration.ToString() + xml.ToString(SaveOptions.DisableFormatting);
        }

        private const string XmlTemplate =
            @"<?xml version='1.0' encoding='windows-1251'?>
<Файл xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:noNamespaceSchemaLocation='{0}' ТипИнф='{1}' ВерсПрог='2.5.000' />";

        public static string GetEOD13Xml(DialogEOD13.Model model)
        {
            var xml = XDocument.Parse(string.Format(XmlTemplate, "TAX3EXCH_CAM_NDS2_13_01.xsd", "CAM_NDS2_13"));
            var xRoot = xml.Root;

            if (xRoot != null)
            {
                var xDoc = new XElement("Документ");
                xDoc.SetAttributeValue("КодНО", model.SounCode);
                
                var xReq = new XElement("ЗАПНОВЫПИС");
                xReq.SetAttributeValue("ИдДок", model.RequestId);
                xReq.SetAttributeValue("НомЗапр", model.RegNumber);
                xReq.SetAttributeValue("ДатаЗапр", model.RequestDate.AsDateString());
                xReq.SetAttributeValue("СостЗапр", model.RequestState);

                xDoc.Add(xReq);
                xRoot.Add(xDoc);

                RemoveEmptyAttributes(xRoot);
            }

            return xml.Declaration + xml.ToString(SaveOptions.DisableFormatting);
        }

        public static string GetEOD14Xml(DialogEOD14.Model model)
        {
            var xml = XDocument.Parse(string.Format(XmlTemplate, "TAX3EXCH_CAM_NDS2_14_01.xsd", "CAM_NDS2_14"));
            var xRoot = xml.Root;

            if (xRoot != null)
            {
                xRoot.SetAttributeValue("ИдФайл", model.FileId);

                var xDoc = new XElement("Документ");
                xDoc.SetAttributeValue("КодНО", model.SounCode);
                xDoc.SetAttributeValue("Период", model.TaxPeriod);
                xDoc.SetAttributeValue("ОтчетГод", model.FiscalYear);
                xDoc.SetAttributeValue("НомКорр", model.CorrectionNum);
                xDoc.SetAttributeValue("РегНомДек", model.RegNum);
                xDoc.SetAttributeValue("ПричинаАннул", model.CancelationCause);
                xDoc.SetAttributeValue("ДатаАннул", model.CancelationDate.AsDateString());
                xDoc.SetAttributeValue("Признак", model.IsOpenPreviousKNPNeeded ? 1 : 0);

                var xNP = new XElement("СвНП");

                if (model.IsNPFL)
                {
                    var xNPDetails = new XElement("НПФЛ");
                    xNPDetails.SetAttributeValue("ИННФЛ", model.INN);
                    xNP.Add(xNPDetails);
                }
                else
                {
                    var xNPDetails = new XElement("НПЮЛ");
                    xNPDetails.SetAttributeValue("ИННЮЛ", model.INN);
                    xNPDetails.SetAttributeValue("КПП", model.KPP);
                    xNP.Add(xNPDetails);
                }

                xDoc.Add(xNP);
                xRoot.Add(xDoc);

                RemoveEmptyAttributes(xRoot);
            }

            return xml.Declaration + xml.ToString(SaveOptions.DisableFormatting);
        }
    }
}