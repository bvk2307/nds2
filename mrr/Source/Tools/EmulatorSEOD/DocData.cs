﻿using System;

namespace EmulatorSEOD
{
    /// <summary>
    /// Содержит информацию об АТ/АИ
    /// </summary>
    public class DocData
    {
        /// <summary>
        /// Номер АТ/АИ в СЭОД
        /// </summary>
        public long? RegNumber { get; set; }

        /// <summary>
        /// Номер АТ/АИ в СЭОД
        /// </summary>
        public long? ExternalDocNumber { get; set; }

        /// <summary>
        /// Номер регистрации АТ/АИ в СЭОД 
        /// </summary>
        public DateTime? ExternalDocCreateDate { get; set; }
    }
}
