﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EmulatorSEOD.Filters
{
    public class TypeDocument
    {
        public long Id { get; set; }
        public long SubTypeId { get; set; }
        public string Name { get; set; }
    }
}
