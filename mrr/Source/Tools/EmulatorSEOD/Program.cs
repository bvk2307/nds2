﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EmulatorSEOD
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            var cultureInfo = new System.Globalization.CultureInfo("ru-RU");
            cultureInfo.NumberFormat.NumberDecimalSeparator = ".";
            Application.CurrentCulture = cultureInfo;            

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new View());
        }
    }
}
