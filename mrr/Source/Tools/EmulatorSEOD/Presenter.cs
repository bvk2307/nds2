﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using EmulatorSEOD.Dialogs;
using EmulatorSEOD.Filters;

namespace EmulatorSEOD
{
    public sealed class Presenter : IDisposable
    {
        private readonly DbOperations _service;
        private View _view;
        private List<TypeDocument> _typeDocuments = new List<TypeDocument>();

        public View View
        {
            get { return _view; }
            set
            {
                if (_view != null)
                {
                    DetachView(_view);
                }
                _view = value;
                AttachView(_view);
                RefreshDoc();
                SetDictionaryFilter();
            }
        }

        public Presenter()
        {
            _service = new DbOperations();
        }

        private void AttachView(View view)
        {
            view.OnRowActivate += View_OnRowActivate;
            view.OnRefresh += View_OnRefresh;
            view.OnSEOD01 += View_OnSEOD01;
            view.OnSEOD02 += View_OnSEOD02;
            view.OnSEOD03 += View_OnSEOD03;
            view.OnSEOD04 += View_OnSEOD04;
            view.OnSEOD05 += View_OnSEOD05;
            view.OnSEOD06 += View_OnSEOD06;
            view.OnSEOD07 += View_OnSEOD07;
            view.OnSEOD08 += View_OnSEOD08;
            view.OnSEOD09 += View_OnSEOD09;
            view.OnSEOD10 += View_OnSEOD10;
            view.OnSEOD11 += View_OnSEOD11;
            view.OnSEOD13 += View_OnSEOD13;
            view.OnSEOD14 += View_OnSEOD14;
        }

        private void DetachView(View view)
        {
            view.OnRowActivate -= View_OnRowActivate;
            view.OnRefresh -= View_OnRefresh;
            view.OnSEOD01 -= View_OnSEOD01;
            view.OnSEOD02 -= View_OnSEOD02;
            view.OnSEOD03 -= View_OnSEOD03;
            view.OnSEOD04 -= View_OnSEOD04;
            view.OnSEOD05 -= View_OnSEOD05;
            view.OnSEOD06 -= View_OnSEOD06;
            view.OnSEOD07 -= View_OnSEOD07;
            view.OnSEOD08 -= View_OnSEOD08;
            view.OnSEOD09 -= View_OnSEOD09;
            view.OnSEOD10 -= View_OnSEOD10;
            view.OnSEOD11 -= View_OnSEOD11;
            view.OnSEOD13 -= View_OnSEOD13;
        }

        private void View_OnRefresh(object sender, EventArgs e)
        {
            RefreshDoc();
        }

        private void View_OnRowActivate(object sender, EventArgs e)
        {
            if (_view != null)
            {
                var result = _service.GetResult((long)(decimal)View.ActiveRow["object_reg_num"]);
                Action<View> showTable = v => v.ShowResult(result);
                if (_view.InvokeRequired)
                {
                    _view.Invoke(showTable, _view);
                }
                else
                {
                    showTable(_view);
                }
            }
        }

        private void View_OnSEOD01(object sender, EventArgs e)
        {
            using (var dialog = new DialogEOD01())
            {
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    View.GetDebugTable = _service.ExecuteSEOD01(dialog.Data);
                    MessageBox.Show("СЭОД-01 отправлен");
                    RefreshDoc();
                }
            }
        }

        private void View_OnSEOD02(object sender, EventArgs e)
        {
            if (View.SelectedRows.Count() > 0)
            {
                bool multiSelection = View.SelectedRows.Count() > 1;
                using (var dialog = CreateDialogEodDoc<DialogEOD02>(multiSelection))
                {
                    if (dialog.ShowDialog() == DialogResult.OK)
                    {
                        bool isSendAll = _view.GetIsSendAll();
                        if (isSendAll)
                        {
                            var dataTable = _service.GetDocAll(GenerateSqlQueryIncomeForAll());
                            var records = new List<DataRow>();
                            foreach (DataRow item in dataTable.Rows)
                            {
                                records.Add(item);
                            }
                            View.GetDebugTable = _service.ExecuteSEOD02(records, dialog.Data);
                            MessageBox.Show(String.Format("СЭОД-2 отправлен всем записям ({0} шт.)", records.Count()));
                            RefreshDoc();
                        }
                        else
                        {
                            View.GetDebugTable = _service.ExecuteSEOD02(View.SelectedRows, dialog.Data);
                            MessageBox.Show(String.Format("СЭОД-2 отправлен({0} шт.)", View.SelectedRows.Count()));
                            RefreshDoc();
                        }
                    }
                }
            }
            else
                MessageBox.Show(String.Format("Необходимо выбрать хотя бы одну запись"));
        }

        /// <summary>
        /// Создает диалоги АТ/АИ для ЭОД2/ЭОД6
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="multiSelection"></param>
        /// <returns></returns>
        private T CreateDialogEodDoc<T>(bool multiSelection)
            where T: DialogEodDoc, new()
        {
            var dialog = new T();
            dialog.Data.MultiSelectionMode = multiSelection;

            if (!multiSelection)
            {
                var docData = new DocDataService()
                                .GetDocData(View.ActiveRow.GetObjectId(), View.ActiveRow.GetObjectTypeId());
                dialog.Data.Update(docData);
            }

            dialog.InitDialog();
            return dialog;
        }

        private void View_OnSEOD03(object sender, EventArgs e)
        {
            string inn, kpp;


            _service.GetInnKppByDeclartionRegNum((long)(decimal)View.ActiveRow["DECLARTION_REG_NUM"], out inn, out kpp);

            using (var dialog = new DialogOD03((long)(decimal)View.ActiveRow["object_reg_num"], inn, kpp))
            {
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    View.GetDebugTable = _service.ExecuteSEOD03(View.ActiveRow, dialog.Data.ToList());
                    MessageBox.Show("СЭОД-3 отправлен");
                    RefreshDoc();
                }
            }
        }

        private void View_OnSEOD04(object sender, EventArgs e)
        {
            var decl_reg_number = View.ActiveRow == null ? 
                                    String.Empty : View.ActiveRow["declartion_reg_num"].ToStringSafe();
            var sono_code = View.ActiveRow == null ? 
                                    String.Empty : View.ActiveRow["sono_code"].ToStringSafe();

            using (var dialog = new DialogEOD04(decl_reg_number, sono_code))
            {
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    View.GetDebugTable = _service.ExecuteSEOD04(View.ActiveRow, dialog.Data);
                    MessageBox.Show("СЭОД-4 отправлен");
                    RefreshDoc();
                }
            }
        }

        private void View_OnSEOD05(object sender, EventArgs e)
        {
            bool isSendAll = _view.GetIsSendAll();
            if (isSendAll)
            {
                var dataTable = _service.GetDocAll(GenerateSqlQueryIncomeForAll());
                var records = new List<DataRow>();
                foreach (DataRow item in dataTable.Rows)
                {
                    records.Add(item);
                }
                View.GetDebugTable = _service.ExecuteSEOD05(records);
                MessageBox.Show(String.Format("СЭОД-5 отправлен всем записям ({0} шт.)", records.Count()));
            }
            else
            {
                View.GetDebugTable = _service.ExecuteSEOD05(View.SelectedRows);
                MessageBox.Show(String.Format("СЭОД-5 отправлен ({0} шт.)", View.SelectedRows.Count()));
            }

            RefreshDoc();
        }

        private void View_OnSEOD06(object sender, EventArgs e)
        {
            bool multiSelection = View.SelectedRows.Count() > 1;
            using (var dialog = CreateDialogEodDoc<DialogEOD06>(multiSelection))
            {
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    View.GetDebugTable = _service.ExecuteSEOD06(View.SelectedRows, dialog.Data);
                    MessageBox.Show(String.Format("СЭОД-6 отправлен({0} шт.)", View.SelectedRows.Count()));
                    RefreshDoc();
                }
            }
        }

        private void View_OnSEOD07(object sender, EventArgs e)
        {
            using (var dialog = new DialogEOD07(View.ActiveRow))
            {
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    View.GetDebugTable = _service.ExecuteSEOD07(View.ActiveRow, dialog.Data);
                    MessageBox.Show("СЭОД-7 отправлен");
                    RefreshDoc();
                }
            }
        }

        private void View_OnSEOD08(object sender, EventArgs e)
        {
            using (var dialog = new DialogEOD08(View.ActiveRow))
            {
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    View.GetDebugTable = _service.ExecuteSEOD08(View.ActiveRow, dialog.Data);
                    MessageBox.Show("СЭОД-8 отправлен");
                    RefreshDoc();
                }
            }
        }

        private void View_OnSEOD09(object sender, EventArgs e)
        {
            using (var dialog = new DialogEOD09(View.ActiveRow))
            {
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    View.GetDebugTable = _service.ExecuteSEOD09(View.ActiveRow, dialog.Data);
                    MessageBox.Show("СЭОД-9 отправлен");
                    RefreshDoc();
                }
            }
        }

        private void View_OnSEOD10(object sender, EventArgs e)
        {
            using (var dialog = new DialogEOD10(View.ActiveRow))
            {
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    View.GetDebugTable = _service.ExecuteSEOD10(View.ActiveRow, dialog.Data);
                    MessageBox.Show("СЭОД-10 отправлен");
                    RefreshDoc();
                }
            }
        }

        private void View_OnSEOD11(object sender, EventArgs e)
        {
            using (var dialog = new DialogEOD11())
            {
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    View.GetDebugTable = _service.ExecuteSEOD11(dialog.Data);
                    MessageBox.Show("СЭОД-11 отправлен");
                    RefreshDoc();
                }
            }
        }

        private void View_OnSEOD13(object sender, EventArgs e)
        {
            using (var dialog = new DialogEOD13())
            {
                if (dialog.ShowDialog(View.ActiveRow) == DialogResult.OK)
                {
                    View.GetDebugTable = _service.ExecuteSEOD13(dialog.Data);
                    MessageBox.Show("СЭОД-13 отправлен");
                    RefreshDoc();
                }
            }
        }

        private void View_OnSEOD14(object sender, EventArgs e)
        {
            using (var dialog = new DialogEOD14())
            {
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    View.GetDebugTable = _service.ExecuteSEOD14(dialog.Data);
                    MessageBox.Show("СЭОД-14 отправлен");
                    RefreshDoc();
                }
            }
        }

        private void RefreshDoc()
        {
            if (_view != null)
            {
                var income = _service.GetDoc(GenerateSqlQueryIncome());
                var result = View.ActiveRow == null ? null : _service.GetResult((long)(decimal)View.ActiveRow["declartion_reg_num"]);
                Action<View> showTable = v => v.ShowTable(income, result);
                if (_view.InvokeRequired)
                {
                    _view.Invoke(showTable, _view);
                }
                else
                {
                    showTable(_view);
                }
            }
        }

        private void InitDictionaryTypeDocuments()
        {
            _typeDocuments.Clear();
            _typeDocuments.Add(new TypeDocument() { Id = 0, Name = "Все" });
            _typeDocuments.Add(new TypeDocument() { Id = 1, Name = "Автотребование по СФ или КС" });
            _typeDocuments.Add(new TypeDocument() { Id = 1, Name = "Автотребование по СФ", SubTypeId = 1 });
            _typeDocuments.Add(new TypeDocument() { Id = 1, Name = "Автотребование по КС", SubTypeId = 5 });
            _typeDocuments.Add(new TypeDocument() { Id = 2, Name = "Автоистребование по ст. 93" });
            _typeDocuments.Add(new TypeDocument() { Id = 4, Name = "Информация о наличии расхождений в декларации" });
            _typeDocuments.Add(new TypeDocument() { Id = 5, Name = "Запрос о предоставлении выписки по операциям на счетах" });
        }

        private void SetDictionaryFilter()
        {
            InitDictionaryTypeDocuments();
            _view.SetDictionaryTypeDocuments(_typeDocuments);
            _view.InitFilters();
        }

        private string GenerateSqlQueryIncome()
        {
            return GenerateSqlQueryIncomeBase(false);
        }

        private string GenerateSqlQueryIncomeForAll()
        {
            return GenerateSqlQueryIncomeBase(true);
        }

        private string GenerateSqlQueryIncomeBase(bool isForAll)
        {
            var queryCondition = new StringBuilder();
            var typeDocumentSelected = _view.GetFilterTypeDocumentSelected();

            if (typeDocumentSelected != null && typeDocumentSelected.Id != 0)
            {
                queryCondition.Append("and object_type_id = " + typeDocumentSelected.Id.ToString());

                if(typeDocumentSelected.SubTypeId != 0)
                    queryCondition.Append(" and doc_type = " + typeDocumentSelected.SubTypeId.ToString());
            }

            var regNumber = _view.GetFilterRegNumber();
            if (regNumber != null)
                queryCondition.Append("and declartion_reg_num = " + regNumber.ToString());

            if (!isForAll)
            {
                var isTop1000 = _view.GetIsSendAll();
                if (isTop1000)
                    queryCondition.Append("and rownum <= 1000 ");
            }

            var sqlQueryIncome = string.Format(Queries.INCOME_SELECT, queryCondition);

            return sqlQueryIncome;
        }

        public void Dispose()
        {
            if (View != null && !View.IsDisposed)
            {
                View = null;
            }
        }

        ~Presenter()
        {
            Dispose();
        }
    }
}