﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using EmulatorSEOD.Dialogs;
using EmulatorSEOD.Properties;
using Oracle.DataAccess.Client;

namespace EmulatorSEOD
{
    internal sealed class DbUser
    {
        private static readonly string _dataSource;
        private static Lazy<DbUser> _mrr;
        private static Lazy<DbUser> _cam;

        private readonly string _userName;
        private readonly string _password;

        public static DbUser MRR
        {
            get { return _mrr.Value; }
        }

        public static DbUser CAM
        {
            get { return _cam.Value; }
        }

        static DbUser()
        {
            _dataSource = Settings.Default.DATA_SOURCE;
            _mrr = new Lazy<DbUser>(() => new DbUser(Settings.Default.MRR_USER, Settings.Default.MRR_PASSWORD));
            _cam = new Lazy<DbUser>(() => new DbUser(Settings.Default.CAM_USER, Settings.Default.CAM_PASSWORD));
        }

        private DbUser(string userName, string password)
        {
            _userName = userName;
            _password = password;
        }

        public string GetConnectionString()
        {
            var builder = new OracleConnectionStringBuilder
            {
                DataSource = _dataSource,
                UserID = _userName,
                Password = _password
            };
            return builder.ConnectionString;
        }
    }

    public sealed class DbOperations
    {
        public DataTable GetDoc(string sqlQueryIncome)
        {
            using (var connection = new OracleConnection(DbUser.CAM.GetConnectionString()))
            using (var command = new OracleCommand(sqlQueryIncome, connection))
            {
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    var result = new DataTable("Income");
                    result.Load(reader);
                    return result;
                }
            }
        }

        public DataTable GetDocAll(string sqlQueryIncome)
        {
            using (var connection = new OracleConnection(DbUser.CAM.GetConnectionString()))
            using (var command = new OracleCommand(sqlQueryIncome, connection))
            {
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    var result = new DataTable("Income");
                    result.Load(reader);
                    return result;
                }
            }
        }

        public DataTable GetResult(long reg_number)
        {
            using (var connection = new OracleConnection(DbUser.CAM.GetConnectionString()))
            using (var command = new OracleCommand(Queries.RESULT_SELECT, connection))
            {
                command.Parameters.Add(":pId", reg_number);
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    var result = new DataTable("Result");
                    result.Load(reader);
                    return result;
                }
            }
        }

        private DataTable GetResponse(string text, long? id)
        {
            var result = new DataTable("Debug");
            if (!id.HasValue)
            {
                result.Columns.Add("NO_DEBUG_DATA_PROVIDED");
                return result;
            }

            using (var connection = new OracleConnection(DbUser.MRR.GetConnectionString()))
            using (var command = new OracleCommand(text, connection))
            {
                command.Parameters.Add(":pId", id);
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    result.Load(reader);
                    return result;
                }
            }
        }

        public bool GetInnKppByDeclartionRegNum(long declartionRegNum, out string inn, out string kpp)
        {
            inn = null;
            kpp = null;
            using (var connection = new OracleConnection(DbUser.CAM.GetConnectionString()))
            using (var command = new OracleCommand(Queries.GetInnKppByDeclartionRegNum, connection))
            {
                command.Parameters.Add(":pDeclartionRegNum", declartionRegNum);
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    var result = new DataTable("Result");
                    result.Load(reader);
                    if (result.Rows.Count > 0)
                    {
                        inn = result.Rows[0]["INN_CONTRACTOR"].ToString();
                        kpp = result.Rows[0]["KPP_EFFECTIVE"].ToString();
                        return true;
                    }
                }
            }

            return false;
        }

        public DataTable GetDocData(long docId)
        {
            using (var connection = new OracleConnection(DbUser.CAM.GetConnectionString()))
            {
                connection.Open();
                using (var command = new OracleCommand(Queries.GetDocData, connection))
                {
                    command.Parameters.Add(":doc_id", docId);
                    using (var reader = command.ExecuteReader())
                    {
                        var tab = new DataTable("Doc");
                        tab.Load(reader);
                        return tab;
                    }
                }
            }
        }

        private Func<DataTable> ExecuteSEOD(string text, string responseText, long? doc_id)
        {
            using (var connection = new OracleConnection(DbUser.CAM.GetConnectionString()))
            using (var command = new OracleCommand(text, connection))
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            return () => GetResponse(responseText, doc_id);
        }

        public Func<DataTable> ExecuteSEOD01(DialogEOD01.Model model)
        {
            var xml = XmlBuilder.GetEOD01Xml(model).Replace("\"", "''");
            var code = "CAM_NDS2_01";
            var name = "АСК НДС-2: : Данные по налоговой декларации (ЭОД-01)";

            var command = string.Format(Queries.NDS2_SEND, model.SounCode, model.RegNum, xml, code, name, "null");
            return ExecuteSEOD(command, Queries.EOD_STUB, 0);
        }

        public Func<DataTable> ExecuteSEOD02(IEnumerable<DataRow> rows, DialogEodDoc.Model model)
        {
            Func<DataTable> retVal = null;
            foreach (var row in rows)
            {
                var id = row.GetObjectId();
                var typeId = row.GetObjectTypeId();

                //При множественном выборе нужно обновить данные в модели 
                if (model.MultiSelectionMode)
                    model.Update(new DocDataService()
                                            .GetDocData(id, typeId));

                var xml = XmlBuilder.GetEOD02Xml(row, model).Replace("\"", "''");
                var code = "CAM_NDS2_02";
                var name = "АСК НДС-2: Сведения по статусам исполнения автотребования (ЭОД-2)";

                var command = string.Format(Queries.NDS2_SEND, row["sono_code"], row["declartion_reg_num"], xml, code, name, id);

                var debugQuery = ExecuteSEOD(command, Queries.EOD2_RESPONSE, id);
                retVal = retVal == null ? debugQuery : retVal + debugQuery;
            }
            return new Func<DataTable>(() => MergeResults(retVal.GetInvocationList()));
        }


        public Func<DataTable> ExecuteSEOD03(DataRow row, List<DialogOD03.Model> models)
        {
            var xml = XmlBuilder.GetEOD03Xml(row, models).Replace("\"", "''");
            var code = "CAM_NDS2_03";
            var name = "АСК НДС-2: Сведения по ответу на автотребование (ЭОД-3)";

            var id = GetId(row);
            var command = string.Format(Queries.NDS2_SEND, row["sono_code"], row["declartion_reg_num"], xml, code, name, id);
            return ExecuteSEOD(command, Queries.EOD3_RESPONSE, id);
        }

        public Func<DataTable> ExecuteSEOD04(DataRow row, DialogEOD04.Model model)
        {
            var xml = XmlBuilder.GetEOD04Xml(model).Replace("\"", "''");
            var code = "CAM_NDS2_04";
            var name = "АСК НДС-2: Сведения о ходе проведения КНП (ЭОД-4)";

            var id = GetId(row);
            var command = string.Format(Queries.NDS2_SEND, model.Declaration.SonoCode, model.Declaration.RegNumber, xml, code, name, id == null ? "null" : id.ToString());
            return ExecuteSEOD(command, Queries.EOD4_RESPONSE, id);
        }

        public Func<DataTable> ExecuteSEOD05(IEnumerable<DataRow> rows)
        {
            Func<DataTable> retVal = null;
            foreach (var row in rows)
            {
                var xml = XmlBuilder.GetEOD05Xml(row).Replace("\"", "''");
                var code = "CAM_NDS2_05";
                var name = "АСК НДС-2: Сведения о получении данных автотребования/автоистребования (ЭОД-5)";

                var id = GetId(row);
                var command = string.Format(Queries.NDS2_SEND, row["sono_code"], row["declartion_reg_num"], xml, code, name, id);

                var debugQuery = ExecuteSEOD(command, Queries.EOD5_RESPONSE, id);
                retVal = retVal == null ? debugQuery : retVal + debugQuery;
            }
            if (retVal != null)
                return new Func<DataTable>(() => MergeResults(retVal.GetInvocationList()));
            else
                return new Func<DataTable>(() => { return null; });
        }

        public Func<DataTable> ExecuteSEOD06(IEnumerable<DataRow> rows, DialogEodDoc.Model model)
        {
            Func<DataTable> retVal = null;
            foreach (var row in rows)
            {
                var id = row.GetObjectId();
                var typeId = row.GetObjectTypeId();
                if (model.MultiSelectionMode)
                    model.Update(new DocDataService()
                                            .GetDocData(id, typeId));

                var xml = XmlBuilder.GetEOD06Xml(row, model).Replace("\"", "''");
                var code = "CAM_NDS2_06";
                var name = "АСК НДС-2: Данные по статусам исполнения автоистребования (ЭОД-6)";

                var command = string.Format(Queries.NDS2_SEND, row["sono_code"], row["declartion_reg_num"], xml, code, name, id);

                var debugQuery = ExecuteSEOD(command, Queries.EOD_STUB, id);
                retVal = retVal == null ? debugQuery : retVal + debugQuery;
            }
            return new Func<DataTable>(() => MergeResults(retVal.GetInvocationList()));
        }

        public Func<DataTable> ExecuteSEOD07(DataRow row, DialogEOD07.Model model)
        {
            var xml = XmlBuilder.GetEOD07Xml(row, model).Replace("\"", "''");
            var code = "CAM_NDS2_07";
            var name = "АСК НДС-2: Данные по статусам Решения о продлении сроков представления документов (ЭОД-7)";

            var id = GetId(row);
            var command = string.Format(Queries.NDS2_SEND, row["sono_code"], row["declartion_reg_num"], xml, code, name, id);
            return ExecuteSEOD(command, Queries.EOD_STUB, id);
        }

        public Func<DataTable> ExecuteSEOD08(DataRow row, DialogEOD08.Model model)
        {
            var xml = XmlBuilder.GetEOD08Xml(row, model).Replace("\"", "''");
            var code = "CAM_NDS2_08";
            var name = "АСК НДС-2: Сведения о поступлении документов от НП ст.93 (ЭОД-8)";

            var id = GetId(row);
            var command = string.Format(Queries.NDS2_SEND, row["sono_code"], row["declartion_reg_num"], xml, code, name, id);
            return ExecuteSEOD(command, Queries.EOD_STUB, id);
        }

        public Func<DataTable> ExecuteSEOD09(DataRow row, DialogEOD09.Model model)
        {
            var xml = XmlBuilder.GetEOD09Xml(row, model).Replace("\"", "''");
            var code = "CAM_NDS2_09";
            var name = "АСК НДС-2: Данные по статусам Поручения об истребовании документов (ЭОД-9)";

            var id = GetId(row);
            var command = string.Format(Queries.NDS2_SEND, row["sono_code"], row["declartion_reg_num"], xml, code, name, id);
            return ExecuteSEOD(command, Queries.EOD_STUB, id);
        }

        public Func<DataTable> ExecuteSEOD10(DataRow row, DialogEOD10.Model model)
        {
            var xml = XmlBuilder.GetEOD10Xml(row, model).Replace("\"", "''");
            var code = "CAM_NDS2_10";
            var name = "АСК НДС-2: Данные по статусам Ответа НП на Требование о представление документов ст93.1 (ЭОД-10)";

            var id = GetId(row);
            var command = string.Format(Queries.NDS2_SEND, row["sono_code"], row["declartion_reg_num"], xml, code, name, id);
            return ExecuteSEOD(command, Queries.EOD_STUB, id);
        }

        public Func<DataTable> ExecuteSEOD11(DialogEOD11.Model model)
        {
            var xml = XmlBuilder.GetEOD11Xml(model).Replace("\"", "''");
            var code = "CAM_NDS2_11";
            var name = "АСК НДС-2: : Данные по журналу учёта полученных и выставленных счетов-фактур (ЭОД-11)";

            var command = string.Format(Queries.NDS2_SEND, model.SounCode, model.RegNum, xml, code, name, "null");
            return ExecuteSEOD(command, Queries.EOD_STUB, 0);
        }

        public Func<DataTable> ExecuteSEOD13(DialogEOD13.Model model)
        {
            var xml = XmlBuilder.GetEOD13Xml(model).Replace("\"", "''");
            var code = "CAM_NDS2_13";
            var name = "АСК НДС-2: : Данные по банковским выпискам (ЭОД-13)";

            var command = string.Format(Queries.NDS2_SEND, model.SounCode, 0, xml, code, name, model.RequestId);
            return ExecuteSEOD(command, Queries.EOD_STUB, 0);
        }

        public Func<DataTable> ExecuteSEOD14(DialogEOD14.Model model)
        {
            var xml = XmlBuilder.GetEOD14Xml(model).Replace("\"", "''");
            var code = "CAM_NDS2_14";
            var name = "АСК НДС-2: : Аннулирование декларации (ЭОД-14)";

            var command = string.Format(Queries.NDS2_SEND, model.SounCode, model.RegNum, xml, code, name, "null");
            return ExecuteSEOD(command, Queries.EOD_STUB, 0);
        }


        private long? GetId(DataRow row)
        {
            return row != null ?
                    (long?)(decimal?)row["object_reg_num"] : new long?();
        }

        private DataTable MergeResults(Delegate[] delegates)
        {
            var tabs = delegates.Select(d => d.DynamicInvoke() as DataTable).ToList();

            foreach (var tab_merged in tabs)
            {
                if (!Object.ReferenceEquals(tab_merged, tabs[0]))
                {
                    foreach (var merged_row in tab_merged.Rows)
                        tabs[0].Rows.Add((merged_row as DataRow).ItemArray);
                }
            }
            return tabs[0];
        }
    }

    internal static class ReadExt
    {
        public static string AsDateString(this object value)
        {
            if (value == null || value == DBNull.Value)
            {
                return string.Empty;
            }
            DateTime date;
            if (value is DateTime?)
            {
                date = ((DateTime?)value).Value;
            }
            else if (value is DateTime)
            {
                date = (DateTime)value;
            }
            else
            {
                throw new InvalidCastException();
            }
            return date.ToString("dd.MM.yyyy");
        }

        public static string ToStringSafe(this object obj)
        {
            return obj == null ? String.Empty : obj.ToString();
        }

        public static long GetObjectId(this DataRow row)
        {
            return (long)(decimal)row["object_reg_num"];
        }

        public static long GetObjectTypeId(this DataRow row)
        {
            return long.Parse(row["object_type_id"].ToString());
        }
    }

}