﻿using System;
using System.Data;
using System.Linq;

namespace EmulatorSEOD
{
    /// <summary>
    /// Предоставляет инфу об АТ/АИ из БД МРР
    /// </summary>
    public class DocDataService
    {
        readonly long?[] validObjectTypes = new long?[] { 1, 2 };
        DbOperations dal = new DbOperations();

        public DocData GetDocData(long doc_id, long? object_type)
        {
            if (!validObjectTypes.Contains(object_type))
                return new DocData();

            var tab = dal.GetDocData(doc_id);
            return CreateDocData(tab);
        }

        private DocData CreateDocData(DataTable tab)
        {
            if (tab.Rows.Count > 0)
            {
                long doc_id;
                long external_doc_num;
                DateTime external_doc_date;
                return new DocData()
                {
                    RegNumber =
                        !long.TryParse(tab.Rows[0]["doc_id"].ToStringSafe(), out doc_id)
                        ? null : new Nullable<Int64>(doc_id)
                    ,
                    ExternalDocNumber =
                        !long.TryParse(tab.Rows[0]["external_doc_num"].ToStringSafe(), out external_doc_num)
                        ? null : new Nullable<Int64>(external_doc_num)
                    ,
                    ExternalDocCreateDate =
                        !DateTime.TryParse(tab.Rows[0]["external_doc_date"].ToStringSafe(), out external_doc_date)
                        ? null : new Nullable<DateTime>(external_doc_date)
                };
            }
            else
                return new DocData();
        }
    }
}
