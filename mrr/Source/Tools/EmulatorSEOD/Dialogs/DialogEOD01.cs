﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EmulatorSEOD.Dialogs
{
    public partial class DialogEOD01 : Form
    {
        public sealed class Model
        {
            private readonly DialogEOD01 _owner;

            public bool IsUL
            {
                get { return _owner.rbUL.Checked; }
                set { _owner.rbUL.Checked = value; }
            }

            public string INN
            {
                get { return _owner.tbINN.Text; }
                set { _owner.tbINN.Text = value; }
            }

            public string KPP
            {
                get { return _owner.tbKPP.Text; }
                set { _owner.tbKPP.Text = value; }
            }

            public string SounCode
            {
                get { return _owner.tbSounCode.Text; }
                set { _owner.tbSounCode.Text = value; }
            }

            public string TaxPeriod
            {
                get { return _owner.tbTaxPeriod.Text; }
                set { _owner.tbTaxPeriod.Text = value; }
            }

            public string FiscalYear
            {
                get { return _owner.tbFiscalYear.Text; }
                set { _owner.tbFiscalYear.Text = value; }
            }

            public string SubmissionDate
            {
                get { return _owner.tbSubmissionDate.Text; }
                set { _owner.tbSubmissionDate.Text = value; }
            }

            public string CorrectionNum
            {
                get { return _owner.tbCorrectionNum.Text; }
                set { _owner.tbCorrectionNum.Text = value; }
            }

            public string FileId
            {
                get { return _owner.tbFileID.Text; }
                set { _owner.tbFileID.Text = value; }
            }

            public long? RegNum
            {
                get 
                {
                    long result;
                    return string.IsNullOrEmpty(_owner.tbRegNum.Text) || !long.TryParse(_owner.tbRegNum.Text, out result) ? (long?)null : result;
                }
                set { _owner.tbRegNum.Text = value == null ? null : value.ToString(); }
            }

            public string FID
            {
                get { return _owner.tbFID.Text; }
                set { _owner.tbFID.Text = value; }
            }

            public string TaxpayerFID
            {
                get { return _owner.tbTaxpayerFID.Text; }
                set { _owner.tbTaxpayerFID.Text = value; }
            }

            public Model(DialogEOD01 owner)
            {
                _owner = owner;
            }
        }

        public Model Data { get; private set; }

        public DialogEOD01()
        {
            InitializeComponent();

            Data = new Model(this);
        }

        private void RadioButton_CheckedChanged(object sender, EventArgs e)
        {
            tbKPP.Enabled = rbUL.Checked;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            Close();
        }
    }
}