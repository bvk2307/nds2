﻿using Infragistics.Win.Misc;
using System;
using System.Windows.Forms;

namespace EmulatorSEOD.Dialogs
{
    /// <summary>
    /// Объединяет общие данные и поведение для диалогов по АТ/АИ (ЭОД2/ЭОД6)
    /// Поведение такое:
    ///  - В режиме множественного выбора дизэйблит текстбокс для ввода номера документа
    /// </summary>
    public abstract partial class DialogEodDoc : Form
    {
        public Model Data { get; private set; }

        public DialogEodDoc()
        {
            InitializeComponent();
            Data = new DialogEodDoc.Model(this);
        }

        /// <summary>
        /// Инициализация диалога
        /// </summary>
        public void InitDialog()
        {
            InitDialogTextLables();
            EnableInit();
        }

        /// <summary>
        /// Содержит логику проверки. Вызывается при нажатии кнопки "Отправить"
        /// </summary>
        /// <param name="ErrorMessage">Текст сообщения об ошибке валидации</param>
        /// <returns>Результат валидации</returns>
        protected virtual bool ValidateData(out string ErrorMessage)
        {
            ErrorMessage = null;
            return true;
        }

        #region Методы возвращающие значения текстовых полей формы
        protected abstract string GetDialogCaptionText();

        protected abstract string GetDocNumText();

        protected abstract string GetDocDateText();

        protected abstract string GetDocSendDateText();

        protected abstract string GetDocRecieveDateText();

        protected abstract string GetDocRecieveMethodText();
        #endregion

        private void EnableInit()
        {
            tbDocNum.Enabled = !Data.MultiSelectionMode;
            deDocDate.Enabled = true;
            deSendDate.Enabled = true;
            deRecieveDate.Enabled = true;
            ceRecieveMethod.Enabled = ceRecieveMethod.SelectedIndex == -1;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string errMessage;
            if (!ValidateData(out errMessage))
            {
                MessageBox.Show(errMessage, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var changed = string.IsNullOrEmpty(tbDocNum.Text) == tbDocNum.Enabled
                    || (deDocDate.Value == null) == deDocDate.Enabled
                    || (deSendDate.Value == null) == deSendDate.Enabled
                    || (deRecieveDate.Value == null) == deRecieveDate.Enabled
                    || (ceRecieveMethod.SelectedIndex >= 0) == ceRecieveMethod.Enabled;

            if (!changed)
            {
                this.DialogResult = DialogResult.Ignore;
            }
            else
            {
                this.DialogResult = DialogResult.OK;
            }
            Close();
        }

        private void InitDialogTextLables()
        {
            this.Text = GetDialogCaptionText();

            FindLabelByName("lDocNum").Text = GetDocNumText();
            FindLabelByName("lDocDate").Text = GetDocDateText();
            FindLabelByName("lSendDate").Text = GetDocSendDateText();
            FindLabelByName("lRecieveDate").Text = GetDocRecieveDateText();
            FindLabelByName("lRecieveMethod").Text = GetDocRecieveMethodText();
        }

        private UltraLabel FindLabelByName(string labelName)
        {
            return this.Controls.Find(labelName, false)[0] as Infragistics.Win.Misc.UltraLabel;
        }

        #region Model
        public sealed class Model
        {
            private readonly DialogEodDoc _owner;

            ///// <summary>
            ///// Признак множественного выбора
            ///// </summary>
            public bool MultiSelectionMode { get; set; }


            public string NumDoc
            {
                get { return _owner.tbDocNum.Text; }
                set { _owner.tbDocNum.Text = value; }
            }

            public DateTime? DateDoc
            {
                get { return _owner.deDocDate.Value == null ? null : (DateTime?)_owner.deDocDate.DateTime; }
                set { _owner.deDocDate.Value = value == null ? null : ((DateTime?)value); }
            }

            public DateTime? SendDate
            {
                get { return _owner.deSendDate.Value == null ? null : (DateTime?)_owner.deSendDate.DateTime; }
                set { _owner.deSendDate.Value = value == null ? null : ((DateTime?)value); }
            }

            public DateTime? RecieveDate
            {
                get { return _owner.deRecieveDate.Value == null ? null : (DateTime?)_owner.deRecieveDate.DateTime; }
                set { _owner.deRecieveDate.Value = value == null ? null : ((DateTime?)value); }
            }

            public string RecieveMethod
            {
                get { return _owner.ceRecieveMethod.Text; }
                set { _owner.ceRecieveMethod.Text = value; }
            }

            public Model(DialogEodDoc owner)
            {
                _owner = owner;
                DefaultValuesInit();
            }

            
            public void Update(DocData docData)
            {
                //По согласованию с тестированием номер документа вычисляется так.
                NumDoc = docData.ExternalDocNumber != null ?
                            docData.ExternalDocNumber.ToString() : docData.RegNumber.ToString();
                DateDoc = docData.ExternalDocCreateDate;
            }

            private void DefaultValuesInit()
            {
                DateDoc = SendDate = RecieveDate = null;
            }
        }
        #endregion
    }
}