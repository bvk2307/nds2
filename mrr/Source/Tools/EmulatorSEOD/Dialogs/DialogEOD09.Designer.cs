﻿namespace EmulatorSEOD.Dialogs
{
    partial class DialogEOD09
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Misc.UltraLabel lDocNum;
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel lRegisterDate;
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel lExecutorSendDate;
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel lExecutorRecieveDate;
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel lRejectReason;
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem4 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem5 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem6 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem7 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem8 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem9 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem10 = new Infragistics.Win.ValueListItem();
            this.tbDocNum = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.deRegisterDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.deExecutorSendDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.deExecutorRecieveDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ceRejectReason = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            lDocNum = new Infragistics.Win.Misc.UltraLabel();
            lRegisterDate = new Infragistics.Win.Misc.UltraLabel();
            lExecutorSendDate = new Infragistics.Win.Misc.UltraLabel();
            lExecutorRecieveDate = new Infragistics.Win.Misc.UltraLabel();
            lRejectReason = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.tbDocNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deRegisterDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deExecutorSendDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deExecutorRecieveDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceRejectReason)).BeginInit();
            this.SuspendLayout();
            // 
            // lDocNum
            // 
            appearance6.TextHAlignAsString = "Left";
            appearance6.TextVAlignAsString = "Middle";
            lDocNum.Appearance = appearance6;
            lDocNum.Location = new System.Drawing.Point(12, 12);
            lDocNum.Name = "lDocNum";
            lDocNum.Size = new System.Drawing.Size(273, 21);
            lDocNum.TabIndex = 3;
            lDocNum.Text = "Номер Поручения об истребовании документов";
            // 
            // lRegisterDate
            // 
            appearance7.TextHAlignAsString = "Left";
            appearance7.TextVAlignAsString = "Middle";
            lRegisterDate.Appearance = appearance7;
            lRegisterDate.Location = new System.Drawing.Point(12, 39);
            lRegisterDate.Name = "lRegisterDate";
            lRegisterDate.Size = new System.Drawing.Size(273, 21);
            lRegisterDate.TabIndex = 4;
            lRegisterDate.Text = "Дата регистрации документа";
            // 
            // lExecutorSendDate
            // 
            appearance8.TextHAlignAsString = "Left";
            appearance8.TextVAlignAsString = "Middle";
            lExecutorSendDate.Appearance = appearance8;
            lExecutorSendDate.Location = new System.Drawing.Point(12, 66);
            lExecutorSendDate.Name = "lExecutorSendDate";
            lExecutorSendDate.Size = new System.Drawing.Size(273, 21);
            lExecutorSendDate.TabIndex = 5;
            lExecutorSendDate.Text = "Дата направления Поручения в НО-исполнитель";
            // 
            // lExecutorRecieveDate
            // 
            appearance10.TextHAlignAsString = "Left";
            appearance10.TextVAlignAsString = "Middle";
            lExecutorRecieveDate.Appearance = appearance10;
            lExecutorRecieveDate.Location = new System.Drawing.Point(12, 93);
            lExecutorRecieveDate.Name = "lExecutorRecieveDate";
            lExecutorRecieveDate.Size = new System.Drawing.Size(273, 21);
            lExecutorRecieveDate.TabIndex = 6;
            lExecutorRecieveDate.Text = "Дата получения Поручения НО-исполнителем";
            // 
            // lRejectReason
            // 
            appearance9.TextHAlignAsString = "Left";
            appearance9.TextVAlignAsString = "Middle";
            lRejectReason.Appearance = appearance9;
            lRejectReason.Location = new System.Drawing.Point(12, 120);
            lRejectReason.Name = "lRejectReason";
            lRejectReason.Size = new System.Drawing.Size(273, 21);
            lRejectReason.TabIndex = 7;
            lRejectReason.Text = "Причина отказа в исполнении Поручения";
            // 
            // tbDocNum
            // 
            appearance4.TextHAlignAsString = "Left";
            appearance4.TextVAlignAsString = "Middle";
            this.tbDocNum.Appearance = appearance4;
            this.tbDocNum.Location = new System.Drawing.Point(295, 12);
            this.tbDocNum.Name = "tbDocNum";
            this.tbDocNum.Size = new System.Drawing.Size(100, 21);
            this.tbDocNum.TabIndex = 0;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(320, 149);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 9;
            this.btnSave.Text = "Сохранить";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // deRegisterDate
            // 
            this.deRegisterDate.DateTime = new System.DateTime(2015, 4, 6, 0, 0, 0, 0);
            this.deRegisterDate.Location = new System.Drawing.Point(295, 39);
            this.deRegisterDate.Name = "deRegisterDate";
            this.deRegisterDate.Size = new System.Drawing.Size(100, 21);
            this.deRegisterDate.TabIndex = 10;
            this.deRegisterDate.Value = new System.DateTime(2015, 4, 6, 0, 0, 0, 0);
            // 
            // deExecutorSendDate
            // 
            this.deExecutorSendDate.DateTime = new System.DateTime(2015, 4, 6, 0, 0, 0, 0);
            this.deExecutorSendDate.Location = new System.Drawing.Point(295, 66);
            this.deExecutorSendDate.Name = "deExecutorSendDate";
            this.deExecutorSendDate.Size = new System.Drawing.Size(100, 21);
            this.deExecutorSendDate.TabIndex = 11;
            this.deExecutorSendDate.Value = new System.DateTime(2015, 4, 6, 0, 0, 0, 0);
            // 
            // deExecutorRecieveDate
            // 
            this.deExecutorRecieveDate.DateTime = new System.DateTime(2015, 4, 6, 0, 0, 0, 0);
            this.deExecutorRecieveDate.Location = new System.Drawing.Point(295, 93);
            this.deExecutorRecieveDate.Name = "deExecutorRecieveDate";
            this.deExecutorRecieveDate.Size = new System.Drawing.Size(100, 21);
            this.deExecutorRecieveDate.TabIndex = 12;
            this.deExecutorRecieveDate.Value = new System.DateTime(2015, 4, 6, 0, 0, 0, 0);
            // 
            // ceRejectReason
            // 
            this.ceRejectReason.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            valueListItem1.DataValue = "1";
            valueListItem1.DisplayText = "1";
            valueListItem2.DataValue = "2";
            valueListItem2.DisplayText = "2";
            valueListItem3.DataValue = "3";
            valueListItem3.DisplayText = "3";
            valueListItem4.DataValue = "4";
            valueListItem4.DisplayText = "4";
            valueListItem5.DataValue = "5";
            valueListItem5.DisplayText = "5";
            valueListItem6.DataValue = "6";
            valueListItem6.DisplayText = "6";
            valueListItem7.DataValue = "7";
            valueListItem7.DisplayText = "7";
            valueListItem8.DataValue = "8";
            valueListItem8.DisplayText = "8";
            valueListItem9.DataValue = "9";
            valueListItem9.DisplayText = "9";
            valueListItem10.DataValue = "10";
            valueListItem10.DisplayText = "10";
            this.ceRejectReason.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2,
            valueListItem3,
            valueListItem4,
            valueListItem5,
            valueListItem6,
            valueListItem7,
            valueListItem8,
            valueListItem9,
            valueListItem10});
            this.ceRejectReason.Location = new System.Drawing.Point(295, 120);
            this.ceRejectReason.Name = "ceRejectReason";
            this.ceRejectReason.Size = new System.Drawing.Size(100, 21);
            this.ceRejectReason.TabIndex = 14;
            // 
            // DialogEOD09
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(407, 184);
            this.Controls.Add(this.ceRejectReason);
            this.Controls.Add(this.deExecutorRecieveDate);
            this.Controls.Add(this.deExecutorSendDate);
            this.Controls.Add(this.deRegisterDate);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(lRejectReason);
            this.Controls.Add(lExecutorRecieveDate);
            this.Controls.Add(lExecutorSendDate);
            this.Controls.Add(lRegisterDate);
            this.Controls.Add(lDocNum);
            this.Controls.Add(this.tbDocNum);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DialogEOD09";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Параметры для ЭОД-9";
            ((System.ComponentModel.ISupportInitialize)(this.tbDocNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deRegisterDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deExecutorSendDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deExecutorRecieveDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceRejectReason)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.UltraWinEditors.UltraTextEditor tbDocNum;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor deRegisterDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor deExecutorSendDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor deExecutorRecieveDate;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor ceRejectReason;
    }
}