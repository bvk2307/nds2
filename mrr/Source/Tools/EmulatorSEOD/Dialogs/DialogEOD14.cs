﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EmulatorSEOD.Dialogs
{
    public partial class DialogEOD14 : Form
    {
        public sealed class Model
        {
            private readonly DialogEOD14 _owner;

            public Model(DialogEOD14 owner)
            {
                _owner = owner;
            }

            public string RequestId
            {
                get
                {
                    return _owner.tbFileID.Text;
                }
                set
                {
                    _owner.tbFileID.Text = value;
                }
            }

            public string SounCode
            {
                get
                {
                    return _owner._editSono.Text;
                }
                set
                {
                    _owner._editSono.Text = value;
                }
            }

            public DateTime CancelationDate
            {
                get
                {
                    return _owner._cancelationDate.DateTime;
                }
                set
                {
                    _owner._cancelationDate.DateTime = value;
                }
            }

            public string TaxPeriod
            {
                get { return _owner.tbTaxPeriod.Text; }
                set { _owner.tbTaxPeriod.Text = value; }
            }

            public string FiscalYear
            {
                get { return _owner.tbFiscalYear.Text; }
                set { _owner.tbFiscalYear.Text = value; }
            }

            public string CorrectionNum
            {
                get { return _owner.tbCorrectionNum.Text; }
                set { _owner.tbCorrectionNum.Text = value; }
            }

            public long? RegNum
            {
                get
                {
                    long result;
                    return string.IsNullOrEmpty(_owner.tbRegNum.Text) || !long.TryParse(_owner.tbRegNum.Text, out result) ? (long?)null : result;
                }
                set { _owner.tbRegNum.Text = value == null ? null : value.ToString(); }
            }

            public int CancelationCause
            {
                get
                {
                    return _owner._cancelationCause.SelectedIndex + 1;
                }
                set
                {
                    _owner._cancelationCause.SelectedIndex = value - 1;
                }
            }

            public bool IsOpenPreviousKNPNeeded
            {
                get { return _owner._openPreviousKNP.SelectedIndex == 1 ? true : false; }
                set { _owner._openPreviousKNP.SelectedIndex = value ? 1 : 0; }
            }

            public bool IsNPFL
            {
                get { return _owner.rbIP.Checked; }
                set { _owner.rbIP.Checked = value; }
            }
            public string INN
            {
                get { return _owner.tbINN.Text; }
                set { _owner.tbINN.Text = value; }
            }

            public string KPP
            {
                get { return _owner.tbKPP.Text; }
                set { _owner.tbKPP.Text = value; }
            }

            public string FileId
            {
                get { return _owner.tbFileID.Text; }
                set { _owner.tbFileID.Text = value; }
            }
        }
        public Model Data { get; private set; }

        public DialogEOD14()
        {
            InitializeComponent();
            
            Data = new Model(this);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void rbIP_CheckedChanged(object sender, EventArgs e)
        {
            tbKPP.Enabled = rbUL.Checked;
        }
    }
}
