﻿using System;
using System.Windows.Forms;

namespace EmulatorSEOD.Dialogs
{
    public partial class DialogEOD13 : Form
    {
        public sealed class Model
        {
            private readonly DialogEOD13 _owner;

            public Model(DialogEOD13 owner)
            {
                _owner = owner;
            }

            public string RequestId
            {
                get
                {
                    return _owner._editRequestId.Text;
                }
                set
                {
                    _owner._editRequestId.Text = value;
                }
            }

            public long? RegNumber
            {
                get
                {
                    long result;
                    return string.IsNullOrEmpty(_owner._editRegNumber.Text) || !long.TryParse(_owner._editRegNumber.Text, out result) ? (long?)null : result;
                }
                set
                {
                    _owner._editRegNumber.Text = value.ToString();
                }
            }

            public string SounCode
            {
                get
                {
                    return _owner._editSono.Text;
                }
                set
                {
                    _owner._editSono.Text = value;
                }
            }

            public DateTime RequestDate
            {
                get
                {
                    return _owner._editDate.DateTime;
                }
                set
                {
                    _owner._editDate.DateTime = value;
                }
            }

            public int RequestState
            {
                get
                {
                    return _owner._editState.SelectedIndex + 1;
                }
                set
                {
                    _owner._editState.SelectedIndex = value - 1;
                }
            }
        }
        
        public Model Data { get; private set; }

        public DialogResult ShowDialog(System.Data.DataRow row)
        {
            Data.SounCode = row["SONO_CODE"].ToString();
            Data.RequestId = row["OBJECT_REG_NUM"].ToString();

            return ShowDialog();
        }

        public DialogEOD13()
        {
            InitializeComponent();

            Data = new Model(this);
        }

        private void SaveClick(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
