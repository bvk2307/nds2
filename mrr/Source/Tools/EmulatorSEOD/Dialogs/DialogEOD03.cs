﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Infragistics.Win.UltraWinListView;
using System.Numerics;

namespace EmulatorSEOD.Dialogs
{
    public partial class DialogOD03 : Form
    {
        public class Model
        {
            public long DocId { get; set; }

            public string ReplyNumber { get; set; }

            public DateTime ReplyDate { get; set; }

            public string ReplyFileName { get; set; }
        }

        private readonly long _doc_id;
        private readonly string _inn;
        private readonly string _kpp;
        private bool _fileNameManual;

        public IEnumerable<Model> Data
        {
            get
            {
                return
                    from item in lvRows.Items.Cast<UltraListViewItem>()
                    select new Model
                    {
                        DocId = _doc_id,
                        ReplyNumber = item.SubItems["Reply_Number"].Value.ToString(),
                        ReplyDate = DateTime.Parse(item.SubItems["Reply_Date"].Value.ToString()),
                        ReplyFileName = item.SubItems["Reply_File_Name"].Value.ToString()
                    };
            }
        }

        public DialogOD03(long doc_id,string inn,string kpp)
        {
            InitializeComponent();
            _doc_id = doc_id;
            _inn = inn;
            _kpp = kpp;

            ResetInput();
        }

        private void ResetInput()
        {
            deDate.Value = null;
            tbNumber.Value = null;
            tbFileName.Value = BuildFileName();
        }

        private string BuildFileName()
        {
            DateTime docDT = (DateTime) (deDate.Value==null?DateTime.Now: deDate.Value);
            string A = "0001";
            string K = "0002";
            string O = string.Format("{0}{1}", _inn, _kpp);
            string fileName = GetUniqueFileName();
            string nameFile = string.Format("ON_OTTRNDS_{0}_{1}_{2}_{3:0000}{4:00}{5:00}_{6}.xml", A, K, O, docDT.Year, docDT.Month, docDT.Day, fileName);

            return nameFile;

        }

        private string GetUniqueFileName()
        {
            Guid guid = Guid.NewGuid();
            byte[] guidAsBytes = guid.ToByteArray();
            BigInteger guidAsInt = BigInteger.Abs(new BigInteger(guidAsBytes));
            return guidAsInt.ToString().Substring(0,35);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbNumber.Text) || deDate.Value == null)
            {
                MessageBox.Show("Номер и дата должны быть заполнены", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var item = new UltraListViewItem(_doc_id.ToString(), new[]
            {
                new UltraListViewSubItem(deDate.DateTime.ToShortDateString(), null),
                new UltraListViewSubItem(tbNumber.Text, null),
                new UltraListViewSubItem(tbFileName.Text, null)

            }, null);

            lvRows.Items.Add(item);
            lvRows.Refresh();
            ResetInput();
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            foreach (var item in lvRows.SelectedItems.ToList())
            {
                lvRows.Items.Remove(item);
            }
            lvRows.Refresh();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void deDate_ValueChanged(object sender, EventArgs e)
        {
            if(!_fileNameManual)
                tbFileName.Value = BuildFileName();



        }

        private void tbFileName_KeyPress(object sender, KeyPressEventArgs e)
        {
            _fileNameManual = true;
        }
    }
}