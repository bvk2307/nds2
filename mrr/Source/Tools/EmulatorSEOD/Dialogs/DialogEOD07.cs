﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EmulatorSEOD.Dialogs
{
    public partial class DialogEOD07 : Form
    {
        public sealed class Model
        {
            private readonly DialogEOD07 _owner;

            public string NumDoc
            {
                get { return _owner.tbDocNum.Text; }
                set { _owner.tbDocNum.Text = value; }
            }

            public DateTime? DateDoc
            {
                get { return _owner.deDocDate.Value == null ? null : (DateTime?)_owner.deDocDate.DateTime; }
                set { _owner.deDocDate.Value = value == null ? null : ((DateTime?)value); }
            }

            public DateTime? ProlongDate
            {
                get { return _owner.deProlongDate.Value == null ? null : (DateTime?)_owner.deProlongDate.DateTime; }
                set { _owner.deProlongDate.Value = value == null ? null : ((DateTime?)value); }
            }

            public Model(DialogEOD07 owner)
            {
                _owner = owner;
                DefaultValuesInit();
            }

            private void DefaultValuesInit()
            {
                DateDoc = ProlongDate = null;
            }
        }

        public Model Data { get; private set; }

        public bool AllDisabled
        {
            get { return !tbDocNum.Enabled && !deDocDate.Enabled && !deProlongDate.Enabled; }
        }

        public DialogEOD07(DataRow row)
        {
            InitializeComponent();

            Data = new DialogEOD07.Model(this)
            {
                //NumDoc = row["external_doc_num"].ToString(),
                //DateDoc = AsDate(row["external_doc_date"]),
                //SendDate = AsDate(row["tax_payer_send_date"]),
                //RecieveDate = AsDate(row["tax_payer_delivery_date"]),
                //RecieveMethod = row["tax_payer_delivery_method"].ToString()
            };
            EnableInit();
        }

        private void EnableInit()
        {
            tbDocNum.Enabled = string.IsNullOrEmpty(tbDocNum.Text);
            deDocDate.Enabled = deDocDate.Value == null;
            deProlongDate.Enabled = deProlongDate.Value == null;
        }

        private DateTime? AsDate(object value)
        {
            if (value == null || value == DBNull.Value || !(value is DateTime) || !(value is DateTime?))
            {
                return null;
            }
            return (DateTime?)value;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Data.NumDoc) || Data.DateDoc == null || Data.ProlongDate == null)
            {
                MessageBox.Show("Все поля должны быть заполнены", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            this.DialogResult = DialogResult.OK;
            Close();
        }
    }
}