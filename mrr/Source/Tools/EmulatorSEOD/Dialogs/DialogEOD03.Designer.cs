﻿namespace EmulatorSEOD.Dialogs
{
    partial class DialogOD03
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.UltraWinListView.UltraListViewSubItemColumn ultraListViewSubItemColumn1 = new Infragistics.Win.UltraWinListView.UltraListViewSubItemColumn("Reply_Date");
            Infragistics.Win.UltraWinListView.UltraListViewSubItemColumn ultraListViewSubItemColumn2 = new Infragistics.Win.UltraWinListView.UltraListViewSubItemColumn("Reply_Number");
            Infragistics.Win.UltraWinListView.UltraListViewSubItemColumn ultraListViewSubItemColumn3 = new Infragistics.Win.UltraWinListView.UltraListViewSubItemColumn("Reply_File_Name");
            this.lvRows = new Infragistics.Win.UltraWinListView.UltraListView();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.btnRemove = new Infragistics.Win.Misc.UltraButton();
            this.tbNumber = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.btnAdd = new Infragistics.Win.Misc.UltraButton();
            this.deDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.tbFileName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            ((System.ComponentModel.ISupportInitialize)(this.lvRows)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbFileName)).BeginInit();
            this.SuspendLayout();
            // 
            // lvRows
            // 
            this.lvRows.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvRows.ItemSettings.AllowEdit = Infragistics.Win.DefaultableBoolean.False;
            this.lvRows.ItemSettings.SelectionType = Infragistics.Win.UltraWinListView.SelectionType.Single;
            this.lvRows.ItemSettings.TipStyle = Infragistics.Win.UltraWinListView.ItemTipStyle.Hide;
            this.lvRows.Location = new System.Drawing.Point(329, 12);
            this.lvRows.MainColumn.AllowMoving = Infragistics.Win.DefaultableBoolean.False;
            this.lvRows.MainColumn.AllowSizing = Infragistics.Win.DefaultableBoolean.False;
            this.lvRows.MainColumn.AllowSorting = Infragistics.Win.DefaultableBoolean.False;
            this.lvRows.MainColumn.Key = "Doc_Id";
            this.lvRows.MainColumn.ShowSortIndicators = Infragistics.Win.DefaultableBoolean.False;
            this.lvRows.MainColumn.VisiblePositionInDetailsView = 0;
            this.lvRows.Name = "lvRows";
            this.lvRows.ShowGroups = false;
            this.lvRows.Size = new System.Drawing.Size(803, 366);
            ultraListViewSubItemColumn1.AllowMoving = Infragistics.Win.DefaultableBoolean.False;
            ultraListViewSubItemColumn1.AllowSizing = Infragistics.Win.DefaultableBoolean.False;
            ultraListViewSubItemColumn1.AllowSorting = Infragistics.Win.DefaultableBoolean.False;
            ultraListViewSubItemColumn1.Key = "Reply_Date";
            ultraListViewSubItemColumn1.VisibleInDetailsView = Infragistics.Win.DefaultableBoolean.True;
            ultraListViewSubItemColumn1.VisibleInTilesView = Infragistics.Win.DefaultableBoolean.False;
            ultraListViewSubItemColumn1.VisibleInToolTip = Infragistics.Win.DefaultableBoolean.False;
            ultraListViewSubItemColumn1.VisiblePositionInDetailsView = 2;
            ultraListViewSubItemColumn2.AllowMoving = Infragistics.Win.DefaultableBoolean.False;
            ultraListViewSubItemColumn2.AllowSizing = Infragistics.Win.DefaultableBoolean.False;
            ultraListViewSubItemColumn2.AllowSorting = Infragistics.Win.DefaultableBoolean.False;
            ultraListViewSubItemColumn2.Key = "Reply_Number";
            ultraListViewSubItemColumn2.VisibleInDetailsView = Infragistics.Win.DefaultableBoolean.True;
            ultraListViewSubItemColumn2.VisibleInTilesView = Infragistics.Win.DefaultableBoolean.False;
            ultraListViewSubItemColumn2.VisibleInToolTip = Infragistics.Win.DefaultableBoolean.False;
            ultraListViewSubItemColumn2.VisiblePositionInDetailsView = 1;
            ultraListViewSubItemColumn3.AllowMoving = Infragistics.Win.DefaultableBoolean.False;
            ultraListViewSubItemColumn3.AllowSizing = Infragistics.Win.DefaultableBoolean.False;
            ultraListViewSubItemColumn3.AllowSorting = Infragistics.Win.DefaultableBoolean.False;
            ultraListViewSubItemColumn3.Key = "Reply_File_Name";
            ultraListViewSubItemColumn3.VisibleInDetailsView = Infragistics.Win.DefaultableBoolean.True;
            ultraListViewSubItemColumn3.VisibleInTilesView = Infragistics.Win.DefaultableBoolean.False;
            ultraListViewSubItemColumn3.VisibleInToolTip = Infragistics.Win.DefaultableBoolean.False;
            ultraListViewSubItemColumn3.Width = 400;
            this.lvRows.SubItemColumns.AddRange(new Infragistics.Win.UltraWinListView.UltraListViewSubItemColumn[] {
            ultraListViewSubItemColumn1,
            ultraListViewSubItemColumn2,
            ultraListViewSubItemColumn3});
            this.lvRows.TabIndex = 0;
            this.lvRows.View = Infragistics.Win.UltraWinListView.UltraListViewStyle.Details;
            this.lvRows.ViewSettingsDetails.AllowColumnMoving = false;
            this.lvRows.ViewSettingsDetails.AllowColumnSizing = false;
            this.lvRows.ViewSettingsDetails.AllowColumnSorting = false;
            this.lvRows.ViewSettingsDetails.ColumnAutoSizeMode = ((Infragistics.Win.UltraWinListView.ColumnAutoSizeMode)((Infragistics.Win.UltraWinListView.ColumnAutoSizeMode.Header | Infragistics.Win.UltraWinListView.ColumnAutoSizeMode.AllItems)));
            this.lvRows.ViewSettingsDetails.ColumnsShowSortIndicators = false;
            this.lvRows.ViewSettingsDetails.FullRowSelect = true;
            this.lvRows.ViewSettingsDetails.ImageSize = new System.Drawing.Size(0, 0);
            this.lvRows.ViewSettingsDetails.SubItemTipStyle = Infragistics.Win.UltraWinListView.SubItemTipStyle.Hide;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(1057, 384);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "Сохранить";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnRemove
            // 
            this.btnRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRemove.Location = new System.Drawing.Point(976, 384);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(75, 23);
            this.btnRemove.TabIndex = 2;
            this.btnRemove.Text = "Удалить";
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // tbNumber
            // 
            this.tbNumber.Location = new System.Drawing.Point(12, 12);
            this.tbNumber.Name = "tbNumber";
            this.tbNumber.NullText = "Входящий номер ответа";
            this.tbNumber.Size = new System.Drawing.Size(311, 21);
            this.tbNumber.TabIndex = 3;
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd.Location = new System.Drawing.Point(895, 384);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 5;
            this.btnAdd.Text = "Добавить";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // deDate
            // 
            this.deDate.Location = new System.Drawing.Point(12, 39);
            this.deDate.Name = "deDate";
            this.deDate.NullText = "Входящая дата ответа";
            this.deDate.Size = new System.Drawing.Size(311, 21);
            this.deDate.TabIndex = 6;
            this.deDate.ValueChanged += new System.EventHandler(this.deDate_ValueChanged);
            // 
            // tbFileName
            // 
            this.tbFileName.Location = new System.Drawing.Point(12, 66);
            this.tbFileName.Multiline = true;
            this.tbFileName.Name = "tbFileName";
            this.tbFileName.NullText = "Имя файла ответа";
            this.tbFileName.Size = new System.Drawing.Size(311, 48);
            this.tbFileName.TabIndex = 7;
            this.tbFileName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbFileName_KeyPress);
            // 
            // DialogOD03
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1144, 419);
            this.Controls.Add(this.tbFileName);
            this.Controls.Add(this.deDate);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.tbNumber);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.lvRows);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DialogOD03";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Параметры для ЭОД-3";
            ((System.ComponentModel.ISupportInitialize)(this.lvRows)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbFileName)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.UltraWinListView.UltraListView lvRows;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.Misc.UltraButton btnRemove;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor tbNumber;
        private Infragistics.Win.Misc.UltraButton btnAdd;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor deDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor tbFileName;
    }
}