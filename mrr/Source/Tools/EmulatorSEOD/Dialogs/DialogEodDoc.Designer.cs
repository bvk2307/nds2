﻿namespace EmulatorSEOD.Dialogs
{
    partial class DialogEodDoc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Misc.UltraLabel lDocNum;
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel lDocDate;
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel lSendDate;
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel lRecieveDate;
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel lRecieveMethod;
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            this.tbDocNum = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.deDocDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.deSendDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.deRecieveDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ceRecieveMethod = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            lDocNum = new Infragistics.Win.Misc.UltraLabel();
            lDocDate = new Infragistics.Win.Misc.UltraLabel();
            lSendDate = new Infragistics.Win.Misc.UltraLabel();
            lRecieveDate = new Infragistics.Win.Misc.UltraLabel();
            lRecieveMethod = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.tbDocNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDocDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deSendDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deRecieveDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceRecieveMethod)).BeginInit();
            this.SuspendLayout();
            // 
            // lDocNum
            // 
            appearance6.TextHAlignAsString = "Left";
            appearance6.TextVAlignAsString = "Middle";
            lDocNum.Appearance = appearance6;
            lDocNum.Location = new System.Drawing.Point(12, 12);
            lDocNum.Name = "lDocNum";
            lDocNum.Size = new System.Drawing.Size(273, 21);
            lDocNum.TabIndex = 3;
            lDocNum.Text = "Номер требования о предоставлении пояснений";
            // 
            // lDocDate
            // 
            appearance7.TextHAlignAsString = "Left";
            appearance7.TextVAlignAsString = "Middle";
            lDocDate.Appearance = appearance7;
            lDocDate.Location = new System.Drawing.Point(12, 39);
            lDocDate.Name = "lDocDate";
            lDocDate.Size = new System.Drawing.Size(273, 21);
            lDocDate.TabIndex = 4;
            lDocDate.Text = "Дата формирования документа";
            // 
            // lSendDate
            // 
            appearance8.TextHAlignAsString = "Left";
            appearance8.TextVAlignAsString = "Middle";
            lSendDate.Appearance = appearance8;
            lSendDate.Location = new System.Drawing.Point(12, 66);
            lSendDate.Name = "lSendDate";
            lSendDate.Size = new System.Drawing.Size(273, 21);
            lSendDate.TabIndex = 5;
            lSendDate.Text = "Дата отправки документа налогоплательщику";
            // 
            // lRecieveDate
            // 
            appearance10.TextHAlignAsString = "Left";
            appearance10.TextVAlignAsString = "Middle";
            lRecieveDate.Appearance = appearance10;
            lRecieveDate.Location = new System.Drawing.Point(12, 93);
            lRecieveDate.Name = "lRecieveDate";
            lRecieveDate.Size = new System.Drawing.Size(273, 21);
            lRecieveDate.TabIndex = 6;
            lRecieveDate.Text = "Дата вручения документа налогоплательщику";
            // 
            // lRecieveMethod
            // 
            appearance1.TextHAlignAsString = "Left";
            appearance1.TextVAlignAsString = "Middle";
            lRecieveMethod.Appearance = appearance1;
            lRecieveMethod.Location = new System.Drawing.Point(12, 120);
            lRecieveMethod.Name = "lRecieveMethod";
            lRecieveMethod.Size = new System.Drawing.Size(273, 21);
            lRecieveMethod.TabIndex = 7;
            lRecieveMethod.Text = "Способ вручения документа";
            // 
            // tbDocNum
            // 
            appearance4.TextHAlignAsString = "Left";
            appearance4.TextVAlignAsString = "Middle";
            this.tbDocNum.Appearance = appearance4;
            this.tbDocNum.Location = new System.Drawing.Point(295, 12);
            this.tbDocNum.Name = "tbDocNum";
            this.tbDocNum.Size = new System.Drawing.Size(100, 21);
            this.tbDocNum.TabIndex = 0;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(320, 149);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "Сохранить";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // deDocDate
            // 
            this.deDocDate.Location = new System.Drawing.Point(295, 39);
            this.deDocDate.Name = "deDocDate";
            this.deDocDate.Size = new System.Drawing.Size(100, 21);
            this.deDocDate.TabIndex = 1;
            // 
            // deSendDate
            // 
            this.deSendDate.Location = new System.Drawing.Point(295, 66);
            this.deSendDate.Name = "deSendDate";
            this.deSendDate.Size = new System.Drawing.Size(100, 21);
            this.deSendDate.TabIndex = 2;
            // 
            // deRecieveDate
            // 
            this.deRecieveDate.Location = new System.Drawing.Point(295, 93);
            this.deRecieveDate.Name = "deRecieveDate";
            this.deRecieveDate.Size = new System.Drawing.Size(100, 21);
            this.deRecieveDate.TabIndex = 3;
            // 
            // ceRecieveMethod
            // 
            this.ceRecieveMethod.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            valueListItem1.DataValue = "1";
            valueListItem1.DisplayText = "1";
            valueListItem2.DataValue = "2";
            valueListItem2.DisplayText = "2";
            valueListItem3.DataValue = "3";
            valueListItem3.DisplayText = "3";
            this.ceRecieveMethod.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2,
            valueListItem3});
            this.ceRecieveMethod.Location = new System.Drawing.Point(295, 120);
            this.ceRecieveMethod.Name = "ceRecieveMethod";
            this.ceRecieveMethod.Size = new System.Drawing.Size(100, 21);
            this.ceRecieveMethod.TabIndex = 4;
            // 
            // DialogEodDoc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(407, 184);
            this.Controls.Add(this.ceRecieveMethod);
            this.Controls.Add(this.deRecieveDate);
            this.Controls.Add(this.deSendDate);
            this.Controls.Add(this.deDocDate);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(lRecieveMethod);
            this.Controls.Add(lRecieveDate);
            this.Controls.Add(lSendDate);
            this.Controls.Add(lDocDate);
            this.Controls.Add(lDocNum);
            this.Controls.Add(this.tbDocNum);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DialogEodDoc";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Параметры для ЭОД-2";
            ((System.ComponentModel.ISupportInitialize)(this.tbDocNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDocDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deSendDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deRecieveDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceRecieveMethod)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.UltraWinEditors.UltraTextEditor tbDocNum;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor deDocDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor deSendDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor deRecieveDate;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor ceRecieveMethod;
    }
}