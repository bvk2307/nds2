﻿namespace EmulatorSEOD.Dialogs
{
    partial class DialogEOD04
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Misc.UltraLabel lSonoCode;
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel lDeclarationRegNumber;
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab6 = new Infragistics.Win.UltraWinTabControl.UltraTab(true);
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab3 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab4 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab5 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab7 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab8 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab9 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab10 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab11 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab12 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            this.ultraTabPageControl6 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.tbSonoCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.tbDeclarationRegNumber = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTabPageControl3 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.udeSuspendDocDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uteSuspendDocNum = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.udeCloseKNP = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uneActReturnTax = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uneActPenalty = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uneActUnpaidTax = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.udeActDateDoc = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uteActNumDoc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTabPageControl4 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uneProsecutePenalty = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uneProsecuteFine = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uneProsecuteAmount = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.udeProsecuteDocDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uteProsecuteDocNum = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTabPageControl5 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uneProsecuteRejectPenalty = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uneProsecuteRejectAmount = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.udeProsecuteRejectDocDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uteProsecuteRejectDocNum = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTabPageControl7 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uneReturnAmount = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.udeReturnDocDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uteReturnDocNum = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTabPageControl8 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uneReturnRejectAmount = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.udeReturnRejectDocDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uteReturnRejectDocNum = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTabPageControl9 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uneReturnDeclarationAmount = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.udeReturnDeclarationDocDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uteReturnDeclarationDocNum = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTabPageControl10 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uneReturnDeclarationRejectAmount = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.udeReturnDeclarationRejectDocDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uteReturnDeclarationRejectDocNum = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTabPageControl11 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uneReturnDeclarationCancelAmount = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.udeReturnDeclarationCancelDocDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uteReturnDeclarationCancelDocNum = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTabPageControl12 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uneReturnDeclarationCancelCorrectionAmount = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.udeReturnDeclarationCancelCorrectionDocDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uteReturnDeclarationCancelCorrectionDocNum = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.upControl = new Infragistics.Win.Misc.UltraPanel();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.upMain = new Infragistics.Win.Misc.UltraPanel();
            this.utcSections = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            lSonoCode = new Infragistics.Win.Misc.UltraLabel();
            lDeclarationRegNumber = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbSonoCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDeclarationRegNumber)).BeginInit();
            this.ultraTabPageControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udeSuspendDocDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uteSuspendDocNum)).BeginInit();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udeCloseKNP)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uneActReturnTax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uneActPenalty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uneActUnpaidTax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udeActDateDoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uteActNumDoc)).BeginInit();
            this.ultraTabPageControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uneProsecutePenalty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uneProsecuteFine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uneProsecuteAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udeProsecuteDocDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uteProsecuteDocNum)).BeginInit();
            this.ultraTabPageControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uneProsecuteRejectPenalty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uneProsecuteRejectAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udeProsecuteRejectDocDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uteProsecuteRejectDocNum)).BeginInit();
            this.ultraTabPageControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uneReturnAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udeReturnDocDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uteReturnDocNum)).BeginInit();
            this.ultraTabPageControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uneReturnRejectAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udeReturnRejectDocDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uteReturnRejectDocNum)).BeginInit();
            this.ultraTabPageControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uneReturnDeclarationAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udeReturnDeclarationDocDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uteReturnDeclarationDocNum)).BeginInit();
            this.ultraTabPageControl10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uneReturnDeclarationRejectAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udeReturnDeclarationRejectDocDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uteReturnDeclarationRejectDocNum)).BeginInit();
            this.ultraTabPageControl11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uneReturnDeclarationCancelAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udeReturnDeclarationCancelDocDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uteReturnDeclarationCancelDocNum)).BeginInit();
            this.ultraTabPageControl12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uneReturnDeclarationCancelCorrectionAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udeReturnDeclarationCancelCorrectionDocDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uteReturnDeclarationCancelCorrectionDocNum)).BeginInit();
            this.upControl.ClientArea.SuspendLayout();
            this.upControl.SuspendLayout();
            this.upMain.ClientArea.SuspendLayout();
            this.upMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.utcSections)).BeginInit();
            this.utcSections.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraTabPageControl6
            // 
            this.ultraTabPageControl6.Controls.Add(lSonoCode);
            this.ultraTabPageControl6.Controls.Add(this.tbSonoCode);
            this.ultraTabPageControl6.Controls.Add(lDeclarationRegNumber);
            this.ultraTabPageControl6.Controls.Add(this.tbDeclarationRegNumber);
            this.ultraTabPageControl6.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl6.Name = "ultraTabPageControl6";
            this.ultraTabPageControl6.Size = new System.Drawing.Size(470, 485);
            // 
            // lSonoCode
            // 
            appearance6.TextHAlignAsString = "Left";
            appearance6.TextVAlignAsString = "Middle";
            lSonoCode.Appearance = appearance6;
            lSonoCode.Location = new System.Drawing.Point(23, 38);
            lSonoCode.Name = "lSonoCode";
            lSonoCode.Size = new System.Drawing.Size(273, 21);
            lSonoCode.TabIndex = 7;
            lSonoCode.Text = "Код ТНО";
            // 
            // tbSonoCode
            // 
            appearance4.TextHAlignAsString = "Left";
            appearance4.TextVAlignAsString = "Middle";
            this.tbSonoCode.Appearance = appearance4;
            this.tbSonoCode.Location = new System.Drawing.Point(306, 38);
            this.tbSonoCode.Name = "tbSonoCode";
            this.tbSonoCode.Size = new System.Drawing.Size(100, 21);
            this.tbSonoCode.TabIndex = 6;
            // 
            // lDeclarationRegNumber
            // 
            appearance24.TextHAlignAsString = "Left";
            appearance24.TextVAlignAsString = "Middle";
            lDeclarationRegNumber.Appearance = appearance24;
            lDeclarationRegNumber.Location = new System.Drawing.Point(23, 11);
            lDeclarationRegNumber.Name = "lDeclarationRegNumber";
            lDeclarationRegNumber.Size = new System.Drawing.Size(273, 21);
            lDeclarationRegNumber.TabIndex = 5;
            lDeclarationRegNumber.Text = "Регистрационный номер декларации в СЭОД";
            // 
            // tbDeclarationRegNumber
            // 
            appearance25.TextHAlignAsString = "Left";
            appearance25.TextVAlignAsString = "Middle";
            this.tbDeclarationRegNumber.Appearance = appearance25;
            this.tbDeclarationRegNumber.Location = new System.Drawing.Point(306, 11);
            this.tbDeclarationRegNumber.Name = "tbDeclarationRegNumber";
            this.tbDeclarationRegNumber.Size = new System.Drawing.Size(100, 21);
            this.tbDeclarationRegNumber.TabIndex = 4;
            // 
            // ultraTabPageControl3
            // 
            this.ultraTabPageControl3.Controls.Add(this.udeSuspendDocDate);
            this.ultraTabPageControl3.Controls.Add(this.uteSuspendDocNum);
            this.ultraTabPageControl3.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl3.Name = "ultraTabPageControl3";
            this.ultraTabPageControl3.Size = new System.Drawing.Size(470, 485);
            // 
            // udeSuspendDocDate
            // 
            appearance2.TextHAlignAsString = "Right";
            this.udeSuspendDocDate.Appearance = appearance2;
            this.udeSuspendDocDate.FormatString = "";
            this.udeSuspendDocDate.Location = new System.Drawing.Point(11, 42);
            this.udeSuspendDocDate.Name = "udeSuspendDocDate";
            this.udeSuspendDocDate.NullText = "Дата документа";
            this.udeSuspendDocDate.Size = new System.Drawing.Size(450, 21);
            this.udeSuspendDocDate.TabIndex = 4;
            this.udeSuspendDocDate.Value = null;
            // 
            // uteSuspendDocNum
            // 
            appearance1.TextHAlignAsString = "Right";
            this.uteSuspendDocNum.Appearance = appearance1;
            this.uteSuspendDocNum.Location = new System.Drawing.Point(11, 15);
            this.uteSuspendDocNum.Name = "uteSuspendDocNum";
            this.uteSuspendDocNum.NullText = "Номер документа";
            this.uteSuspendDocNum.Size = new System.Drawing.Size(450, 21);
            this.uteSuspendDocNum.TabIndex = 3;
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.udeCloseKNP);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(161, 1);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(470, 485);
            // 
            // udeCloseKNP
            // 
            appearance3.TextHAlignAsString = "Right";
            this.udeCloseKNP.Appearance = appearance3;
            this.udeCloseKNP.Location = new System.Drawing.Point(11, 15);
            this.udeCloseKNP.Name = "udeCloseKNP";
            this.udeCloseKNP.NullText = "Дата завершения КНП";
            this.udeCloseKNP.Size = new System.Drawing.Size(450, 21);
            this.udeCloseKNP.TabIndex = 0;
            this.udeCloseKNP.Value = null;
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.uneActReturnTax);
            this.ultraTabPageControl2.Controls.Add(this.uneActPenalty);
            this.ultraTabPageControl2.Controls.Add(this.uneActUnpaidTax);
            this.ultraTabPageControl2.Controls.Add(this.udeActDateDoc);
            this.ultraTabPageControl2.Controls.Add(this.uteActNumDoc);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(470, 485);
            // 
            // uneActReturnTax
            // 
            this.uneActReturnTax.FormatString = "N2";
            this.uneActReturnTax.Location = new System.Drawing.Point(11, 96);
            this.uneActReturnTax.MaskClipMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.Raw;
            this.uneActReturnTax.MinValue = 0D;
            this.uneActReturnTax.Name = "uneActReturnTax";
            this.uneActReturnTax.Nullable = true;
            this.uneActReturnTax.NullText = "Сумма завышения НДС, предъявленного к возмещению";
            this.uneActReturnTax.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Decimal;
            this.uneActReturnTax.Size = new System.Drawing.Size(450, 21);
            this.uneActReturnTax.TabIndex = 7;
            this.uneActReturnTax.Value = null;
            // 
            // uneActPenalty
            // 
            this.uneActPenalty.FormatString = "N2";
            this.uneActPenalty.Location = new System.Drawing.Point(11, 121);
            this.uneActPenalty.MaskClipMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.Raw;
            this.uneActPenalty.MinValue = 0D;
            this.uneActPenalty.Name = "uneActPenalty";
            this.uneActPenalty.Nullable = true;
            this.uneActPenalty.NullText = "Сумма пени";
            this.uneActPenalty.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Decimal;
            this.uneActPenalty.Size = new System.Drawing.Size(450, 21);
            this.uneActPenalty.TabIndex = 6;
            this.uneActPenalty.Value = null;
            // 
            // uneActUnpaidTax
            // 
            this.uneActUnpaidTax.FormatString = "N2";
            this.uneActUnpaidTax.Location = new System.Drawing.Point(11, 69);
            this.uneActUnpaidTax.MaskClipMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.Raw;
            this.uneActUnpaidTax.MinValue = 0D;
            this.uneActUnpaidTax.Name = "uneActUnpaidTax";
            this.uneActUnpaidTax.Nullable = true;
            this.uneActUnpaidTax.NullText = "Сумма неуплаченного (не полностью уплаченного налога (сбора))";
            this.uneActUnpaidTax.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Decimal;
            this.uneActUnpaidTax.Size = new System.Drawing.Size(450, 21);
            this.uneActUnpaidTax.TabIndex = 5;
            this.uneActUnpaidTax.Value = null;
            // 
            // udeActDateDoc
            // 
            appearance22.TextHAlignAsString = "Right";
            this.udeActDateDoc.Appearance = appearance22;
            this.udeActDateDoc.FormatString = "";
            this.udeActDateDoc.Location = new System.Drawing.Point(11, 42);
            this.udeActDateDoc.Name = "udeActDateDoc";
            this.udeActDateDoc.NullText = "Дата документа";
            this.udeActDateDoc.Size = new System.Drawing.Size(450, 21);
            this.udeActDateDoc.TabIndex = 2;
            this.udeActDateDoc.Value = null;
            // 
            // uteActNumDoc
            // 
            appearance7.TextHAlignAsString = "Right";
            this.uteActNumDoc.Appearance = appearance7;
            this.uteActNumDoc.Location = new System.Drawing.Point(11, 15);
            this.uteActNumDoc.Name = "uteActNumDoc";
            this.uteActNumDoc.NullText = "Номер документа";
            this.uteActNumDoc.Size = new System.Drawing.Size(450, 21);
            this.uteActNumDoc.TabIndex = 0;
            // 
            // ultraTabPageControl4
            // 
            this.ultraTabPageControl4.Controls.Add(this.uneProsecutePenalty);
            this.ultraTabPageControl4.Controls.Add(this.uneProsecuteFine);
            this.ultraTabPageControl4.Controls.Add(this.uneProsecuteAmount);
            this.ultraTabPageControl4.Controls.Add(this.udeProsecuteDocDate);
            this.ultraTabPageControl4.Controls.Add(this.uteProsecuteDocNum);
            this.ultraTabPageControl4.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl4.Name = "ultraTabPageControl4";
            this.ultraTabPageControl4.Size = new System.Drawing.Size(470, 485);
            // 
            // uneProsecutePenalty
            // 
            this.uneProsecutePenalty.FormatString = "N2";
            this.uneProsecutePenalty.Location = new System.Drawing.Point(11, 96);
            this.uneProsecutePenalty.MaskClipMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.Raw;
            this.uneProsecutePenalty.MinValue = 0D;
            this.uneProsecutePenalty.Name = "uneProsecutePenalty";
            this.uneProsecutePenalty.Nullable = true;
            this.uneProsecutePenalty.NullText = "Сумма пени";
            this.uneProsecutePenalty.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Decimal;
            this.uneProsecutePenalty.Size = new System.Drawing.Size(450, 21);
            this.uneProsecutePenalty.TabIndex = 12;
            this.uneProsecutePenalty.Value = null;
            // 
            // uneProsecuteFine
            // 
            this.uneProsecuteFine.FormatString = "N2";
            this.uneProsecuteFine.Location = new System.Drawing.Point(11, 121);
            this.uneProsecuteFine.MaskClipMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.Raw;
            this.uneProsecuteFine.MinValue = 0D;
            this.uneProsecuteFine.Name = "uneProsecuteFine";
            this.uneProsecuteFine.Nullable = true;
            this.uneProsecuteFine.NullText = "Сумма штрафа";
            this.uneProsecuteFine.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Decimal;
            this.uneProsecuteFine.Size = new System.Drawing.Size(450, 21);
            this.uneProsecuteFine.TabIndex = 11;
            this.uneProsecuteFine.Value = null;
            // 
            // uneProsecuteAmount
            // 
            this.uneProsecuteAmount.FormatString = "N2";
            this.uneProsecuteAmount.Location = new System.Drawing.Point(11, 69);
            this.uneProsecuteAmount.MaskClipMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.Raw;
            this.uneProsecuteAmount.MinValue = 0D;
            this.uneProsecuteAmount.Name = "uneProsecuteAmount";
            this.uneProsecuteAmount.Nullable = true;
            this.uneProsecuteAmount.NullText = "Сумма неуплаченного (неперечисленного, излишне возмещенного) налога";
            this.uneProsecuteAmount.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Decimal;
            this.uneProsecuteAmount.Size = new System.Drawing.Size(450, 21);
            this.uneProsecuteAmount.TabIndex = 10;
            this.uneProsecuteAmount.Value = null;
            // 
            // udeProsecuteDocDate
            // 
            appearance8.TextHAlignAsString = "Right";
            this.udeProsecuteDocDate.Appearance = appearance8;
            this.udeProsecuteDocDate.FormatString = "";
            this.udeProsecuteDocDate.Location = new System.Drawing.Point(11, 42);
            this.udeProsecuteDocDate.Name = "udeProsecuteDocDate";
            this.udeProsecuteDocDate.NullText = "Дата документа";
            this.udeProsecuteDocDate.Size = new System.Drawing.Size(450, 21);
            this.udeProsecuteDocDate.TabIndex = 9;
            this.udeProsecuteDocDate.Value = null;
            // 
            // uteProsecuteDocNum
            // 
            appearance9.TextHAlignAsString = "Right";
            this.uteProsecuteDocNum.Appearance = appearance9;
            this.uteProsecuteDocNum.Location = new System.Drawing.Point(11, 15);
            this.uteProsecuteDocNum.Name = "uteProsecuteDocNum";
            this.uteProsecuteDocNum.NullText = "Номер документа";
            this.uteProsecuteDocNum.Size = new System.Drawing.Size(450, 21);
            this.uteProsecuteDocNum.TabIndex = 8;
            // 
            // ultraTabPageControl5
            // 
            this.ultraTabPageControl5.Controls.Add(this.uneProsecuteRejectPenalty);
            this.ultraTabPageControl5.Controls.Add(this.uneProsecuteRejectAmount);
            this.ultraTabPageControl5.Controls.Add(this.udeProsecuteRejectDocDate);
            this.ultraTabPageControl5.Controls.Add(this.uteProsecuteRejectDocNum);
            this.ultraTabPageControl5.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl5.Name = "ultraTabPageControl5";
            this.ultraTabPageControl5.Size = new System.Drawing.Size(470, 485);
            // 
            // uneProsecuteRejectPenalty
            // 
            this.uneProsecuteRejectPenalty.FormatString = "N2";
            this.uneProsecuteRejectPenalty.Location = new System.Drawing.Point(11, 96);
            this.uneProsecuteRejectPenalty.MaskClipMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.Raw;
            this.uneProsecuteRejectPenalty.MinValue = 0D;
            this.uneProsecuteRejectPenalty.Name = "uneProsecuteRejectPenalty";
            this.uneProsecuteRejectPenalty.Nullable = true;
            this.uneProsecuteRejectPenalty.NullText = "Сумма пени";
            this.uneProsecuteRejectPenalty.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Decimal;
            this.uneProsecuteRejectPenalty.Size = new System.Drawing.Size(450, 21);
            this.uneProsecuteRejectPenalty.TabIndex = 16;
            this.uneProsecuteRejectPenalty.Value = null;
            // 
            // uneProsecuteRejectAmount
            // 
            this.uneProsecuteRejectAmount.FormatString = "N2";
            this.uneProsecuteRejectAmount.Location = new System.Drawing.Point(11, 69);
            this.uneProsecuteRejectAmount.MaskClipMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.Raw;
            this.uneProsecuteRejectAmount.MinValue = 0D;
            this.uneProsecuteRejectAmount.Name = "uneProsecuteRejectAmount";
            this.uneProsecuteRejectAmount.Nullable = true;
            this.uneProsecuteRejectAmount.NullText = "Сумма неуплаченного (неперечисленного, излишне возмещенного) налога";
            this.uneProsecuteRejectAmount.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Decimal;
            this.uneProsecuteRejectAmount.Size = new System.Drawing.Size(450, 21);
            this.uneProsecuteRejectAmount.TabIndex = 15;
            this.uneProsecuteRejectAmount.Value = null;
            // 
            // udeProsecuteRejectDocDate
            // 
            appearance10.TextHAlignAsString = "Right";
            this.udeProsecuteRejectDocDate.Appearance = appearance10;
            this.udeProsecuteRejectDocDate.FormatString = "";
            this.udeProsecuteRejectDocDate.Location = new System.Drawing.Point(11, 42);
            this.udeProsecuteRejectDocDate.Name = "udeProsecuteRejectDocDate";
            this.udeProsecuteRejectDocDate.NullText = "Дата документа";
            this.udeProsecuteRejectDocDate.Size = new System.Drawing.Size(450, 21);
            this.udeProsecuteRejectDocDate.TabIndex = 14;
            this.udeProsecuteRejectDocDate.Value = null;
            // 
            // uteProsecuteRejectDocNum
            // 
            appearance11.TextHAlignAsString = "Right";
            this.uteProsecuteRejectDocNum.Appearance = appearance11;
            this.uteProsecuteRejectDocNum.Location = new System.Drawing.Point(11, 15);
            this.uteProsecuteRejectDocNum.Name = "uteProsecuteRejectDocNum";
            this.uteProsecuteRejectDocNum.NullText = "Номер документа";
            this.uteProsecuteRejectDocNum.Size = new System.Drawing.Size(450, 21);
            this.uteProsecuteRejectDocNum.TabIndex = 13;
            // 
            // ultraTabPageControl7
            // 
            this.ultraTabPageControl7.Controls.Add(this.uneReturnAmount);
            this.ultraTabPageControl7.Controls.Add(this.udeReturnDocDate);
            this.ultraTabPageControl7.Controls.Add(this.uteReturnDocNum);
            this.ultraTabPageControl7.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl7.Name = "ultraTabPageControl7";
            this.ultraTabPageControl7.Size = new System.Drawing.Size(470, 485);
            // 
            // uneReturnAmount
            // 
            this.uneReturnAmount.FormatString = "N2";
            this.uneReturnAmount.Location = new System.Drawing.Point(11, 69);
            this.uneReturnAmount.MaskClipMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.Raw;
            this.uneReturnAmount.MinValue = 0D;
            this.uneReturnAmount.Name = "uneReturnAmount";
            this.uneReturnAmount.Nullable = true;
            this.uneReturnAmount.NullText = "Сумма НДС, признанная к возмещению";
            this.uneReturnAmount.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Decimal;
            this.uneReturnAmount.Size = new System.Drawing.Size(450, 21);
            this.uneReturnAmount.TabIndex = 18;
            this.uneReturnAmount.Value = null;
            // 
            // udeReturnDocDate
            // 
            appearance12.TextHAlignAsString = "Right";
            this.udeReturnDocDate.Appearance = appearance12;
            this.udeReturnDocDate.FormatString = "";
            this.udeReturnDocDate.Location = new System.Drawing.Point(11, 42);
            this.udeReturnDocDate.Name = "udeReturnDocDate";
            this.udeReturnDocDate.NullText = "Дата документа";
            this.udeReturnDocDate.Size = new System.Drawing.Size(450, 21);
            this.udeReturnDocDate.TabIndex = 17;
            this.udeReturnDocDate.Value = null;
            // 
            // uteReturnDocNum
            // 
            appearance13.TextHAlignAsString = "Right";
            this.uteReturnDocNum.Appearance = appearance13;
            this.uteReturnDocNum.Location = new System.Drawing.Point(11, 15);
            this.uteReturnDocNum.Name = "uteReturnDocNum";
            this.uteReturnDocNum.NullText = "Номер документа";
            this.uteReturnDocNum.Size = new System.Drawing.Size(450, 21);
            this.uteReturnDocNum.TabIndex = 16;
            // 
            // ultraTabPageControl8
            // 
            this.ultraTabPageControl8.Controls.Add(this.uneReturnRejectAmount);
            this.ultraTabPageControl8.Controls.Add(this.udeReturnRejectDocDate);
            this.ultraTabPageControl8.Controls.Add(this.uteReturnRejectDocNum);
            this.ultraTabPageControl8.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl8.Name = "ultraTabPageControl8";
            this.ultraTabPageControl8.Size = new System.Drawing.Size(470, 485);
            // 
            // uneReturnRejectAmount
            // 
            this.uneReturnRejectAmount.FormatString = "N2";
            this.uneReturnRejectAmount.Location = new System.Drawing.Point(11, 69);
            this.uneReturnRejectAmount.MaskClipMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.Raw;
            this.uneReturnRejectAmount.MinValue = 0D;
            this.uneReturnRejectAmount.Name = "uneReturnRejectAmount";
            this.uneReturnRejectAmount.Nullable = true;
            this.uneReturnRejectAmount.NullText = "Сумма НДС, в возмещении которой отказано";
            this.uneReturnRejectAmount.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Decimal;
            this.uneReturnRejectAmount.Size = new System.Drawing.Size(450, 21);
            this.uneReturnRejectAmount.TabIndex = 21;
            this.uneReturnRejectAmount.Value = null;
            // 
            // udeReturnRejectDocDate
            // 
            appearance14.TextHAlignAsString = "Right";
            this.udeReturnRejectDocDate.Appearance = appearance14;
            this.udeReturnRejectDocDate.FormatString = "";
            this.udeReturnRejectDocDate.Location = new System.Drawing.Point(11, 42);
            this.udeReturnRejectDocDate.Name = "udeReturnRejectDocDate";
            this.udeReturnRejectDocDate.NullText = "Дата документа";
            this.udeReturnRejectDocDate.Size = new System.Drawing.Size(450, 21);
            this.udeReturnRejectDocDate.TabIndex = 20;
            this.udeReturnRejectDocDate.Value = null;
            // 
            // uteReturnRejectDocNum
            // 
            appearance15.TextHAlignAsString = "Right";
            this.uteReturnRejectDocNum.Appearance = appearance15;
            this.uteReturnRejectDocNum.Location = new System.Drawing.Point(11, 15);
            this.uteReturnRejectDocNum.Name = "uteReturnRejectDocNum";
            this.uteReturnRejectDocNum.NullText = "Номер документа";
            this.uteReturnRejectDocNum.Size = new System.Drawing.Size(450, 21);
            this.uteReturnRejectDocNum.TabIndex = 19;
            // 
            // ultraTabPageControl9
            // 
            this.ultraTabPageControl9.Controls.Add(this.uneReturnDeclarationAmount);
            this.ultraTabPageControl9.Controls.Add(this.udeReturnDeclarationDocDate);
            this.ultraTabPageControl9.Controls.Add(this.uteReturnDeclarationDocNum);
            this.ultraTabPageControl9.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl9.Name = "ultraTabPageControl9";
            this.ultraTabPageControl9.Size = new System.Drawing.Size(470, 485);
            // 
            // uneReturnDeclarationAmount
            // 
            this.uneReturnDeclarationAmount.FormatString = "N2";
            this.uneReturnDeclarationAmount.Location = new System.Drawing.Point(11, 69);
            this.uneReturnDeclarationAmount.MaskClipMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.Raw;
            this.uneReturnDeclarationAmount.MinValue = 0D;
            this.uneReturnDeclarationAmount.Name = "uneReturnDeclarationAmount";
            this.uneReturnDeclarationAmount.Nullable = true;
            this.uneReturnDeclarationAmount.NullText = "Сумма НДС,возмещаяемая по данному решению";
            this.uneReturnDeclarationAmount.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Decimal;
            this.uneReturnDeclarationAmount.Size = new System.Drawing.Size(450, 21);
            this.uneReturnDeclarationAmount.TabIndex = 24;
            this.uneReturnDeclarationAmount.Value = null;
            // 
            // udeReturnDeclarationDocDate
            // 
            appearance16.TextHAlignAsString = "Right";
            this.udeReturnDeclarationDocDate.Appearance = appearance16;
            this.udeReturnDeclarationDocDate.FormatString = "";
            this.udeReturnDeclarationDocDate.Location = new System.Drawing.Point(11, 42);
            this.udeReturnDeclarationDocDate.Name = "udeReturnDeclarationDocDate";
            this.udeReturnDeclarationDocDate.NullText = "Дата документа";
            this.udeReturnDeclarationDocDate.Size = new System.Drawing.Size(450, 21);
            this.udeReturnDeclarationDocDate.TabIndex = 23;
            this.udeReturnDeclarationDocDate.Value = null;
            // 
            // uteReturnDeclarationDocNum
            // 
            appearance17.TextHAlignAsString = "Right";
            this.uteReturnDeclarationDocNum.Appearance = appearance17;
            this.uteReturnDeclarationDocNum.Location = new System.Drawing.Point(11, 15);
            this.uteReturnDeclarationDocNum.Name = "uteReturnDeclarationDocNum";
            this.uteReturnDeclarationDocNum.NullText = "Номер документа";
            this.uteReturnDeclarationDocNum.Size = new System.Drawing.Size(450, 21);
            this.uteReturnDeclarationDocNum.TabIndex = 22;
            // 
            // ultraTabPageControl10
            // 
            this.ultraTabPageControl10.Controls.Add(this.uneReturnDeclarationRejectAmount);
            this.ultraTabPageControl10.Controls.Add(this.udeReturnDeclarationRejectDocDate);
            this.ultraTabPageControl10.Controls.Add(this.uteReturnDeclarationRejectDocNum);
            this.ultraTabPageControl10.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl10.Name = "ultraTabPageControl10";
            this.ultraTabPageControl10.Size = new System.Drawing.Size(470, 485);
            // 
            // uneReturnDeclarationRejectAmount
            // 
            this.uneReturnDeclarationRejectAmount.FormatString = "N2";
            this.uneReturnDeclarationRejectAmount.Location = new System.Drawing.Point(11, 69);
            this.uneReturnDeclarationRejectAmount.MaskClipMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.Raw;
            this.uneReturnDeclarationRejectAmount.MinValue = 0D;
            this.uneReturnDeclarationRejectAmount.Name = "uneReturnDeclarationRejectAmount";
            this.uneReturnDeclarationRejectAmount.Nullable = true;
            this.uneReturnDeclarationRejectAmount.NullText = "Сумма НДС, отказанная в возмещении по данному решению";
            this.uneReturnDeclarationRejectAmount.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Decimal;
            this.uneReturnDeclarationRejectAmount.Size = new System.Drawing.Size(450, 21);
            this.uneReturnDeclarationRejectAmount.TabIndex = 27;
            this.uneReturnDeclarationRejectAmount.Value = null;
            // 
            // udeReturnDeclarationRejectDocDate
            // 
            appearance18.TextHAlignAsString = "Right";
            this.udeReturnDeclarationRejectDocDate.Appearance = appearance18;
            this.udeReturnDeclarationRejectDocDate.FormatString = "";
            this.udeReturnDeclarationRejectDocDate.Location = new System.Drawing.Point(11, 42);
            this.udeReturnDeclarationRejectDocDate.Name = "udeReturnDeclarationRejectDocDate";
            this.udeReturnDeclarationRejectDocDate.NullText = "Дата документа";
            this.udeReturnDeclarationRejectDocDate.Size = new System.Drawing.Size(450, 21);
            this.udeReturnDeclarationRejectDocDate.TabIndex = 26;
            this.udeReturnDeclarationRejectDocDate.Value = null;
            // 
            // uteReturnDeclarationRejectDocNum
            // 
            appearance19.TextHAlignAsString = "Right";
            this.uteReturnDeclarationRejectDocNum.Appearance = appearance19;
            this.uteReturnDeclarationRejectDocNum.Location = new System.Drawing.Point(11, 15);
            this.uteReturnDeclarationRejectDocNum.Name = "uteReturnDeclarationRejectDocNum";
            this.uteReturnDeclarationRejectDocNum.NullText = "Номер документа";
            this.uteReturnDeclarationRejectDocNum.Size = new System.Drawing.Size(450, 21);
            this.uteReturnDeclarationRejectDocNum.TabIndex = 25;
            // 
            // ultraTabPageControl11
            // 
            this.ultraTabPageControl11.Controls.Add(this.uneReturnDeclarationCancelAmount);
            this.ultraTabPageControl11.Controls.Add(this.udeReturnDeclarationCancelDocDate);
            this.ultraTabPageControl11.Controls.Add(this.uteReturnDeclarationCancelDocNum);
            this.ultraTabPageControl11.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl11.Name = "ultraTabPageControl11";
            this.ultraTabPageControl11.Size = new System.Drawing.Size(470, 485);
            // 
            // uneReturnDeclarationCancelAmount
            // 
            this.uneReturnDeclarationCancelAmount.FormatString = "N2";
            this.uneReturnDeclarationCancelAmount.Location = new System.Drawing.Point(11, 69);
            this.uneReturnDeclarationCancelAmount.MaskClipMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.Raw;
            this.uneReturnDeclarationCancelAmount.MinValue = 0D;
            this.uneReturnDeclarationCancelAmount.Name = "uneReturnDeclarationCancelAmount";
            this.uneReturnDeclarationCancelAmount.Nullable = true;
            this.uneReturnDeclarationCancelAmount.NullText = "Сумма НДС, на которую отменяется решение о возмещении суммы НДС, заявленной к воз" +
    "мещению в заявительном порядке";
            this.uneReturnDeclarationCancelAmount.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Decimal;
            this.uneReturnDeclarationCancelAmount.Size = new System.Drawing.Size(450, 21);
            this.uneReturnDeclarationCancelAmount.TabIndex = 30;
            this.uneReturnDeclarationCancelAmount.Value = null;
            // 
            // udeReturnDeclarationCancelDocDate
            // 
            appearance20.TextHAlignAsString = "Right";
            this.udeReturnDeclarationCancelDocDate.Appearance = appearance20;
            this.udeReturnDeclarationCancelDocDate.FormatString = "";
            this.udeReturnDeclarationCancelDocDate.Location = new System.Drawing.Point(11, 42);
            this.udeReturnDeclarationCancelDocDate.Name = "udeReturnDeclarationCancelDocDate";
            this.udeReturnDeclarationCancelDocDate.NullText = "Дата документа";
            this.udeReturnDeclarationCancelDocDate.Size = new System.Drawing.Size(450, 21);
            this.udeReturnDeclarationCancelDocDate.TabIndex = 29;
            this.udeReturnDeclarationCancelDocDate.Value = null;
            // 
            // uteReturnDeclarationCancelDocNum
            // 
            appearance21.TextHAlignAsString = "Right";
            this.uteReturnDeclarationCancelDocNum.Appearance = appearance21;
            this.uteReturnDeclarationCancelDocNum.Location = new System.Drawing.Point(11, 15);
            this.uteReturnDeclarationCancelDocNum.Name = "uteReturnDeclarationCancelDocNum";
            this.uteReturnDeclarationCancelDocNum.NullText = "Номер документа";
            this.uteReturnDeclarationCancelDocNum.Size = new System.Drawing.Size(450, 21);
            this.uteReturnDeclarationCancelDocNum.TabIndex = 28;
            // 
            // ultraTabPageControl12
            // 
            this.ultraTabPageControl12.Controls.Add(this.uneReturnDeclarationCancelCorrectionAmount);
            this.ultraTabPageControl12.Controls.Add(this.udeReturnDeclarationCancelCorrectionDocDate);
            this.ultraTabPageControl12.Controls.Add(this.uteReturnDeclarationCancelCorrectionDocNum);
            this.ultraTabPageControl12.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl12.Name = "ultraTabPageControl12";
            this.ultraTabPageControl12.Size = new System.Drawing.Size(470, 485);
            // 
            // uneReturnDeclarationCancelCorrectionAmount
            // 
            this.uneReturnDeclarationCancelCorrectionAmount.FormatString = "N2";
            this.uneReturnDeclarationCancelCorrectionAmount.Location = new System.Drawing.Point(11, 69);
            this.uneReturnDeclarationCancelCorrectionAmount.MaskClipMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.Raw;
            this.uneReturnDeclarationCancelCorrectionAmount.MinValue = 0D;
            this.uneReturnDeclarationCancelCorrectionAmount.Name = "uneReturnDeclarationCancelCorrectionAmount";
            this.uneReturnDeclarationCancelCorrectionAmount.Nullable = true;
            this.uneReturnDeclarationCancelCorrectionAmount.NullText = "Сумма НДС, на которую отменяется решение о возмещении суммы НДС, заявленной к воз" +
    "мещению в заявительном порядке";
            this.uneReturnDeclarationCancelCorrectionAmount.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Decimal;
            this.uneReturnDeclarationCancelCorrectionAmount.Size = new System.Drawing.Size(450, 21);
            this.uneReturnDeclarationCancelCorrectionAmount.TabIndex = 33;
            this.uneReturnDeclarationCancelCorrectionAmount.Value = null;
            // 
            // udeReturnDeclarationCancelCorrectionDocDate
            // 
            appearance23.TextHAlignAsString = "Right";
            this.udeReturnDeclarationCancelCorrectionDocDate.Appearance = appearance23;
            this.udeReturnDeclarationCancelCorrectionDocDate.FormatString = "";
            this.udeReturnDeclarationCancelCorrectionDocDate.Location = new System.Drawing.Point(11, 42);
            this.udeReturnDeclarationCancelCorrectionDocDate.Name = "udeReturnDeclarationCancelCorrectionDocDate";
            this.udeReturnDeclarationCancelCorrectionDocDate.NullText = "Дата документа";
            this.udeReturnDeclarationCancelCorrectionDocDate.Size = new System.Drawing.Size(450, 21);
            this.udeReturnDeclarationCancelCorrectionDocDate.TabIndex = 32;
            this.udeReturnDeclarationCancelCorrectionDocDate.Value = null;
            // 
            // uteReturnDeclarationCancelCorrectionDocNum
            // 
            appearance5.TextHAlignAsString = "Right";
            this.uteReturnDeclarationCancelCorrectionDocNum.Appearance = appearance5;
            this.uteReturnDeclarationCancelCorrectionDocNum.Location = new System.Drawing.Point(11, 15);
            this.uteReturnDeclarationCancelCorrectionDocNum.Name = "uteReturnDeclarationCancelCorrectionDocNum";
            this.uteReturnDeclarationCancelCorrectionDocNum.NullText = "Номер документа";
            this.uteReturnDeclarationCancelCorrectionDocNum.Size = new System.Drawing.Size(450, 21);
            this.uteReturnDeclarationCancelCorrectionDocNum.TabIndex = 31;
            // 
            // upControl
            // 
            // 
            // upControl.ClientArea
            // 
            this.upControl.ClientArea.Controls.Add(this.btnSave);
            this.upControl.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.upControl.Location = new System.Drawing.Point(0, 489);
            this.upControl.Name = "upControl";
            this.upControl.Size = new System.Drawing.Size(634, 47);
            this.upControl.TabIndex = 0;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(547, 12);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "Сохранить";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // upMain
            // 
            // 
            // upMain.ClientArea
            // 
            this.upMain.ClientArea.Controls.Add(this.utcSections);
            this.upMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.upMain.Location = new System.Drawing.Point(0, 0);
            this.upMain.Name = "upMain";
            this.upMain.Size = new System.Drawing.Size(634, 489);
            this.upMain.TabIndex = 1;
            // 
            // utcSections
            // 
            this.utcSections.Controls.Add(this.ultraTabSharedControlsPage1);
            this.utcSections.Controls.Add(this.ultraTabPageControl1);
            this.utcSections.Controls.Add(this.ultraTabPageControl2);
            this.utcSections.Controls.Add(this.ultraTabPageControl3);
            this.utcSections.Controls.Add(this.ultraTabPageControl4);
            this.utcSections.Controls.Add(this.ultraTabPageControl5);
            this.utcSections.Controls.Add(this.ultraTabPageControl7);
            this.utcSections.Controls.Add(this.ultraTabPageControl8);
            this.utcSections.Controls.Add(this.ultraTabPageControl9);
            this.utcSections.Controls.Add(this.ultraTabPageControl10);
            this.utcSections.Controls.Add(this.ultraTabPageControl11);
            this.utcSections.Controls.Add(this.ultraTabPageControl12);
            this.utcSections.Controls.Add(this.ultraTabPageControl6);
            this.utcSections.Dock = System.Windows.Forms.DockStyle.Fill;
            this.utcSections.Location = new System.Drawing.Point(0, 0);
            this.utcSections.Name = "utcSections";
            this.utcSections.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.utcSections.Size = new System.Drawing.Size(634, 489);
            this.utcSections.TabIndex = 0;
            this.utcSections.TabOrientation = Infragistics.Win.UltraWinTabs.TabOrientation.LeftTop;
            ultraTab6.TabPage = this.ultraTabPageControl6;
            ultraTab6.Text = "Сведения о декларации";
            ultraTab3.TabPage = this.ultraTabPageControl3;
            ultraTab3.Text = "РешПриост";
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "СвЗавКНП";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "АктКНП";
            ultraTab4.TabPage = this.ultraTabPageControl4;
            ultraTab4.Text = "РешПривлОтв";
            ultraTab5.TabPage = this.ultraTabPageControl5;
            ultraTab5.Text = "РешОтказПривл";
            ultraTab7.TabPage = this.ultraTabPageControl7;
            ultraTab7.Text = "РешВозмНДС";
            ultraTab8.TabPage = this.ultraTabPageControl8;
            ultraTab8.Text = "РешОтказВозмНДС";
            ultraTab9.TabPage = this.ultraTabPageControl9;
            ultraTab9.Text = "РешВозмНДСЗаявит";
            ultraTab10.TabPage = this.ultraTabPageControl10;
            ultraTab10.Text = "РешОтказВозмНДСЗаявит";
            ultraTab11.TabPage = this.ultraTabPageControl11;
            ultraTab11.Text = "РешОтменВозмНДСЗаявит";
            ultraTab12.TabPage = this.ultraTabPageControl12;
            ultraTab12.Text = "РешОтменВозмНДСУточн";
            this.utcSections.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab6,
            ultraTab3,
            ultraTab1,
            ultraTab2,
            ultraTab4,
            ultraTab5,
            ultraTab7,
            ultraTab8,
            ultraTab9,
            ultraTab10,
            ultraTab11,
            ultraTab12});
            this.utcSections.TextOrientation = Infragistics.Win.UltraWinTabs.TextOrientation.Horizontal;
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(470, 485);
            // 
            // DialogEOD04
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(634, 536);
            this.Controls.Add(this.upMain);
            this.Controls.Add(this.upControl);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DialogEOD04";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Параметры для ЭОД-4";
            this.ultraTabPageControl6.ResumeLayout(false);
            this.ultraTabPageControl6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbSonoCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDeclarationRegNumber)).EndInit();
            this.ultraTabPageControl3.ResumeLayout(false);
            this.ultraTabPageControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udeSuspendDocDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uteSuspendDocNum)).EndInit();
            this.ultraTabPageControl1.ResumeLayout(false);
            this.ultraTabPageControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udeCloseKNP)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            this.ultraTabPageControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uneActReturnTax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uneActPenalty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uneActUnpaidTax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udeActDateDoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uteActNumDoc)).EndInit();
            this.ultraTabPageControl4.ResumeLayout(false);
            this.ultraTabPageControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uneProsecutePenalty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uneProsecuteFine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uneProsecuteAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udeProsecuteDocDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uteProsecuteDocNum)).EndInit();
            this.ultraTabPageControl5.ResumeLayout(false);
            this.ultraTabPageControl5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uneProsecuteRejectPenalty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uneProsecuteRejectAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udeProsecuteRejectDocDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uteProsecuteRejectDocNum)).EndInit();
            this.ultraTabPageControl7.ResumeLayout(false);
            this.ultraTabPageControl7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uneReturnAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udeReturnDocDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uteReturnDocNum)).EndInit();
            this.ultraTabPageControl8.ResumeLayout(false);
            this.ultraTabPageControl8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uneReturnRejectAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udeReturnRejectDocDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uteReturnRejectDocNum)).EndInit();
            this.ultraTabPageControl9.ResumeLayout(false);
            this.ultraTabPageControl9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uneReturnDeclarationAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udeReturnDeclarationDocDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uteReturnDeclarationDocNum)).EndInit();
            this.ultraTabPageControl10.ResumeLayout(false);
            this.ultraTabPageControl10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uneReturnDeclarationRejectAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udeReturnDeclarationRejectDocDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uteReturnDeclarationRejectDocNum)).EndInit();
            this.ultraTabPageControl11.ResumeLayout(false);
            this.ultraTabPageControl11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uneReturnDeclarationCancelAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udeReturnDeclarationCancelDocDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uteReturnDeclarationCancelDocNum)).EndInit();
            this.ultraTabPageControl12.ResumeLayout(false);
            this.ultraTabPageControl12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uneReturnDeclarationCancelCorrectionAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udeReturnDeclarationCancelCorrectionDocDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uteReturnDeclarationCancelCorrectionDocNum)).EndInit();
            this.upControl.ClientArea.ResumeLayout(false);
            this.upControl.ResumeLayout(false);
            this.upMain.ClientArea.ResumeLayout(false);
            this.upMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.utcSections)).EndInit();
            this.utcSections.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraPanel upControl;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.Misc.UltraPanel upMain;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl utcSections;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udeCloseKNP;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uteActNumDoc;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udeActDateDoc;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uneActReturnTax;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uneActPenalty;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uneActUnpaidTax;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl3;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udeSuspendDocDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uteSuspendDocNum;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl4;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl5;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl7;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl8;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl9;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl10;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl11;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl12;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uneProsecutePenalty;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uneProsecuteFine;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uneProsecuteAmount;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udeProsecuteDocDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uteProsecuteDocNum;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uneProsecuteRejectPenalty;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uneProsecuteRejectAmount;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udeProsecuteRejectDocDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uteProsecuteRejectDocNum;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uneReturnAmount;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udeReturnDocDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uteReturnDocNum;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uneReturnRejectAmount;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udeReturnRejectDocDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uteReturnRejectDocNum;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uneReturnDeclarationAmount;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udeReturnDeclarationDocDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uteReturnDeclarationDocNum;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uneReturnDeclarationRejectAmount;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udeReturnDeclarationRejectDocDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uteReturnDeclarationRejectDocNum;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uneReturnDeclarationCancelAmount;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udeReturnDeclarationCancelDocDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uteReturnDeclarationCancelDocNum;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uneReturnDeclarationCancelCorrectionAmount;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor udeReturnDeclarationCancelCorrectionDocDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uteReturnDeclarationCancelCorrectionDocNum;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl6;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor tbDeclarationRegNumber;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor tbSonoCode;
    }
}