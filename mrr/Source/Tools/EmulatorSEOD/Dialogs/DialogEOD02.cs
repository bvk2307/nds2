﻿using System;

namespace EmulatorSEOD.Dialogs
{
    /// <summary>
    /// Диалог для отправки ЭОД-2
    /// </summary>
    public class DialogEOD02 : DialogEodDoc
    {
        protected override string GetDialogCaptionText()
        {
            return "Параметры для ЭОД-2";
        }

        protected override string GetDocNumText()
        {
            return "Номер требования о предоставлении пояснений";
        }

        protected override string GetDocDateText()
        {
            return "Дата формирования документа";
        }

        protected override string GetDocSendDateText()
        {
            return "Дата отправки документа налогоплательщику";
        }

        protected override string GetDocRecieveDateText()
        {
            return "Дата вручения документа налогоплательщику";
        }

        protected override string GetDocRecieveMethodText()
        {
            return "Способ вручения документа";
        }

        protected override bool ValidateData(out string ErrorMessage)
        {
            ErrorMessage = String.Empty;

            if (!Data.MultiSelectionMode
                && (string.IsNullOrEmpty(Data.NumDoc) || Data.DateDoc == null))
                ErrorMessage = "Номер и дата документа должны быть заполнены";

            if (Data.MultiSelectionMode
                && Data.DateDoc == null)
                ErrorMessage = "Даты документов должны быть заполнены";

            return string.IsNullOrEmpty(ErrorMessage);
        }
    }
}
