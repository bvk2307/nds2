﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EmulatorSEOD.Dialogs
{
    public partial class DialogEOD09 : Form
    {
        public sealed class Model
        {
            private readonly DialogEOD09 _owner;

            public string NumDoc
            {
                get { return _owner.tbDocNum.Text; }
                set { _owner.tbDocNum.Text = value; }
            }

            public DateTime? RegisterDate
            {
                get { return _owner.deRegisterDate.Value == null ? null : (DateTime?)_owner.deRegisterDate.DateTime; }
                set { _owner.deRegisterDate.Value = value == null ? null : ((DateTime?)value); }
            }

            public DateTime? ExecutorSendDate
            {
                get { return _owner.deExecutorSendDate.Value == null ? null : (DateTime?)_owner.deExecutorSendDate.DateTime; }
                set { _owner.deExecutorSendDate.Value = value == null ? null : ((DateTime?)value); }
            }

            public DateTime? ExecutorRecieveDate
            {
                get { return _owner.deExecutorRecieveDate.Value == null ? null : (DateTime?)_owner.deExecutorRecieveDate.DateTime; }
                set { _owner.deExecutorRecieveDate.Value = value == null ? null : ((DateTime?)value); }
            }

            public string RejectReason
            {
                get { return _owner.ceRejectReason.Text; }
                set { _owner.ceRejectReason.Text = value; }
            }

            public Model(DialogEOD09 owner)
            {
                _owner = owner;
                DefaultValuesInit();
            }

            private void DefaultValuesInit()
            {
                RegisterDate = ExecutorSendDate = ExecutorRecieveDate = null;
            }
        }

        public Model Data { get; private set; }

        public DialogEOD09(DataRow row)
        {
            InitializeComponent();

            Data = new DialogEOD09.Model(this)
            {
                //NumDoc = row["external_doc_num"].ToString(),
                //DateDoc = AsDate(row["external_doc_date"]),
                //SendDate = AsDate(row["tax_payer_send_date"]),
                //RecieveDate = AsDate(row["tax_payer_delivery_date"]),
                //RecieveMethod = row["tax_payer_delivery_method"].ToString()
            };
            EnableInit();
        }

        private void EnableInit()
        {
            tbDocNum.Enabled = string.IsNullOrEmpty(tbDocNum.Text);
            deRegisterDate.Enabled = deRegisterDate.Value == null;
            deExecutorSendDate.Enabled = deExecutorSendDate.Value == null;
            deExecutorRecieveDate.Enabled = deExecutorRecieveDate.Value == null;
            ceRejectReason.Enabled = ceRejectReason.SelectedIndex == -1;
        }

        private DateTime? AsDate(object value)
        {
            if (value == null || value == DBNull.Value || !(value is DateTime) || !(value is DateTime?))
            {
                return null;
            }
            return (DateTime?)value;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            Close();
        }
    }
}