﻿namespace EmulatorSEOD.Dialogs
{
    partial class DialogEOD07
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Misc.UltraLabel lDocNum;
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel lDocDate;
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel lProlongDate;
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            this.tbDocNum = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.deDocDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.deProlongDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            lDocNum = new Infragistics.Win.Misc.UltraLabel();
            lDocDate = new Infragistics.Win.Misc.UltraLabel();
            lProlongDate = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.tbDocNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDocDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deProlongDate)).BeginInit();
            this.SuspendLayout();
            // 
            // lDocNum
            // 
            lDocNum.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance6.TextHAlignAsString = "Left";
            appearance6.TextVAlignAsString = "Middle";
            lDocNum.Appearance = appearance6;
            lDocNum.Location = new System.Drawing.Point(12, 12);
            lDocNum.Name = "lDocNum";
            lDocNum.Size = new System.Drawing.Size(358, 21);
            lDocNum.TabIndex = 3;
            lDocNum.Text = "Номер Решения о продлении сроков представлении документов";
            // 
            // lDocDate
            // 
            lDocDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance7.TextHAlignAsString = "Left";
            appearance7.TextVAlignAsString = "Middle";
            lDocDate.Appearance = appearance7;
            lDocDate.Location = new System.Drawing.Point(12, 39);
            lDocDate.Name = "lDocDate";
            lDocDate.Size = new System.Drawing.Size(358, 21);
            lDocDate.TabIndex = 4;
            lDocDate.Text = "Дата регистрации документа";
            // 
            // lProlongDate
            // 
            lProlongDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance8.TextHAlignAsString = "Left";
            appearance8.TextVAlignAsString = "Middle";
            lProlongDate.Appearance = appearance8;
            lProlongDate.Location = new System.Drawing.Point(12, 66);
            lProlongDate.Name = "lProlongDate";
            lProlongDate.Size = new System.Drawing.Size(358, 21);
            lProlongDate.TabIndex = 5;
            lProlongDate.Text = "Дата, до которой продлен срок представления документов";
            // 
            // tbDocNum
            // 
            this.tbDocNum.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance4.TextHAlignAsString = "Left";
            appearance4.TextVAlignAsString = "Middle";
            this.tbDocNum.Appearance = appearance4;
            this.tbDocNum.Location = new System.Drawing.Point(380, 12);
            this.tbDocNum.Name = "tbDocNum";
            this.tbDocNum.Size = new System.Drawing.Size(100, 21);
            this.tbDocNum.TabIndex = 0;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(405, 93);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 9;
            this.btnSave.Text = "Сохранить";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // deDocDate
            // 
            this.deDocDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.deDocDate.Location = new System.Drawing.Point(380, 39);
            this.deDocDate.Name = "deDocDate";
            this.deDocDate.Size = new System.Drawing.Size(100, 21);
            this.deDocDate.TabIndex = 10;
            // 
            // deProlongDate
            // 
            this.deProlongDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.deProlongDate.Location = new System.Drawing.Point(380, 66);
            this.deProlongDate.Name = "deProlongDate";
            this.deProlongDate.Size = new System.Drawing.Size(100, 21);
            this.deProlongDate.TabIndex = 11;
            // 
            // DialogEOD07
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(492, 128);
            this.Controls.Add(this.deProlongDate);
            this.Controls.Add(this.deDocDate);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(lProlongDate);
            this.Controls.Add(lDocDate);
            this.Controls.Add(lDocNum);
            this.Controls.Add(this.tbDocNum);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DialogEOD07";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Параметры для ЭОД-7";
            ((System.ComponentModel.ISupportInitialize)(this.tbDocNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDocDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deProlongDate)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.UltraWinEditors.UltraTextEditor tbDocNum;
        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor deDocDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor deProlongDate;
    }
}