﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EmulatorSEOD.Dialogs
{
    public partial class DialogEOD10 : Form
    {
        public sealed class Model
        {
            private readonly DialogEOD10 _owner;

            public DateTime? ExecutorRecieveDate
            {
                get { return _owner.deExecutorRecieveDate.Value == null ? null : (DateTime?)_owner.deExecutorRecieveDate.DateTime; }
                set { _owner.deExecutorRecieveDate.Value = value == null ? null : ((DateTime?)value); }
            }

            public DateTime? InitiatorSendDate
            {
                get { return _owner.deInitiatorSendDate.Value == null ? null : (DateTime?)_owner.deInitiatorSendDate.DateTime; }
                set { _owner.deInitiatorSendDate.Value = value == null ? null : ((DateTime?)value); }
            }

            public DateTime? TaxpayerReplyDate
            {
                get { return _owner.deTaxpayerReplyDate.Value == null ? null : (DateTime?)_owner.deTaxpayerReplyDate.DateTime; }
                set { _owner.deTaxpayerReplyDate.Value = value == null ? null : ((DateTime?)value); }
            }

            public Model(DialogEOD10 owner)
            {
                _owner = owner;
                DefaultValuesInit();
            }

            private void DefaultValuesInit()
            {
                ExecutorRecieveDate = InitiatorSendDate = TaxpayerReplyDate = null;
            }
        }

        public Model Data { get; private set; }

        public DialogEOD10(DataRow row)
        {
            InitializeComponent();

            Data = new DialogEOD10.Model(this)
            {
                //NumDoc = row["external_doc_num"].ToString(),
                //DateDoc = AsDate(row["external_doc_date"]),
                //SendDate = AsDate(row["tax_payer_send_date"]),
                //RecieveDate = AsDate(row["tax_payer_delivery_date"]),
                //RecieveMethod = row["tax_payer_delivery_method"].ToString()
            };
            EnableInit();
        }

        private void EnableInit()
        {
            deExecutorRecieveDate.Enabled = deExecutorRecieveDate.Value == null;
            deInitiatorSendDate.Enabled = deInitiatorSendDate.Value == null;
            deTaxpayerReplyDate.Enabled = deTaxpayerReplyDate.Value == null;
        }

        private DateTime? AsDate(object value)
        {
            if (value == null || value == DBNull.Value || !(value is DateTime) || !(value is DateTime?))
            {
                return null;
            }
            return (DateTime?)value;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            Close();
        }
    }
}