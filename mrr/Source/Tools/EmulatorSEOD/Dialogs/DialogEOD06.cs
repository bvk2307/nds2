﻿namespace EmulatorSEOD.Dialogs
{
    /// <summary>
    /// Диалог для отправки ЭОД-6
    /// </summary>
    public class DialogEOD06 : DialogEodDoc
    {
        protected override string GetDialogCaptionText()
        {
            return "Параметры для ЭОД-6";
        }

        protected override string GetDocNumText()
        {
            return "Номер требования о представлении  документов";
        }

        protected override string GetDocDateText()
        {
            return "Дата регистрации документа";
        }

        protected override string GetDocSendDateText()
        {
            return "Дата отправки документа налогоплательщику";
        }

        protected override string GetDocRecieveDateText()
        {
            return "Дата вручения документа налогоплательщику";
        }

        protected override string GetDocRecieveMethodText()
        {
            return "Способ вручения документа";
        }

    }
}
