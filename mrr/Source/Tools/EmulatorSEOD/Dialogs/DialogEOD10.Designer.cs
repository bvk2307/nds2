﻿namespace EmulatorSEOD.Dialogs
{
    partial class DialogEOD10
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Misc.UltraLabel lExecutorRecieveDate;
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel lInitiatorSendDate;
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel lTaxpayerReplyDate;
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.deExecutorRecieveDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.deInitiatorSendDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.deTaxpayerReplyDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            lExecutorRecieveDate = new Infragistics.Win.Misc.UltraLabel();
            lInitiatorSendDate = new Infragistics.Win.Misc.UltraLabel();
            lTaxpayerReplyDate = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.deExecutorRecieveDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deInitiatorSendDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deTaxpayerReplyDate)).BeginInit();
            this.SuspendLayout();
            // 
            // lExecutorRecieveDate
            // 
            lExecutorRecieveDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance7.TextHAlignAsString = "Left";
            appearance7.TextVAlignAsString = "Middle";
            lExecutorRecieveDate.Appearance = appearance7;
            lExecutorRecieveDate.Location = new System.Drawing.Point(12, 12);
            lExecutorRecieveDate.Name = "lExecutorRecieveDate";
            lExecutorRecieveDate.Size = new System.Drawing.Size(298, 21);
            lExecutorRecieveDate.TabIndex = 4;
            lExecutorRecieveDate.Text = "Ответ НП получен от НП НО-исполнителем";
            // 
            // lInitiatorSendDate
            // 
            lInitiatorSendDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance8.TextHAlignAsString = "Left";
            appearance8.TextVAlignAsString = "Middle";
            lInitiatorSendDate.Appearance = appearance8;
            lInitiatorSendDate.Location = new System.Drawing.Point(12, 39);
            lInitiatorSendDate.Name = "lInitiatorSendDate";
            lInitiatorSendDate.Size = new System.Drawing.Size(298, 21);
            lInitiatorSendDate.TabIndex = 5;
            lInitiatorSendDate.Text = "Ответ НП направлен в НО инициатор";
            // 
            // lTaxpayerReplyDate
            // 
            lTaxpayerReplyDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance10.TextHAlignAsString = "Left";
            appearance10.TextVAlignAsString = "Middle";
            lTaxpayerReplyDate.Appearance = appearance10;
            lTaxpayerReplyDate.Location = new System.Drawing.Point(12, 66);
            lTaxpayerReplyDate.Name = "lTaxpayerReplyDate";
            lTaxpayerReplyDate.Size = new System.Drawing.Size(298, 21);
            lTaxpayerReplyDate.TabIndex = 6;
            lTaxpayerReplyDate.Text = "Ответ НП на Требование о представлении докуменотв";
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(345, 93);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 9;
            this.btnSave.Text = "Сохранить";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // deExecutorRecieveDate
            // 
            this.deExecutorRecieveDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.deExecutorRecieveDate.Location = new System.Drawing.Point(320, 12);
            this.deExecutorRecieveDate.Name = "deExecutorRecieveDate";
            this.deExecutorRecieveDate.Size = new System.Drawing.Size(100, 21);
            this.deExecutorRecieveDate.TabIndex = 10;
            // 
            // deInitiatorSendDate
            // 
            this.deInitiatorSendDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.deInitiatorSendDate.Location = new System.Drawing.Point(320, 39);
            this.deInitiatorSendDate.Name = "deInitiatorSendDate";
            this.deInitiatorSendDate.Size = new System.Drawing.Size(100, 21);
            this.deInitiatorSendDate.TabIndex = 11;
            // 
            // deTaxpayerReplyDate
            // 
            this.deTaxpayerReplyDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.deTaxpayerReplyDate.Location = new System.Drawing.Point(320, 66);
            this.deTaxpayerReplyDate.Name = "deTaxpayerReplyDate";
            this.deTaxpayerReplyDate.Size = new System.Drawing.Size(100, 21);
            this.deTaxpayerReplyDate.TabIndex = 12;
            // 
            // DialogEOD10
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(432, 128);
            this.Controls.Add(this.deTaxpayerReplyDate);
            this.Controls.Add(this.deInitiatorSendDate);
            this.Controls.Add(this.deExecutorRecieveDate);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(lTaxpayerReplyDate);
            this.Controls.Add(lInitiatorSendDate);
            this.Controls.Add(lExecutorRecieveDate);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DialogEOD10";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Параметры для ЭОД-10";
            ((System.ComponentModel.ISupportInitialize)(this.deExecutorRecieveDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deInitiatorSendDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deTaxpayerReplyDate)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor deExecutorRecieveDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor deInitiatorSendDate;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor deTaxpayerReplyDate;
    }
}