﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EmulatorSEOD.Dialogs
{
    public partial class DialogEOD08 : Form
    {
        public sealed class Model
        {
            private readonly DialogEOD08 _owner;

            public DateTime? ReplyDate
            {
                get { return _owner.deReplyDate.Value == null ? null : (DateTime?)_owner.deReplyDate.DateTime; }
                set { _owner.deReplyDate.Value = value == null ? null : ((DateTime?)value); }
            }

            public Model(DialogEOD08 owner)
            {
                _owner = owner;
                DefaultValuesInit();
            }

            private void DefaultValuesInit()
            {
                ReplyDate = null;
            }
        }

        public Model Data { get; private set; }

        public bool AllDisabled
        {
            get { return !deReplyDate.Enabled; }
        }

        public DialogEOD08(DataRow row)
        {
            InitializeComponent();

            Data = new DialogEOD08.Model(this)
            {
                //NumDoc = row["external_doc_num"].ToString(),
                //DateDoc = AsDate(row["external_doc_date"]),
                //SendDate = AsDate(row["tax_payer_send_date"]),
                //RecieveDate = AsDate(row["tax_payer_delivery_date"]),
                //RecieveMethod = row["tax_payer_delivery_method"].ToString()
            };
        }

        private DateTime? AsDate(object value)
        {
            if (value == null || value == DBNull.Value || !(value is DateTime) || !(value is DateTime?))
            {
                return null;
            }
            return (DateTime?)value;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (Data.ReplyDate == null)
            {
                MessageBox.Show("Дата ответа должна быть заполнена", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            this.DialogResult = DialogResult.OK;
            Close();
        }
    }
}