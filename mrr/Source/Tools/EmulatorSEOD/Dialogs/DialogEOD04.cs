﻿using System;
using System.Windows.Forms;

namespace EmulatorSEOD.Dialogs
{
    public partial class DialogEOD04 : Form
    {
        public class Model
        {
            public class DeclarationInfo
            {
                public string RegNumber { get; set; }
                public string SonoCode { get; set; }
            }
            public class CloseKNP
            {
                public DateTime CloseDate { get; set; }
            }

            public class ActKNP
            {
                public string NumDoc { get; set; }

                public DateTime DateDoc { get; set; }

                public decimal? Penalty { get; set; }

                public decimal? ReturnTax { get; set; }

                public decimal? UnpaidTax { get; set; }
            }

            public class SuspendDecision
            {
                public string NumDoc { get; set; }

                public DateTime DateDoc { get; set; }
            }

            public class ProsecuteDecision
            {
                public string NumDoc { get; set; }

                public DateTime DateDoc { get; set; }

                public decimal? UnpaidAmount { get; set; }

                public decimal? PenaltyAmount { get; set; }

                public decimal? FineAmount { get; set; }
            }

            public class ProsecuteRejectDecision
            {
                public string NumDoc { get; set; }

                public DateTime DateDoc { get; set; }

                public decimal? UnpaidAmount { get; set; }

                public decimal? PenaltyAmount { get; set; }
            }

            public class ReturnDecision
            {
                public string NumDoc { get; set; }

                public DateTime DateDoc { get; set; }

                public decimal? Amount { get; set; }
            }

            public class ReturnRejectDecision
            {
                public string NumDoc { get; set; }

                public DateTime DateDoc { get; set; }

                public decimal? Amount { get; set; }
            }

            public class ReturnDeclarationDecision
            {
                public string NumDoc { get; set; }

                public DateTime DateDoc { get; set; }

                public decimal? Amount { get; set; }
            }

            public class ReturnDeclarationRejectDecision
            {
                public string NumDoc { get; set; }

                public DateTime DateDoc { get; set; }

                public decimal? Amount { get; set; }
            }

            public class ReturnDeclarationCancelDecision
            {
                public string NumDoc { get; set; }

                public DateTime DateDoc { get; set; }

                public decimal? Amount { get; set; }
            }

            public class ReturnDeclarationCancelCorrectionDecision
            {
                public string NumDoc { get; set; }

                public DateTime DateDoc { get; set; }

                public decimal? Amount { get; set; }
            }


            public DeclarationInfo Declaration { get; set; }

            public SuspendDecision Suspend { get; set; }

            public CloseKNP Close { get; set; }

            public ActKNP Act { get; set; }

            public ProsecuteDecision Prosecute { get; set; }

            public ProsecuteRejectDecision ProsecuteReject { get; set; }

            public ReturnDecision Return { get; set; }

            public ReturnRejectDecision ReturnReject { get; set; }

            public ReturnDeclarationDecision ReturnDeclaration { get; set; }

            public ReturnDeclarationRejectDecision ReturnDeclarationReject { get; set; }

            public ReturnDeclarationCancelDecision ReturnDeclarationCancel { get; set; }

            public ReturnDeclarationCancelCorrectionDecision ReturnDeclarationCancelCorrection { get; set; }
        }

        public Model Data
        {
            get
            {
                return new Model
                {
                    Declaration = new Model.DeclarationInfo()
                    {
                        RegNumber = tbDeclarationRegNumber.Text,
                        SonoCode = tbSonoCode.Text
                    },
                    Suspend = IsSuspendTabEmpty() ? null : new Model.SuspendDecision
                    {
                        DateDoc = udeSuspendDocDate.DateTime,
                        NumDoc = uteSuspendDocNum.Text
                    },
                    Close = IsCloseTabEmpty() ? null : new Model.CloseKNP
                    {
                        CloseDate = udeCloseKNP.DateTime
                    },
                    Act = IsActTabEmpty() || IsActRequiredEmpty() ? null : new Model.ActKNP
                    {
                        DateDoc = udeActDateDoc.DateTime,
                        NumDoc = uteActNumDoc.Text,
                        Penalty = GetDecimal(uneActPenalty.Value),
                        ReturnTax = GetDecimal(uneActReturnTax.Value),
                        UnpaidTax = GetDecimal(uneActUnpaidTax.Value)
                    },
                    Prosecute = IsProsecuteTabEmpty() ? null : new Model.ProsecuteDecision
                    {
                        DateDoc = udeProsecuteDocDate.DateTime,
                        NumDoc = uteProsecuteDocNum.Text,
                        FineAmount = GetDecimal(uneProsecuteFine.Value),
                        PenaltyAmount = GetDecimal(uneProsecutePenalty.Value),
                        UnpaidAmount = GetDecimal(uneProsecuteAmount.Value)
                    },
                    ProsecuteReject = IsProsecuteRejectTabEmpty() ? null : new Model.ProsecuteRejectDecision
                    {
                        DateDoc = udeProsecuteRejectDocDate.DateTime,
                        NumDoc = uteProsecuteRejectDocNum.Text,
                        UnpaidAmount = GetDecimal(uneProsecuteRejectAmount.Value),
                        PenaltyAmount = GetDecimal(uneProsecuteRejectPenalty.Value)
                    },
                    Return = IsReturnTabEmpty() ? null : new Model.ReturnDecision
                    {
                        DateDoc = udeReturnDocDate.DateTime,
                        NumDoc = uteReturnDocNum.Text,
                        Amount = GetDecimal(uneReturnAmount.Value)
                    },
                    ReturnReject = IsReturnRejectTabEmpty() ? null : new Model.ReturnRejectDecision
                    {
                        DateDoc = udeReturnRejectDocDate.DateTime,
                        NumDoc = uteReturnRejectDocNum.Text,
                        Amount = GetDecimal(uneReturnRejectAmount.Value)
                    },
                    ReturnDeclaration = IsReturnDeclarationTabEmpty() ? null : new Model.ReturnDeclarationDecision
                    {
                        DateDoc = udeReturnDeclarationDocDate.DateTime,
                        NumDoc = uteReturnDeclarationDocNum.Text,
                        Amount = GetDecimal(uneReturnDeclarationAmount.Value)
                    },
                    ReturnDeclarationReject = IsReturnDeclarationRejectTabEmpty() ? null : new Model.ReturnDeclarationRejectDecision
                    {
                        DateDoc = udeReturnDeclarationRejectDocDate.DateTime,
                        NumDoc = uteReturnDeclarationRejectDocNum.Text,
                        Amount = GetDecimal(uneReturnDeclarationRejectAmount.Value)
                    },
                    ReturnDeclarationCancel = IsReturnDeclarationCancelTabEmpty() ? null : new Model.ReturnDeclarationCancelDecision
                    {
                        DateDoc = udeReturnDeclarationCancelDocDate.DateTime,
                        NumDoc = uteReturnDeclarationCancelDocNum.Text,
                        Amount = GetDecimal(uneReturnDeclarationCancelAmount.Value)
                    },
                    ReturnDeclarationCancelCorrection = IsReturnDeclarationCancelCorrectionTabEmpty() ? null : new Model.ReturnDeclarationCancelCorrectionDecision
                    {
                        DateDoc = udeReturnDeclarationCancelCorrectionDocDate.DateTime,
                        NumDoc = uteReturnDeclarationCancelCorrectionDocNum.Text,
                        Amount = GetDecimal(uneReturnDeclarationCancelCorrectionAmount.Value)
                    }
                };
            }
        }

        private bool IsReturnDeclarationCancelCorrectionTabEmpty()
        {
            return udeReturnDeclarationCancelCorrectionDocDate.Value == null
                && uteReturnDeclarationCancelCorrectionDocNum.Value == null
                && uneReturnDeclarationCancelCorrectionAmount.Value == null;
        }

        private bool IsReturnDeclarationCancelTabEmpty()
        {
            return udeReturnDeclarationCancelDocDate.Value == null
                && uteReturnDeclarationCancelDocNum.Value == null
                && uneReturnDeclarationCancelAmount.Value == null;
        }

        private bool IsReturnDeclarationRejectTabEmpty()
        {
            return udeReturnDeclarationRejectDocDate.Value == null
                && uteReturnDeclarationRejectDocNum.Value == null
                && uneReturnDeclarationRejectAmount.Value == null;
        }

        private bool IsReturnDeclarationTabEmpty()
        {
            return udeReturnDeclarationDocDate.Value == null
                && uteReturnDeclarationDocNum.Value == null
                && uneReturnDeclarationAmount.Value == null;
        }

        private bool IsReturnRejectTabEmpty()
        {
            return udeReturnRejectDocDate.Value == null
                && uteReturnRejectDocNum.Value == null
                && uneReturnRejectAmount.Value == null;
        }

        private bool IsReturnTabEmpty()
        {
            return udeReturnDocDate.Value == null
                && uteReturnDocNum.Value == null
                && uneReturnAmount.Value == null;
        }

        private bool IsProsecuteRejectTabEmpty()
        {
            return udeProsecuteRejectDocDate.Value == null
                && uteProsecuteRejectDocNum.Value == null
                && uneProsecuteRejectAmount.Value == null
                && uneProsecuteRejectPenalty.Value == null;
        }

        private bool IsProsecuteTabEmpty()
        {
            return udeProsecuteDocDate.Value == null
                && uteProsecuteDocNum.Value == null
                && uneProsecuteFine.Value == null
                && uneProsecutePenalty.Value == null
                && uneProsecuteAmount.Value == null;
        }

        private bool IsSuspendTabEmpty()
        {
            return udeSuspendDocDate.Value == null
                && uteSuspendDocNum.Value == null;
        }

        private bool IsCloseTabEmpty()
        {
            return udeCloseKNP.Value == null;
        }

        private bool IsActTabEmpty()
        {
            return udeActDateDoc.Value == null
                && uteActNumDoc.Value == null
                && uneActPenalty.Value == null
                && uneActReturnTax.Value == null
                && uneActUnpaidTax.Value == null;
        }

        private bool IsActRequiredEmpty()
        {
            return udeActDateDoc.Value == null || uteActNumDoc.Value == null;
        }

        private bool IsDeclarationRequiredEmpty()
        {
            return String.IsNullOrWhiteSpace(tbDeclarationRegNumber.Text) ||
                    String.IsNullOrWhiteSpace(tbSonoCode.Text);
        }

        private decimal? GetDecimal(object value)
        {
            return value == null ? null : (decimal?)value;
        }

        public DialogEOD04(string declRegNumber, string sonoCode)
        {
            InitializeComponent();

            tbDeclarationRegNumber.Text = declRegNumber;
            tbSonoCode.Text = sonoCode;

            udeCloseKNP.Value = null;

            udeActDateDoc.Value = null;
            uteActNumDoc.Value = null;
            uneActPenalty.Value = null;
            uneActReturnTax.Value = null;
            uneActUnpaidTax.Value = null;

            udeSuspendDocDate.Value = null;
            uteSuspendDocNum.Value = null;

            udeProsecuteDocDate.Value = null;
            uteProsecuteDocNum.Value = null;
            uneProsecuteFine.Value = null;
            uneProsecutePenalty.Value = null;
            uneProsecuteAmount.Value = null;

            udeProsecuteRejectDocDate.Value = null;
            uteProsecuteRejectDocNum.Value = null;
            uneProsecuteRejectAmount.Value = null;
            uneProsecuteRejectPenalty.Value = null;

            udeReturnDocDate.Value = null;
            uteReturnDocNum.Value = null;
            uneReturnAmount.Value = null;

            udeReturnRejectDocDate.Value = null;
            uteReturnRejectDocNum.Value = null;
            uneReturnRejectAmount.Value = null;

            udeReturnDeclarationDocDate.Value = null;
            uteReturnDeclarationDocNum.Value = null;
            uneReturnDeclarationAmount.Value = null;

            udeReturnDeclarationRejectDocDate.Value = null;
            uteReturnDeclarationRejectDocNum.Value = null;
            uneReturnDeclarationRejectAmount.Value = null;

            udeReturnDeclarationCancelDocDate.Value = null;
            uteReturnDeclarationCancelDocNum.Value = null;
            uneReturnDeclarationCancelAmount.Value = null;

            udeReturnDeclarationCancelCorrectionDocDate.Value = null;
            uteReturnDeclarationCancelCorrectionDocNum.Value = null;
            uneReturnDeclarationCancelCorrectionAmount.Value = null;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (IsDeclarationRequiredEmpty())
            {
                var result = MessageBox.Show("В сведениях о декларации не заполнены номер декларации в СЭОД или код ТНО.\r\nХотите исправить ошибку?", "Внимание", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if (result == DialogResult.No)
                    CloseDialog(DialogResult.Ignore);
                return;
            }

            if (!IsActTabEmpty() && IsActRequiredEmpty())
            {
                var result = MessageBox.Show("В акте КНП не заполнена дата или номер документа.\r\nХотите исправить ошибку?", "Внимание", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if (result == DialogResult.No)
                {
                    CloseDialog(DialogResult.Ignore);
                }
                return;
            }
            //TODO Добавить проверки на заполненность обязательных атрибутов остальных вкладок
            CloseDialog(DialogResult.OK);
        }

        private void CloseDialog(DialogResult result)
        {
            DialogResult = result;
            Close();
        }
    }
}