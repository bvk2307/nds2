﻿namespace EmulatorSEOD.Dialogs
{
    partial class DialogEOD14
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Misc.UltraLabel ultraLabel1;
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel _labelSono;
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ultraLabel2;
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel lFiscalYear;
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel lTaxPeriod;
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel lCorrectionNum;
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel lRegNum;
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel lCancelationCause;
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ultraLabel4;
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel lKPP;
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel lINN;
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem7 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem8 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem9 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.tbFileID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this._editSono = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this._cancelationDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.tbFiscalYear = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.tbTaxPeriod = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.tbCorrectionNum = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.tbRegNum = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this._cancelationCause = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this._openPreviousKNP = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.rbIP = new System.Windows.Forms.RadioButton();
            this.rbUL = new System.Windows.Forms.RadioButton();
            this.tbKPP = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.tbINN = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            _labelSono = new Infragistics.Win.Misc.UltraLabel();
            ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            lFiscalYear = new Infragistics.Win.Misc.UltraLabel();
            lTaxPeriod = new Infragistics.Win.Misc.UltraLabel();
            lCorrectionNum = new Infragistics.Win.Misc.UltraLabel();
            lRegNum = new Infragistics.Win.Misc.UltraLabel();
            lCancelationCause = new Infragistics.Win.Misc.UltraLabel();
            ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            lKPP = new Infragistics.Win.Misc.UltraLabel();
            lINN = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.tbFileID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._editSono)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._cancelationDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbFiscalYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTaxPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCorrectionNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbRegNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._cancelationCause)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._openPreviousKNP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbKPP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbINN)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraLabel1
            // 
            appearance15.TextHAlignAsString = "Left";
            appearance15.TextVAlignAsString = "Middle";
            ultraLabel1.Appearance = appearance15;
            ultraLabel1.Location = new System.Drawing.Point(16, 219);
            ultraLabel1.Name = "ultraLabel1";
            ultraLabel1.Size = new System.Drawing.Size(174, 21);
            ultraLabel1.TabIndex = 33;
            ultraLabel1.Text = "Имя файла основного раздела";
            // 
            // _labelSono
            // 
            appearance17.TextHAlignAsString = "Left";
            appearance17.TextVAlignAsString = "Middle";
            _labelSono.Appearance = appearance17;
            _labelSono.Location = new System.Drawing.Point(16, 109);
            _labelSono.Name = "_labelSono";
            _labelSono.Size = new System.Drawing.Size(153, 21);
            _labelSono.TabIndex = 32;
            _labelSono.Text = "Код налогового органа";
            // 
            // ultraLabel2
            // 
            appearance9.TextHAlignAsString = "Left";
            appearance9.TextVAlignAsString = "Middle";
            ultraLabel2.Appearance = appearance9;
            ultraLabel2.Location = new System.Drawing.Point(16, 299);
            ultraLabel2.Name = "ultraLabel2";
            ultraLabel2.Size = new System.Drawing.Size(153, 21);
            ultraLabel2.TabIndex = 40;
            ultraLabel2.Text = "Дата аннулирования";
            // 
            // lFiscalYear
            // 
            appearance21.TextHAlignAsString = "Left";
            appearance21.TextVAlignAsString = "Middle";
            lFiscalYear.Appearance = appearance21;
            lFiscalYear.Location = new System.Drawing.Point(16, 164);
            lFiscalYear.Name = "lFiscalYear";
            lFiscalYear.Size = new System.Drawing.Size(180, 21);
            lFiscalYear.TabIndex = 45;
            lFiscalYear.Text = "Отчетный год";
            // 
            // lTaxPeriod
            // 
            appearance3.TextHAlignAsString = "Left";
            appearance3.TextVAlignAsString = "Middle";
            lTaxPeriod.Appearance = appearance3;
            lTaxPeriod.Location = new System.Drawing.Point(16, 136);
            lTaxPeriod.Name = "lTaxPeriod";
            lTaxPeriod.Size = new System.Drawing.Size(169, 21);
            lTaxPeriod.TabIndex = 43;
            lTaxPeriod.Text = "Налоговый период ";
            // 
            // lCorrectionNum
            // 
            appearance7.TextHAlignAsString = "Left";
            appearance7.TextVAlignAsString = "Middle";
            lCorrectionNum.Appearance = appearance7;
            lCorrectionNum.Location = new System.Drawing.Point(16, 192);
            lCorrectionNum.Name = "lCorrectionNum";
            lCorrectionNum.Size = new System.Drawing.Size(169, 21);
            lCorrectionNum.TabIndex = 47;
            lCorrectionNum.Text = "Номер корректировки";
            // 
            // lRegNum
            // 
            appearance12.TextHAlignAsString = "Left";
            appearance12.TextVAlignAsString = "Middle";
            lRegNum.Appearance = appearance12;
            lRegNum.Location = new System.Drawing.Point(16, 245);
            lRegNum.Name = "lRegNum";
            lRegNum.Size = new System.Drawing.Size(174, 21);
            lRegNum.TabIndex = 49;
            lRegNum.Text = "Регистрационный номер декларации";
            // 
            // lCancelationCause
            // 
            appearance13.TextHAlignAsString = "Left";
            appearance13.TextVAlignAsString = "Middle";
            lCancelationCause.Appearance = appearance13;
            lCancelationCause.Location = new System.Drawing.Point(16, 272);
            lCancelationCause.Name = "lCancelationCause";
            lCancelationCause.Size = new System.Drawing.Size(153, 21);
            lCancelationCause.TabIndex = 52;
            lCancelationCause.Text = "Причина аннулирования";
            // 
            // ultraLabel4
            // 
            appearance4.TextHAlignAsString = "Left";
            appearance4.TextVAlignAsString = "Middle";
            ultraLabel4.Appearance = appearance4;
            ultraLabel4.Location = new System.Drawing.Point(16, 326);
            ultraLabel4.Name = "ultraLabel4";
            ultraLabel4.Size = new System.Drawing.Size(169, 31);
            ultraLabel4.TabIndex = 54;
            ultraLabel4.Text = "Открыть КНП по предыдущей корректировке";
            // 
            // lKPP
            // 
            appearance14.TextHAlignAsString = "Left";
            appearance14.TextVAlignAsString = "Middle";
            lKPP.Appearance = appearance14;
            lKPP.Location = new System.Drawing.Point(16, 66);
            lKPP.Name = "lKPP";
            lKPP.Size = new System.Drawing.Size(300, 21);
            lKPP.TabIndex = 58;
            lKPP.Text = "КПП налогоплательщика";
            // 
            // lINN
            // 
            appearance1.TextHAlignAsString = "Left";
            appearance1.TextVAlignAsString = "Middle";
            lINN.Appearance = appearance1;
            lINN.Location = new System.Drawing.Point(16, 39);
            lINN.Name = "lINN";
            lINN.Size = new System.Drawing.Size(300, 21);
            lINN.TabIndex = 56;
            lINN.Text = "ИНН налогоплательщика, предоставившего журнал";
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(435, 374);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 26;
            this.btnSave.Text = "Сохранить";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // tbFileID
            // 
            appearance19.TextHAlignAsString = "Left";
            appearance19.TextVAlignAsString = "Middle";
            this.tbFileID.Appearance = appearance19;
            this.tbFileID.Location = new System.Drawing.Point(206, 218);
            this.tbFileID.Name = "tbFileID";
            this.tbFileID.Size = new System.Drawing.Size(219, 21);
            this.tbFileID.TabIndex = 34;
            // 
            // _editSono
            // 
            appearance20.TextHAlignAsString = "Left";
            appearance20.TextVAlignAsString = "Middle";
            this._editSono.Appearance = appearance20;
            this._editSono.Location = new System.Drawing.Point(206, 109);
            this._editSono.Name = "_editSono";
            this._editSono.Size = new System.Drawing.Size(219, 21);
            this._editSono.TabIndex = 35;
            // 
            // _cancelationDate
            // 
            this._cancelationDate.Location = new System.Drawing.Point(206, 299);
            this._cancelationDate.Name = "_cancelationDate";
            this._cancelationDate.Size = new System.Drawing.Size(219, 21);
            this._cancelationDate.TabIndex = 39;
            // 
            // tbFiscalYear
            // 
            appearance22.TextHAlignAsString = "Left";
            appearance22.TextVAlignAsString = "Middle";
            this.tbFiscalYear.Appearance = appearance22;
            this.tbFiscalYear.Location = new System.Drawing.Point(206, 164);
            this.tbFiscalYear.Name = "tbFiscalYear";
            this.tbFiscalYear.Size = new System.Drawing.Size(219, 21);
            this.tbFiscalYear.TabIndex = 46;
            // 
            // tbTaxPeriod
            // 
            appearance5.TextHAlignAsString = "Left";
            appearance5.TextVAlignAsString = "Middle";
            this.tbTaxPeriod.Appearance = appearance5;
            this.tbTaxPeriod.Location = new System.Drawing.Point(206, 136);
            this.tbTaxPeriod.Name = "tbTaxPeriod";
            this.tbTaxPeriod.Size = new System.Drawing.Size(219, 21);
            this.tbTaxPeriod.TabIndex = 44;
            // 
            // tbCorrectionNum
            // 
            appearance8.TextHAlignAsString = "Left";
            appearance8.TextVAlignAsString = "Middle";
            this.tbCorrectionNum.Appearance = appearance8;
            this.tbCorrectionNum.Location = new System.Drawing.Point(206, 191);
            this.tbCorrectionNum.Name = "tbCorrectionNum";
            this.tbCorrectionNum.Size = new System.Drawing.Size(219, 21);
            this.tbCorrectionNum.TabIndex = 48;
            // 
            // tbRegNum
            // 
            appearance16.TextHAlignAsString = "Left";
            appearance16.TextVAlignAsString = "Middle";
            this.tbRegNum.Appearance = appearance16;
            this.tbRegNum.Location = new System.Drawing.Point(206, 245);
            this.tbRegNum.Name = "tbRegNum";
            this.tbRegNum.Size = new System.Drawing.Size(219, 21);
            this.tbRegNum.TabIndex = 50;
            // 
            // _cancelationCause
            // 
            this._cancelationCause.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            valueListItem7.DataValue = "1";
            valueListItem7.DisplayText = "1";
            valueListItem8.DataValue = "2";
            valueListItem8.DisplayText = "2";
            valueListItem9.DataValue = "3";
            valueListItem9.DisplayText = "3";
            this._cancelationCause.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem7,
            valueListItem8,
            valueListItem9});
            this._cancelationCause.Location = new System.Drawing.Point(206, 272);
            this._cancelationCause.MaxDropDownItems = 3;
            this._cancelationCause.Name = "_cancelationCause";
            this._cancelationCause.Nullable = false;
            this._cancelationCause.Size = new System.Drawing.Size(219, 21);
            this._cancelationCause.TabIndex = 51;
            // 
            // _openPreviousKNP
            // 
            this._openPreviousKNP.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            valueListItem1.DataValue = "1";
            valueListItem1.DisplayText = "Не открывать";
            valueListItem2.DataValue = "2";
            valueListItem2.DisplayText = "Открыть";
            this._openPreviousKNP.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2});
            this._openPreviousKNP.Location = new System.Drawing.Point(206, 326);
            this._openPreviousKNP.MaxDropDownItems = 3;
            this._openPreviousKNP.Name = "_openPreviousKNP";
            this._openPreviousKNP.Nullable = false;
            this._openPreviousKNP.Size = new System.Drawing.Size(219, 21);
            this._openPreviousKNP.TabIndex = 53;
            // 
            // rbIP
            // 
            this.rbIP.AutoSize = true;
            this.rbIP.Location = new System.Drawing.Point(142, 16);
            this.rbIP.Name = "rbIP";
            this.rbIP.Size = new System.Drawing.Size(116, 17);
            this.rbIP.TabIndex = 60;
            this.rbIP.Text = "Физическое лицо";
            this.rbIP.UseVisualStyleBackColor = true;
            this.rbIP.CheckedChanged += new System.EventHandler(this.rbIP_CheckedChanged);
            // 
            // rbUL
            // 
            this.rbUL.AutoSize = true;
            this.rbUL.Checked = true;
            this.rbUL.Location = new System.Drawing.Point(16, 16);
            this.rbUL.Name = "rbUL";
            this.rbUL.Size = new System.Drawing.Size(120, 17);
            this.rbUL.TabIndex = 59;
            this.rbUL.TabStop = true;
            this.rbUL.Text = "Юридическое лицо";
            this.rbUL.UseVisualStyleBackColor = true;
            // 
            // tbKPP
            // 
            appearance18.TextHAlignAsString = "Left";
            appearance18.TextVAlignAsString = "Middle";
            this.tbKPP.Appearance = appearance18;
            this.tbKPP.Location = new System.Drawing.Point(322, 66);
            this.tbKPP.Name = "tbKPP";
            this.tbKPP.Size = new System.Drawing.Size(160, 21);
            this.tbKPP.TabIndex = 57;
            // 
            // tbINN
            // 
            appearance2.TextHAlignAsString = "Left";
            appearance2.TextVAlignAsString = "Middle";
            this.tbINN.Appearance = appearance2;
            this.tbINN.Location = new System.Drawing.Point(322, 39);
            this.tbINN.Name = "tbINN";
            this.tbINN.Size = new System.Drawing.Size(160, 21);
            this.tbINN.TabIndex = 55;
            // 
            // DialogEOD14
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 409);
            this.Controls.Add(this.rbIP);
            this.Controls.Add(this.rbUL);
            this.Controls.Add(lKPP);
            this.Controls.Add(this.tbKPP);
            this.Controls.Add(lINN);
            this.Controls.Add(this.tbINN);
            this.Controls.Add(ultraLabel4);
            this.Controls.Add(this._openPreviousKNP);
            this.Controls.Add(lCancelationCause);
            this.Controls.Add(this._cancelationCause);
            this.Controls.Add(lRegNum);
            this.Controls.Add(this.tbRegNum);
            this.Controls.Add(lCorrectionNum);
            this.Controls.Add(this.tbCorrectionNum);
            this.Controls.Add(lFiscalYear);
            this.Controls.Add(this.tbFiscalYear);
            this.Controls.Add(lTaxPeriod);
            this.Controls.Add(this.tbTaxPeriod);
            this.Controls.Add(this._cancelationDate);
            this.Controls.Add(ultraLabel2);
            this.Controls.Add(this._editSono);
            this.Controls.Add(this.tbFileID);
            this.Controls.Add(ultraLabel1);
            this.Controls.Add(_labelSono);
            this.Controls.Add(this.btnSave);
            this.Name = "DialogEOD14";
            this.Text = "DialogEOD14";
            ((System.ComponentModel.ISupportInitialize)(this.tbFileID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._editSono)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._cancelationDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbFiscalYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTaxPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCorrectionNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbRegNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._cancelationCause)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._openPreviousKNP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbKPP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbINN)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor tbFileID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor _editSono;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor _cancelationDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor tbFiscalYear;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor tbTaxPeriod;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor tbCorrectionNum;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor tbRegNum;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor _cancelationCause;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor _openPreviousKNP;
        private System.Windows.Forms.RadioButton rbIP;
        private System.Windows.Forms.RadioButton rbUL;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor tbKPP;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor tbINN;
    }
}