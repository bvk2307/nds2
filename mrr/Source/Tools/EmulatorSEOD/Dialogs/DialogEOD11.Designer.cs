﻿namespace EmulatorSEOD.Dialogs
{
    partial class DialogEOD11
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Misc.UltraLabel lINN;
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel lKPP;
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel lSounCode;
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel lTaxPeriod;
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel lFiscalYear;
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel lRegNum;
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel lFID;
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel lFileID;
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel lTaxpayerFID;
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            this.tbINN = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.tbKPP = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.rbUL = new System.Windows.Forms.RadioButton();
            this.rbIP = new System.Windows.Forms.RadioButton();
            this.tbSounCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.tbTaxPeriod = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.tbFiscalYear = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.tbRegNum = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.tbFID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.tbFileID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.tbTaxpayerFID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            lINN = new Infragistics.Win.Misc.UltraLabel();
            lKPP = new Infragistics.Win.Misc.UltraLabel();
            lSounCode = new Infragistics.Win.Misc.UltraLabel();
            lTaxPeriod = new Infragistics.Win.Misc.UltraLabel();
            lFiscalYear = new Infragistics.Win.Misc.UltraLabel();
            lRegNum = new Infragistics.Win.Misc.UltraLabel();
            lFID = new Infragistics.Win.Misc.UltraLabel();
            lFileID = new Infragistics.Win.Misc.UltraLabel();
            lTaxpayerFID = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.tbINN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbKPP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSounCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTaxPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbFiscalYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbRegNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbFID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbFileID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTaxpayerFID)).BeginInit();
            this.SuspendLayout();
            // 
            // lINN
            // 
            appearance1.TextHAlignAsString = "Left";
            appearance1.TextVAlignAsString = "Middle";
            lINN.Appearance = appearance1;
            lINN.Location = new System.Drawing.Point(12, 35);
            lINN.Name = "lINN";
            lINN.Size = new System.Drawing.Size(300, 21);
            lINN.TabIndex = 5;
            lINN.Text = "ИНН налогоплательщика, предоставившего журнал";
            // 
            // lKPP
            // 
            appearance14.TextHAlignAsString = "Left";
            appearance14.TextVAlignAsString = "Middle";
            lKPP.Appearance = appearance14;
            lKPP.Location = new System.Drawing.Point(12, 62);
            lKPP.Name = "lKPP";
            lKPP.Size = new System.Drawing.Size(300, 21);
            lKPP.TabIndex = 7;
            lKPP.Text = "КПП налогоплательщика";
            // 
            // tbINN
            // 
            appearance2.TextHAlignAsString = "Left";
            appearance2.TextVAlignAsString = "Middle";
            this.tbINN.Appearance = appearance2;
            this.tbINN.Location = new System.Drawing.Point(318, 35);
            this.tbINN.Name = "tbINN";
            this.tbINN.Size = new System.Drawing.Size(160, 21);
            this.tbINN.TabIndex = 4;
            // 
            // tbKPP
            // 
            appearance18.TextHAlignAsString = "Left";
            appearance18.TextVAlignAsString = "Middle";
            this.tbKPP.Appearance = appearance18;
            this.tbKPP.Location = new System.Drawing.Point(318, 62);
            this.tbKPP.Name = "tbKPP";
            this.tbKPP.Size = new System.Drawing.Size(160, 21);
            this.tbKPP.TabIndex = 6;
            // 
            // rbUL
            // 
            this.rbUL.AutoSize = true;
            this.rbUL.Checked = true;
            this.rbUL.Location = new System.Drawing.Point(12, 12);
            this.rbUL.Name = "rbUL";
            this.rbUL.Size = new System.Drawing.Size(120, 17);
            this.rbUL.TabIndex = 8;
            this.rbUL.TabStop = true;
            this.rbUL.Text = "Юридическое лицо";
            this.rbUL.UseVisualStyleBackColor = true;
            this.rbUL.CheckedChanged += new System.EventHandler(this.RadioButton_CheckedChanged);
            // 
            // rbIP
            // 
            this.rbIP.AutoSize = true;
            this.rbIP.Location = new System.Drawing.Point(138, 12);
            this.rbIP.Name = "rbIP";
            this.rbIP.Size = new System.Drawing.Size(116, 17);
            this.rbIP.TabIndex = 9;
            this.rbIP.Text = "Физическое лицо";
            this.rbIP.UseVisualStyleBackColor = true;
            // 
            // lSounCode
            // 
            appearance13.TextHAlignAsString = "Left";
            appearance13.TextVAlignAsString = "Middle";
            lSounCode.Appearance = appearance13;
            lSounCode.Location = new System.Drawing.Point(12, 89);
            lSounCode.Name = "lSounCode";
            lSounCode.Size = new System.Drawing.Size(300, 21);
            lSounCode.TabIndex = 11;
            lSounCode.Text = "Код налогового органа";
            // 
            // tbSounCode
            // 
            appearance17.TextHAlignAsString = "Left";
            appearance17.TextVAlignAsString = "Middle";
            this.tbSounCode.Appearance = appearance17;
            this.tbSounCode.Location = new System.Drawing.Point(318, 89);
            this.tbSounCode.Name = "tbSounCode";
            this.tbSounCode.Size = new System.Drawing.Size(160, 21);
            this.tbSounCode.TabIndex = 10;
            // 
            // lTaxPeriod
            // 
            appearance3.TextHAlignAsString = "Left";
            appearance3.TextVAlignAsString = "Middle";
            lTaxPeriod.Appearance = appearance3;
            lTaxPeriod.Location = new System.Drawing.Point(12, 116);
            lTaxPeriod.Name = "lTaxPeriod";
            lTaxPeriod.Size = new System.Drawing.Size(300, 21);
            lTaxPeriod.TabIndex = 13;
            lTaxPeriod.Text = "Налоговый период ";
            // 
            // tbTaxPeriod
            // 
            appearance5.TextHAlignAsString = "Left";
            appearance5.TextVAlignAsString = "Middle";
            this.tbTaxPeriod.Appearance = appearance5;
            this.tbTaxPeriod.Location = new System.Drawing.Point(318, 116);
            this.tbTaxPeriod.Name = "tbTaxPeriod";
            this.tbTaxPeriod.Size = new System.Drawing.Size(160, 21);
            this.tbTaxPeriod.TabIndex = 12;
            // 
            // lFiscalYear
            // 
            appearance7.TextHAlignAsString = "Left";
            appearance7.TextVAlignAsString = "Middle";
            lFiscalYear.Appearance = appearance7;
            lFiscalYear.Location = new System.Drawing.Point(12, 143);
            lFiscalYear.Name = "lFiscalYear";
            lFiscalYear.Size = new System.Drawing.Size(300, 21);
            lFiscalYear.TabIndex = 15;
            lFiscalYear.Text = "Отчетный год";
            // 
            // tbFiscalYear
            // 
            appearance8.TextHAlignAsString = "Left";
            appearance8.TextVAlignAsString = "Middle";
            this.tbFiscalYear.Appearance = appearance8;
            this.tbFiscalYear.Location = new System.Drawing.Point(318, 143);
            this.tbFiscalYear.Name = "tbFiscalYear";
            this.tbFiscalYear.Size = new System.Drawing.Size(160, 21);
            this.tbFiscalYear.TabIndex = 14;
            // 
            // lRegNum
            // 
            appearance12.TextHAlignAsString = "Left";
            appearance12.TextVAlignAsString = "Middle";
            lRegNum.Appearance = appearance12;
            lRegNum.Location = new System.Drawing.Point(12, 197);
            lRegNum.Name = "lRegNum";
            lRegNum.Size = new System.Drawing.Size(300, 21);
            lRegNum.TabIndex = 17;
            lRegNum.Text = "Регистрационный номер журнала";
            // 
            // tbRegNum
            // 
            appearance16.TextHAlignAsString = "Left";
            appearance16.TextVAlignAsString = "Middle";
            this.tbRegNum.Appearance = appearance16;
            this.tbRegNum.Location = new System.Drawing.Point(318, 197);
            this.tbRegNum.Name = "tbRegNum";
            this.tbRegNum.Size = new System.Drawing.Size(160, 21);
            this.tbRegNum.TabIndex = 16;
            // 
            // lFID
            // 
            appearance11.TextHAlignAsString = "Left";
            appearance11.TextVAlignAsString = "Middle";
            lFID.Appearance = appearance11;
            lFID.Location = new System.Drawing.Point(12, 224);
            lFID.Name = "lFID";
            lFID.Size = new System.Drawing.Size(300, 21);
            lFID.TabIndex = 19;
            lFID.Text = "Учётный номер журнала";
            // 
            // tbFID
            // 
            appearance15.TextHAlignAsString = "Left";
            appearance15.TextVAlignAsString = "Middle";
            this.tbFID.Appearance = appearance15;
            this.tbFID.Location = new System.Drawing.Point(318, 224);
            this.tbFID.Name = "tbFID";
            this.tbFID.Size = new System.Drawing.Size(160, 21);
            this.tbFID.TabIndex = 18;
            // 
            // lFileID
            // 
            appearance10.TextHAlignAsString = "Left";
            appearance10.TextVAlignAsString = "Middle";
            lFileID.Appearance = appearance10;
            lFileID.Location = new System.Drawing.Point(12, 170);
            lFileID.Name = "lFileID";
            lFileID.Size = new System.Drawing.Size(300, 21);
            lFileID.TabIndex = 21;
            lFileID.Text = "Идентификатор файла";
            // 
            // tbFileID
            // 
            appearance9.TextHAlignAsString = "Left";
            appearance9.TextVAlignAsString = "Middle";
            this.tbFileID.Appearance = appearance9;
            this.tbFileID.Location = new System.Drawing.Point(318, 170);
            this.tbFileID.Name = "tbFileID";
            this.tbFileID.Size = new System.Drawing.Size(160, 21);
            this.tbFileID.TabIndex = 20;
            // 
            // lTaxpayerFID
            // 
            appearance6.TextHAlignAsString = "Left";
            appearance6.TextVAlignAsString = "Middle";
            lTaxpayerFID.Appearance = appearance6;
            lTaxpayerFID.Location = new System.Drawing.Point(12, 251);
            lTaxpayerFID.Name = "lTaxpayerFID";
            lTaxpayerFID.Size = new System.Drawing.Size(300, 21);
            lTaxpayerFID.TabIndex = 23;
            lTaxpayerFID.Text = "ФИД налогоплательщика";
            // 
            // tbTaxpayerFID
            // 
            appearance4.TextHAlignAsString = "Left";
            appearance4.TextVAlignAsString = "Middle";
            this.tbTaxpayerFID.Appearance = appearance4;
            this.tbTaxpayerFID.Location = new System.Drawing.Point(318, 251);
            this.tbTaxpayerFID.Name = "tbTaxpayerFID";
            this.tbTaxpayerFID.Size = new System.Drawing.Size(160, 21);
            this.tbTaxpayerFID.TabIndex = 22;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(403, 278);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 24;
            this.btnSave.Text = "Сохранить";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // DialogEOD11
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(490, 313);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(lTaxpayerFID);
            this.Controls.Add(this.tbTaxpayerFID);
            this.Controls.Add(lFileID);
            this.Controls.Add(this.tbFileID);
            this.Controls.Add(lFID);
            this.Controls.Add(this.tbFID);
            this.Controls.Add(lRegNum);
            this.Controls.Add(this.tbRegNum);
            this.Controls.Add(lFiscalYear);
            this.Controls.Add(this.tbFiscalYear);
            this.Controls.Add(lTaxPeriod);
            this.Controls.Add(this.tbTaxPeriod);
            this.Controls.Add(lSounCode);
            this.Controls.Add(this.tbSounCode);
            this.Controls.Add(this.rbIP);
            this.Controls.Add(this.rbUL);
            this.Controls.Add(lKPP);
            this.Controls.Add(this.tbKPP);
            this.Controls.Add(lINN);
            this.Controls.Add(this.tbINN);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DialogEOD11";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Параметры для ЭОД-11";
            ((System.ComponentModel.ISupportInitialize)(this.tbINN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbKPP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSounCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTaxPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbFiscalYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbRegNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbFID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbFileID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTaxpayerFID)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.UltraWinEditors.UltraTextEditor tbINN;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor tbKPP;
        private System.Windows.Forms.RadioButton rbUL;
        private System.Windows.Forms.RadioButton rbIP;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor tbSounCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor tbTaxPeriod;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor tbFiscalYear;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor tbRegNum;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor tbFID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor tbFileID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor tbTaxpayerFID;
        private Infragistics.Win.Misc.UltraButton btnSave;
    }
}