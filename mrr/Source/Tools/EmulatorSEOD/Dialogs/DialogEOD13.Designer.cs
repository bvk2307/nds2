﻿namespace EmulatorSEOD.Dialogs
{
    partial class DialogEOD13
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Misc.UltraLabel _labelRegNumber;
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel _labelSono;
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ultraLabel1;
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel ultraLabel2;
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Misc.UltraLabel ultraLabel3;
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this._editRegNumber = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this._editSono = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this._editRequestId = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this._editDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this._editState = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            _labelRegNumber = new Infragistics.Win.Misc.UltraLabel();
            _labelSono = new Infragistics.Win.Misc.UltraLabel();
            ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this._editRegNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._editSono)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._editRequestId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._editDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._editState)).BeginInit();
            this.SuspendLayout();
            // 
            // _labelRegNumber
            // 
            appearance5.TextHAlignAsString = "Left";
            appearance5.TextVAlignAsString = "Middle";
            _labelRegNumber.Appearance = appearance5;
            _labelRegNumber.Location = new System.Drawing.Point(8, 66);
            _labelRegNumber.Name = "_labelRegNumber";
            _labelRegNumber.Size = new System.Drawing.Size(153, 21);
            _labelRegNumber.TabIndex = 27;
            _labelRegNumber.Text = "Номер запроса";
            // 
            // _labelSono
            // 
            appearance14.TextHAlignAsString = "Left";
            appearance14.TextVAlignAsString = "Middle";
            _labelSono.Appearance = appearance14;
            _labelSono.Location = new System.Drawing.Point(8, 39);
            _labelSono.Name = "_labelSono";
            _labelSono.Size = new System.Drawing.Size(153, 21);
            _labelSono.TabIndex = 29;
            _labelSono.Text = "Код налогового органа";
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(311, 245);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 25;
            this.btnSave.Text = "Сохранить";
            this.btnSave.Click += new System.EventHandler(this.SaveClick);
            // 
            // _editRegNumber
            // 
            appearance6.TextHAlignAsString = "Left";
            appearance6.TextVAlignAsString = "Middle";
            this._editRegNumber.Appearance = appearance6;
            this._editRegNumber.Location = new System.Drawing.Point(167, 66);
            this._editRegNumber.Name = "_editRegNumber";
            this._editRegNumber.Size = new System.Drawing.Size(219, 21);
            this._editRegNumber.TabIndex = 32;
            // 
            // _editSono
            // 
            appearance18.TextHAlignAsString = "Left";
            appearance18.TextVAlignAsString = "Middle";
            this._editSono.Appearance = appearance18;
            this._editSono.Location = new System.Drawing.Point(167, 39);
            this._editSono.Name = "_editSono";
            this._editSono.ReadOnly = true;
            this._editSono.Size = new System.Drawing.Size(219, 21);
            this._editSono.TabIndex = 31;
            // 
            // ultraLabel1
            // 
            appearance1.TextHAlignAsString = "Left";
            appearance1.TextVAlignAsString = "Middle";
            ultraLabel1.Appearance = appearance1;
            ultraLabel1.Location = new System.Drawing.Point(8, 12);
            ultraLabel1.Name = "ultraLabel1";
            ultraLabel1.Size = new System.Drawing.Size(153, 21);
            ultraLabel1.TabIndex = 31;
            ultraLabel1.Text = "Идентификатор запроса";
            // 
            // _editRequestId
            // 
            appearance2.TextHAlignAsString = "Left";
            appearance2.TextVAlignAsString = "Middle";
            this._editRequestId.Appearance = appearance2;
            this._editRequestId.Location = new System.Drawing.Point(167, 12);
            this._editRequestId.Name = "_editRequestId";
            this._editRequestId.ReadOnly = true;
            this._editRequestId.Size = new System.Drawing.Size(219, 21);
            this._editRequestId.TabIndex = 30;
            // 
            // ultraLabel2
            // 
            appearance7.TextHAlignAsString = "Left";
            appearance7.TextVAlignAsString = "Middle";
            ultraLabel2.Appearance = appearance7;
            ultraLabel2.Location = new System.Drawing.Point(8, 93);
            ultraLabel2.Name = "ultraLabel2";
            ultraLabel2.Size = new System.Drawing.Size(153, 21);
            ultraLabel2.TabIndex = 33;
            ultraLabel2.Text = "Дата запроса";
            // 
            // _editDate
            // 
            this._editDate.Location = new System.Drawing.Point(167, 93);
            this._editDate.Name = "_editDate";
            this._editDate.Size = new System.Drawing.Size(219, 21);
            this._editDate.TabIndex = 33;
            // 
            // _editState
            // 
            this._editState.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            valueListItem1.DataValue = "1";
            valueListItem1.DisplayText = "Запрос отправлен";
            valueListItem2.DataValue = "2";
            valueListItem2.DisplayText = "Запрос не может быть отправлен";
            valueListItem3.DataValue = "3";
            valueListItem3.DisplayText = "Получен ответ";
            this._editState.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2,
            valueListItem3});
            this._editState.Location = new System.Drawing.Point(167, 120);
            this._editState.MaxDropDownItems = 3;
            this._editState.Name = "_editState";
            this._editState.Nullable = false;
            this._editState.Size = new System.Drawing.Size(219, 21);
            this._editState.TabIndex = 34;
            // 
            // ultraLabel3
            // 
            appearance3.TextHAlignAsString = "Left";
            appearance3.TextVAlignAsString = "Middle";
            ultraLabel3.Appearance = appearance3;
            ultraLabel3.Location = new System.Drawing.Point(8, 120);
            ultraLabel3.Name = "ultraLabel3";
            ultraLabel3.Size = new System.Drawing.Size(153, 21);
            ultraLabel3.TabIndex = 36;
            ultraLabel3.Text = "Состояние запроса";
            // 
            // DialogEOD13
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(398, 280);
            this.Controls.Add(ultraLabel3);
            this.Controls.Add(this._editState);
            this.Controls.Add(this._editDate);
            this.Controls.Add(ultraLabel2);
            this.Controls.Add(ultraLabel1);
            this.Controls.Add(this._editRequestId);
            this.Controls.Add(_labelSono);
            this.Controls.Add(this._editSono);
            this.Controls.Add(_labelRegNumber);
            this.Controls.Add(this._editRegNumber);
            this.Controls.Add(this.btnSave);
            this.Name = "DialogEOD13";
            this.Text = "Параметры для ЭОД-13";
            ((System.ComponentModel.ISupportInitialize)(this._editRegNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._editSono)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._editRequestId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._editDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._editState)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor _editRegNumber;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor _editSono;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor _editRequestId;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor _editDate;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor _editState;
    }
}