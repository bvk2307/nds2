﻿namespace EmulatorSEOD.Dialogs
{
    partial class DialogEOD08
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Misc.UltraLabel lDocDate;
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            this.btnSave = new Infragistics.Win.Misc.UltraButton();
            this.deReplyDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            lDocDate = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.deReplyDate)).BeginInit();
            this.SuspendLayout();
            // 
            // lDocDate
            // 
            lDocDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance7.TextHAlignAsString = "Left";
            appearance7.TextVAlignAsString = "Middle";
            lDocDate.Appearance = appearance7;
            lDocDate.Location = new System.Drawing.Point(12, 12);
            lDocDate.Name = "lDocDate";
            lDocDate.Size = new System.Drawing.Size(136, 21);
            lDocDate.TabIndex = 4;
            lDocDate.Text = "Входящая дата ответа";
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(183, 39);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 9;
            this.btnSave.Text = "Сохранить";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // deReplyDate
            // 
            this.deReplyDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.deReplyDate.Location = new System.Drawing.Point(158, 12);
            this.deReplyDate.Name = "deReplyDate";
            this.deReplyDate.Size = new System.Drawing.Size(100, 21);
            this.deReplyDate.TabIndex = 10;
            // 
            // DialogEOD08
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(270, 74);
            this.Controls.Add(this.deReplyDate);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(lDocDate);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DialogEOD08";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Параметры для ЭОД-8";
            ((System.ComponentModel.ISupportInitialize)(this.deReplyDate)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.Misc.UltraButton btnSave;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor deReplyDate;
    }
}