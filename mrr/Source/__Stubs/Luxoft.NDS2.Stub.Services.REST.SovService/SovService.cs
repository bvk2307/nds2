﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel.Activation;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Stub.Services.REST.SovService.Converters;
using Luxoft.NDS2.Stub.Services.REST.SovService.Domain;
using Luxoft.NDS2.Stub.Services.REST.SovService.Search;
using Luxoft.NDS2.Stub.Services.REST.SovService.Services;

namespace Luxoft.NDS2.Stub.Services.REST.SovService
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class SovService : ISovService
    {
        #region Implementation of ISovService

        private const string readyValue = "OK";
        private const string testParamsValue = "1212014";

        public String Check(string param)
        {
            string ret = readyValue;
            Console.WriteLine("Check");

            if (!string.IsNullOrEmpty(param))
            {
                ret = param;
            }

            return ret;
        }

        public OperationResult<BookLine> getBook(string inn, string year, string period, string isSalesBook, string sellerInn,
            string buyerInn, string invoiceNo, string invoiceDate)
        {
            var result = new OperationResult<BookLine>();
            try
            {


            bool salesBookRq = false;
            if (!string.IsNullOrEmpty(isSalesBook))
            {
                salesBookRq = Convert.ToBoolean(isSalesBook);
            }

            if (salesBookRq)
            {
                OperationResult<SellBookLine> rez = getBookSell(period, year, null, inn, null, buyerInn, invoiceNo, invoiceDate, null);
                result.result = GetBookLine(rez.result);
            }
            else
            {
                OperationResult<BuyBookLine> rez = getBookBuy(period, year, null, inn, null, buyerInn, invoiceNo, invoiceDate, null);
                result.result = GetBookLine(rez.result);
            }


            Console.WriteLine("ответ на запрос книги");

            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return result;


        }

        public OperationResult<FlkLine> getFLK(string inn, string year, string period, string isSalesBook)
        {
            var result = new OperationResult<FlkLine>();

            if (string.Concat(inn, period, year).Equals(testParamsValue))
            {
                #region GenerateData

                var currInn = (String.IsNullOrWhiteSpace(inn) ? DataGenerator.GetRandomINN() : inn).Trim();
                result.result = new List<FlkLine>();
                for (int i = 0; i < 10; i++)
                {
                    result.result.Add(item: new FlkLine()
                    {
                        f = (uint)DataGenerator.GetRandomDecimal(),
                        sl = 8,
                        k = ((uint)DataGenerator.GetRandomDecimal()).ToString(),
                        e = "1",
                        a = "2"
                    });
                }

                #endregion
            }
            else
            {
                var stream = BookSearch.GetFileContent("flkPath", inn, period, year);
                if (stream == null)
                {
                    result.result = new List<FlkLine>();
                }
                else
                {
                    using (stream)
                    {
                        result.result = FlkConverter.ToObject(stream);
                    }
                }
            }

            Console.WriteLine("ответ на запрос FLK");

            return result;
        }

        public OperationResult<ResCompLine> getResComp(string inn, string year, string period, string minAlgNum, string cardinality, string isAutoRanked)
        {
            var result = new OperationResult<ResCompLine>();

            if (string.Concat(inn, period, year).Equals(testParamsValue))
            {
                #region GenerateData

                result.result = new List<ResCompLine>();
                for (int i = 0; i < 10; i++)
                {
                    result.result.Add(item: new ResCompLine()
                    {
                        k = ((uint)DataGenerator.GetRandomDecimal()).ToString(),
                        ai = (uint)DataGenerator.GetRandomDecimal(),
                        c = (uint)DataGenerator.GetRandomDecimal(),
                        a = (uint)DataGenerator.GetRandomDecimal(),
                        si = DataGenerator.GetRandomINN(),
                        s = DataGenerator.GetRandomDecimal(),
                        sk = DataGenerator.GetRandomKPP(),
                        bi = DataGenerator.GetRandomINN(),
                        bk = DataGenerator.GetRandomKPP(),
                        n = DataGenerator.GetRandomInvoiceNumber(),
                        d = DataGenerator.GetRandomDateString().ToString("dd.MM.yyyy"),
                        s_a = (uint)DataGenerator.GetRandomDecimal(),
                        s_si = DataGenerator.GetRandomINN(),
                        s_s = DataGenerator.GetRandomDecimal(),
                        s_sk = DataGenerator.GetRandomKPP(),
                        s_bi = DataGenerator.GetRandomINN(),
                        s_bk = DataGenerator.GetRandomKPP(),
                        s_n = DataGenerator.GetRandomInvoiceNumber(),
                        s_d = DataGenerator.GetRandomDateString().ToString("dd.MM.yyyy"),
                        s_k = (uint)DataGenerator.GetRandomDecimal()
                    });
                }

                #endregion
            }
            else
            {
                var stream = BookSearch.GetFileContent("rescompPath", inn, period, year);
                if (stream == null)
                {
                    result.result = new List<ResCompLine>();
                }
                else
                {
                    using (stream)
                    {
                        result.result = ResCompConverter.ToObject(stream);
                    }
                }
            }

            Console.WriteLine("ответ на запрос getResComp");

            return result;
        }

        #endregion

        #region Расхождения

        /// <summary>
        /// returns the current status of request
        /// </summary>
        /// <param name="rid"></param>
        /// <returns></returns>
        public OperationResult getRequestStatus(int rid)
        {
            Console.WriteLine("Request status for {0}", rid);
            OperationResult res = new OperationResult();
            res.Result = MismatchService.Instance.GetStatus(rid).ToString();
            return res;
        }

        /// <summary>
        /// main method to process request
        /// </summary>
        /// <param name="rid"></param>
        /// <returns>
        /// -1 - Not registered
        /// 0 - Ok
        /// 1 - In Progress
        /// 2 - Completed
        /// 9 - Error
        /// </returns>
        public OperationResult requestMismatches(int rid)
        {
            int returnCode = 0;

            try
            {
//                Console.WriteLine(Uri.UnescapeDataString(req));
//                var bytes = Convert.FromBase64String(Uri.UnescapeDataString(req));
//                string xml = Utils.Unzip(bytes);
//
//                var data = SelectionFilter.Deserialize(xml);
//
//                Console.WriteLine(data.FilterName);

                return new OperationResult(MismatchService.Instance.ProcessMissmatches(rid).ToString());

               
            }
            catch (Exception ex)
            {
                returnCode = 2;
                Console.WriteLine(ex);
            }

            return new OperationResult(returnCode.ToString());
        }

        #endregion

        #region 2.0

        private List<BookLine> GetBookLine(List<BuyBookLine> list)
        {
            List<BookLine> ret = new List<BookLine>();

            foreach (var item in list)
            {
                ret.Add(new BookLine()
                {
                    k = item.ИдЗаписиСФ,
                    si = item.ИННПрод,
                    sk = item.КПППрод,
                    bi = item.ИННПокуп,
                    bk = item.КПППокуп,
                    n = item.НомСчФПрод,
                    d = item.ДатаСчФПрод.ToString("dd.MM.yyyy"),
                    s = item.СтоимПокупВ,
                    v = item.СумНДСВыч,
                    //sc = item.
                    //s18 = item.СтоимПродСФ18,
                    //s10 = item.СумНДССФ10,
                    //s0 = item.СтоимПродСФ0,
                    //sn = item.
                    //v18 = item.СумНДССФ18,
                    //v10 = item.СумНДССФ10,
                    o = item.КодВидОпер,
                    t = item.ИдТЗап,
                    a = item.НомАлг
                });
            }

            return ret;
        }

        private List<BookLine> GetBookLine(List<SellBookLine> list)
        {
            List<BookLine> ret = new List<BookLine>();

            foreach (var item in list)
            {
                ret.Add(new BookLine()
                {
                    k = item.ИдЗаписиСФ,
                    si = item.ИННПрод,
                    sk = item.КПППрод,
                    bi = item.ИННПокуп,
                    bk = item.КПППокуп,
                    n = item.НомСчФПрод,
                    d = item.ДатаСчФПрод.ToString("dd.MM.yyyy"),
                    s = item.СтоимПродСФ,
                    //v = item.
                    //sc = item.
                    s18 = item.СтоимПродСФ18,
                    s10 = item.СумНДССФ10,
                    s0 = item.СтоимПродСФ0,
                    //sn = item.
                    v18 = item.СумНДССФ18,
                    v10 = item.СумНДССФ10,
                    o = item.КодВидОпер,
                    t = item.ИдТЗап,
                    a = item.НомАлг
                });
            }

            return ret;
        }

        private OperationResult<BuyBookLine> getBookBuy(string Period, string Year, string BookType, string INNpayer, string StatusRecord,
                                                       string INNseller, string NumberSF, string DateSF, string IDrecordSF)
        {
            var result = new OperationResult<BuyBookLine>();

            if (string.Concat(INNpayer, Period, Year).Equals(testParamsValue))
            {
                #region generated data
                result.result = new List<BuyBookLine>();
                for (int i = 0; i < 10000; i++)
                {
                    result.result.Add(item: new BuyBookLine()
                    {
                        ДатаСчФПрод = DataGenerator.GetRandomDateString(),
                        ИННПокуп = DataGenerator.GetRandomINN(),
                        ИННПрод = DataGenerator.GetRandomINN(),
                        ИдТЗап = DataGenerator.GetRandomTaxPayerName(),
                        КПППокуп = DataGenerator.GetRandomKPP(),
                        КПППрод = DataGenerator.GetRandomKPP(),
                        КодВидОпер = DataGenerator.GetRandomOperationCode(),
                        НомАлг = DataGenerator.GetRandomInvoiceNumber(),
                        НомСчФПрод = DataGenerator.GetRandomInvoiceNumber(),
                        СтоимПокупВ = DataGenerator.GetRandomDecimal(),
                        СумНДСВыч = DataGenerator.GetRandomDecimal()
                    });
                }
                #endregion generated data

            }
            else
            {
                var stream = BookSearch.GetFileContent("buyBooksPath", INNpayer, Period, Year);
                if (stream == null)
                {
                    result.result = new List<BuyBookLine>();
                }
                else
                {
                    using (stream)
                    {
                        result.result = BuyBookConverter.ToObject(stream);
                    }
                }
            }
            Console.WriteLine("ответ на запрос книги покупок");
            return result;
        }

        private OperationResult<SellBookLine> getBookSell(string Period, string Year, string BookType, string INNpayer, string StatusRecord,
                                                string INNbuyer, string NumberSF, string DateSF, string IDrecordSF)
        {
            var result = new OperationResult<SellBookLine>();

            if (string.Concat(INNpayer, Period, Year).Equals(testParamsValue))
            {
                #region generated data
                result.result = new List<SellBookLine>();
                for (int i = 0; i < 10000; i++)
                {
                    result.result.Add(item: new SellBookLine()
                    {
                        ДатаСчФПрод = DataGenerator.GetRandomDateString(),
                        ИННПокуп = DataGenerator.GetRandomINN(),
                        ИННПрод = DataGenerator.GetRandomINN(),
                        ИдТЗап = DataGenerator.GetRandomTaxPayerName(),
                        КПППокуп = DataGenerator.GetRandomKPP(),
                        КПППрод = DataGenerator.GetRandomKPP(),
                        КодВидОпер = DataGenerator.GetRandomOperationCode(),
                        НомАлг = DataGenerator.GetRandomInvoiceNumber(),
                        НомСчФПрод = DataGenerator.GetRandomInvoiceNumber(),
                        СтоимПродОсв = DataGenerator.GetRandomDecimal(),
                        СтоимПродСФ = DataGenerator.GetRandomDecimal(),
                        СтоимПродСФ0 = DataGenerator.GetRandomDecimal(),
                        СтоимПродСФ10 = DataGenerator.GetRandomDecimal(),
                        СтоимПродСФ18 = DataGenerator.GetRandomDecimal(),
                        СумНДССФ10 = DataGenerator.GetRandomDecimal(),
                        СумНДССФ18 = DataGenerator.GetRandomDecimal()
                    });
                }
                #endregion generated data
            }
            else
            {
                var stream = BookSearch.GetFileContent("sellBooksPath", INNpayer, Period, Year);
                if (stream == null)
                {
                    result.result = new List<SellBookLine>();
                }
                else
                {
                    using (stream)
                    {
                        result.result = SellBookConverter.ToObject(stream);
                    }
                }
            }
            Console.WriteLine("ответ на запрос книги продаж");

            return result;
        }

        #endregion 2.0
    }




}
