﻿create table FIR.Declaration
(
       id number(19),
       inn varchar2(12),
       kpp varchar2(9),
       org_name varchar2(512),
       Region_Code varchar2(4),
       Soun_code varchar(4),
       TaxPayer_category number(1),
       Period_code varchar(2),
       Sign number(1),
       Correction_number number,
       Total_mismatches number,
       AmntMismatches_total number,
       AmntMismatches_max number,
       AmntMismatches_min number,
       AmntMismatches_avg number
);
