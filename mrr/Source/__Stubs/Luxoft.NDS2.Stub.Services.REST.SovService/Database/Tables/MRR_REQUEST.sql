﻿create table FIR.MRR_REQUEST
(
    request_id number(12),
    request_body nclob,
    status number(1),
    message varchar2(2048)
);
