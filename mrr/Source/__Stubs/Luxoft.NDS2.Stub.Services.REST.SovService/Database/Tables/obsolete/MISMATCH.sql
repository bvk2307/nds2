﻿create table FIR.DISCREPANCY
(
   request_id varchar2(128),
   mismatch_type number(1),
   compare_kind number(1),
   rule_group number(5),
   deal_amnt_total number(19,2),
   mismatch_amnt number(19,2),
   tax_period varchar2(2),
   fiscal_year varchar2(4),
   inn varchar2(12),
   inn_contractor varchar2(12),
   invoice_id varchar(128),
   decl_id varchar(128),
   invoice_contractor_id varchar(128),
   decl_contractor_id varchar(128),
   status varchar2(128)   
);

create table FIR.DISCREPANCY_RAW
(
   mismatch_type number(1),
   compare_kind number(1),
   rule_group number(5),
   deal_amnt_total number(19,2),
   mismatch_amnt number(19,2),
   tax_period varchar2(2),
   fiscal_year varchar2(4),
   inn varchar2(12),
   inn_contractor varchar2(12),
   invoice_id varchar(128),
   decl_id varchar(128),
   invoice_contractor_id varchar(128),
   decl_contractor_id varchar(128),
   status varchar2(128)      
);

