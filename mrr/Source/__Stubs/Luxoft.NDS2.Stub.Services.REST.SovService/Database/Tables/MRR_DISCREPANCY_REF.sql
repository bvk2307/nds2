﻿create table FIR.MRR_DISCREPANCY_REF
(
    request_id number(12),
    discrepancy_id number(12),
	IsChecked number(1)
);

create table FIR.DISCREPANCY
(
   discrepancy_id number(12),
   discrepancy_date date,
   discrepancy_type number(1),
   compare_kind number(1),
   rule_group number(5),
   deal_amnt_total number(19,2),
   discrepancy_amnt number(19,2),
   tax_period varchar2(2),
   fiscal_year varchar2(4),
   inn varchar2(12),
   inn_contractor varchar2(12),
   invoice_id varchar(128),
   decl_id varchar(128),
   invoice_contractor_id varchar(128),
   decl_contractor_id varchar(128),
   status number(2)   
);

create table FIR.DISCREPANCY_RAW
(
   id number(12),
   discrepancy_date date,
   discrepancy_type number(1),
   compare_kind number(1),
   rule_group number(5),
   deal_amnt_total number(19,2),
   discrepancy_amnt number(19,2),
   tax_period varchar2(2),
   fiscal_year varchar2(4),
   inn varchar2(12),
   inn_contractor varchar2(12),
   invoice_id varchar(128),
   decl_id varchar(128),
   invoice_contractor_id varchar(128),
   decl_contractor_id varchar(128),
   status number(2)      
);

