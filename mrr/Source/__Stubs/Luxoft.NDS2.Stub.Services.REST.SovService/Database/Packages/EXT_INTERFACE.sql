﻿create or replace package FIR.EXT_INTERFACE
is
function request_mismatches
(
 p_request_body in MRR_REQUEST.request_body%type
)
return number;
end EXT_INTERFACE;
/




create or replace package body FIR.EXT_INTERFACE
as
function request_mismatches
(
 p_request_body in MRR_REQUEST.request_body%type
)
return number
as
v_id number(12);
begin
 v_id := MRR_REQ_SEQ.nextval;
 insert into  MRR_REQUEST values(v_id, p_request_body, 0, '');
 return v_id;
end;
end EXT_INTERFACE;
/
