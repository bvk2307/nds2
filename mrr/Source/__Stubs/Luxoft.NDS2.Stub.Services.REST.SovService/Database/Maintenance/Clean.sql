﻿declare 
v_sql varchar2(512);
begin

for l in (select * from all_objects where owner='FIR')
loop
select 'drop '||l.object_type||' '|| l.object_name||  DECODE(l.OBJECT_TYPE,'TABLE',' CASCADE CONSTRAINTS','') into v_sql from dual;
--DBMS_OUTPUT.put_line(v_sql);
begin
execute immediate v_sql;
exception when others then null;
end;
end loop;
end;
/
