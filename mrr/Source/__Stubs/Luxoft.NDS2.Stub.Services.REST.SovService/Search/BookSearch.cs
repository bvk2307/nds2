﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Luxoft.NDS2.Stub.Services.REST.SovService.Search
{
    class BookSearch
    {
        public static Stream GetFileContent(string pathType, string innPayer, string period, string year)
        {
            var searchPattern = string.Format("{0}-{1}-{2}-{3}", innPayer, pathType, period, year);

            var baseStoragePath = ConfigurationManager.AppSettings["storageBasePath"];
            var bookFolder = Path.Combine(baseStoragePath, ConfigurationManager.AppSettings[pathType]);
            var metadataDb = Path.Combine(baseStoragePath, ConfigurationManager.AppSettings["booksMetadataDb"]);

            if (!Directory.Exists(baseStoragePath)) throw new ApplicationException(string.Format("base storage path not found {0}", baseStoragePath));
            if (!Directory.Exists(bookFolder)) throw new ApplicationException(string.Format("book storage not found {0}", bookFolder));
            if (!File.Exists(metadataDb)) throw new ApplicationException(string.Format("book metadata file not found {0}", metadataDb));

            using (StreamReader reader = new StreamReader(metadataDb))
            {
                var line = reader.ReadLine();
                while (line != null)
                {
                    if (line.StartsWith(searchPattern))
                    {
                        var parts = line.Split(';');
                        return File.OpenRead(Path.Combine(bookFolder, parts[1]));

                    }
                    line = reader.ReadLine();
                }
            }

            return null;
        }
    }
}
