﻿using Luxoft.NDS2.Stub.Services.REST.SovService.Domain;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Luxoft.NDS2.Stub.Services.REST.SovService.Converters
{
    class ResCompConverter
    {
        public static List<ResCompLine> ToObject(Stream stream)
        {
            List<ResCompLine> result = new List<ResCompLine>();
            int lineNumnber = 1;
            using (XmlReader reader = XmlReader.Create(stream))
            {
                reader.MoveToContent();
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element)
                    {
                        switch (reader.Name)
                        {
                            case "Данные":
                                if (reader.AttributeCount > 0)
                                {
                                    reader.MoveToAttribute(0);
                                }
                                break;
                            case "Строка":
                                ResCompLine line = new ResCompLine();

                                result.Add(line);

                                InitDefaults(line);
                                ReadBookLineAttributes(line, reader);

                                ++lineNumnber;
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            return result;
        }

        private static void ReadBookLineAttributes(ResCompLine b, XmlReader reader)
        {
            CultureInfo ciENG = new CultureInfo("en-US", false);
            CultureInfo ciRUS = new CultureInfo("ru-RU", false);
            for (int i = 0; i < reader.AttributeCount; i++)
            {
                reader.MoveToAttribute(i);
                switch (reader.Name)
                {
                    case "ДатаСФКнПок":
                        b.d = reader.Value;
                        break;
                    case "ДатаСФКнПрод":
                        b.s_d = reader.Value;
                        break;
                    case "ИдЗаписиСФПок":
                        b.k = reader.Value;
                        break;
                    case "ИдЗаписиСФПрод":
                        b.s_k = Convert.ToUInt32(reader.Value);
                        break;
                    case "ИННПокКнПок":
                        b.bi = reader.Value;
                        break;
                    case "ИННПокКнПрод":
                        b.s_bi = reader.Value;
                        break;
                    case "ИННПродКнПок":
                        b.si = reader.Value;
                        break;
                    case "ИННПродКнПрод":
                        b.s_si = reader.Value;
                        break;
                    case "КПППокКнПок":
                        b.bk = reader.Value;
                        break;
                    case "КПППокКнПрод":
                        b.s_bk = reader.Value;
                        break;
                    case "КПППродКнПок":
                        b.sk = reader.Value;
                        break;
                    case "КПППродКнПрод":
                        b.s_sk = reader.Value;
                        break;
                    case "НомАлг":
                        b.ai = Convert.ToUInt32(reader.Value);
                        break;
                    case "НомАлгКнПок":
                        b.a = Convert.ToUInt32(reader.Value);
                        break;
                    case "НомАлгКнПрод":
                        b.s_a = Convert.ToUInt32(reader.Value);
                        break;
                    case "НомСФКнПок":
                        b.n = reader.Value;
                        break;
                    case "СтоимПродКнПок":
                        b.s = Convert.ToDecimal(reader.Value, ciENG);
                        break;
                    case "СтоимПродКнПрод":
                        b.s_s = Convert.ToDecimal(reader.Value, ciENG);
                        break;

                    default:
                        break;
                }
            }

            reader.MoveToElement();
        }

        private static void InitDefaults(ResCompLine b)
        {
        }
    }
}
