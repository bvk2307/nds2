﻿using Luxoft.NDS2.Stub.Services.REST.SovService.Domain;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Luxoft.NDS2.Stub.Services.REST.SovService.Converters
{
    class FlkConverter : BaseBookConverter
    {
        public static List<FlkLine> ToObject(Stream stream)
        {
            List<FlkLine> result = new List<FlkLine>();
            int lineNumnber = 1;
            using (XmlReader reader = XmlReader.Create(stream))
            {
                reader.MoveToContent();
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element)
                    {
                        switch (reader.Name)
                        {
                            case "Данные":
                                if (reader.AttributeCount > 0)
                                {
                                    reader.MoveToAttribute(0);
                                }
                                break;
                            case "Строка":
                                FlkLine line = new FlkLine();

                                result.Add(line);
                                
                                InitDefaults(line);
                                ReadBookLineAttributes(line, reader);

                                ++lineNumnber;
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            return result;
        }

        private static void ReadBookLineAttributes(FlkLine b, XmlReader reader)
        {
            CultureInfo ciENG = new CultureInfo("en-US", false);
            CultureInfo ciRUS = new CultureInfo("ru-RU", false);
            for (int i = 0; i < reader.AttributeCount; i++)
            {
                reader.MoveToAttribute(i);
                switch (reader.Name)
                {
                    case "ВидОшибки":
                        b.a = reader.Value;
                        break;
                    case "ИдДекл":
                        b.f = Convert.ToUInt32(reader.Value);
                        break;
                    case "НомРазд":
                        b.sl = Convert.ToUInt32(reader.Value);
                        break;
                    case "ИдЗаписиСФ":
                        b.k = reader.Value;
                        break;
                    case "ИНННП":
                        break;
                    case "ТипОшибки":
                        b.e = reader.Value;
                        break;

                    default:
                        break;
                }
            }

            reader.MoveToElement();
        }

        private static void InitDefaults(FlkLine line)
        {
        }
    }
}
