﻿using System;
using Luxoft.NDS2.Common.Contracts.Helpers;

namespace Luxoft.NDS2.Stub.Services.REST.SovService.Converters
{
    public abstract class BaseBookConverter
    {
        protected static DateTime GetRandomDateString()
        {
            return DataGenerator.GetRandomDateString();
        }

        protected static string GetRandomInvoiceNumber()
        {
            return DataGenerator.GetRandomInvoiceNumber();
        }

        protected static string GetRandomOperationCode()
        {
            return DataGenerator.GetRandomOperationCode();
        }

        protected static string GetRandomINN()
        {
            return DataGenerator.GetRandomINN();
        }

        protected static string GetRandomKPP()
        {
            return DataGenerator.GetRandomKPP();
        }

        protected static string GetRandomDecimalString()
        {
            return DataGenerator.GetRandomDecimalString();
        }

        protected static string GetRandomTaxPayerName()
        {
            return DataGenerator.GetRandomTaxPayerName();
        }
    }
}
