﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Xml;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Stub.Services.REST.SovService.Domain;

namespace Luxoft.NDS2.Stub.Services.REST.SovService.Converters
{
    public class BuyBookConverter : Stub.Services.REST.SovService.Converters.BaseBookConverter
    {
        public static List<BuyBookLine> ToObject(Stream xmlcontentStream)
        {
            List<BuyBookLine> result = new List<BuyBookLine>();
            int lineNumnber = 1;
            using (XmlReader reader = XmlReader.Create(xmlcontentStream))
            {
                reader.MoveToContent();
                while (reader.Read())
                {
                    if(reader.NodeType == XmlNodeType.Element)
                    {
                        switch (reader.Name)
                        {
                            case "Документ":
                                if (reader.AttributeCount > 0)
                                {
                                    reader.MoveToAttribute(0);
                                    //sellBook.Knd = Convert.ToInt32(reader.Value);                                    
                                }
                                break;
                            case "КнПокСтр":
                                BuyBookLine line = new BuyBookLine();
                                line.Id = lineNumnber;
                                result.Add(line);
                                InitDefaults(line);
                                ReadBookLineAttributes(line, reader);
                                ReadInnInfo(line, reader);
                                ++lineNumnber;
                                break;
                            case "ДатаОплСчФПрод":
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            return result;
        }

        private static void ReadInnInfo(BuyBookLine line, XmlReader reader)
        {            
            while (reader.Read())
            {
                switch (reader.Name)
                {
                    case "СвПрод":
                        {
                            while (reader.Read())
                            {
                                switch (reader.Name)
                                {
                                    case "СведЮЛ":
                                        {
                                            for (int i = 0; i < reader.AttributeCount; i++)
                                            {
                                                reader.MoveToAttribute(i);
                                                switch (reader.Name)
                                                {
                                                    case "ИННЮЛ":
                                                        line.ИННПрод = reader.Value;
                                                        break;
                                                }
                                            }
                                            return;
                                        }
                                }                                
                            }
                            break;
                        }
                    default:
                        break;                        
                }
            }
        }

        private static void ReadBookLineAttributes(BuyBookLine b, XmlReader reader)
        {
            CultureInfo ciENG = new CultureInfo("en-US", false);
            CultureInfo ciRUS = new CultureInfo("ru-RU", false);
            for (int i = 0; i < reader.AttributeCount; i++)
            {
                reader.MoveToAttribute(i);
                switch (reader.Name)
                {
                    case "ИдЗаписиСФ":
                        b.ИдЗаписиСФ = reader.Value;
                        break;
                    case "НомСчФПрод":
                        b.НомСчФПрод = reader.Value;
                        break;
                    case "ДатаСчФПрод":
                        b.ДатаСчФПрод = Convert.ToDateTime(reader.Value, ciRUS.DateTimeFormat);
                        break;
                    case "НомИспрСчФ":
                        b.НомИспрСчФ  = reader.Value;
                        break;
                    case "ДатаИспрСчФ":
                        b.ДатаИспрСчФ = Convert.ToDateTime(reader.Value, ciRUS.DateTimeFormat);
                        break;
                    case "НомИспрКСчФ":
                        b.НомИспрКСчФ  = reader.Value;
                        break;
                    case "ДатаИспрКСчФ":
                        b.ДатаИспрКСчФ = Convert.ToDateTime(reader.Value, ciRUS.DateTimeFormat);
                        break;
                    case "НомКСчФПрод":
                        b.НомКСчФПрод = reader.Value;
                        break;
                    case "ДатаКСчФПрод":
                        b.ДатаКСчФПрод = Convert.ToDateTime(reader.Value, ciRUS.DateTimeFormat);
                        break;
                    case "НомДокПдтвОпл":
                        b.НомДокПдтвОпл = reader.Value;
                        break;
                    case "ДатаДокПдтвОпл":
                        b.ДатаДокПдтвОпл = Convert.ToDateTime(reader.Value, ciRUS.DateTimeFormat);
                        break;
                    case "НомТД":
                        b.НомТД = reader.Value;
                        break;
                    case "НомерПор":
                        b.НомерПор = reader.Value;
                        break;
                    case "СтоимПокупВ":
                        b.СтоимПокупВ = Convert.ToDecimal(reader.Value, ciENG);
                        break;
                    case "СумНДСВыч":
                        b.СумНДСВыч = Convert.ToDecimal(reader.Value, ciENG);
                        break;
                    case "ДатаУчТов":
                        break;
                    default:
                        break;
                }
            }

            reader.MoveToElement();
        }

        private static void InitDefaults(BuyBookLine book)
        {
//            book.SellerInn = GetRandomINN();
//            book.SellerName = GetRandomTaxPayerName();
//            book.SellerKpp = GetRandomKPP();
//            book.BrokerInn = GetRandomINN();
//            book.BrokerName = GetRandomTaxPayerName();
//            book.BrokerKpp = GetRandomKPP();
//            book.RegisterDate = GetRandomDateString();
//            book.Currency = "Руб.";
        }
    }
}
