﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Xml;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Stub.Services.REST.SovService.Domain;

namespace Luxoft.NDS2.Stub.Services.REST.SovService.Converters
{
    public class SellBookConverter : Stub.Services.REST.SovService.Converters.BaseBookConverter
    {
        public static List<SellBookLine> ToObject(Stream xmlcontentStream)
        {
            List<SellBookLine> result = new List<SellBookLine>();
            int lineNumber = 1;
            using (XmlReader reader = XmlReader.Create(xmlcontentStream))
            {
                reader.MoveToContent();
                while (reader.Read())
                {
                    if(reader.NodeType == XmlNodeType.Element)
                    {
                        switch (reader.Name)
                        {
                            case "Документ":
                                if (reader.AttributeCount > 0)
                                {
                                    reader.MoveToAttribute(0);
                                }
                                break;

                            case "СвПродав":
                                break;

                            case "СведЮЛ":
                                break;

                            case "СвКнПрод":
                                break;

                            case "Всего":

                                break;

                            case "СвПродаж":
                                SellBookLine line = new SellBookLine();
                                line.Id = lineNumber;
                                result.Add(line);
                                InitDefaults(line);
                                ReadBookLineAttributes(line, reader);
                                ReadBookTaxLine(line, reader.ReadSubtree());
                                lineNumber++;
                                break;
                            case "ДатаОплСчФПрод":
                                break;
                            default:
                                break;
                        }
                    }
                }
            }


            return result;
        }

        private static void InitDefaults(SellBookLine book)
        {
//            book.OperationCode = GetRandomOperationCode();
//            book.SellerInvoiceNum = GetRandomInvoiceNumber();
//            book.SellerInvoiceDate = GetRandomDateString();
//            book.SellerInvoiceNumChanged = GetRandomInvoiceNumber();
//            book.SellerInvoiceDateChanged = GetRandomDateString();
//            book.SellerInvoiceNumCorrected = GetRandomInvoiceNumber();
//            book.SellerInvoiceDateCorrected = GetRandomDateString();
//
//            book.BrokerName = GetRandomTaxPayerName();
//            book.BrokerInn = GetRandomINN();
//            book.BrokerKpp = GetRandomKPP();
//
//            book.PaymentDocNumber = GetRandomInvoiceNumber();
//            book.PaymentDocDate = GetRandomDateString();
//
//            book.Currency = "Руб.";

        }

        private static void ReadBookLineAttributes(SellBookLine sl, XmlReader reader)
        {
            CultureInfo ciENG = new CultureInfo("en-US", false);
            CultureInfo ciRUS = new CultureInfo("ru-RU", false);
            for (int i = 0; i < reader.AttributeCount; i++)
            {
                reader.MoveToAttribute(i);
                switch (reader.Name)
                {
                    case "ИдЗаписиСФ":
                        sl.ИдЗаписиСФ = reader.Value;
                        break;
                    case "ДатаСчФ":
                        sl.ДатаСчФПрод = Convert.ToDateTime(reader.Value, ciRUS.DateTimeFormat);
                        break;
                    case "НомерСчФ":
                        sl.НомСчФПрод = reader.Value;
                        break;
                    case "НаимПок":
                        sl.НаимПок = reader.Value;
                        break;
                    case "ИННЮЛ":
                        sl.ИННПокуп = reader.Value;
                        break;
                    case "КПП":
                        sl.КПППокуп = reader.Value;
                        break;
                    case "СтТовУчНалВсего":
                        sl.СтоимПродСФ = Convert.ToDecimal(reader.Value, ciENG);
                        break;
                    default:
                        break;
                }
            }

            reader.MoveToElement();
        }

        private static void ReadBookTaxLine(SellBookLine sl, XmlReader reader)
        {
            var formatProvider = new CultureInfo("en-US", false);
            Action<decimal> ndsValueSetter = v => { };
            decimal nonNdsValue = 0;

            using (reader)
            {
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element)
                    {
                        switch (reader.Name)
                        {
                            case "ДатаОплСчФПрод":
                                reader.Read();
                                //sl.BuyerSellerInvoicePayDay = Convert.ToDateTime(reader.Value);
                                break;
                            case "ВтЧисле":
                                if (reader.AttributeCount > 0)
                                {
                                    reader.MoveToAttribute(0);
                                    nonNdsValue = Convert.ToDecimal(reader.Value, formatProvider);
                                }
                                else
                                {
                                    nonNdsValue = 0;
                                }
                                break;
                            case "НалСт":
                                reader.MoveToAttribute(0);//НалСтВел
                                switch (Convert.ToInt32(reader.Value))
                                {
                                    case 0:
                                        ndsValueSetter = v => { sl.СтоимПродСФ0 = v; };
                                        break;
                                    case 10:
                                        sl.СтоимПродСФ10 = nonNdsValue;
                                        ndsValueSetter = v => { sl.СумНДССФ10 = v; };
                                        break;
                                    case 18:
                                        sl.СтоимПродСФ18 = nonNdsValue;
                                        ndsValueSetter = v => { sl.СумНДССФ18 = v; };
                                        break;
                                    case 20:
//                                        sl.Amount20Tax = nonNdsValue;
//                                        ndsValueSetter = v => { sl.Amount20Nds = v; };
                                        break;
                                }

                                break;
                            case "СумНал":
                                reader.MoveToAttribute(0);
                                ndsValueSetter(Convert.ToDecimal(reader.Value, formatProvider));
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
