﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Luxoft.NDS2.Stub.Services.REST.SovService.Domain
{
    public class OperationResult<T> where T:class 
    {
        public List<T> result { get; set; }
    }

    public class OperationResult
    {
        public OperationResult()
        {
            
        }

        public OperationResult(string value)
        {
            Result = value;
        }

        public string Result { get; set; }
    }
}
