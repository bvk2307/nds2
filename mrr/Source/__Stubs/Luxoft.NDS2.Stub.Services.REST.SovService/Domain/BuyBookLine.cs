﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Luxoft.NDS2.Stub.Services.REST.SovService.Domain
{
    [DataContract]
    public class BuyBookLine
    {
        [DataMember]
        public string ИдЗаписиСФ { get; set; }
        [DataMember]
        public string КодВидОпер { get; set; }
        [DataMember]
        public string НомСчФПрод { get; set; }
        [DataMember]
        public DateTime ДатаСчФПрод { get; set; }
        [DataMember]
        public string ИННПрод { get; set; }
        [DataMember]
        public string КПППрод { get; set; }
        [DataMember]
        public decimal СтоимПокупВ { get; set; }
        [DataMember]
        public decimal СумНДСВыч { get; set; }
        [DataMember]
        public string ИННПокуп { get; set; }
        [DataMember]
        public string КПППокуп { get; set; }
        [DataMember]
        public string ИдТЗап { get; set; }
        [DataMember]
        public string НомАлг { get; set; }
        
        #region added by format
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string НомерПор { get; set; }
        [DataMember]
        public string НомТД { get; set; }
        [DataMember]
        public DateTime? ДатаДокПдтвОпл { get; set; }
        [DataMember]
        public string НомДокПдтвОпл { get; set; }
        [DataMember]
        public DateTime? ДатаКСчФПрод { get; set; }
        [DataMember]
        public string НомКСчФПрод { get; set; }
        [DataMember]
        public DateTime? ДатаИспрКСчФ { get; set; }
        [DataMember]
        public string НомИспрКСчФ{ get; set; }
        [DataMember]
        public DateTime? ДатаИспрСчФ { get; set; }
        [DataMember]
        public string НомИспрСчФ { get; set; }

        #endregion
    }
}
