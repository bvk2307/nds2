﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Luxoft.NDS2.Stub.Services.REST.SovService.Domain
{
    [DataContract]
    public class SellBookLine
    {
        [DataMember]
        public string ИдЗаписиСФ { get; set; }
        [DataMember]
        public string КодВидОпер { get; set; }
        [DataMember]
        public string НомСчФПрод { get; set; }
        [DataMember]
        public DateTime ДатаСчФПрод { get; set; }
        [DataMember]
        public string ИННПокуп { get; set; }
        [DataMember]
        public string КПППокуп { get; set; }
        [DataMember]
        public decimal СтоимПродСФ { get; set; }
        [DataMember]
        public decimal СтоимПродСФ18 { get; set; }
        [DataMember]
        public decimal СтоимПродСФ10 { get; set; }
        [DataMember]
        public decimal СтоимПродСФ0 { get; set; }
        [DataMember]
        public decimal СумНДССФ18 { get; set; }
        [DataMember]
        public decimal СумНДССФ10 { get; set; }
        [DataMember]
        public decimal СтоимПродОсв { get; set; }
        [DataMember]
        public string ИННПрод { get; set; }
        [DataMember]
        public string КПППрод { get; set; }
        [DataMember]
        public string ИдТЗап { get; set; }
        [DataMember]
        public string НомАлг { get; set; }

        #region added by format
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string НаимПок { get; set; }
        #endregion
    }
}
