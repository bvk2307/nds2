﻿//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
//using System.Runtime.Serialization;
//using System.Text;
//using System.Xml;
//using System.Xml.Linq;
//using System.Xml.Serialization;
//
//namespace Luxoft.NDS2.Stub.Services.REST.SovService.Domain
//{
//    [Serializable]
//    public class FilterCriteria
//    {
//        public enum ValueTypes
//        {
//            MultipleSelection = 0,
//            SingleValue = 1,
//            SingleWithComparisonOp = 2,
//            SetOfIntervals = 3,
//            SetOfValues = 4
//        };
//
//        public string Name { get; set; }
//        public ValueTypes ValueTypeCode { get; set; }
//        public string LookupTableName { get; set; }
//        public string InternalName { get; set; }
//        public bool IsActive { get; set; }
//        public List<FilterElement> Values { get; set; }
//    }
//
//    [Serializable]
//    public class FilterElement
//    {
//        public enum ComparisonOperations
//        {
//            Equals,
//            NotEquals,
//            GreatThan,
//            LessThan,
//            Between
//        }
//           
//        public ComparisonOperations Operator { get; set; }
//        public string ValueOne { get; set; }
//        public string ValueTwo { get; set; }
//    }
//
//    [Serializable]
//    public class GroupFilter
//    {
//        public string Name { get; set; }        
//        public List<FilterCriteria> Filters { get; set; }
//        public bool IsActive { get; set; }
//    }
//
//    [Serializable]
//    public class SelectionFilter
//    {
//        public long? SelectionId { get; set; }
//
//        public string RegionCode { get; set; }
//        public bool IsLargestTaxPayer { get; set; }
//        public bool IsActive { get; set; }
//
//        public string FilterName { get; set; }
//        public string UserName { get; set; }
//        public bool IsFavourite { get; set; }
//
//        public List<GroupFilter> GroupsFilters { get; set; }
//
//        public string Serialize()
//        {
//            XmlSerializer ser = new XmlSerializer(typeof(SelectionFilter));
//            StringBuilder sb = new StringBuilder();
//            using(TextWriter w = new StringWriter(sb))
//            {
//                ser.Serialize(w, this);
//            }
//
//            return sb.ToString();
//        }
//
//        public static SelectionFilter Deserialize(string content)
//        {
//            SelectionFilter result = null;
//
//            XmlSerializer ser = new XmlSerializer(typeof(SelectionFilter));
//
//            try
//            {
//                using (TextReader reader = new StringReader(content))
//                {
//                    result = ser.Deserialize(reader) as SelectionFilter;
//                }
//            }
//            catch (Exception)
//            {
//                result = new SelectionFilter();
//            }
//
//            return result;
//        }
//    }
//}
