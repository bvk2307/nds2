﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Luxoft.NDS2.Stub.Services.REST.SovService.Domain
{
    [DataContract]
    public class FlkLine
    {
        [DataMember]
        public uint f { get; set; }

        [DataMember]
        public uint sl { get; set; }

        [DataMember]
        public string k { get; set; }

        [DataMember]
        public string e { get; set; }

        [DataMember]
        public string a { get; set; }
    }
}
