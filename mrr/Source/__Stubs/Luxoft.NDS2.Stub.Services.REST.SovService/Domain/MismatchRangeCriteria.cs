﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Stub.Services.REST.SovService.Domain
{
    public class MismatchRangeCriteria
    {
        public decimal ValueOne { get; set; }
        public decimal ValueTwo { get; set; }
        public string Operator { get; set; }
    }
}
