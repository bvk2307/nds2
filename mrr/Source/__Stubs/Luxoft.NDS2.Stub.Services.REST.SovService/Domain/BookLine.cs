﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Luxoft.NDS2.Stub.Services.REST.SovService.Domain
{
    [DataContract]
    public class BookLine
    {
        [DataMember]
        public string k { get; set; }
        [DataMember]
        public string si { get; set; }
        [DataMember]
        public string sk { get; set; }
        [DataMember]
        public string bi { get; set; }
        [DataMember]
        public string bk { get; set; }
        [DataMember]
        public string n { get; set; }
        [DataMember]
        public string d { get; set; }

        [DataMember]
        public decimal s { get; set; }
        [DataMember]
        public decimal v { get; set; }

        [DataMember]
        public decimal sc { get; set; }
        [DataMember]
        public decimal s18 { get; set; }
        [DataMember]
        public decimal s10 { get; set; }
        [DataMember]
        public decimal s0 { get; set; }
        [DataMember]
        public decimal sn { get; set; }
        [DataMember]
        public decimal v18 { get; set; }
        [DataMember]
        public decimal v10 { get; set; }

        [DataMember]
        public string o { get; set; }
        [DataMember]
        public string t { get; set; }
        [DataMember]
        public string a { get; set; }
    }
}
