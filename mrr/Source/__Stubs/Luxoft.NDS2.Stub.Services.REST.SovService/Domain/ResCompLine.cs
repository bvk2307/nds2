﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Luxoft.NDS2.Stub.Services.REST.SovService.Domain
{
    [DataContract]
    public class ResCompLine
    {
        [DataMember]
        public string k { get; set; }

        [DataMember]
        public uint ai { get; set; }

        [DataMember]
        public uint c { get; set; }

        [DataMember]
        public uint a { get; set; }

        [DataMember]
        public string si { get; set; }

        [DataMember]
        public decimal s { get; set; }

        [DataMember]
        public string sk { get; set; }

        [DataMember]
        public string bi { get; set; }

        [DataMember]
        public string bk { get; set; }

        [DataMember]
        public string n { get; set; }

        [DataMember]
        public string d { get; set; }

        [DataMember]
        public uint s_a { get; set; }

        [DataMember]
        public string s_si { get; set; }

        [DataMember]
        public decimal s_s { get; set; }

        [DataMember]
        public string s_sk { get; set; }

        [DataMember]
        public string s_bi { get; set; }

        [DataMember]
        public string s_bk { get; set; }

        [DataMember]
        public string s_n { get; set; }

        [DataMember]
        public string s_d { get; set; }

        [DataMember]
        public uint s_k { get; set; }
    }
}
