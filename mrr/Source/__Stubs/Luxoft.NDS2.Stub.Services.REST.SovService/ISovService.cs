﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using Luxoft.NDS2.Stub.Services.REST.SovService.Domain;

namespace Luxoft.NDS2.Stub.Services.REST.SovService
{
    [ServiceContract]
    public interface ISovService
    {
        [OperationContract]
        [WebGet(UriTemplate = "check?param={param}", ResponseFormat = WebMessageFormat.Json)]
        String Check(string param);

        [OperationContract]
        [WebGet(UriTemplate = "book?inn={inn}&year={year}&period={period}&isSalesBook={isSalesBook}&sellerInn={sellerInn}&buyerInn={buyerInn}&invoiceNo={invoiceNo}&invoiceDate={invoiceDate}", 
                RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        OperationResult<BookLine> getBook(string inn, string year, string period, string isSalesBook, string sellerInn, string buyerInn, string invoiceNo, string invoiceDate);


        [WebGet(UriTemplate = "errors?inn={inn}&year={year}&period={period}&isSalesBook={isSalesBook}", 
                RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        OperationResult<FlkLine> getFLK(string inn, string year, string period, string isSalesBook);

        [OperationContract]
        [WebGet(UriTemplate = "results?inn={inn}&year={year}&period={period}&minAlgNum={minAlgNum}&cardinality={cardinality}&isAutoRanked={isAutoRanked}", 
                RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        OperationResult<ResCompLine> getResComp(string inn, string year, string period, string minAlgNum, string cardinality, string isAutoRanked);

        [OperationContract]
        [WebGet(UriTemplate = "requestStatus?rid={rid}",
                RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        OperationResult getRequestStatus(int rid);

        [OperationContract]
        [WebGet(UriTemplate = "requestMismatches?rid={rid}",
                RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        OperationResult requestMismatches(int rid);
    }
}
