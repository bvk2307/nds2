﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Stub.Services.REST.SovService.Domain;
using Oracle.DataAccess.Client;
using Timer = System.Timers.Timer;

namespace Luxoft.NDS2.Stub.Services.REST.SovService.Services
{
    public class MismatchService
    {
        private static object _sync = new object();
        private static MismatchService _instance;
        Timer _timer = new Timer();
        CultureInfo _numberFormatProvider = new CultureInfo("en-GB");

        List<Task> _tasks = new List<Task>();

        private string MrrUserName
        {
            get
            {
                return ConfigurationManager.AppSettings["MRR_USER"];
            }
        }

        private string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["FIR"].ConnectionString; }
        }

        public static MismatchService Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_sync)
                    {
                        if (_instance == null)
                        {
                            _instance = new MismatchService();
                        }
                    }
                }

                return _instance;
            }
        }

        private MismatchService()
        {
            _timer.Interval = 60000;
            _timer.Elapsed += TaskCleaner;
            _timer.Start();
        }

        void TaskCleaner(object sender, ElapsedEventArgs e)
        {
            Console.WriteLine("Timer {0}", DateTime.Now);
            foreach (var task in _tasks)
            {
                Console.WriteLine("Task - {0} Status - {1}", task.Id, task.Status);
            }

            foreach (var source in _tasks.Where(t => t.Status == TaskStatus.RanToCompletion).ToList())
            {
                _tasks.Remove(source);
                source.Dispose();
            }
        }

        public int ProcessMissmatches(int rid)
        {
            int result = GetStatus(rid);

            Console.WriteLine("ProcessMissmatches check status = {0}", result);

            switch (result)
            {
                case 0:
                    //register
                    return RegisterRequestInternal(rid);
                    break;
                default:
                    return result;
                    break;
            }
        }

        /// <summary>
        /// register & process the request
        /// </summary>
        /// <param name="rid"></param>
        /// <returns></returns>
        private int RegisterRequestInternal(long rid)
        {
            Console.WriteLine("RegisterRequestInternal");
            int result = 1;
            try
            {
                var rBody = GetRequestBody(rid);
                var filter = SelectionFilter.Deserialize(rBody);
                _tasks.Add(Task.Factory.StartNew(() => StartProcessing(filter, rid)));
                SetRequestStatus(rid, result);
            }
            catch (Exception ex)
            {
                result = 9;
                SetRequestStatus(rid, result, ex.Message);
            }

            return result;
        }

        private void StartProcessing(SelectionFilter filter, long rid)
        {
            try
            {
                ProcessDeclarations(filter, rid);
                ProcessMismatches(filter, rid);
                SetStatusToMrr(rid, 1);
            }
            catch (Exception ex)
            {
                SetStatusToMrr(rid, 9);
                SetRequestStatus(rid, 9, ex.ToString());
            }

        }

        private void SetStatusToMrr(long rid, int statusId)
        {
            using (var conn = new OracleConnection(ConnectionString))
            {
                using (var cmd = new OracleCommand(string.Format("update {2}.SELECTIONS set STATUS = {0} where REQUEST_ID = {1}", statusId, rid, MrrUserName)))
                {
                    cmd.Connection = conn;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        private void ProcessDeclarations(SelectionFilter filter, long rid)
        {
            var declarationPart = CreateDeclarationQuery(filter);
            var mismatchesPart = CreateMismatchQuery(filter);

            var cmd = GetInsertCommand("SELECTION_DISCREPANCY", GetSelectCommand(declarationPart, mismatchesPart, string.Format("{0}, dr.id, 1", rid)));

            DebugWriteContent("decl.sql", cmd);

            ExecuteCmdText(cmd);
        }

        private void ProcessMismatches(SelectionFilter filter, long rid)
        {
            var declarationPart = CreateDeclarationQuery(filter);
            var mismatchesPart = CreateMismatchQuery(filter);

            var cmd = GetMergeCommand(GetSelectCommand(declarationPart, mismatchesPart, " mr.*"));
            var cmd2 = GetInsertCommand("MRR_DISCREPANCY_REF", GetSelectCommand(declarationPart, mismatchesPart, string.Format("{0}, mr.id, 1", rid)));

            DebugWriteContent("discr1.sql", cmd);
            DebugWriteContent("discr2.sql", cmd2);

            ExecuteCmdText(cmd);
            ExecuteCmdText(cmd2);



            Console.WriteLine("discrepantcy query: {0}", cmd);

            Console.WriteLine("discrepantcy ref query: {0}", cmd2);
        }

        private string GetInsertCommand(string tableName, string selectCmd)
        {
            return string.Format("insert into FIR.{0}\n{1}", tableName, selectCmd);
        }

        private string GetMergeCommand(string selectCmd)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("merge into fir.discrepancy t");
            sb.AppendLine("using(");
            sb.AppendLine(selectCmd);
            sb.AppendLine(") tmp on (tmp.id = t.discrepancy_id)");
            sb.AppendLine("when matched then update set t.inn = tmp.inn, t.status = decode(t.status, 1, 2, t.status) ");
            sb.AppendLine("when not matched then insert (discrepancy_id, discrepancy_date, discrepancy_type, compare_kind, rule_group, deal_amnt_total, discrepancy_amnt, tax_period, fiscal_year, inn, inn_contractor, invoice_id, decl_id, invoice_contractor_id, decl_contractor_id, status) ");
            sb.AppendLine("values (tmp.id, tmp.discrepancy_date, tmp.discrepancy_type, tmp.compare_kind, tmp.rule_group, tmp.deal_amnt_total, tmp.discrepancy_amnt, tmp.tax_period, tmp.fiscal_year, tmp.inn, tmp.inn_contractor, tmp.invoice_id, tmp.decl_id, tmp.invoice_contractor_id,tmp. decl_contractor_id, tmp.status) ");

            return sb.ToString();

        }

        private string GetSelectCommand(string declarationQuery, List<string> unionBlocks, string resultsetExpression)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("with decl as ({0})\n", declarationQuery);
            bool isFirstBlock = true;
            string unionWord = string.Empty;

            foreach (var unionBlock in unionBlocks)
            {
                sb.AppendLine(unionWord);
                sb.AppendFormat("({0})", unionBlock.Replace("__S__", resultsetExpression));

                if (isFirstBlock)
                {
                    unionWord = " union all ";
                    isFirstBlock = false;
                }
            }

            return sb.ToString();
        }

        private string CreateDeclarationQuery(SelectionFilter parameters)
        {
            var builder = new StringBuilder();
            var internalBuilder = new StringBuilder();

            builder.AppendFormat(@"select * from fir.declaration 
where 1=1 ");

            if (parameters.GroupsFilters.Any())
            {

                builder.AppendLine(" and (");
                foreach (var group in parameters.GroupsFilters)
                {
                    bool isFirstExpression = true;

                    foreach (var filterCriteria in group.Filters)
                    {
                        switch (filterCriteria.InternalName)
                        {
                            case "Region":
                                BuildExpresion(internalBuilder, filterCriteria, "region_code", true, ref isFirstExpression, typeof(string));
                                break;
                            case "TaxPeriod":
                                BuildExpresion(internalBuilder, filterCriteria, "Period_code", true, ref isFirstExpression, typeof(string));
                                break;
                            case "TaxPayerInn":
                                BuildExpresion(internalBuilder, filterCriteria, "inn", true, ref isFirstExpression, typeof(string));
                                break;
                            case "TaxOrgan":
                                BuildExpresion(internalBuilder, filterCriteria, "Soun_code", false, ref isFirstExpression, typeof(string));
                                break;
                            case "TaxPayerName":
                                BuildExpresion(internalBuilder, filterCriteria, "org_name", true, ref isFirstExpression, typeof(string));
                                break;
                            case "DiscrepancyType":
                                BuildExpresion(internalBuilder, filterCriteria, "Sign", false, ref isFirstExpression, typeof(int));
                                break;
                            case "MismatchAmntTotal":
                                BuildExpresion(internalBuilder, filterCriteria, "AmntMismatches_total", false, ref isFirstExpression, typeof(decimal));
                                break;
                            case "MismatchAmntMin":
                                BuildExpresion(internalBuilder, filterCriteria, "AmntMismatches_min", false, ref isFirstExpression, typeof(decimal));
                                break;
                            case "MismatchAmntMax":
                                BuildExpresion(internalBuilder, filterCriteria, "AmntMismatches_max", false, ref isFirstExpression, typeof(decimal));
                                break;
                            case "MismatchAmntAvg":
                                BuildExpresion(internalBuilder, filterCriteria, "AmntMismatches_avg", false, ref isFirstExpression, typeof(decimal));
                                break;
                        }
                    }
                    if (internalBuilder.Length > 0)
                    {
                        builder.AppendFormat("({0}) {1}\n", internalBuilder.ToString(), parameters.GroupsFilters.Last().Equals(group) ? string.Empty : "OR");
                        internalBuilder.Clear();
                    }
                    else
                    {
                        builder.Append(" 1=1 ");
                    }

                }

                //builder.Remove(builder.Length - 2, 2);

                builder.AppendLine(")");
            }



            return builder.ToString();
        }

        /// <summary>
        /// returns a bunch of group queries
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private List<string> CreateMismatchQuery(SelectionFilter parameters)
        {

            var result = new List<string>();
            string cmdCommonText = string.Format(@"
select distinct
__S__
from
FIR.DISCREPANCY_RAW mr
inner join decl dr on dr.id = mr.decl_id
where 
");

            var commandBuilder = new StringBuilder();
            var internalBuilder = new StringBuilder();

            if (parameters.GroupsFilters.Any())
            {
                foreach (var groupsFilter in parameters.GroupsFilters)
                {
                    bool isFirstExpression = true;

                    foreach (var filterCriteria in groupsFilter.Filters)
                    {
                        switch (filterCriteria.InternalName)
                        {
                            case "TaxPeriod":
                                BuildExpresion(internalBuilder, filterCriteria, "mr.tax_period", true, ref isFirstExpression, typeof(string));
                                break;
                            case "TaxPayerInn":
                                BuildExpresion(internalBuilder, filterCriteria, "mr.inn", true, ref isFirstExpression, typeof(string));
                                break;
                            case "DiscrepancyType":
                                BuildExpresion(internalBuilder, filterCriteria, "mr.discrepancy_type", false, ref isFirstExpression, typeof(int));
                                break;
                            case "DiscrepancyKind":
                                BuildExpresion(internalBuilder, filterCriteria, "mr.compare_kind", false, ref isFirstExpression, typeof(int));
                                break;
                            case "DiscrepancySum":
                                BuildExpresion(internalBuilder, filterCriteria, "mr.discrepancy_amnt", false, ref isFirstExpression, typeof(decimal));
                                break;
                            case "MismatchDate":
                                BuildExpresion(internalBuilder, filterCriteria, "mr.discrepancy_date", false, ref isFirstExpression, typeof(DateTime));
                                break;
                        }
                    }

                    if (internalBuilder.Length > 0)
                    {
                        commandBuilder.AppendFormat("\n--group: {0}\n", groupsFilter.Name);
                        commandBuilder.AppendLine(cmdCommonText);
                        commandBuilder.AppendLine(internalBuilder.ToString());

                        result.Add(commandBuilder.ToString());
                        internalBuilder.Clear();
                        commandBuilder.Clear();
                    }
                }
            }

            if (!result.Any())
            {
                result.Add(cmdCommonText + "1=1");
            }

            return result;
        }

        private void BuildExpresion(StringBuilder builder, FilterCriteria criteria, string criteriaName, bool isString, ref bool isFirstExpression, Type dataType)
        {
            string p1 = isFirstExpression ? string.Empty : " and ";
            if (isFirstExpression) isFirstExpression = false;

            bool isFirst = true;

            if (criteria.Values.Any())
            {
                builder.AppendFormat(" {0} (",  p1);

                foreach (var filterElement in criteria.Values)
                {
                    string p2 = isFirst ? string.Empty : " or ";
                    if (isString)
                    {
                        builder.AppendFormat("\n {0} {1} = '{2}'", p2, criteriaName, filterElement.ValueOne);
                    }
                    else
                    {

                        if (filterElement.Operator == FilterElement.ComparisonOperations.Between)
                        {
                            if (typeof(string) == dataType)
                            {
                                builder.AppendFormat("\n {0} ({1} between '{2}' and '{3}')", p2, criteriaName, filterElement.ValueOne.ToString(_numberFormatProvider), filterElement.ValueTwo.ToString(_numberFormatProvider));
                            }
                            else if (typeof(DateTime) == dataType)
                            {
                                builder.AppendFormat("\n {0} ({1} between  to_date('{2}', 'DD.MM.YYYY HH24:MI:SS') and  to_date('{3}', 'DD.MM.YYYY HH24:MI:SS'))", p2, criteriaName, filterElement.ValueOne.ToString(_numberFormatProvider), filterElement.ValueTwo.ToString(_numberFormatProvider));
                            }
                            else
                            {
                                builder.AppendFormat("\n {0} ({1} between {2} and {3})", p2, criteriaName, filterElement.ValueOne.ToString(_numberFormatProvider), filterElement.ValueTwo.ToString(_numberFormatProvider));
                            }

                        }
                        else
                        {
                            switch (filterElement.Operator)
                            {
                                case FilterElement.ComparisonOperations.Equals:
                                    builder.AppendFormat("\n {0} {1} = {2}", p2, criteriaName, filterElement.ValueOne.ToString(_numberFormatProvider));
                                    break;
                                case FilterElement.ComparisonOperations.NotEquals:
                                    builder.AppendFormat("\n {0} {1} <> {2}", p2, criteriaName, filterElement.ValueOne.ToString(_numberFormatProvider));
                                    break;
                                case FilterElement.ComparisonOperations.GT:
                                    builder.AppendFormat("\n {0} {1} > {2}", p2, criteriaName, filterElement.ValueOne.ToString(_numberFormatProvider));
                                    break;
                                case FilterElement.ComparisonOperations.LT:
                                    builder.AppendFormat("\n {0} {1} < {2}", p2, criteriaName, filterElement.ValueOne.ToString(_numberFormatProvider));
                                    break;
                            }


                        }

                    }

                    if(isFirst)
                    {
                        isFirst = false;
                    }
                }

                builder.AppendFormat(")");
            }

        }

        /// <summary>
        /// returns process status by request id
        /// </summary>
        /// <param name="rid"></param>
        /// <returns>
        /// -1 - not registered
        /// 0 - registered
        /// 1 - In progress
        /// 2 - Completed
        /// 9 - Error
        /// </returns>
        public int GetStatus(int rid)
        {
            int returnValue = -1;
            Console.WriteLine("Get status for rid = {0}", rid);
            using (OracleConnection conn = new OracleConnection(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand(string.Format("select status from FIR.MRR_REQUEST where request_id = {0}", rid)))
                {
                    cmd.Connection = conn;
                    conn.Open();

                    using (OracleDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            returnValue = int.Parse(reader[0].ToString());
                        }
                    }
                }
            }

            return returnValue;
        }

        /// <summary>
        /// sets the status to request
        /// </summary>
        /// <param name="rid"></param>
        /// <param name="statusId"></param>
        /// <param name="message"></param>
        private void SetRequestStatus(long rid, int statusId, string message = null)
        {
            Console.WriteLine("Changing status to {0} with message '{1}'", statusId, message);
            using (OracleConnection conn = new OracleConnection(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand(string.Format("update FIR.MRR_REQUEST set status = {0}, message = '{1}' where request_id = {2} ", statusId, message, rid)))
                {
                    cmd.Connection = conn;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// return the request xml body
        /// </summary>
        /// <param name="rid"></param>
        /// <returns></returns>
        public string GetRequestBody(long rid)
        {

            using (var conn = new OracleConnection(ConnectionString))
            {
                using (var cmd = new OracleCommand(string.Format("select request_body from FIR.MRR_REQUEST where request_id = {0}", rid)))
                {
                    cmd.Connection = conn;
                    conn.Open();
                    using (OracleDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            return reader[0].ToString();
                        }
                    }
                }
            }

            return null;
        }

        private int ExecuteCmdText(string sql)
        {

            using (var conn = new OracleConnection(ConnectionString))
            {
                using (var cmd = new OracleCommand(sql))
                {
                    cmd.Connection = conn;
                    conn.Open();
                    return cmd.ExecuteNonQuery();
                }
            }
        }

        private void DebugWriteContent(string fName, string content)
        {
            var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, fName);

            if (File.Exists(path))
            {
                File.Delete(path);
            }

            File.WriteAllText(path, content);
        }
    }
}
