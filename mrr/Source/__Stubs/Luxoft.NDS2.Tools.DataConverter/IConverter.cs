﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Luxoft.NDS2.Tools.DataConverter
{
    public interface IConverter
    {
        void Convert(string filePath);

        void Convert(Stream xmlStream);
    }
}
