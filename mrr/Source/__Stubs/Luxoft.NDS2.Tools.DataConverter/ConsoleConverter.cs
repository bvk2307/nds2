﻿//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using System.Xml;
//
//namespace Luxoft.NDS2.Tools.DataConverter
//{
//    public class ConsoleConverter : IConverter
//    {
//        #region Implementation of IConverter
//
//        public void Convert(string filePath)
//        {
//            if(!File.Exists(filePath))
//            {
//                throw new FileNotFoundException(filePath);
//            }
//
//            Convert(File.OpenRead(filePath));
//        }
//
//        public void Convert(Stream stream)
//        {
//            using(stream)
//            {
//                using (XmlReader reader = XmlReader.Create(stream))
//                {
//                    reader.MoveToContent();
//                    Dictionary<string, long> elmCounts = new Dictionary<string, long>();
//                    int tabsCount = 0;
//                    while(reader.Read())
//                    {
//                        if(reader.NodeType == XmlNodeType.Element)
//                        {
//                            if(elmCounts.ContainsKey(reader.Name))
//                            {
//                                
//                                elmCounts[reader.Name]++;
//                            }
//                            else
//                            {
//                                elmCounts.Add(reader.Name, 1);
//                            }
//                        }
//                        
//
////                        switch (reader.NodeType)
////                        {
////                            case XmlNodeType.Element:
////                                tabsCount++;
////                                break;
////                            case XmlNodeType.EndElement:
////                                tabsCount--;
////                                break;
////                            default:
////                                break;
////                        }
//
////                        Console.WriteLine("{0}{1} value:{2} type:{3}",
////                            new string('\t', tabsCount),
////                            reader.Name,
////                            reader.Value,
////                            reader.NodeType);
////                        if(reader.NodeType == XmlNodeType.Element)
////                        {
////                            for(int i = 0; i < reader.AttributeCount; i++)
////                            {
////                                reader.MoveToAttribute(i);
////                                Console.WriteLine("{0}\t-{1} value:{2}", new string('\t', tabsCount), reader.Name, reader.Value);
////                            }
////                        }
//
//                        
////                        if(tabsCount == 10000)
////                        {
////                            Console.WriteLine("{0} of {1} readed", stream.Position, stream.Length);
////                            tabsCount = 0;
////                        }
////
////                        tabsCount++;
//                    }
//
//
//                    foreach (var elmCount in elmCounts)
//                    {
//                        Console.WriteLine("{0} = {1}", elmCount.Key, elmCount.Value);
//                    }
//                }                
//            }
//        }
//
//        #endregion
//
//        public Dictionary<string, long> ConvertSellbook(byte[] bytes)
//        {
//            return ConvertSellbook(new MemoryStream(bytes));
//        }
//
//        public Dictionary<string, long> ConvertSellbook(Stream stream)
//        {
//            Dictionary<string, long> result = new Dictionary<string, long>();
//            using (stream)
//            {
//                using (XmlReader reader = XmlReader.Create(stream))
//                {
//                    reader.MoveToContent();
//                    
//                    while (reader.Read())
//                    {
//                        if (reader.NodeType == XmlNodeType.Element)
//                        {
//                            if (result.ContainsKey(reader.Name))
//                            {
//
//                                result[reader.Name]++;
//                            }
//                            else
//                            {
//                                result.Add(reader.Name, 1);
//                            }
//                        }
//                    }
//                }
//            }
//
//            return result;
//        }
//    }
//}
