﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using Luxoft.NDS2.SOVService.Search;

namespace Luxoft.NDS2.SOVService
{
    public class SovService : ISOVService
    {
        #region Implementation of ISOVServiceStub
        /// <summary>
        /// Получение книги
        /// </summary>
        /// <param name="inn">ИНН налогоплательщика</param>
        /// <param name="kpp">КПП налогоплательщика</param>
        /// <param name="bookType">Тип книги. 1-Книга покупок 2-Книга продаж</param>
        /// <param name="reportPeriod">Отчетный период.(занчения от 01 до 99)</param>
        /// <param name="reportYear">Отчетный год.(прим. 2001, 2014 и т.д)</param>
        /// <returns></returns>
        public string GetBook(string inn, string kpp, int bookType, string reportPeriod, string reportYear)
        {
            return BookSearch.GetBookContent(inn, kpp, bookType, reportPeriod, reportYear);
        }

        #endregion
    }
}
