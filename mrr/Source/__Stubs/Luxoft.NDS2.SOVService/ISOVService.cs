﻿using System.ServiceModel;

namespace Luxoft.NDS2.SOVService
{
    [ServiceContract]
    public interface ISOVService
    {
        /// <summary>
        /// Base64 encoded file
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [OperationContract]
        string GetBook(string inn, string kpp, int bookType, string reportPeriod, string reportYear);
    }
}
