﻿using System;
using System.Configuration;
using System.IO;


namespace Luxoft.NDS2.SOVService.Search
{
    public class BookSearch
    {
        public static string GetBookContent(string inn, string kpp, int type, string period, string year)
        {
            var searchPattern = string.Format("{0}-{1}-{2}-{3}", inn, kpp, period, year);
            var baseStoragePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory ,ConfigurationManager.AppSettings["storageBasePath"]);
            var bookFolder = Path.Combine(baseStoragePath, ConfigurationManager.AppSettings[type == 2 ? "sellBooksPath" : "buyBooksPath"]);
            var metadataDb = Path.Combine(baseStoragePath, ConfigurationManager.AppSettings["booksMetadataDb"]);

            //TODO: Guards
            if (!Directory.Exists(baseStoragePath)) throw new ApplicationException(string.Format("base storage path not found {0}", baseStoragePath));
            if (!Directory.Exists(bookFolder)) throw new ApplicationException(string.Format("book storage not found {0}", bookFolder));
            if (!File.Exists(metadataDb)) throw new ApplicationException(string.Format("book metadata file not found {0}", metadataDb));

            using (StreamReader reader = new StreamReader(metadataDb))
            {
                var line = reader.ReadLine();
                while (line != null)
                {
                    if(line.StartsWith(searchPattern))
                    {
                        var parts = line.Split(';');

                        using (FileStream fs = File.OpenRead(Path.Combine(bookFolder, parts[1])))
                        {
                            return Convert.ToBase64String(ReadToEnd(fs));
                        }
                    }

                    line = reader.ReadLine();
                }
            }

            return string.Empty;
        }


        private static byte[] ReadToEnd(System.IO.Stream stream)
        {
            long originalPosition = 0;

            if (stream.CanSeek)
            {
                originalPosition = stream.Position;
                stream.Position = 0;
            }

            try
            {
                byte[] readBuffer = new byte[4096];

                int totalBytesRead = 0;
                int bytesRead;

                while ((bytesRead = stream.Read(readBuffer, totalBytesRead, readBuffer.Length - totalBytesRead)) > 0)
                {
                    totalBytesRead += bytesRead;

                    if (totalBytesRead == readBuffer.Length)
                    {
                        int nextByte = stream.ReadByte();
                        if (nextByte != -1)
                        {
                            byte[] temp = new byte[readBuffer.Length * 2];
                            Buffer.BlockCopy(readBuffer, 0, temp, 0, readBuffer.Length);
                            Buffer.SetByte(temp, totalBytesRead, (byte)nextByte);
                            readBuffer = temp;
                            totalBytesRead++;
                        }
                    }
                }

                byte[] buffer = readBuffer;
                if (readBuffer.Length != totalBytesRead)
                {
                    buffer = new byte[totalBytesRead];
                    Buffer.BlockCopy(readBuffer, 0, buffer, 0, totalBytesRead);
                }
                return buffer;
            }
            finally
            {
                if (stream.CanSeek)
                {
                    stream.Position = originalPosition;
                }
            }
        }
    }
}