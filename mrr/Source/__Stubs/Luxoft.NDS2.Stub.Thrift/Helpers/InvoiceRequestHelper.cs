﻿using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;

namespace Luxoft.NDS2.Stub.Thrift.Helpers
{

    public static class InvoiceRequestHelper
    {
        private static string CopyInvoicesPattern = "BEGIN {0} END;";
        private static string CopySingleInvoice =
            @"INSERT INTO SOV_INVOICE
  SELECT :pId, t.*, 0 as IS_DOP_LIST, 0 as IS_IMPORT, NULL as CLARIFICATION_KEY, null as contragent_key
    FROM {1}.INVOICE_RAW t
   WHERE t.ROW_KEY = '{0}' OR t.ACTUAL_ROW_KEY = '{0}';
     /*AND NOT EXISTS (SELECT 1 FROM SOV_INVOICE x WHERE x.ROW_KEY = '{0}' OR x.ACTUAL_ROW_KEY = '{0}');*/
";
        private const int InvoiceRequestProcessedSuccessfully = 2;
        private const int MRRInvoiceRequestStatus = 4;
        private const string SuccessInvoicesRequest = "UPDATE INVOICE_REQUEST SET STATUS = :p1 WHERE ID = :p2 ";

        private static string ToCopyInvoiceSQL(this List<string> ids)
        {
            return string.Format(
                CopyInvoicesPattern,
                string.Join(" ",
                    ids.Select(
                        x =>
                            string.Format(
                                CopySingleInvoice,
                                x,
                                Config.SchemaName)
                    ).ToArray()));
        }

        public static void CopyInvoices(this OracleConnection connection, long requestId, List<string> ids)
        {
            using (var copyInvoices = new OracleCommand(ids.ToCopyInvoiceSQL(), connection))
            {
                copyInvoices.CommandType = CommandType.Text;
                copyInvoices.Parameters.Add(new OracleParameter("pId", requestId));

                copyInvoices.ExecuteNonQuery();

                copyInvoices.CommandText = SuccessInvoicesRequest;
                copyInvoices.Parameters.Clear();
                copyInvoices.Parameters.Add(new OracleParameter(":p1", OracleDbType.Int32) { Value = InvoiceRequestProcessedSuccessfully });
                copyInvoices.Parameters.Add(new OracleParameter(":p2", OracleDbType.Int64) { Value = requestId });
                copyInvoices.ExecuteNonQuery();
            }
        }

        public static void Watch(Action<string> outputWriter)
        {
            while (true)
            {
                long? requestId = null;

                using (var connect = new OracleConnection(Config.ConnectionString))
                {
                    try
                    {
                        connect.Open();
                        using (var readRequests =
                            new OracleCommand(
                                "BEGIN OPEN :pCursor FOR SELECT ID FROM INVOICE_REQUEST ID WHERE STATUS = :pStatus; END;",
                                connect))
                        {
                            readRequests.Parameters.Add(
                                new OracleParameter(
                                    "pCursor",
                                    OracleDbType.RefCursor,
                                    ParameterDirection.Output));
                            readRequests.Parameters.Add(
                                new OracleParameter("pStatus", MRRInvoiceRequestStatus));

                            var reader = readRequests.ExecuteReader();
                            if (reader.Read())
                            {
                                requestId = long.Parse(reader["ID"].ToString());
                            }
                        }


                        if (requestId.HasValue)
                        {
                            using (var processRequest =
                                new OracleCommand(
                                    "NDS2$EMULATOR.PROCESS_INVOICE_REQUEST",
                                    connect))
                            {
                                processRequest.CommandType = CommandType.StoredProcedure;
                                processRequest.Parameters.Add(
                                    new OracleParameter("pRequestId", requestId.Value));

                                processRequest.ExecuteNonQuery();
                            }
                        }
                    }
                    catch (Exception error)
                    {
                        outputWriter(error.Message);
                    }
                    finally
                    {
                        if (connect.State == ConnectionState.Open)
                        {
                            connect.Close();
                        }
                    }
                }

                if (!requestId.HasValue)
                {
                    Thread.Sleep(10000);
                }
            }
        }
    }
}
