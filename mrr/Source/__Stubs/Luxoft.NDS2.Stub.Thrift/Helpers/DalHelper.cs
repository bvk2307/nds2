﻿using Oracle.DataAccess.Client;
using System;
using System.Data;

namespace Luxoft.NDS2.Stub.Thrift.Helpers
{
    public static class DalHelper
    {
        private const string PackageName = "NDS2$EMULATOR";

        public static void Execute(
            CommandContext command,
            Action<OracleCommand> dalAction)
        {
            using (var connection = new OracleConnection(Config.ConnectionString))
            {
                connection.Open();

                using (var oraCommand = new OracleCommand(command.Text, connection))
                {
                    oraCommand.CommandType = command.Type;
                    oraCommand.Parameters.AddRange(command.Parameters);
                    oraCommand.BindByName = true;

                    try
                    {
                        dalAction(oraCommand);
                    }
                    finally
                    {
                        if (connection.State == ConnectionState.Open)
                        {
                            connection.Close();
                        }
                    }
                }
            }
        }

        public static string FullName(string procedureName)
        {
            return string.Format("{0}.{1}", PackageName, procedureName);
        }
    }
}
