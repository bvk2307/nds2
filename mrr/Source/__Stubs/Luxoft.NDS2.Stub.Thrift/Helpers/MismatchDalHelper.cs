﻿using System.IO;
using System.Security.Cryptography;
using Luxoft.NDS2.Stub.Thrift.Domain;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Stub.Thrift.Helpers
{
    public static class MismatchDalHelper
    {
        # region Константы

        private const string InsertRequestProcedure = "CREATE_MISMATCH_REQUEST";
        private const string UpdateRequestProcedure = "UPDATE_MISMATCH_REQUEST";
        private const string ParamId = "pId";
        private const string ParamBody = "pBody";
        private const string ParamStatus = "pStatus";

        # endregion

        private static CultureInfo _numberFormatProvider = new CultureInfo("en-GB");

        public static void DumpQuery(string fileName, string query)
        {
            using (StreamWriter sw = new StreamWriter(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, fileName)))
            {
                sw.Write(query);
            }
        }

        public static long CreateRequest(List<CriteriaGroup> filter, RequestStatus initialStatus)
        {
            long result = default(long);

            var body = filter.Serialize();

            DalHelper.Execute(
                new CommandContext()
                {
                    Text = DalHelper.FullName(InsertRequestProcedure),
                    Type = CommandType.StoredProcedure,
                    Parameters = 
                        new OracleParameter[] 
                        { 
                            new OracleParameter(
                                ParamId, 
                                OracleDbType.Decimal, 
                                ParameterDirection.Output), 
                            new OracleParameter(ParamBody, body),
                            new OracleParameter(ParamStatus, (int)initialStatus)
                        }
                },
                (cmd) => 
                {
                    cmd.ExecuteNonQuery();
                    result = long.Parse(cmd.Parameters[ParamId].Value.ToString());
                });

            return result;
        }

        public static void UpdateRequest(long requestId, RequestStatus status)
        {
            DalHelper.Execute(
                new CommandContext()
                {
                    Text = DalHelper.FullName(UpdateRequestProcedure),
                    Type = CommandType.StoredProcedure,
                    Parameters =
                        new OracleParameter[] 
                        { 
                            new OracleParameter(ParamId,requestId), 
                            new OracleParameter(ParamStatus, (int)status)
                        }
                },
                (cmd) =>
                {
                    cmd.ExecuteNonQuery();
                });
        }

        public static void ProcessDeclarations(List<CriteriaGroup> filter, long requestId)
        {
            DalHelper.Execute(
                new CommandContext()
                {
                    Type = System.Data.CommandType.Text,
                    Parameters = new OracleParameter[0],
                    Text =
                        GetInsertCommand(
                            "SELECTION_DECLARATION",
                            GetSelectCommand(
                                filter.ToDeclarationSQL(),
                                filter.ToMismatchSQL(),
                                string.Format("{0}, dr.id, 1", requestId))),
                },
                (cmd) => 
                {
                    cmd.ExecuteNonQuery();
                });
        }

        public static void ProcessMismatches(List<CriteriaGroup> filter, long requestId)
        {
            var declarationPart = filter.ToDeclarationSQL();
            var mismatchesPart = filter.ToMismatchSQL();

            var cmd = 
                GetMergeCommand(
                    GetSelectCommand(declarationPart, mismatchesPart, " mr.*"));
            var cmd2 = 
                GetInsertCommand(
                    "SELECTION_DISCREPANCY", 
                    GetSelectCommand(
                        declarationPart,
                        mismatchesPart, 
                        string.Format("{0}, mr.id, 1", requestId)));

            var cmd3 = GetHistoryCommand(GetSelectCommand(declarationPart, mismatchesPart, "mr.id, sysdate as submit_date, 1 as stage_id, 1 as status_id"));

            DalHelper.Execute(
                new CommandContext()
                {
                    Parameters = new OracleParameter[0],
                    Type = CommandType.Text,
                    Text = cmd
                },
                (command) =>
                {
                    command.ExecuteNonQuery();
                });

            DalHelper.Execute(
                new CommandContext()
                {
                    Parameters = new OracleParameter[0],
                    Type = CommandType.Text,
                    Text = cmd2
                },
                (command) =>
                {
                    command.ExecuteNonQuery();
                });

            DalHelper.Execute(
                new CommandContext()
                {
                    Parameters = new OracleParameter[0],
                    Type = CommandType.Text,
                    Text = cmd3
                },
                (command) =>
                {
                    command.ExecuteNonQuery();
                });

            Console.WriteLine("discrepantcy query: {0}", cmd);
            Console.WriteLine("discrepantcy ref query: {0}", cmd2);
            Console.WriteLine("discrepantcy history query: {0}", cmd3);

            DumpQuery("d1.sql", cmd);
            DumpQuery("d2.sql", cmd2);
            DumpQuery("d3.sql", cmd3);
        }

        private static string GetHistoryCommand(string select)
        {
            var result = new StringBuilder("insert into hist_discrepancy_stage(id, submit_date, stage_id, status_id)");
            result.AppendLine("select dis.* from (");
            result.AppendLine(select);
            result.AppendLine(") dis");
            result.AppendLine("left join hist_discrepancy_stage hist_dis on hist_dis.id = dis.id and hist_dis.stage_id = 1 and hist_dis.status_id = 1");
            result.AppendLine("where hist_dis.id is null");
            return result.ToString();
        }

        private static string GetMergeCommand(string selectCmd)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("merge into SOV_DISCREPANCY t");
            sb.AppendLine("using(");
            sb.AppendLine(selectCmd);
            sb.AppendLine(") tmp on (tmp.id = t.id)");
            sb.AppendLine("when matched then update set t.type = tmp.type, t.status = t.status ");
            sb.AppendLine("when not matched then insert (ID, CREATE_DATE, TYPE, rule_group, deal_amnt, amnt, AMOUNT_PVP, COURSE, COURSE_COST, SUR_CODE, invoice_rk, INVOICE_CHAPTER, decl_id, invoice_contractor_rk, INVOICE_CONTRACTOR_CHAPTER, decl_contractor_id, status) ");
            sb.AppendLine("values (tmp.id, tmp.create_date, tmp.type, tmp.rule_group, tmp.deal_amnt, tmp.amnt, tmp.AMOUNT_PVP, tmp.COURSE, tmp.COURSE_COST, tmp.SUR_CODE, tmp.invoice_rk, tmp.INVOICE_CHAPTER, tmp.decl_id, tmp.invoice_contractor_rk,tmp.INVOICE_CONTRACTOR_CHAPTER, tmp.decl_contractor_id, tmp.status) ");
            return sb.ToString();
        }

        private static string GetInsertCommand(string tableName, string selectCmd)
        {
            return string.Format("insert into {0}\n{1}", tableName, selectCmd);
        }

        private static string GetSelectCommand(string declarationQuery, List<string> unionBlocks, string resultsetExpression)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("with decl as ({0})\n", declarationQuery);
            bool isFirstBlock = true;
            string unionWord = string.Empty;

            foreach (var unionBlock in unionBlocks)
            {
                sb.AppendLine(unionWord);
                sb.AppendFormat("({0})", unionBlock.Replace("__S__", resultsetExpression));

                if (isFirstBlock)
                {
                    unionWord = " union all ";
                    isFirstBlock = false;
                }
            }

            return sb.ToString();
        }

        private static string ToDeclarationSQL(this List<CriteriaGroup> parameters)
        {
            var builder = new StringBuilder();
            var internalBuilder = new StringBuilder();

            builder.AppendFormat(@"select * from v$declaration where 1=1 ");

            if (parameters.Any())
            {
                builder.AppendLine(" and (");
                foreach (var group in parameters)
                {
                    bool isFirstExpression = true;

                    foreach (var filterCriteria in group.Criterias)
                    {
                        switch (filterCriteria.Attribute)
                        {
                            case Attribute.REGION:
                                internalBuilder.AppendSQL(filterCriteria, "region_code", false, ref isFirstExpression, typeof(string));
                                break;
                            case Attribute.PERIOD:
                                internalBuilder.AppendSQL(filterCriteria, "Period_code", true, ref isFirstExpression, typeof(string));
                                break;
                            case Attribute.INN:
                                internalBuilder.AppendSQL(filterCriteria, "inn", true, ref isFirstExpression, typeof(string));
                                break;
                            case Attribute.IFNS_NUMBER:
                                internalBuilder.AppendSQL(filterCriteria, "Soun_code", false, ref isFirstExpression, typeof(string));
                                break;
                            case Attribute.NP_NAME:
                                internalBuilder.AppendSQL(filterCriteria, "org_name", true, ref isFirstExpression, typeof(string));
                                break;
                            case Attribute.ND_PRIS:
                                internalBuilder.AppendSQL(filterCriteria, "Sign", false, ref isFirstExpression, typeof(int));
                                break;
                            case Attribute.MISMATCH_COMMON_SUM:
                                internalBuilder.AppendSQL(filterCriteria, "AmntMismatches_total", false, ref isFirstExpression, typeof(decimal));
                                break;
                            case Attribute.MISMATCH_MIN_SUM:
                                internalBuilder.AppendSQL(filterCriteria, "AmntMismatches_min", false, ref isFirstExpression, typeof(decimal));
                                break;
                            case Attribute.MISMATCH_MAX_SUM:
                                internalBuilder.AppendSQL(filterCriteria, "AmntMismatches_max", false, ref isFirstExpression, typeof(decimal));
                                break;
                            case Attribute.MISMATCH_AVG_SUM:
                                internalBuilder.AppendSQL(filterCriteria, "AmntMismatches_avg", false, ref isFirstExpression, typeof(decimal));
                                break;
                        }
                    }

                    if (internalBuilder.Length > 0)
                    {
                        builder.AppendFormat("({0}) {1}\n", internalBuilder.ToString(), parameters.Last().Equals(group) ? string.Empty : "OR");
                        internalBuilder.Clear();
                    }
                    else
                    {
                        builder.Append(" 1=1 ");
                    }

                }
                builder.AppendLine(")");
            }

            return builder.ToString();
        }

        private static List<string> ToMismatchSQL(this List<CriteriaGroup> parameters)
        {
            var result = new List<string>();
            string cmdCommonText = string.Format(@"
select distinct
__S__
from
FIR.DISCREPANCY_RAW mr
inner join decl dr on dr.id = mr.decl_id
where 
");

            var commandBuilder = new StringBuilder();
            var internalBuilder = new StringBuilder();

            if (parameters.Any())
            {
                foreach (var groupsFilter in parameters)
                {
                    bool isFirstExpression = true;

                    foreach (var filterCriteria in groupsFilter.Criterias)
                    {
                        switch (filterCriteria.Attribute)
                        {
                            case Attribute.PERIOD:
                                internalBuilder.AppendSQL(filterCriteria, "mr.tax_period", true, ref isFirstExpression, typeof(string));
                                break;
                            case Attribute.INN:
                                internalBuilder.AppendSQL(filterCriteria, "mr.inn", true, ref isFirstExpression, typeof(string));
                                break;
                            case Attribute.MISMATCH_TYPE:
                                internalBuilder.AppendSQL(filterCriteria, "mr.type", false, ref isFirstExpression, typeof(int));
                                break;
                            case Attribute.MISMATCH_SUM:
                                internalBuilder.AppendSQL(filterCriteria, "mr.amnt", false, ref isFirstExpression, typeof(decimal));
                                break;
                            case Attribute.MISMATCH_DATE:
                                internalBuilder.AppendSQL(filterCriteria, "mr.create_date", false, ref isFirstExpression, typeof(DateTime));
                                break;
                        }
                    }

                    if (internalBuilder.Length > 0)
                    {
                        commandBuilder.AppendLine(cmdCommonText);
                        commandBuilder.AppendLine(internalBuilder.ToString());

                        result.Add(commandBuilder.ToString());
                        internalBuilder.Clear();
                        commandBuilder.Clear();
                    }
                }
            }

            if (!result.Any())
            {
                result.Add(cmdCommonText + "1=1");
            }

            return result;
        }        

        private static void AppendSQL(
            this StringBuilder builder, 
            Criteria criteria, 
            string criteriaName, 
            bool isString, 
            ref bool isFirstExpression, 
            Type dataType)
        {
            var prefix = isFirstExpression 
                ? string.Empty 
                : " and ";
            isFirstExpression = false;

            if (criteria.AttributeValues.Any())
            {
                builder.AppendFormat(" {0} (", prefix);

                string val1 = criteria.AttributeValues.First();
                string val2 = 
                    criteria.AttributeValues.Count > 1 
                    ? criteria.AttributeValues.Skip(1).First()
                    : string.Empty;

                if (isString)
                {
                    builder.AppendFormat("\n {0} {1} = '{2}'", string.Empty, criteriaName, val1);
                }
                else
                {
                    if (criteria.MatchType == MatchType.BETWEEN)
                    {
                        if (typeof(string) == dataType)
                        {
                            builder.AppendFormat("\n {0} ({1} between '{2}' and '{3}')", string.Empty, criteriaName, val1.ToString(_numberFormatProvider), val2.ToString(_numberFormatProvider));
                        }
                        else if (typeof(DateTime) == dataType)
                        {
                            builder.AppendFormat("\n {0} ({1} between  to_date('{2}', 'DD.MM.YYYY HH24:MI:SS') and  to_date('{3}', 'DD.MM.YYYY HH24:MI:SS'))", string.Empty, criteriaName, val1.ToString(_numberFormatProvider), val2.ToString(_numberFormatProvider));
                        }
                        else
                        {
                            if (val2 == String.Empty)
                            {
                                builder.AppendFormat("\n {0} ({1} >= {2})", string.Empty, criteriaName, val1.ToString(_numberFormatProvider));
                            }
                            else
                            {
                                builder.AppendFormat("\n {0} ({1} between {2} and {3})", string.Empty, criteriaName, val1.ToString(_numberFormatProvider), val2.ToString(_numberFormatProvider));
                            }
                        }

                    }
                    else if (criteria.MatchType == MatchType.EXACT)
                    {
                        builder.AppendFormat("\n {0} {1} = {2}", string.Empty, criteriaName, val1.ToString(_numberFormatProvider));
                    }
                    else if (criteria.MatchType == MatchType.IN_SET)
                    {
                        builder.AppendFormat("\n {0} {1} in ({2})", string.Empty, criteriaName, val1.ToString(_numberFormatProvider));
                    }
                }

                builder.AppendFormat(")");
            }
        }
    }
}
