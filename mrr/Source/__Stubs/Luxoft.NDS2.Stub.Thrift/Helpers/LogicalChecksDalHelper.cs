﻿using System.IO;
using System.Security.Cryptography;
using Luxoft.NDS2.Stub.Thrift.Domain;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Stub.Thrift.Helpers
{
    public static class LogicalChecksDalHelper
    {
        # region Константы

        private const string InsertRequestProcedure = "CREATE_LK_REPORT_REQUEST";
        private const string UpdateRequestProcedure = "UPDATE_LK_REPORT_REQUEST";
        private const string FillRepotLKProcedure = "FILL_REPORT_LK";
        private const string ParamId = "pId";
        private const string pRequestId = "pRequestId";
        private const string ParamBody = "pBody";
        private const string ParamStatus = "pStatus";

        # endregion

        private static CultureInfo _numberFormatProvider = new CultureInfo("en-GB");

        public static void DumpQuery(string fileName, string query)
        {
            using (StreamWriter sw = new StreamWriter(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, fileName)))
            {
                sw.Write(query);
            }
        }

        public static long CreateRequest(int year, List<string> periods, string date, List<string> inspections, RequestStatus initialStatus)
        {
            long result = default(long);

            string body = String.Empty;
            body += string.Format("{0}", year);
            body += ";" + periods.ToArray().ToString();
            body += ";" + date;
            body += ";" + inspections.ToArray().ToString();

            DalHelper.Execute(
                new CommandContext()
                {
                    Text = DalHelper.FullName(InsertRequestProcedure),
                    Type = CommandType.StoredProcedure,
                    Parameters =
                        new OracleParameter[] 
                        { 
                            new OracleParameter(
                                ParamId, 
                                OracleDbType.Decimal, 
                                ParameterDirection.Output), 
                            new OracleParameter(ParamBody, body),
                            new OracleParameter(ParamStatus, (int)initialStatus)
                        }
                },
                (cmd) =>
                {
                    cmd.ExecuteNonQuery();
                    result = long.Parse(cmd.Parameters[ParamId].Value.ToString());
                });

            return result;
        }

        public static void UpdateRequest(long requestId, RequestStatus status)
        {
            DalHelper.Execute(
                new CommandContext()
                {
                    Text = DalHelper.FullName(UpdateRequestProcedure),
                    Type = CommandType.StoredProcedure,
                    Parameters =
                        new OracleParameter[] 
                        { 
                            new OracleParameter(ParamId,requestId), 
                            new OracleParameter(ParamStatus, (int)status)
                        }
                },
                (cmd) =>
                {
                    cmd.ExecuteNonQuery();
                });
        }

        public static void FillReport(long requestId)
        {
            DalHelper.Execute(
                new CommandContext()
                {
                    Text = DalHelper.FullName(FillRepotLKProcedure),
                    Type = CommandType.StoredProcedure,
                    Parameters =
                        new OracleParameter[] 
                        { 
                            new OracleParameter(pRequestId,requestId)
                        }
                },
                (cmd) =>
                {
                    cmd.ExecuteNonQuery();
                });
        }
    }
}
