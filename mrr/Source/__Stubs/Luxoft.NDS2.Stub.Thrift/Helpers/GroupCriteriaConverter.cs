﻿using Luxoft.NDS2.Common.Contracts.DTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Stub.Thrift.Helpers
{
    public static class GroupCriteriaConverter
    {
        private const string GroupSplitter = ".";
        private const string CriteriaSplitter = ";";
        private const string ValueSplitter = ",";
        private const string CriteriaFormat = "Attr={0} Match={2} Values={1}";

        public static string Serialize(this List<CriteriaGroup> groups)
        {
            return string.Join(
                GroupSplitter, 
                groups.Select(
                    group => 
                        string.Join(
                            CriteriaSplitter, 
                            group.Criterias.Select(
                                x => 
                                    string.Format(
                                        CriteriaFormat, 
                                        x.Attribute, 
                                        string.Join(ValueSplitter, x.AttributeValues), 
                                        x.MatchType))
                            .ToArray())
                ).ToArray());
        }

        private static FilterElement.ComparisonOperations ToOperation(this MatchType matchType)
        {
            switch (matchType)
            {
                case MatchType.BETWEEN: return FilterElement.ComparisonOperations.Between;
                case MatchType.CONTAINS: return FilterElement.ComparisonOperations.Contains;
                case MatchType.ENDS_WITH: return FilterElement.ComparisonOperations.EndWith;
                case MatchType.EXACT: return FilterElement.ComparisonOperations.Equals;
                case MatchType.STARTS_WITH: return FilterElement.ComparisonOperations.StartWith;
                default: throw new NotSupportedException();
            }
        }
    }
}
