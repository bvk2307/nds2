﻿using Oracle.DataAccess.Client;
using System.Data;

namespace Luxoft.NDS2.Stub.Thrift.Helpers
{
    public class CommandContext
    {
        public string Text
        {
            get;
            set;
        }

        public CommandType Type
        {
            get;
            set;
        }

        public OracleParameter[] Parameters
        {
            get;
            set;
        }
    }
}
