﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Stub.Thrift.Domain;
using Luxoft.NDS2.Stub.Thrift.Helpers;
using Luxoft.NDS2.Stub.Thrift.Services;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;

namespace Luxoft.NDS2.Stub.Thrift
{
    public class Service : RequestService.Iface
    {
        private const int InvoiceRequestProcessedSuccessfully = 1;
        private const int InvoiceRequestProcessedWithError = 2;

        private const string CreateRequest = "NDS2$EMULATOR.CREATE_INVOICE_REQUEST";
        private const string CopyInvoicesPattern = "BEGIN {0} END;";
        private const string SuccessInvoicesRequest = "UPDATE INVOICE_REQUEST SET STATUS = :p1 WHERE ID = :p2 ";
        private const string CopySingleInvoice = "INSERT INTO SOV_INVOICE SELECT :pId,t.* FROM {1}.INVOICE_RAW t WHERE t.ROW_KEY={0} AND NOT EXISTS(SELECT 1 FROM SOV_INVOICE x WHERE x.ROW_KEY={0});";
        
        public long sendBookDataRequest(BookDataRequest request)
        {
            try
            {
                return BookDataService.Instance.DoWork(request);
            }
            catch (Exception error)
            {
                Console.WriteLine(
                    string.Format(
                        "Ошибка sendBookDataRequest: {0}.\r\nСтек вызова: {1}", 
                        error.Message, 
                        error.StackTrace));
                throw error;
            }
        }

        public long sendInvoiceDataRequest(InvoiceDataRequest request)
        {
            Console.WriteLine("Request to process invoices");
            var requestId = default(long);

            using(var connection = new OracleConnection(Config.ConnectionString))
            {
                connection.Open();
                using (var transaction = connection.BeginTransaction())
                {
                    using (var createRequest = new OracleCommand(CreateRequest, connection))
                    {
                        createRequest.Parameters.Add(
                            new OracleParameter(
                                "pId", 
                                OracleDbType.Decimal, 
                                ParameterDirection.Output));
                        createRequest.CommandType = CommandType.StoredProcedure;

                        try
                        {
                            createRequest.ExecuteNonQuery();
                            requestId = Convert.ToInt64(createRequest.Parameters[0].Value.ToString());
                        }
                        catch (Exception)
                        {
                            transaction.Rollback();
                            connection.Close();
                            throw;
                        }
                    }

                    try
                    {
                        foreach (var key in request.InvoiceRecords.Keys)
                        {
                            connection.CopyInvoices(requestId, request.InvoiceRecords[key]);
                        }

                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                    
                }
            }

            return requestId;
        }

        public long sendMismatchRequest(MismatchRequest request)
        {
            System.Threading.Thread.Sleep(3000);

            if (request.Criterias == null || request.Criterias.Count == 0)
            {
                throw new ArgumentException("request.Criterias is null or empty");
            }

            return MismatchService.Instance.ProcessRequest(request);
        }

        public long sendNdsMismatchRequest(NdsMismatchRequest request)
        {
            return MismatchNdsService.Instance.ProcessRequest(
                request,
                (text) => Console.WriteLine(text));
        }

        public long sendLogicalChecksRequest(LogicalChecksRequest request)
        {
            return ReportLogicalChecksService.Instance.ProcessRequest(request);
        }

        public long sendPyramidRequest(PyramidRequest request)
        {
            return PyramidDataService.Instance.DoWork(request);
        }

        public long sendContragentDataRequest(ContragentDataRequest request)
        {
            return ContragentDataService.ContragentDataRequest(request);
        }

        public long sendContragentParamRequest(ContragentParamRequest request)
        {
            return ContragentDataService.ContragentParamsRequest(request);
        }

        private List<FilterCriteria> RealAllCriterias(OracleConnection connection)
        {
            var result = new List<FilterCriteria>();

            using (OracleCommand cmd = new OracleCommand("select * from Dict_Filter_Types", connection))
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var currFilterType = new FilterCriteria()
                        {
                            Name = reader["FILTER_TYPE"].ToString(),
                            ValueTypeCode = (FilterCriteria.ValueTypes)int.Parse(reader["VALUE_TYPE_CODE"].ToString()),
                            LookupTableName = reader["LOOKUP_TABLE_NAME"].ToString(),
                            InternalName = reader["INTERNAL_NAME"].ToString(),
                            FirInternalName = reader["FIR_INTERNAL_NAME"].ToString(),
                            IsRequired = Convert.ToBoolean(reader["IS_REQUIRED"])
                        };
                        result.Add(currFilterType);
                    }
                }
            }
            

            return result;
        }

        public void discardPyramidRequests(List<long> requestIds)
        {
            throw new NotImplementedException();
        }

        public long sendNavigatorDataRequest(NavigatorRequest request)
        {
            return NavigatorDataService.Instance.DoWork(request);
        }

        public long sendBookDataRequestV2(BookDataRequestV2 request)
        {
            return SovInvoiceService.Instance.DoWork(request);
        }

        public void nightModeOn()
        {
        }

        public void nightModeOff()
        {
        }

        public bool nightModeCheck()
        {
            return false;
        }
    }
}
