﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Luxoft.NDS2.Stub.Thrift.Domain
{
    
    public class SellBookLine
    {
        
        public string ИдЗаписиСФ { get; set; }
        
        public string КодВидОпер { get; set; }
        
        public string НомСчФПрод { get; set; }
        
        public DateTime ДатаСчФПрод { get; set; }
        
        public string ИННПокуп { get; set; }
        
        public string КПППокуп { get; set; }
        
        public decimal СтоимПродСФ { get; set; }
        
        public decimal СтоимПродСФ18 { get; set; }
        
        public decimal СтоимПродСФ10 { get; set; }
        
        public decimal СтоимПродСФ0 { get; set; }
        
        public decimal СумНДССФ18 { get; set; }
        
        public decimal СумНДССФ10 { get; set; }
        
        public decimal СтоимПродОсв { get; set; }
        
        public string ИННПрод { get; set; }
        
        public string КПППрод { get; set; }
        
        public string ИдТЗап { get; set; }
        
        public string НомАлг { get; set; }

        #region added by format
        
        public int Id { get; set; }
        
        public string НаимПок { get; set; }
        #endregion
    }
}
