﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Stub.Thrift.Domain
{
    public class MismatchGroup
    {
        public List<string> TaxOrganCodes { get; set; }
        public List<string> InnList { get; set; }
        
        public string TaxPayerName { get; set; }
        public string TaxPayerNameOperator { get; set; }

        public List<string> TaxPeriods { get; set; }

        public List<string> MissmatchTypes { get; set; }
        public  MismatchRangeCriteria SumCriteria { get; set; }
    }
}
