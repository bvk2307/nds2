﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Luxoft.NDS2.Stub.Thrift.Domain
{
    
    public class FlkLine
    {
        
        public uint f { get; set; }

        
        public uint sl { get; set; }

        
        public string k { get; set; }

        
        public string e { get; set; }

        
        public string a { get; set; }
    }
}
