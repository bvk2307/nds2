﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Luxoft.NDS2.Stub.Thrift.Domain
{
    public enum RequestStatus
    {
        NotRegistered = -1,
        Ok = 0,
        InProcess = 1,
        Completed = 2,
        NotSuccessInNightMode = 8,
        Error = 9
    }
}
