﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Luxoft.NDS2.Stub.Thrift.Domain
{
    
    public class BuyBookLine
    {
        
        public string ИдЗаписиСФ { get; set; }
        
        public string КодВидОпер { get; set; }
        
        public string НомСчФПрод { get; set; }
        
        public DateTime ДатаСчФПрод { get; set; }
        
        public string ИННПрод { get; set; }
        
        public string КПППрод { get; set; }
        
        public decimal СтоимПокупВ { get; set; }
        
        public decimal СумНДСВыч { get; set; }
        
        public string ИННПокуп { get; set; }
        
        public string КПППокуп { get; set; }
        
        public string ИдТЗап { get; set; }
        
        public string НомАлг { get; set; }
        
        #region added by format
        
        public int Id { get; set; }
        
        public string НомерПор { get; set; }
        
        public string НомТД { get; set; }
        
        public DateTime? ДатаДокПдтвОпл { get; set; }
        
        public string НомДокПдтвОпл { get; set; }
        
        public DateTime? ДатаКСчФПрод { get; set; }
        
        public string НомКСчФПрод { get; set; }
        
        public DateTime? ДатаИспрКСчФ { get; set; }
        
        public string НомИспрКСчФ{ get; set; }
        
        public DateTime? ДатаИспрСчФ { get; set; }
        
        public string НомИспрСчФ { get; set; }

        #endregion
    }
}
