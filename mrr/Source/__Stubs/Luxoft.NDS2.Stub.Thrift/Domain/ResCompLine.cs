﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Luxoft.NDS2.Stub.Thrift.Domain
{
    
    public class ResCompLine
    {
        
        public string k { get; set; }

        
        public uint ai { get; set; }

        
        public uint c { get; set; }

        
        public uint a { get; set; }

        
        public string si { get; set; }

        
        public decimal s { get; set; }

        
        public string sk { get; set; }

        
        public string bi { get; set; }

        
        public string bk { get; set; }

        
        public string n { get; set; }

        
        public string d { get; set; }

        
        public uint s_a { get; set; }

        
        public string s_si { get; set; }

        
        public decimal s_s { get; set; }

        
        public string s_sk { get; set; }

        
        public string s_bi { get; set; }

        
        public string s_bk { get; set; }

        
        public string s_n { get; set; }

        
        public string s_d { get; set; }

        
        public uint s_k { get; set; }
    }
}
