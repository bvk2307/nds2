﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Stub.Thrift.Domain
{
    public class MismatchParameters
    {
        public string DeclarationSetKey { get; set; }
        public Guid RequestId { get; set; }
        public string Region { get; set; }
        public int TaxPayerType { get; set; }
        public bool IsActive{ get; set; }
        public List<MismatchGroup> FilterGroups { get; set; }
    }
}
