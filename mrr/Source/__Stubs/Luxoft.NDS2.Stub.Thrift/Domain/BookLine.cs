﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Luxoft.NDS2.Stub.Thrift.Domain
{
    
    public class BookLine
    {
        
        public string k { get; set; }
        
        public string si { get; set; }
        
        public string sk { get; set; }
        
        public string bi { get; set; }
        
        public string bk { get; set; }
        
        public string n { get; set; }
        
        public string d { get; set; }
        
        public decimal s { get; set; }
        
        public decimal v { get; set; }
        
        public decimal sc { get; set; }
        
        public decimal s18 { get; set; }
        
        public decimal s10 { get; set; }
        
        public decimal s0 { get; set; }
        
        public decimal sn { get; set; }
        
        public decimal v18 { get; set; }
        
        public decimal v10 { get; set; }
        
        public string o { get; set; }
        
        public string t { get; set; }
        
        public string a { get; set; }
    }
}
