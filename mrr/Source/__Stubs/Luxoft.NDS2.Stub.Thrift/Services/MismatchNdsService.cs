﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using Timer = System.Timers.Timer;
using Luxoft.NDS2.Stub.Thrift.Domain;
using Luxoft.NDS2.Stub.Thrift.Helpers;
using System.Data;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Stub.Thrift.Services
{
    internal class MismatchNdsService
    {
        # region Multithread-safe singleton

        private static MismatchNdsService _instance = null;
        private static object _sync = new object();

        private MismatchNdsService()
        {
            _timer.Interval = 60000;
            _timer.Elapsed += TaskCleaner;
            _timer.Start();
        }

        public static MismatchNdsService Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_sync)
                    {
                        _instance = _instance ?? new MismatchNdsService();
                    }
                }

                return _instance;
            }
        }

        # endregion

        # region Background worker

        System.Timers.Timer _timer = new System.Timers.Timer();
        List<Task> _tasks = new List<Task>();

        private void TaskCleaner(object sender, ElapsedEventArgs e)
        {
            foreach (var source in _tasks.Where(t => t.Status == TaskStatus.RanToCompletion).ToList())
            {
                _tasks.Remove(source);
                source.Dispose();
            }
        }

        private void Start(Action action)
        {
            _tasks.Add(Task.Factory.StartNew(() => action()));
        }

        # endregion

        # region Interface

        private const string ProcessErrorPattern = "Ошибка обработки запроса NdsMismatch {0}";

        public long ProcessRequest(NdsMismatchRequest request, Action<string> outputWriter)
        {
            var result = default(long);

            try
            {
                result = CreateRequest(request);
                Start(() => ProcessAction(result, outputWriter));
            }
            catch (Exception error)
            {
                outputWriter(string.Format(ProcessErrorPattern, error.Message));
                throw error;
            }

            return result;
        }

        private const string UpdateStatusErrorPattern = "Не удалось изменить статус запроса расхождений. {0}.";
        private const string CopyDataErrorPattern = "Не удалось скопировать данные о расхождениях. {0}.";

        private void ProcessAction(long requestId, Action<string> outputWriter)
        {
            try 
            { 
                UpdateRequestStatus(requestId, RequestStatus.InProcess); 
            }
            catch (Exception error) 
            {
                outputWriter(string.Format(UpdateStatusErrorPattern, error.Message));
                return;
            }

            var dataCopied = false;
            try
            {
                CopyData(requestId);
                dataCopied = true;
            }
            catch (Exception error)
            {
                outputWriter(string.Format(CopyDataErrorPattern, error.Message));
            }

            try
            {
                UpdateRequestStatus(
                    requestId, 
                    dataCopied ? RequestStatus.Completed : RequestStatus.Error);
            }
            catch (Exception error)
            {
                outputWriter(string.Format(UpdateStatusErrorPattern, error.Message));
            }
        }

        # endregion

        # region Data operations

        private long CreateRequest(NdsMismatchRequest request)
        {
            var retVal = default(long);

            DalHelper.Execute(
                new CommandContext()
                {
                    Type = CommandType.StoredProcedure,
                    Text = "NDS2$EMULATOR.CREATE_DISCREPANCY_REQUEST",
                    Parameters =
                        new OracleParameter[]
                        {
                            new OracleParameter("pId", OracleDbType.Decimal, ParameterDirection.Output),
                            new OracleParameter("pInn", request.Inn),
                            new OracleParameter("pPeriod", request.Period),
                            new OracleParameter("pYear", request.Year),
                            new OracleParameter("pCorrectionNumber", request.CorrectionNumber),
                            new OracleParameter("pType", (int)request.MismatchType),
                            new OracleParameter("pKind", (int)request.MismatchKind)
                        }
                },
                (cmd) =>
                {
                    cmd.ExecuteNonQuery();
                    retVal = long.Parse(cmd.Parameters["pId"].Value.ToString());
                });

            return retVal;
        }

        private void UpdateRequestStatus(long requestId, RequestStatus status)
        {
            DalHelper.Execute(
                new CommandContext()
                {
                    Type = CommandType.Text,
                    Text = "BEGIN UPDATE DISCREPANCY_REQUEST SET STATUS = :pStatus WHERE ID = :pId; END;",
                    Parameters =
                        new OracleParameter[]
                        {
                            new OracleParameter("pId", requestId),
                            new OracleParameter("pStatus", (int)status)
                        }
                },
                (cmd) => { cmd.ExecuteNonQuery(); });
        }

        private void CopyData(long requestId)
        {
            DalHelper.Execute(
                new CommandContext()
                {
                    Type = CommandType.StoredProcedure,
                    Text = "NDS2$EMULATOR.COPY_DISCREPANCIES",
                    Parameters =
                        new OracleParameter[]
                        {
                            new OracleParameter("pRequestId", requestId)
                        }
                },
                (cmd) => { cmd.ExecuteNonQuery(); });
        }

        # endregion
    }
}
