﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using Luxoft.NDS2.Stub.Thrift.Domain;
using Luxoft.NDS2.Stub.Thrift.Helpers;
using Timer = System.Timers.Timer;

namespace Luxoft.NDS2.Stub.Thrift.Services
{
    public class MismatchService
    {
        # region Multithread-safe singleton

        private static object _sync = new object();
        private static MismatchService _instance;

        private MismatchService()
        {
            _timer.Interval = 60000;
            _timer.Elapsed += TaskCleaner;
            _timer.Start();
        }

        public static MismatchService Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_sync)
                    {
                        if (_instance == null)
                        {
                            _instance = new MismatchService();
                        }
                    }
                }

                return _instance;
            }
        }

        # endregion

        # region Background worker

        Timer _timer = new Timer();        
        List<Task> _tasks = new List<Task>();

        private void TaskCleaner(object sender, ElapsedEventArgs e)
        {
            Console.WriteLine("Timer {0}", DateTime.Now);
            foreach (var task in _tasks)
            {
                Console.WriteLine("Task - {0} Status - {1}", task.Id, task.Status);
            }

            foreach (var source in _tasks.Where(t => t.Status == TaskStatus.RanToCompletion).ToList())
            {
                _tasks.Remove(source);
                source.Dispose();
            }
        }

        # endregion


        # region Работа с запросом расхождений

        public long ProcessRequest(MismatchRequest request)
        {
            Console.WriteLine(string.Format("Запрошены расхождения {0}", request.Criterias.Serialize()));
            long requestId;

            if (
                (requestId = 
                    MismatchDalHelper.CreateRequest(request.Criterias, RequestStatus.Ok)) 
                    != default(long))
            {            
                Console.WriteLine(string.Format("Поиск расхождений по запросу {0}", requestId));

                try
                {
                    _tasks.Add(Task.Factory.StartNew(() => Find(request.Criterias, requestId)));
                    MismatchDalHelper.UpdateRequest(requestId, RequestStatus.InProcess);
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex);
                    MismatchDalHelper.UpdateRequest(requestId, RequestStatus.Error);
                }
            }

            return requestId;
        }

        private void Find(List<CriteriaGroup> filter, long requestId)
        {
            try
            {
                MismatchDalHelper.ProcessDeclarations(filter, requestId);
                MismatchDalHelper.ProcessMismatches(filter, requestId);
                MismatchDalHelper.UpdateRequest(requestId, RequestStatus.Completed);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                MismatchDalHelper.UpdateRequest(requestId, RequestStatus.Error);
            }
        }

        # endregion

        # region Вспомогательные методы

        private void DebugWriteContent(string fName, string content)
        {
            var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, fName);

            if (File.Exists(path))
            {
                File.Delete(path);
            }

            File.WriteAllText(path, content);
        }

        # endregion
    }
}
