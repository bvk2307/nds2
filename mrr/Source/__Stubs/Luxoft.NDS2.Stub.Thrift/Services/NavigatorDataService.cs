﻿using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Stub.Thrift.Helpers;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace Luxoft.NDS2.Stub.Thrift.Services
{
    internal class NavigatorDataService
    {
        # region Singleton

        private NavigatorDataService()
        {
            _timer.Interval = 60000;
            _timer.Elapsed += TaskCleaner;
            _timer.Start();
        }

        private static NavigatorDataService _instance;

        public static NavigatorDataService Instance
        {
            get
            {
                return _instance = (_instance ?? new NavigatorDataService());
            }
        }

        # endregion

        # region Background Processing

        System.Timers.Timer _timer = new System.Timers.Timer();
        List<Task> _tasks = new List<Task>();

        private void TaskCleaner(object sender, ElapsedEventArgs e)
        {
            foreach (var source in _tasks.Where(t => t.Status == TaskStatus.RanToCompletion).ToList())
            {
                _tasks.Remove(source);
                source.Dispose();
            }
        }

        private void Start(Action action)
        {
            _tasks.Add(Task.Factory.StartNew(() => action()));
        }

        # endregion

        # region Интерфейс

        public long DoWork(NavigatorRequest request)
        {
            var result = CreateRequest(request);
            _tasks.Add(Task.Factory.StartNew(() => ProcessRequest(result)));

            return result;
        }

        private void ProcessRequest(long id)
        {
            Thread.Sleep(3000);

            /*if (TestDataExists())
            {
                Thread.Sleep(2000);
            }
            else
            {
                GenerateTestData();
            }*/

            DalHelper.Execute(
                new CommandContext()
                {
                    Type = CommandType.StoredProcedure,
                    Text = "PAC$NAVIGATOR.PROCESS_NAVIGATOR_REQUEST",
                    Parameters = new OracleParameter[] { new OracleParameter("pRequestId", id) }
                },
                (arg) => arg.ExecuteNonQuery());
        }

        # endregion

        # region Закрытые методы

        private long CreateRequest(NavigatorRequest request)
        {
            long id = 0;

            DalHelper.Execute(
                new CommandContext()
                {
                    Text = "PAC$NAVIGATOR.CREATE_NAVIGATOR_REQUEST",
                    Type = CommandType.StoredProcedure,
                    Parameters =
                        new OracleParameter[]
                        {
                            new OracleParameter("pRequestId", OracleDbType.Decimal, ParameterDirection.Output),
                            new OracleParameter("pMonthFrom", request.MonthFrom),
                            new OracleParameter("pYearFrom", request.YearFrom),
                            new OracleParameter("pMonthTo", request.MonthFrom),
                            new OracleParameter("pYearTo", request.YearTo),
                            new OracleParameter("pDepth", request.Depth),
                            new OracleParameter("pMinMappedAmount", request.MinMappedAmount),
                            new OracleParameter("pMaxContractorsQuantity", request.MaxContractorsQuantity),
                            new OracleParameter("pMinBuyerAmount", request.MinBuyerAmount),
                            new OracleParameter("pMinSellerAmount", request.MinSellerAmount)
                        }
                },
                (arg) =>
                {
                    arg.ExecuteNonQuery();
                    id = long.Parse(arg.Parameters["pRequestId"].Value.ToString());
                });

            foreach (var taxPayerInn in request.Contractors)
            {
                DalHelper.Execute(
                    new CommandContext()
                    {
                        Text = "PAC$NAVIGATOR.INSERT_NAVIGATOR_TAXPAYER",
                        Type = CommandType.StoredProcedure,
                        Parameters =
                            new OracleParameter[]
                        {
                            new OracleParameter("pRequestId", id),
                            new OracleParameter("pInn", taxPayerInn)
                        }
                    },
                    (arg) =>
                    {
                        arg.ExecuteNonQuery();
                    });
            }

            return id;
        }
        
        private bool TestDataExists()
        {
            var hasData = false;

            DalHelper.Execute(
                new CommandContext()
                {
                     Text = "BEGIN SELECT COUNT(*) INTO :pOutput FROM TEST_PYRAMID_SUMMARY; END;",
                     Type = CommandType.Text,
                     Parameters = 
                     new OracleParameter[]
                     {
                         new OracleParameter("pOutput", OracleDbType.Decimal, ParameterDirection.Output)
                     }
                },
                (cmd) =>
                {
                    cmd.ExecuteNonQuery();
                    hasData = int.Parse(cmd.Parameters["pOutput"].Value.ToString()) > 0;
                }
            );

            return hasData;
        }

        private const int InnMin = 1000000000;
        private const int InnMax = int.MaxValue;
        private const int KppMin = 10000000;
        private const int KppMax = 88888888;

        private string RandomInn(int id)
        {
            var key = (long)new Random().Next(InnMin, InnMax);
            key = 100 * key + id;

            return key.ToString();
        }

        private string RandomKpp(int id)
        {
            var key = 10 * (new Random().Next(KppMin, KppMax)) + id;

            return key.ToString();
        }

        private void GenerateContractor(int id, string name)
        {
            DalHelper.Execute(
                new CommandContext()
                {
                     Text = "NDS2$EMULATOR.GENERATE_CONTRACTOR",
                     Type = CommandType.StoredProcedure,
                     Parameters =
                        new OracleParameter[]
                        {
                            new OracleParameter("pId", id),
                            new OracleParameter("pName", name),
                            new OracleParameter("pInn", RandomInn(id)),
                            new OracleParameter("pKpp", RandomKpp(id))
                        }
                },
                (arg) => arg.ExecuteNonQuery());
        }

        private void GenerateTestData()
        {
            var contractorTypes = new string[]
            {
                "ЗАО",
                "ООО",
                "ОАО",
                "ИП"
            };

            var contractorPostfix = new string[]
            {
                "Лимитед",
                "Россия",
                "Интернешнл",
                "Профешнл"
            };

            var contractorNames = new string[]
            {
                "Альфа",
                "Бета",
                "Гамма",
                "Дельта",
                "Сигма",
                "Омега",
                "Эпсилон",
                "Ромашка",
                "Лютик",
                "Ландыш",
                "Василек",
                "Люксофт"                
            };

            var id = 1;
            foreach(var prefix in contractorTypes)
            {
                foreach(var name in contractorNames)
                {
                    foreach(var postfix in contractorPostfix)
                    {
                        GenerateContractor(id, string.Format("{0} \"{1} {2}\"", prefix, name, postfix));
                        id++;
                    }
                }
            }
        }

        # endregion
    }
}
