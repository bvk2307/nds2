﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using Luxoft.NDS2.Stub.Thrift.Domain;
using Luxoft.NDS2.Stub.Thrift.Helpers;
using Timer = System.Timers.Timer;

namespace Luxoft.NDS2.Stub.Thrift.Services
{
    public class ReportLogicalChecksService
    {
        # region Multithread-safe singleton

        private static object _sync = new object();
        private static ReportLogicalChecksService _instance;

        private ReportLogicalChecksService()
        {
            _timer.Interval = 60000;
            _timer.Elapsed += TaskCleaner;
            _timer.Start();
        }

        public static ReportLogicalChecksService Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_sync)
                    {
                        if (_instance == null)
                        {
                            _instance = new ReportLogicalChecksService();
                        }
                    }
                }

                return _instance;
            }
        }

        # endregion

        # region Background worker

        Timer _timer = new Timer();
        List<Task> _tasks = new List<Task>();

        private void TaskCleaner(object sender, ElapsedEventArgs e)
        {
            Console.WriteLine("Timer {0}", DateTime.Now);
            foreach (var task in _tasks)
            {
                Console.WriteLine("Task - {0} Status - {1}", task.Id, task.Status);
            }

            foreach (var source in _tasks.Where(t => t.Status == TaskStatus.RanToCompletion).ToList())
            {
                _tasks.Remove(source);
                source.Dispose();
            }
        }

        # endregion


        # region Работа с запросом расхождений

        public long ProcessRequest(LogicalChecksRequest request)
        {
            string periods = String.Empty;
            foreach (string item in request.Periods)
            {
                periods += item + ";";
            }
            string inspections = String.Empty;
            foreach (string item in request.Inspections)
            {
                inspections += item + ";";
            }

            Console.WriteLine(string.Format("Запрошен отчет по проверкам ЛК год = \"{0}\", periods = \"{1}\", inspections = \"{2}\"", request.Year, periods, request.Date, inspections));
            long requestId;

            if (
                (requestId =
                    LogicalChecksDalHelper.CreateRequest(request.Year, request.Periods, request.Date, request.Inspections, RequestStatus.Ok))
                    != default(long))
            {
                Console.WriteLine(string.Format("Поиск расхождений по запросу {0}", requestId));

                try
                {
                    _tasks.Add(Task.Factory.StartNew(() => Find(request.Year, request.Periods, request.Date, request.Inspections, requestId)));
                    LogicalChecksDalHelper.UpdateRequest(requestId, RequestStatus.InProcess);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    LogicalChecksDalHelper.UpdateRequest(requestId, RequestStatus.Error);
                }
            }

            return requestId;
        }

        private void Find(int year, List<string> periods, string date, List<string> inspections, long requestId)
        {
            try
            {
                LogicalChecksDalHelper.FillReport(requestId);
                LogicalChecksDalHelper.UpdateRequest(requestId, RequestStatus.Completed);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                LogicalChecksDalHelper.UpdateRequest(requestId, RequestStatus.Error);
            }
        }

        # endregion
    }
}
