﻿using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Stub.Thrift.Helpers;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace Luxoft.NDS2.Stub.Thrift.Services
{
    public class SovInvoiceService
    {
        # region Singleton

        private SovInvoiceService()
        {
            _timer.Interval = 60000;
            _timer.Elapsed += TaskCleaner;
            _timer.Start();
        }

        private static SovInvoiceService _instance;

        public static SovInvoiceService Instance
        {
            get
            {
                return _instance = (_instance ?? new SovInvoiceService());
            }
        }

        # endregion

        # region Background Processing

        System.Timers.Timer _timer = new System.Timers.Timer();
        List<Task> _tasks = new List<Task>();

        private void TaskCleaner(object sender, ElapsedEventArgs e)
        {
            foreach (var source in _tasks.Where(t => t.Status == TaskStatus.RanToCompletion).ToList())
            {
                _tasks.Remove(source);
                source.Dispose();
            }
        }

        private void Start(Action action)
        {
            _tasks.Add(Task.Factory.StartNew(() => action()));
        }

        private void ProcessInvoiceCopy(long id)
        {
            DalHelper.Execute(
                new CommandContext()
                {
                    Text = "NDS2$EMULATOR.PROCESS_INVOICE_REQUEST",
                    Type = CommandType.StoredProcedure,
                    Parameters =
                       new OracleParameter[]
                        {
                            new OracleParameter("pId", id)
                        }
                },
                (cmd) =>
                {
                    cmd.ExecuteNonQuery();
                });
        }

        # endregion

        # region Работа с БД

        private long CreateRequest(BookDataRequestV2 request)
        {
            long retval = 0;
            var paramId = new OracleParameter("pId", OracleDbType.Decimal, ParameterDirection.Output);

            DalHelper.Execute(
                new CommandContext()
                {
                    Text = "NDS2$EMULATOR.CREATE_INVOICE_REQUEST",
                    Type = CommandType.StoredProcedure,
                    Parameters =
                       new OracleParameter[]
                        {
                            paramId,
                            new OracleParameter("pDeclarationId", request.Zip),
                            new OracleParameter("pPartition", request.PartitionNumber),
                            new OracleParameter("pPriority", request.Priority)
                        }
                },
                (cmd) =>
                {
                    cmd.ExecuteNonQuery();
                    retval= long.Parse(paramId.Value.ToString());
                });

            return retval;
        }

        # endregion

        # region Интерфейс

        public long DoWork(BookDataRequestV2 request)
        {
            var id = CreateRequest(request);

            try
            {
                Start(() => ProcessInvoiceCopy(id));
            }
            catch(Exception)
            {
                throw;
            }

            return id;
        }

        # endregion
    }
}
