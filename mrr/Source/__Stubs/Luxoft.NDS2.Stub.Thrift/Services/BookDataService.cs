﻿using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Stub.Thrift.Helpers;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace Luxoft.NDS2.Stub.Thrift.Services
{
    public class BookDataService
    {
        # region Singleton

        private BookDataService()
        {
            _timer.Interval = 60000;
            _timer.Elapsed += TaskCleaner;
            _timer.Start();
        }

        private static BookDataService _instance;

        public static BookDataService Instance
        {
            get
            {
                return _instance = (_instance ?? new BookDataService());
            }
        }

        # endregion

        # region Background Processing

        System.Timers.Timer _timer = new System.Timers.Timer();
        List<Task> _tasks = new List<Task>();

        private void TaskCleaner(object sender, ElapsedEventArgs e)
        {
            foreach (var source in _tasks.Where(t => t.Status == TaskStatus.RanToCompletion).ToList())
            {
                _tasks.Remove(source);
                source.Dispose();
            }
        }

        private void Start(Action action)
        {
            _tasks.Add(Task.Factory.StartNew(() => action()));
        }

        private void ProcessInvoiceCopy(BookDataRequestInfo requestInfo)
        {
            var step = 100;
            var processed = 0;

            UpdateStatus(requestInfo.RequestId, RequestStatusType.InProcess);
            while (processed < requestInfo.InvoiceQuantity)
            {
                if (!Copy(
                    requestInfo.RequestId,
                    requestInfo.DeclarationId,
                    processed,
                    processed + step))
                {
                    UpdateStatus(requestInfo.RequestId, RequestStatusType.Error);
                    break;
                }

                processed += step;
                Thread.Sleep(2000);
            }
            UpdateStatus(requestInfo.RequestId, RequestStatusType.Completed);
        }

        # endregion

        # region Работа с БД

        private BookDataRequestInfo CreateRequest(BookDataRequest request)
        {
            var retval = new BookDataRequestInfo();
            var paramId = new OracleParameter("pId", OracleDbType.Decimal, ParameterDirection.Output);
            var paramQuantity = new OracleParameter("pResultRecordQuantity", OracleDbType.Decimal, ParameterDirection.Output);
            var paramDeclarationId = new OracleParameter("pDeclarationId", OracleDbType.Decimal, ParameterDirection.Output);

            DalHelper.Execute(
                new CommandContext()
                {
                    Text = "NDS2$EMULATOR.CREATE_BOOK_DATA_REQUEST",
                    Type = CommandType.StoredProcedure,
                    Parameters =
                       new OracleParameter[]
                        {
                            paramId,
                            new OracleParameter("pInn", request.Inn),
                            new OracleParameter("pCorrectionNumber", request.CorrectionNumber.ToString()),
                            new OracleParameter("pPartitionNumber", request.PartitionNumber),
                            new OracleParameter("pPeriod", request.Period.ToString()),
                            new OracleParameter("pYear", request.Year.ToString()),
                            new OracleParameter("pPriority", request.Priority.ToString()),
                            paramQuantity,
                            paramDeclarationId
                        }
                },
                (cmd) =>
                {
                    cmd.ExecuteNonQuery();
                    retval.RequestId = long.Parse(paramId.Value.ToString());
                    retval.DeclarationId = long.Parse(paramDeclarationId.Value.ToString());
                    retval.InvoiceQuantity = long.Parse(paramQuantity.Value.ToString());
                });

            return retval;
        }

        private void UpdateStatus(long requestId, RequestStatusType status)
        {
            DalHelper.Execute(
                new CommandContext()
                {
                    Text = "NDS2$EMULATOR.UPDATE_BOOK_DATA_REQUEST",
                    Type = CommandType.StoredProcedure,
                    Parameters =
                       new OracleParameter[]
                        {
                            new OracleParameter("pId", requestId),
                            new OracleParameter("pStatus", (int)status)
                        }
                },
                (cmd) =>
                {
                    cmd.ExecuteNonQuery();
                });
        }

        private bool Copy(long requestId, long declarationId, int indexFrom, int indexTo)
        {
            DalHelper.Execute(
                new CommandContext()
                {
                    Text = "NDS2$EMULATOR.COPY_BOOK_DATA",
                    Type = CommandType.StoredProcedure,
                    Parameters =
                    new OracleParameter[]
                        {
                            new OracleParameter("pRequestId", requestId),
                            new OracleParameter("pDeclarationId", declarationId),
                            new OracleParameter("pIndexFrom", indexFrom),
                            new OracleParameter("pIndexTo", indexTo)
                        }
                },
                (cmd) =>
                {
                    cmd.ExecuteNonQuery();
                });

            return true;
        }

        # endregion

        # region Интерфейс

        public long DoWork(BookDataRequest request)
        {
            var requestInfo = CreateRequest(request);

            try
            {
                Start(() => ProcessInvoiceCopy(requestInfo));
            }
            catch(Exception)
            {
                UpdateStatus(requestInfo.RequestId, RequestStatusType.Error);
                throw;
            }

            return requestInfo.RequestId;
        }

        # endregion
    }

    public class BookDataRequestInfo
    {
        public long RequestId { get; set; }

        public long DeclarationId { get; set; }

        public long InvoiceQuantity { get; set; }
    }
}
