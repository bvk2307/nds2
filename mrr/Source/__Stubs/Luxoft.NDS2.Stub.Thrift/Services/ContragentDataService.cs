﻿using System;
using System.Data;
using Luxoft.NDS2.Stub.Thrift.Helpers;
using Oracle.DataAccess.Client;

namespace Luxoft.NDS2.Stub.Thrift.Services
{
    class ContragentDataService
    {
        private const string DATA_INSERT_PATTERN = "NDS2$EMULATOR.CREATE_CAGNT_DATA_REQUEST";
        private const string DATA_GENERATE_PATTERN = "NDS2$EMULATOR.GENERATE_CAGNT_DATA";

        private const string PARAMS_INSERT_PATTERN = "NDS2$EMULATOR.CREATE_CAGNT_PARAMS_REQUEST";
        private const string PARAMS_GENERATE_PATTERN = "NDS2$EMULATOR.GENERATE_CAGNT_PARAMS";

        public static long ContragentDataRequest(ContragentDataRequest request)
        {
            Console.WriteLine("Request contragents list");
            var requestId = default(long);
            var paramId = new OracleParameter("pId", OracleDbType.Decimal, ParameterDirection.Output);

            DalHelper.Execute(
                new CommandContext
                {
                    Text = DATA_INSERT_PATTERN,
                    Type = CommandType.StoredProcedure,
                    Parameters =
                        new[]
                        {
                            paramId,
                            new OracleParameter("pInn", request.Inn),
                            new OracleParameter("pCorrectionNumber", request.CorrectionNumber.ToString()),
                            new OracleParameter("pPeriod", request.Period.ToString()),
                            new OracleParameter("pYear", request.Year.ToString())
                        }
                },
                cmd =>
                {
                    cmd.ExecuteNonQuery();
                    requestId = long.Parse(paramId.Value.ToString());
                });

            if (requestId > 0)
            {
                DalHelper.Execute(
                    new CommandContext
                    {
                        Text = DATA_GENERATE_PATTERN,
                        Type = CommandType.StoredProcedure,
                        Parameters =
                            new[]
                            {
                                new OracleParameter("pId", requestId),
                                new OracleParameter("pInn", request.Inn),
                                new OracleParameter("pCorrectionNumber", request.CorrectionNumber.ToString()),
                                new OracleParameter("pPeriod", request.Period.ToString()),
                                new OracleParameter("pYear", request.Year.ToString())
                            }
                    },
                    res => res.ExecuteNonQuery());
            }

            return requestId;
        }

        public static long ContragentParamsRequest(ContragentParamRequest request)
        {
            Console.WriteLine("Request contragent params");
            var requestId = default(long);
            var paramId = new OracleParameter("pId", OracleDbType.Decimal, ParameterDirection.Output);

            DalHelper.Execute(
                new CommandContext
                {
                    Text = PARAMS_INSERT_PATTERN,
                    Type = CommandType.StoredProcedure,
                    Parameters =
                        new[]
                        {
                            paramId,
                            new OracleParameter("pInn", request.Inn),
                            new OracleParameter("pContractorInn", request.Inn),
                            new OracleParameter("pCorrectionNumber", request.CorrectionNumber.ToString()),
                            new OracleParameter("pPeriod", request.Period.ToString()),
                            new OracleParameter("pYear", request.Year.ToString())
                        }
                },
                cmd =>
                {
                    cmd.ExecuteNonQuery();
                    requestId = long.Parse(paramId.Value.ToString());
                });

            if (requestId > 0)
            {
                DalHelper.Execute(
                    new CommandContext
                    {
                        Text = PARAMS_GENERATE_PATTERN,
                        Type = CommandType.StoredProcedure,
                        Parameters =
                            new[]
                            {
                                new OracleParameter("pId", requestId),
                                new OracleParameter("pInn", request.Inn),
                                new OracleParameter("pCorrectionNumber", request.CorrectionNumber),
                                new OracleParameter("pPeriod", request.Period.ToString()),
                                new OracleParameter("pYear", request.Year.ToString())
                            }
                    },
                    res =>
                    {
                        res.ExecuteNonQuery();
                    });
            }

            return requestId;
        }
    }
}
