﻿using System.Configuration;

namespace Luxoft.NDS2.Stub.Thrift
{
    public static class Config
    {
        private const string ConnectionName = "db";
        private const string MCSchema = "mc_schema";
        private const string PortConfigKey = "port";
        private const string TimeoutConfigKey = "timeout";
        private const string MrrUserKey = "mrr_user";

        public static string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings[ConnectionName].ConnectionString;
            }
        }

        public static string SchemaName
        {
            get { return Read(MCSchema);  }
        }

        public static string MrrUser
        {
            get { return Read(MrrUserKey); }
        }

        public static int Port
        {
            get { return int.Parse(Read(PortConfigKey)); }
        }

        public static int Timeout
        {
            get { return int.Parse(Read(TimeoutConfigKey)); }
        }

        private static string Read(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }
    }
}
