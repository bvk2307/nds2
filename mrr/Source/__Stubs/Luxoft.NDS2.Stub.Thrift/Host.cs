﻿using Luxoft.NDS2.Stub.Thrift.Helpers;
using System;
using System.Configuration;
using System.Threading;
using Thrift;
using Thrift.Server;
using Thrift.Transport;

namespace Luxoft.NDS2.Stub.Thrift
{
    class Host
    {
        static void Main(string[] args)
        {
            var server = new TSimpleServer(
                new RequestService.Processor(new Service()),
                new TServerSocket(Config.Port, Config.Timeout));
            
            Console.WriteLine("Starting service... Press ctrl+c to stop service.");

            new Thread(
                new ThreadStart(
                    () => InvoiceRequestHelper.Watch(text => Console.WriteLine(text))))
                .Start();

            Console.WriteLine("Invoice request background processor started ...");

            server.Serve();
        }
    }
}
