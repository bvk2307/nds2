﻿using Luxoft.NDS2.Common.SGDS;
using System;

namespace Luxoft.NDS2.Stub.SGDS
{
    /// <summary>
    /// Этот класс представляет контекст вызова GetFileStatusList операции эмулятора
    /// </summary>
    [Serializable]
    public class GetStatusListContext : OperationContext
    {
        /// <summary>
        /// Возвращает или задает входные данные операции
        /// </summary>
        public GetStatusListContract Contract
        {
            get;
            set;
        }
    }
}
