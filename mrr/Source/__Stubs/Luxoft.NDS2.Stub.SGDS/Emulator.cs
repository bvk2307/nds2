﻿using Luxoft.NDS2.Common.SGDS;
using System;
using System.IO;
using System.Xml.Serialization;

namespace Luxoft.NDS2.Stub.SGDS
{
    /// <summary>
    /// Этот класс реализует эмулятор СГДС интерфейса
    /// </summary>
    public class Emulator : Provider
    {
        # region Конструкторы

        /// <summary>
        /// Создает новый эксземпляр класса Emulator
        /// </summary>
        /// <param name="configuration"></param>
        public Emulator(ProviderConfiguration configuration)
            : base(configuration)
        {
        }

        # endregion

        # region Эмуляция СГДС интерфейса

        /// <summary>
        /// Вычитывает текущую конфигурацию и логирует контекст вызова
        /// </summary>
        /// <param name="user"></param>
        /// <param name="data"></param>
        /// <returns>Сконфигурированный результат операции</returns>
        public override OperationResult Upload(string user, FileUploadContract data)
        {
            var config = ReadConfig();
            var result =
                config.FileUploadResult.IsSuccess 
                ? (OperationResult)config.FileUploadResultData
                : new OperationFailure(config.FileUploadResult.ErrorMessage);

            LogContext<FileUploadContext>(BuildContext(user, result, data));

            return result;
        }

        /// <summary>
        /// Вычитывает текущую конфигурацию и логирует контекст вызова
        /// </summary>
        /// <param name="user"></param>
        /// <param name="guid"></param>
        /// <returns>Сконфигурированный результат операции</returns>
        public override OperationResult GetStatus(string user, string guid)
        {
            var config = ReadConfig();
            var result = 
                config.GetStatusResult.IsSuccess 
                ? (OperationResult)config.GetStatusResultData : 
                  new OperationFailure(config.GetStatusResult.ErrorMessage);

            LogContext<GetStatusContext>(BuildContext(user, result, guid));

            return result;
        }

        /// <summary>
        /// Вычитывает текущую конфигурацию и логирует контекст вызова
        /// </summary>
        /// <param name="user"></param>
        /// <param name="data"></param>
        /// <returns>Сконфигурированный результат операции</returns>
        public override OperationResult GetStatusChanges(string user, GetStatusListContract data)
        {
            var config = ReadConfig();
            var result =
                config.GetStatusListResult.IsSuccess
                ? (OperationResult)config.GetStatusListResultData :
                  new OperationFailure(config.GetStatusListResult.ErrorMessage);

            LogContext<GetStatusListContext>(BuildContext(user, result, data));

            return result;
        }

        # endregion

        # region Построение контекста операции

        private FileUploadContext BuildContext(string user, OperationResult result, FileUploadContract data)
        {
            return new FileUploadContext()
            {
                 Configuration = _configuration,
                 Contract = data,
                 ReceivedAt = DateTime.Now,
                 Result = result, 
                 User = user
            };
        }

        private GetStatusContext BuildContext(string user, OperationResult result, string guid)
        {
            return new GetStatusContext()
            {
                Configuration = _configuration,
                ReceivedAt = DateTime.Now,
                Result = result,
                Guid = guid,
                User = user
            };
        }

        private GetStatusListContext BuildContext(string user, OperationResult result, GetStatusListContract data)
        {
            return new GetStatusListContext()
            {
                Configuration = _configuration,
                Contract = data,
                ReceivedAt = DateTime.Now,
                Result = result,
                User = user
            };
        }

        # endregion

        # region Чтение конфигурации

        public const string ConfigFileName = "SGDSEmulatorConfig.xml";

        /// <summary>
        /// Вычитывает параметры конфигурации эмулятора из текстового файла
        /// </summary>
        /// <returns></returns>
        private static EmulatorConfiguration ReadConfig()
        {
            var serializer = new XmlSerializer(typeof(EmulatorConfiguration));

            using (var fileStream = new FileStream(ConfigFileName, FileMode.Open))
            {
                try
                {
                    return (EmulatorConfiguration)serializer.Deserialize(fileStream);
                }
                catch (FileNotFoundException)
                {
                    return EmulatorConfiguration.Default();
                }
            }
        }

        # endregion

        # region Логирование запросов

        public const string LogFileName = "SGDSEmulatorLog.txt";

        private static void LogContext<T>(T context)
        {
            var serializer = new XmlSerializer(typeof(T), new Type[1]{ typeof(OperationResult)});

            using (var streamWriter = File.AppendText(LogFileName))
            {
                serializer.Serialize(streamWriter, context);
                streamWriter.Flush();
                streamWriter.Close();
            }
        }

        # endregion
    }
}
