﻿using Luxoft.NDS2.Common.SGDS;
using System;

namespace Luxoft.NDS2.Stub.SGDS
{
    /// <summary>
    /// Этот класс представляет контекст вызова метода эмулятора Upload. Контекст сериализуется в файл для проверки корректности входных параметров.
    /// </summary>
    [Serializable]
    public class FileUploadContext : OperationContext
    {
        /// <summary>
        /// Возвращает или задает контракт с входными параметрами, переданными эмулятору
        /// </summary>
        public FileUploadContract Contract 
        {
            get;
            set;
        }
    }
}
