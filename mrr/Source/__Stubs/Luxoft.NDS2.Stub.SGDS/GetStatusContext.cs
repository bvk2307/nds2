﻿using Luxoft.NDS2.Common.SGDS;
using System;

namespace Luxoft.NDS2.Stub.SGDS
{
    /// <summary>
    /// Этот класс представляет контекст вызова метода GetStatus
    /// </summary>
    [Serializable]
    public class GetStatusContext : OperationContext
    {
        /// <summary>
        /// Возвращает или задает идентификатор файла в СГДС
        /// </summary>
        public string Guid
        {
            get;
            set;
        }
    }
}
