﻿using Luxoft.NDS2.Common.SGDS;
using System;
using System.Xml.Serialization;

namespace Luxoft.NDS2.Stub.SGDS
{
    /// <summary>
    /// Этот класс представляет базовую совокупность входных/выходных параметров эмулятора интерфейса СГДС
    /// </summary>
    [Serializable]
    [XmlInclude(typeof(FileUploadContext))]
    [XmlInclude(typeof(GetStatusContext))]
    [XmlInclude(typeof(GetStatusListContext))]
    public class OperationContext
    {
        /// <summary>
        /// Возвращает или задает пользователя, переданного эмулятору
        /// </summary>
        public string User
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает дату и время обращения к эмулятору
        /// </summary>
        public DateTime ReceivedAt
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает результат, который эмулятор вернул клиенту
        /// </summary>
        public OperationResult Result
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает СГДС конфигурацию, переданную эмулятору клиентом
        /// </summary>
        public ProviderConfiguration Configuration
        {
            get;
            set;
        }
    }
}
