﻿using Luxoft.NDS2.Common.SGDS;
using System;
using System.Xml.Serialization;

namespace Luxoft.NDS2.Stub.SGDS
{
    /// <summary>
    /// Этот класс представляет совокупность конфигурационных параметров эмулятора
    /// </summary>
    [Serializable]
    [XmlRoot("emulator.config", Namespace = "")]
    public class EmulatorConfiguration
    {
        /// <summary>
        /// Возвращает или задает натройку результата ,который эмулятор возвращает на вызов метода Upload.
        /// </summary>
        [XmlElement("uploadresult")]
        public OperationResultDefinition FileUploadResult
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает натройку результата ,который эмулятор возвращает на вызов метода GetStatus.
        /// </summary>
        [XmlElement("getstatusresult")]
        public OperationResultDefinition GetStatusResult
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает натройку результата ,который эмулятор возвращает на вызов метода GetStatusList.
        /// </summary>
        [XmlElement("getstatuslistresult")]
        public OperationResultDefinition GetStatusListResult
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает данные, которые эмулятор должен вернуть на вызов метода Upload в случае если он сконфигурирован возвращать результат успешной операции
        /// </summary>
        [XmlElement("uploaddata")]
        public FileUploadSuccess FileUploadResultData
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает данные, которые эмулятор должен вернуть на вызов метода GetStatus в случае если он сконфигурирован возвращать результат успешной операции
        /// </summary>
        [XmlElement("getstatusdata")]
        public GetFileStatusSuccess GetStatusResultData
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает данные, которые эмулятор должен вернуть на вызов метода GetStatusList в случае если он сконфигурирован возвращать результат успешной операции
        /// </summary>
        [XmlElement("getstatuslistdata")]
        public GetStatusListSuccess GetStatusListResultData
        {
            get;
            set;
        }

        internal static EmulatorConfiguration Default()
        {
            return new EmulatorConfiguration()
            {
                FileUploadResult = new OperationResultDefinition() { IsSuccess = true },
                GetStatusResult = new OperationResultDefinition() { IsSuccess = true },
                GetStatusListResult = new OperationResultDefinition() { IsSuccess = true },
                FileUploadResultData = new FileUploadSuccess(Guid.NewGuid().ToString()),
                GetStatusResultData =
                    new GetFileStatusSuccess(new FileStatus()
                        {
                            DateLastChange = DateTime.Now.ToString(ConversionHelper.DateTimeFormat),
                            StateId = 0,
                            Description = string.Empty,
                            ErrorDescription = string.Empty
                        }),
                GetStatusListResultData =
                    new GetStatusListSuccess("<?xml version=\"1.0\" encoding=\"windows-1251\"?><StatusListFile><File GUID_FILE=\"{252DDBE6-E78C-4250-9E5B-815A30415F9A}\" Status=\"5\" DescStatus=\"Файл не найден\" DescErrorStatus=\"По указанному пути файл не найден.C:\\TEMP\\\" DescLastChangeStatus=\"2007-10-05T10:52:37.517\"/><File GUID_FILE=\"{252DDBE6-E78C-4250-9E5B-815A30415F9A}\" Status=\"5\" DescStatus=\"Файл не найден\" DescErrorStatus=\"По указанному пути файл не найден.C:\\TEMP\\\" DescLastChangeStatus=\"2007-10-05T10:52:37.517\"/></StatusListFile>")
            };
        }
    }

    /// <summary>
    /// Этот класс представляет базовый набор параметро кнфигурации ответа эмулятора
    /// </summary>
    [Serializable]
    public class OperationResultDefinition
    {
        /// <summary>
        /// Возвращает или задает признак успешного выполнения операции
        /// </summary>
        [XmlAttribute("success")]
        public bool IsSuccess
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает описание ошибки выполнения операции
        /// </summary>
        [XmlAttribute("error")]
        public string ErrorMessage
        {
            get;
            set;
        }
    }
}
