﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data.SqlServerCe;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Luxoft.NDS2.Client.Helpers.CacheProviders;
using Luxoft.NDS2.Common.Contracts.Converters;
using Luxoft.NDS2.Common.Contracts.DTO.Business;
using Luxoft.NDS2.Common.Contracts.DTO.Business.SellBook;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.ConsoleTestHelper.DataProviders;
using Luxoft.NDS2.Tools.DataConverter;

namespace Luxoft.NDS2.ConsoleTestHelper
{
    class Program
    {
        static void Main(string[] args)
        {
            if(!args.Any())
            {
                Console.WriteLine("Usage: programm.exe xmlfile.xml");
                return;
            }

            foreach (var s in args)
            {
                if(s.Equals("-d"))
                {
                    System.Diagnostics.Debugger.Launch();
                }
            }

            SqlCeCacheProvider provider = new SqlCeCacheProvider();
//            var sClient = new MyServiceClient("BasicHttpBinding_IService1");
//            
//            var receivedStream = sClient.GetSellBook();
//            
//            Console.Write("data received");
//            Console.ReadLine();
//            
//            var obj = SellBookConverter.ToContract(receivedStream);
//            provider.CacheItems("testTable1", obj.Lines);


            Console.WriteLine("Starting read data");
            Console.ReadLine();
            //var data = null;// provier.SelectItems<SellBookLine>("select * from testTable1 order by BuyerName desc, InvoiceNumber asc;");
            Console.WriteLine("Press to exit");
            Console.ReadLine();

//            var sClient = new MyServiceClient("BasicHttpBinding_IService1");
//
//            var receivedStream = sClient.GetSellBook();
//
//            Console.Write("data received");
//            Console.ReadLine();
//
//            var obj = SellBookConverter.ToContract(receivedStream);
//
//            Console.Write("data converted");
//            Console.ReadLine();
//
//            foreach (var sellBookLine in obj.Lines)
//            {
//                Console.WriteLine("{3}\nName - {0} \nInn - {1}\nTotal - {2}", sellBookLine.BuyerName, sellBookLine.BuyerInn, sellBookLine.TotalSum, new string('#', 10));
//            }
//
//            GC.Collect();
//
//            Console.Write("stream cleared frmo GC");
//            Console.ReadLine();
//                
//            Console.WriteLine("Press to exit");
//            Console.ReadLine();                
        }
    }
}
