﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlServerCe;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Tools.DataConverter;

namespace Luxoft.NDS2.ConsoleTestHelper.DataProviders
{
    public class DataProviderWithLocalStorageCache : IDisposable
    {
        private string _connectionString;
        //private Guid _session;
        private SqlCeConnection _connection;
        private ISOVServiceStub _service;
        private const string _dbFileName = "TestCeDatabase.sdf";
        private readonly string _contextKey;

        public DataProviderWithLocalStorageCache(Guid contextId, ISOVServiceStub service)
        {
            _service = service;
            
            _connectionString = string.Format("Data source={0};", _dbFileName);
            _contextKey = contextId.ToString().Replace("-", string.Empty);
            CreateDatabase();
            _connection = new SqlCeConnection(_connectionString);
            _connection.Open();
        }

        #region Implementation of IDataProvider

        public void CreateDatabase()
        {
            if (!File.Exists(_dbFileName))
            {
                using(var dbEngine = new SqlCeEngine(_connectionString))
                {
                    dbEngine.CreateDatabase();
                }
            }
        }

        public bool TableExist(string tableName)
        {
            bool result = false;
            string command = string.Format("select 1 from Information_Schema.Tables where table_name = @tbName");
            using (SqlCeCommand cmd = new SqlCeCommand(command, _connection))
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = command;
                cmd.Parameters.AddWithValue("tbName", tableName);
                var res = cmd.ExecuteScalar();

                result = res != null;
            }

            return result;
        }

        public bool CreateTableIfNotExist(string table)
        {
            if(!TableExist(table))
            {
                using (SqlCeCommand cmd = new SqlCeCommand(string.Format("create table {0} (id varchar(32), cnt int);", table), _connection))
                {
                    cmd.CommandType = CommandType.Text;
                    var res = cmd.ExecuteScalar();
                }

                return true;
            }

            return false;
        }
        #endregion

        #region Implementation of ISOVServiceStub

        public Dictionary<string, long> GetSellBookInfo()
        {

            string cachedTableName = string.Format("SellBook_{0}", _contextKey);
            Dictionary<string, long> result= new Dictionary<string, long>();
            if(TableExist(cachedTableName))
            {
                Console.WriteLine("gettin cached data");
                using (SqlCeCommand cmd = new SqlCeCommand(string.Format("select * from {0};", cachedTableName), _connection))
                {
                    using (SqlCeDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            result.Add(reader.GetString(0), reader.GetInt32(1));
                        }
                    }
                }

                return result;
            }
            else
            {
                ConsoleConverter conv = new ConsoleConverter();
                result = conv.ConvertSellbook(_service.GetSellBook());
                
                using (SqlCeCommand cmd = new SqlCeCommand())
                {
                    cmd.Connection = _connection;
                    cmd.CommandText = string.Format("create table SellBook_{0} (k nvarchar(128), cnt int);", _contextKey);

                    cmd.ExecuteNonQuery();

                    cmd.CommandText = string.Format("insert into {0} values(@v1, @v2)", cachedTableName);
                    foreach (var l in result)
                    {
                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("v1", l.Key);
                        cmd.Parameters.AddWithValue("v2", l.Value);

                        cmd.ExecuteNonQuery();
                    }
                }

                return result;
            }
        }

        #endregion


        #region Implementation of IDisposable

        private bool _disposed;
        public void Dispose()
        {
            Dispose(true);
        }

        private void Dispose(bool disposing)
        {
            if(disposing)
            {
                disposing = false;
                _connection.Dispose();
            }
        }

        #endregion
    }
}
