﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.Services;

namespace Luxoft.NDS2.ConsoleTestHelper
{
    class MyServiceClient : ClientBase<ISOVServiceStub>, ISOVServiceStub
    {
        public MyServiceClient(string endpointName) : base(endpointName)
        {
            
        }

        #region Implementation of ISOVServiceStub

        public byte[] GetSellBook()
        {
            return Channel.GetSellBook();
        }

        public BookHeader[] SearchBook(string inn, string kpp, int bookType, string quarter)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
