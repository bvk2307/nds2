﻿using System;
using System.Linq;
using FLS.CommonComponents.Lib.Serialization;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting.TaxPayer;

namespace Luxoft.NDS2.Client.NodeTreePanel
{
    [Serializable]
    public sealed class GraphTreeParameters : ICloneable
    {
        public GraphNodesKeyParameters KeyParameters { get; private set; }

        public int MaxNodesInLevel { get; private set; }

        /// <summary> Node layer count limitation that is smaller of GraphTreeNodeData.Level on 1. There is not a limitation if it does not have a positive value. </summary>
        public int GraphLayersDepthLimit { get; private set; }

        public GraphTreeParameters(DeclarationSummary declarationSummary)
        {
            KeyParameters = new GraphNodesKeyParameters(
                declarationSummary.COMPENSATION_AMNT < 0,
                CalculateQuarter(declarationSummary.FULL_TAX_PERIOD),
                CalculatYear(declarationSummary.FULL_TAX_PERIOD),
                new[] { new TaxPayerId(declarationSummary.INN, declarationSummary.KPP) });

            InitFromConstructor();
        }

        public GraphTreeParameters(DeclarationBrief declarationBrief)
        {
            KeyParameters = new GraphNodesKeyParameters(
                isByPurchase: declarationBrief.COMPENSATION_AMNT.HasValue && declarationBrief.COMPENSATION_AMNT.Value < 0,
                quarter: CalculateQuarter(declarationBrief.FULL_TAX_PERIOD),
                year: CalculatYear(declarationBrief.FULL_TAX_PERIOD),
                ids: new[] { new TaxPayerId(declarationBrief.INN, declarationBrief.KPP) });

            InitFromConstructor();
        }

        public GraphTreeParameters(TaxPayerId taxPayerId, int year, int quarter, bool isPurchase)
        {
            KeyParameters = new GraphNodesKeyParameters(isPurchase, quarter, year, new[] { taxPayerId });

            InitFromConstructor();
        }

        public static GraphTreeParameters New(GraphTreeParameters treeParameters, TaxPayerId taxPayerId = null, bool? isByPurchase = null, int? year = null, int? quarter = null,
            int? sharedNdsPercent = null, int? maxNodesinLevel = null, int? graphLayersDepthLimit = null, int? mostImportantContractors = null, bool? isRoot = null, bool? showReverted = null)
        {
            GraphTreeParameters treeParametersNew = treeParameters.Clone();

            if (taxPayerId != null || isByPurchase.HasValue || year.HasValue || quarter.HasValue || sharedNdsPercent.HasValue || mostImportantContractors.HasValue || maxNodesinLevel.HasValue || isRoot.HasValue || showReverted.HasValue)
                treeParametersNew.KeyParameters = GraphNodesKeyParameters.New(
                    treeParameters.KeyParameters,
                    isByPurchase,
                    year,
                    quarter,
                    sharedNdsPercent,
                    mostImportantContractors,
                    minReturnedContractors: maxNodesinLevel,
                    isRoot: isRoot,
                    showReverted: showReverted,
                    ids: taxPayerId == null ? null : new[] { taxPayerId });
            if (graphLayersDepthLimit.HasValue)
                treeParametersNew.GraphLayersDepthLimit = graphLayersDepthLimit.Value;

            if (maxNodesinLevel.HasValue)
                treeParametersNew.MaxNodesInLevel = maxNodesinLevel.Value;
             
            return treeParametersNew;
        }

        private void InitFromConstructor()
        {
            GraphLayersDepthLimit = int.MinValue;
            MaxNodesInLevel = 5;
        }

        private static int CalculatYear(string period)
        {
            int year;
            try
            {
                year = Convert.ToInt32(period.Split(' ').Last());
            }
            catch (Exception)
            {
                year = DateTime.Now.Year;
            }

            return year;
        }

        private static int CalculateQuarter(string period)
        {
            int quarter;
            try
            {
                quarter = Convert.ToInt32(period.Split(' ').First());
            }
            catch (Exception)
            {
                quarter = (DateTime.Now.Month - 1) / 3 + 1;
            }
            return quarter;
        }

        /// <summary> Returnes a deep clone created. </summary>
        /// <returns></returns>
        GraphTreeParameters Clone()
        {
            return SerializationDeepCloner.Clone(this);
        }

        #region ICloneable implementation

        /// <summary> Returnes a deep clone created. </summary>
        /// <returns></returns>
        object ICloneable.Clone()
        {
            return Clone();
        }

        #endregion ICloneable implementation
    }
}
