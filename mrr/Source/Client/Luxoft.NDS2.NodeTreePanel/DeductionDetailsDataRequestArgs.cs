﻿using System;
using System.Diagnostics.Contracts;
using Luxoft.NDS2.Client.NodeTreePanel.Interfaces;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;

namespace Luxoft.NDS2.Client.NodeTreePanel
{
    /// <summary> Arguments for event <see cref="NodeTree.RequestDeductionDetailsData"/>. </summary>
    public class DeductionDetailsDataRequestArgs : EventArgs
    {
        /// <summary> </summary>
        /// <param name="commandManager"></param>
        /// <param name="deductionKeyParameters"></param>
        /// <param name="treeParameters"></param>
        /// <param name="parentNodeId"></param>
        public DeductionDetailsDataRequestArgs( ICommandTransactionManager commandManager, DeductionDetailsKeyParameters deductionKeyParameters, 
                                                GraphTreeParameters treeParameters, Guid parentNodeId )
        {
            Contract.Requires( commandManager != null );
            Contract.Requires( deductionKeyParameters != null );
            Contract.Requires( treeParameters != null );
            Contract.Requires( parentNodeId != default(Guid) );

            CommandManager          = commandManager;
            DeductionKeyParameters  = deductionKeyParameters;
            Parameters              = treeParameters;
            ParentNodeId            = parentNodeId;
        }

        public ICommandTransactionManager CommandManager { get; private set; }

        public DeductionDetailsKeyParameters DeductionKeyParameters { get; private set; }

        public GraphTreeParameters Parameters { get; private set; }

        public Guid ParentNodeId { get; private set; }
    }
}