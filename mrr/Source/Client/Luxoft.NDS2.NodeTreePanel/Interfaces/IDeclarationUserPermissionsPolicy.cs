﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting.TaxPayer;

namespace Luxoft.NDS2.Client.NodeTreePanel.Interfaces
{
    /// <summary> User perimissions policy for a declaration. </summary>
    public interface IDeclarationUserPermissionsPolicy
    {
        /// <summary> Is building of Dependency Tree allowable for the current user? </summary>
        /// <param name="declarationBrief"></param>
        /// <returns></returns>
        bool IsTreeRelationEligible( DeclarationBrief declarationBrief );

        /// <summary> Is building of Dependency Tree allowable for the current user? </summary>
        /// <param name="declarationSummary"></param>
        /// <returns></returns>
        bool IsTreeRelationEligible( DeclarationSummary declarationSummary );

        /// <summary> Is building of Dependency Tree allowable for the current user? </summary>
        /// <param name="taxPayerId"></param>
        /// <param name="period"> Optional. Checks for all periods if it is 'null' (by default). </param>
        /// <returns></returns>
        bool IsTreeRelationEligible( TaxPayerId taxPayerId, Period period = null );

        /// <summary> Is building of Dependency Tree allowable for the current user? </summary>
        /// <param name="sounCode"></param>
        /// <returns></returns>
        bool IsTreeRelationEligible( string sounCode );

        /// <summary> Is All tree node report of Dependency Tree allowable for the current user? </summary>
        /// <returns></returns>
        bool IsTreeRelationAllTreeReportEligible();

        int? GetTreeRelationMaxLayersLimit( bool isByPurchase );
    }
}