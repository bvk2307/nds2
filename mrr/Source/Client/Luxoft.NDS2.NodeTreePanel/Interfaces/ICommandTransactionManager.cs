﻿using System;
using System.Threading;

namespace Luxoft.NDS2.Client.NodeTreePanel.Interfaces
{
    public interface ICommandTransactionManager : IDisposable
    {
        int InstanceKey { get; }

        CancellationToken CancelToken { get; }

        void ExecuteInTransaction( string transactionName, Action action );

        void ExecuteInTransaction<T1>( string transactionName, Action<T1> action, T1 param1 );

        void ExecuteInTransaction<T1, T2, T3>( string transactionName, 
            Action<T1, T2, T3> action, T1 param1, T2 param2, T3 param3 );

        void StartTransaction( string transactionName );

        void CommitTransaction( string transactionName );

        void RollbackTransaction();
    }
}