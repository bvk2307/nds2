﻿using System;

namespace Luxoft.NDS2.Client.NodeTreePanel.Interfaces.Actions
{
    /// <summary> An action to load node children count. </summary>
    public interface IInfoNodeChildrenCountAction
    {
        /// <summary> Starts tree node data loading. </summary>
        /// <param name="treeBranchParameters"></param>
        /// <param name="parentNodeId"> </param>
        void StartDataRequest( GraphTreeParameters treeBranchParameters, Guid parentNodeId );
    }
}