﻿using System;

namespace Luxoft.NDS2.Client.NodeTreePanel.Interfaces.Actions
{
    /// <summary> An action to load data for building a new graph tree branch. </summary>
    public interface ILoadGraphTreeBranchAction
    {
        /// <summary> Starts tree node data loading. </summary>
        /// <param name="treeBranchParameters"></param>
        /// <param name="parentNodeId"> </param>
        void StartDataRequest( GraphTreeParameters treeBranchParameters, Guid parentNodeId );
    }
}