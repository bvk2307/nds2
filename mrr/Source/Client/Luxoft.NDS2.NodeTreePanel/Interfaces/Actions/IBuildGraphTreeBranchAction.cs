﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Luxoft.NDS2.Client.NodeTreePanel.Interfaces.Actions
{
    /// <summary> An action to build a new graph tree branch. </summary>
    public interface IBuildGraphTreeBranchAction
    {
        void BuildTreeBranch( IEnumerable<GraphTreeNodeData> graphNodes, Guid parentNodeKey, int instanceKey, CancellationToken cancelToken );
    }
}