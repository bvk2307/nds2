﻿using System;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;

namespace Luxoft.NDS2.Client.NodeTreePanel.Interfaces.Actions
{
    /// <summary> An action to info about Deduction detail sums for graph tree. </summary>
    public interface IInfoDeductionDetailSumsAction
    {
        /// <summary> Starts the data request of graph tree data of deduction detail sums. </summary>
        /// <param name="deductionKeyParameters"></param>
        /// <param name="parentNodeId"> </param>
        void StartDataRequest( DeductionDetailsKeyParameters deductionKeyParameters, Guid parentNodeId );
    }
}