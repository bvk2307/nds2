﻿using System;
using System.Windows.Threading;
using Northwoods.GoXam;

namespace Luxoft.NDS2.Client.NodeTreePanel
{
    public sealed class GraphTreePartManager : PartManager
    {
        private Action<Node> _onNodeAddedHandler = null;

        public void Init( Action<Node> onNodeAddedHandler )
        {
            _onNodeAddedHandler = onNodeAddedHandler;
        }

        protected override void OnNodeAdded( Node node )
        {
            base.OnNodeAdded( node );

            Action<Node> handler = _onNodeAddedHandler;
            if ( handler != null )
                handler( node );
        }
    }
}