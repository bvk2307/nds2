﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Luxoft.NDS2.Client.NodeTreePanel.Converters
{
    public class PopupScaleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double passedValue = (double)value;
            return 2 / passedValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
