﻿namespace Luxoft.NDS2.Client.NodeTreePanel.Converters
{
    using System;
    using System.Collections;
    using System.Globalization;
    using System.Windows;
     
    public sealed class EmptyCollectionVisibilityConverter : EmptyListConverter<Visibility>
    {
        public EmptyCollectionVisibilityConverter() :
            base(Visibility.Collapsed, Visibility.Visible) { }
    }

    public sealed class EmptyListInvisibilityConverter : EmptyListConverter<Visibility>
    {
        public EmptyListInvisibilityConverter() :
            base(Visibility.Visible, Visibility.Collapsed) { }
    }

    public class EmptyListConverter<T> : BooleanConverter<T>
    {
        public EmptyListConverter(T trueValue, T falseValue)
            : base(trueValue, falseValue)
        {
        }

        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return this.False;
            else
            {
                ICollection list = value as ICollection;
                if (list != null)
                {
                    if (list.Count == 0)
                        return this.False;

                    return this.True;
                }
                return this.True;
            }
        }

        public override object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
