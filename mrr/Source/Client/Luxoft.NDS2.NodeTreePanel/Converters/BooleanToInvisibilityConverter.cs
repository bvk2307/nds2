﻿namespace Luxoft.NDS2.Client.NodeTreePanel.Converters
{
    using System.Windows;

    public sealed class BooleanToInvisibilityConverter : BooleanConverter<Visibility>
    {
        public BooleanToInvisibilityConverter() :
            base(Visibility.Collapsed, Visibility.Visible) { }
    }
}