﻿namespace Luxoft.NDS2.Client.NodeTreePanel.Converters
{
    using System;
    using System.Globalization;
    using System.Windows.Data;

    public class NodeLevelItemHeightConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            double scale = (double)values[0];
            double size = (double)values[1];

            size = size * scale;

            return size;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}