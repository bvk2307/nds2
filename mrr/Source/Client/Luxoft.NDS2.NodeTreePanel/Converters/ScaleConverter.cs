﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Luxoft.NDS2.Client.NodeTreePanel.Converters
{
    public class ScaleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double passedValue = (double)value;

            return passedValue * 2;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double passedValue = (double)value;
            return passedValue / 2;
        }
    }
}
