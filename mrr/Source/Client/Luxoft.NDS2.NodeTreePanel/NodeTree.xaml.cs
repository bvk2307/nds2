﻿//#define DONOTUSEDEDUCTIONSPEC
//#define DONOTDISABLE

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Threading;
using FLS.Common.Lib.Primitives;
using FLS.CommonComponents.CorLibAddins.Helpers;
using FLS.CommonComponents.Lib.Execution;
using FLS.CommonComponents.Lib.Interfaces;
using Luxoft.NDS2.Client.NodeTreePanel.Commands;
using Luxoft.NDS2.Client.NodeTreePanel.Constants;
using Luxoft.NDS2.Client.NodeTreePanel.Converters;
using Luxoft.NDS2.Client.NodeTreePanel.Enums;
using Luxoft.NDS2.Client.NodeTreePanel.Extensions;
using Luxoft.NDS2.Client.NodeTreePanel.Interfaces;
using Luxoft.NDS2.Client.NodeTreePanel.Interfaces.Actions;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting.TaxPayer;
using Luxoft.NDS2.Common.Helpers.Enums;
using Northwoods.GoXam;
using Northwoods.GoXam.Model;

namespace Luxoft.NDS2.Client.NodeTreePanel
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class NodeTree : UserControl
    {
        #region Private members 

        private IThreadInvokerWErrorHandling _threadInvoker = null;
        private IInstanceProvider<ICommandTransactionManager> _commandManagerProvider = null;
        private Int32Counter _commandTransactionCounter = null;

        private GraphTreeParameters _graphTreeParameters = null;

        private WeakReference<ILoadGraphTreeBranchAction> _wrLoadDataBranchAction = null;
        private WeakReference<IInfoNodeChildrenCountAction> wrInfoNodeChildrenCountAction = null;

        private ObservableCollection<NodeTreeLevel> _levels;

        private int _graphBuildCounter = -1;
        private bool _viewportBoundsChangedSubscribed = false;
         
        #endregion Private members 

        public event EventHandler<GraphTreeEventArgs> ShowTreeInNewWindowSelected;
        public event EventHandler<GraphTreeEventArgs> ShowTableDataSelected;
        public event EventHandler<GraphTreeDataRequestArgs> RequestTreeData;
        public event EventHandler<DeductionDetailsDataRequestArgs> RequestDeductionDetailsData;
        public event EventHandler<GraphTreeEventArgs> OpenDeclarationCard;
        public event EventHandler<GraphTreeMessageEventArgs> ShowTrayMessage;

        public NodeTree()
        { 
            InitializeComponent();

            Loaded += OnLoaded;
              
            // Data-bind Slider.Value to Diagram.Panel.Scale, once the Diagram.Panel
            // has been created by expanding the Diagram's ControlTemplate.
            // The Slider uses a logarithmic scale via the LogConverter defined below. 
            myDiagram.TemplateApplied += OnTemplateApplied;
   
            myDiagram.CommandHandler = new DiagramCommandHandler(SliderConstants.MinimumScale / 2, SliderConstants.MaximumScale / 2);
        }

        public NodeTree Init( IThreadInvokerWErrorHandling threadInvoker, IInstanceProvider<ICommandTransactionManager> commandManagerProvider, 
                              Int32Counter commandTransactionCounter )
        {
            _commandManagerProvider     = commandManagerProvider;
            _commandTransactionCounter  = commandTransactionCounter;
            _threadInvoker              = threadInvoker;

            return this;
        }

        private void OnLoaded( object sender, RoutedEventArgs routedEventArgs )
        {
            GraphPanel.PartVisibleChangedPublic += OnPartVisibleChanged; 
        }

        public void InitGraph( IReadOnlyDictionary<int, Tuple<SolidColorBrush, string>> surs )
        {
            lvSur.ItemsSource = new ObservableCollection<Tuple<SolidColorBrush, string>>( surs.Select( s => s.Value ) );
            levelsBlock.ItemsSource = Levels;

            var model = new GraphLinksModel<GraphTreeNodeData, Guid, Guid?, GraphTreeLinkData>();
            model.NodeKeyReferenceAutoInserts = true;
            model.NodeKeyPath = "Key"; 
            model.NodeCategoryPath = "NodeTypeKey";
            model.Modifiable = true;

            GraphPartManager.Init( OnNodeAdded );
            myDiagram.Model = model;
            myDiagram.LayoutManager.Animated = false;
        }

        public void AcceptAction( ILoadGraphTreeBranchAction loadDataBranchAction )
        {
            _wrLoadDataBranchAction = new WeakReference<ILoadGraphTreeBranchAction>(loadDataBranchAction);
        }

        public void AcceptAction( IInfoNodeChildrenCountAction infoNodeChildrenCountAction )
        {
            wrInfoNodeChildrenCountAction = new WeakReference<IInfoNodeChildrenCountAction>(infoNodeChildrenCountAction);
        }

        #region Properties

        public ICommandTransactionManager CommandManager
        {
            get { return _commandManagerProvider.Instance; }
        }

        public GraphTreeParameters TreeParameters
        {
            get { return _graphTreeParameters; }
            set { _graphTreeParameters = value; }
        }

        public PrintManager GraphPrintManager
        {
            get { return myDiagram.PrintManager; }
        }
         
        private int TaxYear
        {
            get { return _graphTreeParameters.KeyParameters.TaxYear; }
        }

        private int TaxQuarter
        {
            get { return _graphTreeParameters.KeyParameters.TaxQuarter; }
        }

        private bool IsByPurchase
        {
            get { return _graphTreeParameters.KeyParameters.IsPurchase; }
        }

        private bool ShowReverted
        {
            get { return _graphTreeParameters.KeyParameters.ShowReverted; }
        }

        private GraphLinksModel<GraphTreeNodeData, Guid, Guid?, GraphTreeLinkData> GraphModel
        {
            get { return (GraphLinksModel<GraphTreeNodeData, Guid, Guid?, GraphTreeLinkData>)myDiagram.Model; }
        }

        private GraphTreePartManager GraphPartManager
        {
            get { return (GraphTreePartManager)myDiagram.PartManager; }
        }
         
        private GraphTreeDiagramPanel GraphPanel
        {
            get { return (GraphTreeDiagramPanel)myDiagram.Panel; }
        }

        #endregion Properties

        #region LoadData

        /// <summary> Creates a new command manager and delays a request to load root data later. </summary>
        /// <param name="graphTreeParameters"></param>
        /// <param name="cancelSource"></param>
        /// <returns></returns>
        public ICommandTransactionManager InitAndDelayLoadTree( GraphTreeParameters graphTreeParameters, CancellationTokenSource cancelSource )
        {
            Contract.Requires( graphTreeParameters != null );

            _graphTreeParameters = graphTreeParameters;

            if (IsByPurchase)
            {
                gridPattern.CellSize = new Size(GridSizeConstants.ByPurchaseSize, GridSizeConstants.ByPurchaseSize);
            }
            else
            {
                gridPattern.CellSize = new Size(GridSizeConstants.Size, GridSizeConstants.Size);
            }

            var commandManager = new GraphCommandTransactionManager( 
                ++_graphBuildCounter, _commandTransactionCounter, this, GraphModel, cancelSource );

            _threadInvoker.BeginExecute( OnLoadTree,
                DispatcherPriority.ContextIdle, commandManager, graphTreeParameters, GraphTreeNodeData.NoNodeKey );   //delays while UI thread will be idle

            return commandManager;
        }

        private void OnLoadTree( 
            GraphCommandTransactionManager commandManager, GraphTreeParameters treeBranchParameters, Guid parentMpdeKey, 
            CancellationToken cancellationToken = default(CancellationToken) )
        {
            if ( _wrLoadDataBranchAction == null )
            {
                RaiseRequestTreeData( commandManager, treeBranchParameters, parentMpdeKey );
            }
            else
            {
                ILoadGraphTreeBranchAction action; 
                _wrLoadDataBranchAction.TryGetTarget( out action );
                action.StartDataRequest( treeBranchParameters, parentMpdeKey );
            }
        }

        /// <summary> Loads tree branch nodes into the tree. </summary>
        /// <param name="parentNodeKey"> A parent node key <see cref="GraphTreeNodeData.Key"/> or <see cref="GraphTreeNodeData.NoNodeKey"/> if the node is root one. </param>
        /// <param name="nodes"></param>
        /// <returns> A parent node of the built branch or 'null' if failed. </returns>
        public GraphTreeNodeData BuildTreeBranch( Guid parentNodeKey, IEnumerable<GraphTreeNodeData> nodes )
        {
            //apopov 29.12.2015	//TODO!!!   check exception handling in this method

            Contract.Requires( nodes != null );

            IEnumerable<GraphTreeNodeData> filteredNodes = nodes;

            if (ShowReverted == false)
            {
                filteredNodes = filteredNodes.Where(x => x.GraphDataNode.GraphData.IsReverted == false);
            }

            bool success = true;
            ObservableCollection<GraphTreeNodeData> graphNodes = null;
            GraphTreeNodeData parentNodeData = null;
            bool newGraphBuild = parentNodeKey == GraphTreeNodeData.NoNodeKey;
            if ( newGraphBuild )  //builds a new graph tree
            {
                // ReSharper disable once PossibleMultipleEnumeration
                graphNodes = new ObservableCollection<GraphTreeNodeData>( filteredNodes );
                parentNodeData = graphNodes.FirstOrDefault( node => !node.IsNotQueryRootLevel );
                success = parentNodeData != null;   //ignores this call if there is not the root node in the graph tree
            }
            if ( success )
            {
                IEnumerable<GraphTreeNodeData> childNodes = null;
                if ( newGraphBuild ) //builds a new graph tree
                {
                    parentNodeData.TreeLevel = GraphTreeNodeData.TreeRootLevel;   //the root tree level

                    childNodes = graphNodes.Where( node => node != parentNodeData );

                    // ReSharper disable once PossibleMultipleEnumeration
                    foreach ( GraphTreeNodeData nodeData in childNodes )
                    {
                        nodeData.ParentKey = parentNodeData.Key;
                        nodeData.GraphDataNode.Parent = parentNodeData.GraphDataNode;
                        nodeData.TreeLevel = NodeTreeLevelForChild( parentNodeData );
                    }
                    InitNewTreeGraph( graphNodes );
                }
                else
                {
                    parentNodeData = GraphModel.FindNodeByKey( parentNodeKey );

                    Contract.Assume( parentNodeData != null );

                    // ReSharper disable once PossibleMultipleEnumeration
                    childNodes = filteredNodes.ToArray();
                    // ReSharper disable once PossibleMultipleEnumeration
                    foreach ( GraphTreeNodeData nodeData in childNodes )
                    {
                        nodeData.GraphDataNode.Parent = parentNodeData.GraphDataNode;
                        nodeData.TreeLevel = NodeTreeLevelForChild(parentNodeData);
                    }
                }
                Node parentNode = GraphPartManager.FindNodeForData( parentNodeData, GraphModel );
                    
                Contract.Assume( parentNode != null );

                // ReSharper disable once PossibleMultipleEnumeration
                AddChildNodes( parentNode, childNodes );
            }
            return parentNodeData;
        }

        /// <summary> Loads deduction detail sums into the tree details. </summary>
        /// <param name="nodeId"></param>
        /// <param name="deductionDetails"></param>
        /// <returns></returns>
        public bool InfoDeductionDetailSums( Guid nodeId, IEnumerable<DeductionDetail> deductionDetails )
        {
            GraphTreeNodeData linkToNodeData = GraphModel.FindNodeByKey( nodeId );
            GraphTreeLinkData linkToData = GraphModel.GetFromLinksForNode( linkToNodeData ).First();  //examinates 'into' links for node 'linkToNodeData'. There is one and only one such link by limitations of the visual tree
            linkToData.AddDeductionDetails( deductionDetails );

            return true;
        }

        /// <summary> Sets the child counter for a node. </summary>
        /// <param name="parentNodeKey"> A parent node key <see cref="GraphTreeNodeData.Key"/>. </param>
        /// <param name="childCount"></param>
        /// <param name="revertedChildCount"></param>
        /// <returns></returns>
        public bool SetChildCounter( Guid parentNodeKey, int childCount, int revertedChildCount )
        {
            Contract.Requires( parentNodeKey != GraphTreeNodeData.NoNodeKey );
            Contract.Requires( childCount >= 0 );

            GraphTreeNodeData parentNodeData = null;
            parentNodeData = GraphModel.FindNodeByKey( parentNodeKey );

            Contract.Assume( parentNodeData != null );

            //if node's child count is not loaded yet
            if ( !parentNodeData.NodeState.HasFlag( GraphTreeNodeState.ChildCountLoaded )   
                    && !parentNodeData.NodeState.HasFlag( GraphTreeNodeState.ChildrenLoaded ) )
            {
                ReplaceNodeState( parentNodeData, GraphTreeNodeState.LoadingChildCount, GraphTreeNodeState.ChildCountLoaded );

                parentNodeData.ContentNodeCount = childCount;
                parentNodeData.ContentRevertedNodeCount = revertedChildCount;
            }

            return true;
        }

        public void ClearTreeNodes()
        {
            ClearTreeGraph();
        }

        /// <summary> Raises <see cref="RequestTreeData"/> that graph tree needs datas of a branch. </summary>
        /// <param name="commandManager"></param>
        /// <param name="graphTreeParameters"></param>
        /// <param name="parentNodeKey"> A parent node key <see cref="GraphTreeNodeData.Key"/> or <see cref="GraphTreeNodeData.NoNodeKey"/> if the node is root one. </param>
        /// <param name="priority"></param>
        private void RaiseRequestTreeData( ICommandTransactionManager commandManager, GraphTreeParameters graphTreeParameters, Guid parentNodeKey, CallExecPriority priority = default(CallExecPriority) )
        {
	    //apopov 11.1.2016	//TODO!!!   check exception handling here because OnNodeAdded() is called by Dispatcher.BeginInvoke()

            EventHandler<GraphTreeDataRequestArgs> handlers = RequestTreeData;

            if ( handlers != null )
                handlers( this, new GraphTreeDataRequestArgs( commandManager, graphTreeParameters, parentNodeKey, priority ) );  //it is a signal to request a new tree data if it is GraphTreeNodeData.NoNodeKey
        }

        //apopov 21.6.2016	//DEBUG!!!  //remove Deduction Details methods when all stuff will be passed into InfoDeductionDetailSumsAction
        /// <summary> Raises <see cref="RequestDeductionDetailsData"/> that graph tree needs datas of deduction detail sums. </summary>
        /// <param name="commandManager"></param>
        /// <param name="graphTreeParameters"></param>
        /// <param name="deductionKeyParameters"></param>
        /// <param name="parentNodeId"> </param>
        private void RaiseRequestDeductionDetailsData( ICommandTransactionManager commandManager, GraphTreeParameters graphTreeParameters,
                                                       DeductionDetailsKeyParameters deductionKeyParameters, Guid parentNodeId )
        {
            Contract.Requires( parentNodeId != GraphTreeNodeData.NoNodeKey );

            EventHandler<DeductionDetailsDataRequestArgs> handlers = RequestDeductionDetailsData;

            if ( handlers != null )
                handlers( this, new DeductionDetailsDataRequestArgs( commandManager, deductionKeyParameters, graphTreeParameters, parentNodeId ) );
        }

        private void ClearTreeGraph()
        {
            ( (ObservableCollection<GraphTreeLinkData>)myDiagram.LinksSource ).Clear();
            ( (ObservableCollection<GraphTreeNodeData>)myDiagram.NodesSource ).Clear();
        }

        private void InitNewTreeGraph( ObservableCollection<GraphTreeNodeData> nodes )
        {
            myDiagram.NodesSource = nodes;
            myDiagram.LinksSource = new ObservableCollection<GraphTreeLinkData>();
        }

        private static int NodeTreeLevelForChild( GraphTreeNodeData parentNodeData )
        {
            return parentNodeData.TreeLevel + 1;
        }

        #endregion

        #region CommonOperations

        public void DisableGraphCommandsIfNeeded()
        {
            if ( myDiagram.IsEnabled )
                myDiagram.IsEnabled = false;
            this.nodeTreeProgressBar.Visibility = Visibility.Visible;
        }

        public void EnableGraphCommandsIfNeeded()
        {
            this.nodeTreeProgressBar.Visibility = Visibility.Hidden;

            if ( !myDiagram.IsEnabled )
                myDiagram.IsEnabled = true;
        }

        private void ExpandNode( Node node )
        {
	        Contract.Requires( node.Visible );

            GraphTreeNodeData nodeData = (GraphTreeNodeData)node.Data;
            node.IsExpandedTree = true;
            nodeData.IsNodeExpanded = node.IsExpandedTree;

            if ( !nodeData.IsByPurchase )
            {
                if (IsNodeHasRevertedLinks(node))
                {
                    nodeData.IsCanHideRevertedLinks = true;                      
                }
            }
        }

        private void CollapsedNode( Node node )
        {
            GraphTreeNodeData nodeData = (GraphTreeNodeData)node.Data;
            node.IsExpandedTree = false;
            nodeData.IsNodeExpanded = node.IsExpandedTree;
        }

        public ICollection<GraphTreeNodeData> GetVisibleNonGroupDirectNodeDatas( int levelDownDepthLimit = int.MaxValue )
        {
            Node rootNode = GraphPartManager.Nodes
                .FirstOrDefault( x => x.NodeData().Inn == _graphTreeParameters.KeyParameters.Inn );

            IEnumerable<GraphTreeNodeData> subNodes = GetBranchVisibleNodeDatas( 
                rootNode, levelDownDepthLimit, 
                ( node, parentNnode ) => node.NodeData().NodeType == NodeTypes.Node && !node.NodeData().IsReverted )
                .Reverse();

            var visibleNodeDatas = new Stack<GraphTreeNodeData>( subNodes );
            visibleNodeDatas.Push( rootNode.NodeData() );

            return visibleNodeDatas.ToArray();
        }

        /// <summary> Interates visible nodes of a branch starting from children of <paramref name="parentNode"/>. </summary>
        /// <param name="parentNode"></param>
        /// <param name="levelDownDepthLimit"> A level down depth limit for passage. Optional. Iterates only children of <paramref name="parentNode"/> by default with value '0'. 
        /// Iterates all branch layers if it has a negative value.
        /// </param>
        /// <param name="predicate"> A predicate for additional filtering. </param>
        /// <returns> A created collection of branch visible nodes. </returns>
        private ICollection<GraphTreeNodeData> GetBranchVisibleNodeDatas( 
            Node parentNode, int levelDownDepthLimit = 0, Func<Node, Node, bool> predicate = null )
        {
            ICollection<GraphTreeNodeData> visibleNodeDatas = GetBranchVisibleNodes( 
                parentNode, levelDownDepthLimit, predicate: predicate )
                .Select( node => node.NodeData() ).ToArray();

            return visibleNodeDatas;
        }

        /// <summary> Interates visible nodes of a branch starting from children of <paramref name="parentNode"/>. </summary>
        /// <param name="parentNode"></param>
        /// <param name="levelDownDepthLimit"> A level down depth limit for passage. Optional. Iterates only children of <paramref name="parentNode"/> by default with value '0'. 
        /// Iterates all branch layers if it has a negative value.
        /// </param>
        /// <param name="predicate"> A predicate for additional filtering. </param>
        /// <returns></returns>
        private IEnumerable<Node> GetBranchVisibleNodes( 
            Node parentNode, int levelDownDepthLimit = 0, Func<Node, Node, bool> predicate = null )  //int levelUpDepthLimit = 0 )
        {
            GraphTreeNodeData parentNodeData = parentNode.NodeData();
            IEnumerable<Node> filtredNodes = parentNode.NodesOutOf.Where( nodePar => nodePar.Visible );
            //if ( levelUpDepthLimit >= 0 )
            //    filtredNodes = filtredNodes.Where( nodePar => nodePar.NodeData().Level + levelUpDepthLimit > parentNodeData.Level );
            if ( predicate != null )
                filtredNodes = filtredNodes.Where( nodePar => predicate( nodePar, parentNode ) );
            int childLevelDownDepthLimit = levelDownDepthLimit <= 0 ? levelDownDepthLimit : levelDownDepthLimit - 1;

            foreach ( Node visibleChildNode in filtredNodes )
            {
                yield return visibleChildNode;

                GraphTreeNodeData visibleChidlNodeData = visibleChildNode.NodeData();

                if ( childLevelDownDepthLimit != 0  //the down depth limit isn't reached yet or isn't defined
                     && visibleChidlNodeData.ParentKey == parentNodeData.Key  //ignnores any non child node (for example, a some zombi node referencing for the parent one)
                     && visibleChidlNodeData.NodeType == NodeTypes.Node )   //ignores a group node
                {
                    foreach ( Node visibleChildChildNode in GetBranchVisibleNodes( visibleChildNode, childLevelDownDepthLimit, predicate ) )    //, levelUpDepthLimit ) )
                    {
                        yield return visibleChildChildNode;
                    }
                }
            }
        }

        private IEnumerable<GraphTreeNodeData> GetAllNonGroupNodeDatas()
        {
            foreach ( Node node in GraphPartManager.Nodes )
            {
                GraphTreeNodeData nodeData = node.NodeData();
                if ( nodeData.NodeType == NodeTypes.Node )   //ignores a group node
                    yield return nodeData;
            }
        }

        #endregion CommonOperations

        #region EventHandlers

        private void NodeMouseLeftButtonDown( object sender, MouseButtonEventArgs e )
        {
            if ( !DiagramPanel.IsDoubleClick( e ) )
                return;

            var node = Part.FindAncestor<Node>(sender as UIElement);
            if ( node == null )
                return;

            var nodeData = node.Data as GraphTreeNodeData;
            if ( nodeData == null )
                return;

            e.Handled = true;

            if ( nodeData.IsZombie )
                 OnZombiNodeDoubleClick( nodeData, node );
            else OnNodeDoubleClick( nodeData, node );
        }

        private void OnZombiNodeDoubleClick( GraphTreeNodeData nodeData, Node node )
        {
            if ( IsZombiOutLinkPresent( node ) )
            {
                RemoveNodeZombiLinks( nodeData.Key );
            }
            else
            {
                foreach ( GraphTreeNodeData backNode in FindBackRelationNodes( nodeData ) )
                {
                    AddZombiLink( nodeData, backNode );
                }
            }

            myDiagram.LayoutManager.LayoutDiagram(); 
        }

        private void OnNodeDoubleClick( GraphTreeNodeData nodeData, Node node )
        {
            if ( !node.IsExpandedTree )
            {
                if (!nodeData.NodeState.HasFlag(GraphTreeNodeState.ChildCountLoaded) 
                    || (nodeData.ContentNodeCount.HasValue && nodeData.ContentNodeCount > 0)
                    || (nodeData.ContentRevertedNodeCount.HasValue && nodeData.ContentRevertedNodeCount > 0))
                {
                    if (nodeData.IsChildLevelEligible) //child nodes have level 'nodeData.Level + 1' that must meet the layer limitation: '<= layersLimit + 1'
                    {
                        if (!nodeData.NodeState.HasFlag(GraphTreeNodeState.ChildrenLoaded)
                            && !nodeData.NodeState.HasFlag(GraphTreeNodeState.LoadingChildren)
                            && !(nodeData.NodeState.HasFlag(GraphTreeNodeState.ChildCountLoaded)
                            && (nodeData.ContentNodeCount <= 0 && nodeData.ContentRevertedNodeCount <= 0)))
                        {
                            // only create children once per node!
                            GetAndAddChildNodesAsync(node);
                        }
                        else
                        {
                            ExpandNode(node);
                            HideGrouppedNodeForBranch(node);
                            myDiagram.LayoutManager.LayoutDiagram();
                            HideUnexpectedWanderingZombiLinksIfNeeded(node);
                        }
                    }
                    else
                    {
                        EventHandler<GraphTreeMessageEventArgs> handlers = ShowTrayMessage;
                        if (handlers != null)
                            handlers(this, new GraphTreeMessageEventArgs(Properties.Resources.LayerLevelLimitIsReached));
                    }
                }
            }
            else
            {
                CollapsedNode( node );

                myDiagram.LayoutManager.LayoutDiagram();
            }
        }

        private void OnPartVisibleChanged( object sender, EventArgs eventArgs )
        {
            Node node = sender as Node;
            if ( node != null && node.Visible )
                OnNodeVisible( node );
        }

        private void OnZombieVisibleChanged( object sender, DependencyPropertyChangedEventArgs args )
        {
            LinkPanel linkPanel = sender as LinkPanel;
            if ( linkPanel != null )
            {
                Link link = linkPanel.TemplatedParent as Link;
                if ( link != null && link.Category == "zombilink" )
                {
                    Node linkToNode = link.ToNode;
                    if ( linkToNode != null )
                    {
                        GraphTreeNodeData toNode = linkToNode.NodeData();
                        GraphTreeGroupNodeData toNodeGroup = toNode as GraphTreeGroupNodeData;
                        if ( toNodeGroup != null )
                        {
                            Node linkFromNode = link.FromNode;
                            if ( linkFromNode != null )
                            {
                                TaxPayerId fromNodeId = linkFromNode.NodeData().TaxPayerId;
                                toNode = toNodeGroup.GroupNodes.FirstOrDefault( x => x.TaxPayerId == fromNodeId );
                            }
                        }
                        if ( toNode != null )
                            toNode.HasZombieLinkTo += (bool)args.NewValue ? 1 : -1;
                    }
                }
            }
        }

        /// <summary> Called on a node added with non determined invoking. </summary>
        /// <param name="nodeAdded"></param>
        private void OnNodeAdded( Node nodeAdded )
        {
            _threadInvoker.BeginExecute( StartLoadChildCountIfNeeded, DispatcherPriority.ContextIdle, nodeAdded, CancellationToken.None );   //delays while UI thread will be idle
        }

        private void OnNodeVisible( Node node )
        {
            StartLoadChildCountIfNeeded( node );
        }

        private bool IsLevelEligible( GraphTreeNodeData nodeData )
        {
            return _graphTreeParameters.GraphLayersDepthLimit <= 0 || nodeData.TreeLevel < _graphTreeParameters.GraphLayersDepthLimit;
        }

        private void StartLoadChildCountIfNeeded( Node node, CancellationToken cancellationToken = default(CancellationToken) )
        {
            GraphTreeNodeData nodeData = node.NodeData();
            if ( nodeData != null //the node model could be not found if node's element is not rendered yet
                 && nodeData.NodeType == NodeTypes.Node && !nodeData.IsInGroup && !nodeData.IsZombie && !nodeData.IsSpecial
                 && nodeData.NodeState == GraphTreeNodeState.Unknown ) //there are not loaded children or their count
                GetAndAddChildNodesAsync( node, loadChildCountOnly: true );
        }

        private void GetAndAddChildNodesAsync( Node parentNode, bool loadChildCountOnly = false )
        {
            GraphTreeNodeData parentNodeData = parentNode.NodeData();

            if (SpecialInnsHelper.IsSpecialInn(parentNodeData.Inn))
            {
                return;
            }

            parentNodeData.NodeState |= loadChildCountOnly ? GraphTreeNodeState.LoadingChildCount : GraphTreeNodeState.LoadingChildren;

	//apopov 21.4.2016	//DEBUG!!!
            GraphTreeParameters graphTreeParameters = GraphTreeParameters.New(_graphTreeParameters, new TaxPayerId(parentNodeData.Inn, kppCode: null), isRoot: false);

            if ( loadChildCountOnly )
            {
                if ( wrInfoNodeChildrenCountAction == null ) //apopov 21.6.2016	//DEBUG!!!  //GraphTreeViewer does not use Data Manager and Actions yet
                {
                    //apopov 21.6.2016	//DEBUG!!!  //remove Deduction Details methods when all stuff will be passed into InfoDeductionDetailSumsAction
                    RaiseRequestTreeData( CommandManager, graphTreeParameters, parentNodeData.Key, CallExecPriority.Background );
                }
                else
                {
                    IInfoNodeChildrenCountAction action;
                    wrInfoNodeChildrenCountAction.TryGetTarget( out action );
                    action.StartDataRequest( graphTreeParameters, parentNodeData.Key );
                }
            }
            else
            {
                if ( _wrLoadDataBranchAction == null ) //apopov 21.6.2016	//DEBUG!!!  //GraphTreeViewer does not use Data Manager and Actions yet
                {
                    //apopov 21.6.2016	//DEBUG!!!  //remove Deduction Details methods when all stuff will be passed into InfoDeductionDetailSumsAction
                    RaiseRequestTreeData( CommandManager, graphTreeParameters, parentNodeData.Key, CallExecPriority.Normal );
                }
                else
                {
                    ILoadGraphTreeBranchAction action; 
                    _wrLoadDataBranchAction.TryGetTarget( out action );
                    action.StartDataRequest( graphTreeParameters, parentNodeData.Key );
                }
            }
        }

        //apopov 25.12.2015	//TODO!!! remove if it will useless
        private static bool IsDisposed( Visual visual )
        {
            PresentationSource source = PresentationSource.FromVisual( visual );
            bool isDisposed = source == null || source.IsDisposed;

            return isDisposed;
        }

        private void AddChildNodes( Node parentNode, IEnumerable<GraphTreeNodeData> nodes )
        {
            GraphTreeNodeData parentNodeData = parentNode.NodeData();
            if ( !parentNodeData.NodeState.HasFlag( GraphTreeNodeState.ChildrenLoaded ) ) //ignores if node's children are loaded yet
            {
                ReplaceNodeState( parentNodeData, GraphTreeNodeState.LoadingChildren | GraphTreeNodeState.LoadingChildCount, 
                                                  GraphTreeNodeState.ChildrenLoaded | GraphTreeNodeState.ChildCountLoaded );

                var nodeCount = AddChildrenAndExcessesToGroup( nodes );
                 
                parentNodeData.ContentNodeCount = nodeCount.Item1;
                parentNodeData.ContentRevertedNodeCount = nodeCount.Item2;

                if ( parentNode.Visible )   //'parentNode' can be hidden by user from the moment when children loading has been started
                {
                    ExpandNode( parentNode );

                    HideGrouppedNodeForBranch( parentNode );	//apopov 7.12.2015	//TODO!!!   //remove when groupped nodes will not be linked to the parent one
                }
            }
        }

        private static void ReplaceNodeState( 
            GraphTreeNodeData parentNodeData, GraphTreeNodeState stateToRemove, GraphTreeNodeState stateToAdd )
        {
            parentNodeData.NodeState &= (GraphTreeNodeState)( (int)parentNodeData.NodeState & 0xFFFF ^ (int)stateToRemove );
            parentNodeData.NodeState |= stateToAdd;
        }

        private void OnGroupNodeDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Node clicked = Part.FindAncestor<Node>(sender as UIElement);
            if (clicked != null)
            {
                e.Handled = true;

                CommandManager.ExecuteInTransaction( "ClearGroup", RemoveGroup, clicked );
            }
        }

        private void HideNodeMenuItemOnClick(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
            var contextMenuItem = sender as MenuItem;
            var context = contextMenuItem != null ? contextMenuItem.DataContext as PartManager.PartBinding : null;

            if (context == null)
            {
                return;
            }

            var node = context.Node;
            var parentdata = node != null ? node.Data as GraphTreeNodeData : null;

            if (parentdata != null)
            {
                myDiagram.StartTransaction("AddGroup");
                AddNodeToGroup(node);

                GraphTreeNodeData parentNodeData = GraphModel.FindNodeByKey( parentdata.ParentKey );
                parentNodeData.IsChangedAfterChildrenBuilt = true;

                myDiagram.CommitTransaction("AddGroup");
            }

            myDiagram.LayoutDiagram();
        }

        private void CreateNewTreeMenuItemOnClick(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
            var contextMenuItem = sender as MenuItem;
            var context = contextMenuItem != null ? contextMenuItem.DataContext as PartManager.PartBinding : null;

            if (context == null)
            {
                return;
            }

            var node = context.Node;
            var parentdata = node != null ? node.Data as GraphTreeNodeData : null;

            if (parentdata != null)
            {
                EventHandler<GraphTreeEventArgs> handlers = ShowTreeInNewWindowSelected;
                if (handlers != null)
                    handlers(this, new GraphTreeEventArgs(IsByPurchase, TaxYear, TaxQuarter, parentdata));
            }
        }

        private void ShowTableMenuItemOnClick(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
            var contextMenuItem = sender as MenuItem;
            var context = contextMenuItem != null ? contextMenuItem.DataContext as PartManager.PartBinding : null;

            if (context == null)
            {
                return;
            }
            
            var node = context.Node;
            var parentdata = node != null ? node.Data as GraphTreeNodeData : null;

            if (parentdata != null)
            {
                EventHandler<GraphTreeEventArgs> handlers = ShowTableDataSelected;
                if (handlers != null)
                    handlers(this, new GraphTreeEventArgs(IsByPurchase, TaxYear, TaxQuarter, parentdata));
            }
        }

        private void OpenDeclarationMenuItemOnClick(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
            var contextMenuItem = sender as MenuItem;
            var context = contextMenuItem != null ? contextMenuItem.DataContext as PartManager.PartBinding : null;

            if (context == null)
            {
                return;
            }

            var node = context.Node;
            var data = node != null ? node.Data as GraphTreeNodeData : null;

            if (data != null)
            {
                EventHandler<GraphTreeEventArgs> handlers = OpenDeclarationCard;
                if (handlers != null)
                    handlers(this, new GraphTreeEventArgs(IsByPurchase, TaxYear, TaxQuarter, data));
            }
        }

        private void ReBuildNodeMenuItemOnClick(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
            var contextMenuItem = sender as MenuItem;
            var context = contextMenuItem != null ? contextMenuItem.DataContext as PartManager.PartBinding : null;

            if (context != null)
                CommandManager.ExecuteInTransaction( "Refresh", RebuildNodeChildren, context.Node );
        }

        private void ShowNodeMenuItemOnClick(object sender, RoutedEventArgs e)
        {
            var menu = sender as ContextMenu;
            if (menu == null)
                return;

            var partbinding = menu.DataContext as PartManager.PartBinding;
            Node groupNode = partbinding != null ? partbinding.Node : null;

            var item = e.OriginalSource as MenuItem;
            var nodeData = item != null ? item.Header as GraphTreeNodeData : null;

            if ( groupNode != null )
            {
                Node node = GraphPartManager.FindNodeForData( nodeData, GraphModel );

                CommandManager.ExecuteInTransaction( "MoveNodeOutOfGroup", MoveNodeOutOfGroup, groupNode, node, nodeData );
	        //apopov 2.12.2015	//TODO!!! Move into command execution brackets
                myDiagram.LayoutDiagram();
            }
        }
         
        private void MouseOverText_OnCompleted(object sender, EventArgs e)
        {
            var g = sender as ClockGroup;
            if ( g != null )
            {
                DependencyObject target = Storyboard.GetTarget(g.Timeline);
                UIElement el = target as UIElement;

                if ( el != null && el.IsMouseOver )
                {
                    Link currentLink = Part.FindAncestor<Link>(el);
                    var data = (GraphTreeLinkData)currentLink.Data;
                    if ( data.ToNode.NodeType != NodeTypes.Group )
                    {
                        data.VerticalOffset = 2 / myLogScaleSlider.Value * -100;
                        data.PopupIsOpen = true;
                    } 
                }
            }
        }

        private void MouseLeaveText_OnCompleted(object sender, EventArgs e)
        {
            var g = sender as ClockGroup;
            if ( g != null )
            {
                DependencyObject target = Storyboard.GetTarget(g.Timeline);
                Link currentLink = Part.FindAncestor<Link>(target as UIElement);
               
                var data = (GraphTreeLinkData)currentLink.Data;
                if ( data != null )
                { 
                    if ( !data.MouseOverPopup )
                    {
                        data.PopupIsOpen = false;
                    }
                }
            }
        }

        private void MouseOver_OnCompleted(object sender, EventArgs e)
        {
            DependencyObject target = Storyboard.GetTarget((sender as ClockGroup).Timeline);
            Link currentLink = Part.FindAncestor<Link>(target as UIElement);
            var data = (GraphTreeLinkData)currentLink.Data;
            data.MouseOverPopup = true;
            data.PopupIsOpen = true;  
        }

        private void MouseLeave_OnCompleted(object sender, EventArgs e)
        {
            DependencyObject target = Storyboard.GetTarget((sender as ClockGroup).Timeline);
            Link currentLink = Part.FindAncestor<Link>(target as UIElement);
            var data = (GraphTreeLinkData)currentLink.Data;
            data.MouseOverPopup = false;
            data.PopupIsOpen = false;   
        }

        private void ExpandDeductionPart( ClockGroup clockGroup )
        {
            DependencyObject target = Storyboard.GetTarget( clockGroup.Timeline );
            Link currentLink = Part.FindAncestor<Link>( target as UIElement );
            GraphTreeLinkData linkData = (GraphTreeLinkData)currentLink.Data;
            GraphTreeNodeData linkToNodeData = linkData.ToNode;

            if ( !linkData.DeductionDetailsReceived )
            {
#if DONOTUSEDEDUCTIONSPEC

                GraphTreeParameters graphTreeParameters = GraphTreeParameters.New( _graphTreeParameters, linkToNodeData.TaxPayerId, isRoot: false );
                if ( _infoDeductionAction == null ) //apopov 21.6.2016	//DEBUG!!!  //GraphTreeViewer does not use Data Manager and Actions yet
                                                    //apopov 21.6.2016	//DEBUG!!!  //remove Deduction Details methods when all stuff will be passed into InfoDeductionDetailSumsAction
                    RaiseRequestDeductionDetailsData(
                        CommandManager,
                        graphTreeParameters,
                        new DeductionDetailsKeyParameters( graphTreeParameters.KeyParameters ),
                        linkToNodeData.Key );
                else
                    _infoDeductionAction.StartDataRequest( new DeductionDetailsKeyParameters( graphTreeParameters.KeyParameters ), linkToNodeData.Key );

#else //!DONOTUSEDEDUCTIONSPEC

                IEnumerable<DeductionDetail> deductionDetails = 
                    GraphTreeLinkDeductionsData.ConvertSpecificationToDeductionDetails( linkToNodeData.PreviousDeductionsSpecification );
                linkData.AddDeductionDetails( deductionDetails );

#endif //DONOTUSEDEDUCTIONSPEC
            }
        }

        private void DeductionDetailsFirstPartExpander_OnClick( object sender, EventArgs e )
        {
#if DONOTUSEDEDUCTIONSPEC
            ExpandDeductionPart( (ClockGroup)sender );
#endif //DONOTUSEDEDUCTIONSPEC
        }

        private void DeductionDetailsSecondPartExpander_OnClick( object sender, EventArgs e )
        {
            ExpandDeductionPart( (ClockGroup)sender );
        }

        private void LinkButtonMouseOver_OnCompleted(object sender, EventArgs e)
        {
            if (this.Cursor != Cursors.Wait)
                Mouse.OverrideCursor = Cursors.Hand;
        }

        private void LinkButtonMouseLeave_OnCompleted(object sender, EventArgs e)
        {
            if (this.Cursor != Cursors.Wait)
                Mouse.OverrideCursor = Cursors.Arrow;
        }

        private void OpenNewTreeDeductionDetailsItemOnClick( object sender, RoutedEventArgs e )
        {
            e.Handled = true;
            Button button = sender as Button;
            GraphTreeLinkDeductionsData deductionsData = button != null ? button.DataContext as GraphTreeLinkDeductionsData : null;

            if ( deductionsData != null && deductionsData.Link != null )
            {
                deductionsData.Link.PopupIsOpen = false;

                GraphTreeNodeData nodeData = IsByPurchase ? GraphModel.FindNodeByKey( deductionsData.Link.From )
                                                          : deductionsData.Link.ToNode;

                EventHandler<GraphTreeEventArgs> handlers = ShowTreeInNewWindowSelected;
                if ( handlers != null )
                    handlers( this, new GraphTreeEventArgs( isPurchase: true,
                        year: deductionsData.PeriodOrYear.Year, quarter: deductionsData.PeriodOrYear.QuarterNumber, data: nodeData ) );
            }
        }

        private void ShowRevertedLinksMenuItemOnClick(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
            var contextMenuItem = sender as MenuItem;
            var context = contextMenuItem != null ? contextMenuItem.DataContext as PartManager.PartBinding : null;

            if (context == null)
            {
                return;
            }

            CommandManager.ExecuteInTransaction( "ShowRevertedLinks", ShowRevertedLinks, context.Node );
            myDiagram.LayoutDiagram();
        }

        private void HideRevertedLinksMenuItemOnClick(object sender, RoutedEventArgs e)
        { 
            e.Handled = true;
            var contextMenuItem = sender as MenuItem;
            var context = contextMenuItem != null ? contextMenuItem.DataContext as PartManager.PartBinding : null;

            if (context == null)
            {
                return;
            }

            CommandManager.ExecuteInTransaction( "HideRevertedLinks", HideRevertedLinks, context.Node );
            myDiagram.LayoutDiagram();
        }

        private void OnTemplateApplied(object sender, EventArgs e)
        { 
            var b = new Binding("Scale");
            b.Source = GraphPanel;
            b.Converter = new ScaleConverter();
            b.Mode = BindingMode.TwoWay;
            myLogScaleSlider.SetBinding(Slider.ValueProperty, b);

            if (!_viewportBoundsChangedSubscribed)
            {
                myDiagram.Panel.ViewportBoundsChanged += OnPanelViewportBoundsChanged;
                _viewportBoundsChangedSubscribed = true;
            }
        }

        private void OnPanelViewportBoundsChanged( object sender, EventArgs e )
        {
            Levels.Clear();

            double offsetY = -Math.Round( myDiagram.Panel.VerticalOffset );
            var thickness = new Thickness( 10, offsetY, 0, 0 );

            levelsBlockBorder.Margin = thickness;

            var visiblesNodes = GraphPartManager.Nodes
                .Where( n => n.Visible )
                .Select(x => new { node = x, nodeData = x.NodeData() })
                .OrderBy( x => x.nodeData.TreeLevel );

            foreach ( var node in visiblesNodes )
            {
                int level = node.nodeData.NodeType == NodeTypes.Group
                    ? node.nodeData.TreeLevel - 1
                    : node.nodeData.TreeLevel;

                if ( _levels.All( x => x.LevelNumber != level ) )
                    _levels.Add( new NodeTreeLevel( level, node.nodeData.IsByPurchase ) );
            }
        }

        #endregion EventHandlers

        #region GroupNodesLogic

        private void AddNodeToGroup(Node node)
        {
            var data = (GraphTreeNodeData)node.Data;
            AddNodeToGroup(data);

            Link[] zombiIntoLinksToRemove = GetNodeIntoZombiLinks( node ).ToArray();
            if ( zombiIntoLinksToRemove.Length > 0 )
            {
                IEnumerable<Guid> nodeIdsToLink = 
                    zombiIntoLinksToRemove.Select( link => ( (GraphTreeNodeData)link.FromData ).Key ).ToArray();
                foreach ( GraphTreeLinkData linkData in zombiIntoLinksToRemove.Select( link => (GraphTreeLinkData)link.Data ) )
                {
                    GraphModel.RemoveLink( linkData );
                }
                foreach ( Guid zombiLink in nodeIdsToLink )
                {
                    data.HasZombieLinkTo++;
                    AddZombiLink( fromNodeKey: zombiLink, toNodeKey: data.GroupNode.Key );
                }
            }
            CollapsedNode( node );

            node.Visible = false;
        }

        /// <summary> Adds new nodes into the graph tree and hides exceess ones into the group node. </summary>
        /// <param name="nodes"></param>
        /// <returns> A number of nodes added. </returns>
        private Tuple<int, int> AddChildrenAndExcessesToGroup(IEnumerable<GraphTreeNodeData> nodes)
        { 
            IOrderedEnumerable<GraphTreeNodeData> oderedNodes = nodes.OrderBy(n => n, new GraphTreeNodesDescendingDataComparer());

            var nodeCount = 0;
            var revetedNodeCount = 0;
            
            foreach (GraphTreeNodeData nodeData in oderedNodes)
            {
                GraphModel.AddNode(nodeData);

                AddLink(nodeData);

                if ( nodeCount >= MaxNodesInLevel || nodeData.GraphDataNode.GraphData.IsReverted )
                {
                    AddNodeToGroup(nodeData);
                }

                if ( nodeData.GraphDataNode.GraphData.IsReverted )
                {
                    revetedNodeCount++;
                }
                else
                {
                    nodeCount++;
                }
            }
            return new Tuple<int, int>( nodeCount, revetedNodeCount);
        }

        private void AddNodeToGroup(GraphTreeNodeData data)
        {
            var parentId = data.ParentKey;
            var parentNode = GraphModel.FindNodeByKey(parentId);
             
            if (parentNode != null)
            {  
                GraphTreeGroupNodeData groupNode = null;
                 
                if (parentNode.RevertedGroupKey != Guid.Empty && data.GraphDataNode.GraphData.IsReverted)
                {
                    groupNode = GraphModel.FindNodeByKey(parentNode.RevertedGroupKey) as GraphTreeRevertedGroupNodeData; 
                }
                else if (parentNode.GroupKey != Guid.Empty && !data.GraphDataNode.GraphData.IsReverted)
                {
                    groupNode = GraphModel.FindNodeByKey(parentNode.GroupKey) as GraphTreeGroupNodeData;
                }

                if ( groupNode == null )
                {
                    if ( data.GraphDataNode.GraphData.IsReverted )
                    {
                        groupNode = new GraphTreeRevertedGroupNodeData(data.ParentKey, data.QueryLevel, data.TreeParameters);
                    }
                    else
                    {
                        groupNode = new GraphTreeGroupNodeData(data.ParentKey, data.QueryLevel, data.TreeParameters);
                    }

                    groupNode.TreeLevel = NodeTreeLevelForChild( data );

                    GraphModel.AddNode(groupNode);

                    AddLink(groupNode);

                    if (data.GraphDataNode.GraphData.IsReverted)
                    {
                        parentNode.RevertedGroupKey = groupNode.Key;
                    }
                    else
                    {
                        parentNode.GroupKey = groupNode.Key;
                    } 
                }

                groupNode.AddGroupNode(data);
                data.GroupNode = groupNode;
            } 
        }

        private void RemoveGroup( Node groupNode )
        {
            MoveZombiLinksOutOfGroup( groupNode );
            RemoveGroup( (GraphTreeGroupNodeData)groupNode.Data );
        }

        private void MoveNodeOutOfGroup( Node groupNode, Node grouppedNode, GraphTreeNodeData nodeData )
        {
            MoveZombiLinksOutOfGroup( groupNode, new[] { nodeData } );
            ExcludeNodeFromGroup( (GraphTreeGroupNodeData)groupNode.Data, grouppedNode );
        }

        private void RemoveGroup(GraphTreeGroupNodeData groupNodeData)
        {
            var grouppedNodes = GraphModel.CreateDataCollection();
            foreach ( GraphTreeNodeData node in groupNodeData.GroupNodes )
            {
                grouppedNodes.AddNode( node );
            }
            foreach ( Node groppedNode in GraphPartManager.FindNodesForData( grouppedNodes ) )
            {
                ExcludeNodeFromGroup( groupNodeData, groppedNode );
            }
        }

        private void ExcludeNodeFromGroup( GraphTreeGroupNodeData groupNodeData, Node grouppedNode )
        {
            var nodeData = (GraphTreeNodeData)grouppedNode.Data;
            nodeData.GroupNode = null;  //the node is not in the group now
            grouppedNode.Visible = true;

            if ( nodeData.IsNodeExpanded )
                ExpandNode( grouppedNode );
            else CollapsedNode( grouppedNode );

            GraphTreeNodeData parentNodeData = GraphModel.FindNodeByKey( nodeData.ParentKey );
            parentNodeData.IsChangedAfterChildrenBuilt = true;

            HideGrouppedNodeForBranch( grouppedNode );
            groupNodeData.RemoveGroupNode( nodeData );

            if ( !groupNodeData.GroupNodes.Any() )
                GraphModel.RemoveNode( groupNodeData );
        }

        private void MoveZombiLinksOutOfGroup( Node groupNode, IEnumerable<GraphTreeNodeData> grouppedNodeDatasToProcess = null )
        {
            Link[] zombiIntoLinksToRemove = FilterZombiLinks( groupNode.LinksInto ).ToArray();
            if ( zombiIntoLinksToRemove.Length > 0 )
            {
                if ( grouppedNodeDatasToProcess == null )
                    grouppedNodeDatasToProcess = ( (GraphTreeGroupNodeData)groupNode.Data ).GroupNodes;
                MoveZombiLinksOutOfGroup( zombiIntoLinksToRemove, grouppedNodeDatasToProcess );
            }
        }

        private void MoveZombiLinksOutOfGroup(IEnumerable<Link> zombiIntoLinksToMove, IEnumerable<GraphTreeNodeData> grouppedNodeDatasToProcess)
        {
            var zombiLinks = (
                from zombiLink in zombiIntoLinksToMove.Select(
                    link => new { LinkData = (GraphTreeLinkData)link.Data, FromData = (GraphTreeNodeData)link.FromData })
                join zombiNodeDataInGroup in grouppedNodeDatasToProcess
                on zombiLink.FromData.Inn equals zombiNodeDataInGroup.Inn
                select new
                {
                    LinkData = zombiLink.LinkData,
                    FromKey = zombiLink.FromData.Key,
                    ToKey = zombiNodeDataInGroup.Key
                }).ToArray();

            foreach (var zombiLink in zombiLinks)
            {
                GraphModel.RemoveLink(zombiLink.LinkData);
            }
            foreach (var zombiLink in zombiLinks)
            {
                var toNode = GraphModel.FindNodeByKey(zombiLink.ToKey);
                if (toNode != null)
                {
                    toNode.HasZombieLinkTo++;
                }
                AddZombiLink(zombiLink.FromKey, zombiLink.ToKey);
                //by design adds a zombi link to a hidden yet node inside group removing below
            }
        }

        private void HideGrouppedNodeForBranch( Node parentNode )
        {
            string parentInn = parentNode.NodeData().Inn;	//apopov 11.12.2015	//TODO!!! 'parentNode' can be a group node. While its parent node is included into the group and should be filtered here?!

            foreach ( Node node in GetBranchVisibleNodes( parentNode, 
                predicate: ( nodePar, parentNodePar ) => 
                    nodePar.NodeData().NodeType != NodeTypes.Group && nodePar.NodeData().Inn != parentInn ).ToArray() )  //checks node visibility before changing their visible state
            {
                GraphTreeNodeData data = node.NodeData();
                if ( data.IsInGroup )
                {
                    CollapsedNode( node );

                    node.Visible = false;
                }
                else
                {
                    HideGrouppedNodeForBranch( node );
                }
            }
        }

        private void RebuildNodeChildren( Node node )
        {
            foreach ( Node child in node.NodesOutOf.Where( nodeof => nodeof.Data is GraphTreeGroupNodeData ) )
            {
                RemoveGroup( child );
            }

            int i = 1;
            foreach ( Node nodeof in node.NodesOutOf )
            {
                if ( i++ > MaxNodesInLevel || ((GraphTreeNodeData)nodeof.Data).IsReverted)
                    AddNodeToGroup( nodeof );
                else if ( !nodeof.Visible )
                    nodeof.Visible = true;
            }
            ( (GraphTreeNodeData)node.Data ).IsChangedAfterChildrenBuilt = false;
        }

        private void ShowRevertedLinks(Node node)
        {
            if (node != null)
            {
                var parentdata = node.Data as GraphTreeNodeData;
                if (parentdata != null)
                {
                    parentdata.IsCanShowRevertedLinks = false;
                    parentdata.IsCanHideRevertedLinks = true;
                }

                var nodesRevertedLinksNodes = this.GetNodeRevertedLinks(node);
                foreach (var x in nodesRevertedLinksNodes)
                {
                    if (!((GraphTreeLinkData)x.Data).ToNode.IsInGroup)
                    {
                        x.ToNode.Visible = true;
                    }
                }
            }
        }
         
        private void HideRevertedLinks(Node n)
        {
            if (n != null)
            {
                var parentdata = n.Data as GraphTreeNodeData;
                if (parentdata != null)
                {
                    parentdata.IsCanShowRevertedLinks = true;
                    parentdata.IsCanHideRevertedLinks = false;
                }

                var nodesRevertedLinksNodes = this.GetNodeRevertedLinks(n);
                foreach (var x in nodesRevertedLinksNodes)
                {
                    Link[] zombiIntoLinksToRemove = GetNodeIntoZombiLinks(x.ToNode).ToArray();
                    if (zombiIntoLinksToRemove.Length > 0)
                    {
                        foreach (GraphTreeLinkData linkData in zombiIntoLinksToRemove.Select(link => (GraphTreeLinkData)link.Data))
                        {
                            GraphModel.RemoveLink(linkData);
                        }
                    }
	                //apopov 21.7.2016	//fix of GNIVC_NDS2-5558
                    if ( x.ToNode.IsExpandedTree )
                        CollapsedNode( x.ToNode );
                    x.ToNode.Visible = false;
                }
            }
        }
         
        #endregion

        #region LinksLogic

        private void AddLink( GraphTreeNodeData node )
        {
            Contract.Requires( node.ParentKey != GraphTreeNodeData.NoNodeKey );
            Contract.Requires( node.NodeType == NodeTypes.Group || IsByPurchase == node.IsByPurchase );

            GraphTreeLinkData newLinkData = GraphTreeLinkData.New( node );
            GraphModel.AddLink( newLinkData );
        }

        private void AddZombiLink( GraphTreeNodeData fromNode, GraphTreeNodeData toNode )
        {
            GraphTreeNodeData toNodePaint = toNode.IsInGroup ? toNode.GroupNode : toNode;
            toNode.HasZombieLinkTo++;
            AddZombiLink( fromNode.Key, toNodePaint.Key );
        }

        private void AddZombiLink( Guid fromNodeKey, Guid toNodeKey )
        {
            GraphTreeLinkData newZombiLinkData = GraphTreeLinkData.NewZombi( fromNodeKey, toNodeKey );
            GraphModel.AddLink( newZombiLinkData );
        }


        //apopov 1.12.2015	//TODO!!! replace by implementation with input parameter of type 'Node' and combine with IsZombiOutLinkPresent()
        private void RemoveNodeZombiLinks( Guid fromKey )
        {
            foreach ( GraphTreeLinkData linkData in GetZombiLinks( fromKey ).ToArray() )
            {
                GraphModel.RemoveLink( linkData );
            };
        }

        /// <summary> </summary>
        /// <param name="fromKey"> A node key to filter by <see cref="GraphTreeLinkData.From"/>. Optional. Process all links defined by <paramref /> if it is 'null' (by default). </param>
        /// <returns> Zombi links found. </returns>
        private IEnumerable<GraphTreeLinkData> GetZombiLinks( Guid fromKey )
        {
            return FilterZombiLinks( linksToProcess: null, fromKey: fromKey ).Select( link => (GraphTreeLinkData)link.Data );
        }

        private IEnumerable<Link> GetNodeRevertedLinks( Node node )
        {
            if (node.LinksOutOf == null)
            {
                return Enumerable.Empty<Link>();
            }

            var revertedLinkes = node.LinksOutOf.Where(link =>
                (((GraphTreeLinkData)link.Data).ToNode.GraphDataNode.GraphData != null && ((GraphTreeLinkData)link.Data).ToNode.GraphDataNode.GraphData.IsReverted) ||
                ((GraphTreeLinkData)link.Data).ToNode is GraphTreeRevertedGroupNodeData);

            return revertedLinkes;
        }

        private bool IsNodeHasRevertedLinks( Node node )
        {
            var linksNodes = GetNodeRevertedLinks(node);
            var hasRevertedLinks = linksNodes.Any();
             
            return hasRevertedLinks; 
        }

        private IEnumerable<Link> GetNodeIntoZombiLinks( Node node )
        {
            return FilterZombiLinks( node.LinksInto );
        }

        private IEnumerable<Link> GetNodeOutOfZombiLinks( Node node )
        {
            return FilterZombiLinks( node.LinksOutOf );
        }

        /// <summary> </summary>
        /// <param name="linksToProcess"> Links to process. Optional. Processes all model links if it is 'null' (by default). </param>
        /// <returns> Zombi links found. </returns>
        private IEnumerable<Link> FilterZombiLinks( IEnumerable<Link> linksToProcess = null )
        {
            return FilterZombiLinks( linksToProcess, fromKey: GraphTreeNodeData.NoNodeKey );
        }

        /// <summary> </summary>
        /// <param name="linksToProcess"> Links to process. Optional. Processes all model links if it is 'null'. </param>
        /// <param name="fromKey"> A node key to filter by <see cref="GraphTreeLinkData.From"/>. Optional. Process all links defined by <paramref name="linksToProcess"/> if it is 'null'. </param>
        /// <returns> Zombi links found. </returns>
        private IEnumerable<Link> FilterZombiLinks( IEnumerable<Link> linksToProcess, Guid fromKey )
        {
            if ( linksToProcess == null )
                linksToProcess = myDiagram.Links;
            IEnumerable<Link> zombiLinks = linksToProcess.Where( link => ( (GraphTreeLinkData)link.Data ).IsZombiLink );
            if ( fromKey != GraphTreeNodeData.NoNodeKey )
                zombiLinks = zombiLinks.Where( link => ( (GraphTreeLinkData)link.Data ).From == fromKey );

            return zombiLinks;
        }

        #endregion LinksLogic

        #region ZombieNodesLogic

        private void HideUnexpectedWanderingZombiLinksIfNeeded(Node parentNode)
        {
            foreach (Node visibleChildNode in GetBranchVisibleNodes(parentNode,
                levelDownDepthLimit: -1, predicate: (nodePar, parentNodePar) =>
                    !nodePar.NodeData().IsInGroup
                    && (nodePar.NodeData().NodeType == NodeTypes.Group 
                        || nodePar.NodeData().TaxPayerId != parentNodePar.NodeData().TaxPayerId)))
            {
                HideNodeIntoZombiLinksIfNeeded(visibleChildNode);
            }
        }

        private void HideNodeIntoZombiLinksIfNeeded( Node node )
        {
            foreach ( Link visibleLink in GetNodeIntoZombiLinks( node ).Where( link => link.Visible && !link.FromNode.Visible ) )
            {
                visibleLink.Visible = false;
            }

            foreach (Link visibleLink in GetNodeOutOfZombiLinks(node).Where(link => link.Visible && !link.ToNode.Visible))
            {
                visibleLink.Visible = false;
            }
        }

        private IEnumerable<GraphTreeNodeData> FindBackRelationNodes( GraphTreeNodeData node )
        {
            return FindOtherNodesById( node.TaxPayerId, node.Key );
        }

        private IEnumerable<GraphTreeNodeData> FindOtherNodesById( TaxPayerId taxPayerId, Guid excludedKey )
        {
            IEnumerable<GraphTreeNodeData> otherNodesById = 
                from node in GetAllNonGroupNodeDatas()
                where node.TaxPayerId == taxPayerId && node.Key != excludedKey
                select node;

            return otherNodesById;
        }

        private bool IsZombiOutLinkPresent( Node node )
        {
            bool isZombi = false;
            if ( ( (GraphTreeNodeData)node.Data ).IsZombie )
                isZombi = node.LinksOutOf.Any( link => ( (GraphTreeLinkData)link.Data ).IsZombiLink );

            return isZombi;
        }

        #endregion ZombieNodesLogic

        #region Legend

        public ObservableCollection<NodeTreeLevel> Levels
        {
            get
            {
                return _levels ?? (_levels = new ObservableCollection<NodeTreeLevel>());
            }
        }

        private int MaxNodesInLevel
        {
            get { return _graphTreeParameters.MaxNodesInLevel; }
        }

        private void BtnBottomMenuHideClick(object sender, RoutedEventArgs e)
        {
            ShowHideMenu("sbHideBottomMenu", btnBottomMenuHide, btnBottomMenuShow, pnlBottomMenu);
        }

        private void BtnBottomMenuShowClick(object sender, RoutedEventArgs e)
        {
            ShowHideMenu("sbShowBottomMenu", btnBottomMenuHide, btnBottomMenuShow, pnlBottomMenu);
        }
        
        private void ShowHideMenu(string storyboard, Button btnHide, Button btnShow, StackPanel pnl)
        {
            var sb = Resources[storyboard] as Storyboard;
            if (sb == null)
            {
                return;
            }

            sb.Begin(pnl);

            if (storyboard.Contains("Show"))
            {
                btnHide.Visibility = System.Windows.Visibility.Visible;
                btnShow.Visibility = System.Windows.Visibility.Hidden;
            }
            else if (storyboard.Contains("Hide"))
            {
                btnHide.Visibility = System.Windows.Visibility.Hidden;
                btnShow.Visibility = System.Windows.Visibility.Visible;
            }
        }

        #endregion Legend 
    }
} 
