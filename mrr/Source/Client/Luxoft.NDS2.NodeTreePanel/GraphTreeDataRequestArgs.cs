﻿using System;
using System.Diagnostics.Contracts;
using Luxoft.NDS2.Client.NodeTreePanel.Interfaces;
using Luxoft.NDS2.Common.Helpers.Enums;

namespace Luxoft.NDS2.Client.NodeTreePanel
{
    /// <summary> Arguments for event <see cref="NodeTree.RequestTreeData"/>. </summary>
    public class GraphTreeDataRequestArgs : EventArgs
    {
        /// <summary> </summary>
        /// <param name="commandManager"></param>
        /// <param name="treeParameters"></param>
        /// <param name="parentMpdeKey"> A parent identifier <see cref="GraphTreeNodeData.Key"/> or <see cref="GraphTreeNodeData.NoNodeKey"/> if there is not a parent node (for example for the root node).  </param>
        /// <param name="priority"></param>
        public GraphTreeDataRequestArgs( ICommandTransactionManager commandManager, GraphTreeParameters treeParameters, Guid parentMpdeKey, CallExecPriority priority = default(CallExecPriority) )
        {
            Contract.Requires( commandManager != null );
            Contract.Requires( treeParameters != null );
            if ( parentMpdeKey == Guid.Empty )
                throw new ArgumentNullException( "ParentKey" );
            Contract.EndContractBlock();

            CommandManager      = commandManager;
            Parameters          = treeParameters;
            ParentKey           = parentMpdeKey;
            Priority            = priority;
        }

        public ICommandTransactionManager CommandManager { get; private set; }

        public GraphTreeParameters Parameters { get; private set; }

        /// <summary> A parent identifier <see cref="GraphTreeNodeData.Key"/> or <see cref="GraphTreeNodeData.NoNodeKey"/> if there is not a parent node (for example for the root node). </summary>
        public Guid ParentKey { get; private set; }

        public CallExecPriority Priority { get; set; }
    }
}