﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Northwoods.GoXam.Model;

namespace Luxoft.NDS2.Client.NodeTreePanel
{
    public class GraphTreeGroupNodeData : GraphTreeNodeData
    {
        private ObservableCollection<GraphTreeNodeData> groupNodes = new ObservableCollection<GraphTreeNodeData>();

        private decimal _mappedAmount;
        private decimal _notMappedAmnt;
        private decimal _discrepancyAmnt;
        private decimal _discrepancyNdsAmnt;

        private ICollectionView groupNodesListView;
        
        public ObservableCollection<GraphTreeNodeData> GroupNodes
        {
            get { return groupNodes; }
        }
        
        public ICollectionView GroupNodesListView
        {
            get
            {
                return groupNodesListView ?? (groupNodesListView = CollectionViewSource.GetDefaultView(groupNodes));
            }
        }

        private string innSearch;

        public string InnSearch
        {
            get { return innSearch; }
            set
            {
                innSearch = value;
                OnPropertyChanged("InnSearch");

                GroupNodesListView.Filter = GetFilter(value, nameSearch, ndsSearch);
            }
        }

        private string nameSearch;

        public string NameSearch
        {
            get { return nameSearch; }
            set
            {
                nameSearch = value;
                OnPropertyChanged("NameSearch");

                GroupNodesListView.Filter = GetFilter(innSearch, value, ndsSearch); 
            }
        }

        private string ndsSearch;

        public string NdsSearch
        {
            get { return ndsSearch; }
            set
            {
                ndsSearch = value;
                OnPropertyChanged("NdsSearch");

                GroupNodesListView.Filter = GetFilter(innSearch, nameSearch, value);
            }
        }

        private Predicate<object> GetFilter(string inn, string name, string nds)
        {
            var ndsAmnt = StringToDecimal(nds);

            return o => (string.IsNullOrEmpty(inn) || (!string.IsNullOrEmpty(((GraphTreeNodeData)o).Inn) && ((GraphTreeNodeData)o).Inn.ToLower().Contains(inn.ToLower())))
                        && (string.IsNullOrEmpty(name) || (!string.IsNullOrEmpty(((GraphTreeNodeData)o).NameForSearch) && ((GraphTreeNodeData)o).NameForSearch.ToLower().Contains(name.ToLower()))) 
                        && (((GraphTreeNodeData)o).MainAmnt >= ndsAmnt);
        }


        public override string Inn { get { return string.Empty; } } 
        public override string Kpp { get { return string.Empty; } }
        public override string Name { get { return string.Empty; } }
        public override string ParentInn { get { return string.Empty; } }
        public override string ParentKpp { get { return string.Empty; } }
        public override string ParentName { get { return string.Empty; } }
        public override decimal CreatedInvoiceAmnt { get { return 0; } }
        public override decimal ReceivedInvoiceAmnt { get { return 0; } }
        public override decimal DeductionBefore20150101Amnt { get { return 0; } }
        public override decimal DeductionAfter20150101Amnt { get { return 0; } }

        public override decimal MappedAmount
        {
            get { return _mappedAmount; }
            set
            {
                if ( _mappedAmount != value )
                {
                    _mappedAmount = value;
                   OnPropertyChanged( "MappedAmount" );                    
                   OnPropertyChanged( "MappedAmountText" );                    
                }
            }
        }

        public override decimal NotMappedAmnt
        {
            get { return _notMappedAmnt; }
            set
            {
                if ( _notMappedAmnt != value )
                {
                    _notMappedAmnt = value;
                   OnPropertyChanged( "NotMappedAmnt" );                    
                   OnPropertyChanged( "NotMappedAmntText" );                    
                }
            }
        }

        public override decimal DiscrepancyAmnt
        {
            get { return _discrepancyAmnt; }
            set
            {
                if (_discrepancyAmnt != value)
                {
                    _discrepancyAmnt = value;
                    OnPropertyChanged("DiscrepancyAmnt");
                    OnPropertyChanged("DiscrepancyAmntText"); 
                    OnPropertyChanged("DiscrepancyAmntSum");
                    OnPropertyChanged("DiscrepancyAmntSumText");
                }
            }
        }

        public override decimal DiscrepancyNdsAmnt
        {
            get { return _discrepancyNdsAmnt; }
            set
            {
                if (_discrepancyNdsAmnt != value)
                {
                    _discrepancyNdsAmnt = value;
                    OnPropertyChanged("DiscrepancyNdsAmnt");
                    OnPropertyChanged("DiscrepancyNdsAmntText"); 
                    OnPropertyChanged("DiscrepancyAmntSum");
                    OnPropertyChanged("DiscrepancyAmntSumText");
                }
            }
        } 

        public override decimal DiscrepancySumAmnt
        {
            get { return DiscrepancyAmnt + DiscrepancyNdsAmnt; }
        }

        public override NodeTypes NodeType
        {
            get { return NodeTypes.Group; }
        }

        /// <summary> Связь построена не по данным родительского узла, а по данным дочернего </summary>
        public override bool IsReverted { get { return false; } }

        /// <summary> A number of child or groupped nodes. Child nodes are not loaded yet if it is 'null'. </summary>
        public override int? ContentNodeCount
        {
            get
            {
                return groupNodes.Count;
            }
            set
            {
                OnPropertyChanged( "ContentNodeCount" );                    
                OnPropertyChanged( "ContentNodeCountText" );                    
            }
        }

        /// <summary> A bottom node text about a number of nodes (child or groupped). </summary>
        public override string ContentNodeCountText
        {
            get
            {
                return string.Format("Всего: {0}", ContentNodeCount); 
            }
        }

        /// <summary> Margin of the node bottom label like 'Нет связей' </summary>
        public override Thickness LinkCountLabelMagin
        {
            get
            {
                Thickness thickness = base.LinkCountLabelMagin;
                thickness.Left += 15;
                return thickness;
            }
        } 

        /// <summary> </summary> 
        /// <param name="parentKey"> A parent node key <see cref="GraphTreeNodeData.Key"/> or <see cref="GraphTreeNodeData.NoNodeKey"/> if there is not a parent node (for example for the root node). </param>
        /// <param name="queryLevel"></param>
        /// <param name="treeParameters"></param>
        public GraphTreeGroupNodeData( Guid parentKey, long queryLevel, GraphTreeParameters treeParameters )
            : base( parentKey, treeParameters: treeParameters )
        {
            QueryLevel = queryLevel; 
        }
        
        public void AddGroupNode(GraphTreeNodeData node)
        {
            groupNodes.Add(node);
            groupNodes = new ObservableCollection<GraphTreeNodeData>(groupNodes.OrderByDescending(item => item.MainAmnt));
            MappedAmount += node.MappedAmount;
            NotMappedAmnt += node.NotMappedAmnt;
            DiscrepancyAmnt += node.DiscrepancyAmnt;
            DiscrepancyNdsAmnt += node.DiscrepancyNdsAmnt;

            ContentNodeCount = -1;  //only causes OnPropertyChanged() inside for ContentNodeCount and ContentNodeCountText

            var e = new ModelChangedEventArgs { Data = this };
            OnPropertyChanged(e);
        }

        public void RemoveGroupNode(GraphTreeNodeData node)
        {
            MappedAmount -= node.MappedAmount;
            NotMappedAmnt -= node.NotMappedAmnt;
            DiscrepancyAmnt -= node.DiscrepancyAmnt;
            DiscrepancyNdsAmnt -= node.DiscrepancyNdsAmnt;
            groupNodes.Remove(node);

            ContentNodeCount = -1;  //only causes OnPropertyChanged() inside for ContentNodeCount and ContentNodeCountText

            var e = new ModelChangedEventArgs { Data = this };
            OnPropertyChanged(e);
        }

        /// <summary> Converts a string money into a decimal value. </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        private decimal StringToDecimal(string val)
        {
            try
            {
                return !string.IsNullOrEmpty(val) ? Convert.ToDecimal(val.Replace('.', ',')) : 0;
            }
            catch (Exception)
            {
                return 0;
            }
        }
    }
}
