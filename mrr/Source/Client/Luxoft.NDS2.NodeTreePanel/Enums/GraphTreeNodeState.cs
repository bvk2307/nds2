﻿using System;

namespace Luxoft.NDS2.Client.NodeTreePanel.Enums
{
    /// <summary> A state of <see cref="GraphTreeNodeData"/> (see <see cref="GraphTreeNodeData.NodeState"/>. </summary>
    [Flags]
    public enum GraphTreeNodeState
    {
        Unknown             = (int)default(GraphTreeNodeState),
        LoadingChildren     = 0x01,
        ChildrenLoaded      = 0x02,  
        LoadingChildCount   = 0x04,
        ChildCountLoaded   = 0x08  
    }
}