﻿namespace Luxoft.NDS2.Client.NodeTreePanel.Enums
{
    /// <summary> Types of graph links. ATTENTION! Do not change names of this enumeration because they are used as keys in XAML of <see cref="NodeTree"/>. </summary>
    public enum LinkTypes
    {
        Link
    }
}