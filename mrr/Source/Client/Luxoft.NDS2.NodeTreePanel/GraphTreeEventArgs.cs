﻿using System;

namespace Luxoft.NDS2.Client.NodeTreePanel
{
    public sealed class GraphTreeEventArgs : EventArgs
    {
        private GraphTreeNodeData nodeData;

        public bool IsPurchase { get; set; }
        public int TaxYear { get; set; }
        public int TaxQuarter { get; set; }

        public GraphTreeNodeData NodeData
        {
            get { return nodeData; }
            set { nodeData = value; }
        }

        public GraphTreeEventArgs()
        {
        }

        public GraphTreeEventArgs(bool isPurchase, int year, int quarter, GraphTreeNodeData data)
        {
            IsPurchase = isPurchase;
            TaxYear = year;
            TaxQuarter = quarter;
            NodeData = data;
        }
    }
}
