﻿using System.Collections.Generic;
using Northwoods.GoXam.Layout;

namespace Luxoft.NDS2.Client.NodeTreePanel
{
    public sealed class GraphTreeVertexComparer : IComparer<TreeVertex>
    {
        private readonly GraphTreeNodesDescendingDataComparer _nodeComparer = new GraphTreeNodesDescendingDataComparer();

        public int Compare( TreeVertex vertex1, TreeVertex vertex2 )
        {
            GraphTreeNodeData node1 = vertex1.Node.Data as GraphTreeNodeData;
            GraphTreeNodeData node2 = vertex2.Node.Data as GraphTreeNodeData;
            return _nodeComparer.Compare( node1, node2 );
        }
    }

    public sealed class GraphTreeNodesDescendingDataComparer : IComparer<GraphTreeNodeData>
    {
        public int Compare( GraphTreeNodeData node1, GraphTreeNodeData node2 )
        {
            int order = object.ReferenceEquals( node1, node2 ) ? 0 : 1;
            if ( order != 0 )
            {   //descending order by the sum
                if ( node1 is GraphTreeRevertedGroupNodeData )
                {
                    order = 1;
                }
                else if ( node2 is GraphTreeRevertedGroupNodeData )
                {
                    order = -1;
                }
                else
                {
                    if ( node1 is GraphTreeGroupNodeData )
                        order = node2.IsReverted ? -1 : 1;
                    else if ( node2 is GraphTreeGroupNodeData )
                        order = node1.IsReverted ? 1 : -1;
                    else if ( node1.IsReverted && !node2.IsReverted )
                        order = 1;
                    else if ( node2.IsReverted && !node1.IsReverted )
                        order = -1;
                    else if ( node1.IsReverted )    //'node2.IsReverted == true' here
                        order = node2.NotMappedAmnt.CompareTo( node1.NotMappedAmnt );
                    else    //'node1.IsReverted == false && node2.IsReverted == false' here
                        order = node2.MainAmnt.CompareTo( node1.MainAmnt );
                    //acsending order by INN
                    if ( order == 0 )
                        order = string.Compare( node1.TaxPayerId.Inn, node2.TaxPayerId.Inn, Common.Contracts.Constants.KeysComparison );
                }
            }
            return order;    //descending order
        }
    }
}
