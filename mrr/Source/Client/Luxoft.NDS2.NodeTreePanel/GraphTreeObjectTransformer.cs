﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Windows.Media;
using FLS.Common.Controls.Wpf.Brushes;
using FLS.CommonComponents.CorLibAddins.Helpers;
using Luxoft.NDS2.Client.NodeTreePanel.Interfaces;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting.TaxPayer;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace.Enums;

namespace Luxoft.NDS2.Client.NodeTreePanel
{
    public class GraphTreeObjectTransformer
    {
        #region Private members 

        private readonly IDeclarationUserPermissionsPolicy _declarationUserPermissionsPolicy;
        private readonly HashSet<TaxPayerId> _usedDtos = new HashSet<TaxPayerId>();
        private readonly SolidColorBrush _defaultColor;
        private readonly IReadOnlyDictionary<int, Tuple<SolidColorBrush, string>> _surs;

        /// <summary> Parameters of the sheet with tree graph. </summary>
        private GraphTreeParameters _treeParameters = null;

        #endregion Private members 

        public GraphTreeObjectTransformer( GraphTreeParameters treeParameters,
            IDeclarationUserPermissionsPolicy declarationUserPermissionsPolicy, IReadOnlyDictionary<int, Tuple<SolidColorBrush, string>> surs )
        {
            Contract.Requires( treeParameters != null );
            Contract.Requires( declarationUserPermissionsPolicy != null );
            Contract.Requires( surs != null );

            _treeParameters              = treeParameters;
            _declarationUserPermissionsPolicy   = declarationUserPermissionsPolicy;
            _defaultColor                       = GetDefaultColor();
            _surs                               = surs;
        }

        /// <summary> </summary>
        /// <param name="graphDatas"></param>
        /// <param name="parentKey"> A parent node key <see cref="GraphTreeNodeData.Key"/> or <see cref="GraphTreeNodeData.NoNodeKey"/> if the node is root one. </param>
        /// <returns></returns>
        public IEnumerable<GraphTreeNodeData> TransformTreeData( IEnumerable<GraphData> graphDatas, Guid parentKey )
        {
            foreach ( GraphData graphData in graphDatas )
            {
                yield return TransformDataToNodeObject( graphData, parentKey );
            }
        }

        private SolidColorBrush GetDefaultColor()
        {
             var color = ColorConverter.ConvertFromString("#D1D1D1");

            try
            {
                return SolidColorBrushHelper.GetColorBrush((Color)color);
            }
            catch (Exception)
            {
                return SolidColorBrushHelper.GetColorBrush(Colors.White);
            }
        }

        /// <summary> </summary>
        /// <param name="graphData"></param>
        /// <param name="parentKey"> A parent node key <see cref="GraphTreeNodeData.Key"/> or <see cref="GraphTreeNodeData.NoNodeKey"/> if the node is root one. </param>
        /// <returns></returns>
        private GraphTreeNodeData TransformDataToNodeObject( GraphData graphData, Guid parentKey )
        {
            int sur                 = graphData.Sur.HasValue ? graphData.Sur.Value : 0;
            bool isSpecialInn       = SpecialInnsHelper.IsSpecialInn(graphData.Inn);
            var nodedata            = new GraphTreeNodeData( parentKey, graphData, _treeParameters )
            {
                Sur                 = sur,
                HasBackRelation     = false,
                IsSpecial           = isSpecialInn
            };
            TaxPayerId taxPayerId   = nodedata.GraphDataNode.TaxPayerId;

            if ( !isSpecialInn )
            {
                if ( _usedDtos.Contains( taxPayerId ) )
                    nodedata.IsZombie = true;

                if ( string.IsNullOrEmpty( nodedata.Inspection.EntryId ) )	//apopov 28.10.2016	//TODO!!!   //is not allowed in any cases now if SOUN_CODE is unknown
                    nodedata.IsCanBuildTree = false;
                else
                    nodedata.IsCanBuildTree = _declarationUserPermissionsPolicy.IsTreeRelationEligible( nodedata.Inspection.EntryId );
            }
            SetNodeView( nodedata );

            if ( !nodedata.IsSpecial && !nodedata.IsZombie )
                _usedDtos.Add( taxPayerId );

            return nodedata;
        }

        private void SetNodeView( GraphTreeNodeData data )
        {
            if ( !data.IsSpecial )
            {
                data.IconPath = GetNodeIconPath( data.DeclarationType );
                data.IsIconVisible = data.IconPath != null;
            }
            data.NodeColor = GetNodeColor( -1 );

            if ( data.IsSpecial )
                data.ImagePath = SpecialInnsHelper.GetSpecialInnImgPath(data.Inn);
            else
                data.NodeColor = GetNodeColor( data.Sur );
            if ( data.IsZombie )
                data.ImagePath = "Images/zombie.png";
            else if ( data.IsTransitional )
                data.ImagePath = "Images/transitional.png";
        }

        private SolidColorBrush GetNodeColor( int surCode )
        {
            SolidColorBrush color = _defaultColor;
            Tuple<SolidColorBrush, string> sur;
            if ( surCode != 0 && _surs.TryGetValue( surCode, out sur ) )
                color = sur.Item1;

            return color;
        }

        protected string GetNodeIconPath( DeclarationType? declarationType )
        {
            string iconSpec = null;
            if ( declarationType.HasValue )
            {
                switch ( declarationType.Value )
                {case DeclarationType.ToPay:
                    iconSpec = "Images/declaration_plus.png";
                    break;
                case DeclarationType.Zero:
                    iconSpec = "Images/declaration_zero.png";
                    break;
                case DeclarationType.ToCharge:
                    iconSpec = "Images/declaration_minus.png";
                    break;
                }
            }
            if ( iconSpec == null )
                iconSpec = "Images/declaration_cross.png";

            return iconSpec;
        }
    }
}
