﻿namespace Luxoft.NDS2.Client.NodeTreePanel
{
    /// <summary> Types of graph nodes. ATTENTION! Do not change names of this enumeration because they are used as keys in XAML of <see cref="NodeTree"/>. </summary>
    public enum NodeTypes
    {
        Node,
        Group
    }
}
