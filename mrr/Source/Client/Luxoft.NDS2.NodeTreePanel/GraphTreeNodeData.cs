﻿using System;
using System.Windows;
using System.Windows.Media;
using FLS.Common.Controls.Wpf.Brushes;
using FLS.CommonComponents.CorLibAddins.Helpers;
using Luxoft.NDS2.Client.NodeTreePanel.Constants;
using Luxoft.NDS2.Client.NodeTreePanel.Enums;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration.Enums;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid.Extensions;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting.TaxPayer;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace.Enums;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Northwoods.GoXam.Model;

namespace Luxoft.NDS2.Client.NodeTreePanel
{
    [Serializable]
    public class GraphTreeNodeData : GraphLinksModelNodeData<Guid>
    {
        /// <summary> The tree level of the tree root node. </summary>
        public const int TreeRootLevel = 0;

        /// <summary> The query level of the query root node. </summary>
        public const long QueryRootLevel = 1;

        public static readonly Guid NoNodeKey = new Guid( "0B0BFE60-E590-478D-96E4-6D6BA65F63C8" );

        #region Private members

        private readonly GraphDataNode _graphData;
        private readonly GraphTreeParameters _treeParameters;
         
        private Guid _parentKey = Guid.Empty;
      
        private long _queryLevel;
        private int _sur = 0;
        private int? _contentNodeCount;
        private int? _contentRevertedNodeCount;

        private bool _isCanShowRevertedLinks;
        private bool _isCanHideRevertedLinks;

        private bool _isCanBuildTree = true;
        private bool isNodeExpanded;
        private bool isChangedAfterChildrenBuilt = false;
        private GraphTreeNodeState _nodeState;
        private int _treeLevel = int.MinValue;
        
        #endregion Private members

        #region Data properties

        public GraphDataNode GraphDataNode
        {
            get { return _graphData; }
        }

        public GraphTreeParameters TreeParameters
        {
            get { return _treeParameters; }
        }
          
        /// <summary> A parent identifier <see cref="GraphTreeNodeData.Key"/> or <see cref="NoNodeKey"/> if there is not a parent node (for example for the root node). </summary>
        public Guid ParentKey
        {
            get { return _parentKey; }
            set
            {
                if ( value == Guid.Empty )
                    throw new ArgumentNullException( "ParentKey" );

                _parentKey = value;
            }
        }

        /// <summary> Tax payer string identifier. </summary>
        public TaxPayerId TaxPayerId
        {
            get
            {
                return _graphData.TaxPayerId;
            }
        }

        public virtual string Inn { get { return _graphData.Inn; } }

        public virtual string ParentInn { get { return _graphData.ParentInn; } }


        public virtual decimal MappedAmount
        {
            get { return _graphData.MappedAmount; }
            set
            {
                throw new ArgumentException( string.Format( "Setter isn't supported in: '{0}', assigning value: '{1}'", this.GetType().Name, value ), "MappedAmount" );
            }
        }

        public virtual decimal NotMappedAmnt
        {
            get { return _graphData.NotMappedAmnt; }
            set
            {
                throw new ArgumentException( string.Format( "Setter isn't supported in: '{0}', assigning value: '{1}'", this.GetType().Name, value ), "NotMappedAmnt" );
            }
        }

        public decimal MainAmnt
        {
            get { return IsByPurchase ? NotMappedAmnt : MappedAmount; }
        }

        public virtual String ParentKpp { get { return _graphData.Parent.Kpp; } }

        public virtual String Kpp { get { return _graphData.Kpp; } }

        public virtual String ParentName
        {
            get
            {
                return _graphData.Parent.Name;
            }
        }
         
        public virtual String Name
        {
            get
            {
                return _graphData.Name;
            }
        }

        public virtual String NameForSearch
        {
            get
            {
                var name = SpecialInnsHelper.GetSpecialInnName(_graphData.Inn);

                return string.IsNullOrEmpty(name) ? _graphData.Name : name;
            }
        } 

        public DeclarationType DeclarationType
        {
            get
            {
                return _graphData.GraphData.SignType;
            }
        }

        /// <summary> Document type code: a declaration or a journal. </summary>
        public DeclarationTypeCode TypeCode { get { return _graphData.TypeCode; } }

        /// <summary> A declaration inspection code and name. </summary>
        public CodeValueDictionaryEntry Inspection { get { return _graphData.Inspection; } }

        /// <summary> Processing stage in 'МС'. </summary>
        public DeclarationProcessignStage ProcessingStage { get { return _graphData.ProcessingStage; } }

        /// <summary> Количество покупателей </summary>
        public long ClientNumber { get { return _graphData.ClientNumber; } }

        /// <summary> Количество продавцов </summary>
        public long SellerNumber { get { return _graphData.SellerNumber; } }

        /// <summary> Вычет по НДС, руб. </summary>
        public decimal DeductionNds { get { return _graphData.DeductionNds; } }

        /// <summary> Исчисленный НДС, руб. </summary>
        public decimal CalcNds { get { return _graphData.CalcNds; } }

        /// <summary> Доля вычетов в исчисленом НДС, % </summary>
        public decimal NdsPercentage { get { return _graphData.NdsPercentage; } }

        public int Sur
        {
            get { return _sur; }
            set { _sur = value; }
        }

        public string SurDescription { get { return _graphData.SurDescription; } }

        public virtual decimal DiscrepancyAmnt
        {
            get { return _graphData.DiscrepancyAmnt; }
            set
            {
                throw new ArgumentException( string.Format( "Setter isn't supported in: '{0}', assigning value: '{1}'", this.GetType().Name, value ), "DiscrepancyAmnt" );
            }
        }

        //Сумма расхождений вида проверка НДС
        public virtual decimal DiscrepancyNdsAmnt
        {
            get { return _graphData.DiscrepancyNdsAmnt; }
            set
            {
                throw new ArgumentException(string.Format("Setter isn't supported in: '{0}', assigning value: '{1}'", this.GetType().Name, value), "DiscrepancyNdsAmnt");
            }
        }

        //Сумма расхождений видаразрыв и проверка НДС
        public virtual decimal DiscrepancySumAmnt
        {
            get { return _graphData.DiscrepancySumAmnt; }
            set
            {
                throw new ArgumentException(string.Format("Setter isn't supported in: '{0}', assigning value: '{1}'", this.GetType().Name, value), "DiscrepancySumAmnt");
            }
        }

        //НДС по журналу учета выставленных СФ
        public virtual decimal CreatedInvoiceAmnt { get { return _graphData.CreatedInvoiceAmnt; } }

        //НДС по журналу учета полученных СФ
        public virtual decimal ReceivedInvoiceAmnt { get { return _graphData.ReceivedInvoiceAmnt; } }

        //Данные о вычетах покупателя до 01.01.2015
        public virtual decimal DeductionBefore20150101Amnt { get { return _graphData.DeductionBefore20150101Amnt; } }

        //Данные о вычетах покупателя после 01.01.2015
        public virtual decimal DeductionAfter20150101Amnt { get { return _graphData.DeductionAfter20150101Amnt; } }

        public bool IsZombie { get; set; }

        public bool IsTransitional { get { return _graphData.IsTransitional; } }

        public decimal VatShare { get { return _graphData.VatShare; } }

        public decimal VatTotal { get { return _graphData.VatTotal; } }

        public decimal GapDiscrepancyTotal { get { return _graphData.GapDiscrepancyTotal; } }

        public decimal NdsDiscrepancyTotal { get { return _graphData.NdsDiscrepancyTotal; } }

        public decimal DiscrepancySumAmntTotal { get { return _graphData.DiscrepancySumAmntTotal; } }

        /// <summary> A node level in the source data request. </summary>
        public long QueryLevel
        {
            get { return _graphData == null ? _queryLevel : _graphData.Level; }
            protected set { _queryLevel = value; }
        }

        /// <summary> A node level in the graph tree. </summary>
        public int TreeLevel
        {
            get { return _treeLevel; }
            set
            {
                if ( value < 0 )
                    throw new InvalidOperationException( "GraphTreeNodeData.TreeLevel must hasve a non negative value" );
                if ( _treeLevel >= 0 )
                    throw new InvalidOperationException( "GraphTreeNodeData.TreeLevel is assignable only one time" );

                _treeLevel = value;
            }
        }

        public decimal ClientNdsPercentage { get { return _graphData.ClientNdsPercentage; } }

        public decimal SellerNdsPercentage { get { return _graphData.SellerNdsPercentage; } }

        /// <summary> Specification of 'Вычеты контрагента за предыдущие периоды' (deduction details for the previous period) as the list of value pairs: period - sum with delimiters. </summary>
        public string PreviousDeductionsSpecification { get { return _graphData.PreviousDeductionsSpecification; } }

        #endregion Data properties

        #region Node Properties

        /// <summary> Исчисленный НДС, руб., в строковом представлении </summary>
        public string ClacNdsVal
        {
            get { return GraphDataNode.GraphData.ClacNdsString(); }
        }

        /// <summary> Исчисленный НДС, руб., в строковом представлении c 'руб.' на конце </summary>
        public string CalcNdsText
        {
            get { return string.Format("{0} руб.", ClacNdsVal); }
        }

        /// <summary> Вычет по НДС, руб., в строковом представлении </summary>
        public string DeductionNdsVal   
        {
            get { return GraphDataNode.GraphData.DeductionNdsString(); }
        }

        /// <summary> Вычет по НДС, руб., в строковом представлении с 'руб.' на конце. </summary>
        public string DeductionNdsText
        {
            get { return string.Format("{0} руб.", DeductionNdsVal); }
        }

        /// <summary> Доля вычетов в исчисленом НДС, % </summary>
        public string NdsPercentageText
        {
            get { return string.Format("{0} %", NumericFormatHelper.ToTwoDigitAfterDot(NdsPercentage)); }
        }

        /// <summary> Label 'Cумма НДС к возмещению|уплате:' </summary>
        public string VatTotalLabel
        {
            get { return string.Format( "Cумма НДС к {0}:", VatTotal >= 0 ? "уплате" : "возмещению" ); }
        }

        /// <summary> Cумма НДС к возмещению|уплате, руб. +|- </summary>
        public string VatTotalText
        {
            get { return string.Format("{0} руб.", NumericFormatHelper.ToTwoDigitAfterDot( VatTotal ) ); }
        }

        public string MainAmntText
        {
            get { return NumericFormatHelper.ToTwoDigitAfterDot( MainAmnt ); }
        }

        public string MappedAmountText
        {
            get { return NumericFormatHelper.ToTwoDigitAfterDot( MappedAmount ); }
        }

        public string NotMappedAmntText
        {
            get { return NumericFormatHelper.ToTwoDigitAfterDot( NotMappedAmnt ); }
        }

        public string DiscrepancyAmntText
        {
            get { return NumericFormatHelper.ToTwoDigitAfterDot(DiscrepancyAmnt); }
        }

        public string DiscrepancyAmntSumText
        {
            get { return NumericFormatHelper.ToTwoDigitAfterDot(DiscrepancySumAmnt); }
        }

        public int InvoiceAmntRowNum
        {
            get { return IsByPurchase ? 5 : 4; }
        }

        public int ReceivedAmntRowNum
        {
            get { return IsByPurchase ? 4 : 5; }
        }

        public string CreatedInvoiceAmntText
        {
            get { return NumericFormatHelper.ToTwoDigitAfterDot(CreatedInvoiceAmnt); }
        }

        public string ReceivedInvoiceAmntText
        {
            get { return NumericFormatHelper.ToTwoDigitAfterDot(ReceivedInvoiceAmnt); }
        }

        public string DeductionBefore20150101AmntText
        {
            get { return NumericFormatHelper.ToTwoDigitAfterDot(DeductionBefore20150101Amnt); }
        }

        public string DeductionAfter20150101AmntText
        {
            get { return NumericFormatHelper.ToTwoDigitAfterDot(DeductionAfter20150101Amnt); }
        }
        
        public string DiscrepancyNdsAmntText
        {
            get { return NumericFormatHelper.ToTwoDigitAfterDot(DiscrepancyNdsAmnt); }
        }

        /// <summary> Доля вычетов в исчисленном НДС у контрагента, %%, в строковом представлении </summary>
        public string NdsPercentageVal
        {
            get { return GraphDataNode.GraphData.NdsPercentageString(); }
        }

        /// <summary> Margin of the node bottom label like 'Нет связей' </summary>
        public virtual Thickness LinkCountLabelMagin
        {
            get
            {
                if (IsByPurchase)
                {
                    return new Thickness(0, 12, 0, 0);
                }

                return new Thickness(0, 0, 0, 0);
            }
        }

        public SolidColorBrush StripeControlledBackground
        {
            get
            {
                if ( TreeLevel % 2 == 0 )
                {
                    return StripesConstants.StripeColor;
                }

                return SolidColorBrushHelper.GetColorBrush( Colors.White );
            }
        }
         
        public bool IsByPurchase
        {
            get { return _treeParameters == null ? false : _treeParameters.KeyParameters.IsPurchase; } 
        }

        public bool ShowReverted
        {
            get { return _treeParameters == null ? false : _treeParameters.KeyParameters.ShowReverted; }
        }

        public bool HasBackRelation { get; set; }

        /// <summary> Does a node allow to start a new graph window? </summary>
        public bool IsCanBuildTree
        {
            get { return _isCanBuildTree && !IsSpecial; }
            set { _isCanBuildTree = value; }
        }

        /// <summary> Can a node show children in the table sheet? </summary>
        public bool IsCanShowTable
        {
            get
            {
                return IsCanBuildTree && IsChildLevelEligible; 
                       //MastHaveDeclaration && ( !ContentNodeCount.HasValue || ContentNodeCount.Value > 0 )    //the condition to don't do if a node does not have links
            }
        }

        /// <summary> Is a level of children eligible for the current user by permissions? </summary>
        public bool IsChildLevelEligible
        {   //child nodes have level 'nodeData.Level + 1' that must meet the layer limitation: '<= layersLimit + 1'
            get { return _treeParameters.GraphLayersDepthLimit <= 0 || TreeLevel < _treeParameters.GraphLayersDepthLimit; }
        }

	    //apopov 11.12.2015	//TODO!!! check this property on node opening
        /// <summary> Does a node allow to expand its children? </summary>
        public bool IsExpandable { get; set; }

        public bool IsNodeExpanded
        {
            get { return isNodeExpanded; }

            set
            {
                if ( isNodeExpanded != value )
                {
                    isNodeExpanded = value;
                    OnPropertyChanged("IsNodeExpanded");
                }
            }
        }

        public bool IsNotQueryRootLevel
        {
            get { return QueryLevel != QueryRootLevel; }
        }

        public bool IsNotTreeRootLevel
        {
            get { return TreeLevel != TreeRootLevel; }
        }

        public bool IsChangedAfterChildrenBuilt
        {
            get { return isChangedAfterChildrenBuilt; }
            set
            {
                if ( isChangedAfterChildrenBuilt != value )
                {
                    isChangedAfterChildrenBuilt = value;
                    OnPropertyChanged("IsChangedAfterChildrenBuilt");
                }
            }
        }

        public GraphTreeNodeState NodeState
        {
            get { return _nodeState; }
            set
            {
                if ( _nodeState != value )
                {
                    _nodeState = value;
                    OnPropertyChanged( "NodeState" );                    
                    OnPropertyChanged( "ContentNodeCountText" );                    
                }
            }
        }

        /// <summary> A number of child or groupped nodes. Child nodes are not loaded yet if it is 'null'. </summary>
        public virtual int? ContentNodeCount
        {
            get { return _contentNodeCount; }
            set
            {
                if (_contentNodeCount != value)
                {
                    _contentNodeCount = value;
                    OnPropertyChanged("ContentNodeCount");
                    OnPropertyChanged("ContentNodeCountText");
                    OnPropertyChanged("IsCanShowTable");
                }
            }
        }

        public virtual int? ContentRevertedNodeCount
        {
            get { return _contentRevertedNodeCount; }
            set
            {
                if (_contentRevertedNodeCount != value)
                {
                    _contentRevertedNodeCount = value;
                    OnPropertyChanged("ContentRevertedNodeCount");
                    OnPropertyChanged("ContentNodeCountText");
                }
            }
        }

        /// <summary> A bottom node text about a number of nodes (child or groupped). </summary>
        public virtual string ContentNodeCountText
        {
            get
            { 
                if ( NodeState.HasFlag( GraphTreeNodeState.LoadingChildren ) || NodeState.HasFlag( GraphTreeNodeState.LoadingChildCount ) )
                {
                    return "Загрузка связей  ...";
                }

                if ( ContentNodeCount.HasValue || ContentRevertedNodeCount.HasValue )
                {
                    int contentNodeCount = 0;
                    if (ContentNodeCount.HasValue)
                    {
                        contentNodeCount = ContentNodeCount.Value;
                    }

                    if ( IsByPurchase || (!IsByPurchase && !ShowReverted ) )
                    { 
                        if (contentNodeCount > 0)
                        {
                            return string.Format("Связи: {0}", contentNodeCount);
                        }
                    }
                    else
                    {
                        int contentRevertedNodeCount = 0;
                        if (ContentRevertedNodeCount.HasValue)
                        {
                            contentRevertedNodeCount = ContentRevertedNodeCount.Value;
                        }

                        if (contentNodeCount > 0 || contentRevertedNodeCount > 0)
                        {
                            return string.Format("Связи: {0} ({1})", contentNodeCount, contentRevertedNodeCount);
                        }
                    }
                     
                    return "Связей нет"; 
                }
                 
                return string.Empty; 
            }
        }

        public bool IsCanShowRevertedLinks
        {
            get { return _isCanShowRevertedLinks; }
            set
            {
                if (_isCanShowRevertedLinks != value)
                {
                    _isCanShowRevertedLinks = value;
                    OnPropertyChanged("IsCanShowRevertedLinks");
                }
            }
        }

        public bool IsCanHideRevertedLinks
        {
            get { return _isCanHideRevertedLinks; }
            set
            {
                if (_isCanHideRevertedLinks != value)
                {
                    _isCanHideRevertedLinks = value;
                    OnPropertyChanged("IsCanHideRevertedLinks");
                }
            }
        }

        public object InnColorInSearch
        {
            get { return HasZombieLinkTo > 0 ? Colors.Red : DependencyProperty.UnsetValue; }    //uses system selection style settings
        }

        private int _numOfZombieLinkTo;
        public int HasZombieLinkTo
        {
            get { return _numOfZombieLinkTo; }
            set
            {
                _numOfZombieLinkTo = value;
                OnPropertyChanged("InnColorInSearch");
            }
        }
        
        public bool ShowTooltip
        {
            get { return !IsSpecial; }
        }

        public bool IsCanOpenDeclaration
        {
            get { return !IsSpecial; }
        }

        public bool IsSpecial { get; set; }

        public string ImagePath { get; set; }

        public string IconPath { get; set; }

        public bool IsIconVisible { get; set; }

        public SolidColorBrush NodeColor { get; set; }
        
        public virtual NodeTypes NodeType
        {
            get { return NodeTypes.Node; }
        }

        /// <summary> Связь построена не по данным родительского узла, а по данным дочернего </summary>
        public virtual bool IsReverted { get { return _graphData.IsReverted; } }

        public string NodeTypeKey
        {
            get { return Enum.GetName(typeof(NodeTypes), NodeType); }
        }

        public bool ZombiLinkVisible { get; set; }
        
        public GraphTreeGroupNodeData GroupNode { get; set; }

        public bool IsInGroup { get { return GroupNode != null; } }

        public Guid GroupKey { get; set; }

        public Guid RevertedGroupKey { get; set; }

        #endregion

        /// <summary> </summary> 
        /// <param name="parentKey"> A parent node key <see cref="GraphTreeNodeData.Key"/> or <see cref="NoNodeKey"/> if there is not a parent node (for example for the root node). </param>
        /// <param name="graphData"></param>
        /// <param name="treeParameters"></param>
        public GraphTreeNodeData( Guid parentKey, GraphData graphData = null, GraphTreeParameters treeParameters = null )
        {
            Key                 = Guid.NewGuid();
            ParentKey           = parentKey;
            _graphData          = new GraphDataNode(graphData);
            _treeParameters     = treeParameters;
        }

        public void OnPropertyChanged( String info )
        {
            OnPropertyChanged( new ModelChangedEventArgs( info, data: null, oldval: null, newval: null ) );
        }
    }
}
