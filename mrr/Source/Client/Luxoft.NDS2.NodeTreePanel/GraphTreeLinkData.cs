﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Media;
using FLS.Common.Controls.Wpf.Brushes;
using Luxoft.NDS2.Client.NodeTreePanel.Constants;
using Luxoft.NDS2.Client.NodeTreePanel.Enums;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid.Helpers;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Northwoods.GoXam;
using Northwoods.GoXam.Model;

namespace Luxoft.NDS2.Client.NodeTreePanel
{ 
    public sealed class GraphTreeLinkData : GraphLinksModelLinkData<Guid, Guid?>
    {
        private readonly GraphTreeNodeData _toNode;
           
        private bool _deductionDetailsFirstPartIsShown;
        private bool _deductionDetailsSecondPartIsShown;

        private bool _popupIsOpen;
        private bool _mouseOverPopup;
        private double _verticalOffset; 

        public GraphTreeNodeData ToNode
        {
            get { return _toNode; }
        }

        public bool IsPurchase { get; set; }

        public bool IsZombiLink { get; private set; }

        public string Inn { get { return ToNode.Inn; } }
        
        public string ParentKpp { get { return ToNode.ParentKpp; } }

        public string Kpp { get { return ToNode.Kpp; } }

        public string ParentName { get { return ToNode.ParentName; } }
        
        public string Name { get { return ToNode.Name; } }

        public string ParentInn { get { return ToNode.ParentInn; } }


        public string NotMappedAmntLabelTextFormatted
        {
            get { return string.Format("{0} руб.", NotMappedAmntLabelText); }
        }

        public string NotMappedAmntLabelText { get { return ToNode.NotMappedAmntText; } }


        public string MappedAmntLabelTextFormatted
        {
            get { return string.Format("{0} руб.", MappedAmountLabelText); }
        }

        public string MappedAmountLabelText { get { return ToNode.MappedAmountText; } }


        public string DiscrepancyNdsAmntLabelTextFormatted
        {
            get { return string.Format("{0} руб.", DiscrepancyNdsAmntLabelText); }
        }

        public string DiscrepancyNdsAmntLabelText { get { return ToNode.DiscrepancyNdsAmntText; } }


        public string DiscrepancyAmntLabelTextFormatted
        {
            get { return string.Format("{0} руб.", DiscrepancyAmntLabelText); }
        }

        public string DiscrepancyAmntLabelText { get { return ToNode.DiscrepancyAmntText; } }


        public string DiscrepancyAmntSumLabelText { get { return ToNode.DiscrepancyAmntSumText; } }


        public string CreatedInvoiceAmntTextFormatted
        {
            get { return string.Format("{0} руб.", CreatedInvoiceAmntText); }
        }

        public string CreatedInvoiceAmntText { get { return ToNode.CreatedInvoiceAmntText; } }


        public string ReceivedInvoiceAmntTextFormatted
        {
            get { return string.Format("{0} руб.", ReceivedInvoiceAmntText); }
        }
        
        public string ReceivedInvoiceAmntText { get { return ToNode.ReceivedInvoiceAmntText; } }


        public string DeductionBefore20150101AmntTextFormatted
        {
            get { return string.Format("{0} руб.", DeductionBefore20150101AmntText); }
        }

        public string DeductionBefore20150101AmntText { get { return ToNode.DeductionBefore20150101AmntText; } }


        public string DeductionAfter20150101AmntTextFormatted 
        {
            get { return string.Format("{0} руб.", DeductionAfter20150101AmntText); }
        }

        public string DeductionAfter20150101AmntText { get { return ToNode.DeductionAfter20150101AmntText; } }

         
        public ObservableCollection<GraphTreeLinkDeductionsData> PreviousQuarterDeductions { get; set; }


        public Dock LinkBlueLabelDock
        {
            get { return IsBlueTopAndGreenBottom ? Dock.Top : Dock.Bottom; }
        }

        public Dock LinkGreenLabelDock
        {
            get { return IsBlueTopAndGreenBottom ? Dock.Bottom : Dock.Top; }
        }

        private bool IsBlueTopAndGreenBottom
        {
            get { return IsPurchase; }
        }

        public bool IsDiscrepancyAmntLabelRed
        {
            get { return ToNode.DiscrepancySumAmnt > 0; }
        }

        public bool IsDiscrepancyAmntLabelGrey
        {
            get { return !IsDiscrepancyAmntLabelRed; }
        }

        // A value of the Arrowhead enum which is used for the LinkPanel.ToArrowProperty.
        public Arrowhead ToArrow
        {
            get { return IsPurchase ? Arrowhead.None : Arrowhead.Triangle; }
        }

        // A value of the Arrowhead enum which is used for the LinkPanel.FromArrowProperty.
        public Arrowhead FromArrow
        {
            get { return IsPurchase ? Arrowhead.BackwardTriangle : Arrowhead.None; }
        }
        // A double which represents the Route.ToShortLength property.
        public double ToShortLength
        {
            get { return IsPurchase ? 0 : 8; }
        }

        public string IsRevertedColor
        {
            get
            {
                if (ToNode is GraphTreeRevertedGroupNodeData)
                {
                    return "5 3";
                }

                if (ToNode is GraphTreeGroupNodeData)
                {
                    return "1 0";
                }
                 
                if ( !ToNode.GraphDataNode.GraphData.IsReverted )
                {
                    return "1 0"; 
                }

                return "5 3";
            }
        }

        // A double which represents the Route.FromShortLengthProperty dependency property.
        public double ArrowFromShortLength
        {
            get { return IsPurchase ? 24 : 12; }
        }

        public string LinkTypeKey
        {
            get { return Enum.GetName(typeof(LinkTypes), LinkTypes.Link); }
        }

        public SolidColorBrush StripeControlledBackground
        {
            get
            {
                if ( ToNode.TreeLevel % 2 == 0 && ToNode.NodeType != NodeTypes.Group )
                { 
                    return StripesConstants.StripeColor;  
                }

                if ( ToNode.TreeLevel % 2 != 0 && ToNode.NodeType == NodeTypes.Group )
                {
                    return StripesConstants.StripeColor; 
                } 

                return SolidColorBrushHelper.GetColorBrush(Colors.White);
            }
        }
         
        private GraphTreeLinkData(GraphTreeNodeData node, bool isPurchase)
        {
            Category = LinkTypeKey;
            From = node.ParentKey;
            To = node.Key;

            _toNode = node;
            IsPurchase = isPurchase;

	//apopov 9.12.2015	//TODO!!! uncomment when link labels will be moved so will not intersect
            if (node.NodeType == NodeTypes.Group)
                node.PropertyChanged += OnToNodePropertyChanged; 
        }

        private GraphTreeLinkData(Guid from, Guid to)
        {
            Category = "zombilink";
            From = from;
            To = to;

            IsZombiLink = true;
        }

        public static GraphTreeLinkData New(GraphTreeNodeData node)
        {
            return new GraphTreeLinkData(node, node.IsByPurchase);
        }

        public static GraphTreeLinkData NewZombi(Guid fromNodeKey, Guid toNodeKey)
        {
            return new GraphTreeLinkData(from: fromNodeKey, to: toNodeKey);
        }

        public bool DeductionDetailsReceived { get; set; }

        public bool DeductionDetailsFirstPartIsShown
        {
            get
            {
                return _deductionDetailsFirstPartIsShown;
            }
            set
            {
                _deductionDetailsFirstPartIsShown = value;
                RaisePropertyChanged("DeductionDetailsFirstPartIsShown", value);
            }
        }

        public bool DeductionDetailsSecondPartIsShown
        {
            get
            {
                return _deductionDetailsSecondPartIsShown;
            }
            set
            {
                _deductionDetailsSecondPartIsShown = value;
                RaisePropertyChanged("DeductionDetailsSecondPartIsShown", value);
            }
        }

        public bool PopupIsOpen
        {
            get
            {
                return this._popupIsOpen;
            }
            set
            {
                this._popupIsOpen = value;
                RaisePropertyChanged("PopupIsOpen", value);
            }
        }
          
        public bool MouseOverPopup
        {
            get { return _mouseOverPopup; }
            set
            {
                _mouseOverPopup = value;
                RaisePropertyChanged("MouseOverPopup", value);
            }
        }

        public double VerticalOffset
        {
            get { return _verticalOffset; }
            set
            {
                _verticalOffset = value;
                RaisePropertyChanged("VerticalOffset", value);
            }
        }

        private void OnToNodePropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            switch (propertyChangedEventArgs.PropertyName)
            {
                case "NotMappedAmntText":
                RaisePropertyChanged("NotMappedAmntLabelText", NotMappedAmntLabelText);
                break;
            case "MappedAmountText":
                RaisePropertyChanged("MappedAmountLabelText", MappedAmountLabelText);
                break;
            case "DiscrepancyAmntText":
                RaisePropertyChanged("DiscrepancyAmntLabelText", DiscrepancyAmntLabelText);
                RaisePropertyChanged("DiscrepancyAmntSumLabelText", DiscrepancyAmntSumLabelText);
                RaisePropertyChanged("IsDiscrepancyAmntLabelRed", IsDiscrepancyAmntLabelRed);
                RaisePropertyChanged("IsDiscrepancyAmntLabelGrey", IsDiscrepancyAmntLabelGrey);
                break;
            case "DiscrepancyNdsAmntText":
                RaisePropertyChanged("DiscrepancyAmntLabelText", DiscrepancyAmntLabelText);
                RaisePropertyChanged("DiscrepancyAmntSumLabelText", DiscrepancyAmntSumLabelText);
                RaisePropertyChanged("IsDiscrepancyAmntLabelRed", IsDiscrepancyAmntLabelRed);
                RaisePropertyChanged("IsDiscrepancyAmntLabelGrey", IsDiscrepancyAmntLabelGrey);
                break;
            }
        }

        private void RaisePropertyChanged(string propertyName, object newValue)
        {
            RaisePropertyChanged(propertyName, oldval: null, newval: newValue);
        }

        public void AddDeductionDetails( IEnumerable<DeductionDetail> deductionDetails )
        {
            List<DeductionDetail> sortedDeductionDetails = deductionDetails.ToList();
            sortedDeductionDetails.Sort( new ReverseDecoratorComparer<DeductionDetail>( new DeductionDetailComparer() ) );

            PreviousQuarterDeductions = deductionDetails == null 
                ? null : new ObservableCollection<GraphTreeLinkDeductionsData>( 
                         GraphTreeLinkDeductionsData.NewsFromDetails( this, sortedDeductionDetails ) );
            RaisePropertyChanged( "PreviousQuarterDeductions", deductionDetails );

            DeductionDetailsReceived = deductionDetails != null;
        }
    }
}
