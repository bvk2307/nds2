﻿namespace Luxoft.NDS2.Client.NodeTreePanel.Constants
{
    public static class SliderConstants
    {
        public const double MinimumScale = 0.4;
        public const double MaximumScale = 3.9;
    }
}