﻿using System.Windows.Media;
using FLS.Common.Controls.Wpf.Brushes;

namespace Luxoft.NDS2.Client.NodeTreePanel.Constants
{
    public static class StripesConstants
    {
        public static readonly SolidColorBrush StripeColor = SolidColorBrushHelper.GetColorBrush( Colors.Honeydew );
    }
}