﻿namespace Luxoft.NDS2.Client.NodeTreePanel.Constants
{
    public static class GridSizeConstants
    {
        public const double ByPurchaseSize = 86.75;
        public const double Size = 82.75; 
    }
}