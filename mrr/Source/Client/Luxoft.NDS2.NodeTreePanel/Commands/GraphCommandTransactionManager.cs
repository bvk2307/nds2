﻿using System;
using System.Threading;
using FLS.Common.Lib.Primitives;
using Luxoft.NDS2.Client.NodeTreePanel.Interfaces;
using Northwoods.GoXam.Model;

namespace Luxoft.NDS2.Client.NodeTreePanel.Commands
{
    public sealed class GraphCommandTransactionManager : ICommandTransactionManager
    {
        private readonly int _instanceKey;
        private readonly NodeTree _nodeTree;
        private readonly DiagramModel _diagramModel;
        private readonly CancellationTokenSource _cancelSource;

        private Int32Counter _extCommandCounter;
        private int? _internalCommandCounter = null;

        public GraphCommandTransactionManager( 
            int instanceKey, Int32Counter counter, NodeTree nodeTree, DiagramModel diagramModel, CancellationTokenSource cancelSource = null )
        {
            _instanceKey            = instanceKey;
            _extCommandCounter      = counter;
            _nodeTree               = nodeTree;
            _diagramModel           = diagramModel;
            _cancelSource           = cancelSource;
            _internalCommandCounter = 0;
        }

        public int InstanceKey
        {
            get { return _instanceKey; }
        }

        public CancellationToken CancelToken { get { return _cancelSource.Token; } }

        public void ExecuteInTransaction( string transactionName, Action action )
        {
            DisableCommandsIfNeeded();

            bool errorCaused = false;
            _diagramModel.StartTransaction( transactionName );
            try
            {
                action();
            }
            catch ( Exception )
            {
                errorCaused = true;
                _diagramModel.RollbackTransaction();

                throw;
            }
            finally
            {
                if ( !errorCaused )
                    _diagramModel.CommitTransaction( transactionName );

                EnableCommandsIfNeeded();
            }
        }

        public void ExecuteInTransaction<T1>( string transactionName, Action<T1> action, T1 param1 )
        {
            DisableCommandsIfNeeded();

            bool errorCaused = false;
            _diagramModel.StartTransaction( transactionName );
            try
            {
                action( param1 );
            }
            catch ( Exception )
            {
                errorCaused = true;
                _diagramModel.RollbackTransaction();

                throw;
            }
            finally
            {
                if ( !errorCaused )
                    _diagramModel.CommitTransaction( transactionName );

                EnableCommandsIfNeeded();
            }
        }

        public void ExecuteInTransaction<T1, T2>( string transactionName, Action<T1, T2> action, T1 param1, T2 param2 )
        {
            DisableCommandsIfNeeded();

            bool errorCaused = false;
            _diagramModel.StartTransaction( transactionName );
            try
            {
                action( param1, param2 );
            }
            catch ( Exception )
            {
                errorCaused = true;
                _diagramModel.RollbackTransaction();

                throw;
            }
            finally
            {
                if ( !errorCaused )
                    _diagramModel.CommitTransaction( transactionName );

                EnableCommandsIfNeeded();
            }
        }

        public void ExecuteInTransaction<T1, T2, T3>( string transactionName, 
            Action<T1, T2, T3> action, T1 param1, T2 param2, T3 param3 )
        {
            DisableCommandsIfNeeded();

            bool errorCaused = false;
            _diagramModel.StartTransaction( transactionName );
            try
            {
                action( param1, param2, param3 );
            }
            catch ( Exception )
            {
                errorCaused = true;
                _diagramModel.RollbackTransaction();

                throw;
            }
            finally
            {
                if ( !errorCaused )
                    _diagramModel.CommitTransaction( transactionName );

                EnableCommandsIfNeeded();
            }
        }

        public void ExecuteInTransaction<T1, T2, T3, T4, T5, T6, T7, T8>( string transactionName, 
            Action<T1, T2, T3, T4, T5, T6, T7, T8> action, 
            T1 param1, T2 param2, T3 param3, T4 param4, T5 param5, T6 param6, T7 param7, T8 param8 )
        {
            DisableCommandsIfNeeded();

            bool errorCaused = false;
            _diagramModel.StartTransaction( transactionName );
            try
            {
                action( param1, param2, param3, param4, param5, param6, param7, param8 );
            }
            catch ( Exception )
            {
                errorCaused = true;
                _diagramModel.RollbackTransaction();

                throw;
            }
            finally
            {
                if ( !errorCaused )
                    _diagramModel.CommitTransaction( transactionName );

                EnableCommandsIfNeeded();
            }
        }

        public void StartTransaction( string transactionName )
        {
            _diagramModel.StartTransaction( transactionName );
            DisableCommandsIfNeeded();
        }

        public void CommitTransaction( string transactionName )
        {
            _diagramModel.CommitTransaction( transactionName );
            EnableCommandsIfNeeded();
        }

        public void RollbackTransaction()
        {
            _diagramModel.RollbackTransaction();
            EnableCommandsIfNeeded();
        }

        private void DisableCommandsIfNeeded()
        {
            if ( IsDisposed )
                throw new ObjectDisposedException( objectName: GetType().FullName );

            ++_internalCommandCounter;
            if ( 1 >= ++_extCommandCounter )
                _nodeTree.DisableGraphCommandsIfNeeded();
        }

        private void EnableCommandsIfNeeded()
        {
            if ( IsDisposed )
                throw new ObjectDisposedException( GetType().FullName );

            --_internalCommandCounter;
            if ( 0 >= --_extCommandCounter )
                _nodeTree.EnableGraphCommandsIfNeeded();
        }

        private bool IsDisposed
        {
            get { return !_internalCommandCounter.HasValue; }
        }

        /// <summary> Rollbacks transaction and enables the node tree on clearing. </summary>
        /// <remarks> Can be called any times but only in the main UI thread. </remarks>
        public void Dispose()
        {
            if ( !IsDisposed ) 
            {
                // ReSharper disable once PossibleInvalidOperationException
                int internalCommandCounter = _internalCommandCounter.Value;
                _internalCommandCounter = null;

                if ( _cancelSource != null )
                    _cancelSource.Cancel();

                if ( internalCommandCounter > 0 )
                {
                    _diagramModel.RollbackTransaction();

                    if ( 0 >= _extCommandCounter.Decrement( internalCommandCounter ) )
                        _nodeTree.EnableGraphCommandsIfNeeded();
                }
            }
        }
    }
}