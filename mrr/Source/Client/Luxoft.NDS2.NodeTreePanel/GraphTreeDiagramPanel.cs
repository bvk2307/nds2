﻿using System;
using System.Reflection;
using Northwoods.GoXam;

namespace Luxoft.NDS2.Client.NodeTreePanel
{
    public sealed class GraphTreeDiagramPanel : DiagramPanel
    {
        public event EventHandler PartVisibleChangedPublic;

        protected override void OnInitialLayoutCompleted()
        {
            base.OnInitialLayoutCompleted();

            AddHandlerToPartVisibleChanged();
        }

        private void AddHandlerToPartVisibleChanged()
        {
            //ATTENTION! Uses internal event of DiagramPanel by reflection
            EventInfo eventInfo = typeof(DiagramPanel).GetEvent( 
                "PartVisibleChanged", BindingFlags.Instance | BindingFlags.DeclaredOnly | BindingFlags.NonPublic );
            var handler = new EventHandler( RaisePartVisibleChangedPublic );
            MethodInfo addMethod = eventInfo.GetAddMethod( nonPublic: true );
            addMethod.Invoke( this, new[] { handler } );
        }

        private void RaisePartVisibleChangedPublic( object sender, EventArgs eventArgs )
        {
            var handler = PartVisibleChangedPublic;
            if ( handler != null )
                handler( sender, eventArgs );
        }
    }
}