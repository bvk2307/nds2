﻿namespace Luxoft.NDS2.Client.NodeTreePanel
{
    using System;

    public class GraphTreeRevertedGroupNodeData : GraphTreeGroupNodeData
    {
        public GraphTreeRevertedGroupNodeData(Guid parentKey, long queryLevel, GraphTreeParameters treeParameters)
            : base(parentKey, queryLevel, treeParameters)
        {
        }

        /// <summary> Связь построена не по данным родительского узла, а по данным дочернего </summary>
        public override bool IsReverted { get { return true; } }
    }
}