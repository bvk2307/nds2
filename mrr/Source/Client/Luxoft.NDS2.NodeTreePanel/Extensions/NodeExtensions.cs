﻿using Northwoods.GoXam;

namespace Luxoft.NDS2.Client.NodeTreePanel.Extensions
{
    /// <summary> Auxilaries for <see cref="Node"/>. </summary>
    public static class NodeExtensions
    {
        /// <summary> A typed value of <see cref="Node.Data"/>. </summary>
        /// <param name="parentNode"></param>
        /// <returns></returns>
        public static GraphTreeNodeData NodeData( this Node parentNode )
        {
            return ( (GraphTreeNodeData)parentNode.Data );
        }
    }
}