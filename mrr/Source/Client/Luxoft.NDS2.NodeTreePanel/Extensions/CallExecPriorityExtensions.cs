﻿using Luxoft.NDS2.Common.Helpers.Enums;

namespace Luxoft.NDS2.Client.NodeTreePanel.Extensions
{
    /// <summary> Extension methods for <see cref="CallExecPriority"/>. </summary>
    public static class CallExecPriorityExtensions
    {
        public static bool IsLoadingCountsOnly( this CallExecPriority callExecPriority )
        {
            return callExecPriority == CallExecPriority.Background;
        }
    }
}