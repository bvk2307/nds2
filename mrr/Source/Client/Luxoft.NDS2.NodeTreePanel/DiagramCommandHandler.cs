﻿using Northwoods.GoXam;

namespace Luxoft.NDS2.Client.NodeTreePanel
{
    public class DiagramCommandHandler : CommandHandler
    {
        private readonly double minScale;

        private readonly double maxScale;

        public DiagramCommandHandler(double minScale, double maxScale)
        {
            this.minScale = minScale;
            this.maxScale = maxScale;
        }

        public override void DecreaseZoom(object param)
        {
            if (this.Diagram.Panel.Scale > minScale)
                base.DecreaseZoom(param);
        }

        public override void IncreaseZoom(object param)
        {
            if (this.Diagram.Panel.Scale < maxScale)
                base.IncreaseZoom(param);
        }
    }
}
