﻿using Luxoft.NDS2.Client.NodeTreePanel.Constants;

namespace Luxoft.NDS2.Client.NodeTreePanel
{
    public class NodeTreeLevel 
    { 
        public NodeTreeLevel()
        {
        }

        public NodeTreeLevel(int level, bool isByPurchase)
        {
            IsByPurchase = isByPurchase;
            LevelNumber = level;
        }

        public double Offset
        {
            get
            {
                if (IsByPurchase)
                {
                    return GridSizeConstants.ByPurchaseSize * 3/2d;     
                }

                return GridSizeConstants.Size * 3/2d;
            }
        }

        public bool IsByPurchase { get; private set; }

        public int LevelNumber { get; private set; }

        public string LevelText
        {
            get
            {
                return LevelNumber.ToString();
            }
        }
    }
}