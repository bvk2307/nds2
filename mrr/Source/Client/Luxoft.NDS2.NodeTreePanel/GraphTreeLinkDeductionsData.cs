using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Globalization;
using System.Linq;
using System.Text;
using FLS.CommonComponents.Lib.Exceptions;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Helpers.Serialization;

namespace Luxoft.NDS2.Client.NodeTreePanel
{  
    public class GraphTreeLinkDeductionsData
    {
        private static readonly SequentStringParser s_stringParser = 
            new SequentStringParser( recordDelimiters: new[] { "^" }, recordItemDelimiters: new [] { "~" } );

        private string _periodTextFormatted = null;

        public string PeriodTextFormatted
        {
            get
            {
                if ( _periodTextFormatted == null && PeriodOrYear != null )
                    _periodTextFormatted = PeriodOrYear.ToString();

                return _periodTextFormatted;
            }
        }

        public string DeductionSumStringFormatted { get; set; }

        public bool OpenNewTreeVisible { get { return PeriodOrYear != null && PeriodOrYear.Code == 0; } }

        public GraphTreeLinkData Link { get; set; }

        public PeriodOrYear PeriodOrYear { get; set; }

        public GraphTreeLinkDeductionsData( GraphTreeLinkData link, DeductionDetail details )
        {
            if ( details.PeriodOrYear != null )
                PeriodOrYear = details.PeriodOrYear;
            DeductionSumStringFormatted = NumericFormatHelper.ToTwoDigitAfterDot( details.DecuctionAmount );

            Link = link;
        }

        public static IEnumerable<GraphTreeLinkDeductionsData> NewsFromDetails( GraphTreeLinkData link, IEnumerable<DeductionDetail> deductionDetails )
        {
            return deductionDetails.Select( x => new GraphTreeLinkDeductionsData( link, x ) );
        }

        public static IEnumerable<DeductionDetail> ConvertSpecificationToDeductionDetails( string deductionSpec )
        {
            //apopov 27.6.2016	//TODO!!!   //remove when real deduction details will be come
//#if DEBUG

//            return GenerateDeductionDetails().ToReadOnly();

//#else //!DEBUG

            if ( deductionSpec != null )
            {
                IEnumerable<string[]> recordsFields = s_stringParser.CreateRecordsIterator( deductionSpec );

                foreach ( string[] fields in recordsFields )
                {
                    Contract.Assume( fields != null && fields.Length == 3 );

                    DeductionDetail contractorSpecification = null;
                    try
                    {
                        int year                    = int.Parse( fields[0] );
                        int quarterNumber           = int.Parse( fields[1] );
                        if ( year > 0 || quarterNumber > 00 )   //ignores records for 'all periods' of deduction details
                            contractorSpecification = new DeductionDetail
                                { PeriodOrYear      = new PeriodOrYear( quarterNumber, year ),
                                  DecuctionAmount   = decimal.Parse( fields[2], CultureInfo.InvariantCulture ) };
                    }
                    catch ( Exception exc )
                    {
                        var sbFields = new StringBuilder( 32 );
                        if ( fields.Length > 0 )
                        {
                            sbFields.Append( "'" ).Append( fields[0] );
                            if ( fields.Length > 1 )
                            {
                                sbFields.Append( "|" ).Append( fields[1] );
                                if ( fields.Length > 2 )
                                    sbFields.Append( "|" ).Append( fields[2] );
                            }
                            sbFields.Append( "'" );
                        }
                        var agrs = new ParsingExceptionArgs
                            { FailedRecord = sbFields.ToString(), Source = "[Not outputed]" };  //does not log full parsing data string by security reason

                        throw new GenericException<ParsingExceptionArgs>( agrs, "Parsing error of deduction details", exc );
                    }
                    if ( contractorSpecification != null )
                        yield return contractorSpecification;
                }
            }
//#endif //DEBUG
        }

        //apopov 27.6.2016	//TODO!!!   //remove when real deduction details will be come
#if DEBUG

        private static IEnumerable<DeductionDetail> GenerateDeductionDetails()
        {
            var r = new Random();
            var x = r.Next(100);

            if ( x < 30 )
            {
                return GenerateDeductionDetails( 2015, 2 );
            }

            if ( x < 60 )
            {
                return new ConcurrentBag<DeductionDetail>();
            }

            return GenerateDeductionDetails( 2016, 3 );
        }

        private static IEnumerable<DeductionDetail> GenerateDeductionDetails( int year, int quarter )
        {
            var r = new Random();
            IList<DeductionDetail> details = new List<DeductionDetail>();

            Func<int, int?, DeductionDetail> generateDetail = ( int y, int? q) =>
                {
                    PeriodOrYear periodOrYear;
                    if ( q == null )
                    {
                        periodOrYear = new PeriodOrYear( y );
                    }
                    else
                    {
                        periodOrYear = new PeriodOrYear( q.Value, y );
                    }

                    return new DeductionDetail
                    {
                        DecuctionAmount = r.Next(500, 10000) * 1000,
                        PeriodOrYear = periodOrYear
                    };
                };

            for ( int i = year; i > 2013; i-- )
            {
                if ( i == 2014 )
                {
                    details.Add( generateDetail( i, null ) );
                }
                else
                {
                    for ( int j = quarter; j > 0; j-- )
                    {
                        details.Add( generateDetail( i, j ) );
                    }
                }
            }
            return details.Reverse();
        }

#endif  //DEBUG
    }
}