﻿using System;

namespace Luxoft.NDS2.Client.NodeTreePanel
{
    public sealed class GraphTreeMessageEventArgs : EventArgs
    {
        public string Message { get; private set; }

        public GraphTreeMessageEventArgs( string message )
        {
            Message = message;
        }
    }
}