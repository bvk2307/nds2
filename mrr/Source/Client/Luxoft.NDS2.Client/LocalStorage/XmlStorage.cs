﻿using Luxoft.NDS2.Client.Extensions;
using System;

namespace Luxoft.NDS2.Client.LocalStorage
{
    public class ErrorEventArgs : EventArgs
    {
        public ErrorEventArgs(Exception error)
        {
            Error = error;
        }

        public Exception Error
        {
            get;
            private set;
        }
    }

    /// <summary>
    /// Этот класс реализует абстрактный менеджер настроек грида
    /// </summary>
    public abstract class XmlStorage<T>
    {
        private readonly IStorage _storage;

        private readonly string _key;

        protected XmlStorage(IStorage storage, string key)
        {
            _storage = storage;
            _key = key;
        }

        public void Restore()
        {
            try
            {
                var state = _storage.Load(_key);
                SetState(state.Deserialize<T>());
            }
            catch (Exception error)
            {
                if (OnError != null)
                {
                    OnError(this, new ErrorEventArgs(error));
                }
            }
        }

        public void Save()
        {
            try
            {
                var state = GetState();
                _storage.Save(_key, state.Serialize());
            }
            catch(Exception error)
            {
                if (OnError != null)
                {
                    OnError(this, new ErrorEventArgs(error));
                }
            }
        }

        protected abstract T GetState();

        protected abstract void SetState(T state);

        public event EventHandler<ErrorEventArgs> OnError;
    }
}
