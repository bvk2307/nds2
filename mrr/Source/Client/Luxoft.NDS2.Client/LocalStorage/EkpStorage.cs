﻿using CommonComponents.Uc.Infrastructure.Interface.Services;
using Luxoft.NDS2.Client.Helpers;
using Microsoft.Practices.CompositeUI;
using System;

namespace Luxoft.NDS2.Client.LocalStorage
{
    /// <summary>
    /// Этот класс реализует хранилище IStorage локально на клиенте средствами ЕКП
    /// </summary>
    public class LocalStorage : IStorage
    {
        private const string valueName = "localstorage";

        private readonly IUcOptionsService _service;      

        public LocalStorage(WorkItem workItem)
        {
            if (workItem == null)
            {
                throw new ArgumentNullException("workItem");
            }

            _service = workItem.Services.Get<IUcOptionsService>(true);
        }

        public string Load(string key)
        {
            return _service.Load(key, valueName);
        }

        public void Save(string key, string data)
        {
            _service.Save(key, valueName, data);
        }
    }
}
