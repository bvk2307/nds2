﻿namespace Luxoft.NDS2.Client
{
    public class ResourceManagerNDS2
    {
        internal static class CommonStrings
        {
            public const string Hyperlink = "<a href=\"#\">{0}</a>";
        }
        
        internal class UiFormats
        {
            public const string SHORT_DATE = "dd.MM.yyyy";
        }

        internal class UserControlMessages
        {
            public const string SearchStringHint = "Для сокращения списка записей введите код или наименование";
        }

        internal static class InvoiceMessages
        {
            public const string CustomDeclarationNumbersDialogTitle = "Список номеров таможенных деклараций";
        }

        internal static class DeclarationMessages
        {
            public const string ERROR_OPENING_DECLARATION_CARD = "Ошибка открытия Декларации/Журнала";
            public const string DECLARATION_NOT_LOADED_IN_MS = "НД по НДС не загружена в АСК НДС-2. Повторите операцию позже.";
            public const string JOURNAL_NOT_LOADED_IN_MS = "Журнал не загружен в АСК НДС-2. Повторите операцию позже.";

            public const string CONTRAGENT_DECLARATION_NOT_LOADED =
                "Декларация не загружена в АСК НДС-2. Переход на список контрагентов невозможен. Повторите операцию позже.";
            public const string CONTRAGENT_JOURNAL_NOT_LOADED =
                "Журнал не загружен в АСК НДС-2. Переход на список контрагентов невозможен. Повторите операцию позже.";
            public const string CONTRAGENT_DECLARATION_NOT_PROCESSED =
                "Декларация не обработана. Переход на саписок контрагентов не возможен. Повторите операция позже";
            public const string CONTRAGENT_JOURNAL_NOT_PROCESSED =
                "Журнал не обработан. Переход на саписок контрагентов не возможен. Повторите операция позже";
            public const string UNABLE_TO_OPEN_RELATION_TREE_NO_DATA  = "Декларация не загружена в АСК НДС-2.  Дерево связей построить не возможно. Повторите операцию позже.";
            public const string UNABLE_TO_OPEN_RELATION_TREE_NO_READY = "Декларация не обработана. Дерево связей построить не возможно. Повторите операцию позже.";
            public const string OPEN_RELATION_TREE_PARTIAL_NO_DATA    = "Декларация по текущей корректировке не загружена в АСК НДС-2. Дерево связей отображается по предыдущей корректировке.";
            public const string OPEN_RELATION_TREE_PARTIAL_NO_READY   = "Декларация по текущей корректировке не обработана. Дерево связей отображается по предыдущей корректировке.";

            public const string DECLARATION_NOT_PROCESSED = "НД по НДС не обработана";
            public const string JOURNAL_NOT_PROCESSED = "Журнал учета не обработан";
            public const string DECLARATION_PROCESSED_PREVIOUS = "Данные на {0}. Актуальная корректировка еще не обработана";
            public const string JOURNAL_PROCESSED_PREVIOUS = "Данные на {0}. Актуальная версия еще не обработана";
            public const string DECLARATION_OVERFLOW_JUMP_BETWEEN_CARD = "Вы выполнили 3 перехода между декларациями, дальнейшие переходы невозможны";
            public const string DECLARATION_INOICE_CONTRAGENT_NOT_MAPPED = "СФ контрагента не сопоставлена, переход к разделу невозможен.";
            public const string CONTRACTOR_DECLARATION_RELOCATION_WARNING = "Запись не сопоставлена. Переход в декларацию/журнал контрагента.";
            public const string DECLARATION_NOT_FOUND_PATTERN = "Запись не сопоставлена. Контрагент не подал декларацию/журнал за {0}";
            public const string DECLARATION_DETAILS_ACCESS_DENIED = "Недостаточно прав на просмотр карточки декларации";
            public const string DECLARATION_OPEN_ERROR_ZIP = "Возникла ошибка открытия декларации ZIP = {0}: {1}";

            public const string InvoiceLoadingFailed = "По техническим причинам загрузка записей по СФ невозможна. Повторите операцию позже.";

            public const string HaveToDecreaseNumber =
                "Необходимо уменьшить объем выгружаемых записей до {0:N0}. Измените условия фильтрации";

            public const string ExportingFailed =
                "Произошел сбой при формировании файла {0}. Повторите операцию снова. \"{1}\"";

            public const string ExportConfigurationFailed = "Ошибка чтения конфигурации экпорта в эксель: {0}";

            public const string HeavyLoad =
                "Раздел {0} содержит более {1} записей.\nЗагрузка может занять некоторое время.";

            public const string NoInvoiceRecords = "Записи о счетах-фактурах в разделе отсутcтвуют";

            public const string RecordNotCompared = "Запись не сопоставлена";

            public const string DataLoadingFailed = "Ошибка загрузки данных.";

            public const string ContragentChapterNoData = "У налогоплательщика отсутствуют открытые расхождения для КНП";

            public const string UserOperationsLoadFailed = "Не удалось загрузить список операций пользователя";

        }

        internal static class InspectorDeclarations
        {
            public static string CHANGES_AT = "Сформировано автотребование по СФ";
            public static string CHANGES_SLIDE_AT = "НП не направлено автотребование по СФ";
            public static string CHANGES_NO_TKS = "Необходимо ввести пояснение";
        }

        internal static class MacroReportMessages
        {
            public const string ERROR_GET_MAP_DATA = "Ошибка дрлучения данных для карты";
            public const string ERROR_GET_TIME_SEQUENCE_DATA = "Ошибка получения данных для временного граффика";
            public const string ERROR_GET_NP_DATA = "Ошибка получения данных по налогоплательщикам";
			public const string WARNING_NO_DATABY_QUOTER = "Данные за выбранный регион и квартал отсутствуют. Выберите другой регион или период.";
        }

        #region Captions

        public const string Information = "Информация";
        public const string Warning = "Предупреждение";
        public const string Error = "Ошибка";

        #endregion

        internal static class DeclarationStrings
        {
            public const string Legal = "Законный представитель";
            public const string Authorised = "Уполномоченный представитель";
        }

        internal static class ExportExcelMessages
        {
            public const string Caption = "Экспорт в MS Excel";
            public const string CancelQuery = "Файл \"{0}\" не сформирован. Прервать процесс формирования?";
            public const string OpenQuery = "Файл \"{0}\" сформирован. Открыть файл?";
            public const string MSExcelAppName = "MS Excel";
            
            public const string Requested = "Запрос на формирование отправлен";
            public const string Exported = "Выгружено строк: {0}";
            public const string Failed = "Произошел сбой при формировании файла \"{0}\". Повторите операцию снова.";
            public const string Finished = "Экспорт завершен";

            public const string SaveDialogTitle = "Укажите имя сохраняемого файла";
            public const string ExcelExportDialogTitle = "Экспорт в " + MSExcelAppName;

            public const string DoFileExportCancel = "Прекратить выгрузку списка?";

            public const string FileDownloadProgress = "Получено: {0}, удачных фрагментов: {1}, неудачных попыток: {2}";
            public const string FileDownloadFailed = "Не удалось получить файл";
            public const string FileOpenFailed = "Ошибка открытия файла {0}";
            public const string FileFormedIsItOpen = "Файл «{0}» сформирован. Открыть файл?";
            public const string FileFormedError = "Произошел сбой при формировании файла «{0}». Повторите операцию снова.";
            public const string FileExcelOpenError = "Ошибка открытия Excel файла отчета: '{0}'";
        }

        internal static class SelectionCard
        {
            public const string SelectionFilterError =
                "Ошибка отображении фильтра выборки ({0}). Обратитесь в службу поддержки";
            public const string RegionsLoadingFault = "не удалось загрузить информацию о регионах";
            public const string InspectionsLoadingFault = "не удалось загрузить информацию об инспекциях";
            public const string MetadataLoadingFault = "не удалось загрузить метаданные параметров фильтрации";
            public const string ExcelExportFileName = "Расхождения_{0:yyyyMMdd_HH-mm}";

            public const string ViewSelectionTitle = "Просмотр выборки {0}";
            public const string ViewTemplateTitle = "Просмотр шаблона {0}";
            public const string ViewSelectionByTemplateTitle = "Просмотр выборки {0}";
            public const string EditSelectionTitle = "Редактирование выборки {0}";
            public const string EditTemplateTitle = "Редактирование шаблона {0}";
            public const string EditSelectionByTemplateTitle = "Редактирование выборки {0}";
            public const string ApproveSelectionTitle = "Согласование выборки {0}";
            public const string ApproveSelectionByTemplateTitle = "Согласование выборки {0}";
            public const string NewSelectionTitle = "Создание выборки";
            public const string NewTemplateTitle = "Создание шаблона";
            public const string CopyTemplateTitle = "Создание шаблона";
        }

        #region Список АТ по СФ

        internal static class InvoiceClaimList
        {
            public const string ReportCaption = "Отчёт";

            public const string DefaultSelectionStatsFileName = "Статистика по выборкам_{0:yyyyMMdd}.xlsx";

            public const string DefaultInvoiceClaimListFileName = "Перечень АТ от {0:yyyyMMdd}-{1:yyyyMMdd_HH-mm}.xlsx";

            public const string DefaultTaxMonitoringFileName = "АТ по СФ от {0:yyyyMMdd}_{1}_{2}.xlsx";

            public const string ServiceError = "Ошибка загрузки данных ({0}). Обратитесь в службу поддержки";
            
            public const string CreateDatesLoadingFault = "Не удалось загрузить даты создания АТ по СФ";
            
            public const string StatusListLoadingFault = "Не удалось загрузить список статусов АТ по СФ";
            
            public const string SendStatusLoadingFault = "Не удалось загрузить статус отправки на дату";

            public const string ConfirmSending = "{0} автотребований по СФ готово к передаче в СЭОД.";

            public const string SelectionStatsDataLoadingFault = "Не удалось загрузить данные для построения отчета";

            public const string GetClaimsCountError = "Не удалось получить кол-во АТ для отправки";
        }

        internal static class ClaimCard
        {
            public const string SendToSeod = "Отправлено в СЭОД:";

            public const string ReceivedBySeod = "Получено СЭОД:";

            public const string SentToTaxpayer = "Отправлено налогоплательщику:";

            public const string CalcDeliveryDate = "Расчётная дата вручения:";
            
            public const string DeliveryDate = "Вручено налогоплательщику:";
            
            public const string CalcAnswerDate = "Дата ожидания ответа:";

            public const string Closed = "Закрыто:";

            public const string CalcDateFault = "Ошибка расчета даты";

            public const string NoneKnpDiscrepancies = "Открытие Карточки расхождения не возможно. Отсутствуют расхождения для КНП";
        }

        #endregion

        #region Карточка НП

        internal static class TaxPayerCard
        {
            public const string RequestBankAccountsAvailable = "Сформировать запрос по операциям в банк";

            public const string RequestBankAccountsUnavailable = "Отсутствуют основания для формирования запроса";

            public const string BankOperationsAvailable = "Просмотреть операции по счету";

            public const string BankOperationsUnavailable = "Отсутствуют сведения по операциям по счету";

            public const string RequestAlreadyExists =
                "Ранее по выбранному отчетному периоду был сформирован запрос. Продолжить?";

            public const string AccountsAlreadyRequested =
                "За данный период, по выбранным счетам ранее был отправлен запрос в банк. Измените параметры запроса";

            public const string OperationWindowFailed = "Возникла ошбка при вызове окна просмотра операций:\n{0}";
        }

        #endregion

        public const string NDS2ClientResources = "RecourceManager: Luxoft.NDS2.Client";

        public const string Attention = "Внимание!";
        public const string LoadingIsComplite = "Загрузка завершена";

        public const string SelectionSaveSuccess = "Выборка сохранена успешно";
        public const string SelectionUnknownSatus = "Неизвестный статус выборки - {0}";
        public const string SelectionRequestNotFound = "Запрос для выборк {0} не найден";

        public const string LoadError = "Ошибка загрузки - '{0}'";

        public const string ParamFiltrationResetConfirm = "Параметры фильтрации будут сброшены. Продолжить?";
        public const string ParamFiltrationAndResultResetConfirm = "Параметры фильтрации и результат фильтрации будут сброшены. Продолжить?";

        public const string SelectionCloseWithoutFiltersSave = "Фильтр не был применён. При закрытии выборка не будет создана. Продолжить?";
        public const string TemplateCloseWithoutFiltersSave = "Фильтр не был применён. При закрытии шаблона выборки не будет созданы. Продолжить?";
        public const string SelectionCloseWithoutNameSave = "Название выборки было изменено. При закрытии карточки выборки изменение не будет сохранено. Продолжить?";
        public const string SelectionCloseWithoutEditableDataSave = "Выборка была изменена. При закрытии карточки выборки изменения не будет сохранены. Продолжить?";
        public const string SelectionSendSEODConfirm = "Сформировать автотребования в СЭОД?";
        public const string SelectionDeleteConfirmMessage = "Выборка {0} будет удалена. Продолжить?";
        public const string TemplateSelectionDeleteConfirmMessage = "Шаблон {0} и входящие в него выборки будут удалены. Продолжить?";
        public const string RelationTreeTaxPayerNoData = "В системе отсутствует информация об операциях данного налогоплательщика, удовлетворяющая заданным значениям параметров дерева связей.";
        public const string TemplateSelectionCannotBeDeletedMessage = "Удаление шаблона невозможно";
        public const string HAS_NOT_RIGHTS_TO_THIS_OPERATION = "У Вас отсутствуют права на данную функцию";
        public const string RenameSelectionEmptyName = "Необходимо ввести название";
        public const string RenameSelectionAlreadyExist = "Выборка с таким названием уже существует";
        public const string CreateClaimConfirmMessage = "{0} выборок согласованы. {1} Продолжить формирование АТ?";
        public const string RequestApproveCountMessage = "{0} выборок находятся на согласовании.";
        public const string NotAllSelectionsCanBeApproved = "Выборки {0} не содержат деклараций и не будут согласованы. Продолжить?";


        #region Названия форм

        public const string SettingsMRRLoadingInspection = "Параметры нагрузки на инспекции";



        # endregion

        # region Карточка выборки. Работа с избранными фильтрами

        public const string FavoritSelectionFilterOverwirteConfirm = "Фильтр с данным названием существует. Перезаписать?";
        public const string FavoriteSelectionFilterDeleteConfirm = "Фильтр {0} будет удален. Продолжить?";
        public const string AddFavoriteSelectionFilterTitle = "Cохранить фильтр";
        public const string ManageFavoriteSelectionFilterTitle = "Загрузить или удалить фильтр";

        # endregion

        public const string SelectionSaveText = "Сохранить выборку";
        public const string Comments = "Комментарий";
        public const string Execute = "Выполнить";

        public const string Save = "Сохранить";
        public const string Close = "Закрыть";
        public const string Refresh = "Обновить";
        public const string Delete = "Удалить";

        public const string SelectionCreateTitle = "Создать";
        public const string SelectionCreateDescription = "Создать выборку";
        public const string SelectionViewTitle = "Открыть";
        public const string SelectionViewDescription = "Открыть карточку выборки";
        public const string SelectionEditTitle = "Редактировать";
        public const string SelectionEditDescription = "Редактировать выборку";
        public const string SelectionDeleteTitle = "Удалить";
        public const string SelectionDeleteDescription = "Удалить выборку";
        public const string SelectionToApproveTitle = "На согласование";
        public const string SelectionToApproveDescription = "Отправить выборку на согласование";
        public const string SelectionToCorrectTitle = "На доработку";
        public const string SelectionToCorrectDescription = "Отправить выборку на доработку";
        public const string SelectionApprovedTitle = "Согласовать";
        public const string SelectionApprovedDescription = "Согласовать выборку";
        public const string SelectionSendToSeodTitle = "Отправить";
        public const string SelectionSendToSeodDescription = "Отправить выборку налогоплательщику";
        public const string SelectionFilterApplyTitle = "Применить фильтр";
        public const string SelectionFilterApplyDescription = "Применить фильтр";
        public const string SelectionFilterResetTitle = "Очистить фильтр";
        public const string SelectionFilterResetDescription = "Очистить условия и результат фильтрации";
        public const string SelectionFilterFavoritesAddTitle = "Сохранить фильтр";
        public const string SelectionFilterFavoritesAddDescription = "Сохранить текущий фильтр";
        public const string SelectionFilterFavoritesTitle = "Загрузить фильтр";
        public const string SelectionFilterFavoritesDescription = "Задействовать или удалить ранее сохраненный фильтр";
        public const string SelectionSaveTitle = "Сохранить";
        public const string SelectionSaveDescription = "Сохранить условия и результат фильтрации";
        public const string SelectionSaveAndCloseTitle = "Сохранить и закрыть";
        public const string SelectionSaveAndCloseDescription = "Сохранить условия и результат фильтрации и закрыть карточку выборки";
        public const string SelectionFilterRetParamTitle = "Вернуться к применённому фильтру";
        public const string SelectionFilterRetParamDescription = "Восстанавливаются параметры последнего применённого фильтра (результаты фильтрации не обновляются)";
        public const string SelectionUpdateTitle = "Обновить";
        public const string SelectionUpdateDescription = "Обновить карточку выборки";
        public const string SelectionCloseTitle = "Закрыть";
        public const string SelectionCloseDescription = "Закрыть карточку выборки";
        public const string SelectionRenameTitle = "Переименовать";
        public const string SelectionRenameDescription = "Переименовать выборку";
        public const string SelectionTakeInWorkTitle = "Взять в работу";
        public const string SelectionTakeInWorkDescription = "Взять выборку по шаблону в работу";

        public const string SelectionFilterCreateTitle = "Создать";
        public const string SelectionFilterCreateDescription = "Создать фильтр выборки";
        public const string SelectionTemplateCreateTitle = "Создать шаблон";
        public const string SelectionTemplateCreateDescription = "Создать фильтр шаблона выборки";
        public const string SelectionTemplateCopyTitle = "Копировать шаблон";
        public const string SelectionTemplateCopyDescription = "Копировать фильтр шаблона";
        public const string SelectionCreateClaimTitle = "Сформировать АТ";
        public const string SelectionCreateClaimDescription = "Сформировать автотребования";


        public const string SelectionSendToSeodAllErrorMessagePattern = "Не удалось отправить выборки\r\n{0}";
        public const string SelectionSendToSeodMultipleErrorMessagePattern = "Часть выборок не было отправлено\r\n{0}";
        public const string SelectionSendToSeodNoDiscrepancyMessagePattern = "В выборке «{0}» отсутствуют записи со статусом «Согласовано».";
        public const string SelectionSendToSeodUnexpectedErrorMessagePattern = "Не удалось отправить выборку «{0}» из-за неожиданной ошибки - {1}.";

        public const string DeclarationChapterErrorMessageTemplate = "<span style='color:red'>Ошибка загрузки раздела ({0})</span>";

        public const string NDSTemplateFilePath = "Bin/Templates/{0}/NDS_Template.xml";
        public const string JRNLTemplateFilePath = "Bin/Templates/{0}/JRNL_Template.xml";


        public const string DocCorrectionName = "Корректировка";


        #region Report headers

        public const string InspectorMonitoringWork = "Мониторинг работы налоговых инспекторов";
        public const string LoadingInspection = "Отчет о загруженности инспекций";
        public const string MonitorProcessDeclaration = "Мониторинг обработки налоговых деклараций";
        public const string CheckControlRatio = "Отчет по проверкам КС";
        public const string MatchingRule = "Отчет по правилам сопоставления";
        public const string CheckLogicControl = "Отчет по проверкам ЛК";
        public const string DeclarationStatistic = "Фоновые показатели (По поданным декларациям) ";
        public const string DynamicTechnoParameter = "Отчет по динамике изменения технологических параметров";
        public const string DynamicDeclarationStatistic = "Отчет по динамике изменения фоновых показателей";
        public const string InfoResultsOfMatching = "Информация о результатах сопоставлений";

        public const string ActsAndDecisions = "Отчет по Актам и Решениям";

        #endregion


        public const int RequestsTimeOutMillisec = 1000;

        # region Сообщения СОВ

        public const string SovNotSuccessInNightModeMessage = "Данные по налоговым декларациям, журналам учёта и по расхождениям недоступны, система работает в расчётном режиме. Данные будут доступны после завершения расчёта";
        public const string SovErrorInDeclaration = "Ошибка передачи данных (ошибка сервиса взаимодействия). Обратитесь в службу поддержки.";
        public const string SovError = "Ошибка передачи данных (ошибка сервиса взаимодействия). Обратитесь в службу поддержки.";
        public const string SovNotRegistered = "Ошибка регистрации запроса";

        #endregion

        # region Сообщения НП

        public const string TAXPAYER_NOT_FOUND = "Налогоплательщик не загружен в АСК НДС-2. Повторите операцию позже.";

        #endregion

        #region Graph Tree
        public const string ReloadButtonToolTip = "Построить дерево связей";
        public const string ExcelReportAllButtonText = "Выгрузить всё";
        public const string ExcelReportAllButtonToolTip = "Экспортировать все узлы в файл формата MS Excel"; 
        public const string ExcelReportVisibleButtonText = "Выгрузить открытые узлы";
        public const string ExcelReportVisibleButtonToolTip = "Экспортировать открытые узлы в файл формата MS Excel";
        public const string CancelExcelReportButtonText = "Отменить выгрузку";
        public const string CancelExcelReportButtonToolTip = "Отменить выгрузку в MS Excel";

        public const string NotEnoughRightsToAccessGraphTree = "Недостаточно прав для доступа к Дереву Связей";
        #endregion

        #region Настройка списков

        public const string CreateWhiteListTitle = "Создать БС";
        public const string CreateWhiteListTooltip = "Создать БС";

        public const string UpdateWhiteListsTitle = "Обновить";
        public const string UpdateWhiteListsTooltip = "Обновить";

        public const string ExcludeListTitle = "Список ИВ";
        public const string ExcludeListListTooltip = "Список ИВ";

        public const string TaxMonitorListTitle = "Список НМ";
        public const string TaxMonitorListListTooltip = "Список НМ";

        public const string BackButtonTitle = "Назад";
        public const string BackButtonTooltip = "Вернуться в список настроек";

        public const string SaveListTitle = "Сохранить список";
        public const string SaveListTooltip = "Сохранить список";

        public const string WhiteListSaveError = "Некорректно заполнен белый список. Необходимо укать ИНН и название";
        public const string SettingsListSaveError = "Неверно указан разделитель или нарушен формат ИНН. В качестве разделителя может выступать или пробел или запятая. Формат ИНН - 10 или 12 цифр.";

        public const string WhiteListActivated = "Cписок активирован";
        public const string WhiteListDeactivated = "Список деактивирован";
        #endregion

        internal static class UserSettingsKeys
        {
            public const string DeclarationCommonInfoExpandState = "Декларация.Карточка.ОбщаяИнформация.Состояние";
        }

        internal static class ExplainMessages
        {
            public const string ChangedRecord = "Измененная запись";

            public const string RejectedRecord = "Отказано в применении пояснения";

            public const string ConfirmedRecord = "Подтвержденная запись";

            public const string FormalExplainTitle = "{0} (Формализованное пояснение)";

            public const string ExplainOtherReasonTitle = "{0} (Пояснения по Иным Основаниям)";

            public const string ExplainControlRationTitle = "{0} (Пояснение по КС)";
        }

        internal static class GridView
        {
            public const string NoFilters = "Без фильтров";
            public const string EnterValue = "Введите значение";
            public const string Condition = "Условие";
            public const string Value = "Значение";
            public const string January = "Январи";
            public const string February = "Февраль";
            public const string March = "Март";
            public const string April = "Апрель";
            public const string May = "Май";
            public const string June = "Июнь";
            public const string July = "Июль";
            public const string August = "Август";
            public const string September = "Сентябрь";
            public const string October = "Октябрь";
            public const string November = "Ноябрь";
            public const string December = "Декабрь";
            public const string NextMonth = "Следующий месяц";
            public const string NextWeek = "Следующая неделя";
            public const string NextYear = "Следующий год";
            public const string NextQuarter = "Следующий квартал";
            public const string LastMonth = "Последний месяц";
            public const string LastWeek = "Последняя неделя";
            public const string LastYear = "Последний год";
            public const string LastQuarter = "Последний квартал";
            public const string ThisMonth = "Текущий месяц";
            public const string ThisWeek = "Текущий неделя";
            public const string ThisYear = "Текущий год";
            public const string ThisQuarter = "Текущий квартал";
            public const string Quarter1 = "Квартал 1";
            public const string Quarter2 = "Квартал 2";
            public const string Quarter3 = "Квартал 3";
            public const string Quarter4 = "Квартал 4";
            public const string Today = "Сегодня";
            public const string Tomorrow = "Завтра";
            public const string Yesterday = "Вчера";
            public const string YearToDate = "Год к дате";
        }

        internal static class FilterUiProvider
        {
            public const string ClearFilter = "Очистить";
            public const string TextFilters = "Пользовательский фильтр";
            public const string DateFilters = "Пользовательский фильтр";
            public const string NumberFilters = "Пользовательский фильтр";
            public const string OkButton = "Принять";
            public const string CancelButton = "Отмена";
            public const string EqualsOperand = "Равно";
            public const string DoesNotEqualOperand = "Не равно";
            public const string BeginsWithOperand = "Начинается с";
            public const string EndsWithOperand = "Заканчивается на";
            public const string ContainsOperand = "Содержит";
            public const string GreaterThanOperand = "Больше";
            public const string GreaterThanOrEqualToOperand = "Больше или равно";
            public const string LessThanOperand = "Меньше";
            public const string LessThanOrEqualToOperand = "Меньше или равно";
            public const string BeforeOperand = "До";
            public const string AfterOperand = "После";
            public const string BetweenOperand = "Между";
            public const string AllDatesInPeriod = "Все даты за период";
            public const string CustomFilter = "Расширенный";

        }

        internal static class Integration
        {
            public const string ExternalCallFailed = "Ошибка вызова активности в подсистеме '{0}' по адресу '{1}'.";

            public const string ExternalSystemError =
                "Ошибка вызова в удаленной подсистеме {0}. Подсистема найдена, но не найдена опубликованная активность '{1}'";

            public const string SubsystemNotFound = "Ошибка вызова в удаленной подсистеме '{0}'. Подсистема не найдена";
        }

        internal static class ComparasionProcessMessages
        {
            public const string InProcess = "Выполняется сопоставление данных. Повторите операцию позже.";
            public const string Changed = "Необходимо обновить данные. Нажмите на кнопку «Обновить».";
        }

        internal static class KnpResultDocumentStrings
        {
            public const string ActDiscrepancyExcludeMenuText = "Удалить из акта";
            public const string DecisionDiscrepancyExcludeMenuText = "Удалить из решения";

            public const string ActAmountEditErrorMessage =
                "Некорректно указана сумма по акту. Сумма должна быть меньше или равна сумме расхождения.";

            public const string DecisionAmountEditErrorMessage =
                "Некорректно указана сумма по решению. Сумма должна быть меньше или равна сумме по акту.";

            public const string SaveDecisionMsgCaption = "Сохранение данных по решению";

            public const string SaveDecisionMsgText =
                "При сохранении данных редактирование будет недоступно, продолжить?";


            public const string AmountZeroErrorMessageText = "Сумма по акту должна быть больше 0";
        }

        internal static class UserTaskReport
        {
            public const string NoData = "Данные на {0:dd.MM.yyyy} отсутствуют";
            public const string ErrorUserTask = "Ошибка при попытке получить данные, обратитесь в службу поддержки";
            public const string DeniedUserTask = "Недостаточно прав для чтения данных";

            public const string NoAggregate = "Ошибка при построении отчета на {0:dd.MM.yyyy}, обратитесь в службу поддержки";
        }

        internal static class ActsAndDecisionsMessages
        {
            public const string ExcelExportFault = "Ошибка экспорта в Excel: {0}";

            public const string DefaultExcelFileName = "Акты и Решения_{0:yyyyMMdd_HH-mm}";
        }
        
    }
}
