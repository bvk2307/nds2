﻿using Luxoft.NDS2.Common.Helpers.Executors;

namespace Luxoft.NDS2.Client.Infrastructure
{
    /// <summary> A factory of <see cref="ICallExecutor"/> and a dispatcher of service calls in a process. </summary>
    public interface IServiceCallExecutorManager : ICallExecutorManager
    {
    }
}