﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonComponents.Shared;
using CommonComponents.Uc.Infrastructure.Interface.Constants;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Providers;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using Microsoft.Practices.CompositeUI;
using Microsoft.Practices.CompositeUI.Services;

namespace Luxoft.NDS2.Client.Infrastructure
{
    public class SubsystemInstanceProvider : ISubsystemInstanceProvider
    {
        private readonly SubsystemInstanceInfoContext _instanceInfo;
        private SubsystemFavoriteTreeProvider _favoriteTreeProvider;
        private bool _isConnected;
        private SubsystemMetadataProvider _metadataProvider;
        private SubsystemNavigationNodesProvider _navigationNodesProvider;
        private WorkItem _subsystemWorkItem;

        public SubsystemInstanceProvider(SubsystemInstanceInfoContext instanceInfo)
        {
            this._instanceInfo = instanceInfo;
        }

        #region Implementation of ISubsystemInstanceProvider

        public void Initialize(WorkItem subsystemWorkItem)
        {
            this._subsystemWorkItem = subsystemWorkItem;

            this._navigationNodesProvider = new SubsystemNavigationNodesProvider(this._subsystemWorkItem);
            this._favoriteTreeProvider = new SubsystemFavoriteTreeProvider();
            this._metadataProvider = new SubsystemMetadataProvider(this._navigationNodesProvider);

        }

        public bool TryConnect(IDataContext loginContext, out IDataContext connectionContext)
        {
            connectionContext = new DataContext();
            this._isConnected = true;
            return this._isConnected;
        }

        public void Disconnect(IDataContext connectionContext)
        {
            this._isConnected = false;
        }

        public SubsystemUserInfoContext GetUserInfo(IDataContext connectionContext)
        {
            SubsystemUserInfoContext userInfo = new SubsystemUserInfoContext();

            return userInfo;
        }

        public bool TryGetSubsystemProvider<TProvider>(out TProvider provider) where TProvider : class
        {
            provider = null;

            if (typeof(TProvider) == typeof(ISubsystemNavigationNodesProvider))
            {
                provider = this._navigationNodesProvider as TProvider;
            }
            else if (typeof(TProvider) == typeof(ISubsystemFavoriteTreeProvider))
            {
                provider = this._favoriteTreeProvider as TProvider;
            }

            return provider != null;
        }

        public SubsystemInstanceInfoContext InstanceInfo { get { return _instanceInfo; } 
        }

        public Type RootWorkItemType
        {
            get { return null; }
        }

        public bool IsConnected
        {
            get { return this._isConnected; }
        }

        public ISubsystemNavigationNodesProvider TaskTreeProvider
        {
            get { return this._navigationNodesProvider; }
        }

        public ISubsystemFavoriteTreeProvider FavoriteTreeProvider
        {
            get { return this._favoriteTreeProvider; }
        }

        public ISubsystemMetadataProvider MetadataProvider
        {
            get { return this._metadataProvider; }
        }

        #endregion
    }
}
