﻿using System;
using System.Diagnostics.Contracts;
using Luxoft.NDS2.Common.Helpers.Executors;

namespace Luxoft.NDS2.Client.Infrastructure
{
    /// <summary> A factory for <see cref="IServiceCallExecutorManager"/>. </summary>
    public static class ServiceCallExecutorManagerFactory
    {
        private static Lazy<ServiceCallExecutorManager> s_lzExecutor = new Lazy<ServiceCallExecutorManager>( InitSingleton, isThreadSafe: true );

        /// <summary> Initialize the singleton executor manager. </summary>
        /// <returns></returns>
        private static ServiceCallExecutorManager InitSingleton()
        {
           return new ServiceCallExecutorManager();
        }

        /// <summary> Initialize the singleton executor manager. </summary>
        public static void InitIfNeeded()
        {
            if ( !s_lzExecutor.IsValueCreated )
                // ReSharper disable once ReturnValueOfPureMethodIsNotUsed
                object.ReferenceEquals( s_lzExecutor.Value, null ); //initiates the singleton instance only
        }

        /// <summary> The current executor manager. </summary>
        public static IServiceCallExecutorManager CurrentManager
        {
            get { return s_lzExecutor.IsValueCreated ? s_lzExecutor.Value : null; }
        }

        /// <summary> Maximum number of service call executors controled by <see cref="ServiceCallExecutorManager"/>. </summary>
        public static int MaxExecutorNumber { get { return 5; } }  //8; } }

        /// <summary> A service call executor manager. </summary>
        //private //apopov 13.7.2016	//TODO!!!   //replace when the singleton Executor Manager will not retain Presenter's by Call Dispatchers
        public
            class ServiceCallExecutorManager : CallExecutorManager, IServiceCallExecutorManager
        {
            public ServiceCallExecutorManager() : base( ServiceCallExecutorManagerFactory.MaxExecutorNumber )
            {
                Contract.Requires( ServiceCallExecutorManagerFactory.MaxExecutorNumber > 1 );
            }
        }
    }
}