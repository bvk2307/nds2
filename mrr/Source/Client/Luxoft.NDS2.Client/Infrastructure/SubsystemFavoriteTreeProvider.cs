﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Providers;

namespace Luxoft.NDS2.Client.Infrastructure
{
    public class SubsystemFavoriteTreeProvider : ISubsystemFavoriteTreeProvider
    {
        #region Implementation of ISubsystemFavoriteTreeProvider

        public void Add(NavigationNodeContext nodeContext)
        {
        }

        public void Remove(NavigationNodeContext nodeContext)
        {
        }

        public void Update(NavigationNodeContext nodeContext)
        {
        }

        public void SetNote(NavigationNodeFavoriteInfoContext favoriteContext, string note)
        {
        }

        #endregion
    }
}
