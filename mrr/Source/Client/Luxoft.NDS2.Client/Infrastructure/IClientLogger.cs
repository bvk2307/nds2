﻿using CommonComponents.Instrumentation;
using System;
using System.Runtime.CompilerServices;
using FLS.CommonComponents.Lib.Interfaces;

namespace Luxoft.NDS2.Client.Infrastructure
{
	//apopov 25.2.2016	//TODO!!!   merge and replace with Luxoft.NDS2.Common.Contracts.Services.ILogProvider

    /// <summary> A client logger wrapper for <see cref="IInstrumentationService"/>. </summary>
    public interface IClientLogger : IInstanceProvider<IInstrumentationService>
    {
        void LogError( Exception ex,
                       [CallerMemberName] 
                       string callerMemberName = null );

        void LogError( string message, Exception ex,
                       [CallerMemberName] 
                       string methodName = null );

        void LogWarning( string message,
                         [CallerMemberName] 
                         string callerMemberName = null );

        void LogInfo( string message,
                      [CallerMemberName] 
                      string callerMemberName = null );
    }
}