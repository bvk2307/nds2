﻿using System;
using System.Diagnostics.Contracts;
using System.Threading;
using System.Threading.Tasks;
using Luxoft.NDS2.Common.Helpers.Executors;

namespace Luxoft.NDS2.Client.Infrastructure
{
    /// <summary> A factory of <see cref="ServiceCallDispatcher{TParam,TInstanceKey,TContextKey, TData, TResult}"/>. </summary>
    public sealed partial class ServiceCallDispatcherFactory
    {
        /// <summary> A dispatcher of calls to different server services. </summary>
        /// <typeparam name="TParam"></typeparam>
        /// <typeparam name="TInstanceKey"></typeparam>
        /// <typeparam name="TContextKey"></typeparam>
        /// <typeparam name="TData"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        private sealed class ServiceCallDispatcher<TParam, TInstanceKey, TContextKey, TData, TResult> 
            : CallFlowDispatcher<CallExecContext<TParam, TInstanceKey, TContextKey, TData>,
                                 CallExecContext<TParam, TInstanceKey, TContextKey, TResult>>, 
            IServiceCallDispatcher<TParam, TInstanceKey, TContextKey, TData, TResult>
        {
            private readonly Func<object, CancellationToken, object> _executorHandler;
            private readonly 
                Func<CallExecContext<TParam, TInstanceKey, TContextKey, TData>, CallExecContext<TParam, TInstanceKey, TContextKey, TResult>> 
                    _dataTransofrmHandler;

            public ServiceCallDispatcher( 
                Func<TParam, CancellationToken, TData> serviceCallHandler, 
                IServiceCallExecutorManager сallExecutorManager, 
                Func<CallExecContext<TParam, TInstanceKey, TContextKey, TData>, CallExecContext<TParam, TInstanceKey, TContextKey, TResult>> 
                    dataTransofrmHandler,
                int inputBoundCapacity = 0, int lowInputBoundCapacity = 0, CancellationToken cancelToken = default(CancellationToken) )
                : base( сallExecutorManager, inputBoundCapacity, lowInputBoundCapacity, cancelToken )
            {
                Contract.Requires( serviceCallHandler != null );
                Contract.Requires( dataTransofrmHandler != null );
                Contract.Ensures( _executorHandler != null );

                _executorHandler        = ( parametersPar, cancelTokenPar ) => serviceCallHandler( (TParam)parametersPar, cancelTokenPar );
                _dataTransofrmHandler   = dataTransofrmHandler;
            }

            protected override Task<CallExecContext<TParam, TInstanceKey, TContextKey, TData>> 
                ExecuteCallAsync( ICallExecutor executor, CallExecContext<TParam, TInstanceKey, TContextKey, TData> context )
            {
                Task<CallExecContext<TParam, TInstanceKey, TContextKey, TData>> execute = 
                    executor.Process( _executorHandler, context.Parameters )
                        .ContinueWith(
                            taskPrev =>
                            {
                                context.Result = (TData)taskPrev.Result;

                                return context;
                            },
                            TaskContinuationOptions.ExecuteSynchronously );

                return execute;
            }

            public override CallExecContext<TParam, TInstanceKey, TContextKey, TResult> 
                Process( CallExecContext<TParam, TInstanceKey, TContextKey, TData> context )
            {
                CallExecContext<TParam, TInstanceKey, TContextKey, TResult> transformedContext = _dataTransofrmHandler( context );

                return transformedContext;
            }

            /// <summary> Executes all queued items and completes the mesh. </summary>
            public void Complete()
            {
                InputLow.Complete();
                Input.Complete();
            }
        }
    }
}