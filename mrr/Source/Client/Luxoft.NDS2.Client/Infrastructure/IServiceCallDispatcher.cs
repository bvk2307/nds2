﻿using Luxoft.NDS2.Common.Helpers.Executors;

namespace Luxoft.NDS2.Client.Infrastructure
{
    /// <summary> A dispatcher of calls to a some server service. </summary>
    /// <typeparam name="TParam"></typeparam>
    /// <typeparam name="TInstanceKey"></typeparam>
    /// <typeparam name="TContextKey"></typeparam>
    /// <typeparam name="TData"></typeparam>
    /// <typeparam name="TResult"></typeparam>
    public interface IServiceCallDispatcher<TParam, TInstanceKey, TContextKey, TData, TResult> 
        : ICallFlowDispatcher<CallExecContext<TParam, TInstanceKey, TContextKey, TData>, 
                              CallExecContext<TParam, TInstanceKey, TContextKey, TResult>>
    {
        /// <summary> Executes all queued items and completes the mesh. </summary>
        void Complete();
    }
}