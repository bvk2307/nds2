﻿#define NOTUSESINGLETON_EXECUTORMANAGER	//apopov 13.7.2016	//TODO!!!   //comment when the singleton Executor Manager will not retain Presenter's by Call Dispatchers

using System;
using System.Diagnostics.Contracts;
using System.Threading;
using Luxoft.NDS2.Common.Helpers.Executors;

namespace Luxoft.NDS2.Client.Infrastructure
{
    /// <summary> A factory of service call dispatchers. </summary>
    public sealed partial class ServiceCallDispatcherFactory : IServiceCallDispatcherFactory
    {
        private readonly IServiceCallExecutorManager _сallExecutorManager;

        public ServiceCallDispatcherFactory( IServiceCallExecutorManager сallExecutorManager )
        {
            Contract.Requires( сallExecutorManager != null );

            _сallExecutorManager = сallExecutorManager;
        }

        /// <summary> Creates and starts a prioritized dispatcher. </summary>
        /// <typeparam name="TParam"> A type of service call parameters. </typeparam>
        /// <typeparam name="TInstanceKey"> A type of caller instance key. </typeparam>
        /// <typeparam name="TContextKey"> A type of call context key. </typeparam>
        /// <typeparam name="TData"> A type of non transformed result. </typeparam>
        /// <typeparam name="TResult"> A type of service call result. </typeparam>
        /// <param name="dataTransofrmHandler"></param>
        /// <param name="serviceCallHandler"></param>
        /// <param name="cancelToken"></param>
        /// <param name="inputBoundCapacity"> Input bounded capacity. Bounded capacity is not limited if it is '0'. Optional. By default non limited. </param>
        /// <param name="lowInputBoundCapacity"> Low input bounded capacity. Bounded capacity is not limited if it is '0'. Optional. By default non limited. </param>
        /// <returns> A prioritized dispatcher started. </returns>
        public IServiceCallDispatcher<TParam, TInstanceKey, TContextKey, TData, TResult> 
            Open<TParam, TInstanceKey, TContextKey, TData, TResult>( 
                Func<CallExecContext<TParam, TInstanceKey, TContextKey, TData>, CallExecContext<TParam, TInstanceKey, TContextKey, TResult>> 
                    dataTransofrmHandler, 
                Func<TParam, CancellationToken, TData> serviceCallHandler, CancellationToken cancelToken = default(CancellationToken), 
                int inputBoundCapacity = 0, int lowInputBoundCapacity = 0 )
        {
            Contract.Requires( serviceCallHandler != null );
            Contract.Requires( dataTransofrmHandler != null );
            Contract.Ensures( Contract.Result<IServiceCallDispatcher<TParam, TInstanceKey, TContextKey, TData, TResult>>() != null );

#if NOTUSESINGLETON_EXECUTORMANAGER

            IServiceCallExecutorManager _сallExecutorManager = new ServiceCallExecutorManagerFactory.ServiceCallExecutorManager();

#endif //NOTUSESINGLETON_EXECUTORMANAGER

            ServiceCallDispatcher<TParam, TInstanceKey, TContextKey, TData, TResult> dispatcher = 
                new ServiceCallDispatcher<TParam, TInstanceKey, TContextKey, TData, TResult>( 
                    serviceCallHandler, _сallExecutorManager, dataTransofrmHandler, inputBoundCapacity, lowInputBoundCapacity, cancelToken );

            return dispatcher;
        }

        /// <summary> Creates and inits a batch dispatcher. </summary>
        /// <typeparam name="TParam"> A type of service call parameters. </typeparam>
        /// <typeparam name="TInstanceKey"> A type of caller instance key. </typeparam>
        /// <typeparam name="TContextKey"> A type of call context key. </typeparam>
        /// <typeparam name="TData"> A type of data loading result. </typeparam>
        /// <typeparam name="TResult"> A type of service call result. </typeparam>
        /// <param name="serviceCallHandler"></param>
        /// <param name="dataLoadStrategy"> A batch data strategy. </param>
        /// <param name="dataLoadedHandler"> A handler of data loaded. </param>
        /// <param name="cancelToken"></param>
        /// <param name="inputBoundCapacity"> Input bounded capacity. Bounded capacity is not limited if it is '0'. Optional. By default non limited. </param>
        /// <returns> A batch dispatcher created and initialized. </returns>
        public IServiceCallBatchDispatcher<TParam, TInstanceKey, TContextKey> 
            CreateBatch<TParam, TInstanceKey, TContextKey, TData, TResult>( 
            Func<TParam, CancellationToken, TData> serviceCallHandler, 
            IBatchDataLoadStrategy<TParam, TInstanceKey, TContextKey, TData, TResult> dataLoadStrategy, 
            Action<CallExecContext<TParam, TInstanceKey, TContextKey, TResult>> dataLoadedHandler, 
            CancellationToken cancelToken = default(CancellationToken), int inputBoundCapacity = 0 )
        {
            Contract.Ensures( Contract.Result<IServiceCallBatchDispatcher<TParam, TInstanceKey, TContextKey>>() != null );

#if NOTUSESINGLETON_EXECUTORMANAGER

            IServiceCallExecutorManager _сallExecutorManager = new ServiceCallExecutorManagerFactory.ServiceCallExecutorManager();

#endif //NOTUSESINGLETON_EXECUTORMANAGER

            ServiceCallBatchDispatcher<TParam, TInstanceKey, TContextKey, TData, TResult>
                dispatcher = new ServiceCallBatchDispatcher<TParam, TInstanceKey, TContextKey, TData, TResult>( 
                        serviceCallHandler, dataLoadStrategy, dataLoadedHandler, _сallExecutorManager, inputBoundCapacity, cancelToken );

            return dispatcher;
        }
    }
}