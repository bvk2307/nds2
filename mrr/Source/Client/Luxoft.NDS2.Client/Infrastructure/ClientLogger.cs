﻿using CommonComponents.Instrumentation;
using Luxoft.NDS2.Common.Contracts;
using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using FLS.CommonComponents.Lib.Interfaces;

namespace Luxoft.NDS2.Client.Infrastructure
{
    /// <summary> A client logger wrapper for <see cref="IInstrumentationService"/>. </summary>
    public class ClientLogger : IClientLogger
    {
        private readonly IInstrumentationService _instrumentationService;

        public ClientLogger( IInstrumentationService instrumentationService )
        {
            _instrumentationService = instrumentationService;
        }

        public void LogError( Exception ex,
            [CallerMemberName]
            string callerMemberName = null )
        {
            // ReSharper disable once ExplicitCallerInfoArgument
            LogError( ex.Message, ex, callerMemberName );
        }

        public void LogError( string message, Exception ex,
            [CallerMemberName]
            string methodName = null )
        {
            Log( message, TraceEventType.Error, InstrumentationLevel.Normal, methodName, ex );
        }

        public void LogWarning( string message,
            [CallerMemberName]
            string callerMemberName = null )
        {
            Log( message, TraceEventType.Warning, InstrumentationLevel.Troubleshooting, callerMemberName );
        }

        public void LogInfo( string message,
            [CallerMemberName]
            string callerMemberName = null )
        {
            Log( message, TraceEventType.Information, InstrumentationLevel.Debug, callerMemberName );
        }

        protected void Log( string message, TraceEventType traceEventType, InstrumentationLevel instrumentationLevel, 
                            string methodName, Exception ex = null )
        {
            IProfilingEntry le = _instrumentationService.CreateProfilingEntry(
                traceEventType, (int)instrumentationLevel, Constants.Instrumentation.CLIENT_PROFILE_TYPE_NAME, messagePriority: 100 );

            le.MessageText = message;

            AddException( le, ex );

            le.AddMonitoringItem( "Method name", methodName );

            _instrumentationService.Write( le );
        }

        protected static void AddException( IProfilingEntry rec, Exception ex, int num = 0 )
        {
            if ( ex != null )
            {
                rec.AddMonitoringItem( string.Format( "Exception{0}:", num ), ex );
                AddException( rec, ex.InnerException, num + 1 );
            }
        }

#region Implementation of IInstanceProvider<out IInstrumentationService>

        /// <summary> A provided instance. The provider does not guarantee that returns the same instance on each call. </summary>
        IInstrumentationService IInstanceProvider<IInstrumentationService>.Instance { get { return _instrumentationService; } }

#endregion
    }
}