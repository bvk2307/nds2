﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Providers;

namespace Luxoft.NDS2.Client.Infrastructure
{
    public class SubsystemProviderFactory : SubsystemProviderFactoryBase
    {
        public SubsystemProviderFactory(SubsystemInstanceInfoContext instanceInfo) : base(instanceInfo)
        {
        }

        #region Overrides of SubsystemProviderFactoryBase

        public override ISubsystemInstanceProvider CreateProvider()
        {
//            this.InstanceInfo.SubsystemMetadata.Environments
            return new SubsystemInstanceProvider(this.InstanceInfo);
        }

        #endregion
    }
}
