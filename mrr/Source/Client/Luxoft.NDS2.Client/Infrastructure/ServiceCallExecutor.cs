﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Luxoft.NDS2.Client.Infrastructure
{
    /// <summary> An executor of calls to server services. </summary>
    public sealed class ServiceCallExecutor
    {
        private Func<object, CancellationToken, object> _executeHandler;

        public ServiceCallExecutor( Func<object, CancellationToken, object> executeHandler )
        {
            _executeHandler = executeHandler;
        }

        public Task<object> Process( object parameters, CancellationToken cancelToken = default(CancellationToken) )
        {
            return TaskEx.Run( () => _executeHandler( parameters, cancelToken ), cancelToken );
        }
    }
}