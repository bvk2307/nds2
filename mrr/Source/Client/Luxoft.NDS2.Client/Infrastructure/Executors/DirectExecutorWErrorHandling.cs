﻿using System;
using System.Diagnostics.Contracts;
using System.Threading;
using Luxoft.NDS2.Client.UI;
using Luxoft.NDS2.Common.Helpers.Executors;

namespace Luxoft.NDS2.Client.Infrastructure.Executors
{
    public sealed class DirectExecutorWErrorHandling : IExecutorWErrorHandling
    {
        private readonly INotifier _notifier;
        private readonly IClientLogger _clientLogger;

        public DirectExecutorWErrorHandling( INotifier notifier, IClientLogger clientLogger )
        {
            Contract.Requires( notifier != null );
            Contract.Requires( clientLogger != null );

            _notifier = notifier;
            _clientLogger = clientLogger;
        }

        /// <summary> Вызов с обрабаткой исключения общего характера. Внимание! Перехватывает все exception-ы. </summary>
        /// <param name="callDelegate">Делегат серверного вызова</param>
        /// <param name="cancelToken">Токен для прерывания операции. Опционально, по умолчанию операция не прерывается</param>
        public void Execute( 
            Action<CancellationToken> callDelegate, CancellationToken cancelToken = default(CancellationToken) ) 
        {
            Contract.Requires(callDelegate != null);

            if ( !cancelToken.IsCancellationRequested )
            {
                try
                {
                    callDelegate( cancelToken );
                }
                catch ( Exception exc )
                {
                    NotifyError( exc );
                }
            }
        }

        /// <summary> Вызов с обрабаткой исключения общего характера. Внимание! Перехватывает все exception-ы. </summary>
        /// <typeparam name="TResult">Тип сущности передаваемой между делегатами</typeparam>
        /// <param name="callDelegate">Делегат серверного вызова</param>
        /// <param name="cancelToken">Токен для прерывания операции. Опционально, по умолчанию операция не прерывается</param>
        /// <returns>Результат выполнения операции</returns>
        public TResult Execute<TResult>( Func<CancellationToken, TResult> callDelegate, CancellationToken cancelToken = default(CancellationToken) ) 
        {
            Contract.Requires( callDelegate != null );

            TResult result = default(TResult);
            if ( !cancelToken.IsCancellationRequested )
            {
                try
                {
                    result = callDelegate( cancelToken );
                }
                catch ( Exception exc )
                {
                    NotifyError( exc );
                }
            }
            return result;
        }

        /// <summary> Вызов с обрабаткой исключения общего характера. Внимание! Перехватывает все exception-ы. </summary>
        /// <typeparam name="TParam1"></typeparam>
        /// <param name="callDelegate">Делегат серверного вызова</param>
        /// <param name="param1">Параметр вызова</param>
        /// <param name="cancelToken">Токен для прерывания операции. Опционально, по умолчанию операция не прерывается</param>
        public void Execute<TParam1>( 
            Action<TParam1, CancellationToken> callDelegate, TParam1 param1, CancellationToken cancelToken = default(CancellationToken) ) 
        {
            Contract.Requires(callDelegate != null);

            if ( !cancelToken.IsCancellationRequested )
            {
                try
                {
                    callDelegate( param1, cancelToken );
                }
                catch ( Exception exc )
                {
                    NotifyError( exc );
                }
            }
        }

        /// <summary> Вызов с обрабаткой исключения общего характера. Внимание! Перехватывает все exception-ы. </summary>
        /// <typeparam name="TParam1"></typeparam>
        /// <typeparam name="TResult">Тип сущности передаваемой между делегатами</typeparam>
        /// <param name="callDelegate">Делегат серверного вызова</param>
        /// <param name="param1">Параметр вызова</param>
        /// <param name="cancelToken">Токен для прерывания операции. Опционально, по умолчанию операция не прерывается</param>
        /// <returns>Результат выполнения операции</returns>
        public TResult Execute<TParam1, TResult>( Func<TParam1, CancellationToken, TResult> callDelegate, TParam1 param1, 
            CancellationToken cancelToken = default(CancellationToken) ) 
        {
            Contract.Requires( callDelegate != null );

            TResult result = default(TResult);
            if ( !cancelToken.IsCancellationRequested )
            {
                try
                {
                    result = callDelegate( param1, cancelToken );
                }
                catch ( Exception exc )
                {
                    NotifyError( exc );
                }
            }
            return result;
        }

        /// <summary> Вызов с обрабаткой исключения общего характера. Внимание! Перехватывает все exception-ы. </summary>
        /// <typeparam name="TParam1"></typeparam>
        /// <typeparam name="TParam2"></typeparam>
        /// <param name="callDelegate">Делегат серверного вызова</param>
        /// <param name="param1">Первый параметр вызова</param>
        /// <param name="param2">Второй параметр вызова</param>
        /// <param name="cancelToken">Токен для прерывания операции. Опционально, по умолчанию операция не прерывается</param>
        public void Execute<TParam1, TParam2>( 
            Action<TParam1, TParam2, CancellationToken> callDelegate, TParam1 param1, TParam2 param2, 
            CancellationToken cancelToken = default(CancellationToken) ) 
        {
            Contract.Requires(callDelegate != null);

            if ( !cancelToken.IsCancellationRequested )
            {
                try
                {
                    callDelegate( param1, param2, cancelToken );
                }
                catch ( Exception exc )
                {
                    NotifyError( exc );
                }
            }
        }

        /// <summary> Вызов с обрабаткой исключения общего характера. Внимание! Перехватывает все exception-ы. </summary>
        /// <typeparam name="TParam1"></typeparam>
        /// <typeparam name="TParam2"></typeparam>
        /// <typeparam name="TResult">Тип сущности передаваемой между делегатами</typeparam>
        /// <param name="callDelegate">Делегат серверного вызова</param>
        /// <param name="param1">Параметр вызова</param>
        /// <param name="param2">Второй параметр вызова</param>
        /// <param name="cancelToken">Токен для прерывания операции. Опционально, по умолчанию операция не прерывается</param>
        /// <returns>Результат выполнения операции</returns>
        public TResult Execute<TParam1, TParam2, TResult>( Func<TParam1, TParam2, CancellationToken, TResult> callDelegate, 
            TParam1 param1, TParam2 param2, CancellationToken cancelToken = default(CancellationToken) ) 
        {
            Contract.Requires( callDelegate != null );

            TResult result = default(TResult);
            if ( !cancelToken.IsCancellationRequested )
            {
                try
                {
                    result = callDelegate( param1, param2, cancelToken );
                }
                catch ( Exception exc )
                {
                    NotifyError( exc );
                }
            }
            return result;
        }

        /// <summary> Executes a delegate defined. </summary>
        /// <typeparam name="TParam1"> A type of the first parameter <paramref name="param1"/> of the delegate <paramref name="callDelegate"/>. </typeparam>
        /// <typeparam name="TParam2"> A type of the second parameter <paramref name="param2"/> of the delegate <paramref name="callDelegate"/>. </typeparam>
        /// <typeparam name="TParam3"></typeparam>
        /// <param name="callDelegate"> A deletegate to execute. </param>
        /// <param name="param1"> The first delegate parameter. </param>
        /// <param name="param2"> The second delegate parameter. </param>
        /// <param name="param3">Третий параметр вызова</param>
        /// <param name="cancelToken"> A cancellation token. Optional. Is not cancellable if this parameter is not defined. </param>
        public void Execute<TParam1, TParam2, TParam3>( Action<TParam1, TParam2, TParam3, CancellationToken> callDelegate, 
            TParam1 param1, TParam2 param2, TParam3 param3, CancellationToken cancelToken = default(CancellationToken) )
        {
            Contract.Requires(callDelegate != null);

            if ( !cancelToken.IsCancellationRequested )
            {
                try
                {
                    callDelegate( param1, param2, param3, cancelToken );
                }
                catch ( Exception exc )
                {
                    NotifyError( exc );
                }
            }
        }

        /// <summary> Вызов с обрабаткой исключения общего характера. Внимание! Перехватывает все exception-ы. </summary>
        /// <typeparam name="TParam1"></typeparam>
        /// <typeparam name="TParam2"></typeparam>
        /// <typeparam name="TParam3"></typeparam>
        /// <typeparam name="TResult">Тип сущности передаваемой между делегатами</typeparam>
        /// <param name="callDelegate">Делегат серверного вызова</param>
        /// <param name="param1">Параметр вызова</param>
        /// <param name="param2">Второй параметр вызова</param>
        /// <param name="param3">Третий параметр вызова</param>
        /// <param name="cancelToken">Токен для прерывания операции. Опционально, по умолчанию операция не прерывается</param>
        /// <returns>Результат выполнения операции</returns>
        public TResult Execute<TParam1, TParam2, TParam3, TResult>( Func<TParam1, TParam2, TParam3, CancellationToken, TResult> callDelegate, 
            TParam1 param1, TParam2 param2, TParam3 param3, CancellationToken cancelToken = default(CancellationToken) ) 
        {
            Contract.Requires( callDelegate != null );

            TResult result = default(TResult);
            if ( !cancelToken.IsCancellationRequested )
            {
                try
                {
                    result = callDelegate( param1, param2, param3, cancelToken );
                }
                catch ( Exception exc )
                {
                    NotifyError( exc );
                }
            }
            return result;
        }

        /// <summary> Executes a delegate defined. </summary>
        /// <typeparam name="TParam1"> A type of the first parameter <paramref name="param1"/> of the delegate <paramref name="callDelegate"/>. </typeparam>
        /// <typeparam name="TParam2"> A type of the second parameter <paramref name="param2"/> of the delegate <paramref name="callDelegate"/>. </typeparam>
        /// <typeparam name="TParam3"></typeparam>
        /// <typeparam name="TParam4"></typeparam>
        /// <param name="callDelegate"> A deletegate to execute. </param>
        /// <param name="param1"> The first delegate parameter. </param>
        /// <param name="param2"> The second delegate parameter. </param>
        /// <param name="param3">Третий параметр вызова</param>
        /// <param name="param4">Четвертый параметр вызова</param>
        /// <param name="cancelToken"> A cancellation token. Optional. Is not cancellable if this parameter is not defined. </param>
        public void Execute<TParam1, TParam2, TParam3, TParam4>( Action<TParam1, TParam2, TParam3, TParam4, CancellationToken> callDelegate, 
            TParam1 param1, TParam2 param2, TParam3 param3, TParam4 param4, 
            CancellationToken cancelToken = default(CancellationToken) )
        {
            Contract.Requires(callDelegate != null);

            if ( !cancelToken.IsCancellationRequested )
            {
                try
                {
                    callDelegate( param1, param2, param3, param4, cancelToken );
                }
                catch ( Exception exc )
                {
                    NotifyError( exc );
                }
            }
        }

        /// <summary> Вызов с обрабаткой исключения общего характера. Внимание! Перехватывает все exception-ы. </summary>
        /// <typeparam name="TParam1"></typeparam>
        /// <typeparam name="TParam2"></typeparam>
        /// <typeparam name="TParam3"></typeparam>
        /// <typeparam name="TParam4"></typeparam>
        /// <typeparam name="TResult">Тип сущности передаваемой между делегатами</typeparam>
        /// <param name="callDelegate">Делегат серверного вызова</param>
        /// <param name="param1">Первый араметр вызова</param>
        /// <param name="param2">Второй параметр вызова</param>
        /// <param name="param3">Третий параметр вызова</param>
        /// <param name="param4">Четвертый параметр вызова</param>
        /// <param name="cancelToken">Токен для прерывания операции. Опционально, по умолчанию операция не прерывается</param>
        /// <returns>Результат выполнения операции</returns>
        public TResult Execute<TParam1, TParam2, TParam3, TParam4, TResult>(
            Func<TParam1, TParam2, TParam3, TParam4, CancellationToken, TResult> callDelegate,
            TParam1 param1, TParam2 param2, TParam3 param3, TParam4 param4,
            CancellationToken cancelToken = default( CancellationToken ) )
        {
            Contract.Requires( callDelegate != null );

            TResult result = default(TResult);
            if ( !cancelToken.IsCancellationRequested )
            {
                try
                {
                    result = callDelegate( param1, param2, param3, param4, cancelToken );
                }
                catch ( Exception exc )
                {
                    NotifyError( exc );
                }
            }
            return result;
        }

        private void NotifyError( Exception exc )
        {
            if ( !IsExceptionToIgnore( exc ) )
            {
                _clientLogger.LogError( exc.Message, exc );

                _notifier.ShowError( exc.Message );
            }
        }

        private static bool IsExceptionToIgnore( Exception exc )
        {
            return exc is OperationCanceledException;
        }
    }
}