using System.Collections.Generic;
using Luxoft.NDS2.Common.Helpers.Executors;

namespace Luxoft.NDS2.Client.Infrastructure
{
    /// <summary> A strategy controls batch data processing. </summary>
    /// <typeparam name="TParam"></typeparam>
    /// <typeparam name="TInstanceKey"></typeparam>
    /// <typeparam name="TContextKey"></typeparam>
    /// <typeparam name="TData"></typeparam>
    /// <typeparam name="TResult"></typeparam>
    public interface IBatchDataLoadStrategy<TParam, TInstanceKey, TContextKey, TData, TResult>
    {
        /// <summary> Initializes the strategy parameters. </summary>
        /// <param name="parameters"></param>
        void Init( TParam parameters );

        /// <summary> Buffers recieved datas in the internal buffer before post processing. </summary>
        /// <param name="callExecContext"></param>
        void Buffer( CallExecContext<TParam, TInstanceKey, TContextKey, TData> callExecContext );

        /// <summary> Returns next data requests that should be requested at the bounds of the batch. </summary>
        /// <returns> The next data requests or 'null' if the completion is requested instead of. </returns>
        IReadOnlyCollection<TParam> GetNextToPostOrFinish();

        /// <summary> Takes next datas from the internal buffer to post process. </summary>
        /// <returns></returns>
        CallExecContext<TParam, TInstanceKey, TContextKey, TResult> GetNextToOffer();
    }
}