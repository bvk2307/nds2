﻿using System;
using System.Threading;
using Luxoft.NDS2.Common.Helpers.Executors;

namespace Luxoft.NDS2.Client.Infrastructure
{
    /// <summary> A factory of service call dispatchers. </summary>
    public interface IServiceCallDispatcherFactory
    {
        /// <summary> Creates and starts a prioritized dispatcher. </summary>
        /// <typeparam name="TParam"> A type of service call parameters. </typeparam>
        /// <typeparam name="TInstanceKey"> A type of caller instance key. </typeparam>
        /// <typeparam name="TContextKey"> A type of call context key. </typeparam>
        /// <typeparam name="TData"> A type of non transformed result. </typeparam>
        /// <typeparam name="TResult"> A type of service call result. </typeparam>
        /// <param name="dataTransofrmHandler"></param>
        /// <param name="serviceCallHandler"></param>
        /// <param name="cancelToken"></param>
        /// <param name="inputBoundCapacity"> Input bounded capacity. Bounded capacity is not limited if it is '0'. Optional. By default non limited. </param>
        /// <param name="lowInputBoundCapacity"> Low input bounded capacity. Bounded capacity is not limited if it is '0'. Optional. By default non limited. </param>
        /// <returns> A prioritized dispatcher started. </returns>
        IServiceCallDispatcher<TParam, TInstanceKey, TContextKey, TData, TResult> 
            Open<TParam, TInstanceKey, TContextKey, TData, TResult>( 
                Func<CallExecContext<TParam, TInstanceKey, TContextKey, TData>, CallExecContext<TParam, TInstanceKey, TContextKey, TResult>> 
                    dataTransofrmHandler, 
                Func<TParam, CancellationToken, TData> serviceCallHandler, CancellationToken cancelToken = default(CancellationToken), 
                int inputBoundCapacity = 0, int lowInputBoundCapacity = 0 );

        /// <summary> Creates and inits a batch dispatcher. </summary>
        /// <typeparam name="TParam"> A type of service call parameters. </typeparam>
        /// <typeparam name="TInstanceKey"> A type of caller instance key. </typeparam>
        /// <typeparam name="TContextKey"> A type of call context key. </typeparam>
        /// <typeparam name="TData"> A type of data loading result. </typeparam>
        /// <typeparam name="TResult"> A type of service call result. </typeparam>
        /// <param name="serviceCallHandler"></param>
        /// <param name="dataLoadStrategy"> A batch data strategy. </param>
        /// <param name="dataLoadedHandler"> A handler of data loaded. </param>
        /// <param name="cancelToken"></param>
        /// <param name="inputBoundCapacity"> Input bounded capacity. Bounded capacity is not limited if it is '0'. Optional. By default non limited. </param>
        /// <returns> A batch dispatcher created and initialized. </returns>
        IServiceCallBatchDispatcher<TParam, TInstanceKey, TContextKey> 
            CreateBatch<TParam, TInstanceKey, TContextKey, TData, TResult>( 
                Func<TParam, CancellationToken, TData> serviceCallHandler, 
                IBatchDataLoadStrategy<TParam, TInstanceKey, TContextKey, TData, TResult> dataLoadStrategy, 
                Action<CallExecContext<TParam, TInstanceKey, TContextKey, TResult>> dataLoadedHandler, 
                CancellationToken cancelToken = default(CancellationToken), int inputBoundCapacity = 0 );
    }
}