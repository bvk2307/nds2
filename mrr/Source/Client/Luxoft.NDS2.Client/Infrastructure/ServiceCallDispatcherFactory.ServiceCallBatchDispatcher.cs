﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Threading;
using System.Threading.Tasks;
using Luxoft.NDS2.Common.Helpers.Executors;

namespace Luxoft.NDS2.Client.Infrastructure
{
    /// <summary> A factory of <see cref="ServiceCallDispatcher{TParam, TInstanceKey, TContextKey, TData, TResult}"/>. </summary>
    public sealed partial class ServiceCallDispatcherFactory
    {
        /// <summary> A dispatcher for a batch of calls to a some server service. </summary>
        /// <typeparam name="TParam"></typeparam>
        /// <typeparam name="TData"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <typeparam name="TInstanceKey"></typeparam>
        /// <typeparam name="TContextKey"></typeparam>
        private sealed class ServiceCallBatchDispatcher<TParam, TInstanceKey, TContextKey, TData, TResult> : 
            CallBatchDispatcher<CallExecContext<TParam, TInstanceKey, TContextKey, TData>, 
                                CallExecContext<TParam, TInstanceKey, TContextKey, TResult>>,
            IServiceCallBatchDispatcher<TParam, TInstanceKey, TContextKey>
        {
            private readonly int PostBackPressureLimit = 8 * ServiceCallExecutorManagerFactory.MaxExecutorNumber;

            private readonly IBatchDataLoadStrategy<TParam, TInstanceKey, TContextKey, TData, TResult> _dataLoadStrategy;
            private readonly Func<object, CancellationToken, object> _executorHandler;

            private bool _isCompleting = false;

            public ServiceCallBatchDispatcher( 
                Func<TParam, CancellationToken, TData> serviceCallHandler, 
                IBatchDataLoadStrategy<TParam, TInstanceKey, TContextKey, TData, TResult> dataLoadStrategy, 
                Action<CallExecContext<TParam, TInstanceKey, TContextKey, TResult>> dataLoadedHandler, 
                ICallExecutorManager сallExecutorManager, int inputBoundCapacity = 0, 
                CancellationToken cancelToken = default(CancellationToken) )
                : base( dataLoadedHandler, сallExecutorManager, inputBoundCapacity, cancelToken )
            {
                Contract.Ensures( _executorHandler != null );
                Contract.Assume( serviceCallHandler != null );

                _executorHandler    = 
                    ( parameters, cancelTokenPar ) => serviceCallHandler( (TParam)parameters, cancelTokenPar );
                _dataLoadStrategy   = dataLoadStrategy;
            }

            protected override Task<CallExecContext<TParam, TInstanceKey, TContextKey, TData>> 
                ExecuteCallAsync( ICallExecutor executor, CallExecContext<TParam, TInstanceKey, TContextKey, TData> context )
            {
                Task<CallExecContext<TParam, TInstanceKey, TContextKey, TData>> execute = 
                    executor.Process( _executorHandler, context.Parameters )
                        .ContinueWith(
                            taskPrev =>
                            {
                                context.Result = (TData)taskPrev.Result;

                                return context;
                            },
                            TaskContinuationOptions.ExecuteSynchronously );

                return execute;
            }

            public override CallExecContext<TParam, TInstanceKey, TContextKey, TResult> 
                Process( CallExecContext<TParam, TInstanceKey, TContextKey, TData> callExecContext )
            {
                _dataLoadStrategy.Buffer( callExecContext );

                bool toComplete = false;
                if ( !_isCompleting && !IsTooManyPosted() ) //if no many work items have been posted already
                {
                    IReadOnlyCollection<TParam> toPost = _dataLoadStrategy.GetNextToPostOrFinish();
                    if ( toPost == null )
                    {
                        toComplete = !_isCompleting;    //calls Complete() only one time
                    }
                    else if ( toPost.Count > 0 )
                    {
                        TInstanceKey instanceKey = callExecContext.InstanceKey;
                        TContextKey contextKey = callExecContext.ContextKey;
                        foreach ( TParam param in toPost )
                        {
                            Post( param, instanceKey, contextKey );
                        }
                    }
                }
                CallExecContext<TParam, TInstanceKey, TContextKey, TResult> toOffer = _dataLoadStrategy.GetNextToOffer();

                if ( toComplete )
                {
                    Complete();

                    _isCompleting = true;
                }
                return toOffer;
            }

            /// <summary> Starts the batch execution. </summary>
            /// <param name="keyParameters"> A request parameter. </param>
            /// <param name="instanceKey"> A call instance key that will be returned into the post processing. </param>
            /// <param name="contextKey"> An execution context key that will be returned into the post processing. </param>
            public void Start( TParam keyParameters, TInstanceKey instanceKey, TContextKey contextKey )
            {
                _dataLoadStrategy.Init( keyParameters );

                Post( keyParameters, instanceKey, contextKey );
            }

            private bool IsTooManyPosted()
            {
                bool isTooMany = InputCount >= PostBackPressureLimit;

                return isTooMany;
            }

            /// <summary> Posts the part of batch to execute. </summary>
            /// <param name="keyParameters"> A request parameter. </param>
            /// <param name="instanceKey"> A call instance key that will be returned into the post processing. </param>
            /// <param name="contextKey"> An execution context key that will be returned into the post processing. </param>
            /// <returns> 'true' on success and 'false' on failure. </returns>
            private bool Post( TParam keyParameters, TInstanceKey instanceKey, TContextKey contextKey )
            {
                bool success = Post( new CallExecContext<TParam, TInstanceKey, TContextKey, TData>( 
                                        keyParameters, instanceKey, contextKey ) );

                return success;
            }
        }
    }
}