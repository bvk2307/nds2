﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonComponents.Shared;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Providers;

namespace Luxoft.NDS2.Client.Infrastructure
{
    public class SubsystemMetadataProvider : ISubsystemMetadataProvider
    {
        ISubsystemNavigationNodesProvider _subsystemNavigationNodesProvider;

        public SubsystemMetadataProvider(ISubsystemNavigationNodesProvider subsystemNavigationNodesProvider)
        {
            _subsystemNavigationNodesProvider = subsystemNavigationNodesProvider;
        }

        #region ISubsystemMetadataProvider Members

        NavigationNodeContext ISubsystemMetadataProvider.GetRootNavigationNode(IDataContext connectionContext, string source)
        {
            return _subsystemNavigationNodesProvider.GetRootNavigationNode(connectionContext, source);
        }

        public IList<NavigationNodeContext> GetNavigationNodes(IDataContext connectionContext, NavigationNodeContext parentNode)
        {
            return _subsystemNavigationNodesProvider.GetNavigationNodes(connectionContext, parentNode);
        }

        #endregion
    }
}
