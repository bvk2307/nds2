﻿using CommonComponents.Communication;
using CommonComponents.Configuration;
using CommonComponents.Instrumentation;
using CommonComponents.Security.Authorization;
using CommonComponents.Shared;
using CommonComponents.Uc.Infrastructure.Interface.Constants;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Providers;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using FLS.CommonComponents.App.ErrorHandling;
using FLS.CommonComponents.App.Services.Exrtensions;
using FLS.CommonComponents.App.Wpf.App;
using FLS.GoWPFApp;
using Luxoft.NDS2.Client.Infrastructure.Executors;
using Luxoft.NDS2.Client.Model;
using Luxoft.NDS2.Client.UI;
using Luxoft.NDS2.Client.UI.CommonFunctions;
using Luxoft.NDS2.Client.UI.Demo;
using Luxoft.NDS2.Client.UI.UserTask.List;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.AccessRestriction;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Security;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Common.Helpers.Executors;
using Microsoft.Practices.CompositeUI;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Markup;
using FLS.CommonComponents.Lib.Execution;

namespace Luxoft.NDS2.Client.Infrastructure
{
    public class SubsystemNavigationNodesProvider : ISubsystemNavigationNodesProvider
    {
        #region Private members

        private static readonly string[] SupportedNavigationNodeSources =
        {
            NavigationNodeSourceNames.TaskSource,
            NavigationNodeSourceNames.HelpSource
        };

        private readonly ISecurityService _isecurity;
        private readonly IUcMessageService _messageService;
        private readonly IConfigurationDataService _configuration;
        private List<AccessRight> _userRights;
        private string _appVersion;
        private bool _isDiagnosticMode;
        private string[] _allowedOperations;


        private static readonly Guid Nds2TaskRootNodeId = new Guid("0C01E79F-F0EB-4F9D-B12A-CF7C0A1BB75C");
        private static readonly Guid Nds2TaskRootNodeHelpId = new Guid("86151ECE-2713-491A-AA23-E174BC7D8D35");
        private static readonly Guid Nds2ManagementConsoleId = new Guid("B7F60057-1AB9-4B28-96BD-DA11A7BDFE67");

        private static readonly Guid DevWndId = new Guid("0E0B5E22-68AC-4109-BE60-9A39CE75A1D9");

        private static readonly Guid SelectionsWndId = new Guid("30FC7071-62CE-42D5-83EE-1D94ACA5694E");
        private static readonly Guid SelectionListWndId = new Guid("b9914fab-b0a5-4181-a76a-18c5b68ffec7");
        private static readonly Guid InvoiceClaimListWndId = new Guid("0C508592-D07D-4590-9616-AFDEA7D5DB49");
        private static readonly Guid UpdateInstallWndId = new Guid("a32569a3-8d7e-4761-8e7c-e8ef59431af2");
        private static readonly Guid DeclarationListWndId = new Guid("C10284FD-1034-46A6-BBC6-AA9CF40773DF");
        private static readonly Guid ArmInspectorWndId = new Guid("02b3cb5e-eaf7-4c47-a722-2f7d22ca7e9d");
        private static readonly Guid MacroreportsId = new Guid("10a04805-30f5-4cb5-a1f7-e5cc0242ecf8");
        private static readonly Guid ReportListWndId = new Guid("4D219EDE-FF5A-4A47-8E0F-67D7231FD63A");
        private static readonly Guid NavigatorWndId = new Guid("4D219EDE-FF5A-4A47-8E0F-67D7231FD63B");

        private static readonly Guid SettingsWndId = new Guid("0E05A509-77BD-402F-AC54-E68ACE5E7380");
        private static readonly Guid SettingsListsWndId = new Guid("0dc37f68-dc88-425a-a4d7-a3aef8961c67");
        private static readonly Guid LookupsWndId = new Guid("764BE08F-99FD-4B4E-9E00-49919B3316A4");
        private static readonly Guid CalendarWndId = new Guid("76670E25-E556-40B7-B446-B833C6B1175C");

        private static readonly Guid WpfWndId = new Guid("03C312C2-C884-4B62-B5F4-10E10B7AF633");
        private static readonly Guid WpfEffWndId = new Guid("D20B749A-1E26-42BA-AA02-15306A807AF1");
        private static readonly Guid UserTaskWndId = new Guid("0DC06C0B-33FF-43D6-8D93-80B4AB110ECF");

        private static readonly Guid VersionHistoryNodeId = new Guid("89EFE630-3DB0-42D7-B942-A8586849B255");

        private readonly WorkItem _subsystemWorkItem;

        #endregion Private members

        public SubsystemNavigationNodesProvider(WorkItem subsystemWorkItem)
        {
            _subsystemWorkItem = subsystemWorkItem;
            var services = subsystemWorkItem.Services;
            _messageService = services.Get<IUcMessageService>();
            _isecurity = services.Get<IClientContextService>().CreateCommunicationProxy<ISecurityService>(Constants.EndpointName);
            _configuration = services.Get<IConfigurationDataService>();

            var instrumentationService = services.Get<IInstrumentationService>(true);
            services.Add((IClientLogger)new ClientLogger(instrumentationService));

            InitUserPermissions();
            InitVersion();
            InitDiagMode();

            ApplicationWpfInitializer.SubscribeOrExecuteOnAppInitialized(OnWpfInitialized);

            //apopov 23.8.2016	//commented to avoid exception inside AsyncWorker on application loading. It is agreed with M. Mekhriakov
            #region Commented. Activity log

            //var wi = WindowsIdentity.GetCurrent();
            //var sid = wi != null && wi.User != null ? wi.User.Value : string.Empty;

            //var svc = services.Get<IClientContextService>().CreateCommunicationProxy<IActivityLogService>(Constants.EndpointName);
            //var asyncWorker = new AsyncWorker<OperationResult>();
            //asyncWorker.DoWork += (sender, args) => svc.Write(sid, ActivityLogEntry.New(ActivityLogType.LogonsCount));
            //asyncWorker.Start();

            #endregion
        }

        /// <summary> WPF UI thread subsystem initialization. </summary>
        /// <param name="threadInvoker"></param>
        private void OnWpfInitialized(IThreadInvoker threadInvoker)
        {
            Contract.Requires(threadInvoker != null);
            if (threadInvoker == null)
                throw new ArgumentNullException("threadInvoker");
            Contract.EndContractBlock();

            Contract.Assume(_messageService != null);

            var instrumentationService = _subsystemWorkItem.Services.Get<IInstrumentationService>(ensureExists: true);
            var handlingOptions = new UnhandledExceptionHandlingOptions(
                UnhandledExceptionHandlingOptions.Default.CatchEvents,
                //apopov 11.1.2017	//DEBUG!!! 
                UnhandledExceptionHandlingOptions.Default.QuitOnEvents);   //application exits on unhandled exceptions
            //UnhandledExceptionHandlingOptions.AppQuitOnEvents.None );   //application does not exit on unhandled exceptions

            UnhandledExceptionHandler.InitIfNeeded(instrumentationService, _messageService, handlingOptions);      //assumes like of an application enter point. It supports multi time calls. Must be called after initialization of UIThreadExecutor.CurrentExecutor

            _subsystemWorkItem.Services.InitOrGetService(() => threadInvoker);

            var le = instrumentationService.CreateProfilingEntry(TraceEventType.Information, (int)InstrumentationLevel.Normal,
                                                        Constants.Instrumentation.CLIENT_PROFILE_TYPE_NAME, messagePriority: 100);
            le.MessageText = "Подсистема НДС2 инициализирована";
            le.AddMonitoringItem("Main thread invoker hash code", threadInvoker.GetHashCode());
            le.AddMonitoringItem("AppDomain ID", AppDomain.CurrentDomain.Id);
            instrumentationService.Write(le);

            //Устанавливаем текущую локаль для всех wpf контролов
            var cultureXmlTag = XmlLanguage.GetLanguage(CultureInfo.CurrentCulture.Name);
            FrameworkElement.LanguageProperty.OverrideMetadata(typeof(FrameworkElement), new FrameworkPropertyMetadata(cultureXmlTag));
        }

        private void InitUserPermissions()
        {
            try
            {
                var result = _isecurity.GetUserPermissions();
                _userRights = result.Status == ResultStatus.Success
                    ? result.Result
                    : new List<AccessRight>();
            }
            catch (Exception)
            {
                _userRights = new List<AccessRight>();
            }

        }

        private void InitVersion()
        {
            var prof = ProfileInfo.Default;
            AppSettingsSection apps;
            _appVersion = _configuration.TryGetSection(prof, out apps)
                ? apps.Settings["Version"].Value
                : "0.0.0.0";
        }

        private void InitDiagMode()
        {
            var prof = ProfileInfo.Default;
            AppSettingsSection apps;

            if (_configuration.TryGetSection(prof, out apps))
            {
                try
                {
                    _isDiagnosticMode = bool.Parse(apps.Settings["DiagMode"].Value);
                }
                catch (Exception)
                {

                    _isDiagnosticMode = false;
                }
            }
            else
            {
                _isDiagnosticMode = false;
            }
        }

        /// <summary>
        /// Получить список источников узлов поддерживаемых провайдером дерева подсистемы.
        /// ЕКП поддерживает два источника описанных в <see cref="NavigationNodeSourceNames"/>
        /// </summary>
        /// <returns></returns>
        public string[] GetSupportedNavigationNodeSources()
        {
            InitUserPermissions();
            return SupportedNavigationNodeSources;
        }

        /// <summary>
        /// Получить корневой узел навигации для определенного источника.
        /// Должен описывать экземпляр подсистемы.
        /// Отождествляется с соединением.
        /// </summary>
        /// <param name="connectionContext"></param>
        /// <param name="source"></param>
        /// <returns></returns>
        public NavigationNodeContext GetRootNavigationNode(IDataContext connectionContext, string source)
        {
            if (!HasAnyRole())
            {
                return null;
            }
            InitAllowedOperations();

            const string subsystemName = "АСК НДС 2";

            if (source == NavigationNodeSourceNames.TaskSource)
            {
                var root = new NavigationNodeContext();
                var helper = new DataContextHelper(root)
                {
                    {"CanAddToFavorite", true},
                    {"CanExpand", true},
                    {"Description", "Модуль работы с результатами расхождений встречных проверок"},
                    {"FavoriteInfo", new NavigationNodeFavoriteInfoContext()},
                    {"HandleNavigate", false},
                    {"ImageKey", ""},
                    {"IsVisible", true},
                    {"Key", "NDS2 Subsystem"},
                    {"Name", "NDS2 Subsystem"},
                    {"NodeId", Nds2TaskRootNodeId},
                    {"OrderId", 1},
                    {"Source", NavigationNodeSourceNames.TaskSource},
                    {"UfName", subsystemName},
                    {"VisualisationProviderName", ""}
                };

                return root;
            }
            if (source == NavigationNodeSourceNames.HelpSource)
            {
                var root = new NavigationNodeContext();
                var helper = new DataContextHelper(root)
                {
                    {"CanAddToFavorite", false},
                    {"CanExpand", true},
                    {"Description", "Модуль работы с результатами расхождений встречных проверок"},
                    {"FavoriteInfo", new NavigationNodeFavoriteInfoContext()},
                    {"HandleNavigate", false},
                    {"ImageKey", ""},
                    {"IsVisible", true},
                    {"Key", "NDS2 Subsystem - Help"},
                    {"Name", ""},
                    {"NodeId", Nds2TaskRootNodeHelpId},
                    {"OrderId", 1},
                    {"Source", NavigationNodeSourceNames.HelpSource},
                    {"UfName", subsystemName},
                    {"VisualisationProviderName", ""}
                };
                return root;
            }

            throw new NotImplementedException(
                string.Format("Source '{0}' is not implemented", source));
        }

        /// <summary>
        /// Получить элементы навигационного дерева.
        /// Данный метод вызывается рекурсивно.
        /// </summary>
        /// <returns></returns>
        public IList<NavigationNodeContext> GetNavigationNodes(IDataContext connectionContext, NavigationNodeContext parentNode)
        {
            var orderId = 0;

            if (parentNode == null) throw new ArgumentNullException("parentNode");

            if (parentNode.NodeId == Nds2ManagementConsoleId)
            {
                #region Папка Настройки

                var nodes = new List<NavigationNodeContext>();
                NavigationNodeContext node;
                DataContextHelper helper;

                #region Параметры АСК НДС-2

                if (HasOperation(MrrOperations.AskParams))
                {

                    node = new NavigationNodeContext();
                    helper = new DataContextHelper(node)
                    {
                        {"CanAddToFavorite", false},
                        {"CanExpand", false},
                        {"Description", "Параметры АСК НДС-2"},
                        {"FavoriteInfo", new NavigationNodeFavoriteInfoContext()},
                        {"HandleNavigate", true},
                        {"ImageKey", ""},
                        {"IsVisible", true},
                        {"Key", "ChiefPane"},
                        {"Name", "ChiefPane"},
                        {"NodeId", SettingsWndId},
                        {"OrderId", 0},
                        {"Source", NavigationNodeSourceNames.TaskSource},
                        {"UfName", "Параметры АСК НДС-2"},
                        {"MultiWindow", true},
                        {"VisualisationProviderName", ""}
                    };
                    helper.Add<Type>("ViewType", typeof(UI.Settings.Nds2Parameters.ParametersView));

                    nodes.Add(node);
                }

                #endregion

                #region Настройка списков

                if (HasOperation(MrrOperations.ListSettings))
                {
                    node = new NavigationNodeContext();
                    helper = new DataContextHelper(node)
                        {
                            {"CanAddToFavorite", false},
                            {"CanExpand", false},
                            {"Description", "Настройка списков"},
                            {"FavoriteInfo", new NavigationNodeFavoriteInfoContext()},
                            {"HandleNavigate", true},
                            {"ImageKey", ""},
                            {"IsVisible", true},
                            {"Key", "ChiefPane"},
                            {"Name", "ChiefPane"},
                            {"NodeId", SettingsListsWndId},
                            {"OrderId", 1},
                            {"Source", NavigationNodeSourceNames.TaskSource},
                            {"UfName", "Настройка списков"},
                            {"MultiWindow", true},
                            {"VisualisationProviderName", ""}
                        };
                    helper.Add<Type>("ViewType", typeof(UI.Settings.SettingsLists.WhiteListsView));

                    nodes.Add(node);
                }

                #endregion

                #region Список справочников

                if (HasOperation(MrrOperations.Dictionaries))
                {
                    node = new NavigationNodeContext();
                    helper = new DataContextHelper(node)
                    {
                        {"CanAddToFavorite", false},
                        {"CanExpand", false},
                        {"Description", "Список справочников"},
                        {"FavoriteInfo", new NavigationNodeFavoriteInfoContext()},
                        {"HandleNavigate", true},
                        {"ImageKey", ""},
                        {"IsVisible", true},
                        {"Key", "ChiefPane"},
                        {"Name", "ChiefPane"},
                        {"NodeId", LookupsWndId},
                        {"OrderId", 4},
                        {"Source", NavigationNodeSourceNames.TaskSource},
                        {"UfName", "Список справочников"},
                        {"MultiWindow", true},
                        {"VisualisationProviderName", ""}
                    };

                    helper.Add<Type>("ViewType", typeof(UI.SettingsMRR.Dictionary.DictionaryList.View));

                    nodes.Add(node);
                }

                #endregion

                #region Настройка календаря

                if (HasOperation(MrrOperations.Calendar))
                {
                    node = new NavigationNodeContext();
                    helper = new DataContextHelper(node)
                    {
                        {"CanAddToFavorite", false},
                        {"CanExpand", false},
                        {"Description", "Настройка календаря"},
                        {"FavoriteInfo", new NavigationNodeFavoriteInfoContext()},
                        {"HandleNavigate", true},
                        {"ImageKey", ""},
                        {"IsVisible", true},
                        {"Key", "ChiefPane"},
                        {"Name", "ChiefPane"},
                        {"NodeId", CalendarWndId},
                        {"OrderId", 2},
                        {"Source", NavigationNodeSourceNames.TaskSource},
                        {"UfName", "Настройка календаря"},
                        {"MultiWindow", true},
                        {"VisualisationProviderName", ""}
                    };

                    helper.Add<Type>("ViewType", typeof(UI.SettingsMRR.Calendar.CalendarView));

                    nodes.Add(node);
                }

                #endregion

                return nodes;

                #endregion
            }
            if (parentNode.NodeId == MacroreportsId)
            {
                #region Папка Макроотчеты

                var nodes = new List<NavigationNodeContext>();
                NavigationNodeContext node;
                DataContextHelper helper;

                if (HasOperation(MrrOperations.ReportNdsAndDiscrepancies))
                {
                    #region Отчеты по НДС и расхожедниям

                    node = new NavigationNodeContext();
                    helper = new DataContextHelper(node)
                    {
                        {"CanAddToFavorite", false},
                        {"CanExpand", false},
                        {"Description", "Отчеты по НДС и расхождениям"},
                        {"FavoriteInfo", new NavigationNodeFavoriteInfoContext()},
                        {"HandleNavigate", true},
                        {"ImageKey", ""},
                        {"IsVisible", true},
                        {"Key", "ChiefPane"},
                        {"Name", "ChiefPane"},
                        {"NodeId", WpfWndId},
                        {"OrderId", 0},
                        {"Source", NavigationNodeSourceNames.TaskSource},
                        {"UfName", "Отчеты по НДС и расхождениям"},
                        {"MultiWindow", false},
                        {"VisualisationProviderName", ""}
                    };

                    helper.Add<Type>("ViewType", typeof(UI.MapWpf.MapView));

                    nodes.Add(node);

                    #endregion
                }

                if (IsInRole(new[] { Constants.SystemPermissions.Operations.RoleManager }))
                {
                    #region Мониторинг WPF
                    node = new NavigationNodeContext();
                    helper = new DataContextHelper(node)
                    {
                        {"CanAddToFavorite", false},
                        {"CanExpand", false},
                        {"Description", "Показатели эффективности"},
                        {"FavoriteInfo", new NavigationNodeFavoriteInfoContext()},
                        {"HandleNavigate", true},
                        {"ImageKey", ""},
                        {"IsVisible", true},
                        {"Key", "ChiefPane"},
                        {"Name", "ChiefPane"},
                        {"NodeId", WpfEffWndId},
                        {"OrderId", 1},
                        {"Source", NavigationNodeSourceNames.TaskSource},
                        {"UfName", "Показатели эффективности"},
                        {"MultiWindow", true},
                        {"VisualisationProviderName", ""}
                    };

                    helper.Add<Type>("ViewType", typeof(UI.EfficiencyMonitoringWpf.CentralApparatView));

                    nodes.Add(node);
                    #endregion
                }

                if (HasOperation(
                    MrrOperations.Macroreports.ReportBackgroundMetricsCountry,
                    MrrOperations.Macroreports.ReportBackgroundMetricsFedRegions,
                    MrrOperations.Macroreports.ReportBackgroundMetricsRegions,
                    MrrOperations.Macroreports.ReportBackgroundMetricsInspections))
                {
                    #region Фоновые показатели
                    node = new NavigationNodeContext();
                    helper = new DataContextHelper(node)
                    {
                        {"CanAddToFavorite", false},
                        {"CanExpand", false},
                        {"Description", "Фоновые показатели"},
                        {"FavoriteInfo", new NavigationNodeFavoriteInfoContext()},
                        {"HandleNavigate", true},
                        {"ImageKey", ""},
                        {"IsVisible", true},
                        {"NodeId", DeclarationListWndId},
                        {"OrderId", 2},
                        {"Source", NavigationNodeSourceNames.TaskSource},
                        {"UfName", "Фоновые показатели"},
                        {"MultiWindow", true},
                        {"VisualisationProviderName", ""}
                    };

                    helper.Add<Type>("ViewType", typeof(UI.Reports.ReportCard.Base.DeclarationStatisticView));

                    nodes.Add(node);
                    #endregion
                }
                return nodes;

                #endregion
            }
            if (parentNode.NodeId == SelectionsWndId)
            {

                var nodes = new List<NavigationNodeContext>();

                #region Список выборок

                if (HasOperation(MrrOperations.SelectionList))
                {
                    var node = new NavigationNodeContext();
                    var helper = new DataContextHelper(node)
                    {
                        {"CanAddToFavorite", false},
                        {"CanExpand", false},
                        {"Description", "Список выборок"},
                        {"FavoriteInfo", new NavigationNodeFavoriteInfoContext()},
                        {"HandleNavigate", true},
                        {"ImageKey", ""},
                        {"IsVisible", true},
                        {"Key", "ChiefPane"},
                        {"Name", "ChiefPane"},
                        {"NodeId", SelectionListWndId},
                        {"OrderId", 0},
                        {"Source", NavigationNodeSourceNames.TaskSource},
                        {"UfName", "Список выборок"},
                        {"MultiWindow", true},
                        {"VisualisationProviderName", ""}
                    };

                    helper.Add<Type>("ViewType", typeof(UI.Selections.SelectionListView));

                    nodes.Add(node);
                }

                #endregion

                #region Список АТ по СФ

                if (HasOperation(MrrOperations.InvoiceClaimList))
                {
                    var node = new NavigationNodeContext();
                    var helper = new DataContextHelper(node)
                    {
                        {"CanAddToFavorite", false},
                        {"CanExpand", false},
                        {"Description", "Список автотребований по СФ"},
                        {"FavoriteInfo", new NavigationNodeFavoriteInfoContext()},
                        {"HandleNavigate", true},
                        {"ImageKey", ""},
                        {"IsVisible", true},
                        {"Key", "InvoiceClaimList"},
                        {"Name", "InvoiceClaimList"},
                        {"NodeId", InvoiceClaimListWndId},
                        {"OrderId", 0},
                        {"Source", NavigationNodeSourceNames.TaskSource},
                        {"UfName", "Список автотребований по СФ"},
                        {"MultiWindow", true},
                        {"VisualisationProviderName", ""}
                    };

                    helper.Add<Type>("ViewType", typeof(UI.Selections.InvoiceClaimList.View));

                    nodes.Add(node);
                }

                #endregion

                return nodes;
            }
            if (parentNode.NodeId == Nds2TaskRootNodeId)
            {
                var nodes = new List<NavigationNodeContext>();
                NavigationNodeContext node;
                DataContextHelper helper;

                #region Технологическое окно с номером версии

                if (_isDiagnosticMode)
                {
                    node = new NavigationNodeContext();
                    helper = new DataContextHelper(node)
                    {
                        {"CanAddToFavorite", false},
                        {"CanExpand", false},
                        {"Description", "Технологическое окно"},
                        {"FavoriteInfo", new NavigationNodeFavoriteInfoContext()},
                        {"HandleNavigate", true},
                        {"ImageKey", ""},
                        {"IsVisible", true},
                        {"Key", "OperatorPane"},
                        {"Name", "OperatorPane"},
                        {"NodeId", DevWndId},
                        {"OrderId", ++orderId},
                        {"Source", NavigationNodeSourceNames.TaskSource},
                        {"UfName", string.Format("[{0}]", _appVersion)},
                        {"MultiWindow", true},
                        {"VisualisationProviderName", ""}
                    };

                    helper.Add<Type>("ViewType", typeof(DemoView));

                    nodes.Add(node);
                }

                #endregion

                #region Окно для обновления версии

                if (VerifyVersion() == VersionVerificationState.UpdateAvailable)
                {
                    node = new NavigationNodeContext();
                    helper = new DataContextHelper(node)
                    {
                        {"CanAddToFavorite", false},
                        {"CanExpand", false},
                        {"Description", "Обновите версию"},
                        {"FavoriteInfo", new NavigationNodeFavoriteInfoContext()},
                        {"HandleNavigate", true},
                        {"ImageKey", ""},
                        {"IsVisible", true},
                        {"Key", "AdminPane"},
                        {"Name", "AdminPane"},
                        {"NodeId", UpdateInstallWndId},
                        {"OrderId", ++orderId},
                        {"Source", NavigationNodeSourceNames.TaskSource},
                        {"UfName", "Обновите версию"},
                        {"MultiWindow", true},
                        {"VisualisationProviderName", ""}
                    };

                    helper.Add<Type>("ViewType", typeof(UI.UpdateInstall.ViewInformation));

                    nodes.Add(node);

                    return nodes;
                }

                #endregion

                # region История изменения версий

                if (HasOperation(MrrOperations.VersionChages))
                {
                    node = new NavigationNodeContext();
                    helper = new DataContextHelper(node)
                    {
                        {"CanAddToFavorite", false},
                        {"CanExpand", false},
                        {"Description", "Изменения в версии"},
                        {"FavoriteInfo", new NavigationNodeFavoriteInfoContext()},
                        {"HandleNavigate", true},
                        {"ImageKey", ""},
                        {"IsVisible", true},
                        {"Key", "VersionHistory"},
                        {"Name", "VersionHistory"},
                        {"NodeId", VersionHistoryNodeId},
                        {"OrderId", ++orderId},
                        {"Source", NavigationNodeSourceNames.TaskSource},
                        {"UfName", "Изменения в версии"},
                        {"MultiWindow", true},
                        {"VisualisationProviderName", ""}
                    };

                    helper.Add<Type>("ViewType", typeof(UI.VersionHistory.VersionHistoryView));

                    nodes.Add(node);
                }

                #endregion

                #region Декларации

                if (HasOperation(MrrOperations.DeclarationList))
                {
                    node = new NavigationNodeContext();
                    helper = new DataContextHelper(node)
                    {
                        {"CanAddToFavorite", false},
                        {"CanExpand", false},
                        {"Description", "Список деклараций"},
                        {"FavoriteInfo", new NavigationNodeFavoriteInfoContext()},
                        {"HandleNavigate", true},
                        {"ImageKey", ""},
                        {"IsVisible", true},
                        {"Key", "ChiefPane"},
                        {"Name", "ChiefPane"},
                        {"NodeId", DeclarationListWndId},
                        {"OrderId", ++orderId},
                        {"Source", NavigationNodeSourceNames.TaskSource},
                        {"UfName", "Декларации"},
                        {"MultiWindow", true},
                        {"VisualisationProviderName", ""}
                    };

                    helper.Add<Type>("ViewType", typeof(UI.Declarations.DeclarationsList.View));

                    nodes.Add(node);
                }

                #endregion Декларации

                #region Выборки

                if (HasOperation(MrrOperations.SelectionList, MrrOperations.InvoiceClaimList))
                {
                    node = new NavigationNodeContext();
                    helper = new DataContextHelper(node)
                    {
                        {"CanAddToFavorite", false},
                        {"CanExpand", true},
                        {"Description", "Выборок"},
                        {"FavoriteInfo", new NavigationNodeFavoriteInfoContext()},
                        {"HandleNavigate", true},
                        {"ImageKey", ""},
                        {"IsVisible", true},
                        {"Key", "ChiefPane"},
                        {"Name", "ChiefPane"},
                        {"NodeId", SelectionsWndId},
                        {"OrderId", ++orderId},
                        {"Source", NavigationNodeSourceNames.TaskSource},
                        {"UfName", "Выборки"},
                        {"MultiWindow", true},
                        {"VisualisationProviderName", ""}
                    };

                    nodes.Add(node);

                }

                #endregion Выборки

                #region Пользовательские задания

                if (HasOperation(new []
                {
                    MrrOperations.UserTask.UserTaskReportByCountry,
                    MrrOperations.UserTask.UserTaskReportByInspections,
                    MrrOperations.UserTask.UserTaskReportByInspector,
                    MrrOperations.UserTask.UserTaskReportByRegion,
                    MrrOperations.UserTask.UserTaskReportByRegionInspection
                }))
                {

                    node = new NavigationNodeContext();
                    helper = new DataContextHelper(node)
                    {
                        {"CanAddToFavorite", false},
                        {"CanExpand", false},
                        {"Description", "Пользовательские задания"},
                        {"FavoriteInfo", new NavigationNodeFavoriteInfoContext()},
                        {"HandleNavigate", true},
                        {"ImageKey", ""},
                        {"IsVisible", true},
                        {"Key", "UserTaskPane"},
                        {"Name", "UserTaskPane"},
                        {"NodeId", UserTaskWndId},
                        {"OrderId", ++orderId},
                        {"Source", NavigationNodeSourceNames.TaskSource},
                        {"UfName", "Пользовательские задания"},
                        {"MultiWindow", true},
                        {"VisualisationProviderName", ""}
                    };

                    helper.Add<Type>("ViewType", typeof(UserTaskListView));

                    nodes.Add(node);
                }

                #endregion

                #region АРМ Инспектора или Окно оперативной работы

                if (HasOperation(MrrOperations.InspectorWorkPlace))
                {
                    node = new NavigationNodeContext();
                    helper = new DataContextHelper(node)
                    {
                        {"CanAddToFavorite", false},
                        {"CanExpand", false},
                        {"Description", "Окно оперативной работы"},
                        {"FavoriteInfo", new NavigationNodeFavoriteInfoContext()},
                        {"HandleNavigate", true},
                        {"ImageKey", ""},
                        {"IsVisible", true},
                        {"Key", "ChiefPane"},
                        {"Name", "ChiefPane"},
                        {"NodeId", ArmInspectorWndId},
                        {"OrderId", ++orderId},
                        {"Source", NavigationNodeSourceNames.TaskSource},
                        {"UfName", "Окно оперативной работы"},
                        {"MultiWindow", true},
                        {"VisualisationProviderName", ""}
                    };

                    helper.Add<Type>("ViewType", typeof(UI.InspectorWorkPlace.DeclarationsList.InspectorDeclarationsView));

                    nodes.Add(node);
                }

                #endregion АРМ Инспектора

                #region Макроотчеты

                if ((IsInRole(new[] { Constants.SystemPermissions.Operations.RoleManager }) ||
                    IsInStructAndRole(new[] {
                    Constants.SystemPermissions.Operations.RoleAnalyst,
                    Constants.SystemPermissions.Operations.RoleMedodologist                    
                    }, "0000") ||
                    IsInStructAndRole(new[] {
                    Constants.SystemPermissions.Operations.RoleMedodologist,
                    Constants.SystemPermissions.Operations.RoleAnalyst
                    }, "9962")) ||
                    HasOperation(MrrOperations.Macroreports.ReportBackgroundMetricsCountry, MrrOperations.Macroreports.ReportBackgroundMetricsFedRegions,
                    MrrOperations.Macroreports.ReportBackgroundMetricsRegions, MrrOperations.Macroreports.ReportBackgroundMetricsInspections))
                {
                    node = new NavigationNodeContext();
                    helper = new DataContextHelper(node)
                    {
                        {"CanAddToFavorite", false},
                        {"CanExpand", true},
                        {"Description", "Макроотчёты"},
                        {"FavoriteInfo", new NavigationNodeFavoriteInfoContext()},
                        {"HandleNavigate", true},
                        {"ImageKey", ""},
                        {"IsVisible", true},
                        {"Key", "ChiefPane"},
                        {"Name", "ChiefPane"},
                        {"NodeId", MacroreportsId},
                        {"OrderId", ++orderId},
                        {"Source", NavigationNodeSourceNames.TaskSource},
                        {"UfName", "Макроотчёты"},
                        {"MultiWindow", true},
                        {"VisualisationProviderName", ""}
                    };

                    nodes.Add(node);
                }

                #endregion Макроотчеты

                #region Список отчетов

                if (HasOperation(
                    MrrOperations.ActAndDecision.ActDecisionReportByCountry,
                    MrrOperations.ActAndDecision.ActDecisionReportByFederalDistrict,
                    MrrOperations.ActAndDecision.ActDecisionReportByRegion,
                    MrrOperations.ActAndDecision.ActDecisionReportBySono))
                {
                    node = new NavigationNodeContext();
                    helper = new DataContextHelper(node)
                    {
                        {"CanAddToFavorite", false},
                        {"CanExpand", false},
                        {"Description", "Список отчетов"},
                        {"FavoriteInfo", new NavigationNodeFavoriteInfoContext()},
                        {"HandleNavigate", true},
                        {"ImageKey", ""},
                        {"IsVisible", true},
                        {"Key", "ChiefPane"},
                        {"Name", "ChiefPane"},
                        {"NodeId", ReportListWndId},
                        {"OrderId", ++orderId},
                        {"Source", NavigationNodeSourceNames.TaskSource},
                        {"UfName", "Отчеты"},
                        {"MultiWindow", true},
                        {"VisualisationProviderName", ""}
                    };

                    helper.Add<Type>("ViewType", typeof(UI.Reports.ReportsList.View));

                    nodes.Add(node);
                }

                #endregion  Список отчетов

                #region Настройки

                if (HasOperation(
                    MrrOperations.AskParams,
                    MrrOperations.ListSettings,
                    MrrOperations.Calendar,
                    MrrOperations.Dictionaries))
                {
                    node = new NavigationNodeContext();
                    helper = new DataContextHelper(node)
                    {
                        {"CanAddToFavorite", false},
                        {"CanExpand", true},
                        {"Description", "Управление"},
                        {"FavoriteInfo", new NavigationNodeFavoriteInfoContext()},
                        {"HandleNavigate", true},
                        {"ImageKey", ""},
                        {"IsVisible", true},
                        {"Key", "ChiefPane"},
                        {"Name", "ChiefPane"},
                        {"NodeId", Nds2ManagementConsoleId},
                        {"OrderId", ++orderId},
                        {"Source", NavigationNodeSourceNames.TaskSource},
                        {"UfName", "Настройки МРР"},
                        {"MultiWindow", true},
                        {"VisualisationProviderName", ""}
                    };

                    nodes.Add(node);
                }


                #endregion

                # region Навигатор

                if (false)// Добавлено, чтобы временно выпилить Навигатор  HasOperationWithAllowInspections(Constants.SystemPermissions.Operations.NavigatorView))
                {
                    node = new NavigationNodeContext();
                    helper = new DataContextHelper(node);

                    helper.Add("CanAddToFavorite", false);
                    helper.Add("CanExpand", false);
                    helper.Add("Description", "Отчет \"Навигатор\"");
                    helper.Add("FavoriteInfo", new NavigationNodeFavoriteInfoContext());
                    helper.Add("HandleNavigate", true);
                    helper.Add("ImageKey", "");  // ?
                    helper.Add("IsVisible", true);
                    helper.Add("Key", "AdminPane");
                    helper.Add("Name", "AdminPane");
                    helper.Add("NodeId", NavigatorWndId);
                    helper.Add("OrderId", ++orderId);
                    helper.Add("Source", NavigationNodeSourceNames.TaskSource);
                    helper.Add("UfName", "Навигатор");
                    helper.Add("MultiWindow", true);
                    helper.Add("VisualisationProviderName", "");
                    helper.Add<Type>("ViewType", typeof(Luxoft.NDS2.Client.UI.Navigator.ViewMaster<Luxoft.NDS2.Client.UI.Navigator.IReportSetupView>));

                    nodes.Add(node);
                }

                # endregion

                //========================================

                return nodes;
            }
            return null;
        }

        /// <summary>
        /// Вызывается инфраструктурой ЕКП отвечающей за управление деревьями.
        /// События дерева связанные с визуализацией.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void VisualizationEventHandler(object sender, NavigationNodeEventArgs e)
        {
        }

        private int _winNum;

        /// <summary>
        /// Вызывается инфраструктурой ЕКП отвечающей за управление деревьями.
        /// Элемент дерева активирован.
        /// </summary>
        /// <param name="connectionContext"></param>
        /// <param name="context"></param>
        /// <param name="callContext"></param>
        /// <returns></returns>
        public IDataContext NavigateEventHandler(IDataContext connectionContext, NavigationNodeContext context, IDataContext callContext)
        {
            //<< apopov 26.1.2016	They must be called from the main UI thread like of an enter point. There are here while more suitable place is not found

            //creates and assigns Application.Current with an instance of GoWpfLicensedApplication if 'Application.Current == null' otherwise GoWPF (see Northwoods.GoWPF.dll) control can be non licensed because its licensing must be inside Application's constructor like in GoWpfLicensedApplication's constructor
            bool isDoneInitialization;
            if (null == ApplicationWpfInitializer.InitAppIfNeeded<WpfApplication>(out isDoneInitialization, ShutdownMode.OnExplicitShutdown))
                throw new InvalidOperationException("The main thread invoker is not initialized properly");

            //>> apopov 26.1.2016	They must be called from the main UI thread like of an enter point. There are here while more suitable place is not found

            var windowsManager = _subsystemWorkItem.Services.Get<IWindowsManagerService>(ensureExists: true);
            var subsystemInfoService = _subsystemWorkItem.Services.Get<ISubsystemInfoService>(ensureExists: true);

            IDataContext resultContext = new DataContext(callContext);

            var helper = new DataContextHelper(context);
            ITypedDataContextItem<Type> viewTypeItem;

            if (!helper.TryGetTypedItem("ViewType", out viewTypeItem))
                return resultContext;

            ITypedDataContextItem<bool> multiWindow;
            if (!helper.TryGetTypedItem("MultiWindow", out multiWindow))
                multiWindow = new TypedDataContextItem<bool>("MultiWindow", true);


            if (!multiWindow.ContextItemValue &&
                windowsManager.Contains(context.NodeId))
                windowsManager.Activate(context.NodeId);
            else
            {
                var id = multiWindow.ContextItemValue ? Guid.NewGuid() : context.NodeId;
                var featureContextBase = new FeatureContextBase(Guid.NewGuid(), context);

                var root =
                    new PresentationContext(id, null, featureContextBase)
                        {
                            WindowTitle =
                                String.IsNullOrEmpty(context.UfName)
                                    ? String.Format("{0} [{1}]", subsystemInfoService.Info.UfName, _winNum)
                                    : context.UfName,

                            WindowDescription =
                                String.IsNullOrEmpty(context.Description) ? String.Empty : context.Description
                        };

                CreateAndShowView(viewTypeItem, windowsManager, callContext, root);
            }
            return resultContext;
        }

        private void CreateAndShowView(ITypedDataContextItem<Type> viewTypeItem, IWindowsManagerService windowsManager,
                                       IDataContext callContext, PresentationContext root)
        {
            Type viewType = viewTypeItem.ContextItemValue;

            object dv1 = Activator.CreateInstance(viewType, root);

            _subsystemWorkItem.Services.Get<IThreadInvoker>(ensureExists: true);    //check only that the main thread invoker is present in services

            var wi = _subsystemWorkItem.WorkItems.AddNew<WorkItem>();
            wi.State["Context"] = callContext;

            InitSubsystemWindowServices(wi);

            wi.SmartParts.Add(dv1, root.Name);

            _winNum++;

            windowsManager.Show(root, wi, dv1);
        }

        private void InitSubsystemWindowServices(WorkItem wi)
        {
            ServiceCallExecutorManagerFactory.InitIfNeeded();
            var services = wi.Services;
            var threadInvoker = services.Get<IThreadInvoker>(true);

            services.Add(ServiceCallExecutorManagerFactory.CurrentManager);
            INotifier notifier = new AlertNotifier(threadInvoker, services.Get<IInstrumentationService>(ensureExists: true));
            services.Add(notifier);
            var logger = services.Get<IClientLogger>(true);
            IExecutorWErrorHandling executorWErrorHandling = new DirectExecutorWErrorHandling(notifier, logger);
            services.Add(executorWErrorHandling);
            IThreadInvokerWErrorHandling threadInvokerWErrorHandling = new ThreadInvokerWErrorHandling(threadInvoker, executorWErrorHandling);
            services.Add(threadInvokerWErrorHandling);
        }


        private void InitAllowedOperations()
        {
            _allowedOperations = LoadOperations();
        }

        private bool HasOperation(params string[] operations)
        {
            _allowedOperations = _allowedOperations ?? LoadOperations();

            return _allowedOperations.Intersect(operations).Any();
        }

        private string[] LoadOperations()
        {
            try
            {
                var result =
                    _isecurity.ValidateAccess(
                        new[]
                        {
                            MrrOperations.InspectorWorkPlace,
                            MrrOperations.ActAndDecision.ActDecisionReportByCountry,
                            MrrOperations.ActAndDecision.ActDecisionReportByFederalDistrict,
                            MrrOperations.ActAndDecision.ActDecisionReportByRegion,
                            MrrOperations.ActAndDecision.ActDecisionReportBySono,
                            MrrOperations.Macroreports.ReportBackgroundMetricsCountry,
                            MrrOperations.Macroreports.ReportBackgroundMetricsFedRegions,
                            MrrOperations.Macroreports.ReportBackgroundMetricsRegions,
                            MrrOperations.Macroreports.ReportBackgroundMetricsInspections,
                            MrrOperations.UserTask.UserTaskReportByCountry,
                            MrrOperations.UserTask.UserTaskReportByInspections,
                            MrrOperations.UserTask.UserTaskReportByInspector,
                            MrrOperations.UserTask.UserTaskReportByRegion,
                            MrrOperations.UserTask.UserTaskReportByRegionInspection,
                            MrrOperations.VersionChages,
                            MrrOperations.DeclarationList,
                            MrrOperations.SelectionList,
                            MrrOperations.InvoiceClaimList,
                            MrrOperations.ReportNdsAndDiscrepancies,
                            MrrOperations.ReportEffectiveness,
                            MrrOperations.AskParams,
                            MrrOperations.ListSettings,
                            MrrOperations.Calendar,
                            MrrOperations.Dictionaries
                        });
                return result.Status == ResultStatus.Success
                    ? result.Result
                    : new string[0];
            }
            catch
            {
                return new string[0];
            }
        }

        private bool IsInRole(IEnumerable<string> roles)
        {
            return
                roles.Intersect(_userRights.Where(ur => ur.PermType == PermissionType.Operation).Select(r => r.Name)).Any();
        }

        /// <summary>
        /// Пользователь входит в подразделение
        /// </summary>
        /// <param name="roles">список ролей</param>
        /// <param name="structCode">код поздразделения</param>
        /// <param name="startsWith">передано только начало подразделения</param>
        /// <returns></returns>
        private bool IsInStructAndRole(IEnumerable<string> roles, string structCode, bool startsWith = false)
        {
            if (startsWith)
            {
                return
                    _userRights.Where(ur => ur.PermType == PermissionType.Operation && roles.Contains(ur.Name))
                        .ToList()
                        .Exists(
                            ur =>
                                structCode == "99"
                                    ? ur.StructContext.StartsWith(structCode) && ur.StructContext != "9901"
                                    : ur.StructContext.StartsWith(structCode));
            }
            return _userRights.Where(ur => ur.PermType == PermissionType.Operation && roles.Contains(ur.Name))
                .ToList()
                .Exists(ur => ur.StructContext == structCode);
        }

        private bool HasAnyRole()
        {
            return _userRights != null && _userRights.Any();
        }

        private VersionVerificationState VerifyVersion()
        {
            return VersionVerificationState.UpToDate;

            #region DN Этот код порезал ММ еще в 2015 году. Возможно, так не было задумано, но никто не хватился

            /*
            var versionCheck = VersionVerificationState.Pending;

            Func<string, string> getMajorVersionPart = versionString =>
            {
                var sb = new StringBuilder();
                var dots = 0;
                foreach (var c in versionString)
                {
                    if (c == '.')
                        ++dots;
                    if (dots > 2)
                        return sb.ToString();
                    sb.Append(c);
                }
                return sb.ToString();
            };

            try
            {
                var versionInfoService =
                    services.Get<IClientContextService>()
                        .CreateCommunicationProxy<IVersionInfoService>(Constants.EndpointName);
                var response = versionInfoService.GetLatest();

                if (response.Status == ResultStatus.Success)
                {
                    if (response.Result.Undefined)
                    {
                        versionCheck = VersionVerificationState.Failed;
                    }
                    else
                    {
                        var clientVersion = getMajorVersionPart(Assembly.GetExecutingAssembly().GetName().Version.ToString());
                        var databaseVersion = getMajorVersionPart(response.Result.VersionNumber);
                        versionCheck = clientVersion == databaseVersion
                            ? VersionVerificationState.UpToDate
                            : VersionVerificationState.UpdateAvailable;
                    }
                }
            }
            catch (Exception)
            {
                versionCheck = VersionVerificationState.Failed;
            }

            return versionCheck;
            */

            #endregion
        }

    }
}

