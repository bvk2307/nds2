﻿using System;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Threading;
using CommonComponents.Instrumentation;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using FLS.CommonComponents.App.Execution;
using FLS.CommonComponents.Lib.Exceptions.Extensions;
using Luxoft.NDS2.Common.Contracts;

namespace Luxoft.NDS2.Client.Infrastructure.ErrorHandling
{
    internal class UnhandledExceptionHandler
    {
        private static UnhandledExceptionHandler s_unhandledExceptionHandler = null;

        private readonly IInstrumentationService _instrumentationService;
        private readonly WeakReference<IUcMessageService> _wrUcMessageService;

        private UnhandledExceptionHandler( IInstrumentationService instrumentationService, IUcMessageService ucMessageService )
        {
            Contract.Requires( instrumentationService != null );
            Contract.Requires( ucMessageService != null );

            _instrumentationService = instrumentationService;
            _wrUcMessageService = new WeakReference<IUcMessageService>(ucMessageService);

	        //apopov 17.2.2016	//TODO!!!   implement for non UI thread's processing
            //AppDomain.CurrentDomain.UnhandledException                  += OnDomainUnhandledException;
            Application.ThreadException                                 += OnAppThreadException;
            ////UIThreadExecutor.CurrentDispatcher.UnhandledExceptionFilter += OnDispatcherUnhandledExceptionFilter;
            UIThreadExecutor.CurrentDispatcher.UnhandledException       += OnDispatcherUnhandledException;
            TaskScheduler.UnobservedTaskException                       += OnTaskUnobservedException;
        }

        public static void InitIfNeeded( IInstrumentationService instrumentationService, IUcMessageService ucMessageService )
        {
            if ( s_unhandledExceptionHandler == null )
            {
                Interlocked.CompareExchange(    //only one signleton will be assigned in any cases
                    ref s_unhandledExceptionHandler, new UnhandledExceptionHandler( instrumentationService, ucMessageService ), null );
            }
        }

        private void OnDomainUnhandledException( object sender, UnhandledExceptionEventArgs e )
        {
            //HandleExceptionCalledByMainThread( (Exception)e.ExceptionObject, TraceEventType.Critical, message: "Unhandled:" );
        }

        private void OnAppThreadException( object sender, ThreadExceptionEventArgs e )
        {
            HandleExceptionCalledByMainThread( e.Exception, TraceEventType.Critical, message: "Main thread:" );
        }

	//apopov 16.2.2016	//DEBUG!!!
        //private void OnDispatcherUnhandledExceptionFilter( object sender, DispatcherUnhandledExceptionFilterEventArgs e )
        //{
        //    HandleExceptionCalledByMainThread( e.Exception, TraceEventType.Error, message: "Main thread:" );
        //}

        private void OnDispatcherUnhandledException( object sender, DispatcherUnhandledExceptionEventArgs e )
        {
            HandleExceptionCalledByMainThread( e.Exception, TraceEventType.Critical, message: "Main thread:", willBeAborted: true );
        }
 
        private void OnTaskUnobservedException( object sender, UnobservedTaskExceptionEventArgs e )
        {
            HandleExceptionCalledByFinalazationThread( e.Exception, TraceEventType.Error, message: "Task thread:" );
        }

        private void HandleExceptionCalledByMainThread( Exception exception, TraceEventType traceEventErrorType, string message = null, 
                                      bool willBeAborted = false, 
                                      [CallerMemberName]
                                      string methodName = null )
        {
            IUcMessageService ucMessageService = null;
            _wrUcMessageService.TryGetTarget( out ucMessageService );

            HandleExceptionCalledByMainThread( _instrumentationService, exception, traceEventErrorType, 
                             message, ucMessageService, willBeAborted, methodName );
        }

        private void HandleExceptionCalledByFinalazationThread( 
            Exception exception, TraceEventType traceEventErrorType, string message = null, bool willBeAborted = false, 
            [CallerMemberName]
            string methodName = null )
        {
            if ( IsAppDomainClosing() )
            {
                LogError( _instrumentationService, exception, traceEventErrorType, message, methodName );
            }
            else
            {
                IUcMessageService ucMessageService = null;
                _wrUcMessageService.TryGetTarget( out ucMessageService );

                ThreadPool.QueueUserWorkItem(
                    instrumentService => HandleExceptionCalledByNonMainThread(
                        (IInstrumentationService)instrumentService, exception, traceEventErrorType,
                        message, ucMessageService, willBeAborted, methodName ), _instrumentationService );
            }
        }

        private static void HandleExceptionCalledByMainThread( 
            IInstrumentationService instrumentationService, Exception exception, TraceEventType traceEventErrorType, 
            string message = null, IUcMessageService ucMessageService = null, bool willBeAborted = false, 
                                      [CallerMemberName]
                                      string methodName = null )
        {
            Exception excLogging = null;
            Exception excHandling = null;
            try
            {
                // ReSharper disable once ExplicitCallerInfoArgument
                excHandling = LogError( instrumentationService, exception, traceEventErrorType, message, methodName );
            }
            catch ( Exception exc ) //catches any exceptions during the own work
            {
                excLogging = exc;
            }
            if ( ucMessageService != null )
            {
                ShowErrorCalledByMainThread( ucMessageService, excHandling ?? exception, 
                    traceEventErrorType, willBeAborted, message, instrumentationService, methodName );

                if ( excLogging != null )
                    ShowErrorCalledByMainThread( ucMessageService, excLogging, TraceEventType.Critical, willBeAborted );
            }
        }

        private static void HandleExceptionCalledByNonMainThread( 
            IInstrumentationService instrumentationService, Exception exception, TraceEventType traceEventErrorType, 
            string message = null, IUcMessageService ucMessageService = null, bool willBeAborted = false, 
                                      [CallerMemberName]
                                      string methodName = null )
        {
            Exception excLogging = null;
            Exception excHandling = null;
            try
            {
                excHandling = LogError( instrumentationService, exception, traceEventErrorType, message, methodName );
            }
            catch ( Exception exc ) //catches any exceptions during the own work
            {
                excLogging = exc;
            }
            if ( ucMessageService != null )
            {
                ShowErrorCalledByNonMainThread( ucMessageService, excHandling ?? exception, 
                    traceEventErrorType, willBeAborted, message, instrumentationService, methodName );

                if ( excLogging != null )
                    ShowErrorCalledByNonMainThread( ucMessageService, excLogging, TraceEventType.Critical, willBeAborted );
            }
        }

        private static void ShowErrorCalledByMainThread( 
            IUcMessageService ucMessageService, Exception exception, TraceEventType traceEventErrorType, bool willBeAborted, 
            string message = null, IInstrumentationService instrumentationService = null,
                                [CallerMemberName] 
                                string methodName = null )
        {
            Exception excShow = null;
            try
            {
                if ( ucMessageService == null )
                    throw new ArgumentNullException( "ucMessageService" );  //to catch by debugger and may be to log with code below
                if ( exception == null )
                    throw new ArgumentNullException( "exception" );          //to catch by debugger and may be to log with code below

                bool isCriticalError = traceEventErrorType == TraceEventType.Critical;

                var messageContext = new MessageInfoContext( 
                    string.Join( " ", willBeAborted ? "Аварийное завершение" : isCriticalError ? "Критическая ошибка" : "Ошибка", 
                                      "в", methodName ), 
                    ErrorMessage( exception, message, addCallStack: isCriticalError ), MessageCategory.Error )
                {
                    MessageBoxIcon = MessageBoxIcon.Error, AutoCloseDelay = TimeSpan.MaxValue,
                    SaveToClientLog = true, 
                    DisplayMode = isCriticalError ? MessageDisplayMode.ShowDetails : MessageDisplayMode.ShowAlert
                    //ParentlessForm = true,	//does not have effect
                    //DialogResult = willBeAborted ? DialogResult.Abort : DialogResult.OK,  //is not used in UcMessageService
                    //FormBorderStyle = FormBorderStyle.Sizable   //is not used in UcMessageService
                    //FormStartPosition = FormStartPosition.CenterScreen    //is not used in UcMessageService
                };
                ucMessageService.ShowError( messageContext );
            }
            catch ( Exception exc ) //catches any exceptions during the own work
            {
                excShow = exc;
            }
            try
            {
                if ( excShow != null && instrumentationService != null )
                    LogError( instrumentationService, excShow, TraceEventType.Critical );
            }
            catch ( Exception ) //catches any exceptions during the own work
            {
            }
        }

        private static void ShowErrorCalledByNonMainThread( 
            IUcMessageService ucMessageService, Exception exception, TraceEventType traceEventErrorType, bool willBeAborted, 
            string message = null, IInstrumentationService instrumentationService = null,
                                [CallerMemberName] 
                                string methodName = null )
        {
            UIThreadExecutor.CurrentDispatcher.BeginInvoke( 
                (Action<IUcMessageService, Exception, TraceEventType, bool, string, IInstrumentationService, string>)
                    ShowErrorCalledByMainThread, 
                DispatcherPriority.Send, //uses the highest priority
                ucMessageService, exception, traceEventErrorType, willBeAborted, message, instrumentationService, methodName );
        }


        private static Exception LogError( IInstrumentationService instrumentationService, Exception exception, 
            TraceEventType traceEventErrorType, string message = null, 
            [CallerMemberName] 
            string methodName = null )
        {
            Contract.Ensures( Contract.Result<Exception>() != null );

            if ( instrumentationService == null )
                throw new ArgumentNullException( "instrumentationService" );     //to catch by debugger
            if ( exception == null )
                throw new ArgumentNullException( "exception" );                 //to catch by debugger

            IProfilingEntry le = instrumentationService.CreateProfilingEntry( traceEventErrorType, (int)InstrumentationLevel.Normal, 
                                                        Constants.Instrumentation.CLIENT_PROFILE_TYPE_NAME, messagePriority: 100 );

            Exception mainException = AddException( le, exception );
            le.MessageText = ErrorMessage( mainException, message );
            le.AddMonitoringItem( "Method name", methodName );

            instrumentationService.Write( le );

            return mainException;
        }

        private static string ErrorMessage( Exception exception, string message = null, bool addCallStack = false )
        {
            return ExceptionExtensions.ToStringEx(exception, message, noStack: !addCallStack );
        }

        private static Exception AddException( IProfilingEntry rec, Exception exception )
        {
            Contract.Requires( rec != null );
            Contract.Requires( exception != null );
            Contract.Ensures( Contract.Result<Exception>() != null );

            Exception firstException = null;
            int num = 0;
            foreach (Exception exc in ExceptionExtensions.ForAllOriginal(exception))
            {
                if ( firstException == null )
                    firstException = exc;
                Exception excCur = exc;
                int subnum = 0;
                do
                {
                    bool isTopException = 0 == subnum++;
                    string fullNumber = isTopException
                        ? IntegralToString( num ) : string.Join( ".", IntegralToString( num ), IntegralToString( subnum ) );
                    string exceptionName = isTopException ? "Exception" : "Inner exception";
                    rec.AddMonitoringItem( string.Join( " ", exceptionName, "#", fullNumber ), excCur );
                } while ( null != ( excCur = excCur.InnerException ) );

                ++num;
            }
            return firstException;
        }

        private static bool IsAppDomainClosing()
        {
            bool isClosing = AppDomain.CurrentDomain.IsFinalizingForUnload() || Environment.HasShutdownStarted;

            return isClosing;
        }

        private static string IntegralToString( int integralValue )
        {
            return integralValue.ToString( CultureInfo.InvariantCulture );
        }
   }
}