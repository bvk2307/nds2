﻿using System.Threading.Tasks;

namespace Luxoft.NDS2.Client.Infrastructure
{
    /// <summary> A dispatcher for a batch of calls to a some server service. </summary>
    /// <typeparam name="TParam"></typeparam>
    /// <typeparam name="TInstanceKey"></typeparam>
    /// <typeparam name="TContextKey"></typeparam>
    public interface IServiceCallBatchDispatcher<in TParam, in TInstanceKey, in TContextKey>
    {
        /// <summary> Gets a <see cref="T:System.Threading.Tasks.Task">Task</see> that represents completion of the dataflow mesh. </summary>
        Task Completion { get; }

        /// <summary> Starts the batch execution. </summary>
        /// <param name="keyParameters"> A request parameter. </param>
        /// <param name="instanceKey"> A call instance key that will be returned into the post processing. </param>
        /// <param name="contextKey"> An execution context key that will be returned into the post processing. </param>
        void Start( TParam keyParameters, TInstanceKey instanceKey, TContextKey contextKey );
    }
}