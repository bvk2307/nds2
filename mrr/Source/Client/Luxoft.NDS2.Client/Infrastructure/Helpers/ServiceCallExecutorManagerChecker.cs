﻿using Luxoft.NDS2.Common.Helpers.Events;

namespace Luxoft.NDS2.Client.Infrastructure.Helpers
{
    /// <summary> A checker called with delay by GC finalizer in a pool thread. It warns if all service call executors should be returned alread but not yet. </summary>
    public sealed class ServiceCallExecutorManagerChecker : DelayedFinalizingActionWCount
    {
        protected override void OnDelayedActionCountReset()
        {
	//apopov 11.7.2016	//DEBUG!!!
            System.Diagnostics.Debug.WriteLine( ">>> --- OnDelayedActionCountReset()" );
        }
    }
}