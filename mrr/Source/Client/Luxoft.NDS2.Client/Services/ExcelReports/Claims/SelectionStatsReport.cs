﻿using CommonComponents.Catalog;
using DocumentFormat.OpenXml.Spreadsheet;
using Luxoft.NDS2.Common.Contracts.DTO.Claims;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.Services.ExcelReports.Claims
{
    public class SelectionStatsReport : ExcelTemplateReportBase
    {
        private readonly List<SelectionClaimCompare> _data;

        public SelectionStatsReport(ICatalogService catalogService, string targetPath, List<SelectionClaimCompare> data)
            : base(catalogService, @"Bin/Templates/NDS22600131SelectionStats.xlsx", targetPath)
        {
            _data = data;
        }

        private const int FirstDataLine = 5;

        protected override void WriteData(
            Worksheet sheet,
            uint numberStyleIndex,
            uint amountStyleIndex)
        {
            var index = FirstDataLine;

            var sheetData = sheet.GetFirstChild<SheetData>();
            uint rowIndexLast = GetRowIndexLast(sheetData);

            foreach (var item in _data)
            {
                var row = sheet.RowAppend(index, rowIndexLast);

                var c = 1;

                row.Cell(c).SetValue(item.LineNumber, CellValues.Number, numberStyleIndex);
                row.Cell(++c).SetValue(item.DocId, CellValues.Number, numberStyleIndex);
                row.Cell(++c).SetValue(item.TaxPeriod, CellValues.String);
                row.Cell(++c).SetValue(item.RegionCode, CellValues.String);
                row.Cell(++c).SetValue(item.InspectionCode, CellValues.String);

                row.Cell(++c).SetValue(item.Inn, CellValues.String);
                row.Cell(++c).SetValue(item.Kpp, CellValues.String);

                #region Данные по выборке сформированной 1-ой стороне

                row.Cell(++c).SetValue(item.Side1TotalAmount, CellValues.Number, amountStyleIndex);
                row.Cell(++c).SetValue(item.Side1TotalQuantity, CellValues.Number, numberStyleIndex);

                row.Cell(++c).SetValue(item.Side1GapAmount, CellValues.Number, amountStyleIndex);
                row.Cell(++c).SetValue(item.Side1GapQuantity, CellValues.Number, numberStyleIndex);

                row.Cell(++c).SetValue(item.Side1NdsAmount, CellValues.Number, amountStyleIndex);
                row.Cell(++c).SetValue(item.Side1NdsQuantity, CellValues.Number, numberStyleIndex);

                #endregion

                #region Данные по выборке сформированной 2-ой стороне

                row.Cell(++c).SetValue(item.Side2TotalAmount, CellValues.Number, amountStyleIndex);
                row.Cell(++c).SetValue(item.Side2TotalQuantity, CellValues.Number, numberStyleIndex);

                row.Cell(++c).SetValue(item.Side2GapAmount, CellValues.Number, amountStyleIndex);
                row.Cell(++c).SetValue(item.Side2GapQuantity, CellValues.Number, numberStyleIndex);

                row.Cell(++c).SetValue(item.Side2NdsAmount, CellValues.Number, amountStyleIndex);
                row.Cell(++c).SetValue(item.Side2NdsQuantity, CellValues.Number, numberStyleIndex);

                #endregion

                #region Агрегированные данные по выборкам

                row.Cell(++c).SetValue(item.TotalAmount, CellValues.Number, amountStyleIndex);
                row.Cell(++c).SetValue(item.TotalQuantity, CellValues.Number, numberStyleIndex);

                row.Cell(++c).SetValue(item.GapAmount, CellValues.Number, amountStyleIndex);
                row.Cell(++c).SetValue(item.GapQuantity, CellValues.Number, numberStyleIndex);

                row.Cell(++c).SetValue(item.NdsAmount, CellValues.Number, amountStyleIndex);
                row.Cell(++c).SetValue(item.NdsQuantity, CellValues.Number, numberStyleIndex);

                #endregion

                #region Данные по АТ сформированным на основании выборок

                row.Cell(++c).SetValue(item.ClaimTotalAmount, CellValues.Number, amountStyleIndex);
                row.Cell(++c).SetValue(item.ClaimTotalQuantity, CellValues.Number, numberStyleIndex);

                row.Cell(++c).SetValue(item.ClaimGapAmount, CellValues.Number, amountStyleIndex);
                row.Cell(++c).SetValue(item.ClaimGapQuantity, CellValues.Number, numberStyleIndex);

                row.Cell(++c).SetValue(item.ClaimNdsAmount, CellValues.Number, amountStyleIndex);
                row.Cell(++c).SetValue(item.ClaimNdsQuantity, CellValues.Number, numberStyleIndex);

                #endregion
                
                ++index;
            }
        }
    }
}
