﻿using System;
using System.Collections.Generic;
using System.Linq;
using CommonComponents.Catalog;
using Luxoft.NDS2.Common.Contracts.DTO.Claims;
using Luxoft.NDS2.Common.Excel.ExcelPattern;
using Luxoft.NDS2.Common.Excel.ReportExport;

namespace Luxoft.NDS2.Client.Services.ExcelReports.Claims
{
    /// <summary>Класс для выгрузки в Excel отчета 'Статистика по выборкам'"</summary>
    /// <remarks>Шаблон NDS22600131SelectionStats.xslx</remarks>
    public sealed class SelectionStatsExporter : CustomExporter
    {
        private static readonly string Patternname;

        static SelectionStatsExporter()
        {
            Patternname = @"Bin/Templates/NDS22600131SelectionStats.xlsx";
        }

        private readonly ICatalogService _catalogService;

        /// <summary>Конструктор класса для выгрузки отчета</summary>
        /// <param name="catalogService">Сервис доступа к каталогу для чтения Excel-файла паттерна отчета</param>
        /// <seealso cref="ICatalogService"></seealso>
        public SelectionStatsExporter(ICatalogService catalogService)
        {
            _catalogService = catalogService;
            Datacontainer = new List<IExporterDataContainer>
            {
                new ExporterDataContainer<SelectionClaimCompare>() { Datarownumber = 5 }
            };
        }

        /// <summary>Первый лист</summary>
        /// <param name="items">Коллекция объектов с данными</param>
        /// <seealso cref="IList{CompletedBySonoEmployees}"></seealso>
        /// <param name="hiddenColumns">Список колонок, которые необходимо скрыть, null если ничего скрывать не надо</param>
        /// <seealso cref="IList{Strign}"></seealso>
        /// <exception cref="ArgumentNullException"></exception>
        public void AddWorksheetData(IList<SelectionClaimCompare> items, IList<string> hiddenColumns = null)
        {
            if (items == null)
                throw new ArgumentNullException("items");

            Datacontainer[0].IsVisible = true;
            ((ExporterDataContainer<SelectionClaimCompare>)Datacontainer[0]).Items = items;
            Datacontainer[0].Conformity =
                new List<FieldDefinition>
                {
                    Helper<SelectionClaimCompare>.Create(x => x.LineNumber, hiddenColumns),
                    Helper<SelectionClaimCompare>.Create(x => x.DocId, hiddenColumns),
                    Helper<SelectionClaimCompare>.Create(x => x.TaxPeriod, hiddenColumns),
                    Helper<SelectionClaimCompare>.Create(x => x.RegionCode, hiddenColumns),
                    Helper<SelectionClaimCompare>.Create(x => x.InspectionCode, hiddenColumns),
                    Helper<SelectionClaimCompare>.Create(x => x.Inn, hiddenColumns),
                    Helper<SelectionClaimCompare>.Create(x => x.Kpp, hiddenColumns),
                    Helper<SelectionClaimCompare>.Create(x => x.Side1TotalAmount, hiddenColumns),
                    Helper<SelectionClaimCompare>.Create(x => x.Side1TotalQuantity, hiddenColumns),
                    Helper<SelectionClaimCompare>.Create(x => x.Side1GapAmount, hiddenColumns),
                    Helper<SelectionClaimCompare>.Create(x => x.Side1GapQuantity, hiddenColumns),
                    Helper<SelectionClaimCompare>.Create(x => x.Side1NdsAmount, hiddenColumns),
                    Helper<SelectionClaimCompare>.Create(x => x.Side1NdsQuantity, hiddenColumns),
                    Helper<SelectionClaimCompare>.Create(x => x.Side2TotalAmount, hiddenColumns),
                    Helper<SelectionClaimCompare>.Create(x => x.Side2TotalQuantity, hiddenColumns),
                    Helper<SelectionClaimCompare>.Create(x => x.Side2GapAmount, hiddenColumns),
                    Helper<SelectionClaimCompare>.Create(x => x.Side2GapQuantity, hiddenColumns),
                    Helper<SelectionClaimCompare>.Create(x => x.Side2NdsAmount, hiddenColumns),
                    Helper<SelectionClaimCompare>.Create(x => x.Side2NdsQuantity, hiddenColumns),
                    Helper<SelectionClaimCompare>.Create(x => x.TotalAmount, hiddenColumns),
                    Helper<SelectionClaimCompare>.Create(x => x.TotalQuantity, hiddenColumns),
                    Helper<SelectionClaimCompare>.Create(x => x.GapAmount, hiddenColumns),
                    Helper<SelectionClaimCompare>.Create(x => x.GapQuantity, hiddenColumns),
                    Helper<SelectionClaimCompare>.Create(x => x.NdsAmount, hiddenColumns),
                    Helper<SelectionClaimCompare>.Create(x => x.NdsQuantity, hiddenColumns),
                    Helper<SelectionClaimCompare>.Create(x => x.ClaimTotalAmount, hiddenColumns),
                    Helper<SelectionClaimCompare>.Create(x => x.ClaimTotalQuantity, hiddenColumns),
                    Helper<SelectionClaimCompare>.Create(x => x.ClaimGapAmount, hiddenColumns),
                    Helper<SelectionClaimCompare>.Create(x => x.ClaimGapQuantity, hiddenColumns),
                    Helper<SelectionClaimCompare>.Create(x => x.ClaimNdsAmount, hiddenColumns),
                    Helper<SelectionClaimCompare>.Create(x => x.ClaimNdsQuantity, hiddenColumns)
                };
        }

        /// <summary>Выгрузка в Excel</summary>
        /// <param name="filename">Путь и имя файла отчета, который будет создан этим классом</param>
        /// <seealso cref="String"></seealso>
        public void Export(string filename)
        {
            ExportExcel(filename, Patternname, _catalogService
           , (dc, exporter) =>
           {
               if (dc.Items != null)
                   exporter.Export(((ExporterDataContainer<SelectionClaimCompare>)dc).Items, dc.Conformity, dc.Datarownumber, 1);
           }
         );
        }
    }
}
