﻿using System;
using System.Collections.Generic;
using System.Linq;
using CommonComponents.Catalog;
using Luxoft.NDS2.Common.Contracts.DTO.Claims;
using Luxoft.NDS2.Common.Excel.ExcelPattern;
using Luxoft.NDS2.Common.Excel.ReportExport;

namespace Luxoft.NDS2.Client.Services.ExcelReports.Claims
{
    /// <summary>Класс для выгрузки в Excel отчета 'список АТ'"</summary>
    /// <remarks>Шаблон NDS22600132InvoiceClaimList.xslx</remarks>
    public sealed class InvoiceClaimListExporter : CustomExporter
    {
        private static readonly string Patternname;

        static InvoiceClaimListExporter()
        {
            Patternname = @"Bin/Templates/NDS22600132InvoiceClaimList.xlsx";
        }

        private readonly ICatalogService _catalogService;

        /// <summary>Конструктор класса для выгрузки отчета</summary>
        /// <param name="catalogService">Сервис доступа к каталогу для чтения Excel-файла паттерна отчета</param>
        /// <seealso cref="ICatalogService"></seealso>
        public InvoiceClaimListExporter(ICatalogService catalogService)
        {
            _catalogService = catalogService;
            Datacontainer = new List<IExporterDataContainer>
            {
                new ExporterDataContainer<InvoiceClaimReportItem>() { Datarownumber = 5 }
            };
        }

        /// <summary>Первый лист</summary>
        /// <param name="items">Коллекция объектов с данными</param>
        /// <param name="hiddenColumns">Список колонок, которые необходимо скрыть, null если ничего скрывать не надо</param>
        public void AddWorksheetData(IList<InvoiceClaimReportItem> items, IList<string> hiddenColumns = null)
        {
            if (items == null)
                throw new ArgumentNullException("items");

            Datacontainer[0].IsVisible = true;
            ((ExporterDataContainer<InvoiceClaimReportItem>)Datacontainer[0]).Items = items;
            Datacontainer[0].Conformity =
                new List<FieldDefinition>
                {
                    Helper<InvoiceClaimReportItem>.Create(x => x.RegionCode, hiddenColumns),
                    Helper<InvoiceClaimReportItem>.Create(x => x.SonoCode, hiddenColumns),

                    Helper<InvoiceClaimReportItem>.Create(x => x.DocId, hiddenColumns),
                    Helper<InvoiceClaimReportItem>.Create(x => x.CreateDate, hiddenColumns),
                    Helper<InvoiceClaimReportItem>.Create(x => x.Side, hiddenColumns),

                    Helper<InvoiceClaimReportItem>.Create(x => x.ReceivedBySeodDate, hiddenColumns),
                    Helper<InvoiceClaimReportItem>.Create(x => x.SeodRegNumber, hiddenColumns),
                    Helper<InvoiceClaimReportItem>.Create(x => x.SeodDocDate, hiddenColumns),
                    Helper<InvoiceClaimReportItem>.Create(x => x.SeodAccepted, hiddenColumns),
                    Helper<InvoiceClaimReportItem>.Create(x => x.SendToTaxpayerDate, hiddenColumns),
                    Helper<InvoiceClaimReportItem>.Create(x => x.DeliveryDate, hiddenColumns),
                    Helper<InvoiceClaimReportItem>.Create(x => x.DeliveryType, hiddenColumns),
                    Helper<InvoiceClaimReportItem>.Create(x => x.CloseDate, hiddenColumns),

                    Helper<InvoiceClaimReportItem>.Create(x => x.Status, hiddenColumns),
                    Helper<InvoiceClaimReportItem>.Create(x => x.StatusDate, hiddenColumns),
                    Helper<InvoiceClaimReportItem>.Create(x => x.DeclRegNumber, hiddenColumns),
                    Helper<InvoiceClaimReportItem>.Create(x => x.AskNumber, hiddenColumns),
                    Helper<InvoiceClaimReportItem>.Create(x => x.PeriodCode, hiddenColumns),
                    Helper<InvoiceClaimReportItem>.Create(x => x.FiscalYear, hiddenColumns),
                    Helper<InvoiceClaimReportItem>.Create(x => x.Inn, hiddenColumns),
                    Helper<InvoiceClaimReportItem>.Create(x => x.DeclarationType, hiddenColumns),

                    Helper<InvoiceClaimReportItem>.Create(x => x.Nds1735, hiddenColumns),
                    Helper<InvoiceClaimReportItem>.Create(x => x.Nds1731, hiddenColumns),

                    Helper<InvoiceClaimReportItem>.Create(x => x.TaxBaseAmnt, hiddenColumns),
                    Helper<InvoiceClaimReportItem>.Create(x => x.NdsCalculated, hiddenColumns),
                    Helper<InvoiceClaimReportItem>.Create(x => x.DeductionSum, hiddenColumns),
                    Helper<InvoiceClaimReportItem>.Create(x => x.NdsSum, hiddenColumns),
                    Helper<InvoiceClaimReportItem>.Create(x => x.DeductionPercent, hiddenColumns),
                    Helper<InvoiceClaimReportItem>.Create(x => x.TaxBurdenPercent, hiddenColumns),

                    Helper<InvoiceClaimReportItem>.Create(x => x.Subscriber, hiddenColumns),
                    Helper<InvoiceClaimReportItem>.Create(x => x.SubscriberType, hiddenColumns),

                    Helper<InvoiceClaimReportItem>.Create(x => x.BuyerDiscrepancyAmount, hiddenColumns),
                    Helper<InvoiceClaimReportItem>.Create(x => x.BuyerDiscrepancyCount, hiddenColumns),
                    Helper<InvoiceClaimReportItem>.Create(x => x.BuyerGapDiscrepancyAmt, hiddenColumns),
                    Helper<InvoiceClaimReportItem>.Create(x => x.BuyerGapDiscrepancyQty, hiddenColumns),
                    Helper<InvoiceClaimReportItem>.Create(x => x.BuyerNdsDiscrepancyAmt, hiddenColumns),
                    Helper<InvoiceClaimReportItem>.Create(x => x.BuyerNdsDiscrepancyQty, hiddenColumns),

                    Helper<InvoiceClaimReportItem>.Create(x => x.SellerDiscrepancyAmount, hiddenColumns),
                    Helper<InvoiceClaimReportItem>.Create(x => x.SellerDiscrepancyCount, hiddenColumns),
                    Helper<InvoiceClaimReportItem>.Create(x => x.SellerGapDiscrepancyAmt, hiddenColumns),
                    Helper<InvoiceClaimReportItem>.Create(x => x.SellerGapDiscrepancyQty, hiddenColumns),
                    Helper<InvoiceClaimReportItem>.Create(x => x.SellerNdsDiscrepancyAmt, hiddenColumns),
                    Helper<InvoiceClaimReportItem>.Create(x => x.SellerNdsDiscrepancyQty, hiddenColumns),

                    Helper<InvoiceClaimReportItem>.Create(x => x.Sur, hiddenColumns),
                    Helper<InvoiceClaimReportItem>.Create(x => x.DiscrepancySign, hiddenColumns),
                    Helper<InvoiceClaimReportItem>.Create(x => x.HasActiveAccount, hiddenColumns)
                };
        }

        /// <summary>Выгрузка в Excel</summary>
        /// <param name="filename">Путь и имя файла отчета, который будет создан этим классом</param>
        /// <seealso cref="String"></seealso>
        public void Export(string filename)
        {
            ExportExcel(filename, Patternname, _catalogService
           , (dc, exporter) =>
           {
               if (dc.Items != null)
                   exporter.Export(((ExporterDataContainer<InvoiceClaimReportItem>)dc).Items, dc.Conformity, dc.Datarownumber, 1);
           }
         );
        }
    }
}
