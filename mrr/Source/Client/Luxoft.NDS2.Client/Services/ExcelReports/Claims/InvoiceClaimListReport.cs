﻿using System;
using CommonComponents.Catalog;
using DocumentFormat.OpenXml.Spreadsheet;
using Luxoft.NDS2.Common.Contracts.DTO.Claims;
using System.Collections.Generic;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;

namespace Luxoft.NDS2.Client.Services.ExcelReports.Claims
{
    public sealed class InvoiceClaimListReport : ExcelTemplateReportBase
    {
        private readonly List<InvoiceClaimReportItem> _data;

        public InvoiceClaimListReport(ICatalogService catalogService, string targetPath, List<InvoiceClaimReportItem> data)
            : base(catalogService, @"Bin/Templates/NDS22600132InvoiceClaimList.xlsx", targetPath)
        {
            _data = data;
        }

        private const int FirstDataLine = 5;

        protected override void WriteData(
            Worksheet sheet,
            uint numberStyleIndex,
            uint amountStyleIndex)
        {
            var index = FirstDataLine;

            var sheetData = sheet.GetFirstChild<SheetData>();
            uint rowIndexLast = GetRowIndexLast(sheetData);

            foreach (var item in _data)
            {
                var row = sheet.RowAppend(index, rowIndexLast);

                var c = 1;

                row.Cell(c).SetValue(item.RegionCode, CellValues.String);
                row.Cell(++c).SetValue(item.SonoCode, CellValues.String);
                
                row.Cell(++c).SetValue(item.DocId, CellValues.String);
                row.Cell(++c).SetValue(item.CreateDate.ToShortDateString(), CellValues.String);
                row.Cell(++c).SetValue(item.Side, CellValues.Number, numberStyleIndex);

                row.Cell(++c).SetValue(item.ReceivedBySeodDate == null ? null : ((DateTime)item.ReceivedBySeodDate).ToShortDateString(), CellValues.String);
                row.Cell(++c).SetValue(item.SeodRegNumber, CellValues.Number, numberStyleIndex);
                row.Cell(++c).SetValue(item.SeodDocDate == null ? null : ((DateTime)item.SeodDocDate).ToShortDateString(), CellValues.String);
                row.Cell(++c).SetValue(item.SeodAccepted, CellValues.Number, numberStyleIndex);
                row.Cell(++c).SetValue(item.SendToTaxpayerDate == null ? null : ((DateTime)item.SendToTaxpayerDate).ToShortDateString(), CellValues.String);
                row.Cell(++c).SetValue(item.DeliveryDate == null ? null : ((DateTime)item.DeliveryDate).ToShortDateString(), CellValues.String);
                row.Cell(++c).SetValue(item.DeliveryType, CellValues.String);
                row.Cell(++c).SetValue(item.CloseDate == null ? null : ((DateTime)item.CloseDate).ToShortDateString(), CellValues.String);

                row.Cell(++c).SetValue(item.Status, CellValues.String);
                row.Cell(++c).SetValue(item.StatusDate == null ? null : ((DateTime)item.StatusDate).ToShortDateString(), CellValues.String);
                row.Cell(++c).SetValue(item.DeclRegNumber, CellValues.Number, numberStyleIndex);
                row.Cell(++c).SetValue(item.AskNumber, CellValues.Number, numberStyleIndex);
                row.Cell(++c).SetValue(item.PeriodCode, CellValues.Number, numberStyleIndex);
                row.Cell(++c).SetValue(item.FiscalYear, CellValues.Number, numberStyleIndex);
                row.Cell(++c).SetValue(item.Inn, CellValues.String);
                row.Cell(++c).SetValue(item.DeclarationType, CellValues.Number, numberStyleIndex);
                
                row.Cell(++c).SetValue(item.Nds1735, CellValues.Number, amountStyleIndex);
                row.Cell(++c).SetValue(item.Nds1731, CellValues.Number, amountStyleIndex);
                
                row.Cell(++c).SetValue(item.TaxBaseAmnt, CellValues.Number, amountStyleIndex);
                row.Cell(++c).SetValue(item.NdsCalculated, CellValues.Number, amountStyleIndex);
                row.Cell(++c).SetValue(item.DeductionSum, CellValues.Number, amountStyleIndex);
                row.Cell(++c).SetValue(item.NdsSum, CellValues.Number, amountStyleIndex);
                row.Cell(++c).SetValue(item.DeductionPercent, CellValues.Number, amountStyleIndex);
                row.Cell(++c).SetValue(item.TaxBurdenPercent, CellValues.Number, amountStyleIndex);

                row.Cell(++c).SetValue(item.Subscriber, CellValues.String);
                row.Cell(++c).SetValue(item.SubscriberType, CellValues.Number, numberStyleIndex);

                row.Cell(++c).SetValue(item.BuyerDiscrepancyAmount, CellValues.Number, amountStyleIndex);
                row.Cell(++c).SetValue(item.BuyerDiscrepancyCount, CellValues.Number, numberStyleIndex);
                row.Cell(++c).SetValue(item.BuyerGapDiscrepancyAmt, CellValues.Number, amountStyleIndex);
                row.Cell(++c).SetValue(item.BuyerGapDiscrepancyQty, CellValues.Number, numberStyleIndex);
                row.Cell(++c).SetValue(item.BuyerNdsDiscrepancyAmt, CellValues.Number, amountStyleIndex);
                row.Cell(++c).SetValue(item.BuyerNdsDiscrepancyQty, CellValues.Number, numberStyleIndex);

                row.Cell(++c).SetValue(item.SellerDiscrepancyAmount, CellValues.Number, amountStyleIndex);
                row.Cell(++c).SetValue(item.SellerDiscrepancyCount, CellValues.Number, numberStyleIndex);
                row.Cell(++c).SetValue(item.SellerGapDiscrepancyAmt, CellValues.Number, amountStyleIndex);
                row.Cell(++c).SetValue(item.SellerGapDiscrepancyQty, CellValues.Number, numberStyleIndex);
                row.Cell(++c).SetValue(item.SellerNdsDiscrepancyAmt, CellValues.Number, amountStyleIndex);
                row.Cell(++c).SetValue(item.SellerNdsDiscrepancyQty, CellValues.Number, numberStyleIndex);

                row.Cell(++c).SetValue(item.Sur, CellValues.String);
                row.Cell(++c).SetValue(item.DiscrepancySign, CellValues.Number, numberStyleIndex);
                row.Cell(++c).SetValue(item.HasActiveAccount, CellValues.Number,numberStyleIndex);

                ++index;
            }
        }
    }
}
