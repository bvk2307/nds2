﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CommonComponents.Catalog;
using CommonComponents.Utils.Async;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Luxoft.NDS2.Common.Contracts.DTO.Claims;

namespace Luxoft.NDS2.Client.Services.ExcelReports.Claims
{
    class TaxMonitoringReport
    {
        private readonly ICatalogService _catalogService;
        private readonly string _targetPath;
        public const string TemplatePath = @"Bin/Templates/NDS22600133TaxMonitoring.xlsx";

        private bool _isPrepared;

        private readonly Dictionary<int, Action<Row, TaxMonitoringReportItem>> _partBuilders;

        public TaxMonitoringReport(ICatalogService catalogService, string targetPath)
        {
            _catalogService = catalogService;
            _targetPath = targetPath;

            _partBuilders = new Dictionary<int, Action<Row, TaxMonitoringReportItem>>
            {
                {1, BuildChapter8},
                {2, BuildChapter81},
                {3, BuildChapter9},
                {4, BuildChapter91},
                {5, BuildChapter10},
                {6, BuildChapter11},
                {7, BuildChapter12},
                {8, BuildUnconfirmed},
            };
        }

        public void PrepareReportFile()
        {
            if (_isPrepared)
                return;

            if (File.Exists(_targetPath))
                File.Delete(_targetPath);

            var address = new CatalogAddress(
                CatalogAddressSchemas.LogicalCatalog,
                "RoleCatalog",
                "NDS2Subsystem",
                "FileSystems",
                TemplatePath);

            using (var template = _catalogService.CreateInstance<Stream>(address))
            {
                using (var target = File.Create(_targetPath))
                {
                    template.Seek(0, SeekOrigin.Begin);
                    template.CopyTo(target);
                    target.Flush();
                    target.Close();
                    _isPrepared = true;
                }
            }
        }

        public void BuildReportPart(int part, List<TaxMonitoringReportItem> data)
        {
            if (!_isPrepared)
                throw new Exception("Файл отчета не подготовлен к выводу");

            var worker = new AsyncWorker<bool>();
            worker.DoWork += (sender, args) => WriteReportPart(part, data);
            worker.Complete += (sender, args) =>
            {
                var handler = Complete;
                if (handler != null)
                    handler(this, new IntEventArg(part));
            };
            worker.Failed += (sender, args) =>
            {
                var handler = Failed;
                if (handler != null)
                    handler(this, new IntEventArg(part));
            };
            worker.Start();
        }

        public void CleanUpReport()
        {
            using (var doc = SpreadsheetDocument.Open(_targetPath, true))
            {
                var list = _emptyParts.Select(p => doc.WorkbookPart.Workbook.Sheets.OfType<Sheet>().Skip(p - 1).First()).ToList();
                foreach (var sheet in list)
                {
                    doc.WorkbookPart.Workbook.Sheets.RemoveChild(sheet);
                }
                doc.Close();
            }            
        }

        private readonly List<int> _emptyParts = new List<int>();

        private void WriteReportPart(int part, IEnumerable<TaxMonitoringReportItem> data)
        {
            if (data == null || !data.Any())
            {
                _emptyParts.Add(part);
                return;
            }
            using (var doc = SpreadsheetDocument.Open(_targetPath, true))
            {
                var sheet = doc.WorkbookPart.Workbook.Sheets.OfType<Sheet>().Skip(part - 1).First();
                var worksheet = ((WorksheetPart)doc.WorkbookPart.GetPartById(sheet.Id.Value)).Worksheet;

                LineBuilingCycle(worksheet, part, data);

                worksheet.Save();
                doc.Close();
            }
        }

        private readonly Dictionary<int, int> _firstDataLine = new Dictionary<int, int>
        {
            {1, 3},
            {2, 3},
            {3, 4},
            {4, 4},
            {5, 5},
            {6, 5},
            {7, 3},
            {8, 4},
        };

        private void LineBuilingCycle(Worksheet sheet, int part, IEnumerable<TaxMonitoringReportItem> data)
        {
            var index = _firstDataLine[part];
            foreach (var item in data)
            {
                var row = sheet.Row(index);

                _partBuilders[part](row, item);

                ++index;
            }
        }

        private void BuildChapter8(Row row, TaxMonitoringReportItem item)
        {
            var c = 1;

            row.Cell(c).SetValue(item.LineNumber, CellValues.Number);
            row.Cell(++c).SetValue(item.OperationCode, CellValues.Number);

            row.Cell(++c).SetValue(item.InvoiceNum, CellValues.Number);
            row.Cell(++c).SetValue(item.InvoiceDate, CellValues.Date);

            row.Cell(++c).SetValue(item.ChangeNum, CellValues.Number);
            row.Cell(++c).SetValue(item.ChangeDate, CellValues.Date);

            row.Cell(++c).SetValue(item.CorrectionInvoiceNum, CellValues.Number);
            row.Cell(++c).SetValue(item.CorrectionInvoiceDate, CellValues.Date);

            row.Cell(++c).SetValue(item.CorrectionChangeNum, CellValues.Number);
            row.Cell(++c).SetValue(item.CorrectionChangeDate, CellValues.Date);

            row.Cell(++c).SetValue(item.ConfirmDocNum, CellValues.Number);
            row.Cell(++c).SetValue(item.ConfirmDocDate, CellValues.Date);

            row.Cell(++c).SetValue(item.AcceptDate, CellValues.Date);

            row.Cell(++c).SetValue(item.SellerInn, CellValues.Number);
            row.Cell(++c).SetValue(item.BrokerInn, CellValues.Number);
            row.Cell(++c).SetValue(item.CustomDeclarationNum, CellValues.Number);

            row.Cell(++c).SetValue(item.CurrencyOkvCode, CellValues.Number);
            row.Cell(++c).SetValue(item.PriceAmount, CellValues.Number);
            row.Cell(++c).SetValue(item.NdsAmount, CellValues.Number);

            row.Cell(++c).SetValue(item.ErrorCode, CellValues.Number);
            row.Cell(++c).SetValue(item.TaxPeriod, CellValues.Number);

        }

        private void BuildChapter81(Row row, TaxMonitoringReportItem item)
        {
            BuildChapter8(row, item);
        }

        private void BuildChapter9(Row row, TaxMonitoringReportItem item)
        {
            var c = 1;

            row.Cell(c).SetValue(item.LineNumber, CellValues.Number);
            row.Cell(++c).SetValue(item.OperationCode, CellValues.Number);

            row.Cell(++c).SetValue(item.InvoiceNum, CellValues.Number);
            row.Cell(++c).SetValue(item.InvoiceDate, CellValues.Date);

            row.Cell(++c).SetValue(item.ChangeNum, CellValues.Number);
            row.Cell(++c).SetValue(item.ChangeDate, CellValues.Date);

            row.Cell(++c).SetValue(item.CorrectionInvoiceNum, CellValues.Number);
            row.Cell(++c).SetValue(item.CorrectionInvoiceDate, CellValues.Date);

            row.Cell(++c).SetValue(item.CorrectionChangeNum, CellValues.Number);
            row.Cell(++c).SetValue(item.CorrectionChangeDate, CellValues.Date);

            row.Cell(++c).SetValue(item.BuyerInn, CellValues.Number);
            row.Cell(++c).SetValue(item.BrokerInn, CellValues.Number);

            row.Cell(++c).SetValue(item.ConfirmDocNum, CellValues.Number);
            row.Cell(++c).SetValue(item.ConfirmDocDate, CellValues.Date);

            row.Cell(++c).SetValue(item.CurrencyOkvCode, CellValues.Number);
            row.Cell(++c).SetValue(item.PriceAmount, CellValues.Number);

            row.Cell(++c).SetValue(item.Price10, CellValues.Number);
            row.Cell(++c).SetValue(item.Price18, CellValues.Number);
            row.Cell(++c).SetValue(item.Price0, CellValues.Number);

            row.Cell(++c).SetValue(item.Nds18, CellValues.Number);
            row.Cell(++c).SetValue(item.Nds10, CellValues.Number);

            row.Cell(++c).SetValue(item.PriceTaxFree, CellValues.Number);

            row.Cell(++c).SetValue(item.ErrorCode, CellValues.Number);
            row.Cell(++c).SetValue(item.TaxPeriod, CellValues.Number);

        }

        private void BuildChapter91(Row row, TaxMonitoringReportItem item)
        {
            BuildChapter9(row, item);
        }

        private void BuildChapter10(Row row, TaxMonitoringReportItem item)
        {
            var c = 1;

            row.Cell(c).SetValue(item.LineNumber, CellValues.Number);
            row.Cell(++c).SetValue(item.OperationCode, CellValues.Number);

            row.Cell(++c).SetValue(item.InvoiceNum, CellValues.Number);
            row.Cell(++c).SetValue(item.InvoiceDate, CellValues.Date);

            row.Cell(++c).SetValue(item.ChangeNum, CellValues.Number);
            row.Cell(++c).SetValue(item.ChangeDate, CellValues.Date);

            row.Cell(++c).SetValue(item.CorrectionInvoiceNum, CellValues.Number);
            row.Cell(++c).SetValue(item.CorrectionInvoiceDate, CellValues.Date);

            row.Cell(++c).SetValue(item.CorrectionChangeNum, CellValues.Number);
            row.Cell(++c).SetValue(item.CorrectionChangeDate, CellValues.Date);

            row.Cell(++c).SetValue(item.BuyerInn, CellValues.Number);

            row.Cell(++c).SetValue(item.SellerInn, CellValues.Number);
            row.Cell(++c).SetValue(item.SellerInvoiceNum, CellValues.Number);
            row.Cell(++c).SetValue(item.SellerInvoiceDate, CellValues.Date);
            row.Cell(++c).SetValue(item.SellerCurrencyOkvCode, CellValues.Number);
            row.Cell(++c).SetValue(item.SellerPriceTotal, CellValues.Number);
            row.Cell(++c).SetValue(item.SellerNdsTotal, CellValues.Number);
            row.Cell(++c).SetValue(item.TaxDecrease, CellValues.Number);
            row.Cell(++c).SetValue(item.TaxIncrease, CellValues.Number);
            row.Cell(++c).SetValue(item.NdsDecrease, CellValues.Number);
            row.Cell(++c).SetValue(item.NdsIncrease, CellValues.Number);

            row.Cell(++c).SetValue(item.ErrorCode, CellValues.Number);
            row.Cell(++c).SetValue(item.TaxPeriod, CellValues.Number);

        }

        private void BuildChapter11(Row row, TaxMonitoringReportItem item)
        {
            var c = 1;

            row.Cell(c).SetValue(item.LineNumber, CellValues.Number);
            row.Cell(++c).SetValue(item.OperationCode, CellValues.Number);

            row.Cell(++c).SetValue(item.InvoiceNum, CellValues.Number);
            row.Cell(++c).SetValue(item.InvoiceDate, CellValues.Date);

            row.Cell(++c).SetValue(item.ChangeNum, CellValues.Number);
            row.Cell(++c).SetValue(item.ChangeDate, CellValues.Date);

            row.Cell(++c).SetValue(item.CorrectionInvoiceNum, CellValues.Number);
            row.Cell(++c).SetValue(item.CorrectionInvoiceDate, CellValues.Date);

            row.Cell(++c).SetValue(item.CorrectionChangeNum, CellValues.Number);
            row.Cell(++c).SetValue(item.CorrectionChangeDate, CellValues.Date);

            row.Cell(++c).SetValue(item.SellerInn, CellValues.Number);

            row.Cell(++c).SetValue(item.BrokerInn, CellValues.Number);
            row.Cell(++c).SetValue(item.OperationCode, CellValues.Number);
            row.Cell(++c).SetValue(item.CurrencyOkvCode, CellValues.Number);
            row.Cell(++c).SetValue(item.PriceAmount, CellValues.Number);
            row.Cell(++c).SetValue(item.NdsAmount, CellValues.Number);

            row.Cell(++c).SetValue(item.TaxDecrease, CellValues.Number);
            row.Cell(++c).SetValue(item.TaxIncrease, CellValues.Number);
            row.Cell(++c).SetValue(item.NdsDecrease, CellValues.Number);
            row.Cell(++c).SetValue(item.NdsIncrease, CellValues.Number);

            row.Cell(++c).SetValue(item.ErrorCode, CellValues.Number);
            row.Cell(++c).SetValue(item.TaxPeriod, CellValues.Number);
        }

        private void BuildChapter12(Row row, TaxMonitoringReportItem item)
        {
            var c = 1;

            row.Cell(c).SetValue(item.InvoiceNum, CellValues.Number);
            row.Cell(++c).SetValue(item.InvoiceDate, CellValues.Date);

            row.Cell(++c).SetValue(item.BuyerInn, CellValues.Number);
            row.Cell(++c).SetValue(item.CurrencyOkvCode, CellValues.Number);
            row.Cell(++c).SetValue(item.PriceAmount - item.NdsAmount, CellValues.Number);
            row.Cell(++c).SetValue(item.NdsAmount, CellValues.Number);
            row.Cell(++c).SetValue(item.PriceAmount, CellValues.Number);

            row.Cell(++c).SetValue(item.ErrorCode, CellValues.Number);
            row.Cell(++c).SetValue(item.TaxPeriod, CellValues.Number);
        }

        private void BuildUnconfirmed(Row row, TaxMonitoringReportItem item)
        {
            var c = 1;

            row.Cell(c).SetValue(item.InvoiceNum, CellValues.Number);
            row.Cell(++c).SetValue(item.InvoiceDate, CellValues.Date);

            row.Cell(c).SetValue(item.ContractorName, CellValues.String);
            row.Cell(c).SetValue(item.ContractorInn, CellValues.Number);
            row.Cell(c).SetValue(item.ContractorKpp, CellValues.Number);

        }

        public event EventHandler<IntEventArg> Complete;
        public event EventHandler<IntEventArg> Failed;
    }

    public class IntEventArg : EventArgs
    {
        public int Value { get; private set; }

        public IntEventArg(int value)
        {
            Value = value;
        }
    }
}
