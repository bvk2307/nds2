﻿using CommonComponents.Catalog;
using CommonComponents.Utils.Async;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.IO;
using System.Linq;

namespace Luxoft.NDS2.Client.Services.ExcelReports
{
    public abstract class ExcelTemplateReportBase
    {
        private const int NumberFormatId = 1; //Format code 0 (https://msdn.microsoft.com/en-us/library/documentformat.openxml.spreadsheet.numberingformat.aspx)
        private const int AmountFormatId = 4; //Format code #,##.00 (https://msdn.microsoft.com/en-us/library/documentformat.openxml.spreadsheet.numberingformat.aspx)
        private const int DateFormatId = 14; //Format code mm-dd-yyyy (https://msdn.microsoft.com/en-us/library/documentformat.openxml.spreadsheet.numberingformat.aspx)

        private readonly ICatalogService _catalogService;
        private readonly string _templatePath;
        private readonly string _targetPath;

        protected ExcelTemplateReportBase(ICatalogService catalogService, string templatePath, string targetPath)
        {
            _catalogService = catalogService;
            _templatePath = templatePath;
            _targetPath = targetPath;
        }

        public void WriteReport()
        {
            var worker = new AsyncWorker<bool>();
            worker.DoWork += (sender, args) => WriteReportSync();
            worker.Complete += (sender, args) =>
            {
                var handler = Complete;
                if (handler != null)
                    handler(this, new EventArgs());
            };
            worker.Failed += (sender, args) =>
            {
                var handler = Failed;
                if (handler != null)
                    handler(this, new EventArgs());
            };
            worker.Start();
        }

        private void WriteReportSync()
        {
            if (File.Exists(_targetPath))
                File.Delete(_targetPath);

            var address = new CatalogAddress(
                CatalogAddressSchemas.LogicalCatalog,
                "RoleCatalog",
                "NDS2Subsystem",
                "FileSystems",
                _templatePath);

            using (var template = _catalogService.CreateInstance<Stream>(address))
            {
                using (var target = File.Create(_targetPath))
                {
                    template.Seek(0, SeekOrigin.Begin);
                    template.CopyTo(target);

                    BuildReport(target);

                    target.Flush();
                    target.Close();
                }
            }
        }

        protected uint GetRowIndexLast(SheetData sheetData)
        {
            Row rowLast = sheetData.Elements<Row>().Last<Row>();

            uint rowIndexLast = 0;
            if (rowLast != null)
            {
                rowIndexLast = rowLast.RowIndex.Value;
            }

            return rowIndexLast;
        }

        private void BuildReport(Stream target)
        {
            using (var doc = SpreadsheetDocument.Open(target, true))
            {
                var sheet = doc.WorkbookPart.Workbook.Sheets.OfType<Sheet>().First();
                var worksheet = ((WorksheetPart)doc.WorkbookPart.GetPartById(sheet.Id.Value)).Worksheet;
                var sp = doc.WorkbookPart.WorkbookStylesPart.Stylesheet;

                var cellNumberFormat = new CellFormat
                {
                    NumberFormatId = NumberFormatId
                };
                var cellAmountFormat = new CellFormat
                {
                    NumberFormatId = AmountFormatId
                };


                sp.CellFormats.AppendChild(cellNumberFormat);
                sp.CellFormats.AppendChild(cellAmountFormat);

                sp.Save();

                uint numberFormatIndex = sp.CellFormats.Count.Value;
                WriteData(worksheet, numberFormatIndex, numberFormatIndex+1);

                worksheet.Save();
                doc.Close();
            }
        }

        protected abstract void WriteData(
            Worksheet sheet,
            uint numberStyleIndex,
            uint amountStyleIndex);

        public event EventHandler Complete;
        public event EventHandler Failed;

    }

}
