﻿using System.Globalization;
using CommonComponents.Utils;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Linq;

namespace Luxoft.NDS2.Client.Services.ExcelReports
{

    public static class ExcelHelper
    {
        /// <summary>
        /// Возвращает строку
        /// </summary>
        /// <param name="sheet">Лист</param>
        /// <param name="index">Индекс строки</param>
        /// <returns></returns>
        public static Row Row(this Worksheet sheet, int index)
        {
            var row = sheet.GetFirstChild<SheetData>().Elements<Row>().FirstOrDefault(r => r.RowIndex == index);
            if (row == null)
            {
                var r = sheet.GetFirstChild<SheetData>().Elements<Row>().Last();
                row = (Row) r.Clone();
                var oldIndex = row.RowIndex.Value.ToString();
                var newIndex = index.ToString();
                row.RowIndex = new UInt32Value((uint) index);
                row.Elements<Cell>().ForAll(c => c.CellReference.Value = c.CellReference.Value.Replace(oldIndex, newIndex));
                var data = sheet.GetFirstChild<SheetData>();
                data.Append(row);
            }
            return row;
        }

        /// <summary>
        /// Возвращает строку сразу для добавления
        /// </summary>
        /// <param name="sheet">Лист</param>
        /// <param name="index">Индекс строки</param>
        /// <param name="rowIndexLast">максимальный индекс сущесвующей строки</param>
        /// <returns></returns>
        public static Row RowAppend(this Worksheet sheet, int index, uint rowIndexLast)
        {
            Row row = null;
            if (index <= (rowIndexLast + 10))
                row = sheet.GetFirstChild<SheetData>().Elements<Row>().FirstOrDefault(r => r.RowIndex == index);
            if (row == null)
            {
                var r = sheet.GetFirstChild<SheetData>().Elements<Row>().Last();
                row = (Row)r.Clone();
                var oldIndex = row.RowIndex.Value.ToString();
                var newIndex = index.ToString();
                row.RowIndex = new UInt32Value((uint)index);
                row.Elements<Cell>().ForAll(c => c.CellReference.Value = c.CellReference.Value.Replace(oldIndex, newIndex));
                var data = sheet.GetFirstChild<SheetData>();
                data.Append(row);
            }
            return row;
        }

        /// <summary>
        /// Возвращает ячейку в строке со строковым индексом columnName ('A1'-стиль)
        /// </summary>
        /// <param name="row">Строка</param>
        /// <param name="columnName">Колонка</param>
        /// <returns></returns>
        public static Cell Cell(this Row row, string columnName)
        {
            var index = string.Format("{0}{1}", columnName, row.RowIndex);
            var cell = row.Elements<Cell>().FirstOrDefault(
                c =>
                    String.Compare(
                        c.CellReference.Value,
                        index,
                        StringComparison.OrdinalIgnoreCase) == 0);
            if (cell == null)
            {
                cell = new Cell { CellReference = index };
                row.Append(cell);
            }
            return cell;
        }

        /// <summary>
        /// Возвращает ячейку в строке с числовым индексом index (1 == 'A')
        /// </summary>
        /// <param name="row">Строка</param>
        /// <param name="index">Числовой индекс колонки</param>
        /// <returns></returns>
        public static Cell Cell(this Row row, int index)
        {
            var columnName = GetExcelColumnName(index);
            return row.Cell(columnName);
        }
        
        /// <summary>
        /// Устанавливает значение в ячейку
        /// </summary>
        /// <param name="cell">Ячейка</param>
        /// <param name="value">Значение</param>
        /// <param name="type">Тип значения</param>
        /// <param name="styleIndex"></param>
        public static void SetValue(this Cell cell, object value, CellValues type, uint? styleIndex = null)
        {
            cell.CellValue = new CellValue(Convert.ToString(value, CultureInfo.InvariantCulture));
            cell.DataType = new EnumValue<CellValues>(type);
            if(styleIndex!=null)
                cell.StyleIndex = (UInt32Value)styleIndex;
        }
        
        private static string GetExcelColumnName(int index)
        {
            var d = index;
            var result = String.Empty;

            while (d > 0)
            {
                var modulo = (d - 1) % 26;
                result = Convert.ToChar(65 + modulo).ToString(CultureInfo.InvariantCulture) + result;
                d = ((d - modulo) / 26);
            }

            return result;
        }
    }
}
