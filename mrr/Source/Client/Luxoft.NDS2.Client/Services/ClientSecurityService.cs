﻿using CommonComponents.Communication;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.AccessRestriction;
using Luxoft.NDS2.Common.Contracts.Services;
using Microsoft.Practices.CompositeUI;
using System.Linq;

namespace Luxoft.NDS2.Client.Services
{
    class ClientSecurityService
    {
        private readonly ISecurityService _service;

        public static ClientSecurityService Create(WorkItem workItem)
        {
            return new ClientSecurityService(
                workItem.Services.Get<IClientContextService>()
                    .CreateCommunicationProxy<ISecurityService>(Constants.EndpointName)
                );
        }

        public ClientSecurityService(ISecurityService service)
        {
            _service = service;
        }

        public string[] ValidateOperations(params string[] operations)
        {
            try
            {
                var result = _service.ValidateAccess(operations);

                return result.Status == ResultStatus.Success
                    ? result.Result
                    : new string[0];
            }
            catch
            {
                return new string[0];
            }
        }

        public AccessContext GetAccessContext(params string[] operations)
        {
            var result = _service.GetAccessContext(operations);

            return result.Status == ResultStatus.Success
                ? result.Result
                : new AccessContext {Operations = new OperationAccessContext[] {}};
        }
    }

    public static class OperationHelper
    {
        public static bool AllowOperation(this string[] operations, params string[] check)
        {
            return operations.Intersect(operations).Any();
        }

        public static string[] AvailableInspections(this AccessContext ctx, string operation)
        {
            var o = ctx.Operations.FirstOrDefault(x => x.Name == operation);
            return o != null
                ? o.AvailableInspections
                : new string[] {};
        }
    }
}
