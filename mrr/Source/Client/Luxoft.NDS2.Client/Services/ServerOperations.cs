﻿using Luxoft.NDS2.Client.UI.Selections;
using Luxoft.NDS2.Common.Contracts.Services;

namespace Luxoft.NDS2.Client.Services
{
    /// <summary> Client operations requesting a server operation. ATTENTION! Item names must be short because they are used inside application cache keys. </summary>
    public enum ServerOperations
    {
        /// <summary> Bargains in the base presenter (see <see cref="UI.Base.BasePresenter{TV}.Barg"/>). </summary>
        BP_Bargains                 = 1,
        /// <summary> Currencies in the base presenter (see <see cref="UI.Base.BasePresenter{TV}.Curr"/>). </summary>
        BP_Currs,
        /// <summary> Operations in the base presenter (see <see cref="UI.Base.BasePresenter{TV}.Oper"/>). </summary>
        BP_Opers,
        /// <summary> 'Surs' in the base presenter (see <see cref="UI.Base.BasePresenter{TV}.Sur"/>). </summary>
        BP_Surs,
        /// <summary> 'AnnulmentReason' in the base presenter (see <see cref="UI.Base.BasePresenter{TV}.AnnulmentReason"/>). </summary>
        BP_AnnulReason,
        /// <summary> Tax perions in the base presenter (see <see cref="UI.Base.BasePresenter{TV}.TaxPeriodModel"/>). </summary>
        BP_TaxPrd,
        /// <summary> User permissions in the base presenter (see <see cref="UI.Base.BasePresenter{TV}.GetUserPermissions"/>()). </summary>
        BP_UserPerms,
        /// <summary> User restriction configurations in the base presenter (see <see cref="UI.Base.BasePresenter{TV}.UserRestrictionConfig"/>). </summary>
        BP_UserRestrCfg,
        /// <summary> Dependency tree nodes (see <see cref="IPyramidDataService.LoadGraphNodes"/>). </summary>
        DT_Nodes,
        /// <summary> Selection Status Dictionary in the base presenter (see <see cref="SelectionListPresenter.StatuDictionary"/>). </summary>
        BP_SelStatus,
        /// <summary> 'ExcludeReason' Dictionary in the base presenter (see <see cref="SelectionListPresenter.StatuDictionary"/>). </summary>
        BP_ExclReason

    }
}