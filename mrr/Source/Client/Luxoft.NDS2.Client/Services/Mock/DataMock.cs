﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Luxoft.NDS2.Client.Services.Mock
{
    class DataMock<TData> where TData : new()
    {
        private readonly PropertyInfo[] _properties;

        private readonly Random _random = new Random();

        private readonly Dictionary<Type, Func<object>> _generators;

        private object RandomString()
        {
            var sb = new StringBuilder();
            for (var i = 0; i < _random.Next(5, 20); ++i)
            {
                sb.Append((char)_random.Next(96, 122));
            }
            return sb.ToString();
        }

        public DataMock()
        {
            _properties = typeof(TData).GetProperties();

            _generators = new Dictionary<Type, Func<object>>
            {
                {typeof (string), RandomString},
                {typeof (int), () => _random.Next(10, 500)},
                {typeof (long), () => _random.Next(1000000, 5000000)},
                {typeof (ulong), () => _random.Next(1000000, 5000000)},
                {typeof (decimal), () => Convert.ToDecimal(Math.Round(_random.NextDouble()*10000, 2))},
                {typeof (DateTime), () => DateTime.Now.AddDays(-_random.Next(0, 1000))},
                {typeof (bool), () => _random.Next(0, 2) == 1},
                {typeof (int?), () => _random.Next(0, 2) == 1 ? _random.Next(10, 500) : (int?) null},
                {typeof (long?), () => _random.Next(0, 2) == 1 ? _random.Next(1000000, 5000000) : (long?) null},
                {typeof (ulong?), () => _random.Next(0, 2) == 1 ? _random.Next(1000000, 5000000) : (long?) null},
                {
                    typeof (decimal?),
                    () => _random.Next(0, 2) == 1 ? (decimal?) Math.Round(_random.NextDouble()*10000, 2) : (decimal?) null
                },
                {
                    typeof (DateTime?),
                    () => _random.Next(0, 2) == 1 ? DateTime.Now.AddDays(-_random.Next(0, 1000)) : (DateTime?) null
                }
            };
        }

        public TData Generate()
        {
            var data = new TData();
            foreach (var property in _properties)
            {
                Func<object> generator;
                if (_generators.TryGetValue(property.PropertyType, out generator))
                    property.SetValue(data, generator(), null);
            }
            return data;
        }

        public List<TData> Generate(int quantity)
        {
            var list = new List<TData>(quantity);
            for (var i = 0; i < quantity; ++i)
            {
                list.Add(Generate());
            }
            return list;
        }

        public List<TData> Generate(int minQuantity, int maxQuantity)
        {
            return Generate(_random.Next(
                Math.Min(minQuantity, maxQuantity),
                Math.Max(minQuantity, maxQuantity)));
        }
    }
}
