﻿using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Common.Helpers.Cache;

namespace Luxoft.NDS2.Client.Services.Cache
{
    /// <summary> A decorator working with <see cref="AppCache"/> for <see cref="IPyramidDataService"/>. </summary>
    public interface IPyramidDataCachingService : IPyramidDataService {
        string BuildCacheKey( ServerOperations serverOperations, GraphNodesKeyParameters nodesKeyParameters );
    }
}