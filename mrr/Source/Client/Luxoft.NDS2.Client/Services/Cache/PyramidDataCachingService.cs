﻿using System;
using System.Collections.Generic;
using Luxoft.NDS2.Client.Cache;
using Luxoft.NDS2.Client.Config.GraphTree;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using Luxoft.NDS2.Common.Contracts.DTO.Reporting.TaxPayer;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Common.Helpers.Cache;
using Luxoft.NDS2.Common.Helpers.Cache.Extensions;

namespace Luxoft.NDS2.Client.Services.Cache
{  
    /// <summary> A decorator working with <see cref="AppCache"/> for <see cref="IPyramidDataService"/>. </summary>
    public sealed class PyramidDataCachingService : DataCachableService, IPyramidDataCachingService
    {
        /// <summary> Expiration of Dependency Tree's data in the app cache in seconds. </summary>
        private readonly TimeSpan _nodesExpirationSec;

        private readonly IPyramidDataService _decoratedService;
        private readonly AppCache _appCache;

        public PyramidDataCachingService( IPyramidDataService decoratedService, AppCache appCache, GraphTreeDataCacheSettings dataCacheSettings )
        {
            _decoratedService   = decoratedService;
            _appCache           = appCache;

            _nodesExpirationSec = TimeSpan.FromSeconds( dataCacheSettings.NodesExpirationTimeSec );
        }

        /// <summary> Expiration of Dependency Tree's data in the app cache in seconds. </summary>
        private TimeSpan NodesExpirationSec
        {
            get { return _nodesExpirationSec; }
        }

        /// <summary> Запрашивает данные для узла дерева </summary>
        /// <param name="nodesKeyParameters"></param>
        /// <returns>Возвращает иерархию НП</returns>
        public OperationResult<IReadOnlyCollection<GraphData>> LoadGraphNodes( GraphNodesKeyParameters nodesKeyParameters )
        {
            OperationResult<IReadOnlyCollection<GraphData>> result = _appCache.AddOrGetExisting( 
                BuildCacheKey( ServerOperations.DT_Nodes, nodesKeyParameters ), 
                nodesKeyParams => _decoratedService.LoadGraphNodes( nodesKeyParams ), NodesExpirationSec, nodesKeyParameters );

            return result;
        }

        /// <summary> Запрашивает данные для пакета узлов дерева для отчёта </summary>
        /// <param name="nodesKeyParameters"></param>
        /// <returns> Данные для пакета узлов дерева для отчёта </returns>
        public OperationResult<IReadOnlyCollection<GraphData>> LoadReportGraphNodes( GraphNodesKeyParameters nodesKeyParameters )
        {
            return _decoratedService.LoadReportGraphNodes( nodesKeyParameters );
        }

        public OperationResult<PageResult<GraphData>> LoadGraphNodeChildrenByPage( GraphNodesKeyParameters nodesKeyParameters )
        {
            return _decoratedService.LoadGraphNodeChildrenByPage( nodesKeyParameters );
        }

        /// <summary> Запрашивает данные по суммам вычетов контрагента за предыдущие периоды. </summary>
        /// <param name="deductionKeyParameters"></param>
        /// <returns></returns>
        public OperationResult<IReadOnlyCollection<DeductionDetail>> LoadDeductionDetails( DeductionDetailsKeyParameters deductionKeyParameters )
        {
            OperationResult<IReadOnlyCollection<DeductionDetail>> result = _appCache.AddOrGetExisting( 
                BuildCacheKey( ServerOperations.DT_Nodes, deductionKeyParameters ), 
                deductionKeyParams => _decoratedService.LoadDeductionDetails( deductionKeyParams ), NodesExpirationSec, deductionKeyParameters );

            return result;
        }

        /// <summary> Запрашивает КПП для НП по ИНН. </summary>
        /// <param name="inn"></param>
        /// <returns></returns>
        public OperationResult<IReadOnlyCollection<string>> LoadTaxpayersKpp(string inn)
        {
            return _decoratedService.LoadTaxpayersKpp(inn);
        }

        public string BuildCacheKey( ServerOperations serverOperations, GraphNodesKeyParameters nodesKeyParameters )
        {
            CacheKeyBuilder keyBuider = CacheKeyBuilder.Create()
                .WithCacheKeyFragment(TaxPayerIdHelper.GetInns(nodesKeyParameters.TaxPayerIds))
                .WithCacheKeyPart(nodesKeyParameters.TaxQuarter)
                .WithCacheKeyPart(nodesKeyParameters.TaxYear)
                .WithCacheKeyPart(nodesKeyParameters.IsRoot) 
                .WithCacheKeyPart(nodesKeyParameters.IsPurchase)
                .WithCacheKeyFragment(TaxPayerIdHelper.GetKpps(nodesKeyParameters.TaxPayerIds))
                .WithCacheKeyPart(nodesKeyParameters.MostImportantContractors) 
                .WithCacheKeyPart(nodesKeyParameters.SharedNdsPercent)
                .WithCacheKeyPart(nodesKeyParameters.LayersLimit)
                .WithCacheKeyPart(nodesKeyParameters.MinReturnedContractors);

            string key = keyBuider.WithOperation( serverOperations );
              
            return key;
        }
    }
}