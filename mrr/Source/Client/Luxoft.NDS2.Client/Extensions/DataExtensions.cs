﻿using System;
using System.Linq.Expressions;

namespace Luxoft.NDS2.Client.Extensions
{
    public static class DataExtensions
    {
        public static string FormatAggregateValue(this decimal? value)
        {
            return value == null || value.Value == 0 ? "" : string.Format("{0:#,##0}", value);
        }
    }
}