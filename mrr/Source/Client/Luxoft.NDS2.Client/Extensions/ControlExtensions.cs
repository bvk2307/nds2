﻿using System.Collections.Generic;
using System.Linq;

namespace System.Windows.Forms
{
    public static class ControlExtensions
    {
        public static void StartExecuteInUiThread<TRunner>(this TRunner ctl, Action action) 
            where TRunner : Control
        {
            if (ctl == null)
            {
                return;
            }

            if (ctl.InvokeRequired)
            {
                ctl.BeginInvoke(action);
            }
            else
            {
                action.Invoke();
            }
        }

        [Obsolete( "Do not use Control to call the main UI thread. Use BasePresenter.BeginExecute() or UIThreadExecutor.BeginExecute() instead of", error: false )]
        public static void StartExecuteInUiThread<TRunner, T1>(this TRunner ctl, Action<T1> action, T1 agr1) 
            where TRunner : Control
        {
            if(ctl != null)
                ctl.BeginInvoke(action, agr1);
        }

        [Obsolete( "Do not use Control to call the main UI thread. Use BasePresenter.BeginExecute() or UIThreadExecutor.BeginExecute() instead of", error: false )]
        public static void StartExecuteInUiThread<TRunner, T1, T2>(this TRunner ctl, Action<T1, T2> action, T1 agr1, T2 arg2) 
            where TRunner : Control
        {
            if(ctl != null)
                ctl.BeginInvoke(action, agr1, arg2);
        }

        [Obsolete( "Do not use Control to call the main UI thread. Use BasePresenter.BeginExecute() or UIThreadExecutor.BeginExecute() instead of", error: false )]
        public static void StartExecuteInUiThread<TRunner, T1, T2, T3>(this TRunner ctl, Action<T1, T2, T3> action, T1 agr1, T2 arg2, T3 arg3) 
            where TRunner : Control
        {
            if(ctl != null)
                ctl.BeginInvoke(action, agr1, arg2, arg3);
        }

        [Obsolete( "Do not use Control to call the main UI thread. Use BasePresenter.BeginExecute() or UIThreadExecutor.BeginExecute() instead of", error: false )]
        public static void StartExecuteInUiThread<TRunner, T1, T2, T3, T4>(this TRunner ctl, Action<T1, T2, T3, T4> action, T1 agr1, T2 arg2, T3 arg3, T4 arg4) 
            where TRunner : Control
        {
            if(ctl != null)
                ctl.BeginInvoke(action, agr1, arg2, arg3, arg4);
        }

        [Obsolete( "Do not Invoke() into the main UI thread. Use BasePresenter.BeginExecute() or UIThreadExecutor.BeginExecute() instead of", error: false )]
        public static void ExecuteInUiThread<T>(this T ctl, Action<T> action) where T : Control
        {
            if(ctl != null)
            {
                if(ctl.InvokeRequired)
                {
                    ctl.Invoke(action, ctl);
                }
                else
                {
                    action(ctl);
                }
            }
        }

        public static IEnumerable<TEntity> OrderBy<TEntity>(this IEnumerable<TEntity> source,
                                                    string orderByProperty, bool desc)
        {
            string command = desc ? "OrderByDescending" : "OrderBy";
            var type = typeof(TEntity);
            var property = type.GetProperty(orderByProperty);
            var parameter = Linq.Expressions.Expression.Parameter(type, "p");
            var propertyAccess = Linq.Expressions.Expression.MakeMemberAccess(parameter, property);
            var orderByExpression = Linq.Expressions.Expression.Lambda(propertyAccess, parameter);
            var resultExpression = Linq.Expressions.Expression.Call(typeof(Queryable), command,
                                                   new[] { type, property.PropertyType },
                                                   source.AsQueryable().Expression,
                                                   Linq.Expressions.Expression.Quote(orderByExpression));
            return source.AsQueryable().Provider.CreateQuery<TEntity>(resultExpression);
        }
    }
}
