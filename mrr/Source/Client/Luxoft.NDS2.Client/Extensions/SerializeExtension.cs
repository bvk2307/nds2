﻿﻿using System.IO;
using System.Xml.Serialization;

namespace Luxoft.NDS2.Client.Extensions
{
    static class SerializeExtension
    {
        public static string Serialize<T>(this T obj)
        {
            using (var writer = new StringWriter())
            {
                new XmlSerializer(typeof(T)).Serialize(writer, obj);
                return writer.ToString();
            }
        }

        public static T Deserialize<T>(this string sourceString)
        {
            if (string.IsNullOrEmpty(sourceString))
            {
                return default(T);
            }
            using (var reader = new StringReader(sourceString))
            {
                return (T)new XmlSerializer(typeof(T)).Deserialize(reader);
            }
        }
    }
}
