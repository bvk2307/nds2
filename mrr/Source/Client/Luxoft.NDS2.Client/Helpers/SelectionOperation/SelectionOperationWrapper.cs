﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;

namespace Luxoft.NDS2.Client.Helpers.SelectionOperation
{
    /// <summary>
    /// Класс - обертка над операциями с выборками
    /// нужен для работы с кнопками в риббоне
    /// </summary>
    public class SelectionOperationWrapper
    {
        public Func<Selection, SelectionCommandParam, OperationResult<Selection>> operation { get; set; }
        
        public string MessageForOperationStart { get; set; }
        public string MessageForOperationEnd { get; set; }

        public string OperationName { get; set; }
        public string OperationTitle { get; set; }
        public string OperationDescription { get; set; }

        public string OperationImageResource { get; set; }
        public string OperationImageLargeName { get; set; }
        public string OperationImageSmallName { get; set; }

        public bool Enabled { get; set; }
        public bool ShowWindowWithComment { get; set; }
    }
}
