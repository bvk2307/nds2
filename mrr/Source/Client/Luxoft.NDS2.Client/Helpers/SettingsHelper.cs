﻿using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.Helpers
{
    public static class SettingsHelper
    {
        public static void Save(
            this IUcOptionsService ucOptionsService, 
            string optionsName, 
            string valueName, 
            string valueSetup)
        {
            OptionsDataContextBase _options;
            if (!ucOptionsService.TryGet(optionsName, out _options))
            {
                _options = new OptionsDataContextBase(optionsName, string.Empty, string.Empty);
                _options.AddDefault(new StringDefaultOptionItem(valueName, valueName, valueName, string.Empty));
                ucOptionsService.AddDataContext(_options);
            }
            ucOptionsService.Read(_options.Name);

            _options.SetOptionValue(valueName, valueSetup);

            ucOptionsService.Save();
        }

        public static string Load(
            this IUcOptionsService ucOptionsService,
            string optionsName, 
            string valueName)
        {
            OptionsDataContextBase _options;
            if (!ucOptionsService.TryGet(optionsName, out _options))
            {
                _options = new OptionsDataContextBase(optionsName, string.Empty, string.Empty);
                _options.AddDefault(new StringDefaultOptionItem(valueName, valueName, valueName, string.Empty));
                ucOptionsService.AddDataContext(_options);
            }
            ucOptionsService.Read(_options.Name);

            string ret = string.Empty;
            _options.TryGetOptionValue<string>(valueName, out ret);

            return ret;
        }
    }
}
