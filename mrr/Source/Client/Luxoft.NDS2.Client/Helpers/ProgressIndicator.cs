﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Luxoft.NDS2.Client.Properties;
using Luxoft.NDS2.Client.UI.Controls.LookupDictionary.Helpers;

namespace Luxoft.NDS2.Client.Helpers
{
    public class ProgressIndicator
    {
        private PictureBox _progressIndicator;
        private Control _parentToRender;
        private Control _controlToDisable;

        public ProgressIndicator(Control parentToRender, Control toDisable)
        {
            _parentToRender = parentToRender;
            _controlToDisable = toDisable;
            InitLoadingBox();
        }

        private void InitLoadingBox()
        {
            _progressIndicator = new PictureBox();
            _progressIndicator.Image = Resources.progressIndicator;

            _parentToRender.Controls.Add(_progressIndicator);
            _progressIndicator.SizeMode = PictureBoxSizeMode.StretchImage;
            _progressIndicator.ClientSize = new Size(_progressIndicator.Image.Width,
                                                     _progressIndicator.Image.Height);
        }

        public void ShowProgress()
        {
            _parentToRender.SafeAction(ShowProgressInternal);
        }

        public void HideProgress()
        {
            _parentToRender.SafeAction(HideProgressInternal);
        }

        private void ShowProgressInternal(Control ctl)
        {
            _controlToDisable.Enabled = false;
            _progressIndicator.Left = 0
                + (ctl.Width / 2) - (_progressIndicator.Width / 2);
            _progressIndicator.Top = 0
                + (ctl.Height / 2) - (_progressIndicator.Height / 2);
            _progressIndicator.BringToFront();

            _progressIndicator.Visible = true;
        }

        private void HideProgressInternal(Control ctl)
        {
            _controlToDisable.Enabled = true;
            _progressIndicator.Visible = false;
        }
    }
}
