﻿using System;
using System.Text;

namespace Luxoft.NDS2.Client.Helpers
{
    public class IntValueSignFormatter : IFormatProvider, ICustomFormatter
    {
        public object GetFormat(Type formatType) {
            return formatType == typeof(ICustomFormatter) ? this : null;
        }

        public string Format(string format,
                              object arg,
                              IFormatProvider formatProvider)
        {
            if (!this.Equals(formatProvider))
            {
                return null;
            }
            else
            {
                string ret = String.Empty;
                string formatCurrent = String.Empty;

                if (!string.IsNullOrWhiteSpace(format))
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("{");
                    sb.Append("0:");
                    sb.Append(format);
                    sb.Append("}");
                    formatCurrent = sb.ToString();
                }
                else
                {
                    formatCurrent = "{0:+#,0.00;-#,0.00;#,0.00}";
                }

                Type type = arg.GetType();
                if (type == typeof(int))
                {
                    int intValue = (int)arg;
                    if (intValue != 0)
                    {
                        ret = intValue.ToString("N");
                        if (intValue > 0) ret = "+" + ret;
                    }
                }
                else if (type == typeof(long))
                {
                    long longValue = (long)arg;
                    if (longValue != 0)
                    {
                        ret = longValue.ToString("N");
                        if (longValue > 0) ret = "+" + ret;
                    }
                }
                else if (type == typeof(decimal))
                {
                    decimal decValue = (decimal)arg;
                    if (decValue != 0)
                    {
                        ret = decValue.ToString("N");
                        if (decValue > 0) ret = "+" + ret;
                    }
                }
                return ret;
            }
        }
    }
}
