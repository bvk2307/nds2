﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.Helpers
{
    public class DigitOrDateFormatter: IFormatProvider, ICustomFormatter
    {
        public object GetFormat(Type formatType)
        {
            if (formatType == typeof(ICustomFormatter))
                return this;
            else
                return null;
        }

        public string Format(string format, object arg, IFormatProvider formatProvider)
        {
            if (!this.Equals(formatProvider))
            {
                return null;
            }
            else
            {
                string ret = String.Empty;
                string formatCurrent = String.Empty;

                if (!string.IsNullOrWhiteSpace(format))
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("{");
                    sb.Append(format);
                    sb.Append("}");
                    formatCurrent = sb.ToString();
                }
                else
                {
                    formatCurrent = "{0}";
                }

                if (arg != null)
                {
                    Type type = arg.GetType();
                    if (type == typeof(string))
                    {
                        string strValue = (string)arg;
                        if (!string.IsNullOrWhiteSpace(strValue))
                        {
                            long longValue = 0;
                            decimal decValue = 0;
                            TimeSpan tsValue;
                            if (long.TryParse(strValue, out longValue))
                            {
                                ret = string.Format("{0:N0}", longValue);
                            }
                            else if (decimal.TryParse(strValue, out decValue))
                            {
                                ret = string.Format("{0:N2}", decValue);
                            }
                            else if (TimeSpan.TryParse(strValue, out tsValue))
                            {
                                int hour = 0;
                                int minute = 0;
                                int second = 0;
                                int iTemp = 0;
                                string[] words = strValue.Split(':');
                                if (words.Count() > 0)
                                {
                                    if (int.TryParse(words[0], out iTemp))
                                    {
                                        hour = iTemp;
                                    }
                                }
                                if (words.Count() > 1)
                                {
                                    if (int.TryParse(words[1], out iTemp))
                                    {
                                        minute = iTemp;
                                    }
                                }
                                if (words.Count() > 2)
                                {
                                    if (int.TryParse(words[2], out iTemp))
                                    {
                                        second = iTemp;
                                    }
                                }
                                TimeSpan ts = new TimeSpan(hour, minute, second);
                                ret = ts.ToString();
                            }
                        }
                    }
                }
                else
                {
                    ret = String.Empty;
                }
                return ret;
            }
        }
    }
}
