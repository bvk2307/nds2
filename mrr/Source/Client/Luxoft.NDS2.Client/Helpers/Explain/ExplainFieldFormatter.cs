﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply;
using Luxoft.NDS2.Common.Contracts.Helpers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.Helpers.Explain
{
    public class ExplainFieldFormatter
    {
        private List<string> _fieldOfDates = new List<string>();
        private List<string> _fieldOfDecimal = new List<string>();

        public ExplainFieldFormatter()
        {
            Init();
        }

        private void Init()
        {
            _fieldOfDates.Clear();
            _fieldOfDates.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.INVOICE_DATE));
            _fieldOfDates.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.CHANGE_DATE));
            _fieldOfDates.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.CORRECTION_DATE));
            _fieldOfDates.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.CHANGE_CORRECTION_DATE));
            _fieldOfDates.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.CREATE_DATE));
            _fieldOfDates.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.SELLER_AGENCY_INFO_DATE));
            _fieldOfDates.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.RECEIVE_DATE));
            _fieldOfDates.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.SELLER_INVOICE_DATE));

            _fieldOfDecimal.Clear();
            _fieldOfDecimal.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.PRICE_TOTAL));
            _fieldOfDecimal.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.DIFF_CORRECT_NDS_DECREASE));
            _fieldOfDecimal.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.DIFF_CORRECT_NDS_INCREASE));
            _fieldOfDecimal.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.PRICE_BUY_AMOUNT));
            _fieldOfDecimal.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.PRICE_TAX_FREE));
            _fieldOfDecimal.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.PRICE_SELL_IN_CURR));
            _fieldOfDecimal.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.PRICE_SELL));
            _fieldOfDecimal.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.PRICE_SELL_18));
            _fieldOfDecimal.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.PRICE_SELL_10));
            _fieldOfDecimal.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.PRICE_SELL_0));
        }

        public string PrepareValue(string fieldName, string value)
        {
            string retValue = value;

            if (value != null)
            {
                retValue = retValue.Trim();
                if (_fieldOfDates.Any(p => p == fieldName))
                {
                    var dateTimeValue = ExplainParser.PrepareParseDataTime(value.Trim());
                    if (dateTimeValue != null)
                        retValue = FormatDateTime(dateTimeValue);
                }
                if (_fieldOfDecimal.Any(p => p == fieldName))
                {
                    var decimalValue = ExplainParser.PrepareParseDecimal(value.Trim());
                    if (decimalValue != null)
                        retValue = FormatDecimal(decimalValue);
                }
            }

            return retValue;
        }

        public string FormatDateTime(DateTime? dt)
        {
            string ret = String.Empty;
            if (dt != null)
            {
                ret = string.Format("{0:00}.{1:00}.{2:0000}", dt.Value.Day, dt.Value.Month, dt.Value.Year);
            }
            return ret;
        }

        public string FormatDecimal(decimal? value)
        {
            string ret = String.Empty;
            if (value != null)
            {
                ret = value.Value.ToString(CultureInfo.GetCultureInfo("Ru-ru"));
            }
            return ret;
        }
    }
}
