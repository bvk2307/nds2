﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.Helpers.Explain
{
    public static class ExplainParser
    {
        public static DateTime? PrepareParseDataTime(string value)
        {
            DateTime? result = null;
            DateTime dtParse = DateTime.Now;
            if (PrepareTryParseDataTime(value, out dtParse))
            {
                result = dtParse;
            }
            return result;
        }

        private static bool PrepareTryParseDataTime(string value, out DateTime resultValue)
        {
            bool ret = false;
            resultValue = DateTime.MinValue;

            DateTime dtParse = DateTime.Now;
            if (DateTime.TryParse(value, out dtParse))
            {
                resultValue = dtParse;
                ret = true;
            }
            else
            {
                if (TryParseDataTime(CultureInfo.CurrentCulture, value, out dtParse))
                {
                    resultValue = dtParse;
                    ret = true;
                }
            }

            return ret;
        }

        public static bool TryParseDataTime(string value, out DateTime resultValue)
        {
            bool ret = false;
            resultValue = DateTime.MinValue;

            DateTime dtParse = DateTime.Now;
            if (DateTime.TryParse(value, out dtParse))
            {
                resultValue = dtParse;
                ret = true;
            }
            else
            {
                if (TryParseDataTime(CultureInfo.CurrentCulture, value, out dtParse))
                {
                    resultValue = dtParse;
                    ret = true;
                }
                else
                {
                    var cultures = CultureInfo.GetCultures(CultureTypes.AllCultures);
                    foreach (var itemCulture in cultures)
                    {
                        if (TryParseDataTime(itemCulture, value, out dtParse))
                        {
                            resultValue = dtParse;
                            ret = true;
                            break;
                        }
                    }
                }
            }

            return ret;
        }

        private static bool TryParseDataTime(CultureInfo cultureInfo, string value, out DateTime resultValue)
        {
            bool ret = false;
            resultValue = DateTime.MinValue;
            DateTime dtParse = DateTime.Now;

            var formats = new List<string>();
            formats.AddRange(cultureInfo.DateTimeFormat.GetAllDateTimePatterns());

            if (DateTime.TryParseExact(
                value,
                formats.ToArray(),
                cultureInfo,
                DateTimeStyles.None,
                out dtParse))
            {
                resultValue = dtParse;
                ret = true;
            }

            return ret;
        }

        public static decimal? PrepareParseDecimal(string value)
        {
            decimal? result = null;
            decimal decimalParse = -1;
            if (PrepareTryParseDecimal(value, out decimalParse))
            {
                result = decimalParse;
            }
            return result;
        }

        private static bool PrepareTryParseDecimal(string value, out decimal resultValue)
        {
            bool ret = false;
            resultValue = -1;

            decimal decimalParse = -1;
            if (decimal.TryParse(value, out decimalParse))
            {
                resultValue = decimalParse;
                ret = true;
            }
            else
            {
                if (TryParseDecimal(CultureInfo.CurrentCulture, value, out decimalParse))
                {
                    resultValue = decimalParse;
                    ret = true;
                }
            }

            return ret;
        }

        public static bool TryParseDecimal(string value, out decimal resultValue)
        {
            bool ret = false;
            resultValue = -1;

            decimal decimalParse = -1;
            if (TryParseDecimal(CultureInfo.GetCultureInfo("Ru-ru"), value, out decimalParse))
            {
                resultValue = decimalParse;
                ret = true;
            }
            else
            {
                if (TryParseDecimal(CultureInfo.CurrentCulture, value, out decimalParse))
                {
                    resultValue = decimalParse;
                    ret = true;
                }
                else
                {
                    var cultures = CultureInfo.GetCultures(CultureTypes.AllCultures);
                    foreach (var itemCulture in cultures)
                    {
                        if (TryParseDecimal(itemCulture, value, out decimalParse))
                        {
                            resultValue = decimalParse;
                            ret = true;
                            break;
                        }
                    }
                }
            }

            return ret;
        }

        private static bool TryParseDecimal(CultureInfo cultureInfo, string value, out decimal resultValue)
        {
            bool ret = false;
            resultValue = -1;
            decimal decimalParse = -1;

            var formats = new List<string>();
            formats.AddRange(cultureInfo.DateTimeFormat.GetAllDateTimePatterns());

            if (decimal.TryParse(
                value,
                NumberStyles.Any,
                cultureInfo,
                out decimalParse))
            {
                resultValue = decimalParse;
                ret = true;
            }

            return ret;
        }
    }
}
