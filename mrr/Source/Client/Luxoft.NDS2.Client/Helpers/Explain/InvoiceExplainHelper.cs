﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.Helpers.Explain
{
    public static class InvoiceExplainHelper
    {
        public static string CreateExplainToolTip(Invoice _invoiceData)
        {
            var sbExplainNumDate = new StringBuilder();

            var explainTypes = new Dictionary<int, string>();
            explainTypes.Add(ExplainReplyType.Explain, "Пояснение");
            explainTypes.Add(ExplainReplyType.Reply_93, "Ответ по истребованию 93");
            explainTypes.Add(ExplainReplyType.Reply_93_1, "Ответ по истребованию 93.1");

            if (explainTypes.ContainsKey(_invoiceData.ExplainType))
                sbExplainNumDate.Append(explainTypes[_invoiceData.ExplainType]);

            if (_invoiceData.ExplainType == ExplainReplyType.Explain &&
                !string.IsNullOrWhiteSpace(_invoiceData.ExplainNum))
            {
                sbExplainNumDate.Append(" № ");
                sbExplainNumDate.Append(_invoiceData.ExplainNum);
            }
            if (_invoiceData.ExplainDate != null)
            {
                sbExplainNumDate.Append(" от ");
                sbExplainNumDate.Append(_invoiceData.ExplainDate.Value.ToShortDateString());
            }

            var sbMain = new StringBuilder();

            if (_invoiceData.IS_CHANGED != null)
            {
                if (_invoiceData.IS_CHANGED == (int)InvoiceExplainState.Changing)
                    sbMain.Append("Запись изменена ");
                if (_invoiceData.IS_CHANGED == (int)InvoiceExplainState.Processed)
                    sbMain.Append("Запись подтверждена ");
                if (sbExplainNumDate.Length > 0)
                    sbMain.AppendLine(sbExplainNumDate.ToString());
            }
            else
                sbMain.Append(sbExplainNumDate.ToString());

            return sbMain.ToString();
        }
    }
}
