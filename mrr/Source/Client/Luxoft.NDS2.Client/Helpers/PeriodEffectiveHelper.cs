﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.Helpers
{
    class PeriodEffectiveHelper
    {
        public static Dictionary<string, int> PeriodEffectiveByPeriodCode = 
            new Dictionary<string, int>
            {
                {"01", 101},
                {"02", 102},
                {"03", 103},
                {"04", 104},
                {"05", 105},
                {"06", 106},
                {"07", 107},
                {"08", 108},
                {"09", 109},
                {"10", 110},
                {"11", 111},
                {"12", 112},
                {"21", 1},
                {"22", 2},
                {"23", 3},
                {"24", 4},
                {"51", 1},
                {"54", 2},
                {"55", 3},
                {"56", 4},
                {"71", 101},
                {"72", 102},
                {"73", 103},
                {"74", 104},
                {"75", 105},
                {"76", 106},
                {"77", 107},
                {"78", 108},
                {"79", 109},
                {"80", 110},
                {"81", 111},
                {"82", 112}
            };
        
    }
}
