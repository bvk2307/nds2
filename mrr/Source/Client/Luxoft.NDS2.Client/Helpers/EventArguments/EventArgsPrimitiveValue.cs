﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.Helpers.EventArguments
{
    public class EventArgsPrimitiveValue<T> : EventArgs where T:struct 
    {
        public T Value { get; private set; }

        public EventArgsPrimitiveValue(T value)
        {
            Value = value;
        }
    }
}
