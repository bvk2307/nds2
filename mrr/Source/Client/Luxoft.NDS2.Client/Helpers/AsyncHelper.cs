﻿using CommonComponents.Utils.Async;
using System;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.Helpers
{
    public static class AsyncHelper
    {
        public static void DoAsync(this Action action, Action callback = null)
        {
            var asyncWorker = new AsyncWorker<bool>();
            asyncWorker.DoWork += (sender, args) => action();

            if (callback != null)
            {
                asyncWorker.Complete += (sender, args) => callback();
            }

            asyncWorker.Start();
        }

        public static void DoAsync<TResult>(this Func<TResult> action, Action<TResult> callback)
        {
            TResult result = default(TResult);
            var asyncWorker = new AsyncWorker<bool>();

            asyncWorker.DoWork += (sender, args) => result = action();
            asyncWorker.Complete += (sender, args) => callback(result);
            asyncWorker.Start();
        }

        public static void TryDoAsync<TResult>(
            this Func<TResult> action,
            Func<Exception, TResult> defaultResult,
            Action<TResult> callback)
        {
            var asyncWorker = new AsyncWorker<TResult>();

            asyncWorker.DoWork +=
                (sender, args) =>
                {
                    try
                    {
                        args.Result = action();
                    }
                    catch(Exception error)
                    {
                        args.Result = defaultResult(error);
                    }
                };
            asyncWorker.Complete += (sender, args) => callback(args.Result);
            asyncWorker.Start();
        }
    }
}
