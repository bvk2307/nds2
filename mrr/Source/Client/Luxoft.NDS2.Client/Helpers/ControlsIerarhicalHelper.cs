﻿using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinMaskedEdit;
using Infragistics.Win.UltraWinSchedule;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.Helpers
{
    class ControlsIerarhicalHelper
    {
        public static void SetChangesTracker(Control start, EventHandler handler)
        {
            foreach (Control c in start.Controls)
            {
                if (c is UltraTextEditor)
                {
                    var ctrl = c as UltraTextEditor;
                    ctrl.ValueChanged += handler;
                }
                else if (c is UltraCalendarCombo)
                {
                    var ctrl = c as UltraCalendarCombo;
                    ctrl.ValueChanged += handler;
                }
                else if (c is UltraMaskedEdit)
                {
                    var ctrl = c as UltraMaskedEdit;
                    ctrl.ValueChanged += handler;
                }
                else if (c is UltraOptionSet)
                {
                    var ctrl = c as UltraOptionSet;
                    ctrl.ValueChanged += handler;
                }
                else if (c is UltraComboEditor)
                {
                    var ctrl = c as UltraComboEditor;
                    ctrl.ValueChanged += handler;
                }
                else if (c is UltraCheckEditor)
                {
                    var ctrl = c as UltraCheckEditor;
                    ctrl.CheckedValueChanged += handler;
                }
                else if (c is UltraButton)
                {
                    var ctrl = c as UltraButton;
                    ctrl.Click += handler;
                }
                else
                {
                    SetChangesTracker(c, handler);
                }
            }
        }

        public static void SetEnabled(Control start, bool enabled)
        {
            foreach (Control c in start.Controls)
            {
                if (c is UltraTextEditor)
                {
                    c.Enabled = enabled;
                }
                else if (c is UltraCalendarCombo)
                {
                    c.Enabled = enabled;
                }
                else if (c is UltraMaskedEdit)
                {
                    c.Enabled = enabled;
                }
                else if (c is UltraOptionSet)
                {
                    c.Enabled = enabled;
                }
                else if (c is UltraComboEditor)
                {
                    c.Enabled = enabled;
                }
                else if (c is UltraCheckEditor)
                {
                    c.Enabled = enabled;
                }
                else if (c is UltraButton)
                {
                    c.Enabled = enabled;
                }
                else if (c is GridControl)
                {
                    var ctrl = (GridControl)c;
                    ctrl.SetReadOnlyState(!enabled);
                }
                else
                {
                    SetEnabled(c, enabled);
                }
            }
        }
    }
}