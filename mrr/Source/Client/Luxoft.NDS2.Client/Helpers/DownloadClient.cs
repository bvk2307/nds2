﻿using Luxoft.NDS2.Common.Contracts.Services;
using System;
using System.IO;

namespace Luxoft.NDS2.Client.Helpers
{
    /// <summary>
    /// Информация о ходе скачивания файла
    /// </summary>
    internal class DownloadProgressInfo
    {
        /// <summary>
        /// Приянто байт
        /// </summary>
        public long BytesRecieved { get; set; }

        /// <summary>
        /// Принято блоков
        /// </summary>
        public int ChunkRecieved { get; set; }

        /// <summary>
        /// Неудачных блоков
        /// </summary>
        public int ChunkFailed { get; set; }
    }

    /// <summary>
    /// Клиентская реализация скачивания файла с сервера
    /// </summary>
    internal class DownloadClient
    {
        private readonly IFileService _service;

        public DownloadClient(IFileService service)
        {
            _service = service;
            MaxChunkDownloadAttempt = 5;
            ChunkSize = 131072;
        }

        /// <summary>
        /// Количество неудачных попыток, после которых скачивание будет прервано
        /// </summary>
        public uint MaxChunkDownloadAttempt { get; set; }

        /// <summary>
        /// Размер блока, скачиваемого за раз
        /// </summary>
        public uint ChunkSize { get; set; }

        /// <summary>
        /// Скачивание файла
        /// </summary>
        /// <param name="sourcePath">Полный путь к файлу на сервере</param>
        /// <param name="targetPath">Полный путь к файлу на клиенте</param>
        /// <param name="progress">Делегат для обновления состояния скачивания</param>
        /// <returns>True если удачно скачан, False если скачать не удалось</returns>
        public bool TryDownload(string sourcePath, string targetPath, Action<DownloadProgressInfo> progress)
        {
            var bytesReceived = 0;
            var chunkFailures = 0;
            var chunkReceived = 0;

            var size = _service.GetSize(sourcePath);

            if (size < 0)
                return false;

            var dir = Path.GetDirectoryName(targetPath);
            if (!string.IsNullOrEmpty(dir))
                Directory.CreateDirectory(dir);

            using (var stream = new FileStream(targetPath, FileMode.Create))
            {
                do
                {
                    try
                    {
                        var data =
                            _service.GetChunk(sourcePath, bytesReceived, (int)ChunkSize);

                        if (data.Length < ChunkSize
                            && bytesReceived + data.Length < size)
                        {
                            chunkFailures++;
                        }
                        else
                        {
                            chunkFailures = 0;
                            chunkReceived += 1;
                            stream.Write(data, 0, data.Length);
                            bytesReceived += data.Length;
                        }
                    }
                    catch
                    {
                        chunkFailures++;
                    }

                    if (progress != null)
                        progress(new DownloadProgressInfo
                        {
                            BytesRecieved = bytesReceived,
                            ChunkFailed = chunkFailures,
                            ChunkRecieved = chunkReceived
                        });

                    if (chunkFailures >= MaxChunkDownloadAttempt)
                    {
                        stream.Close();
                        return false;
                    }
                } while (bytesReceived < size);

                stream.Close();
            }

            return true;
        }
    }
}

