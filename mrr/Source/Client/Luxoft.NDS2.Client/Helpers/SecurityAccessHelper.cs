﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonComponents.Security.Authorization;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Security;
using Luxoft.NDS2.Common.Contracts.Services;

namespace Luxoft.NDS2.Client.Helpers
{
    public class SecurityAccessHelper
    {
        private ISecurityService _service;
        private const string ACCESS_RIGHTS_KEY = "NDS2_AccessRigths";
        //private Common.Helpers.Cache.AppCache _cache;


        public SecurityAccessHelper(ISecurityService service)
        {
            _service = service;
            //_cache = Common.Helpers.Cache.AppCache.Current;
        }

        public bool IsInRole(string roleName)
        {
            return true;// GetAccessRights().Any(p => p.PermType == PermissionType.Operation && p.Name == roleName);
        }

//        private AccessRight[]  GetAccessRights()
//        {
//            var rights = _service.Get(ACCESS_RIGHTS_KEY) as AccessRight[];
//            if(rights == null)
//            {
//                CacheItemPolicy p = new CacheItemPolicy();
//                p.AbsoluteExpiration = new DateTimeOffset(DateTime.Now.AddHours(1));
//                rights = _service.GetUserRoles();
//                _cache.Add(ACCESS_RIGHTS_KEY, rights, p);
//            }
//
//            return rights;
//        }
    }
}
