﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using CommonComponents.Utils.Async;
//using Luxoft.NDS2.Client.UI.Proposals.BookSearch.CacheSaver;
//using Luxoft.NDS2.Common.ServiceAgents;
//
//namespace Luxoft.NDS2.Client.Helpers
//{
//    public class RequestDataWorker
//    {
//        public bool AllDataCaptured { get; private set; }
//
//        public bool HasFaults { get; set; }
//        public string FaultMessage { get; set; }
//
//        private AsyncWorker<bool, Func<SovResult>> _internalWorker;
//        public Func<SovResult> WorkDelegate { get; set; }
//        public string CacheKey { get; set; }
//        public JsonCacheSaver CacheSaver { get; set; }
//        public long DataSize { get; private set; }
//        public long CurrentPosition { get; private set; }
//        public long TotalObjectsInCache { get; private set; }
//
//        public event EventHandler OnComplete;
//
//        public event EventHandler OnError;
//
//        public RequestDataWorker()
//        {
//            AllDataCaptured = false;
//        }
//
//        public void Cancel()
//        {
//            if (CacheSaver != null)
//            {
//                CacheSaver.Cancel();
//            }
//        }
//
//        public void Run()
//        {
//            if (_internalWorker != null) return;
//
//            if (WorkDelegate == null) throw new ArgumentNullException("WorkDelegate");
//
//            _internalWorker = new AsyncWorker<bool, Func<SovResult>>(WorkDelegate);
//
//            _internalWorker.DoWork += (sender, args) =>
//                {
//                    try
//                    {
//                        CacheSaver.OnStateChanged += (o, eventArgs) =>
//                        {
//                            TotalObjectsInCache = CacheSaver.TotalObjectsSaved;
//                        };
//
//                        var response = args.Parameter();
//                        DataSize = response.Length;
//                        CacheSaver.Save(CacheKey, response.StreamData);
//                        
//                        AllDataCaptured = true;
//                        args.Result = true;
//
//                        if(OnComplete != null)
//                        {
//                            OnComplete(this, EventArgs.Empty);
//                        }
//                    }
//                    catch (Exception ex)
//                    {
//                        FaultMessage = ex.Message;
//                        HasFaults = true;
//                        args.Result = false;
//                        if(OnError != null)
//                        {
//                            OnError(this, EventArgs.Empty);
//                        }
//                    }
//                };
//
//            _internalWorker.Start();
//        }
//    }
//}
//
