﻿using System;
using System.Text;

namespace Luxoft.NDS2.Client.Helpers
{
    public interface IFormatter
    {
        string GetStringFormat(Type type);
    }

    /// <summary>
    /// Значение 0 или null любого типа возвращает в виде "0"
    /// </summary>
    public class IntFormatValueZeroFormatter : IFormatProvider, ICustomFormatter, IFormatter
    {
        private string _intFormat = "N";

        public object GetFormat(Type formatType)
        {
            return formatType == typeof(ICustomFormatter) ? this : null;
        }

        public string Format(string format,
                              object arg,
                              IFormatProvider formatProvider)
        {
            if (!this.Equals(formatProvider))
            {
                return null;
            }
            else if (arg == null) { return "0"; }
            else
            {
                return arg.ToString();
            }
        }

        public string GetStringFormat(Type type)
        {
            if (type == typeof(int))
            {
                return "{0:" + _intFormat + "}";
            }

            return null;
        }
    }

    /// <summary>
    /// Значение null любого типа возвращает в виде ""
    /// </summary>
    public class IntFormatValueNullFormatter : IFormatProvider, ICustomFormatter, IFormatter
    {
        private string _intFormat = "N";

        public object GetFormat(Type formatType)
        {
            return formatType == typeof(ICustomFormatter) ? this : null;
        }

        public string Format(string format,
                              object arg,
                              IFormatProvider formatProvider)
        {
            if (!this.Equals(formatProvider))
            {
                return null;
            }
            else if (arg == null) { return String.Empty; }
            else
            {
                return arg.ToString();
            }
        }

        public string GetStringFormat(Type type)
        {
            if (type == typeof(int))
            {
                return "{0:" + _intFormat + "}";
            }

            return null;
        }
    }

    public class IntValueZeroFormatter : IFormatProvider, ICustomFormatter, IFormatter
    {
        private string _decimalFormat = "N";

        public object GetFormat(Type formatType) {
            return formatType == typeof(ICustomFormatter) ? this : null;
        }

        public string Format(string format,
                              object arg,
                              IFormatProvider formatProvider)
        {
            if (!this.Equals(formatProvider))
            {
                return null;
            }
            else
            {
                string ret = String.Empty;
                string formatCurrent = String.Empty;

                if (!string.IsNullOrWhiteSpace(format))
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("{");
                    sb.Append("0:");
                    sb.Append(format);
                    sb.Append("}");
                    formatCurrent = sb.ToString();
                }
                else
                {
                    formatCurrent = "{0:0,0.00}";
                }

                Type type = arg.GetType();
                if (type == typeof(int))
                {
                    int intValue = (int)arg;
                    if (intValue != 0)
                    {
                        ret = intValue.ToString(); 
                    }
                }
                else if (type == typeof(long))
                {
                    long longValue = (long)arg;
                    if (longValue != 0)
                    {
                        ret = longValue.ToString(); 
                    }
                }
                else if (type == typeof(decimal))
                {
                    decimal decValue = (decimal)arg;
                    if (decValue != 0)
                    {
                        ret = decValue.ToString("N"); 
                    }
                }
                return ret;
            }
        }

        public string GetStringFormat(Type type)
        {
            if (type == typeof(decimal))
            {
                return "{0:" + _decimalFormat + "}";
            }

            return null;
        }
    }

    public class ControlValueZeroFormatter : IFormatProvider, ICustomFormatter, IFormatter
    {
        private string _decimalFormat = "N";
        private string _longFormat = "N";
        private string _intFormat = "N";

        public object GetFormat(Type formatType)
        {
            return formatType == typeof(ICustomFormatter) ? this : null;
        }

        public string Format(string format,
                              object arg,
                              IFormatProvider formatProvider)
        {            
            if (!this.Equals(formatProvider))
            {
                return null;
            }
            else
            {
                string ret = String.Empty;
                string formatCurrent = String.Empty;

                if (!string.IsNullOrWhiteSpace(format))
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("{");
                    sb.Append("0:");
                    sb.Append(format);
                    sb.Append("}");
                    formatCurrent = sb.ToString();
                }
                else
                {
                    formatCurrent = "{0:0,0.00}";
                }

                Type type = arg.GetType();
                if (type == typeof(int))
                {
                    int intValue = (int)arg;
                    if (intValue != 0)
                    {
                        ret = intValue.ToString(_intFormat);
                    }
                }
                else if (type == typeof(long))
                {
                    long longValue = (long)arg;
                    if (longValue != 0)
                    {
                        ret = longValue.ToString(_longFormat);
                    }
                }
                else if (type == typeof(decimal))
                {
                    decimal decValue = (decimal)arg;
                    if (decValue != 0)
                    {
                        ret = decValue.ToString(_decimalFormat);
                    }
                }
                return ret;
            }
        }

        public string GetStringFormat(Type type)
        {

            if (type == typeof(decimal))
            {
                return "{0:" + _decimalFormat + "}";
            }
            if (type == typeof(int))
            {
                return "{0:" + _intFormat + "}";
            }
            if (type == typeof(long))
            {
                return "{0:" + _longFormat + "}";
            }

            return null;
        }
    }

    public class PriceShowZeroFormatter : IFormatProvider, ICustomFormatter, IFormatter
    {
        private string _decimalFormat = "N";

        public object GetFormat(Type formatType)
        {
            return formatType == typeof(ICustomFormatter) ? this : null;
        }

        public string Format(string format,
                              object arg,
                              IFormatProvider formatProvider)
        {
            if (!this.Equals(formatProvider))
            {
                return null;
            }
            else
            {
                string ret = String.Empty;
                string formatCurrent = String.Empty;

                if (!string.IsNullOrWhiteSpace(format))
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("{");
                    sb.Append("0:");
                    sb.Append(format);
                    sb.Append("}");
                    formatCurrent = sb.ToString();
                }
                else
                {
                    formatCurrent = "{0:0,0.00}";
                }

                Type type = arg.GetType();
                if (type == typeof(int))
                {
                    int intValue = (int)arg;
                    ret = intValue.ToString();
                }
                else if (type == typeof(long))
                {
                    long longValue = (long)arg;
                    ret = longValue.ToString();
                }
                else if (type == typeof(decimal))
                {
                    decimal decValue = (decimal)arg;
                    if (decValue != 0)
                    {
                        ret = decValue.ToString(_decimalFormat);
                    }
                    else
                    {
                        ret = decValue.ToString();
                    }
                }
                return ret;
            }
        }

        public string GetStringFormat(Type type)
        {
            if (type == typeof(decimal))
            {
                return "{0:"+_decimalFormat+"}";
            }

            return null;
        }
    }
}
