﻿using JetBrains.Annotations;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;

namespace Luxoft.NDS2.Client.Helpers
{
    /// <summary>
    /// Тип выборки файла (фильтр)
    /// </summary>
    public enum SelectFileType
    {
        /// <summary>
        /// Фильтр картинок
        /// </summary>
        IMG,
        /// <summary>
        /// Фильтр Текстовый
        /// </summary>
        TXT,
        /// <summary>
        /// Фильтр XML
        /// </summary>
        XML,
        /// <summary>
        /// Фильтр Excel
        /// </summary>
        XLS,
        /// <summary>
        /// Фильтр OOXML
        /// </summary>
        XLSX,
        /// <summary>
        /// Фильтр Pdf
        /// </summary>
        PDF,
        /// <summary>
        /// Фильтр ВСЕ
        /// </summary>
        ALL
    }

    public static class DialogHelper
    {
        # region Выбор файла

        private static Dictionary<SelectFileType, string> FileTypes =
            new Dictionary<SelectFileType, string>
            {
                { SelectFileType.ALL, "Все файлы (*.*)|*.*" },
                { SelectFileType.IMG, "Изображения (*.jpg)|*.jpg" },
                { SelectFileType.PDF, "Pdf файлы (*.pdf)|*.pdf" },
                { SelectFileType.TXT, "Текстовые файлы (*.txt)|*.txt" },
                { SelectFileType.XLS, "Файлы MS Excel 2003 (*.xls)|*.xls" },
                { SelectFileType.XLSX, "Файлы MS Excel (*.xlsx)|*.xlsx" },
                { SelectFileType.XML, "XML файлы (*.xml)|*.xml" }
            };

        /// <summary>
        /// Выбрать любой файл
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static bool SelectFile(out string fileName)
        {
            return SelectFile(SelectFileType.ALL, out fileName);
        }

        /// <summary>
        /// Выбрать файл c маской
        /// </summary>
        /// <param name="type"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static bool SelectFile(SelectFileType type, out string fileName)
        {
            fileName = string.Empty;
            var d = new OpenFileDialog { Filter = FileTypes[type] };
            if (d.ShowDialog() == true)
            {
                fileName = d.FileName;
                return true;
            }
            return false;
        }

        /// <summary>
        /// Выбрать файл c маской
        /// </summary>
        /// <param name="type"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static bool GetFileToSave(SelectFileType type, ref string fileName)
        {
            var dialog = type.GetSaveFileDialog(fileName);

            if (dialog.ShowDialog() != true)
            {                
                return false;
            }

            fileName = dialog.FileName;
            return true;
        }

        /// <summary>
        /// Выбрать файл и контролировать возможность доступа на запись
        /// </summary>
        /// <param name="type"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static bool SelectFileToOverwrite(SelectFileType type, ref string fileName)
        {
            var dialog = type.GetSaveFileDialog(fileName);
            dialog.FileOk += TryOpenForWrite;

            var ret = dialog.ShowDialog();
            if (ret == true)
                fileName = dialog.FileName;

            return (ret == true);
        }

        private static SaveFileDialog GetSaveFileDialog(this SelectFileType type, string fileName)
        {
            return new SaveFileDialog
            {
                Filter = FileTypes[type],
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                FileName = fileName
            };
        }

        private static void TryOpenForWrite(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var filename = (sender as SaveFileDialog).FileName;
            if (!File.Exists(filename)) return;
            try
            {
                using (File.OpenWrite(filename))
                { }
            }
            catch (Exception ex)
            {
                if ((ex is IOException) || (ex is UnauthorizedAccessException))
                {
                    e.Cancel = true;
                    Warning("Файл \"{0}\" не доступен для записи.\r\n" +
                            "Выберете другое имя файла или закройте использующее файл приложение.", filename);
                }
                else throw;
            }
        }

        # endregion

        [StringFormatMethod("messageFormat")]
        [Obsolete]
        public static bool ConfirmWithTitle(string title, string messageFormat, params object[] args)
        {
            return MessageBox.Show(string.Format(messageFormat, args), title, MessageBoxButton.YesNo,
                MessageBoxImage.Question) == MessageBoxResult.Yes;
        }

        [StringFormatMethod("messageFormat")]
        [Obsolete]
        public static bool Confirm(string messageFormat, params object[] args)
        {
            return ConfirmWithTitle("Подтверждение", messageFormat, args);
        }

        [Obsolete]
        public static bool Confirm(string message)
        {
            return MessageBox.Show(message, "Подтверждение", MessageBoxButton.YesNo,
                MessageBoxImage.Question) == MessageBoxResult.Yes;
        }

        [StringFormatMethod("messageFormat")]
        [Obsolete]
        public static void Inform(string messageFormat, params object[] args)
        {
            MessageBox.Show(string.Format(messageFormat, args), "Информация", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        [Obsolete]
        public static void Inform(string message)
        {
            MessageBox.Show(message, "Информация", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        [StringFormatMethod("messageFormat")]
        [Obsolete]
        public static void Warning(string messageFormat, params object[] args)
        {
            WarningWithTitle("Внимание", messageFormat, args);
        }

        [Obsolete]
        public static void Warning(string message)
        {
            MessageBox.Show(message, "Внимание", MessageBoxButton.OK, MessageBoxImage.Warning);
        }

        [StringFormatMethod("messageFormat")]
        [Obsolete]
        public static void WarningWithTitle(string title, string messageFormat, params object[] args)
        {
            MessageBox.Show(string.Format(messageFormat, args), title, MessageBoxButton.OK, MessageBoxImage.Warning);
        }

        [StringFormatMethod("messageFormat")]
        [Obsolete]
        public static void Error(string messageFormat, params object[] args)
        {
            MessageBox.Show(string.Format(messageFormat, args), "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        [Obsolete]
        public static void Error(string message)
        {
            MessageBox.Show(message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
        }       
        
    }
}
