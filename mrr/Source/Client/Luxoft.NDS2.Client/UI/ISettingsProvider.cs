﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI
{
    [Obsolete]
    /// <summary>
    /// Интерфейс провайдера пользовательских локальных настроек
    /// </summary>
    public interface ISettingsProvider
    {
        void SaveSettings(string valueSetup, string settingsKey = "settings");

        string LoadSettings(string settingsKey = "settings");

        void NotifySuccess();

        void LogSuccess();
    }
}
