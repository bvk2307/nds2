﻿using System;

namespace Luxoft.NDS2.Client.UI
{
    public interface ILogger
    {
        void LogError(string message, string methodName = null, Exception ex = null);

        void LogWarning(string message, string methodName = null);

        void LogNotification(string message, string methodName = null);
    }
}
