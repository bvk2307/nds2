﻿namespace Luxoft.NDS2.Client.UI.Navigator
{
    /// <summary>
    /// Описывает представление ввода параметров отчета "Навигатор"
    /// </summary>
    public interface IReportSetupView : IView
    {
    }
}
