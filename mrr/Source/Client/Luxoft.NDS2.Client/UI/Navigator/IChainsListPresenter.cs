﻿namespace Luxoft.NDS2.Client.UI.Navigator
{
    public interface IChainsListPresenter
    {
        Models.ReportModel Model
        {
            get;
        }

        void StartLoading();

        void OpenChainContractorsView(Models.PairModel pair);

        void OpenChainView(Models.ChainSummaryModel chain);
    }
}
