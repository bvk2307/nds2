﻿using CommonComponents.Uc.Infrastructure.Interface;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Base.ChainsViewRestriction;
using Luxoft.NDS2.Client.UI.Base.Navigation;
using Luxoft.NDS2.Common.Contracts.Services;
using Microsoft.Practices.CompositeUI;
using System;

namespace Luxoft.NDS2.Client.UI.Navigator
{
    public class PresenterMaster<TView> : BasePresenter<TView>
        where TView : BaseView
    {
        public PresenterMaster()
        {
        }

        public TPresenter Resolve<TPresenter>(object model = null)
            where TPresenter : class
        {

            if (typeof(TPresenter) == typeof(IReportSetupPresenter))
            {
                return new Presenters.ReportSetupPresenter(
                    new Models.ReportSetupModel(Sur, UserRestrictionConfig),
                    GetServiceProxy<INavigatorReportDataService>(),
                    new EkpTabOpener(WorkItem),
                    new TaxPayerDetailsViewer(
                        WorkItem, 
                        new ChainAccessModel(new DirectionCreator(UserRestrictionConfig)))) as TPresenter;
            }

            if (typeof(TPresenter) == typeof(ITaxPayersSelectPresenter))
            {
                return 
                    new Presenters.TaxPayersSelectPresenter(
                        model as Models.ReportSetupModel,
                        GetServiceProxy<INavigatorReportDataService>()) 
                    as TPresenter;
            }

            if (typeof(TPresenter) == typeof(IChainsListPresenter))
            {
                return new Presenters.ChainListPresenter(
                    model as Models.ReportModel,
                    GetServiceProxy<INavigatorReportDataService>(),
                    new EkpTabOpener(WorkItem)) as TPresenter;
            }

            if (typeof(TPresenter) == typeof(IChainContractorsListPresenter))
            {
                return new Presenters.ChainContractorsListPresenter(
                    model as Models.ContractorsListModel,
                    GetServiceProxy<INavigatorReportDataService>(),
                    new EkpTabOpener(WorkItem),
                    new TaxPayerDetailsViewer(
                        WorkItem,
                        new ChainAccessModel(new DirectionCreator(UserRestrictionConfig)))) as TPresenter;
            }

            if (typeof(TPresenter) == typeof(IChainPresenter))
            {
                return new Presenters.ChainPresenter(
                    model as Models.ChainModel,
                    GetServiceProxy<INavigatorReportDataService>(),
                    new TaxPayerDetailsViewer(
                        WorkItem,
                        new ChainAccessModel(new DirectionCreator(UserRestrictionConfig))))
                            as TPresenter;
            }

            throw new NotSupportedException();
        }
    }
}
