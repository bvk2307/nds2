﻿namespace Luxoft.NDS2.Client.UI.Navigator
{
    public interface IView
    {
        string Title
        {
            get;
        }

        void ExportToExcel();
    }
}
