﻿namespace Luxoft.NDS2.Client.UI.Navigator
{
    public interface IChainContractorsListPresenter
    {
        Models.ContractorsListModel Model
        {
            get;
        }

        void StartLoading();

        void ViewDetails();

        void ViewChain(long chainId);
    }
}
