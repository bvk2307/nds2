﻿namespace Luxoft.NDS2.Client.UI.Navigator.Views
{
    partial class TaxPayersSelectView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Misc.UltraPanel _actionsPanel;
            Infragistics.Win.Misc.UltraButton _cancelButton;
            Infragistics.Win.Misc.UltraButton _acceptButton;
            this._period = new Infragistics.Win.Misc.UltraLabel();
            this._taxPayersListRequirement = new Infragistics.Win.Misc.UltraLabel();
            this._taxPayersGroupBox = new Infragistics.Win.Misc.UltraGroupBox();
            this._taxPayers = new Luxoft.NDS2.Client.UI.Controls.Grid.V2.DataGridView();
            _actionsPanel = new Infragistics.Win.Misc.UltraPanel();
            _cancelButton = new Infragistics.Win.Misc.UltraButton();
            _acceptButton = new Infragistics.Win.Misc.UltraButton();
            _actionsPanel.ClientArea.SuspendLayout();
            _actionsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._taxPayersGroupBox)).BeginInit();
            this._taxPayersGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // _actionsPanel
            // 
            // 
            // _actionsPanel.ClientArea
            // 
            _actionsPanel.ClientArea.Controls.Add(_cancelButton);
            _actionsPanel.ClientArea.Controls.Add(_acceptButton);
            _actionsPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            _actionsPanel.Location = new System.Drawing.Point(0, 328);
            _actionsPanel.Name = "_actionsPanel";
            _actionsPanel.Size = new System.Drawing.Size(792, 45);
            _actionsPanel.TabIndex = 5;
            // 
            // _cancelButton
            // 
            _cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            _cancelButton.Location = new System.Drawing.Point(664, 3);
            _cancelButton.Name = "_cancelButton";
            _cancelButton.Size = new System.Drawing.Size(100, 23);
            _cancelButton.TabIndex = 1;
            _cancelButton.Text = "Отмена";
            _cancelButton.Click += new System.EventHandler(this.OnCancel);
            // 
            // _acceptButton
            // 
            _acceptButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            _acceptButton.Location = new System.Drawing.Point(556, 3);
            _acceptButton.Margin = new System.Windows.Forms.Padding(3, 3, 5, 3);
            _acceptButton.Name = "_acceptButton";
            _acceptButton.Size = new System.Drawing.Size(100, 23);
            _acceptButton.TabIndex = 0;
            _acceptButton.Text = "Выбрать";
            _acceptButton.Click += new System.EventHandler(this.OnAccept);
            // 
            // _period
            // 
            this._period.Dock = System.Windows.Forms.DockStyle.Top;
            this._period.Location = new System.Drawing.Point(0, 10);
            this._period.Name = "_period";
            this._period.Size = new System.Drawing.Size(792, 23);
            this._period.TabIndex = 3;
            // 
            // _taxPayersListRequirement
            // 
            this._taxPayersListRequirement.Dock = System.Windows.Forms.DockStyle.Top;
            this._taxPayersListRequirement.Location = new System.Drawing.Point(0, 33);
            this._taxPayersListRequirement.Name = "_taxPayersListRequirement";
            this._taxPayersListRequirement.Size = new System.Drawing.Size(792, 23);
            this._taxPayersListRequirement.TabIndex = 4;
            // 
            // _taxPayersGroupBox
            // 
            this._taxPayersGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._taxPayersGroupBox.Controls.Add(this._taxPayers);
            this._taxPayersGroupBox.Location = new System.Drawing.Point(10, 62);
            this._taxPayersGroupBox.Margin = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this._taxPayersGroupBox.Name = "_taxPayersGroupBox";
            this._taxPayersGroupBox.Size = new System.Drawing.Size(772, 262);
            this._taxPayersGroupBox.TabIndex = 7;
            this._taxPayersGroupBox.Text = "Налогоплательщики";
            // 
            // _taxPayers
            // 
            this._taxPayers.AggregatePanelVisible = false;
            this._taxPayers.AllowFilterReset = false;
            this._taxPayers.AllowMultiGrouping = false;
            this._taxPayers.AllowResetSettings = false;
            this._taxPayers.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this._taxPayers.BackColor = System.Drawing.Color.Transparent;
            this._taxPayers.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.None;
            this._taxPayers.Dock = System.Windows.Forms.DockStyle.Fill;
            this._taxPayers.ExportExcelCancelVisible = false;
            this._taxPayers.ExportExcelVisible = false;
            this._taxPayers.FilterResetVisible = false;
            this._taxPayers.FooterVisible = true;
            this._taxPayers.GridContextMenuStrip = null;
            this._taxPayers.Location = new System.Drawing.Point(3, 16);
            this._taxPayers.Name = "_taxPayers";
            this._taxPayers.PanelExportExcelStateVisible = false;
            this._taxPayers.PanelLoadingVisible = false;
            this._taxPayers.PanelPagesVisible = true;
            this._taxPayers.RowDoubleClicked = null;
            this._taxPayers.Size = new System.Drawing.Size(766, 243);
            this._taxPayers.TabIndex = 6;
            this._taxPayers.Title = "";
            // 
            // TaxPayersSelectView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(792, 373);
            this.Controls.Add(this._taxPayersGroupBox);
            this.Controls.Add(_actionsPanel);
            this.Controls.Add(this._taxPayersListRequirement);
            this.Controls.Add(this._period);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(600, 400);
            this.Name = "TaxPayersSelectView";
            this.Padding = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Навигатор - выбор налогоплательщиков";
            this.Load += new System.EventHandler(this.OnLoad);
            _actionsPanel.ClientArea.ResumeLayout(false);
            _actionsPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._taxPayersGroupBox)).EndInit();
            this._taxPayersGroupBox.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraLabel _period;
        private Controls.Grid.V2.DataGridView _taxPayers;
        private Infragistics.Win.Misc.UltraLabel _taxPayersListRequirement;
        private Infragistics.Win.Misc.UltraGroupBox _taxPayersGroupBox;

    }
}