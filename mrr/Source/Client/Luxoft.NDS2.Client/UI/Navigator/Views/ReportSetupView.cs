﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Infragistics.Win;
using Luxoft.NDS2.Client.Extensions;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinMaskedEdit;

namespace Luxoft.NDS2.Client.UI.Navigator.Views
{
    public partial class ReportSetupView : UserControl, IReportSetupView
    {
        # region Поля MVP

        private readonly IReportSetupPresenter _presenter;

        # endregion

        # region Дочерние представления

        private readonly ITaxPayersSelectView _taxPayersSelect;

        # endregion

        # region Средства ЕКП

        private readonly INotifier _notifier;

        # endregion

        # region Конструкторы

        /// <summary>
        /// Создает экземпляр представления ввода параметров отчета 
        /// Только в режиме редактора
        /// </summary>
        public ReportSetupView()
        {
            InitializeComponent();
        }

        public ReportSetupView(
            IReportSetupPresenter presenter,
            ITaxPayersSelectView taxPayersSelect,
            INotifier notifier)
            : this()
        {
            _presenter = presenter;
            _taxPayersSelect = taxPayersSelect;
            _notifier = notifier;

            InitGrid();
            SetViewData();
            BindModelEvents();
        }

        # endregion

        # region Реализация IView

        public string Title
        {
            get
            {
                return "Навигатор - параметры";
            }
        }

        public void ExportToExcel()
        {
            throw new NotSupportedException();
        }

        # endregion

        # region Инициализация гридов

        private void InitGrid()
        {
            var helper = new ColumnHelper<Models.ReportTaxPayerModel>(_taxPayers);
            var setup = new GridColumnSetup();

            setup.Columns.Add(
                helper
                    .CreateTextColumn(model => model.Inn, 1, 80)
                    .Configure(col => { col.DisableSort = true; col.DisableFilter = true; }));
            setup.Columns.Add(
                helper
                    .CreateTextColumn(model => model.Kpp, 1, 80)
                    .Configure(col => { col.DisableSort = true; col.DisableFilter = true; }));
            setup.Columns.Add(
                helper
                    .CreateTextColumn(model => model.Name, 1, 200)
                    .Configure(col => { col.DisableSort = true; col.DisableFilter = true; }));
            setup.Columns.Add(
                helper
                    .CreateTextColumn(model => model.Region, 1, 200)
                    .Configure(col => { col.DisableSort = true; col.DisableFilter = true; }));
            setup.Columns.Add(
                helper
                    .CreateTextColumn(model => model.Inspection, 1, 200)
                    .Configure(col => { col.DisableSort = true; col.DisableFilter = true; }));

            _taxPayers.InitColumns(setup);

            _taxPayers.Grid.AfterSelectChange += OnTaxPayerSelectedChange;
            _taxPayers.Grid.AfterRowActivate += OnRowActivate;

            _taxPayers.Grid.DisplayLayout.Override.SelectedRowAppearance.BackColor =
                SystemColors.Highlight;
            _taxPayers.Grid.DisplayLayout.Override.SelectedRowAppearance.ForeColor =
                SystemColors.HighlightText;
            _taxPayers.Grid.DisplayLayout.Override.SelectTypeRow = SelectType.Extended;

            _taxPayers.Grid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
        }

        # endregion

        # region Привязка к событиям модели

        private void BindModelEvents()
        {
            _presenter.Model.Period.PeriodStateChanged += SetPeriodState;
            _presenter.Model.TaxPayers.ReportTaxPayersChanged += SetReportTaxPayersList;
            _presenter.Model.TaxPayers.SelectedQuantityChanged += SetExcludeButtonAvailability;
            _presenter.Model.OnValidationAttributeChanged += SetSubmitButtonAvailability;
            _presenter.Model.OnMaxContractorsValidChanged += SetMaxContractorsError;
            _presenter.Model.OnInputNeededValidChanged += SetInputNeededError;
            _presenter.Model.OnError += _notifier.ShowError;
        }

        # endregion

        # region Отображение данных модели

        private void SetViewData()
        {
            SetPeriodMonthItems();
            SetPeriodYearItems();
            SetDepthLevelItems();

            _periodFromMonthSelect.SelectPeriod(_presenter.Model.Period.StartFrom.Month);
            _periodFromYearSelect.SelectPeriod(_presenter.Model.Period.StartFrom.Year);
            _periodToMonthSelect.SelectPeriod(_presenter.Model.Period.EndWith.Month);
            _periodToYearSelect.SelectPeriod(_presenter.Model.Period.EndWith.Year);
            _taxPayersRequirementLabel.Text = _presenter.Model.TaxPayers.State.Requirement;
            _depthSelect.Select(_presenter.Model.Depth);

            _minMappedAmountEditor.Value = _presenter.Model.MinMappedAmount;
            _maxContractorsEditor.Value = _presenter.Model.MaxContractorsQuantity;
            _buyerAmountEditor.Value = _presenter.Model.MinBuyerAmount;
            _sellerAmountEditor.Value = _presenter.Model.MinSellerAmount;

            SetMaxContractorsError();
            SetInputNeededError();
            SetPeriodState();
            SetExcludeButtonAvailability();
            SetSubmitButtonAvailability();
        }

        private void SetPeriodMonthItems()
        {
            var allMonths = _presenter.Model.Period.AvailableMonths;

            _periodFromMonthSelect.Items.Clear();
            _periodToMonthSelect.Items.Clear();

            _periodFromMonthSelect.Items.AddRange(allMonths.ToListItems());
            _periodToMonthSelect.Items.AddRange(allMonths.ToListItems());
        }

        private void SetPeriodYearItems()
        {
            var allYears = _presenter.Model.Period.AvailableYears;

            _periodFromYearSelect.Items.Clear();
            _periodToYearSelect.Items.Clear();

            _periodFromYearSelect.Items.AddRange(allYears.ToListItems());
            _periodToYearSelect.Items.AddRange(allYears.ToListItems());
        }

        private void SetDepthLevelItems()
        {
            _depthSelect.Items.Clear();
            _depthSelect
                .Items
                .AddRange(
                    _presenter.Model.AvailableDepths.ToListItems());
        }

        private void SetPeriodState()
        {
            _periodValidationError.Visible = !_presenter.Model.Period.State.Valid;
            _periodValidationError.Text = _presenter.Model.Period.State.Text;
        }

        private void SetExcludeButtonAvailability()
        {
            _excludeTaxPayesrButton.Enabled =
                _presenter.Model.TaxPayers.Included.Any(model => model.Selected);
        }

        private void SetSubmitButtonAvailability()
        {
            _submitButton.Enabled = _presenter.Model.Valid;
        }

        private void SetReportTaxPayersList()
        {
            _taxPayers.PushData(_presenter.Model.TaxPayers.Included);
            _taxPayersError.Text = _presenter.Model.TaxPayers.State.Text;
        }

        private void SetMaxContractorsError()
        {
            _maxContractorsError.Visible = !_presenter.Model.MaxContractorsValid;
            _maxContractorsError.Text = _presenter.Model.MaxContractorsErrorMessage;
        }

        private void SetInputNeededError()
        {
            _inputNeededError.Visible = !_presenter.Model.InputNeededValid;
            _inputNeededError.Text = _presenter.Model.InputNeededErrorMessage;
        }

        # endregion

        # region Обработка событий

        private void OnPeriodFromMonthChanged(object sender, EventArgs e)
        {
            _presenter.Model.Period.StartFrom.Month.Value =
                _periodFromMonthSelect.GetValue<Models.MonthModel>().Value;
        }

        private void OnPeriodFromYearChanged(object sender, EventArgs e)
        {
            _presenter.Model.Period.StartFrom.Year.Value =
                _periodFromYearSelect.GetValue<Models.YearModel>().Value;
        }

        private void OnPeriodToMonthChanged(object sender, EventArgs e)
        {
            _presenter.Model.Period.EndWith.Month.Value =
                _periodToMonthSelect.GetValue<Models.MonthModel>().Value;
        }

        private void OnPeriodToYearChanged(object sender, EventArgs e)
        {
            _presenter.Model.Period.EndWith.Year.Value =
                _periodToYearSelect.GetValue<Models.YearModel>().Value;
        }

        private void BeforeTaxPayersModifying(object sender, EventArgs e)
        {
            _taxPayersSelect.ShowDialog();
        }

        private void OnTaxPayerCheckedChange(object sender, MouseEventArgs e)
        {
            var element = _taxPayers.Grid.DisplayLayout.UIElement.ElementFromPoint(e.Location);
            var row = element.GetContext(typeof(UltraGridRow)) as UltraGridRow;

            if (_taxPayers.Grid.ActiveRow == null || row == null)
            {
                return;
            }

            var model = _taxPayers.Grid.ActiveRow.ListObject as Models.ReportTaxPayerModel;
            model.Selected = !model.Selected;
        }

        private void OnRowActivate(object sender, EventArgs e)
        {
            _taxPayers.Grid.ActiveRow.Selected = true;
        }

        private void OnTaxPayerSelectedChange(object sender, AfterSelectChangeEventArgs e)
        {
            foreach (var row in _taxPayers.Grid.Rows)
            {
                var taxPayer = row.ListObject as Models.ReportTaxPayerModel;
                taxPayer.Selected = row.Selected;
            }
        }

        private void OnSubmit(object sender, EventArgs e)
        {
            _presenter.Model.Depth = int.Parse(_depthSelect.SelectedItem.DataValue.ToString());
            _presenter.Model.MinMappedAmount = int.Parse(_minMappedAmountEditor.Value.ToString());
            _presenter.Model.MinBuyerAmount = int.Parse(_buyerAmountEditor.Value.ToString());
            _presenter.Model.MinSellerAmount = int.Parse(_sellerAmountEditor.Value.ToString());

            _presenter.Submit();
        }

        private void MinMappedAmountChanged(object sender, EventArgs e)
        {
            _presenter.Model.MinMappedAmount = _minMappedAmountEditor.Value.ToString().ToNullableInt32(); ;
        }

        private void MaxContractorsQuantityChanged(object sender, EventArgs e)
        {
            _presenter.Model.MaxContractorsQuantity = _maxContractorsEditor.Value.ToString().ToNullableInt32();
        }

        private void MinBuyerMappedAmountChanged(object sender, EventArgs e)
        {
            _presenter.Model.MinBuyerAmount = _buyerAmountEditor.Value.ToString().ToNullableInt32();
        }

        private void MinSellerAmountChanged(object sender, EventArgs e)
        {
            _presenter.Model.MinSellerAmount = _sellerAmountEditor.Value.ToString().ToNullableInt32();
        }
          
        private void ExcludeTaxPayers(object sender, EventArgs e)
        {
            _presenter.Model.TaxPayers.RemoveSelected();
        }

        private void ViewTaxPayerDetails(object sender, EventArgs e)
        {
            _presenter.ViewTaxPayerDetails();
        }

        # endregion
    }
}
