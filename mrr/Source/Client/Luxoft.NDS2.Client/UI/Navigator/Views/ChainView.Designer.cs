﻿namespace Luxoft.NDS2.Client.UI.Navigator.Views
{
    partial class ChainView
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Misc.UltraLabel _title;
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraPanel _dataPanel;
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraPanel _chainSummary;
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel _rub4;
            Infragistics.Win.Misc.UltraLabel _rub3;
            Infragistics.Win.Misc.UltraLabel _rub2;
            Infragistics.Win.Misc.UltraLabel _rub1;
            Infragistics.Win.Misc.UltraLabel _maxNotMappedAmountLabel;
            Infragistics.Win.Misc.UltraLabel _minNotMappedAmountLabel;
            Infragistics.Win.Misc.UltraLabel _MaxMappedAmountLabel;
            Infragistics.Win.Misc.UltraLabel _minMappedAmountLabel;
            Infragistics.Win.Misc.UltraGroupBox _chainContractorsBox;
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            System.Windows.Forms.ToolStripMenuItem _viewDetailsMenuItem;
            this._maxNotMappedAmount = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this._minNotMappedAmount = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this._maxMappedAmount = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this._minMappedAmount = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this._chainContractorsList = new Luxoft.NDS2.Client.UI.Controls.Grid.V2.GridView();
            this._menu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._selectFile = new System.Windows.Forms.SaveFileDialog();
            _title = new Infragistics.Win.Misc.UltraLabel();
            _dataPanel = new Infragistics.Win.Misc.UltraPanel();
            _chainSummary = new Infragistics.Win.Misc.UltraPanel();
            _rub4 = new Infragistics.Win.Misc.UltraLabel();
            _rub3 = new Infragistics.Win.Misc.UltraLabel();
            _rub2 = new Infragistics.Win.Misc.UltraLabel();
            _rub1 = new Infragistics.Win.Misc.UltraLabel();
            _maxNotMappedAmountLabel = new Infragistics.Win.Misc.UltraLabel();
            _minNotMappedAmountLabel = new Infragistics.Win.Misc.UltraLabel();
            _MaxMappedAmountLabel = new Infragistics.Win.Misc.UltraLabel();
            _minMappedAmountLabel = new Infragistics.Win.Misc.UltraLabel();
            _chainContractorsBox = new Infragistics.Win.Misc.UltraGroupBox();
            _viewDetailsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _dataPanel.ClientArea.SuspendLayout();
            _dataPanel.SuspendLayout();
            _chainSummary.ClientArea.SuspendLayout();
            _chainSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._maxNotMappedAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._minNotMappedAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._maxMappedAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._minMappedAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(_chainContractorsBox)).BeginInit();
            _chainContractorsBox.SuspendLayout();
            this._menu.SuspendLayout();
            this.SuspendLayout();
            // 
            // _title
            // 
            appearance2.FontData.BoldAsString = "True";
            _title.Appearance = appearance2;
            _title.Dock = System.Windows.Forms.DockStyle.Top;
            _title.Location = new System.Drawing.Point(10, 10);
            _title.Name = "_title";
            _title.Size = new System.Drawing.Size(580, 23);
            _title.TabIndex = 0;
            _title.Text = "Навигатор - обзор цепочки";
            // 
            // _dataPanel
            // 
            appearance3.BorderColor = System.Drawing.Color.Silver;
            _dataPanel.Appearance = appearance3;
            _dataPanel.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            // 
            // _dataPanel.ClientArea
            // 
            _dataPanel.ClientArea.Controls.Add(_chainSummary);
            _dataPanel.ClientArea.Controls.Add(_chainContractorsBox);
            _dataPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            _dataPanel.Location = new System.Drawing.Point(10, 33);
            _dataPanel.Name = "_dataPanel";
            _dataPanel.Size = new System.Drawing.Size(580, 407);
            _dataPanel.TabIndex = 1;
            // 
            // _chainSummary
            // 
            _chainSummary.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // _chainSummary.ClientArea
            // 
            _chainSummary.ClientArea.Controls.Add(this._maxNotMappedAmount);
            _chainSummary.ClientArea.Controls.Add(this._minNotMappedAmount);
            _chainSummary.ClientArea.Controls.Add(this._maxMappedAmount);
            _chainSummary.ClientArea.Controls.Add(this._minMappedAmount);
            _chainSummary.ClientArea.Controls.Add(_rub4);
            _chainSummary.ClientArea.Controls.Add(_rub3);
            _chainSummary.ClientArea.Controls.Add(_rub2);
            _chainSummary.ClientArea.Controls.Add(_rub1);
            _chainSummary.ClientArea.Controls.Add(_maxNotMappedAmountLabel);
            _chainSummary.ClientArea.Controls.Add(_minNotMappedAmountLabel);
            _chainSummary.ClientArea.Controls.Add(_MaxMappedAmountLabel);
            _chainSummary.ClientArea.Controls.Add(_minMappedAmountLabel);
            _chainSummary.Location = new System.Drawing.Point(19, 14);
            _chainSummary.Name = "_chainSummary";
            _chainSummary.Size = new System.Drawing.Size(526, 100);
            _chainSummary.TabIndex = 2;
            // 
            // _maxNotMappedAmount
            // 
            appearance7.TextHAlignAsString = "Right";
            this._maxNotMappedAmount.Appearance = appearance7;
            this._maxNotMappedAmount.Location = new System.Drawing.Point(272, 73);
            this._maxNotMappedAmount.Name = "_maxNotMappedAmount";
            this._maxNotMappedAmount.ReadOnly = true;
            this._maxNotMappedAmount.Size = new System.Drawing.Size(100, 21);
            this._maxNotMappedAmount.TabIndex = 11;
            this._maxNotMappedAmount.Text = "0";
            // 
            // _minNotMappedAmount
            // 
            appearance6.TextHAlignAsString = "Right";
            this._minNotMappedAmount.Appearance = appearance6;
            this._minNotMappedAmount.Location = new System.Drawing.Point(272, 50);
            this._minNotMappedAmount.Name = "_minNotMappedAmount";
            this._minNotMappedAmount.ReadOnly = true;
            this._minNotMappedAmount.Size = new System.Drawing.Size(100, 21);
            this._minNotMappedAmount.TabIndex = 10;
            this._minNotMappedAmount.Text = "0";
            // 
            // _maxMappedAmount
            // 
            appearance5.TextHAlignAsString = "Right";
            this._maxMappedAmount.Appearance = appearance5;
            this._maxMappedAmount.Location = new System.Drawing.Point(272, 27);
            this._maxMappedAmount.Name = "_maxMappedAmount";
            this._maxMappedAmount.ReadOnly = true;
            this._maxMappedAmount.Size = new System.Drawing.Size(100, 21);
            this._maxMappedAmount.TabIndex = 9;
            this._maxMappedAmount.Text = "0";
            // 
            // _minMappedAmount
            // 
            appearance4.TextHAlignAsString = "Right";
            this._minMappedAmount.Appearance = appearance4;
            this._minMappedAmount.Location = new System.Drawing.Point(272, 4);
            this._minMappedAmount.Name = "_minMappedAmount";
            this._minMappedAmount.ReadOnly = true;
            this._minMappedAmount.Size = new System.Drawing.Size(100, 21);
            this._minMappedAmount.TabIndex = 8;
            this._minMappedAmount.Text = "0";
            // 
            // _rub4
            // 
            _rub4.Location = new System.Drawing.Point(378, 77);
            _rub4.Name = "_rub4";
            _rub4.Size = new System.Drawing.Size(28, 23);
            _rub4.TabIndex = 7;
            _rub4.Text = "руб.";
            // 
            // _rub3
            // 
            _rub3.Location = new System.Drawing.Point(378, 54);
            _rub3.Name = "_rub3";
            _rub3.Size = new System.Drawing.Size(28, 23);
            _rub3.TabIndex = 6;
            _rub3.Text = "руб.";
            // 
            // _rub2
            // 
            _rub2.Location = new System.Drawing.Point(378, 31);
            _rub2.Name = "_rub2";
            _rub2.Size = new System.Drawing.Size(28, 23);
            _rub2.TabIndex = 5;
            _rub2.Text = "руб.";
            // 
            // _rub1
            // 
            _rub1.Location = new System.Drawing.Point(378, 8);
            _rub1.Name = "_rub1";
            _rub1.Size = new System.Drawing.Size(28, 23);
            _rub1.TabIndex = 4;
            _rub1.Text = "руб.";
            // 
            // _maxNotMappedAmountLabel
            // 
            _maxNotMappedAmountLabel.Location = new System.Drawing.Point(0, 77);
            _maxNotMappedAmountLabel.Name = "_maxNotMappedAmountLabel";
            _maxNotMappedAmountLabel.Size = new System.Drawing.Size(268, 23);
            _maxNotMappedAmountLabel.TabIndex = 3;
            _maxNotMappedAmountLabel.Text = "Максимальная сумма несопоставленного потока";
            // 
            // _minNotMappedAmountLabel
            // 
            _minNotMappedAmountLabel.Location = new System.Drawing.Point(0, 54);
            _minNotMappedAmountLabel.Name = "_minNotMappedAmountLabel";
            _minNotMappedAmountLabel.Size = new System.Drawing.Size(268, 23);
            _minNotMappedAmountLabel.TabIndex = 2;
            _minNotMappedAmountLabel.Text = "Минимальная сумма несопоставленного потока";
            // 
            // _MaxMappedAmountLabel
            // 
            _MaxMappedAmountLabel.Location = new System.Drawing.Point(0, 31);
            _MaxMappedAmountLabel.Name = "_MaxMappedAmountLabel";
            _MaxMappedAmountLabel.Size = new System.Drawing.Size(268, 23);
            _MaxMappedAmountLabel.TabIndex = 1;
            _MaxMappedAmountLabel.Text = "Максимальная сумма сопоставленного потока";
            // 
            // _minMappedAmountLabel
            // 
            _minMappedAmountLabel.Location = new System.Drawing.Point(0, 8);
            _minMappedAmountLabel.Name = "_minMappedAmountLabel";
            _minMappedAmountLabel.Size = new System.Drawing.Size(268, 23);
            _minMappedAmountLabel.TabIndex = 0;
            _minMappedAmountLabel.Text = "Минимальная сумма сопоставленного потока";
            // 
            // _chainContractorsBox
            // 
            _chainContractorsBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            _chainContractorsBox.Appearance = appearance1;
            _chainContractorsBox.ContentPadding.Bottom = 10;
            _chainContractorsBox.ContentPadding.Left = 10;
            _chainContractorsBox.ContentPadding.Right = 10;
            _chainContractorsBox.ContentPadding.Top = 10;
            _chainContractorsBox.Controls.Add(this._chainContractorsList);
            _chainContractorsBox.Location = new System.Drawing.Point(19, 120);
            _chainContractorsBox.Name = "_chainContractorsBox";
            _chainContractorsBox.Size = new System.Drawing.Size(526, 252);
            _chainContractorsBox.TabIndex = 1;
            _chainContractorsBox.Text = "Налогоплательщики";
            // 
            // _chainContractorsList
            // 
            this._chainContractorsList.Dock = System.Windows.Forms.DockStyle.Fill;
            this._chainContractorsList.GridContextMenuStrip = this._menu;
            this._chainContractorsList.Location = new System.Drawing.Point(13, 26);
            this._chainContractorsList.Margin = new System.Windows.Forms.Padding(0);
            this._chainContractorsList.Name = "_chainContractorsList";
            this._chainContractorsList.Size = new System.Drawing.Size(500, 213);
            this._chainContractorsList.TabIndex = 0;
            // 
            // _menu
            // 
            this._menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            _viewDetailsMenuItem});
            this._menu.Name = "_menu";
            this._menu.Size = new System.Drawing.Size(279, 26);
            // 
            // _viewDetailsMenuItem
            // 
            _viewDetailsMenuItem.Name = "_viewDetailsMenuItem";
            _viewDetailsMenuItem.Size = new System.Drawing.Size(278, 22);
            _viewDetailsMenuItem.Text = "Открыть карточку налогоплательщика";
            _viewDetailsMenuItem.Click += new System.EventHandler(this.ViewDetails);
            // 
            // _selectFile
            // 
            this._selectFile.DefaultExt = "xlsx";
            this._selectFile.FileName = "обзор_цепочки";
            this._selectFile.Filter = "Excel Workbook (*.xlsx)|*.xlsx";
            // 
            // ChainView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(_dataPanel);
            this.Controls.Add(_title);
            this.Name = "ChainView";
            this.Padding = new System.Windows.Forms.Padding(10);
            this.Size = new System.Drawing.Size(600, 450);
            this.Load += new System.EventHandler(this.OnLoad);
            _dataPanel.ClientArea.ResumeLayout(false);
            _dataPanel.ResumeLayout(false);
            _chainSummary.ClientArea.ResumeLayout(false);
            _chainSummary.ClientArea.PerformLayout();
            _chainSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._maxNotMappedAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._minNotMappedAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._maxMappedAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._minMappedAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(_chainContractorsBox)).EndInit();
            _chainContractorsBox.ResumeLayout(false);
            this._menu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Controls.Grid.V2.GridView _chainContractorsList;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor _maxNotMappedAmount;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor _minNotMappedAmount;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor _maxMappedAmount;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor _minMappedAmount;
        private System.Windows.Forms.ContextMenuStrip _menu;
        private System.Windows.Forms.SaveFileDialog _selectFile;
    }
}
