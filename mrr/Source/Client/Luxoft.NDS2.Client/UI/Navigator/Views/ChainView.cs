﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.UI.Base.Ribbon;

namespace Luxoft.NDS2.Client.UI.Navigator.Views
{
    public partial class ChainView : UserControl, IChainView
    {
        private readonly IChainPresenter _presenter;

        private readonly INotifier _notifier;
        private Workbook _workBook;

        public ChainView()
        {
            InitializeComponent();
        }

        public ChainView(IChainPresenter presenter, IRibbonAdapter ribbon, INotifier notifier)
            : this()
        {
            _presenter = presenter;
            _notifier = notifier;

            ribbon.ToggleVisibility(new[] { MenuCommands.ExportToExcel });
            InitGrid();

            _presenter.Model.OnError += _notifier.ShowError;
            _presenter.Model.OnNotification += _notifier.ShowNotification;
            _presenter.Model.ChainLoaded += SetViewData;
            _chainContractorsList.Grid.AfterRowActivate += AfterContractorActive;
        }

        public string Title
        {
            get
            {
                return "Навигатор - обзор цепочки";
            }
        }

        public void ExportToExcel()
        {
            if (_workBook == null)
            {
                _notifier.ShowError("Невозможно произвести выгрузку данных");
            }
            else
            {
                if (_selectFile.ShowDialog() == DialogResult.OK)
                {
                    _workBook.Save(_selectFile.FileName);
                }
            }
        }

        private void InitGrid()
        {
            var setup = new GridColumnSetup();
            var helper = new ColumnHelper<Models.ChainContractorModel>(_chainContractorsList);

            setup.Columns.Add(helper.CreateTextColumn(model => model.Link, 1, 60).Configure(col => { col.DisableSort = true; col.DisableFilter = true; }));
            setup.Columns.Add(helper.CreateSurColumn(model => model.SurCode, _presenter.Model.SurItems).Configure(col => { col.DisableSort = true; col.DisableFilter = true; }));
            setup.Columns.Add(helper.CreateTextColumn(model => model.Name, 1, 300).Configure(col => { col.DisableSort = true; col.DisableFilter = true; }));
            setup.Columns.Add(helper.CreateTextColumn(model => model.ChainNumber).Configure(col => { col.DisableSort = true; col.DisableFilter = true; }));
            setup.Columns.Add(helper.CreateTextColumn(model => model.MappedAmount, 1, 300).Configure(col => { col.DisableSort = true; col.DisableFilter = true; }));
            setup.Columns.Add(helper.CreateTextColumn(model => model.AmountPerBuyer, 1, 300).Configure(col => { col.DisableSort = true; col.DisableFilter = true; }));
            setup.Columns.Add(helper.CreateTextColumn(model => model.AmountPerSeller, 1, 300).Configure(col => { col.DisableSort = true; col.DisableFilter = true; }));

            _chainContractorsList.InitColumns(setup);
            _chainContractorsList.Grid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
        }

        private void SetViewData()
        {
            this.InvokeIfNeeded(
                () =>
                {
                    _minMappedAmount.Text = _presenter.Model.MinMappedAmount;
                    _maxMappedAmount.Text = _presenter.Model.MaxMappedAmount;
                    _minNotMappedAmount.Text = _presenter.Model.MinNotMappedAmount;
                    _minMappedAmount.Text = _presenter.Model.MaxNotMappedAmount;
                });
            _chainContractorsList.PushData(_presenter.Model.Contractors);
            _workBook = _chainContractorsList.ExportToWorkbook();
        }

        private void AfterContractorActive(object sender, EventArgs e)
        {
            var model = 
                _chainContractorsList.Grid.ActiveRow.ListObject as Models.ChainContractorModel;

            if (model != null)
            {
                model.Activate();
            }
        }

        private void ViewDetails(object sender, EventArgs e)
        {
            _presenter.OpenTaxPayerDetails();
        }

        private void OnLoad(object sender, EventArgs e)
        {
            _presenter.StartLoading();
        }
    }
}
