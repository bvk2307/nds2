﻿namespace Luxoft.NDS2.Client.UI.Navigator.Views
{
    partial class ChainContractorsListView
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._contractorsList = new Luxoft.NDS2.Client.UI.Controls.Grid.V2.GridView();
            this._menu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._detailsViewMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._selectFile = new System.Windows.Forms.SaveFileDialog();
            this._menu.SuspendLayout();
            this.SuspendLayout();
            // 
            // _contractorsList
            // 
            this._contractorsList.Dock = System.Windows.Forms.DockStyle.Fill;
            this._contractorsList.GridContextMenuStrip = this._menu;
            this._contractorsList.Location = new System.Drawing.Point(0, 0);
            this._contractorsList.Margin = new System.Windows.Forms.Padding(0);
            this._contractorsList.Name = "_contractorsList";
            this._contractorsList.Size = new System.Drawing.Size(600, 450);
            this._contractorsList.TabIndex = 0;
            // 
            // _menu
            // 
            this._menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._detailsViewMenuItem});
            this._menu.Name = "_menu";
            this._menu.Size = new System.Drawing.Size(279, 26);
            // 
            // _detailsViewMenuItem
            // 
            this._detailsViewMenuItem.Name = "_detailsViewMenuItem";
            this._detailsViewMenuItem.Size = new System.Drawing.Size(278, 22);
            this._detailsViewMenuItem.Text = "Открыть карточку налогоплательщика";
            this._detailsViewMenuItem.Click += new System.EventHandler(this.ViewDetails);
            // 
            // _selectFile
            // 
            this._selectFile.DefaultExt = "xlsx";
            this._selectFile.FileName = "контрагенты_в_цепочке";
            this._selectFile.Filter = "Excel Workbook (*.xlsx)|*.xlsx";
            // 
            // ChainContractorsListView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this._contractorsList);
            this.Name = "ChainContractorsListView";
            this.Size = new System.Drawing.Size(600, 450);
            this.Load += new System.EventHandler(this.OnLoad);
            this._menu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Controls.Grid.V2.GridView _contractorsList;
        private System.Windows.Forms.ContextMenuStrip _menu;
        private System.Windows.Forms.ToolStripMenuItem _detailsViewMenuItem;
        private System.Windows.Forms.SaveFileDialog _selectFile;
    }
}
