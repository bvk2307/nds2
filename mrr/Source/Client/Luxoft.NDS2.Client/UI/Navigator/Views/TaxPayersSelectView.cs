﻿using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.CustomSettings;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Navigator.Views
{
    public partial class TaxPayersSelectView : Form, ITaxPayersSelectView
    {
        # region Презентер (MVP)

        private readonly ITaxPayersSelectPresenter _presenter;

        # endregion

        # region Конструкторы

        public TaxPayersSelectView()
        {
            InitializeComponent();
        }

        public TaxPayersSelectView(ITaxPayersSelectPresenter presenter)
            : this()
        {
            _presenter = presenter;

            _query = new QueryConditions();
            _filterBuilder = new CustomFilterBuilder(_query.Filter);
            _sortBuilder = new CustomSortBuilder(_query.Sorting);

            InitGrid();
            BindModel();
        }

        # endregion

        # region Грид НП

        private readonly QueryConditions _query;

        private readonly CustomFilterBuilder _filterBuilder;

        private readonly CustomSortBuilder _sortBuilder;

        private readonly PagerStateViewModel _pager = new PagerStateViewModel();

        private void InitGrid()
        {
            var helper = new ColumnHelper<Models.TaxPayerModel>(_taxPayers);
            var setup = new GridColumnSetup();

            setup
                .Columns
                .Add(
                    helper
                        .CreateCheckColumn(model => model.Selected)
                        .Configure(
                            column =>
                            {
                                column.HeaderToolTip = "Флаг выбора записи";
                                column.HideHeaderCheckbox = true;
                            }));
            setup.Columns.Add(helper.CreateTextColumn(model => model.Inn, 1, 80));
            setup.Columns.Add(helper.CreateTextColumn(model => model.Kpp, 1, 80));
            setup.Columns.Add(helper.CreateTextColumn(model => model.Name, 1, 200));
            setup.Columns.Add(helper.CreateTextColumn(model => model.FederalDistrict, 1, 200));
            setup.Columns.Add(helper.CreateTextColumn(model => model.Region, 1 ,200));
            setup.Columns.Add(helper.CreateTextColumn(model => model.Inspection, 1, 200));
            setup.Columns.Add(helper.CreateSurColumn(model => model.SurCode, _presenter.Model.TaxPayers.SurItems));
            setup.Columns.Add(helper.CreateTextColumn(model => model.SalesAmount));
            setup.Columns.Add(helper.CreateTextColumn(model => model.PurchaseAmount));
            setup.Columns.Add(helper.CreateTextColumn(model => model.NdsCalculated));
            setup.Columns.Add(helper.CreateTextColumn(model => model.NdsDeduction, 1, 160));

            _taxPayers.WithPager(new DataGridPageNavigator(_pager)).InitColumns(setup);
            _taxPayers.Grid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;

            _sortBuilder.ApplySorting(
                new List<ColumnSort>
                {
                    new ColumnSort
                    {
                        ColumnKey = "inn",
                        Order = ColumnSort.SortOrder.Asc
                    }
                },
                _taxPayers.Grid.DisplayLayout.Bands[0]);


            _pager.DataListChanging+= PageChanged;
            _taxPayers.BeforeSortingChanged += SortingChanged;
            _taxPayers.BeforeFilterChanged += FilterChanged;
            _taxPayers.Grid.MouseClick += TaxPayerCheckedChanged;
        }

        private void FilterChanged(Infragistics.Win.UltraWinGrid.ColumnFilter data)
        {
            _taxPayers
                .Columns
                .Columns
                .Find(data.Column.Key)
                .OptionsBuilder()
                .OnFilterChanged(data);
            _filterBuilder.ApplyFilter(data);
            _presenter.UpdateData(_query);
        }

        private void SortingChanged(Infragistics.Win.UltraWinGrid.SortedColumnsCollection data)
        {
            _sortBuilder.SortChanged(data);
            _presenter.UpdateData(_query, false);
        }

        private void PageChanged()
        {
            _query.PageIndex = _pager.PageIndex;
            _query.PageSize = _pager.PageSize;
            _presenter.UpdateData(_query, false);
        }

        # endregion

        # region Привязка к событиям модели

        private void BindModel()
        {
            _presenter.Model.TaxPayers.TaxPayersLoaded += SetTaxPayersList;
        }

        # endregion

        # region Отображение данных модели

        private void SetData()
        {
            _taxPayersListRequirement.Text = _presenter.Model.TaxPayers.State.Requirement;
            _period.Text = _presenter.Model.Period.ToString();
        }

        private void SetTaxPayersList()
        {
            _pager.TotalMatches = _presenter.Model.TaxPayers.TotalQuantity;
            _taxPayers.PushData(_presenter.Model.TaxPayers.All);
        }

        # endregion

        # region Обработка событий

        private void OnLoad(object sender, EventArgs e)
        {
            SetData();
            _presenter.UpdateData(_query); //Можно оптимизировать если обновлять данные только если изменился период
        }

        private void OnAccept(object sender, EventArgs e)
        {
            _presenter.Model.TaxPayers.SubmitSelection();
            Close();
        }

        private void OnCancel(object sender, EventArgs e)
        {
            Close();
        }

        private void TaxPayerCheckedChanged(object sender, MouseEventArgs e)
        {
            var element = _taxPayers.Grid.DisplayLayout.UIElement.ElementFromPoint(e.Location);
            var row = element.GetContext(typeof(UltraGridRow)) as UltraGridRow;

            if (_taxPayers.Grid.ActiveRow == null || row == null)
            {
                return;
            }

            var taxPayer = _taxPayers.Grid.ActiveRow.ListObject as Models.TaxPayerModel;

            if (taxPayer != null)
            {
                taxPayer.Selected = !taxPayer.Selected;
                _taxPayers.Grid.Refresh();
            }
        }

        # endregion
    }
}
