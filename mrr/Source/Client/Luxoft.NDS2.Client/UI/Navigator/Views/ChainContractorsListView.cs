﻿using Infragistics.Excel;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.UI.Base.Ribbon;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;
using System;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Navigator.Views
{
    public partial class ChainContractorsListView : UserControl, IChainContractorsListView
    {
        private const string ChainMenuTitleFormat = "Обзор цепочки {0}";

        private readonly IChainContractorsListPresenter _presenter;

        private readonly INotifier _notifier;
        private Workbook _workBook;

        public ChainContractorsListView()
        {
            InitializeComponent();
        }

        public ChainContractorsListView(
            IChainContractorsListPresenter presenter,
            IRibbonAdapter ribbon,
            INotifier notifier)
            : this()
        {
            _presenter = presenter;
            _notifier = notifier;

            ribbon.ToggleVisibility(new[] { MenuCommands.ExportToExcel });
            InitGrid();

            _presenter.Model.OnError += _notifier.ShowError;
            _presenter.Model.OnNotification += _notifier.ShowNotification;
            _presenter.Model.ContractorsLoaded +=
                () =>
                {
                    _contractorsList.PushData(_presenter.Model.Contractors);
                    _workBook = _contractorsList.ExportToWorkbook();
                };
            _contractorsList.Grid.AfterRowActivate += AfterContractorActivated;
        }

        public string Title
        {
            get
            {
                return "Навигатор - обзор НП в цепочках";
            }
        }

        public void ExportToExcel()
        {
            if (_workBook == null)
            {
                _notifier.ShowError("Невозможно произвести выгрузку данных");
            }
            else
            {
                if (_selectFile.ShowDialog() == DialogResult.OK)
                {
                    _workBook.Save(_selectFile.FileName);
                }
            }
        }

        private void InitGrid()
        {
            var setup = new GridColumnSetup();
            var helper = new ColumnHelper<Models.ContractorModel>(_contractorsList);

            setup.Columns.Add(
                helper
                    .CreateTextColumn(model => model.Name, 1, 300)
                    .Configure(col => { col.DisableSort = true; col.DisableFilter = true; }));
            setup.Columns.Add(
                helper
                    .CreateTextColumn(model => model.ChainsQuantity, 1, 200)
                    .Configure(col => { col.DisableSort = true; col.DisableFilter = true; }));
            setup.Columns.Add(
               helper
                    .CreateTextColumn(model => model.ChainsList)
                    .Configure(col => { col.DisableSort = true; col.DisableFilter = true; }));
            setup.Columns.Add(
                helper
                    .CreateTextColumn(model => model.NdsDeduction, 1, 300)
                    .Configure(col => { col.DisableSort = true; col.DisableFilter = true; }));
            setup.Columns.Add(
                helper
                    .CreateTextColumn(model => model.NdsCalculated, 1 , 240)
                    .Configure(col => { col.DisableSort = true; col.DisableFilter = true; }));
            setup.Columns.Add(
                helper
                    .CreateTextColumn(model => model.NdsSales, 1, 240)
                    .Configure(col => { col.DisableSort = true; col.DisableFilter = true; }));
            setup.Columns.Add(
                helper
                    .CreateTextColumn(model => model.NsdPurchase, 1, 240)
                    .Configure(col => { col.DisableSort = true; col.DisableFilter = true; }));

            _contractorsList.InitColumns(setup);
            _contractorsList.Grid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
        }

        private void OnLoad(object sender, EventArgs e)
        {
            _presenter.StartLoading();
        }

        private void ViewDetails(object sender, EventArgs e)
        {
            _presenter.ViewDetails();
        }

        private void ViewChain(object sender, EventArgs e)
        {
            var menuItem = sender as ToolStripMenuItem;
            if (menuItem == null)
            {
                // TODO: notify like "chain with this Id not found"
                //_notifier.ShowError("Не удалось найти указанную цепочку");
                return;
            }

            _presenter.ViewChain((long)menuItem.Tag);
        }

        private void AfterContractorActivated(object sender, EventArgs e)
        {
            _menu.Items.Clear();           

            if (_contractorsList.Grid.ActiveRow == null)
            {
                return;
            }

            var selectedModel = 
                _contractorsList.Grid.ActiveRow.ListObject as Models.ContractorModel;

            if (selectedModel != null)
            {
                selectedModel.Activate();

                _menu.Items.Add(_detailsViewMenuItem);

                foreach (var chain in selectedModel.GetChains())
                {
                    var menuItem = 
                        new ToolStripMenuItem(string.Format(ChainMenuTitleFormat, chain.Title));
                    menuItem.Tag = chain.Id;
                    menuItem.Click += ViewChain;

                    _menu.Items.Add(menuItem);
                }
            }
        }
    }
}
