﻿namespace Luxoft.NDS2.Client.UI.Navigator.Views
{
    partial class ReportSetupView
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label _description;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportSetupView));
            Infragistics.Win.Misc.UltraPanel _periodPanel;
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel _periodToLabel;
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel _periodFrom;
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraPanel _paramsPanel;
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel _fromBuyerLabel;
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel _fromSellerLabel;
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel _amountHeaderLabel;
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel _maxContractorsLabel;
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel _minMappedAmountLabel;
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel _levelLabel;
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel _depthLabel;
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraPanel _taxPayerListActionsPanel;
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            this._periodToYearSelect = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this._periodToMonthSelect = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this._periodValidationError = new Infragistics.Win.Misc.UltraLabel();
            this._periodFromYearSelect = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this._periodFromMonthSelect = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this._inputNeededError = new Infragistics.Win.Misc.UltraLabel();
            this._maxContractorsError = new Infragistics.Win.Misc.UltraLabel();
            this._buyerAmountEditor = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this._sellerAmountEditor = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this._maxContractorsEditor = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this._minMappedAmountEditor = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this._submitButton = new Infragistics.Win.Misc.UltraButton();
            this._depthSelect = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this._excludeTaxPayesrButton = new Infragistics.Win.Misc.UltraButton();
            this._modifyTaxPayersButton = new Infragistics.Win.Misc.UltraButton();
            this._taxPayersMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._viewDetails = new System.Windows.Forms.ToolStripMenuItem();
            this._taxPayersRequirementLabel = new Infragistics.Win.Misc.UltraLabel();
            this._taxPayersPanel = new Infragistics.Win.Misc.UltraGroupBox();
            this._taxPayers = new Luxoft.NDS2.Client.UI.Controls.Grid.V2.GridView();
            this._taxPayersError = new System.Windows.Forms.Label();
            _description = new System.Windows.Forms.Label();
            _periodPanel = new Infragistics.Win.Misc.UltraPanel();
            _periodToLabel = new Infragistics.Win.Misc.UltraLabel();
            _periodFrom = new Infragistics.Win.Misc.UltraLabel();
            _paramsPanel = new Infragistics.Win.Misc.UltraPanel();
            _fromBuyerLabel = new Infragistics.Win.Misc.UltraLabel();
            _fromSellerLabel = new Infragistics.Win.Misc.UltraLabel();
            _amountHeaderLabel = new Infragistics.Win.Misc.UltraLabel();
            _maxContractorsLabel = new Infragistics.Win.Misc.UltraLabel();
            _minMappedAmountLabel = new Infragistics.Win.Misc.UltraLabel();
            _levelLabel = new Infragistics.Win.Misc.UltraLabel();
            _depthLabel = new Infragistics.Win.Misc.UltraLabel();
            _taxPayerListActionsPanel = new Infragistics.Win.Misc.UltraPanel();
            _periodPanel.ClientArea.SuspendLayout();
            _periodPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._periodToYearSelect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._periodToMonthSelect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._periodFromYearSelect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._periodFromMonthSelect)).BeginInit();
            _paramsPanel.ClientArea.SuspendLayout();
            _paramsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._depthSelect)).BeginInit();
            _taxPayerListActionsPanel.ClientArea.SuspendLayout();
            _taxPayerListActionsPanel.SuspendLayout();
            this._taxPayersMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._taxPayersPanel)).BeginInit();
            this._taxPayersPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // _description
            // 
            _description.Dock = System.Windows.Forms.DockStyle.Top;
            _description.Location = new System.Drawing.Point(0, 0);
            _description.Name = "_description";
            _description.Size = new System.Drawing.Size(800, 32);
            _description.TabIndex = 0;
            _description.Text = resources.GetString("_description.Text");
            // 
            // _periodPanel
            // 
            // 
            // _periodPanel.ClientArea
            // 
            _periodPanel.ClientArea.Controls.Add(this._periodToYearSelect);
            _periodPanel.ClientArea.Controls.Add(this._periodToMonthSelect);
            _periodPanel.ClientArea.Controls.Add(this._periodValidationError);
            _periodPanel.ClientArea.Controls.Add(_periodToLabel);
            _periodPanel.ClientArea.Controls.Add(this._periodFromYearSelect);
            _periodPanel.ClientArea.Controls.Add(this._periodFromMonthSelect);
            _periodPanel.ClientArea.Controls.Add(_periodFrom);
            _periodPanel.Dock = System.Windows.Forms.DockStyle.Top;
            _periodPanel.Location = new System.Drawing.Point(0, 32);
            _periodPanel.MaximumSize = new System.Drawing.Size(0, 80);
            _periodPanel.MinimumSize = new System.Drawing.Size(200, 30);
            _periodPanel.Name = "_periodPanel";
            _periodPanel.Size = new System.Drawing.Size(800, 59);
            _periodPanel.TabIndex = 8;
            // 
            // _periodToYearSelect
            // 
            this._periodToYearSelect.DisplayMember = "";
            this._periodToYearSelect.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this._periodToYearSelect.Location = new System.Drawing.Point(504, 5);
            this._periodToYearSelect.Name = "_periodToYearSelect";
            this._periodToYearSelect.Size = new System.Drawing.Size(75, 21);
            this._periodToYearSelect.TabIndex = 5;
            this._periodToYearSelect.ValueMember = "";
            this._periodToYearSelect.ValueChanged += new System.EventHandler(this.OnPeriodToYearChanged);
            // 
            // _periodToMonthSelect
            // 
            this._periodToMonthSelect.DisplayMember = "";
            this._periodToMonthSelect.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this._periodToMonthSelect.Location = new System.Drawing.Point(387, 5);
            this._periodToMonthSelect.Name = "_periodToMonthSelect";
            this._periodToMonthSelect.Size = new System.Drawing.Size(100, 21);
            this._periodToMonthSelect.TabIndex = 4;
            this._periodToMonthSelect.ValueMember = "";
            this._periodToMonthSelect.ValueChanged += new System.EventHandler(this.OnPeriodToMonthChanged);
            // 
            // _periodValidationError
            // 
            this._periodValidationError.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.FontData.BoldAsString = "True";
            appearance1.ForeColor = System.Drawing.Color.Red;
            appearance1.TextVAlignAsString = "Bottom";
            this._periodValidationError.Appearance = appearance1;
            this._periodValidationError.Location = new System.Drawing.Point(4, 32);
            this._periodValidationError.Name = "_periodValidationError";
            this._periodValidationError.Padding = new System.Drawing.Size(0, 5);
            this._periodValidationError.Size = new System.Drawing.Size(4993, 24);
            this._periodValidationError.TabIndex = 7;
            // 
            // _periodToLabel
            // 
            appearance2.TextVAlignAsString = "Bottom";
            _periodToLabel.Appearance = appearance2;
            _periodToLabel.Location = new System.Drawing.Point(357, 2);
            _periodToLabel.Name = "_periodToLabel";
            _periodToLabel.Padding = new System.Drawing.Size(0, 5);
            _periodToLabel.Size = new System.Drawing.Size(19, 26);
            _periodToLabel.TabIndex = 3;
            _periodToLabel.Text = "по";
            // 
            // _periodFromYearSelect
            // 
            this._periodFromYearSelect.DisplayMember = "";
            this._periodFromYearSelect.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this._periodFromYearSelect.Location = new System.Drawing.Point(272, 5);
            this._periodFromYearSelect.Name = "_periodFromYearSelect";
            this._periodFromYearSelect.Size = new System.Drawing.Size(75, 21);
            this._periodFromYearSelect.TabIndex = 2;
            this._periodFromYearSelect.ValueMember = "";
            this._periodFromYearSelect.ValueChanged += new System.EventHandler(this.OnPeriodFromYearChanged);
            // 
            // _periodFromMonthSelect
            // 
            this._periodFromMonthSelect.DisplayMember = "";
            this._periodFromMonthSelect.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this._periodFromMonthSelect.Location = new System.Drawing.Point(155, 5);
            this._periodFromMonthSelect.Name = "_periodFromMonthSelect";
            this._periodFromMonthSelect.Size = new System.Drawing.Size(100, 21);
            this._periodFromMonthSelect.TabIndex = 1;
            this._periodFromMonthSelect.ValueMember = "";
            this._periodFromMonthSelect.ValueChanged += new System.EventHandler(this.OnPeriodFromMonthChanged);
            // 
            // _periodFrom
            // 
            appearance5.TextVAlignAsString = "Bottom";
            _periodFrom.Appearance = appearance5;
            _periodFrom.Location = new System.Drawing.Point(4, 4);
            _periodFrom.Name = "_periodFrom";
            _periodFrom.Padding = new System.Drawing.Size(0, 5);
            _periodFrom.Size = new System.Drawing.Size(150, 23);
            _periodFrom.TabIndex = 0;
            _periodFrom.Text = "Период формирования - с";
            // 
            // _paramsPanel
            // 
            // 
            // _paramsPanel.ClientArea
            // 
            _paramsPanel.ClientArea.Controls.Add(this._inputNeededError);
            _paramsPanel.ClientArea.Controls.Add(this._maxContractorsError);
            _paramsPanel.ClientArea.Controls.Add(this._buyerAmountEditor);
            _paramsPanel.ClientArea.Controls.Add(this._sellerAmountEditor);
            _paramsPanel.ClientArea.Controls.Add(this._maxContractorsEditor);
            _paramsPanel.ClientArea.Controls.Add(this._minMappedAmountEditor);
            _paramsPanel.ClientArea.Controls.Add(this._submitButton);
            _paramsPanel.ClientArea.Controls.Add(_fromBuyerLabel);
            _paramsPanel.ClientArea.Controls.Add(_fromSellerLabel);
            _paramsPanel.ClientArea.Controls.Add(_amountHeaderLabel);
            _paramsPanel.ClientArea.Controls.Add(_maxContractorsLabel);
            _paramsPanel.ClientArea.Controls.Add(_minMappedAmountLabel);
            _paramsPanel.ClientArea.Controls.Add(_levelLabel);
            _paramsPanel.ClientArea.Controls.Add(this._depthSelect);
            _paramsPanel.ClientArea.Controls.Add(_depthLabel);
            _paramsPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            _paramsPanel.Location = new System.Drawing.Point(0, 324);
            _paramsPanel.Name = "_paramsPanel";
            _paramsPanel.Size = new System.Drawing.Size(800, 176);
            _paramsPanel.TabIndex = 10;
            // 
            // _inputNeededError
            // 
            appearance8.FontData.BoldAsString = "True";
            appearance8.ForeColor = System.Drawing.Color.Red;
            appearance8.TextVAlignAsString = "Bottom";
            this._inputNeededError.Appearance = appearance8;
            this._inputNeededError.Location = new System.Drawing.Point(342, 35);
            this._inputNeededError.Name = "_inputNeededError";
            this._inputNeededError.Padding = new System.Drawing.Size(0, 5);
            this._inputNeededError.Size = new System.Drawing.Size(455, 23);
            this._inputNeededError.TabIndex = 19;
            this._inputNeededError.Text = "Необходимо указать значение";
            // 
            // _maxContractorsError
            // 
            this._maxContractorsError.Appearance = appearance8;
            this._maxContractorsError.Location = new System.Drawing.Point(342, 35);
            this._maxContractorsError.Name = "_maxContractorsError";
            this._maxContractorsError.Padding = new System.Drawing.Size(0, 5);
            this._maxContractorsError.Size = new System.Drawing.Size(455, 23);
            this._maxContractorsError.TabIndex = 18;
            this._maxContractorsError.Text = "Минимальная сумма сопоставленного потока";
            // 
            // _buyerAmountEditor
            // 
            this._buyerAmountEditor.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Integer;
            this._buyerAmountEditor.Location = new System.Drawing.Point(264, 148);
            this._buyerAmountEditor.MinValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this._buyerAmountEditor.Name = "_buyerAmountEditor";
            this._buyerAmountEditor.PromptChar = ' ';
            this._buyerAmountEditor.Size = new System.Drawing.Size(72, 20);
            this._buyerAmountEditor.TabIndex = 17;
            this._buyerAmountEditor.Text = "9999999999";
            this._buyerAmountEditor.ValueChanged += new System.EventHandler(this.MinBuyerMappedAmountChanged);
            // 
            // _sellerAmountEditor
            // 
            this._sellerAmountEditor.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Integer;
            this._sellerAmountEditor.Location = new System.Drawing.Point(264, 120);
            this._sellerAmountEditor.MinValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this._sellerAmountEditor.Name = "_sellerAmountEditor";
            this._sellerAmountEditor.PromptChar = ' ';
            this._sellerAmountEditor.Size = new System.Drawing.Size(72, 20);
            this._sellerAmountEditor.TabIndex = 16;
            this._sellerAmountEditor.Text = "9999999999";
            this._sellerAmountEditor.ValueChanged += new System.EventHandler(this.MinSellerAmountChanged);
            // 
            // _maxContractorsEditor
            // 
            this._maxContractorsEditor.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Integer;
            this._maxContractorsEditor.Location = new System.Drawing.Point(264, 64);
            this._maxContractorsEditor.MinValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this._maxContractorsEditor.Name = "_maxContractorsEditor";
            this._maxContractorsEditor.PromptChar = ' ';
            this._maxContractorsEditor.Size = new System.Drawing.Size(72, 20);
            this._maxContractorsEditor.TabIndex = 15;
            this._maxContractorsEditor.Text = "9999999999";
            this._maxContractorsEditor.ValueChanged += new System.EventHandler(this.MaxContractorsQuantityChanged);
            // 
            // _minMappedAmountEditor
            // 
            this._minMappedAmountEditor.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Integer;
            this._minMappedAmountEditor.Location = new System.Drawing.Point(264, 36);
            this._minMappedAmountEditor.MinValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this._minMappedAmountEditor.Name = "_minMappedAmountEditor";
            this._minMappedAmountEditor.PromptChar = ' ';
            this._minMappedAmountEditor.Size = new System.Drawing.Size(72, 20);
            this._minMappedAmountEditor.TabIndex = 14;
            this._minMappedAmountEditor.Text = "9999999999";
            this._minMappedAmountEditor.ValueChanged += new System.EventHandler(this.MinMappedAmountChanged);
            // 
            // _submitButton
            // 
            this._submitButton.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this._submitButton.Location = new System.Drawing.Point(697, 145);
            this._submitButton.Name = "_submitButton";
            this._submitButton.Size = new System.Drawing.Size(100, 23);
            this._submitButton.TabIndex = 13;
            this._submitButton.Text = "Сформировать";
            this._submitButton.Click += new System.EventHandler(this.OnSubmit);
            // 
            // _fromBuyerLabel
            // 
            appearance7.TextVAlignAsString = "Bottom";
            _fromBuyerLabel.Appearance = appearance7;
            _fromBuyerLabel.Location = new System.Drawing.Point(43, 147);
            _fromBuyerLabel.Name = "_fromBuyerLabel";
            _fromBuyerLabel.Padding = new System.Drawing.Size(0, 5);
            _fromBuyerLabel.Size = new System.Drawing.Size(127, 23);
            _fromBuyerLabel.TabIndex = 11;
            _fromBuyerLabel.Text = "по данным покупателя";
            // 
            // _fromSellerLabel
            // 
            appearance6.TextVAlignAsString = "Bottom";
            _fromSellerLabel.Appearance = appearance6;
            _fromSellerLabel.Location = new System.Drawing.Point(43, 119);
            _fromSellerLabel.Name = "_fromSellerLabel";
            _fromSellerLabel.Padding = new System.Drawing.Size(0, 5);
            _fromSellerLabel.Size = new System.Drawing.Size(204, 23);
            _fromSellerLabel.TabIndex = 9;
            _fromSellerLabel.Text = "по данным продавца";
            // 
            // _amountHeaderLabel
            // 
            appearance3.TextVAlignAsString = "Bottom";
            _amountHeaderLabel.Appearance = appearance3;
            _amountHeaderLabel.Location = new System.Drawing.Point(4, 91);
            _amountHeaderLabel.Name = "_amountHeaderLabel";
            _amountHeaderLabel.Padding = new System.Drawing.Size(0, 5);
            _amountHeaderLabel.Size = new System.Drawing.Size(319, 23);
            _amountHeaderLabel.TabIndex = 8;
            _amountHeaderLabel.Text = "Минимальная сумма операций между парой контрагентов";
            // 
            // _maxContractorsLabel
            // 
            appearance12.TextVAlignAsString = "Bottom";
            _maxContractorsLabel.Appearance = appearance12;
            _maxContractorsLabel.Location = new System.Drawing.Point(4, 63);
            _maxContractorsLabel.Name = "_maxContractorsLabel";
            _maxContractorsLabel.Padding = new System.Drawing.Size(0, 5);
            _maxContractorsLabel.Size = new System.Drawing.Size(252, 23);
            _maxContractorsLabel.TabIndex = 6;
            _maxContractorsLabel.Text = "Максимальное число контрагентов";
            // 
            // _minMappedAmountLabel
            // 
            appearance13.TextVAlignAsString = "Bottom";
            _minMappedAmountLabel.Appearance = appearance13;
            _minMappedAmountLabel.Location = new System.Drawing.Point(4, 35);
            _minMappedAmountLabel.Name = "_minMappedAmountLabel";
            _minMappedAmountLabel.Padding = new System.Drawing.Size(0, 5);
            _minMappedAmountLabel.Size = new System.Drawing.Size(252, 23);
            _minMappedAmountLabel.TabIndex = 4;
            _minMappedAmountLabel.Text = "Минимальная сумма сопоставленного потока";
            // 
            // _levelLabel
            // 
            appearance4.TextVAlignAsString = "Bottom";
            _levelLabel.Appearance = appearance4;
            _levelLabel.Location = new System.Drawing.Point(140, 7);
            _levelLabel.Name = "_levelLabel";
            _levelLabel.Padding = new System.Drawing.Size(0, 5);
            _levelLabel.Size = new System.Drawing.Size(54, 23);
            _levelLabel.TabIndex = 2;
            _levelLabel.Text = "уровней";
            // 
            // _depthSelect
            // 
            this._depthSelect.Location = new System.Drawing.Point(99, 9);
            this._depthSelect.Name = "_depthSelect";
            this._depthSelect.Size = new System.Drawing.Size(36, 21);
            this._depthSelect.TabIndex = 1;
            this._depthSelect.Text = "9";
            // 
            // _depthLabel
            // 
            appearance10.TextVAlignAsString = "Bottom";
            _depthLabel.Appearance = appearance10;
            _depthLabel.Location = new System.Drawing.Point(4, 7);
            _depthLabel.Name = "_depthLabel";
            _depthLabel.Padding = new System.Drawing.Size(0, 5);
            _depthLabel.Size = new System.Drawing.Size(88, 23);
            _depthLabel.TabIndex = 0;
            _depthLabel.Text = "Глубина поиска";
            // 
            // _taxPayerListActionsPanel
            // 
            // 
            // _taxPayerListActionsPanel.ClientArea
            // 
            _taxPayerListActionsPanel.ClientArea.Controls.Add(this._excludeTaxPayesrButton);
            _taxPayerListActionsPanel.ClientArea.Controls.Add(this._modifyTaxPayersButton);
            _taxPayerListActionsPanel.Dock = System.Windows.Forms.DockStyle.Right;
            _taxPayerListActionsPanel.Location = new System.Drawing.Point(697, 16);
            _taxPayerListActionsPanel.MaximumSize = new System.Drawing.Size(100, 0);
            _taxPayerListActionsPanel.MinimumSize = new System.Drawing.Size(100, 0);
            _taxPayerListActionsPanel.Name = "_taxPayerListActionsPanel";
            _taxPayerListActionsPanel.Size = new System.Drawing.Size(100, 191);
            _taxPayerListActionsPanel.TabIndex = 2;
            // 
            // _excludeTaxPayesrButton
            // 
            this._excludeTaxPayesrButton.Dock = System.Windows.Forms.DockStyle.Top;
            this._excludeTaxPayesrButton.Location = new System.Drawing.Point(0, 23);
            this._excludeTaxPayesrButton.Name = "_excludeTaxPayesrButton";
            this._excludeTaxPayesrButton.Size = new System.Drawing.Size(100, 23);
            this._excludeTaxPayesrButton.TabIndex = 1;
            this._excludeTaxPayesrButton.Text = "Удалить";
            this._excludeTaxPayesrButton.Click += new System.EventHandler(this.ExcludeTaxPayers);
            // 
            // _modifyTaxPayersButton
            // 
            this._modifyTaxPayersButton.Dock = System.Windows.Forms.DockStyle.Top;
            this._modifyTaxPayersButton.Location = new System.Drawing.Point(0, 0);
            this._modifyTaxPayersButton.Name = "_modifyTaxPayersButton";
            this._modifyTaxPayersButton.Size = new System.Drawing.Size(100, 23);
            this._modifyTaxPayersButton.TabIndex = 0;
            this._modifyTaxPayersButton.Text = "Добавить";
            this._modifyTaxPayersButton.Click += new System.EventHandler(this.BeforeTaxPayersModifying);
            // 
            // _taxPayersMenu
            // 
            this._taxPayersMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._viewDetails});
            this._taxPayersMenu.Name = "_taxPayersMenu";
            this._taxPayersMenu.Size = new System.Drawing.Size(291, 26);
            // 
            // _viewDetails
            // 
            this._viewDetails.Name = "_viewDetails";
            this._viewDetails.Size = new System.Drawing.Size(290, 22);
            this._viewDetails.Text = "Открыть карточку налогоплательщика";
            this._viewDetails.Click += new System.EventHandler(this.ViewTaxPayerDetails);
            // 
            // _taxPayersRequirementLabel
            // 
            this._taxPayersRequirementLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this._taxPayersRequirementLabel.Location = new System.Drawing.Point(0, 91);
            this._taxPayersRequirementLabel.Name = "_taxPayersRequirementLabel";
            this._taxPayersRequirementLabel.Size = new System.Drawing.Size(800, 23);
            this._taxPayersRequirementLabel.TabIndex = 9;
            // 
            // _taxPayersPanel
            // 
            appearance11.BackColor = System.Drawing.Color.Transparent;
            appearance11.BackColor2 = System.Drawing.Color.Transparent;
            appearance11.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            this._taxPayersPanel.Appearance = appearance11;
            this._taxPayersPanel.Controls.Add(this._taxPayers);
            this._taxPayersPanel.Controls.Add(this._taxPayersError);
            this._taxPayersPanel.Controls.Add(_taxPayerListActionsPanel);
            this._taxPayersPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._taxPayersPanel.Location = new System.Drawing.Point(0, 114);
            this._taxPayersPanel.Name = "_taxPayersPanel";
            this._taxPayersPanel.Size = new System.Drawing.Size(800, 210);
            this._taxPayersPanel.TabIndex = 11;
            this._taxPayersPanel.Text = "Налогоплательщики";
            // 
            // _taxPayers
            // 
            this._taxPayers.Dock = System.Windows.Forms.DockStyle.Fill;
            this._taxPayers.GridContextMenuStrip = this._taxPayersMenu;
            this._taxPayers.Location = new System.Drawing.Point(3, 16);
            this._taxPayers.Margin = new System.Windows.Forms.Padding(0);
            this._taxPayers.Name = "_taxPayers";
            this._taxPayers.Size = new System.Drawing.Size(694, 161);
            this._taxPayers.TabIndex = 5;
            // 
            // _taxPayersError
            // 
            this._taxPayersError.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._taxPayersError.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._taxPayersError.ForeColor = System.Drawing.Color.Red;
            this._taxPayersError.Location = new System.Drawing.Point(3, 177);
            this._taxPayersError.Name = "_taxPayersError";
            this._taxPayersError.Size = new System.Drawing.Size(694, 30);
            this._taxPayersError.TabIndex = 4;
            // 
            // ReportSetupView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this._taxPayersPanel);
            this.Controls.Add(_paramsPanel);
            this.Controls.Add(this._taxPayersRequirementLabel);
            this.Controls.Add(_periodPanel);
            this.Controls.Add(_description);
            this.MinimumSize = new System.Drawing.Size(600, 500);
            this.Name = "ReportSetupView";
            this.Size = new System.Drawing.Size(800, 500);
            _periodPanel.ClientArea.ResumeLayout(false);
            _periodPanel.ClientArea.PerformLayout();
            _periodPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._periodToYearSelect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._periodToMonthSelect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._periodFromYearSelect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._periodFromMonthSelect)).EndInit();
            _paramsPanel.ClientArea.ResumeLayout(false);
            _paramsPanel.ClientArea.PerformLayout();
            _paramsPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._depthSelect)).EndInit();
            _taxPayerListActionsPanel.ClientArea.ResumeLayout(false);
            _taxPayerListActionsPanel.ResumeLayout(false);
            this._taxPayersMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._taxPayersPanel)).EndInit();
            this._taxPayersPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip _taxPayersMenu;
        private System.Windows.Forms.ToolStripMenuItem _viewDetails;
        private Infragistics.Win.Misc.UltraLabel _periodValidationError;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor _periodToYearSelect;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor _periodToMonthSelect;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor _periodFromYearSelect;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor _periodFromMonthSelect;
        private Infragistics.Win.Misc.UltraLabel _taxPayersRequirementLabel;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit _buyerAmountEditor;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit _sellerAmountEditor;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit _maxContractorsEditor;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit _minMappedAmountEditor;
        private Infragistics.Win.Misc.UltraButton _submitButton;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor _depthSelect;
        private Infragistics.Win.Misc.UltraGroupBox _taxPayersPanel;
        private Infragistics.Win.Misc.UltraButton _excludeTaxPayesrButton;
        private Infragistics.Win.Misc.UltraButton _modifyTaxPayersButton;
        private Controls.Grid.V2.GridView _taxPayers;
        private System.Windows.Forms.Label _taxPayersError;
        private Infragistics.Win.Misc.UltraLabel _maxContractorsError;
        private Infragistics.Win.Misc.UltraLabel _inputNeededError;

    }
}
