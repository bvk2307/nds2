﻿using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Navigator.Views
{
    public static class Helper
    {
        public static ValueListItem[] ToListItems<TModel>(this IEnumerable<TModel> models)
        {
            return models
                .Select(model => new ValueListItem(model))
                .ToArray();
        }

        public static T GetValue<T>(this UltraComboEditor selectList)
        {
            return (T)selectList.SelectedItem.DataValue;
        }

        public static void SelectPeriod<TModel>(this UltraComboEditor selectList, TModel model)
            where TModel : Models.IPeriod
        {
            foreach (var item in selectList.Items)
            {
                if (model.Compare(item.DataValue) == Models.PeriodCompareResult.Equal)
                {
                    selectList.SelectedItem = item;
                }
            }
        }

        public static void Select<TModel>(this UltraComboEditor selectList, TModel model)
        {
            foreach (var item in selectList.Items)
            {
                if (model.Equals(item.DataValue))
                {
                    selectList.SelectedItem = item;
                }
            }
        }
    }
}
