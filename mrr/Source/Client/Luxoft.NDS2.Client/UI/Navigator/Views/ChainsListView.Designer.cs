﻿namespace Luxoft.NDS2.Client.UI.Navigator.Views
{
    partial class ChainsListView
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._statusPanel = new Infragistics.Win.Misc.UltraPanel();
            this._status = new Infragistics.Win.Misc.UltraLabel();
            this._pairsList = new Luxoft.NDS2.Client.UI.Controls.Grid.V2.GridView();
            this._menu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._viewChainContractorsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._viewChainMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._exportFileDialog = new System.Windows.Forms.SaveFileDialog();
            this._statusPanel.ClientArea.SuspendLayout();
            this._statusPanel.SuspendLayout();
            this._menu.SuspendLayout();
            this.SuspendLayout();
            // 
            // _statusPanel
            // 
            // 
            // _statusPanel.ClientArea
            // 
            this._statusPanel.ClientArea.Controls.Add(this._status);
            this._statusPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this._statusPanel.Location = new System.Drawing.Point(0, 0);
            this._statusPanel.Name = "_statusPanel";
            this._statusPanel.Size = new System.Drawing.Size(600, 40);
            this._statusPanel.TabIndex = 0;
            // 
            // _status
            // 
            this._status.Dock = System.Windows.Forms.DockStyle.Top;
            this._status.Location = new System.Drawing.Point(0, 0);
            this._status.Name = "_status";
            this._status.Size = new System.Drawing.Size(600, 23);
            this._status.TabIndex = 0;
            this._status.Text = "Статус ";
            // 
            // _pairsList
            // 
            this._pairsList.Dock = System.Windows.Forms.DockStyle.Fill;
            this._pairsList.GridContextMenuStrip = this._menu;
            this._pairsList.Location = new System.Drawing.Point(0, 40);
            this._pairsList.Margin = new System.Windows.Forms.Padding(0);
            this._pairsList.Name = "_pairsList";
            this._pairsList.Size = new System.Drawing.Size(600, 410);
            this._pairsList.TabIndex = 1;
            // 
            // _menu
            // 
            this._menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._viewChainContractorsMenuItem,
            this._viewChainMenuItem});
            this._menu.Name = "_menu";
            this._menu.Size = new System.Drawing.Size(273, 48);
            // 
            // _viewChainContractorsMenuItem
            // 
            this._viewChainContractorsMenuItem.Name = "_viewChainContractorsMenuItem";
            this._viewChainContractorsMenuItem.Size = new System.Drawing.Size(272, 22);
            this._viewChainContractorsMenuItem.Text = "Обзор налогоплательщиков в цепочке";
            this._viewChainContractorsMenuItem.Click += new System.EventHandler(this.OnChainContractorsViewClick);
            // 
            // _viewChainMenuItem
            // 
            this._viewChainMenuItem.Name = "_viewChainMenuItem";
            this._viewChainMenuItem.Size = new System.Drawing.Size(272, 22);
            this._viewChainMenuItem.Text = "Обзор цепочки";
            this._viewChainMenuItem.Click += new System.EventHandler(this.OnChainViewClick);
            // 
            // _exportFileDialog
            // 
            this._exportFileDialog.FileName = "Выявленные_пары_нп.xlsx";
            this._exportFileDialog.Filter = "Excel Workbook (*.xlsx)|*.xlsx";
            this._exportFileDialog.RestoreDirectory = true;
            this._exportFileDialog.Title = "Имя файла с данными";
            // 
            // ChainsListView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this._pairsList);
            this.Controls.Add(this._statusPanel);
            this.Name = "ChainsListView";
            this.Size = new System.Drawing.Size(600, 450);
            this.Load += new System.EventHandler(this.OnLoad);
            this._statusPanel.ClientArea.ResumeLayout(false);
            this._statusPanel.ResumeLayout(false);
            this._menu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraPanel _statusPanel;
        private Controls.Grid.V2.GridView _pairsList;
        private Infragistics.Win.Misc.UltraLabel _status;
        private System.Windows.Forms.ContextMenuStrip _menu;
        private System.Windows.Forms.ToolStripMenuItem _viewChainContractorsMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _viewChainMenuItem;
        private System.Windows.Forms.SaveFileDialog _exportFileDialog;
    }
}
