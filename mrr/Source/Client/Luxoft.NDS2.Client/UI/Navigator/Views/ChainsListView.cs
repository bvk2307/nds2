﻿using Infragistics.Excel;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.UI.Base.Ribbon;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Addins;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Navigator.Views
{
    public partial class ChainsListView : UserControl, IChainsListView
    {
        # region Поля

        private readonly IChainsListPresenter _presenter;

        private readonly INotifier _notifier;
        private Workbook _workBook;

        # endregion

        # region Конструкторы

        public ChainsListView()
        {
            InitializeComponent();
        }

        public ChainsListView(
            IChainsListPresenter presenter,
            IRibbonAdapter ribbon,
            INotifier notifier)
            : this()
        {
            _presenter = presenter;
            _notifier = notifier;

            InitRibbonMenu(ribbon);
            InitGrid();
            InitChildBand();
            SetData();
            BindModelEvents();

            _pairsList.Grid.BeforeRowActivate += SetupContextMenu;
        }

        # endregion

        # region Реализация IView

        public string Title
        {
            get
            {
                return "Навигатор – выявленные пары налогоплательщиков";
            }
        }

        public void ExportToExcel()
        {
            if (_workBook == null)
            {
                _notifier.ShowError("Невозможно произвести выгрузку данных");
            }
            else
            {
                if (_exportFileDialog.ShowDialog() == DialogResult.OK)
                {
                    _workBook.Save(_exportFileDialog.FileName);
                }
            }
        }

        # endregion

        # region Инициализация таблицы

        private void InitGrid()
        {
            var mainColumns = new GridColumnSetup();
            var helper = new ColumnHelper<Models.PairModel>(_pairsList);

            mainColumns.Columns.Add(
                helper
                    .CreateTextColumn(model => model.TaxPayersTitle, 1, 300)
                    .Configure(col => { col.DisableSort = true; col.DisableFilter = true; }));
            mainColumns.Columns.Add(
                helper
                    .CreateTextColumn(model => model.ChainsQuantity, 1, 300)
                    .Configure(col => { col.DisableSort = true; col.DisableFilter = true; }));
            mainColumns.Columns.Add(
                helper
                    .CreateTextColumn(model => model.TargetTaxPayersInfo, 1, 300)
                    .Configure(col => { col.DisableSort = true; col.DisableFilter = true; }));
            mainColumns.Columns.Add(
                helper
                    .CreateTextColumn(model => model.LinksQuantityInfo, 1, 300)
                    .Configure(col => { col.DisableSort = true; col.DisableFilter = true; }));

            _pairsList.InitColumns(mainColumns);
            _pairsList.Grid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
        }

        private void InitChildBand()
        {
            var childColumns = new GridColumnSetup();
            var helper = new ColumnHelper<Models.ChainSummaryModel>(_pairsList);

            childColumns.Columns.Add(
                helper
                    .CreateChildTextColumn(model => model.TargetQuantity, 1, 300)
                    .Configure(col => { col.DisableSort = true; col.DisableFilter = true; }));
            childColumns.Columns.Add(
                helper
                    .CreateChildTextColumn(model => model.LinksQuantity, 1, 300)
                    .Configure(col => { col.DisableSort = true; col.DisableFilter = true; }));
            childColumns.Columns.Add(
                helper
                    .CreateChildTextColumn(model => model.Number, 1, 300)
                    .Configure(col => { col.DisableSort = true; col.DisableFilter = true; }));

            _pairsList.RegisterTableAddin(
                new ChildBandAddin(childColumns));
        }

        # endregion

        # region Инициализация Меню

        private void InitRibbonMenu(IRibbonAdapter ribbon)
        {
            ribbon.ToggleVisibility(new[] { MenuCommands.ExportToExcel });
        }

        # endregion

        # region Отображение данных модели

        private void SetData()
        {
            _status.Text = _presenter.Model.Status.ToString();
            _status.Appearance.ForeColor =
                _presenter.Model.Status.Failed
                    ? Color.Red
                    : Color.Gray;
            _status.Appearance.FontData.Bold =
                _presenter.Model.Status.Failed
                    ? DefaultableBoolean.True
                    : DefaultableBoolean.Default;
        }

        private void PushPairsToList()
        {
            _pairsList.PushData(_presenter.Model.Pairs);
            _workBook = _pairsList.ExportToWorkbook();
        }

        private void BindModelEvents()
        {
            _presenter.Model.OnError += _notifier.ShowError;
            _presenter.Model.OnNotification += _notifier.ShowNotification;
            _presenter.Model.Status.StatusChanged += SetData;
            _presenter.Model.PairsLoaded += PushPairsToList;
        }

        # endregion

        # region Обработка событий пользовательского интерфейса

        private void OnLoad(object sender, System.EventArgs e)
        {
            _presenter.StartLoading();
        }

        private void SetupContextMenu(object sender, RowEventArgs args)
        {
            _viewChainContractorsMenuItem.Visible = args.Row.ListObject is Models.PairModel;
            _viewChainMenuItem.Visible = args.Row.ListObject is Models.ChainSummaryModel;
        }

        private void OnChainContractorsViewClick(object sender, EventArgs e)
        {
            if (_pairsList.Grid.ActiveRow == null)
            {
                return;
            }

            var selectedItem = _pairsList.Grid.ActiveRow.ListObject as Models.PairModel;

            if (selectedItem != null)
            {
                _presenter.OpenChainContractorsView(selectedItem);
            }
        }

        private void OnChainViewClick(object sender, EventArgs e)
        {
            if (_pairsList.Grid.ActiveRow == null)
            {
                return;
            }

            var selectedItem = _pairsList.Grid.ActiveRow.ListObject as Models.ChainSummaryModel;

            if (selectedItem != null)
            {
                _presenter.OpenChainView(selectedItem);
            }
        }

        # endregion
    }
}
