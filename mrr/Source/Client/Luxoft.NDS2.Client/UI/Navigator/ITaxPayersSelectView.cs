﻿using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Navigator
{
    public interface ITaxPayersSelectView
    {
        DialogResult ShowDialog();
    }
}
