﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Navigator
{
    public interface ITaxPayersSelectPresenter
    {
        Models.ReportSetupModel Model
        {
            get;
        }

        void UpdateData(QueryConditions query, bool updateTotalQuantity = true);
    }
}
