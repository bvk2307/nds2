﻿using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Base.Navigation;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Navigator;
using Luxoft.NDS2.Common.Contracts.Services;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Navigator.Presenters
{
    public class ChainPresenter : IChainPresenter
    {
        private const string UnexpectedServerError = "Ошибка соединения с сервером МРР";

        private const string LoadChainErrorFormat = "Ошибка загрузки данных цепочки. {0}";

        private readonly INavigatorReportDataService _service;

        private readonly ITaxPayerDetails _tabOpener;

        public ChainPresenter(
            Models.ChainModel model,
            INavigatorReportDataService service,
            ITaxPayerDetails legacyTabOpener)
        {
            Model = model;
            _service = service;
            _tabOpener = legacyTabOpener;
        }

        public Models.ChainModel Model
        {
            get;
            private set;
        }

        public void StartLoading()
        {
            StartLoadingChain();        
        }

        public void OpenTaxPayerDetails()
        {
            if (Model.ActiveContractor == null)
            {
                return;
            }

            _tabOpener.ViewByKppOriginal(
                Model.ActiveContractor.Inn,
                Model.ActiveContractor.Kpp);
        }

        private void StartLoadingChain()
        {
            Func<OperationResult<List<ChainContractorData>>> loader =
                () => _service.GetChain(Model.Id, Model.RequestId);

            loader.TryDoAsync(
                (error) =>
                    new OperationResult<List<ChainContractorData>>
                    {
                        Status = ResultStatus.Error,
                        Message = UnexpectedServerError
                    },
                (response) =>
                {
                    if (response.Status == ResultStatus.Success)
                    {
                        Model.SetChain(response.Result);
                    }
                    else
                    {
                        Model.SetChainLoadError(
                            string.Format(LoadChainErrorFormat, response.Message));
                    }
                });
                    
        }
    }
}
