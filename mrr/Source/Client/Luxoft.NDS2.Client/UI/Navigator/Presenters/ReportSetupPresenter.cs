﻿using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Base.Navigation;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Navigator.Presenters
{
    public class ReportSetupPresenter : IReportSetupPresenter
    {
        # region Конструкторы

        public ReportSetupPresenter(
            Models.ReportSetupModel model, 
            INavigatorReportDataService service,
            ITabOpener tabOpener,
            ITaxPayerDetails taxPayerDetails)
        {
            Model = model;
            _service = service;
            _tabOpener = tabOpener;
            _taxPayerDetails = taxPayerDetails;

            Model.BuyersStatusUpdateRequired += UpdateBuyerStatus;
        }

        # endregion

        # region Модель

        public Models.ReportSetupModel Model
        {
            get;
            private set;
        }

        # endregion

        # region Отправка запроса

        private readonly INavigatorReportDataService _service;

        private readonly ITabOpener _tabOpener;

        public void Submit()
        {
            Func<OperationResult<long>> action =
                () =>
                {
                    try
                    {
                        return _service.SubmitRequest(Model.Request());
                    }
                    catch
                    {
                        return new OperationResult<long>
                        {
                            Status = ResultStatus.Error,
                            Message = "Ошибка связи с сервером"
                        };
                    }
                };

            action.DoAsync(
                (response) => 
                {
                    if (response.Status == ResultStatus.Success)
                    {
                        OpenReportViewer(response.Result);
                    }
                    else
                    {
                        Model.SubmitError(response.Message);
                    }
                });
        }

        private void OpenReportViewer(long reportId)
        {
            _tabOpener.Details<IChainsListView>(reportId);
        }

        # endregion

        # region Обновление признака "Возмещенец"

        public void UpdateBuyerStatus()
        {
            Func<OperationResult<Dictionary<string, bool>>> action =
                () =>
                {
                    try
                    {
                        return _service.GetTaxPayersPurchaseStatus(
                            Model.IncludedInnList(),
                            Model.Period.Range());
                    }
                    catch
                    {
                        return new OperationResult<Dictionary<string, bool>>
                        {
                            Status = ResultStatus.Error,
                            Message = "Ошибка связи с сервером"
                        };
                    }
                };

            action.DoAsync(
                (response) =>
                {
                    if (response.Status == ResultStatus.Success)
                    {
                        Model.TaxPayers.UpdateTaxPayerStatus(response.Result);
                    }
                    else
                    {
                        Model.RaiseError(
                            string.Format(
                                "Ошибка поиска деклараций на возмещение НДС. {0}", 
                                response.Message));
                    }
                });
        }

        # endregion

        # region Открытие карточек НП

        private readonly ITaxPayerDetails _taxPayerDetails;

        public void ViewTaxPayerDetails()
        {
            foreach (var taxPayer in Model.TaxPayers.Selected)
            {
                _taxPayerDetails.ViewByKppOriginal(taxPayer.Inn, taxPayer.Kpp);
            }
        }

        # endregion
    }
}
