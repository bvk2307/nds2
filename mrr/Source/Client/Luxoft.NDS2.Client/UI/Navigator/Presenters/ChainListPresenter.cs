﻿using System.Linq;
using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Navigator;
using Luxoft.NDS2.Common.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Threading;

namespace Luxoft.NDS2.Client.UI.Navigator.Presenters
{
    public class ChainListPresenter : IChainsListPresenter
    {
        private const int WaitInterval = 1000;

        private const string StatusLoadFailedMessagePattern =
            "Не удалось получить статус запроса данных отчета. {0}";

        private const string DataLoadFailedMessagePattern =
            "Не удалось получить список найденых пар. {0}";

        private const string ServerError = "Ошибка соединения с сервером.";

        private readonly INavigatorReportDataService _service;

        private readonly ITabOpener _tabOpener;

        public ChainListPresenter(
            Models.ReportModel model,
            INavigatorReportDataService service,
            ITabOpener tabOpener)
        {
            Model = model;
            _service = service;
            _tabOpener = tabOpener;
        }

        public Models.ReportModel Model
        {
            get;
            private set;
        }

        # region Загрузка данных

        public void StartLoading()
        {
            Action waitTilCompleted =
                () =>
                {
                    do
                    {
                        OperationResult<RequestStatus> response = null;

                        try
                        {
                            response = _service.GetRequestStatus(Model.RequestId);
                        }
                        catch
                        {
                            response =
                                new OperationResult<RequestStatus>()
                                {
                                    Status = ResultStatus.Error,
                                    Message = ServerError
                                };
                        }

                        if (response.Status == ResultStatus.Success)
                        {
                            Model.Status.Status = response.Result;

                            if (!Model.Status.Completed)
                            {
                                Thread.Sleep(WaitInterval);
                            }
                            else if (Model.Status.Status == RequestStatus.NotSuccessInNightMode)
                            {
                                Model.Status.StatusLoadNotSuccessInNightMode(ResourceManagerNDS2.SovNotSuccessInNightModeMessage);
                            }
                        }
                        else
                        {
                            Model.Status.StatusLoadError(
                                string.Format(StatusLoadFailedMessagePattern, response.Message));
                        }
                    }
                    while (!Model.Status.Completed && !Model.Status.LoadError);
                };

            waitTilCompleted.DoAsync(
                () =>
                {
                    if (Model.Status.Status == RequestStatus.Completed)
                    {
                        StartLoadingPairs();
                    }
                });
        }

        private void StartLoadingPairs()
        {
            Func<OperationResult<List<ChainSummary>>> action =
                () =>
                {
                    try
                    {
                        return _service.GetPairs(Model.RequestId);
                    }
                    catch
                    {
                        return new OperationResult<List<ChainSummary>>()
                        {
                            Status = ResultStatus.Error,
                            Message = ServerError
                        };
                    }
                };

            action.DoAsync(
                (response) =>
                {
                    if (response.Status == ResultStatus.Success)
                    {
                        //if (!response.Result.Any())
                        //{
                        //    response.Result.AddRange(CreateTestData());
                        //}

                        Model.SetPairs(response.Result);
                    }
                    else
                    {
                        Model.SetPairsLoadError(response.Message);
                    }
                });
        }

        public List<ChainSummary> CreateTestData()
        {
            var chain = new ChainSummary
            {
                PairId = 1,
                ChainId = 1,
                HeadTaxPayerInn = "789123456",
                HeadTaxPayerName = "ООО Витязь",
                TailTaxPayerName = "Агрокомплекс-1",
                TailTaxPayerInn = "4566543321",
                Number = 10,
                TargetTaxPayersQuantity = 2,
                LinksQuantity = 3
            };


            var chain2 = new ChainSummary
            {
                PairId = 1,
                ChainId = 2,
                HeadTaxPayerInn = "789123456",
                HeadTaxPayerName = "ООО Витязь",
                TailTaxPayerName = "Агрокомплекс-1",
                TailTaxPayerInn = "4566543321",
                Number = 10,
                TargetTaxPayersQuantity = 2,
                LinksQuantity = 3
            };

            var chain3 = new ChainSummary
            {
                PairId = 2,
                ChainId = 3,
                HeadTaxPayerInn = "11189320",
                HeadTaxPayerName = "ООО Василек",
                TailTaxPayerName = "ЧП Александров",
                TailTaxPayerInn = "777654321",
                Number = 10,
                TargetTaxPayersQuantity = 2,
                LinksQuantity = 3
            };


            var chain4 = new ChainSummary
            {
                PairId = 2,
                ChainId = 4,
                HeadTaxPayerInn = "11189320",
                HeadTaxPayerName = "ООО Василек",
                TailTaxPayerName = "ЧП Александров",
                TailTaxPayerInn = "777654321",
                Number = 10,
                TargetTaxPayersQuantity = 2,
                LinksQuantity = 3
            };

            return new List<ChainSummary>
            {
                chain,
                chain2,
                chain3,
                chain4
            };
        }

        # endregion

        # region Навигация к другим формам

        public void OpenChainContractorsView(Models.PairModel pair)
        {
            _tabOpener.Details<IChainContractorsListView>(Model.RequestId, pair.Id);
        }

        public void OpenChainView(Models.ChainSummaryModel chain)
        {
            _tabOpener.Details<IChainView>(Model.RequestId, chain.Id);
        }

        # endregion
    }
}
