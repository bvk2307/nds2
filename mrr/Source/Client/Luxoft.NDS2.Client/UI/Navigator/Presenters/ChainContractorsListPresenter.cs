﻿using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Base.Navigation;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Navigator;
using Luxoft.NDS2.Common.Contracts.Services;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Navigator.Presenters
{
    public class ChainContractorsListPresenter : IChainContractorsListPresenter
    {
        private const string DataLoadErrorFormat = "Ошибка загрузки данных.{0}";

        private readonly INavigatorReportDataService _service;

        private readonly ITabOpener _tabOpener;

        private readonly ITaxPayerDetails _legacyTabOpener;

        public ChainContractorsListPresenter(
            Models.ContractorsListModel model,
            INavigatorReportDataService service,
            ITabOpener tabOpener,
            ITaxPayerDetails legacyTabOpener)
        {
            Model = model;
            _service = service;
            _tabOpener = tabOpener;
            _legacyTabOpener = legacyTabOpener;
        }

        public Models.ContractorsListModel Model
        {
            get;
            private set;
        }

        public void StartLoading()
        {
            Func<OperationResult<List<ChainContractorData>>> action =
                () => _service.GetChainContractors(Model.PairId, Model.RequestId);

            action.TryDoAsync(
                (error) =>
                    new OperationResult<List<ChainContractorData>>
                    {
                        Status = ResultStatus.Error,
                        Message = string.Format("Неизвестная ошибка")
                    },
                (response) =>
                {
                    if (response.Status == ResultStatus.Success)
                    {
                        Model.SetContractors(response.Result);
                    }
                    else
                    {
                        Model.SetContractorsLoadError(
                            string.Format(DataLoadErrorFormat, response.Message));
                    }
                });
        }

        public void ViewDetails()
        {
            if (Model.ActiveContractor == null)
            {
                return;
            }

            _legacyTabOpener.ViewByKppOriginal(
                Model.ActiveContractor.Inn,
                Model.ActiveContractor.Kpp);
        }

        public void ViewChain(long chainId)
        {
            _tabOpener.Details<IChainView>(Model.RequestId, chainId);
        }
    }
}
