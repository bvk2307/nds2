﻿using CommonComponents.Utils.Async;
using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using dto = Luxoft.NDS2.Common.Contracts.DTO.Navigator;
using Luxoft.NDS2.Common.Contracts.Services;
using System;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Client.UI.Navigator.Presenters
{
    public class TaxPayersSelectPresenter : ITaxPayersSelectPresenter
    {
        private const string UnexepectedError = "Ошибка соединения с сервером";

        private const string LoadErrorFormat = "Не удалось загрузить список НП. {0}";

        private readonly INavigatorReportDataService _dataService;

        public TaxPayersSelectPresenter(
            Models.ReportSetupModel model,
            INavigatorReportDataService dataService)
        {
            Model = model;
            _dataService = dataService;
        }

        public Models.ReportSetupModel Model
        {
            get;
            private set;
        }

        public void UpdateData(QueryConditions query, bool updateTotalQuantity = true)
        {
            if (!updateTotalQuantity || UpdateQuantity(query.Filter))
            {
                UpdateData(query);
            }
        }

        private bool UpdateQuantity(List<FilterQuery> filter)
        {
            OperationResult<int> response = null;

            try
            {
                response =
                    _dataService.CountTaxPayers(
                                    new dto.CountTaxPayersContract
                                    {
                                        Range = Model.Period.Range(),
                                        Filter = filter
                                    });
            }
            catch(Exception ex)
            {
                response =
                    new OperationResult<int>
                    {
                        Status = ResultStatus.Error,
                        Message = UnexepectedError
                    };
            }

            if (response.Status == ResultStatus.Success)
            {
                Model.TaxPayers.TotalQuantity = (uint)response.Result;
                return true;
            }
            else
            {
                Model.RaiseError(response.Message);
                return false;
            }
        }

        private void UpdateData(QueryConditions query)
        {
            OperationResult<List<dto.TaxPayer>> response = null;

            try
            {
                response =
                    _dataService.GetTaxPayers(
                        new dto.LoadTaxPayersContract
                        {
                            Filter = query.Filter,
                            PageIndex = query.PageIndex,
                            PageSize = query.PageSize,
                            Range = Model.Period.Range(),
                            Sort = query.Sorting
                        });
            }
            catch
            {
                response =
                    new OperationResult<List<dto.TaxPayer>>
                    {
                        Status = ResultStatus.Error,
                        Message = UnexepectedError
                    };
            }

            if (response.Status == ResultStatus.Success)
            {
                Model.TaxPayers.SetTaxPayers(response.Result);
            }
            else
            {
                Model.RaiseError(string.Format(LoadErrorFormat, response.Message));
            }
        }
    }
}
