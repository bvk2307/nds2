﻿using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Common.Contracts.DTO.Navigator;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Navigator.Models
{
    public class PeriodRangeModel
    {
        private const string InvalidPeriodFromMessageFormat =
            "Период формирования отчёта не может начинаться ранее {0}";

        private const string InvalidPeriodToMessage =
            "Период формирования отчёта не может заканчиваться позднее {0}";

        private const string InvalidPeriodMessage =
            "Начало периода не может быть позднее конца периода";

        private const string InvalidPeriodTitle =
            "Период формирования отчёта не задан";

        private const string PeriodTitle =
            "Период формирования отчёта: с {0} по {1}";

        private const int MaxYearDepth = 4;

        private const int AvailableQuarterDepth = 12;

        private readonly Dictionary<int, string> _allMonths =
            new Dictionary<int, string>()
            {
                { 1, "Январь" },
                { 2, "Февраль" },
                { 3, "Март" },
                { 4, "Апрель" },
                { 5, "Май" },
                { 6, "Июнь" },
                { 7, "Июль" },
                { 8, "Август" },
                { 9, "Сентябрь" },
                { 10, "Октябрь" },
                { 11, "Ноябрь" },
                { 12, "Декабрь" }
            };

        public PeriodRangeModel()
        {
            StartFrom = DateTime.Now.LastCompletedQuarterStart(_allMonths);
            StartFrom.ValueChanged += OnPeriodChanged;

            EndWith = DateTime.Now.LastCompletedQuarter(_allMonths);
            EndWith.ValueChanged += OnPeriodChanged;

            State = new PeriodState(true, string.Empty);
        }

        private readonly DateTime _currentDate = DateTime.Now;

        public PeriodState State
        {
            get;
            private set;
        }

        public PeriodModel StartFrom
        {
            get;
            private set;
        }

        public PeriodModel EndWith
        {
            get;
            private set;
        }

        public IEnumerable<MonthModel> AvailableMonths
        {
            get
            {
                return
                    _allMonths
                        .Select(keyValue => new MonthModel(_allMonths, keyValue.Key))
                        .ToArray();
            }
        }

        public IEnumerable<YearModel> AvailableYears
        {
            get
            {
                var allYears = new List<YearModel>();

                for (var counter = 0; counter < MaxYearDepth; counter++)
                {
                    allYears.Add(new YearModel(_currentDate.Year - counter));
                }

                return allYears.AsReadOnly();
            }
        }

        public PeriodRange Range()
        {
            if (State.Valid)
            {
                return new PeriodRange
                {
                    From = StartFrom.ToContract(),
                    To = EndWith.ToContract()
                };
            }

            return null;
        }

        public event ParameterlessEventHandler PeriodChanged;

        public event ParameterlessEventHandler PeriodStateChanged;

        private void OnPeriodChanged()
        {
            ValidatePeriod();

            if (PeriodChanged != null)
            {
                PeriodChanged();
            }
        }

        private void ValidatePeriod()
        {
            if (EndWith.Compare(StartFrom) == PeriodCompareResult.More)
            {
                SetPeriodState(false, InvalidPeriodMessage);
                return;
            }

            var maxEndWith = _currentDate.LastCompletedQuarter(_allMonths);

            if (maxEndWith.Compare(EndWith) == PeriodCompareResult.More)
            {
                SetPeriodState(false, string.Format(InvalidPeriodToMessage, maxEndWith));
                return;
            }

            var minStartFrom =
                _currentDate.AddQuarter(-AvailableQuarterDepth).QuarterStart(_allMonths);

            if (StartFrom.Compare(minStartFrom) == PeriodCompareResult.More)
            {
                SetPeriodState(false, string.Format(InvalidPeriodFromMessageFormat, minStartFrom));
                return;
            }

            SetPeriodState(true, string.Empty);
        }

        private void SetPeriodState(bool valid, string text)
        {
            if (State.Valid != valid || State.Text != text)
            {
                State = new PeriodState(valid, text);

                if (PeriodStateChanged != null)
                {
                    PeriodStateChanged();
                }
            }
        }

        public override string ToString()
        {
            return State.Valid 
                ? string.Format(PeriodTitle, StartFrom, EndWith) 
                : InvalidPeriodTitle;
        }
    }
}
