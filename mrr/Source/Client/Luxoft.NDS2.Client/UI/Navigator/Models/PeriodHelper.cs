﻿using Luxoft.NDS2.Common.Contracts.DTO.Pyramid; 
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Navigator.Models
{
    public static class PeriodHelper
    {
        private const int QuarterDurationMonths = 3;

        private const int LastMonthIndex = 12;

        public static PeriodModel LastCompletedQuarterStart(
            this DateTime currentDate,
            Dictionary<int, string> allMonths)
        {
            return DateTime.Now.AddMonths(-QuarterDurationMonths).QuarterStart(allMonths);
        }

        public static PeriodModel LastCompletedQuarter(
            this DateTime currentDate, 
            Dictionary<int, string> allMonths)
        {
            var month =
                currentDate.Month < QuarterDurationMonths
                ? LastMonthIndex
                : currentDate.Month.PrevQuarterComplete();

            var year =
                currentDate.Month < QuarterDurationMonths
                ? currentDate.Year - 1
                : currentDate.Year;

            return new PeriodModel(allMonths, year, month);
        }

        public static PeriodModel QuarterStart(
            this DateTime currentDate, 
            Dictionary<int, string> allMonths)
        {
            return new PeriodModel(
                allMonths,
                currentDate.Year,
                currentDate.Month.PrevQuarterComplete());
        }

        public static DateTime AddQuarter(this DateTime source, int quarterQuantity)
        {
            return source.AddMonths(QuarterDurationMonths * quarterQuantity);
        }

        public static PeriodCompareResult PeriodCompare(this int val, int valToCompare)
        {
            if (val > valToCompare)
            {
                return PeriodCompareResult.Less;
            }

            if (val < valToCompare)
            {
                return PeriodCompareResult.More;
            }

            return PeriodCompareResult.Equal;
        }

        private static int PrevQuarterComplete(this int monthIndex)
        {
            return QuarterDurationMonths * monthIndex.PrevQuarterIndex();                    
        }

        private static int PrevQuarterIndex(this int monthIndex)
        {
            return (int)Math.Floor((double)(monthIndex - 1) / QuarterDurationMonths) + 1;
        }

        public static Period ToContract(this PeriodModel model)
        {
            var quarter = model.Month.Value.PrevQuarterIndex();

            return new Period(quarter, model.Year.Value);
        }
    }
}
