﻿using System.ComponentModel;
using contracts = Luxoft.NDS2.Common.Contracts.DTO.Navigator;

namespace Luxoft.NDS2.Client.UI.Navigator.Models
{
    public class TaxPayerModel
    {
        # region Конструктор

        public TaxPayerModel(contracts.TaxPayer data, bool selected = false)
        {
            Selected = selected;
            Inn = data.Inn;
            Kpp = data.Kpp;
            Name = data.Name;
            Inspection = data.Inspection;
            Region = data.Region;
            FederalDistrict = data.FederalDistrict;
            SurCode = data.SurCode;
            _salesAmount = new CurrencyModel(data.SalesAmount);
            _purchaseAmount = new CurrencyModel(data.PurchaseAmount);
            _ndsCalculated = new CurrencyModel(data.NdsCalculated);
            _ndsDeduction = new CurrencyModel(data.NdsDeduction);
        }

        # endregion

        # region Признак выбора в списке

        public bool Selected
        {
            get;
            set;
        }

        # endregion

        # region Данные НП

        [DisplayName("ИНН")]
        [Description("ИНН налогоплательщика")]
        public string Inn
        {
            get;
            private set;
        }

        [DisplayName("КПП")]
        [Description("КПП налогоплательщика")]
        public string Kpp
        {
            get;
            private set;
        }

        [DisplayName("Наименование")]
        [Description("Наименование налогоплательщика")]
        public string Name
        {
            get;
            private set;
        }

        [DisplayName("Инспекция")]
        [Description("Код и наименование налогового органа")]
        public string Inspection
        {
            get;
            private set;
        }

        [DisplayName("Регион")]
        [Description("Регион налогоплательщика")]
        public string Region
        {
            get;
            private set;
        }

        [DisplayName("Федеральный округ")]
        [Description("Федеральный округ налогоплательщика")]
        public string FederalDistrict
        {
            get;
            private set;
        }

        [DisplayName("СУР")]
        [Description("СУР налогоплательщика")]
        public int SurCode
        {
            get;
            private set;
        }

        private readonly CurrencyModel _salesAmount;

        [DisplayName("Сумма продаж")]
        [Description("Сумма продаж налогоплательщика за все выбранные отчетные периоды")]
        public string SalesAmount
        {
            get
            {
                return _salesAmount.ToString();
            }
        }

        private readonly CurrencyModel _purchaseAmount;

        [DisplayName("Сумма покупок")]
        [Description("Сумма покупок налогоплательщика за все выбранные отчетные периоды")]
        public string PurchaseAmount
        {
            get
            {
                return _purchaseAmount.ToString();
            }
        }

        private readonly CurrencyModel _ndsCalculated;

        [DisplayName("Исчисленный НДС")]
        [Description("Сумма исчисленного НДС налогоплательщика за все выбранные отчетные периоды")]
        public string NdsCalculated
        {
            get
            {
                return _ndsCalculated.ToString();
            }
        }

        private readonly CurrencyModel _ndsDeduction;

        [DisplayName("НДС к вычету")]
        [Description("Сумма НДС к вычету за все выбранные отчетные периоды")]
        public string NdsDeduction
        {
            get
            {
                return _ndsDeduction.ToString();
            }
        }

        # endregion
    }
}
