﻿namespace Luxoft.NDS2.Client.UI.Navigator.Models
{
    public class CurrencyModel
    {
        private decimal? _value;

        public CurrencyModel(decimal? value)
        {
            _value = value;
        }

        public override string ToString()
        {
            return !_value.HasValue
                ? string.Empty
                : _value.Value.ToString();
        }
    }
}
