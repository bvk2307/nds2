﻿using Luxoft.NDS2.Client.UI.Base;

namespace Luxoft.NDS2.Client.UI.Navigator.Models
{
    public abstract class ModelBase
    {
        public event GenericEventHandler<string> OnError;
        public event GenericEventHandler<string> OnNotification;

        protected void RaiseError(string message)
        {
            if (OnError != null)
            {
                OnError(message);
            }
        }

        protected void RaiseNotification(string message)
        {
            if (OnNotification != null)
            {
                OnNotification(message);
            }
        }
    }
}
