﻿using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Sur;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Navigator;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Navigator.Models
{
    public class ChainModel : ModelBase
    {
        # region Конструктор

        public ChainModel(long id, long requestId, DictionarySur surItems)
        {
            Id = id;
            RequestId = requestId;
            SurItems = surItems;
        }

        # endregion

        # region Данные цепочки

        public long Id
        {
            get;
            private set;
        }

        public long RequestId
        {
            get;
            private set;
        }

        private CurrencyModel _minMappedAmount = new CurrencyModel(0);

        public string MinMappedAmount
        {
            get
            {
                return _minMappedAmount.ToString();
            }
        }

        private CurrencyModel _maxMappedAmount = new CurrencyModel(0);

        public string MaxMappedAmount
        {
            get
            {
                return _maxMappedAmount.ToString();
            }
        }

        private CurrencyModel _minNotMappedAmount = new CurrencyModel(0);

        public string MinNotMappedAmount
        {
            get
            {
                return _minNotMappedAmount.ToString();
            }
        }

        private CurrencyModel _maxNotMappedAmount = new CurrencyModel(0);

        public string MaxNotMappedAmount
        {
            get
            {
                return _maxNotMappedAmount.ToString();
            }
        }

        private readonly List<ChainContractorModel> _contractors = 
            new List<ChainContractorModel>();

        public IEnumerable<ChainContractorModel> Contractors
        {
            get
            {
                return _contractors.AsReadOnly();
            }
        }

        private bool _chainLoaded;

        public void SetChain(IEnumerable<ChainContractorData> dataItems)
        {
            if (dataItems.Any())
            {
                var anyRow = dataItems.First();
                _minMappedAmount = new CurrencyModel(anyRow.ChainMinMappedAmount);
                _maxMappedAmount = new CurrencyModel(anyRow.ChainMaxMappedAmount);
                _minNotMappedAmount = new CurrencyModel(anyRow.ChainMinNotMappedAmount);
                _maxNotMappedAmount = new CurrencyModel(anyRow.ChainMaxNotMappedAmount);
            }

            var totalQuantity = dataItems.Count();
            _contractors.Clear();
            _contractors.AddRange(
                dataItems
                    .OrderBy(dataItem => dataItem.Index)
                    .Select(dataItem => Build(dataItem, totalQuantity)));

            BindActivatedEvent();

            _chainLoaded = true;
            TryRaiseChainLoaded();
        }

        private ChainContractorModel Build(ChainContractorData item, int totalQuantity)
        {
            if (item.Index == 0)
            {
                return new HeadContractorModel(item);
            }

            if (item.Index + 1 == totalQuantity)
            {
                return new TailContractorModel(item);
            }

            return new IntermediateContractorModel(item);
        }

        public void SetChainLoadError(string message)
        {
            RaiseError(message);
        }

        # endregion       

        # region Справочник СУР

        public DictionarySur SurItems
        {
            get;
            private set;
        }

        # endregion        

        # region Событие загрузки данных

        private static object _lock = new object(); 

        public event ParameterlessEventHandler ChainLoaded;

        private void TryRaiseChainLoaded()
        {
            if (ChainLoaded == null)
            {
                return;
            }

            lock (_lock)
            {
                if (_chainLoaded)
                {
                    ChainLoaded();
                }
            }
        }

        # endregion

        # region Текущий контрагент

        public ChainContractorModel ActiveContractor
        {
            get;
            private set;
        }

        private void BindActivatedEvent()
        {
            foreach(var contractor in _contractors)
            {
                contractor.Activated += (sender) => ActiveContractor = sender;
            }
        }

        # endregion
    }
}
