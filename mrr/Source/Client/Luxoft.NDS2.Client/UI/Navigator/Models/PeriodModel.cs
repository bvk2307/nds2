﻿using Luxoft.NDS2.Client.UI.Base;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Navigator.Models
{
    public enum PeriodCompareResult { More, Less, Equal, Incomparable }

    /// <summary>
    /// Модель данных периода
    /// </summary>
    public class PeriodModel : IPeriod
    {
        public PeriodModel(Dictionary<int, string> allMonths, int year, int month)
        {
            Month = new MonthModel(allMonths, month);
            Year = new YearModel(year);

            Month.ValueChanged += RaiseValueChanged;
            Year.ValueChanged += RaiseValueChanged;
        }

        public MonthModel Month
        {
            get;
            private set;
        }

        public YearModel Year
        {
            get;
            private set;
        }

        public event ParameterlessEventHandler ValueChanged;

        private void RaiseValueChanged()
        {
            if (ValueChanged != null)
            {
                ValueChanged();
            }
        }

        public PeriodCompareResult Compare(object objectToCompare)
        {
            var periodToCompare = objectToCompare as PeriodModel;

            if (periodToCompare == null)
            {
                return PeriodCompareResult.Incomparable;
            }

            var yearCompare = Year.Compare(periodToCompare.Year);

            if (yearCompare == PeriodCompareResult.Equal)
            {
                return Month.Compare(periodToCompare.Month);
            }

            return yearCompare;
        }

        private const string Format = "{0}, {1}";

        public override string ToString()
        {
            return string.Format(Format, Month, Year);
        }
    }
}
