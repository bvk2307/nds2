﻿using Luxoft.NDS2.Client.UI.Base;
namespace Luxoft.NDS2.Client.UI.Navigator.Models
{
    public class YearModel : IPeriod
    {
        private int _year;

        public YearModel(int year)
        {
            _year = year;
        }

        public int Value
        {
            get
            {
                return _year;
            }
            set
            {
                if (_year != value)
                {
                    _year = value;
                    RaiseValueChanged();
                }
            }
        }

        public string Text
        {
            get
            {
                return _year.ToString();
            }
        }

        public override string ToString()
        {
            return Text;
        }

        public event ParameterlessEventHandler ValueChanged;

        private void RaiseValueChanged()
        {
            if (ValueChanged != null)
            {
                ValueChanged();
            }
        }

        public PeriodCompareResult Compare(object objectToCompare)
        {
            var yearToCompare = objectToCompare as YearModel;

            if (yearToCompare == null)
            {
                return PeriodCompareResult.Incomparable;
            }

            return Value.PeriodCompare(yearToCompare.Value);
        }
    }
}
