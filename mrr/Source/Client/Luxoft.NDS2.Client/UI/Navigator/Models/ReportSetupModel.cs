﻿using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Sur;
using Luxoft.NDS2.Common.Contracts.DTO.Navigator;
using Luxoft.NDS2.Common.Contracts.DTO.Security;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Navigator.Models
{
    public class ReportSetupModel
    {
        # region Конструкторы

        public ReportSetupModel(DictionarySur surItems, ContractorDataPolicy configuration)
        {
            Period = new PeriodRangeModel();
            Period.PeriodStateChanged += RaiseValidChanged;
            Period.PeriodChanged += OnPeriodChanged;

            TaxPayers = new ReportTaxPayerListModel(MinTaxPayers, MaxTaxPayers, surItems);
            TaxPayers.ReportTaxPayersChanged += OnReportTaxPayersChanged;
            TaxPayers.TaxPayersBuyerStatusUpdated += RaiseValidChanged;
            TaxPayers.TaxPayersLoadError += RaiseError;

            MinMappedAmount = 0;
            MaxContractorsQuantity = DefaultContractorsQuantity;
            MinBuyerAmount = DefaultAmount;
            MinSellerAmount = DefaultAmount;
            BuyersOnly = !configuration.AllowPurchase;
            _maxDepth = configuration.MaxNavigatorChains;
        }

        # endregion

        # region Период отчета

        public PeriodRangeModel Period
        {
            get;
            private set;
        }

        # endregion

        # region Обработка событий

        /// <summary>
        /// Обрабатывает событие изменения периода формирования отчета.
        /// Попытка обновить статус "Возмещенец" у всех выбранных НП
        /// </summary>
        private void OnPeriodChanged()
        {
            TryUpdateBuyersStatus();
        }

        /// <summary>
        /// Обрабатывает событие изменение выбранных НП для отчета
        /// 1. Поднятие события говорящего о том, что возможно валидность параметров отчета - изменилась.
        /// 2. Попытка обновить статус "Возмещенец" у всех выбранных НП
        /// </summary>
        private void OnReportTaxPayersChanged()
        {
            RaiseValidChanged();
            TryUpdateBuyersStatus();
        }

        /// <summary>
        /// Если действует ограничение, что в цепочке должен быть как минимум 1 "Возмещенец", а так же достаточно данных, для того чтобы запросить обновление этого статуса, то
        /// сброс значение статуса "Возмещенец" у всех выбранных НП и поднятие события, говорящего о том ,что модели требуется обновление данных о статусе "Возмещенец"
        /// </summary>
        private void TryUpdateBuyersStatus()
        {
            if (BuyersStatusCanBeUpdated && BuyersOnly)
            {
                TaxPayers.ResetTaxPayerStatus();
                RaiseBuyersUpdateRequired();
            }
        }

        # endregion

        # region Выбранные НП

        private const int MinTaxPayers = 2;

        private const int MaxTaxPayers = 6;

        public ReportTaxPayerListModel TaxPayers
        {
            get;
            private set;
        }

        # endregion

        # region Прочие параметры отчета

        private const string ContractorsQuantityTooLowPattern = "Максимальное количество контрагентов должно быть не менее {0}";

        private const string ContractorsQuantityTooMuchPattern = "Максимально количество контрагентов должно быть не более {0}";

        private const string InputNeededMessage = "Необходимо указать значение";

        private const int DefaultContractorsQuantity = 10;

        private const int ContractorsQuantityLowerLimit = 1;

        private const int ContractorsQuantityUpperLimit = 100;

        private const int DefaultAmount = 1000;

        private const int MinDepth = 1;

        private readonly int _maxDepth;

        private const int DefaultDepth = 3;

        private int _depth = DefaultDepth;

        public int Depth
        {
            get
            {
                return _depth;
            }
            set
            {
                if (value < MinDepth || value > _maxDepth)
                {
                    throw new ArgumentException();
                }

                _depth = value;
            }
        }

        public IEnumerable<int> AvailableDepths
        {
            get
            {
                var allDepths = new List<int>();

                for (var depth = MinDepth; depth <= _maxDepth; depth++)
                {
                    allDepths.Add(depth);
                }

                return allDepths.AsReadOnly();
            }
        }

        private int? _minMappedAmount;
        public int? MinMappedAmount
        {
            get
            {
                return _minMappedAmount;
            }
            set
            {
                if (value != _minMappedAmount)
                {
                    var currentValidState = InputNeededValid;

                    _minMappedAmount = value;

                    if (currentValidState != InputNeededValid)
                    {
                        RaiseInputNeededValidChanged();
                    }
                }
            }
        }


        public bool MaxContractorsValid
        {
            get
            {
                return MaxContractorsQuantity >= ContractorsQuantityLowerLimit
                    && MaxContractorsQuantity <= ContractorsQuantityUpperLimit;
            }
        }

        public string MaxContractorsErrorMessage
        {
            get
            {
                if (MaxContractorsValid)
                {
                    return string.Empty;
                }

                return MaxContractorsQuantity < ContractorsQuantityLowerLimit
                    ? string.Format(ContractorsQuantityTooLowPattern, ContractorsQuantityLowerLimit)
                    : string.Format(ContractorsQuantityTooMuchPattern, ContractorsQuantityUpperLimit);
            }
        }

        public bool InputNeededValid
        {
            get
            {
                return MaxContractorsQuantity != null
                    && MinMappedAmount != null
                    && MinBuyerAmount != null
                    && MinSellerAmount != null;
            }
        }

        public string InputNeededErrorMessage
        {
            get
            {
                if (InputNeededValid)
                {
                    return string.Empty;
                }

                return InputNeededMessage;
            }
        }

        private int? _maxContractorsQuantity;
        public int? MaxContractorsQuantity
        {
            get
            {
                return _maxContractorsQuantity;
            }
            set
            {
                if (value != _maxContractorsQuantity)
                {
                    var currentValidState = MaxContractorsValid;

                    _maxContractorsQuantity = value;

                    if (currentValidState != MaxContractorsValid)
                    {
                        RaiseMaxContractorsValidChanged();
                    }
                }
            }
        }

        public event ParameterlessEventHandler OnMaxContractorsValidChanged;

        public event ParameterlessEventHandler OnInputNeededValidChanged;

        private void RaiseMaxContractorsValidChanged()
        {
            if (OnMaxContractorsValidChanged != null)
            {
                OnMaxContractorsValidChanged();
            }

            RaiseValidChanged();
        }

        private void RaiseInputNeededValidChanged()
        {
            if (OnInputNeededValidChanged != null)
            {
                OnInputNeededValidChanged();
            }

            RaiseValidChanged();
        }

        private int? _minBuyerAmount;
        public int? MinBuyerAmount
        {
            get
            {
                return _minBuyerAmount;
            }
            set
            {
                if (value != _minBuyerAmount)
                {
                    var currentValidState = InputNeededValid;

                    _minBuyerAmount = value;

                    if (currentValidState != InputNeededValid)
                    {
                        RaiseInputNeededValidChanged();
                    }
                }
            }
        }

        private int? _minSellerAmount;
        public int? MinSellerAmount
        {
            get
            {
                return _minSellerAmount;
            }
            set
            {
                if (value != _minSellerAmount)
                {
                    var currentValidState = InputNeededValid;

                    _minSellerAmount = value;

                    if (currentValidState != InputNeededValid)
                    {
                        RaiseInputNeededValidChanged();
                    }
                }
            }
        }

        # endregion

        # region Ограничение прав на глубину построения отчета

        public event ParameterlessEventHandler BuyersStatusUpdateRequired;

        private void RaiseBuyersUpdateRequired()
        {
            if (BuyersStatusUpdateRequired != null)
            {
                BuyersStatusUpdateRequired();
            }
        }

        public bool BuyersOnly
        {
            get;
            private set;
        }

        /// <summary>
        /// Возвращает признак того, что у модели достаточно данных для обновления статуса "Возмещенец" у выбранных НП.
        /// 1) Должен быть выбран хотя бы 1 НП 
        /// 2) Должен быть задан валидный период отчета (значение признака зависит от периода)
        /// </summary>
        private bool BuyersStatusCanBeUpdated
        {
            get
            {
                return Period.State.Valid
                    && TaxPayers.Included.Any();
            }
        }

        # endregion

        # region Отправка данных

        public Request Request()
        {
            return new Request
            {
                MonthFrom = Period.StartFrom.Month.Value,
                YearFrom = Period.StartFrom.Year.Value,
                MonthTo = Period.EndWith.Month.Value,
                YearTo = Period.EndWith.Year.Value,
                Depth = Depth,
                MinMappedAmount = MinMappedAmount.Value,
                MaxContractorsQuantity = MaxContractorsQuantity.Value,
                MinBuyerAmount = MinBuyerAmount.Value,
                MinSellerAmount = MinSellerAmount.Value,
                Contractors = TaxPayers.Included.Select(taxPayer => taxPayer.Inn).ToArray()
            };
        }

        public string[] IncludedInnList()
        {
            return TaxPayers
                .Included
                .Select(x => x.Inn)
                .ToArray();
        }

        public void SubmitError(string message)
        {
            RaiseError(message);
        }

        # endregion

        # region Валидация

        /// <summary>
        /// Срабатывает на изменение любого аттрибута модели, который влияет на валидность всей модели
        /// </summary>
        public ParameterlessEventHandler OnValidationAttributeChanged;

        private void RaiseValidChanged()
        {
            if (OnValidationAttributeChanged != null)
            {
                OnValidationAttributeChanged();
            }
        }

        public bool Valid
        {
            get
            {
                return
                    TaxPayers.State.IsValid
                    && Period.State.Valid
                    && MaxContractorsValid
                    && InputNeededValid
                    && (!BuyersOnly || TaxPayers.Included.Any(x => x.IsBuyer.HasValue && x.IsBuyer.Value));
            }
        }

        # endregion

        # region Ошибки модели

        public void RaiseError(string errorText)
        {
            if (OnError != null)
            {
                OnError(errorText);
            }
        }

        public GenericEventHandler<string> OnError;

        # endregion
    }
}
