﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Navigator.Models
{
    public class MinMaxModel<TModel>
    {
        private const string DisplayFormat = "{0}-{1}";

        private readonly int _min;

        private readonly int _max;

        public MinMaxModel(
            IEnumerable<TModel> dataRows,
            Func<TModel, int> getValue)
        {
            _min = dataRows.Min(dataRow => getValue(dataRow));
            _max = dataRows.Max(dataRow => getValue(dataRow));
        }

        public override string ToString()
        {
            return string.Format(DisplayFormat, _min, _max);
        }
    }
}
