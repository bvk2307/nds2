﻿using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Common.Contracts.DTO.Navigator;
using System.ComponentModel;

namespace Luxoft.NDS2.Client.UI.Navigator.Models
{
    public abstract class ChainContractorModel
    {
        protected static string EmptyLink = "-";

        protected ChainContractorModel(ChainContractorData data)
        {
            Data = data;
            MappedAmountModel = 
                new CoupledCurrencyModel(data.MappedAmountAsBuyer, data.MappedAmountAsSeller);
            AmountPerBuyerModel =
                new CoupledCurrencyModel(data.BuyerAmountAsBuyer, data.BuyerAmountAsSeller);
            AmountPerSellerModel =
                new CoupledCurrencyModel(data.SellerAmountAsBuyer, data.SellerAmountAsSeller);
        }

        protected ChainContractorData Data
        {
            get;
            private set;
        }

        protected CoupledCurrencyModel MappedAmountModel
        {
            get;
            private set;
        }

        protected CoupledCurrencyModel AmountPerBuyerModel
        {
            get;
            private set;
        }

        protected CoupledCurrencyModel AmountPerSellerModel
        {
            get;
            private set;
        }

        public string Inn
        {
            get
            {
                return Data.Inn;
            }
        }

        public string Kpp
        {
            get
            {
                return Data.Kpp;
            }
        }

        [DisplayName("Звено")]
        [Description("Номер звена в цепочке")]
        public abstract string Link
        {
            get;
        }

        [DisplayName("СУР")]
        [Description("Значение признака СУР по данным декларации НП за последний из выбранных отчётных периодов")]
        public int SurCode
        {
            get
            {
                return Data.SurCode;
            }
        }

        [DisplayName("Налогоплательщик")]
        [Description("Наименование налогоплательщика")]
        public string Name
        {
            get
            {
                return Data.Name;
            }
        }

        [DisplayName("Номер цепочки")]
        [Description("Номер цепочки")]
        public string ChainNumber
        {
            get
            {
                return Data.ChainNumber.ToString();
            }
        }

        [DisplayName("Операции по продаже (соп. поток)")]
        [Description("Операции по продаже (соп. поток)")]
        public abstract string MappedAmount
        {
            get;
        }

        [DisplayName("Операции по продаже (данные покупателя)")]
        [Description("Операции по продаже (данные покупателя)")]
        public abstract string AmountPerBuyer
        {
            get;
        }

        [DisplayName("Операции по продаже (данные продавца)")]
        [Description("Операции по продаже (данные продавца)")]
        public abstract string AmountPerSeller
        {
            get;
        }

        public event GenericEventHandler<ChainContractorModel> Activated;

        public void Activate()
        {
            if (Activated != null)
            {
                Activated(this);
            }
        }
    }

    public class HeadContractorModel : ChainContractorModel
    {
        public HeadContractorModel(ChainContractorData data)
            : base(data)
        {
        }

        public override string Link
        {
            get 
            { 
                return ChainContractorModel.EmptyLink; 
            }
        }

        public override string MappedAmount
        {
            get 
            { 
                return MappedAmountModel.ToString(); 
            }
        }

        public override string AmountPerBuyer
        {
            get 
            { 
                return AmountPerBuyerModel.ToString(); 
            }
        }

        public override string AmountPerSeller
        {
            get 
            { 
                return AmountPerSellerModel.ToString(); 
            }
        }
    }

    public class TailContractorModel : ChainContractorModel
    {
        public TailContractorModel(ChainContractorData data)
            : base(data)
        {
        }

        public override string Link
        {
            get 
            { 
                return ChainContractorModel.EmptyLink; 
            }
        }

        public override string MappedAmount
        {
            get 
            {
                return string.Empty;
            }
        }

        public override string AmountPerBuyer
        {
            get 
            { 
               return string.Empty; 
            }
        }

        public override string AmountPerSeller
        {
            get 
            { 
                return string.Empty; 
            }
        }
    }

    public class IntermediateContractorModel : ChainContractorModel
    {
        public IntermediateContractorModel(ChainContractorData data)
            : base(data)
        {
        }

        public override string Link
        {
            get 
            { 
                return Data.Index.ToString(); 
            }
        }

        public override string MappedAmount
        {
            get 
            { 
                return MappedAmountModel.ToString(); 
            }
        }

        public override string AmountPerBuyer
        {
            get 
            { 
                return AmountPerBuyerModel.ToString(); 
            }
        }

        public override string AmountPerSeller
        {
            get 
            { 
                return AmountPerSellerModel.ToString(); 
            }
        }
    }
}
