﻿namespace Luxoft.NDS2.Client.UI.Navigator.Models
{
    public interface IPeriod
    {
        PeriodCompareResult Compare(object objectToCompare);
    }
}
