﻿using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Common.Contracts.DTO.Navigator;
using System.ComponentModel;

namespace Luxoft.NDS2.Client.UI.Navigator.Models
{
    public class ChainSummaryModel
    {
        private const string NumberFormat = "Цепочка {0}";

        private readonly ChainSummary _data;

        public ChainSummaryModel(ChainSummary data)
        {
            _data = data;
        }

        public long Id
        {
            get
            {
                return _data.ChainId;
            }
        }

        [DisplayName("Количество целевых НП в цепочке")]
        [Description("Количество НП в цепочке из списка НП, выбранных для отчета")]
        public int TargetQuantity
        {
            get
            {
                return _data.TargetTaxPayersQuantity;
            }
        }

        [DisplayName("Количество промежуточных звеньев")]
        [Description("Количество промежуточных звеньев в цепочке")]
        public int LinksQuantity
        {
            get
            {
                return _data.LinksQuantity;
            }
        }

        [DisplayName("Порядковый номер цепочки")]
        [Description("Порядковый номер цепочки в рамках отчета")]
        public string Number
        {
            get
            {
                return string.Format(NumberFormat, _data.Number);
            }
        }
    }
}
