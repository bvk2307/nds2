﻿using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Common.Contracts.DTO.Navigator;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Navigator.Models
{
    public class ReportModel : ModelBase
    {
        # region Конструктор

        public ReportModel(long requestId)
        {
            RequestId = requestId;

            Status = new RequetsStatusModel();
            Status.OnError += RaiseError;
            Status.OnNotification += RaiseNotification;
        }

        # endregion

        # region Данные отчета

        public long RequestId
        {
            get;
            private set;
        }

        public RequetsStatusModel Status
        {
            get;
            private set;
        }

        # endregion

        # region Пары Контрагентов

        private readonly List<PairModel> _pairs = new List<PairModel>();        

        public IList<PairModel> Pairs
        {
            get
            {
                return _pairs;
            }
        }

        public void SetPairs(IEnumerable<ChainSummary> chains)
        {
            _pairs.Clear();
            _pairs.AddRange(
                chains
                    .GroupBy(chain => new { chain.HeadTaxPayerInn, chain.TailTaxPayerInn })
                    .Select(
                        pair =>
                            new PairModel(
                                chains.Where(
                                    chain =>
                                        chain.HeadTaxPayerInn == pair.Key.HeadTaxPayerInn
                                        && chain.TailTaxPayerInn == pair.Key.TailTaxPayerInn))));

            if (PairsLoaded != null)
            {
                PairsLoaded();
            }
        }

        public void SetPairsLoadError(string message)
        {
            RaiseError(message);
        }

        public event ParameterlessEventHandler PairsLoaded;

        # endregion
    }
}
