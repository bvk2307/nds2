﻿namespace Luxoft.NDS2.Client.UI.Navigator.Models
{
    public class CoupledCurrencyModel
    {
        private const string Format = "↓{0}|{1}↑";

        private readonly decimal _firstValue;

        private readonly decimal _secondValue;

        public CoupledCurrencyModel(decimal firstValue, decimal secondValue)
        {
            _firstValue = firstValue;
            _secondValue = secondValue;
        }

        public override string ToString()
        {
            return string.Format(
                Format,
                _firstValue,
                _secondValue);
        }
    }
}
