﻿using Luxoft.NDS2.Client.UI.Base;
using System.ComponentModel;

namespace Luxoft.NDS2.Client.UI.Navigator.Models
{
    public class ReportTaxPayerModel
    {
        # region Конструктор

        public ReportTaxPayerModel(TaxPayerModel taxPayer)
        {
            _selected = false;
            _taxPayer = taxPayer;
        }

        # endregion

        # region Выбор налогоплательщика в списке

        private bool _selected;

        public ParameterlessEventHandler SelectionChanged;

        public bool Selected
        {
            get
            {
                return _selected;
            }
            set
            {
                if (_selected != value)
                {
                    _selected = value;

                    if (SelectionChanged != null)
                    {
                        SelectionChanged();
                    }
                }
            }
        }

        # endregion

        # region Данные НП

        private readonly TaxPayerModel _taxPayer;

        [DisplayName("ИНН")]
        [Description("ИНН налогоплательщика")]
        public string Inn
        {
            get
            {
                return _taxPayer.Inn;
            }
        }

        [DisplayName("КПП")]
        [Description("КПП налогоплательщика")]
        public string Kpp
        {
            get
            {
                return _taxPayer.Kpp;
            }
        }

        [DisplayName("Наименование")]
        [Description("Наименование налогоплательщика")]
        public string Name
        {
            get
            {
                return _taxPayer.Name;
            }
        }

        [DisplayName("Инспекция")]
        [Description("Код и наименование налогового органа")]
        public string Inspection
        {
            get
            {
                return _taxPayer.Inspection;
            }
        }

        [DisplayName("Регион")]
        [Description("Регион налогоплательщика")]
        public string Region
        {
            get
            {
                return _taxPayer.Region;
            }
        }

        public bool? IsBuyer
        {
            get;
            set;
        }

        public TaxPayerModel TaxPayer
        {
            get
            {
                return _taxPayer;
            }
        }

        # endregion
    }
}
