﻿using System;

namespace Luxoft.NDS2.Client.UI.Navigator.Models
{
    public class DataNotLoadedException : Exception
    {
    }
}
