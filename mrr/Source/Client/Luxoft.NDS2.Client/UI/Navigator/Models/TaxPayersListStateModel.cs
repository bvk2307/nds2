﻿using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Navigator.Models
{
    public class TaxPayersListStateModel
    {
        private const string SingleSelected =
            "Выбран 1 налогоплательщик.";

        private const string MultipleSelectedPattern =
            "Выбрано {0} налогоплательщиков.";

        private const string TooFewSelectedMessagePattern =
            "{0} Для формирования отчёта выберите не менее {1} налогоплательщиков";

        private const string TooManySelectedMessagePattern =
            "{0} Для формирования отчёта выберите не более {1} налогоплательщиков";

        private const string TaxPayerListRequirementPattern =
            "Выберите не менее {0} и не более {1} налогоплательщиков для поиска цепочек между ними";

        private readonly int _minQuantity;

        private readonly int _maxQuantity;

        private readonly IEnumerable<ReportTaxPayerModel> _taxPayers;

        private readonly object _taxPayersLock;

        public TaxPayersListStateModel(int minQuantity, int maxQuantity, IEnumerable<ReportTaxPayerModel> taxPayers, object taxPayersLock)
        {
            _minQuantity = minQuantity;
            _maxQuantity = maxQuantity;
            _taxPayers = taxPayers;
            _quantity = _taxPayers.Count();
            _taxPayersLock = taxPayersLock;
        }

        private int _quantity;

        private void UpdateQuantity()
        {
            lock(_taxPayersLock)
            {
                _quantity = _taxPayers.Count();
            }
        }

        public bool IsValid
        {
            get
            {
                UpdateQuantity();
                return _quantity >= _minQuantity 
                    && _quantity <= _maxQuantity;
            }
        }

        public string Text
        {
            get
            {
                if (IsValid)
                {
                    return string.Empty;
                }

                return _quantity < _minQuantity
                    ? string.Format(TooFewSelectedMessagePattern, SelectedText(), _minQuantity)
                    : string.Format(TooManySelectedMessagePattern, SelectedText(), _maxQuantity);
            }
        }

        private string SelectedText()
        {
            return _quantity == 1
                ? SingleSelected
                : string.Format(MultipleSelectedPattern, _quantity);
        }

        public string Requirement
        {
            get
            {
                return string.Format(TaxPayerListRequirementPattern, _minQuantity, _maxQuantity);
            }
        }
    }
}
