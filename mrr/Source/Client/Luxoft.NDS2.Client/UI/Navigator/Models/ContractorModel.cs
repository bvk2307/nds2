﻿using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Common.Contracts.DTO.Navigator;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Navigator.Models
{
    public class ContractorModel
    {
        # region Конструктор

        public ContractorModel(IEnumerable<ChainContractorData> dataItems)
        {
            if (dataItems == null || !dataItems.Any())
            {
                throw new ArgumentException("dataItems");
            }

            var anyRow = dataItems.First();

            Inn = anyRow.Inn;
            Kpp = anyRow.Kpp;
            Name = anyRow.Name;
            _ndsCalculated = new CurrencyModel(anyRow.NdsCalculated);
            _ndsDeduction = new CurrencyModel(anyRow.NdsDeduction);
            _ndsPurchase = new CurrencyModel(anyRow.NdsPurchase);
            _ndsSales = new CurrencyModel(anyRow.NdsSales);

            _chains.AddRange(
                dataItems
                    .OrderBy(data => data.ChainNumber)
                    .Select(
                        data => 
                            new ContractorChainInfo(data.ChainId, data.ChainNumber.ToString())));
        }

        # endregion

        # region Состояние

        public event GenericEventHandler<ContractorModel> Activating;

        public void Activate()
        {
            if (Activating != null)
            {
                Activating(this);
            }
        }

        # endregion

        # region Идентификация НП

        public string Inn
        {
            get;
            private set;
        }

        public string Kpp
        {
            get;
            private set;
        }

        [DisplayName("Налогоплательщик")]
        [Description("Наименование налогоплательщика")]
        public string Name
        {
            get;
            private set;
        }

        # endregion

        # region Цепочки НП

        private const string ChainNumberSeparator = ",";

        private readonly List<ContractorChainInfo> _chains =
            new List<ContractorChainInfo>();

        [DisplayName("Количество участий в цепочках")]
        [Description("Количество включений НП в цепочки, найденные для указанной пары налогоплательщиков")]
        public int ChainsQuantity
        {
            get
            {
                return _chains.Count();
            }
        }

        [DisplayName("Номера цепочек")]
        [Description("Номера цепочек, в которые был включён НП, через запятую")]
        public string ChainsList
        {
            get
            {
                return string.Join(ChainNumberSeparator, _chains.Select(chain => chain.Title));
            }
        }

        public IEnumerable<ContractorChainInfo> GetChains()
        {
            return _chains.AsReadOnly();
        }        

        # endregion

        # region НДС

        private readonly CurrencyModel _ndsCalculated;

        private readonly CurrencyModel _ndsDeduction;

        private readonly CurrencyModel _ndsSales;

        private readonly CurrencyModel _ndsPurchase;

        [DisplayName("Исчислено НДС, согласно разделам 9 и 9.1 (или 12)")]
        [Description("Исчислено НДС, согласно разделам 9 и 9.1 (или 12)")]        
        public string NdsCalculated
        {
            get
            {
                return _ndsCalculated.ToString();
            }
        }

        [DisplayName("НДС к вычету, согласно разделам 8 и 8.1")]
        [Description("НДС к вычету, согласно разделам 8 и 8.1")]
        public string NdsDeduction
        {
            get
            {
                return _ndsDeduction.ToString();
            }
        }

        [DisplayName("НДС по выст. СФ, согласно разделу 10")]
        [Description("НДС по выст. СФ, согласно разделу 10")]
        public string NdsSales
        {
            get
            {
                return _ndsSales.ToString();
            }
        }

        [DisplayName("НДС по получ. СФ, согласно разделу 11")]
        [Description("НДС по получ. СФ, согласно разделу 11")]
        public string NsdPurchase
        {
            get
            {
                return _ndsPurchase.ToString();
            }
        }

        # endregion
    }

    public class ContractorChainInfo
    {
        public ContractorChainInfo(long id, string title)
        {
            Id = id;
            Title = title;
        }

        public long Id
        {
            get;
            private set;
        }

        public string Title
        {
            get;
            private set;
        }
    }
}
