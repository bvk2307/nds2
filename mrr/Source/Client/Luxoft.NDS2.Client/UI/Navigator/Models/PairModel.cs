﻿using Luxoft.NDS2.Common.Contracts.DTO.Navigator;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Navigator.Models
{
    public class PairModel
    {
        private const string TaxPayersTitleFormat = "{0} - {1}";

        private readonly List<ChainSummaryModel> _chains = new List<ChainSummaryModel>();

        private readonly MinMaxModel<ChainSummary> _targetTaxPayers;

        private readonly MinMaxModel<ChainSummary> _links;

        public PairModel(IEnumerable<ChainSummary> dataRows)
        {
            if (!dataRows.Any())
            {
                throw new ArgumentException("Пара должна содержать хотя бы 1 цепочку", "dto");
            }

            var firstRow = dataRows.First();
            Id = firstRow.PairId;
            TaxPayersTitle = 
                string.Format(
                    TaxPayersTitleFormat, 
                    firstRow.HeadTaxPayerName, 
                    firstRow.TailTaxPayerName);
            ChainsQuantity = dataRows.Count();
            _targetTaxPayers = new MinMaxModel<ChainSummary>(dataRows, (row) => row.TargetTaxPayersQuantity);
            _links = new MinMaxModel<ChainSummary>(dataRows, (row) => row.LinksQuantity);

            _chains.AddRange(dataRows.Select(row => new ChainSummaryModel(row)));
        }

        public long Id
        {
            get;
            private set;
        }

        [DisplayName("Пара налогоплательщиков")]
        [Description("Наименования первого и последнего ПН в цепочках")]
        public string TaxPayersTitle
        {
            get;
            private set;
        }

        [DisplayName("Количество цепочек")]
        [Description("Количество цепочек, найденных для данной пары налогоплательщиков")]
        public int ChainsQuantity
        {
            get;
            private set;
        }

        [DisplayName("Количество целевых НП в цепочке")]
        [Description("Минимальное и максимальное количество НП в цепочках данной пары из списка НП, выбранных для отчета")]
        public string TargetTaxPayersInfo
        {
            get
            {
                return _targetTaxPayers.ToString();
            }
        }

        [DisplayName("Количество промежуточных звеньев")]
        [Description("Минимальное и максимальное количество промежуточных звеньев в цепочках данной пары")]
        public string LinksQuantityInfo
        {
            get
            {
                return _links.ToString();
            }
        }

        public IList<ChainSummaryModel> Chains
        {
            get
            {
                return _chains;
            }
        }
    }
}
