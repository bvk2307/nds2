﻿using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Common.Contracts.DTO.Navigator;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Navigator.Models
{
    public class ContractorsListModel : ModelBase
    {
        private readonly List<ContractorModel> _contractors = new List<ContractorModel>();

        public ContractorsListModel(long pairId, long requestId)
        {
            PairId = pairId;
            RequestId = requestId;
        }

        public long PairId
        {
            get;
            private set;
        }

        public long RequestId
        {
            get;
            private set;
        }

        public IEnumerable<ContractorModel> Contractors
        {
            get
            {
                return _contractors.AsReadOnly();
            }
        }

        public ContractorModel ActiveContractor
        {
            get;
            private set;
        }

        public void SetContractors(IEnumerable<ChainContractorData> dataItems)
        {
            if (dataItems == null)
            {
                throw new ArgumentNullException("dataItems");
            }

            _contractors.Clear();
            _contractors.AddRange(
                dataItems
                    .GroupBy(data => data.Id)
                    .Select(
                        group => 
                            new ContractorModel(
                                dataItems.Where(data => data.Id == group.Key))));

            foreach (var contractor in _contractors)
            {
                contractor.Activating += (model) => ActiveContractor = model; 
            }

            if (ContractorsLoaded != null)
            {
                ContractorsLoaded();
            }
        }

        public void SetContractorsLoadError(string message)
        {
            RaiseError(message);
        }

        public event ParameterlessEventHandler ContractorsLoaded;
    }
}
