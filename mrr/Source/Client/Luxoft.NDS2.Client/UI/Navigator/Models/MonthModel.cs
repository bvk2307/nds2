﻿using Luxoft.NDS2.Client.UI.Base;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Navigator.Models
{
    public class MonthModel : IPeriod
    {
        # region Конструктор

        public MonthModel(Dictionary<int, string> allMonths, int value)
        {
            _allMonths = allMonths;
            _value = value;
        }

        # endregion

        # region Словарь месяцев

        private readonly Dictionary<int, string> _allMonths;

        # endregion

        # region Значение

        private int _value;

        public int Value
        {
            get
            {
                return _value;
            }
            set
            {
                if (_value != value)
                {
                    ValidateValue(value);

                    _value = value;
                    RaiseValueChanged();
                }
            }
        }

        public string Text
        {
            get
            {
                return _allMonths[_value];
            }

        }

        public override string ToString()
        {
            return Text;
        }

        # endregion

        # region Валидация значения

        private void ValidateValue(int value)
        {
            if (!_allMonths.ContainsKey(value))
            {
                throw new ArgumentException(
                    string.Format("{0} - Недопусимое значение месяца", value));
            }
        }

        # endregion

        # region Сравнение

        public PeriodCompareResult Compare(object objectToCompare)
        {
            var monthToCompare = objectToCompare as MonthModel;

            if (monthToCompare == null)
            {
                return PeriodCompareResult.Incomparable;
            }

            return Value.PeriodCompare(monthToCompare.Value);
        }

        # endregion

        # region События

        public ParameterlessEventHandler ValueChanged;

        private void RaiseValueChanged()
        {
            if (ValueChanged != null)
            {
                ValueChanged();
            }
        }

        # endregion
    }
}
