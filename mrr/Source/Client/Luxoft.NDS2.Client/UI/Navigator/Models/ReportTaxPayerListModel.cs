﻿using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using contracts = Luxoft.NDS2.Common.Contracts.DTO.Navigator;
using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading;
using Luxoft.NDS2.Client.UI.Controls.Sur;

namespace Luxoft.NDS2.Client.UI.Navigator.Models
{
    public class ReportTaxPayerListModel
    {
        # region Конструктор

        public ReportTaxPayerListModel(int minQuantity, int maxQuantity, DictionarySur surItems)
        {
            State = 
                new TaxPayersListStateModel(
                    minQuantity, 
                    maxQuantity, 
                    _reportTaxPayers, 
                    _selectedTaxPayersLock);
            SurItems = surItems;
        }

        # endregion

        # region Данные налогоплательщиков

        private object _selectedTaxPayersLock = new object();

        private readonly List<ReportTaxPayerModel> _reportTaxPayers = new List<ReportTaxPayerModel>();

        private readonly List<TaxPayerModel> _taxPayers = new List<TaxPayerModel>();

        private static object _lock = new object();

        public IEnumerable<ReportTaxPayerModel> Selected
        {
            get
            {
                return _reportTaxPayers.Where(x => x.Selected);
            }
        }

        public IEnumerable<ReportTaxPayerModel> Included
        {
            get
            {
                return _reportTaxPayers.AsReadOnly();
            }
        }

        public IEnumerable<TaxPayerModel> All
        {
            get
            {
                return _taxPayers.AsReadOnly();
            }
        }

        public uint TotalQuantity
        {
            get;
            set;
        }

        /// <summary>
        /// Обновляет список НП отчета на основе выбора из списка всех НП
        /// </summary>
        public void SubmitSelection()
        {
            Func<ReportTaxPayerModel, TaxPayerModel, bool> taxPayerEquality =
                (reportTp, tp) =>
                    tp.Inn == reportTp.Inn
                    && (string.IsNullOrWhiteSpace(reportTp.Kpp) || reportTp.Kpp == tp.Kpp);

            lock (_selectedTaxPayersLock)
            {
                var taxPayersToExclude =
                    _reportTaxPayers
                        .Where(
                            reportTp =>
                                !_taxPayers.Any(
                                    tp => tp.Selected && taxPayerEquality(reportTp, tp)))
                        .ToArray();

                var taxPayersToInclude =
                    _taxPayers
                        .Where(
                            tp =>
                                tp.Selected
                                && !_reportTaxPayers.Any(reportTp => taxPayerEquality(reportTp, tp)))
                        .ToArray();

                foreach (var taxPayer in taxPayersToExclude)
                {
                    _reportTaxPayers.Remove(taxPayer);
                }

                foreach (var taxPayer in taxPayersToInclude)
                {
                    var model = new ReportTaxPayerModel(taxPayer);
                    model.SelectionChanged += RaiseTaxPayerSelectedChanged;

                    _reportTaxPayers.Add(model);
                }

                if (taxPayersToExclude.Any() || taxPayersToInclude.Any())
                {
                    RaiseReportTaxPayersChanged();
                }

                if (taxPayersToExclude.Any())
                {
                    RaiseTaxPayerSelectedChanged();
                }
            }
        }

        /// <summary>
        /// Удаляет выбранных НП из параметров отчета
        /// </summary>
        public void RemoveSelected()
        {
            lock (_selectedTaxPayersLock)
            {
                if (_reportTaxPayers.All(tp => !tp.Selected))
                {
                    return;
                }

                foreach (var taxPayer in _reportTaxPayers.Where(tp => tp.Selected).ToArray())
                {
                    _reportTaxPayers.Remove(taxPayer);
                }
            }

            RaiseReportTaxPayersChanged();
            RaiseTaxPayerSelectedChanged();
        }

        public event GenericEventHandler<string> TaxPayersLoadError;

        /// <summary>
        /// Задает список НП - кандидатов на включение в отчет
        /// </summary>
        /// <param name="data"></param>
        public void SetTaxPayers(IEnumerable<contracts.TaxPayer> data)
        {
            lock (_lock)
            {
                _taxPayers.Clear();

                var included = new List<TaxPayerModel>();
                var others = new List<TaxPayerModel>();

                foreach(var dataItem in data)
                {
                    if (_reportTaxPayers.Any(
                        model => model.Inn == dataItem.Inn
                            && model.Kpp == dataItem.Kpp))
                    {
                        included.Add(new TaxPayerModel(dataItem, true));
                    }
                    else
                    {
                        others.Add(new TaxPayerModel(dataItem));
                    }
                }

                _taxPayers.AddRange(included.OrderBy(x => x.Inn));
                _taxPayers.AddRange(others);
            }

            RaiseTaxPayersLoaded();
        }

        public void SetTaxPayersLoadError(string reason)
        {
            lock (_lock)
            {
                _taxPayers.Clear();

                if (TaxPayersLoadError != null)
                {
                    TaxPayersLoadError(reason);
                }
            }

            RaiseTaxPayersLoaded();
        }

        # endregion

        # region Валидация

        public TaxPayersListStateModel State
        {
            get;
            private set;
        }

        # endregion

        # region Коды СУР

        public DictionarySur SurItems
        {
            get;
            private set;
        }

        # endregion

        # region Обновление признака "Возмещенец"

        public void ResetTaxPayerStatus()
        {
            if (_reportTaxPayers.Any(x => x.IsBuyer.HasValue))
            {
                lock (_selectedTaxPayersLock)
                {
                    foreach (var taxPayer in _reportTaxPayers)
                    {
                        taxPayer.IsBuyer = null;
                    }
                }

                if (TaxPayersBuyerStatusUpdated != null)
                {
                    TaxPayersBuyerStatusUpdated();
                }
            }
        }

        public void UpdateTaxPayerStatus(Dictionary<string, bool> statuses)
        {
            lock (_selectedTaxPayersLock)
            {
                foreach (var taxPayer in _reportTaxPayers)
                {
                    if (statuses.ContainsKey(taxPayer.Inn))
                    {
                        taxPayer.IsBuyer = statuses[taxPayer.Inn];
                    }
                }
            }

            if (TaxPayersBuyerStatusUpdated != null)
            {
                TaxPayersBuyerStatusUpdated();
            }
        }

        # endregion

        # region События изменения данных модели

        public event ParameterlessEventHandler SelectedQuantityChanged;

        public event ParameterlessEventHandler ReportTaxPayersChanged;

        public event ParameterlessEventHandler TaxPayersLoaded;

        public event ParameterlessEventHandler TaxPayersBuyerStatusUpdated;

        private void RaiseTaxPayerSelectedChanged()
        {
            if (SelectedQuantityChanged != null)
            {
                SelectedQuantityChanged();
            }
        }

        private void RaiseReportTaxPayersChanged()
        {
            if (ReportTaxPayersChanged != null)
            {
                ReportTaxPayersChanged();
            }
        }

        private void RaiseTaxPayersLoaded()
        {
            if (TaxPayersLoaded != null)
            {
                TaxPayersLoaded();
            }
        }

        # endregion
    }
}
