﻿namespace Luxoft.NDS2.Client.UI.Navigator.Models
{
    public class PeriodState
    {
        public PeriodState(bool valid, string text)
        {
            Valid = valid;
            Text = text;
        }

        public bool Valid
        {
            get;
            private set;
        }

        public string Text
        {
            get;
            private set;
        }
    }
}
