﻿using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Common.Contracts.DTO;

namespace Luxoft.NDS2.Client.UI.Navigator.Models
{
    public class RequetsStatusModel : ModelBase
    {
        private RequestStatus _status;

        public bool Loaded
        {
            get;
            private set;
        }

        public bool LoadError
        {
            get;
            private set;
        }

        public RequestStatus Status
        {
            get
            {
                if (!Loaded)
                {
                    throw new DataNotLoadedException();
                }

                return _status;
            }
            set
            {
                if (!Loaded || _status != value)
                {
                    _status = value;
                    Loaded = true;

                    if (StatusChanged != null)
                    {
                        StatusChanged();
                    }
                }
            }
        }

        public bool Completed
        {
            get
            {
                return Loaded && 
                    (_status == RequestStatus.Completed 
                        || _status == RequestStatus.Failed
                        || _status == RequestStatus.NotSuccessInNightMode);
            }
        }

        public bool Failed
        {
            get
            {
                return LoadError
                    || (Loaded && _status == RequestStatus.Failed);
            }
        }

        public void StatusLoadError(string message)
        {
            LoadError = true;            

            if (StatusChanged != null)
            {
                StatusChanged();
            }

            RaiseError(message);
        }


        public void StatusLoadNotSuccessInNightMode(string message)
        {
            if (StatusChanged != null)
            {
                StatusChanged();
            }

            RaiseNotification(message);
        }        
        
        public event ParameterlessEventHandler StatusChanged;

        public override string ToString()
        {
            if (!Loaded)
            {
                return "Получение статуса поиска пар НП. Пожалуйста подождите...";
            }

            if (LoadError)
            {
                return "При попытке получить статус подготовки данных возникла ошибка!";
            }

            switch (_status)
            {
                case RequestStatus.Failed: return "При поиске пар НП возникла ошибка!";
                case RequestStatus.InProcess: return "Система готовит данные о парах НП. Пожалуйста подождите...";
                case RequestStatus.Submited: return "Запрос на поиск пар НП отправлен. Пожалуйста подождите...";
                case RequestStatus.NotSuccessInNightMode: return ResourceManagerNDS2.SovNotSuccessInNightModeMessage;
            }

            return string.Empty;
        }
    }
}
