﻿namespace Luxoft.NDS2.Client.UI.Navigator
{
    public interface IReportSetupPresenter
    {
        Models.ReportSetupModel Model
        {
            get;
        }

        void Submit();

        void UpdateBuyerStatus();

        void ViewTaxPayerDetails();
    }
}
