﻿namespace Luxoft.NDS2.Client.UI.Navigator
{
    public interface IChainPresenter
    {
        Models.ChainModel Model
        {
            get;
        }

        void StartLoading();

        void OpenTaxPayerDetails();
    }
}
