﻿using System;
using System.Windows.Forms;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.UI;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Base.Ribbon;
using Microsoft.Practices.CompositeUI.Commands;
using Microsoft.Practices.ObjectBuilder;

namespace Luxoft.NDS2.Client.UI.Navigator
{
    public partial class ViewMaster<TView> : BaseView
    {
        # region Конструкторы

        private long? _idParam;

        private long _requestId;

        /// <summary>
        /// Создает экземпляр мастер представление (View) отчета Навигатор.
        /// Используется только для поддержки редактора форм.
        /// </summary>
        public ViewMaster()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Создает экземпляр мастер представление (View) отчета Навигатор.
        /// </summary>
        /// <param name="presentationContext">Ссылка на контекст представления КПИ</param>
        public ViewMaster(PresentationContext presentationContext)
            : base(presentationContext)
        {
            InitializeComponent();
        }

        public ViewMaster(PresentationContext presentationContext, long requestId, long? idParam)
            : this(presentationContext)
        {
            _idParam = idParam;
            _requestId = requestId;
        }

        # endregion

        # region Мастер - презентер

        private PresenterMaster<ViewMaster<TView>> _presenter;

        private IView _view;

        private IRibbonAdapter _ribbon;

        [CreateNew]
        public PresenterMaster<ViewMaster<TView>> Presenter
        {
            set
            {
                _presenter = value;
                _presenter.View = this;
                WorkItem = _presenter.WorkItem;

                InitRibbonMenu();

                var nestedControl = Resolve();
                _view = nestedControl as IView;

                _presentationContext.WindowTitle =
                    _view == null ? nestedControl.Text : _view.Title; 

                nestedControl.Margin = new Padding(0);
                nestedControl.Dock = DockStyle.Fill;
                Controls.Add(nestedControl);
            }
        }

        # endregion

        # region Создание представлений

        private Control Resolve()
        {
            if (typeof(TView) == typeof(IReportSetupView))
            {
                var presenter = _presenter.Resolve<IReportSetupPresenter>();

                return new Views.ReportSetupView(
                    presenter,
                    new Views.TaxPayersSelectView(
                        _presenter.Resolve<ITaxPayersSelectPresenter>(
                            presenter.Model)),
                    this);
            }

            if (typeof(TView) == typeof(IChainsListView))
            {
                return new Views.ChainsListView(
                    _presenter.Resolve<IChainsListPresenter>(new Models.ReportModel(_requestId)),
                    _ribbon,
                    this);
            }

            if (typeof(TView) == typeof(IChainContractorsListView))
            {
                return new Views.ChainContractorsListView(
                    _presenter.Resolve<IChainContractorsListPresenter>(
                        new Models.ContractorsListModel(_idParam.Value, _requestId)),
                        _ribbon,
                        this);
            }

            if (typeof(TView) == typeof(IChainView))
            {
                return new Views.ChainView(
                    _presenter.Resolve<IChainPresenter>(new Models.ChainModel(_idParam.Value, _requestId, _presenter.Sur)),
                    _ribbon,
                    this);
            }

            throw new NotSupportedException();
        }

        # endregion

        # region Создание меню

        private const string Functions = "Функции";

        private const string FunctionsToolTip =
            "Функции окна просмотра выявленных пар контрагентов";

        private const string RibbonName = "NDS2NavigatorPairsViewRibbon";

        private const string RibbonTitle = "Навигатор";

        private const string GroupName = "NDS2NavigatorPairsViewManage";

        private const string ButtonCaption = "Выгрузить";

        private const string ButtonToolTip = "Выгрузить найденные пары НП в MS Excel";

        private void InitRibbonMenu()
        {
            _ribbon =
                new RibbonCreator(_presentationContext, WorkItem)
                    .WithRibbon(
                        RibbonName,
                        RibbonTitle,
                        FunctionsToolTip);

            _ribbon
                .WithGroup(GroupName, Functions)
                    .WithButton(UcRibbonToolSize.Large, MenuCommands.ExportToExcel)
                        .WithText(ButtonCaption)
                        .WithToolTip(ButtonToolTip)
                        .WithResourse(ResourceManagerNDS2.NDS2ClientResources)
                        .WithImage("excel_create");

            _ribbon.Init();            
        }

        [CommandHandler("NDS2NavigatorExportToExcel")]
        public virtual void OnExportToExcel(object sender, EventArgs e)
        {
            if (_view != null)
            {
                try
                {
                    _view.ExportToExcel();
                }
                catch (NotSupportedException)
                {
                }
            }
        }

        # endregion
    }
}
