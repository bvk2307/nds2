﻿using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;
using Luxoft.NDS2.Common.Contracts.Services;
using Microsoft.Practices.CompositeUI;
using System;
using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf
{
    public interface IBaseMonitoringPresenter
    {
        BaseView _View { get; }

        Dictionary<string, List<TnoRatingInfo>> GetQuartersRatings(ChartData request);

        Dictionary<QuarterInfo, List<ReportCalculationInfo>> GetCalculationDates();

        List<TnoData> GetFederalCollection(CalculationData request);

        List<DateTime> GetDateList(QuarterInfo quarter);
    }
}
