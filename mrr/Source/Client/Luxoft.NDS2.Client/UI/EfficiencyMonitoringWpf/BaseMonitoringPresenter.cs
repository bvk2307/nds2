﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using CommonComponents.Directory;
using CommonComponents.Security.SecurityLogger;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Helpers;
using Luxoft.NDS2.Common.Contract.DTO.SearchEntity;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;
using Luxoft.NDS2.Common.Contracts.Services;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf
{
    public class BaseMonitoringPresenter<TV> : BasePresenter<TV>, IBaseMonitoringPresenter where TV : BaseView
    {

        protected IEfficiencyMonitoringService Service { get; private set; }
        public SecurityLogger SecurityLogger { get; private set; }

        public override void OnObjectCreated()
        {
            Service = base.GetServiceProxy<IEfficiencyMonitoringService>();
            SecurityLogger = new SecurityLogger(WorkItem.Services.Get<ISecurityLoggerService>(), WorkItem.Services.Get<IUserInfoService>(), base.GetServiceProxy<ISecurityService>());
        }

        public BaseView _View
        {
            get { return this.View; } 
        }


        public string GetSonoName(string code)
        {
            var lookupService = base.GetServiceProxy<IDictionaryDataService>();
            var enrty = lookupService.GetLookupData(new LookupRequest()
            {
                CodeFieldName = "s_code",
                FilterValue = code.PadRight(4, '0'),
                DataSourceKey = "v$sono",
            });
            if (enrty.Status == ResultStatus.Success)
            {
                var entryValue = enrty.Result.FirstOrDefault();
                if (entryValue != null)
                {
                    return entryValue.Value2;
                }
            }
            return null;
        }

        public virtual Dictionary<string, List<TnoRatingInfo>> GetQuartersRatings(ChartData request)
        {
            var ret = new Dictionary<string, List<TnoRatingInfo>>();
            
            ExecuteServiceCall(
                () => Service.GetAllQuartersRatings(request),
                result =>
                {
                    ret = result.Result;
                },
                resultErr => View.ShowError(resultErr.Message));

            return ret;
        }

        public Dictionary<QuarterInfo, List<ReportCalculationInfo>> GetCalculationDates()
        {
            var ret = new Dictionary<QuarterInfo, List<ReportCalculationInfo>>();

            ExecuteServiceCall(
                () => Service.GetQuarterCalculationDates(),
                result =>
                {
                    ret = result.Result;
                },
                resultErr => View.ShowError(resultErr.Message));

            return ret;
        }

        public List<TnoData> GetFederalCollection(CalculationData request)
        {
            var ret = new List<TnoData>();
            ExecuteServiceCall(
                () => Service.GetFederalCollection(request),
                result =>
                {
                    ret = result.Result;
                },
                resultErr => View.ShowError(resultErr.Message));

            return ret;
        }

        public virtual List<DateTime> GetDateList(QuarterInfo quarter)
        {
            return new List<DateTime>();
        }
    }
}
