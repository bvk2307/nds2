﻿using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Report
{
    public class YearReportItem: BaseViewModel
    {
        private ObservableCollection<QuarterReportItem> _quarters;
        private QuarterReportItem _selectedQuarter;

        public ObservableCollection<QuarterReportItem> Quarters
        {
            get { return _quarters; }
            set
            {
                if (_quarters != value)
                {
                    _quarters = value;
                    OnPropertyChanged("Quarters");
                    if (_quarters != null)
                    {
                        SelectedQuarter = _quarters.FirstOrDefault();
                    }
                }
            }
        }

        public int Year { get; set; }

        public QuarterReportItem SelectedQuarter
        {
            get { return _selectedQuarter; }
            set
            {
                _selectedQuarter = value;
                OnPropertyChanged("SelectedQuarter");
            }
        }

        public override string ToString()
        {
            return string.Format("{0} г.", Year);
        }
    }
}
