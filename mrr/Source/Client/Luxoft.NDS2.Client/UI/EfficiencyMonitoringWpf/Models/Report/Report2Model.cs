﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common.TnoMonitorData;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Report
{
    public class Report2Model
    {
        public string FileName { get; set; }
        
        public DateTime ReportDate { get; set; }

        public IEnumerable<TnoMonitorDataInfoModel> Data { get; set; }

        public ScopeEnum TypeEnum { get; set; }

        public bool Weekly { get; set; }

        public QuarterInfo QuarterInfo { get; set; }

    }
}
