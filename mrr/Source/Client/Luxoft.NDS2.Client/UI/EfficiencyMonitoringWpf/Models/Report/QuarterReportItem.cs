﻿using System;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Report
{
    public class QuarterReportItem
    {
        public DateTime Date { get; set; }

        public DateTime ReportDate { get; set; }

        public int Quarter { get; set; }

        public int Year { get; set; }

        public override string ToString()
        {
            return ReportDate.ToString("dd.MM");
        }
    }
}
