﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using System.Windows.Threading;
using CommonComponents.Utils;
using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.UI.Base.Wpf.Model;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Chart;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Controls;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Helpers;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common.TnoMonitorData;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Report;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Reports;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;
using Microsoft.Win32;
using Telerik.Windows.Controls.GridView;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.CentralApparat
{
    public class CentralApparatViewModel : BaseMainViewModel<CentralApparatPresenter, CentralApparatView>, IChartDataProvider, IClerableModel
    {
        private readonly ChartDataController _chartController;
        
        public CentralApparatViewModel()
        {
            _chartController = new ChartDataController(this);
        }
        

        protected override void OnInternalPropertyChanged(string name)
        {
            if (name == "SelectedRatingData")
            {
                if (SelectedRatingData != null)
                {
                    ChartModel = CreateChartModel();
                    HeaderString = string.Format("Основные показатели камеральной налоговой проверки - {0}",
                        SelectedRatingData.TnoCode == "0" || SelectedRatingData.TnoCode == "9900" ? "РФ" : SelectedRatingData.TnoName);
                }
                else
                {
                    ChartModel = null;
                }
            }
        }

        protected override List<TnoData> ReadCalculatedData()
        {
            return Presenter.GetFederalCollection(new CalculationData()
            {
                SelectedTnoCode = State.SelectedDistrict.Id,
                Quarter = State.SelectedQuarter.Quarter,
                CalculationDate = State.SelectedQuarterDate,
                DetailLevel = ModelHelper.ConvertTnoCodeToScope(State.SelectedDistrict.Id)

            });
        }

        protected override void InitCalculatedData(List<TnoData> data)
        {
            if (data.Any())
            {
                var list =
                    data.Select(
                        i => new TnoMonitorDataInfoModel(i, State.SelectedRating.Id, State.SelectedScope)).ToList();

                FixedRatingData = list.FirstOrDefault(p => p.TnoCode == State.SelectedDistrict.Id);
                RatingData = new ObservableCollection<TnoMonitorDataInfoModel>(list);
                SelectedRatingData = FixedRatingData;
            }
            else
            {
                FixedRatingData = new TnoMonitorDataInfoModel(new TnoData { TNO_CODE = State.SelectedDistrict.Id, TNO_NAME = State.SelectedDistrict.Name }, State.SelectedRating.Id, State.SelectedScope);
                RatingData = new ObservableCollection<TnoMonitorDataInfoModel>();
                SelectedRatingData = FixedRatingData;
            }
        }

        private BaseChartDataModel CreateChartModel()
        {
            string tnoCode = SelectedRatingData.TnoCode;
            TnoData parent = null;
            ScopeEnum scope;
            ScopeEnum parentScope;

            if (ModelHelper.IsRf(State.SelectedDistrict.Id))
            {
                parent = FixedRatingData.Dto;
                parentScope = ScopeEnum.RF;
                scope = ScopeEnum.UFNS;

            }
            else if (ModelHelper.IsMiKn(State.SelectedDistrict.Id) || ModelHelper.IsRf(State.SelectedDistrict.Id))
            {
                parent = FixedRatingData.Dto;
                parentScope = ScopeEnum.RF;
                scope = ScopeEnum.UFNS;
            }
            else
            {
                parentScope = ScopeEnum.RF;
                parent = new TnoData() { TNO_CODE = "0", TNO_NAME = "РФ" };
                scope = FixedRatingData.TnoCode.Equals(tnoCode)
                    ? ScopeEnum.DISTRICT : ScopeEnum.UFNS;
            }

            return _chartController.CreateChartModel(
                new ChartWebDataRequest()
                {
                    Quarter = State.SelectedQuarter.Quarter,
                    IndexValue = State.SelectedRating.Id,
                    CalculationDate = State.SelectedQuarterDate,
                    Context = ContextType.Central,
                    TnoModel = SelectedRatingData.Dto,
                    Scope = scope,
                    ParentTnoModel = parent,
                    ParentScope = parentScope
                });

        }

        Dictionary<string, List<TnoRatingInfo>> IChartDataProvider.GetQuartersRatings(ChartData request)
        {
            return Presenter.GetQuartersRatings(request);
        }

        List<QuarterInfo> IChartDataProvider.GetQuarters()
        {
            return this.Years.SelectMany(s => s.Quarters.Select(q => q.Quarter)).ToList();
        }

        public void CreateUfnsRatingView()
        {
            if (this.SelectedRatingData.TnoCode == this.FixedRatingData.TnoCode)
                return;

            if (this.SelectedRatingData.TnoCode.EndsWith("00"))
            {
                Dispatcher.CurrentDispatcher.Invoke(
                    new Action(
                        () =>
                            Presenter.CreateUfnsRatingView(
                                new InitStateModel
                                {
                                    TnoCode = this.SelectedRatingData == null ? null : this.SelectedRatingData.TnoCode,
                                    TnoName = this.SelectedRatingData == null ? null : this.SelectedRatingData.TnoName,
                                    Year = this.State.SelectedQuarter.Quarter.Year,
                                    Quarter = this.State.SelectedQuarter.Quarter.Quarter,
                                    Date = this.State.SelectedQuarterDate
                                })));
            }
            else
            {
                Dispatcher.CurrentDispatcher.Invoke(
                    new Action(
                        () =>
                            Presenter.CreateInspectionView(
                                new InitStateModel
                                {
                                    TnoCode = this.SelectedRatingData.TnoCode,
                                    TnoName = this.SelectedRatingData.TnoName,
                                    Year = this.State.SelectedQuarter.Quarter.Year,
                                    Quarter = this.State.SelectedQuarter.Quarter.Quarter,
                                    Date = this.State.SelectedQuarterDate
                                })));
            }
        }

        public void CreateInspectionRatingView()
        {
            Dispatcher.CurrentDispatcher.Invoke(
                new Action(
                    () =>
                        Presenter.CreateInspectionRatingView(
                            new InitStateModel
                            {
                                TnoCode = this.SelectedRatingData == null ? null : this.SelectedRatingData.TnoCode,
                                TnoName = this.SelectedRatingData == null ? null : this.SelectedRatingData.TnoName,
                                Year = this.State.SelectedQuarter.Quarter.Year,
                                Quarter = this.State.SelectedQuarter.Quarter.Quarter,
                                Date = this.State.SelectedQuarterDate
                            })));
        }

        public void CreateReport2M()
        {
            var date = SelectDateWindow.SelectDate(this.Years);
            if (date == null)
                return;

            string fileName = String.Format("{0}_{1}_{2}_{3}.xls",
                DateTime.Now.ToString("yyyyMMdd_HH-mm"),
                "Отчет 2-МЭ",
                date.ReportDate.ToString("dd-MM-yyyy"),
                "0000"
                );

            if (DialogHelper.GetFileToSave(SelectFileType.XLS, ref fileName))
            {
                var scopeDataList = Presenter.GetFederalCollection(new CalculationData()
                {
                    SelectedTnoCode = State.SelectedDistrict.Id,
                    Quarter = new QuarterInfo {Year = date.Year, Quarter = date.Quarter},
                    CalculationDate = date.Date,
                    DetailLevel = ScopeEnum.RF_UFNS

                });

                var scopeDataListMiKn = Presenter.GetFederalCollection(new CalculationData()
                {
                    SelectedTnoCode = State.SelectedDistrict.Id,
                    Quarter = new QuarterInfo {Year = date.Year, Quarter = date.Quarter},
                    CalculationDate = date.Date,
                    DetailLevel = ScopeEnum.MIKN
                });

                scopeDataList.AddRange(scopeDataListMiKn.Where(i => i.TNO_CODE != "9900"));

                var list =
                    scopeDataList.Select(
                        i => new TnoMonitorDataInfoModel(i, IndexEnum.DiscrepancyShareInDeduction, State.SelectedScope))
                        .ToList();

                new Report2M().Create(new Report2Model
                {
                    FileName = fileName,
                    ReportDate = date.ReportDate,
                    Data = list,
                    TypeEnum = ScopeEnum.RF
                });

                Presenter.SecurityLogger.ExportReportFormat2(date.ReportDate, "0000");
                if (DialogHelper.ConfirmWithTitle("Отчет 2-МЭ", "Файл «{0}» сформирован. Открыть файл?", fileName))
                {
                    System.Diagnostics.Process.Start(fileName);
                }
            }
        }

        public void CreateReportEd()
        {
            string fileName = String.Format("{0}_{1}_{2}_{3}.xls",
                DateTime.Now.ToString("yyyyMMdd_HH-mm"),
                "ОТЧЕТ_ЕЖЕНЕДЕЛЬНЫЙ",
                this.State.SelectedQuarterDate.ToString("dd-MM-yyyy"),
                "0000"
                );

            if (DialogHelper.GetFileToSave(SelectFileType.XLS, ref fileName))
            {
                var scopeDataList = Presenter.GetFederalCollection(new CalculationData()
                {
                    SelectedTnoCode = State.SelectedDistrict.Id,
                    Quarter = new QuarterInfo { Year = this.State.SelectedQuarter.Quarter.Year, Quarter = this.State.SelectedQuarter.Quarter.Quarter },
                    CalculationDate = this.State.SelectedQuarterDate,
                    DetailLevel = ScopeEnum.RF_UFNS

                });

                var scopeDataListMiKn = Presenter.GetFederalCollection(new CalculationData()
                {
                    SelectedTnoCode = State.SelectedDistrict.Id,
                    Quarter = new QuarterInfo { Year = this.State.SelectedQuarter.Quarter.Year, Quarter = this.State.SelectedQuarter.Quarter.Quarter },
                    CalculationDate = this.State.SelectedQuarterDate,
                    DetailLevel = ScopeEnum.MIKN
                });

                scopeDataList.AddRange(scopeDataListMiKn.Where(i => i.TNO_CODE != "9900"));

                var list =
                    scopeDataList.Select(
                        i => new TnoMonitorDataInfoModel(i, IndexEnum.DiscrepancyShareInDeduction, State.SelectedScope)).ToList();

                new Report2M().Create(new Report2Model
                {
                    FileName = fileName,
                    ReportDate = State.SelectedQuarterDate,
                    Weekly = true,
                    QuarterInfo = State.SelectedQuarter.Quarter,
                    Data = list,
                    TypeEnum = ScopeEnum.RF
                });

                Presenter.SecurityLogger.ExportReportDaily(State.SelectedQuarterDate, this.State.SelectedQuarter.Quarter, "0000");

                if (DialogHelper.ConfirmWithTitle("Отчет еженедельный", "Файл «{0}» сформирован. Открыть файл?", fileName))
                {
                    System.Diagnostics.Process.Start(fileName);
                }
            }
        }


        public void RestoreState(GridViewDataControl gridView)
        {
            gridView.SortDescriptors.Clear();
            var csd = new ColumnSortDescriptor
            {
                Column = gridView.Columns["CurrentEffIndexRatingValueView"],
                SortDirection = ListSortDirection.Ascending
            };
            gridView.SortDescriptors.Add(csd);
        }

        public bool CanRestoreState(GridViewDataControl gridView)
        {
            return !gridView.IsSorted("CurrentEffIndexRatingValueView", ListSortDirection.Ascending);
        }
    }
}
