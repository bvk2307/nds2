﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Threading;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Chart;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Helpers;
using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common.TnoMonitorData;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Inspection
{
    public class InspectionViewModel : BaseMainViewModel<InspectionPresenter, InspectionView>, IChartDataProvider
    {
        public InspectionViewModel()
            : base()
        {
            _chartController = new ChartDataController(this);
        }

        private readonly ChartDataController _chartController;
        private TnoMonitorDataInfoModel _parentRatingData;


        public TnoMonitorDataInfoModel ParentRatingData
        {
            get { return _parentRatingData; }
            set
            {
                _parentRatingData = value;
                OnPropertyChanged("ParentRatingData");
            }
        }


        protected override void OnInternalPropertyChanged(string name)
        {
            if (name == "SelectedRatingData")
            {
                if (SelectedRatingData != null)
                {
                    ChartModel = CreateChartModel();
                    if (FixedRatingData == SelectedRatingData)
                    {
                        HeaderString = FixedRatingData.TnoName;
                    }
                    else
                    {
                        HeaderString = string.Format("{0} - {1}", FixedRatingData.TnoName, SelectedRatingData.TnoName);
                    }
                }
                else
                {
                    ChartModel = null;
                }
            }
        }

        protected override List<TnoData> ReadCalculatedData()
        {
            return Presenter.GetFederalCollection(new CalculationData
            {
                SelectedTnoCode = State.SelectedDistrict.Id,
                Quarter = State.SelectedQuarter.Quarter,
                CalculationDate = State.SelectedQuarterDate,
                DetailLevel = ScopeEnum.IFNS
            });
        }

        protected override void InitCalculatedData(List<TnoData> data)
        {
            if (data.Any())
            {
                var parentIdentifier = GetParentTnoIdentifier(State.SelectedDistrict.Id);

                var parentScopeDataList = Presenter.GetFederalCollection(new CalculationData()
                {
                    SelectedTnoCode = parentIdentifier,
                    Quarter = State.SelectedQuarter.Quarter,
                    CalculationDate = State.SelectedQuarterDate,
                    DetailLevel = ModelHelper.IsMiKn(parentIdentifier) ? ScopeEnum.MIKN : ScopeEnum.UFNS
                });

                var list =
                    parentScopeDataList.Select(
                        i => new TnoMonitorDataInfoModel(i, State.SelectedRating.Id, State.SelectedScope)).ToList();

                ParentRatingData = new TnoMonitorDataInfoModel(parentScopeDataList.FirstOrDefault(
                        t => t.TNO_CODE.Equals(parentIdentifier)), State.SelectedRating.Id, State.SelectedScope);

                FixedRatingData = list.FirstOrDefault(p => p.TnoCode == State.SelectedDistrict.Id);
                RatingData = new ObservableCollection<TnoMonitorDataInfoModel>(list);
                SelectedRatingData = FixedRatingData;
            }
            else
            {
                ParentRatingData = new TnoMonitorDataInfoModel(new TnoData(), State.SelectedRating.Id, State.SelectedScope);
                FixedRatingData = new TnoMonitorDataInfoModel(new TnoData(), State.SelectedRating.Id, State.SelectedScope);
                RatingData = new ObservableCollection<TnoMonitorDataInfoModel>();
                SelectedRatingData = FixedRatingData;
            }
        }

        private string GetParentTnoIdentifier(string tnoCode)
        {
            if (tnoCode.Equals("9901")) return "5000";

            return tnoCode.Substring(0, 2) + "00";
        }

        private BaseChartDataModel CreateChartModel()
        {
            if (string.IsNullOrEmpty(SelectedRatingData.TnoCode))
            {
                return null;
            }
            ScopeEnum parentScope;
            if (ModelHelper.IsMiKn(SelectedRatingData.TnoCode.Substring(0, 2)))
            {
                parentScope = ScopeEnum.RF;
            }
            else
            {
                parentScope = ScopeEnum.UFNS;
            }
            return _chartController.CreateChartModel(
                new ChartWebDataRequest()
                {
                    Quarter = State.SelectedQuarter.Quarter,
                    IndexValue = State.SelectedRating.Id,
                    CalculationDate = State.SelectedQuarterDate,
                    Context = ModelHelper.IsMiKn(SelectedRatingData.TnoCode.Substring(0, 2)) ? ContextType.MI_KN : ContextType.IFNS,
                    TnoModel = SelectedRatingData.Dto,
                    Scope = ScopeEnum.IFNS,
                    ParentTnoModel = ParentRatingData.Dto,
                    ParentScope = parentScope
                });
        }

        Dictionary<string, List<TnoRatingInfo>> IChartDataProvider.GetQuartersRatings(ChartData request)
        {
            return Presenter.GetQuartersRatings(request);
        }

        List<QuarterInfo> IChartDataProvider.GetQuarters()
        {
            return this.Years.SelectMany(s => s.Quarters.Select(q => q.Quarter)).ToList();
        }

        public void CreateUnprovenDiffDetailsView()
        {
            if (this.SelectedRatingData == null)
                return;
            Dispatcher.CurrentDispatcher.Invoke(
                new Action(
                    () =>
                        Presenter.CreateUnprovenDiffDetailsView(
                            new InitStateModel
                            {
                                TnoCode = this.SelectedRatingData == null ? null : this.SelectedRatingData.TnoCode,
                                TnoName = this.SelectedRatingData == null ? null : this.SelectedRatingData.TnoName,
                                Year = this.State.SelectedQuarter.Quarter.Year,
                                Quarter = this.State.SelectedQuarter.Quarter.Quarter,
                                Date = this.State.SelectedQuarterDate,
                                DimensionType = this.State.Dimension.DimensionType
                            })));
        }

        public void CreateInspectionRatingView()
        {
            Dispatcher.CurrentDispatcher.Invoke(
                new Action(
                    () =>
                        Presenter.CreateInspectionRatingView(
                            new InitStateModel
                            {
                                TnoCode = this.SelectedRatingData == null ? null : this.SelectedRatingData.TnoCode,
                                TnoName = this.SelectedRatingData == null ? null : this.SelectedRatingData.TnoName,
                                Year = this.State.SelectedQuarter.Quarter.Year,
                                Quarter = this.State.SelectedQuarter.Quarter.Quarter,
                                Date = this.State.SelectedQuarterDate
                            })));
        }
    }
}
