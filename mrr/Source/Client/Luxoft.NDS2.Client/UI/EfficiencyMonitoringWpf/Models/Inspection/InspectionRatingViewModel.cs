﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using CommonComponents.Utils;
using CommonComponents.Utils.Async;
using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.UI.Base.Wpf.Model;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Chart;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Commands;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Helpers;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.CentralApparat;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Reports;
using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common.TnoMonitorData;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Microsoft.Win32;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Controls.GridView.SearchPanel;
using Telerik.Windows.Data;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Inspection
{
    public class InspectionRatingViewModel : BaseViewModel, IClerableModel
    {

        public InspectionRatingViewModel()
        {
            State = new StateModel();
            State.PropertyChanged += State_PropertyChanged;
            HeaderString = "Удельный вес суммы расхождений по декларациям по НДС в общей сумме налоговых вычетов по НДС";
            _exportToExcelCommand = new DelegateImageExcelCommand(ExportToExcel, CanExportToExcel);
        }

        private InspectionRatingPresenter _presenter;
        private ObservableCollection<IfnsRatingByDiscrepancyShareInDeductionData> _ratingData = new ObservableCollection<IfnsRatingByDiscrepancyShareInDeductionData>();
        private BaseChartDataModel _chartModel;
        private string _headerString;
        private readonly DelegateImageExcelCommand _exportToExcelCommand;
        private bool _exportToExcelCommandExecuting;
        private string _exportToExcelFileName;
        private IfnsRatingByDiscrepancyShareInDeductionData _selectedRatingData;
        private ObservableCollection<YearModel> _years;
        private bool _isBusy = true;

        public ObservableCollection<District> Districts { get; set; }

        public string HeaderString
        {
            get { return _headerString; }
            set
            {
                _headerString = value;
                OnPropertyChanged("HeaderString");
            }
        }

        public bool ExportToExcelCommandExecuting
        {
            get { return _exportToExcelCommandExecuting; }
            set
            {
                if (this._exportToExcelCommandExecuting != value)
                {
                    this._exportToExcelCommandExecuting = value;
                    OnPropertyChanged("ExportToExcelCommandExecuting");
                }
            }
        }

        public bool IsBusy
        {
            get { return this._isBusy; }
            set
            {
                if (this._isBusy != value)
                {
                    this._isBusy = value;
                    OnPropertyChanged("IsBusy");
                }
            }
        }

        /// <summary>
        /// Биндится из шаблона
        /// </summary>
        public DelegateCommand ExportToExcelCommand
        {
            get { return _exportToExcelCommand; }
        }


        public InspectionRatingPresenter Presenter
        {
            get { return _presenter; }
            set
            {
                _presenter = value;
                if (_presenter != null)
                {
                    InitCalculatedData();
                }
            }
        }

        public IfnsRatingByDiscrepancyShareInDeductionData SelectedRatingData
        {
            get { return _selectedRatingData; }
            set
            {
                if (_selectedRatingData != value)
                {
                    OnPropertyChanged("SelectedRatingData");
                }
            }
        }

        /// <summary>
        /// Список доступных кварталов
        /// </summary>      
        public ObservableCollection<YearModel> Years
        {
            get { return _years; }
            set
            {
                if (_years != null)
                {
                    _years.Clear();
                    OnPropertyChanged("Years");
                }
                _years = value;
                OnPropertyChanged("Years");
            }
        }

        public ObservableCollection<IfnsRatingByDiscrepancyShareInDeductionData> RatingData
        {
            get { return _ratingData; }
            set
            {
                if (_ratingData != null)
                {
                    _ratingData.Clear();
                    OnPropertyChanged("RatingData");
                }
                _ratingData = value;
                OnPropertyChanged("RatingData");
            }
        }

        /// <summary>
        /// Список рейтингов
        /// </summary>
        public ObservableCollection<Rating> Ratings { get; set; }

        public StateModel State { get; set; }

        void State_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            // Потому что дата поменятеся тоже
            if (e.PropertyName == "SelectedQuarter" || e.PropertyName == "SelectedYear")
                return;

            if (e.PropertyName == "SelectedRating")
            {
                if (RatingData != null)
                {
                    var newList = RatingData.Select(s => s).ToList();
                    var newRating = new ObservableCollection<IfnsRatingByDiscrepancyShareInDeductionData>(newList);
                    RatingData = newRating;
                }
            }
            else
            {
                InitCalculatedData();
            }
        }

        public void InitCalculatedData()
        {
            if (Presenter == null)
                return;
            IsBusy = true;

            var asyncWorker = new AsyncWorker<List<IfnsRatingByDiscrepancyShareInDeductionData>>();
            asyncWorker.DoWork +=
                (sender, args) =>
                {
                    args.Result = Presenter.GetIfnsRatingsByDiscrepancyShareInDeduction(new CalculationData
                    {
                        SelectedTnoCode = State.SelectedDistrict.Id,
                        Quarter = State.SelectedQuarter.Quarter,
                        CalculationDate = State.SelectedQuarterDate,
                    });
                };

            asyncWorker.Complete += (sender, args) =>
                {
                    RatingData = new ObservableCollection<IfnsRatingByDiscrepancyShareInDeductionData>(args.Result);
                    IsBusy = false;
                };

            asyncWorker.Start();
        }


        /// <summary>
        /// Ссылка на грид для выгрузки в эксель
        /// </summary>
        public RadGridView GridView { get; set; }

        

        private void ExportToExcel(object parameter)
        {
            if (ExportToExcelCommandExecuting)
            {
                if (DialogHelper.ConfirmWithTitle("Экспорт в MS Excel", "Прекратить выгрузку записей рейтинга ИФНС?"))
                {
                    ExportToExcelCommandExecuting = false;
                }
            }
            else
            {
                string fileName = String.Format("{0}_{1}_{2}.xls",
                    DateTime.Now.ToString("yyyyMMdd_HH-mm"),
                    "ОТЧЕТ_РЕЙТИНГИ_ИФНС",
                    State.SelectedQuarterDate.ToString("dd-MM-yyyy")
                );
                _exportToExcelFileName = fileName;
                if (DialogHelper.GetFileToSave(SelectFileType.XLS, ref fileName))
                {

                    var exportToExcelWorker = new AsyncWorker<object>();
                    exportToExcelWorker.DoWork +=
                        (sender, args) =>
                        {
                            ExportToExcelCommandExecuting = true;
                            var data = GridView.Items.OfType<IfnsRatingByDiscrepancyShareInDeductionData>().ToList();
                            if (ExportToExcelCommandExecuting)
                            {
                                new ReportIfnsMe().Create(fileName, State.SelectedQuarter.Quarter,
                                    State.Dimension.DimensionType, State.SelectedQuarterDate, data);
                            }
                            if (ExportToExcelCommandExecuting)
                            {
                                Presenter.SecurityLogger.ExportReportIfnsRating(State.SelectedQuarterDate,
                                    State.SelectedQuarter.Quarter);
                            }
                        };

                    exportToExcelWorker.Complete += (sender, args) =>
                    {
                        if (ExportToExcelCommandExecuting)
                        {
                            if (DialogHelper.ConfirmWithTitle("Экспорт в MS Excel", "Файл «{0}» сформирован. Открыть файл?", fileName))
                            {
                                System.Diagnostics.Process.Start(fileName);
                            }
                        }
                        ExportToExcelCommandExecuting = false;
                    };
                    exportToExcelWorker.Failed += (sender, args) =>
                    {
                        ExportToExcelCommandExecuting = false;
                        DialogHelper.Error("Произошел сбой при формировании файла «{0}». Повторите операцию снова.»", fileName);
                    };
                    exportToExcelWorker.Start();
                }
            }
        }

       

        private bool CanExportToExcel(object parameter)
        {
            var grid = parameter as RadGridView;
            if (grid == null)
                return false;

            return grid.Items.Count > 0;
        }

        public void ControlClosed()
        {
            if (ExportToExcelCommandExecuting)
            {
                if (DialogHelper.ConfirmWithTitle("Экспорт в MS Excel",
                    "Файл «{0}» не сформирован. Прервать процесс формирования?", _exportToExcelFileName))
                {
                    ExportToExcelCommandExecuting = false;
                }
            }
        }

        public void RestoreState(GridViewDataControl gridView)
        {
            gridView.SortDescriptors.Clear();
            var csd = new ColumnSortDescriptor()
            {
                Column = gridView.Columns["ShareValue"],
                SortDirection = ListSortDirection.Descending
            };
            gridView.SortDescriptors.Add(csd);
            ExportToExcelCommand.InvalidateCanExecute();
        }

        public bool CanRestoreState(GridViewDataControl gridView)
        {
            return !gridView.IsSorted("ShareValue", ListSortDirection.Descending);
        }
    }
}
