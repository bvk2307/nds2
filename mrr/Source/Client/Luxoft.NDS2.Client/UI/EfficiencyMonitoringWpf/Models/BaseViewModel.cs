﻿using System.ComponentModel;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models
{
    public class BaseViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string name)
        {
            OnInternalPropertyChanged(name);
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }

        protected virtual void OnInternalPropertyChanged(string name)
        {
        }
    }
}
