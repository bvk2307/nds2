﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common
{
    public class District
    {
        [DataMember(Name = "id")]
        public string Id { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        public static District Create(string id, string name)
        {
            return new District {Id = id, Name = name};
        }

        public static ObservableCollection<District> GetAllDistricts()
        {
            return new ObservableCollection<District>
            {
                Create("0", "УФНС России по субъектам РФ"),
                Create("36", "Дальневосточный ФО"),
                Create("33", "Приволжский ФО"),
                Create("31", "Северо-Западный ФО"),
                Create("38", "Северо-Кавказский ФО"),
                Create("35", "Сибирский ФО"),
                Create("34", "Уральский ФО"),
                Create("30", "Центральный ФО"),
                Create("37", "Южный ФО"),
                Create("9900", "МИ по КН"),
            };
        }
    }
}
