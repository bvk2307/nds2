﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common
{
    [DataContract]
    public class TableDataWithDatesBase<T>
    {
        public TableDataWithDatesBase()
        {
            Dates = new DatesCollection();
            TableData = new List<T>();
        }

        [DataMember(Name = "dates")]
        public DatesCollection Dates { get; set; }

        [DataMember(Name = "tableData")]
        public IList<T> TableData { get; set; }
    }
}
