﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Microsoft.Practices.CompositeUI.Utility;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common
{
    public class YearModel : BaseViewModel
    {
        private ObservableCollection<QuarterModel> _quarters;
        public int  Year { get; set; }

        /// <summary>
        /// Список доступных кварталов
        /// </summary>
        public ObservableCollection<QuarterModel> Quarters
        {
            get { return _quarters; }
            set
            {
                _quarters = value;
                OnPropertyChanged("Quarters");
                if (_quarters != null)
                {
                    foreach (var quarterModel in _quarters)
                    {
                        quarterModel.FillProperties();
                    }
                }
            }
        }

        public override string ToString()
        {
            return string.Format("{0} г.", Year);
        }
    }
}
