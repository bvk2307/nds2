﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common
{
    public class BaseChartDataModel: BaseViewModel
    {
        private bool _showBarSet = true;
        private bool _showLineSet = true;
        public GraphDataSetModel BarSet { get; set; }

        public GraphDataSetModel LineSet { get; set; }

        public bool ShowBarSet
        {
            get { return _showBarSet; }
            set
            {
                _showBarSet = value;
                OnPropertyChanged("ShowBarSet");
            }
        }

        public bool ShowLineSet
        {
            get { return _showLineSet; }
            set
            {
                _showLineSet = value;
                OnPropertyChanged("ShowLineSet");
            }
        }

        public string Code { get; set; }

        public string Title { get; set; }

        public decimal Maximum { get; set; }

        /// <summary>
        /// Количество недель для расчета графика
        /// </summary>
        protected const int MaxWeek = 14;


    }
}
