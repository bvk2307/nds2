﻿using System.Runtime.Serialization;
using System.Windows.Media;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common
{
    public class GraphDataValueModel: BaseViewModel
    {
        private QuarterInfo _quarter;

        public string TnoName { get; set; }

        public decimal Value { get; set; }

		public Color Color { get; set; }

        public string Label { get; set; }

        public QuarterInfo Quarter
        {
            get { return _quarter; }
            set
            {
                _quarter = value;
                if (_quarter != null)
                {
                    Label = _quarter.ToString();
                }
            }
        }

        
    }
}
