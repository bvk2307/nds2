﻿using System;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common
{
    /// <summary>
    /// Представляет информацию о параметрах расчета данных для отчета Мониторинг Эффективности
    /// </summary>
    public class CalculationDateModel
    {
        public CalculationDateModel(DateTime date, bool isBasedOnEfficiencyAgregate)
        {
            Date = date;
            IsBasedOnEfficiencyAgregate = isBasedOnEfficiencyAgregate;
        }

        /// <summary>
        /// Дата расчета данных в БД (агрегат Мониторинга эффективности или Агрегат Фоновых показателей)
        /// </summary>
        public DateTime Date { get; private set; }

        /// <summary>
        /// Признак выполнения расчета на основе агрегата Мониторинга эффективности
        /// </summary>
        public bool IsBasedOnEfficiencyAgregate { get; private set; }
    }
}
