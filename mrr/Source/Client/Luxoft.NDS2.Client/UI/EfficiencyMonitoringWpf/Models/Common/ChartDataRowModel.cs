﻿using System.Runtime.Serialization;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common
{
    [DataContract]
    public class ChartDataRowModel
    {
        [DataMember(Name = "label")]
        public string Label { get; set; }
        [DataMember(Name = "value")]
        public decimal Value { get; set; }
    }
}
