﻿using System;
using System.Linq;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Helpers;
using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common
{
    public class StateModel : BaseViewModel
    {
        private QuarterModel _selectedQuarter;
        private DateTime _selectedQuarterDate;
        private District _selectedDistrict;
        private Rating _selectedRating;
        private YearModel _selectedYear;

        private DimensionModel _dimension = new DimensionModel();

        public DimensionModel Dimension
        {
            get { return _dimension; }
            set
            {
                _dimension = value;
                OnPropertyChanged("Dimension");
            }
        }

        public YearModel SelectedYear
        {
            get { return _selectedYear; }
            set
            {
                if (_selectedYear != value)
                {
                    var rememberQuarter = SelectedQuarter;
                    _selectedYear = value;
                    OnPropertyChanged("SelectedYear");
                    if (_selectedYear != null)
                    {
                        if (rememberQuarter != null)
                        {
                            var selectQ =
                                _selectedYear.Quarters.FirstOrDefault(
                                    s => s.Quarter.Quarter == rememberQuarter.Quarter.Quarter);
                            SelectedQuarter = selectQ ?? _selectedYear.Quarters.FirstOrDefault();
                        }
                        else
                        {
                            SelectedQuarter = _selectedYear.Quarters.FirstOrDefault();
                        }
                    }
                }
            }
        }

        public QuarterModel SelectedQuarter
        {
            get { return _selectedQuarter; }
            set
            {
                if (_selectedQuarter != value)
                {
                    _selectedQuarter = value;
                    OnPropertyChanged("SelectedQuarter");
                    if (_selectedQuarter != null)
                    {
                        if (SelectedQuarterDate == _selectedQuarter.End)
                        {
                            // Что бы сработало событие обновления данных
                            OnPropertyChanged("SelectedQuarterDate");
                        }
                        else
                        {
                            SelectedQuarterDate = _selectedQuarter.End;
                        }
                    }
                }
            }
        }
        

        public DateTime SelectedQuarterDate
        {
            get { return _selectedQuarterDate; }
            set
            {
                if (!SelectedQuarter.Dates.Contains(value))
                {
                    return;
                }
                if (_selectedQuarterDate != value)
                {
                    _selectedQuarterDate = value;    
                    OnPropertyChanged("SelectedQuarterDate");
                }
            }
        }

        public District SelectedDistrict
        {
            get { return _selectedDistrict; }
            set
            {
                _selectedDistrict = value;
                OnPropertyChanged("SelectedDistrict");
            }
        }

        public Rating SelectedRating
        {
            get { return _selectedRating; }
            set
            {
                _selectedRating = value;
                OnPropertyChanged("SelectedRating");
            }
        }

        public ScopeEnum SelectedScope
        {
            get { return ModelHelper.ConvertTnoCodeToScope(SelectedDistrict.Id); }
        }
    }
}
