﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Helpers;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Report;
using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common
{
    public class SelectDateModel : BaseViewModel
    {
        private YearReportItem _selectedYear;
        private ObservableCollection<YearReportItem> _years;

        public ObservableCollection<YearReportItem> Years
        {
            get { return _years; }
            set
            {
                if (_years != value)
                {
                    _years = value;
                    OnPropertyChanged("Years");
                    if (_years != null)
                    {
                        SelectedYear = _years.FirstOrDefault();
                    }
                }
            }
        }

        public YearReportItem SelectedYear
        {
            get { return _selectedYear; }
            set
            {
                _selectedYear = value;
                OnPropertyChanged("SelectedYear");
            }
        }
   }
}
