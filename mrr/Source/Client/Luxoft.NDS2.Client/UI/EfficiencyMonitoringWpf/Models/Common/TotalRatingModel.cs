﻿using System.Runtime.Serialization;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common
{
    [DataContract]
    public class TotalRatingModel
    {
        // Название управления
        [DataMember(Name = "windowCaption")]
        public string Caption { get; set; }

        // Название управления
        [DataMember(Name = "title")]
        public string Title { get; set; }

        // Руководитель управления
        [DataMember(Name = "boss")]
        public string Boss { get; set; }

        // Ответственный за КНП
        [DataMember(Name = "knpInCharge")]
        public string KnpInCharge { get; set; }

        // Общий рейнтинг
        [DataMember(Name = "totalRating")]
        public int TotalRating { get; set; }

        public TotalRatingModel()
        {
            Caption = "";
            Title = "";
            Boss = "";
            KnpInCharge = "";
            TotalRating = -1;
        }
    }
}
