﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common
{
    [DataContract]
    public class CollectionSelectedBase<T>
    {
        public delegate void SelectedChanged(T selectedObject);
        public event SelectedChanged OnSelectedObjectChanged;

        public CollectionSelectedBase(){
            Collection = new List<T>();
        }

        [DataMember(Name = "collection")]
        public IList<T> Collection { get; set; }

        private T _selectedObject;
        [DataMember(Name = "selected")]
        public T SelectedObject
        { 
            get { return _selectedObject; }
            set
            {
                _selectedObject = value;
                if (OnSelectedObjectChanged != null)
                {
                    OnSelectedObjectChanged(value);
                }
            }
        }
    }
}
