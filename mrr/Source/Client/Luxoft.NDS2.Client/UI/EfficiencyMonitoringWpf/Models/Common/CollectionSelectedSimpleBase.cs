﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common
{
    [DataContract]
    public class CollectionSelectedSimpleBase<T>
    {

        public CollectionSelectedSimpleBase()
        {
            Collection = new List<T>();
            SelectedId = "0";
        }

        [DataMember(Name = "collection")]
        public List<T> Collection { get; set; }

        [DataMember(Name = "selected")]
        public string SelectedId { get; set; }

        [DataMember(Name = "dateSelected")]
        public string DateSelected { get; set; }
    }
}
