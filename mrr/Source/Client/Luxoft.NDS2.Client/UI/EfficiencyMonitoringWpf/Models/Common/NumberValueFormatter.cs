﻿using System;
using System.Globalization;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common
{
    public static class NumberValueFormatter
    {
        public static string GetStringValue(decimal value, int precision = 0, bool withDot = false)
        {
            var res = Math.Round(value, precision).ToString("f" + precision, CultureInfo.CurrentCulture);
            return withDot ? res.Replace(',', '.') : res;
        }

        public static string GetStringPercent(decimal value, int precision = 0, bool withDot = false)
        {
            return GetStringValue(value, precision, withDot) + "%";
        }

        public static string GetStringDifferenceValue(string valueStr1, string valueStr2, int precision = 0, bool withDot = false)
        {
            return GetStringValue(GetDifferenceValue(valueStr1, valueStr2, precision), precision, withDot);
        }

        public static decimal GetDifferenceValue(string valueStr1, string valueStr2, int precision = 0)
        {
            decimal value1;
            decimal value2;
            decimal.TryParse(valueStr1, out value1);
            decimal.TryParse(valueStr2, out value2);           
            return Math.Round(value1 - value2, precision);
        }

        public static decimal GetDifferenceValue(decimal value1, decimal value2, int precision = 0)
        {
            return Math.Round(value1 - value2, precision);
        }
    }
}
