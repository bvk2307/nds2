﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;
using CommonComponents.Utils;
using CommonComponents.Utils.Async;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Base.Wpf;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common.TnoMonitorData;
using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common
{
    public abstract class BaseMainViewModel<Prez, AV> : BaseViewModel
        where AV : BaseView
        where Prez : BaseMonitoringPresenter<AV>
    {

        protected BaseMainViewModel()
        {
            State = new StateModel();
            State.PropertyChanged += State_PropertyChanged;
        }

        private Prez _presenter;
        private TnoMonitorDataInfoModel _selectedRatingData;
        private TnoMonitorDataInfoModel _fixedRatingData;
        private ObservableCollection<TnoMonitorDataInfoModel> _ratingData = new ObservableCollection<TnoMonitorDataInfoModel>();
        private BaseChartDataModel _chartModel;
        private string _headerString;
        private string _tickerString;
        private ObservableCollection<YearModel> _years;
        private bool _isBusy;
        private bool _tickerAnimated;
        private bool _tickerEmpty;

        public Prez Presenter
        {
            get { return _presenter; }
            set
            {
                _presenter = value;
                if (_presenter != null)
                {
                    InvokeInitCalculatedData();
                    OnPresenterChanged();
                }
            }
        }

        protected abstract List<TnoData> ReadCalculatedData();

        protected abstract void InitCalculatedData(List<TnoData> data);

        public void InvokeInitCalculatedData()
        {
            if (Presenter != null)
            {
                IsBusy = true;
                ReadTickerString();
                var asyncWorker = new AsyncWorker<List<TnoData>>();
                asyncWorker.DoWork +=
                    (sender, args) =>
                    {
                        args.Result = ReadCalculatedData();
                    };
                asyncWorker.Complete += (sender, args) =>
                {
                    InitCalculatedData(args.Result);
                    IsBusy = false;
                    TickerAnimated = !TickerEmpty;
                };
                asyncWorker.Start();
            }
        }

        public bool IsBusy
        {
            get { return this._isBusy; }
            set
            {
                if (this._isBusy != value)
                {
                    this._isBusy = value;
                    OnPropertyChanged("IsBusy");
                }
            }
        }

        protected void OnPresenterChanged()
        {
        }

        private void ReadTickerString()
        {
            TickerAnimated = false;
            var scopeDataList = Presenter.GetFederalCollection(new CalculationData()
            {
                SelectedTnoCode = "0",
                Quarter = State.SelectedQuarter.Quarter,
                CalculationDate = State.SelectedQuarterDate,
                DetailLevel = ScopeEnum.RF_UFNS
            });

            if (scopeDataList.Sum(s => s.DIS_CLS_TOTAL_CNT + s.DIS_OPN_TOTAL_CNT) == 0)
            {
                TickerString = "нет данных";
                TickerEmpty = true;
            }
            else
            {
                var list =
                scopeDataList.Where(s => s.TNO_CODE != "0").OrderByDescending(s => s.RNK_FEDERAL_EFF_IDX_2_4)
                    .Take(10)
                    .Select(s => s.RNK_FEDERAL_EFF_IDX_2_4 + " " + s.TNO_NAME);
                TickerString = string.Join("       ", list);
                TickerEmpty = false;
            }
        }

        public ObservableCollection<District> Districts { get; set; }


        /// <summary>
        /// Рейтинг УФНС в бегущей строке TOP 10 худших
        /// </summary>
        public string TickerString
        {
            get { return _tickerString; }
            set
            {
                _tickerString = value;
                OnPropertyChanged("TickerString");
            }
        }

        /// <summary>
        /// Рейтинг УФНС в бегущей строке TOP 10 худших
        /// </summary>
        public bool TickerAnimated
        {
            get { return _tickerAnimated; }
            set
            {
                if (_tickerAnimated != value)
                {
                    _tickerAnimated = value;
                    OnPropertyChanged("TickerAnimated");
                }
            }
        }

        public bool TickerEmpty
        {
            get { return _tickerEmpty; }
            set
            {
                if (_tickerEmpty != value)
                {
                    _tickerEmpty = value;
                    OnPropertyChanged("TickerEmpty");
                }
            }
        }

        /// <summary>
        /// Заголовок в форме
        /// </summary>
        public string HeaderString
        {
            get { return _headerString; }
            set
            {
                _headerString = value;
                OnPropertyChanged("HeaderString");
            }
        }
        
        /// <summary>
        /// Список доступных кварталов
        /// </summary>      
        public ObservableCollection<YearModel> Years
        {
            get { return _years; }
            set
            {
                if (_years != null)
                {
                    _years.Clear();
                    OnPropertyChanged("Years");
                }
                _years = value;
                OnPropertyChanged("Years");
            }
        }

        /// <summary>
        /// Фиксированная строка
        /// </summary>
        public TnoMonitorDataInfoModel FixedRatingData
        {
            get { return _fixedRatingData; }
            set
            {
                _fixedRatingData = value;
                if (_fixedRatingData != null)
                {
                    _fixedRatingData.ShowRatingValue = false;
                }
                OnPropertyChanged("FixedRatingData");
            }
        }

        /// <summary>
        /// Выделенная строка
        /// </summary>
        public TnoMonitorDataInfoModel SelectedRatingData
        {
            get { return _selectedRatingData; }
            set
            {
                if (_selectedRatingData != value)
                {
                    if (value == null && RatingData.Count > 0)
                    {
                        // Не изменяем, если есть данные - фильтр работает
                        return;
                    }
                    else
                    {
                        RatingData.ForAll(s => s.Selected = false);
                        _selectedRatingData = value;
                        if (_selectedRatingData != null)
                        {
                            _selectedRatingData.Selected = true;
                        }
                        OnPropertyChanged("SelectedRatingData");    
                    }
                }
            }
        }

        private void RealocateSelectedRatingData()
        {
            var worker = new AsyncWorker<object>();
            worker.DoWork +=
                (sender, args) =>
                {
                    Thread.Sleep(200);
                };

            worker.Complete += (sender, args) =>
            {
                RatingData.ForAll(s => s.Selected = false);
                OnPropertyChanged("SelectedRatingData");
                if (_selectedRatingData != null)
                {
                    _selectedRatingData.Selected = true;
                }
                OnPropertyChanged("SelectedRatingData");
            };
            worker.Start();
        }

        public ObservableCollection<TnoMonitorDataInfoModel> RatingData
        {
            get { return _ratingData; }
            set
            {
                if (_ratingData != null)
                {
                    _ratingData.Clear();
                    OnPropertyChanged("RatingData");
                }
                _ratingData = value;
                OnPropertyChanged("RatingData");
            }
        }

        public BaseChartDataModel ChartModel
        {
            get { return _chartModel; }
            set
            {
                _chartModel = value;
                OnPropertyChanged("ChartModel");
            }
        }

        /// <summary>
        /// Список рейтингов
        /// </summary>
        public ObservableCollection<Rating> Ratings { get; set; }

        public StateModel State { get; set; }

        void State_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            // Потому что дата поменятеся тоже
            if (e.PropertyName == "SelectedQuarter" || e.PropertyName == "SelectedYear")
                return;

            if (e.PropertyName == "SelectedRating")
            {
                if (RatingData != null)
                {
                    SelectedRatingData = null;
                    var newList = RatingData.Select(s => new TnoMonitorDataInfoModel(s.Dto, State.SelectedRating.Id, State.SelectedScope)).ToList();
                    var newRating = new ObservableCollection<TnoMonitorDataInfoModel>(newList);
                    FixedRatingData = newRating.FirstOrDefault(p => p.TnoCode == State.SelectedDistrict.Id);
                    RatingData = newRating;
                    SelectedRatingData = FixedRatingData;
                }
            }
            else
            {
                InvokeInitCalculatedData();
            }
        }

        public void ControlClosed()
        {
            OnClosed();
        }

        protected virtual void OnClosed(){}

    }

}
