﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common
{
    public class ChartDataModel : BaseChartDataModel
    {
        private readonly IDictionary<QuarterInfo, IEnumerable<FederalDistrictData>> _federalDistrictCollection;

		public ChartDataModel(IDictionary<QuarterInfo, IEnumerable<FederalDistrictData>> federalDistrictCollection)
        {
            _federalDistrictCollection = federalDistrictCollection;
            Data = new List<ChartDataRowModel>();
            _quartersForCompare = new List<QuarterInfo>();
        }

        public IList<ChartDataRowModel> Data { get; set; }

        private List<QuarterInfo> _quartersForCompare { get; set; }

    }
}
