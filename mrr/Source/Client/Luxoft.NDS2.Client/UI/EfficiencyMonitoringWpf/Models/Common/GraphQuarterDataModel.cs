﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common
{
    [DataContract]
    public class GraphQuarterDataModel
    {
        // Квартал
        [DataMember(Name = "quarter")]
        public QuarterInfo Quarter { get; set; }

        // Данные
        [DataMember(Name = "weekData")]
        public IDictionary<int, string> WeekData { get; set; }

        // Сводное число по кварталу (для гистограммы)
        [DataMember(Name = "value")]
        public decimal Total { get; set; }

        // Строковое представление квартала
        [DataMember(Name = "label")]
        public string Label
        {
            get { return Quarter.ToString(); }
            set { }
        }
    }
}
