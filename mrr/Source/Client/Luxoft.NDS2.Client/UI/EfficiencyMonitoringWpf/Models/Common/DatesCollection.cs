﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common
{
    [DataContract]
    public class DatesCollection
    {
        public delegate void SelectedDateChanged(DateTime date);

        public event SelectedDateChanged OnSelectedDateChanged;

        public DatesCollection()
        {
            ListDates = new List<DateTime>();
        }

        [IgnoreDataMember]
        public DateTime MaxDate
        {
            get { return ListDates.OrderByDescending(d => d).FirstOrDefault(); }
        }

        private DateTime _minDate;

        [IgnoreDataMember]
        public DateTime SelectedDate
        {
            get { return _minDate; }
            set
            {
                _minDate = value;
                StringSelectedDate = value.ToString("dd.MM.yyyy");
            }
        }

        [DataMember(Name = "maxDate")]
        public string StringMaxDate 
        {
            get
            {
                return ListDates.Max().ToString("dd.MM.yyyy");
            } set{} 
        }

        [DataMember(Name = "minDate")]
        public string StringSelectedDate
        {
            get { return ListDates.Min().ToString("dd.MM.yyyy"); } 
            set{}
        }

        [DataMember(Name = "collection")]
        public IEnumerable<string> StringListDates {
            get
            {
                return ListDates.OrderByDescending(d => d).Select(d => d.ToString("dd.MM.yyyy"));
            } set {}}

        [IgnoreDataMember]
        public List<DateTime> ListDates { get; set; }

        public void SortDescRemoveMax()
        {
            ListDates.Sort((d1, d2) => d2.CompareTo(d1));
        }

        public void SetStringsDates()
        {
            StringMaxDate = MaxDate.ToString("dd.MM.yyyy");
            StringSelectedDate = SelectedDate.ToString("dd.MM.yyyy");
            StringListDates = ListDates.Select(d => d.ToString("dd.MM.yyyy"));
        }
    }
}
