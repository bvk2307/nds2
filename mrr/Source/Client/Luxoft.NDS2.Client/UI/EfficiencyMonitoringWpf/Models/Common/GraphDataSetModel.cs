﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Windows.Media;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common
{
    public class GraphDataSetModel: List<GraphDataValueModel>
    {
		public string SeriesName { get; set; }

        public bool Visible { get; set; }

        public string AnchorBgColor { get; set; }
		
        public Brush Color { get; set; }

    }
}
