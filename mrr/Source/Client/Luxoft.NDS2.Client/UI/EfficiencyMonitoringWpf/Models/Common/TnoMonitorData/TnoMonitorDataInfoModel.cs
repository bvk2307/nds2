﻿using System.Runtime.Serialization;
using System.Windows.Navigation;
using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common.TnoMonitorData
{
    public class TnoMonitorDataInfoModel: BaseViewModel
    {
        public TnoMonitorDataInfoModel(TnoData data, IndexEnum ratingMode, ScopeEnum scope)
        {
            _dto = data;
            _scope = scope;
            RatingMode = ratingMode;
        }

        private bool _showRatingValue = true;
        private ScopeEnum _scope;
        private TnoData _dto;
        private bool _selected;
        private IndexEnum _ratingMode;
        private decimal? _currentEffIndexRatingValueView;

        public IndexEnum RatingMode
        {
            get { return _ratingMode; }
            set
            {
                _ratingMode = value;
                OnPropertyChanged("RatingMode");
                OnPropertyChanged("CurrentEffIndexValue");
                OnPropertyChanged("CurrentEffIndexRatingValue");
                CurrentEffIndexRatingValueView = _showRatingValue ? CurrentEffIndexRatingValue : new decimal?();
            }
        }

        public bool ShowRatingValue
        {
            get { return _showRatingValue; }
            set
            {
                _showRatingValue = value;
                OnPropertyChanged("ShowRatingValue");
                CurrentEffIndexRatingValueView = _showRatingValue ? CurrentEffIndexRatingValue : new decimal?();
            }
        }

        public ScopeEnum Scope
        {
            get { return _scope; }
        }

        public bool Selected
        {
            get { return _selected; }
            set
            {
                _selected = value;
                OnPropertyChanged("Selected");
            }
        }

        public TnoData Dto
        {
            get { return _dto; }
        }

        public virtual decimal CurrentEffIndexValue
        {
            get
            {
                switch (RatingMode)
                {
                    case IndexEnum.ClosedDiscrepancyShareByAmount:
                        return EffIndex21;
                    case IndexEnum.ClosedDiscrepancyShareByQuantity:
                        return EffIndex22;
                    case IndexEnum.DeclarationWithDiscrepancyShareByQuantity:
                        return EffIndex23;
                    case IndexEnum.DiscrepancyShareInDeduction:
                        return EffIndex24;
                }

                return 0;
            }
        }


        public virtual decimal? CurrentEffIndexRatingValueView
        {
            get { return _currentEffIndexRatingValueView; }
            set
            {
                _currentEffIndexRatingValueView = value;
                OnPropertyChanged("CurrentEffIndexRatingValueView");
            }
        }

        public virtual decimal? CurrentEffIndexRatingValue
        {
            get
            {
                switch (RatingMode)
                {
                    case IndexEnum.ClosedDiscrepancyShareByAmount:
                        return ResolveRatingClosedDiscrepancyShareByAmount;

                    case IndexEnum.ClosedDiscrepancyShareByQuantity:
                        return ResolveRatingClosedDiscrepancyShareByQuantity;

                    case IndexEnum.DeclarationWithDiscrepancyShareByQuantity:
                        return ResolveRatingDeclarationWithDiscrepancyShareByQuantity;

                    case IndexEnum.DiscrepancyShareInDeduction:
                        return ResolveRatingDiscrepancyShareInDeduction;
                }
                return 0;
            }
        }

        public virtual decimal? CurrentEffIndexRatingValuePrev
        {
            get
            {
                decimal? value = 0;
                switch (RatingMode)
                {
                    case IndexEnum.ClosedDiscrepancyShareByAmount:
                        value = ResolveRatingClosedDiscrepancyShareByAmountPrev;
                        break;

                    case IndexEnum.ClosedDiscrepancyShareByQuantity:
                        value = ResolveRatingClosedDiscrepancyShareByQuantityPrev;
                        break;

                    case IndexEnum.DeclarationWithDiscrepancyShareByQuantity:
                        value = ResolveRatingDeclarationWithDiscrepancyShareByQuantityPrev;
                        break;

                    case IndexEnum.DiscrepancyShareInDeduction:
                        value = ResolveRatingDiscrepancyShareInDeductionPrev;
                        break;
                }
                return value == 0 ? new decimal?() : value;
            }
        }


        public string TnoCode { get { return _dto.TNO_CODE; } }

        public string TnoName { get { return _dto.TNO_NAME; }  }

        
        public decimal EffIndex21 { get { return _dto.EFF_IDX_2_1; }  }

        
        public decimal EffIndex22 { get { return _dto.EFF_IDX_2_2; }  }

        
        public decimal EffIndex23 { get { return _dto.EFF_IDX_2_3; }  }

        
        public decimal EffIndex24 { get { return _dto.EFF_IDX_2_4; }  }

        
        public decimal RankEfficiencyIndex_21 { get { return _dto.RNK_EFF_IDX_2_1; }  }

        
        public decimal RankEfficiencyIndex_22 { get { return _dto.RNK_EFF_IDX_2_2; }  }

        
        public decimal RankEfficiencyIndex_23 { get { return _dto.RNK_EFF_IDX_2_3; }  }

        
        public decimal RankEfficiencyIndex_24 { get { return _dto.RNK_EFF_IDX_2_4; }  }

        
        public decimal RankFederalEfficiencyIndex_21 { get { return _dto.RNK_FEDERAL_EFF_IDX_2_1; }  }

        
        public decimal RankFederalEfficiencyIndex_22 { get { return _dto.RNK_FEDERAL_EFF_IDX_2_2; }  }

        
        public decimal RankFederalEfficiencyIndex_23 { get { return _dto.RNK_FEDERAL_EFF_IDX_2_3; }  }

        
        public decimal RankFederalEfficiencyIndex_24 { get { return _dto.RNK_FEDERAL_EFF_IDX_2_4; }  }


        public decimal EffIndex21Prev { get { return _dto.EFF_IDX_2_1_PREV; } }


        public decimal EffIndex22Prev { get { return _dto.EFF_IDX_2_2_PREV; } }


        public decimal EffIndex23Prev { get { return _dto.EFF_IDX_2_3_PREV; } }


        public decimal EffIndex24Prev { get { return _dto.EFF_IDX_2_4_PREV; } }


        public decimal? RankEfficiencyIndex_21Prev { get { return _dto.RNK_EFF_IDX_2_1_PREV; } }


        public decimal? RankEfficiencyIndex_22Prev { get { return _dto.RNK_EFF_IDX_2_2_PREV; } }


        public decimal? RankEfficiencyIndex_23Prev { get { return _dto.RNK_EFF_IDX_2_3_PREV; } }


        public decimal? RankEfficiencyIndex_24Prev { get { return _dto.RNK_EFF_IDX_2_4_PREV; } }


        public decimal? RankFederalEfficiencyIndex_21Prev { get { return _dto.RNK_FEDERAL_EFF_IDX_2_1_PREV; } }


        public decimal? RankFederalEfficiencyIndex_22Prev { get { return _dto.RNK_FEDERAL_EFF_IDX_2_2_PREV; } }


        public decimal? RankFederalEfficiencyIndex_23Prev { get { return _dto.RNK_FEDERAL_EFF_IDX_2_3_PREV; } }


        public decimal? RankFederalEfficiencyIndex_24Prev { get { return _dto.RNK_FEDERAL_EFF_IDX_2_4_PREV; } }

        public decimal DeclarationDeductionAmount { get { return _dto.DEDUCTION_AMNT; }  }

        
        public decimal DeclarationCaclulationAmount { get { return _dto.CALCULATION_AMNT; }  }

        
        public int DeclarationCount { get { return _dto.DECL_ALL_CNT; }  }

        
        public decimal DeclarationNdsSum { get { return _dto.DECL_ALL_NDS_SUM; }  }

        
        public int DeclarationWithOperationCount { get { return _dto.DECL_OPERATION_CNT; }  }

        
        public int DeclarationWithDiscrepancyCount { get { return _dto.DECL_DISCREPANCY_CNT; }  }

        
        public int DeclarationPaymentTypeCount { get { return _dto.DECL_PAY_CNT; }  }

        
        public decimal DeclarationPaymentTypeNdsSum { get { return _dto.DECL_PAY_NDS_SUM; }  }

        
        public int DeclarationCompensationTypeCount { get { return _dto.DECL_COMPENSATE_CNT; }  }

        
        public decimal DeclarationCompensationNdsSum { get { return _dto.DECL_COMPENSATE_NDS_SUM; }  }

        
        public int DeclarationZeroTypeCount { get { return _dto.DECL_ZERO_CNT; }  }


        public decimal DiscrepancyAllAmount { get { return _dto.DIS_TOTAL_AMNT; }  }

        
        public int DiscrepancyAllCount { get { return _dto.DIS_TOTAL_CNT; }  }

        
        public decimal DiscrepancyAllGapAmount { get { return _dto.DIS_GAP_TOTAL_AMNT; }  }

        
        public int DiscrepancyAllGapCount { get { return _dto.DIS_GAP_TOTAL_CNT; }  }

        
        public decimal DiscrepancyAllNdsAmount { get { return _dto.DIS_NDS_TOTAL_AMNT; }  }

        
        public int DiscrepancyAllNdsCount { get { return _dto.DIS_NDS_TOTAL_CNT; }  }

        
        public decimal DiscrepancyClosedAmount { get { return _dto.DIS_CLS_TOTAL_AMNT; }  }

        
        public int DiscrepancyClosedCount { get { return _dto.DIS_CLS_TOTAL_CNT; }  }

        
        public decimal DiscrepancyClosedGapAmount { get { return _dto.DIS_CLS_GAP_TOTAL_AMNT; }  }

        
        public int DiscrepancyClosedGapCount { get { return _dto.DIS_CLS_GAP_TOTAL_CNT; }  }

        
        public decimal DiscrepancyClosedNdsAmount { get { return _dto.DIS_CLS_NDS_TOTAL_AMNT; }  }

        
        public int DiscrepancyClosedNdsCount { get { return _dto.DIS_CLS_NDS_TOTAL_CNT; }  }


        public decimal DiscrepancyOpenAmount { get { return _dto.DIS_OPN_TOTAL_AMNT; }  }

        
        public int DiscrepancyOpenCount { get { return _dto.DIS_OPN_TOTAL_CNT; }  }

        
        public decimal DiscrepancyOpenGapAmount { get { return _dto.DIS_OPN_GAP_TOTAL_AMNT; }  }

        
        public int DiscrepancyOpenGapCount { get { return _dto.DIS_OPN_GAP_TOTAL_CNT; }  }

        
        public decimal DiscrepancyOpenNdsAmount { get { return _dto.DIS_OPN_NDS_TOTAL_AMNT; }  }

        
        public int DiscrepancyOpenNdsCount { get { return _dto.DIS_OPN_NDS_TOTAL_CNT; }  }

        public decimal ResolveRatingClosedDiscrepancyShareByAmount
        {
            get
            {
                switch (_scope)
                {
                    case ScopeEnum.RF_UFNS:
                    case ScopeEnum.MIKN:
                        return this.RankFederalEfficiencyIndex_21;
                    case ScopeEnum.RF_DISTRICTS:
                    case ScopeEnum.UFNS:
                        return this.RankEfficiencyIndex_21;
                    default:
                        return -1;
                }
            }
        }

        private decimal ResolveRatingClosedDiscrepancyShareByQuantity
        {
            get
            {
                switch (_scope)
                {
                    case ScopeEnum.RF_UFNS:
                    case ScopeEnum.MIKN:
                        return this.RankFederalEfficiencyIndex_22;
                    case ScopeEnum.UFNS:
                    case ScopeEnum.RF_DISTRICTS:
                        return this.RankEfficiencyIndex_22;
                    default:
                        return -1;
                }
            }
        }

        private decimal ResolveRatingDeclarationWithDiscrepancyShareByQuantity
        {
            get
            {
                switch (_scope)
                {
                    case ScopeEnum.RF_UFNS:
                    case ScopeEnum.MIKN:
                        return this.RankFederalEfficiencyIndex_23;
                    case ScopeEnum.UFNS:
                    case ScopeEnum.RF_DISTRICTS:
                        return this.RankEfficiencyIndex_23;
                    default:
                        return -1;
                }
            }
        }


        public decimal? ResolveRatingDiscrepancyShareInDeduction
        {
            get
            {
                switch (_scope)
                {
                    case ScopeEnum.RF_UFNS:
                    case ScopeEnum.MIKN:
                        return this.RankFederalEfficiencyIndex_24;
                    case ScopeEnum.UFNS:
                    case ScopeEnum.RF_DISTRICTS:
                        return this.RankEfficiencyIndex_24;
                    default:
                        return -1;
                }
            }
        }

        private decimal? ResolveRatingClosedDiscrepancyShareByAmountPrev
        {
            get
            {
                switch (_scope)
                {
                    case ScopeEnum.RF_UFNS:
                    case ScopeEnum.MIKN:
                        return this.RankFederalEfficiencyIndex_21Prev;
                    case ScopeEnum.RF_DISTRICTS:
                    case ScopeEnum.UFNS:
                        return this.RankEfficiencyIndex_21Prev;
                    default:
                        return -1;
                }
            }
        }

        private decimal? ResolveRatingClosedDiscrepancyShareByQuantityPrev
        {
            get
            {
                switch (_scope)
                {
                    case ScopeEnum.RF_UFNS:
                    case ScopeEnum.MIKN:
                        return this.RankFederalEfficiencyIndex_22Prev;
                    case ScopeEnum.UFNS:
                    case ScopeEnum.RF_DISTRICTS:
                        return this.RankEfficiencyIndex_22Prev;
                    default:
                        return -1;
                }
            }
        }

        private decimal? ResolveRatingDeclarationWithDiscrepancyShareByQuantityPrev
        {
            get
            {
                switch (_scope)
                {
                    case ScopeEnum.RF_UFNS:
                    case ScopeEnum.MIKN:
                        return this.RankFederalEfficiencyIndex_23Prev;
                    case ScopeEnum.UFNS:
                    case ScopeEnum.RF_DISTRICTS:
                        return this.RankEfficiencyIndex_23Prev;
                    default:
                        return -1;
                }
            }
        }


        private decimal? ResolveRatingDiscrepancyShareInDeductionPrev
        {
            get
            {
                switch (_scope)
                {
                    case ScopeEnum.RF_UFNS:
                    case ScopeEnum.MIKN:
                        return this.RankFederalEfficiencyIndex_24Prev;
                    case ScopeEnum.UFNS:
                    case ScopeEnum.RF_DISTRICTS:
                        return this.RankEfficiencyIndex_24Prev;
                    default:
                        return -1;
                }
            }
        }
    }
}
