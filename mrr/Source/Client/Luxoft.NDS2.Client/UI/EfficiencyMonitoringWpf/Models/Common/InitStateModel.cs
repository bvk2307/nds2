﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common
{
    public class InitStateModel
    {

        public string TnoCode { get; set; }

        public string TnoName { get; set; }

        public int Year { get; set; }

        public int Quarter { get; set; }

        public DateTime Date { get; set; }

        public DimensionType DimensionType { get; set; }

    }
}
