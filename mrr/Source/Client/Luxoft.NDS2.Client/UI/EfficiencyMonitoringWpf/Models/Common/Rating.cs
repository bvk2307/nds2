﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common
{
    public class Rating: BaseViewModel
    {
        public IndexEnum Id { get; set; }

        public string Name { get; set; }

        public static Rating Create(IndexEnum id, string name)
        {
            return new Rating {Id = id, Name = name};
        }

        public static List<Rating> GetAllRatings()
        {
            var ratings = new List<Rating>
            {
                Create(IndexEnum.DiscrepancyShareInDeduction, "Доля расхождений в сумме вычетов по НДС"),
                Create(IndexEnum.ClosedDiscrepancyShareByAmount, "Доля закрытых расхождений в расхождениях всего (по сумме)"),
                Create(IndexEnum.ClosedDiscrepancyShareByQuantity, "Доля закрытых расхождений в расхождениях всего (по количеству)"),
                Create(IndexEnum.DeclarationWithDiscrepancyShareByQuantity, "Доля деклараций с расхождениями в декларациях с операциями (по количеству)"),
            };
            return ratings;
        }
    }
}