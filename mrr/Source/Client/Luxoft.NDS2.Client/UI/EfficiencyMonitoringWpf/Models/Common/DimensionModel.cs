﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Infragistics.Win.UltraWinSchedule;
using System.ComponentModel.DataAnnotations;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common
{
    public enum DimensionType
    {
        [Display(Name = "млрд.")]
        Mlrd,
        [Display(Name = "млн.")]
        Mln,
        [Display(Name = "тыс.")]
        Ts
    }

    public class DimensionModel: BaseViewModel
    {
        private bool _ts;
        private bool _mln;
        private bool _mlrd = true;
        private DimensionType _dimensionType = DimensionType.Mlrd;

        public void InitDimensionType(DimensionType dimensionType)
        {
            DimensionType = dimensionType;
            switch (dimensionType)
            {
                case DimensionType.Mlrd:
                {
                    _mlrd = true;
                    _mln = false;
                    _ts = false;
                    break;
                }
                case DimensionType.Mln:
                {
                    _mlrd = false;
                    _mln = true;
                    _ts = false;
                    break;
                }
                case DimensionType.Ts:
                {
                    _mlrd = false;
                    _mln = false;
                    _ts = true;
                    break;
                }
            }
            OnPropertyChanged("Ts");
            OnPropertyChanged("Mln");
            OnPropertyChanged("Mlrd");
        }

        public DimensionType DimensionType
        {
            get { return _dimensionType; }
            set
            {
                _dimensionType = value;
                OnPropertyChanged("DimensionType");
            }
        }

        public bool Ts
        {
            get { return _ts; }
            set
            {
                _ts = value;
                OnPropertyChanged("Ts");
                if (value)
                {
                    DimensionType = DimensionType.Ts;
                }
            }
        }

        public bool Mln
        {
            get { return _mln; }
            set
            {
                _mln = value;
                OnPropertyChanged("Mln");
                if (value)
                {
                    DimensionType = DimensionType.Mln;
                }
            }
        }

        public bool Mlrd
        {
            get { return _mlrd; }
            set
            {
                _mlrd = value;
                OnPropertyChanged("Mlrd");
                if (value)
                {
                    DimensionType = DimensionType.Mlrd;
                }

            }
        }
    }
}
