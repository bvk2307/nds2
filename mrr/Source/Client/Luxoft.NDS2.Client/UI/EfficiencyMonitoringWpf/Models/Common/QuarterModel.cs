﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common
{
    public class QuarterModel : BaseViewModel
    {
        private ObservableCollection<DateTime> _dates;
        private IEnumerable<CalculationDateModel> _dateModels;

        public QuarterInfo Quarter { get; set; }

        /// <summary>
        /// Перечень моделей доступных дат
        /// </summary>
        public IEnumerable<CalculationDateModel> DateModels
        {
            get { return _dateModels; }
            set
            {
                _dateModels = value;
                _dates = new ObservableCollection<DateTime>(_dateModels.Select(x => x.Date));
            }
        }

        /// <summary>
        /// Список доступных дат
        /// </summary>
        public ObservableCollection<DateTime> Dates
        {
            get { return _dates; }
            private set
            {
                _dates = value;
                OnPropertyChanged("Dates");
                FillProperties();
            }
        }

        public DateTime Start { get; set; }

        public DateTime End { get; set; }

        /// <summary>
        /// Список заблокированных дат
        /// </summary>
        public ObservableCollection<DateTime> BlackoutDates { get; set; }

        /// <summary>
        /// Признак что квартал содержит расчет данных основанный на агрегате Мониторинг Эффективности
        /// </summary>
        public bool ContainsEffAgrCalculation
        {
            get
            {
                return _dateModels == null ? false :
                            _dateModels.Any(x => x.IsBasedOnEfficiencyAgregate);
            }
        }

        internal void FillProperties()
        {

            Start = Dates.Min();
            End = Dates.Max();
            var allDays = Enumerable.Range(0, 1 + End.Subtract(Start).Days)
                .Select(offset => Start.AddDays(offset)).AsQueryable();

            BlackoutDates = new ObservableCollection<DateTime>(allDays.Where(x => !Dates.Contains(x)));
        }

        public override string ToString()
        {
            return Quarter.QuarterToString();
        }
    }
}
