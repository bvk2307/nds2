﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common
{

    public class ColumnSortModel
    {
        public string Name { get; set; }
        
        public ListSortDirection SortDirection { get; set; }
    }
}
