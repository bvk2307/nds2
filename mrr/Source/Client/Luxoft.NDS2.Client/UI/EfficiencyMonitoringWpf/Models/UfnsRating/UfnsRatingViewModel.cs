﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Windows.Threading;
using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.UI.Base.Wpf.Model;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Chart;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Controls;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Helpers;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.CentralApparat;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common.TnoMonitorData;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Report;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Reports;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;
using Microsoft.Win32;
using Telerik.Windows.Controls.GridView;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.UfnsRating
{
    public class UfnsRatingViewModel : BaseMainViewModel<UfnsRatingPresenter, UfnsRatingView>, IChartDataProvider, IClerableModel
    {
        public UfnsRatingViewModel() : base()
        {
            _chartController = new ChartDataController(this);
        }

        private readonly ChartDataController _chartController;

        protected override void OnInternalPropertyChanged(string name)
        {
            if (name == "SelectedRatingData")
            {
                if (SelectedRatingData != null)
                {
                    ChartModel = CreateChartModel();
                    if (FixedRatingData == SelectedRatingData)
                    {
                        HeaderString = FixedRatingData.TnoName;
                    }
                    else
                    {
                        HeaderString = string.Format("{0} - {1}", FixedRatingData.TnoName, SelectedRatingData.TnoName);
                    }
                }
                else
                {
                    ChartModel = null;
                }
            }
        }

        protected override List<TnoData> ReadCalculatedData()
        {
            return Presenter.GetFederalCollection(new CalculationData
            {
                SelectedTnoCode = State.SelectedDistrict.Id,
                Quarter = State.SelectedQuarter.Quarter,
                CalculationDate = State.SelectedQuarterDate,
                DetailLevel = ScopeEnum.UFNS
            });
        }

        protected override void InitCalculatedData(List<TnoData> data)
        {
            if (data.Any())
            {
                var list =
                    data.Where(e => e.RATING_TYPE == "1").Select(
                        i => new TnoMonitorDataInfoModel(i, State.SelectedRating.Id, State.SelectedScope)).ToList();

                FixedRatingData = list.FirstOrDefault(p => p.TnoCode == State.SelectedDistrict.Id);
                RatingData = new ObservableCollection<TnoMonitorDataInfoModel>(list);
                SelectedRatingData = FixedRatingData;
            }
            else
            {
                FixedRatingData = new TnoMonitorDataInfoModel(new TnoData(), State.SelectedRating.Id, State.SelectedScope);
                RatingData = new ObservableCollection<TnoMonitorDataInfoModel>();
                SelectedRatingData = FixedRatingData;
            }
        }

        private BaseChartDataModel CreateChartModel()
        {
            string tnoCode = SelectedRatingData.TnoCode;
            TnoData parent = null;
            ScopeEnum scope;
            ScopeEnum parentScope;

            if (String.Equals(tnoCode, State.SelectedDistrict.Id))
            {
                parent = new TnoData { TNO_CODE = "0", TNO_NAME = "РФ" };
                parentScope = ScopeEnum.RF;
                scope = ScopeEnum.UFNS;
            }
            else
            {
                var ppp = RatingData.FirstOrDefault(d => d.TnoCode == State.SelectedDistrict.Id);
                parent = ppp != null ? ppp.Dto : null;
                parentScope = ScopeEnum.UFNS;
                scope = ScopeEnum.IFNS;
            }

            return _chartController.CreateChartModel(
                new ChartWebDataRequest()
                {
                    Quarter = State.SelectedQuarter.Quarter,
                    IndexValue = State.SelectedRating.Id,
                    CalculationDate = State.SelectedQuarterDate,
                    Context = ContextType.Central,
                    TnoModel = SelectedRatingData.Dto,
                    Scope = scope,
                    ParentTnoModel = parent,
                    ParentScope = parentScope
                });
        }

        Dictionary<string, List<TnoRatingInfo>> IChartDataProvider.GetQuartersRatings(ChartData request)
        {
            return Presenter.GetQuartersRatings(request);
        }

        List<QuarterInfo> IChartDataProvider.GetQuarters()
        {
            return this.Years.SelectMany(s => s.Quarters.Select(q => q.Quarter)).ToList();
        }

        public void CreateInspectionView()
        {
            if (this.SelectedRatingData.TnoCode == this.FixedRatingData.TnoCode)
                return;

            Dispatcher.CurrentDispatcher.Invoke(
                new Action(
                    () =>
                        Presenter.CreateInspectionView(new InitStateModel
                        {
                            TnoCode = this.SelectedRatingData == null ? null : this.SelectedRatingData.TnoCode,
                            TnoName = this.SelectedRatingData == null ? null : this.SelectedRatingData.TnoName,
                            Year = this.State.SelectedQuarter.Quarter.Year,
                            Quarter = this.State.SelectedQuarter.Quarter.Quarter,
                            Date = this.State.SelectedQuarterDate
                        })));
        }

        public void CreateUnprovenDiffDetailsView()
        {
            if (this.SelectedRatingData == null)
                return;
            Dispatcher.CurrentDispatcher.Invoke(
                new Action(
                    () =>
                        Presenter.CreateUnprovenDiffDetailsView(
                            new InitStateModel
                            {
                                TnoCode = this.SelectedRatingData == null ? null : this.SelectedRatingData.TnoCode,
                                TnoName = this.SelectedRatingData == null ? null : this.SelectedRatingData.TnoName,
                                Year = this.State.SelectedQuarter.Quarter.Year,
                                Quarter = this.State.SelectedQuarter.Quarter.Quarter,
                                Date = this.State.SelectedQuarterDate,
                                DimensionType = this.State.Dimension.DimensionType
                            })));
        }

        public void CreateReport2M()
        {
            var date = SelectDateWindow.SelectDate(this.Years);
            if (date == null)
                return;
            string fileName = String.Format("{0}_{1}_{2}_{3}.xls",
                DateTime.Now.ToString("yyyyMMdd_HH-mm"),
                "Отчет 2-МЭ",
                date.ReportDate.ToString("dd-MM-yyyy"),
                State.SelectedDistrict.Id
                );

            if (DialogHelper.GetFileToSave(SelectFileType.XLS, ref fileName))
            {

                var scopeDataList = Presenter.GetFederalCollection(new CalculationData()
                {
                    SelectedTnoCode = State.SelectedDistrict.Id,
                    Quarter = new QuarterInfo { Year = date.Year, Quarter = date.Quarter },
                    CalculationDate = date.Date,
                    DetailLevel = ScopeEnum.UFNS

                });

                var list =
                    scopeDataList.Where(e => e.RATING_TYPE == "1").Select(
                        i => new TnoMonitorDataInfoModel(i, IndexEnum.DiscrepancyShareInDeduction, State.SelectedScope)).ToList();

                new Report2M().Create(new Report2Model
                {
                    FileName = fileName,
                    ReportDate = date.ReportDate,
                    Data = list,
                    TypeEnum = ScopeEnum.UFNS
                });

                Presenter.SecurityLogger.ExportReportFormat2(date.Date, State.SelectedDistrict.Id);

                if (DialogHelper.ConfirmWithTitle("Отчет 2-МЭ", "Файл «{0}» сформирован. Открыть файл?", fileName))
                {
                    System.Diagnostics.Process.Start(fileName);
                }
            }
        }

        public void CreateInspectionRatingView()
        {
            Dispatcher.CurrentDispatcher.Invoke(
                new Action(
                    () =>
                        Presenter.CreateInspectionRatingView(
                            new InitStateModel
                            {
                                TnoCode = this.SelectedRatingData == null ? null : this.SelectedRatingData.TnoCode,
                                TnoName = this.SelectedRatingData == null ? null : this.SelectedRatingData.TnoName,
                                Year = this.State.SelectedQuarter.Quarter.Year,
                                Quarter = this.State.SelectedQuarter.Quarter.Quarter,
                                Date = this.State.SelectedQuarterDate
                            })));
        }

        public void CreateReportEd()
        {
            string fileName = String.Format("{0}_{1}_{2}_{3}.xls",
                DateTime.Now.ToString("yyyyMMdd_HH-mm"),
                "ОТЧЕТ_ЕЖЕНЕДЕЛЬНЫЙ",
                State.SelectedQuarterDate.ToString("dd-MM-yyyy"),
                State.SelectedDistrict.Id
                );

            if (DialogHelper.GetFileToSave(SelectFileType.XLS, ref fileName))
            {

                var scopeDataList = Presenter.GetFederalCollection(new CalculationData()
                {
                    SelectedTnoCode = State.SelectedDistrict.Id,
                    Quarter =
                        new QuarterInfo
                        {
                            Year = State.SelectedQuarter.Quarter.Year,
                            Quarter = State.SelectedQuarter.Quarter.Quarter
                        },
                    CalculationDate = State.SelectedQuarterDate,
                    DetailLevel = ScopeEnum.UFNS

                });

                var list =
                    scopeDataList.Where(e => e.RATING_TYPE == "1").Select(
                        i => new TnoMonitorDataInfoModel(i, IndexEnum.DiscrepancyShareInDeduction, State.SelectedScope))
                        .ToList();

                new Report2M().Create(new Report2Model
                {
                    FileName = fileName,
                    ReportDate = State.SelectedQuarterDate,
                    Weekly = true,
                    QuarterInfo = State.SelectedQuarter.Quarter,
                    Data = list,
                    TypeEnum = ScopeEnum.UFNS
                });

                Presenter.SecurityLogger.ExportReportDaily(State.SelectedQuarterDate, this.State.SelectedQuarter.Quarter, State.SelectedDistrict.Id);

                if (DialogHelper.ConfirmWithTitle("Отчет еженедельный", "Файл «{0}» сформирован. Открыть файл?",
                    fileName))
                {
                    System.Diagnostics.Process.Start(fileName);
                }
            }
        }

        public void RestoreState(GridViewDataControl gridView)
        {
            gridView.SortDescriptors.Clear();
            var csd = new ColumnSortDescriptor
            {
                Column = gridView.Columns["CurrentEffIndexRatingValueView"],
                SortDirection = ListSortDirection.Ascending
            };
            gridView.SortDescriptors.Add(csd);
        }

        public bool CanRestoreState(GridViewDataControl gridView)
        {
            return !gridView.IsSorted("CurrentEffIndexRatingValueView", ListSortDirection.Ascending);
        }
    }
}