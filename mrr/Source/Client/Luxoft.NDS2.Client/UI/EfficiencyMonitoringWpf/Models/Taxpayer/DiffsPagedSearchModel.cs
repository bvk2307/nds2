﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Client.UI.Base.Wpf.Model;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common;
using Luxoft.NDS2.Client.UI.MapWpf.Extensions;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Taxpayer
{
    public class DiffsPagedSearchModel
    {
        public string SelectedTnoCode { get; set; }

        public ScopeEnum DetailLevel { get; set; }

        public QuarterInfo Quarter { get; set; }

        public DateTime CalculationDate { get; set; }
        
        public int Skip { get; set; }
        
        public int Take { get; set; }

        public IEnumerable<ColumnSortModel> Order { get; set; }

        public IEnumerable<FilterSetting> Filter { get; set; }

        public bool IsIfns { get; set; }

        public bool WithTotal { get; set; }
    }
}
