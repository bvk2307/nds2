﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using CommonComponents.Utils.Async;
using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.UI.Base.Wpf.Model;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Commands;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Reports;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;
using Luxoft.NDS2.Common.Contracts.DTO.Navigator;
using Telerik.Windows.Controls;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Taxpayer
{
    public class UnprovenDetailsGridViewModel : BaseViewModel
    {
        private IEnumerable<FilterSetting> _filterState;
        private IEnumerable<ColumnSortModel> _sortState;
        private UnprovenDiffDetailsPresenter _presenter;
        private bool _isBusy;

        public UnprovenDiffDetailsViewModel ParentDataContext { get; private set; }

        public UnprovenDetailsGridViewModel(UnprovenDiffDetailsViewModel parent)
        {
            ParentDataContext = parent;
            GridDataSource = new QueriableDataSource<DiscrepancyDetail>(new List<DiscrepancyDetail>(50));
            ExtensionViewModel = new RadGridServerSideViewModel<DiscrepancyDetail>(DataProvider);
            InitSortState();
            RequestForTotal = true;
            TotalItemCount = 0;
        }

        public bool RequestForTotal { get; set; }

        public bool IsBusy
        {
            get { return this._isBusy; }
            set
            {
                if (this._isBusy != value)
                {
                    this._isBusy = value;
                    OnPropertyChanged("IsBusy");
                }
            }
        }

        private void InitSortState()
        {
            ExtensionViewModel.InitSortState(new ColumnSortModel { Name = "OpenKnpDiscrepancyAmount", SortDirection = ListSortDirection.Descending });
        }

        public void RestoreSortState()
        {
            InitSortState();
            ReloadData();
        }

        public UnprovenDiffDetailsPresenter Presenter
        {
            get { return _presenter; }
            set
            {
                _presenter = value;
                if (_presenter != null)
                {
                    ReloadData();
                }
            }
        }

        private StateModel State
        {
            get { return ParentDataContext.State; }
        }

        #region ExportExcel

        public RadGridServerSideViewModel<DiscrepancyDetail> ExtensionViewModel { get; private set; }

        public PageResult<DiscrepancyDetail> ReadFromServer(int skip, int take)
        {
            if (_presenter == null)
                return null;

            if (State.SelectedDistrict != null)
            {
                return _presenter.GetCalulatedDiscrepancy(new DiffsPagedSearchModel
                {
                    SelectedTnoCode = State.SelectedDistrict.Id,
                    Quarter = State.SelectedQuarter.Quarter,
                    CalculationDate = State.SelectedQuarterDate,
                    DetailLevel = ScopeEnum.RF_UFNS,
                    Skip = skip,
                    Take = take,
                    Order = SortState,
                    Filter = FilterState,
                    IsIfns = ParentDataContext.IsIfns,
                    WithTotal = RequestForTotal
                });
            }
            return new PageResult<DiscrepancyDetail>();
        }

        #endregion ExportExcel

        #region получение/обновление данных

        private QueriableDataSource<DiscrepancyDetail> _data;
        
        public QueriableDataSource<DiscrepancyDetail> GridDataSource
        {
            get { return _data; }
            set
            {
                _data = value;
                base.OnPropertyChanged("GridDataSource");
            }
        }

        public void ReloadData()
        {
            RequestForTotal = true;
            ExtensionViewModel.RefreshData();
        }

        private void DataProvider(PageRequestModel pageRequest)
        {
            if (_presenter == null || State.SelectedDistrict == null)
                return;

            IsBusy = true;
            if (Application.Current != null)
                Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, new ThreadStart(delegate { }));
            FilterState = pageRequest.Filters;
            SortState = pageRequest.Sorting;
            
            var data = ReadFromServer(pageRequest.PageIndex * pageRequest.PageSize, pageRequest.PageSize);
            if (RequestForTotal)
            {
                TotalItemCount = data.TotalAvailable;
                RequestForTotal = false;
            }
            ItemsCount = data.TotalMatches;
            PageIndex = pageRequest.PageIndex;
            PageSize = pageRequest.PageSize;
            PageCount = (int)Math.Ceiling((double)ItemsCount / PageSize); 
            GridDataSource.Data = data.Rows;
            IsBusy = false;
            ParentDataContext.ExportToExcelCommand.InvalidateCanExecute();
        }

        private void DataProviderAsync(PageRequestModel pageRequest)
        {
            if (_presenter == null || State.SelectedDistrict == null)
                return;

            var asyncWorker = new AsyncWorker<PageResult<DiscrepancyDetail>>();
            asyncWorker.DoWork +=
                (sender, args) =>
                {

                    FilterState = pageRequest.Filters;
                    SortState = pageRequest.Sorting;
                    args.Result = ReadFromServer(pageRequest.PageIndex * pageRequest.PageSize, pageRequest.PageSize);
                };

            asyncWorker.Complete +=
                (sender, args) =>
                {
                    Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(() =>
                    {
                        if (RequestForTotal)
                        {
                            TotalItemCount = args.Result.TotalAvailable;
                            RequestForTotal = false;
                            ParentDataContext.ExportToExcelCommand.InvalidateCanExecute();
                        }
                        ItemsCount = args.Result.TotalMatches;
                        GridDataSource.Data = args.Result.Rows;

                    }));
                    IsBusy = false;
                };

            asyncWorker.Failed += (sender, args) =>
            {
                IsBusy = false;

            };
            IsBusy = true;
            asyncWorker.Start();
        }

        #endregion получение/обновление данных

        #region Паджинация


        private int _pageSize = 50;
        public int PageSize
        {
            get { return _pageSize; }
            set
            {
                if (_pageSize != value)
                {
                    _pageSize = value;
                    OnPropertyChanged("PageSize");
                    ExtensionViewModel.PageSize = _pageSize;
                }
            }
        }

        private int _itemsCount;
        public int ItemsCount
        {
            get { return _itemsCount; }
            set
            {
                _itemsCount = value;
                OnPropertyChanged("ItemsCount");
            }
        }

        private int _totalCount;
        public int TotalItemCount
        {
            get { return _totalCount; }
            set
            {
                _totalCount = value;
                OnPropertyChanged("TotalItemCount");
            }
        }

        private int _pageIndex;

        public int PageIndex
        {
            get { return _pageIndex; }
            set
            {
                _pageIndex = value;
                OnPropertyChanged("PageIndex");
            }
        }

        private int _pageCount;

        public int PageCount
        {
            get { return _pageCount; }
            set
            {
                _pageCount = value;
                OnPropertyChanged("PagesCount");
            }
        }


        #endregion

        #region сортировка

        public IEnumerable<ColumnSortModel> SortState
        {
            get { return _sortState; }
            set
            {
                _sortState = value;
                OnPropertyChanged("SortState");
            }
        }

        #endregion

        #region фильтрация
        public IEnumerable<FilterSetting> FilterState
        {
            get { return _filterState; }
            set
            {
                _filterState = value;
                OnPropertyChanged("FilterState");
            }
        }

        #endregion

        
    }
}
