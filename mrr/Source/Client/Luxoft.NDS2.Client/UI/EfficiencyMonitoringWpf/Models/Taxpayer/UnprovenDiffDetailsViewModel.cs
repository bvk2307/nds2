﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using CommonComponents.Utils.Async;
using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.UI.Base.Wpf.Model;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Commands;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Helpers;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Reports;
using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Taxpayer
{
    public class UnprovenDiffDetailsViewModel : BaseViewModel, IClerableModel, IPagedModel
    {
        public UnprovenDiffDetailsViewModel()
        {
            State = new StateModel();
            State.PropertyChanged += State_PropertyChanged;
            Districts = new ObservableCollection<District>();
            _exportToExcelCommand = new DelegateImageExcelCommand(ExportToExcel, CanExportToExcel);
            GridViewModel = new UnprovenDetailsGridViewModel(this);
        }

        private readonly DelegateImageExcelCommand _exportToExcelCommand;
        private bool _exportToExcelCommandExecuting;
        private string _exportToExcelFileName;

        private string _headerString;
        //private DiscrepancyDetail _selectedRatingData;
        private ObservableCollection<YearModel> _years;
        private bool _isIfns;

        public UnprovenDetailsGridViewModel GridViewModel { get; private set; }

        public StateModel State { get; private set; }

        public ObservableCollection<District> Districts { get; set; }

        public Func<bool> NextPageAction { get; set; }

        public Func<int, bool> GoPageAction { get; set; }

        public Action FirstPageAction { get; set; }

        /// <summary>
        /// Список рейтингов
        /// </summary>
        public ObservableCollection<Rating> Ratings { get; set; }


        public UnprovenDiffDetailsPresenter Presenter
        {
            get { return GridViewModel.Presenter; }
            set { GridViewModel.Presenter = value; }
        }

        public bool IsIfns
        {
            get { return _isIfns; }
            set
            {
                _isIfns = value;
                OnPropertyChanged("IsIfns");
            }
        }

        public bool ExportToExcelCommandExecuting
        {
            get { return _exportToExcelCommandExecuting; }
            set
            {
                if (this._exportToExcelCommandExecuting != value)
                {
                    this._exportToExcelCommandExecuting = value;
                    OnPropertyChanged("ExportToExcelCommandExecuting");
                }
            }
        }

        public string HeaderString
        {
            get { return _headerString; }
            set
            {
                _headerString = value;
                OnPropertyChanged("HeaderString");
            }
        }

        public DelegateCommand ExportToExcelCommand
        {
            get { return _exportToExcelCommand; }
        }

        /// <summary>
        /// Список доступных кварталов
        /// </summary>      
        public ObservableCollection<YearModel> Years
        {
            get { return _years; }
            set
            {
                if (_years != null)
                {
                    _years.Clear();
                    OnPropertyChanged("Years");
                }
                _years = value;
                OnPropertyChanged("Years");
            }
        }

        //public DiscrepancyDetail SelectedRatingData
        //{
        //    get { return _selectedRatingData; }
        //    set
        //    {
        //        if (_selectedRatingData != value)
        //        {
        //            OnPropertyChanged("SelectedRatingData");
        //        }
        //    }
        //}

        public IEnumerable<SurCode> Curs { get; set; }

        void State_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            // Потому что дата поменятеся тоже
            if (e.PropertyName == "SelectedQuarter" || e.PropertyName == "SelectedYear")
                return;

            // Потому что дата поменятеся тоже
            if (e.PropertyName == "SelectedDistrict")
            {
                if (State.SelectedDistrict != null)
                {
                    HeaderString = State.SelectedDistrict.Name;
                }
            }

            GridViewModel.ReloadData();
        }


        private void ExportToExcel(object parameter)
        {
            if (ExportToExcelCommandExecuting)
            {
                if (DialogHelper.ConfirmWithTitle("Экспорт в MS Excel", "Прекратить выгрузку записей неотработанных расхождений?"))
                {
                    ExportToExcelCommandExecuting = false;
                }
            }
            else
            {

                if (this.GridViewModel.TotalItemCount > 80000)
                {
                    DialogHelper.Warning(
                        "Необходимо уменьшить объем выгружаемых записей до 80000. Измените условия фильтрации.");
                    return;
                }

                string fileName = string.Format("{0}_{1}_{2}_{3}.xls",
                    DateTime.Now.ToString("yyyyMMdd_HH-mm"),
                    "ОТЧЕТ_НЕ_ОТРАБОТАНО_РАСХОЖДЕНИЙ",
                    State.SelectedQuarterDate.ToString("dd-MM-yyyy"),
                    State.SelectedDistrict.Id
                    );
                _exportToExcelFileName = fileName;
                if (DialogHelper.GetFileToSave(SelectFileType.XLS, ref fileName))
                {
                    var exportToExcelWorker = new AsyncWorker<object>();
                    exportToExcelWorker.DoWork +=
                        (sender, args) =>
                        {
                            ExportToExcelCommandExecuting = true;
                            var data = this.GridViewModel.ReadFromServer(0, this.GridViewModel.TotalItemCount);
                            if (ExportToExcelCommandExecuting)
                            {
                                new ReportUnproven().Create(fileName, State.SelectedQuarter,
                                    State.Dimension.DimensionType,
                                    State.SelectedQuarterDate, data.Rows, Curs);
                            }
                            if (ExportToExcelCommandExecuting)
                            {
                                Presenter.SecurityLogger.ExportReportUnresolvedDiscrepancies(State.SelectedQuarterDate,
                                    State.SelectedQuarter.Quarter, State.SelectedDistrict.Id);
                            }
                        };

                    exportToExcelWorker.Complete += (sender, args) =>
                    {
                        if (ExportToExcelCommandExecuting)
                        {
                            if (DialogHelper.ConfirmWithTitle("Экспорт в MS Excel",
                                "Файл «{0}» сформирован. Открыть файл?",
                                fileName))
                            {
                                System.Diagnostics.Process.Start(fileName);
                            }
                        }
                        ExportToExcelCommandExecuting = false;
                    };
                    exportToExcelWorker.Failed += (sender, args) =>
                    {
                        ExportToExcelCommandExecuting = false;
                        DialogHelper.Error("Произошел сбой при формировании файла «{0}». Повторите операцию снова.»", fileName);
                    };
                    exportToExcelWorker.Start();
                }
            }
        }

        private bool CanExportToExcel(object parameter)
        {
            return this.GridViewModel.PageIndex > 0 || this.GridViewModel.ItemsCount > 0;
        }

        public void ControlClosed()
        {
            if (ExportToExcelCommandExecuting)
            {
                if (DialogHelper.ConfirmWithTitle("Экспорт в MS Excel",
                    "Файл «{0}» не сформирован. Прервать процесс формирования?", _exportToExcelFileName))
                {
                    ExportToExcelCommandExecuting = false;
                }
            }
        }

        public void RestoreState(GridViewDataControl gridView)
        {
            gridView.SortDescriptors.Clear();
            var csd = new ColumnSortDescriptor
            {
                Column = gridView.Columns["OpenKnpDiscrepancyAmount"],
                SortDirection = ListSortDirection.Descending
            };
            gridView.SortDescriptors.Add(csd);
            GridViewModel.ExtensionViewModel.CustomFilterChanged(gridView, false);
            GridViewModel.RestoreSortState();
            this.ExportToExcelCommand.InvalidateCanExecute();
        }

        public bool CanRestoreState(GridViewDataControl gridView)
        {
            return !gridView.IsSorted("OpenKnpDiscrepancyAmount", ListSortDirection.Descending);
        }


        public int PageIndex { get { return this.GridViewModel.PageIndex;  } }
        public int PageCount { get { return this.GridViewModel.PageCount; } }

        public ICollection PageData(int pageIndex)
        {
            return this.GridViewModel.ReadFromServer(this.GridViewModel.PageSize * pageIndex,
                this.GridViewModel.PageSize).Rows;
        }

        public bool NextPage()
        {
            if (NextPageAction != null)
                return NextPageAction();
            return false;
        }

        public void FirstPage()
        {
            //GridViewModel.ExtensionViewModel.PageIndex = 0;
            if (FirstPageAction != null)
                FirstPageAction();
        }

        public bool GoPage(int pageIndex)
        {
            if (GoPageAction != null)
                return GoPageAction(pageIndex);
            return false;
        }
    }
}
