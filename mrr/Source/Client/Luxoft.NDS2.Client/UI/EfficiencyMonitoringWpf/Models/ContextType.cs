﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models
{
    [DataContract]
    public enum ContextType
    {
        Central,
        MI_KN,
        UFNS,
        IFNS
    }
}
