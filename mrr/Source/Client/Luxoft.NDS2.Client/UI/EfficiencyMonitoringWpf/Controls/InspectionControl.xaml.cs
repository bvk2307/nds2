﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media.Animation;
using CommonComponents.Utils.Async;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Helpers;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common.TnoMonitorData;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Inspection;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.UfnsRating;
using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Controls
{
    /// <summary>
    /// Interaction logic for UfnsRatingControl.xaml
    /// </summary>
    public partial class InspectionControl
    {

        public InspectionControl()
        {
            InitializeComponent();
        }

        
        public InspectionPresenter Presenter { get; set; }


        public void InitViewModel(InitStateModel stateModel)
        {
             // Загрузим данные кварталов и их дат расчета
            var calculationDates = Presenter.GetCalculationDates();
            if (!calculationDates.Any())
            {
                Presenter._View.ShowError("Данные не найдены");
                return;
            }
            string tnoCode;
            string tnoName;
            if (stateModel == null)
            {
                tnoCode = Presenter.GetInspections().FirstOrDefault();
                tnoName = Presenter.GetSonoName(tnoCode);
            }
            else
            {
                tnoCode = stateModel.TnoCode;
                tnoName = stateModel.TnoName;
            }

            var model = new InspectionViewModel
            {
                RatingData = new ObservableCollection<TnoMonitorDataInfoModel>(),
                Districts = new ObservableCollection<District> { new District { Id = tnoCode, Name = tnoName } },
                Years = calculationDates.ToYearModel(),
                Ratings = new ObservableCollection<Rating>(Rating.GetAllRatings()),
            };
            model.State.SelectedDistrict = model.Districts.FirstOrDefault();
            if (!ModelHelper.IsMiKn(tnoCode))
            {
                model.State.Dimension.InitDimensionType(DimensionType.Mln);
            }

            if (stateModel != null)
            {
                model.State.SelectedYear = model.Years.FirstOrDefault(s => s.Year == stateModel.Year);
                if (model.State.SelectedYear != null)
                {
                    model.State.SelectedQuarter =
                        model.State.SelectedYear.Quarters.FirstOrDefault(s => s.Quarter.Quarter == stateModel.Quarter);
                }

                model.State.SelectedQuarterDate = stateModel.Date;
            }
            else
            {
                model.State.SelectedYear = model.Years.FirstOrDefault();
            }
            model.State.SelectedRating = model.Ratings.FirstOrDefault();
            model.PropertyChanged += ModelOnPropertyChanged;
            DataContext = model;
            Presenter.View.SetWindowTitle(tnoName);
            Presenter.View.SetWindowDesciption(tnoName);

            model.Presenter = this.Presenter;
        }

        private void ModelOnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            if (propertyChangedEventArgs.PropertyName == "TickerAnimated")
            {
                AnimateTicker();
            }
        }

        private void AnimateTicker()
        {
            if (DataContextModel.TickerAnimated)
            {
                var da = new DoubleAnimation
                {
                    From = 100,
                    To = -TickerLabel.ActualWidth,
                    Duration = new Duration(TimeSpan.FromSeconds(TickerLabel.ActualWidth / 45)),
                    RepeatBehavior = RepeatBehavior.Forever
                };
                TickerLabel.BeginAnimation(System.Windows.Controls.Canvas.LeftProperty, da);
            }
            else
            {
                TickerLabel.BeginAnimation(System.Windows.Controls.Canvas.LeftProperty, null);
            }
        }

        public InspectionViewModel DataContextModel
        {
            get { return this.DataContext as InspectionViewModel; }
        }

        private void UIElement_OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            this.DataContextModel.CreateUnprovenDiffDetailsView();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.DataContextModel.CreateInspectionRatingView();
        }

        private void BarSet_OnMouseUp(object sender, MouseButtonEventArgs e)
        {
            if (this.DataContextModel.ChartModel == null)
                return;

            this.DataContextModel.ChartModel.ShowBarSet = !this.DataContextModel.ChartModel.ShowBarSet;
        }

        private void LineSet_OnMouseUp(object sender, MouseButtonEventArgs e)
        {
            if (this.DataContextModel.ChartModel == null)
                return;

            this.DataContextModel.ChartModel.ShowLineSet = !this.DataContextModel.ChartModel.ShowLineSet;
        }
        
    }
}
