﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using CommonComponents.Utils.Async;
using Luxoft.NDS2.Client.UI.Base.Wpf.Model;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Helpers;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.CentralApparat;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common.TnoMonitorData;
using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Telerik.Windows;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Controls
{
    /// <summary>
    /// Interaction logic for CentralApparatControl.xaml
    /// </summary>
    public partial class CentralApparatControl
    {

        public CentralApparatControl()
        {
            InitializeComponent();
            this.RatingGrid.Items.CollectionChanged += delegate
            {
                this.RatingGrid.SelectedItems.Clear();
                if (this.DataContextModel.SelectedRatingData != null)
                {
                    this.RatingGrid.SelectedItems.Add(this.DataContextModel.SelectedRatingData);
                }
            };
            this.FindPanel.ReplaceActions.Add(new FindFieldReplaceAction<TnoMonitorDataInfoModel>
            {
                FieldName = "CurrentEffIndexRatingValueView",
                ReplaceFunc = (o) => { return o.CurrentEffIndexRatingValueView.HasValue ? o.CurrentEffIndexRatingValueView.Value.ToString() : null; }
            });
        }

        private bool _firstLoad = true;

        public CentralApparatPresenter Presenter { get; set; }


        public void InitViewModel()
        {
            // Загрузим данные кварталов и их дат расчета
            var calculationDates = Presenter.GetCalculationDates();
            if (!calculationDates.Any())
            {
                Presenter._View.ShowError("Данные не найдены");
                return;
            }

            var model = new CentralApparatViewModel
            {
                RatingData = new ObservableCollection<TnoMonitorDataInfoModel>(),
                Districts = District.GetAllDistricts(),
                Years = calculationDates.ToYearModel(),
                Ratings = new ObservableCollection<Rating>(Rating.GetAllRatings()),
            };
            model.State.SelectedDistrict = model.Districts.FirstOrDefault();

            //Выбираем год который содержит квартал содержащий данные расчитанные по агрегату Эффективности
            model.State.SelectedYear = model.Years
                                        .FirstOrDefault(y => y.Quarters.Any(q=>q.ContainsEffAgrCalculation))
                                            ?? model.Years.FirstOrDefault();
            //Выбираем квартал 
            model.State.SelectedQuarter = model.State.SelectedYear.Quarters
                                            .FirstOrDefault(q => q.ContainsEffAgrCalculation)
                                                ?? model.State.SelectedYear.Quarters.FirstOrDefault();

            model.State.SelectedRating = model.Ratings.FirstOrDefault();
            this.DataContext = model;
            model.PropertyChanged += ModelOnPropertyChanged;

            model.Presenter = this.Presenter;
        }


        private void ModelOnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            // Перегрузили таблицу
            if (propertyChangedEventArgs.PropertyName == "RatingData" && !_firstLoad)
            {
                PinnFirstRow();
            }
            if (propertyChangedEventArgs.PropertyName == "TickerAnimated")
            {
                AnimateTicker();
            }
        }

        private void AnimateTicker()
        {
            if (DataContextModel.TickerAnimated)
            {
                var da = new DoubleAnimation
                {
                    From = 100,
                    To = -TickerLabel.ActualWidth,
                    Duration = new Duration(TimeSpan.FromSeconds(TickerLabel.ActualWidth / 45)),
                    RepeatBehavior = RepeatBehavior.Forever
                };
                TickerLabel.BeginAnimation(System.Windows.Controls.Canvas.LeftProperty, da);
            }
            else
            {
                TickerLabel.BeginAnimation(System.Windows.Controls.Canvas.LeftProperty, null);
            }
        }

        private void RatingGrid_OnDataLoaded(object sender, EventArgs e)
        {
            // Только для инициализации
            if (_firstLoad)
            {
                _firstLoad = false;
                PinnFirstRow();
            }
        }

        public CentralApparatViewModel DataContextModel
        {
            get { return this.DataContext as CentralApparatViewModel; }
        }


        private void PinnFirstRow()
        {
            if (!this.RatingGrid.IsInitialized)
                return;

            var togglePinnedStateCommand = RadGridViewCommands.TogglePinnedRowState as RoutedUICommand;
            if (togglePinnedStateCommand != null && DataContextModel.FixedRatingData != null)
            {
                togglePinnedStateCommand.Execute(DataContextModel.FixedRatingData, this.RatingGrid);
            }
        }

        private void RatingGrid_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var originalSender = e.OriginalSource as FrameworkElement;
            if (originalSender != null)
            {
                var row = originalSender.ParentOfType<GridViewRow>();
                if (row != null)
                {
                    this.DataContextModel.CreateUfnsRatingView();
                }
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.DataContextModel.CreateInspectionRatingView();
        }

        private void Button_2mClick(object sender, RoutedEventArgs e)
        {
            this.DataContextModel.CreateReport2M();
        }

        private void Button_edClick(object sender, RoutedEventArgs e)
        {
            this.DataContextModel.CreateReportEd();
        }

        private void BarSet_OnMouseUp(object sender, MouseButtonEventArgs e)
        {
            if (this.DataContextModel.ChartModel == null)
                return;

            this.DataContextModel.ChartModel.ShowBarSet = !this.DataContextModel.ChartModel.ShowBarSet;
        }

        private void LineSet_OnMouseUp(object sender, MouseButtonEventArgs e)
        {
            if (this.DataContextModel.ChartModel == null)
                return;

            this.DataContextModel.ChartModel.ShowLineSet = !this.DataContextModel.ChartModel.ShowLineSet;
        }

    }
}
