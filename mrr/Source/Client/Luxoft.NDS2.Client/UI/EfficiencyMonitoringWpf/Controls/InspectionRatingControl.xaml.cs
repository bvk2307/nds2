﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using CommonComponents.Utils.Async;
using DocumentFormat.OpenXml.Spreadsheet;
using Luxoft.NDS2.Client.UI.Base.Wpf.Model;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Commands;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Converters;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Helpers;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common.TnoMonitorData;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Inspection;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.UfnsRating;
using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Controls
{
    /// <summary>
    /// Interaction logic for InspectionRatingControl.xaml
    /// </summary>
    public partial class InspectionRatingControl
    {
        public InspectionRatingControl()
        {
            InitializeComponent();
            RatingGrid.Items.CollectionChanged += Items_CollectionChanged;
        }

        private bool _searchApplied;

        public InspectionRatingPresenter Presenter { get; set; }


        public void InitViewModel(InitStateModel stateModel)
        {
             // Загрузим данные кварталов и их дат расчета
            var calculationDates = Presenter.GetCalculationDates();
            if (!calculationDates.Any() || stateModel == null)
            {
                Presenter._View.ShowError("Данные не найдены");
                return;
            }

            // модель контрол создаст!
            var model = this.DataContextModel;
            model.GridView = RatingGrid;
            model.Districts = new ObservableCollection<District>
            {
                new District {Id = stateModel.TnoCode, Name = stateModel.TnoName}
            };
            model.Years = calculationDates.ToYearModel();
            model.Ratings = new ObservableCollection<Rating>(Rating.GetAllRatings());
            model.State.SelectedDistrict = model.Districts.FirstOrDefault();
            model.State.SelectedYear = model.Years.FirstOrDefault(s => s.Year == stateModel.Year);
            if (model.State.SelectedYear != null)
            {
                model.State.SelectedQuarter =
                    model.State.SelectedYear.Quarters.FirstOrDefault(s => s.Quarter.Quarter == stateModel.Quarter);
            }
            model.State.SelectedQuarterDate = stateModel.Date;
            model.State.SelectedRating = model.Ratings.FirstOrDefault();
            model.Presenter = this.Presenter;
            InitFind();
        }


        public InspectionRatingViewModel DataContextModel
        {
            get { return this.DataContext as InspectionRatingViewModel; }
        }

        private void InitFind()
        {
            this.FindPanel.ReplaceActions.Add(new FindFieldReplaceAction<IfnsRatingByDiscrepancyShareInDeductionData>
            {
                FieldName = "Rating",
                ReplaceFunc = (o) => o.Rating.ToString()
            });
            this.FindPanel.ReplaceActions.Add(new FindFieldReplaceAction<IfnsRatingByDiscrepancyShareInDeductionData>
            {
                FieldName = "DiscrepancyAmount",
                ReplaceFunc = (o) => DimensionValueConverter.Convert(o.DiscrepancyAmount, this.DataContextModel.State.Dimension.DimensionType)
            });
            this.FindPanel.ReplaceActions.Add(new FindFieldReplaceAction<IfnsRatingByDiscrepancyShareInDeductionData>
            {
                FieldName = "DeductionAmount",
                ReplaceFunc = (o) => DimensionValueConverter.Convert(o.DeductionAmount, this.DataContextModel.State.Dimension.DimensionType)
            });
            this.FindPanel.ReplaceActions.Add(new FindFieldReplaceAction<IfnsRatingByDiscrepancyShareInDeductionData>
            {
                FieldName = "ShareValue",
                ReplaceFunc = (o) => o.ShareValue.ToString("##.## %")
            });
            
        }

        private void RatingGrid_OnFiltered(object sender, GridViewFilteredEventArgs e)
        {
            DataContextModel.ExportToExcelCommand.InvalidateCanExecute();
        }

        void Items_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            DataContextModel.ExportToExcelCommand.InvalidateCanExecute();
        }
    }
}
