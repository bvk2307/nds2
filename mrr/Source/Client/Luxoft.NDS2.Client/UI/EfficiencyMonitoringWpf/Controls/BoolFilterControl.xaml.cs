﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls.Primitives;
using Luxoft.NDS2.Client.UI.Base.Wpf.EventArgs;
using Luxoft.NDS2.Client.UI.Base.Wpf.Interfaces;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Taxpayer;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Controls
{
    /// <summary>
    /// Interaction logic for BoolFilterControl.xaml
    /// </summary>
    public partial class BoolFilterControl : IFilteringClearableControl
    {

        public BoolFilterControl()
        {
            InitializeComponent();
        }

        private GridViewCheckBoxColumn _column;
        private Telerik.Windows.Data.FilterDescriptor _eqFilter;

        public bool? Value
        {
            get { return ChYes.IsChecked.HasValue && ChYes.IsChecked.Value ? true : (ChNo.IsChecked.HasValue && ChNo.IsChecked.Value ? new bool?(false) : null); }
            set
            {
                if (value.HasValue)
                {
                    if (value.Value)
                    {
                        ChYes.IsChecked = true;
                        
                    }
                    else
                    {
                        ChNo.IsChecked = true;
                    }
                }
                else
                {
                    ChYes.IsChecked = false;
                    ChNo.IsChecked = false;
                }
            }
        }

        /// <summary>
        /// Identifies the <see cref="IsActive"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty IsActiveProperty =
            DependencyProperty.Register(
                "IsActive",
                typeof(bool),
                typeof(BoolFilterControl),
                new System.Windows.PropertyMetadata(false));

        /// <summary>
        /// Gets or sets a value indicating whether the filtering is active.
        /// </summary>
        public bool IsActive
        {
            get { return (bool) GetValue(IsActiveProperty); }
            set { SetValue(IsActiveProperty, value); }
        }

        public void Prepare(Telerik.Windows.Controls.GridViewColumn column)
        {
            this._column = column as GridViewCheckBoxColumn;
            if (this._column == null)
            {
                return;
            }

            if (this._eqFilter == null)
            {
                this.CreateFilters();
            }
            this._eqFilter.Value = Value;
        }

        private void CreateFilters()
        {
            string dataMember = this._column.DataMemberBinding.Path.Path;
            this._eqFilter = new Telerik.Windows.Data.FilterDescriptor(dataMember, FilterOperator.IsEqualTo, null, false, typeof (bool));
        }

        public void ClearFilters()
        {
            if (this._column == null)
                return;
            if (this._column.DataControl.FilterDescriptors.Contains(this._eqFilter))
            {
                this._column.DataControl.FilterDescriptors.Remove(this._eqFilter);
            }
            Value = null;
            IsActive = false;
        }

        private void OnFilter(object sender, RoutedEventArgs e)
        {
            if (this.Value == null)
            {
                if (this._column.DataControl.FilterDescriptors.Contains(this._eqFilter))
                {
                    this._column.DataControl.FilterDescriptors.Remove(this._eqFilter);
                }
                IsActive = false;
            }
            else
            {
                this._eqFilter.Value = !Value.Value;
                if (!this._column.DataControl.FilterDescriptors.Contains(this._eqFilter))
                {
                    this._column.DataControl.FilterDescriptors.Add(this._eqFilter);
                }
                IsActive = true;
            }
            ((UnprovenDiffDetailsViewModel)_column.DataContext).GridViewModel.ExtensionViewModel.CustomFilterChanged(_column.DataControl);
            Close();
        }

        private void OnClear(object sender, RoutedEventArgs e)
        {
            if (this._column == null)
                return;
            if (this._column.DataControl.FilterDescriptors.Contains(this._eqFilter))
            {
                this._column.DataControl.FilterDescriptors.Remove(this._eqFilter);
            }
            Value = null;
            IsActive = false;
            ((UnprovenDiffDetailsViewModel)_column.DataContext).GridViewModel.ExtensionViewModel.CustomFilterChanged(_column.DataControl);
        }

        private void Close()
        {
            var popup = this.ParentOfType<Popup>();
            if (popup != null)
            {
                popup.IsOpen = false;
            }
        }

        private void ChYes_OnChecked(object sender, RoutedEventArgs e)
        {
            ChNo.IsChecked = false;
        }

        private void ChNo_OnChecked(object sender, RoutedEventArgs e)
        {
            ChYes.IsChecked = false;
        }

        private void FilterCloseButton_OnClick(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
