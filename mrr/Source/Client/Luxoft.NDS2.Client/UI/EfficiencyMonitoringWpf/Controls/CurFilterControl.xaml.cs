﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Luxoft.NDS2.Client.UI.Base.Wpf.EventArgs;
using Luxoft.NDS2.Client.UI.Base.Wpf.Interfaces;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Taxpayer;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Controls
{
    /// <summary>
    /// Interaction logic for CurFilterControl.xaml
    /// </summary>
    public partial class CurFilterControl : IFilteringClearableControl
    {

        /// <summary>
        /// Identifies the <see cref="CursSourse"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty CursSourseProperty = DependencyProperty.Register("CursSourse", typeof(IEnumerable<SurCode>), typeof(CurFilterControl), new PropertyMetadata(null, null));

        //public event EventHandler<CustomFilterEventArgs> OnCustomFilterApplied;

        public CurFilterControl()
        {
            InitializeComponent();
        }

        private GridViewBoundColumnBase _column;
        private Telerik.Windows.Data.FilterDescriptor _eqFilter;

        /// <summary>
        /// Identifies the <see cref="IsActive"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty IsActiveProperty =
            DependencyProperty.Register(
                "IsActive",
                typeof(bool),
                typeof(CurFilterControl),
                new System.Windows.PropertyMetadata(false));

        /// <summary>
        /// Gets or sets a value indicating whether the filtering is active.
        /// </summary>
        public bool IsActive
        {
            get { return (bool) GetValue(IsActiveProperty); }
            set { SetValue(IsActiveProperty, value); }
        }

        public SurCode Value
        {
            get { return this.CbCur.SelectedValue as SurCode; }
            set { this.CbCur.SelectedValue = value; }
        }

        public IEnumerable<SurCode> CursSourse
        {
            get { return GetValue(CursSourseProperty) as IEnumerable<SurCode>; }
            set
            {
                SetValue(CursSourseProperty, value);
                this.CbCur.ItemsSource = value;
            }
        }

        public void Prepare(Telerik.Windows.Controls.GridViewColumn column)
        {
            this._column = column as GridViewBoundColumnBase;
            if (this._column == null)
                return;

            this.CursSourse = ((UnprovenDiffDetailsViewModel)column.DataContext).Curs;
            if (this._eqFilter == null)
            {
                this.CreateFilters();
            }

            this._eqFilter.Value = this.Value == null ? null : (this.Value.Code == 4 ? "" : this.Value.Code.ToString());
        }

        private void CreateFilters()
        {
            string dataMember = this._column.DataMemberBinding.Path.Path;
            this._eqFilter = new Telerik.Windows.Data.FilterDescriptor(dataMember, FilterOperator.IsEqualTo, null, false, typeof (string));
        }

        public void ClearFilters()
        {
            if (this._column == null)
                return;
            if (this._column.DataControl.FilterDescriptors.Contains(this._eqFilter))
            {
                this._column.DataControl.FilterDescriptors.Remove(this._eqFilter);
            }
            Value = null;
            IsActive = false;
        }


        private void OnFilter(object sender, RoutedEventArgs e)
        {
            if (this.Value == null)
            {
                if (this._column.DataControl.FilterDescriptors.Contains(this._eqFilter))
                {
                    this._column.DataControl.FilterDescriptors.Remove(this._eqFilter);
                }
                IsActive = false;
            }
            else
            {
                this._eqFilter.Value = this.Value.Code == 4 ? "" : this.Value.Code.ToString();
                if (!this._column.DataControl.FilterDescriptors.Contains(this._eqFilter))
                {
                    this._column.DataControl.FilterDescriptors.Add(this._eqFilter);
                }
                IsActive = true;
            }
            ((UnprovenDiffDetailsViewModel)_column.DataContext).GridViewModel.ExtensionViewModel.CustomFilterChanged(_column.DataControl);
            Close();
        }

        private void OnClear(object sender, RoutedEventArgs e)
        {
            if (this._column == null)
                return;
            if (this._column.DataControl.FilterDescriptors.Contains(this._eqFilter))
            {
                this._column.DataControl.FilterDescriptors.Remove(this._eqFilter);
            }
            Value = null;
            IsActive = false;
            ((UnprovenDiffDetailsViewModel)_column.DataContext).GridViewModel.ExtensionViewModel.CustomFilterChanged(_column.DataControl);
        }

        private void Close()
        {
            var popup = this.ParentOfType<Popup>();
            if (popup != null)
            {
                popup.IsOpen = false;
            }
        }

        private void FilterCloseButton_OnClick(object sender, RoutedEventArgs e)
        {
            Close();   
        }
    }
}
