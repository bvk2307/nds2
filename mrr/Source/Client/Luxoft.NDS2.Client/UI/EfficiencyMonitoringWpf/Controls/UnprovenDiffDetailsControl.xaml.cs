﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using CommonComponents.Utils.Async;
using DocumentFormat.OpenXml.Drawing.Charts;
using Luxoft.NDS2.Client.UI.Base.Wpf.Model;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Converters;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Helpers;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common.TnoMonitorData;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Taxpayer;
using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Controls
{
    /// <summary>
    /// Interaction logic for UnprovenDiffDetailsControl.xaml
    /// </summary>
    public partial class UnprovenDiffDetailsControl
    {
        public UnprovenDiffDetailsControl()
        {
            InitializeComponent();
        }

        public UnprovenDiffDetailsPresenter Presenter { get; set; }

        public void InitViewModel(InitStateModel stateModel)
        {
             // Загрузим данные кварталов и их дат расчета
            var calculationDates = Presenter.GetCalculationDates();
            if (!calculationDates.Any())
            {
                Presenter._View.ShowError("Данные не найдены");
                return;
            }

            // модель контрол создаст!
            var model = DataContextModel;
            if (!string.IsNullOrEmpty(stateModel.TnoCode))
            {
                model.Districts = new ObservableCollection<District>
                {
                    new District {Id = stateModel.TnoCode, Name = stateModel.TnoName}
                };
                model.IsIfns = !stateModel.TnoCode.EndsWith("00");
            }
            model.Years = calculationDates.ToYearModel();
            model.Ratings = new ObservableCollection<Rating>(Rating.GetAllRatings());
            model.State.SelectedDistrict = model.Districts.FirstOrDefault();
            model.State.SelectedYear = model.Years.FirstOrDefault(s => s.Year == stateModel.Year);
            if (model.State.SelectedYear != null)
            {
                model.State.SelectedQuarter =
                    model.State.SelectedYear.Quarters.FirstOrDefault(s => s.Quarter.Quarter == stateModel.Quarter);
            }
            model.State.SelectedQuarterDate = stateModel.Date;
            model.State.SelectedRating = model.Ratings.FirstOrDefault();
            model.State.Dimension.InitDimensionType(stateModel.DimensionType);
            model.Curs = Presenter.ReadCurs();
            model.Presenter = Presenter;
            model.NextPageAction = () => RatingGridPager.CanMoveToNextPage && RatingGridPager.MoveToNextPage();
            model.FirstPageAction = () => RatingGridPager.MoveToFirstPage();
            model.GoPageAction = (index) => RatingGridPager.MoveToPage(index);
            model.GridViewModel.PropertyChanged += GridViewModel_PropertyChanged;
            InitFind();
        }

        void GridViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "PageSize")
            {
                RatingGridPager.PageSize = DataContextModel.GridViewModel.PageSize;
            }
        }

        public UnprovenDiffDetailsViewModel DataContextModel
        {
            get { return this.DataContext as UnprovenDiffDetailsViewModel; }
        }

        private void InitFind()
        {
            this.FindPanel.ReplaceActions.Add(new FindFieldReplaceAction<DiscrepancyDetail>
            {
                FieldName = "SurCode",
                ReplaceFunc = (o) => CurColumnTextConverter.ConvertDirect(o.SurCode, this.DataContextModel.Curs)
            });
            this.FindPanel.ReplaceActions.Add(new FindFieldReplaceAction<DiscrepancyDetail>
            {
                FieldName = "NdsSum",
                ReplaceFunc = (o) => DimensionValueConverter.Convert(o.NdsSum, this.DataContextModel.State.Dimension.DimensionType)
            });
            this.FindPanel.ReplaceActions.Add(new FindFieldReplaceAction<DiscrepancyDetail>
            {
                FieldName = "OpenKnpDiscrepancyAmount",
                ReplaceFunc = (o) => DimensionValueConverter.Convert(o.OpenKnpDiscrepancyAmount, this.DataContextModel.State.Dimension.DimensionType)
            });
            this.FindPanel.ReplaceActions.Add(new FindFieldReplaceAction<DiscrepancyDetail>
            {
                FieldName = "OpenKnpGapDiscrepancyAmount",
                ReplaceFunc = (o) => DimensionValueConverter.Convert(o.OpenKnpGapDiscrepancyAmount, this.DataContextModel.State.Dimension.DimensionType)
            });
            this.FindPanel.ReplaceActions.Add(new FindFieldReplaceAction<DiscrepancyDetail>
            {
                FieldName = "OpenKnpNdsDiscrepancyAmount",
                ReplaceFunc = (o) => DimensionValueConverter.Convert(o.OpenKnpNdsDiscrepancyAmount, this.DataContextModel.State.Dimension.DimensionType)
            });
            this.FindPanel.SearchColumns = new List<string> { "SonoCode", "SonoName", "Inn", "TaxPayerName" };
        }
    }
}
