﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common.TnoMonitorData;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Inspection;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.UfnsRating;
using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Controls
{
    /// <summary>
    /// Interaction logic for InspectionMiknTableControl.xaml
    /// </summary>
    public partial class InspectionMiknTableControl
    {

        public InspectionMiknTableControl()
        {
            InitializeComponent();
        }
        
        public InspectionViewModel DataContextModel
        {
            get { return this.DataContext as InspectionViewModel; }
        }

        private void UIElement1_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DataContextModel.State.SelectedRating =
                DataContextModel.Ratings.SingleOrDefault(s => s.Id == IndexEnum.DiscrepancyShareInDeduction);
        }

        private void UIElement2_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DataContextModel.State.SelectedRating =
                DataContextModel.Ratings.SingleOrDefault(s => s.Id == IndexEnum.ClosedDiscrepancyShareByAmount);
        }

        private void UIElement3_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DataContextModel.State.SelectedRating =
                DataContextModel.Ratings.SingleOrDefault(s => s.Id == IndexEnum.ClosedDiscrepancyShareByQuantity);
        }

        private void UIElement4_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DataContextModel.State.SelectedRating =
                DataContextModel.Ratings.SingleOrDefault(s => s.Id == IndexEnum.DeclarationWithDiscrepancyShareByQuantity);
        }
    }
}
