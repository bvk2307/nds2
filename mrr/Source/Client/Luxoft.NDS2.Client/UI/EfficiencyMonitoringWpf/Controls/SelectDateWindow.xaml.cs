﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Report;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Controls
{
    /// <summary>
    /// Interaction logic for SelectDateWindow.xaml
    /// </summary>
    public partial class SelectDateWindow
    {
        public SelectDateWindow()
        {
            InitializeComponent();
            DataContext = new SelectDateModel();
        }

        public static QuarterReportItem SelectDate(IEnumerable<YearModel> years)
        {
            var quarters = years.SelectMany(s => s.Quarters).Select(q =>
                        new QuarterReportItem
                        {
                            Quarter = q.Quarter.Quarter,
                            ReportDate = q.Quarter.ToReportDate(),
                            Year = q.Quarter.Year,
                            Date = q.Dates.FirstOrDefault(d => d >= q.Quarter.ToReportDate())
                        }).OrderBy(d => d.ReportDate).Where(s=>s.ReportDate <= DateTime.Today);
            
            var selectModel = quarters.GroupBy(s=>s.ReportDate.Year).OrderByDescending(s => s.Key).Select(s => new YearReportItem
            {
                Year = s.Key,
                Quarters = new ObservableCollection<QuarterReportItem>(s)
            });
            return SelectDate(selectModel);
        }

        public static QuarterReportItem SelectDate(IEnumerable<YearReportItem> list)
        {
            var window = new SelectDateWindow();
            window.DataContextModel.Years = new ObservableCollection<YearReportItem>(list);
            if (window.ShowDialog() == true)
            {
                if (window.DataContextModel.SelectedYear != null)
                    return window.DataContextModel.SelectedYear.SelectedQuarter;
            }
            return null;
        }

        public SelectDateModel DataContextModel
        {
            get { return (SelectDateModel) this.DataContext; }
        }

        private void Button_OkClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void Button_CancelClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
