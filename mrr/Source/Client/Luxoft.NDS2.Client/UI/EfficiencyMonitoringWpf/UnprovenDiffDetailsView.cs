﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Controls;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Helpers;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common;
using Microsoft.Practices.ObjectBuilder;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using Luxoft.NDS2.Client.UI.Base;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf
{
    public partial class UnprovenDiffDetailsView : BaseView
    {
        private UnprovenDiffDetailsPresenter _presenter;
        private ElementHost _ctrlHost;
        private InitStateModel _model;
        private UnprovenDiffDetailsControl _unprovenDiffDetailsControl;

        public UnprovenDiffDetailsView(PresentationContext context)
        {
            _presentationContext = context;
            InitializeComponent();
            context.WindowTitle = "Детализация неотработанных расхождений (по налогоплательщикам)";
            context.WindowDescription = "Детализация неотработанных расхождений (по налогоплательщикам)";
        }

        public UnprovenDiffDetailsView(PresentationContext context, InitStateModel model)
            : this(context)
        {
            _model = model;
       }

       [CreateNew]
        public UnprovenDiffDetailsPresenter Presenter
        {
            set
            {
                _presenter = value;
                _presenter.PresentationContext = _presentationContext;
                _presenter.View = this;
                WorkItem = _presenter.WorkItem;
                _presenter.OnObjectCreated();
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (!DesignMode)
            {
                ResourceHelper.EnsureApplicationResources();
                _ctrlHost = new ElementHost {Visible = false};
                this.Controls.Add(_ctrlHost);

                _unprovenDiffDetailsControl = new UnprovenDiffDetailsControl{Presenter = _presenter};
                _unprovenDiffDetailsControl.InitViewModel(_model);
                _ctrlHost.Child = _unprovenDiffDetailsControl;
            }
        }

        public override void OnClosed(WindowCloseContextBase closeContext)
        {
            base.OnClosed(closeContext);
            if (_unprovenDiffDetailsControl != null && _unprovenDiffDetailsControl.DataContextModel != null)
            {
                _unprovenDiffDetailsControl.DataContextModel.ControlClosed();
            }
        }

        public override void OnActivate()
        {
            if (!_ctrlHost.Visible)
            {
                Application.DoEvents();
                _ctrlHost.Size = this.Size;
                _ctrlHost.Dock = DockStyle.Fill;
                _ctrlHost.Visible = true;
            }
        }
    }
}
