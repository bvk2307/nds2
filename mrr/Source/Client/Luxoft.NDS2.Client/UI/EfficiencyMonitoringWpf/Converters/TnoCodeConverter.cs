﻿using System;
using System.Windows.Data;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Helpers;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Converters
{
    class TnoCodeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var tnoCode = value.ToString();
            if (ModelHelper.IsRf(tnoCode) || 
                    ModelHelper.IsMiKnDistrict(tnoCode))
                return String.Empty;
            return tnoCode;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
