﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common.TnoMonitorData;
using Luxoft.NDS2.Client.UI.MapWpf.Models;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Map;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Converters
{
    public class CurColumnTextConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values[1] != DependencyProperty.UnsetValue)
            {
                var cyrs = (IEnumerable<SurCode>)values[1];
                if (values[0] != DependencyProperty.UnsetValue)
                {
                    return ConvertDirect((string) values[0], cyrs);
                }
            }
            return string.Empty;
        }

        public static string ConvertDirect(string code, IEnumerable<SurCode> items)
        {
            var c = items.SingleOrDefault(s => s.Code.ToString() == code);
            if (c != null) return c.Description;

            var cc = items.SingleOrDefault(s => s.IsDefault);
            if (cc != null) return cc.Description;
            return string.Empty;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
