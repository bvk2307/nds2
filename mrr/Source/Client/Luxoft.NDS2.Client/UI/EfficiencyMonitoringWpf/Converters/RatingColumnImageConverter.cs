﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common.TnoMonitorData;
using Luxoft.NDS2.Client.UI.MapWpf.Models;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Map;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Converters
{
    public class RatingColumnImageConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
                return null;
            
            var origValue = ((TnoMonitorDataInfoModel)value).CurrentEffIndexRatingValueView;
            var prevValue = ((TnoMonitorDataInfoModel)value).CurrentEffIndexRatingValuePrev;
            if (origValue.HasValue && !prevValue.HasValue)
            {
                return null;
            }
            if (origValue < prevValue)
            {
                return new BitmapImage(new Uri("/Luxoft.NDS2.Client;component/Resources/green_up.png", UriKind.Relative));
            }
            if (origValue > prevValue)
            {
                return new BitmapImage(new Uri("/Luxoft.NDS2.Client;component/Resources/red_down.png", UriKind.Relative));
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
