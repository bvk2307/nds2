﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common.TnoMonitorData;
using Luxoft.NDS2.Client.UI.MapWpf.Models;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Map;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Converters
{
    public class RatingColumnPlusValueConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
                return null;
            
            var origValue = ((TnoMonitorDataInfoModel)value).CurrentEffIndexRatingValueView;
            var prevValue = ((TnoMonitorDataInfoModel)value).CurrentEffIndexRatingValuePrev;
            if (origValue.HasValue && !prevValue.HasValue)
            {
                return null;
            }
            if (origValue > prevValue)
            {
                return string.Format("- {0}", Math.Abs((decimal)prevValue - (decimal)origValue));
            }
            if (origValue < prevValue)
            {
                return string.Format("+ {0}", Math.Abs((decimal)prevValue - (decimal)origValue));
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
