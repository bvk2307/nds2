﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common.TnoMonitorData;
using Luxoft.NDS2.Client.UI.MapWpf.Models;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Map;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Converters
{
    public class CurColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null)
            {
                var color = CurColumnColorConverter.FromArgb((int) value);
                return new SolidColorBrush(color);
            }
            return new SolidColorBrush(Colors.Silver);
        }


        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
