﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Converters
{
    public class InspectionRatingColumnPlusValueConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
                return null;

            var origValue = ((IfnsRatingByDiscrepancyShareInDeductionData)value).Rating;
            var prevValue = ((IfnsRatingByDiscrepancyShareInDeductionData)value).RatingPrev;
            if (!prevValue.HasValue)
            {
                return null;
            }

            if (origValue > prevValue)
            {
                return string.Format("- {0}", Math.Abs((int)prevValue - origValue));
            }
            if (origValue < prevValue)
            {
                return string.Format("+ {0}", Math.Abs((int)prevValue - origValue));
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
