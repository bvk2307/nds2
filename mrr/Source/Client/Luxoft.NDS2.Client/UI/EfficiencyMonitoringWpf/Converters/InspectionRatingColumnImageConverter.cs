﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common.TnoMonitorData;
using Luxoft.NDS2.Client.UI.MapWpf.Models;
using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Map;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Converters
{
    public class InspectionRatingColumnImageConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
                return null;

            var origValue = ((IfnsRatingByDiscrepancyShareInDeductionData)value).Rating;
            var prevValue = ((IfnsRatingByDiscrepancyShareInDeductionData)value).RatingPrev;
            if (!prevValue.HasValue)
            {
                return null;
            }
            if (origValue < prevValue)
            {
                return new BitmapImage(new Uri("/Luxoft.NDS2.Client;component/Resources/green_up.png", UriKind.Relative));
            }
            if (origValue > prevValue)
            {
                return new BitmapImage(new Uri("/Luxoft.NDS2.Client;component/Resources/red_down.png", UriKind.Relative));
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
