﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Navigation;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Helpers;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common.TnoMonitorData;
using Luxoft.NDS2.Client.UI.MapWpf.Models;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Map;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Converters
{
    public class IsNotMiknVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null || value == DependencyProperty.UnsetValue)
                return Visibility.Collapsed;
            var item = value as TnoMonitorDataInfoModel;
            if (item == null)
                return Visibility.Collapsed;

            return !ModelHelper.IsMiKn(item.TnoCode) ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
