﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common;
using Luxoft.NDS2.Client.UI.MapWpf.Models;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Map;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Converters
{
    public class DimensionValueConverter : IMultiValueConverter
    {
        static DimensionValueConverter()
        {
            var nfi = (NumberFormatInfo)CultureInfo.InvariantCulture.NumberFormat.Clone();
            nfi.NumberGroupSeparator = " ";
            FORMAT = nfi;
        }

        private static readonly NumberFormatInfo FORMAT;

        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            decimal value = 0;
            if (values[1] != DependencyProperty.UnsetValue)
            {
                value = (decimal)values[1];    
            }
            if (values[0] != DependencyProperty.UnsetValue)
            {
                return Convert(value, (DimensionType)values[0]);
            }
            return value;
        }

        public static string Convert(decimal value, DimensionType type)
        {
            switch (type)
            {
                case DimensionType.Mlrd:
                    return Math.Round(value / 1000000000, MidpointRounding.AwayFromZero).ToString("N0", FORMAT);
                case DimensionType.Mln:
                    return Math.Round(value / 1000000, MidpointRounding.AwayFromZero).ToString("N0", FORMAT);
                case DimensionType.Ts:
                    return Math.Round(value / 1000, MidpointRounding.AwayFromZero).ToString("N0", FORMAT);
            }
            return string.Empty;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
