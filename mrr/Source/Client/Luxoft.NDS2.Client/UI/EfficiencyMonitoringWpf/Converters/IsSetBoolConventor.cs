﻿using System;
using System.Windows;
using System.Windows.Data;

namespace Luxoft.NDS2.Client.UI.Base.Wpf.Conveters
{
    public class IsSetBoolConventor : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value != null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
