﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common.TnoMonitorData;
using Luxoft.NDS2.Client.UI.MapWpf.Models;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Map;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Converters
{
    public class CurColumnColorConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values[1] != DependencyProperty.UnsetValue)
            {
                var cyrs = (IEnumerable<SurCode>)values[1];    
                if (values[0] != DependencyProperty.UnsetValue)
                {
                    var strValue = (string) values[0];
                    var c = cyrs.SingleOrDefault(s => s.Code.ToString() == strValue);
                    if (c != null) return new SolidColorBrush(FromArgb(c.ColorARGB));
                }
                var cc = cyrs.SingleOrDefault(s => s.IsDefault);
                if (cc != null) return new SolidColorBrush(FromArgb(cc.ColorARGB));
            }
            return new SolidColorBrush(Colors.Silver);
        }


        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public static Color FromColor(System.Drawing.Color drawingColor)
        {
            Color resultColor = Color.FromArgb(drawingColor.A, drawingColor.R, drawingColor.G, drawingColor.B);

            return resultColor;
        }

        public static Color FromArgb(int colorArgb)
        {
            Color resultColor = FromColor(System.Drawing.Color.FromArgb(colorArgb));

            return resultColor;
        }
    }
}
