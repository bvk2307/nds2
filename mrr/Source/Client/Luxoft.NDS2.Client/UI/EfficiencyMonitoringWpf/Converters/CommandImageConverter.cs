﻿using System;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Commands;

namespace Luxoft.NDS2.Client.UI.Base.Wpf.Conveters
{
    public class CommandImageConverter  : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values[0] != DependencyProperty.UnsetValue && values[1] != DependencyProperty.UnsetValue)
            {
                var origValue = (DelegateImageCommand)values[0];
                if ((bool)values[1])
                {
                    return new BitmapImage(new Uri(origValue.ImageStopUri, UriKind.Relative));
                }
                else
                {
                    return new BitmapImage(new Uri(origValue.ImageUri, UriKind.Relative));
                }
            }
            return null;
        }


        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
