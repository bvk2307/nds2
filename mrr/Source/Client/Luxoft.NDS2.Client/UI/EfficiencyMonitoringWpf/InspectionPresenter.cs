﻿using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;
using Luxoft.NDS2.Common.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Common.Contract.DTO.SearchEntity;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Microsoft.Practices.CompositeUI;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf
{
    public class InspectionPresenter : BaseMonitoringPresenter<InspectionView>
    {

        public override Dictionary<string, List<TnoRatingInfo>> GetQuartersRatings(ChartData request)
        {
            var ret = new Dictionary<string, List<TnoRatingInfo>>();

            ExecuteServiceCall(
                () => Service.GetAllQuartersRatings(request),
                result =>
                {
                    ret = result.Result;
                },
                resultErr => View.ShowError(resultErr.Message));

            return ret;
        }
        

        public override List<DateTime> GetDateList(QuarterInfo quarter)
        {
            var ret = new List<DateTime>();
            ExecuteServiceCall(
                () => Service.GetEnabledDates(quarter.Year, quarter.Quarter),
                result =>
                {
                    ret.AddRange(result.Result);
                },
                resultErr =>
                {
                });
            return ret;
        }

        public List<string> GetInspections()
        {
            return UserRoles.Where(ur => ur.PermType == CommonComponents.Security.Authorization.PermissionType.Operation

                        && ur.StructContext != "0000" // Не ЦА
                        && !ur.StructContext.EndsWith("00") // Не УФНС
                        && ur.StructContext != "9962" // Не МИ по КК
                )
                .Select(ur => ur.StructContext).ToList();
        }

        public void CreateUnprovenDiffDetailsView(InitStateModel model)
        {
            var winManager = WorkItem.Services.Get<IWindowsManagerService>();
            var viewId = Guid.NewGuid();
            var featureId = Guid.NewGuid();
            var workItemAdded = WorkItem.WorkItems.AddNew<WorkItem>(Guid.NewGuid().ToString());
            var featureContext = new FeatureContextBase(featureId);
            var presentationContext = new PresentationContext(viewId, null, featureContext)
            {
                WindowTitle = "Не обработано расхождений",
                WindowDescription = "Не обработано расхождений 1"
            };


            var view =
                winManager.AddNewSmartPart<UnprovenDiffDetailsView>(
                    workItemAdded,
                    viewId.ToString(),
                    presentationContext,
                    model);

            winManager.Show(presentationContext, workItemAdded, view);
        }

        public void CreateInspectionRatingView(InitStateModel model)
        {
            var winManager = WorkItem.Services.Get<IWindowsManagerService>();
            var viewId = Guid.NewGuid();
            var featureId = Guid.NewGuid();
            var workItemAdded = WorkItem.WorkItems.AddNew<WorkItem>(Guid.NewGuid().ToString());
            var featureContext = new FeatureContextBase(featureId);
            var presentationContext = new PresentationContext(viewId, null, featureContext);


            var view =
                winManager.AddNewSmartPart<InspectionRatingView>(
                    workItemAdded,
                    viewId.ToString(),
                    presentationContext,
                    model);

            winManager.Show(presentationContext, workItemAdded, view);
        }
    }
}
