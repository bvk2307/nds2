﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using CommonComponents.Communication;
using CommonComponents.Security.Authorization;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Controls;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Helpers;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.Services;
using Microsoft.Practices.ObjectBuilder;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using Luxoft.NDS2.Client.UI.Base;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf
{
    public partial class UfnsRatingView : BaseView
    {
        private UfnsRatingPresenter _presenter;
        private ElementHost _ctrlHost;
        private InitStateModel _model;

        public UfnsRatingView(PresentationContext context)
        {
            _presentationContext = context;
            InitializeComponent();
            context.WindowTitle = "УФНС";
            context.WindowDescription = "Окно управления";
        }

        public UfnsRatingView(PresentationContext context, InitStateModel model)
            : this(context)
        {
            _model = model;
       }

       [CreateNew]
        public UfnsRatingPresenter Presenter
        {
            set
            {
                _presenter = value;
                _presenter.PresentationContext = _presentationContext;
                _presenter.View = this;
                WorkItem = _presenter.WorkItem;
                _presenter.OnObjectCreated();
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (!DesignMode)
            {
                ResourceHelper.EnsureApplicationResources();
                _ctrlHost = new ElementHost {Visible = false};
                this.Controls.Add(_ctrlHost);

                var control = new UfnsRatingControl {Presenter = this._presenter};
                control.InitViewModel(_model);
                _ctrlHost.Child = control;
            }
        }

        public override void OnActivate()
        {
            if (!_ctrlHost.Visible)
            {
                Application.DoEvents();
                _ctrlHost.Size = this.Size;
                _ctrlHost.Dock = DockStyle.Fill;
                _ctrlHost.Visible = true;
            }
        }
    }
}
