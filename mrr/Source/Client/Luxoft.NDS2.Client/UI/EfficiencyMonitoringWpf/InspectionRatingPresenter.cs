﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;
using Luxoft.NDS2.Common.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Common.Contract.DTO.SearchEntity;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf
{
    public class InspectionRatingPresenter : BaseMonitoringPresenter<InspectionRatingView>
    {

        public override Dictionary<string, List<TnoRatingInfo>> GetQuartersRatings(ChartData request)
        {
            var ret = new Dictionary<string, List<TnoRatingInfo>>();

            ExecuteServiceCall(
                () => Service.GetAllQuartersRatings(request),
                result =>
                {
                    ret = result.Result;
                },
                resultErr => View.ShowError(resultErr.Message));

            return ret;
        }


        public List<IfnsRatingByDiscrepancyShareInDeductionData> GetIfnsRatingsByDiscrepancyShareInDeduction(CalculationData requestModel)
        {
            var ret = new List<IfnsRatingByDiscrepancyShareInDeductionData>();

            ExecuteServiceCall(
                () => Service.GetIfnsRatingByDiscrepancyShareInDeduction(requestModel.Quarter.Year, requestModel.Quarter.Quarter, requestModel.CalculationDate),
                result =>
                {
                    ret = result.Result;
                },
                resultErr => View.ShowError(resultErr.Message));

            return ret;
        }
        

        public override List<DateTime> GetDateList(QuarterInfo quarter)
        {
            var ret = new List<DateTime>();
            ExecuteServiceCall(
                () => Service.GetEnabledDates(quarter.Year, quarter.Quarter),
                result => ret.AddRange(result.Result),
                resultErr =>
                {
                });
            return ret;
        }

        public List<string> GetInspections()
        {
            return UserRoles.Where(ur => ur.PermType == CommonComponents.Security.Authorization.PermissionType.Operation

                        && ur.StructContext != "0000" // Не ЦА
                        && !ur.StructContext.EndsWith("00") // Не УФНС
                        && ur.StructContext != "9962" // Не МИ по КК
                )
                .Select(ur => ur.StructContext).ToList();
        }

    }
}
