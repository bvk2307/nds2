﻿using System;
using System.Threading;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Controls;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Helpers;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common;
using Microsoft.Practices.ObjectBuilder;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using Luxoft.NDS2.Client.UI.Base;
using Application = System.Windows.Forms.Application;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf
{
    public partial class InspectionRatingView : BaseView
    {
        private InspectionRatingPresenter _presenter;
        private ElementHost _ctrlHost;
        private InitStateModel _model;
        private InspectionRatingControl _inspectionRatingControl;

        public InspectionRatingView(PresentationContext context)
        {
            _presentationContext = context;
            InitializeComponent();
            context.WindowTitle = "Рейтинг ИФНС по показателю доля расхождений в сумме вычетов";
            context.WindowDescription = "Рейтинг ИФНС по показателю доля расхождений в сумме вычетов";
        }

        public InspectionRatingView(PresentationContext context, InitStateModel model)
            : this(context)
        {
            _model = model;
       }

       [CreateNew]
        public InspectionRatingPresenter Presenter
        {
            set
            {
                _presenter = value;
                _presenter.PresentationContext = _presentationContext;
                _presenter.View = this;
                WorkItem = _presenter.WorkItem;
                _presenter.OnObjectCreated();
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (!DesignMode)
            {
                ResourceHelper.EnsureApplicationResources();
                _ctrlHost = new ElementHost { Visible = false };
                _inspectionRatingControl = new InspectionRatingControl { Presenter = this._presenter};
                _inspectionRatingControl.InitViewModel(_model);
                _ctrlHost.Child = _inspectionRatingControl;
                this.Controls.Add(_ctrlHost);
            }
        }

        public override void OnClosed(WindowCloseContextBase closeContext)
        {
            base.OnClosed(closeContext);
            if (_inspectionRatingControl != null && _inspectionRatingControl.DataContextModel != null)
            {
                _inspectionRatingControl.DataContextModel.ControlClosed();
            }
        }

        public override void OnActivate()
        {
            if (!_ctrlHost.Visible)
            {
                Application.DoEvents();
                _ctrlHost.Size = this.Size;
                _ctrlHost.Dock = DockStyle.Fill;
                _ctrlHost.Visible = true;
            }
        }
    }
}
