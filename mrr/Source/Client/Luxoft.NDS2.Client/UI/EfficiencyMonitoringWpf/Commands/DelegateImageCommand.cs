﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using CommonComponents.Utils.Async;
using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;
using Telerik.Windows.Controls;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Commands
{
    public class DelegateImageCommand: DelegateCommand
    {
        public DelegateImageCommand(Action<object> execute, Predicate<object> canExecute)
            : base(execute, canExecute)
        {
        }

        
        public string ImageUri { get; set; }

        public string ImageStopUri { get; set; }

        
    }
}
