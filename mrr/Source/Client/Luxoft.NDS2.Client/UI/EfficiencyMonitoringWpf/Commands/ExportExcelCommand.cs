﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2;
using Microsoft.Win32;
using Telerik.Windows.Controls;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Commands
{
    public class ExportExcelCommand: ICommand
    {
        public ExportExcelCommand(RadGridView gridView)
        {
            _gridView = gridView;
        }

        private readonly RadGridView _gridView;

        public void Execute(object parameter)
        {
            string extension = "xls";
            var dialog = new SaveFileDialog()
            {
                DefaultExt = extension,
                Filter = String.Format("{1} files (*.{0})|*.{0}|All files (*.*)|*.*", extension, "Excel"),
                FilterIndex = 1
            };
            if (dialog.ShowDialog() == true)
            {
                using (Stream stream = dialog.OpenFile())
                {
                    _gridView.Export(stream,
                     new GridViewExportOptions()
                     {
                         Format = ExportFormat.ExcelML,
                         ShowColumnHeaders = true,
                         ShowColumnFooters = true,
                         ShowGroupFooters = false,
                     });
                }
            }
        }

        public bool CanExecute(object parameter)
        {
            return _gridView != null;
        }

        public event EventHandler CanExecuteChanged;
    }
}
