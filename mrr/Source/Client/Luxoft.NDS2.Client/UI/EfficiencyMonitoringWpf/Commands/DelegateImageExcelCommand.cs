﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Telerik.Windows.Controls;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Commands
{
    public class DelegateImageExcelCommand : DelegateImageCommand
    {
        public DelegateImageExcelCommand(Action<object> execute, Predicate<object> canExecute)
            : base(execute, canExecute)
        {
            ImageUri = "/Luxoft.NDS2.Client;component/Resources/excel.png";
            ImageStopUri = "/Luxoft.NDS2.Client;component/Resources/reset.png";
        }


        
    }
}
