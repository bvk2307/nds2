﻿using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;
using Luxoft.NDS2.Common.Contracts.Services;
using Microsoft.Practices.CompositeUI;
using System;
using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf
{
    public class CentralApparatPresenter : BaseMonitoringPresenter<CentralApparatView>
    {

        //public PageResult<DiscrepancyDetail> GetDiscrepancyDetail(QueryConditions request)
        //{
        //    var ret = new PageResult<DiscrepancyDetail>();

        //    ExecuteServiceCall(
        //        () => Service.GetDiscrepancyDetails(request, null),
        //        result =>
        //        {
        //            ret = result.Result;
        //        },
        //        resultErr =>
        //        {
        //            View.ShowError(resultErr.Message);
        //        });

        //    return ret;
        //}

        public override List<DateTime> GetDateList(QuarterInfo quarter)
        {
            var ret = new List<DateTime>();
            ExecuteServiceCall(
                () => Service.GetEnabledDates(quarter.Year, quarter.Quarter),
                result =>
                {
                    ret.AddRange(result.Result.OrderBy(q => q));
                },
                resultErr =>
                {
                });
            return ret;
        }

        public override Dictionary<string, List<TnoRatingInfo>> GetQuartersRatings(ChartData request)
        {
            var ret = new Dictionary<string, List<TnoRatingInfo>>();

            ExecuteServiceCall(
                () => Service.GetAllQuartersRatings(request),
                result =>
                {
                    ret = result.Result;
                },
                resultErr =>
                {
                    View.ShowError(resultErr.Message);
                });

            return ret;
        }

        public void CreateUfnsRatingView(InitStateModel model)
        {
            var winManager = WorkItem.Services.Get<IWindowsManagerService>();
            var viewId = Guid.NewGuid();
            var featureId = Guid.NewGuid();
            var workItem = WorkItem.WorkItems.AddNew<WorkItem>(Guid.NewGuid().ToString());
            var featureContext = new FeatureContextBase(featureId);
            var presentationContext = new PresentationContext(viewId, null, featureContext)
            {
                WindowTitle = "УФНС",
                WindowDescription = "Окно управления"
            };

            var view =
                winManager.AddNewSmartPart<UfnsRatingView>(
                    workItem,
                    viewId.ToString(),
                    presentationContext,
                    model);
            winManager.Show(presentationContext, workItem, view);
        }

        public void CreateInspectionView(InitStateModel model)
        {
            var winManager = WorkItem.Services.Get<IWindowsManagerService>();
            var viewId = Guid.NewGuid();
            var featureId = Guid.NewGuid();
            var workItemAdded = WorkItem.WorkItems.AddNew<WorkItem>(Guid.NewGuid().ToString());
            var featureContext = new FeatureContextBase(featureId);
            var presentationContext = new PresentationContext(viewId, null, featureContext);

            var view =
                winManager.AddNewSmartPart<InspectionView>(
                    workItemAdded,
                    viewId.ToString(),
                    presentationContext,
                    model);

            winManager.Show(presentationContext, workItemAdded, view);
        }

        public void CreateInspectionRatingView(InitStateModel model)
        {
            var winManager = WorkItem.Services.Get<IWindowsManagerService>();
            var viewId = Guid.NewGuid();
            var featureId = Guid.NewGuid();
            var workItemAdded = WorkItem.WorkItems.AddNew<WorkItem>(Guid.NewGuid().ToString());
            var featureContext = new FeatureContextBase(featureId);
            var presentationContext = new PresentationContext(viewId, null, featureContext);


            var view =
                winManager.AddNewSmartPart<InspectionRatingView>(
                    workItemAdded,
                    viewId.ToString(),
                    presentationContext,
                    model);

            winManager.Show(presentationContext, workItemAdded, view);
        }
    }
}
