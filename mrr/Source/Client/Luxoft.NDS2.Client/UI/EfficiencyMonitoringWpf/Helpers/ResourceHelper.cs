﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Luxoft.NDS2.Client.UI.Base.Wpf;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Helpers
{
    public static class ResourceHelper
    {

        private static bool _loaded = false;

        public static void EnsureApplicationResources()
        {
            if (!_loaded)
            {
                TelerikLoader.Init();
                // merge in your application resources
                System.Windows.Application.Current.Resources.MergedDictionaries.Add(
                    System.Windows.Application.LoadComponent(
                        new Uri("/Luxoft.NDS2.Client;component/UI/EfficiencyMonitoringWpf/Styles/Settings.xaml",
                            UriKind.Relative)) as ResourceDictionary);
                _loaded = true;
            }
        }
    }
}
