﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using CommonComponents.Directory;
using CommonComponents.Security.SecurityLogger;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.Security;
using Luxoft.NDS2.Common.Contracts.Services;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Helpers
{
    public class SecurityLogger
    {
        private ISecurityLoggerService _loggerService;
        private IUserInfoService _userInfoService;
        private ISecurityService _securityService;
        private const string _dateTimeStringMask = "dd.MM.yyyy HH:mm:ss";
        private const string _dateStringMask = "dd.MM.yyyy";

        internal SecurityLogger(ISecurityLoggerService service, IUserInfoService userInfoService,
            ISecurityService securityService)
        {
            Contract.Ensures(service != null, "Сервис лоигрования санкционарованных действий не проинициализирован");
            Contract.Ensures(userInfoService != null, "Сервис информации о пользователе не проинициализирован");
            Contract.Ensures(securityService != null, "Сервис прав доступа не проинициализирован");

            _loggerService = service;
            _userInfoService = userInfoService;
            _securityService = securityService;
        }



        private string CurrentUserSid
        {
            get
            {
                try
                {
                    return _userInfoService.GetCurrentUserInfo().LogonName; 
                }
                catch (Exception ex)
                {
                    return ex.Message;
                }
            }
        }

        private string CurrentIPAddress
        {

            get
            {
                try
                {
                    return string.Join(", ",
                        Array.FindAll(Dns.GetHostEntry(string.Empty).AddressList,
                            a => a.AddressFamily == AddressFamily.InterNetwork).Select(i => i.ToString()));

                }
                catch (Exception ex)
                {
                    return ex.Message;
                }

            }
        }

        public List<KeyValuePair<string, string>> GetCommonParameters()
        {
            List<KeyValuePair<string, string>> data = new List<KeyValuePair<string, string>>();
            var permissions = _securityService.GetUserPermissions();


            var roles = string.Join(", ",
                permissions.Result.Where(ar => ar.Name.Contains("Роль."))
                    .Select(r => r.Name.Replace("Роль.", string.Empty)).Distinct());


            var sonos = string.Join(", ", permissions.Result.Select(ar => ar.StructContext).Distinct());

            data.Add(new KeyValuePair<string, string>("Идентификатор пользователя", CurrentUserSid));
            data.Add(new KeyValuePair<string, string>("IP адрес", CurrentIPAddress));
            data.Add(new KeyValuePair<string, string>("Роль пользователя", roles));
            data.Add(new KeyValuePair<string, string>("НО пользователя", sonos));
            data.Add(new KeyValuePair<string, string>("Дата и время выполнения события", DateTime.Now.ToString(_dateTimeStringMask)));

            return data;
        }

        /// <summary>
        /// Выгрузка в Excel. Отчет 2 МЭ
        /// </summary>
        /// <param name="calcDate">Дата расчета</param>
        /// <param name="tnoCode">Субъект РФ </param>
        public void ExportReportFormat2(DateTime calcDate, string tnoCode)
        {
            var param = GetCommonParameters();
            param.Add(new KeyValuePair<string, string>("Субъект РФ", tnoCode));
            param.Add(new KeyValuePair<string, string>("Отчетная дата", calcDate.ToString(_dateStringMask)));
            LogObject logObj = new LogObject()
            {
                Operation = LogOperation.EfficeincyExcelMonitoringReport_2ME_UFNS
            };

            _loggerService.Write(logObj, true, param.ToArray());
        }

        /// <summary>
        /// Выгрузка в Excel. МЭ Рейтинг ИФНС
        /// </summary>
        /// <param name="calcDate">Дата расчета</param>
        /// <param name="quarter">Квартал</param>
        public void ExportReportIfnsRating(DateTime calcDate, QuarterInfo quarter)
        {
            var param = GetCommonParameters();
            param.Add(new KeyValuePair<string, string>("Квартал", quarter.ToString()));
            param.Add(new KeyValuePair<string, string>("Дата расчета", calcDate.ToString(_dateStringMask)));

            LogObject logObj = new LogObject()
            {
                Operation = LogOperation.EfficeincyExcelMonitoringReportIFNSRating
            };

            _loggerService.Write(logObj, true, param.ToArray());
        }

        /// <summary>
        /// Выгрузка в Excel. МЭ ДНР
        /// </summary>
        /// <param name="calcDate">Дата расчета</param>
        /// <param name="quarter">Квартал</param>
        /// <param name="tnoCode">Субъект РФ</param>
        public void ExportReportUnresolvedDiscrepancies(DateTime calcDate, QuarterInfo quarter, string tnoCode)
        {
            var param = GetCommonParameters();
            param.Add(new KeyValuePair<string, string>("Субъект РФ", RecognizeSonoCode(tnoCode)));
            param.Add(new KeyValuePair<string, string>("Квартал", quarter.ToString()));
            param.Add(new KeyValuePair<string, string>("Дата расчета", calcDate.ToString(_dateStringMask)));
            LogObject logObj = new LogObject()
            {
                Operation = LogOperation.EfficeincyExcelMonitoringUnresilvedDiscrepancyReport
            };

            _loggerService.Write(logObj, true, param.ToArray());
        }

        /// <summary>
        /// Выгрузка в Excel. МЭ Еженедельный отчет	
        /// </summary>
        /// <param name="calcDate">Дата расчета</param>
        /// <param name="quarter">Квартал</param>
        /// <param name="tnoCode">Субъект РФ</param>
        public void ExportReportDaily(DateTime calcDate, QuarterInfo quarter, string tnoCode)
        {
            var param = GetCommonParameters();
            param.Add(new KeyValuePair<string, string>("Субъект РФ", RecognizeSonoCode(tnoCode)));
            param.Add(new KeyValuePair<string, string>("Квартал", quarter.ToString()));
            param.Add(new KeyValuePair<string, string>("Дата расчета", calcDate.ToString(_dateStringMask)));
            LogObject logObj = new LogObject()
            {
                Operation = LogOperation.EfficeincyExcelMonitoringReportWeekly
            };

            _loggerService.Write(logObj, true, param.ToArray());
        }


        //TODO: Убрать после рефакторинга МЭ
        private string RecognizeSonoCode(string tnoCode)
        {
            if (tnoCode.Equals("0")) return "0000";
            if (tnoCode.Length == 2) return tnoCode+"00";

            return tnoCode;
        }
    }
}
