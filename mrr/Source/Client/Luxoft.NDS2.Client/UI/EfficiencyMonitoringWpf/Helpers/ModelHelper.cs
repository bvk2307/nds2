﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Helpers
{
    public static class ModelHelper
    {
        public static ScopeEnum ConvertTnoCodeToScope(string code)
        {
            if (IsRf(code))
                return ScopeEnum.RF_UFNS;

            if (IsMiKn(code))
                return ScopeEnum.MIKN;

            return ScopeEnum.RF_DISTRICTS;
        }

        public static bool IsRf(string code)
        {
            if (string.IsNullOrEmpty(code))
                return false;
            return code.Equals("0");
        }

        public static bool IsMiKn(string code)
        {
            if (string.IsNullOrEmpty(code))
                return false;
            return code.StartsWith("99");
        }

        public static bool IsMiKnDistrict(string code)
        {
            if (string.IsNullOrEmpty(code))
                return false;
            return code.Equals("9900");
        }

        public static bool IsSorted(this GridViewDataControl grid, string columnName, ListSortDirection direction)
        {
            var fsd = grid.SortDescriptors.FirstOrDefault();
            return grid.SortDescriptors.Count == 1 &&
                ((fsd is ColumnSortDescriptor && ((ColumnSortDescriptor)fsd).Column.UniqueName == columnName) ||
                (fsd is SortDescriptor && ((SortDescriptor)fsd).Member == columnName)) &&
                             fsd.SortDirection == direction;
        }

        public static ObservableCollection<YearModel> ToYearModel(this Dictionary<QuarterInfo, List<ReportCalculationInfo>> data)
        {
            return new ObservableCollection<YearModel>(
                data.AsQueryable()
                        .GroupBy(y => y.Key.Year)
                            .OrderByDescending(s => s.Key)
                                .Select(y => new YearModel
            {
                Year = y.Key,
                Quarters =
                    new ObservableCollection<QuarterModel>(
                        y.AsQueryable()
                            .OrderByDescending(s => s.Key.Index)
                                .Select(s =>
                                    new QuarterModel
                                    {
                                        Quarter = s.Key,
                                        DateModels = new List<CalculationDateModel>(
                                            s.Value
                                                .Select(x=>new CalculationDateModel(x.Date, x.IsBasedOnEfficiencyAgregate == 1))
                                                    .OrderBy(x => x.Date))
                                    }))
            }));
        }
    }
}
