﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Markup;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;
using Luxoft.NDS2.Common.Contracts.Services;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using Microsoft.Practices.CompositeUI;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contract.DTO.SearchEntity;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf
{
    public class UfnsRatingPresenter : BaseMonitoringPresenter<UfnsRatingView>
    {

        public override List<DateTime> GetDateList(QuarterInfo quarter)
        {
            var ret = new List<DateTime>();
            ExecuteServiceCall(
                () => Service.GetEnabledDates(quarter.Year, quarter.Quarter),
                result =>
                {
                    ret.AddRange(result.Result.OrderBy(q => q));
                },
                resultErr =>
                {
                });
            return ret;
        }

        /// <summary>
        /// Получить управление, которому принадлежит пользователь
        /// </summary>
        /// <returns></returns>
        public string GetDirection()
        {
            var role = UserRoles.FirstOrDefault(ur => ur.PermType == CommonComponents.Security.Authorization.PermissionType.Operation && ur.Name == Constants.SystemPermissions.Operations.RoleManager && ur.StructContext != "0000" && ur.StructContext.EndsWith("00"));
            return role != null ? role.StructContext.Substring(0, 2) : string.Empty;
        }

        public override Dictionary<string, List<TnoRatingInfo>> GetQuartersRatings(ChartData request)
        {
            var ret = new Dictionary<string, List<TnoRatingInfo>>();

            ExecuteServiceCall(
                () => Service.GetAllQuartersRatings(request),
                result =>
                {
                    ret = result.Result;
                },
                resultErr =>
                {
                    View.ShowError(resultErr.Message);
                });

            return ret;
        }

        public void CreateInspectionView(InitStateModel model)
        {
            var winManager = WorkItem.Services.Get<IWindowsManagerService>();
            var viewId = Guid.NewGuid();
            var featureId = Guid.NewGuid();
            var workItem = WorkItem.WorkItems.AddNew<WorkItem>(Guid.NewGuid().ToString());
            var featureContext = new FeatureContextBase(featureId);
            var presentationContext = new PresentationContext(viewId, null, featureContext);

            var view =
                winManager.AddNewSmartPart<InspectionView>(
                    workItem,
                    viewId.ToString(),
                    presentationContext,
                    model);
            winManager.Show(presentationContext, workItem, view);
        }

        public void CreateUnprovenDiffDetailsView(InitStateModel model)
        {
            var winManager = WorkItem.Services.Get<IWindowsManagerService>();
            var viewId = Guid.NewGuid();
            var featureId = Guid.NewGuid();
            var workItemAdded = WorkItem.WorkItems.AddNew<WorkItem>(Guid.NewGuid().ToString());
            var featureContext = new FeatureContextBase(featureId);
            var presentationContext = new PresentationContext(viewId, null, featureContext)
            {
                WindowTitle = "Не обработано расхождений",
                WindowDescription = "Не обработано расхождений"
            };


            var view =
                winManager.AddNewSmartPart<UnprovenDiffDetailsView>(
                    workItemAdded,
                    viewId.ToString(),
                    presentationContext,
                    model);

            winManager.Show(presentationContext, workItemAdded, view);
        }

        public void CreateInspectionRatingView(InitStateModel model)
        {
            var winManager = WorkItem.Services.Get<IWindowsManagerService>();
            var viewId = Guid.NewGuid();
            var featureId = Guid.NewGuid();
            var workItemAdded = WorkItem.WorkItems.AddNew<WorkItem>(Guid.NewGuid().ToString());
            var featureContext = new FeatureContextBase(featureId);
            var presentationContext = new PresentationContext(viewId, null, featureContext);


            var view =
                winManager.AddNewSmartPart<InspectionRatingView>(
                    workItemAdded,
                    viewId.ToString(),
                    presentationContext,
                    model);

            winManager.Show(presentationContext, workItemAdded, view);
        }
    }
}
