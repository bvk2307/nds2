﻿using System;
using System.Collections.Generic;
using Infragistics.Excel;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Reports
{
    /// <summary>
    /// Содержит логику формирования отчета "рейтинг ифнс"
    /// </summary>
    public class ReportIfnsMe
    {
        private const string _tabName = "Отчет рейтинг ИФНС";
        private const int _dataStartRowIndex = 7;
        private const string _formatString2 = "#,##0.00";

        private int[] _columnsWidth = { 2000, 2000, 14000, 2000, 14000, 6000, 6000, 6000 };
        private DataHeader[] Headers;
        private Worksheet _ws;
        private class DataHeader
        {
            public string Text;
            public int Row;
            public int Column;

            public bool IsMerged;
            public int MergedRow;
            public int MergedColumn;

            //Содержит действия форматирования заголовка
            public Action<IWorksheetCellFormat> Formatter;
        }

        #region Часто используемые виды форматирования в ячейке
        Action<IWorksheetCellFormat> CenterAlignment = (cf) =>
        {
            cf.VerticalAlignment = VerticalCellAlignment.Center;
            cf.Alignment = HorizontalCellAlignment.Center;
            cf.WrapText = ExcelDefaultableBoolean.True;
        };

        Action<IWorksheetCellFormat> RightAlignment = (cf) =>
        {
            cf.VerticalAlignment = VerticalCellAlignment.Center;
            cf.Alignment = HorizontalCellAlignment.Right;
        };

        Action<IWorksheetCellFormat> Bold = (cf) =>
        {
            cf.Font.Bold = ExcelDefaultableBoolean.True;
        };

        Action<IWorksheetCellFormat> Border = (cf) =>
        {
            cf.BottomBorderColor = System.Drawing.Color.Black;
            cf.LeftBorderColor = System.Drawing.Color.Black;
            cf.TopBorderColor = System.Drawing.Color.Black;
            cf.RightBorderColor = System.Drawing.Color.Black;
        };
        #endregion

        public void Create(string fileName, QuarterInfo quarter, DimensionType dimensionType, DateTime reportDate, IEnumerable<IfnsRatingByDiscrepancyShareInDeductionData> data)
        {
            var _wb = new Workbook();
            _ws = _wb.Worksheets.Add(_tabName);
            InitHeaders(quarter, reportDate);
            SetHeaders();
            SetData(data, _dataStartRowIndex);
            SetRowsHeight();
            SetColumnsWidth();
            _wb.Save(fileName);
        }

        private void InitHeaders(QuarterInfo quarter, DateTime reportDate)
        {
            var centerWithBorderFormatter = Border + CenterAlignment;

            Headers = new DataHeader[]
            {
            new DataHeader
            {
                Text ="Форма рейтингов ИФНС - МЭ",
                Row = 0,
                Column = 6,
                IsMerged = true,
                MergedRow = 0,
                MergedColumn = 7,
                Formatter = RightAlignment,
            },
            new DataHeader
            {
                Text ="ОТЧЕТ\nО результатах эффективности деятельности территориальных налоговых органов\n\"Удельный вес суммы расхождений по декларациям по НДС в общей сумме налоговых вычетов по НДС\"",
                Row =1,
                Column =0,
                IsMerged =true,
                MergedRow = 1,
                MergedColumn = 8,
                Formatter = CenterAlignment + Bold
            },
            new DataHeader
            {
                Text =String.Format("налоговый период {0} кв. {1} г.", quarter.Quarter, quarter.Year),
                Row =2,
                Column = 0,
                IsMerged = true,
                MergedRow = 2,
                MergedColumn = 8,
                Formatter = CenterAlignment
            },
            new DataHeader
            {
                Text =String.Format("по состоянию на {0} г.", reportDate.ToString("dd.MM.yyyy")),
                Row =3,
                Column =0,
                IsMerged =true,
                MergedRow = 3,
                MergedColumn = 8,
                Formatter = CenterAlignment
            },
            new DataHeader
            {
                Text ="Руб.",
                Row = 4,
                Column = 7,
                Formatter = RightAlignment
            },
            new DataHeader
            {
                Text ="ИФНС",
                Row =5,
                Column = 0,
                IsMerged =true,
                MergedRow = 5,
                MergedColumn = 2,
                Formatter = centerWithBorderFormatter
            },

            new DataHeader {Text ="Рейтинг", Row=6, Column=0, Formatter = centerWithBorderFormatter},
            new DataHeader {Text ="Код НО", Row=6, Column=1, Formatter = centerWithBorderFormatter},
            new DataHeader {Text ="Налоговый орган", Row=6, Column=2, Formatter = centerWithBorderFormatter},

            new DataHeader {Text ="УФНС",Row=5,Column=3, IsMerged=true, MergedRow = 5, MergedColumn = 4, Formatter = centerWithBorderFormatter},
            new DataHeader {Text ="Код НО", Row=6, Column=3, Formatter = centerWithBorderFormatter},
            new DataHeader {Text ="Налоговый орган", Row=6, Column=4, Formatter = centerWithBorderFormatter},

            new DataHeader {Text ="Сумма расхождений", Row=5,Column=5, IsMerged=true, MergedRow = 6, MergedColumn = 5, Formatter = centerWithBorderFormatter},
            new DataHeader {Text ="Сумма вычетов по НДС", Row=5,Column=6, IsMerged=true, MergedRow = 6, MergedColumn = 6, Formatter = centerWithBorderFormatter},
            new DataHeader {Text ="Доля расхождений в сумме вычетов (%)",Row=5,Column=7, IsMerged=true, MergedRow = 6, MergedColumn = 7, Formatter = centerWithBorderFormatter}
            };
        }

        private void SetHeaders()
        {
            foreach (var h in Headers)
            {
                if (h.IsMerged)
                {
                    _ws.MergedCellsRegions.Add(h.Row, h.Column, h.MergedRow, h.MergedColumn);
                }
                SetCellValue(h.Row, h.Column, h.Text, h.Formatter);
            }
        }

        private void SetData(IEnumerable<IfnsRatingByDiscrepancyShareInDeductionData> data, int indexRow)
        {
            Action<IWorksheetCellFormat> digitFormat = (cf) => cf.FormatString = _formatString2;
            foreach (var item in data)
            {
                SetCellValue(indexRow, 0, item.Rating, CenterAlignment + Border);
                SetCellValue(indexRow, 1, item.SonoCode, CenterAlignment + Border);
                SetCellValue(indexRow, 2, item.SonoName, Border);
                SetCellValue(indexRow, 3, item.RegionCode, CenterAlignment + Border);
                SetCellValue(indexRow, 4, item.RegionName, Border);
                SetCellValue(indexRow, 5, item.DiscrepancyAmount, CenterAlignment + Border + digitFormat);
                SetCellValue(indexRow, 6, item.DeductionAmount, CenterAlignment + Border + digitFormat);
                SetCellValue(indexRow, 7, item.ShareValue, CenterAlignment + Border + digitFormat);
                indexRow++;
            }
        }

        private void SetRowsHeight()
        {
            _ws.Rows[1].Height = 1000;
            _ws.Rows[5].Height = 500;
            _ws.Rows[6].Height = 500;
        }

        private void SetColumnsWidth()
        {
            int index = 0;
            foreach (int w in _columnsWidth)
            {
                _ws.Columns[index].Width = w;
                index++;
            }
        }

        private void SetCellValue(int row, int column, object value, Action<IWorksheetCellFormat> cellFormatAction)
        {
            if (cellFormatAction != null)
                cellFormatAction(_ws.Rows[row].Cells[column].CellFormat);

            if (String.IsNullOrEmpty(value.ToString()) || value.ToString() == "0")
            {
                value = "-";
            }
            _ws.Rows[row].Cells[column].Value = value;
        }
    }
}
