﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Infragistics.Excel;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common.TnoMonitorData;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Report;
using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Reports
{
    public class Report2M
    {
        private string _formatString = "#,###";
        private string _formatString2 = "#,##0.00";

        private string _tabNameRF = "Отчет ФНС";
        private string _tabNameUFNS = "Отчет УФНС";

        private string _tabNameRF_E = "Отчет УФНС (Е)";
        private string _tabNameUFNS_E = "Отчет ИФНС (Е)";

        private string _headerText = "Форма №2 - МЭ";
        private string _headerTextEd = "Форма еженедельнго отчета - МЭ";
        private string _dateTemplate = "по состоянию на {0} г.";
        private string _measureText = "руб.";
        
        private int[] _headerPosition = { 0, 4 };
        private int[] _reportNamePosition = { 1, 0 };
        private int[] _datePosition = { 2, 0 };
        private int[] _measurePosition = { 3, 4 };

        private int[] _columnsWidth = { 3000, 15000, 8000, 8000, 5000 };
        private int[] _dataHeaderStartPosition = { 4, 0 };
        private int[] _dataStartPosition = { 6, 0 };


        private string[] _dataColumnHeader = {
			"Код НО",
			"Налоговый орган",
			"Сумма не устраненных расхождений",
			"Общая сумма НДС, подлежащая вычету",
			"Доля расхождений в сумме вычетов, (%)"
		};
        private string[] _dataColumnLetter = { "A", "B", "1", "2", "3" };

        private string _reportName = "ОТЧЕТ\nО результатах эффективности деятельности территориальных налоговых органов\n\"Удельный вес суммы расхождений по декларациям по НДС в общей сумме налоговых вычетов по НДС\"";
        Workbook _wb;
        Worksheet _ws;

        public void Create(Report2Model model)
        {
            _wb = new Workbook();
            _ws = _wb.Worksheets.Add((model.TypeEnum == ScopeEnum.UFNS) ? (model.Weekly ? _tabNameUFNS_E : _tabNameUFNS) : (model.Weekly ? _tabNameRF_E : _tabNameRF));
            SetCellValue(_headerPosition, model.Weekly ? _headerTextEd : _headerText, HorizontalCellAlignment.Right);
            SetCellValue(_reportNamePosition, _reportName, HorizontalCellAlignment.Center, true);
            if (model.Weekly)
            {
                SetCellValue(new [] { 2, 0 }, string.Format("за налоговый период {0}", model.QuarterInfo), HorizontalCellAlignment.Center);
                MoveStartPositions();
            }
            SetCellValue(_datePosition, String.Format(_dateTemplate, model.ReportDate.ToString("dd.MM.yyyy")), HorizontalCellAlignment.Center);
            SetCellValue(_measurePosition, _measureText, HorizontalCellAlignment.Right);
            MergeCells(model);
            SetRowHeight();
            SetDataColumnHeader();
            SetColumnWidth();
            SetData(model.Data, _dataStartPosition);
            _wb.Save(model.FileName);
        }

        private void MoveStartPositions()
        {
            _datePosition = new [] { 3, 0 };
            _measurePosition = new[] { 4, 4 };
            _dataHeaderStartPosition = new[] { 5, 0 };
            _dataStartPosition = new[] { 7, 0 };
        }

        private void SetRowHeight()
        {
            _ws.Rows[_reportNamePosition[0]].Height = 1000;
            _ws.Rows[_dataHeaderStartPosition[0]].Height = 1000;
        }

        private void SetDataColumnHeader()
        {
            int indexRow = _dataHeaderStartPosition[0];
            int indexColumn = 0;
            foreach (string header in _dataColumnHeader)
            {
                SetCellValue(new int[] { indexRow, indexColumn }, header, HorizontalCellAlignment.Center);
                indexColumn++;
            }
            indexColumn = 0;
            foreach (string letter in _dataColumnLetter)
            {
                SetCellValue(new int[] { indexRow + 1, indexColumn }, letter, HorizontalCellAlignment.Center);
                indexColumn++;
            }
        }
        private void SetData(IEnumerable<TnoMonitorDataInfoModel> data, int[] position)
        {
            List<TnoMonitorDataInfoModel> sorted = data.OrderBy(d => d.TnoCode.PadRight(4, '0')).ToList();
            int indexRow = position[0];
            foreach (TnoMonitorDataInfoModel item in sorted)
            {
                SetDataItem(indexRow, item);
                indexRow++;
            }
        }
        private void SetDataItem(int indexRow, TnoMonitorDataInfoModel item)
        {
            string tnoCode = item.TnoCode.PadRight(4, '0');
            SetCellValue(new int[] { indexRow, 0 }, tnoCode, HorizontalCellAlignment.Center);
            SetCellValue(new int[] { indexRow, 1 }, item.TnoName);
            SetCellValue(new int[] { indexRow, 2 }, item.DiscrepancyOpenAmount, HorizontalCellAlignment.Right);
            SetCellValue(new int[] { indexRow, 3 }, item.DeclarationDeductionAmount, HorizontalCellAlignment.Right);
            SetCellValue(new int[] { indexRow, 4 }, item.EffIndex24, HorizontalCellAlignment.Right);

            _ws.Rows[indexRow].Cells[2].CellFormat.FormatString = _formatString;
            _ws.Rows[indexRow].Cells[3].CellFormat.FormatString = _formatString;
            _ws.Rows[indexRow].Cells[4].CellFormat.FormatString = _formatString2;
        }

        private void SetColumnWidth()
        {
            int index = 0;
            foreach (int w in _columnsWidth)
            {
                _ws.Columns[index].Width = w;
                index++;
            }
        }

        private void MergeCells(Report2Model model)
        {
            _ws.MergedCellsRegions.Add(_reportNamePosition[0], 0, _reportNamePosition[0], 4);
            _ws.MergedCellsRegions.Add(_datePosition[0], 0, _datePosition[0], 4);
            if (model.Weekly)
            {
                _ws.MergedCellsRegions.Add(2, 0, 2, 4);
            }
        }
        private void SetCellValue(int[] position, object value, HorizontalCellAlignment alignment = HorizontalCellAlignment.Left, bool bold = false)
        {
            if (position[0] >= _dataHeaderStartPosition[0])
            {
                _ws.Rows[position[0]].Cells[position[1]].CellFormat.BottomBorderColor = System.Drawing.Color.Black;
                _ws.Rows[position[0]].Cells[position[1]].CellFormat.LeftBorderColor = System.Drawing.Color.Black;
                _ws.Rows[position[0]].Cells[position[1]].CellFormat.RightBorderColor = System.Drawing.Color.Black;
                _ws.Rows[position[0]].Cells[position[1]].CellFormat.TopBorderColor = System.Drawing.Color.Black;
            }
            if (String.IsNullOrEmpty(value.ToString()) || value.ToString() == "0")
            {
                value = "-";
            }
            _ws.Rows[position[0]].Cells[position[1]].Value = value;
            if (bold)
            {
                _ws.Rows[position[0]].Cells[position[1]].CellFormat.Font.Bold = ExcelDefaultableBoolean.True;
            }
            if (alignment != HorizontalCellAlignment.Left)
            {
                _ws.Rows[position[0]].Cells[position[1]].CellFormat.Alignment = alignment;
            }
            if (alignment == HorizontalCellAlignment.Center)
            {
                _ws.Rows[position[0]].Cells[position[1]].CellFormat.VerticalAlignment = VerticalCellAlignment.Center;
                _ws.Rows[position[0]].Cells[position[1]].CellFormat.WrapText = ExcelDefaultableBoolean.True;
            }
        }
    }
}
