﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Infragistics.Excel;
using Luxoft.NDS2.Client.Extensions;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Converters;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common.TnoMonitorData;
using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Reports
{
    public class ReportUnproven
    {
        private string _formatString = "#,###";
        private string _formatString2 = "#,##0.00";

        private string _tabName = "Детализация расхождений";
        private int[] _headerPosition = { 0, 13 };
        private int[] _reportNamePosition = { 1, 0 };
        private int[] _periodPosition = { 2, 0 };
        private int[] _datePosition = { 3, 0 };
        private int[] _measurePosition = { 4, 13 };

        private int[] _columnsWidth = { 1500, 6000, 2000, 5000, 6000, 4000, 4000, 4000, 4000, 4000, 4000, 4000, 6000, 4000 };
        private int _dataHeaderStartPosition = 5;

        Workbook _wb;
        Worksheet _ws;

        public void Create(string fileName, QuarterModel quarter, DimensionType dimensionType, DateTime reportDate, IEnumerable<DiscrepancyDetail> data, IEnumerable<SurCode> сurs)
        {
            _wb = new Workbook();
            _ws = _wb.Worksheets.Add(_tabName);
            SetCellValue(_headerPosition, "Форма детализация не отработанных расхождений - МЭ", HorizontalCellAlignment.Right);
            SetCellValue(_reportNamePosition, "ОТЧЕТ\nО результатах деятельности территориальных налоговых органов\n\"Детализация неотработанных расхождений (по налогоплательщикам)\"", HorizontalCellAlignment.Center, true);
            SetCellValue(_periodPosition, String.Format("налоговый период {0} кв. {1} г.", quarter.Quarter.Quarter, quarter.Quarter.Year), HorizontalCellAlignment.Center);
            SetCellValue(_datePosition, String.Format("по состоянию на {0} г.", reportDate.ToString("dd.MM.yyyy")), HorizontalCellAlignment.Center);
            SetCellValue(_measurePosition, "руб.", HorizontalCellAlignment.Right);
            //SetCellValue(_measurePosition, String.Format("{0} руб.", dimensionType.GetDisplayName()), HorizontalCellAlignment.Right);
            SetDataColumnHeader();
            MergeCells();
            SetRowHeight();
            SetColumnWidth();
            SetData(data, 8, dimensionType, сurs);
            _wb.Save(fileName);
        }


        private void SetRowHeight()
        {
            _ws.Rows[1].Height = 1000;
            _ws.Rows[5].Height = 1000;
        }

        private void SetDataColumnHeader()
        {
            int indexRow = _dataHeaderStartPosition;
            SetCellValue(new[] { indexRow, 0 }, "Код НО", HorizontalCellAlignment.Center);
            SetCellValue(new[] { indexRow, 1 }, "Налоговый орган", HorizontalCellAlignment.Center);
            SetCellValue(new[] { indexRow, 2 }, "СУР", HorizontalCellAlignment.Center);
            SetCellValue(new[] { indexRow, 3 }, "ИНН", HorizontalCellAlignment.Center);
            SetCellValue(new[] { indexRow, 4 }, "Наименование налогоплательщика", HorizontalCellAlignment.Center);
            SetCellValue(new[] { indexRow, 5 }, "Сумма НДС по налоговой декларации", HorizontalCellAlignment.Center);
            SetCellValue(new[] { indexRow, 6 }, "Неотработанные Расхождения", HorizontalCellAlignment.Center);
            SetCellValue(new[] { indexRow, 8 }, "В том числе, неотработанные Расхождения вида", HorizontalCellAlignment.Center);
            SetCellValue(new[] { indexRow, 12 }, "Налоговый  инспектор, проводящий КНП", HorizontalCellAlignment.Center);
            SetCellValue(new[] { indexRow, 13 }, "Признак подачи уточненной НД", HorizontalCellAlignment.Center);

            indexRow++;
            SetCellValue(new[] { indexRow, 6 }, "Количество", HorizontalCellAlignment.Center);
            SetCellValue(new[] { indexRow, 7 }, "Сумма", HorizontalCellAlignment.Center);
            SetCellValue(new[] { indexRow, 8 }, "\"Разрыв\"", HorizontalCellAlignment.Center);
            SetCellValue(new[] { indexRow, 10 }, "\"НДС\"", HorizontalCellAlignment.Center);

            indexRow++;
            SetCellValue(new[] { indexRow, 8 }, "Количество", HorizontalCellAlignment.Center);
            SetCellValue(new[] { indexRow, 9 }, "Сумма", HorizontalCellAlignment.Center);
            SetCellValue(new[] { indexRow, 10 }, "Количество", HorizontalCellAlignment.Center);
            SetCellValue(new[] { indexRow, 11 }, "Сумма", HorizontalCellAlignment.Center);
        }
        private void SetData(IEnumerable<DiscrepancyDetail> data, int position, DimensionType dimensionType, IEnumerable<SurCode> сurs)
        {
            int indexRow = position;
            foreach (var item in data)
            {
                SetDataItem(indexRow, item, dimensionType, сurs);
                indexRow++;
            }
        }
        private void SetDataItem(int indexRow, DiscrepancyDetail item, DimensionType dimensionType, IEnumerable<SurCode> сurs)
        {
            SetCellValue(new [] { indexRow, 0 }, item.SonoCode, HorizontalCellAlignment.Center);
            SetCellValue(new[] { indexRow, 1 }, item.SonoName);
            SetCellValue(new[] { indexRow, 2 }, CurColumnTextConverter.ConvertDirect(item.SurCode, сurs));
            SetCellValue(new [] { indexRow, 3 }, item.Inn);
            SetCellValue(new [] { indexRow, 4 }, item.TaxPayerName);
            SetCellValue(new[] { indexRow, 5 }, item.NdsSum, HorizontalCellAlignment.Right);
            SetCellValue(new[] { indexRow, 6 }, item.OpenKnpDiscrepancyCount, HorizontalCellAlignment.Center);
            SetCellValue(new[] { indexRow, 7 }, item.OpenKnpDiscrepancyAmount, HorizontalCellAlignment.Right);
            SetCellValue(new[] { indexRow, 8 }, item.OpenKnpGapDiscrepancyCount, HorizontalCellAlignment.Center);
            SetCellValue(new[] { indexRow, 9 }, item.OpenKnpGapDiscrepancyAmount, HorizontalCellAlignment.Right);
            SetCellValue(new[] { indexRow, 10 }, item.OpenKnpNdsDiscrepancyCount, HorizontalCellAlignment.Center);
            SetCellValue(new[] { indexRow, 11 }, item.OpenKnpNdsDiscrepancyAmount, HorizontalCellAlignment.Right);
            SetCellValue(new[] { indexRow, 12 }, item.AssignedInspectorName);
            SetCellValue(new[] { indexRow, 13 }, item.IsActual ? "Нет" : "Да" , HorizontalCellAlignment.Right);
            

            _ws.Rows[indexRow].Cells[5].CellFormat.FormatString = _formatString2;
            _ws.Rows[indexRow].Cells[7].CellFormat.FormatString = _formatString2;
            _ws.Rows[indexRow].Cells[9].CellFormat.FormatString = _formatString2;
            _ws.Rows[indexRow].Cells[11].CellFormat.FormatString = _formatString2;
        }

        private void SetColumnWidth()
        {
            int index = 0;
            foreach (int w in _columnsWidth)
            {
                _ws.Columns[index].Width = w;
                index++;
            }
        }

        private void MergeCells()
        {
            _ws.MergedCellsRegions.Add(1, 0, 1, 13);
            _ws.MergedCellsRegions.Add(2, 0, 2, 13);
            _ws.MergedCellsRegions.Add(3, 0, 3, 13);

            _ws.MergedCellsRegions.Add(5, 0, 7, 0);
            _ws.MergedCellsRegions.Add(5, 1, 7, 1);
            _ws.MergedCellsRegions.Add(5, 2, 7, 2);
            _ws.MergedCellsRegions.Add(5, 3, 7, 3);
            _ws.MergedCellsRegions.Add(5, 4, 7, 4);
            _ws.MergedCellsRegions.Add(5, 5, 7, 5);
            _ws.MergedCellsRegions.Add(5, 6, 5, 7);
            _ws.MergedCellsRegions.Add(5, 8, 5, 11);
            _ws.MergedCellsRegions.Add(5, 12, 7, 12);
            _ws.MergedCellsRegions.Add(5, 13, 7, 13);
            _ws.MergedCellsRegions.Add(6, 6, 7, 6);
            _ws.MergedCellsRegions.Add(6, 7, 7, 7);
            _ws.MergedCellsRegions.Add(6, 8, 6, 9);
            _ws.MergedCellsRegions.Add(6, 10, 6, 11);
        }

        private void SetCellValue(int[] position, object value, HorizontalCellAlignment alignment = HorizontalCellAlignment.Left, bool bold = false)
        {
            if (position[0] >= _dataHeaderStartPosition)
            {
                _ws.Rows[position[0]].Cells[position[1]].CellFormat.BottomBorderColor = System.Drawing.Color.Black;
                _ws.Rows[position[0]].Cells[position[1]].CellFormat.LeftBorderColor = System.Drawing.Color.Black;
                _ws.Rows[position[0]].Cells[position[1]].CellFormat.RightBorderColor = System.Drawing.Color.Black;
                _ws.Rows[position[0]].Cells[position[1]].CellFormat.TopBorderColor = System.Drawing.Color.Black;
            }
            if (String.IsNullOrEmpty(value.ToString()) || value.ToString() == "0")
            {
                value = "-";
            }
            _ws.Rows[position[0]].Cells[position[1]].Value = value;
            if (bold)
            {
                _ws.Rows[position[0]].Cells[position[1]].CellFormat.Font.Bold = ExcelDefaultableBoolean.True;
            }
            if (alignment != HorizontalCellAlignment.Left)
            {
                _ws.Rows[position[0]].Cells[position[1]].CellFormat.Alignment = alignment;
            }
            if (alignment == HorizontalCellAlignment.Center)
            {
                _ws.Rows[position[0]].Cells[position[1]].CellFormat.VerticalAlignment = VerticalCellAlignment.Center;
                _ws.Rows[position[0]].Cells[position[1]].CellFormat.WrapText = ExcelDefaultableBoolean.True;
            }
        }
    }
}
