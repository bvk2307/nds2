﻿using System;
using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.CentralApparat;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Chart
{
    public class ChartWebDataRequest
    {
        public ChartWebDataRequest()
        {
            TnoModel = new TnoData();
            ParentTnoModel = new TnoData();
            Quarter = new QuarterInfo();
        }

        public DateTime CalculationDate { get; set; }

        public IndexEnum IndexValue { get; set; }

        public ContextType Context { get; set; }

        public ScopeEnum Scope { get; set; } 

        public TnoData TnoModel { get; set; }

        public ScopeEnum ParentScope { get; set; } 

        public TnoData ParentTnoModel { get; set; }

        public QuarterInfo Quarter { get; set; }
    }
}
