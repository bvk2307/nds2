﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Chart
{
    public interface IChartDataProvider
    {
        Dictionary<string, List<TnoRatingInfo>> GetQuartersRatings(Common.Contracts.DTO.Business.EfficiencyMonitoring.ChartData request);

        List<QuarterInfo> GetQuarters();
    }
}
