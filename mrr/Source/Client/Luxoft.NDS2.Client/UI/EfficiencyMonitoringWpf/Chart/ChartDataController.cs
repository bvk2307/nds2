﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Media;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.CentralApparat;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Common;
using Luxoft.NDS2.Client.UI.MapWpf.Extensions;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Chart
{
    public class ChartDataController
    {
        private IChartDataProvider _dataProvider;

        public ChartDataController(IChartDataProvider provider)
        {
            _dataProvider = provider;
        }



        public BaseChartDataModel CreateChartModel( ChartWebDataRequest webRequest )
        {
            var request = new ChartData();
            var tnoCode = webRequest.TnoModel.TNO_CODE;
            var parentTnoCode = webRequest.ParentTnoModel.TNO_CODE;

            request.CalculateDate = webRequest.CalculationDate;
            request.FiscalYear = webRequest.Quarter.Year;
            request.Quarter = webRequest.Quarter.Quarter;

            if (tnoCode == parentTnoCode)
            {
                request.ScopeCode = parentTnoCode;
                request.Scope = webRequest.ParentScope;
                parentTnoCode = null;
            }
            else
            {
                request.ScopeCode = tnoCode;
                request.Scope = webRequest.Scope;
                request.ParentScopeCode = parentTnoCode;
                request.ParentScope = webRequest.ParentScope;
            }
            
            var data = _dataProvider.GetQuartersRatings(request);

            //Добавим пустые данные кварталов, в случае если одна серия больше другой
            if (data.Keys.Count == 2)
            {
                NormalizeQuarterSeries(data, tnoCode, parentTnoCode);
            }
            

            var chartModel = new BaseChartDataModel();
            var firstDataSetModel = new GraphDataSetModel
            {
                SeriesName = webRequest.TnoModel.TNO_NAME,
                Visible = true,
            };
            firstDataSetModel.AddRange(data.FirstOrDefault().
                    Value.
                    Select(i => new GraphDataValueModel()
                    {
                        TnoName = webRequest.TnoModel.TNO_NAME,
                        Value = GetRatingValue(webRequest.IndexValue, i),
                        Quarter = new QuarterInfo { Quarter = i.Quarter, Year = i.FiscalYear },
                        Color = webRequest.Quarter.Equals(i.FiscalYear, i.Quarter) ? Color.FromArgb(0xFF, 0xfe, 0xa6, 0x10) : Color.FromArgb(0xFF, 0x55, 0x84, 0xb0),
                    })
                    .Where(t => t.Quarter.Index <= webRequest.Quarter.Index)
                    .OrderByDescending(d => d.Quarter.Index)
                    .Take(5)
                    .Reverse());
            chartModel.BarSet = firstDataSetModel;
            if (!string.IsNullOrEmpty(parentTnoCode))
            {
                var secondDataSetModel = new GraphDataSetModel
                {
                    SeriesName = webRequest.ParentTnoModel.TNO_NAME,
                    Visible = true,
                };
                secondDataSetModel.AddRange(
                    data[parentTnoCode]
                    .Select(i => new GraphDataValueModel()
                    {
                        TnoName = webRequest.ParentTnoModel.TNO_NAME,
                        Value = GetRatingValue(webRequest.IndexValue, i),
                        Quarter = new QuarterInfo { Quarter = i.Quarter, Year = i.FiscalYear },
                        Color = Color.FromArgb(0xFF, 0x55, 0x84, 0xb0)
                    })
                    .Where(t=>t.Quarter.Index <= webRequest.Quarter.Index)
                    .OrderByDescending(d=>d.Quarter.Index)
                    .Take(5)
                    .Reverse());
                chartModel.LineSet = secondDataSetModel;
            }

            chartModel.Maximum = Math.Max(
                chartModel.BarSet != null && chartModel.BarSet.Any() ? chartModel.BarSet.Max(s => s.Value) : 0,
                chartModel.LineSet != null && chartModel.LineSet.Any() ? chartModel.LineSet.Max(s => s.Value) : 0);

            chartModel.Maximum = chartModel.Maximum <= (decimal)0.8 ? 1 : chartModel.Maximum * 100 / 80;
            return chartModel;
        }

        
        private void NormalizeQuarterSeries(Dictionary<string, List<TnoRatingInfo>> chartData, string key1, string key2)
        {
            var missedSeq1 = chartData[key2].Except(chartData[key1], new TnoRatingInfoEqualityComparerByQuarter()).Select(i=>i.Clone()).ToList();
            missedSeq1.ForEach(m =>
            {
                m.Rating_2_1_Value = 0;
                m.Rating_2_2_Value = 0;
                m.Rating_2_3_Value = 0;
                m.Rating_2_4_Value = 0;
                m.TnoCode = key1;
            });

            chartData[key1].AddRange(missedSeq1);

            var missedSeq2 = chartData[key1].Except(chartData[key2], new TnoRatingInfoEqualityComparerByQuarter()).Select(i => i.Clone()).ToList();
            missedSeq1.ForEach(m =>
            {
                m.Rating_2_1_Value = 0;
                m.Rating_2_2_Value = 0;
                m.Rating_2_3_Value = 0;
                m.Rating_2_4_Value = 0;
                m.TnoCode = key2;
            });

            chartData[key2].AddRange(missedSeq2);
        }

        private decimal GetRatingValue(IndexEnum index, TnoRatingInfo data)
        {
            switch (index)
            {
                   case IndexEnum.ClosedDiscrepancyShareByAmount:
                   return data.Rating_2_1_Value;
                   case IndexEnum.ClosedDiscrepancyShareByQuantity:
                   return data.Rating_2_2_Value;
                   case IndexEnum.DeclarationWithDiscrepancyShareByQuantity:
                   return data.Rating_2_3_Value;
                   case IndexEnum.DiscrepancyShareInDeduction:
                   return data.Rating_2_4_Value;
            }

            return 0;
        }
    }
}
