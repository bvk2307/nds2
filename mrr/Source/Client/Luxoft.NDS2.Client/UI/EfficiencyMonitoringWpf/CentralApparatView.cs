﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using Luxoft.NDS2.Client.UI.Base.Wpf;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Controls;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Helpers;
using Microsoft.Practices.ObjectBuilder;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Base.Ribbon;
using CommonComponents.Uc.Infrastructure.Interface.UI;
using Microsoft.Practices.CompositeUI;
using Microsoft.Practices.CompositeUI.Commands;
using Application = System.Windows.Forms.Application;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf
{
    public partial class CentralApparatView : BaseView
    {
        private CentralApparatPresenter _presenter;
        private ElementHost _ctrlHost;
        private CentralApparatControl _centralApparatControl;

		#region ribbon menu
		private IRibbonAdapter _ribbon;
		private const string Functions = "Функции";
		private const string FunctionsToolTip = "Функции окна дерева связей";
		private const string RibbonName = "NDS2GraphTreePairsViewRibbon";
		private const string RibbonTitle = "Функции";
		private const string GroupName = "NDS2NavigatorGraphTreeManage";
		private const string ButtonCaption = "Выгрузить в MS Excel";
		private const string ButtonToolTip = "Выгрузить построенное дерево связей в файл формата MS Excel";
		private const string ButtonCommand = "cmdGraphTreeExcelReport";
		#endregion

        [CreateNew]
        public CentralApparatPresenter Presenter
        {
            set
            {
                _presenter = value;
                _presenter.PresentationContext = _presentationContext;
                _presenter.View = this;
                WorkItem = _presenter.WorkItem;
                _presenter.OnObjectCreated();
            }
        }

        public CentralApparatView(PresentationContext context)
        {
            _presentationContext = context;
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            if (!DesignMode)
            {
                ResourceHelper.EnsureApplicationResources();
                _ctrlHost = new ElementHost { Visible = false };
                _centralApparatControl = new CentralApparatControl { Presenter = this._presenter };
                _centralApparatControl.InitViewModel();
                _ctrlHost.Child = _centralApparatControl;
                this.Controls.Add(_ctrlHost);
            }
        }

        public override void OnClosed(WindowCloseContextBase closeContext)
        {
            base.OnClosed(closeContext);
            if (_centralApparatControl != null && _centralApparatControl.DataContextModel != null)
            {
                _centralApparatControl.DataContextModel.ControlClosed();
            }
        }

        public override void OnActivate()
        {
            if (!_ctrlHost.Visible)
            {
                Application.DoEvents();
                _ctrlHost.Size = this.Size;
                _ctrlHost.Dock = DockStyle.Fill;
                _ctrlHost.Visible = true;
            }
        }
    }
}
