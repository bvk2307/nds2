﻿using System.ComponentModel;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Base.Wpf.Extensions;
using Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf.Models.Taxpayer;
using Luxoft.NDS2.Common.Contracts.DTO.Business.EfficiencyMonitoring;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Common.Contract.DTO.SearchEntity;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Telerik.Windows.Data;

namespace Luxoft.NDS2.Client.UI.EfficiencyMonitoringWpf
{
    public class UnprovenDiffDetailsPresenter : BaseMonitoringPresenter<UnprovenDiffDetailsView>
    {
        public PageResult<DiscrepancyDetail> GetCalulatedDiscrepancy(DiffsPagedSearchModel requestModel)
        {
            var ret = new PageResult<DiscrepancyDetail>();

            var request = new QueryConditions();
            /*Условия фильтрации*/
            request.Filter.Add(
                new FilterQuery
                {
                    ColumnName = "Quarter",
                    ColumnType = typeof (int),
                    FilterOperator = FilterQuery.FilterLogicalOperator.And,
                    Filtering =
                        new List<ColumnFilter>
                        {
                            new ColumnFilter
                            {
                                ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                                Value = requestModel.Quarter.Quarter
                            }
                        }
                });

            request.Filter.Add(
                new FilterQuery
                {
                    ColumnName = "FiscalYear",
                    ColumnType = typeof (int),
                    FilterOperator = FilterQuery.FilterLogicalOperator.And,
                    Filtering = new List<ColumnFilter>
                    {
                        new ColumnFilter
                        {
                            ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                            Value = requestModel.Quarter.Year
                        }
                    }
                });

            if (requestModel.IsIfns)
            {
                request.Filter.Add(
                    new FilterQuery
                    {
                        ColumnName = "SonoCode",
                        ColumnType = typeof (string),
                        FilterOperator = FilterQuery.FilterLogicalOperator.And,
                        Filtering = new List<ColumnFilter>
                        {
                            new ColumnFilter
                            {
                                ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                                Value = requestModel.SelectedTnoCode
                            }
                        }
                    });
            }
            else
            {
                request.Filter.Add(
                    new FilterQuery
                    {
                        ColumnName = "UfnsCode",
                        ColumnType = typeof (string),
                        FilterOperator = FilterQuery.FilterLogicalOperator.And,
                        Filtering = new List<ColumnFilter>
                        {
                            new ColumnFilter
                            {
                                ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                                Value = requestModel.SelectedTnoCode
                            }
                        }
                    });
            }

            request.Filter.Add(
                new FilterQuery
                {

                    ColumnName = "CalculationDate",
                    ColumnType = typeof (DateTime),
                    FilterOperator = FilterQuery.FilterLogicalOperator.And,
                    Filtering = new List<ColumnFilter>
                    {
                        new ColumnFilter
                        {
                            ComparisonOperator = ColumnFilter.FilterComparisionOperator.Equals,
                            Value = requestModel.CalculationDate
                        }
                    }
                });
            if (requestModel.Filter != null)
            {
                foreach (var filterSetting in requestModel.Filter)
                {
                    var fq = new FilterQuery
                    {
                        UserDefined = true,
                        ColumnName = filterSetting.ColumnName,
                        ColumnType = filterSetting.ColumnType,
                        FilterOperator =
                            filterSetting.FieldFilterLogicalOperator == FilterCompositionLogicalOperator.And
                                ? FilterQuery.FilterLogicalOperator.And
                                : FilterQuery.FilterLogicalOperator.Or,
                        Filtering = new List<ColumnFilter>()
                    };

                    if (filterSetting.Filter1 != null)
                    {
                        fq.Filtering.Add(filterSetting.Filter1.ToFilterComparision());
                    }
                    if (filterSetting.Filter2 != null)
                    {
                        fq.Filtering.Add(filterSetting.Filter2.ToFilterComparision());
                    }

                    request.Filter.Add(fq);
                }
            }
            /*Условия сортировки*/
            foreach (var sortDescriptor in requestModel.Order)
            {
                request.Sorting.Add(new ColumnSort
                {
                    ColumnKey = sortDescriptor.Name,
                    Order =
                        sortDescriptor.SortDirection == ListSortDirection.Ascending
                            ? ColumnSort.SortOrder.Asc
                            : ColumnSort.SortOrder.Desc
                });
            }

            /*Условия паджинации*/
            request.PaginationDetails.RowsToSkip = (uint) requestModel.Skip;
            request.PaginationDetails.RowsToTake = (uint) requestModel.Take;

            ExecuteServiceCall(
                () => Service.GetDiscrepancyDetails(request, requestModel.WithTotal),
                result =>
                {
                    ret = result.Result;
                },
                resultErr =>
                {
                    View.ShowError(resultErr.Message);
                });

            return ret;
        }

        public IEnumerable<SurCode> ReadCurs()
        {
            IEnumerable<SurCode> result = null;
            ExecuteServiceCall(
                () => GetServiceProxy<IDictionaryDataService>().GetSurDictionary(),
                (response) => result = response.Result);

            return result ?? new List<SurCode>();
        }

        public override Dictionary<string, List<TnoRatingInfo>> GetQuartersRatings(ChartData request)
        {
            var ret = new Dictionary<string, List<TnoRatingInfo>>();

            ExecuteServiceCall(
                () => Service.GetAllQuartersRatings(request),
                result =>
                {
                    ret = result.Result;
                },
                resultErr =>
                {
                    View.ShowError(resultErr.Message);
                });

            return ret;
        }


        public override List<DateTime> GetDateList(QuarterInfo quarter)
        {
            var ret = new List<DateTime>();
            ExecuteServiceCall(
                () => Service.GetEnabledDates(quarter.Year, quarter.Quarter),
                result =>
                {
                    ret.AddRange(result.Result);
                },
                resultErr =>
                {
                });
            return ret;
        }

        public List<string> GetInspections()
        {
            return UserRoles.Where(ur => ur.PermType == CommonComponents.Security.Authorization.PermissionType.Operation

                        && ur.StructContext != "0000" // Не ЦА
                        && !ur.StructContext.EndsWith("00") // Не УФНС
                        && ur.StructContext != "9962" // Не МИ по КК
                )
                .Select(ur => ur.StructContext).ToList();
        }

    }
}
