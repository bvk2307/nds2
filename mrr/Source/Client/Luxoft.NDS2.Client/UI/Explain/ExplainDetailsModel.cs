﻿using Luxoft.NDS2.Client.UI.Controls.Sur;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using System;

namespace Luxoft.NDS2.Client.UI.Explain
{
    public class ExplainDetailsModel
    {
        # region .ctor

        public ExplainDetailsModel(
            ExplainDetailsData explainData,
            DeclarationSummary declarationData,
            DictionarySur surValuesDictionary)
        {
            if (explainData == null)
            {
                throw new ArgumentNullException("explainData");
            }

            if (declarationData == null)
            {
                throw new ArgumentNullException("declarationData");
            }

            if (surValuesDictionary == null)
            {
                throw new ArgumentNullException("surValuesDictionary");
            }

            _data = explainData;
            _declarationData = declarationData;
            _surValuesDictionary = surValuesDictionary;
            Comment = _data.Comment;
        }

        # endregion

        # region Данные пояснения/ответа, требования/истребования

        private readonly ExplainDetailsData _data;

        public long ExplainId
        {
            get
            {
                return _data.Id;
            }
        }

        public long ExplainZip
        {
            get
            {
                return _data.Zip;
            }
        }

        public string ExplainNumber
        {
            get
            {
                return _data.Number;
            }
        }

        public DateTime ExplainDate
        {
            get
            {
                return _data.Date;
            }
        }

        public string DocumentStatus
        {
            get
            {
                return _data.Status;
            }
        }

        public string DocumentNumber
        {
            get
            {
                return _data.ClaimNumber;
            }
        }

        public long DocumentId
        {
            get
            {
                return _data.ClaimId;
            }
        }

        public DateTime? DocumentDate
        {
            get
            {
                return _data.ClaimDate;
            }
        }

        # endregion

        # region Данные декларации и налогоплательщика

        private readonly DeclarationSummary _declarationData;

        private readonly DictionarySur _surValuesDictionary;

        public long DeclarationId
        {
            get
            {
                return _declarationData.DECLARATION_VERSION_ID;
            }
        }

        public string TaxPayerName
        {
            get
            {
                return _declarationData.NAME;
            }
        }

        public string TaxPayerInn
        {
            get
            {
                return _declarationData.INN_CONTRACTOR;
            }
        }

        public string TaxPayerKpp
        {
            get
            {
                return _declarationData.KPP;
            }
        }

        public string TaxPayerKppEffecive
        {
            get
            {
                return _declarationData.KPP_EFFECTIVE;
            }
        }

        public string CorrectionNumber
        {
            get
            {
                return _declarationData.CORRECTION_NUMBER;
            }
        }

        public string FullTaxPeriod
        {
            get
            {
                return _declarationData.FULL_TAX_PERIOD;
            }
        }

        public DictionarySur TaxPayerSurDictionary
        {
            get
            {
                return _surValuesDictionary;
            }
        }

        public int? TaxPayerSur
        {
            get
            {
                return _declarationData.SUR_CODE;
            }
        }

        #endregion

        #region Комментаий инспектора

        public string Comment
        {
            get;
            set;
        }

        public bool CommentChanged
        {
            get
            {
                return Comment != _data.Comment;
            }
        }

        #endregion
    }
}
