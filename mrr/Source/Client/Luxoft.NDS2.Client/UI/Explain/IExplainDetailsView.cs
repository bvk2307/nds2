﻿using Luxoft.NDS2.Client.UI.Controls.Sur;
using System;

namespace Luxoft.NDS2.Client.UI.Explain
{
    public interface IExplainDetailsView : IViewBase
    {
        # region Основные

        event EventHandler Opened;

        event EventHandler Closed;

        # endregion

        # region Данные пояснения

        string ExplainNumber { set; }

        string ExplainDate { set; }

        string ExplainStatus { set; }

        # endregion

        # region Данные требования\истребования

        string DocumentNumber { set; }

        string DocumentDate { set; }

        event EventHandler ClaimDetailsOpening;

        # endregion

        # region Данные декларации

        event EventHandler DeclarationOpening;

        string CorrectionNumber { set; }

        string TaxPeriod { set; }

        # endregion      

        # region Данные налогоплательщика

        string TaxPayerName { set; }

        string TaxPayerInn { set; }

        string TaxPayerKpp { set; }

        DictionarySur TaxPayerSurValuesDictionary { set; }

        int? TaxPayerSur { set; }        

        # endregion

        # region Комментарий инспектора

        string Comment { get; set; }

        event EventHandler CommentChanged;

        # endregion
    }
}
