﻿using Luxoft.NDS2.Client.UI.Base.Navigation;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Common.Models.KnpDocuments;
using Microsoft.Practices.CompositeUI;

namespace Luxoft.NDS2.Client.UI.Explain.Manual
{
    public class ManualExplainDetailsViewer : IKnpDocumentDetailsViewer
    {
        private readonly WorkItem _workItem;

        public ManualExplainDetailsViewer(WorkItem workItem)
        {
            _workItem = workItem;
        }

        public bool Supports(KnpDocument document)
        {
            return document is ManualExplainModel;
        }

        private bool GetData(long id, out ExplainReplyInfo data)
        {
            return 
                _workItem
                    .GetWcfServiceProxy<IExplainReplyDataService>()
                    .ExecuteNoHandling(proxy => proxy.GetExplainReplyInfo(id), out data);
        }

        private bool GetDeclarationData(long zip, out DeclarationSummary declaration)
        {
            return
                _workItem
                    .GetWcfServiceProxy<IDeclarationsDataService>()
                    .ExecuteNoHandling(proxy => proxy.GetDeclaration(zip), out declaration);
        }

        public void View(KnpDocument document)
        {
            var manualExplain = (ManualExplainModel) document;
            if(manualExplain != null)
                Open(manualExplain.GetId(), ExplainRegim.View, manualExplain.TypeName);
        }

        public void Edit(KnpDocument document)
        {
            var manualExplain = (ManualExplainModel)document;
            if (manualExplain != null)
                Open(manualExplain.GetId(), ExplainRegim.Edit, manualExplain.TypeName);
        }

        private void Open(long id, ExplainRegim mode, string typeName)
        {
            var notifier = _workItem.Services.Get<INotifier>(true);

            ExplainReplyCardData data = new ExplainReplyCardData(id, mode);
            ExplainReplyInfo explainData;
            DeclarationSummary declarationData;
            if (!GetData(id, out explainData)
                || !GetDeclarationData(explainData.DECLARATION_VERSION_ID, out declarationData))
            {
                notifier.ShowError("Ошибка загрузки данных");
                return;
            }
            data.DeclarationSummary = declarationData;
            data.ExplainReplyInfo = explainData;


            _workItem.Show<ExplainReplyCardPresenter, View>(
                (px, wi) => new object[] { px, wi, data },
                (px, wi, vw) => new ExplainReplyCardPresenter(vw, px, wi, data),
                string.Format("{0} ({1})", declarationData.INN, typeName));            
        }
    }
}
