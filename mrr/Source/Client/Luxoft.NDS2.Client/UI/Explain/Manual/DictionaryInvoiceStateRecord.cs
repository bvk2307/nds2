﻿using Luxoft.NDS2.Client.Properties;
using Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Explain.Manual
{
    public class DictionaryInvoiceStateRecord
    {
        private Dictionary<int, Image> _icons;

        public ExplainInvoiceState InvoiceState { get; set; }

        public string Description { get; set; }

        public int ColorARGB { get; set; }

        public DictionaryInvoiceStateRecord()
        {
            _icons = new Dictionary<int, Image>();
        }

        public Image GetIcon(Control owner)
        {
            if (!_icons.ContainsKey(ColorARGB))
            {
                lock (_icons)
                {
                    if (!_icons.ContainsKey(ColorARGB))
                    {
                        _icons[ColorARGB] = DrawIcon(owner);
                    }
                }
            }
            return _icons[ColorARGB];
        }

        private Image DrawIcon(Control owner)
        {
            using (var stream = new MemoryStream())
            using (var hdc = owner.CreateGraphics())
            {
                var rectangle = new Rectangle(0, 0, 16, 16);
                var emf = new Metafile(stream, hdc.GetHdc(), rectangle, MetafileFrameUnit.Pixel, EmfType.EmfPlusOnly);
                using (var graphics = Graphics.FromImage(emf))
                using (var brush = new SolidBrush(Color.FromArgb(ColorARGB)))
                {
                    graphics.FillRectangle(Brushes.Transparent, rectangle);
                    if (InvoiceState == ExplainInvoiceState.ChangingInThisExplain)
                    {
                        graphics.DrawImage(Resources.edit_yellow_Best, rectangle);
                    }
                    else if (InvoiceState == ExplainInvoiceState.ChangingInPreviousExplain)
                    {
                        graphics.DrawImage(Resources.edit_gray_22x22, rectangle);
                    }
                    else if (InvoiceState == ExplainInvoiceState.ProcessedInThisExplain)
                    {
                        graphics.DrawImage(Resources.check_green_48x48, rectangle);
                    }
                    else if (InvoiceState == ExplainInvoiceState.ProcessedInPreviousExplain)
                    {
                        graphics.DrawImage(Resources.check_gray_48, rectangle);
                    }
                }
                return emf;
            }
        }
    }
}
