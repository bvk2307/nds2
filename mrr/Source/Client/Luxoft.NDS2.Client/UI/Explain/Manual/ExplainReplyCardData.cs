﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;

namespace Luxoft.NDS2.Client.UI.Explain.Manual
{
    public class ExplainReplyCardData
    {
        public ExplainReplyCardData()
        {
            Regim = ExplainRegim.View;
        }

        public ExplainReplyCardData(long explainId, ExplainRegim mode)
            : this()
        {
            ExplainReplyId = explainId;
            Regim = mode;
        }

        public ExplainReplyInfo ExplainReplyInfo { get; set; }
        public DeclarationSummary DeclarationSummary { get; set; }
        public long ExplainReplyId { get; private set; }
        public ExplainRegim Regim { get; set; }
        public Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.UserAccessOpearation UserAccessOpearation { get; set; }
    }
}
