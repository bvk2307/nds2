﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Practices.CompositeUI;
using Microsoft.Practices.CompositeUI.Commands;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using CommonComponents.Uc.Infrastructure.Interface.UI;
using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.Model.Bargain;
using Luxoft.NDS2.Client.Model.Curr;
using Luxoft.NDS2.Client.Model.Oper;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.EventArguments;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Helpers;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;
using Luxoft.NDS2.Client.UI.DiscrepancyDocuments.Models;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;

namespace Luxoft.NDS2.Client.UI.Explain.Manual
{
    public partial class View : BaseView
    {
        #region Переменные

        private ExplainReplyCardPresenter _presenter;
        private ExplainReplyCardData _exlpainReplyCardData;
        private Dictionary<int, GridControl> _grids = new Dictionary<int,GridControl>();
        private Dictionary<int, List<string>> _columnInChangable = new Dictionary<int, List<string>>();
        private Dictionary<int, List<string>> _columnIsRequiredFill = new Dictionary<int, List<string>>();
        private Dictionary<int, DeclarationPart> chapterTabs;
        private ConditionValueAppearance _conditionValueAppearance;
        private bool isInitializeComplete = false;
        private Timer _timerAfterLoadGrid = new Timer() { Interval = 100, Enabled = false };
        private Timer _timerLoadChapter = new Timer() { Interval = 100, Enabled = false };
        private string FIELD_EMPTY = "<пусто>";

        private int widthInvoice = 40;
        private int widthDate = 45;
        private int widthInn = 30;
        private int widthKpp = 30;
        private int widthName = 300;
        private int widthMoney = 100;

        private UcRibbonButtonToolContext btnUpdate;
        private UcRibbonButtonToolContext btnSendToWork;
        private UcRibbonButtonToolContext btnCancelAllChange;
        private UcRibbonButtonToolContext btnCommit;
        private UcRibbonButtonToolContext btnChange;
        private UcRibbonButtonToolContext btnCancel;

        private bool isSelectOneRowWithoutStateChanging = false;
        private bool isSelectAnyRowWithoutStateProcessed = false;
        private bool isSelectAnyRowStateChangingOrProcessed = false;
        private bool isExistsInvoiceChangeOfExplain = false;

        private string dateFmt = CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern;

        private Dictionary<ExplainRegim, string> _titleGridRazdels = new Dictionary<ExplainRegim, string>()
        {
            {ExplainRegim.View, "Подтвержденные/отредактированные записи о СФ" },
            {ExplainRegim.Edit, "Записи о СФ, содержащие расхождения для КНП" }
        };

        #endregion

        public View()
        {
            InitializeComponent();
        }

        public View(PresentationContext ctx, WorkItem wi, ExplainReplyCardData exlpainReplyCardData)
            : base(ctx, wi)
        {
            InitializeComponent();
            _exlpainReplyCardData = exlpainReplyCardData;
            _presenter = new ExplainReplyCardPresenter(this, ctx, wi, exlpainReplyCardData);

            _timerAfterLoadGrid.Tick += TimerAfterLoadGrid_Tick;
            _timerLoadChapter.Tick += TimerLoadChapter_Tick;

            SetDataEmpty();

            InitDictionaryChapterGrid();
            InitChapterTabs();
            SetupAllGridBandEditMode();
            SetupAllGridPanelVisible(false);
            SetupAllGridMultiSelect();

            InitializeGridR8();
            InitializeGridR9();
            InitializeGridR10();
            InitializeGridR11();
            InitializeGridR12();

            InitializeRibbon();
            isInitializeComplete = true;

            _presenter.LoadMainData();
        }

        #region Инициализация

        private void InitDictionaryChapterGrid()
        {
            _grids.Add((int)DeclarationPart.R8, gridR8);
            _grids.Add((int)DeclarationPart.R9, gridR9);
            _grids.Add((int)DeclarationPart.R10, gridR10);
            _grids.Add((int)DeclarationPart.R11, gridR11);
            _grids.Add((int)DeclarationPart.R12, gridR12);

        }

        private void InitChapterTabs()
        {
            chapterTabs = new Dictionary<int, DeclarationPart>
            {
                {tabR8.Tab.VisibleIndex, DeclarationPart.R8}, 
                {tabR9.Tab.VisibleIndex, DeclarationPart.R9}, 
                {tabR10.Tab.VisibleIndex, DeclarationPart.R10}, 
                {tabR11.Tab.VisibleIndex, DeclarationPart.R11}, 
                {tabR12.Tab.VisibleIndex, DeclarationPart.R12},
            };
        }

        private void SetupAllGridPanelVisible(bool visibled)
        {
            gridR8.PanelLoadingVisible = visibled;
            gridR9.PanelLoadingVisible = visibled;
            gridR10.PanelLoadingVisible = visibled;
            gridR11.PanelLoadingVisible = visibled;
            gridR12.PanelLoadingVisible = visibled;
        }

        private void SetupAllGridBandEditMode()
        {
            foreach (KeyValuePair<int, GridControl> item in _grids)
            {
                item.Value.Band_EditMode = true;
                if (_exlpainReplyCardData.Regim == ExplainRegim.Edit)
                {
                    item.Value.Band_GridRegimEdit = GridRegimEdit.Edit;
                }
                else if (_exlpainReplyCardData.Regim == ExplainRegim.View)
                {
                    item.Value.Band_GridRegimEdit = GridRegimEdit.View;
                }
            }
        }

        private void SetupAllGridMultiSelect()
        {
            foreach (KeyValuePair<int, GridControl> item in _grids)
            {
                if (_exlpainReplyCardData.Regim == ExplainRegim.Edit)
                {
                    item.Value.SetupMultiSelect(true);
                }
                else if (_exlpainReplyCardData.Regim == ExplainRegim.View)
                {
                    item.Value.SetupMultiSelect(false);
                }
            }
        }

        #endregion

        #region Получение ГридКонтролов

        public GridControl GetGridInvoices(int num)
        {
            GridControl ret = null;
            if (_grids.ContainsKey(num))
            {
                ret = _grids[num];
            }
            return ret;
        }

        public GridControl GetCurrentGridInvoices()
        {
            GridControl grid = null;
            int tabIndex = ultraTabControlMain.SelectedTab.Index;
            if (chapterTabs.Keys.Contains(tabIndex))
            {
                DeclarationPart partIndex = chapterTabs[tabIndex];
                grid = GetGridInvoices((int)partIndex);
            }
            return grid;
        }

        public int GetCurrentChapter()
        {
            int ret = -1;
            int tabIndex = ultraTabControlMain.SelectedTab.Index;
            if (chapterTabs.Keys.Contains(tabIndex))
            {
                DeclarationPart partIndex = chapterTabs[tabIndex];
                ret = (int)partIndex;
            }
            return ret;
        }

        #endregion

        #region Обновление ГридКонтролов

        private void TimerAfterLoadGrid_Tick(object sender, EventArgs e)
        {
            _timerAfterLoadGrid.Enabled = false;
            CheckEnabledAllRibbonButton();
        }

        private void TimerLoadChapter_Tick(object sender, EventArgs e)
        {
            _timerLoadChapter.Enabled = false;
            GridControl currentgrid = GetCurrentGridInvoices();
            if (currentgrid != null)
            {
                currentgrid.UpdateData();
            }
        }

        private void UpdateCurrentInvoice(bool isReload)
        {
            if (isReload)
            {
                int chapter = GetCurrentChapter();
                _presenter.SetStateWorkerStart(chapter);
            }
            GridControl currentgrid = GetCurrentGridInvoices();
            if (currentgrid != null)
            {
                currentgrid.UpdateData();
            }
        }

        private void SetLoadingVisibleCurrentInvoice()
        {
            GridControl currentgrid = GetCurrentGridInvoices();
            if (currentgrid != null)
            {
                currentgrid.PanelLoadingVisible = true;
            }
        }

        #endregion

        #region Привязка данных

        public void SetDataEmpty()
        {
            SetHeaderDataEmpty();
            SetTaxPayerDataEmpty();
            SetHistoryStatusDataEmpty();
        }

        public void LoadData()
        {
            BindingHeader();
            BindingTaxPayerInfo(_presenter.ExplainReplyInfo, _presenter.DeclarationSummary);
            BindingHistoryStatus();
            _presenter.ExistsInvoiceChangeOfExplain();
        }

        public void SetHeaderDataEmpty()
        {
            labelTypeValue.Text = String.Empty;
            labelIncomingNumValue.Text = String.Empty;
            labelIncomingDataValue.Text = String.Empty;
            labelDocInfoNameValue.Text = String.Empty;
            labelStatusValue.Text = String.Empty;
            linkLabelDocInfoNumValue.Value = string.Concat("<a href='#'>", String.Empty, "</a>");
            labelDocInfoDataValue.Text = String.Empty;
            labelCorrectionNumberValue.Text = String.Empty;
            labelTaxPeriodValue.Text = String.Empty;
        }

        private void BindingHeader()
        {
            BindingHeaderReply(_presenter.ExplainReplyInfo);
            BindingHederDocumentInfo(_presenter.DiscrepancyDocumentInfo);
            BindingHederDeclarationInfo(_presenter.DeclarationSummary);
            SetHeaderColumnsActualWidth();
        }

        private void BindingHeaderReply(ExplainReplyInfo explainReplyInfo)
        {
            string typeName = String.Empty;
            string statusName = String.Empty;
            if (explainReplyInfo != null)
            {
                typeName = explainReplyInfo.TYPE_NAME;
                if (explainReplyInfo.TYPE_ID == ExplainReplyType.Explain)
                {
                    tableLayoutMain.ColumnStyles[1].Width = 17;
                    tableLayoutMain.ColumnStyles[2].Width = 50;
                    if (explainReplyInfo.RECEIVE_BY_TKS)
                    {
                        typeName += " (по ТКС)";
                    }
                    labelIncomingNumValue.Text = explainReplyInfo.INCOMING_NUM;
                    labelIncomingDataValue.Text = FormatDateTime(explainReplyInfo.INCOMING_DATE);
                }
                else if (explainReplyInfo.TYPE_ID == ExplainReplyType.Reply_93 ||
                         explainReplyInfo.TYPE_ID == ExplainReplyType.Reply_93_1)
                {
                    tableLayoutMain.ColumnStyles[1].Width = 0;
                    tableLayoutMain.ColumnStyles[2].Width = 0;
                    labelIncomingDataValue.Text = FormatDateTime(explainReplyInfo.INCOMING_DATE);
                }
                statusName = explainReplyInfo.status_name;
            }

            labelTypeValue.Text = typeName;
            labelStatusValue.Text = statusName;
        }

        private void SetHeaderColumnsActualWidth()
        {
            Size len;
            len = TextRenderer.MeasureText(labelTypeValue.Text, labelTypeValue.Font);
            tableLayoutMain.ColumnStyles[0].Width = len.Width + 2;

            len = TextRenderer.MeasureText(labelIncomingNumValue.Text, labelIncomingNumValue.Font);
            tableLayoutMain.ColumnStyles[2].Width = len.Width;

            len = TextRenderer.MeasureText(labelSimvolNumber.Text, labelSimvolNumber.Font);
            tableLayoutMain.ColumnStyles[3].Width = len.Width;

            len = TextRenderer.MeasureText(labelDocInfoNameValue.Text, labelDocInfoNameValue.Font);
            tableLayoutMain.ColumnStyles[7].Width = len.Width + 2;

            len = TextRenderer.MeasureText(linkLabelDocInfoNumValue.Text, linkLabelDocInfoNumValue.Font);
            tableLayoutMain.ColumnStyles[9].Width = len.Width;

            len = TextRenderer.MeasureText(labelStatusValue.Text, labelStatusValue.Font);
            tableLayoutMain.ColumnStyles[6].Width = len.Width + 20;
        }

        private void BindingHederDocumentInfo(DiscrepancyDocumentInfo docInfo)
        {
            string docInfoName = String.Empty;
            string docInfoNum = String.Empty;
            string docInfoDate = String.Empty;

            Dictionary<int, string> dictDocType = new Dictionary<int, string>();
            dictDocType.Add(DocType.ClaimSF, "к автотребованию");
            dictDocType.Add(DocType.Reclaim93, "к автоистребованию");
            dictDocType.Add(DocType.Reclaim93_1, "к автоистребованию");

            if (docInfo != null)
            {
                if (docInfo.DocType != null)
                {
                    if (dictDocType.ContainsKey(docInfo.DocType.EntryId))
                    {
                        docInfoName = dictDocType[docInfo.DocType.EntryId];
                    }
                }
                docInfoNum = docInfo.EodId;
                docInfoDate = FormatDateTime(docInfo.SeodAcceptDate);
            }

            labelDocInfoNameValue.Text = docInfoName;
            linkLabelDocInfoNumValue.Value = String.Concat("<a href='#'>", docInfoNum, "</a>");
            labelDocInfoDataValue.Text = String.IsNullOrEmpty(docInfoDate) ? String.Empty : String.Concat("от ", docInfoDate);
        }

        private void BindingHederDeclarationInfo(DeclarationSummary declaration)
        {
            string correctionNum = String.Empty;
            string taxPayerPeriod = String.Empty;
            if (declaration != null)
            {
                correctionNum = declaration.CORRECTION_NUMBER_EFFECTIVE;
                taxPayerPeriod = declaration.FULL_TAX_PERIOD;
            }
            labelCorrectionNumberValue.Text = correctionNum;
            labelTaxPeriodValue.Text = taxPayerPeriod;
        }

        private void BindingTaxPayerInfo(ExplainReplyInfo explainReplyInfo, DeclarationSummary BindingDeclaration)
        {
            SetTaxPayerDataDeclaration(BindingDeclaration);
            SetUserComment(explainReplyInfo.user_comment);
        }

        private void SetHistoryStatusDataEmpty()
        {
            labelStatusNotProcessedValue.Text = String.Empty;
            labelStatusSendToMCValue.Text = String.Empty;
            labelStatusProcessedInMCValue.Text = String.Empty;
            labelStatusSendToIniciatorValue.Text = String.Empty;
            labelStatusRecieveIniciatorValue.Text = String.Empty;
        }

        private void BindingHistoryStatus()
        {
            List<ExplainReplyHistoryStatus> histories = _presenter.ExplainReplyHistoryStatuses;

            labelStatusNotProcessedValue.Text = GetHistoryStatusDate(histories, ExplainReplyStatus.NotProcessed);
            labelStatusSendToMCValue.Text = GetHistoryStatusDate(histories, ExplainReplyStatus.SendToMC);
            labelStatusProcessedInMCValue.Text = GetHistoryStatusDate(histories, ExplainReplyStatus.ProcessedInMC);

            ExplainReplyInfo explainReplyInfo = _presenter.ExplainReplyInfo;
            if (explainReplyInfo != null &&
                explainReplyInfo.TYPE_ID == ExplainReplyType.Reply_93_1)
            {
                labelStatusSendToIniciatorCaption.Visible = true;
                labelStatusRecieveIniciatorCaption.Visible = true;
                labelStatusSendToIniciatorValue.Visible = true;
                labelStatusRecieveIniciatorValue.Visible = true;
                labelStatusSendToIniciatorValue.Text = GetHistoryStatusDate(histories, ExplainReplyStatus.SendToIniciator);
                labelStatusRecieveIniciatorValue.Text = GetHistoryStatusDate(histories, ExplainReplyStatus.RecieveIniciator);
            }
            else
            {
                labelStatusSendToIniciatorCaption.Visible = false;
                labelStatusRecieveIniciatorCaption.Visible = false;
                labelStatusSendToIniciatorValue.Visible = false;
                labelStatusRecieveIniciatorValue.Visible = false;
            }
        }

        private string GetHistoryStatusDate(List<ExplainReplyHistoryStatus> histories, int statusId)
        {
            string ret = String.Empty;
            ExplainReplyHistoryStatus status = histories.Where(p => p.Status_id == statusId).OrderByDescending(p => p.Submit_date).FirstOrDefault();
            if (status != null)
            {
                ret = FormatDateTime(status.Submit_date);
            }
            return ret;
        }

        private string FormatDateTime(DateTime? dt)
        {
            string ret = String.Empty;
            if (dt != null)
            {
                ret = dt.Value.ToShortDateString();
            }
            return ret;
        }

        private void SetTaxPayerDataEmpty()
        {
            LinkLabelNameValue.Value = string.Concat("<a href='#'>", String.Empty, "</a>");
            labelInnValue.Text = String.Empty;
            labelKPPValue.Text = String.Empty;
        }

        private void SetTaxPayerDataDeclaration(DeclarationSummary declaration)
        {
            if (declaration != null)
            {
                LinkLabelNameValue.Value = string.Concat("<a href='#'>", declaration.NAME, "</a>");
                labelInnValue.Text = declaration.INN;
                labelKPPValue.Text = declaration.KPP;
            }
            SetSURInfo(declaration);
        }

        private void SetUserComment(string userComment)
        {
            if (userComment != null)
            {
                teComment.Text = userComment;
            }
            else
            {
                teComment.Text = String.Empty;
            }
        }

        private void SetSURInfo(DeclarationSummary declaration)
        {
            _surIndicator.SetSurDictionary(_presenter.Sur);
            _surIndicator.Code = declaration.SUR_CODE;
        }

        #endregion

        #region Вспомогательные функции для разделов

        private void GeneratePropertyFromColumns(List<ColumnGroupDefinition> columns, List<string> columnsInChangable,
            List<string> columnsIsRequiredFill, Dictionary<string, string> bandCellToolTips)
        {
            ColumnDefinition columnDef;
            ColumnGroupDefinition columnGroupDef;
            foreach (ColumnBase elem in columns)
            {
                columnDef = elem as ColumnDefinition;
                if (columnDef != null)
                {
                    if (columnDef.IsUseInChangable)
                    {
                        columnsInChangable.Add(columnDef.Key);
                    }
                    if (columnDef.IsRequiredFill)
                    {
                        columnsIsRequiredFill.Add(columnDef.Key);
                    }
                    if (!string.IsNullOrEmpty(columnDef.BandCellToolTip))
                    {
                        bandCellToolTips.Add(columnDef.Key, columnDef.BandCellToolTip);
                    }   
                }
                else
                {
                    columnGroupDef = elem as ColumnGroupDefinition;
                    if (columnGroupDef != null)
                    {
                        foreach (ColumnDefinition itemColumnDef in columnGroupDef.Columns)
                        {
                            if (itemColumnDef.IsUseInChangable)
                            {
                                columnsInChangable.Add(itemColumnDef.Key);
                            }
                            if (itemColumnDef.IsRequiredFill)
                            {
                                columnsIsRequiredFill.Add(itemColumnDef.Key);
                            }
                            if (!string.IsNullOrEmpty(itemColumnDef.BandCellToolTip))
                            {
                                bandCellToolTips.Add(itemColumnDef.Key, itemColumnDef.BandCellToolTip);
                            }                     
                        }
                    }
                }
            }
        }

        private object GetFieldEntity(Dictionary<string, object> filedsEntity, string fieldKey)
        {
            object fieldValue = null;
            if (filedsEntity.ContainsKey(fieldKey))
            {
                fieldValue = filedsEntity[fieldKey];
            }
            return fieldValue;
        }

        public void GridRowUpdate(int chapter, UltraGridRow rowParent)
        {
            foreach (UltraGridChildBand itemBand in rowParent.ChildBands)
            {
                foreach (UltraGridRow row in itemBand.Rows)
                {
                    GridRowAction(chapter, row, false);
                }
            }
        }

        private void GridCellAction(int chapter, UltraGridCell cell, bool isUpdate)
        {
            if (cell.Row.ListObject is ExplainInvoice)
            {
                ExplainInvoice explainInv = (ExplainInvoice)cell.Row.ListObject;
                if (explainInv.Type == ExplainInvoiceType.Correct)
                {
                    GridCellActionBase(chapter, cell, isUpdate, 
                        GenerateFiledsEntityOriginal(explainInv), 
                        explainInv,
                        GenerateFiledsEntityLastChanged(explainInv));
                }
            }
        }

        private Dictionary<string, object> GenerateFiledsEntityOriginal(ExplainInvoice explainInv)
        {
            Dictionary<string, object> filedsEntityOriginal = null;
            if (explainInv.Parent.State == ExplainInvoiceState.ChangingInThisExplain)
            {
                filedsEntityOriginal = _presenter.ConvertToDictionary(explainInv.Parent.PreviousInvoiceChange);
            }
            else
            {
                filedsEntityOriginal =  _presenter.ConvertToDictionary(explainInv.Parent);
            }
            return filedsEntityOriginal;
        }

        private Dictionary<string, object> GenerateFiledsEntityLastChanged(ExplainInvoice explainInv)
        {
            if (explainInv.LastInvoiceChange != null)
                return _presenter.ConvertToDictionary(explainInv.LastInvoiceChange);
            else
                return _presenter.ConvertToDictionary(explainInv.Parent);
        }

        private void GridCellActionBase(int chapter, UltraGridCell cell, bool isUpdate, 
            Dictionary<string, object> filedsEntityOriginal, 
            ExplainInvoice explainInv,
            Dictionary<string, object> fieldsEntityLastChanged)
        {
            var appearanceActiveRow = cell.Band.Override.ActiveRowAppearance;
            var appearanceActiveCell = cell.Band.Override.ActiveCellAppearance;
            appearanceActiveRow.Reset();
            appearanceActiveCell.Reset();

            string columnKey = cell.Column.Key;
            if (_columnInChangable[chapter].Contains(columnKey) && explainInv.Parent != null)
            {
                if (_exlpainReplyCardData.Regim == ExplainRegim.Edit &&
                    explainInv.Parent.State == ExplainInvoiceState.ChangingInThisExplain)
                {
                    cell.Column.CellClickAction = CellClickAction.Edit;
                    cell.Column.CellActivation = Activation.AllowEdit;
                }
                else
                {
                    cell.Column.CellClickAction = CellClickAction.CellSelect;
                    cell.Column.CellActivation = Activation.NoEdit;
                }

                bool isFieldcorrect = IsFieldCorrect(chapter, columnKey, cell.Value);
                if (_columnIsRequiredFill[chapter].Contains(columnKey) &&
                    (cell.Value == null || (cell.Value != null && string.IsNullOrWhiteSpace(cell.Value.ToString()))))
                {
                    cell.Value = FIELD_EMPTY;
                }
                object fieldValue = cell.Value;
                object fieldValueOriginal = GetFieldEntity(filedsEntityOriginal, columnKey);
                bool isEqals = IsEqalsFieldValue(fieldValueOriginal, fieldValue);
                bool isClearFieldValue = IsClearFieldValue(fieldValueOriginal, fieldValue);

                if (isClearFieldValue)
                {
                    object fieldValueLastChanged = GetFieldEntity(fieldsEntityLastChanged, columnKey);
                    cell.Value = ConvertValueToString(fieldValueLastChanged);
                    ShowWarning("Удаление данных невозможно");
                    return;
                }

                if (!isEqals)
                {
                    if (isFieldcorrect)
                    {
                        if (explainInv.Parent.State == ExplainInvoiceState.ChangingInPreviousExplain)
                        {
                            cell.Appearance.ForeColor = System.Drawing.Color.Blue;
                            cell.Appearance.FontData.Bold = DefaultableBoolean.True;
                        }
                        else if (explainInv.Parent.State == ExplainInvoiceState.ChangingInThisExplain)
                        {
                            cell.Appearance.ForeColor = System.Drawing.Color.Green;
                            cell.Appearance.FontData.Bold = DefaultableBoolean.True;
                        }
                        else
                        {
                            cell.Appearance.ForeColor = System.Drawing.Color.Gray;
                            cell.Appearance.FontData.Bold = DefaultableBoolean.True;
                        }
                        if (isUpdate)
                        {
                            if (explainInv.Parent.State == ExplainInvoiceState.ChangingInThisExplain)
                            {
                                _presenter.UpdateInvoiceCorrect(explainInv, explainInv.ROW_KEY, columnKey, fieldValue, InvoiceCorrectTypeOperation.AddOrUpdate);
                            }
                        }
                    }
                    else
                    {
                        cell.Appearance.ForeColor = System.Drawing.Color.Red;
                        cell.Appearance.FontData.Bold = DefaultableBoolean.True;
                    }
                }
                else
                {
                    cell.Appearance.ResetForeColor();
                    cell.Appearance.ResetFontData();
                    if (isUpdate)
                    {
                        _presenter.UpdateInvoiceCorrect(explainInv, explainInv.ROW_KEY, columnKey, fieldValue, InvoiceCorrectTypeOperation.Remove);
                    }
                }
            }
            else
            {
                cell.Column.CellClickAction = CellClickAction.CellSelect;
                cell.Column.CellActivation = Activation.NoEdit;
            }
            cell.Refresh();

            if (isUpdate)
            {
                RefreshGridRowParent(cell);
            }
        }

        private bool IsEqalsFieldValue(object fieldValueOriginal, object fieldValue)
        {
            bool isEqals = true;
            if (fieldValueOriginal != null)
            {
                if (!fieldValueOriginal.Equals(fieldValue)) { isEqals = false; }
                else { isEqals = true; }
            }
            else if (fieldValue != null) { isEqals = false; }
            else { isEqals = true; }
            return isEqals;
        }

        private bool IsClearFieldValue(object fieldValueOriginal, object fieldValue)
        {
            bool isClearFieldValue = false;

            string stringOriginalValue = ConvertValueToString(fieldValueOriginal);
            string stringValue = ConvertValueToString(fieldValue);

            if (!string.IsNullOrWhiteSpace(stringOriginalValue) &&
                (string.IsNullOrWhiteSpace(stringValue) || stringValue == FIELD_EMPTY))
                isClearFieldValue = true;

            return isClearFieldValue;
        }

        private string ConvertValueToString(object value)
        {
            string stringValue = String.Empty;
            if (value != null)
            {
                if (value is string)
                    stringValue = (string)value;
                else
                    stringValue = value.ToString();
            }
            return stringValue;
        }

        private bool IsFieldCorrect(int chapter, string key, object value)
        {
            bool isFieldcorrect = false;

            if (value != null)
            {
                string typ = value.GetType().ToString();
                if (typ == "System.Decimal" || typ == "System.DateTime" && value != null)
                {
                    isFieldcorrect = true;
                    return isFieldcorrect;
                }
            }
            string valueString = (string)value;
            if (_columnIsRequiredFill[chapter].Contains(key) &&
                string.IsNullOrWhiteSpace(valueString) || valueString == FIELD_EMPTY)
            {
                isFieldcorrect = false;
            }
            else
            {
                isFieldcorrect = _presenter.CheckFieldCorrectInvoice(chapter, key, value);
            }
            return isFieldcorrect;
        }

        private void RefreshGridRowParent(UltraGridCell cell)
        {
            UltraGridRow rowParent = cell.Row.ParentRow;
            if (rowParent != null)
            {
                UltraGridCell cellParent = rowParent.Cells[TypeHelper<ExplainInvoice>.GetMemberName(t => t.State)];
                if (cellParent != null)
                {
                    cellParent.Refresh();
                }
            }
        }

        private void GridRowAction(int chapter, UltraGridRow row, bool isUpdate)
        {
            if (row.ListObject is ExplainInvoice)
            {
                ExplainInvoice explainInv = (ExplainInvoice)row.ListObject;
                if (explainInv.Type == ExplainInvoiceType.Correct)
                {
                    var filedsEntityOriginal = GenerateFiledsEntityOriginal(explainInv);
                    var fieldsEntityLastChanged = GenerateFiledsEntityLastChanged(explainInv);
                    foreach (var cell in row.Cells)
                    {
                        GridCellActionBase(chapter, cell, isUpdate, filedsEntityOriginal, explainInv, fieldsEntityLastChanged);
                    }
                    var cellFirst = row.Cells[TypeHelper<ExplainInvoice>.GetMemberName(t => t.State)];
                    cellFirst.Appearance.Reset();
                    cellFirst.Appearance.BorderAlpha = Alpha.Transparent;
                    cellFirst.Appearance.BorderColor = Color.White;
                    CheckAttributesInnIfMany(row, explainInv);
                }
            }
        }

        private void CheckAttributesInnIfMany(UltraGridRow row, ExplainInvoice exlplainInvoice)
        {
            if (exlplainInvoice.Attributes.Sellers.Where(p => !string.IsNullOrWhiteSpace(p.Inn)).Count() > 1)
            {
                var cellSellerInn = row.Cells[TypeHelper<ExplainInvoice>.GetMemberName(t => t.SELLER_INN)];
                CellSetNoEdit(cellSellerInn);
            }
            if (exlplainInvoice.Attributes.Buyers.Where(p => !string.IsNullOrWhiteSpace(p.Inn)).Count() > 1)
            {
                var cellBuyerInn = row.Cells[TypeHelper<ExplainInvoice>.GetMemberName(t => t.BUYER_INN)];
                CellSetNoEdit(cellBuyerInn);
            }
        }

        private void CellSetNoEdit(UltraGridCell cell)
        {
            cell.Column.CellClickAction = CellClickAction.CellSelect;
            cell.Column.CellActivation = Activation.NoEdit;
        }

        private void GridDrawInvoiceState(ColumnDefinition column)
        {
            column.Caption = String.Empty;
            column.ToolTip = "Признак редактирования записи в ходе полученного пояснения/ответа от НП";
            column.FormatInfo = new IntValueZeroFormatter();
            column.IsUseInChangable = false;
            column.CustomConditionValueAppearance = () =>
            {
                return _conditionValueAppearance;
            };
        }

        private void InitConditionValueAppearance()
        {
            if (null == _conditionValueAppearance)
            {
                _conditionValueAppearance = new ConditionValueAppearance();
                foreach (var typeRec in _presenter.InvoiceStateRecords)
                {
                    var condition = new OperatorCondition(ConditionOperator.Equals, typeRec.InvoiceState);
                    var appearance = new Infragistics.Win.Appearance
                    {
                        Image = typeRec.GetIcon(this),
                        ImageHAlign = HAlign.Center,
                        ImageVAlign = VAlign.Middle,
                        ForegroundAlpha = Alpha.Transparent
                    };
                    _conditionValueAppearance.Add(condition, appearance);
                }
            }
        }


        # region CellToolTip

        DictionaryOper dicOper = new DictionaryOper();
        DictionaryCurr dicCurr = new DictionaryCurr();
        DictionaryBargain dicBargain = new DictionaryBargain();
        void InitDict()
        {
            dicOper = _presenter.Oper;
            dicCurr = _presenter.Curr;
            dicBargain = _presenter.Barg;
        }
        private string InvoiceConvert(object inv)
        {
            ExplainInvoice temp = (ExplainInvoice)inv;
            string opcode = "";
            opcode = temp.OPERATION_CODE;
            return dicOper.NAME(opcode);
        }

        private string InvoiceCurrencyConvert(object inv)
        {
            ExplainInvoice temp = (ExplainInvoice)inv;
            string opcode = "";
            opcode = temp.OKV_CODE;
            return dicCurr.NAME(opcode);
        }

        private string InvoiceBargainConvert(object inv)
        {
            ExplainInvoice temp = (ExplainInvoice)inv;
            string opcode = "";
            opcode = temp.DEAL_KIND_CODE;
            return dicBargain.NAME(opcode);
        }

        private Dictionary<string, Func<object, string>> GetCellToolTipsBase()
        {
            Dictionary<string, Func<object, string>> cellToolTips = new Dictionary<string, Func<object, string>>();

            InitDict();
            Func<object, string> funcOper = InvoiceConvert;
            Func<object, string> funcCurr = InvoiceCurrencyConvert;
            Func<object, string> funcBarg = InvoiceBargainConvert;
            cellToolTips.Add("OPERATION_CODE", funcOper);
            cellToolTips.Add("OKV_CODE", funcCurr);
            cellToolTips.Add("DEAL_KIND_CODE", funcBarg);

            cellToolTips.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.State), (dataObject) =>
            {
                ExplainInvoice invoice = (ExplainInvoice)dataObject;
                if (invoice != null)
                {
                    string ret = String.Empty;
                    DictionaryInvoiceStateRecord state = _presenter.InvoiceStateRecords.Where(p => p.InvoiceState == invoice.State).FirstOrDefault();
                    if (state != null)
                    {
                        ret = state.Description;
                    }
                    return ret;
                }
                return String.Empty;
            });
            return cellToolTips;
        }

        #endregion

        private string GetTitleGridRazdel()
        {
            string ret = String.Empty; 
            if (_titleGridRazdels.ContainsKey(_exlpainReplyCardData.Regim))
            {
                ret = _titleGridRazdels[_exlpainReplyCardData.Regim];
            }
            return ret; 
        }

        private object GetDataR8_12(DeclarationPart part, string keyTab, QueryConditions qc, out long totalRowsNumber)
        {
            if (ultraTabControlMain.SelectedTab != null
                && chapterTabs.Keys.Contains(ultraTabControlMain.SelectedTab.Index)
                && chapterTabs[ultraTabControlMain.SelectedTab.Index] == part)
            {
                return _presenter.GetInvocesCached((int)part, qc, out totalRowsNumber);
            }
                
            totalRowsNumber = 0;
            List<ExplainInvoice> result = null;
            return result;
        }

        #endregion

        #region Раздел 8

        private void InitializeGridR8()
        {
            GridSetup setup = _presenter.CommonSetup(string.Format("{0}_InitializeGridExplainR8", GetType()));
            setup.GetData = this.GetDataR8;
            GridSetupHelper<ExplainInvoice> helper = new GridSetupHelper<ExplainInvoice>();

            setup.Columns.AddRange(GetGroupsR8(helper));
            setup.QueryCondition.Sorting.Add(new ColumnSort() { ColumnKey = TypeHelper<ExplainInvoice>.GetMemberName(t => t.ORDINAL_NUMBER), Order = ColumnSort.SortOrder.Asc });

            List<ColumnGroupDefinition> bandGroups = GetGroupsR8(helper);
            var band = new BandDefinition
            {
                BindWidth = true,
                ColumnsHeadersVisible = false,
                Name = helper.GetMemberName(o => o.RelatedInvoices),
                Groups = bandGroups
            };
            setup.Bands.Add(band);

            List<Action<UltraGridRow>> rowActions = new List<Action<UltraGridRow>>();
            rowActions.Add((row) =>
                {
                    GridRowAction((int)DeclarationPart.R8, row, false);
                });
            setup.RowActions = rowActions;
            setup.CellUpdateAction = (cell) =>
                {
                    GridCellAction((int)DeclarationPart.R8, cell, true);
                };

            List<string> columnsInChangable = new List<string>();
            List<string> columnsIsRequiredFill = new List<string>();
            Dictionary<string, string> bandCellToolTips = new Dictionary<string, string>();
            GeneratePropertyFromColumns(bandGroups, columnsInChangable, columnsIsRequiredFill, bandCellToolTips);
            _columnInChangable.Add((int)DeclarationPart.R8, columnsInChangable);
            _columnIsRequiredFill.Add((int)DeclarationPart.R8, columnsIsRequiredFill);

            setup.CellToolTips = GetCellToolTipsBase();
            setup.BandCellToolTips = bandCellToolTips;
            setup.AllowSaveSettings = false;

            gridR8.Setup = setup;
            gridR8.Title = GetTitleGridRazdel();
        }

        IntValueZeroFormatter formatPrices = new IntValueZeroFormatter();

        private List<ColumnGroupDefinition> GetGroupsR8(GridSetupHelper<ExplainInvoice> helper)
        {
            InitConditionValueAppearance();
            Action<ColumnDefinition> drawInvoiceState = column =>
            {
                GridDrawInvoiceState(column);
            };
            ColumnGroupDefinition gStateRecord = new ColumnGroupDefinition() { Caption = string.Empty };
            gStateRecord.Columns.Add(helper.CreateColumnDefinition(o => o.State, d =>
            {
                drawInvoiceState(d);
                d.AllowRowFiltering = false; d.SortIndicator = false;
            }));

            ColumnGroupDefinition gOrdinalNumber = new ColumnGroupDefinition() { Caption = string.Empty };
            gOrdinalNumber.Columns.Add(helper.CreateColumnDefinition(o => o.ORDINAL_NUMBER, d =>
            {
                d.IsUseInChangable = false;
                d.ToolTip = "";
                d.BandCellToolTip = "";
            }));

            ColumnGroupDefinition g0 = new ColumnGroupDefinition() { Caption = string.Empty };
            g0.Columns.Add(helper.CreateColumnDefinition(o => o.OPERATION_CODE, d =>
            {
                d.IsUseInChangable = true;
                d.BandCellToolTip = "Обязательно для заполнения. Формат ввода: XX Где XX – цифры"; d.IsRequiredFill = true;
                d.AllowRowFiltering = false;
                d.SortIndicator = false;
            }));

            ColumnGroupDefinition g1 = new ColumnGroupDefinition() { Caption = "СФ" };
            g1.Columns.Add(helper.CreateColumnDefinition(o => o.INVOICE_NUM, d => { d.Width = widthInvoice; d.ToolTip = "Номер счета-фактуры продавца"; d.IsUseInChangable = true;
            d.BandCellToolTip = "Обязательно для заполнения"; d.IsRequiredFill = true; 
            }));
            g1.Columns.Add(helper.CreateColumnDefinition(o => o.INVOICE_DATE, d => { d.Width = widthDate; d.ToolTip = "Дата счета-фактуры продавца"; d.IsUseInChangable = true;
            d.BandCellToolTip = "Не обязательно для заполнения. Формат ввода: " + dateFmt;
            }));

            ColumnGroupDefinition g2 = new ColumnGroupDefinition() { Caption = "ИСФ" };
            g2.Columns.Add(helper.CreateColumnDefinition(o => o.CHANGE_NUM, d => { d.Width = widthInvoice; d.IsUseInChangable = true;
                d.BandCellToolTip = "Не обязательно для заполнения. Ввод цифр (от 1 до 3 знаков)"; 
            }));
            g2.Columns.Add(helper.CreateColumnDefinition(o => o.CHANGE_DATE, d => { d.Width = widthDate; d.IsUseInChangable = true;
            d.BandCellToolTip = "Не обязательно для заполнения. Формат ввода: " + dateFmt;
            }));

            ColumnGroupDefinition g3 = new ColumnGroupDefinition() { Caption = "КСФ" };
            g3.Columns.Add(helper.CreateColumnDefinition(o => o.CORRECTION_NUM, d => { d.Width = widthInvoice; d.IsUseInChangable = true; d.BandCellToolTip = "Не обязательно для заполнения"; }));
            g3.Columns.Add(helper.CreateColumnDefinition(o => o.CORRECTION_DATE, d => { d.Width = widthDate; d.IsUseInChangable = true; d.BandCellToolTip = "Не обязательно для заполнения. Формат ввода: " + dateFmt; }));

            ColumnGroupDefinition g4 = new ColumnGroupDefinition() { Caption = "ИКСФ" };
            g4.Columns.Add(helper.CreateColumnDefinition(o => o.CHANGE_CORRECTION_NUM, d => { d.Width = widthInvoice; d.IsUseInChangable = true; d.BandCellToolTip = "Не обязательно для заполнения. Ввод цифр (от 1 до 3 знаков)."; }));
            g4.Columns.Add(helper.CreateColumnDefinition(o => o.CHANGE_CORRECTION_DATE, d => { d.Width = widthDate; d.IsUseInChangable = true; d.BandCellToolTip = "Не обязательно для заполнения. Формат ввода: " + dateFmt; }));

            ColumnGroupDefinition g5 = new ColumnGroupDefinition() { Caption = "Документ, подтверждающий уплату налога" };
            g5.Columns.Add(helper.CreateColumnDefinition(o => o.RECEIPT_DOC_NUM, d => { d.Width = widthInvoice + 80; d.IsUseInChangable = false;
                d.BandCellToolTip = "Не обязательно для заполнения"; d.AllowRowFiltering = false; d.SortIndicator = false; }));
            g5.Columns.Add(helper.CreateColumnDefinition(o => o.RECEIPT_DOC_DATE, d => { d.Width = widthDate + 80; d.IsUseInChangable = false;
            d.BandCellToolTip = "Не обязательно для заполнения. Формат ввода: " + dateFmt; d.AllowRowFiltering = false; d.SortIndicator = false; }));

            ColumnGroupDefinition g6 = new ColumnGroupDefinition() { Caption = string.Empty };
            g6.Columns.Add(helper.CreateColumnDefinition(o => o.BUY_ACCEPT_DATE, d => { d.Width = widthDate; d.IsUseInChangable = false;
            d.BandCellToolTip = "Не обязательно для заполнения. Формат ввода: " + dateFmt; d.AllowRowFiltering = false; d.SortIndicator = false; }));

            ColumnGroupDefinition g7 = new ColumnGroupDefinition() { Caption = "Сведения о продавце" };
            g7.Columns.Add(helper.CreateColumnDefinition(o => o.SELLER_INN, d => { d.Width = widthInn; d.IsUseInChangable = true;
            d.BandCellToolTip = "Не обязательно для заполнения. Ввод цифр (10 или 12 знаков)"; d.AllowRowFiltering = false; d.SortIndicator = false; }));
            g7.Columns.Add(helper.CreateColumnDefinition(o => o.SELLER_KPP, d => { d.Width = widthKpp; d.IsUseInChangable = false; 
                d.BandCellToolTip = "Не обязательно для заполнения. Формат ввода: NNNNPPXXX\r\nГде NNNN – цифры; PP – цифры или латинские буквы; XXX – цифры";
                d.AllowRowFiltering = false; d.SortIndicator = false; }));

            ColumnGroupDefinition g8 = new ColumnGroupDefinition() { Caption = "Сведения о посреднике" };
            g8.Columns.Add(helper.CreateColumnDefinition(o => o.BROKER_INN, d => { d.Width = widthInn; d.IsUseInChangable = true;
            d.BandCellToolTip = "Не обязательно для заполнения. Ввод цифр (10 или 12 знаков)"; d.AllowRowFiltering = false; d.SortIndicator = false;
            }));
            g8.Columns.Add(helper.CreateColumnDefinition(o => o.BROKER_KPP, d => { d.Width = widthKpp; d.IsUseInChangable = false; 
                d.BandCellToolTip = "Не обязательно для заполнения. Формат ввода: NNNNPPXXX\r\nГде NNNN – цифры; PP – цифры или латинские буквы; XXX – цифры";
                d.AllowRowFiltering = false; d.SortIndicator = false;
            }));
            g8.Columns.Add(helper.CreateColumnDefinition(o => o.BROKER_NAME, d => { d.Width = widthName; d.IsUseInChangable = false;
                d.AllowRowFiltering = false; d.SortIndicator = false;
            }));

            ColumnGroupDefinition g9 = new ColumnGroupDefinition() { Caption = string.Empty };
            g9.Columns.Add(helper.CreateColumnDefinition(o => o.CUSTOMS_DECLARATION_NUM, d =>
            {
                d.Width = widthInvoice;
                d.IsUseInChangable = false;
                d.AllowRowFiltering = false;
                d.SortIndicator = false;
                d.BandCellToolTip = "Не обязательно для заполнения";
            }));

            g9.Columns.Add(helper.CreateColumnDefinition(o => o.OKV_CODE, d => { d.IsUseInChangable = true; d.BandCellToolTip = "Не обязательно для заполнения. Формат ввода: XXX\r\nГде XXX – цифры"; }));
            g9.Columns.Add(helper.CreateColumnDefinition(o => o.PRICE_BUY_AMOUNT, d =>
            {
                d.Width = widthMoney;
                d.FormatInfo = formatPrices;
                d.IsUseInChangable = true;
                d.BandCellToolTip = "Обязательно для заполнения. Ввод числа (до 17 знаков) с точностью до копеек (2 знака)";
                d.IsRequiredFill = true;
            }));
            g9.Columns.Add(helper.CreateColumnDefinition(o => o.PRICE_BUY_NDS_AMOUNT, d => { d.Width = widthMoney; d.FormatInfo = formatPrices; d.IsUseInChangable = false; }));

            ColumnGroupDefinition g10 = new ColumnGroupDefinition() { Caption = string.Empty };
            g10.Columns.Add(helper.CreateColumnDefinition(o => o.DISPLAY_FULL_TAX_PERIOD, d => { d.IsUseInChangable = false; }));

            return new List<ColumnGroupDefinition> { gStateRecord, gOrdinalNumber, g0, g1, g2, g3, g4, g5, g6, g7, g8, g9, g10 };
        }

        private object GetDataR8(QueryConditions qc, out long totalRowsNumber)
        {
            return GetDataR8_12(DeclarationPart.R8, "tabChapter8", qc, out totalRowsNumber);
        }

        #endregion

        #region Раздел 9

        private void InitializeGridR9()
        {
            GridSetup setup = _presenter.CommonSetup(string.Format("{0}_InitializeExplainGridR9", GetType()));
            setup.GetData = this.GetDataR9;
            GridSetupHelper<ExplainInvoice> helper = new GridSetupHelper<ExplainInvoice>();

            setup.Columns.AddRange(GetGroupsR9(helper));
            setup.QueryCondition.Sorting.Add(new ColumnSort() { ColumnKey = TypeHelper<ExplainInvoice>.GetMemberName(t => t.ORDINAL_NUMBER), Order = ColumnSort.SortOrder.Asc });

            List<ColumnGroupDefinition> bandGroups = GetGroupsR9(helper);
            var band = new BandDefinition
            {
                BindWidth = true,
                ColumnsHeadersVisible = false,
                Name = helper.GetMemberName(o => o.RelatedInvoices),
                Groups = bandGroups
            };
            setup.Bands.Add(band);

            List<Action<UltraGridRow>> rowActions = new List<Action<UltraGridRow>>();
            rowActions.Add((row) =>
            {
                GridRowAction((int)DeclarationPart.R9, row, false);
            });
            setup.RowActions = rowActions;
            setup.CellUpdateAction = (cell) =>
            {
                GridCellAction((int)DeclarationPart.R9, cell, true);
            };

            List<string> columnsInChangable = new List<string>();
            List<string> columnsIsRequiredFill = new List<string>();
            Dictionary<string, string> bandCellToolTips = new Dictionary<string, string>();
            GeneratePropertyFromColumns(bandGroups, columnsInChangable, columnsIsRequiredFill, bandCellToolTips);
            _columnInChangable.Add((int)DeclarationPart.R9, columnsInChangable);
            _columnIsRequiredFill.Add((int)DeclarationPart.R9, columnsIsRequiredFill);

            setup.CellToolTips = GetCellToolTipsBase();
            setup.BandCellToolTips = bandCellToolTips;
            setup.AllowSaveSettings = false;

            gridR9.Setup = setup;
            gridR9.Title = GetTitleGridRazdel();
        }

        private List<ColumnGroupDefinition> GetGroupsR9(GridSetupHelper<ExplainInvoice> helper)
        {
            InitConditionValueAppearance();
            Action<ColumnDefinition> drawInvoiceState = column =>
            {
                GridDrawInvoiceState(column);
            };
            ColumnGroupDefinition gStateRecord = new ColumnGroupDefinition() { Caption = string.Empty };
            gStateRecord.Columns.Add(helper.CreateColumnDefinition(o => o.State, d =>
            {
                drawInvoiceState(d);
                d.AllowRowFiltering = false; d.SortIndicator = false;
            }));

            ColumnGroupDefinition gOrdinalNumber = new ColumnGroupDefinition() { Caption = string.Empty };
            gOrdinalNumber.Columns.Add(helper.CreateColumnDefinition(o => o.ORDINAL_NUMBER, d =>
            {
                d.IsUseInChangable = false;
                d.ToolTip = "";
                d.BandCellToolTip = "";
            }));

            ColumnGroupDefinition g0 = new ColumnGroupDefinition() { Caption = string.Empty };
            g0.Columns.Add(helper.CreateColumnDefinition(o => o.OPERATION_CODE, d => { d.IsUseInChangable = true;
            d.BandCellToolTip = "Обязательно для заполнения. Формат ввода: XX\r\nГде XX – цифры"; d.IsRequiredFill = true;
            d.AllowRowFiltering = false; d.SortIndicator = false;
            }));

            ColumnGroupDefinition g1 = new ColumnGroupDefinition() { Caption = "СФ" };
            g1.Columns.Add(helper.CreateColumnDefinition(o => o.INVOICE_NUM, d => { d.Width = widthInvoice; d.ToolTip = "Номер счета-фактуры продавца"; d.IsUseInChangable = true;
            d.BandCellToolTip = "Обязательно для заполнения."; d.IsRequiredFill = true;
            }));
            g1.Columns.Add(helper.CreateColumnDefinition(o => o.INVOICE_DATE, d => { d.Width = widthDate; d.ToolTip = "Дата счета-фактуры продавца"; d.IsUseInChangable = true; d.Width = widthDate;
            d.BandCellToolTip = "Обязательно для заполнения. Формат ввода: " + dateFmt; d.IsRequiredFill = true;
            }));

            ColumnGroupDefinition g1_1 = new ColumnGroupDefinition() { Caption = string.Empty };
            g1_1.Columns.Add(helper.CreateColumnDefinition(o => o.CUSTOMS_DECLARATION_NUM, d =>
            {
                d.Width = widthInvoice;
                d.IsUseInChangable = false;
                d.AllowRowFiltering = false;
                d.SortIndicator = false;
                d.BandCellToolTip = "Не обязательно для заполнения";
            }));


            ColumnGroupDefinition g2 = new ColumnGroupDefinition() { Caption = "ИСФ" };
            g2.Columns.Add(helper.CreateColumnDefinition(o => o.CHANGE_NUM, d => { d.Width = widthInvoice; d.IsUseInChangable = true; 
                d.BandCellToolTip = "Не обязательно для заполнения. Ввод цифр (от 1 до 3 знаков)."; 
            }));
            g2.Columns.Add(helper.CreateColumnDefinition(o => o.CHANGE_DATE, d => { d.Width = widthDate; d.IsUseInChangable = true; d.Width = widthDate;
                d.BandCellToolTip = "Не обязательно для заполнения. Формат ввода: " + dateFmt;
            }));

            ColumnGroupDefinition g3 = new ColumnGroupDefinition() { Caption = "КСФ" };
            g3.Columns.Add(helper.CreateColumnDefinition(o => o.CORRECTION_NUM, d => { d.Width = widthInvoice; d.IsUseInChangable = true; d.BandCellToolTip = "Не обязательно для заполнения"; }));
            g3.Columns.Add(helper.CreateColumnDefinition(o => o.CORRECTION_DATE, d => { d.Width = widthDate; d.IsUseInChangable = true; d.Width = widthDate;
                d.BandCellToolTip = "Не обязательно для заполнения. Формат ввода: " + dateFmt;
            }));

            ColumnGroupDefinition g4 = new ColumnGroupDefinition() { Caption = "ИКСФ" };
            g4.Columns.Add(helper.CreateColumnDefinition(o => o.CHANGE_CORRECTION_NUM, d => { d.Width = widthInvoice; d.IsUseInChangable = true;
                d.BandCellToolTip = "Не обязательно для заполнения. Ввод цифр (от 1 до 3 знаков).";
            }));
            g4.Columns.Add(helper.CreateColumnDefinition(o => o.CHANGE_CORRECTION_DATE, d => { d.Width = widthDate; d.IsUseInChangable = true; d.Width = widthDate;
                d.BandCellToolTip = "Не обязательно для заполнения. Формат ввода: " + dateFmt;
            }));

            ColumnGroupDefinition g5 = new ColumnGroupDefinition() { Caption = "Документ, подтверждающий оплату" };
            g5.Columns.Add(helper.CreateColumnDefinition(o => o.RECEIPT_DOC_NUM, d => { d.Width = widthInvoice + 80; 
                d.IsUseInChangable = false; d.BandCellToolTip = "Не обязательно для заполнения";
                d.AllowRowFiltering = false; d.SortIndicator = false;
            }));
            g5.Columns.Add(helper.CreateColumnDefinition(o => o.RECEIPT_DOC_DATE, d => { d.Width = widthDate + 80; 
                d.IsUseInChangable = false; d.Width = widthDate;
                d.BandCellToolTip = "Не обязательно для заполнения. Формат ввода: " + dateFmt;
                d.AllowRowFiltering = false; d.SortIndicator = false;
            }));

            ColumnGroupDefinition g7 = new ColumnGroupDefinition() { Caption = "Сведения о покупателе" };
            g7.Columns.Add(helper.CreateColumnDefinition(o => o.BUYER_INN, d => { d.Width = widthInn; d.IsUseInChangable = true;
                d.BandCellToolTip = "Не обязательно для заполнения. Ввод цифр (10 или 12 знаков)";
                d.AllowRowFiltering = false; d.SortIndicator = false;
            }));
            g7.Columns.Add(helper.CreateColumnDefinition(o => o.BUYER_KPP, d => { d.Width = widthKpp; d.IsUseInChangable = false;
                d.BandCellToolTip = "Не обязательно для заполнения. Формат ввода: NNNNPPXXX\r\nГде NNNN – цифры; PP – цифры или латинские буквы; XXX – цифры";
                d.AllowRowFiltering = false; d.SortIndicator = false;
            }));

            ColumnGroupDefinition g8 = new ColumnGroupDefinition() { Caption = "Сведения о посреднике" };
            g8.Columns.Add(helper.CreateColumnDefinition(o => o.BROKER_INN, d => { d.Width = widthInn; d.IsUseInChangable = true;
                d.BandCellToolTip = "Не обязательно для заполнения. Ввод цифр (10 или 12 знаков)";
                d.AllowRowFiltering = false; d.SortIndicator = false;
            }));
            g8.Columns.Add(helper.CreateColumnDefinition(o => o.BROKER_KPP, d => { d.Width = widthKpp; d.ToolTip = "КПП посредника (комиссионера, агента)"; d.IsUseInChangable = false; 
                d.BandCellToolTip = "Не обязательно для заполнения. Формат ввода: NNNNPPXXX\r\nГде NNNN – цифры; PP – цифры или латинские буквы; XXX – цифры";
                d.AllowRowFiltering = false; d.SortIndicator = false;
            }));

            ColumnGroupDefinition g9 = new ColumnGroupDefinition() { Caption = string.Empty };
            g9.Columns.Add(helper.CreateColumnDefinition(o => o.OKV_CODE, d => { d.IsUseInChangable = true; 
                d.BandCellToolTip = "Не обязательно для заполнения. Формат ввода: XXX\r\nГде XXX – цифры"; 
            }));

            ColumnGroupDefinition g10 = new ColumnGroupDefinition() { Caption = "Стоимость продаж с НДС" };
            g10.Columns.Add(helper.CreateColumnDefinition(o => o.PRICE_SELL_IN_CURR, d =>
            {  d.FormatInfo = formatPrices; d.Width = widthMoney; d.IsUseInChangable = true;
                d.BandCellToolTip = "Не обязательно для заполнения. Ввод числа (до 17 знаков) с точностью до копеек (2 знака)";
            }));
            g10.Columns.Add(helper.CreateColumnDefinition(o => o.PRICE_SELL, d =>
            {
                d.FormatInfo = formatPrices; d.Width = widthMoney; d.IsUseInChangable = true;
                d.BandCellToolTip = "Не обязательно для заполнения. Ввод числа (до 17 знаков) с точностью до копеек (2 знака)";
            }));

            ColumnGroupDefinition g11 = new ColumnGroupDefinition() { Caption = "Стоимость продаж облагаемых налогом (в руб.) без НДС" };
            g11.Columns.Add(helper.CreateColumnDefinition(o => o.PRICE_SELL_18, d =>
            {
                d.FormatInfo = formatPrices; d.Width = widthMoney + 10; d.IsUseInChangable = true;
                d.BandCellToolTip = "Не обязательно для заполнения. Ввод числа (до 17 знаков) с точностью до копеек (2 знака)";
            }));
            g11.Columns.Add(helper.CreateColumnDefinition(o => o.PRICE_SELL_10, d =>
            {
                d.FormatInfo = formatPrices; d.Width = widthMoney + 10; d.IsUseInChangable = true; 
                d.BandCellToolTip = "Не обязательно для заполнения. Ввод числа (до 17 знаков) с точностью до копеек (2 знака)"; }));
            g11.Columns.Add(helper.CreateColumnDefinition(o => o.PRICE_SELL_0, d => { d.Width = widthMoney + 10; d.IsUseInChangable = true;
                d.BandCellToolTip = "Не обязательно для заполнения. Ввод числа (до 17 знаков) с точностью до копеек (2 знака)";
            }));

            ColumnGroupDefinition g12 = new ColumnGroupDefinition() { Caption = "Сумма НДС" };
            g12.Columns.Add(helper.CreateColumnDefinition(o => o.PRICE_NDS_18, d => { d.FormatInfo = formatPrices; d.Width = widthMoney; d.IsUseInChangable = false; }));
            g12.Columns.Add(helper.CreateColumnDefinition(o => o.PRICE_NDS_10, d => { d.FormatInfo = formatPrices; d.Width = widthMoney; d.IsUseInChangable = false; }));

            ColumnGroupDefinition g13 = new ColumnGroupDefinition() { Caption = string.Empty };
            g13.Columns.Add(helper.CreateColumnDefinition(o => o.PRICE_TAX_FREE, d =>
            {
                d.FormatInfo = formatPrices; d.Width = widthMoney; d.IsUseInChangable = true;
                d.BandCellToolTip = "Не обязательно для заполнения. Ввод числа (до 17 знаков) с точностью до копеек (2 знака)";
            }));

            ColumnGroupDefinition g14 = new ColumnGroupDefinition() { Caption = string.Empty };
            g14.Columns.Add(helper.CreateColumnDefinition(o => o.DISPLAY_FULL_TAX_PERIOD, d => { d.IsUseInChangable = false; }));

            return new List<ColumnGroupDefinition> { gStateRecord, gOrdinalNumber, g0, g1, g1_1, g2, g3, g4, g5, g7, g8, g9, g10, g11, g12, g13, g14 };
        }

        private object GetDataR9(QueryConditions qc, out long totalRowsNumber)
        {
            return GetDataR8_12(DeclarationPart.R9, "tabChapter9", qc, out totalRowsNumber);
        }

        #endregion

        #region Раздел 10

        private void InitializeGridR10()
        {
            GridSetup setup = _presenter.CommonSetup(string.Format("{0}_InitializeExplainGridR10", GetType()));
            setup.GetData = GetDataR10;
            GridSetupHelper<ExplainInvoice> helper = new GridSetupHelper<ExplainInvoice>();

            setup.Columns.AddRange(GetGroupsR10(helper));
            setup.QueryCondition.Sorting.Add(new ColumnSort() { ColumnKey = TypeHelper<ExplainInvoice>.GetMemberName(t => t.ORDINAL_NUMBER), Order = ColumnSort.SortOrder.Asc });

            List<ColumnGroupDefinition> bandGroups = GetGroupsR10(helper);
            var band = new BandDefinition
            {
                BindWidth = true,
                ColumnsHeadersVisible = false,
                Name = helper.GetMemberName(o => o.RelatedInvoices),
                Groups = bandGroups
            };
            setup.Bands.Add(band);

            List<Action<UltraGridRow>> rowActions = new List<Action<UltraGridRow>>();
            rowActions.Add((row) =>
            {
                GridRowAction((int)DeclarationPart.R10, row, false);
            });
            setup.RowActions = rowActions;
            setup.CellUpdateAction = (cell) =>
            {
                GridCellAction((int)DeclarationPart.R10, cell, true);
            };

            List<string> columnsInChangable = new List<string>();
            List<string> columnsIsRequiredFill = new List<string>();
            Dictionary<string, string> bandCellToolTips = new Dictionary<string, string>();
            GeneratePropertyFromColumns(bandGroups, columnsInChangable, columnsIsRequiredFill, bandCellToolTips);
            _columnInChangable.Add((int)DeclarationPart.R10, columnsInChangable);
            _columnIsRequiredFill.Add((int)DeclarationPart.R10, columnsIsRequiredFill);

            setup.CellToolTips = GetCellToolTipsBase();
            setup.BandCellToolTips = bandCellToolTips;
            setup.AllowSaveSettings = false;

            gridR10.Setup = setup;
            gridR10.Title = GetTitleGridRazdel();
        }

        private List<ColumnGroupDefinition> GetGroupsR10(GridSetupHelper<ExplainInvoice> helper)
        {
            InitConditionValueAppearance();
            Action<ColumnDefinition> drawInvoiceState = column =>
            {
                GridDrawInvoiceState(column);
            };
            ColumnGroupDefinition gStateRecord = new ColumnGroupDefinition() { Caption = string.Empty };
            gStateRecord.Columns.Add(helper.CreateColumnDefinition(o => o.State, d =>
            {
                drawInvoiceState(d);
                d.AllowRowFiltering = false; d.SortIndicator = false;
            }));

            ColumnGroupDefinition gOrdinalNumber = new ColumnGroupDefinition() { Caption = string.Empty };
            gOrdinalNumber.Columns.Add(helper.CreateColumnDefinition(o => o.ORDINAL_NUMBER, d =>
            {
                d.IsUseInChangable = false;
                d.ToolTip = "";
                d.BandCellToolTip = "";
            }));

            ColumnGroupDefinition g0 = new ColumnGroupDefinition() { Caption = string.Empty };
            g0.Columns.Add(helper.CreateColumnDefinition(o => o.CREATE_DATE, d => { d.IsUseInChangable = true; d.Width = widthDate;
            d.BandCellToolTip = "Обязательно для заполнения. Формат ввода: " + dateFmt; d.IsRequiredFill = true;
            }));

            ColumnGroupDefinition g0_1 = new ColumnGroupDefinition() { Caption = string.Empty };
            g0_1.Columns.Add(helper.CreateColumnDefinition(o => o.OPERATION_CODE, d => { d.IsUseInChangable = true; 
                d.BandCellToolTip = "Обязательно для заполнения. Формат ввода: XX\r\nГде XX – цифры";
                d.AllowRowFiltering = false; d.SortIndicator = false;
            }));

            ColumnGroupDefinition g1 = new ColumnGroupDefinition() { Caption = "СФ" };
            g1.Columns.Add(helper.CreateColumnDefinition(o => o.INVOICE_NUM, d => { d.Width = widthInvoice; d.ToolTip = "Номер счета-фактуры"; d.IsUseInChangable = true;
            d.BandCellToolTip = "Обязательно для заполнения."; d.IsRequiredFill = true;
            }));
            g1.Columns.Add(helper.CreateColumnDefinition(o => o.INVOICE_DATE, d => { d.Width = widthDate; d.ToolTip = "Дата счета-фактуры"; d.IsUseInChangable = true;
            d.BandCellToolTip = "Обязательно для заполнения. Формат ввода: " + dateFmt; d.IsRequiredFill = true;
            }));

            ColumnGroupDefinition g2 = new ColumnGroupDefinition() { Caption = "ИСФ" };
            g2.Columns.Add(helper.CreateColumnDefinition(o => o.CHANGE_NUM, d => { d.Width = widthInvoice; d.ToolTip = "Номер исправления счета-фактуры"; d.IsUseInChangable = true;
                d.BandCellToolTip = "Не обязательно для заполнения. Ввод цифр (от 1 до 3 знаков).";
            }));
            g2.Columns.Add(helper.CreateColumnDefinition(o => o.CHANGE_DATE, d => { d.Width = widthDate; d.ToolTip = "Дата исправления счета-фактуры"; d.IsUseInChangable = true;
                d.BandCellToolTip = "Не обязательно для заполнения. Формат ввода: " + dateFmt;
            }));

            ColumnGroupDefinition g3 = new ColumnGroupDefinition() { Caption = "КСФ" };
            g3.Columns.Add(helper.CreateColumnDefinition(o => o.CORRECTION_NUM, d => { d.Width = widthInvoice; d.ToolTip = "Номер корректировочного счета-фактуры"; d.IsUseInChangable = true;
                d.BandCellToolTip = "Не обязательно для заполнения";
            }));
            g3.Columns.Add(helper.CreateColumnDefinition(o => o.CORRECTION_DATE, d => { d.Width = widthDate; d.ToolTip = "Дата корректировочного счета-фактуры"; d.IsUseInChangable = true;
                d.BandCellToolTip = "Не обязательно для заполнения. Формат ввода: " + dateFmt;
            }));

            ColumnGroupDefinition g4 = new ColumnGroupDefinition() { Caption = "ИКСФ" };
            g4.Columns.Add(helper.CreateColumnDefinition(o => o.CHANGE_CORRECTION_NUM, d => { d.Width = widthInvoice; d.ToolTip = "Номер исправления корректировочного счета-фактуры"; d.IsUseInChangable = true;
                d.BandCellToolTip = "Не обязательно для заполнения. Ввод цифр (от 1 до 3 знаков).";
            }));
            g4.Columns.Add(helper.CreateColumnDefinition(o => o.CHANGE_CORRECTION_DATE, d => { d.Width = widthDate; d.ToolTip = "Дата исправления корректировочного счета-фактуры"; d.IsUseInChangable = true;
                d.BandCellToolTip = "Не обязательно для заполнения. Формат ввода: " + dateFmt;
            }));

            ColumnGroupDefinition g5 = new ColumnGroupDefinition() { Caption = "Сведения о покупателе" };
            g5.Columns.Add(helper.CreateColumnDefinition(o => o.BUYER_INN, d => { d.Width = widthInn; d.IsUseInChangable = true;
                d.BandCellToolTip = "Не обязательно для заполнения. Ввод цифр (10 или 12 знаков) ";
            }));
            g5.Columns.Add(helper.CreateColumnDefinition(o => o.BUYER_KPP, d => { d.Width = widthKpp; d.IsUseInChangable = false; 
                d.BandCellToolTip = "Не обязательно для заполнения. Формат ввода: NNNNPPXXX\r\nГде NNNN – цифры; PP – цифры или латинские буквы; XXX – цифры"; 
            }));

            ColumnGroupDefinition gSellerAgencyInfo = new ColumnGroupDefinition() { Caption = "Сведения о посреднической деятельности продавца" };
            gSellerAgencyInfo.Columns.Add(helper.CreateColumnDefinition(o => o.SELLER_AGENCY_INFO_INN, d => { d.Caption = "ИНН"; d.IsUseInChangable = true;
            d.BandCellToolTip = "Обязательно для заполнения. Ввод цифр (10 или 12 знаков)"; d.IsRequiredFill = true;
            d.AllowRowFiltering = false; d.SortIndicator = false;
            }));
            gSellerAgencyInfo.Columns.Add(helper.CreateColumnDefinition(o => o.SELLER_AGENCY_INFO_KPP, d => { d.Caption = "КПП"; d.IsUseInChangable = false; 
                d.BandCellToolTip = "Не обязательно для заполнения. Формат ввода: NNNNPPXXX\r\nГде NNNN – цифры; PP – цифры или латинские буквы; XXX – цифры";
                d.AllowRowFiltering = false; d.SortIndicator = false;
            }));
            gSellerAgencyInfo.Columns.Add(helper.CreateColumnDefinition(o => o.SELLER_AGENCY_INFO_NUM, d => { d.Caption = "№ СФ"; d.IsUseInChangable = true;
            d.BandCellToolTip = "Обязательно для заполнения."; d.IsRequiredFill = true;
            }));
            gSellerAgencyInfo.Columns.Add(helper.CreateColumnDefinition(o => o.SELLER_AGENCY_INFO_DATE, d => { d.Caption = "Дата СФ"; d.IsUseInChangable = true; d.Width = widthDate;
            d.BandCellToolTip = "Обязательно для заполнения. Формат ввода: " + dateFmt; d.IsRequiredFill = true;
            }));

            ColumnGroupDefinition g7 = new ColumnGroupDefinition() { Caption = string.Empty };
            g7.Columns.Add(helper.CreateColumnDefinition(o => o.OKV_CODE, d => { d.IsUseInChangable = true; 
                d.BandCellToolTip = "Не обязательно для заполнения. Формат ввода: XXX\r\nГде XXX – цифры"; 
            }));
            g7.Columns.Add(helper.CreateColumnDefinition(o => o.PRICE_TOTAL, d =>
            {
                d.FormatInfo = formatPrices; d.Width = widthMoney; d.IsUseInChangable = true;
            d.BandCellToolTip = "Обязательно для заполнения. Ввод числа (до 17 знаков) с точностью до копеек (2 знака)"; d.IsRequiredFill = true;
            }));
            g7.Columns.Add(helper.CreateColumnDefinition(o => o.PRICE_NDS_TOTAL, d => { d.FormatInfo = formatPrices; d.Width = widthMoney; d.IsUseInChangable = false; }));

            ColumnGroupDefinition g8 = new ColumnGroupDefinition() { Caption = "Разница стоимости с НДС по КСФ" };
            g8.Columns.Add(helper.CreateColumnDefinition(o => o.DIFF_CORRECT_NDS_DECREASE, d => { d.FormatInfo = formatPrices; d.Width = widthMoney; d.IsUseInChangable = true; d.BandCellToolTip = "Не обязательно для заполнения. Ввод числа (до 17 знаков) с точностью до копеек (2 знака)"; }));
            g8.Columns.Add(helper.CreateColumnDefinition(o => o.DIFF_CORRECT_NDS_INCREASE, d => { d.FormatInfo = formatPrices; d.Width = widthMoney; d.IsUseInChangable = true; d.BandCellToolTip = "Не обязательно для заполнения. Ввод числа (до 17 знаков) с точностью до копеек (2 знака)"; }));

            ColumnGroupDefinition g9 = new ColumnGroupDefinition() { Caption = "Разница НДС по КСФ" };
            g9.Columns.Add(helper.CreateColumnDefinition(o => o.DIFF_CORRECT_DECREASE, d => { d.FormatInfo = formatPrices; d.Width = widthMoney; d.IsUseInChangable = false; }));
            g9.Columns.Add(helper.CreateColumnDefinition(o => o.DIFF_CORRECT_INCREASE, d => { d.FormatInfo = formatPrices; d.Width = widthMoney; d.IsUseInChangable = false; }));

            ColumnGroupDefinition g10 = new ColumnGroupDefinition() { Caption = string.Empty };
            g10.Columns.Add(helper.CreateColumnDefinition(o => o.DISPLAY_FULL_TAX_PERIOD, d => { d.IsUseInChangable = false; }));

            return new List<ColumnGroupDefinition> { gStateRecord, gOrdinalNumber, g0, g0_1, g1, g2, g3, g4, g5, gSellerAgencyInfo, g7, g8, g9, g10 };
        }

        private object GetDataR10(QueryConditions qc, out long totalRowsNumber)
        {
            return GetDataR8_12(DeclarationPart.R10, "tabChapter10", qc, out totalRowsNumber);
        }
        #endregion

        #region Раздел 11

        private void InitializeGridR11()
        {
            GridSetup setup = _presenter.CommonSetup(string.Format("{0}_InitializeExplainGridR11", GetType())); ;
            setup.GetData = this.GetDataR11;
            GridSetupHelper<ExplainInvoice> helper = new GridSetupHelper<ExplainInvoice>();

            setup.Columns.AddRange(GetGroupsR11(helper));
            setup.QueryCondition.Sorting.Add(new ColumnSort() { ColumnKey = TypeHelper<ExplainInvoice>.GetMemberName(t => t.ORDINAL_NUMBER), Order = ColumnSort.SortOrder.Asc });

            List<ColumnGroupDefinition> bandGroups = GetGroupsR11(helper);
            var band = new BandDefinition
            {
                BindWidth = true,
                ColumnsHeadersVisible = false,
                Name = helper.GetMemberName(o => o.RelatedInvoices),
                Groups = bandGroups
            };
            setup.Bands.Add(band);

            List<Action<UltraGridRow>> rowActions = new List<Action<UltraGridRow>>();
            rowActions.Add((row) =>
            {
                GridRowAction((int)DeclarationPart.R11, row, false);
            });
            setup.RowActions = rowActions;
            setup.CellUpdateAction = (cell) =>
            {
                GridCellAction((int)DeclarationPart.R11, cell, true);
            };

            List<string> columnsInChangable = new List<string>();
            List<string> columnsIsRequiredFill = new List<string>();
            Dictionary<string, string> bandCellToolTips = new Dictionary<string, string>();
            GeneratePropertyFromColumns(bandGroups, columnsInChangable, columnsIsRequiredFill, bandCellToolTips);
            _columnInChangable.Add((int)DeclarationPart.R11, columnsInChangable);
            _columnIsRequiredFill.Add((int)DeclarationPart.R11, columnsIsRequiredFill);

            setup.CellToolTips = GetCellToolTipsBase();
            setup.BandCellToolTips = bandCellToolTips;
            setup.AllowSaveSettings = false;

            gridR11.Setup = setup;
            gridR11.Title = GetTitleGridRazdel();
        }

        private List<ColumnGroupDefinition> GetGroupsR11(GridSetupHelper<ExplainInvoice> helper)
        {
            InitConditionValueAppearance();
            Action<ColumnDefinition> drawInvoiceState = column =>
            {
                GridDrawInvoiceState(column);
            };
            ColumnGroupDefinition gStateRecord = new ColumnGroupDefinition() { Caption = string.Empty };
            gStateRecord.Columns.Add(helper.CreateColumnDefinition(o => o.State, d => { drawInvoiceState(d); d.AllowRowFiltering = false; d.SortIndicator = false; }));

            ColumnGroupDefinition gOrdinalNumber = new ColumnGroupDefinition() { Caption = string.Empty };
            gOrdinalNumber.Columns.Add(helper.CreateColumnDefinition(o => o.ORDINAL_NUMBER, d =>
            {
                d.IsUseInChangable = false;
                d.ToolTip = "";
                d.BandCellToolTip = "";
            }));

            ColumnGroupDefinition g0 = new ColumnGroupDefinition() { Caption = string.Empty };
            g0.Columns.Add(helper.CreateColumnDefinition(o => o.RECEIVE_DATE, d => { d.IsUseInChangable = true; d.Width = widthDate;
            d.BandCellToolTip = "Обязательно для заполнения. Формат ввода: " + dateFmt; d.IsRequiredFill = true;
            }));

            ColumnGroupDefinition g0_1 = new ColumnGroupDefinition() { Caption = string.Empty };
            g0_1.Columns.Add(helper.CreateColumnDefinition(o => o.OPERATION_CODE, d => { d.IsUseInChangable = true;
                d.BandCellToolTip = "Обязательно для заполнения. Формат ввода: XX\r\nГде XX – цифры"; d.IsRequiredFill = true;
                d.AllowRowFiltering = false; d.SortIndicator = false;
            }));

            ColumnGroupDefinition g1 = new ColumnGroupDefinition() { Caption = "СФ" };
            g1.Columns.Add(helper.CreateColumnDefinition(o => o.INVOICE_NUM, d => { d.Width = widthInvoice; d.ToolTip = "Номер счета-фактуры"; d.IsUseInChangable = true;
            d.BandCellToolTip = "Обязательно для заполнения."; d.IsRequiredFill = true;
            }));
            g1.Columns.Add(helper.CreateColumnDefinition(o => o.INVOICE_DATE, d => { d.Width = widthDate; d.ToolTip = "Дата счета-фактуры"; d.IsUseInChangable = true;
            d.BandCellToolTip = "Обязательно для заполнения. Формат ввода: " + dateFmt; d.IsRequiredFill = true;
            }));

            ColumnGroupDefinition g2 = new ColumnGroupDefinition() { Caption = "ИСФ" };
            g2.Columns.Add(helper.CreateColumnDefinition(o => o.CHANGE_NUM, d => { d.Width = widthInvoice; d.ToolTip = "Номер исправления счета-фактуры"; d.IsUseInChangable = true;
                d.BandCellToolTip = "Не обязательно для заполнения. Ввод цифр (от 1 до 3 знаков).";
            }));
            g2.Columns.Add(helper.CreateColumnDefinition(o => o.CHANGE_DATE, d => { d.Width = widthDate; d.ToolTip = "Дата исправления счета-фактуры"; d.IsUseInChangable = true;
                d.BandCellToolTip = "Не обязательно для заполнения. Формат ввода: " + dateFmt;
            }));

            ColumnGroupDefinition g3 = new ColumnGroupDefinition() { Caption = "КСФ" };
            g3.Columns.Add(helper.CreateColumnDefinition(o => o.CORRECTION_NUM, d => { d.Width = widthInvoice; d.ToolTip = "Номер корректировочного счета-фактуры"; d.IsUseInChangable = true;
                d.BandCellToolTip = "Не обязательно для заполнения";
            }));
            g3.Columns.Add(helper.CreateColumnDefinition(o => o.CORRECTION_DATE, d => { d.Width = widthDate; d.ToolTip = "Дата корректировочного счета-фактуры"; d.IsUseInChangable = true;
                d.BandCellToolTip = "Не обязательно для заполнения. Формат ввода: " + dateFmt;
            }));

            ColumnGroupDefinition g4 = new ColumnGroupDefinition() { Caption = "ИКСФ" };
            g4.Columns.Add(helper.CreateColumnDefinition(o => o.CHANGE_CORRECTION_NUM, d => { d.Width = widthInvoice; d.ToolTip = "Номер исправления корректировочного счета-фактуры"; d.IsUseInChangable = true;
                d.BandCellToolTip = "Не обязательно для заполнения. Ввод цифр (от 1 до 3 знаков).";
            }));
            g4.Columns.Add(helper.CreateColumnDefinition(o => o.CHANGE_CORRECTION_DATE, d => { d.Width = widthDate; d.ToolTip = "Дата исправления корректировочного счета-фактуры"; d.IsUseInChangable = true;
                d.BandCellToolTip = "Не обязательно для заполнения. Формат ввода: " + dateFmt;
            }));

            ColumnGroupDefinition g5 = new ColumnGroupDefinition() { Caption = "Сведения о продавце" };
            g5.Columns.Add(helper.CreateColumnDefinition(o => o.SELLER_INN, d => { d.Width = widthInn; d.IsUseInChangable = true;
                d.BandCellToolTip = "Обязательно для заполнения. Ввод цифр (10 или 12 знаков)"; d.IsRequiredFill = true;
                d.AllowRowFiltering = false; d.SortIndicator = false;
            }));
            g5.Columns.Add(helper.CreateColumnDefinition(o => o.SELLER_KPP, d => { d.Width = widthKpp; d.IsUseInChangable = false; 
                d.BandCellToolTip = "Не обязательно для заполнения. Формат ввода: NNNNPPXXX\r\nГде NNNN – цифры; PP – цифры или латинские буквы; XXX – цифры"; 
                d.AllowRowFiltering = false; d.SortIndicator = false;
            }));

            ColumnGroupDefinition g6 = new ColumnGroupDefinition() { Caption = "Сведения о субкомиссионере" };
            g6.Columns.Add(helper.CreateColumnDefinition(o => o.BROKER_INN, d => { d.Width = widthInn; d.ToolTip = "ИНН субкомиссионера (субагента)"; d.IsUseInChangable = true;
                d.BandCellToolTip = "Не обязательно для заполнения. Ввод цифр (10 или 12 знаков)";
                d.AllowRowFiltering = false; d.SortIndicator = false;
            }));
            g6.Columns.Add(helper.CreateColumnDefinition(o => o.BROKER_KPP, d => { d.Width = widthKpp; d.ToolTip = "КПП субкомиссионера (субагента)"; d.IsUseInChangable = false; 
                d.BandCellToolTip = "Не обязательно для заполнения. Формат ввода: NNNNPPXXX\r\nГде NNNN – цифры; PP – цифры или латинские буквы; XXX – цифры"; 
                d.AllowRowFiltering = false; d.SortIndicator = false; }));

            ColumnGroupDefinition g7 = new ColumnGroupDefinition() { Caption = string.Empty };
            g7.Columns.Add(helper.CreateColumnDefinition(o => o.DEAL_KIND_CODE, d => { d.IsUseInChangable = true; d.BandCellToolTip = "Обязательно для заполнения. Формат ввода: X\r\nГде X – цифры от 0 до 4."; d.IsRequiredFill = true; }));
            g7.Columns.Add(helper.CreateColumnDefinition(o => o.OKV_CODE, d => { d.IsUseInChangable = true; d.BandCellToolTip = "Не обязательно для заполнения. Формат ввода: XXX\r\nГде XXX – цифры"; }));
            g7.Columns.Add(helper.CreateColumnDefinition(o => o.PRICE_TOTAL, d =>
            {
                d.FormatInfo = formatPrices; d.Width = widthMoney; d.IsUseInChangable = true; 
                d.BandCellToolTip = "Обязательно для заполнения. Ввод числа (до 17 знаков) с точностью до копеек (2 знака)"; d.IsRequiredFill = true; }));
            g7.Columns.Add(helper.CreateColumnDefinition(o => o.PRICE_NDS_TOTAL, d => { d.FormatInfo = formatPrices; d.Width = widthMoney; d.IsUseInChangable = false; }));

            ColumnGroupDefinition g8 = new ColumnGroupDefinition() { Caption = "Разница стоимости с НДС по КСФ" };
            g8.Columns.Add(helper.CreateColumnDefinition(o => o.DIFF_CORRECT_NDS_DECREASE, d => { d.FormatInfo = formatPrices; d.Width = widthMoney; d.IsUseInChangable = true; d.BandCellToolTip = "Не обязательно для заполнения. Ввод числа (до 17 знаков) с точностью до копеек (2 знака)"; }));
            g8.Columns.Add(helper.CreateColumnDefinition(o => o.DIFF_CORRECT_NDS_INCREASE, d => { d.FormatInfo = formatPrices; d.Width = widthMoney; d.IsUseInChangable = true; d.BandCellToolTip = "Не обязательно для заполнения. Ввод числа (до 17 знаков) с точностью до копеек (2 знака)"; }));

            ColumnGroupDefinition g9 = new ColumnGroupDefinition() { Caption = "Разница НДС по КСФ" };
            g9.Columns.Add(helper.CreateColumnDefinition(o => o.DIFF_CORRECT_DECREASE, d => { d.FormatInfo = formatPrices; d.Width = widthMoney; d.IsUseInChangable = false; }));
            g9.Columns.Add(helper.CreateColumnDefinition(o => o.DIFF_CORRECT_INCREASE, d => { d.FormatInfo = formatPrices; d.Width = widthMoney; d.IsUseInChangable = false; }));

            ColumnGroupDefinition g10 = new ColumnGroupDefinition() { Caption = string.Empty };
            g10.Columns.Add(helper.CreateColumnDefinition(o => o.DISPLAY_FULL_TAX_PERIOD, d => { d.IsUseInChangable = false; }));

            return new List<ColumnGroupDefinition> { gStateRecord, gOrdinalNumber, g0, g0_1, g1, g2, g3, g4, g5, g6, g7, g8, g9, g10 };
        }

        private object GetDataR11(QueryConditions qc, out long totalRowsNumber)
        {
            return GetDataR8_12(DeclarationPart.R11, "tabChapter11", qc, out totalRowsNumber);
        }

        #endregion

        #region Раздел 12

        private void InitializeGridR12()
        {
            GridSetup setup = _presenter.CommonSetup(string.Format("{0}_InitializeExplainGridR12", GetType())); ;
            setup.GetData = this.GetDataR12;
            GridSetupHelper<ExplainInvoice> helper = new GridSetupHelper<ExplainInvoice>();

            setup.Columns.AddRange(GetGroupsR12(helper));
            setup.QueryCondition.Sorting.Add(new ColumnSort() { ColumnKey = TypeHelper<ExplainInvoice>.GetMemberName(t => t.INVOICE_DATE), Order = ColumnSort.SortOrder.Asc });

            List<ColumnGroupDefinition> bandGroups = GetGroupsR12(helper);
            var band = new BandDefinition
            {
                BindWidth = true,
                ColumnsHeadersVisible = false,
                Name = helper.GetMemberName(o => o.RelatedInvoices),
                Groups = bandGroups
            };
            setup.Bands.Add(band);

            List<Action<UltraGridRow>> rowActions = new List<Action<UltraGridRow>>();
            rowActions.Add((row) =>
            {
                GridRowAction((int)DeclarationPart.R12, row, false);
            });
            setup.RowActions = rowActions;
            setup.CellUpdateAction = (cell) =>
            {
                GridCellAction((int)DeclarationPart.R12, cell, true);
            };

            List<string> columnsInChangable = new List<string>();
            List<string> columnsIsRequiredFill = new List<string>();
            Dictionary<string, string> bandCellToolTips = new Dictionary<string, string>();
            GeneratePropertyFromColumns(bandGroups, columnsInChangable, columnsIsRequiredFill, bandCellToolTips);
            _columnInChangable.Add((int)DeclarationPart.R12, columnsInChangable);
            _columnIsRequiredFill.Add((int)DeclarationPart.R12, columnsIsRequiredFill);

            setup.CellToolTips = GetCellToolTipsBase();
            setup.BandCellToolTips = bandCellToolTips;
            setup.AllowSaveSettings = false;

            gridR12.Setup = setup;
            gridR12.Title = GetTitleGridRazdel();
        }

        private List<ColumnGroupDefinition> GetGroupsR12(GridSetupHelper<ExplainInvoice> helper)
        {
            InitConditionValueAppearance();
            Action<ColumnDefinition> drawInvoiceState = column =>
            {
                GridDrawInvoiceState(column);
            };
            ColumnGroupDefinition gStateRecord = new ColumnGroupDefinition() { Caption = string.Empty };
            gStateRecord.Columns.Add(helper.CreateColumnDefinition(o => o.State, d => { drawInvoiceState(d); d.AllowRowFiltering = false; d.SortIndicator = false; }));

            ColumnGroupDefinition gSF = new ColumnGroupDefinition() { Caption = "СФ" };
            gSF.Columns.Add(helper.CreateColumnDefinition(o => o.INVOICE_NUM, d => { d.Caption = "№"; d.ToolTip = "Номер счета-фактуры продавца"; d.Width = widthInvoice; d.IsUseInChangable = true;
            d.BandCellToolTip = "Обязательно для заполнения."; d.IsRequiredFill = true;
            }));
            gSF.Columns.Add(helper.CreateColumnDefinition(o => o.INVOICE_DATE, d => { d.Caption = "Дата"; d.ToolTip = "Дата счета-фактуры продавца"; d.IsUseInChangable = true; d.Width = widthDate;
            d.BandCellToolTip = "Обязательно для заполнения. Формат ввода: " + dateFmt; d.IsRequiredFill = true;
            }));

            ColumnGroupDefinition gBuyerInfo = new ColumnGroupDefinition() { Caption = "Сведения о покупателе" };
            gBuyerInfo.Columns.Add(helper.CreateColumnDefinition(o => o.BUYER_INN, d => { d.Caption = "ИНН"; d.ToolTip = "ИНН покупателя"; d.IsUseInChangable = true;
                d.BandCellToolTip = "Обязательно для заполнения. Ввод цифр (10 или 12 знаков) "; d.IsRequiredFill = true;
                d.AllowRowFiltering = false; d.SortIndicator = false;
            }));
            gBuyerInfo.Columns.Add(helper.CreateColumnDefinition(o => o.BUYER_KPP, d => { d.Caption = "КПП"; d.ToolTip = "КПП покупателя"; d.IsUseInChangable = false; 
                d.BandCellToolTip = "Не обязательно для заполнения. Формат ввода: NNNNPPXXX\r\nГде NNNN – цифры; PP – цифры или латинские буквы; XXX – цифры";
                d.AllowRowFiltering = false; d.SortIndicator = false;
            }));

            ColumnGroupDefinition gCurrencyCode = new ColumnGroupDefinition() { Caption = string.Empty };
            gCurrencyCode.Columns.Add(helper.CreateColumnDefinition(o => o.OKV_CODE, d => { d.Caption = "Код валюты"; d.ToolTip = "Код валюты по ОКВ"; d.IsUseInChangable = true; 
                d.BandCellToolTip = "Не обязательно для заполнения. Формат ввода: XXX\r\nГде XXX – цифры"; 
            }));

            ColumnGroupDefinition gPriceWithoutNdsTotal = new ColumnGroupDefinition() { Caption = string.Empty };
            gPriceWithoutNdsTotal.Columns.Add(helper.CreateColumnDefinition(o => o.PRICE_TAX_FREE, d =>
            {
                d.FormatInfo = formatPrices; d.Caption = "Стоимость без НДС"; d.ToolTip = "Стоимость товаров (работ, услуг), имущественных прав без налога - всего"; d.IsUseInChangable = true;
            d.BandCellToolTip = "Обязательно для заполнения. Ввод числа (до 19 знаков) с точностью до копеек (2 знака)"; d.IsRequiredFill = true;
            }));

            ColumnGroupDefinition gPriceNdsBuyerTotal = new ColumnGroupDefinition() { Caption = string.Empty };
            gPriceNdsBuyerTotal.Columns.Add(helper.CreateColumnDefinition(o => o.PRICE_BUY_NDS_AMOUNT, d => { d.FormatInfo = formatPrices; d.Caption = "Сумма НДС"; d.ToolTip = "Сумма налога, предъявляемая покупателю"; d.IsUseInChangable = false; }));

            ColumnGroupDefinition gPriceWithNdsTotal = new ColumnGroupDefinition() { Caption = string.Empty };
            gPriceWithNdsTotal.Columns.Add(helper.CreateColumnDefinition(o => o.PRICE_BUY_AMOUNT, d =>
            {
                d.FormatInfo = formatPrices; d.Caption = "Стоимость с НДС"; d.ToolTip = "Стоимость товаров (работ, услуг), имущественных прав с налогом - всего"; d.IsUseInChangable = true;
            d.BandCellToolTip = "Обязательно для заполнения. Ввод числа (до 19 знаков) с точностью до копеек (2 знака)"; d.IsRequiredFill = true;
            }));

            ColumnGroupDefinition gDisplayTaxPeriod = new ColumnGroupDefinition() { Caption = string.Empty };
            gDisplayTaxPeriod.Columns.Add(helper.CreateColumnDefinition(o => o.DISPLAY_FULL_TAX_PERIOD, d => { d.IsUseInChangable = false; }));

            return new List<ColumnGroupDefinition> { gStateRecord, gSF, gBuyerInfo, gCurrencyCode, gPriceWithoutNdsTotal, gPriceNdsBuyerTotal, gPriceWithNdsTotal, gDisplayTaxPeriod };
        }

        private object GetDataR12(QueryConditions qc, out long totalRowsNumber)
        {
            return GetDataR8_12(DeclarationPart.R12, "tabChapter12", qc, out totalRowsNumber);
        }

        #endregion

        #region Функции

        private void InitializeRibbon()
        {
            IUcResourceManagersService resourceManagersService = _presenter.WorkItem.Services.Get<IUcResourceManagersService>(true);
            if (!resourceManagersService.Contains(ResourceManagerNDS2.NDS2ClientResources))
            {
                resourceManagersService.Add(ResourceManagerNDS2.NDS2ClientResources, Properties.Resources.ResourceManager);
            }

            //закладка
            UcRibbonTabContext tabNavigator = new UcRibbonTabContext(_presenter.PresentationContext, "NDS2Result")
            {
                Text = _presenter.PresentationContext.WindowTitle,
                ToolTipText = _presenter.PresentationContext.WindowTitle,
                Visible = true,
                Order = 1
            };

            UcRibbonGroupContext groupNavigation1 = tabNavigator.AddGroup("NDS2DeclarationListManage");
            groupNavigation1.Text = "Функции";
            groupNavigation1.Visible = true;

            btnUpdate = new UcRibbonButtonToolContext(_presenter.PresentationContext, "btn" + "Update", "cmdExplainCard" + "Update")
            {
                Text = "Обновить",
                ToolTipText = "Обновить",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "update",
                SmallImageName = "update",
                Enabled = true
            };

            btnSendToWork = new UcRibbonButtonToolContext(_presenter.PresentationContext, "btn" + "SendToWork", "cmdExplainCard" + "SendToWork")
            {
                Text = "Передать в работу",
                ToolTipText = "Передать в работу",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "send",
                SmallImageName = "send",
                Enabled = true
            };

            btnCancelAllChange = new UcRibbonButtonToolContext(_presenter.PresentationContext, "btn" + "CancelAllChange", "cmdExplainCard" + "CancelAllChange")
            {
                Text = "Отменить все изменения",
                ToolTipText = "Отменить все изменения",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "arrow_undo",
                SmallImageName = "arrow_undo",
                Enabled = true
            };
            
            btnCommit = new UcRibbonButtonToolContext(_presenter.PresentationContext, "btn" + "Commit", "cmdExplainCard" + "Commit")
            {
                Text = "Подтвердить",
                ToolTipText = "Подтвердить",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "dialog_apply",
                SmallImageName = "dialog_apply",
                Enabled = true
            };

            btnChange = new UcRibbonButtonToolContext(_presenter.PresentationContext, "btn" + "Change", "cmdExplainCard" + "Change")
            {
                Text = "Изменить",
                ToolTipText = "Изменить",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "edit_explain",
                SmallImageName = "edit_explain",
                Enabled = true
            };

            btnCancel = new UcRibbonButtonToolContext(_presenter.PresentationContext, "btn" + "Cancel", "cmdExplainCard" + "Cancel")
            {
                Text = "Отменить",
                ToolTipText = "Отменить",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "arrow_undo",
                SmallImageName = "arrow_undo",
                Enabled = true
            };
            

            groupNavigation1.ToolList.AddRange(
                new UcRibbonToolInstanceSettings[] { 
                    new UcRibbonToolInstanceSettings(btnUpdate.ItemName, UcRibbonToolSize.Large),
                    new UcRibbonToolInstanceSettings(btnSendToWork.ItemName, UcRibbonToolSize.Large),
                    new UcRibbonToolInstanceSettings(btnCancelAllChange.ItemName, UcRibbonToolSize.Large),
                    new UcRibbonToolInstanceSettings(btnCommit.ItemName, UcRibbonToolSize.Large),
                    new UcRibbonToolInstanceSettings(btnChange.ItemName, UcRibbonToolSize.Large),
                    new UcRibbonToolInstanceSettings(btnCancel.ItemName, UcRibbonToolSize.Large),
                });

            //общее
            _presentationContext.UiVisualizationCollection.AddRange(new VisualizationElementBase[]
            {
                btnUpdate,
                btnSendToWork,
                btnCancelAllChange,
                btnCommit,
                btnChange,
                btnCancel,
                tabNavigator
            });

            _presentationContext.ActiveMenuTab = tabNavigator.ItemName;

            SetRibbonButtonEnaled();
        }

        private void SetRibbonButtonEnaled()
        {
            if (_exlpainReplyCardData.Regim == ExplainRegim.View)
            {
                btnUpdate.Enabled = true;
                btnSendToWork.Enabled = false;
                btnCancelAllChange.Enabled = false;
                btnCommit.Enabled = false;
                btnChange.Enabled = false;
                btnCancel.Enabled = false;
            }
            else if (_exlpainReplyCardData.Regim == ExplainRegim.Edit)
            {
                btnUpdate.Enabled = false;
                btnSendToWork.Enabled = isExistsInvoiceChangeOfExplain;
                btnCommit.Enabled = isSelectAnyRowWithoutStateProcessed;
                btnChange.Enabled = isSelectOneRowWithoutStateChanging;
                btnCancel.Enabled = isSelectAnyRowStateChangingOrProcessed;
                btnCancelAllChange.Enabled = isExistsInvoiceChangeOfExplain;
            }
            _presenter.RefreshEKP();
        }

        private bool CheckCorrectInvoices(out string message)
        {
            bool isCorrect = true;
            StringBuilder sb = new StringBuilder();
            message = String.Empty;
            int currentChapterKey = -1;
            string currentFieldKey = String.Empty;
            bool isBeginMessage = false;
            Dictionary<int, List<UltraGridRow>> chapters = GetChapterRowsFromAllGrid();
            foreach (KeyValuePair<int, List<UltraGridRow>> itemChapter in chapters)
            {
                foreach (UltraGridRow row in itemChapter.Value)
                {
                    ExplainInvoice inv = (ExplainInvoice)row.ListObject;
                    if (inv.State == ExplainInvoiceState.ChangingInThisExplain)
                    {
                        ExplainInvoice invCorrect = inv.RelatedInvoices.FirstOrDefault();
                        if (invCorrect != null)
                        {
                            Dictionary<string, object> filedsEntityOriginal = _presenter.ConvertToDictionary(inv);
                            Dictionary<string, object> filedsEntityCorrect = _presenter.ConvertToDictionary(invCorrect);
                            foreach (KeyValuePair<string, object> itemField in filedsEntityCorrect)
                            {
                                if (_columnInChangable[itemChapter.Key].Contains(itemField.Key))
                                {
                                    object fieldValueOriginal = GetFieldEntity(filedsEntityOriginal, itemField.Key);
                                    if (!EqualsInvoiceFieldValue(fieldValueOriginal, itemField.Value))
                                    {
                                        bool isFieldcorrect = IsFieldCorrect(itemChapter.Key, itemField.Key, itemField.Value);
                                        if (!isFieldcorrect)
                                        {
                                            isCorrect = false;
                                            if (sb.Length == 0)
                                            {
                                                sb.AppendLine("Изменненные записи содержат некорректно введенные данные.");
                                                sb.Append("Смотри: ");
                                                isBeginMessage = true;
                                            }

                                            if (currentChapterKey != itemChapter.Key)
                                            {
                                                currentChapterKey = itemChapter.Key;
                                                if (!isBeginMessage)
                                                {
                                                    sb.Append("; ");
                                                }
                                                isBeginMessage = false;
                                            }
                                            else if (currentFieldKey != itemField.Key)
                                            {
                                                currentFieldKey = itemField.Key;
                                                if (!isBeginMessage)
                                                {
                                                    sb.Append(", ");
                                                }
                                                isBeginMessage = false;
                                            }

                                            sb.Append(string.Format("Раздел {0} СФ № {1}", inv.CHAPTER, inv.INVOICE_NUM));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            message = sb.ToString().Trim();
            //Изменненные записи содержат некорректно введенные данные. 
            //Смотри: Раздел <номер раздела> СФ № <номер СФ> [{, СФ № <номер СФ>]} {[; Раздел <номер раздела> СФ № <номер СФ> {[, СФ № <номер СФ>}}]].»
            return isCorrect;
        }

        private bool EqualsInvoiceFieldValue(object fieldValueOriginal, object fieldValue)
        {
            bool isEqals = true;
            if (fieldValueOriginal != null)
            {
                if (fieldValueOriginal is string && 
                    string.IsNullOrWhiteSpace((string)fieldValueOriginal) && 
                    fieldValue == null)
                {
                    isEqals = true;
                }
                else if (!fieldValueOriginal.Equals(fieldValue))
                {
                    isEqals = false;
                }
                else
                {
                    isEqals = true;
                }
            }
            else if (fieldValue != null) 
            {
                if (fieldValue is string &&
                    string.IsNullOrWhiteSpace((string)fieldValue)) { isEqals = true; }
                else { isEqals = false; }
            }
            else { isEqals = true; }
            return isEqals;
        }

        [CommandHandler("cmdExplainCardUpdate")]
        public virtual void UpdateBtnClick(object sender, EventArgs e)
        {
            _presenter.LoadMainData();
            UpdateCurrentInvoice(true);
        }

        [CommandHandler("cmdExplainCardSendToWork")]
        public virtual void SendToWorkBtnClick(object sender, EventArgs e)
        {
            // смена фокуса для обновления значений редактируемах ячеек
            this.ActiveControl = ueGroupBoxTaxPayerInfo;
            
            string message = String.Empty;
            bool isCorrect = CheckCorrectInvoices(out message);
            if (isCorrect)
            {
                _presenter.SendToMC("Передать в работу");
            }
            else
            {
                ShowWarning(message);
            }
        }

        [CommandHandler("cmdExplainCardCancelAllChange")]
        public virtual void CancelAllChangeBtnClick(object sender, EventArgs e)
        {
            List<UltraGridRow> rows = GetRowsFromAllGrid();
            _presenter.DeleteAllInvoiceCorrectState(rows, "Отменить все изменения");
        }

        [CommandHandler("cmdExplainCardCommit")]
        public virtual void CommitBtnClick(object sender, EventArgs e)
        {
            List<UltraGridRow> selectedRows = GetSelectedRowsFromCurrentGrid();
            foreach (var row in selectedRows)
            {
                ExplainInvoice inv = (ExplainInvoice)row.ListObject;
                string invoiceId = inv.ROW_KEY;
                _presenter.UpdateInvoiceCorrectState(invoiceId, ExplainInvoiceStateInThis.Processed, row, "Подтвердить");
            }
        }

        [CommandHandler("cmdExplainCardChange")]
        public virtual void ChangeBtnClick(object sender, EventArgs e)
        {
            List<UltraGridRow> selectedRows = GetSelectedRowsFromCurrentGrid();
            foreach (var row in selectedRows)
            {
                string invoiceId = ((ExplainInvoice)row.ListObject).ROW_KEY;
                _presenter.UpdateInvoiceCorrectState(invoiceId, ExplainInvoiceStateInThis.Changing, row, "Изменить");
            }
        }

        [CommandHandler("cmdExplainCardCancel")]
        public virtual void CancelBtnClick(object sender, EventArgs e)
        {
            List<UltraGridRow> selectedRows = GetSelectedRowsFromAllGrid();
            foreach (var row in selectedRows)
            {
                ExplainInvoice inv = (ExplainInvoice)row.ListObject;
                string invoiceId = inv.ROW_KEY;
                _presenter.DeleteInvoiceCorrectState(invoiceId, row, "Отменить");
            }
        }

        private List<UltraGridRow> GetSelectedRowsFromAllGrid()
        {
            List<UltraGridRow> selectedRows = new List<UltraGridRow>();
            foreach (KeyValuePair<int, GridControl> item in _grids)
            {
                selectedRows.AddRange(item.Value.GetSelectedRows());
            }
            return selectedRows;
        }

        private List<UltraGridRow> GetSelectedRowsFromCurrentGrid()
        {
            List<UltraGridRow> selectedRows = new List<UltraGridRow>();
            GridControl grid = GetCurrentGridInvoices();
            if (grid != null)
            {
                selectedRows.AddRange(grid.GetSelectedRows());
            }
            return selectedRows;
        }

        private List<UltraGridRow> GetRowsFromAllGrid()
        {
            List<UltraGridRow> rows = new List<UltraGridRow>();
            foreach (KeyValuePair<int, GridControl> item in _grids)
            {
                rows.AddRange(item.Value.GetRows());
            }
            return rows;
        }

        private Dictionary<int,List<UltraGridRow>> GetChapterRowsFromAllGrid()
        {
            Dictionary<int, List<UltraGridRow>> chapters = new Dictionary<int, List<UltraGridRow>>();

            foreach (KeyValuePair<int, GridControl> item in _grids)
            {
                List<UltraGridRow> rows = new List<UltraGridRow>();
                rows.AddRange(item.Value.GetRows());
                chapters.Add(item.Key, rows);
            }
            return chapters;
        }

        #endregion

        #region События

        private void View_OnFormClosing(object sender, EventArgs e)
        {
            _presenter.UpdateUserComment(teComment.Text);
        }

        private void linkLabelDocInfoNumValue_LinkClicked(object sender, Infragistics.Win.FormattedLinkLabel.LinkClickedEventArgs e)
        {
            _presenter.OpenDiscrepancyDocument();
        }

        private void LinkLabelNameValue_LinkClicked(object sender, Infragistics.Win.FormattedLinkLabel.LinkClickedEventArgs e)
        {
            _presenter.ViewTaxPayerByKppEffective(_presenter.DeclarationSummary.INN, _presenter.DeclarationSummary.KPP_EFFECTIVE);
        }

        private void ultraTabControlMain_SelectedTabChanged(object sender, Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventArgs e)
        {
            _timerLoadChapter.Enabled = true;
            SetLoadingVisibleCurrentInvoice();
        }

        private void CheckRowsForCommandChange(List<UltraGridRow> selectedRows)
        {
            isSelectOneRowWithoutStateChanging = false;
            foreach (UltraGridRow row in selectedRows)
            {
                ExplainInvoice inv = (ExplainInvoice)row.ListObject;
                if (!inv.EStates.States.Contains(ExplainInvoiceState.ChangingInThisExplain) &&
                    !inv.EStates.States.Contains(ExplainInvoiceState.ProcessedInThisExplain))
                {
                    isSelectOneRowWithoutStateChanging = true;
                }
                else
                {
                    isSelectOneRowWithoutStateChanging = false;
                    break;
                }
            }
        }

        private void CheckRowsForCommandCommit(List<UltraGridRow> selectedRows)
        {
            isSelectAnyRowWithoutStateProcessed = false;
            foreach (UltraGridRow row in selectedRows)
            {
                ExplainInvoice inv = (ExplainInvoice)row.ListObject;
                if (inv.State == ExplainInvoiceState.NotChanging)
                {
                    isSelectAnyRowWithoutStateProcessed = true;
                }
                else
                {
                    isSelectAnyRowWithoutStateProcessed = false;
                    break;
                }
            }
        }

        private void CheckRowsForCommandCancel(List<UltraGridRow> selectedRows)
        {
            isSelectAnyRowStateChangingOrProcessed = false;
            foreach (UltraGridRow row in selectedRows)
            {
                ExplainInvoice inv = (ExplainInvoice)row.ListObject;
                if (inv.State == ExplainInvoiceState.ChangingInThisExplain ||
                    inv.State == ExplainInvoiceState.ProcessedInThisExplain)
                {
                    isSelectAnyRowStateChangingOrProcessed = true;
                }
                else
                {
                    isSelectAnyRowStateChangingOrProcessed = false;
                    break;
                }
            }
        }

        public void CheckEnabledAllRibbonButton()
        {
            List<UltraGridRow> selectedRows = GetSelectedRowsFromCurrentGrid();
            CheckRowsForCommandChange(selectedRows);
            CheckRowsForCommandCommit(selectedRows);
            CheckRowsForCommandCancel(selectedRows);
            SetRibbonButtonEnaled();
        }

        public void CheckEnabledCancelAllButton(bool value)
        {
            isExistsInvoiceChangeOfExplain = value;
            SetRibbonButtonEnaled();
        }

        private void gridR8_AfterSelectRow(object sender, EventArgs e)
        {
            CheckEnabledAllRibbonButton();
        }

        private void GridAfterLoadData(DeclarationPart part)
        {
            if (isInitializeComplete && GetCurrentChapter() == (int)part)
            {
                GridControl currentGrid = GetCurrentGridInvoices();
                object data = GetCurrentGridInvoices().GetDataSource();
                if (data != null)
                {
                    List<ExplainInvoice> list = (List<ExplainInvoice>)data;
                    if (list.Count() > 0)
                    {
                        currentGrid.ResetShowSelectedItem();
                        _timerAfterLoadGrid.Enabled = true;
                    }
                    else
                    {
                        CheckEnabledAllRibbonButton();
                    }
                }
                else
                {
                    CheckEnabledAllRibbonButton();
                }
            }
        }

        private void gridR8_AfterLoadData(object sender, EventArgs e)
        {
            GridAfterLoadData(DeclarationPart.R8);
        }

        private void gridR9_AfterLoadData(object sender, EventArgs e)
        {
            GridAfterLoadData(DeclarationPart.R9);
        }

        private void gridR10_AfterLoadData(object sender, EventArgs e)
        {
            GridAfterLoadData(DeclarationPart.R10);
        }

        private void gridR11_AfterLoadData(object sender, EventArgs e)
        {
            GridAfterLoadData(DeclarationPart.R11);
        }

        private void gridR12_AfterLoadData(object sender, EventArgs e)
        {
            GridAfterLoadData(DeclarationPart.R12);
        }

        private void gridR8_ChildBandBeforeRowExpanded(object sender, EventArgs e)
        {
            SelectedRowEventArgs eRow = (SelectedRowEventArgs)e;
            UltraGridRow row = (UltraGridRow)eRow.DataObject;
            ExplainInvoice inv = (ExplainInvoice)row.ListObject;
            GridRowAction(inv.CHAPTER, row, false);
        }

        #endregion
    }
}
