﻿using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using CommonComponents.Utils.Async;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.Helpers.Explain;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Base.TransformQueryCondition;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard;
using Luxoft.NDS2.Client.UI.DiscrepancyDocuments.Models;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.Services;
using Microsoft.Practices.CompositeUI;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Explain.Manual
{
    public class ExplainReplyCardPresenter : BasePresenter<View>
    {
        #region Поля

        private AsyncWorker<int> mainDataWorker;

        private bool stopWorkers = false;

        private ExplainReplyCardData _exlpainReplyCardData;
        private ExplainReplyInfo _explainReplyInfo;
        private DiscrepancyDocumentInfo _discrepancyDocumentInfo;
        private DeclarationSummary _declarationSummary;
        private IExplainReplyDataService explainReplyService;
        private IDiscrepancyDocumentService discrepancyDocumentService;
        private Dictionary<int, Action<DiscrepancyDocumentInfo>> actions;
        private List<ExplainReplyHistoryStatus> _explainReplyHistoryStatuses;
        private List<DictionaryInvoiceStateRecord> _invoiceStateRecords;
        private Dictionary<int, AsyncWorker<int>> invoiceWorkers = new Dictionary<int, AsyncWorker<int>>();
        private Dictionary<int, RequestStateAsync> requestStateAsyncInvoices = new Dictionary<int, RequestStateAsync>();
        private Dictionary<string, Func<object, bool>> actionChecks = new Dictionary<string, Func<object, bool>>();
        private List<OperationUpdateParameters> operationUpdateStates = new List<OperationUpdateParameters>();

        private readonly IExplainInvoiceService _explainInvoiceService;
        private readonly ExplainFieldFormatter _explainFieldFormatter;

        #endregion

        public ExplainReplyCardPresenter(View view, PresentationContext ctx, WorkItem wi, ExplainReplyCardData exlpainReplyCardData)
            : base(ctx, wi, view)
        {
            _exlpainReplyCardData = exlpainReplyCardData;
            explainReplyService = GetServiceProxy<IExplainReplyDataService>();
            _explainInvoiceService = GetServiceProxy<IExplainInvoiceService>();
            discrepancyDocumentService = GetServiceProxy<IDiscrepancyDocumentService>();
            _explainFieldFormatter = new ExplainFieldFormatter();
            InitActionViewDocInfo();
            InitActionCheckFields();
        }

        private void InitActionViewDocInfo()
        {
            actions = new Dictionary<int, Action<DiscrepancyDocumentInfo>>
            {
                {DocType.ClaimKS, 		ViewClaimDetails},
                {DocType.ClaimSF, 		ViewClaimDetails},
                {DocType.Reclaim93, 	ViewReclaimDetails},
                {DocType.Reclaim93_1, 	ViewReclaimDetails}
            };
        }

        public void LoadMainData()
        {
            mainDataWorker = new AsyncWorker<int>();
            mainDataWorker.DoWork += (sender, args) =>
            {
                FillExplainReplyInfo(_exlpainReplyCardData.ExplainReplyId);
                if (_explainReplyInfo != null)
                {
                    FillDocument(_explainReplyInfo.DOC_ID);
                    if (_discrepancyDocumentInfo != null)
                    {
                        FillDeclaration(_discrepancyDocumentInfo.DeclarationVersionId);
                    }
                }
                FillExplainReplyHistoryStatuses(_exlpainReplyCardData.ExplainReplyId);
            };
            mainDataWorker.Complete += (sender, args) =>
            {
                View.ExecuteInUiThread(c =>
                {
                    View.LoadData();
                });
            };
            mainDataWorker.Failed += (sender, args) =>
            {
                if (!this.stopWorkers)
                    View.ShowError(string.Format(ResourceManagerNDS2.LoadError, args.Exception == null ? string.Empty : args.Exception.Message));
            };
            mainDataWorker.Start();
        }

        public Dictionary<string, object> ConvertToDictionary<T>(T entity)
        {
            Dictionary<string, object> dictEntity = new Dictionary<string, object>();
            Type typeClass = entity.GetType();
            PropertyInfo[] properties = typeClass.GetProperties();
            foreach (PropertyInfo itemProp in properties)
            {
                string name = itemProp.Name;
                object value = itemProp.GetValue(entity, null);
                dictEntity.Add(name, value);
            }
            return dictEntity;
        }

        public void OpenDiscrepancyDocument()
        {
            DiscrepancyDocumentInfo docInfo = _discrepancyDocumentInfo;
            if (actions.ContainsKey(docInfo.DocType.EntryId))
            {
                actions[docInfo.DocType.EntryId].Invoke(docInfo);
            }
        }

        #region Загрузка СФ

        public object GetInvocesCached(int num, QueryConditions qc, out long totalRowsNumber)
        {
            RequestStateAsync requestStateAsyncInvoice = GetRequestStateAsyncInvoices(num);
            if (requestStateAsyncInvoice.State == StateAsync.None ||
                requestStateAsyncInvoice.State == StateAsync.Start)
            {
                GetInvoicesAsync(num, qc);
            }
            else if (requestStateAsyncInvoice.State == StateAsync.Complete)
            {
                if (qc != null &&
                    requestStateAsyncInvoice.QueryConditionsSend != null &&
                    (!qc.EqualsBase(requestStateAsyncInvoice.QueryConditionsSend) ||
                    !QueryConditions.EqualsSorting(qc, requestStateAsyncInvoice.QueryConditionsSend)))
                {
                    GetInvoicesAsync(num, qc);
                }
                else
                {
                    requestStateAsyncInvoice.QueryConditionsSend = (QueryConditions)qc.Clone();
                    View.GetGridInvoices(num).PanelLoadingVisible = false;
                }
            }
            totalRowsNumber = requestStateAsyncInvoice.TotalRowsNumber;
            return requestStateAsyncInvoice.ResultData;
        }

        public void SetStateWorkerStart(int chapter)
        {
            RequestStateAsync requestStateAsyncInvoice = GetRequestStateAsyncInvoices(chapter);
            SetRuqestStateAsyncInvoceEmpty(requestStateAsyncInvoice);
            requestStateAsyncInvoice.State = StateAsync.Start;
        }

        private void SetRuqestStateAsyncInvoceEmpty(RequestStateAsync requestStateAsyncInvoice)
        {
            List<ExplainInvoice> invoices = new List<ExplainInvoice>();
            requestStateAsyncInvoice.ResultData = invoices;
            requestStateAsyncInvoice.TotalRowsNumber = 0;
        }

        private RequestStateAsync GetRequestStateAsyncInvoices(int num)
        {
            RequestStateAsync ret = null;
            if (requestStateAsyncInvoices.ContainsKey(num))
            {
                ret = requestStateAsyncInvoices[num];
            }
            else
            {
                ret = new RequestStateAsync();
                SetRuqestStateAsyncInvoceEmpty(ret);
                requestStateAsyncInvoices.Add(num, ret);
            }
            return ret;
        }

        private void GetInvoicesAsync(int num, QueryConditions qc)
        {
            RequestStateAsync requestStateAsyncInvoice = GetRequestStateAsyncInvoices(num);
            SetRuqestStateAsyncInvoceEmpty(requestStateAsyncInvoice);
            requestStateAsyncInvoice.QueryConditionsSend = (QueryConditions)qc.Clone();

            if (requestStateAsyncInvoice.OperationCTS != null)
            {
                requestStateAsyncInvoice.OperationCTS.Cancel();
            }

            requestStateAsyncInvoice.OperationCTS = ExecuteServiceCallAsync<OperationResult>(
            (c) =>
            {
                if (!c.IsCancellationRequested)
                {
                    requestStateAsyncInvoice.State = StateAsync.Send;
                }

                var queryConditionsTransform = TransformQueryConditions(qc);

                long totalRowsNumber = 0;
                List<ExplainInvoice> ret = null;
                OperationResult rez = new OperationResult();
                OperationResult<PageResult<ExplainInvoice>> result =
                    _exlpainReplyCardData.Regim == ExplainRegim.Edit
                        ? _explainInvoiceService.GetAllClaimInvoices(
                            _exlpainReplyCardData.ExplainReplyInfo.EXPLAIN_ID, 
                            num,
                            queryConditionsTransform)
                        : _explainInvoiceService.GetModifiedInvoices(
                            _exlpainReplyCardData.ExplainReplyInfo.EXPLAIN_ID, 
                            num,
                            queryConditionsTransform);
                if (result != null)
                {
                    rez.Status = result.Status;
                    rez.Message = string.Format("загрузка данных раздела {0}, {1}", num, result.Message);
                    PageResult<ExplainInvoice> serviceResult = result.Result;
                    if (serviceResult != null)
                    {
                        ret = serviceResult.Rows;
                        ret.ForEach(invoice =>
                                        {
                                            invoice.RefreshProperties();
                                            TransformChildInvoiceFields(invoice);
                                        });
                        totalRowsNumber = serviceResult.TotalMatches;
                    }
                }
                if (null == ret)
                {
                    ret = new List<ExplainInvoice>();
                    totalRowsNumber = 0;
                }

                if (!c.IsCancellationRequested)
                {
                    requestStateAsyncInvoice.ResultData = ret;
                    requestStateAsyncInvoice.TotalRowsNumber = totalRowsNumber;
                }

                return rez;
            },
            (c, rez) =>
            {
                requestStateAsyncInvoice.State = StateAsync.Complete;
                View.ExecuteInUiThread(n =>
                {
                    View.GetGridInvoices(num).PanelLoadingVisible = false;
                    View.GetGridInvoices(num).UpdateData();
                });
            },
            (c, rez, exp) =>
            {
                SetRuqestStateAsyncInvoceEmpty(requestStateAsyncInvoice);
                requestStateAsyncInvoice.State = StateAsync.Complete;
                View.ExecuteInUiThread(m =>
                {
                    if (!this.stopWorkers)
                        View.GetGridInvoices(num).PanelLoadingVisible = false;
                });
                if (!this.stopWorkers)
                {
                    string errorMessage = String.Empty;
                    if (rez != null && !string.IsNullOrWhiteSpace(rez.Message))
                        errorMessage = rez.Message;
                    else
                        errorMessage = exp == null ? string.Empty : exp.Message;
                    View.ShowError(string.Format(ResourceManagerNDS2.LoadError, errorMessage));
                }
            });
        }

        private void TransformChildInvoiceFields(ExplainInvoice invoice)
        {
            foreach (var invoiceCorrect in invoice.RelatedInvoices)
            {
                invoiceCorrect.INVOICE_DATE = TransformDate(invoiceCorrect.INVOICE_DATE);
                invoiceCorrect.CHANGE_DATE = TransformDate(invoiceCorrect.CHANGE_DATE);
                invoiceCorrect.CORRECTION_DATE = TransformDate(invoiceCorrect.CORRECTION_DATE);
                invoiceCorrect.CHANGE_CORRECTION_DATE = TransformDate(invoiceCorrect.CHANGE_CORRECTION_DATE);
                invoiceCorrect.RECEIPT_DOC_DATE = TransformDate(invoiceCorrect.RECEIPT_DOC_DATE);
                invoiceCorrect.BUY_ACCEPT_DATE = TransformDate(invoiceCorrect.BUY_ACCEPT_DATE);
                invoiceCorrect.CREATE_DATE = TransformDate(invoiceCorrect.CREATE_DATE);
                invoiceCorrect.SELLER_AGENCY_INFO_DATE = TransformDate(invoiceCorrect.SELLER_AGENCY_INFO_DATE);
                invoiceCorrect.RECEIVE_DATE = TransformDate(invoiceCorrect.RECEIVE_DATE);
                invoiceCorrect.SELLER_INVOICE_DATE = TransformDate(invoiceCorrect.SELLER_INVOICE_DATE);

                invoiceCorrect.PRICE_TOTAL = TransformDecimal(invoiceCorrect.PRICE_TOTAL);
                invoiceCorrect.DIFF_CORRECT_NDS_DECREASE = TransformDecimal(invoiceCorrect.DIFF_CORRECT_NDS_DECREASE);
                invoiceCorrect.DIFF_CORRECT_NDS_INCREASE = TransformDecimal(invoiceCorrect.DIFF_CORRECT_NDS_INCREASE);
                invoiceCorrect.PRICE_BUY_AMOUNT = TransformDecimal(invoiceCorrect.PRICE_BUY_AMOUNT);
                invoiceCorrect.PRICE_TAX_FREE = TransformDecimal(invoiceCorrect.PRICE_TAX_FREE);
                invoiceCorrect.PRICE_SELL_IN_CURR = TransformDecimal(invoiceCorrect.PRICE_SELL_IN_CURR);
                invoiceCorrect.PRICE_SELL = TransformDecimal(invoiceCorrect.PRICE_SELL);
                invoiceCorrect.PRICE_SELL_18 = TransformDecimal(invoiceCorrect.PRICE_SELL_18);
                invoiceCorrect.PRICE_SELL_10 = TransformDecimal(invoiceCorrect.PRICE_SELL_10);
                invoiceCorrect.PRICE_SELL_0 = TransformDecimal(invoiceCorrect.PRICE_SELL_0);

                invoiceCorrect.LastInvoiceChange = (ExplainInvoice)invoiceCorrect.Clone();
            }
        }

        private string TransformDate(string valueDate)
        {
            string ret = valueDate;
            DateTime dt = DateTime.MinValue;
            if (ExplainParser.TryParseDataTime(valueDate, out dt))
            {
                ret = dt.ToShortDateString();
            }
            return ret;
        }

        private string TransformDecimal(string value)
        {
            string ret = value;
            decimal decimalValue = -1;
            if (ExplainParser.TryParseDecimal(value, out decimalValue))
            {
                ret = decimalValue.ToString(CultureInfo.CurrentCulture);
            }
            return ret;
        }

        private QueryConditions TransformQueryConditions(QueryConditions queryConditions)
        {
            QueryConditions queryConditionsTransform = (QueryConditions)queryConditions.Clone();

            var fieldNamesOfDateTime = new List<string>()
            {
                TypeHelper<ExplainInvoice>.GetMemberName(t => t.INVOICE_DATE),
                TypeHelper<ExplainInvoice>.GetMemberName(t => t.CHANGE_DATE),
                TypeHelper<ExplainInvoice>.GetMemberName(t => t.CORRECTION_DATE),
                TypeHelper<ExplainInvoice>.GetMemberName(t => t.CHANGE_CORRECTION_DATE),
                TypeHelper<ExplainInvoice>.GetMemberName(t => t.RECEIPT_DOC_DATE),
                TypeHelper<ExplainInvoice>.GetMemberName(t => t.BUY_ACCEPT_DATE),
                TypeHelper<ExplainInvoice>.GetMemberName(t => t.CREATE_DATE),
                TypeHelper<ExplainInvoice>.GetMemberName(t => t.SELLER_AGENCY_INFO_DATE),
                TypeHelper<ExplainInvoice>.GetMemberName(t => t.RECEIVE_DATE),
                TypeHelper<ExplainInvoice>.GetMemberName(t => t.INVOICE_DATE)
            };

            TransformQueryConditionCreator
                .CreateTransformExplainDateTime(queryConditionsTransform, fieldNamesOfDateTime)
                .TransformFiltering();

            var fieldNamesOfDecimal = new List<string>()
            {
                TypeHelper<ExplainInvoice>.GetMemberName(t => t.PRICE_BUY_AMOUNT),
                TypeHelper<ExplainInvoice>.GetMemberName(t => t.PRICE_SELL_IN_CURR),
                TypeHelper<ExplainInvoice>.GetMemberName(t => t.PRICE_SELL),
                TypeHelper<ExplainInvoice>.GetMemberName(t => t.PRICE_SELL_18),
                TypeHelper<ExplainInvoice>.GetMemberName(t => t.PRICE_SELL_10),
                TypeHelper<ExplainInvoice>.GetMemberName(t => t.PRICE_SELL_0),
                TypeHelper<ExplainInvoice>.GetMemberName(t => t.PRICE_TAX_FREE),
                TypeHelper<ExplainInvoice>.GetMemberName(t => t.PRICE_TOTAL),
                TypeHelper<ExplainInvoice>.GetMemberName(t => t.DIFF_CORRECT_NDS_DECREASE),
                TypeHelper<ExplainInvoice>.GetMemberName(t => t.DIFF_CORRECT_NDS_INCREASE)
            };

            TransformQueryConditionCreator
                .CreateTransformExplainDecimal(queryConditionsTransform, fieldNamesOfDecimal)
                .TransformFiltering();

            return queryConditionsTransform;
        }

        #endregion

        #region Заполнение данных

        private void FillExplainReplyInfo(long explainReplyId)
        {
            _explainReplyInfo = GetExplainReplyInfoDB(explainReplyId);
        }

        private void FillDeclaration(long declarationId)
        {
            _declarationSummary = GetDeclarationDB(declarationId);
        }

        private void FillDocument(long docId)
        {
            _discrepancyDocumentInfo = GetDiscrepancyDocumentInfoDB(docId);
        }

        private void FillDictionaryInvoiceStateRecords()
        {
            _invoiceStateRecords = new List<DictionaryInvoiceStateRecord>();
            _invoiceStateRecords.Add(new DictionaryInvoiceStateRecord() { InvoiceState = ExplainInvoiceState.NotChanging, ColorARGB = 0, Description = "" });
            _invoiceStateRecords.Add(new DictionaryInvoiceStateRecord() { InvoiceState = ExplainInvoiceState.ChangingInThisExplain, ColorARGB = -252, Description = "Измененная запись" });
            _invoiceStateRecords.Add(new DictionaryInvoiceStateRecord() { InvoiceState = ExplainInvoiceState.ChangingInPreviousExplain, ColorARGB = -253, Description = "Ранее измененная запись" });
            _invoiceStateRecords.Add(new DictionaryInvoiceStateRecord() { InvoiceState = ExplainInvoiceState.ProcessedInThisExplain, ColorARGB = -254, Description = "Подтвержденная запись" });
            _invoiceStateRecords.Add(new DictionaryInvoiceStateRecord() { InvoiceState = ExplainInvoiceState.ProcessedInPreviousExplain, ColorARGB = -255, Description = "Ранее подтвержденная запись" });
        }

        private void FillExplainReplyHistoryStatuses(long explainId)
        {
            _explainReplyHistoryStatuses = GetExplainReplyHistoryStatuesDB(explainId);
        }

        #endregion

        #region Данные

        public ExplainReplyInfo ExplainReplyInfo
        {
            get
            {
                return _explainReplyInfo;
            }
            set
            {
                _explainReplyInfo = value;
            }
        }

        public DiscrepancyDocumentInfo DiscrepancyDocumentInfo
        {
            get
            {
                return _discrepancyDocumentInfo;
            }
            set
            {
                _discrepancyDocumentInfo = value;
            }
        }

        public DeclarationSummary DeclarationSummary
        {
            get
            {
                return _declarationSummary;
            }
            set
            {
                _declarationSummary = value;
            }
        }

        public List<ExplainReplyHistoryStatus> ExplainReplyHistoryStatuses
        {
            get
            {
                if (null == _explainReplyHistoryStatuses)
                {
                    FillExplainReplyHistoryStatuses(_exlpainReplyCardData.ExplainReplyId);
                }
                return _explainReplyHistoryStatuses;
            }
        }

        public List<DictionaryInvoiceStateRecord> InvoiceStateRecords
        {
            get
            {
                if (null == _invoiceStateRecords)
                {
                    FillDictionaryInvoiceStateRecords();
                }
                return _invoiceStateRecords;
            }
        }

        #endregion

        #region Методы работы с сервисами (БД)

        private ExplainReplyInfo GetExplainReplyInfoDB(long explainId)
        {
            ExplainReplyInfo ret = null;
            if (ExecuteServiceCall(
                    () => explainReplyService.GetExplainReplyInfo(explainId),
                    (result) => ret = result.Result))
            { }
            return ret;
        }

        private DeclarationSummary GetDeclarationDB(long declarationId)
        {
            DeclarationSummary ret = null;
            if (ExecuteServiceCall(
                    () => discrepancyDocumentService.GetDeclaration(declarationId),
                    (result) => ret = result.Result))
            { }
            return ret;
        }

        public DiscrepancyDocumentInfo GetDiscrepancyDocumentInfoDB(long docId)
        {
            DiscrepancyDocumentInfo ddi = null;
            if (ExecuteServiceCall(() => discrepancyDocumentService.GetDocument(docId),
                res =>
                {
                    ddi = res.Result;
                }))
            { }
            return ddi;
        }

        private List<ExplainReplyHistoryStatus> GetExplainReplyHistoryStatuesDB(long explainId)
        {
            List<ExplainReplyHistoryStatus> surs = null;
            if (ExecuteServiceCall(() => explainReplyService.GetExplainReplyHistoryStatues(explainId),
                res =>
                {
                    surs = res.Result;
                }))
            {
                return surs;
            }
            return new List<ExplainReplyHistoryStatus>();
        }

        public void UpdateUserComment(string userComment)
        {
            if (_explainReplyInfo != null && _explainReplyInfo.user_comment != userComment)
            {
                OperationResult operRes = null;
                if (ExecuteServiceCall(
                        () => explainReplyService.UpdateUserComment(_explainReplyInfo.EXPLAIN_ID, userComment),
                        (result) => operRes = result))
                { }
            }
        }

        #endregion

        #region Форматный контроль

        //Обязательно для заполнения. Формат ввода: XX Где XX – цифры
        private bool IsOperationCode(int code)
        {
            // TODO ДН добавить внятный контроль
            return code > 0 && code < 100;
        }

        private bool IsOperationCode(string code)
        {
            if (string.IsNullOrWhiteSpace(code))
                return false;
            code = code.Trim();
            int temp;
            return (code.Length >= 1 && int.TryParse(code, out temp) && IsOperationCode(temp));
        }

        private bool CheckOperationCode(object value)
        {
            var line = value as string;
            if (string.IsNullOrEmpty(line))
                return false;

            var codes = line.Split(',');
            return codes.All(IsOperationCode);
        }

        //Текст (от 1 до 256 знаков)
        private bool CheckLengthString(object value, int maxLength)
        {
            bool rez = false;
            if (value != null)
            {
                string s = (string)value;
                s = s.Trim();
                if (s.Length <= maxLength)
                {
                    rez = true;
                }
            }
            return rez;
        }

        private bool CheckListOfLengthString(object value, int maxLength)
        {
            var stringValue = value as string;
            if (string.IsNullOrWhiteSpace(stringValue))
                return false;
            var list = stringValue.Split(',');
            return list.All(x => CheckLengthString(x, maxLength));
        }

        //Дата (дд.мм.гггг) или не заполняется
        private bool CheckDate(object value)
        {
            bool rez = false;
            if (value != null)
            {
                string s = (string)value;
                s = s.Trim();
                DateTime dt;
                string currentFormat = CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern;
                if (DateTime.TryParseExact(s, currentFormat, CultureInfo.CurrentCulture, DateTimeStyles.None, out dt))
                {
                    rez = true;
                }
            }
            return rez;
        }

        private bool CheckListDate(object value)
        {
            var stringValue = value as string;
            if (string.IsNullOrWhiteSpace(stringValue))
                return false;
            var list = stringValue.Split(',');
            return list.All(x => CheckDate(x));
        }

        //Только цифры (от 1 до 3 знаков) или не заполняется
        private bool CheckInteger(object value, int maxLength)
        {
            bool rez = false;
            if (value != null)
            {
                string s = (string)value;
                s = s.Trim();
                if (s.Length <= maxLength)
                {
                    int temp = -1;
                    if (int.TryParse(s, out temp))
                    {
                        rez = true;
                    }
                }
            }
            return rez;
        }

        //Только цифры (10 или 12 знаков) или не заполняется
        private bool CheckIntegerCertainLength(object value, List<int> lengths)
        {
            var s = value as string;
            if (string.IsNullOrWhiteSpace(s))
                return false;
            s = s.Trim();
            return (lengths.Contains(s.Length) && IsDigit(s));
        }

        private bool CheckListOfIntegerCertainLength(object value, List<int> lengths)
        {
            var s = value as string;
            if (string.IsNullOrWhiteSpace(s))
                return false;
            var list = s.Split(',');
            return list.All(x => CheckIntegerCertainLength(x, lengths));
        }

        private bool IsDigit(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                return false;
            if (value.Length == 0)
                return false;
            for (int i = -1; ++i < value.Length; )
            {
                if (!char.IsDigit(value[i]))
                    return false;
            }
            return true;
        }

        private bool IsDigitOrLatin(string value)
        {
            bool rez = false;
            Match match = Regex.Match(value, @"^[A-Za-z0-9]+$", RegexOptions.None);
            if (match.Success)
            {
                rez = true;
            }
            return rez;
        }

        //Цифры / буквы (9 знаков)  или не заполняется.
        //Формат ввода: NNNNPPXXX
        //Где 
        //NNNN – цифры
        //PP – цифры или латинские буквы
        //XXX – цифры
        private bool CheckKPP(object value)
        {
            bool rez = false;
            if (value != null)
            {
                string s = (string)value;
                s = s.Trim();
                if (s.Length == 0)
                {
                    rez = true;
                }
                else if (s.Length == 9)
                {
                    bool isNext = true;
                    isNext = IsDigit(s.Substring(0, 4));
                    if (isNext)
                    {
                        isNext = IsDigitOrLatin(s.Substring(4, 2));
                    }
                    if (isNext)
                    {
                        isNext = IsDigit(s.Substring(6, 3));
                    }
                    rez = isNext;
                }
            }
            return rez;
        }

        private bool CheckListKPP(object value)
        {
            var stringValue = value as string;
            if (string.IsNullOrWhiteSpace(stringValue))
                return false;
            var list = stringValue.Split(',');
            return list.All(x => CheckKPP(x));
        }

        //Число с точностью до копеек (до 17 знаков в целой части и 2 знака в дробной части). 
        //Необходима маска ввода, с указанием разделителя.
        //В качестве разделителя используется разделитель указанный в локальных настройках.
        //Если копейки не указываются, то необходимо подставлять «00».
        private bool CheckDecimal(object value, int integerPart, int fractionalPart, string decimalSeparator)
        {
            bool rez = false;
            if (value != null)
            {
                string stringValue = (string)value;
                stringValue = stringValue.Trim();
                int separatorLength = decimalSeparator.Length;
                if (stringValue.Length <= (integerPart + fractionalPart + separatorLength))
                {
                    bool isNext = false;
                    int pos = stringValue.IndexOf(decimalSeparator);
                    if (pos >= 0)
                    {
                        isNext = true;
                        string mainPart = stringValue.Substring(0, pos);
                        if (mainPart.Length > 0 && mainPart.Length <= integerPart)
                        {
                            isNext = IsDigit(mainPart);
                        }
                        else
                        {
                            isNext = false;
                        }
                        if (isNext)
                        {
                            string secondPart = stringValue.Substring(pos + separatorLength, stringValue.Length - pos - separatorLength);
                            if (secondPart.Length > 0 && secondPart.Length <= fractionalPart)
                            {
                                isNext = IsDigit(secondPart);
                            }
                            else
                            {
                                isNext = false;
                            }
                        }
                    }
                    else
                        isNext = IsDigit(stringValue);
                    rez = isNext;
                }
            }
            return rez;
        }

        //Только цифры (1 знак).
        //Формат ввода: X
        //Где X – цифры от 0 до 4.
        private bool CheckDealKindCode(object value)
        {
            bool rez = false;
            if (value != null)
            {
                string s = (string)value;
                s = s.Trim();
                Match match = Regex.Match(s, @"^[0-4]{1}$", RegexOptions.None);
                if (match.Success)
                {
                    rez = true;
                }
            }
            return rez;
        }

        private void InitActionCheckFields()
        {
            NumberFormatInfo numberFormatInfo = System.Globalization.CultureInfo.CurrentCulture.NumberFormat;
            string decimalSeparator = numberFormatInfo.NumberDecimalSeparator;

            actionChecks.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.OPERATION_CODE), (value) => { return CheckOperationCode(value); });
            actionChecks.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.INVOICE_NUM), (value) => { return CheckLengthString(value, 256); });
            actionChecks.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.INVOICE_DATE), (value) => { return CheckDate(value); });
            actionChecks.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.CHANGE_NUM), (value) => { return CheckInteger(value, 3); });
            actionChecks.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.CHANGE_DATE), (value) => { return CheckDate(value); });
            actionChecks.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.CORRECTION_NUM), (value) => { return CheckLengthString(value, 256); });
            actionChecks.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.CORRECTION_DATE), (value) => { return CheckDate(value); });
            actionChecks.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.CHANGE_CORRECTION_NUM), (value) => { return CheckInteger(value, 3); });
            actionChecks.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.CHANGE_CORRECTION_DATE), (value) => { return CheckDate(value); });
            actionChecks.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.RECEIPT_DOC_NUM), (value) => { return CheckListOfLengthString(value, 256); });
            actionChecks.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.RECEIPT_DOC_DATE), (value) => { return CheckListDate(value); });
            actionChecks.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.BUY_ACCEPT_DATE), (value) => { return CheckListDate(value); });
            actionChecks.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.SELLER_INN), (value) => { return CheckIntegerCertainLength(value, new List<int>() { 10, 12 }); });
            actionChecks.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.SELLER_KPP), (value) => { return CheckListKPP(value); });
            actionChecks.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.BROKER_INN), (value) => { return CheckIntegerCertainLength(value, new List<int>() { 10, 12 }); });
            actionChecks.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.BROKER_KPP), (value) => { return CheckKPP(value); });
            actionChecks.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.CUSTOMS_DECLARATION_NUM), (value) => { return CheckLengthString(value, 29); });
            actionChecks.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.OKV_CODE), (value) => { return CheckIntegerCertainLength(value, new List<int>() { 3 }); });
            actionChecks.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.BUYER_INN), (value) => { return CheckIntegerCertainLength(value, new List<int>() { 10, 12 }); });
            actionChecks.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.BUYER_KPP), (value) => { return CheckListKPP(value); });
            actionChecks.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.SELLER_AGENCY_INFO_INN), (value) => { return CheckIntegerCertainLength(value, new List<int>() { 10, 12 }); });
            actionChecks.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.SELLER_AGENCY_INFO_KPP), (value) => { return CheckKPP(value); });
            actionChecks.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.SELLER_AGENCY_INFO_NUM), (value) => { return CheckLengthString(value, 256); });
            actionChecks.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.SELLER_AGENCY_INFO_DATE), (value) => { return CheckDate(value); });
            actionChecks.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.RECEIVE_DATE), (value) => { return CheckDate(value); });
            actionChecks.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.DEAL_KIND_CODE), (value) => { return CheckDealKindCode(value); });
            actionChecks.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.SELLER_INVOICE_DATE), (value) => { return CheckDate(value); });
            actionChecks.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.CREATE_DATE), (value) => { return CheckDate(value); });

            actionChecks.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.PRICE_TOTAL), (value) => { return CheckDecimal(value, 19, 2, decimalSeparator); });
            actionChecks.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.DIFF_CORRECT_NDS_DECREASE), (value) => { return CheckDecimal(value, 19, 2, decimalSeparator); });
            actionChecks.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.DIFF_CORRECT_NDS_INCREASE), (value) => { return CheckDecimal(value, 19, 2, decimalSeparator); });
            actionChecks.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.PRICE_BUY_AMOUNT), (value) => { return CheckDecimal(value, 19, 2, decimalSeparator); });
            actionChecks.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.PRICE_TAX_FREE), (value) => { return CheckDecimal(value, 19, 2, decimalSeparator); });
            actionChecks.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.PRICE_SELL_IN_CURR), (value) => { return CheckDecimal(value, 19, 2, decimalSeparator); });
            actionChecks.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.PRICE_SELL), (value) => { return CheckDecimal(value, 19, 2, decimalSeparator); });
            actionChecks.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.PRICE_SELL_18), (value) => { return CheckDecimal(value, 19, 2, decimalSeparator); });
            actionChecks.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.PRICE_SELL_10), (value) => { return CheckDecimal(value, 19, 2, decimalSeparator); });
            actionChecks.Add(TypeHelper<ExplainInvoice>.GetMemberName(t => t.PRICE_SELL_0), (value) => { return CheckDecimal(value, 19, 2, decimalSeparator); });
       }

        public bool CheckFieldCorrectInvoice(int chapter, string fieldName, object fieldValue)
        {
            bool ret = false;
            if (actionChecks.ContainsKey(fieldName))
            {
                ret = actionChecks[fieldName].Invoke(fieldValue);
            }
            return ret;
        }

        #endregion

        #region Функции

        private OperationUpdateParameters CreateOperationUpdateParameters(ExplainInvoice explainInvoice, string invoiceId, string fieldName, object fieldValue, InvoiceCorrectTypeOperation typeOper)
        {
            var operationParams = new OperationUpdateParameters();

            operationParams.Id = DateTime.Now.Ticks;
            operationParams.InvoiceId = invoiceId;
            operationParams.FieldName = fieldName;
            operationParams.ExplainInvoice = explainInvoice;
            operationParams.FieldValue = (string)fieldValue;
            operationParams.TypeOperation = typeOper;
            operationUpdateStates.Add(operationParams);

            return operationParams;
        }

        public void UpdateInvoiceCorrect(ExplainInvoice explainInvoice, string invoiceId, string fieldName, object fieldValue, InvoiceCorrectTypeOperation typeOper)
        {
            OperationUpdateParameters operationParams = CreateOperationUpdateParameters(explainInvoice, invoiceId, fieldName, fieldValue, typeOper);
            UpdateInvoiceCorrect(operationParams);
        }

        private void UpdateInvoiceCorrect(OperationUpdateParameters operationParams)
        {
            operationParams.OperationCTS = ExecuteServiceCallAsync<OperationResult>(
            (c) =>
            {
                OperationResult rez = null;

                if (_explainReplyInfo != null)
                {
                    ExplainInvoiceCorrect correct = new ExplainInvoiceCorrect();
                    correct.ExplainId = _explainReplyInfo.EXPLAIN_ID;
                    correct.InvoiceId = operationParams.InvoiceId;
                    correct.FieldName = operationParams.FieldName;
                    correct.FieldValue = _explainFieldFormatter.PrepareValue(operationParams.FieldName, operationParams.FieldValue);

                    rez = explainReplyService.UpdateInvoiceCorrect(_explainReplyInfo.EXPLAIN_ID, correct, operationParams.TypeOperation);
                }
                else
                {
                    rez = new OperationResult();
                    rez.Status = ResultStatus.Undefined;
                }

                return rez;
            },
            (c, rez) =>
            {
                operationParams.ExplainInvoice.LastInvoiceChange = (ExplainInvoice)operationParams.ExplainInvoice.Clone();
                RemoveOpearation(operationParams);
            },
            (c, rez, exp) =>
            {
                RemoveOpearation(operationParams);
                FailedOperation(rez, exp);
            },
            (rez) =>
            {
                RemoveOpearation(operationParams);
            });
        }

        private void RemoveOpearation(OperationUpdateParameters operationParams)
        {
            operationUpdateStates.Remove(operationParams);
        }

        public void SendToMC(string commandName)
        {
            OperationUpdateParameters operationParams = new OperationUpdateParameters();
            operationParams.ExplainStatusId = ExplainReplyStatus.SendToMC;
            operationParams.CommandName = commandName;

            operationParams.OperationCTS = ExecuteServiceCallAsync<OperationResult>(
            (c) =>
            {
                OperationResult resExplainXML = explainReplyService.SendExplainXMLtoMC(_explainReplyInfo.EXPLAIN_ID, _declarationSummary.DECLARATION_VERSION_ID);
                if (resExplainXML.Status == ResultStatus.Success)
                {
                    return explainReplyService.UpdateExplainStatus(_explainReplyInfo.EXPLAIN_ID, operationParams.ExplainStatusId);
                }
                else
                {
                    OperationResult res = new OperationResult();
                    res.Status = resExplainXML.Status;
                    res.Message = resExplainXML.Message;
                    return res;
                }
            },
            (c, rez) =>
            {
                Close();
            },
            (c, rez, exp) =>
            {
                FailedOperation(rez, exp);
            });
        }

        private void FailedOperation(OperationResult rez, Exception exp)
        {
            if (rez != null || exp != null)
            {
                string message = String.Empty;
                if (rez != null)
                {
                    message = rez.Message;
                }
                else if (exp != null)
                {
                    message = exp.Message;
                }
                View.ExecuteInUiThread(a =>
                {
                    View.ShowError(message);
                });
            }
        }

        private void Close()
        {
            var managementService = WorkItem.Services.Get<IWindowsManagerService>();

            Guid smartPartId;

            if (managementService.TryGetSmartPartId(this.View, out smartPartId))
                managementService.TryClose(smartPartId);
        }

        public void UpdateInvoiceCorrectState(string invoiceId, ExplainInvoiceStateInThis invoiceStateId, UltraGridRow row, string commandName)
        {
            OperationUpdateParameters operationParams = new OperationUpdateParameters();
            operationParams.InvoiceId = invoiceId;
            operationParams.InvoiceStateId = invoiceStateId;
            operationParams.CommandName = commandName;
            operationParams.Row = row;

            UpdateInvoiceCorrectState(operationParams);
        }

        private void UpdateInvoiceCorrectState(OperationUpdateParameters operationParams)
        {
            operationParams.OperationCTS = ExecuteServiceCallAsync<OperationResult>(
            (c) =>
            {
                return explainReplyService.UpdateInvoiceCorrectState(_explainReplyInfo.EXPLAIN_ID, operationParams.InvoiceId, operationParams.InvoiceStateId);
            },
            (c, rez) =>
            {
                ExplainInvoice inv = (ExplainInvoice)operationParams.Row.ListObject;
                if (operationParams.InvoiceStateId == ExplainInvoiceStateInThis.Changing)
                {
                    inv.AddState(ExplainInvoiceState.ChangingInThisExplain);

                    //--------------Add New Correct Invoice ----------------------
                    ExplainInvoice invoiceCorrect = null;
                    if (inv.RelatedInvoices.Count > 0)
                    {
                        invoiceCorrect = (ExplainInvoice)inv.RelatedInvoices[0].Clone();
                        invoiceCorrect.LastInvoiceChange = (ExplainInvoice)invoiceCorrect.Clone();
                    }
                    else
                    {
                        invoiceCorrect = (ExplainInvoice)inv.Clone();
                        invoiceCorrect.LastInvoiceChange = (ExplainInvoice)invoiceCorrect.Clone();
                    }
                    invoiceCorrect.Type = ExplainInvoiceType.Correct;
                    invoiceCorrect.SetState(ExplainInvoiceState.NotUsing);
                    invoiceCorrect.Parent = inv;
                    inv.RelatedInvoices.Clear();
                    inv.RelatedInvoices.Add(invoiceCorrect);
                    //---------------------------------------------------------

                    RequestStateAsync requestStateAsyncInvoice = GetRequestStateAsyncInvoices(inv.CHAPTER);
                    List<ExplainInvoice> list = (List<ExplainInvoice>)requestStateAsyncInvoice.ResultData;
                    list[operationParams.Row.ListIndex] = inv;
                    requestStateAsyncInvoice.ResultData = list;

                    View.ExecuteInUiThread(a =>
                    {
                        View.GetGridInvoices(inv.CHAPTER).UpdateData();
                        View.CheckEnabledAllRibbonButton();
                        operationParams.Row.ExpandAll();
                    });
                }
                if (operationParams.InvoiceStateId == ExplainInvoiceStateInThis.Processed)
                {
                    inv.AddState(ExplainInvoiceState.ProcessedInThisExplain);
                    //--- надо разрешить возможность редактировать
                    //--- или заблокировать возможность редактировать
                    View.ExecuteInUiThread(a =>
                    {
                        View.GridRowUpdate(inv.CHAPTER, operationParams.Row);
                        View.CheckEnabledAllRibbonButton();
                        operationParams.Row.Refresh();
                    });
                }

                ExistsInvoiceChangeOfExplain();
            });
        }

        public void DeleteInvoiceCorrectState(string invoiceId, UltraGridRow row, string commandName)
        {
            OperationUpdateParameters operationParams = new OperationUpdateParameters();
            operationParams.InvoiceId = invoiceId;
            operationParams.CommandName = commandName;
            operationParams.Row = row;

            DeleteInvoiceCorrectState(operationParams);
        }

        private void DeleteInvoiceCorrectState(OperationUpdateParameters operationParams)
        {
            operationParams.OperationCTS = ExecuteServiceCallAsync<OperationResult>(
            (c) =>
            {
                return explainReplyService.DeleteInvoiceCorrectState(_explainReplyInfo.EXPLAIN_ID, operationParams.InvoiceId);
            },
            (c, rez) =>
            {
                ExplainInvoice inv = (ExplainInvoice)operationParams.Row.ListObject;
                inv.RemoveState(ExplainInvoiceState.ProcessedInThisExplain);
                inv.RemoveState(ExplainInvoiceState.ChangingInThisExplain);

                //--- надо разрешить возможность редактировать
                //--- или заблокировать возможность редактировать
                View.ExecuteInUiThread(a =>
                {
                    View.GridRowUpdate(inv.CHAPTER, operationParams.Row);
                    operationParams.Row.Refresh();
                });

                ExistsInvoiceChangeOfExplain();
                LoadOneInvoice(operationParams.Row, operationParams.InvoiceId);
            });
        }

        public void DeleteAllInvoiceCorrectState(List<UltraGridRow> rows, string commandName)
        {
            OperationUpdateParameters operationParams = new OperationUpdateParameters();
            operationParams.CommandName = commandName;
            operationParams.Rows = rows;

            DeleteAllInvoiceCorrectState(operationParams);
        }

        private void DeleteAllInvoiceCorrectState(OperationUpdateParameters operationParams)
        {
            operationParams.OperationCTS = ExecuteServiceCallAsync<OperationResult>(
            (c) =>
            {
                return explainReplyService.DeleteAllInvoiceCorrectState(_explainReplyInfo.EXPLAIN_ID);
            },
            (c, rez) =>
            {
                foreach (UltraGridRow row in operationParams.Rows)
                {
                    ExplainInvoice inv = (ExplainInvoice)row.ListObject;
                    inv.RemoveState(ExplainInvoiceState.ProcessedInThisExplain);
                    inv.RemoveState(ExplainInvoiceState.ChangingInThisExplain);
                    row.Refresh();
                }

                int chapter = View.GetCurrentChapter();
                SetStateWorkerStart(chapter);

                View.ExecuteInUiThread(a =>
                {
                    if (chapter > -1)
                    {
                        View.GetCurrentGridInvoices().UpdateData();
                    }
                });
                ExistsInvoiceChangeOfExplain();
            });
        }

        public void ExistsInvoiceChangeOfExplain()
        {
            OperationUpdateParameters operationParams = new OperationUpdateParameters();
            operationParams.OperationCTS = ExecuteServiceCallAsync<OperationResult<bool>>(
            (c) =>
            {
                return explainReplyService.ExistsInvoiceChangeOfExplain(_explainReplyInfo.EXPLAIN_ID);
            },
            (c, rez) =>
            {
                View.ExecuteInUiThread(a =>
                {
                    View.CheckEnabledCancelAllButton(rez.Result);
                });
            });
        }

        public void LoadOneInvoice(UltraGridRow row, string invoiceId)
        {
            OperationUpdateParameters operationParams = new OperationUpdateParameters();
            operationParams.InvoiceId = invoiceId;
            operationParams.Row = row;

            operationParams.OperationCTS = ExecuteServiceCallAsync<OperationResult<ExplainInvoice>>(
            (c) =>
            {
                var result = _explainInvoiceService.GetOneExplainInvoice(_explainReplyInfo, _exlpainReplyCardData.Regim, operationParams.InvoiceId);
                result.Result.LastInvoiceChange = (ExplainInvoice)result.Result.Clone();
                return result;
            },
            (c, rez) =>
            {
                ExplainInvoice inv = rez.Result;
                inv.RefreshProperties();
                RequestStateAsync requestStateAsyncInvoice = GetRequestStateAsyncInvoices(inv.CHAPTER);
                List<ExplainInvoice> list = (List<ExplainInvoice>)requestStateAsyncInvoice.ResultData;
                list[operationParams.Row.ListIndex] = inv;
                requestStateAsyncInvoice.ResultData = list;

                View.ExecuteInUiThread(a =>
                {
                    View.GetGridInvoices(inv.CHAPTER).UpdateData();
                });
            });
        }

        #endregion
    }
}
