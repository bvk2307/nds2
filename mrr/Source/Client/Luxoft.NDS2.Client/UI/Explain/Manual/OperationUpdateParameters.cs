﻿using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard;
using dto = Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Luxoft.NDS2.Client.UI.Explain.Manual
{
    public class OperationUpdateParameters
    {
        public CancellationTokenSource OperationCTS { get; set; }
        public bool IsComplete { get; set; }
        public string InvoiceId { get; set; }
        public string FieldName { get; set; }
        public string FieldValue { get; set; }
        public long Id { get; set; }
        public dto.InvoiceCorrectTypeOperation TypeOperation { get; set; }
        public dto.ExplainInvoice ExplainInvoice { get; set; }
        public int ExplainStatusId { get; set; }
        public string CommandName { get; set; }
        public dto.ExplainInvoiceStateInThis InvoiceStateId { get; set; }
        public int Chapter { get; set; }
        public UltraGridRow Row { get; set; }
        public List<UltraGridRow> Rows { get; set; }
        public RequestStateAsync RequestState { get; set; }
    }
}
