﻿namespace Luxoft.NDS2.Client.UI.Explain.Manual
{
    partial class View
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab3 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab4 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab5 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab6 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            this.tabR8 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.gridR8 = new Luxoft.NDS2.Client.UI.Controls.Grid.V1.GridControl();
            this.tabR9 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.gridR9 = new Luxoft.NDS2.Client.UI.Controls.Grid.V1.GridControl();
            this.tabR10 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.gridR10 = new Luxoft.NDS2.Client.UI.Controls.Grid.V1.GridControl();
            this.tabR11 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.gridR11 = new Luxoft.NDS2.Client.UI.Controls.Grid.V1.GridControl();
            this.tabR12 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.gridR12 = new Luxoft.NDS2.Client.UI.Controls.Grid.V1.GridControl();
            this.tabHistory = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.tableLayoutHistoryStatusMain = new System.Windows.Forms.TableLayoutPanel();
            this.labelStatusNotProcessedCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelStatusNotProcessedValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelStatusSendToMCCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelStatusSendToMCValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelStatusProcessedInMCCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelStatusProcessedInMCValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelStatusSendToIniciatorCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelStatusSendToIniciatorValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelStatusRecieveIniciatorCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelStatusRecieveIniciatorValue = new Infragistics.Win.Misc.UltraLabel();
            this.tableLayoutMain = new System.Windows.Forms.TableLayoutPanel();
            this.labelTypeValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelNumberCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelIncomingDataValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelStatusCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelStatusValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelCorrectionNumberCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelCorrectionNumberValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelTaxPeriodCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelTaxPeriodValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelDocInfoNameValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelDocInfoNumCaption = new Infragistics.Win.Misc.UltraLabel();
            this.linkLabelDocInfoNumValue = new Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel();
            this.labelDocInfoDataValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelIncomingNumValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelSimvolNumber = new Infragistics.Win.Misc.UltraLabel();
            this.ueGroupBoxTaxPayerInfo = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExGroupBoxTaxPayerInfo = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.tableLayoutTaxPayer = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutTaxPayerSecond = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayout_INN_KPP = new System.Windows.Forms.TableLayoutPanel();
            this.labelKPPCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelKPPValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelInnCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelInnValue = new Infragistics.Win.Misc.UltraLabel();
            this.tableLayoutCommentMain = new System.Windows.Forms.TableLayoutPanel();
            this.teComment = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.tableLayoutCommentCaption = new System.Windows.Forms.TableLayoutPanel();
            this.labelCommentCaption = new Infragistics.Win.Misc.UltraLabel();
            this.tableLayoutTaxPayerName = new System.Windows.Forms.TableLayoutPanel();
            this.LinkLabelNameValue = new Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel();
            this._surIndicator = new Luxoft.NDS2.Client.UI.Controls.Sur.SurIcon();
            this.ultraTabControlMain = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.tabR8.SuspendLayout();
            this.tabR9.SuspendLayout();
            this.tabR10.SuspendLayout();
            this.tabR11.SuspendLayout();
            this.tabR12.SuspendLayout();
            this.tabHistory.SuspendLayout();
            this.tableLayoutHistoryStatusMain.SuspendLayout();
            this.tableLayoutMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ueGroupBoxTaxPayerInfo)).BeginInit();
            this.ueGroupBoxTaxPayerInfo.SuspendLayout();
            this.ultraExGroupBoxTaxPayerInfo.SuspendLayout();
            this.tableLayoutTaxPayer.SuspendLayout();
            this.tableLayoutTaxPayerSecond.SuspendLayout();
            this.tableLayout_INN_KPP.SuspendLayout();
            this.tableLayoutCommentMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teComment)).BeginInit();
            this.tableLayoutCommentCaption.SuspendLayout();
            this.tableLayoutTaxPayerName.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControlMain)).BeginInit();
            this.ultraTabControlMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabR8
            // 
            this.tabR8.Controls.Add(this.gridR8);
            this.tabR8.Location = new System.Drawing.Point(1, 23);
            this.tabR8.Name = "tabR8";
            this.tabR8.Size = new System.Drawing.Size(1264, 401);
            // 
            // gridR8
            // 
            this.gridR8.AddVirtualCheckColumn = false;
            this.gridR8.AggregatePanelVisible = true;
            this.gridR8.AllowMultiGrouping = true;
            this.gridR8.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.gridR8.AllowSaveFilterSettings = false;
            this.gridR8.BackColor = System.Drawing.Color.Transparent;
            this.gridR8.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.None;
            this.gridR8.DefaultPageSize = "200";
            this.gridR8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridR8.ExportExcelCancelVisible = false;
            this.gridR8.ExportExcelVisible = false;
            this.gridR8.FilterResetVisible = false;
            this.gridR8.FooterVisible = true;
            this.gridR8.GridContextMenuStrip = null;
            this.gridR8.Location = new System.Drawing.Point(0, 0);
            this.gridR8.Name = "gridR8";
            this.gridR8.PanelExportExcelStateVisible = false;
            this.gridR8.PanelLoadingVisible = true;
            this.gridR8.PanelPagesVisible = true;
            this.gridR8.RowDoubleClicked = null;
            this.gridR8.Setup = null;
            this.gridR8.Size = new System.Drawing.Size(1264, 401);
            this.gridR8.TabIndex = 0;
            this.gridR8.Title = "";
            this.gridR8.AfterSelectRow += new System.EventHandler(this.gridR8_AfterSelectRow);
            this.gridR8.AfterLoadData += new System.EventHandler(this.gridR8_AfterLoadData);
            this.gridR8.ChildBandBeforeRowExpanded += new System.EventHandler(this.gridR8_ChildBandBeforeRowExpanded);
            // 
            // tabR9
            // 
            this.tabR9.Controls.Add(this.gridR9);
            this.tabR9.Location = new System.Drawing.Point(-10000, -10000);
            this.tabR9.Name = "tabR9";
            this.tabR9.Size = new System.Drawing.Size(1264, 401);
            // 
            // gridR9
            // 
            this.gridR9.AddVirtualCheckColumn = false;
            this.gridR9.AggregatePanelVisible = true;
            this.gridR9.AllowMultiGrouping = true;
            this.gridR9.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.gridR9.AllowSaveFilterSettings = false;
            this.gridR9.BackColor = System.Drawing.Color.Transparent;
            this.gridR9.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.None;
            this.gridR9.DefaultPageSize = "";
            this.gridR9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridR9.ExportExcelCancelVisible = false;
            this.gridR9.ExportExcelVisible = false;
            this.gridR9.FilterResetVisible = false;
            this.gridR9.FooterVisible = true;
            this.gridR9.GridContextMenuStrip = null;
            this.gridR9.Location = new System.Drawing.Point(0, 0);
            this.gridR9.Name = "gridR9";
            this.gridR9.PanelExportExcelStateVisible = false;
            this.gridR9.PanelLoadingVisible = true;
            this.gridR9.PanelPagesVisible = true;
            this.gridR9.RowDoubleClicked = null;
            this.gridR9.Setup = null;
            this.gridR9.Size = new System.Drawing.Size(1264, 401);
            this.gridR9.TabIndex = 1;
            this.gridR9.Title = "";
            this.gridR9.AfterSelectRow += new System.EventHandler(this.gridR8_AfterSelectRow);
            this.gridR9.AfterLoadData += new System.EventHandler(this.gridR9_AfterLoadData);
            this.gridR9.ChildBandBeforeRowExpanded += new System.EventHandler(this.gridR8_ChildBandBeforeRowExpanded);
            // 
            // tabR10
            // 
            this.tabR10.Controls.Add(this.gridR10);
            this.tabR10.Location = new System.Drawing.Point(-10000, -10000);
            this.tabR10.Name = "tabR10";
            this.tabR10.Size = new System.Drawing.Size(1264, 401);
            // 
            // gridR10
            // 
            this.gridR10.AddVirtualCheckColumn = false;
            this.gridR10.AggregatePanelVisible = true;
            this.gridR10.AllowMultiGrouping = true;
            this.gridR10.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.gridR10.AllowSaveFilterSettings = false;
            this.gridR10.BackColor = System.Drawing.Color.Transparent;
            this.gridR10.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.None;
            this.gridR10.DefaultPageSize = "";
            this.gridR10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridR10.ExportExcelCancelVisible = false;
            this.gridR10.ExportExcelVisible = false;
            this.gridR10.FilterResetVisible = false;
            this.gridR10.FooterVisible = true;
            this.gridR10.GridContextMenuStrip = null;
            this.gridR10.Location = new System.Drawing.Point(0, 0);
            this.gridR10.Name = "gridR10";
            this.gridR10.PanelExportExcelStateVisible = false;
            this.gridR10.PanelLoadingVisible = true;
            this.gridR10.PanelPagesVisible = true;
            this.gridR10.RowDoubleClicked = null;
            this.gridR10.Setup = null;
            this.gridR10.Size = new System.Drawing.Size(1264, 401);
            this.gridR10.TabIndex = 2;
            this.gridR10.Title = "";
            this.gridR10.AfterSelectRow += new System.EventHandler(this.gridR8_AfterSelectRow);
            this.gridR10.AfterLoadData += new System.EventHandler(this.gridR10_AfterLoadData);
            this.gridR10.ChildBandBeforeRowExpanded += new System.EventHandler(this.gridR8_ChildBandBeforeRowExpanded);
            // 
            // tabR11
            // 
            this.tabR11.Controls.Add(this.gridR11);
            this.tabR11.Location = new System.Drawing.Point(-10000, -10000);
            this.tabR11.Name = "tabR11";
            this.tabR11.Size = new System.Drawing.Size(1264, 401);
            // 
            // gridR11
            // 
            this.gridR11.AddVirtualCheckColumn = false;
            this.gridR11.AggregatePanelVisible = true;
            this.gridR11.AllowMultiGrouping = true;
            this.gridR11.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.gridR11.AllowSaveFilterSettings = false;
            this.gridR11.BackColor = System.Drawing.Color.Transparent;
            this.gridR11.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.None;
            this.gridR11.DefaultPageSize = "";
            this.gridR11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridR11.ExportExcelCancelVisible = false;
            this.gridR11.ExportExcelVisible = false;
            this.gridR11.FilterResetVisible = false;
            this.gridR11.FooterVisible = true;
            this.gridR11.GridContextMenuStrip = null;
            this.gridR11.Location = new System.Drawing.Point(0, 0);
            this.gridR11.Name = "gridR11";
            this.gridR11.PanelExportExcelStateVisible = false;
            this.gridR11.PanelLoadingVisible = true;
            this.gridR11.PanelPagesVisible = true;
            this.gridR11.RowDoubleClicked = null;
            this.gridR11.Setup = null;
            this.gridR11.Size = new System.Drawing.Size(1264, 401);
            this.gridR11.TabIndex = 3;
            this.gridR11.Title = "";
            this.gridR11.AfterSelectRow += new System.EventHandler(this.gridR8_AfterSelectRow);
            this.gridR11.AfterLoadData += new System.EventHandler(this.gridR11_AfterLoadData);
            this.gridR11.ChildBandBeforeRowExpanded += new System.EventHandler(this.gridR8_ChildBandBeforeRowExpanded);
            // 
            // tabR12
            // 
            this.tabR12.Controls.Add(this.gridR12);
            this.tabR12.Location = new System.Drawing.Point(-10000, -10000);
            this.tabR12.Name = "tabR12";
            this.tabR12.Size = new System.Drawing.Size(1264, 401);
            // 
            // gridR12
            // 
            this.gridR12.AddVirtualCheckColumn = false;
            this.gridR12.AggregatePanelVisible = true;
            this.gridR12.AllowMultiGrouping = true;
            this.gridR12.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.gridR12.AllowSaveFilterSettings = false;
            this.gridR12.BackColor = System.Drawing.Color.Transparent;
            this.gridR12.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.None;
            this.gridR12.DefaultPageSize = "";
            this.gridR12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridR12.ExportExcelCancelVisible = false;
            this.gridR12.ExportExcelVisible = false;
            this.gridR12.FilterResetVisible = false;
            this.gridR12.FooterVisible = true;
            this.gridR12.GridContextMenuStrip = null;
            this.gridR12.Location = new System.Drawing.Point(0, 0);
            this.gridR12.Name = "gridR12";
            this.gridR12.PanelExportExcelStateVisible = false;
            this.gridR12.PanelLoadingVisible = true;
            this.gridR12.PanelPagesVisible = true;
            this.gridR12.RowDoubleClicked = null;
            this.gridR12.Setup = null;
            this.gridR12.Size = new System.Drawing.Size(1264, 401);
            this.gridR12.TabIndex = 4;
            this.gridR12.Title = "";
            this.gridR12.AfterSelectRow += new System.EventHandler(this.gridR8_AfterSelectRow);
            this.gridR12.AfterLoadData += new System.EventHandler(this.gridR12_AfterLoadData);
            this.gridR12.ChildBandBeforeRowExpanded += new System.EventHandler(this.gridR8_ChildBandBeforeRowExpanded);
            // 
            // tabHistory
            // 
            this.tabHistory.Controls.Add(this.tableLayoutHistoryStatusMain);
            this.tabHistory.Location = new System.Drawing.Point(-10000, -10000);
            this.tabHistory.Name = "tabHistory";
            this.tabHistory.Size = new System.Drawing.Size(1264, 401);
            // 
            // tableLayoutHistoryStatusMain
            // 
            this.tableLayoutHistoryStatusMain.ColumnCount = 6;
            this.tableLayoutHistoryStatusMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 130F));
            this.tableLayoutHistoryStatusMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutHistoryStatusMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 170F));
            this.tableLayoutHistoryStatusMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 300F));
            this.tableLayoutHistoryStatusMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutHistoryStatusMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 132F));
            this.tableLayoutHistoryStatusMain.Controls.Add(this.labelStatusNotProcessedCaption, 0, 0);
            this.tableLayoutHistoryStatusMain.Controls.Add(this.labelStatusNotProcessedValue, 1, 0);
            this.tableLayoutHistoryStatusMain.Controls.Add(this.labelStatusSendToMCCaption, 0, 1);
            this.tableLayoutHistoryStatusMain.Controls.Add(this.labelStatusSendToMCValue, 1, 1);
            this.tableLayoutHistoryStatusMain.Controls.Add(this.labelStatusProcessedInMCCaption, 0, 2);
            this.tableLayoutHistoryStatusMain.Controls.Add(this.labelStatusProcessedInMCValue, 1, 2);
            this.tableLayoutHistoryStatusMain.Controls.Add(this.labelStatusSendToIniciatorCaption, 3, 0);
            this.tableLayoutHistoryStatusMain.Controls.Add(this.labelStatusSendToIniciatorValue, 4, 0);
            this.tableLayoutHistoryStatusMain.Controls.Add(this.labelStatusRecieveIniciatorCaption, 3, 1);
            this.tableLayoutHistoryStatusMain.Controls.Add(this.labelStatusRecieveIniciatorValue, 4, 1);
            this.tableLayoutHistoryStatusMain.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tableLayoutHistoryStatusMain.Location = new System.Drawing.Point(3, 4);
            this.tableLayoutHistoryStatusMain.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutHistoryStatusMain.Name = "tableLayoutHistoryStatusMain";
            this.tableLayoutHistoryStatusMain.RowCount = 4;
            this.tableLayoutHistoryStatusMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutHistoryStatusMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutHistoryStatusMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutHistoryStatusMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutHistoryStatusMain.Size = new System.Drawing.Size(892, 77);
            this.tableLayoutHistoryStatusMain.TabIndex = 1;
            // 
            // labelStatusNotProcessedCaption
            // 
            appearance6.TextHAlignAsString = "Right";
            this.labelStatusNotProcessedCaption.Appearance = appearance6;
            this.labelStatusNotProcessedCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelStatusNotProcessedCaption.Location = new System.Drawing.Point(1, 1);
            this.labelStatusNotProcessedCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelStatusNotProcessedCaption.Name = "labelStatusNotProcessedCaption";
            this.labelStatusNotProcessedCaption.Size = new System.Drawing.Size(128, 23);
            this.labelStatusNotProcessedCaption.TabIndex = 0;
            this.labelStatusNotProcessedCaption.Text = "Необработано:";
            // 
            // labelStatusNotProcessedValue
            // 
            this.labelStatusNotProcessedValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelStatusNotProcessedValue.Location = new System.Drawing.Point(131, 1);
            this.labelStatusNotProcessedValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelStatusNotProcessedValue.Name = "labelStatusNotProcessedValue";
            this.labelStatusNotProcessedValue.Size = new System.Drawing.Size(78, 23);
            this.labelStatusNotProcessedValue.TabIndex = 1;
            this.labelStatusNotProcessedValue.Text = "10.05.2015";
            // 
            // labelStatusSendToMCCaption
            // 
            appearance7.TextHAlignAsString = "Right";
            this.labelStatusSendToMCCaption.Appearance = appearance7;
            this.labelStatusSendToMCCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelStatusSendToMCCaption.Location = new System.Drawing.Point(1, 26);
            this.labelStatusSendToMCCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelStatusSendToMCCaption.Name = "labelStatusSendToMCCaption";
            this.labelStatusSendToMCCaption.Size = new System.Drawing.Size(128, 23);
            this.labelStatusSendToMCCaption.TabIndex = 2;
            this.labelStatusSendToMCCaption.Text = "Отправлено в МС:";
            // 
            // labelStatusSendToMCValue
            // 
            this.labelStatusSendToMCValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelStatusSendToMCValue.Location = new System.Drawing.Point(131, 26);
            this.labelStatusSendToMCValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelStatusSendToMCValue.Name = "labelStatusSendToMCValue";
            this.labelStatusSendToMCValue.Size = new System.Drawing.Size(78, 23);
            this.labelStatusSendToMCValue.TabIndex = 3;
            this.labelStatusSendToMCValue.Text = "10.05.2015";
            // 
            // labelStatusProcessedInMCCaption
            // 
            appearance10.TextHAlignAsString = "Right";
            this.labelStatusProcessedInMCCaption.Appearance = appearance10;
            this.labelStatusProcessedInMCCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelStatusProcessedInMCCaption.Location = new System.Drawing.Point(1, 51);
            this.labelStatusProcessedInMCCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelStatusProcessedInMCCaption.Name = "labelStatusProcessedInMCCaption";
            this.labelStatusProcessedInMCCaption.Size = new System.Drawing.Size(128, 23);
            this.labelStatusProcessedInMCCaption.TabIndex = 4;
            this.labelStatusProcessedInMCCaption.Text = "Обработано в МС:";
            // 
            // labelStatusProcessedInMCValue
            // 
            this.labelStatusProcessedInMCValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelStatusProcessedInMCValue.Location = new System.Drawing.Point(131, 51);
            this.labelStatusProcessedInMCValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelStatusProcessedInMCValue.Name = "labelStatusProcessedInMCValue";
            this.labelStatusProcessedInMCValue.Size = new System.Drawing.Size(78, 23);
            this.labelStatusProcessedInMCValue.TabIndex = 5;
            this.labelStatusProcessedInMCValue.Text = "10.05.2015";
            // 
            // labelStatusSendToIniciatorCaption
            // 
            appearance8.TextHAlignAsString = "Right";
            this.labelStatusSendToIniciatorCaption.Appearance = appearance8;
            this.labelStatusSendToIniciatorCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelStatusSendToIniciatorCaption.Location = new System.Drawing.Point(381, 1);
            this.labelStatusSendToIniciatorCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelStatusSendToIniciatorCaption.Name = "labelStatusSendToIniciatorCaption";
            this.labelStatusSendToIniciatorCaption.Size = new System.Drawing.Size(298, 23);
            this.labelStatusSendToIniciatorCaption.TabIndex = 14;
            this.labelStatusSendToIniciatorCaption.Text = "Отправлен ответ из НО-исполнителя в НО-инициатор:";
            // 
            // labelStatusSendToIniciatorValue
            // 
            this.labelStatusSendToIniciatorValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelStatusSendToIniciatorValue.Location = new System.Drawing.Point(681, 1);
            this.labelStatusSendToIniciatorValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelStatusSendToIniciatorValue.Name = "labelStatusSendToIniciatorValue";
            this.labelStatusSendToIniciatorValue.Size = new System.Drawing.Size(78, 23);
            this.labelStatusSendToIniciatorValue.TabIndex = 15;
            this.labelStatusSendToIniciatorValue.Text = "21.05.2015";
            // 
            // labelStatusRecieveIniciatorCaption
            // 
            appearance9.TextHAlignAsString = "Right";
            this.labelStatusRecieveIniciatorCaption.Appearance = appearance9;
            this.labelStatusRecieveIniciatorCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelStatusRecieveIniciatorCaption.Location = new System.Drawing.Point(381, 26);
            this.labelStatusRecieveIniciatorCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelStatusRecieveIniciatorCaption.Name = "labelStatusRecieveIniciatorCaption";
            this.labelStatusRecieveIniciatorCaption.Size = new System.Drawing.Size(298, 23);
            this.labelStatusRecieveIniciatorCaption.TabIndex = 16;
            this.labelStatusRecieveIniciatorCaption.Text = "Получен ответ НО-инициатором:";
            // 
            // labelStatusRecieveIniciatorValue
            // 
            this.labelStatusRecieveIniciatorValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelStatusRecieveIniciatorValue.Location = new System.Drawing.Point(681, 26);
            this.labelStatusRecieveIniciatorValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelStatusRecieveIniciatorValue.Name = "labelStatusRecieveIniciatorValue";
            this.labelStatusRecieveIniciatorValue.Size = new System.Drawing.Size(78, 23);
            this.labelStatusRecieveIniciatorValue.TabIndex = 17;
            this.labelStatusRecieveIniciatorValue.Text = "28.05.2015";
            // 
            // tableLayoutMain
            // 
            this.tableLayoutMain.ColumnCount = 16;
            this.tableLayoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 115F));
            this.tableLayoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 17F));
            this.tableLayoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 87F));
            this.tableLayoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 190F));
            this.tableLayoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 115F));
            this.tableLayoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.tableLayoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 77F));
            this.tableLayoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 121F));
            this.tableLayoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 105F));
            this.tableLayoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 105F));
            this.tableLayoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 161F));
            this.tableLayoutMain.Controls.Add(this.labelTypeValue, 0, 0);
            this.tableLayoutMain.Controls.Add(this.labelNumberCaption, 1, 0);
            this.tableLayoutMain.Controls.Add(this.labelIncomingDataValue, 4, 0);
            this.tableLayoutMain.Controls.Add(this.labelStatusCaption, 5, 0);
            this.tableLayoutMain.Controls.Add(this.labelStatusValue, 6, 0);
            this.tableLayoutMain.Controls.Add(this.labelCorrectionNumberCaption, 11, 0);
            this.tableLayoutMain.Controls.Add(this.labelCorrectionNumberValue, 12, 0);
            this.tableLayoutMain.Controls.Add(this.labelTaxPeriodCaption, 13, 0);
            this.tableLayoutMain.Controls.Add(this.labelTaxPeriodValue, 14, 0);
            this.tableLayoutMain.Controls.Add(this.labelDocInfoNameValue, 7, 0);
            this.tableLayoutMain.Controls.Add(this.labelDocInfoNumCaption, 8, 0);
            this.tableLayoutMain.Controls.Add(this.linkLabelDocInfoNumValue, 9, 0);
            this.tableLayoutMain.Controls.Add(this.labelDocInfoDataValue, 10, 0);
            this.tableLayoutMain.Controls.Add(this.labelIncomingNumValue, 2, 0);
            this.tableLayoutMain.Controls.Add(this.labelSimvolNumber, 3, 0);
            this.tableLayoutMain.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutMain.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutMain.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutMain.Name = "tableLayoutMain";
            this.tableLayoutMain.RowCount = 2;
            this.tableLayoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tableLayoutMain.Size = new System.Drawing.Size(1268, 25);
            this.tableLayoutMain.TabIndex = 1;
            // 
            // labelTypeValue
            // 
            this.labelTypeValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelTypeValue.Location = new System.Drawing.Point(1, 1);
            this.labelTypeValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelTypeValue.Name = "labelTypeValue";
            this.labelTypeValue.Size = new System.Drawing.Size(113, 23);
            this.labelTypeValue.TabIndex = 0;
            this.labelTypeValue.Text = "Пояснение (по ТКС)";
            // 
            // labelNumberCaption
            // 
            appearance14.BackColor = System.Drawing.Color.Transparent;
            this.labelNumberCaption.Appearance = appearance14;
            this.labelNumberCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelNumberCaption.Location = new System.Drawing.Point(116, 1);
            this.labelNumberCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelNumberCaption.Name = "labelNumberCaption";
            this.labelNumberCaption.Size = new System.Drawing.Size(15, 23);
            this.labelNumberCaption.TabIndex = 2;
            this.labelNumberCaption.Text = "№";
            // 
            // labelIncomingDataValue
            // 
            this.labelIncomingDataValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelIncomingDataValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelIncomingDataValue.Location = new System.Drawing.Point(193, 1);
            this.labelIncomingDataValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelIncomingDataValue.Name = "labelIncomingDataValue";
            this.labelIncomingDataValue.Size = new System.Drawing.Size(85, 23);
            this.labelIncomingDataValue.TabIndex = 3;
            this.labelIncomingDataValue.Text = "29.04.2015";
            // 
            // labelStatusCaption
            // 
            this.labelStatusCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelStatusCaption.Location = new System.Drawing.Point(280, 1);
            this.labelStatusCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelStatusCaption.Name = "labelStatusCaption";
            this.labelStatusCaption.Size = new System.Drawing.Size(43, 23);
            this.labelStatusCaption.TabIndex = 4;
            this.labelStatusCaption.Text = "Статус:";
            // 
            // labelStatusValue
            // 
            this.labelStatusValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelStatusValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelStatusValue.Location = new System.Drawing.Point(325, 1);
            this.labelStatusValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelStatusValue.Name = "labelStatusValue";
            this.labelStatusValue.Size = new System.Drawing.Size(188, 23);
            this.labelStatusValue.TabIndex = 5;
            this.labelStatusValue.Text = "Отправлено в МС";
            // 
            // labelCorrectionNumberCaption
            // 
            this.labelCorrectionNumberCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCorrectionNumberCaption.Location = new System.Drawing.Point(820, 1);
            this.labelCorrectionNumberCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelCorrectionNumberCaption.Name = "labelCorrectionNumberCaption";
            this.labelCorrectionNumberCaption.Size = new System.Drawing.Size(75, 23);
            this.labelCorrectionNumberCaption.TabIndex = 6;
            this.labelCorrectionNumberCaption.Text = "Номер корр.:";
            // 
            // labelCorrectionNumberValue
            // 
            this.labelCorrectionNumberValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCorrectionNumberValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelCorrectionNumberValue.Location = new System.Drawing.Point(897, 1);
            this.labelCorrectionNumberValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelCorrectionNumberValue.Name = "labelCorrectionNumberValue";
            this.labelCorrectionNumberValue.Size = new System.Drawing.Size(119, 23);
            this.labelCorrectionNumberValue.TabIndex = 7;
            this.labelCorrectionNumberValue.Text = "999 (123456789098)";
            // 
            // labelTaxPeriodCaption
            // 
            this.labelTaxPeriodCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelTaxPeriodCaption.Location = new System.Drawing.Point(1018, 1);
            this.labelTaxPeriodCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelTaxPeriodCaption.Name = "labelTaxPeriodCaption";
            this.labelTaxPeriodCaption.Size = new System.Drawing.Size(103, 23);
            this.labelTaxPeriodCaption.TabIndex = 8;
            this.labelTaxPeriodCaption.Text = "Отчетный период:";
            // 
            // labelTaxPeriodValue
            // 
            this.labelTaxPeriodValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelTaxPeriodValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelTaxPeriodValue.Location = new System.Drawing.Point(1123, 1);
            this.labelTaxPeriodValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelTaxPeriodValue.Name = "labelTaxPeriodValue";
            this.labelTaxPeriodValue.Size = new System.Drawing.Size(103, 23);
            this.labelTaxPeriodValue.TabIndex = 9;
            this.labelTaxPeriodValue.Text = "сентябрь 2014";
            // 
            // labelDocInfoNameValue
            // 
            this.labelDocInfoNameValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDocInfoNameValue.Location = new System.Drawing.Point(515, 1);
            this.labelDocInfoNameValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelDocInfoNameValue.Name = "labelDocInfoNameValue";
            this.labelDocInfoNameValue.Size = new System.Drawing.Size(113, 23);
            this.labelDocInfoNameValue.TabIndex = 10;
            this.labelDocInfoNameValue.Text = "к автоистребованию";
            // 
            // labelDocInfoNumCaption
            // 
            this.labelDocInfoNumCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDocInfoNumCaption.Location = new System.Drawing.Point(630, 1);
            this.labelDocInfoNumCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelDocInfoNumCaption.Name = "labelDocInfoNumCaption";
            this.labelDocInfoNumCaption.Size = new System.Drawing.Size(18, 23);
            this.labelDocInfoNumCaption.TabIndex = 11;
            this.labelDocInfoNumCaption.Text = "№";
            // 
            // linkLabelDocInfoNumValue
            // 
            appearance11.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance11.TextVAlignAsString = "Top";
            this.linkLabelDocInfoNumValue.Appearance = appearance11;
            this.linkLabelDocInfoNumValue.Dock = System.Windows.Forms.DockStyle.Top;
            appearance12.TextVAlignAsString = "Top";
            this.linkLabelDocInfoNumValue.HotTrackLinkAppearance = appearance12;
            appearance13.TextVAlignAsString = "Top";
            this.linkLabelDocInfoNumValue.LinkAppearance = appearance13;
            this.linkLabelDocInfoNumValue.Location = new System.Drawing.Point(650, 0);
            this.linkLabelDocInfoNumValue.Margin = new System.Windows.Forms.Padding(1, 0, 1, 3);
            this.linkLabelDocInfoNumValue.Name = "linkLabelDocInfoNumValue";
            this.linkLabelDocInfoNumValue.Size = new System.Drawing.Size(78, 18);
            this.linkLabelDocInfoNumValue.TabIndex = 12;
            this.linkLabelDocInfoNumValue.TabStop = true;
            this.linkLabelDocInfoNumValue.Value = "<a href=\'#\'>12</a>";
            this.linkLabelDocInfoNumValue.LinkClicked += new Infragistics.Win.FormattedLinkLabel.LinkClickedEventHandler(this.linkLabelDocInfoNumValue_LinkClicked);
            // 
            // labelDocInfoDataValue
            // 
            this.labelDocInfoDataValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDocInfoDataValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelDocInfoDataValue.Location = new System.Drawing.Point(730, 1);
            this.labelDocInfoDataValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelDocInfoDataValue.Name = "labelDocInfoDataValue";
            this.labelDocInfoDataValue.Size = new System.Drawing.Size(88, 23);
            this.labelDocInfoDataValue.TabIndex = 13;
            this.labelDocInfoDataValue.Text = "от 23.01.2015";
            // 
            // labelIncomingNumValue
            // 
            this.labelIncomingNumValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelIncomingNumValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelIncomingNumValue.Location = new System.Drawing.Point(133, 1);
            this.labelIncomingNumValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelIncomingNumValue.Name = "labelIncomingNumValue";
            this.labelIncomingNumValue.Size = new System.Drawing.Size(38, 23);
            this.labelIncomingNumValue.TabIndex = 14;
            this.labelIncomingNumValue.Text = "12345";
            // 
            // labelSimvolNumber
            // 
            this.labelSimvolNumber.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSimvolNumber.Location = new System.Drawing.Point(173, 1);
            this.labelSimvolNumber.Margin = new System.Windows.Forms.Padding(1);
            this.labelSimvolNumber.Name = "labelSimvolNumber";
            this.labelSimvolNumber.Size = new System.Drawing.Size(18, 23);
            this.labelSimvolNumber.TabIndex = 15;
            this.labelSimvolNumber.Text = "от";
            // 
            // ueGroupBoxTaxPayerInfo
            // 
            this.ueGroupBoxTaxPayerInfo.Controls.Add(this.ultraExGroupBoxTaxPayerInfo);
            this.ueGroupBoxTaxPayerInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.ueGroupBoxTaxPayerInfo.ExpandedSize = new System.Drawing.Size(1268, 98);
            this.ueGroupBoxTaxPayerInfo.Location = new System.Drawing.Point(0, 25);
            this.ueGroupBoxTaxPayerInfo.Name = "ueGroupBoxTaxPayerInfo";
            this.ueGroupBoxTaxPayerInfo.Size = new System.Drawing.Size(1268, 98);
            this.ueGroupBoxTaxPayerInfo.TabIndex = 2;
            this.ueGroupBoxTaxPayerInfo.Text = "Данные налогоплательщика";
            // 
            // ultraExGroupBoxTaxPayerInfo
            // 
            this.ultraExGroupBoxTaxPayerInfo.Controls.Add(this.tableLayoutTaxPayer);
            this.ultraExGroupBoxTaxPayerInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExGroupBoxTaxPayerInfo.Location = new System.Drawing.Point(3, 19);
            this.ultraExGroupBoxTaxPayerInfo.Name = "ultraExGroupBoxTaxPayerInfo";
            this.ultraExGroupBoxTaxPayerInfo.Size = new System.Drawing.Size(1262, 76);
            this.ultraExGroupBoxTaxPayerInfo.TabIndex = 0;
            // 
            // tableLayoutTaxPayer
            // 
            this.tableLayoutTaxPayer.ColumnCount = 1;
            this.tableLayoutTaxPayer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutTaxPayer.Controls.Add(this.tableLayoutTaxPayerSecond, 0, 1);
            this.tableLayoutTaxPayer.Controls.Add(this.tableLayoutTaxPayerName, 0, 0);
            this.tableLayoutTaxPayer.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutTaxPayer.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutTaxPayer.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutTaxPayer.Name = "tableLayoutTaxPayer";
            this.tableLayoutTaxPayer.RowCount = 3;
            this.tableLayoutTaxPayer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutTaxPayer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutTaxPayer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutTaxPayer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutTaxPayer.Size = new System.Drawing.Size(1262, 76);
            this.tableLayoutTaxPayer.TabIndex = 3;
            // 
            // tableLayoutTaxPayerSecond
            // 
            this.tableLayoutTaxPayerSecond.ColumnCount = 3;
            this.tableLayoutTaxPayerSecond.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutTaxPayerSecond.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 700F));
            this.tableLayoutTaxPayerSecond.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 412F));
            this.tableLayoutTaxPayerSecond.Controls.Add(this.tableLayout_INN_KPP, 0, 0);
            this.tableLayoutTaxPayerSecond.Controls.Add(this.tableLayoutCommentMain, 1, 0);
            this.tableLayoutTaxPayerSecond.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutTaxPayerSecond.Location = new System.Drawing.Point(0, 25);
            this.tableLayoutTaxPayerSecond.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutTaxPayerSecond.Name = "tableLayoutTaxPayerSecond";
            this.tableLayoutTaxPayerSecond.RowCount = 1;
            this.tableLayoutTaxPayerSecond.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutTaxPayerSecond.Size = new System.Drawing.Size(1262, 50);
            this.tableLayoutTaxPayerSecond.TabIndex = 0;
            // 
            // tableLayout_INN_KPP
            // 
            this.tableLayout_INN_KPP.ColumnCount = 2;
            this.tableLayout_INN_KPP.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayout_INN_KPP.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayout_INN_KPP.Controls.Add(this.labelKPPCaption, 0, 1);
            this.tableLayout_INN_KPP.Controls.Add(this.labelKPPValue, 1, 1);
            this.tableLayout_INN_KPP.Controls.Add(this.labelInnCaption, 0, 0);
            this.tableLayout_INN_KPP.Controls.Add(this.labelInnValue, 1, 0);
            this.tableLayout_INN_KPP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayout_INN_KPP.Location = new System.Drawing.Point(0, 0);
            this.tableLayout_INN_KPP.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayout_INN_KPP.Name = "tableLayout_INN_KPP";
            this.tableLayout_INN_KPP.RowCount = 2;
            this.tableLayout_INN_KPP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayout_INN_KPP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayout_INN_KPP.Size = new System.Drawing.Size(150, 50);
            this.tableLayout_INN_KPP.TabIndex = 0;
            // 
            // labelKPPCaption
            // 
            appearance3.TextHAlignAsString = "Right";
            this.labelKPPCaption.Appearance = appearance3;
            this.labelKPPCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelKPPCaption.Location = new System.Drawing.Point(1, 26);
            this.labelKPPCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelKPPCaption.Name = "labelKPPCaption";
            this.labelKPPCaption.Size = new System.Drawing.Size(38, 23);
            this.labelKPPCaption.TabIndex = 0;
            this.labelKPPCaption.Text = "КПП:";
            // 
            // labelKPPValue
            // 
            this.labelKPPValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelKPPValue.Location = new System.Drawing.Point(41, 26);
            this.labelKPPValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelKPPValue.Name = "labelKPPValue";
            this.labelKPPValue.Size = new System.Drawing.Size(118, 23);
            this.labelKPPValue.TabIndex = 1;
            this.labelKPPValue.Text = "123456789";
            // 
            // labelInnCaption
            // 
            appearance1.TextHAlignAsString = "Right";
            this.labelInnCaption.Appearance = appearance1;
            this.labelInnCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInnCaption.Location = new System.Drawing.Point(1, 1);
            this.labelInnCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelInnCaption.Name = "labelInnCaption";
            this.labelInnCaption.Size = new System.Drawing.Size(38, 23);
            this.labelInnCaption.TabIndex = 0;
            this.labelInnCaption.Text = "ИНН:";
            // 
            // labelInnValue
            // 
            this.labelInnValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInnValue.Location = new System.Drawing.Point(41, 1);
            this.labelInnValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelInnValue.Name = "labelInnValue";
            this.labelInnValue.Size = new System.Drawing.Size(118, 23);
            this.labelInnValue.TabIndex = 1;
            this.labelInnValue.Text = "1234567893";
            // 
            // tableLayoutCommentMain
            // 
            this.tableLayoutCommentMain.ColumnCount = 2;
            this.tableLayoutCommentMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutCommentMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutCommentMain.Controls.Add(this.teComment, 1, 0);
            this.tableLayoutCommentMain.Controls.Add(this.tableLayoutCommentCaption, 0, 0);
            this.tableLayoutCommentMain.Location = new System.Drawing.Point(150, 0);
            this.tableLayoutCommentMain.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutCommentMain.Name = "tableLayoutCommentMain";
            this.tableLayoutCommentMain.RowCount = 1;
            this.tableLayoutCommentMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutCommentMain.Size = new System.Drawing.Size(700, 50);
            this.tableLayoutCommentMain.TabIndex = 3;
            // 
            // teComment
            // 
            this.teComment.AlwaysInEditMode = true;
            this.teComment.Location = new System.Drawing.Point(101, 1);
            this.teComment.Margin = new System.Windows.Forms.Padding(1);
            this.teComment.Multiline = true;
            this.teComment.Name = "teComment";
            this.teComment.Scrollbars = System.Windows.Forms.ScrollBars.Vertical;
            this.teComment.Size = new System.Drawing.Size(568, 48);
            this.teComment.TabIndex = 0;
            // 
            // tableLayoutCommentCaption
            // 
            this.tableLayoutCommentCaption.ColumnCount = 1;
            this.tableLayoutCommentCaption.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutCommentCaption.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutCommentCaption.Controls.Add(this.labelCommentCaption, 0, 0);
            this.tableLayoutCommentCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutCommentCaption.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutCommentCaption.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutCommentCaption.Name = "tableLayoutCommentCaption";
            this.tableLayoutCommentCaption.RowCount = 2;
            this.tableLayoutCommentCaption.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutCommentCaption.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutCommentCaption.Size = new System.Drawing.Size(100, 50);
            this.tableLayoutCommentCaption.TabIndex = 1;
            // 
            // labelCommentCaption
            // 
            appearance4.TextHAlignAsString = "Right";
            this.labelCommentCaption.Appearance = appearance4;
            this.labelCommentCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCommentCaption.Location = new System.Drawing.Point(1, 1);
            this.labelCommentCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelCommentCaption.Name = "labelCommentCaption";
            this.labelCommentCaption.Size = new System.Drawing.Size(98, 23);
            this.labelCommentCaption.TabIndex = 0;
            this.labelCommentCaption.Text = "Комментарий:";
            // 
            // tableLayoutTaxPayerName
            // 
            this.tableLayoutTaxPayerName.ColumnCount = 3;
            this.tableLayoutTaxPayerName.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutTaxPayerName.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 950F));
            this.tableLayoutTaxPayerName.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutTaxPayerName.Controls.Add(this.LinkLabelNameValue, 1, 0);
            this.tableLayoutTaxPayerName.Controls.Add(this._surIndicator, 0, 0);
            this.tableLayoutTaxPayerName.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutTaxPayerName.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutTaxPayerName.Name = "tableLayoutTaxPayerName";
            this.tableLayoutTaxPayerName.RowCount = 1;
            this.tableLayoutTaxPayerName.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutTaxPayerName.Size = new System.Drawing.Size(850, 25);
            this.tableLayoutTaxPayerName.TabIndex = 0;
            // 
            // LinkLabelNameValue
            // 
            appearance5.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this.LinkLabelNameValue.Appearance = appearance5;
            this.LinkLabelNameValue.Location = new System.Drawing.Point(31, 1);
            this.LinkLabelNameValue.Margin = new System.Windows.Forms.Padding(1);
            this.LinkLabelNameValue.Name = "LinkLabelNameValue";
            this.LinkLabelNameValue.Size = new System.Drawing.Size(498, 23);
            this.LinkLabelNameValue.TabIndex = 3;
            this.LinkLabelNameValue.TabStop = true;
            this.LinkLabelNameValue.Value = "<a href=\"#\">Наименование налогоплательщика с геперлинком</a>";
            this.LinkLabelNameValue.LinkClicked += new Infragistics.Win.FormattedLinkLabel.LinkClickedEventHandler(this.LinkLabelNameValue_LinkClicked);
            // 
            // _surIndicator
            // 
            this._surIndicator.BackColor = System.Drawing.Color.Transparent;
            this._surIndicator.Code = null;
            this._surIndicator.Location = new System.Drawing.Point(3, 3);
            this._surIndicator.MaximumSize = new System.Drawing.Size(20, 20);
            this._surIndicator.MinimumSize = new System.Drawing.Size(20, 20);
            this._surIndicator.Name = "_surIndicator";
            this._surIndicator.Size = new System.Drawing.Size(20, 20);
            this._surIndicator.TabIndex = 4;
            // 
            // ultraTabControlMain
            // 
            this.ultraTabControlMain.Controls.Add(this.ultraTabSharedControlsPage1);
            this.ultraTabControlMain.Controls.Add(this.tabR8);
            this.ultraTabControlMain.Controls.Add(this.tabHistory);
            this.ultraTabControlMain.Controls.Add(this.tabR9);
            this.ultraTabControlMain.Controls.Add(this.tabR10);
            this.ultraTabControlMain.Controls.Add(this.tabR11);
            this.ultraTabControlMain.Controls.Add(this.tabR12);
            this.ultraTabControlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraTabControlMain.Location = new System.Drawing.Point(0, 123);
            this.ultraTabControlMain.Name = "ultraTabControlMain";
            this.ultraTabControlMain.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.ultraTabControlMain.Size = new System.Drawing.Size(1268, 427);
            this.ultraTabControlMain.TabIndex = 3;
            ultraTab1.Key = "tabChapter8";
            ultraTab1.TabPage = this.tabR8;
            ultraTab1.Text = "Раздел 8";
            ultraTab3.Key = "tabChapter9";
            ultraTab3.TabPage = this.tabR9;
            ultraTab3.Text = "Раздел 9";
            ultraTab4.Key = "tabChapter10";
            ultraTab4.TabPage = this.tabR10;
            ultraTab4.Text = "Раздел 10";
            ultraTab5.Key = "tabChapter11";
            ultraTab5.TabPage = this.tabR11;
            ultraTab5.Text = "Раздел 11";
            ultraTab6.Key = "tabChapter12";
            ultraTab6.TabPage = this.tabR12;
            ultraTab6.Text = "Раздел 12";
            ultraTab2.Key = "tabHistoryStatus";
            ultraTab2.TabPage = this.tabHistory;
            ultraTab2.Text = "История";
            this.ultraTabControlMain.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab3,
            ultraTab4,
            ultraTab5,
            ultraTab6,
            ultraTab2});
            this.ultraTabControlMain.SelectedTabChanged += new Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventHandler(this.ultraTabControlMain_SelectedTabChanged);
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(1264, 401);
            // 
            // View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.ultraTabControlMain);
            this.Controls.Add(this.ueGroupBoxTaxPayerInfo);
            this.Controls.Add(this.tableLayoutMain);
            this.Name = "View";
            this.Size = new System.Drawing.Size(1268, 550);
            this.OnFormClosing += new System.EventHandler(this.View_OnFormClosing);
            this.tabR8.ResumeLayout(false);
            this.tabR9.ResumeLayout(false);
            this.tabR10.ResumeLayout(false);
            this.tabR11.ResumeLayout(false);
            this.tabR12.ResumeLayout(false);
            this.tabHistory.ResumeLayout(false);
            this.tableLayoutHistoryStatusMain.ResumeLayout(false);
            this.tableLayoutMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ueGroupBoxTaxPayerInfo)).EndInit();
            this.ueGroupBoxTaxPayerInfo.ResumeLayout(false);
            this.ultraExGroupBoxTaxPayerInfo.ResumeLayout(false);
            this.tableLayoutTaxPayer.ResumeLayout(false);
            this.tableLayoutTaxPayerSecond.ResumeLayout(false);
            this.tableLayout_INN_KPP.ResumeLayout(false);
            this.tableLayoutCommentMain.ResumeLayout(false);
            this.tableLayoutCommentMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teComment)).EndInit();
            this.tableLayoutCommentCaption.ResumeLayout(false);
            this.tableLayoutTaxPayerName.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraTabControlMain)).EndInit();
            this.ultraTabControlMain.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutMain;
        private Infragistics.Win.Misc.UltraLabel labelTypeValue;
        private Infragistics.Win.Misc.UltraLabel labelNumberCaption;
        private Infragistics.Win.Misc.UltraLabel labelIncomingDataValue;
        private Infragistics.Win.Misc.UltraLabel labelStatusCaption;
        private Infragistics.Win.Misc.UltraLabel labelStatusValue;
        private Infragistics.Win.Misc.UltraLabel labelCorrectionNumberCaption;
        private Infragistics.Win.Misc.UltraLabel labelCorrectionNumberValue;
        private Infragistics.Win.Misc.UltraLabel labelTaxPeriodCaption;
        private Infragistics.Win.Misc.UltraLabel labelTaxPeriodValue;
        private Infragistics.Win.Misc.UltraLabel labelDocInfoNameValue;
        private Infragistics.Win.Misc.UltraLabel labelDocInfoNumCaption;
        private Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel linkLabelDocInfoNumValue;
        private Infragistics.Win.Misc.UltraLabel labelDocInfoDataValue;
        private Infragistics.Win.Misc.UltraLabel labelIncomingNumValue;
        private Infragistics.Win.Misc.UltraLabel labelSimvolNumber;
        private Infragistics.Win.Misc.UltraExpandableGroupBox ueGroupBoxTaxPayerInfo;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExGroupBoxTaxPayerInfo;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl ultraTabControlMain;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl tabR8;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl tabHistory;
        private System.Windows.Forms.TableLayoutPanel tableLayoutHistoryStatusMain;
        private Infragistics.Win.Misc.UltraLabel labelStatusNotProcessedCaption;
        private Infragistics.Win.Misc.UltraLabel labelStatusNotProcessedValue;
        private Infragistics.Win.Misc.UltraLabel labelStatusSendToMCCaption;
        private Infragistics.Win.Misc.UltraLabel labelStatusSendToMCValue;
        private Infragistics.Win.Misc.UltraLabel labelStatusProcessedInMCCaption;
        private Infragistics.Win.Misc.UltraLabel labelStatusProcessedInMCValue;
        private Infragistics.Win.Misc.UltraLabel labelStatusSendToIniciatorCaption;
        private Infragistics.Win.Misc.UltraLabel labelStatusSendToIniciatorValue;
        private Infragistics.Win.Misc.UltraLabel labelStatusRecieveIniciatorCaption;
        private Infragistics.Win.Misc.UltraLabel labelStatusRecieveIniciatorValue;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl tabR9;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl tabR10;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl tabR11;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl tabR12;
        private Controls.Grid.V1.GridControl gridR8;
        private Controls.Grid.V1.GridControl gridR9;
        private Controls.Grid.V1.GridControl gridR10;
        private Controls.Grid.V1.GridControl gridR11;
        private Controls.Grid.V1.GridControl gridR12;
        private System.Windows.Forms.TableLayoutPanel tableLayoutTaxPayerName;
        private Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel LinkLabelNameValue;
        private Infragistics.Win.Misc.UltraLabel labelInnCaption;
        private Infragistics.Win.Misc.UltraLabel labelInnValue;
        private Infragistics.Win.Misc.UltraLabel labelKPPCaption;
        private Infragistics.Win.Misc.UltraLabel labelKPPValue;
        private System.Windows.Forms.TableLayoutPanel tableLayoutCommentMain;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor teComment;
        private System.Windows.Forms.TableLayoutPanel tableLayoutCommentCaption;
        private Infragistics.Win.Misc.UltraLabel labelCommentCaption;
        private System.Windows.Forms.TableLayoutPanel tableLayoutTaxPayer;
        private System.Windows.Forms.TableLayoutPanel tableLayoutTaxPayerSecond;
        private System.Windows.Forms.TableLayoutPanel tableLayout_INN_KPP;
        private Controls.Sur.SurIcon _surIndicator;
    }
}
