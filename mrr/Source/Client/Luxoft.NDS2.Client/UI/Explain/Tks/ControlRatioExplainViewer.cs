﻿using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Base.Navigation;
using Luxoft.NDS2.Client.UI.Controls.Sur;
using Luxoft.NDS2.Client.UI.Explain.Tks.ControlRatio;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using Luxoft.NDS2.Common.Contracts.Services;
using list = Luxoft.NDS2.Common.Models.KnpDocuments;
using Microsoft.Practices.CompositeUI;

namespace Luxoft.NDS2.Client.UI.Explain.Tks
{
    public class ControlRatioExplainViewer : ExplainDetailsViewerBase<ControlRatioExplainView, ControlRatioExplainModel, list.ControlRatioExplainModel>
    {
        public ControlRatioExplainViewer(
            WorkItem workItem,
            DictionarySur sur,
            IClaimCardOpener claimCardOpener,
            ITaxPayerDetails taxPayerOpener)
            : base(workItem, sur, claimCardOpener, taxPayerOpener, ExplainType.ControlRatio)
        {
        }

        protected override string GetWindowTitle(string inn)
        {
            return string.Format(ResourceManagerNDS2.ExplainMessages.ExplainControlRationTitle,inn);
        }

        protected override ControlRatioExplainModel CreateModel(ExplainDetailsData explainData, DeclarationSummary declarationData)
        {
            return new ControlRatioExplainModel(explainData, declarationData, _surValuesDictionary);
        }

        protected override ExplainDetailsPresenterBase CreatePresenter(ControlRatioExplainView view, ControlRatioExplainModel model, WorkItem workItem)
        {
            return new ControlRatioExplainPresenter(
                new ExplainControlRatioLoader(_dataService, model.ExplainZip, _notifier, _logger),
                view,
                model,
                _claimCardOpener,
                _taxpayerOpener,
                _commentUpdater,
                _logger);
        }
    }
}
