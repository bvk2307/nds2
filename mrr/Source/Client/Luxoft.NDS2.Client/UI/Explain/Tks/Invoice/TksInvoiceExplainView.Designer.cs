﻿using Luxoft.NDS2.Client.UI.Controls.Sur;

namespace Luxoft.NDS2.Client.UI.Explain.Tks.Invoice
{
    partial class TksInvoiceExplainView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab3 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab4 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab5 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab6 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            this.tabR8 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this._chapter8Container = new Luxoft.NDS2.Client.UI.Controls.Grid.Controls.GridContainer();
            this.tabR9 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this._chapter9GridContainer = new Luxoft.NDS2.Client.UI.Controls.Grid.Controls.GridContainer();
            this.tabR10 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this._chapter10GridContainer = new Luxoft.NDS2.Client.UI.Controls.Grid.Controls.GridContainer();
            this.tabR11 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this._chapter11GridContainer = new Luxoft.NDS2.Client.UI.Controls.Grid.Controls.GridContainer();
            this.tabR12 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this._chapter12GridContainer = new Luxoft.NDS2.Client.UI.Controls.Grid.Controls.GridContainer();
            this.tabNotReflected = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this._confirmedInvoicesGroup = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel2 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this._confirmedContainer = new Luxoft.NDS2.Client.UI.Controls.Grid.Controls.GridContainer();
            this._unconfirmedInvoicesGroup = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this._unconfirmedView = new Luxoft.NDS2.Client.UI.Explain.Tks.Invoice.Controls.UnconfirmedInvoicesView();
            this._tabControl = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.tabR8.SuspendLayout();
            this.tabR9.SuspendLayout();
            this.tabR10.SuspendLayout();
            this.tabR11.SuspendLayout();
            this.tabR12.SuspendLayout();
            this.tabNotReflected.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._confirmedInvoicesGroup)).BeginInit();
            this._confirmedInvoicesGroup.SuspendLayout();
            this.ultraExpandableGroupBoxPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._unconfirmedInvoicesGroup)).BeginInit();
            this._unconfirmedInvoicesGroup.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._tabControl)).BeginInit();
            this._tabControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabR8
            // 
            this.tabR8.Controls.Add(this._chapter8Container);
            this.tabR8.Location = new System.Drawing.Point(1, 23);
            this.tabR8.Name = "tabR8";
            this.tabR8.Size = new System.Drawing.Size(1450, 451);
            // 
            // _chapter8Container
            // 
            this._chapter8Container.AutoScroll = true;
            this._chapter8Container.AutoSize = true;
            this._chapter8Container.Dock = System.Windows.Forms.DockStyle.Fill;
            this._chapter8Container.Location = new System.Drawing.Point(0, 0);
            this._chapter8Container.MinimumSize = new System.Drawing.Size(0, 160);
            this._chapter8Container.Name = "_chapter8Container";
            this._chapter8Container.Size = new System.Drawing.Size(1450, 451);
            this._chapter8Container.TabIndex = 1;
            // 
            // tabR9
            // 
            this.tabR9.Controls.Add(this._chapter9GridContainer);
            this.tabR9.Location = new System.Drawing.Point(-10000, -10000);
            this.tabR9.Name = "tabR9";
            this.tabR9.Size = new System.Drawing.Size(1450, 451);
            // 
            // _chapter9GridContainer
            // 
            this._chapter9GridContainer.AutoScroll = true;
            this._chapter9GridContainer.AutoSize = true;
            this._chapter9GridContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this._chapter9GridContainer.Location = new System.Drawing.Point(0, 0);
            this._chapter9GridContainer.MinimumSize = new System.Drawing.Size(0, 160);
            this._chapter9GridContainer.Name = "_chapter9GridContainer";
            this._chapter9GridContainer.Size = new System.Drawing.Size(1450, 451);
            this._chapter9GridContainer.TabIndex = 2;
            // 
            // tabR10
            // 
            this.tabR10.Controls.Add(this._chapter10GridContainer);
            this.tabR10.Location = new System.Drawing.Point(-10000, -10000);
            this.tabR10.Name = "tabR10";
            this.tabR10.Size = new System.Drawing.Size(1450, 451);
            // 
            // _chapter10GridContainer
            // 
            this._chapter10GridContainer.AutoScroll = true;
            this._chapter10GridContainer.AutoSize = true;
            this._chapter10GridContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this._chapter10GridContainer.Location = new System.Drawing.Point(0, 0);
            this._chapter10GridContainer.MinimumSize = new System.Drawing.Size(0, 160);
            this._chapter10GridContainer.Name = "_chapter10GridContainer";
            this._chapter10GridContainer.Size = new System.Drawing.Size(1450, 451);
            this._chapter10GridContainer.TabIndex = 2;
            // 
            // tabR11
            // 
            this.tabR11.Controls.Add(this._chapter11GridContainer);
            this.tabR11.Location = new System.Drawing.Point(-10000, -10000);
            this.tabR11.Name = "tabR11";
            this.tabR11.Size = new System.Drawing.Size(1450, 451);
            // 
            // _chapter11GridContainer
            // 
            this._chapter11GridContainer.AutoScroll = true;
            this._chapter11GridContainer.AutoSize = true;
            this._chapter11GridContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this._chapter11GridContainer.Location = new System.Drawing.Point(0, 0);
            this._chapter11GridContainer.MinimumSize = new System.Drawing.Size(0, 160);
            this._chapter11GridContainer.Name = "_chapter11GridContainer";
            this._chapter11GridContainer.Size = new System.Drawing.Size(1450, 451);
            this._chapter11GridContainer.TabIndex = 2;
            // 
            // tabR12
            // 
            this.tabR12.Controls.Add(this._chapter12GridContainer);
            this.tabR12.Location = new System.Drawing.Point(-10000, -10000);
            this.tabR12.Name = "tabR12";
            this.tabR12.Size = new System.Drawing.Size(1450, 451);
            // 
            // _chapter12GridContainer
            // 
            this._chapter12GridContainer.AutoScroll = true;
            this._chapter12GridContainer.AutoSize = true;
            this._chapter12GridContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this._chapter12GridContainer.Location = new System.Drawing.Point(0, 0);
            this._chapter12GridContainer.MinimumSize = new System.Drawing.Size(0, 160);
            this._chapter12GridContainer.Name = "_chapter12GridContainer";
            this._chapter12GridContainer.Size = new System.Drawing.Size(1450, 451);
            this._chapter12GridContainer.TabIndex = 2;
            // 
            // tabNotReflected
            // 
            this.tabNotReflected.Controls.Add(this._confirmedInvoicesGroup);
            this.tabNotReflected.Controls.Add(this._unconfirmedInvoicesGroup);
            this.tabNotReflected.Location = new System.Drawing.Point(-10000, -10000);
            this.tabNotReflected.Name = "tabNotReflected";
            this.tabNotReflected.Size = new System.Drawing.Size(1450, 451);
            // 
            // _confirmedInvoicesGroup
            // 
            this._confirmedInvoicesGroup.Controls.Add(this.ultraExpandableGroupBoxPanel2);
            this._confirmedInvoicesGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this._confirmedInvoicesGroup.ExpandedSize = new System.Drawing.Size(1450, 167);
            this._confirmedInvoicesGroup.Location = new System.Drawing.Point(0, 284);
            this._confirmedInvoicesGroup.Name = "_confirmedInvoicesGroup";
            this._confirmedInvoicesGroup.Size = new System.Drawing.Size(1450, 167);
            this._confirmedInvoicesGroup.TabIndex = 7;
            this._confirmedInvoicesGroup.Text = "Сведения, поясняющие расхождения";
            // 
            // ultraExpandableGroupBoxPanel2
            // 
            this.ultraExpandableGroupBoxPanel2.Controls.Add(this._confirmedContainer);
            this.ultraExpandableGroupBoxPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel2.Location = new System.Drawing.Point(3, 19);
            this.ultraExpandableGroupBoxPanel2.Name = "ultraExpandableGroupBoxPanel2";
            this.ultraExpandableGroupBoxPanel2.Size = new System.Drawing.Size(1444, 145);
            this.ultraExpandableGroupBoxPanel2.TabIndex = 0;
            // 
            // _confirmedContainer
            // 
            this._confirmedContainer.AutoScroll = true;
            this._confirmedContainer.AutoSize = true;
            this._confirmedContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this._confirmedContainer.Location = new System.Drawing.Point(0, 0);
            this._confirmedContainer.MinimumSize = new System.Drawing.Size(0, 160);
            this._confirmedContainer.Name = "_confirmedContainer";
            this._confirmedContainer.Size = new System.Drawing.Size(1444, 160);
            this._confirmedContainer.TabIndex = 2;
            // 
            // _unconfirmedInvoicesGroup
            // 
            this._unconfirmedInvoicesGroup.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this._unconfirmedInvoicesGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this._unconfirmedInvoicesGroup.ExpandedSize = new System.Drawing.Size(1450, 284);
            this._unconfirmedInvoicesGroup.Location = new System.Drawing.Point(0, 0);
            this._unconfirmedInvoicesGroup.Name = "_unconfirmedInvoicesGroup";
            this._unconfirmedInvoicesGroup.Size = new System.Drawing.Size(1450, 284);
            this._unconfirmedInvoicesGroup.TabIndex = 6;
            this._unconfirmedInvoicesGroup.Text = "Сведения о записях, операции по которым не подтверждаются";
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this._unconfirmedView);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 19);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1444, 262);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // _unconfirmedView
            // 
            this._unconfirmedView.Dock = System.Windows.Forms.DockStyle.Top;
            this._unconfirmedView.Location = new System.Drawing.Point(0, 0);
            this._unconfirmedView.Name = "_unconfirmedView";
            this._unconfirmedView.Size = new System.Drawing.Size(1444, 262);
            this._unconfirmedView.TabIndex = 3;
            // 
            // _tabControl
            // 
            this._tabControl.Controls.Add(this.ultraTabSharedControlsPage1);
            this._tabControl.Controls.Add(this.tabR8);
            this._tabControl.Controls.Add(this.tabNotReflected);
            this._tabControl.Controls.Add(this.tabR9);
            this._tabControl.Controls.Add(this.tabR10);
            this._tabControl.Controls.Add(this.tabR11);
            this._tabControl.Controls.Add(this.tabR12);
            this._tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tabControl.Location = new System.Drawing.Point(0, 123);
            this._tabControl.Name = "_tabControl";
            this._tabControl.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this._tabControl.Size = new System.Drawing.Size(1454, 477);
            this._tabControl.TabIndex = 6;
            ultraTab1.Key = "tabChapter8";
            ultraTab1.TabPage = this.tabR8;
            ultraTab1.Text = "Раздел 8";
            ultraTab3.Key = "tabChapter9";
            ultraTab3.TabPage = this.tabR9;
            ultraTab3.Text = "Раздел 9";
            ultraTab4.Key = "tabChapter10";
            ultraTab4.TabPage = this.tabR10;
            ultraTab4.Text = "Раздел 10";
            ultraTab5.Key = "tabChapter11";
            ultraTab5.TabPage = this.tabR11;
            ultraTab5.Text = "Раздел 11";
            ultraTab6.Key = "tabChapter12";
            ultraTab6.TabPage = this.tabR12;
            ultraTab6.Text = "Раздел 12";
            ultraTab2.Key = "tabHistoryStatus";
            ultraTab2.TabPage = this.tabNotReflected;
            ultraTab2.Text = "Неотраженные операции";
            this._tabControl.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab3,
            ultraTab4,
            ultraTab5,
            ultraTab6,
            ultraTab2});
            this._tabControl.ActiveTabChanged += new Infragistics.Win.UltraWinTabControl.ActiveTabChangedEventHandler(this.ActiveTabChanged);
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(1450, 451);
            // 
            // TksInvoiceExplainView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this._tabControl);
            this.Name = "TksInvoiceExplainView";
            this.Controls.SetChildIndex(this._tabControl, 0);
            this.tabR8.ResumeLayout(false);
            this.tabR8.PerformLayout();
            this.tabR9.ResumeLayout(false);
            this.tabR9.PerformLayout();
            this.tabR10.ResumeLayout(false);
            this.tabR10.PerformLayout();
            this.tabR11.ResumeLayout(false);
            this.tabR11.PerformLayout();
            this.tabR12.ResumeLayout(false);
            this.tabR12.PerformLayout();
            this.tabNotReflected.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._confirmedInvoicesGroup)).EndInit();
            this._confirmedInvoicesGroup.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel2.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._unconfirmedInvoicesGroup)).EndInit();
            this._unconfirmedInvoicesGroup.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._tabControl)).EndInit();
            this._tabControl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinTabControl.UltraTabControl _tabControl;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl tabR8;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl tabR9;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl tabR10;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl tabR11;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl tabR12;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl tabNotReflected;
        private Infragistics.Win.Misc.UltraExpandableGroupBox _confirmedInvoicesGroup;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel2;
        private Infragistics.Win.Misc.UltraExpandableGroupBox _unconfirmedInvoicesGroup;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Controls.UnconfirmedInvoicesView _unconfirmedView;
        private Luxoft.NDS2.Client.UI.Controls.Grid.Controls.GridContainer _chapter8Container;
        private Luxoft.NDS2.Client.UI.Controls.Grid.Controls.GridContainer _confirmedContainer;
        private Luxoft.NDS2.Client.UI.Controls.Grid.Controls.GridContainer _chapter9GridContainer;
        private Luxoft.NDS2.Client.UI.Controls.Grid.Controls.GridContainer _chapter10GridContainer;
        private Luxoft.NDS2.Client.UI.Controls.Grid.Controls.GridContainer _chapter11GridContainer;
        private Luxoft.NDS2.Client.UI.Controls.Grid.Controls.GridContainer _chapter12GridContainer;
    }
}
