﻿using Infragistics.Win.UltraWinGrid;

namespace Luxoft.NDS2.Client.UI.Explain.Tks.Invoice.Controls
{
    partial class ConfirmedInvoiceGridView
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand1 = new Infragistics.Win.UltraWinGrid.UltraGridBand("ListItems", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn23 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ExplainZip");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn24 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("OperationCodes");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn25 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Status");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn26 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Chapter");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn27 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("InvoiceNumber");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn28 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("InvoiceDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn29 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("BuyerInn");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn30 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("BuyerKpp");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn31 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("BrokerInn");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn32 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("BrokerKpp");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn33 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Amount");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn34 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("AmountRur");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn35 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ChangedState");
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup1 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup4", 184533786);
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup2 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup5", 184533958);
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup3 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup1", 184534114);
            Infragistics.Win.UltraWinGrid.RowLayout rowLayout1 = new Infragistics.Win.UltraWinGrid.RowLayout("1");
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo1 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "ExplainZip", -1, Infragistics.Win.DefaultableBoolean.True);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo2 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "OperationCodes", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo3 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "Status", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo4 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "Chapter", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo5 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "InvoiceNumber", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo6 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "InvoiceDate", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo7 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "BuyerInn", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo8 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "BuyerKpp", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo9 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "BrokerInn", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo10 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "BrokerKpp", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo11 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "Amount", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo12 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "AmountRur", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo13 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "ChangedState", -1, Infragistics.Win.DefaultableBoolean.True);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo14 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Group, "NewGroup4", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo15 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Group, "NewGroup5", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo16 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Group, "NewGroup1", -1, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand2 = new Infragistics.Win.UltraWinGrid.UltraGridBand("ChangedState", 0);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn36 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("NewInvoiceNumber");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn37 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("NewInvoiceDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn38 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("NewBuyerInn");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn39 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("NewBuyerKpp");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn40 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("NewBrokerInn");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn41 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("NewBrokerKpp");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn42 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("NewAmount");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn43 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("NewAmountRur");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn44 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("NewOperationCodes");
            Infragistics.Win.UltraWinGrid.RowLayout rowLayout2 = new Infragistics.Win.UltraWinGrid.RowLayout("0");
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo17 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "NewInvoiceNumber", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo18 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "NewInvoiceDate", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo19 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "NewBuyerInn", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo20 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "NewBuyerKpp", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo21 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "NewBrokerInn", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo22 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "NewBrokerKpp", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo23 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "NewAmount", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo24 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "NewAmountRur", -1, Infragistics.Win.DefaultableBoolean.False);
            Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo25 = new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo(Infragistics.Win.UltraWinGrid.RowLayoutColumnInfoContext.Column, "NewOperationCodes", -1, Infragistics.Win.DefaultableBoolean.False);
            this._grid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this._filterProvider = new Infragistics.Win.SupportDialogs.FilterUIProvider.UltraGridFilterUIProvider(this.components);
            this._bindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this._grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._bindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // _grid
            // 
            this._grid.DataSource = this._bindingSource;
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this._grid.DisplayLayout.Appearance = appearance1;
            ultraGridColumn23.Header.VisiblePosition = 0;
            ultraGridColumn23.Hidden = true;
            ultraGridColumn23.RowLayoutColumnInfo.OriginX = 18;
            ultraGridColumn23.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn23.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn23.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn24.Header.ToolTipText = "Код вида операции";
            ultraGridColumn24.Header.VisiblePosition = 1;
            ultraGridColumn24.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn24.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn24.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(120, 0);
            ultraGridColumn24.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn24.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn24.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn25.Header.Caption = "";
            ultraGridColumn25.Header.ToolTipText = "Признак пояснения записи";
            ultraGridColumn25.Header.VisiblePosition = 11;
            ultraGridColumn25.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn25.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn25.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(20, 0);
            ultraGridColumn25.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 40);
            ultraGridColumn25.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn25.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn26.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn26.Header.ToolTipText = "Раздел, в котором отражена запись о счете-фактуре";
            ultraGridColumn26.Header.VisiblePosition = 2;
            ultraGridColumn26.RowLayoutColumnInfo.OriginX = 20;
            ultraGridColumn26.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn26.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(90, 0);
            ultraGridColumn26.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn26.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn27.Header.Caption = "№";
            ultraGridColumn27.Header.ToolTipText = "Номер счета-фактуры продавца";
            ultraGridColumn27.Header.VisiblePosition = 7;
            ultraGridColumn27.RowLayoutColumnInfo.OriginX = 4;
            ultraGridColumn27.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn27.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn27.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn27.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn27.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn28.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn28.Header.Caption = "Дата";
            ultraGridColumn28.Header.ToolTipText = "Дата счета-фактуры продавца";
            ultraGridColumn28.Header.VisiblePosition = 9;
            ultraGridColumn28.RowLayoutColumnInfo.OriginX = 6;
            ultraGridColumn28.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn28.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn28.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn28.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn29.Header.ToolTipText = "ИНН покупателя";
            ultraGridColumn29.Header.VisiblePosition = 5;
            ultraGridColumn29.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn29.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn29.RowLayoutColumnInfo.ParentGroupIndex = 0;
            ultraGridColumn29.RowLayoutColumnInfo.ParentGroupKey = "NewGroup4";
            ultraGridColumn29.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(100, 0);
            ultraGridColumn29.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn29.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn30.Header.ToolTipText = "КПП покупателя";
            ultraGridColumn30.Header.VisiblePosition = 6;
            ultraGridColumn30.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn30.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn30.RowLayoutColumnInfo.ParentGroupIndex = 0;
            ultraGridColumn30.RowLayoutColumnInfo.ParentGroupKey = "NewGroup4";
            ultraGridColumn30.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(100, 0);
            ultraGridColumn30.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn30.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn30.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn31.Header.ToolTipText = "ИНН посредника";
            ultraGridColumn31.Header.VisiblePosition = 3;
            ultraGridColumn31.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn31.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn31.RowLayoutColumnInfo.ParentGroupIndex = 1;
            ultraGridColumn31.RowLayoutColumnInfo.ParentGroupKey = "NewGroup5";
            ultraGridColumn31.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(100, 0);
            ultraGridColumn31.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn31.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn31.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn32.Header.ToolTipText = "КПП посредника";
            ultraGridColumn32.Header.VisiblePosition = 4;
            ultraGridColumn32.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn32.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn32.RowLayoutColumnInfo.ParentGroupIndex = 1;
            ultraGridColumn32.RowLayoutColumnInfo.ParentGroupKey = "NewGroup5";
            ultraGridColumn32.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(100, 0);
            ultraGridColumn32.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn32.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn32.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn33.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn33.Header.ToolTipText = "Стоимость продаж по счету-фактуре, разница стоимости по корректировочному счету-ф" +
    "актуре (включая налог), в валюте счета-фактуры";
            ultraGridColumn33.Header.VisiblePosition = 8;
            ultraGridColumn33.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn33.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn33.RowLayoutColumnInfo.ParentGroupIndex = 2;
            ultraGridColumn33.RowLayoutColumnInfo.ParentGroupKey = "NewGroup1";
            ultraGridColumn33.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(90, 0);
            ultraGridColumn33.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn33.RowLayoutColumnInfo.SpanY = 1;
            ultraGridColumn34.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn34.Header.ToolTipText = "Стоимость продаж по счету-фактуре, разница стоимости по корректировочному счету-ф" +
    "актуре (включая налог) в рублях и копейках";
            ultraGridColumn34.Header.VisiblePosition = 10;
            ultraGridColumn34.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn34.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn34.RowLayoutColumnInfo.ParentGroupIndex = 2;
            ultraGridColumn34.RowLayoutColumnInfo.ParentGroupKey = "NewGroup1";
            ultraGridColumn34.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(90, 0);
            ultraGridColumn34.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn34.RowLayoutColumnInfo.SpanY = 1;
            ultraGridColumn35.Header.VisiblePosition = 12;
            ultraGridColumn35.Hidden = true;
            ultraGridBand1.Columns.AddRange(new object[] {
            ultraGridColumn23,
            ultraGridColumn24,
            ultraGridColumn25,
            ultraGridColumn26,
            ultraGridColumn27,
            ultraGridColumn28,
            ultraGridColumn29,
            ultraGridColumn30,
            ultraGridColumn31,
            ultraGridColumn32,
            ultraGridColumn33,
            ultraGridColumn34,
            ultraGridColumn35});
            ultraGridGroup1.Header.Caption = "Сведения о покупателе";
            ultraGridGroup1.Key = "NewGroup4";
            ultraGridGroup1.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup1.RowLayoutGroupInfo.OriginX = 8;
            ultraGridGroup1.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup1.RowLayoutGroupInfo.SpanX = 4;
            ultraGridGroup1.RowLayoutGroupInfo.SpanY = 3;
            ultraGridGroup2.Header.Caption = "Сведения о посреднике";
            ultraGridGroup2.Key = "NewGroup5";
            ultraGridGroup2.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup2.RowLayoutGroupInfo.OriginX = 12;
            ultraGridGroup2.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup2.RowLayoutGroupInfo.SpanX = 4;
            ultraGridGroup2.RowLayoutGroupInfo.SpanY = 3;
            ultraGridGroup3.Header.Caption = "Стоимость продаж с НДС";
            ultraGridGroup3.Key = "NewGroup1";
            ultraGridGroup3.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup3.RowLayoutGroupInfo.OriginX = 16;
            ultraGridGroup3.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup3.RowLayoutGroupInfo.SpanX = 4;
            ultraGridGroup3.RowLayoutGroupInfo.SpanY = 2;
            ultraGridBand1.Groups.AddRange(new Infragistics.Win.UltraWinGrid.UltraGridGroup[] {
            ultraGridGroup1,
            ultraGridGroup2,
            ultraGridGroup3});
            rowLayoutColumnInfo1.OriginX = 18;
            rowLayoutColumnInfo1.OriginY = 0;
            rowLayoutColumnInfo1.SpanX = 2;
            rowLayoutColumnInfo1.SpanY = 2;
            rowLayoutColumnInfo3.OriginX = 0;
            rowLayoutColumnInfo3.OriginY = 0;
            rowLayoutColumnInfo3.PreferredCellSize = new System.Drawing.Size(20, 0);
            rowLayoutColumnInfo3.PreferredLabelSize = new System.Drawing.Size(0, 40);
            rowLayoutColumnInfo3.SpanX = 2;
            rowLayoutColumnInfo3.SpanY = 4;
            rowLayoutColumnInfo4.OriginX = 20;
            rowLayoutColumnInfo4.OriginY = 0;
            rowLayoutColumnInfo4.SpanX = 2;
            rowLayoutColumnInfo4.SpanY = 4;
            rowLayoutColumnInfo5.OriginX = 2;
            rowLayoutColumnInfo5.OriginY = 0;
            rowLayoutColumnInfo5.PreferredCellSize = new System.Drawing.Size(80, 0);
            rowLayoutColumnInfo5.SpanX = 2;
            rowLayoutColumnInfo5.SpanY = 4;
            rowLayoutColumnInfo6.OriginX = 4;
            rowLayoutColumnInfo6.OriginY = 0;
            rowLayoutColumnInfo6.PreferredCellSize = new System.Drawing.Size(80, 0);
            rowLayoutColumnInfo6.SpanX = 2;
            rowLayoutColumnInfo6.SpanY = 4;
            rowLayoutColumnInfo7.OriginX = 0;
            rowLayoutColumnInfo7.OriginY = 0;
            rowLayoutColumnInfo7.ParentGroupIndex = 0;
            rowLayoutColumnInfo7.ParentGroupKey = "NewGroup4";
            rowLayoutColumnInfo7.SpanX = 2;
            rowLayoutColumnInfo7.SpanY = 2;
            rowLayoutColumnInfo8.OriginX = 2;
            rowLayoutColumnInfo8.OriginY = 0;
            rowLayoutColumnInfo8.ParentGroupIndex = 0;
            rowLayoutColumnInfo8.ParentGroupKey = "NewGroup4";
            rowLayoutColumnInfo8.SpanX = 2;
            rowLayoutColumnInfo8.SpanY = 2;
            rowLayoutColumnInfo9.OriginX = 0;
            rowLayoutColumnInfo9.OriginY = 0;
            rowLayoutColumnInfo9.ParentGroupIndex = 1;
            rowLayoutColumnInfo9.ParentGroupKey = "NewGroup5";
            rowLayoutColumnInfo9.PreferredCellSize = new System.Drawing.Size(100, 0);
            rowLayoutColumnInfo9.SpanX = 2;
            rowLayoutColumnInfo9.SpanY = 2;
            rowLayoutColumnInfo10.OriginX = 2;
            rowLayoutColumnInfo10.OriginY = 0;
            rowLayoutColumnInfo10.ParentGroupIndex = 1;
            rowLayoutColumnInfo10.ParentGroupKey = "NewGroup5";
            rowLayoutColumnInfo10.PreferredCellSize = new System.Drawing.Size(80, 0);
            rowLayoutColumnInfo10.SpanX = 2;
            rowLayoutColumnInfo10.SpanY = 2;
            rowLayoutColumnInfo11.OriginX = 0;
            rowLayoutColumnInfo11.OriginY = 0;
            rowLayoutColumnInfo11.ParentGroupIndex = 2;
            rowLayoutColumnInfo11.ParentGroupKey = "NewGroup1";
            rowLayoutColumnInfo11.SpanX = 2;
            rowLayoutColumnInfo11.SpanY = 1;
            rowLayoutColumnInfo12.OriginX = 2;
            rowLayoutColumnInfo12.OriginY = 0;
            rowLayoutColumnInfo12.ParentGroupIndex = 2;
            rowLayoutColumnInfo12.ParentGroupKey = "NewGroup1";
            rowLayoutColumnInfo12.SpanX = 2;
            rowLayoutColumnInfo12.SpanY = 1;
            rowLayout1.ColumnInfos.AddRange(new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo[] {
            rowLayoutColumnInfo1,
            rowLayoutColumnInfo2,
            rowLayoutColumnInfo3,
            rowLayoutColumnInfo4,
            rowLayoutColumnInfo5,
            rowLayoutColumnInfo6,
            rowLayoutColumnInfo7,
            rowLayoutColumnInfo8,
            rowLayoutColumnInfo9,
            rowLayoutColumnInfo10,
            rowLayoutColumnInfo11,
            rowLayoutColumnInfo12,
            rowLayoutColumnInfo13,
            rowLayoutColumnInfo14,
            rowLayoutColumnInfo15,
            rowLayoutColumnInfo16});
            rowLayout1.RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.GroupLayout;
            ultraGridBand1.RowLayouts.AddRange(new Infragistics.Win.UltraWinGrid.RowLayout[] {
            rowLayout1});
            ultraGridBand1.RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.GroupLayout;
            ultraGridColumn36.Header.VisiblePosition = 0;
            ultraGridColumn36.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn36.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn36.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn36.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn36.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn37.Header.VisiblePosition = 1;
            ultraGridColumn37.RowLayoutColumnInfo.OriginX = 4;
            ultraGridColumn37.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn37.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn37.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn37.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn38.Header.VisiblePosition = 2;
            ultraGridColumn38.RowLayoutColumnInfo.OriginX = 6;
            ultraGridColumn38.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn38.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(100, 0);
            ultraGridColumn38.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn38.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn39.Header.VisiblePosition = 3;
            ultraGridColumn39.RowLayoutColumnInfo.OriginX = 8;
            ultraGridColumn39.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn39.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(100, 0);
            ultraGridColumn39.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn39.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn40.Header.VisiblePosition = 4;
            ultraGridColumn40.RowLayoutColumnInfo.OriginX = 10;
            ultraGridColumn40.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn40.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(100, 0);
            ultraGridColumn40.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn40.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn41.Header.VisiblePosition = 5;
            ultraGridColumn41.RowLayoutColumnInfo.OriginX = 12;
            ultraGridColumn41.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn41.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(100, 0);
            ultraGridColumn41.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn41.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn42.Header.VisiblePosition = 6;
            ultraGridColumn42.RowLayoutColumnInfo.OriginX = 14;
            ultraGridColumn42.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn42.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(90, 0);
            ultraGridColumn42.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn42.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn43.Header.VisiblePosition = 7;
            ultraGridColumn43.RowLayoutColumnInfo.OriginX = 16;
            ultraGridColumn43.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn43.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(90, 0);
            ultraGridColumn43.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn43.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn44.Header.VisiblePosition = 8;
            ultraGridColumn44.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn44.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn44.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(120, 0);
            ultraGridColumn44.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn44.RowLayoutColumnInfo.SpanY = 2;
            ultraGridBand2.Columns.AddRange(new object[] {
            ultraGridColumn36,
            ultraGridColumn37,
            ultraGridColumn38,
            ultraGridColumn39,
            ultraGridColumn40,
            ultraGridColumn41,
            ultraGridColumn42,
            ultraGridColumn43,
            ultraGridColumn44});
            ultraGridBand2.RowLayoutLabelPosition = Infragistics.Win.UltraWinGrid.LabelPosition.None;
            rowLayoutColumnInfo17.OriginX = 0;
            rowLayoutColumnInfo18.OriginX = 2;
            rowLayoutColumnInfo19.OriginX = 4;
            rowLayoutColumnInfo20.OriginX = 6;
            rowLayoutColumnInfo21.OriginX = 8;
            rowLayoutColumnInfo22.OriginX = 10;
            rowLayoutColumnInfo23.OriginX = 12;
            rowLayoutColumnInfo24.OriginX = 14;
            rowLayoutColumnInfo25.OriginX = 16;
            rowLayout2.ColumnInfos.AddRange(new Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo[] {
            rowLayoutColumnInfo17,
            rowLayoutColumnInfo18,
            rowLayoutColumnInfo19,
            rowLayoutColumnInfo20,
            rowLayoutColumnInfo21,
            rowLayoutColumnInfo22,
            rowLayoutColumnInfo23,
            rowLayoutColumnInfo24,
            rowLayoutColumnInfo25});
            rowLayout2.RowLayoutLabelPosition = Infragistics.Win.UltraWinGrid.LabelPosition.None;
            rowLayout2.RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.ColumnLayout;
            ultraGridBand2.RowLayouts.AddRange(new Infragistics.Win.UltraWinGrid.RowLayout[] {
            rowLayout2});
            ultraGridBand2.RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.GroupLayout;
            this._grid.DisplayLayout.BandsSerializer.Add(ultraGridBand1);
            this._grid.DisplayLayout.BandsSerializer.Add(ultraGridBand2);
            this._grid.DisplayLayout.InterBandSpacing = 0;
            this._grid.DisplayLayout.Override.AllowColMoving = Infragistics.Win.UltraWinGrid.AllowColMoving.NotAllowed;
            this._grid.DisplayLayout.Override.AllowColSizing = Infragistics.Win.UltraWinGrid.AllowColSizing.Free;
            this._grid.DisplayLayout.Override.AllowColSwapping = Infragistics.Win.UltraWinGrid.AllowColSwapping.NotAllowed;
            this._grid.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            this._grid.DisplayLayout.Override.ExpansionIndicator = Infragistics.Win.UltraWinGrid.ShowExpansionIndicator.CheckOnDisplay;
            this._grid.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Row;
            this._grid.DisplayLayout.Override.FilterEvaluationTrigger = Infragistics.Win.UltraWinGrid.FilterEvaluationTrigger.OnLeaveCell;
            this._grid.DisplayLayout.Override.FilterOperandStyle = Infragistics.Win.UltraWinGrid.FilterOperandStyle.DropDownList;
            this._grid.DisplayLayout.Override.FilterOperatorDropDownItems = ((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems)((((((((((((((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.NotEquals) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.LessThan) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.LessThanOrEqualTo) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.GreaterThan) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.GreaterThanOrEqualTo) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Like) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.NotLike) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.StartsWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotStartWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.EndsWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotEndWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Contains) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotContain)));
            this._grid.DisplayLayout.Override.FilterOperatorLocation = Infragistics.Win.UltraWinGrid.FilterOperatorLocation.WithOperand;
            this._grid.DisplayLayout.Override.FilterUIProvider = this._filterProvider;
            this._grid.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons;
            this._grid.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.ExternalSortSingle;
            this._grid.DisplayLayout.Override.HeaderPlacement = Infragistics.Win.UltraWinGrid.HeaderPlacement.FixedOnTop;
            this._grid.DisplayLayout.Override.RowFilterAction = Infragistics.Win.UltraWinGrid.RowFilterAction.AppearancesOnly;
            this._grid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this._grid.DisplayLayout.Override.WrapHeaderText = Infragistics.Win.DefaultableBoolean.True;
            this._grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._grid.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._grid.Location = new System.Drawing.Point(0, 0);
            this._grid.Name = "_grid";
            this._grid.Size = new System.Drawing.Size(804, 306);
            this._grid.TabIndex = 0;
            this._grid.UseAppStyling = false;
            // 
            // _bindingSource
            // 
            this._bindingSource.DataMember = "ListItems";
            this._bindingSource.DataSource = typeof(Luxoft.NDS2.Client.UI.Explain.Tks.Invoice.Models.ConfirmedInvoiceChapterModel);
            // 
            // ConfirmedInvoiceGridView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._grid);
            this.Name = "ConfirmedInvoiceGridView";
            this.Size = new System.Drawing.Size(804, 306);
            ((System.ComponentModel.ISupportInitialize)(this._grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._bindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.BindingSource _bindingSource;
        private Infragistics.Win.UltraWinGrid.UltraGrid _grid;
        private Infragistics.Win.SupportDialogs.FilterUIProvider.UltraGridFilterUIProvider _filterProvider;
    }
}
