﻿using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Client.UI.Controls.Grid.ViewModels;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using Luxoft.NDS2.Common.Contracts.Helpers;

namespace Luxoft.NDS2.Client.UI.Explain.Tks.Invoice.Models
{
    public class Chapter11Model : ListViewModel<Chapter11InvoiceModel, ExplainTksInvoiceChapter11>
    {
        public Chapter11Model()
        {
            AddFilterConverter(
                TypeHelper<Chapter11InvoiceModel>.GetMemberName(x => x.OperationCodes),
                    new ColumnNameReplacer(
                    TypeHelper<IExplainTksInvoice>.GetMemberName(x => x.OperationCodesBit)));
        }

        protected override Chapter11InvoiceModel ConvertToModel(ExplainTksInvoiceChapter11 dataItem)
        {
            return new Chapter11InvoiceModel(dataItem);
        }
    }
}
