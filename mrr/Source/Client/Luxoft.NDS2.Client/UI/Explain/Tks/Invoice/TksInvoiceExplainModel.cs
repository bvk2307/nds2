﻿using Luxoft.NDS2.Client.UI.Controls.Sur;
using Luxoft.NDS2.Client.UI.Explain.Tks.Invoice.Models;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.ExplainReply;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;

namespace Luxoft.NDS2.Client.UI.Explain.Tks.Invoice
{
    public class TksInvoiceExplainModel : ExplainDetailsModel
    {
        # region .ctor

        public TksInvoiceExplainModel(
            ExplainDetailsData explainData, 
            DeclarationSummary declarationData, 
            DictionarySur surValuesDictionary)
            : base(explainData, declarationData, surValuesDictionary)
        {
            Chapter8 = new Chapter8Model();
            Chapter9 = new Chapter9Model();
            Chapter10 = new Chapter10Model();
            Chapter11 = new Chapter11Model();
            Chapter12 = new Chapter12Model();
            ConfirmedInvoices = new ConfirmedInvoiceChapterModel();
        }

        # endregion

        # region Разделы пояснения

        public Chapter8Model Chapter8
        {
            get;
            private set;
        }

        public Chapter9Model Chapter9
        {
            get;
            private set;
        }

        public Chapter10Model Chapter10
        {
            get;
            private set;
        }

        public Chapter11Model Chapter11
        {
            get;
            private set;
        }

        public Chapter12Model Chapter12
        {
            get;
            private set;
        }

        public ConfirmedInvoiceChapterModel ConfirmedInvoices
        {
            get;
            private set;
        }

        # endregion
    }
}
