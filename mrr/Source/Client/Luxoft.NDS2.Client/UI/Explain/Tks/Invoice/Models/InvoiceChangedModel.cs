﻿using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using Luxoft.NDS2.Common.Models;
using System;

namespace Luxoft.NDS2.Client.UI.Explain.Tks.Invoice.Models
{
    public abstract class InvoiceChangedModel<TDto> : ExplainInvoiceChangedModel<TDto>
        where TDto : IExplainTksInvoice
    {
        protected InvoiceChangedModel(TDto data)
            : base(data)
        {
        }

        public string OrdinalNumber
        {
            get { return string.Empty; }
        }

        public string OperationCodes
        {
            get
            {
                return _data.NewOperationCodesBit.HasValue
                    ? _data.NewOperationCodesBit.Value.ToOperationCodes()
                    : string.Empty;
            }
        }

        public string ChangeNumber
        {
            get { return _data.NewChangeNumber; }
        }

        public DateTime? ChangeDate
        {
            get { return _data.NewChangeDate; }
        }

        public string CorrectionNumber
        {
            get { return _data.NewCorrectionNumber; }
        }

        public DateTime? CorrectionDate
        {
            get { return _data.NewCorrectionDate; }
        }

        public string ChangeCorrectionNumber
        {
            get { return _data.NewChangeCorrectionNumber; }
        }

        public DateTime? ChangeCorrectionDate
        {
            get { return _data.NewChangeCorrectionDate; }
        }
    }
}
