﻿using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using System;

namespace Luxoft.NDS2.Client.UI.Explain.Tks.Invoice.Models
{
    public abstract class BookInvoiceModel<TDto, TChangedModel> : InvoiceModel<TDto, TChangedModel>
        where TDto : IExplainTksBookInvoice
        where TChangedModel : BookInvoiceChangedModel<TDto>
    {
        protected BookInvoiceModel(TDto data)
            : base(data)
        {
        }

        public abstract bool AddList
        {
            get;
        }

        public string ReceiveDate
        {
            get { return _data.ReceiveDates.All().Format(); }
        }

        public string ReceiptDocumentsAll
        {
            get { return _data.ReceiptDocuments.All().Format(); }
        }

        public string BrokerInn
        {
            get { return _data.BrokerInn; }
        }

        public string BrokerKpp
        {
            get { return _data.BrokerKpp; }
        }
    }
}
