﻿using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using Luxoft.NDS2.Common.Models;
using System;

namespace Luxoft.NDS2.Client.UI.Explain.Tks.Invoice.Models
{
    public abstract class InvoiceModel<TDto, TChangedModel> : ExplainInvoiceModel<TDto, TChangedModel>
        where TDto : IExplainTksInvoice
        where TChangedModel : InvoiceChangedModel<TDto>
    {
        protected InvoiceModel(TDto data)
            : base(data)
        {
        }

        public string OrdinalNumber
        {
            get { return _data.OrdinalNumber; }
        }

        public string OperationCodes
        {
            get
            {
                return _data.OperationCodesBit.HasValue
                    ? _data.OperationCodesBit.Value.ToOperationCodes()
                    : string.Empty;
            }
        }

        public string ChangeNumber
        {
            get { return _data.ChangeNumber; }
        }

        public DateTime? ChangeDate
        {
            get { return _data.ChangeDate; }
        }

        public string CorrectionNumber
        {
            get { return _data.CorrectionNumber; }
        }

        public DateTime? CorrectionDate
        {
            get { return _data.CorrectionDate; }
        }

        public string ChangeCorrectionNumber
        {
            get { return _data.ChangeCorrectionNumber; }
        }

        public DateTime? ChangeCorrectionDate
        {
            get { return _data.ChangeCorrectionDate; }
        }
    }
}
