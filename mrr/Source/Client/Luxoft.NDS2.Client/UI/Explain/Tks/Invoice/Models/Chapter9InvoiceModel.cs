﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;

namespace Luxoft.NDS2.Client.UI.Explain.Tks.Invoice.Models
{
    public class Chapter9InvoiceModel : BookInvoiceModel<ExplainTksInvoiceChapter9, Chapter9InvoiceChangedModel>
    {
        public Chapter9InvoiceModel(ExplainTksInvoiceChapter9 data)
            : base(data)
        {
        }

        public string BuyerInn
        {
            get
            {
                return _data.Contractors.All().FormatInn();
            }
        }

        public string BuyerKpp
        {
            get
            {
                return _data.Contractors.All().FormatKpp();
            }
        }

        public decimal? SalesAmount
        {
            get
            {
                return _data.SalesAmount;
            }
        }

        public decimal? SalesAmountRur
        {
            get
            {
                return _data.SalesAmountRur;
            }
        }

        public override bool AddList
        {
            get 
            {
                return _data.Chapter == AskInvoiceChapterNumber.Chapter91;
            }
        }

        protected override Chapter9InvoiceChangedModel ChangedState()
        {
            return new Chapter9InvoiceChangedModel(_data);
        }
    }
}
