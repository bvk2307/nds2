﻿using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using Luxoft.NDS2.Common.Models;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Client.UI.Controls.Grid.ViewModels;

namespace Luxoft.NDS2.Client.UI.Explain.Tks.Invoice.Models
{
    public static class ChapterNumberConvertor
    {
        public static Dictionary<AskInvoiceChapterNumber, string> ChapterTitles =
            new Dictionary<AskInvoiceChapterNumber, string>
            {
                { AskInvoiceChapterNumber.Chapter8, "8" },
                { AskInvoiceChapterNumber.Chapter81, "8.1" },
                { AskInvoiceChapterNumber.Chapter9, "9" },
                { AskInvoiceChapterNumber.Chapter91, "9.1" },
                { AskInvoiceChapterNumber.Chapter10, "10" },
                { AskInvoiceChapterNumber.Chapter11, "11" },
                { AskInvoiceChapterNumber.Chapter12, "12" }
            };

        public static string Convert(this AskInvoiceChapterNumber chapterNumber)
        {
            return ChapterTitles[chapterNumber];
        }
    }

    public class ConfirmedInvoiceModel
    {
        private readonly ExplainTksInvoiceConfirmed _data;

        private readonly IList<ConfirmedInvoiceChangedModel> _changedState;

        public ConfirmedInvoiceModel(ExplainTksInvoiceConfirmed data)
        {
            _data = data;
            _changedState = new List<ConfirmedInvoiceChangedModel> {new ConfirmedInvoiceChangedModel(_data)};
        }

        public long ExplainZip { get { return _data.ExplainZip; } }

        [DisplayName(@"Код вида операции")]
        [Description(@"Код вида операции")]
        public string OperationCodes
        {
            get
            {
                return _data.OperationCodesBit.HasValue
                    ? _data.OperationCodesBit.Value.ToOperationCodes()
                    : string.Empty;
            }
        }

        [DisplayName(@"Признак записи")]
        [Description("Признак пояснения записи")]
        public ExplainTksInvoiceState Status { get { return _data.Status; } }

        [DisplayName(@"Раздел")]
        [Description("Раздел, в котором отражена запись о счете-фактуре")]
        public string Chapter 
        { 
            get 
            { 
                return _data.Chapter.Convert(); 
            } 
        }

        [DisplayName(@"Номер СФ")]
        [Description("Номер счета-фактуры продав")]
        public string InvoiceNumber { get { return _data.InvoiceNumber; } }

        [DisplayName(@"Дата СФ")]
        [Description("Дата счета-фактуры продав")]
        public DateTime? InvoiceDate { get { return _data.InvoiceDate; } }

        [DisplayName(@"ИНН")]
        [Description("ИНН покупателя")]
        public string BuyerInn { get { return _data.BuyerInn; } }

        [DisplayName(@"КПП")]
        [Description("КПП покупателя")]
        public string BuyerKpp { get { return _data.BuyerKpp; } }

        [DisplayName(@"ИНН")]
        [Description("ИНН посредника")]
        public string BrokerInn { get { return _data.BrokerInn; } }

        [DisplayName(@"КПП")]
        [Description("КПП посредника")]
        public string BrokerKpp { get { return _data.BrokerKpp; } }

        [DisplayName(@"в валюте")]
        [Description("Стоимость продаж по счету-фактуре, разница стоимости по корректировочному счету-фактуре (включая налог), в валюте счета-фактуры")]
        public decimal? Amount { get { return _data.Amount; } }

        [DisplayName(@"в руб. и коп.")]
        [Description("Стоимость продаж по счету-фактуре, разница стоимости по корректировочному счету-фактуре (включая налог) в рублях и копейках")]
        public decimal? AmountRur { get { return _data.AmountRur; } }

        public IList<ConfirmedInvoiceChangedModel> ChangedState
        {
            get { return _changedState; }
        }
    }

    public class ConfirmedInvoiceChangedModel
    {
        private readonly ExplainTksInvoiceConfirmed _data;

        public ConfirmedInvoiceChangedModel(ExplainTksInvoiceConfirmed data)
        {
            _data = data;
        }

        public string NewInvoiceNumber { get { return _data.NewInvoiceNumber; } }

        public DateTime? NewInvoiceDate { get { return _data.NewInvoiceDate; }}

        public string NewBuyerInn { get { return _data.NewBuyerInn; } }

        public string NewBuyerKpp { get { return _data.NewBuyerKpp; } }

        public string NewBrokerInn { get { return _data.NewBrokerInn; } }

        public string NewBrokerKpp { get { return _data.NewBrokerKpp; } }

        public decimal? NewAmount { get { return _data.NewAmount; } }

        public decimal? NewAmountRur { get { return _data.NewAmountRur; } }

        public string NewOperationCodes
        {
            get
            {
                return _data.NewOperationCodesBit.HasValue
                    ? _data.NewOperationCodesBit.Value.ToOperationCodes()
                    : string.Empty;
            }
        }
    }

    public class ConfirmedInvoiceChapterModel : ListViewModel<ConfirmedInvoiceModel, ExplainTksInvoiceConfirmed>
    {
        public ConfirmedInvoiceChapterModel()
        {
            AddFilterConverter(
                TypeHelper<ConfirmedInvoiceModel>.GetMemberName(x => x.OperationCodes),
                    new ColumnNameReplacer(
                    TypeHelper<ExplainTksInvoiceConfirmed>.GetMemberName(x => x.OperationCodesBit)));
        }

        protected override ConfirmedInvoiceModel ConvertToModel(ExplainTksInvoiceConfirmed dataItem)
        {
            return new ConfirmedInvoiceModel(dataItem);
        }
    }
}
