﻿using Luxoft.NDS2.Common.Contracts.DTO.Explain;

namespace Luxoft.NDS2.Client.UI.Explain.Tks.Invoice.Models
{
    public class Chapter9InvoiceChangedModel : BookInvoiceChangedModel<ExplainTksInvoiceChapter9>
    {
        public Chapter9InvoiceChangedModel(ExplainTksInvoiceChapter9 data)
            : base(data)
        {
        }

        public string BuyerInn
        {
            get
            {
                return _data.Contractors.New().FormatInn();
            }
        }

        public string BuyerKpp
        {
            get
            {
                return _data.Contractors.New().FormatKpp();
            }
        }

        public decimal? SalesAmount
        {
            get
            {
                return _data.NewSalesAmount;
            }
        }

        public decimal? SalesAmountRur
        {
            get
            {
                return _data.NewSalesAmountRur;
            }
        }
    }
}
