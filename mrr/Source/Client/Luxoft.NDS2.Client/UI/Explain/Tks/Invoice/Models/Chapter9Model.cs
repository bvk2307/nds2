﻿using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Client.UI.Controls.Grid.ViewModels;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using Luxoft.NDS2.Common.Contracts.Helpers;

namespace Luxoft.NDS2.Client.UI.Explain.Tks.Invoice.Models
{
    public class Chapter9Model : ListViewModel<Chapter9InvoiceModel, ExplainTksInvoiceChapter9>
    {
        public Chapter9Model()
        {
            AddSortBuilder(
                TypeHelper<Chapter9InvoiceModel>.GetMemberName(x => x.AddList),
                new DefaultColumnSortBuilder(
                    TypeHelper<IExplainTksBookInvoice>.GetMemberName(x => x.Chapter)));
            AddFilterConverter(
                TypeHelper<Chapter9InvoiceModel>.GetMemberName(x => x.OperationCodes),
                new ColumnNameReplacer(
                    TypeHelper<IExplainTksBookInvoice>.GetMemberName(x => x.OperationCodesBit)));
            AddFilterConverter(
                TypeHelper<Chapter9InvoiceModel>.GetMemberName(x => x.BuyerInn),
                new ColumnNameReplacer(
                    TypeHelper<ExplainTksInvoiceChapter9>.GetMemberName(x => x.FirstBuyerInn)));
            AddFilterConverter(
                TypeHelper<Chapter9InvoiceModel>.GetMemberName(x => x.BuyerKpp),
                new ColumnNameReplacer(
                    TypeHelper<ExplainTksInvoiceChapter9>.GetMemberName(x => x.FirstBuyerKpp)));
        }

        protected override Chapter9InvoiceModel ConvertToModel(ExplainTksInvoiceChapter9 dataItem)
        {
            return new Chapter9InvoiceModel(dataItem);
        }
    }
}
