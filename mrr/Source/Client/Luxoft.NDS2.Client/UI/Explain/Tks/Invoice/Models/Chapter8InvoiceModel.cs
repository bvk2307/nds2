﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;

namespace Luxoft.NDS2.Client.UI.Explain.Tks.Invoice.Models
{
    public class Chapter8InvoiceModel : BookInvoiceModel<ExplainTksInvoiceChapter8, Chapter8InvoiceChangedModel>
    {
        public Chapter8InvoiceModel(ExplainTksInvoiceChapter8 data)
            : base(data)
        {
        }

        protected override Chapter8InvoiceChangedModel ChangedState()
        {
            return new Chapter8InvoiceChangedModel(_data);
        }

        public override bool AddList
        {
            get { return _data.Chapter == AskInvoiceChapterNumber.Chapter81; }
        }

        public string SellerInn
        {
            get { return _data.Contractors.All().FormatInn(); }
        }

        public string SellerKpp
        {
            get { return _data.Contractors.All().FormatKpp(); }
        }

        public decimal? PurchaseAmount
        {
            get { return _data.PurchaseAmount; }
        }
    }
}
