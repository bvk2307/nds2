﻿using Luxoft.NDS2.Common.Contracts.DTO.Explain;

namespace Luxoft.NDS2.Client.UI.Explain.Tks.Invoice.Models
{
    public class Chapter11InvoiceModel : InvoiceModel<ExplainTksInvoiceChapter11, Chapter11InvoiceChangedModel>
    {
        public Chapter11InvoiceModel(ExplainTksInvoiceChapter11 data)
            : base(data)
        {
        }

        protected override Chapter11InvoiceChangedModel ChangedState()
        {
            return new Chapter11InvoiceChangedModel(_data);
        }

        public string SellerInn
        {
            get
            {
                return _data.SellerInn;
            }
        }

        public string SellerKpp
        {
            get
            {
                return _data.SellerKpp;
            }
        }

        public string BrokerInn
        {
            get
            {
                return _data.BrokerInn;
            }
        }

        public string BrokerKpp
        {
            get
            {
                return _data.BrokerKpp;
            }
        }

        public string DealCode
        {
            get
            {
                return _data.DealCode;
            }
        }

        public decimal? Amount
        {
            get
            {
                return _data.Amount;
            }
        }
    }
}
