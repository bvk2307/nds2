﻿using System.Drawing;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Explain.Tks.Invoice.Models
{
    public static class InvoiceStatusDictionary
    {
        private static readonly Dictionary<ExplainTksInvoiceState, string> Tips =
            new Dictionary<ExplainTksInvoiceState, string>
            {
                { ExplainTksInvoiceState.Changed, ResourceManagerNDS2.ExplainMessages.ChangedRecord },
                { ExplainTksInvoiceState.Confirmed, ResourceManagerNDS2.ExplainMessages.ConfirmedRecord },
                { ExplainTksInvoiceState.Ignored, ResourceManagerNDS2.ExplainMessages.RejectedRecord },
            };

        public static string GetTooltip(ExplainTksInvoiceState state)
        {
            string s;
            return Tips.TryGetValue(state, out s) ? s : null;
        }

        public static readonly Dictionary<ExplainTksInvoiceState, Bitmap> StatusIcons =
            new Dictionary<ExplainTksInvoiceState, Bitmap>
            {
                {ExplainTksInvoiceState.Changed, Properties.Resources.invoice_modified_16}, 
                {ExplainTksInvoiceState.Confirmed, Properties.Resources.invoice_confirmed_16},
                {ExplainTksInvoiceState.Ignored, Properties.Resources.invoice_ignored_16}
            };
    }
}
