﻿using Infragistics.Win.UltraWinTabControl;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Client.UI.Controls.Grid.Addins;
using Luxoft.NDS2.Client.UI.Controls.Grid.Controls;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;
using Luxoft.NDS2.Client.UI.Explain.Tks.Invoice.Controls;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using System;

namespace Luxoft.NDS2.Client.UI.Explain.Tks.Invoice
{
    /// <summary>
    /// Этот класс реализует представление экранной формы "Карточка пояснения на АТ по СФ, пришедшего по ТКС"
    /// </summary>
    public partial class TksInvoiceExplainView : ExplainDetailsViewBase, ITksInvoiceExplainView
    {
        # region Размеры диалогового окна настройки видимости колонок

        private const int ChapterColumnChooserWidth = 300;

        private const int ChapterColumnChooserHeigth = 350;

        # endregion

        # region .ctor

        public TksInvoiceExplainView()
        {
            InitializeComponent();
            InitChapter8Component();
            InitChapter9Component();
            InitChapter10Component();
            InitChapter11Component();
            InitChapter12Component();
            InitConfirmedInvoices();
            _explainType.Text = @"Пояснение по СФ №";
        }

        # endregion

        # region Смена активной вкладки

        public event EventHandler ChapterChanged;

        private void ActiveTabChanged(object sender, ActiveTabChangedEventArgs e)
        {
            CurrentChapter = (ExplainCardTab)e.Tab.Index;

            if (ChapterChanged != null)
            {
                ChapterChanged(this, new EventArgs());
            }
        }

        public ExplainCardTab CurrentChapter
        {
            get;
            private set;
        }

        # endregion

        # region Раздел 8

        private readonly ExplainChapter8GridView _chapter8Grid =
            new ExplainChapter8GridView();

        public IExtendedGridView Chapter8Grid
        {
            get
            {
                return _chapter8Grid;
            }
        }

        private readonly GridPagerView _chapter8GridPager =
            new GridPagerView();

        public IGridPagerView Chapter8Pager
        {
            get
            {
                return _chapter8GridPager;
            }
        }

        private void InitChapter8Component()
        {
            _chapter8Container
                .WithGrid(_chapter8Grid)
                .WithPager(_chapter8GridPager)
                .WithGridResetter(new GridColumnsVisibilityResetter(_chapter8Grid))
                .WithGridResetter(new GridColumnSortResetter(_chapter8Grid))
                .WithGridResetter(new GridColumnFilterResetter(_chapter8Grid))
                .WithColumnChooser(
                    new GridColumnChooserView(
                        ChapterColumnChooserWidth,
                        ChapterColumnChooserHeigth));
        }

        # endregion

        # region Раздел 9

        private readonly ExplainChapter9GridView _chapter9Grid =
            new ExplainChapter9GridView();

        public IExtendedGridView Chapter9Grid
        {
            get
            {
                return _chapter9Grid;
            }
        }

        private readonly GridPagerView _chapter9GridPager =
            new GridPagerView();

        public IGridPagerView Chapter9Pager
        {
            get
            {
                return _chapter9GridPager;
            }
        }

        private void InitChapter9Component()
        {
            _chapter9GridContainer
                .WithGrid(_chapter9Grid)
                .WithPager(_chapter9GridPager)
                .WithGridResetter(new GridColumnsVisibilityResetter(_chapter9Grid))
                .WithGridResetter(new GridColumnSortResetter(_chapter9Grid))
                .WithGridResetter(new GridColumnFilterResetter(_chapter9Grid))
                .WithColumnChooser(
                    new GridColumnChooserView(
                        ChapterColumnChooserWidth,
                        ChapterColumnChooserHeigth));
        }

        # endregion

        # region Раздел 10

        private readonly ExplainChapter10GridView _chapter10Grid =
            new ExplainChapter10GridView();

        public IExtendedGridView Chapter10Grid
        {
            get
            {
                return _chapter10Grid;
            }
        }

        private readonly GridPagerView _chapter10GridPager =
            new GridPagerView();

        public IGridPagerView Chapter10Pager
        {
            get
            {
                return _chapter10GridPager;
            }
        }

        private void InitChapter10Component()
        {
            _chapter10GridContainer
                .WithGrid(_chapter10Grid)
                .WithPager(_chapter10GridPager)
                .WithGridResetter(new GridColumnsVisibilityResetter(_chapter10Grid))
                .WithGridResetter(new GridColumnSortResetter(_chapter10Grid))
                .WithGridResetter(new GridColumnFilterResetter(_chapter10Grid))
                .WithColumnChooser(
                    new GridColumnChooserView(
                        ChapterColumnChooserWidth,
                        ChapterColumnChooserHeigth));
        }

        # endregion

        # region Раздел 11

        private readonly ExplainChapter11GridView _chapter11Grid =
            new ExplainChapter11GridView();

        public IExtendedGridView Chapter11Grid
        {
            get
            {
                return _chapter11Grid;
            }
        }

        private readonly GridPagerView _chapter11GridPager =
            new GridPagerView();

        public IGridPagerView Chapter11Pager
        {
            get
            {
                return _chapter11GridPager;
            }
        }

        private void InitChapter11Component()
        {
            _chapter11GridContainer
                .WithGrid(_chapter11Grid)
                .WithPager(_chapter11GridPager)
                .WithGridResetter(new GridColumnsVisibilityResetter(_chapter11Grid))
                .WithGridResetter(new GridColumnSortResetter(_chapter11Grid))
                .WithGridResetter(new GridColumnFilterResetter(_chapter11Grid))
                .WithColumnChooser(
                    new GridColumnChooserView(
                        ChapterColumnChooserWidth,
                        ChapterColumnChooserHeigth));
        }

        # endregion

        # region Раздел 12

        private readonly ExplainChapter12GridView _chapter12Grid =
            new ExplainChapter12GridView();

        public IExtendedGridView Chapter12Grid
        {
            get
            {
                return _chapter12Grid;
            }
        }

        private readonly GridPagerView _chapter12GridPager =
            new GridPagerView();

        public IGridPagerView Chapter12Pager
        {
            get
            {
                return _chapter12GridPager;
            }
        }

        private void InitChapter12Component()
        {
            _chapter12GridContainer
                .WithGrid(_chapter12Grid)
                .WithPager(_chapter12GridPager)
                .WithGridResetter(new GridColumnsVisibilityResetter(_chapter12Grid))
                .WithGridResetter(new GridColumnSortResetter(_chapter12Grid))
                .WithGridResetter(new GridColumnFilterResetter(_chapter12Grid))
                .WithColumnChooser(
                    new GridColumnChooserView(
                        ChapterColumnChooserWidth,
                        ChapterColumnChooserHeigth));
        }

        # endregion

        # region Неподтвержденные сведения

        public IUnconfirmedInvoicesView UnconfirmedInvoices
        {
            get
            {
                return _unconfirmedView;
            }
        }

        public void InitUnconfirmedInvoices()
        {
            var helper = new ColumnHelper<ExplainTksInvoiceUnconfirmed>(_unconfirmedView.Grid);
            var setup = new GridColumnSetup();

            setup.Columns.Add(helper.CreateTextColumn(x => x.InvoiceNumber, 2).Configure(c => c.DisableSort = true));
            setup.Columns.Add(helper.CreateTextColumn(x => x.InvoiceDate, 2).Configure(c => c.DisableFilter = true));

            var group = helper.CreateGroup("Сведения о контрагенте");
            group.Columns.Add(helper.CreateTextColumn(x => x.ContractorInn));
            group.Columns.Add(helper.CreateTextColumn(x => x.ContractorKpp).Configure(c => c.DisableSort = true));
            setup.Columns.Add(group);

            _unconfirmedView.InitColumns(setup);
        }

        public void UnconfirmedInvoiceLoading()
        {
            _unconfirmedView.IsProgressVisible = true;
        }

        public void UnconfirmedInvoiceLoaded()
        {
            _unconfirmedView.IsProgressVisible = false;
        }

        # endregion

        #region Подтвержденные сведения

        private readonly ConfirmedInvoiceGridView _confirmedGridView =
            new ConfirmedInvoiceGridView();

        private void InitConfirmedInvoices()
        {
            _confirmedContainer
                .WithGrid(_confirmedGridView)
                .WithPager(_confirmedGridPager)
                .WithGridResetter(new GridColumnsVisibilityResetter(_confirmedGridView))
                .WithGridResetter(new GridColumnSortResetter(_confirmedGridView))
                .WithGridResetter(new GridColumnFilterResetter(_confirmedGridView))
                .WithColumnChooser(
                    new GridColumnChooserView(
                        ChapterColumnChooserWidth,
                        ChapterColumnChooserHeigth));
        }
        
        public IExtendedGridView ConfirmedInvoicesGridView
        {
            get
            {
                return _confirmedGridView;
            }
        }

        private readonly GridPagerView _confirmedGridPager =
            new GridPagerView();

        public IGridPagerView ConfirmedPager
        {
            get
            {
                return _confirmedGridPager;
            }
        }

        #endregion
    }
}
