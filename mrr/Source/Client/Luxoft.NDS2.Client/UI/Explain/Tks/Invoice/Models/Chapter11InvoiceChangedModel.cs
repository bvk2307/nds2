﻿using Luxoft.NDS2.Common.Contracts.DTO.Explain;

namespace Luxoft.NDS2.Client.UI.Explain.Tks.Invoice.Models
{
    public class Chapter11InvoiceChangedModel : InvoiceChangedModel<ExplainTksInvoiceChapter11>
    {
        public Chapter11InvoiceChangedModel(ExplainTksInvoiceChapter11 data)
            : base(data)
        {
        }

        public string SellerInn
        {
            get
            {
                return _data.NewSellerInn;
            }
        }

        public string SellerKpp
        {
            get
            {
                return _data.NewSellerKpp;
            }
        }

        public string BrokerInn
        {
            get
            {
                return _data.NewBrokerInn;
            }
        }

        public string BrokerKpp
        {
            get
            {
                return _data.NewBrokerKpp;
            }
        }

        public string DealCode
        {
            get
            {
                return _data.NewDealCode;
            }
        }

        public decimal? Amount
        {
            get
            {
                return _data.NewAmount;
            }
        }
    }
}
