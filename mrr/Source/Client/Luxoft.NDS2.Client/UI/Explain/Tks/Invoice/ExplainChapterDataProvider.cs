﻿using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Services;
using System;

namespace Luxoft.NDS2.Client.UI.Explain.Tks.Invoice
{
    public class ExplainChapterDataProvider<TInvoice> : 
        ServiceProxyBase<PageResult<TInvoice>>,
        IServerGridDataProvider<PageResult<TInvoice>>
    {
        private readonly Func<QueryConditions, OperationResult<PageResult<TInvoice>>> _dataLoader;

        public ExplainChapterDataProvider(
            INotifier notifier,
            IClientLogger logger,
            Func<QueryConditions, OperationResult<PageResult<TInvoice>>> dataLoader)
            : base(notifier, logger)
        {
            _dataLoader = dataLoader;
        }        

        public void BeginDataLoad(QueryConditions queryConditions)
        {
            BeginInvoke(() => _dataLoader(queryConditions));
        }

        protected override PageResult<TInvoice> FailureResult()
        {
            return new PageResult<TInvoice>();
        }

        public event EventHandler<DataLoadedEventArgs<PageResult<TInvoice>>> DataLoaded;

        protected override void CallBack(PageResult<TInvoice> result)
        {
            if (DataLoaded != null)
            {
                DataLoaded(this, new DataLoadedEventArgs<PageResult<TInvoice>>(result));
            }
        }
        
    }
}
