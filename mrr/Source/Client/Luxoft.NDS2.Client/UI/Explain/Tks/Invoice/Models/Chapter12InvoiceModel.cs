﻿using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using System;

namespace Luxoft.NDS2.Client.UI.Explain.Tks.Invoice.Models
{
    public class Chapter12InvoiceModel : ExplainInvoiceModel<ExplainTksInvoiceChapter12, Chapter12InvoiceChangedModel>
    {
        public Chapter12InvoiceModel(ExplainTksInvoiceChapter12 data)
            : base(data)
        { 
        }

        protected override Chapter12InvoiceChangedModel ChangedState()
        {
            return new Chapter12InvoiceChangedModel(_data);
        }

        public string BuyerInn
        {
            get
            {
                return _data.BuyerInn;
            }
        }

        public string BuyerKpp
        {
            get
            {
                return _data.BuyerKpp;
            }
        }

        public decimal? Amount
        {
            get
            {
                return _data.Amount;
            }
        }

        public decimal? AmountNoNds
        {
            get
            {
                return _data.AmountNoNds;
            }
        }
    }
}
