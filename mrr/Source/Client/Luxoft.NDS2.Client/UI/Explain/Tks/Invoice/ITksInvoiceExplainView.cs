﻿using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Sur;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Explain.Tks.Invoice
{
    public enum ExplainCardTab { Chapter8, Chapter9, Chapter10, Chapter11, Chapter12, Unspecified };

    public interface ITksInvoiceExplainView : IExplainDetailsView
    {
        IUnconfirmedInvoicesView UnconfirmedInvoices 
        { 
            get; 
        }

        void UnconfirmedInvoiceLoading();

        void UnconfirmedInvoiceLoaded();

        void InitUnconfirmedInvoices();

        IExtendedGridView Chapter8Grid
        {
            get;
        }

        IExtendedGridView Chapter9Grid
        {
            get;
        }

        IExtendedGridView Chapter10Grid
        {
            get;
        }

        IExtendedGridView Chapter11Grid
        {
            get;
        }

        IExtendedGridView Chapter12Grid
        {
            get;
        }

        IGridPagerView Chapter8Pager
        {
            get;
        }

        IGridPagerView Chapter9Pager
        {
            get;
        }

        IGridPagerView Chapter10Pager
        {
            get;
        }

        IGridPagerView Chapter11Pager
        {
            get;
        }

        IGridPagerView Chapter12Pager
        {
            get;
        }

        IExtendedGridView ConfirmedInvoicesGridView
        {
            get;
        }

        IGridPagerView ConfirmedPager { get; }

        event EventHandler ChapterChanged;

        ExplainCardTab CurrentChapter
        {
            get;
        }

    }
}
