﻿using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Explain.Tks.Invoice.Models
{
    public abstract class AddListFilterConverter : IColumnFilterConverter
    {
        private readonly AskInvoiceChapterNumber _true;

        private readonly AskInvoiceChapterNumber _false;

        protected AddListFilterConverter(AskInvoiceChapterNumber trueValue, AskInvoiceChapterNumber falseValue)
        {
            _true = trueValue;
            _false = falseValue;
        }

        public void Convert(FilterQuery columnFilter)
        {
            if (!columnFilter.Filtering.Any())
            {
                return;
            }
            
            columnFilter.ColumnName =
                TypeHelper<IExplainTksBookInvoice>
                    .GetMemberName(x => x.Chapter);
            columnFilter.ColumnType = typeof(int);

            var value = (bool)columnFilter.Filtering[0].Value;

            columnFilter.Filtering[0].Value = value ? _true : _false;
        }
    }

    public class Chapter8AddListFilterConverter : AddListFilterConverter
    {
        public Chapter8AddListFilterConverter()
            : base(AskInvoiceChapterNumber.Chapter81, AskInvoiceChapterNumber.Chapter8)
        {
        }
    }

    public class Chapter9AddListFilterConverter : AddListFilterConverter
    {
        public Chapter9AddListFilterConverter()
            : base(AskInvoiceChapterNumber.Chapter91, AskInvoiceChapterNumber.Chapter9)
        {
        }
    }
}
