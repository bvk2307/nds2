﻿using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using System;

namespace Luxoft.NDS2.Client.UI.Explain.Tks.Invoice.Models
{
    public abstract class ExplainInvoiceChangedModel<TDto> where TDto : IExplainTksInvoiceBase
    {
        protected readonly TDto _data;

        protected ExplainInvoiceChangedModel(TDto data)
        {
            _data = data;
        }

        public string InvoiceNumber
        {
            get 
            {
                return _data.NewInvoiceNumber; 
            }
        }

        public DateTime? InvoiceDate
        {
            get 
            { 
                return _data.NewInvoiceDate; 
            }
        }

        public string OkvCode
        {
            get
            {
                return _data.NewOkvCode;
            }
        }
    }
}
