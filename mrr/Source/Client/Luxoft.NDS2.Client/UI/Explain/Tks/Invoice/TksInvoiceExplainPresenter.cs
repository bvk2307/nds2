﻿using CommonComponents.Utils.Async;
using FLS.CommonComponents.App.Execution;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Base.Navigation;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.Services;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Explain.Tks.Invoice
{
    public class TksInvoiceExplainPresenter : ExplainDetailsPresenterBase
    {
        # region .ctor

        public TksInvoiceExplainPresenter(
            ITksInvoiceExplainView view,
            TksInvoiceExplainModel viewModel,
            IClaimCardOpener claimCardOpener,
            ITaxPayerDetails taxpayerOpener,
            IExplainTksInvoiceService tksService,
            IExplainCommentUpdater commentUpdater,
            Func<string, ISettingsProvider> createProvider,
            IClientLogger logger)
            : base(view, viewModel, claimCardOpener, taxpayerOpener, commentUpdater, logger)
        {
            _view = view;
            _tksService = tksService;
            _createProvider = createProvider;

            InitSubPresenters(viewModel);

            InitView();
            BindView();            
        }

        # endregion

        # region View

        private readonly ITksInvoiceExplainView _view;

        private void InitView()
        {
            _view.InitUnconfirmedInvoices();
        }

        private void BindView()
        {
            _view.Opened += OnViewLoaded;
            _view.ChapterChanged += TabChanged;
        }

        private void OnViewLoaded(object sender, EventArgs e)
        {
            var chapter8 = _chapterGridPresenters[ExplainCardTab.Chapter8];

            if (!chapter8.IsLoaded)
            {
                chapter8.BeginLoad();
            }
        }

        private void TabChanged(object sender, EventArgs e)
        {
            var gridPresenter = _chapterGridPresenters[_view.CurrentChapter];

            if (!gridPresenter.IsLoaded)
            {
                gridPresenter.BeginLoad();

                if (_view.CurrentChapter == ExplainCardTab.Unspecified)
                {
                    _unconfirmedPresenter.Load();
                }
            }
        }

        # endregion        

        # region Разделы

        private readonly IExplainTksInvoiceService _tksService;

        private const string SettingsKey = "ExplainReply.FormalExplainInvoice.{0}";

        private readonly Func<string, ISettingsProvider> _createProvider; 

        private DataGridPresenter<ExplainTksInvoiceUnconfirmed> _unconfirmedPresenter;

        private readonly Dictionary<ExplainCardTab, IGridViewPresenter> _chapterGridPresenters =
            new Dictionary<ExplainCardTab, IGridViewPresenter>();

        private void InitSubPresenters(TksInvoiceExplainModel viewModel)
        {
            var gridPresentersCreator = new ExplainChapterGridPresenterCreator(_view, viewModel, _logger, _tksService);
            _chapterGridPresenters[ExplainCardTab.Chapter8] = gridPresentersCreator.Chapter8();
            _chapterGridPresenters[ExplainCardTab.Chapter9] = gridPresentersCreator.Chapter9();
            _chapterGridPresenters[ExplainCardTab.Chapter10] = gridPresentersCreator.Chapter10();
            _chapterGridPresenters[ExplainCardTab.Chapter11] = gridPresentersCreator.Chapter11();
            _chapterGridPresenters[ExplainCardTab.Chapter12] = gridPresentersCreator.Chapter12();
            _chapterGridPresenters[ExplainCardTab.Unspecified] = gridPresentersCreator.ConfirmedInvoices();

            var qc = new QueryConditions
            {
                Sorting = new List<ColumnSort> { new ColumnSort
                {
                    ColumnKey = TypeHelper<ExplainTksInvoiceUnconfirmed>.GetMemberName(t => t.ContractorInn),
                    Order = ColumnSort.SortOrder.Asc
                } }
            };

            _unconfirmedPresenter = 
                new PagedDataGridPresenter<ExplainTksInvoiceUnconfirmed>(
                    _view.UnconfirmedInvoices.GridView, 
                    _view.UnconfirmedInvoices.Pager, 
                    GetUnconfirmedInvoices, 
                    qc);

            var settingsProvider = _createProvider(string.Format(SettingsKey, "ConfirmedInvoices"));

            _unconfirmedPresenter.Init(settingsProvider);
        }        

        private PageResult<ExplainTksInvoiceUnconfirmed> GetUnconfirmedInvoices(QueryConditions qc)
        {
            UIThreadExecutor.CurrentDispatcher.BeginInvoke(new Action(() => _view.UnconfirmedInvoiceLoading()));

            var data = _tksService.FindUnconfirmedInvoices(_model.ExplainZip, qc);

            UIThreadExecutor.CurrentDispatcher.BeginInvoke(new Action(() => _view.UnconfirmedInvoiceLoaded()));

            if (data.Status != ResultStatus.Success)
                _view.ShowError(data.Message);

            return new PageResult<ExplainTksInvoiceUnconfirmed>
            {
                Rows = data.Result.Rows,
                TotalMatches = data.Result.TotalMatches
            };
        }

        # endregion       
    }
}
