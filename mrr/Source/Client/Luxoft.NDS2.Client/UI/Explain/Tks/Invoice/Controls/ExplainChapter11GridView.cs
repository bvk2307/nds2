﻿using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Client.UI.Explain.Tks.Invoice.Models;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using Luxoft.NDS2.Common.Contracts.Helpers;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Explain.Tks.Invoice.Controls
{
    public partial class ExplainChapter11GridView : UserControl, IExtendedGridView
    {
        # region Constructor

        public ExplainChapter11GridView()
        {
            InitializeComponent();
            FilterUIProviderLocalizer.Localize();

            GridEvents = new UltraGridEventsHandler(_grid);            
            Columns = 
                new GridColumnsCollection(
                    null,
                    _grid.DisplayLayout.Bands[0].Groups.AsEnumerable(),
                    _grid.DisplayLayout.Bands[0].Columns.AsEnumerable(new [] { "Changes" }),
                    new GridMultiBandColumnCreator(new List<ColumnsCollection> { _grid.DisplayLayout.Bands[1].Columns})
                        .WithFilterViewFor("OperationCodes", (column) => new OperationCodesColumnFilterView(column))
                        .WithFilterViewFor("Status", (column) => new StatusColumnFilterView(column)));            

            ApplyFormatters();
        }

        private void ApplyFormatters()
        {
            Columns
                .FindByKey(TypeHelper<Chapter11InvoiceModel>.GetMemberName(x => x.Status))
                .ApplyFormatter(
                    new ImageColumnFormatter<ExplainTksInvoiceState>(InvoiceStatusDictionary.StatusIcons, this))
                .ApplyFormatter(
                    new ToolTipFormatter<Chapter11InvoiceModel>(model => InvoiceStatusDictionary.GetTooltip(model.Status), _grid)); ;
            
            Columns
                .FindByKey(TypeHelper<Chapter11InvoiceModel>.GetMemberName(x => x.Amount))
                .ApplyFormatter(new ValueColumnFormatter<Chapter11InvoiceModel>(
                    new IntValueZeroFormatter(),
                    "{0: 0,0.0}"));
        }

        # endregion

        # region IGridView

        public IGridColumnsCollection Columns
        {
            get;
            private set;
        }

        public void SetDataSource(object dataSource)
        {
            _bindingSource.DataSource = dataSource;
        }

        public IGridEventsHandler GridEvents
        {
            get;
            private set;
        }

        # endregion
    }
}
