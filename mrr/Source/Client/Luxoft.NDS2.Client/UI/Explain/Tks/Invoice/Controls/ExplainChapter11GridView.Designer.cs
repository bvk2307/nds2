﻿namespace Luxoft.NDS2.Client.UI.Explain.Tks.Invoice.Controls
{
    partial class ExplainChapter11GridView
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand1 = new Infragistics.Win.UltraWinGrid.UltraGridBand("ListItems", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn73 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SellerInn");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn74 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SellerKpp");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn75 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("BrokerInn");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn76 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("BrokerKpp");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn77 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DealCode");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn78 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Amount");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn79 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("OrdinalNumber");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn80 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("OperationCodes");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn81 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ChangeNumber");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn82 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ChangeDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn83 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("CorrectionNumber");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn84 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("CorrectionDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn85 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ChangeCorrectionNumber");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn86 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ChangeCorrectionDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn87 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Changes");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn88 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Status");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn89 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("InvoiceNumber");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn90 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("InvoiceDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn91 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("OkvCode");
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup1 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup0", 104471031);
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup2 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup1", 104471032);
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup3 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup2", 104471033);
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup4 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup3", 104471034);
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup5 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup4", 104471035);
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup6 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup5", 104471036);
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand2 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Changes", 0);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn92 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SellerInn");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn93 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SellerKpp");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn94 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("BrokerInn");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn95 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("BrokerKpp");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn96 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DealCode");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn97 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Amount");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn98 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("OrdinalNumber");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn99 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("OperationCodes");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn100 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ChangeNumber");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn101 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ChangeDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn102 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("CorrectionNumber");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn103 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("CorrectionDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn104 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ChangeCorrectionNumber");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn105 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ChangeCorrectionDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn106 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("InvoiceNumber");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn107 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("InvoiceDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn108 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("OkvCode");
            this._grid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this._bindingSource = new System.Windows.Forms.BindingSource(this.components);
            this._filterUIProvider = new Infragistics.Win.SupportDialogs.FilterUIProvider.UltraGridFilterUIProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this._grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._bindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // _grid
            // 
            this._grid.DataSource = this._bindingSource;
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this._grid.DisplayLayout.Appearance = appearance1;
            ultraGridColumn73.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn73.Header.Caption = "ИНН";
            ultraGridColumn73.Header.ToolTipText = "ИНН Продавца";
            ultraGridColumn73.Header.VisiblePosition = 3;
            ultraGridColumn73.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn73.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn73.RowLayoutColumnInfo.ParentGroupIndex = 4;
            ultraGridColumn73.RowLayoutColumnInfo.ParentGroupKey = "NewGroup4";
            ultraGridColumn73.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(100, 0);
            ultraGridColumn73.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 45);
            ultraGridColumn73.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn73.RowLayoutColumnInfo.SpanY = 1;
            ultraGridColumn73.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn74.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn74.Header.Caption = "КПП";
            ultraGridColumn74.Header.ToolTipText = "КПП Продавца";
            ultraGridColumn74.Header.VisiblePosition = 4;
            ultraGridColumn74.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn74.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn74.RowLayoutColumnInfo.ParentGroupIndex = 4;
            ultraGridColumn74.RowLayoutColumnInfo.ParentGroupKey = "NewGroup4";
            ultraGridColumn74.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn74.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 45);
            ultraGridColumn74.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn74.RowLayoutColumnInfo.SpanY = 1;
            ultraGridColumn74.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn75.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn75.Header.Caption = "ИНН";
            ultraGridColumn75.Header.ToolTipText = "ИНН субкомиссионера (субагента)";
            ultraGridColumn75.Header.VisiblePosition = 1;
            ultraGridColumn75.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn75.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn75.RowLayoutColumnInfo.ParentGroupIndex = 5;
            ultraGridColumn75.RowLayoutColumnInfo.ParentGroupKey = "NewGroup5";
            ultraGridColumn75.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 45);
            ultraGridColumn75.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn75.RowLayoutColumnInfo.SpanY = 1;
            ultraGridColumn75.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn76.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn76.Header.Caption = "КПП";
            ultraGridColumn76.Header.ToolTipText = "КПП субкомиссионера (субагента)";
            ultraGridColumn76.Header.VisiblePosition = 0;
            ultraGridColumn76.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn76.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn76.RowLayoutColumnInfo.ParentGroupIndex = 5;
            ultraGridColumn76.RowLayoutColumnInfo.ParentGroupKey = "NewGroup5";
            ultraGridColumn76.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 45);
            ultraGridColumn76.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn76.RowLayoutColumnInfo.SpanY = 1;
            ultraGridColumn76.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn77.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn77.Header.Caption = "Код вида сделки";
            ultraGridColumn77.Header.ToolTipText = "Код вида сделки";
            ultraGridColumn77.Header.VisiblePosition = 2;
            ultraGridColumn77.RowLayoutColumnInfo.OriginX = 30;
            ultraGridColumn77.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn77.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(50, 0);
            ultraGridColumn77.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 90);
            ultraGridColumn77.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn77.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn77.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn78.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn78.Header.Caption = "Стоимость товаров";
            ultraGridColumn78.Header.ToolTipText = "Стоимость товаров (работ, услуг), имущественных прав по счету-фактура";
            ultraGridColumn78.Header.VisiblePosition = 5;
            ultraGridColumn78.RowLayoutColumnInfo.OriginX = 34;
            ultraGridColumn78.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn78.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn78.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 90);
            ultraGridColumn78.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn78.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn79.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn79.Header.Caption = "№";
            ultraGridColumn79.Header.ToolTipText = "Порядковый номер";
            ultraGridColumn79.Header.VisiblePosition = 7;
            ultraGridColumn79.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn79.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn79.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(50, 0);
            ultraGridColumn79.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 33);
            ultraGridColumn79.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn79.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn80.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn80.Header.Caption = "Код вида операции";
            ultraGridColumn80.Header.ToolTipText = "Код вида операции";
            ultraGridColumn80.Header.VisiblePosition = 9;
            ultraGridColumn80.RowLayoutColumnInfo.OriginX = 4;
            ultraGridColumn80.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn80.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn80.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn80.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn80.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn81.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn81.Header.Caption = "№";
            ultraGridColumn81.Header.ToolTipText = "Номер исправления счета-фактуры";
            ultraGridColumn81.Header.VisiblePosition = 11;
            ultraGridColumn81.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn81.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn81.RowLayoutColumnInfo.ParentGroupIndex = 1;
            ultraGridColumn81.RowLayoutColumnInfo.ParentGroupKey = "NewGroup1";
            ultraGridColumn81.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn81.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 33);
            ultraGridColumn81.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn81.RowLayoutColumnInfo.SpanY = 1;
            ultraGridColumn81.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn82.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn82.Header.Caption = "Дата";
            ultraGridColumn82.Header.ToolTipText = "Дата исправления счета-фактуры";
            ultraGridColumn82.Header.VisiblePosition = 13;
            ultraGridColumn82.RowLayoutColumnInfo.MinimumLabelSize = new System.Drawing.Size(80, 0);
            ultraGridColumn82.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn82.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn82.RowLayoutColumnInfo.ParentGroupIndex = 1;
            ultraGridColumn82.RowLayoutColumnInfo.ParentGroupKey = "NewGroup1";
            ultraGridColumn82.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn82.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn82.RowLayoutColumnInfo.SpanY = 1;
            ultraGridColumn83.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn83.Header.Caption = "№";
            ultraGridColumn83.Header.ToolTipText = "Номер корректировки счета-фактуры";
            ultraGridColumn83.Header.VisiblePosition = 14;
            ultraGridColumn83.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn83.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn83.RowLayoutColumnInfo.ParentGroupIndex = 2;
            ultraGridColumn83.RowLayoutColumnInfo.ParentGroupKey = "NewGroup2";
            ultraGridColumn83.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn83.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn83.RowLayoutColumnInfo.SpanY = 1;
            ultraGridColumn83.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn84.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn84.Header.Caption = "Дата";
            ultraGridColumn84.Header.ToolTipText = "Дата корректировки счета-фактуры";
            ultraGridColumn84.Header.VisiblePosition = 15;
            ultraGridColumn84.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn84.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn84.RowLayoutColumnInfo.ParentGroupIndex = 2;
            ultraGridColumn84.RowLayoutColumnInfo.ParentGroupKey = "NewGroup2";
            ultraGridColumn84.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn84.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 20);
            ultraGridColumn84.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn84.RowLayoutColumnInfo.SpanY = 1;
            ultraGridColumn85.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn85.Header.Caption = "№";
            ultraGridColumn85.Header.ToolTipText = "Номер исправления корректировочного счета-фактуры";
            ultraGridColumn85.Header.VisiblePosition = 16;
            ultraGridColumn85.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn85.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn85.RowLayoutColumnInfo.ParentGroupIndex = 3;
            ultraGridColumn85.RowLayoutColumnInfo.ParentGroupKey = "NewGroup3";
            ultraGridColumn85.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn85.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 20);
            ultraGridColumn85.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn85.RowLayoutColumnInfo.SpanY = 1;
            ultraGridColumn85.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn86.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn86.Header.Caption = "Дата";
            ultraGridColumn86.Header.ToolTipText = "Дата исправления корректировочного счета-фактуры";
            ultraGridColumn86.Header.VisiblePosition = 17;
            ultraGridColumn86.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn86.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn86.RowLayoutColumnInfo.ParentGroupIndex = 3;
            ultraGridColumn86.RowLayoutColumnInfo.ParentGroupKey = "NewGroup3";
            ultraGridColumn86.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn86.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 20);
            ultraGridColumn86.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn86.RowLayoutColumnInfo.SpanY = 1;
            ultraGridColumn87.Header.VisiblePosition = 18;
            ultraGridColumn88.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn88.Header.Caption = "";
            ultraGridColumn88.Header.ToolTipText = "Признак пояснения записи";
            ultraGridColumn88.Header.VisiblePosition = 12;
            ultraGridColumn88.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn88.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn88.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(20, 0);
            ultraGridColumn88.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 33);
            ultraGridColumn88.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn88.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn89.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn89.Header.Caption = "№";
            ultraGridColumn89.Header.ToolTipText = "Номер счета-фактуры продавца";
            ultraGridColumn89.Header.VisiblePosition = 8;
            ultraGridColumn89.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn89.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn89.RowLayoutColumnInfo.ParentGroupIndex = 0;
            ultraGridColumn89.RowLayoutColumnInfo.ParentGroupKey = "NewGroup0";
            ultraGridColumn89.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn89.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn89.RowLayoutColumnInfo.SpanY = 1;
            ultraGridColumn89.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn90.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn90.Header.Caption = "Дата";
            ultraGridColumn90.Header.ToolTipText = "Дата счета-фактуры продавца";
            ultraGridColumn90.Header.VisiblePosition = 10;
            ultraGridColumn90.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn90.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn90.RowLayoutColumnInfo.ParentGroupIndex = 0;
            ultraGridColumn90.RowLayoutColumnInfo.ParentGroupKey = "NewGroup0";
            ultraGridColumn90.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn90.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn90.RowLayoutColumnInfo.SpanY = 1;
            ultraGridColumn91.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn91.Header.Caption = "Код валюты";
            ultraGridColumn91.Header.ToolTipText = "Код валюты по ОКВ";
            ultraGridColumn91.Header.VisiblePosition = 6;
            ultraGridColumn91.RowLayoutColumnInfo.OriginX = 32;
            ultraGridColumn91.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn91.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(50, 0);
            ultraGridColumn91.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 90);
            ultraGridColumn91.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn91.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn91.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridBand1.Columns.AddRange(new object[] {
            ultraGridColumn73,
            ultraGridColumn74,
            ultraGridColumn75,
            ultraGridColumn76,
            ultraGridColumn77,
            ultraGridColumn78,
            ultraGridColumn79,
            ultraGridColumn80,
            ultraGridColumn81,
            ultraGridColumn82,
            ultraGridColumn83,
            ultraGridColumn84,
            ultraGridColumn85,
            ultraGridColumn86,
            ultraGridColumn87,
            ultraGridColumn88,
            ultraGridColumn89,
            ultraGridColumn90,
            ultraGridColumn91});
            ultraGridGroup1.Header.Caption = "СФ";
            ultraGridGroup1.Key = "NewGroup0";
            ultraGridGroup1.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup1.RowLayoutGroupInfo.OriginX = 6;
            ultraGridGroup1.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup1.RowLayoutGroupInfo.SpanX = 4;
            ultraGridGroup1.RowLayoutGroupInfo.SpanY = 2;
            ultraGridGroup2.Header.Caption = "ИСФ";
            ultraGridGroup2.Key = "NewGroup1";
            ultraGridGroup2.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup2.RowLayoutGroupInfo.OriginX = 10;
            ultraGridGroup2.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup2.RowLayoutGroupInfo.SpanX = 4;
            ultraGridGroup2.RowLayoutGroupInfo.SpanY = 2;
            ultraGridGroup3.Header.Caption = "КСФ";
            ultraGridGroup3.Key = "NewGroup2";
            ultraGridGroup3.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup3.RowLayoutGroupInfo.OriginX = 14;
            ultraGridGroup3.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup3.RowLayoutGroupInfo.SpanX = 4;
            ultraGridGroup3.RowLayoutGroupInfo.SpanY = 2;
            ultraGridGroup4.Header.Caption = "ИКСФ";
            ultraGridGroup4.Key = "NewGroup3";
            ultraGridGroup4.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup4.RowLayoutGroupInfo.OriginX = 18;
            ultraGridGroup4.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup4.RowLayoutGroupInfo.SpanX = 4;
            ultraGridGroup4.RowLayoutGroupInfo.SpanY = 2;
            ultraGridGroup5.Header.Caption = "Сведения о продавце";
            ultraGridGroup5.Key = "NewGroup4";
            ultraGridGroup5.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup5.RowLayoutGroupInfo.OriginX = 22;
            ultraGridGroup5.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup5.RowLayoutGroupInfo.SpanX = 4;
            ultraGridGroup5.RowLayoutGroupInfo.SpanY = 2;
            ultraGridGroup6.Header.Caption = "Сведения о субкомиссионере";
            ultraGridGroup6.Key = "NewGroup5";
            ultraGridGroup6.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup6.RowLayoutGroupInfo.OriginX = 26;
            ultraGridGroup6.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup6.RowLayoutGroupInfo.SpanX = 4;
            ultraGridGroup6.RowLayoutGroupInfo.SpanY = 2;
            ultraGridBand1.Groups.AddRange(new Infragistics.Win.UltraWinGrid.UltraGridGroup[] {
            ultraGridGroup1,
            ultraGridGroup2,
            ultraGridGroup3,
            ultraGridGroup4,
            ultraGridGroup5,
            ultraGridGroup6});
            ultraGridBand1.RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.GroupLayout;
            ultraGridBand2.ColHeadersVisible = false;
            ultraGridColumn92.Header.VisiblePosition = 0;
            ultraGridColumn92.RowLayoutColumnInfo.OriginX = 20;
            ultraGridColumn92.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn92.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(100, 0);
            ultraGridColumn92.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn92.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn93.Header.VisiblePosition = 2;
            ultraGridColumn93.RowLayoutColumnInfo.OriginX = 22;
            ultraGridColumn93.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn93.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn93.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn93.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn94.Header.VisiblePosition = 1;
            ultraGridColumn94.RowLayoutColumnInfo.OriginX = 24;
            ultraGridColumn94.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(100, 0);
            ultraGridColumn95.Header.VisiblePosition = 3;
            ultraGridColumn95.RowLayoutColumnInfo.OriginX = 26;
            ultraGridColumn96.Header.VisiblePosition = 4;
            ultraGridColumn96.RowLayoutColumnInfo.OriginX = 28;
            ultraGridColumn96.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(50, 0);
            ultraGridColumn97.Header.VisiblePosition = 5;
            ultraGridColumn97.RowLayoutColumnInfo.OriginX = 32;
            ultraGridColumn97.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn97.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn97.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn97.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn98.Header.VisiblePosition = 8;
            ultraGridColumn98.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn98.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn98.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(50, 0);
            ultraGridColumn98.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn98.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn99.Header.VisiblePosition = 10;
            ultraGridColumn99.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn99.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn99.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn99.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn99.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn100.Header.VisiblePosition = 11;
            ultraGridColumn100.RowLayoutColumnInfo.OriginX = 8;
            ultraGridColumn100.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn100.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn100.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn100.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn101.Header.VisiblePosition = 12;
            ultraGridColumn101.RowLayoutColumnInfo.OriginX = 10;
            ultraGridColumn101.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn101.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn101.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn101.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn102.Header.VisiblePosition = 13;
            ultraGridColumn102.RowLayoutColumnInfo.OriginX = 12;
            ultraGridColumn102.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn102.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn102.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn102.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn103.Header.VisiblePosition = 14;
            ultraGridColumn103.RowLayoutColumnInfo.OriginX = 14;
            ultraGridColumn103.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn103.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn103.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn103.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn104.Header.VisiblePosition = 15;
            ultraGridColumn104.RowLayoutColumnInfo.OriginX = 16;
            ultraGridColumn104.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn104.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn104.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn104.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn105.Header.VisiblePosition = 16;
            ultraGridColumn105.RowLayoutColumnInfo.OriginX = 18;
            ultraGridColumn105.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn105.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn105.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn105.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn106.Header.VisiblePosition = 6;
            ultraGridColumn106.RowLayoutColumnInfo.OriginX = 4;
            ultraGridColumn106.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn106.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn106.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn106.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn107.Header.VisiblePosition = 7;
            ultraGridColumn107.RowLayoutColumnInfo.OriginX = 6;
            ultraGridColumn107.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn107.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn107.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn107.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn108.Header.VisiblePosition = 9;
            ultraGridColumn108.RowLayoutColumnInfo.OriginX = 30;
            ultraGridColumn108.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn108.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(50, 0);
            ultraGridColumn108.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn108.RowLayoutColumnInfo.SpanY = 2;
            ultraGridBand2.Columns.AddRange(new object[] {
            ultraGridColumn92,
            ultraGridColumn93,
            ultraGridColumn94,
            ultraGridColumn95,
            ultraGridColumn96,
            ultraGridColumn97,
            ultraGridColumn98,
            ultraGridColumn99,
            ultraGridColumn100,
            ultraGridColumn101,
            ultraGridColumn102,
            ultraGridColumn103,
            ultraGridColumn104,
            ultraGridColumn105,
            ultraGridColumn106,
            ultraGridColumn107,
            ultraGridColumn108});
            ultraGridBand2.GroupHeadersVisible = false;
            ultraGridBand2.RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.GroupLayout;
            this._grid.DisplayLayout.BandsSerializer.Add(ultraGridBand1);
            this._grid.DisplayLayout.BandsSerializer.Add(ultraGridBand2);
            this._grid.DisplayLayout.InterBandSpacing = 0;
            this._grid.DisplayLayout.Override.AllowColMoving = Infragistics.Win.UltraWinGrid.AllowColMoving.NotAllowed;
            this._grid.DisplayLayout.Override.AllowColSizing = Infragistics.Win.UltraWinGrid.AllowColSizing.Free;
            this._grid.DisplayLayout.Override.AllowColSwapping = Infragistics.Win.UltraWinGrid.AllowColSwapping.NotAllowed;
            this._grid.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            this._grid.DisplayLayout.Override.ExpansionIndicator = Infragistics.Win.UltraWinGrid.ShowExpansionIndicator.CheckOnDisplay;
            this._grid.DisplayLayout.Override.FilterUIProvider = this._filterUIProvider;
            this._grid.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.ExternalSortSingle;
            this._grid.DisplayLayout.Override.HeaderPlacement = Infragistics.Win.UltraWinGrid.HeaderPlacement.FixedOnTop;
            this._grid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this._grid.DisplayLayout.Override.WrapHeaderText = Infragistics.Win.DefaultableBoolean.True;
            this._grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._grid.Location = new System.Drawing.Point(0, 0);
            this._grid.Name = "_grid";
            this._grid.Size = new System.Drawing.Size(804, 306);
            this._grid.TabIndex = 0;
            this._grid.UseAppStyling = false;
            // 
            // _bindingSource
            // 
            this._bindingSource.DataMember = "ListItems";
            this._bindingSource.DataSource = typeof(Luxoft.NDS2.Client.UI.Explain.Tks.Invoice.Models.Chapter11Model);
            // 
            // ExplainChapter11GridView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._grid);
            this.Name = "ExplainChapter11GridView";
            this.Size = new System.Drawing.Size(804, 306);
            ((System.ComponentModel.ISupportInitialize)(this._grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._bindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinGrid.UltraGrid _grid;
        private System.Windows.Forms.BindingSource _bindingSource;
        private Infragistics.Win.SupportDialogs.FilterUIProvider.UltraGridFilterUIProvider _filterUIProvider;
    }
}
