﻿using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Explain.Tks.Invoice.Models
{
    public abstract class ExplainInvoiceModel<TDto, TChangedModel> 
        where TDto : IExplainTksInvoiceBase
        where TChangedModel : ExplainInvoiceChangedModel<TDto>
    {
        private readonly TChangedModel[] _changedState;

        protected readonly TDto _data;

        protected ExplainInvoiceModel(TDto data)
        {
            _data = data;
            _changedState =
                _data.Status == ExplainTksInvoiceState.Changed
                    ? new TChangedModel[] { ChangedState() }
                    : new TChangedModel[0];
        }

        protected abstract TChangedModel ChangedState();

        public IList<TChangedModel> Changes
        {
            get
            {
                return _changedState;
            }
        }
        
        public ExplainTksInvoiceState Status
        {
            get
            {
                return _data.Status;
            }
        }

        public string InvoiceNumber
        {
            get 
            { 
                return _data.InvoiceNumber; 
            }
        }

        public DateTime? InvoiceDate
        {
            get 
            { 
                return _data.InvoiceDate; 
            }
        }

        public string OkvCode
        {
            get
            {
                return _data.OkvCode;
            }
        }
    }
}
