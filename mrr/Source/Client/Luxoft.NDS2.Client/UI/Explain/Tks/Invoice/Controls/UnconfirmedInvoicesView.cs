﻿using Luxoft.NDS2.Client.UI.Controls.Grid.V2;
using System.Windows.Forms;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination;

namespace Luxoft.NDS2.Client.UI.Explain.Tks.Invoice.Controls
{
    public partial class UnconfirmedInvoicesView : UserControl, IUnconfirmedInvoicesView
    {
        private const int _defaultPageSize = 50;
        private ExtendedDataGridPageNavigator _pageNavigator;

        public UnconfirmedInvoicesView()
        {
            InitializeComponent();

            Pager = new PagerStateViewModel()
            {
                PageSize = _defaultPageSize
            };

            _pageNavigator = new ExtendedDataGridPageNavigator(Pager);
        }

        public IDataGridView GridView { get { return _grid; } }

        public IGrid Grid { get { return _grid; } }

        public IPager Pager { get; private set; }

        public bool IsProgressVisible
        {
            set
            {
                _grid.PanelLoadingVisible = value;
            }
        }

        public void InitColumns(GridColumnSetup setup)
        {
            _grid
                .WithPager(_pageNavigator)
                .InitColumns(setup);
        }
    }
}
