﻿using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using System;

namespace Luxoft.NDS2.Client.UI.Explain.Tks.Invoice.Models
{
    public class Chapter10InvoiceChangedModel : InvoiceChangedModel<ExplainTksInvoiceChapter10>
    {
        public Chapter10InvoiceChangedModel(ExplainTksInvoiceChapter10 data)
            : base(data)
        {
        }

        public string BuyerInn
        {
            get
            {
                return _data.NewBuyerInn;
            }
        }

        public string BuyerKpp
        {
            get
            {
                return _data.NewBuyerKpp;
            }
        }

        public string SellerInn
        {
            get
            {
                return _data.NewSellerInn;
            }
        }

        public string SellerKpp
        {
            get
            {
                return _data.NewSellerKpp;
            }
        }

        public string AgencyInvoiceNumber
        {
            get
            {
                return _data.NewAgencyInvoiceNumber;
            }
        }

        public DateTime? AgencyInvoiceDate
        {
            get
            {
                return _data.NewAgencyInvoiceDate;
            }
        }

        public decimal? Amount
        {
            get
            {
                return _data.NewAmount;
            }
        }
    }
}
