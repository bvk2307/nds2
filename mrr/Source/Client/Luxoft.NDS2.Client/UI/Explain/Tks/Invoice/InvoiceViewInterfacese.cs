﻿using Luxoft.NDS2.Client.UI.Controls.Grid.V2;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination;

namespace Luxoft.NDS2.Client.UI.Explain.Tks.Invoice
{
    public interface IUnconfirmedInvoicesView
    {
        IDataGridView GridView { get; }
        IGrid Grid { get; }
        IPager Pager { get; }

        void InitColumns(GridColumnSetup setup);
    }

    public interface IConfirmedInvoicesView
    {
        IDataGridView GridView { get; }
        IGrid Grid { get; }
        IPager Pager { get; }

        void InitColumns(GridColumnSetup setup);
    }

    public interface IInvoiceChapterView
    {
        IDataGridView GridView { get; }
        IPager Pager { get; }
    }
}
