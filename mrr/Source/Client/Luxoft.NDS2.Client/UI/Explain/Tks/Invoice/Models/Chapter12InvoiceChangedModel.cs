﻿using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using System;

namespace Luxoft.NDS2.Client.UI.Explain.Tks.Invoice.Models
{
    public class Chapter12InvoiceChangedModel : ExplainInvoiceChangedModel<ExplainTksInvoiceChapter12>
    {
        public Chapter12InvoiceChangedModel(ExplainTksInvoiceChapter12 data)
            : base(data)
        {
        }

        public string BuyerInn
        {
            get
            {
                return _data.NewBuyerInn;
            }
        }

        public string BuyerKpp
        {
            get
            {
                return _data.NewBuyerKpp;
            }
        }

        public decimal? Amount
        {
            get
            {
                return _data.NewAmount;
            }
        }

        public decimal? AmountNoNds
        {
            get
            {
                return _data.NewAmountNoNds;
            }
        }
    }
}
