﻿namespace Luxoft.NDS2.Client.UI.Explain.Tks.Invoice.Controls
{
    partial class ExplainChapter9GridView
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand1 = new Infragistics.Win.UltraWinGrid.UltraGridBand("ListItems", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn42 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("BuyerInn");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn43 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("BuyerKpp");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn44 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SalesAmount");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn45 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SalesAmountRur");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn46 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("AddList");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn47 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ReceiveDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn48 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ReceiptDocumentsAll");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn49 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("BrokerInn");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn50 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("BrokerKpp");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn51 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("OrdinalNumber");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn52 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("OperationCodes");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn53 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ChangeNumber");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn54 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ChangeDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn55 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("CorrectionNumber");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn56 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("CorrectionDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn57 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ChangeCorrectionNumber");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn58 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ChangeCorrectionDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn59 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Changes");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn60 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Status");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn61 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("InvoiceNumber");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn62 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("InvoiceDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn63 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("OkvCode");
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup1 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup0", 103636457);
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup2 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup1", 103636458);
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup3 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup2", 103636459);
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup4 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup3", 103636460);
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup5 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup4", 103636461);
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup6 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup5", 103636462);
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup7 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup6", 103636463);
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand2 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Changes", 0);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn64 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("BuyerInn");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn65 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("BuyerKpp");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn66 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SalesAmount");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn67 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SalesAmountRur");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn68 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ReceiveDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn69 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ReceiptDocumentsAll");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn70 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("BrokerInn");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn71 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("BrokerKpp");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn72 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("OrdinalNumber");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn73 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("OperationCodes");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn74 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ChangeNumber");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn75 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ChangeDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn76 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("CorrectionNumber");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn77 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("CorrectionDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn78 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ChangeCorrectionNumber");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn79 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ChangeCorrectionDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn80 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("InvoiceNumber");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn81 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("InvoiceDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn82 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("OkvCode");
            this._grid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this._bindingSource = new System.Windows.Forms.BindingSource(this.components);
            this._filterUIProvider = new Infragistics.Win.SupportDialogs.FilterUIProvider.UltraGridFilterUIProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this._grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._bindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // _grid
            // 
            this._grid.DataSource = this._bindingSource;
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this._grid.DisplayLayout.Appearance = appearance1;
            ultraGridColumn42.AllowGroupBy = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn42.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn42.Header.Caption = "ИНН";
            ultraGridColumn42.Header.ToolTipText = "ИНН покупателя";
            ultraGridColumn42.Header.VisiblePosition = 0;
            ultraGridColumn42.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn42.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn42.RowLayoutColumnInfo.ParentGroupIndex = 4;
            ultraGridColumn42.RowLayoutColumnInfo.ParentGroupKey = "NewGroup4";
            ultraGridColumn42.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(100, 0);
            ultraGridColumn42.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn42.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn42.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn43.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn43.Header.Caption = "КПП";
            ultraGridColumn43.Header.ToolTipText = "КПП покупателя";
            ultraGridColumn43.Header.VisiblePosition = 2;
            ultraGridColumn43.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn43.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn43.RowLayoutColumnInfo.ParentGroupIndex = 4;
            ultraGridColumn43.RowLayoutColumnInfo.ParentGroupKey = "NewGroup4";
            ultraGridColumn43.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn43.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn43.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn43.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn44.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn44.Header.Caption = "в валюте";
            ultraGridColumn44.Header.ToolTipText = "Стоимость продаж по счету-фактуре, разница стоимости по корректировочному счету-ф" +
    "актуре (включая налог), в валюте счета-фактуры";
            ultraGridColumn44.Header.VisiblePosition = 3;
            ultraGridColumn44.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn44.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn44.RowLayoutColumnInfo.ParentGroupIndex = 6;
            ultraGridColumn44.RowLayoutColumnInfo.ParentGroupKey = "NewGroup6";
            ultraGridColumn44.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(100, 0);
            ultraGridColumn44.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 25);
            ultraGridColumn44.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn44.RowLayoutColumnInfo.SpanY = 1;
            ultraGridColumn45.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn45.Header.Caption = "в руб. и коп.";
            ultraGridColumn45.Header.ToolTipText = "Стоимость продаж по счету-фактуре, разница стоимости по корректировочному счету-ф" +
    "актуре (включая налог) в рублях и копейках";
            ultraGridColumn45.Header.VisiblePosition = 4;
            ultraGridColumn45.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn45.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn45.RowLayoutColumnInfo.ParentGroupIndex = 6;
            ultraGridColumn45.RowLayoutColumnInfo.ParentGroupKey = "NewGroup6";
            ultraGridColumn45.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(100, 0);
            ultraGridColumn45.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 33);
            ultraGridColumn45.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn45.RowLayoutColumnInfo.SpanY = 1;
            ultraGridColumn46.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn46.Header.Caption = "Доп. лист";
            ultraGridColumn46.Header.ToolTipText = "Сведения из дополнительного листа";
            ultraGridColumn46.Header.VisiblePosition = 1;
            ultraGridColumn46.RowLayoutColumnInfo.OriginX = 40;
            ultraGridColumn46.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn46.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(50, 0);
            ultraGridColumn46.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 40);
            ultraGridColumn46.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn46.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn47.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn47.Header.Caption = "Дата принятия на учет";
            ultraGridColumn47.Header.ToolTipText = "Дата принятия на учет товаров (работ, услуг), имущественных прав";
            ultraGridColumn47.Header.VisiblePosition = 18;
            ultraGridColumn47.RowLayoutColumnInfo.OriginX = 24;
            ultraGridColumn47.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn47.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn47.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 45);
            ultraGridColumn47.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn47.RowLayoutColumnInfo.SpanY = 3;
            ultraGridColumn47.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn48.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn48.Header.Caption = "Документ, подтверждающий оплату";
            ultraGridColumn48.Header.ToolTipText = "Номер и дата документа, подтверждающего оплату";
            ultraGridColumn48.Header.VisiblePosition = 19;
            ultraGridColumn48.RowLayoutColumnInfo.OriginX = 22;
            ultraGridColumn48.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn48.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(200, 0);
            ultraGridColumn48.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 40);
            ultraGridColumn48.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn48.RowLayoutColumnInfo.SpanY = 3;
            ultraGridColumn48.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn49.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn49.Header.Caption = "ИНН";
            ultraGridColumn49.Header.ToolTipText = "ИНН посредника (комиссионера, агента, экспедитора или застройщика)";
            ultraGridColumn49.Header.VisiblePosition = 5;
            ultraGridColumn49.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn49.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn49.RowLayoutColumnInfo.ParentGroupIndex = 5;
            ultraGridColumn49.RowLayoutColumnInfo.ParentGroupKey = "NewGroup5";
            ultraGridColumn49.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(100, 0);
            ultraGridColumn49.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn49.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn49.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn50.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn50.Header.Caption = "КПП";
            ultraGridColumn50.Header.ToolTipText = "КПП посредника (комиссионера, агента, экспедитора или застройщика)";
            ultraGridColumn50.Header.VisiblePosition = 6;
            ultraGridColumn50.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn50.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn50.RowLayoutColumnInfo.ParentGroupIndex = 5;
            ultraGridColumn50.RowLayoutColumnInfo.ParentGroupKey = "NewGroup5";
            ultraGridColumn50.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn50.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn50.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn50.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn51.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn51.Header.Caption = "№";
            ultraGridColumn51.Header.ToolTipText = "Порядковый номер";
            ultraGridColumn51.Header.VisiblePosition = 8;
            ultraGridColumn51.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn51.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn51.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(50, 0);
            ultraGridColumn51.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 40);
            ultraGridColumn51.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn51.RowLayoutColumnInfo.SpanY = 3;
            ultraGridColumn51.Width = 100;
            ultraGridColumn52.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn52.Header.Caption = "Код вида операции";
            ultraGridColumn52.Header.ToolTipText = "Код вида операции";
            ultraGridColumn52.Header.VisiblePosition = 9;
            ultraGridColumn52.RowLayoutColumnInfo.OriginX = 4;
            ultraGridColumn52.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn52.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(100, 0);
            ultraGridColumn52.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 40);
            ultraGridColumn52.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn52.RowLayoutColumnInfo.SpanY = 3;
            ultraGridColumn52.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn53.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn53.Header.Caption = "№";
            ultraGridColumn53.Header.ToolTipText = "Номер исправления счета-фактуры продавца";
            ultraGridColumn53.Header.VisiblePosition = 12;
            ultraGridColumn53.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn53.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn53.RowLayoutColumnInfo.ParentGroupIndex = 1;
            ultraGridColumn53.RowLayoutColumnInfo.ParentGroupKey = "NewGroup1";
            ultraGridColumn53.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn53.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn53.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn53.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn54.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn54.Header.Caption = "Дата";
            ultraGridColumn54.Header.ToolTipText = "Дата исправления счета-фактуры продавца";
            ultraGridColumn54.Header.VisiblePosition = 13;
            ultraGridColumn54.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn54.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn54.RowLayoutColumnInfo.ParentGroupIndex = 1;
            ultraGridColumn54.RowLayoutColumnInfo.ParentGroupKey = "NewGroup1";
            ultraGridColumn54.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn54.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn54.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn55.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn55.Header.Caption = "№";
            ultraGridColumn55.Header.ToolTipText = "Номер корректировочного счета-фактуры продавца";
            ultraGridColumn55.Header.VisiblePosition = 14;
            ultraGridColumn55.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn55.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn55.RowLayoutColumnInfo.ParentGroupIndex = 2;
            ultraGridColumn55.RowLayoutColumnInfo.ParentGroupKey = "NewGroup2";
            ultraGridColumn55.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn55.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn55.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn55.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn56.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn56.Header.Caption = "Дата";
            ultraGridColumn56.Header.ToolTipText = "Дата корректировочного счета-фактуры продавца";
            ultraGridColumn56.Header.VisiblePosition = 15;
            ultraGridColumn56.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn56.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn56.RowLayoutColumnInfo.ParentGroupIndex = 2;
            ultraGridColumn56.RowLayoutColumnInfo.ParentGroupKey = "NewGroup2";
            ultraGridColumn56.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn56.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn56.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn57.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn57.Header.Caption = "№";
            ultraGridColumn57.Header.ToolTipText = "Номер исправления корректировочного счета-фактуры продавца";
            ultraGridColumn57.Header.VisiblePosition = 16;
            ultraGridColumn57.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn57.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn57.RowLayoutColumnInfo.ParentGroupIndex = 3;
            ultraGridColumn57.RowLayoutColumnInfo.ParentGroupKey = "NewGroup3";
            ultraGridColumn57.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn57.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn57.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn57.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn58.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn58.Header.Caption = "Дата";
            ultraGridColumn58.Header.ToolTipText = "Дата исправления корректировочного счета-фактуры продавца";
            ultraGridColumn58.Header.VisiblePosition = 17;
            ultraGridColumn58.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn58.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn58.RowLayoutColumnInfo.ParentGroupIndex = 3;
            ultraGridColumn58.RowLayoutColumnInfo.ParentGroupKey = "NewGroup3";
            ultraGridColumn58.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn58.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn58.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn59.Header.VisiblePosition = 21;
            ultraGridColumn60.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn60.Header.Caption = "";
            ultraGridColumn60.Header.ToolTipText = "Признак пояснения записи";
            ultraGridColumn60.Header.VisiblePosition = 20;
            ultraGridColumn60.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn60.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn60.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(20, 0);
            ultraGridColumn60.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 45);
            ultraGridColumn60.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn60.RowLayoutColumnInfo.SpanY = 3;
            ultraGridColumn61.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn61.Header.Caption = "№";
            ultraGridColumn61.Header.ToolTipText = "Номер счета-фактуры продавца";
            ultraGridColumn61.Header.VisiblePosition = 10;
            ultraGridColumn61.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn61.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn61.RowLayoutColumnInfo.ParentGroupIndex = 0;
            ultraGridColumn61.RowLayoutColumnInfo.ParentGroupKey = "NewGroup0";
            ultraGridColumn61.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn61.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn61.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn61.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn62.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn62.Header.Caption = "Дата";
            ultraGridColumn62.Header.ToolTipText = "Дата счета-фактуры продавца";
            ultraGridColumn62.Header.VisiblePosition = 11;
            ultraGridColumn62.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn62.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn62.RowLayoutColumnInfo.ParentGroupIndex = 0;
            ultraGridColumn62.RowLayoutColumnInfo.ParentGroupKey = "NewGroup0";
            ultraGridColumn62.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn62.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn62.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn63.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn63.Header.Caption = "Код валюты";
            ultraGridColumn63.Header.ToolTipText = "Код валюты по ОКВ";
            ultraGridColumn63.Header.VisiblePosition = 7;
            ultraGridColumn63.RowLayoutColumnInfo.OriginX = 34;
            ultraGridColumn63.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn63.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(50, 0);
            ultraGridColumn63.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 40);
            ultraGridColumn63.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn63.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn63.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridBand1.Columns.AddRange(new object[] {
            ultraGridColumn42,
            ultraGridColumn43,
            ultraGridColumn44,
            ultraGridColumn45,
            ultraGridColumn46,
            ultraGridColumn47,
            ultraGridColumn48,
            ultraGridColumn49,
            ultraGridColumn50,
            ultraGridColumn51,
            ultraGridColumn52,
            ultraGridColumn53,
            ultraGridColumn54,
            ultraGridColumn55,
            ultraGridColumn56,
            ultraGridColumn57,
            ultraGridColumn58,
            ultraGridColumn59,
            ultraGridColumn60,
            ultraGridColumn61,
            ultraGridColumn62,
            ultraGridColumn63});
            ultraGridGroup1.Header.Caption = "СФ";
            ultraGridGroup1.Key = "NewGroup0";
            ultraGridGroup1.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup1.RowLayoutGroupInfo.OriginX = 6;
            ultraGridGroup1.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup1.RowLayoutGroupInfo.SpanX = 4;
            ultraGridGroup1.RowLayoutGroupInfo.SpanY = 3;
            ultraGridGroup2.Header.Caption = "ИСФ";
            ultraGridGroup2.Key = "NewGroup1";
            ultraGridGroup2.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup2.RowLayoutGroupInfo.OriginX = 10;
            ultraGridGroup2.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup2.RowLayoutGroupInfo.SpanX = 4;
            ultraGridGroup2.RowLayoutGroupInfo.SpanY = 3;
            ultraGridGroup3.Header.Caption = "КСФ";
            ultraGridGroup3.Key = "NewGroup2";
            ultraGridGroup3.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup3.RowLayoutGroupInfo.OriginX = 14;
            ultraGridGroup3.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup3.RowLayoutGroupInfo.SpanX = 4;
            ultraGridGroup3.RowLayoutGroupInfo.SpanY = 3;
            ultraGridGroup4.Header.Caption = "ИКСФ";
            ultraGridGroup4.Key = "NewGroup3";
            ultraGridGroup4.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup4.RowLayoutGroupInfo.OriginX = 18;
            ultraGridGroup4.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup4.RowLayoutGroupInfo.SpanX = 4;
            ultraGridGroup4.RowLayoutGroupInfo.SpanY = 3;
            ultraGridGroup5.Header.Caption = "Сведения о покупателе";
            ultraGridGroup5.Key = "NewGroup4";
            ultraGridGroup5.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup5.RowLayoutGroupInfo.OriginX = 26;
            ultraGridGroup5.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup5.RowLayoutGroupInfo.SpanX = 4;
            ultraGridGroup5.RowLayoutGroupInfo.SpanY = 3;
            ultraGridGroup6.Header.Caption = "Сведения о посреднике";
            ultraGridGroup6.Key = "NewGroup5";
            ultraGridGroup6.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup6.RowLayoutGroupInfo.OriginX = 30;
            ultraGridGroup6.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup6.RowLayoutGroupInfo.SpanX = 4;
            ultraGridGroup6.RowLayoutGroupInfo.SpanY = 3;
            ultraGridGroup7.Header.Caption = "Стоимость продаж с НДС";
            ultraGridGroup7.Key = "NewGroup6";
            ultraGridGroup7.RowLayoutGroupInfo.OriginX = 36;
            ultraGridGroup7.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup7.RowLayoutGroupInfo.SpanX = 4;
            ultraGridGroup7.RowLayoutGroupInfo.SpanY = 3;
            ultraGridBand1.Groups.AddRange(new Infragistics.Win.UltraWinGrid.UltraGridGroup[] {
            ultraGridGroup1,
            ultraGridGroup2,
            ultraGridGroup3,
            ultraGridGroup4,
            ultraGridGroup5,
            ultraGridGroup6,
            ultraGridGroup7});
            ultraGridBand1.RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.GroupLayout;
            ultraGridBand2.ColHeadersVisible = false;
            ultraGridColumn64.Header.VisiblePosition = 0;
            ultraGridColumn64.RowLayoutColumnInfo.OriginX = 24;
            ultraGridColumn64.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn64.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(100, 0);
            ultraGridColumn64.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn64.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn65.Header.VisiblePosition = 2;
            ultraGridColumn65.RowLayoutColumnInfo.OriginX = 26;
            ultraGridColumn65.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn65.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn65.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn65.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn66.Header.VisiblePosition = 4;
            ultraGridColumn66.RowLayoutColumnInfo.OriginX = 34;
            ultraGridColumn66.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn66.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(100, 0);
            ultraGridColumn66.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn66.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn67.Header.VisiblePosition = 6;
            ultraGridColumn67.RowLayoutColumnInfo.OriginX = 36;
            ultraGridColumn67.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn67.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(100, 0);
            ultraGridColumn67.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn67.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn68.Header.VisiblePosition = 15;
            ultraGridColumn68.RowLayoutColumnInfo.OriginX = 22;
            ultraGridColumn68.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn68.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn68.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn68.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn69.Header.VisiblePosition = 14;
            ultraGridColumn69.RowLayoutColumnInfo.OriginX = 20;
            ultraGridColumn69.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn69.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(200, 0);
            ultraGridColumn69.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn69.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn70.Header.VisiblePosition = 16;
            ultraGridColumn70.RowLayoutColumnInfo.OriginX = 28;
            ultraGridColumn70.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn70.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(100, 0);
            ultraGridColumn70.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn70.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn71.Header.VisiblePosition = 17;
            ultraGridColumn71.RowLayoutColumnInfo.OriginX = 30;
            ultraGridColumn71.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn71.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn71.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn71.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn72.Header.VisiblePosition = 1;
            ultraGridColumn72.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn72.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn72.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(50, 0);
            ultraGridColumn72.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn72.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn73.Header.VisiblePosition = 3;
            ultraGridColumn73.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn73.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn73.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(100, 0);
            ultraGridColumn73.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn73.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn74.Header.VisiblePosition = 8;
            ultraGridColumn74.RowLayoutColumnInfo.OriginX = 8;
            ultraGridColumn74.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn74.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn74.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn74.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn75.Header.VisiblePosition = 9;
            ultraGridColumn75.RowLayoutColumnInfo.OriginX = 10;
            ultraGridColumn75.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn75.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn75.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn75.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn76.Header.VisiblePosition = 10;
            ultraGridColumn76.RowLayoutColumnInfo.OriginX = 12;
            ultraGridColumn76.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn76.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn76.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn76.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn77.Header.VisiblePosition = 11;
            ultraGridColumn77.RowLayoutColumnInfo.OriginX = 14;
            ultraGridColumn77.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn77.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn77.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn77.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn78.Header.VisiblePosition = 12;
            ultraGridColumn78.RowLayoutColumnInfo.OriginX = 16;
            ultraGridColumn78.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn78.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn78.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn78.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn79.Header.VisiblePosition = 13;
            ultraGridColumn79.RowLayoutColumnInfo.OriginX = 18;
            ultraGridColumn79.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn79.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn79.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn79.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn80.Header.VisiblePosition = 5;
            ultraGridColumn80.RowLayoutColumnInfo.OriginX = 4;
            ultraGridColumn80.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn80.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn80.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn80.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn81.Header.VisiblePosition = 7;
            ultraGridColumn81.RowLayoutColumnInfo.OriginX = 6;
            ultraGridColumn81.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn81.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn81.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn81.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn82.Header.VisiblePosition = 18;
            ultraGridColumn82.RowLayoutColumnInfo.OriginX = 32;
            ultraGridColumn82.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn82.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(50, 0);
            ultraGridColumn82.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn82.RowLayoutColumnInfo.SpanY = 2;
            ultraGridBand2.Columns.AddRange(new object[] {
            ultraGridColumn64,
            ultraGridColumn65,
            ultraGridColumn66,
            ultraGridColumn67,
            ultraGridColumn68,
            ultraGridColumn69,
            ultraGridColumn70,
            ultraGridColumn71,
            ultraGridColumn72,
            ultraGridColumn73,
            ultraGridColumn74,
            ultraGridColumn75,
            ultraGridColumn76,
            ultraGridColumn77,
            ultraGridColumn78,
            ultraGridColumn79,
            ultraGridColumn80,
            ultraGridColumn81,
            ultraGridColumn82});
            ultraGridBand2.GroupHeadersVisible = false;
            ultraGridBand2.RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.GroupLayout;
            this._grid.DisplayLayout.BandsSerializer.Add(ultraGridBand1);
            this._grid.DisplayLayout.BandsSerializer.Add(ultraGridBand2);
            this._grid.DisplayLayout.InterBandSpacing = 0;
            this._grid.DisplayLayout.Override.AllowColMoving = Infragistics.Win.UltraWinGrid.AllowColMoving.NotAllowed;
            this._grid.DisplayLayout.Override.AllowColSizing = Infragistics.Win.UltraWinGrid.AllowColSizing.Free;
            this._grid.DisplayLayout.Override.AllowColSwapping = Infragistics.Win.UltraWinGrid.AllowColSwapping.NotAllowed;
            this._grid.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            this._grid.DisplayLayout.Override.ExpansionIndicator = Infragistics.Win.UltraWinGrid.ShowExpansionIndicator.CheckOnDisplay;
            this._grid.DisplayLayout.Override.FilterUIProvider = this._filterUIProvider;
            this._grid.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.ExternalSortSingle;
            this._grid.DisplayLayout.Override.HeaderPlacement = Infragistics.Win.UltraWinGrid.HeaderPlacement.FixedOnTop;
            this._grid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this._grid.DisplayLayout.Override.WrapHeaderText = Infragistics.Win.DefaultableBoolean.True;
            this._grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._grid.Location = new System.Drawing.Point(0, 0);
            this._grid.Name = "_grid";
            this._grid.Size = new System.Drawing.Size(804, 306);
            this._grid.TabIndex = 0;
            this._grid.UseAppStyling = false;
            // 
            // _bindingSource
            // 
            this._bindingSource.DataMember = "ListItems";
            this._bindingSource.DataSource = typeof(Luxoft.NDS2.Client.UI.Explain.Tks.Invoice.Models.Chapter9Model);
            // 
            // ExplainChapter9GridView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._grid);
            this.Name = "ExplainChapter9GridView";
            this.Size = new System.Drawing.Size(804, 306);
            ((System.ComponentModel.ISupportInitialize)(this._grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._bindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinGrid.UltraGrid _grid;
        private System.Windows.Forms.BindingSource _bindingSource;
        private Infragistics.Win.SupportDialogs.FilterUIProvider.UltraGridFilterUIProvider _filterUIProvider;
    }
}
