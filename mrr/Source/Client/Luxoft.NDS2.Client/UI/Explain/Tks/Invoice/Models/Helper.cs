﻿using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Explain.Tks.Invoice.Models
{
    public static class Helper
    {
        public static int MaxAttributes = 3;

        public static string MultiAttributeFormat = "{0} ...";

        public static string ReceiptDocumentFormat = "{0} от {1:d}";

        public static string AttributeSeparator = ",";

        public static IEnumerable<T> All<T>(this T[] attributes) where T : InvoiceAttribute
        {
            return attributes.Where(x => x.State != InvoiceAttributeState.Added);
        }

        public static IEnumerable<T> New<T>(this T[] attributes) where T : InvoiceAttribute
        {
            return attributes.Where(x => x.State != InvoiceAttributeState.Deleted);
        }

        public static string Format(this IEnumerable<InvoiceReceiptDocument> attributes)
        {
            return attributes
                .Select(x => string.Format(ReceiptDocumentFormat, x.DocumentNumber, x.DocumentDate))
                .ToArray().Format();
        }

        public static string Format(this IEnumerable<InvoiceReceiveDate> attributes)
        {
            return attributes.Select(x => string.Format("{0:d}", x.ReceiveDate)).ToArray().Format();
        }

        public static string FormatInn(this IEnumerable<InvoiceContractor> attributes)
        {
            return attributes.Select(x => x.Inn).ToArray().Format();
        }

        public static string FormatKpp(this IEnumerable<InvoiceContractor> attributes)
        {
            return attributes.Select(x => x.Kpp).ToArray().Format();
        }

        private static string Format(this string[] values)
        {
            var result = string.Join(AttributeSeparator, values.Where(s => !string.IsNullOrWhiteSpace(s)));
            if (values.Length <= MaxAttributes)
            {
                return result;
            }

            return string.Format(MultiAttributeFormat, result);
        }
    }
}
