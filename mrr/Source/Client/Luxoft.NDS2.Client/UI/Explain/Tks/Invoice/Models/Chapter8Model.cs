﻿using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Client.UI.Controls.Grid.ViewModels;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using Luxoft.NDS2.Common.Contracts.Helpers;

namespace Luxoft.NDS2.Client.UI.Explain.Tks.Invoice.Models
{
    public class Chapter8Model : ListViewModel<Chapter8InvoiceModel, ExplainTksInvoiceChapter8>
    {
        public Chapter8Model()
        {
            AddSortBuilder(
                TypeHelper<Chapter8InvoiceModel>.GetMemberName(x => x.AddList),
                new DefaultColumnSortBuilder(
                    TypeHelper<IExplainTksBookInvoice>.GetMemberName(x => x.Chapter)));
            AddFilterConverter(
                TypeHelper<Chapter8InvoiceModel>.GetMemberName(x => x.AddList),
                new Chapter8AddListFilterConverter());
            AddFilterConverter(
                TypeHelper<Chapter8InvoiceModel>.GetMemberName(x => x.OperationCodes),
                    new ColumnNameReplacer(
                    TypeHelper<IExplainTksBookInvoice>.GetMemberName(x => x.OperationCodesBit)));
            AddFilterConverter(
                TypeHelper<Chapter8InvoiceModel>.GetMemberName(x => x.SellerInn),
                new ColumnNameReplacer(
                    TypeHelper<ExplainTksInvoiceChapter8>.GetMemberName(x => x.FirstSellerInn)));
            AddFilterConverter(
                TypeHelper<Chapter8InvoiceModel>.GetMemberName(x => x.SellerKpp),
                new ColumnNameReplacer(
                    TypeHelper<ExplainTksInvoiceChapter8>.GetMemberName(x => x.FirstSellerKpp)));
        }

        protected override Chapter8InvoiceModel ConvertToModel(ExplainTksInvoiceChapter8 dataItem)
        {
            return new Chapter8InvoiceModel(dataItem);
        }
    }
}
