﻿namespace Luxoft.NDS2.Client.UI.Explain.Tks.Invoice.Controls
{
    partial class UnconfirmedInvoicesView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._grid = new Luxoft.NDS2.Client.UI.Controls.Grid.V2.DataGridView();
            this.SuspendLayout();
            // 
            // _grid
            // 
            this._grid.AggregatePanelVisible = true;
            this._grid.AllowFilterReset = false;
            this._grid.AllowMultiGrouping = true;
            this._grid.AllowResetSettings = false;
            this._grid.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this._grid.BackColor = System.Drawing.Color.Transparent;
            this._grid.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this._grid.ColumnVisibilitySetupButtonVisible = true;
            this._grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._grid.ExportExcelCancelHint = "Отмена экспорта в MS Excel";
            this._grid.ExportExcelCancelVisible = false;
            this._grid.ExportExcelHint = "Экспорт в MS Excel";
            this._grid.ExportExcelVisible = false;
            this._grid.FilterResetVisible = false;
            this._grid.FooterVisible = true;
            this._grid.GridContextMenuStrip = null;
            this._grid.Location = new System.Drawing.Point(0, 0);
            this._grid.Name = "_grid";
            this._grid.PanelExportExcelStateVisible = false;
            this._grid.PanelLoadingVisible = true;
            this._grid.PanelPagesVisible = true;
            this._grid.RowDoubleClicked = null;
            this._grid.Size = new System.Drawing.Size(866, 539);
            this._grid.TabIndex = 2;
            this._grid.Title = "";
            // 
            // UnconfirmedInvoicesView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._grid);
            this.Name = "UnconfirmedInvoicesView";
            this.Size = new System.Drawing.Size(866, 539);
            this.ResumeLayout(false);

        }

        #endregion

        private UI.Controls.Grid.V2.DataGridView _grid;
    }
}
