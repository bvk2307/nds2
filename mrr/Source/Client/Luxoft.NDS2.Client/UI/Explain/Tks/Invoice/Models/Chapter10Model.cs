﻿using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Client.UI.Controls.Grid.ViewModels;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using Luxoft.NDS2.Common.Contracts.Helpers;

namespace Luxoft.NDS2.Client.UI.Explain.Tks.Invoice.Models
{
    public class Chapter10Model : ListViewModel<Chapter10InvoiceModel, ExplainTksInvoiceChapter10>
    {
        public Chapter10Model()
        {
            AddFilterConverter(
                TypeHelper<Chapter10InvoiceModel>.GetMemberName(x => x.OperationCodes),
                    new ColumnNameReplacer(
                    TypeHelper<IExplainTksInvoice>.GetMemberName(x => x.OperationCodesBit)));
        }

        protected override Chapter10InvoiceModel ConvertToModel(ExplainTksInvoiceChapter10 dataItem)
        {
            return new Chapter10InvoiceModel(dataItem);
        }
    }
}
