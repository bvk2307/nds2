﻿using Luxoft.NDS2.Client.UI.Controls.Grid.ViewModels;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;

namespace Luxoft.NDS2.Client.UI.Explain.Tks.Invoice.Models
{
    public class Chapter12Model : ListViewModel<Chapter12InvoiceModel, ExplainTksInvoiceChapter12>
    {
        protected override Chapter12InvoiceModel ConvertToModel(ExplainTksInvoiceChapter12 dataItem)
        {
            return new Chapter12InvoiceModel(dataItem);
        }
    }
}
