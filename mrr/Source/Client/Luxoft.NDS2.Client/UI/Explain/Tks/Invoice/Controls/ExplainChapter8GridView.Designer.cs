﻿namespace Luxoft.NDS2.Client.UI.Explain.Tks.Invoice.Controls
{
    partial class ExplainChapter8GridView
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand1 = new Infragistics.Win.UltraWinGrid.UltraGridBand("ListItems", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn79 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("AddList");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn80 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SellerInn");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn81 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SellerKpp");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn82 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("PurchaseAmount");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn83 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ReceiveDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn84 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ReceiptDocumentsAll");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn85 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("BrokerInn");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn86 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("BrokerKpp");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn87 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("OrdinalNumber");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn88 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("OperationCodes");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn89 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ChangeNumber");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn90 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ChangeDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn91 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("CorrectionNumber");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn92 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("CorrectionDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn93 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ChangeCorrectionNumber");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn94 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ChangeCorrectionDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn95 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Changes");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn96 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Status");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn97 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("InvoiceNumber");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn98 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("InvoiceDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn99 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("OkvCode");
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup1 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup0", 103490736);
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup2 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup1", 103490861);
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup3 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup2", 103490986);
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup4 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup3", 103491111);
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup5 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup4", 103491235);
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup6 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup5", 103491360);
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand2 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Changes", 0);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn100 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SellerInn");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn101 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SellerKpp");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn102 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("PurchaseAmount");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn103 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ReceiveDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn104 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ReceiptDocumentsAll");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn105 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("BrokerInn");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn106 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("BrokerKpp");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn107 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("OrdinalNumber");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn108 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("OperationCodes");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn109 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ChangeNumber");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn110 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ChangeDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn111 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("CorrectionNumber");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn112 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("CorrectionDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn113 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ChangeCorrectionNumber");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn114 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ChangeCorrectionDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn115 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("InvoiceNumber");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn116 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("InvoiceDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn117 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("OkvCode");
            this._grid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this._filterProvider = new Infragistics.Win.SupportDialogs.FilterUIProvider.UltraGridFilterUIProvider(this.components);
            this._bindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this._grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._bindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // _grid
            // 
            this._grid.DataSource = this._bindingSource;
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this._grid.DisplayLayout.Appearance = appearance1;
            ultraGridColumn79.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn79.Header.Caption = "Доп. лист";
            ultraGridColumn79.Header.ToolTipText = "Сведения из дополнительного листа";
            ultraGridColumn79.Header.VisiblePosition = 0;
            ultraGridColumn79.RowLayoutColumnInfo.OriginX = 38;
            ultraGridColumn79.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn79.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(50, 0);
            ultraGridColumn79.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 40);
            ultraGridColumn79.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn79.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn80.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn80.Header.Caption = "ИНН";
            ultraGridColumn80.Header.ToolTipText = "ИНН продавца";
            ultraGridColumn80.Header.VisiblePosition = 1;
            ultraGridColumn80.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn80.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn80.RowLayoutColumnInfo.ParentGroupIndex = 4;
            ultraGridColumn80.RowLayoutColumnInfo.ParentGroupKey = "NewGroup4";
            ultraGridColumn80.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(100, 0);
            ultraGridColumn80.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn80.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn80.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn81.Header.Caption = "КПП";
            ultraGridColumn81.Header.ToolTipText = "КПП продавца";
            ultraGridColumn81.Header.VisiblePosition = 2;
            ultraGridColumn81.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn81.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn81.RowLayoutColumnInfo.ParentGroupIndex = 4;
            ultraGridColumn81.RowLayoutColumnInfo.ParentGroupKey = "NewGroup4";
            ultraGridColumn81.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn81.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn81.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn81.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn82.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn82.Header.Caption = "Стоимость покупок с НДС";
            ultraGridColumn82.Header.ToolTipText = "Стоимость покупок по счету-фактуре, разница стоимости по корректировочному счету-" +
    "фактуре (включая налог), в валюте счета-фактуры";
            ultraGridColumn82.Header.VisiblePosition = 5;
            ultraGridColumn82.RowLayoutColumnInfo.OriginX = 36;
            ultraGridColumn82.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn82.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(100, 0);
            ultraGridColumn82.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 40);
            ultraGridColumn82.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn82.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn83.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn83.Header.Caption = "Дата принятия на учет";
            ultraGridColumn83.Header.ToolTipText = "Дата принятия на учет товаров (работ, услуг), имущественных прав";
            ultraGridColumn83.Header.VisiblePosition = 17;
            ultraGridColumn83.RowLayoutColumnInfo.OriginX = 24;
            ultraGridColumn83.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn83.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn83.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 45);
            ultraGridColumn83.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn83.RowLayoutColumnInfo.SpanY = 3;
            ultraGridColumn83.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn84.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn84.Header.Caption = "Документ, подтверждающий оплату";
            ultraGridColumn84.Header.ToolTipText = "Номер и дата документа, подтверждающего оплату";
            ultraGridColumn84.Header.VisiblePosition = 18;
            ultraGridColumn84.RowLayoutColumnInfo.OriginX = 22;
            ultraGridColumn84.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn84.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(200, 0);
            ultraGridColumn84.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 40);
            ultraGridColumn84.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn84.RowLayoutColumnInfo.SpanY = 3;
            ultraGridColumn84.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn85.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn85.Header.Caption = "ИНН";
            ultraGridColumn85.Header.ToolTipText = "ИНН посредника (комиссионера, агента, экспедитора или застройщика)";
            ultraGridColumn85.Header.VisiblePosition = 3;
            ultraGridColumn85.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn85.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn85.RowLayoutColumnInfo.ParentGroupIndex = 5;
            ultraGridColumn85.RowLayoutColumnInfo.ParentGroupKey = "NewGroup5";
            ultraGridColumn85.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(100, 0);
            ultraGridColumn85.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn85.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn85.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn86.Header.Caption = "КПП";
            ultraGridColumn86.Header.ToolTipText = "КПП посредника (комиссионера, агента, экспедитора или застройщика)";
            ultraGridColumn86.Header.VisiblePosition = 4;
            ultraGridColumn86.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn86.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn86.RowLayoutColumnInfo.ParentGroupIndex = 5;
            ultraGridColumn86.RowLayoutColumnInfo.ParentGroupKey = "NewGroup5";
            ultraGridColumn86.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn86.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn86.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn86.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn87.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn87.Header.Caption = "№";
            ultraGridColumn87.Header.ToolTipText = "Порядковый номер";
            ultraGridColumn87.Header.VisiblePosition = 7;
            ultraGridColumn87.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn87.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn87.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(50, 0);
            ultraGridColumn87.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 40);
            ultraGridColumn87.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn87.RowLayoutColumnInfo.SpanY = 3;
            ultraGridColumn87.Width = 100;
            ultraGridColumn88.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn88.Header.Caption = "Код вида операции";
            ultraGridColumn88.Header.ToolTipText = "Код вида операции";
            ultraGridColumn88.Header.VisiblePosition = 8;
            ultraGridColumn88.RowLayoutColumnInfo.OriginX = 4;
            ultraGridColumn88.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn88.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(100, 0);
            ultraGridColumn88.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 40);
            ultraGridColumn88.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn88.RowLayoutColumnInfo.SpanY = 3;
            ultraGridColumn88.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn89.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn89.Header.Caption = "№";
            ultraGridColumn89.Header.ToolTipText = "Номер исправления счета-фактуры продавца";
            ultraGridColumn89.Header.VisiblePosition = 11;
            ultraGridColumn89.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn89.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn89.RowLayoutColumnInfo.ParentGroupIndex = 1;
            ultraGridColumn89.RowLayoutColumnInfo.ParentGroupKey = "NewGroup1";
            ultraGridColumn89.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn89.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn89.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn89.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn90.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn90.Header.Caption = "Дата";
            ultraGridColumn90.Header.ToolTipText = "Дата исправления счета-фактуры продавца";
            ultraGridColumn90.Header.VisiblePosition = 12;
            ultraGridColumn90.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn90.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn90.RowLayoutColumnInfo.ParentGroupIndex = 1;
            ultraGridColumn90.RowLayoutColumnInfo.ParentGroupKey = "NewGroup1";
            ultraGridColumn90.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn90.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn90.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn91.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn91.Header.Caption = "№";
            ultraGridColumn91.Header.ToolTipText = "Номер корректировочного счета-фактуры продавца";
            ultraGridColumn91.Header.VisiblePosition = 13;
            ultraGridColumn91.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn91.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn91.RowLayoutColumnInfo.ParentGroupIndex = 2;
            ultraGridColumn91.RowLayoutColumnInfo.ParentGroupKey = "NewGroup2";
            ultraGridColumn91.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn91.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn91.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn91.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn92.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn92.Header.Caption = "Дата";
            ultraGridColumn92.Header.ToolTipText = "Дата корректировочного счета-фактуры продавца";
            ultraGridColumn92.Header.VisiblePosition = 14;
            ultraGridColumn92.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn92.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn92.RowLayoutColumnInfo.ParentGroupIndex = 2;
            ultraGridColumn92.RowLayoutColumnInfo.ParentGroupKey = "NewGroup2";
            ultraGridColumn92.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn92.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn92.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn93.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn93.Header.Caption = "№";
            ultraGridColumn93.Header.ToolTipText = "Номер исправления корректировочного счета-фактуры продавца";
            ultraGridColumn93.Header.VisiblePosition = 15;
            ultraGridColumn93.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn93.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn93.RowLayoutColumnInfo.ParentGroupIndex = 3;
            ultraGridColumn93.RowLayoutColumnInfo.ParentGroupKey = "NewGroup3";
            ultraGridColumn93.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn93.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn93.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn93.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn94.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn94.Header.Caption = "Дата";
            ultraGridColumn94.Header.ToolTipText = "Дата исправления корректировочного счета-фактуры продавца";
            ultraGridColumn94.Header.VisiblePosition = 16;
            ultraGridColumn94.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn94.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn94.RowLayoutColumnInfo.ParentGroupIndex = 3;
            ultraGridColumn94.RowLayoutColumnInfo.ParentGroupKey = "NewGroup3";
            ultraGridColumn94.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn94.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn94.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn95.Header.VisiblePosition = 20;
            ultraGridColumn96.AllowGroupBy = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn96.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn96.Header.Caption = "";
            ultraGridColumn96.Header.ToolTipText = "Признак пояснения записи";
            ultraGridColumn96.Header.VisiblePosition = 19;
            ultraGridColumn96.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn96.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn96.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(20, 0);
            ultraGridColumn96.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 45);
            ultraGridColumn96.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn96.RowLayoutColumnInfo.SpanY = 3;
            ultraGridColumn97.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn97.Header.Caption = "№";
            ultraGridColumn97.Header.ToolTipText = "Номер счета-фактуры продавца";
            ultraGridColumn97.Header.VisiblePosition = 9;
            ultraGridColumn97.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn97.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn97.RowLayoutColumnInfo.ParentGroupIndex = 0;
            ultraGridColumn97.RowLayoutColumnInfo.ParentGroupKey = "NewGroup0";
            ultraGridColumn97.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn97.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn97.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn97.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn98.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn98.Header.Caption = "Дата";
            ultraGridColumn98.Header.ToolTipText = "Дата счета-фактуры продавца";
            ultraGridColumn98.Header.VisiblePosition = 10;
            ultraGridColumn98.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn98.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn98.RowLayoutColumnInfo.ParentGroupIndex = 0;
            ultraGridColumn98.RowLayoutColumnInfo.ParentGroupKey = "NewGroup0";
            ultraGridColumn98.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn98.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn98.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn99.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn99.Header.Caption = "Код валюты";
            ultraGridColumn99.Header.ToolTipText = "Код валюты по ОКВ";
            ultraGridColumn99.Header.VisiblePosition = 6;
            ultraGridColumn99.RowLayoutColumnInfo.OriginX = 34;
            ultraGridColumn99.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn99.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(50, 0);
            ultraGridColumn99.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 40);
            ultraGridColumn99.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn99.RowLayoutColumnInfo.SpanY = 4;
            ultraGridColumn99.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridBand1.Columns.AddRange(new object[] {
            ultraGridColumn79,
            ultraGridColumn80,
            ultraGridColumn81,
            ultraGridColumn82,
            ultraGridColumn83,
            ultraGridColumn84,
            ultraGridColumn85,
            ultraGridColumn86,
            ultraGridColumn87,
            ultraGridColumn88,
            ultraGridColumn89,
            ultraGridColumn90,
            ultraGridColumn91,
            ultraGridColumn92,
            ultraGridColumn93,
            ultraGridColumn94,
            ultraGridColumn95,
            ultraGridColumn96,
            ultraGridColumn97,
            ultraGridColumn98,
            ultraGridColumn99});
            ultraGridGroup1.Header.Caption = "СФ";
            ultraGridGroup1.Key = "NewGroup0";
            ultraGridGroup1.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup1.RowLayoutGroupInfo.OriginX = 6;
            ultraGridGroup1.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup1.RowLayoutGroupInfo.SpanX = 4;
            ultraGridGroup1.RowLayoutGroupInfo.SpanY = 3;
            ultraGridGroup2.Header.Caption = "ИСФ";
            ultraGridGroup2.Key = "NewGroup1";
            ultraGridGroup2.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup2.RowLayoutGroupInfo.OriginX = 10;
            ultraGridGroup2.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup2.RowLayoutGroupInfo.SpanX = 4;
            ultraGridGroup2.RowLayoutGroupInfo.SpanY = 3;
            ultraGridGroup3.Header.Caption = "КСФ";
            ultraGridGroup3.Key = "NewGroup2";
            ultraGridGroup3.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup3.RowLayoutGroupInfo.OriginX = 14;
            ultraGridGroup3.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup3.RowLayoutGroupInfo.SpanX = 4;
            ultraGridGroup3.RowLayoutGroupInfo.SpanY = 3;
            ultraGridGroup4.Header.Caption = "ИКСФ";
            ultraGridGroup4.Key = "NewGroup3";
            ultraGridGroup4.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup4.RowLayoutGroupInfo.OriginX = 18;
            ultraGridGroup4.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup4.RowLayoutGroupInfo.SpanX = 4;
            ultraGridGroup4.RowLayoutGroupInfo.SpanY = 3;
            ultraGridGroup5.Header.Caption = "Сведения о продавце";
            ultraGridGroup5.Key = "NewGroup4";
            ultraGridGroup5.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup5.RowLayoutGroupInfo.OriginX = 26;
            ultraGridGroup5.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup5.RowLayoutGroupInfo.SpanX = 4;
            ultraGridGroup5.RowLayoutGroupInfo.SpanY = 3;
            ultraGridGroup6.Header.Caption = "Сведения о посреднике";
            ultraGridGroup6.Key = "NewGroup5";
            ultraGridGroup6.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup6.RowLayoutGroupInfo.OriginX = 30;
            ultraGridGroup6.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup6.RowLayoutGroupInfo.SpanX = 4;
            ultraGridGroup6.RowLayoutGroupInfo.SpanY = 3;
            ultraGridBand1.Groups.AddRange(new Infragistics.Win.UltraWinGrid.UltraGridGroup[] {
            ultraGridGroup1,
            ultraGridGroup2,
            ultraGridGroup3,
            ultraGridGroup4,
            ultraGridGroup5,
            ultraGridGroup6});
            ultraGridBand1.RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.GroupLayout;
            ultraGridBand2.ColHeadersVisible = false;
            ultraGridColumn100.Header.VisiblePosition = 12;
            ultraGridColumn100.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(100, 0);
            ultraGridColumn101.Header.VisiblePosition = 13;
            ultraGridColumn101.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn102.Header.VisiblePosition = 17;
            ultraGridColumn102.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(100, 0);
            ultraGridColumn103.Header.VisiblePosition = 11;
            ultraGridColumn103.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn104.Header.VisiblePosition = 10;
            ultraGridColumn104.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(200, 0);
            ultraGridColumn105.Header.VisiblePosition = 14;
            ultraGridColumn105.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(100, 0);
            ultraGridColumn106.Header.VisiblePosition = 15;
            ultraGridColumn106.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn107.Header.VisiblePosition = 0;
            ultraGridColumn107.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(50, 0);
            ultraGridColumn108.Header.VisiblePosition = 1;
            ultraGridColumn108.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(100, 0);
            ultraGridColumn109.Header.VisiblePosition = 4;
            ultraGridColumn109.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn110.Header.VisiblePosition = 5;
            ultraGridColumn110.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn111.Header.VisiblePosition = 6;
            ultraGridColumn111.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn112.Header.VisiblePosition = 7;
            ultraGridColumn112.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn113.Header.VisiblePosition = 8;
            ultraGridColumn113.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn114.Header.VisiblePosition = 9;
            ultraGridColumn114.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn115.Header.VisiblePosition = 2;
            ultraGridColumn115.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn116.Header.VisiblePosition = 3;
            ultraGridColumn116.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn117.Header.VisiblePosition = 16;
            ultraGridColumn117.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(50, 0);
            ultraGridBand2.Columns.AddRange(new object[] {
            ultraGridColumn100,
            ultraGridColumn101,
            ultraGridColumn102,
            ultraGridColumn103,
            ultraGridColumn104,
            ultraGridColumn105,
            ultraGridColumn106,
            ultraGridColumn107,
            ultraGridColumn108,
            ultraGridColumn109,
            ultraGridColumn110,
            ultraGridColumn111,
            ultraGridColumn112,
            ultraGridColumn113,
            ultraGridColumn114,
            ultraGridColumn115,
            ultraGridColumn116,
            ultraGridColumn117});
            ultraGridBand2.GroupHeadersVisible = false;
            ultraGridBand2.RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.GroupLayout;
            this._grid.DisplayLayout.BandsSerializer.Add(ultraGridBand1);
            this._grid.DisplayLayout.BandsSerializer.Add(ultraGridBand2);
            this._grid.DisplayLayout.InterBandSpacing = 0;
            this._grid.DisplayLayout.Override.AllowColMoving = Infragistics.Win.UltraWinGrid.AllowColMoving.NotAllowed;
            this._grid.DisplayLayout.Override.AllowColSizing = Infragistics.Win.UltraWinGrid.AllowColSizing.Free;
            this._grid.DisplayLayout.Override.AllowColSwapping = Infragistics.Win.UltraWinGrid.AllowColSwapping.NotAllowed;
            this._grid.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            this._grid.DisplayLayout.Override.ExpansionIndicator = Infragistics.Win.UltraWinGrid.ShowExpansionIndicator.CheckOnDisplay;
            this._grid.DisplayLayout.Override.FilterUIProvider = this._filterProvider;
            this._grid.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.ExternalSortSingle;
            this._grid.DisplayLayout.Override.HeaderPlacement = Infragistics.Win.UltraWinGrid.HeaderPlacement.FixedOnTop;
            this._grid.DisplayLayout.Override.RowFilterAction = Infragistics.Win.UltraWinGrid.RowFilterAction.AppearancesOnly;
            this._grid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this._grid.DisplayLayout.Override.WrapHeaderText = Infragistics.Win.DefaultableBoolean.True;
            this._grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._grid.Location = new System.Drawing.Point(0, 0);
            this._grid.Name = "_grid";
            this._grid.Size = new System.Drawing.Size(804, 306);
            this._grid.TabIndex = 0;
            this._grid.UseAppStyling = false;
            // 
            // _bindingSource
            // 
            this._bindingSource.DataMember = "ListItems";
            this._bindingSource.DataSource = typeof(Luxoft.NDS2.Client.UI.Explain.Tks.Invoice.Models.Chapter8Model);
            // 
            // ExplainChapter8GridView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._grid);
            this.Name = "ExplainChapter8GridView";
            this.Size = new System.Drawing.Size(804, 306);
            ((System.ComponentModel.ISupportInitialize)(this._grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._bindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinGrid.UltraGrid _grid;
        private System.Windows.Forms.BindingSource _bindingSource;
        private Infragistics.Win.SupportDialogs.FilterUIProvider.UltraGridFilterUIProvider _filterProvider;
    }
}
