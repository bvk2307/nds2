﻿using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using Luxoft.NDS2.Common.Models;
using System;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Explain.Tks.Invoice.Models
{
    public abstract class BookInvoiceChangedModel<TDto> : InvoiceChangedModel<TDto>
        where TDto : IExplainTksBookInvoice
    { 

        protected BookInvoiceChangedModel(TDto data)
            : base(data)
        {
        }

        public string ReceiveDate
        {
            get 
            { 
                return _data.ReceiveDates.New().Format(); 
            }
        }

        public string ReceiptDocumentsAll
        {
            get { return _data.ReceiptDocuments.New().Format(); }
        }

        public string BrokerInn
        {
            get { return _data.NewBrokerInn; }
        }

        public string BrokerKpp
        {
            get { return _data.NewBrokerKpp; }
        }
    }
}
