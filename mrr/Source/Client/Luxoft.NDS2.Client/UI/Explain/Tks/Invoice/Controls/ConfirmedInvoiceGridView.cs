﻿using System.Collections.Generic;
using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Client.UI.Explain.Tks.Invoice.Models;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using Luxoft.NDS2.Common.Contracts.Helpers;
using System.Windows.Forms;
using Infragistics.Win.UltraWinGrid;

namespace Luxoft.NDS2.Client.UI.Explain.Tks.Invoice.Controls
{

    public partial class ConfirmedInvoiceGridView : UserControl, IExtendedGridView
    {
        public ConfirmedInvoiceGridView()
        {
            InitializeComponent();

            GridEvents = new UltraGridEventsHandler(_grid);
            Columns =
                new GridColumnsCollection(
                    null,
                    _grid.DisplayLayout.Bands[0].Groups.AsEnumerable(),
                    _grid.DisplayLayout.Bands[0].Columns.AsEnumerable(new[]
                    {
                        TypeHelper<ConfirmedInvoiceModel>.GetMemberName(x => x.ChangedState),
                        TypeHelper<ConfirmedInvoiceModel>.GetMemberName(x => x.ExplainZip)
                    }
                        ),
                    new GridMultiBandColumnCreator(new List<ColumnsCollection> { _grid.DisplayLayout.Bands[1].Columns})
                        .WithFilterViewFor(TypeHelper<ConfirmedInvoiceModel>.GetMemberName(x => x.OperationCodes),
                            column => new OperationCodesColumnFilterView(column))
                        .WithFilterViewFor(TypeHelper<ConfirmedInvoiceModel>.GetMemberName(x => x.Status),
                            column => new ConfirmedInoicesStatusColumnFilterView(column)));
            ApplyFormatters();

            _grid.SetBaseConfig();
        }

        public void SetGridHeight(int h)
        {
            _grid.Height = h;
        }

        private void ApplyFormatters()
        {
            Columns
                .FindByKey(TypeHelper<ConfirmedInvoiceModel>.GetMemberName(x => x.Status))
                .ApplyFormatter(
                    new ImageColumnFormatter<ExplainTksInvoiceState>(InvoiceStatusDictionary.StatusIcons, this))
                .ApplyFormatter(
                    new ToolTipFormatter<ConfirmedInvoiceModel>(model => InvoiceStatusDictionary.GetTooltip(model.Status), _grid));

            Columns
                .FindByKey(TypeHelper<ConfirmedInvoiceModel>.GetMemberName(x => x.Amount))
                .ApplyFormatter(new ValueColumnFormatter<ConfirmedInvoiceModel>(
                    new IntValueZeroFormatter(),
                    "{0: 0,0.0}"));

            Columns
                .FindByKey(TypeHelper<ConfirmedInvoiceModel>.GetMemberName(x => x.AmountRur))
                .ApplyFormatter(new ValueColumnFormatter<ConfirmedInvoiceModel>(
                    new IntValueZeroFormatter(),
                    "{0: 0,0.0}"));
        }

        public void SetDataSource(object dataSource)
        {
            _bindingSource.DataSource = dataSource;
        }

        public IGridColumnsCollection Columns
        {
            get;
            private set;
        }

        public IGridEventsHandler GridEvents
        {
            get;
            private set;
        }
    }
}
