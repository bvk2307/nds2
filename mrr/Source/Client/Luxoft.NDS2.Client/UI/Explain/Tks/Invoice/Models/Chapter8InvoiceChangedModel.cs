﻿using Luxoft.NDS2.Common.Contracts.DTO.Explain;

namespace Luxoft.NDS2.Client.UI.Explain.Tks.Invoice.Models
{
    public class Chapter8InvoiceChangedModel : BookInvoiceChangedModel<ExplainTksInvoiceChapter8>
    {
        public Chapter8InvoiceChangedModel(ExplainTksInvoiceChapter8 data)
            : base(data)
        {
        }

        public string SellerInn
        {
            get { return _data.Contractors.New().FormatInn(); }
        }

        public string SellerKpp
        {
            get { return _data.Contractors.New().FormatKpp(); }
        }

        public decimal? PurchaseAmount
        {
            get { return _data.NewPurchaseAmount; }
        }
    }
}
