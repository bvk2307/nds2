﻿namespace Luxoft.NDS2.Client.UI.Explain.Tks.Invoice.Controls
{
    partial class ExplainChapter10GridView
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand1 = new Infragistics.Win.UltraWinGrid.UltraGridBand("ListItems", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn39 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("BuyerInn");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn40 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("BuyerKpp");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn41 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SellerInn");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn42 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SellerKpp");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn43 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("AgencyInvoiceNumber");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn44 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("AgencyInvoiceDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn45 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Amount");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn46 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("OrdinalNumber");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn47 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("OperationCodes");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn48 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ChangeNumber");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn49 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ChangeDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn50 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("CorrectionNumber");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn51 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("CorrectionDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn52 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ChangeCorrectionNumber");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn53 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ChangeCorrectionDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn54 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Changes");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn55 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Status");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn56 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("InvoiceNumber");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn57 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("InvoiceDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn58 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("OkvCode");
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup1 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup0", 103671869);
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup2 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup1", 103671870);
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup3 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup2", 103671871);
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup4 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup3", 103671872);
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup5 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup4", 103671873);
            Infragistics.Win.UltraWinGrid.UltraGridGroup ultraGridGroup6 = new Infragistics.Win.UltraWinGrid.UltraGridGroup("NewGroup5", 103671874);
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand2 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Changes", 0);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn59 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("BuyerInn");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn60 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("BuyerKpp");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn61 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SellerInn");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn62 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SellerKpp");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn63 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("AgencyInvoiceNumber");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn64 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("AgencyInvoiceDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn65 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Amount");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn66 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("OrdinalNumber");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn67 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("OperationCodes");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn68 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ChangeNumber");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn69 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ChangeDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn70 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("CorrectionNumber");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn71 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("CorrectionDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn72 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ChangeCorrectionNumber");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn73 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("ChangeCorrectionDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn74 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("InvoiceNumber");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn75 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("InvoiceDate");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn76 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("OkvCode");
            this._grid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this._bindingSource = new System.Windows.Forms.BindingSource(this.components);
            this._filterUIProvider = new Infragistics.Win.SupportDialogs.FilterUIProvider.UltraGridFilterUIProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this._grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._bindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // _grid
            // 
            this._grid.DataSource = this._bindingSource;
            appearance4.BackColor = System.Drawing.SystemColors.Window;
            appearance4.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this._grid.DisplayLayout.Appearance = appearance4;
            ultraGridColumn39.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn39.Header.Caption = "ИНН";
            ultraGridColumn39.Header.ToolTipText = "ИНН покупателя";
            ultraGridColumn39.Header.VisiblePosition = 0;
            ultraGridColumn39.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn39.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn39.RowLayoutColumnInfo.ParentGroupIndex = 4;
            ultraGridColumn39.RowLayoutColumnInfo.ParentGroupKey = "NewGroup4";
            ultraGridColumn39.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(100, 0);
            ultraGridColumn39.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 20);
            ultraGridColumn39.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn39.RowLayoutColumnInfo.SpanY = 1;
            ultraGridColumn39.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn40.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn40.Header.Caption = "КПП";
            ultraGridColumn40.Header.ToolTipText = "КПП покупателя";
            ultraGridColumn40.Header.VisiblePosition = 1;
            ultraGridColumn40.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn40.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn40.RowLayoutColumnInfo.ParentGroupIndex = 4;
            ultraGridColumn40.RowLayoutColumnInfo.ParentGroupKey = "NewGroup4";
            ultraGridColumn40.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn40.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 20);
            ultraGridColumn40.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn40.RowLayoutColumnInfo.SpanY = 1;
            ultraGridColumn40.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn41.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn41.Header.Caption = "ИНН Продавца";
            ultraGridColumn41.Header.ToolTipText = "ИНН Продавца";
            ultraGridColumn41.Header.VisiblePosition = 2;
            ultraGridColumn41.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn41.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn41.RowLayoutColumnInfo.ParentGroupIndex = 5;
            ultraGridColumn41.RowLayoutColumnInfo.ParentGroupKey = "NewGroup5";
            ultraGridColumn41.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(100, 0);
            ultraGridColumn41.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 45);
            ultraGridColumn41.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn41.RowLayoutColumnInfo.SpanY = 1;
            ultraGridColumn41.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn42.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn42.Header.Caption = "КПП Продавца";
            ultraGridColumn42.Header.ToolTipText = "КПП Продавца";
            ultraGridColumn42.Header.VisiblePosition = 3;
            ultraGridColumn42.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn42.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn42.RowLayoutColumnInfo.ParentGroupIndex = 5;
            ultraGridColumn42.RowLayoutColumnInfo.ParentGroupKey = "NewGroup5";
            ultraGridColumn42.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn42.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 45);
            ultraGridColumn42.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn42.RowLayoutColumnInfo.SpanY = 1;
            ultraGridColumn42.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn43.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn43.Header.Caption = "№ СФ";
            ultraGridColumn43.Header.ToolTipText = "Номер счета-фактуры полученного от продавца";
            ultraGridColumn43.Header.VisiblePosition = 5;
            ultraGridColumn43.RowLayoutColumnInfo.OriginX = 4;
            ultraGridColumn43.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn43.RowLayoutColumnInfo.ParentGroupIndex = 5;
            ultraGridColumn43.RowLayoutColumnInfo.ParentGroupKey = "NewGroup5";
            ultraGridColumn43.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn43.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 45);
            ultraGridColumn43.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn43.RowLayoutColumnInfo.SpanY = 1;
            ultraGridColumn43.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn44.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn44.Header.Caption = "Дата СФ";
            ultraGridColumn44.Header.ToolTipText = "Дата счета-фактуры полученного от продавца";
            ultraGridColumn44.Header.VisiblePosition = 6;
            ultraGridColumn44.RowLayoutColumnInfo.OriginX = 6;
            ultraGridColumn44.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn44.RowLayoutColumnInfo.ParentGroupIndex = 5;
            ultraGridColumn44.RowLayoutColumnInfo.ParentGroupKey = "NewGroup5";
            ultraGridColumn44.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn44.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 45);
            ultraGridColumn44.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn44.RowLayoutColumnInfo.SpanY = 1;
            ultraGridColumn45.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn45.Header.Caption = "Стоимость товаров";
            ultraGridColumn45.Header.ToolTipText = "Стоимость товаров (работ, услуг), имущественных прав по счету-фактура";
            ultraGridColumn45.Header.VisiblePosition = 4;
            ultraGridColumn45.RowLayoutColumnInfo.OriginX = 10;
            ultraGridColumn45.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn45.RowLayoutColumnInfo.ParentGroupIndex = 5;
            ultraGridColumn45.RowLayoutColumnInfo.ParentGroupKey = "NewGroup5";
            ultraGridColumn45.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn45.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 20);
            ultraGridColumn45.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn45.RowLayoutColumnInfo.SpanY = 1;
            ultraGridColumn46.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn46.Header.Caption = "№";
            ultraGridColumn46.Header.ToolTipText = "Порядковый номер";
            ultraGridColumn46.Header.VisiblePosition = 8;
            ultraGridColumn46.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn46.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn46.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(50, 0);
            ultraGridColumn46.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 33);
            ultraGridColumn46.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn46.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn47.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn47.Header.Caption = "Код вида операции";
            ultraGridColumn47.Header.ToolTipText = "Код вида операции";
            ultraGridColumn47.Header.VisiblePosition = 10;
            ultraGridColumn47.RowLayoutColumnInfo.OriginX = 4;
            ultraGridColumn47.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn47.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn47.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn47.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn47.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn48.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn48.Header.Caption = "№";
            ultraGridColumn48.Header.ToolTipText = "Номер исправления счета-фактуры";
            ultraGridColumn48.Header.VisiblePosition = 12;
            ultraGridColumn48.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn48.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn48.RowLayoutColumnInfo.ParentGroupIndex = 1;
            ultraGridColumn48.RowLayoutColumnInfo.ParentGroupKey = "NewGroup1";
            ultraGridColumn48.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn48.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 33);
            ultraGridColumn48.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn48.RowLayoutColumnInfo.SpanY = 1;
            ultraGridColumn48.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn49.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn49.Header.Caption = "Дата";
            ultraGridColumn49.Header.ToolTipText = "Дата исправления счета-фактуры";
            ultraGridColumn49.Header.VisiblePosition = 14;
            ultraGridColumn49.RowLayoutColumnInfo.MinimumLabelSize = new System.Drawing.Size(80, 0);
            ultraGridColumn49.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn49.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn49.RowLayoutColumnInfo.ParentGroupIndex = 1;
            ultraGridColumn49.RowLayoutColumnInfo.ParentGroupKey = "NewGroup1";
            ultraGridColumn49.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn49.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn49.RowLayoutColumnInfo.SpanY = 1;
            ultraGridColumn50.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn50.Header.Caption = "№";
            ultraGridColumn50.Header.ToolTipText = "Номер корректировки счета-фактуры";
            ultraGridColumn50.Header.VisiblePosition = 15;
            ultraGridColumn50.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn50.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn50.RowLayoutColumnInfo.ParentGroupIndex = 2;
            ultraGridColumn50.RowLayoutColumnInfo.ParentGroupKey = "NewGroup2";
            ultraGridColumn50.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn50.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn50.RowLayoutColumnInfo.SpanY = 1;
            ultraGridColumn50.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn51.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn51.Header.Caption = "Дата";
            ultraGridColumn51.Header.ToolTipText = "Дата корректировки счета-фактуры";
            ultraGridColumn51.Header.VisiblePosition = 16;
            ultraGridColumn51.RowLayoutColumnInfo.MinimumLabelSize = new System.Drawing.Size(80, 0);
            ultraGridColumn51.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn51.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn51.RowLayoutColumnInfo.ParentGroupIndex = 2;
            ultraGridColumn51.RowLayoutColumnInfo.ParentGroupKey = "NewGroup2";
            ultraGridColumn51.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 20);
            ultraGridColumn51.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn51.RowLayoutColumnInfo.SpanY = 1;
            ultraGridColumn52.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn52.Header.Caption = "№";
            ultraGridColumn52.Header.ToolTipText = "Номер исправления корректировочного счета-фактуры";
            ultraGridColumn52.Header.VisiblePosition = 17;
            ultraGridColumn52.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn52.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn52.RowLayoutColumnInfo.ParentGroupIndex = 3;
            ultraGridColumn52.RowLayoutColumnInfo.ParentGroupKey = "NewGroup3";
            ultraGridColumn52.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn52.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 20);
            ultraGridColumn52.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn52.RowLayoutColumnInfo.SpanY = 1;
            ultraGridColumn52.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn53.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn53.Header.Caption = "Дата";
            ultraGridColumn53.Header.ToolTipText = "Дата исправления корректировочного счета-фактуры";
            ultraGridColumn53.Header.VisiblePosition = 18;
            ultraGridColumn53.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn53.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn53.RowLayoutColumnInfo.ParentGroupIndex = 3;
            ultraGridColumn53.RowLayoutColumnInfo.ParentGroupKey = "NewGroup3";
            ultraGridColumn53.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn53.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 20);
            ultraGridColumn53.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn53.RowLayoutColumnInfo.SpanY = 1;
            ultraGridColumn54.Header.VisiblePosition = 19;
            ultraGridColumn55.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn55.Header.Caption = "";
            ultraGridColumn55.Header.ToolTipText = "Признак пояснения записи";
            ultraGridColumn55.Header.VisiblePosition = 13;
            ultraGridColumn55.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn55.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn55.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(20, 0);
            ultraGridColumn55.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 33);
            ultraGridColumn55.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn55.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn56.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            ultraGridColumn56.Header.Caption = "№";
            ultraGridColumn56.Header.ToolTipText = "Номер счета-фактуры продавца";
            ultraGridColumn56.Header.VisiblePosition = 9;
            ultraGridColumn56.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn56.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn56.RowLayoutColumnInfo.ParentGroupIndex = 0;
            ultraGridColumn56.RowLayoutColumnInfo.ParentGroupKey = "NewGroup0";
            ultraGridColumn56.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn56.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn56.RowLayoutColumnInfo.SpanY = 1;
            ultraGridColumn56.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn57.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn57.Header.Caption = "Дата";
            ultraGridColumn57.Header.ToolTipText = "Дата счета-фактуры продавца";
            ultraGridColumn57.Header.VisiblePosition = 11;
            ultraGridColumn57.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn57.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn57.RowLayoutColumnInfo.ParentGroupIndex = 0;
            ultraGridColumn57.RowLayoutColumnInfo.ParentGroupKey = "NewGroup0";
            ultraGridColumn57.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn57.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn57.RowLayoutColumnInfo.SpanY = 1;
            ultraGridColumn58.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn58.Header.Caption = "Код валюты";
            ultraGridColumn58.Header.ToolTipText = "Код валюты по ОКВ";
            ultraGridColumn58.Header.VisiblePosition = 7;
            ultraGridColumn58.RowLayoutColumnInfo.OriginX = 8;
            ultraGridColumn58.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn58.RowLayoutColumnInfo.ParentGroupIndex = 5;
            ultraGridColumn58.RowLayoutColumnInfo.ParentGroupKey = "NewGroup5";
            ultraGridColumn58.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(50, 0);
            ultraGridColumn58.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 45);
            ultraGridColumn58.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn58.RowLayoutColumnInfo.SpanY = 1;
            ultraGridColumn58.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridBand1.Columns.AddRange(new object[] {
            ultraGridColumn39,
            ultraGridColumn40,
            ultraGridColumn41,
            ultraGridColumn42,
            ultraGridColumn43,
            ultraGridColumn44,
            ultraGridColumn45,
            ultraGridColumn46,
            ultraGridColumn47,
            ultraGridColumn48,
            ultraGridColumn49,
            ultraGridColumn50,
            ultraGridColumn51,
            ultraGridColumn52,
            ultraGridColumn53,
            ultraGridColumn54,
            ultraGridColumn55,
            ultraGridColumn56,
            ultraGridColumn57,
            ultraGridColumn58});
            ultraGridGroup1.Header.Caption = "СФ";
            ultraGridGroup1.Key = "NewGroup0";
            ultraGridGroup1.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup1.RowLayoutGroupInfo.OriginX = 6;
            ultraGridGroup1.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup1.RowLayoutGroupInfo.SpanX = 4;
            ultraGridGroup1.RowLayoutGroupInfo.SpanY = 2;
            ultraGridGroup2.Header.Caption = "ИСФ";
            ultraGridGroup2.Key = "NewGroup1";
            ultraGridGroup2.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup2.RowLayoutGroupInfo.OriginX = 10;
            ultraGridGroup2.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup2.RowLayoutGroupInfo.SpanX = 4;
            ultraGridGroup2.RowLayoutGroupInfo.SpanY = 2;
            ultraGridGroup3.Header.Caption = "КСФ";
            ultraGridGroup3.Key = "NewGroup2";
            ultraGridGroup3.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup3.RowLayoutGroupInfo.OriginX = 14;
            ultraGridGroup3.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup3.RowLayoutGroupInfo.SpanX = 4;
            ultraGridGroup3.RowLayoutGroupInfo.SpanY = 2;
            ultraGridGroup4.Header.Caption = "ИКСФ";
            ultraGridGroup4.Key = "NewGroup3";
            ultraGridGroup4.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup4.RowLayoutGroupInfo.OriginX = 18;
            ultraGridGroup4.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup4.RowLayoutGroupInfo.SpanX = 4;
            ultraGridGroup4.RowLayoutGroupInfo.SpanY = 2;
            ultraGridGroup5.Header.Caption = "Сведения о покупателе";
            ultraGridGroup5.Key = "NewGroup4";
            ultraGridGroup5.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup5.RowLayoutGroupInfo.OriginX = 22;
            ultraGridGroup5.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup5.RowLayoutGroupInfo.SpanX = 4;
            ultraGridGroup5.RowLayoutGroupInfo.SpanY = 2;
            ultraGridGroup6.Header.Caption = "Сведения о посреднической деятельности";
            ultraGridGroup6.Key = "NewGroup5";
            ultraGridGroup6.RowLayoutGroupInfo.LabelSpan = 1;
            ultraGridGroup6.RowLayoutGroupInfo.OriginX = 26;
            ultraGridGroup6.RowLayoutGroupInfo.OriginY = 0;
            ultraGridGroup6.RowLayoutGroupInfo.SpanX = 12;
            ultraGridGroup6.RowLayoutGroupInfo.SpanY = 2;
            ultraGridBand1.Groups.AddRange(new Infragistics.Win.UltraWinGrid.UltraGridGroup[] {
            ultraGridGroup1,
            ultraGridGroup2,
            ultraGridGroup3,
            ultraGridGroup4,
            ultraGridGroup5,
            ultraGridGroup6});
            ultraGridBand1.RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.GroupLayout;
            ultraGridBand2.ColHeadersVisible = false;
            ultraGridColumn59.Header.VisiblePosition = 0;
            ultraGridColumn59.RowLayoutColumnInfo.OriginX = 20;
            ultraGridColumn59.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn59.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn59.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn60.Header.VisiblePosition = 2;
            ultraGridColumn60.RowLayoutColumnInfo.OriginX = 22;
            ultraGridColumn60.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn60.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn60.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn60.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn61.Header.VisiblePosition = 1;
            ultraGridColumn61.RowLayoutColumnInfo.OriginX = 24;
            ultraGridColumn61.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn61.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn61.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn62.Header.VisiblePosition = 3;
            ultraGridColumn62.RowLayoutColumnInfo.OriginX = 26;
            ultraGridColumn62.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn62.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn62.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn62.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn63.Header.VisiblePosition = 5;
            ultraGridColumn63.RowLayoutColumnInfo.OriginX = 28;
            ultraGridColumn63.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn63.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn63.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn63.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn64.Header.VisiblePosition = 7;
            ultraGridColumn64.RowLayoutColumnInfo.OriginX = 30;
            ultraGridColumn64.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn64.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn64.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn64.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn65.Header.VisiblePosition = 4;
            ultraGridColumn65.RowLayoutColumnInfo.OriginX = 34;
            ultraGridColumn65.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn65.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn65.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn65.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn66.Header.VisiblePosition = 9;
            ultraGridColumn66.RowLayoutColumnInfo.OriginX = 0;
            ultraGridColumn66.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn66.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(50, 0);
            ultraGridColumn66.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn66.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn67.Header.VisiblePosition = 11;
            ultraGridColumn67.RowLayoutColumnInfo.OriginX = 2;
            ultraGridColumn67.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn67.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn67.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn67.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn68.Header.VisiblePosition = 12;
            ultraGridColumn68.RowLayoutColumnInfo.OriginX = 8;
            ultraGridColumn68.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn68.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn68.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn68.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn69.Header.VisiblePosition = 13;
            ultraGridColumn69.RowLayoutColumnInfo.OriginX = 10;
            ultraGridColumn69.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn69.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn69.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn69.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn70.Header.VisiblePosition = 14;
            ultraGridColumn70.RowLayoutColumnInfo.OriginX = 12;
            ultraGridColumn70.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn70.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn70.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn70.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn71.Header.VisiblePosition = 15;
            ultraGridColumn71.RowLayoutColumnInfo.OriginX = 14;
            ultraGridColumn71.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn71.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn71.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn71.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn72.Header.VisiblePosition = 16;
            ultraGridColumn72.RowLayoutColumnInfo.OriginX = 16;
            ultraGridColumn72.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn72.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn72.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn72.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn73.Header.VisiblePosition = 17;
            ultraGridColumn73.RowLayoutColumnInfo.OriginX = 18;
            ultraGridColumn73.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn73.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn73.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn73.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn74.Header.VisiblePosition = 6;
            ultraGridColumn74.RowLayoutColumnInfo.OriginX = 4;
            ultraGridColumn74.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn74.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn74.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn74.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn75.Header.VisiblePosition = 8;
            ultraGridColumn75.RowLayoutColumnInfo.OriginX = 6;
            ultraGridColumn75.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn75.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(80, 0);
            ultraGridColumn75.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn75.RowLayoutColumnInfo.SpanY = 2;
            ultraGridColumn76.Header.VisiblePosition = 10;
            ultraGridColumn76.RowLayoutColumnInfo.OriginX = 32;
            ultraGridColumn76.RowLayoutColumnInfo.OriginY = 0;
            ultraGridColumn76.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(50, 0);
            ultraGridColumn76.RowLayoutColumnInfo.SpanX = 2;
            ultraGridColumn76.RowLayoutColumnInfo.SpanY = 2;
            ultraGridBand2.Columns.AddRange(new object[] {
            ultraGridColumn59,
            ultraGridColumn60,
            ultraGridColumn61,
            ultraGridColumn62,
            ultraGridColumn63,
            ultraGridColumn64,
            ultraGridColumn65,
            ultraGridColumn66,
            ultraGridColumn67,
            ultraGridColumn68,
            ultraGridColumn69,
            ultraGridColumn70,
            ultraGridColumn71,
            ultraGridColumn72,
            ultraGridColumn73,
            ultraGridColumn74,
            ultraGridColumn75,
            ultraGridColumn76});
            ultraGridBand2.GroupHeadersVisible = false;
            ultraGridBand2.RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.GroupLayout;
            this._grid.DisplayLayout.BandsSerializer.Add(ultraGridBand1);
            this._grid.DisplayLayout.BandsSerializer.Add(ultraGridBand2);
            this._grid.DisplayLayout.InterBandSpacing = 0;
            this._grid.DisplayLayout.Override.AllowColMoving = Infragistics.Win.UltraWinGrid.AllowColMoving.NotAllowed;
            this._grid.DisplayLayout.Override.AllowColSizing = Infragistics.Win.UltraWinGrid.AllowColSizing.Free;
            this._grid.DisplayLayout.Override.AllowColSwapping = Infragistics.Win.UltraWinGrid.AllowColSwapping.NotAllowed;
            this._grid.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            this._grid.DisplayLayout.Override.ExpansionIndicator = Infragistics.Win.UltraWinGrid.ShowExpansionIndicator.CheckOnDisplay;
            this._grid.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.ExternalSortSingle;
            this._grid.DisplayLayout.Override.HeaderPlacement = Infragistics.Win.UltraWinGrid.HeaderPlacement.FixedOnTop;
            this._grid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this._grid.DisplayLayout.Override.WrapHeaderText = Infragistics.Win.DefaultableBoolean.True;
            this._grid.DisplayLayout.Override.FilterUIProvider = this._filterUIProvider;
            this._grid.DisplayLayout.Override.RowFilterAction = Infragistics.Win.UltraWinGrid.RowFilterAction.AppearancesOnly;
            this._grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._grid.Location = new System.Drawing.Point(0, 0);
            this._grid.Name = "_grid";
            this._grid.Size = new System.Drawing.Size(804, 306);
            this._grid.TabIndex = 0;
            this._grid.UseAppStyling = false;
            // 
            // _bindingSource
            // 
            this._bindingSource.DataMember = "ListItems";
            this._bindingSource.DataSource = typeof(Luxoft.NDS2.Client.UI.Explain.Tks.Invoice.Models.Chapter10Model);
            // 
            // ExplainChapter10GridView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._grid);
            this.Name = "ExplainChapter10GridView";
            this.Size = new System.Drawing.Size(804, 306);
            ((System.ComponentModel.ISupportInitialize)(this._grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._bindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinGrid.UltraGrid _grid;
        private System.Windows.Forms.BindingSource _bindingSource;
        private Infragistics.Win.SupportDialogs.FilterUIProvider.UltraGridFilterUIProvider _filterUIProvider;
    }
}
