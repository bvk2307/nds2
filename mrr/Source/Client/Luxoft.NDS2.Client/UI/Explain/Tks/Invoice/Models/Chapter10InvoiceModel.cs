﻿using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using System;

namespace Luxoft.NDS2.Client.UI.Explain.Tks.Invoice.Models
{
    public class Chapter10InvoiceModel : InvoiceModel<ExplainTksInvoiceChapter10, Chapter10InvoiceChangedModel>
    {
        public Chapter10InvoiceModel(ExplainTksInvoiceChapter10 data)
            : base(data)
        {
        }

        public string BuyerInn
        {
            get
            {
                return _data.BuyerInn;
            }
        }

        public string BuyerKpp
        {
            get
            {
                return _data.BuyerKpp;
            }
        }

        public string SellerInn
        {
            get
            {
                return _data.SellerInn;
            }
        }

        public string SellerKpp
        {
            get
            {
                return _data.SellerKpp;
            }
        }

        public string AgencyInvoiceNumber
        {
            get
            {
                return _data.AgencyInvoiceNumber;
            }
        }

        public DateTime? AgencyInvoiceDate
        {
            get
            {
                return _data.AgencyInvoiceDate;
            }
        }

        public decimal? Amount
        {
            get
            {
                return _data.Amount;
            }
        }

        protected override Chapter10InvoiceChangedModel ChangedState()
        {
            return new Chapter10InvoiceChangedModel(_data);
        }
    }
}
