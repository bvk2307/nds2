﻿using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Client.UI.Explain.Tks.Invoice.Models;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using Luxoft.NDS2.Common.Contracts.Services;

namespace Luxoft.NDS2.Client.UI.Explain.Tks.Invoice
{
    public class ExplainChapterGridPresenterCreator
    {
        private readonly ITksInvoiceExplainView _view;

        private readonly TksInvoiceExplainModel _model;

        private readonly IClientLogger _logger;

        private readonly IExplainTksInvoiceService _service;

        public ExplainChapterGridPresenterCreator(
            ITksInvoiceExplainView view,
            TksInvoiceExplainModel model,
            IClientLogger logger,
            IExplainTksInvoiceService service)
        {
            _view = view;
            _model = model;
            _logger = logger;
            _service = service;
        }

        public IGridViewPresenter Chapter8()
        {
            return new PagingGridViewPresenter<
                Chapter8InvoiceModel,
                ExplainTksInvoiceChapter8,
                PageResult<ExplainTksInvoiceChapter8>>(
                new ExplainChapterDataProvider<ExplainTksInvoiceChapter8>(
                    _view, _logger, q => _service.FindInChapter8(_model.ExplainZip, q)),
                _model.Chapter8,
                _view.Chapter8Grid,
                _view.Chapter8Pager)
                .WithLogger(_logger);
        }

        public IGridViewPresenter Chapter9()
        {
            return new PagingGridViewPresenter<
                Chapter9InvoiceModel,
                ExplainTksInvoiceChapter9,
                PageResult<ExplainTksInvoiceChapter9>>(
                    new ExplainChapterDataProvider<ExplainTksInvoiceChapter9>(
                        _view, _logger, q => _service.FindInChapter9(_model.ExplainZip, q)),
                    _model.Chapter9,
                    _view.Chapter9Grid,
                    _view.Chapter9Pager)
                    .WithLogger(_logger);

        }

        public IGridViewPresenter Chapter10()
        {
            return new PagingGridViewPresenter<
                Chapter10InvoiceModel,
                ExplainTksInvoiceChapter10,
                PageResult<ExplainTksInvoiceChapter10>>(
                    new ExplainChapterDataProvider<ExplainTksInvoiceChapter10>(
                        _view, _logger, q => _service.FindInChapter10(_model.ExplainZip, q)),
                    _model.Chapter10,
                    _view.Chapter10Grid,
                    _view.Chapter10Pager)
                    .WithLogger(_logger);

        }

        public IGridViewPresenter Chapter11()
        {
            return new PagingGridViewPresenter<
                Chapter11InvoiceModel,
                ExplainTksInvoiceChapter11,
                PageResult<ExplainTksInvoiceChapter11>>(
                new ExplainChapterDataProvider<ExplainTksInvoiceChapter11>(
                    _view, _logger, q => _service.FindInChapter11(_model.ExplainZip, q)),
                _model.Chapter11,
                _view.Chapter11Grid,
                _view.Chapter11Pager)
                .WithLogger(_logger);
        }

        public IGridViewPresenter Chapter12()
        {
            return new PagingGridViewPresenter<
                Chapter12InvoiceModel,
                ExplainTksInvoiceChapter12,
                PageResult<ExplainTksInvoiceChapter12>>(
                    new ExplainChapterDataProvider<ExplainTksInvoiceChapter12>(
                        _view, _logger, q => _service.FindInChapter12(_model.ExplainZip, q)),
                    _model.Chapter12,
                    _view.Chapter12Grid,
                    _view.Chapter12Pager)
                    .WithLogger(_logger);
        }

        public IGridViewPresenter ConfirmedInvoices()
        {
            return new PagingGridViewPresenter<
                ConfirmedInvoiceModel,
                ExplainTksInvoiceConfirmed,
                PageResult<ExplainTksInvoiceConfirmed>>(
                new ExplainChapterDataProvider<ExplainTksInvoiceConfirmed>(
                    _view, _logger, q => _service.FindConfirmedInvoices(_model.ExplainZip, q)),
                _model.ConfirmedInvoices,
                _view.ConfirmedInvoicesGridView,
                _view.ConfirmedPager)
                .WithLogger(_logger);
        }
    }
}
