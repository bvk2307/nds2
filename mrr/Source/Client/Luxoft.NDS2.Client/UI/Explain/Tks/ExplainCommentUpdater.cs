﻿using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Common.Contracts.Services;
using System;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;

namespace Luxoft.NDS2.Client.UI.Explain.Tks
{
    public class ExplainCommentUpdater : IExplainCommentUpdater
    {
        private readonly IExplainReplyDataService _service;

        private readonly IClientLogger _logger;

        private readonly ExplainType _explainType;

        public ExplainCommentUpdater(
            IExplainReplyDataService service,
            IClientLogger logger,
            ExplainType explainType)
        {
            _service = service;
            _logger = logger;
            _explainType = explainType;
        }

        public void Update(long explainId, string commentText)
        {
            try
            {
                _service.UpdateComment(explainId, _explainType, commentText);
            }
            catch(Exception error)
            {
                _logger.LogError(error);
            }
        }
    }
}
