﻿namespace Luxoft.NDS2.Client.UI.Explain.Tks.Other
{
    partial class OtherReasonExplainView
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this._taxPayerReplyText = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.tableLayoutExplainPanel = new System.Windows.Forms.TableLayoutPanel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this._taxPayerReplyText)).BeginInit();
            this.tableLayoutExplainPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // _taxPayerReplyText
            // 
            this._taxPayerReplyText.Dock = System.Windows.Forms.DockStyle.Fill;
            this._taxPayerReplyText.Location = new System.Drawing.Point(3, 23);
            this._taxPayerReplyText.Multiline = true;
            this._taxPayerReplyText.Name = "_taxPayerReplyText";
            this._taxPayerReplyText.ReadOnly = true;
            this._taxPayerReplyText.Size = new System.Drawing.Size(1448, 451);
            this._taxPayerReplyText.TabIndex = 6;
            // 
            // tableLayoutExplainPanel
            // 
            this.tableLayoutExplainPanel.ColumnCount = 1;
            this.tableLayoutExplainPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutExplainPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutExplainPanel.Controls.Add(this._taxPayerReplyText, 0, 1);
            this.tableLayoutExplainPanel.Controls.Add(this.ultraLabel1, 0, 0);
            this.tableLayoutExplainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutExplainPanel.Location = new System.Drawing.Point(0, 123);
            this.tableLayoutExplainPanel.Name = "tableLayoutExplainPanel";
            this.tableLayoutExplainPanel.RowCount = 2;
            this.tableLayoutExplainPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutExplainPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutExplainPanel.Size = new System.Drawing.Size(1454, 477);
            this.tableLayoutExplainPanel.TabIndex = 7;
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Location = new System.Drawing.Point(3, 3);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(222, 14);
            this.ultraLabel1.TabIndex = 7;
            this.ultraLabel1.Text = "Пояснения по иным основаниям";
            // 
            // OtherReasonExplainView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutExplainPanel);
            this.Name = "OtherReasonExplainView";
            this.Controls.SetChildIndex(this.tableLayoutExplainPanel, 0);
            ((System.ComponentModel.ISupportInitialize)(this._taxPayerReplyText)).EndInit();
            this.tableLayoutExplainPanel.ResumeLayout(false);
            this.tableLayoutExplainPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinEditors.UltraTextEditor _taxPayerReplyText;
        private System.Windows.Forms.TableLayoutPanel tableLayoutExplainPanel;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
    }
}
