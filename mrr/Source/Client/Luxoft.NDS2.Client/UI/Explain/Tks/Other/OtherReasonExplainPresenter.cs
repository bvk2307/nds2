﻿using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Base.Navigation;
using Luxoft.NDS2.Client.UI.Explain.Tks.Invoice;

namespace Luxoft.NDS2.Client.UI.Explain.Tks.Other
{
    public class OtherReasonExplainPresenter : ExplainDetailsPresenterBase
    {
        public OtherReasonExplainPresenter(
            IOtherReasonExplainView view,
            OtherReasonExplainModel model,
            IClaimCardOpener claimCardOpener,
            ITaxPayerDetails taxpayerOpener,
            IExplainCommentUpdater commentUpdater,
            IClientLogger logger)
            : base(view, model, claimCardOpener, taxpayerOpener, commentUpdater, logger)
        {
            view.TaxPayerReplyText = model.TaxPayerReply;
        }
    }
}
