﻿namespace Luxoft.NDS2.Client.UI.Explain.Tks.Other
{
    public partial class OtherReasonExplainView : ExplainDetailsViewBase, IOtherReasonExplainView
    {
        public OtherReasonExplainView()
        {
            InitializeComponent();
            _explainType.Text = "Пояснения по иным основаниям №";
        }

        public string TaxPayerReplyText
        {
            set { _taxPayerReplyText.Text = value; }
        }
    }
}
