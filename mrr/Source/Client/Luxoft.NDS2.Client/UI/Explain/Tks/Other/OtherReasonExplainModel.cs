﻿using Luxoft.NDS2.Client.UI.Controls.Sur;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;

namespace Luxoft.NDS2.Client.UI.Explain.Tks.Other
{
    public class OtherReasonExplainModel : ExplainDetailsModel
    {
        public OtherReasonExplainModel(
            ExplainDetailsData explainData,
            DeclarationSummary declarationData,
            DictionarySur surDictionary)
            : base(explainData, declarationData, surDictionary)
        {
            TaxPayerReply = explainData.Reply;
        }

        public string TaxPayerReply
        {
            get;
            private set;
        }
    }
}
