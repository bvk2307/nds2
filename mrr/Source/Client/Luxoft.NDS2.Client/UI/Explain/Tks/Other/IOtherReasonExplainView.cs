﻿namespace Luxoft.NDS2.Client.UI.Explain.Tks.Other
{
    public interface IOtherReasonExplainView : IExplainDetailsView
    {
        string TaxPayerReplyText
        {
            set;
        }
    }
}
