﻿using Luxoft.NDS2.Common.Contracts.DTO.Explain;

namespace Luxoft.NDS2.Client.UI.Explain.Tks
{
    public interface IExplainCommentUpdater
    {
        void Update(long explainId, string commentText);
    }
}
