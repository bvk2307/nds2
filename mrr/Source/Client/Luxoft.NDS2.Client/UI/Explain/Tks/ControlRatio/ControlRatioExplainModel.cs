﻿using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Client.UI.Controls.Sur;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using Luxoft.NDS2.Common.Models;
using System.Collections.Generic;
using System.ComponentModel;

namespace Luxoft.NDS2.Client.UI.Explain.Tks.ControlRatio
{
    public class ControlRatioExplainModel : 
        ExplainDetailsModel, 
        IGridViewDataSource<ExplainControlRatio>, 
        IListViewModel<ExplainControlRatio, ExplainControlRatio>
    {
        public ControlRatioExplainModel(
            ExplainDetailsData explainData,
            DeclarationSummary declarationData,
            DictionarySur surDictionary)
            : base(explainData, declarationData, surDictionary)
        {
            ListItems = new BindingList<ExplainControlRatio>();
        }

        public BindingList<ExplainControlRatio> ListItems
        {
            get;
            private set;
        }

        public void UpdateList(IEnumerable<ExplainControlRatio> dataItems)
        {
            ListItems.Clear();

            foreach (var item in dataItems)
            {
                ListItems.Add(item);
            }
        }
    }
}
