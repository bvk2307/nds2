﻿using Luxoft.NDS2.Client.UI.Controls.Grid;

namespace Luxoft.NDS2.Client.UI.Explain.Tks.ControlRatio
{
    public partial class ControlRatioExplainView : ExplainDetailsViewBase, IControlRatioExplainView
    {
        public ControlRatioExplainView()
        {
            InitializeComponent();
            _explainType.Text = "Пояснение по КС №";
        }

        public IGridView ControlRatioList
        {
            get { return _explainControlRatioList; }
        }
    }
}
