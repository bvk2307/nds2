﻿using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Base.Navigation;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Explain.Tks.ControlRatio
{
    public class ControlRatioExplainPresenter : ExplainDetailsPresenterBase
    {
        private readonly IGridViewPresenter _controlRatio;

        public ControlRatioExplainPresenter(
            IClientGridDataProvider<List<ExplainControlRatio>> controlRatioDataProvider,
            IControlRatioExplainView view,
            ControlRatioExplainModel model,
            IClaimCardOpener claimCardOpener,
            ITaxPayerDetails taxPayerOpener,
            IExplainCommentUpdater commentUpdater,
            IClientLogger logger)
            : base(view, model, claimCardOpener, taxPayerOpener, commentUpdater, logger)
        {
            _controlRatio =
                new ClientGridViewPresenter<ExplainControlRatio, ExplainControlRatio, List<ExplainControlRatio>>(
                    view.ControlRatioList,
                    model,
                    controlRatioDataProvider);
            view.Opened += ViewOpened;
        }

        private void ViewOpened(object sender, EventArgs e)
        {
            _controlRatio.BeginLoad();
        }
    }
}
