﻿using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using Luxoft.NDS2.Common.Contracts.Services;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Explain.Tks.ControlRatio
{
    public class ExplainControlRatioLoader : ServiceProxyBase<List<ExplainControlRatio>>, IClientGridDataProvider<List<ExplainControlRatio>>
    {
        private readonly IExplainReplyDataService _service;

        private readonly long _zip;

        public ExplainControlRatioLoader(
            IExplainReplyDataService service,
            long zip,
            INotifier notifier,
            IClientLogger logger)
            : base(notifier, logger)
        {
            _service = service;
            _zip = zip;
        }

        public void StartLoading()
        {
            BeginInvoke(() => _service.GetExplainControlRatio(_zip));
        }

        protected override void CallBack(List<ExplainControlRatio> result)
        {
            if (DataLoaded != null)
            {
                DataLoaded(this, new DataLoadedEventArgs<List<ExplainControlRatio>>(result));
            }
        }

        public event EventHandler<DataLoadedEventArgs<List<ExplainControlRatio>>> DataLoaded;
    }
}
