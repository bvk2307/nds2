﻿namespace Luxoft.NDS2.Client.UI.Explain.Tks.ControlRatio
{
    partial class ControlRatioExplainView
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this._explainControlRatioList = new Luxoft.NDS2.Client.UI.Explain.Tks.ControlRatio.ExplainControlRatioListView();
            this.SuspendLayout();
            // 
            // _explainControlRatioList
            // 
            this._explainControlRatioList.Dock = System.Windows.Forms.DockStyle.Fill;
            this._explainControlRatioList.Location = new System.Drawing.Point(0, 123);
            this._explainControlRatioList.Name = "_explainControlRatioList";
            this._explainControlRatioList.Size = new System.Drawing.Size(1454, 477);
            this._explainControlRatioList.TabIndex = 6;
            // 
            // ControlRatioExplainView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._explainControlRatioList);
            this.Name = "ControlRatioExplainView";
            this.Controls.SetChildIndex(this._explainControlRatioList, 0);
            this.ResumeLayout(false);

        }

        #endregion

        private ExplainControlRatioListView _explainControlRatioList;
    }
}
