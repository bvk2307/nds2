﻿using Luxoft.NDS2.Client.UI.Controls.Grid;

namespace Luxoft.NDS2.Client.UI.Explain.Tks.ControlRatio
{
    public interface IControlRatioExplainView : IExplainDetailsView
    {
        IGridView ControlRatioList
        {
            get;
        }
    }
}
