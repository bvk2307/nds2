﻿using Luxoft.NDS2.Client.UI.Controls.Grid;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Explain.Tks.ControlRatio
{
    public partial class ExplainControlRatioListView : UserControl, IGridView
    {
        public ExplainControlRatioListView()
        {
            InitializeComponent();
        }

        public void SetDataSource(object dataSource)
        {
            _bindingSource.DataSource = dataSource;
        }
    }
}
