﻿using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Base.Navigation;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Providers;
using Luxoft.NDS2.Client.UI.Controls.Sur;
using Luxoft.NDS2.Client.UI.Explain.Tks.Invoice;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using list = Luxoft.NDS2.Common.Models.KnpDocuments;
using Microsoft.Practices.CompositeUI;

namespace Luxoft.NDS2.Client.UI.Explain.Tks
{
    public class TksInvoiceExplainViewer : ExplainDetailsViewerBase<TksInvoiceExplainView, TksInvoiceExplainModel, list.TksInvoiceExplainModel>
    {

        public TksInvoiceExplainViewer(
            WorkItem workItem,
            DictionarySur sur,
            IClaimCardOpener claimCardOpener,
            ITaxPayerDetails taxPayerOpener)
            : base(workItem, sur, claimCardOpener, taxPayerOpener, ExplainType.Invoice)
        {
        }

        protected override string GetWindowTitle(string inn)
        {
            return string.Format(ResourceManagerNDS2.ExplainMessages.FormalExplainTitle, inn);
        }

        protected override TksInvoiceExplainModel CreateModel(ExplainDetailsData explainData, DeclarationSummary declarationData)
        {
            return new TksInvoiceExplainModel(explainData, declarationData, _surValuesDictionary);
        }

        protected override ExplainDetailsPresenterBase CreatePresenter(TksInvoiceExplainView view, TksInvoiceExplainModel model, WorkItem workItem)
        {
            return new TksInvoiceExplainPresenter(
                view,
                model,
                _claimCardOpener,
                _taxpayerOpener,
                _invoiceService,
                _commentUpdater,
                key => new CommonSettingsProvider(workItem, key),
                _logger);
        }
    }
}
