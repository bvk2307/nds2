﻿using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Base.Navigation;
using Luxoft.NDS2.Client.UI.Controls.Sur;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using Luxoft.NDS2.Common.Contracts.Services;
using Luxoft.NDS2.Common.Models.KnpDocuments;
using Microsoft.Practices.CompositeUI;
using System;


namespace Luxoft.NDS2.Client.UI.Explain.Tks
{
    public abstract class ExplainDetailsViewerBase<TView, TModel, TDocument> : IKnpDocumentDetailsViewer
        where TView : IExplainDetailsView
        where TModel : ExplainDetailsModel
        where TDocument : KnpDocument
    {
        # region .ctor

        private readonly WorkItem _workItem;

        protected readonly DictionarySur _surValuesDictionary;

        protected readonly IClaimCardOpener _claimCardOpener;

        protected readonly ITaxPayerDetails _taxpayerOpener;

        protected readonly INotifier _notifier;

        protected readonly IClientLogger _logger;

        protected readonly IExplainReplyDataService _dataService;

        protected readonly IExplainTksInvoiceService _invoiceService;

        private readonly ExplainType _explainType;

        protected readonly IExplainCommentUpdater _commentUpdater;

        protected ExplainDetailsViewerBase(
            WorkItem workItem, 
            DictionarySur sur, 
            IClaimCardOpener claimCardOpener, 
            ITaxPayerDetails taxPayerOpener,
            ExplainType explainType)
        {
            _workItem = workItem;
            _surValuesDictionary = sur;
            _claimCardOpener = claimCardOpener;
            _taxpayerOpener = taxPayerOpener;
            _explainType = explainType;
            _notifier = _workItem.Services.Get<INotifier>(true);
            _logger = _workItem.Services.Get<IClientLogger>(true);
            _dataService = _workItem.GetWcfServiceProxy<IExplainReplyDataService>();
            _invoiceService = _workItem.GetWcfServiceProxy<IExplainTksInvoiceService>();
            _commentUpdater = new ExplainCommentUpdater(_dataService, _logger, _explainType);
        }

        # endregion

        # region IKnpDocumentDetailsViewer

        public bool Supports(KnpDocument knpDocument)
        {
            return knpDocument is TDocument;
        }

        public void View(KnpDocument knpDocument)
        {
            var explain = knpDocument as TDocument;
            ExplainDetailsData explainData;
            if (!_dataService.ExecuteNoHandling(
                proxy => proxy.GetExplainDetails(explain.GetId(), _explainType), 
                out explainData))
            {
                _notifier.ShowError("Ошибка загрузки данных");
                return;
            }

            DeclarationSummary declarationData;
            if (!_dataService.ExecuteNoHandling(
                proxy => proxy.GetDeclaration(explainData.DeclarationZip), 
                out declarationData))
            {
                _notifier.ShowError("Ошибка закгрузки данных");
                return;
            }

            _workItem.Show<TView>(
                (workItem, view) =>
                {
                    var model = CreateModel(explainData, declarationData);
                    var presenter = CreatePresenter(view, model, workItem);
                    view.Closed += (sender, e) => GC.KeepAlive(presenter);
                },
                GetWindowTitle(declarationData.INN));
        }

        public void Edit(KnpDocument knpDocument)
        {
            throw new NotSupportedException();
        }

        protected abstract string GetWindowTitle(string inn);

        protected abstract TModel CreateModel(ExplainDetailsData explainData, DeclarationSummary declarationData);

        protected abstract ExplainDetailsPresenterBase CreatePresenter(TView view, TModel model, WorkItem workItem);

        # endregion
    }
}
