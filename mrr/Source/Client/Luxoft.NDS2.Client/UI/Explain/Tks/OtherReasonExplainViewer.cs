﻿using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Base.Navigation;
using Luxoft.NDS2.Client.UI.Controls.Sur;
using Luxoft.NDS2.Client.UI.Explain.Tks.Other;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using list = Luxoft.NDS2.Common.Models.KnpDocuments;
using Microsoft.Practices.CompositeUI;

namespace Luxoft.NDS2.Client.UI.Explain.Tks
{
    public class OtherReasonExplainViewer : ExplainDetailsViewerBase<OtherReasonExplainView, OtherReasonExplainModel, list.OtherReasonExplainModel>
    {
        public OtherReasonExplainViewer(
            WorkItem workItem,
            DictionarySur sur,
            IClaimCardOpener claimCardOpener,
            ITaxPayerDetails taxPayerOpener)
            : base(workItem, sur, claimCardOpener, taxPayerOpener, ExplainType.OtherReason)
        {
        }

        protected override string GetWindowTitle(string inn)
        {
            return string.Format(ResourceManagerNDS2.ExplainMessages.ExplainOtherReasonTitle, inn);
        }

        protected override OtherReasonExplainModel CreateModel(ExplainDetailsData explainData, DeclarationSummary declarationData)
        {
            return new OtherReasonExplainModel(explainData, declarationData, _surValuesDictionary);
        }

        protected override ExplainDetailsPresenterBase CreatePresenter(OtherReasonExplainView view, OtherReasonExplainModel model, WorkItem workItem)
        {
            return new OtherReasonExplainPresenter(
                view,
                model,
                _claimCardOpener,
                _taxpayerOpener,
                _commentUpdater,
                _logger);
        }
    }
}
