﻿using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Sur;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Explain
{
    public partial class ExplainDetailsViewBase : BaseView, IExplainDetailsView
    {
        # region .ctor

        public ExplainDetailsViewBase()
        {
            InitializeComponent();
        }

        # endregion

        # region Общие

        public event EventHandler Opened;

        private void ViewVisibleChanged(object sender, EventArgs e)
        {
            var handler = Opened;
            if (Visible && handler != null)
            {
                handler(this, e);
            }
        }

        public event EventHandler Closed;

        public override void OnClosed(WindowCloseContextBase closeContext)
        {
            var handler = Closed;
            if (handler != null)
                handler(this, new EventArgs());
        }

        # endregion

        # region Данные пояснения

        private const int StringStartIndex = 0;

        private const int ExplainMaxLength = 5;

        private readonly ToolTip _explainNumberTooltip = new ToolTip();

        public string ExplainNumber
        {
            set
            {
                if (value.Length > ExplainMaxLength)
                {
                    _explainNumber.Text =
                        string.Format("{0}...", value.Substring(0, ExplainMaxLength));
                    _explainNumberTooltip.SetToolTip(_explainNumber, value);
                }
                else
                {
                    _explainNumber.Text = value;
                    _explainNumberTooltip.RemoveAll();
                }
            }
        }

        public string ExplainDate
        {
            set
            {
                _explainDate.Text = value;
            }
        }

        public string ExplainStatus
        {
            set
            {
                _explainStatus.Text = value;
            }
        }

        # endregion

        # region Данные Требования\Истребования

        public event EventHandler ClaimDetailsOpening;

        private void ClaimNumberClicked(object sender, EventArgs e)
        {
            var handler = ClaimDetailsOpening;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public string DocumentNumber
        {
            set
            {
                _claimLink.Value = String.Format(ResourceManagerNDS2.CommonStrings.Hyperlink, value);
            }
        }

        public string DocumentDate
        {
            set
            {
                _claimDate.Text = String.IsNullOrEmpty(value) ? String.Empty : String.Concat("от ", value);
            }
        }

        # endregion

        # region Данные декларации

        public event EventHandler DeclarationOpening;

        private void TaxPayerNameClicked(object sender, EventArgs e)
        {
            var handler = DeclarationOpening;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public string CorrectionNumber
        {
            set
            {
                _declarationCorrectionNumber.Text = value;
            }
        }

        public string TaxPeriod
        {
            set
            {
                _taxPeriod.Text = value;
            }
        }

        # endregion

        #region Данные налогоплательщика
        
        public string TaxPayerName
        {
            set
            {
                _taxPayerName.Value = string.Format(ResourceManagerNDS2.CommonStrings.Hyperlink, value);
            }
        }

        public string TaxPayerInn
        {
            set
            {
                _taxPayerInn.Text = value;
            }
        }

        public string TaxPayerKpp
        {
            set
            {
                _taxPayerKpp.Text = value;
            }
        }

        public DictionarySur TaxPayerSurValuesDictionary
        {
            set
            {
                _surIndicator.SetSurDictionary(value);
            }
        }

        public int? TaxPayerSur
        {
            set
            {
                _surIndicator.Code = value;
            }
        }

        #endregion

        #region Комментарий инспектора

        public string Comment
        {
            get
            {
                return _commentInput.Text;
            }
            set
            {
                _commentInput.Text = value;
            }
        }

        public event EventHandler CommentChanged;

        private void CommentTextChanged(object sender, EventArgs e)
        {
            var handler = CommentChanged;
            if (handler != null)
            {
                handler(this, new EventArgs());
            }
        }
        #endregion
    }
}
