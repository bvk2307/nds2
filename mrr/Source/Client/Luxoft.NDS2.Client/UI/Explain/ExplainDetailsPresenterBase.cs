﻿using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Base.Navigation;
using Luxoft.NDS2.Common.Contracts.Services;
using System;
using Luxoft.NDS2.Client.UI.Explain.Tks;

namespace Luxoft.NDS2.Client.UI.Explain
{
    public abstract class ExplainDetailsPresenterBase
    {
        # region .ctor

        protected readonly IClientLogger _logger;

        protected ExplainDetailsPresenterBase(
            IExplainDetailsView view, 
            ExplainDetailsModel model,
            IClaimCardOpener claimCardOpener,
            ITaxPayerDetails taxPayerOpener,
            IExplainCommentUpdater commentUpdater,
            IClientLogger logger)
        {
            _view = view;
            _model = model;
            _commentUpdater = commentUpdater;
            _logger = logger;
            _claimCardOpener = claimCardOpener;
            _taxpayerOpener = taxPayerOpener;

            InitView();
            BindView();
        }

        # endregion

        # region View

        private readonly IExplainDetailsView _view;

        private void BindView()
        {
            _view.CommentChanged += CommentChanged;
            _view.Closed += ViewClosed;
            _view.ClaimDetailsOpening += OpenClaim;
            _view.DeclarationOpening += OpenTaxpayerCard;
        }

        private void InitView()
        {
            _view.TaxPayerName = _model.TaxPayerName;
            _view.TaxPayerInn = _model.TaxPayerInn;
            _view.TaxPayerKpp = _model.TaxPayerKpp;
            _view.ExplainNumber = _model.ExplainNumber;
            _view.ExplainDate = _model.ExplainDate.ToShortDateString();
            _view.ExplainStatus = _model.DocumentStatus;
            _view.DocumentNumber = _model.DocumentNumber;
            _view.DocumentDate = _model.DocumentDate == null ? string.Empty : ((DateTime)_model.DocumentDate).ToShortDateString();
            _view.CorrectionNumber = _model.CorrectionNumber;
            _view.TaxPeriod = _model.FullTaxPeriod;
            _view.TaxPayerSurValuesDictionary = _model.TaxPayerSurDictionary;
            _view.TaxPayerSur = _model.TaxPayerSur;
            _view.Comment = _model.Comment;            
        }

        private void CommentChanged(object sender, EventArgs e)
        {
            _model.Comment = _view.Comment;
        }

        private void ViewClosed(object sender, EventArgs e)
        {
            UpdateComment();
        }

        # endregion

        # region ViewModel

        protected readonly ExplainDetailsModel _model;

        # endregion

        # region Сохранение комментария

        private readonly IExplainCommentUpdater _commentUpdater;

        private void UpdateComment()
        {
            if (_model.CommentChanged)
            {
                _commentUpdater.Update(_model.ExplainId, _model.Comment);
            }
        }

        # endregion

        # region Переход на карточку требования/истребования

        private readonly IClaimCardOpener _claimCardOpener;

        private void OpenClaim(object sender, EventArgs e)
        {
            _claimCardOpener.ViewClaimDetails(_model.DocumentId);
        }

        # endregion

        # region Переход на карточку НП

        private readonly ITaxPayerDetails _taxpayerOpener;

        private void OpenTaxpayerCard(object sender, EventArgs e)
        {
            _taxpayerOpener.ViewByKppEffective(_model.TaxPayerInn, _model.TaxPayerKppEffecive);
        }

        # endregion
    }
}
