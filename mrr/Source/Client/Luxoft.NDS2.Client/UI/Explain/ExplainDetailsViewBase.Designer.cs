﻿using Luxoft.NDS2.Client.UI.Controls.Sur;

namespace Luxoft.NDS2.Client.UI.Explain
{
    partial class ExplainDetailsViewBase
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Misc.UltraLabel labelStatusCaption;
            Infragistics.Win.Misc.UltraLabel labelCorrectionNumberCaption;
            Infragistics.Win.Misc.UltraLabel labelTaxPeriodCaption;
            Infragistics.Win.Misc.UltraLabel labelDocInfoNameValue;
            Infragistics.Win.Misc.UltraLabel labelSimvolNumber;
            Infragistics.Win.Misc.UltraLabel labelCommentCaption;
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel labelInnCaption;
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel labelKPPCaption;
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraExpandableGroupBoxPanel taxPayerInfoGroup;
            System.Windows.Forms.TableLayoutPanel taxPayerDetailsTable;
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraExpandableGroupBox taxPayerInfoArea;
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            this.tableLayoutTaxPayer = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutTaxPayerSecond = new System.Windows.Forms.TableLayoutPanel();
            this._taxPayerKpp = new Infragistics.Win.Misc.UltraLabel();
            this._taxPayerInn = new Infragistics.Win.Misc.UltraLabel();
            this.tableLayoutCommentMain = new System.Windows.Forms.TableLayoutPanel();
            this._commentInput = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.tableLayoutCommentCaption = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutTaxPayerName = new System.Windows.Forms.TableLayoutPanel();
            this._surIndicator = new Luxoft.NDS2.Client.UI.Controls.Sur.SurIcon();
            this._taxPayerName = new Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel();
            this.headerTable = new System.Windows.Forms.TableLayoutPanel();
            this._explainType = new Infragistics.Win.Misc.UltraLabel();
            this._explainDate = new Infragistics.Win.Misc.UltraLabel();
            this._explainStatus = new Infragistics.Win.Misc.UltraLabel();
            this._declarationCorrectionNumber = new Infragistics.Win.Misc.UltraLabel();
            this._taxPeriod = new Infragistics.Win.Misc.UltraLabel();
            this._claimLink = new Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel();
            this._claimDate = new Infragistics.Win.Misc.UltraLabel();
            this._explainNumber = new Infragistics.Win.Misc.UltraLabel();
            labelStatusCaption = new Infragistics.Win.Misc.UltraLabel();
            labelCorrectionNumberCaption = new Infragistics.Win.Misc.UltraLabel();
            labelTaxPeriodCaption = new Infragistics.Win.Misc.UltraLabel();
            labelDocInfoNameValue = new Infragistics.Win.Misc.UltraLabel();
            labelSimvolNumber = new Infragistics.Win.Misc.UltraLabel();
            labelCommentCaption = new Infragistics.Win.Misc.UltraLabel();
            labelInnCaption = new Infragistics.Win.Misc.UltraLabel();
            labelKPPCaption = new Infragistics.Win.Misc.UltraLabel();
            taxPayerInfoGroup = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            taxPayerDetailsTable = new System.Windows.Forms.TableLayoutPanel();
            taxPayerInfoArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            taxPayerInfoGroup.SuspendLayout();
            this.tableLayoutTaxPayer.SuspendLayout();
            this.tableLayoutTaxPayerSecond.SuspendLayout();
            taxPayerDetailsTable.SuspendLayout();
            this.tableLayoutCommentMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._commentInput)).BeginInit();
            this.tableLayoutCommentCaption.SuspendLayout();
            this.tableLayoutTaxPayerName.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(taxPayerInfoArea)).BeginInit();
            taxPayerInfoArea.SuspendLayout();
            this.headerTable.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelStatusCaption
            // 
            labelStatusCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            labelStatusCaption.Location = new System.Drawing.Point(271, 1);
            labelStatusCaption.Margin = new System.Windows.Forms.Padding(1);
            labelStatusCaption.Name = "labelStatusCaption";
            labelStatusCaption.Size = new System.Drawing.Size(43, 23);
            labelStatusCaption.TabIndex = 4;
            labelStatusCaption.Text = "Статус:";
            // 
            // labelCorrectionNumberCaption
            // 
            labelCorrectionNumberCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            labelCorrectionNumberCaption.Location = new System.Drawing.Point(756, 1);
            labelCorrectionNumberCaption.Margin = new System.Windows.Forms.Padding(1);
            labelCorrectionNumberCaption.Name = "labelCorrectionNumberCaption";
            labelCorrectionNumberCaption.Size = new System.Drawing.Size(75, 23);
            labelCorrectionNumberCaption.TabIndex = 6;
            labelCorrectionNumberCaption.Text = "Номер корр.:";
            // 
            // labelTaxPeriodCaption
            // 
            labelTaxPeriodCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            labelTaxPeriodCaption.Location = new System.Drawing.Point(954, 1);
            labelTaxPeriodCaption.Margin = new System.Windows.Forms.Padding(1);
            labelTaxPeriodCaption.Name = "labelTaxPeriodCaption";
            labelTaxPeriodCaption.Size = new System.Drawing.Size(103, 23);
            labelTaxPeriodCaption.TabIndex = 8;
            labelTaxPeriodCaption.Text = "Отчетный период:";
            // 
            // labelDocInfoNameValue
            // 
            labelDocInfoNameValue.Dock = System.Windows.Forms.DockStyle.Fill;
            labelDocInfoNameValue.Location = new System.Drawing.Point(506, 1);
            labelDocInfoNameValue.Margin = new System.Windows.Forms.Padding(1);
            labelDocInfoNameValue.Name = "labelDocInfoNameValue";
            labelDocInfoNameValue.Size = new System.Drawing.Size(118, 23);
            labelDocInfoNameValue.TabIndex = 10;
            labelDocInfoNameValue.Text = "к автотребованию №";
            // 
            // labelSimvolNumber
            // 
            labelSimvolNumber.Dock = System.Windows.Forms.DockStyle.Fill;
            labelSimvolNumber.Location = new System.Drawing.Point(164, 1);
            labelSimvolNumber.Margin = new System.Windows.Forms.Padding(1);
            labelSimvolNumber.Name = "labelSimvolNumber";
            labelSimvolNumber.Size = new System.Drawing.Size(18, 23);
            labelSimvolNumber.TabIndex = 15;
            labelSimvolNumber.Text = "от";
            // 
            // labelCommentCaption
            // 
            appearance3.TextHAlignAsString = "Right";
            labelCommentCaption.Appearance = appearance3;
            labelCommentCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            labelCommentCaption.Location = new System.Drawing.Point(1, 1);
            labelCommentCaption.Margin = new System.Windows.Forms.Padding(1);
            labelCommentCaption.Name = "labelCommentCaption";
            labelCommentCaption.Size = new System.Drawing.Size(98, 23);
            labelCommentCaption.TabIndex = 0;
            labelCommentCaption.Text = "Комментарий:";
            // 
            // labelInnCaption
            // 
            appearance2.TextHAlignAsString = "Right";
            labelInnCaption.Appearance = appearance2;
            labelInnCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            labelInnCaption.Location = new System.Drawing.Point(1, 1);
            labelInnCaption.Margin = new System.Windows.Forms.Padding(1);
            labelInnCaption.Name = "labelInnCaption";
            labelInnCaption.Size = new System.Drawing.Size(38, 23);
            labelInnCaption.TabIndex = 0;
            labelInnCaption.Text = "ИНН:";
            // 
            // labelKPPCaption
            // 
            appearance1.TextHAlignAsString = "Right";
            labelKPPCaption.Appearance = appearance1;
            labelKPPCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            labelKPPCaption.Location = new System.Drawing.Point(1, 26);
            labelKPPCaption.Margin = new System.Windows.Forms.Padding(1);
            labelKPPCaption.Name = "labelKPPCaption";
            labelKPPCaption.Size = new System.Drawing.Size(38, 23);
            labelKPPCaption.TabIndex = 0;
            labelKPPCaption.Text = "КПП:";
            // 
            // taxPayerInfoGroup
            // 
            taxPayerInfoGroup.Controls.Add(this.tableLayoutTaxPayer);
            taxPayerInfoGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            taxPayerInfoGroup.Location = new System.Drawing.Point(3, 19);
            taxPayerInfoGroup.Name = "taxPayerInfoGroup";
            taxPayerInfoGroup.Size = new System.Drawing.Size(1448, 76);
            taxPayerInfoGroup.TabIndex = 0;
            // 
            // tableLayoutTaxPayer
            // 
            this.tableLayoutTaxPayer.ColumnCount = 1;
            this.tableLayoutTaxPayer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutTaxPayer.Controls.Add(this.tableLayoutTaxPayerSecond, 0, 1);
            this.tableLayoutTaxPayer.Controls.Add(this.tableLayoutTaxPayerName, 0, 0);
            this.tableLayoutTaxPayer.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutTaxPayer.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutTaxPayer.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutTaxPayer.Name = "tableLayoutTaxPayer";
            this.tableLayoutTaxPayer.RowCount = 3;
            this.tableLayoutTaxPayer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutTaxPayer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutTaxPayer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutTaxPayer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutTaxPayer.Size = new System.Drawing.Size(1448, 76);
            this.tableLayoutTaxPayer.TabIndex = 3;
            // 
            // tableLayoutTaxPayerSecond
            // 
            this.tableLayoutTaxPayerSecond.ColumnCount = 2;
            this.tableLayoutTaxPayerSecond.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutTaxPayerSecond.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutTaxPayerSecond.Controls.Add(taxPayerDetailsTable, 0, 0);
            this.tableLayoutTaxPayerSecond.Controls.Add(this.tableLayoutCommentMain, 1, 0);
            this.tableLayoutTaxPayerSecond.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutTaxPayerSecond.Location = new System.Drawing.Point(0, 25);
            this.tableLayoutTaxPayerSecond.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutTaxPayerSecond.Name = "tableLayoutTaxPayerSecond";
            this.tableLayoutTaxPayerSecond.RowCount = 1;
            this.tableLayoutTaxPayerSecond.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutTaxPayerSecond.Size = new System.Drawing.Size(1448, 50);
            this.tableLayoutTaxPayerSecond.TabIndex = 0;
            // 
            // taxPayerDetailsTable
            // 
            taxPayerDetailsTable.ColumnCount = 2;
            taxPayerDetailsTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            taxPayerDetailsTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            taxPayerDetailsTable.Controls.Add(labelKPPCaption, 0, 1);
            taxPayerDetailsTable.Controls.Add(this._taxPayerKpp, 1, 1);
            taxPayerDetailsTable.Controls.Add(labelInnCaption, 0, 0);
            taxPayerDetailsTable.Controls.Add(this._taxPayerInn, 1, 0);
            taxPayerDetailsTable.Dock = System.Windows.Forms.DockStyle.Fill;
            taxPayerDetailsTable.Location = new System.Drawing.Point(0, 0);
            taxPayerDetailsTable.Margin = new System.Windows.Forms.Padding(0);
            taxPayerDetailsTable.Name = "taxPayerDetailsTable";
            taxPayerDetailsTable.RowCount = 2;
            taxPayerDetailsTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            taxPayerDetailsTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            taxPayerDetailsTable.Size = new System.Drawing.Size(150, 50);
            taxPayerDetailsTable.TabIndex = 0;
            // 
            // _taxPayerKpp
            // 
            this._taxPayerKpp.Dock = System.Windows.Forms.DockStyle.Fill;
            this._taxPayerKpp.Location = new System.Drawing.Point(41, 26);
            this._taxPayerKpp.Margin = new System.Windows.Forms.Padding(1);
            this._taxPayerKpp.Name = "_taxPayerKpp";
            this._taxPayerKpp.Size = new System.Drawing.Size(118, 23);
            this._taxPayerKpp.TabIndex = 1;
            this._taxPayerKpp.Text = "123456789";
            // 
            // _taxPayerInn
            // 
            this._taxPayerInn.Dock = System.Windows.Forms.DockStyle.Fill;
            this._taxPayerInn.Location = new System.Drawing.Point(41, 1);
            this._taxPayerInn.Margin = new System.Windows.Forms.Padding(1);
            this._taxPayerInn.Name = "_taxPayerInn";
            this._taxPayerInn.Size = new System.Drawing.Size(118, 23);
            this._taxPayerInn.TabIndex = 1;
            this._taxPayerInn.Text = "1234567893";
            // 
            // tableLayoutCommentMain
            // 
            this.tableLayoutCommentMain.ColumnCount = 2;
            this.tableLayoutCommentMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutCommentMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutCommentMain.Controls.Add(this._commentInput, 1, 0);
            this.tableLayoutCommentMain.Controls.Add(this.tableLayoutCommentCaption, 0, 0);
            this.tableLayoutCommentMain.Location = new System.Drawing.Point(150, 0);
            this.tableLayoutCommentMain.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutCommentMain.Name = "tableLayoutCommentMain";
            this.tableLayoutCommentMain.RowCount = 1;
            this.tableLayoutCommentMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutCommentMain.Size = new System.Drawing.Size(1298, 50);
            this.tableLayoutCommentMain.TabIndex = 3;
            // 
            // _commentInput
            // 
            this._commentInput.AlwaysInEditMode = true;
            this._commentInput.Dock = System.Windows.Forms.DockStyle.Fill;
            this._commentInput.Location = new System.Drawing.Point(101, 1);
            this._commentInput.Margin = new System.Windows.Forms.Padding(1);
            this._commentInput.Multiline = true;
            this._commentInput.Name = "_commentInput";
            this._commentInput.Scrollbars = System.Windows.Forms.ScrollBars.Vertical;
            this._commentInput.Size = new System.Drawing.Size(1196, 48);
            this._commentInput.TabIndex = 0;
            this._commentInput.TextChanged += new System.EventHandler(this.CommentTextChanged);
            // 
            // tableLayoutCommentCaption
            // 
            this.tableLayoutCommentCaption.ColumnCount = 1;
            this.tableLayoutCommentCaption.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutCommentCaption.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutCommentCaption.Controls.Add(labelCommentCaption, 0, 0);
            this.tableLayoutCommentCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutCommentCaption.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutCommentCaption.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutCommentCaption.Name = "tableLayoutCommentCaption";
            this.tableLayoutCommentCaption.RowCount = 2;
            this.tableLayoutCommentCaption.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutCommentCaption.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutCommentCaption.Size = new System.Drawing.Size(100, 50);
            this.tableLayoutCommentCaption.TabIndex = 1;
            // 
            // tableLayoutTaxPayerName
            // 
            this.tableLayoutTaxPayerName.ColumnCount = 3;
            this.tableLayoutTaxPayerName.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutTaxPayerName.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 950F));
            this.tableLayoutTaxPayerName.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 468F));
            this.tableLayoutTaxPayerName.Controls.Add(this._surIndicator, 0, 0);
            this.tableLayoutTaxPayerName.Controls.Add(this._taxPayerName, 1, 0);
            this.tableLayoutTaxPayerName.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutTaxPayerName.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutTaxPayerName.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutTaxPayerName.Name = "tableLayoutTaxPayerName";
            this.tableLayoutTaxPayerName.RowCount = 1;
            this.tableLayoutTaxPayerName.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutTaxPayerName.Size = new System.Drawing.Size(1448, 25);
            this.tableLayoutTaxPayerName.TabIndex = 0;
            // 
            // _surIndicator
            // 
            this._surIndicator.BackColor = System.Drawing.Color.Transparent;
            this._surIndicator.Code = null;
            this._surIndicator.Location = new System.Drawing.Point(3, 3);
            this._surIndicator.MaximumSize = new System.Drawing.Size(20, 20);
            this._surIndicator.MinimumSize = new System.Drawing.Size(20, 20);
            this._surIndicator.Name = "_surIndicator";
            this._surIndicator.Size = new System.Drawing.Size(20, 20);
            this._surIndicator.TabIndex = 4;
            // 
            // _taxPayerName
            // 
            appearance4.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this._taxPayerName.Appearance = appearance4;
            this._taxPayerName.Location = new System.Drawing.Point(31, 1);
            this._taxPayerName.Margin = new System.Windows.Forms.Padding(1);
            this._taxPayerName.Name = "_taxPayerName";
            this._taxPayerName.Size = new System.Drawing.Size(498, 23);
            this._taxPayerName.TabIndex = 3;
            this._taxPayerName.TabStop = true;
            this._taxPayerName.Value = "<a href=\"#\">Наименование налогоплательщика с геперлинком</a>";
            this._taxPayerName.Click += new System.EventHandler(this.TaxPayerNameClicked);
            // 
            // taxPayerInfoArea
            // 
            taxPayerInfoArea.Controls.Add(taxPayerInfoGroup);
            taxPayerInfoArea.Dock = System.Windows.Forms.DockStyle.Top;
            taxPayerInfoArea.ExpandedSize = new System.Drawing.Size(1454, 98);
            taxPayerInfoArea.Location = new System.Drawing.Point(0, 25);
            taxPayerInfoArea.Name = "taxPayerInfoArea";
            taxPayerInfoArea.Size = new System.Drawing.Size(1454, 98);
            taxPayerInfoArea.TabIndex = 5;
            taxPayerInfoArea.Text = "Данные налогоплательщика";
            // 
            // headerTable
            // 
            this.headerTable.BackColor = System.Drawing.Color.Transparent;
            this.headerTable.ColumnCount = 14;
            this.headerTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.headerTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this.headerTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.headerTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 87F));
            this.headerTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.headerTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 190F));
            this.headerTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.headerTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.headerTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.headerTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 77F));
            this.headerTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 121F));
            this.headerTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 105F));
            this.headerTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 105F));
            this.headerTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.headerTable.Controls.Add(this._explainType, 0, 0);
            this.headerTable.Controls.Add(this._explainDate, 3, 0);
            this.headerTable.Controls.Add(labelStatusCaption, 4, 0);
            this.headerTable.Controls.Add(this._explainStatus, 5, 0);
            this.headerTable.Controls.Add(labelCorrectionNumberCaption, 9, 0);
            this.headerTable.Controls.Add(this._declarationCorrectionNumber, 10, 0);
            this.headerTable.Controls.Add(labelTaxPeriodCaption, 11, 0);
            this.headerTable.Controls.Add(this._taxPeriod, 12, 0);
            this.headerTable.Controls.Add(labelDocInfoNameValue, 6, 0);
            this.headerTable.Controls.Add(this._claimLink, 7, 0);
            this.headerTable.Controls.Add(this._claimDate, 8, 0);
            this.headerTable.Controls.Add(this._explainNumber, 1, 0);
            this.headerTable.Controls.Add(labelSimvolNumber, 2, 0);
            this.headerTable.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerTable.Location = new System.Drawing.Point(0, 0);
            this.headerTable.Margin = new System.Windows.Forms.Padding(0);
            this.headerTable.Name = "headerTable";
            this.headerTable.RowCount = 2;
            this.headerTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.headerTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.headerTable.Size = new System.Drawing.Size(1454, 25);
            this.headerTable.TabIndex = 4;
            // 
            // _explainType
            // 
            this._explainType.AutoSize = true;
            this._explainType.Dock = System.Windows.Forms.DockStyle.Fill;
            this._explainType.Location = new System.Drawing.Point(1, 1);
            this._explainType.Margin = new System.Windows.Forms.Padding(1);
            this._explainType.Name = "_explainType";
            this._explainType.Size = new System.Drawing.Size(113, 23);
            this._explainType.TabIndex = 0;
            this._explainType.Text = "Пояснение по СФ №";
            // 
            // _explainDate
            // 
            this._explainDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this._explainDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._explainDate.Location = new System.Drawing.Point(184, 1);
            this._explainDate.Margin = new System.Windows.Forms.Padding(1);
            this._explainDate.Name = "_explainDate";
            this._explainDate.Size = new System.Drawing.Size(85, 23);
            this._explainDate.TabIndex = 3;
            this._explainDate.Text = "29.04.2015";
            // 
            // _explainStatus
            // 
            this._explainStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this._explainStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._explainStatus.Location = new System.Drawing.Point(316, 1);
            this._explainStatus.Margin = new System.Windows.Forms.Padding(1);
            this._explainStatus.Name = "_explainStatus";
            this._explainStatus.Size = new System.Drawing.Size(188, 23);
            this._explainStatus.TabIndex = 5;
            this._explainStatus.Text = "Обработано в МС";
            // 
            // _declarationCorrectionNumber
            // 
            this._declarationCorrectionNumber.Dock = System.Windows.Forms.DockStyle.Fill;
            this._declarationCorrectionNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._declarationCorrectionNumber.Location = new System.Drawing.Point(833, 1);
            this._declarationCorrectionNumber.Margin = new System.Windows.Forms.Padding(1);
            this._declarationCorrectionNumber.Name = "_declarationCorrectionNumber";
            this._declarationCorrectionNumber.Size = new System.Drawing.Size(119, 23);
            this._declarationCorrectionNumber.TabIndex = 7;
            this._declarationCorrectionNumber.Text = "999 (123456789098)";
            // 
            // _taxPeriod
            // 
            this._taxPeriod.Dock = System.Windows.Forms.DockStyle.Fill;
            this._taxPeriod.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._taxPeriod.Location = new System.Drawing.Point(1059, 1);
            this._taxPeriod.Margin = new System.Windows.Forms.Padding(1);
            this._taxPeriod.Name = "_taxPeriod";
            this._taxPeriod.Size = new System.Drawing.Size(103, 23);
            this._taxPeriod.TabIndex = 9;
            this._taxPeriod.Text = "сентябрь 2014";
            // 
            // _claimLink
            // 
            appearance11.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance11.TextVAlignAsString = "Top";
            this._claimLink.Appearance = appearance11;
            this._claimLink.Dock = System.Windows.Forms.DockStyle.Top;
            appearance12.TextVAlignAsString = "Top";
            this._claimLink.HotTrackLinkAppearance = appearance12;
            appearance13.TextVAlignAsString = "Top";
            this._claimLink.LinkAppearance = appearance13;
            this._claimLink.Location = new System.Drawing.Point(626, 0);
            this._claimLink.Margin = new System.Windows.Forms.Padding(1, 0, 1, 3);
            this._claimLink.Name = "_claimLink";
            this._claimLink.Size = new System.Drawing.Size(38, 19);
            this._claimLink.TabIndex = 12;
            this._claimLink.TabStop = true;
            this._claimLink.Value = "<a href=\'#\'>12</a>";
            this._claimLink.Click += new System.EventHandler(this.ClaimNumberClicked);
            // 
            // _claimDate
            // 
            this._claimDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this._claimDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._claimDate.Location = new System.Drawing.Point(666, 1);
            this._claimDate.Margin = new System.Windows.Forms.Padding(1);
            this._claimDate.Name = "_claimDate";
            this._claimDate.Size = new System.Drawing.Size(88, 23);
            this._claimDate.TabIndex = 13;
            this._claimDate.Text = "от 23.01.2015";
            // 
            // _explainNumber
            // 
            this._explainNumber.AutoSize = true;
            this._explainNumber.Dock = System.Windows.Forms.DockStyle.Fill;
            this._explainNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._explainNumber.Location = new System.Drawing.Point(116, 1);
            this._explainNumber.Margin = new System.Windows.Forms.Padding(1);
            this._explainNumber.Name = "_explainNumber";
            this._explainNumber.Size = new System.Drawing.Size(46, 23);
            this._explainNumber.TabIndex = 14;
            this._explainNumber.Text = "12345";
            // 
            // ExplainDetailsViewBase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(taxPayerInfoArea);
            this.Controls.Add(this.headerTable);
            this.Name = "ExplainDetailsViewBase";
            this.Size = new System.Drawing.Size(1454, 600);
            this.VisibleChanged += new System.EventHandler(this.ViewVisibleChanged);
            taxPayerInfoGroup.ResumeLayout(false);
            this.tableLayoutTaxPayer.ResumeLayout(false);
            this.tableLayoutTaxPayerSecond.ResumeLayout(false);
            taxPayerDetailsTable.ResumeLayout(false);
            this.tableLayoutCommentMain.ResumeLayout(false);
            this.tableLayoutCommentMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._commentInput)).EndInit();
            this.tableLayoutCommentCaption.ResumeLayout(false);
            this.tableLayoutTaxPayerName.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(taxPayerInfoArea)).EndInit();
            taxPayerInfoArea.ResumeLayout(false);
            this.headerTable.ResumeLayout(false);
            this.headerTable.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraLabel _explainDate;
        private Infragistics.Win.Misc.UltraLabel _explainStatus;
        private Infragistics.Win.Misc.UltraLabel _declarationCorrectionNumber;
        private Infragistics.Win.Misc.UltraLabel _taxPeriod;
        private Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel _claimLink;
        private Infragistics.Win.Misc.UltraLabel _claimDate;
        private Infragistics.Win.Misc.UltraLabel _explainNumber;
        private System.Windows.Forms.TableLayoutPanel tableLayoutTaxPayer;
        private System.Windows.Forms.TableLayoutPanel tableLayoutTaxPayerSecond;
        private Infragistics.Win.Misc.UltraLabel _taxPayerKpp;
        private Infragistics.Win.Misc.UltraLabel _taxPayerInn;
        private System.Windows.Forms.TableLayoutPanel tableLayoutCommentMain;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor _commentInput;
        private System.Windows.Forms.TableLayoutPanel tableLayoutCommentCaption;
        private System.Windows.Forms.TableLayoutPanel tableLayoutTaxPayerName;
        private SurIcon _surIndicator;
        private Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel _taxPayerName;
        private System.Windows.Forms.TableLayoutPanel headerTable;
        protected Infragistics.Win.Misc.UltraLabel _explainType;
    }
}
