﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.SettingsMRR.AssignEmployee.Dialog;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Services;
using Microsoft.Practices.CompositeUI;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.LoadingInspection
{
    public class Presenter : BasePresenter<View>
    {
        private IDataService dtService;


        public Presenter()
        {
        }

        public Presenter(PresentationContext presentationContext, WorkItem wi, View view)
            : base(presentationContext, wi, view)
        {
            dtService = base.GetServiceProxy<IDataService>();
        }

        public override void OnObjectCreated()
        {
            base.OnObjectCreated();
            dtService = base.GetServiceProxy<IDataService>();
        }

        public object GetData(QueryConditions input, out long output)
        {
            List<CFGRegion> rez = null;
            output = 0;

            ExecuteServiceCall(
                () => dtService.GetLoadingInspection(),
                result => rez = result.Result );

            if (rez != null)
                output = rez.Count;

            return rez;
        }

        public void ManageSoun(CFGRegion curItem)
        {
            var p = new MetodologSounDialogPresener(WorkItem, curItem.Region, GetServiceProxy<IUserToRegionService>(), WorkItem.Services.Get<IUcMessageService>());
            if (p.ShowDialog() == DialogResult.OK)
            {
                View.Grid.UpdateData();
            }
        }

        public void ValueChange(CFGRegion curItem)
        {
            ExecuteServiceCall(
                () => dtService.LoadingInspectionSetValue(curItem),
                result => curItem.Id = result.Result.Id );
        }
    }
}
