﻿using System;
using System.Drawing;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;
using Microsoft.Practices.CompositeUI;
using Microsoft.Practices.ObjectBuilder;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.LoadingInspection
{
    public partial class View : BaseView
    {
        private readonly PresentationContext _context;
        private Presenter _presenter;

        [CreateNew]
        public Presenter Presenter
        {
            set
            {
                _presenter = value;
                _presenter.PresentationContext = this._presentationContext;
                _presenter.View = this;
                this.WorkItem = _presenter.WorkItem;
                _presenter.OnObjectCreated();
            }
        }

        public GridControl Grid
        {
            get { return grid; }
        }

        public View()
        {
            InitializeComponent();
        }

        public View(PresentationContext ctx)
            : base(ctx)
        {
            _context = ctx;

            InitializeComponent();
        }

        public View(PresentationContext ctx, WorkItem wi)
            : base(ctx, wi)
        {
            _context = ctx;
            _presenter = new Presenter(ctx, wi, this);

            InitializeComponent();
        }

        private void View_Load(object sender, EventArgs e)
        {
            InitializeGrid();
        }

        private void InitializeGrid()
        {
            GridSetupHelper<CFGRegion> helper = new GridSetupHelper<CFGRegion>();
            GridSetup setup = _presenter.CommonSetup(string.Format("{0}_InitializeGrid", GetType()));

            setup.Columns.Add(helper.CreateColumnDefinition(o => o.RegionName, x => x.Width = 400));
            setup.Columns.Add(helper.CreateColumnDefinition(o => o.RegionInspections, x => 
            { 
                x.StyleDefinition.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton;
                x.StyleDefinition.Image = Luxoft.NDS2.Client.Properties.Resources.ThreeDots;
                x.StyleDefinition.Handler = Cell_ButtonClick;
                x.Width = 200;
            }));
            setup.Columns.Add(helper.CreateColumnDefinition(o => o.ParamValue, x =>
            {
                x.Caption = "ПВП расхождения (руб.)";
                x.ToolTip = "ПВП расхождения (руб.)";
                x.EditableDefinition.Editable = true;
                x.EditableDefinition.MaskInput = "nnnnnnnn";
                x.EditableDefinition.Handler = Cell_ValueChange;
                x.EditableDefinition.Nullable = Infragistics.Win.UltraWinGrid.Nullable.EmptyString;
                x.Width = 100;
            }));

            setup.GetData = _presenter.GetData;
            setup.RowActions.Add(SetGridRowColor);

            grid.Setup = setup;
        }

        private void Cell_ButtonClick(object sender, EventArgs e)
        {
            CFGRegion curItem = grid.GetCurrentItem<CFGRegion>();
            if (curItem != null && curItem.Region != null && curItem.Region.Code != "00")
            {
                _presenter.ManageSoun(curItem);
            }
        }

        private void Cell_ValueChange(object sender, EventArgs e)
        {
            CFGRegion curItem = grid.GetCurrentItem<CFGRegion>();
            if (curItem != null)
            {
                string val = (sender as UltraGrid).ActiveCell.EditorResolved.Value.ToString();
                curItem.ParamValue = val;
                _presenter.ValueChange(curItem);
                var row = (sender as UltraGrid).ActiveRow;
                SetGridRowColor(row);
            }
        }

        private void SetGridRowColor(UltraGridRow row)
        {
            var item = (CFGRegion)row.ListObject;

            if (!string.IsNullOrEmpty(item.ParamValue))
            {
                row.Appearance.ResetBackColor();
            }
            else
            {
                row.Appearance.BackColor = Color.LightPink;
            }
        }

    }
}
