﻿namespace Luxoft.NDS2.Client.UI.SettingsMRR.LoadingInspection
{
    partial class View
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grid = new Luxoft.NDS2.Client.UI.Controls.Grid.V1.GridControl();
            this.SuspendLayout();
            // 
            // grid
            // 
            this.grid.AddVirtualCheckColumn = false;
            this.grid.AggregatePanelVisible = false;
            this.grid.AllowMultiGrouping = false;
            this.grid.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.grid.BackColor = System.Drawing.Color.Transparent;
            this.grid.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.None;
            this.grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grid.FooterVisible = false;
            this.grid.Location = new System.Drawing.Point(0, 0);
            this.grid.Name = "grid";
            this.grid.PanelLoadingVisible = false;
            this.grid.PanelPagesVisible = false;
            this.grid.RowDoubleClicked = null;
            this.grid.Setup = null;
            this.grid.Size = new System.Drawing.Size(889, 515);
            this.grid.TabIndex = 0;
            this.grid.Title = "";
            // 
            // View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.grid);
            this.Name = "View";
            this.Size = new System.Drawing.Size(889, 515);
            this.Load += new System.EventHandler(this.View_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private Controls.Grid.V1.GridControl grid;
    }
}
