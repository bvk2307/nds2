﻿using System.Globalization;
using System.Windows.Forms;
using Infragistics.Win;
using Infragistics.Win.Misc;
using System;
using Appearance = Infragistics.Win.Appearance;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.Calendar
{
    internal sealed class DayLabel : UltraLabel
    {
        private bool _isRedDay;

        public DateTime Date { get; private set; }

        public bool IsRedDay
        {
            get { return _isRedDay; }
            set
            {
                _isRedDay = value;
                if (_isRedDay)
                {
                    Appearance.BackColor = System.Drawing.Color.FromArgb(255, 128, 128);
                    Appearance.BackColor2 = System.Drawing.Color.FromArgb(255, 128, 128);
                }
                else
                {
                    Appearance.BackColor = System.Drawing.Color.FromArgb(224, 224, 224);
                    Appearance.BackColor2 = System.Drawing.Color.FromArgb(224, 224, 224);
                }
            }
        }

        public DayLabel(DateTime date, bool isRed)
        {
            Date = date;

            var appearance = new Appearance
            {
                AlphaLevel = 255,
                BackColorAlpha = Alpha.Opaque,
                TextHAlign = HAlign.Center,
                TextVAlign = VAlign.Middle
            };
            Appearance = appearance;
            Dock = System.Windows.Forms.DockStyle.Fill;
            Name = "dayLabel" + Date.ToString("yyyyMMdd");
            UseAppStyling = false;
            Text = date.Day.ToString(CultureInfo.InvariantCulture);
            IsRedDay = isRed;
        }

    }

    internal sealed class MonthPanel : TableLayoutPanel
    {
        public MonthPanel(string month)
        {
            ColumnCount = 7;
            ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 14.28572F));
            ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 14.28572F));
            ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 14.28572F));
            ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 14.28572F));
            ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 14.28572F));
            ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 14.28572F));
            ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 14.28572F));
            RowCount = 6;
            RowStyles.Add(new RowStyle(SizeType.Percent, 14.28572F));
            RowStyles.Add(new RowStyle(SizeType.Percent, 14.28572F));
            RowStyles.Add(new RowStyle(SizeType.Percent, 14.28572F));
            RowStyles.Add(new RowStyle(SizeType.Percent, 14.28572F));
            RowStyles.Add(new RowStyle(SizeType.Percent, 14.28572F));
            RowStyles.Add(new RowStyle(SizeType.Percent, 14.28572F));
            RowStyles.Add(new RowStyle(SizeType.Percent, 14.28572F));
            var label = new UltraLabel() { Text = month, Dock = DockStyle.Fill };
            Controls.Add(label, 0, 0);
            SetColumnSpan(label, 7);
            Dock = DockStyle.Fill;
            Margin = new Padding(8);
            Name = "month";
        }
    }
}
