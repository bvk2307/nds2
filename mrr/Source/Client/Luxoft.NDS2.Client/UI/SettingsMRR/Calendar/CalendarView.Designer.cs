﻿using System.Drawing;
using Infragistics.Win.UltraWinEditors;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.Calendar
{
    partial class CalendarView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            this.BasePanel = new Infragistics.Win.Misc.UltraPanel();
            this.LayoutBasePanel = new System.Windows.Forms.TableLayoutPanel();
            this.PanelMonthes = new System.Windows.Forms.TableLayoutPanel();
            this.LayoutAutoSelectionGroup = new System.Windows.Forms.TableLayoutPanel();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.labelRedDay = new Infragistics.Win.Misc.UltraLabel();
            this.labelWeekDay = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.LabelAutoselectionPeriodFormingDaysMark = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.ComboYear = new System.Windows.Forms.ComboBox();
            this.BasePanel.ClientArea.SuspendLayout();
            this.BasePanel.SuspendLayout();
            this.LayoutBasePanel.SuspendLayout();
            this.LayoutAutoSelectionGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // BasePanel
            // 
            // 
            // BasePanel.ClientArea
            // 
            this.BasePanel.ClientArea.Controls.Add(this.LayoutBasePanel);
            this.BasePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BasePanel.Location = new System.Drawing.Point(0, 0);
            this.BasePanel.Name = "BasePanel";
            this.BasePanel.Size = new System.Drawing.Size(742, 594);
            this.BasePanel.TabIndex = 1;
            // 
            // LayoutBasePanel
            // 
            this.LayoutBasePanel.BackColor = System.Drawing.Color.Transparent;
            this.LayoutBasePanel.ColumnCount = 3;
            this.LayoutBasePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.LayoutBasePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.LayoutBasePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.LayoutBasePanel.Controls.Add(this.PanelMonthes, 1, 2);
            this.LayoutBasePanel.Controls.Add(this.LayoutAutoSelectionGroup, 1, 1);
            this.LayoutBasePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutBasePanel.Location = new System.Drawing.Point(0, 0);
            this.LayoutBasePanel.Name = "LayoutBasePanel";
            this.LayoutBasePanel.RowCount = 3;
            this.LayoutBasePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.LayoutBasePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.LayoutBasePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.LayoutBasePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.LayoutBasePanel.Size = new System.Drawing.Size(742, 594);
            this.LayoutBasePanel.TabIndex = 0;
            // 
            // PanelMonthes
            // 
            this.PanelMonthes.ColumnCount = 3;
            this.PanelMonthes.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.PanelMonthes.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.PanelMonthes.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.PanelMonthes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelMonthes.Location = new System.Drawing.Point(23, 88);
            this.PanelMonthes.Name = "PanelMonthes";
            this.PanelMonthes.RowCount = 4;
            this.PanelMonthes.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.PanelMonthes.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.PanelMonthes.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.PanelMonthes.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.PanelMonthes.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.PanelMonthes.Size = new System.Drawing.Size(696, 503);
            this.PanelMonthes.TabIndex = 2;
            // 
            // LayoutAutoSelectionGroup
            // 
            this.LayoutAutoSelectionGroup.ColumnCount = 4;
            this.LayoutAutoSelectionGroup.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.LayoutAutoSelectionGroup.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.LayoutAutoSelectionGroup.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.LayoutAutoSelectionGroup.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.LayoutAutoSelectionGroup.Controls.Add(this.ultraLabel5, 0, 2);
            this.LayoutAutoSelectionGroup.Controls.Add(this.labelRedDay, 2, 1);
            this.LayoutAutoSelectionGroup.Controls.Add(this.labelWeekDay, 2, 0);
            this.LayoutAutoSelectionGroup.Controls.Add(this.ultraLabel2, 3, 1);
            this.LayoutAutoSelectionGroup.Controls.Add(this.LabelAutoselectionPeriodFormingDaysMark, 3, 0);
            this.LayoutAutoSelectionGroup.Controls.Add(this.ultraLabel1, 0, 0);
            this.LayoutAutoSelectionGroup.Controls.Add(this.ComboYear, 1, 2);
            this.LayoutAutoSelectionGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutAutoSelectionGroup.Location = new System.Drawing.Point(23, 8);
            this.LayoutAutoSelectionGroup.Name = "LayoutAutoSelectionGroup";
            this.LayoutAutoSelectionGroup.RowCount = 3;
            this.LayoutAutoSelectionGroup.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.LayoutAutoSelectionGroup.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.LayoutAutoSelectionGroup.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.LayoutAutoSelectionGroup.Size = new System.Drawing.Size(696, 74);
            this.LayoutAutoSelectionGroup.TabIndex = 1;
            // 
            // ultraLabel5
            // 
            appearance5.TextHAlignAsString = "Left";
            appearance5.TextVAlignAsString = "Middle";
            this.ultraLabel5.Appearance = appearance5;
            this.ultraLabel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabel5.Location = new System.Drawing.Point(3, 51);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(34, 20);
            this.ultraLabel5.TabIndex = 6;
            this.ultraLabel5.Text = "Год";
            // 
            // labelRedDay
            // 
            appearance6.AlphaLevel = ((short)(255));
            appearance6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            appearance6.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            appearance6.BackColorAlpha = Infragistics.Win.Alpha.Opaque;
            appearance6.TextHAlignAsString = "Left";
            appearance6.TextVAlignAsString = "Middle";
            this.labelRedDay.Appearance = appearance6;
            this.labelRedDay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelRedDay.Location = new System.Drawing.Point(539, 27);
            this.labelRedDay.Name = "labelRedDay";
            this.labelRedDay.Size = new System.Drawing.Size(34, 18);
            this.labelRedDay.TabIndex = 5;
            this.labelRedDay.UseAppStyling = false;
            // 
            // labelWeekDay
            // 
            appearance7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            appearance7.TextHAlignAsString = "Left";
            appearance7.TextVAlignAsString = "Middle";
            this.labelWeekDay.Appearance = appearance7;
            this.labelWeekDay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelWeekDay.Location = new System.Drawing.Point(539, 3);
            this.labelWeekDay.Name = "labelWeekDay";
            this.labelWeekDay.Size = new System.Drawing.Size(34, 18);
            this.labelWeekDay.TabIndex = 4;
            this.labelWeekDay.Text = " ";
            this.labelWeekDay.UseAppStyling = false;
            // 
            // ultraLabel2
            // 
            appearance8.TextHAlignAsString = "Left";
            appearance8.TextVAlignAsString = "Middle";
            this.ultraLabel2.Appearance = appearance8;
            this.ultraLabel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabel2.Location = new System.Drawing.Point(579, 27);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(114, 18);
            this.ultraLabel2.TabIndex = 3;
            this.ultraLabel2.Text = "- нерабочие дни";
            // 
            // LabelAutoselectionPeriodFormingDaysMark
            // 
            appearance9.TextHAlignAsString = "Left";
            appearance9.TextVAlignAsString = "Middle";
            this.LabelAutoselectionPeriodFormingDaysMark.Appearance = appearance9;
            this.LabelAutoselectionPeriodFormingDaysMark.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LabelAutoselectionPeriodFormingDaysMark.Location = new System.Drawing.Point(579, 3);
            this.LabelAutoselectionPeriodFormingDaysMark.Name = "LabelAutoselectionPeriodFormingDaysMark";
            this.LabelAutoselectionPeriodFormingDaysMark.Size = new System.Drawing.Size(114, 18);
            this.LabelAutoselectionPeriodFormingDaysMark.TabIndex = 2;
            this.LabelAutoselectionPeriodFormingDaysMark.Text = "- рабочие дни";
            // 
            // ultraLabel1
            // 
            this.LayoutAutoSelectionGroup.SetColumnSpan(this.ultraLabel1, 2);
            this.ultraLabel1.Location = new System.Drawing.Point(3, 3);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(484, 18);
            this.ultraLabel1.TabIndex = 0;
            this.ultraLabel1.Text = "Щелкните по выбранному дню календаря, чтобы изменить его тип (рабочий/нерабочий)";
            // 
            // ComboYear
            // 
            this.ComboYear.FormattingEnabled = true;
            this.ComboYear.Location = new System.Drawing.Point(43, 51);
            this.ComboYear.Name = "ComboYear";
            this.ComboYear.Size = new System.Drawing.Size(88, 21);
            this.ComboYear.TabIndex = 7;
            this.ComboYear.SelectedIndexChanged += new System.EventHandler(this.YearChanged);
            this.ComboYear.SelectedValueChanged += new System.EventHandler(this.YearChanged);
            // 
            // CalendarView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.BasePanel);
            this.Name = "CalendarView";
            this.Size = new System.Drawing.Size(742, 594);
            this.BasePanel.ClientArea.ResumeLayout(false);
            this.BasePanel.ResumeLayout(false);
            this.LayoutBasePanel.ResumeLayout(false);
            this.LayoutAutoSelectionGroup.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraPanel BasePanel;
        private System.Windows.Forms.TableLayoutPanel LayoutBasePanel;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private System.Windows.Forms.TableLayoutPanel LayoutAutoSelectionGroup;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.Misc.UltraLabel labelRedDay;
        private Infragistics.Win.Misc.UltraLabel labelWeekDay;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.Misc.UltraLabel LabelAutoselectionPeriodFormingDaysMark;
        private System.Windows.Forms.ComboBox ComboYear;
        private System.Windows.Forms.TableLayoutPanel PanelMonthes;

    }
}
