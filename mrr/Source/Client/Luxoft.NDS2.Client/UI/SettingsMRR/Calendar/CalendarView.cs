﻿using System.Windows.Forms;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using Luxoft.NDS2.Client.UI.Base;
using Microsoft.Practices.ObjectBuilder;
using System;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.Calendar
{
    public partial class CalendarView : BaseView
    {
        private CalendarPresenter _presenter;

        private readonly string[] _month =
        {
            "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь",
            "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"
        };

        public CalendarView()
        {
            InitializeComponent();
        }

        public CalendarView(PresentationContext context)
            : base(context)
        {
            //_presenter = new CalendarPresenter(context, this);

            InitializeComponent();

            _presentationContext.WindowTitle = "Рабочий календарь";
            _presentationContext.WindowDescription = "Рабочий календарь";

            var a = new object[85];
            for (var i = 2015; i < 2100; ++i)
                a[i - 2015] = i;

            ComboYear.Items.AddRange(a);
            ComboYear.SelectedIndex = DateTime.Today.Year - 2015;
        }

        [CreateNew]
        public CalendarPresenter Presenter
        {
            set
            {
                _presenter = value;
                _presenter.PresentationContext = _presentationContext;
                WorkItem = _presenter.WorkItem;
                _presenter.OnObjectCreated();
                UpdateData();
            }
        }

        private void UpdateData()
        {
            if (_presenter == null)
                return;
            int y;
            if (!int.TryParse(ComboYear.Text, out y) || y < 2015 || y > 2099)
            {
                MessageBox.Show("Некорректно задан год!\nГод должен быть в интервале от 2015 до 2099 включительно.", "Ошибка");
                return;
            }
            _presenter.RequestData(new DateTime(y, 1, 1), new DateTime(y + 1, 1, 1));

            var begin = new DateTime(y, 1, 1);
            PanelMonthes.SuspendLayout();
            PanelMonthes.Controls.Clear();

            foreach (var month in _month)
            {
                var nextMonth = begin.AddMonths(1).Month;
                var line = 1;
                var panel = new MonthPanel(month);
                panel.SuspendLayout();
                var row = begin.Month / 3;
                var col = begin.Month - 1 - row * 3;
                PanelMonthes.Controls.Add(panel, col, row);

                while (begin.Month != nextMonth)
                {
                    var isRed = _presenter.RedDays.Any(d => d.Date == begin.Date);
                    var label = new DayLabel(begin, isRed);
                    label.Click += DaySwitch;
                    var c = begin.DayOfWeek == DayOfWeek.Sunday ? 6 : (int)begin.DayOfWeek - 1;
                    panel.Controls.Add(label, c, line);
                    if (begin.DayOfWeek == DayOfWeek.Sunday)
                        ++line;
                    begin = begin.AddDays(1);
                }
                panel.ResumeLayout();
            }
            PanelMonthes.ResumeLayout();

        }

        private void DaySwitch(object sender, EventArgs e)
        {
            var label = sender as DayLabel;
            if (label == null)
                return;
            label.IsRedDay = !label.IsRedDay;
            _presenter.DaySwitch(label.Date);
        }

        private void YearChanged(object sender, EventArgs e)
        {
            ComboYear.Enabled = false;
            UpdateData();
            ComboYear.Enabled = true;
        }

    }
}
