﻿using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.Calendar
{
    public class CalendarPresenter : BasePresenter<CalendarView>
    {
        private ICalendarDataService _service;

        public List<DateTime> RedDays { get; set; }

        public override void OnObjectCreated()
        {
            base.OnObjectCreated();

            _service = GetServiceProxy<ICalendarDataService>();
        }

        public void RequestData(DateTime begin, DateTime end)
        {
            RedDays = _service.GetRedDays(begin, end).Result;
            if (RedDays.Count > 0)
                return;

            // Генерируем данные для периода
            var gen = new CalendarGenerator();
            var list = gen.GenerateRedDays(begin, end);
            while (begin < end)
            {
                if (gen.WeekEnds.Contains(begin.DayOfWeek) || list.Any(d => d.Date == begin.Date))
                {
                    RedDays.Add(begin.Date);
                    _service.AddRedDay(begin);
                }
                begin = begin.AddDays(1);
            }
        }

        public void DaySwitch(DateTime date)
        {
            if (RedDays.Any(d => d.Date == date.Date))
            {
                RedDays.RemoveAll(d => d.Date == date.Date);
                _service.RemoveRedDay(date);
            }
            else
            {
                RedDays.Add(date.Date);
                _service.AddRedDay(date);
            }
        }

        /// <summary>
        /// Пример использования помощника по календарю
        /// </summary>
        /// <returns></returns>
        public string TestCalendarHelper()
        {
            var days = (new Random()).Next(-10, +10);

            var ch = new CalendarHelper(_service, DateTime.Today.AddMonths(-1), DateTime.Today.AddMonths(1));
            return string.Format("Для даты {0} сдвиг на {1} рабочих дней придется на дату {2}",
                DateTime.Today.ToString("dd.MM.yyyy"), days,
                ch.AddWorkingDays(DateTime.Today, days).ToString("dd.MM.yyyy"));
        }
    }
}
