﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.Dictionary.DictionaryEditor
{
    public class DataTypeColumn
    {
        public string DataTypeDb { get; set; }
        public Type TypeColumn { get; set; }
    }
}
