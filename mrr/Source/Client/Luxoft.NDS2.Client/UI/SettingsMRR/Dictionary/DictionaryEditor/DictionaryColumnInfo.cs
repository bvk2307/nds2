﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.Dictionary.DictionaryEditor
{
    public class DictionaryColumnInfo
    {
        public string TypeName { get; set; }
        public Type Type { get; set; }
        public string TableName { get; set; }
        public string TableNameEdit { get; set; }
        public string Name { get; set; }
        public string Caption { get; set; }
        public bool Hidden { get; set; }
        public bool ReadOnly { get; set; }
        public int Width { get; set; }
        public int Index { get; set; }
        public bool IsKey { get; set; }
        public Infragistics.Win.UltraWinGrid.SortIndicator Order { get; set; }

        public DictionaryColumnInfo()
        {
            TypeName = String.Empty;
            Type = typeof(string);
            TableName = String.Empty;
            TableNameEdit = String.Empty;
            Name = String.Empty;
            Caption = String.Empty;
            Hidden = false;
            ReadOnly = false;
            Width = 100;
            Index = 0;
            IsKey = false;
            Order = Infragistics.Win.UltraWinGrid.SortIndicator.None;
        }
    }
}
