﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Luxoft.NDS2.Client.UI.Base;
using Microsoft.Practices.ObjectBuilder;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using Infragistics.Win.UltraWinGrid;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using Microsoft.Practices.CompositeUI;
using Luxoft.NDS2.Client.UI.SettingsMRR.Dictionary.DictionaryEditor;
using Microsoft.Practices.CompositeUI.Commands;
using CommonComponents.Uc.Infrastructure.Interface.UI;
using Infragistics.Win;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.Dictionary.DictionaryList
{
    public partial class View : BaseView
    {
        private PresentationContext presentationContext;
        private Presenter _presenter;

        [CreateNew]
        public Presenter Presenter
        {
            set
            {
                _presenter = value;
                _presenter.View = this;
                _presenter.PresentationContext = presentationContext;
                _presentationContext = presentationContext;
                this.WorkItem = _presenter.WorkItem;
                InitData();
            }
        }

        public View(PresentationContext context)
        {
            presentationContext = context;
            InitializeComponent();

            CreateDataTableDictionaryList();

            this.gridDictionaryList.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.gridDictionaryList.DisplayLayout.AutoFitStyle = AutoFitStyle.ExtendLastColumn;

            InitGridDictionaryEditor();
        }

        private System.Data.DataTable dictionaryList = null;

        private void CreateDataTableDictionaryList()
        {
            if (null == this.dictionaryList)
            {
                this.dictionaryList = new System.Data.DataTable("DictionaryList");

                DataColumn columnName = this.dictionaryList.Columns.Add("Name", typeof(string));
                columnName.ReadOnly = true;
                columnName.Caption = "Название";
                DataColumn columnId = this.dictionaryList.Columns.Add("Id", typeof(string));
                columnId.ReadOnly = true;
                columnId.Caption = "Номер";
            }
        }

        private void InitData()
        {
            _presenter.InitDictionaryLists();
            foreach (KeyValuePair<string, string> item in _presenter.GetDictionaryLists())
            {
                this.dictionaryList.Rows.Add(new object[] { item.Value, item.Key });
            }
            this.gridDictionaryList.DataSource = this.dictionaryList;
            InitDataDictionaryEditorActions();
            InitializeRibbon();
        }

        private string GetCurrentDictionaryId()
        {
            UltraGridRow currentRow = this.gridDictionaryList.ActiveRow;
            string dictionaryId = String.Empty;
            if (currentRow != null)
            {
                dictionaryId = (string)currentRow.Cells["Id"].Value;
            }
            return dictionaryId;
        }

        private string GetCurrentDictionaryName()
        {
            UltraGridRow currentRow = this.gridDictionaryList.ActiveRow;
            string dictionaryName = String.Empty;
            if (currentRow != null)
            {
                dictionaryName = (string)currentRow.Cells["Name"].Value;
            }
            return dictionaryName;
        }

        private void gridDictionaryList_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            SelectOneDictionary();
        }

        private void gridDictionaryList_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
        }

        private void SelectOneDictionary()
        {
            this.DictionaryId = GetCurrentDictionaryId();
            SetupDataDictionaryEditor();
            ResizeGridDictionaryEditor();
        }

        private void View_Load(object sender, EventArgs e)
        {
            if (this.gridDictionaryList.Rows.Count > 0)
            {
                this.gridDictionaryList.ActiveRow = this.gridDictionaryList.Rows[0];
            }
            ResizeGrid();
            SelectOneDictionary();
        }

        private void ResizeGrid()
        {
            int heightNeedGrid = this.dictionaryList.Rows.Count * 20;
            if (this.dictionaryList.Rows.Count < 6)
            {
                heightNeedGrid = this.dictionaryList.Rows.Count * 28;
            }
            if (heightNeedGrid > tableLayoutPanelMain.Height)
            {
                heightNeedGrid = tableLayoutPanelMain.Height;
            }
            if (heightNeedGrid < 100)
            {
                heightNeedGrid = 100;
            }
            gridDictionaryList.Height = heightNeedGrid;
            ultraGroupBoxDictionaryList.Height = heightNeedGrid + 35;
            gridDictionaryList.Dock = DockStyle.Fill;
        }

        private void tableLayoutPanelMain_SizeChanged(object sender, EventArgs e)
        {
            ResizeGrid();
            ResizeGridDictionaryEditor();
        }

        private void InitGridDictionaryEditor()
        {
            this.gridDictionaryGeneral.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag;
            this.gridDictionaryGeneral.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.gridDictionaryGeneral.DisplayLayout.AutoFitStyle = AutoFitStyle.ExtendLastColumn;
            this.gridDictionaryGeneral.DisplayLayout.Override.WrapHeaderText = DefaultableBoolean.True;
        }

        public string DictionaryId { get; set; }
        private List<DictionaryColumnInfo> _columns;

        private void InitDataDictionaryEditorActions()
        {
            _presenter.InitActions();
        }

        private void SetupDataDictionaryEditor()
        {
            List<DictionaryColumnInfo> columns = _presenter.GetColumns(DictionaryId);
            _columns = columns;
            CreateDataTableDictionaryList(columns);

            this.dataGeneral.Rows.Clear();
            List<Dictionary<string, object>> rows = _presenter.GetRows(DictionaryId);
            foreach (Dictionary<string, object> dicts in rows)
            {
                List<object> objs = new List<object>();
                foreach (DictionaryColumnInfo itemColumn in columns)
                {
                    foreach (KeyValuePair<string, object> itemRow in dicts)
                    {
                        if (itemColumn.Name == itemRow.Key)
                        {
                            objs.Add(itemRow.Value);
                            break;
                        }
                    }
                }
                this.dataGeneral.Rows.Add(objs.ToArray());
            }
            gridDictionaryGeneral.DataSource = this.dataGeneral;
            SetupViewDictionaryEditor();
        }

        private void SetupViewDictionaryEditor()
        {
            int widthGeneral = 0;
            foreach (DictionaryColumnInfo columnInfo in _columns)
            {
                UltraGridColumn col = gridDictionaryGeneral.DisplayLayout.Bands[0].Columns[columnInfo.Name];
                col.Hidden = columnInfo.Hidden;
                if (!columnInfo.Hidden)
                {
                    widthGeneral += columnInfo.Width;
                    col.Width = columnInfo.Width;
                    col.SortIndicator = columnInfo.Order;
                    if (columnInfo.ReadOnly)
                    {
                        col.CellClickAction = CellClickAction.RowSelect;
                        col.CellActivation = Activation.NoEdit;
                        col.CellAppearance.BackColor = Color.LightGray;
                    }
                    else
                    {
                        col.CellClickAction = CellClickAction.Edit;
                        col.CellActivation = Activation.AllowEdit;
                    }
                    col.Header.Appearance.TextHAlign = HAlign.Center;
                }
            }
            int widthIsEditableDictionary = 0;
            if (!IsEditableDictionary())
            {
                tableLayoutPanelDictionaryEditor.ColumnStyles[0].SizeType = SizeType.Percent;
                tableLayoutPanelDictionaryEditor.ColumnStyles[0].Width = 100;
                tableLayoutPanelDictionaryEditor.ColumnStyles[1].SizeType = SizeType.Percent;
                tableLayoutPanelDictionaryEditor.ColumnStyles[1].Width = 0;
            }
            else
            {
                tableLayoutPanelDictionaryEditor.ColumnStyles[0].SizeType = SizeType.Percent;
                tableLayoutPanelDictionaryEditor.ColumnStyles[0].Width = 100;
                tableLayoutPanelDictionaryEditor.ColumnStyles[1].SizeType = SizeType.Absolute;
                tableLayoutPanelDictionaryEditor.ColumnStyles[1].Width = 150;
                widthIsEditableDictionary = 150;
            }
            ultraGroupBoxDictionaryEditor.Width = widthGeneral + 38 + widthIsEditableDictionary;
            if (gridDictionaryGeneral.Rows.Count > 0)
            {
                gridDictionaryGeneral.ActiveRow = gridDictionaryGeneral.Rows[0];
            }
            ultraGroupBoxDictionaryEditor.Text = "Справочник - " + GetCurrentDictionaryName();
        }

        private bool IsEditableDictionary()
        {
            bool dictionaryEditable = false;
            if (_columns.Where(p => p.ReadOnly).Count() == 0)
            {
                dictionaryEditable = true;
            }
            return dictionaryEditable; 
        }

        private System.Data.DataTable dataGeneral = null;

        private void CreateDataTableDictionaryList(List<DictionaryColumnInfo> columns)
        {
            if (null == this.dataGeneral)
            {
                this.dataGeneral = new System.Data.DataTable("DataGeneral");
            }
            else
            {
                this.dataGeneral = new System.Data.DataTable("DataGeneral");
            }
            foreach (DictionaryColumnInfo columnInfo in columns.OrderBy(p => p.Index))
            {
                DataColumn dataColumn = this.dataGeneral.Columns.Add(columnInfo.Name, columnInfo.Type);
                dataColumn.ReadOnly = columnInfo.ReadOnly;
                dataColumn.Caption = columnInfo.Caption;
            }
        }

        private void gridDictionaryGeneral_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {
            string key = String.Empty;
            string value = String.Empty;
            string tableNameEdit = String.Empty;
            Dictionary<string, string> tableRow = new Dictionary<string, string>();
            foreach (DictionaryColumnInfo columnInfo in _columns)
            {
                if ((e.Row.IsAddRow && IsEditableDictionary()) || (!e.Row.IsAddRow && !columnInfo.ReadOnly))
                {
                    tableRow.Add(columnInfo.Name, e.Row.Cells[columnInfo.Name].Value.ToString());
                    if (string.IsNullOrEmpty(tableNameEdit))
                    {
                        tableNameEdit = columnInfo.TableNameEdit;
                    }
                }
                if (columnInfo.IsKey)
                {
                    key = columnInfo.Name;
                    value = e.Row.Cells[columnInfo.Name].Value.ToString();
                }
            }
            if (tableRow.Count > 0)
            {
                if (e.Row.IsAddRow)
                {
                    _presenter.AddTableRow(tableNameEdit, tableRow);
                }
                else
                {
                    _presenter.UpdateTableRow(tableNameEdit, key, value, tableRow);
                }
            }
        }

        private void ResizeGridDictionaryEditor()
        {
            int heightNeedGrid = this.dataGeneral.Rows.Count * 20;
            if (this.dataGeneral.Rows.Count < 1)
            {
                heightNeedGrid = 100;
            }
            else if (this.dataGeneral.Rows.Count < 10)
            {
                heightNeedGrid = this.dataGeneral.Rows.Count * 30;
            }
            if (heightNeedGrid > tableLayoutPanelMain.Height)
            {
                heightNeedGrid = tableLayoutPanelMain.Height;
            }
            if (IsEditableDictionary() && heightNeedGrid < 150)
            {
                heightNeedGrid = 150;
            }
            gridDictionaryGeneral.Height = heightNeedGrid;
            ultraGroupBoxDictionaryEditor.Height = heightNeedGrid + 30;
            if (gridDictionaryGeneral.Dock != DockStyle.Fill)
            {
                gridDictionaryGeneral.Dock = DockStyle.Fill;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            UltraGridRow row = gridDictionaryGeneral.DisplayLayout.Bands[0].AddNew();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DictionaryColumnInfo columnInfo = _columns.Where(p=>p.IsKey).FirstOrDefault();
            if (columnInfo != null)
            {
                string key = columnInfo.Name;
                string value = GetCurrentDictionaryGeneralKeyValue(key);
            }
        }

        private string GetCurrentDictionaryGeneralKeyValue(string key)
        {
            UltraGridRow currentRow = this.gridDictionaryGeneral.ActiveRow;
            string value = String.Empty;
            if (currentRow != null)
            {
                value = (string)currentRow.Cells[key].Value;
            }
            return value;
        }

        private void InitializeRibbon()
        {
            IUcResourceManagersService resourceManagersService = _presenter.WorkItem.Services.Get<IUcResourceManagersService>(true);
            if (!resourceManagersService.Contains(ResourceManagerNDS2.NDS2ClientResources))
            {
                resourceManagersService.Add(ResourceManagerNDS2.NDS2ClientResources, Properties.Resources.ResourceManager);
            }

            //закладка
            UcRibbonTabContext tabNavigator = new UcRibbonTabContext(_presenter.PresentationContext, "NDS2Result")
            {
                Text = _presenter.PresentationContext.WindowTitle,
                ToolTipText = _presenter.PresentationContext.WindowTitle,
                Visible = true,
                Order = 1
            };

            UcRibbonGroupContext groupNavigation1 = tabNavigator.AddGroup("NDS2DeclarationListManage");
            groupNavigation1.Text = "Функции";
            groupNavigation1.Visible = true;

            var btnDictionaryOpen = new UcRibbonButtonToolContext(_presenter.PresentationContext, "btn" + "Open", "cmdDictionary" + "Open")
            {
                Text = "Открыть",
                ToolTipText = "Открыть",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "view",
                SmallImageName = "view",
                Enabled = true
            };

            groupNavigation1.ToolList.AddRange(
                new UcRibbonToolInstanceSettings[] { 
                    new UcRibbonToolInstanceSettings(btnDictionaryOpen.ItemName, UcRibbonToolSize.Large),
                });

            //общее
            _presentationContext.UiVisualizationCollection.AddRange(new VisualizationElementBase[]
            {
                btnDictionaryOpen,
                tabNavigator
            });

            _presentationContext.ActiveMenuTab = tabNavigator.ItemName;
        }

        [CommandHandler("cmdDictionaryOpen")]
        public virtual void DictionaryOpenBtnClick(object sender, EventArgs e)
        {
            SelectOneDictionary();             
        }
    }
}
