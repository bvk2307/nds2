﻿using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.SettingsMRR.Dictionary.DictionaryEditor;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.Services;
using Microsoft.Practices.CompositeUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.Dictionary.DictionaryList
{
    public class Presenter : BasePresenter<View>
    {
        public Presenter()
        {
        }

        public Presenter(PresentationContext presentationContext, WorkItem wi, View view)
            : base(presentationContext, wi, view)
        {
        }

        private Dictionary<string, string> dictionaryLists = new Dictionary<string, string>();

        private List<TableDictionaryInfo> GetTablesDictionaryInfo()
        {
            List<TableDictionaryInfo> tablesDictionaryInfo = null;
            if (ExecuteServiceCall(base.GetServiceProxy<IDictionaryDataService>().GetTablesDictionaryInfo,
                res => { tablesDictionaryInfo = res.Result; }))
            { }
            else
            {
                tablesDictionaryInfo = new List<TableDictionaryInfo>();
            }
            return tablesDictionaryInfo;
        }

        public void InitDictionaryLists()
        {
            dictionaryLists.Clear();
            foreach (TableDictionaryInfo info in GetTablesDictionaryInfo())
            {
                dictionaryLists.Add(info.TableName, info.TableCaption);
            }
        }

        public Dictionary<string, string> GetDictionaryLists()
        {
            return dictionaryLists;
        }

        private string GetDictionaryCaption(string dictionaryId)
        {
            string ret = String.Empty;
            if (dictionaryLists.ContainsKey(dictionaryId))
            {
                ret = dictionaryLists[dictionaryId];
            }
            return ret; 
        }

        private Dictionary<string, Func<List<DictionaryColumnInfo>>> actionsGetColumns = new Dictionary<string, Func<List<DictionaryColumnInfo>>>();
        private Dictionary<string, Func<List<Dictionary<string, object>>>> actionsGetRows = new Dictionary<string, Func<List<Dictionary<string, object>>>>();

        private List<TableDictionaryColumnInfo> GetTablesDictionaryColumnInfo()
        {
            List<TableDictionaryColumnInfo> tablesDictionaryColumnInfo = null;
            if (ExecuteServiceCall(base.GetServiceProxy<IDictionaryDataService>().GetTablesDictionaryColumnInfo,
                res => { tablesDictionaryColumnInfo = res.Result; }))
            { }
            else
            {
                tablesDictionaryColumnInfo = new List<TableDictionaryColumnInfo>();
            }
            return tablesDictionaryColumnInfo;
        }

        public void InitActions()
        {
            InitActionsColumns();
            InitActionsRows();
        }

        private List<TableDictionaryColumnInfo> _tablesDictionaryInfo;

        private void InitActionsColumns()
        {
            actionsGetColumns.Clear();

            List<DataTypeColumn> dataTypesColumn = new List<DataTypeColumn>();
            dataTypesColumn.Add(new DataTypeColumn() { DataTypeDb = "NVARCHAR2", TypeColumn = typeof(string) });
            dataTypesColumn.Add(new DataTypeColumn() { DataTypeDb = "VARCHAR2", TypeColumn = typeof(string) });
            dataTypesColumn.Add(new DataTypeColumn() { DataTypeDb = "NUMBER", TypeColumn = typeof(int) });

            _tablesDictionaryInfo = GetTablesDictionaryColumnInfo();
            IEnumerable<IGrouping<string, TableDictionaryColumnInfo>> groupTables = _tablesDictionaryInfo.GroupBy(p => p.TableName);
            foreach (IGrouping<string, TableDictionaryColumnInfo> item in groupTables)
            {
                actionsGetColumns.Add(item.Key.ToLower(), () =>
                {
                    List<DictionaryColumnInfo> columns = new List<DictionaryColumnInfo>();
                    foreach (TableDictionaryColumnInfo tableInfo in _tablesDictionaryInfo.Where(p => p.TableName == item.Key).OrderBy(p => p.Index))
                    {
                        Type TypeColumn = typeof(string);
                        DataTypeColumn dataTypeColumnFind = dataTypesColumn.Where(p => p.DataTypeDb.ToLower() == tableInfo.ColumnDateType.ToLower()).FirstOrDefault();
                        if (dataTypeColumnFind != null)
                        {
                            TypeColumn = dataTypeColumnFind.TypeColumn;
                        }
                        DictionaryColumnInfo columnInfo = new DictionaryColumnInfo()
                        {
                            Type = TypeColumn,
                            TypeName = TypeColumn.ToString(),
                            TableName = tableInfo.TableName,
                            TableNameEdit = tableInfo.TableNameEdit,
                            Name = tableInfo.ColumnName,
                            Caption = tableInfo.Caption,
                            ReadOnly = tableInfo.IsReadOnly,
                            Width = tableInfo.Width,
                            Hidden = !tableInfo.Visible,
                            Index = tableInfo.Index,
                            IsKey = tableInfo.IsKey
                        };
                        if (tableInfo.TypeSort.ToLower() == "asc")
                        {
                            columnInfo.Order = Infragistics.Win.UltraWinGrid.SortIndicator.Ascending;
                        }
                        columns.Add(columnInfo);
                    }
                    return columns;
                });
            }
        }

        private List<Dictionary<string, object>> GetTableRow(string tableName, List<string> columns)
        {
            List<Dictionary<string, object>> rows = null;
            if (ExecuteServiceCall(() => base.GetServiceProxy<IDictionaryDataService>().GetTableRows(tableName, columns),
                res => { rows = res.Result; }))
            {
                return rows;
            }
            return new List<Dictionary<string, object>>();
        }

        private void InitActionsRows()
        {
            actionsGetRows.Clear();

            IEnumerable<IGrouping<string, TableDictionaryColumnInfo>> groupTables = _tablesDictionaryInfo.GroupBy(p => p.TableName);
            foreach (IGrouping<string, TableDictionaryColumnInfo> item in groupTables)
            {
                List<string> columns = new List<string>();
                foreach (TableDictionaryColumnInfo tableInfo in _tablesDictionaryInfo.Where(p => p.TableName == item.Key).OrderBy(p => p.Index))
                {
                    columns.Add(tableInfo.ColumnName);
                }
                actionsGetRows.Add(item.Key.ToLower(), () =>
                {
                    List<Dictionary<string, object>> rows = GetTableRow(item.Key, columns);
                    return rows;
                });
            }
        }

        public List<DictionaryColumnInfo> GetColumns(string dictionaryId)
        {
            List<DictionaryColumnInfo> columns = null;
            string key = dictionaryId.ToLower();
            if (actionsGetColumns.ContainsKey(key))
            {
                columns = actionsGetColumns[key].Invoke();
            }
            if (null == columns)
            {
                columns = new List<DictionaryColumnInfo>();
            }
            return columns;
        }

        public List<Dictionary<string, object>> GetRows(string dictionaryId)
        {
            List<Dictionary<string, object>> rows = null;
            string key = dictionaryId.ToLower();
            if (actionsGetRows.ContainsKey(key))
            {
                rows = actionsGetRows[key].Invoke();
            }
            if (null == rows)
            {
                rows = new List<Dictionary<string, object>>();
            }
            return rows;
        }

        public void UpdateTableRow(string tableName, string key, string value, Dictionary<string, string> row)
        {
            if (ExecuteServiceCall(() => base.GetServiceProxy<IDictionaryDataService>().UpdateTableRow(tableName, key, value, row),
                res => { }))
            {
            }
        }

        public void AddTableRow(string tableName, Dictionary<string, string> row)
        {
            if (ExecuteServiceCall(() => base.GetServiceProxy<IDictionaryDataService>().AddTableRow(tableName, row),
                res => { }))
            {
            }
        }

        public void DeleteTableRow(string tableName, string key, string value)
        {
            if (ExecuteServiceCall(() => base.GetServiceProxy<IDictionaryDataService>().DeleteTableRow(tableName, key, value),
                res => { }))
            {
            }
        }
    }
}
