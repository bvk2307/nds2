﻿namespace Luxoft.NDS2.Client.UI.SettingsMRR.Dictionary.DictionaryList
{
    partial class View
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand1 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Band 0", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn1 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Name", -1, null, 0, Infragistics.Win.UltraWinGrid.SortIndicator.Ascending, false);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn2 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Id");
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn3 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Name");
            Infragistics.Win.UltraWinDataSource.UltraDataColumn ultraDataColumn4 = new Infragistics.Win.UltraWinDataSource.UltraDataColumn("Id");
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            this.gridDictionaryList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraDataSourceDictionaryList = new Infragistics.Win.UltraWinDataSource.UltraDataSource(this.components);
            this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
            this.ultraGroupBoxDictionaryList = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraGroupBoxDictionaryEditor = new Infragistics.Win.Misc.UltraGroupBox();
            this.tableLayoutPanelDictionaryEditor = new System.Windows.Forms.TableLayoutPanel();
            this.ultraPanelDictionaryButton = new Infragistics.Win.Misc.UltraPanel();
            this.btnNew = new Infragistics.Win.Misc.UltraButton();
            this.btnDelete = new Infragistics.Win.Misc.UltraButton();
            this.gridDictionaryGeneral = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uDataSourceDictionaryGeneral = new Infragistics.Win.UltraWinDataSource.UltraDataSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.gridDictionaryList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSourceDictionaryList)).BeginInit();
            this.tableLayoutPanelMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxDictionaryList)).BeginInit();
            this.ultraGroupBoxDictionaryList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxDictionaryEditor)).BeginInit();
            this.ultraGroupBoxDictionaryEditor.SuspendLayout();
            this.tableLayoutPanelDictionaryEditor.SuspendLayout();
            this.ultraPanelDictionaryButton.ClientArea.SuspendLayout();
            this.ultraPanelDictionaryButton.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridDictionaryGeneral)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDataSourceDictionaryGeneral)).BeginInit();
            this.SuspendLayout();
            // 
            // gridDictionaryList
            // 
            this.gridDictionaryList.DataSource = this.ultraDataSourceDictionaryList;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.gridDictionaryList.DisplayLayout.Appearance = appearance13;
            ultraGridColumn1.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn1.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            ultraGridColumn1.Header.VisiblePosition = 0;
            ultraGridColumn1.Width = 350;
            ultraGridColumn2.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ultraGridColumn2.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            ultraGridColumn2.Header.VisiblePosition = 1;
            ultraGridColumn2.Hidden = true;
            ultraGridBand1.Columns.AddRange(new object[] {
            ultraGridColumn1,
            ultraGridColumn2});
            this.gridDictionaryList.DisplayLayout.BandsSerializer.Add(ultraGridBand1);
            this.gridDictionaryList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.gridDictionaryList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.gridDictionaryList.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.gridDictionaryList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.gridDictionaryList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.gridDictionaryList.DisplayLayout.GroupByBox.Hidden = true;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.gridDictionaryList.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.gridDictionaryList.DisplayLayout.MaxColScrollRegions = 1;
            this.gridDictionaryList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gridDictionaryList.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.gridDictionaryList.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.gridDictionaryList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.gridDictionaryList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.gridDictionaryList.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.gridDictionaryList.DisplayLayout.Override.CellAppearance = appearance20;
            this.gridDictionaryList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.gridDictionaryList.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.gridDictionaryList.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.gridDictionaryList.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.gridDictionaryList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.gridDictionaryList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.gridDictionaryList.DisplayLayout.Override.RowAppearance = appearance23;
            this.gridDictionaryList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.gridDictionaryList.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.gridDictionaryList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.gridDictionaryList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.gridDictionaryList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gridDictionaryList.Location = new System.Drawing.Point(4, 26);
            this.gridDictionaryList.Name = "gridDictionaryList";
            this.gridDictionaryList.Size = new System.Drawing.Size(372, 137);
            this.gridDictionaryList.TabIndex = 9;
            this.gridDictionaryList.UseAppStyling = false;
            this.gridDictionaryList.AfterSelectChange += new Infragistics.Win.UltraWinGrid.AfterSelectChangeEventHandler(this.gridDictionaryList_AfterSelectChange);
            this.gridDictionaryList.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.gridDictionaryList_DoubleClickRow);
            // 
            // ultraDataSourceDictionaryList
            // 
            ultraDataColumn3.ReadOnly = Infragistics.Win.DefaultableBoolean.True;
            ultraDataColumn4.ReadOnly = Infragistics.Win.DefaultableBoolean.True;
            this.ultraDataSourceDictionaryList.Band.Columns.AddRange(new object[] {
            ultraDataColumn3,
            ultraDataColumn4});
            // 
            // tableLayoutPanelMain
            // 
            this.tableLayoutPanelMain.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanelMain.ColumnCount = 2;
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 380F));
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMain.Controls.Add(this.ultraGroupBoxDictionaryList, 0, 0);
            this.tableLayoutPanelMain.Controls.Add(this.ultraGroupBoxDictionaryEditor, 1, 0);
            this.tableLayoutPanelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelMain.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
            this.tableLayoutPanelMain.RowCount = 1;
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelMain.Size = new System.Drawing.Size(1091, 381);
            this.tableLayoutPanelMain.TabIndex = 10;
            this.tableLayoutPanelMain.SizeChanged += new System.EventHandler(this.tableLayoutPanelMain_SizeChanged);
            // 
            // ultraGroupBoxDictionaryList
            // 
            this.ultraGroupBoxDictionaryList.Controls.Add(this.gridDictionaryList);
            this.ultraGroupBoxDictionaryList.Location = new System.Drawing.Point(3, 3);
            this.ultraGroupBoxDictionaryList.Name = "ultraGroupBoxDictionaryList";
            this.ultraGroupBoxDictionaryList.Size = new System.Drawing.Size(374, 168);
            this.ultraGroupBoxDictionaryList.TabIndex = 12;
            this.ultraGroupBoxDictionaryList.Text = "Выбор справочника";
            // 
            // ultraGroupBoxDictionaryEditor
            // 
            this.ultraGroupBoxDictionaryEditor.Controls.Add(this.tableLayoutPanelDictionaryEditor);
            this.ultraGroupBoxDictionaryEditor.Location = new System.Drawing.Point(383, 3);
            this.ultraGroupBoxDictionaryEditor.Name = "ultraGroupBoxDictionaryEditor";
            this.ultraGroupBoxDictionaryEditor.Size = new System.Drawing.Size(687, 202);
            this.ultraGroupBoxDictionaryEditor.TabIndex = 0;
            this.ultraGroupBoxDictionaryEditor.Text = "Справочник";
            // 
            // tableLayoutPanelDictionaryEditor
            // 
            this.tableLayoutPanelDictionaryEditor.ColumnCount = 2;
            this.tableLayoutPanelDictionaryEditor.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelDictionaryEditor.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanelDictionaryEditor.Controls.Add(this.ultraPanelDictionaryButton, 1, 0);
            this.tableLayoutPanelDictionaryEditor.Controls.Add(this.gridDictionaryGeneral, 0, 0);
            this.tableLayoutPanelDictionaryEditor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelDictionaryEditor.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanelDictionaryEditor.Name = "tableLayoutPanelDictionaryEditor";
            this.tableLayoutPanelDictionaryEditor.RowCount = 1;
            this.tableLayoutPanelDictionaryEditor.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelDictionaryEditor.Size = new System.Drawing.Size(681, 183);
            this.tableLayoutPanelDictionaryEditor.TabIndex = 0;
            // 
            // ultraPanelDictionaryButton
            // 
            // 
            // ultraPanelDictionaryButton.ClientArea
            // 
            this.ultraPanelDictionaryButton.ClientArea.Controls.Add(this.btnNew);
            this.ultraPanelDictionaryButton.ClientArea.Controls.Add(this.btnDelete);
            this.ultraPanelDictionaryButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanelDictionaryButton.Location = new System.Drawing.Point(534, 3);
            this.ultraPanelDictionaryButton.Name = "ultraPanelDictionaryButton";
            this.ultraPanelDictionaryButton.Size = new System.Drawing.Size(144, 177);
            this.ultraPanelDictionaryButton.TabIndex = 0;
            // 
            // btnNew
            // 
            this.btnNew.Location = new System.Drawing.Point(16, 7);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(99, 23);
            this.btnNew.TabIndex = 14;
            this.btnNew.Text = "Новый";
            this.btnNew.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(16, 41);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(99, 23);
            this.btnDelete.TabIndex = 15;
            this.btnDelete.Text = "Удалить";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // gridDictionaryGeneral
            // 
            this.gridDictionaryGeneral.DataSource = this.uDataSourceDictionaryGeneral;
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.gridDictionaryGeneral.DisplayLayout.Appearance = appearance1;
            this.gridDictionaryGeneral.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.gridDictionaryGeneral.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.gridDictionaryGeneral.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.gridDictionaryGeneral.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.gridDictionaryGeneral.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.gridDictionaryGeneral.DisplayLayout.GroupByBox.Hidden = true;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.gridDictionaryGeneral.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.gridDictionaryGeneral.DisplayLayout.MaxColScrollRegions = 1;
            this.gridDictionaryGeneral.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gridDictionaryGeneral.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.gridDictionaryGeneral.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.gridDictionaryGeneral.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.gridDictionaryGeneral.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.gridDictionaryGeneral.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.gridDictionaryGeneral.DisplayLayout.Override.CellAppearance = appearance8;
            this.gridDictionaryGeneral.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.gridDictionaryGeneral.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.gridDictionaryGeneral.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.gridDictionaryGeneral.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.gridDictionaryGeneral.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.gridDictionaryGeneral.DisplayLayout.Override.RowAppearance = appearance11;
            this.gridDictionaryGeneral.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.gridDictionaryGeneral.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.gridDictionaryGeneral.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.gridDictionaryGeneral.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.gridDictionaryGeneral.Location = new System.Drawing.Point(3, 3);
            this.gridDictionaryGeneral.Name = "gridDictionaryGeneral";
            this.gridDictionaryGeneral.Size = new System.Drawing.Size(462, 144);
            this.gridDictionaryGeneral.TabIndex = 11;
            this.gridDictionaryGeneral.UseAppStyling = false;
            this.gridDictionaryGeneral.BeforeRowUpdate += new Infragistics.Win.UltraWinGrid.CancelableRowEventHandler(this.gridDictionaryGeneral_BeforeRowUpdate);
            // 
            // View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.tableLayoutPanelMain);
            this.Name = "View";
            this.Size = new System.Drawing.Size(1091, 381);
            this.Load += new System.EventHandler(this.View_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridDictionaryList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDataSourceDictionaryList)).EndInit();
            this.tableLayoutPanelMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxDictionaryList)).EndInit();
            this.ultraGroupBoxDictionaryList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxDictionaryEditor)).EndInit();
            this.ultraGroupBoxDictionaryEditor.ResumeLayout(false);
            this.tableLayoutPanelDictionaryEditor.ResumeLayout(false);
            this.ultraPanelDictionaryButton.ClientArea.ResumeLayout(false);
            this.ultraPanelDictionaryButton.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridDictionaryGeneral)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDataSourceDictionaryGeneral)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinGrid.UltraGrid gridDictionaryList;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelMain;
        private Infragistics.Win.UltraWinDataSource.UltraDataSource ultraDataSourceDictionaryList;
        private Infragistics.Win.UltraWinGrid.UltraGrid gridDictionaryGeneral;
        private Infragistics.Win.UltraWinDataSource.UltraDataSource uDataSourceDictionaryGeneral;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBoxDictionaryList;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBoxDictionaryEditor;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelDictionaryEditor;
        private Infragistics.Win.Misc.UltraPanel ultraPanelDictionaryButton;
        private Infragistics.Win.Misc.UltraButton btnNew;
        private Infragistics.Win.Misc.UltraButton btnDelete;
    }
}
