﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using Microsoft.Practices.CompositeUI;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1;
using Microsoft.Practices.ObjectBuilder;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.AssignEmployee
{
    public partial class View : BaseView
    {
        private ResponsibilityRegionPresenter _presenter;

        public GridControl Grid
        {
            get { return grid; }
        }

        [CreateNew]
        public ResponsibilityRegionPresenter Presenter
        {
            set
            {
                _presenter = value;
                _presenter.PresentationContext = this._presentationContext;
                _presenter.View = this;
                this.WorkItem = _presenter.WorkItem;
                _presenter.OnObjectCreated();
            }
        }

        public View(PresentationContext context)
            : base(context)
        {
            InitializeComponent();
        }

        // public View(PresentationContext ctx, WorkItem wi)
        //    : base(ctx, wi)
        //{
        //    InitializeComponent();
        //    _presenter = new ResponsibilityRegionPresenter(this, ctx, wi);
        //}
    }
}