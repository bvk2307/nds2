﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinToolTip;
using Luxoft.NDS2.Client.UI.Controls.LookupDictionary;
using Luxoft.NDS2.Client.UI.Controls.LookupDictionary.Entity;
using Luxoft.NDS2.Common.Contract.DTO.SearchEntity;
using Luxoft.NDS2.Common.Contracts.ControlContracts;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.AssignEmployee.Dialog
{
    public partial class EmployeeDialog : Form, ILookupSuitableView
    {
        private readonly EmployeeDialogPresenter _presenter;
        private readonly BindingList<PrincipalInfo> _source;
        private readonly Func<UltraGridRow, bool> _rowSelected;

        public string _nalogOrganCode;

        public string SelectedCountFormat { get; set; }

        public IList<PrincipalInfo> DataSource
        {
            get { return _source; }
        }

        private readonly UltraToolTipInfo _toolTipRefresh = new UltraToolTipInfo("Обновить список сотрудников",
            ToolTipImage.Default, null, DefaultableBoolean.Default);
        private readonly UltraToolTipInfo _toolTipClose = new UltraToolTipInfo("Закрыть окно без сохранения изменений.",
            ToolTipImage.Default, null, DefaultableBoolean.Default);
        private readonly UltraToolTipInfo _toolTipSave = new UltraToolTipInfo("Сохранить изменения",
            ToolTipImage.Default, null, DefaultableBoolean.Default);

        public EmployeeDialog(EmployeeDialogPresenter presenter)
        {
            InitializeComponent();

            _presenter = presenter;
            _rowSelected = row => (bool)row.Cells["IS_IN_PROCESS"].Value;
            ugPrincpalList.DataSource = new BindingSource { DataSource = _source = new BindingList<PrincipalInfo>() };

            Load += (sender, e) =>
            {
                _presenter.InitializeView();
                ugPrincpalList.DisplayLayout.Bands[0].Columns["IS_IN_PROCESS"].SetHeaderCheckedState(ugPrincpalList.Rows, _source.All(p => p.Selected) && _source.Count > 0);
            };

            ubSave.Enabled = _source.Any(x => x.Selected);
            ubSave.Click += (sender, e) => _presenter.Save();
            ubCancel.Click += (sender, e) => _presenter.Close();

            ugPrincpalList.ClickCell += ugPrincpalList_ClickCell;
            ugPrincpalList.AfterHeaderCheckStateChanged += ugPrincpalList_AfterHeaderCheckStateChanged;

            ultraToolTipManager1.SetUltraToolTip(ubCancel, _toolTipClose);
            ultraToolTipManager1.SetUltraToolTip(ubSave, _toolTipSave);

            SetButtonGetUserEnabled(false);
        }

        private bool _isCheckByCode = false;

        private void ugPrincpalList_AfterHeaderCheckStateChanged(object sender, AfterHeaderCheckStateChangedEventArgs e)
        {
            if (_isCheckByCode)
                return;
            if (e.Column.Key != "IS_IN_PROCESS")
                return;
            if (e.Column.GetHeaderCheckedState(e.Rows) == CheckState.Checked)
            {
                foreach (var p in _source)
                {
                    p.Selected = true;
                }
            }
            UpdateSelectedCount();
            ugPrincpalList.Refresh();
        }

        private void ugPrincpalList_ClickCell(object sender, ClickCellEventArgs e)
        {
            if (e.Cell.Column.Key != "IS_IN_PROCESS")
            {
                return;
            }
            var principal = ((PrincipalInfo)e.Cell.Row.ListObject);
            principal.Selected = !principal.Selected;
            ugPrincpalList.Refresh();
            UpdateSelectedCount();
            _isCheckByCode = true;
            e.Cell.Column.SetHeaderCheckedState(ugPrincpalList.Rows, _source.All(p => p.Selected));
            _isCheckByCode = false;
            ubSave.Enabled = _source.Any(x => x.Selected);
        }

        public void UpdateSelectedCount()
        {
            ulSelectedCount.Text = string.Format(SelectedCountFormat, _source.Count(p => p.Selected));
        }

        private void uteNalogOrganCode_ValueChanged(object sender, EventArgs e)
        {
            if (uteNalogOrganCode.Text.Length > 4)
            {
                uteNalogOrganCode.Text = uteNalogOrganCode.Text.Substring(0, 4);
            }
            SetButtonGetUserEnabled(false);
            uteNalogOrganName.Text = String.Empty;
            _nalogOrganCode = String.Empty;
        }

        private void ultraButtonShowControl_Click(object sender, EventArgs e)
        {
            ShowLookupGridParamControl();
        }

        private void ShowLookupGridParamControl()
        {
            Form editAttributeDialog = null;

            ParameterSettings ps = new ParameterSettings();
            ps.LookupSource = "v$sono";

            GridParameterItem gpi = new GridParameterItem(ps, "Наименование");
            gpi.FilterSearchString = uteNalogOrganCode.Text;

            Point location = GetPointOfClientApp(uteNalogOrganCode);
            location.Y += uteNalogOrganCode.Height + 5;

            editAttributeDialog = new LookupGridParamControl(gpi, GetLookupSuitableViewLink(), location);
            if (editAttributeDialog.ShowDialog(this) == DialogResult.OK)
            {
                GridParameterItem data = ((IEditSearchAttributeDialog)editAttributeDialog).ResultData;
                List<GridParameterDetailItem> checkList = data.GetChildItem();
                if (checkList.Count > 0)
                {
                    GridParameterDetailItem item = checkList.First();
                    LookupResponse lr = (LookupResponse)item.ValueOne;
                    uteNalogOrganName.Text = lr.DisplayValue;
                    SetSounCodeSafe(_nalogOrganCode = lr.Code);
                }
            }
        }

        private void SetSounCodeSafe(string text)
        {
            uteNalogOrganCode.ValueChanged -= uteNalogOrganCode_ValueChanged;
            uteNalogOrganCode.Text = text;
            uteNalogOrganCode.ValueChanged += uteNalogOrganCode_ValueChanged;
            SetButtonGetUserEnabled(true);
        }

        private void SetButtonGetUserEnabled(bool isEnabled)
        {
            btnGetUsers.Enabled = isEnabled;
        }

        private Point GetPointOfClientApp(Control ctrl)
        {
            Point point = new Point();
            point.X = ctrl.Left;
            point.Y = ctrl.Top;
            while (ctrl.Parent != null)
            {
                ctrl = ctrl.Parent;
                point.X += ctrl.Left;
                point.Y += ctrl.Top;
            }
            return point;
        }

        private ILookupSuitableView GetLookupSuitableViewLink()
        {
            Control parent = this;
            while (parent != null && !(parent is ILookupSuitableView))
            {
                parent = parent.Parent;
            }
            return parent as ILookupSuitableView;
        }

        private void btnGetUsers_Click(object sender, EventArgs e)
        {
            _presenter.Refresh(_nalogOrganCode);
        }

        public List<LookupResponse> GetLookupData(LookupRequest request)
        {
            return _presenter.GetLookupData(request);
        }

        public NsiMetadataEntry GetLookupMetadata(string lookupName)
        {
            return _presenter.GetLookupMetadata(lookupName);
        }
    }
}