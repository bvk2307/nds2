﻿namespace Luxoft.NDS2.Client.UI.SettingsMRR.AssignEmployee.Dialog
{
    partial class SounDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Misc.UltraPanel upFooter;
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            this.ulSelectedCount = new Infragistics.Win.Misc.UltraLabel();
            this.ubCancel = new Infragistics.Win.Misc.UltraButton();
            this.ubOk = new Infragistics.Win.Misc.UltraButton();
            this.grid = new Luxoft.NDS2.Client.UI.Controls.Grid.V1.GridControl();
            upFooter = new Infragistics.Win.Misc.UltraPanel();
            upFooter.ClientArea.SuspendLayout();
            upFooter.SuspendLayout();
            this.SuspendLayout();
            // 
            // upFooter
            // 
            // 
            // upFooter.ClientArea
            // 
            upFooter.ClientArea.Controls.Add(this.ulSelectedCount);
            upFooter.ClientArea.Controls.Add(this.ubCancel);
            upFooter.ClientArea.Controls.Add(this.ubOk);
            upFooter.Dock = System.Windows.Forms.DockStyle.Bottom;
            upFooter.Location = new System.Drawing.Point(0, 381);
            upFooter.Name = "upFooter";
            upFooter.Size = new System.Drawing.Size(655, 38);
            upFooter.TabIndex = 2;
            // 
            // ulSelectedCount
            // 
            appearance1.TextVAlignAsString = "Middle";
            this.ulSelectedCount.Appearance = appearance1;
            this.ulSelectedCount.Location = new System.Drawing.Point(6, 6);
            this.ulSelectedCount.Name = "ulSelectedCount";
            this.ulSelectedCount.Size = new System.Drawing.Size(475, 26);
            this.ulSelectedCount.TabIndex = 2;
            // 
            // ubCancel
            // 
            this.ubCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ubCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.ubCancel.Location = new System.Drawing.Point(568, 6);
            this.ubCancel.Name = "ubCancel";
            this.ubCancel.Size = new System.Drawing.Size(75, 26);
            this.ubCancel.TabIndex = 0;
            this.ubCancel.Text = "Отменить";
            // 
            // ubOk
            // 
            this.ubOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ubOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.ubOk.Location = new System.Drawing.Point(487, 6);
            this.ubOk.Name = "ubOk";
            this.ubOk.Size = new System.Drawing.Size(75, 26);
            this.ubOk.TabIndex = 1;
            this.ubOk.Text = "Сохранить";
            // 
            // grid
            // 
            this.grid.AddVirtualCheckColumn = false;
            this.grid.AggregatePanelVisible = false;
            this.grid.AllowMultiGrouping = false;
            this.grid.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.grid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grid.BackColor = System.Drawing.Color.Transparent;
            this.grid.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.grid.FooterVisible = false;
            this.grid.Location = new System.Drawing.Point(6, 6);
            this.grid.Name = "grid";
            this.grid.PanelLoadingVisible = false;
            this.grid.PanelPagesVisible = false;
            this.grid.RowDoubleClicked = null;
            this.grid.Setup = null;
            this.grid.Size = new System.Drawing.Size(643, 369);
            this.grid.TabIndex = 3;
            this.grid.Title = "";
            // 
            // SounDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(655, 419);
            this.Controls.Add(this.grid);
            this.Controls.Add(upFooter);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SounDialog";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            upFooter.ClientArea.ResumeLayout(false);
            upFooter.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraButton ubCancel;
        private Infragistics.Win.Misc.UltraButton ubOk;
        private Controls.Grid.V1.GridControl grid;
        private Infragistics.Win.Misc.UltraLabel ulSelectedCount;

    }
}