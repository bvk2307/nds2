﻿using Infragistics.Win.UltraWinGrid;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.AssignEmployee.Dialog
{
    partial class EmployeeDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand1 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Band 0", -1);
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn1 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("DisplayName");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn2 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("UserPrincipalName");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn3 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("IS_IN_PROCESS");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn4 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SID");
            Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn5 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("SounFullName");
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            this.ultraToolTipManager1 = new Infragistics.Win.UltraWinToolTip.UltraToolTipManager(this.components);
            this.upBody = new Infragistics.Win.Misc.UltraPanel();
            this.upGrid = new Infragistics.Win.Misc.UltraPanel();
            this.ugPrincpalList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.upFooter = new Infragistics.Win.Misc.UltraPanel();
            this.ulSelectedCount = new Infragistics.Win.Misc.UltraLabel();
            this.ubSave = new Infragistics.Win.Misc.UltraButton();
            this.ubCancel = new Infragistics.Win.Misc.UltraButton();
            this.upSounCodeSelection = new Infragistics.Win.Misc.UltraPanel();
            this.ultraButtonShowControl = new Infragistics.Win.Misc.UltraButton();
            this.ulNalogOrganCode = new Infragistics.Win.Misc.UltraLabel();
            this.uteNalogOrganCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ulNalogOrganName = new Infragistics.Win.Misc.UltraLabel();
            this.uteNalogOrganName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.btnGetUsers = new Infragistics.Win.Misc.UltraButton();
            this.upBody.ClientArea.SuspendLayout();
            this.upBody.SuspendLayout();
            this.upGrid.ClientArea.SuspendLayout();
            this.upGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ugPrincpalList)).BeginInit();
            this.upFooter.ClientArea.SuspendLayout();
            this.upFooter.SuspendLayout();
            this.upSounCodeSelection.ClientArea.SuspendLayout();
            this.upSounCodeSelection.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uteNalogOrganCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uteNalogOrganName)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraToolTipManager1
            // 
            this.ultraToolTipManager1.ContainingControl = this;
            // 
            // upBody
            // 
            // 
            // upBody.ClientArea
            // 
            this.upBody.ClientArea.Controls.Add(this.upGrid);
            this.upBody.ClientArea.Controls.Add(this.upFooter);
            this.upBody.ClientArea.Controls.Add(this.upSounCodeSelection);
            this.upBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.upBody.Location = new System.Drawing.Point(0, 0);
            this.upBody.Name = "upBody";
            this.upBody.Size = new System.Drawing.Size(862, 576);
            this.upBody.TabIndex = 2;
            // 
            // upGrid
            // 
            // 
            // upGrid.ClientArea
            // 
            this.upGrid.ClientArea.Controls.Add(this.ugPrincpalList);
            this.upGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.upGrid.Location = new System.Drawing.Point(0, 88);
            this.upGrid.Name = "upGrid";
            this.upGrid.Size = new System.Drawing.Size(862, 450);
            this.upGrid.TabIndex = 5;
            // 
            // ugPrincpalList
            // 
            this.ugPrincpalList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ugPrincpalList.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            ultraGridColumn1.Header.Caption = "ФИО";
            ultraGridColumn1.Header.VisiblePosition = 2;
            ultraGridColumn1.Width = 141;
            ultraGridColumn2.Header.Caption = "Код учетной записи";
            ultraGridColumn2.Header.VisiblePosition = 1;
            ultraGridColumn2.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(171, 0);

            ultraGridColumn3.AllowRowSummaries = Infragistics.Win.UltraWinGrid.AllowRowSummaries.False;
            ultraGridColumn3.AllowGroupBy = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn3.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            ultraGridColumn3.AutoSizeMode = Infragistics.Win.UltraWinGrid.ColumnAutoSizeMode.None;
            ultraGridColumn3.CellActivation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
            ultraGridColumn3.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.Edit;
            ultraGridColumn3.Header.Caption = System.String.Empty;
            ultraGridColumn3.Header.CheckBoxSynchronization = Infragistics.Win.UltraWinGrid.HeaderCheckBoxSynchronization.None;
            ultraGridColumn3.Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Always;
            ultraGridColumn3.Header.VisiblePosition = 0;
            ultraGridColumn3.LockedWidth = true;
            //ultraGridColumn3.DataType = typeof(bool);
            ultraGridColumn3.SortIndicator = Infragistics.Win.UltraWinGrid.SortIndicator.Disabled;
            ultraGridColumn3.Width = 20;

            ultraGridColumn4.Header.VisiblePosition = 3;
            ultraGridColumn4.Hidden = true;
            ultraGridColumn5.Header.Caption = "Название налогового органа";
            ultraGridColumn5.Header.VisiblePosition = 4;
            ultraGridBand1.Columns.AddRange(new object[] {
            ultraGridColumn1,
            ultraGridColumn2,
            ultraGridColumn3,
            ultraGridColumn4,
            ultraGridColumn5});
            ultraGridBand1.RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.ColumnLayout;
            this.ugPrincpalList.DisplayLayout.BandsSerializer.Add(ultraGridBand1);
            this.ugPrincpalList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ugPrincpalList.DisplayLayout.MaxColScrollRegions = 1;
            this.ugPrincpalList.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.ugPrincpalList.DisplayLayout.Override.AllowColMoving = Infragistics.Win.UltraWinGrid.AllowColMoving.NotAllowed;
            this.ugPrincpalList.DisplayLayout.Override.AllowColSizing = Infragistics.Win.UltraWinGrid.AllowColSizing.None;
            this.ugPrincpalList.DisplayLayout.Override.AllowColSwapping = Infragistics.Win.UltraWinGrid.AllowColSwapping.NotAllowed;
            this.ugPrincpalList.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.ugPrincpalList.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.False;
            this.ugPrincpalList.DisplayLayout.Override.AllowRowSummaries = Infragistics.Win.UltraWinGrid.AllowRowSummaries.False;
            this.ugPrincpalList.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            this.ugPrincpalList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            this.ugPrincpalList.DisplayLayout.Override.DefaultRowHeight = 20;
            this.ugPrincpalList.DisplayLayout.Override.FixedRowIndicator = Infragistics.Win.UltraWinGrid.FixedRowIndicator.None;
            this.ugPrincpalList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortSingle;
            this.ugPrincpalList.DisplayLayout.Override.MergedCellStyle = Infragistics.Win.UltraWinGrid.MergedCellStyle.Never;
            this.ugPrincpalList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.ugPrincpalList.DisplayLayout.Override.RowSizing = Infragistics.Win.UltraWinGrid.RowSizing.Fixed;
            this.ugPrincpalList.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.ugPrincpalList.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.ugPrincpalList.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            this.ugPrincpalList.DisplayLayout.Override.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.None;
            this.ugPrincpalList.DisplayLayout.Override.SupportDataErrorInfo = Infragistics.Win.UltraWinGrid.SupportDataErrorInfo.None;
            this.ugPrincpalList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ugPrincpalList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ugPrincpalList.Location = new System.Drawing.Point(6, 6);
            this.ugPrincpalList.Name = "ugPrincpalList";
            this.ugPrincpalList.Size = new System.Drawing.Size(850, 438);
            this.ugPrincpalList.TabIndex = 7;
            // 
            // upFooter
            // 
            // 
            // upFooter.ClientArea
            // 
            this.upFooter.ClientArea.Controls.Add(this.ulSelectedCount);
            this.upFooter.ClientArea.Controls.Add(this.ubSave);
            this.upFooter.ClientArea.Controls.Add(this.ubCancel);
            this.upFooter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.upFooter.Location = new System.Drawing.Point(0, 538);
            this.upFooter.Name = "upFooter";
            this.upFooter.Size = new System.Drawing.Size(862, 38);
            this.upFooter.TabIndex = 4;
            // 
            // ulSelectedCount
            // 
            appearance4.TextHAlignAsString = "Left";
            appearance4.TextVAlignAsString = "Middle";
            this.ulSelectedCount.Appearance = appearance4;
            this.ulSelectedCount.Location = new System.Drawing.Point(6, 6);
            this.ulSelectedCount.Name = "ulSelectedCount";
            this.ulSelectedCount.Size = new System.Drawing.Size(493, 26);
            this.ulSelectedCount.TabIndex = 6;
            this.ulSelectedCount.Text = "Выбрано";
            // 
            // ubSave
            // 
            this.ubSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ubSave.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.ubSave.Location = new System.Drawing.Point(671, 6);
            this.ubSave.Name = "ubSave";
            this.ubSave.Size = new System.Drawing.Size(88, 26);
            this.ubSave.TabIndex = 5;
            this.ubSave.Text = "Сохранить";
            // 
            // ubCancel
            // 
            this.ubCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ubCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.ubCancel.Location = new System.Drawing.Point(765, 6);
            this.ubCancel.Name = "ubCancel";
            this.ubCancel.Size = new System.Drawing.Size(91, 26);
            this.ubCancel.TabIndex = 5;
            this.ubCancel.Text = "Отмена";
            // 
            // upSounCodeSelection
            // 
            // 
            // upSounCodeSelection.ClientArea
            // 
            this.upSounCodeSelection.ClientArea.Controls.Add(this.ultraButtonShowControl);
            this.upSounCodeSelection.ClientArea.Controls.Add(this.ulNalogOrganCode);
            this.upSounCodeSelection.ClientArea.Controls.Add(this.uteNalogOrganCode);
            this.upSounCodeSelection.ClientArea.Controls.Add(this.ulNalogOrganName);
            this.upSounCodeSelection.ClientArea.Controls.Add(this.uteNalogOrganName);
            this.upSounCodeSelection.ClientArea.Controls.Add(this.ultraLabel1);
            this.upSounCodeSelection.ClientArea.Controls.Add(this.btnGetUsers);
            this.upSounCodeSelection.Dock = System.Windows.Forms.DockStyle.Top;
            this.upSounCodeSelection.Location = new System.Drawing.Point(0, 0);
            this.upSounCodeSelection.Name = "upSounCodeSelection";
            this.upSounCodeSelection.Size = new System.Drawing.Size(862, 88);
            this.upSounCodeSelection.TabIndex = 2;
            // 
            // ultraButtonShowControl
            // 
            this.ultraButtonShowControl.Location = new System.Drawing.Point(199, 32);
            this.ultraButtonShowControl.Name = "ultraButtonShowControl";
            this.ultraButtonShowControl.Size = new System.Drawing.Size(156, 23);
            this.ultraButtonShowControl.TabIndex = 17;
            this.ultraButtonShowControl.Text = "Выбор из справочника";
            this.ultraButtonShowControl.Click += new System.EventHandler(this.ultraButtonShowControl_Click);
            // 
            // ulNalogOrganCode
            // 
            this.ulNalogOrganCode.Location = new System.Drawing.Point(6, 34);
            this.ulNalogOrganCode.Name = "ulNalogOrganCode";
            this.ulNalogOrganCode.Size = new System.Drawing.Size(64, 17);
            this.ulNalogOrganCode.TabIndex = 21;
            this.ulNalogOrganCode.Text = "Код:";
            // 
            // uteNalogOrganCode
            // 
            this.uteNalogOrganCode.Location = new System.Drawing.Point(76, 32);
            this.uteNalogOrganCode.Name = "uteNalogOrganCode";
            this.uteNalogOrganCode.Size = new System.Drawing.Size(116, 21);
            this.uteNalogOrganCode.TabIndex = 20;
            this.uteNalogOrganCode.ValueChanged += new System.EventHandler(this.uteNalogOrganCode_ValueChanged);
            // 
            // ulNalogOrganName
            // 
            this.ulNalogOrganName.Location = new System.Drawing.Point(6, 61);
            this.ulNalogOrganName.Name = "ulNalogOrganName";
            this.ulNalogOrganName.Size = new System.Drawing.Size(64, 17);
            this.ulNalogOrganName.TabIndex = 19;
            this.ulNalogOrganName.Text = "Название:";
            // 
            // uteNalogOrganName
            // 
            this.uteNalogOrganName.Location = new System.Drawing.Point(76, 59);
            this.uteNalogOrganName.Name = "uteNalogOrganName";
            this.uteNalogOrganName.ReadOnly = true;
            this.uteNalogOrganName.Size = new System.Drawing.Size(671, 21);
            this.uteNalogOrganName.TabIndex = 18;
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Location = new System.Drawing.Point(6, 12);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(312, 17);
            this.ultraLabel1.TabIndex = 16;
            this.ultraLabel1.Text = "Налоговый орган";
            // 
            // btnGetUsers
            // 
            this.btnGetUsers.Location = new System.Drawing.Point(753, 59);
            this.btnGetUsers.Name = "btnGetUsers";
            this.btnGetUsers.Size = new System.Drawing.Size(103, 23);
            this.btnGetUsers.TabIndex = 15;
            this.btnGetUsers.Text = "Применить";
            this.btnGetUsers.Click += new System.EventHandler(this.btnGetUsers_Click);
            // 
            // EmployeeDialog
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(862, 576);
            this.Controls.Add(this.upBody);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EmployeeDialog";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.upBody.ClientArea.ResumeLayout(false);
            this.upBody.ResumeLayout(false);
            this.upGrid.ClientArea.ResumeLayout(false);
            this.upGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ugPrincpalList)).EndInit();
            this.upFooter.ClientArea.ResumeLayout(false);
            this.upFooter.ResumeLayout(false);
            this.upSounCodeSelection.ClientArea.ResumeLayout(false);
            this.upSounCodeSelection.ClientArea.PerformLayout();
            this.upSounCodeSelection.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uteNalogOrganCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uteNalogOrganName)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinToolTip.UltraToolTipManager ultraToolTipManager1;
        private Infragistics.Win.Misc.UltraPanel upBody;
        private Infragistics.Win.Misc.UltraPanel upSounCodeSelection;
        private Infragistics.Win.Misc.UltraPanel upGrid;
        private Infragistics.Win.Misc.UltraPanel upFooter;
        private Infragistics.Win.Misc.UltraButton ubSave;
        private Infragistics.Win.Misc.UltraButton ubCancel;
        private Infragistics.Win.Misc.UltraLabel ulSelectedCount;
        private Infragistics.Win.UltraWinGrid.UltraGrid ugPrincpalList;
        private Infragistics.Win.Misc.UltraButton ultraButtonShowControl;
        private Infragistics.Win.Misc.UltraLabel ulNalogOrganCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uteNalogOrganCode;
        private Infragistics.Win.Misc.UltraLabel ulNalogOrganName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uteNalogOrganName;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraButton btnGetUsers;
    }
}