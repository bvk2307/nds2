﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.AssignEmployee.Dialog
{
    public partial class SounDialog : Form
    {
        public string Caption
        {
            get { return Text; }
            set { Text = value; }
        }

        public GridControl Grid
        {
            get { return grid; }
        }

        public SounDialog(SounDialogPresenerBase presenter)
        {
            _presenter = presenter;
            InitializeComponent();
            grid.OnGridRowsCheckedStateChanged += (sender, e) => UpdateSelectedCount();
            ubOk.Enabled = grid.RowSelected.Count > 0;
        }

        private readonly SounDialogPresenerBase _presenter;

        public void UpdateSelectedCount()
        {
            ulSelectedCount.Text = string.Format("Выбрано инспекций: {0}", grid.RowSelected.Count < _presenter.InspectionsTotalCount ? grid.RowSelected.Count.ToString() : "Все инспекции региона");
            ubOk.Enabled = grid.RowSelected.Count > 0;
        }
    }
}