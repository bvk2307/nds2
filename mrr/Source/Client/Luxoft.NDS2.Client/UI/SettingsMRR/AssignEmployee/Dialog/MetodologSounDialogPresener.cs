﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Providers;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.Services;
using Microsoft.Practices.CompositeUI;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.AssignEmployee.Dialog
{
    public abstract class SounDialogPresenerBase
    {
        private readonly SounDialog _dialog;
        private readonly GridSetup _setup;

        public SounDialog View
        {
            get { return _dialog; }
        }

        protected string Caption
        {
            get { return _dialog.Caption; }
            set { _dialog.Caption = value; }
        }

        protected GridSetup Setup
        {
            get { return _setup; }
        }

        public int InspectionsTotalCount { get; set; }

        protected abstract object UpdateGrid(QueryConditions conditions, out long rowCount);

        public SounDialogPresenerBase(WorkItem wi)
        {
            _dialog = new SounDialog(this);
            _setup = new GridSetup(new CommonSettingsProvider(wi, GetType().FullName)) { GetData = UpdateGrid };
        }

        public virtual DialogResult ShowDialog()
        {
            _dialog.Grid.Setup = Setup;
            _dialog.Grid.UpdateData();
            _dialog.UpdateSelectedCount();
            return _dialog.ShowDialog();
        }
    }

    public sealed class MetodologSounDialogPresener : SounDialogPresenerBase
    {
        private readonly Region _region;
        private readonly IUserToRegionService _service;
        private readonly IUcMessageService _messager;

        public MetodologSounDialogPresener(WorkItem wi, Region region, IUserToRegionService service, IUcMessageService messager)
            : base(wi)
        {
            _service = service;
            _region = region;
            _messager = messager;
            Caption = string.Format("Инспекции региона {0} «{1}»", _region.Code, _region.Name);
            SetupGrid();
            View.UpdateSelectedCount();
        }

        private void SetupGrid()
        {
            var helper = new GridSetupHelper<Inspection>();
            Setup.Columns.AddRange(new[]
            {
                helper.CreateColumnDefinition(obj => obj.Selected, c => c.SelectionColumn = true),
                helper.CreateColumnDefinition(obj => obj.Code),
                helper.CreateColumnDefinition(obj => obj.Name)
            });
        }

        protected override object UpdateGrid(QueryConditions conditions, out long rowCount)
        {
            rowCount = 0;
            var result = _service.GetSounDialogData(_region, conditions ?? View.Grid.QueryConditions);
            if (result.Status != ResultStatus.Success)
            {
                _messager.ShowError(new MessageInfoContext("Ошибка", result.Message));
                InspectionsTotalCount = 0;
            }
            else
            {
                InspectionsTotalCount = result.Result.TotalInspectionCount;
            }

            return result.Result.Inspections;
        }

        public override DialogResult ShowDialog()
        {
            var result = base.ShowDialog();
            if (result == DialogResult.OK)
            {
                _region.Inspections = View.Grid.Rows.Cast<Inspection>().ToList();
                _service.SaveSounDialogData(_region);
            }
            return result;
        }
    }
}