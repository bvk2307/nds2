﻿using System;
using System.Collections.Generic;
using System.Linq;
using CommonComponents.Directory;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.AssignEmployee.Dialog
{
    public sealed class PrincipalInfo
    {
        public bool Selected { get; set; }

        public string SID { get; private set; }

        public string DisplayName { get; private set; }

        public string UserPrincipalName { get; private set; }

        public string SounFullName { get; private set; }

        private PrincipalInfo() { }

        public static PrincipalInfo FromSid(IUserInfoService userInfoService, string sid)
        {
            try
            {
                return userInfoService.GetUserInfoBySID(sid);
            }
            catch
            {
                return null;
            }
        }

        public static IEnumerable<PrincipalInfo> FromSid(IUserInfoService userInfoService, IEnumerable<string> sids)
        {
            return sids.Select(sid => FromSid(userInfoService, sid));
        }

        public static PrincipalInfo GetCurrentPrincipal(IUserInfoService userInfoService)
        {
            return userInfoService.GetCurrentUserInfo();
        }

        public static implicit operator PrincipalInfo(UserInfo user)
        {
            return new PrincipalInfo
            {
                SID = user.ObjectSID,
                DisplayName = user.DisplayName,
                UserPrincipalName = user.UserPrincipalName,
                SounFullName = GetSounName(user)
            };
        }

        private static string GetSounName(UserInfo user)
        {
            switch (user.Level)
            {
                case Level.CA:
                    return "Центральный аппарат";
                case Level.IFNS:
                case Level.MIFNS:
                    return user.IFNS;
                case Level.UFNS:
                    return user.UFNS;
                default:
                    return null;
            }
        }

        public override bool Equals(object obj)
        {
            if (object.ReferenceEquals(obj, this))
            {
                return true;
            }
            var other = obj as PrincipalInfo;
            if (object.ReferenceEquals(other, null))
            {
                return false;
            }

            Func<string, string, bool> equal = (str1, str2) => str1 == null ? str2 == null : str1.Equals(str2);
            return equal(this.SID, other.SID)
                && equal(this.UserPrincipalName, other.UserPrincipalName)
                && equal(this.DisplayName, other.DisplayName)
                && equal(this.SounFullName, other.SounFullName);
        }

        public override int GetHashCode()
        {
            return SID.GetHashCode();
        }
    }
}