﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using CommonComponents.Directory;
using CommonComponents.Instrumentation;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using CommonComponents.Utils.Async;
using Luxoft.NDS2.Common.Contract.DTO.SearchEntity;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.Services;
using Microsoft.Practices.CompositeUI;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.AssignEmployee.Dialog
{
    public sealed class EmployeeDialogPresenter
    {
        private readonly EmployeeData _outputData;
        private readonly IEmployeeDialogDataService _service;
        private readonly IUserInfoService _userInfoService;
        private readonly IUcMessageService _messagerService;
        private readonly IDictionaryDataService _dictionaryService;


        private EmployeeDialog _dialog;
        private EmployeeType _type;
        private Region _region;
        private List<string> _initialSids;

        public EmployeeData OutputData
        {
            get { return _outputData; }
        }

        public EmployeeDialogPresenter(WorkItem wi, IEmployeeDialogDataService service, IUserInfoService userInfo, IUcMessageService messager, IDictionaryDataService dictionary)
        {
            _service = service;
            _userInfoService = userInfo;
            _messagerService = messager;
            _dictionaryService = dictionary;
            _outputData = new EmployeeData();
            _initialSids = new List<string>();
        }

        public DialogResult ShowDialog(Region region, EmployeeType type)
        {
            _region = region;
            _type = type;
            string title, countFormat;
            switch (type)
            {
                case EmployeeType.Analyst:
                    title = "Выбор аналитиков";
                    countFormat = "Выбрано аналитиков: {0}";
                    break;
                case EmployeeType.Approver:
                    title = "Выбор согласующих";
                    countFormat = "Выбрано согласующих: {0}";
                    break;
                default:
                    throw new ArgumentException("Неизвестный тип объекта", "type");
            }
            _dialog = new EmployeeDialog(this) { Text = title, SelectedCountFormat = countFormat };
            return _dialog.ShowDialog();
        }

        public void InitializeView()
        {
            var result = _service.Load(_region.Id, _type);
            if (result.Status != ResultStatus.Success)
            {
                ShowError(result.Message);
                return;
            }

            _dialog.DataSource.Clear();
            var principals =
                from sid in result.Result.Sids
                let principal = PrincipalInfo.FromSid(_userInfoService, sid)
                where principal != null
                select principal;

            foreach (var principal in principals)
            {
                principal.Selected = true;
                _dialog.DataSource.Add(principal);
            }
            _dialog.UpdateSelectedCount();
            _initialSids = principals.Select(principal => principal.SID).ToList();
        }

        public void Refresh(string code)
        {
            var result = _service.GetSIDsByCode(_type, code);
            if (result.Status != ResultStatus.Success)
            {
                //"Налоговый орган с данным кодом отсутсвует"
                ShowWarning(result.Message);
            }

            var unselected = _dialog.DataSource.Where(p => !p.Selected).ToList();
            unselected.ForEach(item => _dialog.DataSource.Remove(item));

            var piList =
                from sid in result.Result ?? new List<string>()
                let principal = PrincipalInfo.FromSid(_userInfoService, sid)
                where principal != null
                    && !_dialog.DataSource.Contains(principal)
                //&& principal.CodeNO == code
                select principal;

            foreach (var principal in piList)
            {
                _dialog.DataSource.Add(principal);
            }
        }

        public void Close()
        {
            if (_dialog != null)
            {
                _dialog.Close();
                _dialog.Dispose();
                _dialog = null;
            }
            _outputData.Sids.Clear();
        }

        #region Save
        public void Save()
        {
            var sids =
                from principal in _dialog.DataSource
                where principal.Selected
                select principal.SID;
            _outputData.Type = _type;
            _outputData.Sids.Clear();
            _outputData.Sids.AddRange(sids);

            var result = _service.Save(_region, _outputData);
            if (result.Status != ResultStatus.Success)
            {
                ShowError("Ошибка при сохранении: " + result.Message);
                return;
            }
            _dialog.DialogResult = DialogResult.OK;
            Close();
        }
        #endregion

        #region Helpers
        private void ShowError(string message)
        {
            _messagerService.ShowError(new MessageInfoContext("Ошибка", message));
        }
        private void ShowWarning(string message)
        {
            _messagerService.Show(new MessageInfoContext("Предупреждение", message, MessageCategory.Exclamation));
        }
        public DialogResult ShowQuestion(string message)
        {
            MessageInfoContext mess = new MessageInfoContext("Предупреждение", message, MessageCategory.Question) { Buttons = MessageBoxButtons.YesNo };
            return _messagerService.ShowQuestion(mess);
        }
        #endregion

        #region NSI
        private T Execute<T>(Func<OperationResult<T>> get) where T : class, new()
        {
            var opResult = get();
            if (opResult.Status != ResultStatus.Success)
            {
                ShowError(opResult.Message);
                return new T();
            }
            return opResult.Result;
        }

        public NsiMetadataEntry GetLookupMetadata(string lookupName)
        {
            return Execute(() => _dictionaryService.GetLookupMetadata(lookupName));
        }

        public List<LookupResponse> GetLookupData(Common.Contract.DTO.SearchEntity.LookupRequest request)
        {
            return Execute(() => _dictionaryService.GetLookupData(request));
        }
        #endregion
    }
}