﻿namespace Luxoft.NDS2.Client.UI.SettingsMRR.AssignEmployee
{
    partial class View
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            this.upHeader = new Infragistics.Win.Misc.UltraPanel();
            this.ulTitle = new Infragistics.Win.Misc.UltraLabel();
            this.upBody = new Infragistics.Win.Misc.UltraPanel();
            this.grid = new Luxoft.NDS2.Client.UI.Controls.Grid.V1.GridControl();
            this.upHeader.ClientArea.SuspendLayout();
            this.upHeader.SuspendLayout();
            this.upBody.ClientArea.SuspendLayout();
            this.upBody.SuspendLayout();
            this.SuspendLayout();
            // 
            // upHeader
            // 
            // 
            // upHeader.ClientArea
            // 
            this.upHeader.ClientArea.Controls.Add(this.ulTitle);
            this.upHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.upHeader.Location = new System.Drawing.Point(0, 0);
            this.upHeader.Name = "upHeader";
            this.upHeader.Size = new System.Drawing.Size(1081, 35);
            this.upHeader.TabIndex = 0;
            // 
            // ulTitle
            // 
            this.ulTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.TextHAlignAsString = "Left";
            appearance1.TextVAlignAsString = "Middle";
            this.ulTitle.Appearance = appearance1;
            this.ulTitle.Location = new System.Drawing.Point(6, 6);
            this.ulTitle.Name = "ulTitle";
            this.ulTitle.Size = new System.Drawing.Size(1069, 23);
            this.ulTitle.TabIndex = 0;
            this.ulTitle.Text = "Список назначений согласующих и аналитиков на регионы и инспекции регионов";
            // 
            // upBody
            // 
            // 
            // upBody.ClientArea
            // 
            this.upBody.ClientArea.Controls.Add(this.grid);
            this.upBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.upBody.Location = new System.Drawing.Point(0, 35);
            this.upBody.Name = "upBody";
            this.upBody.Size = new System.Drawing.Size(1081, 536);
            this.upBody.TabIndex = 1;
            // 
            // grid
            // 
            this.grid.AddVirtualCheckColumn = false;
            this.grid.AggregatePanelVisible = false;
            this.grid.AllowMultiGrouping = false;
            this.grid.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.grid.BackColor = System.Drawing.Color.Transparent;
            this.grid.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grid.FooterVisible = false;
            this.grid.Location = new System.Drawing.Point(0, 0);
            this.grid.Name = "grid";
            this.grid.PanelLoadingVisible = false;
            this.grid.PanelPagesVisible = false;
            this.grid.RowDoubleClicked = null;
            this.grid.Setup = null;
            this.grid.Size = new System.Drawing.Size(1081, 536);
            this.grid.TabIndex = 1;
            this.grid.Title = "";
            // 
            // View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.upBody);
            this.Controls.Add(this.upHeader);
            this.Name = "View";
            this.Size = new System.Drawing.Size(1081, 571);
            this.upHeader.ClientArea.ResumeLayout(false);
            this.upHeader.ResumeLayout(false);
            this.upBody.ClientArea.ResumeLayout(false);
            this.upBody.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraPanel upHeader;
        private Infragistics.Win.Misc.UltraLabel ulTitle;
        private Infragistics.Win.Misc.UltraPanel upBody;
        private Controls.Grid.V1.GridControl grid;

    }
}
