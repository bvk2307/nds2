﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using CommonComponents.Directory;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Providers;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;
using Luxoft.NDS2.Client.UI.SettingsMRR.AssignEmployee.Dialog;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.Services;
using Microsoft.Practices.CompositeUI;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.AssignEmployee
{
    public sealed class ResponsibilityRegionPresenter : BasePresenter<View>
    {
        private IUserToRegionService _service;
        private IUserInfoService _userInfo;
        private const int CONTEXT_ID = 2;

        private IUserToRegionService Service
        {
            get { return _service; }
        }

        public ResponsibilityRegionPresenter()
        {

        }

        public override void OnObjectCreated()
        {
            base.OnObjectCreated();
            InitializePresenter();
        }

        private void InitializePresenter()
        {
            _service = GetServiceProxy<IUserToRegionService>();
            _userInfo = WorkItem.Services.Get<IUserInfoService>();
            View.Grid.Setup = GetSetup();
            View.Grid.UpdateData();
        }

        private GridSetup GetSetup()
        {
            var result = new GridSetup(new CommonSettingsProvider(WorkItem, GetType().ToString()));
            var helper = new GridSetupHelper<ResponsibilityRegion>();

            Func<CellEventHandler, Action<ColumnDefinition>> addButton = handler => column =>
            {
                column.StyleDefinition.Style = ColumnStyle.EditButton;
                column.StyleDefinition.Image = Luxoft.NDS2.Client.Properties.Resources.ThreeDots;
                column.StyleDefinition.Handler = handler;
            };


            result.Columns.AddRange(new[]
            {
                helper.CreateColumnDefinition(rr => rr.RegionFullName, c => c.Sorting = new ColumnSort{ Order = ColumnSort.SortOrder.Asc }),
                helper.CreateColumnDefinition(rr => rr.InspectionsList, addButton(InspectionsDialog)),
                helper.CreateColumnDefinition(rr => rr.AnalystsList, addButton(AnalystDialog)),
                helper.CreateColumnDefinition(rr => rr.ApproversList, addButton(ApproverDialog))
            });

            result.RowActions.Add(SetColor);
            result.GetData = GetResponsibilityRegions;
            return result;
        }

        private object GetResponsibilityRegions(QueryConditions conditions, out long rowCount)
        {
            rowCount = 0;
            var result = _service.GetResponsibilityRegionData(CONTEXT_ID, conditions);
            if (result.Status != ResultStatus.Success)
            {
                View.ShowError("Ошибка при загрузке данных: " + result.Message);
                return null;
            }

            foreach (var rRegion in result.Result)
            {
                rRegion.AnalystsList = GetEmployeesString(rRegion.Analitics.Sids, EmployeeType.Analyst);
                rRegion.ApproversList = GetEmployeesString(rRegion.Approvers.Sids, EmployeeType.Approver);
            }
            return result.Result;
        }

        private string GetEmployeesString(IEnumerable<string> sids, EmployeeType type)
        {
            var names =
                from sid in sids
                let userInfo = _userInfo.GetUserInfoBySID(sid)
                select userInfo.DisplayName;

            return string.Join(", ", names);
        }

        private void InspectionsDialog(object sender, CellEventArgs e)
        {
            var instance = (ResponsibilityRegion)e.Cell.Row.ListObject;
            var presenter = new MetodologSounDialogPresener(WorkItem, instance.Region, _service, WorkItem.Services.Get<IUcMessageService>());
            if (presenter.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                View.Grid.UpdateData();
            }
        }

        private void AnalystDialog(object sender, CellEventArgs e)
        {
            EmployeeDialog(e, EmployeeType.Analyst);
        }

        private void ApproverDialog(object sender, CellEventArgs e)
        {
            EmployeeDialog(e, EmployeeType.Approver);
        }

        private void EmployeeDialog(CellEventArgs e, EmployeeType type)
        {
            var presenter = new EmployeeDialogPresenter(
                WorkItem, 
                GetServiceProxy<IEmployeeDialogDataService>(),
                _userInfo,
                WorkItem.Services.Get<IUcMessageService>(),
                GetServiceProxy<IDictionaryDataService>());
            var instance = (ResponsibilityRegion)e.Cell.Row.ListObject;
            if (presenter.ShowDialog(instance.Region, type) == System.Windows.Forms.DialogResult.OK)
            {
                View.Grid.UpdateData();
            }
        }

        private void SetColor(UltraGridRow row)
        {
            var rRegion = (ResponsibilityRegion)row.ListObject;
            var condition = rRegion.Analitics.Sids.Any() & rRegion.Approvers.Sids.Any();
            if (condition)
            {
                row.Appearance.ResetBackColor();
            }
            else
            {
                row.Appearance.BackColor = Color.LightPink;
            }
        }
    }
}