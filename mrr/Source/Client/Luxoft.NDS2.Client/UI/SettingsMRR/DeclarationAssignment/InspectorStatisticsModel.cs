﻿using Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment;
using System.ComponentModel;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.DeclarationAssignment
{
    public class InspectorStatisticsModel
    {
        private InspectorStatistics _inspector;

        public InspectorStatisticsModel(InspectorStatistics inspector)
        {
            _inspector = inspector;
        }

        public string SID { get { return _inspector.SID; } }

        [DisplayName("ФИО")]
        [Description("ФИО инспектора")]
        public string Name { get { return _inspector.NAME; } }

        [DisplayName("Табельный номер")]
        [Description("Табельный номер инспектора")]
        public string EmployeeNum { get { return _inspector.EMPLOYEE_NUM; } }

        public string DisplayField { get { return Name + " - " + EmployeeNum; } }

        [DisplayName("Кол-во")]
        [Description("Кол-во назначенных НД к возмещению")]
        public decimal NdCompensationAmnt { get { return _inspector.COMPENSATION_CNT; } }

        [DisplayName("НДС к возмещению")]
        [Description("НДС по назначенным НД к возмещению")]
        public decimal NdsCompensation { get { return _inspector.NDS_COMPENSATION_AMOUNT; } }

        [DisplayName("Кол-во")]
        [Description("Кол-во назначенных НД к уплате/нулевой НД")]
        public decimal NdPayCnt { get { return _inspector.PAY_CNT; } }

        [DisplayName("Сумма расхождений по АТ")]
        [Description("Сумма расхождений по автотребованиям по СФ НД к уплате(нулевой НД)")]
        public decimal SumGapAt { get { return _inspector.CLAIM_AMOUNT; } }

        [DisplayName("")]
        [Description("Наличие доступа")]
        public bool IsActive { get { return _inspector.IS_ACTIVE; }}


        public object[] GetUltraRow()
        {
            return new object[] { EmployeeNum, Name, DisplayField};
        }

        public InspectorStatistics GetInspector()
        {
            return _inspector;
        }
    }
}
