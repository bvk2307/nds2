﻿using System.Collections.Generic;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.Services.DeclarationAssignment;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.DeclarationAssignment.Workload
{
    public class Presenter : BasePresenter<View>
    {
        private IInspectorStatisticsService _inspectorStatistisService;
        private PagedDataGridPresenter<WorkloadModel> _gridPresenter;

        public override void OnObjectCreated()
        {
            base.OnObjectCreated();
            View.GridInit += ViewGridInit;
            View.RefreshData += ViewRefreshData;
            _inspectorStatistisService = GetServiceProxy<IInspectorStatisticsService>();

        }

        void ViewRefreshData(object sender, System.EventArgs e)
        {
            _gridPresenter.Load();
        }

        private void ViewGridInit(object sender, GridInitEventArgs e)
        {
            var conditions = new QueryConditions
            {
                Sorting = new List<ColumnSort> 
                { 
                    new ColumnSort
                    {
                        ColumnKey = TypeHelper<WorkloadModel>.GetMemberName( w=>  w.IS_ACTIVE), 
                        Order = ColumnSort.SortOrder.Desc
                    },
                    new ColumnSort
                    {
                        ColumnKey = TypeHelper<WorkloadModel>.GetMemberName( w=>  w.NDS_COMPENSATION_AMOUNT), 
                        Order = ColumnSort.SortOrder.Asc
                    },
                    new ColumnSort
                    {
                        ColumnKey = TypeHelper<WorkloadModel>.GetMemberName( w=>  w.CLAIM_AMOUNT), 
                        Order = ColumnSort.SortOrder.Asc
                    } 
                }
            };
            _gridPresenter = new PagedDataGridPresenter<WorkloadModel>(e.Grid, e.Pager, GetSourceData, conditions);
            _gridPresenter.Init(SettingsProvider(string.Format("{0}_InitializeGrid", GetType())));
            _gridPresenter.Load();
        }

        private PageResult<WorkloadModel> GetSourceData(QueryConditions arg)
        {
            var pageResult = new PageResult<InspectorStatistics>();

            ExecuteServiceCall(() => _inspectorStatistisService.GetFullInspectorsStatistics(arg), result => { pageResult = result.Result; });

            return new PageResult<WorkloadModel>(pageResult.Rows.Select(r => new WorkloadModel(r)).ToList(), pageResult.TotalMatches, pageResult.TotalAvailable);
        }
    }
}
