﻿using System;
using System.Drawing;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using CommonComponents.Uc.Infrastructure.Interface.UI;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Addins;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination;
using Microsoft.Practices.CompositeUI.Commands;
using Microsoft.Practices.ObjectBuilder;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.DeclarationAssignment.Workload
{
    public partial class View : BaseView
    {
        private Presenter _presenter;

        [CreateNew]
        public Presenter Presenter
        {
            set
            {
                _presenter = value;
                _presenter.PresentationContext = _presentationContext;
                _presenter.View = this;
                WorkItem = _presenter.WorkItem;
                _presenter.OnObjectCreated();
                InitGrid();
                InitializeRibbon();
            }
        }

        public View(PresentationContext ctx)
            : base(ctx)
        {
            InitializeComponent();
        }
        public View()
        {
            InitializeComponent();
        }

        public event EventHandler<GridInitEventArgs> GridInit;


        public event EventHandler RefreshData;

        private void InitializeRibbon()
        {
            IUcResourceManagersService resourceManagersService = _presenter.WorkItem.Services.Get<IUcResourceManagersService>(true);
            if (!resourceManagersService.Contains(ResourceManagerNDS2.NDS2ClientResources))
            {
                resourceManagersService.Add(ResourceManagerNDS2.NDS2ClientResources, Properties.Resources.ResourceManager);
            }

            //закладка
            UcRibbonTabContext tabNavigator = new UcRibbonTabContext(_presenter.PresentationContext, "NDS2Result")
            {
                Text = _presenter.PresentationContext.WindowTitle,
                ToolTipText = _presenter.PresentationContext.WindowTitle,
                Visible = true,
                Order = 1
            };

            UcRibbonGroupContext groupNavigation1 = tabNavigator.AddGroup("NDS2DeclarationListManage");
            groupNavigation1.Text = "Функции";
            groupNavigation1.Visible = true;

            var btnRefresh = new UcRibbonButtonToolContext(_presenter.PresentationContext, "btnRefresh", "cmdRefresh")
            {
                Text = "Обновить",
                ToolTipText = "Обновить",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "update",
                SmallImageName = "update",
                Enabled = true
            };


            groupNavigation1.ToolList.AddRange(
                new UcRibbonToolInstanceSettings[] { 
                    new UcRibbonToolInstanceSettings(btnRefresh.ItemName, UcRibbonToolSize.Large)
                });

            //общее
            _presentationContext.UiVisualizationCollection.AddRange(new VisualizationElementBase[]
            {
                btnRefresh,
                tabNavigator
            });

            _presentationContext.ActiveMenuTab = tabNavigator.ItemName;
        }

        [CommandHandler("cmdRefresh")]
        public virtual void RefreshtnClick(object sender, EventArgs e)
        {
            OnRefreshData();
        }

        private void InitGrid()
        {

            var pagerStateViewModel = new PagerStateViewModel();
            var pageNavigatorSource = new ExtendedDataGridPageNavigator(pagerStateViewModel);
            pageNavigatorSource.HidePageSizer();
            _declarationsGrid.WithPager(pageNavigatorSource);
            _declarationsGrid.RegisterTableAddin(new ExpansionIndicatorRemover());
            var helper = new ColumnHelper<WorkloadModel>(_declarationsGrid);
            var setup = new GridColumnSetup();
            var acessTooltips = new Dictionary<string, string>
                                                               {
                                                                   {false.ToString(), ""},
                                                                   {true.ToString(), "У инспектора нет доступа"}  
                                                               };
            var enableIcons = new Dictionary<bool, Bitmap>
                            {
                                {false, Properties.Resources.disabled},
                                {true, null}
                            };

            setup.Columns.Add(helper.CreateImageColumn(model => model.IS_ACTIVE, this, enableIcons, 2).Configure(col =>
            {
                col.
                    DisableSort
                    =
                    true;
                col.
                    DisableFilter
                    =
                    true;
                col.
                    DisableMoving
                    =
                    true;
                col.
                    Width
                    =
                    10;
                col.HeaderToolTip = "Наличие доступа";
                col.ToolTipViewer = new DictionaryToolTipViewer(acessTooltips);
            }));

            var inspectorGroup = helper.CreateGroup("Инспектор", 2);
            inspectorGroup.Columns.Add(helper.CreateTextColumn(m => m.NAME).Configure(c => c.DisableMoving = true));
            inspectorGroup.Columns.Add(helper.CreateTextColumn(m => m.EMPLOYEE_NUM).Configure(c => c.DisableMoving = true));
            setup.Columns.Add(inspectorGroup);

            var compensationGroup = helper.CreateGroup("НД к возмещению", 2);
            compensationGroup.Columns.Add(helper.CreateTextColumn(m => m.COMPENSATION_CNT).Configure(c => c.DisableMoving = true));
            compensationGroup.Columns.Add(helper.CreateTextColumn(m => m.NDS_COMPENSATION_AMOUNT).Configure(c => c.DisableMoving = true));
            setup.Columns.Add(compensationGroup);

            var paymentGroup = helper.CreateGroup("НД к уплате", 2);
            paymentGroup.Columns.Add(helper.CreateTextColumn(m => m.PAY_CNT).Configure(c => c.DisableMoving = true));
            paymentGroup.Columns.Add(helper.CreateTextColumn(m => m.CLAIM_AMOUNT).Configure(c => c.DisableMoving = true));
            setup.Columns.Add(paymentGroup);

            _declarationsGrid.InitColumns(setup);
            OnGridInit(new GridInitEventArgs(_declarationsGrid, pagerStateViewModel));
        }

        private void OnRefreshData()
        {
            var tmp = RefreshData;
            if (tmp != null)
            {
                tmp(this, EventArgs.Empty);
            }
        }

        private void OnGridInit(GridInitEventArgs e)
        {
            var tmp = GridInit;
            if (tmp != null)
            {
                tmp(this, e);
            }
        }
    }
}
