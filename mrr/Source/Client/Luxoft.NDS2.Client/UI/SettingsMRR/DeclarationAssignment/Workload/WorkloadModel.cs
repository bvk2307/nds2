﻿using Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment;
using System.ComponentModel;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.DeclarationAssignment.Workload
{
    public class WorkloadModel
    {
         private InspectorStatistics _inspector;

         public WorkloadModel(InspectorStatistics inspector)
        {
            _inspector = inspector;
        }

        public string SID { get { return _inspector.SID; } }

        [DisplayName("ФИО")]
        [Description("ФИО инспектора")]
        public string NAME { get { return _inspector.NAME; } }

        [DisplayName("Табельный №")]
        [Description("Табельный номер инспектора")]
        public string EMPLOYEE_NUM { get { return _inspector.EMPLOYEE_NUM; } }


        [DisplayName("Кол-во")]
        [Description("Кол-во назначенных НД к возмещению")]
        public decimal COMPENSATION_CNT { get { return _inspector.COMPENSATION_CNT; } }

        [DisplayName("НДС к возмещению")]
        [Description("НДС по назначенным НД к возмещению")]
        public decimal NDS_COMPENSATION_AMOUNT { get { return _inspector.NDS_COMPENSATION_AMOUNT; } }

        [DisplayName("Кол-во")]
        [Description("Кол-во назначенных НД к уплате")]
        public decimal PAY_CNT { get { return _inspector.PAY_CNT; } }

        [DisplayName("Сумма расхождений по АТ")]
        [Description("Сумма расхождений по автотребованиям по СФ НД к уплате(нулевой НД)")]
        public decimal CLAIM_AMOUNT { get { return _inspector.CLAIM_AMOUNT; } }

        [DisplayName("")]
        [Description("Наличие доступа")]
        public bool IS_ACTIVE { get { return _inspector.IS_ACTIVE; } }


        public InspectorStatistics GetInspector()
        {
            return _inspector;
        }
    }
}
