﻿namespace Luxoft.NDS2.Client.UI.SettingsMRR.DeclarationAssignment.Workload
{
    partial class View
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this._declarationsGrid = new Luxoft.NDS2.Client.UI.Controls.Grid.V2.DataGridView();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraPanel1
            // 
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this._declarationsGrid);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel1.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(834, 778);
            this.ultraPanel1.TabIndex = 21;
            // 
            // _declarationsGrid
            // 
            this._declarationsGrid.AggregatePanelVisible = true;
            this._declarationsGrid.AllowFilterReset = false;
            this._declarationsGrid.AllowMultiGrouping = true;
            this._declarationsGrid.AllowResetSettings = false;
            this._declarationsGrid.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this._declarationsGrid.BackColor = System.Drawing.Color.Transparent;
            this._declarationsGrid.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.None;
            this._declarationsGrid.ColumnVisibilitySetupButtonVisible = false;
            this._declarationsGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._declarationsGrid.ExportExcelCancelVisible = false;
            this._declarationsGrid.ExportExcelVisible = false;
            this._declarationsGrid.FilterResetVisible = false;
            this._declarationsGrid.FooterVisible = true;
            this._declarationsGrid.Location = new System.Drawing.Point(0, 0);
            this._declarationsGrid.Name = "_declarationsGrid";
            this._declarationsGrid.PanelExportExcelStateVisible = false;
            this._declarationsGrid.PanelLoadingVisible = true;
            this._declarationsGrid.PanelPagesVisible = true;
            this._declarationsGrid.RowDoubleClicked = null;
            this._declarationsGrid.Size = new System.Drawing.Size(834, 778);
            this._declarationsGrid.TabIndex = 0;
            this._declarationsGrid.Title = "";
            // 
            // View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ultraPanel1);
            this.Name = "View";
            this.Size = new System.Drawing.Size(834, 778);
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Controls.Grid.V2.DataGridView _declarationsGrid;

    }
}
