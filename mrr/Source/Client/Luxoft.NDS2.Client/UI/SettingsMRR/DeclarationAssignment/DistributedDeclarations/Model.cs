﻿using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination;
using Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.DeclarationAssignment.DistributedDeclarations
{
    public class Model
    {
        # region Конструктор

        public Model()
        {
            DistributedDeclarations = new List<InspectorDeclarationModel>();
            Pager = new PagerStateViewModel();
        }

        # endregion

        # region События модели

        public ParameterlessEventHandler AfterSelectionChanged;

        # endregion

        # region Декларации

        public IEnumerable<InspectorDeclarationModel> DistributedDeclarations
        {
            get;
            private set;
        }

        public void SetNotDistributedDeclarations(
            IEnumerable<InspetorDeclarationDistribution> rawData)
        {
            var selectionChanged = AnyDeclarationSelected();
            var distributedDeclarations = rawData.Select(x => new InspectorDeclarationModel(x)).ToList();

            foreach (var declaration in distributedDeclarations)
                declaration.SelectedChanged += OnDeclarationSelectedChanged;
            DistributedDeclarations = distributedDeclarations.ToArray();

            if (selectionChanged && AfterSelectionChanged != null)
            {
                AfterSelectionChanged();
            }
        }

        private void OnDeclarationSelectedChanged()
        {
            if (AfterSelectionChanged != null)
            {
                AfterSelectionChanged();
            }
        }

        public PagerStateViewModel Pager
        {
            get;
            private set;
        }

        public bool AnyDeclarationSelected()
        {
            return DistributedDeclarations.Any(x => x.Selected);
        }

        public long[] SelectedDeclarationIdentifiers()
        {
            return DistributedDeclarations.Where(p => p.Selected).Select(x => x.GetDeclaration().Id).ToArray();
        }

        public bool IsSingleDeclarationSelected()
        {
            return DistributedDeclarations.Where(p => p.Selected).Count() == 1;
        }

        # endregion
    }
}
