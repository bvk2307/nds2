﻿using System;
using System.Drawing;
using System.Windows.Forms;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using CommonComponents.Uc.Infrastructure.Interface.UI;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Addins;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination;
using Microsoft.Practices.CompositeUI.Commands;
using Microsoft.Practices.ObjectBuilder;
using System.Collections.Generic;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Helpers;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.DeclarationAssignment.DistributedDeclarations
{
    public partial class View : BaseView
    {
        #region Создание презентера

        private Presenter _presenter;

        [CreateNew]
        public Presenter Presenter
        {
            set
            {
                _presenter = value;
                _presenter.PresentationContext = _presentationContext;
                _presenter.View = this;
                WorkItem = _presenter.WorkItem;
                _presenter.OnObjectCreated();
                InitGrid();
                AddEvents();
                InitializeRibbon();
            }
        }

        #endregion

        #region Конструктор

        public View(PresentationContext ctx)
            : base(ctx)
        {
            InitializeComponent();
        }
        public View()
        {
            InitializeComponent();
        }

        #endregion

        #region Публичные свойства

        public bool AllowViewTaxPayerDetails
        {
            get
            {
                return _taxPayerDetailsButton.Enabled;
            }
            set
            {
                this.ExecuteInUiThread(
                    v =>
                    {
                        _taxPayerDetailsButton.Enabled = value;
                        gridContextMenu.Items["openTaxPayerCard"].Enabled = value;
                        RefreshEkp();
                    });
            }
        }

        public bool AllowViewDeclarationDetails
        {
            get
            {
                return _declarationDetailsButton.Enabled;
            }
            set
            {
                this.ExecuteInUiThread(
                    v =>
                    {
                        _declarationDetailsButton.Enabled = value;
                        gridContextMenu.Items["openDeclarationMenu"].Enabled = value;
                        RefreshEkp();
                    });
            }
        }

        public bool AllowRefresh
        {
            get
            {
                return _refreshButton.Enabled;
            }
            set
            {
                this.ExecuteInUiThread(
                    v =>
                    {
                        _refreshButton.Enabled = value;
                        RefreshEkp();
                    });
            }
        }

        public bool AllowReAssignInspector
        {
            get
            {
                return _reAssignInspectorButton.Enabled;
            }
            set
            {
                this.ExecuteInUiThread(
                    v =>
                    {
                        _reAssignInspectorButton.Enabled = value;
                        RefreshEkp();
                    });
            }
        }

        public object ActiveDeclarationListItem
        {
            get
            {
                // Во избежание изменения ActiveRow в отдельном потоке сохраним на него ссылку
                var activeRow = _declarationsGrid.Grid.ActiveRow;

                return activeRow == null
                    ? null
                    : activeRow.ListObject;
            }
        }

        #endregion

        #region Публичные события

        public event EventHandler<GridInitEventArgs> GridInit;

        public event EventHandler<RowActionEventArgs<InspectorDeclarationModel>> InspectorChange;

        public event EventHandler SelectedInspectorsChange;

        public event EventHandler<RowActionEventArgs<InspectorDeclarationModel>> DeclarationOpen;

        public event EventHandler<RowActionEventArgs<InspectorDeclarationModel>> CardOpen;

        public event EventHandler RefreshData;

        public event ParameterlessEventHandler DeclarationListItemActivated;

        #endregion

        #region Риббон меню

        private UcRibbonButtonToolContext _declarationDetailsButton;

        private UcRibbonButtonToolContext _taxPayerDetailsButton;

        private UcRibbonButtonToolContext _reAssignInspectorButton;

        private UcRibbonButtonToolContext _refreshButton;

        private void InitializeRibbon()
        {
            IUcResourceManagersService resourceManagersService = _presenter.WorkItem.Services.Get<IUcResourceManagersService>(true);
            if (!resourceManagersService.Contains(ResourceManagerNDS2.NDS2ClientResources))
            {
                resourceManagersService.Add(ResourceManagerNDS2.NDS2ClientResources, Properties.Resources.ResourceManager);
            }

            UcRibbonTabContext tabNavigator = new UcRibbonTabContext(_presenter.PresentationContext, "NDS2Result")
            {
                Text = _presenter.PresentationContext.WindowTitle,
                ToolTipText = _presenter.PresentationContext.WindowTitle,
                Visible = true,
                Order = 1
            };

            UcRibbonGroupContext groupNavigation1 = tabNavigator.AddGroup("NDS2DeclarationListManage");
            groupNavigation1.Text = "Функции";
            groupNavigation1.Visible = true;

            _refreshButton = new UcRibbonButtonToolContext(_presenter.PresentationContext, "btnRefresh", "cmdRefresh")
            {
                Text = "Обновить",
                ToolTipText = "Обновить",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "update",
                SmallImageName = "update",
                Enabled = true
            };

            _reAssignInspectorButton = new UcRibbonButtonToolContext(_presenter.PresentationContext, "btnChangeInspector", "cmdChangeInspector")
            {
                Text = "Переназначить инспектора",
                ToolTipText = "Переназначить инспектора",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "switchuser_1333",
                SmallImageName = "switchuser_1333",
                Enabled = false
            };


            _declarationDetailsButton = new UcRibbonButtonToolContext(_presenter.PresentationContext, "btnOpenDecl ", "cmdOpenDecl")
            {
                Text = "Открыть декларацию",
                ToolTipText = "Открыть декларацию",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "view",
                SmallImageName = "view",
                Enabled = false
            };

            _taxPayerDetailsButton = new UcRibbonButtonToolContext(_presenter.PresentationContext, "btnOpenTPCard", "cmdOpenTPCard")
            {
                Text = "Открыть карточку НП",
                ToolTipText = "Открыть карточку НП",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "taxpayer",
                SmallImageName = "taxpayer",
                Enabled = false
            };

            groupNavigation1.ToolList.AddRange(
                new UcRibbonToolInstanceSettings[] { 
                    new UcRibbonToolInstanceSettings(_refreshButton.ItemName, UcRibbonToolSize.Large),
                    new UcRibbonToolInstanceSettings(_reAssignInspectorButton.ItemName, UcRibbonToolSize.Large),
                    new UcRibbonToolInstanceSettings(_taxPayerDetailsButton.ItemName, UcRibbonToolSize.Large),
                    new UcRibbonToolInstanceSettings(_declarationDetailsButton.ItemName, UcRibbonToolSize.Large),
                });

            _presentationContext.UiVisualizationCollection.AddRange(new VisualizationElementBase[]
            {
                _refreshButton,
                _reAssignInspectorButton,
                _declarationDetailsButton,
                _taxPayerDetailsButton,
                tabNavigator
            });

            _presentationContext.ActiveMenuTab = tabNavigator.ItemName;
        }

        [CommandHandler("cmdRefresh")]
        public virtual void RefreshtnClick(object sender, EventArgs e)
        {
            OnRefreshData();
        }

        [CommandHandler("cmdChangeInspector")]
        public virtual void InspectorChangeClick(object sender, EventArgs e)
        {
            OnSelectedInspectorsChange();
            _declarationsGrid.Refresh();
        }

        [CommandHandler("cmdOpenDecl")]
        public virtual void OpenDeclClick(object sender, EventArgs e)
        {
            var model = _declarationsGrid.Grid.ActiveRow.ListObject as InspectorDeclarationModel;
            OnOpenDeclaration(new RowActionEventArgs<InspectorDeclarationModel>(model));
        }

        [CommandHandler("cmdOpenTPCard")]
        public virtual void OpetTPCardClick(object sender, EventArgs e)
        {
            var model = _declarationsGrid.Grid.ActiveRow.ListObject as InspectorDeclarationModel;
            OnOpenCard(new RowActionEventArgs<InspectorDeclarationModel>(model));
        }

        #endregion

        private void InitGrid()
        {
            var clipIcons = new Dictionary<bool, Bitmap>
                            {
                                {false, null},
                                {true, Properties.Resources.paperclip2_black_5661}
                            };
            var enableIcons = new Dictionary<bool, Bitmap>
                            {
                                {false, Properties.Resources.disabled},
                                {true, null}
                            };

            var acessTooltips = new Dictionary<string, string>
                                                               {
                                                                   {false.ToString(), "У инспектора нет доступа"},
                                                                   {true.ToString(), ""}  
                                                               };

            var attachmentTooltips = new Dictionary<string, string>
                                                               {
                                                                   {false.ToString(), "НП подлежит автораспределению"},
                                                                   {true.ToString(), "НП не подлежит автораспределению"}  
                                                               };
            var pagerStateViewModel = new PagerStateViewModel();
            var pageNavigatorSource = new ExtendedDataGridPageNavigator(pagerStateViewModel);
            pageNavigatorSource.HidePageSizer();
            _declarationsGrid.WithPager(pageNavigatorSource);
            _declarationsGrid.RegisterTableAddin(new ExpansionIndicatorRemover());
            var setup = new GridColumnSetup();
            var helper = new ColumnHelper<InspectorDeclarationModel>(_declarationsGrid);
            _checkColumn = helper.CreateCheckColumnThreeWay(m => m.Selected, 2).Configure(c =>
            {
                c.DisableMoving = true;
                c.HeaderToolTip = "Флаг выбора записи";
                c.HeaderCheckedChange += GridHeaderCheckedChange;
            });
            setup.Columns.Add(_checkColumn);
            setup.Columns.Add(helper.CreateImageColumn(model => model.IsInspectorActive, this, enableIcons, 2).Configure(col =>
            {
                col.DisableSort = true;
                col.DisableFilter = true;
                col.DisableMoving = true;
                col.Width = 10;
                col.HeaderToolTip = "Наличие доступа";
                col.ToolTipViewer = new DictionaryToolTipViewer(acessTooltips);
            }));
            setup.Columns.Add(helper.CreateImageColumn(m => m.IsTaxPayerAttached, this, clipIcons, 2).Configure(c =>
            {
                c.DisableMoving = true;
                c.DisableFilter = true;
                c.ToolTipViewer = new DictionaryToolTipViewer(attachmentTooltips);
            }));

            var taxPayerGroup = helper.CreateGroup("Налогоплательщик", 2);
            taxPayerGroup.Columns.Add(helper.CreateTextColumn(m => m.Inn).Configure(c => c.DisableMoving = true));
            taxPayerGroup.Columns.Add(helper.CreateTextColumn(m => m.Kpp).Configure(c => c.DisableMoving = true));
            taxPayerGroup.Columns.Add(helper.CreateTextColumn(m => m.Name).Configure(c => c.DisableMoving = true));
            setup.Columns.Add(taxPayerGroup);
            setup.Columns.Add(helper.CreateTextColumn(m => m.Period, 2).Configure(c => c.DisableMoving = true));
            setup.Columns.Add(helper.CreateTextColumn(m => m.KNPStatus, 2).Configure(c => c.DisableMoving = true));
            setup.Columns.Add(helper.CreateTextColumn(m => m.DeclSign, 2).Configure(c => c.DisableMoving = true));
            setup.Columns.Add(helper.CreateTextColumn(m => m.CompensationAmount, 2).Configure(c => c.DisableMoving = true));
            setup.Columns.Add(helper.CreateTextColumn(m => m.ClaimAmount, 2).Configure(c => c.DisableMoving = true));

            var inspectorGroup = helper.CreateGroup("Инспектор", 2);
            inspectorGroup.Columns.Add(helper.CreateTextColumn(m => m.InspectorName).Configure(c => c.DisableMoving = true));
            inspectorGroup.Columns.Add(helper.CreateTextColumn(m => m.EmployeeNum).Configure(c => c.DisableMoving = true));
            setup.Columns.Add(inspectorGroup);

            _declarationsGrid.InitColumns(setup);

            _multiSelectedRow = new MultiSelectedRow(_declarationsGrid.Grid);
            _multiSelectedRow.SetupMultiSelectedRow();

            OnGridInit(new GridInitEventArgs(_declarationsGrid, pagerStateViewModel));
        }

        private CheckColumnThreeWay _checkColumn;

        private void GridHeaderCheckedChange(CheckState checkState)
        {
            var columnNameSelected = TypeHelper<InspectorDeclarationModel>.GetMemberName(t => t.Selected);
            bool storeInUndoCheck = false;
                   
            foreach (var itemRow in _declarationsGrid.Grid.Rows)
            {
                if (checkState == CheckState.Checked)
                    itemRow.Cells[columnNameSelected].SetValue(true, storeInUndoCheck);
                if (checkState == CheckState.Unchecked)
                    itemRow.Cells[columnNameSelected].SetValue(false, storeInUndoCheck);
                itemRow.Update();
            }
            SetCheckedRows();
            AllCheckedActionExtended(checkState);
            RaiseDeclarationListItemActivated();
        }

        private void AddEvents()
        {
            _declarationsGrid.Grid.AfterSelectChange += GridAfterSelectChange;
            _declarationsGrid.Grid.CellChange += GridCellChange;
            _declarationsGrid.Grid.AfterRowActivate += GridAfterRowActivate;
            _declarationsGrid.BeforeShowContextMenu += GridBeforeShowContextMenu;
        }

        #region События

        private void GridAfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            if (!_wasSelectedInConextMenu)
            {
                SetSelectedRows();
                GridCheckHeaderState();
            }
            else
                _wasSelectedInConextMenu = false;

            RaiseDeclarationListItemActivated();
        }

        private void GridCellChange(object sender, CellEventArgs e)
        {
            e.Cell.Row.Update();
            SetCheckedRows();
            RaiseDeclarationListItemActivated();
        }

        private void GridAfterRowActivate(object sender, EventArgs e)
        {
            RaiseDeclarationListItemActivated();
        }

        bool _wasSelectedInConextMenu = false;

        private void GridBeforeShowContextMenu(object sender, CommonComponents.Uc.Infrastructure.Interface.EventArgs<UltraGridRow> e)
        {
            _declarationsGrid.Grid.AfterSelectChange -= GridAfterSelectChange;
            _declarationsGrid.Grid.CellChange -= GridCellChange;

            _wasSelectedInConextMenu = _multiSelectedRow.ClearAllSelectedRowExceptOne(e.Data);
            _multiSelectedRow.SetSelectedRows(TypeHelper<InspectorDeclarationModel>.GetMemberName(t => t.Selected));

            _declarationsGrid.Grid.CellChange += GridCellChange;
            _declarationsGrid.Grid.AfterSelectChange += GridAfterSelectChange;
        }

        #endregion

        #region Гененрация событий

        private void OnRefreshData()
        {
            var tmp = RefreshData;
            if (tmp != null)
            {
                tmp(this, EventArgs.Empty);
            }
        }

        private void OnGridInit(GridInitEventArgs e)
        {
            var tmp = GridInit;
            if (tmp != null)
            {
                tmp(this, e);
            }
        }

        private void OnSelectedInspectorsChange()
        {
            var tmp = SelectedInspectorsChange;
            if (tmp != null)
            {
                tmp(this, EventArgs.Empty);
            }
        }

        private void OnInspectorChange(RowActionEventArgs<InspectorDeclarationModel> e)
        {
            var tmp = InspectorChange;
            if (tmp != null)
            {
                tmp(this, e);
            }
        }

        private void OnOpenDeclaration(RowActionEventArgs<InspectorDeclarationModel> e)
        {
            var tmp = DeclarationOpen;
            if (tmp != null)
            {
                tmp(this, e);
            }
        }

        private void OnOpenCard(RowActionEventArgs<InspectorDeclarationModel> e)
        {
            var tmp = CardOpen;
            if (tmp != null)
            {
                tmp(this, e);
            }
        }

        private void RaiseDeclarationListItemActivated()
        {
            if (DeclarationListItemActivated != null)
            {
                DeclarationListItemActivated();
            }
        }

        #endregion

        #region Контекстное меню

        private void changeInspectorMenu_Click(object sender, EventArgs e)
        {
            var model = _declarationsGrid.Grid.ActiveRow.ListObject as InspectorDeclarationModel;
            OnInspectorChange(new RowActionEventArgs<InspectorDeclarationModel>(model));
        }

        private void openDeclarationMenu_Click(object sender, EventArgs e)
        {
            var model = _declarationsGrid.Grid.ActiveRow.ListObject as InspectorDeclarationModel;
            OnOpenDeclaration(new RowActionEventArgs<InspectorDeclarationModel>(model));
        }

        private void openTaxPayerCard_Click(object sender, EventArgs e)
        {
            var model = _declarationsGrid.Grid.ActiveRow.ListObject as InspectorDeclarationModel;
            OnOpenCard(new RowActionEventArgs<InspectorDeclarationModel>(model));
        }

        #endregion

        # region MultiSelectedRow

        private MultiSelectedRow _multiSelectedRow = null;

        private void SetSelectedRows()
        {
            _declarationsGrid.Grid.CellChange -= GridCellChange;
            _declarationsGrid.Grid.AfterSelectChange -= GridAfterSelectChange;

            _multiSelectedRow.SetSelectedRows(TypeHelper<InspectorDeclarationModel>.GetMemberName(t => t.Selected));

            _declarationsGrid.Grid.AfterSelectChange += GridAfterSelectChange;
            _declarationsGrid.Grid.CellChange += GridCellChange;
        }

        private void SetCheckedRows()
        {
            _declarationsGrid.Grid.AfterSelectChange -= GridAfterSelectChange;

            _multiSelectedRow.SetCheckedRows(TypeHelper<InspectorDeclarationModel>.GetMemberName(t => t.Selected));

            _declarationsGrid.Grid.AfterSelectChange += GridAfterSelectChange;
        }

        private void AllCheckedActionExtended(CheckState checkState)
        {
            _declarationsGrid.Grid.AfterSelectChange -= GridAfterSelectChange;

            _multiSelectedRow.AllCheckedActionExtended(checkState);

            _declarationsGrid.Grid.AfterSelectChange += GridAfterSelectChange;
        }

        private void GridCheckHeaderState()
        {
            _checkColumn.HeaderCheckedChange -= GridHeaderCheckedChange;

            _multiSelectedRow.CheckHeaderState(TypeHelper<InspectorDeclarationModel>.GetMemberName(t => t.Selected));

            _checkColumn.HeaderCheckedChange += GridHeaderCheckedChange;
        }

        public void GridClearHeaderState()
        {
            _checkColumn.HeaderCheckedChange -= GridHeaderCheckedChange;

            _multiSelectedRow.ClearHeaderState(TypeHelper<InspectorDeclarationModel>.GetMemberName(t => t.Selected));

            _checkColumn.HeaderCheckedChange += GridHeaderCheckedChange;
        }

        # endregion
   }
}
