﻿using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment;
using System.ComponentModel;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.DeclarationAssignment.DistributedDeclarations
{
    public class InspectorDeclarationModel
    {
        # region Данные

        private readonly Declaration _declaration;
        private readonly InspectorSonoAssignment _inspector;

        #endregion

        # region Конструктор

        public InspectorDeclarationModel(InspetorDeclarationDistribution declarationDistribution)
        {
            _declaration = declarationDistribution.Declaration;
            _inspector = declarationDistribution.Inspector;
        }

        #endregion

        # region Свойства декларации (для списка)

        [DisplayName("")]
        [Description("Наличие доступа")]
        public bool IsInspectorActive
        {
            get
            {
                return _inspector.IsActive;
            }
        }

        [DisplayName("Табельный №")]
        [Description("Табельный номер инспектора")]
        public string EmployeeNum
        {
            get { return _inspector.EmployeeNum; }
        }

        [DisplayName("ФИО")]
        [Description("ФИО инспектора")]
        public string InspectorName
        {
            get { return _inspector.Name; }
        }

        [DisplayName("ИНН")]
        [Description("ИНН налогоплательщика")]
        public string Inn
        {
            get
            {
                return _declaration.Inn;
            }
        }

        [DisplayName("КПП")]
        [Description("КПП налогоплательщика")]
        public string Kpp
        {
            get { return _declaration.Kpp; }
        }

        [DisplayName("Наименование")]
        [Description("Наименование налогоплательщика")]
        public string Name
        {
            get
            {
                return _declaration.Name;
            }
        }

        [DisplayName("Отчетный период")]
        [Description("Отчетный период")]
        public string Period { get { return _declaration.Period; } }

        [DisplayName("Признак НД")]
        [Description("Признак декларации")]
        public string DeclSign { get { return _declaration.DeclSign; } }

        [DisplayName("Сумма НДС")]
        [Description("Сумма налога, исчисленная к \"+\" уплате/\"-\" возмещению в бюджет")]
        public decimal CompensationAmount { get { return _declaration.CompensationAmount; } }


        [DisplayName("Сумма расхождений по АТ")]
        [Description("Сумма расхождений по открытым автотребованиям по СФ")]
        public decimal? ClaimAmount { get { return _declaration.ClaimAmount; } }

        [DisplayName("Статус КНП")]
        [Description("Статус КНП")]
        public string KNPStatus
        {
            get { return _declaration.KNPStatus; }
        }

        [DisplayName("")]
        [Description("НП не подлежит автораспределению")]
        public bool IsTaxPayerAttached { get { return _declaration.IsTaxPayerAttached; } }

        private bool _selected;

        [DisplayName("")]
        [Description("Флаг выбора записи")]
        public bool Selected
        {
            get
            {
                return _selected;
            }
            set
            {
                var changed = _selected != value;
                _selected = value;

                if (changed && SelectedChanged != null)
                {
                    SelectedChanged();
                }
            }
        }

        #endregion

        #region Методы

        public Declaration GetDeclaration()
        {
            return _declaration;
        }

        public InspectorSonoAssignment GetInspector()
        {
            return _inspector;
        }

        #endregion

        # region События

        public ParameterlessEventHandler SelectedChanged;

        # endregion
    }
}
