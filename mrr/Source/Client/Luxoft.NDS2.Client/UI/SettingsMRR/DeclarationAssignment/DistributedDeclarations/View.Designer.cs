﻿namespace Luxoft.NDS2.Client.UI.SettingsMRR.DeclarationAssignment.DistributedDeclarations
{
    partial class View
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ultraPanel3 = new Infragistics.Win.Misc.UltraPanel();
            this._declarationsGrid = new Luxoft.NDS2.Client.UI.Controls.Grid.V2.DataGridView();
            this.gridContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.changeInspectorMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.openDeclarationMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.openTaxPayerCard = new System.Windows.Forms.ToolStripMenuItem();
            this.ultraPanel3.ClientArea.SuspendLayout();
            this.ultraPanel3.SuspendLayout();
            this.gridContextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraPanel3
            // 
            // 
            // ultraPanel3.ClientArea
            // 
            this.ultraPanel3.ClientArea.Controls.Add(this._declarationsGrid);
            this.ultraPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel3.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel3.Name = "ultraPanel3";
            this.ultraPanel3.Size = new System.Drawing.Size(834, 778);
            this.ultraPanel3.TabIndex = 0;
            // 
            // _declarationsGrid
            // 
            this._declarationsGrid.AggregatePanelVisible = true;
            this._declarationsGrid.AllowFilterReset = false;
            this._declarationsGrid.AllowMultiGrouping = true;
            this._declarationsGrid.AllowResetSettings = false;
            this._declarationsGrid.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this._declarationsGrid.BackColor = System.Drawing.Color.Transparent;
            this._declarationsGrid.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.None;
            this._declarationsGrid.ColumnVisibilitySetupButtonVisible = false;
            this._declarationsGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._declarationsGrid.ExportExcelCancelVisible = false;
            this._declarationsGrid.ExportExcelVisible = false;
            this._declarationsGrid.FilterResetVisible = false;
            this._declarationsGrid.FooterVisible = true;
            this._declarationsGrid.GridContextMenuStrip = this.gridContextMenu;
            this._declarationsGrid.Location = new System.Drawing.Point(0, 0);
            this._declarationsGrid.Name = "_declarationsGrid";
            this._declarationsGrid.PanelExportExcelStateVisible = false;
            this._declarationsGrid.PanelLoadingVisible = true;
            this._declarationsGrid.PanelPagesVisible = true;
            this._declarationsGrid.RowDoubleClicked = null;
            this._declarationsGrid.Size = new System.Drawing.Size(834, 778);
            this._declarationsGrid.TabIndex = 0;
            this._declarationsGrid.Title = "";
            // 
            // gridContextMenu
            // 
            this.gridContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.changeInspectorMenu,
            this.openDeclarationMenu,
            this.openTaxPayerCard});
            this.gridContextMenu.Name = "gridContextMenu";
            this.gridContextMenu.Size = new System.Drawing.Size(279, 92);
            // 
            // changeInspectorMenu
            // 
            this.changeInspectorMenu.Name = "changeInspectorMenu";
            this.changeInspectorMenu.Size = new System.Drawing.Size(278, 22);
            this.changeInspectorMenu.Text = "Переназначить инспектора";
            this.changeInspectorMenu.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.changeInspectorMenu.Click += new System.EventHandler(this.changeInspectorMenu_Click);
            // 
            // openDeclarationMenu
            // 
            this.openDeclarationMenu.Name = "openDeclarationMenu";
            this.openDeclarationMenu.Size = new System.Drawing.Size(278, 22);
            this.openDeclarationMenu.Text = "Открыть декларацию";
            this.openDeclarationMenu.Click += new System.EventHandler(this.openDeclarationMenu_Click);
            // 
            // openTaxPayerCard
            // 
            this.openTaxPayerCard.Name = "openTaxPayerCard";
            this.openTaxPayerCard.Size = new System.Drawing.Size(278, 22);
            this.openTaxPayerCard.Text = "Открыть карточку налогоплательщика";
            this.openTaxPayerCard.Click += new System.EventHandler(this.openTaxPayerCard_Click);
            // 
            // View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ultraPanel3);
            this.Name = "View";
            this.Size = new System.Drawing.Size(834, 778);
            this.ultraPanel3.ClientArea.ResumeLayout(false);
            this.ultraPanel3.ResumeLayout(false);
            this.gridContextMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraPanel ultraPanel3;
        private Controls.Grid.V2.DataGridView _declarationsGrid;
        private System.Windows.Forms.ContextMenuStrip gridContextMenu;
        private System.Windows.Forms.ToolStripMenuItem changeInspectorMenu;
        private System.Windows.Forms.ToolStripMenuItem openDeclarationMenu;
        private System.Windows.Forms.ToolStripMenuItem openTaxPayerCard;


    }
}
