﻿using System.Collections.Generic;
using System.Linq;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.Services.DeclarationAssignment;
using System;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.DeclarationAssignment.DistributedDeclarations
{
    public class Presenter : BasePresenter<View>
    {
        # region Модель

        private readonly Model _model = new Model();

        # endregion

        private IInspectorDeclarationService _inspectorDeclarationService;
        private PagedDataGridPresenter<InspectorDeclarationModel> _gridPresenter;

        public override void OnObjectCreated()
        {
            base.OnObjectCreated();
            View.GridInit += ViewGridInit;
            View.CardOpen += ViewCardOpen;
            View.DeclarationOpen += ViewDeclarationOpen;
            View.InspectorChange += ViewInspectorChange;
            View.SelectedInspectorsChange += SelectedInspectorsChange;
            View.RefreshData += ViewRefreshData;
            View.DeclarationListItemActivated += OnDeclarationActivated;
            _model.AfterSelectionChanged += OnAfterSelectionChanged;
            _inspectorDeclarationService = GetServiceProxy<IInspectorDeclarationService>();
        }

        private void SelectedInspectorsChange(object sender, System.EventArgs e)
        {
            ChangeInspectorOnDeclaration();
        }

        private void ViewInspectorChange(object sender, RowActionEventArgs<InspectorDeclarationModel> e)
        {
            ChangeInspectorOnDeclaration();
        }

        private void ChangeInspectorOnDeclaration()
        {
            var selectedDeclarations = _gridPresenter.Rows.Where(d => d.Selected).ToArray();
            var navigation = new Navigation(PresentationContext, WorkItem, View);
            if (selectedDeclarations.Length > 0)
            {
                var ids = selectedDeclarations.Select(d => d.GetDeclaration().Id).ToArray();
                int countGroupByInspector = selectedDeclarations.GroupBy(p=>p.GetInspector().SID).Count();
                if (ids.Count() == 1 || countGroupByInspector == 1)
                {
                    if (navigation.ShowTransferDeclarationsDialogWithCurrent(ids, selectedDeclarations.First().GetInspector().SID))
                    {
                        _gridPresenter.Load();
                        View.GridClearHeaderState();
                    }
                }
                else
                {
                    if (navigation.ShowTransferDeclarationsDialogWithDefault(ids, String.Empty))
                    {
                        _gridPresenter.Load();
                        View.GridClearHeaderState();
                    }
                }
            }
        }

        private void ViewDeclarationOpen(object sender, RowActionEventArgs<InspectorDeclarationModel> e)
        {
            InspectorDeclarationModel decl = e.Model;
            var opener = GetDeclarationCardOpener();
            var declId = decl.GetDeclaration().DeclarationVersionId;
            var cardOpenResult = opener.Open(declId.GetValueOrDefault());
            if (!cardOpenResult.IsSuccess)
            {
                View.ShowError(cardOpenResult.Message);
            }
        }

        private void ViewCardOpen(object sender, RowActionEventArgs<InspectorDeclarationModel> e)
        {
            InspectorDeclarationModel decl = e.Model;
            ViewTaxPayerByKppOriginal(decl.Inn, decl.Kpp);
        }

        private void ViewGridInit(object sender, GridInitEventArgs e)
        {
            var conditions = new QueryConditions
            {
                Sorting = new List<ColumnSort> 
                { 
                    new ColumnSort
                    {
                        ColumnKey = TypeHelper<InspectorDeclarationModel>.GetMemberName(t => t.IsTaxPayerAttached), 
                        Order = ColumnSort.SortOrder.Desc
                    },
                    new ColumnSort
                    {
                        ColumnKey = TypeHelper<InspectorDeclarationModel>.GetMemberName(t => t.Period), 
                        Order = ColumnSort.SortOrder.Asc
                    },
                    new ColumnSort
                    {
                        ColumnKey = TypeHelper<InspectorDeclarationModel>.GetMemberName(t => t.CompensationAmount), 
                        Order = ColumnSort.SortOrder.Asc
                    } 
                }
            };

            var gridOptions = new GridOptions { AllowManageSettings = true, AllowManageFilterSettings = true };

            gridOptions.DefaultFilter.ApplyFilter(TypeHelper<InspectorDeclarationModel>.GetMemberName(t => t.Period), new FilterCondition(FilterComparisionOperator.Equals, "Предыдущий"));
            gridOptions.DefaultFilter.ApplyFilter(TypeHelper<InspectorDeclarationModel>.GetMemberName(t => t.KNPStatus), new FilterCondition(FilterComparisionOperator.Equals, "Открыта"));

            _gridPresenter = new PagedDataGridPresenter<InspectorDeclarationModel>(e.Grid, e.Pager, GetSourceData, conditions);
            _gridPresenter.Init(SettingsProvider(string.Format("{0}_InitializeGrid", GetType())), gridOptions);

            _gridPresenter.Load();
        }



        private PageResult<InspectorDeclarationModel> GetSourceData(QueryConditions searchCriteria)
        {
            var pageResult = new PageResult<InspetorDeclarationDistribution>();

            ExecuteServiceCall(() =>
            {
                return _inspectorDeclarationService
                    .GetInspectorDeclationDistribution(searchCriteria);
            },
            result =>
            {
                pageResult = result.Result;
            });

            _model.SetNotDistributedDeclarations(pageResult.Rows);
            View.AllowRefresh = true;

            return new PageResult<InspectorDeclarationModel>(
                _model.DistributedDeclarations.ToList(),
                pageResult.TotalMatches,
                pageResult.TotalAvailable);

        }

        # region Обработка событий модели

        private void OnAfterSelectionChanged()
        {
            View.AllowReAssignInspector = _model.AnyDeclarationSelected();

            AllowViewOpenDetails();
        }

        #endregion

        # region Обработка событий представления

        /// <summary>
        /// Сбрасывает доступность функций в дефолтное состояние и загружает список деклараций
        /// </summary>
        private void ViewRefreshData(object sender, System.EventArgs e)
        {
            View.AllowReAssignInspector = false;
            View.AllowRefresh = false;
            View.AllowViewDeclarationDetails = false;
            View.AllowViewTaxPayerDetails = false;
            _gridPresenter.Load();
        }

        /// <summary>
        /// Обновляет доступность функций просмотра карточке НП и декларации
        /// </summary>
        private void OnDeclarationActivated()
        {
            AllowViewOpenDetails();
        }

        private void AllowViewOpenDetails()
        {
            bool allowOpenDetails = _model.IsSingleDeclarationSelected();

            View.AllowViewDeclarationDetails = allowOpenDetails;
            View.AllowViewTaxPayerDetails = allowOpenDetails;
        }

        # endregion
    }
}
