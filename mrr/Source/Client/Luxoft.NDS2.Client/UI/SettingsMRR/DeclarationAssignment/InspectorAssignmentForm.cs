﻿using System.Collections.Generic;
using System.Reflection;
using System.Windows.Forms;
using System.ComponentModel;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.DeclarationAssignment
{
    public partial class InspectorAssignmentForm : Form
    {
        IList<InspectorStatisticsModel> _dataSource;

        public InspectorAssignmentForm(IList<InspectorStatisticsModel> dataSource)
        {
            InitializeComponent();

            _dataSource = dataSource;
            ucInspectors.DisplayMember = "DisplayField";
            ucInspectors.ValueMember = "SID";
            ucInspectors.DataSource = dataSource;
        }

        InspectorStatisticsModel _oldSelectedInspector;
        public void SetCurrentInspector(string inspectorSid)
        {
            _oldSelectedInspector = _dataSource.FirstOrDefault(i => i.SID == inspectorSid);
            if (_oldSelectedInspector != null)
            {
                ucInspectors.Value = _oldSelectedInspector.SID;
            }
        }

        InspectorStatisticsModel _defaultInspector;
        public void SetDefaultInspector(string inspectorSid)
        {
            _defaultInspector = _dataSource.FirstOrDefault(i => i.SID == inspectorSid);
            if (_defaultInspector != null)
            {
                ucInspectors.Value = _defaultInspector.SID;
            }
        }

        public string GetInspectorSid()
        {
            return ucInspectors.Value != null ? (string)ucInspectors.Value : null;
        }

        private void btOk_Click(object sender, System.EventArgs e)
        {
            var newInspector = ucInspectors.SelectedRow != null && ucInspectors.SelectedRow.ListObject != null ? ucInspectors.SelectedRow.ListObject as InspectorStatisticsModel : null;
            bool isResultOk = false;
            if (_oldSelectedInspector != null)
            {
                isResultOk = (newInspector != null && newInspector != _oldSelectedInspector);
            }
            else
            {
                isResultOk = (newInspector != null);
            }

            if (isResultOk)
            {
                DialogResult = DialogResult.OK;
            }
            else
            {
                DialogResult = DialogResult.Cancel;
            }
        }

        private void AllowButtonSave()
        {
            var selectedInspector = ucInspectors.SelectedRow != null && ucInspectors.SelectedRow.ListObject != null ? ucInspectors.SelectedRow.ListObject as InspectorStatisticsModel : null;
            btOk.Enabled = selectedInspector != null;
        }

        private void ucInspectors_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            var propertyInfos = typeof(InspectorStatisticsModel).GetProperties().ToDictionary(p => p.Name);
            foreach (var column in e.Layout.Bands[0].Columns)
            {
                PropertyInfo propertyInfo;
                if (propertyInfos.TryGetValue(column.Key, out propertyInfo))
                {
                    var descriptionAttr = propertyInfo.GetCustomAttributes(typeof(DescriptionAttribute), false).SingleOrDefault();
                    if(descriptionAttr != null)
                    {
                        column.Header.ToolTipText = ((DescriptionAttribute) descriptionAttr).Description;
                    }
                }
            }
        }

        private void InspectorAssignmentForm_Load(object sender, System.EventArgs e)
        {
            AllowButtonSave();
        }

        private void ucInspectors_ValueChanged(object sender, System.EventArgs e)
        {
            AllowButtonSave();
        }
    }
}
