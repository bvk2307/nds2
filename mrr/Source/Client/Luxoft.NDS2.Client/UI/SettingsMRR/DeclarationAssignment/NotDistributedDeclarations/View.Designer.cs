﻿namespace Luxoft.NDS2.Client.UI.SettingsMRR.DeclarationAssignment.NotDistributedDeclarations
{
    partial class View
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gridDeclaration = new Luxoft.NDS2.Client.UI.Controls.Grid.V2.DataGridView();
            this.gridContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.setInspectorMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.openDeclarationMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.openTaxPayerCard = new System.Windows.Forms.ToolStripMenuItem();
            this.gridContextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridDeclaration
            // 
            this.gridDeclaration.AggregatePanelVisible = true;
            this.gridDeclaration.AllowFilterReset = false;
            this.gridDeclaration.AllowMultiGrouping = true;
            this.gridDeclaration.AllowResetSettings = false;
            this.gridDeclaration.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.gridDeclaration.BackColor = System.Drawing.Color.Transparent;
            this.gridDeclaration.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.None;
            this.gridDeclaration.ColumnVisibilitySetupButtonVisible = false;
            this.gridDeclaration.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridDeclaration.ExportExcelCancelVisible = false;
            this.gridDeclaration.ExportExcelVisible = false;
            this.gridDeclaration.FilterResetVisible = false;
            this.gridDeclaration.FooterVisible = true;
            this.gridDeclaration.GridContextMenuStrip = this.gridContextMenu;
            this.gridDeclaration.Location = new System.Drawing.Point(0, 0);
            this.gridDeclaration.Name = "gridDeclaration";
            this.gridDeclaration.PanelExportExcelStateVisible = false;
            this.gridDeclaration.PanelLoadingVisible = true;
            this.gridDeclaration.PanelPagesVisible = true;
            this.gridDeclaration.RowDoubleClicked = null;
            this.gridDeclaration.Size = new System.Drawing.Size(848, 318);
            this.gridDeclaration.TabIndex = 0;
            this.gridDeclaration.Title = "";
            // 
            // gridContextMenu
            // 
            this.gridContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.setInspectorMenu,
            this.openDeclarationMenu,
            this.openTaxPayerCard});
            this.gridContextMenu.Name = "gridContextMenu";
            this.gridContextMenu.Size = new System.Drawing.Size(279, 70);
            // 
            // setInspectorMenu
            // 
            this.setInspectorMenu.Name = "setInspectorMenu";
            this.setInspectorMenu.Size = new System.Drawing.Size(278, 22);
            this.setInspectorMenu.Text = "Назначить инспектора";
            this.setInspectorMenu.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.setInspectorMenu.Click += new System.EventHandler(this.SetInspectorMenu_Click);
            // 
            // openDeclarationMenu
            // 
            this.openDeclarationMenu.Name = "openDeclarationMenu";
            this.openDeclarationMenu.Size = new System.Drawing.Size(278, 22);
            this.openDeclarationMenu.Text = "Открыть декларацию";
            this.openDeclarationMenu.Click += new System.EventHandler(this.OpenDeclarationMenu_Click);
            // 
            // openTaxPayerCard
            // 
            this.openTaxPayerCard.Name = "openTaxPayerCard";
            this.openTaxPayerCard.Size = new System.Drawing.Size(278, 22);
            this.openTaxPayerCard.Text = "Открыть карточку налогоплательщика";
            this.openTaxPayerCard.Click += new System.EventHandler(this.OpenTaxPayerCard_Click);
            // 
            // View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridDeclaration);
            this.Name = "View";
            this.Size = new System.Drawing.Size(848, 318);
            this.gridContextMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Controls.Grid.V2.DataGridView gridDeclaration;
        private System.Windows.Forms.ContextMenuStrip gridContextMenu;
        private System.Windows.Forms.ToolStripMenuItem setInspectorMenu;
        private System.Windows.Forms.ToolStripMenuItem openDeclarationMenu;
        private System.Windows.Forms.ToolStripMenuItem openTaxPayerCard;
    }
}
