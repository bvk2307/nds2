﻿using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Base.ServiceAsync;
using Luxoft.NDS2.Common.Contracts.Services.DeclarationAssignment;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.DeclarationAssignment.NotDistributedDeclarations
{
    public class PresenterCreator : BasePresenter<View>
    {
        public void Init()
        {
            new Presenter(
                View,
                SettingsProvider(string.Format("{0}_InitializeGrid", View.GetType())),
                GetServiceProxy<IInspectorDeclarationService>(),
                this,
                GetTaxPayerDetailsViewer(),
                GetDeclarationCardOpener(),
                new Navigation(PresentationContext, WorkItem, View)
            );
        }
    }
}
