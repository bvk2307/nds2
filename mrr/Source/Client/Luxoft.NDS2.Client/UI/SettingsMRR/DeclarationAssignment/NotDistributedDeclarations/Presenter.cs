﻿using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Base.Navigation;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Common.Contracts.Services.DeclarationAssignment;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.DeclarationAssignment.NotDistributedDeclarations
{
    public class Presenter
    {
        # region Модель

        private readonly Model _model = new Model();

        # endregion

        # region Представление

        private readonly INotDistributedDeclarationsView _view;

        # endregion

        # region Дочерний презентер (списка деклараций)

        private PagedDataGridPresenter<DeclarationModel> _gridPresenter;

        # endregion

        # region Сервисы

        private readonly IInspectorDeclarationService _inspectorDeclarationService;

        # endregion

        # region Инфраструктура

        private readonly ISettingsProvider _settingsProvider;

        private readonly IServiceCallWrapper _serviceRequestWrapper;

        # endregion

        # region Конструктор

        public Presenter(
            INotDistributedDeclarationsView view,
            ISettingsProvider settingsProvider,
            IInspectorDeclarationService inspectorDeclarationService,
            IServiceCallWrapper asyncExecutor,
            ITaxPayerDetails taxPayerDetailsViewer,
            IDeclarationCardOpener declarationCardOpener,
            IInspectorAssignment inspectorAssignment)
        {
            _view = view;
            _inspectorDeclarationService = inspectorDeclarationService;
            _serviceRequestWrapper = asyncExecutor;
            _taxPayerDetailsViewer = taxPayerDetailsViewer;
            _declarationCardOpener = declarationCardOpener;
            _inspectorAssignment = inspectorAssignment;
            _settingsProvider = settingsProvider;

            InitGrid();
            BindViewEvents();
            BindModelEvents();
        }

        private void BindViewEvents()
        {
            _view.Loaded += Refresh;
            _view.OnRefresh += Refresh;
            _view.DeclarationListItemActivated += OnDeclarationActivated;
            _view.InspectorSet += ViewInspectorSet;
            _view.ActiveDeclarationInspectorSet += ViewActiveDeclarationInspectorSet;
            _view.TaxPayerOpen += ViewTaxPayerOpen;
            _view.DeclarationOpen += ViewDeclarationOpen;
            _view.DeclarationCheckSelected += ViewDeclarationCheckSelected;
        }

        private void BindModelEvents()
        {
            _model.AfterSelectionChanged += OnAfterSelectionChanged;
        }

        private void InitGrid()
        {
            var pageNavigatorSource = new ExtendedDataGridPageNavigator(_model.Pager);
            _view.InitGrid(pageNavigatorSource);

            var conditions = new QueryConditions
            {
                Sorting = new List<ColumnSort> 
                { 
                    new ColumnSort
                    {
                        ColumnKey = TypeHelper<DeclarationModel>.GetMemberName(t => t.Inn), 
                        Order = ColumnSort.SortOrder.Asc
                    } 
                }
            };
            _gridPresenter =
                new PagedDataGridPresenter<DeclarationModel>(
                    _view.DataGridView,
                    _model.Pager,
                    GetData,
                    conditions);

            _gridPresenter.Init(_settingsProvider);
            _gridPresenter.Load();
        }

        # endregion

        # region Обработка событий представления

        /// <summary>
        /// Сбрасывает доступность функций в дефолтное состояние и загружает список деклараций
        /// </summary>
        private void Refresh()
        {
            _view.AllowAssignInspector = false;
            _view.AllowRefresh = false;
            _view.AllowViewDeclarationDetails = false;
            _view.AllowViewTaxPayerDetails = false;
            _gridPresenter.Load();
        }

        /// <summary>
        /// Обновляет доступность функций просмотра карточке НП и декларации
        /// </summary>
        private void OnDeclarationActivated()
        {
            AllowViewOpenDetails();
        }

        /// <summary>
        /// Обновляет доступность функции назначения инспектора
        /// </summary>
        private void ViewDeclarationCheckSelected()
        {
            _view.AllowAssignInspector = _model.AnyDeclarationSelected();

            AllowViewOpenDetails();
        }

        private void AllowViewOpenDetails()
        {
            bool allowOpenDetails = _model.IsSingleDeclarationSelected();

            _view.AllowViewDeclarationDetails = allowOpenDetails;
            _view.AllowViewTaxPayerDetails = allowOpenDetails;
        }

        # endregion

        # region Обработка событий модели

        private void OnAfterSelectionChanged()
        {
            _view.AllowAssignInspector = _model.AnyDeclarationSelected();
        }

        # endregion    
    
        # region Загрузка данных

        private PageResult<DeclarationModel> GetData(QueryConditions searchCriteria)
        {
            var pageResult = new PageResult<InspectorDeclarationNotDistribution>();

            _serviceRequestWrapper.ExecuteServiceCall(() =>
            {
                return _inspectorDeclarationService
                    .GetInspectorDeclationNotDistribution(searchCriteria);
            }, 
            result => 
            { 
                pageResult = result.Result; 
            });

            _model.SetNotDistributedDeclarations(pageResult.Rows);
            _view.AllowRefresh = true;

            return new PageResult<DeclarationModel>(
                _model.NotDistributedDeclarations.ToList(), 
                pageResult.TotalMatches, 
                pageResult.TotalAvailable);
        }

        # endregion

        # region Навигация к другим экранным формам

        private readonly ITaxPayerDetails _taxPayerDetailsViewer;

        private readonly IDeclarationCardOpener _declarationCardOpener;

        private IInspectorAssignment _inspectorAssignment;

        /// <summary>
        /// Открывает диалог назначения выделенных чекбоксом деклараций на инспектора
        /// </summary>
        private void ViewInspectorSet()
        {
            if (!_model.AnyDeclarationSelected())
            {
                _view.ShowNotification("Выберите одну или несколько деклараций");
                return;
            }

            if (_model.IsSingleDeclarationSelected())
            {
                SetInspectorSingleDeclaration(_model.SingleDeclarationSelected());
            }
            else
            {
                _view.AllowAssignInspector = false;
                if (_inspectorAssignment.ShowTransferDeclarationsDialogWithDefault(
                    _model.SelectedDeclarationIdentifiers(),
                    String.Empty))
                {
                    Refresh();
                    _view.GridClearHeaderState();
                }
                else
                {
                    _view.AllowAssignInspector = _model.AnyDeclarationSelected();
                }
            }
        }

        /// <summary>
        /// Открывает диалог назначения активной декларации на инспектора
        /// </summary>
        private void ViewActiveDeclarationInspectorSet()
        {
            ViewInspectorSet();
        }

        /// <summary>
        /// Открывает диалог назначения одиночной декларации на инспектора
        /// </summary>
        private void SetInspectorSingleDeclaration(DeclarationModel declarationModel)
        {
            _view.AllowAssignInspector = false;
            if (_inspectorAssignment.ShowTransferDeclarationsDialogWithDefault(
                new long[] { declarationModel.GetDeclarationId() },
                declarationModel.InspectorSid()))
            {
                Refresh();
                _view.GridClearHeaderState();
            }
            else
            {
                _view.AllowAssignInspector = _model.AnyDeclarationSelected();
            }
        }

        /// <summary>
        /// Открывает карточку декларации
        /// </summary>
        private void ViewDeclarationOpen()
        {
            var activeDeclaration = _view.ActiveDeclarationListItem as DeclarationModel;

            if (activeDeclaration == null)
            {
                _view.ShowNotification("Выберите декларацию в списке");
                return;
            }

            var tabOpenResult = 
                _declarationCardOpener.Open(activeDeclaration.DeclarationZip().Value);

            if (!tabOpenResult.IsSuccess)
            {
                _view.ShowError(tabOpenResult.Message);
            }
        }

        /// <summary>
        /// Открывает карточку НП
        /// </summary>
        private void ViewTaxPayerOpen()
        {
            var activeDeclaration = _view.ActiveDeclarationListItem as DeclarationModel;

            if (activeDeclaration == null)
            {
                _view.ShowNotification("Выберите декларацию в списке");
                return;
            }

            var tabOpenResult =
                _taxPayerDetailsViewer.ViewByKppOriginal(
                    activeDeclaration.Inn,
                    activeDeclaration.Kpp);

            if (!tabOpenResult.IsSuccess)
            {
                _view.ShowError(tabOpenResult.Message);
            }
        }

        # endregion

    }
}
