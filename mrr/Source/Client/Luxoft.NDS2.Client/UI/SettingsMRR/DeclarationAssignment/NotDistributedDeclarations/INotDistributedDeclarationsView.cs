﻿using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination;
using System;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.DeclarationAssignment.NotDistributedDeclarations
{
    /// <summary>
    /// Интерфейс ЭФ нераспределенные декларации
    /// </summary>
    public interface INotDistributedDeclarationsView : IViewBase
    {
        /// <summary>
        /// Возвращает ссылку на представление списка деклараций
        /// </summary>
        IDataGridView DataGridView 
        { 
            get; 
        }

        /// <summary>
        /// Инициализирует список деклараций
        /// </summary>
        /// <param name="pageNavigatorSource">Паджинатор списка деклараций</param>
        void InitGrid(ExtendedDataGridPageNavigator pageNavigatorSource);

        /// <summary>
        /// Возвращает или задает доступность функции просмотра карточки НП
        /// </summary>
        bool AllowViewTaxPayerDetails 
        { 
            get; 
            set; 
        }

        /// <summary>
        /// Возвращает или задает доступность функции просмотра карточки декларации
        /// </summary>
        bool AllowViewDeclarationDetails 
        { 
            get; 
            set; 
        }

        /// <summary>
        /// Возвращает или задает доступность функции назначения инспектора
        /// </summary>
        bool AllowAssignInspector 
        { 
            get; 
            set; 
        }

        /// <summary>
        /// Возвращает или задает доступность функции обновления списка
        /// </summary>
        bool AllowRefresh
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает ссылку на активный элемент списка деклараций
        /// </summary>
        object ActiveDeclarationListItem 
        { 
            get; 
        }

        event ParameterlessEventHandler Loaded;

        event ParameterlessEventHandler OnRefresh;

        event ParameterlessEventHandler DeclarationListItemActivated;

        event ParameterlessEventHandler DeclarationOpen;

        event ParameterlessEventHandler TaxPayerOpen;

        event ParameterlessEventHandler InspectorSet;

        event ParameterlessEventHandler ActiveDeclarationInspectorSet;

        event ParameterlessEventHandler DeclarationCheckSelected;

        void GridClearHeaderState();
    }
}
