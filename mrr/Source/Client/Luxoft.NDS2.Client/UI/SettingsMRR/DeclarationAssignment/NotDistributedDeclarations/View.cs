﻿using System;
using System.Windows.Forms;
using Luxoft.NDS2.Client.UI.Base;
using Microsoft.Practices.ObjectBuilder;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using CommonComponents.Uc.Infrastructure.Interface.UI;
using Microsoft.Practices.CompositeUI.Commands;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Addins;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2;
using System.Collections.Generic;
using Infragistics.Win.UltraWinGrid;
using System.Drawing;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.ColumnExpressions;
using Infragistics.Win;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Helpers;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.DeclarationAssignment.NotDistributedDeclarations
{
    public partial class View : BaseView, INotDistributedDeclarationsView
    {
        # region Создание экземпляра презентера

        [CreateNew]
        public PresenterCreator Presenter
        {
            set
            {
                PresenterCreator _presenterCreator = value;
                _presenterCreator.PresentationContext = _presentationContext;
                _presenterCreator.View = this;
                WorkItem = _presenterCreator.WorkItem;
                InitializeRibbon();
                _presenterCreator.Init();
            }
        }

        # endregion        

        #region Конструкторы

        public View(PresentationContext ctx)
            : base(ctx)
        {
            InitializeComponent();
        }

        public View()
        {
            InitializeComponent();
        }

        #endregion

        #region Реализация интерфейса

        public IDataGridView DataGridView
        {
            get { return gridDeclaration; }
        }

        public void InitGrid(ExtendedDataGridPageNavigator pageNavigatorSource)
        {
            pageNavigatorSource.HidePageSizer();
            gridDeclaration.WithPager(pageNavigatorSource);
            gridDeclaration.RegisterTableAddin(new ExpansionIndicatorRemover());
            gridDeclaration.InitColumns(CreateSetupGrid());

            _multiSelectedRow = new MultiSelectedRow(gridDeclaration.Grid);
            _multiSelectedRow.SetupMultiSelectedRow();

            gridDeclaration.Grid.AfterSelectChange += GridAfterSelectChange;
            gridDeclaration.Grid.AfterRowActivate += GridAfterRowActivate;
            gridDeclaration.Grid.CellChange += GridCellChange;
            gridDeclaration.BeforeShowContextMenu += GridBeforeShowContextMenu;
        }

        public bool AllowViewTaxPayerDetails
        {
            get
            {
                return _taxPayerDetailsButton.Enabled;
            }
            set
            {
                this.ExecuteInUiThread(
                    v =>
                    {
                        _taxPayerDetailsButton.Enabled = value;
                        gridContextMenu.Items["openTaxPayerCard"].Enabled = value;
                        RefreshEkp();
                    });
            }
        }

        public bool AllowViewDeclarationDetails
        {
            get
            {
                return _declarationDetailsButton.Enabled;
            }
            set
            {
                this.ExecuteInUiThread(
                    v =>
                    {
                        _declarationDetailsButton.Enabled = value;
                        gridContextMenu.Items["openDeclarationMenu"].Enabled = value;
                        RefreshEkp();
                    });
            }
        }

        public bool AllowRefresh
        {
            get
            {
                return _refreshButton.Enabled;
            }
            set
            {
                this.ExecuteInUiThread(
                    v =>
                    {
                        _refreshButton.Enabled = value;
                        RefreshEkp();
                    });
            }
        }

        public bool AllowAssignInspector
        {
            get
            {
                return _assignInspectorButton.Enabled;
            }
            set
            {
                this.ExecuteInUiThread(
                    v =>
                    {
                        _assignInspectorButton.Enabled = value;
                        RefreshEkp();
                    });
            }
        }

        public object ActiveDeclarationListItem
        {
            get
            {
                // Во избежание изменения ActiveRow в отдельном потоке сохраним на него ссылку
                var activeRow = gridDeclaration.Grid.ActiveRow;

                return activeRow == null
                    ? null
                    : activeRow.ListObject;
            }
        }

        public event ParameterlessEventHandler Loaded;

        public event ParameterlessEventHandler OnRefresh;

        public event ParameterlessEventHandler DeclarationListItemActivated;

        public event ParameterlessEventHandler DeclarationOpen;

        public event ParameterlessEventHandler TaxPayerOpen;

        public event ParameterlessEventHandler InspectorSet;

        public event ParameterlessEventHandler ActiveDeclarationInspectorSet;

        public event ParameterlessEventHandler DeclarationCheckSelected;

        #endregion

        #region Риббон меню

        private UcRibbonButtonToolContext _declarationDetailsButton;

        private UcRibbonButtonToolContext _taxPayerDetailsButton;

        private UcRibbonButtonToolContext _assignInspectorButton;

        private UcRibbonButtonToolContext _refreshButton;

        private void InitializeRibbon()
        {
            var resourceManagersService = WorkItem.Services.Get<IUcResourceManagersService>(true);

            if (!resourceManagersService.Contains(ResourceManagerNDS2.NDS2ClientResources))
            {
                resourceManagersService.Add(
                    ResourceManagerNDS2.NDS2ClientResources,
                    Properties.Resources.ResourceManager);
            }

            var tabNavigator = new UcRibbonTabContext(_presentationContext, "NDS2Result")
            {
                Text = _presentationContext.WindowTitle,
                ToolTipText = _presentationContext.WindowTitle,
                Visible = true,
                Order = 1
            };

            var groupNavigation = tabNavigator.AddGroup("NDS2DeclarationListManage");
            groupNavigation.Text = "Функции";
            groupNavigation.Visible = true;

            _refreshButton =
                new UcRibbonButtonToolContext(_presentationContext, "btnRefresh", "cmdRefresh")
                {
                    Text = "Обновить",
                    ToolTipText = "Обновить",
                    ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                    LargeImageName = "update",
                    SmallImageName = "update",
                    Enabled = true
                };

            _assignInspectorButton =
                new UcRibbonButtonToolContext(_presentationContext, "btnSetInspector", "cmdSetInspector")
                {
                    Text = "Назначить инспектора",
                    ToolTipText = "Назначить инспектора",
                    ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                    LargeImageName = "set_decl_inspector",
                    SmallImageName = "set_decl_inspector",
                    Enabled = false
                };

            _declarationDetailsButton = 
                new UcRibbonButtonToolContext(_presentationContext, "btnOpenDecl ", "cmdOpenDecl")
                {
                    Text = "Открыть декларацию",
                    ToolTipText = "Открыть декларацию",
                    ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                    LargeImageName = "view",
                    SmallImageName = "view",
                    Enabled = false
                };

            _taxPayerDetailsButton =
                new UcRibbonButtonToolContext(_presentationContext, "btnOpenTPCard", "cmdOpenTPCard")
                {
                    Text = "Открыть карточку НП",
                    ToolTipText = "Открыть карточку НП",
                    ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                    LargeImageName = "taxpayer",
                    SmallImageName = "taxpayer",
                    Enabled = false
                };

            groupNavigation.ToolList.AddRange(
                new UcRibbonToolInstanceSettings[] { 
                    new UcRibbonToolInstanceSettings(
                        _refreshButton.ItemName, 
                        UcRibbonToolSize.Large),
                    new UcRibbonToolInstanceSettings(
                        _assignInspectorButton.ItemName, 
                        UcRibbonToolSize.Large),
                    new UcRibbonToolInstanceSettings(
                        _declarationDetailsButton.ItemName, 
                        UcRibbonToolSize.Large),
                    new UcRibbonToolInstanceSettings(
                        _taxPayerDetailsButton.ItemName, 
                        UcRibbonToolSize.Large),
                });

            _presentationContext.UiVisualizationCollection.AddRange(
                new VisualizationElementBase[]
                {
                    _refreshButton,
                    _assignInspectorButton,
                    _declarationDetailsButton,
                    _taxPayerDetailsButton,
                    tabNavigator
                });

            _presentationContext.ActiveMenuTab = tabNavigator.ItemName;
        }

        [CommandHandler("cmdRefresh")]
        public virtual void RefreshtnClick(object sender, EventArgs e)
        {
            if (OnRefresh != null)
            {
                OnRefresh();
            }
        }

        [CommandHandler("cmdSetInspector")]
        public virtual void InspectorSetClick(object sender, EventArgs e)
        {
            OnInspectorSet();
        }

        [CommandHandler("cmdOpenDecl")]
        public virtual void OpenDeclClick(object sender, EventArgs e)
        {
            OnOpenDeclaration();
        }

        [CommandHandler("cmdOpenTPCard")]
        public virtual void OpetTPCardClick(object sender, EventArgs e)
        {
            OnOpenTaxPayer();
        }

        #endregion

        #region Генерация событий

        private void RaiseDeclarationListItemActivated()
        {
            if (DeclarationListItemActivated != null)
            {
                DeclarationListItemActivated();
            }
        }

        private void RaiseDeclarationCheckSelected()
        {
            if (DeclarationCheckSelected != null)
            {
                DeclarationCheckSelected();
            }
        }

        private void OnOpenDeclaration()
        {
            if (DeclarationOpen != null)
            {
                DeclarationOpen();
            }
        }

        private void OnOpenTaxPayer()
        {
            if (TaxPayerOpen != null)
            {
                TaxPayerOpen();
            }
        }

        private void OnInspectorSet()
        {
            if (InspectorSet != null)
            {
                InspectorSet();
            }
        }

        private void RaiseActiveDeclarationInspectorSet()
        {
            if (ActiveDeclarationInspectorSet != null)
            {
                ActiveDeclarationInspectorSet();
            }
        }

        #endregion

        #region Контекстное меню

        private void SetInspectorMenu_Click(object sender, EventArgs e)
        {
            RaiseActiveDeclarationInspectorSet();
        }

        private void OpenDeclarationMenu_Click(object sender, EventArgs e)
        {
            OnOpenDeclaration();
        }

        private void OpenTaxPayerCard_Click(object sender, EventArgs e)
        {
            OnOpenTaxPayer();
        }

        #endregion

        #region События

        private void GridAfterRowActivate(object sender, EventArgs e)
        {
            RaiseDeclarationListItemActivated();
        }

        private void GridAfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            if (!_wasSelectedInConextMenu)
            {
                SetSelectedRows();
                GridCheckHeaderState();
            }
            else
                _wasSelectedInConextMenu = false;

            RaiseDeclarationListItemActivated();
        }

        private void GridCellChange(object sender, CellEventArgs e)
        {
            e.Cell.Row.Update();
            SetCheckedRows();
            RaiseDeclarationCheckSelected();
        }

        bool _wasSelectedInConextMenu = false;

        private void GridBeforeShowContextMenu(object sender, CommonComponents.Uc.Infrastructure.Interface.EventArgs<UltraGridRow> e)
        {
            gridDeclaration.Grid.AfterSelectChange -= GridAfterSelectChange;
            gridDeclaration.Grid.CellChange -= GridCellChange;

            _wasSelectedInConextMenu = _multiSelectedRow.ClearAllSelectedRowExceptOne(e.Data);
            _multiSelectedRow.SetSelectedRows(TypeHelper<DeclarationModel>.GetMemberName(t => t.Selected));

            gridDeclaration.Grid.AfterSelectChange += GridAfterSelectChange;
            gridDeclaration.Grid.CellChange += GridCellChange;
        }

        #endregion

        # region Список деклараций

        private GridColumnSetup CreateSetupGrid()
        {
            var setup = new GridColumnSetup();
            var helper = new ColumnHelper<DeclarationModel>(gridDeclaration);

            var clipIcons = new Dictionary<bool, Bitmap>
            {
                { false, null },
                { true, Properties.Resources.paperclip2_black_5661 }
            };

            var enableIcons = new Dictionary<bool, Bitmap>
            {
                { false, Properties.Resources.disabled },
                { true, null }
            };

            var tooltipDictionary = new Dictionary<string, string>
            {
                { false.ToString(), "НП подлежит автораспределению" },
                { true.ToString(), "НП не подлежит автораспределению" }  
            };

            _checkColumn = helper.CreateCheckColumnThreeWay(m => m.Selected, 2).Configure(d =>
            {
                d.Caption = String.Empty;
                d.HeaderToolTip = "Флаг выбора записи";
                d.DisableMoving = true;
                d.Width = 20;
                d.MinWidth = 20;
            });
            setup.Columns.Add(_checkColumn);

            _checkColumn.HeaderCheckedChange += GridHeaderCheckedChange;

            setup.Columns.Add(helper.CreateImageColumn(m => m.IsTaxPayerAttached, this, clipIcons, 2).Configure(d =>
            {
                d.Caption = String.Empty;
                d.HeaderToolTip = "НП не подлежит автораспределению";
                d.DisableMoving = true;
                d.DisableFilter = true;
                d.ToolTipViewer = new DictionaryToolTipViewer(tooltipDictionary);
                d.Width = 50;
            }));

            var taxPayerGroup = helper.CreateGroup("Налогоплательщик", 2);
            taxPayerGroup.Columns.Add(helper.CreateTextColumn(m => m.Inn, 2).Configure(d =>
            {
                d.Caption = "ИНН";
                d.HeaderToolTip = "ИНН предыдущего инспектора";
                d.DisableMoving = true;
            }));
            taxPayerGroup.Columns.Add(helper.CreateTextColumn(m => m.Kpp, 2).Configure(d =>
            {
                d.Caption = "КПП";
                d.HeaderToolTip = "КПП предыдущего инспектора";
                d.DisableMoving = true;
            }));
            taxPayerGroup.Columns.Add(helper.CreateTextColumn(m => m.Name, 2).Configure(d =>
            {
                d.Caption = "Наименование";
                d.HeaderToolTip = "Наименование налогоплательщика";
                d.DisableMoving = true;
            }));
            setup.Columns.Add(taxPayerGroup);
            setup.Columns.Add(helper.CreateTextColumn(m => m.Period, 2).Configure(d =>
            {
                d.Caption = "Отчетный период";
                d.HeaderToolTip = "Отчетный период";
                d.DisableMoving = true;
            }));
            setup.Columns.Add(helper.CreateTextColumn(m => m.DeclSign, 2).Configure(d =>
            {
                d.Caption = "Признак НД";
                d.HeaderToolTip = "Признак декларации";
                d.DisableMoving = true;
            }));
            setup.Columns.Add(helper.CreateTextColumn(m => m.CompensationAmount, 2).Configure(d =>
            {
                d.Caption = "Сумма НДС";
                d.HeaderToolTip = "Сумма налога, исчисленная к «+» уплате / «-» возмещению в бюджет";
                d.DisableMoving = true;
            }));
            setup.Columns.Add(helper.CreateTextColumn(m => m.ClaimAmount, 2).Configure(d =>
            {
                d.Caption = "Сумма расхождений по АТ";
                d.HeaderToolTip = "Сумма расхождений по открытым автотребованиям по СФ";
                d.DisableMoving = true;
            }));

            var inspectorGroup = helper.CreateGroup("Инспектор, обрабатывающий данную НД до открепления", 2);
            inspectorGroup.Columns.Add(helper.CreateTextColumn(
                    m => m.InspectorTabelNumber,
                    ColumnExpressionCreator.ColumnExpressionChangeMapping("EMPLOYEENUM")).Configure(d =>
                    {
                        d.Caption = "Табельный номер №";
                        d.HeaderToolTip = "Табельный номер предыдущего инспектора";
                        d.DisableMoving = true;
                        d.Width = 150;
                    }));
            inspectorGroup.Columns.Add(helper.CreateTextColumn(m => m.InspectorName).Configure(d =>
            {
                d.Caption = "ФИО";
                d.HeaderToolTip = "ФИО предыдущего инспектора";
                d.DisableMoving = true;
                d.Width = 220;
            }));
            setup.Columns.Add(inspectorGroup);

            return setup;
        }

        private CheckColumnThreeWay _checkColumn;

        private void GridHeaderCheckedChange(CheckState checkState)
        {
            var columnNameSelected = TypeHelper<DeclarationModel>.GetMemberName(t => t.Selected);
            bool storeInUndoCheck = false;
            foreach (var itemRow in gridDeclaration.Grid.Rows)
            {
                if (checkState == CheckState.Checked)
                    itemRow.Cells[columnNameSelected].SetValue(true, storeInUndoCheck);
                if (checkState == CheckState.Unchecked)
                    itemRow.Cells[columnNameSelected].SetValue(false, storeInUndoCheck);
                itemRow.Update();
            }
            SetCheckedRows();
            AllCheckedActionExtended(checkState);
            RaiseDeclarationCheckSelected();
        }

        # endregion

        # region MultiSelectedRow

        private MultiSelectedRow _multiSelectedRow = null;

        private void SetSelectedRows()
        {
            gridDeclaration.Grid.CellChange -= GridCellChange;
            gridDeclaration.Grid.AfterSelectChange -= GridAfterSelectChange;

            _multiSelectedRow.SetSelectedRows(TypeHelper<DeclarationModel>.GetMemberName(t => t.Selected));

            RaiseDeclarationCheckSelected();

            gridDeclaration.Grid.AfterSelectChange += GridAfterSelectChange;
            gridDeclaration.Grid.CellChange += GridCellChange;
        }

        private void SetCheckedRows()
        {
            gridDeclaration.Grid.AfterSelectChange -= GridAfterSelectChange;

            _multiSelectedRow.SetCheckedRows(TypeHelper<DeclarationModel>.GetMemberName(t => t.Selected));

            gridDeclaration.Grid.AfterSelectChange += GridAfterSelectChange;
        }

        private void AllCheckedActionExtended(CheckState checkState)
        {
            gridDeclaration.Grid.AfterSelectChange -= GridAfterSelectChange;

            _multiSelectedRow.AllCheckedActionExtended(checkState);

            gridDeclaration.Grid.AfterSelectChange += GridAfterSelectChange;
        }

        private void GridCheckHeaderState()
        {
            _checkColumn.HeaderCheckedChange -= GridHeaderCheckedChange;

            _multiSelectedRow.CheckHeaderState(TypeHelper<DeclarationModel>.GetMemberName(t => t.Selected));

            _checkColumn.HeaderCheckedChange += GridHeaderCheckedChange;
        }

        public void GridClearHeaderState()
        {
            _checkColumn.HeaderCheckedChange -= GridHeaderCheckedChange;

            _multiSelectedRow.ClearHeaderState(TypeHelper<DeclarationModel>.GetMemberName(t => t.Selected));

            _checkColumn.HeaderCheckedChange += GridHeaderCheckedChange;
        }

        # endregion
    }
}
