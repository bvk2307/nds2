﻿using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination;
using Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.DeclarationAssignment.NotDistributedDeclarations
{
    public class Model
    {
        # region Конструктор

        public Model()
        {
            NotDistributedDeclarations = new List<DeclarationModel>();
            Pager = new PagerStateViewModel();
        }

        # endregion

        # region События модели

        public ParameterlessEventHandler AfterSelectionChanged;

        # endregion

        # region Декларации

        public IEnumerable<DeclarationModel> NotDistributedDeclarations
        {
            get;
            private set;
        }

        public void SetNotDistributedDeclarations(
            IEnumerable<InspectorDeclarationNotDistribution> rawData)
        {
            var selectionChanged = AnyDeclarationSelected();
            var declarations = rawData.Select(x => new DeclarationModel(x)).ToList();

            foreach (var declaration in declarations)
            {
                declaration.SelectedChanged += OnDeclarationSelectedChanged;
            }

            NotDistributedDeclarations = declarations.ToArray();

            if (selectionChanged && AfterSelectionChanged != null)
            {
                AfterSelectionChanged();
            }
        }

        private void OnDeclarationSelectedChanged()
        {
            if (AfterSelectionChanged != null)
            {
                AfterSelectionChanged();
            }
        }

        public PagerStateViewModel Pager
        {
            get;
            private set;
        }

        public bool AnyDeclarationSelected()
        {
            return NotDistributedDeclarations.Any(x => x.Selected);
        }

        public long[] SelectedDeclarationIdentifiers()
        {
            return NotDistributedDeclarations.Where(p => p.Selected).Select(x => x.GetDeclarationId()).ToArray();
        }

        public DeclarationModel SingleDeclarationSelected()
        {
            return NotDistributedDeclarations.Where(p => p.Selected).Single();
        }

        public bool IsSingleDeclarationSelected()
        {
            return NotDistributedDeclarations.Where(p => p.Selected).Count() == 1;
        }

        # endregion
    }
}
