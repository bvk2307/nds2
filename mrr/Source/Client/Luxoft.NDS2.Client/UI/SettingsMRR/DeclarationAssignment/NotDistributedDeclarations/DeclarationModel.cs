﻿using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.DeclarationAssignment.NotDistributedDeclarations
{
    public class DeclarationModel
    {
        # region Данные

        private readonly Declaration _declaration;

        private readonly InspectorDeclarationLastAssignment _inspectorLastAssignment;

        # endregion

        # region Конструктор

        public DeclarationModel(InspectorDeclarationNotDistribution declaration)
        {
            _declaration = declaration.Declaration;
            _inspectorLastAssignment = declaration.InspectorLastAssignment;
        }

        # endregion

        # region Свойства декларации (для списка)

        /// <summary>
        /// ИНН налогоплательщика
        /// </summary>
        public string Inn 
        { 
            get { return _declaration.Inn; } 
        }

        /// <summary>
        /// КПП налогоплательщика
        /// </summary>
        public string Kpp 
        { 
            get { return _declaration.Kpp; } 
        }

        /// <summary>
        /// Наименование налогоплательщика
        /// </summary>
        public string Name 
        { 
            get { return _declaration.Name; } 
        }

        /// <summary>
        /// отчетный период декларации
        /// </summary>
        public string Period 
        { 
            get { return _declaration.Period; } 
        }

        /// <summary>
        /// признак декларации
        /// </summary>
        public string DeclSign 
        { 
            get { return _declaration.DeclSign; } 
        }

        /// <summary>
        /// сумма НДС
        /// </summary>
        public decimal CompensationAmount 
        { 
            get { return _declaration.CompensationAmount; } 
        }

        /// <summary>
        /// Сумма расхождений по АТ
        /// </summary>
        public decimal? ClaimAmount 
        { 
            get { return _declaration.ClaimAmount; } 
        }

        private bool _selected;

        /// <summary>
        /// Возвращает или задает признак выбора декларации
        /// </summary>
        public bool Selected 
        {
            get
            {
                return _selected;
            }
            set
            {
                var changed = _selected != value;                
                _selected = value;

                if (changed && SelectedChanged != null)
                {
                    SelectedChanged();
                }
            }
        }

        /// <summary>
        /// Табельный номер инспектора (до открепления)
        /// </summary>
        public string InspectorTabelNumber 
        { 
            get { return _inspectorLastAssignment.EmployeeNum; } 
        }

        /// <summary>
        /// ФИО инспектора (до открепления)
        /// </summary>
        public string InspectorName 
        { 
            get { return _inspectorLastAssignment.Name; } 
        }

        /// <summary>
        /// Признак закрепления НП за инспектором
        /// </summary>
        public bool IsTaxPayerAttached 
        { 
            get { return _declaration.IsTaxPayerAttached; } 
        }

        # endregion

        # region События

        public ParameterlessEventHandler SelectedChanged;

        # endregion

        # region Методы

        public long? DeclarationZip()
        {
            return _declaration.DeclarationVersionId;
        }

        public long GetDeclarationId()
        {
            return _declaration.Id;
        }

        public string InspectorSid()
        {
            return _inspectorLastAssignment.SID;
        }

        # endregion
    }
}
