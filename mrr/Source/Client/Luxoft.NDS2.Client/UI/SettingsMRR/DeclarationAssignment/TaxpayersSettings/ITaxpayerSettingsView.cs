﻿using Luxoft.NDS2.Client.UI.Base;
using System.Windows.Forms;
namespace Luxoft.NDS2.Client.UI.SettingsMRR.DeclarationAssignment.TaxpayersSettings
{
    public interface ITaxpayerSettingsView : IViewBase
    {
        void SetRightButtonEnabled(bool enabled);

        void SetLeftButtonEnabled(bool enabled);

        void SetButtonEnabled();

        void ClearSortDest();

        /// <summary>
        /// Возвращает или задает доступность функции назначения инспектора
        /// </summary>
        bool AllowAssignInspector
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает доступность функции отмены назначения
        /// </summary>
        bool AllowViewCancelAssignment
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает ссылку на активный элемент списка деклараций
        /// </summary>
        object ActiveDeclarationListItem
        {
            get;
        }

        event ParameterlessEventHandler DeclarationListItemActivated;

        event ParameterlessEventHandler InspectorSet;

        event ParameterlessEventHandler DeclarationCheckSelected;

        event ParameterlessEventHandler ActiveDeclarationInspectorSet;

        Form ParentForm { get; }

        void UnSelectAllRows();
    }
}
