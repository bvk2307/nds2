﻿using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination;
using Luxoft.NDS2.Client.UI.Controls.DependedGridsSelector;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.Services.DeclarationAssignment;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.DeclarationAssignment.TaxpayersSettings
{
    public class Presenter : BasePresenter<View>
    {
        # region Модель

        private readonly Model _model = new Model();

        # endregion

        private DataGridPresenter<TaxpayerModel> _gridPresenterSource;
        private DataGridPresenter<TaxpayerAssignmentModel> _gridPresenterDest;
        private ITaxpayerService _taxpayerService;
        private ITaxpayerAssignmentService _taxAssignService;
        private IInspectorStatisticsService _inspectorService;
        private ITaxpayerSettingsView _tsView;

        public override void OnObjectCreated()
        {
            _taxpayerService = base.GetServiceProxy<ITaxpayerService>();
            _taxAssignService = base.GetServiceProxy<ITaxpayerAssignmentService>();
            _inspectorService = base.GetServiceProxy<IInspectorStatisticsService>();
            _tsView = (ITaxpayerSettingsView)View;

            BindViewEvents();
            BindModelEvents();
        }

        public void _taxpayerSettingsView_ObjectsMoving(object sender, MoveEventArgs e)
        {
            switch (e.Direction)
            {
                case MoveDirection.SourceToDest:
                    CreateTaxpayerAssignment();
                    break;
                case MoveDirection.DestToSource:
                    if (View.ShowQuestion("Удаление налогоплательщиков", "При удалении налогоплательщиков из списка для персонального закрепления все их последующие декларации будут распределяться в общем порядке в соответствии с настройками распределения деклараций. Удалить?") == System.Windows.Forms.DialogResult.Yes)
                    {
                        RemoveTaxpayerAssignment();
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public void DataUpdate()
        {
            _gridPresenterSource.Load();
            _tsView.ClearSortDest();
            _gridPresenterDest.Load();
            _tsView.SetLeftButtonEnabled(false);
            _tsView.SetRightButtonEnabled(false);
        }

        #region Source DataGrid

        public DataGridPresenter<TaxpayerModel> GridPresenterSource { get { return _gridPresenterSource; } }

        public void InitializeSourceGrid(IDataGridView grid, IPager pager)
        {
            var conditions = new QueryConditions
            {
                Sorting = new List<ColumnSort> { new ColumnSort
                {
                    ColumnKey = TypeHelper<Taxpayer>.GetMemberName(t => t.INN), 
                    Order = ColumnSort.SortOrder.Asc
                } }
            };

            _gridPresenterSource =
                new PagedDataGridPresenter<TaxpayerModel>(
                    grid,
                    pager,
                    GetSourceData,
                    conditions);

            _gridPresenterSource.Init(SettingsProvider(string.Format("{0}_InitializeSrcGrid", GetType())));

            _gridPresenterSource.Load();
        }        

        private PageResult<TaxpayerModel> GetSourceData(QueryConditions conditions)
        {
            var taxpayersPageResult = new PageResult<Taxpayer>();
            ExecuteServiceCall(() => _taxpayerService.GeTaxpayersList(conditions),
                result => { taxpayersPageResult = result.Result; });

            return new PageResult<TaxpayerModel>(taxpayersPageResult.Rows.Select(t => new TaxpayerModel(t)).ToList()
                , taxpayersPageResult.TotalMatches, taxpayersPageResult.TotalAvailable);
        }

        #endregion

        #region Dest DataGrid

        public DataGridPresenter<TaxpayerAssignmentModel> GridPresenterDest { get { return _gridPresenterDest; } }

        public void InitializeDestGrid(IDataGridView grid, IPager pager)
        {
            _gridPresenterDest =
                new PagedDataGridPresenter<TaxpayerAssignmentModel>(
                    grid,
                    pager,
                    GetDestData,
                    null);

            _gridPresenterDest.Init(SettingsProvider(string.Format("{0}_InitializeDstGrid", GetType())));

            _gridPresenterDest.Load();
        }

        private PageResult<TaxpayerAssignmentModel> GetDestData(QueryConditions conditions)
        {
            var taxpayersPageResult = new PageResult<TaxpayerAssignment>();
            ExecuteServiceCall(() => _taxAssignService.GetTaxpayersAssigmentsList(conditions),
                result => { taxpayersPageResult = result.Result; });

            _model.SetDeclarations(taxpayersPageResult.Rows);
            
            return new PageResult<TaxpayerAssignmentModel>(_model.Declarations.ToList(),
                taxpayersPageResult.TotalMatches, taxpayersPageResult.TotalAvailable);
        }

        #endregion

        private void CreateTaxpayerAssignment()
        {
            List<Taxpayer> taxpayers =
                _gridPresenterSource.Rows.Where(t => t.Selected).Select(t => t.GetTaxpayer()).ToList();
            
            ExecuteServiceCall(() => _taxAssignService.AddTaxpayerAssignment(taxpayers), OnAssignmentsAdd);

        }

        public void CancelInspectorAssignment(TaxpayerAssignment ta)
        {
            ExecuteServiceCall(() => _taxAssignService.CancelInspectorAssignment(ta), OnInspectorAssignmentChanged);
        }

        public void AddInspectorAssignment(TaxpayerAssignment ta)
        {
            ExecuteServiceCall(() => _taxAssignService.AddInspectorAssignment(ta), OnInspectorAssignmentChanged);
        }

        public List<InspectorStatistics> GetInspectors()
        {
            var result = new List<InspectorStatistics>();
            ExecuteServiceCall(() => _inspectorService.GetActiveInspectorsStatistics(), (res => result = res.Result));
            return result;
        }

        private void OnAssignmentsAdd(OperationResult<TaxpayersResult> result)
        {
            DataUpdate();
        }

        private void RemoveTaxpayerAssignment()
        {
            List<TaxpayerAssignment> taxpayersAssignments =
                _gridPresenterDest.Rows.Where(t => t.Selected).Select(t => t.GetTaxpayerAssignment()).ToList();

            ExecuteServiceCall(() => _taxAssignService.RemoveTaxpayerAssignment(taxpayersAssignments), OnAssignmentsRemove);
        }

        private void OnAssignmentsRemove(OperationResult<TaxpayersAssignmentsResult> result)
        {
            DataUpdate();
        }

        private void OnInspectorAssignmentChanged(OperationResult<bool> result)
        {
            if (result.Result)
            {
                _gridPresenterDest.Load();
            }
        }

        private void BindViewEvents()
        {
            _tsView.DeclarationListItemActivated += OnDeclarationActivated;
            _tsView.InspectorSet += ViewInspectorSet;
            _tsView.ActiveDeclarationInspectorSet += ViewActiveDeclarationInspectorSet;
            _tsView.DeclarationCheckSelected += ViewDeclarationCheckSelected;
        }

        private void BindModelEvents()
        {
            _model.AfterSelectionChanged += OnAfterSelectionChanged;
        }

        # region Обработка событий представления

        /// <summary>
        /// Обновляет доступность функции отмены назначения
        /// </summary>
        private void OnDeclarationActivated()
        {
            _tsView.AllowViewCancelAssignment = _model.IsSingleDeclarationSelected();
        }

        /// <summary>
        /// Обновляет доступность функции назначения инспектора и функции отмены назначения
        /// </summary>
        private void ViewDeclarationCheckSelected()
        {
            _tsView.AllowAssignInspector = _model.AnyDeclarationSelected();
            _tsView.AllowViewCancelAssignment = _model.IsSingleDeclarationSelected();
        }

        # endregion

        # region Обработка событий модели

        private void OnAfterSelectionChanged()
        {
            _tsView.AllowAssignInspector = _model.AnyDeclarationSelected();
            _tsView.AllowViewCancelAssignment = _model.IsSingleDeclarationSelected();
        }

        # endregion  

        # region Навигация к другим экранным формам

        /// <summary>
        /// Открывает диалог назначения выделенных чекбоксом деклараций на инспектора
        /// </summary>
        private void ViewInspectorSet()
        {
            if (!_model.AnyDeclarationSelected())
            {
                _tsView.ShowNotification("Выберите одну или несколько деклараций");
                return;
            }

            if (_model.IsSingleDeclarationSelected())
            {
                SetInspectorSingleDeclaration(_model.SingleDeclarationSelected());
            }
            else
            {
                _tsView.AllowAssignInspector = false;

                var inspAssgForm = new InspectorAssignmentForm(GetInspectors().Select(i => new InspectorStatisticsModel(i)).ToList());
                inspAssgForm.SetCurrentInspector(String.Empty);
                if (inspAssgForm.ShowDialog(_tsView.ParentForm) == System.Windows.Forms.DialogResult.OK)
                {
                    foreach (var taxpayerAssignmentModel in _model.Declarations.Where(p => p.Selected))
                    {
                        TaxpayerAssignment taxpayerAssignment = taxpayerAssignmentModel.GetTaxpayerAssignment();
                        taxpayerAssignment.SID = inspAssgForm.GetInspectorSid();
                        if (!String.IsNullOrEmpty(taxpayerAssignment.SID))
                        {
                            AddInspectorAssignment(taxpayerAssignment);
                        }
                        else
                        {
                            CancelInspectorAssignment(taxpayerAssignment);
                        }
                    }
                    _tsView.UnSelectAllRows();
                }
                _tsView.AllowAssignInspector = _model.AnyDeclarationSelected();
            }
        }

        /// <summary>
        /// Открывает диалог назначения активной декларации на инспектора
        /// </summary>
        private void ViewActiveDeclarationInspectorSet()
        {
            ViewInspectorSet();
        }

        /// <summary>
        /// Открывает диалог назначения одиночной декларации на инспектора
        /// </summary>
        private void SetInspectorSingleDeclaration(TaxpayerAssignmentModel declarationModel)
        {
            var taxpayerAssignment = declarationModel.GetTaxpayerAssignment();
            var inspAssgForm = new InspectorAssignmentForm(GetInspectors().Select(i => new InspectorStatisticsModel(i)).ToList());
            inspAssgForm.SetCurrentInspector(taxpayerAssignment.SID);
            var resultDialog = inspAssgForm.ShowDialog(_tsView.ParentForm);
            if (resultDialog  == System.Windows.Forms.DialogResult.OK)
            {
                taxpayerAssignment.SID = inspAssgForm.GetInspectorSid();
                if (!String.IsNullOrEmpty(taxpayerAssignment.SID))
                {
                    AddInspectorAssignment(taxpayerAssignment); 
                }
                else 
                {
                    CancelInspectorAssignment(taxpayerAssignment); 
                }
                _tsView.UnSelectAllRows();
            }
        }

        # endregion
    }
}
