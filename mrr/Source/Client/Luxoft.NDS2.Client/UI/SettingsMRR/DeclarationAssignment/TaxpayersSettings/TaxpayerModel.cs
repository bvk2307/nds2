﻿using Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment;
using System.ComponentModel;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.DeclarationAssignment.TaxpayersSettings
{
    public class TaxpayerModel
    {
        private Taxpayer _taxpayer;

        public TaxpayerModel(Taxpayer taxpayer)
        {
            _taxpayer = taxpayer;
        }

        [DisplayName("")]
        [Description("Флаг выбора записи")]
        public bool Selected { get; set; }

        [DisplayName("ИНН")]
        [Description("ИНН налогоплательщика")]
        public string Inn { get { return _taxpayer.INN; } }

        [DisplayName("КПП")]
        [Description("КПП налогоплательщика")]
        public string Kpp { get { return _taxpayer.KPP; } }

        [DisplayName("Наименование")]
        [Description("Наименование налогоплательщика")]
        public string Name { get { return _taxpayer.NAME; } }

        public Taxpayer GetTaxpayer()
        {
            return _taxpayer;
        }

        public TaxpayerAssignment GetTaxpayerAssignment()
        {
            return new TaxpayerAssignment()
            {
                INN = _taxpayer.INN,
                KPP = _taxpayer.KPP,

            };
        }
    }
}
