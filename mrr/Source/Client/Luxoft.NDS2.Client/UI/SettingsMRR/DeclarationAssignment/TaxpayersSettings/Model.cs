﻿using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.DeclarationAssignment.TaxpayersSettings
{
    public class Model
    {
        # region Конструктор

        public Model()
        {
            Declarations = new List<TaxpayerAssignmentModel>();
        }

        # endregion

        # region События модели

        public ParameterlessEventHandler AfterSelectionChanged;

        # endregion

        # region Декларации

        public IEnumerable<TaxpayerAssignmentModel> Declarations
        {
            get;
            private set;
        }

        public void SetDeclarations(
            IEnumerable<TaxpayerAssignment> rawData)
        {
            var selectionChanged = AnyDeclarationSelected();
            var declarations = rawData.Select(t => new TaxpayerAssignmentModel(t)).ToList();

            foreach (var declaration in declarations)
            {
                declaration.SelectedChanged += OnDeclarationSelectedChanged;
            }

            Declarations = declarations.ToArray();

            if (selectionChanged && AfterSelectionChanged != null)
            {
                AfterSelectionChanged();
            }
        }

        private void OnDeclarationSelectedChanged()
        {
            if (AfterSelectionChanged != null)
            {
                AfterSelectionChanged();
            }
        }

        public bool AnyDeclarationSelected()
        {
            return Declarations.Any(x => x.Selected);
        }

        public IEnumerable<TaxpayerAssignmentModel> SelectedDeclarations()
        {
            return Declarations.Where(p => p.Selected).ToArray();
        }

        public TaxpayerAssignmentModel SingleDeclarationSelected()
        {
            return Declarations.Where(p => p.Selected).Single();
        }

        public bool IsSingleDeclarationSelected()
        {
            return Declarations.Where(p => p.Selected).Count() == 1;
        }

        # endregion
    }
}
