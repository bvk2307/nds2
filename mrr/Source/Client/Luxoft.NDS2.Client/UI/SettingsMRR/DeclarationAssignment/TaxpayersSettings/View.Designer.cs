﻿namespace Luxoft.NDS2.Client.UI.SettingsMRR.DeclarationAssignment.TaxpayersSettings
{
    partial class View
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._dependedGridsSelector = new Luxoft.NDS2.Client.UI.Controls.DependedGridsSelector.DependedDataGridsSelector(false);
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            this.SuspendLayout();
            cmRowMenu = new System.Windows.Forms.ContextMenuStrip();
            cmiCancelAssignment = new System.Windows.Forms.ToolStripMenuItem();
            this.setInspectorMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.cmRowMenu.SuspendLayout();
            //
            // cmRowMenu
            //
            this.cmRowMenu.Items.Add(cmiCancelAssignment);
            this.cmRowMenu.Items.Add(setInspectorMenu);
            this.cmRowMenu.Name = "cmRowMenu";
            //this.cmRowMenu.Size = new System.Drawing.Size(291, 148);
            this.cmRowMenu.Opening += new System.ComponentModel.CancelEventHandler(this.cmRowMenu_Opening);
            //
            // cmiCancelAssignment
            //
            this.cmiCancelAssignment.Name = "cmiCancelAssignment";
            this.cmiCancelAssignment.Size = new System.Drawing.Size(290, 22);
            this.cmiCancelAssignment.Text = "Отменить назначение";
            this.cmiCancelAssignment.Click += new System.EventHandler(this.cmiCancelAssignment_Click);
            // 
            // setInspectorMenu
            // 
            this.setInspectorMenu.Name = "setInspectorMenu";
            this.setInspectorMenu.Size = new System.Drawing.Size(278, 22);
            this.setInspectorMenu.Text = "Назначить инспектора";
            this.setInspectorMenu.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.setInspectorMenu.Click += new System.EventHandler(this.SetInspectorMenu_Click);
            // 
            // _dependedGridsSelector
            // 
            this._dependedGridsSelector.BackColor = System.Drawing.Color.Transparent;
            this._dependedGridsSelector.Dock = System.Windows.Forms.DockStyle.Fill;
            this._dependedGridsSelector.Location = new System.Drawing.Point(0, 0);
            this._dependedGridsSelector.Name = "_dependedGridsSelector";
            this._dependedGridsSelector.Size = new System.Drawing.Size(878, 573);
            this._dependedGridsSelector.DestGrid.GridContextMenuStrip = cmRowMenu;
            this._dependedGridsSelector.SourceGridAreaRatio = 0.35M;
            this._dependedGridsSelector.TabIndex = 0;
            // 
            // ultraPanel1
            // 
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this._dependedGridsSelector);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel1.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(878, 573);
            this.ultraPanel1.TabIndex = 1;
            // 
            // View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ultraPanel1);            
            this.Name = "View";
            this.Size = new System.Drawing.Size(878, 573);
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            this.cmRowMenu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.Load += View_Load;

        }

        #endregion

        #region Context menu

        private System.Windows.Forms.ContextMenuStrip cmRowMenu;
        private System.Windows.Forms.ToolStripMenuItem cmiCancelAssignment;
        private System.Windows.Forms.ToolStripMenuItem setInspectorMenu;

        #endregion

        //private System.Windows.Forms.SplitContainer splitContainer1;
        private Controls.DependedGridsSelector.DependedDataGridsSelector _dependedGridsSelector;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
    }
}
