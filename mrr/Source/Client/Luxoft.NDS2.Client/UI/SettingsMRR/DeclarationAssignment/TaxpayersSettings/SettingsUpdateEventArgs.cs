﻿using System;
using Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.DeclarationAssignment.TaxpayersSettings
{
    public class AssigmentUpdateEventArgs : EventArgs
    {
        private readonly TaxpayerAssignment _assigment;
        public AssigmentUpdateEventArgs(TaxpayerAssignment assigment)
        {
            _assigment = assigment;
        }

        public TaxpayerAssignment Assigment
        {
            get { return _assigment; }
        }
    }
}
