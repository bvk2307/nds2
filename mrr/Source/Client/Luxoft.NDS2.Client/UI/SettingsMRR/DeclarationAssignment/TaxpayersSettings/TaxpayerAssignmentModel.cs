﻿using System.ComponentModel;
using Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment;
using Luxoft.NDS2.Client.UI.Base;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.DeclarationAssignment.TaxpayersSettings
{
    public class TaxpayerAssignmentModel
    {
        private readonly TaxpayerAssignment _assignment;
        public TaxpayerAssignmentModel(TaxpayerAssignment assignment)
        {
            _assignment = assignment;
        }

        private bool _selected;

        /// <summary>
        /// Возвращает или задает признак выбора декларации
        /// </summary>
        [DisplayName("")]
        [Description("Флаг выбора записи")]
        public bool Selected
        {
            get
            {
                return _selected;
            }
            set
            {
                var changed = _selected != value;
                _selected = value;

                if (changed && SelectedChanged != null)
                {
                    SelectedChanged();
                }
            }
        }

        [DisplayName("")]
        [Description("Наличие доступа")]
        public bool HasAccess { get { return (string.IsNullOrEmpty(_assignment.EMPLOYEE_NUM) && string.IsNullOrEmpty(_assignment.INSPECTOR_NAME)) || _assignment.IS_INSPECTOR_ACTIVE; } }

        [DisplayName("")]
        [Description("Редактирование инспектора")]
        public int EditImage { get { return 1; } }

        [DisplayName("ИНН")]
        [Description("ИНН налогоплательщика")]
        public string Inn { get { return _assignment.INN; } }

        [DisplayName("КПП")]
        [Description("КПП налогоплательщика")]
        public string Kpp { get { return _assignment.KPP; } }

        [DisplayName("Наименование")]
        [Description("Наименование налогоплательщика")]
        public string TAX_PAYER_NAME { get { return _assignment.TAX_PAYER_NAME; } }

        [DisplayName("Табельный №")]
        [Description("Табельный номер инспектора")]
        public string EMPLOYEE_NUM { get { return _assignment.EMPLOYEE_NUM; } }

        [DisplayName("ФИО")]
        [Description("ФИО инспектора")]
        public string INSPECTOR_NAME { get { return _assignment.INSPECTOR_NAME; } }

        public TaxpayerAssignment GetTaxpayerAssignment()
        {
            return _assignment;
        }

        # region События

        public ParameterlessEventHandler SelectedChanged;

        # endregion
    }
}
