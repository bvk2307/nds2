﻿using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using CommonComponents.Uc.Infrastructure.Interface.UI;
using CommonComponents.Utils;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Addins;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Helpers;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Microsoft.Practices.CompositeUI.Commands;
using Microsoft.Practices.ObjectBuilder;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.DeclarationAssignment.TaxpayersSettings
{
    public partial class View : BaseView, ITaxpayerSettingsView
    {
        private readonly PagerStateViewModel _pagerSource = new PagerStateViewModel();
        private readonly PagerStateViewModel _pagerDest = new PagerStateViewModel();
        private ExtendedDataGridPageNavigator _pageNavigatorSource;
        private ExtendedDataGridPageNavigator _pageNavigatorDest;

        private Presenter _presenter;
        [CreateNew]
        public Presenter Presenter
        {
            set
            {
                _presenter = value;
                _presenter.PresentationContext = _presentationContext;
                _presenter.View = this;
                WorkItem = _presenter.WorkItem;
                InitializeRibbon();
                _presenter.OnObjectCreated();
            }
        }

        public View()
        {
            InitializeComponent();
        }

        public View(PresentationContext ctx)
            : base(ctx)
        {
            InitializeComponent();
        }

        private void View_Load(object sender, EventArgs e)
        {
            InitializeGrid();
            //InitializePreFilter();
            _dependedGridsSelector.MoveLeftToolTip = "Удалить налогоплательщика";
            _dependedGridsSelector.MoveRightToolTip = "Добавить налогоплательщика";
            _presenter.RefreshEKP();

            _timer.Interval = 200;
            _timer.Tick += TimerTick;
            _timer.Enabled = true;
        }

        private void TimerTick(object sender, EventArgs e)
        {
            _timer.Enabled = false;
            _dependedGridsSelector.DestGrid.Grid.ActiveRow = null;
            _dependedGridsSelector.DestGrid.Grid.Selected.Rows.Clear();
        }

        private Timer _timer = new Timer();

        private void InitializeGrid()
        {
            _dependedGridsSelector.SourceHeaderHtmlText = "Перечень НП для закрепления";
            _dependedGridsSelector.ObjectsMoving += _presenter._taxpayerSettingsView_ObjectsMoving;

            #region Init SourceGrid
            _pageNavigatorSource = new ExtendedDataGridPageNavigator(_pagerSource);
            _pageNavigatorSource.HidePageSizer();
            _dependedGridsSelector.SourceGrid
                 .WithPager(_pageNavigatorSource)
                 .InitColumns(SetupSourceGrid());
            _dependedGridsSelector.SourceGrid.RegisterTableAddin(new ExpansionIndicatorRemover());

            _dependedGridsSelector.SourceGrid.Grid.DisplayLayout.AutoFitStyle = AutoFitStyle.None;
            _dependedGridsSelector.SourceGrid.PanelLoadingVisible = true;

            _presenter.InitializeSourceGrid(_dependedGridsSelector.SourceGrid, _pagerSource);
            #endregion

            _dependedGridsSelector.DestinationHeaderHtmlText = "Перечень НП, персонально закрепленных";
            #region Init DestGrid
            _pageNavigatorDest = new ExtendedDataGridPageNavigator(_pagerDest);
            _pageNavigatorDest.HidePageSizer();
            _dependedGridsSelector.DestGrid
                 .WithPager(_pageNavigatorDest)
                 .InitColumns(SetupDestGrid());
            _dependedGridsSelector.DestGrid.RegisterTableAddin(new ExpansionIndicatorRemover());

            _dependedGridsSelector.DestGrid.Grid.DisplayLayout.AutoFitStyle = AutoFitStyle.None;
            _dependedGridsSelector.DestGrid.PanelLoadingVisible = true;

            _presenter.InitializeDestGrid(_dependedGridsSelector.DestGrid, _pagerDest);

            _multiSelectedRow = new MultiSelectedRow(_dependedGridsSelector.DestGrid.Grid);
            _multiSelectedRow.SetupMultiSelectedRow();

            _dependedGridsSelector.DestGrid.Grid.AfterSelectChange += GridAfterSelectChange;
            _dependedGridsSelector.DestGrid.Grid.AfterRowActivate += GridAfterRowActivate;
            _dependedGridsSelector.DestGrid.Grid.CellChange += GridCellChange;
            _dependedGridsSelector.DestGrid.BeforeShowContextMenu += GridBeforeShowContextMenu;

            #endregion

        }

        private GridColumnSetup SetupSourceGrid()
        {
            var setup = new GridColumnSetup();
            var helper = new ColumnHelper<TaxpayerModel>(_dependedGridsSelector.SourceGrid);
            var checkColumn = helper.CreateCheckColumn(model => model.Selected).Configure(col =>
                {
                    col.DisableFilter = true;
                    col.DisableSort = true;
                    col.DisableMoving = true;
                    col.HeaderToolTip = "Флаг выбора записи";
                });
            checkColumn.RowCheckedChange += (o, b) =>
                {
                    var item = o as TaxpayerModel;
                    if (item != null)
                        item.Selected = b;
                    _pageNavigatorSource.SetSelectedCount(_presenter.GridPresenterSource.Rows.Where(r => r.Selected).Count());
                    _dependedGridsSelector.SetMoveSrcToDstEnabled(_presenter.GridPresenterSource.Rows.Any(x => x.Selected));
                };
            checkColumn.HeaderCheckedChange += state =>
                {
                    _presenter.GridPresenterSource.Rows.ForAll(row => row.Selected = (state == CheckState.Checked));
                    _pageNavigatorSource.SetSelectedCount(_presenter.GridPresenterSource.Rows.Where(r => r.Selected).Count());
                    _dependedGridsSelector.SetMoveSrcToDstEnabled(_presenter.GridPresenterSource.Rows.Any(x => x.Selected));
                    _dependedGridsSelector.SourceGrid.Refresh();
                };
            setup.Columns.Add(checkColumn);
            var group = helper.CreateGroup("Налогоплательщик", 2);
            group.Columns.Add(helper.CreateTextColumn(model => model.Inn, 1, 100).Configure(c =>
                { c.DisableMoving = true; c.MinWidth = 60; c.Width = 100; }));
            group.Columns.Add(helper.CreateTextColumn(model => model.Kpp, 1, 100).Configure(c =>
                { c.DisableMoving = true; c.MinWidth = 60; c.Width = 100; }));
            group.Columns.Add(helper.CreateTextColumn(model => model.Name, 1, 135).Configure(c =>
                { c.DisableMoving = true; c.MinWidth = 80; c.Width = 135; }));
            setup.Columns.Add(group);

            return setup;
        }

        private GridColumnSetup SetupDestGrid()
        {
            #region Icons and Tooltips
            var editIcons = new Dictionary<int, Bitmap>
                            {
                                {1, Properties.Resources.edit_yellow_Best}
                            };
            //var editTips = new Dictionary<int, string>
            //{
            //    {1, "Редактирование инспектора"}
            //};

            var accessIcons = new Dictionary<bool, Bitmap>
            {
                {false, Properties.Resources.disabled},
                {true, null}
            };
            var accessTips = new Dictionary<string, string>
            {
                {"true", "У инспектора нет доступа"},
                {"false", null}
            };
            #endregion

            var setup = new GridColumnSetup();
            var helper = new ColumnHelper<TaxpayerAssignmentModel>(_dependedGridsSelector.DestGrid);

            _checkColumn = helper.CreateCheckColumn(model => model.Selected).Configure(col =>
                {
                    col.DisableFilter = true;
                    col.DisableSort = true;
                    col.DisableMoving = true;
                    col.HeaderToolTip = "Флаг выбора записи";
                });
            _checkColumn.RowCheckedChange += RowCheckedChange;

            _checkColumn.HeaderCheckedChange += GridHeaderCheckedChange;
            setup.Columns.Add(_checkColumn);
            setup.Columns.Add(helper.CreateImageColumn(model => model.HasAccess, this, accessIcons, 2).Configure(col =>
                {
                    col.ToolTipViewer = new DictionaryToolTipViewer(accessTips);
                    col.DisableSort = true;
                    col.DisableFilter = true;
                    col.DisableMoving = true;
                    col.Width = 10;
                    col.HeaderToolTip = "Наличие доступа";
                }));

            var group = helper.CreateGroup("Налогоплательщик", 2);
            group.Columns.Add(helper.CreateTextColumn(model => model.Inn, 1, 100).Configure(c =>
                { c.DisableFilter = true; c.DisableMoving = true; c.MinWidth = 60; c.Width = 100; }));
            group.Columns.Add(helper.CreateTextColumn(model => model.Kpp, 1, 100).Configure(c =>
                { c.DisableFilter = true; c.DisableMoving = true; c.MinWidth = 60; c.Width = 100; }));
            group.Columns.Add(helper.CreateTextColumn(model => model.TAX_PAYER_NAME, 1, 135).Configure(c =>
                { c.DisableFilter = true; c.DisableMoving = true; c.MinWidth = 60; c.Width = 135; }));
            setup.Columns.Add(group);

            group = helper.CreateGroup("Инспектор", 2);
            group.Columns.Add(helper.CreateTextColumn(model => model.EMPLOYEE_NUM, 1, 100).Configure(c =>
                { c.DisableFilter = true; c.DisableMoving = true; c.MinWidth = 60; c.Width = 100; }));
            group.Columns.Add(helper.CreateTextColumn(model => model.INSPECTOR_NAME, 1, 135).Configure(c =>
                { c.DisableFilter = true; c.DisableMoving = true; c.MinWidth = 60; c.Width = 135; }));
            setup.Columns.Add(group);

            return setup;
        }

        public void SetRightButtonEnabled(bool enabled)
        {
            _dependedGridsSelector.SetMoveSrcToDstEnabled(enabled);
        }

        public void SetLeftButtonEnabled(bool enabled)
        {
            _dependedGridsSelector.SetMoveDstToSrcEnabled(enabled);
        }

        public void SetButtonEnabled()
        {
            _dependedGridsSelector.SetMoveSrcToDstEnabled(_presenter.GridPresenterSource.Rows.Any(x => x.Selected));
            _dependedGridsSelector.SourceGrid.Refresh();
            _dependedGridsSelector.SetMoveDstToSrcEnabled(_presenter.GridPresenterDest.Rows.Any(x => x.Selected));
            _dependedGridsSelector.DestGrid.Refresh();
        }

        public void ClearSortDest()
        {
            _dependedGridsSelector.DestGrid.Grid.DisplayLayout.Bands[0].SortedColumns.Clear();
        }

        private void cmiCancelAssignment_Click(object sender, EventArgs e)
        {
            var taModel = _dependedGridsSelector.DestGrid.GetCurrentItem<TaxpayerAssignmentModel>();
            if (taModel != null)
            {
                var ta = taModel.GetTaxpayerAssignment();
                _presenter.CancelInspectorAssignment(ta);
            }
        }

        private void cmRowMenu_Opening(object sender, EventArgs e)
        {
            if (_allowViewCancelAssignment)
            {
                var taModel = _dependedGridsSelector.DestGrid.GetCurrentItem<TaxpayerAssignmentModel>();
                if (taModel == null)
                {
                    cmiCancelAssignment.Enabled = false;
                    return;
                }
                var ta = taModel.GetTaxpayerAssignment();
                if (ta != null && !String.IsNullOrEmpty(ta.SID))
                {
                    cmiCancelAssignment.Enabled = true;
                }
                else
                {
                    cmiCancelAssignment.Enabled = false;
                }
            }
            else
            {
                cmiCancelAssignment.Enabled = _allowViewCancelAssignment;
            }
        }

        #region Риббон меню

        private UcRibbonButtonToolContext _assignInspectorButton;

        private void InitializeRibbon()
        {
            var resourceManagersService = WorkItem.Services.Get<IUcResourceManagersService>(true);

            if (!resourceManagersService.Contains(ResourceManagerNDS2.NDS2ClientResources))
            {
                resourceManagersService.Add(
                    ResourceManagerNDS2.NDS2ClientResources,
                    Properties.Resources.ResourceManager);
            }

            var tabNavigator = new UcRibbonTabContext(_presentationContext, "NDS2Result")
            {
                Text = _presentationContext.WindowTitle,
                ToolTipText = _presentationContext.WindowTitle,
                Visible = true,
                Order = 1
            };

            var groupNavigation = tabNavigator.AddGroup("NDS2DeclarationListManage");
            groupNavigation.Text = "Функции";
            groupNavigation.Visible = true;

            _assignInspectorButton =
                new UcRibbonButtonToolContext(_presentationContext, "btnSetInspector", "cmdSetInspector")
                {
                    Text = "Назначить инспектора",
                    ToolTipText = "Назначить инспектора",
                    ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                    LargeImageName = "set_decl_inspector",
                    SmallImageName = "set_decl_inspector",
                    Enabled = false
                };

            groupNavigation.ToolList.AddRange(
                new UcRibbonToolInstanceSettings[] { 
                    new UcRibbonToolInstanceSettings(
                        _assignInspectorButton.ItemName, 
                        UcRibbonToolSize.Large)
                });

            _presentationContext.UiVisualizationCollection.AddRange(
                new VisualizationElementBase[]
                {
                    _assignInspectorButton,
                    tabNavigator
                });

            _presentationContext.ActiveMenuTab = tabNavigator.ItemName;
        }

        [CommandHandler("cmdSetInspector")]
        public virtual void InspectorSetClick(object sender, EventArgs e)
        {
            OnInspectorSet();
        }

        #endregion

        # region CheckColumnHeader

        private CheckColumn _checkColumn;

        private void GridHeaderCheckedChange(CheckState checkState)
        {
            _presenter.GridPresenterDest.Rows.ForAll(row => row.Selected = (checkState == CheckState.Checked));
            _pageNavigatorDest.SetSelectedCount(_presenter.GridPresenterDest.Rows.Where(r => r.Selected).Count());
            _dependedGridsSelector.SetMoveDstToSrcEnabled(_presenter.GridPresenterDest.Rows.Any(x => x.Selected));
            _dependedGridsSelector.DestGrid.Refresh();

            var columnNameSelected = TypeHelper<TaxpayerAssignmentModel>.GetMemberName(t => t.Selected);
            bool storeInUndoCheck = false;
            foreach (var itemRow in _dependedGridsSelector.DestGrid.Grid.Rows)
            {
                if (checkState == CheckState.Checked)
                    itemRow.Cells[columnNameSelected].SetValue(true, storeInUndoCheck);
                if (checkState == CheckState.Unchecked)
                    itemRow.Cells[columnNameSelected].SetValue(false, storeInUndoCheck);
                itemRow.Update();
            }
            SetCheckedRows();
            AllCheckedActionExtended(checkState);
            RaiseDeclarationCheckSelected();
        }

        #endregion

        #region Публичные свойства

        public bool AllowAssignInspector
        {
            get
            {
                return _assignInspectorButton.Enabled;
            }
            set
            {
                this.ExecuteInUiThread(
                    v =>
                    {
                        _assignInspectorButton.Enabled = value;
                        RefreshEkp();
                    });
            }
        }

        private bool _allowViewCancelAssignment = true;

        public bool AllowViewCancelAssignment
        {
            get
            {
                return _allowViewCancelAssignment;
            }
            set
            {
                _allowViewCancelAssignment = value;
            }
        }

        public object ActiveDeclarationListItem
        {
            get
            {
                // Во избежание изменения ActiveRow в отдельном потоке сохраним на него ссылку
                var activeRow = _dependedGridsSelector.DestGrid.Grid.ActiveRow;

                return activeRow == null
                    ? null
                    : activeRow.ListObject;
            }
        }

        public event ParameterlessEventHandler DeclarationListItemActivated;

        public event ParameterlessEventHandler InspectorSet;

        public event ParameterlessEventHandler DeclarationCheckSelected;

        public event ParameterlessEventHandler ActiveDeclarationInspectorSet;

        #endregion

        #region Генерация событий

        private void RaiseDeclarationListItemActivated()
        {
            if (DeclarationListItemActivated != null)
            {
                DeclarationListItemActivated();
            }
        }

        private void RaiseDeclarationCheckSelected()
        {
            if (DeclarationCheckSelected != null)
            {
                DeclarationCheckSelected();
            }
        }

        private void OnInspectorSet()
        {
            if (InspectorSet != null)
            {
                InspectorSet();
            }
        }

        private void RaiseActiveDeclarationInspectorSet()
        {
            if (ActiveDeclarationInspectorSet != null)
            {
                ActiveDeclarationInspectorSet();
            }
        }

        #endregion

        #region Контекстное меню

        private void SetInspectorMenu_Click(object sender, EventArgs e)
        {
            RaiseActiveDeclarationInspectorSet();
        }

        #endregion

        #region События DestGrid

        private void GridAfterRowActivate(object sender, EventArgs e)
        {
            RaiseDeclarationListItemActivated();
        }

        private void GridAfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            if (!_wasSelectedInConextMenu)
            {
                SetSelectedRows();
                GridCheckHeaderState();
                AllowButtonSelector();
            }
            else
                _wasSelectedInConextMenu = false;

            RaiseDeclarationListItemActivated();
        }

        private void RowCheckedChange(object obj, bool selected)
        {
            var item = obj as TaxpayerAssignmentModel;
            if (item != null)
                item.Selected = selected;
            AllowButtonSelector();
        }

        private void AllowButtonSelector()
        {
            _pageNavigatorDest.SetSelectedCount(_presenter.GridPresenterDest.Rows.Where(r => r.Selected).Count());
            _dependedGridsSelector.SetMoveDstToSrcEnabled(_presenter.GridPresenterDest.Rows.Any(x => x.Selected));
        }

        private void GridCellChange(object sender, CellEventArgs e)
        {
            e.Cell.Row.Update();
            SetCheckedRows();
            AllowButtonSelector();
            RaiseDeclarationCheckSelected();
        }

        bool _wasSelectedInConextMenu = false;

        private void GridBeforeShowContextMenu(object sender, CommonComponents.Uc.Infrastructure.Interface.EventArgs<UltraGridRow> e)
        {
            _dependedGridsSelector.DestGrid.Grid.AfterSelectChange -= GridAfterSelectChange;
            _dependedGridsSelector.DestGrid.Grid.CellChange -= GridCellChange;

            _wasSelectedInConextMenu = _multiSelectedRow.ClearAllSelectedRowExceptOne(e.Data);
            _multiSelectedRow.SetSelectedRows(TypeHelper<TaxpayerAssignmentModel>.GetMemberName(t => t.Selected));
            AllowButtonSelector();

            _dependedGridsSelector.DestGrid.Grid.AfterSelectChange += GridAfterSelectChange;
            _dependedGridsSelector.DestGrid.Grid.CellChange += GridCellChange;
        }

        #endregion

        # region MultiSelectedRow

        private MultiSelectedRow _multiSelectedRow = null;

        private void SetSelectedRows()
        {
            _dependedGridsSelector.DestGrid.Grid.CellChange -= GridCellChange;
            _dependedGridsSelector.DestGrid.Grid.AfterSelectChange -= GridAfterSelectChange;

            _multiSelectedRow.SetSelectedRows(TypeHelper<TaxpayerAssignmentModel>.GetMemberName(t => t.Selected));

            RaiseDeclarationCheckSelected();

            _dependedGridsSelector.DestGrid.Grid.AfterSelectChange += GridAfterSelectChange;
            _dependedGridsSelector.DestGrid.Grid.CellChange += GridCellChange;
        }

        private void SetCheckedRows()
        {
            _dependedGridsSelector.DestGrid.Grid.AfterSelectChange -= GridAfterSelectChange;

            _multiSelectedRow.SetCheckedRows(TypeHelper<TaxpayerAssignmentModel>.GetMemberName(t => t.Selected));

            _dependedGridsSelector.DestGrid.Grid.AfterSelectChange += GridAfterSelectChange;
        }

        private void AllCheckedActionExtended(CheckState checkState)
        {
            _dependedGridsSelector.DestGrid.Grid.AfterSelectChange -= GridAfterSelectChange;

            _multiSelectedRow.AllCheckedActionExtended(checkState);

            _dependedGridsSelector.DestGrid.Grid.AfterSelectChange += GridAfterSelectChange;
        }

        private void GridCheckHeaderState()
        {
            _checkColumn.HeaderCheckedChange -= GridHeaderCheckedChange;

            _multiSelectedRow.CheckHeaderState(TypeHelper<TaxpayerAssignmentModel>.GetMemberName(t => t.Selected));

            _checkColumn.HeaderCheckedChange += GridHeaderCheckedChange;
        }

        public void UnSelectAllRows()
        {
            _dependedGridsSelector.DestGrid.Grid.CellChange -= GridCellChange;
            _dependedGridsSelector.DestGrid.Grid.AfterSelectChange -= GridAfterSelectChange;

            _multiSelectedRow.UnCheckedAndUnSelectAllRows(TypeHelper<TaxpayerAssignmentModel>.GetMemberName(t => t.Selected));

            _dependedGridsSelector.DestGrid.Grid.AfterSelectChange += GridAfterSelectChange;
            _dependedGridsSelector.DestGrid.Grid.CellChange += GridCellChange;
        }

        # endregion
    }
}
