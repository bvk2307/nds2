﻿using System.ComponentModel;
using Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.DeclarationAssignment.InspectorSettings
{
    public class InspectorModel
    {
        private readonly InspectorSonoAssignment _inspector;

        public InspectorModel(InspectorSonoAssignment inspector)
        {
            _inspector = inspector;
        }

        [DisplayName("")]
        [Description("Флаг выбора записи")]
        public bool Selected
        {
            get;
            set;
        }

        [DisplayName("ФИО")]
        [Description("ФИО инспектора")]
        public string Name { get { return _inspector.Name; } }

        [DisplayName("Табельный №")]
        [Description("Табельный номер инспектора")]
        public string EmployeeNum { get { return _inspector.EmployeeNum; } }

        public InspectorSonoAssignment GetInspector()
        {
            return _inspector;
        }

    }
}
