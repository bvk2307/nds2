using System.ComponentModel;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.DeclarationAssignment.InspectorSettings
{
    public enum ViewImage
    {
        [Description("")]
        None = -1,
        Edit,
        EditGray,
        Disabled
    }
}