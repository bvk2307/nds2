﻿using System;
using Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.DeclarationAssignment.InspectorSettings
{
    public class WorkloadUpdateEventArgs : EventArgs
    {
        private readonly InspectorWorkload _workload;
        public WorkloadUpdateEventArgs(InspectorWorkload workload)
        {
            _workload = workload;
        }

        public InspectorWorkload Workload
        {
            get { return _workload; }
        }
    }
}
