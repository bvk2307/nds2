﻿using System;
using Luxoft.NDS2.Client.UI.Controls.DependedGridsSelector;
using Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.DeclarationAssignment.InspectorSettings
{
    interface IInspectorSettingsView : IDependedGridsSelector
    {
        void SetDefaultWorkLoad(DefaultInspectorWorkLoad defaultWorkLoad);

        void ShowWarning(string message);

        void ShowNotification(string message);

        DefaultInspectorWorkLoad GetDefaultWorkload();

        event EventHandler<WorkloadUpdateEventArgs> WorkloadUpdate;

        event EventHandler SelectionChanged;

        event EventHandler<DefaultWorkLoadUpdateEventArgs> DefaultWorkLoadUpdate;
    }
}
