﻿using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using CommonComponents.Uc.Infrastructure.Interface.UI;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;
using Luxoft.NDS2.Client.UI.Controls.DependedGridsSelector;
using Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment;
using Luxoft.NDS2.Common.Contracts.Helpers;
using Microsoft.Practices.CompositeUI.Commands;
using Microsoft.Practices.ObjectBuilder;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.DeclarationAssignment.InspectorSettings
{
    public partial class View : BaseView, IInspectorSettingsView
    {
        private Presenter _presenter;


        [CreateNew]
        public Presenter Presenter
        {
            set
            {
                _presenter = value;
                _presenter.PresentationContext = _presentationContext;
                _presenter.View = this;
                WorkItem = _presenter.WorkItem;
                _presenter.OnObjectCreated();
                InitializeRibbon();
            }
        }


        public View()
        {
            InitializeComponent();
        }

        public View(PresentationContext ctx)
            : base(ctx)
        {
            InitializeComponent();

            InitSourceGrid();

            InitDestGrid();
        }

        public event EventHandler<WorkloadUpdateEventArgs> WorkloadUpdate;
        public event EventHandler SelectionChanged;
        public event EventHandler<DefaultWorkLoadUpdateEventArgs> DefaultWorkLoadUpdate;
        public event EventHandler RefreshData;

        public event EventHandler<MoveEventArgs> ObjectsMoving
        {
            add { _dependedGridsSelector.ObjectsMoving += value; }
            remove { _dependedGridsSelector.ObjectsMoving -= value; }
        }

        public void SetMoveSrcToDstEnabled(bool enabled)
        {
            _dependedGridsSelector.SetMoveSrcToDstEnabled(enabled);
        }

        public void SetMoveDstToSrcEnabled(bool enabled)
        {
            _dependedGridsSelector.SetMoveDstToSrcEnabled(enabled);
        }

        public void SetSouceGridCollection(IEnumerable collection)
        {
            _dependedGridsSelector.SetSouceGridCollection(collection);
        }

        public void SetDestGridCollection(IEnumerable collection)
        {
            _dependedGridsSelector.SetDestGridCollection(collection);
        }

        public void SetDefaultWorkLoad(DefaultInspectorWorkLoad defaultWorkLoad)
        {
            _paymentNumericEditor.Value = defaultWorkLoad.HasPaymentPermission
                                              ? (decimal?)defaultWorkLoad.MaxPaymentCapacity
                                              : null;
            _compensationNumericEditor.Value = defaultWorkLoad.HasCompensationPermission
                                                   ? (decimal?)defaultWorkLoad.MaxCompenationCapacity
                                                   : null;
        }


        public DefaultInspectorWorkLoad GetDefaultWorkload()
        {
            var defaultWorkLoad = new DefaultInspectorWorkLoad();
            var paymentValue = _paymentNumericEditor.Value == DBNull.Value ? null : (decimal?)_paymentNumericEditor.Value;
            var compensationValue = _compensationNumericEditor.Value == DBNull.Value ? null : (decimal?)_compensationNumericEditor.Value;
            defaultWorkLoad.HasPaymentPermission = paymentValue.GetValueOrDefault() != 0;
            defaultWorkLoad.MaxPaymentCapacity = paymentValue.GetValueOrDefault();
            defaultWorkLoad.HasCompensationPermission = compensationValue.GetValueOrDefault() != 0;
            defaultWorkLoad.MaxCompenationCapacity = compensationValue.GetValueOrDefault();
            defaultWorkLoad.IsActive = defaultWorkLoad.HasPaymentPermission || defaultWorkLoad.HasCompensationPermission;
            return defaultWorkLoad;
        }

        [CommandHandler("cmdRefresh")]
        public virtual void RefreshtnClick(object sender, EventArgs e)
        {
            OnRefreshData();
        }


        private void InitializeRibbon()
        {
            IUcResourceManagersService resourceManagersService = _presenter.WorkItem.Services.Get<IUcResourceManagersService>(true);
            if (!resourceManagersService.Contains(ResourceManagerNDS2.NDS2ClientResources))
            {
                resourceManagersService.Add(ResourceManagerNDS2.NDS2ClientResources, Properties.Resources.ResourceManager);
            }

            //закладка
            UcRibbonTabContext tabNavigator = new UcRibbonTabContext(_presenter.PresentationContext, "NDS2Result")
            {
                Text = _presenter.PresentationContext.WindowTitle,
                ToolTipText = _presenter.PresentationContext.WindowTitle,
                Visible = true,
                Order = 1
            };

            UcRibbonGroupContext groupNavigation1 = tabNavigator.AddGroup("NDS2DeclarationListManage");
            groupNavigation1.Text = "Функции";
            groupNavigation1.Visible = true;

            var btnRefresh = new UcRibbonButtonToolContext(_presenter.PresentationContext, "btnRefresh", "cmdRefresh")
            {
                Text = "Обновить",
                ToolTipText = "Обновить",
                ResourceManagerName = ResourceManagerNDS2.NDS2ClientResources,
                LargeImageName = "update",
                SmallImageName = "update",
                Enabled = true
            };



            groupNavigation1.ToolList.AddRange(
                new UcRibbonToolInstanceSettings[] { 
                    new UcRibbonToolInstanceSettings(btnRefresh.ItemName, UcRibbonToolSize.Large),
                });

            //общее
            _presentationContext.UiVisualizationCollection.AddRange(new VisualizationElementBase[]
            {
                btnRefresh,
                tabNavigator
            });

            _presentationContext.ActiveMenuTab = tabNavigator.ItemName;
        }

        private void _saveBtn_Click(object sender, EventArgs e)
        {
            var defaultWorkLoad = GetDefaultWorkload();
            OnDefaultWorkloadUpdate(new DefaultWorkLoadUpdateEventArgs(defaultWorkLoad));
        }


        private void _clearBtn_Click(object sender, EventArgs e)
        {
            var defaultWorkLoad = new DefaultInspectorWorkLoad
                                      {
                                          HasPaymentPermission = false,
                                          MaxPaymentCapacity = 0,
                                          HasCompensationPermission = false,
                                          MaxCompenationCapacity = 0,
                                          IsActive = false
                                      };
            OnDefaultWorkloadUpdate(new DefaultWorkLoadUpdateEventArgs(defaultWorkLoad));
        }

        private void Grid_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            var wl = ((InspectorWorkloadModel)e.Row.ListObject).GetInspectorWorkLoad();
            if (!wl.Inspector.IsActive)
            {
                foreach (var cell in e.Row.Cells)
                {
                    if (cell.Column.Key != "IS_IN_PROCESS" && cell.Column.Key != "StatusImage")
                    {
                        cell.Activation = Activation.Disabled;
                    }
                }
            }
        }

        private void Grid_CellChange(object sender, EventArgs e)
        {
            var grid = ((UltraGrid)sender);
            if (grid.ActiveCell.Column.Key == "IS_IN_PROCESS")
            {
                grid.PerformAction(UltraGridAction.ExitEditMode);
                grid.UpdateData();
                OnSelectionChange();
            }
        }

        private void Grid_ClickCell(object sender, ClickCellEventArgs e)
        {
            if (e.Cell.Column.Key == "EditImage" && e.Cell.Activation != Activation.Disabled)
            {
                var wl = ((InspectorWorkloadModel)e.Cell.Row.ListObject).GetInspectorWorkLoad();
                var editInspWlForm = new EditInspectorWorkloadForm();
                editInspWlForm.SetWorkload(wl);
                var dialogRes = editInspWlForm.ShowDialog(ParentForm);
                if (dialogRes == System.Windows.Forms.DialogResult.OK)
                {
                    OnWorkloadUpdate(new WorkloadUpdateEventArgs(wl));
                }
            }
        }

        private void Grid_ClickCellButton(object sender, CellEventArgs e)
        {
            var wl = ((InspectorWorkloadModel)e.Cell.Row.ListObject).GetInspectorWorkLoad();
            wl.IsPaused = !wl.IsPaused;
            OnWorkloadUpdate(new WorkloadUpdateEventArgs(wl));
        }

        private void OnSelectionChange()
        {
            var tmp = SelectionChanged;
            if (tmp != null)
            {
                tmp(this, EventArgs.Empty);
            }
        }

        private void OnWorkloadUpdate(WorkloadUpdateEventArgs e)
        {
            var tmp = WorkloadUpdate;
            if (tmp != null)
            {
                tmp(this, e);
            }
        }


        private void OnDefaultWorkloadUpdate(DefaultWorkLoadUpdateEventArgs e)
        {
            var tmp = DefaultWorkLoadUpdate;
            if (tmp != null)
            {
                tmp(this, e);
            }
        }


        private void OnRefreshData()
        {
            var tmp = RefreshData;
            if (tmp != null)
            {
                tmp(this, EventArgs.Empty);
            }
        }


        private void InitSourceGrid()
        {
            _dependedGridsSelector.SourceHeaderHtmlText = "Сотрудники инспекции с правами доступа \n к системе АСК НДС-2 с ролью \"Инспектор\"";
            var setup = new GridColumnSetup();
            var helper = new ColumnHelper<InspectorModel>(_dependedGridsSelector.SourceGrid);

            setup.Columns.Add(helper.CreateCheckColumn(model => model.Selected).Configure(col =>
            {
                col.DisableFilter =
                    true;
                col.DisableSort =
                    true;
                col.HeaderToolTip = "Флаг выбора записи";
                col.HeaderCheckedChange += (checkState) =>
                {
                    var columnNameSelected = TypeHelper<InspectorWorkloadModel>.GetMemberName(t => t.Selected);
                    bool storeInUndoCheck = false;
                    foreach (var itemRow in _dependedGridsSelector.SourceGrid.Grid.Rows)
                    {
                        if (checkState == System.Windows.Forms.CheckState.Checked)
                            itemRow.Cells[columnNameSelected].SetValue(true, storeInUndoCheck);
                        if (checkState == System.Windows.Forms.CheckState.Unchecked)
                            itemRow.Cells[columnNameSelected].SetValue(false, storeInUndoCheck);
                        itemRow.Update();
                    }
                    OnSelectionChange();
                };
            }));
            setup.Columns.Add(
                helper
                    .CreateTextColumn(model => model.EmployeeNum, 1, 100)
                    .Configure(col =>
                    {
                        col.DisableFilter = true;
                    }));
            setup.Columns.Add(
                helper
                    .CreateTextColumn(model => model.Name, 1, 150)
                    .Configure(col =>
                    {
                        col.DisableFilter = true;
                    }));

            _dependedGridsSelector.SourceGrid.InitColumns(setup);
            _dependedGridsSelector.SourceGrid.Grid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            _dependedGridsSelector.SourceGrid.Grid.DisplayLayout.Override.ActiveRowAppearance.Reset();
            _dependedGridsSelector.SourceGrid.Grid.DisplayLayout.Override.ActiveRowCellAppearance.Reset();
            _dependedGridsSelector.SourceGrid.Grid.DisplayLayout.Override.ActiveCellAppearance.Reset();
            _dependedGridsSelector.SourceGrid.Grid.DisplayLayout.Override.SelectTypeRow = SelectType.None;
            _dependedGridsSelector.SourceGrid.Grid.DisplayLayout.Override.SelectTypeCell = SelectType.None;
            _dependedGridsSelector.SourceGrid.Grid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            _dependedGridsSelector.SourceGrid.Grid.CellChange += Grid_CellChange;
        }

        private void InitDestGrid()
        {
            _dependedGridsSelector.DestGrid.AllowRowFiltering = DefaultableBoolean.False;
            _dependedGridsSelector.DestGrid.PanelLoadingVisible = false;
            _dependedGridsSelector.DestGrid.PanelPagesVisible = false;
            _dependedGridsSelector.DestinationHeaderHtmlText = "Список сотрудников для закрепления НД";
            var accessIcons = new Dictionary<ViewImage, Bitmap>
                            {
                                {ViewImage.None, null},
                                {ViewImage.Disabled, Properties.Resources.disabled}
                            };
            var editIcons = new Dictionary<ViewImage, Bitmap>
                            {
                                {ViewImage.Edit, Properties.Resources.edit_yellow_Best},
                                {ViewImage.EditGray, Properties.Resources.edit_gray_22x22},
                            };

            var acessTooltips = new Dictionary<string, string>
                                                               {
                                                                   {ViewImage.None.ToString(), ""},
                                                                   {ViewImage.Disabled.ToString(), "У инспектора нет доступа"}  
                                                               };

            var setup = new GridColumnSetup();
            var helper = new ColumnHelper<InspectorWorkloadModel>(_dependedGridsSelector.DestGrid);
            setup.Columns.Add(helper.CreateCheckColumn(model => model.Selected).Configure(col =>
                {
                    col.DisableFilter = true;
                    col.HeaderCheckedChange += (checkState) =>
                    {
                        var columnNameSelected = TypeHelper<InspectorWorkloadModel>.GetMemberName(t => t.Selected);
                        const bool storeInUndoCheck = false;
                        foreach (var itemRow in _dependedGridsSelector.DestGrid.Grid.Rows)
                        {
                            if (checkState == System.Windows.Forms.CheckState.Checked)
                            {
                                itemRow.Cells[columnNameSelected].SetValue(true, storeInUndoCheck);
                            }
                            if (checkState == System.Windows.Forms.CheckState.Unchecked)
                            {
                                itemRow.Cells[columnNameSelected].SetValue(false, storeInUndoCheck);
                            }
                            itemRow.Update();
                        }
                        OnSelectionChange();
                    };
                }));

            setup.Columns.Add(helper.CreateImageColumn(model => model.StatusImage, this, accessIcons).Configure(col =>
                                                                                                              {
                                                                                                                  col.DisableFilter = true;
                                                                                                                  col.DisableMoving = true;
                                                                                                                  col.Width = 10;
                                                                                                                  col.ToolTipViewer = new DictionaryToolTipViewer(acessTooltips);
                                                                                                              }));
            setup.Columns.Add(helper.CreateImageColumn(model => model.EditImage, this, editIcons).Configure(col =>
                                                                                                            {
                                                                                                                col.DisableFilter = true;
                                                                                                                col.DisableMoving = true;
                                                                                                                col.Width = 10;
                                                                                                            }));
            setup.Columns.Add(helper.CreateTextColumn(model => model.PausedText, 1, 15)
                                  .Configure(col =>
                                                 {
                                                     col.DisableFilter = true;
                                                     col.DisableMoving = true;
                                                 }));
            setup.Columns.Add(
                helper
                    .CreateTextColumn(model => model.EmployeeNum, 1, 100)
                    .Configure(col =>
                                   {
                                       col.DisableFilter = true;
                                       col.DisableMoving = true;
                                   }));
            setup.Columns.Add(
                helper
                    .CreateTextColumn(model => model.Name, 1, 150)
                    .Configure(col =>
                                   {
                                       col.DisableFilter = true;
                                       col.DisableMoving = true;
                                   }));
            setup.Columns.Add(
                helper
                    .CreateTextColumn(model => model.MaxCompenationCapacity, 1, 60)
                    .Configure(col =>
                                   {
                                       col.DisableFilter = true;
                                       col.DisableMoving = true;
                                   }));
            setup.Columns.Add(
                helper
                    .CreateTextColumn(model => model.MaxPaymentCapacity, 1, 60)
                    .Configure(col =>
                                   {
                                       col.DisableFilter = true;
                                       col.DisableMoving = true;
                                   }));
            _dependedGridsSelector.DestGrid.InitColumns(setup);
            _dependedGridsSelector.DestGrid.Grid.ClickCellButton += Grid_ClickCellButton;
            _dependedGridsSelector.DestGrid.Grid.ClickCell += Grid_ClickCell;
            _dependedGridsSelector.DestGrid.Grid.CellChange += Grid_CellChange;
            _dependedGridsSelector.DestGrid.Grid.InitializeRow += Grid_InitializeRow;
            _dependedGridsSelector.DestGrid.Grid.DisplayLayout.Override.ActiveRowAppearance.Reset();
            _dependedGridsSelector.DestGrid.Grid.DisplayLayout.Override.ActiveRowCellAppearance.Reset();
            _dependedGridsSelector.DestGrid.Grid.DisplayLayout.Override.ActiveCellAppearance.Reset();
            _dependedGridsSelector.DestGrid.Grid.DisplayLayout.Override.SelectTypeRow = SelectType.None;
            _dependedGridsSelector.DestGrid.Grid.DisplayLayout.Override.SelectTypeCell = SelectType.None;
            _dependedGridsSelector.DestGrid.Grid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            _dependedGridsSelector.DestGrid.Grid.DisplayLayout.Bands[0].Columns["StatusImage"].Header.Appearance.Image = Properties.Resources.disabled;
            _dependedGridsSelector.DestGrid.Grid.DisplayLayout.Bands[0].Columns["StatusImage"].Header.Appearance.ImageHAlign = HAlign.Center;
            _dependedGridsSelector.DestGrid.Grid.DisplayLayout.Bands[0].Columns["EditImage"].Header.Appearance.Image = Properties.Resources.edit_yellow_Best;
            _dependedGridsSelector.DestGrid.Grid.DisplayLayout.Bands[0].Columns["EditImage"].Header.Appearance.ImageHAlign = HAlign.Center;
        }

        public DataGridView DestinationGrid { get { return _dependedGridsSelector.DestGrid; } }

        public DataGridView SourceGrid { get { return _dependedGridsSelector.SourceGrid; } }

    }
}
