﻿using System.ComponentModel;
using Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.DeclarationAssignment.InspectorSettings
{
    public class InspectorWorkloadModel
    {
        private readonly InspectorWorkload _workload;
        public InspectorWorkloadModel(InspectorWorkload workload)
        {
            _workload = workload;
        }

        [DisplayName("")]
        [Description("Флаг выбора записи")]
        public bool Selected { get; set; }

        [DisplayName("ФИО")]
        [Description("ФИО инспектора")]
        public string Name { get { return _workload.Inspector.Name; } }

        [DisplayName("Табельный №")]
        [Description("Табельный номер инспектора")]
        public string EmployeeNum
        {
            get { return _workload.Inspector.EmployeeNum; }
        }


        [DisplayName("Cумма расхождений по АТ")]
        [Description("Сумма расхождений по автотребованиям по СФ НД к уплате(нулевой НД)")]
        public decimal? MaxPaymentCapacity
        {
            get { return _workload.HasPaymentPermission ? _workload.MaxPaymentCapacity : (decimal?)null; }
        }


        [DisplayName("НДС к возмещению")]
        [Description("Сумма НДС к возмещению")]
        public decimal? MaxCompenationCapacity
        {
            get { return _workload.HasCompensationPermission ? _workload.MaxCompenationCapacity : (decimal?)null; }
        }

        [DisplayName("")]
        [Description("Изменение параметров")]
        public ViewImage EditImage
        {
            get { return _workload.Inspector.IsActive ? ViewImage.Edit : ViewImage.EditGray; }
        }

        [DisplayName("")]
        [Description("Наличие доступа")]
        public ViewImage StatusImage
        {
            get { return _workload.Inspector.IsActive ? ViewImage.None : ViewImage.Disabled; }
        }

        [DisplayName("Автоназначение НД")]
        [Description("Автоназначение НД за сотрудником")]
        public string PausedText
        {
            get
            {
                return _workload.IsPaused ? "Отключено" : "Включено";
            }
        }

        public InspectorWorkload GetInspectorWorkLoad()
        {
            return _workload;
        }
    }
}
