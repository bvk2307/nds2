﻿namespace Luxoft.NDS2.Client.UI.SettingsMRR.DeclarationAssignment.InspectorSettings
{
    partial class View
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.UltraWinToolTip.UltraToolTipInfo ultraToolTipInfo1 = new Infragistics.Win.UltraWinToolTip.UltraToolTipInfo("Сбросить пороговые значения", Infragistics.Win.ToolTipImage.Default, null, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.UltraWinToolTip.UltraToolTipInfo ultraToolTipInfo2 = new Infragistics.Win.UltraWinToolTip.UltraToolTipInfo("Сохранить пороговые значения", Infragistics.Win.ToolTipImage.Default, null, Infragistics.Win.DefaultableBoolean.Default);
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this._defaultSettingsGrpBox = new System.Windows.Forms.GroupBox();
            this._clearBtn = new Infragistics.Win.Misc.UltraButton();
            this._saveBtn = new Infragistics.Win.Misc.UltraButton();
            this._paymentNumericEditor = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this._compensationNumericEditor = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this._dependedGridsSelector = new Luxoft.NDS2.Client.UI.Controls.DependedGridsSelector.DependedDataGridsSelector();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this._ultraToolTipManager = new Infragistics.Win.UltraWinToolTip.UltraToolTipManager(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this._defaultSettingsGrpBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._paymentNumericEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._compensationNumericEditor)).BeginInit();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = System.Drawing.Color.Transparent;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this._defaultSettingsGrpBox);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this._dependedGridsSelector);
            this.splitContainer1.Size = new System.Drawing.Size(801, 573);
            this.splitContainer1.SplitterDistance = 140;
            this.splitContainer1.TabIndex = 0;
            // 
            // _defaultSettingsGrpBox
            // 
            this._defaultSettingsGrpBox.Controls.Add(this._clearBtn);
            this._defaultSettingsGrpBox.Controls.Add(this._saveBtn);
            this._defaultSettingsGrpBox.Controls.Add(this._paymentNumericEditor);
            this._defaultSettingsGrpBox.Controls.Add(this._compensationNumericEditor);
            this._defaultSettingsGrpBox.Controls.Add(this.label2);
            this._defaultSettingsGrpBox.Controls.Add(this.label1);
            this._defaultSettingsGrpBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this._defaultSettingsGrpBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._defaultSettingsGrpBox.Location = new System.Drawing.Point(0, 0);
            this._defaultSettingsGrpBox.Name = "_defaultSettingsGrpBox";
            this._defaultSettingsGrpBox.Size = new System.Drawing.Size(801, 140);
            this._defaultSettingsGrpBox.TabIndex = 0;
            this._defaultSettingsGrpBox.TabStop = false;
            this._defaultSettingsGrpBox.Text = "Пороговые значения, задаваемые по умолчанию при выборе инспектора";
            // 
            // _clearBtn
            // 
            this._clearBtn.Location = new System.Drawing.Point(551, 29);
            this._clearBtn.Name = "_clearBtn";
            this._clearBtn.Size = new System.Drawing.Size(75, 23);
            this._clearBtn.TabIndex = 13;
            this._clearBtn.Text = "Сбросить";
            ultraToolTipInfo1.ToolTipText = "Сбросить пороговые значения";
            this._ultraToolTipManager.SetUltraToolTip(this._clearBtn, ultraToolTipInfo1);
            this._clearBtn.Click += new System.EventHandler(this._clearBtn_Click);
            // 
            // _saveBtn
            // 
            this._saveBtn.Location = new System.Drawing.Point(470, 29);
            this._saveBtn.Name = "_saveBtn";
            this._saveBtn.Size = new System.Drawing.Size(75, 23);
            this._saveBtn.TabIndex = 12;
            this._saveBtn.Text = "Сохранить";
            ultraToolTipInfo2.ToolTipText = "Сохранить пороговые значения";
            this._ultraToolTipManager.SetUltraToolTip(this._saveBtn, ultraToolTipInfo2);
            this._saveBtn.Click += new System.EventHandler(this._saveBtn_Click);
            // 
            // _paymentNumericEditor
            // 
            this._paymentNumericEditor.FormatString = "#,###,###,##0";
            this._paymentNumericEditor.Location = new System.Drawing.Point(256, 74);
            this._paymentNumericEditor.Margin = new System.Windows.Forms.Padding(0);
            this._paymentNumericEditor.MaskInput = "{LOC}n,nnn,nnn,nnn";
            this._paymentNumericEditor.MaxValue = new decimal(new int[] {
            1410065407,
            2,
            0,
            0});
            this._paymentNumericEditor.MinValue = 0;
            this._paymentNumericEditor.Name = "_paymentNumericEditor";
            this._paymentNumericEditor.Nullable = true;
            this._paymentNumericEditor.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Decimal;
            this._paymentNumericEditor.PromptChar = ' ';
            this._paymentNumericEditor.Size = new System.Drawing.Size(201, 21);
            this._paymentNumericEditor.TabIndex = 9;
            // 
            // _compensationNumericEditor
            // 
            this._compensationNumericEditor.FormatString = "#,###,###,##0";
            this._compensationNumericEditor.Location = new System.Drawing.Point(256, 31);
            this._compensationNumericEditor.Margin = new System.Windows.Forms.Padding(0);
            this._compensationNumericEditor.MaskInput = "{LOC}n,nnn,nnn,nnn";
            this._compensationNumericEditor.MaxValue = new decimal(new int[] {
            1410065407,
            2,
            0,
            0});
            this._compensationNumericEditor.MinValue = 0;
            this._compensationNumericEditor.Name = "_compensationNumericEditor";
            this._compensationNumericEditor.Nullable = true;
            this._compensationNumericEditor.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Decimal;
            this._compensationNumericEditor.PromptChar = ' ';
            this._compensationNumericEditor.Size = new System.Drawing.Size(201, 21);
            this._compensationNumericEditor.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(31, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(222, 26);
            this.label2.TabIndex = 1;
            this.label2.Text = "Сумма расхождений по автотребованиям \r\nНД к уплате (нулевой НД):";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(150, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Сумма НДС к возмещению:";
            // 
            // _dependedGridsSelector
            // 
            this._dependedGridsSelector.BackColor = System.Drawing.Color.Transparent;
            this._dependedGridsSelector.DestinationHeaderHtmlText = "";
            this._dependedGridsSelector.Dock = System.Windows.Forms.DockStyle.Fill;
            this._dependedGridsSelector.Location = new System.Drawing.Point(0, 0);
            this._dependedGridsSelector.MoveLeftToolTip = "Удалить инспектора";
            this._dependedGridsSelector.MoveRightToolTip = "Добавить инспектора";
            this._dependedGridsSelector.Name = "_dependedGridsSelector";
            this._dependedGridsSelector.Size = new System.Drawing.Size(801, 429);
            this._dependedGridsSelector.SourceGridAreaRatio = new decimal(new int[] {
            2018795489,
            -283722080,
            178670020,
            1835008});
            this._dependedGridsSelector.SourceHeaderHtmlText = "";
            this._dependedGridsSelector.TabIndex = 0;
            // 
            // ultraPanel1
            // 
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.splitContainer1);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel1.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(801, 573);
            this.ultraPanel1.TabIndex = 1;
            // 
            // _ultraToolTipManager
            // 
            this._ultraToolTipManager.ContainingControl = this;
            // 
            // View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ultraPanel1);
            this.Name = "View";
            this.Size = new System.Drawing.Size(801, 573);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this._defaultSettingsGrpBox.ResumeLayout(false);
            this._defaultSettingsGrpBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._paymentNumericEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._compensationNumericEditor)).EndInit();
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox _defaultSettingsGrpBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor _paymentNumericEditor;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor _compensationNumericEditor;
        private Infragistics.Win.Misc.UltraButton _saveBtn;
        private Infragistics.Win.Misc.UltraButton _clearBtn;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Controls.DependedGridsSelector.DependedDataGridsSelector _dependedGridsSelector;
        private Infragistics.Win.UltraWinToolTip.UltraToolTipManager _ultraToolTipManager;
    }
}
