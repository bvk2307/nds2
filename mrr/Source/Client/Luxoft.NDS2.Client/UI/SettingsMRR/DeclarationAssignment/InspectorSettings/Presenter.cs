﻿using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2;
using Luxoft.NDS2.Client.UI.Controls.DependedGridsSelector;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Providers;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment;
using Luxoft.NDS2.Common.Contracts.Services.DeclarationAssignment;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.DeclarationAssignment.InspectorSettings
{
    public class Presenter : BasePresenter<View>
    {
        private IWorkloadService _declAssignmentService;
        private IInspectorService _inspectorService;
        private IWorkloadDefaultsService _workloadDefaultsService;
        private IInspectorSettingsView _inspectorSettingsView;
        private DefaultInspectorWorkLoad _defaultInspectorWorkLoad;
        private BindingList<InspectorModel> _source;
        private BindingList<InspectorWorkloadModel> _dest;
        private ClientGridPresenter<InspectorModel> _srcGridPresenter;
        private ClientGridPresenter<InspectorWorkloadModel> _dstGridPresenter;

        public override void OnObjectCreated()
        {
            base.OnObjectCreated();
            _declAssignmentService = GetServiceProxy<IWorkloadService>();
            _inspectorService = GetServiceProxy<IInspectorService>();
            _workloadDefaultsService = GetServiceProxy<IWorkloadDefaultsService>();
            _inspectorSettingsView = View;
            _inspectorSettingsView.ObjectsMoving += _inspectorSettingsView_ObjectsMoving;
            _inspectorSettingsView.WorkloadUpdate += _inspectorSettingsView_WorkloadUpdate;
            _inspectorSettingsView.SelectionChanged += _inspectorSettingsView_SelectionChanged;
            _inspectorSettingsView.DefaultWorkLoadUpdate += _inspectorSettingsView_DefaultWorkLoadUpdate;
            View.RefreshData += ViewRefreshData;
            ExecuteServiceCall(_inspectorService.GetFreeInspectors, OnGetSourceCollection);
            ExecuteServiceCall(_declAssignmentService.GetActiveWorkLoads, OnGetDestCollection);
            ExecuteServiceCall(_workloadDefaultsService.GetDefaultWorkLoad, OnGetDefaultWorkload);
            OnSelectionChanged();
            _srcGridPresenter = new ClientGridPresenter<InspectorModel>(View.SourceGrid);
            _srcGridPresenter.Init(new CommonSettingsProvider(WorkItem, string.Format("{0}_InitializeSrcGrid", GetType())));
            _dstGridPresenter = new ClientGridPresenter<InspectorWorkloadModel>(View.DestinationGrid);
            _dstGridPresenter.Init(new CommonSettingsProvider(WorkItem, string.Format("{0}_InitializeDstGrid", GetType())));
        }

        void ViewRefreshData(object sender, EventArgs e)
        {
            ExecuteServiceCall(_inspectorService.GetFreeInspectors, OnGetSourceCollection);
            ExecuteServiceCall(_declAssignmentService.GetActiveWorkLoads, OnGetDestCollection);
            ExecuteServiceCall(_workloadDefaultsService.GetDefaultWorkLoad, OnGetDefaultWorkload);
            OnSelectionChanged();
        }

        private void _inspectorSettingsView_DefaultWorkLoadUpdate(object sender, DefaultWorkLoadUpdateEventArgs e)
        {
            ExecuteServiceCall(() => _workloadDefaultsService.SetDefaultWorkLoad(e.DefaultWorkload));
            ExecuteServiceCall(_workloadDefaultsService.GetDefaultWorkLoad, OnGetDefaultWorkload);
        }


        private void _inspectorSettingsView_SelectionChanged(object sender, EventArgs e)
        {
            OnSelectionChanged();
        }

        private void _inspectorSettingsView_WorkloadUpdate(object sender, WorkloadUpdateEventArgs e)
        {
            ExecuteServiceCall(() => _declAssignmentService.UpdateWorkLoad(e.Workload));
        }

        private void _inspectorSettingsView_ObjectsMoving(object sender, MoveEventArgs e)
        {
            if (CanMoveObjects())
            {
                switch (e.Direction)
                {
                    case MoveDirection.SourceToDest:
                        CreateInspectorWorlkLoad();
                        break;
                    case MoveDirection.DestToSource:
                        if (View.ShowQuestion("Удаление инспекторов", "Удалить выбранных инспекторов и их настройки из списка инспекторов, доступных для закрепления НД?") == System.Windows.Forms.DialogResult.Yes)
                        {
                            ClearInspectorWorkLoad();
                        }
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
                OnSelectionChanged();
            }
        }

        private bool CanMoveObjects()
        {
            bool canMoveObjects = false;
            ExecuteServiceCall(_workloadDefaultsService.GetDefaultWorkLoad, storedDefaultWorkloadResult =>
            {
                var viewDefaultWorkload = _inspectorSettingsView.GetDefaultWorkload();
                var storedDefaultWorkload = storedDefaultWorkloadResult.Result ?? new DefaultInspectorWorkLoad();
                canMoveObjects = AreEqual(storedDefaultWorkload, viewDefaultWorkload);
                if (!canMoveObjects)
                {
                    _inspectorSettingsView.
                        ShowWarning(
                            "На форме имеются не сохраненные измения нагрузки на инспектора по умолчанию. " +
                            "Пожалуйста сохраните изменения перед выполнение выбранного действия.");
                }
            });
            return canMoveObjects;
        }

        private static bool AreEqual(DefaultInspectorWorkLoad wl1, DefaultInspectorWorkLoad wl2)
        {
            return wl1.HasCompensationPermission == wl2.HasCompensationPermission &&
                   wl1.HasPaymentPermission == wl2.HasPaymentPermission &&
                   wl1.IsActive == wl2.IsActive &&
                   wl1.MaxCompenationCapacity == wl2.MaxCompenationCapacity &&
                   wl1.MaxPaymentCapacity == wl2.MaxPaymentCapacity;
        }

        private void ClearInspectorWorkLoad()
        {
            IEnumerable<InspectorWorkload> inspectors =
                _dest.Where(i => i.Selected).Select(wl => wl.GetInspectorWorkLoad());
            ExecuteServiceCall(() => _declAssignmentService.ClearInspectorsWorkload(inspectors.ToList()), OnWorkloadClear);
        }

        private void CreateInspectorWorlkLoad()
        {
            List<InspectorWorkload> inspectorWorkloads =
                _source.Where(i => i.Selected).Select(i => new InspectorWorkload
                                                               {
                                                                   Inspector = i.GetInspector(),
                                                               }).ToList();
            foreach (InspectorWorkload workLoad in inspectorWorkloads)
            {
                workLoad.SonoCode = workLoad.Inspector.SonoCode;
                workLoad.HasCompensationPermission = _defaultInspectorWorkLoad.HasCompensationPermission;
                workLoad.HasPaymentPermission = _defaultInspectorWorkLoad.HasPaymentPermission;
                workLoad.MaxCompenationCapacity = _defaultInspectorWorkLoad.MaxCompenationCapacity;
                workLoad.MaxPaymentCapacity = _defaultInspectorWorkLoad.MaxPaymentCapacity;
            }
            ExecuteServiceCall(() => _declAssignmentService.SetWorkloadForInspectors(inspectorWorkloads), OnWorkloadAdd);

        }

        private void OnWorkloadClear(OperationResult<ClearInspectorsWorkloadResult> result)
        {
            _srcGridPresenter.ResetSettings();
            foreach (var inspector in result.Result.SuccesfullRecords)
            {
                var inspectorModel = _dest.FirstOrDefault(i =>
                                                              {
                                                                  var wl = i.GetInspectorWorkLoad();
                                                                  return wl.Inspector.SID == inspector.SID;
                                                              });
                if (inspectorModel != null)
                {
                    _dest.Remove(inspectorModel);
                }
                if (inspector.IsActive)
                {
                    _source.Insert(0, new InspectorModel(inspector));
                }
            }
            if (result.Result.FailedRecords.Count > 0)
            {
                var msg = "Невозможно удалить инспекторов из списка, т.к. за ними есть закрепленные декларации:";
                var msgInfo = new MessageInfoContext();
                msgInfo.Caption = "Удаление инспектора";
                msgInfo.MessageBoxIcon = System.Windows.Forms.MessageBoxIcon.Warning;
                msgInfo.Message = "Некоторые из выбранных инспекторов, не могут быть удалены, так как за ними закреплены налогоплательщики и/или декларации с открытой КНП. " +
                                  "Отмените данные назначения, передайте необработанные декларации другим инспекторам и после этого вернитесь к операции удаления.";
                View.ShowTrayMessage(msgInfo);
            }
        }

        private void OnGetDefaultWorkload(OperationResult<DefaultInspectorWorkLoad> defaultWorkload)
        {
            _defaultInspectorWorkLoad = defaultWorkload.Result ?? new DefaultInspectorWorkLoad();
            _inspectorSettingsView.SetDefaultWorkLoad(_defaultInspectorWorkLoad);
        }

        private void OnGetSourceCollection(OperationResult<List<InspectorSonoAssignment>> sourceInspectors)
        {
            var inspectors = sourceInspectors.Result.Select(insp => new InspectorModel(insp)).ToList();
            _source = new BindingList<InspectorModel>(inspectors);
            _inspectorSettingsView.SetSouceGridCollection(_source);
        }

        private void OnGetDestCollection(OperationResult<List<InspectorWorkload>> destInspectors)
        {
            var inspectors = destInspectors.Result.Select(insp => new InspectorWorkloadModel(insp)).ToList();
            _dest = new BindingList<InspectorWorkloadModel>(inspectors);
            _inspectorSettingsView.SetDestGridCollection(_dest);
        }

        private void OnSelectionChanged()
        {
            _inspectorSettingsView.SetMoveSrcToDstEnabled(_source.Any(i => i.Selected));
            _inspectorSettingsView.SetMoveDstToSrcEnabled(_dest.Any(i => i.Selected));
        }

        private void OnWorkloadAdd(OperationResult<SetInspectorWorkLoadResult> result)
        {
            _dstGridPresenter.ResetSettings();
            foreach (var workload in result.Result.SuccesfullRecords)
            {
                var inspectorModel = _source.FirstOrDefault(i => i.GetInspector().SID == workload.Inspector.SID);
                if (inspectorModel != null)
                {
                    _source.Remove(inspectorModel);
                }
                _dest.Insert(0, new InspectorWorkloadModel(workload));
            }
            if (result.Result.FailedRecords.Count > 0)
            {
                _inspectorSettingsView.ShowNotification(
                    "Не на всех инспекторов была назначена нагрузка по умолчанию. Возможно данные на форме устарели." +
                    "Пожалуйста обновите форму.");
            }
        }
    }
}
