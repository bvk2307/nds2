﻿using System.Windows.Forms;
using Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment;
using System;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.DeclarationAssignment.InspectorSettings
{
    public partial class EditInspectorWorkloadForm : Form
    {
        private InspectorWorkload _workload;

        public EditInspectorWorkloadForm()
        {
            InitializeComponent();
        }

        public void SetWorkload(InspectorWorkload workload)
        {
            _workload = workload;
            _compensationNumericEditor.Value = _workload.HasCompensationPermission
                                                   ? (decimal?)_workload.MaxCompenationCapacity
                                                   : null;
            _paymentNumericEditor.Value = _workload.HasPaymentPermission
                                              ? (decimal?)_workload.MaxPaymentCapacity
                                              : null;
            _enabledCheckEditor.Checked = !_workload.IsPaused;
            _nameLabel.Value = "<b>"+_workload.Inspector.Name + "</b>, табельный № " + _workload.Inspector.EmployeeNum;
        }

        private void _saveBtn_Click(object sender, System.EventArgs e)
        {
            var compensationAmt = (_compensationNumericEditor.Value == DBNull.Value ? null : (decimal?)_compensationNumericEditor.Value);
            var paymentAmt = (_paymentNumericEditor.Value == DBNull.Value ? null : (decimal?)_paymentNumericEditor.Value);
            _workload.HasCompensationPermission = compensationAmt.GetValueOrDefault() != 0;
            _workload.HasPaymentPermission = paymentAmt.GetValueOrDefault() != 0;
            _workload.MaxCompenationCapacity = compensationAmt.GetValueOrDefault();
            _workload.MaxPaymentCapacity = paymentAmt.GetValueOrDefault();
            _workload.IsPaused = !_enabledCheckEditor.Checked;
            DialogResult = DialogResult.OK;
        }

    }
}
