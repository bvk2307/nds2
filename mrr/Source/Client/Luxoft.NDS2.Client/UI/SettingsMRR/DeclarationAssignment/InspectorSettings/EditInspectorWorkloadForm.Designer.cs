﻿namespace Luxoft.NDS2.Client.UI.SettingsMRR.DeclarationAssignment.InspectorSettings
{
    partial class EditInspectorWorkloadForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            this._saveBtn = new Infragistics.Win.Misc.UltraButton();
            this._cancelBtn = new Infragistics.Win.Misc.UltraButton();
            this._paymentNumericEditor = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this._compensationNumericEditor = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this._enabledCheckEditor = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this._nameLabel = new Infragistics.Win.FormattedLinkLabel.UltraFormattedTextEditor();
            ((System.ComponentModel.ISupportInitialize)(this._paymentNumericEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._compensationNumericEditor)).BeginInit();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _saveBtn
            // 
            this._saveBtn.Location = new System.Drawing.Point(298, 109);
            this._saveBtn.Name = "_saveBtn";
            this._saveBtn.Size = new System.Drawing.Size(75, 23);
            this._saveBtn.TabIndex = 4;
            this._saveBtn.Text = "Сохранить";
            this._saveBtn.Click += new System.EventHandler(this._saveBtn_Click);
            // 
            // _cancelBtn
            // 
            this._cancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._cancelBtn.Location = new System.Drawing.Point(391, 109);
            this._cancelBtn.Name = "_cancelBtn";
            this._cancelBtn.Size = new System.Drawing.Size(75, 23);
            this._cancelBtn.TabIndex = 5;
            this._cancelBtn.Text = "Отмена";
            // 
            // _paymentNumericEditor
            // 
            this._paymentNumericEditor.FormatString = "#,###,###,##0";
            this._paymentNumericEditor.Location = new System.Drawing.Point(267, 72);
            this._paymentNumericEditor.Margin = new System.Windows.Forms.Padding(0);
            this._paymentNumericEditor.MaskInput = "{LOC}n,nnn,nnn,nnn";
            this._paymentNumericEditor.MaxValue = new decimal(new int[] {
            1410065407,
            2,
            0,
            0});
            this._paymentNumericEditor.MinValue = 0;
            this._paymentNumericEditor.Name = "_paymentNumericEditor";
            this._paymentNumericEditor.Nullable = true;
            this._paymentNumericEditor.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Decimal;
            this._paymentNumericEditor.PromptChar = ' ';
            this._paymentNumericEditor.Size = new System.Drawing.Size(201, 21);
            this._paymentNumericEditor.TabIndex = 11;
            // 
            // _compensationNumericEditor
            // 
            this._compensationNumericEditor.FormatString = "#,###,###,##0";
            this._compensationNumericEditor.Location = new System.Drawing.Point(267, 30);
            this._compensationNumericEditor.Margin = new System.Windows.Forms.Padding(0);
            this._compensationNumericEditor.MaskInput = "{LOC}n,nnn,nnn,nnn";
            this._compensationNumericEditor.MaxValue = new decimal(new int[] {
            1410065407,
            2,
            0,
            0});
            this._compensationNumericEditor.MinValue = 0;
            this._compensationNumericEditor.Name = "_compensationNumericEditor";
            this._compensationNumericEditor.Nullable = true;
            this._compensationNumericEditor.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Decimal;
            this._compensationNumericEditor.PromptChar = ' ';
            this._compensationNumericEditor.Size = new System.Drawing.Size(201, 21);
            this._compensationNumericEditor.TabIndex = 10;
            // 
            // ultraPanel1
            // 
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this._nameLabel);
            this.ultraPanel1.ClientArea.Controls.Add(this._enabledCheckEditor);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraLabel2);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraLabel1);
            this.ultraPanel1.ClientArea.Controls.Add(this._paymentNumericEditor);
            this.ultraPanel1.ClientArea.Controls.Add(this._compensationNumericEditor);
            this.ultraPanel1.ClientArea.Controls.Add(this._cancelBtn);
            this.ultraPanel1.ClientArea.Controls.Add(this._saveBtn);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel1.Location = new System.Drawing.Point(0, 0);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(493, 142);
            this.ultraPanel1.TabIndex = 12;
            // 
            // _enabledCheckEditor
            // 
            this._enabledCheckEditor.Location = new System.Drawing.Point(12, 109);
            this._enabledCheckEditor.Name = "_enabledCheckEditor";
            this._enabledCheckEditor.Size = new System.Drawing.Size(202, 20);
            this._enabledCheckEditor.TabIndex = 14;
            this._enabledCheckEditor.Text = "Включить автоназначение НД";
            // 
            // ultraLabel2
            // 
            this.ultraLabel2.Location = new System.Drawing.Point(12, 65);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(242, 37);
            this.ultraLabel2.TabIndex = 13;
            this.ultraLabel2.Text = "Сумма расхождений по автотребованиям\r\nНД к уплате (нулевой НД):";
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Location = new System.Drawing.Point(12, 35);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(176, 23);
            this.ultraLabel1.TabIndex = 12;
            this.ultraLabel1.Text = "Сумма НДС к возмещению:";
            // 
            // _nameLabel
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            this._nameLabel.Appearance = appearance1;
            this._nameLabel.AutoSize = true;
            this._nameLabel.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this._nameLabel.Location = new System.Drawing.Point(12, 5);
            this._nameLabel.Name = "_nameLabel";
            this._nameLabel.ReadOnly = true;
            this._nameLabel.Size = new System.Drawing.Size(164, 0);
            this._nameLabel.TabIndex = 17;
            this._nameLabel.UseAppStyling = false;
            this._nameLabel.Value = "";
            this._nameLabel.WrapText = false;
            // 
            // EditInspectorWorkloadForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(493, 142);
            this.Controls.Add(this.ultraPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EditInspectorWorkloadForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Редактирование пороговых значений";
            ((System.ComponentModel.ISupportInitialize)(this._paymentNumericEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._compensationNumericEditor)).EndInit();
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ClientArea.PerformLayout();
            this.ultraPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraButton _saveBtn;
        private Infragistics.Win.Misc.UltraButton _cancelBtn;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor _paymentNumericEditor;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor _compensationNumericEditor;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor _enabledCheckEditor;
        private Infragistics.Win.FormattedLinkLabel.UltraFormattedTextEditor _nameLabel;
    }
}