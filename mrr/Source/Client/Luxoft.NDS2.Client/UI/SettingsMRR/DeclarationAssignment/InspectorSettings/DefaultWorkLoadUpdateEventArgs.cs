﻿using Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment;
using System;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.DeclarationAssignment.InspectorSettings
{
    public sealed class DefaultWorkLoadUpdateEventArgs : EventArgs
    {
        private readonly DefaultInspectorWorkLoad _defaultWorkload;

        public DefaultWorkLoadUpdateEventArgs(DefaultInspectorWorkLoad defaultWorkload)
        {
            _defaultWorkload = defaultWorkload;
        }

        public DefaultInspectorWorkLoad DefaultWorkload
        {
            get { return _defaultWorkload; }
        }
    }
}