using System;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.DeclarationAssignment
{
    public sealed class RowActionEventArgs<T> : EventArgs
    {
        private readonly T _model;
        public RowActionEventArgs(T model)
        {
            _model = model;
        }

        public T Model
        {
            get { return _model; }
        }
    }
}