using System;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.DeclarationAssignment
{
    public sealed class GridInitEventArgs : EventArgs
    {
        private readonly IDataGridView _grid;
        private readonly IPager _pager;

        public GridInitEventArgs(IDataGridView grid, IPager pager)
        {
            _grid = grid;
            _pager = pager;
        }

        public IDataGridView Grid
        {
            get { return _grid; }
        }

        public IPager Pager
        {
            get { return _pager; }
        }
    }
}