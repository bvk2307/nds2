﻿using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Common.Contracts.DTO.DeclarationAssigment;
using Luxoft.NDS2.Common.Contracts.Services.DeclarationAssignment;
using Microsoft.Practices.CompositeUI;
using System;
using System.Linq;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.SettingsMRR.DeclarationAssignment
{
    class Navigation : BasePresenter<BaseView>, IInspectorAssignment
    {
        private readonly ITaxpayerService _taxpayerService;
        private readonly IInspectorDeclarationService _inspectorDeclarationService ;
        public Navigation(PresentationContext c, WorkItem wi, BaseView v)
            : base(c, wi, v)
        {
            _taxpayerService = GetServiceProxy<ITaxpayerService>();
            _inspectorDeclarationService = GetServiceProxy<IInspectorDeclarationService>();
        }

        public bool ShowTransferDeclarationsDialogWithCurrent(long[] declarationIds, string currentInspectorSid)
        {
            var inspectors = GetInspectors();
            return ShowTransferDeclarationsDialog(inspectors, currentInspectorSid, String.Empty, declarationIds);
        }

        public bool ShowTransferDeclarationsDialogWithDefault(long[] declarationIds, string defaultInspectorSid)
        {
            var inspectors = GetInspectors();
            return ShowTransferDeclarationsDialog(inspectors, String.Empty, defaultInspectorSid, declarationIds);
        }

        public bool ShowTransferDeclarationsDialog(long[] declarationIds, string currentInspectorSid, string defaultInspectorSid)
        {
            var inspectors = GetInspectors();
            return ShowTransferDeclarationsDialog(inspectors, currentInspectorSid, defaultInspectorSid, declarationIds);
        }

        private bool ShowTransferDeclarationsDialog(List<InspectorStatistics> inspectros,
            string currentInspectorSid, string defaultInspectorSid, params long[] declarationIds)
        {
            var form = new InspectorAssignmentForm(inspectros.Select(i => new InspectorStatisticsModel(i)).ToList());
            if (!string.IsNullOrEmpty(currentInspectorSid))
            {
                form.SetCurrentInspector(currentInspectorSid);
            }
            if (!string.IsNullOrEmpty(defaultInspectorSid))
            {
                form.SetDefaultInspector(defaultInspectorSid);
            }
            if (form.ShowDialog(View.ParentForm) == System.Windows.Forms.DialogResult.OK)
            {
                var sid = form.GetInspectorSid();
                if (!string.IsNullOrEmpty(sid))
                {
                    foreach(var id in declarationIds)
                    {
                        ExecuteServiceCall(
                            () => _inspectorDeclarationService.AssignInspector(sid, id));
                    }
                }
                return !string.IsNullOrEmpty(sid);
            }
            return false;
        }

        private List<InspectorStatistics> GetInspectors()
        {
            var result = new List<InspectorStatistics>();
            var inspectorService = GetServiceProxy<IInspectorStatisticsService>();
            ExecuteServiceCall(inspectorService.GetActiveInspectorsStatistics, (res) => result = res.Result);
            return result;
        }
    }
}
