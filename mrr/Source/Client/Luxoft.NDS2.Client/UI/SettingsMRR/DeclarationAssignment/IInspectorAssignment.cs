﻿
namespace Luxoft.NDS2.Client.UI.SettingsMRR.DeclarationAssignment
{
    public interface IInspectorAssignment
    {
        bool ShowTransferDeclarationsDialog(long[] declarationIds, string currentInspectorSid, string defaultInspectorSid);

        bool ShowTransferDeclarationsDialogWithCurrent(long[] declarationIds, string currentInspectorSid);

        bool ShowTransferDeclarationsDialogWithDefault(long[] declarationIds, string defaultInspectorSid);
    }
}
