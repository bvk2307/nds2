﻿using Luxoft.NDS2.Common.Contracts.Services;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Client.Infrastructure;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.VersionHistory
{
    public class VersionListItemLoader : ServiceProxyBase<IEnumerable<VersionInfo>>, IVersionListItemLoader
    {
        private readonly IVersionInfoService _service;
        public VersionListItemLoader(IVersionInfoService service, INotifier notifier, IClientLogger logger)
            : base(notifier, logger)
        {
            _service = service;
        }

        public IList<VersionListItemViewModel> Load()
        {
            IEnumerable<VersionInfo> versions = null;
            Invoke(() => _service.GetVersionsWithReleaseNotes(), out versions);

            if (versions != null)
                return versions
                        .Select(v => new VersionListItemViewModel(v.VersionNumber, v.ReleaseFileName))
                        .OrderByDescending(n => n.SortId)
                        .ToList();
            return new List<VersionListItemViewModel>();
        }
    }
}
