﻿using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.UI.Base;
using Microsoft.Practices.ObjectBuilder;
using System;
using Infragistics.Win.FormattedLinkLabel;

namespace Luxoft.NDS2.Client.UI.VersionHistory
{
    public partial class VersionHistoryView : BaseView, IVersionHistoryView
    {
        # region Создание презентера

        [CreateNew]
        public VersionHistoryPresenterCreator Creator
        {
            set
            {
                value.Create(this, this.WorkItem);
            }
        }

        # endregion

        # region .ctor

        public VersionHistoryView()
        {
            InitializeComponent();
            InitGrid();
        }

        public VersionHistoryView(PresentationContext presentationContext)
            : base(presentationContext)
        {
            InitializeComponent();
            InitGrid();
        }

        private void InitGrid()
        {
            var viewDetailColumn = _grid.DisplayLayout.Bands["items"].Columns["ViewDetails"];
            var viewDetailEditor = (FormattedLinkEditor)viewDetailColumn.Editor;
            viewDetailEditor.LinkClicked += ViewDetailClicked;
        }


        # endregion

        # region IVersionHistoryView

        public event EventHandler<ObjectEventArgs> ReleaseNotesViewing;

        public void SetDataSource(object dataSource)
        {
            _gridViewDataSource.DataSource = dataSource;
        }

        private void ViewDetailClicked(object sender, LinkClickedEventArgs e)
        {
            var cell = (UltraGridCell) e.Context;
            var handler = ReleaseNotesViewing;
            if (handler != null)
            {
                handler(this, new ObjectEventArgs(cell.Row.ListObject));
            }
        }

        #endregion

    }
}
