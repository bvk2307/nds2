﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.VersionHistory
{
    public class VersionListViewModel
    {
        public VersionListViewModel(IList<VersionListItemViewModel> versions)
        {
            Items = new BindingList<VersionListItemViewModel>(versions);
        }

        public BindingList<VersionListItemViewModel> Items
        {
            get;
            private set;
        }
    }
}
