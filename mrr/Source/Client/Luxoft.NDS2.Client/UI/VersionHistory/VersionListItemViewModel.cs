﻿using System;
using System.Linq;
using CommonComponents.Utils;

namespace Luxoft.NDS2.Client.UI.VersionHistory
{
    public class VersionListItemViewModel
    {
        private readonly string _fileName;
        private readonly long _sortId;

        public VersionListItemViewModel(string id, string fileName)
        {
            _fileName = fileName;
            Id = id;
            _sortId = GetSortId(Id);
        }

        public string Id
        {
            get;
            set;
        }

        public string ViewDetails
        {
            get
            {
                return "Состав версии";
            }
        }

        private static long GetSortId(string id)
        {
            long r = 0;
            id.Split('.').Reverse().ForAll((n, i) =>
            {
                r += (int.Parse(n)+1) * Convert.ToInt64(Math.Pow(10000.0, i));
            });
            return r;
        }

        public string GetFileName()
        {
            return _fileName;
        }

        public long SortId { get { return _sortId; } }
    }
}
