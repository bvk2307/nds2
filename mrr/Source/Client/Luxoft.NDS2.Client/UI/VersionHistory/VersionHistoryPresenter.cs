﻿using CommonComponents.Catalog;
using System;
using System.IO;
using Luxoft.NDS2.Client.Infrastructure;

namespace Luxoft.NDS2.Client.UI.VersionHistory
{
    public class VersionHistoryPresenter
    {
        private readonly IVersionHistoryView _view;
        private readonly ICatalogService _catalogService;
        private readonly IFileOpener _fileOpener;
        private readonly INotifier _notifierService;
        private readonly IClientLogger _clientLogger;
        private readonly VersionListViewModel _viewModel;

        public VersionHistoryPresenter(
            IVersionHistoryView view,
            ICatalogService catalogService,
            IVersionListItemLoader versionListLoader,
            IFileOpener fileOpener,
            INotifier notifierService,
            IClientLogger clientLogger
            )
        {
            _notifierService = notifierService;
            _clientLogger = clientLogger;
            _view = view;
            _fileOpener = fileOpener;
            _catalogService = catalogService;
            _viewModel = new VersionListViewModel(versionListLoader.Load());

            _view.SetDataSource(_viewModel);
            _view.ReleaseNotesViewing += OpenReleaseNotes;
        }

        private void OpenReleaseNotes(object sender, ObjectEventArgs e)
        {
            var versionInfo = e.Item as VersionListItemViewModel;
            if (versionInfo == null)
                return;

            var address =
                new CatalogAddress(
                    CatalogAddressSchemas.LogicalCatalog,
                    "RoleCatalog",
                    "NDS2Subsystem",
                    "FileSystems",
                    string.Format("Bin/ReleaseNotes/{0}", versionInfo.GetFileName()));
            try
            {
                var fileInfo = _catalogService.CreateInstance<FileInfo>(address);
                _fileOpener.Open(fileInfo.FullName);
            }
            catch (Exception ex)
            {
                _clientLogger.LogError(ex);
                _notifierService.ShowError("При открытии файла произошла ошибка");
            }
        }
    }
}
