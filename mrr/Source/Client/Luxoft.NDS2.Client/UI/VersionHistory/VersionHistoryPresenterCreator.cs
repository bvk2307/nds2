﻿using CommonComponents.Catalog;
using CommonComponents.Uc.Infrastructure.Interface;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Common.Contracts.Services;
using Microsoft.Practices.CompositeUI;

namespace Luxoft.NDS2.Client.UI.VersionHistory
{
    public class VersionHistoryPresenterCreator : Presenter<IVersionHistoryView>
    {
        public object Create(IVersionHistoryView view, WorkItem workItem)
        {
            var notifier = WorkItem.Services.Get<INotifier>(true);
            var clientLogger = WorkItem.Services.Get<IClientLogger>(true);

            return new VersionHistoryPresenter(
                view,
                WorkItem.Services.Get<ICatalogService>(true),
                new VersionListItemLoader(
                        WorkItem.Services.GetServiceProxy<IVersionInfoService>(),
                        notifier,
                        clientLogger
                         ),
                new FileOpener(),
               notifier,
               clientLogger
                );
        }
    }
}
