﻿using Luxoft.NDS2.Client.UI.Controls.Grid;
using System;

namespace Luxoft.NDS2.Client.UI.VersionHistory
{
    public interface IVersionHistoryView : IGridView
    {
        event EventHandler<ObjectEventArgs> ReleaseNotesViewing;
    }
}
