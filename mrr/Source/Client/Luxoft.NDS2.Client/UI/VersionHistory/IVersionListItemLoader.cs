﻿using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.VersionHistory
{
    public interface IVersionListItemLoader
    {
        IList<VersionListItemViewModel> Load();
    }
}
