﻿using System;
using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Common.Contracts.DTO;
using System.Threading;

namespace Luxoft.NDS2.Client.UI
{
    public interface IServiceProxy<TResult> 
    {
        bool TryExecute(out TResult result);
    }

    public class ServiceProxy<TService, TResult> : ServiceProxyBase<TResult>, IServiceProxy<TResult>
        where TResult : class
        where TService : class
    {
        private readonly TService _service;

        private readonly Func<TService, OperationResult<TResult>> _serviceMethod;

        public ServiceProxy(
            INotifier notifier, 
            IClientLogger logger, 
            TService service, 
            Func<TService, OperationResult<TResult>> serviceMethod)
            : base(notifier, logger)
        {
            _service = service;
            _serviceMethod = serviceMethod;
        }

        public ServiceProxy(
            INotifier notifier,
            IClientLogger logger,
            TService service)
            : base(notifier, logger)
        {
            _service = service;
        }

        public bool TryExecute(out TResult result)
        {
            return Invoke(() => _serviceMethod(_service), out result);
        }

        protected override TResult FailureResult()
        {
            return Activator.CreateInstance<TResult>();
        }

        protected override void CallBack(TResult result)
        {
        }
    }
}
