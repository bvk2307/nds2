﻿using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Infragistics.Win.UltraWinEditors;

namespace Luxoft.NDS2.Client.UI
{
    public static class DateTimeEditorHelper
    {
        private static Keys[] AllowedKeys = new Keys[] { Keys.Left, Keys.Enter, Keys.Right, Keys.Escape, Keys.Tab };

        private static Keys[] KeysToDropDown = new Keys[] { Keys.Down, Keys.Up };

        public static void DisableManualInput(this UltraDateTimeEditor dateEditor)
        {
            dateEditor.KeyDown += DisabledEditorKeyDown;
        }

        private static void DisabledEditorKeyDown(object sender, KeyEventArgs e)
        {
            if (!AllowedKeys.Contains(e.KeyCode))
            {
                e.SuppressKeyPress = true;

                if (KeysToDropDown.Contains(e.KeyCode))
                {
                    ((UltraDateTimeEditor)sender).DropDown();
                }
            }
        }

        public static void SetValidState(this UltraDateTimeEditor dateEditor, bool valid)
        {
            dateEditor.Appearance.ForeColor = valid ? Color.Black : Color.Red;
        }
    }
}
