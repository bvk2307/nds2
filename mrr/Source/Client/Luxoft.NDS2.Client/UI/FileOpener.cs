﻿using System.Diagnostics;

namespace Luxoft.NDS2.Client.UI
{
    public class FileOpener : IFileOpener
    {
        public void Open(string filename)
        {
            new Process
            {
                StartInfo =
                {
                    FileName = filename,
                    UseShellExecute = true,
                    CreateNoWindow = true
                }
            }.Start();
        }
    }
}
