﻿using System;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using System.Collections;

namespace Luxoft.NDS2.Client.UI
{
    public static class UltraComboEditorHelper
    {
        public static void SetDataSource(
            this UltraComboEditor editor, 
            IEnumerable dataItems)
        {
            editor.Items.Clear();

            foreach (var dataItem in dataItems)
            {
                editor.Items.Add(new ValueListItem(dataItem, dataItem.ToString()));
            }
        }

        public static object GetValue(this UltraComboEditor editor)
        {
            return editor.SelectedItem.DataValue;
        }

        public static T GetValue<T>(this UltraComboEditor editor)
        {
            return (T)editor.GetValue();
        }

        public static void SetValue(this UltraComboEditor editor, object value)
        {
            foreach (var editorItem in editor.Items)
            {
                if (editorItem.DataValue.Equals(value))
                {
                    editor.SelectedItem = editorItem;
                }
            }
            editor.Refresh();
        }
    }
}
