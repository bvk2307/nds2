﻿using CommonComponents.Communication;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO;
using Microsoft.Practices.CompositeUI;
using Microsoft.Practices.CompositeUI.Collections;
using System;

namespace Luxoft.NDS2.Client.UI
{
    public static class ServiceHelper
    {
        public static TService GetWcfServiceProxy<TService>(this WorkItem workItem)
            where TService : class
        {
            return workItem.Services.GetServiceProxy<TService>();
        }

        public static T GetServiceProxy<T>(this ServiceCollection services) 
            where T : class
        {
            return services.Get<IClientContextService>().CreateCommunicationProxy<T>(Constants.EndpointName);
        }

        public static bool ExecuteNoHandling<TService, TResult>(
            this TService service, 
            Func<TService, OperationResult<TResult>> method, 
            out TResult result)
        {
            var response = method(service);

            if (response.Status == ResultStatus.Success)
            {
                result = response.Result;
                return true;
            }

            result = default(TResult);
            return false;
        }
    }
}
