﻿using System;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Controls
{
    public partial class TextViewer : Form
    {
        public static void Show(Form owner, string caption, string text)
        {
            new TextViewer
            {
                Owner = owner,
                Text = caption,
                Message = text
            }.ShowDialog();
        }

        private string Message
        {
            set
            {
                _message.Text = value;
            }
        }

        private TextViewer()
        {
            InitializeComponent();
        }

        private void CloseClick(object sender, EventArgs e)
        {
            Close();
        }
    }
}
