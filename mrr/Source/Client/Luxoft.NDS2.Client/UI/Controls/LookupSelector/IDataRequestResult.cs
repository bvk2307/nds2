﻿using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Controls.LookupSelector
{
    /// <summary>
    /// Этот интерфейс описывает результат запроса данных справочника
    /// </summary>
    /// <typeparam name="TLookupEntry">Тип элемента справочника</typeparam>
    public interface IDataRequestResult<TLookupEntry>
    {
        /// <summary>
        /// Возвращает данные справочника
        /// </summary>
        IEnumerable<TLookupEntry> Data { get; }

        /// <summary>
        /// Возвращает признак того, что критериям запроса удовлетворяет большее количество записей справочника, чем было возвращено
        /// </summary>
        bool ExceedsMaxQuantity { get; }
    }
}
