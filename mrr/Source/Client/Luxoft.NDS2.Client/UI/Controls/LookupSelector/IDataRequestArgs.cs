﻿namespace Luxoft.NDS2.Client.UI.Controls.LookupSelector
{
    /// <summary>
    /// Этот интерфейс описывает параметры запроса данных справочника
    /// </summary>
    public interface IDataRequestArgs
    {
        /// <summary>
        /// Возвращает условия поиска
        /// </summary>
        string SearchPattern { get; }

        /// <summary>
        /// Возвращает максимальное число жлементов справочника ,которые необходимо вернуть
        /// </summary>
        int MaxQuantity { get; }
    }
}
