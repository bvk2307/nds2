﻿using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.Model;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Linq.Expressions;
using System.Reflection;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Controls.LookupSelector.Helpers
{
    /// <summary>
    /// Этот класс содержит методы расширения элементов UltraGrid
    /// </summary>
    public static class UltraGridHelper
    {
        public static UltraGridColumn AddCheckColumn<TRowData>(
            this UltraGridBand band,
            Expression<Func<TRowData, bool>> columnExpression)
        {
            return band.Columns.Add(((MemberExpression)columnExpression.Body).Member.Name, string.Empty);
        }

        public static string AddColumn(
            this UltraGridBand band,
            PropertyInfo property)
        {
            var attributes = 
                property.GetCustomAttributes(typeof(ListDisplaySettingsAttribute), false);

            if (attributes.Length == 0)
            {
                return string.Empty;
            }

            var displayAttribute = (ListDisplaySettingsAttribute)attributes[0];
            var column = band.Columns.Add(property.Name, displayAttribute.Title);

            column.AllowGroupBy = DefaultableBoolean.False;
            column.Hidden = displayAttribute.IsHidden;
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            column.Width = displayAttribute.Width;

            return column.Key;
        }

        public static UltraGridColumn WithCheckColumnSetup(
            this UltraGridColumn column)
        {
            column.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.Never;
            column.Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.Default;
            column.DataType = typeof(bool);
            column.CellActivation = Activation.AllowEdit;
            column.CellClickAction = CellClickAction.Edit;
            column.AllowRowSummaries = AllowRowSummaries.False;
            column.AllowGroupBy = DefaultableBoolean.False;
            column.AllowRowFiltering = DefaultableBoolean.False;
            column.Width = 10;

            return column;
        }
    }
}
