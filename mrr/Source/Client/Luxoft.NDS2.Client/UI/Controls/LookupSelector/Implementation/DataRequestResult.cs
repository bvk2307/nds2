﻿using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Controls.LookupSelector.Implementation
{
    public class DataRequestResult<TLookupEntry> : IDataRequestResult<TLookupEntry>
    {
        public DataRequestResult(
            IEnumerable<TLookupEntry> data,
            bool exceedsMaxQuantity = false)
        {
            Data = data;
            ExceedsMaxQuantity = exceedsMaxQuantity;
        }

        public IEnumerable<TLookupEntry> Data
        {
            get;
            private set;
        }

        public bool ExceedsMaxQuantity
        {
            get;
            private set;
        }
    }
}
