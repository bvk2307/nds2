﻿namespace Luxoft.NDS2.Client.UI.Controls.LookupSelector.Implementation.MultiSelect
{
    partial class LookupMultiSelectView
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._viewer = new Luxoft.NDS2.Client.UI.Controls.LookupSelector.Implementation.Base.LookupSelectionViewer();
            this._popup = new Infragistics.Win.Misc.UltraPopupControlContainer(this.components);
            this.SuspendLayout();
            // 
            // _viewer
            // 
            this._viewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this._viewer.Location = new System.Drawing.Point(0, 0);
            this._viewer.MinimumSize = new System.Drawing.Size(21, 21);
            this._viewer.Name = "_viewer";
            this._viewer.Size = new System.Drawing.Size(300, 25);
            this._viewer.TabIndex = 0;
            // 
            // View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._viewer);
            this.Name = "View";
            this.Size = new System.Drawing.Size(300, 25);
            this.ResumeLayout(false);

        }

        #endregion

        private Base.LookupSelectionViewer _viewer;
        private Infragistics.Win.Misc.UltraPopupControlContainer _popup;
    }
}
