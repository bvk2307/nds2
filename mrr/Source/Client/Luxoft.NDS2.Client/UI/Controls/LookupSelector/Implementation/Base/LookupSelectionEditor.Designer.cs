﻿namespace Luxoft.NDS2.Client.UI.Controls.LookupSelector.Implementation.Base
{
    partial class LookupSelectionEditor
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            this._saveButton = new Infragistics.Win.Misc.UltraButton();
            this._searchCriteriaInput = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this._lookupItemsList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this._dataSource = new System.Windows.Forms.BindingSource(this.components);
            this._searchButton = new Infragistics.Win.Misc.UltraButton();
            this._cancelButton = new Infragistics.Win.Misc.UltraButton();
            ((System.ComponentModel.ISupportInitialize)(this._searchCriteriaInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._lookupItemsList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._dataSource)).BeginInit();
            this.SuspendLayout();
            // 
            // _saveButton
            // 
            this._saveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._saveButton.Location = new System.Drawing.Point(166, 117);
            this._saveButton.Name = "_saveButton";
            this._saveButton.Size = new System.Drawing.Size(75, 21);
            this._saveButton.TabIndex = 3;
            this._saveButton.Text = "Сохранить";
            // 
            // _searchCriteriaInput
            // 
            this._searchCriteriaInput.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._searchCriteriaInput.Location = new System.Drawing.Point(0, 0);
            this._searchCriteriaInput.Name = "_searchCriteriaInput";
            this._searchCriteriaInput.Size = new System.Drawing.Size(293, 21);
            this._searchCriteriaInput.TabIndex = 0;
            // 
            // _lookupItemsList
            // 
            this._lookupItemsList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._lookupItemsList.DataSource = this._dataSource;
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this._lookupItemsList.DisplayLayout.Appearance = appearance1;
            this._lookupItemsList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this._lookupItemsList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this._lookupItemsList.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance13.ForeColor = System.Drawing.SystemColors.GrayText;
            this._lookupItemsList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance13;
            this._lookupItemsList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance3.BackColor2 = System.Drawing.SystemColors.Control;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this._lookupItemsList.DisplayLayout.GroupByBox.PromptAppearance = appearance3;
            this._lookupItemsList.DisplayLayout.MaxColScrollRegions = 1;
            this._lookupItemsList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            appearance7.ForeColor = System.Drawing.SystemColors.ControlText;
            this._lookupItemsList.DisplayLayout.Override.ActiveCellAppearance = appearance7;
            appearance10.BackColor = System.Drawing.SystemColors.Highlight;
            appearance10.ForeColor = System.Drawing.SystemColors.HighlightText;
            this._lookupItemsList.DisplayLayout.Override.ActiveRowAppearance = appearance10;
            this._lookupItemsList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this._lookupItemsList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            this._lookupItemsList.DisplayLayout.Override.CardAreaAppearance = appearance12;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this._lookupItemsList.DisplayLayout.Override.CellAppearance = appearance8;
            this._lookupItemsList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this._lookupItemsList.DisplayLayout.Override.CellPadding = 0;
            appearance6.BackColor = System.Drawing.SystemColors.Control;
            appearance6.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance6.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance6.BorderColor = System.Drawing.SystemColors.Window;
            this._lookupItemsList.DisplayLayout.Override.GroupByRowAppearance = appearance6;
            appearance5.TextHAlignAsString = "Left";
            this._lookupItemsList.DisplayLayout.Override.HeaderAppearance = appearance5;
            this._lookupItemsList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this._lookupItemsList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this._lookupItemsList.DisplayLayout.Override.RowAppearance = appearance11;
            this._lookupItemsList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance9.BackColor = System.Drawing.SystemColors.ControlLight;
            this._lookupItemsList.DisplayLayout.Override.TemplateAddRowAppearance = appearance9;
            this._lookupItemsList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this._lookupItemsList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this._lookupItemsList.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
            this._lookupItemsList.Location = new System.Drawing.Point(0, 23);
            this._lookupItemsList.Name = "_lookupItemsList";
            this._lookupItemsList.Size = new System.Drawing.Size(319, 94);
            this._lookupItemsList.TabIndex = 2;
            this._lookupItemsList.UseAppStyling = false;
            // 
            // _searchButton
            // 
            this._searchButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance4.Image = global::Luxoft.NDS2.Client.Properties.Resources.find16;
            appearance4.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance4.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this._searchButton.Appearance = appearance4;
            this._searchButton.Location = new System.Drawing.Point(296, 0);
            this._searchButton.Margin = new System.Windows.Forms.Padding(0);
            this._searchButton.Name = "_searchButton";
            this._searchButton.Size = new System.Drawing.Size(21, 21);
            this._searchButton.TabIndex = 1;
            // 
            // _cancelButton
            // 
            this._cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._cancelButton.Location = new System.Drawing.Point(242, 117);
            this._cancelButton.Name = "_cancelButton";
            this._cancelButton.Size = new System.Drawing.Size(75, 21);
            this._cancelButton.TabIndex = 4;
            this._cancelButton.Text = "Отмена";
            // 
            // LookupSelectionEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this._cancelButton);
            this.Controls.Add(this._saveButton);
            this.Controls.Add(this._searchButton);
            this.Controls.Add(this._lookupItemsList);
            this.Controls.Add(this._searchCriteriaInput);
            this.MinimumSize = new System.Drawing.Size(121, 40);
            this.Name = "LookupSelectionEditor";
            this.Padding = new System.Windows.Forms.Padding(2);
            this.Size = new System.Drawing.Size(319, 140);
            ((System.ComponentModel.ISupportInitialize)(this._searchCriteriaInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._lookupItemsList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._dataSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.UltraWinEditors.UltraTextEditor _searchCriteriaInput;
        private Infragistics.Win.UltraWinGrid.UltraGrid _lookupItemsList;
        private Infragistics.Win.Misc.UltraButton _searchButton;
        private Infragistics.Win.Misc.UltraButton _cancelButton;
        private Infragistics.Win.Misc.UltraButton _saveButton;
        private System.Windows.Forms.BindingSource _dataSource;

    }
}
