﻿using Infragistics.Win;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.LookupSelector.Implementation.Base;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Luxoft.NDS2.Client;

namespace Luxoft.NDS2.Client.UI.Controls.LookupSelector.Implementation.MultiSelect
{
    public partial class LookupMultiSelectView : UserControl, ILookupSelectionView, ILookupSelectionEditView
    {
        # region Инициализация

        private LookupSelectionEditor _editor = new LookupSelectionEditor();

        public LookupMultiSelectView()
        {
            InitializeComponent();
            BindEvents();

            _popup.PopupControl = _editor;
            _editor.Width = Width;
        }

        private void BindEvents()
        {
            _viewer.OnEdit += () => OnEdit();
            _editor.OnCancel += () => OnCancel();
            _editor.OnReset += () => OnReset();
            _editor.OnSave += () => OnSave();
            _editor.OnSearch += () => OnSearch();
            _editor.OnSelectionChanged += RaiseSelectionChanged;
        }

        private void RaiseSelectionChanged(object sender)
        {
            if (OnSelectionChanged == null)
            {
                return;
            }

            OnSelectionChanged(sender);

            if (OnAfterSelectionChanged != null)
            {
                OnAfterSelectionChanged();
            }
        }

        # endregion

        # region Визуальные свойства

        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Description("Popup Height")]
        [Category("Luxoft")]
        public int PopupHeight
        {
            get { return _editor.Height; }
            set { _editor.Height = value; }
        }

        # endregion

        # region Реализация интерфейса ILookupSelectionView

        public event SimpleEventHandler OnEdit;

        public void SetSelection(string[] selectedItems)
        {
            _viewer.SetSelection(selectedItems);
        }

        # endregion

        # region Реализация интерфейса ILookupSelectionEditView

        public string SearchPattern
        {
            get { return _editor.SearchPattern; }
        }

        public int MaxQuantity
        {
            get { return _editor.MaxQuantity; }
        }

        public void SetLookupData<TLookupData>(IEnumerable<TLookupData> dataObjects)
            where TLookupData : Luxoft.NDS2.Client.Model.ISelectableEntry
        {
            _editor.SetLookupData<TLookupData>(dataObjects);
        }

        public void Notify(string notificationText)
        {
            _editor.Notify(notificationText);
        }

        private int PopupYOffset = 2;

        private int PopupXOffset = 8;

        public void DropDown()
        {
            var frm = this;
            var location = PointToScreen(frm.Location);

            _editor.Width = Width;
            _editor.Reset();

            _popup.Show(
                new PopupInfo()
                {
                     PreferredLocation = 
                        new Point(location.X + PopupXOffset, location.Y + Height + PopupYOffset)                     
                });

            _editor.FocusCriteriaInput();
        }

        public void Close()
        {
            _popup.Close();
        }

        public event SimpleEventHandler OnSave;

        public event SimpleEventHandler OnReset;

        public event SimpleEventHandler OnCancel;

        public event SimpleEventHandler OnSearch;

        public event ObjectEventHandler OnSelectionChanged;

        # endregion

        # region События

        public event SimpleEventHandler OnAfterSelectionChanged;

        # endregion
    }
}
