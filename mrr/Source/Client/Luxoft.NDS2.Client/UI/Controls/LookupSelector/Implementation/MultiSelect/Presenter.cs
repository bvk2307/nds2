﻿using Luxoft.NDS2.Client.Model;
using Luxoft.NDS2.Client.UI.Controls.LookupSelector.Base;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Controls.LookupSelector.Implementation.MultiSelect
{
    public class Presenter<TLookupEntryKey, TLookupEntry>
        : LookupSelectionPresenter<TLookupEntryKey, TLookupEntry>
        where TLookupEntry : ILookupEntry<TLookupEntryKey>
    {
        public Presenter(
            LookupMultiSelectView view, 
            Func<IDataRequestArgs, IDataRequestResult<TLookupEntry>> dataLoader,
            Dictionary<TLookupEntryKey, TLookupEntry> selectedItems = null)
            : base(view, view, dataLoader, selectedItems)
        {
        }
    }
}
