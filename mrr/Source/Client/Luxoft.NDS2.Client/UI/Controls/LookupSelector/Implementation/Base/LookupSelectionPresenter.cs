﻿using Luxoft.NDS2.Client.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Controls.LookupSelector.Base
{
    /// <summary>
    /// Этот интерфейс описывает функционал презентера элемента управления
    /// </summary>
    /// <typeparam name="TLookupEntryKey">Тип ключа элементов справочника (например. long)</typeparam>
    /// <typeparam name="TLookupEntry">Тип элементов справочника</typeparam>
    public class LookupSelectionPresenter<TLookupEntryKey, TLookupEntry>
        : ILookupSelectionPresenter<TLookupEntryKey, TLookupEntry>
        where TLookupEntry : ILookupEntry<TLookupEntryKey>
    {
        # region Константы

        private const string SearchExceedsMaxQuantityPattern =
            "Введенному критерию {0} удовлетворяет более {1} записей справочника. Пожалуйста уточните запрос.";

        # endregion

        # region Поля

        private Dictionary<TLookupEntryKey, TLookupEntry> _selectedItems;

        private Dictionary<TLookupEntryKey, TLookupEntry> _selectedItemsCopy;

        private ILookupSelectionView _readonlyView;

        private ILookupSelectionEditView _editView;

        private Func<IDataRequestArgs, IDataRequestResult<TLookupEntry>> _dataLoader;

        # endregion

        # region Конструктор

        /// <summary>
        /// Создает новый экземпляр класса LookupSelectionPresenter
        /// </summary>
        /// <param name="readonlyView">Представление просмотра выборки элементов справочнике в режиме "только чтение"</param>
        /// <param name="view">Представление редактирования выборки элементов справочника</param>
        /// <param name="selectedItems">Выбранные по умолчанию элементы справочника</param>
        public LookupSelectionPresenter(
            ILookupSelectionView readonlyView,
            ILookupSelectionEditView editView,
            Func<IDataRequestArgs, IDataRequestResult<TLookupEntry>> dataLoader,
            Dictionary<TLookupEntryKey, TLookupEntry> selectedItems = null)
        {
            if (readonlyView == null)
            {
                throw new ArgumentNullException("readonlyView");
            }

            if (editView == null)
            {
                throw new ArgumentNullException("editView");
            }

            this._readonlyView = readonlyView;
            this._editView = editView;
            this._dataLoader = dataLoader;
            SelectedItems = selectedItems ?? new Dictionary<TLookupEntryKey, TLookupEntry>();

            BindEvents();
        }

        /// <summary>
        /// Задает начальное состояние представлений
        /// </summary>
        private void SetReadonlyView()
        {
            _readonlyView.SetSelection(
                _selectedItems.Values.Select(entry => entry.Title).ToArray());
        }

        /// <summary>
        /// Задает обработчики событий представлений
        /// </summary>
        private void BindEvents()
        {
            _readonlyView.OnEdit += () => Edit();
            _editView.OnCancel += () => Cancel();
            _editView.OnReset += () => Reset();
            _editView.OnSave += () => Save();
            _editView.OnSearch += () => Search();
            _editView.OnSelectionChanged += (item) => ChangeItemSelection((TLookupEntry)item);
        }

        # endregion

        # region Обработчики событий представлений

        private void Edit()
        {
            _selectedItemsCopy = new Dictionary<TLookupEntryKey, TLookupEntry>();
            foreach (var key in _selectedItems.Keys)
            {
                _selectedItemsCopy.Add(key, _selectedItems[key]);
            }

            _editView.SetLookupData(_selectedItems.Values.ToList());
            _editView.DropDown();
        }

        private void Cancel()
        {
            _selectedItems = _selectedItemsCopy;

            _editView.Close();
        }

        private void Reset()
        {
            _selectedItems = _selectedItemsCopy;
        }

        private void Save()
        {
            _selectedItemsCopy = null;

            SetReadonlyView();
            _editView.Close();
        }

        private void Search()
        {
            var lookupData =
                _dataLoader(
                    new DataRequestArgs()
                    {
                        MaxQuantity = _editView.MaxQuantity,
                        SearchPattern = _editView.SearchPattern
                    });

            if (lookupData.ExceedsMaxQuantity)
            {
                _editView.Notify(
                    string.Format(
                        SearchExceedsMaxQuantityPattern,
                        _editView.SearchPattern,
                        _editView.MaxQuantity));
            }

            var tempList = _selectedItems.Values.ToList();
            tempList.AddRange(
                lookupData.Data.Where(entry => !_selectedItems.ContainsKey(entry.Key)));

            _editView.SetLookupData(tempList);
        }

        private void ChangeItemSelection(TLookupEntry item)
        {
            item.IsSelected = !_selectedItems.ContainsKey(item.Key);

            if (item.IsSelected)
            {
                _selectedItems.Add(item.Key, item);
            }
            else
            {
                _selectedItems.Remove(item.Key);
            }
        }

        # endregion

        # region Реализация IDataRequestEventArgs

        private class DataRequestArgs : IDataRequestArgs
        {
            public string SearchPattern
            {
                get;
                set;
            }

            public int MaxQuantity
            {
                get;
                set;
            }
        }

        # endregion

        # region Реализация интерфейса ILookupSelectionPresenter<TLookupEntryKey, TLookupEntry>

        /// <summary>
        /// Возвращает или задает словарь элементов справочника, выбранных пользователем
        /// </summary>
        public Dictionary<TLookupEntryKey, TLookupEntry> SelectedItems
        {
            get
            {
                return _selectedItems;
            }
            set
            {
                foreach (var item in value.Values)
                {
                    item.IsSelected = true;
                }

                _selectedItems = value;
                SetReadonlyView();
            }
        }

        public void SetOnlySelection(Dictionary<TLookupEntryKey, TLookupEntry> value)
        {
            foreach (var item in value.Values)
            {
                item.IsSelected = true;
            }

            _selectedItems = value;
            _editView.SetLookupData(_selectedItems.Values.ToList());
            Search();
        }

        public void RefreshViewer()
        {
            SetReadonlyView();
        }

        # endregion
    }
}
