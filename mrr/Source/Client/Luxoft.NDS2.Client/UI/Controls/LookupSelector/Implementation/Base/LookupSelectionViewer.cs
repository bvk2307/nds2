﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Controls.LookupSelector.Implementation.Base
{
    public partial class LookupSelectionViewer : UserControl, ILookupSelectionView
    {
        # region Конструктор

        public LookupSelectionViewer()
        {
            InitializeComponent();
        }

        # endregion

        # region Реализация интерфейса ILookupSelectionView

        public event SimpleEventHandler OnEdit;

        public void SetSelection(string[] selectedItems)
        {
            _selectionList.Text = string.Join(",", selectedItems);
        }

        # endregion

        # region Обработка событий

        private void EditSelection(object sender, EventArgs e)
        {
            if (OnEdit != null)
            {
                OnEdit();
            }
        }

        # endregion
        
    }
}
