﻿namespace Luxoft.NDS2.Client.UI.Controls.LookupSelector.Implementation.Base
{
    partial class LookupSelectionViewer
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this._selectionList = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this._editSelection = new Infragistics.Win.Misc.UltraButton();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._selectionList)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel1.Controls.Add(this._selectionList, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this._editSelection, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(412, 24);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // _selectionList
            // 
            this._selectionList.AutoSize = false;
            this._selectionList.Dock = System.Windows.Forms.DockStyle.Top;
            this._selectionList.Location = new System.Drawing.Point(0, 0);
            this._selectionList.Margin = new System.Windows.Forms.Padding(0);
            this._selectionList.Name = "_selectionList";
            this._selectionList.ReadOnly = true;
            this._selectionList.Size = new System.Drawing.Size(388, 24);
            this._selectionList.TabIndex = 3;
            // 
            // _editSelection
            // 
            this._editSelection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.Image = global::Luxoft.NDS2.Client.Properties.Resources.drop_down;
            appearance1.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance1.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this._editSelection.Appearance = appearance1;
            this._editSelection.Location = new System.Drawing.Point(388, 0);
            this._editSelection.Margin = new System.Windows.Forms.Padding(0);
            this._editSelection.Name = "_editSelection";
            this._editSelection.Size = new System.Drawing.Size(24, 24);
            this._editSelection.TabIndex = 2;
            this._editSelection.Click += new System.EventHandler(this.EditSelection);
            // 
            // LookupSelectionViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.MinimumSize = new System.Drawing.Size(30, 24);
            this.Name = "LookupSelectionViewer";
            this.Size = new System.Drawing.Size(412, 24);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._selectionList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor _selectionList;
        private Infragistics.Win.Misc.UltraButton _editSelection;

    }
}
