﻿using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Controls.LookupSelector
{
    /// <summary>
    /// Этот интерфейс описывает функции элемента управления, который отвечает за отображение выбранных элементов справочника в режиме readonly
    /// </summary>
    public interface ILookupSelectionView
    {
        /// <summary>
        /// Возникает после того, как пользователь запрашивает редактирование выборки
        /// </summary>
        event SimpleEventHandler OnEdit;

        /// <summary>
        /// Задает массив выбранных жлементов справочника для отображения пользователю в режиме "только чтение"
        /// </summary>
        /// <param name="selectedItems">Массив выбранных элементов справочника</param>
        void SetSelection(string[] selectedItems);
    }
}
