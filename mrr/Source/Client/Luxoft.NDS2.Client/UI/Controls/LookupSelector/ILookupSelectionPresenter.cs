﻿using Luxoft.NDS2.Client.Model;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Controls.LookupSelector
{
    /// <summary>
    /// Этот интерфейс описывает функционал презентера элемента управления
    /// </summary>
    /// <typeparam name="TLookupEntryKey">Тип ключа элементов справочника (например. long)</typeparam>
    /// <typeparam name="TLookupEntry">Тип элементов справочника</typeparam>
    public interface ILookupSelectionPresenter<TLookupEntryKey, TLookupEntry>
        where TLookupEntry : ILookupEntry<TLookupEntryKey>
    {
        /// <summary>
        /// Возвращает или задает словарь элементов справочника, выбранных пользователем
        /// </summary>
        Dictionary<TLookupEntryKey, TLookupEntry> SelectedItems
        {
            get;
            set;
        }

        void RefreshViewer();
    }
}
