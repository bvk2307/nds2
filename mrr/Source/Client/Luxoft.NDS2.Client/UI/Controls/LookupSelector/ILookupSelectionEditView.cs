﻿using Luxoft.NDS2.Client.Model;
using System.Collections.Generic;
using System.Drawing;

namespace Luxoft.NDS2.Client.UI.Controls.LookupSelector
{
    public delegate void SimpleEventHandler();

    public delegate void ObjectEventHandler(object sender);

    /// <summary>
    /// Этот интерфейс описывает представление для редактирования выборки записей справочника
    /// </summary>
    public interface ILookupSelectionEditView
    {
        # region Ввод пользователя/настройки

        /// <summary>
        /// Возвращает критерий поиска записей справочника
        /// </summary>
        string SearchPattern { get; }

        /// <summary>
        /// Возвращает максимальное количество элементов справочника, которое можно отобразить
        /// </summary>
        int MaxQuantity { get; }

        # endregion

        # region Вывод пользователю

        /// <summary>
        /// Задает набор элементов справочника для отображения
        /// </summary>
        /// <param name="dataObjects"></param>
        void SetLookupData<TLookupData>(IEnumerable<TLookupData> dataObjects) where TLookupData : ISelectableEntry;       

        /// <summary>
        /// Показывает пользователю текст уведомления
        /// </summary>
        /// <param name="notificationText">Текст уведомления</param>
        void Notify(string notificationText);

        # endregion

        # region Управление состоянием

        /// <summary>
        /// Показывает форму редактирования выборки видимой
        /// </summary>
        void DropDown();

        /// <summary>
        /// Скрывает форму редактирования выборки
        /// </summary>
        void Close();

        # endregion

        # region Действия пользователя

        event SimpleEventHandler OnSave;

        event SimpleEventHandler OnReset;

        event SimpleEventHandler OnCancel;

        event SimpleEventHandler OnSearch;

        event ObjectEventHandler OnSelectionChanged;

        # endregion
    }
}
