﻿namespace Luxoft.NDS2.Client.UI.Controls
{
    partial class TextViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._close = new Infragistics.Win.Misc.UltraButton();
            this._message = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // _close
            // 
            this._close.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this._close.Location = new System.Drawing.Point(81, 229);
            this._close.Margin = new System.Windows.Forms.Padding(6);
            this._close.Name = "_close";
            this._close.Size = new System.Drawing.Size(120, 28);
            this._close.TabIndex = 7;
            this._close.Text = "Закрыть";
            this._close.Click += new System.EventHandler(this.CloseClick);
            // 
            // _message
            // 
            this._message.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._message.Location = new System.Drawing.Point(12, 8);
            this._message.Multiline = true;
            this._message.Name = "_message";
            this._message.ReadOnly = true;
            this._message.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._message.Size = new System.Drawing.Size(260, 212);
            this._message.TabIndex = 8;
            // 
            // TextViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this._message);
            this.Controls.Add(this._close);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "TextViewer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "TextViewer";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.Misc.UltraButton _close;
        private System.Windows.Forms.TextBox _message;
    }
}