﻿using System;

namespace Luxoft.NDS2.Client.UI.Controls.DatePicker
{
    public class DatePickerModel
    {
        public DateTime[] Dates
        {
            get;
            private set;
        }

        //TODO доработать set, чтоб устанавливал ближайшую дату из массива дат или выкидывал ошибку
        public DateTime SelectedDate
        {
            get
            {
                return _selectedDate;
            }
            set
            {
                _selectedDate = value;
                var handler = SelectedDateChanged;
                if (handler != null)
                    handler(this, new EventArgs());
            }
        }

        private DateTime _selectedDate;

        public event EventHandler SelectedDateChanged;

        public DatePickerModel(DateTime[] dates, DateTime selectedDate)
        {
            Dates = dates;
            SelectedDate = selectedDate;
        }
    }
}
