﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Infragistics.Win.UltraWinSchedule;

namespace Luxoft.NDS2.Client.UI.Controls.DatePicker
{
    public partial class DatePickerView : UserControl, IDatePickerView
    {
        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Description("Show/Hide filters")]
        [Category("Luxoft")]
        public string Caption
        {
            get
            {
                return _caption.Text;
            }
            set
            {
                _caption.Text = value;
            }
        }
        
        public DatePickerView()
        {
            InitializeComponent();
        }

        public void Init(DatePickerModel model)
        {
            _picker.Value = model.SelectedDate;

            _picker.KeyPress += (sender, args) => args.Handled = true;

            _picker.ValueChanged += (sender, args) =>
            {
                if (_picker.Value == DBNull.Value)
                    _picker.Value = model.Dates.Last();
                model.SelectedDate = (DateTime)_picker.Value;
            };

            foreach (var btn in _picker.DateButtons)
            {
                if (btn.Type == DateButtonType.Today)
                    btn.Visible = false;
            }

            var info = _picker.CalendarInfo;
            info.MinDate = model.Dates.First();
            info.MaxDate = model.Dates.Last();

            var look = _picker.CalendarLook;
            var date = info.MinDate;
            while (date <= info.MaxDate)
            {
                if (!model.Dates.Contains(date))
                {
                    info.GetDay(date, true).Enabled = false;
                }
                else
                {
                    look.GetDayLook(date, true).Appearance.BackColor = Color.IndianRed;
                }
                date = date.AddDays(1);
            }
        }
    }
}
