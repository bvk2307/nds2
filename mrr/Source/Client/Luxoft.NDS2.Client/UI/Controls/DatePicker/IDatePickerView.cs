﻿
namespace Luxoft.NDS2.Client.UI.Controls.DatePicker
{
    public interface IDatePickerView
    {
        void Init(DatePickerModel model);
    }
}
