﻿
namespace Luxoft.NDS2.Client.UI.Controls.DatePicker
{
    class DatePickerPresenter
    {
        private readonly IDatePickerView _view;
        private readonly DatePickerModel _model;

        public DatePickerPresenter(IDatePickerView view, DatePickerModel model)
        {
            _view = view;
            _model = model;

            _view.Init(_model);
        }
    }
}
