﻿namespace Luxoft.NDS2.Client.UI.Controls.DatePicker
{
    partial class DatePickerView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton dateButton1 = new Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton();
            this._picker = new Infragistics.Win.UltraWinSchedule.UltraCalendarCombo();
            this._caption = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this._picker)).BeginInit();
            this.SuspendLayout();
            // 
            // _picker
            // 
            this._picker.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._picker.DateButtons.Add(dateButton1);
            this._picker.Location = new System.Drawing.Point(380, 3);
            this._picker.Name = "_picker";
            this._picker.NonAutoSizeHeight = 21;
            this._picker.Size = new System.Drawing.Size(148, 21);
            this._picker.TabIndex = 2;
            // 
            // _caption
            // 
            this._caption.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._caption.Location = new System.Drawing.Point(3, 6);
            this._caption.Name = "_caption";
            this._caption.Size = new System.Drawing.Size(371, 17);
            this._caption.TabIndex = 4;
            this._caption.Text = "Дата:";
            // 
            // DatePickerView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._caption);
            this.Controls.Add(this._picker);
            this.Name = "DatePicker";
            this.Size = new System.Drawing.Size(531, 26);
            ((System.ComponentModel.ISupportInitialize)(this._picker)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.UltraWinSchedule.UltraCalendarCombo _picker;
        private Infragistics.Win.Misc.UltraLabel _caption;
    }
}
