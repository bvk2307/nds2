﻿using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;
using Telerik.Windows.Controls.Primitives;

namespace Luxoft.NDS2.Client.UI.Proposals.ResultView
{
    public class SelectionStateManager
    {
        class StateTransition
        {
            public readonly SelectionStatus CurrentState;
            public readonly string Command;

            public StateTransition(SelectionStatus currentState, string command)
            {
                CurrentState = currentState;
                Command = command;
            }

            public override int GetHashCode()
            {
                return 17 + 31 * CurrentState.GetHashCode() + 31 * Command.GetHashCode();
            }

            public override bool Equals(object obj)
            {
                StateTransition other = obj as StateTransition;
                return other != null && this.CurrentState == other.CurrentState && this.Command == other.Command;
            }
        }

        private Dictionary<StateTransition, SelectionStatus> transitions = new Dictionary<StateTransition, SelectionStatus>();
        public SelectionStatus CurrentState { get; set; }

        public SelectionStateManager()
        {
            CurrentState = SelectionStatus.Draft;
        }

        public SelectionStateManager(IList<SelectionTransition> selectionTransitions)
        {
            CurrentState = SelectionStatus.Draft;
            LoadStateTransitions(selectionTransitions);
        }

        public void LoadStateTransitions(IList<SelectionTransition> selectionTransitions)
        {
            string operation = String.Empty;
            transitions.Clear();
            foreach (SelectionTransition selectionTransition in selectionTransitions)
            {
                operation = (selectionTransition.OPERATION != null) ? selectionTransition.OPERATION : String.Empty;
                transitions.Add(new StateTransition(selectionTransition.STATE_FROM, operation), selectionTransition.STATE_TO);
            }
        }

        public SelectionStatus GetNext(string command)
        {
            StateTransition transition = new StateTransition(CurrentState, command);
            SelectionStatus nextState;
            if (!transitions.TryGetValue(transition, out nextState))
                throw new Exception("Invalid transition: " + CurrentState + " -> " + command);
            return nextState;
        }

        public SelectionStatus MoveNext(string command)
        {
            CurrentState = GetNext(command);
            return CurrentState;
        }

        public List<string> GetPossibleOperations(SelectionStatus status)
        {
            return transitions.Keys.Where(x => x.CurrentState == status).Select(x => x.Command).ToList();
        }

        public List<string> GetAvailableOperations()
        {
            return transitions.Keys.Select(x => x.Command).Distinct().ToList();
        }
    }
}
