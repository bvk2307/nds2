﻿namespace Luxoft.NDS2.Client.UI.Controls.StatusChanger
{
    partial class StatusControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ultraButton1 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton2 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton3 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton4 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton5 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton6 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton7 = new Infragistics.Win.Misc.UltraButton();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraButton8 = new Infragistics.Win.Misc.UltraButton();
            this.ultraComboEditor1 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraComboEditor2 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboEditor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboEditor2)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraButton1
            // 
            this.ultraButton1.Location = new System.Drawing.Point(241, 26);
            this.ultraButton1.Name = "ultraButton1";
            this.ultraButton1.Size = new System.Drawing.Size(179, 30);
            this.ultraButton1.TabIndex = 1;
            this.ultraButton1.Text = "Создать";
            this.ultraButton1.Click += new System.EventHandler(this.CreateEvent);
            // 
            // ultraButton2
            // 
            this.ultraButton2.Location = new System.Drawing.Point(241, 79);
            this.ultraButton2.Name = "ultraButton2";
            this.ultraButton2.Size = new System.Drawing.Size(179, 30);
            this.ultraButton2.TabIndex = 2;
            this.ultraButton2.Text = "Удалить";
            this.ultraButton2.Click += new System.EventHandler(this.StateEvent);
            // 
            // ultraButton3
            // 
            this.ultraButton3.Location = new System.Drawing.Point(241, 132);
            this.ultraButton3.Name = "ultraButton3";
            this.ultraButton3.Size = new System.Drawing.Size(179, 30);
            this.ultraButton3.TabIndex = 3;
            this.ultraButton3.Text = "Отправить на согласование";
            this.ultraButton3.Click += new System.EventHandler(this.StateEvent);
            // 
            // ultraButton4
            // 
            this.ultraButton4.Location = new System.Drawing.Point(241, 185);
            this.ultraButton4.Name = "ultraButton4";
            this.ultraButton4.Size = new System.Drawing.Size(179, 30);
            this.ultraButton4.TabIndex = 4;
            this.ultraButton4.Text = "Утвердить";
            this.ultraButton4.Click += new System.EventHandler(this.StateEvent);
            // 
            // ultraButton5
            // 
            this.ultraButton5.Location = new System.Drawing.Point(241, 238);
            this.ultraButton5.Name = "ultraButton5";
            this.ultraButton5.Size = new System.Drawing.Size(179, 30);
            this.ultraButton5.TabIndex = 5;
            this.ultraButton5.Text = "Отклонить";
            this.ultraButton5.Click += new System.EventHandler(this.StateEvent);
            // 
            // ultraButton6
            // 
            this.ultraButton6.Location = new System.Drawing.Point(241, 291);
            this.ultraButton6.Name = "ultraButton6";
            this.ultraButton6.Size = new System.Drawing.Size(179, 30);
            this.ultraButton6.TabIndex = 6;
            this.ultraButton6.Text = "Отправить на доработку";
            this.ultraButton6.Click += new System.EventHandler(this.StateEvent);
            // 
            // ultraButton7
            // 
            this.ultraButton7.Location = new System.Drawing.Point(241, 344);
            this.ultraButton7.Name = "ultraButton7";
            this.ultraButton7.Size = new System.Drawing.Size(179, 30);
            this.ultraButton7.TabIndex = 7;
            this.ultraButton7.Text = "Отправить в СЭОД";
            this.ultraButton7.Click += new System.EventHandler(this.StateEvent);
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Location = new System.Drawing.Point(486, 30);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(116, 23);
            this.ultraLabel1.TabIndex = 8;
            this.ultraLabel1.Text = "Присвоить выборке:";
            // 
            // ultraButton8
            // 
            this.ultraButton8.Location = new System.Drawing.Point(814, 22);
            this.ultraButton8.Name = "ultraButton8";
            this.ultraButton8.Size = new System.Drawing.Size(118, 30);
            this.ultraButton8.TabIndex = 10;
            this.ultraButton8.Text = "Присвоить";
            this.ultraButton8.Click += new System.EventHandler(this.ultraButton8_Click);
            // 
            // ultraComboEditor1
            // 
            this.ultraComboEditor1.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.ultraComboEditor1.Location = new System.Drawing.Point(26, 28);
            this.ultraComboEditor1.Name = "ultraComboEditor1";
            this.ultraComboEditor1.Size = new System.Drawing.Size(198, 21);
            this.ultraComboEditor1.TabIndex = 0;
            this.ultraComboEditor1.ValueChanged += new System.EventHandler(this.ultraCombo1_ValueChanged);
            // 
            // ultraComboEditor2
            // 
            this.ultraComboEditor2.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.ultraComboEditor2.Location = new System.Drawing.Point(604, 28);
            this.ultraComboEditor2.Name = "ultraComboEditor2";
            this.ultraComboEditor2.Size = new System.Drawing.Size(198, 21);
            this.ultraComboEditor2.TabIndex = 9;
            // 
            // StatusControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ultraComboEditor2);
            this.Controls.Add(this.ultraComboEditor1);
            this.Controls.Add(this.ultraLabel1);
            this.Controls.Add(this.ultraButton8);
            this.Controls.Add(this.ultraButton7);
            this.Controls.Add(this.ultraButton6);
            this.Controls.Add(this.ultraButton5);
            this.Controls.Add(this.ultraButton4);
            this.Controls.Add(this.ultraButton3);
            this.Controls.Add(this.ultraButton2);
            this.Controls.Add(this.ultraButton1);
            this.Name = "StatusControl";
            this.Size = new System.Drawing.Size(950, 408);
            this.Load += new System.EventHandler(this.StatusControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboEditor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboEditor2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.Misc.UltraButton ultraButton1;
        private Infragistics.Win.Misc.UltraButton ultraButton2;
        private Infragistics.Win.Misc.UltraButton ultraButton3;
        private Infragistics.Win.Misc.UltraButton ultraButton4;
        private Infragistics.Win.Misc.UltraButton ultraButton5;
        private Infragistics.Win.Misc.UltraButton ultraButton6;
        private Infragistics.Win.Misc.UltraButton ultraButton7;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraButton ultraButton8;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor ultraComboEditor1;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor ultraComboEditor2;
    }
}
