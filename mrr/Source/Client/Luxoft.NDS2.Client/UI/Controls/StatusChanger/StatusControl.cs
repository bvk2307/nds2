﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Infragistics.Win.Misc;
using Luxoft.NDS2.Client.UI.Proposals.ResultView;
using Luxoft.NDS2.Common.Contracts;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;

namespace Luxoft.NDS2.Client.UI.Controls.StatusChanger
{
    public partial class StatusControl : UserControl
    {
        private readonly Dictionary<UltraButton, string> buttonToOperation = new Dictionary<UltraButton, string>();
        private readonly SelectionStateManager mng = new SelectionStateManager();
        private readonly List<Common.Contracts.DTO.Business.Selection.Selection> selectionsTable = null;

        private readonly Dictionary<int, string> statTable = null;

        public StatusControl()
        {
            InitializeComponent();

            buttonToOperation.Add(ultraButton2, Constants.SystemPermissions.Operations.SelectionRemove);
            buttonToOperation.Add(ultraButton3, Constants.SystemPermissions.Operations.SelectionToApprove);
            buttonToOperation.Add(ultraButton4, Constants.SystemPermissions.Operations.SelectionApproved);
            buttonToOperation.Add(ultraButton6, Constants.SystemPermissions.Operations.SelectionToCorrect);
        }


        public void LoadStateTransitions(IList<SelectionTransition> selectionTransitions)
        {
            mng.LoadStateTransitions(selectionTransitions);
        }

        private void StatusControl_Load(object sender, EventArgs e)
        {
            UpdateSatus();
        }

        private void ultraCombo1_ValueChanged(object sender, EventArgs e)
        {
            UpdateSatus();
        }

        private void UpdateSatus()
        {
            List<string> accessibleOperations = null;

            if (ultraComboEditor1.SelectedItem != null)
            {
                var name = ultraComboEditor1.SelectedItem.DisplayText;
                var currentItem = statTable.Where(x => x.Value == name).FirstOrDefault();

                mng.CurrentState = (SelectionStatus) currentItem.Key;
                accessibleOperations = mng.GetPossibleOperations((SelectionStatus) currentItem.Key);
            }
            else
            {
                accessibleOperations = new List<string>();
            }

            foreach (var item in buttonToOperation)
                if (accessibleOperations.Contains(item.Value))
                    item.Key.Enabled = true;
                else
                    item.Key.Enabled = false;
        }

        private void CreateEvent(object sender, EventArgs e)
        {
            ultraComboEditor1.SelectedItem = ultraComboEditor1.Items[0];
        }

        private void StateEvent(object sender, EventArgs e)
        {
            var op = buttonToOperation[(UltraButton) sender];


            var nextOp = mng.MoveNext(op);

            var name = statTable[(int) nextOp];

            foreach (var row in ultraComboEditor1.Items)
                if (row.DisplayText == name)
                    ultraComboEditor1.SelectedItem = row;
        }

        private void ultraButton8_Click(object sender, EventArgs e)
        {
            if ((ultraComboEditor1.SelectedItem != null) && (ultraComboEditor2.SelectedItem != null))
            {
                var name = ultraComboEditor1.SelectedItem.DisplayText;
                var currentItem1 = statTable.Where(x => x.Value == name).FirstOrDefault();

                name = ultraComboEditor2.SelectedItem.DisplayText;
                var currentItem2 = selectionsTable.Where(x => x.Name == name).FirstOrDefault();


                MessageBox.Show("Готово");
            }
        }
    }
}