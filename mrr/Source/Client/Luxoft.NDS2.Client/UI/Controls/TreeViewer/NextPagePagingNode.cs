﻿namespace Luxoft.NDS2.Client.UI.Controls.TreeViewer
{
    public class NextPagePagingNode : PagingNode
    {
        public NextPagePagingNode(int offset, int pageSize, int total)
            : this(new NullDataNode(), offset, pageSize, total)
        {
        }

        public NextPagePagingNode(DataNode parent, int offset, int pageSize, int total)
            : base(
                string.Format(
                    "{0}-{1} >>", 
                    offset + 1, 
                    total > offset + pageSize ? offset + pageSize + 1 : total),
                parent, 
                offset)
        {
        }
    }
}
