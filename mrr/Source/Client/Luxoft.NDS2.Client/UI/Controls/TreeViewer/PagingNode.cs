﻿using System;

namespace Luxoft.NDS2.Client.UI.Controls.TreeViewer
{
    /// <summary>
    /// Этот класс представляет
    /// </summary>
    public abstract class PagingNode : TreeNodeBase
    {
        protected PagingNode(string text, DataNode parent, int offset)
            : base(text)
        {
            Parent = parent;
            Offset = offset;
        }

        public DataNode Parent
        {
            get;
            private set;
        }

        public int Offset
        {
            get;
            private set;
        }

        public bool IsRoot
        {
            get
            {
                return Parent is NullDataNode;
            }
        }
    }
}
