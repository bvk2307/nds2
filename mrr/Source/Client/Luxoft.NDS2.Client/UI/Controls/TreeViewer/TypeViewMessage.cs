﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.Controls.TreeViewer
{
    public enum TypeViewMessage
    {
        NotifyMessage = 1,
        ErrorMessage = 2
    }
}
