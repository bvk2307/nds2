﻿using CommonComponents.Utils.Async;
using DocumentFormat.OpenXml.Wordprocessing;
using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.Model;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Pyramid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Luxoft.NDS2.Client.UI.Controls.TreeViewer
{
    public enum AutoStartMode { OnViewLoad, None }
    /// <summary>
    /// Этот класс описывает реализацию презентера асинхронного дерева 
    /// </summary>
    public class Presenter<TKey, TElement>
        where TElement : IIdentifiableEntity<TKey>
    {
        # region Поля

        private readonly IDataLoader<TKey, TElement> _dataLoader;

        private readonly Func<string, TKey> _keyConverter;

        # endregion

        # region Конструкторы

        public Presenter(
            IView view,
            IDataLoader<TKey, TElement> dataLoader,
            TreeNodeManager<TKey, TElement> nodeManager,
            Func<string, TKey> keyConverter,
            AutoStartMode autoStartMode = AutoStartMode.OnViewLoad)
        {
            View = view;
            _dataLoader = dataLoader;
            NodeManager = nodeManager;
            _keyConverter = keyConverter;

            view.SelectedNodeChanged += (args, lvl) => SelectedNodeChanged(args, lvl);
            view.NodeExpanding += (args) => NodeExpanding(args);

            if (autoStartMode == AutoStartMode.OnViewLoad)
            {
                view.Loading += () => StartBuilding();
            }
        }

        # endregion

        # region Представление

        protected IView View
        {
            get;
            private set;
        }

        # endregion

        # region Открытые свойства и методы

        public void StartBuilding()
        {
            View.ClearNodes();
            NodeManager.ClearCache();

            var rootPending = NodeManager.PendingNode();
            View.ReplaceRootNodes(new TreeNodeBase[] { rootPending });

            Action action = () => RootLoadAndShow();
            action.DoAsync();
        }

        protected TreeNodeManager<TKey, TElement> NodeManager
        {
            get;
            private set;
        }

        # endregion

        # region Обработка событий представления

        private void SelectedNodeChanged(string key, int lvl)
        {
            var paging = NodeManager.GetNode<PagingNode>(key);

            if (paging == null)
            {
                return;
            }

            if (!paging.IsRoot)
            {
                LoadAndShow(((PagingNode)paging).Parent, paging.Offset);

            }
            else
            {
                RootLoadAndShow(paging.Offset);
            }
        }

        private void NodeExpanding(string key)
        {
            var dataNode = NodeManager.GetNode<DataNode>(key);

            if (dataNode == null)
            {
                return;
            }

            if (dataNode.LoadingStatus == DataLoadingStatus.Completed)
            {
                Action action =
                    () =>
                    {
                        foreach (var node in NodeManager.PullChildren(dataNode.Key))
                        {
                            NodeVisible(node);
                        }
                    };
                action.DoAsync();
            }
            else if (dataNode.LoadingStatus == DataLoadingStatus.NotStarted)
            {
                StartLoadingContent(dataNode);
            }
        }

        # endregion

        # region Отслеживание выполнения запросов данных

        private const int WaitTimeout = 2000;

        protected void LoadDataAsync(DataNode node)
        {
            Action action =
                () =>
                {
                    if (node != null
                        && WaitData(node) == DataLoadingStatus.Completed)
                    {
                        LoadAndShow(node);
                    }
                };
            action.DoAsync();
        }

        protected DataLoadingStatus WaitData(DataNode node)
        {
            do
            {
                node.LoadingStatus = _dataLoader.Status(_keyConverter(node.DataItemKey));

                if (!node.LoadingStatus.IsFinal())
                {
                    Thread.Sleep(WaitTimeout);
                }
            }
            while (!node.LoadingStatus.IsFinal());

            WaitCompleted(node);

            return node.LoadingStatus;
        }       

        # endregion

        # region Загрузка и отображение данных в дереве

        protected void StartLoadingContent(DataNode node)
        {
            Action action =
                () =>
                {
                    WaitStarted(node);
                    _dataLoader.StartLoading(_keyConverter(node.DataItemKey));
                    LoadDataAsync(node);
                };
            action.DoAsync();
        }

        protected void RootLoadAndShow(int offset = 0)
        {
            var items = _dataLoader.Load(offset, View.PageSize);
            
            if (!items.Rows.Any())
            {
                View.ClearNodes();
                return;
            }

            var nodes = new List<TreeNodeBase>();

            if (offset > 0)
            {
                nodes.Add(NodeManager.RootPrevPageNode(offset));
            }

            nodes.AddRange(items.Rows.Select(x => NodeManager.RootDataNode(x)));

            if (items.TotalMatches > offset + View.PageSize)
            {
                nodes.Add(NodeManager.RootNextPageNode(offset, items.TotalMatches));
            }

            View.ReplaceRootNodes(nodes.ToArray());

            foreach (var node in nodes)
            {
                NodeAdded(node);
            }
        }

        protected void LoadAndShow(DataNode parentNode, int offset = 0)
        {
            var items = 
                _dataLoader.Load(
                    _keyConverter(parentNode.DataItemKey), 
                    offset, 
                    View.PageSize);

            if (!items.Rows.Any())
            {
                View.ClearNodes(parentNode.Key);
                return;
            }

            var nodes = new List<TreeNodeBase>();

            if (offset > 0)
            {
                nodes.Add(NodeManager.PrevPageNode(parentNode, offset));
            }

            nodes.AddRange(items.Rows.Select(x => NodeManager.DataNode(x, parentNode)));

            if (items.TotalMatches > offset + View.PageSize)
            {
                nodes.Add(NodeManager.NextPageNode(parentNode, offset, items.TotalMatches));
            }

            View.ReplaceNodes(parentNode.Key, nodes.ToArray());

            var isExpanded = View.IsNodeExpanded(parentNode.Key.ToString());

            foreach (var node in nodes)
            {
                NodeAdded(node);

                if (isExpanded)
                {
                    NodeVisible(node);
                }
            }
        }

        # endregion

        # region Переопределяемые обработчики событий

        protected virtual void NodeAdded(TreeNodeBase node)
        {
            var dataNode = node as DataNode;

            if (dataNode != null)
            {
                View.ReplaceNodes(
                    node.Key,
                    new TreeNodeBase[] { NodeManager.PendingNode() });
            }
        }

        protected virtual void NodeVisible(TreeNodeBase node)
        {
        }

        protected virtual void WaitStarted(DataNode data)
        {
        }

        protected virtual void WaitCompleted(DataNode node)
        {
        }

        # endregion
    }

    public static class DataLoadingStatusHelper
    {
        public static bool IsFinal(this DataLoadingStatus status)
        {
            return status != DataLoadingStatus.InProcess;
        }
    }
}
