﻿using Luxoft.NDS2.Client.UI.Base;

namespace Luxoft.NDS2.Client.UI.Controls.TreeViewer
{
    /// <summary>
    /// Этот интерфейс описывает представление дерева
    /// </summary>
    public interface IView
    {
        # region Управление деревом контрагентов

        void ClearNodes();

        void ClearNodes(string parentKey);

        void ReplaceRootNodes(TreeNodeBase[] context);

        void AddNodes(string parentKey, TreeNodeBase[] nodes);

        void ReplaceNodes(string parentKey, TreeNodeBase[] nodes);

        void SelectNode(string key);

        # endregion

        # region События

        event ParameterlessEventHandler Loading;

        event GenericEventHandler<string, int> SelectedNodeChanged;

        event GenericEventHandler<string> NodeExpanding;

        # endregion

        # region Настройка представления

        int PageSize { get; }

        # endregion

        # region Состояние

        bool IsNodeExpanded(string key);

        # endregion
    }
}
