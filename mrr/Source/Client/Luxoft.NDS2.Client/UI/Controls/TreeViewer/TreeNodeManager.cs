﻿using Luxoft.NDS2.Client.Model;
using Luxoft.NDS2.Client.UI.Base;
using System;

namespace Luxoft.NDS2.Client.UI.Controls.TreeViewer
{
    public class TreeNodeManager<TKey, TElement>
        where TElement : IIdentifiableEntity<TKey>
    {
        # region Конструктор

        private readonly ITreeNodeStorage _storage;

        private readonly int _pageSize;

        private readonly NullDataNode _nullNode = new NullDataNode();

        public TreeNodeManager(
            ITreeNodeStorage storage,
            int pageSize)
        {
            _storage = storage;
            _pageSize = pageSize;
        }

        # endregion

        # region Публичные методы построения узлов дерева

        public DataNode DataNode(TElement data, DataNode parent)
        {
            var context = parent.ChildNode(data.ToString(), data.Key.ToString());                
            _storage.Push(context.Key, context);

            return context;
        }

        public DataNode RootDataNode(TElement data)
        {
            var context = DataNode(data, _nullNode);
            _storage.Push(context.Key, context);

            return context;
        }

        public PreviousPagePagingNode PrevPageNode(DataNode parent, int offset)
        {
            var context = parent.PrevPage(offset, _pageSize);
            _storage.Push(context.Key, context);

            return context;
        }

        public PreviousPagePagingNode RootPrevPageNode(int offset)
        {
            return PrevPageNode(_nullNode, offset);
        }

        public NextPagePagingNode NextPageNode(DataNode parent, int offset, int total)
        {
            var context = parent.NextPage(offset, _pageSize, total);
            _storage.Push(context.Key, context);

            return context;
        }

        public NextPagePagingNode RootNextPageNode(int offset, int total)
        {
            return NextPageNode(_nullNode, offset, total);
        }

        public virtual PendingNode PendingNode()
        {
            var context = _nullNode.Pending(PendingNodeTitle());
            _storage.Push(context.Key, context);

            return context;
        }

        # endregion

        # region Перегружаемые методы

        protected virtual string PendingNodeTitle()
        {
            return "Загрузка...";
        }

        # endregion

        # region Публичные методы управления кэшем

        public void ClearCache()
        {
            _storage.Clear();
        }

        public TNode GetNode<TNode>(string key)
            where TNode : TreeNodeBase
        {
            TreeNodeBase result;

            if (_storage.Pull(key, out result))
            {
                return result as TNode;
            }

            return null;
        }

        public TreeNodeBase[] PullChildren(string parentKey)
        {
            return _storage.PullChildren(parentKey);
        }

        # endregion
    }
}
