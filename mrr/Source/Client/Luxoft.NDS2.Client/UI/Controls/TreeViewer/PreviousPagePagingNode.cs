﻿namespace Luxoft.NDS2.Client.UI.Controls.TreeViewer
{
    public class PreviousPagePagingNode : PagingNode
    {
        public PreviousPagePagingNode(int offset, int pageSize)
            : this(new NullDataNode(), offset, pageSize)
        {
        }

        public PreviousPagePagingNode(DataNode parent, int offset, int pageSize)
            : base(string.Format("<< {0}-{1}", offset + 1, offset + pageSize), parent, offset)
        {
        }
    }
}
