﻿namespace Luxoft.NDS2.Client.UI.Controls.TreeViewer
{
    public enum DataLoadingStatus
    {
        NotStarted = -2,
        NotRegistered = -1,
        Ok = 0,
        InProcess = 1,
        Completed = 2,
        PartialCompleted = 3,
        NotSuccessInNightMode = 8,
        Error = 9
    }
}
