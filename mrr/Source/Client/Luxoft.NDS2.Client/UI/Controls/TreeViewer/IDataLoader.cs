﻿using Luxoft.NDS2.Client.Model;
using Luxoft.NDS2.Common.Contracts.DTO;

namespace Luxoft.NDS2.Client.UI.Controls.TreeViewer
{
    /// <summary>
    /// Этот интерфейс описывает методы загрузчика данных для асинхронного дерева
    /// </summary>
    /// <typeparam name="TTreeElementKey">Тип ключа элемента дерева</typeparam>
    /// <typeparam name="TTreeElement">Тип элемента дерева</typeparam>
    public interface IDataLoader<TTreeElementKey, TTreeElement>
        where TTreeElement : IIdentifiableEntity<TTreeElementKey>
    {
        /// <summary>
        /// Указывает источнику данных, что необходимо начать подготовку/загрузку дочерних узлов
        /// </summary>
        /// <param name="parentKey">Ключ элемента в родительском узле</param>
        void StartLoading(TTreeElementKey parentKey);

        /// <summary>
        /// Запрашивает статус подготовки данных дочерних узлов
        /// </summary>
        /// <param name="parentKey">Ключ элемента родительского узла</param>
        /// <returns>Статус подготовки данных</returns>
        DataLoadingStatus Status(TTreeElementKey parentKey);

        /// <summary>
        /// Запрашивает одну страницу данных для постороения дочерних элементов
        /// </summary>
        /// <param name="parentKey">Ключ элемента родительского узла</param>
        /// <param name="offset">Кол-во элементов, которые необходимо пропустить</param>
        /// <param name="pageSize">Максимальное количество элементов, которые необходимо загрузить</param>
        /// <returns>Результат выполнения запроса</returns>
        PageResult<TTreeElement> Load(TTreeElementKey parentKey, int offset, int pageSize);

        /// <summary>
        /// Запрашивает одну страницу корневых элементов дерева
        /// </summary>
        /// <param name="offset">Ключ элемента родительского узла</param>
        /// <param name="pageSize">Максимальное количество элементов, которые необходимо вернуть</param>
        /// <returns>Результат выполнения запроса</returns>
        PageResult<TTreeElement> Load(int offset, int pageSize);
    }
}
