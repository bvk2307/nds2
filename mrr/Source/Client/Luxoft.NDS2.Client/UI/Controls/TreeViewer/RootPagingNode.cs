﻿namespace Luxoft.NDS2.Client.UI.Controls.TreeViewer
{
    public class RootPagingNode : TreeNodeBase
    {
        public int Offset
        {
            get;
            set;
        }
    }
}
