﻿using System;

namespace Luxoft.NDS2.Client.UI.Controls.TreeViewer
{
    public class PendingNode : TreeNodeBase
    {
        public PendingNode(string text)
            : base(text)
        {
        }
    }
}
