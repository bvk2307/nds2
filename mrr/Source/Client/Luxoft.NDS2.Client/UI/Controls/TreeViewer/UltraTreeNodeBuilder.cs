﻿using Infragistics.Win.UltraWinTree;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Controls.TreeViewer
{
    /// <summary>
    /// Этот класс реализует построитель узлов UltraTreeView
    /// </summary>
    public class UltraTreeNodeBuilder
    {
        private readonly Dictionary<Type, Action<UltraTreeNode>> _formatters =
            new Dictionary<Type, Action<UltraTreeNode>>();

        /// <summary>
        /// Регистрирует форматировщик узлов дерева
        /// </summary>
        /// <typeparam name="TNode">Тип узла дерева</typeparam>
        /// <param name="formatter">Форматировщик</param>
        public void AddFormatter<TNode>(Action<UltraTreeNode> formatter)
            where TNode : TreeNodeBase
        {
            _formatters.Add(typeof(TNode), formatter);
        }

        /// <summary>
        /// Строит визуальный узел дерева
        /// </summary>
        /// <param name="node">Узел дерева</param>
        /// <returns>ССылка на визуальный узел дерева</returns>
        public UltraTreeNode Build(TreeNodeBase node)
        {
            var viewNode = new UltraTreeNode(node.Key, node.Text);

            if (_formatters.ContainsKey(node.GetType()))
            {
                _formatters[node.GetType()](viewNode);
            }

            return viewNode;
        }
    }
}
