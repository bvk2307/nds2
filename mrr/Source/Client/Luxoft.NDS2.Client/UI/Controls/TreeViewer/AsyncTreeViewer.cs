﻿using Infragistics.Win.UltraWinTree;
using Luxoft.NDS2.Client.UI.Base;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Controls.TreeViewer
{
    public partial class AsyncTreeViewer : UserControl, IView
    {
        # region Поля

        private readonly UltraTreeNodeBuilder _nodeBuilder =
            new UltraTreeNodeBuilder();

        private IKeyValueStorage<string, UltraTreeNode> _nodeStorage;

        # endregion

        # region Конструктор

        public AsyncTreeViewer()
        {
            InitializeComponent();

            _tree.AfterSelect +=
                (sender, args) =>
                {
                    if (SelectedNodeChanged != null
                        && _tree.SelectedNodes.Count > 0)
                    {
                        SelectedNodeChanged(_tree.SelectedNodes[0].Key, _tree.SelectedNodes[0].Level);
                    }
                };

            _tree.BeforeExpand +=
                (sender, args) =>
                {
                    if (NodeExpanding != null)
                    {
                        NodeExpanding(args.TreeNode.Key);
                    }
                };

            Load +=
                (sender, args) =>
                {
                    if (Loading != null)
                    {
                        Loading();
                    }
                };
        }

        # endregion

        # region Настройка

        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Description("Макс. кол-во ветвей у ноды")]
        [Category("Custom")]
        public string Title
        {
            get 
            { 
                return _title.Text; 
            }
            set 
            { 
                _title.Text = value; 
            }
        }

        public void AddFormatter<TNode>(Action<UltraTreeNode> formatter)
            where TNode : TreeNodeBase
        {
            _nodeBuilder.AddFormatter<TNode>(formatter);
        }

        public void SetNodeStorage(IKeyValueStorage<string, UltraTreeNode> storage)
        {
            _nodeStorage = storage;
        }

        # endregion

        # region Объявление внутренних делегатов

        private delegate void ContextDelegate();

        private delegate void RootContextDelegate(TreeNodeBase[] context);

        private delegate void BranchContextDelegate(string parentKey, TreeNodeBase[] children);

        # endregion

        # region Реализация интерфейса IView

        public void ClearNodes()
        {
            InvokeIfRequired(() => Clear());
        }

        public void ClearNodes(string parentKey)
        {
            InvokeIfRequired(() => Clear(parentKey));
        }

        public void ReplaceRootNodes(TreeNodeBase[] context)
        {
            InvokeIfRequired(() => { Clear(); AddRootNodes(context); });
        }

        public void AddNodes(string parentKey, TreeNodeBase[] nodes)
        {
            InvokeIfRequired(() => AddRange(parentKey, nodes));
        }

        public void ReplaceNodes(string parentKey, TreeNodeBase[] nodes)
        {        
            InvokeIfRequired(
                (argX, argY) =>
                {
                    Clear(argX);
                    AddRange(argX, argY);
                },
                parentKey,
                nodes);
        }

        public void SelectNode(string key)  
        {
            var node = Search(key);

            if (node != null)
            {
                InvokeIfRequired(() => SelectNode(node));
            }
        }

        public bool IsNodeExpanded(string key)
        {
            var node = Search(key);

            return node != null && node.Expanded;
        }

        public event GenericEventHandler<string, int> SelectedNodeChanged;

        public event GenericEventHandler<string> NodeExpanding;

        public event ParameterlessEventHandler Loading;

        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Description("Макс. кол-во ветвей у ноды")]
        [Category("Custom")]
        public int PageSize { get; set; }

        # endregion

        # region Работа с элементами дерева

        private void SelectNode(UltraTreeNode node)
        {
            _tree.Focus();
            _tree.ActiveNode = node;
            _tree.ActiveNode.Selected = true;            
        }

        private void Clear()
        {
            foreach (var node in _tree.Nodes)
            {
                Clear(node.Key);
            }
            _tree.Nodes.Clear();
        }

        private void Clear(string parentKey)
        {
            var parent = Search(parentKey);

            if (parent == null)
            {
                return;
            }

            foreach(var node in parent.Nodes)
            {
                Clear(node.Key);
                _nodeStorage.Release(node.Key);
            }

            parent.Nodes.Clear();
        }

        private void AddRange(string parentKey, TreeNodeBase[] itemsContext)
        {
            var parent = Search(parentKey);

            if (parent == null)
            {
                return;
            }

            foreach (var item in itemsContext)
            {
                var node = _nodeBuilder.Build(item);
                _nodeStorage.Push(item.Key, node);
                parent.Nodes.Add(node);
            }
        }

        private void AddRootNodes(TreeNodeBase[] context)
        {
            foreach (var nodeContext in context)
            {
                var root = _nodeBuilder.Build(nodeContext);
                _nodeStorage.Push(nodeContext.Key, root);
                _tree.Nodes.Add(root);
            }
        }

        private UltraTreeNode Search(string key)
        {
            if (_tree.Nodes.Count == 0)
            {
                return null;
            }

            UltraTreeNode result;

            if (_nodeStorage.Pull(key, out result))
            {
                return result;
            }

            return null; // Search(key, _tree.Nodes[0]); Этот код понадобится, если возникнет необходимость удалять объекты из хранилища по мере наполнения без вызова Release
                        // До этого момента считаем, что если узла нет в хранилище, значит уже нет в дереве
        }

        private UltraTreeNode Search(string key, UltraTreeNode parent)
        {
            if (parent.Key == key)
            {
                return parent;
            }

            foreach (var node in parent.Nodes)
            {
                var result = Search(key, node);

                if (result != null)
                {
                    return result;
                }
            }

            return null;
        }

        # endregion

        # region Многопоточность

        private void InvokeIfRequired(Action action)
        {
            if (_tree.InvokeRequired)
            {
                _tree.BeginInvoke(new ContextDelegate(action)).AsyncWaitHandle.WaitOne();
            }
            else
            {
                action();
            }
        }

        private void InvokeIfRequired(Action<TreeNodeBase[]> action, TreeNodeBase[] context)
        {
            if (_tree.InvokeRequired)
            {
                _tree.BeginInvoke(new RootContextDelegate(action), context).AsyncWaitHandle.WaitOne();
            }
            else
            {
                action(context);
            }
        }

        private void InvokeIfRequired(
            Action<string,TreeNodeBase[]> action,
            string parentKey,
            TreeNodeBase[] children)
        {
            if (_tree.InvokeRequired)
            {
                _tree.BeginInvoke(new BranchContextDelegate(action), parentKey, children).AsyncWaitHandle.WaitOne();
            }
            else
            {
                action(parentKey, children);
            }
        }

        # endregion
    }
}
