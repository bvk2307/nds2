﻿using Luxoft.NDS2.Client.Model;
using Luxoft.NDS2.Client.UI.Base;

namespace Luxoft.NDS2.Client.UI.Controls.TreeViewer
{
    public interface ITreeNodeStorage : IKeyValueStorage<string, TreeNodeBase>
    {
        TreeNodeBase[] PullChildren(string parentKey);
    }
}
