﻿namespace Luxoft.NDS2.Client.UI.Controls.TreeViewer
{
    partial class AsyncTreeViewer
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            this._title = new Infragistics.Win.Misc.UltraLabel();
            this._tree = new Infragistics.Win.UltraWinTree.UltraTree();
            this.gridLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this._tree)).BeginInit();
            this.gridLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // _title
            // 
            appearance1.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance1.TextHAlignAsString = "Left";
            appearance1.TextVAlignAsString = "Middle";
            this._title.Appearance = appearance1;
            this._title.Dock = System.Windows.Forms.DockStyle.Fill;
            this._title.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._title.Location = new System.Drawing.Point(3, 3);
            this._title.Name = "_title";
            this._title.Size = new System.Drawing.Size(223, 26);
            this._title.TabIndex = 1;
            // 
            // _tree
            // 
            this._tree.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tree.ImageTransparentColor = System.Drawing.Color.Transparent;
            this._tree.Location = new System.Drawing.Point(3, 35);
            this._tree.Name = "_tree";
            this._tree.NodeConnectorColor = System.Drawing.SystemColors.ControlDark;
            this._tree.Size = new System.Drawing.Size(223, 198);
            this._tree.TabIndex = 2;
            // 
            // gridLayoutPanel
            // 
            this.gridLayoutPanel.ColumnCount = 1;
            this.gridLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.gridLayoutPanel.Controls.Add(this._title, 0, 0);
            this.gridLayoutPanel.Controls.Add(this._tree, 0, 1);
            this.gridLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.gridLayoutPanel.Name = "gridLayoutPanel";
            this.gridLayoutPanel.RowCount = 3;
            this.gridLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.gridLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.gridLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 9F));
            this.gridLayoutPanel.Size = new System.Drawing.Size(229, 245);
            this.gridLayoutPanel.TabIndex = 3;
            // 
            // AsyncTreeViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridLayoutPanel);
            this.Name = "AsyncTreeViewer";
            this.Size = new System.Drawing.Size(229, 245);
            ((System.ComponentModel.ISupportInitialize)(this._tree)).EndInit();
            this.gridLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraLabel _title;
        private Infragistics.Win.UltraWinTree.UltraTree _tree;
        private System.Windows.Forms.TableLayoutPanel gridLayoutPanel;

    }
}
