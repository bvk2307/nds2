﻿namespace Luxoft.NDS2.Client.UI.Controls.TreeViewer
{
    public class NullDataNode : DataNode
    {
        public NullDataNode()
            : base(string.Empty, string.Empty, -1)
        {
        }
    }
}
