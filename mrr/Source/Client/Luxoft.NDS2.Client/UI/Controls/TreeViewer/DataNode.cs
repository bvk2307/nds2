﻿namespace Luxoft.NDS2.Client.UI.Controls.TreeViewer
{
    /// <summary>
    /// Этот класс описывает узел дерева с данными
    /// </summary>
    public class DataNode : TreeNodeBase
    {
        public DataNode(string text, string dataItemKey)
            : this(text, dataItemKey, 0)
        {
        }

        public DataNode(string text, string dataItemKey, int level)
            : base(text)
        {
            DataItemKey = dataItemKey;
            Level = level;
            LoadingStatus = DataLoadingStatus.NotStarted;
        }

        public bool IsRoot
        {
            get
            {
                return Level == 0;
            }
        }

        public int Level
        {
            get;
            private set;
        }

        public string DataItemKey
        {
            get;
            private set;
        }

        public DataLoadingStatus LoadingStatus
        {
            get;
            set;
        }

        public PendingNode Pending(string text)
        {
            return new PendingNode(text);
        }

        public NextPagePagingNode NextPage(int offset, int pageSize, int total)
        {
            return new NextPagePagingNode(this, offset + pageSize, pageSize, total);
        }

        public PreviousPagePagingNode PrevPage(int offset, int pageSize)
        {
            return new PreviousPagePagingNode(this, offset - pageSize, pageSize);
        }

        public DataNode ChildNode(string text, string dataItemKey)
        {
            return new DataNode(text, dataItemKey, Level + 1);
        }
    }
}
