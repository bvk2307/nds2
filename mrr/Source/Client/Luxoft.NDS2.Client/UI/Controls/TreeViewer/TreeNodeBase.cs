﻿using System;

namespace Luxoft.NDS2.Client.UI.Controls.TreeViewer
{
    /// <summary>
    /// Этот класс представляет узел дерева
    /// </summary>
    public abstract class TreeNodeBase
    {
        protected TreeNodeBase(string text)
        {
            Key = Guid.NewGuid().ToString();
            Text = text;
        }

        public string Key
        {
            get;
            private set;
        }

        public string Text
        {
            get;
            private set;
        }
    }
}
