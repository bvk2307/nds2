﻿using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    class ToolTipFormatter<TModel> : IGridColumnFormatter
    {
        private readonly Func<TModel, string> _getToolTip;
        private readonly UltraGrid _grid;

        public ToolTipFormatter(Func<TModel, string> getToolTip, UltraGrid grid)
        {
            _getToolTip = getToolTip;
            _grid = grid;
        }
        public UltraGridColumn ApplyFormat(UltraGridColumn column)
        {

            _grid.InitializeRow += (sender, args) =>
            {
                var row = args.Row;

                if (!row.Cells.Exists(column.Key))
                    return;

                var cell = row.Cells[column.Key];
                cell.ToolTipText = _getToolTip((TModel)row.ListObject);
            };
            return column;
        }

        public List<UltraGridColumn> ApplyFormat(List<UltraGridColumn> columns)
        {

            _grid.InitializeRow += (sender, args) =>
            {
                var row = args.Row;

                if (!row.Cells.Exists(columns.First().Key))
                    return;

                var cell = row.Cells[columns.First().Key];
                cell.ToolTipText = _getToolTip((TModel)row.ListObject);
            };
            return columns;
        }
    }
}

