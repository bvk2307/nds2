﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    public interface IServerGridDataProvider<TResponse> : IGridDataProvider<TResponse>
    {
        void BeginDataLoad(QueryConditions queryConditions);        
    }
}
