﻿using Infragistics.Win.UltraWinGrid;

namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    public class CheckboxGridColumnView : SingleGridColumnView
    {
        public CheckboxGridColumnView(UltraGridColumn control, IGridColumnFilterView filterView)
            : base(control, filterView)
        {
        }

        public override bool ReadOnly
        {
            get { return Control.IsReadOnly; }
            set
            {
                Control.CellActivation = value
                    ? Activation.NoEdit
                    : Activation.AllowEdit;
                Control.CellClickAction = value 
                    ? CellClickAction.RowSelect 
                    : CellClickAction.Edit;
                Control.Header.CheckBoxVisibility = value
                    ? HeaderCheckBoxVisibility.Never
                    : HeaderCheckBoxVisibility.Always;
            }
        }
    }
}