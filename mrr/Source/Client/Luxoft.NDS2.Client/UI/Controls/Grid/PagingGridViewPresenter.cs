﻿using Luxoft.NDS2.Client.UI.Controls.Grid;
using Luxoft.NDS2.Client.UI.Explain.Tks.Invoice.Models;
using Luxoft.NDS2.Common.Contracts.DTO;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    public class PagingGridViewPresenter<TModel, TDto, TResult> : GridViewPresenter<TModel, TDto, TResult>
        where TResult : PageResult<TDto>
    {
        private readonly IGridPagerView _pagerView;

        public PagingGridViewPresenter(
            IServerGridDataProvider<TResult> dataProvider,
            IChangeableListViewModel<TModel, TDto> model,
            IExtendedGridView gridView,
            IGridPagerView pagerView)
            : base(dataProvider, model, gridView)
        {
            _pagerView = pagerView;
            SetPaginationDetails();

            _pagerView.PageIndexChanged += PageIndexChanged;
        }

        private void PageIndexChanged(object sender, EventArgs e)
        {
            SetPaginationDetails();
            BeginLoad();
        }

        private void SetPaginationDetails()
        {
            QueryConditions.PaginationDetails.RowsToSkip =
                (uint)((_pagerView.PageIndex - 1) * _pagerView.RowsPerPage);
            QueryConditions.PaginationDetails.RowsToTake =
                (uint)_pagerView.RowsPerPage;
        }

        protected override void BeforeFilterChanged()
        {
            _pagerView.ResetPageIndex();
            SetPaginationDetails();
            QueryConditions.PaginationDetails.SkipTotalMatches = false;
        }

        protected override void AfterDataLoaded(TResult data)
        {
            if (!QueryConditions.PaginationDetails.SkipTotalAvailable)
            {
                QueryConditions.PaginationDetails.SkipTotalAvailable = true;
                _pagerView.RowsAvailable = data.TotalAvailable;
            }

            if (!QueryConditions.PaginationDetails.SkipTotalMatches)
            {
                QueryConditions.PaginationDetails.SkipTotalMatches = true;
                _pagerView.RowMatches = data.TotalMatches;
            }
        }
    }
}
