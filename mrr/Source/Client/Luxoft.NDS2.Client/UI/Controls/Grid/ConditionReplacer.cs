﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Contracts.Helpers;

namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    public class ColumnPair<TModel, TData>
        where TModel : class
        where TData: class
    {
        private readonly ConditionReplacer<TModel, TData> _replacer;

        public ColumnPair(
            ConditionReplacer<TModel, TData> replacer,
            string initialColumn)
        {
            _replacer = replacer;
            InitialColumn = initialColumn;
        }

        public string InitialColumn
        {
            get;
            private set;
        }

        public string ColumnToReplace
        {
            get;
            private set;
        }

        public ConditionReplacer<TModel, TData> ReplaceWith<T>(Expression<Func<TData, T>> expression)
        {
            ColumnToReplace = TypeHelper<TData>.GetMemberName(expression);
            return _replacer;
        }
    }

    public class ConditionReplacer<TModel, TData>
        where TModel : class
        where TData: class
    {
        private readonly List<ColumnPair<TModel, TData>> _sortingPairs = new List<ColumnPair<TModel, TData>>();

        private readonly List<ColumnPair<TModel, TData>> _filteringPairs = new List<ColumnPair<TModel, TData>>();

        public ConditionReplacer<TModel, TData> OptimizeSorting(List<ColumnSort> originalColumnsList)
        {
            foreach (var columnSort in originalColumnsList)
            {
                var pair = _sortingPairs.FirstOrDefault(p => p.InitialColumn == columnSort.ColumnKey);

                if (pair != null)
                {
                    columnSort.ColumnKey = pair.ColumnToReplace;
                }
            }

            return this;
        }

        public ConditionReplacer<TModel, TData> OptimizeFiltering(List<FilterQuery> filterList)
        {
            foreach (var filter in filterList)
            {
                var pair = _filteringPairs.FirstOrDefault(p => p.InitialColumn == filter.ColumnName);

                if (pair != null)
                {
                    filter.ColumnName = pair.ColumnToReplace;
                }
            }

            return this;
        }
        
        public ColumnPair<TModel, TData> WhenSortingBy<T>(Expression<Func<TModel, T>> expression)
        {
            var pair =
                new ColumnPair<TModel, TData>(
                    this,
                    TypeHelper<TModel>.GetMemberName(expression));
            _sortingPairs.Add(pair);

            return pair;
        }

        public ColumnPair<TModel, TData> WhenFilteringOn<T>(Expression<Func<TModel, T>> expression)
        {
            var pair =
                new ColumnPair<TModel, TData>(
                    this,
                    TypeHelper<TModel>.GetMemberName(expression));
            _filteringPairs.Add(pair);

            return pair;
        }
    }
}
