﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System;

namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    public class ColumnNameReplacer : IColumnFilterConverter
    {
        private readonly string _fieldName;

        public ColumnNameReplacer(string fieldName)
        {
            _fieldName = fieldName;
        }

        public virtual void Convert(FilterQuery columnFilter)
        {
            columnFilter.ColumnName = _fieldName;
        }
    }
}
