﻿using Infragistics.Win.SupportDialogs;
using GridResources = Infragistics.Win.UltraWinGrid.Resources;

namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    public static class FilterUIProviderLocalizer
    {
        public static void Localize()
        {
            var resource = Resources.Customizer;
            resource.SetCustomizedString("FilterUIProvider_Menu_ClearFilter", ResourceManagerNDS2.FilterUiProvider.ClearFilter);
            resource.SetCustomizedString("FilterUIProvider_Menu_TextFilters", ResourceManagerNDS2.FilterUiProvider.TextFilters);
            resource.SetCustomizedString("FilterUIProvider_Menu_DateFilters", ResourceManagerNDS2.FilterUiProvider.DateFilters);
            resource.SetCustomizedString("FilterUIProvider_Menu_NumberFilters", ResourceManagerNDS2.FilterUiProvider.NumberFilters);
            resource.SetCustomizedString("FilterUIProvider_OKButton", ResourceManagerNDS2.FilterUiProvider.OkButton);
            resource.SetCustomizedString("FilterUIProvider_CancelButton", ResourceManagerNDS2.FilterUiProvider.CancelButton);
            resource.SetCustomizedString("UltraGridFilterUIProvider_EqualsOperand", ResourceManagerNDS2.FilterUiProvider.EqualsOperand);
            resource.SetCustomizedString("UltraGridFilterUIProvider_DoesNotEqualOperand", ResourceManagerNDS2.FilterUiProvider.DoesNotEqualOperand);
            resource.SetCustomizedString("UltraGridFilterUIProvider_BeginsWithOperand", ResourceManagerNDS2.FilterUiProvider.BeginsWithOperand);
            resource.SetCustomizedString("UltraGridFilterUIProvider_EndsWithOperand", ResourceManagerNDS2.FilterUiProvider.EndsWithOperand);
            resource.SetCustomizedString("UltraGridFilterUIProvider_ContainsOperand", ResourceManagerNDS2.FilterUiProvider.ContainsOperand);
            resource.SetCustomizedString("UltraGridFilterUIProvider_GreaterThanOperand", ResourceManagerNDS2.FilterUiProvider.GreaterThanOperand);
            resource.SetCustomizedString("UltraGridFilterUIProvider_GreaterThanOrEqualToOperand", ResourceManagerNDS2.FilterUiProvider.GreaterThanOrEqualToOperand);
            resource.SetCustomizedString("UltraGridFilterUIProvider_LessThanOperand", ResourceManagerNDS2.FilterUiProvider.LessThanOperand);
            resource.SetCustomizedString("UltraGridFilterUIProvider_LessThanOrEqualToOperand", ResourceManagerNDS2.FilterUiProvider.LessThanOrEqualToOperand);
            resource.SetCustomizedString("UltraGridFilterUIProvider_BeforeOperand", ResourceManagerNDS2.FilterUiProvider.BeforeOperand);
            resource.SetCustomizedString("UltraGridFilterUIProvider_AfterOperand", ResourceManagerNDS2.FilterUiProvider.AfterOperand);
            resource.SetCustomizedString("UltraGridFilterUIProvider_BetweenOperand", ResourceManagerNDS2.FilterUiProvider.BetweenOperand);
            resource.SetCustomizedString("UltraGridFilterUIProvider_AllDatesInPeriod_Menu", ResourceManagerNDS2.FilterUiProvider.AllDatesInPeriod);
            resource.SetCustomizedString("UltraGridFilterUIProvider_CustomFilter", ResourceManagerNDS2.FilterUiProvider.CustomFilter);

            var rc = GridResources.Customizer;
            rc.SetCustomizedString("SpecialFilterOperand_January", ResourceManagerNDS2.GridView.January);
            rc.SetCustomizedString("SpecialFilterOperand_February", ResourceManagerNDS2.GridView.February);
            rc.SetCustomizedString("SpecialFilterOperand_March", ResourceManagerNDS2.GridView.March);
            rc.SetCustomizedString("SpecialFilterOperand_April", ResourceManagerNDS2.GridView.April);
            rc.SetCustomizedString("SpecialFilterOperand_May", ResourceManagerNDS2.GridView.May);
            rc.SetCustomizedString("SpecialFilterOperand_June", ResourceManagerNDS2.GridView.June);
            rc.SetCustomizedString("SpecialFilterOperand_July", ResourceManagerNDS2.GridView.July);
            rc.SetCustomizedString("SpecialFilterOperand_August", ResourceManagerNDS2.GridView.August);
            rc.SetCustomizedString("SpecialFilterOperand_September", ResourceManagerNDS2.GridView.September);
            rc.SetCustomizedString("SpecialFilterOperand_October", ResourceManagerNDS2.GridView.October);
            rc.SetCustomizedString("SpecialFilterOperand_November", ResourceManagerNDS2.GridView.November);
            rc.SetCustomizedString("SpecialFilterOperand_December", ResourceManagerNDS2.GridView.December);
            rc.SetCustomizedString("SpecialFilterOperand_LastMonth", ResourceManagerNDS2.GridView.LastMonth);
            rc.SetCustomizedString("SpecialFilterOperand_LastQuarter", ResourceManagerNDS2.GridView.LastQuarter);
            rc.SetCustomizedString("SpecialFilterOperand_LastWeek", ResourceManagerNDS2.GridView.LastWeek);
            rc.SetCustomizedString("SpecialFilterOperand_LastYear", ResourceManagerNDS2.GridView.LastYear);
            rc.SetCustomizedString("SpecialFilterOperand_NextMonth", ResourceManagerNDS2.GridView.NextMonth);
            rc.SetCustomizedString("SpecialFilterOperand_NextQuarter", ResourceManagerNDS2.GridView.NextQuarter);
            rc.SetCustomizedString("SpecialFilterOperand_NextWeek", ResourceManagerNDS2.GridView.NextWeek);
            rc.SetCustomizedString("SpecialFilterOperand_NextYear", ResourceManagerNDS2.GridView.NextYear);
            rc.SetCustomizedString("SpecialFilterOperand_ThisMonth", ResourceManagerNDS2.GridView.ThisMonth);
            rc.SetCustomizedString("SpecialFilterOperand_ThisQuarter", ResourceManagerNDS2.GridView.ThisQuarter);
            rc.SetCustomizedString("SpecialFilterOperand_ThisWeek", ResourceManagerNDS2.GridView.ThisWeek);
            rc.SetCustomizedString("SpecialFilterOperand_ThisYear", ResourceManagerNDS2.GridView.ThisYear);
            rc.SetCustomizedString("SpecialFilterOperand_Quarter1", ResourceManagerNDS2.GridView.Quarter1);
            rc.SetCustomizedString("SpecialFilterOperand_Quarter2", ResourceManagerNDS2.GridView.Quarter2);
            rc.SetCustomizedString("SpecialFilterOperand_Quarter3", ResourceManagerNDS2.GridView.Quarter3);
            rc.SetCustomizedString("SpecialFilterOperand_Quarter4", ResourceManagerNDS2.GridView.Quarter4);
            rc.SetCustomizedString("SpecialFilterOperand_Today", ResourceManagerNDS2.GridView.Today);
            rc.SetCustomizedString("SpecialFilterOperand_Tomorrow", ResourceManagerNDS2.GridView.Tomorrow);
            rc.SetCustomizedString("SpecialFilterOperand_Yesterday", ResourceManagerNDS2.GridView.Yesterday);
            rc.SetCustomizedString("SpecialFilterOperand_YearToDate", ResourceManagerNDS2.GridView.YearToDate);
            rc.SetCustomizedString("FilterDialogOkButtonNoFiltersText", ResourceManagerNDS2.GridView.NoFilters);
            rc.SetCustomizedString("RowFilterDialogDBNullItem", ResourceManagerNDS2.GridView.EnterValue);
        }
    }
}
