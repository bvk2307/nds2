﻿using System;

namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    public interface IGridDataProvider<TResponse>
    {
        event EventHandler<DataLoadedEventArgs<TResponse>> DataLoaded;
    }
}
