﻿using Luxoft.NDS2.Common.Models;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    public class ClientGridViewPresenter<TModel, TDto, TResponse> : IGridViewPresenter
        where TResponse : IEnumerable<TDto>
    {
        private readonly IGridView _view;

        private readonly IListViewModel<TModel, TDto> _viewModel;

        private readonly IClientGridDataProvider<TResponse> _dataProvider;

        public ClientGridViewPresenter(
            IGridView view,
            IListViewModel<TModel, TDto> viewModel,
            IClientGridDataProvider<TResponse> dataProvider)
        {
            _view = view;
            _viewModel = viewModel;
            _dataProvider = dataProvider;

            _view.SetDataSource(_viewModel.ListItems);
            _dataProvider.DataLoaded += DataLoaded;
        }

        public bool IsLoaded
        {
            get;
            private set;
        }

        public void BeginLoad()
        {
            _dataProvider.StartLoading();
        }

        private void DataLoaded(object sender, DataLoadedEventArgs<TResponse> e)
        {
            IsLoaded = true;
            _viewModel.UpdateList(e.Data);
        }
    }
}
