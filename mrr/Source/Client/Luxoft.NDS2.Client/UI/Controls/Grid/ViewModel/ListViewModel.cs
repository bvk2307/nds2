﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.ViewModels
{
    public abstract class ListViewModel<TModel, TDto> : IChangeableListViewModel<TModel, TDto>
    {
        # region Конструктор

        protected ListViewModel()
        {
            ListItems = new BindingList<TModel>();
        }

        # endregion

        # region Список данных

        public event EventHandler ListUpdateCompleted;

        public BindingList<TModel> ListItems
        {
            get;
            private set;
        }

        public void UpdateList(IEnumerable<TDto> data)
        {
            ListItems.Clear();
            foreach (var dto in data)
                ListItems.Add(ConvertToModel(dto));

            if (ListUpdateCompleted != null)
                ListUpdateCompleted(this, new EventArgs());
        }

        protected abstract TModel ConvertToModel(TDto dataItem);

        public QueryConditions GetQueryConditions()
        {
            return new QueryConditions();
        }

        # endregion

        # region Сортировка

        private readonly Dictionary<string, IColumnSortBuilder> _sortBuilders =
            new Dictionary<string, IColumnSortBuilder>();

        public IColumnSortBuilder ColumnSortBuilder(string columnKey)
        {
            if (_sortBuilders.ContainsKey(columnKey))
            {
                return _sortBuilders[columnKey];
            }

            return new DefaultColumnSortBuilder(columnKey);
        }

        protected void AddSortBuilder(string columnKey, IColumnSortBuilder sortBuilder)
        {
            _sortBuilders.Add(columnKey, sortBuilder);
        }

        # endregion

        # region Фильтрация

        private Dictionary<string, IColumnFilterConverter> _filterConverters =
            new Dictionary<string, IColumnFilterConverter>();

        protected void AddFilterConverter(string columnKey, IColumnFilterConverter converter)
        {
            _filterConverters.Add(columnKey, converter);
        }

        public void ConvertFilter(FilterQuery filter)
        {
            if (_filterConverters.ContainsKey(filter.ColumnName))
            {
                _filterConverters[filter.ColumnName].Convert(filter);
            }
        }

        # endregion
    }
}
