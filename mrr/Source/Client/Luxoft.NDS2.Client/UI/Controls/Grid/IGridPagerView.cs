﻿using System;

namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    public interface IGridPagerView
    {
        int RowsAvailable
        {
            set;
        }

        int RowMatches
        {
            set;
            get;
        }

        int RowsPerPage
        {
            get;
        }

        int PageIndex
        {
            get;
        }

        void ResetPageIndex();

        event EventHandler PageIndexChanged;
    }
}
