﻿using Infragistics.Win.UltraWinGrid;
using System;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    public static class ContextMenuHelper
    {
        public static ContextMenuStrip WithCopyCell(this ContextMenuStrip menu, Func<string> getText)
        {
            var cmItem = new ToolStripMenuItem
            {
                Name = "cmCopyCell",
                Text = "Копировать ячейку",
            };
            cmItem.Click += (sender, e) => OnCopyClick(getText);
            menu.Items.Add(cmItem);
            
            return menu;
        }

        public static ContextMenuStrip WithCopyTable(this ContextMenuStrip menu, UltraGrid grid)
        {
            var cmItem = new ToolStripMenuItem
            {
                Name = "cmCopyTable",
                Text = "Копировать таблицу",
            };
            cmItem.Click += (sender, e) =>
            {
                var currentSelType = grid.DisplayLayout.Override.SelectTypeRow;
                grid.DisplayLayout.Override.SelectTypeRow = SelectType.Extended;
                grid.Selected.Rows.AddRange(grid.Rows.GetAllNonGroupByRows());
                grid.PerformAction(UltraGridAction.Copy, false, false);
                grid.Selected.Rows.Clear();
                grid.DisplayLayout.Override.SelectTypeRow = currentSelType;
            };
            menu.Items.Add(cmItem);

            return menu;
        }

        private static void OnCopyClick(Func<string> getText)
        {
            var text = getText();
            if (string.IsNullOrEmpty(text))
                Clipboard.Clear();
            else
                Clipboard.SetText(text);
        }
    }
}
