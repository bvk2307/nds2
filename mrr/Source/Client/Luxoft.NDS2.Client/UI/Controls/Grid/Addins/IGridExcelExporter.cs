﻿using System;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.Addins
{
    public interface IGridExcelExporter
    {
        /// <summary>
        /// Экспортирует грид
        /// </summary>
        void Export();
    }
}
