﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System;
using Infragistics.Win.UltraWinGrid;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.Addins
{
    public class GridExcelExporter : IGridExcelExporter
    {
        private Action _excelExporter;

        public GridExcelExporter(Action excelExporter )
        {
            _excelExporter = excelExporter;
        }

        public void Export()
        {
            _excelExporter.Invoke();
        }

    }
}
