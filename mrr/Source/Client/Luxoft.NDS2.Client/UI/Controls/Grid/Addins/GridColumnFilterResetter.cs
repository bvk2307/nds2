﻿using System;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.Addins
{
    public class GridColumnFilterResetter : IGridResetter
    {
        private readonly IExtendedGridView _gridView;

        public GridColumnFilterResetter(IExtendedGridView gridView)
        {
            _gridView = gridView;
            _gridView.GridEvents.Filtered += (sender, e) => SetDefaultState();
            SetDefaultState();
        }

        public void Reset()
        {
            if (DefaultState)
            {
                return;
            }

            _gridView.GridEvents.Disable();
            _gridView.Columns.ForAll(c => c.Filter.ResetFilter());
            DefaultState = true;
            RaiseStateChanged();

            _gridView.GridEvents.Enable();
            _gridView.GridEvents.Filter();

        }

        private void SetDefaultState()
        {
            var currentValue = DefaultState;
            DefaultState = true;
            _gridView
                .Columns
                .ForAll(
                    c =>
                    {
                        try
                        {
                            DefaultState = c.Filter.Current == c.Filter.Default
                                           && DefaultState;
                        }
                        catch (Exception)
                        {
                            DefaultState = false;
                        }
                    });

            if (DefaultState != currentValue)
            {
                RaiseStateChanged();
            }
        }

        public bool DefaultState
        {
            get;
            private set;
        }

        public event EventHandler StateChanged;

        private void RaiseStateChanged()
        {
            if (StateChanged != null)
            {
                StateChanged(this, new EventArgs());
            }
        }
    }
}
