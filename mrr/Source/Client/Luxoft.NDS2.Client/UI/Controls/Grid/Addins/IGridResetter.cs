﻿using System;
using Luxoft.NDS2.Client.LocalStorage;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.Addins
{
    public interface IGridResetter
    {
        /// <summary>
        /// Сбрасывает состояние таблицы к значениям по умолчанию
        /// </summary>
        void Reset();

        /// <summary>
        /// Возвращает признак того, что состояние таблицы соответствует значениям по умолчанию
        /// </summary>
        bool DefaultState
        {
            get;
        }

        event EventHandler StateChanged;
    }
}
