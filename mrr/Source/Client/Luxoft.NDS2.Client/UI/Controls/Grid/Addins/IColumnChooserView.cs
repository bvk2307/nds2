﻿using Luxoft.NDS2.Client.UI.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.Addins
{
    public interface IColumnChooserView
    {
        void InitList(IEnumerable<IGridColumnView> columns);
    }
}
