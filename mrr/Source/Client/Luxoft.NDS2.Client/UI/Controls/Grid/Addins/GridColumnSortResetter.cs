﻿using System;
using Luxoft.NDS2.Client.LocalStorage;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.Addins
{
    public class GridColumnSortResetter : IGridResetter
    {
        private readonly IExtendedGridView _gridView;

        public GridColumnSortResetter(IExtendedGridView gridView)
        {
            _gridView = gridView;
            SetDefaultState();
            _gridView.GridEvents.Sorted += (sender, e) => SetDefaultState();
        }

        private void SetDefaultState()
        {
            _gridView.Columns.ForAll(
                column => DefaultState = DefaultState 
                    && column.DefaultSortOrder == column.SortOrder);
        }

        public void Reset()
        {
            _gridView.GridEvents.Disable();

            foreach (var column in _gridView.Columns)
            {
                column.ResetSortOrder();
            }

            DefaultState = true;
            RaiseStateChanged();

            _gridView.GridEvents.Enable();
        }

        private bool _defaulState = true;

        public bool DefaultState
        {
            get
            {
                return _defaulState;
            }
            private set
            {
                if (_defaulState != value)
                {
                    _defaulState = value;
                    RaiseStateChanged();
                }
            }
        }

        public event EventHandler StateChanged;

        public event EventHandler<ErrorEventArgs> ErrorRaised;

        private void RaiseStateChanged()
        {
            if (StateChanged != null)
            {
                StateChanged(this, new EventArgs());
            }
        }
    }
}
