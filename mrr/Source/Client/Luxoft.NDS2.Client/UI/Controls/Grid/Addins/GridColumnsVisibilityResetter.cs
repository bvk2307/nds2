﻿using System;
using Luxoft.NDS2.Client.LocalStorage;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.Addins
{
    public class GridColumnsVisibilityResetter : IGridResetter
    {
        private readonly IExtendedGridView _grid;

        public GridColumnsVisibilityResetter(IExtendedGridView grid)
        {
            _grid = grid;
            DefaultState = true;

            _grid.Columns.ForAll(
                column =>
                    {
                        column.VisibleChanged += ColumnVisibleChanged;
                        DefaultState = DefaultState && column.VisibleByDefault == column.Visible;
                    });
        }

        private void ColumnVisibleChanged(object sender, EventArgs e)
        {
            var prevState = DefaultState;
            _grid.Columns.ForAll(column => DefaultState = DefaultState && column.VisibleByDefault == column.Visible);

            if (prevState != DefaultState)
            {
                RaiseStateChanged();
            }
        }

        public void Reset()
        {
            if (DefaultState)
            {
                return;
            }

            _grid.Columns.ForAll(column => column.VisibleChanged -= ColumnVisibleChanged);

            foreach (var column in _grid.Columns)
            {
                column.ResetVisibility();
            }

            DefaultState = true;
            RaiseStateChanged();

            _grid.Columns.ForAll(column => column.VisibleChanged += ColumnVisibleChanged);
        }

        public bool DefaultState
        {
            get;
            private set;
        }

        private void RaiseStateChanged()
        {
            if (StateChanged != null)
            {
                StateChanged(this, new EventArgs());
            }
        }

        public event EventHandler StateChanged;

        public event EventHandler<ErrorEventArgs> ErrorRaised;
    }
}
