﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    public interface IColumnFilterConverter
    {
        void Convert(FilterQuery columnFilter);
    }
}
