﻿using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    /// <summary>
    /// Этот класс форматирует UltraGridColumn для отображения значений с заданным провайдером формата
    /// </summary>
    public class ValueColumnFormatter<TCellData> : IGridColumnFormatter
    {
        private readonly IFormatProvider _formatProvider;
        private readonly string _format;

        public ValueColumnFormatter(IFormatProvider formatProvider, string format)
        {
            _formatProvider = formatProvider;
            _format = format;
        }

        public UltraGridColumn ApplyFormat(UltraGridColumn column)
        {
            column.FormatInfo = _formatProvider;
            column.Format = _format;
            return column;
        }

        public List<UltraGridColumn> ApplyFormat(List<UltraGridColumn> columns)
        {
            columns.ForEach(x => { x.FormatInfo = _formatProvider;
                x.Format = _format;
            });

            return columns;
        }
    }
}
