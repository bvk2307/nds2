﻿using System;

namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    public class ListItemEventArgs : EventArgs
    {
        public ListItemEventArgs(object listItem)
        {
            ListItem = listItem;
        }

        public object ListItem
        {
            get;
            private set;
        }
    }
}
