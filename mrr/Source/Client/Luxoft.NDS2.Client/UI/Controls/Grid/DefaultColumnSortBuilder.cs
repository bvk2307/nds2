﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    public class DefaultColumnSortBuilder : IColumnSortBuilder
    {
        private readonly string _columnKey;

        public DefaultColumnSortBuilder(string columnKey)
        {
            _columnKey = columnKey;
        }

        public virtual IEnumerable<ColumnSort> Build(ColumnSort.SortOrder sortOrder)
        {
            yield return new ColumnSort { ColumnKey = _columnKey, Order = sortOrder };
        }
    }
}
