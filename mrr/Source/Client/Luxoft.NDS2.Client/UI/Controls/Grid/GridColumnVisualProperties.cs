﻿using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    [Serializable]
    public class GridColumnVisualProperties : ISerializable
    {
        public int Width { get; set; }

        public int VPosition { get; set; }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        protected virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Width", Width);
            info.AddValue("VPosition", VPosition);
        }

        [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
                throw new ArgumentNullException("info");

            GetObjectData(info, context);
        }
    }
}
