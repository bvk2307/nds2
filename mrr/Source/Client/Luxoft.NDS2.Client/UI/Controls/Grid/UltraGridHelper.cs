﻿using Infragistics.Win.UltraWinGrid;
using System.Windows.Forms;
using ColumnStyle = Infragistics.Win.UltraWinGrid.ColumnStyle;
using Infragistics.Win;
using System;
using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;

namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    public static class UltraGridHelper
    {
        /// <summary>
        /// Устанавливает стиль гиперлинк ячейке таблицы
        /// </summary>
        /// <param name="row"></param>
        /// <param name="cellIndex"></param>
        public static void MakeHyperlink(this UltraGridRow row, string cellIndex)
        {
            row.Cells[cellIndex].Style = ColumnStyle.URL;
            row.Cells[cellIndex].Appearance.Cursor = Cursors.Hand;
        }
    }
}
