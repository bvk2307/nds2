﻿using Infragistics.Shared;
using Infragistics.Win.UltraWinGrid;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    public interface IGridColumnViewCreator
    {
        IGridColumnView Create(
            UltraGridGroup group, 
            IEnumerable<UltraGridGroup> lesserGroups,
            IEnumerable<UltraGridColumn> lesserColumns);

        IGridColumnView Create(UltraGridColumn column);
    }
}
