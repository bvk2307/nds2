﻿namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    public interface IGridView
    {
        void SetDataSource(object dataSource);
    }
}
