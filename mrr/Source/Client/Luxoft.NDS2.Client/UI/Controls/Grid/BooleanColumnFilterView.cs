﻿using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    public class BooleanColumnFilterView : GridColumnFilterView
    {
        private static string TextTrue = "Да";

        private static string TextFalse = "Нет";

        private static object ValueTrue = true;

        private static object ValueFalse = false;

        public BooleanColumnFilterView(UltraGridColumn column)
            : base(column)
        {
        }

        protected override void BuildQuickFilterList(BeforeRowFilterDropDownEventArgs e)
        {
            var itemsToDelete = e.ValueList.ValueListItems.All.Skip(1).ToArray();

            foreach (var item in itemsToDelete)
            {
                e.ValueList.ValueListItems.Remove(item);
            }

            e.ValueList.ValueListItems.Add(new ValueListItem(ValueTrue, TextTrue));
            e.ValueList.ValueListItems.Add(new ValueListItem(ValueFalse, TextFalse));
        }
    }
}
