﻿using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Common.Contracts.DTO.Explain;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Selections.Enums;
using Telerik.Windows.Controls.Primitives;

namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    public class StatusColumnFilterView : GridColumnFilterView
    {
        public StatusColumnFilterView(UltraGridColumn column)
            : base(column)
        {
        }

        protected override void BuildQuickFilterList(BeforeRowFilterDropDownEventArgs e)
        {
            var itemsToDelete = e.ValueList.ValueListItems.All.Skip(1).ToArray();

            foreach (var item in itemsToDelete)
            {
                e.ValueList.ValueListItems.Remove(item);
            }

            FillStatusItems(e);
        }

        protected virtual void FillStatusItems(BeforeRowFilterDropDownEventArgs e)
        {
            e.ValueList.ValueListItems.Add(new ValueListItem(ExplainTksInvoiceState.Changed, "Измененные"));
            e.ValueList.ValueListItems.Add(new ValueListItem(ExplainTksInvoiceState.Confirmed, "Подтвержденные"));
            e.ValueList.ValueListItems.Add(new ValueListItem(ExplainTksInvoiceState.Ignored, "Не примененные"));
        }
    }

    public class ConfirmedInoicesStatusColumnFilterView : StatusColumnFilterView
    {
        public ConfirmedInoicesStatusColumnFilterView(UltraGridColumn column)
            : base(column)
        {
        }

        protected override void FillStatusItems(BeforeRowFilterDropDownEventArgs e)
        {
            e.ValueList.ValueListItems.Add(new ValueListItem(ExplainTksInvoiceState.Changed, "Измененные"));
            e.ValueList.ValueListItems.Add(new ValueListItem(ExplainTksInvoiceState.Ignored, "Не примененные"));
        }
    }
}
