﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Common.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    public interface IChangeableListViewModel<TModel, TDto> : IListViewModel<TModel, TDto>
    {
        IColumnSortBuilder ColumnSortBuilder(string columnKey);

        void ConvertFilter(FilterQuery filter);
    } 
}
