﻿using Infragistics.Win.UltraWinGrid;
using System;

namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    public class UltraGridEventsHandler : IGridEventsHandler
    {
        private bool _eventsDisabled;

        private bool _filteredOnDelay;

        private bool _sortedOnDelay;

        public UltraGridEventsHandler(UltraGrid gridControl)
        {
            if (gridControl == null)
            {
                throw new ArgumentNullException("gridControl");
            }

            gridControl.AfterSortChange += AfterSorted;
            gridControl.AfterRowFilterChanged += AfterFiltered;
            gridControl.Leave += OnLeave;
        }

        public event EventHandler Enabled;

        public void Enable()
        {
            if (!_eventsDisabled)
            {
                return;
            }

            _eventsDisabled = false;

            if (Enabled != null)
            {
                Enabled(this, new EventArgs());
            }

            if (_filteredOnDelay)
            {
                RaiseFiltered();
            }
            else if (_sortedOnDelay)
            {
                RaiseSorted();
            }

            _filteredOnDelay = false;
            _sortedOnDelay = false;
        }

        public event EventHandler Disabled;

        public void Disable()
        {
            if (_eventsDisabled)
            {
                return;
            }

            _eventsDisabled = true;

            if (Disabled != null)
            {
                Disabled(this, new EventArgs());
            }
        }

        public event EventHandler Sorted;

        private void AfterSorted(object sender, BandEventArgs e)
        {
            if (_eventsDisabled)
            {
                _sortedOnDelay = true;
                return;
            }

            RaiseSorted();
        }

        private void RaiseSorted()
        {
            if (Sorted != null)
            {
                Sorted(this, new EventArgs());
            }            
        }

        public event EventHandler Filtered;

        private void AfterFiltered(object sender, AfterRowFilterChangedEventArgs e)
        {
            if (_eventsDisabled)
            {
                _filteredOnDelay = true;
                return;
            }

            RaiseFiltered();
        }

        private void RaiseFiltered()
        {
            if (Filtered != null)
            {
                Filtered(this, new EventArgs());
            }
        }

        public void Filter()
        {
            RaiseFiltered();
        }

        public event EventHandler Unloaded;
        public void OnLeave(object sender, EventArgs e)
        {
            if (Unloaded != null)
                Unloaded(sender, e);
        }
    }
}
