﻿using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using query = Luxoft.NDS2.Common.Contracts.DTO.Query;
using System.Collections.Generic;
using System;

namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    public class NullGridColumnFilterView : IGridColumnFilterView
    {
        public NullGridColumnFilterView(UltraGridColumn column)
        {
            column.AllowRowFiltering = DefaultableBoolean.False;
        }

        public bool AllowFilter
        {
            get 
            {
                return false; 
            }
            set 
            { 
                return; 
            }
        }

        public query.FilterQuery Default
        {
            get
            {
                return null;
            }
        }

        public query.FilterQuery Current
        {
            get
            {
                return null;
            }
            set
            {

            }
        }

        public Infragistics.Win.UltraWinGrid.ColumnFilter DefaultColumnFilter
        {
            get
            {
               return null;
            }
        }

        public void ResetFilter()
        {
            return;
        }
    }
}
