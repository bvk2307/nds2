﻿using System;
using System.Linq;
using CommonComponents.Utils;
using Infragistics.Win;
using Infragistics.Win.SupportDialogs.FilterUIProvider;
using Infragistics.Win.UltraWinGrid;
using Resources = Infragistics.Win.UltraWinGrid.Resources;

namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    public static class GridViewConfig
    {
        private static string[] OperationCodesColumnNames = { "OperationCodes", "BuyerOperation" };

        public static UltraGrid SetBaseConfig(this UltraGrid grid)
        {
            FilterUIProviderLocalizer.Localize();

            var rc = Resources.Customizer;
            rc.SetCustomizedString("FilterDialogOkButtonNoFiltersText", ResourceManagerNDS2.GridView.NoFilters);
            rc.SetCustomizedString("RowFilterDialogDBNullItem", ResourceManagerNDS2.GridView.EnterValue);

            grid.UseAppStyling = false;
            grid.DisplayLayout.Override.HeaderStyle = HeaderStyle.WindowsXPCommand;

            grid.InitializeLayout +=
                (sender, args) =>
                {
                    args.Layout.Override.HeaderAppearance.TextTrimming =
                        TextTrimming.EllipsisCharacter;
                    args.Layout.Override.WrapHeaderText = DefaultableBoolean.False;
                };
            grid.BeforeCustomRowFilterDialog += (sender, args) =>
            {
                try
                {
                    var g =
                        args.CustomRowFiltersDialog.Controls[5].Controls[0] as UltraGrid;
                    g.DisplayLayout.Bands[0].Columns[0].Header.Caption = ResourceManagerNDS2.GridView.Condition;
                    g.DisplayLayout.Bands[0].Columns[1].Header.Caption = ResourceManagerNDS2.GridView.Value;
                    var editor = (EditorWithCombo)g.DisplayLayout.Bands[0].Columns[1].Layout.Rows[0].Cells[1].EditorResolved;
                    if (editor != null)
                    {
                        editor.BeforeDropDown += (sender1, args1) =>
                        {
                            ValueListItem[] items = new ValueListItem[((ValueList)editor.ValueList).ValueListItems.Count];
                            ((ValueList)editor.ValueList).ValueListItems.CopyTo(items, 0);
                            foreach (ValueListItem item in items)
                            {
                                if (item.DataValue is Infragistics.Win.UltraWinGrid.BlanksClass)
                                    continue;

                                ((ValueList)editor.ValueList).ValueListItems.Remove(item);
                            }
                        };
                    }
                }
                catch { }
            };
            grid.BeforeRowFilterDropDownPopulate += (sender, args) =>
            {
                args.Handled = true;
            };
            if(grid.DisplayLayout.Override.FilterUIProvider != null)
               ((UltraGridFilterUIProvider)grid.DisplayLayout.Override.FilterUIProvider).AfterMenuPopulate += (sender, args) =>
               {
                   if (args.ColumnFilter.Column.DataType == typeof(DateTime) ||
                       args.ColumnFilter.Column.DataType == typeof(DateTime?))
                   {
                       var menuItem = (FilterMenuTool)args.MenuItems[1];
                       if (menuItem != null)
                       {
                           var itemsToRemove = menuItem.Tools.Where(x => x.GetType() == typeof(FilterOperandTool)
                                                                         || x.GetType() == typeof(FilterMenuTool)).ToList();
                           foreach (var item in itemsToRemove)
                           {
                               menuItem.Tools.Remove(item);
                           }
                       }
                   }
                   if (OperationCodesColumnNames.Contains(args.ColumnFilter.Column.Key))
                   {
                       var menuItem = (FilterMenuTool)args.MenuItems[1];
                       if (menuItem != null)
                       {
                           var itemsToRemove = menuItem.Tools.Where(x => x.DisplayText == ResourceManagerNDS2.FilterUiProvider.BeginsWithOperand
                                                                         || x.DisplayText == ResourceManagerNDS2.FilterUiProvider.EndsWithOperand).ToList();
                           foreach (var item in itemsToRemove)
                           {
                               menuItem.Tools.Remove(item);
                           }
                       }
                   }
                   args.Handled = true;
               };
            return grid;
        }
    }
}
