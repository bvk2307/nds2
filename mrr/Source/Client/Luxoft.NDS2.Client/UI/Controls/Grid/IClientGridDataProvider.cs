﻿using System.Threading;

namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    public interface IClientGridDataProvider<TResponse> : IGridDataProvider<TResponse>
    {
        void StartLoading();
    }
}
