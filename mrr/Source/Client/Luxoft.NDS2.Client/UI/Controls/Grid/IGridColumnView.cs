﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System;
using Infragistics.Win.UltraWinGrid;

namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    public interface IGridColumnView
    {
        # region Main Attributes

        string Key
        {
            get;
        }

        string Caption
        {
            get;
            set;
        }

        string Description
        {
            get;
            set;
        }

        GridColumnVisualProperties HeaderProps { get; set; }

        # endregion

        # region Visibility

        bool VisibleByDefault
        {
            get;
            set;
        }

        bool Visible
        {
            get;
            set;
        }

        bool ReadOnly
        {
            get;
            set;
        }

        event EventHandler VisibleChanged;

        void ResetVisibility();

        # endregion

        # region Sorting

        ColumnSort.SortOrder DefaultSortOrder
        {
            get;
            set;
        }

        ColumnSort.SortOrder SortOrder
        {
            get;
            set;
        }

        bool AllowSort
        {
            get;
        }

        void ResetSortOrder();


        IGridColumnView Sort(SortIndicator sortIndicator);
        # endregion

        # region Filtering

        IGridColumnFilterView Filter
        {
            get;
        }

        # endregion

        # region Sub Columns

        IGridColumnsCollection SubColumns
        {
            get;
        }

        # endregion

        # region Formatting

        IGridColumnView ApplyFormatter(IGridColumnFormatter formatter);

        # endregion
    }
}
