﻿using System.Diagnostics;
using CommonComponents.Instrumentation;
using CommonComponents.Uc.Infrastructure.Interface.Contexts;
using CommonComponents.Uc.Infrastructure.Interface.Services;
using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Common.Contracts;
using Microsoft.Practices.CompositeUI;
using System;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V1.Providers
{
    [Obsolete]
    public class CommonSettingsProvider : ISettingsProvider
    {
        protected readonly WorkItem _workItem;
        private readonly string _key;

        public CommonSettingsProvider(WorkItem workItem, string key)
        {
            _workItem = workItem;
            _key = key;
        }

        public virtual void SaveSettings(string valueSetup, string settingsKey = "settings")
        {
            _workItem.Services.Get<IUcOptionsService>(true).Save(_key, settingsKey, valueSetup);           
        }

        public virtual string LoadSettings(string settingsKey = "settings")
        {
            return _workItem.Services.Get<IUcOptionsService>(true).Load(_key, settingsKey);
        }

        private IUcMessageService Messenger()
        {
            return _workItem.Services.Get<IUcMessageService>(true);
        }

        private IInstrumentationService Logger()
        {
            return _workItem.Services.Get<IInstrumentationService>();
        }

        public void NotifySuccess()
        {
            Messenger().ShowInfo(new MessageInfoContext("Информация", "Настройки сохранены"));
        }

        public void LogSuccess()
        {
            var le = Logger().CreateLogEntry(new LogEntryTemplate(Constants.SubsystemId, 223, TraceEventType.Information, InstrumentationLevel.Normal));
            le.Title = "NDS2 " + this.GetType();
            le.MethodName = "Save";
            le.Message = "Настройки сохранены";
            Logger().Write(le);
        }

    }
}
