﻿using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V1
{
    public class SummaryRowCreationFilter : IUIElementCreationFilter
    {
        public ICustomSummaryCalculator FullSumCalculator { get; set; }
        public ICustomSummaryCalculator PerGroupSumCalculator { get; set; }
        public IGroupingGeneralInfo GroupingGridSetup { get; set; }

        public void AfterCreateChildElements(UIElement parent)
        {
            if (FullSumCalculator == null || PerGroupSumCalculator == null || GroupingGridSetup == null)
            {
                return;//return false;
            }

            if (parent is FixedSummaryLineUIElement)
            {
                var elem = (RowsCollection)parent.GetContext();

                var grouping = GroupingGridSetup.GetAllGroupingColumns();
                if (!grouping.Any())
                {
                    var tmpVal = elem.SummaryValues.SummaryFooterCaption;
                    elem.SummaryValues.SummaryFooterCaption = "Всего по книге";
                    if (tmpVal != elem.SummaryValues.SummaryFooterCaption)
                    {
                        elem.Refresh(RefreshRow.RefreshDisplay);
                    }
                    return;
                }

                if (!elem.IsGroupByRows)
                {
                    var tmpVal = elem.SummaryValues.SummaryFooterCaption;
                    elem.SummaryValues.SummaryFooterCaption = "Всего по группе";
                    if (tmpVal != elem.SummaryValues.SummaryFooterCaption)
                    {
                        elem.Refresh(RefreshRow.RefreshDisplay);
                    }
                }
                else
                {
                    var tmpVal = elem.SummaryValues.SummaryFooterCaption;
                    elem.SummaryValues.SummaryFooterCaption = "Всего по книге";
                    if (tmpVal != elem.SummaryValues.SummaryFooterCaption)
                    {
                        elem.Refresh(RefreshRow.RefreshDisplay);
                    }
                }
            }
        }

        public bool BeforeCreateChildElements(UIElement parent)
        {
            if (FullSumCalculator == null || PerGroupSumCalculator == null || GroupingGridSetup == null)
            {
                return false;
            }
            
            if (parent is FixedSummaryLineUIElement)
            {
                var grouping = GroupingGridSetup.GetAllGroupingColumns();
                if (!grouping.Any())
                {
                    return false;
                }

                var elem = (RowsCollection)parent.GetContext();
                if (!elem.IsGroupByRows)
                {
                    for (int i = 0; i < elem.SummaryValues.Count; i++)
                    {
                        var item = elem.SummaryValues[i];
                        if (item.SummarySettings.CustomSummaryCalculator != PerGroupSumCalculator)
                        {
                            item.Appearance.ForegroundAlpha = Alpha.Transparent;                            
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < elem.SummaryValues.Count; i++)
                    {
                        var item = elem.SummaryValues[i];
                        if (item.SummarySettings.CustomSummaryCalculator != FullSumCalculator)
                        {
                            item.Appearance.ForegroundAlpha = Alpha.Transparent;                            
                        }
                    }
                }
            }

            return false;
        }
    }
}
