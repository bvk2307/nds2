﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Luxoft.NDS2.Common.Contracts.DTO.Query
{
    /// <summary>Настнойка многотабличного грида</summary>
    [Serializable]
    public sealed class BandDefinition
    {
        /// <summary>Имя свойства</summary>
        public string Name { get; set; }

        /// <summary>Заголовок</summary>\
        public string Caption { get; set; }

        /// <summary>Признак зависимости ширины столбцов от родительской таблицы</summary>
        public bool BindWidth { get; set; }

        /// <summary>Отображать заголовки столбцов</summary>
        public bool ColumnsHeadersVisible { get; set; }

        /// <summary>Описание связанных таблиц</summary>
        public List<BandDefinition> Bands { get; set; }

        /// <summary>Группы</summary>
        public List<ColumnGroupDefinition> Groups { get; set; }

        /// <summary>Столбцы без групп</summary>
        public List<ColumnDefinition> Columns { get; set; }

        public BandDefinition()
        {
            Bands = new List<BandDefinition>();
            Groups = new List<ColumnGroupDefinition>();
            Columns = new List<ColumnDefinition>();
            ColumnsHeadersVisible = true;
        }

        /// <summary>
        /// Поиск в дереве бандов по имени
        /// </summary>
        /// <param name="name">Имя искомого банда</param>
        public BandDefinition GetBandByName(string name)
        {
            if (name.Equals("List`1") || name.Equals(Name))
                return this;

            return FindBand(name);
        }

        private BandDefinition FindBand(string name, BandDefinition root = null)
        {
            var band = root ?? this;
            if (band.Name.Equals(name))
            {
                return band;
            }
            return band.Bands.FirstOrDefault(b => FindBand(name, b) != null);
        }

        public IEnumerable<ColumnDefinition> GetAllColumns()
        {
            Func<ColumnGroupDefinition, IEnumerable<ColumnDefinition>> getColumns = null;
            getColumns = group => group.Columns.Union(group.SubGroups.SelectMany(subGroup => getColumns(subGroup)));
            return Columns.Union(Groups.SelectMany(group => getColumns(group)));
        }
    }
}