﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Infragistics.Shared;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using UltraWinGrid = Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Helpers;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.EventArguments;
using Infragistics.Win.UltraWinMaskedEdit;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V1
{
    public partial class GridControl
    {
        public bool Band_EditMode = false;
        public GridRegimEdit Band_GridRegimEdit = GridRegimEdit.View;

        private void SetupMultiBands()
        {
            if (!Band_EditMode)
            {
                foreach (var row in grid.DisplayLayout.Rows)
                {
                    EnsureRowExpandable(row);
                }
            }
        }

        private BandDefinition GetBandDefenition(UltraGridBand ugBand)
        {
            return Setup.Bands.Select(band => FindBand(ugBand.Key, band)).FirstOrDefault(band => band != null);
        }

        private void EnsureRowExpandable(UltraGridRow parent)
        {
            if (parent.ChildBands != null)
            {
                var rows = parent.ChildBands
                    .Cast<UltraGridChildBand>()
                    .Where(child => GetBandDefenition(child.Band) != null)
                    .SelectMany(child => child.Rows)
                    .ToList();
                if (rows.Count > 0)
                {
                    parent.ExpansionIndicator = ShowExpansionIndicator.Always;
                    rows.ForEach(row => EnsureRowExpandable(row));
                    return;
                }
            }
            parent.ExpansionIndicator = ShowExpansionIndicator.Never;
        }

        private void Grid_BeforeRowExpanded(object sender, CancelableRowEventArgs e)
        {
            if (e.Row.ChildBands == null)
            {
                return;
            }
            var childBands =
                from childBand in e.Row.ChildBands.Cast<UltraGridChildBand>()
                let band = childBand.Band
                where band != null && band.Tag == null
                select band;

            foreach (var ugBand in childBands)
            {
                SetupBand(ugBand);
            }
            GenerateChildBandBeforeRowExpanded(e.Row);
        }

        private void GenerateChildBandBeforeRowExpanded(UltraGridRow bandRow)
        {
            if (bandRow.ChildBands.Count > 0 && ChildBandBeforeRowExpanded != null)
            {
                foreach (UltraGridChildBand itemBand in bandRow.ChildBands)
                {
                    foreach (UltraGridRow row in itemBand.Rows)
                    {
                        bool ch = _rowSelected.Contains(row);
                        SelectedRowEventArgs args = new SelectedRowEventArgs(row, ch);
                        ChildBandBeforeRowExpanded(this, args);
                    }
                }
            }
        }

        private void BandColumnsSynhronizeHideOrShow(string columnKey)
        {
            foreach (var row in grid.DisplayLayout.Rows)
            {
                if (row.ChildBands != null)
                {
                    var childBands =
                        from childBand in row.ChildBands.Cast<UltraGridChildBand>()
                        let band = childBand.Band
                        where band != null && band.Tag != null
                        select band;

                    foreach (var ugBand in childBands)
                    {
                        UltraGridColumn col = ugBand.Columns[columnKey];
                        UltraGridColumn colBandParent = col.Band.ParentBand.Columns[col.Key];
                        col.Hidden = colBandParent.Hidden;
                    }
                }
            }
        }

        private void SetupBand(UltraGridBand ugBand)
        {
            var sBand = GetBandDefenition(ugBand);
            if (sBand == null)
            {
                ugBand.Hidden = true;
                return;
            }
            ugBand.RowLayoutStyle = RowLayoutStyle.GroupLayout;
            ugBand.ColHeadersVisible = sBand.ColumnsHeadersVisible;
            ugBand.GroupHeadersVisible = sBand.ColumnsHeadersVisible;
            ugBand.HeaderVisible = !string.IsNullOrWhiteSpace(sBand.Caption);
            if (ugBand.HeaderVisible)
            {
                ugBand.Header.Caption = sBand.Caption;
                var appearance = ugBand.Header.Appearance;
                appearance.BackColor = SystemColors.Window;
                appearance.BorderAlpha = Alpha.Transparent;
                appearance.ThemedElementAlpha = Alpha.Transparent;
                appearance.TextHAlign = HAlign.Left;
                appearance.TextVAlign = VAlign.Middle;
            }
            ugBand.Groups.Clear();
            foreach (var sGroup in sBand.Groups)
            {
                AppendGroup(ugBand, sGroup);
            }
            for (int index = 0; index < sBand.Columns.Count; index++)
            {
                SetupColumn(ugBand, sBand.Columns[index], index);
            }
            HideUnmappedColumns(ugBand);
            if (sBand.BindWidth)
            {
                ugBand.Indentation = 0;
                ugBand.Override.AllowColSizing = AllowColSizing.None;
                BindWidth(ugBand);
            }
            if (ugBand.Tag == null)
            {
                ugBand.Tag = new object();
            }

            if (Band_EditMode)
            {
                var appearanceActiveRow = ugBand.Override.ActiveRowAppearance;
                appearanceActiveRow.BackColor = SystemColors.Window;
                appearanceActiveRow.ForeColor = SystemColors.ControlText;
            }
        }

        private void AppendGroup(UltraGridBand ugBand, ColumnGroupDefinition sGroup, UltraGridGroup ugParentGroup = null)
        {
            var ugGroup = ugBand.Groups.Add(sGroup.Key, sGroup.Caption);
            ugGroup.RowLayoutGroupInfo.LabelSpan = sGroup.RowSpan;
            var appearance = ugGroup.Header.Appearance;
            appearance.TextTrimming = TextTrimming.None;
            appearance.TextHAlign = HAlign.Center;
            appearance.TextVAlign = VAlign.Top;
            ugGroup.RowLayoutGroupInfo.ParentGroup = ugParentGroup;
            foreach (var sSubGroup in sGroup.SubGroups)
            {
                AppendGroup(ugBand, sSubGroup, ugGroup);
            }
            for (int index = 0; index < sGroup.Columns.Count; index++)
            {
                SetupColumn(ugBand, sGroup.Columns[index], index, ugGroup);
            }
        }

        private void SetupColumn(UltraGridBand ugBand, ColumnDefinition sColumn, int index, UltraGridGroup ugParentGroup = null)
        {
            var ugColumn = ugBand.Columns.Cast<UltraGridColumn>().FirstOrDefault(column => column.Key == sColumn.Key);
            if (ugColumn == null)
            {
                return;
            }
            ugColumn.CellClickAction = CellClickAction.RowSelect;
            ugColumn.AllowRowSummaries = AllowRowSummaries.False;
            ugColumn.Format = sColumn.Format;
            ugColumn.RowLayoutColumnInfo.ParentGroup = ugParentGroup;
            ugColumn.Hidden = (sColumn.Visibility == ColumnVisibility.Hidden) || (sColumn.Visibility == ColumnVisibility.Disabled);
            ugColumn.CellAppearance.TextHAlign = (HAlign)Enum.Parse(typeof(HAlign), sColumn.Align.ToString());
            ugColumn.Header.VisiblePosition = index;
            ugColumn.CellActivation = Activation.NoEdit;
            ugColumn.FormatInfo = sColumn.FormatInfo;

            if (sColumn.Width.HasValue)
            {
                ugColumn.MinWidth = sColumn.Width.Value;
            }
            if (sColumn.IsHyperLink)
            {
                ugColumn.CellAppearance.Cursor = Cursors.Hand;
                ugColumn.CellAppearance.FontData.Underline = DefaultableBoolean.True;
                ugColumn.CellAppearance.ForeColor = Color.Blue;
            }
            if (sColumn.IsHtml)
            {
                ugColumn.Style = UltraWinGrid.ColumnStyle.FormattedText;
            }

            if (Band_EditMode && Band_GridRegimEdit == GridRegimEdit.Edit)
            {
                ugColumn.Nullable = Infragistics.Win.UltraWinGrid.Nullable.EmptyString;
            }

            //--- Set Hidden From Parent 
            UltraGridColumn colBandParent = ugColumn.Band.ParentBand.Columns[ugColumn.Key];
            ugColumn.Hidden = colBandParent.Hidden;

            if (sColumn.IsMaskInput)
            {
                ugColumn.MaskInput = sColumn.MaskInput;

                ugColumn.MaskDataMode = MaskMode.Raw;
                ugColumn.MaskDisplayMode = MaskMode.IncludeBoth;
                ugColumn.MaskClipMode = MaskMode.IncludeLiterals;

                ugColumn.PadChar = ' ';
                ugColumn.PromptChar = '_';
            }
            ugColumn.Tag = new object();
        }

        private void HideUnmappedColumns(UltraGridBand ugBand)
        {
            var unmappedColumns =
                from ugColumn in ugBand.Columns.Cast<UltraGridColumn>()
                where !ugColumn.Hidden && ugColumn.Tag == null
                select ugColumn;
            foreach (var ugColumn in unmappedColumns)
            {
                ugColumn.Hidden = true;
            }
        }

        private BandDefinition FindBand(string name, BandDefinition band)
        {
            if (band.Name.Equals(name))
            {
                return band;
            }
            foreach (var item in band.Bands)
            {
                var result = FindBand(name, item);
                if (result != null)
                {
                    return result;
                }
            }
            return null;
        }

        private void BindWidth(UltraGridBand ugBand)
        {
            var ugParentBand = ugBand.ParentBand;
            if (ugParentBand == null)
            {
                return;
            }
            SubObjectPropChangeEventHandler handler = propChange => SyncWidthHandler(ugBand, propChange);
            ugBand.Tag = handler;
            ugParentBand.Columns.SubObjectPropChanged += handler;
            SyncWidthInternal(ugBand);
        }

        private void SyncWidthHandler(UltraGridBand ugBand, PropChangeInfo propChange)
        {
            if (propChange.PropId is UltraWinGrid.PropertyIds
               && ((UltraWinGrid.PropertyIds)propChange.PropId) == UltraWinGrid.PropertyIds.Columns)
            {
                SyncWidthInternal(ugBand);
            }
        }

        private void SyncWidthInternal(UltraGridBand ugBand)
        {
            Func<ColumnsCollection, IEnumerable<UltraGridColumn>> getVisibleColumns = columns =>
                from column in columns.Cast<UltraGridColumn>()
                where !column.Hidden
                orderby column.Header.VisiblePosition
                select column;

            var nestedCloumns = getVisibleColumns(ugBand.Columns).ToList();

            foreach (var nestedColumn in getVisibleColumns(ugBand.Columns))
            {
                var parentColumn = ugBand.ParentBand.Columns.All.Cast<UltraGridColumn>().FirstOrDefault(c => c.Key == nestedColumn.Key);
                if (parentColumn != null && (Band_EditMode && !parentColumn.Hidden))
                {
                    nestedColumn.RowLayoutColumnInfo.PreferredCellSize = parentColumn.RowLayoutColumnInfo.PreferredCellSize;
                    nestedColumn.Width = parentColumn.Width;
                }
            }
        }
    }
}