﻿using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Common.Contracts.DTO.Query
{
    [Serializable]
    public abstract class ColumnBase
    {
        public string Key { get; set; }

        private int _originX = -1;
        public int OriginX { get { return _originX; } set { _originX = value; } }

        /// <summary> Название колонки </summary>
        public string Caption { get; set; }
    }
}
