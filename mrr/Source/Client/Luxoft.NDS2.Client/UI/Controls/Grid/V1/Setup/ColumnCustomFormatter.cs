﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup
{
    public class ColumnCustomFormatter : IFormatProvider, ICustomFormatter
    {
        private string _extString = String.Empty;

        public object GetFormat(Type formatType)
        {
            if (formatType == typeof(ICustomFormatter))
                return this;
            else
                return null;
        }

        public ColumnCustomFormatter()
        {
        }

        public ColumnCustomFormatter(string extString)
        {
            _extString = extString;
        }

        public string Format(string format,
                              object arg,
                              IFormatProvider formatProvider)
        {
            if (!this.Equals(formatProvider))
            {
                return null;
            }
            else
            {
                string ret = String.Empty;
                string formatCurrent = String.Empty;

                if (!string.IsNullOrWhiteSpace(format))
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("{");
                    sb.Append(format);
                    sb.Append("}");
                    sb.Append(_extString);
                    formatCurrent = sb.ToString();
                }
                else
                {
                    formatCurrent = "{0}" + _extString;
                }

                if (arg != null)
                {
                    ret = string.Format(formatCurrent, arg);
                }
                else
                {
                    ret = String.Empty;
                }
                return ret;
            }
        }
    }
}
