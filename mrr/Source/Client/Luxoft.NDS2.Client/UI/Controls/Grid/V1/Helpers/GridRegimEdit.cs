﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V1.Helpers
{
    public enum GridRegimEdit
    {
        View = 1,
        Edit = 2
    }
}
