﻿using System;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup
{
    [Serializable]
    public class GridColumnSort
    {
        public string ColumnKey
        {
            get;
            set;
        }

        public bool IsAscending
        {
            get;
            set;
        }
    }
}
