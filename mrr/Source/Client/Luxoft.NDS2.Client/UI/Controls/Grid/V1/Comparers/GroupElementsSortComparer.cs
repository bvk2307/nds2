﻿using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V1.Comparers
{
    public class GroupElementsSortComparer : BaseComparer, IComparer
    {
        private UltraGrid _grid;
        private string _field;

        public GroupElementsSortComparer(UltraGrid grid, string field, SortKind kind)
        {
            _grid = grid;
            _field = field;
            Kind = kind;
        }

        private object GetPropValue(object src, string propName)
        {
            return src.GetType().GetProperty(propName).GetValue(src, null);
        }

        int IComparer.Compare(object x, object y)
        {
            var elemX = ((UltraGridCell)x).Row.ListObject;
            var elemY = ((UltraGridCell)y).Row.ListObject;

            var valX = Convert.ToString(((UltraGridCell)x).Value);
            var valY = Convert.ToString(((UltraGridCell)y).Value);

            List<Object> dataSource = ((IEnumerable)_grid.DataSource).Cast<object>().ToList();
            var countX = dataSource.Where(a => GetPropValue(a, _field).ToString() == valX).Count();
            var countY = dataSource.Where(b => GetPropValue(b, _field).ToString() == valY).Count();

            if (Kind == SortKind.Asc)
            {
                if (countX > countY)
                {
                    return 1;
                }
                if (countY > countX)
                {
                    return -1;
                }
                if (countX == countY)
                {
                    return String.Compare(valX, valY);
                }
            }
            else
            {
                if (countX > countY)
                {
                    return -1;
                }
                if (countY > countX)
                {
                    return 1;
                }
                if (countX == countY)
                {
                    return String.Compare(valY, valX);
                }
            }

            return 0;
        }
    }
}
