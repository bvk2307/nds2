﻿using System;
using System.Collections.Generic;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnSupport;

namespace Luxoft.NDS2.Common.Contracts.DTO.Query
{
    public enum ColumnAlign
    {
        Default,
        Left,
        Center,
        Right,
    }

    public enum ColumnVisibility
    {
        //Визуальна, но можно скрыть
        Visible,
        //Скрыта, но можно отобразить
        Hidden,
        //Скрыта и недоступна для отображения
        Disabled
    }

    [Serializable]
    public class ColumnDefinition : ColumnBase
    {
        private int _rowSpan;
        /// <summary>
        /// Высота хэдэра в гриде
        /// </summary>
        public int RowSpan
        {
            get { return _rowSpan > 0 ? _rowSpan : 1; }
            set { _rowSpan = value; }
        }

        /// <summary>
        /// Высота колонки
        /// </summary>
        public int Height { get; set; }

        /// <summary>
        /// Ширина колонки
        /// </summary>
        public int? Width { get; set; }

        /// <summary>
        /// Максимальная ширина колонки
        /// </summary>
        public int? MaxWidth { get; set; }

        /// <summary>
        /// Ширина колонки для Экселя
        /// </summary>
        public double? WidthColumnExcel { get; set; }

        /// <summary>
        /// Рассчет summary
        /// </summary>
        public bool AllowSummary { get; set; }

        /// <summary>
        /// Формат выходных данных
        /// </summary>
        public string Format { get; set; }

        [NonSerialized]
        private IFormatProvider _formatInfo;

        /// <summary> Формат провайдер </summary>
        public IFormatProvider FormatInfo
        {
            get { return _formatInfo; }
            set { _formatInfo = value; }
        }

        /// <summary>
        /// Формат выходных данных для Экселя
        /// </summary>
        public string FormatExcel { get; set; }

        [NonSerialized]
        private IFormatProvider _formatInfoExcel;

        /// <summary>
        /// Формат провайдер для Экселя
        /// </summary>
        public IFormatProvider FormatInfoExcel
        {
            get { return _formatInfoExcel; }
            set { _formatInfoExcel = value; }
        }

        /// <summary>
        /// Выравнивание
        /// </summary>
        public ColumnAlign Align { get; set; }

        /// <summary>
        /// Визуальное отображение колонки
        /// </summary>
        public ColumnVisibility Visibility { get; set; }

        /// <summary>
        /// Столбец с чеками
        /// </summary>
        public bool SelectionColumn { get; set; }

        /// <summary>
        /// Всплывающая подсказка
        /// </summary>
        public string ToolTip { get; set; }

        private Action<object, EventArgs> _cellClickAction;
        public Action<object, EventArgs> CellClick
        {
            get
            {
                if(_cellClickAction == null)
                {
                    return (o, e) => { };
                }

                return _cellClickAction;
            }
            set { _cellClickAction = value; }
        }

        public bool IsHyperLink { get; set; }

        public Func<ConditionValueAppearance> CustomConditionValueAppearance { get; set; }

        public bool? AllowRowFiltering { get; set; }

        public bool DisableMoving { get; set; }

        public bool? SortIndicator { get; set; }

        public bool CellMultiLine { get; set; }

        public ColumnEditableDefinition EditableDefinition { get; private set; }

        public ColumnStyleDefinition StyleDefinition { get; private set; }

        public bool IsUseInChangable { get; set; }

        public string BandCellToolTip { get; set; }

        public bool IsRequiredFill { get; set; }

        public bool IsMaskInput { get; set; }

        public string MaskInput { get; set; }

        #region Aggregates

        private bool _allowGrouping = false;

        public ColumnSort Sorting { get; set; }
        
        public bool AllowGrouping
        {
            get
            {
                return _allowGrouping;
            }
            set
            {
                _allowGrouping = value;
            }
        }

        public ColumnGrouping Grouping { get; set; }

        public List<ColumnFilter> Filtering { get; set; }

        #endregion Aggregates

        public ColumnDefinition()
        {
            Align = ColumnAlign.Default;
            Visibility = ColumnVisibility.Visible;
            StyleDefinition = new ColumnStyleDefinition();
            EditableDefinition = new ColumnEditableDefinition();
        }

        public ColumnDefinition(string key)
            : this()
        {
            Key = key;
        }

        public bool IsHtml { get; set; }

        public bool IsAutoIncrement { get; set; }
        public bool DontHide { get; set; }
    }
}