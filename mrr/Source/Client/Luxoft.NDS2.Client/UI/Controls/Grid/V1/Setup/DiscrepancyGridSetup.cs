﻿using System;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup
{
    public sealed class DiscrepancyGridSetup
    {
        /// <summary>
        /// Описание главной таблицы
        /// </summary>
        public BandDefinition Band { get; private set; }

        public DiscrepancyGridSetup()
        {
            Band = new BandDefinition();
        }

        /// <summary>
        /// Поиск банда в дереве по имени
        /// </summary>
        /// <param name="name">Имя искомого банда</param>
        public BandDefinition GetBandByName(string name)
        {
            return name.Equals("List`1") ? Band : FindBand(name, Band);
        }

        private BandDefinition FindBand(string name, BandDefinition band)
        {
            if (name.Equals(band.Name))
            {
                return band;
            }
            foreach (var item in band.Bands)
            {
                var result = FindBand(name, item);
                if (result != null)
                {
                    return result;
                }
            }
            return null;
        }

        # region Настройки для конкретных ячеек строки

        public Dictionary<string, Func<object, string>> CellToolTips { get; set; }

        public Dictionary<string, Func<object, bool>> CellIsHyperLinks { get; set; }

        # endregion
    }
}