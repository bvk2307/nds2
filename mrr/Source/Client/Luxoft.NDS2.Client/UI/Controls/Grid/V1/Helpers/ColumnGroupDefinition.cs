﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Common.Contracts.DTO.Query
{
    [Serializable]
    public class ColumnGroupDefinition : ColumnBase
    {
        public ColumnGroupDefinition()
        {
            SubGroups = new List<ColumnGroupDefinition>();
            Columns = new List<ColumnDefinition>();
            Key = Guid.NewGuid().ToString();
        }

        private int _rowSpan;
        public int RowSpan
        {
            get { return _rowSpan > 0 ? _rowSpan : 1; }
            set { _rowSpan = value; }
        }

        public List<ColumnGroupDefinition> SubGroups { get; set; }

        public List<ColumnDefinition> Columns { get; set; }

        public bool HasSubGroups
        {
            get { return SubGroups != null && SubGroups.Any(); }
        }

        public int? Height { get; set; }

        public int? Width { get; set; }

        public string ToolTip { get; set; }
    }
}