﻿using System;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V1.EventArguments
{
    public class SelectedRowEventArgs : EventArgs 
    {
        public object DataObject { get; private set; }
        public bool IsSelected { get; private set; }


        public SelectedRowEventArgs(object data, bool selected)
        {
            DataObject = data;
            IsSelected = selected;
        }
    }
}
