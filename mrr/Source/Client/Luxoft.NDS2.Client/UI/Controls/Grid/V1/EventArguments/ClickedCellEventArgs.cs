﻿using System;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V1.EventArguments
{
    public class ClickedCellEventArgs : EventArgs
    {
        public object Instance { get; set; }

        public string DataMember { get; set; }

        public ClickedCellEventArgs() { }
    }
}