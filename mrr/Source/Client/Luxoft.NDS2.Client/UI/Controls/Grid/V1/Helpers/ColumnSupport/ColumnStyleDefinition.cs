﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using Infragistics.Win.UltraWinGrid;

namespace Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnSupport
{
    [Serializable]
    public sealed class ColumnStyleDefinition
    {
        public ColumnStyle Style { get; set; }

        public Image Image { get; set; }

        public CellEventHandler Handler { get; set; }

        public ColumnStyleDefinition()
        {
            Style = ColumnStyle.Edit;
            Image = null;
            Handler = null;
        }
    }
}