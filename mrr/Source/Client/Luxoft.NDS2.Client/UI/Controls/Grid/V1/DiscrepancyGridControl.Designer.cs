﻿namespace Luxoft.NDS2.Client.UI.Controls.Grid.V1
{
    partial class DiscrepancyGridControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            this.upHeader = new Infragistics.Win.Misc.UltraPanel();
            this.ulTitle = new Infragistics.Win.Misc.UltraLabel();
            this.columnVisibilityManager = new Infragistics.Win.UltraWinListView.UltraListView();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.btnColumnVisibilitySetup = new Infragistics.Win.Misc.UltraDropDownButton();
            this.popupColumnContainer = new Infragistics.Win.Misc.UltraPopupControlContainer(this.components);
            this.lblGridName = new Infragistics.Win.Misc.UltraLabel();
            this.popupControlContainerGrouping = new Infragistics.Win.Misc.UltraPopupControlContainer(this.components);
            this.grid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.gridBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.upHeader.ClientArea.SuspendLayout();
            this.upHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.columnVisibilityManager)).BeginInit();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridBindingSource)).BeginInit();
            this.gridLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // upHeader
            // 
            // 
            // upHeader.ClientArea
            // 
            this.upHeader.ClientArea.Controls.Add(this.ulTitle);
            this.upHeader.ClientArea.Controls.Add(this.columnVisibilityManager);
            this.upHeader.ClientArea.Controls.Add(this.ultraPanel1);
            this.upHeader.ClientArea.Controls.Add(this.lblGridName);
            this.upHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.upHeader.Location = new System.Drawing.Point(0, 0);
            this.upHeader.Margin = new System.Windows.Forms.Padding(0);
            this.upHeader.Name = "upHeader";
            this.upHeader.Size = new System.Drawing.Size(692, 32);
            this.upHeader.TabIndex = 0;
            // 
            // ulTitle
            // 
            appearance2.TextHAlignAsString = "Left";
            appearance2.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            appearance2.TextVAlignAsString = "Middle";
            this.ulTitle.Appearance = appearance2;
            this.ulTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ulTitle.Location = new System.Drawing.Point(179, 0);
            this.ulTitle.Name = "ulTitle";
            this.ulTitle.Size = new System.Drawing.Size(473, 32);
            this.ulTitle.TabIndex = 6;
            // 
            // columnVisibilityManager
            // 
            this.columnVisibilityManager.BorderStyle = Infragistics.Win.UIElementBorderStyle.Rounded4;
            this.columnVisibilityManager.CausesValidation = false;
            this.columnVisibilityManager.ItemSettings.AllowEdit = Infragistics.Win.DefaultableBoolean.False;
            this.columnVisibilityManager.Location = new System.Drawing.Point(0, 0);
            this.columnVisibilityManager.MainColumn.AllowSizing = Infragistics.Win.DefaultableBoolean.True;
            this.columnVisibilityManager.MainColumn.AutoSizeMode = Infragistics.Win.UltraWinListView.ColumnAutoSizeMode.AllItems;
            this.columnVisibilityManager.Name = "columnVisibilityManager";
            this.columnVisibilityManager.Size = new System.Drawing.Size(340, 247);
            this.columnVisibilityManager.TabIndex = 5;
            this.columnVisibilityManager.TabStop = false;
            this.columnVisibilityManager.View = Infragistics.Win.UltraWinListView.UltraListViewStyle.List;
            this.columnVisibilityManager.ViewSettingsList.CheckBoxStyle = Infragistics.Win.UltraWinListView.CheckBoxStyle.CheckBox;
            this.columnVisibilityManager.ViewSettingsList.ImageSize = new System.Drawing.Size(0, 0);
            this.columnVisibilityManager.ViewSettingsList.MultiColumn = false;
            this.columnVisibilityManager.Visible = false;
            // 
            // ultraPanel1
            // 
            this.ultraPanel1.AutoSize = true;
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.btnColumnVisibilitySetup);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.ultraPanel1.Location = new System.Drawing.Point(652, 0);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(40, 32);
            this.ultraPanel1.TabIndex = 1;
            // 
            // btnColumnVisibilitySetup
            // 
            appearance8.Image = global::Luxoft.NDS2.Client.Properties.Resources.table_select_column;
            appearance8.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance8.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this.btnColumnVisibilitySetup.Appearance = appearance8;
            this.btnColumnVisibilitySetup.CausesValidation = false;
            this.btnColumnVisibilitySetup.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnColumnVisibilitySetup.Location = new System.Drawing.Point(0, 0);
            this.btnColumnVisibilitySetup.Name = "btnColumnVisibilitySetup";
            this.btnColumnVisibilitySetup.PopupItemKey = "columnVisibilityManager";
            this.btnColumnVisibilitySetup.PopupItemProvider = this.popupColumnContainer;
            this.btnColumnVisibilitySetup.Size = new System.Drawing.Size(40, 32);
            this.btnColumnVisibilitySetup.Style = Infragistics.Win.Misc.SplitButtonDisplayStyle.DropDownButtonOnly;
            this.btnColumnVisibilitySetup.TabIndex = 4;
            this.btnColumnVisibilitySetup.TabStop = false;
            // 
            // popupColumnContainer
            // 
            this.popupColumnContainer.PopupControl = this.columnVisibilityManager;
            // 
            // lblGridName
            // 
            appearance1.TextVAlignAsString = "Middle";
            this.lblGridName.Appearance = appearance1;
            this.lblGridName.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblGridName.Location = new System.Drawing.Point(0, 0);
            this.lblGridName.Name = "lblGridName";
            this.lblGridName.Size = new System.Drawing.Size(179, 32);
            this.lblGridName.TabIndex = 0;
            // 
            // grid
            // 
            this.grid.DataSource = this.gridBindingSource;
            appearance3.BackColor = System.Drawing.SystemColors.Window;
            appearance3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.grid.DisplayLayout.Appearance = appearance3;
            this.grid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.grid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            this.grid.DisplayLayout.MaxColScrollRegions = 1;
            this.grid.DisplayLayout.MaxRowScrollRegions = 1;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.grid.DisplayLayout.Override.ActiveCellAppearance = appearance13;
            this.grid.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.grid.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance16.BorderColor = System.Drawing.Color.Silver;
            appearance16.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.grid.DisplayLayout.Override.CellAppearance = appearance16;
            this.grid.DisplayLayout.Override.CellPadding = 0;
            appearance18.TextHAlignAsString = "Left";
            this.grid.DisplayLayout.Override.HeaderAppearance = appearance18;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            appearance19.BorderColor = System.Drawing.Color.Silver;
            this.grid.DisplayLayout.Override.RowAppearance = appearance19;
            this.grid.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.grid.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grid.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.grid.Location = new System.Drawing.Point(3, 35);
            this.grid.Name = "grid";
            this.grid.Size = new System.Drawing.Size(686, 278);
            this.grid.TabIndex = 1;
            this.grid.Text = "GridControl";
            this.grid.UseAppStyling = false;
            this.grid.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            this.grid.AfterRowActivate += new System.EventHandler(this.grid_AfterRowActivate);
            this.grid.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.grid_DoubleClickRow);
            this.grid.BeforeCustomRowFilterDialog += new Infragistics.Win.UltraWinGrid.BeforeCustomRowFilterDialogEventHandler(this.grid_BeforeCustomRowFilterDialog);
            // 
            // gridLayoutPanel
            // 
            this.gridLayoutPanel.ColumnCount = 1;
            this.gridLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.gridLayoutPanel.Controls.Add(this.grid, 0, 1);
            this.gridLayoutPanel.Controls.Add(this.upHeader, 0, 0);
            this.gridLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.gridLayoutPanel.Name = "gridLayoutPanel";
            this.gridLayoutPanel.RowCount = 2;
            this.gridLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.gridLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.gridLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.gridLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.gridLayoutPanel.Size = new System.Drawing.Size(692, 316);
            this.gridLayoutPanel.TabIndex = 4;
            // 
            // DiscrepancyGridControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridLayoutPanel);
            this.Name = "DiscrepancyGridControl";
            this.Size = new System.Drawing.Size(692, 316);
            this.upHeader.ClientArea.ResumeLayout(false);
            this.upHeader.ClientArea.PerformLayout();
            this.upHeader.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.columnVisibilityManager)).EndInit();
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            this.ultraPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridBindingSource)).EndInit();
            this.gridLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraPanel upHeader;
        private Infragistics.Win.UltraWinGrid.UltraGrid grid;
        private System.Windows.Forms.TableLayoutPanel gridLayoutPanel;
        private System.Windows.Forms.BindingSource gridBindingSource;
        private Infragistics.Win.Misc.UltraLabel lblGridName;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.Misc.UltraDropDownButton btnColumnVisibilitySetup;
        private Infragistics.Win.Misc.UltraPopupControlContainer popupColumnContainer;
        private Infragistics.Win.UltraWinListView.UltraListView columnVisibilityManager;
        private Infragistics.Win.Misc.UltraPopupControlContainer popupControlContainerGrouping;
        private Infragistics.Win.Misc.UltraLabel ulTitle;

    }
}
