﻿using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V1.FilterConditions
{
    public class AlwaysMetFilterCondition : FilterCondition
    {
        public AlwaysMetFilterCondition(FilterCondition originalFilterCondition)
            : base(originalFilterCondition.Column, originalFilterCondition.ComparisionOperator, originalFilterCondition.CompareValue) { }

        public override bool MeetsCriteria(UltraGridRow row)
        {
            /*if ((this.CompareValue is DateTime) == false)
                return base.MeetsCriteria(row);
            DateTime compareDate = (DateTime)this.CompareValue;
            DateTime val = new DateTime();
            bool valid = DateTime.TryParse(row.Cells[this.Column].Value.ToString(), out val);*/
            /*if (valid)
            {                
                return compareDate.Date == ((DateTime)row.Cells[this.Column].Value).Date;
            }                
            else
                return false;*/
            return true;
        }
    }
}
