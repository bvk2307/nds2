﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Infragistics.Win.UltraWinGrid;

namespace Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnSupport
{
    [Serializable]
    public sealed class ColumnEditableDefinition
    {
        public bool Editable { get; set; }

        public CellEventHandler Handler { get; set; }

        public string MaskInput { get; set; }

        public Infragistics.Win.UltraWinGrid.Nullable Nullable { get; set; }
    }
}