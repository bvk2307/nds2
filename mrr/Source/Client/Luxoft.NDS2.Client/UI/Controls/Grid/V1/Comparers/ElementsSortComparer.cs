﻿using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V1.Comparers
{
    public class ElementsSortComparer: BaseComparer, IComparer
    {
        private UltraGrid _grid;
        private string _masterField;
        private string _slaveField;

        public ElementsSortComparer(UltraGrid grid, string masterField, string slaveField, SortKind kind)
        {
            _grid = grid;
            _masterField = masterField;
            _slaveField = slaveField;
            Kind = kind;
        }

        private object GetPropValue(object src, string propName)
        {
            if (src == null) return null;

            return src.GetType().GetProperty(propName).GetValue(src, null);
        }

        int IComparer.Compare(object x, object y)
        {
            var elemX = ((UltraGridCell)x).Row.ListObject;
            var elemY = ((UltraGridCell)y).Row.ListObject;

            if ((elemX == null) || (elemY == null))
            {
                return 0;
            }

            List<Object> dataSource = ((IEnumerable)_grid.DataSource).Cast<object>().ToList();

            var valXMaster = GetPropValue(elemX, _masterField).ToString();
            var valYMaster = GetPropValue(elemY, _masterField).ToString();

            var elemsX = (List<object>)dataSource.Where(a => GetPropValue(a, _masterField).ToString() == valXMaster).ToList();
            var elemsY = (List<object>)dataSource.Where(b => GetPropValue(b, _masterField).ToString() == valYMaster).ToList();

            if (valXMaster == valYMaster)
            {
                return 0;
            }

            decimal valXSlave = 0;
            decimal valYSlave = 0;
            
            foreach(var elem in elemsX)
            {
                var tmpValX = GetPropValue(elemX, _slaveField).ToString();
                valXSlave += Convert.ToDecimal(tmpValX);
            }
            foreach(var elem in elemsY)
            {
                var tmpValY = GetPropValue(elemY, _slaveField).ToString();
                valYSlave += Convert.ToDecimal(tmpValY);
            }

            if (Kind == SortKind.Asc)
            {
                if (valXSlave == valYSlave)
                {
                    return String.Compare(valXMaster, valYMaster);
                }

                return Decimal.Compare(valXSlave, valYSlave);
            }
            else
            {
                if (valYSlave == valXSlave)
                {
                    return String.Compare(valYMaster, valXMaster);
                }

                return Decimal.Compare(valYSlave, valXSlave);
            }
        }
    }
}
