﻿using System;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Forms;
using Infragistics.Win;
using Infragistics.Win.FormattedLinkLabel;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinListView;
using Infragistics.Win.UltraWinMaskedEdit;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.EventArguments;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Helpers;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns.AutoComplete;
using ColumnHeader = Infragistics.Win.UltraWinGrid.ColumnHeader;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V1
{
    public partial class GridControl : UserControl
    {
        # region Конструктор

        private UserDefinedSettingsManager _customSettings = null;
        private ContextMenuStrip _contextMenuStrip;     

        public GridControl()
        {
            InitializeComponent();
            InitTooltip();
            ultraComboEditorLinesPerPage.DataSource = RowsPerPageNumber;
            grid.BeforeRowExpanded += Grid_BeforeRowExpanded;
            grid.DisplayLayout.Override.RowFilterAction = RowFilterAction.AppearancesOnly;
            grid.MouseDown += (sender, args) =>
            {
                /*if (args.Button == MouseButtons.Middle)
                {
                    DescribePositions();
                }*/

                if (args.Button != MouseButtons.Right)
                    return;
                foreach (var r in grid.Selected.Rows)
                    r.Selected = false;

                var element = grid.DisplayLayout.UIElement.ElementFromPoint(args.Location);
                var row = element.GetContext(typeof(UltraGridRow)) as UltraGridRow;

                if (row != null && row.IsDataRow)
                {
                    row.Selected = true;
                    grid.ActiveRow = row;
                    if (grid.ContextMenuStrip == null)
                        grid.ContextMenuStrip = _contextMenuStrip;
                }
                else
                {
                    grid.ContextMenuStrip = null;
                }
            };

            var rc = Infragistics.Win.UltraWinGrid.Resources.Customizer;
            rc.SetCustomizedString("FilterDialogOkButtonNoFiltersText", "Без фильтров");
            rc.SetCustomizedString("RowFilterDialogDBNullItem", "Введите значение");
        }

        public void ResetShowSelectedItem()
        {
            grid.DisplayLayout.Override.ActiveCellAppearance.Reset();
            grid.DisplayLayout.Override.ActiveRowAppearance.Reset();
        }

        public void SetupMultiSelect(bool isEnabled)
        {
            if (isEnabled)
            {
                //grid.DisplayLayout.Override.SelectTypeCell = SelectType.None;
                grid.DisplayLayout.Override.SelectTypeRow = SelectType.Extended;
            }
            else
            {
                //grid.DisplayLayout.Override.SelectTypeCell = SelectType.None;
                grid.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            }
        }

        protected UltraGrid UltraGrid
        {
            get
            {
                return grid;
            }
        }

 
        # endregion

        # region Кастомизация

        private List<string> _bindedColumns = new List<string>();

        private void EnlistColumnsInSetup(GridSetup setup)
        {
            _bindedColumns.Clear();

            if (setup != null)
            {
                foreach (var columnBase in setup.Columns)
                {
                    AddKeyToList(columnBase, _bindedColumns);
                }
            }
        }

        private void AddKeyToList(ColumnBase col, List<string> collection)
        {
            if (!collection.Contains(col.Key))
            {
                collection.Add(col.Key);
            }

            if (col is ColumnGroupDefinition)
            {
                foreach (var childCol in ((ColumnGroupDefinition)col).Columns)
                {
                    AddKeyToList(childCol, collection);
                }

                foreach (var childCol in ((ColumnGroupDefinition)col).SubGroups)
                {
                    AddKeyToList(childCol, collection);
                }
            }
        }

        private GridSetup _setup = null;
        public GridSetup Setup
        {
            get
            {
                return _setup;
            }
            set
            {
                _setup = value;

                EnlistColumnsInSetup(_setup);
                ApplyCurrentSetup();

                RequestNewData();
            }
        }

        private void ApplyCurrentSetup()
        {
            if (Setup == null) return;

            QueryConditions = Setup.QueryCondition ?? new QueryConditions();

            _customSettings =
                new UserDefinedSettingsManager(
                    grid.DisplayLayout.Bands[0],
                    _setup);

            PrepareWorkspace();
            CreateColumns();

            grid.DisplayLayout.Bands[0].RowLayoutStyle = RowLayoutStyle.GroupLayout;
            grid.DisplayLayout.Override.WrapHeaderText = DefaultableBoolean.True;
            grid.DisplayLayout.Override.TipStyleCell = _setup.TypeStyleCell;

            _cellToolTips = _setup.CellToolTips;
            _сellsIsHyperLinks = _setup.CellIsHyperLinks;
            _bandCellToolTips = _setup.BandCellToolTips;
            _rowActions = _setup.RowActions;
            _cellUpdateAction = _setup.CellUpdateAction;
        }

        private void PrepareWorkspace()
        {
            ColumnsToSummary.Clear();
            _unavailableColumnsKeys.Clear();
        }

        private List<SummarySettings> _summaryCells = new List<SummarySettings>();
        private void ApplySummaries()
        {
            var bnd = grid.DisplayLayout.Bands[0];
            bnd.Summaries.Clear();

            foreach (var elem in ColumnsToSummary)
            {
                var settings = bnd.Summaries.Add(SummaryType.Sum, elem);
                settings.SummaryType = SummaryType.Custom;
                settings.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
                settings.CustomSummaryCalculator = Setup.SummaryRowCalculator;
                settings.DisplayFormat = "{0:### ### ### ### ##0.00}";
                settings.Tag = elem.Key;
                _summaryCells.Add(settings);
            }

            grid.DisplayLayout.Override.SummaryDisplayArea = SummaryDisplayAreas.GroupByRowsFooter | SummaryDisplayAreas.BottomFixed;
            bnd.Override.SummaryFooterCaptionAppearance.FontData.Bold = DefaultableBoolean.True;
            bnd.Override.SummaryFooterCaptionAppearance.BackColor = Color.WhiteSmoke;
            bnd.Override.SummaryFooterCaptionAppearance.ForeColor = Color.Black;
            bnd.Override.SummaryValueAppearance.BackColor = Color.WhiteSmoke;
            bnd.Override.SummaryFooterCaptionVisible = DefaultableBoolean.True;
            bnd.Override.SummaryValueAppearance.TextHAlign = HAlign.Right;
            bnd.Override.SummaryValueAppearance.FontData.Bold = DefaultableBoolean.True;
        }

        private void InitTooltip()
        {
            Infragistics.Win.UltraWinToolTip.UltraToolTipInfo ultraToolTipInfo1 = new Infragistics.Win.UltraWinToolTip.UltraToolTipInfo("Сбросить", Infragistics.Win.ToolTipImage.None, "", Infragistics.Win.DefaultableBoolean.True);
            Infragistics.Win.UltraWinToolTip.UltraToolTipInfo ultraToolTipInfo2 = new Infragistics.Win.UltraWinToolTip.UltraToolTipInfo("Настроить видимость столбцов", Infragistics.Win.ToolTipImage.None, "", Infragistics.Win.DefaultableBoolean.True);
            Infragistics.Win.UltraWinToolTip.UltraToolTipInfo ultraToolTipInfo3 = new Infragistics.Win.UltraWinToolTip.UltraToolTipInfo("Сбросить фильтр", Infragistics.Win.ToolTipImage.None, "", Infragistics.Win.DefaultableBoolean.True);
            this.ultraToolTipManager1.SetUltraToolTip(this.btnReset, ultraToolTipInfo1);
            this.ultraToolTipManager1.SetUltraToolTip(this.btnColumnVisibilitySetup, ultraToolTipInfo2);
            this.ultraToolTipManager1.SetUltraToolTip(this.btnFilterReset, ultraToolTipInfo3);
            this.ultraToolTipManager1.DisplayStyle = ToolTipDisplayStyle.Standard;
            Infragistics.Win.UltraWinToolTip.UltraToolTipInfo ultraToolTipInfoExportExcel = new Infragistics.Win.UltraWinToolTip.UltraToolTipInfo("Экпорт записей о СФ в MS Excel", Infragistics.Win.ToolTipImage.None, "", Infragistics.Win.DefaultableBoolean.True);
            this.ultraToolTipManager1.SetUltraToolTip(this.btnExportExcel, ultraToolTipInfoExportExcel);
            Infragistics.Win.UltraWinToolTip.UltraToolTipInfo ultraToolTipInfoExportExcelCancel = new Infragistics.Win.UltraWinToolTip.UltraToolTipInfo("Отменить экпорт записей о СФ", Infragistics.Win.ToolTipImage.None, "", Infragistics.Win.DefaultableBoolean.True);
            this.ultraToolTipManager1.SetUltraToolTip(this.btnExportExcelCancel, ultraToolTipInfoExportExcelCancel);
        }

        # endregion

        #region editable panel


        #endregion

        #region Создание столбцов

        private void CreateColumns()
        {
            AddGridElements(Setup.Columns);

            if (!_customSettings.Restore())
            {
                _customSettings.Reset();
            }

            RefreshResetButtonState();
            BindGridEvents();

            var c =
                grid.DisplayLayout.Bands[0].Columns.All.Cast<UltraGridColumn>()
                .FirstOrDefault(x => x.Key == _checkColumName);
            if (c != null)
            {
                c.MinWidth = 30;
                c.Width = 30;
                c.MaxWidth = 30;
                c.LockedWidth = true;
            }
        }

        private void AddGridElements(List<ColumnBase> elements, UltraGridGroup parentGroup = null)
        {
            foreach (var elem in Setup.Columns)
            {
                var currColumn = elem as ColumnDefinition;
                if (currColumn != null)
                {
                    AddColumn(currColumn);
                    continue;
                }

                var currGroup = elem as ColumnGroupDefinition;
                if (currGroup != null)
                {
                    AddGroup(currGroup);
                }
            }
        }

        #region TODO Выяснить проблему порядка колонок/групп

        private void DescribePositions()
        {
            var sb = new StringBuilder();
            foreach (var col in grid.DisplayLayout.Bands[0].Columns)
            {
                sb.Append(DescribePosition(col));
            }
            MessageBox.Show(sb.ToString(), "info");
        }

        private string DescribePosition(UltraGridColumn column)
        {
            if (column == null || !column.IsVisibleInLayout)
                return string.Empty;
            try
            {
                return string.Format("{0}{1}[{2} - {3}]\n",
                    DescribePosition(column.RowLayoutColumnInfo.ParentGroup), string.IsNullOrWhiteSpace(column.Header.Caption) ? column.Key : column.Header.Caption,
                    column.RowLayoutColumnInfo.OriginX, column.RowLayoutColumnInfo.OriginXResolved);
            }
            catch (Exception e)
            {
                return string.Format("{0}{1}[{2}].",
                    DescribePosition(column.RowLayoutColumnInfo.ParentGroup), column.Header.Caption,
                    e.Message);
            }
        }

        private string DescribePosition(UltraGridGroup group)
        {
            if (group == null)
                return string.Empty;
            try
            {
                return string.Format("{0}{1}[{2} - {3}].",
                    DescribePosition(group.RowLayoutGroupInfo.ParentGroup), group.Header.Caption,
                    group.RowLayoutGroupInfo.OriginX, group.RowLayoutGroupInfo.OriginXResolved);
            }
            catch (Exception e)
            {
                return string.Format("{0}{1}[{2}].",
                    DescribePosition(group.RowLayoutGroupInfo.ParentGroup), group.Header.Caption,
                    e.Message);
            }
        }

        #endregion

        private List<UltraGridColumn> _columnsToSummary = new List<UltraGridColumn>();
        public List<UltraGridColumn> ColumnsToSummary
        {
            get
            {
                return _columnsToSummary;
            }
        }

        private bool _addVirtualCheckColumn = false;
        public bool AddVirtualCheckColumn
        {
            get
            {
                return _addVirtualCheckColumn;
            }
            set
            {
                if (value)
                {
                    if (_checkColumn != null) return;

                    var col = grid.DisplayLayout.Bands[0].Columns.Add();
                    SetColumnCheckRole(col);
                }
                else
                {
                    if (grid.DisplayLayout.Bands[0].Columns.Contains(_checkColumName))
                    {
                        grid.DisplayLayout.Bands[0].Columns.Remove(_checkColumName);
                    }
                }
            }
        }

        private List<string> _unavailableColumnsKeys = new List<string>();
        private void AddColumn(ColumnDefinition currColumn, UltraGridGroup parentGroup = null)
        {
            UltraGridColumn col = null;

            if (currColumn.SelectionColumn && _checkColumn == null)
            {
                _checkColumName = currColumn.Key;
                col = grid.DisplayLayout.Bands[0].Columns.Add(_checkColumName, String.Empty);
                SetColumnCheckRole(col);
                currColumn.AllowRowFiltering = false;
            }
            else if (currColumn.IsAutoIncrement)
            {
                col = BuildIncrementColumn(currColumn, parentGroup);
            }
            else
            {
                #region general column

                col = grid.DisplayLayout.Bands[0].Columns.Add(currColumn.Key, currColumn.Caption);
                col.CellActivation = Activation.NoEdit;
                col.CellClickAction = CellClickAction.RowSelect;

                if (currColumn.CellClick != null)
                {
                    grid.ClickCell +=
                        (sender, args) =>
                        {
                            bool isCanRowHyperLink = GetCanCellsIsHyperLink(args.Cell);
                            if (args.Cell.Column.Key == col.Key && isCanRowHyperLink)
                            {
                                currColumn.CellClick(sender, args);
                            }
                        };
                }

                if (currColumn.IsHyperLink)
                {
                    col.CellAppearance.Cursor = Cursors.Hand;
                    col.CellAppearance.FontData.Underline = DefaultableBoolean.True;
                    col.CellAppearance.ForeColor = Color.Blue;
                }

                if (currColumn.EditableDefinition.Editable)
                {
                    col.CellActivation = Activation.AllowEdit;
                    col.CellClickAction = CellClickAction.Edit;
                    col.Nullable = currColumn.EditableDefinition.Nullable;

                    if (!string.IsNullOrEmpty(currColumn.EditableDefinition.MaskInput))
                    {
                        col.MaskInput = currColumn.EditableDefinition.MaskInput;
                        col.MaskDataMode = MaskMode.Raw;
                        col.MaskDisplayMode = MaskMode.Raw;
                        col.MaskClipMode = MaskMode.IncludeBoth;
                        col.PadChar = ' ';
                        col.PromptChar = '_';
                    }

                    if (currColumn.EditableDefinition.Handler != null)
                    {
                        grid.AfterCellUpdate += (sender, e) => GridCellEventHandler(sender, e, currColumn.Key, currColumn.EditableDefinition.Handler);
                    }
                }
                if (currColumn.StyleDefinition.Style == Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton
                    || currColumn.StyleDefinition.Style == Infragistics.Win.UltraWinGrid.ColumnStyle.Button)
                {
                    col.ButtonDisplayStyle = Infragistics.Win.UltraWinGrid.ButtonDisplayStyle.Always;
                    col.Style = currColumn.StyleDefinition.Style;
                    col.CellButtonAppearance.Image = currColumn.StyleDefinition.Image;
                    grid.ClickCellButton += (sender, e) => GridCellEventHandler(sender, e, currColumn.Key, currColumn.StyleDefinition.Handler);
                }
                if (currColumn.IsHtml)
                {
                    col.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.FormattedText;
                    ((FormattedLinkEditor)col.Editor).UnderlineLinks = UnderlineLink.Always;
                    //((FormattedLinkEditor)col.Editor).LinkClicked += new Infragistics.Win.FormattedLinkLabel.LinkClickedEventHandler(rtfColumnn_LinkClicked);
                    col.MaskClipMode = MaskMode.Raw;
                    ((FormattedLinkEditor)col.Editor).TreatValueAs = TreatValueAs.FormattedText;
                }

                #endregion
            }

            if (parentGroup != null)
            {
                col.RowLayoutColumnInfo.ParentGroup = parentGroup;
                col.Group = parentGroup;
            }

            col.Header.ToolTipText = currColumn.ToolTip;
            col.RowLayoutColumnInfo.SpanY = currColumn.RowSpan;
            col.RowLayoutColumnInfo.OriginX = currColumn.OriginX;
            col.Format = currColumn.Format;
            col.FormatInfo = currColumn.FormatInfo;
            col.Header.Appearance.TextHAlign = HAlign.Center;
            col.CellAppearance.TextHAlign = (HAlign)Enum.Parse(typeof(HAlign), currColumn.Align.ToString());

            if (this.AllowRowFiltering == DefaultableBoolean.True && currColumn.AllowRowFiltering == false)
            {
                col.AllowRowFiltering = DefaultableBoolean.False;
            }
            if (currColumn.SortIndicator != null && currColumn.SortIndicator == false)
            {
                col.SortIndicator = SortIndicator.Disabled;
            }
            if (currColumn.CellMultiLine)
            {
                col.CellMultiLine = DefaultableBoolean.True;
                grid.DisplayLayout.Override.RowSizing = RowSizing.AutoFree;
            }

            col.Hidden = (currColumn.Visibility == ColumnVisibility.Hidden) || (currColumn.Visibility == ColumnVisibility.Disabled);
            if (col.Hidden)
                _customSettings.Hide(parentGroup);
            if (currColumn.Visibility == ColumnVisibility.Disabled || currColumn.DontHide)
            {
                _unavailableColumnsKeys.Add(currColumn.Key);
            }

            col.AllowRowSummaries = AllowRowSummaries.False;
            if (currColumn.AllowSummary)
            {
                _columnsToSummary.Add(col);
            }

            if (currColumn.Width.HasValue)
            {
                col.MinWidth = currColumn.Width.Value;
            }

            if (currColumn.MaxWidth.HasValue)
            {
                col.MaxWidth = currColumn.MaxWidth.Value;
            }

            if (currColumn.CustomConditionValueAppearance != null)
            {
                col.ValueBasedAppearance = currColumn.CustomConditionValueAppearance();
            }
        }

        private void GridCellEventHandler(object sender, CellEventArgs e, string memberName, CellEventHandler handler)
        {
            if (handler != null && e.Cell.Column.Key == memberName)
            {
                handler(sender, e);
            }
        }

        private void AddGroup(ColumnGroupDefinition currGroup, UltraGridGroup parentGroup = null)
        {
            UltraGridGroup group = grid.DisplayLayout.Bands[0].Groups.Add(currGroup.Key, currGroup.Caption);

            if (parentGroup != null)
            {
                group.RowLayoutGroupInfo.ParentGroup = parentGroup;
            }

            group.RowLayoutGroupInfo.SpanY = group.RowLayoutGroupInfo.LabelSpan = currGroup.RowSpan;
            group.RowLayoutGroupInfo.OriginX = currGroup.OriginX;
            group.Header.Appearance.TextTrimming = TextTrimming.None;
            group.Header.Appearance.TextHAlign = HAlign.Center;
            group.Header.Appearance.TextVAlign = VAlign.Top;

            WalkChildrenElementsInGroup(currGroup, group);
        }

        private void WalkChildrenElementsInGroup(ColumnGroupDefinition currGroup, UltraGridGroup group)
        {
            var grColumns = currGroup.Columns;
            foreach (var elem in grColumns)
            {
                AddColumn(elem, group);
            }

            if (currGroup.HasSubGroups)
            {
                var grSubGroups = currGroup.SubGroups;
                foreach (var elem in grSubGroups)
                {
                    AddGroup(elem, group);
                }
            }
        }

        public void ChangeColumnsCaption(Dictionary<string, GridColumnHeaderChanage> columns)
        {
            foreach (KeyValuePair<string, GridColumnHeaderChanage> item in columns)
            {
                foreach (UltraGridGroup itemGroup in grid.DisplayLayout.Bands[0].Groups)
                {
                    if (itemGroup.Key == item.Key)
                    {
                        itemGroup.Header.Caption = item.Value.Caption;
                        itemGroup.Header.ToolTipText = item.Value.ToolTipText;
                        itemGroup.Hidden = item.Value.Hidden;
                        break;
                    }
                }
                foreach (UltraGridColumn itemColumn in grid.DisplayLayout.Bands[0].Columns)
                {
                    if (itemColumn.Key == item.Key)
                    {
                        itemColumn.Header.Caption = item.Value.Caption;
                        itemColumn.Header.ToolTipText = item.Value.ToolTipText;
                        itemColumn.Hidden = item.Value.Hidden;
                        break;
                    }
                }
            }
        }

        #endregion Создание столбцов

        # region Данные

        private QueryConditions _queryConditions = new QueryConditions();
        public QueryConditions QueryConditions
        {
            get
            {
                return _queryConditions;
            }
            private set
            {
                _queryConditions = value;
            }
        }

        private object DataSource
        {
            get
            {
                return gridBindingSource.DataSource;
            }
            set
            {
                gridBindingSource.DataSource = value;
            }
        }

        public object GetDataSource()
        {
            return this.DataSource;
        }

        public List<UltraGridRow> GetSelectedRows()
        {
            List<UltraGridRow> selectedRows = new List<UltraGridRow>();
            foreach (UltraGridRow row in grid.Selected.Rows)
            {
                selectedRows.Add(row);
            }
            return selectedRows;
        }

        public List<UltraGridRow> GetRows()
        {
            List<UltraGridRow> selectedRows = new List<UltraGridRow>();
            selectedRows.AddRange(grid.Rows);
            return selectedRows;
        }

        private void RequestNewData(
            QueryConditions conditions = null,
            bool needToUpdateTotalRowsCount = false)
        {
            if (Setup == null) return;
            if (Setup.GetData == null) return;
            if (QueryConditions == null && conditions == null) return;

            if (this.BeforeLoadData != null)
            {
                this.BeforeLoadData(this, new EventArgs());
            }


            var currQc = conditions ?? QueryConditions;

            long rowsNumber;
            DataSource = Setup.GetData(currQc, out rowsNumber);
            SetupMultiBands();
            TotalRows = rowsNumber;

            if (Band_EditMode)
            {
                foreach (UltraGridRow gridRow in grid.Rows)
                {
                    gridRow.ExpansionIndicator = ShowExpansionIndicator.CheckOnDisplay;
                }
                grid.Rows.Refresh(Infragistics.Win.UltraWinGrid.RefreshRow.ReloadData, true);
                foreach (UltraGridRow gridRow in grid.Rows)
                {
                    if (!gridRow.IsExpandable && !gridRow.IsExpanded)
                        gridRow.ExpandAll();
                }
            }

            grid.Refresh();
            HideNonSetupCols();
            DisplayPageNumberLabel();
            ApplySummaries();

            if (_checkColumn != null)
            {
                _rowSelected.Clear();
                foreach (var row in grid.Rows)
                {
                    row.Cells[_checkColumName].Activation =
                        _checkColumnActivityCriteria(row.ListObject) ? Activation.AllowEdit : Activation.Disabled;
                    if ((bool)row.Cells[_checkColumName].Value)
                    {
                        _rowSelected.Add(row.ListObject);
                    }
                }

                if (grid.Rows.Count == _rowSelected.Count)
                {
                    _checkColumn.SetHeaderCheckedState(grid.Rows, true);
                }
                RiseGridRowsCheckedStateChanged(new EventArgs());
            }

            UpdateCells();
            UpdateRowsActions();

            btnPrev.Enabled = QueryConditions.PageIndex > 1;
            btnNext.Enabled = QueryConditions.PageIndex < TotalPages;

            if (this.AfterLoadData != null)
            {
                this.AfterLoadData(this, new EventArgs());
            }
        }

        private void HideNonSetupCols()
        {
            if (grid.DisplayLayout.Bands[0] != null)
            {
                foreach (var col in grid.DisplayLayout.Bands[0].Columns)
                {
                    if (!_bindedColumns.Contains(col.Key))
                    {
                        col.Hidden = true;
                    }
                }
            }
        }

        public void UpdateData(
            bool useCachedTotalRowCount = false)
        {
            RequestNewData(null, !useCachedTotalRowCount);
        }

        private List<object> _rows = new List<object>();
        public ReadOnlyCollection<object> Rows
        {
            get
            {
                foreach (var elem in grid.Rows)
                {
                    _rows.Add(elem.ListObject);
                }

                return _rows.AsReadOnly();
            }
        }

        private bool _isInReadOnlyState = false;

        public void UpdateRow<TData>(TData obj, Func<TData, TData, bool> comparer = null)
        {
            int index = -1;
            int listIndex = -1;

            foreach (var row in grid.Rows)
            {
                var equal =
                    comparer == null
                    ? obj.Equals(row.ListObject)
                    : comparer(obj, (TData)row.ListObject);

                if (equal)
                {
                    index = row.Index;
                    listIndex = row.ListIndex;
                }
            }

            var dataSource = DataSource as IEnumerable<TData>;

            if (dataSource != null && listIndex != -1)
            {
                var list = dataSource.ToList();
                list.RemoveAt(listIndex);
                list.Insert(listIndex, obj);
                DataSource = list;
                grid.Rows[index].Refresh();
            }
        }

        private Dictionary<string, Func<object, string>> _cellToolTips = null;
        private Dictionary<string, Func<object, bool>> _сellsIsHyperLinks = null;
        private Dictionary<string, string> _bandCellToolTips = null;

        private void UpdateCells()
        {
            for (int i = -1; ++i < grid.Rows.Count; )
            {
                UltraGridRow row = grid.Rows[i];
                UpdateCellsToolTip(row);
                UpdateCellsIsHyperLink(row);
                if (row.ChildBands != null)
                {
                    var nestedRows = row.ChildBands.Cast<UltraGridChildBand>().SelectMany(childBand => childBand.Rows);
                    foreach (var nestedRow in nestedRows)
                    {
                        UpdateBandCellsToolTip(nestedRow);
                    }
                }
            }
        }

        private void UpdateCellsToolTip(UltraGridRow row)
        {
            if (_cellToolTips != null)
            {
                foreach (KeyValuePair<string, Func<object, string>> item in _cellToolTips)
                {
                    if (row.Cells.Exists(item.Key))
                    {
                        row.Cells[item.Key].ToolTipText = item.Value.Invoke(row.ListObject);
                    }
                }
            }
        }

        private void UpdateBandCellsToolTip(UltraGridRow row)
        {
            if (_bandCellToolTips != null)
            {
                foreach (KeyValuePair<string, string> item in _bandCellToolTips)
                {
                    if (row.Cells.Exists(item.Key))
                    {
                        row.Cells[item.Key].ToolTipText = item.Value;
                    }
                }
            }
        }

        private void UpdateCellsIsHyperLink(UltraGridRow row)
        {
            if (_сellsIsHyperLinks != null)
            {
                foreach (KeyValuePair<string, Func<object, bool>> item in _сellsIsHyperLinks)
                {
                    if (row.Cells.Exists(item.Key))
                    {
                        UltraGridCell cell = row.Cells[item.Key];
                        if (item.Value.Invoke(row.ListObject))
                        {
                            cell.Appearance.Cursor = Cursors.Hand;
                            cell.Appearance.FontData.Underline = DefaultableBoolean.True;
                            cell.Appearance.ForeColor = System.Drawing.Color.Blue;
                        }
                        else
                        {
                            cell.Appearance.Cursor = Cursors.Default;
                            cell.Appearance.FontData.Underline = DefaultableBoolean.Default;
                            cell.Appearance.ResetForeColor();
                        }
                    }
                }
            }
        }

        private bool GetCanCellsIsHyperLink(UltraGridCell cell)
        {
            bool isCan = true;
            if (_сellsIsHyperLinks != null)
            {
                if (_сellsIsHyperLinks.ContainsKey(cell.Column.Key))
                {
                    isCan = _сellsIsHyperLinks[cell.Column.Key].Invoke(cell.Row.ListObject);
                }
            }
            return isCan;
        }

        private List<Action<UltraGridRow>> _rowActions = null;
        private void UpdateRowsActions()
        {
            if (_rowActions == null)
            {
                return;
            }

            foreach (var row in GetRowsRecursive(grid.Rows))
            {
                foreach (var action in _rowActions)
                {
                    if (action != null)
                    {
                        action(row);
                    }
                }
            }
        }

        private IEnumerable<UltraGridRow> GetRowsRecursive(RowsCollection rows)
        {
            foreach (var row in rows)
            {
                yield return row;
                if (row.ChildBands != null)
                {
                    var nestedRows = row.ChildBands.Cast<UltraGridChildBand>().SelectMany(childBand => childBand.Rows);
                    foreach (var nestedRow in nestedRows)
                    {
                        yield return nestedRow;
                    }
                }
            }
        }

        private void UpdateOneRowActions(UltraGridRow row)
        {
            if (_rowActions == null)
            {
                return;
            }

            foreach (var action in _rowActions)
            {
                if (action != null)
                {
                    action(row);
                }
            }
        }

        private Action<UltraGridCell> _cellUpdateAction = null;

        private void CellUpdateAction(UltraGridCell cell)
        {
            if (_cellUpdateAction != null)
            {
                _cellUpdateAction(cell);
            }
        }

        # endregion

        #region Пэйджинг

        public long TotalPages
        {
            get
            {
                var result = (long)Math.Ceiling((double)TotalRows / (double)QueryConditions.PageSize);
                return result == 0 ? 1 : result;
            }
        }

        public long TotalRows { get; private set; }

        public List<string> RowsPerPageNumber
        {
            get
            {
                var lst = new List<string>();

                for (long l = 50; l <= 200; l += 50)
                {
                    lst.Add(l.ToString());
                }

                return lst;
            }
        }

        private void btnPrev_Click(object sender, EventArgs e)
        {
            if (QueryConditions == null) return;
            if (Setup == null) return;
            if (Setup.GetData == null) return;

            QueryConditions.PageIndex--;
            lblPageIndex.Text = string.Format("СТраница {0}, загрузка...", QueryConditions.PageIndex.ToString(CultureInfo.InvariantCulture));

            RequestNewData(QueryConditions);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (Setup == null) return;
            if (Setup.GetData == null) return;
            if (QueryConditions == null) return;

            QueryConditions.PageIndex++;
            lblPageIndex.Text = string.Format("СТраница {0}, загрузка...", QueryConditions.PageIndex.ToString(CultureInfo.InvariantCulture));

            RequestNewData(QueryConditions);
        }

        #region Строки на страницу

        private void ApplyGridLinesPerPageNumber()
        {
            ushort linesNumber = 0;
            if (!ushort.TryParse(ultraComboEditorLinesPerPage.Text, out linesNumber)) return;

            if ((linesNumber < 1) | (linesNumber > 1000))
            {
                ultraComboEditorLinesPerPage.Text = QueryConditions.DEFAULT_PAGE_SIZE.ToString();
                return;
            }

            QueryConditions.PageSize = linesNumber;
            QueryConditions.PageIndex = 1;
            RequestNewData(QueryConditions);

        }

        public void ApplyPageNumberWithoutRequest()
        {
            ushort linesNumber = 0;
            if (!ushort.TryParse(ultraComboEditorLinesPerPage.Text, out linesNumber)) return;

            if ((linesNumber < 1) | (linesNumber > 1000))
            {
                ultraComboEditorLinesPerPage.Text = QueryConditions.DEFAULT_PAGE_SIZE.ToString();
                return;
            }

            QueryConditions.PageSize = linesNumber;
            QueryConditions.PageIndex = 1;
        }

        private void ultraComboEditorLinesPerPage_KeyDown(object sender, KeyEventArgs e)
        {
            KeysConverter kc = new KeysConverter();
            char keyChar = kc.ConvertToString(e.KeyData)[0];
            if (Char.IsDigit(keyChar))
            {
                e.Handled = false;
                return;
            }

            if (e.KeyCode == Keys.Enter)
            {
                ApplyGridLinesPerPageNumber();
            }

            e.Handled = e.SuppressKeyPress = e.KeyCode != Keys.Delete &&
                e.KeyCode != Keys.Back && e.KeyCode != Keys.Right &&
                e.KeyCode != Keys.Left;
        }

        private void ultraComboEditorLinesPerPage_SelectionChanged(object sender, EventArgs e)
        {
            ApplyGridLinesPerPageNumber();
        }

        #endregion Строки на страницу

        private void DisplayPageNumberLabel(long? rows = null)
        {
            lblPageIndex.Text = string.Format("Страница {0} из {1}", _queryConditions.PageIndex, TotalPages);
        }

        #endregion Пэйджинг

        #region Фоновая загрузка данных

        public void ShowLoadingStatus(long cachedRows)
        {
            pnlLoading.Visible = false;
            UpdateLoadingRowsCount(cachedRows);
        }

        public void HideLoadingStatus()
        {
            pnlLoading.Visible = false;
            DisplayPageNumberLabel();
            ApplySummaries();
        }

        public void UpdateLoadingRowsCount(long count)
        {
            lblCachedRows.Text = string.Format("Загружено {0} записей", count);
            TotalRows = count;
            DisplayPageNumberLabel(count);
            ApplySummaries();
        }

        #endregion

        #region Агрегация

        #region Сортировка

        private void grid_BeforeSortChange(object sender, BeforeSortChangeEventArgs e)
        {
            _customSettings.SortChanged(e.SortedColumns);
            RequestNewData();
        }

        #endregion Сортировка

        #region Фильтрация

        private void grid_BeforeRowFilterChanged(object sender, BeforeRowFilterChangedEventArgs e)
        {
            GetOptionsBuilder(e.NewColumnFilter.Column.Key).OnFilterChanged(e.NewColumnFilter); 

            FilterDatas(e.NewColumnFilter);
        }

        public UltraGridColumn GetColumn(string key)
        {
            return grid.DisplayLayout.Bands[0].Columns[key];
        }

        private bool IsInConditions(FilterConditionsCollection conditions, object value)
        {
            return conditions != null && conditions.Cast<FilterCondition>().Any(condition => Equals(value, condition.CompareValue));
        }

        public void FilterDatas(Infragistics.Win.UltraWinGrid.ColumnFilter e)
        {
            _customSettings.ApplyFilter(e);
            QueryConditions.PageIndex = 1;
            RequestNewData(QueryConditions);
            btnReset.Enabled = true;
            btnFilterReset.Enabled = true;
            grid.DisplayLayout.Bands[0].ColumnFilters[e.Column.Key].FilterConditions.Clear();
            foreach (FilterCondition condition in e.FilterConditions)
            {
                grid.DisplayLayout.Bands[0].ColumnFilters[e.Column.Key].FilterConditions.Add(condition);
            }
        }

        bool shouldCorrectDateFormat = false;
        private void grid_BeforeCustomRowFilterDialog(object sender, BeforeCustomRowFilterDialogEventArgs e)
        {
            e.CustomRowFiltersDialog.Grid.BeforeCellListDropDown += Grid_BeforeCellListDropDown;
            e.CustomRowFiltersDialog.FormClosing += CustomRowFiltersDialog_FormClosing;

            if (e.Column.DataType == typeof(DateTime?))
            {
                shouldCorrectDateFormat = true;
                e.CustomRowFiltersDialog.Grid.DisplayLayout.Bands[0].Columns[1].Format = "dd.MM.yyyy";
            }

            try
            {
                var grid =
                    e.CustomRowFiltersDialog.Controls[5].Controls[0] as UltraGrid;

                grid.DisplayLayout.Bands[0].Columns[0].Header.Caption = "Условие";
                grid.DisplayLayout.Bands[0].Columns[1].Header.Caption = "Значение";
            }
            catch //TODO: Временное решение, подменить текст. Нет времени на анализ всегда ли есть такие колонки соотв. catch нужен чтобы не допустить блокер
            {
            }
        }

        private void grid_BeforeRowFilterDropDown(object sender, BeforeRowFilterDropDownEventArgs e)
        {
            GetOptionsBuilder(e.Column.Key).AutoComplete(e.ValueList); 
        }

        void CustomRowFiltersDialog_FormClosing(object sender, FormClosingEventArgs e)
        {
            shouldCorrectDateFormat = false;
        }

        private void Grid_BeforeCellListDropDown(object sender, CancelableCellEventArgs e)
        {
            var values = (ValueList)e.Cell.ValueListResolved;

            #region Отрубить лишние записи

            var listToRemove = new List<ValueListItem>();
            for (int i = 0; i < values.ValueListItems.Count; i++)
            {
                var val = (ValueListItem)values.ValueListItems.GetItem(i);
                if (val == null) continue;
                if (val.ToString().Contains("["))
                {
                    listToRemove.Add(val);
                }
            }

            if (listToRemove.Any())
            {
                foreach (var elem in listToRemove)
                {
                    values.ValueListItems.Remove(elem);
                }
            }

            #endregion Отрубить лишние записи

            if (shouldCorrectDateFormat && e.Cell.Column.Index == 1)
            {
                CutTimePartDateTime(values);
            }
        }

        private void CutTimePartDateTime(ValueList values)
        {
            #region Отрубить время

            for (int i = 0; i < values.ValueListItems.Count; i++)
            {
                DateTime outDate;

                if (DateTime.TryParse(values.ValueListItems[i].DataValue.ToString(), out outDate))
                {
                    values.ValueListItems[i].DisplayText = outDate.ToString("dd.MM.yyyy");

                    if (values.ValueListItems.All.Count(x => ((ValueListItem)x).DisplayText == values.ValueListItems[i].DisplayText) > 1)
                    {
                        values.ValueListItems.RemoveAt(i);
                        i--;
                    }
                }
            }

            #endregion Отрубить время
        }

        #endregion Фильтрация

        #endregion Агрегация

        #region Видимости столбцов

        private void btnColumnVisibilitySetup_DroppingDown(object sender, System.ComponentModel.CancelEventArgs e)
        {
            columnVisibilityManager.Items.Clear();
            //foreach (var elem in grid.DisplayLayout.Bands[0].Columns)
            foreach (var item in _bindedColumns)
            {
                var elem = (from UltraGridColumn c in grid.DisplayLayout.Bands[0].Columns where c.Key == item select c).FirstOrDefault();
                if (elem == null) continue;
                if (elem.Key == _checkColumName) continue;
                if (_unavailableColumnsKeys.Contains(elem.Key)) continue;

                /*if (!_bindedColumns.Contains(elem.Key))
                    continue;*/

                var currItem = new UltraListViewItem();
                currItem.Key = elem.Key;
                var fullName = String.Empty;
                if (elem.RowLayoutColumnInfo.ParentGroup != null)
                {
                    var parentGroupsName = GetParentGroupsName(elem.RowLayoutColumnInfo.ParentGroup);

                    var lst = parentGroupsName.Split(new[] { "->" }, StringSplitOptions.RemoveEmptyEntries).ToList();
                    lst.Reverse();
                    parentGroupsName = string.Join("->", lst);

                    if (!string.IsNullOrWhiteSpace(parentGroupsName))
                        fullName = parentGroupsName + "->";
                }
                fullName += elem.Header.Caption;

                currItem.Value = fullName;
                currItem.CheckState = elem.Hidden ? CheckState.Unchecked : CheckState.Checked;
                columnVisibilityManager.Items.Add(currItem);
            }
        }

        private string GetParentGroupsName(UltraGridGroup elem, string name = "")
        {
            if (elem == null) return name;

            var currName = String.IsNullOrEmpty(name) ? elem.Header.Caption : name + "->" + elem.Header.Caption;

            if (elem.RowLayoutGroupInfo.ParentGroup == null)
            {
                return currName;
            }

            return GetParentGroupsName(elem.RowLayoutGroupInfo.ParentGroup, currName);
        }

        private void columnVisibilityManager_ItemCheckStateChanged(object sender, Infragistics.Win.UltraWinListView.ItemCheckStateChangedEventArgs e)
        {
            if (e.Item.CheckState == CheckState.Indeterminate)
            {
                return;
            }

            _customSettings
                .SetColumnVisibility(
                    e.Item.Key,
                    e.Item.CheckState == CheckState.Checked);

            BandColumnsSynhronizeHideOrShow(e.Item.Key);

            RefreshResetButtonState();

            _customSettings.Save(true);
        }

        #endregion Видимости столбцов

        # region Состояние

        /// <summary>
        /// Выставляет контрол в режим чтения(колонки выбора записей не обрабатываются)
        /// </summary>
        /// <param name="state">состояние(true - в режим чтения, false - нормальный режим)</param>
        public void SetReadOnlyState(bool state)
        {
            if (_checkColumn != null)
            {
                if (state)
                {
                    _isInReadOnlyState = true;
                    _checkColumn.CellActivation = Activation.NoEdit;
                    _checkColumn.CellClickAction = CellClickAction.RowSelect;
                    _checkColumn.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.Never;
                }
                else
                {
                    _isInReadOnlyState = false;
                    _checkColumn.CellActivation = Activation.AllowEdit;
                    _checkColumn.CellClickAction = CellClickAction.Edit;
                    _checkColumn.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.Always;
                }
            }
        }

        private List<object> _rowSelected = new List<object>();
        public ReadOnlyCollection<object> RowSelected
        {
            get
            {
                return _rowSelected.AsReadOnly();
            }
        }

        private void CleanAllChecks()
        {
            if (!grid.DisplayLayout.Bands[0].Columns.All.Where(x => ((UltraGridColumn)x).Key == _checkColumName).Any()) return;

            foreach (var elem in grid.Rows)
            {
                elem.Cells[_checkColumName].Value = false.ToString();
                _rowSelected.Remove(elem.ListObject);
            }
        }

        public void CheckAll()
        {
            if (!grid.DisplayLayout.Bands[0].Columns.All.Where(x => ((UltraGridColumn)x).Key == _checkColumName).Any()) return;

            foreach (var elem in grid.Rows)
            {
                elem.Cells[_checkColumName].Value = true.ToString();
                _rowSelected.Add(elem.ListObject);
            }
        }

        # endregion

        #region События

        [Category("Luxoft")]
        [Description("Fires when row selection is checked")]
        public event EventHandler OnGridRowsCheckedStateChanged;

        [Category("Luxoft")]
        [Description("Fires when row selection is changed")]
        public event EventHandler SelectRow;

        [Category("Luxoft")]
        [Description("Fires after row selection is changed")]
        public event EventHandler AfterSelectRow;

        [Category("Luxoft")]
        [Description("Fires before load data in grid")]
        public event EventHandler BeforeLoadData;

        [Category("Luxoft")]
        [Description("Fires after load data in grid")]
        public event EventHandler AfterLoadData;

        [Category("Luxoft")]
        [Description("Fires when expand row of child band")]
        public event EventHandler ChildBandBeforeRowExpanded;

        [Category("Luxoft")]
        [Description("Fires when press button ExportExcel")]
        public event EventHandler OnExportExcel;

        [Category("Luxoft")]
        [Description("Fires when press button ExportExcelCancel")]
        public event EventHandler OnExportExcelCancel;

        #region Выбор строки

        //Выделение строки
        private void grid_AfterRowActivate(object sender, EventArgs e)
        {
            if ((this.SelectRow != null) && (grid.ActiveRow != null))
            {
                var row = grid.ActiveRow.ListObject;
                bool ch = _rowSelected.Contains(row);

                SelectedRowEventArgs args = new SelectedRowEventArgs(row, ch);

                this.SelectRow(this, args);
            }
        }

        private void grid_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
        {
            if ((this.AfterSelectRow != null) && (grid.ActiveRow != null))
            {
                var row = grid.ActiveRow.ListObject;
                bool ch = _rowSelected.Contains(row);

                SelectedRowEventArgs args = new SelectedRowEventArgs(row, ch);

                this.AfterSelectRow(this, args);
            }
        }

        //Двойной щелчек на строке
        public Action<object> RowDoubleClicked { get; set; }
        private void grid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (RowDoubleClicked == null) return;
            RowDoubleClicked(e.Row.ListObject);
        }

        public T GetCurrentItem<T>()
        {
            T ret = default(T);

            if (grid.ActiveRow != null)
            {
                ret = (T)grid.ActiveRow.ListObject;
            }
            return ret;
        }

        #endregion Выбор строки

        #region Обновление ячейки

        private void grid_AfterCellUpdate(object sender, CellEventArgs e)
        {
            if (Band_EditMode && Band_GridRegimEdit == GridRegimEdit.Edit)
            {
                CellUpdateAction(e.Cell);
            }
        }

        private void grid_KeyUp(object sender, KeyEventArgs e)
        {
            if (Band_EditMode && Band_GridRegimEdit == GridRegimEdit.Edit)
            {
                var grid = (UltraGrid)sender;
                if (e.KeyCode == Keys.Enter)
                {
                    grid.PerformAction(UltraGridAction.CommitRow);
                }
            }
        }

        #endregion

        #endregion События

        #region Lux свойства грида

        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Description("Show/Hide filters")]
        [Category("Luxoft")]
        public Infragistics.Win.DefaultableBoolean AllowRowFiltering
        {
            get
            {
                return this.grid.DisplayLayout.Override.AllowRowFiltering;
            }
            set
            {
                this.grid.DisplayLayout.Override.AllowRowFiltering = value;
            }
        }

        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Description("Show/Hide the top panel with aggregates")]
        [Category("Luxoft")]
        public bool AggregatePanelVisible
        {
            get
            {
                return pnlRoot.Visible;
            }
            set
            {
                pnlRoot.Visible = value;
            }
        }

        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Description("Manage grid column auto fit")]
        [Category("Luxoft")]
        public AutoFitStyle ColumnsAutoFitStyle
        {
            get
            {
                return grid.DisplayLayout.AutoFitStyle;
            }
            set
            {
                grid.DisplayLayout.AutoFitStyle = value;
            }
        }

        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Description("Show/Hide the pages panel")]
        [Category("Luxoft")]
        public bool PanelPagesVisible
        {
            get
            {
                return ultraPnlPages.Visible;
            }
            set
            {
                ultraPnlPages.Visible = value;
            }
        }

        private bool _allowMultiGrouping = true;
        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Description("Show/Hide the top panel with aggregates")]
        [Category("Luxoft")]
        public bool AllowMultiGrouping
        {
            get
            {
                return _allowMultiGrouping;
            }
            set
            {
                _allowMultiGrouping = value;
            }
        }

        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Description("Show/Hide the loading panel")]
        [Category("Luxoft")]
        public bool PanelLoadingVisible
        {
            get
            {
                return pnlLoading.Visible;
            }
            set
            {
                pnlLoading.Visible = value;
            }
        }

        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Description("Show/Hide footer panel")]
        [Category("Luxoft")]
        public bool FooterVisible
        {
            get
            {
                return pnlGridBottom.Visible;
            }
            set
            {
                pnlGridBottom.Visible = value;
            }
        }

        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Description("Show/Hide ExportExcel button")]
        [Category("Luxoft")]
        public bool ExportExcelVisible
        {
            get
            {
                return btnExportExcel.Visible;
            }
            set
            {
                btnExportExcel.Visible = value;
            }
        }

        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Description("Show/Hide ExportExcelCancel button")]
        [Category("Luxoft")]
        public bool ExportExcelCancelVisible
        {
            get
            {
                return btnExportExcelCancel.Visible;
            }
            set
            {
                btnExportExcelCancel.Visible = value;
            }
        }

        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Description("Show/Hide the loading PanelExportExcelState")]
        [Category("Luxoft")]
        public bool PanelExportExcelStateVisible
        {
            get
            {
                return ultraPanelExportExcelState.Visible;
            }
            set
            {
                ultraPanelExportExcelState.Visible = value;
            }
        }

        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Description("Show/Hide FilterReset button")]
        [Category("Luxoft")]
        public bool FilterResetVisible
        {
            get
            {
                return btnFilterReset.Visible;
            }
            set
            {
                btnFilterReset.Visible = value;
            }
        }


        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Description("Sets grid title")]
        [Category("Luxoft")]
        public string Title
        {
            get
            {
                return lblGridName.Text;
            }
            set
            {
                lblGridName.Value = value;
            }
        }


        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Description("Default Page Size")]
        [Category("Luxoft")]
        public string DefaultPageSize
        {
            get
            {
                string ret = string.Empty;

                if (this.ultraComboEditorLinesPerPage.SelectedItem != null)
                    ret = this.ultraComboEditorLinesPerPage.Text;

                return ret;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                    this.ultraComboEditorLinesPerPage.Text = value;
            }
        }

        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Description("ContextMenuStrip for grid only")]
        [Category("Luxoft")]
        public ContextMenuStrip GridContextMenuStrip
        {
            get
            {
                return _contextMenuStrip;
            }
            set
            {
                _contextMenuStrip = value;
            }
        }

        #endregion Lux свойства грида

        #region Save and Load Settings

        private void btnReset_Click(object sender, EventArgs e)
        {
            UnbindGridEvents();

            var conditionsCount = _queryConditions.Filter.Count + _queryConditions.Sorting.Count;
            _customSettings.Reset();
            _customSettings.Save(true);
            _queryConditions.Filter.Clear();

            btnReset.Enabled = _customSettings.AllowReset();
            btnFilterReset.Enabled = _customSettings.AllowResetFilter();
            BindGridEvents();

            if (conditionsCount > 0)
            {
                UpdateData();
            }
        }

        private void btnFilterReset_Click(object sender, EventArgs e)
        {
            UnbindGridEvents();

            var conditionsCount = _queryConditions.Filter.Count;

            _customSettings.ResetFilter();
            _customSettings.Save(true);
            _queryConditions.Filter.Clear();

            btnReset.Enabled = _customSettings.AllowReset();
            btnFilterReset.Enabled = _customSettings.AllowResetFilter();
            BindGridEvents();

            if (conditionsCount > 0)
            {
                UpdateData();
            }
        }

        private void grid_AfterRowFilterChanged(object sender, AfterRowFilterChangedEventArgs e)
        {
            RefreshResetButtonState();

            if (AllowSaveFilterSettings)
                _customSettings.Save(true);
        }

        private void grid_AfterSortChange(object sender, BandEventArgs e)
        {
            RefreshResetButtonState();
            _customSettings.Save(true);
        }

        private void RefreshResetButtonState()
        {
            btnReset.Enabled = _customSettings.AllowReset();
            btnFilterReset.Enabled = _customSettings.AllowResetFilter();
        }

        private void BindGridEvents()
        {
            grid.BeforeSortChange += new BeforeSortChangeEventHandler(grid_BeforeSortChange);
            grid.BeforeRowFilterChanged += new BeforeRowFilterChangedEventHandler(grid_BeforeRowFilterChanged);
            grid.AfterRowFilterChanged += new AfterRowFilterChangedEventHandler(grid_AfterRowFilterChanged);
            grid.AfterSortChange += new BandEventHandler(grid_AfterSortChange);
        }

        private void UnbindGridEvents()
        {
            grid.BeforeSortChange -= new BeforeSortChangeEventHandler(grid_BeforeSortChange);
            grid.BeforeRowFilterChanged -= new BeforeRowFilterChangedEventHandler(grid_BeforeRowFilterChanged);
            grid.AfterRowFilterChanged -= new AfterRowFilterChangedEventHandler(grid_AfterRowFilterChanged);
            grid.AfterSortChange -= new BandEventHandler(grid_AfterSortChange);
        }

        #endregion Save and Load Settings

        # region Check Column

        private string _checkColumName = "CheckColumn";
        private bool _isUserCheck = false;
        private bool _isCheckByCode = false;
        private UltraGridColumn _checkColumn = null;
        private CheckState _lastColumnSetValue = CheckState.Unchecked;
        private Func<object, bool> _checkColumnActivityCriteria = (obj) => true;
        private int _columnPosition = 0;

        private void SetColumnCheckRole(
            UltraGridColumn col,
            HeaderCheckBoxSynchronization checkColumnSyncMode =
                HeaderCheckBoxSynchronization.None)
        {
            col.Key = _checkColumName;
            col.Header.Caption = String.Empty;
            col.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.Always;
            col.Header.CheckBoxSynchronization = checkColumnSyncMode;
            col.DataType = typeof(bool);
            col.CellActivation = Activation.AllowEdit;
            col.CellClickAction = CellClickAction.Edit;
            col.AllowRowSummaries = AllowRowSummaries.False;
            col.AllowGroupBy = DefaultableBoolean.False;
            col.AllowRowFiltering = DefaultableBoolean.False;
            _checkColumn = col;
        }

        private void grid_BeforeHeaderCheckStateChanged(object sender, BeforeHeaderCheckStateChangedEventArgs e)
        {
            _isUserCheck = _lastColumnSetValue != e.NewCheckState && e.IsCancelable;

            if (_isUserCheck)
            {
                _lastColumnSetValue = e.NewCheckState; //Нельзя канселять ивент после этой строчки
            }
        }

        //Чек на хедере столбца
        private void grid_AfterHeaderCheckStateChanged(object sender, AfterHeaderCheckStateChangedEventArgs e)
        {
            var state = e.Column.GetHeaderCheckedState(e.Rows);

            if (!_isUserCheck || _isCheckByCode)
            {
                e.Column.SetHeaderCheckedState(e.Rows, _lastColumnSetValue); // Это магия. Если вызвать этот метод тут, то события Before и After не вызываются и строки грида не синхронизируются, чего нам в этом случае и надо
                return;
            }

            Action<UltraGridRow> action = null;
            switch (state)
            {
                case CheckState.Checked:
                    action = row =>
                    {
                        row.Cells[_checkColumName].Value = true.ToString();
                        _rowSelected.Add(row.ListObject);
                    };
                    break;
                case CheckState.Unchecked:
                    action = row => row.Cells[_checkColumName].Value = false.ToString();
                    break;
                default:
                    return;
            }

            _rowSelected.Clear();
            foreach (var elem in grid.Rows)
            {
                action(elem);
            }
            RiseGridRowsCheckedStateChanged(e);
        }

        private void RiseGridRowsCheckedStateChanged(EventArgs e)
        {
            var handler = OnGridRowsCheckedStateChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        //Чек на ячейке
        private void grid_CellChange(object sender, CellEventArgs e)
        {
            if (e.Cell.Column.Key == _checkColumName)
            {
                if (!Convert.ToBoolean(e.Cell.Text))
                {
                    _rowSelected.Remove(e.Cell.Row.ListObject);
                }
                else if (!_rowSelected.Contains(e.Cell.Row.ListObject))
                {
                    _rowSelected.Add(e.Cell.Row.ListObject);
                }
                // DN Общее решение: если не все галочки выставлены, то и в заголовке галочки нет
                _isCheckByCode = true;
                _checkColumn.SetHeaderCheckedState(grid.Rows, _rowSelected.Count == grid.Rows.Count);
                _isCheckByCode = false;
                RiseGridRowsCheckedStateChanged(e);
            }
        }

        public void SetHeaderCheckState(CheckState state)
        {
            _lastColumnSetValue = state;
            _checkColumn.SetHeaderCheckedState(grid.Rows, _lastColumnSetValue);
        }

        public void SetHeaderCheckEnabled(bool enable)
        {
            _checkColumn.Header.CheckBoxVisibility =
                enable && !_isInReadOnlyState
                ? HeaderCheckBoxVisibility.Always
                : HeaderCheckBoxVisibility.Never;
        }

        public void SetCheckColumnVisibility(bool isVisible)
        {
            if (_checkColumn != null)
            {
                _checkColumn.Hidden = !isVisible;
                if (_checkColumn.Group != null)
                    _checkColumn.Group.Hidden = !isVisible;
            }
        }

        public void SetCheckColumnActivityCriteria(Func<object, bool> criteria)
        {
            _checkColumnActivityCriteria = criteria;
        }

        # endregion

        # region Increment Column

        private UltraGridColumn BuildIncrementColumn(
            ColumnDefinition definition,
            UltraGridGroup group = null)
        {
            var result =
                grid.DisplayLayout.Bands[0].Columns.Add(
                    definition.Key,
                    definition.Caption);
            result.RowLayoutColumnInfo.SpanY = definition.RowSpan;
            SetupIncrementColumn(result);

            if (group != null)
            {
                result.RowLayoutColumnInfo.ParentGroup = group;
                result.Group = group;
            }

            return result;
        }

        private void SetupIncrementColumn(UltraGridColumn column)
        {
            column.SortIndicator = SortIndicator.Disabled;
            column.AllowGroupBy = DefaultableBoolean.False;
            column.AllowRowFiltering = DefaultableBoolean.False;
            column.AllowRowSummaries = AllowRowSummaries.False;
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;

            grid.InitializeRow +=
                (sender, args) =>
                {
                    FilllIncrementColumn(column.Key, args.Row);
                };
        }

        private void FilllIncrementColumn(string columnKey, UltraGridRow row)
        {
            var rowNumber =
                (int)(QueryConditions.PageIndex - 1) * QueryConditions.PageSize
                + row.Index + 1;

            row.Cells[columnKey].Value = rowNumber;
        }

        # endregion

        # region Обновление

        public void Refresh()
        {
            grid.Refresh();
        }

        public void RefreshRow(int dataIndex)
        {
            foreach (var row in grid.Rows)
            {
                if (row.ListIndex == dataIndex)
                {
                    row.Refresh();
                }
            }
        }

        public void ProgressUpdate()
        {
            if (this.ProgressGridData.Value >= this.ProgressGridData.Maximum)
            {
                this.ProgressGridData.Value = this.ProgressGridData.Minimum;
            }

            this.ProgressGridData.Value++;
        }

        # endregion

        # region Костыль для поддержки фильтра по СУР и чек-колонке

        private readonly Dictionary<string, IColumnFilterManager> _builders =
            new Dictionary<string, IColumnFilterManager>();

        public void AppendOptionsBuilder(string columnKey, IColumnFilterManager builder)
        {
            _builders.Add(columnKey, builder);
        }

        private IColumnFilterManager GetOptionsBuilder(string columnKey)
        {
            if (_builders.ContainsKey(columnKey))
            {
                return _builders[columnKey];
            }

            return new DefaultColumnFilterManager();
        }

        # endregion

        public bool AllowSaveFilterSettings { get; set; }

        # region Экспорт в эксель

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            if (OnExportExcel != null)
            {
                OnExportExcel(sender, e);
            }
        }

        private void btnExportExcelCancel_Click(object sender, EventArgs e)
        {
            if (OnExportExcelCancel != null)
            {
                OnExportExcelCancel(sender, e);
            }
        }

        public void SetExportExcelState(string stateText)
        {
            labelExportExcelState.Text = stateText;
        }

        public void SetExportExcelEnabled(bool isEnabled)
        {
            btnExportExcel.Enabled = isEnabled;
        }

        public void SetExportExcelCancelEnabled(bool isEnabled)
        {
            btnExportExcelCancel.Enabled = isEnabled;
        }

        public void SetPanelExportExcelStateWidth(int width)
        {
            ultraPanelExportExcelState.Width = width;
        }

        # endregion

        # region Выдача сообщения в рабочей области таблицы

        private UltraLabel lblMessage = null;

        public void ShowMessageInWorkplace(string msg)
        {
            if (grid == null) return;
            if (grid.DisplayLayout.Bands.Count == 0) return;
            
            HideMessageInWorkplace();

            lblMessage = new UltraLabel()
            {
                Text = msg,
                Visible = true,
                Name = "lblMessage",
                AutoSize = true,
                UseAppStyling = false,
                UseOsThemes = DefaultableBoolean.False
            };

            SetMsgLabelParameters();
            lblMessage.BackColor = grid.DisplayLayout.Appearance.BackColor;
            lblMessage.Appearance.BackColor = grid.DisplayLayout.Appearance.BackColor;
            lblMessage.Appearance.BackColor2 = grid.DisplayLayout.Appearance.BackColor2;

            this.Controls.Add(lblMessage);

            Font f = new Font(lblMessage.Font, FontStyle.Regular);
            lblMessage.Font = f;

            lblMessage.BringToFront();
            lblMessage.Invalidate();

            grid.AfterRowLayoutItemResized += RowResizeProcessing;
            grid.Resize += ResizeProcessing;
        }

        public void HideMessageInWorkplace()
        {
            if (lblMessage != null)
            {
                grid.AfterRowLayoutItemResized -= RowResizeProcessing;
                grid.Resize -= ResizeProcessing;
                this.Controls.Remove(lblMessage);
                lblMessage = null;
            }
        }

        private void RowResizeProcessing(object sender, AfterRowLayoutItemResizedEventArgs e)
        {
            if (lblMessage != null)
            {
                if (!(e.RowLayoutItem is UltraGridColumn))
                {
                    SetMsgLabelParameters();
                    lblMessage.Invalidate();
                }
            }
        }

        private void ResizeProcessing(object sender, System.EventArgs e)
        {
            SetMsgLabelParameters();
        }


        private void SetMsgLabelParameters()
        {
            if (lblMessage != null)
            {
                int startX = grid.Location.X + grid.Margin.Left;
                int startY = grid.Location.Y + grid.Margin.Top + grid.DisplayLayout.Bands[0].TotalHeaderHeight;

                lblMessage.Location = new Point(startX, startY);
                
                lblMessage.Visible = (lblMessage.Location.Y + lblMessage.Height) < (grid.Location.Y + grid.Height);
            }
        }


        # endregion

    }
}