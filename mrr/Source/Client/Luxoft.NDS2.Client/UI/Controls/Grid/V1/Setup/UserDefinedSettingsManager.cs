﻿using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.Extensions;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Helpers;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Providers;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using Nullable = Infragistics.Win.UltraWinGrid.Nullable;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup
{
    public delegate void SettingsChanged(bool allowRestore, bool allowSave);
    /// <summary>
    /// Этот класс представляет набор настроек отображения данных в списке, сделанных пользователем, а так же реализует операции с ними.
    /// </summary>
    public class UserDefinedSettingsManager
    {
        # region Константы

        private const string SettingsKey = "column_visibility";
        private const bool UserDefinedTrue = true;

        # endregion

        # region Конструктор

        private UltraGridBand _band;
        private GridSetup _setup;
        private SettingsWrapper<SettingsContainer> _lastSavedSettings;

        /// <summary>
        /// Создает экземпляр класса UserDefinedSettingsManager
        /// </summary>
        /// <param name="band">Ссылка на Band таблицы, к которой применяются настройки</param>
        /// <param name="gridSetup">Ссылка на набор настроект таблицы</param>
        public UserDefinedSettingsManager(
            UltraGridBand band,
            GridSetup setup)
        {
            _band = band;
            _setup = setup;

            if (setup.QueryCondition != null)
                _defaultSort = setup.QueryCondition.Sorting.Select(sort => (ColumnSort)sort.Clone()).ToList();

            _lastSavedSettings = new SettingsWrapper<SettingsContainer>(
                SettingsKey,
                _setup.SettingsProvider);
        }

        # endregion

        # region Видимость колонок

        public void SetColumnVisibility(string key, bool visible)
        {
            var column = Column(key);

            if (visible)
            {
                Show(column);
            }
            else
            {
                Hide(column);
            }
        }

        private void Hide(UltraGridColumn column)
        {
            column.Hidden = true;
            Hide(Parent(column));
        }

        private bool CanHideGroup(UltraGridGroup group)
        {
            if (group == null)
                return false;
            foreach (var column in _band.Columns)
            {
                if (column.Hidden)
                    continue;
                var parent = Parent(column);
                while (parent != null)
                {
                    if (Equals(parent, group))
                        return false;
                    parent = Parent(parent);
                }
            }
            return true;
        }

        public void Hide(UltraGridGroup group)
        {
            if (!CanHideGroup(group))
                return;

            group.Hidden = true;
            Hide(Parent(group));
        }

        private void Show(UltraGridColumn column)
        {
            column.Hidden = false;
            Show(Parent(column));
        }

        private void Show(UltraGridGroup group)
        {
            if (group == null)
            {
                return;
            }

            group.Hidden = false;
            Show(Parent(group));
        }

        private List<string> BuildColumnsVisibility()
        {
            return
                _band
                    .Columns
                    .All
                    .Where(
                        col =>
                            ((UltraGridColumn)col).Hidden
                            && Definition(((UltraGridColumn)col).Key, _setup.Columns) != null)
                    .Select(col => ((UltraGridColumn)col).Key)
                    .ToList();
        }

        private bool RestoreColumnVisibility()
        {
            if (_lastSavedSettings.Status == SettingLoadState.Loaded)
            {
                foreach (var columnKey in _lastSavedSettings.Data.HiddenColumns)
                {
                    Hide(Column(columnKey));
                }

                return true;
            }

            return false;
        }

        private void ResetColumnsVisibility()
        {
            foreach (var item in _band.Columns.All)
            {
                var column = (UltraGridColumn)item;
                var definition = Definition(column.Key, _setup.Columns);

                if (definition != null)
                {
                    column.Hidden = definition.Visibility == ColumnVisibility.Hidden || definition.Visibility == ColumnVisibility.Disabled;
                }
                if (column.Group != null)
                {
                    column.Group.Hidden = column.Group.Columns.All(p => p.Hidden);
                }
            }
        }

        private bool AllowResetColumnVisibility()
        {
            var allowReset = false;

            foreach (var item in _band.Columns.All)
            {
                var column = (UltraGridColumn)item;
                var definition = Definition(column.Key, _setup.Columns);

                if (definition != null)
                {
                    allowReset =
                        allowReset
                        || (
                            (!column.Hidden && definition.Visibility ==
                                ColumnVisibility.Hidden)
                            || (column.Hidden && definition.Visibility ==
                                ColumnVisibility.Visible));
                }
            }

            return allowReset;
        }

        private bool AllowSaveColumnVisibility()
        {
            if (_lastSavedSettings.Status == SettingLoadState.FailedToLoad)
            {
                return AllowResetColumnVisibility();
            }

            var isChanged = false;

            foreach (var item in _band.Columns.All)
            {
                var column = (UltraGridColumn)item;

                if (Definition(column.Key, _setup.Columns) != null)
                {
                    isChanged =
                        isChanged
                        || (column.Hidden != _lastSavedSettings.Data.HiddenColumns.Contains(column.Key));
                }
            }

            return isChanged;
        }

        # endregion

        # region Фильтрация данных

        public void ApplyFilter(Infragistics.Win.UltraWinGrid.ColumnFilter e)
        {
            if (e.FilterConditions.All.Any())
            {
                ApplyUserFilter(e);
            }
            else
            {
                _setup.QueryCondition.Filter.RemoveAll(x => x.ColumnName == e.Column.Key);
            }
        }

        private bool AllowSaveFilter()
        {
            if (_lastSavedSettings.Status == SettingLoadState.FailedToLoad)
            {
                return AllowResetFilter();
            }

            return !Compare(_lastSavedSettings.Data.ColumnFilters);
        }

        public bool AllowResetFilter()
        {
            return !Compare(_setup.AllConditions());
        }

        private void ApplyUserFilter(Infragistics.Win.UltraWinGrid.ColumnFilter gridFilter)
        {
            if (_setup.QueryCondition == null)
                _setup.QueryCondition = new QueryConditions();

            _setup.QueryCondition.Filter.RemoveAll(
                x => x.ColumnName == gridFilter.Column.Key
                && x.UserDefined);

            var currFilterOperator = 
                gridFilter.LogicalOperator == FilterLogicalOperator.And 
                ? FilterQuery.FilterLogicalOperator.And 
                : FilterQuery.FilterLogicalOperator.Or;

            var filterQuery =
                new FilterQuery(UserDefinedTrue)
                {
                    FilterOperator = currFilterOperator,
                    ColumnName = gridFilter.Column.Key,
                    ColumnType = gridFilter.Column.DataType
                };          

            _setup.QueryCondition.Filter.Add(filterQuery);

            foreach (var elem in gridFilter.FilterConditions.All)
            {
                var currFilterItem = (FilterCondition)elem;
                if (String.IsNullOrEmpty(currFilterItem.CompareValue == null ? String.Empty : currFilterItem.CompareValue.ToString()))
                {
                    currFilterItem.CompareValue = DBNull.Value;
                }

                //--- Преобразование к необходимому типу
                if (currFilterItem.CompareValue != null)
                {
                    if (currFilterItem.CompareValue.GetType() == typeof(string))
                    {
                        if (gridFilter.Column.DataType == typeof(DateTime) ||
                            gridFilter.Column.DataType == typeof(DateTime?))
                        {
                            DateTime? valueDT = TransformValueDT(currFilterItem.CompareValue);
                            if (valueDT != null)
                            {
                                currFilterItem.CompareValue = valueDT;
                            }
                        }
                    }
                }
                //-----------

                var currFilterName = currFilterItem.Column.Key;
                var currFilterComparison = currFilterItem.ComparisionOperator;
                var currFilterValue = currFilterItem.CompareValue;

                var currFilter = new Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter();
                currFilter.ComparisonOperator = currFilterItem.ComparisionOperator == FilterComparisionOperator.Contains ? Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.Contains :
                        currFilterItem.ComparisionOperator == FilterComparisionOperator.DoesNotContain ? Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.DoesNotContain :
                        currFilterItem.ComparisionOperator == FilterComparisionOperator.DoesNotEndWith ? Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.DoesNotEndWith :
                        currFilterItem.ComparisionOperator == FilterComparisionOperator.DoesNotMatch ? Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.DoesNotMatch :
                        currFilterItem.ComparisionOperator == FilterComparisionOperator.DoesNotStartWith ? Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.DoesNotStartWith :
                        currFilterItem.ComparisionOperator == FilterComparisionOperator.EndsWith ? Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.EndsWith :
                        currFilterItem.ComparisionOperator == FilterComparisionOperator.Equals ? Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.Equals :
                        currFilterItem.ComparisionOperator == FilterComparisionOperator.GreaterThan ? Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.GreaterThan :
                        currFilterItem.ComparisionOperator == FilterComparisionOperator.GreaterThanOrEqualTo ? Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.GreaterThanOrEqualTo :
                        currFilterItem.ComparisionOperator == FilterComparisionOperator.LessThan ? Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.LessThan :
                        currFilterItem.ComparisionOperator == FilterComparisionOperator.LessThanOrEqualTo ? Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.LessThanOrEqualTo :
                        currFilterItem.ComparisionOperator == FilterComparisionOperator.Like ? Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.Like :
                        currFilterItem.ComparisionOperator == FilterComparisionOperator.Match ? Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.Match :
                        currFilterItem.ComparisionOperator == FilterComparisionOperator.NotEquals ? Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.NotEquals :
                        currFilterItem.ComparisionOperator == FilterComparisionOperator.NotLike ? Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.NotLike :
                        currFilterItem.ComparisionOperator == FilterComparisionOperator.StartsWith ? Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.StartsWith :
                        Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.NotDefinedOperator;
                currFilter.Value = currFilterItem.CompareValue;

                filterQuery.Filtering.Add(currFilter);
            }
        }

        private DateTime? TransformValueDT(object value)
        {
            string valueString = (string)value;
            DateTime? valueDT = null;
            DateTime dtTemp;
            if (DateTime.TryParse(valueString, out dtTemp))
            {
                valueDT = dtTemp;
            }
            else
            {
                List<string> words = valueString.Split('.').ToList();
                int day = ParseWord(words, 0);
                int month = ParseWord(words, 1);
                int year = ParseWord(words, 2);
                valueDT = new DateTime(year, month, day);
            }
            return valueDT;
        }

        private int ParseWord(List<string> words, int index)
        {
            int ret = 0;
            if (words.Count > 0)
            {
                int temp = 0;
                if (int.TryParse(words[0], out temp))
                {
                    ret = temp;
                }
            }
            return ret;
        }

        private List<GridColumnFilter> BuildFilter()
        {
            var newFilter = new List<GridColumnFilter>();

            foreach (var itemI in _band.ColumnFilters.All)
            {
                var filter = (Infragistics.Win.UltraWinGrid.ColumnFilter)itemI;

                if (filter.FilterConditions.Count > 0)
                {
                    var columnFilter =
                        new GridColumnFilter()
                        {
                            ColumnKey = filter.Column.Key,
                            Conditions = new List<FilterCondition>(),
                            LogicalOperator = filter.LogicalOperator
                        };
                    newFilter.Add(columnFilter);
                    foreach (var itemJ in filter.FilterConditions.All)
                    {
                        var condition = (FilterCondition)itemJ;
                        columnFilter.Conditions.Add(condition);
                    }
                }
            }

            return newFilter;
        }

        private bool RestoreFilter()
        {
            if (_lastSavedSettings.Status == SettingLoadState.Loaded)
            {
                _band.ColumnFilters.ClearAllFilters();
                foreach (var columnFilter in _lastSavedSettings.Data.ColumnFilters)
                {
                    var fc = _band.ColumnFilters[columnFilter.ColumnKey];
                    fc.LogicalOperator = columnFilter.LogicalOperator;
                    foreach (var condition in columnFilter.Conditions)
                    {
                        fc.FilterConditions.Add(condition);
                    }
                    if (fc.FilterConditions.Count > 0)
                    {
                        ApplyUserFilter(fc);
                    }
                }


                return true;
            }

            return false;
        }

        public void ResetFilter()
        {
            _band.ColumnFilters.ClearAllFilters();

            if (_setup.HasAnyFilter())
            {
                foreach (var column in _band.Columns)
                {
                    var filter = _band.ColumnFilters[column.Key];
                    filter.ClearFilterConditions();
                    filter.LogicalOperator = FilterLogicalOperator.Or;

                    foreach (var condition in _setup.ColumnFilter(column.Key))
                    {
                        filter
                            .FilterConditions
                            .Add(condition);
                    }

                    if (filter.FilterConditions.Count > 0)
                    {
                        ApplyUserFilter(filter);
                    }
                }
            }
        }

        # endregion

        # region Сортировка данных

        private List<ColumnSort> _defaultSort;

        public void SortChanged(SortedColumnsCollection sortedColumns)
        {
            _setup.QueryCondition.Sorting =
                BuildSorting(sortedColumns.All.Select(x => (UltraGridColumn)x));
        }

        private void ResetSorting()
        {
            ApplySorting(_defaultSort);
        }

        private List<ColumnSort> BuildSorting(IEnumerable<UltraGridColumn> columns)
        {
            return
                    columns
                    .Where(
                        col =>
                            col.SortIndicator == SortIndicator.Ascending
                            || col.SortIndicator == SortIndicator.Descending)
                    .Select(
                        col =>
                        {
                            var gridCol = (UltraGridColumn)col;
                            return
                                new ColumnSort()
                                {
                                    ColumnKey = gridCol.Key,
                                    Order =
                                        gridCol.SortIndicator == SortIndicator.Ascending
                                        ? ColumnSort.SortOrder.Asc
                                        : ColumnSort.SortOrder.Desc,
                                    NullAsZero = gridCol.Nullable == Nullable.Null
                                };
                        })
                    .ToList();
        }

        private void RestoreSorting()
        {
            ApplySorting(_lastSavedSettings.Data.Sorting);
        }

        private void ApplySorting(List<ColumnSort> sorting)
        {
            if (sorting != null)
            {
                _setup.QueryCondition.Sorting.Clear();
                sorting.ForEach(sort => _setup.QueryCondition.Sorting.Add((ColumnSort)sort.Clone()));

                foreach (var item in _band.Columns.All)
                {
                    var column = (UltraGridColumn)item;

                    if (column.SortIndicator != SortIndicator.Disabled)
                    {
                        column.SortIndicator =
                            sorting.Any(sort => sort.ColumnKey.ToUpper() == column.Key.ToUpper())
                            ? sorting.First(sort => sort.ColumnKey.ToUpper() == column.Key.ToUpper()).Order.ToSortIndicator()
                            : SortIndicator.None;
                    }
                }
            }
        }

        private bool CompareSort(List<ColumnSort> compare)
        {
            if (compare == null)
                return false;

            var currentSortList = BuildSorting(_band.Columns.All.Select(x => (UltraGridColumn)x));

            if (currentSortList.Count != compare.Count)
            {
                return true;
            }

            var currentSort = currentSortList.ToArray();
            var compareSort = compare.ToArray();

            for (var counter = 0; counter < currentSort.Length; counter++)
            {
                if (currentSort[counter].ColumnKey.ToUpper() != compareSort[counter].ColumnKey.ToUpper()
                    || currentSort[counter].Order != compareSort[counter].Order)
                {
                    return true;
                }
            }

            return false;
        }

        private bool AllowResetSort()
        {
            return CompareSort(_defaultSort);
        }

        private bool AllowSaveSort()
        {
            return CompareSort(_lastSavedSettings.Data.Sorting);
        }

        # endregion

        # region Сравнение с настройками по умолчанию

        public bool AllowReset()
        {
            return AllowResetColumnVisibility()
                || AllowResetFilter()
                || AllowResetSort();
        }

        public bool AllowSave()
        {
            return (AllowSaveColumnVisibility()
                    || AllowSaveFilter()
                    || AllowSaveSort())
                && _setup.AllowSaveSettings;
        }

        # endregion

        # region Операции

        public bool Save(bool isSilent = false)
        {
            if (!_setup.AllowSaveSettings)
            {
                return false;
            }

            var newSettings = new SettingsContainer();

            newSettings.ColumnFilters = _setup.SettingsSaveOptions.AllowSaveFilters ?
                BuildFilter() :
                new List<GridColumnFilter>();

            newSettings.Sorting = _setup.SettingsSaveOptions.AllowSaveSortings
                ? BuildSorting(_band.Columns.All.Select(x => (UltraGridColumn) x))
                : new List<ColumnSort>();

            newSettings.HiddenColumns = BuildColumnsVisibility();

            if (!_lastSavedSettings.Save(newSettings))
                return false;

            if (!isSilent)
                _setup.SettingsProvider.NotifySuccess();
            return true;
        }

        public bool Restore()
        {
            _lastSavedSettings.Restore();

            if (_lastSavedSettings.Status != SettingLoadState.FailedToLoad)
            {
                try
                {
                    if (_setup.SettingsSaveOptions.AllowSaveFilters)
                    {
                        RestoreFilter();                        
                    }

                    if (_setup.SettingsSaveOptions.AllowSaveSortings)
                    {
                        RestoreSorting();
                    }

                    RestoreColumnVisibility();

                    return true;
                }
                catch
                {
                    return false;
                }
            }

            return false;
        }

        public void Reset()
        {
            ResetColumnsVisibility();
            ResetFilter();
            ResetSorting();
        }

        # endregion

        # region Вспомогательные методы

        private UltraGridColumn Column(string columnKey)
        {
            return _band.Columns[columnKey];
        }

        private UltraGridGroup Parent(UltraGridColumn column)
        {
            return column.RowLayoutColumnInfo.ParentGroup;
        }

        private UltraGridGroup Parent(string columnKey)
        {
            return Parent(Column(columnKey));
        }

        private UltraGridGroup Parent(UltraGridGroup group)
        {
            return group.RowLayoutGroupInfo.ParentGroup;
        }

        private ColumnDefinition Definition(string columnKey, ColumnGroupDefinition group)
        {
            var searchResult = Definition(columnKey, group.Columns);

            if (searchResult != null)
            {
                return searchResult;
            }

            foreach (var sg in group.SubGroups)
            {
                searchResult = Definition(columnKey, sg);

                if (searchResult != null)
                {
                    return searchResult;
                }
            }
            return null;
        }
        
        private ColumnDefinition Definition(string columnKey, IEnumerable<ColumnBase> all)
        {
            foreach (var definition in all)
            {
                var column = definition as ColumnDefinition;

                if (column != null && column.Key == columnKey)
                {
                    return column;
                }

                var group = definition as ColumnGroupDefinition;
                if (group != null)
                {
                    var searchResult = Definition(columnKey, group);
                    if (searchResult != null)
                    {
                        return searchResult;
                    }
                }
            }

            return null;
        }

        private bool Compare(List<GridColumnFilter> settings)
        {
            bool ret = true;
            int index = 0;

            while ((index < _band.Columns.All.Length) && ret)
            {
                var item = _band.Columns.All[index];
                var column = (UltraGridColumn) item;
                var definition = Definition(column.Key, _setup.Columns);

                if (definition != null)
                {
                    var currentConditionQuantity =
                        _band.ColumnFilters[column.Key].FilterConditions.Count;
                    var compareConditionQuantity =
                        settings.Any(x => x.ColumnKey == column.Key)
                            ? settings.First(x => x.ColumnKey == column.Key).Conditions.Count
                            : 0;
                    var queryConditionQuantity =
                        _setup.QueryCondition.Filter.Any(x => x.ColumnName == column.Key)
                            ? _setup.QueryCondition.Filter.First(x => x.ColumnName == column.Key).Filtering.Count
                            : 0;


                    if ((definition.Visibility == ColumnVisibility.Disabled) && (queryConditionQuantity > 0))
                    {
                        ret = false;
                    } 
                    else if (currentConditionQuantity != compareConditionQuantity)
                    {
                        ret = false;
                    }
                    else if (currentConditionQuantity > 0)
                    {
                        var arrayToCompare = settings.First(x => x.ColumnKey == column.Key).Conditions.ToArray();
                        int counter = 0;
                        
                        while ((counter < currentConditionQuantity) && ret)
                        {
                            var compare = arrayToCompare[counter];
                            var current =
                                (FilterCondition) _band
                                                      .ColumnFilters[column.Key]
                                                      .FilterConditions
                                                      .All[counter];

                            if (current.CompareValue.ToString() != compare.CompareValue.ToString()
                                || current.ComparisionOperator != compare.ComparisionOperator)
                            {
                                ret = false;
                            }

                            counter++;
                        }
                    }
                }

                index++;
            }

            return ret;
        }

        # endregion
    }

    public enum SettingLoadState { NotLoaded, Loaded, FailedToLoad }

    public class SettingsWrapper<T> where T : new()
    {
        # region Fields

        private T _settings = new T();
        private string _key;
        private ISettingsProvider _settingsProvider;

        # endregion

        # region Constructor

        public SettingsWrapper(
            string key,
            ISettingsProvider settingsProvider)
        {
            _key = key;
            _settingsProvider = settingsProvider;
            Status = SettingLoadState.NotLoaded;
        }

        # endregion

        # region Properties

        public SettingLoadState Status
        {
            get;
            private set;
        }

        public T Data
        {
            get
            {
                if (Status == SettingLoadState.NotLoaded)
                {
                    Restore();
                }

                return _settings;
            }
        }

        # endregion

        # region Methods

        public bool Save(T columns)
        {
            try
            {
                _settingsProvider.SaveSettings(columns.Serialize(), _key);
                _settings = columns;
                Status = SettingLoadState.Loaded;

                return true;
            }
            catch
            {
                return false;
            }
        }

        public void Restore()
        {
            Action setFail = () =>
            {
                _settings = new T();
                Status = SettingLoadState.FailedToLoad;
            };
            try
            {
                if (_settingsProvider != null)
                {
                    var settings = _settingsProvider.LoadSettings(_key).Deserialize<T>();
                    if (ReferenceEquals(settings, default(T)))
                    {
                        setFail();
                    }
                    else
                    {
                        _settings = settings;
                        Status = SettingLoadState.Loaded;
                    }                   
                }
                else
                {
                    setFail();
                }
            }
            catch
            {
                setFail();
            }
        }

        # endregion
    }

    [Serializable]
    public class SettingsContainer
    {
        public List<GridColumnFilter> ColumnFilters
        {
            get;
            set;
        }

        public List<string> HiddenColumns
        {
            get;
            set;
        }

        public List<ColumnSort> Sorting
        {
            get;
            set;
        }
    }
}
