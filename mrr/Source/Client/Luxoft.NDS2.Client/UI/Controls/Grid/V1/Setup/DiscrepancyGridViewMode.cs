﻿using System;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup
{
    public enum DiscrepancyGridViewMode
    {
        Normal,
        Edit
    }
}