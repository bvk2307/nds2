﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup
{
    public interface IGroupingGeneralInfo
    {
        List<ColumnDefinition> GetAllGroupingColumns();
    }
}
