﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Infragistics.Shared;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.EventArguments;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using UltraWinGrid = Infragistics.Win.UltraWinGrid;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V1
{
    public partial class DiscrepancyGridControl : UserControl
    {
        private readonly int _actualHeaderHeight;
        private bool _headerVisible;
        private DiscrepancyGridSetup _setup;
        private DiscrepancyGridViewMode _viewMode;

        public bool HeaderVisible
        {
            get { return _headerVisible; }
            set { SetHeaderVisible(_headerVisible = value); }
        }

        public DiscrepancyGridViewMode ViewMode
        {
            get { return _viewMode; }
            set
            {
                _viewMode = value;
                ApplyMode();
            }
        }

        public AutoFitStyle AutoFitStyle
        {
            get { return grid.DisplayLayout.AutoFitStyle; }
            set { grid.DisplayLayout.AutoFitStyle = value; }
        }

        public DiscrepancyGridSetup Setup
        {
            get { return _setup; }
            set
            {
                _setup = value;
                ApplySetup();
            }
        }

        public object DataSource
        {
            get { return gridBindingSource.DataSource; }
            set
            {
                gridBindingSource.DataSource = value;
                ApplySetup();
                UpdateCells();
            }
        }

        public DiscrepancyGridControl()
        {
            InitializeComponent();
            InitGrid();
            _setup = null;
            _viewMode = DiscrepancyGridViewMode.Normal;
            _actualHeaderHeight = upHeader.Height;
            DataSource = null;
            _headerVisible = true;
            grid.MouseEnterElement += grid_MouseEnterElement;

            var rc = Infragistics.Win.UltraWinGrid.Resources.Customizer;
            rc.SetCustomizedString("FilterDialogOkButtonNoFiltersText", "Без фильтров");
            rc.SetCustomizedString("RowFilterDialogDBNullItem", "Введите значение");
        }

        private void InitGrid()
        {
            grid.DisplayLayout.Override.AllowGroupSwapping = AllowGroupSwapping.NotAllowed;
            grid.DisplayLayout.Override.AllowGroupMoving = AllowGroupMoving.NotAllowed;
            grid.DisplayLayout.Override.WrapHeaderText = DefaultableBoolean.True;
            grid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.False;
            grid.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.False;
            grid.DisplayLayout.Override.FixedRowSortOrder = FixedRowSortOrder.FixOrder;
            grid.DisplayLayout.Override.AllowColMoving = AllowColMoving.NotAllowed;
            grid.DisplayLayout.Override.AllowColSwapping = AllowColSwapping.NotAllowed;
            grid.DisplayLayout.Override.AllowAddNew = AllowAddNew.No;
            grid.DisplayLayout.Override.AllowDelete = DefaultableBoolean.False;
            grid.DisplayLayout.Override.AllowUpdate = DefaultableBoolean.False;
            grid.DisplayLayout.Override.RowSizing = RowSizing.Fixed;
            grid.DisplayLayout.Override.SelectTypeCell = SelectType.Single;
            grid.DisplayLayout.Override.SelectTypeCol = SelectType.None;
            grid.DisplayLayout.Override.SelectTypeRow = SelectType.None;
            grid.DisplayLayout.Override.CellClickAction = CellClickAction.RowSelect;
            grid.DisplayLayout.Override.ColumnSizingArea = ColumnSizingArea.EntireColumn;
            grid.DisplayLayout.Override.ColumnAutoSizeMode = ColumnAutoSizeMode.AllRowsInBand;
            grid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
            grid.ClickCell += grid_ClickCell;
        }

        #region UI
        private void ApplyMode()
        {
            switch (_viewMode)
            {
                case DiscrepancyGridViewMode.Normal:
                    grid.DisplayLayout.Override.AllowUpdate = DefaultableBoolean.False;
                    break;
                case DiscrepancyGridViewMode.Edit:
                    grid.DisplayLayout.Override.AllowUpdate = DefaultableBoolean.True;
                    break;
            }
        }

        private void SetHeaderVisible(bool visible)
        {
            upHeader.Height = visible ? _actualHeaderHeight : 0;
        }

        private void ApplySetup()
        {
            if (Setup == null || DataSource == null)
            {
                return;
            }
            foreach (var band in grid.DisplayLayout.Bands)
            {
                SetupBand(band);
            }
            foreach (var row in grid.DisplayLayout.Rows)
            {
                EnsureRowExpandable(row);
            }
        }
        #endregion
        #region Настройка элементов
        private void ResetSetup(UltraGridBand ugBand)
        {
            ugBand.Groups.Clear();
            foreach (var ugColumn in ugBand.Columns)
            {
                ugColumn.Tag = null;
            }
            if (ugBand.Tag != null)
            {
                ugBand.ParentBand.SubObjectPropChanged -= (SubObjectPropChangeEventHandler)ugBand.Tag;
            }
        }

        private void SetupBand(UltraGridBand ugBand)
        {
            var sBand = Setup.GetBandByName(ugBand.Key);
            if (sBand == null)
            {
                ugBand.Hidden = true;
                return;
            }
            ugBand.RowLayoutStyle = RowLayoutStyle.GroupLayout;
            ugBand.ColHeadersVisible = sBand.ColumnsHeadersVisible;
            ugBand.HeaderVisible = !string.IsNullOrWhiteSpace(sBand.Caption);
            ugBand.ColHeaderLines = 2;
            if (ugBand.HeaderVisible)
            {
                ugBand.Header.Caption = sBand.Caption;
                var appearance = ugBand.Header.Appearance;
                appearance.BackColor = SystemColors.Window;
                appearance.BorderAlpha = Alpha.Transparent;
                appearance.ThemedElementAlpha = Alpha.Transparent;
                appearance.TextHAlign = HAlign.Left;
                appearance.TextVAlign = VAlign.Middle;
            }

            ResetSetup(ugBand);
            foreach (var sGroup in sBand.Groups)
            {
                AppendGroup(ugBand, sGroup);
            }
            for (int index = 0; index < sBand.Columns.Count; index++)
            {
                SetupColumn(ugBand, sBand.Columns[index], index);
            }
            HideUnmappedColumns(ugBand);
            if (sBand.BindWidth)
            {
                ugBand.Indentation = 0;
                ugBand.Override.AllowColSizing = AllowColSizing.Synchronized;
                BindWidth(ugBand);
                
            }
            if (_viewMode == DiscrepancyGridViewMode.Edit)
            {
                grid.DisplayLayout.PerformAutoResizeColumns(false, PerformAutoSizeType.AllRowsInBand);
            }

            _cellToolTips = _setup.CellToolTips;
            _сellsIsHyperLinks = _setup.CellIsHyperLinks;
        }

        private void AppendGroup(UltraGridBand ugBand, ColumnGroupDefinition sGroup, UltraGridGroup ugParentGroup = null)
        {
            var ugGroup = ugBand.Groups.Add(sGroup.Key, sGroup.Caption);
            ugGroup.RowLayoutGroupInfo.LabelSpan = sGroup.RowSpan;
            ugGroup.RowLayoutGroupInfo.MinimumLabelSize = new Size(50, 40);
            var appearance = ugGroup.Header.Appearance;
            appearance.TextTrimming = TextTrimming.None;
            appearance.TextHAlign = HAlign.Center;
            appearance.TextVAlign = VAlign.Top;
            

            ugGroup.Header.ToolTipText = sGroup.ToolTip;
            if (ugGroup.Header.ToolTipText == null)
            {
                ugGroup.Header.ToolTipText = String.Empty;
            }
            ugGroup.RowLayoutGroupInfo.ParentGroup = ugParentGroup;
            foreach (var sSubGroup in sGroup.SubGroups)
            {
                AppendGroup(ugBand, sSubGroup, ugGroup);
            }
            for (int index = 0; index < sGroup.Columns.Count; index++)
            {
                SetupColumn(ugBand, sGroup.Columns[index], index, ugGroup);
            }
        }

        private void SetupColumn(UltraGridBand ugBand, ColumnDefinition sColumn, int visiblePosition, UltraGridGroup ugParentGroup = null)
        {
            var ugColumn = ugBand.Columns.Cast<UltraGridColumn>().FirstOrDefault(column => column.Key == sColumn.Key);
            if (ugColumn == null)
            {
                return;
            }
            ugColumn.CellClickAction = CellClickAction.RowSelect;
            ugColumn.AllowRowSummaries = AllowRowSummaries.False;
            ugColumn.AllowRowFiltering = DefaultableBoolean.False;
            ugColumn.SortIndicator = SortIndicator.Disabled;
            ugColumn.Format = sColumn.Format;
            ugColumn.RowLayoutColumnInfo.ParentGroup = ugParentGroup;
            ugColumn.Hidden = (sColumn.Visibility == ColumnVisibility.Hidden) || (sColumn.Visibility == ColumnVisibility.Disabled);
            ugColumn.CellAppearance.TextHAlign = (HAlign)Enum.Parse(typeof(HAlign), sColumn.Align.ToString());
            ugColumn.Header.Appearance.TextHAlign = HAlign.Center;
            ugColumn.Header.ToolTipText = sColumn.ToolTip;
            if (ugColumn.Header.ToolTipText == null)
            {
                ugColumn.Header.ToolTipText = String.Empty;
            }
            ugColumn.Header.VisiblePosition = visiblePosition;
            ugColumn.Header.Caption = sColumn.Caption;
            //ugColumn.PerformAutoResize();

            switch (_viewMode)
            {
                case DiscrepancyGridViewMode.Normal:
                    ugColumn.CellActivation = Activation.NoEdit;
                    break;
                case DiscrepancyGridViewMode.Edit:
                    if (sColumn.EditableDefinition.Editable)
                    {
                        ugColumn.CellActivation = Activation.AllowEdit;
                        ugColumn.CellClickAction = CellClickAction.Edit;
                        break;
                    }
                    ugColumn.CellActivation = Activation.NoEdit;
                    ugColumn.CellAppearance.BackColor = Color.LightGray;
                    break;
            }

            if (sColumn.Width.HasValue)
            {
                ugColumn.MinWidth = sColumn.Width.Value;
            }
            if (sColumn.IsHyperLink)
            {
                ugColumn.CellAppearance.Cursor = Cursors.Hand;
                ugColumn.CellAppearance.FontData.Underline = DefaultableBoolean.True;
                ugColumn.CellAppearance.ForeColor = Color.Blue;
            }
            if (sColumn.IsHtml)
            {
                ugColumn.Style = UltraWinGrid.ColumnStyle.FormattedText;
            }

            ugColumn.MinWidth = 50;
            ugColumn.PerformAutoResize(Infragistics.Win.UltraWinGrid.PerformAutoSizeType.AllRowsInBand);
            ugColumn.RowLayoutColumnInfo.PreferredLabelSize = new Size(100, 40);

            ugColumn.Tag = new object();
        }

        private void HideUnmappedColumns(UltraGridBand ugBand)
        {
            var unmappedColumns =
                from ugColumn in ugBand.Columns.Cast<UltraGridColumn>()
                where !ugColumn.Hidden && ugColumn.Tag == null
                select ugColumn;
            foreach (var ugColumn in unmappedColumns)
            {
                ugColumn.Hidden = true;
            }
        }

        private void EnsureRowExpandable(UltraGridRow parent)
        {
            if (_viewMode != DiscrepancyGridViewMode.Edit && parent.ChildBands != null)
            {
                var rows = parent.ChildBands.Cast<UltraGridChildBand>().SelectMany(band => band.Rows).ToList();
                if (rows.Count > 0 
                    && !rows[0].ListObject.GetType().IsPrimitive) //TODO: этим костылем снимаем "плюс" для ошибок ЛК.
                {
                    parent.ExpansionIndicator = ShowExpansionIndicator.Always;
                    rows.ForEach(row => EnsureRowExpandable(row));
                    return;
                }
            }
            parent.ExpansionIndicator = ShowExpansionIndicator.Never;
        }

        private void BindWidth(UltraGridBand ugBand)
        {
            var ugParentBand = ugBand.ParentBand;
            if (ugParentBand == null)
            {
                return;
            }
            SubObjectPropChangeEventHandler handler = propChange => SyncWidthHandler(ugBand, propChange);
            ugBand.Tag = handler;
            ugParentBand.Columns.SubObjectPropChanged += handler;
            SyncWidthInternal(ugBand);
        }
        #endregion
        #region Видимости столбцов

        //private void btnColumnVisibilitySetup_DroppingDown(object sender, System.ComponentModel.CancelEventArgs e)
        //{
        //    columnVisibilityManager.Items.Clear();
        //    foreach (var elem in grid.DisplayLayout.Bands[0].Columns)
        //    {
        //        if (elem == null) continue;
        //        if (elem.Key == _checkColumName) continue;
        //        if (_unavailableColumnsKeys.Contains(elem.Key)) continue;

        //        UltraListViewItem currItem = new UltraListViewItem();
        //        currItem.Key = elem.Key;
        //        var fullName = String.Empty;
        //        if (elem.RowLayoutColumnInfo.ParentGroup != null)
        //        {
        //            var parentGroupsName = GetParentGroupsName(elem.RowLayoutColumnInfo.ParentGroup);

        //            var lst = parentGroupsName.Split(new string[] { "->" }, StringSplitOptions.RemoveEmptyEntries).ToList();
        //            lst.Reverse();
        //            parentGroupsName = string.Join("->", lst);

        //            fullName = parentGroupsName + "->";
        //        }
        //        fullName += elem.Header.Caption;

        //        currItem.Value = fullName;
        //        currItem.CheckState = elem.Hidden == true ? CheckState.Unchecked : CheckState.Checked;
        //        columnVisibilityManager.Items.Add(currItem);
        //    }
        //}

        //private string GetParentGroupsName(UltraGridGroup elem, string name = "")
        //{
        //    if (elem == null) return name;

        //    var currName = String.IsNullOrEmpty(name) ? elem.Header.Caption : name + "->" + elem.Header.Caption;

        //    if (elem.RowLayoutGroupInfo.ParentGroup == null)
        //    {
        //        return currName;
        //    }

        //    return GetParentGroupsName(elem.RowLayoutGroupInfo.ParentGroup, currName);
        //}

        //private void columnVisibilityManager_ItemCheckStateChanged(object sender, Infragistics.Win.UltraWinListView.ItemCheckStateChangedEventArgs e)
        //{
        //    if (e.Item.CheckState == CheckState.Indeterminate) return;

        //    grid.DisplayLayout.Bands[0].Columns[e.Item.Key].Hidden = e.Item.CheckState == CheckState.Unchecked;

        //    if (e.Item.CheckState == CheckState.Unchecked)
        //    {
        //        var groups = grid.DisplayLayout.Bands[0].Groups;
        //        HideParentGroups(grid.DisplayLayout.Bands[0].Columns[e.Item.Key].RowLayoutColumnInfo.ParentGroup);
        //    }
        //    else if (e.Item.CheckState == CheckState.Checked)
        //    {
        //        ShowParentGroups(grid.DisplayLayout.Bands[0].Columns[e.Item.Key].RowLayoutColumnInfo.ParentGroup);
        //    }
        //}

        //private void HideParentGroups(UltraGridGroup currGroup)
        //{
        //    if (currGroup == null) return;

        //    var notHidedChildrensColumns = currGroup.Columns.All.Where(x => ((UltraGridColumn)x).Hidden == false);
        //    if (notHidedChildrensColumns.Any())
        //    {
        //        return;
        //    }

        //    var notHidedChildrensGroups = grid.DisplayLayout.Bands[0].Groups.All.Where(x => ((UltraGridGroup)x).RowLayoutGroupInfo.ParentGroup == currGroup).Where(x => ((UltraGridGroup)x).Hidden == false && ((UltraGridGroup)x) != currGroup);
        //    if (notHidedChildrensGroups.Any())
        //    {
        //        return;
        //    }

        //    currGroup.Hidden = true;

        //    var parentGroup = currGroup.RowLayoutGroupInfo.ParentGroup;
        //    if (parentGroup == null)
        //    {
        //        return;
        //    }

        //    var notHidedBrothersColumns = parentGroup.Columns;
        //    if (notHidedBrothersColumns.Any())
        //    {
        //        return;
        //    }

        //    currGroup.Hidden = true;
        //    var notHidedBrothersGroups = grid.DisplayLayout.Bands[0].Groups.All.Where(x => ((UltraGridGroup)x).RowLayoutGroupInfo.ParentGroup == parentGroup).Where(x => ((UltraGridGroup)x).Hidden == false && ((UltraGridGroup)x) != currGroup);
        //    if (notHidedBrothersGroups.Any())
        //    {
        //        return;
        //    }

        //    HideParentGroups(parentGroup);
        //}

        //private void ShowParentGroups(UltraGridGroup currGroup)
        //{
        //    if (currGroup == null) return;

        //    currGroup.Hidden = false;

        //    if (currGroup.RowLayoutGroupInfo == null) return;
        //    if (currGroup.RowLayoutGroupInfo.ParentGroup == null) return;

        //    ShowParentGroups(currGroup.RowLayoutGroupInfo.ParentGroup);
        //}

        #endregion Видимости столбцов
        #region События
        [Category("Luxoft")]
        [Description("Fires when row selection is changed")]
        public event EventHandler<SelectedRowEventArgs> SelectRow;

        [Category("Luxoft")]
        [Description("Fires when cell clicked")]
        public event EventHandler<ClickedCellEventArgs> CellClick;

        #region Выбор строки
        //Выделение строки
        private void grid_AfterRowActivate(object sender, EventArgs e)
        {
            if ((this.SelectRow != null) && (grid.ActiveRow != null))
            {
                var row = grid.ActiveRow.ListObject;
                var args = new SelectedRowEventArgs(row, false);
                this.SelectRow(this, args);
            }
        }

        //Двойной щелчек на строке
        public Action<object> RowDoubleClicked { get; set; }
        private void grid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (RowDoubleClicked == null) return;
            RowDoubleClicked(e.Row.ListObject);
        }

        private void grid_ClickCell(object sender, ClickCellEventArgs e)
        {
            var handler = CellClick;
            var cell = e.Cell;
            var column = cell.Column;
            var data = cell.Row.ListObject;

            if (handler != null && column.CellAppearance.Cursor == Cursors.Hand && cell.Value != null &&
                !string.IsNullOrWhiteSpace(cell.Value.ToString()) && cell.Appearance.ForeColor != SystemColors.ControlText)
            {
                handler(this, new ClickedCellEventArgs { Instance = data, DataMember = column.Key });
            }
        }

        void grid_MouseEnterElement(object sender, UIElementEventArgs e)
        {
            if (e.Element.Cursor == Cursors.Hand)
            {
                var control = ((UltraGrid)e.Element.ControlElement.UltraControl).GetChildAtPoint(e.Element.ControlElement.CurrentMousePosition);
                bool b = false;
                b = !b;
            }
            //throw new NotImplementedException();
        }
        #endregion Выбор строки

        private void SyncWidthHandler(UltraGridBand ugBand, PropChangeInfo propChange)
        {
            if (propChange.PropId is UltraWinGrid.PropertyIds
               && ((UltraWinGrid.PropertyIds)propChange.PropId) == UltraWinGrid.PropertyIds.Columns)
            {
                SyncWidthInternal(ugBand);
            }
        }

        private void SyncWidthInternal(UltraGridBand ugBand)
        {
            Func<ColumnsCollection, IEnumerable<UltraGridColumn>> getVisibleColumns = columns =>
                from column in columns.Cast<UltraGridColumn>()
                where !column.Hidden
                orderby column.Header.VisiblePosition
                select column;

            foreach (var nestedColumn in getVisibleColumns(ugBand.Columns))
            {
                var parentColumn = ugBand.ParentBand.Columns.All.Cast<UltraGridColumn>().FirstOrDefault(c => c.Key == nestedColumn.Key);
                if (parentColumn != null && !parentColumn.Hidden)
                {
                    nestedColumn.RowLayoutColumnInfo.PreferredCellSize = parentColumn.RowLayoutColumnInfo.PreferredCellSize;
                    nestedColumn.Width = parentColumn.Width;
                }
            }
        }
        #endregion События
        #region Операции
        public void ResetCellBackColor(object listObject, string dataMember)
        {
            ResetCellBackColor(listObject, new[] { dataMember });
        }

        public void SetCellBackColor(object listObject, string dataMember, Color color, string tooltip = null)
        {
            SetCellBackColor(listObject, new[] { dataMember }, color, tooltip);
        }

        public void SetCellBackColor(object listObject, IEnumerable<string> dataMembers, Color color, string tooltip = null)
        {
            CellAppearanceAction(listObject, dataMembers, app => app.BackColor = color, tooltip);
        }

        public void ResetCellBackColor(object listObject, IEnumerable<string> dataMembers)
        {
            CellAppearanceAction(listObject, dataMembers, app => app.ResetBackColor());
        }

        public void RemoveEmptyLinks()
        {
            RemoveEmptyLinks(grid.DisplayLayout.Bands[0], grid.DisplayLayout.Rows);
        }

        private void CellAppearanceAction(object listObject, IEnumerable<string> dataMembers, Action<AppearanceBase> action, string tooltip = null)
        {
            var row = FindRow(grid.DisplayLayout.Rows, listObject);
            if (row == null)
            {
                return;
            }
            foreach (var dataMember in dataMembers)
            {
                var cell = row.Cells[dataMember];
                action(cell.Appearance);
                cell.ToolTipText = tooltip;
            }
        }

        private UltraGridRow FindRow(RowsCollection rows, object listObject)
        {
            foreach (var row in rows)
            {
                if (object.ReferenceEquals(row.ListObject, listObject))
                {
                    return row;
                }
                if (row.ChildBands != null)
                {
                    foreach (var band in row.ChildBands.Cast<UltraGridChildBand>())
                    {
                        var result = FindRow(band.Rows, listObject);
                        if (result != null)
                        {
                            return result;
                        }
                    }
                }
            }
            return null;
        }

        private void RemoveEmptyLinks(UltraGridBand band, RowsCollection rows)
        {
            var links =
                from column in band.Columns.Cast<UltraGridColumn>()
                where column.CellAppearance.Cursor == Cursors.Hand
                select column;

            foreach (var row in rows)
            {
                var instance = row.ListObject;
                var type = instance.GetType();
                foreach (var column in links)
                {
                    var property = type.GetProperty(column.Key);
                    var val = property.GetValue(instance, null);
                    if (val == null || string.IsNullOrEmpty(val.ToString()))
                    {
                        var cellAppearance = row.Cells[column].Appearance;
                        ResetHyperlink(cellAppearance);
                    }
                }
                if (row.ChildBands != null)
                {
                    foreach (var nested in row.ChildBands.Cast<UltraGridChildBand>())
                    {
                        RemoveEmptyLinks(nested.Band, nested.Rows);
                    }
                }
            }
        }

        private void ResetHyperlink(Infragistics.Win.Appearance cellAppearance)
        {
            cellAppearance.Cursor = Cursors.Default;
            cellAppearance.FontData.Underline = DefaultableBoolean.False;
            cellAppearance.ForeColor = SystemColors.ControlText;
        }

        public void RemoveHyperlink(object listItem, string columnKey)
        {
            RemoveHyperlink(grid.DisplayLayout.Rows, listItem, columnKey);
        }

        private void RemoveHyperlink(RowsCollection rows, object listItem, string columnKey)
        {
            foreach (var row in rows)
            {
                if (row.ListObject.Equals(listItem))
                {
                    ResetHyperlink(row.Cells[columnKey].Appearance);
                }
                if (row.ChildBands != null)
                {
                    foreach (var nested in row.ChildBands.Cast<UltraGridChildBand>())
                    {
                        RemoveHyperlink(nested.Rows, listItem, columnKey);
                    }
                }
            }
        }

        private Dictionary<string, Func<object, string>> _cellToolTips = null;
        private Dictionary<string, Func<object, bool>> _сellsIsHyperLinks = null;

        private void UpdateCells()
        {
            UpdateCells(grid.DisplayLayout.Bands[0], grid.DisplayLayout.Rows);
        }

        private void UpdateCells(UltraGridBand band, RowsCollection rows)
        {
            for (int i = -1; ++i < rows.Count; )
            {
                UltraGridRow row = rows[i];
                row.ExpansionIndicator = ShowExpansionIndicator.CheckOnDisplay;
                UpdateCellsToolTip(row);
                UpdateCellsIsHyperLink(row);
                if (row.ChildBands != null)
                {
                    foreach (var nested in row.ChildBands.Cast<UltraGridChildBand>())
                    {
                        UpdateCells(nested.Band, nested.Rows);
                    }
                }
            }
        }

        private void UpdateCellsToolTip(UltraGridRow row)
        {
            if (_cellToolTips != null)
            {
                foreach (KeyValuePair<string, Func<object, string>> item in _cellToolTips)
                {
                    if (row.Cells.Exists(item.Key))
                    {
                        row.Cells[item.Key].ToolTipText = item.Value.Invoke(row.ListObject);
                    }
                }
            }
        }

        private void UpdateCellsIsHyperLink(UltraGridRow row)
        {
            if (_сellsIsHyperLinks != null)
            {
                foreach (KeyValuePair<string, Func<object, bool>> item in _сellsIsHyperLinks)
                {
                    if (row.Cells.Exists(item.Key))
                    {
                        UltraGridCell cell = row.Cells[item.Key];
                        if (item.Value.Invoke(row.ListObject))
                        {
                            cell.Appearance.Cursor = Cursors.Hand;
                            cell.Appearance.FontData.Underline = DefaultableBoolean.True;
                            cell.Appearance.ForeColor = System.Drawing.Color.Blue;
                        }
                        else
                        {
                            cell.Appearance.Cursor = Cursors.Default;
                            //cell.Appearance.FontData.Underline = DefaultableBoolean.Default;
                            //cell.Appearance.ResetForeColor();
                            cell.Appearance.FontData.Underline = DefaultableBoolean.False;
                            cell.Appearance.ForeColor = System.Drawing.Color.Black;
                        }
                    }
                }
            }
        }

        #endregion

        private void grid_BeforeCustomRowFilterDialog(object sender, BeforeCustomRowFilterDialogEventArgs e)
        {
            try
            {
                var grid =
                    e.CustomRowFiltersDialog.Controls[5].Controls[0] as UltraGrid;

                grid.DisplayLayout.Bands[0].Columns[0].Header.Caption = "Условие";
                grid.DisplayLayout.Bands[0].Columns[1].Header.Caption = "Значение";
            }
            catch //TODO: Временное решение, подменить текст. Нет времени на анализ всегда ли есть такие колонки соотв. catch нужен чтобы не допустить блокер
            {
            }
        }
    }
}