﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.ComponentModel;
using System.Text;
using Luxoft.NDS2.Common.Contracts.CustomAttributes;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup
{
    public class GridSetupHelper<T> where T : class
    {
        public ColumnDefinition CreateColumnDefinition<TProperty>(Expression<Func<T, TProperty>> propertyExpresion)
        {
            return CreateColumnDefinition<TProperty>(propertyExpresion, null, null, null);
        }

        public ColumnDefinition CreateColumnDefinition<TProperty>(Expression<Func<T, TProperty>> propertyExpresion, Action<ColumnDefinition> initializer)
        {
            return CreateColumnDefinition<TProperty>(propertyExpresion, null, null, initializer);
        }

        public ColumnDefinition CreateColumnDefinition<TProperty>(Expression<Func<T, TProperty>> propertyExpresion, string caption, Action<ColumnDefinition> initializer = null)
        {
            return CreateColumnDefinition<TProperty>(propertyExpresion, caption, null, initializer);
        }

        public string GetMemberName<TProperty>(Expression<Func<T, TProperty>> propertyExpresion)
        {
            return ((MemberExpression)propertyExpresion.Body).Member.Name;
        }

        public ColumnDefinition CreateColumnDefinition<TProperty>(
            Expression<Func<T, TProperty>> propertyExpresion, string caption, string toolTipText, Action<ColumnDefinition> initializer = null)
        {
            string columnName = GetMemberName(propertyExpresion);

            return CreateColumnDefinition(propertyExpresion, columnName, caption, toolTipText, initializer);
        }

        public ColumnDefinition CreateColumnDefinition<TProperty>(Expression<Func<T, TProperty>> propertyExpresion, string columnName,
            string caption, string toolTipText, Action<ColumnDefinition> initializer)
        {
            var column = new ColumnDefinition(columnName);
            column.Caption = column.Key;

            foreach (var attr in GetColumnAttributes(propertyExpresion))
            {
                if (attr.GetType().IsAssignableFrom(typeof(DisplayNameAttribute)))
                {
                    column.Caption = ((DisplayNameAttribute)attr).DisplayName;
                }

                if (attr.GetType().IsAssignableFrom(typeof(DescriptionAttribute)))
                {
                    column.ToolTip = ((DescriptionAttribute)attr).Description;
                }

                if (attr.GetType().IsAssignableFrom(typeof(CurrencyFormatAttribute)))
                {
                    column.Format = ((FormatAttribute)attr).Format;
                    column.Align = ColumnAlign.Right;
                }
            }

            #warning Нужен рефакторинг и удаление этого метода
            if (caption != null)
            {
                column.Caption = caption;
            }
            if (toolTipText != null)
            {
                column.ToolTip = toolTipText;
            }

            if (initializer != null)
            {
                initializer(column);
            }

            return column;
        }

        public object[] GetColumnAttributes<TProperty>( Expression<Func<T, TProperty>> propertyExpresion )
        {
            MemberExpression memberBody = (MemberExpression)propertyExpresion.Body;
            object[] attrs = memberBody.Member.GetCustomAttributes( false );

            return attrs;
        }

        public ColumnDefinition CreateHyperLinkColumn<TProperty>(
            Expression<Func<T, TProperty>> propertyExpression, 
            Action<object, EventArgs> clickHandler)
        {
            var columnDef = CreateColumnDefinition(propertyExpression);
            columnDef.IsHyperLink = true;
            columnDef.CellClick = clickHandler;

            return columnDef;
        }

        public ColumnDefinition CreateHyperLinkColumn<TProperty>(
            Expression<Func<T, TProperty>> propertyExpression,
            Action<ColumnDefinition> initializer,
            Action<object, EventArgs> clickHandler)
        {
            var columnDef = CreateColumnDefinition(propertyExpression, initializer);
            columnDef.IsHyperLink = true;
            columnDef.CellClick = clickHandler;

            return columnDef;
        }

        public ColumnDefinition CreateHyperLinkOnlyNeedCellColumn<TProperty>(
            Expression<Func<T, TProperty>> propertyExpression,
            Action<object, EventArgs> clickHandler)
        {
            var columnDef = CreateColumnDefinition(propertyExpression);
            columnDef.CellClick = clickHandler;
            return columnDef;
        }

        public ColumnDefinition CreateHyperLinkOnlyNeedCellColumn<TProperty>(
            Expression<Func<T, TProperty>> propertyExpression,
            Action<ColumnDefinition> initializer,
            Action<object, EventArgs> clickHandler)
        {
            var columnDef = CreateColumnDefinition(propertyExpression, initializer);
            columnDef.CellClick = clickHandler;
            return columnDef;
        }
    }
}