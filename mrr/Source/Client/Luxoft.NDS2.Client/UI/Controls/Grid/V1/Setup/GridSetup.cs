﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using DocumentFormat.OpenXml.Drawing.Charts;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Providers;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup
{
    public delegate object DataRequestHandler<in T, T1>(T input, out T1 output);

    /// <summary>
    /// Этот класс инкапсулирует настройки для таблиц (списков).
    /// </summary>
    public class GridSetup
    {
        public SaveStateOptions SettingsSaveOptions { get; private set; }
        

        # region Конструктор

        /// <summary>
        /// Создает экземпляр класса GridSetup
        /// </summary>
        /// <param name="settingsProvider">Провайдер сохранения/загрузки параметров конфигурации</param>
        public GridSetup(ISettingsProvider settingsProvider)
        {
            SettingsProvider = settingsProvider;
            Columns = new List<ColumnBase>();
            Bands = new List<BandDefinition>();
            QueryCondition = new QueryConditions();
            RowActions = new List<Action<UltraGridRow>>();
            _allowSaveSettings = settingsProvider != null;
            SettingsSaveOptions = new SaveStateOptions(){AllowSaveFilters = true, AllowSaveSortings = true};
        }

        # endregion

        # region Данные таблицы

        /// <summary>
        /// Вызывается для получения данных
        /// </summary>
        public DataRequestHandler<QueryConditions, long> GetData;

        /// <summary>
        /// Вызывается при запросе общего количества строк таблицы
        /// </summary>
        public Func<QueryConditions, int> GetRowsCountDelegate { get; set; }

        /// <summary>
        /// Возвращает или задает условия фильтрации/сортировки данных
        /// </summary>
        public QueryConditions QueryCondition { get; set; }

        # endregion

        # region Параметры колонок таблицы

        public List<ColumnBase> Columns
        {
            get;
            set;
        }

        public List<BandDefinition> Bands { get; set; }

        # endregion

        # region Провайдеры

        /// <summary>
        /// Возвращает или задает ссылку калькулятора промежуточных итогов
        /// </summary>
        public ICustomSummaryCalculator SummaryRowCalculator
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает провайдер сохранения/загрузки настроек
        /// </summary>
        public ISettingsProvider SettingsProvider
        {
            get;
            private set;
        }

        # endregion

        # region Настройки доступности операций

        private bool _allowSaveSettings;
        /// <summary>
        /// Возвращает признак возможности сохранения настроек таблицы
        /// </summary>
        public bool AllowSaveSettings
        {
            get
            {
                return _allowSaveSettings;
            }
            set
            {
                if (SettingsProvider == null && value)
                {
                    throw new NotSupportedException(
                        "Провайдер сохранения настроек списка не задан. Включение сохранения настроек не поддерживается.");
                }

                _allowSaveSettings = value;
            }
        }

        # endregion

        # region Фильтр по умолчанию

        private List<GridColumnFilter> _defaultFilter =
            new List<GridColumnFilter>();

        public IEnumerable<FilterCondition> ColumnFilter(string columnKey)
        {
            if (_defaultFilter.Any(filter => filter.ColumnKey == columnKey))
            {
                return _defaultFilter.First(filter => filter.ColumnKey == columnKey).Conditions;
            }

            return new List<FilterCondition>();
        }

        public List<GridColumnFilter> AllConditions()
        {
            return _defaultFilter;
        }

        public bool HasAnyFilter()
        {
            return _defaultFilter.Any();
        }

        public void ApplyFilter(string columnKey, FilterCondition condition)
        {
            if (_defaultFilter.All(filter => filter.ColumnKey != columnKey))
            {
                _defaultFilter.Add(
                    new GridColumnFilter()
                    {
                        ColumnKey = columnKey,
                        Conditions = new List<FilterCondition>() { condition }
                    });
            }
            else
            {
                _defaultFilter
                    .First(filter => filter.ColumnKey == columnKey)
                    .Conditions.Add(condition);
            }
        }

        # endregion

        # region Настройки для конкретных ячеек строки

        public Dictionary<string, Func<object, string>> CellToolTips { get; set; }

        public Dictionary<string, string> BandCellToolTips { get; set; }

        public Dictionary<string, Func<object, bool>> CellIsHyperLinks { get; set; }

        # endregion

        public List<Action<UltraGridRow>> RowActions { get; set; }

        public Action<UltraGridCell> CellUpdateAction { get; set; }

        public ColumnBase FindColumn(string key)
        {
            return FindColumn(key, Columns);
        }

        public void OrderColumns(int shift = 2)
        {
            Columns.Aggregate(0, (current, item) => OrderItem(item, current, shift));
        }

        private int OrderItem(ColumnBase item, int origin, int shift)
        {
            var column = item as ColumnDefinition;
            if (column != null && column.Visibility != ColumnVisibility.Disabled)
            {
                if (column.OriginX < 0)
                    column.OriginX = origin;
                return origin + shift;
            }
            var group = item as ColumnGroupDefinition;
            if (group == null)
                return origin;
            if (group.OriginX < 0)
                group.OriginX = origin;
            var subOrigin = @group.HasSubGroups ? group.SubGroups.Aggregate(0, (current, subGroup) => OrderItem(subGroup, current, shift)) : 0;
            subOrigin = @group.Columns.Aggregate(subOrigin, (current, subColumns) => OrderItem(subColumns, current, shift));
            return origin + subOrigin + shift;
        }

        private ColumnBase FindColumn(string key, IEnumerable<ColumnBase> columns)
        {
            foreach (var column in columns)
            {
                if (column.Key == key)
                {
                    return column;
                }

                var group = column as ColumnGroupDefinition;

                if (group != null)
                {
                    var findResult = FindColumn(key, group.Columns);

                    if (findResult != null)
                    {
                        return findResult;
                    }
                }
            }

            return null;
        }

        private TipStyle _typeStyleCell = TipStyle.Default;
        public TipStyle TypeStyleCell
        {
            get { return _typeStyleCell; }
            set { _typeStyleCell = value; }
        }


        public class SaveStateOptions
        {
            public bool AllowSaveFilters { get; set; }

            public bool AllowSaveSortings { get; set; }
        }
    }
}