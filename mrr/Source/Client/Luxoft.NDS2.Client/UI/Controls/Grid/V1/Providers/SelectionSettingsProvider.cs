﻿using CommonComponents.Uc.Infrastructure.Interface.Services;
using Luxoft.NDS2.Client.Helpers;
using Microsoft.Practices.CompositeUI;
using System;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V1.Providers
{
    public class SelectionSettingsProvider : CommonSettingsProvider
    {
        private readonly string _key;
        private readonly Func<long> _getSelectionId;

        public SelectionSettingsProvider(WorkItem workItem, string key, Func<long> getSelectionId) : base(workItem, key)
        {
            _key = key;
            _getSelectionId = getSelectionId;
        }

        public override void SaveSettings(string valueSetup, string settingsKey = "settings")
        {
            var manager = _workItem.Services.Get<IUcOptionsService>(true);
            manager.Save(_key, settingsKey, valueSetup);
            manager.Save(string.Format("{0}_{1}", _key, _getSelectionId), settingsKey, valueSetup);
        }

        public override string LoadSettings(string settingsKey = "settings")
        {
            return _getSelectionId() > 0
                ? _workItem.Services.Get<IUcOptionsService>(true).Load(string.Format("{0}_{1}", _key, _getSelectionId), settingsKey)
                : _workItem.Services.Get<IUcOptionsService>(true).Load(_key, settingsKey);
        }
    }
}
