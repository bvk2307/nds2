﻿using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V1.Comparers
{
    public class SimpleElementsSortComparer : BaseComparer, IComparer
    {
        public SimpleElementsSortComparer(SortKind kind)
        {
            Kind = kind;
        }
        
        private object GetPropValue(object src, string propName)
        {
            return src.GetType().GetProperty(propName).GetValue(src, null);
        }

        int IComparer.Compare(object x, object y)
        {
            string xVal = String.Empty;
            string yVal = String.Empty;
            if ((x == null) || (x == DBNull.Value))
            {
                x = String.Empty;
            }
            else
            {
                if (((UltraGridCell)x).Value == null)
                {
                    x = String.Empty;
                }
                else
                {
                    xVal = ((UltraGridCell)x).Value.ToString();
                }
            }
            if ((y == null) || (y == DBNull.Value))
            {
                y = string.Empty;
            }
            else
            {
                if (((UltraGridCell)y).Value == null)
                {
                    y = string.Empty;
                }
                else
                {
                    yVal = ((UltraGridCell)y).Value.ToString();
                }
            }

            if (Kind == SortKind.Asc)
            {
                return String.Compare(xVal, yVal);
            }
            else
            {
                return String.Compare(yVal, xVal);
            }
        }
    }
}
