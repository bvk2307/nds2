﻿using System;
using System.Xml.Serialization;

namespace Luxoft.NDS2.Common.Contracts.DTO.Query
{
    [Serializable]
    public class ColumnGrouping : ICloneable
    {
        public int GroupIndex { get; set; }

        [XmlIgnore]
        public ColumnDefinition Column { get; set; }

        object ICloneable.Clone()
        {
            return Clone();
        }

        public ColumnGrouping Clone()
        {
            return new ColumnGrouping() { GroupIndex = this.GroupIndex };
        }
    }
}
