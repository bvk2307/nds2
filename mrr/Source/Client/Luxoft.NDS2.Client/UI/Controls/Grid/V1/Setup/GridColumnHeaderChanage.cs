﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup
{
    public class GridColumnHeaderChanage
    {
        public string Caption { get; set; }
        public string ToolTipText { get; set; }
        public bool Hidden { get; set; }
        public ColumnVisibility Visibility { get; set; }

        public GridColumnHeaderChanage()
        {
            Hidden = false;
        }
    }
}
