﻿using Infragistics.Win.UltraWinToolTip;
namespace Luxoft.NDS2.Client.UI.Controls.Grid.V1
{
    partial class GridControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand1 = new Infragistics.Win.UltraWinGrid.UltraGridBand("BindingList`1", -1);
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            this.pnlRoot = new Infragistics.Win.Misc.UltraPanel();
            this.btnExportExcelCancel = new Infragistics.Win.Misc.UltraButton();
            this.btnExportExcel = new Infragistics.Win.Misc.UltraButton();
            this.btnFilterReset = new Infragistics.Win.Misc.UltraButton();
            this.btnReset = new Infragistics.Win.Misc.UltraButton();
            this.btnColumnVisibilitySetup = new Infragistics.Win.Misc.UltraDropDownButton();
            this.popupColumnContainer = new Infragistics.Win.Misc.UltraPopupControlContainer(this.components);
            this.columnVisibilityManager = new Infragistics.Win.UltraWinListView.UltraListView();
            this.popupControlContainerGrouping = new Infragistics.Win.Misc.UltraPopupControlContainer(this.components);
            this.pnlGridBottom = new Infragistics.Win.Misc.UltraPanel();
            this.pnlLoading = new Infragistics.Win.Misc.UltraPanel();
            this.lblCachedRows = new Infragistics.Win.Misc.UltraLabel();
            this.ProgressGridData = new System.Windows.Forms.ProgressBar();
            this.ultraPanelExportExcelState = new Infragistics.Win.Misc.UltraPanel();
            this.labelExportExcelState = new Infragistics.Win.Misc.UltraLabel();
            this.ultraPnlPages = new Infragistics.Win.Misc.UltraPanel();
            this.ultraComboEditorLinesPerPage = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLblLinesperPage = new Infragistics.Win.Misc.UltraLabel();
            this.btnNext = new Infragistics.Win.Misc.UltraButton();
            this.lblPageIndex = new Infragistics.Win.Misc.UltraLabel();
            this.btnPrev = new Infragistics.Win.Misc.UltraButton();
            this.grid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.gridBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.lblGridName = new Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel();
            this.ultraToolTipManager1 = new Infragistics.Win.UltraWinToolTip.UltraToolTipManager(this.components);
            this.pnlRoot.ClientArea.SuspendLayout();
            this.pnlRoot.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.columnVisibilityManager)).BeginInit();
            this.pnlGridBottom.ClientArea.SuspendLayout();
            this.pnlGridBottom.SuspendLayout();
            this.pnlLoading.ClientArea.SuspendLayout();
            this.pnlLoading.SuspendLayout();
            this.ultraPanelExportExcelState.ClientArea.SuspendLayout();
            this.ultraPanelExportExcelState.SuspendLayout();
            this.ultraPnlPages.ClientArea.SuspendLayout();
            this.ultraPnlPages.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboEditorLinesPerPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridBindingSource)).BeginInit();
            this.gridLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlRoot
            // 
            // 
            // pnlRoot.ClientArea
            // 
            this.pnlRoot.ClientArea.Controls.Add(this.btnExportExcelCancel);
            this.pnlRoot.ClientArea.Controls.Add(this.btnExportExcel);
            this.pnlRoot.ClientArea.Controls.Add(this.btnFilterReset);
            this.pnlRoot.ClientArea.Controls.Add(this.btnReset);
            this.pnlRoot.ClientArea.Controls.Add(this.btnColumnVisibilitySetup);
            this.pnlRoot.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlRoot.Location = new System.Drawing.Point(797, 0);
            this.pnlRoot.Margin = new System.Windows.Forms.Padding(0);
            this.pnlRoot.Name = "pnlRoot";
            this.pnlRoot.Size = new System.Drawing.Size(170, 29);
            this.pnlRoot.TabIndex = 0;
            // 
            // btnExportExcelCancel
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.BackColor2 = System.Drawing.Color.Transparent;
            appearance4.ForeColor = System.Drawing.Color.Transparent;
            appearance4.ForeColorDisabled = System.Drawing.Color.Transparent;
            appearance4.Image = global::Luxoft.NDS2.Client.Properties.Resources.reset;
            appearance4.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched;
            appearance4.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance4.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this.btnExportExcelCancel.Appearance = appearance4;
            this.btnExportExcelCancel.ButtonStyle = Infragistics.Win.UIElementButtonStyle.ButtonSoft;
            this.btnExportExcelCancel.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnExportExcelCancel.Location = new System.Drawing.Point(2, 0);
            this.btnExportExcelCancel.Margin = new System.Windows.Forms.Padding(7);
            this.btnExportExcelCancel.Name = "btnExportExcelCancel";
            this.btnExportExcelCancel.Size = new System.Drawing.Size(32, 29);
            this.btnExportExcelCancel.TabIndex = 5;
            this.btnExportExcelCancel.Visible = false;
            this.btnExportExcelCancel.Click += new System.EventHandler(this.btnExportExcelCancel_Click);
            // 
            // btnExportExcel
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.BackColor2 = System.Drawing.Color.Transparent;
            appearance5.ForeColor = System.Drawing.Color.Transparent;
            appearance5.ForeColorDisabled = System.Drawing.Color.Transparent;
            appearance5.Image = global::Luxoft.NDS2.Client.Properties.Resources.excel_create;
            appearance5.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched;
            appearance5.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance5.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this.btnExportExcel.Appearance = appearance5;
            this.btnExportExcel.ButtonStyle = Infragistics.Win.UIElementButtonStyle.ButtonSoft;
            this.btnExportExcel.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnExportExcel.Location = new System.Drawing.Point(34, 0);
            this.btnExportExcel.Margin = new System.Windows.Forms.Padding(7);
            this.btnExportExcel.Name = "btnExportExcel";
            this.btnExportExcel.Size = new System.Drawing.Size(32, 29);
            this.btnExportExcel.TabIndex = 4;
            this.btnExportExcel.Visible = false;
            this.btnExportExcel.Click += new System.EventHandler(this.btnExportExcel_Click);
            // 
            // btnFilterReset
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.BackColor2 = System.Drawing.Color.Transparent;
            appearance1.ForeColor = System.Drawing.Color.Transparent;
            appearance1.ForeColorDisabled = System.Drawing.Color.Transparent;
            appearance1.Image = global::Luxoft.NDS2.Client.Properties.Resources.filter;
            appearance1.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched;
            appearance1.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance1.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this.btnFilterReset.Appearance = appearance1;
            this.btnFilterReset.ButtonStyle = Infragistics.Win.UIElementButtonStyle.ButtonSoft;
            this.btnFilterReset.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnFilterReset.Enabled = false;
            this.btnFilterReset.Location = new System.Drawing.Point(66, 0);
            this.btnFilterReset.Margin = new System.Windows.Forms.Padding(7);
            this.btnFilterReset.Name = "btnFilterReset";
            this.btnFilterReset.Size = new System.Drawing.Size(32, 29);
            this.btnFilterReset.TabIndex = 6;
            this.btnFilterReset.Visible = false;
            this.btnFilterReset.Click += new System.EventHandler(this.btnFilterReset_Click);
            // 
            // btnReset
            // 
            appearance9.BackColor = System.Drawing.Color.Transparent;
            appearance9.BackColor2 = System.Drawing.Color.Transparent;
            appearance9.ForeColor = System.Drawing.Color.Transparent;
            appearance9.ForeColorDisabled = System.Drawing.Color.Transparent;
            appearance9.Image = global::Luxoft.NDS2.Client.Properties.Resources.edit_clear;
            appearance9.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched;
            appearance9.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance9.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this.btnReset.Appearance = appearance9;
            this.btnReset.ButtonStyle = Infragistics.Win.UIElementButtonStyle.ButtonSoft;
            this.btnReset.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnReset.Enabled = false;
            this.btnReset.Location = new System.Drawing.Point(98, 0);
            this.btnReset.Margin = new System.Windows.Forms.Padding(7);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(32, 29);
            this.btnReset.TabIndex = 3;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnColumnVisibilitySetup
            // 
            appearance8.Image = global::Luxoft.NDS2.Client.Properties.Resources.table_select_column;
            appearance8.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance8.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this.btnColumnVisibilitySetup.Appearance = appearance8;
            this.btnColumnVisibilitySetup.CausesValidation = false;
            this.btnColumnVisibilitySetup.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnColumnVisibilitySetup.Location = new System.Drawing.Point(130, 0);
            this.btnColumnVisibilitySetup.Name = "btnColumnVisibilitySetup";
            this.btnColumnVisibilitySetup.PopupItemKey = "columnVisibilityManager";
            this.btnColumnVisibilitySetup.PopupItemProvider = this.popupColumnContainer;
            this.btnColumnVisibilitySetup.Size = new System.Drawing.Size(40, 29);
            this.btnColumnVisibilitySetup.Style = Infragistics.Win.Misc.SplitButtonDisplayStyle.DropDownButtonOnly;
            this.btnColumnVisibilitySetup.TabIndex = 4;
            this.btnColumnVisibilitySetup.TabStop = false;
            this.btnColumnVisibilitySetup.DroppingDown += new System.ComponentModel.CancelEventHandler(this.btnColumnVisibilitySetup_DroppingDown);
            // 
            // popupColumnContainer
            // 
            this.popupColumnContainer.PopupControl = this.columnVisibilityManager;
            // 
            // columnVisibilityManager
            // 
            this.columnVisibilityManager.BorderStyle = Infragistics.Win.UIElementBorderStyle.Rounded4;
            this.columnVisibilityManager.CausesValidation = false;
            this.columnVisibilityManager.ItemSettings.AllowEdit = Infragistics.Win.DefaultableBoolean.False;
            this.columnVisibilityManager.Location = new System.Drawing.Point(99, 3);
            this.columnVisibilityManager.MainColumn.AllowSizing = Infragistics.Win.DefaultableBoolean.True;
            this.columnVisibilityManager.MainColumn.AutoSizeMode = Infragistics.Win.UltraWinListView.ColumnAutoSizeMode.AllItems;
            this.columnVisibilityManager.Name = "columnVisibilityManager";
            this.columnVisibilityManager.Size = new System.Drawing.Size(98, 247);
            this.columnVisibilityManager.TabIndex = 5;
            this.columnVisibilityManager.TabStop = false;
            this.columnVisibilityManager.View = Infragistics.Win.UltraWinListView.UltraListViewStyle.List;
            this.columnVisibilityManager.ViewSettingsList.CheckBoxStyle = Infragistics.Win.UltraWinListView.CheckBoxStyle.CheckBox;
            this.columnVisibilityManager.ViewSettingsList.ImageSize = new System.Drawing.Size(0, 0);
            this.columnVisibilityManager.ViewSettingsList.MultiColumn = false;
            this.columnVisibilityManager.Visible = false;
            this.columnVisibilityManager.ItemCheckStateChanged += new Infragistics.Win.UltraWinListView.ItemCheckStateChangedEventHandler(this.columnVisibilityManager_ItemCheckStateChanged);
            // 
            // pnlGridBottom
            // 
            this.pnlGridBottom.AutoSize = true;
            this.pnlGridBottom.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            // 
            // pnlGridBottom.ClientArea
            // 
            this.pnlGridBottom.ClientArea.Controls.Add(this.pnlLoading);
            this.pnlGridBottom.ClientArea.Controls.Add(this.ultraPanelExportExcelState);
            this.pnlGridBottom.ClientArea.Controls.Add(this.ultraPnlPages);
            this.gridLayoutPanel.SetColumnSpan(this.pnlGridBottom, 3);
            this.pnlGridBottom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlGridBottom.Location = new System.Drawing.Point(0, 363);
            this.pnlGridBottom.Margin = new System.Windows.Forms.Padding(0);
            this.pnlGridBottom.Name = "pnlGridBottom";
            this.pnlGridBottom.Size = new System.Drawing.Size(967, 25);
            this.pnlGridBottom.TabIndex = 2;
            // 
            // pnlLoading
            // 
            this.pnlLoading.AutoSize = true;
            // 
            // pnlLoading.ClientArea
            // 
            this.pnlLoading.ClientArea.Controls.Add(this.lblCachedRows);
            this.pnlLoading.ClientArea.Controls.Add(this.columnVisibilityManager);
            this.pnlLoading.ClientArea.Controls.Add(this.ProgressGridData);
            this.pnlLoading.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlLoading.Location = new System.Drawing.Point(467, 0);
            this.pnlLoading.Name = "pnlLoading";
            this.pnlLoading.Size = new System.Drawing.Size(500, 25);
            this.pnlLoading.TabIndex = 1;
            // 
            // lblCachedRows
            // 
            appearance6.BackColor = System.Drawing.Color.Transparent;
            appearance6.BackColor2 = System.Drawing.Color.Transparent;
            appearance6.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance6.BorderColor = System.Drawing.Color.Transparent;
            appearance6.BorderColor2 = System.Drawing.Color.Transparent;
            appearance6.ForeColor = System.Drawing.Color.DimGray;
            appearance6.ForeColorDisabled = System.Drawing.Color.DimGray;
            appearance6.TextHAlignAsString = "Center";
            appearance6.TextVAlignAsString = "Middle";
            this.lblCachedRows.Appearance = appearance6;
            this.lblCachedRows.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblCachedRows.Location = new System.Drawing.Point(267, 0);
            this.lblCachedRows.Name = "lblCachedRows";
            this.lblCachedRows.Size = new System.Drawing.Size(92, 25);
            this.lblCachedRows.TabIndex = 1;
            // 
            // ProgressGridData
            // 
            this.ProgressGridData.Dock = System.Windows.Forms.DockStyle.Right;
            this.ProgressGridData.Location = new System.Drawing.Point(359, 0);
            this.ProgressGridData.Name = "ProgressGridData";
            this.ProgressGridData.Size = new System.Drawing.Size(141, 25);
            this.ProgressGridData.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.ProgressGridData.TabIndex = 0;
            // 
            // ultraPanelExportExcelState
            // 
            // 
            // ultraPanelExportExcelState.ClientArea
            // 
            this.ultraPanelExportExcelState.ClientArea.Controls.Add(this.labelExportExcelState);
            this.ultraPanelExportExcelState.Dock = System.Windows.Forms.DockStyle.Left;
            this.ultraPanelExportExcelState.Location = new System.Drawing.Point(271, 0);
            this.ultraPanelExportExcelState.Name = "ultraPanelExportExcelState";
            this.ultraPanelExportExcelState.Size = new System.Drawing.Size(196, 25);
            this.ultraPanelExportExcelState.TabIndex = 2;
            this.ultraPanelExportExcelState.Visible = false;
            // 
            // labelExportExcelState
            // 
            this.labelExportExcelState.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelExportExcelState.Location = new System.Drawing.Point(0, 0);
            this.labelExportExcelState.Name = "labelExportExcelState";
            this.labelExportExcelState.Size = new System.Drawing.Size(196, 25);
            this.labelExportExcelState.TabIndex = 8;
            // 
            // ultraPnlPages
            // 
            this.ultraPnlPages.AutoSize = true;
            this.ultraPnlPages.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            // 
            // ultraPnlPages.ClientArea
            // 
            this.ultraPnlPages.ClientArea.Controls.Add(this.ultraComboEditorLinesPerPage);
            this.ultraPnlPages.ClientArea.Controls.Add(this.ultraLblLinesperPage);
            this.ultraPnlPages.ClientArea.Controls.Add(this.btnNext);
            this.ultraPnlPages.ClientArea.Controls.Add(this.lblPageIndex);
            this.ultraPnlPages.ClientArea.Controls.Add(this.btnPrev);
            this.ultraPnlPages.Dock = System.Windows.Forms.DockStyle.Left;
            this.ultraPnlPages.Location = new System.Drawing.Point(0, 0);
            this.ultraPnlPages.Name = "ultraPnlPages";
            this.ultraPnlPages.Size = new System.Drawing.Size(271, 25);
            this.ultraPnlPages.TabIndex = 0;
            // 
            // ultraComboEditorLinesPerPage
            // 
            this.ultraComboEditorLinesPerPage.Dock = System.Windows.Forms.DockStyle.Left;
            this.ultraComboEditorLinesPerPage.Location = new System.Drawing.Point(187, 0);
            this.ultraComboEditorLinesPerPage.Name = "ultraComboEditorLinesPerPage";
            this.ultraComboEditorLinesPerPage.Size = new System.Drawing.Size(84, 21);
            this.ultraComboEditorLinesPerPage.TabIndex = 7;
            this.ultraComboEditorLinesPerPage.Text = "200";
            this.ultraComboEditorLinesPerPage.SelectionChanged += new System.EventHandler(this.ultraComboEditorLinesPerPage_SelectionChanged);
            this.ultraComboEditorLinesPerPage.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ultraComboEditorLinesPerPage_KeyDown);
            // 
            // ultraLblLinesperPage
            // 
            this.ultraLblLinesperPage.Dock = System.Windows.Forms.DockStyle.Left;
            this.ultraLblLinesperPage.Location = new System.Drawing.Point(72, 0);
            this.ultraLblLinesperPage.Name = "ultraLblLinesperPage";
            this.ultraLblLinesperPage.Size = new System.Drawing.Size(115, 25);
            this.ultraLblLinesperPage.TabIndex = 6;
            this.ultraLblLinesperPage.Text = "Строк на страницу:";
            // 
            // btnNext
            // 
            appearance3.Image = global::Luxoft.NDS2.Client.Properties.Resources.right_grey;
            appearance3.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance3.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this.btnNext.Appearance = appearance3;
            this.btnNext.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnNext.Location = new System.Drawing.Point(46, 0);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(26, 25);
            this.btnNext.TabIndex = 4;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // lblPageIndex
            // 
            this.lblPageIndex.AutoSize = true;
            this.lblPageIndex.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblPageIndex.Location = new System.Drawing.Point(26, 0);
            this.lblPageIndex.Name = "lblPageIndex";
            this.lblPageIndex.Padding = new System.Drawing.Size(5, 0);
            this.lblPageIndex.Size = new System.Drawing.Size(20, 25);
            this.lblPageIndex.TabIndex = 5;
            this.lblPageIndex.Text = "1";
            // 
            // btnPrev
            // 
            appearance2.Image = global::Luxoft.NDS2.Client.Properties.Resources.left_grey;
            appearance2.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance2.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this.btnPrev.Appearance = appearance2;
            this.btnPrev.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnPrev.Enabled = false;
            this.btnPrev.Location = new System.Drawing.Point(0, 0);
            this.btnPrev.Name = "btnPrev";
            this.btnPrev.Size = new System.Drawing.Size(26, 25);
            this.btnPrev.TabIndex = 3;
            this.btnPrev.Click += new System.EventHandler(this.btnPrev_Click);
            // 
            // grid
            // 
            this.gridLayoutPanel.SetColumnSpan(this.grid, 3);
            this.grid.DataSource = this.gridBindingSource;
            appearance21.BackColor = System.Drawing.SystemColors.Window;
            appearance21.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.grid.DisplayLayout.Appearance = appearance21;
            ultraGridBand1.Override.HeaderPlacement = Infragistics.Win.UltraWinGrid.HeaderPlacement.FixedOnTop;
            this.grid.DisplayLayout.BandsSerializer.Add(ultraGridBand1);
            this.grid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.grid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance22.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance22.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance22.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance22.BorderColor = System.Drawing.SystemColors.Window;
            this.grid.DisplayLayout.GroupByBox.Appearance = appearance22;
            appearance23.ForeColor = System.Drawing.SystemColors.GrayText;
            this.grid.DisplayLayout.GroupByBox.BandLabelAppearance = appearance23;
            this.grid.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance24.BackColor2 = System.Drawing.SystemColors.Control;
            appearance24.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance24.ForeColor = System.Drawing.SystemColors.GrayText;
            this.grid.DisplayLayout.GroupByBox.PromptAppearance = appearance24;
            this.grid.DisplayLayout.MaxColScrollRegions = 1;
            this.grid.DisplayLayout.MaxRowScrollRegions = 1;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.grid.DisplayLayout.Override.ActiveCellAppearance = appearance13;
            appearance14.BackColor = System.Drawing.SystemColors.Highlight;
            appearance14.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.grid.DisplayLayout.Override.ActiveRowAppearance = appearance14;
            this.grid.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.grid.DisplayLayout.Override.AllowColMoving = Infragistics.Win.UltraWinGrid.AllowColMoving.WithinGroup;
            this.grid.DisplayLayout.Override.AllowColSwapping = Infragistics.Win.UltraWinGrid.AllowColSwapping.NotAllowed;
            this.grid.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.grid.DisplayLayout.Override.AllowMultiCellOperations = Infragistics.Win.UltraWinGrid.AllowMultiCellOperation.Copy;
            this.grid.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.grid.DisplayLayout.Override.AllowRowSummaries = Infragistics.Win.UltraWinGrid.AllowRowSummaries.True;
            this.grid.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
            this.grid.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.grid.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance15.BackColor = System.Drawing.SystemColors.Window;
            this.grid.DisplayLayout.Override.CardAreaAppearance = appearance15;
            appearance16.BorderColor = System.Drawing.Color.Silver;
            appearance16.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.grid.DisplayLayout.Override.CellAppearance = appearance16;
            this.grid.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
            this.grid.DisplayLayout.Override.CellPadding = 0;
            this.grid.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Row;
            this.grid.DisplayLayout.Override.FilterEvaluationTrigger = Infragistics.Win.UltraWinGrid.FilterEvaluationTrigger.OnLeaveCell;
            this.grid.DisplayLayout.Override.FilterOperandStyle = Infragistics.Win.UltraWinGrid.FilterOperandStyle.DropDownList;
            this.grid.DisplayLayout.Override.FilterOperatorDropDownItems = ((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems)((((((((((((((Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Equals | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.NotEquals) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.LessThan) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.LessThanOrEqualTo) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.GreaterThan) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.GreaterThanOrEqualTo) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Like) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.NotLike) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.StartsWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotStartWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.EndsWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotEndWith) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.Contains) 
            | Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems.DoesNotContain)));
            this.grid.DisplayLayout.Override.FilterOperatorLocation = Infragistics.Win.UltraWinGrid.FilterOperatorLocation.WithOperand;
            this.grid.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons;
            appearance17.BackColor = System.Drawing.SystemColors.Control;
            appearance17.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance17.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance17.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance17.BorderColor = System.Drawing.SystemColors.Window;
            this.grid.DisplayLayout.Override.GroupByRowAppearance = appearance17;
            this.grid.DisplayLayout.Override.GroupBySummaryDisplayStyle = Infragistics.Win.UltraWinGrid.GroupBySummaryDisplayStyle.SummaryCellsAlwaysBelowDescription;
            appearance18.TextHAlignAsString = "Left";
            this.grid.DisplayLayout.Override.HeaderAppearance = appearance18;
            this.grid.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.ExternalSortMulti;
            this.grid.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            appearance19.BorderColor = System.Drawing.Color.Silver;
            this.grid.DisplayLayout.Override.RowAppearance = appearance19;
            this.grid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            this.grid.DisplayLayout.Override.RowSizing = Infragistics.Win.UltraWinGrid.RowSizing.Fixed;
            this.grid.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.grid.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
            this.grid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            this.grid.DisplayLayout.Override.SummaryDisplayArea = ((Infragistics.Win.UltraWinGrid.SummaryDisplayAreas)(((Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.BottomFixed | Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.InGroupByRows) 
            | Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.GroupByRowsFooter)));
            appearance20.BackColor = System.Drawing.SystemColors.ControlLight;
            this.grid.DisplayLayout.Override.TemplateAddRowAppearance = appearance20;
            this.grid.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.grid.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grid.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.grid.Location = new System.Drawing.Point(3, 35);
            this.grid.Name = "grid";
            this.grid.Size = new System.Drawing.Size(961, 325);
            this.grid.TabIndex = 1;
            this.grid.Text = "GridControl";
            this.grid.UseAppStyling = false;
            this.grid.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.grid_AfterCellUpdate);
            this.grid.AfterRowActivate += new System.EventHandler(this.grid_AfterRowActivate);
            this.grid.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.grid_CellChange);
            this.grid.AfterSelectChange += new Infragistics.Win.UltraWinGrid.AfterSelectChangeEventHandler(this.grid_AfterSelectChange);
            this.grid.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.grid_DoubleClickRow);
            this.grid.BeforeRowFilterDropDown += new Infragistics.Win.UltraWinGrid.BeforeRowFilterDropDownEventHandler(this.grid_BeforeRowFilterDropDown);
            this.grid.BeforeCustomRowFilterDialog += new Infragistics.Win.UltraWinGrid.BeforeCustomRowFilterDialogEventHandler(this.grid_BeforeCustomRowFilterDialog);
            this.grid.BeforeHeaderCheckStateChanged += new Infragistics.Win.UltraWinGrid.BeforeHeaderCheckStateChangedEventHandler(this.grid_BeforeHeaderCheckStateChanged);
            this.grid.AfterHeaderCheckStateChanged += new Infragistics.Win.UltraWinGrid.AfterHeaderCheckStateChangedEventHandler(this.grid_AfterHeaderCheckStateChanged);
            this.grid.KeyUp += new System.Windows.Forms.KeyEventHandler(this.grid_KeyUp);
            // 
            // gridLayoutPanel
            // 
            this.gridLayoutPanel.ColumnCount = 3;
            this.gridLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 206F));
            this.gridLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.gridLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 170F));
            this.gridLayoutPanel.Controls.Add(this.grid, 0, 1);
            this.gridLayoutPanel.Controls.Add(this.pnlGridBottom, 0, 2);
            this.gridLayoutPanel.Controls.Add(this.pnlRoot, 2, 0);
            this.gridLayoutPanel.Controls.Add(this.lblGridName, 0, 0);
            this.gridLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.gridLayoutPanel.Name = "gridLayoutPanel";
            this.gridLayoutPanel.RowCount = 4;
            this.gridLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.gridLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.gridLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.gridLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 9F));
            this.gridLayoutPanel.Size = new System.Drawing.Size(967, 397);
            this.gridLayoutPanel.TabIndex = 4;
            // 
            // lblGridName
            // 
            appearance7.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance7.FontData.BoldAsString = "True";
            appearance7.TextVAlignAsString = "Middle";
            this.lblGridName.Appearance = appearance7;
            this.lblGridName.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.gridLayoutPanel.SetColumnSpan(this.lblGridName, 2);
            this.lblGridName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblGridName.Location = new System.Drawing.Point(3, 3);
            this.lblGridName.Name = "lblGridName";
            this.lblGridName.Size = new System.Drawing.Size(791, 26);
            this.lblGridName.TabIndex = 3;
            this.lblGridName.TabStop = true;
            this.lblGridName.Value = "";
            // 
            // ultraToolTipManager1
            // 
            this.ultraToolTipManager1.ContainingControl = this;
            // 
            // GridControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.gridLayoutPanel);
            this.Name = "GridControl";
            this.Size = new System.Drawing.Size(967, 397);
            this.pnlRoot.ClientArea.ResumeLayout(false);
            this.pnlRoot.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.columnVisibilityManager)).EndInit();
            this.pnlGridBottom.ClientArea.ResumeLayout(false);
            this.pnlGridBottom.ClientArea.PerformLayout();
            this.pnlGridBottom.ResumeLayout(false);
            this.pnlGridBottom.PerformLayout();
            this.pnlLoading.ClientArea.ResumeLayout(false);
            this.pnlLoading.ResumeLayout(false);
            this.pnlLoading.PerformLayout();
            this.ultraPanelExportExcelState.ClientArea.ResumeLayout(false);
            this.ultraPanelExportExcelState.ResumeLayout(false);
            this.ultraPnlPages.ClientArea.ResumeLayout(false);
            this.ultraPnlPages.ClientArea.PerformLayout();
            this.ultraPnlPages.ResumeLayout(false);
            this.ultraPnlPages.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboEditorLinesPerPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridBindingSource)).EndInit();
            this.gridLayoutPanel.ResumeLayout(false);
            this.gridLayoutPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraPanel pnlRoot;
        private Infragistics.Win.Misc.UltraPanel pnlGridBottom;
        private Infragistics.Win.UltraWinGrid.UltraGrid grid;
        private System.Windows.Forms.TableLayoutPanel gridLayoutPanel;
        private System.Windows.Forms.BindingSource gridBindingSource;
        private Infragistics.Win.Misc.UltraButton btnReset;
        private Infragistics.Win.Misc.UltraPanel ultraPnlPages;
        private Infragistics.Win.Misc.UltraLabel lblPageIndex;
        private Infragistics.Win.Misc.UltraButton btnNext;
        private Infragistics.Win.Misc.UltraButton btnPrev;
        private Infragistics.Win.Misc.UltraPanel pnlLoading;
        private System.Windows.Forms.ProgressBar ProgressGridData;
        private Infragistics.Win.Misc.UltraDropDownButton btnColumnVisibilitySetup;
        private Infragistics.Win.Misc.UltraPopupControlContainer popupColumnContainer;
        private Infragistics.Win.UltraWinListView.UltraListView columnVisibilityManager;
        private Infragistics.Win.Misc.UltraPopupControlContainer popupControlContainerGrouping;
        private Infragistics.Win.Misc.UltraLabel lblCachedRows;
        private Infragistics.Win.Misc.UltraLabel ultraLblLinesperPage;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor ultraComboEditorLinesPerPage;
        private Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel lblGridName;
        private UltraToolTipManager ultraToolTipManager1;
        private Infragistics.Win.Misc.UltraButton btnExportExcel;
        private Infragistics.Win.Misc.UltraButton btnExportExcelCancel;
        private Infragistics.Win.Misc.UltraPanel ultraPanelExportExcelState;
        private Infragistics.Win.Misc.UltraLabel labelExportExcelState;
        private Infragistics.Win.Misc.UltraButton btnFilterReset;

    }
}
