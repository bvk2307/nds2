﻿using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V1.Helpers
{
    public static class GridColumnsHelper
    {
        public static SortIndicator ToSortIndicator(this ColumnSort.SortOrder order)
        {
            switch (order)
            {
                case ColumnSort.SortOrder.Asc: return SortIndicator.Ascending;
                case ColumnSort.SortOrder.Desc: return SortIndicator.Descending;
                default: return SortIndicator.None;
            }
        }
    }
}
