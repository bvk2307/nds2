﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V1.Comparers
{
    public class BaseComparer
    {
        public enum SortKind
        {
            Asc,
            Desc
        }

        public SortKind Kind { get; set; }
    }
}
