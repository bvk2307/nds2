﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using Luxoft.NDS2.Common.Contracts.DTO.Query;
//
//namespace Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup
//{
//    class GridSetupPopupHelper
//    {
//        private const string FORMAT_COL_RELATIONSHIP = "->";
//
//
//        public Dictionary<string, ColumnDefinition> StartDeepColumnNamesResolving(Setup.GridSetup setup)
//        {
//            if (setup == null)
//            {
//                return null;
//            }
//            //var groups = setup.Groups;
//            var globalResult = new Dictionary<string, ColumnDefinition>();
//            //foreach (var elem in groups)
//            //{
//            //    GetDeepColumnFullNameRecursive(elem, ref globalResult, elem.Caption);
//            //}
//            foreach (var elem in setup.Columns.Where(x => x != null).Where(x => x.GetType() == typeof(ColumnDefinition)))
//            {
//                globalResult.Add(((ColumnDefinition)elem).Caption, (ColumnDefinition)elem);
//            }
//
//            foreach (var elem in setup.Columns.Where(x => x != null).Where(x => x.GetType() == typeof(ColumnGroupDefinition)))
//            {
//                GetDeepColumnFullNameRecursive((ColumnGroupDefinition)elem, ref globalResult, ((ColumnGroupDefinition)elem).Caption);
//            }
//
//            return globalResult;
//
//            return null;
//        }
//
//        public void GetDeepColumnFullNameRecursive(ColumnGroupDefinition currColumnGroupDefinition, ref Dictionary<string, ColumnDefinition> columns, string partOfName = "")
//        {
//            if (currColumnGroupDefinition.HasSubGroups)
//            {
//                foreach (var elem in currColumnGroupDefinition.SubGroups)
//                {
//                    var newPartOfName = String.Format("{0}{1}{2}", partOfName, FORMAT_COL_RELATIONSHIP, elem.Caption);
//                    GetDeepColumnFullNameRecursive(elem, ref columns, newPartOfName);
//                }
//            }
//            foreach (var elem in currColumnGroupDefinition.Columns)
//            {
//                string newPartOfName = String.Format("{0}{1}{2}", partOfName, FORMAT_COL_RELATIONSHIP, elem.Caption);
//                columns.Add(newPartOfName, elem);
//            }
//        }
//    }
//}
