﻿using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup
{
    [Serializable]
    public class GridColumnFilter
    {
        public string ColumnKey
        {
            get;
            set;
        }

        public List<FilterCondition> Conditions
        {
            get;
            set;
        }

        public FilterLogicalOperator LogicalOperator
        {
            get;
            set;
        }
    }
}
