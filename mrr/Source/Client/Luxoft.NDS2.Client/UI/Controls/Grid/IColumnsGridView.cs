﻿namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    public interface IColumnsGridView : IGridView
    {
        IGridColumnsCollection Columns
        {
            get;
        }
    }
}
