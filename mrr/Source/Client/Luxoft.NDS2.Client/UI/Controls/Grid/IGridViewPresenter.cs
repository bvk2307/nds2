﻿namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    public interface IGridViewPresenter
    {
        bool IsLoaded
        {
            get;
        }

        void BeginLoad();
    }
}
