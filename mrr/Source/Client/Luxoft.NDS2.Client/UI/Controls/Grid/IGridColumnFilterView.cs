﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    public interface IGridColumnFilterView
    {
        bool AllowFilter
        {
            get;
            set;
        }

        FilterQuery Default
        {
            get;
        }

        Infragistics.Win.UltraWinGrid.ColumnFilter DefaultColumnFilter
        {
            get;
        }

        FilterQuery Current
        {
            get;
            set;
        }

        void ResetFilter();
    }
}
