﻿using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    public interface IExtendedGridView : IColumnsGridView
    {
        IGridEventsHandler GridEvents
        {
            get;
        }
    }
}
