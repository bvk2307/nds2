﻿using Luxoft.NDS2.Client.LocalStorage;
using query = Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.Extensions
{
    public class ColumnFilterStorage : ColumnSettingsStorage<query.FilterQuery>
    {
        private readonly IGridColumnsCollection _columns;
        public ColumnFilterStorage(
            IGridColumnsCollection columns,
            IStorage storage,
            string key)
            : base(columns, storage, key)
        {
            _columns = columns;
        }

        protected override query.FilterQuery GetValue(IGridColumnView column)
        {
            return column.Filter.Current;
        }

        protected override void SetValue(IGridColumnView column, query.FilterQuery value)
        {
            column.Filter.Current = value;
        }
    }
}
