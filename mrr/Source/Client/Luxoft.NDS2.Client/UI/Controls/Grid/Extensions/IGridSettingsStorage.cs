﻿namespace Luxoft.NDS2.Client.UI.Controls.Grid.Extensions
{
    public interface IGridSettingsStorage
    {
        void Save();

        void Restore();
    }
}
