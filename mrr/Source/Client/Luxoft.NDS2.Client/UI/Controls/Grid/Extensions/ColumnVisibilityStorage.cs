﻿
namespace Luxoft.NDS2.Client.UI.Controls.Grid.Extensions
{
    public class ColumnVisibilityStorage : ColumnSettingsStorage<bool>
    {
        public ColumnVisibilityStorage(
            IGridColumnsCollection columns, 
            IStorage storage, 
            string key)
            : base(columns, storage, key)
        {
        }

        protected override bool GetValue(IGridColumnView column)
        {
            return column.Visible;
        }

        protected override void SetValue(IGridColumnView column, bool value)
        {
            column.Visible = value;
        }
    }
}
