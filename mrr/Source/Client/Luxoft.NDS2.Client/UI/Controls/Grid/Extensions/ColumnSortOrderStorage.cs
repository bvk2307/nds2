﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.Extensions
{
    public class ColumnSortOrderStorage : ColumnSettingsStorage<ColumnSort.SortOrder>
    {
        public ColumnSortOrderStorage(
            IGridColumnsCollection columns, 
            IStorage storage, 
            string key)
            : base(columns, storage, key)
        {
        }

        protected override ColumnSort.SortOrder GetValue(IGridColumnView column)
        {
            return column.SortOrder;
        }

        protected override void SetValue(IGridColumnView column, ColumnSort.SortOrder value)
        {
            if (column.AllowSort)
            {
                column.SortOrder = value;
            }
        }
    }
}
