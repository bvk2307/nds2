﻿using Luxoft.NDS2.Client.LocalStorage;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.Extensions
{
    public abstract class ColumnSettingsStorage<T> : XmlStorage<SerializableDictionary<string, T>>, IGridSettingsStorage
    {
        private readonly IGridColumnsCollection _columns;

        protected ColumnSettingsStorage(
            IGridColumnsCollection columns, 
            IStorage storage, 
            string key)
            : base(storage, key)
        {
            _columns = columns;
        }

        protected sealed override SerializableDictionary<string, T> GetState()
        {
            var data = new SerializableDictionary<string, T>();
            _columns.ForAll(col => data.Add(col.Key, GetValue(col)));

            return data;
        }

        protected abstract T GetValue(IGridColumnView column);

        protected override void SetState(SerializableDictionary<string, T> state)
        {
            if (state != null)
                _columns.ForAll(col =>
                {
                    if (state.ContainsKey(col.Key))
                    {
                        SetValue(col, state[col.Key]);
                    }
                });
        }

        protected abstract void SetValue(IGridColumnView column, T value);
    }
}
