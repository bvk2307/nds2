﻿using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.Extensions
{
    public static class QueryConditionColumnReplacement
    {
        public static QueryConditions ReplaceFilterColumn(
            this QueryConditions qc,
            string oldColumnKey,
            string newColumnKey)
        {
            var filter =
                qc.Filter.FirstOrDefault(x => x.ColumnName == oldColumnKey);

            if (filter != null)
                filter.ColumnName = newColumnKey;

            return qc;
        }

        public static QueryConditions ReplaceSortColumn(
            this QueryConditions qc,
            string oldColumnKey,
            string newColumnKey)
        {
            var sorting =
                qc.Sorting.FirstOrDefault(x => x.ColumnKey == oldColumnKey);

            if (sorting != null)
                sorting.ColumnKey = newColumnKey;

            return qc;
        }
    }
}
