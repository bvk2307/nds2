﻿namespace Luxoft.NDS2.Client.UI.Controls.Grid.Extensions
{
    public class ColumnPropertiesStorage : ColumnSettingsStorage<GridColumnVisualProperties>
    {
        public ColumnPropertiesStorage(
            IGridColumnsCollection columns,
            IStorage storage,
            string key)
            : base(columns, storage, key)
        {
        }

        protected override GridColumnVisualProperties GetValue(IGridColumnView column)
        {
            return column.HeaderProps;
        }

        protected override void SetValue(IGridColumnView column, GridColumnVisualProperties value)
        {
            column.HeaderProps = value;
        }
    }
}
