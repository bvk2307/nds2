﻿using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.CustomSettings;
using query = Luxoft.NDS2.Common.Contracts.DTO.Query;
using System.Collections.Generic;
using System.Linq;
using System;

namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    public class GridColumnFilterView : IGridColumnFilterView
    {
        private const int NoneValueOptionsQuantity = 2;

        protected readonly UltraGridColumn _column;

        public GridColumnFilterView(UltraGridColumn column)
        {
            _column = column;
            DefaultColumnFilter = null;
            column.Band.Layout.Grid.BeforeRowFilterDropDown += BeforeQuickFilterDropDown;
        }

        public bool AllowFilter
        {
            get
            {
                return _column.AllowRowFiltering == DefaultableBoolean.True;
            }
            set
            {
                _column.AllowRowFiltering = 
                    value ? DefaultableBoolean.True : DefaultableBoolean.False;
            }
        }

        public query.FilterQuery Default
        {
            get { return DefaultColumnFilter == null ? null : DefaultColumnFilter.Convert(); }
        }

        public virtual query.FilterQuery Current
        {
            get
            {
                if (_column.Band.ColumnFilters[_column].FilterConditions.Count == 0)
                {
                    return null;
                }

                return _column.Band.ColumnFilters[_column.Key].Convert();
            }

            set
            {
                _column.Band.ColumnFilters[_column.Key].ClearFilterConditions();

                if (value != null)
                {
                    foreach(var filter in value.Filtering)
                    {
                        _column.Band.ColumnFilters[_column.Key].FilterConditions.Add(
                            new FilterCondition(filter.ComparisonOperator.Convert(), filter.Value));
                    }
                }
            }
        }

        public Infragistics.Win.UltraWinGrid.ColumnFilter DefaultColumnFilter
        {
            get;
            protected set;
        }

        public void ResetFilter()
        {
            _column.Band.ColumnFilters[_column.Key].ClearFilterConditions();
            if (Default == null)
                return;

            _column.Band.ColumnFilters[_column.Key].LogicalOperator = DefaultColumnFilter.LogicalOperator;
            foreach (var condition in DefaultColumnFilter.FilterConditions)
            {
                _column.Band.ColumnFilters[_column.Key].FilterConditions.Add((FilterCondition)condition);
            }
        }

        private void BeforeQuickFilterDropDown(object sender, BeforeRowFilterDropDownEventArgs e)
        {
            if (e.Column.Key == _column.Key)
            {
                BuildQuickFilterList(e);
            }
        }

        protected virtual void BuildQuickFilterList(BeforeRowFilterDropDownEventArgs e)
        {
            var itemsToDelete =
                e.ValueList
                    .ValueListItems
                    .All
                    .Skip(NoneValueOptionsQuantity);

            foreach (var item in itemsToDelete)
            {
                e.ValueList.ValueListItems.Remove(item);
            }
            e.ValueList.ValueListItems.Add(new ValueListItem(FilterCondition.NonBlankCellValue, "(Не пусто)"));
        }
    }
}
