﻿using Infragistics.Win.UltraWinGrid;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    public static class GridContextMenuHelper
    {
        public static void SetRowContextMenu(this UltraGrid grid, ContextMenuStrip menu)
        {
            grid.MouseDown += 
                (sender, e) =>
                {
                    if (e.Button != MouseButtons.Right)
                    {
                        return;
                    }

                    var element = grid.DisplayLayout.UIElement.ElementFromPoint(e.Location);
                    var row = element.GetContext(typeof(UltraGridRow)) as UltraGridRow;
                    var cell = element.GetContext(typeof(UltraGridCell)) as UltraGridCell;

                    if (cell != null)
                    {
                        grid.Selected.Rows.Clear();
                        grid.ActiveCell = cell;
                    }
                    if (row != null && row.IsDataRow)
                    {
                        grid.ActiveRow = row;
                        menu.Show(grid.PointToScreen(e.Location));
                    }

                };
        }
    }
}
