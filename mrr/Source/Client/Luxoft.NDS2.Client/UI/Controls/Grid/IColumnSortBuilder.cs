﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    /// <summary>
    /// Этот интерфейс описывает построитель выражения для сортировки по отдельной колонке списка.
    /// Основное назначение таких построителей на клиенте заключается в том, чтобы клиент имел возможность на своей стороне обрабатывать данные, полученные от сервера
    /// и выводить их в таблице (напр. конкатенация 2 полей) и при этом передавать на сервер валидный запрос, в случае сортировки
    /// </summary>
    public interface IColumnSortBuilder
    {
        IEnumerable<ColumnSort> Build(ColumnSort.SortOrder sortOrder);
    }
}
