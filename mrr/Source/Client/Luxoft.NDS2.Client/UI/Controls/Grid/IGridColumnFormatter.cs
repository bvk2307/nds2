﻿using Infragistics.Win.UltraWinGrid;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    public interface IGridColumnFormatter
    {
        List<UltraGridColumn> ApplyFormat(List<UltraGridColumn> columns);

        UltraGridColumn ApplyFormat(UltraGridColumn column);
    }
}
