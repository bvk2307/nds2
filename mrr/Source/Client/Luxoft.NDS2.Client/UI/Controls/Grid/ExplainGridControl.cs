﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    public partial class ExplainGridControl : GridControl
    {
        public ExplainGridControl()
        {
            InitializeComponent();
            InitGrid();
        }

        private void InitGrid()
        {
            Band_EditMode = true;

            SetupBandEditMode();
        }

        public object GetDataSource()
        {
            return this.DataSource;
        }
    }
}
