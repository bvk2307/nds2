﻿using Infragistics.Shared;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    public class GridColumnCreator : IGridColumnViewCreator
    {
        private Dictionary<string, Func<UltraGridColumn, IGridColumnFilterView>> _filterViewCreators =
            new Dictionary<string, Func<UltraGridColumn, IGridColumnFilterView>>();

        public IGridColumnView Create(UltraGridGroup group,
            IEnumerable<UltraGridGroup> lesserGroups,
            IEnumerable<UltraGridColumn> lesserColumns)
        {
            return new GridColumnGroupView(group, lesserGroups, lesserColumns, this);
        }

        public GridColumnCreator WithFilterViewFor(string columnKey, Func<UltraGridColumn, IGridColumnFilterView> creator)
        {
            _filterViewCreators.Add(columnKey, creator);

            return this;
        }

        public virtual IGridColumnView Create(UltraGridColumn column)
        {
            return column.Style == ColumnStyle.CheckBox ?
                new CheckboxGridColumnView(column, FilterView(column))
                : new SingleGridColumnView(column, FilterView(column));
        }

        protected IGridColumnFilterView FilterView(UltraGridColumn column)
        {
            if (column.AllowRowFiltering == DefaultableBoolean.False)
            {
                return new NullGridColumnFilterView(column);
            }

            if (_filterViewCreators.ContainsKey(column.Key))
            {
                return _filterViewCreators[column.Key](column);
            }

            return new GridColumnFilterView(column);
        }
    }
}
