﻿using Infragistics.Win;
using System;
using System.Linq;
using System.Windows.Forms;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.Controls
{
    public partial class GridPagerView : UserControl, IGridPagerView
    {
        # region Константы

        private const string SummaryFormat = "Всего {0} (из {1})";

        private const int DefaulPageSize = 50;

        # endregion

        # region Конструкторы

        public GridPagerView()
        {
            InitializeComponent();
            _pageSizeSelect.Value = DefaulPageSize;
            _pageIndex = 1;
        }

        public GridPagerView(int[] availablePageSizes, int defaultPageSizeIndex)
            : this()
        {
            _pageSizeSelect.Items.Clear();
            _pageSizeSelect.Items.AddRange(
                availablePageSizes.Select(
                    pageSize => new ValueListItem(pageSize)).ToArray());
            _pageSizeSelect.Value = availablePageSizes[defaultPageSizeIndex];
        }

        # endregion

        # region IGridPagerView

        private int _rowsAvailable;

        public int RowsAvailable
        {
            set 
            {
                _rowsAvailable = value;
                DrawSummary();
            }
        }

        private int _rowsMatches;

        public int RowMatches
        {
            set 
            {
                _rowsMatches = value;
                DrawSummary();
                DrawTotalPagesSafe();
            }
            get
            {
                return _rowsMatches;
            }
        }

        private void DrawSummary()
        {
            Action action = () => _summary.Text = string.Format(SummaryFormat, _rowsMatches, _rowsAvailable);
            _summary.StartExecuteInUiThread(action);
        }

        public int RowsPerPage
        {
            get
            {
                return Convert.ToInt32(_pageSizeSelect.Value);
            }
        }

        private int _pageIndex;

        public int PageIndex
        {
            get
            {
                return _pageIndex;
            }
            set
            {
                if (_pageIndex != value && ValidPageIndex(value))
                {
                    _pageIndex = value;
                    DrawPageIndexSafe();
                    DrawTotalPagesSafe();
                    RaisePageIndexChanged();
                }
            }
        }

        public void ResetPageIndex()
        {
            _pageIndex = 1;
            DrawPageIndexSafe();
            DrawTotalPagesSafe();
        }

        private void DrawPageIndexSafe()
        {
            Action action = () => _pageIndexInput.Value = _pageIndex;
            _pageIndexInput.StartExecuteInUiThread(action);
        }

        private void DrawTotalPagesSafe()
        {
            Action action = DrawTotalPages;
            this.StartExecuteInUiThread(action);
        }

        private void DrawTotalPages()
        {
            var pageIndex = PageIndex;
            var pageCount = PageCount();
            _pageCount.Text = pageCount.ToString();
            _firstPage.Enabled = pageIndex > 1;
            _prevPage.Enabled = pageIndex > 1;
            _nextPage.Enabled = pageCount > pageIndex;
            _lastPage.Enabled = pageCount > pageIndex;
        }

        private int PageCount()
        {
            return _rowsMatches / RowsPerPage + (_rowsMatches % RowsPerPage == 0 ? 0 : 1);
        }

        private void RaisePageIndexChanged()
        {
            if (PageIndexChanged != null)
            {
                PageIndexChanged(this, new EventArgs());
            }
        }

        public event EventHandler PageIndexChanged;

        # endregion

        # region События

        private void PageSizeChanged(object sender, EventArgs e)
        {
            if (PageIndex == 1)
            {
                RaisePageIndexChanged();
            }
            else
            {
                PageIndex = 1;
            }
            DrawTotalPages();
        }

        private void ToLastPage(object sender, EventArgs e)
        {
            PageIndex = PageCount();
        }

        private void ToNextPage(object sender, EventArgs e)
        {
            PageIndex++;
        }

        private void ToFirstPage(object sender, EventArgs e)
        {
            PageIndex = 1;
        }

        private void ToPrevPage(object sender, EventArgs e)
        {
            PageIndex--;
        }

        private void PageIndexInputKeyUp(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrWhiteSpace(_pageIndexInput.Text))
            {
                _pageIndexInput.Text = _pageIndex.ToString();
            }
        }

        private void PageIndexInputKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Enter)
            {
                return;
            }

            int temp;
            if (int.TryParse(_pageIndexInput.Text, out temp)
                && ValidPageIndex(temp))
            {
                PageIndex = temp;
            }
            else
            {
                _pageIndexInput.Text = _pageIndex.ToString();
            }
        }

        private bool ValidPageIndex(int index)
        {
            return index >= 1 && index <= PageCount();
        }

        # endregion
    }
}
