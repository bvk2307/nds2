﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Infragistics.Win.UltraWinTree;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Grid.Addins;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.Controls
{
    public partial class GridColumnChooserView : UserControl, IColumnChooserView
    {
        private readonly Dictionary<string, UltraTreeNode> _cache =
            new Dictionary<string, UltraTreeNode>();

        public GridColumnChooserView()
        {
            InitializeComponent();
        }

        public GridColumnChooserView(int width, int height)
            : this()
        {
            Width = width;
            Height = height;
        }

        public void InitList(IEnumerable<IGridColumnView> columns)
        {
            _tree.Nodes.Clear();
            _cache.Clear();
            AddNodes(_tree.Nodes, columns);
        }

        private void AddNodes(
            TreeNodesCollection nodesCollection,
            IEnumerable<IGridColumnView> columns)
        {
            foreach (var column in columns)
            {
                var node =
                    new UltraTreeNode
                    {
                        CheckedState =
                            column.Visible
                                ? CheckState.Checked 
                                : CheckState.Unchecked,
                        Key = column.Key,
                        Text = column.ToString(),
                        DataKey = column
                    };

                column.VisibleChanged += DataItemSelectedChanged;
                AddNodes(node.Nodes, column.SubColumns);
                nodesCollection.Add(node);
                _cache.Add(column.Key, node);
            }
        }

        private void DataItemSelectedChanged(object sender, EventArgs e)
        {
            var column = sender as IGridColumnView;

            if (column == null)
            {
                return;
            }

            _cache[column.Key].CheckedState =
                column.Visible
                    ? CheckState.Checked
                    : CheckState.Unchecked;
        }

        private void NodeCheckedChanged(object sender, NodeEventArgs e)
        {
            var dataItem = e.TreeNode.DataKey as IGridColumnView;
            dataItem.Visible = e.TreeNode.CheckedState == CheckState.Checked;
        }
    }
}
