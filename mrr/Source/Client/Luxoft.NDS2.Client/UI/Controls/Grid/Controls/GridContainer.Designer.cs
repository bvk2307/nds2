﻿namespace Luxoft.NDS2.Client.UI.Controls.Grid.Controls
{
    partial class GridContainer
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolTip.UltraToolTipInfo ultraToolTipInfo1 = new Infragistics.Win.UltraWinToolTip.UltraToolTipInfo("Экспорт списка в MS Excel", Infragistics.Win.ToolTipImage.Default, null, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolTip.UltraToolTipInfo ultraToolTipInfo2 = new Infragistics.Win.UltraWinToolTip.UltraToolTipInfo("Сбросить", Infragistics.Win.ToolTipImage.Default, null, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            this._commands = new Infragistics.Win.Misc.UltraPanel();
            this._excelButton = new Infragistics.Win.Misc.UltraButton();
            this._resetButton = new Infragistics.Win.Misc.UltraButton();
            this._columnChooserButton = new Infragistics.Win.Misc.UltraDropDownButton();
            this._gridArea = new Infragistics.Win.Misc.UltraPanel();
            this._columnChooserPopup = new Infragistics.Win.Misc.UltraPopupControlContainer(this.components);
            this._toolTipManager = new Infragistics.Win.UltraWinToolTip.UltraToolTipManager(this.components);
            this._bottomArea = new Infragistics.Win.Misc.UltraPanel();
            this._commands.ClientArea.SuspendLayout();
            this._commands.SuspendLayout();
            this._gridArea.SuspendLayout();
            this._bottomArea.SuspendLayout();
            this.SuspendLayout();
            // 
            // _commands
            // 
            // 
            // _commands.ClientArea
            // 
            this._commands.ClientArea.Controls.Add(this._excelButton);
            this._commands.ClientArea.Controls.Add(this._resetButton);
            this._commands.ClientArea.Controls.Add(this._columnChooserButton);
            this._commands.Dock = System.Windows.Forms.DockStyle.Top;
            this._commands.Location = new System.Drawing.Point(0, 0);
            this._commands.Name = "_commands";
            this._commands.Size = new System.Drawing.Size(634, 30);
            this._commands.TabIndex = 0;
            // 
            // _excelButton
            // 
            appearance1.Image = global::Luxoft.NDS2.Client.Properties.Resources.excel_create;
            this._excelButton.Appearance = appearance1;
            this._excelButton.ButtonStyle = Infragistics.Win.UIElementButtonStyle.ButtonSoft;
            this._excelButton.Dock = System.Windows.Forms.DockStyle.Right;
            this._excelButton.Location = new System.Drawing.Point(530, 0);
            this._excelButton.Name = "_excelButton";
            this._excelButton.Size = new System.Drawing.Size(32, 30);
            this._excelButton.TabIndex = 7;
            ultraToolTipInfo1.ToolTipText = "Экспорт списка в MS Excel";
            this._toolTipManager.SetUltraToolTip(this._excelButton, ultraToolTipInfo1);
            this._excelButton.Visible = false;
            // 
            // _resetButton
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.BackColor2 = System.Drawing.Color.Transparent;
            appearance3.ForeColor = System.Drawing.Color.Transparent;
            appearance3.ForeColorDisabled = System.Drawing.Color.Transparent;
            appearance3.Image = global::Luxoft.NDS2.Client.Properties.Resources.edit_clear;
            appearance3.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched;
            appearance3.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance3.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this._resetButton.Appearance = appearance3;
            this._resetButton.ButtonStyle = Infragistics.Win.UIElementButtonStyle.ButtonSoft;
            this._resetButton.Dock = System.Windows.Forms.DockStyle.Right;
            this._resetButton.Enabled = false;
            this._resetButton.Location = new System.Drawing.Point(562, 0);
            this._resetButton.Margin = new System.Windows.Forms.Padding(7);
            this._resetButton.Name = "_resetButton";
            this._resetButton.Size = new System.Drawing.Size(32, 30);
            this._resetButton.TabIndex = 6;
            ultraToolTipInfo2.ToolTipText = "Сбросить";
            this._toolTipManager.SetUltraToolTip(this._resetButton, ultraToolTipInfo2);
            this._resetButton.Visible = false;
            // 
            // _columnChooserButton
            // 
            appearance8.Image = global::Luxoft.NDS2.Client.Properties.Resources.table_select_column;
            appearance8.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance8.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this._columnChooserButton.Appearance = appearance8;
            this._columnChooserButton.CausesValidation = false;
            this._columnChooserButton.Dock = System.Windows.Forms.DockStyle.Right;
            this._columnChooserButton.Location = new System.Drawing.Point(594, 0);
            this._columnChooserButton.Name = "_columnChooserButton";
            this._columnChooserButton.PopupItemKey = "columnVisibilityManager";
            this._columnChooserButton.Size = new System.Drawing.Size(40, 30);
            this._columnChooserButton.Style = Infragistics.Win.Misc.SplitButtonDisplayStyle.DropDownButtonOnly;
            this._columnChooserButton.TabIndex = 5;
            this._columnChooserButton.TabStop = false;
            this._columnChooserButton.Visible = false;
            // 
            // _gridArea
            // 
            this._gridArea.Dock = System.Windows.Forms.DockStyle.Fill;
            this._gridArea.Location = new System.Drawing.Point(0, 30);
            this._gridArea.MinimumSize = new System.Drawing.Size(0, 100);
            this._gridArea.Name = "_gridArea";
            this._gridArea.Size = new System.Drawing.Size(634, 540);
            this._gridArea.TabIndex = 2;
            // 
            // _toolTipManager
            // 
            this._toolTipManager.ContainingControl = this;
            this._toolTipManager.DisplayStyle = Infragistics.Win.ToolTipDisplayStyle.Standard;
            // 
            // _bottomArea
            // 
            this._bottomArea.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._bottomArea.Location = new System.Drawing.Point(0, 570);
            this._bottomArea.Name = "_bottomArea";
            this._bottomArea.Size = new System.Drawing.Size(634, 30);
            this._bottomArea.TabIndex = 3;
            // 
            // GridContainer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.Controls.Add(this._gridArea);
            this.Controls.Add(this._commands);
            this.Controls.Add(this._bottomArea);
            this.MinimumSize = new System.Drawing.Size(0, 160);
            this.Name = "GridContainer";
            this.Size = new System.Drawing.Size(634, 600);
            this._commands.ClientArea.ResumeLayout(false);
            this._commands.ResumeLayout(false);
            this._gridArea.ResumeLayout(false);
            this._bottomArea.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraPanel _commands;
        private Infragistics.Win.Misc.UltraPanel _gridArea;
        private Infragistics.Win.Misc.UltraPopupControlContainer _columnChooserPopup;
        private Infragistics.Win.Misc.UltraDropDownButton _columnChooserButton;
        private Infragistics.Win.UltraWinToolTip.UltraToolTipManager _toolTipManager;
        private Infragistics.Win.Misc.UltraButton _resetButton;
        private Infragistics.Win.Misc.UltraPanel _bottomArea;
        private Infragistics.Win.Misc.UltraButton _excelButton;
    }
}
