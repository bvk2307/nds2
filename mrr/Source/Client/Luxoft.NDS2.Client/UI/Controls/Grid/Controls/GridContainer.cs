﻿using Infragistics.Win;
using Infragistics.Win.UltraWinToolTip;
using Luxoft.NDS2.Client.UI.Controls.Grid.Addins;
using Luxoft.NDS2.Client.UI.Controls.Grid.Extensions;
using Luxoft.NDS2.Client.UI.Controls.LookupDictionary.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.Controls
{
    public partial class GridContainer : UserControl, IProgressIndicatorSupportable
    {
        # region .ctor

        public GridContainer()
        {
            InitializeComponent();
        }

        # endregion

        # region Grid

        public IExtendedGridView Grid
        {
            get;
            private set;
        }

        public GridContainer WithGrid<TGrid>(TGrid grid)
            where TGrid : Control, IExtendedGridView
        {
            _gridArea.ClientArea.Controls.Clear();
            grid.Dock = DockStyle.Fill;
            _gridArea.ClientArea.Controls.Add(grid);

            grid.GridEvents.Disabled += GridEventsDisabled;
            grid.GridEvents.Enabled += GridEventsEnabled;

            Grid = grid;

            return this;
        }

        # endregion

        # region Addins

        public GridContainer WithColumnChooser<T>(T columnChooser)
            where T : Control, IColumnChooserView
        {
            columnChooser.InitList(Grid.Columns);

            _columnChooserButton.Visible = true;
            _toolTipManager.SetUltraToolTip(
                _columnChooserButton,
                new UltraToolTipInfo(
                    "Настроить видимость столбцов", 
                    ToolTipImage.None, 
                    string.Empty, 
                    DefaultableBoolean.True));            
            _columnChooserPopup.PopupControl = columnChooser;
            _columnChooserButton.PopupItemKey = "columnChooser";
            _columnChooserButton.PopupItemProvider = _columnChooserPopup;

            return this;
        }

        private readonly List<IGridResetter> _gridResetters = new List<IGridResetter>();

        public GridContainer WithGridResetter(IGridResetter gridResetter)
        {
            _gridResetters.Add(gridResetter);
            _resetButton.Visible = true;
            _resetButton.Enabled = _resetButton.Enabled || !gridResetter.DefaultState;
            _resetButton.Click += (sender, e) => gridResetter.Reset();

            gridResetter.StateChanged += GridResetterChanged;

            return this;
        }

        private IGridExcelExporter _excelExporter;
        public GridContainer WithGridExcelExporter(IGridExcelExporter gridExcelExporter)
        {
            _excelExporter = gridExcelExporter;
            _excelButton.Visible = true;
            _excelButton.Click += (sender, e) => _excelExporter.Export();
            return this;
        }

        public GridContainer WithGridSettingsStorage(IGridSettingsStorage gridSettingStorage)
        {
            Grid.GridEvents.Unloaded += (sender, e) => { gridSettingStorage.Save(); };
            gridSettingStorage.Restore();
            return this;
        }

        private void GridResetterChanged(object sender, EventArgs e)
        {
            _resetButton.Enabled = _gridResetters.Any(x => !x.DefaultState);
        }

        public GridContainer WithPager(Control control)
        {
            control.Dock = DockStyle.Left;
            _bottomArea.ClientArea.Controls.Clear();
            _bottomArea.ClientArea.Controls.Add(control);            

            return this;
        }

        public GridContainer WithLoadingIndicator(Control control)
        {
            control.Dock = DockStyle.Right;
            _bottomArea.ClientArea.Controls.Add(control);

            return this;
        }

        # endregion

        # region IProgressIndicatorSupportable

        public Control SurfaceControl
        {
            get { return this.Parent; }
        }

        public Control ControlToDisable
        {
            get { return this; }
        }

        private ControlLoadingProgressHelper _progressBar;

        private void GridEventsEnabled(object sender, EventArgs e)
        {
            _progressBar = _progressBar ?? new ControlLoadingProgressHelper(this);
            _progressBar.HideProgress();
        }

        private void GridEventsDisabled(object sender, EventArgs e)
        {
            _progressBar = _progressBar ?? new ControlLoadingProgressHelper(this);
            _progressBar.ShowProgress();
        }

        # endregion
    }
}
