﻿using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    public abstract class GridViewPresenter<TModel, TDto, TResponse> : IGridViewPresenter
        where TResponse : IEnumerable<TDto>
    {
        private readonly IChangeableListViewModel<TModel, TDto> _model;

        protected QueryConditions QueryConditions;

        private readonly IServerGridDataProvider<TResponse> _dataProvider;

        private readonly IExtendedGridView _view;

        protected GridViewPresenter(
            IServerGridDataProvider<TResponse> dataProvider,
            IChangeableListViewModel<TModel, TDto> model,
            IExtendedGridView view)
        {
            _model = model;
            _view = view;
            _dataProvider = dataProvider;
            QueryConditions = new QueryConditions();
            BuildSortOrder();
            
            _dataProvider.DataLoaded += DataLoaded;
            _view.SetDataSource(_model.ListItems);
            _view.GridEvents.Sorted += SortChanged;
            _view.GridEvents.Filtered += FilterChanged;
        }

        public bool IsLoaded
        {
            get;
            private set;
        }

        public void BeginLoad()
        {
            BeforeDataLoaded();
            IsLoaded = true;
            _view.GridEvents.Disable();
            _dataProvider.BeginDataLoad(QueryConditions);           
        }

        private void DataLoaded(object sender, DataLoadedEventArgs<TResponse> e)
        {
            _model.UpdateList(e.Data);
            AfterDataLoaded(e.Data);

            _view.GridEvents.Enable();
        }

        protected virtual void AfterDataLoaded(TResponse response)
        {
        }

        protected virtual void BeforeDataLoaded()
        {
        }

        private void SortChanged(object sender, EventArgs e)
        {
            BuildSortOrder();
            BeginLoad();
        }

        private void FilterChanged(object sender, EventArgs e)
        {
            BeforeFilterChanged();
            BuildSortOrder();
            BuildFilter();
            BeginLoad();
        }

        protected virtual void BeforeFilterChanged()
        {
        }

        private void BuildSortOrder()
        {
            QueryConditions.Sorting.Clear();
            _view.Columns.ForAll(
                col =>
                {
                    if (col.SortOrder != ColumnSort.SortOrder.None)
                    {
                        QueryConditions.Sorting.AddRange(
                            _model.ColumnSortBuilder(col.Key).Build(col.SortOrder));
                    }
                });
        }

        private void BuildFilter()
        {
            QueryConditions.Filter.Clear();
            _view.Columns.ForAll(
                column =>
                {
                    try
                    {
                        if (column.Filter.Current != null)
                        {
                            var filterCopy = (FilterQuery) column.Filter.Current.Clone();
                            _model.ConvertFilter(filterCopy);
                            QueryConditions.Filter.Add(filterCopy);
                        }
                    }
                    catch (Exception ex)
                    {
                        if (_notifier != null)
                            _notifier.ShowError(ex.Message);
                        if (_logger != null)
                            _logger.LogError(ex, column.GetType().ToString());
                        column.Filter.Current = null;
                    }
                });
        }

        private INotifier _notifier;

        public IGridViewPresenter WithNotifier(INotifier notifier)
        {
            _notifier = notifier;
            return this;
        }

        private IClientLogger _logger;

        public IGridViewPresenter WithLogger(IClientLogger logger)
        {
            _logger = logger;
            return this;
        }
    }
}
