﻿using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    public interface IPageResultServiceProxy<TDto>
    {
        event GenericEventHandler<PageResult<TDto>> Completed;
    }
}
