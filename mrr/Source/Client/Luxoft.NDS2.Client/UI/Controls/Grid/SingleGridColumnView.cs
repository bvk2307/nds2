﻿using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    public class SingleGridColumnView : GridColumnView<UltraGridColumn>
    {
        private readonly EmptyGridColumnCollection _subColumns =
            new EmptyGridColumnCollection();

        public SingleGridColumnView(UltraGridColumn control, IGridColumnFilterView filterView)
            : base(control, filterView)
        {
        }

        # region Visibility

        public override bool Visible
        {
            get
            {
                return !Control.Hidden;
            }
            set
            {
                if (Control.Hidden == value)
                {
                    Control.Hidden = !value;
                    RaiseVisibleChanged();
                }
            }
        }

        public override bool ReadOnly
        {
            get
            {
                return Control.IsReadOnly;
            }
            set
            {
                Control.CellActivation = value ? Activation.NoEdit : Activation.AllowEdit;
            }
        }

        public override void ResetVisibility()
        {
            Visible = VisibleByDefault;
        }

        # endregion

        # region Dummy sub-column collection

        public override IGridColumnsCollection SubColumns
        {
            get { return _subColumns; }
        }

        # endregion

        # region Formatter

        public override IGridColumnView ApplyFormatter(IGridColumnFormatter formatter)
        {
            formatter.ApplyFormat(Control);
            return this;
        }

        public override IGridColumnView Sort(SortIndicator sortIndicator)
        {
            return this;
        }

        # endregion

        # region Sorting

        private ColumnSort.SortOrder? _defaultSortOrder;

        public override ColumnSort.SortOrder DefaultSortOrder
        {
            get
            {
                if (!_defaultSortOrder.HasValue)
                {
                    _defaultSortOrder = SortOrder;
                }

                return _defaultSortOrder.Value;
            }
            set
            {
                _defaultSortOrder = value;
            }
        }

        public override ColumnSort.SortOrder SortOrder
        {
            get
            {
                return Control.SortIndicator.ToOrder();
            }
            set
            {
                var newOrder = value.ToIndicator();

                if (Control.SortIndicator != SortIndicator.Disabled
                    && Control.SortIndicator != newOrder)
                {
                    Control.SortIndicator = newOrder;
                }
            }
        }

        public override bool AllowSort
        {
            get 
            { 
                return Control.SortIndicator != SortIndicator.Disabled; 
            }
        }

        public override void ResetSortOrder()
        {
            SortOrder = DefaultSortOrder;
        }

        # endregion

        public override string ToString()
        {
            return string.IsNullOrWhiteSpace(Caption)
                ? Description
                : Caption;
        }
    }

    public class EmptyGridColumnCollection : IGridColumnsCollection
    {
        private readonly IEnumerable<IGridColumnView> _columns = new List<IGridColumnView>();

        public bool HasColumn(string columnKey)
        {
            return false;
        }

        public IGridColumnView FindByKey(string columnKey)
        {
            throw new KeyNotFoundException();
        }

        public IEnumerator<IGridColumnView> GetEnumerator()
        {
            return _columns.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _columns.GetEnumerator();
        }

        public void ForAll(Action<IGridColumnView> action)
        {
        }
    }
}
