﻿using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    public static class UltraSortIndicatorConverter
    {
        private static Dictionary<SortIndicator, ColumnSort.SortOrder> _mapper =
            new Dictionary<SortIndicator, ColumnSort.SortOrder>
            {
                { SortIndicator.Ascending, ColumnSort.SortOrder.Asc },
                { SortIndicator.Descending, ColumnSort.SortOrder.Desc },
                { SortIndicator.Disabled, ColumnSort.SortOrder.None },
                { SortIndicator.None, ColumnSort.SortOrder.None }
            };

        public static ColumnSort.SortOrder ToOrder(this SortIndicator sortIndicator)
        {
            return _mapper[sortIndicator];
        }

        public static SortIndicator ToIndicator(this ColumnSort.SortOrder sortOrder)
        {
            return _mapper.Single(x => x.Key != SortIndicator.Disabled && x.Value == sortOrder).Key;
        }
    }
}
