﻿using System;
using Infragistics.Win.UltraWinGrid;

namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    public interface IGridEventsHandler
    {
        void Enable();

        void Disable();

        void Filter();

        event EventHandler Sorted;

        event EventHandler Filtered;

        event EventHandler Enabled;

        event EventHandler Disabled;

        event EventHandler Unloaded;
    }
}
