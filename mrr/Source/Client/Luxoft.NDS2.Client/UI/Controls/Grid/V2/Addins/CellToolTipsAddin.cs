﻿using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.Addins
{
    public class CellToolTipsAddin : IRowAddin
    {
        private readonly Dictionary<string, Func<object, string>> _cellToolTips;

        public CellToolTipsAddin(Dictionary<string, Func<object, string>> cellToolTips)
        {
            _cellToolTips = cellToolTips;
        }

        public void Customize(UltraGridRow row)
        {
            foreach (KeyValuePair<string, Func<object, string>> item in _cellToolTips)
            {
                if (row.Cells.Exists(item.Key))
                {
                    row.Cells[item.Key].ToolTipText = item.Value.Invoke(row.ListObject);
                }
            }
        }
    }
}
