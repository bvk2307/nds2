﻿using Luxoft.NDS2.Client.UI.Controls.Grid.V2.CustomSettings;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models;
using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models.Books;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Book;
using Luxoft.NDS2.Client.UI.Reports.ReportCard.Controls;
using Infragistics.Win.UltraWinGrid;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.ColumnExpressions
{
    public class ColumnExpressionIsAdditionalSheet : ColumnExpressionBase, IColumnExpression
    {
        private readonly DeclarationModelChapterDataExtractor _chapterModelExtractor;
        private const string ColumnNameChapter = "CHAPTER";
        private const string ColumnNameIsAdditionalSheet = "IsAdditionalSheet";

        public ColumnExpressionIsAdditionalSheet(DeclarationModelChapterDataExtractor context)
            : base()
        {
            _chapterModelExtractor = context;
        }

        public override void ApplyFilter(Infragistics.Win.UltraWinGrid.ColumnFilter columnFilter, QueryConditions searchCriteria)
        {
            if (_chapterModelExtractor.Extract() != null)
            {
                var filterBuilder = new CustomFilterBuilder(searchCriteria.Filter);

                filterBuilder.RemoveFilter(columnFilter);
                searchCriteria.Filter.RemoveAll(p => p.ColumnName == ColumnNameChapter);

                var filterOne = filterBuilder.GetColumnFilterOne(columnFilter);
                if (filterOne != null)
                {
                    int? chapter = GetChapter(filterOne.Value);

                    if (chapter.HasValue)
                        SearchCriteriaHelper.AddFilter(searchCriteria,
                                                       ColumnNameChapter,
                                                       FilterQuery.FilterLogicalOperator.And,
                                                       chapter.Value,
                                                       filterOne.ComparisonOperator);
                }
            }
        }

        private int? GetChapter(object value)
        {
            int? chapter = null;
            int? booleanCode = value as int?;
            if (booleanCode != null)
            {
                DeclarationInvoiceChapterInfo cache = null;

                if (booleanCode == 0)
                    cache = _chapterModelExtractor.Extract()
                                                  .Cache
                                                  .SingleOrDefault(p => (p.Part == AskInvoiceChapterNumber.Chapter8 ||
                                                                         p.Part == AskInvoiceChapterNumber.Chapter9));

                if (booleanCode == 1)
                    cache = _chapterModelExtractor.Extract()
                                                  .Cache
                                                  .SingleOrDefault(p => (p.Part == AskInvoiceChapterNumber.Chapter81 ||
                                                                         p.Part == AskInvoiceChapterNumber.Chapter91));

                if (cache != null)
                {
                    chapter = (int)cache.Part.ToBusinessNumber();
                }
            }
            return chapter;
        }


        public override void AddSorting(UltraGridColumn sortedColumn, QueryConditions searchCriteria)
        {
            ApplySorting(sortedColumn, searchCriteria, false);
        }

        public override void ApplySorting(UltraGridColumn sortedColumn, QueryConditions searchCriteria)
        {
            ApplySorting(sortedColumn, searchCriteria, true);
        }

        private void ApplySorting(UltraGridColumn sortedColumn, QueryConditions searchCriteria, bool isWithClearOtherSort)
        {
            if (_chapterModelExtractor.Extract() != null)
            {
                if (isWithClearOtherSort)
                    searchCriteria.Sorting.Clear();
                else
                    searchCriteria.Sorting.RemoveAll(p => p.ColumnKey == ColumnNameIsAdditionalSheet || p.ColumnKey == ColumnNameChapter);

                bool? isAscending = null;
                if (sortedColumn.SortIndicator == SortIndicator.Ascending)
                    isAscending = true;
                if (sortedColumn.SortIndicator == SortIndicator.Descending)
                    isAscending = false;

                if (isAscending != null)
                {
                    if (isAscending.Value)
                        searchCriteria.Sorting.Add(new ColumnSort() { ColumnKey = ColumnNameChapter, Order = ColumnSort.SortOrder.Asc });
                    else
                        searchCriteria.Sorting.Add(new ColumnSort() { ColumnKey = ColumnNameChapter, Order = ColumnSort.SortOrder.Desc });
                }
            }
        }
    }
}
