﻿using System;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;
using common = Luxoft.NDS2.Common.Contracts.DTO.Query;
using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.Helpers
{
    public static class ColumnHelper
    {
        public static ColumnGroup WithColumn(this ColumnGroup group, ColumnBase childColumn)
        {
            group.Columns.Add(childColumn);
            return group;
        }
        public static GridColumnSetup WithColumn(this GridColumnSetup setup, ColumnBase childColumn)
        {
            setup.Columns.Add(childColumn);
            return setup;
        }
        public static GridColumnSetup WithGroup(this GridColumnSetup setup, ColumnGroup group)
        {
            setup.Columns.Add(group);
            return setup;
        }


        public static GridSetup WithColumn(this GridSetup setup, common.ColumnBase childColumn)
        {
            setup.Columns.Add(childColumn);
            return setup;
        }

        public static GridSetup WithGroup(this GridSetup setup, common.ColumnGroupDefinition group)
        {
            setup.Columns.Add(group);
            return setup;
        }

        public static common.ColumnGroupDefinition WithColumn(this common.ColumnGroupDefinition setup, common.ColumnDefinition childColumn)
        {
            setup.Columns.Add(childColumn);
            return setup;
        }


        public static SortIndicator ToSortIndicator(this common.ColumnSort.SortOrder order)
        {
            switch (order)
            {
                case common.ColumnSort.SortOrder.Asc: 
                    return SortIndicator.Ascending;
                case common.ColumnSort.SortOrder.Desc: 
                    return SortIndicator.Descending;
                default:
                    return SortIndicator.None;
            }
        }

        public static ColumnBase Find(this IEnumerable<ColumnBase> columns, string key)
        {
            var column = columns.FirstOrDefault(x => x.Contains(key));

            if (column == null)
            {
                return null;
            }

            if (column is ColumnGroup)
            {
                return column.Key == key 
                    ? column
                    : ((ColumnGroup)column).Columns.Find(key);
            }

            return column;
        }

        public static SingleColumn FindSingle(this IEnumerable<ColumnBase> columns, string key)
        {
            return columns.Find(key) as SingleColumn;
        }

        public static IEnumerable<ColumnBase> ForEachSingle(this IEnumerable<ColumnBase> columns, Action<SingleColumn> action)
        {
            var listColumns = columns.ToList();

            foreach (var item in listColumns)
            {
                if (item is ColumnGroup)
                {
                    ((ColumnGroup)item).Columns.ForEachSingle(action);
                }
                else if (item is SingleColumn)
                {
                    action((SingleColumn) item);
                }
            }

            return listColumns;
        }


    }
}
