﻿using Infragistics.Win;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns.AutoComplete;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns
{
    public static class ImageColumnHelper
    {
        public static ConditionValueAppearance ImageAppearance<T>(
            this Dictionary<T, Bitmap> bitMapper,
            Control owner,
            Func<T, Bitmap, Control, Image> imageCreator)
        {
            var appearances = new ConditionValueAppearance();
            foreach (var pair in bitMapper)
            {
                var condition = new OperatorCondition(ConditionOperator.Equals, pair.Key);
                var appearance = new Infragistics.Win.Appearance
                {
                    Image = imageCreator(pair.Key, pair.Value, owner),
                    ImageHAlign = HAlign.Center,
                    ImageVAlign = VAlign.Middle,
                    ForegroundAlpha = Alpha.Transparent
                };
                appearances.Add(condition, appearance);
            }

            return appearances;
        }

        public static Image DrawIcon(this Bitmap bitmap, Control owner)
        {
            using (var stream = new MemoryStream())
            using (var hdc = owner.CreateGraphics())
            {
                var rectangle = new Rectangle(0, 0, 16, 16);
                var emf = new Metafile(stream, hdc.GetHdc(), rectangle, MetafileFrameUnit.Pixel, EmfType.EmfPlusOnly);
                using (var graphics = Graphics.FromImage(emf))
                {
                    graphics.FillRectangle(Brushes.Transparent, rectangle);
                    if (bitmap != null)
                        graphics.DrawImage(bitmap, rectangle);
                }
                return emf;
            }
        }
    }

    public class ImageColumn<T> : SingleColumn
    {
        private readonly IColumnFilterManager _optionsBuilder;

        public ImageColumn(string key, Control owner, Dictionary<T, Bitmap> dic, IGrid grid, IColumnExpression columnExpression, IColumnFilterManager optionsBuilder = null)
            : base(key, grid, columnExpression)
        {
            CustomConditionValueAppearance = 
                () => 
                    dic.ImageAppearance(
                        owner, 
                        (cellValue, value, ctrl) => GetIcon(owner, cellValue, value));
            _optionsBuilder = optionsBuilder;
        }

        private readonly Dictionary<T, Image> _icons = new Dictionary<T, Image>();

        private Image GetIcon(Control owner, T key, Bitmap bitmap)
        {
            if (!_icons.ContainsKey(key))
            {
                lock (_icons)
                {
                    if (!_icons.ContainsKey(key))
                    {
                        _icons[key] = bitmap.DrawIcon(owner);
                    }
                }
            }
            return _icons[key];
        }

        public override IColumnFilterManager OptionsBuilder()
        {           
            return _optionsBuilder ?? base.OptionsBuilder();
        }
    }
}
