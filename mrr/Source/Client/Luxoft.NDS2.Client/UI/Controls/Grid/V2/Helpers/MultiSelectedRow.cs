﻿using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Common.Contracts.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.Helpers
{
    public class MultiSelectedRow
    {
        private UltraGrid _grid;

        public MultiSelectedRow(UltraGrid grid)
        {
            _grid = grid;
        }

        public void SetupMultiSelectedRow()
        {
            _grid.DisplayLayout.Override.SelectTypeRow = SelectType.Extended;
        }

        private bool IsHasActiveRowWithCheck(string columnNameSelected)
        {
            bool isSelected = false;
            var activeRow = _grid.ActiveRow;
            if (activeRow != null)
            {
                var cellSelected = activeRow.Cells[columnNameSelected];
                if (cellSelected != null)
                    isSelected = (bool)cellSelected.Value;
            }
            return isSelected;
        }

        public void SetSelectedRows(string columnNameSelected)
        {
            bool storeInUndoCheck = false;

            bool isSelected = IsHasActiveRowWithCheck(columnNameSelected);

            if (!isSelected)
            {
                foreach (var itemRow in _grid.Rows)
                {
                    if (_grid.Selected.Rows.Contains(itemRow))
                    {
                        itemRow.Cells[columnNameSelected].SetValue(true, storeInUndoCheck);
                        itemRow.Update();
                        itemRow.Appearance.BackColor = _grid.DisplayLayout.Override.ActiveRowAppearance.BackColor;
                        itemRow.Appearance.ForeColor = _grid.DisplayLayout.Override.ActiveRowAppearance.ForeColor;
                    }
                    else
                    {
                        itemRow.Cells[columnNameSelected].SetValue(false, storeInUndoCheck);
                        itemRow.Update();
                        itemRow.Appearance.BackColor = _grid.DisplayLayout.Override.RowAppearance.BackColor;
                        itemRow.Appearance.ForeColor = _grid.DisplayLayout.Override.RowAppearance.ForeColor;
                    }
                }
                SetAnyActiveOfCheckedRows(columnNameSelected);
            }
            else
            {
                _grid.Selected.Rows.Clear();
            }
        }

        public void SetCheckedRows(string columnNameSelected)
        {
            foreach (UltraGridRow itemRow in _grid.Rows)
            {
                var cellSelected = itemRow.Cells[columnNameSelected];
                if (cellSelected != null && (bool)cellSelected.Value)
                {
                    itemRow.Appearance.BackColor = _grid.DisplayLayout.Override.ActiveRowAppearance.BackColor;
                    itemRow.Appearance.ForeColor = _grid.DisplayLayout.Override.ActiveRowAppearance.ForeColor;
                    cellSelected.Activated = false;
                }
                else
                {
                    itemRow.Appearance.BackColor = _grid.DisplayLayout.Override.RowAppearance.BackColor;
                    itemRow.Appearance.ForeColor = _grid.DisplayLayout.Override.RowAppearance.ForeColor;
                    itemRow.Activated = false;
                }
            }
            SetAnyActiveOfCheckedRows(columnNameSelected);
        }

        public void AllCheckedActionExtended(CheckState checkState)
        {
            if (checkState == CheckState.Checked)
                SetAnyActiveRow();
            if (checkState == CheckState.Unchecked)
                UnSelectAllRows();
        }

        private void UnSelectAllRows()
        {
            _grid.Selected.Rows.Clear();
            _grid.ActiveRow = null;
        }

        private void SetAnyActiveRow()
        {
            if (_grid.Rows.Count > 0)
            {
                _grid.ActiveRow = _grid.Rows[0];
            }
        }

        private void SetAnyActiveOfCheckedRows(string columnNameSelected)
        {
            foreach (var itemRow in _grid.Rows)
            {
                var cellSelected = itemRow.Cells[columnNameSelected];
                if (cellSelected != null && (bool)cellSelected.Value)
                {
                    _grid.ActiveRow = itemRow;
                    break;
                }
            }
        }

        public bool ClearAllSelectedRowExceptOne(UltraGridRow row)
        {
            int beforeCount = _grid.Selected.Rows.Count;

            _grid.Selected.Rows.Clear();
            _grid.Selected.Rows.Add(row);
            _grid.ActiveRow = row;

            int afterCount = _grid.Selected.Rows.Count;

            return (beforeCount != afterCount);
        }

        public void CheckHeaderState(string columnNameSelected)
        {
            bool isChecked = false;
            bool isUnChecked = false;
            foreach (var itemRow in _grid.Rows)
            {
                var cellSelected = itemRow.Cells[columnNameSelected];
                if (cellSelected != null)
                {
                    if ((bool)cellSelected.Value)
                        isChecked = true;
                    else
                        isUnChecked = true;
                }
            }

            if (isChecked)
            {
                if (isUnChecked)
                    _grid.DisplayLayout.Bands[0].Columns[columnNameSelected].SetHeaderCheckedState(_grid.Rows, CheckState.Indeterminate);
                else
                    _grid.DisplayLayout.Bands[0].Columns[columnNameSelected].SetHeaderCheckedState(_grid.Rows, CheckState.Checked);
            }
            else
            {
                _grid.DisplayLayout.Bands[0].Columns[columnNameSelected].SetHeaderCheckedState(_grid.Rows, CheckState.Unchecked);
            }
        }

        public void UnCheckedAndUnSelectAllRows(string columnNameSelected)
        {
            bool storeInUndoCheck = false;
            foreach (var itemRow in _grid.Rows)
            {
                itemRow.Cells[columnNameSelected].SetValue(false, storeInUndoCheck);
                itemRow.Update();
                itemRow.Appearance.BackColor = _grid.DisplayLayout.Override.RowAppearance.BackColor;
                itemRow.Appearance.ForeColor = _grid.DisplayLayout.Override.RowAppearance.ForeColor;
            }
            _grid.DisplayLayout.Bands[0].Columns[columnNameSelected].SetHeaderCheckedState(_grid.Rows, CheckState.Unchecked);
            _grid.Selected.Rows.Clear();
            _grid.ActiveRow = null;
        }

        public void ClearHeaderState(string columnNameSelected)
        {
            _grid.DisplayLayout.Bands[0].Columns[columnNameSelected].SetHeaderCheckedState(_grid.Rows, CheckState.Unchecked);
        }
    }
}
