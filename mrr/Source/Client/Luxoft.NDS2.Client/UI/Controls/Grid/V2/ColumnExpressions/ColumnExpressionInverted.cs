﻿using Luxoft.NDS2.Client.UI.Controls.Grid.V2.CustomSettings;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using Infragistics.Win.UltraWinGrid;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.ColumnExpressions
{
    class ColumnExpressionInverted: ColumnExpressionDefault
    {
        public override void ApplySorting(UltraGridColumn sortedColumn, QueryConditions searchCriteria)
        {
            var sortBuilder = new CustomSortBuilder(searchCriteria.Sorting);
            sortBuilder.SortChanged(sortedColumn, true);
        }
    }
}
