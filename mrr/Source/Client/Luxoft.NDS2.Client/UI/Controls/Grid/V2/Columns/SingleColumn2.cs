﻿using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns.AutoComplete;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns
{
    /// <summary>
    /// Этот класс добавляет в SingleColumn возможность задать IColumnFilterManager
    /// </summary>

    public class SingleColumn2 : SingleColumn
    {
        private readonly IColumnFilterManager _optionsBuilder;

        public SingleColumn2
            (string key, 
            IGrid grid,
            IColumnExpression columnExpression, 
            IColumnFilterManager optionsBuilder = null) 
            : base(key, grid, columnExpression)
        {
            _optionsBuilder = optionsBuilder;
        }

        public override IColumnFilterManager OptionsBuilder()
        {
            return _optionsBuilder ?? base.OptionsBuilder();
        }
    }
}