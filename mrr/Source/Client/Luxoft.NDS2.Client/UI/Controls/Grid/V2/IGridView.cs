﻿using Infragistics.Excel;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2
{
    public interface IGridView
    {
        Workbook ExportToWorkbook(WorkbookFormat format = WorkbookFormat.Excel2007);

        void PushData<TDataRow>(IEnumerable<TDataRow> data);

        void UpdateRow<TDataRow>(TDataRow obj, Func<TDataRow, TDataRow, bool> comparer = null);

        GridColumnSetup Columns
        {
            get;
        }
    }
}
