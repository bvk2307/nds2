﻿using Luxoft.NDS2.Client.UI.Base;

namespace Luxoft.NDS2.Client.UI.Controls.DataGrid.Pagination
{
    public interface IPagerView : IPager
    {
        uint TotalDataRows
        {
            get;
            set;
        }

        uint TotalMatches
        {
            get;
            set;
        }

        event ParameterlessEventHandler PageIndexChanged;

        event ParameterlessEventHandler PageSizeChanged;
    }
}
