﻿using Luxoft.NDS2.Client.UI.Controls.Grid.V2.CustomSettings;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2
{
    public interface IDataGridViewManager
    {
        IDataGridViewManager WithFilter(QueryConditions filter);

        IDataGridViewManager WithInitialFilter(DefaultFilter filter);

        IDataGridViewManager WithSettingsProvider(ISettingsProvider settingsProvider);

        IDataGridViewManager WithRestoreFilters(bool allowSaveFilterSettings);

        void InitColumnVisibility();

        bool Restore();

        void Reset();

        bool AllowReset();

        bool Save();

        void SetColumnVisibility(string key, bool visible);
    }
}
