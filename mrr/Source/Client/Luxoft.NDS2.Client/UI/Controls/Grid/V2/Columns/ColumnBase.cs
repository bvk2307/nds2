﻿using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.ColumnExpressions;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns.AutoComplete;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Helpers;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns
{
    /// <summary>
    /// Этот класс реализует абстракную колонку таблицы
    /// </summary>
    public abstract class ColumnBase
    {
        # region Конструкторы

        protected ColumnBase(string key, IGrid grid, IColumnExpression columnExpression)
        {
            Key = key;
            Grid = grid;
            DisableEdit = true;
            AllowToggleVisibility = true;
            OriginX = -1;
            _columnExpression = columnExpression;
            if (_columnExpression == null)
                _columnExpression = ColumnExpressionCreator.CreateColumnExpressionDefault();

            FilterOperatorDropDownItems = FilterOperatorDropDownItems.Default;
        }

        # endregion

        # region Свойства

        protected int BandIndex
        {
            get;
            private set;
        }

        /// <summary>
        /// Возвращает или задает признак того, что все элементы UI были добавлены в таблицу (Grid)
        /// </summary>
        protected bool InitCompleted
        {
            get;
            private set;
        }

        /// <summary>
        /// Возвращает ссылку на таблицу. TODO: Закрыть UltraGrid интерфейсом
        /// </summary>
        protected IGrid Grid
        {
            get;
            private set;
        }

        /// <summary>
        /// Возвращает ключ объекта таблицы
        /// </summary>
        public string Key
        {
            get;
            protected set;
        }

        private string _caption;

        /// <summary>
        /// Возвращает или задает заголовок колонки
        /// </summary>
        public string Caption
        {
            get
            {
                return _caption;
            }
            set
            {
                _caption = value;

                if (InitCompleted)
                {
                    SetCaption();
                }
            }
        }

        /// <summary>
        /// Возвращает высоту заголовка колонки
        /// </summary>
        public int HeaderSpanY
        {
            get;
            set;
        }

        private bool _disableEdit;

        /// <summary>
        /// Возвращает или задает признак, запрещающий редактировать значения в колонке
        /// </summary>
        public bool DisableEdit
        {
            get
            {
                return _disableEdit;
            }
            set
            {
                _disableEdit = value;
            }
        }

        private bool _hidden;

        public bool Hidden
        {
            get
            {
                return _hidden;
            }
            set
            {
                _hidden = value;
            }
        }

        public bool AllowToggleVisibility
        {
            get;
            set;
        }

        public bool DisableMoving { get; set; }

        public virtual string ListTitle()
        {
            return Caption;
        }

        protected IColumnExpression _columnExpression;

        public FilterOperatorDropDownItems FilterOperatorDropDownItems { get; set; }

        # endregion

        # region Добавление колонки в таблицу

        /// <summary>
        /// Инициализирует объекты таблицы
        /// </summary>
        public void Init()
        {
            InitInternal();
            InitCompleted = true;
        }

        protected virtual void InitInternal()
        {
            CreateColumn();
        }

        /// <summary>
        /// Создает и настраивает колонку
        /// </summary>
        /// <returns></returns>
        public UltraGridColumn CreateColumn()
        {
            var column = CreateOrGetColumn();
            column.RowLayoutColumnInfo.SpanY = HeaderSpanY;
            if (OriginX > -1)
                column.RowLayoutColumnInfo.OriginX = OriginX;
            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
            SetupColumn();
            column.Header.Caption = Caption;
            ToggleEdit(DisableEdit);

            return column;
        }

        private UltraGridColumn CreateOrGetColumn()
        {
            UltraGridColumn column;
            if (Band().Columns.Exists(Key))
            {
                column = Band().Columns[Key];
                column.Header.Caption = Caption;
                return column;
            }

            column = Band().Columns.Add(Key, Caption);
            
            return column;
        }

        public int OriginX { get; set; }

        protected abstract void SetupColumn();

        # endregion

        # region Изменение состояния колонки

        /// <summary>
        /// Задает заголовок колонки в таблице
        /// </summary>
        protected abstract void SetCaption();

        /// <summary>
        /// Включает/выключает редактирование
        /// </summary>
        public virtual void ToggleEdit(bool disableEdit)
        {
            var column = Column();

            column.CellActivation =
                DisableEdit || disableEdit ? Activation.NoEdit : Activation.AllowEdit;
            column.CellClickAction =
                DisableEdit || disableEdit ? CellClickAction.RowSelect : CellClickAction.Edit;
        }

        # endregion

        # region Навигация по объектам таблицы

        /// <summary>
        /// Ищет колонку среди связанных объектов в таблице
        /// </summary>
        /// <param name="columnKey">Ключ колонки</param>
        /// <returns>Находится или не находится колонка в связанных элементах таблице</returns>
        public abstract bool Contains(string columnKey);

        public virtual Dictionary<string, ColumnBase> All()
        {
            return new Dictionary<string, ColumnBase>
                {
                    { Key, this }
                };
        }

        public virtual ColumnBase Find(string key)
        {
            return Key == key ? this : null;
        }

        # endregion

        # region Вспомогательные методы

        protected virtual UltraGridBand Band()
        {
            return Grid.Grid.Band();
        }

        protected UltraGridColumn Column()
        {
            return Band().Columns[Key];
        }

        # endregion

        # region Фильтр по колонке

        public virtual IColumnFilterManager OptionsBuilder()
        {
            return new DefaultColumnFilterManager();
        }

        # endregion

        # region Построитель условий для поиска (по фильтру)

        public virtual IColumnExpression ColumnExpression()
        {
            return _columnExpression;
        }

        # endregion
    }
}
