﻿using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.CheckBoolColumn
{
    public class DictionaryBoolean
    {
        private readonly List<BooleanCode> _dataItems = new List<BooleanCode>();

        public DictionaryBoolean(IEnumerable<BooleanCode> dataItems)
        {
            _dataItems.AddRange(dataItems);
        }

        public DictionaryBoolean()
        {
            _dataItems.AddRange(CreateDataItemsDefault());
        }

        /// <summary>
        /// Создает справочник значений фильтра
        /// </summary>
        /// <param name="filterValues">Переданные значения фильтра (пары Код-Значение)</param>
        public DictionaryBoolean(Dictionary<int, string> filterValues)
        {
            _dataItems.AddRange(CreateDataItems(filterValues));
        }

        private List<BooleanCode> CreateDataItemsDefault()
        {
            var boolCodes = new List<BooleanCode>();
            boolCodes.Add(new BooleanCode() { Index = 0, Code = 1, Description = "Да" });
            boolCodes.Add(new BooleanCode() { Index = 1, Code = 0, Description = "Нет" });
            return boolCodes;
        }

        private List<BooleanCode> CreateDataItems(Dictionary<int, string> filterValues)
        {
            var boolCodes = new List<BooleanCode>();
            int index = 0;
            foreach (var filterValue in filterValues)
            {
                boolCodes.Add(new BooleanCode() { Index = index++, Code = filterValue.Key, Description = filterValue.Value });
            }
            return boolCodes;
        }

        public IEnumerable<BooleanCode> Items
        {
            get
            {
                return _dataItems
                    .OrderBy(item => item.Code)
                    .Select(item => item);
            }
        }
    }
}
