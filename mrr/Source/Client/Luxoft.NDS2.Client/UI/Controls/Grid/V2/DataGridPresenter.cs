﻿using System;
using System.Collections.Generic;
using System.Linq;
using CommonComponents.Utils.Async;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.CustomSettings;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2
{
    public class DataGridPresenter<TRowData> 
    {
        # region Конструктор

        private readonly IDataGridView _gridView;

        protected Func<QueryConditions, PageResult<TRowData>> DataLoader;

        private GridOptions _options;

        protected IDataGridViewManager _customSettings;

        /// <summary>
        /// TODO: Отвязать этот класс от Infragistic, перенеся функционал по взаимодействию с объектами грида в ColumnBase
        /// TODO: После создания PagedDataGridPresenter-а, dataLoader должен возвращать IEnumerable<TRowData>
        /// </summary>
        public DataGridPresenter(
            IDataGridView gridView,
            Func<QueryConditions, PageResult<TRowData>> dataLoader,
            QueryConditions searchCriteria = null)
        {
            _gridView = gridView;
            DataLoader = dataLoader;
            SearchCriteria = searchCriteria ?? new QueryConditions();

            _gridView.BeforeSortingChanged += SortingChanged;
            _gridView.BeforeFilterChanged += FilterChanged;
            _gridView.AfterFilterChanged += AfterFiltering;
            _gridView.ColumnVisibilityChanged += ColumnVisibilityChanged;
            _gridView.SaveSettings += SaveSettingsFire;
            _gridView.ResetSettings += ResetSettingsFire;
            _gridView.FilterResetSettings += FilterResetSettingsFire;
            _gridView.AfterSortingChanged += SaveSettingsFire;
            _gridView.AfterFilterChanged += SaveSettingsFire;
        }

        protected QueryConditions SearchCriteria
        {
            get;
            private set;
        }

        # endregion

        # region Данные

        public bool IsIgnoreData { get; set; }

        public virtual void Load()
        {
            var conditionsCount = SearchCriteria.Filter.Count;
            AllowFilterReset(conditionsCount > 0);

            if (IsIgnoreData)
                DoClearGrid();
            else
                DoAsyncLoad();
        }

        private void DoClearGrid()
        {
            _rows = new TRowData[] { };
            _gridView.PushData(_rows);
        }

        private void DoAsyncLoad()
        {
            var asyncWorker = new AsyncWorker<bool>();
            asyncWorker.DoWork += (sender, args) =>
            {
                BeforeDataLoad();

                var dataPage = DataLoader(BuildSearchCriteria());
                _rows = dataPage.Rows;

                BeforeDataPush(dataPage);
                _gridView.PushData(_rows);

                AfterDataLoad();
            };

            asyncWorker.Complete += (sender, args) => _gridView.ProgressIndicator.HideProgress();

            asyncWorker.Failed += (sender, args) => _gridView.ProgressIndicator.HideProgress();

            _gridView.ProgressIndicator.ShowProgress();
            asyncWorker.Start();


        }


        protected virtual void BeforeDataLoad()
        {
        }

        protected virtual void BeforeDataPush(PageResult<TRowData> data)
        {
        }

        protected virtual void AfterDataLoad()
        {
        }

        private IEnumerable<TRowData> _rows;

        public IEnumerable<TRowData> Rows
        {
            get
            {
                return _rows != null ? _rows.ToList().AsReadOnly() : new List<TRowData>().AsReadOnly();
            }
        }

        # endregion

        # region Инициализация представления

        public void Init(
            ISettingsProvider settingsProvider,
            GridOptions options = null)
        {
            _options = options ?? GridOptions.Default();

            _customSettings =
                _gridView.SettingsManager()
                .WithFilter(SearchCriteria)
                .WithInitialFilter(_options.DefaultFilter)
                .WithSettingsProvider(settingsProvider)
                .WithRestoreFilters(_options.AllowManageFilterSettings);

            _customSettings.InitColumnVisibility();

            _gridView.LockEvents();
            if (!_customSettings.Restore())
            {
                _customSettings.Reset();
            }
            _gridView.UnlockEvents();

            RefreshSettingsAvailability();
        }

        public void Init(GridOptions options = null)
        {
            _options = options ?? GridOptions.Default();
            _customSettings = _gridView.DefaultSettingsManager()
                .WithFilter(SearchCriteria)
                .WithInitialFilter(_options.DefaultFilter)
                .WithRestoreFilters(_options.AllowManageFilterSettings);

            _customSettings.InitColumnVisibility();

            _gridView.LockEvents();
            if (!_customSettings.Restore())
            {
                _customSettings.Reset();
            }
            _gridView.UnlockEvents();

            RefreshSettingsAvailability();
        }

        # endregion

        # region Управление пользовательскими настройками

        public void ResetSettings()
        {
            _gridView.LockEvents();

            _customSettings.Reset();
            if (SaveSettings())
                AfterSaveSettings();

            AfterResetSettings();

            _gridView.UnlockEvents();
        }

        private void RefreshSettingsAvailability()
        {
            _gridView.AllowResetSettings =
                _options.AllowManageSettings && _customSettings.AllowReset();
        }


        # endregion

        # region Обработка событий

        private void SortingChanged(SortedColumnsCollection sortedColumns)
        {
            foreach (UltraGridColumn itemColumn in sortedColumns)
            {
                var column = _gridView.Columns.Columns.Find(itemColumn.Key);
                column.ColumnExpression().ApplySorting(itemColumn, SearchCriteria);
            }

            AfterSorting();

        }

        protected virtual void AfterSorting()
        {
        }

        private void FilterChanged(Infragistics.Win.UltraWinGrid.ColumnFilter filter)
        {
            var column = _gridView.Columns.Columns.Find(filter.Column.Key);
            column.OptionsBuilder().OnFilterChanged(filter);
            column.ColumnExpression().ApplyFilter(filter, SearchCriteria);
        }

        protected virtual void AfterFiltering()
        {
        }

        private void ColumnVisibilityChanged(string key, bool state)
        {
            _customSettings.SetColumnVisibility(key, state);
            SaveSettings();
        }

        private void SaveSettingsFire()
        {
            SaveSettings();
        }

        private void ResetSettingsFire()
        {
            ResetSettings();
        }



        protected virtual void AfterSaveSettings()
        {

        }

        protected virtual void AfterResetSettings()
        {

        }

        protected virtual void AfterFilterResetSettings()
        {

        }

        private void FilterResetSettingsFire()
        {
            _gridView.LockEvents();

            var conditionsCount = SearchCriteria.Filter.Count;
            _customSettings.Reset();
            if (SaveSettings())
            {
                SearchCriteria.Filter.Clear();
                if (conditionsCount > 0)
                    AfterSaveSettings();
            }

            AfterFilterResetSettings();

            _gridView.UnlockEvents();
        }

        private bool SaveSettings()
        {
            bool rez = _customSettings.Save();

            if (rez)
                RefreshSettingsAvailability();

            return rez;
        }

        private void AllowFilterReset(bool isEnabled)
        {
            _gridView.AllowFilterReset = isEnabled;
        }

        # endregion

        # region Колонки

        public TColumn Find<TColumn>(string key)
            where TColumn : Columns.ColumnBase
        {
            return _gridView.Columns.Columns.Find(key) as TColumn;
        }

        # endregion

        # region Создание SearchCriteria

        private QueryConditions BuildSearchCriteria()
        {
            var searchCriteriaCalculated = (QueryConditions)SearchCriteria.Clone();

            foreach (Infragistics.Win.UltraWinGrid.ColumnFilter columnFilter in _gridView.ColumnFilters)
            {
                var column = _gridView.Columns.Columns.Find(columnFilter.Column.Key);
                if (column != null)
                {
                    //специальный фикс для корректной фильтрации при переходе на сопоставленную запись по скрытому полю ROW_KEY
                    //TODO: реализовать более гибкий вариант перехода на сопоставку, чтоб не зависеть от типа колонки
                    if (!column.Hidden)
                    {
                        column.ColumnExpression().ApplyFilter(columnFilter, searchCriteriaCalculated);
                        column.ColumnExpression().AddSorting(columnFilter.Column, searchCriteriaCalculated);
                    }
                }
            }

            return searchCriteriaCalculated;
        }

        public QueryConditions GetSearchCriteria()
        {
            return BuildSearchCriteria();
        }

        # endregion
    }
}
