﻿using System.Collections.Generic;
using System.Linq;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Helpers;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.CustomSettings
{
	/// <summary>
	///  Общие данные для потомков IDataGridViewManager
	/// </summary>
	public class FilterComparer
	{
		public FilterComparer(UltraGridBand band, GridColumnSetup columns)
		{
			Band = band;
			Columns = columns;
		}

		private UltraGridBand Band { get; set; }
		private GridColumnSetup Columns { get; set; }

		/// <summary>
		/// сравнение фильтра в Band и на входе
		/// </summary>
		/// <param name="settings">фильтр для сранения</param>
		public bool Compare(List<GridColumnFilter> settings)
		{
			if (Columns == null) return true;

			foreach (var item in Band.Columns.All)
			{
				var column = (UltraGridColumn) item;
				var definition = Columns.Columns.FindSingle(column.Key);

				if (definition == null) continue;

				var currentConditionQuantity =
					Band.ColumnFilters[column.Key].FilterConditions.Count;
				var compareConditionQuantity =
					settings.Any(x => x.ColumnKey == column.Key)
						? settings.First(x => x.ColumnKey == column.Key).Conditions.Count
						: 0;

				if (currentConditionQuantity != compareConditionQuantity)
					return false;

				if (currentConditionQuantity <= 0) continue;

				var arrayToCompare =
					settings.First(x => x.ColumnKey == column.Key).Conditions.ToArray();
				for (var counter = 0; counter < currentConditionQuantity; counter++)
				{
					var compare = arrayToCompare[counter];
					var current =
						(FilterCondition) Band
							.ColumnFilters[column.Key]
							.FilterConditions
							.All[counter];

					if (current.CompareValue.ToString() != compare.CompareValue.ToString()
					    || current.ComparisionOperator != compare.ComparisionOperator)
					{
						return false;
					}
				}
			}

			return true;
		}

	}
}