﻿namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns.IndexColumn
{
    public class IndexProvider : IIndexProvider
    {
        public int FirstRowIndex()
        {
            return 1;
        }
    }
}
