﻿using System;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination
{
    public class InvalidPageIndexException : Exception
    {
        private const string MessageFormat = 
            "Значение {0} не является допустимым для номера страницы списка данных";

        public InvalidPageIndexException(object pageIndexValue)
            : base(string.Format(MessageFormat, pageIndexValue))
        {
            PageIndexValue = pageIndexValue;
        }

        public object PageIndexValue
        {
            get;
            private set;
        }
    }
}
