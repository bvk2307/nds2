﻿using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2
{
    sealed class ClientGridPresenter<TRowData> : DataGridPresenter<TRowData>
    {
        public ClientGridPresenter(DataGridView gridView):
            base(gridView, GetData, null)
        {
            gridView.Grid.DisplayLayout.Override.HeaderClickAction = HeaderClickAction.SortMulti;
            gridView.Grid.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.Default;
            gridView.Grid.DisplayLayout.Override.AllowRowSummaries = AllowRowSummaries.Default;
        }

        private static PageResult<TRowData> GetData(QueryConditions qc)
        {
            return new PageResult<TRowData>();
        }

    }
}
