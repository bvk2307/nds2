﻿using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Helpers;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.CustomSettings
{
    public class CustomSortBuilder
    {
        private readonly List<ColumnSort> _sort;

        public CustomSortBuilder(List<ColumnSort> sort)
        {
            _sort = sort;
        }

        public void SortChanged(SortedColumnsCollection sortedColumns)
        {
            _sort.Clear();
            _sort.AddRange(BuildSorting(sortedColumns.All.Select(x => (UltraGridColumn)x)));
        }

        public void SortChanged(UltraGridColumn column, bool inverted = false)
        {
            var buildSorting = BuildSorting(column);
            buildSorting[0].Inverted = inverted;

            _sort.Clear();
            _sort.AddRange(buildSorting);
        }

        public void ApplySorting(List<ColumnSort> sorting, UltraGridBand band)
        {
            if (sorting != null)
            {
                _sort.Clear();
                sorting.ForEach(sort => _sort.Add((ColumnSort)sort.Clone()));

                List<UltraGridColumn> columnMustAscending = new List<UltraGridColumn>();
                List<UltraGridColumn> columnMustDescending = new List<UltraGridColumn>();

                foreach (var item in band.Columns.All)
                {
                    var column = (UltraGridColumn)item;

                    if (column.SortIndicator != SortIndicator.Disabled)
                    {
                        column.SortIndicator = SortIndicator.None;
                        var columnSort = sorting.FirstOrDefault(sort => sort.ColumnKey.ToUpper() == column.Key.ToUpper());
                        if (columnSort != null)
                            column.SortIndicator = (columnSort.Inverted ? ColumnSort.InvertOrder(columnSort.Order) : columnSort.Order).ToSortIndicator();                        
                            
                        if (column.SortIndicator == SortIndicator.Ascending)
                            columnMustAscending.Add(column);
                        if (column.SortIndicator == SortIndicator.Descending)
                            columnMustDescending.Add(column);
                    }
                }
                foreach (UltraGridColumn col in columnMustAscending)
                    col.SortIndicator = SortIndicator.Ascending;
                foreach (UltraGridColumn col in columnMustDescending)
                    col.SortIndicator = SortIndicator.Descending;
            }
        }

        public List<ColumnSort> BuildSorting(IEnumerable<UltraGridColumn> columns)
        {
            return 
                columns
                    .Where(
                        col =>
                            col.SortIndicator == SortIndicator.Ascending
                            || col.SortIndicator == SortIndicator.Descending)
                    .Select(
                        col =>
                        {
                            return
                                new ColumnSort()
                                {
                                    ColumnKey = col.Key,
                                    Order =
                                        col.SortIndicator == SortIndicator.Ascending
                                        ? ColumnSort.SortOrder.Asc
                                        : ColumnSort.SortOrder.Desc,
                                    NullAsZero = col.Nullable == Nullable.Null
                                };
                        }).ToList();
        }

        private List<ColumnSort> BuildSorting(UltraGridColumn column)
        {
            var columns = new List<UltraGridColumn>();
            columns.Add(column);
            return BuildSorting(columns);
        }
        
        /// <summary>
        /// Возвращает текущую сортировку по переданным колонкам грида (используется для сохранения настроек). 
        /// </summary>
        /// <param name="columns"></param>
        /// <returns></returns>
        public List<ColumnSort> GetSorting(IEnumerable<UltraGridColumn> columns)
        {
            var sortingColumns =
                columns.Where(
                    c => c.SortIndicator == SortIndicator.Ascending || c.SortIndicator == SortIndicator.Descending);
            return _sort.Where(s => sortingColumns.Any(c => c.Key == s.ColumnKey)).ToList();
        } 
    }
}
