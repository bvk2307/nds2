﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns
{
    public class CheckColumnThreeWay : CheckColumn
    {
        public CheckColumnThreeWay(string key, IGrid grid, IColumnExpression columnExpression)
            : base(key, grid, columnExpression)
        {
        }

        public override void RefreshHeaderState()
        {
            var self = Grid.Grid.DisplayLayout.Bands[0].Columns[Key];
            var cnt = self.Layout.Rows.Count;
            var checkedRows = self.Layout.Rows.Where(x => x.Cells[Key].Text == "True").ToArray();
            var checkedCnt = checkedRows.Count();
            var state = checkedCnt == cnt ? CheckState.Checked : CheckState.Unchecked;
            if (checkedCnt > 0 && checkedCnt != cnt)
                state = CheckState.Indeterminate;
            if (state == self.GetHeaderCheckedState(self.Layout.Rows))
                return;

            Grid.Grid.BeginUpdate();
            self.SetHeaderCheckedState(self.Layout.Rows, state);
            foreach (var row in self.Layout.Rows)
            {
                row.Cells[Key].Value = checkedRows.Contains(row);
            }
            Grid.Grid.EndUpdate();
        }
    }
}
