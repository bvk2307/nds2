﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.ColumnExpressions
{
    public class ColumnExpressionChangeMapping : ColumnExpressionDefault
    {
        private string _fieldNameInsearchCriteria;

        public ColumnExpressionChangeMapping(string fieldNameInsearchCriteria)
            : base()
        {
            _fieldNameInsearchCriteria = fieldNameInsearchCriteria;
        }

        public override void ApplyFilter(Infragistics.Win.UltraWinGrid.ColumnFilter columnFilter, QueryConditions searchCriteria)
        {
            searchCriteria.Filter.RemoveAll(p => p.ColumnName == _fieldNameInsearchCriteria);

            base.ApplyFilter(columnFilter, searchCriteria);

            foreach (var filterQuery in searchCriteria.Filter.Where(p=>p.ColumnName == columnFilter.Column.Key))
            {
                filterQuery.ColumnName = _fieldNameInsearchCriteria;
            }
        }

        public override void ApplySorting(Infragistics.Win.UltraWinGrid.UltraGridColumn sortedColumn, QueryConditions searchCriteria)
        {
            searchCriteria.Sorting.RemoveAll(p => p.ColumnKey == _fieldNameInsearchCriteria);

            base.ApplySorting(sortedColumn, searchCriteria);

            foreach (var columnSort in searchCriteria.Sorting.Where(p => p.ColumnKey == sortedColumn.Key))
            {
                columnSort.ColumnKey = _fieldNameInsearchCriteria;
            }
        }

        public override void AddSorting(Infragistics.Win.UltraWinGrid.UltraGridColumn sortedColumn, QueryConditions searchCriteria)
        {
            searchCriteria.Sorting.RemoveAll(p => p.ColumnKey == _fieldNameInsearchCriteria);

            base.AddSorting(sortedColumn, searchCriteria);

            foreach (var columnSort in searchCriteria.Sorting.Where(p => p.ColumnKey == sortedColumn.Key))
            {
                columnSort.ColumnKey = _fieldNameInsearchCriteria;
            }
        }
    }
}
