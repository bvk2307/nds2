﻿using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns.IndexColumn
{
    public class PagerIndexProvider : IIndexProvider
    {
        private readonly IPager _pager;

        public PagerIndexProvider(IPager pager)
        {
            _pager = pager;
        }

        public int FirstRowIndex()
        {
            return (int) ((_pager.PageIndex - 1) * _pager.PageSize + 1);
        }
    }
}
