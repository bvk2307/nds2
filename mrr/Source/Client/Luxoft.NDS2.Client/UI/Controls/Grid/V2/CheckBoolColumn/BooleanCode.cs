﻿
namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.CheckBoolColumn
{
    public class BooleanCode
    {
        public int Index
        {
            get;
            set;
        }

        public int Code
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }
    }
}
