﻿using System;
using System.Drawing;
using Infragistics.Win.UltraWinGrid;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns
{
    [Serializable]
    public sealed class ColumnStyleDefinition
    {
        public ColumnStyle Style { get; set; }

        public Image Image { get; set; }

        public CellEventHandler Handler { get; set; }

        public ColumnStyleDefinition()
        {
            Style = ColumnStyle.Edit;
        }
    }    
}
