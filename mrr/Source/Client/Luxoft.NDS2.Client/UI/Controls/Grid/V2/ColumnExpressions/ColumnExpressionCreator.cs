﻿using Luxoft.NDS2.Client.UI.Declarations.DeclarationCard.Models;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.ColumnExpressions
{
    public class ColumnExpressionCreator
    {
        public static IColumnExpression CreateColumnExpressionDefault()
        {
            return new ColumnExpressionDefault();
        }

        public static IColumnExpression CreateColumnExpressionIsAdditionalSheet(DeclarationModelChapterDataExtractor context)
        {
            return new ColumnExpressionIsAdditionalSheet(context);
        }

        public static IColumnExpression CreateColumnInvertedExpression()
        {
            return new ColumnExpressionInverted();
        }

        public static IColumnExpression ColumnExpressionChangeMapping(string fieldNameInsearchCriteria)
        {
            return new ColumnExpressionChangeMapping(fieldNameInsearchCriteria);
        }

        public static IColumnExpression ColumnExpressionTaxPeriod()
        {
            return new ColumnExpressionTaxPeriod();
        }
    }
}
