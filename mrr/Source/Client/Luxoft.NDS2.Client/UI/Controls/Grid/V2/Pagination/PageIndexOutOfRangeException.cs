﻿using System;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination
{
    public class PageIndexOutOfRangeException : Exception
    {
        private const string MessageFormat =
            "Значение {0} не является допустимым номеров страницы. Доступные номера страниц {1} из {2}";

        public PageIndexOutOfRangeException(
            uint pageIndex,
            uint lastPageIndex)
            : base(string.Format(MessageFormat, pageIndex, Defaults.FirstPageIndex, lastPageIndex))
        {
            PageIndex = pageIndex;
            MaxPageIndex = lastPageIndex;
        }

        public uint PageIndex
        {
            get;
            private set;
        }

        public uint MinPageIndex
        {
            get
            {
                return Defaults.FirstPageIndex;
            }
        }

        public uint MaxPageIndex
        {
            get;
            private set;
        }
    }
}
