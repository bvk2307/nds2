﻿using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.UI.Base;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2
{
    public interface IGrid
    {
        UltraGrid Grid { get; }

        event ParameterlessEventHandler AfterDataSourceChanged;
    }
}
