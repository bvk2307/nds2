﻿using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.CustomSettings;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2
{
    public interface IDataGridView : IGridView
    {
        ProgressIndicator ProgressIndicator { get; }

        # region Пользовательские настройки

        IDataGridViewManager SettingsManager();
        IDataGridViewManager DefaultSettingsManager();

        bool AllowResetSettings
        {
            get;
            set;
        }

        bool AllowFilterReset
        {
            get;
            set;
        }

        event ParameterlessEventHandler SaveSettings;

        event ParameterlessEventHandler ResetSettings;

        event ParameterlessEventHandler FilterResetSettings;

        # endregion

        # region Сортировка/группировка

        void LockEvents();

        void UnlockEvents();

        event GenericEventHandler<SortedColumnsCollection> BeforeSortingChanged;

        event GenericEventHandler<Infragistics.Win.UltraWinGrid.ColumnFilter> BeforeFilterChanged;

        event ParameterlessEventHandler AfterSortingChanged;

        event ParameterlessEventHandler AfterFilterChanged;

        event GenericEventHandler<string, bool> ColumnVisibilityChanged;

        # endregion

        ColumnFiltersCollection ColumnFilters { get; }
    }
}
