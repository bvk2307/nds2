﻿namespace Luxoft.NDS2.Client.UI.Controls.DataGrid.Columns.IndexColumn
{
    public interface IPager
    {
        ushort PageSize
        {
            get;
        }

        uint PageIndex
        {
            get;
        }
    }
}
