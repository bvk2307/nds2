﻿using Infragistics.Win;
using grid = Infragistics.Win.UltraWinGrid;
using System.Linq;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns.AutoComplete;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.CheckBoolColumn
{
    public class BooleanAutoCompleteOptionsBuilder : IColumnFilterManager
    {
        private readonly DictionaryBoolean _dictionary;

        public BooleanAutoCompleteOptionsBuilder(DictionaryBoolean dictionary)
        {
            _dictionary = dictionary;
        }

        public void AutoComplete(ValueList initialList)
        {
            var allItem = grid.Resources.Customizer.GetCustomizedString(Constants.AllItemKey);
            var itemsToDelete = 
                initialList
                    .ValueListItems
                    .WhereItem(item => item.DisplayText != allItem)
                    .ToArray();

            foreach (var item in itemsToDelete)
            {
                initialList.ValueListItems.Remove(item);
            }

            foreach (var boolItem in _dictionary.Items)
            {
                initialList.ValueListItems.Add(new ValueListItem(boolItem.Code, boolItem.Description));
            }
        }

        public void OnFilterChanged(grid.ColumnFilter gridFilter)
        {
        }
    }
}
