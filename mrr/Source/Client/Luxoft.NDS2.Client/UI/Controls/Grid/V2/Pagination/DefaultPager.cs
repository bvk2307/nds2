﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Luxoft.NDS2.Client.UI.Base;
using Infragistics.Win;

namespace Luxoft.NDS2.Client.UI.Controls.DataGrid.Pagination
{
    public partial class DefaultPager : UserControl, IPagerView
    {
        private const string PageIndexTextFormat = "Страница {0} из {1}";

        private uint _pageIndex = Defaults.FirstPageIndex;

        private ushort _pageSize = Defaults.DefaulPageSize;

        private uint? _totalMatches;

        public DefaultPager()
        {
            InitializeComponent();

            _pageSizeSelect.Items.Clear();
            _pageSizeSelect.Items.AddRange(
                Defaults.AvailablePageSizes.Select(
                    pageSize => new ValueListItem(pageSize)).ToArray());
           
            try
            {
                _pageSizeSelect.Text = _pageSize.ToString();
            }
            catch (Exception ex)
            {
            }
            SetControlsAvailability();
        }

        private void OnPrevPageClick(object sender, EventArgs e)
        {
            if (CurrentPageIndex == Defaults.FirstPageIndex)
            {
                return;
            }

            CurrentPageIndex--;
        }

        private void OnNextPageClick(object sender, EventArgs e)
        {
            if (CurrentPageIndex == PageQuantity())
            {
                return;
            }

            CurrentPageIndex++;
        }

        private void OnPageSizeChanged(object sender, EventArgs e)
        {
            if (_pageSizeSelect.Value == null)
            {
                throw new InvalidPageSizeException(null);
            }

            ushort newPageSize;

            if (!ushort.TryParse(_pageSizeSelect.Value.ToString(), out newPageSize))
            {
                throw new InvalidPageSizeException(_pageSizeSelect.Value.ToString());
            }

            try
            {
                CurrentPageSize = newPageSize;
            }
            catch (PageSizeOutOfRangeException error)
            {
                _pageSizeSelect.Value = CurrentPageSize;

                if (PageSizeInputError != null)
                {
                    PageSizeInputError(error.Message);
                }
            }
        }

        public uint CurrentPageIndex
        {
            get
            {
                return _pageIndex;
            }
            set
            {
                if (_pageIndex == value)
                {
                    return;
                }

                if (!_totalMatches.HasValue && value > Defaults.FirstPageIndex)
                {
                    throw new PageQuantityNotDefinedException();
                }

                if (value < Defaults.FirstPageIndex || value > PageQuantity())
                {
                    throw new PageIndexOutOfRangeException(value, PageQuantity());
                }

                _pageIndex = value;
                SetControlsAvailability();
                SetPageLabel();
                RaiseIndexChanged();
            }
        }

        public ushort CurrentPageSize
        {
            get
            {
                return _pageSize;
            }
            set
            {
                if (_pageSize == value)
                {
                    return;
                }

                if (value < Defaults.MinPageSize || value > Defaults.MaxPageSize)
                {
                    throw new PageSizeOutOfRangeException(value);
                }

                _pageSize= value;
                _pageIndex = Defaults.FirstPageIndex;
                _pageSizeSelect.Text = _pageSize.ToString();
                SetControlsAvailability();
                SetPageLabel();
                RaiseSizeChanged();
            }
        }

        public uint TotalDataRows
        {
            get;
            set;
        }

        public uint TotalMatches
        {
            get
            {
                return _totalMatches.HasValue ? _totalMatches.Value : 0;
            }
            set
            {
                if (_totalMatches.HasValue && _totalMatches.Value == value)
                {
                    return;
                }

                this.ExecuteInUiThread((control) => control.SetTotalMatches(value));
            }
        }

        private void SetTotalMatches(uint value)
        {

            _totalMatches = value;
            SetPageLabel();
            SetControlsAvailability();
        }

        public event ParameterlessEventHandler PageIndexChanged;

        public event ParameterlessEventHandler PageSizeChanged;

        public event GenericEventHandler<string> PageSizeInputError;

        private void SetPageLabel()
        {
            _pageIndexLabel.Text = string.Format(PageIndexTextFormat, _pageIndex, PageQuantity());
        }

        private void SetControlsAvailability()
        {
            _prevPageButton.Enabled = _pageIndex > Defaults.FirstPageIndex;
            _nextPageButton.Enabled = _pageIndex < PageQuantity();
        }

        private uint PageQuantity()
        {
            if (_totalMatches.HasValue)
            {
                return (uint)_totalMatches.Value / _pageSize + 1;
            }

            return 1;
        }

        private void RaiseIndexChanged()
        {
            if (PageIndexChanged != null)
            {
                PageIndexChanged();
            }
        }

        private void RaiseSizeChanged()
        {
            if (PageSizeChanged != null)
            {
                PageSizeChanged();
            }
        }
    }
}
