﻿using Infragistics.Win.UltraWinToolTip;
namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2
{
    partial class DataGridView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            this.pnlRoot = new Infragistics.Win.Misc.UltraPanel();
            this.btnExportExcelCancel = new Infragistics.Win.Misc.UltraButton();
            this.btnExportExcel = new Infragistics.Win.Misc.UltraButton();
            this.btnFilterReset = new Infragistics.Win.Misc.UltraButton();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this._resetSettingButton = new Infragistics.Win.Misc.UltraButton();
            this.btnColumnVisibilitySetup = new Infragistics.Win.Misc.UltraDropDownButton();
            this.popupColumnContainer = new Infragistics.Win.Misc.UltraPopupControlContainer(this.components);
            this.columnVisibilityManager = new Infragistics.Win.UltraWinListView.UltraListView();
            this.popupControlContainerGrouping = new Infragistics.Win.Misc.UltraPopupControlContainer(this.components);
            this.pnlGridBottom = new Infragistics.Win.Misc.UltraPanel();
            this.pnlLoading = new Infragistics.Win.Misc.UltraPanel();
            this.ProgressGridData = new System.Windows.Forms.ProgressBar();
            this.ultraPanelExportExcelState = new Infragistics.Win.Misc.UltraPanel();
            this.labelExportExcelState = new Infragistics.Win.Misc.UltraLabel();
            this._pagerPanel = new Infragistics.Win.Misc.UltraPanel();
            this.gridLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.lblGridName = new Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel();
            this.ultraToolTipManager1 = new Infragistics.Win.UltraWinToolTip.UltraToolTipManager(this.components);
            this.pnlMain = new System.Windows.Forms.Panel();
            this.gridExporter = new Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter(this.components);
            this._grid = new Luxoft.NDS2.Client.UI.Controls.Grid.V2.GridView();
            this.pnlRoot.ClientArea.SuspendLayout();
            this.pnlRoot.SuspendLayout();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.columnVisibilityManager)).BeginInit();
            this.pnlGridBottom.ClientArea.SuspendLayout();
            this.pnlGridBottom.SuspendLayout();
            this.pnlLoading.ClientArea.SuspendLayout();
            this.pnlLoading.SuspendLayout();
            this.ultraPanelExportExcelState.ClientArea.SuspendLayout();
            this.ultraPanelExportExcelState.SuspendLayout();
            this._pagerPanel.SuspendLayout();
            this.gridLayoutPanel.SuspendLayout();
            this.pnlMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlRoot
            // 
            // 
            // pnlRoot.ClientArea
            // 
            this.pnlRoot.ClientArea.Controls.Add(this.btnExportExcelCancel);
            this.pnlRoot.ClientArea.Controls.Add(this.btnExportExcel);
            this.pnlRoot.ClientArea.Controls.Add(this.btnFilterReset);
            this.pnlRoot.ClientArea.Controls.Add(this.ultraPanel1);
            this.pnlRoot.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlRoot.Location = new System.Drawing.Point(486, 0);
            this.pnlRoot.Margin = new System.Windows.Forms.Padding(0);
            this.pnlRoot.Name = "pnlRoot";
            this.pnlRoot.Size = new System.Drawing.Size(172, 29);
            this.pnlRoot.TabIndex = 0;
            // 
            // btnExportExcelCancel
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.BackColor2 = System.Drawing.Color.Transparent;
            appearance4.ForeColor = System.Drawing.Color.Transparent;
            appearance4.ForeColorDisabled = System.Drawing.Color.Transparent;
            appearance4.Image = global::Luxoft.NDS2.Client.Properties.Resources.reset;
            appearance4.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched;
            appearance4.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance4.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this.btnExportExcelCancel.Appearance = appearance4;
            this.btnExportExcelCancel.ButtonStyle = Infragistics.Win.UIElementButtonStyle.ButtonSoft;
            this.btnExportExcelCancel.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnExportExcelCancel.Location = new System.Drawing.Point(4, 0);
            this.btnExportExcelCancel.Margin = new System.Windows.Forms.Padding(7);
            this.btnExportExcelCancel.Name = "btnExportExcelCancel";
            this.btnExportExcelCancel.Size = new System.Drawing.Size(32, 29);
            this.btnExportExcelCancel.TabIndex = 6;
            this.btnExportExcelCancel.Visible = false;
            this.btnExportExcelCancel.Click += new System.EventHandler(this.btnExportExcelCancel_Click);
            // 
            // btnExportExcel
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.BackColor2 = System.Drawing.Color.Transparent;
            appearance5.ForeColor = System.Drawing.Color.Transparent;
            appearance5.ForeColorDisabled = System.Drawing.Color.Transparent;
            appearance5.Image = global::Luxoft.NDS2.Client.Properties.Resources.excel_create;
            appearance5.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched;
            appearance5.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance5.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this.btnExportExcel.Appearance = appearance5;
            this.btnExportExcel.ButtonStyle = Infragistics.Win.UIElementButtonStyle.ButtonSoft;
            this.btnExportExcel.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnExportExcel.Location = new System.Drawing.Point(36, 0);
            this.btnExportExcel.Margin = new System.Windows.Forms.Padding(7);
            this.btnExportExcel.Name = "btnExportExcel";
            this.btnExportExcel.Size = new System.Drawing.Size(32, 29);
            this.btnExportExcel.TabIndex = 5;
            this.btnExportExcel.Visible = false;
            this.btnExportExcel.Click += new System.EventHandler(this.btnExportExcel_Click);
            // 
            // btnFilterReset
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.BackColor2 = System.Drawing.Color.Transparent;
            appearance1.ForeColor = System.Drawing.Color.Transparent;
            appearance1.ForeColorDisabled = System.Drawing.Color.Transparent;
            appearance1.Image = global::Luxoft.NDS2.Client.Properties.Resources.filter;
            appearance1.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched;
            appearance1.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance1.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this.btnFilterReset.Appearance = appearance1;
            this.btnFilterReset.ButtonStyle = Infragistics.Win.UIElementButtonStyle.ButtonSoft;
            this.btnFilterReset.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnFilterReset.Enabled = false;
            this.btnFilterReset.Location = new System.Drawing.Point(68, 0);
            this.btnFilterReset.Margin = new System.Windows.Forms.Padding(7);
            this.btnFilterReset.Name = "btnFilterReset";
            this.btnFilterReset.Size = new System.Drawing.Size(32, 29);
            this.btnFilterReset.TabIndex = 7;
            this.btnFilterReset.Visible = false;
            this.btnFilterReset.Click += new System.EventHandler(this.btnFilterReset_Click);
            // 
            // ultraPanel1
            // 
            this.ultraPanel1.AutoSize = true;
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this._resetSettingButton);
            this.ultraPanel1.ClientArea.Controls.Add(this.btnColumnVisibilitySetup);
            this.ultraPanel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.ultraPanel1.Location = new System.Drawing.Point(100, 0);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(72, 29);
            this.ultraPanel1.TabIndex = 1;
            // 
            // _resetSettingButton
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.BackColor2 = System.Drawing.Color.Transparent;
            appearance3.ForeColor = System.Drawing.Color.Transparent;
            appearance3.ForeColorDisabled = System.Drawing.Color.Transparent;
            appearance3.Image = global::Luxoft.NDS2.Client.Properties.Resources.edit_clear;
            appearance3.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched;
            appearance3.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance3.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this._resetSettingButton.Appearance = appearance3;
            this._resetSettingButton.ButtonStyle = Infragistics.Win.UIElementButtonStyle.ButtonSoft;
            this._resetSettingButton.Dock = System.Windows.Forms.DockStyle.Right;
            this._resetSettingButton.Enabled = false;
            this._resetSettingButton.Location = new System.Drawing.Point(0, 0);
            this._resetSettingButton.Margin = new System.Windows.Forms.Padding(7);
            this._resetSettingButton.Name = "_resetSettingButton";
            this._resetSettingButton.Size = new System.Drawing.Size(32, 29);
            this._resetSettingButton.TabIndex = 3;
            this._resetSettingButton.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnColumnVisibilitySetup
            // 
            appearance8.Image = global::Luxoft.NDS2.Client.Properties.Resources.table_select_column;
            appearance8.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance8.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this.btnColumnVisibilitySetup.Appearance = appearance8;
            this.btnColumnVisibilitySetup.CausesValidation = false;
            this.btnColumnVisibilitySetup.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnColumnVisibilitySetup.Location = new System.Drawing.Point(32, 0);
            this.btnColumnVisibilitySetup.Name = "btnColumnVisibilitySetup";
            this.btnColumnVisibilitySetup.PopupItemKey = "columnVisibilityManager";
            this.btnColumnVisibilitySetup.PopupItemProvider = this.popupColumnContainer;
            this.btnColumnVisibilitySetup.Size = new System.Drawing.Size(40, 29);
            this.btnColumnVisibilitySetup.Style = Infragistics.Win.Misc.SplitButtonDisplayStyle.DropDownButtonOnly;
            this.btnColumnVisibilitySetup.TabIndex = 4;
            this.btnColumnVisibilitySetup.TabStop = false;
            this.btnColumnVisibilitySetup.DroppingDown += new System.ComponentModel.CancelEventHandler(this.btnColumnVisibilitySetup_DroppingDown);
            // 
            // popupColumnContainer
            // 
            this.popupColumnContainer.PopupControl = this.columnVisibilityManager;
            // 
            // columnVisibilityManager
            // 
            this.columnVisibilityManager.BorderStyle = Infragistics.Win.UIElementBorderStyle.Rounded4;
            this.columnVisibilityManager.CausesValidation = false;
            this.columnVisibilityManager.ItemSettings.AllowEdit = Infragistics.Win.DefaultableBoolean.False;
            this.columnVisibilityManager.Location = new System.Drawing.Point(401, 0);
            this.columnVisibilityManager.MainColumn.AllowSizing = Infragistics.Win.DefaultableBoolean.True;
            this.columnVisibilityManager.MainColumn.AutoSizeMode = Infragistics.Win.UltraWinListView.ColumnAutoSizeMode.AllItems;
            this.columnVisibilityManager.Name = "columnVisibilityManager";
            this.columnVisibilityManager.Size = new System.Drawing.Size(217, 247);
            this.columnVisibilityManager.TabIndex = 5;
            this.columnVisibilityManager.TabStop = false;
            this.columnVisibilityManager.View = Infragistics.Win.UltraWinListView.UltraListViewStyle.List;
            this.columnVisibilityManager.ViewSettingsList.CheckBoxStyle = Infragistics.Win.UltraWinListView.CheckBoxStyle.CheckBox;
            this.columnVisibilityManager.ViewSettingsList.ImageSize = new System.Drawing.Size(0, 0);
            this.columnVisibilityManager.ViewSettingsList.MultiColumn = false;
            this.columnVisibilityManager.Visible = false;
            this.columnVisibilityManager.ItemCheckStateChanged += new Infragistics.Win.UltraWinListView.ItemCheckStateChangedEventHandler(this.columnVisibilityManager_ItemCheckStateChanged);
            // 
            // pnlGridBottom
            // 
            this.pnlGridBottom.AutoSize = true;
            this.pnlGridBottom.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            // 
            // pnlGridBottom.ClientArea
            // 
            this.pnlGridBottom.ClientArea.Controls.Add(this.pnlLoading);
            this.pnlGridBottom.ClientArea.Controls.Add(this.ultraPanelExportExcelState);
            this.pnlGridBottom.ClientArea.Controls.Add(this._pagerPanel);
            this.gridLayoutPanel.SetColumnSpan(this.pnlGridBottom, 3);
            this.pnlGridBottom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlGridBottom.Location = new System.Drawing.Point(0, 371);
            this.pnlGridBottom.Margin = new System.Windows.Forms.Padding(0);
            this.pnlGridBottom.Name = "pnlGridBottom";
            this.pnlGridBottom.Size = new System.Drawing.Size(658, 25);
            this.pnlGridBottom.TabIndex = 2;
            // 
            // pnlLoading
            // 
            this.pnlLoading.AutoSize = true;
            // 
            // pnlLoading.ClientArea
            // 
            this.pnlLoading.ClientArea.Controls.Add(this.columnVisibilityManager);
            this.pnlLoading.ClientArea.Controls.Add(this.ProgressGridData);
            this.pnlLoading.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlLoading.Location = new System.Drawing.Point(0, 0);
            this.pnlLoading.Name = "pnlLoading";
            this.pnlLoading.Size = new System.Drawing.Size(658, 25);
            this.pnlLoading.TabIndex = 1;
            // 
            // ProgressGridData
            // 
            this.ProgressGridData.Dock = System.Windows.Forms.DockStyle.Right;
            this.ProgressGridData.Location = new System.Drawing.Point(517, 0);
            this.ProgressGridData.Name = "ProgressGridData";
            this.ProgressGridData.Size = new System.Drawing.Size(141, 25);
            this.ProgressGridData.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.ProgressGridData.TabIndex = 0;
            // 
            // ultraPanelExportExcelState
            // 
            this.ultraPanelExportExcelState.AutoSize = true;
            // 
            // ultraPanelExportExcelState.ClientArea
            // 
            this.ultraPanelExportExcelState.ClientArea.Controls.Add(this.labelExportExcelState);
            this.ultraPanelExportExcelState.Dock = System.Windows.Forms.DockStyle.Left;
            this.ultraPanelExportExcelState.Location = new System.Drawing.Point(0, 0);
            this.ultraPanelExportExcelState.Name = "ultraPanelExportExcelState";
            this.ultraPanelExportExcelState.Size = new System.Drawing.Size(0, 25);
            this.ultraPanelExportExcelState.TabIndex = 6;
            this.ultraPanelExportExcelState.Visible = false;
            // 
            // labelExportExcelState
            // 
            appearance2.TextHAlignAsString = "Right";
            appearance2.TextVAlignAsString = "Middle";
            this.labelExportExcelState.Appearance = appearance2;
            this.labelExportExcelState.AutoSize = true;
            this.labelExportExcelState.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelExportExcelState.Location = new System.Drawing.Point(0, 0);
            this.labelExportExcelState.Name = "labelExportExcelState";
            this.labelExportExcelState.Size = new System.Drawing.Size(0, 25);
            this.labelExportExcelState.TabIndex = 8;
            // 
            // _pagerPanel
            // 
            this._pagerPanel.AutoSize = true;
            this._pagerPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this._pagerPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this._pagerPanel.Location = new System.Drawing.Point(0, 0);
            this._pagerPanel.Name = "_pagerPanel";
            this._pagerPanel.Size = new System.Drawing.Size(0, 25);
            this._pagerPanel.TabIndex = 0;
            // 
            // gridLayoutPanel
            // 
            this.gridLayoutPanel.ColumnCount = 3;
            this.gridLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 206F));
            this.gridLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.gridLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 172F));
            this.gridLayoutPanel.Controls.Add(this._grid, 0, 1);
            this.gridLayoutPanel.Controls.Add(this.pnlGridBottom, 0, 2);
            this.gridLayoutPanel.Controls.Add(this.pnlRoot, 2, 0);
            this.gridLayoutPanel.Controls.Add(this.lblGridName, 0, 0);
            this.gridLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.gridLayoutPanel.Name = "gridLayoutPanel";
            this.gridLayoutPanel.RowCount = 4;
            this.gridLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.gridLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.gridLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.gridLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 9F));
            this.gridLayoutPanel.Size = new System.Drawing.Size(658, 405);
            this.gridLayoutPanel.TabIndex = 4;
            // 
            // lblGridName
            // 
            appearance7.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            appearance7.FontData.BoldAsString = "True";
            appearance7.TextVAlignAsString = "Middle";
            this.lblGridName.Appearance = appearance7;
            this.lblGridName.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this.gridLayoutPanel.SetColumnSpan(this.lblGridName, 2);
            this.lblGridName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblGridName.Location = new System.Drawing.Point(3, 3);
            this.lblGridName.Name = "lblGridName";
            this.lblGridName.Size = new System.Drawing.Size(480, 26);
            this.lblGridName.TabIndex = 3;
            this.lblGridName.TabStop = true;
            this.lblGridName.Value = "";
            // 
            // ultraToolTipManager1
            // 
            this.ultraToolTipManager1.ContainingControl = this;
            this.ultraToolTipManager1.DisplayStyle = Infragistics.Win.ToolTipDisplayStyle.Standard;
            // 
            // pnlMain
            // 
            this.pnlMain.Controls.Add(this.gridLayoutPanel);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(0, 0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(658, 405);
            this.pnlMain.TabIndex = 5;
            // 
            // _grid
            // 
            this._grid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridLayoutPanel.SetColumnSpan(this._grid, 3);
            this._grid.GridContextMenuStrip = null;
            this._grid.Location = new System.Drawing.Point(0, 32);
            this._grid.Margin = new System.Windows.Forms.Padding(0);
            this._grid.Name = "_grid";
            this._grid.Size = new System.Drawing.Size(658, 339);
            this._grid.TabIndex = 1;
            // 
            // DataGridView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.pnlMain);
            this.Name = "DataGridView";
            this.Size = new System.Drawing.Size(658, 405);
            this.pnlRoot.ClientArea.ResumeLayout(false);
            this.pnlRoot.ClientArea.PerformLayout();
            this.pnlRoot.ResumeLayout(false);
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            this.ultraPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.columnVisibilityManager)).EndInit();
            this.pnlGridBottom.ClientArea.ResumeLayout(false);
            this.pnlGridBottom.ClientArea.PerformLayout();
            this.pnlGridBottom.ResumeLayout(false);
            this.pnlGridBottom.PerformLayout();
            this.pnlLoading.ClientArea.ResumeLayout(false);
            this.pnlLoading.ResumeLayout(false);
            this.pnlLoading.PerformLayout();
            this.ultraPanelExportExcelState.ClientArea.ResumeLayout(false);
            this.ultraPanelExportExcelState.ClientArea.PerformLayout();
            this.ultraPanelExportExcelState.ResumeLayout(false);
            this.ultraPanelExportExcelState.PerformLayout();
            this._pagerPanel.ResumeLayout(false);
            this._pagerPanel.PerformLayout();
            this.gridLayoutPanel.ResumeLayout(false);
            this.gridLayoutPanel.PerformLayout();
            this.pnlMain.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraPanel pnlRoot;
        private Infragistics.Win.Misc.UltraPanel pnlGridBottom;
        private GridView _grid;
        private System.Windows.Forms.TableLayoutPanel gridLayoutPanel;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.Misc.UltraButton _resetSettingButton;
        private Infragistics.Win.Misc.UltraDropDownButton btnColumnVisibilitySetup;
        private Infragistics.Win.Misc.UltraPopupControlContainer popupColumnContainer;
        private Infragistics.Win.Misc.UltraPopupControlContainer popupControlContainerGrouping;
        private Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel lblGridName;
        private Infragistics.Win.UltraWinToolTip.UltraToolTipManager ultraToolTipManager1;
        private Infragistics.Win.UltraWinListView.UltraListView columnVisibilityManager;
        private Infragistics.Win.Misc.UltraPanel pnlLoading;
        private System.Windows.Forms.ProgressBar ProgressGridData;
        private Infragistics.Win.Misc.UltraPanel _pagerPanel;
        private System.Windows.Forms.Panel pnlMain;
        private Infragistics.Win.Misc.UltraButton btnExportExcel;
        private Infragistics.Win.Misc.UltraButton btnExportExcelCancel;
        private Infragistics.Win.Misc.UltraPanel ultraPanelExportExcelState;
        private Infragistics.Win.Misc.UltraLabel labelExportExcelState;
        private Infragistics.Win.Misc.UltraButton btnFilterReset;
        private Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter gridExporter;

    }
}
