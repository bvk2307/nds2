﻿using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.Extensions;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Helpers;
using Luxoft.NDS2.Common.Contracts.DTO.Business.TaxPeriod;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using CommonComponents.Utils;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.CustomSettings
{
    public delegate void SettingsChanged(bool allowRestore, bool allowSave);
    /// <summary>
    /// Этот класс представляет набор настроек отображения данных в списке, сделанных пользователем, а так же реализует операции с ними.
    /// </summary>
    public class UserDefinedSettingsManager : IDataGridViewManager
    {
        # region Константы

        private const string SettingsKey = "column_visibility";
        private const bool UserDefinedTrue = true;

        # endregion

        # region Конструктор

        private UltraGridBand _band;
        private GridColumnSetup _columns;
        private QueryConditions _filter;
        private ISettingsProvider _settingsProvider;
        private SettingsWrapper<SettingsContainer> _lastSavedSettings;
        private bool _allowRestoreFilterSettings;

		private CustomFilterBuilder _filterBuilder;
		private FilterComparer _filterComparer;

        /// <summary>
        /// Создает экземпляр класса UserDefinedSettingsManager
        /// </summary>
        /// <param name="band">Ссылка на Band таблицы, к которой применяются настройки</param>
        /// <param name="gridSetup">Ссылка на набор настроект таблицы</param>
        public UserDefinedSettingsManager(
            UltraGridBand band,
            GridColumnSetup columns)
        {
            _band = band;
            _columns = columns;
            _filterComparer = new FilterComparer(band, columns);
            _allowRestoreFilterSettings = true;
        }

        public IDataGridViewManager WithFilter(QueryConditions filter)
        {
            _filter = filter;

            if (_filter != null)
            {
                _defaultSort =
                    _filter.Sorting.Select(sort => (ColumnSort)sort.Clone()).ToList();
                _filterBuilder = new CustomFilterBuilder(_filter.Filter);
                _sortBuilder = new CustomSortBuilder(_filter.Sorting);
            }

            return this;
        }

        public IDataGridViewManager WithSettingsProvider(ISettingsProvider settingsProvider)
        {
            _settingsProvider = settingsProvider;
            _lastSavedSettings = new SettingsWrapper<SettingsContainer>(
                SettingsKey,
                _settingsProvider);

            return this;
        }

        public IDataGridViewManager WithInitialFilter(DefaultFilter defaultFilter)
        {
            _filterBuilder.Default = defaultFilter;

            return this;
        }

        public IDataGridViewManager WithRestoreFilters(bool val)
        {
            _allowRestoreFilterSettings = val;

            return this;
        }

        # endregion

        # region Видимость колонок

        public void SetColumnVisibility(string key, bool visible)
        {
            var column = Column(key);

            if (visible)
            {
                Show(column);
            }
            else
            {
                Hide(column);
            }
        }

        private void Hide(UltraGridColumn column)
        {
            column.Hidden = true;
            Hide(Parent(column));
        }

        private bool CanHideGroup(UltraGridGroup group)
        {
            if (group == null)
                return false;
            foreach (var column in _band.Columns)
            {
                if (column.Hidden)
                    continue;
                var parent = Parent(column);
                while (parent != null)
                {
                    if (Equals(parent, group))
                        return false;
                    parent = Parent(parent);
                }
            }
            return true;
        }

        public void Hide(UltraGridGroup group)
        {
            if (!CanHideGroup(group))
                return;

            group.Hidden = true;
            Hide(Parent(group));
        }



        private void Show(UltraGridColumn column)
        {
            var parent = Parent(column);
            Show(parent);
            column.Hidden = false;
            _band.Columns.FixLayout();
        }

        private void Show(UltraGridGroup group)
        {
            if (group == null)
            {
                return;
            }

            Show(Parent(group));
            group.Hidden = false;
        }

        private List<string> BuildColumnsVisibility()
        {
            return
                _band
                    .Columns
                    .All
                    .Where(
                        col =>
                            ((UltraGridColumn)col).Hidden
                            && _columns.Columns.Find(((UltraGridColumn)col).Key) != null)
                    .Select(col => ((UltraGridColumn)col).Key)
                    .ToList();
        }

        private bool RestoreColumnVisibility()
        {
            if (_lastSavedSettings.Status != SettingLoadState.Loaded)
                return false;

            foreach (var column in _band.Columns)
            {
                if (_lastSavedSettings.Data.HiddenColumns.Contains(column.Key))
                    Hide(column);
                else if (_columns.Contains(column.Key) && !_columns.Columns.Find(column.Key).Hidden)
                    Show(column);
            }

            return true;
        }

        public void InitColumnVisibility()
        {
            ResetColumnsVisibility();
        }

        private void ResetColumnsVisibility()
        {
            foreach (var item in _band.Columns.All)
            {
                var column = (UltraGridColumn)item;
                var definition = _columns.Columns.Find(column.Key) as SingleColumn;

                if (definition != null)
                {
                    column.Hidden = definition.Hidden;
                }

                var group = column.Group;
                while (group != null)
                {
                    var allColumnsHidden = group.Columns.All(col => col.Hidden);
                    var allSubgroupsHidden = group.Band.Groups.All.Cast<UltraGridGroup>()
                        .Where(grp => grp.RowLayoutGroupInfo.ParentGroup == group)
                        .All(grp => grp.Hidden);

                    group.Hidden = allColumnsHidden && allSubgroupsHidden;
                    group = group.RowLayoutGroupInfo.ParentGroup;
                }
            }
            _band.Columns.FixLayout();
        }

        private bool AllowResetColumnVisibility()
        {
            var allowReset = false;

            if (_columns != null)
            {
                foreach (var item in _band.Columns.All)
                {
                    var column = (UltraGridColumn)item;
                    var definition = _columns.Columns.FindSingle(column.Key);

                    if (definition != null)
                    {
                        allowReset = allowReset || (column.Hidden != definition.Hidden);
                    }
                }
            }

            return allowReset;
        }

        private bool AllowSaveColumnVisibility()
        {
            if (_lastSavedSettings.Status == SettingLoadState.FailedToLoad)
            {
                return AllowResetColumnVisibility();
            }

            var isChanged = false;

            foreach (var item in _band.Columns.All)
            {
                var column = (UltraGridColumn)item;

                if (_columns.Columns.FindSingle(column.Key) != null)
                {
                    isChanged =
                        isChanged
                        || (column.Hidden != _lastSavedSettings.Data.HiddenColumns.Contains(column.Key));
                }
            }

            return isChanged;
        }

        # endregion

        # region Фильтрация данных

        public void ApplyFilter(Infragistics.Win.UltraWinGrid.ColumnFilter e)
        {
            _filterBuilder.ApplyFilter(e);
        }

        private bool AllowSaveFilter()
        {
            if (_lastSavedSettings.Status == SettingLoadState.FailedToLoad)
            {
                return AllowResetFilter();
            }

            return !_filterComparer.Compare(_lastSavedSettings.Data.ColumnFilters);
        }

        private bool AllowResetFilter()
        {
            return !_filterComparer.Compare(_filterBuilder.Default.AllConditions());
        }

        private List<GridColumnFilter> BuildFilter()
        {
            var newFilter = new List<GridColumnFilter>();

            foreach (var itemI in _band.ColumnFilters.All)
            {
                var filter = (Infragistics.Win.UltraWinGrid.ColumnFilter)itemI;

                if (filter.FilterConditions.Count > 0)
                {
                    var columnFilter =
                        new GridColumnFilter()
                        {
                            ColumnKey = filter.Column.Key,
                            Conditions = new List<FilterCondition>()
                        };
                    newFilter.Add(columnFilter);
                    foreach (var itemJ in filter.FilterConditions.All)
                    {
                        var condition = (FilterCondition)itemJ;
                        columnFilter.Conditions.Add(condition);
                    }
                }
            }

            return newFilter;
        }

        private bool RestoreFilter()
        {
            if (_lastSavedSettings.Status == SettingLoadState.Loaded)
            {
                var allowRestoreFilter = new List<GridColumnFilter>();
                foreach (var item in _lastSavedSettings.Data.ColumnFilters)
                {
                    if (_columns.Columns.Contains(item.ColumnKey))
                        allowRestoreFilter.Add(item);
                }

                _band.ColumnFilters.ClearAllFilters();
                allowRestoreFilter
                    .ForEach(
                        columnFilter =>
                        {
                            columnFilter
                                .Conditions
                                .ForEach(
                                        condition =>
                                            _band
                                                .ColumnFilters[columnFilter.ColumnKey]
                                                .FilterConditions
                                                .Add(condition));
                            if (
                                _band
                                    .ColumnFilters[columnFilter.ColumnKey]
                                    .FilterConditions.Count > 0)
                            {
                                _filterBuilder.ApplyUserFilter(_band.ColumnFilters[columnFilter.ColumnKey]);
                            }
                        });

                return true;
            }

            return false;
        }

        private void ResetFilter()
        {
            _filterBuilder.Reset(_band);
        }

        # endregion

        # region Сортировка данных

        private List<ColumnSort> _defaultSort;

        private CustomSortBuilder _sortBuilder;

        public void SortChanged(SortedColumnsCollection sortedColumns)
        {
            _sortBuilder.SortChanged(sortedColumns);
        }

        private void ResetSorting()
        {
            _sortBuilder.ApplySorting(_defaultSort, _band);
        }

        private void RestoreSorting()
        {
            var allowRestoreSorting = new List<ColumnSort>();
            foreach(var item in _lastSavedSettings.Data.Sorting)
            {
                if (_columns.Columns.Contains(item.ColumnKey))
                    allowRestoreSorting.Add(item);
            }

            _sortBuilder.ApplySorting(allowRestoreSorting, _band);
        }

        private bool CompareSort(List<ColumnSort> compare)
        {
            if (compare == null)
                return false;

            var currentSortList = _sortBuilder.BuildSorting(_band.Columns.All.Select(x => (UltraGridColumn)x));

            if (currentSortList.Count != compare.Count)
            {
                return true;
            }

            var currentSort = currentSortList.ToArray();
            var compareSort = compare.ToArray();

            for (var counter = 0; counter < currentSort.Length; counter++)
            {
                if (currentSort[counter].ColumnKey != compareSort[counter].ColumnKey
                    || currentSort[counter].Order != compareSort[counter].Order)
                {
                    return true;
                }
            }

            return false;
        }

        private bool AllowResetSort()
        {
            return CompareSort(_defaultSort);
        }

        private bool AllowSaveSort()
        {
            return CompareSort(_lastSavedSettings.Data.Sorting);
        }

        # endregion

        # region Сравнение с настройками по умолчанию

        public bool AllowReset()
        {
            return AllowResetColumnVisibility()
                || AllowResetFilter()
                || AllowResetSort();
        }

        public bool AllowSave()
        {
            return (AllowSaveColumnVisibility()
                    || AllowSaveFilter()
                    || AllowSaveSort());
        }

        # endregion

        # region Операции

        public bool Save()
        {
            if (! _lastSavedSettings.SaveSettings) return true;

            var newSettings =
                new SettingsContainer()
                {
                    HiddenColumns = BuildColumnsVisibility(),
                    ColumnFilters = BuildFilter(),
                    Sorting = _sortBuilder.GetSorting(_band.Columns.All.Select(x => (UltraGridColumn)x))//.BuildSorting(_band.Columns.All.Select(x => (UltraGridColumn)x))
                };
            if (_lastSavedSettings.Save(newSettings))
            {
                _settingsProvider.Do(provider=>provider.LogSuccess());
                return true;
            }

            return false;
        }

        public bool Restore()
        {
            if (!_lastSavedSettings.SaveSettings)
            {
                Reset();
                return false;
            }

            _lastSavedSettings.Restore();

            if (_lastSavedSettings.Status != SettingLoadState.FailedToLoad)
            {
                try
                {
                    RestoreColumnVisibility();
                    RestoreSorting();
                    
                    if (_allowRestoreFilterSettings)
                        RestoreFilter();

                    return true;
                }
                catch
                {
                    return false;
                }
            }

            return false;
        }

        public void Reset()
        {
            ResetColumnsVisibility();
            ResetFilter();
            ResetSorting();
        }

        # endregion

        # region Вспомогательные методы

        private UltraGridColumn Column(string columnKey)
        {
            return _band.Columns[columnKey];
        }

        private UltraGridGroup Parent(UltraGridColumn column)
        {
            return column.RowLayoutColumnInfo.ParentGroup;
        }

        private UltraGridGroup Parent(string columnKey)
        {
            return Parent(Column(columnKey));
        }

        private UltraGridGroup Parent(UltraGridGroup group)
        {
            return group.RowLayoutGroupInfo.ParentGroup;
        }

        # endregion
    }

    public enum SettingLoadState { NotLoaded, Loaded, FailedToLoad }

    public class SettingsWrapper<T> where T : new()
    {
        # region Fields

        private T _settings = new T();
        private string _key;
        private ISettingsProvider _settingsProvider;

        # endregion

        # region Constructor

        public SettingsWrapper(
            string key,
            ISettingsProvider settingsProvider)
        {
            _key = key;
            _settingsProvider = settingsProvider;
            Status = SettingLoadState.NotLoaded;
        }

        # endregion

        # region Properties

        public SettingLoadState Status
        {
            get;
            private set;
        }

        public T Data
        {
            get
            {
                if (Status == SettingLoadState.NotLoaded)
                {
                    Restore();
                }

                return _settings;
            }
        }

        /// <summary>
        /// настройки грида сохраняются на диск
        /// </summary>
        public bool SaveSettings
        {
            get { return _settingsProvider != null; }
        }

        # endregion

        # region Methods

        public bool Save(T columns)
        {
            try
            {
                _settingsProvider.Do(provider => provider.SaveSettings(columns.Serialize(), _key));
                _settings = columns;
                Status = SettingLoadState.Loaded;

                return true;
            }
            catch
            {
                return false;
            }
        }

        public void Restore()
        {
            if (_settingsProvider == null)
            {
                _settings = new T();
                Status = SettingLoadState.Loaded;
                return;
            }

            Action setFail = () =>
            {
                _settings = new T();
                Status = SettingLoadState.FailedToLoad;
            };
            try
            {
                var settings = _settingsProvider.LoadSettings(_key).Deserialize<T>();
                if (ReferenceEquals(settings, default(T)))
                {
                    setFail();
                }
                else
                {
                    _settings = settings;
                    Status = SettingLoadState.Loaded;
                }
            }
            catch
            {
                setFail();
            }
        }

        # endregion
    }

    [Serializable]
    [System.Xml.Serialization.XmlInclude(typeof(DBNull))]
    [System.Xml.Serialization.XmlInclude(typeof(FullTaxPeriodInfo))]
    public class SettingsContainer
    {
        public List<GridColumnFilter> ColumnFilters
        {
            get;
            set;
        }

        public List<string> HiddenColumns
        {
            get;
            set;
        }

        public List<ColumnSort> Sorting
        {
            get;
            set;
        }
    }
}
