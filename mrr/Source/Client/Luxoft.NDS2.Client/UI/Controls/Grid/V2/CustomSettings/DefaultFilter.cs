﻿using Infragistics.Win.UltraWinGrid;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.CustomSettings
{
    public class DefaultFilter
    {
        private List<GridColumnFilter> _defaultFilter =
            new List<GridColumnFilter>();

        public IEnumerable<FilterCondition> ColumnFilter(string columnKey)
        {
            if (_defaultFilter.Any(filter => filter.ColumnKey == columnKey))
            {
                return _defaultFilter.First(filter => filter.ColumnKey == columnKey).Conditions;
            }

            return new List<FilterCondition>();
        }

        public List<GridColumnFilter> AllConditions()
        {
            return _defaultFilter;
        }

        public bool HasAnyFilter()
        {
            return _defaultFilter.Any();
        }

        public void ApplyFilter(string columnKey, FilterCondition condition)
        {
            if (!_defaultFilter.Any(filter => filter.ColumnKey == columnKey))
            {
                _defaultFilter.Add(
                    new GridColumnFilter()
                    {
                        ColumnKey = columnKey,
                        Conditions = new List<FilterCondition>() { condition }
                    });
            }
            else
            {
                _defaultFilter
                    .First(filter => filter.ColumnKey == columnKey)
                    .Conditions.Add(condition);
            }
        }
    }
}
