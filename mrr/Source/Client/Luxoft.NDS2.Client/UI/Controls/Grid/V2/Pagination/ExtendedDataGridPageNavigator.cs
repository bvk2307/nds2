﻿using Infragistics.Win;
using System;
using System.Linq;
using System.Windows.Forms;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination
{
    public partial class ExtendedDataGridPageNavigator : UserControl
    {
        private const string SummaryFormat = "Всего {0} (из {1})";

        private readonly IPager _pager;

        private const string TOTAL_SELECTED = "Выбрано записей - {0}";

        public ExtendedDataGridPageNavigator()
        {
            InitializeComponent();
        }

        public ExtendedDataGridPageNavigator(IPager pager)
            : this()
        {
            _pager = pager;

            _pageSizeSelect.Items.Clear();
            _pageSizeSelect.Items.AddRange(
                Defaults.AvailablePageSizes.Select(
                    pageSize => new ValueListItem(pageSize)).ToArray());

            _pager.ViewStateChanged += ShowModel;
            ShowModel();
        }

        /// <summary>
        /// Скрывает количество выводимых строк на страницу
        /// </summary>
        public void HidePageSizer()
        {
            _pageSizeSelect.Enabled = false;
            _pageSizeSelect.Visible = false;
            _pageSizeLabel.Enabled = false;
            _pageSizeLabel.Visible = false;
        }

        public void SetSelectedCount(int count)
        {
            _selectedCount.Visible = count > 0;
            if (_selectedCount.Visible)
            {
                _selectedCount.Text = String.Format(TOTAL_SELECTED, count);
            }
        }

        private void ShowModel()
        {
            _firstPage.Enabled = _pager.AllowPrevPage;
            _prevPage.Enabled = _pager.AllowPrevPage;
            _nextPage.Enabled = _pager.AllowNextPage;
            _lastPage.Enabled = _pager.AllowNextPage;
            _pageIndexInput.Value = _pager.PageIndex;
            _pageCount.Text = _pager.PageCount.ToString();
            _pageSizeSelect.Text = _pager.PageSize.ToString();
            _summary.Text =
                _pager.HasTotals
                ? string.Format(SummaryFormat, _pager.TotalMatches, _pager.TotalDataRows)
                : string.Empty;
        }

        private void ToFirstPage(object sender, EventArgs e)
        {
            _pager.FirstPage();
        }

        private void ToPrevPage(object sender, EventArgs e)
        {
            _pager.PrevPage();
        }

        private void ToPage()
        {
            if (_pageIndexInput.Value == null)
            {
                OnPageIndexError(new InvalidPageIndexException(_pageIndexInput.Value));
            }

            try
            {
                _pager.PageIndex = Convert.ToUInt32(_pageIndexInput.Value);
                SetSelectedCount(0);
            }
            catch (IndexOutOfRangeException error)
            {
                OnPageIndexError(error);
            }
            catch (PageIndexOutOfRangeException error)
            {
                OnPageIndexError(error);
            }
            catch (FormatException)
            {
                OnPageIndexError(new InvalidPageIndexException(_pageIndexInput.Value));
            }
            catch (OverflowException)
            {
                OnPageIndexError(new InvalidPageIndexException(_pageIndexInput.Value));
            }
        }

        private void ToNextPage(object sender, EventArgs e)
        {
            _pager.NextPage();
        }

        private void ToLastPage(object sender, EventArgs e)
        {
            _pager.LastPage();
        }

        private void PageSizeChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(_pageSizeSelect.Text))
            {
                OnPageSizeError(new InvalidPageSizeException(_pageSizeSelect.Text));
            }

            ushort newPageSize;

            if (!ushort.TryParse(_pageSizeSelect.Text, out newPageSize))
            {
                OnPageSizeError(new InvalidPageSizeException(_pageSizeSelect.Text));
            }

            try
            {
                _pager.PageSize = newPageSize;
            }
            catch (PageSizeOutOfRangeException error)
            {
                OnPageSizeError(error);
            }
        }

        private void OnPageIndexError(Exception error)
        {
            _pageIndexInput.Text = _pager.PageIndex.ToString();

            RaiseError(error);
        }

        private void OnPageSizeError(Exception error)
        {
            _pageSizeSelect.Text = _pager.PageSize.ToString();

            RaiseError(error);
        }

        private void RaiseError(Exception error)
        {
            var handler = OnError;
            if (handler != null)
                handler(error.Message);
        }

        public event GenericEventHandler<string> OnError;

        private void PageIndexInputKeyUp(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrWhiteSpace(_pageIndexInput.Text))
            {
                _pageIndexInput.Text = _pager.PageIndex.ToString();
            }
        }

        private void PageIndexInputKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) ToPage();
        }
    }
}
