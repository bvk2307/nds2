﻿using Infragistics.Win;
using System.Drawing;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns
{
    public class HyperlinkColumn : PropertyBoundColumn
    {
        public HyperlinkColumn(
            string key,
            IGrid grid,
            IColumnExpression columnExpression)
            : base(key, grid, columnExpression)
        {
        }

        protected override void SetupColumn()
        {
            var column = Column();            

            column.CellAppearance.Cursor = Cursors.Hand;
            column.CellAppearance.FontData.Underline = DefaultableBoolean.True;
            column.CellAppearance.ForeColor = Color.Blue;

            base.SetupColumn();
        }
    }
}
