﻿using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns
{
    public class GridColumnSetup
    {
        public GridColumnSetup()
        {
            Columns = new ColumnCollection();
        }

        public ColumnCollection Columns
        {
            get;
            private set;
        }

        public Dictionary<string, ColumnBase> All()
        {
            var result = new Dictionary<string, ColumnBase>();

            foreach (var column in Columns)
            {
                foreach (var inner in column.All())
                {
                    result.Add(inner.Key, inner.Value);
                }
            }

            return result;
        }

        public bool Contains(string columnKey)
        {
            return Columns.Contains(columnKey);
        }
    }
}
