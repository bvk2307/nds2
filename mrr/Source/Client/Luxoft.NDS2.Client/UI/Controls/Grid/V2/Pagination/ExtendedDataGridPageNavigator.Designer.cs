﻿namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination
{
    partial class ExtendedDataGridPageNavigator
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Misc.UltraLabel _pageCountLabel;
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            this._firstPage = new Infragistics.Win.Misc.UltraButton();
            this._prevPage = new Infragistics.Win.Misc.UltraButton();
            this._pageIndexInput = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this._pageCount = new Infragistics.Win.Misc.UltraLabel();
            this._nextPage = new Infragistics.Win.Misc.UltraButton();
            this._lastPage = new Infragistics.Win.Misc.UltraButton();
            this._pageSizeLabel = new Infragistics.Win.Misc.UltraLabel();
            this._pageSizeSelect = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this._summary = new Infragistics.Win.Misc.UltraLabel();
            this._selectedCount = new Infragistics.Win.Misc.UltraLabel();
            _pageCountLabel = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this._pageIndexInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._pageSizeSelect)).BeginInit();
            this.SuspendLayout();
            // 
            // _pageCountLabel
            // 
            appearance3.TextHAlignAsString = "Left";
            appearance3.TextVAlignAsString = "Middle";
            _pageCountLabel.Appearance = appearance3;
            _pageCountLabel.Location = new System.Drawing.Point(118, 1);
            _pageCountLabel.Name = "_pageCountLabel";
            _pageCountLabel.Size = new System.Drawing.Size(17, 23);
            _pageCountLabel.TabIndex = 3;
            _pageCountLabel.Text = "из";
            // 
            // _firstPage
            // 
            appearance1.FontData.SizeInPoints = 6F;
            appearance1.Image = global::Luxoft.NDS2.Client.Properties.Resources.start;
            this._firstPage.Appearance = appearance1;
            this._firstPage.Dock = System.Windows.Forms.DockStyle.Left;
            this._firstPage.Location = new System.Drawing.Point(0, 0);
            this._firstPage.Name = "_firstPage";
            this._firstPage.Size = new System.Drawing.Size(25, 25);
            this._firstPage.TabIndex = 0;
            this._firstPage.Click += new System.EventHandler(this.ToFirstPage);
            // 
            // _prevPage
            // 
            appearance5.FontData.SizeInPoints = 6F;
            appearance5.Image = global::Luxoft.NDS2.Client.Properties.Resources.previous;
            this._prevPage.Appearance = appearance5;
            this._prevPage.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._prevPage.Location = new System.Drawing.Point(25, 0);
            this._prevPage.Name = "_prevPage";
            this._prevPage.Size = new System.Drawing.Size(25, 25);
            this._prevPage.TabIndex = 1;
            this._prevPage.Click += new System.EventHandler(this.ToPrevPage);
            // 
            // _pageIndexInput
            // 
            this._pageIndexInput.Location = new System.Drawing.Point(50, 2);
            this._pageIndexInput.MaskInput = "nnnnnnnn";
            this._pageIndexInput.MaxValue = 99999999;
            this._pageIndexInput.MinValue = 0;
            this._pageIndexInput.Name = "_pageIndexInput";
            this._pageIndexInput.PromptChar = ' ';
            this._pageIndexInput.Size = new System.Drawing.Size(66, 21);
            this._pageIndexInput.SpinIncrement = 1;
            this._pageIndexInput.SpinWrap = true;
            this._pageIndexInput.TabIndex = 2;
            this._pageIndexInput.Value = 99999999;
            this._pageIndexInput.KeyDown += new System.Windows.Forms.KeyEventHandler(this.PageIndexInputKeyDown);
            this._pageIndexInput.KeyUp += new System.Windows.Forms.KeyEventHandler(this.PageIndexInputKeyUp);
            // 
            // _pageCount
            // 
            appearance4.TextHAlignAsString = "Left";
            appearance4.TextVAlignAsString = "Middle";
            this._pageCount.Appearance = appearance4;
            this._pageCount.Location = new System.Drawing.Point(140, 1);
            this._pageCount.Name = "_pageCount";
            this._pageCount.Size = new System.Drawing.Size(55, 23);
            this._pageCount.TabIndex = 4;
            this._pageCount.Text = "99999999";
            // 
            // _nextPage
            // 
            appearance6.FontData.SizeInPoints = 6F;
            appearance6.Image = global::Luxoft.NDS2.Client.Properties.Resources.next;
            this._nextPage.Appearance = appearance6;
            this._nextPage.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._nextPage.Location = new System.Drawing.Point(198, 0);
            this._nextPage.Name = "_nextPage";
            this._nextPage.Size = new System.Drawing.Size(25, 25);
            this._nextPage.TabIndex = 5;
            this._nextPage.Click += new System.EventHandler(this.ToNextPage);
            // 
            // _lastPage
            // 
            appearance2.FontData.SizeInPoints = 6F;
            appearance2.Image = global::Luxoft.NDS2.Client.Properties.Resources.finish;
            this._lastPage.Appearance = appearance2;
            this._lastPage.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lastPage.Location = new System.Drawing.Point(223, 0);
            this._lastPage.Name = "_lastPage";
            this._lastPage.Size = new System.Drawing.Size(25, 25);
            this._lastPage.TabIndex = 6;
            this._lastPage.Click += new System.EventHandler(this.ToLastPage);
            // 
            // _pageSizeLabel
            // 
            appearance8.TextHAlignAsString = "Right";
            appearance8.TextVAlignAsString = "Middle";
            this._pageSizeLabel.Appearance = appearance8;
            this._pageSizeLabel.Location = new System.Drawing.Point(248, 1);
            this._pageSizeLabel.Name = "_pageSizeLabel";
            this._pageSizeLabel.Size = new System.Drawing.Size(45, 23);
            this._pageSizeLabel.TabIndex = 7;
            this._pageSizeLabel.Text = "на стр.";
            // 
            // _pageSizeSelect
            // 
            this._pageSizeSelect.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this._pageSizeSelect.Location = new System.Drawing.Point(293, 2);
            this._pageSizeSelect.Name = "_pageSizeSelect";
            this._pageSizeSelect.Size = new System.Drawing.Size(52, 21);
            this._pageSizeSelect.TabIndex = 8;
            this._pageSizeSelect.TextChanged += new System.EventHandler(this.PageSizeChanged);
            // 
            // _summary
            // 
            appearance7.TextHAlignAsString = "Right";
            appearance7.TextVAlignAsString = "Middle";
            this._summary.Appearance = appearance7;
            this._summary.AutoSize = true;
            this._summary.Dock = System.Windows.Forms.DockStyle.Right;
            this._summary.Location = new System.Drawing.Point(565, 0);
            this._summary.Name = "_summary";
            this._summary.Size = new System.Drawing.Size(0, 25);
            this._summary.TabIndex = 9;
            // 
            // _selectedCount
            // 
            appearance9.ForeColor = System.Drawing.Color.Red;
            appearance9.TextHAlignAsString = "Right";
            appearance9.TextVAlignAsString = "Middle";
            this._selectedCount.Appearance = appearance9;
            this._selectedCount.AutoSize = true;
            this._selectedCount.Dock = System.Windows.Forms.DockStyle.Right;
            this._selectedCount.Location = new System.Drawing.Point(565, 0);
            this._selectedCount.Name = "_selectedCount";
            this._selectedCount.Size = new System.Drawing.Size(0, 25);
            this._selectedCount.TabIndex = 10;
            this._selectedCount.Visible = false;
            // 
            // ExtendedDataGridPageNavigator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._summary);
            this.Controls.Add(this._selectedCount);
            this.Controls.Add(this._pageSizeSelect);
            this.Controls.Add(this._pageSizeLabel);
            this.Controls.Add(this._lastPage);
            this.Controls.Add(this._nextPage);
            this.Controls.Add(this._pageCount);
            this.Controls.Add(_pageCountLabel);
            this.Controls.Add(this._pageIndexInput);
            this.Controls.Add(this._prevPage);
            this.Controls.Add(this._firstPage);
            this.Name = "ExtendedDataGridPageNavigator";
            this.Size = new System.Drawing.Size(565, 25);
            ((System.ComponentModel.ISupportInitialize)(this._pageIndexInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._pageSizeSelect)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.Misc.UltraButton _firstPage;
        private Infragistics.Win.Misc.UltraButton _prevPage;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor _pageIndexInput;
        private Infragistics.Win.Misc.UltraLabel _pageCount;
        private Infragistics.Win.Misc.UltraButton _nextPage;
        private Infragistics.Win.Misc.UltraButton _lastPage;
        private Infragistics.Win.Misc.UltraLabel _pageSizeLabel;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor _pageSizeSelect;
        private Infragistics.Win.Misc.UltraLabel _summary;
        private Infragistics.Win.Misc.UltraLabel _selectedCount;
        //private Infragistics.Win.Misc.UltraLabel _emptySpace;
    }
}
