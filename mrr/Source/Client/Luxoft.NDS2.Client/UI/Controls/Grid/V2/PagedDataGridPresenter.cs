﻿using System.Linq;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2
{
    public class PagedDataGridPresenter<TRowData> : DataGridPresenter<TRowData>
    {
        protected readonly IPager _pager;

        public PagedDataGridPresenter(
            IDataGridView gridView,
            IPager pager,
            Func<QueryConditions, PageResult<TRowData>> dataLoader,
            QueryConditions searchCriteria = null
            )
            : base(gridView, dataLoader, searchCriteria)
        {
            if (pager == null)
            {
                throw new ArgumentNullException("pager");
            }

            _pager = pager;

            SetPaginationCriteria();

            _pager.DataListChanging += PageIndexChanged;
        }

        public virtual void ResetTotalsCache()
        {
            SearchCriteria.PaginationDetails.SkipTotalAvailable = false;
            SearchCriteria.PaginationDetails.SkipTotalMatches = false;
        }

        public virtual void SetTotalAvalaible(uint totalAvalable)
        {
            _pager.TotalDataRows = totalAvalable;
            SearchCriteria.PaginationDetails.SkipTotalAvailable = true;

            if (!SearchCriteria.Filter.Any())
            {
                _pager.TotalMatches = totalAvalable;
                SearchCriteria.PaginationDetails.SkipTotalMatches = true;
            }
        }

        protected virtual void SetPaginationCriteria()
        {
            SearchCriteria.PageIndex = _pager.PageIndex;
            SearchCriteria.PaginationDetails.RowsToSkip =
                (_pager.PageIndex - 1) * _pager.PageSize;
            SearchCriteria.PageSize = _pager.PageSize;
            SearchCriteria.PaginationDetails.RowsToTake = _pager.PageSize;
        }

        protected virtual void PageIndexChanged()
        {
            SetPaginationCriteria();
            SearchCriteria.PaginationDetails.SkipTotalMatches = true;
            Load();
        }

        protected override void BeforeDataPush(PageResult<TRowData> data)
        {
            if (!SearchCriteria.PaginationDetails.SkipTotalMatches)
            {
                _pager.TotalMatches = (uint)data.TotalMatches;
            }

            if (!SearchCriteria.PaginationDetails.SkipTotalAvailable)
            {
                _pager.TotalDataRows = 
                    data.TotalAvailable > data.TotalMatches
                    ? (uint)data.TotalAvailable
                    : (uint)data.TotalMatches;
            }

            SearchCriteria.PaginationDetails.SkipTotalMatches = false;
            SearchCriteria.PaginationDetails.SkipTotalAvailable = true;
        }

        protected override void AfterSorting()
        {
            SearchCriteria.PaginationDetails.SkipTotalMatches = true;
            Load();
        }

        protected override void AfterSaveSettings()
        {
            Load();
        }

        public event EventHandler OnAfterDataLoad;

        protected override void AfterDataLoad()
        {
            var subscriber = OnAfterDataLoad;
            if (subscriber != null)
                subscriber.Invoke(_pager, EventArgs.Empty);
        }

        protected override void AfterFiltering()
        {
            _pager.SuppressEvents = true;
            _pager.SetFirstPage();
            _pager.SuppressEvents = false;

            SetPaginationCriteria();

            if (!SearchCriteria.Filter.Any())
            {
                SearchCriteria.PaginationDetails.SkipTotalMatches = 
                    SearchCriteria.PaginationDetails.SkipTotalAvailable;
            }
        
            Load();

            if (!SearchCriteria.Filter.Any())
            {
                _pager.TotalMatches = _pager.TotalDataRows;
            }
        }
    }
}
