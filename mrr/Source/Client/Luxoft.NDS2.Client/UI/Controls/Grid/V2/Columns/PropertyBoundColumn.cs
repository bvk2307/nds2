﻿using Infragistics.Win.FormattedLinkLabel;
using Infragistics.Win.UltraWinMaskedEdit;
using System;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns
{
    public class PropertyBoundColumn : SingleColumn
    {
        # region Конструкторы

        public PropertyBoundColumn(string key, IGrid grid, IColumnExpression columnExpression)
            : base(key, grid, columnExpression)
        {
            EditableDefinition = new ColumnEditableDefinition();
            StyleDefinition = new ColumnStyleDefinition();
        }

        # endregion

        # region Свойства

        public Action<object> CellClick
        {
            get;
            set;
        }

        public ColumnEditableDefinition EditableDefinition { get; private set; }

        public ColumnStyleDefinition StyleDefinition { get; private set; }

        public bool IsHtml
        {
            get;
            set;
        }

        # endregion

        # region Настройки колонки

        protected override void SetupColumn()
        {
            var column = Column();

            if (CellClick != null)
            {
                Grid.Grid.ClickCell +=
                    (sender, args) =>
                    {
                        if (args.Cell.Column.Key == column.Key)
                        {
                            CellClick(args.Cell.Row.ListObject);
                        }
                    };
            }

            if (StyleDefinition.Style == Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton
                || StyleDefinition.Style == Infragistics.Win.UltraWinGrid.ColumnStyle.Button)
            {
                column.ButtonDisplayStyle = Infragistics.Win.UltraWinGrid.ButtonDisplayStyle.Always;
                column.Style = StyleDefinition.Style;
                column.CellButtonAppearance.Image = StyleDefinition.Image;
                Grid.Grid.ClickCellButton +=
                    (sender, args) =>
                    {
                        if (args.Cell.Column.Key == column.Key)
                        {
                            StyleDefinition.Handler(sender, args);
                        }
                    };
            }

            if (IsHtml)
            {
                column.Style = Infragistics.Win.UltraWinGrid.ColumnStyle.FormattedText;
                ((FormattedLinkEditor)column.Editor).UnderlineLinks = UnderlineLink.Always;
                column.MaskClipMode = MaskMode.Raw;
                ((FormattedLinkEditor)column.Editor).TreatValueAs = TreatValueAs.FormattedText;
            }

            if (EditableDefinition.Handler != null)
            {
                Grid.Grid.AfterCellUpdate +=
                    (sender, e) =>
                    {
                        if (e.Cell.Column.Key == column.Key)
                        {
                            EditableDefinition.Handler(sender, e);
                        }
                    };
            }

            base.SetupColumn();
        }

        public override void ToggleEdit(bool disableEdit)
        {
            base.ToggleEdit(disableEdit);

            var column = Column();

            if (!DisableEdit && !disableEdit)
            {
                column.Nullable = EditableDefinition.Nullable;

                if (!string.IsNullOrEmpty(EditableDefinition.MaskInput))
                {
                    column.MaskInput = EditableDefinition.MaskInput;
                    column.MaskDataMode = MaskMode.Raw;
                    column.MaskDisplayMode = MaskMode.Raw;
                    column.MaskClipMode = MaskMode.IncludeBoth;
                    column.PadChar = ' ';
                    column.PromptChar = '_';
                }
            }
        }

        # endregion
    }
}
