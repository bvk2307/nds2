﻿namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns.IndexColumn
{
    public interface IIndexProvider
    {
        int FirstRowIndex();
    }
}
