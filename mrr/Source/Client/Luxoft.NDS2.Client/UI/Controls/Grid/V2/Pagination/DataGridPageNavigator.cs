﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Luxoft.NDS2.Client.UI.Base;
using Infragistics.Win;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination
{
    public partial class DataGridPageNavigator : UserControl
    {
        private const string PageIndexTextFormat = "Страница {0} из {1}";

        private readonly IPager _pager;

        public DataGridPageNavigator()
        {
            InitializeComponent();
        }

        public DataGridPageNavigator(IPager pager)
            : this()
        {
            _pager = pager;

            _pageSizeSelect.Items.Clear();
            _pageSizeSelect.Items.AddRange(
                Defaults.AvailablePageSizes.Select(
                    pageSize => new ValueListItem(pageSize)).ToArray());
            
            _pager.ViewStateChanged += ShowModel;
            ShowModel();
        }        

        public void HidePageSizer()
        {
            _pageSizeSelect.Enabled = false;
            _pageSizeSelect.Visible = false;
            ultraLblLinesperPage.Enabled = false;
            ultraLblLinesperPage.Visible = false;
        }

        private void ShowModel()
        {
            _pageSizeSelect.Text = _pager.PageSize.ToString();
            _pageIndexLabel.Text = 
                string.Format(PageIndexTextFormat, _pager.PageIndex, _pager.PageCount);
            _prevPageButton.Enabled = _pager.AllowPrevPage;
            _nextPageButton.Enabled = _pager.AllowNextPage;
        }

        private void OnPrevPageClick(object sender, EventArgs e)
        {
            _pager.PrevPage();
        }

        private void OnNextPageClick(object sender, EventArgs e)
        {
            _pager.NextPage();
        }

        private void OnPageSizeChanged(object sender, EventArgs e)
        {
            if (_pageSizeSelect.Value == null)
            {
                OnPageSizeError(new InvalidPageSizeException(null));
            }

            ushort newPageSize;

            if (!ushort.TryParse(_pageSizeSelect.Value.ToString(), out newPageSize))
            {
                OnPageSizeError(new InvalidPageSizeException(_pageSizeSelect.Value.ToString()));
            }

            try
            {
                _pager.PageSize = newPageSize;
            }
            catch (PageSizeOutOfRangeException error)
            {
                OnPageSizeError(error);
            }
        }

        private void OnPageSizeError(Exception error)
        {
            _pageSizeSelect.Text = _pager.PageSize.ToString();
            RaisePageSizeError(error);
        }

        public event GenericEventHandler<string> PageSizeInputError;

        private void RaisePageSizeError(Exception e)
        {
            var handler = PageSizeInputError;
            if (handler != null)
                handler(e.Message);
        }
    }
}
