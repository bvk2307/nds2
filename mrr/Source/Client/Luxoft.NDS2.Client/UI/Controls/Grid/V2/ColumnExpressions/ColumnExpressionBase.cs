﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.ColumnExpressions
{
    public abstract class ColumnExpressionBase : IColumnExpression
    {
        public ColumnExpressionBase()
        {
        }

        public virtual void ApplyFilter(Infragistics.Win.UltraWinGrid.ColumnFilter columnFilter, QueryConditions filter)
        {
        }

        public virtual void ApplySorting(Infragistics.Win.UltraWinGrid.UltraGridColumn sortedColumn, QueryConditions searchCriteria)
        {
        }

        public virtual void AddSorting(Infragistics.Win.UltraWinGrid.UltraGridColumn sortedColumn, QueryConditions searchCriteria)
        {
        }
    }
}
