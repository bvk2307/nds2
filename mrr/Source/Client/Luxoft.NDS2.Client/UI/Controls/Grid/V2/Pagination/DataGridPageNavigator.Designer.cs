﻿namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination
{
    partial class DataGridPageNavigator
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            this._pageSizeSelect = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLblLinesperPage = new Infragistics.Win.Misc.UltraLabel();
            this._nextPageButton = new Infragistics.Win.Misc.UltraButton();
            this._pageIndexLabel = new Infragistics.Win.Misc.UltraLabel();
            this._prevPageButton = new Infragistics.Win.Misc.UltraButton();
            ((System.ComponentModel.ISupportInitialize)(this._pageSizeSelect)).BeginInit();
            this.SuspendLayout();
            // 
            // _pageSizeSelect
            // 
            this._pageSizeSelect.Dock = System.Windows.Forms.DockStyle.Left;
            this._pageSizeSelect.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this._pageSizeSelect.Location = new System.Drawing.Point(177, 0);
            this._pageSizeSelect.Name = "_pageSizeSelect";
            this._pageSizeSelect.NullText = "50";
            this._pageSizeSelect.Size = new System.Drawing.Size(50, 21);
            this._pageSizeSelect.TabIndex = 12;
            this._pageSizeSelect.ValueChanged += new System.EventHandler(this.OnPageSizeChanged);
            // 
            // ultraLblLinesperPage
            // 
            appearance1.TextVAlignAsString = "Middle";
            this.ultraLblLinesperPage.Appearance = appearance1;
            this.ultraLblLinesperPage.Dock = System.Windows.Forms.DockStyle.Left;
            this.ultraLblLinesperPage.Location = new System.Drawing.Point(62, 0);
            this.ultraLblLinesperPage.Name = "ultraLblLinesperPage";
            this.ultraLblLinesperPage.Size = new System.Drawing.Size(115, 24);
            this.ultraLblLinesperPage.TabIndex = 11;
            this.ultraLblLinesperPage.Text = "Строк на страницу:";
            // 
            // _nextPageButton
            // 
            appearance3.Image = global::Luxoft.NDS2.Client.Properties.Resources.right_grey;
            appearance3.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance3.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this._nextPageButton.Appearance = appearance3;
            this._nextPageButton.Dock = System.Windows.Forms.DockStyle.Left;
            this._nextPageButton.Location = new System.Drawing.Point(36, 0);
            this._nextPageButton.Name = "_nextPageButton";
            this._nextPageButton.Size = new System.Drawing.Size(26, 24);
            this._nextPageButton.TabIndex = 9;
            this._nextPageButton.Click += new System.EventHandler(this.OnNextPageClick);
            // 
            // _pageIndexLabel
            // 
            appearance4.TextVAlignAsString = "Middle";
            this._pageIndexLabel.Appearance = appearance4;
            this._pageIndexLabel.AutoSize = true;
            this._pageIndexLabel.Dock = System.Windows.Forms.DockStyle.Left;
            this._pageIndexLabel.Location = new System.Drawing.Point(26, 0);
            this._pageIndexLabel.Name = "_pageIndexLabel";
            this._pageIndexLabel.Padding = new System.Drawing.Size(5, 0);
            this._pageIndexLabel.Size = new System.Drawing.Size(10, 24);
            this._pageIndexLabel.TabIndex = 10;
            // 
            // _prevPageButton
            // 
            appearance2.Image = global::Luxoft.NDS2.Client.Properties.Resources.left_grey;
            appearance2.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance2.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this._prevPageButton.Appearance = appearance2;
            this._prevPageButton.Dock = System.Windows.Forms.DockStyle.Left;
            this._prevPageButton.Enabled = false;
            this._prevPageButton.Location = new System.Drawing.Point(0, 0);
            this._prevPageButton.Name = "_prevPageButton";
            this._prevPageButton.Size = new System.Drawing.Size(26, 24);
            this._prevPageButton.TabIndex = 8;
            this._prevPageButton.Click += new System.EventHandler(this.OnPrevPageClick);
            // 
            // DataGridPageNavigator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._pageSizeSelect);
            this.Controls.Add(this.ultraLblLinesperPage);
            this.Controls.Add(this._nextPageButton);
            this.Controls.Add(this._pageIndexLabel);
            this.Controls.Add(this._prevPageButton);
            this.Name = "DataGridPageNavigator";
            this.Size = new System.Drawing.Size(345, 24);
            ((System.ComponentModel.ISupportInitialize)(this._pageSizeSelect)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.UltraWinEditors.UltraComboEditor _pageSizeSelect;
        private Infragistics.Win.Misc.UltraLabel ultraLblLinesperPage;
        private Infragistics.Win.Misc.UltraButton _nextPageButton;
        private Infragistics.Win.Misc.UltraLabel _pageIndexLabel;
        private Infragistics.Win.Misc.UltraButton _prevPageButton;
    }
}
