﻿using Infragistics.Win.UltraWinGrid;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.CustomSettings
{
    public enum OrderableObjectType { Column, Group }
    public class OrderableObject
    {
        public readonly object Subject;
        public readonly OrderableObjectType Type;
        public int OriginX
        {
            get
            {
                return Type == OrderableObjectType.Column
                    ? ((UltraGridColumn)Subject).RowLayoutColumnInfo.OriginX
                    : ((UltraGridGroup)Subject).RowLayoutGroupInfo.OriginX;
            }
            set
            {
                if (Type == OrderableObjectType.Column)
                    ((UltraGridColumn)Subject).RowLayoutColumnInfo.OriginX = value;
                else
                    ((UltraGridGroup)Subject).RowLayoutGroupInfo.OriginX = value;
            }
        }

        public List<OrderableObject> Children = new List<OrderableObject>();

        public string Key { get; private set; }

        public OrderableObject(UltraGridColumn column)
        {
            Subject = column;
            Type = OrderableObjectType.Column;
            Key = column.Key;
        }

        public OrderableObject(UltraGridGroup group)
        {
            Subject = group;
            Type = OrderableObjectType.Group;
            Key = group.Key;
        }
    }

    public static class LayoutOrder
    {
        public static OrderableObject FindByKey(this List<OrderableObject> list, string key)
        {
            foreach (var item in list)
            {
                if (item.Key == key)
                    return item;

                if (item.Children.Count <= 0)
                    continue;

                var res = item.Children.FindByKey(key);
                if (res != null)
                    return res;
            }
            return null;
        }

        public static int SetOrder(this OrderableObject column, int x)
        {
            column.OriginX = x;
            if (column.Children.Count == 0)
                return x + 2;

            var subX = column.Children.OrderBy(c => c.OriginX).ToArray().Aggregate(0, (current, child) => child.SetOrder(current));
            return x + subX;
        }

        /// <summary>
        /// Упорядочивает колонки в гриде Infragistic после сложных манипуляций с порядком и видимостью колонок
        /// </summary>
        /// <param name="columns">Колекция колонок в гриде</param>
        public static void FixLayout(this ColumnsCollection columns)
        {
            var orderable = new List<OrderableObject>();
            foreach (var c in columns)
            {
                if (c.Hidden)
                    continue;
                if (c.RowLayoutColumnInfo.ParentGroup == null)
                {
                    orderable.Add(new OrderableObject(c));
                }
                else
                {

                    var g = c.RowLayoutColumnInfo.ParentGroup;
                    var co = new OrderableObject(c);
                    var item = orderable.FindByKey(g.Key);

                    if (item != null)
                    {
                        item.Children.Add(co);
                        continue;
                    }

                    var go = new OrderableObject(g);
                    go.Children.Add(co);
                    var found = false;
                    while (g.RowLayoutGroupInfo.ParentGroup != null)
                    {
                        g = g.RowLayoutGroupInfo.ParentGroup;
                        var group = orderable.FindByKey(g.Key);
                        if (group != null)
                        {
                            group.Children.Add(go);
                            found = true;
                            break;
                        }
                        var tmp = new OrderableObject(g);
                        tmp.Children.Add(go);
                        go = tmp;
                    }
                    if (!found)
                        orderable.Add(go);
                }
            }
            orderable.OrderBy(c => c.OriginX).ToArray().Aggregate(0, (current, child) => child.SetOrder(current));
        }
    }
}
