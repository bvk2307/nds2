﻿using Infragistics.Win.UltraWinGrid;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.Addins
{
    public class ExpansionIndicatorRemover : ITableAddin
    {
        public void Customize(UltraGrid grid)
        {
            grid.DisplayLayout.Override.ExpansionIndicator = ShowExpansionIndicator.Never;
        }
    }
}
