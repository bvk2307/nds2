﻿using System;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination
{
    public class InvalidPageSizeException : Exception
    {
        private const string MessageFormat = 
            "Значение {0} не является допустимым для размера страницы списка данных";

        public InvalidPageSizeException(string pageSizeValue)
            : base(string.Format(MessageFormat, pageSizeValue))
        {
            PageSizeValue = pageSizeValue;
        }

        public string PageSizeValue
        {
            get;
            private set;
        }
    }
}
