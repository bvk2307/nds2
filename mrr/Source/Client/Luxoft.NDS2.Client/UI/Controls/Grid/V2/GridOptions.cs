﻿using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.CustomSettings;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2
{
    public class GridOptions
    {
        # region Настройки по умолчанию

        public static GridOptions Default()
        {
            return new GridOptions()
            {
                AllowManageSettings = true,
                AllowManageFilterSettings = true
            };
        }

        public static GridOptions Default(Luxoft.NDS2.Common.Contracts.DTO.Query.QueryConditions criteria)
        {
            var result =  new GridOptions()
            {
                AllowManageSettings = true,
                AllowManageFilterSettings = true
            };

            foreach (var filter in criteria.Filter)
            {
                foreach (var columnFilter in filter.Filtering)
                    result.DefaultFilter.ApplyFilter(
                        filter.ColumnName,
                        columnFilter.ToCondition());
            }

            return result;
        }

        # endregion

        # region Конструктор

        public GridOptions()
        {
            DefaultFilter = new DefaultFilter();
        }

        # endregion

        # region Включение/выключение возможностей 

        public bool AllowManageSettings 
        { 
            get; 
            set; 
        }

        # endregion

        # region Фильтр данных

        public DefaultFilter DefaultFilter
        {
            get;
            private set;
        }

        # endregion

        # region Включение/выключение возможностей сохранения/восстановления пользовательских настроек фильтрации

        public bool AllowManageFilterSettings
        {
            get;
            set;
        }

        # endregion
   
    }
}
