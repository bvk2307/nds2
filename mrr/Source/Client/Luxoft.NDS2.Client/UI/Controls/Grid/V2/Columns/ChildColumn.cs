﻿using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.ColumnExpressions;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns
{
    public class ChildColumn : SingleColumn
    {
        public ChildColumn(string key, IGrid grid)
            : base(key, grid, ColumnExpressionCreator.CreateColumnExpressionDefault())
        {
        }

        public UltraGridRow Row
        {
            get;
            set;
        }

        protected override UltraGridBand Band()
        {
            return Row.ChildBands[0].Band;
        }
    }
}
