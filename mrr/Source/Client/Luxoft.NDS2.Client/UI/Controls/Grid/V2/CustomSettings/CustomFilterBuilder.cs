﻿using Infragistics.Win.UltraWinGrid;
using query = Luxoft.NDS2.Common.Contracts.DTO.Query;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.CustomSettings
{
    public static class FilterConertHelper
    {
        public static FilterComparisionOperator Convert(this query.ColumnFilter.FilterComparisionOperator filterOperator)
        {
            return filterOperator == Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.Contains ? FilterComparisionOperator.Contains :
                    filterOperator == Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.DoesNotContain ? FilterComparisionOperator.DoesNotContain :
                    filterOperator == Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.DoesNotEndWith ? FilterComparisionOperator.DoesNotEndWith :
                    filterOperator == Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.DoesNotMatch ? FilterComparisionOperator.DoesNotMatch :
                    filterOperator == Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.DoesNotStartWith ? FilterComparisionOperator.DoesNotStartWith :
                    filterOperator == Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.EndsWith ? FilterComparisionOperator.EndsWith :
                    filterOperator == Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.Equals ? FilterComparisionOperator.Equals :
                    filterOperator == Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.GreaterThan ? FilterComparisionOperator.GreaterThan :
                    filterOperator == Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.GreaterThanOrEqualTo ? FilterComparisionOperator.GreaterThanOrEqualTo :
                    filterOperator == Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.LessThan ? FilterComparisionOperator.LessThan :
                    filterOperator == Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.LessThanOrEqualTo ? FilterComparisionOperator.LessThanOrEqualTo :
                    filterOperator == Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.Like ? FilterComparisionOperator.Like :
                    filterOperator == Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.Match ? FilterComparisionOperator.Match :
                    filterOperator == Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.NotEquals ? FilterComparisionOperator.NotEquals :
                    filterOperator == Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.NotLike ? FilterComparisionOperator.NotLike :
                    filterOperator == Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.StartsWith ? FilterComparisionOperator.StartsWith :
                    FilterComparisionOperator.Custom;
        }

        public static query.ColumnFilter.FilterComparisionOperator Convert(this FilterComparisionOperator filterOperator)
        {
            return filterOperator == FilterComparisionOperator.Contains ? Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.Contains :
                    filterOperator == FilterComparisionOperator.DoesNotContain ? Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.DoesNotContain :
                    filterOperator == FilterComparisionOperator.DoesNotEndWith ? Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.DoesNotEndWith :
                    filterOperator == FilterComparisionOperator.DoesNotMatch ? Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.DoesNotMatch :
                    filterOperator == FilterComparisionOperator.DoesNotStartWith ? Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.DoesNotStartWith :
                    filterOperator == FilterComparisionOperator.EndsWith ? Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.EndsWith :
                    filterOperator == FilterComparisionOperator.Equals ? Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.Equals :
                    filterOperator == FilterComparisionOperator.GreaterThan ? Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.GreaterThan :
                    filterOperator == FilterComparisionOperator.GreaterThanOrEqualTo ? Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.GreaterThanOrEqualTo :
                    filterOperator == FilterComparisionOperator.LessThan ? Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.LessThan :
                    filterOperator == FilterComparisionOperator.LessThanOrEqualTo ? Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.LessThanOrEqualTo :
                    filterOperator == FilterComparisionOperator.Like ? Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.Like :
                    filterOperator == FilterComparisionOperator.Match ? Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.Match :
                    filterOperator == FilterComparisionOperator.NotEquals ? Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.NotEquals :
                    filterOperator == FilterComparisionOperator.NotLike ? Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.NotLike :
                    filterOperator == FilterComparisionOperator.StartsWith ? Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.StartsWith :
                    Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter.FilterComparisionOperator.NotDefinedOperator;
        }

        public static FilterCondition ToCondition(this query.ColumnFilter filter)
        {
            return new FilterCondition(filter.ComparisonOperator.Convert(), filter.Value);
        }

        public static query.FilterQuery Convert(this ColumnFilter gridFilter)
        {
            if (gridFilter == null)
                return new Common.Contracts.DTO.Query.FilterQuery();

            var currFilterOperator =
                gridFilter.LogicalOperator == FilterLogicalOperator.And
                    ? query.FilterQuery.FilterLogicalOperator.And
                    : query.FilterQuery.FilterLogicalOperator.Or;

            var filterQuery =
                new query.FilterQuery(true)
                {
                    FilterOperator = currFilterOperator,
                    ColumnName = gridFilter.Column.Key,
                    ColumnType = gridFilter.Column.DataType
                };
           
            foreach (var elem in gridFilter.FilterConditions.All)
            {
                var currFilterItem = (FilterCondition)elem;
                var compareValue = currFilterItem.CompareValue == null ? String.Empty : currFilterItem.CompareValue.ToString();
                if (String.IsNullOrEmpty(compareValue) || currFilterItem.CompareValue == FilterCondition.BlankCellValue)
                {
                    currFilterItem.CompareValue = currFilterItem.Column.DefaultCellValue ?? DBNull.Value;
                }
              
                else if (currFilterItem.CompareValue is string)
                {
                    string filterValueAsString = currFilterItem.CompareValue.ToString();
                    Exception incorrectFormatException =
                        new FormatException(string.Format("Некорректное задание фильтра '{0}'",
                            currFilterItem.Column.Header.Caption));


                    if (gridFilter.Column.DataType == typeof(DateTime) || gridFilter.Column.DataType == typeof(DateTime?))
                    {
                        DateTime? valueDT = currFilterItem.CompareValue.MagicTransform();
                        if (valueDT != null)
                        {
                            currFilterItem.CompareValue = valueDT;
                        }
                    }

                    if (gridFilter.Column.DataType == typeof(decimal) || gridFilter.Column.DataType == typeof(decimal?))
                    {
                        decimal val;
                        NumberFormatInfo formatRUStyle = new CultureInfo("ru-RU", false).NumberFormat;
                        if (decimal.TryParse(filterValueAsString.Replace(".", ","), NumberStyles.Any, formatRUStyle, out val))
                        {
                            currFilterItem.CompareValue = val;
                        }
                        else
                        {
                            throw incorrectFormatException;
                        }
                    }

                    if (gridFilter.Column.DataType == typeof(int) || gridFilter.Column.DataType == typeof(int?))
                    {
                        int val;

                        if (int.TryParse(filterValueAsString, out val))
                        {
                            currFilterItem.CompareValue = val;
                        }
                        else
                        {
                            throw incorrectFormatException;
                        }
                    }

                    if (gridFilter.Column.DataType == typeof(long) || gridFilter.Column.DataType == typeof(long?))
                    {
                        long val;

                        if (long.TryParse(filterValueAsString, out val))
                        {
                            currFilterItem.CompareValue = val;
                        }
                        else
                        {
                            throw incorrectFormatException;
                        }
                    }
                }

                var currFilter = new Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter();
                currFilter.ComparisonOperator = currFilterItem.ComparisionOperator.Convert();
                currFilter.Value = currFilterItem.CompareValue;
                if (currFilterItem.CompareValue == FilterCondition.NonBlankCellValue)
                {
                    currFilter.ComparisonOperator = FilterComparisionOperator.NotEquals.Convert();
                    currFilter.Value = currFilterItem.Column.DefaultCellValue ?? DBNull.Value;
                }
                if (!filterQuery.Filtering.Contains(currFilter))
                    filterQuery.Filtering.Add(currFilter);
            }

            return filterQuery;
        }

        public static DateTime? MagicTransform(this object value)
        {
            string valueString = (string)value;
            DateTime? valueDT = null;
            DateTime dtTemp;
            if (DateTime.TryParse(valueString, out dtTemp))
            {
                valueDT = dtTemp;
            }
            else
            {
                List<string> words = valueString.Split('.').ToList();
                int day = ParseWord(words, 0);
                int month = ParseWord(words, 1);
                int year = ParseWord(words, 2);
                valueDT = new DateTime(year, month, day);
            }
            return valueDT;
        }

        public static int ParseWord(List<string> words, int index)
        {
            int ret = 0;
            if (words.Count > 0)
            {
                int temp = 0;
                if (int.TryParse(words[0], out temp))
                {
                    ret = temp;
                }
            }
            return ret;
        }
    }

    public class CustomFilterBuilder
    {
        private const bool UserDefinedTrue = true;

        private readonly List<query.FilterQuery> _filter;

        public CustomFilterBuilder(List<query.FilterQuery> filter)
        {
            _filter = filter;
            Default = new DefaultFilter();
        }

        public DefaultFilter Default
        {
            get;
            set;
        }

        public void ApplyFilter(Infragistics.Win.UltraWinGrid.ColumnFilter e)
        {
            if (e.FilterConditions.All.Any())
            {
                ApplyUserFilter(e);
            }
            else
            {
                RemoveFilter(e);
            }
        }

        public void RemoveFilter(Infragistics.Win.UltraWinGrid.ColumnFilter e)
        {
            _filter.RemoveAll(x => x.ColumnName == e.Column.Key);
        }

        public void ApplyUserFilter(Infragistics.Win.UltraWinGrid.ColumnFilter gridFilter)
        {
            _filter.RemoveAll(
                x => x.ColumnName == gridFilter.Column.Key
                && x.UserDefined);
            _filter.Add(gridFilter.Convert());
        }

        public Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter GetColumnFilterOne(Infragistics.Win.UltraWinGrid.ColumnFilter gridFilter)
        {
            Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter currFilter = null;
            FilterCondition filterCondition = (FilterCondition)gridFilter.FilterConditions.All.FirstOrDefault();
            if (filterCondition != null)
            {
                var currFilterItem = (FilterCondition)filterCondition;
                if (String.IsNullOrEmpty(currFilterItem.CompareValue == null ? String.Empty : currFilterItem.CompareValue.ToString()))
                {
                    currFilterItem.CompareValue = DBNull.Value;
                }
                var currFilterName = currFilterItem.Column.Key;
                var currFilterComparison = currFilterItem.ComparisionOperator;
                var currFilterValue = currFilterItem.CompareValue;

                currFilter = new Luxoft.NDS2.Common.Contracts.DTO.Query.ColumnFilter();
                currFilter.ComparisonOperator = currFilterItem.ComparisionOperator.Convert();
                currFilter.Value = currFilterItem.CompareValue;
            }

            return currFilter;
        }

        public void Reset(UltraGridBand band)
        {
            band.ColumnFilters.ClearAllFilters();

            if (Default.HasAnyFilter())
            {
                foreach (var column in band.Columns)
                {
                    var filter = band.ColumnFilters[column.Key];
                    filter.ClearFilterConditions();
                    filter.LogicalOperator = FilterLogicalOperator.Or;

                    foreach (var condition in Default.ColumnFilter(column.Key))
                    {
                        filter
                            .FilterConditions
                            .Add(condition);
                    }

                    if (filter.FilterConditions.Count > 0)
                    {
                        ApplyUserFilter(filter);
                    }
                }
            }
            else
            {
                _filter.Clear();
            }
        }        
    }
}
