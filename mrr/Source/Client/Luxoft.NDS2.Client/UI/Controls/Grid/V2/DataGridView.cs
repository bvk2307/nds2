﻿using Infragistics.Excel;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinListView;
using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Adapters;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Addins;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns.IndexColumn;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.CustomSettings;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Helpers;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination;
using Luxoft.NDS2.Client.UI.Controls.Sur;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using ColumnBase = Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns.ColumnBase;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2
{
    public partial class DataGridView : UserControl, IDataGridView, IGrid, IGridContextMenu
    {
        # region Плагины

        public void RegisterRowAddin(IRowAddin addin)
        {
            _grid.RegisterRowAddin(addin);
        }

        public void RegisterTableAddin(ITableAddin addin)
        {
            _grid.RegisterTableAddin(addin);
        }

        # endregion

        #region Индикатор прогресса

        private ProgressIndicator _progressIndicator;

        public ProgressIndicator ProgressIndicator
        {
            get { return _progressIndicator; }
        }

        #endregion     

        # region Конструктор

        private bool _lockSorting;

        public DataGridView()
        {
            InitializeComponent();
            InitTooltip();

            _progressIndicator = new ProgressIndicator(this, pnlMain);

            _grid.Grid.DisplayLayout.Override.HeaderClickAction = HeaderClickAction.ExternalSortSingle;
            _grid.Grid.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.True;
            _grid.Grid.DisplayLayout.Override.AllowRowSummaries = AllowRowSummaries.True;
            _grid.Grid.DisplayLayout.Override.AllowUpdate = DefaultableBoolean.True;
            _grid.Grid.DisplayLayout.Override.RowFilterAction = RowFilterAction.AppearancesOnly;
            _grid.Grid.AfterRowActivate += grid_AfterRowActivate;
            _grid.Grid.DoubleClickRow += grid_DoubleClickRow;
            _grid.Grid.BeforeRowFilterDropDown += grid_BeforeRowFilterDropDown;
            _grid.Grid.BeforeCustomRowFilterDialog += grid_BeforeCustomRowFilterDialog;

            _grid.Grid.BeforeSortChange +=
                (sender, args) =>
                {
                    if (BeforeSortingChanged != null && !_lockSorting)
                    {
                        BeforeSortingChanged(args.SortedColumns);
                    }

                    //aip включить прогресс
                    if (!this.PanelLoadingVisible && _grid.Grid.Rows.Count > 0)
                        this.PanelLoadingVisible = true;
                };

            _grid.Grid.BeforeRowFilterChanged +=
                (sender, args) =>
                {
                    if (BeforeFilterChanged != null && !_lockSorting)
                    {
                        BeforeFilterChanged(args.NewColumnFilter);
                    }

                    //aip включить прогресс
                    if (!this.PanelLoadingVisible && _grid.Grid.Rows.Count > 0)
                        this.PanelLoadingVisible = true;
                };

            _grid.Grid.AfterSortChange +=
                (sender, args) =>
                {
                    if (AfterSortingChanged != null && !_lockSorting)
                    {
                        AfterSortingChanged();
                    }

                    //aip выключить прогресс
                    if (this.PanelLoadingVisible)
                        this.PanelLoadingVisible = false;
                };

            _grid.Grid.AfterRowFilterChanged +=
                (sender, args) =>
                {
                    if (AfterFilterChanged != null && !_lockSorting)
                    {
                        AfterFilterChanged();
                    }

                    //aip выключить прогресс
                    if (this.PanelLoadingVisible)
                        this.PanelLoadingVisible = false;
                };

            _grid.AfterDataSourceChanged +=
                () =>
                {
                    if (AfterDataSourceChanged != null)
                    {
                        AfterDataSourceChanged();
                    }

                    //aip выключить прогресс
                    if(this.PanelLoadingVisible)
                        this.PanelLoadingVisible = false;
                };

            _grid.BeforeShowContextMenu +=
                (sender, eventArgs) =>
                {
                    if (BeforeShowContextMenu != null)
                    {
                        BeforeShowContextMenu(sender, eventArgs);
                    }
                };
        }

        # endregion

        # region Инициализация колонок

        public void ClearColumns()
        {
            _grid.ClearColumns();
        }

        public void InitColumns(GridColumnSetup columnSetup)
        {
            _grid.InitColumns(columnSetup);
        }

        [Obsolete]
        public void InitColumns(
            Grid.V1.Setup.GridSetup columnSetup,
            DictionarySur surProvider = null, 
            List<PropertyBoundColumnBuilder> customBuilders = null)
        {
            var indexer =
                _pagerPanel.ClientArea.Controls.Count > 0
                    && _pagerPanel.ClientArea.Controls[0] is IPager
                ? (IIndexProvider)new PagerIndexProvider(_pagerPanel.ClientArea.Controls[0] as IPager)
                : new IndexProvider();

            _grid.InitColumns(
                columnSetup, 
                indexer, 
                surProvider, 
                customBuilders);
        }

        private void InitTooltip()
        {
            Infragistics.Win.UltraWinToolTip.UltraToolTipInfo ultraToolTipInfo1 = new Infragistics.Win.UltraWinToolTip.UltraToolTipInfo("Сбросить", Infragistics.Win.ToolTipImage.None, "", Infragistics.Win.DefaultableBoolean.True);
            Infragistics.Win.UltraWinToolTip.UltraToolTipInfo ultraToolTipInfo2 = new Infragistics.Win.UltraWinToolTip.UltraToolTipInfo("Настроить видимость столбцов", Infragistics.Win.ToolTipImage.None, "", Infragistics.Win.DefaultableBoolean.True);
            _hintExportExcel = new Infragistics.Win.UltraWinToolTip.UltraToolTipInfo("Экспорт в MS Excel", ToolTipImage.None, "", DefaultableBoolean.True);
            _hintExportExcelCancel = new Infragistics.Win.UltraWinToolTip.UltraToolTipInfo("Отмена экспорта в MS Excel", ToolTipImage.None, "", DefaultableBoolean.True);
            this.ultraToolTipManager1.SetUltraToolTip(this._resetSettingButton, ultraToolTipInfo1);
            this.ultraToolTipManager1.SetUltraToolTip(this.btnColumnVisibilitySetup, ultraToolTipInfo2);
            this.ultraToolTipManager1.SetUltraToolTip(this.btnExportExcel, _hintExportExcel);
            this.ultraToolTipManager1.SetUltraToolTip(this.btnExportExcelCancel, _hintExportExcelCancel);
            this.ultraToolTipManager1.DisplayStyle = ToolTipDisplayStyle.Standard;
        }

        # endregion

        # region Реализация IDataGridView

        public Workbook ExportToWorkbook(WorkbookFormat format = WorkbookFormat.Excel2007)
        {
            try
            {
                var ultraGrid = _grid.Grid;
                var workBook = gridExporter.Export(ultraGrid);
                workBook.SetCurrentFormat(format);
                return workBook;
            }
            catch (Exception exception)
            {
                //todo. add logs
            }

            return null;
        }

        public void PushData<TDataRow>(IEnumerable<TDataRow> data)
        {
            _grid.PushData(data);
        }

        public void UpdateRow<TData>(TData obj, Func<TData, TData, bool> comparer = null)
        {
            _grid.UpdateRow(obj, comparer);
        }

        public GridColumnSetup Columns
        {
            get
            {
                return _grid.Columns;
            }
        }

        public IDataGridViewManager SettingsManager()
        {
            return new UserDefinedSettingsManager(_grid.Grid.Band(), Columns);
        }

		  public IDataGridViewManager DefaultSettingsManager()
        {
			  return new DefaultSettingsManager(_grid.Grid.Band(), Columns);
        }

        public bool AllowResetSettings
        {
            get
            {
                return _resetSettingButton.Enabled;
            }
            set
            {
                _resetSettingButton.Enabled = value;
            }
        }

        public bool AllowFilterReset
        {
            get
            {
                return btnFilterReset.Enabled;
            }
            set
            {
                btnFilterReset.Enabled = value;
            }
        }

        # endregion

        # region Реализация IGrid

        public event ParameterlessEventHandler AfterDataSourceChanged;

        # endregion

        #region UltraGrid

        public UltraGrid Grid
        {
            get { return _grid.Grid; }
        }

        public ColumnFiltersCollection ColumnFilters
        {
            get
            {
                return _grid.Grid.DisplayLayout.Bands[0].ColumnFilters;
            }
        }

        #endregion

        # region Сортировка/Группировака

        public void LockEvents()
        {
            _lockSorting = true;
        }

        public void UnlockEvents()
        {
            _lockSorting = false;
        }

        public event GenericEventHandler<SortedColumnsCollection> BeforeSortingChanged;

        public event GenericEventHandler<ColumnFilter> BeforeFilterChanged;

        public event ParameterlessEventHandler AfterSortingChanged;

        public event ParameterlessEventHandler AfterFilterChanged;

        # endregion

        #region Агрегация

        bool _shouldCorrectDateFormat;
        private void grid_BeforeCustomRowFilterDialog(object sender, BeforeCustomRowFilterDialogEventArgs e)
        {
            e.CustomRowFiltersDialog.Grid.BeforeCellListDropDown += Grid_BeforeCellListDropDown;
            e.CustomRowFiltersDialog.FormClosing += CustomRowFiltersDialog_FormClosing;

            if (e.Column.DataType == typeof(DateTime?))
            {
                _shouldCorrectDateFormat = true;
                e.CustomRowFiltersDialog.Grid.DisplayLayout.Bands[0].Columns[1].Format = "dd.MM.yyyy";
            }

            var operand = e.CustomRowFiltersDialog.Grid.Rows[0].Cells["Operand"].Column;
            operand.FormatInfo = e.Column.FormatInfo;
            operand.Format = e.Column.Format;
        }

        private void grid_BeforeRowFilterDropDown(object sender, BeforeRowFilterDropDownEventArgs e)
        {
            Columns.Columns.Find(e.Column.Key).OptionsBuilder().AutoComplete(e.ValueList);
        }

        void CustomRowFiltersDialog_FormClosing(object sender, FormClosingEventArgs e)
        {
            _shouldCorrectDateFormat = false;
        }

        private void Grid_BeforeCellListDropDown(object sender, CancelableCellEventArgs e)
        {
            var values = (ValueList)e.Cell.ValueListResolved;

            #region Отрубить лишние записи

            var listToRemove = new List<ValueListItem>();
            for (int i = 0; i < values.ValueListItems.Count; i++)
            {
                var val = (ValueListItem)values.ValueListItems.GetItem(i);
                if (val == null) continue;
                if (val.ToString().Contains("["))
                {
                    listToRemove.Add(val);
                }
            }

            if (listToRemove.Any())
            {
                foreach (var elem in listToRemove)
                {
                    values.ValueListItems.Remove(elem);
                }
            }

            #endregion Отрубить лишние записи

            if (_shouldCorrectDateFormat && e.Cell.Column.Index == 1)
            {
                CutTimePartDateTime(values);
            }
        }

        private void CutTimePartDateTime(ValueList values)
        {
            #region Отрубить время

            for (int i = 0; i < values.ValueListItems.Count; i++)
            {
                DateTime outDate;

                if (DateTime.TryParse(values.ValueListItems[i].DataValue.ToString(), out outDate))
                {
                    values.ValueListItems[i].DisplayText = outDate.ToString("dd.MM.yyyy");

                    if (values.ValueListItems.All.Count(x => ((ValueListItem)x).DisplayText == values.ValueListItems[i].DisplayText) > 1)
                    {
                        values.ValueListItems.RemoveAt(i);
                        i--;
                    }
                }
            }

            #endregion Отрубить время
        }

        #endregion Фильтрация

        #region Видимости столбцов

        public bool ColumnVisibilitySetupButtonVisible
        {
            get { return btnColumnVisibilitySetup.Visible; }
            set { btnColumnVisibilitySetup.Visible = value; }
        }

        public event GenericEventHandler<string, bool> ColumnVisibilityChanged;

        private IEnumerable<Tuple<string, string>> GetVisibilityList(IEnumerable<ColumnBase> collection)
        {
            foreach (var item in collection)
            {
                var group = item as ColumnGroup;
                if (group != null)
                {
                    var title = string.IsNullOrWhiteSpace(group.ListTitle()) ? string.Empty : group.ListTitle() + "->";
                    foreach (var subItem in GetVisibilityList(group.Columns))
                        yield return new Tuple<string, string>(subItem.Item1, title + subItem.Item2);
                }
                else
                {
                    yield return new Tuple<string, string>(item.Key, item.ListTitle());
                }
            }
        }

        private void btnColumnVisibilitySetup_DroppingDown(object sender, CancelEventArgs e)
        {
            columnVisibilityManager.Items.Clear();

            foreach (var item in GetVisibilityList(Columns.Columns.Where(x => x.AllowToggleVisibility)))
            {
                var newItem = new UltraListViewItem
                {
                    Key = item.Item1,
                    Value = item.Item2,
                    CheckState =
                        _grid.Grid.Band().Columns[item.Item1].Hidden
                            ? CheckState.Unchecked
                            : CheckState.Checked
                };

                columnVisibilityManager.Items.Add(newItem);
            }
        }

        private void columnVisibilityManager_ItemCheckStateChanged(object sender, ItemCheckStateChangedEventArgs e)
        {
            if (e.Item.CheckState == CheckState.Indeterminate)
            {
                return;
            }

            if (ColumnVisibilityChanged != null)
            {
                ColumnVisibilityChanged(e.Item.Key, e.Item.CheckState == CheckState.Checked);
            }
        }

        public void SetColumnVisibility(string key, bool visible)
        {
            if (ColumnVisibilityChanged != null)
            {
                ColumnVisibilityChanged(key, visible);
            }
        }

        #endregion Видимости столбцов

        # region Readonly

        /// <summary>
        /// Выставляет контрол в режим чтения(колонки выбора записей не обрабатываются)
        /// </summary>
        /// <param name="state">состояние(true - в режим чтения, false - нормальный режим)</param>
        public void SetReadOnlyState(bool state)
        {
            foreach (var column in Columns.All().Values)
            {
                column.ToggleEdit(state);
            }
        }

        # endregion

        #region События

        [Category("Luxoft")]
        [Description("Fires when row selection is checked")]
        public event EventHandler OnGridRowsCheckedStateChanged;

        [Category("Luxoft")]
        [Description("Fires when row selection is changed")]
        public event EventHandler SelectRow;

        [Category("Luxoft")]
        [Description("Fires when press button ExportExcel")]
        public event EventHandler OnExportExcel;

        [Category("Luxoft")]
        [Description("Fires when press button ExportExcelCancel")]
        public event EventHandler OnExportExcelCancel;

        #region Выбор строки

        //Выделение строки
        private void grid_AfterRowActivate(object sender, EventArgs e)
        {
            if ((SelectRow != null) && (_grid.Grid.ActiveRow != null))
            {
                SelectRow(this, e);
            }
        }

        //Двойной щелчек на строке
        public Action<object> RowDoubleClicked { get; set; }
        private void grid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (RowDoubleClicked == null)
            {
                return;
            }

            RowDoubleClicked(e.Row.ListObject);
        }

        public T GetCurrentItem<T>()
        {
            T ret = default(T);

            if (_grid.Grid.ActiveRow != null)
            {
                ret = (T)_grid.Grid.ActiveRow.ListObject;
            }
            return ret;
        }


        #endregion Выбор строки

        #endregion События

        # region Постраничный вывод данных

        public DataGridView WithPager(Control control)
        {
            _pagerPanel.ClientArea.Controls.Clear();
            _pagerPanel.Size = control.Size;
            _pagerPanel.ClientArea.Controls.Add(control);

            return this;
        }

        # endregion

        #region Lux свойства грида

        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Description("Show/Hide filters")]
        [Category("Luxoft")]
        public DefaultableBoolean AllowRowFiltering
        {
            get
            {
                return _grid.Grid.DisplayLayout.Override.AllowRowFiltering;
            }
            set
            {
                _grid.Grid.DisplayLayout.Override.AllowRowFiltering = value;
            }
        }

        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Description("Show/Hide the top panel with aggregates")]
        [Category("Luxoft")]
        public bool AggregatePanelVisible
        {
            get
            {
                return pnlRoot.Visible;
            }
            set
            {
                pnlRoot.Visible = value;
            }
        }

        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Description("Manage grid column auto fit")]
        [Category("Luxoft")]
        public AutoFitStyle ColumnsAutoFitStyle
        {
            get
            {
                return _grid.Grid.DisplayLayout.AutoFitStyle;
            }
            set
            {
                _grid.Grid.DisplayLayout.AutoFitStyle = value;
            }
        }

        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Description("Show/Hide the pages panel")]
        [Category("Luxoft")]
        public bool PanelPagesVisible
        {
            get
            {
                return _pagerPanel.Visible;
            }
            set
            {
                _pagerPanel.Visible = value;
            }
        }

        private bool _allowMultiGrouping = true;
        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Description("Show/Hide the top panel with aggregates")]
        [Category("Luxoft")]
        public bool AllowMultiGrouping
        {
            get
            {
                return _allowMultiGrouping;
            }
            set
            {
                _allowMultiGrouping = value;
            }
        }

        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Description("Show/Hide the loading panel")]
        [Category("Luxoft")]
        public bool PanelLoadingVisible
        {
            get
            {
                return pnlLoading.Visible;
            }
            set
            {
                pnlLoading.Visible = value;
            }
        }

        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Description("Show/Hide footer panel")]
        [Category("Luxoft")]
        public bool FooterVisible
        {
            get
            {
                return pnlGridBottom.Visible;
            }
            set
            {
                pnlGridBottom.Visible = value;
            }
        }

        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Description("Sets grid title")]
        [Category("Luxoft")]
        public string Title
        {
            get
            {
                return lblGridName.Text;
            }
            set
            {
                lblGridName.Value = value;
            }
        }

        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Description("ContextMenuStrip for grid only")]
        [Category("Luxoft")]
        public ContextMenuStrip GridContextMenuStrip
        {
            get
            {
                return _grid.GridContextMenuStrip;
            }
            set
            {
                _grid.GridContextMenuStrip = value;
            }
        }

        private Infragistics.Win.UltraWinToolTip.UltraToolTipInfo _hintExportExcel;
        
        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Description("ExportExcel button tooltip")]
        [Category("Luxoft")]
        public string ExportExcelHint
        {
            get
            {
                return _hintExportExcel.ToolTipText;
            }
            set
            {
                _hintExportExcel.ToolTipText = value;
            }
        }

        private Infragistics.Win.UltraWinToolTip.UltraToolTipInfo _hintExportExcelCancel;

        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Description("ExportExcelCancel button tooltip")]
        [Category("Luxoft")]
        public string ExportExcelCancelHint
        {
            get
            {
                return _hintExportExcelCancel.ToolTipText;
            }
            set
            {
                _hintExportExcelCancel.ToolTipText = value;
            }
        }
        
        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Description("Show/Hide ExportExcel button")]
        [Category("Luxoft")]
        public bool ExportExcelVisible
        {
            get
            {
                return btnExportExcel.Visible;
            }
            set
            {
                btnExportExcel.Visible = value;
            }
        }

        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Description("Show/Hide ExportExcelCancel button")]
        [Category("Luxoft")]
        public bool ExportExcelCancelVisible
        {
            get
            {
                return btnExportExcelCancel.Visible;
            }
            set
            {
                btnExportExcelCancel.Visible = value;
            }
        }

        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Description("Show/Hide the loading PanelExportExcelState")]
        [Category("Luxoft")]
        public bool PanelExportExcelStateVisible
        {
            get
            {
                return ultraPanelExportExcelState.Visible;
            }
            set
            {
                ultraPanelExportExcelState.Visible = value;
            }
        }

        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Description("Show/Hide FilterReset button")]
        [Category("Luxoft")]
        public bool FilterResetVisible
        {
            get
            {
                return btnFilterReset.Visible;
            }
            set
            {
                btnFilterReset.Visible = value;
            }
        }

        private ContextMenuStrip _subContextMenu;

        #endregion Lux свойства грида

        #region Save and Load Settings

        public event ParameterlessEventHandler SaveSettings;

        public event ParameterlessEventHandler ResetSettings;

        public event ParameterlessEventHandler FilterResetSettings;

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (SaveSettings != null)
            {
                SaveSettings();
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            if (ResetSettings != null)
            {
                ResetSettings();
            }
        }

        #endregion Save and Load Settings

        # region Обновление

        public void Refresh()
        {
            _grid.Grid.Refresh();

            //aip включить прогресс
            if (this.PanelLoadingVisible)
                this.PanelLoadingVisible = false;

        }

        public void RefreshRow(int dataIndex)
        {
            foreach (var row in _grid.Grid.Rows)
            {
                if (row.ListIndex == dataIndex)
                {
                    row.Refresh();
                }
            }
        }

        # endregion

        # region Экспорт в эксель

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            if (OnExportExcel != null)
            {
                OnExportExcel(sender, e);
            }
        }

        private void btnExportExcelCancel_Click(object sender, EventArgs e)
        {
            if (OnExportExcelCancel != null)
            {
                OnExportExcelCancel(sender, e);
            }
        }

        public void SetExportExcelState(string stateText)
        {
            labelExportExcelState.Text = stateText;
        }

        public void SetExportExcelEnabled(bool isEnabled)
        {
            btnExportExcel.Enabled = isEnabled;
        }

        public void SetPanelExportExcelStateWidth(int width)
        {
            ultraPanelExportExcelState.Width = width;
        }

        # endregion

        # region Очистка фильтра

        private void btnFilterReset_Click(object sender, EventArgs e)
        {
            if (FilterResetSettings != null)
            {
                FilterResetSettings();
            }
        }

        # endregion

        # region Реализация IDataGridView

        public event EventHandler<CommonComponents.Uc.Infrastructure.Interface.EventArgs<UltraGridRow>> BeforeShowContextMenu;

        #endregion
    }
}