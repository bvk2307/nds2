﻿using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.ColumnExpressions;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns.IndexColumn;
using Luxoft.NDS2.Client.UI.Controls.Grid.V1.Setup;
using Luxoft.NDS2.Client.UI.Controls.Sur;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.Helpers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using retired = Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.Adapters
{
    public class GridSetupAdapter
    {
        private readonly GridSetup _setup;

        private readonly List<IColumnBuilder> _builders =
            new List<IColumnBuilder>();

        public GridSetupAdapter(GridSetup setup, IIndexProvider indexProvider, IGrid grid, IColumnExpression expression, DictionarySur surProvider, List<PropertyBoundColumnBuilder> customBuilders = null)
        {
            _setup = setup;

            if (customBuilders != null && customBuilders.Any())
            {
                _builders.AddRange(customBuilders);
            }

            _builders.Add(new CheckColumnBuilder(grid, expression));
            _builders.Add(new HyperlinkColumnBuilder(grid, expression));
            _builders.Add(new IndexColumnBuilder(grid, indexProvider, expression));
            _builders.Add(new SurColumnBuilder(grid, surProvider, expression));
            _builders.Add(new PropertyBoundColumnBuilder(grid, expression));
            _builders.Add(new ColumnGroupBuilder(grid, _builders, expression));            
        }

        public GridColumnSetup BuildSetup()
        {
            var result = new GridColumnSetup();

            foreach (var columnData in _setup.Columns)
            {
                var applicableBuilder = 
                    _builders.FirstOrDefault(builder => builder.IsApplicable(columnData));

                if (applicableBuilder != null 
                    && (columnData is retired.ColumnGroupDefinition 
                        || ((retired.ColumnDefinition)columnData).Visibility != retired.ColumnVisibility.Disabled))
                {
                    result.Columns.Add(applicableBuilder.Build(columnData));
                }
            }

            return result;
        }

        public bool HasSummaryColumns()
        {
            foreach (var columnBase in _setup.Columns)
            {
                var column = columnBase as retired.ColumnDefinition;

                if (column != null && column.AllowSummary)
                {
                    return true;
                }

                var group = columnBase as retired.ColumnGroupDefinition;

                if (group != null && HasSummaryColumns(group))
                {
                    return true;
                }
            }

            return false;
        }

        private bool HasSummaryColumns(retired.ColumnGroupDefinition group)
        {
            if (group.Columns.Any(c => c.AllowSummary))
            {
                return true;
            }

            if (group.SubGroups.Any(HasSummaryColumns))
            {
                return true;
            }

            return false;
        }
    }

    public abstract class ColumnBuilderBase : IColumnBuilder
    {
        protected IGrid Grid
        {
            get;
            private set;
        }

        protected IColumnExpression ColumnExpression
        {
            get;
            private set;
        }

        protected ColumnBuilderBase(IGrid grid, IColumnExpression columnExpression)
        {
            Grid = grid;
            ColumnExpression = columnExpression;
        }

        public abstract bool IsApplicable(retired.ColumnBase initialColumn);

        public abstract ColumnBase Build(retired.ColumnBase initialColumn);
    }

    public class ColumnGroupBuilder : ColumnBuilderBase
    {
        private readonly List<IColumnBuilder> _builders;

        public ColumnGroupBuilder(IGrid grid, List<IColumnBuilder> builders, IColumnExpression columnExpression)
            : base(grid, columnExpression)
        {
            _builders = builders;
        }

        public override bool IsApplicable(retired.ColumnBase initialColumn)
        {
            return initialColumn is retired.ColumnGroupDefinition;
        }

        public override ColumnBase Build(retired.ColumnBase initialColumn)
        {
            var groupData = initialColumn as retired.ColumnGroupDefinition;
            var group = new ColumnGroup(groupData.Key, Grid);

            group.Caption = groupData.Caption;
            group.HeaderSpanY = groupData.RowSpan;
            group.Hidden = true;

            foreach (var subColumn in groupData.Columns)
            {
                var builder = 
                    _builders.FirstOrDefault(x => x.IsApplicable(subColumn));

                if (builder != null)
                {
                    group.Columns.Add(builder.Build(subColumn));
                }
            }

            foreach (var subGroup in groupData.SubGroups)
            {
                group.Columns.Add(Build(subGroup));
            }

            return group;
        }
    }

    public abstract class SingleColumnBuilder : ColumnBuilderBase
    {
        protected SingleColumnBuilder(IGrid grid, IColumnExpression columnExpression)
            : base(grid, columnExpression)
        {
        }

        public override bool IsApplicable(retired.ColumnBase initialColumn)
        {
            return initialColumn is retired.ColumnDefinition;
        }

        public override ColumnBase Build(retired.ColumnBase initialColumn)
        {
            var data = initialColumn as retired.ColumnDefinition;
            var column = Create(initialColumn.Key);
            Customize(column, data);

            return column;
        }

        protected virtual SingleColumn Create(string key)
        {
            return new SingleColumn(key, Grid, ColumnExpression);
        }

        protected virtual void Customize(ColumnBase baseColumn, retired.ColumnDefinition data)
        {
            var column = (SingleColumn)baseColumn;

            column.HeaderSpanY = data.RowSpan;
            column.Caption = data.Caption;
            column.CellTextAlign = (HorizontalAlign)data.Align;
            column.CustomConditionValueAppearance = data.CustomConditionValueAppearance;
            column.DataFormat = data.Format;
            column.DataFormatInfo = data.FormatInfo;
            column.DisableEdit = !data.EditableDefinition.Editable;
            column.DisableFilter = 
                data.AllowRowFiltering.HasValue && !data.AllowRowFiltering.Value;
            column.HeaderToolTip = data.ToolTip;
            column.Hidden = data.Visibility == retired.ColumnVisibility.Hidden;
            column.MinWidth = data.Width;
            column.WrapText = data.CellMultiLine;
        }
    }

    public class PropertyBoundColumnBuilder : SingleColumnBuilder
    {
        public PropertyBoundColumnBuilder(IGrid grid, IColumnExpression columnExpression)
            : base(grid, columnExpression)
        {
        }

        protected override SingleColumn Create(string key)
        {
            return new PropertyBoundColumn(key, Grid, ColumnExpression);
        }

        protected override void Customize(ColumnBase baseColumn, retired.ColumnDefinition data)
        {
            base.Customize(baseColumn, data);

            var column = baseColumn as PropertyBoundColumn;

            column.CellClick = (sender) => data.CellClick(sender, new EventArgs());
            column.StyleDefinition.Handler =
                data.StyleDefinition == null
                            ? null
                            : (CellEventHandler)data.StyleDefinition.Handler;
            column.StyleDefinition.Image =
                data.StyleDefinition == null
                            ? null
                            : (Image)data.StyleDefinition.Image;
            column.StyleDefinition.Style =
                data.StyleDefinition == null
                            ? ColumnStyle.Default
                            : data.StyleDefinition.Style;
            column.IsHtml = data.IsHtml;
            column.EditableDefinition.Handler =
                data.EditableDefinition == null
                    ? (CellEventHandler)null
                    : data.EditableDefinition.Handler;
        }
    }

    public class IndexColumnBuilder : SingleColumnBuilder
    {
        private readonly IIndexProvider _indexProvider;

        public IndexColumnBuilder(IGrid grid, IIndexProvider indexProvider, IColumnExpression columnExpression)
            : base(grid, columnExpression)
        {
            _indexProvider = indexProvider;
        }

        public override bool IsApplicable(retired.ColumnBase initialColumn)
        {
            var definition = initialColumn as retired.ColumnDefinition;

            return definition != null
                && definition.IsAutoIncrement;
        }

        protected override SingleColumn Create(string key)
        {
            return new IndexColumn(key, Grid, _indexProvider, ColumnExpression);
        }
    }

    public class HyperlinkColumnBuilder : PropertyBoundColumnBuilder
    {
        public HyperlinkColumnBuilder(IGrid grid, IColumnExpression columnExpression)
            : base(grid, columnExpression)
        {
        }

        public override bool IsApplicable(retired.ColumnBase initialColumn)
        {
            var definition = initialColumn as retired.ColumnDefinition;

            return definition != null
                && definition.IsHyperLink;
        }

        protected override SingleColumn Create(string key)
        {
            return new HyperlinkColumn(key, Grid, ColumnExpressionCreator.CreateColumnExpressionDefault());
        }
    }

    public class CheckColumnBuilder : PropertyBoundColumnBuilder
    {
        public CheckColumnBuilder(IGrid grid, IColumnExpression columnExpression)
            : base(grid, columnExpression)
        {
        }

        public override bool IsApplicable(retired.ColumnBase initialColumn)
        {
            var definition = initialColumn as retired.ColumnDefinition;

            return definition != null
                && definition.SelectionColumn;
        }

        protected override SingleColumn Create(string key)
        {
            return new CheckColumn(key, Grid, ColumnExpressionCreator.CreateColumnExpressionDefault());
        }

        protected override void Customize(ColumnBase baseColumn, retired.ColumnDefinition data)
        {
            base.Customize(baseColumn, data);

            baseColumn.Caption = string.Empty;
        }
    }

    public class SurColumnBuilder : PropertyBoundColumnBuilder
    {
        private readonly DictionarySur _surProvider;
        private IGrid _iGrid;

        public SurColumnBuilder(IGrid grid, DictionarySur surProvider, IColumnExpression columnExpression)
            : base(grid, columnExpression)
        {
            _surProvider = surProvider;
            _iGrid = grid;
        }

        public override bool IsApplicable(retired.ColumnBase initialColumn)
        {
            var column = initialColumn as retired.ColumnDefinition;

            return column != null
                && (column.Key.Contains(TypeHelper<DeclarationSummary>.GetMemberName(t => t.SUR_CODE)) ||
                    column.Key.Contains(TypeHelper<Discrepancy>.GetMemberName(t => t.SURCode)));
        }

        protected override SingleColumn Create(string key)
        {
            return new SurColumn(key, _iGrid, _surProvider, ColumnExpression);
        }
    }
}
