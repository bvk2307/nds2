﻿using Infragistics.Win;
using grid = Infragistics.Win.UltraWinGrid;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns.AutoComplete
{
    public class CheckColumnFilterManager : IColumnFilterManager
    {
        private const string DefaultCheckedTitle = "Только отмеченные";

        private const string DefaultUncheckedTitle = "Только неотмеченные";

        private const int CheckedValue = 1;

        private const int UncheckedValue = 0;

        private readonly string _checkedTitle;

        private readonly string _uncheckedTitle;

        public CheckColumnFilterManager(string checkedTitle, string uncheckedTitle)
        {
            _checkedTitle = checkedTitle;
            _uncheckedTitle = uncheckedTitle;
        }

        public CheckColumnFilterManager()
            : this(DefaultCheckedTitle, DefaultUncheckedTitle)
        {            
        }

        public void AutoComplete(ValueList initialList)
        {
            var allItem = grid.Resources.Customizer.GetCustomizedString(Constants.AllItemKey);
            var itemsToDelete = 
                initialList
                    .ValueListItems
                    .WhereItem(item => item.DisplayText != allItem)
                    .ToArray();

            foreach (var item in itemsToDelete)
            {
                initialList.ValueListItems.Remove(item);
            }

            initialList.ValueListItems.Add(CheckedValue, _checkedTitle);
            initialList.ValueListItems.Add(UncheckedValue, _uncheckedTitle);
        }

        public void OnFilterChanged(grid.ColumnFilter gridFilter)
        {
        }
    }
}
