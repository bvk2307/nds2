﻿using CommonComponents.Uc.Infrastructure.Interface;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.UI.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2
{
    public interface IGridContextMenu
    {
        event EventHandler<EventArgs<UltraGridRow>> BeforeShowContextMenu;
    }
}
