﻿using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.ColumnExpressions;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns
{
    public class ColumnGroup : ColumnBase
    {
        # region Конструкторы

        public ColumnGroup(string key, IGrid grid)
            : base(key, grid, ColumnExpressionCreator.CreateColumnExpressionDefault())
        {
            Columns = new ColumnCollection();
            OriginX = -1;
        }

        # endregion

        # region Вложенные колонки

        public ColumnCollection Columns
        {
            get;
            private set;
        }

        # endregion

        # region Поиск колонок внутри группы

        public override bool Contains(string columnKey)
        {
            return Key == columnKey
                || Columns.Contains(columnKey);
        }

        public override Dictionary<string, ColumnBase> All()
        {
            var result = new Dictionary<string, ColumnBase>();

            foreach (var column in Columns)
            {
                foreach (var item in column.All())
                {
                    result.Add(string.Format("{0}->{1}", Caption, item.Key), item.Value);
                }
            }

            return result;
        }

        public override ColumnBase Find(string key)
        {
            return base.Find(key) ?? Columns.Find(key);
        }

        # endregion

        # region Настройка группы и вложенных колонок

        protected override void InitInternal()
        {
            Group();
        }

        protected UltraGridGroup Group()
        {
            var group = Band().Groups.Add(Key, Caption);
            group.RowLayoutGroupInfo.SpanY = group.RowLayoutGroupInfo.LabelSpan = HeaderSpanY;
            if (OriginX > -1)
                group.RowLayoutGroupInfo.OriginX = OriginX;
            group.Header.Appearance.TextTrimming = TextTrimming.None;
            group.Header.Appearance.TextHAlign = HAlign.Center;
            group.Header.Appearance.TextVAlign = VAlign.Top;

            foreach (var childColumnDefinition in Columns)
            {
                var subGroupDefinition = childColumnDefinition as ColumnGroup;

                if (subGroupDefinition != null)
                {
                    var subGroup = subGroupDefinition.Group();
                    subGroup.RowLayoutGroupInfo.ParentGroup = group;
                }
                else
                {
                    var childColumn = childColumnDefinition.CreateColumn();
                    childColumn.RowLayoutColumnInfo.ParentGroup = group;
                    childColumn.Group = group;
                }
            }

            return group;
        }

        protected override void SetupColumn()
        {
            throw new System.NotSupportedException();
        }

        public override void ToggleEdit(bool disableEdit)
        {
            foreach (var column in Columns)
            {
                column.ToggleEdit(disableEdit);
            }
        }

        protected override void SetCaption()
        {
            Band().Groups[Key].Header.Caption = Caption;
        }

        # endregion
    }
}
