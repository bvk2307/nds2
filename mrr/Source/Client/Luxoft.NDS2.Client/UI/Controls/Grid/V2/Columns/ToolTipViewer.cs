﻿using System.Collections.Generic;
using Infragistics.Win.UltraWinGrid;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns
{
    public interface IToolTipViewer
    {
        void SetToolTip(UltraGridCell cell);
    }

    public class ToolTipViewerBase : IToolTipViewer
    {
        public virtual string GetToolTip(object listObject)
        {
            return null;
        }

        public void SetToolTip(UltraGridCell cell)
        {
            var text = GetToolTip(cell.Row.ListObject);
            if (!string.IsNullOrWhiteSpace(text))
                cell.ToolTipText = text;
        }
    }

    public class DictionaryToolTipViewer : IToolTipViewer
    {
        private readonly Dictionary<string, string> _dictionary;

        public DictionaryToolTipViewer(Dictionary<string, string> dictionary)
        {
            _dictionary = dictionary;
        }

         public void SetToolTip(UltraGridCell cell)
        {
            string tip;
            if (_dictionary.TryGetValue(cell.Value.ToString(), out tip))
                cell.ToolTipText = tip;
        }
    }
}
