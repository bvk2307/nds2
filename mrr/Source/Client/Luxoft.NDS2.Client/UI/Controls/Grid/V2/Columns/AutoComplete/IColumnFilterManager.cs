﻿using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns.AutoComplete
{
    public interface IColumnFilterManager
    {
        void AutoComplete(ValueList initialList);

        void OnFilterChanged(ColumnFilter gridFilter);
    }
}
