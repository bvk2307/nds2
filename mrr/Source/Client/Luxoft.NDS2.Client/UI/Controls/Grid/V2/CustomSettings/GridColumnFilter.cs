﻿using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.CustomSettings
{
    [Serializable]
    public class GridColumnFilter
    {
        public string ColumnKey
        {
            get;
            set;
        }

        public List<FilterCondition> Conditions
        {
            get;
            set;
        }
    }
}
