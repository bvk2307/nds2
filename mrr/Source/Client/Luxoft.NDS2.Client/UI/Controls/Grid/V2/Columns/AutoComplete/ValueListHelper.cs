﻿using Infragistics.Win;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns.AutoComplete
{
    public static class ValueListHelper 
    {
        public static IEnumerable<ValueListItem> WhereItem(
            this ValueListItemsCollection items,
            Func<ValueListItem, bool> expression)
        {
            return items.All
                .Select(item => item as ValueListItem)
                .Where(item => item != null && expression((ValueListItem)item));
        }

        public static ValueList Skip(
            this ValueList collection,
            int skipItems)
        {
            var itemsToDelete =
                collection.ValueListItems.All.Skip(skipItems).ToList();

            foreach (var item in itemsToDelete)
            {
                collection.ValueListItems.Remove(item);
            }

            return collection;
        }

    }
}
