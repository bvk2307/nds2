﻿using Infragistics.Win.UltraWinGrid;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.Addins
{
    public interface ITableAddin
    {
        void Customize(UltraGrid grid);
    }
}
