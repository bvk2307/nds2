﻿using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.ColumnExpressions
{
    public class ColumnExpressionTaxPeriod : ColumnExpressionBase, IColumnExpression
    {
        private const string COLUMN_FULL_TAX_PERIOD = "FULL_TAX_PERIOD";
        private const string COLUMN_TAX_PERIOD_SORT = "TAX_PERIOD_SORT_ORDER";
        private const string COLUMN_FISCAL_YEAR = "FISCAL_YEAR";

        public override void AddSorting(UltraGridColumn sortedColumn, QueryConditions searchCriteria)
        {
            var filedNamesRemove = new List<string>() { COLUMN_FULL_TAX_PERIOD, COLUMN_TAX_PERIOD_SORT, COLUMN_FISCAL_YEAR };
            searchCriteria.Sorting.RemoveAll(p => filedNamesRemove.Contains(p.ColumnKey));

            CreateSorting(sortedColumn, searchCriteria);
        }

        public override void ApplySorting(UltraGridColumn sortedColumn, QueryConditions searchCriteria)
        {
            searchCriteria.Sorting.Clear();

            CreateSorting(sortedColumn, searchCriteria);
        }

        private void CreateSorting(UltraGridColumn sortedColumn, QueryConditions searchCriteria)
        {
            if (sortedColumn.SortIndicator == SortIndicator.Ascending)
            {
                searchCriteria.Sorting.Add(new ColumnSort() { ColumnKey = COLUMN_FISCAL_YEAR, Order = ColumnSort.SortOrder.Asc });
                searchCriteria.Sorting.Add(new ColumnSort() { ColumnKey = COLUMN_TAX_PERIOD_SORT, Order = ColumnSort.SortOrder.Asc });
            }

            if (sortedColumn.SortIndicator == SortIndicator.Descending)
            {
                searchCriteria.Sorting.Add(new ColumnSort() { ColumnKey = COLUMN_FISCAL_YEAR, Order = ColumnSort.SortOrder.Desc });
                searchCriteria.Sorting.Add(new ColumnSort() { ColumnKey = COLUMN_TAX_PERIOD_SORT, Order = ColumnSort.SortOrder.Desc });
            }
        }
    }
}
