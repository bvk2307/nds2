﻿using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System.Drawing;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.Addins
{
    public class SummaryAddin : ITableAddin
    {
        public SummaryAddin()
        {
        }

        public void Customize(UltraGrid grid)
        {
            var band = grid.DisplayLayout.Bands[0];

            band.Summaries.Clear();

            grid.DisplayLayout.Override.SummaryDisplayArea = SummaryDisplayAreas.GroupByRowsFooter | SummaryDisplayAreas.BottomFixed;
            band.Override.SummaryFooterCaptionAppearance.FontData.Bold = DefaultableBoolean.True;
            band.Override.SummaryFooterCaptionAppearance.BackColor = Color.WhiteSmoke;
            band.Override.SummaryFooterCaptionAppearance.ForeColor = Color.Black;
            band.Override.SummaryValueAppearance.BackColor = Color.WhiteSmoke;
            band.Override.SummaryFooterCaptionVisible = DefaultableBoolean.True;
            band.Override.SummaryValueAppearance.TextHAlign = HAlign.Right;
            band.Override.SummaryValueAppearance.FontData.Bold = DefaultableBoolean.True;
        }
    }
}
