﻿using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2
{
    public interface IColumnExpression
    {
        void ApplyFilter(Infragistics.Win.UltraWinGrid.ColumnFilter columnFilter, QueryConditions searchCriteria);
        void ApplySorting(Infragistics.Win.UltraWinGrid.UltraGridColumn sortedColumn, QueryConditions searchCriteria);
        void AddSorting(Infragistics.Win.UltraWinGrid.UltraGridColumn sortedColumn, QueryConditions searchCriteria);
    }
}
