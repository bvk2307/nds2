﻿using System;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination
{
    public class PageSizeOutOfRangeException : Exception
    {
        private const string MessagePattern =
            "Значение {0} не является допустимым для размера страницы списка данных. Допустимыми являются целочисленные значения от {1} до {2}.";

        public PageSizeOutOfRangeException(uint pageSize)
            : base(
                string.Format(
                    MessagePattern, 
                    pageSize, 
                    Defaults.MinPageSize, 
                    Defaults.MaxPageSize))
        {
            PageSize = pageSize;
        }

        public uint PageSize
        {
            get;
            private set;
        }

        public uint MaxPageSize
        {
            get
            {
                return Defaults.MaxPageSize;
            }
        }

        public uint MinPageSize
        {
            get
            {
                return Defaults.MinPageSize;
            }
        }
    }
}
