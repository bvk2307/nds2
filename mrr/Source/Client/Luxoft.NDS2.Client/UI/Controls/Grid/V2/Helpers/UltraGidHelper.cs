﻿using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.Helpers
{
    public static class UltraGidHelper
    {
        public static UltraGridBand Band(this UltraGrid grid, int bandIndex = 0)
        {
            return grid.DisplayLayout.Bands[bandIndex];
        }

        public static UltraGridColumn Column(this UltraGrid grid, string columnKey, int bandIndex = 0)
        {
            return grid.Band(bandIndex).Columns[columnKey];
        }

        public static UltraGridRow FirstRowWithChildBand(this UltraGrid grid)
        {
            foreach (var row in grid.Rows)
            {
                if (row.ChildBands.Count > 0)
                {
                    return row;
                }
            }

            return null;
        }

        public static string GetDecimalFormatString(this UltraGridColumn column, string defaultFormat = "")
        {
            if (String.IsNullOrEmpty(defaultFormat)) { defaultFormat = "{0:n2}"; }
            object format = column.FormatInfo != null ? column.FormatInfo.GetFormat(typeof(ICustomFormatter)) : null;
            if (format != null)
            {
                try
                {
                    return (format as IFormatter).GetStringFormat(typeof(decimal));
                }
                catch { }
            }
            return defaultFormat;
        }
    }
}
