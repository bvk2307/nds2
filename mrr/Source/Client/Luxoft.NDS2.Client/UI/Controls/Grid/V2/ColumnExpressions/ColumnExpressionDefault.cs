﻿using Luxoft.NDS2.Client.UI.Controls.Grid.V2.CustomSettings;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.ColumnExpressions
{
    public class ColumnExpressionDefault : ColumnExpressionBase
    {

        public override void ApplyFilter(Infragistics.Win.UltraWinGrid.ColumnFilter columnFilter, QueryConditions searchCriteria)
        {
            var filterBuilder = new CustomFilterBuilder(searchCriteria.Filter);
            filterBuilder.ApplyFilter(columnFilter);
        }

        public override void ApplySorting(Infragistics.Win.UltraWinGrid.UltraGridColumn sortedColumn, QueryConditions searchCriteria)
        {
            var sortBuilder = new CustomSortBuilder(searchCriteria.Sorting);
            sortBuilder.SortChanged(sortedColumn);
        }

        public override void AddSorting(Infragistics.Win.UltraWinGrid.UltraGridColumn sortedColumn, QueryConditions searchCriteria)
        {
            if (!searchCriteria.Sorting.Any(p => p.ColumnKey == sortedColumn.Key))
            {
                if (sortedColumn.SortIndicator == Infragistics.Win.UltraWinGrid.SortIndicator.Ascending)
                    searchCriteria.Sorting.Add(new ColumnSort() { ColumnKey = sortedColumn.Key, Order = ColumnSort.SortOrder.Asc });
                if (sortedColumn.SortIndicator == Infragistics.Win.UltraWinGrid.SortIndicator.Descending)
                    searchCriteria.Sorting.Add(new ColumnSort() { ColumnKey = sortedColumn.Key, Order = ColumnSort.SortOrder.Desc });
            }
        }
    }
}
