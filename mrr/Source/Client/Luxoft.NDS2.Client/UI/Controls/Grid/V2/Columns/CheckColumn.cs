﻿using System.Linq;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns
{
    public class CheckColumn : PropertyBoundColumn
    {
        # region Конструкторы

        public CheckColumn(string key, IGrid grid, IColumnExpression columnExpression)
            : base(key, grid, columnExpression)
        {
            AllowToggleVisibility = false;
            Caption = string.Empty;
            DisableHeaderSyncronization = true;
        }

        # endregion

        # region Параметры и обработчики событий

        public Action<object, bool> RowCheckedChange
        {
            get;
            set;
        }

        public Action<CheckState> HeaderCheckedChange
        {
            get;
            set;
        }

        public Func<object, bool> CheckEditableCriteria
        {
            get;
            set;
        }

        public bool DisableHeaderSyncronization
        {
            get;
            set;
        }

        public bool HideHeaderCheckbox
        {
            get;
            set;
        }

        public override string ListTitle()
        {
            return HeaderToolTip;
        }

        # endregion

        # region Настройка колонки

        private const int COLUMN_WIDTH = 30;

        private bool _needCallHeaderChangeHandler = true;

        protected override void SetupColumn()
        {
            var column = Column();

            column.Header.ToolTipText = HeaderToolTip;
            column.MinWidth = COLUMN_WIDTH;
            column.Width = COLUMN_WIDTH;
            column.MaxWidth = COLUMN_WIDTH;
            column.LockedWidth = true;
            column.Header.CheckBoxVisibility = 
                HideHeaderCheckbox 
                ? HeaderCheckBoxVisibility.Never 
                : HeaderCheckBoxVisibility.Always;

            column.Header.CheckBoxSynchronization = 
                DisableHeaderSyncronization
                ? HeaderCheckBoxSynchronization.None
                : HeaderCheckBoxSynchronization.Default;
            
            column.DataType = typeof(bool);
            column.CellActivation = Activation.AllowEdit;
            column.CellClickAction = CellClickAction.Edit;
            column.AllowRowSummaries = AllowRowSummaries.False;
            column.AllowGroupBy = DefaultableBoolean.False;
            column.AllowRowFiltering =
                DisableFilter ? DefaultableBoolean.False : DefaultableBoolean.True;
            column.SortIndicator =
                DisableSort ? SortIndicator.Disabled : SortIndicator.None;

            Grid.Grid.InitializeRow += OnInitializeRow;
            Grid.Grid.CellChange += OnCellChange;
            Grid.Grid.AfterHeaderCheckStateChanged += OnAfterHeaderCheckStateChanged;
            Grid.Grid.BeforeHeaderCheckStateChanged += OnBeforeHeaderCheckStateChanged;
        }

        # endregion

        # region Логика работы колонки

        private bool _isUserCheck; 

        private CheckState _lastColumnSetValue = CheckState.Unchecked;

        private void OnInitializeRow(object sender, InitializeRowEventArgs e)
        {            
            e.Row.Cells[Key].Activation =
                        DisableEdit || (
                            CheckEditableCriteria != null 
                                && !CheckEditableCriteria(e.Row.ListObject))
                            ? Activation.Disabled
                            : Activation.AllowEdit;
        }

        public virtual void RefreshHeaderState()
        {
            var self = Grid.Grid.DisplayLayout.Bands[0].Columns[Key];
            var cnt = self.Layout.Rows.Count;
            var checkedRows = self.Layout.Rows.Where(x => x.Cells[Key].Text == "True").ToArray();
            var checkedCnt = checkedRows.Count();
            var state = (cnt > 0 && checkedCnt == cnt) ? CheckState.Checked : CheckState.Unchecked;
            if (state == self.GetHeaderCheckedState(self.Layout.Rows))
                return;

            Grid.Grid.BeginUpdate();
            _needCallHeaderChangeHandler = false;
            self.SetHeaderCheckedState(self.Layout.Rows, state);
            _needCallHeaderChangeHandler = true;
            //foreach (var row in self.Layout.Rows)
            //{
            //    row.Cells[Key].Value = checkedRows.Contains(row);
            //}
            Grid.Grid.EndUpdate();
        }

        private void OnCellChange(object sender, CellEventArgs e)
        {
            if (e.Cell.Column.Key != Key)
                return;

            if (RowCheckedChange != null)
                RowCheckedChange(e.Cell.Row.ListObject, Convert.ToBoolean(e.Cell.Text));

            RefreshHeaderState();
        }

        private void OnBeforeHeaderCheckStateChanged(object sender, BeforeHeaderCheckStateChangedEventArgs e)
        {
            _isUserCheck = _lastColumnSetValue != e.NewCheckState && e.IsCancelable;

            if (_isUserCheck)
            {
                _lastColumnSetValue = e.NewCheckState; //Нельзя канселять ивент после этой строчки
            }
        }

        private void OnAfterHeaderCheckStateChanged(object sender, AfterHeaderCheckStateChangedEventArgs e)
        {
            var state = e.Column.GetHeaderCheckedState(e.Rows);

            if (!_isUserCheck)
            {
                e.Column.SetHeaderCheckedState(e.Rows, _lastColumnSetValue); // Это магия. Если вызвать этот метод тут, то события Before и After не вызываются и строки грида не синхронизируются, чего нам в этом случае и надо
                return;
            }
            if (HeaderCheckedChange != null && _needCallHeaderChangeHandler)
                HeaderCheckedChange(state);
        }               

        # endregion

        # region Управление состоянием

        public override void ToggleEdit(bool disableEdit)
        {
            base.ToggleEdit(disableEdit);

            Column().Header.CheckBoxVisibility =
                DisableEdit || disableEdit || HideHeaderCheckbox
                ? HeaderCheckBoxVisibility.Never 
                : HeaderCheckBoxVisibility.Always;
        }

        public void SetHeaderCheckState(CheckState state)
        {
            _lastColumnSetValue = state;
            Column().SetHeaderCheckedState(Grid.Grid.Rows, _lastColumnSetValue);
        }

        # endregion
    }
}
