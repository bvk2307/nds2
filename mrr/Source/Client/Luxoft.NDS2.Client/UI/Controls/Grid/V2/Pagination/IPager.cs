﻿using Luxoft.NDS2.Client.UI.Base;
namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination
{
    public interface IPager
    {
        bool SuppressEvents { get; set; }

        # region Состояние

        uint PageIndex
        {
            get;
            set;
        }

        ushort PageSize
        {
            get;
            set;
        }

        uint TotalDataRows
        {
            get;
            set;
        }

        uint TotalMatches
        {
            get;
            set;
        }

        uint PageCount
        {
            get;
        }

        bool HasTotals
        {
            get;
        }

        bool AllowPrevPage
        {
            get;
        }

        bool AllowNextPage
        {
            get;
        }

        # endregion

        # region События

        event ParameterlessEventHandler DataListChanging;

        event ParameterlessEventHandler ViewStateChanged;

        # endregion

        # region Навигация по страницам

        void NextPage();

        void PrevPage();

        void FirstPage();

        void LastPage();

        # endregion

        # region Методы

        void SetFirstPage();

        # endregion
    }
}
