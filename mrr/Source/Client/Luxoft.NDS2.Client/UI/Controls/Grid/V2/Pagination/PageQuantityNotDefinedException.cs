﻿using System;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination
{
    public class PageQuantityNotDefinedException : Exception
    {
        public PageQuantityNotDefinedException()
            : base("Кол-во страниц не определено")
        {
        }
    }
}
