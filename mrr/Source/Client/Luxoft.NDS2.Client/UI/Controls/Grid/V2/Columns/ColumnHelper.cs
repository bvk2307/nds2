﻿using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.CheckBoolColumn;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.ColumnExpressions;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns.AutoComplete;
using Luxoft.NDS2.Client.UI.Controls.Sur;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq.Expressions;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns
{
    public class ColumnHelper<T> where T : class
    {
        private readonly IGrid _grid;

        readonly IntValueZeroFormatter _formatInfoZeroEmpty = new IntValueZeroFormatter();

        private IColumnExpression _columnExpressionDefault = ColumnExpressionCreator.CreateColumnExpressionDefault();

        public ColumnHelper(IGrid grid)
        {
            _grid = grid;
        }

        public string NameOf<T1>(Expression<Func<T, T1>> lambda)
        {
            return Common.Contracts.Helpers.TypeHelper<T>.GetMemberName(lambda);
        }

        public SingleColumn CreateTextColumn<T1>(Expression<Func<T, T1>> key, int rowSpan = 1, int width = 120)
        {
            return CreateTextColumn<T1>(key, _columnExpressionDefault, rowSpan, width);
        }

        public SingleColumn CreateTextColumn<T1>(Expression<Func<T, T1>> key, 
            IColumnExpression columnExpression, int rowSpan = 1, int width = 120)
        {
            var c = new SingleColumn(NameOf(key), _grid, columnExpression)
            {
                Width = width,
                MinWidth = width,
                HeaderSpanY = rowSpan,
            };
            SetCaptionAndHint(c, key);
            return c;
        }

        public SingleColumn CreateTextColumn<T1>(Expression<Func<T, T1>> key,
            IColumnFilterManager optionsBuilder, int rowSpan = 1, int width = 120)
        {
            var c = new SingleColumn2(NameOf(key), _grid, _columnExpressionDefault, optionsBuilder)
            {
                Width = width,
                MinWidth = width,
                HeaderSpanY = rowSpan,
            };
            SetCaptionAndHint(c, key);
            return c;
        }

        public SingleColumn CreateTextColumn<T1>(
            Expression<Func<T, T1>> key,
            IColumnExpression columnExpression,
            IColumnFilterManager optionsBuilder,
            int rowSpan = 1, 
            int width = 120)
        {
            var c = new SingleColumn2(NameOf(key), _grid, columnExpression, optionsBuilder)
            {
                Width = width,
                MinWidth = width,
                HeaderSpanY = rowSpan,
            };
            SetCaptionAndHint(c, key);
            return c;
        }

        public HyperlinkColumn CreateHyperlinkColumn<T1>(Expression<Func<T, T1>> key, Action<object> cellClick, int rowSpan = 1, int width = 120)
        {
            var c = new HyperlinkColumn(NameOf(key), _grid, _columnExpressionDefault)
            {
                Width = width,
                MinWidth = width,
                HeaderSpanY = rowSpan,
                CellClick = cellClick
            };
            SetCaptionAndHint(c, key);
            return c;
        }

        public ChildColumn CreateChildTextColumn<TData>(Expression<Func<T, TData>> key, int rowSpan = 1, int width = 120)
        {
            var column = new ChildColumn(NameOf(key), _grid)
            {
                Width = width,
                MinWidth = width,
                HeaderSpanY = rowSpan
            };
            SetCaptionAndHint(column, key);

            return column;
        }

        private void SetCaptionAndHint<T1>(SingleColumn column, Expression<Func<T, T1>> key)
        {
            var memberBody = (MemberExpression)key.Body;
            column.Caption = column.Key;

            var attrs = memberBody.Member.GetCustomAttributes(false);
            foreach (var attr in attrs)
            {
                var t = attr.GetType();
                if (t.IsAssignableFrom(typeof(DisplayNameAttribute)))
                    column.Caption = ((DisplayNameAttribute)attr).DisplayName;
                else if (t.IsAssignableFrom(typeof(DescriptionAttribute)))
                    column.HeaderToolTip = ((DescriptionAttribute)attr).Description;
            }
        }

        public SingleColumn CreateNumberColumn<T1>(Expression<Func<T, T1>> key, int rowSpan = 1, int width = 120)
        {
            var c = new SingleColumn(NameOf(key), _grid, _columnExpressionDefault)
            {
                Width = width,
                MinWidth = width,
                HeaderSpanY = rowSpan,
                DataFormatInfo = _formatInfoZeroEmpty,
                CellTextAlign = HorizontalAlign.Right
            };
            SetCaptionAndHint(c, key);
            return c;
        }

        // TODO: использовать этот метод везде, где принменяется IntValueZeroFormatter
        public SingleColumn CreateNumberColumnWithEmptyZero<T1>(Expression<Func<T, T1>> key, int rowSpan = 1, int width = 120)
        {
            object blankValue = null;
            if (typeof(T1) == typeof(decimal) || typeof(T1) == typeof(decimal?) ||
                typeof(T1) == typeof(long) || typeof(T1) == typeof(long?) ||
                typeof(T1) == typeof(int) || typeof(T1) == typeof(int?))
            {
                blankValue = "0";
            }
            var c = new SingleColumn(NameOf(key), _grid, _columnExpressionDefault)
            {
                Width = width,
                MinWidth = width,
                HeaderSpanY = rowSpan,
                DataFormatInfo = _formatInfoZeroEmpty,
                CellTextAlign = HorizontalAlign.Right,
                BlankValue = blankValue
            };
            SetCaptionAndHint(c, key);
            return c;
        }

        public SingleColumn CreateNumberColumn<T1>(Expression<Func<T, T1>> key, IFormatProvider format, int rowSpan = 1, int width = 120)
        {
            var c = new SingleColumn(NameOf(key), _grid, _columnExpressionDefault)
            {
                Width = width,
                MinWidth = width,
                HeaderSpanY = rowSpan,
                DataFormatInfo = format,
                CellTextAlign = HorizontalAlign.Right
            };
            SetCaptionAndHint(c, key);
            return c;
        }

        public ImageColumn<T2> CreateImageColumn<T1, T2>(Expression<Func<T, T1>> key, Control owner, Dictionary<T2, Bitmap> dic, int rowSpan = 1, IColumnFilterManager optionsBuilder = null, IColumnExpression columnExpression = null)
        {

            var c = new ImageColumn<T2>(NameOf(key), owner, dic, _grid, columnExpression ?? _columnExpressionDefault, optionsBuilder)
            {
                Width = 20,
                MinWidth = 20,
                HeaderSpanY = rowSpan
            };
            SetCaptionAndHint(c, key);
            return c;
        }

        public SurColumn CreateSurColumn<T1>(Expression<Func<T, T1>> key, DictionarySur surDictionary, int rowSpan = 1)
        {
            var c = new SurColumn(NameOf(key), _grid, surDictionary, _columnExpressionDefault)
            {
                Width = 60,
                MinWidth = 60,
                HeaderSpanY = rowSpan,
            };
            SetCaptionAndHint(c, key);
            return c;
        }

        public SurColumn CreatePicColumn<T1>(Expression<Func<T, T1>> key, DictionarySur surDictionary, int rowSpan = 1)
        {
            var c = new SurColumn(NameOf(key), _grid, surDictionary, _columnExpressionDefault)
            {
                Width = 60,
                MinWidth = 60,
                HeaderSpanY = rowSpan,
            };
            SetCaptionAndHint(c, key);
            return c;
        }

        public CheckColumn CreateCheckColumn<T1>(Expression<Func<T, T1>> key, int rowSpan = 1)
        {
            return CreateCheckColumn<T1>(key, _columnExpressionDefault, rowSpan);
        }

        public CheckColumn CreateCheckColumn<T1>(Expression<Func<T, T1>> key, IColumnExpression columnExpression, int rowSpan = 1)
        {
            return new CheckColumn(NameOf(key), _grid, columnExpression)
            {
                DisableSort = true,
                DisableFilter = true,
                DisableEdit = false,
                MinWidth = 20,
                Width = 20
            };
        }

        public CheckColumnThreeWay CreateCheckColumnThreeWay<T1>(Expression<Func<T, T1>> key, int rowSpan = 1)
        {
            return CreateCheckColumnThreeWay<T1>(key, _columnExpressionDefault, rowSpan);
        }

        public CheckColumnThreeWay CreateCheckColumnThreeWay<T1>(Expression<Func<T, T1>> key, IColumnExpression columnExpression, int rowSpan = 1)
        {
            return new CheckColumnThreeWay(NameOf(key), _grid, columnExpression)
            {
                DisableSort = true,
                DisableFilter = true,
                DisableEdit = false,
                MinWidth = 20,
                Width = 20
            };
        }

        public BoolColumn CreateBoolColumn<T1>(Expression<Func<T, T1>> key, IColumnExpression columnExpression, DictionaryBoolean boolDictionary, 
            IColumnFilterManager optionsBuilder, int rowSpan = 1, int width = 20, bool setCaptionAndHint = false)
        {
            var c = new BoolColumn(NameOf(key), _grid, boolDictionary, columnExpression, optionsBuilder)
            {
                DisableSort = false,
                DisableFilter = false,
                DisableEdit = false,
                MinWidth = width,
                Width = width
            };

            if (setCaptionAndHint)
            {
                SetCaptionAndHint(c, key);
            }
            return c;
        }

        public ColumnGroup CreateGroup(string key, int rowSpan = 1)
        {
            return new ColumnGroup(Guid.NewGuid().ToString(), _grid) { HeaderSpanY = 1, Caption = key };
        }
    }

    public static class ColumnExtensions
    {
        private const int MAGIC_WIDTH = 8;

        public static TColumn Configure<TColumn>(this TColumn column, Action<TColumn> action)
            where TColumn : ColumnBase
        {
            action(column);
            return column;
        }

        public static SingleColumn MakeHidden(this SingleColumn column)
        {
            column.Hidden = true;
            return column;
        }

        public static SingleColumn DisableFilter(this SingleColumn column)
        {
            column.DisableFilter = true;
            return column;
        }

        private static int GetMaxLenghtWordInWords(string caption)
        {
            char[] delimiterChars = { '\n', '\r' };
            string[] words = caption.Split(delimiterChars);

            int maxLength = 0;
            foreach (string word in words)
            {
                if (!string.IsNullOrEmpty(word))
                    maxLength = word.Length;
            }
            return maxLength;
        }

        public static SingleColumn SetFilterOperatorForDateTime(this SingleColumn column)
        {
            column.FilterOperatorDropDownItems =
                    FilterOperatorDropDownItems.Equals | FilterOperatorDropDownItems.NotEquals |
                    FilterOperatorDropDownItems.GreaterThan | FilterOperatorDropDownItems.GreaterThanOrEqualTo |
                    FilterOperatorDropDownItems.LessThan | FilterOperatorDropDownItems.LessThanOrEqualTo;
            return column;
        }

        public static SingleColumn SetFilterOperatorForDigit(this SingleColumn column)
        {
            column.FilterOperatorDropDownItems =
                    FilterOperatorDropDownItems.Equals | FilterOperatorDropDownItems.NotEquals |
                    FilterOperatorDropDownItems.GreaterThan | FilterOperatorDropDownItems.GreaterThanOrEqualTo |
                    FilterOperatorDropDownItems.LessThan | FilterOperatorDropDownItems.LessThanOrEqualTo;
            return column;
        }

        public static SingleColumn SetFilterOperatorForOpearationCode(this SingleColumn column)
        {
            column.FilterOperatorDropDownItems =
                    FilterOperatorDropDownItems.Equals | FilterOperatorDropDownItems.NotEquals |
                    FilterOperatorDropDownItems.Contains | FilterOperatorDropDownItems.DoesNotContain;
            return column;
        }

        public static SingleColumn SetFilterOperatorForPaymentDoc(this SingleColumn column)
        {
            column.FilterOperatorDropDownItems = FilterOperatorDropDownItems.Equals | FilterOperatorDropDownItems.NotEquals;
            return column;
        }
    }
}
