﻿using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns.AutoComplete
{
    public class DefaultColumnFilterManager : IColumnFilterManager
    {
        private const int NoneValueOptionsQuantity = 4;

        public void AutoComplete(ValueList initialList)
        {
            initialList.Skip(NoneValueOptionsQuantity);
        }

        public void OnFilterChanged(ColumnFilter gridFilter)
        {
        }
    }
}
