﻿using Infragistics.Win.UltraWinGrid;
using System;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns
{
    [Serializable]
    public sealed class ColumnEditableDefinition
    {
        public CellEventHandler Handler { get; set; }

        public string MaskInput { get; set; }

        public Infragistics.Win.UltraWinGrid.Nullable Nullable { get; set; }
    }
}
