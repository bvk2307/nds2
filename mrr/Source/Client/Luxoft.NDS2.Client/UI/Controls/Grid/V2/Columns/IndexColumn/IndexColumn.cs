﻿using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns.IndexColumn
{
    public class IndexColumn : SingleColumn
    {
        private readonly IIndexProvider _indexProvider;

        public IndexColumn(string key, IGrid grid, IIndexProvider indexProvider, IColumnExpression columnExpression)
            : base(key, grid, columnExpression)
        {
            _indexProvider = indexProvider;
        }

        protected override void SetupColumn()
        {
            var column = Column();

            column.SortIndicator = SortIndicator.Disabled;
            column.AllowGroupBy = DefaultableBoolean.False;
            column.AllowRowFiltering = DefaultableBoolean.False;
            column.AllowRowSummaries = AllowRowSummaries.False;           

            Grid.Grid.InitializeRow +=
                (sender, args) =>
                {
                    args.Row.Cells[column.Key].Value = 
                        _indexProvider.FirstRowIndex() + args.Row.Index;
                };
        }

        public override void ToggleEdit(bool disableEdit)
        {
            var column = Column();

            column.CellActivation = Activation.NoEdit;
            column.CellClickAction = CellClickAction.RowSelect;
        }
    }    
}
