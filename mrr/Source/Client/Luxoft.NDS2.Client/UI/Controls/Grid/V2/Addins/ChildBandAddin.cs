﻿using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Helpers;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.Addins
{
    public class ChildBandAddin : ITableAddin
    {
        private readonly GridColumnSetup _columns;

        public ChildBandAddin(GridColumnSetup columns)
        {
            _columns = columns;
        }

        public void Customize(UltraGrid grid)
        {
            var row = grid.FirstRowWithChildBand();

            if (row == null)
            {
                return;
            }

            foreach (var gridColumn in row.ChildBands[0].Band.Columns)
            {
                if (!_columns.Contains(gridColumn.Key))
                {
                    gridColumn.Hidden = true;
                }
            }

            foreach (var column in _columns.Columns)
            {
                var child = column as ChildColumn;

                if (child != null)
                {
                    child.Row = grid.Rows[0];
                }

                child.Init();
            }
        }

    }
}
