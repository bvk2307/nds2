﻿using System.Collections.Generic;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System;
using Nullable = Infragistics.Win.UltraWinGrid.Nullable;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns
{
    public enum HorizontalAlign
    {
        Default,
        Left,
        Center,
        Right,
    }

    /// <summary>
    /// Этот класс представляет элемент таблицы, состоящий из 1 колонки
    /// </summary>
    public class SingleColumn : ColumnBase
    {
        # region Конструкторы

        public SingleColumn(
            string key,
            IGrid grid,
            IColumnExpression columnExpression)
            : base(key, grid, columnExpression)
        {
        }

        # endregion

        # region Свойства

        /// <summary>
        /// Возвращает или задает текст подсказки колонки
        /// </summary>
        public string HeaderToolTip { get; set; }

        private IToolTipViewer _toolTipViewer;

        public IToolTipViewer ToolTipViewer
        {
            get
            {
                return _toolTipViewer ?? (_toolTipViewer = new ToolTipViewerBase());
            }
            set
            {
                _toolTipViewer = value;
            }
        }

        public string DataFormat
        {
            get;
            set;
        }

        public IFormatProvider DataFormatInfo
        {
            get;
            set;
        }

        public HorizontalAlign CellTextAlign
        {
            get;
            set;
        }

        public bool DisableFilter
        {
            get;
            set;
        }

        public bool DisableSort
        {
            get;
            set;
        }

        public bool SortNullAsZero { get; set; }

        public bool WrapText
        {
            get;
            set;
        }

        private int? _minWidth;

        public int? MinWidth
        {
            get
            {
                return _minWidth;
            }
            set
            {
                _minWidth = value;

                if (_minWidth.HasValue && _width.HasValue && _minWidth.Value > _width.Value)
                {
                    _width = _minWidth;
                }
            }
        }

        private int? _width;

        public int? Width
        {
            get
            {
                return _width;
            }
            set
            {
                _width =
                    value.HasValue
                        && _minWidth.HasValue
                        && _minWidth.Value > value.Value
                    ? _minWidth
                    : value;
            }
        }

        public bool ShowTotal
        {
            get;
            set;
        }

        public object BlankValue
        {
            get;
            set;
        }

        public Func<ConditionValueAppearance> CustomConditionValueAppearance
        {
            get;
            set;
        }

        public override string ListTitle()
        {
            return string.IsNullOrWhiteSpace(Caption)
                ? HeaderToolTip
                : Caption;
        }

        # endregion

        # region Настройка колонки

        protected override void SetupColumn()
        {
            SetToolTip();

            var column = Column();

            column.Format = DataFormat;
            column.FormatInfo = DataFormatInfo;
            column.Header.Appearance.TextHAlign = HAlign.Center;
            column.AllowRowFiltering =
                DisableFilter ? DefaultableBoolean.False : DefaultableBoolean.True;
            column.SortIndicator =
                DisableSort ? SortIndicator.Disabled : SortIndicator.None;
            column.Hidden = Hidden;

            if (WrapText)
            {
                column.CellMultiLine = DefaultableBoolean.True;
                Grid.Grid.DisplayLayout.Override.RowSizing = RowSizing.AutoFree;
            }

            if (_minWidth.HasValue)
            {
                column.MinWidth = _minWidth.Value;
            }

            if (_width.HasValue)
            {
                column.Width = _width.Value;
            }

            column.AllowRowSummaries = AllowRowSummaries.False;
            column.CellAppearance.TextHAlign =
                (HAlign)Enum.Parse(typeof(HAlign), CellTextAlign.ToString());
            column.AllowRowSummaries =
                ShowTotal ? AllowRowSummaries.True : AllowRowSummaries.False;

            if (CustomConditionValueAppearance != null)
            {
                column.ValueBasedAppearance = CustomConditionValueAppearance();
            }

            if (BlankValue != null)
            {
                column.DefaultCellValue = BlankValue;
            }

            if (SortNullAsZero)
            {
                column.Nullable = Nullable.Null;
            }

            column.FilterOperatorDropDownItems = FilterOperatorDropDownItems;
        }

        # endregion

        # region Изменение состояния колонки

        protected override void SetCaption()
        {
            Column().Header.Caption = Caption;
        }

        private void SetToolTip()
        {
            var column = Column();

            column.Header.ToolTipText = HeaderToolTip;

            Grid.AfterDataSourceChanged += () =>
            {
                foreach (var row in column.Band.Layout.Grid.Rows)
                {
                    if (row.Cells.Exists(column.Key))
                    {
                        ToolTipViewer.SetToolTip(row.Cells[column.Key]);
                    }
                }
            };
        }
        # endregion

        # region Навигация по объектам таблицы

        public override bool Contains(string columnKey)
        {
            return Key == columnKey;
        }

        # endregion

    }
}
