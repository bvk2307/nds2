﻿using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;
using retired = Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.Adapters
{
    public interface IColumnBuilder
    {
        bool IsApplicable(retired.ColumnBase initialColumn);

        ColumnBase Build(retired.ColumnBase initialColumn);
    }
}
