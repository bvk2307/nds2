﻿using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.Extensions;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Helpers;
using Luxoft.NDS2.Common.Contracts.DTO.Business.TaxPeriod;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.CustomSettings
{
    /// <summary>
    /// Этот класс представляет заглушку в случае если не надо сохранять настройки сортировки и фильтрации
    /// </summary>
    public class DefaultSettingsManager : IDataGridViewManager
    {
		 private UltraGridBand _band;
		 private CustomFilterBuilder _filterBuilder;
		 private FilterComparer _filterComparer;


	    public DefaultSettingsManager(UltraGridBand band, GridColumnSetup columns)
	    {
			 _filterComparer = new FilterComparer(band, columns);
		    _band = band;
	    }

        public IDataGridViewManager WithFilter(QueryConditions filter)
        {
            return this;
        }

        public IDataGridViewManager WithSettingsProvider(ISettingsProvider settingsProvider)
        {
            return this;
        }

        public IDataGridViewManager WithInitialFilter(DefaultFilter defaultFilter)
        {
			   _filterBuilder = new CustomFilterBuilder(new List<FilterQuery>());
			   _filterBuilder.Default = defaultFilter;
               return this;
        }

        public IDataGridViewManager WithRestoreFilters(bool val)
        {
            return this;
        }

        public void SetColumnVisibility(string key, bool visible)
        {
        }

        public void InitColumnVisibility()
        {
        }

        public bool AllowReset()
        {
            if (_filterBuilder == null) return true;
			return !_filterComparer.Compare(_filterBuilder.Default.AllConditions());
        }

        public bool AllowSave()
        {
            return true;
        }

        public bool Save()
        {
            if (_filterBuilder == null) return false;
            return true;
        }

        public bool Restore()
        {
            return false;
        }

        public void Reset()
        {
              if (_filterBuilder != null) _filterBuilder.Reset(_band);
        }


    }
}
