﻿namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination
{
    public static class Defaults
    {
        public static uint FirstPageIndex = 1;

        public static ushort DefaulPageSize = 200;

        public static ushort MinPageSize = 1;

        public static ushort MaxPageSize = ushort.MaxValue;

        public static int[] AvailablePageSizes =
            new int[] { 1, 50, 100, 150, 200 };
    }
}
