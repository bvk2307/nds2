﻿using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns.AutoComplete;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.CheckBoolColumn
{
    public class BoolColumn : PropertyBoundColumn
    {
        private readonly DictionaryBoolean _dictionary;

        private readonly IGrid _iGrid;

        private readonly IColumnFilterManager _optionsBuilder;

        public BoolColumn(string key, IGrid iGrid, DictionaryBoolean dictionary, 
            IColumnExpression columnExpression, IColumnFilterManager optionsBuilder)
            : base(key, iGrid, columnExpression)
        {
            _dictionary = dictionary;
            _iGrid = iGrid;
            _optionsBuilder = optionsBuilder;
            DisableEdit = true;
        }

        protected override void SetupColumn()
        {
            base.SetupColumn();
        }

        public override IColumnFilterManager OptionsBuilder()
        {
            return _optionsBuilder;
        }
    }
}
