﻿using Luxoft.NDS2.Client.UI.Base;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns
{
    public class ColumnCollection : IEnumerable<ColumnBase>
    {
        private const string KeyAlreadyAddedMessagePattern = "Колонка с ключем {0} уже существует в коллекции";

        private readonly List<ColumnBase> _columns = new List<ColumnBase>();

        public void Add(ColumnBase newColumn)
        {
            if (_columns.Any(col => col.Contains(newColumn.Key)))
            {
                throw new Exception(string.Format(KeyAlreadyAddedMessagePattern, newColumn.Key));
            }

            _columns.Add(newColumn);
        }

        public bool Contains(string key)
        {
            return _columns.Any(column => column.Contains(key));
        }

        public ColumnBase Find(string key)
        {
            foreach (var column in _columns)
            {
                var findResult = column.Find(key);

                if (findResult != null)
                {
                    return findResult;
                }
            }

            return null;
        }

        public TColumn Find<TColumn>(string key) where TColumn : ColumnBase
        {
            return Find(key) as TColumn;
        }

        public IEnumerator<ColumnBase> GetEnumerator()
        {
            return _columns.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _columns.GetEnumerator();
        }
    }
}
