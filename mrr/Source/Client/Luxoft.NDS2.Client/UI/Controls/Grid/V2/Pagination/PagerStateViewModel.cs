﻿using Luxoft.NDS2.Client.UI.Base;

namespace Luxoft.NDS2.Client.UI.Controls.Grid.V2.Pagination
{
    public class PagerStateViewModel : IPager
    {
        # region Поля

        private uint _pageIndex = Defaults.FirstPageIndex;

        private ushort _pageSize = Defaults.DefaulPageSize;

        private uint? _totalMatches;

        private uint? _totalRows;

        # endregion

        public bool SuppressEvents { get; set; }

        # region Свойства

        public uint PageIndex
        {
            get
            {
                return _pageIndex;
            }
            set
            {
                if (_pageIndex == value)
                {
                    return;
                }

                if (!_totalMatches.HasValue && value > Defaults.FirstPageIndex)
                {
                    throw new PageQuantityNotDefinedException();
                }

                if (value < Defaults.FirstPageIndex || value > PageCount)
                {
                    throw new PageIndexOutOfRangeException(value, PageCount);
                }

                _pageIndex = value;
                RaiseDataListChanging();
            }
        }

        public ushort PageSize
        {
            get
            {
                return _pageSize;
            }
            set
            {
                if (_pageSize == value)
                {
                    return;
                }

                if (value < Defaults.MinPageSize || value > Defaults.MaxPageSize)
                {
                    throw new PageSizeOutOfRangeException(value);
                }

                _pageSize = value;
                _pageIndex = Defaults.FirstPageIndex;

                RaiseDataListChanging();
            }
        }

        public uint TotalDataRows
        {
            get
            {
                return _totalRows.HasValue ? _totalRows.Value : 0;
            }
            set
            {
                if (_totalRows.HasValue && _totalRows.Value == value)
                {
                    return;
                }

                _totalRows = value;
                RaiseStateChanged();
            }
        }

        public uint TotalMatches
        {
            get
            {
                return _totalMatches.HasValue ? _totalMatches.Value : 0;
            }
            set
            {
                if (_totalMatches.HasValue && _totalMatches.Value == value)
                {
                    return;
                }

                _totalMatches = value;
                RaiseStateChanged();
            }
        }

        public uint PageCount
        {
            get
            {
                if (_totalMatches.HasValue)
                {
                    return (uint)_totalMatches.Value / _pageSize + 1;
                }

                return 1;
            }
        }

        public bool AllowPrevPage
        {
            get
            {
                return _pageIndex > Defaults.FirstPageIndex;
            }
        }

        public bool AllowNextPage
        {
            get
            {
                return _totalMatches.HasValue && PageIndex < PageCount;
            }
        }

        public bool HasTotals
        {
            get
            {
                return _totalMatches.HasValue
                    && _totalRows.HasValue;
            }
        }

        # endregion

        # region События

        public event ParameterlessEventHandler DataListChanging;

        public event ParameterlessEventHandler ViewStateChanged;

        private void RaiseDataListChanging()
        {
            if (SuppressEvents) return;

            if (DataListChanging != null)
            {
                DataListChanging();
            }

            RaiseStateChanged();
        }

        private void RaiseStateChanged()
        {
            if (SuppressEvents) return;

            if (ViewStateChanged != null)
            {
                ViewStateChanged();
            }
        }

        # endregion

        # region Регион навигация по страницам

        public void NextPage()
        {
            if (PageIndex < PageCount)
            {
                PageIndex++;
            }
        }

        public void PrevPage()
        {
            if (PageIndex > Defaults.FirstPageIndex)
            {
                PageIndex--;
            }
        }

        public void FirstPage()
        {
            PageIndex = Defaults.FirstPageIndex;
        }

        public void LastPage()
        {
            PageIndex = PageCount;
        }

        # endregion

        # region Методы

        public void SetFirstPage()
        {
            if (_pageIndex != Defaults.FirstPageIndex)
            {
                _pageIndex = Defaults.FirstPageIndex;
                RaiseStateChanged();
            }
        }

        # endregion
    }
}
