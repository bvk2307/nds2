﻿using System;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.Helpers;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    /// <summary>
    /// Этот класс форматирует UltraGridColumn для отображения иконок вместо значений
    /// </summary>
    public class ImageColumnFormatter<TCellData> : IGridColumnFormatter
    {
        private readonly Dictionary<TCellData, Bitmap> _bitmaps;

        private readonly Control _owner;

        private readonly Dictionary<TCellData, Image> _imageCache = new Dictionary<TCellData,Image>();

        public ImageColumnFormatter(Dictionary<TCellData, Bitmap> bitmaps, Control owner)
        {
            _bitmaps = bitmaps;
            _owner = owner;
        }
        public UltraGridColumn ApplyFormat(UltraGridColumn column)
        {
            column.ValueBasedAppearance =
                _bitmaps.ImageAppearance(
                    _owner,
                    (key, bitmap, ctrl) =>
                    {
                        if (!_imageCache.ContainsKey(key))
                        {
                            _imageCache.Add(key, bitmap.DrawIcon(ctrl));
                        }

                        return _imageCache[key];
                    });
            return column;
        }

        public List<UltraGridColumn> ApplyFormat(List<UltraGridColumn> columns)
        {
            columns.ForEach(x=> x.ValueBasedAppearance = 
                _bitmaps.ImageAppearance(
                    _owner,
                    (key, bitmap, ctrl) => 
                    {
                        if (!_imageCache.ContainsKey(key))
                        {
                            _imageCache.Add(key, bitmap.DrawIcon(ctrl));
                        }

                        return _imageCache[key];
                    }));
            return columns;
        }
    }

}
