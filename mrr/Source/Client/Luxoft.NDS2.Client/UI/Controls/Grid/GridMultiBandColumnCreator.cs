﻿using System.Collections.Generic;
using System.Linq;
using Infragistics.Win.UltraWinGrid;

namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    public class GridMultiBandColumnCreator : GridColumnCreator
    {
        private readonly List<ColumnsCollection> _childColumns;

        public GridMultiBandColumnCreator(List<ColumnsCollection> childColumns)
        {
            _childColumns = childColumns;
        }

        public override IGridColumnView Create(UltraGridColumn column)
        {
            bool exist = false;
            foreach (var columns in _childColumns)
            {
                if (columns.Exists(column.Key))
                {
                    exist = true;
                    break;
                }
            }

            if (!exist)
                return base.Create(column);

            var listBandColumns = new List<UltraGridColumn>();
            foreach (var columns in _childColumns)
            {
                if (columns.Exists(column.Key))
                {
                    listBandColumns.Add(columns[column.Key]);
                }
            }
            return new GridMultiBandColumnView(column, listBandColumns, FilterView(column));
        }
    }
}
