﻿using Infragistics.Shared;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.CustomSettings;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System;

namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    public abstract class GridColumnView<TControl> : IGridColumnView
        where TControl : KeyedSubObjectBase, IProvideRowLayoutColumnInfo
    {
        protected GridColumnView(TControl control, IGridColumnFilterView filterView)
        {
            Control = control;
            Filter = filterView;
        }

        protected TControl Control
        {
            get;
            private set;
        }

        public string Key
        {
            get { return Control.Key; }
        }

        public string Caption
        {
            get
            {
                return Control.Header.Caption;
            }
            set
            {
                Control.Header.Caption = value;
            }
        }

        public string Description
        {
            get
            {
                return Control.Header.ToolTipText;
            }
            set
            {
                Control.Header.ToolTipText = value;
            }
        }

        public abstract IGridColumnsCollection SubColumns
        {
            get;
        }

        public GridColumnVisualProperties HeaderProps
        {
            get
            {
                var ret = new GridColumnVisualProperties
                {
                    VPosition = Control.Header.VisiblePosition,
                    Width = Control.Header.Column.Width
                };
                return ret;
            }
            set
            {
                Control.Header.VisiblePosition = value.VPosition;
                Control.Header.Column.Width = value.Width;
            }
        }

        # region Visibility

        private bool? _visibleByDefault;

        public bool VisibleByDefault
        {
            get
            {
                if (!_visibleByDefault.HasValue)
                {
                    _visibleByDefault = Visible;
                }

                return _visibleByDefault.Value;
            }
            set
            {
                _visibleByDefault = value;
            }
        }

        public abstract void ResetVisibility();

        public abstract bool Visible
        {
            get;
            set;
        }

        public abstract bool ReadOnly
        {
            get;
            set;
        }

        protected void RaiseVisibleChanged()
        {
            if (VisibleChanged != null)
            {
                VisibleChanged(this, new EventArgs());
            }
        }

        public event EventHandler VisibleChanged;

        # endregion

        # region Sorting

        public abstract ColumnSort.SortOrder DefaultSortOrder
        {
            get;
            set;
        }

        public abstract ColumnSort.SortOrder SortOrder
        {
            get;
            set;
        }

        public abstract bool AllowSort
        {
            get;
        }

        public abstract void ResetSortOrder();

        # endregion

        # region Formatting

        public abstract IGridColumnView ApplyFormatter(IGridColumnFormatter formatter);

        public abstract IGridColumnView Sort(SortIndicator sortIndicator);

        #endregion

        #region Filtering

        public IGridColumnFilterView Filter
        {
            get;
            private set;
        }
      
        #endregion
    }
}
