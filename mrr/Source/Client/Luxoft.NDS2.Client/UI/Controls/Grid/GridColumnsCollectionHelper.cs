﻿using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    public static class GridColumnsCollectionHelper
    {
        public static bool HasColumn(
            this IEnumerable<IGridColumnView> columnCollection, 
            string columnKey)
        {
            return columnCollection.Any(
                col => col.Key == columnKey
                    || col.SubColumns.HasColumn(columnKey));
        }

        public static IGridColumnView FindByKey(
            this IEnumerable<IGridColumnView> columnCollection,
            string columnKey)
        {
            foreach (var column in columnCollection)
            {
                if (column.Key == columnKey)
                {
                    return column;
                }

                if (column.SubColumns.HasColumn(columnKey))
                {
                    return column.SubColumns.FindByKey(columnKey);
                }
            }

            throw new KeyNotFoundException();
        }

        public static void ForAll(
            this IEnumerable<IGridColumnView> columnCollection, 
            Action<IGridColumnView> action)
        {
            foreach (var column in columnCollection)
            {
                column.SubColumns.ForAll(action);
                action(column);
            }
        }

        public static IEnumerable<UltraGridColumn> AsEnumerable(
            this ColumnsCollection columns,
            string[] columnsToExclude)
        {
            foreach (UltraGridColumn column in columns)
            {
                if (column != null && !columnsToExclude.Contains(column.Key))
                {
                    yield return column;
                }
            }
        }

        public static IEnumerable<UltraGridColumn> AsEnumerable(this ColumnsCollection columns)
        {
            return columns.AsEnumerable(new string[0]);
        }

        public static IEnumerable<UltraGridGroup> AsEnumerable(this GroupsCollection groups)
        {
            foreach (UltraGridGroup group in groups)
            {
                if (group != null)
                {
                    yield return group;
                }
            }
        }
    }
}
