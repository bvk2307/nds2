﻿using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    public interface IGridColumnsCollection : IEnumerable<IGridColumnView>
    {
    }
}
