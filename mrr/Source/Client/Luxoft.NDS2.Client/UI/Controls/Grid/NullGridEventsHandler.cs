﻿using System;

namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    public class NullGridEventsHandler : IGridEventsHandler
    {
        public void Enable()
        {
        }

        public void Disable()
        {
        }

        public void Filter()
        {
        }

        public event EventHandler Sorted;

        public event EventHandler Filtered;

        public event EventHandler Enabled;

        public event EventHandler Disabled;

        public event EventHandler Unloaded;
    }
}
