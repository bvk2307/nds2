﻿using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System.Linq;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    public class OperationCodesColumnFilterView : GridColumnFilterView
    {
        public OperationCodesColumnFilterView(UltraGridColumn column)
            : base(column)
        {
            _column.Band.ColumnFilters[_column].LogicalOperator = FilterLogicalOperator.Or;
        }

        protected override void BuildQuickFilterList(BeforeRowFilterDropDownEventArgs e)
        {            
            var itemsToDelete = e.ValueList.ValueListItems.All.Skip(1).ToArray();

            foreach (var item in itemsToDelete)
            {
                e.ValueList.ValueListItems.Remove(item);
            }

            for (var code = 1; code <= 32; code++)
            {
                e.ValueList.ValueListItems.Add(
                    new ValueListItem(code, code.ToString().PadLeft(2, '0'))
                    {
                        CheckState = CheckState.Checked
                    });
            }
        }
    }
}
