﻿using Infragistics.Shared;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    /// <summary>
    /// Реализует 2-уровневую коллекцию колонок грида
    /// </summary>
    public class GridColumnsCollection : IGridColumnsCollection
    {
        private readonly List<IGridColumnView> _columns = new List<IGridColumnView>();

        public GridColumnsCollection()
        {
        }

        public GridColumnsCollection(
            UltraGridGroup root,
            IEnumerable<UltraGridGroup> groups,
            IEnumerable<UltraGridColumn> columns,
            IGridColumnViewCreator creator)
        {
            var temp = new Dictionary<int, List<IGridColumnView>>();
            var groupsCopy = groups.ToArray();

            foreach (var group in groups)
            {
                var groupColumns =
                    columns.Where(
                        x => x.GroupResolved != null
                            && x.GroupResolved.Key == group.Key)
                    .ToArray();

                int originX;
                GetGroupOriginX(group, out originX);
                var item = creator.Create(group, 
                        new UltraGridGroup[0], columns);

                if (!temp.ContainsKey(originX))
                temp.Add(originX, new List<IGridColumnView>{item});
                else
                {
                    temp[originX].Add(item);
                }

            }

            int rootOriginX = 0;
            if(root != null)
               GetGroupOriginX(root, out rootOriginX);
            
            foreach (var column in columns)
            {
                if (column.GroupResolved == root)
                {
                    var origin = column.RowLayoutColumnInfo.OriginX + rootOriginX;
                    var colItem = creator.Create(column);
                    if (!temp.ContainsKey(origin))
                    temp.Add(column.RowLayoutColumnInfo.OriginX + rootOriginX, new List<IGridColumnView>{colItem});
                    else
                    {
                        temp[origin].Add(colItem);
                    }
                }
            }

            _columns.AddRange(temp.OrderBy(x => x.Key).SelectMany(x => x.Value));
        }

        private void GetGroupOriginX(UltraGridGroup group, out int origin)
        {
            origin = group.RowLayoutGroupInfo.OriginX;
            UltraGridGroup parent = group.RowLayoutGroupInfo.ParentGroup;
            origin = parent == null && origin == 0 ? 1 : origin;
            while (parent != null)
            {
                origin += parent.RowLayoutGroupInfo.OriginX;
                parent = parent.RowLayoutGroupInfo.ParentGroup;
            }
        }

        public void Add(IGridColumnView columnView)
        {
            _columns.Add(columnView);
        }

        public IEnumerator<IGridColumnView> GetEnumerator()
        {
            return _columns.GetEnumerator() as IEnumerator<IGridColumnView>;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
