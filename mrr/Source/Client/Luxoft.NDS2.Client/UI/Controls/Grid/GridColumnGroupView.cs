﻿using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    public class GridColumnGroupView : GridColumnView<UltraGridGroup>
    {
        private readonly GridColumnsCollection _subColumns;

        public GridColumnGroupView(
            UltraGridGroup group, 
            IEnumerable<UltraGridGroup> lesserGroups,
            IEnumerable<UltraGridColumn> lesserColumns,
            IGridColumnViewCreator columnCreator)
            : base(group, new NullGridGroupFilterView())
        {
            _subColumns = 
                new GridColumnsCollection(  
                    group,
                    lesserGroups,
                    lesserColumns,
                    columnCreator);
            SetGroupVisibility();

            foreach (var column in _subColumns)
            {
                column.VisibleChanged += (sender, e) => SetGroupVisibility();
            }
        }

        # region Visibility

        private void SetGroupVisibility()
        {
            var visible = SubColumns.Any(x => x.Visible);

            if (visible != Visible)
            {
                Control.Hidden = !visible;
                RaiseVisibleChanged();
            }
        }

        public override bool Visible
        {
            get
            {
                return !Control.Hidden;
            }
            set
            {
                if (Control.Hidden == value)
                {
                    Control.Hidden = !value;
                    RaiseVisibleChanged();

                    foreach (var column in SubColumns)
                    {
                        column.Visible = value;
                    }
                }
            }
        }

        public override bool ReadOnly
        {
            get;
            set;
        }

        public override void ResetVisibility()
        {
            foreach (var column in _subColumns)
            {
                column.ResetVisibility();
            }
        }

        # endregion

        # region Sorting

        public override ColumnSort.SortOrder DefaultSortOrder
        {
            get
            {
                return ColumnSort.SortOrder.None;
            }
            set
            {
                return;
            }
        }

        public override ColumnSort.SortOrder SortOrder
        {
            get
            {
                return ColumnSort.SortOrder.None;
            }
            set
            {
                return;
            }
        }

        public override bool AllowSort
        {
            get
            {
                return false;
            }
        }

        public override void ResetSortOrder()
        {
            foreach (var column in SubColumns)
            {
                column.ResetSortOrder();
            }
        }

        # endregion

        public override IGridColumnsCollection SubColumns
        {
            get
            {
                return _subColumns;
            }
        }

        public override string ToString()
        {
            return Caption;
        }

        public override IGridColumnView ApplyFormatter(IGridColumnFormatter formatter)
        {
            throw new NotSupportedException();
        }

        public override IGridColumnView Sort(SortIndicator sortIndicator)
        {
            return this;
        }
    }

    public class NullGridGroupFilterView : IGridColumnFilterView
    {
        public bool AllowFilter
        {
            get
            {
                return false;
            }
            set
            {
                return;
            }
        }

        public FilterQuery Default
        {
            get { return null; }
        }

        public Infragistics.Win.UltraWinGrid.ColumnFilter DefaultColumnFilter
        {
            get { return null; }
        }

        public FilterQuery Current
        {
            get { return null; }
            set { }
        }

        public void ResetFilter()
        {
            
        }
    }
}
