﻿using System.Collections.Generic;
using Infragistics.Win.UltraWinGrid;

namespace Luxoft.NDS2.Client.UI.Controls.Grid
{
    public class GridMultiBandColumnView : SingleGridColumnView
    {
        private readonly List<UltraGridColumn> _childColumns;

        public GridMultiBandColumnView(
            UltraGridColumn mainColumn, 
            List<UltraGridColumn> childColumns,
            IGridColumnFilterView filterView)
            : base(mainColumn, filterView)
        {
            _childColumns = childColumns;
            VisibleChanged += (sender, e) => childColumns.ForEach(x => x.Hidden = mainColumn.Hidden);
            if (mainColumn.Band.Layout.Grid.DisplayLayout.Override.AllowColSizing != AllowColSizing.Synchronized)
            {
                mainColumn.Band.Layout.Grid.AfterColPosChanged +=
                    (sender, e) => childColumns.ForEach(x => x.Width = mainColumn.Width);
            }
        }

        public override IGridColumnView ApplyFormatter(IGridColumnFormatter formatter)
        {
            base.ApplyFormatter(formatter);
            formatter.ApplyFormat(_childColumns);
            return this;
        }

        public override IGridColumnView Sort(SortIndicator sortIndicator)
        {
            _childColumns.ForEach(x=>x.SortIndicator = sortIndicator);
            return this;
        }
    }
}
