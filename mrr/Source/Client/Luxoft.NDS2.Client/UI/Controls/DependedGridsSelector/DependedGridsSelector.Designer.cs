﻿namespace Luxoft.NDS2.Client.UI.Controls.DependedGridsSelector
{
    partial class DependedGridsSelector
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this._sourceGridView = new Luxoft.NDS2.Client.UI.Controls.DataGrid.GridView();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this._moveRightBtn = new Infragistics.Win.Misc.UltraButton();
            this._destGridView = new Luxoft.NDS2.Client.UI.Controls.DataGrid.GridView();
            this._moveLeftBtn = new Infragistics.Win.Misc.UltraButton();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer2
            // 
            this.splitContainer2.BackColor = System.Drawing.Color.Transparent;
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.IsSplitterFixed = true;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this._sourceGridView);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.splitContainer3);
            this.splitContainer2.Size = new System.Drawing.Size(787, 728);
            this.splitContainer2.SplitterDistance = 351;
            this.splitContainer2.TabIndex = 1;
            // 
            // _sourceGridView
            // 
            this._sourceGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this._sourceGridView.GridContextMenuStrip = null;
            this._sourceGridView.Location = new System.Drawing.Point(0, 0);
            this._sourceGridView.Margin = new System.Windows.Forms.Padding(0);
            this._sourceGridView.Name = "_sourceGridView";
            this._sourceGridView.Size = new System.Drawing.Size(351, 728);
            this._sourceGridView.TabIndex = 0;
            // 
            // splitContainer3
            // 
            this.splitContainer3.BackColor = System.Drawing.Color.Transparent;
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer3.IsSplitterFixed = true;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this._moveLeftBtn);
            this.splitContainer3.Panel1.Controls.Add(this._moveRightBtn);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this._destGridView);
            this.splitContainer3.Size = new System.Drawing.Size(432, 728);
            this.splitContainer3.SplitterDistance = 70;
            this.splitContainer3.TabIndex = 0;
            // 
            // _moveRightBtn
            // 
            this._moveRightBtn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance2.Image = global::Luxoft.NDS2.Client.Properties.Resources.right_arrow_big;
            this._moveRightBtn.Appearance = appearance2;
            this._moveRightBtn.AutoSize = true;
            this._moveRightBtn.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Borderless;
            this._moveRightBtn.ImageSize = new System.Drawing.Size(32, 32);
            this._moveRightBtn.Location = new System.Drawing.Point(19, 25);
            this._moveRightBtn.Margin = new System.Windows.Forms.Padding(0);
            this._moveRightBtn.Name = "_moveRightBtn";
            this._moveRightBtn.ShowOutline = false;
            this._moveRightBtn.Size = new System.Drawing.Size(36, 36);
            this._moveRightBtn.TabIndex = 0;
            this._moveRightBtn.UseAppStyling = false;
            this._moveRightBtn.UseHotTracking = Infragistics.Win.DefaultableBoolean.True;
            this._moveRightBtn.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            this._moveRightBtn.Click += new System.EventHandler(this._moveRightBtn_Click);
            // 
            // _destGridView
            // 
            this._destGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this._destGridView.GridContextMenuStrip = null;
            this._destGridView.Location = new System.Drawing.Point(0, 0);
            this._destGridView.Margin = new System.Windows.Forms.Padding(0);
            this._destGridView.Name = "_destGridView";
            this._destGridView.Size = new System.Drawing.Size(358, 728);
            this._destGridView.TabIndex = 0;
            // 
            // _moveLeftBtn
            // 
            this._moveLeftBtn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.Image = global::Luxoft.NDS2.Client.Properties.Resources.left_arrow;
            this._moveLeftBtn.Appearance = appearance1;
            this._moveLeftBtn.AutoSize = true;
            this._moveLeftBtn.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Borderless;
            this._moveLeftBtn.ImageSize = new System.Drawing.Size(32, 32);
            this._moveLeftBtn.Location = new System.Drawing.Point(19, 75);
            this._moveLeftBtn.Margin = new System.Windows.Forms.Padding(0);
            this._moveLeftBtn.Name = "_moveLeftBtn";
            this._moveLeftBtn.ShowOutline = false;
            this._moveLeftBtn.Size = new System.Drawing.Size(36, 36);
            this._moveLeftBtn.TabIndex = 2;
            this._moveLeftBtn.UseAppStyling = false;
            this._moveLeftBtn.UseHotTracking = Infragistics.Win.DefaultableBoolean.True;
            this._moveLeftBtn.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            this._moveLeftBtn.Click += new System.EventHandler(this._moveLeftBtn_Click);
            // 
            // DependedGridsSelector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.splitContainer2);
            this.Name = "DependedGridsSelector";
            this.Size = new System.Drawing.Size(787, 728);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel1.PerformLayout();
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private Infragistics.Win.Misc.UltraButton _moveRightBtn;
        private DataGrid.GridView _sourceGridView;
        private DataGrid.GridView _destGridView;
        private Infragistics.Win.Misc.UltraButton _moveLeftBtn;
    }
}
