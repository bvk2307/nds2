﻿namespace Luxoft.NDS2.Client.UI.Controls.DependedGridsSelector
{
    partial class DependedDataGridsSelector
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            this.mainsplitContainer = new System.Windows.Forms.SplitContainer();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer6 = new System.Windows.Forms.SplitContainer();
            this._moveRightBtn = new Infragistics.Win.Misc.UltraButton();
            this._moveLeftBtn = new Infragistics.Win.Misc.UltraButton();
            this.splitContainer7 = new System.Windows.Forms.SplitContainer();
            this._ultraToolTipManager = new Infragistics.Win.UltraWinToolTip.UltraToolTipManager(this.components);
            this._srcHeader = new Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel();
            this._sourceGridView = new Luxoft.NDS2.Client.UI.Controls.Grid.V2.DataGridView();
            this._destGridView = new Luxoft.NDS2.Client.UI.Controls.Grid.V2.DataGridView();
            this._destHeader = new Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel();
            ((System.ComponentModel.ISupportInitialize)(this.mainsplitContainer)).BeginInit();
            this.mainsplitContainer.Panel1.SuspendLayout();
            this.mainsplitContainer.Panel2.SuspendLayout();
            this.mainsplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).BeginInit();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.Panel2.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer6)).BeginInit();
            this.splitContainer6.Panel2.SuspendLayout();
            this.splitContainer6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer7)).BeginInit();
            this.splitContainer7.Panel1.SuspendLayout();
            this.splitContainer7.Panel2.SuspendLayout();
            this.splitContainer7.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainsplitContainer
            // 
            this.mainsplitContainer.BackColor = System.Drawing.Color.Transparent;
            this.mainsplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainsplitContainer.Location = new System.Drawing.Point(0, 0);
            this.mainsplitContainer.Name = "mainsplitContainer";
            // 
            // mainsplitContainer.Panel1
            // 
            this.mainsplitContainer.Panel1.Controls.Add(this.splitContainer4);
            // 
            // mainsplitContainer.Panel2
            // 
            this.mainsplitContainer.Panel2.Controls.Add(this.splitContainer1);
            this.mainsplitContainer.Size = new System.Drawing.Size(787, 728);
            this.mainsplitContainer.SplitterDistance = 346;
            this.mainsplitContainer.TabIndex = 2;
            // 
            // splitContainer4
            // 
            this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer4.IsSplitterFixed = true;
            this.splitContainer4.Location = new System.Drawing.Point(0, 0);
            this.splitContainer4.Name = "splitContainer4";
            this.splitContainer4.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.Controls.Add(this._srcHeader);
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.Controls.Add(this._sourceGridView);
            this.splitContainer4.Size = new System.Drawing.Size(346, 728);
            this.splitContainer4.SplitterDistance = 40;
            this.splitContainer4.TabIndex = 16;
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = System.Drawing.Color.Transparent;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer6);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer7);
            this.splitContainer1.Size = new System.Drawing.Size(437, 728);
            this.splitContainer1.SplitterDistance = 70;
            this.splitContainer1.TabIndex = 0;
            // 
            // splitContainer6
            // 
            this.splitContainer6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer6.IsSplitterFixed = true;
            this.splitContainer6.Location = new System.Drawing.Point(0, 0);
            this.splitContainer6.Name = "splitContainer6";
            this.splitContainer6.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer6.Panel2
            // 
            this.splitContainer6.Panel2.Controls.Add(this._moveRightBtn);
            this.splitContainer6.Panel2.Controls.Add(this._moveLeftBtn);
            this.splitContainer6.Size = new System.Drawing.Size(70, 728);
            this.splitContainer6.SplitterDistance = 40;
            this.splitContainer6.TabIndex = 3;
            // 
            // _moveRightBtn
            // 
            this._moveRightBtn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance3.Image = global::Luxoft.NDS2.Client.Properties.Resources.right_arrow_big;
            this._moveRightBtn.Appearance = appearance3;
            this._moveRightBtn.AutoSize = true;
            this._moveRightBtn.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Borderless;
            this._moveRightBtn.ImageSize = new System.Drawing.Size(32, 32);
            this._moveRightBtn.Location = new System.Drawing.Point(18, 30);
            this._moveRightBtn.Margin = new System.Windows.Forms.Padding(0);
            this._moveRightBtn.Name = "_moveRightBtn";
            this._moveRightBtn.ShowOutline = false;
            this._moveRightBtn.Size = new System.Drawing.Size(36, 36);
            this._moveRightBtn.TabIndex = 0;
            this._moveRightBtn.UseAppStyling = false;
            this._moveRightBtn.UseHotTracking = Infragistics.Win.DefaultableBoolean.True;
            this._moveRightBtn.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            this._moveRightBtn.Click += new System.EventHandler(this._moveRightBtn_Click);
            // 
            // _moveLeftBtn
            // 
            this._moveLeftBtn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.Image = global::Luxoft.NDS2.Client.Properties.Resources.left_arrow;
            this._moveLeftBtn.Appearance = appearance1;
            this._moveLeftBtn.AutoSize = true;
            this._moveLeftBtn.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Borderless;
            this._moveLeftBtn.ImageSize = new System.Drawing.Size(32, 32);
            this._moveLeftBtn.Location = new System.Drawing.Point(18, 80);
            this._moveLeftBtn.Margin = new System.Windows.Forms.Padding(0);
            this._moveLeftBtn.Name = "_moveLeftBtn";
            this._moveLeftBtn.ShowOutline = false;
            this._moveLeftBtn.Size = new System.Drawing.Size(36, 36);
            this._moveLeftBtn.TabIndex = 2;
            this._moveLeftBtn.UseAppStyling = false;
            this._moveLeftBtn.UseHotTracking = Infragistics.Win.DefaultableBoolean.True;
            this._moveLeftBtn.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            this._moveLeftBtn.Click += new System.EventHandler(this._moveLeftBtn_Click);
            // 
            // splitContainer7
            // 
            this.splitContainer7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer7.IsSplitterFixed = true;
            this.splitContainer7.Location = new System.Drawing.Point(0, 0);
            this.splitContainer7.Name = "splitContainer7";
            this.splitContainer7.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer7.Panel1
            // 
            this.splitContainer7.Panel1.Controls.Add(this._destHeader);
            // 
            // splitContainer7.Panel2
            // 
            this.splitContainer7.Panel2.Controls.Add(this._destGridView);
            this.splitContainer7.Size = new System.Drawing.Size(363, 728);
            this.splitContainer7.SplitterDistance = 40;
            this.splitContainer7.TabIndex = 1;
            // 
            // _ultraToolTipManager
            // 
            this._ultraToolTipManager.ContainingControl = this;
            // 
            // _srcHeader
            // 
            appearance4.BackColor = System.Drawing.Color.Transparent;
            appearance4.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            this._srcHeader.Appearance = appearance4;
            this._srcHeader.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this._srcHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this._srcHeader.Location = new System.Drawing.Point(0, 0);
            this._srcHeader.Name = "_srcHeader";
            this._srcHeader.Size = new System.Drawing.Size(346, 40);
            this._srcHeader.TabIndex = 21;
            this._srcHeader.TabStop = true;
            this._srcHeader.UseAppStyling = false;
            this._srcHeader.Value = "";
            // 
            // _sourceGridView
            // 
            this._sourceGridView.AggregatePanelVisible = true;
            this._sourceGridView.AllowFilterReset = false;
            this._sourceGridView.AllowMultiGrouping = true;
            this._sourceGridView.AllowResetSettings = false;
            this._sourceGridView.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this._sourceGridView.BackColor = System.Drawing.Color.Transparent;
            this._sourceGridView.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.None;
            this._sourceGridView.ColumnVisibilitySetupButtonVisible = false;
            this._sourceGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this._sourceGridView.ExportExcelCancelVisible = false;
            this._sourceGridView.ExportExcelVisible = false;
            this._sourceGridView.FilterResetVisible = false;
            this._sourceGridView.FooterVisible = true;
            this._sourceGridView.GridContextMenuStrip = null;
            this._sourceGridView.Location = new System.Drawing.Point(0, 0);
            this._sourceGridView.Margin = new System.Windows.Forms.Padding(0);
            this._sourceGridView.Name = "_sourceGridView";
            this._sourceGridView.PanelExportExcelStateVisible = false;
            this._sourceGridView.PanelLoadingVisible = true;
            this._sourceGridView.PanelPagesVisible = true;
            this._sourceGridView.RowDoubleClicked = null;
            this._sourceGridView.Size = new System.Drawing.Size(346, 684);
            this._sourceGridView.TabIndex = 0;
            this._sourceGridView.Title = "";
            // 
            // _destGridView
            // 
            this._destGridView.AggregatePanelVisible = true;
            this._destGridView.AllowFilterReset = false;
            this._destGridView.AllowMultiGrouping = true;
            this._destGridView.AllowResetSettings = false;
            this._destGridView.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this._destGridView.BackColor = System.Drawing.Color.Transparent;
            this._destGridView.ColumnsAutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.None;
            this._destGridView.ColumnVisibilitySetupButtonVisible = false;
            this._destGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this._destGridView.ExportExcelCancelVisible = false;
            this._destGridView.ExportExcelVisible = false;
            this._destGridView.FilterResetVisible = false;
            this._destGridView.FooterVisible = true;
            this._destGridView.GridContextMenuStrip = null;
            this._destGridView.Location = new System.Drawing.Point(0, 0);
            this._destGridView.Margin = new System.Windows.Forms.Padding(0);
            this._destGridView.Name = "_destGridView";
            this._destGridView.PanelExportExcelStateVisible = false;
            this._destGridView.PanelLoadingVisible = true;
            this._destGridView.PanelPagesVisible = true;
            this._destGridView.RowDoubleClicked = null;
            this._destGridView.Size = new System.Drawing.Size(363, 684);
            this._destGridView.TabIndex = 0;
            this._destGridView.Title = "";
            // 
            // _destHeader
            // 
            appearance5.BackColor = System.Drawing.Color.Transparent;
            appearance5.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            this._destHeader.Appearance = appearance5;
            this._destHeader.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            this._destHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this._destHeader.Location = new System.Drawing.Point(0, 0);
            this._destHeader.Name = "_destHeader";
            this._destHeader.Size = new System.Drawing.Size(363, 40);
            this._destHeader.TabIndex = 22;
            this._destHeader.TabStop = true;
            this._destHeader.UseAppStyling = false;
            this._destHeader.Value = "";
            // 
            // DependedDataGridsSelector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.mainsplitContainer);
            this.Name = "DependedDataGridsSelector";
            this.Size = new System.Drawing.Size(787, 728);
            this.mainsplitContainer.Panel1.ResumeLayout(false);
            this.mainsplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainsplitContainer)).EndInit();
            this.mainsplitContainer.ResumeLayout(false);
            this.splitContainer4.Panel1.ResumeLayout(false);
            this.splitContainer4.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).EndInit();
            this.splitContainer4.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer6.Panel2.ResumeLayout(false);
            this.splitContainer6.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer6)).EndInit();
            this.splitContainer6.ResumeLayout(false);
            this.splitContainer7.Panel1.ResumeLayout(false);
            this.splitContainer7.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer7)).EndInit();
            this.splitContainer7.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Grid.V2.DataGridView _sourceGridView;
        private Grid.V2.DataGridView _destGridView;
        private System.Windows.Forms.SplitContainer mainsplitContainer;
        private System.Windows.Forms.SplitContainer splitContainer4;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer6;
        private Infragistics.Win.Misc.UltraButton _moveRightBtn;
        private Infragistics.Win.Misc.UltraButton _moveLeftBtn;
        private System.Windows.Forms.SplitContainer splitContainer7;
        private Infragistics.Win.UltraWinToolTip.UltraToolTipManager _ultraToolTipManager;
        private Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel _srcHeader;
        private Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel _destHeader;
    }
}
