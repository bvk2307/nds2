﻿using System;
using System.Collections;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Controls.DependedGridsSelector
{
    public partial class DependedDataGridsSelector : UserControl, IDependedGridsSelector
    {
        public DependedDataGridsSelector()
            : this(true)
        {
        }

        public DependedDataGridsSelector(bool isButtonsEnabled)
        {
            InitializeComponent();

            if (!isButtonsEnabled)
            {
                _moveLeftBtn.Enabled = false;
                _moveRightBtn.Enabled = false;
            }
        }

        public event EventHandler<MoveEventArgs> ObjectsMoving;

        public void SetMoveSrcToDstEnabled(bool enabled)
        {
            _moveRightBtn.Enabled = enabled;
        }

        public void SetMoveDstToSrcEnabled(bool enabled)
        {
            _moveLeftBtn.Enabled = enabled;
        }

        public void SetSouceGridCollection(IEnumerable collection)
        {
            _sourceGridView.Grid.DataSource = collection;
        }

        public void SetDestGridCollection(IEnumerable collection)
        {
            _destGridView.Grid.DataSource = collection;
        }

        public Grid.V2.DataGridView SourceGrid
        {
            get { return _sourceGridView; }
        }

        public Grid.V2.DataGridView DestGrid
        {
            get { return _destGridView; }
        }

        private void OnObjectsMoving(MoveEventArgs e)
        {
            var tmp = ObjectsMoving;
            if (tmp != null)
            {
                tmp(this, e);
            }
        }

        

        public  string MoveLeftToolTip
        {
            get { return _ultraToolTipManager.GetUltraToolTip(_moveLeftBtn).ToolTipText; }
            set
            {
                _ultraToolTipManager.SetUltraToolTip(_moveLeftBtn,
                                                     new Infragistics.Win.UltraWinToolTip.UltraToolTipInfo{ ToolTipText = value });
            }
        }

                
        public  string MoveRightToolTip
        {
            get { return _ultraToolTipManager.GetUltraToolTip(_moveRightBtn).ToolTipText; }
            set
            {
                _ultraToolTipManager.SetUltraToolTip(_moveRightBtn,
                                                     new Infragistics.Win.UltraWinToolTip.UltraToolTipInfo{ ToolTipText = value });
            }
        }

        public decimal SourceGridAreaRatio
        {
            get { return (decimal)mainsplitContainer.SplitterDistance / (decimal)mainsplitContainer.Width; }
            set
            {
                if (value < 0 || value > 1)
                {
                    throw new ArgumentOutOfRangeException("value");
                }
                mainsplitContainer.SplitterDistance = Convert.ToInt32(value * mainsplitContainer.Width);
            }
        }

        private void _moveRightBtn_Click(object sender, EventArgs e)
        {
            OnObjectsMoving(new MoveEventArgs(MoveDirection.SourceToDest));
        }

  


        private void _moveLeftBtn_Click(object sender, EventArgs e)
        {
            OnObjectsMoving(new MoveEventArgs(MoveDirection.DestToSource));
        }

        public string DestinationHeaderHtmlText
        {
            get { return _destHeader.Value.ToString(); }
            set { _destHeader.Value = value; }
        }

        public string SourceHeaderHtmlText 
        { 
            get { return _srcHeader.Value.ToString(); }
            set { _srcHeader.Value = value; }
        }
    }


    public enum MoveDirection
    {
        SourceToDest,
        DestToSource
    }
}
