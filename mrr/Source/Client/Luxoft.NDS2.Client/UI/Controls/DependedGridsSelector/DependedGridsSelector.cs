﻿using System;
using System.Collections;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Controls.DependedGridsSelector
{
    public partial class DependedGridsSelector : UserControl, IDependedGridsSelector
    {
        public DependedGridsSelector()
            : this(true)
        {
        }

        public DependedGridsSelector(bool isButtonsEnabled)
        {
            InitializeComponent();

            if (!isButtonsEnabled)
            {
                _moveLeftBtn.Enabled = false;
                _moveRightBtn.Enabled = false;
            }
        }

        public event EventHandler<MoveEventArgs> ObjectsMoving;

        public void SetMoveSrcToDstEnabled(bool enabled)
        {
            _moveRightBtn.Enabled = enabled;
        }

        public void SetMoveDstToSrcEnabled(bool enabled)
        {
            _moveLeftBtn.Enabled = enabled;
        }

        public void SetSouceGridCollection(IEnumerable collection)
        {
            _sourceGridView.Grid.DataSource = collection;
        }

        public void SetDestGridCollection(IEnumerable collection)
        {
            _destGridView.Grid.DataSource = collection;
        }

        public DataGrid.GridView SourceGrid
        {
            get { return _sourceGridView; }
        }

        public DataGrid.GridView DestGrid
        {
            get { return _destGridView; }
        }


        private void OnObjectsMoving(MoveEventArgs e)
        {
            var tmp = ObjectsMoving;
            if (tmp != null)
            {
                tmp(this, e);
            }
        }

        public decimal SourceGridAreaRatio
        {
            get { return (decimal)splitContainer2.Panel1.Width / (decimal)splitContainer2.Width; }
            set
            {
                if (value < 0 || value > 1)
                {
                    throw new ArgumentOutOfRangeException("value");
                }
                splitContainer2.SplitterDistance = Convert.ToInt32(value * splitContainer2.Width);
            }
        }

        private void _moveRightBtn_Click(object sender, EventArgs e)
        {
            OnObjectsMoving(new MoveEventArgs(MoveDirection.SourceToDest));
        }

        private void _moveLeftBtn_Click(object sender, EventArgs e)
        {
            OnObjectsMoving(new MoveEventArgs(MoveDirection.DestToSource));
        }
    }
}
