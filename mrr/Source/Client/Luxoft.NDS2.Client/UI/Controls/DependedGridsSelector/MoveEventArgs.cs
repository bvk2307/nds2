using System;
using System.Collections;

namespace Luxoft.NDS2.Client.UI.Controls.DependedGridsSelector
{
    public class MoveEventArgs: EventArgs
    {
        private readonly MoveDirection _direction;

        public MoveEventArgs(MoveDirection direction)
        {
            _direction = direction;
        }

        public MoveDirection Direction
        {
            get { return _direction; }
        }
    }
}