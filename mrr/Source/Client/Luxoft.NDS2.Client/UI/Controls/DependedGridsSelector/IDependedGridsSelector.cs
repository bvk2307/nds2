using System;
using System.Collections;

namespace Luxoft.NDS2.Client.UI.Controls.DependedGridsSelector
{
    public interface IDependedGridsSelector
    {
        event EventHandler<MoveEventArgs> ObjectsMoving;

        void SetMoveSrcToDstEnabled(bool enabled);

        void SetMoveDstToSrcEnabled(bool enabled);

        void SetSouceGridCollection(IEnumerable collection);

        void SetDestGridCollection(IEnumerable collection);
    }
}