﻿namespace Luxoft.NDS2.Client.UI.Controls.LookupEditor
{
    partial class LookupBase
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            this._selectionView = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this._editSelection = new Infragistics.Win.Misc.UltraButton();
            this._popup = new Infragistics.Win.Misc.UltraPopupControlContainer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this._selectionView)).BeginInit();
            this.SuspendLayout();
            // 
            // _selectionView
            // 
            this._selectionView.AutoSize = false;
            this._selectionView.Dock = System.Windows.Forms.DockStyle.Top;
            this._selectionView.Location = new System.Drawing.Point(0, 0);
            this._selectionView.Margin = new System.Windows.Forms.Padding(0);
            this._selectionView.Name = "_selectionView";
            this._selectionView.ReadOnly = true;
            this._selectionView.Size = new System.Drawing.Size(534, 24);
            this._selectionView.TabIndex = 5;
            // 
            // _editSelection
            // 
            appearance1.Image = global::Luxoft.NDS2.Client.Properties.Resources.drop_down;
            appearance1.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance1.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this._editSelection.Appearance = appearance1;
            this._editSelection.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Office2007RibbonButton;
            this._editSelection.Dock = System.Windows.Forms.DockStyle.Right;
            this._editSelection.Location = new System.Drawing.Point(534, 0);
            this._editSelection.Margin = new System.Windows.Forms.Padding(0);
            this._editSelection.Name = "_editSelection";
            this._editSelection.Size = new System.Drawing.Size(24, 25);
            this._editSelection.TabIndex = 4;
            this._editSelection.UseAppStyling = false;
            this._editSelection.UseFlatMode = Infragistics.Win.DefaultableBoolean.True;
            // 
            // LookupBase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._selectionView);
            this.Controls.Add(this._editSelection);
            this.Name = "LookupBase";
            this.Size = new System.Drawing.Size(558, 25);
            ((System.ComponentModel.ISupportInitialize)(this._selectionView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinEditors.UltraTextEditor _selectionView;
        private Infragistics.Win.Misc.UltraButton _editSelection;
        private Infragistics.Win.Misc.UltraPopupControlContainer _popup;
    }
}
