﻿using Infragistics.Win;
using System.Drawing;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Controls.LookupEditor
{
    public partial class LookupBase : UserControl, ILookupViewer
    {
        private const int PopupYOffset = 2;

        private const int PopupXOffset = 8;

        public LookupBase()
        {
            InitializeComponent();

            _editSelection.Click += (sender, args) =>
            {
                Editor.Width = Width;

                var location = PointToScreen(Point.Empty);
                _popup.Show(
                    new PopupInfo
                    {
                        PreferredLocation =
                           new Point(location.X + PopupXOffset, location.Y + Height + PopupYOffset)
                    });
            };
        }

        public void WithEditor<T>(T editor)
            where T : Control, ILookupEditor
        {
            Editor = editor;
            _popup.PopupControl = editor;
        }

        public override string Text
        {
            get
            {
                return _selectionView.Text;
            }
            set
            {
                _selectionView.Text = value;
            }
        }

        public ILookupEditor Editor { get; private set; }
    }
}
