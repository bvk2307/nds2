﻿using Luxoft.NDS2.Common.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Controls.LookupEditor.LookupFull
{
    public partial class Editor : UserControl, ILookupEditor
    {
        private readonly LookupItemDataSource _dataSource = new LookupItemDataSource();
        
        public Editor()
        {
            InitializeComponent();

            _bindingSource.DataSource = _dataSource;

            _searchString.TextChanged += (sender, args) =>
            {
                var handler = SearchStringChanged;
                if (handler != null)
                    handler(this, new EventArgs());
            };

            _selectAll.Click += (sender, args) =>
            {
                var handler = SelectAllRequested;
                if (handler != null)
                    handler(this, new EventArgs());
            };

            _selectNone.Click += (sender, args) =>
            {
                var handler = SelectNoneRequested;
                if (handler != null)
                    handler(this, new EventArgs());
            };

            new ToolTip().SetToolTip(_searchString, ResourceManagerNDS2.UserControlMessages.SearchStringHint);
        }

        public event EventHandler SelectAllRequested;

        public event EventHandler SelectNoneRequested;

        public event EventHandler SearchStringChanged;

        public event EventHandler HeaderChecking;

        public event EventHandler HeaderChecked;

        public bool AllowSelectAll
        {
            set
            {
                _selectAll.Enabled = value;
            }
        }

        public bool AllowSelectNone
        {
            set
            {
                _selectNone.Enabled = value;
            }
        }

        public string SearchString
        {
            get
            {
                return _searchString.Text;
            }
        }

        public void PushData(IEnumerable<ISelectableLookupItem> data)
        {
            _dataSource.UpdateList(data);
        }
    }
}
