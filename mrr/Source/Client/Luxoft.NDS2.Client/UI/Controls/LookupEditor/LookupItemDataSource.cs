﻿using Luxoft.NDS2.Common.Models.ViewModels;
using System.Collections.Generic;
using System.ComponentModel;

namespace Luxoft.NDS2.Client.UI.Controls.LookupEditor
{
    public class LookupItemDataSource
    {
        public BindingList<ISelectableLookupItem> ListItems
        {
            get;
            private set;
        }

        public LookupItemDataSource()
        {
            ListItems = new BindingList<ISelectableLookupItem>();
        }

        public void UpdateList(IEnumerable<ISelectableLookupItem> data)
        {
            ListItems.Clear();
            foreach (var item in data)
            {
                ListItems.Add(item);
            }
        }
    }
}
