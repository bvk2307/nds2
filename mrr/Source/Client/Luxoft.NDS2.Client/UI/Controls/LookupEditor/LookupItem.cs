﻿using Luxoft.NDS2.Common.Models.ViewModels;
using System;
using System.Globalization;

namespace Luxoft.NDS2.Client.UI.Controls.LookupEditor
{
    public class LookupItem : ISelectableLookupItem
    {
        private bool _isChecked;

        public bool IsChecked
        {
            get { return _isChecked; }
            set
            {
                if (_isChecked == value)
                    return;
                _isChecked = value;
                var handler = CheckedChanged;
                if (handler != null)
                    handler(this, new EventArgs());
            }
        }

        public string Title { get; set; }
        public string Description { get; set; }
        public event EventHandler CheckedChanged;
    }

    public class DataMock
    {

        public ISelectableLookupItem[] Items { get; private set; }

        public DataMock(uint size)
        {
            Items = new ISelectableLookupItem[size];
            
            for (var i = 0; i < size; ++i)
            {
                var code = i.ToString(CultureInfo.InvariantCulture).PadLeft(4, '0');
                Items[i] = new LookupItem { Description = code + " - Классная запись " + i, Title = code };
            }
        }

    }
}
