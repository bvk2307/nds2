﻿using Luxoft.NDS2.Common.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Controls.LookupEditor
{
    public class LookupModel
    {
        private ISelectableLookupItem[] _data;

        private ISelectableLookupItem[] _fullData;

        private bool _disableSelectionChangedEvent;

        public IEnumerable<ISelectableLookupItem> Data
        {
            get { return _data; }
        }

        public LookupModel(ISelectableLookupItem[] data)
        {
            UpdateDictionary(data);
        }

        public bool IsChecked(string title)
        {
            return Selection.Any(x => x.Title == title);
        }

        public void UpdateDictionary(ISelectableLookupItem[] data)
        {
            if (data == null)
                throw new ArgumentNullException("data");

            if (_fullData != null)
            {
                foreach (var item in _fullData)
                {
                    item.CheckedChanged -= OnSelectionChanged;
                }
            }

            foreach (var item in data)
            {
                item.IsChecked = (_fullData != null) && _fullData.Any(i => i.Title == item.Title && i.IsChecked);
                item.CheckedChanged += OnSelectionChanged;
            }

            _fullData = data;
            _data = data;

            RaiseDictionaryChanged();
        }

        private void OnSelectionChanged(object sender, EventArgs e)
        {
            RaiseSelectionChanged();
        }

        public void Search(string condition)
        {
            _data = _fullData.Where(i => i.Description.Contains(condition)).ToArray();

            RaiseDataChanged();
        }

        public void SelectAll()
        {
            Suspend();

            foreach (var item in _data)
            {
                item.IsChecked = true;
            }

            Resume();
        }

        public void SelectNone()
        {
            Suspend();

            foreach (var item in _data)
            {
                item.IsChecked = false;
            }

            Resume();
        }

        public IEnumerable<ISelectableLookupItem> Selection
        {
            get
            {
                return _fullData != null ? _fullData.Where(i => i.IsChecked) : null;
            }
            set
            {
                if (_data == null)
                    return;

                Suspend();

                foreach (var item in _fullData)
                {
                    item.IsChecked = value.Any(i => item.Title == i.Title);
                }

                Resume();
            }
        }

        public bool AllChecked
        {
            get { return _data.All(i => i.IsChecked); }
        }

        public bool AnyChecked
        {
            get { return _data.Any(i => i.IsChecked); }
        }

        public bool Empty()
        {
            return !_fullData.Any();
        }

        public event EventHandler SelectionChanged;

        private void RaiseSelectionChanged()
        {
            if (_disableSelectionChangedEvent)
                return;

            var handler = SelectionChanged;
            if (handler != null)
                handler(this, new EventArgs());
        }

        public event EventHandler DataChanged;

        private void RaiseDataChanged()
        {
            var handler = DataChanged;
            if (handler != null)
                handler(this, new EventArgs());
        }

        public event EventHandler DictionaryChanged;

        private void RaiseDictionaryChanged()
        {
            var handler = DictionaryChanged;
            if (handler != null)
                handler(this, new EventArgs());
        }

        public void Suspend()
        {
            _disableSelectionChangedEvent = true;
        }

        public void Resume()
        {
            _disableSelectionChangedEvent = false;
            RaiseSelectionChanged();
        }
    }


}
