﻿using Luxoft.NDS2.Common.Models.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Controls.LookupEditor
{
    public class LookupPresenter
    {
        private readonly ILookupViewer _viewer;

        private readonly LookupModel _model;

        private const string Separator = " / ";

        private const string AllItemsSelected = "Все";

        private bool EmptySearch
        {
            get
            {
                return string.IsNullOrEmpty(_viewer.Editor.SearchString);
            }
        }

        public IEnumerable<ISelectableLookupItem> Selection
        {
            get
            {
                return _model.Selection;
            }
        }

        public LookupPresenter(ILookupViewer viewer, LookupModel model)
        {
            _viewer = viewer;
            _model = model;
            _viewer.Editor.SearchStringChanged += (s, a) => UpdateModel();
            _viewer.Editor.HeaderChecking += (s, a) => _model.Suspend();
            _viewer.Editor.HeaderChecked += (s, a) => _model.Resume();
            _viewer.Editor.SelectAllRequested += (s, a) => _model.SelectAll();
            _viewer.Editor.SelectNoneRequested += (s, a) => _model.SelectNone();

            _model.SelectionChanged += (s, a) => OnSelectionChanged();
            _model.DataChanged += (s, a) => PushData();
            _model.DictionaryChanged += (s, a) => UpdateModel();

            PushData();
        }

        private void PushData()
        {
            _viewer.Editor.PushData(_model.Data);

            _viewer.Text = EmptySearch && _model.AllChecked
                ? AllItemsSelected
                : string.Join(Separator, _model.Selection.Select(i => i.Title));

            _viewer.Editor.AllowSelectAll = !_model.AllChecked;
            _viewer.Editor.AllowSelectNone = _model.AnyChecked;
        }

        public void OnSelectionChanged()
        {
            PushData();
        }

        private void UpdateModel()
        {
            _model.Search(_viewer.Editor.SearchString);
        }
    }
}
