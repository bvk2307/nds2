﻿using Luxoft.NDS2.Common.Models.ViewModels;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Controls.LookupEditor
{
    public interface ILookupProvider
    {
        IEnumerable<ISelectableLookupItem> Lookup(string searchString = null);
    }
    
    public interface ILookupViewer
    {
        string Text { get; set; }

        ILookupEditor Editor { get; }
    }

    public interface ILookupEditor
    {
        event EventHandler SelectAllRequested;

        event EventHandler SelectNoneRequested;

        event EventHandler SearchStringChanged;

        event EventHandler HeaderChecking;

        event EventHandler HeaderChecked;

        bool AllowSelectAll { set; }

        bool AllowSelectNone { set; }

        string SearchString { get; }

        void PushData(IEnumerable<ISelectableLookupItem> data);

        void Show();

        int Width { set; }
    }
}
