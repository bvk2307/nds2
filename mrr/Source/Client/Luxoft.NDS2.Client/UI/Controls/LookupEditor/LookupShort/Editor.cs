﻿using Luxoft.NDS2.Common.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Controls.LookupEditor.LookupShort
{
    public partial class Editor : UserControl, ILookupEditor
    {
        private readonly LookupItemDataSource _dataSource = new LookupItemDataSource();

        public Editor()
        {
            InitializeComponent();

            _bindingSource.DataSource = _dataSource;

            _grid.BeforeHeaderCheckStateChanged += (s, a) =>
            {
                var handler = HeaderChecking;
                if (handler != null)
                    handler(this, new EventArgs());
            };
            _grid.AfterHeaderCheckStateChanged += (s, a) =>
            {
                var handler = HeaderChecked;
                if (handler != null)
                    handler(this, new EventArgs());
            };
        }

        public event EventHandler HeaderChecking;

        public event EventHandler HeaderChecked;

        public event EventHandler SelectAllRequested;

        public event EventHandler SelectNoneRequested;

        #region Unusable part of ILookupEditor

        public event EventHandler SearchStringChanged;

        // ReSharper disable UnusedAutoPropertyAccessor.Local
        public bool AllowSelectAll { set; private get; }

        public bool AllowSelectNone { set; private get; }

        public string SearchString { get; private set; }
        // ReSharper restore UnusedAutoPropertyAccessor.Local

        #endregion

        public void PushData(IEnumerable<ISelectableLookupItem> data)
        {
            _dataSource.UpdateList(data);
        }
    }
}
