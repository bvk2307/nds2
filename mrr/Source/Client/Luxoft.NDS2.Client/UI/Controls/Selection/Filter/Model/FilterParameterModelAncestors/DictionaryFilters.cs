﻿using Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Mock;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using System;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Model
{
    public class DictionaryListParameterModelBase<T> : ListParameterModelBase<T>
    {
        private IDictionary<T> _dictionary;

        public DictionaryListParameterModelBase(Parameter param, EditorParameter meta, IDictionary<T> dictionary)
            : base(param, meta)
        {
            _dictionary = dictionary;
        }

        public void UpdateDictionary(IDictionary<T> dictionary)
        {
            _dictionary = dictionary;
        }

        public T[] Search(string key)
        {
            return _dictionary.Search(key);
        }
    }

    public class RegionsParameterModel : DictionaryListParameterModelBase<Region>, IRegionBound
    {
        public RegionsParameterModel(Parameter param, EditorParameter meta, IDictionary<Region> dictionary)
            : base(param, meta, dictionary)
        {
            Changed += (sender, args) => RaiseRegionsChanged();
            SetSelection(dictionary.Where(r => param.Values.Any(v => v.Value == r.Code)).ToArray());
        }

        public Region[] Regions
        {
            get
            {
                return Selection;
            } 
        }

        public event EventHandler RegionsChanged;

        private void RaiseRegionsChanged()
        {
            var handler = RegionsChanged;
            if (handler != null)
                handler(this, null);
        }

        protected override ParameterValue[] BuildValueList()
        {
            return Selection.Select(o => new ParameterValue {Value = o.Code, ValueDescription = o.Name}).ToArray();
        }
    }

    public class TaxAuthorityParameterModel : DictionaryListParameterModelBase<Inspection>, IRegionDepender
    {
        public TaxAuthorityParameterModel(Parameter param, EditorParameter meta, IDictionary<Inspection> dictionary)
            : base(param, meta, dictionary)
        {
            SetSelection(dictionary.Where(item => param.Values.Any(v => v.Value == item.Code)).ToArray());
        }

        public void UpdateRegions(Region[] regions)
        {
            var rd = new RegionDictionary(regions);
            var dictionary = rd.GetAuthorities();
            UpdateDictionary(dictionary);
            SetSelection(dictionary.Where(item => Selection.Any(v => v.Code == item.Code)).ToArray());
            RaiseChanged();
        }

        protected override ParameterValue[] BuildValueList()
        {
            return Selection.Select(o => new ParameterValue { Value = o.Code, ValueDescription = o.Name }).ToArray();
        }
    }

}
