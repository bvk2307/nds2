﻿using System;
using System.Collections.Generic;
using DocumentFormat.OpenXml.Vml.Spreadsheet;
using Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Editors;
using Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Model;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using Luxoft.NDS2.Common.Contracts.DTO.SystemSettings;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter
{
    public interface IFilterParameterCreator
    {
        FilterParameterModelBase FactoryMethod(Parameter parameter);

        EditorParameter[] GetMetaData();

        Parameter CreateDefaultParameter(EditorParameter meta);

        Common.Contracts.DTO.Dictionaries.SelectionType SelectionType { get; }
    }

    interface IFilterViewCreator
    {
        IFilterParameterView FactoryMethod(FilterParameterModelBase model);
    }

    public interface IFilterDictionaryService
    {
        RegionDictionary GetRegions();

        TaxAuthorityDictionary GetTaxAuthorities(RegionDictionary regions);

        KeyValuePair<string, string>[] GetOptions(int id);

        EditorParameter[] GetMetaData(Common.Contracts.DTO.Dictionaries.SelectionType type);

        List<KeyValuePair<string, string>> GetWhiteLists();
    }
}
