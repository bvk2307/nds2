﻿namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Editors
{
    partial class RegionControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._lookup = new Luxoft.NDS2.Client.UI.Controls.LookupSelector.Implementation.MultiSelect.LookupMultiSelectView();
            this.SuspendLayout();
            // 
            // _lookup
            // 
            this._lookup.Dock = System.Windows.Forms.DockStyle.Top;
            this._lookup.Location = new System.Drawing.Point(0, 0);
            this._lookup.Margin = new System.Windows.Forms.Padding(0);
            this._lookup.Name = "_lookup";
            this._lookup.PopupHeight = 250;
            this._lookup.Size = new System.Drawing.Size(461, 31);
            this._lookup.TabIndex = 0;
            // 
            // RegionControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._lookup);
            this.Name = "RegionControl";
            this.Size = new System.Drawing.Size(461, 31);
            this.ResumeLayout(false);

        }

        #endregion

        private LookupSelector.Implementation.MultiSelect.LookupMultiSelectView _lookup;
    }
}
