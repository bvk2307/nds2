﻿using System;
using System.Collections.Generic;
using Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Model;
using System.Linq;
using System.Windows.Forms;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Editors
{
    public partial class ListMultiControlWithOperators : UserControl, IFilterParameterView
    {
        private readonly ListParameterModel _model;

        private class KeyComparer : IComparer<string>
        {
            public int Compare(string x, string y)
            {
                return int.Parse(x) - int.Parse(y);
            }
        }

        public ListMultiControlWithOperators(ListParameterModel model)
        {
            _model = model;

            InitializeComponent();

            _comboOperator.DataSource = _model.Operators;
            _comboOperator.ValueMember = "Key";
            _comboOperator.DisplayMember = "Value";
            _comboOperator.Value = _model.Operators.Any(x => x.Key == _model.CompareMethod)
                ? _model.Operators.First(x => x.Key == _model.CompareMethod).Key
                : _model.Operators.First().Key;
            _comboOperator.ValueChanged += (sender, args) =>
            {
                if (_comboOperator.Value is ComparisonOperator) 
                _model.CompareMethod = (ComparisonOperator)_comboOperator.Value;
            };

            _combo.DataSource = _model.Options.OrderBy(o => o.Key, new KeyComparer()).ToArray();
            _combo.ValueMember = "Key";
            _combo.DisplayMember = "Key";
            _combo.AfterDropDown += (sender, args) =>
            {
                _combo.DisplayMember = "Value";
            };

            _combo.AfterCloseUp += (sender, args) =>
            {
                _combo.DisplayMember = "Key";
            };

            SetCheckedItems();

            _combo.ValueChanged += (sender, args) =>
            {
                var list = _combo.Value as List<object>;
                if (list == null)
                {
                    _model.SetSelection(new KeyValuePair<string, string>[] {});
                    return;
                }
                var keys = list.Select(x => x as string);
                _model.SetSelection(_model.Options.Where(x => keys.Contains(x.Key)).ToArray());
            };
        }

        private void SetCheckedItems()
        {
            foreach (var item in _combo.Items)
            {
                if (_model.Selection.Any(sel => sel.Key == item.DataValue.ToString()))
                {
                    item.CheckState = CheckState.Checked;
                }
            }
        }

        public FilterParameterModelBase Model
        {
            get
            {
                return _model;
            }
        }

        public UserControl AsControl()
        {
            return this;
        }

        public event EventHandler HeightChanged;

        public int GetHeight()
        {
            return Height;
        }

        public void Init() {  }
    }
}
