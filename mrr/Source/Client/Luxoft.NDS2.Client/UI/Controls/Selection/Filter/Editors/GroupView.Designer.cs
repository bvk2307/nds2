﻿using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Editors
{
    partial class GroupView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            this._table = new System.Windows.Forms.TableLayoutPanel();
            this._isEnabled = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this._removeButton = new Infragistics.Win.Misc.UltraButton();
            this._expandable = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this._contentTable = new System.Windows.Forms.TableLayoutPanel();
            this._regionMessageLabelPanel = new System.Windows.Forms.TableLayoutPanel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this._contentPanel = new System.Windows.Forms.Panel();
            this._table.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._expandable)).BeginInit();
            this._expandable.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            this._contentTable.SuspendLayout();
            this._regionMessageLabelPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // _table
            // 
            this._table.AutoSize = true;
            this._table.ColumnCount = 3;
            this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this._table.Controls.Add(this._isEnabled, 0, 0);
            this._table.Controls.Add(this._removeButton, 2, 0);
            this._table.Controls.Add(this._expandable, 1, 0);
            this._table.Dock = System.Windows.Forms.DockStyle.Fill;
            this._table.Location = new System.Drawing.Point(0, 0);
            this._table.Name = "_table";
            this._table.RowCount = 1;
            this._table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._table.Size = new System.Drawing.Size(776, 300);
            this._table.TabIndex = 4;
            // 
            // _isEnabled
            // 
            this._isEnabled.Checked = true;
            this._isEnabled.CheckState = System.Windows.Forms.CheckState.Checked;
            this._isEnabled.Dock = System.Windows.Forms.DockStyle.Top;
            this._isEnabled.Location = new System.Drawing.Point(8, 3);
            this._isEnabled.Margin = new System.Windows.Forms.Padding(8, 3, 3, 3);
            this._isEnabled.Name = "_isEnabled";
            this._isEnabled.Size = new System.Drawing.Size(19, 24);
            this._isEnabled.TabIndex = 4;
            // 
            // _removeButton
            // 
            appearance1.Image = global::Luxoft.NDS2.Client.Properties.Resources.delete;
            appearance1.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance1.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this._removeButton.Appearance = appearance1;
            this._removeButton.Dock = System.Windows.Forms.DockStyle.Top;
            this._removeButton.Location = new System.Drawing.Point(749, 3);
            this._removeButton.Name = "_removeButton";
            this._removeButton.Size = new System.Drawing.Size(24, 24);
            this._removeButton.TabIndex = 3;
            this._removeButton.Click += new System.EventHandler(this.RemoveClick);
            // 
            // _expandable
            // 
            this._expandable.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this._expandable.Dock = System.Windows.Forms.DockStyle.Fill;
            this._expandable.ExpandedSize = new System.Drawing.Size(710, 294);
            this._expandable.Location = new System.Drawing.Point(33, 3);
            this._expandable.Name = "_expandable";
            this._expandable.Size = new System.Drawing.Size(710, 294);
            this._expandable.TabIndex = 1;
            this._expandable.Text = "Title";
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.AutoSize = true;
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this._contentTable);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 19);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(704, 272);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // _contentTable
            // 
            this._contentTable.AutoSize = true;
            this._contentTable.ColumnCount = 1;
            this._contentTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._contentTable.Controls.Add(this._regionMessageLabelPanel, 0, 0);
            this._contentTable.Controls.Add(this._contentPanel, 0, 1);
            this._contentTable.Dock = System.Windows.Forms.DockStyle.Top;
            this._contentTable.Location = new System.Drawing.Point(0, 0);
            this._contentTable.Name = "_contentTable";
            this._contentTable.RowCount = 2;
            this._contentTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this._contentTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._contentTable.Size = new System.Drawing.Size(704, 56);
            this._contentTable.TabIndex = 7;
            // 
            // _regionMessageLabelPanel
            // 
            this._regionMessageLabelPanel.AutoSize = true;
            this._regionMessageLabelPanel.ColumnCount = 1;
            this._regionMessageLabelPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 698F));
            this._regionMessageLabelPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this._regionMessageLabelPanel.Controls.Add(this.ultraLabel1, 0, 0);
            this._regionMessageLabelPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._regionMessageLabelPanel.Location = new System.Drawing.Point(3, 3);
            this._regionMessageLabelPanel.Name = "_regionMessageLabelPanel";
            this._regionMessageLabelPanel.RowCount = 1;
            this._regionMessageLabelPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 74F));
            this._regionMessageLabelPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 74F));
            this._regionMessageLabelPanel.Size = new System.Drawing.Size(698, 44);
            this._regionMessageLabelPanel.TabIndex = 7;
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.ultraLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ultraLabel1.Location = new System.Drawing.Point(3, 3);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(645, 68);
            this.ultraLabel1.TabIndex = 0;
            this.ultraLabel1.Text = "Для стороны покупателя атрибуты \"Регион\" и \"Отчетный период\" являются обязательны" +
    "ми для заполнения. Максимальное кол-во регионов, которые можно выбрать = 3";
            // 
            // _contentPanel
            // 
            this._contentPanel.AutoScroll = true;
            this._contentPanel.AutoSize = true;
            this._contentPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._contentPanel.Location = new System.Drawing.Point(3, 53);
            this._contentPanel.Name = "_contentPanel";
            this._contentPanel.Size = new System.Drawing.Size(698, 1);
            this._contentPanel.TabIndex = 8;
            // 
            // GroupView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._table);
            this.Name = "GroupView";
            this.Size = new System.Drawing.Size(776, 300);
            this._table.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._expandable)).EndInit();
            this._expandable.ResumeLayout(false);
            this._expandable.PerformLayout();
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.PerformLayout();
            this._contentTable.ResumeLayout(false);
            this._contentTable.PerformLayout();
            this._regionMessageLabelPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel _table;
        private Infragistics.Win.Misc.UltraExpandableGroupBox _expandable;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.Misc.UltraButton _removeButton;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor _isEnabled;
        private TableLayoutPanel _contentTable;
        private TableLayoutPanel _regionMessageLabelPanel;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Panel _contentPanel;
    }
}
