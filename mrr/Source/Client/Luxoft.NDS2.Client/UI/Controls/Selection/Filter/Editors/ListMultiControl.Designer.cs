﻿namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Editors
{
    partial class ListMultiControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._combo = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            ((System.ComponentModel.ISupportInitialize)(this._combo)).BeginInit();
            this.SuspendLayout();
            // 
            // _combo
            // 
            this._combo.CheckedListSettings.CheckBoxStyle = Infragistics.Win.CheckStyle.CheckBox;
            this._combo.CheckedListSettings.EditorValueSource = Infragistics.Win.EditorWithComboValueSource.CheckedItems;
            this._combo.CheckedListSettings.ListSeparator = "; ";
            this._combo.Dock = System.Windows.Forms.DockStyle.Fill;
            this._combo.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this._combo.Location = new System.Drawing.Point(0, 0);
            this._combo.Name = "_combo";
            this._combo.Size = new System.Drawing.Size(435, 21);
            this._combo.TabIndex = 0;
            // 
            // ListMultiControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._combo);
            this.Name = "ListMultiControl";
            this.Size = new System.Drawing.Size(435, 24);
            ((System.ComponentModel.ISupportInitialize)(this._combo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.UltraWinEditors.UltraComboEditor _combo;


    }
}
