﻿using System;
using Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Model;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Editors
{
    public interface IFilterParameterView
    {
        FilterParameterModelBase Model { get; }

        UserControl AsControl();

        event EventHandler HeightChanged;

        int GetHeight();

        void Init();
    }
}
