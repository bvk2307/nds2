﻿using System;
using Luxoft.NDS2.Client.UI.Navigator.Views;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Model
{
    public class ListParameterModelBase<T> : FilterParameterModelBase, IEnumerable<T>
    {
        public ListParameterModelBase(Parameter param, EditorParameter meta)
            : base(param, meta)
        {
            CompareMethod = param.Operator;
        }

        public override bool IsValid
        {
            get { return true; }
        }

        public override Parameter ToDto()
        {
            return new Parameter
            {
                AttributeId = Id,
                IsEnabled = IsEnabled,
                Operator = CompareMethod,
                Values = BuildValueList()
            };
        }

        protected override ParameterValue[] BuildValueList()
        {
            return Selection.Select(o => new ParameterValue { Value = o.ToString() }).ToArray();
        }

        public ComparisonOperator CompareMethod
        {
            get { return _compareMethod; }
            set
            {
                if (_compareMethod == value)
                    return;
                _compareMethod = value;
                RaiseChanged();
            }
        }

        private T[] _options;

        public IEnumerable<T> Options
        {
            get
            {
                return _options;
            }
            set
            {
                _options = value.ToArray();
                RaiseOptionsChaged();
            }
        }

        public KeyValuePair<ComparisonOperator, string>[] Operators { get; set; }

        private T[] _selection = { };
        private ComparisonOperator _compareMethod;

        public T[] Selection
        {
            get
            {
                return _selection.ToArray();
            }
        }

        public void SetSelection(T[] selection)
        {
            _selection = selection ?? new T[] {};
            RaiseChanged();
        }

        public event EventHandler OptionsChanged;

        private void RaiseOptionsChaged()
        {
            var handler = OptionsChanged;
            if (handler != null)
                handler(this, null);
        }

        #region IEnumerable

        public IEnumerator<T> GetEnumerator()
        {
            return Selection.AsEnumerable().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator() { return GetEnumerator(); }

        #endregion
    }

    public class ListParameterModel : ListParameterModelBase<KeyValuePair<string, string>>
    {
        public ListParameterModel(Parameter param, EditorParameter meta, IEnumerable<KeyValuePair<string, string>> options)
            : base(param, meta)
        {
            Options = options;
            SetSelection(
                param.Values.Any()
                    ? Options.Where(o => param.Values.Any(v => v.Value == o.Key)).ToArray()
                    : new KeyValuePair<string, string>[] { });
        }

        protected override ParameterValue[] BuildValueList()
        {
            return Selection.Select(o => new ParameterValue { Value = o.Key, ValueDescription = o.Value }).ToArray();
        }

        public void ToggleSelection(string key, bool isChecked)
        {
            var list = new List<KeyValuePair<string, string>>();

            if (isChecked)
                list.Add(Options.FirstOrDefault(option => option.Key == key));

            list.AddRange(Selection.Where(x => x.Key != key));

            SetSelection(list.ToArray());
        }
    }

    public class DiscrepancyTypeModel : ListParameterModel, IDiscrepancyTypeSource
    {
        public DiscrepancyTypeModel(Parameter param, EditorParameter meta, IEnumerable<KeyValuePair<string, string>> options)
            : base(param, meta, options) { }
    }
    public class CompareRuleModel : ListParameterModel, IDiscrepancyTypeDepender
    {
        private readonly KeyValuePair<string, string> [] _options;

        public CompareRuleModel(Parameter param, EditorParameter meta, IEnumerable<KeyValuePair<string, string>> options)
            : base(param, meta, options)
        {
            _options = options.ToArray();
        }

        private const string GapKey = "1";
        private const string CurrencyKey = "3";

        private readonly string[] _lockTypeKeys = {GapKey, CurrencyKey};

        public void UpdateOptions(KeyValuePair<string, string>[] selectedTypes)
        {
            if (selectedTypes.Length > 0 && selectedTypes.Any(t => _lockTypeKeys.Contains(t.Key)))
            {
                SetSelection(null);
                Options = new KeyValuePair<string, string>[] {};
            }
            else
            {
                Options = _options;
            }
        }
    }

}
