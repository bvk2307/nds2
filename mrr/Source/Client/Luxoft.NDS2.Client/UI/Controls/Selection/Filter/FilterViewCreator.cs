﻿using Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Editors;
using Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Model;
using Luxoft.NDS2.Client.UI.Controls.Sur;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter
{
    public class FilterViewCreator : IFilterViewCreator
    {
        private readonly INotifier _notifier;
        private readonly DictionarySur _sur;

        public FilterViewCreator(INotifier notifier, DictionarySur sur)
        {
            _notifier = notifier;
            _sur = sur;
            InitCreatorsMap();
        }

        private Dictionary<EditorType, Func<FilterParameterModelBase, IFilterParameterView>> _creatorsMap;

        private void InitCreatorsMap()
        {
            _creatorsMap =
                new Dictionary<EditorType, Func<FilterParameterModelBase, IFilterParameterView>>
                {
                    {EditorType.RegionEditor, CreateRegionEditor},
                    {EditorType.TaxAuthorityEditor, CreateTaxAuthorityEditor},
                    {EditorType.SurEditor, CreateSurEditor},
                    {EditorType.PeriodEditor, CreateListEditor},
                    {EditorType.OperationCodeEditor, CreateListWithOperatorsEditor},
                    {EditorType.IntRangeEditor, CreateRangeEditor},
                    {EditorType.DataRangeEditor, CreateDateRangeEditor},
                    {EditorType.NdsRangeEditor, CreateNdsRangeEditor},
                    {EditorType.InnEditor, CreateInnEditor},
                    {EditorType.CheckBoxEditor, CreateCheckBoxEditor},
                    {EditorType.UntitledCheckBoxEditor, CreateCheckBoxEditor},
                    {EditorType.SimpleListEditor, CreateListEditor},
                    {EditorType.ListMultiEditor, CreateListMultiEditor},
                    {EditorType.UnEditor, CreateReadonlyEditor},
                    {EditorType.WhiteListEditor, CreateListEditor},
                    {EditorType.MultiDiscrepancyAmountControl, CreateDiscrepAmtEditor}
                };
        }

        private static IFilterParameterView CreateReadonlyEditor(FilterParameterModelBase model)
        {
            var m = model as FilterParameterReadonlyModel;
            if (m == null)
                throw new ArgumentException("Модель не является FilterParameterReadonlyModel");

            return new ReadonlyControl(m);
        }

        private static IFilterParameterView CreateNdsRangeEditor(FilterParameterModelBase model)
        {
            var m = model as MultiRangeNDSParameterModel;
            if (m == null)
                throw new ArgumentException("Модель не является MultiRangeNDSParameterModel");

            return new MultiRangeOfNDSControl(m);
        }

        private static IFilterParameterView CreateDiscrepAmtEditor(FilterParameterModelBase model)
        {
            var m = model as MultiDiscrepancyTypeParameterModel;
            if (m == null)
                throw new ArgumentException("Модель не является MultiDiscrepancyTypeParameterModel");

            return new MultiDiscrepancyAmountControl(m);
        }

        private static IFilterParameterView CreateDateRangeEditor(FilterParameterModelBase model)
        {
            var m = model as RangeFilterParameterModel<DateTime?>;
            if (m == null)
                throw new ArgumentException("Модель не является RangeFilterParameterModel<DateTime?>");

            return new RangeOfDateControl(m);
        }

        private static IFilterParameterView CreateListWithOperatorsEditor(FilterParameterModelBase model)
        {
            var m = model as ListParameterModel;
            if (m == null)
                throw new ArgumentException("Модель не является ListParameterModel");

            return new ListMultiControlWithOperators(m);            
        }

        private static IFilterParameterView CreateListMultiEditor(FilterParameterModelBase model)
        {
            var m = model as ListParameterModel;
            if (m == null)
                throw new ArgumentException("Модель не является ListParameterModel");

            return new ListMultiControl(m);
        }

        private static IFilterParameterView CreateInnEditor(FilterParameterModelBase model)
        {
            var m = model as InnFilterModel;
            if (m == null)
                throw new ArgumentException("Модель не является ListParameterModelBase<string>");

            return new InnControl(m);
        }

        private static IFilterParameterView CreateCheckBoxEditor(FilterParameterModelBase model)
        {
            var m = model as ListParameterModel;
            if (m == null)
                throw new ArgumentException("Модель не является ListParameterModel");

            return new CheckBoxControl(m);

        }

        private static IFilterParameterView CreateRangeEditor(FilterParameterModelBase model)
        {
            var m = model as MultiRangeParameterModel<long?>;
            if (m == null)
                throw new ArgumentException("Модель не является MultiRangeParameterModel<long?>");

            return new MultiRangeOfIntControl(m);
        }

        private static IFilterParameterView CreateListEditor(FilterParameterModelBase model)
        {
            var m = model as ListParameterModel;
            if (m == null)
                throw new ArgumentException("Модель не является ListParameterModel");

            return new ListControl(m);
        }

        private IFilterParameterView CreateSurEditor(FilterParameterModelBase model)
        {
            var m = model as ListParameterModel;
            if (m == null)
                throw new ArgumentException("Модель не является ListParameterModel");

            return new SurControl(m, _sur);
        }

        private static IFilterParameterView CreateTaxAuthorityEditor(FilterParameterModelBase model)
        {
            var m = model as TaxAuthorityParameterModel;
            if (m == null)
                throw new ArgumentException("Модель не является TaxAuthorityParameterModel");

            return new TaxAuthorityControl(m);
        }

        private static IFilterParameterView CreateRegionEditor(FilterParameterModelBase model)
        {
            var m = model as RegionsParameterModel;
            if (m == null)
                throw new ArgumentException("Модель не является RegionsParameterModel");

            return new RegionControl(m);
        }

        public IFilterParameterView FactoryMethod(FilterParameterModelBase model)
        {
            try
            {
                return _creatorsMap[model.EditorParameter.ViewType].Invoke(model);
            }
            catch (Exception e)
            {
                _notifier.ShowError(string.Format("Не удалось создать представление: {0}", e.Message));
                return null;
            }
        }
    }
}
