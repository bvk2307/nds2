﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Model;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Editors
{
    public partial class SubgroupView : UserControl
    {
        private readonly FilterGroupModel _model;

        private readonly EditorArea _area;

        // DN поправка при рассчете высоты на заголовок _expandable
        private const int ConstantHeight = 36;

        private readonly Dictionary<EditorArea, string> _areaTitles =
            new Dictionary<EditorArea, string>
            {
                {EditorArea.BuyerSide, "Сторона покупателя"},
                {EditorArea.SellerSide, "Сторона продавца"},
                {EditorArea.DiscrepancyParameters, "Параметры для отбора расхождений"},
            };

        public SubgroupView(FilterGroupModel model, EditorArea area)
        {
            _model = model;

            InitializeComponent();

            _area = area;
            _expandable.Text = _areaTitles[_area];
            _expandable.ExpandedStateChanged += OnExpandedStateChanged;
            SetSize();
            UpdateList();
        }

        public bool IsEditable
        {
            get
            {
                return _table.Enabled;
            }
            set
            {
                _table.Enabled = value;
            }
        }

        private void OnExpandedStateChanged(object sender, EventArgs e)
        {
            SetSize();
        }

        private void UpdateList()
        {
            _selector.SetParameterList(_model.AvailableParameters.Where(x => x.Area == _area));
        }

        private readonly Dictionary<FilterParameterModelBase, Control> _parameterControls = new Dictionary<FilterParameterModelBase, Control>();
        private readonly Padding _noMargin = new Padding(0);

        public void AddFilterParameterViewAndRefresh(IFilterParameterView item)
        {
            AddFilterParameterView(item);
            RefreshViews();
        }

        public void AddFilterParameterView(IFilterParameterView item)
        {
            var container = new EditorBaseView(item);
            _parameterControls[item.Model] = container;
            container.Dock = DockStyle.Top;
            container.Margin = _noMargin;
            item.Model.Deleting += OnDeleting;
            container.HeightChanged += (sender, args) =>
            {
                SetSize();
                var handler = HeightChanged;
                if (handler != null)
                    handler(this, null);
            };
        }

        public void RefreshViews()
        {
            _panel.Controls.Clear();

            SetSize();
            _panel.Controls.AddRange(_parameterControls.Values.Reverse().ToArray());

            UpdateList();
        }

        private void OnDeleting(object sender, EventArgs e)
        {
            var model = sender as FilterParameterModelBase;

            if (model == null)
                return;

            // Может, стоит вынести подписку в модель
            _model.DeleteFilter(model);
            _panel.Controls.Remove(_parameterControls[model]);
            _parameterControls.Remove(model);
            SetSize();
            UpdateList();
        }

        public void SetSize()
        {
            var h = _parameterControls.Values.Sum(control => control.Height);
            _panel.Height = h + 4;
            var height = _selector.Height + h + ConstantHeight;
            Height = _expandable.Expanded
                ? height
                : _expandable.Height;
        }

        private void AddAttribute(object sender, EditorParameterEventArg e)
        {
            try
            {
                _model.AddParameter(e.Meta);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public event EventHandler HeightChanged;
    }
}
