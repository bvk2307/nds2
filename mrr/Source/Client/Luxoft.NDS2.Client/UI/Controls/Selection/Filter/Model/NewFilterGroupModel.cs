﻿using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using System;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Model
{
    public class NewFilterGroupModel : FilterGroupModel
    {
        public NewFilterGroupModel(ParameterGroup group, IFilterParameterCreator paramCreator) 
            : base(group, paramCreator)
        {
            foreach (var p in AvailableParameters)
            {
                AddParameter(p);
            }
        }
    }
}
