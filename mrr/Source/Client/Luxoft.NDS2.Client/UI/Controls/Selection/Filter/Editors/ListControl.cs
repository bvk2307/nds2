﻿using System;
using Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Model;
using System.Linq;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Editors
{
    public partial class ListControl : UserControl, IFilterParameterView
    {
        private readonly ListParameterModel _model;
        
        public ListControl(ListParameterModel model)
        {
            _model = model;

            InitializeComponent();

            _combo.DataSource = _model.Options;
            _combo.ValueMember = "Key";
            _combo.DisplayMember = "Value";
            _combo.Value = _model.Selection.Any()? _model.Selection.First().Key : String.Empty;
            _model.SetSelection(_model.Options.Where(x => x.Key == (string)_combo.Value).ToArray());
            _combo.ValueChanged += (sender, args) =>
            {
                var key = _combo.Value as string;
                if (!string.IsNullOrEmpty(key))
                _model.SetSelection(_model.Options.Where(x => x.Key == key).ToArray());
            };
        }

        public FilterParameterModelBase Model
        {
            get
            {
                return _model;
            }
        }

        public UserControl AsControl()
        {
            return this;
        }

        public event EventHandler HeightChanged;
        public int GetHeight() { return Height; }
        public void Init() {  }
    }
}
