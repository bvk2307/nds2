﻿using Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Model;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using System.Collections.Generic;
using DocumentFormat.OpenXml.Vml.Spreadsheet;
using Luxoft.NDS2.Common.Contracts.DTO.SystemSettings;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Mock
{
    public class FilterDictionaryService :IFilterDictionaryService
    {
        private readonly Dictionary<int, KeyValuePair<string, string>[]> _optionsDictionary;
        private readonly KeyValuePair<string, string>[] _notDefined =
        {
            new KeyValuePair<string, string>("0", "Разрыв")
        };
        
        public FilterDictionaryService()
        {
            var sur = new[]
            {
                new KeyValuePair<string, string>("0", "Высокий"),
                new KeyValuePair<string, string>("1", "Средний"),
                new KeyValuePair<string, string>("2", "Низкий"),
                new KeyValuePair<string, string>("3", "Неопределенный")
            };
            var discrepancies = new[]
            {
                new KeyValuePair<string, string>("0", "Разрыв"),
                new KeyValuePair<string, string>("2", "Проверка НДС"),
            };
            var rules = new[]
            {
                new KeyValuePair<string, string>("0", "Точное сопоставление"),
                new KeyValuePair<string, string>("1", "НДС"),
            };
            var largest = new[]
            {
                new KeyValuePair<string, string>("0", "Крупнейший"),
                new KeyValuePair<string, string>("1", "Не крупнейший")
            };
            var zero = new[]
            {
                new KeyValuePair<string, string>("1", "")
            };
            var notOffered = new[]
            {
                new KeyValuePair<string, string>("1", "")
            };
            var selected = new[]
            {
                new KeyValuePair<string, string>("0", "Отобрано в другую выборку"),
                new KeyValuePair<string, string>("1", "Не отобрано в другую выборку")
            };

            _optionsDictionary = new Dictionary<int, KeyValuePair<string, string>[]>
            {
                {104, sur},
                {204, sur},
                {105, largest},
                {205, largest},
                {219, zero},
                {220, notOffered},
                {301, discrepancies},
                {302, rules},
                {308, selected}
            };
        }
        
        public RegionDictionary GetRegions()
        {
            return Regions.GetRegions(80, 3);
        }

        public TaxAuthorityDictionary GetTaxAuthorities(RegionDictionary regions)
        {
            return regions.GetAuthorities();
        }

        public KeyValuePair<string, string>[] GetOptions(int id)
        {
            KeyValuePair<string, string>[] options;
            return _optionsDictionary.TryGetValue(id, out options) ? options : _notDefined;
        }

        public EditorParameter[] GetMetaData(Common.Contracts.DTO.Dictionaries.SelectionType type)
        {
            return MetaData.GetMetaData();
        }

        public List<KeyValuePair<string, string>> GetWhiteLists()
        {
            throw new System.NotImplementedException();
        }
    }
}
