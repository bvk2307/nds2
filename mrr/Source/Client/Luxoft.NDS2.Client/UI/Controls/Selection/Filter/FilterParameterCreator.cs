﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Model;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace.Enums;
using Luxoft.NDS2.Common.Contracts.Helpers;
using System.Globalization;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter
{
    public class FilterParameterCreator : IFilterParameterCreator
    {
        private readonly IFilterDictionaryService _dictionaryService;
        private readonly INotifier _notifier;

        private readonly RegionDictionary _regions;

        private readonly TaxAuthorityDictionary _taxAuthorities;

        private readonly EditorParameter[] _editorParameters;

        private readonly Dictionary<ParameterType, Func<Parameter, EditorParameter, FilterParameterModelBase>> _createMap;

        private readonly Dictionary<ComparisonOperator, ComparisonOperator> _convertForListModel =
            new Dictionary<ComparisonOperator, ComparisonOperator>
            {
                {ComparisonOperator.InList, ComparisonOperator.Contains},
                {ComparisonOperator.NotInList, ComparisonOperator.NotContains}
            };

        #region .ctor

        public Common.Contracts.DTO.Dictionaries.SelectionType SelectionType { get; private set; }

        public FilterParameterCreator(IFilterDictionaryService dictionaryService,
            INotifier notifier,
            Common.Contracts.DTO.Dictionaries.SelectionType type)
        {
            _dictionaryService = dictionaryService;
            _notifier = notifier;
            _regions = _dictionaryService.GetRegions();
            _taxAuthorities = _dictionaryService.GetTaxAuthorities(_regions);
            _editorParameters = _dictionaryService.GetMetaData(type);
            SelectionType = type;

            _createMap = new Dictionary<ParameterType, Func<Parameter, EditorParameter, FilterParameterModelBase>>
            {
                {ParameterType.RangeOfIntModel, (p, m) => new RangeFilterParameterModel<int?>(p, m, ValidateIntRange)},
                {ParameterType.RangeOfDateModel, CreateRangeOfDateModel},
                {ParameterType.MultiRangeOfIntModel, CreateMultiRangeOfIntModel},
                {ParameterType.RegionsModel, (p, m) => new RegionsParameterModel(p, m, _regions)},
                {ParameterType.TaxAuthorityModel, (p, m) => new TaxAuthorityParameterModel(p, m, _taxAuthorities)},
                {ParameterType.ListOfStringModel, (p, m) =>
                {
                    var model = new ListParameterModelBase<string>(p, m)
                    {
                        Operators = new[]
                        {
                            new KeyValuePair<ComparisonOperator, string>(ComparisonOperator.InList, "Из перечня"),
                            new KeyValuePair<ComparisonOperator, string>(ComparisonOperator.NotInList, "Вне перечня")
                        }
                    };
                    model.SetSelection(p.Values.Select(v => v.Value).ToArray());
                    return model;
                }},
                {ParameterType.ListModel, CreateListModel},
                {ParameterType.PeriodsModel, CreateListModel},
                {ParameterType.OperationCodes, CreateOperationCodeListModel},
                {ParameterType.LabelModel, (p, m) => new FilterParameterReadonlyModel(p, m)},
                {ParameterType.MultiRangeOfNDSModel, CreateMultiRangeOfNdsModel},
                 {ParameterType.DiscrepancyTypeModel, (p, m) =>
                {
                    var options = _dictionaryService.GetOptions(m.Id);
                    var model = new DiscrepancyTypeModel(p, m, options);
                    return model;
                }},
                {ParameterType.MultiDiscrepancyTypeParameterModel, CreateMultiDiscrepTypeModel},            
                {ParameterType.CompareRuleModel, (p, m) =>
                {
                    var options = _dictionaryService.GetOptions(m.Id);
                    var model = new CompareRuleModel(p, m, options);
                    return model;
                }},
                {ParameterType.InnFilterModel, (p, m) =>
                {
                    var options = _dictionaryService.GetWhiteLists();
                    var model = new InnFilterModel(p, m, options);
                    model.Operators =  new[]
                    {
                        new KeyValuePair<ComparisonOperator, string>(ComparisonOperator.InList, "Из перечня"),
                        new KeyValuePair<ComparisonOperator, string>(ComparisonOperator.NotInList, "Вне перечня"),
                        new KeyValuePair<ComparisonOperator, string>(ComparisonOperator.NotDefinedOperator,
                            "Белый список")
                    };
                    return model;
                }},
                {ParameterType.WhiteListModel, CreateWhiteListModel},
           };
        }

        #endregion

        public FilterParameterModelBase CreateOperationCodeListModel(Parameter p, EditorParameter m)
        {
            var options = _dictionaryService.GetOptions(m.Id);
            var model = new ListParameterModel(p, m, options)
            {
                Operators = new[]
                        {
                            new KeyValuePair<ComparisonOperator, string>(ComparisonOperator.Equals, "Равно"),
                            new KeyValuePair<ComparisonOperator, string>(ComparisonOperator.NotEquals, "Не равно"),
                            new KeyValuePair<ComparisonOperator, string>(ComparisonOperator.Contains, "Содержит"),
                            new KeyValuePair<ComparisonOperator, string>(ComparisonOperator.NotContains, "Не содержит"),
                            new KeyValuePair<ComparisonOperator, string>(ComparisonOperator.OneOf, "Один из"),
                            new KeyValuePair<ComparisonOperator, string>(ComparisonOperator.InList, "Из перечня")
                        }
            };
            return model;
        }

        public FilterParameterModelBase CreateListModel(Parameter p, EditorParameter m)
        {
            var options = _dictionaryService.GetOptions(m.Id);
            if (_convertForListModel.Any(x => x.Value == p.Operator))
                p.Operator = _convertForListModel.FirstOrDefault(x => x.Value == p.Operator).Key;
            var model = new ListParameterModel(p, m, options)
            {
                Operators = new[]
                        {
                            new KeyValuePair<ComparisonOperator, string>(ComparisonOperator.InList, "Из перечня"),
                            new KeyValuePair<ComparisonOperator, string>(ComparisonOperator.NotInList, "Вне перечня")
                        }
            };
            return model;
        }

        public FilterParameterModelBase CreateWhiteListModel(Parameter p, EditorParameter m)
        {
            var options = _dictionaryService.GetWhiteLists();
            if (_convertForListModel.Any(x => x.Value == p.Operator))
                p.Operator = _convertForListModel.FirstOrDefault(x => x.Value == p.Operator).Key;
            var model = new ListParameterModel(p, m, options)
            {
                Operators = new[]
                        {
                            new KeyValuePair<ComparisonOperator, string>(ComparisonOperator.NotDefinedOperator,
                            "Белый список")
                        }
            };
            return model;
        }
        #region валидаторы

        private bool ValidateIntRange(int? a, int? b)
        {
            return (a != null && b != null && a <= b) || (a == null || b == null);
        }

        private bool ValidateLongRange(long? a, long? b)
        {
            return (a != null && b != null && a <= b) || (a == null || b == null);
        }

        private bool ValidateDateRange(DateTime? a, DateTime? b)
        {
            return (a != null && b != null && a <= b) || (a == null || b == null);
        }

        #endregion

        #region Диапазонные модели

        private string CorrectNumberValue(string value)
        {
            if (string.IsNullOrEmpty(value))
                return value;
            var result = new StringBuilder();
            foreach (var c in value)
            {
                if (c >= '0' && c <= '9')
                    result.Append(c);
                if (c == '.' || c == ',')
                    break;
            }
            return result.ToString();
        }

        private FilterParameterModelBase CreateRangeOfDateModel(Parameter p, EditorParameter m)
        {
            var model = new RangeFilterParameterModel<DateTime?>(p, m, ValidateDateRange);
            if (!p.Values.Any())
                return model;

            var value = p.Values.First();
            DateTime date;
            model.ValueFrom = DateTime.TryParse(value.ValueFrom, out date) ? date : (DateTime?)null;
            model.ValueTo = DateTime.TryParse(value.ValueTo, out date) ? date : (DateTime?)null;

            return model;
        }

        private FilterParameterModelBase CreateMultiRangeOfIntModel(Parameter p, EditorParameter m)
        {
            var model = new MultiRangeParameterModel<long?>(p, m, ValidateLongRange);

            foreach (var value in p.Values)
            {
                long i;
                var from = long.TryParse(CorrectNumberValue(value.ValueFrom), out i) ? i : (long?)null;
                var to = long.TryParse(CorrectNumberValue(value.ValueTo), out i) ? i : (long?)null;
                model.AddRange(new MultiRangeItemModel<long?>(p, m, ValidateLongRange)
                {
                    ValueFrom = from,
                    ValueTo = to
                });
            }
            return model;
        }

        private FilterParameterModelBase CreateMultiRangeOfNdsModel(Parameter p, EditorParameter m)
        {
            var model = new MultiRangeNDSParameterModel(p, m);

            foreach (var value in p.Values)
            {
                int type;
                decimal i;
                model.AddRange(new RangeNDSFilterParameterModel(p, m)
                {
                    TypeOfDeclaration = int.TryParse(value.Value, out type) ? (DeclarationType)type : DeclarationType.Empty,
                    ValueFrom = decimal.TryParse(value.ValueFrom, NumberStyles.Number, CultureInfo.InvariantCulture, out i) ? i : (decimal?)null,
                    ValueTo = decimal.TryParse(value.ValueTo, NumberStyles.Number, CultureInfo.InvariantCulture, out i) ? i : (decimal?)null
                });
            }
            return model;

        }

        private FilterParameterModelBase CreateMultiDiscrepTypeModel(Parameter p, EditorParameter m)
        {
            var model = new MultiDiscrepancyTypeParameterModel(p, m);

            foreach (var value in p.Values)
            {
                decimal i;
                int type;
                model.AddRange(new DiscrepancyAmountModel(p, m)
                {
                    TypeOfiscrepancy = int.TryParse(value.Value, out type) ? (type > 0 ? (DiscrepancyType)type : DiscrepancyType.Break) : DiscrepancyType.Break,
                    ValueFrom = decimal.TryParse(value.ValueFrom, NumberStyles.Number, CultureInfo.InvariantCulture, out i) ? i : (decimal?)null,
                    ValueTo = decimal.TryParse(value.ValueTo, NumberStyles.Number, CultureInfo.InvariantCulture, out i) ? i : (decimal?)null
                });
            }
            return model;

        }

        #endregion

        public FilterParameterModelBase FactoryMethod(Parameter parameter)
        {
            try
            {
                var ep = _editorParameters.FirstOrDefault(x => x.Id == parameter.AttributeId);
                if (ep == null)
                    throw new ArgumentOutOfRangeException(TypeHelper<Parameter>.GetMemberName(x => x.AttributeId));

                return _createMap[ep.ModelType].Invoke(parameter, ep);
            }
            catch (Exception e)
            {
                _notifier.ShowError(string.Format("Не удалось создать модель: {0}", e.Message));
                return null;
            }
        }

        public EditorParameter[] GetMetaData()
        {
            return _editorParameters;
        }

        #region Параметры по-умолчанию

        private readonly Dictionary<ParameterType, Func<EditorParameter, Parameter>> _parameterMap = new Dictionary
            <ParameterType, Func<EditorParameter, Parameter>>
        {
            {ParameterType.ListModel, DefaultListParameter},
            {ParameterType.PeriodsModel, DefaultPeriodParameter},
            {ParameterType.ListOfStringModel, DefaultListParameter},
            {ParameterType.RangeOfIntModel, DefaultRangeParameter},
            {ParameterType.RangeOfDateModel, DefaultRangeParameter},
            {ParameterType.MultiRangeItemOfIntModel, DefaultRangeParameter},
            {ParameterType.MultiRangeOfIntModel, DefaultRangeParameter},
            {ParameterType.MultiRangeOfNDSModel, DefaultRangeParameter},
            {ParameterType.RegionsModel, DefaultListParameter},
            {ParameterType.TaxAuthorityModel, DefaultListParameter},
            {ParameterType.InnFilterModel, DefaultListParameter},
            {ParameterType.WhiteListModel, DefaultNotDefinedListParameter},
            {ParameterType.OperationCodes, DefaultOperationCodeParameter}
        };


        private static Parameter DefaultRangeParameter(EditorParameter meta)
        {
            return new Parameter
            {
                AttributeId = meta.Id,
                IsEnabled = true,
                Operator = ComparisonOperator.Beetween,
                Values = new ParameterValue[] { }
            };
        }

        private static Parameter DefaultListParameter(EditorParameter meta)
        {
            return new Parameter
            {
                AttributeId = meta.Id,
                IsEnabled = true,
                Operator = ComparisonOperator.InList,
                Values = new ParameterValue[] { }
            };
        }

        private static Parameter DefaultOperationCodeParameter(EditorParameter meta)
        {
            return new Parameter
            {
                AttributeId = meta.Id,
                IsEnabled = true,
                Operator = ComparisonOperator.Contains,
                Values = new ParameterValue[] { }
            };
        }

        private static Parameter DefaultNotDefinedListParameter(EditorParameter meta)
        {
            return new Parameter
            {
                AttributeId = meta.Id,
                IsEnabled = true,
                Operator = ComparisonOperator.NotDefinedOperator,
                Values = new ParameterValue[] { }
            };
        }

        private static Parameter DefaultPeriodParameter(EditorParameter meta)
        {
            return new Parameter
            {
                AttributeId = meta.Id,
                IsEnabled = true,
                Operator = ComparisonOperator.InList,
                Values = new[]
                {
                    new ParameterValue { Value = GetFinishedPeriod()}
                }
            };

        }

        private static string GetFinishedPeriod()
        {
            var qmap = new Dictionary<int, string>
            {
                {1, "24"},
                {2, "24"},
                {3, "24"},
                {4, "21"},
                {5, "21"},
                {6, "21"},
                {7, "22"},
                {8, "22"},
                {9, "22"},
                {10, "23"},
                {11, "23"},
                {12, "23"}
            };

            var p = qmap[DateTime.Today.Month];

            return (p != "24" ? DateTime.Today.Year : DateTime.Today.Year - 1) + p;
        }
        
        public Parameter CreateDefaultParameter(EditorParameter meta)
        {
            Func<EditorParameter, Parameter> creator;
            return _parameterMap.TryGetValue(meta.ModelType, out creator)
                ? creator.Invoke(meta)
                : new Parameter
                {
                    AttributeId = meta.Id,
                    IsEnabled = true,
                    Operator = ComparisonOperator.Equals,
                    Values = new ParameterValue[] { }
                };
        }

        #endregion
    }
}
