﻿using System;
using System.Collections.Generic;
using Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Model;
using System.Linq;
using System.Windows.Forms;
using System.ComponentModel;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Editors
{
    public partial class ListMultiControl : UserControl, IFilterParameterView
    {
        private readonly ListParameterModel _model;

        private readonly BindingList<KeyValuePair<string, string>> _dataSource = new BindingList<KeyValuePair<string, string>>();
        private BindingSource _bindingSoure;
        private bool _ignoreModelEvents;

        public ListMultiControl(ListParameterModel model)
        {
            _model = model;

            InitializeComponent();

            _model.Options.ToList().ForEach(x => _dataSource.Add(x));

            _bindingSoure = new BindingSource();
            _bindingSoure.DataSource = _dataSource;
            _combo.DataSource = _bindingSoure;
            _combo.ValueMember = "Key";
            _combo.DisplayMember = "Value";
            _combo.DataBind();
            SetCheckedItems();
            _combo.Enabled = _model.Options.Any();

            _combo.ValueChanged += (sender, args) =>
            {
                if (_ignoreModelEvents)
                    return;
                _ignoreModelEvents = true;

                var list = _combo.Value as List<object>;
                if (list == null)
                {
                    _model.SetSelection(new KeyValuePair<string, string>[] { });
                    _ignoreModelEvents = false;
                    return;
                }
                var keys = list.Select(x => x as string);
                _model.SetSelection(_model.Options.Where(x => keys.Contains(x.Key)).ToArray());

                _ignoreModelEvents = false;
            };

            _model.Changed += (sender, args) =>
            {
                if (_ignoreModelEvents)
                    return;
                _ignoreModelEvents = true;

                SetCheckedItems();

                _ignoreModelEvents = false;
            };
            _model.OptionsChanged += (sender, args) =>
            {
                if (_ignoreModelEvents)
                    return;
                _ignoreModelEvents = true;

                _combo.Enabled = _model.Options.Any();
                if (_combo.Enabled && (!_dataSource.Any() || !_dataSource.All(x => _model.Options.Any(o => o.Key == x.Key))))
                {
                    _dataSource.Clear();
                    _model.Options.ToList().ForEach(x=>_dataSource.Add(x));
                    _combo.DataBind();
                    SetCheckedItems();
                }

                _ignoreModelEvents = false;
            };
        }

        private void SetCheckedItems()
        {
            foreach (var item in _combo.Items)
            {
                item.CheckState = _model.Selection.Any(sel => sel.Key == item.DataValue.ToString())
                    ? CheckState.Checked : CheckState.Unchecked;
            }
        }

        public FilterParameterModelBase Model
        {
            get
            {
                return _model;
            }
        }

        public UserControl AsControl()
        {
            return this;
        }

        public event EventHandler HeightChanged;

        public int GetHeight()
        {
            return Height;
        }

        public void Init() { }
    }
}
