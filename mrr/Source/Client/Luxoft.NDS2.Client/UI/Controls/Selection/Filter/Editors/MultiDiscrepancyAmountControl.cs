﻿using Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Editors
{
    public partial class MultiDiscrepancyAmountControl : UserControl, IFilterParameterView
    {
        private readonly MultiDiscrepancyTypeParameterModel _model;
        private const string DiscrepBreakTypeId = "1";
        public MultiDiscrepancyAmountControl(MultiDiscrepancyTypeParameterModel model)
        {
            _model = model;
            InitializeComponent();

            foreach (var range in _model.Ranges)
            {
                AddRange(range);
            }

            _model.Changed += (sender, args) =>
            {
                foreach (var range in _model.Ranges.Where(r => _controlMap.All(item => !item.Key.Equals(r))))
                {
                    AddRange(range);
                }
            };
        }

        private readonly List<KeyValuePair<DiscrepancyAmountModel, DiscrepancyAmountEditor>> _controlMap =
            new List<KeyValuePair<DiscrepancyAmountModel, DiscrepancyAmountEditor>>();

        private void SetupRangeEditorButtons()
        {
            if (_controlMap.Count == 1)
            {
                var control = _controlMap.First().Value;
                control.IsAllowAdd = true;
                control.IsAllowRemove = false;
                control.IsAllowChangeType = true;
                return;
            }
            if (_controlMap.Count == 2)
            {
                foreach (var control in _controlMap.Select(x=>x.Value))
                {
                    control.IsAllowAdd = false;
                    control.IsAllowRemove = true;
                    control.IsAllowChangeType = false;

                }
            }
        }

        private void AddRange(DiscrepancyAmountModel range)
        {
            Controls.Clear();

            var control = new DiscrepancyAmountEditor(range) { Dock = DockStyle.Top };
            _controlMap.Add(new KeyValuePair<DiscrepancyAmountModel, DiscrepancyAmountEditor>(range,control));
            range.Deleting += DeleteRange;
            control.Adding += AddRange;
            _controlMap.Reverse();
            foreach (var editor in _controlMap.Select(x=>x.Value))
            {
                Controls.Add(editor);
            }

            SetSize();
            SetupRangeEditorButtons();
        }

        private void AddRange(object sender, EventArgs e)
        {
            Common.Contracts.DTO.Business.Discrepancy.DiscrepancyType type = Common.Contracts.DTO.Business.Discrepancy.DiscrepancyType.Break;
            if (_model.Selection != null && _model.Selection.FirstOrDefault().Key == DiscrepBreakTypeId)
               type = Common.Contracts.DTO.Business.Discrepancy.DiscrepancyType.NDS;
            _model.AddRange(new DiscrepancyAmountModel(new Common.Contracts.DTO.SelectionFilterNamespace.Parameter(), _model.EditorParameter)
            {
                TypeOfiscrepancy = type,
                ValueFrom = null,
                ValueTo = null
            });
        }

        private void DeleteRange(object sender, EventArgs e)
        {
            var model = sender as DiscrepancyAmountModel;
            if (model == null)
                return;
            Controls.Remove(_controlMap.FirstOrDefault(x => x.Key == model).Value);
            _controlMap.Remove(_controlMap.FirstOrDefault(x => x.Key == model));
            _model.DeleteRange(model);
            
            SetSize();
            SetupRangeEditorButtons();
        }

        private void SetSize()
        {
            var height = _controlMap.Select(x=> x.Value).Sum(c => c.Height) + 8;
            Height = height;
            var handler = HeightChanged;
            if (handler != null)
                handler(this, null);
        }

        public FilterParameterModelBase Model
        {
            get
            {
                return _model;
            }
        }

        public UserControl AsControl() { return this; }

        public event EventHandler HeightChanged;

        public int GetHeight()
        {
            return _controlMap.Select(x=> x.Value).Sum(c => c.Height) + 8;
        }

        public void Init()
        {
            SetSize();
        }
    }
}
