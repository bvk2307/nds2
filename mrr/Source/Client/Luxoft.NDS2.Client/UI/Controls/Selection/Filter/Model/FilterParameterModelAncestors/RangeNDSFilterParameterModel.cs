﻿using System.Globalization;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace.Enums;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Model
{
    public class RangeNDSFilterParameterModel : FilterParameterModelBase
    {
        public RangeNDSFilterParameterModel(Parameter param, EditorParameter meta)
            : base(param, meta)
        {
        }

        public override bool IsValid
        {
            get
            {
                return (_valueFrom != null && _valueTo != null && _valueFrom <= _valueTo) || (_valueFrom == null | _valueTo == null);
            }
        }

        public decimal? ValueFrom
        {
            get { return _valueFrom; }
            set
            {
                if (value == _valueFrom)
                    return; 
                _valueFrom = value;
                RaiseChanged();
            }
        }

        public decimal? ValueTo
        {
            get { return _valueTo; }
            set
            {
                if (value == _valueTo)
                    return;
                _valueTo = value;
                RaiseChanged();
            }
        }

        public DeclarationType TypeOfDeclaration
        {
            get { return _typeOfDeclaration; }
            set
            {
                if (value == _typeOfDeclaration)
                    return;
                var prevType = _typeOfDeclaration;
                _typeOfDeclaration = value;
                if (prevType == DeclarationType.Zero)
                {
                    ValueTo = null;
                    ValueFrom = null;
                }
                if (_typeOfDeclaration == DeclarationType.Zero)
                {
                    ValueTo = 0;
                    ValueFrom = 0;
                }
                RaiseChanged();
            }
        }

        protected override ParameterValue[] BuildValueList()
        {
            return new ParameterValue[] { };
        }

        public ParameterValue GetValue()
        {
            string desc;
            return new ParameterValue
            {
                Value = ((int) TypeOfDeclaration).ToString(CultureInfo.InvariantCulture),
                ValueFrom = ValueToString(ValueFrom),
                ValueTo = ValueToString(ValueTo),
                ValueDescription = _typeDic.TryGetValue(_typeOfDeclaration, out desc) ? desc : string.Empty
            };
        }

        protected string ValueToString(decimal? value)
        {
            return value == null ? null : ((decimal)value).ToString("0.00", CultureInfo.InvariantCulture);
        }

        public override Parameter ToDto()
        {
            return new Parameter
            {
                AttributeId = Id,
                IsEnabled = IsEnabled,
                Operator = ComparisonOperator.Beetween,
                Values = BuildValueList()
            };
        }

        private decimal? _valueFrom;

        private decimal? _valueTo;

        private DeclarationType _typeOfDeclaration;

        private readonly Dictionary<DeclarationType, string> _typeDic = new Dictionary<DeclarationType, string>
        {
            {DeclarationType.Empty, ""},
            {DeclarationType.ToPay, "К уплате"},
            {DeclarationType.ToCharge, "К возмещению"},
            {DeclarationType.Zero, "Нулевая"}
        };
    }

    public class MultiRangeNDSParameterModel : FilterParameterModelBase
    {
        private readonly List<RangeNDSFilterParameterModel> _ranges;

        public MultiRangeNDSParameterModel(Parameter param, EditorParameter meta)
            : base(param, meta)
        {
            _ranges = new List<RangeNDSFilterParameterModel>();
            if (!param.Values.Any())
                AddRange();
        }

        protected override ParameterValue[] BuildValueList()
        {
            return _ranges.Select(r => r.GetValue()).ToArray();
        }

        public override Parameter ToDto()
        {
            return new Parameter
            {
                AttributeId = Id,
                IsEnabled = IsEnabled,
                Operator = ComparisonOperator.Beetween,
                Values = BuildValueList()
            };
        }

        public void Clear()
        {
            _ranges.Clear();
            RaiseChanged();
        }

        public void AddRange(RangeNDSFilterParameterModel range = null)
        {
            if (range == null)
            {
                range = new RangeNDSFilterParameterModel(new Parameter(), EditorParameter);
            }
            range.Changed += (sender, args) => RaiseChanged();
            _ranges.Add(range);
            RaiseChanged();
        }

        public void DeleteRange(RangeNDSFilterParameterModel range)
        {
            _ranges.Remove(range);
            RaiseChanged();
        }

        public RangeNDSFilterParameterModel[] Ranges
        {
            get
            {
                return _ranges.ToArray();
            }
        }

        public override bool IsValid
        {
            get
            {
                return _ranges.All(r => r.IsValid);
            }
        }
    }
}
