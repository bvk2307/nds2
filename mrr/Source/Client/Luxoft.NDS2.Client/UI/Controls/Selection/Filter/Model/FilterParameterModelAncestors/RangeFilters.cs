﻿using System;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Model
{
    public class RangeFilterParameterModel<T> : FilterParameterModelBase
    {
        private readonly Func<T, T, bool> _compare;

        public RangeFilterParameterModel(Parameter param, EditorParameter meta, Func<T, T, bool> compare)
            : base(param, meta)
        {
            _compare = compare;
        }

        public override bool IsValid
        {
            get
            {
                return _compare(ValueFrom, ValueTo);
            }
        }

        public T ValueFrom
        {
            get { return _valueFrom; }
            set
            {
                _valueFrom = value;
                RaiseChanged();
            }
        }

        public T ValueTo
        {
            get { return _valueTo; }
            set
            {
                _valueTo = value;
                RaiseChanged();
            }
        }

        protected override ParameterValue[] BuildValueList()
        {
            return new[] { new ParameterValue { ValueFrom = ValueToString(ValueFrom), ValueTo = ValueToString(ValueTo) } };
        }

        protected string ValueToString(T value)
        {
            return value == null ? null : value.ToString();
        }

        public override Parameter ToDto()
        {
            return new Parameter
            {
                AttributeId = Id,
                IsEnabled = IsEnabled,
                Operator = ComparisonOperator.Beetween,
                Values = BuildValueList()
            };
        }

        private T _valueFrom;

        private T _valueTo;
        
    }

    public class MultiRangeItemModel<T> : RangeFilterParameterModel<T>
    {
        public MultiRangeItemModel(Parameter param, EditorParameter meta, Func<T, T, bool> compare)
            : base(param, meta, compare)
        {
        }

        public ParameterValue GetValue()
        {
            return new ParameterValue { ValueFrom = ValueToString(ValueFrom), ValueTo = ValueToString(ValueTo) };
        }
    }

    public class MultiRangeParameterModel<T> : FilterParameterModelBase
    {
        private readonly List<MultiRangeItemModel<T>> _ranges;

        private readonly Func<T, T, bool> _compare;

        public MultiRangeParameterModel(Parameter param, EditorParameter meta, Func<T, T, bool> compare)
            : base(param, meta)
        {
            _compare = compare;
            _ranges = new List<MultiRangeItemModel<T>>();
            if (!param.Values.Any())
                AddRange();
        }

        protected override ParameterValue[] BuildValueList()
        {
            return _ranges.Select(r => r.GetValue()).ToArray();
        }

        public override Parameter ToDto()
        {
            return new Parameter
            {
                AttributeId = Id,
                IsEnabled = IsEnabled,
                Operator = ComparisonOperator.Beetween,
                Values = BuildValueList()
            };
        }

        public void Clear()
        {
            _ranges.Clear();
            RaiseChanged();
        }

        public void AddRange(MultiRangeItemModel<T> range = null)
        {
            if (range == null)
            {
                range = new MultiRangeItemModel<T>(new Parameter(), EditorParameter, _compare);
            }
            range.Changed += (sender, args) => RaiseChanged();
            _ranges.Add(range);
            RaiseChanged();
        }

        public void DeleteRange(MultiRangeItemModel<T> range)
        {
            _ranges.Remove(range);
            RaiseChanged();
        }

        public MultiRangeItemModel<T>[] Ranges
        {
            get
            {
                return _ranges.ToArray();
            }
        }

        public override bool IsValid
        {
            get
            {
                return _ranges.All(r => r.IsValid);
            }
        }
    }
}
