﻿using System;
using Luxoft.NDS2.Client.Model;
using Luxoft.NDS2.Client.UI.Controls.LookupSelector;
using Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Model;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using MultiSelect = Luxoft.NDS2.Client.UI.Controls.LookupSelector.Implementation.MultiSelect;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Editors
{
    public partial class RegionControl : UserControl, IFilterParameterView
    {
        #region nested adapters

        class RegionLookupEntry : ILookupEntry<string>
        {
            private readonly Region _region;
            public bool IsSelected { get; set; }

            [ListDisplaySettings(Title = "Код", Width = 80)]
            public string Key
            {
                get
                {
                    return _region.Code;
                }
            }

            [ListDisplaySettings(Title = "Наименование", Width = 1000)]
            public string Title
            {
                get
                {
                    return _region.Name;
                }
            }

            public Region Region
            {
                get
                {
                    return _region;
                }
            }

            public RegionLookupEntry(Region region)
            {
                _region = region;
            }
        }

        class RegionRequestResult : IDataRequestResult<RegionLookupEntry>
        {
            public IEnumerable<RegionLookupEntry> Data { get; set; }

            public bool ExceedsMaxQuantity { get; set; }
        }

        #endregion

        private readonly RegionsParameterModel _model;
        private readonly MultiSelect.Presenter<string, RegionLookupEntry> _presenter;

        private Region[] _goodSelection;

        public RegionControl(RegionsParameterModel model)
        {
            _model = model;

            InitializeComponent();

            _presenter = new MultiSelect.Presenter<string, RegionLookupEntry>(
                _lookup,
                Search)
            {
                SelectedItems = _model.Selection.ToDictionary(
                    region => region.Code,
                    region => new RegionLookupEntry(region))
            };
            _goodSelection = _presenter.SelectedItems.Values.Select(e => e.Region).ToArray();
            _lookup.OnSelectionChanged += sender =>
            {
                if (_presenter.SelectedItems.Count > 3)
                    _presenter.SetOnlySelection(_goodSelection.ToDictionary(
                        region => region.Code,
                        region => new RegionLookupEntry(region)));
                else
                    _goodSelection = _presenter.SelectedItems.Values.Select(e => e.Region).ToArray();
            };
            _lookup.OnSave += () => _model.SetSelection(_presenter.SelectedItems.Select(x => x.Value.Region).ToArray());
        }

        private IDataRequestResult<RegionLookupEntry> Search(IDataRequestArgs arg)
        {
            return new RegionRequestResult
            {
                Data = _model.Search(arg.SearchPattern).OrderBy(r => r.Code).Select(r => new RegionLookupEntry(r)).ToArray()
            };
        }

        public FilterParameterModelBase Model
        {
            get
            {
                return _model;
            }
        }

        public UserControl AsControl() { return this; }
        public event EventHandler HeightChanged;

        public int GetHeight()
        {
            return Height;
        }

        public void Init() {  }
    }
}
