﻿using System.Drawing;
using DocumentFormat.OpenXml.Office2010.Excel;
using Infragistics.Win;
using Infragistics.Win.Misc;
using Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Model;
using System;
using System.Linq;
using System.Windows.Forms;
using Luxoft.NDS2.Client.UI.Controls.Sur;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Editors
{
    public partial class SurControl : UserControl, IFilterParameterView
    {
        private readonly ListParameterModel _model;

        private readonly Padding _checkPadding = new Padding(8, 0, 56, 0);

        public SurControl(ListParameterModel model, DictionarySur sur)
        {
            _model = model;
            InitializeComponent();

            foreach (var option in _model.Options.Reverse())
            {
                var checkBox = new CheckBox
                {
                    Text = "",
                    Width = 56,
                    Padding = _checkPadding,
                    Tag = option.Key,
                    Dock = DockStyle.Left,
                    Checked = _model.Selection.Any(x => x.Key == option.Key)
                };
                Controls.Add(checkBox);
                var label = new UltraLabel
                {
                    Text = option.Value,
                    Appearance = {TextVAlign = VAlign.Middle},
                    AutoSize = true,
                    Dock = DockStyle.Left,
                };
                Controls.Add(label);
                int code;
                var icon = new SurIcon
                {
                    BackColor = System.Drawing.Color.Transparent,
                    MaximumSize = new Size(20, 20),
                    MinimumSize = new Size(20, 20),
                    Size = new Size(20, 20),
                    Dock = DockStyle.Left
                };
                icon.SetSurDictionary(sur);
                icon.Code = int.TryParse(option.Key, out code) ? code : (int?) null;
                Controls.Add(icon);
                checkBox.CheckedChanged += UpdateModel;
            }
        }

        private void UpdateModel(object sender, EventArgs eventArgs)
        {
            var checkBox = sender as CheckBox;
            if (checkBox != null)
                _model.ToggleSelection(checkBox.Tag as string, checkBox.Checked);
        }

        public FilterParameterModelBase Model
        {
            get
            {
                return _model;
            }
        }
        
        public UserControl AsControl() { return this; }
        public event EventHandler HeightChanged;
        public int GetHeight() { return Height; }
        public void Init() {  }
    }
}
