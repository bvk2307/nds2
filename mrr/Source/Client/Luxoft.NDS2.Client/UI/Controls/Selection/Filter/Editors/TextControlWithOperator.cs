﻿using System;
using System.Linq;
using System.Windows.Forms;
using Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Model;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Editors
{
    public partial class TextControlWithOperator : UserControl, IFilterParameterView
    {
        private readonly ListParameterModelBase<string> _model;

        public TextControlWithOperator(ListParameterModelBase<string> model)
        {
            _model = model;
            InitializeComponent();

            _comboOperator.DataSource = _model.Operators;
            _comboOperator.ValueMember = "Key";
            _comboOperator.DisplayMember = "Value";
            _comboOperator.Value = _model.Operators.Any(x => x.Key == _model.CompareMethod)
                ? _model.Operators.First(x => x.Key == _model.CompareMethod).Key
                : _model.Operators.First().Key;
            _comboOperator.ValueChanged += (sender, args) =>
            {
                if (_comboOperator.Value is ComparisonOperator)
                    _model.CompareMethod = (ComparisonOperator) _comboOperator.Value;
            };

            _text.Text = string.Join(",", _model.Selection);
            // DN LostFocus срабатывает на получении фокуса, GotFocus не срабатывает вообще
            _text.ValueChanged +=
                (sender, args) => _model.SetSelection(_text.Text.Split(',').Select(s => s.Trim()).ToArray());
        }

        public FilterParameterModelBase Model
        {
            get
            {
                return _model;
            }
        }

        public UserControl AsControl() { return this; }
        public event EventHandler HeightChanged;

        public int GetHeight()
        {
            return Height;
        }

        public void Init() {  }
    }
}
