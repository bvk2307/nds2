﻿using System.Globalization;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace.Enums;
using Luxoft.NDS2.Client.UI.Selections.SelectionCard.Helpers;
using Luxoft.NDS2.Common.Contracts.Helpers;
using System;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Model
{
    public class DiscrepancyAmountModel : FilterParameterModelBase
    {
        public DiscrepancyAmountModel(Parameter param, EditorParameter meta)
            : base(param, meta)
        {
        }

        public override bool IsValid
        {
            get
            {
                return true;
            }
        }

        public decimal? ValueFrom
        {
            get { return _valueFrom; }
            set
            {
                if (value == _valueFrom)
                    return; 
                _valueFrom = value;
                RaiseChanged();
            }
        }

        public decimal? ValueTo
        {
            get { return _valueTo; }
            set
            {
                if (value == _valueTo)
                    return;
                _valueTo = value;
                RaiseChanged();
            }
        }

        public DiscrepancyType TypeOfiscrepancy
        {
            get { return _typeOfDiscrepancy; }
            set
            {
                if (value == _typeOfDiscrepancy)
                    return;
                _typeOfDiscrepancy = value;
               
                RaiseChanged();
            }
        }

        protected override ParameterValue[] BuildValueList()
        {
            return new ParameterValue[] { };
        }

        public ParameterValue GetValue()
        {
            string desc;
            return new ParameterValue
            {
                Value = ((int)TypeOfiscrepancy).ToString(CultureInfo.InvariantCulture),
                ValueFrom = ValueToString(ValueFrom),
                ValueTo = ValueToString(ValueTo),
                ValueDescription = _typeDic.TryGetValue(_typeOfDiscrepancy, out desc) ? desc : string.Empty
            };
        }

        protected string ValueToString(decimal? value)
        {
            return value == null ? null : ((decimal)value).ToString("0.00", CultureInfo.InvariantCulture);
        }

        public override Parameter ToDto()
        {
            return new Parameter
            {
                AttributeId = Id,
                IsEnabled = IsEnabled,
                Operator = ComparisonOperator.Beetween,
                Values = BuildValueList()
            };
        }

        private decimal? _valueFrom;

        private decimal? _valueTo;

        private DiscrepancyType _typeOfDiscrepancy = DiscrepancyType.Break;

        private readonly Dictionary<DiscrepancyType, string> _typeDic = new Dictionary<DiscrepancyType, string>
        {
            {DiscrepancyType.Break, "Разрыв"},
            {DiscrepancyType.NDS, "НДС" }
        };
    }

    public class MultiDiscrepancyTypeParameterModel : FilterParameterModelBase, IDiscrepancyTypeSource
    {
        private readonly List<DiscrepancyAmountModel> _ranges;

        public MultiDiscrepancyTypeParameterModel(Parameter param, EditorParameter meta)
            : base(param, meta)
        {
            _ranges = new List<DiscrepancyAmountModel>();
            if (!param.Values.Any())
                AddRange();
        }

        protected override ParameterValue[] BuildValueList()
        {
            return _ranges.Select(r => r.GetValue()).ToArray();
        }

        public override Parameter ToDto()
        {
            return new Parameter
            {
                AttributeId = Id,
                IsEnabled = IsEnabled,
                Operator = ComparisonOperator.GE,
                Values = BuildValueList()
            };
        }

        public void Clear()
        {
            _ranges.Clear();
            RaiseChanged();
        }

        public void AddRange(DiscrepancyAmountModel range = null)
        {
            if (range == null)
            {
                range = new DiscrepancyAmountModel(new Parameter(), EditorParameter);
            }
            range.Changed += (sender, args) => RaiseChanged();
            _ranges.Add(range);
            RaiseChanged();
        }

        public void DeleteRange(DiscrepancyAmountModel range)
        {
            _ranges.Remove(range);
            RaiseChanged();
        }

        private KeyValuePair<string, string>[] _selection = { };
        public KeyValuePair<string, string>[] Selection
        {
            get
            {
                return _ranges.Select(x => new KeyValuePair<string, string>(((int)x.TypeOfiscrepancy).ToString(CultureInfo.InvariantCulture),
                    x.TypeOfiscrepancy == DiscrepancyType.Break ? "Разрыв" : "НДС")).ToArray();
            }
        }

        public DiscrepancyAmountModel[] Ranges
        {
            get
            {
                return _ranges.ToArray();
            }
        }

        public override bool IsValid
        {
            get
            {
                return _ranges.All(r => r.IsValid);
            }
        }
    }
}
