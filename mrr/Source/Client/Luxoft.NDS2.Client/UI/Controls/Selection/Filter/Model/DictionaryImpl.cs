﻿using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Model
{
    public class RegionDictionary : IDictionary<Region>
    {
        private readonly List<Region> _regions;

        public RegionDictionary(IEnumerable<Region> regions)
        {
            _regions = new List<Region>(regions);
        }

        public Region[] Search(string pattern)
        {
            return !string.IsNullOrEmpty(pattern)
                ? _regions.Where(r => r.Code.Contains(pattern)).ToArray()
                : _regions.ToArray();
        }

        public IEnumerator<Region> GetEnumerator()
        {
            return _regions.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

    public class TaxAuthorityDictionary : IDictionary<Inspection>
    {
        private readonly List<Inspection> _taxAuthorities;

        public TaxAuthorityDictionary(IEnumerable<Inspection> taxAuthorities)
        {
            _taxAuthorities = new List<Inspection>(taxAuthorities);
        }

        public Inspection[] Search(string pattern)
        {
            return !string.IsNullOrEmpty(pattern)
            ? _taxAuthorities.Where(ta => ta.Code.Contains(pattern)).ToArray()
            : _taxAuthorities.ToArray();
        }

        public IEnumerator<Inspection> GetEnumerator()
        {
            return _taxAuthorities.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
