﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Model;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Editors
{
    public partial class RangeOfIntEditor : UserControl
    {
        private readonly MultiRangeItemModel<long?> _model;

        public RangeOfIntEditor(MultiRangeItemModel<long?> model)
        {
            _model = model;
            InitializeComponent();

            _from.Value = _model.ValueFrom;

            _from.ValueChanged += (sender, args) =>
            {
                _model.ValueFrom = GetValue(_from.Value.ToString());
            };

            _removeButton.Click += (sender, args) => _model.Delete();
            _addButton.Click += (sender, args) => RaiseAdding();
        }

        private static long? GetValue(string value)
        {
            long l;
            return long.TryParse(value, out l) ? l : (long?)null;
        }

        public bool IsAllowRemove
        {
            get
            {
                return _removeButton.Visible;
            }
            set
            {
                _removeButton.Visible = value;
            }
        }

        public bool IsAllowAdd
        {
            get
            {
                return _addButton.Visible;
            }
            set
            {
                _addButton.Visible = value;
            }
        }

        public event EventHandler Adding;

        private void RaiseAdding()
        {
            var handler = Adding;
            if (handler != null)
                handler(this, null);
        }
    }
}
