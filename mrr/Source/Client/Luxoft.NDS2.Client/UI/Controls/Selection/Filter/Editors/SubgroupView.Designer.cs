﻿using System;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Editors
{
    partial class SubgroupView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._expandable = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this._table = new System.Windows.Forms.TableLayoutPanel();
            this._panel = new Panel();
            this._selector = new Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Editors.AttributeSelectorView();
            ((System.ComponentModel.ISupportInitialize)(this._expandable)).BeginInit();
            this._expandable.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            this._table.SuspendLayout();
            this._panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // _expandable
            // 
            this._expandable.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this._expandable.Dock = System.Windows.Forms.DockStyle.Fill;
            this._expandable.ExpandedSize = new System.Drawing.Size(554, 150);
            this._expandable.Location = new System.Drawing.Point(0, 0);
            this._expandable.Name = "_expandable";
            this._expandable.Size = new System.Drawing.Size(554, 150);
            this._expandable.TabIndex = 0;
            this._expandable.Text = "Title";
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this._table);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 19);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(548, 128);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // _table
            // 
            this._table.AutoSize = false;
            this._table.ColumnCount = 1;
            this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._table.Controls.Add(this._selector, 0, 1);
            this._table.Controls.Add(this._panel, 0, 0);
            this._table.Dock = System.Windows.Forms.DockStyle.Fill;
            this._table.Location = new System.Drawing.Point(0, 0);
            this._table.Name = "_table";
            this._table.RowCount = 2;
            this._table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 56F));
            this._table.Size = new System.Drawing.Size(548, 128);
            this._table.TabIndex = 4;
            // 
            // _panel
            // 
            this._panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._panel.Location = new System.Drawing.Point(3, 3);
            this._panel.Name = "_panel";
            this._panel.Size = new System.Drawing.Size(542, 66);
            this._panel.TabIndex = 1;
            // 
            // _selector
            // 
            this._selector.Dock = System.Windows.Forms.DockStyle.Fill;
            this._selector.Location = new System.Drawing.Point(3, 75);
            this._selector.Name = "_selector";
            this._selector.Size = new System.Drawing.Size(542, 50);
            this._selector.TabIndex = 0;
            this._selector.Applying += new System.EventHandler<Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Editors.EditorParameterEventArg>(this.AddAttribute);
            // 
            // SubgroupView
            // 
            this.Controls.Add(this._expandable);
            this.Margin = new System.Windows.Forms.Padding(3, 8, 3, 3);
            this.Name = "SubgroupView";
            this.Size = new System.Drawing.Size(554, 150);
            ((System.ComponentModel.ISupportInitialize)(this._expandable)).EndInit();
            this._expandable.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.PerformLayout();
            this._table.ResumeLayout(false);
            this._panel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraExpandableGroupBox _expandable;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private System.Windows.Forms.TableLayoutPanel _table;
        private Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Editors.AttributeSelectorView _selector;
        private System.Windows.Forms.Panel _panel;



    }
}
