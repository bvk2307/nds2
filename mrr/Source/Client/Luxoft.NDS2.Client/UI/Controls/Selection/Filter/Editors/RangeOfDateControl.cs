﻿using System.Drawing;
using Infragistics.Win.UltraWinEditors;
using Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Model;
using System;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Editors
{
    public partial class RangeOfDateControl : UserControl, IFilterParameterView
    {
        private readonly RangeFilterParameterModel<DateTime?> _model;

        public RangeOfDateControl(RangeFilterParameterModel<DateTime?> model)
        {
            _model = model;
            InitializeComponent();

            _from.Value = _model.ValueFrom;
            _to.Value = _model.ValueTo;

            CheckValue(_from);
            CheckValid();

            _from.AfterEnterEditMode += (sender, args) => _from.Appearance.ForeColor = Color.Black;
            _to.AfterEnterEditMode += (sender, args) => _to.Appearance.ForeColor = Color.Black;

            _from.AfterExitEditMode += (sender, args) => CheckValue(_from);
            _to.AfterExitEditMode += (sender, args) => CheckValid();

            _from.ValueChanged += (sender, args) =>
            {
                _model.ValueFrom = GetDate(_from);
                CheckValid();
            };
            _to.ValueChanged += (sender, args) =>
            {
                _model.ValueTo = GetDate(_to);
                CheckValid();
            };
        }

        private void CheckValid()
        {
            _to.Appearance.ForeColor = _model.IsValid ? (_to.Value == null ? Color.Gray : Color.Black) : Color.Red;
        }

        private static void CheckValue(UltraDateTimeEditor editor)
        {
            editor.Appearance.ForeColor = editor.Value == null
                ? Color.Gray
                : Color.Black;
        }

        private static DateTime? GetDate(UltraDateTimeEditor editor)
        {
            return editor.Value != null ? editor.DateTime : (DateTime?)null;
        }

        public FilterParameterModelBase Model
        {
            get
            {
                return _model;
            }
        }

        public UserControl AsControl() { return this; }

        public event EventHandler HeightChanged;

        public int GetHeight()
        {
            return Height;
        }

        public void Init() {  }
    }
}
