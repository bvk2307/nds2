﻿using System;
using Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Model;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Editors
{
    public partial class ReadonlyControl : UserControl, IFilterParameterView
    {
        private readonly FilterParameterReadonlyModel _model;

        public ReadonlyControl(FilterParameterReadonlyModel model)
        {
            _model = model;
            InitializeComponent();

            _text.Text = _model.Value;
        }

        public FilterParameterModelBase Model
        {
            get
            {
                return _model;
            }
        }

        public UserControl AsControl() { return this; }
        public event EventHandler HeightChanged;

        public int GetHeight()
        {
            return Height;
        }

        public void Init() {  }
    }
}
