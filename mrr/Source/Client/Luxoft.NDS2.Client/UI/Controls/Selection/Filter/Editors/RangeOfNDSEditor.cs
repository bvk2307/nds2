﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Model;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace.Enums;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Editors
{
    public partial class RangeOfNDSEditor : UserControl
    {
        private readonly RangeNDSFilterParameterModel _model;

        private KeyValuePair<DeclarationType, string>[] _operators;
        public RangeOfNDSEditor(RangeNDSFilterParameterModel model)
        {
            _model = model;
            InitializeComponent();

            _operators = GetOperators(model.EditorParameter.Area);

            _comboOperator.DataSource = _operators;
            _comboOperator.ValueMember = "Key";
            _comboOperator.DisplayMember = "Value";

            InitFromModel();

            _model.Changed += (sender, args) =>
            {
                _from.Value = _model.ValueFrom;
                SelectedType = _model.TypeOfDeclaration;
            };

            var keys = new[] { Keys.Delete, Keys.Back, Keys.Enter, Keys.Escape, Keys.Up, Keys.Down };

            _comboOperator.KeyDown += (sender, args) =>
            {
                if (!keys.Contains(args.KeyData))
                    args.SuppressKeyPress = true;
            };

            _comboOperator.ValueChanged += (sender, args) =>
            {
                _model.TypeOfDeclaration = SelectedType;

                _from.Enabled = SelectedType != DeclarationType.Zero;
            };
        }

        private DeclarationType SelectedType
        {
            get
            {
                if (_comboOperator.Value is DeclarationType)
                    return (DeclarationType)_comboOperator.Value;
                return DeclarationType.Empty;
            }
            set
            {
                _comboOperator.Value = value == DeclarationType.Empty ? (object)null : value;
            }
        }

        private static KeyValuePair<DeclarationType, string>[] GetOperators(EditorArea side)
        {
            return side == EditorArea.BuyerSide
                ? new[]
                {
                    new KeyValuePair<DeclarationType, string>(DeclarationType.ToPay, "К уплате"),
                    new KeyValuePair<DeclarationType, string>(DeclarationType.ToCharge, "К возмещению")
                }
                : new[]
                {
                    new KeyValuePair<DeclarationType, string>(DeclarationType.ToPay, "К уплате"),
                    new KeyValuePair<DeclarationType, string>(DeclarationType.ToCharge, "К возмещению"),
                    new KeyValuePair<DeclarationType, string>(DeclarationType.Zero, "Нулевая")
                };
        }

        #region Init

        private void InitFromModel()
        {
            _from.Value = _model.ValueFrom;
            SelectedType = _model.TypeOfDeclaration;

            _from.ValueChanged += (sender, args) =>
            {
                _model.ValueFrom = GetValue(_from.Value);
            };

            _removeButton.Click += (sender, args) => _model.Delete();
            _addButton.Click += (sender, args) => RaiseAdding();
        }

        #endregion

        private static decimal? GetValue(object value)
        {
            decimal l;
            return value != null && decimal.TryParse(value.ToString(), out l) ? l : (decimal?)null;
        }

        public bool IsAllowRemove
        {
            get
            {
                return _removeButton.Visible;
            }
            set
            {
                _removeButton.Visible = value;
            }
        }

        public bool IsAllowAdd
        {
            get
            {
                return _addButton.Visible;
            }
            set
            {
                _addButton.Visible = value;
            }
        }

        public event EventHandler Adding;

        private void RaiseAdding()
        {
            var handler = Adding;
            if (handler != null)
                handler(this, null);
        }
    }
}
