﻿namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Editors
{
    partial class EditorBaseView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolTip.UltraToolTipInfo ultraToolTipInfo1 = new Infragistics.Win.UltraWinToolTip.UltraToolTipInfo("Удалить атрибут", Infragistics.Win.ToolTipImage.Default, null, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolTip.UltraToolTipManager ultraToolTipManager1;
            this._table = new System.Windows.Forms.TableLayoutPanel();
            this._removeButton = new Infragistics.Win.Misc.UltraButton();
            this._nameLabel = new Infragistics.Win.Misc.UltraLabel();
            ultraToolTipManager1 = new Infragistics.Win.UltraWinToolTip.UltraToolTipManager(this.components);
            this._table.SuspendLayout();
            this.SuspendLayout();
            // 
            // _table
            // 
            this._table.AutoSize = true;
            this._table.ColumnCount = 3;
            this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28.57143F));
            this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 71.42857F));
            this._table.Controls.Add(this._removeButton, 0, 0);
            this._table.Controls.Add(this._nameLabel, 1, 0);
            this._table.Dock = System.Windows.Forms.DockStyle.Fill;
            this._table.Location = new System.Drawing.Point(0, 0);
            this._table.Name = "_table";
            this._table.RowCount = 1;
            this._table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._table.Size = new System.Drawing.Size(622, 30);
            this._table.TabIndex = 2;
            // 
            // _removeButton
            // 
            appearance2.Image = global::Luxoft.NDS2.Client.Properties.Resources.delete;
            appearance2.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance2.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this._removeButton.Appearance = appearance2;
            this._removeButton.Dock = System.Windows.Forms.DockStyle.Top;
            this._removeButton.Location = new System.Drawing.Point(3, 3);
            this._removeButton.Name = "_removeButton";
            this._removeButton.Size = new System.Drawing.Size(24, 24);
            this._removeButton.TabIndex = 2;
            ultraToolTipInfo1.ToolTipText = "Удалить атрибут";
            ultraToolTipManager1.SetUltraToolTip(this._removeButton, ultraToolTipInfo1);
            this._removeButton.Click += new System.EventHandler(this.DeleteClick);
            // 
            // _nameLabel
            // 
            appearance1.TextVAlignAsString = "Middle";
            this._nameLabel.Appearance = appearance1;
            this._nameLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this._nameLabel.Location = new System.Drawing.Point(33, 3);
            this._nameLabel.Name = "_nameLabel";
            this._nameLabel.Size = new System.Drawing.Size(163, 24);
            this._nameLabel.TabIndex = 3;
            this._nameLabel.Text = "AttributeName";
            // 
            // ultraToolTipManager1
            // 
            ultraToolTipManager1.ContainingControl = this;
            // 
            // EditorBaseView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._table);
            this.Name = "EditorBaseView";
            this.Size = new System.Drawing.Size(622, 30);
            this._table.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected System.Windows.Forms.TableLayoutPanel _table;
        private Infragistics.Win.Misc.UltraButton _removeButton;
        protected Infragistics.Win.Misc.UltraLabel _nameLabel;
    }
}
