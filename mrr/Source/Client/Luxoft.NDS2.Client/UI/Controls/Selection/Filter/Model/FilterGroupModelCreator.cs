﻿using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Model
{
    public static class FilterGroupModelCreator
    {
        public static FilterGroupModel Create(ParameterGroup group, IFilterParameterCreator paramCreator, bool isNew)
        {
            if (isNew)
                return new NewFilterGroupModel(group, paramCreator);
            else
            {
                var model = new FilterGroupModel(group, paramCreator);
                model.InitializeParameters();
                return model;
            }
        }
    }
}
