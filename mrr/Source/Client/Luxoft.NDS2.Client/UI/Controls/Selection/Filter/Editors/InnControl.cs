﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Model;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Editors
{
    public partial class InnControl : UserControl, IFilterParameterView
    {

        private readonly InnFilterModel _model;
        private class KeyComparer : IComparer<string>
        {
            public int Compare(string x, string y)
            {
                return int.Parse(x) - int.Parse(y);
            }
        }
        public InnControl(InnFilterModel model)
        {
            _model = model;
            InitializeComponent();

            _comboOperator.DataSource = _model.Operators;
            _comboOperator.ValueMember = "Key";
            _comboOperator.DisplayMember = "Value";
            _comboOperator.Value = _model.Operators.Any(x => x.Key == _model.CompareMethod)
                ? _model.Operators.First(x => x.Key == _model.CompareMethod).Key
                : _model.Operators.First().Key;
            _comboOperator.ValueChanged += (sender, args) =>
            {
                if (_comboOperator.Value is ComparisonOperator)
                    _model.CompareMethod = (ComparisonOperator)_comboOperator.Value;
                if (_model.CompareMethod == ComparisonOperator.NotDefinedOperator)
                {
                    tableLayoutPanel3.Visible = true;
                    tableLayoutPanel2.Visible = false;
                }
                else
                {
                    tableLayoutPanel3.Visible = false;
                    tableLayoutPanel2.Visible = true;
                }
            };

            _combo.DataSource = _model.Options;
            _combo.ValueMember = "Key";
            _combo.DisplayMember = "Value";
            _combo.Value = _model.Selection.Any() ? _model.Selection.First().Key : String.Empty;


            _combo.ValueChanged += (sender, args) =>
            {
                var key = _combo.Value as string;
                if (!string.IsNullOrEmpty(key))
                    _model.SetSelection(_model.Options.Where(x => x.Key == key).ToArray());
            };

            _text.ValueChanged +=
                (sender, args) => _model.SetSelection(_text.Text.Split(',').Select(s => new KeyValuePair<string, string>(s.Trim(), s.Trim())).ToArray());


            if (_model.CompareMethod == ComparisonOperator.NotDefinedOperator)
            {
                tableLayoutPanel3.Visible = true;
                tableLayoutPanel2.Visible = false;
                _model.SetSelection(_model.Options.Where(x => x.Key == (string)_combo.Value).ToArray());
            }
            else
            {
                tableLayoutPanel3.Visible = false;
                tableLayoutPanel2.Visible = true;
                _text.Text = string.Join(",", _model.Selection.Select(x => x.Value));
            }

        }

        public FilterParameterModelBase Model
        {
            get
            {
                return _model;
            }
        }

        public UserControl AsControl() { return this; }
        public event EventHandler HeightChanged;

        public int GetHeight()
        {
            return Height;
        }

        public void Init() {  }
    }
}
