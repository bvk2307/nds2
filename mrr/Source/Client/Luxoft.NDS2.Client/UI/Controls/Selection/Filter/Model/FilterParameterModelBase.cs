﻿using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using System;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Model
{
    public abstract class FilterParameterModelBase
    {
        public int Id { get; private set; }

        public abstract bool IsValid { get; }

        public string Name { get; private set; }

        public bool AllowDelete { get; set; }

        public bool IsEnabled { get; set; }

        public EditorParameter EditorParameter { get; set; }

        protected FilterParameterModelBase(Parameter parameter, EditorParameter meta)
        {
            Id = parameter.AttributeId;
            EditorParameter = meta;
            IsEnabled = parameter.IsEnabled;
        }

        public virtual Parameter ToDto()
        {
            return new Parameter
            {
                AttributeId = Id,
                IsEnabled = IsEnabled,
                Operator = ComparisonOperator.Equals,
                Values = BuildValueList()
            };
        }

        protected abstract ParameterValue[] BuildValueList();

        public event EventHandler Changed;

        protected void RaiseChanged()
        {
            var handler = Changed;
            if (handler != null)
                handler(this, null);
        }

        public event EventHandler Deleting;

        public void Delete()
        {
            var handler = Deleting;
            if (handler != null)
                handler(this, null);
        }
    }

    public class FilterParameterModelBaseEventArgs : EventArgs
    {
        public FilterParameterModelBase Model { get; set; }
    }
}
