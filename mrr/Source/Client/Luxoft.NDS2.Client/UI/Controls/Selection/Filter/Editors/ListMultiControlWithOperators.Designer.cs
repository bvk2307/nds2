﻿namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Editors
{
    partial class ListMultiControlWithOperators
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this._comboOperator = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this._combo = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._comboOperator)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._combo)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel1.Controls.Add(this._comboOperator, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this._combo, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(799, 32);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // _comboOperator
            // 
            this._comboOperator.Dock = System.Windows.Forms.DockStyle.Fill;
            this._comboOperator.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this._comboOperator.Location = new System.Drawing.Point(0, 0);
            this._comboOperator.Margin = new System.Windows.Forms.Padding(0);
            this._comboOperator.Name = "_comboOperator";
            this._comboOperator.Size = new System.Drawing.Size(239, 21);
            this._comboOperator.TabIndex = 2;
            // 
            // _combo
            // 
            this._combo.CheckedListSettings.CheckBoxStyle = Infragistics.Win.CheckStyle.CheckBox;
            this._combo.CheckedListSettings.EditorValueSource = Infragistics.Win.EditorWithComboValueSource.CheckedItems;
            this._combo.Dock = System.Windows.Forms.DockStyle.Fill;
            this._combo.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this._combo.Location = new System.Drawing.Point(239, 0);
            this._combo.Margin = new System.Windows.Forms.Padding(0);
            this._combo.MaxDropDownItems = 15;
            this._combo.Name = "_combo";
            this._combo.Size = new System.Drawing.Size(560, 21);
            this._combo.TabIndex = 1;
            // 
            // ListMultiControlWithOperators
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "ListMultiControlWithOperators";
            this.Size = new System.Drawing.Size(799, 32);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._comboOperator)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._combo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor _combo;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor _comboOperator;

    }
}
