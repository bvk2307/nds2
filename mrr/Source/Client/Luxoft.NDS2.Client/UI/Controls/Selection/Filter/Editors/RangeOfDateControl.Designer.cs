﻿namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Editors
{
    partial class RangeOfDateControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            this._table = new System.Windows.Forms.TableLayoutPanel();
            this._to = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this._label = new Infragistics.Win.Misc.UltraLabel();
            this._from = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this._table.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._to)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._from)).BeginInit();
            this.SuspendLayout();
            // 
            // _table
            // 
            this._table.ColumnCount = 3;
            this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._table.Controls.Add(this._to, 2, 0);
            this._table.Controls.Add(this._label, 1, 0);
            this._table.Controls.Add(this._from, 0, 0);
            this._table.Dock = System.Windows.Forms.DockStyle.Fill;
            this._table.Location = new System.Drawing.Point(0, 0);
            this._table.Name = "_table";
            this._table.RowCount = 1;
            this._table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._table.Size = new System.Drawing.Size(794, 32);
            this._table.TabIndex = 1;
            // 
            // _to
            // 
            appearance4.TextHAlignAsString = "Right";
            this._to.Appearance = appearance4;
            appearance5.Image = global::Luxoft.NDS2.Client.Properties.Resources.VisibleBtn;
            this._to.ButtonAppearance = appearance5;
            this._to.Dock = System.Windows.Forms.DockStyle.Fill;
            this._to.FormatString = "";
            this._to.Location = new System.Drawing.Point(407, 0);
            this._to.Margin = new System.Windows.Forms.Padding(0);
            this._to.MaskInput = "{LOC}dd.mm.yyyy";
            this._to.Name = "_to";
            this._to.NullText = "ДД.ММ.ГГГГ";
            this._to.PromptChar = ' ';
            this._to.Size = new System.Drawing.Size(387, 21);
            this._to.TabIndex = 7;
            // 
            // _label
            // 
            appearance2.TextHAlignAsString = "Center";
            appearance2.TextVAlignAsString = "Middle";
            this._label.Appearance = appearance2;
            this._label.Location = new System.Drawing.Point(390, 3);
            this._label.Name = "_label";
            this._label.Size = new System.Drawing.Size(14, 23);
            this._label.TabIndex = 2;
            this._label.Text = "—";
            // 
            // _from
            // 
            appearance1.TextHAlignAsString = "Right";
            this._from.Appearance = appearance1;
            appearance3.Image = global::Luxoft.NDS2.Client.Properties.Resources.VisibleBtn;
            this._from.ButtonAppearance = appearance3;
            this._from.Dock = System.Windows.Forms.DockStyle.Fill;
            this._from.Location = new System.Drawing.Point(0, 0);
            this._from.Margin = new System.Windows.Forms.Padding(0);
            this._from.MaskInput = "{LOC}dd.mm.yyyy";
            this._from.Name = "_from";
            this._from.NullText = "ДД.ММ.ГГГГ";
            this._from.PromptChar = ' ';
            this._from.Size = new System.Drawing.Size(387, 21);
            this._from.TabIndex = 6;
            // 
            // RangeOfDateControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._table);
            this.MinimumSize = new System.Drawing.Size(200, 32);
            this.Name = "RangeOfDateControl";
            this.Size = new System.Drawing.Size(794, 32);
            this._table.ResumeLayout(false);
            this._table.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._to)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._from)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel _table;
        private Infragistics.Win.Misc.UltraLabel _label;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor _from;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor _to;
    }
}
