﻿using System;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Model
{
    interface IRegionBound
    {
        Region[] Regions { get; }

        event EventHandler RegionsChanged;
    }

    interface IRegionDepender
    {
        void UpdateRegions(Region[] regions);
    }

    interface IDiscrepancyTypeSource
    {
        KeyValuePair<string, string>[] Selection { get; }
    }

    interface IDiscrepancyTypeDepender
    {
        void UpdateOptions(KeyValuePair<string, string>[] selectedTypes);
    }

    public interface IDictionary<T> : IEnumerable<T>
    {
        T[] Search(string pattern);
    }
}
