﻿namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Editors
{
    partial class DiscrepancyAmountEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.UltraWinToolTip.UltraToolTipManager ultraToolTipManager1;
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolTip.UltraToolTipInfo ultraToolTipInfo2 = new Infragistics.Win.UltraWinToolTip.UltraToolTipInfo("Добавить", Infragistics.Win.ToolTipImage.Default, null, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DiscrepancyAmountEditor));
            Infragistics.Win.UltraWinToolTip.UltraToolTipInfo ultraToolTipInfo1 = new Infragistics.Win.UltraWinToolTip.UltraToolTipInfo("Удалить", Infragistics.Win.ToolTipImage.Default, null, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            this._addButton = new Infragistics.Win.Misc.UltraButton();
            this._removeButton = new Infragistics.Win.Misc.UltraButton();
            this._comboOperator = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this._from = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this._table = new System.Windows.Forms.TableLayoutPanel();
            this._to = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            ultraToolTipManager1 = new Infragistics.Win.UltraWinToolTip.UltraToolTipManager(this.components);
            ((System.ComponentModel.ISupportInitialize)(this._comboOperator)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._from)).BeginInit();
            this._table.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._to)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraToolTipManager1
            // 
            ultraToolTipManager1.ContainingControl = this;
            // 
            // _addButton
            // 
            appearance3.Image = global::Luxoft.NDS2.Client.Properties.Resources.list_add;
            appearance3.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance3.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this._addButton.Appearance = appearance3;
            this._addButton.Dock = System.Windows.Forms.DockStyle.Top;
            this._addButton.Location = new System.Drawing.Point(763, 0);
            this._addButton.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this._addButton.Name = "_addButton";
            this._addButton.Size = new System.Drawing.Size(25, 24);
            this._addButton.TabIndex = 4;
            ultraToolTipInfo2.ToolTipText = "Добавить";
            ultraToolTipManager1.SetUltraToolTip(this._addButton, ultraToolTipInfo2);
            // 
            // _removeButton
            // 
            appearance1.Image = ((object)(resources.GetObject("appearance1.Image")));
            appearance1.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance1.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this._removeButton.Appearance = appearance1;
            this._removeButton.Dock = System.Windows.Forms.DockStyle.Top;
            this._removeButton.Location = new System.Drawing.Point(733, 0);
            this._removeButton.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this._removeButton.Name = "_removeButton";
            this._removeButton.Size = new System.Drawing.Size(24, 24);
            this._removeButton.TabIndex = 5;
            ultraToolTipInfo1.ToolTipText = "Удалить";
            ultraToolTipManager1.SetUltraToolTip(this._removeButton, ultraToolTipInfo1);
            // 
            // _comboOperator
            // 
            this._comboOperator.Dock = System.Windows.Forms.DockStyle.Fill;
            this._comboOperator.Location = new System.Drawing.Point(0, 0);
            this._comboOperator.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._comboOperator.Name = "_comboOperator";
            this._comboOperator.Size = new System.Drawing.Size(67, 21);
            this._comboOperator.TabIndex = 6;
            // 
            // _from
            // 
            this._from.Dock = System.Windows.Forms.DockStyle.Fill;
            this._from.FormatString = "N";
            this._from.Location = new System.Drawing.Point(220, 0);
            this._from.Margin = new System.Windows.Forms.Padding(0);
            this._from.MaskInput = "{double:-22.2}";
            this._from.MaxValue = new decimal(new int[] {
            -159383553,
            46653770,
            5421,
            0});
            this._from.MinValue = 0;
            this._from.Name = "_from";
            this._from.Nullable = true;
            this._from.NullText = "0";
            this._from.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Decimal;
            this._from.PromptChar = ' ';
            this._from.Size = new System.Drawing.Size(180, 21);
            this._from.TabIndex = 7;
            // 
            // _table
            // 
            this._table.ColumnCount = 8;
            this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180F));
            this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180F));
            this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this._table.Controls.Add(this._to, 4, 0);
            this._table.Controls.Add(this._from, 2, 0);
            this._table.Controls.Add(this._comboOperator, 0, 0);
            this._table.Controls.Add(this._removeButton, 5, 0);
            this._table.Controls.Add(this._addButton, 6, 0);
            this._table.Controls.Add(this.ultraLabel1, 1, 0);
            this._table.Controls.Add(this.ultraLabel2, 3, 0);
            this._table.Dock = System.Windows.Forms.DockStyle.Fill;
            this._table.Location = new System.Drawing.Point(0, 0);
            this._table.Name = "_table";
            this._table.RowCount = 1;
            this._table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._table.Size = new System.Drawing.Size(992, 30);
            this._table.TabIndex = 0;
            // 
            // _to
            // 
            this._to.Dock = System.Windows.Forms.DockStyle.Fill;
            this._to.FormatString = "N";
            this._to.Location = new System.Drawing.Point(550, 0);
            this._to.Margin = new System.Windows.Forms.Padding(0);
            this._to.MaskInput = "{double:-22.2}";
            this._to.MaxValue = 1E+23D;
            this._to.MinValue = 0;
            this._to.Name = "_to";
            this._to.Nullable = true;
            this._to.NullText = "0";
            this._to.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Decimal;
            this._to.PromptChar = ' ';
            this._to.Size = new System.Drawing.Size(180, 21);
            this._to.TabIndex = 10;
            // 
            // ultraLabel1
            // 
            appearance2.TextHAlignAsString = "Left";
            this.ultraLabel1.Appearance = appearance2;
            this.ultraLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabel1.Location = new System.Drawing.Point(73, 3);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ultraLabel1.Size = new System.Drawing.Size(144, 24);
            this.ultraLabel1.TabIndex = 8;
            this.ultraLabel1.Text = "Общ. сумма расхождений";
            // 
            // ultraLabel2
            // 
            appearance4.TextHAlignAsString = "Left";
            this.ultraLabel2.Appearance = appearance4;
            this.ultraLabel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraLabel2.Location = new System.Drawing.Point(403, 3);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(144, 24);
            this.ultraLabel2.TabIndex = 9;
            this.ultraLabel2.Text = "Мин. сумма расхождения";
            // 
            // DiscrepancyAmountEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._table);
            this.MinimumSize = new System.Drawing.Size(200, 30);
            this.Name = "DiscrepancyAmountEditor";
            this.Size = new System.Drawing.Size(992, 30);
            ((System.ComponentModel.ISupportInitialize)(this._comboOperator)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._from)).EndInit();
            this._table.ResumeLayout(false);
            this._table.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._to)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraButton _addButton;
        private Infragistics.Win.Misc.UltraButton _removeButton;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor _comboOperator;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor _from;
        private System.Windows.Forms.TableLayoutPanel _table;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor _to;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
    }
}
