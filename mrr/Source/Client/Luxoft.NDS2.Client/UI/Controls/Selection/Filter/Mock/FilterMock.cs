﻿using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Mock
{
    public static class FilterMock
    {
        private static ParameterValue GetParameterValue(string value, string description = "")
        {
            return new ParameterValue
            {
                Value = value,
                ValueDescription = description
            };
        }

        private static ParameterValue GetParameterValueRange(string valueFrom, string valueTo, string description = "")
        {
            return new ParameterValue
            {
                ValueFrom = valueFrom,
                ValueTo = valueTo,
                ValueDescription = description
            };
        }

        public static Common.Contracts.DTO.SelectionFilterNamespace.Filter GetEmptyFilter()
        {
            return new Common.Contracts.DTO.SelectionFilterNamespace.Filter
            {
                Groups = new[]
                {
                    GetEmptyGroup()
                }
            };
        }

        public static ParameterGroup GetEmptyGroup()
        {
            return new ParameterGroup
            {
                IsEnabled = true,
                Parameters = new Parameter[] {}
            };
        }

        public static Common.Contracts.DTO.SelectionFilterNamespace.Filter GetFilter()
        {
            #region parameters

            var parameters = new[]
            {
                new Parameter
                {
                    AttributeId = 1,
                    IsEnabled = true,
                    Operator = ComparisonOperator.InList,
                    Values = new []
                    {
                        GetParameterValue("123456789001"),
                        GetParameterValue("123456789002"),
                        GetParameterValue("123456789003"),
                        GetParameterValue("123456789004"),
                        GetParameterValue("123456789005")
                    }
                },
                /*new Parameter
                {
                    AttributeId = 2,
                    IsEnabled = true,
                    Operator = ComparisonOperator.InList,
                    Values = new []
                    {
                        GetParameterValue("1", "СУР какой-то")
                    }
                },*/
                new Parameter
                {
                    AttributeId = 3,
                    IsEnabled = true,
                    Operator = ComparisonOperator.InList,
                    Values = new []
                    {
                        GetParameterValue("123456789001"),
                        GetParameterValue("123456789002"),
                        GetParameterValue("123456789003"),
                        GetParameterValue("123456789004"),
                        GetParameterValue("123456789005")
                    }
                },
                new Parameter
                {
                    AttributeId = 5,
                    IsEnabled = true,
                    Operator = ComparisonOperator.InList,
                    Values = new []
                    {
                        GetParameterValue("1", "Разрыв")
                    }
                },
            };

            #endregion

            var groups = new []
            {
                new ParameterGroup
                {
                    IsEnabled = true,
                    Name = "Unusable group name",
                    Parameters = parameters,
                }
            };
            
            return new Common.Contracts.DTO.SelectionFilterNamespace.Filter
            {
                Groups = groups
            };
        }
    }
}
