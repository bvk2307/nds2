﻿using Luxoft.NDS2.Client.Infrastructure;
using Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Model;
using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using Luxoft.NDS2.Common.Contracts.Services;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Mock
{
    class FilterDictionaryServiceAdapter : IFilterDictionaryService
    {
        private readonly ISelectionService _svc;

        private readonly INotifier _notifier;
        private readonly IClientLogger _logger;
        private readonly OptionsLoader _optionsLoader;



        public FilterDictionaryServiceAdapter(ISelectionService svc, INotifier notifier, IClientLogger logger)
        {
            _svc = svc;
            _notifier = notifier;
            _logger = logger;

            _optionsLoader = new OptionsLoader(_svc, _notifier, _logger);
        }

        #region nested loaders

        class RegionsLoader : ServiceProxyBase

        {
            private readonly ISelectionService _svc;

            public RegionsLoader(ISelectionService svc, INotifier notifier, IClientLogger logger)
                : base(notifier, logger)
            {
                _svc = svc;
            }

            public IEnumerable<Region> Load()
            {
                return Invoke(() => _svc.GetRegions(), 
                              () => new OperationResult<List<Region>>()).Result;
            }
        }

        class OptionsLoader : ServiceProxyBase<List<KeyValuePair<string, string>>>
        {
            private readonly ISelectionService _svc;

            public OptionsLoader(ISelectionService svc, INotifier notifier, IClientLogger logger)
                : base(notifier, logger)
            {
                _svc = svc;
            }

            public KeyValuePair<string, string>[] Load(int id)
            {
                List<KeyValuePair<string, string>> result;

                Invoke(() => _svc.GetFilterParameterOptions(id), out result);

                return (result != null)
                    ? result.ToArray()
                    : new KeyValuePair<string, string>[] { };
            }
        }

        class EditorParametersLoader : ServiceProxyBase<List<EditorParameter>>
        {
            private readonly ISelectionService _svc;
            private readonly SelectionType _type;

            public EditorParametersLoader(SelectionType type,
                ISelectionService svc, 
                INotifier notifier,
                IClientLogger logger) : base(notifier, logger)
            {
                _svc = svc;
                _type = type;
            }

            public EditorParameter[] Load()
            {
                List<EditorParameter> result;

                if (!Invoke(() => _svc.GetFilterEditorParameters(_type), out result))
                {
                    _notifier.ShowError(string.Format(
                        ResourceManagerNDS2.SelectionCard.SelectionFilterError,
                        ResourceManagerNDS2.SelectionCard.MetadataLoadingFault
                        ));
                }

                return (result != null)
                    ? result.ToArray()
                    : new EditorParameter[] { };
            }
        }

        #endregion

        public RegionDictionary GetRegions()
        {
            var loader = new RegionsLoader(_svc, _notifier, _logger);
            return new RegionDictionary(loader.Load());
        }

        public List<KeyValuePair<string, string>> GetWhiteLists()
        {
            return _svc.GetWhiteLists().Result;
        }

        public TaxAuthorityDictionary GetTaxAuthorities(RegionDictionary regions)
        {
            return regions.GetAuthorities();
        }
        
        public KeyValuePair<string, string>[] GetOptions(int id)
        {
            return _optionsLoader.Load(id);
        }

        public EditorParameter[] GetMetaData(SelectionType type)
        {
            return new EditorParametersLoader(type, _svc, _notifier, _logger).Load();
        }

    }


}
