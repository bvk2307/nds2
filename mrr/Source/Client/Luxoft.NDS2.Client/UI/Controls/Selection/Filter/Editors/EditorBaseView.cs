﻿using System.Drawing;
using System.Linq;
using System;
using System.Windows.Forms;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Editors
{
    public partial class EditorBaseView : UserControl
    {
        private readonly IFilterParameterView _control;

        private readonly EditorType[] _noTitle = {EditorType.UntitledCheckBoxEditor};

        protected EditorBaseView()
        {
            InitializeComponent();
        }

        public EditorBaseView(IFilterParameterView control)
        {
            InitializeComponent();

            _control = control;

            SetupVisual();
        }

        private void SetupVisual()
        {
            var control = _control.AsControl();
            control.Dock = DockStyle.Top;
            if (_noTitle.Contains(_control.Model.EditorParameter.ViewType))
            {
                _table.Controls.Remove(_nameLabel);
                _table.Controls.Add(control, 1, 0);
                _table.SetColumnSpan(control, 2);
            }
            else
            {
                _table.Controls.Add(control, 2, 0);
                _nameLabel.Text = _control.Model.EditorParameter.Name;
                _control.Model.Changed += (sender, args) =>
                {
                    _nameLabel.Appearance.ForeColor = _control.Model.IsValid ? Color.Black : Color.Red;
                };
            }
            _control.HeightChanged += (sender, args) =>
            {
                Height = _control.GetHeight() + 8;
                var handler = HeightChanged;
                if (handler != null)
                    handler(this, null);
            };
        }

        private void DeleteClick(object sender, EventArgs e)
        {
            _control.Model.Delete();
        }

        public int AttributeId
        {
            get
            {
                return _control.Model.Id;
            }
        }

        public event EventHandler HeightChanged;
    
    }
}
