﻿using Infragistics.Win;
using Infragistics.Win.Misc;
using Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Model;
using System;
using System.Linq;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Editors
{
    public partial class CheckBoxControl : UserControl, IFilterParameterView
    {
        private readonly ListParameterModel _model;

        private readonly Padding _margin = new Padding(8, 0, 32, 0);

        public CheckBoxControl(ListParameterModel model)
        {
            _model = model;
            InitializeComponent();

            foreach (var option in _model.Options.Reverse())
            {
                var checkBox = new CheckBox
                {
                    Text = "",
                    Width = 35,
                    Padding = _margin,
                    Tag = option.Key,
                    Dock = DockStyle.Left,
                    Checked = _model.Selection.Any(x => x.Key == option.Key)
                };
                Controls.Add(checkBox);
                var label = new UltraLabel
                {
                    Text = option.Value,
                    Appearance = {TextVAlign = VAlign.Middle},
                    AutoSize = true,
                    Dock = DockStyle.Left,
                };
                Controls.Add(label);
                checkBox.CheckedChanged += UpdateModel;
            }
        }

        private void UpdateModel(object sender, EventArgs eventArgs)
        {
            var checkBox = sender as CheckBox;
            if (checkBox != null)
                _model.ToggleSelection(checkBox.Tag as string, checkBox.Checked);
        }

        public FilterParameterModelBase Model
        {
            get
            {
                return _model;
            }
        }
        
        public UserControl AsControl() { return this; }
        public event EventHandler HeightChanged;
        public int GetHeight() { return Height; }
        public void Init() {  }
    }
}
