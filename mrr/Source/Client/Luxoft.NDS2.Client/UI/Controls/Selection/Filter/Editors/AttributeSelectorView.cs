﻿using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using Luxoft.NDS2.Common.Contracts.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Editors
{
    public partial class AttributeSelectorView : UserControl
    {
        public AttributeSelectorView()
        {
            InitializeComponent();
        }

        public void SetParameterList(IEnumerable<EditorParameter> list)
        {
            var src = list.ToArray();
            _selector.DataSource = src;
            _selector.ValueMember = TypeHelper<EditorParameter>.GetMemberName(x => x.Id);
            _selector.DisplayMember = TypeHelper<EditorParameter>.GetMemberName(x => x.Name);
            _selector.SelectedItem = null;
            _selector.SelectionChanged += (sender, args) =>
            {
                _applyButton.Enabled = _selector.SelectedItem != null;
            };

            if (src.Any())
            {
                _selector.Enabled = true;
                _applyButton.Enabled = false;
            }
            else
            {
                _selector.Enabled = false;
                _applyButton.Enabled = false;
            }
        }

        public event EventHandler<EditorParameterEventArg> Applying;

        private void ApplyClick(object sender, EventArgs e)
        {
            var handler = Applying;
            if (handler == null)
                return;
            var meta = _selector.SelectedItem.ListObject as EditorParameter;
            handler(this, new EditorParameterEventArg(meta));
        }
    }

    public class EditorParameterEventArg : EventArgs
    {
        public EditorParameter Meta { get; set; }

        public EditorParameterEventArg(EditorParameter meta)
        {
            Meta = meta;
        }
    }
}
