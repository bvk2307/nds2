﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Model;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace.Enums;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Editors
{
    public partial class DiscrepancyAmountEditor : UserControl
    {
        private readonly DiscrepancyAmountModel _model;

        private KeyValuePair<DiscrepancyType, string>[] _operators;
        public DiscrepancyAmountEditor(DiscrepancyAmountModel model)
        {
            _model = model;
            InitializeComponent();

            _operators = GetOperators(model.EditorParameter.Area);

            _comboOperator.DataSource = _operators;
            _comboOperator.ValueMember = "Key";
            _comboOperator.DisplayMember = "Value";

            InitFromModel();

            _model.Changed += (sender, args) =>
            {
                _from.Value = _model.ValueFrom;
                _to.Value = _model.ValueTo;
                SelectedType = _model.TypeOfiscrepancy;
            };

            var keys = new[] { Keys.Delete, Keys.Back, Keys.Enter, Keys.Escape, Keys.Up, Keys.Down };

            _comboOperator.KeyDown += (sender, args) =>
            {
                if (!keys.Contains(args.KeyData))
                    args.SuppressKeyPress = true;
            };

            _comboOperator.ValueChanged += (sender, args) =>
            {
                _model.TypeOfiscrepancy = SelectedType;
            };
        }

        private DiscrepancyType SelectedType
        {
            get
            {
                if (_comboOperator.Value is DiscrepancyType)
                    return (DiscrepancyType)_comboOperator.Value;
                return DiscrepancyType.Break;
            }
            set
            {
                _comboOperator.Value = value;
            }
        }

        private static KeyValuePair<DiscrepancyType, string>[] GetOperators(EditorArea side)
        {
            return new[]
            {
                new KeyValuePair<DiscrepancyType, string>(DiscrepancyType.Break, "Разрыв"),
                new KeyValuePair<DiscrepancyType, string>(DiscrepancyType.NDS, "НДС")
            };
        }

        #region Init

        private void InitFromModel()
        {
            _from.Value = _model.ValueFrom;
            _to.Value = _model.ValueTo;

            SelectedType = _model.TypeOfiscrepancy;

            _from.ValueChanged += (sender, args) =>
            {
                _model.ValueFrom = GetValue(_from.Value);
            };

            _to.ValueChanged += (sender, args) =>
            {
                _model.ValueTo = GetValue(_to.Value);
            };

            _removeButton.Click += (sender, args) => _model.Delete();
            _addButton.Click += (sender, args) => RaiseAdding();
        }

        #endregion

        private static decimal? GetValue(object value)
        {
            decimal l;
            return value != null && decimal.TryParse(value.ToString(), out l) ? l : (decimal?)null;
        }

        public bool IsAllowRemove
        {
            get
            {
                return _removeButton.Visible;
            }
            set
            {
                _removeButton.Visible = value;
            }
        }

        public bool IsAllowAdd
        {
            get
            {
                return _addButton.Visible;
            }
            set
            {
                _addButton.Visible = value;
            }
        }

        public bool IsAllowChangeType
        {
            get
            {
                return _comboOperator.Enabled;
            }
            set
            {
                _comboOperator.Enabled = value;
            }
        }
        public event EventHandler Adding;

        private void RaiseAdding()
        {
            var handler = Adding;
            if (handler != null)
                handler(this, null);
        }
    }
}
