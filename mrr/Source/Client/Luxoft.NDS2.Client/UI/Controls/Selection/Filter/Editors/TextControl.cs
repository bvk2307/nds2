﻿using System;
using Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Model;
using System.Linq;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Editors
{
    public partial class TextControl : UserControl, IFilterParameterView
    {
        private readonly ListParameterModelBase<string> _model;

        public TextControl(ListParameterModelBase<string> model)
        {
            _model = model;
            InitializeComponent();

            _text.Text = string.Join(";", _model.Selection);
            // DN LostFocus срабатывает на получении фокуса, GotFocus не срабатывает вообще
            _text.ValueChanged +=
                (sender, args) => _model.SetSelection(_text.Text.Split(';').Select(s => s.Trim()).ToArray());
        }

        public FilterParameterModelBase Model
        {
            get
            {
                return _model;
            }
        }

        public UserControl AsControl() { return this; }
        public event EventHandler HeightChanged;

        public int GetHeight()
        {
            return Height;
        }

        public void Init() {  }
    }
}
