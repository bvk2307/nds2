﻿using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Mock
{
    public static class MetaData
    {
        public static EditorParameter[] GetMetaData()
        {
            return new[]
            {
                #region сторона покупателя

                new EditorParameter
                {
                    Id = 101,
                    Area = EditorArea.BuyerSide,
                    ModelType = ParameterType.RegionsModel,
                    ViewType = EditorType.RegionEditor,
                    Name = "Регион"
                },
                new EditorParameter
                {
                    Id = 102,
                    Area = EditorArea.BuyerSide,
                    ModelType = ParameterType.TaxAuthorityModel,
                    ViewType = EditorType.TaxAuthorityEditor,
                    Name = "Инспекция"
                },
                new EditorParameter
                {
                    Id = 103,
                    Area = EditorArea.BuyerSide,
                    ModelType = ParameterType.ListOfStringModel,
                    ViewType = EditorType.InnEditor,
                    Name = "ИНН"
                },
                new EditorParameter
                {
                    Id = 104,
                    Area = EditorArea.BuyerSide,
                    ModelType = ParameterType.ListModel,
                    ViewType = EditorType.SurEditor,
                    Name = "Значение СУР"
                },
                new EditorParameter
                {
                    Id = 105,
                    Area = EditorArea.BuyerSide,
                    ModelType = ParameterType.ListModel,
                    ViewType = EditorType.UntitledCheckBoxEditor,
                    Name = "Признак \"Крупнейший\""
                },
                new EditorParameter
                {
                    Id = 106,
                    Area = EditorArea.BuyerSide,
                    ModelType = ParameterType.ListModel,
                    ViewType = EditorType.ListMultiEditor,
                    Name = "Отчетный период"
                },
                new EditorParameter
                {
                    Id = 107,
                    Area = EditorArea.BuyerSide,
                    ModelType = ParameterType.RangeOfDateModel,
                    ViewType = EditorType.DataRangeEditor,
                    Name = "Дата представления в НО"
                },
                new EditorParameter
                {
                    Id = 108,
                    Area = EditorArea.BuyerSide,
                    ModelType = ParameterType.MultiRangeOfIntModel,
                    ViewType = EditorType.IntRangeEditor,
                    Name = "Признак НД — К уплате"
                },
                new EditorParameter
                {
                    Id = 109,
                    Area = EditorArea.BuyerSide,
                    ModelType = ParameterType.MultiRangeOfIntModel,
                    ViewType = EditorType.IntRangeEditor,
                    Name = "Признак НД — К возмещению"
                },
                new EditorParameter
                {
                    Id = 110,
                    Area = EditorArea.BuyerSide,
                    ModelType = ParameterType.MultiRangeOfIntModel,
                    ViewType = EditorType.IntRangeEditor,
                    Name = "Общая сумма расхождений"
                },
                new EditorParameter
                {
                    Id = 111,
                    Area = EditorArea.BuyerSide,
                    ModelType = ParameterType.MultiRangeOfIntModel,
                    ViewType = EditorType.IntRangeEditor,
                    Name = "Общая сумма ПВП расхождений"
                },
                new EditorParameter
                {
                    Id = 112,
                    Area = EditorArea.BuyerSide,
                    ModelType = ParameterType.MultiRangeOfIntModel,
                    ViewType = EditorType.IntRangeEditor,
                    Name = "Минимальная сумма расхождения"
                },
                new EditorParameter
                {
                    Id = 113,
                    Area = EditorArea.BuyerSide,
                    ModelType = ParameterType.MultiRangeOfIntModel,
                    ViewType = EditorType.IntRangeEditor,
                    Name = "Максимальная сумма расхождения"
                },
                new EditorParameter
                {
                    Id = 114,
                    Area = EditorArea.BuyerSide,
                    ModelType = ParameterType.MultiRangeOfIntModel,
                    ViewType = EditorType.IntRangeEditor,
                    Name = "Минимальное значение ПВП расхождения"
                },
                new EditorParameter
                {
                    Id = 115,
                    Area = EditorArea.BuyerSide,
                    ModelType = ParameterType.MultiRangeOfIntModel,
                    ViewType = EditorType.IntRangeEditor,
                    Name = "Максимальное значение ПВП расхождения"
                },
                new EditorParameter
                {
                    Id = 116,
                    Area = EditorArea.BuyerSide,
                    ModelType = ParameterType.MultiRangeOfIntModel,
                    ViewType = EditorType.IntRangeEditor,
                    Name = "Сумма расхождений по книге покупок"
                },
                new EditorParameter
                {
                    Id = 117,
                    Area = EditorArea.BuyerSide,
                    ModelType = ParameterType.MultiRangeOfIntModel,
                    ViewType = EditorType.IntRangeEditor,
                    Name = "Сумма расхождений по книге продаж"
                },
                new EditorParameter
                {
                    Id = 118,
                    Area = EditorArea.BuyerSide,
                    ModelType = ParameterType.MultiRangeOfIntModel,
                    ViewType = EditorType.IntRangeEditor,
                    Name = "Общее количество расхождений"
                },

                #endregion

                #region сторона продавца

                new EditorParameter
                {
                    Id = 201,
                    Area = EditorArea.SellerSide,
                    ModelType = ParameterType.RegionsModel,
                    ViewType = EditorType.RegionEditor,
                    Name = "Регион"
                },
                new EditorParameter
                {
                    Id = 202,
                    Area = EditorArea.SellerSide,
                    ModelType = ParameterType.TaxAuthorityModel,
                    ViewType = EditorType.TaxAuthorityEditor,
                    Name = "Инспекция"
                },
                new EditorParameter
                {
                    Id = 203,
                    Area = EditorArea.SellerSide,
                    ModelType = ParameterType.ListOfStringModel,
                    ViewType = EditorType.InnEditor,
                    Name = "ИНН"
                },
                new EditorParameter
                {
                    Id = 204,
                    Area = EditorArea.SellerSide,
                    ModelType = ParameterType.ListModel,
                    ViewType = EditorType.SurEditor,
                    Name = "Значение СУР"
                },
                new EditorParameter
                {
                    Id = 205,
                    Area = EditorArea.SellerSide,
                    ModelType = ParameterType.ListModel,
                    ViewType = EditorType.UntitledCheckBoxEditor,
                    Name = "Признак \"Крупнейший\""
                },
                new EditorParameter
                {
                    Id = 206,
                    Area = EditorArea.SellerSide,
                    ModelType = ParameterType.ListModel,
                    ViewType = EditorType.ListMultiEditor,
                    Name = "Отчетный период"
                },
                /*new EditorParameter
                {
                    Id = 207,
                    Area = EditorArea.SellerSide,
                    ModelType = ParameterType.RangeOfDateModel,
                    ViewType = EditorType.DataRangeEditor,
                    Name = "Дата представления в НО"
                },*/
                new EditorParameter
                {
                    Id = 208,
                    Area = EditorArea.SellerSide,
                    ModelType = ParameterType.MultiRangeOfIntModel,
                    ViewType = EditorType.IntRangeEditor,
                    Name = "Признак НД — К уплате"
                },
                new EditorParameter
                {
                    Id = 209,
                    Area = EditorArea.SellerSide,
                    ModelType = ParameterType.MultiRangeOfIntModel,
                    ViewType = EditorType.IntRangeEditor,
                    Name = "Признак НД — К возмещению"
                },
                new EditorParameter
                {
                    Id = 210,
                    Area = EditorArea.SellerSide,
                    ModelType = ParameterType.MultiRangeOfIntModel,
                    ViewType = EditorType.IntRangeEditor,
                    Name = "Общая сумма расхождений"
                },
                new EditorParameter
                {
                    Id = 211,
                    Area = EditorArea.SellerSide,
                    ModelType = ParameterType.MultiRangeOfIntModel,
                    ViewType = EditorType.IntRangeEditor,
                    Name = "Общая сумма ПВП расхождений"
                },
                new EditorParameter
                {
                    Id = 212,
                    Area = EditorArea.SellerSide,
                    ModelType = ParameterType.MultiRangeOfIntModel,
                    ViewType = EditorType.IntRangeEditor,
                    Name = "Минимальная сумма расхождения"
                },
                new EditorParameter
                {
                    Id = 213,
                    Area = EditorArea.SellerSide,
                    ModelType = ParameterType.MultiRangeOfIntModel,
                    ViewType = EditorType.IntRangeEditor,
                    Name = "Максимальная сумма расхождения"
                },
                new EditorParameter
                {
                    Id = 214,
                    Area = EditorArea.SellerSide,
                    ModelType = ParameterType.MultiRangeOfIntModel,
                    ViewType = EditorType.IntRangeEditor,
                    Name = "Минимальное значение ПВП расхождения"
                },
                new EditorParameter
                {
                    Id = 215,
                    Area = EditorArea.SellerSide,
                    ModelType = ParameterType.MultiRangeOfIntModel,
                    ViewType = EditorType.IntRangeEditor,
                    Name = "Максимальное значение ПВП расхождения"
                },
                new EditorParameter
                {
                    Id = 216,
                    Area = EditorArea.SellerSide,
                    ModelType = ParameterType.MultiRangeOfIntModel,
                    ViewType = EditorType.IntRangeEditor,
                    Name = "Сумма расхождений по книге покупок"
                },
                new EditorParameter
                {
                    Id = 217,
                    Area = EditorArea.SellerSide,
                    ModelType = ParameterType.MultiRangeOfIntModel,
                    ViewType = EditorType.IntRangeEditor,
                    Name = "Сумма расхождений по книге продаж"
                },
                new EditorParameter
                {
                    Id = 218,
                    Area = EditorArea.SellerSide,
                    ModelType = ParameterType.MultiRangeOfIntModel,
                    ViewType = EditorType.IntRangeEditor,
                    Name = "Общее количество расхождений"
                },
                new EditorParameter
                {
                    Id = 219,
                    Area = EditorArea.SellerSide,
                    ModelType = ParameterType.ListModel,
                    ViewType = EditorType.CheckBoxEditor,
                    Name = "Признак НД — Нулевая"
                },
                new EditorParameter
                {
                    Id = 220,
                    Area = EditorArea.SellerSide,
                    ModelType = ParameterType.ListModel,
                    ViewType = EditorType.CheckBoxEditor,
                    Name = "Признак \"Не представлена НД/Журнал\""
                },

                #endregion

                #region параметры отбора расхождений

                new EditorParameter
                {
                    Id = 301,
                    Area = EditorArea.DiscrepancyParameters,
                    ModelType = ParameterType.ListModel,
                    ViewType = EditorType.UntitledCheckBoxEditor,
                    Name = "Вид расхождения"
                },
                new EditorParameter
                {
                    Id = 302,
                    Area = EditorArea.DiscrepancyParameters,
                    ModelType = ParameterType.ListModel,
                    ViewType = EditorType.ListMultiEditor,
                    Name = "Правило сопоставления"
                },
                new EditorParameter
                {
                    Id = 303,
                    Area = EditorArea.DiscrepancyParameters,
                    ModelType = ParameterType.MultiRangeOfIntModel,
                    ViewType = EditorType.IntRangeEditor,
                    Name = "Сумма расхождения"
                },
                new EditorParameter
                {
                    Id = 304,
                    Area = EditorArea.DiscrepancyParameters,
                    ModelType = ParameterType.MultiRangeOfIntModel,
                    ViewType = EditorType.IntRangeEditor,
                    Name = "ПВП расхождения"
                },
                new EditorParameter
                {
                    Id = 305,
                    Area = EditorArea.DiscrepancyParameters,
                    ModelType = ParameterType.ListModel,
                    ViewType = EditorType.SimpleListEditor,
                    Name = "Источник по стороне покупателя"
                },
                new EditorParameter
                {
                    Id = 305,
                    Area = EditorArea.DiscrepancyParameters,
                    ModelType = ParameterType.ListModel,
                    ViewType = EditorType.SimpleListEditor,
                    Name = "Источник по стороне продавца"
                },
                new EditorParameter
                {
                    Id = 306,
                    Area = EditorArea.DiscrepancyParameters,
                    ModelType = ParameterType.ListModel,
                    ViewType = EditorType.ListMultiEditor,
                    Name = "Код вида операции по стороне покупателя"
                },
                new EditorParameter
                {
                    Id = 307,
                    Area = EditorArea.DiscrepancyParameters,
                    ModelType = ParameterType.ListModel,
                    ViewType = EditorType.OperationCodeEditor,
                    Name = "Код вида операции по стороне продавца"
                },
                new EditorParameter
                {
                    Id = 308,
                    Area = EditorArea.DiscrepancyParameters,
                    ModelType = ParameterType.ListModel,
                    ViewType = EditorType.OperationCodeEditor,
                    Name = "Признак \"Отобрано в выборку\""
                }
                    
                #endregion
            };
        }
    }
}
