﻿using System;
using Luxoft.NDS2.Client.UI.Navigator.Views;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Model{
   

    public class InnFilterModel : ListParameterModelBase<KeyValuePair<string, string>>
    {
        public InnFilterModel(Parameter param, EditorParameter meta, IEnumerable<KeyValuePair<string, string>> options)
            : base(param, meta)
        {
            Options = options;
            if(param.Operator == ComparisonOperator.NotDefinedOperator)
            SetSelection(
                param.Values.Any()
                    ? Options.Where(o => param.Values.Any(v => v.Value == o.Key)).ToArray()
                    : new KeyValuePair<string, string>[] { });
            else
            {
                SetSelection(param.Values.Select(x => new KeyValuePair<string, string>(x.Value, x.ValueDescription)).ToArray()); 
            }
        }

        protected override ParameterValue[] BuildValueList()
        {
            return Selection.Select(o => new ParameterValue { Value = o.Key, ValueDescription = o.Value }).ToArray();
        }

        public void ToggleSelection(string key, bool isChecked)
        {
            var list = new List<KeyValuePair<string, string>>();

            if (isChecked)
                list.Add(Options.FirstOrDefault(option => option.Key == key));

            list.AddRange(Selection.Where(x => x.Key != key));

            SetSelection(list.ToArray());
        }
    }
   


}
