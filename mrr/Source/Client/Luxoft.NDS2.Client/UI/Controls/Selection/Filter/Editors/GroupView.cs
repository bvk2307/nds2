﻿using Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Model;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Editors
{
    public partial class GroupView : UserControl
    {
        private readonly FilterGroupModel _model;
        private readonly FilterViewCreator _viewCreator;

        private Dictionary<EditorArea, SubgroupView> _subgroups;

        private const int ConstantHeight = 128;
        private const int CollapsedHeight = 36;

        public GroupView(FilterGroupModel model, FilterViewCreator viewCreator)
        {
            _model = model;
            _viewCreator = viewCreator;
            InitializeComponent();

            _expandable.Text = _model.Name;

            new ToolTip().SetToolTip(_removeButton, "Удалить группу");

            InitSubgroups();

            foreach (var parameter in model.Parameters)
            {
                AddEditor(parameter);
            }

            foreach (var subgroup in _subgroups.Values)
            {
                subgroup.RefreshViews();
            }

            _expandable.Expanded = false;

            SetSize();
            _expandable.ExpandedStateChanged += (sender, args) => SetSize();

            _model.Changed += OnModelChanged;

            _isEnabled.Checked = _model.IsEnabled;
            _model.EnabledChanged += (sender, args) => _isEnabled.Checked = _model.IsEnabled;
            _isEnabled.CheckStateChanged += (sender, args) => _model.IsEnabled = _isEnabled.Checked;
        }

        #region Подгруппы

        private void InitSubgroups()
        {
            _subgroups = new Dictionary<EditorArea, SubgroupView>
            {
                {
                    EditorArea.BuyerSide,
                    new SubgroupView(_model, EditorArea.BuyerSide)
                },
                {
                    EditorArea.SellerSide,
                    new SubgroupView(_model, EditorArea.SellerSide)
                },
                {
                    EditorArea.DiscrepancyParameters,
                    new SubgroupView(_model, EditorArea.DiscrepancyParameters)
                }
            };

            foreach (var subgroup in _subgroups.Values.Reverse())
            {
                subgroup.Dock = DockStyle.Top;
                _contentPanel.Controls.Add(subgroup);
                subgroup.SizeChanged += (sender, args) => SetSize();
            }

            SetSize();
        }

        private void SetSize()
        {
            var height = _subgroups.Sum(c => c.Value.Height) + ConstantHeight;
            Height = _expandable.Expanded
                ? height
                : CollapsedHeight;
        }

        #endregion

        public bool IsExpanded
        {
            get
            {
                return _expandable.Expanded;
            }
            set
            {
                _expandable.Expanded = value;
            }
        }

        public bool IsAllowDeleteOrDisable
        {
            get
            {
                return _removeButton.Enabled;
            }
            set
            {
                _removeButton.Enabled = value;
                _isEnabled.Enabled = value;
            }
        }

        private bool _isEditable;
        
        public bool IsEditable {
            get
            {
                return _isEditable;
            }
            set
            {
                _isEditable = value;
                _isEnabled.Enabled = value;
                _removeButton.Enabled = value;
                foreach (var sg in _subgroups.Values)
                {
                    sg.IsEditable = value;
                }
            }}

        private void OnModelChanged(object sender, FilterParameterEventArg e)
        {
            if (e.Model != null)
            {
                AddEditor(e.Model);
                RefreshSubgroup(_subgroups[e.Model.EditorParameter.Area]);
            }
        }

        private void AddEditor(FilterParameterModelBase model)
        {
            var editor = _viewCreator.FactoryMethod(model);
            if (editor == null)
                return;
            var subgroup = _subgroups[editor.Model.EditorParameter.Area];
            subgroup.AddFilterParameterView(editor);
            editor.Init();
            subgroup.HeightChanged += (sender, args) => SetSize();
            SetSize();
        }

        private void RefreshSubgroup(SubgroupView subgroup)
        {
            subgroup.RefreshViews();
        }

        private void RemoveClick(object sender, EventArgs e)
        {
            _model.Delete();
        }

        public bool GroupForTemplate
        {
            set
            {
                _regionMessageLabelPanel.Visible = !value;
                if(value)
                _contentTable.RowStyles[0].Height = 0;
            }
        }
    }
}
