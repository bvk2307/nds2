﻿using System;
using System.Collections.Generic;
using System.Linq;
using DocumentFormat.OpenXml.Drawing.Charts;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Model
{
    public class FilterModel : IRegionBound
    {
        private readonly IFilterParameterCreator _paramCreator;

        #region IRegionBound

        public Region[] Regions
        {
            get
            {
                var regions = new List<Region>();
                foreach (var group in Groups)
                {
                    regions.AddRange(group.Regions.Where(r => regions.All( x => x.Code != r.Code)));
                }
                return regions.ToArray();
            }
        }

        public event EventHandler RegionsChanged;

        private void RaiseRegionsChanged()
        {
            var handler = RegionsChanged;
            if (handler != null)
                handler(this, new EventArgs());
        }

        #endregion

        #region privates

        private readonly List<FilterGroupModel> _groups = new List<FilterGroupModel>();

        private readonly List<Region> _regions;

        private int _index;

        #endregion

        public FilterModel(Common.Contracts.DTO.SelectionFilterNamespace.Filter filter, IFilterParameterCreator paramCreator, bool isNew)
        {
            _paramCreator = paramCreator;
            foreach (var group in filter.Groups)
            {
                AddGroup(FilterGroupModelCreator.Create(group, paramCreator, isNew));
            }

            _regions = new List<Region>();
        }

        public FilterGroupModel[] Groups
        {
            get
            {
                return _groups.ToArray();
            }
        }

        public bool IsValid
        {
            get
            {
                return _groups.All(g => g.IsValid || !g.IsEnabled);
            }
        }

        private FilterGroupModel CreateEmptyGroup()
        {
            return FilterGroupModelCreator.Create(
                new ParameterGroup
                {
                    Parameters = new Parameter[] {},
                    IsEnabled = false
                },
                _paramCreator,
                true);
        }

        public void AddGroup(FilterGroupModel group = null)
        {
            if (group == null)
                group = CreateEmptyGroup();

            if (string.IsNullOrEmpty(group.Name))
                group.Name = string.Format("Группа {0}", GetNewIndex());

            group.Changed += (sender, arg) => RaiseChanged(null);
            _groups.Add(group);
            RaiseChanged(group);
        }

        private int GetNewIndex()
        {
            var index = 0;
            foreach (var group in _groups)
            {
                var parts = group.Name.Split(' ').Select(s => s.Trim()).Reverse();
                var num = 0;
                foreach (var part in parts)
                {
                    if (int.TryParse(part, out num))
                        break;
                }
                if (num > index)
                    index = num;
            }
            return index + 1;
        }

        public Common.Contracts.DTO.SelectionFilterNamespace.Filter ToDto()
        {
            return new Common.Contracts.DTO.SelectionFilterNamespace.Filter
            {
                Groups = _groups.Select(g => g.ToDto()).ToArray()
            };
        }

        public event EventHandler<FilterGroupModelEventArgs> Changed;

        private void RaiseChanged(FilterGroupModel group)
        {
            var handler = Changed;
            if (handler != null)
                handler(this, new FilterGroupModelEventArgs(group));
        }

        public void DeleteGroup(FilterGroupModel group)
        {
            _groups.Remove(group);
            RaiseChanged(null);
        }
    }


    public class FilterGroupModelEventArgs : EventArgs
    {
        public FilterGroupModelEventArgs(FilterGroupModel model)
        {
            Model = model;
        }

        public FilterGroupModel Model { get; set; }
    }
}
