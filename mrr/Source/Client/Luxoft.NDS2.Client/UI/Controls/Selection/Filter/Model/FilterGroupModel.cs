﻿using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;
using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using System;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Model
{
    public class FilterGroupModel : IRegionBound
    {
        #region IRegionBound

        public Region[] Regions
        {
            get
            {
                var src = Parameters.FirstOrDefault(p => p.EditorParameter.Area == EditorArea.BuyerSide && p is IRegionBound) as IRegionBound;
                return src != null ? src.Regions.ToArray() : new Region[] { };
            }
        }

        public event EventHandler RegionsChanged;
        #endregion

        private const int BuyerPeriodParameterId = 5;
        public string[] Periods
        {
            get
            {
                var src = Parameters.FirstOrDefault(p => p.EditorParameter.Area == EditorArea.BuyerSide && p.Id == BuyerPeriodParameterId) as ListParameterModel;
                if (src != null && src.Selection.Any())
                    return src.Selection.Select(x => x.Key).ToArray();
                return new string[] { };
            }
        }

        #region privates

        private bool _isEnabled;

        private ParameterGroup _group;

        private readonly List<FilterParameterModelBase> _params = new List<FilterParameterModelBase>();

        private readonly IFilterParameterCreator _paramCreator;

        #endregion

        public void InitializeParameters()
        {
            foreach (var p in _group.Parameters)
            {
                AddParameter(p);
            }
        }

        public FilterGroupModel(ParameterGroup group, IFilterParameterCreator paramCreator)
        {
            _paramCreator = paramCreator;
            _group = group;
            Name = group.Name;
            IsEnabled = group.IsEnabled;
        }

        public bool IsEnabled
        {
            get
            {
                return _isEnabled;
            }
            set
            {
                if (value == _isEnabled)
                    return;
                _isEnabled = value;
                RaiseEnabledChanged();
                RaiseChanged(null);
            }
        }

        public EditorParameter[] AvailableParameters
        {
            get
            {
                return _paramCreator.GetMetaData().Where(meta => meta.ViewType != EditorType.UnEditor && _params.All(p => p.EditorParameter.Id != meta.Id)).ToArray();
            }
        }

        public FilterParameterModelBase[] Parameters
        {
            get
            {
                return _params.ToArray();
            }
        }

        public bool IsValid
        {
            get
            {
                return 
                    (_params.Any(p => p.EditorParameter.Area == EditorArea.BuyerSide && p is IRegionBound && _paramCreator.SelectionType == SelectionType.Hand)
                    || _paramCreator.SelectionType == SelectionType.Template) && _params.All(p => p.IsValid);
            }
        }

        public string Name { get; set; }

        public bool AllowDeleteOrDisable { get; set; }

        public void AddParameter(EditorParameter meta)
        {
            var param = _paramCreator.CreateDefaultParameter(meta);
            AddParameter(param);
        }

        private void AddParameter(Parameter param)
        {
            var model = _paramCreator.FactoryMethod(param);
            if (model == null)
                return;
            model.Changed += (sender, args) => RaiseChanged(null);
            _params.Add(model);

            TryBindByRegion(model);
            TryBindByDiscrepancyType(model);

            RaiseChanged(model);
        }

        #region Manage associatetd parameters

        private void TryBindByRegion(FilterParameterModelBase model)
        {
            var regionModel = model as IRegionBound;
            if (regionModel != null)
            {
                regionModel.RegionsChanged += (sender, args) =>
                {
                    var src = sender as IRegionBound;
                    if (src == null)
                        return;
                    foreach (var trg in _params.Where(p => p.EditorParameter.Area == model.EditorParameter.Area).OfType<IRegionDepender>())
                    {
                        trg.UpdateRegions(src.Regions);
                    }
                };
            }

            var dependModel = model as IRegionDepender;
            if (dependModel != null)
            {
                var src = _params.Where(p => p.EditorParameter.Area == model.EditorParameter.Area).OfType<IRegionBound>().FirstOrDefault();
                if (src != null)
                    dependModel.UpdateRegions(src.Regions);
            }
        }

        private void TryBindByDiscrepancyType(FilterParameterModelBase model)
        {
            var source = model as IDiscrepancyTypeSource;
            if (source != null)
            {
                model.Changed += (sender, args) =>
                {
                    var src = sender as IDiscrepancyTypeSource;
                    if (src == null)
                        return;
                    foreach (var trg in _params.Where(p => p.EditorParameter.Area == model.EditorParameter.Area).OfType<IDiscrepancyTypeDepender>())
                    {
                        trg.UpdateOptions(src.Selection);
                    }
                };
            }

            var depender = model as IDiscrepancyTypeDepender;
            if (depender != null)
            {
                var src = _params.Where(p => p.EditorParameter.Area == model.EditorParameter.Area).OfType<IDiscrepancyTypeSource>().FirstOrDefault();
                if (src != null)
                    depender.UpdateOptions(src.Selection);
            }
        }

        #endregion

        public void DeleteFilter(FilterParameterModelBase model)
        {
            _params.Remove(model);
            RaiseChanged(null);
        }

        public ParameterGroup ToDto()
        {
            return new ParameterGroup
            {
                Name = Name,
                IsEnabled = IsEnabled,
                Parameters = _params.Select(p => p.ToDto()).ToArray()
            };
        }

        #region events

        private void RaiseChanged(FilterParameterModelBase model)
        {
            var handler = Changed;
            if (handler != null)
                handler(this, new FilterParameterEventArg(model));
        }

        private void RaiseDeleting()
        {
            var handler = Deleting;
            if (handler != null)
                handler(this, new EventArgs());
        }

        private void RaiseEnabledChanged()
        {
            var handler = EnabledChanged;
            if (handler != null)
                handler(this, new EventArgs());
        }

        private void RaiseSideChanged()
        {
            var handler = SideChanged;
            if (handler != null)
                handler(this, new EventArgs());
        }

        public event EventHandler<FilterParameterEventArg> Changed;

        public event EventHandler Deleting;

        public event EventHandler EnabledChanged;

        public event EventHandler SideChanged;

        #endregion

        public void Delete()
        {
            RaiseDeleting();
        }
    }

    public class FilterParameterEventArg : EventArgs
    {
        public FilterParameterEventArg(FilterParameterModelBase model)
        {
            Model = model;
        }

        public FilterParameterModelBase Model { get; set; }
    }
   
}
