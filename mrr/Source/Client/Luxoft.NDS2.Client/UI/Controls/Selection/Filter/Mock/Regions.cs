﻿using Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Model;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Mock
{
    public static class Regions
    {
        public static RegionDictionary GetRegions(int regionCount, int authorityCount)
        {
            var list = new List<Region>();
            for (var i = 0; i < regionCount; ++i)
            {
                var r = new Region
                {
                    Id = i,
                    Code = i.ToString().PadLeft(2, '0')
                };
                r.Name = "Регион " + r.Code;
                r.RegionFullName = r.Code + " - " + r.Name;
                var authorityList = new List<Inspection>();
                for (var j = 0; j < authorityCount; ++j)
                    authorityList.Add(new Inspection
                    {
                        Code = r.Code + j.ToString().PadLeft(2, '0'),
                        Name = "Инспекция №" + j + ", " + r.Name
                    });
                list.Add(r);
                r.Inspections = authorityList;
            }
            return new RegionDictionary(list);
        }

        public static TaxAuthorityDictionary GetAuthorities(this RegionDictionary regions)
        {
            var list = new List<Inspection>();
            foreach (var region in regions.Search(""))
            {
                list.AddRange(region.Inspections);
            }
            return new TaxAuthorityDictionary(list);
        }
    }
}
