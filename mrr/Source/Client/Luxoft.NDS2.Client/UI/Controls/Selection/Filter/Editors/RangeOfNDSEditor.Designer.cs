﻿namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Editors
{
    partial class RangeOfNDSEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.UltraWinToolTip.UltraToolTipManager ultraToolTipManager1;
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinToolTip.UltraToolTipInfo ultraToolTipInfo2 = new Infragistics.Win.UltraWinToolTip.UltraToolTipInfo("Добавить", Infragistics.Win.ToolTipImage.Default, null, Infragistics.Win.DefaultableBoolean.Default);
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RangeOfNDSEditor));
            Infragistics.Win.UltraWinToolTip.UltraToolTipInfo ultraToolTipInfo1 = new Infragistics.Win.UltraWinToolTip.UltraToolTipInfo("Удалить", Infragistics.Win.ToolTipImage.Default, null, Infragistics.Win.DefaultableBoolean.Default);
            this._addButton = new Infragistics.Win.Misc.UltraButton();
            this._removeButton = new Infragistics.Win.Misc.UltraButton();
            this._comboOperator = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this._from = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this._table = new System.Windows.Forms.TableLayoutPanel();
            ultraToolTipManager1 = new Infragistics.Win.UltraWinToolTip.UltraToolTipManager(this.components);
            ((System.ComponentModel.ISupportInitialize)(this._comboOperator)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._from)).BeginInit();
            this._table.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraToolTipManager1
            // 
            ultraToolTipManager1.ContainingControl = this;
            // 
            // _addButton
            // 
            appearance3.Image = global::Luxoft.NDS2.Client.Properties.Resources.list_add;
            appearance3.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance3.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this._addButton.Appearance = appearance3;
            this._addButton.Dock = System.Windows.Forms.DockStyle.Top;
            this._addButton.Location = new System.Drawing.Point(413, 0);
            this._addButton.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this._addButton.Name = "_addButton";
            this._addButton.Size = new System.Drawing.Size(25, 24);
            this._addButton.TabIndex = 4;
            ultraToolTipInfo2.ToolTipText = "Добавить";
            ultraToolTipManager1.SetUltraToolTip(this._addButton, ultraToolTipInfo2);
            // 
            // _removeButton
            // 
            appearance1.Image = ((object)(resources.GetObject("appearance1.Image")));
            appearance1.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance1.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this._removeButton.Appearance = appearance1;
            this._removeButton.Dock = System.Windows.Forms.DockStyle.Top;
            this._removeButton.Location = new System.Drawing.Point(383, 0);
            this._removeButton.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this._removeButton.Name = "_removeButton";
            this._removeButton.Size = new System.Drawing.Size(24, 24);
            this._removeButton.TabIndex = 5;
            ultraToolTipInfo1.ToolTipText = "Удалить";
            ultraToolTipManager1.SetUltraToolTip(this._removeButton, ultraToolTipInfo1);
            // 
            // _comboOperator
            // 
            this._comboOperator.Dock = System.Windows.Forms.DockStyle.Top;
            this._comboOperator.Location = new System.Drawing.Point(0, 0);
            this._comboOperator.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._comboOperator.Name = "_comboOperator";
            this._comboOperator.Size = new System.Drawing.Size(197, 21);
            this._comboOperator.TabIndex = 6;
            // 
            // _from
            // 
            this._from.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._from.FormatString = "N";
            this._from.Location = new System.Drawing.Point(200, 0);
            this._from.Margin = new System.Windows.Forms.Padding(0);
            this._from.MaskInput = "{double:-22.2}";
            this._from.MaxValue = new decimal(new int[] {
            -159383553,
            46653770,
            5421,
            0});
            this._from.MinValue = 0;
            this._from.Name = "_from";
            this._from.Nullable = true;
            this._from.NullText = "от";
            this._from.NumericType = Infragistics.Win.UltraWinEditors.NumericType.Decimal;
            this._from.PromptChar = ' ';
            this._from.Size = new System.Drawing.Size(180, 21);
            this._from.TabIndex = 7;
            // 
            // _table
            // 
            this._table.ColumnCount = 5;
            this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180F));
            this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._table.Controls.Add(this._from, 1, 0);
            this._table.Controls.Add(this._comboOperator, 0, 0);
            this._table.Controls.Add(this._removeButton, 2, 0);
            this._table.Controls.Add(this._addButton, 3, 0);
            this._table.Dock = System.Windows.Forms.DockStyle.Fill;
            this._table.Location = new System.Drawing.Point(0, 0);
            this._table.Name = "_table";
            this._table.RowCount = 1;
            this._table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._table.Size = new System.Drawing.Size(867, 30);
            this._table.TabIndex = 0;
            // 
            // RangeOfNDSEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._table);
            this.MinimumSize = new System.Drawing.Size(200, 30);
            this.Name = "RangeOfNDSEditor";
            this.Size = new System.Drawing.Size(867, 30);
            ((System.ComponentModel.ISupportInitialize)(this._comboOperator)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._from)).EndInit();
            this._table.ResumeLayout(false);
            this._table.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraButton _addButton;
        private Infragistics.Win.Misc.UltraButton _removeButton;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor _comboOperator;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor _from;
        private System.Windows.Forms.TableLayoutPanel _table;
    }
}
