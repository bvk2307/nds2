﻿namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Editors
{
    partial class AttributeSelectorView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this._table = new System.Windows.Forms.TableLayoutPanel();
            this._applyButton = new Infragistics.Win.Misc.UltraButton();
            this._selector = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            this._table.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._selector)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this._table);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(622, 46);
            this.ultraGroupBox1.TabIndex = 0;
            this.ultraGroupBox1.Text = "Выберите атрибут для фильтра";
            // 
            // _table
            // 
            this._table.AutoSize = true;
            this._table.ColumnCount = 2;
            this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this._table.Controls.Add(this._applyButton, 1, 0);
            this._table.Controls.Add(this._selector, 0, 0);
            this._table.Dock = System.Windows.Forms.DockStyle.Fill;
            this._table.Location = new System.Drawing.Point(3, 16);
            this._table.Name = "_table";
            this._table.RowCount = 1;
            this._table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._table.Size = new System.Drawing.Size(616, 27);
            this._table.TabIndex = 3;
            // 
            // _applyButton
            // 
            this._applyButton.Dock = System.Windows.Forms.DockStyle.Top;
            this._applyButton.Location = new System.Drawing.Point(519, 3);
            this._applyButton.Name = "_applyButton";
            this._applyButton.Size = new System.Drawing.Size(94, 21);
            this._applyButton.TabIndex = 4;
            this._applyButton.Text = "Добавить";
            this._applyButton.Click += new System.EventHandler(this.ApplyClick);
            // 
            // _selector
            // 
            this._selector.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.None;
            this._selector.Dock = System.Windows.Forms.DockStyle.Top;
            this._selector.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this._selector.Location = new System.Drawing.Point(3, 3);
            this._selector.MaxDropDownItems = 20;
            this._selector.Name = "_selector";
            this._selector.Size = new System.Drawing.Size(510, 21);
            this._selector.TabIndex = 5;
            // 
            // AttributeSelectorView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ultraGroupBox1);
            this.Name = "AttributeSelectorView";
            this.Size = new System.Drawing.Size(622, 46);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            this._table.ResumeLayout(false);
            this._table.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._selector)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private System.Windows.Forms.TableLayoutPanel _table;
        private Infragistics.Win.Misc.UltraButton _applyButton;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor _selector;



    }
}
