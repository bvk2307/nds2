﻿using Luxoft.NDS2.Common.Contracts.DTO.SelectionFilterNamespace;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Model
{
    public class FilterParameterReadonlyModel : FilterParameterModelBase
    {
        private readonly Parameter _parameter;

        public FilterParameterReadonlyModel(Parameter parameter, EditorParameter meta)
            : base(parameter, meta)
        {
            _parameter = parameter;

            if (parameter.Values == null || parameter.Values.Length == 0)
            {
                Value = "Параметры не заданы";
                return;
            }
            Value = string.Join("; ", _parameter.Values.Select(GetValueString));
        }

        private string GetValueString(ParameterValue value)
        {
            if (value == null)
                return "Параметр не задан";
            
            var sb = new StringBuilder();

            if (value.Value != null)
            {
                sb.Append(value.Value);
                if (value.ValueDescription != null)
                    sb.Append(": ").Append(value.ValueDescription);
            }
            else
                sb.Append("[")
                    .Append(string.IsNullOrEmpty(value.ValueFrom) ? "Не задано" : value.ValueFrom)
                    .Append(" — ")
                    .Append(string.IsNullOrEmpty(value.ValueTo) ? "Не задано" : value.ValueTo)
                    .Append("]");

            return sb.ToString();
        }

        public override bool IsValid
        {
            get { return true; }
        }

        protected override ParameterValue[] BuildValueList() { return null; }

        public override Parameter ToDto()
        {
            return _parameter;
        }

        public string Value { get; set; }
    }
}
