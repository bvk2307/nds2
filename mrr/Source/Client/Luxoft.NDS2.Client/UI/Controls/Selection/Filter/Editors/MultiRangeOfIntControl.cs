﻿using Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Editors
{
    public partial class MultiRangeOfIntControl : UserControl, IFilterParameterView
    {
        private readonly MultiRangeParameterModel<long?> _model;

        public MultiRangeOfIntControl(MultiRangeParameterModel<long?> model)
        {
            _model = model;
            InitializeComponent();

            foreach (var range in _model.Ranges)
            {
                AddRange(range);
            }

            _model.Changed += (sender, args) =>
            {
                foreach (var range in _model.Ranges.Where(r => _controlMap.All(item => !item.Key.Equals(r))))
                {
                    AddRange(range);
                }
            };
        }

        private readonly Dictionary<MultiRangeItemModel<long?>, RangeOfIntEditor> _controlMap = new Dictionary<MultiRangeItemModel<long?>, RangeOfIntEditor>();

        private void SetupRangeEditorButtons()
        {
            if (_controlMap.Count == 1)
            {
                var control = _controlMap.Values.First();
                control.IsAllowAdd = true;
                control.IsAllowRemove = false;
                return;
            }
            foreach (var control in _controlMap.Values)
            {
                control.IsAllowAdd = false;
                control.IsAllowRemove = true;
            }
            _controlMap.Values.Last().IsAllowAdd = true;
        }

        private void AddRange(MultiRangeItemModel<long?> range)
        {
            Controls.Clear();

            var control = new RangeOfIntEditor(range) {Dock = DockStyle.Top};
            _controlMap[range] = control;
            range.Deleting += DeleteRange;
            control.Adding += AddRange;
            foreach (var editor in _controlMap.Values.Reverse())
            {
                Controls.Add(editor);
            }

            SetSize();
            SetupRangeEditorButtons();
        }

        private void AddRange(object sender, EventArgs e)
        {
            _model.AddRange();
        }

        private void DeleteRange(object sender, EventArgs e)
        {
            var model = sender as MultiRangeItemModel<long?>;
            if (model == null)
                return;
            Controls.Remove(_controlMap[model]);
            _controlMap.Remove(model);
            _model.DeleteRange(model);
            
            SetSize();
            SetupRangeEditorButtons();
        }

        private void SetSize()
        {
            var height = _controlMap.Values.Sum(c => c.Height) + 8;
            Height = height;
            var handler = HeightChanged;
            if (handler != null)
                handler(this, null);
        }

        public FilterParameterModelBase Model
        {
            get
            {
                return _model;
            }
        }

        public UserControl AsControl() { return this; }

        public event EventHandler HeightChanged;

        public int GetHeight()
        {
            return _controlMap.Values.Sum(c => c.Height) + 8;
        }

        public void Init()
        {
            SetSize();
        }
    }
}
