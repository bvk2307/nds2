﻿using System;
using Luxoft.NDS2.Client.Model;
using Luxoft.NDS2.Client.UI.Controls.LookupSelector;
using Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Model;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Metodolog;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using MultiSelect = Luxoft.NDS2.Client.UI.Controls.LookupSelector.Implementation.MultiSelect;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.Filter.Editors
{
    public partial class TaxAuthorityControl : UserControl, IFilterParameterView
    {
        #region nested adapters

        class InspectionLookupEntry : ILookupEntry<string>
        {
            private readonly Inspection _inspection;
            public bool IsSelected { get; set; }

            [ListDisplaySettings(Title = "Код", Width = 80)]
            public string Key
            {
                get
                {
                    return _inspection.Code;
                }
            }

            [ListDisplaySettings(Title = "Наименование", Width = 1000)]
            public string Title
            {
                get
                {
                    return _inspection.Name;
                }
            }

            public Inspection Inspection
            {
                get
                {
                    return _inspection;
                }
            }

            public InspectionLookupEntry(Inspection inspection)
            {
                _inspection = inspection;
            }
        }

        class RegionRequestResult : IDataRequestResult<InspectionLookupEntry>
        {
            public IEnumerable<InspectionLookupEntry> Data { get; set; }

            public bool ExceedsMaxQuantity { get; set; }
        }

        #endregion

        private readonly TaxAuthorityParameterModel _model;
        private readonly MultiSelect.Presenter<string, InspectionLookupEntry> _presenter;

        public TaxAuthorityControl(TaxAuthorityParameterModel model)
        {
            _model = model;

            InitializeComponent();

            _presenter = new MultiSelect.Presenter<string, InspectionLookupEntry>(
                _lookup,
                Search)
            {
                SelectedItems = _model.Selection.ToDictionary(
                    inspection => inspection.Code,
                    inspection => new InspectionLookupEntry(inspection))
            };
            _lookup.OnSave += () => _model.SetSelection(_presenter.SelectedItems.Select(x => x.Value.Inspection).ToArray());

            _model.Changed += (sender, args) =>
            {
                _presenter.SelectedItems = model.Selection.ToDictionary(
                    inspection => inspection.Code,
                    inspection => new InspectionLookupEntry(inspection));
            };
        }

        private IDataRequestResult<InspectionLookupEntry> Search(IDataRequestArgs arg)
        {
            return new RegionRequestResult
            {
                Data = _model.Search(arg.SearchPattern).Select(r => new InspectionLookupEntry(r)).ToArray()
            };
        }

        public FilterParameterModelBase Model
        {
            get
            {
                return _model;
            }
        }

        public UserControl AsControl() { return this; }
        public event EventHandler HeightChanged;

        public int GetHeight()
        {
            return Height;
        }

        public void Init() {  }
    }
}
