﻿using Luxoft.NDS2.Client.UI.Controls.LookupSelector;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.ElementEditors
{
    public interface ITaxAuthorityRequestArgs : IDataRequestArgs
    {
        bool RestrictByRegion
        {
            get;
        }

        string[] RegionCodes
        {
            get;
        }
    }
}
