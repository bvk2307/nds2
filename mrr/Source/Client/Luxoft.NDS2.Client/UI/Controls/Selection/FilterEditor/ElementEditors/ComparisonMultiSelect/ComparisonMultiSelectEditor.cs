﻿using Infragistics.Win;
using Luxoft.NDS2.Client.UI.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.ElementEditors.ComparisonMultiSelect
{
    public partial class ComparisonMultiSelectEditor : UserControl, IComparisonMultiSelectView
    {
        public ComparisonMultiSelectEditor()
        {
            InitializeComponent();

            _optionsMultiSelect.ValueChanged += OnValueChanged;
            _optionsList.ValueChanged += OnValueChanged;
        }

        public void InitLookup(Dictionary<string, string> options)
        {
            _optionsMultiSelect.Items.Clear();

            if (options.Any())
            {
                _optionsMultiSelect.Items.AddRange(
                    options.Select(x => new ValueListItem(x.Key, x.Value)).ToArray());
            }
        }

        public void Select(string key)
        {
            foreach (var item in _optionsMultiSelect.Items)
            {
                if (item.DataValue.ToString() == key)
                {
                    item.CheckState = CheckState.Checked;
                }
            }
        }

        public string[] SelectedKeys
        {
            get
            {
                var selectedKeys = new List<string>();

                foreach (var item in _optionsMultiSelect.Items)
                {
                    if (item.CheckState == CheckState.Checked)
                    {
                        selectedKeys.Add(item.DataValue.ToString());
                    }
                }

                return selectedKeys.ToArray();
            }
        }

        public void SetComparisonOptions(List<string> comparisonTypes)
        {
            _optionsList.DataSource = comparisonTypes;
        }

        public string SelectedComparisonType
        {
            get
            {
                return _optionsList.Value.ToString();
            }
            set
            {
                _optionsList.Value = value;
            }
        }

        public event ParameterlessEventHandler ValueChanged;

        private void OnValueChanged(object sender, EventArgs e)
        {
            if (ValueChanged != null)
            {
                ValueChanged();
            }
            GenerateAfterValueChange();
        }

        public event ParameterlessEventHandler AfterValueChanged;

        private void GenerateAfterValueChange()
        {
            if (AfterValueChanged != null)
            {
                AfterValueChanged();
            }
        }
    }
}
