﻿using Infragistics.Win;
using Luxoft.NDS2.Client.UI.Base;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.ElementEditors
{
    public partial class ListMultiSelectView : UserControl, IListMultiSelectView
    {
        public ListMultiSelectView()
        {
            InitializeComponent();

            _optionsMultiSelect.ValueChanged += OnValueChanged;
        }

        public void InitLookup(Dictionary<string, string> options)
        {
            _optionsMultiSelect.Items.Clear();

            if (options.Any())
            {
                _optionsMultiSelect.Items.AddRange(
                    options.Select(x => new ValueListItem(x.Key, x.Value)).ToArray());
            }
        }

        public void Select(string key)
        {
            foreach (var item in _optionsMultiSelect.Items)
            {
                if (item.DataValue.ToString() == key)
                {
                    item.CheckState = CheckState.Checked;
                }
            }
        }

        public string[] SelectedKeys
        {
            get 
            {
                var selectedKeys = new List<string>();

                foreach (var item in _optionsMultiSelect.Items)
                {
                    if (item.CheckState == CheckState.Checked)
                    {
                        selectedKeys.Add(item.DataValue.ToString());
                    }
                }

                return selectedKeys.ToArray();
            }
        }

        public event ParameterlessEventHandler ValueChanged;

        private void OnValueChanged(object sender, EventArgs e)
        {
            if (ValueChanged != null)
            {
                ValueChanged();
            }
        }
    }
}
