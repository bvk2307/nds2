﻿using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.ElementEditors
{
    public interface IComparisonTypeSelectView : IElementView
    {
        void SetComparisonOptions(List<string> criteriaTypes);

        string SelectedComparisonType { get; set; }

        string ComparisonExpression { get; set; }
    }
}
