﻿using System;
using System.Collections.Generic;
using System.Linq;
using Luxoft.NDS2.Common.Contracts.DTO;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.ElementEditors.ComparisonMultiSelect
{
    public class ComparisonMultiSelectPresenter : ElementPresenterBase<IComparisonMultiSelectView>
    {
        private readonly Dictionary<string, FilterElement.ComparisonOperations> _availableOperations;
        private readonly Func<Dictionary<string, string>> _lookupItemsSource;

        public ComparisonMultiSelectPresenter(
            IComparisonMultiSelectView view,
            Dictionary<string, FilterElement.ComparisonOperations> availableOperations,
             Func<Dictionary<string, string>> lookupItemsSource,
            List<FilterElement> model)
            : base(view, model)
        {
            _availableOperations = availableOperations;
            _lookupItemsSource = lookupItemsSource;
        }

        private FilterElement.ComparisonOperations Operator
        {
            get
            {
                return Model.Count > 0 ? Model.First().Operator : FilterElement.ComparisonOperations.Contains;
            }
        }

        protected override void InitViewInternal()
        {
            View.SetComparisonOptions(_availableOperations.Select(x => x.Key).ToList());

            if (_availableOperations.ContainsValue(Operator))
            {
                View.SelectedComparisonType =
                    _availableOperations.First(x => x.Value == Operator).Key;
            }

            var options = _lookupItemsSource();

            View.InitLookup(options);

            foreach (var element in Model)
            {
                if (options.ContainsKey(element.ValueOne))
                {
                    View.Select(element.ValueOne);
                }
            }
        }

        protected override void UpdateModel()
        {
            var op = _availableOperations[View.SelectedComparisonType];
            Model.Clear();
            Model.AddRange(
                View.SelectedKeys.Select(
                    x => new FilterElement
                    {
                        Operator = op,
                        ValueOne = x,
                        ValueTwo = string.Empty
                    }));
        }

        protected override bool IsChanged()
        {
            return Model.Count != View.SelectedKeys.Count()
                || Model.Any(m => View.SelectedKeys.All(vw => m.ValueOne != vw))
                || Operator != _availableOperations[View.SelectedComparisonType];
        }
    }
}
