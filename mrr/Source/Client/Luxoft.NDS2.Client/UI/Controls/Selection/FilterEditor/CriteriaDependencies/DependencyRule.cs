﻿using Luxoft.NDS2.Common.Contracts.DTO;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Discrepancy;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.CriteriaDependencies
{
    public class DependencyRule : IDependencyRule
    {
        private const string DiscrepancyKind = "DiscrepancyKind";

        private const string CompareRule = "CompareRule";

        private readonly DiscrepancyType[] _discrepancyCompareRuleToKindApplicability =
            new[] { DiscrepancyType.InaccurateComparison, DiscrepancyType.NDS };

        public bool IsApplicable(GroupFilter currentState, FilterCriteria criteria)
        {
            return criteria.InternalName != CompareRule
                || currentState.Filters.All(x => 
                      !x.IsActive
                    || x.InternalName != DiscrepancyKind
                    || x.Values.Any(
                        val => _discrepancyCompareRuleToKindApplicability.Contains(
                                (DiscrepancyType)int.Parse(val.ValueOne)))
                    || x.Values.All(val => string.IsNullOrWhiteSpace(val.ValueOne)));
        }
    }
}
