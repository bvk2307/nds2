﻿using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Common.Contracts.DTO;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor
{
    public interface ICriteriaView
    {
        ICriteriaTypeSelectView TypeSelectView { get; }

        IElementView ShowValueEditor(FilterCriteria.ValueTypes type);

        bool IsActive { get; set; }

        bool AllowAdd { get; set; }

        bool AllowChangeType { get; set; }

        bool AllowDelete { get; set; }

        int Index { get; set; }

        event UserActionHandler Adding;

        event UserActionHandler Deleting;

        event UserActionHandler ActivityChanged;
    }
}
