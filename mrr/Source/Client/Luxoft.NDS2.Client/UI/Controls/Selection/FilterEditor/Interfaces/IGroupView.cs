﻿namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor
{
    public interface IGroupView
    {
        ICriteriaView NewCriteria();

        void Draw(ICriteriaView[] view);

        bool IsActive { get; set; }

        bool AllowRemove { get; set; }

        int Index { get; set; }

        event UserActionHandler Adding;

        event UserActionHandler Deleting;

        event UserActionHandler ActivityChanged;

        event UserActionHandler ScrollChanged;
    }
}
