﻿using Luxoft.NDS2.Client.Model;
using Luxoft.NDS2.Client.UI.Controls.LookupSelector;
using Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.ElementEditors;
using Luxoft.NDS2.Client.UI.Controls.Selection.Models;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.Creators
{
    public interface ILookupDataSource
    {
        Func<Dictionary<string, string>> SimpleLookup(string lookupName);

        IDataRequestResult<TEntityLookup> Search<TEntityLookup>(IDataRequestArgs criteria)
            where TEntityLookup : ILookupEntry<string>;

        IDataRequestResult<TaxAuthorityModel> Search(ITaxAuthorityRequestArgs criteria);
    }
}
