﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.GroupEditor
{
    public partial class FilterGroupEditor : UserControl, IGroupView
    {
        private const int ContainerColumnIndex = 1;

        private readonly ICriteriaViewCreator _viewCreator;

        private readonly List<ICriteriaView> _criterias = new List<ICriteriaView>();

        public FilterGroupEditor()
        {
            InitializeComponent();   
        }

        public FilterGroupEditor(ICriteriaViewCreator viewCreator)
            : this()
        {            
            _table.RowStyles.Clear();
            _viewCreator = viewCreator;

            _addButton.Click +=
                (sender, args) =>
                {
                    if (Adding != null)
                    {
                        Adding();
                    }
                };

            _removeButton.Click +=
                (sender, args) =>
                {
                    if (Deleting != null)
                    {
                        Deleting();
                    }
                };

            _isActiveInput.CheckedChanged +=
                (sender, args) =>
                {
                    if (ActivityChanged != null)
                    {
                        ActivityChanged();
                    }
                };
        }      

        public bool IsActive
        {
            get
            {
                return _isActiveInput.Checked;
            }
            set
            {
                _isActiveInput.Checked = value;
            }
        }

        public bool AllowRemove
        {
            get
            {
                return _removeButton.Visible;
            }
            set
            {
                _removeButton.Visible = value;
            }
        }

        public int Index
        {
            get;
            set;
        }

        public event UserActionHandler Adding;

        public event UserActionHandler Deleting;

        public event UserActionHandler ActivityChanged;

        public event UserActionHandler FilterGroupChanged;

        public ICriteriaView NewCriteria()
        {
            return _viewCreator.Create();
        }

        public void Draw(ICriteriaView[] views)
        {
            SuspendLayout();

            ClearContainer();

            for (var idx = 0; idx < views.Length; idx++)            
            {
                var controlToAdd = views[idx] as Control;
                controlToAdd.Dock = DockStyle.Top;

                _table.RowStyles.Add(new RowStyle(SizeType.AutoSize));
                _table.Controls.Add(controlToAdd, ContainerColumnIndex, idx);

                ICriteriaView criteriaView = views[idx];
                criteriaView.ActivityChanged += criteriaView_ActivityChanged;
                criteriaView.Adding += criteriaView_Adding;
                criteriaView.Deleting += criteriaView_Deleting;

                ICriteriaTypeSelectView criteriaTypeSelectView = (ICriteriaTypeSelectView)views[idx];
                criteriaTypeSelectView.CriteriaTypeChanged += criteriaTypeSelectView_CriteriaTypeChanged;
            }

            ResumeLayout();

            GenerateScrollChanged();
        }

        private void criteriaView_FilterCriteriaChanged()
        {
            GenerateFilterGroupChanged();
        }

        private void criteriaTypeSelectView_CriteriaTypeChanged(string selectedObject)
        {
            GenerateFilterGroupChanged();
        }

        private void criteriaView_Deleting()
        {
            GenerateFilterGroupChanged();
        }

        private void criteriaView_Adding()
        {
            GenerateFilterGroupChanged();
            GenerateScrollChanged();
        }

        private void criteriaView_ActivityChanged()
        {
            GenerateFilterGroupChanged();
        }

        private void GenerateFilterGroupChanged()
        {
            if (FilterGroupChanged != null)
            {
                FilterGroupChanged();
            }
        }

        private void GenerateScrollChanged()
        {
            if (ScrollChanged != null)
            {
                ScrollChanged();
            }
        }

        private void ClearContainer()
        {            
            _table.RowStyles.Clear();
            var controlsToRemove = new List<Control>();

            foreach (var control in _table.Controls)
            {
                if (control is ICriteriaView)
                {
                    controlsToRemove.Add((Control)control);
                }
            }

            foreach (var control in controlsToRemove)
            {
                _table.Controls.Remove(control);
            }
        }

        public event UserActionHandler ScrollChanged;
    }
}
