﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor
{
    public interface IVertiacalResizableView
    {
        int Height { get; }

        int MarginTop { get; }

        int MarginBottom { get; }        
    }
}
