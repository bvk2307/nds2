﻿using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.ElementEditors
{
    public interface IListMultiSelectView : IElementView
    {
        void InitLookup(Dictionary<string, string> options);

        void Select(string key);

        string[] SelectedKeys { get; }
    }
}
