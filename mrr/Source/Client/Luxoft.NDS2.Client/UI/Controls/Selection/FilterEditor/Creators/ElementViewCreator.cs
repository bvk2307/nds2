﻿using Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.ElementEditors;
using Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.ElementEditors.ComparisonMultiSelect;
using Luxoft.NDS2.Common.Contracts.DTO;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.Creators
{
    public class ElementViewCreator : IElementViewCreator
    {
        private const string TypeNotSupportedMessagePattern = "Selection filter criteria type {0} is not supported";

        private readonly Dictionary<FilterCriteria.ValueTypes, Func<IElementView>> _views =
            new Dictionary<FilterCriteria.ValueTypes, Func<IElementView>>
            {
                { FilterCriteria.ValueTypes.Edit, () => new  PlainTextFilterCriteriaEditor() },
                { FilterCriteria.ValueTypes.List, () => new LookupSelectFilterCriteriaEditor() },
                { FilterCriteria.ValueTypes.EditWithComparisonStr, () => new ComparisonTypeSelectEditor() },
                { FilterCriteria.ValueTypes.EditWithComparisonNum, () => new ComparisonTypeSelectEditor() },
                { FilterCriteria.ValueTypes.EditIntervalDates, () => new DateRangeCriteriaEditor() },
                { FilterCriteria.ValueTypes.EditIntervalPrice, () => new CurrencyRangeCriteriaEditor() },
                { FilterCriteria.ValueTypes.EditIntervalInt, () => new IntegerRangeCriteriaEditor() },
                { FilterCriteria.ValueTypes.ListMultpleChoice, () => new ListMultiSelectView() },
                { FilterCriteria.ValueTypes.RegionsMultiSelect, () => new SearchMultiSelectView() },
                { FilterCriteria.ValueTypes.TaxAuthoritiesMultiSelect, () => new SearchMultiSelectView() },
                { FilterCriteria.ValueTypes.TextMultipleInput, () => new PlainTextFilterCriteriaEditor() },
                { FilterCriteria.ValueTypes.ListMultipleCompirison, () => new ComparisonMultiSelectEditor() },
            };

        public IElementView Create(FilterCriteria.ValueTypes type)
        {
            if (!_views.ContainsKey(type))
            {
                throw new NotSupportedException(
                    string.Format(TypeNotSupportedMessagePattern, type));
            }

            return _views[type]();
        }
    }
}
