﻿using Luxoft.NDS2.Client.Model;
using Luxoft.NDS2.Client.UI.Controls.LookupSelector;
using Luxoft.NDS2.Client.UI.Controls.LookupSelector.Base;
using Luxoft.NDS2.Common.Contracts.DTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.ElementEditors
{
    public class SearchMultiSelectPresenter<TLookupEntry>
        : ElementPresenterBase<ISearchMultiSelectView>
        where TLookupEntry : ILookupEntry<string>
    {
        private readonly Func<IDataRequestArgs, IDataRequestResult<TLookupEntry>> _searchDataSource;

        private readonly Func<string, string, TLookupEntry> _lookupEntryCreator;

        protected ILookupSelectionPresenter<string, TLookupEntry> ChildPresenter
        {
            get;
            private set;
        }

        public SearchMultiSelectPresenter(
            ISearchMultiSelectView view,
            Func<IDataRequestArgs, IDataRequestResult<TLookupEntry>> searchDataSource,
            Func<string, string, TLookupEntry> lookupEntryCreator,
            List<FilterElement> model)
            : base(view, model)
        {
            _searchDataSource = searchDataSource;
            _lookupEntryCreator = lookupEntryCreator;
        }

        protected override void InitViewInternal()
        {
            ChildPresenter =
                new LookupSelectionPresenter<string, TLookupEntry>(
                    View.SelectionView,
                    View.SelectionEditView,
                    _searchDataSource,
                    Model.Select(val => _lookupEntryCreator(val.ValueOne, val.ValueTwo))
                        .ToDictionary(x => x.Key, x => x));
        }

        protected override void UpdateModel()
        {
            Model.Clear();
            Model.AddRange(
                ChildPresenter.SelectedItems.Select(
                    x => new FilterElement() 
                    { 
                        Operator = FilterElement.ComparisonOperations.Equals, 
                        ValueOne = x.Key, 
                        ValueTwo = x.Value.Title 
                    }));
        }

        protected override bool IsChanged()
        {
            return Model.Count() != ChildPresenter.SelectedItems.Count
                || Model.Any(m => ChildPresenter.SelectedItems.All(vw => vw.Key != m.ValueOne));
        }
    }
}
