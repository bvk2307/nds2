﻿using System;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor
{
    public partial class SelectionFilterEditor : UserControl, IView
    {
        private readonly IGroupViewCreator _groupCreator;

        public SelectionFilterEditor()
        {
            InitializeComponent();            
        }

        public SelectionFilterEditor(IGroupViewCreator groupCreator)
            : this()
        {            
            _groupCreator = groupCreator;
            _container.RowStyles.Clear();
        }

        public IGroupView NewGroup()
        {
            return _groupCreator.Create();
        }

        public void Draw(IGroupView[] groups)
        {
            SuspendLayout();
            ClearContainer();

            for (var idx = 0; idx < groups.Length; idx++)
            {
                var control = (Control)groups[idx];
                control.Dock = DockStyle.Top;

                var groupView = (IGroupView)control;
                groupView.ScrollChanged += GroupView_ScrollChanged;

                _container.RowStyles.Add(new RowStyle(SizeType.AutoSize));
                _container.Controls.Add(control, 0, idx);

                IGroupView controlGroup = groups[idx];
            }

            ResumeLayout();
        }

        private void GroupView_ScrollChanged()
        {
            _container.VerticalScroll.Value = _container.VerticalScroll.Maximum;
        }

        private void ClearContainer()
        {
            _container.RowStyles.Clear();
            _container.Controls.Clear();
        }

        public void SetAllowEdit(bool allowEdit)
        {
            foreach (Control childControl in _container.Controls)
                childControl.Enabled = allowEdit;
        }
    }
}
