﻿using Luxoft.NDS2.Client.Model;
using Luxoft.NDS2.Client.UI.Controls.LookupSelector;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.ElementEditors
{
    public interface IDataLoader<TLookupEntry>
        where TLookupEntry : ILookupEntry<string>
    {
        IDataRequestResult<TLookupEntry> Load(IDataRequestArgs args);
    }
}
