﻿namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor
{
    public delegate void SelectedItemChangedHandler<T>(T selectedObject);
}
