﻿namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.ElementEditors
{
    partial class CurrencyRangeCriteriaEditor
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Misc.UltraLabel _valueToLabel;
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel _valueFromLabel;
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            this._valueToInput = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            this._valueFromInput = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
            _valueToLabel = new Infragistics.Win.Misc.UltraLabel();
            _valueFromLabel = new Infragistics.Win.Misc.UltraLabel();
            this.SuspendLayout();
            // 
            // _valueToLabel
            // 
            appearance6.TextHAlignAsString = "Center";
            appearance6.TextVAlignAsString = "Top";
            _valueToLabel.Appearance = appearance6;
            _valueToLabel.Dock = System.Windows.Forms.DockStyle.Left;
            _valueToLabel.Location = new System.Drawing.Point(267, 0);
            _valueToLabel.Name = "_valueToLabel";
            _valueToLabel.Size = new System.Drawing.Size(30, 25);
            _valueToLabel.TabIndex = 6;
            _valueToLabel.Text = "до:";
            // 
            // _valueFromLabel
            // 
            appearance7.TextHAlignAsString = "Center";
            appearance7.TextVAlignAsString = "Top";
            _valueFromLabel.Appearance = appearance7;
            _valueFromLabel.Dock = System.Windows.Forms.DockStyle.Left;
            _valueFromLabel.Location = new System.Drawing.Point(0, 0);
            _valueFromLabel.Margin = new System.Windows.Forms.Padding(2);
            _valueFromLabel.Name = "_valueFromLabel";
            _valueFromLabel.Size = new System.Drawing.Size(30, 25);
            _valueFromLabel.TabIndex = 4;
            _valueFromLabel.Text = "от:";
            // 
            // _valueToInput
            // 
            this._valueToInput.Dock = System.Windows.Forms.DockStyle.Left;
            this._valueToInput.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.UseSpecifiedMask;
            this._valueToInput.InputMask = "nnn,nnn,nnn";
            this._valueToInput.Location = new System.Drawing.Point(297, 0);
            this._valueToInput.Name = "_valueToInput";
            this._valueToInput.Size = new System.Drawing.Size(237, 20);
            this._valueToInput.TabIndex = 7;
            this._valueToInput.Text = "ultraMaskedEdit1";
            // 
            // _valueFromInput
            // 
            this._valueFromInput.Dock = System.Windows.Forms.DockStyle.Left;
            this._valueFromInput.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.UseSpecifiedMask;
            this._valueFromInput.InputMask = "nnn,nnn,nnn";
            this._valueFromInput.Location = new System.Drawing.Point(30, 0);
            this._valueFromInput.Margin = new System.Windows.Forms.Padding(2);
            this._valueFromInput.Name = "_valueFromInput";
            this._valueFromInput.Size = new System.Drawing.Size(237, 20);
            this._valueFromInput.TabIndex = 5;
            this._valueFromInput.Text = "ultraMaskedEdit1";
            // 
            // CurrencyRangeCriteriaEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._valueToInput);
            this.Controls.Add(_valueToLabel);
            this.Controls.Add(this._valueFromInput);
            this.Controls.Add(_valueFromLabel);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "CurrencyRangeCriteriaEditor";
            this.Size = new System.Drawing.Size(540, 25);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit _valueToInput;
        private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit _valueFromInput;
    }
}
