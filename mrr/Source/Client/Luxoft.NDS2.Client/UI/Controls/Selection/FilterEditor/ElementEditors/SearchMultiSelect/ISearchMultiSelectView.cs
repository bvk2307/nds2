﻿using Luxoft.NDS2.Client.UI.Controls.LookupSelector;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.ElementEditors
{
    public interface ISearchMultiSelectView : IElementView
    {
        ILookupSelectionView SelectionView { get; }

        ILookupSelectionEditView SelectionEditView { get; }
    }
}
