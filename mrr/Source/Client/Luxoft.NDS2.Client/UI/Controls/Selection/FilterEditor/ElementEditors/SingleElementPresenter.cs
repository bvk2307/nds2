﻿using Luxoft.NDS2.Common.Contracts.DTO;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.ElementEditors
{
    public abstract class SingleElementPresenter<TView> : ElementPresenterBase<TView>
        where TView : IElementView
    {
        protected SingleElementPresenter(TView view, List<FilterElement> model)
            : base(view, model)
        {
        }

        protected FilterElement Element
        {
            get
            {
                return Model.First();
            }
        }

        protected override void InitViewInternal()
        {
            if (!Model.Any())
            {
                Model.Add(Default());
            }

            ViewInitializing();
        }

        protected abstract FilterElement Default();

        protected abstract void ViewInitializing();
    }
}
