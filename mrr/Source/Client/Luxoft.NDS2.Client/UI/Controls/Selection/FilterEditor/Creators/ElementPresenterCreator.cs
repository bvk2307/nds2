﻿using Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.ElementEditors;
using Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.ElementEditors.ComparisonMultiSelect;
using Luxoft.NDS2.Client.UI.Controls.Selection.Models;
using Luxoft.NDS2.Common.Contracts.DTO;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.Creators
{
    public class ElementPresenterCreator : IElementPresenterCreator
    {
        # region Константы

        private const string CriteriaNotSupportedMessagePattern =
            "Selection filter criteria type {0} is not supported";

        private Dictionary<string, FilterElement.ComparisonOperations> numericExpressionComparisonTypes =
            new Dictionary<string, FilterElement.ComparisonOperations>
            {
                { "=", FilterElement.ComparisonOperations.Equals },
                { ">", FilterElement.ComparisonOperations.GT },
                { "<", FilterElement.ComparisonOperations.LT },
                { ">=", FilterElement.ComparisonOperations.GE },
                { "<=", FilterElement.ComparisonOperations.LE },
            };

        private Dictionary<string, FilterElement.ComparisonOperations> stringExpressionComparisonTypes =
            new Dictionary<string, FilterElement.ComparisonOperations>
            {
                { "Равно", FilterElement.ComparisonOperations.Equals },
                { "Начинается с", FilterElement.ComparisonOperations.StartWith },
                { "Заканчивается на", FilterElement.ComparisonOperations.EndWith },
                { "Содержит", FilterElement.ComparisonOperations.Contains }
            };

        private Dictionary<string, FilterElement.ComparisonOperations> _listExpressionComparisonTypes =
            new Dictionary<string, FilterElement.ComparisonOperations>
            {
                {"Равно", FilterElement.ComparisonOperations.Equals},
                {"Не равно", FilterElement.ComparisonOperations.NotEquals},
                {"Содержит", FilterElement.ComparisonOperations.Contains},
                {"Не содержит", FilterElement.ComparisonOperations.NotContains}
            };

        # endregion

        # region Поля

        private readonly ILookupDataSource _lookupDataSource;

        # endregion

        # region Конструктор

        public ElementPresenterCreator(ILookupDataSource lookupDataSource)
        {
            _lookupDataSource = lookupDataSource;
        }

        # endregion

        # region Реализация интерфейса IElementPresenterCreator

        public IElementPresenter Create(
            IElementView view,
            FilterCriteria model)
        {
            if (model.ValueTypeCode == FilterCriteria.ValueTypes.Edit)
            {
                return new PlainTextPresenter((IPlainTextView)view, model.Values);
            }

            if (model.ValueTypeCode == FilterCriteria.ValueTypes.List)
            {
                return new LookupSelectPresenter(
                    (ILookupSelectView)view,
                    _lookupDataSource.SimpleLookup(model.LookupTableName),
                    model.Values);
            }

            if (model.ValueTypeCode == FilterCriteria.ValueTypes.EditWithComparisonStr)
            {
                return new ComparisonTypeSelectPresenter(
                    (IComparisonTypeSelectView)view,
                    stringExpressionComparisonTypes,
                    model.Values);
            }

            if (model.ValueTypeCode == FilterCriteria.ValueTypes.EditWithComparisonNum)
            {
                return new ComparisonTypeSelectPresenter(
                    (IComparisonTypeSelectView)view,
                    numericExpressionComparisonTypes,
                    model.Values);
            }

            if (model.ValueTypeCode == FilterCriteria.ValueTypes.EditIntervalDates
                || model.ValueTypeCode == FilterCriteria.ValueTypes.EditIntervalInt
                || model.ValueTypeCode == FilterCriteria.ValueTypes.EditIntervalPrice)
            {
                return new RangePresenter((IRangeCriteriaView)view, model.Values);
            }

            if (model.ValueTypeCode == FilterCriteria.ValueTypes.ListMultpleChoice)
            {
                return new ListMultiSelectPresenter(
                    (IListMultiSelectView)view,
                    _lookupDataSource.SimpleLookup(model.LookupTableName),
                    model.Values);
            }

            if (model.ValueTypeCode == FilterCriteria.ValueTypes.RegionsMultiSelect)
            {
                return new SearchMultiSelectPresenter<RegionModel>(
                    (ISearchMultiSelectView)view,
                    (args) => _lookupDataSource.Search<RegionModel>(args),
                    (key, title) => new RegionModel() { Code = key, Name = title },
                    model.Values);
            }

            if (model.ValueTypeCode == FilterCriteria.ValueTypes.TaxAuthoritiesMultiSelect)
            {
                return new TaxAuthorityMultiSelectPresenter(
                    (ISearchMultiSelectView)view,
                    new TaxAuthorityDataLoader(x => _lookupDataSource.Search(x)),
                    (key, title) => new TaxAuthorityModel() { Code = key, Name = title },
                    model.Values);
            }

            if (model.ValueTypeCode == FilterCriteria.ValueTypes.TextMultipleInput)
            {
                return new MultipleTextItemsPresenter((IPlainTextView)view, model.Values);
            }

            if (model.ValueTypeCode == FilterCriteria.ValueTypes.ListMultipleCompirison)
            {
                return new ComparisonMultiSelectPresenter((IComparisonMultiSelectView)view,
                    _listExpressionComparisonTypes,
                    _lookupDataSource.SimpleLookup(model.LookupTableName),
                    model.Values);
            } 
            
            throw new NotSupportedException(
                string.Format(CriteriaNotSupportedMessagePattern, model.ValueTypeCode));
        }

        # endregion
    }
}
