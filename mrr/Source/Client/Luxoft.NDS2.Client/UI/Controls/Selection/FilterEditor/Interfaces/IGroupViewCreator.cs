﻿namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor
{
    public interface IGroupViewCreator
    {
        IGroupView Create();
    }
}
