﻿using Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.CriteriaEditor;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.Creators
{
    public class CriteriaViewCreator : ICriteriaViewCreator
    {
        private readonly IElementViewCreator _elementCreator =
            new ElementViewCreator();

        public ICriteriaView Create()
        {
            return new FilterCriteriaEditor(_elementCreator);
        }
    }
}
