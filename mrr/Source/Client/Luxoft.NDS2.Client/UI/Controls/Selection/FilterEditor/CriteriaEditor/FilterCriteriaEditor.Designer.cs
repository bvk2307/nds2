﻿namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.CriteriaEditor
{
    partial class FilterCriteriaEditor
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            this._table = new System.Windows.Forms.TableLayoutPanel();
            this._isActiveEdit = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this._criteriaTypeSelect = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this._add = new Infragistics.Win.Misc.UltraButton();
            this._deleteButton = new Infragistics.Win.Misc.UltraButton();
            this._table.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._criteriaTypeSelect)).BeginInit();
            this.SuspendLayout();
            // 
            // _table
            // 
            this._table.ColumnCount = 5;
            this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this._table.Controls.Add(this._isActiveEdit, 0, 0);
            this._table.Controls.Add(this._criteriaTypeSelect, 1, 0);
            this._table.Controls.Add(this._add, 3, 0);
            this._table.Controls.Add(this._deleteButton, 4, 0);
            this._table.Dock = System.Windows.Forms.DockStyle.Fill;
            this._table.Location = new System.Drawing.Point(0, 0);
            this._table.Margin = new System.Windows.Forms.Padding(0);
            this._table.Name = "_table";
            this._table.RowCount = 1;
            this._table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._table.Size = new System.Drawing.Size(847, 31);
            this._table.TabIndex = 1;
            // 
            // _isActiveEdit
            // 
            this._isActiveEdit.Checked = true;
            this._isActiveEdit.CheckState = System.Windows.Forms.CheckState.Checked;
            this._isActiveEdit.Dock = System.Windows.Forms.DockStyle.Top;
            this._isActiveEdit.Location = new System.Drawing.Point(3, 3);
            this._isActiveEdit.Name = "_isActiveEdit";
            this._isActiveEdit.Size = new System.Drawing.Size(14, 19);
            this._isActiveEdit.TabIndex = 0;
            // 
            // _criteriaTypeSelect
            // 
            this._criteriaTypeSelect.Dock = System.Windows.Forms.DockStyle.Top;
            this._criteriaTypeSelect.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this._criteriaTypeSelect.Location = new System.Drawing.Point(23, 3);
            this._criteriaTypeSelect.Name = "_criteriaTypeSelect";
            this._criteriaTypeSelect.Size = new System.Drawing.Size(222, 21);
            this._criteriaTypeSelect.TabIndex = 1;
            // 
            // _add
            // 
            appearance2.Image = global::Luxoft.NDS2.Client.Properties.Resources.list_add;
            appearance2.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance2.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this._add.Appearance = appearance2;
            this._add.Location = new System.Drawing.Point(785, 3);
            this._add.Name = "_add";
            this._add.Size = new System.Drawing.Size(25, 25);
            this._add.TabIndex = 8;
            // 
            // _deleteButton
            // 
            appearance1.Image = global::Luxoft.NDS2.Client.Properties.Resources.delete;
            appearance1.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance1.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this._deleteButton.Appearance = appearance1;
            this._deleteButton.Location = new System.Drawing.Point(816, 3);
            this._deleteButton.Name = "_deleteButton";
            this._deleteButton.Size = new System.Drawing.Size(25, 25);
            this._deleteButton.TabIndex = 9;
            // 
            // FilterCriteriaEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._table);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FilterCriteriaEditor";
            this.Size = new System.Drawing.Size(847, 31);
            this._table.ResumeLayout(false);
            this._table.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._criteriaTypeSelect)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel _table;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor _isActiveEdit;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor _criteriaTypeSelect;
        private Infragistics.Win.Misc.UltraButton _add;
        private Infragistics.Win.Misc.UltraButton _deleteButton;
    }
}
