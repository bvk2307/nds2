﻿using Luxoft.NDS2.Common.Contracts.DTO;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.ElementEditors
{
    public class RangePresenter : SingleElementPresenter<IRangeCriteriaView>
    {
        public RangePresenter(IRangeCriteriaView view, List<FilterElement> model)
            : base(view, model)
        {
        }

        protected override FilterElement Default()
        {
            return new FilterElement
            {
                Operator = FilterElement.ComparisonOperations.Between
            };
        }

        protected override void ViewInitializing()
        {
            View.FromValue = Element.ValueOne;
            View.ToValue = Element.ValueTwo;
        }

        protected override void UpdateModel()
        {
            Element.ValueOne = View.FromValue.Trim();
            Element.ValueTwo = View.ToValue.Trim();
        }

        protected override bool IsChanged()
        {
            return View.FromValue.Trim() != Element.ValueOne
                || View.ToValue.Trim() != Element.ValueTwo;
        }
    }
}
