﻿using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.ElementEditors.ComparisonMultiSelect
{
    public interface IComparisonMultiSelectView : IElementView
    {
        void InitLookup(Dictionary<string, string> options);

        void Select(string key);

        string[] SelectedKeys { get; }

        void SetComparisonOptions(List<string> comparisonTypes);

        string SelectedComparisonType { get; set; }
    }
}
