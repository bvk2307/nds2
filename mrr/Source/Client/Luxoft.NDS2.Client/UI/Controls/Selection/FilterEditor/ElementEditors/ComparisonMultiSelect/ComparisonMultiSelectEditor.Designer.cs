﻿namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.ElementEditors.ComparisonMultiSelect
{
    partial class ComparisonMultiSelectEditor
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Misc.UltraPanel _separator;
            this._optionsList = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this._optionsMultiSelect = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            _separator = new Infragistics.Win.Misc.UltraPanel();
            _separator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._optionsList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._optionsMultiSelect)).BeginInit();
            this.SuspendLayout();
            // 
            // _separator
            // 
            _separator.Dock = System.Windows.Forms.DockStyle.Left;
            _separator.Location = new System.Drawing.Point(137, 0);
            _separator.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            _separator.Name = "_separator";
            _separator.Size = new System.Drawing.Size(5, 25);
            _separator.TabIndex = 8;
            _separator.TabStop = false;
            // 
            // _optionsList
            // 
            this._optionsList.Dock = System.Windows.Forms.DockStyle.Left;
            this._optionsList.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this._optionsList.Location = new System.Drawing.Point(0, 0);
            this._optionsList.Margin = new System.Windows.Forms.Padding(2);
            this._optionsList.Name = "_optionsList";
            this._optionsList.Size = new System.Drawing.Size(137, 21);
            this._optionsList.TabIndex = 6;
            // 
            // _optionsMultiSelect
            // 
            this._optionsMultiSelect.CheckedListSettings.CheckBoxStyle = Infragistics.Win.CheckStyle.CheckBox;
            this._optionsMultiSelect.CheckedListSettings.EditorValueSource = Infragistics.Win.EditorWithComboValueSource.CheckedItems;
            this._optionsMultiSelect.CheckedListSettings.ItemCheckArea = Infragistics.Win.ItemCheckArea.Item;
            this._optionsMultiSelect.CheckedListSettings.ListSeparator = " / ";
            this._optionsMultiSelect.Dock = System.Windows.Forms.DockStyle.Fill;
            this._optionsMultiSelect.Location = new System.Drawing.Point(142, 0);
            this._optionsMultiSelect.Margin = new System.Windows.Forms.Padding(2);
            this._optionsMultiSelect.Name = "_optionsMultiSelect";
            this._optionsMultiSelect.Size = new System.Drawing.Size(398, 21);
            this._optionsMultiSelect.TabIndex = 9;
            // 
            // ComparisonMultiSelectEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._optionsMultiSelect);
            this.Controls.Add(_separator);
            this.Controls.Add(this._optionsList);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "ComparisonMultiSelectEditor";
            this.Size = new System.Drawing.Size(540, 25);
            _separator.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._optionsList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._optionsMultiSelect)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.UltraWinEditors.UltraComboEditor _optionsList;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor _optionsMultiSelect;

    }
}
