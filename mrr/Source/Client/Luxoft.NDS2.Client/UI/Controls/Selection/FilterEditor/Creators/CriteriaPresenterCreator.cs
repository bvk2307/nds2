﻿using Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.CriteriaEditor;
using Luxoft.NDS2.Common.Contracts.DTO;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.Creators
{
    public class CriteriaPresenterCreator : ICriteriaPresenterCreator
    {
        private readonly IElementPresenterCreator _elementPresenterCreator;

        private readonly List<FilterCriteria> _allCriterias;

        public CriteriaPresenterCreator(
            IElementPresenterCreator elementPresenterCreator,
            List<FilterCriteria> allCriterias)
        {
            _elementPresenterCreator = elementPresenterCreator;
            _allCriterias = allCriterias;
        }

        public ICriteriaPresenter Create(ICriteriaView view, FilterCriteria model)
        {
            return new CriteriaPresenter(
                view,
                _elementPresenterCreator,
                model);
        }

        public FilterCriteria[] DefaultModel()
        {
            if (_allCriterias.All(x => !x.IsRequired))
            {
                return new FilterCriteria[]
                {
                    _allCriterias[0].CloneAsActive()
                };
            }
            else
            {
                return _allCriterias
                    .Where(x => x.IsRequired)
                    .Select(x => x.CloneAsActive())
                    .ToArray();
            }
        }

        public FilterCriteria DefaultCriteria()
        {
            return _allCriterias[0].CloneAsActive();
        }
    }
}
