﻿using Luxoft.NDS2.Common.Contracts.DTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.ElementEditors
{
    public class ListMultiSelectPresenter : ElementPresenterBase<IListMultiSelectView>
    {
        private readonly Func<Dictionary<string, string>> _lookupItemsSource;

        public ListMultiSelectPresenter(
            IListMultiSelectView view,
            Func<Dictionary<string, string>> lookupItemsSource,
            List<FilterElement> model)
            : base(view, model)
        {
            _lookupItemsSource = lookupItemsSource;
        }

        protected override void InitViewInternal()
        {
            var options = _lookupItemsSource();

            View.InitLookup(options);

            foreach (var element in Model)
            {
                if (options.ContainsKey(element.ValueOne))
                {
                    View.Select(element.ValueOne);
                }
            }
        }

        protected override void UpdateModel()
        {
            Model.Clear();
            Model.AddRange(
                View.SelectedKeys.Select(
                    x => new FilterElement()
                    {
                        Operator = FilterElement.ComparisonOperations.Equals,
                        ValueOne = x,
                        ValueTwo = string.Empty
                    }));
        }

        protected override bool IsChanged()
        {
            return Model.Count != View.SelectedKeys.Count()
                || Model.Any(m => View.SelectedKeys.All(vw => m.ValueOne != vw));
        }
    }
}
