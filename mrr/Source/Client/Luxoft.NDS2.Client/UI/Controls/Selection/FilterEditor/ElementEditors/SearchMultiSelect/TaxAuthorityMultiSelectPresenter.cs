﻿using Luxoft.NDS2.Client.UI.Controls.LookupSelector;
using Luxoft.NDS2.Client.UI.Controls.Selection.Models;
using Luxoft.NDS2.Common.Contracts.DTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.ElementEditors
{
    public class TaxAuthorityMultiSelectPresenter : SearchMultiSelectPresenter<TaxAuthorityModel>, IRegionDependentElement
    {
        private readonly TaxAuthorityDataLoader _dataLoader;

        public TaxAuthorityMultiSelectPresenter(
           ISearchMultiSelectView view,
           TaxAuthorityDataLoader searchDataSource,
           Func<string, string, TaxAuthorityModel> lookupEntryCreator,
           List<FilterElement> model)
            : base(
                view, 
                (args) => searchDataSource.Load(args),
                lookupEntryCreator, 
                model)
        {
            _dataLoader = searchDataSource;
        }

        public void SetRegions(string[] regionCodes, bool restrictByRegion)
        {
            _dataLoader.RestrictByRegion = restrictByRegion;
            _dataLoader.RegionCodes = regionCodes;

            if (!restrictByRegion)
            {
                return;
            }

            var valuesToDelete =
                Model.Where(
                    val => regionCodes.All(regionCode => !val.ValueOne.StartsWith(regionCode)));

            foreach (var taxAuthority in Model)
            {
                if (!regionCodes.Any(regionCode => taxAuthority.ValueOne.StartsWith(regionCode)))
                {
                    ChildPresenter.SelectedItems.Remove(taxAuthority.ValueOne);
                }
            }
            ChildPresenter.RefreshViewer();
            UpdateModel();
        }
    }
}
