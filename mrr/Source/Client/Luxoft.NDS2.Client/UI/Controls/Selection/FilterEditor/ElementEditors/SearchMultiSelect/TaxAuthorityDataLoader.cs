﻿using Luxoft.NDS2.Client.UI.Controls.LookupSelector;
using Luxoft.NDS2.Client.UI.Controls.Selection.Models;
using System;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.ElementEditors
{
    public class TaxAuthorityDataLoader : IDataLoader<TaxAuthorityModel>
    {
        private readonly Func<ITaxAuthorityRequestArgs, IDataRequestResult<TaxAuthorityModel>> _dataLoader;

        public TaxAuthorityDataLoader(
            Func<ITaxAuthorityRequestArgs, IDataRequestResult<TaxAuthorityModel>> dataLoader)
        {
            _dataLoader = dataLoader;
        }

        public string[] RegionCodes
        {
            get;
            set;
        }

        public bool RestrictByRegion
        {
            get;
            set;
        }

        public IDataRequestResult<TaxAuthorityModel> Load(IDataRequestArgs args)
        {
            return _dataLoader(
                new TaxAuthorityRequestArgs(args)
                {
                    RegionCodes = RegionCodes,
                    RestrictByRegion = RestrictByRegion
                });
        }

        private class TaxAuthorityRequestArgs : ITaxAuthorityRequestArgs
        {

            public TaxAuthorityRequestArgs(IDataRequestArgs args)
            {
                SearchPattern = args.SearchPattern;
                MaxQuantity = args.MaxQuantity;
            }

            public bool RestrictByRegion
            {
                get;
                set;
            }

            public string[] RegionCodes
            {
                get;
                set;
            }

            public string SearchPattern
            {
                get;
                private set;
            }

            public int MaxQuantity
            {
                get;
                private set;
            }
        }
    }
}
