﻿namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.ElementEditors
{
    partial class LookupSelectFilterCriteriaEditor
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this._listSelect = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            ((System.ComponentModel.ISupportInitialize)(this._listSelect)).BeginInit();
            this.SuspendLayout();
            // 
            // _listSelect
            // 
            this._listSelect.Dock = System.Windows.Forms.DockStyle.Fill;
            this._listSelect.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this._listSelect.Location = new System.Drawing.Point(0, 0);
            this._listSelect.Margin = new System.Windows.Forms.Padding(2);
            this._listSelect.Name = "_listSelect";
            this._listSelect.Size = new System.Drawing.Size(540, 21);
            this._listSelect.TabIndex = 4;
            // 
            // LookupSelectFilterCriteriaEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._listSelect);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "LookupSelectFilterCriteriaEditor";
            this.Size = new System.Drawing.Size(540, 25);
            ((System.ComponentModel.ISupportInitialize)(this._listSelect)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.UltraWinEditors.UltraComboEditor _listSelect;
    }
}
