﻿using Luxoft.NDS2.Common.Contracts.DTO;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.CriteriaDependencies
{
    public interface IDependencyRule
    {
        bool IsApplicable(GroupFilter currentState, FilterCriteria criteria);
    }
}
