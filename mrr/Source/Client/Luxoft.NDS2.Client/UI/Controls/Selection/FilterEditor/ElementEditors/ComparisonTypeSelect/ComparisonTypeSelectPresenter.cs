﻿using Luxoft.NDS2.Common.Contracts.DTO;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.ElementEditors
{
    public class ComparisonTypeSelectPresenter : SingleElementPresenter<IComparisonTypeSelectView>
    {
        private Dictionary<string, FilterElement.ComparisonOperations> _availableOperations;

        public ComparisonTypeSelectPresenter(            
            IComparisonTypeSelectView view,
            Dictionary<string, FilterElement.ComparisonOperations> availableOperations,
            List<FilterElement> model
            )
            : base(view, model)
        {
            _availableOperations = availableOperations;
        }

        protected override FilterElement Default()
        {
            return new FilterElement
            {
                Operator = _availableOperations.First().Value
            };
        }

        protected override void ViewInitializing()
        {
            View.ComparisonExpression = Element.ValueOne;
            View.SetComparisonOptions(_availableOperations.Select(x => x.Key).ToList());            

            if (_availableOperations.ContainsValue(Element.Operator))
            {
                View.SelectedComparisonType = 
                    _availableOperations.First(x => x.Value == Element.Operator).Key;
            }
        }

        protected override void UpdateModel()
        {
            Element.ValueOne = View.ComparisonExpression;
            Element.ValueTwo = string.Empty;
            Element.Operator = _availableOperations[View.SelectedComparisonType];
        }

        protected override bool IsChanged()
        {
            return Element.ValueOne != View.ComparisonExpression
                || Element.Operator != _availableOperations[View.SelectedComparisonType];
        }
    }
}
