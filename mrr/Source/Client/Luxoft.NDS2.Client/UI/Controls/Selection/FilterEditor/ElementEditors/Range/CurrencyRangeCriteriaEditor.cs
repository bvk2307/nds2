﻿using Luxoft.NDS2.Client.UI.Base;
using System;
using System.Globalization;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.ElementEditors
{
    public partial class CurrencyRangeCriteriaEditor : UserControl, IRangeCriteriaView
    {
        private const string NullDecimal = "0";
        private CultureInfo decimalFormatProvider = new CultureInfo("en-GB");

        public CurrencyRangeCriteriaEditor()
        {
            InitializeComponent();
            _valueFromInput.ValueChanged += OnValueChanged;
            _valueToInput.ValueChanged += OnValueChanged;
        }

        public string FromValue
        {
            get
            {
                return _valueFromInput.Value is DBNull
                    ? NullDecimal
                    : _valueFromInput.Value.ToString();
            }
            set
            {
                _valueFromInput.Value = value;
            }
        }

        public string ToValue
        {
            get
            {
                return _valueToInput.Value is DBNull
                    ? NullDecimal
                    : _valueToInput.Value.ToString();
            }
            set
            {
                _valueToInput.Value = value;
            }
        }

        public event ParameterlessEventHandler ValueChanged;

        private void OnValueChanged(object sender, EventArgs e)
        {
            if (ValueChanged != null)
            {
                ValueChanged();
            }
            GenerateAfterValueChange();
        }

        public event ParameterlessEventHandler AfterValueChanged;

        private void GenerateAfterValueChange()
        {
            if (AfterValueChanged != null)
            {
                AfterValueChanged();
            }
        }
    }
}
