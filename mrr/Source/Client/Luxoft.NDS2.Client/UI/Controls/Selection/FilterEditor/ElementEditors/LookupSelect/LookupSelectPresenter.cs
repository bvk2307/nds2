﻿using Luxoft.NDS2.Common.Contracts.DTO;
using System;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.ElementEditors
{
    public class LookupSelectPresenter : SingleElementPresenter<ILookupSelectView>
    {
        private readonly Func<Dictionary<string, string>> _lookupItemsSource;

        public LookupSelectPresenter(
            ILookupSelectView view,
            Func<Dictionary<string, string>> lookupItemsSource,
            List<FilterElement> model)
            : base(view, model)
        {
            _lookupItemsSource = lookupItemsSource;
        }

        protected override FilterElement Default()
        {
            return new FilterElement
            {
                Operator = FilterElement.ComparisonOperations.Equals
            };
        }

        protected override void ViewInitializing()
        {
            var options = _lookupItemsSource();

            View.InitLookup(options);

            if (
                !string.IsNullOrWhiteSpace(Element.ValueOne)
                && options.ContainsKey(Element.ValueOne))
            {
                View.SelectedOption = Element.ValueOne;
            }
        }

        protected override void UpdateModel()
        {
            Element.ValueOne = View.SelectedOption ?? string.Empty;
            Element.ValueTwo = string.Empty;
        }

        protected override bool IsChanged()
        {
            return (View.SelectedOption != null || Element.ValueOne != string.Empty)
                && View.SelectedOption != Element.ValueOne;
        }
    }
}
