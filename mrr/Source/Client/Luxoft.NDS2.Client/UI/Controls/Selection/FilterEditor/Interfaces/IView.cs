﻿namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor
{
    public interface IView
    {
        IGroupView NewGroup();

        void Draw(IGroupView[] groups);
    }
}
