﻿using Luxoft.NDS2.Common.Contracts.DTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.ElementEditors
{
    public class MultipleTextItemsPresenter : PlainTextPresenter
    {
        private const string CommaSeparator = ",";

        public MultipleTextItemsPresenter(IPlainTextView view, List<FilterElement> model)
            : base(view, model)
        {
        }

        protected override void ViewInitializing()
        {
            View.Text = string.Join(CommaSeparator, Model.Select(x => x.ValueOne).ToArray());
        }

        protected override void UpdateModel()
        {
            Model.Clear();
            Model.AddRange(
                View.Text
                    .Split(new [] { CommaSeparator }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(item => 
                        new FilterElement
                        { 
                            Operator = FilterElement.ComparisonOperations.Equals,
                            ValueOne = item.Trim()
                        }));
        }

        protected override bool IsChanged()
        {
            return string.Join(CommaSeparator, Model.Select(x => x.ValueOne).ToArray())
                != View.Text;
        }
    }
}
