﻿using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Common.Contracts.DTO;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.CriteriaEditor
{
    public class CriteriaPresenter : ICriteriaPresenter
    {
        private readonly ICriteriaView _view;

        private readonly IElementPresenterCreator _elementPresenterCreator;

        private FilterCriteria _model;

        private List<FilterCriteria> _allCriterias;

        public CriteriaPresenter(
            ICriteriaView view,
            IElementPresenterCreator elementPresenterCreator,
            FilterCriteria model)
        {
            _view = view;
            _elementPresenterCreator = elementPresenterCreator;
            _model = model;
            _model.Values = _model.Values ?? new List<FilterElement>();
            _allCriterias = new List<FilterCriteria>();
        }

        public void Init(List<FilterCriteria> allCriterias)
        {
            InitElementPresenter();
            SetCriterias(allCriterias);

            _view.IsActive = _model.IsActive;

            _view.TypeSelectView.CriteriaTypeChanged +=
                (name) =>
                {
                    if (name != _model.Name)
                    {
                        AlterCriteriaType(name);
                    }
                };
            _view.ActivityChanged += () =>
                {
                    _model.IsActive = _view.IsActive;
                    RaiseAfterCriteriaChanged();
                };
        }

        public void SetCriterias(List<FilterCriteria> allCriterias)
        {
            if (allCriterias.Count != _allCriterias.Count
                || allCriterias.Any(
                    nw => _allCriterias.All(curr => curr.InternalName != nw.InternalName)))
            {
                _allCriterias = allCriterias;
                _view.TypeSelectView.SetOptions(
                    _allCriterias.Select(x => x.Name).ToList(),
                    _model.Name);
            }
        }

        public IElementPresenter ElementPresenter
        {
            get;
            private set;
        }

        private void AlterCriteriaType(string newCriteria)
        {
            var metadata = _allCriterias.First(x => x.Name == newCriteria);

            _model.Name = newCriteria;
            _model.FirInternalName = metadata.FirInternalName;
            _model.InternalName = metadata.InternalName;
            _model.IsRequired = metadata.IsRequired;
            _model.LookupTableName = metadata.LookupTableName;
            _model.ValueTypeCode = metadata.ValueTypeCode;
            _model.Values = new List<FilterElement>();

            InitElementPresenter();

            if (AfterCriteriaTypeChanged != null)
            {
                AfterCriteriaTypeChanged();
            }

            RaiseAfterCriteriaChanged();
        }

        private void InitElementPresenter()
        {
            ElementPresenter =
                _elementPresenterCreator.Create(
                    _view.ShowValueEditor(_model.ValueTypeCode),
                    _model);

            ElementPresenter.InitView();
            ElementPresenter.AfterValueChanged += RaiseAfterCriteriaChanged;
        }

        private void RaiseAfterCriteriaChanged()
        {
            if (AfterCriteriaChanged != null)
            {
                AfterCriteriaChanged();
            }
        }

        public event ParameterlessEventHandler AfterCriteriaTypeChanged;

        public event ParameterlessEventHandler AfterCriteriaChanged;
    }
}
