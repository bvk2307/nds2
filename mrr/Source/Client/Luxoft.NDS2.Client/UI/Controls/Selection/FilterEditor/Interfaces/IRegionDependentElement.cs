﻿namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor
{
    public interface IRegionDependentElement
    {
        void SetRegions(string[] regionCodes, bool restrictByRegion);
    }
}
