﻿using System;
using System.Globalization;
using System.Windows.Forms;
using Luxoft.NDS2.Client.UI.Base;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.ElementEditors
{
    public partial class DateRangeCriteriaEditor : UserControl, IRangeCriteriaView
    {
        private CultureInfo ciRUS = new CultureInfo("ru-RU", false);

        public DateRangeCriteriaEditor()
        {
            InitializeComponent();
            _dateFromInput.ValueChanged += OnValueChanged;
            _dateToInput.ValueChanged += OnValueChanged;            
        }

        public string FromValue
        {
            get
            {
                DateTime dt = Convert.ToDateTime(_dateFromInput.Value);
                return dt.Date.ToString(ciRUS);
            }
            set
            {
                _dateFromInput.Value = Convert.ToDateTime(value);
            }
        }

        public string ToValue
        {
            get
            {
                DateTime dt = Convert.ToDateTime(_dateToInput.Value);
                return dt.Date.AddDays(1).AddMilliseconds(-1).ToString(ciRUS);
            }
            set
            {
                _dateToInput.Value = Convert.ToDateTime(value);
            }
        }

        public event ParameterlessEventHandler ValueChanged;

        private void OnValueChanged(object sender, EventArgs e)
        {
            if (ValueChanged != null)
            {
                ValueChanged();
                GenerateAfterValueChange();
            }
        }

        public event ParameterlessEventHandler AfterValueChanged;

        private void GenerateAfterValueChange()
        {
            if (AfterValueChanged != null)
            {
                AfterValueChanged();
            }
        }
    }
}
