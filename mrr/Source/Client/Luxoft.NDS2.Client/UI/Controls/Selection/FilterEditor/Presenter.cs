﻿using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.Creators;
using Luxoft.NDS2.Common.Contracts.DTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor
{
    /// <summary>
    /// Этот класс реализует Presenter пользовательского элемента редактирования фильтра выборки
    /// </summary>
    public class Presenter
    {
        # region Поля

        private readonly IView _view;

        private List<GroupFilter> _model;

        private readonly IGroupPresenterCreator _childCreator;

        private readonly List<IGroupView> _childViews =
            new List<IGroupView>();

        private readonly List<IGroupPresenter> _childPresenters =
            new List<IGroupPresenter>();

        private readonly Func<GroupFilter> _defaulGroupCreator;            

        # endregion

        # region Конструктор

        /// <summary>
        /// Создает новый экземпляр класса Presenter
        /// </summary>
        /// <param name="view">Ссылка на реализацию пользовательского жлемента управления</param>
        /// <param name="model">Ссылка на модель данных. Список групп фильтров выборки данных о расхождениях</param>
        /// <param name="groupCreator">Ссылка на фабрику дочерних презенторов, представляющих 1 группу фильтра</param>
        public Presenter(
            IView view,
            List<GroupFilter> model,
            IGroupPresenterCreator groupCreator,
            Func<GroupFilter> defaulGroupCreator)
        {
            _view = view;
            _model = model;
            _childCreator = groupCreator;
            _defaulGroupCreator = defaulGroupCreator;

            if (!_model.Any())
            {
                _model.Add(defaulGroupCreator());
            }
        }

        # endregion

        # region Интерфейс

        /// <summary>
        /// Инициализирует представление, заполняя его данными фильтра.
        /// Необходимо вызвать в тот момент, когда нужно отобразить представление и источник данных доступен
        /// </summary>
        public void Init()
        {
            for (var idx = 0; idx < _model.Count; idx++)
            {
                AddGroupPresenter(
                    NewGroupView(idx),
                    _model[idx],
                    idx);
            }

            RefreshActions();
            RefreshView();
        }

        public void Reset(List<GroupFilter> newGroups)
        {
            _childPresenters.Clear();
            _childViews.Clear();
            _model = newGroups;

            Init();

            RaiseAfterFilterChanged();
        }

        public event ParameterlessEventHandler AfterFilterChanged;

        # endregion

        # region Приватные методы

        private IGroupView NewGroupView(int index)
        {
            var groupView = _view.NewGroup();
            groupView.Index = index;

            groupView.Adding += () => AddAfter(groupView);
            groupView.Deleting += () => Remove(groupView);
            _childViews.Insert(index, groupView);

            return groupView;
        }

        private void AddGroupPresenter(IGroupView view, GroupFilter model, int index)
        {
            var groupPresenter = _childCreator.Create(view, model);

            groupPresenter.Init();
            _childPresenters.Insert(index, groupPresenter);
            RaiseAfterFilterChanged();
        }

        private void AddAfter(IGroupView view)
        {
            foreach(var lowerGroup in _childViews.Where(vw => vw.Index > view.Index))
            {
                lowerGroup.Index++;
            }

            var newGroup = _defaulGroupCreator();
            _model.Insert(view.Index + 1, newGroup);

            AddGroupPresenter(
                NewGroupView(view.Index + 1),
                newGroup,
                view.Index);

            RefreshActions();
            RefreshView();
            RaiseAfterFilterChanged();
        }

        private void Remove(IGroupView view)
        {
            foreach(var group in _childViews.Where(vw => vw.Index > view.Index))
            {
                group.Index--;
            }

            _childViews.RemoveAt(view.Index);
            _childPresenters.RemoveAt(view.Index);
            _model.RemoveAt(view.Index);

            RefreshActions();
            RefreshView();
        }

        private void RefreshView()
        {
            _view.Draw(_childViews.ToArray());
        }

        private void RefreshActions()
        {
            var allowRemove = _childViews.Count > 1;

            foreach (var view in _childViews)
            {
                view.AllowRemove = allowRemove;
            }
        }

        private void RaiseAfterFilterChanged()
        {
            if (AfterFilterChanged != null)
            {
                AfterFilterChanged();
            }
        }

        # endregion
    }
}
