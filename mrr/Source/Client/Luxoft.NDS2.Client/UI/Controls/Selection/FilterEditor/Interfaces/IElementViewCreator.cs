﻿using Luxoft.NDS2.Common.Contracts.DTO;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor
{
    public interface IElementViewCreator
    {
        IElementView Create(FilterCriteria.ValueTypes type);
    }
}
