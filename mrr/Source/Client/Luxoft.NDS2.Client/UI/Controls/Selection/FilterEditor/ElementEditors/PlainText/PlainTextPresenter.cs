﻿using Luxoft.NDS2.Common.Contracts.DTO;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.ElementEditors
{
    public class PlainTextPresenter : SingleElementPresenter<IPlainTextView>
    {
        public PlainTextPresenter(IPlainTextView view, List<FilterElement> model)
            : base(view, model)
        {           
        }

        protected override FilterElement Default()
        {
            return new FilterElement
            {
                Operator = FilterElement.ComparisonOperations.Equals
            };
        }

        protected override void ViewInitializing()
        {
            View.Text = Element.ValueOne;
        }

        protected override void UpdateModel()
        {
            Element.ValueOne = View.Text.Trim();
            Element.ValueTwo = string.Empty;
        }

        protected override bool IsChanged()
        {
            return Element.ValueOne != View.Text.Trim();
        }
    }
}
