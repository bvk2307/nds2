﻿namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.ElementEditors
{
    partial class PlainTextFilterCriteriaEditor
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this._textInput = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            ((System.ComponentModel.ISupportInitialize)(this._textInput)).BeginInit();
            this.SuspendLayout();
            // 
            // _textInput
            // 
            this._textInput.Dock = System.Windows.Forms.DockStyle.Fill;
            this._textInput.Location = new System.Drawing.Point(0, 0);
            this._textInput.Margin = new System.Windows.Forms.Padding(2);
            this._textInput.Name = "_textInput";
            this._textInput.Size = new System.Drawing.Size(540, 21);
            this._textInput.TabIndex = 3;
            // 
            // PlainTextFilterCriteriaEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._textInput);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "PlainTextFilterCriteriaEditor";
            this.Size = new System.Drawing.Size(540, 25);
            ((System.ComponentModel.ISupportInitialize)(this._textInput)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.UltraWinEditors.UltraTextEditor _textInput;
    }
}
