﻿using Luxoft.NDS2.Common.Contracts.DTO;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor
{
    public interface IGroupPresenterCreator
    {
        IGroupPresenter Create(IGroupView view, GroupFilter model);
    }
}
