﻿using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Common.Contracts.DTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.ElementEditors
{
    public abstract class ElementPresenterBase<TView> : IElementPresenter
        where TView : IElementView
    {
        protected List<FilterElement> Model
        {
            get;
            private set;
        }

        protected TView View
        {
            get;
            private set;
        }

        protected ElementPresenterBase(TView view, List<FilterElement> model)
        {
            if (view == null)
            {
                throw new ArgumentNullException("view");
            }

            if (model == null)
            {
                throw new ArgumentNullException("model");
            }

            View = view;
            Model = model;
        }

        public void InitView()
        {
            InitViewInternal();

            UpdateModel();
            View.ValueChanged += OnValueChanged;
        }

        private void OnValueChanged()
        {
            if (IsChanged())
            {
                UpdateModel();

                if (AfterValueChanged != null)
                {
                    AfterValueChanged();
                }
            }
        }

        protected abstract void InitViewInternal();

        protected abstract void UpdateModel();

        protected abstract bool IsChanged();

        public event ParameterlessEventHandler AfterValueChanged;
    }
}
