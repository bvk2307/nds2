﻿namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.ElementEditors
{
    partial class SearchMultiSelectView
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this._lookupMultiSelect = new Luxoft.NDS2.Client.UI.Controls.LookupSelector.Implementation.MultiSelect.LookupMultiSelectView();
            this.SuspendLayout();
            // 
            // _lookupMultiSelect
            // 
            this._lookupMultiSelect.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lookupMultiSelect.Location = new System.Drawing.Point(0, 0);
            this._lookupMultiSelect.Margin = new System.Windows.Forms.Padding(2);
            this._lookupMultiSelect.Name = "_lookupMultiSelect";
            this._lookupMultiSelect.PopupHeight = 140;
            this._lookupMultiSelect.Size = new System.Drawing.Size(540, 25);
            this._lookupMultiSelect.TabIndex = 0;
            // 
            // SearchMultiSelectView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._lookupMultiSelect);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "SearchMultiSelectView";
            this.Size = new System.Drawing.Size(540, 25);
            this.ResumeLayout(false);

        }

        #endregion

        private LookupSelector.Implementation.MultiSelect.LookupMultiSelectView _lookupMultiSelect;
    }
}
