﻿using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.LookupSelector;
using System;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.ElementEditors
{
    public partial class SearchMultiSelectView : UserControl, ISearchMultiSelectView
    {
        public SearchMultiSelectView()
        {
            InitializeComponent();
            _lookupMultiSelect.OnAfterSelectionChanged += OnValueChanged;
            _lookupMultiSelect.OnSave += _lookupMultiSelect_OnSave;
        }

        public ILookupSelectionView SelectionView
        {
            get { return _lookupMultiSelect; }
        }

        public ILookupSelectionEditView SelectionEditView
        {
            get { return _lookupMultiSelect; }
        }

        public event ParameterlessEventHandler ValueChanged;

        private void GenerateValueChanged()
        {
            if (ValueChanged != null)
            {
                ValueChanged();
            }
            GenerateAfterValueChange();
        }

        private void OnValueChanged()
        {
            GenerateValueChanged();
        }

        private void _lookupMultiSelect_OnSave()
        {
            GenerateValueChanged();
        }

        public event ParameterlessEventHandler AfterValueChanged;

        private void GenerateAfterValueChange()
        {
            if (AfterValueChanged != null)
            {
                AfterValueChanged();
            }
        }
    }
}
