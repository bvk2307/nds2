﻿namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.GroupEditor
{
    partial class FilterGroupEditor
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            this._table = new System.Windows.Forms.TableLayoutPanel();
            this._isActiveInput = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this._addButton = new Infragistics.Win.Misc.UltraButton();
            this._removeButton = new Infragistics.Win.Misc.UltraButton();
            this._table.SuspendLayout();
            this.SuspendLayout();
            // 
            // _table
            // 
            this._table.AutoSize = true;
            this._table.ColumnCount = 4;
            this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this._table.Controls.Add(this._isActiveInput, 0, 0);
            this._table.Controls.Add(this._addButton, 2, 0);
            this._table.Controls.Add(this._removeButton, 3, 0);
            this._table.Dock = System.Windows.Forms.DockStyle.Fill;
            this._table.Location = new System.Drawing.Point(0, 0);
            this._table.Name = "_table";
            this._table.RowCount = 1;
            this._table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._table.Size = new System.Drawing.Size(669, 28);
            this._table.TabIndex = 1;
            // 
            // _isActiveInput
            // 
            this._isActiveInput.Checked = true;
            this._isActiveInput.CheckState = System.Windows.Forms.CheckState.Checked;
            this._isActiveInput.Dock = System.Windows.Forms.DockStyle.Top;
            this._isActiveInput.Location = new System.Drawing.Point(3, 3);
            this._isActiveInput.Name = "_isActiveInput";
            this._isActiveInput.Size = new System.Drawing.Size(14, 22);
            this._isActiveInput.TabIndex = 0;
            this._isActiveInput.Text = "ultraCheckEditor1";
            // 
            // _addButton
            // 
            appearance1.Image = global::Luxoft.NDS2.Client.Properties.Resources.list_add;
            appearance1.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance1.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this._addButton.Appearance = appearance1;
            this._addButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this._addButton.Location = new System.Drawing.Point(611, 2);
            this._addButton.Margin = new System.Windows.Forms.Padding(2);
            this._addButton.Name = "_addButton";
            this._addButton.Size = new System.Drawing.Size(26, 24);
            this._addButton.TabIndex = 1;
            // 
            // _removeButton
            // 
            appearance2.Image = global::Luxoft.NDS2.Client.Properties.Resources.delete;
            appearance2.ImageHAlign = Infragistics.Win.HAlign.Center;
            appearance2.ImageVAlign = Infragistics.Win.VAlign.Middle;
            this._removeButton.Appearance = appearance2;
            this._removeButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this._removeButton.Location = new System.Drawing.Point(642, 3);
            this._removeButton.Name = "_removeButton";
            this._removeButton.Size = new System.Drawing.Size(24, 22);
            this._removeButton.TabIndex = 2;
            // 
            // FilterGroupEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this._table);
            this.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.Name = "FilterGroupEditor";
            this.Size = new System.Drawing.Size(669, 28);
            this._table.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.UltraWinEditors.UltraCheckEditor _isActiveInput;
        private Infragistics.Win.Misc.UltraButton _addButton;
        private System.Windows.Forms.TableLayoutPanel _table;
        private Infragistics.Win.Misc.UltraButton _removeButton;
    }
}
