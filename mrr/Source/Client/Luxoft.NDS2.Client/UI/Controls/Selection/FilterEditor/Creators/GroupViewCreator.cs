﻿using Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.GroupEditor;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.Creators
{
    public class GroupViewCreator : IGroupViewCreator
    {
        private readonly ICriteriaViewCreator _criteriaCreator =
            new CriteriaViewCreator();

        public IGroupView Create()
        {
            return new FilterGroupEditor(_criteriaCreator);
        }
    }
}
