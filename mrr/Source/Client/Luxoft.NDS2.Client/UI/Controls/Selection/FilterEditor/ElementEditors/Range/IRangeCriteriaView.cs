﻿namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.ElementEditors
{
    public interface IRangeCriteriaView : IElementView
    {
        string FromValue {  get; set; }

        string ToValue { get; set; }
    }
}
