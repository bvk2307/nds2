﻿namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.ElementEditors
{
    public interface IPlainTextView : IElementView
    {
        string Text
        {
            get;
            set;
        }
    }
}
