﻿using Luxoft.NDS2.Common.Contracts.DTO;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor
{
    public interface ICriteriaPresenterCreator
    {
        FilterCriteria[] DefaultModel();

        FilterCriteria DefaultCriteria();

        ICriteriaPresenter Create(ICriteriaView view,FilterCriteria model);
    }
}
