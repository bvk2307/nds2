﻿using Luxoft.NDS2.Common.Contracts.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.CriteriaEditor
{
    public partial class FilterCriteriaEditor : UserControl, ICriteriaTypeSelectView, ICriteriaView
    {
        private const int ElementViewColumnIndex = 2;

        private readonly IElementViewCreator _viewCreator;

        private IElementView _view;

        public FilterCriteriaEditor()
        {
            InitializeComponent();
        }

        public FilterCriteriaEditor(IElementViewCreator viewCreator)
            : this()
        {
            _viewCreator = viewCreator;

            _criteriaTypeSelect.ValueChanged +=
                (sender, args) =>
                {
                    if (CriteriaTypeChanged != null)
                    {
                        CriteriaTypeChanged(_criteriaTypeSelect.Value.ToString());
                    }
                };

            _add.Click +=
                (sender, args) =>
                {
                    if (Adding != null)
                    {
                        Adding();
                    }
                };

            _deleteButton.Click +=
                (sender, args) =>
                {
                    if (Deleting != null)
                    {
                        Deleting();
                    }
                };

            _isActiveEdit.CheckedChanged +=
                (sender, args) =>
                {
                    if (ActivityChanged != null)
                    {
                        ActivityChanged();
                    }
                };
        }

        public void SetOptions(List<string> criteriaTypes, string defaultOption)
        {
            _criteriaTypeSelect.DataSource = criteriaTypes;

            foreach (var item in _criteriaTypeSelect.Items)
            {
                if (item.DisplayText == defaultOption)
                {
                    _criteriaTypeSelect.SelectedItem = item;
                }
            }
        }        

        public ICriteriaTypeSelectView TypeSelectView
        {
            get { return this; }
        }

        public IElementView ShowValueEditor(FilterCriteria.ValueTypes type)
        {
            if (_view != null)
            {
                _table.Controls.Remove((Control)_view);
            }

            _view = _viewCreator.Create(type);
            var control = (Control)_view;
            control.Dock = DockStyle.Top;
            control.Margin = new Padding(_criteriaTypeSelect.Margin.All);

            _table.Controls.Add(control, ElementViewColumnIndex, 0);

            return _view;
        }

        public bool IsActive
        {
            get
            {
                return _isActiveEdit.Checked;
            }
            set
            {
                _isActiveEdit.Checked = value;
            }
        }

        public bool AllowAdd
        {
            get
            {
                return _add.Visible;
            }
            set
            {
                _add.Visible = value;
            }
        }

        public bool AllowDelete
        {
            get
            {
                return _deleteButton.Visible;
            }
            set
            {
                _deleteButton.Visible = value;
            }
        }

        public bool AllowChangeType
        {
            get
            {
                return _criteriaTypeSelect.Enabled;
            }
            set
            {
                _criteriaTypeSelect.Enabled = value;
            }
        }

        public int Index
        {
            get;
            set;
        }

        public event SelectedItemChangedHandler<string> CriteriaTypeChanged;

        public event UserActionHandler Adding;

        public event UserActionHandler Deleting;

        public event UserActionHandler ActivityChanged;
    }
}
