﻿using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Common.Contracts.DTO;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor
{
    public interface ICriteriaPresenter
    {
        void Init(List<FilterCriteria> allCriterias);

        void SetCriterias(List<FilterCriteria> allCriterias);

        IElementPresenter ElementPresenter { get; } 

        event ParameterlessEventHandler AfterCriteriaTypeChanged;

        event ParameterlessEventHandler AfterCriteriaChanged;
    }
}
