﻿using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Common.Contracts.DTO;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor
{
    public interface IElementPresenter
    {
        void InitView();

        event ParameterlessEventHandler AfterValueChanged;
    }
}
