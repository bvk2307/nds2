﻿using Luxoft.NDS2.Client.UI.Base;
using System;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.ElementEditors
{
    public partial class PlainTextFilterCriteriaEditor : UserControl, IPlainTextView
    {
        public PlainTextFilterCriteriaEditor()
        {
            InitializeComponent();

            _textInput.TextChanged += OnValueChanged;
        }

        public override string Text
        {
            get
            {
                return _textInput.Text;
            }
            set
            {
                _textInput.Text = value;
            }
        }

        public event ParameterlessEventHandler ValueChanged;

        private void OnValueChanged(object sender, EventArgs e)
        {
            if (ValueChanged != null)
            {
                ValueChanged();
            }
            GenerateAfterValueChange();
        }

        public event ParameterlessEventHandler AfterValueChanged;

        private void GenerateAfterValueChange()
        {
            if (AfterValueChanged != null)
            {
                AfterValueChanged();
            }
        }
    }
}
