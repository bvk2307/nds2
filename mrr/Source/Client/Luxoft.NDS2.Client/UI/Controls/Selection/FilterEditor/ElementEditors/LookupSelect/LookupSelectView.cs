﻿using Infragistics.Win;
using Luxoft.NDS2.Client.UI.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.ElementEditors
{
    public partial class LookupSelectFilterCriteriaEditor : UserControl, ILookupSelectView
    {
        public LookupSelectFilterCriteriaEditor()
        {
            InitializeComponent();

            _listSelect.SelectionChanged += OnValueChanged;
        }

        public void InitLookup(Dictionary<string, string> options)
        {
            _listSelect.Items.Clear();

            if (options.Any())
            {
                _listSelect.Items.AddRange(
                    options.Select(x => new ValueListItem(x.Key, x.Value)).ToArray());
            }
        }

        public string SelectedOption
        {
            get
            {
                if (_listSelect.SelectedItem == null)
                {
                    return null;
                }

                return _listSelect.Value.ToString();
            }
            set
            {
                foreach (var item in _listSelect.Items)
                {
                    if (item.DataValue.ToString() == value)
                    {
                        _listSelect.SelectedItem = item;
                    }
                }
            }
        }

        public event ParameterlessEventHandler ValueChanged;

        private void OnValueChanged(object sender, EventArgs e)
        {
            if (ValueChanged != null)
            {
                ValueChanged();
            }
            GenerateAfterValueChange();
        }

        public event ParameterlessEventHandler AfterValueChanged;

        private void GenerateAfterValueChange()
        {
            if (AfterValueChanged != null)
            {
                AfterValueChanged();
            }
        }
    }
}
