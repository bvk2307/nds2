﻿using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor
{
    public interface ICriteriaTypeSelectView
    {
        void SetOptions(
            List<string> criteriaTypes,
            string defaultOption);

        event SelectedItemChangedHandler<string> CriteriaTypeChanged;
    }
}
