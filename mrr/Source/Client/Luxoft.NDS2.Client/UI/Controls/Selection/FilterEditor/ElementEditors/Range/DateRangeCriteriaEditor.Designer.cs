﻿namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.ElementEditors
{
    partial class DateRangeCriteriaEditor
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Misc.UltraLabel _dateToLabel;
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Misc.UltraLabel _dateFromLabel;
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton dateButton1 = new Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton();
            Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton dateButton2 = new Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton();
            this._dateToInput = new Infragistics.Win.UltraWinSchedule.UltraCalendarCombo();
            this._dateFromInput = new Infragistics.Win.UltraWinSchedule.UltraCalendarCombo();
            _dateToLabel = new Infragistics.Win.Misc.UltraLabel();
            _dateFromLabel = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this._dateToInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._dateFromInput)).BeginInit();
            this.SuspendLayout();
            // 
            // _dateToLabel
            // 
            appearance2.TextHAlignAsString = "Center";
            appearance2.TextVAlignAsString = "Top";
            _dateToLabel.Appearance = appearance2;
            _dateToLabel.Dock = System.Windows.Forms.DockStyle.Left;
            _dateToLabel.Location = new System.Drawing.Point(181, 0);
            _dateToLabel.Name = "_dateToLabel";
            _dateToLabel.Size = new System.Drawing.Size(30, 25);
            _dateToLabel.TabIndex = 5;
            _dateToLabel.Text = "до:";
            // 
            // _dateFromLabel
            // 
            appearance1.TextHAlignAsString = "Center";
            appearance1.TextVAlignAsString = "Top";
            _dateFromLabel.Appearance = appearance1;
            _dateFromLabel.Dock = System.Windows.Forms.DockStyle.Left;
            _dateFromLabel.Location = new System.Drawing.Point(0, 0);
            _dateFromLabel.Name = "_dateFromLabel";
            _dateFromLabel.Size = new System.Drawing.Size(30, 25);
            _dateFromLabel.TabIndex = 6;
            _dateFromLabel.Text = "от:";
            // 
            // _dateToInput
            // 
            this._dateToInput.DateButtons.Add(dateButton1);
            this._dateToInput.Dock = System.Windows.Forms.DockStyle.Left;
            this._dateToInput.Format = "mm.dd.yyyy";
            this._dateToInput.Location = new System.Drawing.Point(211, 0);
            this._dateToInput.Name = "_dateToInput";
            this._dateToInput.NonAutoSizeHeight = 29;
            this._dateToInput.Size = new System.Drawing.Size(151, 25);
            this._dateToInput.TabIndex = 8;
            this._dateToInput.Value = new System.DateTime(2014, 8, 1, 0, 0, 0, 0);
            // 
            // _dateFromInput
            // 
            this._dateFromInput.DateButtons.Add(dateButton2);
            this._dateFromInput.Dock = System.Windows.Forms.DockStyle.Left;
            this._dateFromInput.Format = "mm.dd.yyyy";
            this._dateFromInput.Location = new System.Drawing.Point(30, 0);
            this._dateFromInput.Name = "_dateFromInput";
            this._dateFromInput.NonAutoSizeHeight = 29;
            this._dateFromInput.Size = new System.Drawing.Size(151, 25);
            this._dateFromInput.TabIndex = 7;
            this._dateFromInput.Value = new System.DateTime(2014, 8, 1, 0, 0, 0, 0);
            // 
            // DateRangeCriteriaEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._dateToInput);
            this.Controls.Add(_dateToLabel);
            this.Controls.Add(this._dateFromInput);
            this.Controls.Add(_dateFromLabel);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "DateRangeCriteriaEditor";
            this.Size = new System.Drawing.Size(540, 25);
            ((System.ComponentModel.ISupportInitialize)(this._dateToInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._dateFromInput)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.UltraWinSchedule.UltraCalendarCombo _dateToInput;
        private Infragistics.Win.UltraWinSchedule.UltraCalendarCombo _dateFromInput;
    }
}
