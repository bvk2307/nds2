﻿using Luxoft.NDS2.Client.UI.Base;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor
{
    public interface IElementView
    {
        event ParameterlessEventHandler ValueChanged;
    }
}
