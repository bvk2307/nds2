﻿using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor
{
    public static class ComparisonHelper
    {
        public static bool IsEqual(this string initialValue, string valueToCompare)
        {
            return (string.IsNullOrWhiteSpace(initialValue) 
                    && string.IsNullOrWhiteSpace(valueToCompare))
                || initialValue == valueToCompare;
        }

        public static bool ContainsSameElements<T>(
            this IEnumerable<T> initialArray, 
            IEnumerable<T> arrayToCompare)
        {
            if (initialArray.Count() != arrayToCompare.Count())
            {
                return false;
            }

            return initialArray
                .All(item => arrayToCompare.Any(anotherItem => anotherItem.Equals(item)));
        }

        public static bool IsEqual<T>(
            this IEnumerable<T> initialArray,
            IEnumerable<T> arrayToCompare)
        {
            if (initialArray.Count() != arrayToCompare.Count())
            {
                return false;
            }

            for (var idx = 0; idx < initialArray.Count(); idx++)
            {
                if (!initialArray.Skip(idx).First().Equals(
                        arrayToCompare.Skip(idx).First()))
                {
                    return false;
                }
            }

            return true;
        }
    }
}
