﻿using Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.CriteriaDependencies;
using Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.GroupEditor;
using Luxoft.NDS2.Common.Contracts.DTO;
using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.Creators
{
    public class GroupPresenterCreator : IGroupPresenterCreator
    {
        private readonly ICriteriaPresenterCreator _childCreator;

        private readonly List<FilterCriteria> _allCriterias;

        public GroupPresenterCreator(
            ICriteriaPresenterCreator criteriaCreator,
            List<FilterCriteria> allCriterias)
        {
            _childCreator = criteriaCreator;
            _allCriterias = allCriterias;
        }

        public IGroupPresenter Create(IGroupView view, GroupFilter model)
        {
            return new GroupPresenter(view, model, _childCreator, _allCriterias, new DependencyRule());
        }
    }
}
