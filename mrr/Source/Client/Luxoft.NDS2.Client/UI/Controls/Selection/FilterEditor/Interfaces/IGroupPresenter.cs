﻿using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Common.Contracts.DTO;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor
{
    public interface IGroupPresenter
    {
        void Init();

        event ParameterlessEventHandler AfterChanged;

        bool HasRegions { get; }

        string[] RegionCodes { get; }
    }
}
