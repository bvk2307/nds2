﻿using System.Collections.Generic;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.ElementEditors
{
    public interface ILookupSelectView : IElementView
    {
        void InitLookup(Dictionary<string, string> options);

        string SelectedOption { get; set; }
    }
}
