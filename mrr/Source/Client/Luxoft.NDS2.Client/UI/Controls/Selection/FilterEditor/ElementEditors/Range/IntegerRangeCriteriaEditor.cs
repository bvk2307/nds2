﻿using Luxoft.NDS2.Client.UI.Base;
using System;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.ElementEditors
{
    public partial class IntegerRangeCriteriaEditor : UserControl, IRangeCriteriaView
    {
        private string NullInteger = "0";

        public IntegerRangeCriteriaEditor()
        {
            InitializeComponent();
            _valueFromInput.ValueChanged += OnValueChanged;
            _valueToInput.ValueChanged += OnValueChanged;
        }

        public string FromValue
        {
            get
            {
                return _valueFromInput.Value is DBNull
                    ? NullInteger
                    : ((int)_valueFromInput.Value).ToString();
            }
            set
            {
                _valueFromInput.Value = Convert.ToInt32(value);
            }
        }

        public string ToValue
        {
            get
            {
                return _valueToInput.Value is DBNull
                    ? NullInteger
                    : ((int)_valueToInput.Value).ToString();
            }
            set
            {
                _valueToInput.Value = Convert.ToInt32(value);
            }
        }

        public event ParameterlessEventHandler ValueChanged;

        private void OnValueChanged(object sender, EventArgs e)
        {
            if (ValueChanged != null)
            {
                ValueChanged();
            }
            GenerateAfterValueChange();
        }

        public event ParameterlessEventHandler AfterValueChanged;

        private void GenerateAfterValueChange()
        {
            if (AfterValueChanged != null)
            {
                AfterValueChanged();
            }
        }
    }
}
