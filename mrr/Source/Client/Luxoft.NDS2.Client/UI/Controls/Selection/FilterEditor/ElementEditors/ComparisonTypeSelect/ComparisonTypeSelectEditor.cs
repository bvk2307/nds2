﻿using Luxoft.NDS2.Client.UI.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.ElementEditors
{
    public partial class ComparisonTypeSelectEditor : UserControl, IComparisonTypeSelectView
    {
        public ComparisonTypeSelectEditor()
        {
            InitializeComponent();

            _textInput.ValueChanged += OnValueChanged;
            _optionsList.ValueChanged += OnValueChanged;
        }

        public void SetComparisonOptions(List<string> comparisonTypes)
        {
            _optionsList.DataSource = comparisonTypes;
        }

        public string SelectedComparisonType
        {
            get
            {
                return _optionsList.Value.ToString();
            }
            set
            {
                _optionsList.Value = value;
            }
        }

        public string ComparisonExpression
        {
            get
            {
                return _textInput.Text;
            }
            set
            {
                _textInput.Text = value;
            }
        }

        public event ParameterlessEventHandler ValueChanged;

        private void OnValueChanged(object sender, EventArgs e)
        {
            if (ValueChanged != null)
            {
                ValueChanged();
            }
            GenerateAfterValueChange();
        }

        public event ParameterlessEventHandler AfterValueChanged;

        private void GenerateAfterValueChange()
        {
            if (AfterValueChanged != null)
            {
                AfterValueChanged();
            }
        }
    }
}
