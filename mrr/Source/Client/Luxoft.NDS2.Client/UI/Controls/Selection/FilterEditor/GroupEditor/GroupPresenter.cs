﻿using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.CriteriaDependencies;
using Luxoft.NDS2.Common.Contracts.DTO;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.GroupEditor
{
    public class GroupPresenter : IGroupPresenter
    {
        private const string RegionCriteria = "Region";

        private readonly List<ICriteriaPresenter> _childPresenters =
            new List<ICriteriaPresenter>();

        private readonly List<ICriteriaView> _childViews =
            new List<ICriteriaView>();

        private readonly GroupFilter _model;

        private readonly IGroupView _view;

        private readonly ICriteriaPresenterCreator _childCreator;

        private readonly List<FilterCriteria> _allCriterias;

        private readonly IDependencyRule _dependency;

        public GroupPresenter(
            IGroupView view,
            GroupFilter model,
            ICriteriaPresenterCreator childCreator,
            List<FilterCriteria> allCriterias,
            IDependencyRule dependency)
        {
            _view = view;
            _model = model;
            _childCreator = childCreator;
            _allCriterias = allCriterias;
            _dependency = dependency;

            _model.Filters = _model.Filters ?? new List<FilterCriteria>();

            if (!_model.Filters.Any())
            {
                _model.Filters.AddRange(_childCreator.DefaultModel());
            }
        }

        public void Init()
        {
            _view.IsActive = _model.IsActive;
            _view.ActivityChanged += () => _model.IsActive = _view.IsActive;

            for (var idx = 0; idx < _model.Filters.Count; idx++)
            {
                var childView = NewView(idx);
                _childViews.Add(childView);
                _childPresenters.Add(NewPresenter(childView, _model.Filters[idx]));
            }

            Draw();
            SetViewActions();
        }

        public bool HasRegions
        {
            get
            {
                return _model.Filters.Any(x => x.IsActive && x.InternalName == RegionCriteria);
            }
        }

        public string[] RegionCodes
        {
            get
            {
                var result = new List<string>();
                var first = true;

                foreach (var regionFilter 
                    in _model.Filters.Where(x => x.IsActive && x.InternalName == RegionCriteria))
                {
                    if (first)
                    {
                        result.AddRange(regionFilter.Values.Select(x => x.ValueOne));
                    }
                    else
                    {
                        result.Intersect(regionFilter.Values.Select(x => x.ValueOne));
                    }

                    first = false;
                }

                return result.ToArray();
            }
        }

        private ICriteriaPresenter NewPresenter(ICriteriaView view, FilterCriteria model)
        {
            var childPresenter = _childCreator.Create(view, model);
            childPresenter.Init(AllApplicable());

            childPresenter.AfterCriteriaTypeChanged += SetViewActions;
            childPresenter.AfterCriteriaChanged += () => OnCriteriaChanged();


            return childPresenter;
        }

        private ICriteriaView NewView(int index)
        {
            var childView = _view.NewCriteria();
            childView.Index = index;
            childView.AllowAdd = true;
            childView.Adding += () => AddAfter(childView);
            childView.Deleting += () => OnDelete(childView);

            return childView;
        }

        private void OnDelete(ICriteriaView view)
        {
            Remove(view);            
            OnCriteriaChanged(true);
            Draw();
        }

        private void AddAfter(ICriteriaView view)
        {
            foreach (var lowerView in _childViews.Where(vw => vw.Index > view.Index))
            {
                lowerView.Index++;
            }

            var newView = NewView(view.Index + 1);
            var newCriteria = _childCreator.DefaultCriteria();
            _model.Filters.Insert(view.Index + 1, newCriteria);

            _childViews.Insert(view.Index + 1, newView);
            _childPresenters.Insert(view.Index + 1, NewPresenter(newView, newCriteria));

            Draw();
            SetViewActions();
            RaiseAfterChanged();
        }

        private void Remove(ICriteriaView view)
        {
            _model.Filters.RemoveAt(view.Index);
            _childPresenters.RemoveAt(view.Index);
            _childViews.RemoveAt(view.Index);

            foreach (var childView in _childViews.Where(vw => vw.Index > view.Index))
            {
                childView.Index--;
            }         
        }

        private void SetViewActions()
        {
            var singleCriteria = _model.Filters.Count == 1;

            for (var index = 0; index < _model.Filters.Count; index++)
            {
                var criteria = _model.Filters[index];
                var childView = _childViews[index];

                childView.AllowAdd = true;
                childView.AllowDelete = !singleCriteria
                    && (!criteria.IsRequired 
                        || _model.Filters.Count(x => x.Name == criteria.Name) > 1);
                childView.AllowChangeType = childView.AllowDelete;
            }
        }

        private void Draw()
        {
            _view.Draw(_childViews.ToArray());
        }

        public event ParameterlessEventHandler AfterChanged;

        private void OnCriteriaChanged(bool noDraw = false)
        {
            var allApplicable = AllApplicable();
            var needReDraw = false;

            for (var index = 0; index < _model.Filters.Count; index++)
            {
                if (allApplicable.Any(x => x.InternalName == _model.Filters[index].InternalName))
                {
                    _childPresenters[index].SetCriterias(allApplicable);
                }
                else
                {
                    needReDraw = true;
                    Remove(_childViews[index]);
                }
            }

            foreach (var regionDependent 
                in _childPresenters
                    .Select(x => x.ElementPresenter)
                    .OfType<IRegionDependentElement>())
            {
                var hasRegions = HasRegions;
                regionDependent.SetRegions(hasRegions ? RegionCodes : new string[0], hasRegions);
            }

            if (needReDraw && !noDraw)
            {
                Draw();
            }
            SetViewActions();
            RaiseAfterChanged();
        }

        private List<FilterCriteria> AllApplicable()
        {
            return _allCriterias.Where(x => _dependency.IsApplicable(_model, x)).ToList();
        }

        private void RaiseAfterChanged()
        {
            if (AfterChanged != null)
            {
                AfterChanged();
            }
        }
    }
}
