﻿namespace Luxoft.NDS2.Client.UI.Controls.Selection.FilterEditor.ElementEditors
{
    partial class ListMultiSelectView
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this._optionsMultiSelect = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            ((System.ComponentModel.ISupportInitialize)(this._optionsMultiSelect)).BeginInit();
            this.SuspendLayout();
            // 
            // _optionsMultiSelect
            // 
            this._optionsMultiSelect.CheckedListSettings.CheckBoxStyle = Infragistics.Win.CheckStyle.CheckBox;
            this._optionsMultiSelect.CheckedListSettings.EditorValueSource = Infragistics.Win.EditorWithComboValueSource.CheckedItems;
            this._optionsMultiSelect.CheckedListSettings.ItemCheckArea = Infragistics.Win.ItemCheckArea.Item;
            this._optionsMultiSelect.CheckedListSettings.ListSeparator = " / ";
            this._optionsMultiSelect.Dock = System.Windows.Forms.DockStyle.Fill;
            this._optionsMultiSelect.Location = new System.Drawing.Point(0, 0);
            this._optionsMultiSelect.Margin = new System.Windows.Forms.Padding(2);
            this._optionsMultiSelect.Name = "_optionsMultiSelect";
            this._optionsMultiSelect.Size = new System.Drawing.Size(540, 21);
            this._optionsMultiSelect.TabIndex = 3;
            // 
            // ListMultiSelectView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._optionsMultiSelect);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "ListMultiSelectView";
            this.Size = new System.Drawing.Size(540, 25);
            ((System.ComponentModel.ISupportInitialize)(this._optionsMultiSelect)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.UltraWinEditors.UltraComboEditor _optionsMultiSelect;
    }
}
