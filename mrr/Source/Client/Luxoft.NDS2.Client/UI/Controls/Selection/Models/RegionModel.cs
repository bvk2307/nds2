﻿using Luxoft.NDS2.Client.Model;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.Models
{
    /// <summary>
    /// Этот класс представляет модель данных о регионе РФ
    /// </summary>
    public class RegionModel : ILookupEntry<string>
    {
        # region Свойства региона (отображаемые)

        /// <summary>
        /// Возвращает или задает Код региона
        /// </summary>
        [ListDisplaySettings(Title="Код", Width=80)]
        public string Code
        {
            get;
            set;
        }

        /// <summary>
        /// Возвращает или задает название региона
        /// </summary>
        [ListDisplaySettings(Title="Наименование", Width=1000)]
        public string Name
        {
            get;
            set;
        }

        # endregion

        # region Реализация интерфейса RegionModel

        public string Key
        {
            get { return Code; }
        }

        public bool IsSelected
        {
            get;
            set;
        }

        public string Title
        {
            get { return Name; }
        }

        # endregion
    }
}
