﻿using Luxoft.NDS2.Client.Model;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.Models
{
    /// <summary>
    /// Этот класс представляет модель данных налогового органа
    /// </summary>
    public class TaxAuthorityModel : ILookupEntry<string>
    {
        # region Данные налогового органа

        /// <summary>
        /// Возвращает или задает код налогового органа
        /// </summary>
        [ListDisplaySettings(Title="Код", Width=80)]
        public string Code { get; set; }

        /// <summary>
        /// Возвращает или задает наименование налогового органа
        /// </summary>
        [ListDisplaySettings(Title="Наименование", Width=1000)]
        public string Name { get; set; }

        # endregion

        # region Реализация интерфейса ILookupEntry<string>

        public string Key
        {
            get
            {
                return Code;
            }
        }

        public bool IsSelected { get; set; }

        public string Title
        {
            get
            {
                return Name;
            }
        }

        # endregion
    }
}
