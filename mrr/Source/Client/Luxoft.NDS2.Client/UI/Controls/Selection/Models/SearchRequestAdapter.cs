﻿using Luxoft.NDS2.Client.UI.Base;
using Luxoft.NDS2.Client.UI.Controls.LookupSelector;
using Luxoft.NDS2.Client.UI.Controls.LookupSelector.Implementation;
using Luxoft.NDS2.Common.Contracts.DTO.Query;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Controls.Selection.Models
{
    /// <summary>
    /// Этот класс реализует функцию преобразования запроса от MultiSelectLookup контрола в запрос к IDataTableSelector
    /// </summary>
    public class SearchRequestAdapter
    {
        private const string RegionTableName = "v$ssrf";

        private const string TaxAuthorityTableName = "v$sono";

        private const string CodeColumnName = "S_CODE";

        private const string NameColumnName = "S_NAME";

        private readonly IDataTableSelector _tableSelector;

        /// <summary>
        /// Создает экземпляр класса SearchRequestAdapter
        /// </summary>
        /// <param name="tableSelector">Реализация IDataTableSelector</param>
        public SearchRequestAdapter(IDataTableSelector tableSelector)
        {
            _tableSelector = tableSelector;
        }

        /// <summary>
        /// Выполняет поиск Регионов РФ по ключевому слову
        /// </summary>
        /// <param name="request">Параметры поиска регионов</param>
        /// <returns>Набор моделей регионов, удовлетворяющих критерию поиска</returns>
        public DataRequestResult<RegionModel> SearchRegions(IDataRequestArgs request)
        {
            var query = new DictionaryConditions();

            if (!string.IsNullOrWhiteSpace(request.SearchPattern))
            {
                query.Filter.Add(
                    new FilterQuery()
                    {
                        ColumnName = NameColumnName,
                        Filtering =
                            new List<ColumnFilter>()
                            {
                                { 
                                    new ColumnFilter() 
                                    { 
                                        ComparisonOperator =
                                            ColumnFilter.FilterComparisionOperator.Contains, 
                                        Value = request.SearchPattern
                                    }
                                }
                            }
                    });
            }

            return new DataRequestResult<RegionModel>(
                _tableSelector
                    .GetData(RegionTableName, query)
                    .Select(pair => new RegionModel() { Code = pair.Key, Name = pair.Value }));

        }

        /// <summary>
        /// Выполняет поиск Налоговых органов по кючевому слову
        /// </summary>
        /// <param name="request">Параметры поиска налоговых органов</param>
        /// <returns>Набор моделей данных налоговых органов, удовлетворяющих условию поиска</returns>
        public DataRequestResult<TaxAuthorityModel> SearchTaxAuthoritites(IDataRequestArgs request)
        {
            var query = new DictionaryConditions();

            if (!string.IsNullOrWhiteSpace(request.SearchPattern))
            {
                query.Filter.Add(
                    new FilterQuery()
                    {
                        ColumnName = NameColumnName,
                        Filtering =
                            new List<ColumnFilter>()
                            {
                                { 
                                    new ColumnFilter() 
                                    { 
                                        ComparisonOperator =
                                            ColumnFilter.FilterComparisionOperator.Contains, 
                                        Value = request.SearchPattern
                                    }
                                }
                            }
                    });
            }

            return new DataRequestResult<TaxAuthorityModel>(
                _tableSelector
                    .GetData(TaxAuthorityTableName, query)
                    .Select(pair => new TaxAuthorityModel() { Code = pair.Key, Name = pair.Value }));

        }
    }
}
