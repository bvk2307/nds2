﻿using Luxoft.NDS2.Client.UI.Controls.Grid.V2;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns.AutoComplete;

namespace Luxoft.NDS2.Client.UI.Controls.Sur
{
    public class SurColumn : PropertyBoundColumn
    {
        private readonly DictionarySur _dictionary;

        private readonly IGrid _iGrid;

        public SurColumn(string key, IGrid iGrid, DictionarySur dictionary, IColumnExpression columnExpression)
            : base(key, iGrid, columnExpression)
        {
            _dictionary = dictionary;
            _iGrid = iGrid;
            DisableEdit = true;
        }

        protected override void SetupColumn()
        {
            base.SetupColumn();
            _iGrid.AfterDataSourceChanged +=
                () =>
                {
                    Column().DrawSur(_dictionary);
                };
        }

        public override IColumnFilterManager OptionsBuilder()
        {
            return new SurAutoCompleteOptionsBuilder(_dictionary);
        }
    }
}
