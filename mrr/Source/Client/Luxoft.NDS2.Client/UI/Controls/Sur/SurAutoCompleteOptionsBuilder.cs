﻿using Infragistics.Win;
using grid = Infragistics.Win.UltraWinGrid;
using System;
using System.Linq;
using System.Collections.Generic;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using Luxoft.NDS2.Client.UI.Controls.Grid.V2.Columns.AutoComplete;

namespace Luxoft.NDS2.Client.UI.Controls.Sur
{
    public class SurAutoCompleteOptionsBuilder : IColumnFilterManager
    {
        private readonly DictionarySur _dictionary;

        public SurAutoCompleteOptionsBuilder(DictionarySur dictionary)
        {
            _dictionary = dictionary;
        }

        public void AutoComplete(ValueList initialList)
        {
            var allItem = grid.Resources.Customizer.GetCustomizedString(Constants.AllItemKey);
            var itemsToDelete = 
                initialList
                    .ValueListItems
                    .WhereItem(item => item.DisplayText != allItem)
                    .ToArray();

            foreach (var item in itemsToDelete)
            {
                initialList.ValueListItems.Remove(item);
            }

            foreach (var surItem in _dictionary.Items)
            {
                initialList.ValueListItems.Add(new ValueListItem(surItem.Code, surItem.Description));
            }
        }

        public void OnFilterChanged(grid.ColumnFilter gridFilter)
        {
            if (gridFilter.FilterConditions.Count == 1 
                && gridFilter.FilterConditions[0].ComparisionOperator
                    == grid.FilterComparisionOperator.Equals
                && (int)gridFilter.FilterConditions[0].CompareValue == _dictionary.DefaultCode()) //Если пользователь указал дефолтный код, надо учесть null и всякий мусор
            {
                gridFilter.LogicalOperator = grid.FilterLogicalOperator.Or;
                gridFilter.FilterConditions.Add(
                    new grid.FilterCondition(
                        grid.FilterComparisionOperator.Equals,
                        null));
                gridFilter.FilterConditions.Add(
                    new grid.FilterCondition(
                        grid.FilterComparisionOperator.LessThan,
                        _dictionary.Items.Min(sur => sur.Code)));
                gridFilter.FilterConditions.Add(
                    new grid.FilterCondition(
                        grid.FilterComparisionOperator.GreaterThan,
                        _dictionary.Items.Max(sur => sur.Code)));
            }
        }
    }
}
