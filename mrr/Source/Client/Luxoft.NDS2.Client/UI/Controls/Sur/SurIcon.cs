﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Controls.Sur
{
    [Obsolete("Необходимо использовать SurView")]
    public partial class SurIcon : UserControl
    {
        private DictionarySur _allItems;

        private int? _code;

        private readonly ToolTip _toolTip = new ToolTip();

        public SurIcon()
        {
            InitializeComponent();
        }

        public void SetSurDictionary(DictionarySur dictionary)
        {
            _allItems = dictionary;
        }

        public int? Code
        {
            get
            {
                return _code;
            }
            set
            {
                if (_code != value)
                {
                    _code = value;
                    Refresh();                    
                }
            }
        }

        public override void Refresh()
        {
            base.Refresh();

            if (_allItems != null)
            {
                _image.DrawSur(_allItems.GetARGB(_code));
                _toolTip.SetToolTip(_image, _allItems.Description(_code));
            }
        }
    }
}
