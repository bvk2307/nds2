﻿using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using System.Collections.Generic;
using System.Linq;

namespace Luxoft.NDS2.Client.UI.Controls.Sur
{
    public class DictionarySur
    {
        private readonly List<SurCode> _dataItems = new List<SurCode>();

        public DictionarySur(IEnumerable<SurCode> dataItems)
        {
            _dataItems.AddRange(dataItems);
        }

        public IEnumerable<SurCode> SurCodes
        {
            get { return _dataItems; }
        }

        public IEnumerable<SurItem> Items
        {
            get
            {
                return _dataItems
                    .OrderBy(item => item.Code)
                    .Select(item => new SurItem(item.Code, item.Description, item.ColorARGB));
            }
        }
        
        public int GetARGB(int? code)
        {
            return Find(code).ColorARGB;
        }

        public string Description(int? code)
        {
            return Find(code).Description;
        }

        public int DefaultCode()
        {
            return Default().Code;
        }

        public int DefaultARGB()
        {
            return Default().ColorARGB;
        }

        public string DefaultDescription()
        {
            return Default().Description;
        }

        public SurCode Find(int? code)
        {
            return code.HasValue && _dataItems.Any(item => item.Code == code.Value)
                ? _dataItems.First(item => item.Code == code.Value)
                : Default();
        }

        private SurCode Default()
        {
            return _dataItems.First(item => item.IsDefault);
        }
    }
}
