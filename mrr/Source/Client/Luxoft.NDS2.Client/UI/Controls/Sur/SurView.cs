﻿using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Controls.Sur
{
    public partial class SurView : UserControl, ISurView
    {
        private readonly ToolTip _toolTip = new ToolTip();

        public SurView()
        {
            InitializeComponent();
        }

        public int ARGB
        {
            set
            {
                _image.DrawSur(value);
                Refresh();
            }
        }

        public string Description
        {
            set
            {
                _toolTip.SetToolTip(_image, value);
                Refresh();
            }
        }
    }
}
