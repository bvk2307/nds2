﻿namespace Luxoft.NDS2.Client.UI.Controls.Sur
{
    public class SurItem
    {
        public SurItem(int code, string description, int colorARGB)
        {
            Code = code;
            Description = description;
            ColorARGB = colorARGB;
        }

        public int Code
        {
            get;
            private set;
        }

        public string Description
        {
            get;
            private set;
        }

        public int ColorARGB
        {
            get;
            private set;
        }
    }
}
