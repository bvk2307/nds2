﻿using Infragistics.Win;
using Infragistics.Win.Misc;
using Infragistics.Win.UltraWinGrid;
using Luxoft.NDS2.Common.Contracts.DTO.Dictionaries;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace Luxoft.NDS2.Client.UI.Controls.Sur
{
    public static class SurHelper
    {
        private static readonly Dictionary<int, Image> _icons = new Dictionary<int,Image>();

        public static void DrawSur(this UltraLabel label, int colorARGB)
        {
            label.Appearance.Image = GetIcon(label, colorARGB);
        }

        public static void DrawSur(this UltraGridColumn column, DictionarySur dictionary)
        {
            column.ValueBasedAppearance = column.Band.Layout.Grid.DrawSurColumn(dictionary);

            foreach (var row in column.Band.Layout.Grid.Rows)
            {
                row.Cells[column.Key].ToolTipText =
                    dictionary.Description((int?)row.Cells[column.Key].Value);
            }            
        }

        /// <summary>
        /// Используется для отрисовки СУР в Excel
        /// </summary>
        /// <param name="grid"></param>
        /// <param name="colorARGB"></param>
        /// <param name="icon"></param>
        public static void DrawExcelSur(this Control grid, int colorARGB, out Image icon)
        {
            icon = GetIcon(grid, colorARGB);
        }

        public static ConditionValueAppearance DrawSurColumn(this Control grid, DictionarySur dictionary)
        {
            var appearance = new ConditionValueAppearance();

            appearance.Add(
                new OperatorCondition(ConditionOperator.IsNullOrEmpty, null),
                grid.GetIcon(dictionary.DefaultARGB()).Appearance());

            appearance.Add(
                new OperatorCondition(ConditionOperator.LessThan, dictionary.Items.Min(sur => sur.Code)),
                grid.GetIcon(dictionary.DefaultARGB()).Appearance());

            appearance.Add(
                new OperatorCondition(ConditionOperator.GreaterThan, dictionary.Items.Max(sur => sur.Code)),
                grid.GetIcon(dictionary.DefaultARGB()).Appearance());

            foreach (var item in dictionary.Items)
            {
                appearance.Add(
                    new OperatorCondition(ConditionOperator.Equals, item.Code),
                    grid.GetIcon(item.ColorARGB).Appearance());
            }

            return appearance;
        }

        private static Infragistics.Win.Appearance Appearance(this Image image)
        {
            return
                new Infragistics.Win.Appearance
                {
                    Image = image,
                    ImageHAlign = HAlign.Center,
                    ImageVAlign = VAlign.Middle,
                    ForegroundAlpha = Alpha.Transparent
                };
        }

        private static Image GetIcon(this Control owner, int colorARGB)
        {
            if (!_icons.ContainsKey(colorARGB))
            {
                lock (_icons)
                {
                    if (!_icons.ContainsKey(colorARGB))
                    {
                        _icons[colorARGB] = owner.NewIcon(colorARGB);
                    }
                }
            }
            return _icons[colorARGB];
        }

        private static Image NewIcon(this Control owner, int colorARGB)
        {
            using (var stream = new MemoryStream())
            using (var hdc = owner.CreateGraphics())
            {
                var rectangle = new Rectangle(0, 0, 16, 16);
                var emf = new Metafile(stream, hdc.GetHdc(), rectangle, MetafileFrameUnit.Pixel, EmfType.EmfPlusOnly);
                using (var graphics = Graphics.FromImage(emf))
                using (var brush = new SolidBrush(Color.FromArgb(colorARGB)))
                {
                    graphics.FillRectangle(Brushes.Transparent, rectangle);
                    rectangle = new Rectangle(0, 0, 15, 15);
                    graphics.FillEllipse(brush, rectangle);
                    graphics.DrawEllipse(Pens.DarkSlateGray, rectangle);
                }
                return emf;
            }
        }
    }
}
