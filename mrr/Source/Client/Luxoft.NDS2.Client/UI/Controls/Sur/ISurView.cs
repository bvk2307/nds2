﻿namespace Luxoft.NDS2.Client.UI.Controls.Sur
{
    public interface ISurView
    {
        int ARGB { set; }
        string Description { set; }
    }
}