﻿namespace Luxoft.NDS2.Client.UI.Controls.Reclaim
{
    partial class ReclaimHistoryChangeStatus
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            this.tableLayoutMain = new System.Windows.Forms.TableLayoutPanel();
            this.labelCreateCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelCreateValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelSendToSEODCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelSendToSEODValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelSendToNOExecutorCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelSendToNOExecutorValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelReceivedNOExecutorCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelReceivedNOExecutorValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelSendToTaxPayerCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelSendToTaxPayerValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelHandedToTaxpayerCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelHandedToTaxpayerValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelCloseCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelCloseValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelCalculatedDateDeliveryCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelCalculatedDateDeliveryValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelProlongAnswerDateCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelProlongAnswerDateValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelFinishInputAnswerDataCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelFinishInputAnswerDataValue = new Infragistics.Win.Misc.UltraLabel();
            this.tableLayoutMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutMain
            // 
            this.tableLayoutMain.ColumnCount = 7;
            this.tableLayoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutMain.Controls.Add(this.labelCreateCaption, 0, 1);
            this.tableLayoutMain.Controls.Add(this.labelCreateValue, 1, 1);
            this.tableLayoutMain.Controls.Add(this.labelSendToSEODCaption, 0, 2);
            this.tableLayoutMain.Controls.Add(this.labelSendToSEODValue, 1, 2);
            this.tableLayoutMain.Controls.Add(this.labelSendToNOExecutorCaption, 0, 3);
            this.tableLayoutMain.Controls.Add(this.labelSendToNOExecutorValue, 1, 3);
            this.tableLayoutMain.Controls.Add(this.labelReceivedNOExecutorCaption, 0, 4);
            this.tableLayoutMain.Controls.Add(this.labelReceivedNOExecutorValue, 1, 4);
            this.tableLayoutMain.Controls.Add(this.labelSendToTaxPayerCaption, 0, 5);
            this.tableLayoutMain.Controls.Add(this.labelSendToTaxPayerValue, 1, 5);
            this.tableLayoutMain.Controls.Add(this.labelHandedToTaxpayerCaption, 0, 6);
            this.tableLayoutMain.Controls.Add(this.labelHandedToTaxpayerValue, 1, 6);
            this.tableLayoutMain.Controls.Add(this.labelCloseCaption, 0, 7);
            this.tableLayoutMain.Controls.Add(this.labelCloseValue, 1, 7);
            this.tableLayoutMain.Controls.Add(this.labelCalculatedDateDeliveryCaption, 2, 5);
            this.tableLayoutMain.Controls.Add(this.labelCalculatedDateDeliveryValue, 3, 5);
            this.tableLayoutMain.Controls.Add(this.labelProlongAnswerDateCaption, 2, 6);
            this.tableLayoutMain.Controls.Add(this.labelProlongAnswerDateValue, 3, 6);
            this.tableLayoutMain.Controls.Add(this.labelFinishInputAnswerDataCaption, 4, 6);
            this.tableLayoutMain.Controls.Add(this.labelFinishInputAnswerDataValue, 5, 6);
            this.tableLayoutMain.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutMain.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutMain.Margin = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.tableLayoutMain.Name = "tableLayoutMain";
            this.tableLayoutMain.RowCount = 9;
            this.tableLayoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 3F));
            this.tableLayoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutMain.Size = new System.Drawing.Size(911, 175);
            this.tableLayoutMain.TabIndex = 0;
            // 
            // labelCreateCaption
            // 
            appearance1.TextHAlignAsString = "Right";
            this.labelCreateCaption.Appearance = appearance1;
            this.labelCreateCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCreateCaption.Location = new System.Drawing.Point(1, 4);
            this.labelCreateCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelCreateCaption.Name = "labelCreateCaption";
            this.labelCreateCaption.Size = new System.Drawing.Size(198, 23);
            this.labelCreateCaption.TabIndex = 0;
            this.labelCreateCaption.Text = "Создано:";
            // 
            // labelCreateValue
            // 
            this.labelCreateValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCreateValue.Location = new System.Drawing.Point(201, 4);
            this.labelCreateValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelCreateValue.Name = "labelCreateValue";
            this.labelCreateValue.Size = new System.Drawing.Size(78, 23);
            this.labelCreateValue.TabIndex = 1;
            this.labelCreateValue.Text = "10.05.2015";
            // 
            // labelSendToSEODCaption
            // 
            appearance2.TextHAlignAsString = "Right";
            this.labelSendToSEODCaption.Appearance = appearance2;
            this.labelSendToSEODCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSendToSEODCaption.Location = new System.Drawing.Point(1, 29);
            this.labelSendToSEODCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelSendToSEODCaption.Name = "labelSendToSEODCaption";
            this.labelSendToSEODCaption.Size = new System.Drawing.Size(198, 23);
            this.labelSendToSEODCaption.TabIndex = 2;
            this.labelSendToSEODCaption.Text = "Отправлено в СЕОД:";
            // 
            // labelSendToSEODValue
            // 
            this.labelSendToSEODValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSendToSEODValue.Location = new System.Drawing.Point(201, 29);
            this.labelSendToSEODValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelSendToSEODValue.Name = "labelSendToSEODValue";
            this.labelSendToSEODValue.Size = new System.Drawing.Size(78, 23);
            this.labelSendToSEODValue.TabIndex = 3;
            this.labelSendToSEODValue.Text = "10.05.2015";
            // 
            // labelSendToNOExecutorCaption
            // 
            appearance3.TextHAlignAsString = "Right";
            this.labelSendToNOExecutorCaption.Appearance = appearance3;
            this.labelSendToNOExecutorCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSendToNOExecutorCaption.Location = new System.Drawing.Point(1, 54);
            this.labelSendToNOExecutorCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelSendToNOExecutorCaption.Name = "labelSendToNOExecutorCaption";
            this.labelSendToNOExecutorCaption.Size = new System.Drawing.Size(198, 23);
            this.labelSendToNOExecutorCaption.TabIndex = 4;
            this.labelSendToNOExecutorCaption.Text = "Отправлено в НО-испольнитель:";
            // 
            // labelSendToNOExecutorValue
            // 
            this.labelSendToNOExecutorValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSendToNOExecutorValue.Location = new System.Drawing.Point(201, 54);
            this.labelSendToNOExecutorValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelSendToNOExecutorValue.Name = "labelSendToNOExecutorValue";
            this.labelSendToNOExecutorValue.Size = new System.Drawing.Size(78, 23);
            this.labelSendToNOExecutorValue.TabIndex = 5;
            this.labelSendToNOExecutorValue.Text = "10.05.2015";
            // 
            // labelReceivedNOExecutorCaption
            // 
            appearance4.TextHAlignAsString = "Right";
            this.labelReceivedNOExecutorCaption.Appearance = appearance4;
            this.labelReceivedNOExecutorCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelReceivedNOExecutorCaption.Location = new System.Drawing.Point(1, 79);
            this.labelReceivedNOExecutorCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelReceivedNOExecutorCaption.Name = "labelReceivedNOExecutorCaption";
            this.labelReceivedNOExecutorCaption.Size = new System.Drawing.Size(198, 23);
            this.labelReceivedNOExecutorCaption.TabIndex = 6;
            this.labelReceivedNOExecutorCaption.Text = "Получено НО-исполнителем:";
            // 
            // labelReceivedNOExecutorValue
            // 
            this.labelReceivedNOExecutorValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelReceivedNOExecutorValue.Location = new System.Drawing.Point(201, 79);
            this.labelReceivedNOExecutorValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelReceivedNOExecutorValue.Name = "labelReceivedNOExecutorValue";
            this.labelReceivedNOExecutorValue.Size = new System.Drawing.Size(78, 23);
            this.labelReceivedNOExecutorValue.TabIndex = 7;
            this.labelReceivedNOExecutorValue.Text = "11.05.2015";
            // 
            // labelSendToTaxPayerCaption
            // 
            appearance5.TextHAlignAsString = "Right";
            this.labelSendToTaxPayerCaption.Appearance = appearance5;
            this.labelSendToTaxPayerCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSendToTaxPayerCaption.Location = new System.Drawing.Point(1, 104);
            this.labelSendToTaxPayerCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelSendToTaxPayerCaption.Name = "labelSendToTaxPayerCaption";
            this.labelSendToTaxPayerCaption.Size = new System.Drawing.Size(198, 23);
            this.labelSendToTaxPayerCaption.TabIndex = 8;
            this.labelSendToTaxPayerCaption.Text = "Отправлено налогоплательщику:";
            // 
            // labelSendToTaxPayerValue
            // 
            this.labelSendToTaxPayerValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelSendToTaxPayerValue.Location = new System.Drawing.Point(201, 104);
            this.labelSendToTaxPayerValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelSendToTaxPayerValue.Name = "labelSendToTaxPayerValue";
            this.labelSendToTaxPayerValue.Size = new System.Drawing.Size(78, 23);
            this.labelSendToTaxPayerValue.TabIndex = 9;
            this.labelSendToTaxPayerValue.Text = "12.05.2015";
            // 
            // labelHandedToTaxpayerCaption
            // 
            appearance6.TextHAlignAsString = "Right";
            this.labelHandedToTaxpayerCaption.Appearance = appearance6;
            this.labelHandedToTaxpayerCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelHandedToTaxpayerCaption.Location = new System.Drawing.Point(1, 129);
            this.labelHandedToTaxpayerCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelHandedToTaxpayerCaption.Name = "labelHandedToTaxpayerCaption";
            this.labelHandedToTaxpayerCaption.Size = new System.Drawing.Size(198, 23);
            this.labelHandedToTaxpayerCaption.TabIndex = 10;
            this.labelHandedToTaxpayerCaption.Text = "Вручено налогоплательщику:";
            // 
            // labelHandedToTaxpayerValue
            // 
            this.labelHandedToTaxpayerValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelHandedToTaxpayerValue.Location = new System.Drawing.Point(201, 129);
            this.labelHandedToTaxpayerValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelHandedToTaxpayerValue.Name = "labelHandedToTaxpayerValue";
            this.labelHandedToTaxpayerValue.Size = new System.Drawing.Size(78, 23);
            this.labelHandedToTaxpayerValue.TabIndex = 11;
            this.labelHandedToTaxpayerValue.Text = "14.05.2015";
            // 
            // labelCloseCaption
            // 
            appearance7.TextHAlignAsString = "Right";
            this.labelCloseCaption.Appearance = appearance7;
            this.labelCloseCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCloseCaption.Location = new System.Drawing.Point(1, 154);
            this.labelCloseCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelCloseCaption.Name = "labelCloseCaption";
            this.labelCloseCaption.Size = new System.Drawing.Size(198, 23);
            this.labelCloseCaption.TabIndex = 12;
            this.labelCloseCaption.Text = "Закрыто:";
            // 
            // labelCloseValue
            // 
            this.labelCloseValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCloseValue.Location = new System.Drawing.Point(201, 154);
            this.labelCloseValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelCloseValue.Name = "labelCloseValue";
            this.labelCloseValue.Size = new System.Drawing.Size(78, 23);
            this.labelCloseValue.TabIndex = 13;
            this.labelCloseValue.Text = "15.05.2015";
            // 
            // labelCalculatedDateDeliveryCaption
            // 
            appearance8.TextHAlignAsString = "Right";
            this.labelCalculatedDateDeliveryCaption.Appearance = appearance8;
            this.labelCalculatedDateDeliveryCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCalculatedDateDeliveryCaption.Location = new System.Drawing.Point(281, 104);
            this.labelCalculatedDateDeliveryCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelCalculatedDateDeliveryCaption.Name = "labelCalculatedDateDeliveryCaption";
            this.labelCalculatedDateDeliveryCaption.Size = new System.Drawing.Size(198, 23);
            this.labelCalculatedDateDeliveryCaption.TabIndex = 14;
            this.labelCalculatedDateDeliveryCaption.Text = "Расчетная дата вручения:";
            // 
            // labelCalculatedDateDeliveryValue
            // 
            this.labelCalculatedDateDeliveryValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCalculatedDateDeliveryValue.Location = new System.Drawing.Point(481, 104);
            this.labelCalculatedDateDeliveryValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelCalculatedDateDeliveryValue.Name = "labelCalculatedDateDeliveryValue";
            this.labelCalculatedDateDeliveryValue.Size = new System.Drawing.Size(78, 23);
            this.labelCalculatedDateDeliveryValue.TabIndex = 15;
            this.labelCalculatedDateDeliveryValue.Text = "21.05.2015";
            // 
            // labelProlongAnswerDateCaption
            // 
            appearance9.TextHAlignAsString = "Right";
            this.labelProlongAnswerDateCaption.Appearance = appearance9;
            this.labelProlongAnswerDateCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelProlongAnswerDateCaption.Location = new System.Drawing.Point(281, 129);
            this.labelProlongAnswerDateCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelProlongAnswerDateCaption.Name = "labelProlongAnswerDateCaption";
            this.labelProlongAnswerDateCaption.Size = new System.Drawing.Size(198, 23);
            this.labelProlongAnswerDateCaption.TabIndex = 16;
            this.labelProlongAnswerDateCaption.Text = "Дата продления ответа:";
            // 
            // labelProlongAnswerDateValue
            // 
            this.labelProlongAnswerDateValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelProlongAnswerDateValue.Location = new System.Drawing.Point(481, 129);
            this.labelProlongAnswerDateValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelProlongAnswerDateValue.Name = "labelProlongAnswerDateValue";
            this.labelProlongAnswerDateValue.Size = new System.Drawing.Size(78, 23);
            this.labelProlongAnswerDateValue.TabIndex = 17;
            this.labelProlongAnswerDateValue.Text = "28.05.2015";
            // 
            // labelFinishInputAnswerDataCaption
            // 
            appearance10.TextHAlignAsString = "Right";
            this.labelFinishInputAnswerDataCaption.Appearance = appearance10;
            this.labelFinishInputAnswerDataCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelFinishInputAnswerDataCaption.Location = new System.Drawing.Point(561, 129);
            this.labelFinishInputAnswerDataCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelFinishInputAnswerDataCaption.Name = "labelFinishInputAnswerDataCaption";
            this.labelFinishInputAnswerDataCaption.Size = new System.Drawing.Size(198, 23);
            this.labelFinishInputAnswerDataCaption.TabIndex = 18;
            this.labelFinishInputAnswerDataCaption.Text = "Дата окончания ввода ответа:";
            // 
            // labelFinishInputAnswerDataValue
            // 
            this.labelFinishInputAnswerDataValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelFinishInputAnswerDataValue.Location = new System.Drawing.Point(761, 129);
            this.labelFinishInputAnswerDataValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelFinishInputAnswerDataValue.Name = "labelFinishInputAnswerDataValue";
            this.labelFinishInputAnswerDataValue.Size = new System.Drawing.Size(78, 23);
            this.labelFinishInputAnswerDataValue.TabIndex = 19;
            this.labelFinishInputAnswerDataValue.Text = "31.05.2015";
            // 
            // ReclaimHistoryChangeStatus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutMain);
            this.Name = "ReclaimHistoryChangeStatus";
            this.Size = new System.Drawing.Size(911, 174);
            this.tableLayoutMain.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutMain;
        private Infragistics.Win.Misc.UltraLabel labelCreateCaption;
        private Infragistics.Win.Misc.UltraLabel labelCreateValue;
        private Infragistics.Win.Misc.UltraLabel labelSendToSEODCaption;
        private Infragistics.Win.Misc.UltraLabel labelSendToSEODValue;
        private Infragistics.Win.Misc.UltraLabel labelSendToNOExecutorCaption;
        private Infragistics.Win.Misc.UltraLabel labelSendToNOExecutorValue;
        private Infragistics.Win.Misc.UltraLabel labelReceivedNOExecutorCaption;
        private Infragistics.Win.Misc.UltraLabel labelReceivedNOExecutorValue;
        private Infragistics.Win.Misc.UltraLabel labelSendToTaxPayerCaption;
        private Infragistics.Win.Misc.UltraLabel labelSendToTaxPayerValue;
        private Infragistics.Win.Misc.UltraLabel labelHandedToTaxpayerCaption;
        private Infragistics.Win.Misc.UltraLabel labelHandedToTaxpayerValue;
        private Infragistics.Win.Misc.UltraLabel labelCloseCaption;
        private Infragistics.Win.Misc.UltraLabel labelCloseValue;
        private Infragistics.Win.Misc.UltraLabel labelCalculatedDateDeliveryCaption;
        private Infragistics.Win.Misc.UltraLabel labelCalculatedDateDeliveryValue;
        private Infragistics.Win.Misc.UltraLabel labelProlongAnswerDateCaption;
        private Infragistics.Win.Misc.UltraLabel labelProlongAnswerDateValue;
        private Infragistics.Win.Misc.UltraLabel labelFinishInputAnswerDataCaption;
        private Infragistics.Win.Misc.UltraLabel labelFinishInputAnswerDataValue;
    }
}
