﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Luxoft.NDS2.Client.UI.DiscrepancyDocuments.Models;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Client.UI.Controls.Sur;

namespace Luxoft.NDS2.Client.UI.Controls.Reclaim
{
    public partial class ReclaimTaxPayerInfo : UserControl
    {
        public DictionarySur SURCodes
        {
            set
            {
                _surIndicator.SetSurDictionary(value);
            }
        }

        public ReclaimTaxPayerInfo()
        {
            InitializeComponent();
        }

        public void SetDataEmpty(DiscrepancyDocumentInfo docInfo)
        {
            SetRegimView(docInfo);

            SetDataEmpty();
        }

        public void SetDataEmpty()
        {
            LinkLabelNameValue.Value = string.Concat("<a href='#'>", String.Empty, "</a>");
            labelInnValue.Text = String.Empty;
            labelKPPValue.Text = String.Empty;
            labelDiscrepancyCountValue.Text = String.Empty;
            labelCountSFValue.Text = String.Empty;
            labelDiscrepancyAmountValue.Text = String.Empty;
            labelDiscrepancyPVPAmountValue.Text = String.Empty;
            _surIndicator.Code = null;
        }

        private void SetRegimView(DiscrepancyDocumentInfo docInfo)
        {
            if (docInfo.DocType != null &&
                (docInfo.DocType.EntryId == DocType.ClaimSF ||
                 docInfo.DocType.EntryId == DocType.Reclaim93 ||
                 docInfo.DocType.EntryId == DocType.Reclaim93_1))
            {
                labelCountSFCaption.Visible = true;
                labelDiscrepancyAmountCaption.Visible = true;
                labelDiscrepancyPVPAmountCaption.Visible = true;
                labelCountSFValue.Visible = true;
                labelDiscrepancyAmountValue.Visible = true;
                labelDiscrepancyPVPAmountValue.Visible = true;
            }
            else
            {
                labelCountSFCaption.Visible = false;
                labelDiscrepancyAmountCaption.Visible = false;
                labelDiscrepancyPVPAmountCaption.Visible = false;
                labelCountSFValue.Visible = false;
                labelDiscrepancyAmountValue.Visible = false;
                labelDiscrepancyPVPAmountValue.Visible = false;
            }
        }

        public void SetDataDeclaration(DeclarationSummary declaration, DocumentCalculateInfo calcInfo)
        {
            if (declaration != null)
            {
                LinkLabelNameValue.Value = string.Concat("<a href='#'>", declaration.NAME, "</a>");
                labelInnValue.Text = declaration.INN;
                labelKPPValue.Text = declaration.KPP;
                labelDiscrepancyAmountValue.Text = calcInfo.SumAmountDiscrepancy.ToString("N2");
                labelDiscrepancyPVPAmountValue.Text = calcInfo.SumPVPAmountDiscrepancy.ToString("N2");
                labelDiscrepancyCountValue.Text = calcInfo.CountDiscrepancy.ToString("N0");
                labelCountSFValue.Text = calcInfo.CountInvoice.ToString("N0");
            }
            SetSURInfo(declaration);
        }

        public void SetDataDocumentCalculateInfo(DocumentCalculateInfo docCalcInfo, DiscrepancyDocumentInfo docInfo)
        {
            if (docCalcInfo != null)
            {
                SetRegimView(docInfo);

                string format = "{0:N2}";
                if (docInfo.DocType != null &&
                    (docInfo.DocType.EntryId == DocType.ClaimSF ||
                     docInfo.DocType.EntryId == DocType.Reclaim93 ||
                     docInfo.DocType.EntryId == DocType.Reclaim93_1))
                {
                    labelDiscrepancyCountValue.Text = docCalcInfo.CountDiscrepancy.ToString();
                    labelCountSFValue.Text = docCalcInfo.CountInvoice.ToString();
                    labelDiscrepancyAmountValue.Text = string.Format(format, docCalcInfo.SumAmountDiscrepancy);
                    labelDiscrepancyPVPAmountValue.Text = string.Format(format, docCalcInfo.SumPVPAmountDiscrepancy);
                }
                else
                {
                    labelDiscrepancyCountValue.Text = docCalcInfo.CountDiscrepancyControlRatio.ToString();
                }
            }
        }

        public void SetDataDocumentInfo(DiscrepancyDocumentInfo document)
        {
            if (document != null)
            {
                SetUserComment(document.UserComment);
            }
            else
            {
                teComment.Text = String.Empty;
            }
        }

        public void SetUserComment(string userComment)
        {
            if (userComment != null)
            {
                teComment.Text = userComment;
            }
            else
            {
                teComment.Text = String.Empty;
            }
        }

        public string GetUserComment()
        {
            return teComment.Text;
        }

        private void SetSURInfo(DeclarationSummary declaration)
        {
            _surIndicator.Code = declaration.SUR_CODE;
        }

        [Category("Luxoft")]
        [Description("Fires when user click inn")]
        public event EventHandler OnInnClick;

        private void LinkLabelNameValue_LinkClicked(object sender, Infragistics.Win.FormattedLinkLabel.LinkClickedEventArgs e)
        {
            if (this.OnInnClick != null)
            {
                this.OnInnClick(this, e);
            }
        }
    }
}