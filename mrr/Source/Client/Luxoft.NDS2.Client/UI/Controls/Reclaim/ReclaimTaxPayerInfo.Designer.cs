﻿namespace Luxoft.NDS2.Client.UI.Controls.Reclaim
{
    partial class ReclaimTaxPayerInfo
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            this.tableLayoutMain = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutName = new System.Windows.Forms.TableLayoutPanel();
            this.labelNameCaption = new Infragistics.Win.Misc.UltraLabel();
            this.LinkLabelNameValue = new Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel();
            this._surIndicator = new Luxoft.NDS2.Client.UI.Controls.Sur.SurIcon();
            this.tableLayoutSecond = new System.Windows.Forms.TableLayoutPanel();
            this.labelInnCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelInnValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelDiscrepancyCountCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelDiscrepancyCountValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelDiscrepancyAmountCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelDiscrepancyAmountValue = new Infragistics.Win.Misc.UltraLabel();
            this.tableLayoutThird = new System.Windows.Forms.TableLayoutPanel();
            this.labelKPPCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelKPPValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelCountSFCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelCountSFValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelDiscrepancyPVPAmountCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelDiscrepancyPVPAmountValue = new Infragistics.Win.Misc.UltraLabel();
            this.tableLayoutFourth = new System.Windows.Forms.TableLayoutPanel();
            this.teComment = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.tableLayoutCommentCaption = new System.Windows.Forms.TableLayoutPanel();
            this.labelCommentCaption = new Infragistics.Win.Misc.UltraLabel();
            this.tableLayoutMain.SuspendLayout();
            this.tableLayoutName.SuspendLayout();
            this.tableLayoutSecond.SuspendLayout();
            this.tableLayoutThird.SuspendLayout();
            this.tableLayoutFourth.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teComment)).BeginInit();
            this.tableLayoutCommentCaption.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutMain
            // 
            this.tableLayoutMain.ColumnCount = 1;
            this.tableLayoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutMain.Controls.Add(this.tableLayoutName, 0, 0);
            this.tableLayoutMain.Controls.Add(this.tableLayoutSecond, 0, 1);
            this.tableLayoutMain.Controls.Add(this.tableLayoutThird, 0, 2);
            this.tableLayoutMain.Controls.Add(this.tableLayoutFourth, 0, 3);
            this.tableLayoutMain.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutMain.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutMain.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutMain.Name = "tableLayoutMain";
            this.tableLayoutMain.RowCount = 5;
            this.tableLayoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tableLayoutMain.Size = new System.Drawing.Size(947, 125);
            this.tableLayoutMain.TabIndex = 0;
            // 
            // tableLayoutName
            // 
            this.tableLayoutName.ColumnCount = 4;
            this.tableLayoutName.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutName.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutName.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 500F));
            this.tableLayoutName.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 317F));
            this.tableLayoutName.Controls.Add(this.labelNameCaption, 1, 0);
            this.tableLayoutName.Controls.Add(this.LinkLabelNameValue, 2, 0);
            this.tableLayoutName.Controls.Add(this._surIndicator, 0, 0);
            this.tableLayoutName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutName.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutName.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutName.Name = "tableLayoutName";
            this.tableLayoutName.RowCount = 1;
            this.tableLayoutName.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutName.Size = new System.Drawing.Size(947, 25);
            this.tableLayoutName.TabIndex = 0;
            // 
            // labelNameCaption
            // 
            appearance2.TextHAlignAsString = "Right";
            this.labelNameCaption.Appearance = appearance2;
            this.labelNameCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelNameCaption.Location = new System.Drawing.Point(31, 1);
            this.labelNameCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelNameCaption.Name = "labelNameCaption";
            this.labelNameCaption.Size = new System.Drawing.Size(118, 23);
            this.labelNameCaption.TabIndex = 0;
            this.labelNameCaption.Text = "Наименование:";
            // 
            // LinkLabelNameValue
            // 
            appearance5.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this.LinkLabelNameValue.Appearance = appearance5;
            this.LinkLabelNameValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LinkLabelNameValue.Location = new System.Drawing.Point(151, 1);
            this.LinkLabelNameValue.Margin = new System.Windows.Forms.Padding(1);
            this.LinkLabelNameValue.Name = "LinkLabelNameValue";
            this.LinkLabelNameValue.Size = new System.Drawing.Size(498, 23);
            this.LinkLabelNameValue.TabIndex = 3;
            this.LinkLabelNameValue.TabStop = true;
            this.LinkLabelNameValue.Value = "<a href=\"#\">Наименование налогоплательщика с гиперлинком</a>";
            this.LinkLabelNameValue.LinkClicked += new Infragistics.Win.FormattedLinkLabel.LinkClickedEventHandler(this.LinkLabelNameValue_LinkClicked);
            // 
            // _surIndicator
            // 
            this._surIndicator.BackColor = System.Drawing.Color.Transparent;
            this._surIndicator.Code = null;
            this._surIndicator.Location = new System.Drawing.Point(3, 3);
            this._surIndicator.MaximumSize = new System.Drawing.Size(20, 20);
            this._surIndicator.MinimumSize = new System.Drawing.Size(20, 20);
            this._surIndicator.Name = "_surIndicator";
            this._surIndicator.Size = new System.Drawing.Size(20, 20);
            this._surIndicator.TabIndex = 4;
            // 
            // tableLayoutSecond
            // 
            this.tableLayoutSecond.ColumnCount = 7;
            this.tableLayoutSecond.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutSecond.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutSecond.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutSecond.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutSecond.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tableLayoutSecond.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutSecond.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 187F));
            this.tableLayoutSecond.Controls.Add(this.labelInnCaption, 0, 0);
            this.tableLayoutSecond.Controls.Add(this.labelInnValue, 1, 0);
            this.tableLayoutSecond.Controls.Add(this.labelDiscrepancyCountCaption, 2, 0);
            this.tableLayoutSecond.Controls.Add(this.labelDiscrepancyCountValue, 3, 0);
            this.tableLayoutSecond.Controls.Add(this.labelDiscrepancyAmountCaption, 4, 0);
            this.tableLayoutSecond.Controls.Add(this.labelDiscrepancyAmountValue, 5, 0);
            this.tableLayoutSecond.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutSecond.Location = new System.Drawing.Point(0, 25);
            this.tableLayoutSecond.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutSecond.Name = "tableLayoutSecond";
            this.tableLayoutSecond.RowCount = 1;
            this.tableLayoutSecond.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutSecond.Size = new System.Drawing.Size(947, 25);
            this.tableLayoutSecond.TabIndex = 1;
            // 
            // labelInnCaption
            // 
            appearance1.TextHAlignAsString = "Right";
            this.labelInnCaption.Appearance = appearance1;
            this.labelInnCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInnCaption.Location = new System.Drawing.Point(1, 1);
            this.labelInnCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelInnCaption.Name = "labelInnCaption";
            this.labelInnCaption.Size = new System.Drawing.Size(148, 23);
            this.labelInnCaption.TabIndex = 0;
            this.labelInnCaption.Text = "ИНН:";
            // 
            // labelInnValue
            // 
            this.labelInnValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInnValue.Location = new System.Drawing.Point(151, 1);
            this.labelInnValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelInnValue.Name = "labelInnValue";
            this.labelInnValue.Size = new System.Drawing.Size(108, 23);
            this.labelInnValue.TabIndex = 1;
            this.labelInnValue.Text = "1234567893";
            // 
            // labelDiscrepancyCountCaption
            // 
            this.labelDiscrepancyCountCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDiscrepancyCountCaption.Location = new System.Drawing.Point(261, 1);
            this.labelDiscrepancyCountCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelDiscrepancyCountCaption.Name = "labelDiscrepancyCountCaption";
            this.labelDiscrepancyCountCaption.Size = new System.Drawing.Size(148, 23);
            this.labelDiscrepancyCountCaption.TabIndex = 2;
            this.labelDiscrepancyCountCaption.Text = "Количество расхождений:";
            // 
            // labelDiscrepancyCountValue
            // 
            this.labelDiscrepancyCountValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDiscrepancyCountValue.Location = new System.Drawing.Point(411, 1);
            this.labelDiscrepancyCountValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelDiscrepancyCountValue.Name = "labelDiscrepancyCountValue";
            this.labelDiscrepancyCountValue.Size = new System.Drawing.Size(78, 23);
            this.labelDiscrepancyCountValue.TabIndex = 3;
            this.labelDiscrepancyCountValue.Text = "120";
            // 
            // labelDiscrepancyAmountCaption
            // 
            this.labelDiscrepancyAmountCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDiscrepancyAmountCaption.Location = new System.Drawing.Point(491, 1);
            this.labelDiscrepancyAmountCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelDiscrepancyAmountCaption.Name = "labelDiscrepancyAmountCaption";
            this.labelDiscrepancyAmountCaption.Size = new System.Drawing.Size(158, 23);
            this.labelDiscrepancyAmountCaption.TabIndex = 4;
            this.labelDiscrepancyAmountCaption.Text = "Сумма расхождений (руб.):";
            // 
            // labelDiscrepancyAmountValue
            // 
            this.labelDiscrepancyAmountValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDiscrepancyAmountValue.Location = new System.Drawing.Point(651, 1);
            this.labelDiscrepancyAmountValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelDiscrepancyAmountValue.Name = "labelDiscrepancyAmountValue";
            this.labelDiscrepancyAmountValue.Size = new System.Drawing.Size(148, 23);
            this.labelDiscrepancyAmountValue.TabIndex = 5;
            this.labelDiscrepancyAmountValue.Text = "500 060,00";
            // 
            // tableLayoutThird
            // 
            this.tableLayoutThird.ColumnCount = 7;
            this.tableLayoutThird.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutThird.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutThird.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutThird.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutThird.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tableLayoutThird.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutThird.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 187F));
            this.tableLayoutThird.Controls.Add(this.labelKPPCaption, 0, 0);
            this.tableLayoutThird.Controls.Add(this.labelKPPValue, 1, 0);
            this.tableLayoutThird.Controls.Add(this.labelCountSFCaption, 2, 0);
            this.tableLayoutThird.Controls.Add(this.labelCountSFValue, 3, 0);
            this.tableLayoutThird.Controls.Add(this.labelDiscrepancyPVPAmountCaption, 4, 0);
            this.tableLayoutThird.Controls.Add(this.labelDiscrepancyPVPAmountValue, 5, 0);
            this.tableLayoutThird.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutThird.Location = new System.Drawing.Point(0, 50);
            this.tableLayoutThird.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutThird.Name = "tableLayoutThird";
            this.tableLayoutThird.RowCount = 1;
            this.tableLayoutThird.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutThird.Size = new System.Drawing.Size(947, 25);
            this.tableLayoutThird.TabIndex = 2;
            // 
            // labelKPPCaption
            // 
            appearance3.TextHAlignAsString = "Right";
            this.labelKPPCaption.Appearance = appearance3;
            this.labelKPPCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelKPPCaption.Location = new System.Drawing.Point(1, 1);
            this.labelKPPCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelKPPCaption.Name = "labelKPPCaption";
            this.labelKPPCaption.Size = new System.Drawing.Size(148, 23);
            this.labelKPPCaption.TabIndex = 0;
            this.labelKPPCaption.Text = "КПП:";
            // 
            // labelKPPValue
            // 
            this.labelKPPValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelKPPValue.Location = new System.Drawing.Point(151, 1);
            this.labelKPPValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelKPPValue.Name = "labelKPPValue";
            this.labelKPPValue.Size = new System.Drawing.Size(108, 23);
            this.labelKPPValue.TabIndex = 1;
            this.labelKPPValue.Text = "123456789";
            // 
            // labelCountSFCaption
            // 
            this.labelCountSFCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCountSFCaption.Location = new System.Drawing.Point(261, 1);
            this.labelCountSFCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelCountSFCaption.Name = "labelCountSFCaption";
            this.labelCountSFCaption.Size = new System.Drawing.Size(148, 23);
            this.labelCountSFCaption.TabIndex = 2;
            this.labelCountSFCaption.Text = "Количество записей о СФ:";
            // 
            // labelCountSFValue
            // 
            this.labelCountSFValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCountSFValue.Location = new System.Drawing.Point(411, 1);
            this.labelCountSFValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelCountSFValue.Name = "labelCountSFValue";
            this.labelCountSFValue.Size = new System.Drawing.Size(78, 23);
            this.labelCountSFValue.TabIndex = 3;
            this.labelCountSFValue.Text = "30";
            // 
            // labelDiscrepancyPVPAmountCaption
            // 
            this.labelDiscrepancyPVPAmountCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDiscrepancyPVPAmountCaption.Location = new System.Drawing.Point(491, 1);
            this.labelDiscrepancyPVPAmountCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelDiscrepancyPVPAmountCaption.Name = "labelDiscrepancyPVPAmountCaption";
            this.labelDiscrepancyPVPAmountCaption.Size = new System.Drawing.Size(158, 23);
            this.labelDiscrepancyPVPAmountCaption.TabIndex = 4;
            this.labelDiscrepancyPVPAmountCaption.Text = "ПВП расхождений (руб.):";
            // 
            // labelDiscrepancyPVPAmountValue
            // 
            this.labelDiscrepancyPVPAmountValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDiscrepancyPVPAmountValue.Location = new System.Drawing.Point(651, 1);
            this.labelDiscrepancyPVPAmountValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelDiscrepancyPVPAmountValue.Name = "labelDiscrepancyPVPAmountValue";
            this.labelDiscrepancyPVPAmountValue.Size = new System.Drawing.Size(148, 23);
            this.labelDiscrepancyPVPAmountValue.TabIndex = 5;
            this.labelDiscrepancyPVPAmountValue.Text = "500 060,00";
            // 
            // tableLayoutFourth
            // 
            this.tableLayoutFourth.ColumnCount = 2;
            this.tableLayoutFourth.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutFourth.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutFourth.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutFourth.Controls.Add(this.teComment, 1, 0);
            this.tableLayoutFourth.Controls.Add(this.tableLayoutCommentCaption, 0, 0);
            this.tableLayoutFourth.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutFourth.Location = new System.Drawing.Point(0, 75);
            this.tableLayoutFourth.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutFourth.Name = "tableLayoutFourth";
            this.tableLayoutFourth.RowCount = 1;
            this.tableLayoutFourth.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutFourth.Size = new System.Drawing.Size(947, 50);
            this.tableLayoutFourth.TabIndex = 3;
            // 
            // teComment
            // 
            this.teComment.AlwaysInEditMode = true;
            this.teComment.Location = new System.Drawing.Point(151, 1);
            this.teComment.Margin = new System.Windows.Forms.Padding(1);
            this.teComment.MaxLength = 2000;
            this.teComment.Multiline = true;
            this.teComment.Name = "teComment";
            this.teComment.Scrollbars = System.Windows.Forms.ScrollBars.Vertical;
            this.teComment.Size = new System.Drawing.Size(743, 48);
            this.teComment.TabIndex = 0;
            // 
            // tableLayoutCommentCaption
            // 
            this.tableLayoutCommentCaption.ColumnCount = 1;
            this.tableLayoutCommentCaption.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutCommentCaption.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutCommentCaption.Controls.Add(this.labelCommentCaption, 0, 0);
            this.tableLayoutCommentCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutCommentCaption.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutCommentCaption.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutCommentCaption.Name = "tableLayoutCommentCaption";
            this.tableLayoutCommentCaption.RowCount = 2;
            this.tableLayoutCommentCaption.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutCommentCaption.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutCommentCaption.Size = new System.Drawing.Size(150, 50);
            this.tableLayoutCommentCaption.TabIndex = 1;
            // 
            // labelCommentCaption
            // 
            appearance4.TextHAlignAsString = "Right";
            this.labelCommentCaption.Appearance = appearance4;
            this.labelCommentCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCommentCaption.Location = new System.Drawing.Point(1, 1);
            this.labelCommentCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelCommentCaption.Name = "labelCommentCaption";
            this.labelCommentCaption.Size = new System.Drawing.Size(148, 23);
            this.labelCommentCaption.TabIndex = 0;
            this.labelCommentCaption.Text = "Комментарий:";
            // 
            // ReclaimTaxPayerInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutMain);
            this.Name = "ReclaimTaxPayerInfo";
            this.Size = new System.Drawing.Size(947, 125);
            this.tableLayoutMain.ResumeLayout(false);
            this.tableLayoutName.ResumeLayout(false);
            this.tableLayoutSecond.ResumeLayout(false);
            this.tableLayoutThird.ResumeLayout(false);
            this.tableLayoutFourth.ResumeLayout(false);
            this.tableLayoutFourth.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teComment)).EndInit();
            this.tableLayoutCommentCaption.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutMain;
        private System.Windows.Forms.TableLayoutPanel tableLayoutName;
        private Infragistics.Win.Misc.UltraLabel labelNameCaption;
        private System.Windows.Forms.TableLayoutPanel tableLayoutSecond;
        private Infragistics.Win.Misc.UltraLabel labelInnCaption;
        private Infragistics.Win.Misc.UltraLabel labelInnValue;
        private Infragistics.Win.Misc.UltraLabel labelDiscrepancyCountCaption;
        private Infragistics.Win.Misc.UltraLabel labelDiscrepancyCountValue;
        private Infragistics.Win.Misc.UltraLabel labelDiscrepancyAmountCaption;
        private Infragistics.Win.Misc.UltraLabel labelDiscrepancyAmountValue;
        private System.Windows.Forms.TableLayoutPanel tableLayoutThird;
        private Infragistics.Win.Misc.UltraLabel labelKPPCaption;
        private Infragistics.Win.Misc.UltraLabel labelKPPValue;
        private Infragistics.Win.Misc.UltraLabel labelCountSFCaption;
        private Infragistics.Win.Misc.UltraLabel labelCountSFValue;
        private Infragistics.Win.Misc.UltraLabel labelDiscrepancyPVPAmountCaption;
        private Infragistics.Win.Misc.UltraLabel labelDiscrepancyPVPAmountValue;
        private System.Windows.Forms.TableLayoutPanel tableLayoutFourth;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor teComment;
        private System.Windows.Forms.TableLayoutPanel tableLayoutCommentCaption;
        private Infragistics.Win.Misc.UltraLabel labelCommentCaption;
        private Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel LinkLabelNameValue;
        private Sur.SurIcon _surIndicator;
    }
}
