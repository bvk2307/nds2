﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using Luxoft.NDS2.Client.UI.DiscrepancyDocuments.Models;

namespace Luxoft.NDS2.Client.UI.Controls.Reclaim
{
    public partial class ReclaimHistoryChangeStatus : UserControl
    {
        public ReclaimHistoryChangeStatus()
        {
            InitializeComponent();
        }

        public void SetDataEmpty()
        {
            labelCreateValue.Text = String.Empty;
            labelSendToSEODValue.Text = String.Empty;
            labelSendToNOExecutorValue.Text = String.Empty;
            labelReceivedNOExecutorValue.Text = String.Empty;
            labelSendToTaxPayerValue.Text = String.Empty;
            labelHandedToTaxpayerValue.Text = String.Empty;
            labelCloseValue.Text = String.Empty;
            labelCalculatedDateDeliveryValue.Text = String.Empty;
            labelProlongAnswerDateCaption.Text = "Дата продления ответа:";
            labelProlongAnswerDateValue.Text = String.Empty;
        }

        public void SetDates(DiscrepancyDocumentInfo docInfo, Dictionary<string, DateTime> dates)
        {
            if (dates != null)
            {
                labelCreateValue.Text = FormatDateTime(GetStatusDate(dates, DocStatusDate.Create));
                labelSendToSEODValue.Text = FormatDateTime(GetStatusDate(dates, DocStatusDate.SendToSEOD));

                if (docInfo.DocType != null && docInfo.DocType.EntryId == DocType.Reclaim93_1)
                {
                    labelSendToNOExecutorCaption.Visible = true;
                    labelSendToNOExecutorValue.Visible = true;
                    labelReceivedNOExecutorCaption.Visible = true;
                    labelReceivedNOExecutorValue.Visible = true;
                    labelSendToNOExecutorValue.Text = FormatDateTime(GetStatusDate(dates, DocStatusDate.SendToNOExecutor));
                    labelReceivedNOExecutorValue.Text = FormatDateTime(GetStatusDate(dates, DocStatusDate.ReceivedNOExecutor));
                }
                else
                {
                    labelSendToNOExecutorCaption.Visible = false;
                    labelSendToNOExecutorValue.Visible = false;
                    labelReceivedNOExecutorCaption.Visible = false;
                    labelReceivedNOExecutorValue.Visible = false;
                }

                labelSendToTaxPayerValue.Text = FormatDateTime(GetStatusDate(dates, DocStatusDate.SendToNP));
                labelHandedToTaxpayerValue.Text = FormatDateTime(GetStatusDate(dates, DocStatusDate.ReceivedNP));
                labelCalculatedDateDeliveryValue.Text = FormatDateTime(GetStatusDate(dates, DocStatusDate.CalculateReceived));

                if (dates.ContainsKey(DocStatusDate.CalculateProlong))
                {
                    labelProlongAnswerDateCaption.Text = "Дата продления ответа:";
                    labelProlongAnswerDateValue.Text = FormatDateTime(GetStatusDate(dates, DocStatusDate.CalculateProlong));
                    labelFinishInputAnswerDataValue.Visible = true;
                    labelFinishInputAnswerDataCaption.Visible = true;
                    labelFinishInputAnswerDataValue.Text = FormatDateTime(GetStatusDate(dates, DocStatusDate.CalculateReceived));
                }
                else
                {
                    labelProlongAnswerDateCaption.Text = "Дата ожидания ответа:";
                    labelProlongAnswerDateValue.Text = FormatDateTime(GetStatusDate(dates, DocStatusDate.CalculateWaitAnswer));
                    labelFinishInputAnswerDataValue.Visible = false;
                    labelFinishInputAnswerDataCaption.Visible = false;
                }

                labelCloseValue.Text = FormatDateTime(GetStatusDate(dates, DocStatusDate.Close));
            }
            else
            {
                SetDataEmpty();
            }
        }

        private DateTime? GetStatusDate(Dictionary<string, DateTime> dates, string key)
        {
            DateTime? dt = null;
            if (dates.ContainsKey(key))
            {
                dt = dates[key];
            }
            return dt;
        }

        private string FormatDateTime(DateTime? dt)
        {
            string ret = String.Empty;
            if (dt != null)
            {
                DateTime dtF = (DateTime)dt;
                ret = string.Format("{0:00}.{1:00}.{2:00}", dtF.Day, dtF.Month, dtF.Year);
            }
            return ret;
        }
    }
}
