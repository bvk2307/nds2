﻿namespace Luxoft.NDS2.Client.UI.Controls.Reclaim
{
    partial class ReclaimCommonInfo
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearence1 = new Infragistics.Win.Appearance();
            this.tableLayoutMain = new System.Windows.Forms.TableLayoutPanel();
            this.labelTypeValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelNumberCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelNumberDataValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelStatusCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelStatusValue = new Infragistics.Win.Misc.UltraLabel();
            this.labelCorrectionNumberCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelCorrectionNumberValue = new Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel();
            this.labelTaxPeriodCaption = new Infragistics.Win.Misc.UltraLabel();
            this.labelTaxPeriodValue = new Infragistics.Win.Misc.UltraLabel();
            this.tableLayoutMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutMain
            // 
            this.tableLayoutMain.ColumnCount = 10;
            this.tableLayoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 146F));
            this.tableLayoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 17F));
            this.tableLayoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 137F));
            this.tableLayoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 207F));
            this.tableLayoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 77F));
            this.tableLayoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 105F));
            this.tableLayoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutMain.Controls.Add(this.labelTypeValue, 0, 0);
            this.tableLayoutMain.Controls.Add(this.labelNumberCaption, 1, 0);
            this.tableLayoutMain.Controls.Add(this.labelNumberDataValue, 2, 0);
            this.tableLayoutMain.Controls.Add(this.labelStatusCaption, 3, 0);
            this.tableLayoutMain.Controls.Add(this.labelStatusValue, 4, 0);
            this.tableLayoutMain.Controls.Add(this.labelCorrectionNumberCaption, 5, 0);
            this.tableLayoutMain.Controls.Add(this.labelCorrectionNumberValue, 6, 0);
            this.tableLayoutMain.Controls.Add(this.labelTaxPeriodCaption, 7, 0);
            this.tableLayoutMain.Controls.Add(this.labelTaxPeriodValue, 8, 0);
            this.tableLayoutMain.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutMain.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutMain.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutMain.Name = "tableLayoutMain";
            this.tableLayoutMain.RowCount = 2;
            this.tableLayoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tableLayoutMain.Size = new System.Drawing.Size(1016, 25);
            this.tableLayoutMain.TabIndex = 0;
            // 
            // labelTypeValue
            // 
            this.labelTypeValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelTypeValue.Location = new System.Drawing.Point(1, 1);
            this.labelTypeValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelTypeValue.Name = "labelTypeValue";
            this.labelTypeValue.Size = new System.Drawing.Size(144, 23);
            this.labelTypeValue.TabIndex = 0;
            this.labelTypeValue.Text = "Автоистребование по 93.1";
            // 
            // labelNumberCaption
            // 
            this.labelNumberCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelNumberCaption.Location = new System.Drawing.Point(147, 1);
            this.labelNumberCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelNumberCaption.Name = "labelNumberCaption";
            this.labelNumberCaption.Size = new System.Drawing.Size(15, 23);
            this.labelNumberCaption.TabIndex = 2;
            this.labelNumberCaption.Text = "№";
            // 
            // labelNumberDataValue
            // 
            this.labelNumberDataValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelNumberDataValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelNumberDataValue.Location = new System.Drawing.Point(164, 1);
            this.labelNumberDataValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelNumberDataValue.Name = "labelNumberDataValue";
            this.labelNumberDataValue.Size = new System.Drawing.Size(135, 23);
            this.labelNumberDataValue.TabIndex = 3;
            this.labelNumberDataValue.Text = "15654 от 29.04.2015";
            // 
            // labelStatusCaption
            // 
            this.labelStatusCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelStatusCaption.Location = new System.Drawing.Point(301, 1);
            this.labelStatusCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelStatusCaption.Name = "labelStatusCaption";
            this.labelStatusCaption.Size = new System.Drawing.Size(43, 23);
            this.labelStatusCaption.TabIndex = 4;
            this.labelStatusCaption.Text = "Статус:";
            // 
            // labelStatusValue
            // 
            this.labelStatusValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelStatusValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelStatusValue.Location = new System.Drawing.Point(346, 1);
            this.labelStatusValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelStatusValue.Name = "labelStatusValue";
            this.labelStatusValue.Size = new System.Drawing.Size(205, 23);
            this.labelStatusValue.TabIndex = 5;
            this.labelStatusValue.Text = "Отправлено налогоплательщику";
            // 
            // labelCorrectionNumberCaption
            // 
            this.labelCorrectionNumberCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCorrectionNumberCaption.Location = new System.Drawing.Point(553, 1);
            this.labelCorrectionNumberCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelCorrectionNumberCaption.Name = "labelCorrectionNumberCaption";
            this.labelCorrectionNumberCaption.Size = new System.Drawing.Size(75, 23);
            this.labelCorrectionNumberCaption.TabIndex = 6;
            this.labelCorrectionNumberCaption.Text = "Номер корр.:";
            // 
            // labelCorrectionNumberValue
            // 
            appearence1.BorderAlpha = Infragistics.Win.Alpha.Transparent;
            this.labelCorrectionNumberValue.Appearance = appearence1;
            this.labelCorrectionNumberValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCorrectionNumberValue.Location = new System.Drawing.Point(630, 1);
            this.labelCorrectionNumberValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelCorrectionNumberValue.Name = "labelCorrectionNumberValue";
            this.labelCorrectionNumberValue.Size = new System.Drawing.Size(108, 23);
            this.labelCorrectionNumberValue.TabIndex = 7;
            this.labelCorrectionNumberValue.TabStop = true;
            this.labelCorrectionNumberValue.Value = "<a href=\"#\">999 (123456789098)</a>";
            this.labelCorrectionNumberValue.LinkClicked += new Infragistics.Win.FormattedLinkLabel.LinkClickedEventHandler(this.LabelCorrectionNumberValueLinkClicked);
            // 
            // labelTaxPeriodCaption
            // 
            this.labelTaxPeriodCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelTaxPeriodCaption.Location = new System.Drawing.Point(740, 1);
            this.labelTaxPeriodCaption.Margin = new System.Windows.Forms.Padding(1);
            this.labelTaxPeriodCaption.Name = "labelTaxPeriodCaption";
            this.labelTaxPeriodCaption.Size = new System.Drawing.Size(103, 23);
            this.labelTaxPeriodCaption.TabIndex = 8;
            this.labelTaxPeriodCaption.Text = "Отчетный период:";
            // 
            // labelTaxPeriodValue
            // 
            this.labelTaxPeriodValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelTaxPeriodValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelTaxPeriodValue.Location = new System.Drawing.Point(845, 1);
            this.labelTaxPeriodValue.Margin = new System.Windows.Forms.Padding(1);
            this.labelTaxPeriodValue.Name = "labelTaxPeriodValue";
            this.labelTaxPeriodValue.Size = new System.Drawing.Size(108, 23);
            this.labelTaxPeriodValue.TabIndex = 9;
            this.labelTaxPeriodValue.Text = "сентябрь 2014";
            // 
            // ReclaimCommonInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutMain);
            this.Name = "ReclaimCommonInfo";
            this.Size = new System.Drawing.Size(1016, 25);
            this.tableLayoutMain.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutMain;
        private Infragistics.Win.Misc.UltraLabel labelTypeValue;
        private Infragistics.Win.Misc.UltraLabel labelNumberCaption;
        private Infragistics.Win.Misc.UltraLabel labelNumberDataValue;
        private Infragistics.Win.Misc.UltraLabel labelStatusCaption;
        private Infragistics.Win.Misc.UltraLabel labelStatusValue;
        private Infragistics.Win.Misc.UltraLabel labelCorrectionNumberCaption;
        private Infragistics.Win.Misc.UltraLabel labelTaxPeriodCaption;
        private Infragistics.Win.Misc.UltraLabel labelTaxPeriodValue;
        private Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel labelCorrectionNumberValue;
    }
}
