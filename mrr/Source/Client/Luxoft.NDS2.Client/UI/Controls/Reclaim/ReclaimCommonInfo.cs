﻿ using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Luxoft.NDS2.Common.Contracts.DTO.Business.DiscrepancyDocument;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Declaration;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Selection;
using Luxoft.NDS2.Common.Contracts.DTO.Business.Common;
using Luxoft.NDS2.Client.UI.DiscrepancyDocuments.Models;

namespace Luxoft.NDS2.Client.UI.Controls.Reclaim
{
    public partial class ReclaimCommonInfo : UserControl
    {
        private Dictionary<int, string> typeDesc = new Dictionary<int, string>();

        public ReclaimCommonInfo()
        {
            InitializeComponent();
            InitTypeDesc();
        }

        private void InitTypeDesc()
        {
            typeDesc.Clear();
            typeDesc.Add(1, "Автотребование по СФ");
            typeDesc.Add(2, "Автоистребование по 93");
            typeDesc.Add(3, "Автоистребование по 93.1");
            typeDesc.Add(5, "Автотребование по КС");
        }

        public void SetDataEmpty()
        {
            labelTypeValue.Text = String.Empty;
            labelNumberDataValue.Text = String.Empty;
            labelStatusValue.Text = String.Empty;
            labelStatusValue.Text = String.Empty;
            labelCorrectionNumberValue.Value = "<a href=\"#\"></a>";
            labelTaxPeriodValue.Text = String.Empty;
        }

        public void SetDataDocumentInfo(DiscrepancyDocumentInfo documentInfo, Dictionary<string, DateTime> dates)
        {
            if (documentInfo != null)
            {
                SetTypeName(documentInfo);
                SetNumberDate(documentInfo, dates);
                labelStatusValue.Text = "---";
                if (documentInfo.Status != null)
                {
                    labelStatusValue.Text = documentInfo.Status.EntryValue;
                }
                else
                {
                    labelStatusValue.Text = String.Empty;
                }
            }
        }

        private void SetTypeName(DiscrepancyDocumentInfo documentInfo)
        {
            string typeName = String.Empty;
            if (documentInfo.DocType != null)
            {
                if (typeDesc.ContainsKey(documentInfo.DocType.EntryId))
                {
                    typeName = typeDesc[documentInfo.DocType.EntryId];
                }
            }
            labelTypeValue.Text = typeName;
        }

        private void SetNumberDate(DiscrepancyDocumentInfo documentInfo, Dictionary<string, DateTime> dates)
        {
            if (!string.IsNullOrEmpty(documentInfo.EodId))
            {
                labelNumberDataValue.Text = string.Format("{0} от {1}", documentInfo.EodId, FormatDateTime(documentInfo.EodDate));
            }
            else
            {
                labelNumberDataValue.Text = string.Format("*{0}", documentInfo.Id);
            }
        }

        public void SetDataDeclaration(DeclarationSummary declaration)
        {
            if (declaration != null)
            {
                labelCorrectionNumberValue.Value = string.Format("<a href=\"#\">{0}</a>", declaration.CORRECTION_NUMBER_EFFECTIVE);
                labelTaxPeriodValue.Text = declaration.FULL_TAX_PERIOD;
            }
        }

        private string FormatDateTime(DateTime? dt)
        {
            string ret = String.Empty;
            if (dt != null)
            {
                DateTime dtF = (DateTime)dt;
                ret = string.Format("{0:00}.{1:00}.{2:00}", dtF.Day, dtF.Month, dtF.Year);
            }
            return ret;
        }

        private void LabelCorrectionNumberValueLinkClicked(object sender, Infragistics.Win.FormattedLinkLabel.LinkClickedEventArgs e)
        {
            if (CorrectionNumberLinkClicked != null)
                CorrectionNumberLinkClicked(this, e);
        }

        [Category("Luxoft")]
        [Description("Fires when user click correction number")]
        public event EventHandler CorrectionNumberLinkClicked;
    }
}
