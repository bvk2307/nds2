﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Luxoft.NDS2.Common.Contracts.DTO.Query;

namespace Luxoft.NDS2.Client.UI.Controls.DeclarationsListFilter
{
    public static class QueryConditionsHelper
    {
        public static QueryConditions JoinConditions(this QueryConditions targetConditions,
            QueryConditions sourceConditions)
        {
            var result = (QueryConditions)targetConditions.Clone();

            result.Filter.AddRange(sourceConditions.Filter);
            result.Sorting.AddRange(sourceConditions.Sorting);
            return result;

        }
    }
}
